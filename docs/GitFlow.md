# Structure

The project is based on React Native default template

```
  Root: React Native project
    |_ android: Android Native Project (git submodule) 
    |_ ios: iOS Native Project (git submodule) 
```

# Conventions 

## Branch name convention

This convention is applied to `android`, `ios`, and `React Native`. Each sprint will have its own set of branches

for naming convention, we use *Snake case*

- Main: used to release Dev, QA and Production

    ```
    sprint_[x]/master
    ```

    Example: `sprint_32/master`

- Feature: Used for developing features, will be merged to sprint's main branch and be deleted after merging

    ```
    sprint_[x]/feature/[feature name]
    ```

    Example: `sprint_32/feature/car-park-list-ui`

- Bug: Used for fixing bugs in sprint, will be merged to sprint's main branch and be deleted after merging

    ```
    sprint_[x]/fixbug/[bug name]
    ```
    
    Example: `sprint_32/fixbug/[bug name]`

- Hot fix: In case of bug happened after Production release

    ```
    sprint_[x]/hotfix/android_[android last release's version code]_ios_[ios last release's version code]
    ```
    
    Example: `sprint_32/hotfix/android_254_ios_115`

## Commit message

- For task/bug that have **Jira** ticket, put ticket id at the beginning of the message

    ```
     Breeze [ticket id]: [message]
    ```

    Example: `Breeze 8555: crash when push notification`


# Installation
- Follow [React Native Environment setup](https://reactnative.dev/docs/environment-setup) 
- After install NodeJS, Install yarn by running `npm install -g yarn`. We have to install `yarn` globally for other usages
# Prepare project
- After cloning source code, check out the latest branch
- Remove **Breeze** folder if it exist. This is an redundant folder that had been used to contain source code before we changed the submodule configuration. Its existent can cause unexpected build errors
- Run `git submodule update --init` to pull *android* and *ios* submodules
- From root folder `yarn` to install react native packages
- After finishing all these steps, navigate to `android` or `ios` folder, depend on which platform you are developing on then follow the steps below to build app
- In debug mode, we have to run Metro server to use React Native component in the app. To start Metro server, run `yarn start` from project's root folder in terminal

## Android

- Open `android` folder with *Android Studio* , choose `devDebug` as build variant for `app` from the left panel then waiting for it to finish syncing

  ![](images/android-studio-select-build-variant.png "Title")

- After connecting Android device to PC (Real device through usb cable or emulator), run `adb reverse tcp:8081 tcp:8081` to forward port between device and your PC. This step should be done whenever app cant' connect to React Native's Metro server
- From Menu bar, choose *Run->Run* to run app to phone
## iOS
- Copy 2 files (keys files), `.mapbox` and `.netrc`, to your home folder in your Mac. These files are provided by managers or team leads
- Checkout require branch then run `pod install` to install packages
- Choose `staggingDebug`, then run app

  ![](images/mac-xcode-screen.png "Title")


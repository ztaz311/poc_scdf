# Upgrade React Native 
Version: 0.64.2 -> 0.67.5

Full changelog: https://github.com/facebook/react-native/blob/c05a2c56e2df25a0bc6c75cd494d7e47834a3dc9/CHANGELOG.md

## Major changes

* Remove alert's window when call to hide. (a46a99e120 by @asafkorem)

* ImageBackground now respects imageStyle width and height (dbd5c3d8e5 by @Naturalclar)
* Rename deprecated Keyboard.removeEventListener to Keyboard.removeListener. (8880c09076 by @yungsters)
* Update Modal's mock to not render its children when it is not visible (ec614c16b3 by @AntoineDoubovetzky)
* Setting overflow: scroll in View component style will clip the children in the View container (93beb83abe by @ryancat)
* Use new Locale API on Android 11 (API 30)+ (b7c023a8c1)
* Compute Android Notch in keyboardDidShow height calculation API 28+ (8bef3b1f11 by @fabriziobertoglio1987)
* Fix missing WebView provider crash in ForwardingCookieHandler (d40cb0e1b0 by @RodolfoGS)
* Exclude unused .so files for reduce android .apk and .aab (6f126740fa by @enniel)

* Fixed the issue when moving cursor in multi-line TextInput. (22801870f0 by @xiankuncheng)
* Rename deprecated Keyboard.removeEventListener to Keyboard.removeListener. (8880c09076 by @yungsters)

* Remove StatusBarIOS component (7ce0f40f5c by @ecreeth)

* Support for foreground ripple in Pressable (0823f299e5 by @intergalacticspacehighway)
* StyleSheet.create will no longer do DEV-time validation. (2e8c0bd7ea by @yungsters)

* ColorProps with value null should be defaultColor instead of transparent (842bcb902e by @hank121314)
* It is now deprecated to pass a constructor argument to EventEmitter(...). (14f7a2b707 by @yungsters)
* Removed warning on Android for setTimeout with delays greater than 1 minute. (480dabd665 by @yungsters)
* Remove okhttp3 proguard rules (b4c9f13fe7 by @doniwinata0309)
* Fix font weight numeric values (3827ca6171 by @fabriziobertoglio1987)
* Make NativeModules immediately initializable (2bf866e401 by @RSNara)
* Fix race-condition on the initialization of ReactRootViews (74a756846f by @mdvacca)

## Steps
1. Run `yarn add react-native@0.67.5`
2. Run `yarn add react@17.0.2`
3. Run `cd ios && pod update RCT-Folly --no-repo-update` to update dependencies for iOS
4. There will be warning regarding to modules that used for communicating  between iOS and RN like:
   ``` 
    Module ShareBusinessLogicModule requires main queue setup since it overrides `constantsToExport` but doesn't implement `requiresMainQueueSetup`. In a future release React Native will default to initializing all native modules on a background thread unless explicitly opted-out of.
   ```
    These should be resolved with iOS
source 'https://github.com/CocoaPods/Specs.git'
require_relative '../node_modules/react-native/scripts/react_native_pods'
require_relative '../node_modules/@react-native-community/cli-platform-ios/native_modules'

project 'Breeze',
    'Demo-Release' => :release,
    'Demo-Debug' => :debug,
    'betaRelease' => :release,
    'QA-Debug' => :debug,
    'Staging' => :release,
    'Debug-Testing' => :debug,
    'Staging-Debug' => :debug,
    'Appstore' => :release,
    'Staging-DebugVoice' => :debug,
    'Staging-ReleaseVoice' => :release

platform :ios, '12.0'
use_frameworks!

target 'Breeze' do  
  config = use_native_modules!

  use_react_native!(
    :path => config[:reactNativePath],
    # to enable hermes on iOS, change `false` to `true` and then install pods
    :hermes_enabled => false
  )

  # Enables Flipper.
  #
  # Note that if you have use_frameworks! enabled, Flipper will not work and
  # you should disable the next line.
  #use_flipper!()

 # pod 'Mapbox-iOS-SDK', '~> 6.3.0'
  # pod 'MapboxGeocoder.swift', '~> 0.14'
  # pod 'MapboxCoreNavigation', :git => 'https://github.com/mapbox/mapbox-navigation-ios.git', :tag => 'v2.0.0-rc.2'
  # pod 'MapboxNavigation', :git => 'https://github.com/mapbox/mapbox-navigation-ios.git', :tag => 'v2.0.0-rc.2'
  pod 'SnapKit', '~> 5.0.0'
  #pod 'AWSCognito'
  pod 'AWSCognitoIdentityProvider', '~> 2.30.4'
#  pod 'Amplify'
#  pod 'AmplifyPlugins/AWSPinpointAnalyticsPlugin'
#  pod 'AmplifyPlugins/AWSAPIPlugin'
#  pod 'AmplifyPlugins/AWSDataStorePlugin'
  pod 'Firebase/Analytics'  
  pod 'Firebase/Storage'
  pod 'Firebase/Firestore'
  pod 'Firebase/RemoteConfig'
  #pod 'FirebaseFirestoreSwift'
  pod 'Firebase/Crashlytics'
#  pod 'AmplifyPlugins/AWSCognitoAuthPlugin'
  pod 'FontAwesome.swift'
  pod 'FontAwesomeKit.Swift'
  pod 'ReachabilityManager'
  pod 'SwiftyBeaver'
  #pod 'Instabug', :podspec => 'https://ios-releases.instabug.com/custom/fix-carplay_scene_crash/Instabug.podspec'
  pod 'Instabug', '~> 11.1.0'
  pod 'SwiftyGif'
  pod 'ImageScrollView'
#  pod 'RealmSwift'
  pod 'AWSMobileClient', '~> 2.30.4'
  pod 'AWSKinesis', '~> 2.30.4'
  pod 'AWSAppSync', '~> 3.6.2'
  pod "SwiftyXMLParser", :git => 'https://github.com/yahoojapan/SwiftyXMLParser.git'
  pod 'BVLinearGradient', :path => '../node_modules/react-native-linear-gradient'
  pod 'RxSwift', '6.2.0'
  pod 'RxCocoa', '6.2.0'
  #pod 'gRPC-Swift', '~> 1.0.0'
#  pod 'gRPC-Swift', :podspec => 'gRPC-Swift.podspec'
  pod 'SwiftGen'
#  pod 'gRPC-Swift-Plugins'
#  pod 'MapboxNavigation', :git => 'https://github.com/mapbox/mapbox-navigation-ios.git', :tag => 'v2.4.0'
#  pod 'MapboxCoreNavigation', :git => 'https://github.com/mapbox/mapbox-navigation-ios.git', :tag => 'v2.4.0'
#  pod 'Kingfisher', '~> 7.0'
#  pod "MonthYearPicker", '~> 4.0.2'
  pod 'Firebase/DynamicLinks'
  pod 'KeychainSwift'
  pod 'extol', '1.0.2'
#  pod 'RealmSwift', '10.19.0'
end

target 'BreezeTests' do
  inherit! :search_paths
end

pre_install do |installer|
  installer.pod_targets.each do |pod|
    if pod.name.eql?('RNCMaskedView') || pod.name.eql?('BVLinearGradient') || pod.name.eql?('RNFBAnalytics') || pod.name.eql?('RNStaticSafeAreaInsets') || pod.name.eql?('react-native-geolocation-service')
        def pod.build_type
            Pod::BuildType.static_library
             end
         end
     end
 end

post_install do |installer|
  react_native_post_install(installer)
  installer.pods_project.targets.each do |target|
    
    target.build_configurations.each do |config|
      config.build_settings['IPHONEOS_DEPLOYMENT_TARGET'] = '12.0'
    end
    
    `sed -i -e  $'s/__IPHONE_10_0/__IPHONE_13_0/' #{installer.sandbox.root}/RCT-Folly/folly/portability/Time.h`
    
    # Fix the Cycle inside FBReactNativeSpec issue
    if (target.name&.eql?('FBReactNativeSpec'))
      target.build_phases.each do |build_phase|
        if (build_phase.respond_to?(:name) && build_phase.name.eql?('[CP-User] Generate Specs'))
          target.build_phases.move(build_phase, 0)
        end
      end
    end
    # Fix pod build error for resource build (need team id)
    if target.respond_to?(:product_type) and target.product_type == "com.apple.product-type.bundle"
      target.build_configurations.each do |config|
          config.build_settings['CODE_SIGNING_ALLOWED'] = 'NO'
      end
    end
    target.build_configurations.each do |config|
      if config.name == 'Debug' || config.name == 'Debug_Testing'
        config.build_settings['OTHER_SWIFT_FLAGS'] = ['$(inherited)', '-Onone']
        config.build_settings['SWIFT_OPTIMIZATION_LEVEL'] = '-Owholemodule'
      end
    end
  end
end

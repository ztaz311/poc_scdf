//
//  PriorityQueueTest.swift
//  BreezeTests
//
//  Created by Zhou Hao on 26/5/21.
//

import XCTest
@testable import Breeze

class PriorityQueueTest: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        let array = [1,5,3,2,41,23]
        let queue = PriorityQueue<Int>(ascending: false, startingValues: array)
        XCTAssertEqual(queue.peek(), 41, "The higest priority is wrong")
    }
    
    func testFeatureDetectable() throws {
                
        // TODO: should check distance? It's able to do that by add ordered closure in init
        let erp = FeatureDetectable(id: "ERP-1", distance: 108, priority: FeatureDetection.cruisePriorityErp)
        let schoolzone1 = FeatureDetectable(id: "SchoolZone-1", distance: 28, priority: FeatureDetection.cruisePrioritySchoolZone)
        let silverzone1 = FeatureDetectable(id: "SilverZone-1", distance: 208, priority: FeatureDetection.cruisePrioritySilverZone)
        var queue = PriorityQueue<FeatureDetectable>()
        queue.push(erp)
        queue.push(schoolzone1)
        queue.push(silverzone1)
        XCTAssertEqual(queue.peek()!.id, "ERP-1", "The higest priority is wrong")
        XCTAssertNotEqual(queue.peek()!.id, "SilverZone-1", "The higest priority is wrong")
    }
    
//    func testEqual() throws {
//        let schoolzone1 = FeatureDetectable(id: "SchoolZone-1", distance: 108, priority: 100)
//        let schoolzone2 = FeatureDetectable(id: "SchoolZone-1", distance: 201, priority: 100)
//
//        XCTAssertTrue(schoolzone1 == schoolzone2)
//
//        let silverzone1 = FeatureDetectable(id: "SchoolZone-1", distance: 208, priority: 50)
//        XCTAssertFalse(schoolzone1 == silverzone1)
//    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}


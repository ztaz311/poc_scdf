//
//  DateExtTest.swift
//  BreezeTests
//
//  Created by Zhou Hao on 1/3/22.
//

import XCTest
@testable import Breeze

class DateExtTest: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
//        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testCurrentTimeisBetween() throws {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd HH:mm"
        let testDate = formatter.date(from: "2022/01/01 22:31")
        
//        dateFormatter.dateFormat = "hh:mm:ss a"
        // case 1: Empty start/endtime
        XCTAssertEqual(testDate!.currentTimeisBetween(startTime: "", endTime: ""), false, "Empty startTime or endTime should return false")
        XCTAssertEqual(testDate!.currentTimeisBetween(startTime: "22:01", endTime: "22:40"), true, "Should between start and end time")
        XCTAssertEqual(testDate!.currentTimeisBetween(startTime: "22:32", endTime: "22:40"), false, "Outside the start and end time")
        XCTAssertEqual(testDate!.currentTimeisBetween(startTime: "10:01 PM", endTime: "10:40 PM"), false, "Should between start and end time") // time with pm/am is not valid here
        XCTAssertEqual(testDate!.currentTimeisBetween(startTime: "10:01 am", endTime: "10:40 pm"), false, "Should between start and end time") // time with pm/am is not valid here

        let testDate2 = formatter.date(from: "2022/01/01 09:31")
        XCTAssertEqual(testDate2!.currentTimeisBetween(startTime: "9:01", endTime: "9:40"), true, "Should between start and end time")
        XCTAssertEqual(testDate2!.currentTimeisBetween(startTime: "9:32", endTime: "9:40"), false, "Outside the start and end time")


    }

}

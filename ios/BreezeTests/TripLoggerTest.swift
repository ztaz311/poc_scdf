//
//  TripLoggerTest.swift
//  BreezeTests
//
//  Created by Zhou Hao on 3/9/21.
//

import XCTest
@testable import Breeze
import CoreLocation

class TripLoggerTest: XCTestCase {
    
    override func setUpWithError() throws {
        // Put for testing
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    /*
    func testERPAddNoDuplication() throws {
        let tripLogger = TripLogger(service: MockTripLogService(), isNavigation: true)
        let time = Int(Date().timeIntervalSince1970)
        tripLogger.start(startLocation: CLLocationCoordinate2D(latitude: 0, longitude: 0), address1: "Address1", address2: "Address2")
        tripLogger.exitErp(ERPCost(erpId: 1, name: "ERP-1", cost: 1, exitTime: time, isOBU: "N"))
        tripLogger.exitErp(ERPCost(erpId: 2, name: "ERP-2", cost: 0, exitTime: time + 100, isOBU: "N"))
        XCTAssertEqual(tripLogger.totalERPs.count, 2, "Should be 2 ERPs")
        tripLogger.stop(endLocation: CLLocationCoordinate2D(latitude: 0, longitude: 0), address1: "Address3", address2: "Address4", stoppedByUser: true)
    }

    // add 1 duplicates
    func testERPDuplication1() throws {
        let tripLogger = TripLogger(service: MockTripLogService(), isNavigation: true)
        let time = Int(Date().timeIntervalSince1970)
        tripLogger.start(startLocation: CLLocationCoordinate2D(latitude: 0, longitude: 0), address1: "Address1", address2: "Address2")
        tripLogger.exitErp(ERPCost(erpId: 1, name: "ERP-1", cost: 1, exitTime: time, isOBU: "N"))
        tripLogger.exitErp(ERPCost(erpId: 1, name: "ERP-1", cost: 1, exitTime: time + 10, isOBU: "N"))
        XCTAssertEqual(tripLogger.totalERPs.count, 1, "Should be only 1 ERP")
        tripLogger.stop(endLocation: CLLocationCoordinate2D(latitude: 0, longitude: 0), address1: "Address3", address2: "Address4", stoppedByUser: true)
    }
    
    func testERPDuplicationWithIn15Min() throws {
        let tripLogger = TripLogger(service: MockTripLogService(), isNavigation: true)
        let time = Int(Date().timeIntervalSince1970)
        tripLogger.start(startLocation: CLLocationCoordinate2D(latitude: 0, longitude: 0), address1: "Address1", address2: "Address2")
        tripLogger.exitErp(ERPCost(erpId: 1, name: "ERP-1", cost: 1, exitTime: time, isOBU: "N"))
        tripLogger.exitErp(ERPCost(erpId: 2, name: "ERP-2", cost: 0, exitTime: time + 100, isOBU: "N"))
        tripLogger.exitErp(ERPCost(erpId: 2, name: "ERP-2", cost: 0, exitTime: time + 150, isOBU: "N"))
        XCTAssertEqual(tripLogger.totalERPs.count, 2, "Should be 2 ERPs")
        tripLogger.stop(endLocation: CLLocationCoordinate2D(latitude: 0, longitude: 0), address1: "Address3", address2: "Address4", stoppedByUser: true)
    }

    func testERPDuplicationMoreThan15Min() throws {
        let tripLogger = TripLogger(service: MockTripLogService(), isNavigation: true)
        let time = Int(Date().timeIntervalSince1970)
        tripLogger.start(startLocation: CLLocationCoordinate2D(latitude: 0, longitude: 0), address1: "Address1", address2: "Address2")
        tripLogger.exitErp(ERPCost(erpId: 1, name: "ERP-1", cost: 1, exitTime: time, isOBU: "N"))
        tripLogger.exitErp(ERPCost(erpId: 2, name: "ERP-2", cost: 0, exitTime: time + 100, isOBU: "N"))
        tripLogger.exitErp(ERPCost(erpId: 1, name: "ERP-1", cost: 0, exitTime: time + 901, isOBU: "N"))
        XCTAssertEqual(tripLogger.totalERPs.count, 3, "Should be 3 ERPs")
        tripLogger.stop(endLocation: CLLocationCoordinate2D(latitude: 0, longitude: 0), address1: "Address3", address2: "Address4", stoppedByUser: true)
    }
     */


    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}

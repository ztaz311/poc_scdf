//
//  VoiceInstructorTest.swift
//  BreezeTests
//
//  Created by Zhou Hao on 19/9/22.
//

import XCTest
@testable import Breeze

final class VoiceInstructorTest: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testReplaceInText() throws {
        
        let instructor = VoiceInstructor()
        let text1 = "Turn left. Then, in 30 metres, Turn left onto Third Hospital Avenue."
        let replacedText1 = instructor.testReplaceInText(text1)
        XCTAssertEqual(replacedText1, text1)
        
        let text2 = "In 30 metres, CTE ahead."
        let replacedText2 = instructor.testReplaceInText(text2)
//        XCTAssertEqual(replacedText2, "In 30 metres, C.T.E. ahead.")
        XCTAssertEqual(replacedText2, "In 30 metres, CTE ahead.")
                
        let text3 = "This is CTE ramp"
        let replacedText3 = instructor.testReplaceInText(text3)
//        XCTAssertEqual(replacedText3, "This is C.T.E. ramp")
        XCTAssertEqual(replacedText3, "This is CTE ramp.")

        let text4 = "This is CTE, ramp"
        let replacedText4 = instructor.testReplaceInText(text4)
//        XCTAssertEqual(replacedText3, "This is C.T.E. ramp")
        XCTAssertEqual(replacedText4, "This is CTE, ramp.")

        let text5 = "This is CTE"
        let replacedText5 = instructor.testReplaceInText(text5)
        XCTAssertEqual(replacedText5, "This is CTE.")

        let text6 = "This is CTE."
        let replacedText6 = instructor.testReplaceInText(text6)
        XCTAssertEqual(replacedText6, "This is CTE.")
        
//        let replacedText31 = instructor.testReplaceInText(replacedText3)
    }
    
    func testReplaceInSSML() throws {
        let instructor = VoiceInstructor()
        let xml1 = "<speak><amazon:effect name=\"drc\"><prosody rate=\"1.08\">Drive northeast on <say-as interpret-as=\"address\">Adam Road</say-as>. Then, in 200 metres, Take the <say-as interpret-as=\"address\">PIE</say-as> ramp.</prosody></amazon:effect></speak>"
        let replacedXml2 = instructor.testReplaceInSSMLText(xml1)
    }
    
    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}

//
//  SingaporeMobilePhoneValidatorTest.swift
//  BreezeTests
//
//  Created by Zhou Hao on 22/7/21.
//

import XCTest
@testable import Breeze


class SingaporeMobilePhoneValidatorTest: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testPhoneNumber() throws {
        XCTAssert(!PhoneValidator.isValidSingaporeMobile(number: "8156612"),"Phone number length should be 8")
        XCTAssert(!PhoneValidator.isValidSingaporeMobile(number: "71256612"),"Should only start with 8, 9")
        XCTAssert(PhoneValidator.isValidSingaporeMobile(number: "(658) 221-2819"),"Invalid phone number")
        XCTAssert(PhoneValidator.isValidSingaporeMobile(number: "+65 8221 2819"),"Invalid phone number")
        XCTAssert(!PhoneValidator.isValidSingaporeMobile(number: "+51 8221 2819"),"Invalid country code")
        XCTAssert(!PhoneValidator.isValidSingaporeMobile(number: "51182212819"),"Too long")
    }
    
}


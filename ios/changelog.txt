23/6/2021 3:54 PM Zhou Hao

- #953 - utility module implementation and testing
- Mapbox #136 - ssml text fix (for such as PIE, AYE, etc...)
- #1147 - Speed limit icon too high in Navigation

22/6/2021 6:51 PM Zhou Hao

- Add a xml parser for ssml (Mapbox issue #136)
- #953 - utility module
- Push notification implemented

14/6/2021 7:37 PM Zhou Hao

- Trip Summary UI
- Trip Summary Mock service

13/6/2021 8:10 PM VishnuKanth

- Added AnalyticsEventUtils File which consists of all the event name and event parameters
- Enabled Firebase analytics debug mode
- Added EventService class to log events using Firebase
- Added onBoarding_sign events to firebase

12/6/2021 8:33 AM Zhou Hao

- Modify Trip log function using Trip Summary API

11/6/2021 3:49 PM Zhou Hao

- Json format for Trip Log
- Remove enter time for ERP

11/6/2021 12:45: PM Malou

- JIRA #946 - Arrive to ARrival in turn by turn change in storyboard

11/6/2021 12:45: PM Malou

- JIRA #943 - OTP screen heading modified
- JIRA #946 - Arrive to ARrival in Routeplannig change in storyboard

11/6/2021 8:42 AM VishnuKanth

- Added Sepearate Google-service info.plist for both QA and dev env
- Added custom shell script in build phases to fetch Google service plist and adding them to the target
- Added Dev version 0.3 Build 10 for internal QA release
- Added QA version 0.3 Build 9 for test flight release to Map box team

10/6/2021 10:40 AM Zhou Hao

- SetupPuck after the symbol added

9/6/2021 7:22 PM Zhou Hao

- Remove LocationTrackingManager (PassiveLocationManager) in TurnByTurnNavigationVC.
- Use NavigationService's method to check if it's in Tunnel

9/6/2021 6:46 PM Zhou Hao

- Fixed the data ERP/Traffic reload issue. Should call onEvery(.mapLoaded) instead of onNext(.mapLoaded).

9/6/2021 5:55 PM Zhou Hao

- Add documents/diagrams to docs folder
- Implement a method to stopCruise mode when the speed no more than 1 for more than 300s.

9/6/2021 3:23 PM Zhou Hao

- JIRA #977 - filter out event is not within the valid time range
- Create a timer to check if events are valid (refresh)

9/6/2021 1:04 PM Zhou Hao

- JIRA #981 - recenter button/feedback button
- navigation notification background
- toast message and icon change in route planning
- Replace pen icon

9/6/2021 12:20 PM Zhou Hao

- Exchange mute/unmute button image in navigation and cruise mode
- Trip summary in cruise mode

9/6/2021 11:08 AM Zhou Hao

- Hide the bubble for ERP summary
- Change from onNext to onEvery of .styleLoaded in RoutePlanningVC and CarparksVC so that the route and carpark will be refreshed correctly
- Replace the ERP price bubble icon and adjust the y offset so that the text will be in the center

8/6/2021 9:57 PM Zhou Hao

- JIRA #976: Filter out invalid calendar location coordinates. 















11/6/2021 8:42 AM VishnuKanth

- Added Sepearate Google-service info.plist for both QA and dev env
- Added custom shell script in build phases to fetch Google service plist and adding them to the target
- Added Dev version 0.3 Build 10 for internal QA release
- Added QA version 0.3 Build 9 for test flight release to Map box team


11/6/2021 12:45: PM Malou

- JIRA #943 - OTP screen heading modified
- JIRA #946 - Arrive to ARrival in Routeplannig change in storyboard


11/6/2021 12:45: PM Malou

- JIRA #946 - Arrive to ARrival in turn by turn change in storyboard


11/6/2021 3:45 PM Malou

- JIRA #930 - UI development for Trip log page, viewmodel, mockTripService


12/6/2021 10:00 AM Malou
 - Added/change Trip model according to API requirements
 - Added dateformatters to be used in Trip log page
 
14/6/2021 7:19 PM Malou
 - Added pagination on trip log list
 - Added multiple select on trips
 
 14/6/2021 11:00 PM Malou
 - Added the Back to Top floating button
 
 15/6/2021 5PM Malou
 - Confirmaiton screen for email sending trips selected

 16/6/2021 12:30AM Malou
 - API for fetching trip list integrated, with pagination functionality working also
 - API for sending selected trips to email integrated

 16/6/2021 3:00PM Malou
 - API for tripd details integrated
 
 16/6/2021 7:30PM Malou
  - Trip details UI progess, allow editing of ERP costs, Parking expense and other validations
  
16/6/2021 9:30PM Malou
  - API for updating trip log detail integrated
  
17/6/2021 9:00AM Malou
  - Few changes in API for updating trip detail implementation

Rules:

1.) (Swift) Functions and closures should not be empty - We can't leave a function empty or a block/closures empty

Example: Noncompliant Code Example
let alert = UIAlertController(title: "Alert!", message: message as? String, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default , handler:{ (UIAlertAction)in
            
            
        }))

Compliant Solution
let alert = UIAlertController(title: "Alert!", message: message as? String, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default , handler:{ (UIAlertAction)in
            
            
        }))

deinit {} //This is Non compliant code according to the Sonar Cube rule.

deinit { // Adding a comment why this is not implemented or we can remove if it's not required
  //No need to implement
}

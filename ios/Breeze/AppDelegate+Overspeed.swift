//
//  AppDelegate+Overspeed.swift
//  Breeze
//
//  Created by Tuan, Pham Hai on 24/04/2023.
//

import Foundation
import AVFoundation
import SwiftyBeaver

extension AppDelegate {
    
    private func playCustomSystemSound(name: String, type: String) {
        if let path = Bundle.main.path(forResource: name, ofType: type) {
            let url = URL(fileURLWithPath: path) as CFURL
            var soundID: SystemSoundID = 0
            AudioServicesCreateSystemSoundID(url, &soundID)
//            AudioServicesPlaySystemSound(soundID) // This might be  deprecated in latest iOS version
            AudioServicesPlaySystemSoundWithCompletion(soundID) {
                AudioServicesDisposeSystemSoundID(soundID)
            }
        } else {
            print("file not found!")
        }
    }
    
    func playAudioCue() {
        
        if Prefs.shared.getBoolValue(.enableAudio) {
            playCustomSystemSound(name: "overspeed", type: "caf")
        }
    }
    
    func playBeepAudio() {
        if Prefs.shared.getBoolValue(.enableAudio) {
            playCustomSystemSound(name: "alert_mapsdirectionsinapp_haptic", type: "caf")
        }
    }
}

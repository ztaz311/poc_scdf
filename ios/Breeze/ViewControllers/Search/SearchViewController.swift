//
//  SearchViewController.swift
//  Breeze
//
//  Created by VishnuKanth on 16/12/20.
//

import UIKit
//import MapboxGeocoder
import MapboxNavigation
import MapboxDirections
import CoreLocation



class SearchViewController: BaseViewController,
                            UITableViewDelegate,
                            UITableViewDataSource,
                            UISearchBarDelegate,
                            CLLocationManagerDelegate {
    
    let reuseIdentifier = "EasyBreezyCollectionCell"
    
    @IBOutlet weak var addressSearchBar:UISearchBar!
    
    @IBOutlet weak var backBtn:UIButton!
    
    @IBOutlet weak var moreBtn:UIButton!
    
    @IBOutlet weak var easyBreezesSavedView:UIView!
    
    @IBOutlet weak var recentSearchTableView:UITableView!
    @IBOutlet weak var recentSearchView:UIView!
    
    @IBOutlet weak var searchResultTableView:UITableView!
    
    @IBOutlet weak var searchErrorLabel:UILabel!
    
    @IBOutlet weak var tagListView: TagListView!
    @IBOutlet weak var tagListHeight: NSLayoutConstraint!
    @IBOutlet weak var topView: UIView!
    
    private var dataTask: URLSessionDataTask?
    //private var placeMarks: [GeocodedPlacemark]?
    
    private var searchResult = [SearchAddresses]()
    // private var sortedSearchResult = [NSMutableDictionary]()
    private var filteredLocalSearch:[SavedPlaces] = []
    //private var sortedLocalSearchResult = [NSMutableDictionary]()
    private var filteredEasyBreezySearch:[EasyBreezyAddresses] = []
    // private var sortedEasyBreezySearchResult = [NSMutableDictionary]()
    
    private var savedPlaces:[SavedPlaces] = []
    private var easyBreeziesArray:[EasyBreezyAddresses] = SavedSearchManager.shared.easyBreezy
    private var easyBreeziesTags = [Int]() // keep the index in SavedSearchManager.shared.easyBreezy
    
    var mapView: NavigationMapView?
    
    let numberKeysArray = ["NCS Hub", "MOM", "Suntec City", "Home", "123", "6", "12222", "8", "fa fasf saf", "0", ".", "fasf asfas asgasg asg asg asg as gsag ag ag as ga"]
    
    
    let locationManager = CLLocationManager()
    var location = CLLocation()
    
    var tokenString: String = ""
    var searchSource: String = ""
    private var newSearchResultArray = [AddressBaseModel]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchErrorLabel.setTopView(topView: UILabel())
        addressSearchBar.backgroundImage = UIImage(color: UIColor.white)
        // addressSearchBar.setLeftImage(UIImage(), with: 0, tintColor: UIColor.brandPurpleColor)
        addressSearchBar.setLeftImage(UIImage(), with: 0, tintColor: UIColor.brandPurpleColor, size: CGSize(width: 10, height: 22), textPadding: 19)
        
        addressSearchBar.clearBackgroundColor()
        addressSearchBar.textField?.backgroundColor = UIColor.white//UIColor.searchBarTextFieldBackground
        addressSearchBar.textField?.layer.borderColor = UIColor.searchBorderColor.cgColor
        addressSearchBar.textField?.layer.borderWidth = 1
        addressSearchBar.textField?.clearButtonMode = .never
        addressSearchBar.textField?.textColor = UIColor.searchBarTextColor
        addressSearchBar.textField?.font = UIFont.searchBarTextFont()
        addressSearchBar.delegate = self
        
        
        
        searchResultTableView.isHidden = true
        searchResultTableView.tableFooterView = UIView()
        searchResultTableView.keyboardDismissMode = .onDrag
        recentSearchTableView.keyboardDismissMode = .onDrag
        
        //For changing search bar cursor color
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).tintColor = .brandPurpleColor
        
        
        searchErrorLabel.isHidden = true
        
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.startUpdatingLocation()
        // stackedGrid(rows: 4, columns: 3, rootView: view)
        
        topView.backgroundColor = .clear
        setupTagListView()
        
        setupDarkLightAppearance(forceLightMode: true)
        
        self.view.backgroundColor = .white
    }
    
    deinit {}
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        //        addressSearchBar.setRightImage(normalImage: UIImage(named: "clearSearch")!, highLightedImage: UIImage(named: "clearSearch")!)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        self.tokenString = UUID().uuidString
        if (self.tokenString.count > 10)
        {
            self.tokenString = String(self.tokenString.prefix(10))
            
        }
        
        print(self.tokenString)
        AnalyticsManager.shared.logScreenView(screenName: ParameterName.home_search)
        // keep the search result if any
        if addressSearchBar.text?.count == 0 {
            fetchRecentSearchData()
            easyBreeziesArray = SavedSearchManager.shared.easyBreezy
            updateEasyBreezies()
            updateTopView()
            updateToBeforeSearch()
        }
    }
    
    override func viewDidLayoutSubviews() {
        updateEasyBreeziesLayout()
    }
    
    private func setupTagListView() {
        tagListView.delegate = self
        tagListView.borderColor = UIColor.easyBreezyTagBorderColor
        tagListView.cornerRadius = 16
        tagListView.borderWidth = 0.3
        tagListView.tagBackgroundColor = UIColor.easyBreezyTagBackgroundColor
        tagListView.backgroundColor = .clear
        tagListView.alignment = .left
        tagListView.textFont = UIFont.easyBreeziesTagFont()
        tagListView.textColor = UIColor.easyBreezyTagFontColor
        tagListView.paddingX = 12
        tagListView.rightPaddingX = 12
        tagListView.paddingY = 8
        tagListView.marginY = 10
        tagListView.marginX = 10
    }
    
    private func updateEasyBreezies() {
        self.tagListHeight.constant = 0
        easyBreeziesTags.removeAll()
        
        for (index, _) in SavedSearchManager.shared.easyBreezy.enumerated() {
            if index == 5 {
                break
            }
            easyBreeziesTags.append(index)
        }
        
        self.tagListView.removeAllTags()
        easyBreeziesTags.forEach {
            if(self.tagListView.rowViews.count <= 2)
            {
                let eb = SavedSearchManager.shared.easyBreezy[$0]
                if let alias = eb.alias {
                    let tag = alias.truncate(limit: 18)
                    self.tagListView.addTag(tag, image: UIImage(named: "smallBookmark"))
                }
            }
            
        }
        updateEasyBreeziesLayout()
    }
    
    private func updateEasyBreeziesLayout() {
        self.tagListHeight.constant = self.tagListView.intrinsicContentSize.height
    }
    
    @objc func onButton(button: UIButton){
        print(button.currentTitle!)
    }
    
    func fetchRecentSearchData() {
        
        savedPlaces = SavedSearchManager.shared.places
        if(savedPlaces.count == 0){
            recentSearchView.isHidden = true
        }
        else
        {
            recentSearchView.isHidden = false
            easyBreezesSavedView.isHidden = false
            searchResultTableView.isHidden = true
            self.recentSearchTableView.reloadData()
            
        }
    }
    
    @IBAction func backbtnAction(){
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.back, screenName: ParameterName.home_search)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clearSaveSearch(sender: UIButton) {
        
        self.popupAlert(title: nil, message: Constants.searchClearMessage, actionTitles: [Constants.cancel,Constants.clearAll], actions:[{action1 in
            
            
        },{action2 in
            
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.recent_search_clear, screenName: ParameterName.home_search)
            self.savedPlaces.removeAll()
            SavedSearchManager.shared.places = self.savedPlaces
            self.fetchRecentSearchData()
            
        }, nil])
        
    }
    
    @IBAction func moreBtnClick(sender: UIButton) {
        
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.favourite_more, screenName: ParameterName.home_search)
        let storyboard: UIStoryboard = UIStoryboard(name: "MenuView", bundle: nil)
        let vc: UIViewController = storyboard.instantiateViewController(withIdentifier: "EasyBreezyVC") as UIViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func deleteSearch(sender: UIButton){
        
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.recent_search_single_clear, screenName: ParameterName.home_search)
        savedPlaces.remove(at: sender.tag)
        SavedSearchManager.shared.places = savedPlaces
        fetchRecentSearchData()
        
    }
    
    // MARK: - Theme updated
    //    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
    //        super.traitCollectionDidChange(previousTraitCollection)
    //        guard previousTraitCollection != traitCollection else {
    //            return
    //        }
    //        setupGradientColor()
    //    }
    
    //MARK: table view datasource
    
    open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        var returnCell = UITableViewCell()
        
        if tableView == searchResultTableView {
            
            if(indexPath.section == 0)
            {
                guard let cell : EBSearchCell = tableView.dequeueReusableCell(withIdentifier: "ebSearchCell") as? EBSearchCell else {
                    preconditionFailure("reusable cell not found")
                }
                
                guard filteredEasyBreezySearch.count > 0 else {
                    return cell
                }
                
                if(filteredEasyBreezySearch.count > 0 && filteredEasyBreezySearch.count > indexPath.row)
                {
                    //cell.easyBreezyAlias.isHidden = false
                    
                    cell.searchsubTitle.text = filteredEasyBreezySearch[indexPath.row].address2
                    let lat = filteredEasyBreezySearch[indexPath.row].lat?.toDouble()
                    let long = filteredEasyBreezySearch[indexPath.row].long?.toDouble()
                    cell.navigationImage.image = UIImage(named: "smallBookmark")
                    cell.searchdistance.text = LocationManager.shared.getDistance(lat: lat!, long: long!)
                    cell.easyBreezyAlias.text = filteredEasyBreezySearch[indexPath.row].alias
                    cell.searchMainTitle.text = filteredEasyBreezySearch[indexPath.row].address1
                    
                    returnCell = cell
                    
                    //                    //sorted local search result
                    //                    let sortedDic = sortedEasyBreezySearchResult[indexPath.row]
                    //                    let searchObj = sortedDic.value(forKey: "searchAddresObj") as! EasyBreezyAddresses
                    //                    cell.searchsubTitle.text = searchObj.address2
                    //                    let lat = searchObj.lat?.toDouble()
                    //                    let long = searchObj.long?.toDouble()
                    //
                    //                    cell.navigationImage.image = UIImage(named: "destinationMark")
                    //                    cell.searchdistance.text = LocationManager.shared.getDistance(lat: lat!, long: long!)
                    //                    cell.easyBreezyAlias.text = searchObj.alias
                    //                    cell.searchMainTitle.text = searchObj.address1
                    //
                    //                    returnCell = cell
                    
                }
                
            }
            else
            {
                guard let cell : SearchCell = tableView.dequeueReusableCell(withIdentifier: "searchCell") as? SearchCell else {
                    preconditionFailure("reusable cell not found")
                }
                
                guard filteredLocalSearch.count > 0 || newSearchResultArray.count > 0 else {
                    return cell
                }
                
                if(indexPath.section == 1)
                {
                    if(filteredLocalSearch.count > 0 && filteredLocalSearch.count > indexPath.row)
                    {
                        cell.searchMainTitle.text = filteredLocalSearch[indexPath.row].placeName
                        cell.searchsubTitle.text = filteredLocalSearch[indexPath.row].address
                        cell.navigationImage.image = UIImage(named:"clock")
                        let lat = filteredLocalSearch[indexPath.row].lat.toDouble()
                        let long = filteredLocalSearch[indexPath.row].long.toDouble()
                        
                        cell.searchdistance.text = LocationManager.shared.getDistance(lat: lat!, long: long!)
                        
                        returnCell = cell
                        
                        //                        //sorted local search result
                        //                        let sortedDic = sortedLocalSearchResult[indexPath.row]
                        //                        let searchObj = sortedDic.value(forKey: "searchAddresObj") as! SavedPlaces
                        //                        cell.searchMainTitle.text = searchObj.placeName
                        //                        cell.searchsubTitle.text = searchObj.address
                        //                        cell.navigationImage.image = UIImage(named:"clock")
                        //                        let lat = searchObj.lat.toDouble()
                        //                        let long = searchObj.long.toDouble()
                        //
                        //                        cell.searchdistance.text = LocationManager.shared.getDistance(lat: lat!, long: long!)
                        //
                        //                        returnCell = cell
                    }
                }
                else
                {
                    
                    cell.searchsubTitle.text = newSearchResultArray[indexPath.row].address2
                    let lat = newSearchResultArray[indexPath.row].lat?.toDouble()
                    let long = newSearchResultArray[indexPath.row].long?.toDouble()
                    cell.navigationImage.image = UIImage(named: "navigation")
                    cell.searchdistance.text = newSearchResultArray[indexPath.row].distance//LocationManager.shared.getDistance(lat: lat!, long: long!)
                    
                    
                    cell.searchMainTitle.text = newSearchResultArray[indexPath.row].address1
                    
                    returnCell = cell
                    
                    
                    //                    cell.searchsubTitle.text = searchResult[indexPath.row].address2
                    //                    let lat = searchResult[indexPath.row].lat?.toDouble()
                    //                    let long = searchResult[indexPath.row].long?.toDouble()
                    //                    cell.navigationImage.image = UIImage(named: "navigation")
                    //                    cell.searchdistance.text = LocationManager.shared.getDistance(lat: lat!, long: long!)
                    //
                    //
                    //                    cell.searchMainTitle.text = searchResult[indexPath.row].address1
                    //
                    //                    returnCell = cell
                    
                    
                    
                    //                    //sorted saerch result
                    //                    let sortedDic = sortedSearchResult[indexPath.row]
                    //                    let searchObj = sortedDic.value(forKey: "searchAddresObj") as! SearchAddresses
                    //                    cell.searchsubTitle.text = searchObj.address2
                    //                    let lat = searchObj.lat?.toDouble()
                    //                    let long = searchObj.long?.toDouble()
                    //                    cell.navigationImage.image = UIImage(named: "navigation")
                    //                    cell.searchdistance.text = LocationManager.shared.getDistance(lat: lat!, long: long!)
                    //
                    //
                    //                    cell.searchMainTitle.text = searchObj.address1
                    //
                    //                    returnCell = cell
                }
                
            }
            
        }
        else
        {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "recentSearchCell") as! RecentSearchCell
            
            //            guard let cell = tableView.dequeueReusableCell(withIdentifier: "recentSearchCell") as! RecentSearchCell else { }
            
            let savedSearch = savedPlaces[indexPath.row]
            cell.recentSearchText.text = savedSearch.placeName
            cell.deleteButton.tag = indexPath.row
            cell.deleteButton.addTarget(self, action: #selector(deleteSearch(sender:)), for: .touchUpInside)
            
            returnCell = cell
        }
        
        return returnCell
    }
    
    open func numberOfSections(in tableView: UITableView) -> Int {
        var count = 0
        if tableView == searchResultTableView {
            
            count = 3
        }
        else
        {
            count = 1
        }
        
        return count
    }
    
    open func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count = 0
        if(addressSearchBar.text!.count > 0)
        {
            if(section == 0)
            {
                count = self.filteredEasyBreezySearch.count
            }
            else if(section == 1)
            {
                count = self.filteredLocalSearch.count
            }
            else if(section == 2)
            {
                //  count = self.searchResult.count
                count = self.newSearchResultArray.count
            }
        }
        else{
            count = savedPlaces.count
        }
        return count
    }
    
    //MARK: table view delegate
    
    open func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        if ReachabilityManager.shared.connectionStatus == .unavailable {
            self.popupAlert(title: Constants.noInternetTitle, message: Constants.noInternetMessage, actionTitles: [Constants.gotIt], actions:[{action1 in
                self.dismissAnyAlertControllerIfPresent()
                
            },{action2 in
                
            }, nil])
            return
        }
        addressSearchBar.resignFirstResponder()
        tableView.deselectRow(at: indexPath, animated: false)
        if tableView == searchResultTableView {
            
            if(indexPath.section == 2)
            {
                if(newSearchResultArray.count > 0){
                    
                    var placeLat: String = ""
                    var placeLong: String = ""
                    
                    //call API search details if source is google to get lat long
                    if (self.searchSource == "GOOGLE"){
                        DispatchQueue.global(qos: .background).async { [self] in
                            
                            // SearchRecommendService.shared.getSearchResult(lat: lat, long: long, searchString: searchString) { [weak self] (result) in
                            
                            SearchRecommendService.shared.getSearchDetails(placeId: newSearchResultArray[indexPath.row].placeId, token: self.tokenString) { [weak self] (result) in
                                
                                guard let strongSelf = self else {
                                    return
                                }
                                
                                switch result {
                                
                                case .success(let json):
                                    print(json)
                                    DispatchQueue.main.async {
                                        
                                        placeLat = String(format: "%f", json.details.lat as! CVarArg)
                                        placeLong = String(format: "%f", json.details.long as! CVarArg)
                                        

                                        let strDate = Date().dateString(format: Date.defaultFormat)
                                        
                                        print(newSearchResultArray[indexPath.row])
                                        let place = SearchAddresses.init(alias: "", address1:  newSearchResultArray[indexPath.row].address1!, lat:  placeLat, long:  placeLong, address2:  newSearchResultArray[indexPath.row].address2!, distance:  newSearchResultArray[indexPath.row].distance!)
                                        
                                        
                                        
                                        let savedPlace = SavedPlaces(placeName: place.address1 ?? "", address: place.address2 ?? "", lat: place.lat ?? "", long: place.long ?? "", time: strDate)
                                        
                                        //If local saved search array count is equals to 8, then we will replace last object with latest selected search item else we will append
                                        if(!SavedSearchManager.shared.ifRecentSearchHasName(address1: place.address1 ?? ""))
                                        {
                                            if(savedPlaces.count == 8){
                                                savedPlaces[7] = savedPlace
                                            }
                                            else
                                            {
                                                savedPlaces.append(savedPlace)
                                            }
                                            
                                            SavedSearchManager.shared.places = savedPlaces
                                        }
                                        
                                        //                    let selectedAdd = SelectedAddress(lat: savedPlace.lat , long: savedPlace.long , address: savedPlace.placeName )
                                        gotoRoutePlanning(address: place,index: indexPath.row+1,section: indexPath.section,ebAddress: nil)
                                        }
                                        
                                    
                                case .failure(_):
                                    
                                    DispatchQueue.main.async {
                                       
                                        
                                    }
                                }
                                
                            }
                        }
                    } else {
                        let strDate = Date().dateString(format: Date.defaultFormat)
                        
                        print(newSearchResultArray[indexPath.row])
                        let place = SearchAddresses.init(alias: "", address1:  newSearchResultArray[indexPath.row].address1!, lat:  newSearchResultArray[indexPath.row].lat!, long:  newSearchResultArray[indexPath.row].long!, address2:  newSearchResultArray[indexPath.row].address2!, distance:  newSearchResultArray[indexPath.row].distance!)
                        
                        
                        
                        let savedPlace = SavedPlaces(placeName: place.address1 ?? "", address: place.address2 ?? "", lat: place.lat ?? "", long: place.long ?? "", time: strDate)
                        
                        //If local saved search array count is equals to 8, then we will replace last object with latest selected search item else we will append
                        if(!SavedSearchManager.shared.ifRecentSearchHasName(address1: place.address1 ?? ""))
                        {
                            if(savedPlaces.count == 8){
                                savedPlaces[7] = savedPlace
                            }
                            else
                            {
                                savedPlaces.append(savedPlace)
                            }
                            
                            SavedSearchManager.shared.places = savedPlaces
                        }
                        
                        //                    let selectedAdd = SelectedAddress(lat: savedPlace.lat , long: savedPlace.long , address: savedPlace.placeName )
                        gotoRoutePlanning(address: place,index: indexPath.row+1,section: indexPath.section,ebAddress: nil)
                    }
                   
                    
                    
                    
                }
            }
            else
            {
                if(indexPath.section == 0)
                {
                    if(filteredEasyBreezySearch.count > 0 && filteredEasyBreezySearch.count > indexPath.row)
                    {
                        //                        let sortedDic = filteredEasyBreezySearch[indexPath.row]
                        //                        let place = sortedDic.value(forKey: "searchAddresObj") as! EasyBreezyAddresses
                        
                        gotoRoutePlanning(address: nil,index: indexPath.row+1,section: indexPath.section,ebAddress: filteredEasyBreezySearch[indexPath.row])
                    }
                }
                else if(indexPath.section == 1)
                {
                    if(filteredLocalSearch.count > 0 && filteredLocalSearch.count > indexPath.row)
                    {
                        
                        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.recent_search_select, screenName: ParameterName.home_search)
                        
                        //                        let sortedDic = filteredLocalSearch[indexPath.row]
                        //                        let place = sortedDic.value(forKey: "searchAddresObj") as! SavedPlaces
                        
                        //let place =  filteredLocalSearch[indexPath.row]
                        
                        let place =  filteredLocalSearch[indexPath.row]
                        
                        let addressSearch = SearchAddresses(alias: "", address1: place.placeName, lat: place.lat, long: place.long, address2: place.address, distance: "")
                        
                        let index = SavedSearchManager.shared.recentSearchIndex(name: place.placeName)
                        
                        gotoRoutePlanning(address: addressSearch,index: index + 1,section: indexPath.section,ebAddress: nil)
                    }
                }
                
            }
        }
        else
        {
            let place =  savedPlaces[indexPath.row]
            
            let addressSearch = SearchAddresses(alias: "", address1: place.placeName, lat: place.lat, long: place.long, address2: place.address, distance: "")
            
            
            gotoRoutePlanning(address: addressSearch,index: indexPath.row + 1,section: 1,ebAddress: nil)
        }
        
    }
    
    open func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.0
    }
    
    // Make the background color show through
    open func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    
    
    func selectAddress(address:[String:Any]){
        
        if ReachabilityManager.shared.connectionStatus == .unavailable {
            self.popupAlert(title: Constants.noInternetTitle, message: Constants.noInternetMessage, actionTitles: [Constants.gotIt], actions:[{action1 in
                self.dismissAnyAlertControllerIfPresent()
                
            },{action2 in
                
            }, nil])
            return
        }
        
        DispatchQueue.global(qos: .background).async {
            
            SelectAddressService.shared.selectAddress(address: address) { (result) in
                print(result)
                switch result {
                case .success(_):
                    print("successSelectAddress")
                    
                case .failure(_):
                    DispatchQueue.main.async {
                        print("failedSelectAddress")
                    }
                }
            }
        }
    }
    
    private func gotoRoutePlanning(address: SearchAddresses?,index:Int,section:Int,ebAddress:EasyBreezyAddresses?){
        
        if ReachabilityManager.shared.connectionStatus == .unavailable {
            self.popupAlert(title: Constants.noInternetTitle, message: Constants.noInternetMessage, actionTitles: [Constants.gotIt], actions:[{action1 in
                self.dismissAnyAlertControllerIfPresent()
                
            },{action2 in
                
            }, nil])
            return
        }
        let story = UIStoryboard(name: "RoutePlanning", bundle: nil)
        if let vc = story.instantiateViewController(withIdentifier: String(describing: RoutePlanningVC.self)) as? RoutePlanningVC {
            if(section == 0)
            {
                let index =   SavedSearchManager.shared.getEBIndex(name: ebAddress?.alias ?? "")
                //                vc.easyBreezyAddress =  ebAddress
                vc.rankingIndex = index + 1
                vc.selectionSource = SourceAddressSelection.EasyBreezies
                vc.originalAddress = ebAddress
            }
            else
            {
                //                vc.destinationAddress =  address
                vc.selectionSource = section == 1 ? SourceAddressSelection.SearchHistory : SourceAddressSelection.AddressSearch
                vc.rankingIndex = index
                vc.originalAddress = address
            }
            // TODO: To remove easyBreezyAddress/destinationAddress from RoutePlanningVC in the future
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    private func updateTopView() {
        
        if addressSearchBar.text != nil && addressSearchBar.text!.count > 0 {
            self.topView.backgroundColor = .white
            self.topView.showBottomShadow()
        } else {
            self.topView.backgroundColor = .clear
            self.topView.hideBottomShadow()
        }
        
    }
    
    @objc func processSearch() {
        guard let searchString = addressSearchBar.text else {
            return
        }
        
        updateTopView()
        
        if searchString.count == 0 {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.addressSearchBar.resignFirstResponder()
            }
            //self.placeMarks?.removeAll()
            updateToBeforeSearch()
            fetchRecentSearchData()
        }
        else if(searchString.count < 3){
            DispatchQueue.main.async {
                //self.placeMarks?.removeAll()
                self.filteredEasyBreezySearch = [EasyBreezyAddresses]()
                self.searchResult = [SearchAddresses]();
                self.newSearchResultArray = [AddressBaseModel]();
                self.filteredLocalSearch = [SavedPlaces]()
                //self.updateWithNoSearchResult()
                self.easyBreezySearch(searchString)
                self.localSearch(searchString)
            }
        }
        else
        {
            //Fetching default recommended search address based on user current location
            self.searchResult = [SearchAddresses]();
            self.newSearchResultArray = [AddressBaseModel]();
            //            self.sortedSearchResult = [NSMutableDictionary]()
            //            self.sortedLocalSearchResult = [NSMutableDictionary]()
            //            self.sortedEasyBreezySearchResult = [NSMutableDictionary]()
            let lat = location.coordinate.latitude.toString()
            let long = location.coordinate.longitude.toString()
            
            DispatchQueue.global(qos: .background).async {
                
                // SearchRecommendService.shared.getSearchResult(lat: lat, long: long, searchString: searchString) { [weak self] (result) in
                
                SearchRecommendService.shared.getNewSearchResult(lat: lat, long: long, searchString: searchString, token: self.tokenString) { [weak self] (result) in
                    
                    guard let self = self else { return }
                    
                    switch result {
                    case .success(let json):
                        DispatchQueue.main.async {
                            if(json.addresses.count > 0){
                                self.searchSource = json.source
                                self.newSearchResultArray = json.addresses
                                //sort searchresult
                                // strongSelf.sortSearchResult()
                                
                                self.searchErrorLabel.isHidden = true
                                self.searchResultTableView.isHidden = false
                                self.searchResultTableView.reloadData()
                                self.easyBreezesSavedView.isHidden = true
                                self.recentSearchTableView.isHidden = true
                                self.easyBreezySearch(searchString)
                                self.localSearch(searchString)
                            } else {
                                self.easyBreezySearch(searchString)
                                self.localSearch(searchString)
                            }
                        }
                    case .failure(_):
                        DispatchQueue.main.async {
                            self.easyBreezySearch(searchString)
                            self.localSearch(searchString)
                        }
                    }
                }
            }
        }
    }
    
    
    
    
    
    //    func sortSearchResult(){
    //
    //        print(searchResult)
    //
    //        self.sortedSearchResult = [NSMutableDictionary]()
    //        for searchAddress in self.searchResult{
    //            let toLocation = CLLocation(latitude: searchAddress.lat!.toDouble()!, longitude: searchAddress.long!.toDouble()!)
    //            let distance = location.distance(from: toLocation) / 1000
    //
    //
    //            let resultDetail = NSMutableDictionary()
    //            resultDetail.setValue(searchAddress, forKey: "searchAddresObj")
    //            resultDetail.setValue(distance, forKey: "distance")
    //            self.sortedSearchResult.append(resultDetail)
    //        }
    //
    //        self.sortedSearchResult.sort{
    //            ((($0 as! Dictionary<String, AnyObject>)["distance"] as? Double)!) < ((($1 as!  Dictionary<String, AnyObject>)["distance"] as? Double)!)
    //        }
    //
    //        print(sortedSearchResult)
    //
    //    }
    //
    //    func sortLocalSearchResult(){
    //
    //        print(filteredLocalSearch)
    //
    //        self.sortedLocalSearchResult = [NSMutableDictionary]()
    //        for searchAddress in self.filteredLocalSearch{
    //            let toLocation = CLLocation(latitude: searchAddress.lat.toDouble()!, longitude: searchAddress.long.toDouble()!)
    //            let distance = location.distance(from: toLocation) / 1000
    //
    //
    //            let resultDetail = NSMutableDictionary()
    //            resultDetail.setValue(searchAddress, forKey: "searchAddresObj")
    //            resultDetail.setValue(distance, forKey: "distance")
    //            self.sortedLocalSearchResult.append(resultDetail)
    //        }
    //
    //        self.sortedLocalSearchResult.sort{
    //            ((($0 as! Dictionary<String, AnyObject>)["distance"] as? Double)!) < ((($1 as!  Dictionary<String, AnyObject>)["distance"] as? Double)!)
    //        }
    //
    //        print(sortedLocalSearchResult)
    //
    //    }
    //
    //    func sortEasyBreezySearchResult(){
    //
    //        print(filteredEasyBreezySearch)
    //
    //        self.sortedEasyBreezySearchResult = [NSMutableDictionary]()
    //        for searchAddress in self.filteredEasyBreezySearch{
    //            let toLocation = CLLocation(latitude: searchAddress.lat!.toDouble()!, longitude: searchAddress.long!.toDouble()!)
    //            let distance = location.distance(from: toLocation) / 1000
    //
    //
    //            let resultDetail = NSMutableDictionary()
    //            resultDetail.setValue(searchAddress, forKey: "searchAddresObj")
    //            resultDetail.setValue(distance, forKey: "distance")
    //            self.sortedEasyBreezySearchResult.append(resultDetail)
    //        }
    //
    //        self.sortedEasyBreezySearchResult.sort{
    //            ((($0 as! Dictionary<String, AnyObject>)["distance"] as? Double)!) < ((($1 as!  Dictionary<String, AnyObject>)["distance"] as? Double)!)
    //        }
    //
    //        print(sortedSearchResult)
    //
    //    }
    
    
    
    private func updateWithNoSearchResult() {
        
        if(self.newSearchResultArray.count == 0 && self.filteredEasyBreezySearch.count == 0 && self.filteredLocalSearch.count == 0)
        {
            
            self.searchResult = [SearchAddresses]();
            self.newSearchResultArray = [AddressBaseModel]();
            // self.sortedSearchResult = [NSMutableDictionary]()
            self.filteredLocalSearch = [SavedPlaces]()
            // self.sortedLocalSearchResult = [NSMutableDictionary]()
            self.filteredEasyBreezySearch = [EasyBreezyAddresses]()
            // self.sortedEasyBreezySearchResult = [NSMutableDictionary]()
            self.searchResultTableView.reloadData()
            self.searchErrorLabel.isHidden = false
            self.searchResultTableView.isHidden = true
            self.easyBreezesSavedView.isHidden = true
            self.recentSearchTableView.isHidden = true
            
            
        }
        else if(self.newSearchResultArray.count > 0 || self.filteredEasyBreezySearch.count > 0 || self.filteredLocalSearch.count > 0)
        {
            self.searchResultTableView.reloadData()
            self.searchErrorLabel.isHidden = true
            self.searchResultTableView.isHidden = false
            self.easyBreezesSavedView.isHidden = true
            self.recentSearchTableView.isHidden = true
        }
        else if(newSearchResultArray.count == 0)
        {
            self.searchResult = [SearchAddresses]();
            self.newSearchResultArray = [AddressBaseModel]();
            // self.sortedSearchResult = [NSMutableDictionary]()
            self.searchResultTableView.reloadData()
        }
        
        else if(filteredEasyBreezySearch.count == 0)
        {
            self.filteredEasyBreezySearch = [EasyBreezyAddresses]()
            // self.sortedEasyBreezySearchResult = [NSMutableDictionary]()
            self.searchResultTableView.reloadData()
        }
        
        else if(filteredLocalSearch.count == 0)
        {
            self.filteredLocalSearch = [SavedPlaces]()
            // self.sortedLocalSearchResult = [NSMutableDictionary]()
            self.searchResultTableView.reloadData()
        }
        
        //        else if(self.searchResult.count == 0 && self.filteredEasyBreezySearch.count > 0 && self.filteredLocalSearch.count == 0)
        //        {
        //
        //            self.searchResult = [SearchAddresses]();
        //            self.filteredLocalSearch = [SavedPlaces]()
        //            self.searchResultTableView.reloadData()
        //            self.searchErrorLabel.isHidden = true
        //            self.searchResultTableView.isHidden = false
        //            self.easyBreezesSavedView.isHidden = true
        //            self.recentSearchTableView.isHidden = true
        //        }
        //
        //        else if(self.searchResult.count == 0 && self.filteredEasyBreezySearch.count > 0 && self.filteredLocalSearch.count == 0)
        //        {
        //
        //            self.searchResult = [SearchAddresses]();
        //            self.filteredLocalSearch = [SavedPlaces]()
        //            self.searchResultTableView.reloadData()
        //            self.searchErrorLabel.isHidden = true
        //            self.searchResultTableView.isHidden = false
        //            self.easyBreezesSavedView.isHidden = true
        //            self.recentSearchTableView.isHidden = true
        //        }
        
    }
    
    private func localSearch(_ searchString:String){
        
        
        filteredLocalSearch = savedPlaces.filter { (place: SavedPlaces) -> Bool in
            
            if (place.placeName.lowercased().range(of:searchString.lowercased()) != nil || place.address.lowercased().range(of:searchString.lowercased()) != nil)
            {
                return true
            }
            else
            {
                return false
            }
            
        }
        
       // var tempFilteredLocalSearch:[SavedPlaces] = []
        
        if(filteredLocalSearch.count > 0){
            //sortLocalSearchResult()
            DispatchQueue.main.async {
                
                if(self.newSearchResultArray.count > 0){
                    
                    for i in 0..<self.filteredLocalSearch.count{
                        
                        for j in 0..<self.newSearchResultArray.count{
                            
                            if(self.filteredLocalSearch.count > 0)
                            {
                                if(self.filteredLocalSearch[i].placeName == self.newSearchResultArray[j].address1){
                                    self.filteredLocalSearch.remove(at: i)
                                    
                                }
                                
                                
                            }
                            
                        }
                        
                    }
                    
//                    self.filteredLocalSearch.removeAll()
//                    self.filteredLocalSearch = tempFilteredLocalSearch
                }
                
                self.searchErrorLabel.isHidden = true
                self.searchResultTableView.isHidden = false
                self.easyBreezesSavedView.isHidden = true
                self.recentSearchTableView.isHidden = true
                self.searchResultTableView.reloadData()
            }
            
        }
        else
        {
            DispatchQueue.main.async {
                self.updateWithNoSearchResult()
            }
            
        }
    }
    
    private func easyBreezySearch(_ searchString:String){
        
        filteredEasyBreezySearch = easyBreeziesArray.filter { (place: EasyBreezyAddresses) -> Bool in
            
            if (place.alias!.lowercased().range(of:searchString.lowercased()) != nil || place.address1!.lowercased().range(of:searchString.lowercased()) != nil || place.address2!.lowercased().range(of:searchString.lowercased()) != nil)
            {
                
                return true
            }
            else
            {
                return false
            }
            
        }
        
        if(filteredEasyBreezySearch.count > 0){
            // sortEasyBreezySearchResult()
            DispatchQueue.main.async {
                self.searchErrorLabel.isHidden = true
                self.searchResultTableView.isHidden = false
                self.easyBreezesSavedView.isHidden = true
                self.recentSearchTableView.isHidden = true
                self.searchResultTableView.reloadData()
            }
            
        }
        else
        {
            DispatchQueue.main.async {
                self.updateWithNoSearchResult()
            }
            
        }
    }
    
    private func updateToBeforeSearch() {
        self.searchResultTableView.isHidden = true
        self.recentSearchTableView.isHidden = false
        self.easyBreezesSavedView.isHidden = false
        self.addressSearchBar.text = ""
        self.searchErrorLabel.isHidden = true
    }
    
    //MARK: Search API
    //    @objc func searchLocations(_ searchString: String){
    
    //        self.processSearch(searchString)
    //        ReachabilityManager.shared.isReachable(success: {
    //            self.processSearch(searchString)
    //          }, failure: {
    //            self.popupAlert(title: Constants.noInternetTitle, message: Constants.noInternetMessage, actionTitles: [Constants.gotIt], actions:[{action1 in
    //                self.dismissAnyAlertControllerIfPresent()
    //
    //
    //            },{action2 in
    //
    //            }, nil])
    //          })
    
    
    
    
    
    
    //        let geocoder = Geocoder.shared
    //
    //        let options = ForwardGeocodeOptions(query: searchString)
    //
    //        // To refine the search, you can set various properties on the options object.
    //        options.allowedISOCountryCodes = ["SG"]
    //        options.allowedScopes = [.all] //[.address, .pointOfInterest,]
    //        options.maximumResultCount = 10
    //
    //        let task = geocoder.geocode(options) { (placemarks, attribution, error) in
    //
    //            if(placemarks!.count > 0)
    //            {
    //                self.placeMarks = placemarks;
    //                self.searchResultTableView.isHidden = false
    //                self.searchResultTableView.reloadData()
    //            }
    //            else
    //            {
    //                self.placeMarks = placemarks;
    //                self.searchResultTableView.isHidden = true
    //                self.searchResultTableView.reloadData()
    //            }
    //
    //        }
    //
    //        task.resume()
    //
    //        self.dataTask = task
    //    }
    
    // MARK: - UISearchBar Delegate Methods
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool
    {
        AnalyticsManager.shared.logEditEvent(eventValue: ParameterName.search_address_input, screenName: ParameterName.home_search)
        if ReachabilityManager.shared.connectionStatus == .unavailable {
            self.popupAlert(title: Constants.noInternetTitle, message: Constants.noInternetMessage, actionTitles: [Constants.gotIt], actions:[{action1 in
                self.dismissAnyAlertControllerIfPresent()
                
            },{action2 in
                
            }, nil])
            return false
        }
        
        return true
    }
    
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool
    {
        
        return true
    }
    
    //    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
    //        guard let searchString = addressSearchBar.text else { return }
    //
    //        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(processSearch), object: searchString)
    //        perform(#selector(processSearch), with: searchString, afterDelay: 0.5)
    //    }
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(processSearch), object: nil)
        perform(#selector(processSearch), with: nil, afterDelay: 0.5)
        return true
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if(searchText == "")
        {
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.search_address_clear, screenName: ParameterName.home_search)
        }
        addressSearchBar.setRightImage(normalImage: UIImage(named: "clearSearch")!, highLightedImage: UIImage(named: "clearSearch")!)
        
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(processSearch), object: nil)
        perform(#selector(processSearch), with: nil, afterDelay: 0.3)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        searchBar.resignFirstResponder()
        
        guard let searchString = searchBar.text else { return }
        
        //DispatchQueue.global().async {
        if(searchString.count > 0)
        {
            self.processSearch()
        }
        //}
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        location = locations.last!
    }
    
    private func  locationManager(manager: CLLocationManager, didFailWithError error: NSError)
    {
        print ("Errors:" + error.localizedDescription)
    }
    
    
}

extension SearchViewController: TagListViewDelegate {
    func tagPressed(_ title: String, tagView: TagView, sender: TagListView) -> Void {
        
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.favourite_select, screenName: ParameterName.home_search)
        if ReachabilityManager.shared.connectionStatus == .unavailable {
            self.popupAlert(title: Constants.noInternetTitle, message: Constants.noInternetMessage, actionTitles: [Constants.gotIt], actions:[{action1 in
                self.dismissAnyAlertControllerIfPresent()
                
            },{action2 in
                
            }, nil])
            return
        }
        
        // this may cause crash because the some title are too long and it's not match the original eb name
        //        let index = SavedSearchManager.shared.getEBIndex(name: title)
        var ebIndex = -1
        for (index,tagView) in tagListView.tagViews.enumerated() {
            if tagView.title(for: .normal) == title {
                ebIndex = index
                break
            }
        }
        
        guard ebIndex >= 0 else {
            return
        }
        
        let index = easyBreeziesTags[ebIndex]
        
        let storybard = UIStoryboard(name: "RoutePlanning", bundle: nil)
        if let vc = storybard.instantiateViewController(withIdentifier: String(describing: RoutePlanningVC.self)) as? RoutePlanningVC {
            let address = SavedSearchManager.shared.easyBreezy[index]
            //            vc.easyBreezyAddress = address
            vc.selectionSource = SourceAddressSelection.EasyBreezies
            vc.rankingIndex = index + 1
            vc.addressID = String(address.addressid!)
            vc.originalAddress = address
            self.navigationController?.pushViewController(vc, animated: true)
        }
        // TODO: Remove easyBreezyAddress from RoutePlanning in the future
    }
    
    func tagRemoveButtonPressed(_ title: String, tagView: TagView, sender: TagListView) -> Void {
        
    }
}


class CustomTextField: UITextField {
    
    var inset:CGFloat = 12  // You can set the inset you want
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        
        return bounds.insetBy(dx: inset, dy: 0)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        
        return bounds.insetBy(dx: inset, dy: 0)
    }
    
}

//
//  WebVC.swift
//  Breeze
//
//  Created by Malou Mendoza on 26/8/21.
//

import UIKit
import WebKit

enum WebVCType {
    case signup
    case onlyShow
}

protocol WebVCDelegate: NSObjectProtocol {
    func didAccepted()
    func didDeclined()
    func didBacked()
}

class WebVC: BaseViewController, WKUIDelegate,WKNavigationDelegate  {

    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var backbtn: UIButton!
    @IBOutlet weak var headerLbl: UILabel!
    var isAccountCreation: Bool = false
    var headerString: String = ""
    var urlString: String = ""
    
    var type: WebVCType = .signup
    var isGuestUser: Bool = false
    
    weak var delegate: WebVCDelegate?
    
    var leading = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupUI()
        
        if(headerString != "FAQ")
        {
            urlString = self.urlString.replacingOccurrences(of: "/api/v1", with: "")
            let timestamp = NSDate().timeIntervalSince1970
            let appendTimeStamp = "\(urlString)?time=\(timestamp)"
            let myURL = URL(string: appendTimeStamp)
                    let myRequest = URLRequest(url: myURL!)
            webView.load(myRequest)
        }
        else{
            
            
            let myURL = URL(string: urlString)
                    let myRequest = URLRequest(url: myURL!)
            webView.load(myRequest)
        }
        
//        if !AWSAuth.sharedInstance.isSignedIn{
//            setupDarkLightAppearance(forceLightMode: true)
//        }
        setupDarkLightAppearance(forceLightMode: true)
        
    }
    
    // MARK: - Properties
        lazy var webView: WKWebView = {
            let webConfiguration = WKWebViewConfiguration()
            let webView = WKWebView(frame: .zero, configuration: webConfiguration)
            webView.uiDelegate = self
            webView.navigationDelegate = self
            webView.scrollView.showsVerticalScrollIndicator = false
            return webView
        }()
    
    override var useDynamicTheme: Bool {
        return true
    }

    func setupUI() {
        
        //Adding contentview
        let contentView = UIView()
        contentView.backgroundColor = UIColor(named: "topViewBGColor")!
        self.view.addSubview(contentView)
                
        if headerString != "FAQ" {
            webView.backgroundColor = .clear
            webView.isOpaque = false
            
            leading = 24.0
        }
        
        //Adding webview inside content view
        contentView.addSubview(webView)
        
        contentView.translatesAutoresizingMaskIntoConstraints = false
        webView.translatesAutoresizingMaskIntoConstraints = false
        
        if (self.isAccountCreation){
            self.headerLbl.isHidden = true
            let image = UIImage(named: "newAppLogo")
            let imageView = UIImageView(image: image!)
            imageView.frame = CGRect(x: 10, y: 30, width: 56, height: 74)
            contentView.addSubview(imageView)
            
            let noThanksBtn = UIButton(frame: .zero)
            noThanksBtn.addTarget(self, action:#selector(self.noThankAction), for: .touchUpInside)
            contentView.addSubview(noThanksBtn)
            
            let iAcceptBtn = UIButton(frame: .zero)
            iAcceptBtn.addTarget(self, action:#selector(self.acceptBtnAction), for: .touchUpInside)
            contentView.addSubview(iAcceptBtn)
      
            noThanksBtn.translatesAutoresizingMaskIntoConstraints = false
            iAcceptBtn.translatesAutoresizingMaskIntoConstraints = false
            
            NSLayoutConstraint.activate([
                
                contentView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: leading),
                
                contentView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -leading),
                contentView.topAnchor.constraint(equalTo: self.backbtn.bottomAnchor, constant: 5),
                contentView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -20),
                
                webView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 0),
                webView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: 0),
                webView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 120),
                webView.bottomAnchor.constraint(equalTo: noThanksBtn.topAnchor, constant: -20),
                
                
                noThanksBtn.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 2),
               // noThanksBtn.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -24),
                noThanksBtn.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -10),
                noThanksBtn.heightAnchor.constraint(equalToConstant: 48),
                
               // iAcceptBtn.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 26),
                iAcceptBtn.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -2),
                iAcceptBtn.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -10),
                iAcceptBtn.heightAnchor.constraint(equalToConstant: 48)
            ])
           
            noThanksBtn.setTitle("   No thanks   ", for: .normal)
            noThanksBtn.backgroundColor = .white
            noThanksBtn.setTitleColor(UIColor.brandPurpleColor, for: .normal)
            noThanksBtn.titleLabel?.font = UIFont.buttonWebivewTitleTextFont()
            noThanksBtn.layer.cornerRadius = 24
            noThanksBtn.layer.borderColor = UIColor.brandPurpleColor.cgColor
            noThanksBtn.layer.borderWidth = 1.0
           // noThanksBtn.applyGradient(colors: [UIColor.white.cgColor,UIColor.white.cgColor], withShadow: false)
            
            iAcceptBtn.setTitle("   I accept   ", for: .normal)
            iAcceptBtn.backgroundColor = UIColor.brandPurpleColor
            iAcceptBtn.setTitleColor(.white, for: .normal)
            iAcceptBtn.titleLabel?.font = UIFont.buttonWebivewTitleTextFont()
            iAcceptBtn.layer.cornerRadius = 30
            iAcceptBtn.applyGradient(colors: [UIColor.brandPurpleColor.cgColor,UIColor.brandPurpleColor.cgColor], withShadow:  false)
            
        } else {
            
            self.headerLbl.isHidden = false
            self.headerLbl.text = self.headerString
            NSLayoutConstraint.activate([
                
                
                contentView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: leading),
                
                contentView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -leading),
                contentView.topAnchor.constraint(equalTo: self.topView.bottomAnchor, constant: 10),
                contentView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -10),
                
                webView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 0),
                webView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: 0),
                webView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 0),
                webView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -5)
                
            ])
           
            self.topView.showBottomShadow()
        }
        
        if type == .onlyShow {
            self.backbtn.isHidden = true
        }
    }
    
    @IBAction func backbtnAction() {
        self.navigationController?.popViewController(animated: true)
        
        delegate?.didBacked()
    }
    
    @IBAction func noThankAction() {
        if type == .signup {
            self.navigationController?.popViewController(animated: true)
        }
        
        delegate?.didDeclined()
    }
    
    @IBAction func acceptBtnAction(){
        // Accepted TC => on local
        if type == .signup && !isGuestUser {
            let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc: NewSigninGuestModeViewController = storyboard.instantiateViewController(withIdentifier: "NewSigninGuestModeViewController")  as! NewSigninGuestModeViewController
            vc.isSignUp = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
        delegate?.didAccepted()
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        
        //This is to adjust the wkwebview size
        if(headerString != "FAQ")
        {
            let js = "document.getElementsByTagName('body')[0].style.webkitTextSizeAdjust='300%'"//dual size
            webView.evaluateJavaScript(js, completionHandler: nil)
            
            if isDarkMode {
                let js = "document.getElementsByTagName('body')[0].style.webkitTextFillColor= 'white'"
                webView.evaluateJavaScript(js, completionHandler: nil)
            }
        } 
    }

    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction,
                 decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        guard
            let url = navigationAction.request.url,
            let scheme = url.scheme else {
            decisionHandler(.cancel)
            return
        }
        
        
        if (scheme.lowercased() == "mailto") {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
            
            decisionHandler(.cancel)
            return
        }
        decisionHandler(.allow)
    }
   
}

//
//  ParkingWebVC.swift
//  Breeze
//
//  Created by Malou Mendoza on 4/10/21.
//

import UIKit
import WebKit

class ParkingWebVC: UIViewController, WKUIDelegate,WKNavigationDelegate, WKScriptMessageHandler  {
    
    
    
    @IBOutlet weak var contentView: UIView!
    
    
    var urlString: String = ""
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupUI()
        
        setupDarkLightAppearance(forceLightMode: true)
    }
    
    // MARK: - Properties
    lazy var webView: WKWebView = {
        
        let preferences = WKPreferences()
        preferences.javaScriptEnabled = true
        
        let config = WKWebViewConfiguration()
        config.preferences = preferences
        
        let script = WKUserScript(source: createScript(), injectionTime: .atDocumentEnd, forMainFrameOnly: false)
        
        config.userContentController.addUserScript(script)
        config.userContentController.add(self, name: "iosClickListener")
        
        
        print(config)
        webView = WKWebView(frame: UIScreen.main.bounds, configuration: config)
        webView.uiDelegate = self
        webView.navigationDelegate = self
        webView.scrollView.showsVerticalScrollIndicator = false
        return webView
    }()
    
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        
        
        print("\(message.name) " + "\(message.body)")
        if message.body as! String == "btn-next-step" {
            print("✅ NEXT")
        }
        else if message.body as! String == "cls-extend-parking" {
            
            
            print("✅ EXTEND PARKING")
        }
        else if message.body as! String == "btn-cancel cls-extend-parking-cancel" {
            print("✅ CANCEL EXTEND")
        }
        else if message.body as! String == "btn-next-step cls-start-parking" {
            print("✅ START PARKING")
            //trigger local notif 8seconds for extend parking
            // if appDelegate().carPlayConnected {
            UNUserNotificationCenter.current().removeAllDeliveredNotifications()
            let manager = LocalNotificationManager()
            manager.addNotification(title: "Let's make parking a Breeze!",subTitle: "Extend your parking!")
            manager.scheduleNotifications(timeInterval: 8,userInfo:["endtime":"1","cost":"2","timeleft":"3"])
            //}
            
        }
        else if message.body as! String == "btn-cancel cls-start-parking-cancel" {
            print("✅ CANCEL")
            let manager = LocalNotificationManager()
            manager.cancelNotifications()
            goBacktoHomeScreen()
        }
        else if message.body as! String == "started-actions-extend" {
            
            print("✅ EXTEND")
        }
        else if message.body as! String == "started-actions-end" {
            print("✅ END EARLY")
            //            let storyboard = UIStoryboard(name: "TravelLog", bundle: nil)
            //            if let vc = storyboard.instantiateViewController(withIdentifier: String(describing: TrvaelLogListVC.self)) as? TrvaelLogListVC {
            //                self.navigationController?.pushViewController(vc, animated: true)
            //            }
        }
        
        else if message.body as! String == "btn-cancel cls-extend-parking-cancel-ok" {
            print("✅ OK EXTEND PARKING")
        }
        else if message.body as! String == "btn-next-step cls-end-parking" {
            print("✅ END NOW")
        }
        else if message.body as! String == "btn-cancel cls-end-parking-cancel" {
            print("✅ CANCEL END")
        }
        else if message.body as! String == "btn-cancel cls-end-parking-cancel-ok" {
            print("✅ OK END NOW")
            let manager = LocalNotificationManager()
            manager.cancelNotifications()
            goBacktoHomeScreen()
            
        }
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super .viewDidAppear(animated)
        
        
        urlString = "https://ezpark.coderuse.xyz/"
        
        let myURL = URL(string: urlString)
        let myRequest = URLRequest(url: myURL!)
        webView.load(myRequest)
        
    }
    
    
    func setupUI() {
        
        
        
        //Adding contentview
        //let contentView = UIView()
        contentView.backgroundColor = .red
        // self.view.addSubview( contentView)
        
        
        //Adding webview inside content view
        contentView.addSubview(webView)
        
        //  contentView.translatesAutoresizingMaskIntoConstraints = false
        webView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            
            
            webView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 0),
            webView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: 0),
            webView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 0),
            webView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: 0)
            
        ])
        
        
    }
    
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        //No need to handle this
    }
    
    
    
    func createScript()-> String{
        let scriptSource = """
            
         
            var nextBtn = document.getElementById('next-step');
         
            if(nextBtn != null) {
                    nextBtn.addEventListener("click", function(){
                    window.webkit.messageHandlers.iosClickListener.postMessage('btn-next-step');
                   
                });
            }
                
            var startParkingBtn = document.getElementsByClassName('btn-next-step cls-start-parking')[0];
            
            if(startParkingBtn != null) {
                    startParkingBtn.addEventListener("click", function(e){
                     console.log(this.className)
                    window.webkit.messageHandlers.iosClickListener.postMessage(this.className);
                   
                });
            }
            var CancelBtn = document.getElementsByClassName('btn-cancel cls-start-parking-cancel')[0];
            
            if(CancelBtn != null) {
                    CancelBtn.addEventListener("click", function(){
                    window.webkit.messageHandlers.iosClickListener.postMessage(this.className);
                   
                });
            }
         
            var ExtendBtn = document.getElementById('started-actions-extend');
            
            if(ExtendBtn != null) {
                    ExtendBtn.addEventListener("click", function(){
                    window.webkit.messageHandlers.iosClickListener.postMessage('started-actions-extend');
                   
             });
            }
         
            var EndEarlyBtn = document.getElementById('started-actions-end');
            
            if(EndEarlyBtn != null) {
                    EndEarlyBtn.addEventListener("click", function(){
                    window.webkit.messageHandlers.iosClickListener.postMessage('started-actions-end');
                   
                });
            }
         
         
            var ExtendParkingBtn = document.getElementsByClassName('cls-extend-parking')[0];
            
            if(ExtendParkingBtn != null) {
                    ExtendParkingBtn.addEventListener("click", function(){
                    window.webkit.messageHandlers.iosClickListener.postMessage('cls-extend-parking');
                   
                });
            }
            var CancelParkingBtn = document.getElementsByClassName('btn-cancel cls-extend-parking-cancel')[0];
            
            if(CancelParkingBtn != null) {
                                                                        CancelParkingBtn.addEventListener("click", function(){
                    window.webkit.messageHandlers.iosClickListener.postMessage('btn-cancel cls-extend-parking-cancel');
                   
                });
            }
         
            var OkExtendBtn = document.getElementsByClassName('btn-cancel cls-extend-parking-cancel-ok')[0];
            
            if(OkExtendBtn != null) {
                                                                                OkExtendBtn.addEventListener("click", function(){
                    window.webkit.messageHandlers.iosClickListener.postMessage('btn-cancel cls-extend-parking-cancel-ok');
                   
                });
            }
         
            var EndNowBtn = document.getElementsByClassName('btn-next-step cls-end-parking')[0];
            
            if(EndNowBtn != null) {
                                                                                        EndNowBtn.addEventListener("click", function(){
                    window.webkit.messageHandlers.iosClickListener.postMessage('btn-next-step cls-end-parking');
                   
                });
            }
         
         
            var CancelEndNowBtn = document.getElementsByClassName('btn-cancel cls-end-parking-cancel')[0];
            
            if(CancelEndNowBtn != null) {
                                                                                        CancelEndNowBtn.addEventListener("click", function(){
                    window.webkit.messageHandlers.iosClickListener.postMessage('btn-cancel cls-end-parking-cancel');
                   
                });
            }
         
            var CancelOkEndNowBtn = document.getElementsByClassName('btn-cancel cls-end-parking-cancel-ok')[0];
            
            if(CancelOkEndNowBtn != null) {
                                                                                                                                CancelOkEndNowBtn.addEventListener("click", function(){
                    window.webkit.messageHandlers.iosClickListener.postMessage('btn-cancel cls-end-parking-cancel-ok');
                   
                });
            }
         
         var observer = new MutationObserver(function(mutations) {
             mutations.forEach(function(mutation) {
               console.log('mutation.type = ' + mutation.type);
               for (var i = 0; i < mutation.addedNodes.length; i++) {
                 var node = mutation.addedNodes[i];
                 if (node.nodeType == Node.ELEMENT_NODE && node.className == 'cls-extend-parking') {
                     var content = node.textContent;
                     console.log('  "' + content + '" added');
                     window.webkit.messageHandlers.stopTimesLoaded.postMessage('cls-extend-parking');
                 }
               }
             });
           });
         observer.observe(document, { childList: true, subtree: true });
         
         """
        return scriptSource
    }
    
    func goBacktoHomeScreen(){
        if let controllers = self.navigationController?.viewControllers, !controllers.isEmpty {
            for controller in controllers {
                if controller.isKind(of: MapLandingVC.self) {
                    self.navigationController?.popToViewController(controller, animated: true)                    
                    break
                }
            }
        }
    }
}



//
//  WebViewController.swift
//  Breeze
//
//  Created by VishnuKanth on 03/12/20.
//

import UIKit
import WebKit
class WebViewController: UIViewController,WKUIDelegate,WKNavigationDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupUI()
        let urlString = appDelegate().isTermsClicked ? "https://dev.breeze.com.sg/app/terms-of-use.html" : "https://dev.breeze.com.sg/app/privacy.html"
        
        let timestamp = NSDate().timeIntervalSince1970
        let appendTimeStamp = "\(urlString)?time=\(timestamp)"
        let myURL = URL(string: appendTimeStamp)
        let myRequest = URLRequest(url: myURL!,cachePolicy: .reloadIgnoringLocalAndRemoteCacheData)
        webView.load(myRequest)
        
        setupDarkLightAppearance(forceLightMode: true)
    }
    
    // MARK: - Properties
        lazy var webView: WKWebView = {
            let webConfiguration = WKWebViewConfiguration()
            let webView = WKWebView(frame: .zero, configuration: webConfiguration)
            webView.uiDelegate = self
            webView.navigationDelegate = self
            return webView
        }()
    
    func setupUI() {
        
        self.view.backgroundColor = UIColor.viewTransparentBackground.withAlphaComponent(0.5)
        
        //Adding contentview
        let contentView = UIView()
        contentView.backgroundColor = .white
        self.view.addSubview(contentView)
        
        //Adding webview inside content view
        contentView.addSubview(webView)
        
        let okayButton = UIButton(frame: .zero)
        okayButton.addTarget(self, action:#selector(self.buttonClicked), for: .touchUpInside)
        contentView.addSubview(okayButton)
        
        
        contentView.translatesAutoresizingMaskIntoConstraints = false
        webView.translatesAutoresizingMaskIntoConstraints = false
        okayButton.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            
            contentView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 16),
            
            contentView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -16),
            contentView.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 98),
            contentView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -98),
            
            webView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
            
            webView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16),
            
            webView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 20),
            
            webView.bottomAnchor.constraint(equalTo: okayButton.topAnchor, constant: -20),
            
            
            okayButton.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 24),
            okayButton.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -24),
            okayButton.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -30),
            okayButton.heightAnchor.constraint(equalToConstant: 56)
        ])
        contentView.layer.cornerRadius = 20
        okayButton.setTitle(Constants.okay, for: .normal)
        okayButton.backgroundColor = UIColor.rgba(r: 255, g: 138, b: 25, a: 1.0)
        okayButton.setTitleColor(.white, for: .normal)
        okayButton.titleLabel?.font = UIFont.buttonTextBoldFont()
        okayButton.layer.cornerRadius = 30
        okayButton.applyGradient(colors: [UIColor.brandPurpleColor.cgColor,UIColor.brandPurpleColor.cgColor])
        
        }
    
    @objc func buttonClicked() {
        
        removeChildViewController(self)
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        
        //This is to adjust the wkwebview size
        let js = "document.getElementsByTagName('body')[0].style.webkitTextSizeAdjust='300%'"//dual size
        webView.evaluateJavaScript(js, completionHandler: nil)
    }
    
}

//
//  OBUDisplayMobileMapUpdatable.swift
//  Breeze
//
//  Created by Tuan, Pham Hai on 05/06/2023.
//

import Foundation
import Turf
import MapboxNavigation
@_spi(Restricted) import MapboxMaps
import MapboxDirections
import SwiftyBeaver

final class OBUDisplayMobileMapUpdatable: MapViewUpdatable {
    weak var navigationMapView: MapboxNavigation.NavigationMapView?
    weak var delegate: MapViewUpdatableDelegate?
    
    // MARK: - Constants
    private let topPadding: CGFloat = 48
    private let trailingX: CGFloat = 24
    private let iconSize: CGFloat = 48
    
    private var speedLimitView: CustomSpeedLimitView?
    private var closeButton: UIButton!
    private var containerView: UIView!
    private var searchButton: UIButton!
    private var muteButton: ToggleButton!
    private var parkingButton: ToggleButton!
    private var petrolButton: ToggleButton!
    private var evButton: ToggleButton!
    private var roadNamePanel: RoadNamePanel?
    private var shareDriveButton: UIButton!
    private var liveLocationButton: LiveLocationSharingButton! //Set it up after mute button
//    private var endButton: UIButton!
    
    private var notificationView: BreezeNotificationView!
    private var showTrafficInformationView: TrafficNotificationView!
    private var obuCommonNotificationView: ObuCommonNotificationView!
    private var showCarParkUpdateView: ParkingAndTravelTimeView!
    private var showGeneralMessageView: GeneralMessageNotificationView!  

    init(navigationMapView: MapboxNavigation.NavigationMapView) {
        SwiftyBeaver.debug("OBUDisplayMobileMapUpdatable init")
        self.navigationMapView = navigationMapView
        self.navigationMapView?.mapView.ornaments.options.attributionButton.visibility = .hidden
        setupUI()
    }
    
    private func obuDisplayStarted() {
        updateCameraLayer(show: true)
    }
    
    private func obuDisplayStopped() {
        updateCameraLayer(show: false)
    }
    
    func updateParkingAvailability(carpark: Carpark?) {
        
    }
    
    func updateNotificationWhenUserSoonArrive(carpark: Carpark?, message: String) {
        
    }
    
    func updateNotification(featureId: String?, currentFeature: FeatureDetectable?) {
        if let feature = currentFeature {
            if notificationView != nil {
                notificationView.dismiss()
                notificationView = nil
            }
            notificationView = BreezeNotificationView(leading: 22, trailing: 26)
            if let navMapView = self.navigationMapView {
                FeatureNotification.showNotification(notificationView: notificationView, inView: navMapView, feature: feature, yOffset: topPadding,onCompletion: { [weak self] in
                    guard let self = self else { return }
                },  onDismiss: { [weak self] in
                    guard let self = self else { return }
                    self.notificationView = nil
                })
            }
        }
        
    }
    
    func updateTunnel(isInTunnel: Bool) {
        
    }
    
    func isParkingNearByEnable() -> Bool {
        return self.parkingButton.isSelected
    }
    
    func toggleOffCarparkNearby() {
        if self.parkingButton.isSelected {
            self.parkingButton.select(false)
        }
    }
    
    func toggleOffAmenityOnMap() {
        if self.parkingButton.isSelected {
            self.parkingButton.select(false)
        }
        
        if self.evButton.isSelected {
            self.evButton.select(false)
        }
        
        if self.petrolButton.isSelected {
            self.petrolButton.select(false)
        }
    }
    
    func setIsFromDisplay(_ isFromObuDisplay: Bool, isShowingMap: Bool) {
        speedLimitView?.isFromObuDisplay = isFromObuDisplay
        speedLimitView?.isShowingMapViewFromObu = isShowingMap
    }
    
    private func calculateFurthestDistance(coordinates:[CLLocationCoordinate2D],defaultFurthestDistance:Double,nearestCoordinate:CLLocationCoordinate2D) -> Double {
        
        var finalFurthestDistance = defaultFurthestDistance
        for coordinate in coordinates {
            
            let toLocation = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
            
            let fromLocation = CLLocation(latitude: nearestCoordinate.latitude, longitude: nearestCoordinate.longitude)
            
            let distance = fromLocation.distance(from: toLocation)
            
            if(distance > finalFurthestDistance) {
                
                finalFurthestDistance = distance
            }
             
        }
        
        return finalFurthestDistance
    }
    
    func adjustDynamicZoomLevel(_ coordinates: [CLLocationCoordinate2D], currentLocation: CLLocationCoordinate2D?) {
        if let location = currentLocation {
            
            var allCoordinates: [CLLocationCoordinate2D] = []
            allCoordinates.append(contentsOf: coordinates)
            allCoordinates.append(location)
            
            let finalFarthestDistance = self.calculateFurthestDistance(coordinates: allCoordinates, defaultFurthestDistance: 0.0, nearestCoordinate: location)
            
            let padding = UIEdgeInsets(top: UIApplication.topSafeAreaHeight, left: 24, bottom: 24, right: 24)
            
            self.navigationMapView?.mapView.setDynamicZoomLevelBasedonRadius(coordinate1: coordinates[0], dynamicRadius: Int(finalFarthestDistance), padding: padding, coordinate2: location)
        }
    }
    
    func isShowingAmenity() -> Bool {
        return (parkingButton.isSelected || petrolButton.isSelected || evButton.isSelected)
    }
    
    func showCarparkNearbyWhileStopDriving() {
        if !parkingButton.isSelected {
            parkingButton.select(true)
        }
    }
    
    func cruiseStatusUpdate(isCruise: Bool) {
        setCamera(at: navigationMapView?.mapView.location.latestLocation?.coordinate, isCruise: isCruise)
        if isCruise {
            obuDisplayStarted()
        } else {
            obuDisplayStopped()
        }
    }
    
    func updateETA(eta: TripCruiseETA?) {
        /*
        if let tripETA = eta {
            
            if tripETA.shareLiveLocation.uppercased() == "Y" {
                self.liveLocationButton.isHidden = false
                self.liveLocationButton.resume()
            } else {
                self.liveLocationButton.pause()
                self.liveLocationButton.isHidden = true
            }

        } else {
            self.liveLocationButton.pause()
            self.liveLocationButton.isHidden = true
        }
        
        self.shareDriveButton.isHidden = !self.liveLocationButton.isHidden
        */
    }
    
    func updateETA(eta: TripETA?) {
        //Not needed
    }
    
    func updateLiveLocationEnabled(_ isEnabled: Bool) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            let offset:CGFloat = self.getToastOffset(isTunnel: false)
            
            if let mapView = self.navigationMapView?.mapView {
                if isEnabled {
                    showToast(from: mapView, message: Constants.toastLiveLocationResumed, topOffset: offset, icon: "toastLiveLocationResumed", messageColor: .black, duration: 3)
                } else {
                    showToast(from: mapView, message: Constants.toastLiveLocationPaused, topOffset: offset, icon: "toastLiveLocationPaused", messageColor: .black, duration: 3)
                }
            }
            
            self.liveLocationButton.updateToggleWithOutCallBack(status: isEnabled)
            
//            if(appDelegate().carPlayMapController != nil)
//            {
//                appDelegate().carPlayMapController?.updateCPCruiseToggleTitle(activity: .navigating,isEnabled:isEnabled,isLiveLoc: true)
//            }
        }
    }
    
    func shareDriveButtonEnabled(_ isEnabled: Bool) {
        /*
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.shareDriveButton.isHidden = !isEnabled
        }
         */
    }
    
    func showETAMessage(recipient: String, message: String) {
        
    }
    
    func initERPUpdater(response: MapboxDirections.RouteResponse?) {
        
    }
    
    func updateRoadName(_ name: String) {
        self.showRoad(name)
    }
    
    func updateSpeedLimit(_ value: Double) {
        speedLimitView?.speed = value
    }
    
    func overSpeed(value: Bool) {
        speedLimitView?.updateOverSpeed(value: value)
    }
    
    func updateMuteStatus() {
        self.muteButton?.select(!Prefs.shared.getBoolValue(.enableAudio))
    }
    
    func updateParkingStatus(isEnabled:Bool){
        self.parkingButton?.toggleWithOutCallBack(status: !isEnabled)
    }
    
    // MARK: - Private Methods
    private func setupUI() {
        setupNotification()
        setupSpeedLimitView()
        setupSearchButton()
        setupMuteButton()
        
        setupCloseButton()
        setupContainerView()
        setupParkingButton()
        setupPetrolButton()
        setupEVButton()
        speedLimitView?.setNeedsLayout()
        speedLimitView?.layoutIfNeeded()
        
//        setupShareDriveButton()
//        setupLiveLocationButton()
//        setupShowEndButton()
        
        setupRoadNamePanel()
    }
    
    
    private func setupNotification() {
        showTrafficInformationView = TrafficNotificationView(leading: 24, trailing: 24)
        obuCommonNotificationView = ObuCommonNotificationView(leading: 24, trailing: 24)
        showCarParkUpdateView = ParkingAndTravelTimeView(leading: 24, trailing: 24)
        showGeneralMessageView = GeneralMessageNotificationView(leading: 24, trailing: 24)
        notificationView = BreezeNotificationView(leading: 24, trailing: 24)
    }
    
    // MARK: - Close button
    private func setupCloseButton() {
        if self.closeButton == nil {
            let button = UIButton(frame: CGRect(x: 0, y: 0, width: 72, height: 38))
            button.addTarget(self, action: #selector(onEndClicked), for: .touchUpInside)
            button.setTitle("Close", for: .normal)
            button.setTitleColor(UIColor(hexString: "#E82370", alpha: 1.0), for: .normal)
            button.titleLabel?.font = UIFont(name: fontFamilySFPro.MaxBold, size: 18.0)!
            button.backgroundColor = .white
            button.roundedBorder(cornerRadius: 19, borderColor: UIColor(hexString: "#E2E2E2", alpha: 1.0).cgColor, borderWidth: 1.0)
            navigationMapView?.mapView.addSubview(button)
            self.closeButton = button
        }
        
        // setup auto-layout
        self.closeButton.translatesAutoresizingMaskIntoConstraints = false
        if let speedView = self.speedLimitView {
            self.closeButton?.snp.makeConstraints { make in
                make.trailing.equalToSuperview().offset(-24)
                make.centerY.equalTo(speedView.snp.centerY).offset(-16)
                make.size.equalTo(CGSize(width: 72, height: 38))
            }
        }
        self.closeButton.isHidden = false
    }
    
    // MARK: - Speed limit view
    private func setupSpeedLimitView() {
        if speedLimitView == nil {
            speedLimitView = CustomSpeedLimitView(frame: CGRect(x: 0, y: 0, width: 70, height: 70))
            
            navigationMapView?.mapView.addSubview(speedLimitView!)
            speedLimitView?.speed = 0

            speedLimitView?.translatesAutoresizingMaskIntoConstraints = false
            
            if let navMapView = self.navigationMapView {
                speedLimitView?.snp.makeConstraints { (make) in
                    make.leading.equalToSuperview().offset(24)
                    make.top.equalTo(navMapView.snp.top).offset(topPadding)
                    make.size.equalTo(CGSize(width: 70, height: 70))
                }
                speedLimitView?.setNeedsLayout()
                speedLimitView?.layoutIfNeeded()
            }
        }
    }
    
    // MARK: - ContainerView
    private func setupContainerView() {
        if containerView == nil {
            containerView = UIView(frame: CGRect(x: 0, y: 0, width: 58, height: 184))
            containerView.backgroundColor = UIColor(hexString: "#FCFCFC", alpha: 0.75)
            containerView.roundedBorder(cornerRadius: 29, borderColor: UIColor(hexString: "#E2E2E2", alpha: 1.0).cgColor, borderWidth: 1.0)
            navigationMapView?.mapView.addSubview(containerView!)
            containerView?.translatesAutoresizingMaskIntoConstraints = false
            
            if let closeBtn = self.closeButton {
                containerView?.snp.makeConstraints { (make) in
                    make.trailing.equalToSuperview().offset(-24)
                    make.top.equalTo(closeBtn.snp.bottom).offset(24)
                    make.size.equalTo(CGSize(width: 58, height: 187))
                }
            }
        }
    }
    
    
    // MARK: - Mute Parking view
    private func setupParkingButton() {
        
        if self.parkingButton == nil {
            let parkingButton = ToggleButton(frame: CGRect(x: 0, y: 0, width: iconSize, height: iconSize))
            parkingButton.onImage = UIImage(named: "obu_lite_parking_inactive")
            parkingButton.offImage = UIImage(named: "obu_lite_parking_active")
            parkingButton.addTarget(self, action: #selector(onParkingButtonClicked), for: .touchUpInside)
            parkingButton.onToggled = { [weak self] isSelected in
                guard let self = self else { return }
                self.delegate?.onParkingEnabled(isSelected)
                if isSelected {
                    //  Need to remove other amenity
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { [weak self] in
                        guard let self = self else { return }
                        if self.evButton.isSelected {
                            self.evButton.select(false)
                        }
                        if self.petrolButton.isSelected {
                            self.petrolButton.select(false)
                        }
                    }
                }
                
                let event = isSelected ? ParameterName.ObuDisplayMode.UserToggle.p_button_on : ParameterName.ObuDisplayMode.UserToggle.p_button_off
                
                AnalyticsManager.shared.logToggleEvent(eventValue: event, screenName: ParameterName.ObuDisplayMode.screen_view)
            }
            
            self.containerView.addSubview(parkingButton)
            self.parkingButton = parkingButton
            
            self.parkingButton?.translatesAutoresizingMaskIntoConstraints = false
           
            self.parkingButton?.snp.makeConstraints { make in
                make.top.leading.trailing.equalToSuperview().offset(5)
                make.trailing.equalToSuperview().offset(-5)
                make.size.equalTo(CGSize(width: iconSize, height: iconSize))
            }
        }
    }
    
    private func setupPetrolButton() {
        
        if self.petrolButton == nil {
            let petrolBtn = ToggleButton(frame: CGRect(x: 0, y: 0, width: iconSize, height: iconSize))
            petrolBtn.onImage = UIImage(named: "obu_lite_petrol_inactive")
            petrolBtn.offImage = UIImage(named: "obu_lite_petrol_active")
            
            petrolBtn.onToggled = { [weak self] isSelected in
                guard let self = self else { return }
                
                //  On action petrol
                self.delegate?.onPetrolAction(isSelected)
                if isSelected {
                    //  Need to remove other amenity
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { [weak self] in
                        guard let self = self else { return }
                        if self.parkingButton.isSelected {
                            self.parkingButton.select(false)
                        }
                        if self.evButton.isSelected {
                            self.evButton.select(false)
                        }
                    }
                }
                
                if isSelected {
                    AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.ObuDisplayMode.UserToggle.petrol_button_on, screenName: ParameterName.ObuDisplayMode.screen_view)
                } else {
                    AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.ObuDisplayMode.UserToggle.petrol_button_off, screenName: ParameterName.ObuDisplayMode.screen_view)
                }
            }
            
            self.containerView.addSubview(petrolBtn)
            self.petrolButton = petrolBtn
            self.petrolButton?.translatesAutoresizingMaskIntoConstraints = false
           
            self.petrolButton?.snp.makeConstraints { make in
                make.top.equalTo(self.parkingButton.snp.bottom).offset(15)
                make.leading.equalToSuperview().offset(5)
                make.trailing.equalToSuperview().offset(-5)
                make.size.equalTo(CGSize(width: iconSize, height: iconSize))
            }
        }
    }
    
    private func setupEVButton() {
        
        if self.evButton == nil {
            let evBtn = ToggleButton(frame: CGRect(x: 0, y: 0, width: iconSize, height: iconSize))
            evBtn.onImage = UIImage(named: "obu_lite_ev_inactive")
            evBtn.offImage = UIImage(named: "obu_lite_ev_active")
            
            evBtn.onToggled = { [weak self] isSelected in
                guard let self = self else { return }
                //  On EV action
                self.delegate?.onEvChargerAction(isSelected)
                
                if isSelected {
                    //  Need to remove other amenity
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { [weak self] in
                        guard let self = self else { return }
                        if self.parkingButton.isSelected {
                            self.parkingButton.select(false)
                        }
                        if self.petrolButton.isSelected {
                            self.petrolButton.select(false)
                        }
                    }
                }
                
                if isSelected {
                    AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.ObuDisplayMode.UserToggle.ev_button_on, screenName: ParameterName.ObuDisplayMode.screen_view)
                } else {
                    AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.ObuDisplayMode.UserToggle.ev_button_off, screenName: ParameterName.ObuDisplayMode.screen_view)
                }
            }
            
            self.containerView.addSubview(evBtn)
            self.evButton = evBtn
            
            self.evButton?.translatesAutoresizingMaskIntoConstraints = false
           
            self.evButton?.snp.makeConstraints { make in
                make.top.equalTo(self.petrolButton.snp.bottom).offset(15)
                make.leading.equalToSuperview().offset(5)
                make.trailing.equalToSuperview().offset(-5)
                make.size.equalTo(CGSize(width: iconSize, height: iconSize))
            }
        }
    }
    
    // MARK: - Search Button view
    private func setupSearchButton() {
        if self.searchButton == nil {
            let button = UIButton(frame: CGRect(x: 0, y: 0, width: iconSize, height: iconSize))
            button.setImage(UIImage(named: "obuDisplaySearchIcon"), for: .normal)
            button.addTarget(self, action: #selector(onSearchButtonClicked), for: .touchUpInside)
            navigationMapView?.mapView.addSubview(button)
            self.searchButton = button
        }
        
        // setup auto-layout
        self.searchButton.translatesAutoresizingMaskIntoConstraints = false
        if let navMapView = self.navigationMapView {
            self.searchButton?.snp.makeConstraints { make in
                make.trailing.equalToSuperview().offset(-24)
                make.bottom.equalTo(navMapView.safeAreaLayoutGuide.snp.bottom).offset(-26)
                make.size.equalTo(CGSize(width: iconSize, height: iconSize))
            }
        }
        self.searchButton.isHidden = false
    }
    
    // MARK: - Mute Button view
    private func setupMuteButton() {
        if self.muteButton == nil {
            let muteButton = ToggleButton(frame: CGRect(x: 0, y: 0, width: 60, height: 60))
            muteButton.isSelected = !Prefs.shared.getBoolValue(.enableAudio)
            muteButton.onImage = Images.unmuteImage
            muteButton.offImage = Images.muteImage
            self.delegate?.onMuteEnabled(muteButton.isSelected)
            
            muteButton.onToggled = { [weak self] isSelected in
                guard let self = self else { return }
                self.delegate?.onMuteEnabled(isSelected)
                
                if isSelected {
                    AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.ObuDisplayMode.UserToggle.mute_on, screenName: ParameterName.ObuDisplayMode.screen_view)
                } else {
                    AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.ObuDisplayMode.UserToggle.mute_off, screenName: ParameterName.ObuDisplayMode.screen_view)
                }
                
            }
            navigationMapView?.mapView.addSubview(muteButton)
            self.muteButton = muteButton
            self.muteButton?.translatesAutoresizingMaskIntoConstraints = false
            
            if let navMapView = self.navigationMapView {
                self.muteButton?.snp.makeConstraints { make in
                    make.leading.equalToSuperview().offset(16)
                    make.bottom.equalTo(navMapView.safeAreaLayoutGuide.snp.bottom).offset(-20)
                    make.size.equalTo(CGSize(width: 60, height: 60))
                }
            }
        }
    }
    
    
    // MARK: - Road Name view
    private func setupRoadNamePanel() {
        
        if self.roadNamePanel == nil {
            roadNamePanel = RoadNamePanel(frame: CGRect(x: 0, y: 0, width: 170, height: 40))
            navigationMapView?.addSubview(roadNamePanel!)
            navigationMapView?.bringSubviewToFront(roadNamePanel!)
                        
            let minW = UIScreen.main.bounds.width > 400 ? 220 : 190
            roadNamePanel?.translatesAutoresizingMaskIntoConstraints = false
            if let navMapView = navigationMapView {
                roadNamePanel?.snp.makeConstraints { (make) in
                    make.centerX.equalToSuperview()
                    make.bottom.equalTo(navMapView.safeAreaLayoutGuide.snp.bottom).offset(-20)
                    make.width.greaterThanOrEqualTo(170)
                    make.width.lessThanOrEqualTo(minW)
                    make.height.equalTo(40)
                }
            }
            
            updateRoadPanelConstraint(isHidden: true)
            roadNamePanel?.setNeedsLayout()
            roadNamePanel?.layoutIfNeeded()
        }
    }
    
    private func updateRoadPanelConstraint(isHidden: Bool) {
        if let navMapView = self.navigationMapView {
            roadNamePanel?.snp.updateConstraints { (make) in
                make.bottom.equalTo(navMapView.safeAreaLayoutGuide.snp.bottom).offset(isHidden ? 80 : -trailingX)
            }
        }
    }
    
    private func showRoad(_ name: String) {
                
        guard let roadNamePanel = self.roadNamePanel else { return }
        
        let hide = name.isEmpty
        let needUpdate = roadNamePanel.text() != name
        
        roadNamePanel.set(name)
        
        // animation
        if needUpdate {
            UIView.animate(withDuration: Values.standardAnimationDuration) {
                [weak self] in
                guard let self = self else { return }
                self.updateRoadPanelConstraint(isHidden: hide)
            }
        }
    }
    
    
    // MARK: - Share drive button on top of the live location button
    private func setupShareDriveButton() {
        
        if self.shareDriveButton == nil {
            let button = UIButton(frame: CGRect(x: 0, y: 0, width: 60, height: 60))
            button.setImage(UIImage(named: "shareDriveBtnIcon"), for: .normal)
            button.addTarget(self, action: #selector(onShareDriveButtonClicked), for: .touchUpInside)
            navigationMapView?.mapView.addSubview(button)
            self.shareDriveButton = button
        }
        
        // setup auto-layout
        self.shareDriveButton.translatesAutoresizingMaskIntoConstraints = false
        self.shareDriveButton.snp.makeConstraints { make in
            make.trailing.equalToSuperview().offset(-18)
            make.bottom.equalTo(self.searchButton.snp.top).offset(-8)
            make.width.equalTo(60)
            make.height.equalTo(60)
        }
        
        self.shareDriveButton.isHidden = false
    }
    
    /*
    private func setupShowEndButton() {
        if self.endButton == nil {
            let button = UIButton(frame: CGRect(x: 0, y: 0, width: 59, height: 40))
            button.addTarget(self, action: #selector(onEndClicked), for: .touchUpInside)
            button.setTitle("End", for: .normal)
            button.backgroundColor = UIColor(hexString: "#E82370", alpha: 1.0)
            button.layer.cornerRadius = 20
            navigationMapView?.mapView.addSubview(button)
            self.endButton = button
        }
        
        // setup auto-layout
        self.endButton.translatesAutoresizingMaskIntoConstraints = false
        if let navMapView = navigationMapView {
            self.endButton.snp.makeConstraints { make in
                make.trailing.equalToSuperview().offset(-trailingX)
                make.bottom.equalTo(navMapView.safeAreaLayoutGuide.snp.bottom).offset(-trailingX)
                make.width.equalTo(59)
                make.height.equalTo(40)
            }
        }
        self.endButton.isHidden = false
    }
     */
    
    @objc func onShareDriveButtonClicked() {
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.ObuDisplayMode.UserClick.share_drive, screenName: ParameterName.ObuDisplayMode.screen_view)
        self.delegate?.onShareDriveEnabled(true)
    }
    
    @objc func onEndClicked() {
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.ObuDisplayMode.UserClick.close, screenName: ParameterName.ObuDisplayMode.screen_view)
        self.delegate?.onEndAction()
    }
    
    @objc func onSearchButtonClicked() {
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.ObuDisplayMode.UserClick.search, screenName: ParameterName.ObuDisplayMode.screen_view)
        self.delegate?.onSearchAction()
    }
    
    @objc func onParkingButtonClicked() {
        self.delegate?.onParkingAction()
    }
    
    // MARK: - Live location button
    private func setupLiveLocationButton() {
    
        if self.liveLocationButton == nil {
            let etaButton = LiveLocationSharingButton(frame: CGRect(x: 0, y: 0, width: 72, height: 72))
            etaButton.onImage = Images.liveLocationResumeImage
            etaButton.offImage = Images.liveLocationPauseImage
            etaButton.setSizeToggleButton(60)
            //etaButton.backgroundColor = .white
            //etaButton.layer.cornerRadius = 31
            var isFirstTime: Bool = true
            etaButton.onToggled = { [weak self] isStarted in
                guard let self = self else { return }
            
                let name = isStarted ? ParameterName.Cruise.UserClick.eta_main_button: ParameterName.Cruise.UserClick.eta_pause
                AnalyticsManager.shared.logClickEvent(eventValue: name, screenName: ParameterName.Cruise.screen_view)
                
                // Don't show toast the first time
                if isFirstTime {
                    isFirstTime = false
                } else {
                    self.delegate?.onLiveLocationEnabled(isStarted)
                }
            }
                        
            navigationMapView?.mapView.addSubview(etaButton)
            self.liveLocationButton = etaButton

            self.liveLocationButton.translatesAutoresizingMaskIntoConstraints = false
            self.liveLocationButton.snp.makeConstraints { make in
                make.center.equalTo(self.shareDriveButton.snp.center)
                make.width.equalTo(72)
                make.height.equalTo(72)
            }
            
            //Check initially if ETA is not available, then hide the live location button
            self.liveLocationButton.isHidden = true
        }
    }
    
    
//    private func getToastOffset(isTunnel: Bool) -> CGFloat {
//        var offset: CGFloat = 0
//        if let view = notificationView {
//            offset =  view.bounds.origin.y + view.bounds.height + 15
//        }
//        else {
//            offset = UIApplication.bottomSafeAreaHeight > 0 ? 0 : 15
//        }
//        if isTunnel {
//            offset += 60
//        }
//
//        return offset
//    }
    
    
//    private func updateIconsLayoutTraffic() {
//        let top = UIApplication.topSafeAreaHeight + (showTrafficInformationView != nil ? showTrafficInformationView!.bounds.origin.y + showTrafficInformationView!.bounds.height + 16 : topPadding)
//
////        if let speedLimitIcon = self.speedLimitView {
////            if let navMapView = self.navigationMapView {
////                speedLimitIcon.snp.updateConstraints { make in
////                    make.top.equalTo(navMapView).offset(top)
////                }
////            }
////        }
////
////        if let searchBtn = self.searchButton {
////            if let navMapView = self.navigationMapView {
////                searchBtn.snp.updateConstraints { make in
////                    make.top.equalTo(navMapView).offset(top)
////                }
////            }
////        }
//    }
    
//    private func updateIconsLayoutCommon() {
//        let top = UIApplication.topSafeAreaHeight + (obuCommonNotificationView != nil ? obuCommonNotificationView!.bounds.origin.y + obuCommonNotificationView!.bounds.height + 16 : topPadding)
//
////        if let speedLimitIcon = self.speedLimitView {
////            if let navMapView = self.navigationMapView {
////                speedLimitIcon.snp.updateConstraints { make in
////                    make.top.equalTo(navMapView).offset(top)
////                }
////            }
////        }
////
////        if let searchBtn = self.searchButton {
////            if let navMapView = self.navigationMapView {
////                searchBtn.snp.updateConstraints { make in
////                    make.top.equalTo(navMapView).offset(top)
////                }
////            }
////        }
//    }
    
    
//    private func updateIconsLayoutCarPark() {
//        let top = UIApplication.topSafeAreaHeight + (showCarParkUpdateView != nil ? showCarParkUpdateView!.bounds.origin.y + showCarParkUpdateView!.bounds.height + 16 : topPadding)
//
////        if let speedLimitIcon = self.speedLimitView {
////            if let navMapView = self.navigationMapView {
////                speedLimitIcon.snp.updateConstraints { make in
////                    make.top.equalTo(navMapView).offset(top)
////                }
////            }
////        }
////
////        if let searchBtn = self.searchButton {
////            if let navMapView = self.navigationMapView {
////                searchBtn.snp.updateConstraints { make in
////                    make.top.equalTo(navMapView).offset(top)
////                }
////            }
////        }
//    }
    
//    private func updateIconsLayoutMesaage() {
//        let top = UIApplication.topSafeAreaHeight + (showGeneralMessageView != nil ? showGeneralMessageView!.bounds.origin.y + showGeneralMessageView!.bounds.height + 16 : topPadding)
//
////        if let speedLimitIcon = self.speedLimitView {
////            if let navMapView = self.navigationMapView {
////                speedLimitIcon.snp.updateConstraints { make in
////                    make.top.equalTo(navMapView).offset(top)
////                }
////            }
////        }
////
////        if let searchBtn = self.searchButton {
////            if let navMapView = self.navigationMapView {
////                searchBtn.snp.updateConstraints { make in
////                    make.top.equalTo(navMapView).offset(top)
////                }
////            }
////        }
//    }
    
    
//    private func updateIconsLayoutNoitification() {
//        let top = UIApplication.topSafeAreaHeight + (notificationView != nil ? notificationView!.bounds.origin.y + notificationView!.bounds.height + 16 : topPadding)
//
////        if let speedLimitIcon = self.speedLimitView {
////            if let navMapView = self.navigationMapView {
////                speedLimitIcon.snp.updateConstraints { make in
////                    make.top.equalTo(navMapView).offset(top)
////                }
////            }
////        }
////
////        if let searchBtn = self.searchButton {
////            if let navMapView = self.navigationMapView {
////                searchBtn.snp.updateConstraints { make in
////                    make.top.equalTo(navMapView).offset(top)
////                }
////            }
////        }
//    }
    
    
    func updateTrafficInformationNotificationObuLite(title: String?, subTitle: String?, backgroundColor: UIColor, icon: UIImage, dismissInSeconds:Int? = 5, needAdjustFont: Bool = false) {
        if let navMapView = self.navigationMapView {
            FeatureNotification.showTrafficStatusView(notification: showTrafficInformationView, title: title, subTitle: subTitle, backgroundColor: backgroundColor, icon: icon, inview: navMapView, yOffset: topPadding, dismissInSeconds: dismissInSeconds, needAdjustFont: needAdjustFont) { [weak self] in
                guard let self = self else { return }
            } onDismiss: { [weak self] in
                guard let self = self else { return }
                self.whenNotificationDissmiss()
            }
            
        }
    }
    
    
    func updateObuCommonNotificationObuLite(title: String, subTitle: String, price: String, backgroundColor: UIColor, icon: UIImage, iconRate: UIImage?) {
        if let navMapView = self.navigationMapView {
            self.obuCommonNotificationView.updateUI(title: title, subTitle: subTitle, icon: icon, backgroundColor: backgroundColor, rate: price, iconRate: iconRate)
            FeatureNotification.showOBUDataView(notificationView: obuCommonNotificationView, inview: navMapView, yOffset: topPadding) { [weak self] in
                guard let self = self else { return }
            } onDismiss: { [weak self] in
                guard let self = self else { return }
                self.whenNotificationDissmiss()
            }
        }
    }
    
    func updateCarParkandTravelTimeNotificationObuLite(title: String, subTitle: String, backgroundColor: UIColor, icon: UIImage) {
        if let navMapView = self.navigationMapView {
            FeatureNotification.showCarparkUpdateView(notificationView: showCarParkUpdateView, title: title, subTitle: subTitle, backgroundColor: backgroundColor, icon: icon, inView: navMapView, yOffset: topPadding) { [weak self] in
                guard let self = self else { return }
            } onDismiss: { [weak self] in
                guard let self = self else { return }
                self.whenNotificationDissmiss()
            }
            
            showCarParkUpdateView.onReadMore = { [weak self] in
                guard let self = self else { return }
                if title == "Travel Time" {
                    
                    NotificationCenter.default.post(name: .showTravelTimeList, object: nil)
                } else {
                    NotificationCenter.default.post(name: .showCarParkList, object: nil)
                }
                self.whenNotificationDissmiss()
                
            }

        }
    }
    
    func updateGeneralMessageNotificationObuLite(title: String?, subTitle: String?, backgroundColor: UIColor) {
        if let navMapView = self.navigationMapView {
            FeatureNotification.showGeneralMessageView(notification: showGeneralMessageView, title: title, subTitle: subTitle, backgroundColor: backgroundColor, inview: navMapView, yOffset: topPadding) { [weak self] in
                guard let self = self else {return}
            } onDismiss: { [weak self] in
                guard let self = self else {return}
                self.whenNotificationDissmiss()
            }
        }
    }
    
    
    
    
    func whenNotificationDissmiss() {
//        if let speedLimitIcon = self.speedLimitView {
//            if let navMapView = self.navigationMapView {
//                speedLimitIcon.snp.updateConstraints { make in
//                    make.top.equalTo(navMapView.snp.top).offset(topPadding)
//
//                }
//            }
//        }
//
//        if let searchBtn = self.searchButton {
//            if let navMapView = self.navigationMapView {
//                searchBtn.snp.updateConstraints { make in
//                    make.top.equalTo(navMapView.snp.top).offset(topPadding)
//                }
//            }
//        }
    }
    
    func showMapMenuView(_ isShowingMap: Bool) {
        self.muteButton.isHidden = !isShowingMap
//        self.shareDriveButton.isHidden = !isShowingMap
        self.speedLimitView?.isHidden = !isShowingMap
        self.parkingButton.isHidden = !isShowingMap
        self.petrolButton.isHidden = !isShowingMap
        self.evButton.isHidden = !isShowingMap
        self.searchButton.isHidden = !isShowingMap
        self.closeButton.isHidden = !isShowingMap
//        self.endButton.isHidden = !isShowingMap
    }
    
    // TODO: Consider to refactor the Toast implementation so that its position can be adjusted dynamically
    private func getToastOffset(isTunnel: Bool) -> CGFloat {
        var offset: CGFloat = UIApplication.bottomSafeAreaHeight > 0 ? 0 : 15
        if isTunnel {
            offset += 60
        }

        return offset
    }
}

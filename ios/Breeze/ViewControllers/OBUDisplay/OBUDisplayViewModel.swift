//
//  OBUDisplayViewModel.swift
//  Breeze
//
//  Created by Tuan, Pham Hai on 05/06/2023.
//

import Foundation
import OBUSDK
import Turf
import CoreLocation
import Combine
import SwiftyBeaver
import CloudKit
import MapboxMaps
import MapboxNavigation
import MapboxCoreNavigation
import MapboxDirections
import AVFAudio

protocol OBUDisplayModelDelegate: NSObjectProtocol {
    func showNoCarpark(_ model: OBUDisplayViewModel)
    func hideNoCarpark(_ model: OBUDisplayViewModel)
    func updateLimitSpeed(_ limitSpeed: Double, currentSpeed: Double)
    func showTopMenu(_ isShow: Bool)
    func showMapAnimation()
    func getBearing() -> String
    func dismissToolTip()
    func getCallout() -> ToolTipsView?
    func showToastConnectedOBU()
}

final class OBUDisplayViewModel: BreezeObuProtocol {
    
    // MARK: - Public properties
    var mobileMapViewUpdatable: OBUDisplayMobileMapUpdatable? {
        didSet {
            if mobileMapViewUpdatable != nil {
                mobileMapViewUpdatable?.delegate = self
                syncUpdate(mapViewUpdatable: mobileMapViewUpdatable!)
            }
        }
    }
    var carPlayMapViewUpdatable: OBUDisplayCarplayMapUpdatable? {
        didSet {
            if carPlayMapViewUpdatable != nil {
                carPlayMapViewUpdatable?.delegate = self
                syncUpdate(mapViewUpdatable: carPlayMapViewUpdatable!,override: false)
            }
        }
    }
    
    weak var delegate: OBUDisplayModelDelegate?
    private var limitSpeed: Double = 0.0
    private var currentSpeed: Double = 0.0
    public var userIsMoving: Bool = false
    private var showCarparkWhenSpeedIsZero: Bool = false
    
    //  MARK - EHorizontal detector
    private var tripLogger: TripLogger!
    @Published private (set) var isTrackingUser = true
    private var currentTrafficIncidentsFeatures: Turf.FeatureCollection?
    private var currentCostERPFeatures: Turf.FeatureCollection?
    private var featureCollection3km = Turf.FeatureCollection(features: [])
    private var featureDetector: FeatureDetector?
    private var currentFeature: FeatureDetectable? {
        
        willSet {
            if newValue?.id != currentFeature?.id {
                if (newValue is DetectedSchoolZone) || (newValue is DetectedSilverZone) || (newValue is DetectedSpeedCamera) || (newValue is DetectedERP) {
                    if !OBUHelper.shared.isObuConnected() {
                        self.onNewFeatureUpdated(newValue)
                    }
                } else {
                    self.onNewFeatureUpdated(newValue)
                }
            }
        }
        didSet {
            var needHandleEHorizon: Bool = false
            if (currentFeature is DetectedSchoolZone) || (currentFeature is DetectedSilverZone) || (currentFeature is DetectedSpeedCamera) || (currentFeature is DetectedERP) {
                if !OBUHelper.shared.isObuConnected() {
                    needHandleEHorizon = true
                }
            } else {
                needHandleEHorizon = true
            }
            if needHandleEHorizon {
                DispatchQueue.main.async { [weak self] in
                    guard let self = self else { return }
                    self.updateNotification(featureId: self.currentFeature == nil ? oldValue?.id : self.currentFeature!.id)
                }
            }
        }
    }
    
    //  MARK - Properties for tracking current location, speed, limit speed
    var lastLocation: CLLocation?
    private var lastCarparkLocation: CLLocation?
    private (set) var locationTrackManager: LocationTrackingManager!
    
    //  MARK - Handle muted voice instruction
    private var voiceInstructor: VoiceInstructor!
    
    //  MARK - Handle update carpark nearby
    private var carparkUpdater: CarParkUpdater?
    
    var lastTimeUpdateLocation = Date()
    var lastTimeUpdateSpeed = Date()
    private let TIME_DURATION: TimeInterval = 1.0
//    private let tripGUID = UUID().uuidString
    private var roadName: String?
    
    private var idleTime: TimeInterval = 0
    private var watchdogIdleTime: Date?
    
    var totalTravelTime: Int = -1
    var totalTravelDistance: Int = -1
    
    var isShowingMap: Bool = true   //  BREEZE2-1011 - Always show map
    
    var notifyArrivalAction: (()->Void)?
    var notifyOnSearchAction: (() -> Void)?
    var onShowInappNotification: ((String, String, String) -> Void)?
    var onEndClick: (()->Void)?
    var isOpenningSearch: Bool = false
    var isPSelected: Bool = false
    
    var amenitySymbolThemeType = Values.LIGHT_MODE_UN_SELECTED
    var isSelectedAmenity: Bool = false
    var amenityTimer: Timer?
    var amenityCoordinates: [CLLocationCoordinate2D] = []
    
    var dismissCarparkList: (() -> Void)?
    
    var openCarparkList: (() -> Void)?
    var isOverSpeed: Bool = false
    
    //  Share live location
    private (set) var isLiveLocationSharingEnabled = false
    
    // MARK: - Methods
    init(etaFav: Favourites?) {
        SwiftyBeaver.debug("start OBU display")
        appDelegate().isObuLiteDisplay = true
        
        //  Setup mock server by default
        OBUHelper.shared.needObserveAutoDisconnected = true
        OBUHelper.shared.setMode(OBUHelper.shared.getObuMode(), delegate: self)

        let cashCardParams: [String: Any] = ["status": BreezeCardStatus.valid.rawValue, "balance": OBUHelper.shared.getCardBalance(), "paymentMode": OBUHelper.shared.getPaymentMode().rawValue]
        updateCashCardInfo(cashCardParams)
        
        voiceInstructor = VoiceInstructor()
        voiceInstructor.synthesizer.delegate = self
        voiceInstructor.synthesizer.managesAudioSession = false // We need to make this false in order to control the volume when navigation is not running
        
        // init PassiveLocationManager
        locationTrackManager = LocationTrackingManager()
        locationTrackManager.delegate = self
        
        // Adding below based on map box suggestions to over come GPS issues
         //https://github.com/mapbox-collab/ncs-collab/issues/159#issuecomment-1034548036
        locationTrackManager.passiveLocationDataSource.systemLocationManager.activityType = .otherNavigation

        // 1. Start Trip logger
        tripLogger = TripLogger(service: TripLogService(), isNavigation: false, isOBULite: true)

        // 2. Log the start coordinate
        let coordinate = LocationManager.shared.location.coordinate
        getAddressFromLocation(location: CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)) {[weak self] address in
            let obuStatus = OBUHelper.shared.getTripSummaryObuStatus()
            self?.tripLogger?.start(startLocation: coordinate, address1: address, address2: "", obuStatus: obuStatus)
            OBUHelper.shared.setTripGUID(self?.tripLogger?.getTripGUID() ?? "")
        }
                
        // 3. Disable the idle timer
        UIApplication.shared.isIdleTimerDisabled = true

        // 4. Update camera and custom puck
        mapUpdate()
        
        // since set customized puck will recenter
        isTrackingUser = true

        // 5. Setup feature detector
        setupFeatureDetector()
        
        // 6. Start history data recording
//        appDelegate().startCruiseRecordingHistoryData()
        
        // 7. Setup ETA if needed -> Santoso remove this feature
        if let fav = etaFav {
            ETATrigger.shared.setUpTripETA(etaFav: fav)
            setupETA()
        }
        
        //  8. Handle mock event via appsync
        if OBUHelper.shared.getObuMode() == .mockServer {
            OBUHelper.shared.startBreezeMockEvent()
        } else {
            OBUHelper.shared.startObuEvent()
        }
        
        // Speeding check initial status
        LocationManager.shared.startSpeedCheck()
        
        // MARK: - For Testing: Overspeed

//        #if STAGING || TESTING || BETARELEASE
//        // For testing, don't comment out previous test case
//        // Test case 1
//        LocationManager.shared.startSimulateOverspeed(after: 5, speed: 90)
//        // Test case 2 - overspeed, during blinking, below the speed limit
//        DispatchQueue.main.asyncAfter(deadline: .now() + 11) {
//            LocationManager.shared.startSimulateOverspeed(after: 1, speed: 0)
//        }
//        // Test case 3 - overspeed, not overspeed, overspeed again
//        DispatchQueue.main.asyncAfter(deadline: .now() + 65) {
//            LocationManager.shared.startSimulateOverspeed(after: 1, speed: 2)
//        }
//        // Test case 3 - overspeed, not overspeed, overspeed again
//        DispatchQueue.main.asyncAfter(deadline: .now() + 100) {
//            LocationManager.shared.startSimulateOverspeed(after: 1, speed: 1)
//        }
//        #endif
        
/*
        // MARK: - For Testing: Notifications
        DispatchQueue.main.asyncAfter(deadline: .now() + 10) {
            // FlashFlood
            self.currentFeature = DetectedTrafficIncident(id: "1", distance: 10, priority: FeatureDetection.cruisePriorityTraffic, category: TrafficIncidentCategories.FlashFlood, message: "Blk 35 Mandalay Road Mandalay Towers.")
//            self.currentFeature = DetectedERP(id: "2", distance: 282.3, priority: FeatureDetection.cruisePriorityErp, address: "Blk 35 Mandalay Road Mandalay Towers", price: 1.50, zoneId: "A")
//            self.currentFeature = DetectedSchoolZone(id: "3", distance: 299.0, priority: FeatureDetection.cruisePrioritySchoolZone, feature: Turf.Feature(geometry: .point(Point(CLLocationCoordinate2D(latitude: 0, longitude: 0)))))
        }
        
        // MAKRK: - For Testing: Toast
        DispatchQueue.main.asyncAfter(deadline: .now() + 12) {
            self.mobileMapViewUpdatable?.updateTunnel(isInTunnel: true)
        }
*/
    }
    
    deinit {
        notifyArrivalAction = nil
        OBUHelper.shared.stopObuMockEvent()
        OBUHelper.shared.stopBreezeMockEvent()
        OBUHelper.shared.stopObuEvent()
    }
    
    //  Update trip obu status
    func updateTripOBUStatus(_ status: TripOBUStatus) {
        self.tripLogger.updateOBUStatus(status)
    }
    
    func setMockDataMode(_ mode: ObuMode) {

    }
    
    func startLocationUpdate() {
        locationTrackManager.startUpdating()
//        recenter()
    }
    
    func stopLocationUpdate() {
        locationTrackManager.stopUpdating()
        locationTrackManager.delegate = nil
    }
    
    private func syncUpdate(mapViewUpdatable: MapViewUpdatable, override: Bool = true) {
        
        // only start zone monitor when the map is attached, otherwise the park icon will not be added
        mapViewUpdatable.setupCustomUserLocationIcon(isCruise: true)
        if override {
            mapViewUpdatable.overridePassiveLocationProvider(locationTrackManager.passiveLocationProvider)
        }
        mapViewUpdatable.cruiseStatusUpdate(isCruise: true)
        
        if let tripETA = ETATrigger.shared.tripCruiseETA {
            mapViewUpdatable.updateETA(eta: tripETA)
        }
        
        loadData()
//        #if DEBUG
//        mapViewUpdatable.navigationMapView?.mapView.addProfileZonesDynamically(zoneColors: SelectedProfiles.shared.getZoneColors(), zoneDetails: SelectedProfiles.shared.getAllZones())
//        #endif
    }
    
    // MARK: Private methods
    private func loadData() {
        DataCenter.shared.addERPUpdaterListner(listner: self)
        DataCenter.shared.addTrafficUpdaterListener(listener: self)
    }
    
    // Call by init or when added a share drive in CruiseModeVC
    func setupETA() {
            
        if let tripETA = ETATrigger.shared.tripCruiseETA {
            ETATrigger.shared.lastLiveLocationUpdateTime = Date().timeIntervalSince1970
            TripETAService().sendTripCruiseETA(tripETA) { [weak self] result in
                guard let self = self else { return }
                                
                switch result {
                case .success(let tripETAResponse):
                    DispatchQueue.main.async {
                        if Prefs.shared.getBoolValue(.enableAudio) {
                            self.voiceInstructor.speak(text: "Message sent to \(tripETA.recipientName).")
                        }
                        self.updateETAUI(eta: tripETA)
                        ETATrigger.shared.liveLocTripId = tripETAResponse.tripEtaUUID
                        self.isLiveLocationSharingEnabled = true
                        print("tripETAUUID: \(tripETAResponse.tripEtaUUID)")
                        DataCenter.shared.subscribeToMessageInbox(tripETAId: ETATrigger.shared.liveLocTripId) { [weak self] message in
                            guard let self = self, !message.isEmpty else { return }

                            self.showETAMessage(recipient: tripETA.recipientName, message: message)
                        }
                    }
                case .failure(let error):
                    DispatchQueue.main.async {
                        
                        self.hideShareDriveButton(true)
                    }
                    print(error.localizedDescription)
                }
            }
            // Update CarPlay Toggle buttons for ETA too
            if(appDelegate().carPlayMapController != nil) {
                appDelegate().carPlayMapController?.updateCPToggleButtons(activity: .navigating)
            }

        } else {
            updateETAUI(eta: nil)
            // No ETA, so should not send Reminder
            ETATrigger.shared.isReminder1Sent = true
            isLiveLocationSharingEnabled = false
        }
    }
    
    func hideShareDriveButton(_ isEnabled:Bool){
        mobileMapViewUpdatable?.shareDriveButtonEnabled(isEnabled)
    }
    
    private func updateETAUI(eta: TripCruiseETA?) {
        mobileMapViewUpdatable?.updateETA(eta: eta)
//        carPlayMapViewUpdatable?.updateETA(eta: eta)
    }

    private func showETAMessage(recipient: String, message: String) {
        mobileMapViewUpdatable?.showETAMessage(recipient: recipient, message: message)
//        carPlayMapViewUpdatable?.showETAMessage(recipient: recipient, message: message)
    }
    
    private func onNewFeatureUpdated(_ feature: FeatureDetectable?) {
        if let feature = feature {
            SwiftyBeaver.debug("onNewFeatureUpdated: \(feature.id) \(feature.type())")
            //  Should ignore RoadClosure when OBU connected
            if OBUHelper.shared.isObuConnected() {
                if feature.type() == TrafficIncidentCategories.RoadClosure {
                    return
                }
            }
            
            // only speak when there is new feature
            //  Remove voice instruction from eHorizontal detector
//            if !OBUHelper.shared.isObuConnected() {
                
                if feature is DetectedSeasonParking {
                    // voice alert
                    if Prefs.shared.getBoolValue(.enableAudio) {
                        self.voiceInstructor.speak(text: Constants.seasonParkingVoice)
                    }
                    return
                } else {
                    self.voiceInstruction(for: feature)
                }
//            }
            
            if let feature = feature as? DetectedSchoolZone {
                
                DispatchQueue.main.async { [weak self] in
                    
                    guard let self = self else { return }
                    
                    self.mobileMapViewUpdatable?.addSchoolZoneSymbol(feature)
//                    self.carPlayMapViewUpdatable?.addSchoolZoneSymbol(feature)
                }
                
            } else if let feature = feature as? DetectedSilverZone {
                DispatchQueue.main.async { [weak self] in
                    
                    guard let self = self else { return }
                    
                    self.mobileMapViewUpdatable?.addSilverZoneSymbol(feature)
//                    self.carPlayMapViewUpdatable?.addSilverZoneSymbol(feature)
                }
                
            }
        }
    }
    
    func voiceInstruction(for feature: FeatureDetectable) {
        if Prefs.shared.getBoolValue(.enableAudio) && !isOpenningSearch {
            SwiftyBeaver.debug("OBUDisplay Voice Instructor speak \(feature)")
            voiceInstructor.speak(feature)
        }
    }
    
    private func updateNotification(featureId: String?) {
        mobileMapViewUpdatable?.updateNotification(featureId: featureId, currentFeature: currentFeature)
//        carPlayMapViewUpdatable?.updateNotification(featureId: featureId, currentFeature: currentFeature)
    }
    
    private func setupFeatureDetector() {
        featureDetector = FeatureDetector(roadObjectsStore: locationTrackManager.passiveLocationDataSource.roadObjectStore, roadObjectMatcher: locationTrackManager.passiveLocationDataSource.roadObjectMatcher, roadGraph: locationTrackManager.roadGraph)
        currentFeature = nil
        featureDetector?.delegate = self
        featureDetector?.addERPObjectsToRoadMatcher()
        featureDetector?.addSchoolObjectsToRoadMatcher()
        featureDetector?.addSilverZoneObjectsToRoadMatcher()
        featureDetector?.addTrafficIncidentObjectsToRoadMatch()
        featureDetector?.addSpeedCameraObjectsToRoadMatcher()
        featureDetector?.addSeasonParkingsObjectsToRoadMatch()
        locationTrackManager.startUpdatingEHorizon() //Explicitly start eHorizon
    }

    // this will only be called when cruise start
    private func mapUpdate() {
        
        // Update camera
        mobileMapViewUpdatable?.cruiseStatusUpdate(isCruise: true)
//        carPlayMapViewUpdatable?.cruiseStatusUpdate(isCruise: true)
        // Update puck
        mobileMapViewUpdatable?.setupCustomUserLocationIcon(isCruise: true)
//        carPlayMapViewUpdatable?.setupCustomUserLocationIcon(isCruise: true)
        
        //Update close Notification Alert boolean value and make cpDisplayFeature to nil
//        carPlayMapViewUpdatable?.cpFeatureDisplayClose = false
//        carPlayMapViewUpdatable?.cpFeatureDisplay = nil
        
//        if let templates = appDelegate().carPlayManager.interfaceController?.templates{
//            if(templates.count > 1){
//                if #available(iOS 14, *) {
//
//                    appDelegate().carPlayManager.interfaceController?.popToRootTemplate(animated: true, completion: { value, error in
//                        if(!value){
//                            SwiftyBeaver.debug("Cruise map update Pop to root template failed", "\(value)","\(String(describing: error))")
//                        }
//
//                    })
//                }
//                else{
//                    appDelegate().carPlayManager.interfaceController?.popToRootTemplate(animated: true)
//                }
//
//            }
//        }
    }
    
    func queryAmenitiesLayer(point:CGPoint) {
        carparkUpdater?.queryAmenitiesLayer(point: point) { [weak self] detected in
            guard let self = self else { return }
            if detected {
                self.noTracking()
            }
        }
    }
    
    func clearAudioQueue(){
        
        if let synthesizer = voiceInstructor.synthesizer as? MultiplexedSpeechSynthesizer {
            synthesizer.speechSynthesizers.forEach {
                if let audioQueable = $0 as? AudioQueuable {
                    audioQueable.clearAudioQueue()
                }
            }
        }
    }
    
    func stopSpeakingAudioInstructions(){
        
        voiceInstructor.synthesizer.stopSpeaking()
    }
    
    func endObuDisplay(){
        
        if isOverSpeed {
            appDelegate().cruiseEndDate = Date()
        }
        
        _ = AVAudioSession.sharedInstance().setAudioSessionActive()
        clearAudioQueue()
        stopSpeakingAudioInstructions()
        
        DataCenter.shared.removeERPUpdaterListner(listner: self)
        DataCenter.shared.removeTrafficUpdaterListener(listener: self)
        
        OBUHelper.shared.needObserveAutoDisconnected = false
        
        //  Dismiss template in Carplay
        appDelegate().isObuLiteDisplay = false
        
        UIApplication.shared.isIdleTimerDisabled = false
        OBUHelper.shared.setTripGUID("")
        
        // 3. Log
        let coordinate = LocationManager.shared.location.coordinate
        getAddressFromLocation(location: CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)) {[weak self] address in
            //  cruise mode is always stopped by user
            self?.tripLogger?.stop(endLocation: coordinate, address1: address, address2: "", stoppedByUser: true, amenityType: "", amenityId: "", layerCode: "", plannedDestAddress1: "", plannedDestAddress2: "", plannedDestLat: 0.0, plannedDestLong: 0.0, idleTime: self?.idleTime ?? 0, bookmarkId: "", bookmarkName: "")
            self?.tripLogger = nil
        }
        
        if ETATrigger.shared.tripCruiseETA != nil {
            // In a scenario, force wrapping the lastLocation may cause crash. For example, after starting cruise
            // mode, there is no location update from PassiveLocationManager for some reason, then user end the
            // the cruise mode
            ETATrigger.shared.sendLiveLocationCoordinates(latitude: (self.lastLocation?.coordinate.latitude) ?? 0, longitude: (self.lastLocation?.coordinate.longitude) ?? 0, status: .cancel)

            ETATrigger.shared.resetETA() //Clearing the old values
        }
        
        locationTrackManager.stopUpdatingEHorizon()
        self.stopLocationUpdate()
        
        // 7. Stop history data recording
//        appDelegate().cruiseSubmitHistoryData(description: "OBUMode-End")
        
//        if(appDelegate().carPlayMapController != nil){
//
//            appDelegate().carPlayMapController?.updateCPToggleButtons(activity: .browsing)
//        }
        
//        carPlayMapViewUpdatable?.cruiseStatusUpdate(isCruise: false)
    }
    
    func recenter() {
        isTrackingUser = true
        self.mobileMapViewUpdatable?.navigationMapView?.navigationCamera.follow()
        mobileMapViewUpdatable?.resetCamera(isCruise: true)
//        carPlayMapViewUpdatable?.resetCamera(isCruise: true)
    }
    
    func noTracking() {
        isTrackingUser = false
    }
    
    func dismissTooltips() {
        carparkUpdater?.didDeselectCarparkSymbol()
        carparkUpdater?.calloutView = nil
    }
    
    func updateTooltipsPosition(naviMapView: NavigationMapView) {
        
        if let calloutView = carparkUpdater?.calloutView {
            calloutView.adjust(to: calloutView.tooltipCoordinate, in: naviMapView)
            
            //  Check if callout is carpark tooltip or not to send analytics
            if let carpark = calloutView.getCarpark() {
                let extraData: NSDictionary = ["message": carpark.name ?? "", "location_name": "location_\(carpark.name ?? "")", "longitude": carpark.long ?? 0.0, "latitude": carpark.lat ?? 0.0]
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.ObuDisplayMode.UserClick.tap_carpark, screenName: ParameterName.ObuDisplayMode.screen_view, extraData: extraData)
            }
        } else if let calloutView = delegate?.getCallout() {
            calloutView.adjust(to: calloutView.tooltipCoordinate, in: naviMapView)
        }
    }
    
    private func obuDisplayTrackLocation(_ location: CLLocation) {
        
        var data = ""
        let tripId = tripLogger?.getTripGUID() ?? ""
        if let name = self.roadName, !name.isEmpty {
            data = "{\"roadName\" : \"\(self.roadName ?? "")\", \"tripGUID\":\"\(tripId)\"}"
        } else {
            data = "{\"tripGUID\":\"\(tripId)\"}"
        }
        
        var bearing = ""
        let userId = AWSAuth.sharedInstance.cognitoId
        
        if let mobileUpdatable = mobileMapViewUpdatable {
            bearing = mobileUpdatable.navigationMapView?.mapView.cameraState.bearing.toString() ?? ""
        } else {
            bearing = carPlayMapViewUpdatable?.navigationMapView?.mapView.cameraState.bearing.toString() ?? ""
        }
        if OBUHelper.shared.getObuMode() == .mockServer {
            DataCenter.shared.startUpdatingDeviceUserLocation(userId: userId, bearing: bearing, latitude: location.coordinate.latitude.toString(), longitude: location.coordinate.longitude.toString(), data: data)
        } else {
            DataCenter.shared.startUpdatingOBUDeviceUserLocation(userId: userId, bearing: bearing, latitude: location.coordinate.latitude.toString(), longitude: location.coordinate.longitude.toString(), data: data)
        }
    }
    
    private func calculateIdleTime() {
        if currentSpeed < OBUHelper.shared.idleSpeed {
            if let lastTime = watchdogIdleTime {
                let duration = Date().timeIntervalSince(lastTime)
                if duration > 0 {
                    idleTime += duration
                    print("Idle time: \(idleTime)")
                }
            }
            watchdogIdleTime = Date()
        } else {
            watchdogIdleTime = nil
        }
    }
    
    private func handleShowParkingNearbyIfNeeded() {
        
        SwiftyBeaver.debug("speed: \(currentSpeed) km/h")
        
        if currentSpeed < OBUHelper.shared.idleSpeedZero {
            //  When user change state from moving to stopping then show carpark nearby
            if userIsMoving {
                
                if mobileMapViewUpdatable?.isShowingAmenity() == true {
                    //  Do nothing
                    SwiftyBeaver.debug("handleShowParkingNearbyIfNeeded stop moving but showing amenity, current speed: \(currentSpeed) km/h")
                } else {
                    //  Otherwise will hit API to show carpark nearby
                    SwiftyBeaver.debug("handleShowParkingNearbyIfNeeded show carpark nearby: \(currentSpeed) km/h")
                    showCarparkWhenSpeedIsZero = true
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: { [weak self] in
                        self?.mobileMapViewUpdatable?.showCarparkNearbyWhileStopDriving()
                    })
                    userIsMoving = false
                    dismissCarparkList?()
                }
            }
            
        } else {
            //  User is moving
            if !userIsMoving {
                //  When user change state from stop to moving immediately toggle off carpark nearby
                SwiftyBeaver.debug("handleShowParkingNearbyIfNeeded stopping to moving then toggle off: \(currentSpeed) km/h")
                DispatchQueue.main.async { [weak self] in
                    self?.mobileMapViewUpdatable?.toggleOffCarparkNearby()
                    self?.recenter()
                }
                disableToggleButtonCarparkList(true)
            }
            
            showCarparkWhenSpeedIsZero = false
            //  Then update moving state to true
            userIsMoving = true
        }
    }
    
    private func logOpenChargingInfo(_ charging: BreezeObuChargingInfo) {
        var analyticMessage = ""
        let type = charging.getType()
        
        if type == ObuEventType.ERP_CHARGING.rawValue || type == ObuEventType.ERP_FAILURE.rawValue || type == ObuEventType.ERP_SUCCESS.rawValue {
            analyticMessage = "message_erp"
        } else if type == ObuEventType.PARKING_SUCCESS.rawValue || type == ObuEventType.PARKING_FEE.rawValue || type == ObuEventType.PARKING_FAILURE.rawValue {
            analyticMessage = "message_parking"
        }
        
        //  add charge amount
        if charging.chargingAmount > 0 {
            analyticMessage.append(", price_$\(String(format: "%.2f", Double(charging.chargingAmount) / 100.0))")
        }
        
        //  Add parking start/end time
        if type == ObuEventType.PARKING_SUCCESS.rawValue || type == ObuEventType.PARKING_FAILURE.rawValue {
            if let parkingString = charging.getParkingStartAndEnd(), !parkingString.isEmpty {
                analyticMessage.append(", \(parkingString)")
            }
        }
        //  Send :popup_open
        let extraData: NSDictionary = ["message": analyticMessage, "location_name": "location_\(self.roadName ?? "")", "longitude": lastLocation?.coordinate.longitude ?? 0.0, "latitude": lastLocation?.coordinate.latitude ?? 0.0]
        
        AnalyticsManager.shared.logOpenPopupEventV2(popupName: ParameterName.ObuDisplayMode.UserPopup.obu_display_notification, screenName: ParameterName.ObuDisplayMode.screen_view, extraData: extraData)
    }
    
    private func checkLowCardBalance() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 10) { [weak self] in
            // MARK: Check Low Card Balance
            if OBUHelper.shared.isLowBalance() {
                
                self?.playBeepSound()
                
                let balance = OBUHelper.shared.getCardBalance()
                var cardBalance = "$\(balance)"
                if balance.isEmpty {
                    cardBalance = "$-"
                }
                
                self?.playSpokenText(text: Constants.lowCardVoiceMeasage)
                if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                    if appDelegate.appInBackground {
                        appDelegate.showLocalNotificationCharges(title: "Low card balance - \(cardBalance)", subTitle: "Please top up your card" )
                    } else {
                        //  Send event to RN for showing low card balance
                        if self?.isShowingMap == false && self?.isOpenningSearch == false && self?.isPSelected == false {
                            let resultDic: [String: Any] = ["type": "LOW_CARD",
                                                            "message": "Low Card Balance",
                                                            "chargeAmount": cardBalance
                            ]
                            updateOBUNoti(resultDic)
                        }
                        appDelegate.showOnAppErrorNotification(title: "Low card balance - \(cardBalance)", subTitle: "Please top up your card", controller: self?.getController())

                    }
                }
                
                if self?.isShowingMap == true && self?.isOpenningSearch == false {
                    self?.mobileMapViewUpdatable?.updateObuCommonNotificationObuLite(title: "Low card balance", subTitle: "", price: cardBalance, backgroundColor: UIColor(hexString: "#E82370", alpha: 1.0), icon: UIImage(named: "Miscellaneous-Icon")!, iconRate: UIImage(named: "Miscellaneous")!)
                }
            }
        }
    }
    
    private func getController() -> OBUDisplayViewController? {
        if let vc = self.delegate as? OBUDisplayViewController {
            return vc
        }
        return nil
    }
    
    private func handleErpAndParkingNotification(_ charging: BreezeObuChargingInfo) {
        
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate, appDelegate.appInBackground {
            let type = charging.getType()
            let message = charging.getMessage()
            let price = charging.getChargeAmount()
            
            if type == ObuEventType.ERP_FAILURE.rawValue || type == ObuEventType.ERP_SUCCESS.rawValue {
                appDelegate.showLocalNotificationCharges(title: "ERP Fee - \(price)", subTitle: message)
            } else if type == ObuEventType.PARKING_FAILURE.rawValue || type == ObuEventType.PARKING_SUCCESS.rawValue {
                appDelegate.showLocalNotificationCharges(title: "Parking Fee - \(price)", subTitle: message)
            }
        }
    }
    
    
    // MARK: Update Notification On MapView OBU LITE
    func displayNotificationOnMapView(_ event: ObuMockEventModel) {
        if isShowingMap == true  && !isOpenningSearch {
            dismissCarparkList?()
            if let eventType = ObuEventType(rawValue: event.eventType) {
                let message = event.message ?? ""
                let price = event.chargeAmount ?? ""
//                let balance = event.balance ?? 0.0
//                let balanceString = "$\(String(format: "%.2f", balance / 100))"
                switch eventType {
                case .PARKING_FAILURE, .PARKING_FEE, .PARKING_SUCCESS:
                    var iconRate: UIImage?
                    if eventType == .PARKING_SUCCESS {
                        iconRate = UIImage(named: "greenTick")
                    } else if eventType == .PARKING_FAILURE {
                        iconRate = UIImage(named: "failedERPIcon")
                    }
                    mobileMapViewUpdatable?.updateObuCommonNotificationObuLite(title: message , subTitle: "", price: price, backgroundColor: ((eventType == .PARKING_FEE || eventType == .PARKING_SUCCESS) ? UIColor(hexString: "#782EB1", alpha: 1.0) : UIColor(hexString: "#E82370", alpha: 1.0)), icon: ((eventType == .PARKING_FEE || eventType == .PARKING_SUCCESS) ? UIImage(named: "parkingPurpleIcon")! : UIImage(named: "ParkingRedIcon")!), iconRate: iconRate)
                case .TRAFFIC:
                    mobileMapViewUpdatable?.updateTrafficInformationNotificationObuLite(title: event.distance ?? "", subTitle: event.message ?? "", backgroundColor: UIColor(hexString: "#E82370", alpha: 1.0),  icon: UIImage(named: "Accident-Icon")!)
                case .ERP_CHARGING, .ERP_SUCCESS, .ERP_FAILURE:
                    var iconRate: UIImage?
                    if eventType == .ERP_SUCCESS {
                        iconRate = UIImage(named: "greenTick")
                    } else if eventType == .ERP_FAILURE {
                        iconRate = UIImage(named: "failedERPIcon")
                    }
                    let price = event.chargeAmount ?? ""
                    mobileMapViewUpdatable?.updateObuCommonNotificationObuLite(title: event.distance ?? "" , subTitle: (eventType == .ERP_CHARGING ? (event.detailMessage ?? "") : (event.message ?? "")) , price: price, backgroundColor: ((eventType == .ERP_SUCCESS || eventType == .ERP_CHARGING) ? UIColor(hexString: "#005FD0", alpha: 1.0) : UIColor(hexString: "#E82370", alpha: 1.0)), icon: ((eventType == .ERP_SUCCESS || eventType == .ERP_CHARGING) ? UIImage(named: "erpIcon")! : UIImage(named: "erpRedIcon")!), iconRate: iconRate)
                    
                case .SCHOOL_ZONE:
                    mobileMapViewUpdatable?.updateTrafficInformationNotificationObuLite(title: event.distance ?? "", subTitle: event.message ?? "", backgroundColor: UIColor(hexString: "#F3712B", alpha: 1.0),  icon: UIImage(named: "schoolZoneIcon")!)
                case .SILVER_ZONE:
                    break
                case .PARKING_AVAILABILITY:
                    let trafficParking = BreezeTrafficParking(priority: "", templateId: 0, dataList: event.getParkingList())
                    let displayItem = trafficParking.getInfoToDisplayOnMobile()
                    mobileMapViewUpdatable?.updateCarParkandTravelTimeNotificationObuLite(title: displayItem.title ?? "", subTitle: displayItem.subTitle ?? "", backgroundColor: UIColor(hexString: displayItem.getBackgroundColor(), alpha: 1.0), icon: UIImage(named: displayItem.getIconName())!)
                case .TRAVEL_TIME:
                    mobileMapViewUpdatable?.updateCarParkandTravelTimeNotificationObuLite(title: "Travel Time", subTitle: "", backgroundColor: UIColor(hexString: "#E82370", alpha: 1.0), icon: UIImage(named: "TravelTimeIcon")!)
                case .CARD_ERROR, .CARD_EXPIRED, .CARD_INSUFFICIENT, .CARD_VALID:
                    
//                    mobileMapViewUpdatable?.updateObuCommonNotificationObuLite(title: message, subTitle: "", price: balanceString, backgroundColor: UIColor(hexString: "#E82370", alpha: 1.0), icon: UIImage(named: "erpRedIcon")!, iconRate: UIImage(named: "failedERPIcon")!)
                    break
                case .ROAD_CLOSURE:
                    mobileMapViewUpdatable?.updateTrafficInformationNotificationObuLite(title: event.distance ?? "", subTitle: "\(event.detailTitle ?? "") \(event.detailMessage ?? "")", backgroundColor: UIColor(hexString: "#E82370", alpha: 1.0),  icon: UIImage(named: "roadClosureIcon")!)
                case .ROAD_DIVERSION:
                    mobileMapViewUpdatable?.updateTrafficInformationNotificationObuLite(title: event.distance ?? "", subTitle: event.message ?? "", backgroundColor: UIColor(hexString: "#E82370", alpha: 1.0),  icon: UIImage(named: "roadDiversionIcon")!)
                case .GENERAL_MESSAGE:
                        self.mobileMapViewUpdatable?.updateGeneralMessageNotificationObuLite(title: event.message ?? "", subTitle: event.detailMessage ?? "", backgroundColor: UIColor(hexString: "#222638", alpha: 1.0))
                case .SPEED_CAMERA:
                    mobileMapViewUpdatable?.updateTrafficInformationNotificationObuLite(title: event.distance ?? "", subTitle: event.message ?? "", backgroundColor: UIColor(hexString: "#E82370", alpha: 1.0),  icon: UIImage(named: "speedCameraIcon")!)
                case .EVENT:
                    break
                case .FLASH_FLOOD, .HEAVY_TRAFFIC, .UNATTENDED_VEHICLE, .MISCELLANEOUS, .ROAD_WORK, .OBSTACLE, .VEHICLE_BREAKDOWN, .MAJOR_ACCIDENT, .SEASON_PARKING, .TREE_PRUNING, .YELLOW_DENGUE_ZONE, .RED_DENGUE_ZONE, .ROAD_BLOCK:
                    if let image = UIImage(named: event.getIconEventOBU()) {
                        mobileMapViewUpdatable?.updateTrafficInformationNotificationObuLite(title: event.distance ?? "", subTitle: event.message ?? "", backgroundColor: UIColor(hexString: "#E82370", alpha: 1.0),  icon: image)
                    }
                case .BUS_LANE:
                    mobileMapViewUpdatable?.updateTrafficInformationNotificationObuLite(title: event.distance ?? "", subTitle: event.detailMessage ?? "", backgroundColor: UIColor(hexString: "#F26415", alpha: 1.0),  icon: UIImage(named: "busLaneIcon")!)
                case .LOW_CARD:
                    mobileMapViewUpdatable?.updateObuCommonNotificationObuLite(title: message, subTitle: "", price: price, backgroundColor: UIColor(hexString: "#E82370", alpha: 1.0), icon: UIImage(named: "Miscellaneous-Icon")!, iconRate: UIImage(named: "Miscellaneous")!)
                }
            }
        }
    }
    
}

extension OBUDisplayViewModel: BreezeObuConnectionDelegate {
    
    func onOBUDisconnected(_ device: OBUSDK.OBU, error: NSError?) {
        //  Handle OBU disconnected
        self.tripLogger.updateOBUStatus(.notConnected)
    }
    
    func onOBUConnectionFailure(_ error: NSError) {
        //  Handle OBU connection failed
        self.tripLogger.updateOBUStatus(.notConnected)
    }
    
    func onBluetoothStateUpdated(_ state: OBUSDK.BluetoothState) {
        //  Handle bluetooth change status
    }
    
    func onOBUConnected(_ device: OBUSDK.OBU) {
        //  Connect to OBU success
        delegate?.showToastConnectedOBU()
        self.tripLogger.updateOBUStatus(.connected)
    }
}

extension OBUDisplayViewModel: BreezeObuDataDelegate {
    
    func onVelocityInformation(_ velocity: Double) {
        //  Update speed OBU return velocity in m/s
        if OBUHelper.shared.getObuMode() == .device && OBUHelper.shared.isObuConnected() {
            currentSpeed = velocity * 3.6   //  convert m/s to km/h
//            SwiftyBeaver.debug("currentSpeed: \(currentSpeed), limitSpeed: \(limitSpeed)")
            //  NO need update speed to RN
//            updateOBUSpeed(currentSpeed, limitSpeed: limitSpeed)
            calculateIdleTime()
            handleShowParkingNearbyIfNeeded()
        }
    }
    
    func onChargingInformation(_ chargingInfo: [BreezeObuChargingInfo]) {
        
        //  TODO BREEZE2-810
        //  Dont need to handle event when user is seeing search screen
        
        var name = self.roadName ?? ""
        if let charging = OBUHelper.shared.getPriorityChargingInfo(chargingInfo) {
            
            if name.isEmpty {
                name = charging.content1 ?? ""
            }
            
            let type = charging.getType()
            
            print("chargingInfo: \(charging.getNewNotiDic())")
            if isShowingMap == false && !isOpenningSearch && !isPSelected {
                updateOBUNoti(charging.getNewNotiDic())
            }
            
            if type == ObuEventType.ERP_CHARGING.rawValue {
                self.playSpokenText(text: "ERP ahead")
                //  Show map when ERP ahead
                if isShowingMap == false {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 5) { [weak self] in
                        guard let self = self else { return }
                        self.delegate?.showMapAnimation()
                    }
                }
            } else if (type == ObuEventType.ERP_SUCCESS.rawValue || type == ObuEventType.PARKING_SUCCESS.rawValue || type == ObuEventType.PARKING_FEE.rawValue || type == ObuEventType.ERP_FAILURE.rawValue || type == ObuEventType.PARKING_FAILURE.rawValue)  {
                //  Play beep sound when receive ERP charging success event
                self.playBeepSound()
            }
            
            self.logOpenChargingInfo(charging)
            
            //  Handle notification
            if (type == ObuEventType.ERP_SUCCESS.rawValue || type == ObuEventType.PARKING_SUCCESS.rawValue || type == ObuEventType.ERP_FAILURE.rawValue || type == ObuEventType.PARKING_FAILURE.rawValue) {
                let tripId = tripLogger?.getTripGUID() ?? ""
                if !tripId.isEmpty {
                    handleErpAndParkingNotification(charging)
                }
            }
            
            //  Handle show in app notification while staying in search page
            if let appDelegate = UIApplication.shared.delegate as? AppDelegate, (isOpenningSearch && !appDelegate.appInBackground)  {
                
                if type == ObuEventType.ERP_SUCCESS.rawValue || type == ObuEventType.ERP_FAILURE.rawValue {
                    if type == ObuEventType.ERP_SUCCESS.rawValue {
                        onShowInappNotification?("erp", "ERP Fee - \(charging.getChargeAmount())", charging.getMessage())
                    } else {
                        onShowInappNotification?("", "ERP Fee - \(charging.getChargeAmount())", charging.getMessage())
                    }
                    
                } else if type == ObuEventType.PARKING_SUCCESS.rawValue || type == ObuEventType.PARKING_FAILURE.rawValue {
                    if type == ObuEventType.PARKING_SUCCESS.rawValue {
                        onShowInappNotification?("parking", "Parking Fee - \(charging.getChargeAmount())", charging.getMessage())
                    } else {
                        onShowInappNotification?("", "Parking Fee - \(charging.getChargeAmount())", charging.getMessage())
                    }
                }
            }
            
            //  Deduction sucess then check low card balance after 5 seconds
            if (type == ObuEventType.ERP_SUCCESS.rawValue || type == ObuEventType.PARKING_SUCCESS.rawValue) {
                let tripId = tripLogger?.getTripGUID() ?? ""
                if !tripId.isEmpty {
                    self.checkLowCardBalance()
                }
            }
            
            if isShowingMap == true && !isOpenningSearch {
                dismissCarparkList?()
                let price = charging.getChargeAmount()
                let messageCard = charging.getMessage()
                
                if type == ObuEventType.ERP_SUCCESS.rawValue || type == ObuEventType.ERP_FAILURE.rawValue || type == ObuEventType.ERP_CHARGING.rawValue {
                    mobileMapViewUpdatable?.updateObuCommonNotificationObuLite(title: charging.getDistance() ?? "", subTitle: messageCard, price: price, backgroundColor: (type == ObuEventType.ERP_SUCCESS.rawValue ? UIColor(hexString: "#005FD0", alpha: 1.0) : UIColor(hexString: "#E82370", alpha: 1.0)), icon: (type == ObuEventType.ERP_CHARGING.rawValue ? UIImage(named: "erpIcon")! : UIImage(named: "erpRedIcon")!), iconRate: (type == ObuEventType.ERP_FAILURE.rawValue ? UIImage(named: "failedERPIcon")! : UIImage(named: "greenTick")!))
                }
                
                if type == ObuEventType.PARKING_SUCCESS.rawValue || type == ObuEventType.PARKING_FAILURE.rawValue || type == ObuEventType.PARKING_FEE.rawValue {
                    
                    var iconRate: UIImage?
                    if type == ObuEventType.PARKING_SUCCESS.rawValue {
                        iconRate = UIImage(named: "greenTick")
                    } else if type == ObuEventType.PARKING_FAILURE.rawValue {
                        iconRate = UIImage(named: "failedERPIcon")
                    }
                    
                    mobileMapViewUpdatable?.updateObuCommonNotificationObuLite(title: messageCard , subTitle: "", price: price, backgroundColor: ((type == ObuEventType.PARKING_FEE.rawValue || type == ObuEventType.PARKING_SUCCESS.rawValue) ? UIColor(hexString: "#782EB1", alpha: 1.0) : UIColor(hexString: "#E82370", alpha: 1.0)), icon: ((type == ObuEventType.PARKING_FEE.rawValue || type == ObuEventType.PARKING_SUCCESS.rawValue) ? UIImage(named: "parkingPurpleIcon")! : UIImage(named: "ParkingRedIcon")!), iconRate: iconRate)
                }
            }
            
        }
                
        let tripId = tripLogger?.getTripGUID() ?? ""
        if !tripId.isEmpty {
            
            ObuDataService().sendTripObuErp(chargingInfo, tripGUID: tripId, roadName: name, lat: lastLocation?.coordinate.latitude ?? 0, long: lastLocation?.coordinate.longitude ?? 0) { result in
                switch result {
                case .success(_):
                    print("Send charging information success")
                case .failure(let error):
                    print("Send charging information failed: \(error.localizedDescription)")
                }
            }
        }
    }
    
    func onTrafficInformation(_ trafficInfo: BreezeObuTrafficInfo) {
             
        //  TODO BREEZE2-810
        //  Dont need to handle event when user is seeing search screen
        
        var analyticMessage = "message_traffic"
        
        if let parking = trafficInfo.trafficParking {
            
            //  Get all carpark names then send event to RN
            analyticMessage = "message_parking_availabilty"
            
            var carparks: [Any] = []
            for item in parking.dataList {
                let carparkDic = ["name": item.location, "availableLots": item.lots, "color": item.color]
                carparks.append(carparkDic)
            }
            
            //  Fake data when receive events
            if OBUHelper.shared.getObuMode() == .mockSDK {
                carparks.removeAll()
                carparks.append(["name": "NCS Hub", "availableLots": 150, "color": "Green"])
                carparks.append(["name": "Resorts World", "availableLots": 0, "color": "Red"])
                carparks.append(["name": "HarbourFront Centre", "availableLots": 86, "color": "Green"])
                carparks.append(["name": "Keppel Bay", "availableLots": 101, "color": "Green"])
                carparks.append(["name": "Mount Faber Park", "availableLots": 10, "color": "Yellow"])
            }
            
            if !carparks.isEmpty {
                
                if UserSettingsModel.sharedInstance.obuParkingAvailabilityAudioEnable {
                    self.playBeepSound()
                    playSpokenText(text: parking.getSpokenText())
                }
                
                if UserSettingsModel.sharedInstance.obuParkingAvailabilityDisplayEnable {
                    dismissCarparkList?()
                    //  Send :popup_open
                    if !isOpenningSearch {
                        let extraData: NSDictionary = ["message": analyticMessage, "location_name": "location_\(self.roadName ?? "")", "longitude": lastLocation?.coordinate.longitude ?? 0.0, "latitude": lastLocation?.coordinate.latitude ?? 0.0]
                        
                        AnalyticsManager.shared.logOpenPopupEventV2(popupName: ParameterName.ObuDisplayMode.UserPopup.obu_display_notification, screenName: ParameterName.ObuDisplayMode.screen_view, extraData: extraData)
                        
                        let params = ["carparks": carparks]
                        openObuCarparkAvailability(params)
                    }
                }
            }
        } else if let travel = trafficInfo.travelTime {
            //  Get travel time info then send event to RN
            
            analyticMessage = "message_traveltime"
            
            var params: [[String: Any]] = []
            for item in travel.dataList {
                let dic: [String: Any] = ["name": item.location, "estTime": item.min, "color": item.color]
                params.append(dic)
            }
            
            //  Fake data when receive events
            if OBUHelper.shared.getObuMode() == .mockSDK {
                params.removeAll()
                params.append(["name": "Woodlands Ave 2", "estTime": "29 min", "color": "Red"])
                params.append(["name": "SLE", "estTime": "20 min", "color": "Yellow"])
                params.append(["name": "AMK AV1", "estTime": "13 min", "color": "Yellow"])
                params.append(["name": "Yishun AV2", "estTime": "12 min", "color": "Yellow"])
            }
            
            if !params.isEmpty {
                
                if UserSettingsModel.sharedInstance.obuEstimatedTravelTimeAudioEnable {
                    self.playBeepSound()
                    //  Play voice instruction
                    self.playSpokenText(text: travel.getSpokenText())
                }
                
                if UserSettingsModel.sharedInstance.obuEstimatedTravelTimeDisplayEnable {
                    dismissCarparkList?()
                    //  Send :popup_open
                    if !isOpenningSearch {
                        let extraData: NSDictionary = ["message": analyticMessage, "location_name": "location_\(self.roadName ?? "")", "longitude": lastLocation?.coordinate.longitude ?? 0.0, "latitude": lastLocation?.coordinate.latitude ?? 0.0]
                        
                        AnalyticsManager.shared.logOpenPopupEventV2(popupName: ParameterName.ObuDisplayMode.UserPopup.obu_display_notification, screenName: ParameterName.ObuDisplayMode.screen_view, extraData: extraData)
                        let data = ["data": params]
                        openObuTravelTime(data)
                    }
                }
            }
        }else {
            
            var needDisplayAlert = true
            var needVoiceAlert = true
            if let text = trafficInfo.trafficText {
                if text.icon == .some("2002") {
                    needDisplayAlert = UserSettingsModel.sharedInstance.schoolZoneDisplayAlert
                    needVoiceAlert = UserSettingsModel.sharedInstance.schoolZoneVoiceAlert
                } else if text.icon == .some("2003") {
                    needDisplayAlert = UserSettingsModel.sharedInstance.silverZoneDisplayAlert
                    needVoiceAlert = UserSettingsModel.sharedInstance.silverZoneVoiceAlert
                } else if text.icon == .some("2004") {
                    needDisplayAlert = UserSettingsModel.sharedInstance.busLaneDisplayAlert
                    needVoiceAlert = UserSettingsModel.sharedInstance.busLaneVoiceAlert
                }
            }
            
            let traficNotiDic = trafficInfo.getTrafficNotiDic(roadName ?? "")
            print("traficNotiDic: \(traficNotiDic)")
            SwiftyBeaver.debug("onTrafficInformation OBU display: \(traficNotiDic)")
            if isShowingMap == false && !isOpenningSearch && !isPSelected {
                if needDisplayAlert {
                    updateOBUNoti(traficNotiDic)
                }
            }
            
            if let message = traficNotiDic["message"] as? String {
                analyticMessage = "message_\(message)"
            }
            let distance = traficNotiDic["distance"] as? String
            let title = traficNotiDic["message"] as? String
            //  Play beep
            if let text = trafficInfo.trafficText {
                if text.icon == .some("1412") || text.icon == .some("9011") {
                    //  Show map when accident happen
                    if isShowingMap == false {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 5) { [weak self] in
                            guard let self = self else { return }
                            self.delegate?.showMapAnimation()
                        }
                    }
                }
                
                if text.icon == .some("2004") {
                    //  Play beep sound for bus lane
                    if needVoiceAlert {
                        self.playBeepSound()
                    }
                } else {
                    if needVoiceAlert {
                        let spokenText = trafficInfo.getSpokenText()
                        self.playSpokenText(text: spokenText)
                    }
                }
                
                if isShowingMap == true && !isOpenningSearch && needDisplayAlert {
                    dismissCarparkList?()
                    if text.icon == .some("1412") {
                        mobileMapViewUpdatable?.updateTrafficInformationNotificationObuLite(title: distance, subTitle: title , backgroundColor: UIColor(hexString: "#E82370", alpha: 1.0),  icon: UIImage(named: "Accident-Icon")!)
                    } else if text.icon == .some("9011") {
                        mobileMapViewUpdatable?.updateTrafficInformationNotificationObuLite(title: distance, subTitle: title, backgroundColor: UIColor(hexString: "#E82370", alpha: 1.0),  icon: UIImage(named: "speedCameraIcon")!)
                    } else if text.icon == .some("2004") {
                        mobileMapViewUpdatable?.updateTrafficInformationNotificationObuLite(title: distance, subTitle: title, backgroundColor: UIColor(hexString: "#F26415", alpha: 1.0),  icon: UIImage(named: "busLaneIcon")!)
                    } else if text.icon == .some("2002") {
                        mobileMapViewUpdatable?.updateTrafficInformationNotificationObuLite(title: distance, subTitle: title, backgroundColor: UIColor(hexString: "#F3712B", alpha: 1.0),  icon: UIImage(named: "schoolZoneIcon")!)
                    } else if text.icon == .some("1401") {
                        mobileMapViewUpdatable?.updateTrafficInformationNotificationObuLite(title: distance, subTitle: title, backgroundColor: UIColor(hexString: "#E82370", alpha: 1.0),  icon: UIImage(named: "roadClosureIcon")!)
                    } else if text.icon == .some("2003") {
                        mobileMapViewUpdatable?.updateTrafficInformationNotificationObuLite(title: distance, subTitle: title, backgroundColor: UIColor(hexString: "#F3712B", alpha: 1.0),  icon: UIImage(named: "silverZoneIcon")!)
                    } else {
                        mobileMapViewUpdatable?.updateTrafficInformationNotificationObuLite(title: distance, subTitle: title, backgroundColor: UIColor(hexString: "#F3712B", alpha: 1.0),  icon: UIImage(named: "carplay-Miscellaneous-Icon")!)
                    }
                }
                
            }
            
            //  Send :popup_open
            if !isOpenningSearch && needDisplayAlert {
                let extraData: NSDictionary = ["message": analyticMessage, "location_name": "location_\(self.roadName ?? "")", "longitude": lastLocation?.coordinate.longitude ?? 0.0, "latitude": lastLocation?.coordinate.latitude ?? 0.0]
                AnalyticsManager.shared.logOpenPopupEventV2(popupName: ParameterName.ObuDisplayMode.UserPopup.obu_display_notification, screenName: ParameterName.ObuDisplayMode.screen_view, extraData: extraData)
            }
        }
    }
    
    func onErpChargingAndTrafficInfo(trafficInfo: BreezeObuTrafficInfo?, chargingInfo: [BreezeObuChargingInfo]?) {
        //  Nothing to be done here, will handle in separated callback
    }
    
    func onPaymentHistories(_ histories: [BreezePaymentHistory]) {
        let tripId = tripLogger?.getTripGUID() ?? ""
        if !tripId.isEmpty {
            ObuDataService().sendObuTransactions(histories, tripGUID: tripId) { result in
                switch result {
                case .success(_):
                    print("Send transactions success")
                case .failure(let error):
                    print("Send transactions failed: \(error.localizedDescription)")
                }
            }
        }
    }
    
    func onTravelSummary(_ travelSummary: BreezeTravelSummary) {
        let tripId = tripLogger?.getTripGUID() ?? ""
        if !tripId.isEmpty {
            
            //  Need to handle how to reduce duplicated call API because this callback call multiple times with the same data
            if let time = travelSummary.totalTravelTime, let distance = travelSummary.totalTravelDistance, (totalTravelTime != Int(time) || totalTravelDistance != Int(distance)) {

                totalTravelTime = Int(time)
                totalTravelDistance = Int(distance)
                
                ObuDataService().sendObuTripSummary(travelSummary, tripGUID: tripId) { result in
                    switch result {
                    case .success(_):
                        print("Send travel summary success")
                    case .failure(let error):
                        print("Send travel summary failed: \(error.localizedDescription)")
                    }
                }
            }
        }
    }
    
    func onTotalTripCharged(_ totalCharged: TravelCharge) {
        //  Already covered in onTravelSummary
    }
    
    func onError(_ errorCode: NSError) {
        
    }
    
    func onCardInformation(_ status: BreezeCardStatus?, paymentMode: BreezePaymentMode?, chargingPaymentMode: BreezeChargingPaymentMode?, balance: Int) {
        
        /*
         if let cardStatus = status {
            let cashCardParams: [String: Any] = ["status": cardStatus.rawValue, "balance": String(format: "%.2f", Double(balance) / 100.0)]
            print("Card Info: \(cashCardParams)")
            updateCashCardInfo(cashCardParams)
         }
         */
    }
}

extension OBUDisplayViewModel: BreezeServerMockProtocol {
    
    private func playSpokenText(text: String?) {
        
        if Prefs.shared.getBoolValue(.enableAudio) && !isOpenningSearch {
            if let spokenText = text, !spokenText.isEmpty {
                self.voiceInstructor.speak(text: spokenText)
            }
        }
    }
    
    private func playBeepSound() {
        if !isOpenningSearch {
            appDelegate().playBeepAudio()
        }
    }
    
    func onReceiveObuEvent(_ event: ObuMockEventModel) {
        
        //  Dont need to handle event when user is seeing search screen
        if let eventType = ObuEventType(rawValue: event.eventType) {
            let notiDic = event.getNotiDic()
            
            var analyticMessage = "message_\(eventType.rawValue)"
            
            //  These two variables only apply for school zone, silver zone and bus lane event
            var needDisplayAlert = true
            var needVoiceAlert = true
            if eventType == .SCHOOL_ZONE {
                needDisplayAlert = UserSettingsModel.sharedInstance.schoolZoneDisplayAlert
                needVoiceAlert = UserSettingsModel.sharedInstance.schoolZoneVoiceAlert
            } else if eventType == .SILVER_ZONE {
                needDisplayAlert = UserSettingsModel.sharedInstance.silverZoneDisplayAlert
                needVoiceAlert = UserSettingsModel.sharedInstance.silverZoneVoiceAlert
            } else if eventType == .BUS_LANE {
                needDisplayAlert = UserSettingsModel.sharedInstance.busLaneDisplayAlert
                needVoiceAlert = UserSettingsModel.sharedInstance.busLaneVoiceAlert
            }
            
            switch eventType {
            case .PARKING_AVAILABILITY:
                dismissCarparkList?()
                if UserSettingsModel.sharedInstance.obuParkingAvailabilityAudioEnable {
                    self.playBeepSound()
                }
                if UserSettingsModel.sharedInstance.obuParkingAvailabilityDisplayEnable {
                    //  Send :popup_open
                    let extraData: NSDictionary = ["message": analyticMessage, "location_name": "location_\(self.roadName ?? "")", "longitude": lastLocation?.coordinate.longitude ?? 0.0, "latitude": lastLocation?.coordinate.latitude ?? 0.0]
                    if OBUHelper.shared.getObuMode() == .device && !isOpenningSearch {
                        AnalyticsManager.shared.logOpenPopupEventV2(popupName: ParameterName.ObuDisplayMode.UserPopup.obu_display_notification, screenName: ParameterName.ObuDisplayMode.screen_view, extraData: extraData)
                    }
                    if !isOpenningSearch && !isPSelected {
                        openObuCarparkAvailability(notiDic)
                    }
                }
            case .TRAVEL_TIME:
                dismissCarparkList?()
                if UserSettingsModel.sharedInstance.obuEstimatedTravelTimeAudioEnable {
                    self.playBeepSound()
                }
                
                if UserSettingsModel.sharedInstance.obuEstimatedTravelTimeDisplayEnable {
                    //  Send :popup_open
                    let extraData: NSDictionary = ["message": analyticMessage, "location_name": "location_\(self.roadName ?? "")", "longitude": lastLocation?.coordinate.longitude ?? 0.0, "latitude": lastLocation?.coordinate.latitude ?? 0.0]
                    if OBUHelper.shared.getObuMode() == .device && !isOpenningSearch {
                        AnalyticsManager.shared.logOpenPopupEventV2(popupName: ParameterName.ObuDisplayMode.UserPopup.obu_display_notification, screenName: ParameterName.ObuDisplayMode.screen_view, extraData: extraData)
                    }
                    if !isOpenningSearch && !isPSelected {
                        openObuTravelTime(notiDic)
                    }
                }
            case .CARD_ERROR, .CARD_EXPIRED, .CARD_INSUFFICIENT, .CARD_VALID:
                //  Need update card balance
                if OBUHelper.shared.getObuMode() != .device {
                    if let balance = event.balance, (eventType == .CARD_INSUFFICIENT || eventType == .CARD_VALID) {
                        OBUHelper.shared.CARD_BALANCE = Float(balance)
                        let cashCardParams: [String: Any] = ["status": eventType.rawValue, "balance": String(format: "%.2f", balance / 100), "paymentMode": OBUHelper.shared.getPaymentMode().rawValue]
                        updateCashCardInfo(cashCardParams)
                    }
//                    displayNotificationOnMapView(event)
                }
            case .ERP_CHARGING, .ERP_SUCCESS, .ERP_FAILURE, .PARKING_FEE, .PARKING_SUCCESS, .PARKING_FAILURE:
                
                if !OBUHelper.shared.isObuConnected() {
                    return
                }
                
                if eventType == .ERP_SUCCESS || eventType == .PARKING_SUCCESS || eventType == .PARKING_FEE {
                    self.playBeepSound()
                    
                } else if eventType == .ERP_CHARGING {
                    //  Show map when erp ahead
                    if isShowingMap == false {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 3) { [weak self] in
                            guard let self = self else { return }
                            self.delegate?.showMapAnimation()
                        }
                    }
                }
                
                
                if let appDelegate = UIApplication.shared.delegate as? AppDelegate, appDelegate.appInBackground {
                    if let price = event.chargeAmount {
                        if eventType == .ERP_SUCCESS || eventType == .ERP_FAILURE {
                            appDelegate.showLocalNotificationCharges(title: "ERP Fee - \(price)", subTitle: event.message ?? "")
                        } else if eventType == .PARKING_SUCCESS || eventType == .PARKING_FAILURE{
                            appDelegate.showLocalNotificationCharges(title: "Parking Fee - \(price)", subTitle: event.message ?? "")
                        }
                    }
                }
                
                //  Handle show in app notification while staying in search page
                if let appDelegate = UIApplication.shared.delegate as? AppDelegate, let price = event.chargeAmount, (isOpenningSearch && !appDelegate.appInBackground)  {
                    if eventType == .ERP_SUCCESS || eventType == .ERP_FAILURE {
                        if eventType == .ERP_SUCCESS {
                            onShowInappNotification?("erp", "ERP Fee - \(price)", event.message ?? "")
                        } else {
                            onShowInappNotification?("", "ERP Fee - \(price)", event.message ?? "")
                        }
                        
                    } else if eventType == .PARKING_SUCCESS || eventType == .PARKING_FAILURE {
                        if eventType == .PARKING_SUCCESS {
                            onShowInappNotification?("parking", "Parking Fee - \(price)", event.message ?? "")
                        } else {
                            onShowInappNotification?("", "Parking Fee - \(price)", event.message ?? "")
                        }
                    }
                }
                
                if eventType == .ERP_SUCCESS || eventType == .PARKING_SUCCESS {
                    checkLowCardBalance()
                }
                
                //  add charge amount
                if let amount = event.chargeAmount, !amount.isEmpty {
                    analyticMessage.append(", price_\(amount)")
                }
                if isShowingMap == false && !isOpenningSearch && !isPSelected {
                    updateOBUNoti(notiDic)
                }
                
                if !event.chargingInfo.isEmpty {
                    var infos: [BreezeObuChargingInfo] = []
                    for item in event.chargingInfo {
                        infos.append(item.getChargingInfo())
                    }
                    let tripId = tripLogger?.getTripGUID() ?? ""
                    if !tripId.isEmpty {
                        ObuDataService().sendTripObuErp(infos, tripGUID: tripId, roadName: self.roadName, lat: lastLocation?.coordinate.latitude ?? 0, long: lastLocation?.coordinate.longitude ?? 0) { result in
                            switch result {
                            case .success(_):
                                print("Send charging information success")
                            case .failure(let error):
                                print("Send charging information failed: \(error.localizedDescription)")
                            }
                        }
                    }
                }
                displayNotificationOnMapView(event)
            case .TRAFFIC:
                //  BREEZE2-1800 - Removed
//                displayNotificationOnMapView(event)
//
//                if isShowingMap == false && !isOpenningSearch && !isPSelected {
//                    //  Show map when accident happen
//                    updateOBUNoti(notiDic)
//                    DispatchQueue.main.asyncAfter(deadline: .now() + 5) { [weak self] in
//                        guard let self = self else { return }
//                        self.delegate?.showMapAnimation()
//                    }
//                }
                return
            case .ROAD_CLOSURE:
                //  BREEZE2-1800 - Removed
//                if !OBUHelper.shared.isObuConnected() {
//                    if needVoiceAlert {
//                        self.playBeepSound()
//                    }
//                    if isShowingMap == false && !isOpenningSearch && !isPSelected {
//                        if needDisplayAlert {
//                            updateOBUNoti(notiDic)
//                        }
//                    }
//                    if needDisplayAlert {
//                        displayNotificationOnMapView(event)
//                    }
//                }
                return
            case .BUS_LANE:
                if needVoiceAlert {
                    self.playBeepSound()
                }
                if isShowingMap == false && !isOpenningSearch && !isPSelected {
                    if needDisplayAlert {
                        updateOBUNoti(notiDic)
                    }
                }
                if needDisplayAlert {
                    displayNotificationOnMapView(event)
                }
            case .SPEED_CAMERA:
                //  Beep not required
                if isShowingMap == false && !isOpenningSearch && !isPSelected {
                    updateOBUNoti(notiDic)
                    //  Show speed camera ahead
                    DispatchQueue.main.asyncAfter(deadline: .now() + 5) { [weak self] in
                        guard let self = self else { return }
                        self.delegate?.showMapAnimation()
                    }
                }
                displayNotificationOnMapView(event)
            case .SCHOOL_ZONE, .SILVER_ZONE, .EVENT, .GENERAL_MESSAGE, .ROAD_DIVERSION:
                
                //  Beep not required
                if isShowingMap == false && !isOpenningSearch && !isPSelected {
                    if needDisplayAlert {
                        updateOBUNoti(notiDic)
                    }
                }
                if needDisplayAlert {
                    displayNotificationOnMapView(event)
                }
            
            case .FLASH_FLOOD, .HEAVY_TRAFFIC, .MAJOR_ACCIDENT, .SEASON_PARKING:
                if isShowingMap == false && !isOpenningSearch && !isPSelected {
                    updateOBUNoti(notiDic)
                }
                displayNotificationOnMapView(event)
            case .VEHICLE_BREAKDOWN, .UNATTENDED_VEHICLE, .ROAD_WORK, .ROAD_BLOCK, .MISCELLANEOUS, .TRAFFIC, .OBSTACLE, .TREE_PRUNING, .YELLOW_DENGUE_ZONE, .RED_DENGUE_ZONE:
                //  BREEZE2-1800 - Removed
                return
            case .LOW_CARD:
                self.playBeepSound()
                playSpokenText(text: Constants.lowCardVoiceMeasage)
                if isShowingMap == false && !isOpenningSearch && !isPSelected {
                    updateOBUNoti(notiDic)
                }
                let price = event.chargeAmount ?? ""
                if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                    if appDelegate.appInBackground {
                            appDelegate.showLocalNotificationCharges(title: "Low card balance - \(price)", subTitle: event.message ?? "Please top up your card")
                    } else {
                        if isOpenningSearch {
                            appDelegate.showOnAppErrorNotification(title: "Low card balance - \(price)", subTitle: event.message ?? "Please top up your card", controller: self.getController())
                        }
                    }
                }
                displayNotificationOnMapView(event)
            default:
                self.playBeepSound()
                if isShowingMap == false && !isOpenningSearch && !isPSelected {
                    updateOBUNoti(notiDic)
                }
                displayNotificationOnMapView(event)
            }
            
            print("Spoken text: \(event.spokenText)")
            
            if eventType == .PARKING_AVAILABILITY {
                if UserSettingsModel.sharedInstance.obuParkingAvailabilityAudioEnable {
                    playSpokenText(text: event.getParkingSpokenText())
                }
            } else if eventType == .TRAVEL_TIME {
                if UserSettingsModel.sharedInstance.obuEstimatedTravelTimeAudioEnable {
                    playSpokenText(text: event.getTravelTimeSpokenText())
                }
            } else {
                //  Send :popup_open
                let extraData: NSDictionary = ["message": analyticMessage, "location_name": "location_\(self.roadName ?? "")", "longitude": lastLocation?.coordinate.longitude ?? 0.0, "latitude": lastLocation?.coordinate.latitude ?? 0.0]
                if OBUHelper.shared.getObuMode() == .device {
                    
                    //  Specific check for erp/parking
                    if eventType == .ERP_CHARGING || eventType == .ERP_SUCCESS || eventType == .ERP_FAILURE || eventType == .PARKING_FEE || eventType == .PARKING_SUCCESS || eventType == .PARKING_FAILURE {
                        AnalyticsManager.shared.logOpenPopupEventV2(popupName: ParameterName.ObuDisplayMode.UserPopup.obu_display_notification, screenName: ParameterName.ObuDisplayMode.screen_view, extraData: extraData)
                    } else {
                        if !isOpenningSearch {
                            AnalyticsManager.shared.logOpenPopupEventV2(popupName: ParameterName.ObuDisplayMode.UserPopup.obu_display_notification, screenName: ParameterName.ObuDisplayMode.screen_view, extraData: extraData)
                        }
                    }
                }
                
                if needVoiceAlert {
                    playSpokenText(text: event.spokenText)
                }
            }
        }
        
        let tripId = tripLogger?.getTripGUID() ?? ""
        if !tripId.isEmpty {
            //  Send trip log travel summary and transaction history
            if let tripSummary = event.tripSummary {
                let summary = tripSummary.getTravelSummary()
                ObuDataService().sendObuTripSummary(summary, tripGUID: tripId) { result in
                    switch result {
                    case .success(_):
                        print("Send travel summary success")
                    case .failure(let error):
                        print("Send travel summary failed: \(error.localizedDescription)")
                    }
                }
            }
            
            if !event.payments.isEmpty {
                var histories: [BreezePaymentHistory] = []
                for history in event.payments {
                    let payment = history.getPaymentHistory()
                    histories.append(payment)
                }
                ObuDataService().sendObuTransactions(histories, tripGUID: tripId) { result in
                    switch result {
                    case .success(_):
                        print("Send transactions success")
                    case .failure(let error):
                        print("Send transactions failed: \(error.localizedDescription)")
                    }
                }
            }
        }
    }
}

// MARK: - TrafficIncidentUpdaterListener
extension OBUDisplayViewModel: TrafficIncidentUpdaterListener {
    
    func shouldAddTrafficIncident(features:Turf.FeatureCollection) {
        SwiftyBeaver.debug("\(features.features.count) traffic added:")
        
        // Remove old road objects and matcher if it's in cruise mode and add new one
        if let detector = featureDetector, let featureCollection = self.currentTrafficIncidentsFeatures {
            detector.removeTrafficIncidentObjects(featureCollection: featureCollection)
            detector.addTrafficIncidentObjectsToRoadMatch()
        }

        // For 3km road objects not from eHorizon instead using Turf Distance
        for feature in features.features {
            if case let .string(id) = feature.properties?["id"] {
                if(id.contains(TrafficIncidentCategories.FlashFlood) || id.contains(TrafficIncidentCategories.MajorAccident) || id.contains(TrafficIncidentCategories.RoadClosure) || id.contains(TrafficIncidentCategories.HeavyTraffic)){

                    self.featureCollection3km.features.append(feature)
                }
            }
        }

        currentTrafficIncidentsFeatures = features
        if let mapViewUpdatable = mobileMapViewUpdatable {
            updateTrafficIncidentsLayer(mapViewUpdatable: mapViewUpdatable)
        }
//        if let mapViewUpdatable = carPlayMapViewUpdatable {
//            updateTrafficIncidentsLayer(mapViewUpdatable: mapViewUpdatable)
//        }
    }
    
    private func updateTrafficIncidentsLayer(mapViewUpdatable: MapViewUpdatable) {
        mapViewUpdatable.removeTrafficLayer()
        if let features =  currentTrafficIncidentsFeatures {
            if features.features.count > 0 {
                mapViewUpdatable.addTrafficeLayer(features: features)
            }
        }
    }

}

extension OBUDisplayViewModel: MapViewUpdatableDelegate {
    
    func onLiveLocationEnabled(_ value: Bool) {
        //  No need
        guard isLiveLocationSharingEnabled != value else { return } // only change onece
        
        if value {
            ETATrigger.shared.updateLiveLocationStatus(status: .inprogress)
        } else {
            if let location = self.lastLocation {
                if !ETATrigger.shared.liveLocTripId.isEmpty {
                    DataCenter.shared.startUpdatingUserTripLiveLocation(tripId: ETATrigger.shared.liveLocTripId, latitude: location.coordinate.latitude.toString(), longitude: location.coordinate.longitude.toString(), status: LiveLocationStatusType.pause.rawValue,arrivalTime: "",coordinates: "")
                    ETATrigger.shared.updateLiveLocationStatus(status: .pause)
                }
            }
        }
        isLiveLocationSharingEnabled = value
        
        // show toast
        mobileMapViewUpdatable?.updateLiveLocationEnabled(value)
//        carPlayMapViewUpdatable?.updateLiveLocationEnabled(value)
    }
    
    func onMuteEnabled(_ value: Bool) {
        //  No need
        Prefs.shared.setValue(!value, forkey: .enableAudio)
    }
    
    func onParkingEnabled(_ value: Bool) {
        
        delegate?.dismissToolTip()
        
        //  No need
        if value {
            self.startTimer()
            
            lastCarparkLocation = lastLocation
            carparkUpdater =  CarParkUpdater(destName: "",
                                             carPlayNavigationMapView: carPlayMapViewUpdatable?.navigationMapView,
                                             mobileNavigationMapView: mobileMapViewUpdatable?.navigationMapView,
                                             fromScreen: .cruise)
            carparkUpdater?.delegate = self
            carparkUpdater?.shouldIgnoreDestination = true
            
            if let loc = lastLocation {
                carparkUpdater?.loadCarParks(maxCarParksCount: UserSettingsModel.sharedInstance.carparkCount, radius: UserSettingsModel.sharedInstance.carparkDistance, maxCarParkRadius: Values.carparkMonitorMaxDistanceInLanding , location: loc.coordinate, shortTermOnly: true)
            }
            
            mobileMapViewUpdatable?.navigationMapView?.navigationCamera.stop()
                        
        } else {
            
            lastCarparkLocation = nil
            removeCarparks()
            carparkUpdater = nil
            self.delegate?.hideNoCarpark(self)
            removeLayerTypeFrom(type: Values.EV_CHARGER)
            removeLayerTypeFrom(type: Values.PETROL)
        }
        
//        mobileMapViewUpdatable?.updateParkingStatus(isEnabled: value)
//        carPlayMapViewUpdatable?.updateParkingStatus(isEnabled: value)
    }
    
    private func removeCarparks() {
        guard let carparkUpdater = self.carparkUpdater else { return }
        carparkUpdater.removeCarParks()
        carparkUpdater.removeCallOut()
//        self.mobileMapViewUpdatable?.removeCarParks(carparkViewModel.carparks ?? [])
//        self.carPlayMapViewUpdatable?.removeCarParks(carparkViewModel.carparks ?? [])
    }
    
    func onShareDriveEnabled(_ value: Bool) {
        if value {
            notifyArrivalAction?()
        }
    }
    
    func onSearchAction() {
        notifyOnSearchAction?()
    }
    
    func onEndAction() {
        onEndClick?()
    }
    
    func onEvChargerAction(_ value: Bool) {
        delegate?.dismissToolTip()
        if value {
            self.startTimer()
            onAmenitySelection(Values.EV_CHARGER)
        } else {
            removeLayerTypeFrom(type: Values.EV_CHARGER)
        }
    }
    
    func onPetrolAction(_ value: Bool) {
        delegate?.dismissToolTip()
        if value {
            self.startTimer()
            onAmenitySelection(Values.PETROL)
        } else {
            removeLayerTypeFrom(type: Values.PETROL)
        }
    }
    
    func onParkingAction() {
        self.showCarparkWhenSpeedIsZero = false
    }
    
}

extension OBUDisplayViewModel: CarParkUpdaterDelegate {
    func carParkNavigateHere(carPark: Carpark?) {
        if let theCarpark = carPark {
            if isShowingMap == true {
                //  Check if callout is carpark tooltip or not to send analytics
                if let carpark = carPark {
                    let extraData: NSDictionary = ["message": carpark.name ?? "", "location_name": "location_\(carpark.name ?? "")", "longitude": carpark.long ?? 0.0, "latitude": carpark.lat ?? 0.0]
                    
                    AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.ObuDisplayMode.UserClick.tooltip_carpark_navigate_here, screenName: ParameterName.ObuDisplayMode.screen_view, extraData: extraData)
                }
            } else {
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Cruise.UserClick.navigate_here_parking, screenName: ParameterName.Cruise.screen_view)
            }
            
            let carparkInfo = ["carpark":theCarpark] as [String : Any]
            NotificationCenter.default.post(name: Notification.Name("onNavigationToCarpark"), object: nil,userInfo: carparkInfo)
        }
    }
    
    func didUpdateCarparks(carParks: [Carpark]?, location: CLLocationCoordinate2D) {
                
        self.amenityCoordinates.removeAll()
        
        if carParks?.count == 0 {
            DispatchQueue.main.async { [weak self] in
                let title = "No Available Data"
                let message = "Unable to find any carpark within 1km radius"
                
                AnalyticsManager.shared.logOpenPopupEventV2(popupName: ParameterName.ObuDisplayMode.UserPopup.no_parking_vicinity, screenName: ParameterName.ObuDisplayMode.screen_view)
                appDelegate().showOnAppErrorNotification(title: title, subTitle: message, controller: self?.getController()) {
                    //  Analytics for close action
                    AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.ObuDisplayMode.UserClick.parking_vicinity_close, screenName: ParameterName.ObuDisplayMode.screen_view)
                }
            }
        } else {
            if let carparks = carParks {
                for carpark in carparks {
                    let coordinate = CLLocationCoordinate2D(latitude: carpark.lat ?? 0.0, longitude: carpark.long ?? 0.0)
                    self.amenityCoordinates.append(coordinate)
                }
            }
            
            if let location = lastLocation {
                let currentLocation = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
                mobileMapViewUpdatable?.adjustDynamicZoomLevel(amenityCoordinates, currentLocation: currentLocation)
                
                self.noTracking()
                mobileMapViewUpdatable?.navigationMapView?.navigationCamera.stop()
            }
        }
        
        /*  Remove show toast no carpark message
        if carParks?.count == 0 {
            DispatchQueue.main.async {
                self.delegate?.showNoCarpark(self)
            }
        }
        else{
            self.delegate?.hideNoCarpark(self)
        }
        */
    }
}

extension OBUDisplayViewModel: SpeechSynthesizingDelegate {
    func speechSynthesizer(_ speechSynthesizer: SpeechSynthesizing, willSpeak instruction: SpokenInstruction) -> SpokenInstruction? {

        AVAudioSession.sharedInstance().tryDuckOtherAudio()
        SwiftyBeaver.debug("willSpeak: \(instruction.text)")
        return voiceInstructor.willSpeak(instruction, stepDistance: 0)
    }
        
    func speechSynthesizer(_ speechSynthesizer: SpeechSynthesizing, didSpeak instruction: SpokenInstruction, with error: SpeechError?) {
        
        SwiftyBeaver.debug("Cruise mode Instruction Played")
        AVAudioSession.sharedInstance().tryUnduckOtherAudio()
    }
    
    func speechSynthesizer(_ speechSynthesizer: SpeechSynthesizing, encounteredError error: SpeechError) {
        AVAudioSession.sharedInstance().tryUnduckOtherAudio()
    }
}

// MARK: - FeatureDetectorDelegate
extension OBUDisplayViewModel: FeatureDetectorDelegate {
    
    func featureDetector(_ detector: FeatureDetector, didEnter erp: DetectedERP) {
        // Do nothing here
    }
    
    func featureDetector(_ detector: FeatureDetector, didExit erp: DetectedERP) {
        tripLogger?.exitErp(ERPCost(erpId: Int(erp.id)!, name: erp.address, cost: erp.price, exitTime: Int(Date().timeIntervalSince1970), isOBU: "0"))
    }
    
    func featureDetector(_ detector: FeatureDetector, didUpdate feature: FeatureDetectable?) {
        SwiftyBeaver.debug("Feature detector did update called in CruiseViewModel \(String(describing: feature))")
        currentFeature = feature
    }
    
    func featureDetector(_ detector: FeatureDetector, didExit forId: String) {
        /*  TODO remove sync on Carplay
        // Making CarPlayPrevious displayed notification variable to nil and notification alert close button value to false
        if let carPlayUpdatable = self.carPlayMapViewUpdatable {
            
            //Checking for nil
            if let feature = carPlayUpdatable.cpFeatureDisplay {
                
                //Checking if both are equal
                if( feature == currentFeature){
                    
                    carPlayUpdatable.cpFeatureDisplay = nil
                    carPlayUpdatable.cpFeatureDisplayClose = false
                }
            }
        }*/
        
        if currentFeature?.id == forId {
            self.currentFeature = nil
        }
        
        // Remove symbol for school zone and silver zone
        mobileMapViewUpdatable?.removeSymbol(forId)
        //  TODO remove sync on carplay
//        carPlayMapViewUpdatable?.removeSymbol(forId)
    }
    
    func featureDetector(_ detector: FeatureDetector, isInTunnel: Bool) {
        SwiftyBeaver.debug("In tunnel - \(isInTunnel)")
        mobileMapViewUpdatable?.updateTunnel(isInTunnel: isInTunnel)
//        carPlayMapViewUpdatable?.updateTunnel(isInTunnel: isInTunnel)
    }
}

// MARK: - LocationTrackingManagerDelegate
extension OBUDisplayViewModel: LocationTrackingManagerDelegate {
    func didUpdate(roadName: String,speedLimit:Double, location: CLLocation?, rawLocation: CLLocation?) {
                
        self.roadName = roadName
        mobileMapViewUpdatable?.updateRoadName(roadName)
        mobileMapViewUpdatable?.updateSpeedLimit(limitSpeed)
        
        //  Save limit speed and current speed
        limitSpeed = speedLimit > 0 ? speedLimit : 0
       
        //  Update limit speed on OBU display
        let speed = (location?.speed ?? 0 * 3.6) // LocationManager.shared.getCurrentSpeed()
        currentSpeed = speed > 0 ? speed : 0
        delegate?.updateLimitSpeed(limitSpeed, currentSpeed: currentSpeed)
        
        //  Update current speed and speed limit to RN
        if Date().timeIntervalSince(lastTimeUpdateSpeed) > TIME_DURATION {
            lastTimeUpdateSpeed = Date()
            if !OBUHelper.shared.isObuConnected() {
                //  No need send speed to RN
//                updateOBUSpeed(currentSpeed, limitSpeed: limitSpeed)
            }
            
            //  Handle if user using mock OBU
            if (!OBUHelper.shared.isObuConnected() || OBUHelper.shared.getObuMode() != .device) {
                handleShowParkingNearbyIfNeeded()
            }
            
            //  Handle update idle time every second
            calculateIdleTime()
        }
        
        
//        carPlayMapViewUpdatable?.updateRoadName(roadName)
//        carPlayMapViewUpdatable?.updateSpeedLimit(speedLimit)
        
        // update road name and speed limit for mobile and carPlay
        let (overSpeed, over1min, over10mins) = LocationManager.shared.checkSpeeding(speedLimit: limitSpeed)
        if over10mins || over1min {
            SwiftyBeaver.debug("Overspeed - playAudioCue")
            appDelegate().playAudioCue()
        }
        
        mobileMapViewUpdatable?.overSpeed(value: overSpeed)
//        carPlayMapViewUpdatable?.overSpeed(value: overSpeed)
        
//        if over10mins || over1min {
//            SwiftyBeaver.debug("Overspeed - playAudioCue")
//            appDelegate().playAudioCue()
//        }
        
        // Update the trip log when in cruise mode
        if let location = location {
            
            updateLocation(location)
            tripLogger?.update(location: location.coordinate)
            
            //  Update share location
            etaCallbacks(location: location)
            
            if let featureDetector = self.featureDetector{
                
                featureDetector.notify3kmRoadObjects(featureCollection: featureCollection3km, currentLocation: location)
            }
            
            /*
            self.zoneMonitor?.update(currentLocation: location.coordinate)
             */
        }
        
        // tracking the location
        if let location = location {
            lastLocation = location
            
            if let provider = locationTrackManager.passiveLocationProvider {
                mobileMapViewUpdatable?.updateLocation(location, provider: provider)
//                carPlayMapViewUpdatable?.updateLocation(location, provider: provider)
            }
        }
    }
    
    private func etaCallbacks(location:CLLocation){
        
        if let tripETA = ETATrigger.shared.tripCruiseETA{
            
            if  isLiveLocationSharingEnabled && ETATrigger.shared.tripCruiseETA != nil && tripETA.shareLiveLocation.uppercased() == "Y" && !ETATrigger.shared.liveLocTripId.isEmpty  {
                
                ETATrigger.shared.sendLiveLocationCoordinates(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude, status: .inprogress)
            }
            
            //We don't need this ETA reminder API call in cruise. We don't remove the code. We will uncomment when needed
            /*if(LocationManager.shared.getDistanceInMeters(fromLoc: location, toLoc: CLLocation(latitude: tripETA.tripDestLat, longitude: tripETA.tripDestLong)) <= Values.etaCruiseDistance && !ETATrigger.shared.isReminder1Sent)
            {
                ETATrigger.shared.etaReminder(reminderType: TripETA.ETAType.Reminder1)
            }*/
        }
    }
    
    // This should be done by MapBox tracking. But it doesn't work now.
    private func updateLocation(_ location: CLLocation) {
        SwiftyBeaver.self.debug("update location = \(location)")
        var heading: Double = 0
        var newLocation = location
        
        if Date().timeIntervalSince(lastTimeUpdateLocation) >= TIME_DURATION {
            lastTimeUpdateLocation = Date()
            self.obuDisplayTrackLocation(newLocation)
        }
        
#if targetEnvironment(simulator)
        if lastLocation != nil {
            heading = lastLocation!.coordinate.heading(to: location.coordinate)
        }
        SwiftyBeaver.self.debug("simulation heading = \(heading)")
        newLocation = CLLocation(coordinate: location.coordinate, altitude: location.altitude, horizontalAccuracy: location.horizontalAccuracy, verticalAccuracy: location.verticalAccuracy, course: heading, speed: location.speed, timestamp: location.timestamp)
#else
        heading = newLocation.course
        SwiftyBeaver.self.debug("device heading = \(heading)")
#endif
        
        lastLocation = newLocation
        
        if let carparkLocation = lastCarparkLocation, let location = lastLocation {
            if carparkLocation.distance(from: location) > Double(Values.carparkMonitorDistanceInCruise) {
                onParkingEnabled(false)
            }
        }
        
        // update the camera
        if isTrackingUser {
            mobileMapViewUpdatable?.setCamera(at: newLocation.coordinate, heading: heading, animated: true, isCruise: true)
//            carPlayMapViewUpdatable?.setCamera(at: newLocation.coordinate, heading: heading, animated: true, isCruise: true, isCarPlay: true)
        }
    }
}

// MARK: - ERPUpdaterListener
extension OBUDisplayViewModel: ERPUpdaterListener {
    func shouldAddERPItems(noCostfeatures:Turf.FeatureCollection, costfeatures:Turf.FeatureCollection) {
        SwiftyBeaver.debug("\(costfeatures.features.count) ERPs added:\n \(costfeatures.features) cost erps, \(noCostfeatures.features.count) no-cost erps")
        
        currentCostERPFeatures = costfeatures
        if let mapViewUpdatable = mobileMapViewUpdatable {
            updateERPLayer(mapViewUpdatable: mapViewUpdatable)
        }
        if let mapViewUpdatable = carPlayMapViewUpdatable {
            updateERPLayer(mapViewUpdatable: mapViewUpdatable)
        }
    }
    
    private func updateERPLayer(mapViewUpdatable: MapViewUpdatable) {
        mapViewUpdatable.removeERPLayer()
        if let features = currentCostERPFeatures {
            if features.features.count > 0 {
                mapViewUpdatable.addERPLayer(features: features)
            }
        }
    }
}

//  MARK: - Implement for showing evcharger and petrol
extension OBUDisplayViewModel {
    
    func onAmenitySelection(_ amenity: String) {

        if let lastLocation = self.lastLocation {
            let lat = lastLocation.coordinate.latitude
            let long = lastLocation.coordinate.longitude
            let location2D =  CLLocationCoordinate2D(latitude: lat, longitude: long)
            AmenitiesSharedInstance.shared.addRequestForSelectedAmenityInCruise(location: location2D, selectedAmenity: amenity) {[weak self] success in
                DispatchQueue.main.async { [weak self] in
                    self?.updateMapWithAmenitiesForEVandPetrol(selectedID: amenity)
                }
            }
        }
    }
    
    private func showNoDataNotification(_ amenity: String) {
        let title = "No Available Data"
        var message = ""
        let radiusInKm = Int(Double(UserSettingsModel.sharedInstance.evPetrolRange) / 1000)
        if amenity == Values.EV_CHARGER {
            AnalyticsManager.shared.logOpenPopupEventV2(popupName: ParameterName.ObuDisplayMode.UserPopup.no_ev_vicinity, screenName: ParameterName.ObuDisplayMode.screen_view)
            message = "Unable to find any EV charger within \(radiusInKm)km radius"
        } else if amenity == Values.PETROL {
            AnalyticsManager.shared.logOpenPopupEventV2(popupName: ParameterName.ObuDisplayMode.UserPopup.no_petrol_vicinity, screenName: ParameterName.ObuDisplayMode.screen_view)
            message = "Unable to find any petrol station within \(radiusInKm)km radius"
        }
        if !message.isEmpty {
            
            appDelegate().showOnAppErrorNotification(title: title, subTitle: message, controller: self.getController()) {
                //  Analytics for close action
                if amenity == Values.EV_CHARGER {
                    AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.ObuDisplayMode.UserClick.ev_vicinity_close, screenName: ParameterName.ObuDisplayMode.screen_view)
                } else if amenity == Values.PETROL {
                    AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.ObuDisplayMode.UserClick.petrol_vicinity_close, screenName: ParameterName.ObuDisplayMode.screen_view)
                }
            }
        }
    }
    
    func updateMapWithAmenitiesForEVandPetrol(selectedID: String = "") {
        
        let allKeys = AmenitiesSharedInstance.shared.cruiseSymbolLayer.keys
        amenityCoordinates.removeAll()
         
        if allKeys.isEmpty {
            //  There is no data
            showNoDataNotification(selectedID)
        }
        
        var needShowNodata = true
        
        for key in allKeys {
            if key == selectedID {
                let array = AmenitiesSharedInstance.shared.cruiseSymbolLayer[key] as! [Any]
                
                var turfArray = [Turf.Feature]()
                print(array)
                for item in array {
                    
                    let itemDict = item as! [String:Any]
                    let feature = self.getAmenitiesFeatures(location: itemDict, type: key, selectedID: selectedID)
                    turfArray.append(feature)
                }
                
                let amenityFeatureCollection = FeatureCollection(features: turfArray)
                
                self.updateAmenitiesOnMap(collection: amenityFeatureCollection,type: selectedID)
                
                //  Handle show there is no data for parking/ev/petrol
                if turfArray.isEmpty {
                    needShowNodata = false
                    showNoDataNotification(selectedID)
                } else {
                    needShowNodata = false
                    if let location = lastLocation {
                        let currentLocation = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
                        mobileMapViewUpdatable?.adjustDynamicZoomLevel(amenityCoordinates, currentLocation: currentLocation)
                        
                        self.noTracking()
                        mobileMapViewUpdatable?.navigationMapView?.navigationCamera.stop()
                    }
                }
            }
        }
        
        if needShowNodata {
            showNoDataNotification(selectedID)
        }
    }
    
    
    
    private func getAmenitiesFeatures(location:[String:Any],type:String, selectedID: String = "")-> Turf.Feature {
        
        let coordinate = CLLocationCoordinate2D(latitude: location["lat"] as! Double, longitude: location["long"] as! Double)
        amenityCoordinates.append(coordinate)
        
        let name = location["name"] as! String
        let address = location["address"] as! String
        let id = location["id"] as! String
        let poiID = location["poiID"] as? String ?? ""
        let startDate = location["startDate"] as! Double
        let endDate = location["endDate"] as! Double
        let clusterID = location["clusterID"] as! Int
        let clusterColor = location["clusterColor"] as! String
        let originalType = location["originalType"] as! String
        let textAdditionalInfo = location["textAdditionalInfo"] as? String ?? ""
        let styleLightColor = location["styleLightColor"] as? String ?? ""
        let styleDarkColor = location["styleDarkColor"] as? String ?? ""
        let isBookmarked = location["isBookmarked"] as? Bool ?? false
        let bookmarkId = location["bookmarkId"] as? Int ?? -1
        var feature = Turf.Feature(geometry: .point(Point(coordinate)))
        feature.properties = [
            "id":.string(id),
            "type":.string(type),
            "name":.string(name),
            "address":.string(address),
            "startDate": .number(startDate),
            "endDate": .number(endDate),
            "isSelected": .boolean(!selectedID.isEmpty && id == selectedID),
            "clusterID":.string("\(clusterID)"),
            "clusterColor":.string(clusterColor),
            "bookmarkId": .number(Double(bookmarkId)),
            "isBookmarked": .boolean(isBookmarked),
            "poiID": .string(poiID),
            "textAdditionalInfo": .string(textAdditionalInfo),
            "styleLightColor": .string(styleLightColor),
            "styleDarkColor": .string(styleDarkColor),
            "originalType":.string(originalType)
        ]
        return feature
    }

    
    private func updateAmenitiesOnMap(collection:Turf.FeatureCollection,type:String){
        
        mobileMapViewUpdatable?.navigationMapView?.mapView.removeAmenitiesPetrolAndEVLayer(type: "\(type)")
        mobileMapViewUpdatable?.navigationMapView?.mapView.addPetrolEvToMapTurnbyTurn(features: collection, type: "\(type)", isBelowPuck: false, isSelectedAmenity: self.isSelectedAmenity)
//
//        AmenitiesSharedInstance.shared.isAmenitiesChanged = false
//        if let mapViewUpdatable = mobileMapViewUpdatable {
//            mapViewUpdatable.addAmenitiesToMap(collection, type: "\(type)")
//        }
    }
    
    private func getAmenitiesFeaturesForEV(location:[String:Any],type:String, selectedId: String = "")-> Turf.Feature {
        
        let coordinate = CLLocationCoordinate2D(latitude: location["lat"] as! Double, longitude: location["long"] as! Double)
        let name = location["name"] as! String
        let address = location["address"] as! String
        let sName = location["stationName"] as! String
        let amenityID = location["id"] as! String
        
        let multiplePlugTypes = location["plugType"] as! Array<String>
        
        var jsonArray = JSONArray()
        for type in multiplePlugTypes {

            let value = JSONValue(type)
            jsonArray.append(value)
        }
        
        let isSelected = !selectedId.isEmpty && selectedId == amenityID
        var feature = Turf.Feature(geometry: .point(Point(coordinate)))
        feature.properties = [
            "type":.string(type),
            "name":.string(name),
            "address":.string(address),
            "stationName":.string(sName),
            "image_id":.string("\(amenitySymbolThemeType)\(type)"),
            "plugTypes":.array(jsonArray),
            "amenityID":.string(amenityID),
            "isSelected": .boolean(isSelected)
        ]
        self.isSelectedAmenity = isSelected
        return feature
    }
    
//    func removeAmenitiesFromMap(){
//
//        if AmenitiesSharedInstance.shared.amenitiesSymbolLayer.count > 0 {
//            let allKeys = AmenitiesSharedInstance.shared.amenitiesSymbolLayer.keys
//
//             //self.parseAmenityImage()
//             for key in allKeys {
//
//                 if AmenitiesSharedInstance.shared.amenitiesSymbolLayer.keys.contains(key){
//
//                     if let mapViewUpdatable = mobileMapViewUpdatable {
//
//                         mapViewUpdatable.removeAmenitiesFromMap(type: "\(key)")
//                     }
//
//                 }
//             }
//        }
//    }
    
    func removeLayerTypeFrom(type:String){
        mobileMapViewUpdatable?.navigationMapView?.mapView.removeAmenitiesPetrolAndEVLayer(type: "\(type)")
    }
    
//    func setSelectedStateAmenity(amenityID: String, type: String) {
//        if type == Values.EV_CHARGER { // reset selection for amenity and traffic camera
//            updateMapWithAmenitiesForEVandPetrol(selectedID: amenityID)
//        } else if type == Values.PETROL {
//            updateMapWithAmenitiesForEVandPetrol(selectedID: amenityID)
//        }
//    }
}

//  MARK: - Remove amenity after 10 seconds
extension OBUDisplayViewModel {
    
    func startTimer() {
        self.stopTimer()
        self.amenityTimer = Timer.scheduledTimer(withTimeInterval: 10, repeats: false, block: { [weak self] _ in
            guard let self = self else { return }
            
            //  If showing carpark nearby when speed is zero then do nothing
            if self.showCarparkWhenSpeedIsZero && self.mobileMapViewUpdatable?.isParkingNearByEnable() == true {
                //  Keep showing carpark
            } else {
                //  Toggle off amenity
                self.mobileMapViewUpdatable?.toggleOffAmenityOnMap()
                if self.userIsMoving {
                    self.recenter()
                } else {
                    if self.showCarparkWhenSpeedIsZero {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: { [weak self] in
                            self?.mobileMapViewUpdatable?.showCarparkNearbyWhileStopDriving()
                        })
                    } else {
                        self.recenter()
                    }
                }
            }
        })
    }
    
    func stopTimer() {
        self.amenityTimer?.invalidate()
        self.amenityTimer = nil
    }
}



//
//  OBUDisplayViewController+Query.swift
//  Breeze
//
//  Created by Tuan, Pham Hai on 13/10/2023.
//

import Foundation
import MapboxNavigation
import MapboxCoreNavigation
import MapboxMaps

extension OBUDisplayViewController {
    func queryAmenitiesLayer(point:CGPoint){
        
        _ =  self.navigationMapView.mapView.mapboxMap.coordinate(for: point)
        
        var layerID = [String]()
        guard let amenitiesPreference = AmenitiesSharedInstance.shared.amenitiesPreference else {
            return
        }
        
        layerID.append("_\(Values.PETROL)")
        layerID.append("_\(Values.EV_CHARGER)")
        
        //  Upgrade new mapbox sdk 2.10.0 causing issue annotation is duplicated event calls so we just fake handle anotation layer in here
        if(layerID.count > 0){
            
            let queryOptions = RenderedQueryOptions(layerIds: layerID, filter: nil)
            self.navigationMapView.mapView.mapboxMap.queryRenderedFeatures(at: point, options: queryOptions) { [weak self] (result) in
                guard let self = self else { return }
                
                switch result {
                    
                case .success(let features):
                    
                    // Return the first feature at that location, then pass attributes to the alert controller.
                    if let selectedFeatureProperties = features.first?.feature.properties,
                       case let .point(point) = features.first?.feature.geometry
                    {
                        if let sourceID = features.first?.source,
                           sourceID == CarparkValues.destinationLayerIdentifier {
                            // tap on destination layer, we dont do anythings
                            return
                        }
                        
                        if case let .boolean(isCluster) = selectedFeatureProperties["cluster"] {
                            //  Do nothing
                        } else {
                            if case let .string(type) = selectedFeatureProperties["type"] {
                                if (type == Values.PETROL || type == Values.EV_CHARGER) {
                                    self.viewModel?.noTracking()
                                    self.viewModel?.startTimer()
                                    self.openAmenityToolTipView(feature: features.first!.feature,location:point.coordinates)
                                }
                            } else {
                                // no feature detected
                                self.didDeselectAmenityAnnotation()
                                self.didDeselectAnnotation()
                            }
                        }
                    } else {
                        // no feature detected
                        self.didDeselectAmenityAnnotation()
                        self.didDeselectAnnotation()
                    }
                case .failure(_):
                    break
                }
            }
        }
    }
    
    func didDeselectAnnotation() {
        if let callout = self.toolTipCalloutView {
            callout.dismiss(animated: true)
            callout.removeFromSuperview()
            self.toolTipCalloutView = nil
        }
    }
    
    func removeCalloutView() {
        if let callout = self.toolTipCalloutView {
            callout.dismiss(animated: true)
            callout.removeFromSuperview()
            self.toolTipCalloutView = nil
        }
    }
    
    func didDeselectAmenityAnnotation() {
        if let callout = self.toolTipCalloutView {
            callout.dismiss(animated: true)
            callout.removeFromSuperview()
            self.toolTipCalloutView = nil
        }
        previouslyTappedAmenityId = ""
    }
    
    func openAmenityToolTipView(feature:Turf.Feature,location:CLLocationCoordinate2D){
        didDeselectAnnotation() // this is for carpark. Consider to move to one dismiss function
        
        if let selectedFeatureProperties = feature.properties,
           case let .string(featureInformation) = selectedFeatureProperties["name"],
           case let .string(featureAddress) = selectedFeatureProperties["address"],
           case let .string(type) = selectedFeatureProperties["type"],
           case let .string(id) = selectedFeatureProperties["id"],
           case let .number(startDate) = selectedFeatureProperties["startDate"],
           case let .number(endDate) = selectedFeatureProperties["endDate"],
           case let .number(bookmarkId) = selectedFeatureProperties["bookmarkId"],
           case let .string(poiID) = selectedFeatureProperties["poiID"],
           case let .string(originalType) = selectedFeatureProperties["originalType"],
           case let .string(textAdditionalInfo) = selectedFeatureProperties["textAdditionalInfo"],
           case let .string(styleLightColor) = selectedFeatureProperties["styleLightColor"],
           case let .string(styleDarkColor) = selectedFeatureProperties["styleDarkColor"],
           case let .boolean(isBookmarked) = selectedFeatureProperties["isBookmarked"] {
                
            let extraData: NSDictionary = ["message": featureInformation, "location_name": "location_\(featureInformation)", "longitude": location.longitude, "latitude": location.latitude]
            if type == Values.EV_CHARGER {
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.ObuDisplayMode.UserClick.tap_ev, screenName: ParameterName.ObuDisplayMode.screen_view, extraData: extraData)
            } else if type == Values.PETROL {
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.ObuDisplayMode.UserClick.tap_petrol, screenName: ParameterName.ObuDisplayMode.screen_view, extraData: extraData)
            }
            
            let amenityID = id
            let shouldReturn = amenityID == self.previouslyTappedAmenityId
            self.didDeselectAnnotation()
            self.didDeselectAmenityAnnotation()
            if shouldReturn {
                return
            }
//            self.setSelectedState(amenityID: amenityID, type: type)
            
            // Reset a previously tapped to be "unselected".
            //            self.resetPreviouslySelectedStateIfNeeded(currentTappedamenityId: amenityID, type: type)
            
            // TODO: Should use generated id for seletion of symbol
            // I use it here to identify the amenity right now. Need to change for selection of symbol later.
            self.previouslyTappedAmenityId = amenityID
            
            let amenityToolTip: AmenityToolTipsView = type == "pasarmalam" ? PasarMalamToolTipsView(id: amenityID, name: featureInformation, address: featureAddress, startDate: startDate, endDate: endDate, location: location) : AmenityToolTipsView(id: amenityID, name: featureInformation, address: featureAddress, location:location, type: type, text: textAdditionalInfo, styleLightColor: styleLightColor, styleDarkColor: styleDarkColor, isBiggerStyle: true)
            amenityToolTip.bookmarkId = Int(bookmarkId)
            amenityToolTip.amenityIdBookMark = poiID
            amenityToolTip.onSaveAction = nil
//            amenityToolTip.onSaveAction = { isSave in
//                sendSaveActionToRN(amenityId: poiID,
//                                   lat: location.latitude,
//                                   long: location.longitude,
//                                   name:  featureInformation,
//                                   address1: featureAddress,
//                                   address2:"",
//                                   amenityType: originalType,
//                                   isDestination: false,
//                                   isSelected: !isBookmarked)
//            }
            
            amenityToolTip.present(from: self.navigationMapView.mapView.frame, in: self.navigationMapView.mapView, constrainedTo: amenityToolTip.frame, animated: true)
            amenityToolTip.center = self.view.center
            
            let cameraOptions = CameraOptions(center: location, zoom: navigationMapView.mapView.mapboxMap.cameraState.zoom) // using current zoom level
            navigationMapView.mapView.camera.ease(to: cameraOptions, duration: Values.standardAnimationDuration, curve: .easeInOut, completion: nil)
            
            self.view.bringSubviewToFront(amenityToolTip)
            self.toolTipCalloutView = amenityToolTip
            self.toolTipCalloutView?.adjust(to: location, in: self.navigationMapView)
            amenityToolTip.isSaved = isBookmarked
            
            amenityToolTip.onNavigationHere = { [weak self] (name, address,coordinate) in
                guard let self = self else { return }
                if type == Values.EV_CHARGER {
                    AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.ObuDisplayMode.UserClick.tooltip_ev_navigate_here, screenName: ParameterName.ObuDisplayMode.screen_view, extraData: extraData)
                } else if type == Values.PETROL {
                    AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.ObuDisplayMode.UserClick.tooltip_petrol_navigate_here, screenName: ParameterName.ObuDisplayMode.screen_view, extraData: extraData)
                }
                
                self.endObuDisplay { [weak self] in
                    self?.onNavigateToAmenity?(name, address, coordinate, poiID, type)
                }
                
            }
            
//            if let viewModel = self.viewModel {
//
//                self.adjustZoomOnlyForAmenity(at: location, callout: amenityToolTip, viewModel: viewModel)
//            }
            
            
            amenityToolTip.shareViaLocation = {
                shareLocationFromNative(address1: featureInformation, latitude: location.latitude.toString(), longtitude: location.longitude.toString(), amenity_id: amenityID, amenity_type: type, name: featureInformation, bookmarkId: Int(bookmarkId))
                
            }
            
            amenityToolTip.inviteLocation = {
                inviteLocationFromNative(address1: featureInformation, latitude: location.latitude.toString(), longtitude: location.longitude.toString(), amenity_id: amenityID, amenity_type: type, name: featureInformation, bookmarkId: Int(bookmarkId))
                
            }
        }
    }
    
    func adjustZoomOnlyForAmenity(at location: LocationCoordinate2D, callout: ToolTipsView?,viewModel: OBUDisplayViewModel){
        
        if SelectedProfiles.shared.selectedProfileName() != "" {
            if let profileCoordinate = SelectedProfiles.shared.getOuterZoneCentrePoint(){
                self.navigationMapView.mapView.setDynamicZoomLevelBasedonRadius(coordinate1: location, dynamicRadius: Values.toolTipDistance , padding: UIEdgeInsets(top: 340, left: 40, bottom:  120, right: 40), coordinate2: profileCoordinate,openToolTip: true)
            }
            
        } else {
            self.navigationMapView.mapView.setDynamicZoomLevelBasedonRadius(coordinate1: location, dynamicRadius: Values.toolTipDistance, padding: UIEdgeInsets(top: 340, left: 40, bottom:  120, right: 40), coordinate2: LocationManager.shared.location.coordinate,openToolTip: true)
        }
        
        callout?.adjust(to: location, in: self.navigationMapView)
    }
}

//
//  OBUDisplayCarplayMapUpdatable.swift
//  Breeze
//
//  Created by Tuan, Pham Hai on 05/06/2023.
//

import Foundation
import MapboxNavigation
@_spi(Restricted) import MapboxMaps
import MapboxDirections

final class OBUDisplayCarplayMapUpdatable: MapViewUpdatable {
    weak var navigationMapView: MapboxNavigation.NavigationMapView?
    
    weak var delegate: MapViewUpdatableDelegate?
    
    init(navigationMapView: MapboxNavigation.NavigationMapView) {
                    
    }
    
    func updateParkingAvailability(carpark: Carpark?) {
        
    }
    
    func updateNotificationWhenUserSoonArrive(carpark: Carpark?, message: String) {
        
    }
    
    func updateNotification(featureId: String?, currentFeature: FeatureDetectable?) {
    
    }
    
    func updateTunnel(isInTunnel: Bool) {
        
    }
    
    func cruiseStatusUpdate(isCruise: Bool) {
        
    }
    
    func updateETA(eta: TripCruiseETA?) {
        
    }
    
    func updateETA(eta: TripETA?) {
        
    }
    
    func updateLiveLocationEnabled(_ isEnabled: Bool) {
        
    }
    
    func shareDriveButtonEnabled(_ isEnabled: Bool) {
        
    }
    
    func showETAMessage(recipient: String, message: String) {
        
    }
    
    func initERPUpdater(response: MapboxDirections.RouteResponse?) {
        
    }
    
    func updateRoadName(_ name: String) {
        
    }
    
    func updateSpeedLimit(_ value: Double) {
        
    }
    
    func overSpeed(value: Bool) {
        
    }
    
}

//
//  OBUDisplayViewController.swift
//  Breeze
//
//  Created by Tuan, Pham Hai on 01/06/2023.
//

import Foundation
import Combine
import UIKit
import MapboxCoreNavigation
@_spi(Restricted) import MapboxMaps
import MapboxCoreMaps
import MapboxNavigation
import AVFAudio
import SwiftyBeaver

class OBUDisplayViewController: UIViewController {
    
    @IBOutlet weak var statusView: UIView!
    
    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var speedView: UIView!
    @IBOutlet weak var limitSpeedLabel: UILabel!
    
    @IBOutlet weak var closeView: UIView!
    
    @IBOutlet weak var voiceView: UIView!
    @IBOutlet weak var voiceImageView: UIImageView!
    @IBOutlet weak var voiceButton: UIButton!
    
    @IBOutlet weak var parkingView: UIView!
    @IBOutlet weak var parkingImageView: UIImageView!
    @IBOutlet weak var parkingButton: UIButton!
    
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var searchImageView: UIImageView!
    @IBOutlet weak var searchButton: UIButton!
    
    @IBOutlet weak var navigationMapView: NavigationMapView!
    var userLocationButton: UserLocationButton!
    
    @IBOutlet weak var  mapviewBottomConstraint: NSLayoutConstraint!
    
    
    
    private var carparkAvailableVC: UIViewController?
    private var obuDisplayVC: UIViewController?
    private var obuTravelTimeVC: UIViewController?
    
    private var carParkListVC: UIViewController?
    
    var limitSpeed: CGFloat = 60.0
    var currentSpeed: CGFloat = 40.0
    var viewModel: OBUDisplayViewModel? {
        didSet {
            viewModel?.delegate = self
        }
    }
    var carparkToast: BreezeToast?
    var trackingTimer: Timer?
    
    var onNavigateHere: ((_ carpark: Carpark) -> Void)?
    var onNavigateToAmenity: ((_ name: String, _ address: String, _ coordinate: CLLocationCoordinate2D, _ amenityId: String, _ amenityType: String) -> Void)?
    
    private var disposables = Set<AnyCancellable>()
    var etaChildVC: UIViewController?
    
    var toolTipCalloutView: ToolTipsView?    
    var previouslyTappedAmenityId: String = ""
    var isOverSpeed: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.ObuDisplayMode.UserClick.cruise_start, screenName: ParameterName.ObuDisplayMode.screen_view)
        
        setupView()
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(updateCarplayConnectionStatus(_:)),
            name: .carplayConnectionDidChangeStatus,
            object: nil)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(obuConnectFailure(_:)),
            name: .obuConnectFailure,
            object: nil)
        
        NotificationCenter.default.publisher(for: Notification.Name("onNavigationToCarpark"), object: nil)
            .receive(on: DispatchQueue.main)
            .sink { [weak self] notification in
                guard let self = self else { return }
                if let carpark = notification.userInfo?["carpark"] as? Carpark {
                    for disposable in self.disposables {
                        disposable.cancel()
                    }
                    self.endObuDisplay(byCarparkSelection: true) { [weak self] in
                        self?.onNavigateHere?(carpark)
                    }
                }
            }.store(in: &disposables)
        
        NotificationCenter.default.publisher(for: Notification.Name(Values.NotificationCloseNotifyArrivalScreen), object: nil)
            .receive(on: DispatchQueue.main)
            .sink { [weak self] notification in
                guard let self = self, let vc = self.etaChildVC else { return }
                self.removeChildViewController(vc)
                self.etaChildVC = nil
            }.store(in: &disposables)
        
//        NotificationCenter.default.publisher(for: Notification.Name(Values.NotificationETASetUp), object: nil)
//            .receive(on: DispatchQueue.main)
//            .sink { [weak self] notification in
//                guard let self = self else { return }
//                self.receiveETA(notification)
//            }.store(in: &disposables)
        
        //  Init view model and will listen for OBU event in view model
        if viewModel == nil {
            viewModel = OBUDisplayViewModel(etaFav: nil)
            viewModel?.isOverSpeed = isOverSpeed
        }
        
        navigationMapView.mapView.mapboxMap.onNext(event: .mapLoaded) { [weak self] _ in
            guard let self = self, let viewmodel = self.viewModel else { return }
            
            // check the tracking status
            viewmodel.$isTrackingUser
                .receive(on: DispatchQueue.main)
                .sink(receiveValue: { [weak self] isTracking in
                    guard let self = self else { return }
                    self.userLocationButton.isHidden = isTracking
                    if !isTracking {
                        self.startTrackingTimer()
                    } else {
                        self.stopTrackingTimer()
                    }
                }).store(in: &self.disposables)
            viewmodel.mobileMapViewUpdatable = OBUDisplayMobileMapUpdatable(navigationMapView: self.navigationMapView)
            viewmodel.startLocationUpdate()
            
            viewmodel.notifyArrivalAction = { [weak self] in
                guard let self = self else { return }
                self.etaChildVC = showShareDriveScreen(viewController: self, address: "", etaTime: "", fromScreen: "cruise")
            }
            
            viewmodel.dismissCarparkList = { [weak self] in
                guard let self = self else { return }
                self.carParkListVC?.view.removeFromSuperview()
                self.carParkListVC?.removeFromParent()
                self.carParkListVC = nil
            }
            
            viewmodel.openCarparkList = { [weak self] in
                guard let self = self else { return }
                self.openCarparkList()
            }
            
            viewmodel.notifyOnSearchAction = { [weak self] in
                guard let self = self else { return }
                self.searchAction("")
            }
            
            viewmodel.onShowInappNotification = {[weak self] (type, title, subtitle) in
                
                let appDelegate = UIApplication.shared.delegate as? AppDelegate
                if type == "erp" {
                    appDelegate?.showOnAppERPNotification(title: title, subTitle: subtitle, controller: self)
                } else if type == "parking" {
                    appDelegate?.showOnAppParkingNotification(title: title, subTitle: subtitle, controller: self)
                } else {
                    appDelegate?.showOnAppErrorNotification(title: title, subTitle: subtitle, controller: self)
                }
            }
            
            viewmodel.onEndClick = { [weak self] in
                guard let self = self else { return }
                self.viewModel?.endObuDisplay()                
                NotificationCenter.default.removeObserver(self)
//                self.dismiss(animated: true)
                self.navigationController?.popViewController(animated: true)
            }
        }
        
        navigationMapView.mapView.mapboxMap.onEvery(event: .cameraChanged) { [weak self] _ in
            guard let self = self, let viewmodel = self.viewModel else { return }
            viewmodel.updateTooltipsPosition(naviMapView: self.navigationMapView)
        }
        
        setupLocationButton()
        
        navigationMapView.mapView.gestures.delegate = self
        navigationMapView.mapView.gestures.options.pitchEnabled = false
        navigationMapView.mapView.ornaments.options.attributionButton.visibility = .hidden
        toggleDarkLightMode()
        
//        NotificationCenter.default.addObserver(self, selector: #selector(onDisplayParkingAvailability(notification:)), name: .obuOnShowCarparkAvailability, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(onCloseParkingAvailability), name: .obuOnCloseCarparkAvailability, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(onShowTravelTime(notification:)), name: .obuOnShowTravelTime, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(onCloseTravelTime), name: .obuOnCloseTravelTime, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(toggleObuDisplayTopMenu(notification:)), name: .toggleObuDisplayTopMenu, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(selectCarparkFromObuDisplay(notification:)), name: .selectCarparkFromObuDisplay, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(toggleMapViewOBU(notification:)), name: .toggleMapViewOBU, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(selectCarparkFromObuDisplay(notification:)), name: .selectCarparkFromObuDisplay, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(openCarparkList), name: .openCarparkListScreen, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(onCloseCarparkList), name: .onCloseCarparkListScreen, object: nil)

        
        showObuDisplay()        
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            self.setMapViewAsDefault()
//            self.showDemoOptions()
        }
        
        if OBUHelper.shared.hasLastObuData() {
            AnalyticsManager.shared.logSystemEvent(eventValue: ParameterName.SystemAnalytics.SystemEvent.bottom_obu_card_shown, screenName: ParameterName.ObuDisplayMode.screen_view)
        }
        disableToggleButtonCarparkList(false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
        viewModel?.isOpenningSearch = false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    private func setupView() {
        
        //  Get current voice status then display
        let audioEnable = Prefs.shared.getBoolValue(.enableAudio)
        voiceButton.isSelected = !audioEnable
        if voiceButton.isSelected {
            voiceImageView.image = UIImage(named: "obuVoiceMuted")
        } else {
            voiceImageView.image = UIImage(named: "obuVoiceEnable")
        }
        
        //  Setup border and corner radius
        let borderColor = UIColor(hexString: "4F5778", alpha: 1.0)
        speedView.roundedBorder(cornerRadius: 40, borderColor: borderColor.cgColor, borderWidth: 2.0)
        closeView.roundedBorder(cornerRadius: 19, borderColor: borderColor.cgColor, borderWidth: 1.0)
        voiceView.roundedBorder(cornerRadius: 24, borderColor: borderColor.cgColor, borderWidth: 1.0)
        searchView.roundedBorder(cornerRadius: 24, borderColor: borderColor.cgColor, borderWidth: 1.0)
        parkingView.roundedBorder(cornerRadius: 24, borderColor: borderColor.cgColor, borderWidth: 1.0)
    }
    
    private func setMapViewAsDefault() {
        //  BREEZE2-1011 Always hide top menu
        let isShowingMap = viewModel?.isShowingMap ?? true
        
        if OBUHelper.shared.hasLastObuData() {
            mapviewBottomConstraint.constant = isShowingMap ? -155 : 0
        } else {
            mapviewBottomConstraint.constant = -114
        }
        
        self.view.backgroundColor = isShowingMap ? UIColor.white : UIColor(hexString: "#222638", alpha: 1.0)
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
        self.showTopMenu(!isShowingMap)
        self.updateShowMapMenu(isShowingMap)
    }
    
    private func showDemoOptions() {
        
        let alertVC = UIAlertController(title: "Mock Data Alert", message: "Please choose mock data source", preferredStyle: .alert)
        // create a cancel action
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            // handle cancel response here. Doing nothing will dismiss the view.
        }
        // add the cancel action to the alertController
        alertVC.addAction(cancelAction)

        // create mock data from SDK
        let mockManagerAction = UIAlertAction(title: "Use Mock Manger", style: .default) { (action) in
            self.viewModel?.setMockDataMode(.mockSDK)
        }
        
        // create mock data from server
        let serverBreezeAction = UIAlertAction(title: "Use Breeze Server", style: .default) { (action) in
            self.viewModel?.setMockDataMode(.mockServer)
        }
        
        // add the OK action to the alert controller
        alertVC.addAction(mockManagerAction)
        alertVC.addAction(serverBreezeAction)
        
        self.present(alertVC, animated: true)

    }
    
    @IBAction func searchAction(_ sender: Any) {
        viewModel?.isOpenningSearch = true
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.ObuDisplayMode.UserClick.search, screenName: ParameterName.ObuDisplayMode.screen_view)
        self.openRNScreen()
    }
    
    @IBAction func closeAction(_ sender: Any) {
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.ObuDisplayMode.UserClick.close, screenName: ParameterName.ObuDisplayMode.screen_view)
        viewModel?.endObuDisplay()
        NotificationCenter.default.removeObserver(self)
//        self.dismiss(animated: true)
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func voiceAction(_ sender: UIButton) {
        voiceButton.isSelected = !sender.isSelected
        Prefs.shared.setValue(!voiceButton.isSelected, forkey: .enableAudio)
        
        if voiceButton.isSelected {
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.ObuDisplayMode.UserToggle.mute_on, screenName: ParameterName.ObuDisplayMode.screen_view)
            voiceImageView.image = UIImage(named: "obuVoiceMuted")
        } else {
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.ObuDisplayMode.UserToggle.mute_off, screenName: ParameterName.ObuDisplayMode.screen_view)
            voiceImageView.image = UIImage(named: "obuVoiceEnable")
        }
    }
    
    @IBAction func parkingAction(_ sender: UIButton) {
        parkingButton.isSelected = !sender.isSelected
        parkingImageView.image = UIImage(named: "obuParking")
        let borderColor = UIColor(hexString: "4F5778", alpha: 1.0)
        if parkingButton.isSelected {
            
            AnalyticsManager.shared.logToggleEvent(eventValue: ParameterName.ObuDisplayMode.UserToggle.p_button_off, screenName: ParameterName.ObuDisplayMode.screen_view)
            
            parkingView.backgroundColor = UIColor(hexString: "B155F8", alpha: 1.0)
            parkingView.roundedBorder(cornerRadius: 24, borderColor: borderColor.cgColor, borderWidth: 0.0)
        } else {
            
            AnalyticsManager.shared.logToggleEvent(eventValue: ParameterName.ObuDisplayMode.UserToggle.p_button_on, screenName: ParameterName.ObuDisplayMode.screen_view)
            
            parkingView.backgroundColor = UIColor(hexString: "393F58", alpha: 1.0)
            parkingView.roundedBorder(cornerRadius: 24, borderColor: borderColor.cgColor, borderWidth: 1.0)
        }
        self.showObuDisplay(!parkingButton.isSelected)
        
        viewModel?.onParkingEnabled(parkingButton.isSelected)
        
        if !parkingButton.isSelected {
            startTrackingTimer()
        }else {
            stopTrackingTimer()
        }
        self.updateShowMapMenu(false)
        
        viewModel?.isPSelected = parkingButton.isSelected
    }
    

    private func openRNScreen(){
        
        let params = [ // Object data to be used by that scree
             "fromScreen":"OBUDisplay",
             "baseURL": appDelegate().backendURL,
             "idToken":AWSAuth.sharedInstance.idToken,
             "deviceos":parameters!["deviceos"],
             "deviceosversion":parameters!["deviceosversion"],
             "devicemodel":parameters!["devicemodel"],
             "appversion":parameters!["appversion"],
        ]
         self.goToRNScreen(toScreen: RNScreeNames.SERACH_LOCATION,navigationParams: params as [String:Any])
    }
    
    private func showObuDisplay(_ isShow: Bool) {
        
        if viewModel?.isShowingMap == true {
            return
        }
        
        if isShow {
            self.obuDisplayVC?.view.alpha = 0
            self.obuDisplayVC?.view.isHidden = true
            UIView.animate(withDuration: 0.35, delay: 0.0) {
                self.obuDisplayVC?.view.alpha = 1
                self.obuDisplayVC?.view.isHidden = false
            }
        } else {
            self.obuDisplayVC?.view.alpha = 1
            self.obuDisplayVC?.view.isHidden = false
            UIView.animate(withDuration: 0.35, delay: 0.0) {
                self.obuDisplayVC?.view.alpha = 0
                self.obuDisplayVC?.view.isHidden = true
            }
        }
    }
    
    private func updateSpeed(_ velocity: CGFloat, limitSpeed: CGFloat) {
        self.currentSpeed = velocity > 0 ? velocity : 0
        self.limitSpeed = limitSpeed
        
//        speedView.isHidden = !(limitSpeed > 0)
        if currentSpeed > limitSpeed, limitSpeed > 0 {
            //  Over speed then fire event to RN show slow down
            let borderColor = UIColor(hexString: "E82370", alpha: 1.0)
            speedView.backgroundColor = borderColor
            speedView.roundedBorder(cornerRadius: 40, borderColor: borderColor.cgColor, borderWidth: 2.0)
        } else {
            //  Normal speed
            let borderColor = UIColor(hexString: "4F5778", alpha: 1.0)
            speedView.backgroundColor = UIColor(hexString: "393F58", alpha: 1.0)
            speedView.roundedBorder(cornerRadius: 40, borderColor: borderColor.cgColor, borderWidth: 2.0)
        }
    }
    
    func showObuDisplay() {
        if self.obuDisplayVC == nil {
            self.obuDisplayVC = self.getRNView(toScreen: "OBUDisplay", navigationParams: [:])
            self.obuDisplayVC?.view.frame = UIScreen.main.bounds
            if let vc = self.obuDisplayVC {
                self.addChildViewControllerWithView(childViewController: vc, toView: self.view, belowView: self.containerView)
            }
        }
    }
    
    private func setupLocationButton() {
        userLocationButton = UserLocationButton(buttonSize: 60)
        userLocationButton.addTarget(self, action: #selector(locationButtonTapped), for: .touchUpInside)
        navigationMapView.mapView.addSubview(userLocationButton)
        userLocationButton.isHidden = true // hide it by default
        
        userLocationButton.translatesAutoresizingMaskIntoConstraints = false
        
        if let navMapview = navigationMapView {
            userLocationButton.snp.makeConstraints { make in
                make.trailing.equalToSuperview().offset(-18)
                make.bottom.equalTo(navMapview.safeAreaLayoutGuide.snp.bottom).offset(-142 + 60)    //  Remove share drive button
                make.height.equalTo(60)
                make.width.equalTo(60)
            }
        }
    }
    
    @objc func locationButtonTapped(sender: UserLocationButton) {
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.ObuDisplayMode.UserClick.recentre_map, screenName: ParameterName.ObuDisplayMode.screen_view)
        resetCamera()
    }
    
    func resetCamera() {
        viewModel?.recenter()
    }
    
    func endObuDisplay(byCarparkSelection: Bool = false, completion: (() -> Void)? = nil){                
        // don't trigger the call back to prevent from crashing
        navigationMapView.mapView.gestures.delegate = nil
        self.viewModel?.endObuDisplay()
        appDelegate().enterHome()
        /*
        self.dismiss(animated: false) {
            completion?()
        }
         */
        self.navigationController?.popViewController(animated: false)
        completion?()
    }
    
    // MARK: - Theme updated
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        guard previousTraitCollection?.userInterfaceStyle != traitCollection.userInterfaceStyle else {
            return
        }
        toggleDarkLightMode()
    }
    
    private func toggleDarkLightMode() {
        navigationMapView.mapView.mapboxMap.style.uri = StyleURI(url: URL(string: isDarkMode ? Constants.Map.nightStyleUrl: Constants.Map.styleUrl)!)
    }
    
    @objc func updateCarplayConnectionStatus(_ notification: NSNotification) {
        if let status = notification.object as? Bool {
            if status {
                DispatchQueue.main.async {
                    //  Stop obulite display
                    self.closeAction("")
                    
                    //  Start cruise mode
                    if let mapLandingVC = appDelegate().getMapLandingVCInTop() {
                        mapLandingVC.startCruiseMode()
                    }
                }
            }
        }
    }
    
    deinit {
        self.obuTravelTimeVC = nil
        self.carparkAvailableVC = nil
        self.obuDisplayVC = nil
        OBUHelper.shared.stopObuMockEvent()
        OBUHelper.shared.stopBreezeMockEvent()
        OBUHelper.shared.stopObuEvent()
        NotificationCenter.default.removeObserver(self)
    }
}

extension OBUDisplayViewController {
    
    @objc func recenterPuck() {
        if viewModel?.userIsMoving == true {
            resetCamera()
        }
    }
    
    private func startTrackingTimer() {
        stopTrackingTimer()
        self.trackingTimer = Timer.scheduledTimer(withTimeInterval: 10, repeats: false, block: { _ in
            self.recenterPuck()
        })
    }
    
    private func stopTrackingTimer() {
        self.trackingTimer?.invalidate()
        self.trackingTimer = nil
    }
    
    //MARK: - ETA SetUp
    @objc func receiveETA(_ notification: Notification) {
        
        if let dictionary = notification.userInfo {
            let name = dictionary[Values.etaContactName] as! String
            let number = dictionary[Values.etaPhoneNumber] as! String
            var message = dictionary[Values.etaMessage] as! String
            let favoriteId = dictionary[Values.etaFavoriteId] as! Int
            DispatchQueue.main.async { [weak self] in
                guard let self = self, let vm = self.viewModel else { return }

                // 1. Update ETATrigger.shared.tripCruiseETA by converting ETAInfo to TripCruiseETA
                // TODO: May need refactoring by not using singleton and use standard ETA object for  cruise/routeplaning/navigation
                
                message = "\(AWSAuth.sharedInstance.userName) is sharing real-time location to you on Breeze"
                ETATrigger.shared.tripCruiseETA = TripCruiseETA(type: TripETA.ETAType.Init, message: message, recipientName: name, recipientNumber: number, shareLiveLocation: "Y", tripStartTime: Int(Date().timeIntervalSince1970), tripDestLat: 0, tripDestLong: 0, destination: "", tripEtaFavouriteId: favoriteId)
                // 2. Setup ETA
                vm.hideShareDriveButton(false)
                vm.setupETA()
            }
        }
    }
}

//  Handle show parking available
extension OBUDisplayViewController {
    
    @objc func onDisplayParkingAvailability(notification: Notification) {
        if let carparkNames = notification.object as? [String] {
            showObuCarparkAvailability(carparkNames)
        }
    }
    
    @objc func onCloseParkingAvailability() {
        self.carparkAvailableVC?.view.removeFromSuperview()
    }
    
    func showObuCarparkAvailability(_ carparkNames: [String]) {
        self.carparkAvailableVC = self.getRNView(toScreen: "OBUCarparkAvailability", navigationParams: ["carparkNames": carparkNames] as [String: Any])
        self.carparkAvailableVC?.view.frame = UIScreen.main.bounds
        if let vc = self.carparkAvailableVC {
            self.addChildViewControllerWithView(childViewController: vc, toView: self.view, belowView: nil)
            self.view.bringSubviewToFront(vc.view)
        }
    }
    
    
    @objc func openCarparkList() {
        DispatchQueue.main.async {
            if let vm = self.viewModel {
                if let userLocation = vm.lastLocation?.coordinate {
                    let locationDic = ["lat": userLocation.latitude, "long": userLocation.longitude]
                    let params: [String: Any] = [ // Object data to be used by that scree
                        "location": locationDic,
                        "baseURL": appDelegate().backendURL,
                        "idToken": AWSAuth.sharedInstance.idToken,
                        "deviceos":parameters!["deviceos"] as Any,
                        "deviceosversion":parameters!["deviceosversion"] as Any,
                        "devicemodel":parameters!["devicemodel"] as Any,
                        "appversion":parameters!["appversion"] as Any
                    ]
                    
                    //            onCloseCarparkList()
                    
                    self.carParkListVC = self.getRNViewBottomSheet(toScreen: RNScreeNames.CARPARK_LIST, navigationParams: params)
                    self.carParkListVC?.view.frame = UIScreen.main.bounds
                    
                    if let vc = self.carParkListVC {
                        self.addChildViewControllerWithView(childViewController: vc, toView: self.view, belowView: self.containerView)
                    }
                }
            }
            
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.ObuDisplayMode.UserClick.show_carpark_list, screenName: ParameterName.ObuDisplayMode.screen_view)
        }
    }
    
    @objc func onCloseCarparkList() {
        self.carParkListVC?.view.removeFromSuperview()
        self.carParkListVC?.removeFromParent()
        self.carParkListVC = nil
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.ObuDisplayMode.UserClick.carpark_list_close, screenName: ParameterName.ObuDisplayMode.screen_view)
    }
    
    
}

//  Handle show estimation time
extension OBUDisplayViewController {
    
    @objc func onShowTravelTime(notification: Notification) {
        if let travelParams = notification.object as? [String: Any] {
            showTravelTime(travelParams)
        }
    }
    
    @objc func onCloseTravelTime() {
        self.obuTravelTimeVC?.view.removeFromSuperview()
    }
    
    func showTravelTime(_ params: [String: Any]) {
        self.obuTravelTimeVC = self.getRNView(toScreen: "OBUTravelTime", navigationParams: params)
        self.obuTravelTimeVC?.view.frame = UIScreen.main.bounds
        if let vc = self.obuTravelTimeVC {
            self.addChildViewControllerWithView(childViewController: vc, toView: self.view, belowView: nil)
            self.view.bringSubviewToFront(vc.view)
        }
    }
    
    /*  BREEZE2-1011: No need handle toggle button because always show mapview
    @objc func toggleObuDisplayTopMenu(notification: Notification) {
        if let isShow = notification.object as? Bool {
            //  Dont toggle top buttons if showing map view
            if self.viewModel?.isShowingMap == false && !self.parkingButton.isSelected {
                showTopMenu(isShow)
            }
        }
    }
    
    @objc func toggleMapViewOBU(notification: Notification) {
        if let isShowingMap = notification.object as? Bool {
            self.viewModel?.isShowingMap = isShowingMap
            mapviewBottomConstraint.constant = isShowingMap ? -130 : 0
            self.view.backgroundColor = isShowingMap ? UIColor.white : UIColor(hexString: "#222638", alpha: 1.0)
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
            self.showTopMenu(!isShowingMap)
            self.updateShowMapMenu(isShowingMap)
        }
    }
     */
    
    func updateShowMapMenu(_ isShowingMap: Bool) {
        //  Sync mute status
        let audioEnable = Prefs.shared.getBoolValue(.enableAudio)
        voiceButton.isSelected = !audioEnable
        if voiceButton.isSelected {
            voiceImageView.image = UIImage(named: "obuVoiceMuted")
        } else {
            voiceImageView.image = UIImage(named: "obuVoiceEnable")
        }
        viewModel?.mobileMapViewUpdatable?.updateMuteStatus()
  
        viewModel?.mobileMapViewUpdatable?.setIsFromDisplay(true, isShowingMap: isShowingMap)
        viewModel?.mobileMapViewUpdatable?.showMapMenuView(isShowingMap)
        self.userLocationButton.isHidden = true
    }
    
    @objc func selectCarparkFromObuDisplay(notification: Notification) {
        DispatchQueue.main.async {
            if let carparkDic = notification.object as? NSDictionary {
                //  Navigate to carpark
                do {
                    let jsonData = try JSONSerialization.data(withJSONObject: carparkDic, options: .prettyPrinted)
                    let theCarpark = try JSONDecoder().decode(Carpark.self, from: jsonData)
                    
                    let extraData: NSDictionary = ["message": (theCarpark.isCheapest == true) ? "select_cheapest" : "select_normal", "location_name": "location_\(theCarpark.name ?? "")", "longitude": theCarpark.long ?? 0.0, "latitude": theCarpark.lat ?? 0.0]
                    AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.ObuDisplayMode.UserClick.parking_guidance_system_carpark_select, screenName: ParameterName.ObuDisplayMode.screen_view, extraData: extraData)
                    
                    let carparkInfo = ["carpark":theCarpark] as [String : Any]
                    NotificationCenter.default.post(name: Notification.Name("onNavigationToCarpark"), object: nil,userInfo: carparkInfo)
                } catch let error {
                    print(error)
                }
            }
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.ObuDisplayMode.UserClick.carpark_list_select_carpark, screenName: ParameterName.ObuDisplayMode.screen_view)
            
        }
    }
}

extension OBUDisplayViewController: OBUDisplayModelDelegate {
    func showNoCarpark(_ model: OBUDisplayViewModel) {
        if self.carparkToast == nil {
            self.carparkToast = BreezeToast(appearance: BreezeToastStyle.carparkToast, onClose: { [weak self] in
                guard let self = self else { return }
                self.carparkToast = nil
            }, actionTitle: "", onAction: nil)

            self.carparkToast?.show(in: self.navigationMapView, title: "No Carpark Nearby", message: Constants.toastNearestCarparkCruise, animated: true)
            self.carparkToast?.yOffset = getTopOffset()
        }
    }
    
    func hideNoCarpark(_ model: OBUDisplayViewModel){
        if self.carparkToast != nil {
            self.carparkToast?.removeFromSuperview()
        }
    }
    
    func updateLimitSpeed(_ limitSpeed: Double, currentSpeed: Double) {
        limitSpeedLabel.text = "\(Int(limitSpeed))"
        updateSpeed(CGFloat(currentSpeed), limitSpeed: CGFloat(limitSpeed))
    }
    
    func showTopMenu(_ isShow: Bool) {
        speedView.isHidden = !isShow
        closeView.isHidden = !isShow
        voiceView.isHidden = !isShow
        parkingView.isHidden = !isShow
        searchView.isHidden = !isShow
    }
    
    func showMapAnimation() {
        parkingAction(parkingButton)
        DispatchQueue.main.asyncAfter(deadline: .now() + 16) {
            self.parkingAction(self.parkingButton)
        }
    }
    
    func getBearing() -> String {
        if let bearing = navigationMapView?.mapView.cameraState.bearing {
            return "\(bearing)"
        }
        return ""
    }
    
    func dismissToolTip() {
        self.didDeselectAmenityAnnotation()
        self.didDeselectAnnotation()
    }
    
    func getCallout() -> ToolTipsView? {
        return toolTipCalloutView
    }
    
    func showToastConnectedOBU() {
        if !appDelegate().carPlayConnected {
            DispatchQueue.main.async {
                showToast(from: self.view, message: "OBU is connected", topOffset: 20, fromCarPlay: false)
            }
        }
    }
}

//  MARK: - GestureManagerDelegate methods
extension OBUDisplayViewController: GestureManagerDelegate {
   
    func gestureManager(_ gestureManager: GestureManager, didEnd gestureType: GestureType, willAnimate: Bool) {
        
        //We will use this if there is an action neede for map touch
    }
    
    func gestureManager(_ gestureManager: GestureManager, didEndAnimatingFor gestureType: GestureType) {
        
        //We will use this if there is an action neede for map touch
    }
    
    func gestureManager(_ gestureManager: GestureManager, didBegin gestureType: GestureType) {
        
        var name = ParameterName.Home.MapInteraction.drag
        switch gestureType {
        case .pinch:
            name = ParameterName.Home.MapInteraction.pinch
        // rc4 - remove rotate
//        case .rotate:
//            name = ParameterName.Home.MapInteraction.rotate
        case .singleTap:
            let point = gestureManager.singleTapGestureRecognizer.location(in: navigationMapView)
            viewModel?.queryAmenitiesLayer(point: point)
            
            self.queryAmenitiesLayer(point: point)
            
        default:
            name = ParameterName.Home.MapInteraction.drag
        }
        
        if gestureType == .pan || gestureType == .pinch || gestureType == .doubleTapToZoomIn || gestureType == .doubleTouchToZoomOut {
//            isTrackingUser = false
            viewModel?.noTracking()
        }
        
        if gestureType == .singleTap || gestureType == .pinch || gestureType == .doubleTapToZoomIn || gestureType == .doubleTouchToZoomOut {
            viewModel?.dismissTooltips()
        }
    }
}

//  MARK: - Handle OBU connection suddenly disconnected
extension OBUDisplayViewController {
    
    @objc func obuConnectFailure(_ notification: NSNotification) {
        //  Define error come from what screen the decide what to do
        if let model = notification.object as? ObuErrorModel {
            DispatchQueue.main.async {
                self.handleObuConnectionError(model)
            }
        }
    }
    
    private func handleObuConnectionError(_ model: ObuErrorModel) {
        DispatchQueue.main.async {
            if let errorCode = model.error?.code {
                
                SwiftyBeaver.debug("OBU - handleObuConnectionError: OBU name \(model.name ?? ""), Vehicle No.\(model.vehicleNumber ?? ""), error code: \(errorCode)")
                
                switch errorCode {
                case 1000:
                    self.showAlert(title: "Connection Error", message: "Make sure your mobile data is enable", confirmTitle: "Got It")
                case 1001:
                    self.showAlert(title: "Connection Error", message: "Bluetooth needs to be enabled to connect", confirmTitle: "Got It")
                case 1003, 1004, 1005, 1006, 2002, 2003, 2009, 2010, 400, 401, 408, 500, 616:
                    self.showObuError(.SystemError, model: model)
                case 2000:
                    self.showObuError(.PairingError, model: model)
                case 2001, 1002:
                    self.showObuError(.OBUConnectioLost, model: model)
                case 404:
                    self.showObuError(.OBUNotDetected, model: model)
                case 2004:
                    self.showAlert(title: "Pairing failed", message: "Permission is needed to Pair OBU to \"Breeze\"", confirmTitle: "Close")
                case 2006:
                    self.showAlert(title: "", message: "Mac address entered did not match Mac address registered with LTA", confirmTitle: "Close")
                default:
                    break
                }
            }
        }
    }
    
    private func showObuError(_ errorCase: ConnectError, model: ObuErrorModel) {
        let storyboard: UIStoryboard = UIStoryboard(name: "OBU", bundle: nil)
        if let errorConnectVC = storyboard.instantiateViewController(withIdentifier: "OBUConnectErrorViewController") as? OBUConnectErrorViewController {
            errorConnectVC.modalPresentationStyle = .fullScreen
            errorConnectVC.errorCase = errorCase
            errorConnectVC.errorModel = model
            errorConnectVC.skipCompletion = { [weak self] in
                //  make sure the error popup already dismiss
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    self?.closeAction("")
                }
            }
            self.present(errorConnectVC, animated: true)
        }
    }
}


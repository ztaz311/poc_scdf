//
//  OnboardingVC.swift
//  Breeze
//
//  Created by Malou Mendoza on 8/3/21.
//

import UIKit

protocol OnboardingVCDelegate: class {
    func hideOnboardingScreen()
    
}
class OnboardingVC: UIViewController, UIScrollViewDelegate {
    
    weak var delegate: OnboardingVCDelegate?
    @IBOutlet weak var scrollView: UIScrollView!{
        didSet{
            scrollView.delegate = self
        }
    }
    
    @IBOutlet weak var pageControl: UIPageControl!
    
    
    var slides:[PageView] = [];
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Prefs.shared.setValue(true, forkey: .isOnboardingShown)        
        slides = createSlides()
        setupSlideScrollView(slides: slides)

        pageControl.numberOfPages = slides.count
        pageControl.currentPage = 0
        pageControl.pageIndicatorTintColor = UIColor.gray
        pageControl.currentPageIndicatorTintColor = UIColor.brandPurpleColor
        view.bringSubviewToFront(pageControl)
        
        setupDarkLightAppearance(forceLightMode: true)
    }    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func createSlides() -> [PageView] {
        
        let slide1:PageView = Bundle.main.loadNibNamed("PageView", owner: self, options: nil)?.first as! PageView
        slide1.displayImg.image = UIImage(named: "page1Img")
        slide1.headerLbl.text = "Safety is always the first priority"
        slide1.detailsLbl.text = "Please don’t drive while playing in BeBeep Testing Ground."
        slide1.nextBtn.setTitle("Alright!", for: .normal)
       // slide1.nextBtn.applyGradient(colors: [UIColor.brandPurpleColor.cgColor,UIColor.brandPurpleColor.cgColor])
        
        let slide2:PageView = Bundle.main.loadNibNamed("PageView", owner: self, options: nil)?.first as! PageView
        slide2.displayImg.image = UIImage(named: "page2Img")
        slide2.headerLbl.text = "Welcome to Round 2 of BeBeep Testing Ground!"
        slide2.detailsLbl.text = "Search for locations, get navigated, and also save locations as favourite!"
        slide2.nextBtn.setTitle("Nice!", for: .normal)
       // slide2.nextBtn.applyGradient(colors: [UIColor.brandPurpleColor.cgColor,UIColor.brandPurpleColor.cgColor])
        
        
        let slide3:PageView = Bundle.main.loadNibNamed("PageView", owner: self, options: nil)?.first as! PageView
        slide3.displayImg.image = UIImage(named: "page3Img")
        slide3.headerLbl.text = "Ready to play?"
        slide3.detailsLbl.text = "We work hard to action on your feedback, and we would love more of it this round!"
        slide3.nextBtn.setTitle("Got it, let’s start!", for: .normal)
       // slide3.nextBtn.applyGradient(colors: [UIColor.brandPurpleColor.cgColor,UIColor.brandPurpleColor.cgColor])
        
        return [slide1, slide2, slide3]
    }
    
    
    func setupSlideScrollView(slides : [PageView]) {
        scrollView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        scrollView.contentSize = CGSize(width: view.frame.width * CGFloat(slides.count), height: view.frame.height)
        scrollView.isPagingEnabled = true
        scrollView.bounces = false
        
        
        let heightSlide = view.frame.height
        for i in 0 ..< slides.count {
            slides[i].frame = CGRect(x: view.frame.width * CGFloat(i), y: 0, width: view.frame.width, height: heightSlide)
            slides[i].nextBtn.tag = i
            slides[i].nextBtn.addTarget(self, action: #selector(nextPageAction), for: .touchUpInside)
            scrollView.addSubview(slides[i])
            slides[i].nextBtn.applyGradient(colors: [UIColor.brandPurpleColor.cgColor,UIColor.brandPurpleColor.cgColor], withShadow: false)
            slides[i].setNeedsLayout()
        }
        scrollView.contentSize = CGSize(width: view.frame.width * CGFloat(slides.count), height: 0)
        
        
    }
    
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageIndex = round(scrollView.contentOffset.x/view.frame.width)
        pageControl.currentPage = Int(pageIndex)
        
    }
    
    @objc func nextPageAction(_ sender: UIButton){
        let activePage = sender.tag + 1
        if activePage < slides.count{
            pageControl.currentPage = activePage
            pageControl.updateCurrentPageDisplay()
            
            scrollView.setContentOffset(CGPoint.init(x: (activePage) * Int(view.bounds.width), y: 0), animated: true)
        }else{
            self.navigationController?.popViewController(animated: false)
            delegate?.hideOnboardingScreen()
        }
    }
    
}



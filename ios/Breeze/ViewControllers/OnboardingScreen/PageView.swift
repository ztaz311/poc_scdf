//
//  PageView.swift
//  Breeze
//
//  Created by Malou Mendoza on 8/3/21.
//

import UIKit

class PageView: UIView {
    
    @IBOutlet weak var displayImg: UIImageView!
    @IBOutlet weak var headerLbl: UILabel!
    @IBOutlet weak var detailsLbl: UILabel!
    @IBOutlet weak var nextBtn: UIButton!
    
   
}

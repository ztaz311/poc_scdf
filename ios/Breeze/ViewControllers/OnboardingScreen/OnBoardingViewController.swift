//
//  OnBoardingViewController.swift
//  Breeze
//
//  Created by Malou Mendoza on 23/11/21.
//

import UIKit

class OnBoardingViewController: UIViewController {

    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var trafficEventBtn: UIButton!
    @IBOutlet weak var schoolZnBtn: UIButton!
    @IBOutlet weak var silverZnBtn: UIButton!
    @IBOutlet weak var roadBlkBtn: UIButton!
    @IBOutlet weak var floodBtn: UIButton!
    @IBOutlet weak var trafficImg: UIImageView!
    @IBOutlet weak var schoolZnImg: UIImageView!
    @IBOutlet weak var silverZnimg: UIImageView!
    @IBOutlet weak var floodImg: UIImageView!
    @IBOutlet weak var roadBlkImg: UIImageView!
    @IBOutlet weak var gapConstraints: NSLayoutConstraint!
   
    
   
    override func viewDidLoad() {
        super.viewDidLoad()

        self.nextBtn.applyGradient(colors: [UIColor.brandPurpleColor.cgColor,UIColor.brandPurpleColor.cgColor], withShadow: false)
        setupDarkLightAppearance(forceLightMode: true)
        
    }
    

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if ( UIScreen.main.bounds.size.height < 844){
            self.gapConstraints.constant = 10
          
        }
        getNotificationSelected()
        updateButtons()
        
        AnalyticsManager.shared.logScreenView(screenName: ParameterName.OnBoardingCruiseIntro.screen_view)
        AnalyticsManager.shared.logScreenView(screenName: ParameterName.OnBoardingRoad.screen_view)
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        updateButtons()
    }
    
    func getNotificationSelected(){
        let defaults = UserDefaults.standard
        let amenitiesSelected = defaults.stringArray(forKey: "notificationsSelected") ?? [String]()
        if amenitiesSelected.contains("trafficEvents"){
            trafficImg.isHidden = false
        }
        if amenitiesSelected.contains("schoolZone"){
            schoolZnImg.isHidden = false
        }
        if amenitiesSelected.contains("silverZone"){
            silverZnimg.isHidden = false
        }
        if amenitiesSelected.contains("fashFlood"){
            floodImg.isHidden = false
        }
        if amenitiesSelected.contains("roadClosure"){
            roadBlkImg.isHidden = false
        }
       
        
      
    }
    
    
    @IBAction func nextBtnClicked(_ sender: Any) {

        
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.OnBoardingCruiseIntro.UserClick.next, screenName: ParameterName.OnBoardingCruiseIntro.screen_view)
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.OnBoardingRoad.UserClick.next, screenName: ParameterName.OnBoardingRoad.screen_view)
        
        let storyboard = UIStoryboard(name: "Onboarding", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "OnboardingAmenitiesVC") as! OnboardingAmenitiesVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func trafficBtnClicked(_ sender: Any) {
        self.trafficImg.isHidden = !self.trafficImg.isHidden
        updateButtons()
       
    }
    
    @IBAction func schoolZnBtnClicked(_ sender: Any) {
        self.schoolZnImg.isHidden = !self.schoolZnImg.isHidden
        updateButtons()
    }
    
    @IBAction func SIlverZnBtnClicked(_ sender: Any) {
        self.silverZnimg.isHidden = !self.silverZnimg.isHidden
        updateButtons()
    }
    
    @IBAction func floodBtnClicked(_ sender: Any) {
        self.floodImg.isHidden = !self.floodImg.isHidden
        updateButtons()
    }
    
    @IBAction func roadBlkBtnClicked(_ sender: Any) {
        self.roadBlkImg.isHidden = !self.roadBlkImg.isHidden
        updateButtons()
    }
    
    
    func updateButtons(){
        self.trafficImg.isHidden ?  self.trafficEventBtn.setImage(UIImage.init(named: "trafficUnselected"), for: .normal) :  self.trafficEventBtn.setImage(UIImage.init(named: "trafficEventsOnboard"), for: .normal)
        self.schoolZnImg.isHidden ?  self.schoolZnBtn.setImage(UIImage.init(named: "schoolUnselected"), for: .normal) :  self.schoolZnBtn.setImage(UIImage.init(named: "schoolZoneOnBoard"), for: .normal)
        self.silverZnimg.isHidden ?  self.silverZnBtn.setImage(UIImage.init(named: "silverZnUnselectd"), for: .normal) :  self.silverZnBtn.setImage(UIImage.init(named: "silverZoneOnBoard"), for: .normal)
        self.floodImg.isHidden ?  self.floodBtn.setImage(UIImage.init(named: "floodUnselected"), for: .normal) :  self.floodBtn.setImage(UIImage.init(named: "flasFloodOnBoard"), for: .normal)
        self.roadBlkImg.isHidden ?  self.roadBlkBtn.setImage(UIImage.init(named: "roadBlkUnselected"), for: .normal) :  self.roadBlkBtn.setImage(UIImage.init(named: "roadBlockOnBoard"), for: .normal)
        
        
        var notificationsArray = [String]()
        notificationsArray.append("erp")
        if (!trafficImg.isHidden){
            notificationsArray.append("trafficEvents")
        }
        if (!schoolZnImg.isHidden){
            notificationsArray.append("schoolZone")
        }
        if (!silverZnimg.isHidden){
            notificationsArray.append("silverZone")
        }
        if (!floodImg.isHidden){
            notificationsArray.append("fashFlood")
        }
        if (!roadBlkImg.isHidden){
            notificationsArray.append("roadClosure")
        }
        
       
       
        let defaults = UserDefaults.standard
        defaults.set("", forKey: "notificationsSelected")
        defaults.set(notificationsArray, forKey: "notificationsSelected")
        
        
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.OnBoardingRoad.UserClick.road_notify_selections, screenName: ParameterName.OnBoardingRoad.screen_view)
    }

}

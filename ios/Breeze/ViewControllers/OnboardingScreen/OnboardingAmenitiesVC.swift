//
//  OnboardingAmenitiesVC.swift
//  Breeze
//
//  Created by Malou Mendoza on 24/11/21.
//

import UIKit

class OnboardingAmenitiesVC: UIViewController {

    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var doneBtn: UIButton!
    @IBOutlet weak var petrolBtn: UIButton!
    @IBOutlet weak var parksBtn: UIButton!
    @IBOutlet weak var hawkerBtn: UIButton!
    @IBOutlet weak var chargerBtn: UIButton!
    @IBOutlet weak var cafeBtn: UIButton!
    @IBOutlet weak var libraryBtn: UIButton!
    @IBOutlet weak var playgroundBtn: UIButton!
    @IBOutlet weak var restoBtn: UIButton!
    @IBOutlet weak var museumBtn: UIButton!
    @IBOutlet weak var sportsBtn: UIButton!
    @IBOutlet weak var trailBtn: UIButton!
    @IBOutlet weak var heritageBtn: UIButton!
    
    
    @IBOutlet weak var amenityWalkingtrail: UIImageView!
    @IBOutlet weak var amenityPCN: UIImageView!
    @IBOutlet weak var amenityMuseum: UIImageView!
    @IBOutlet weak var amenityheritage: UIImageView!
    @IBOutlet weak var amenitySports: UIImageView!
    @IBOutlet weak var amenityCafe: UIImageView!
    @IBOutlet weak var amenityCharger: UIImageView!
    @IBOutlet weak var amenityPark: UIImageView!
    @IBOutlet weak var amenityLibrary: UIImageView!
    @IBOutlet weak var amenityPetrol: UIImageView!
    @IBOutlet weak var amenityHawker: UIImageView!
    @IBOutlet weak var amenityPlayground: UIImageView!
    @IBOutlet weak var amenityResto: UIImageView!
    @IBOutlet weak var contentView :UIView!
    
    @IBOutlet weak var tableViewHeightConstraints: NSLayoutConstraint!
    @IBOutlet weak var gapWidthConstraints: NSLayoutConstraint!
    @IBOutlet weak var gapConstraints: NSLayoutConstraint!
    @IBOutlet weak var gapButtonsConstraints: NSLayoutConstraint!
    @IBOutlet weak var gapButtons2Constraints: NSLayoutConstraint!
   
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.doneBtn.applyGradient(colors: [UIColor.brandPurpleColor.cgColor,UIColor.brandPurpleColor.cgColor], withShadow: false)
        setupDarkLightAppearance(forceLightMode: true)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if ( UIScreen.main.bounds.size.height < 812){
            self.tableViewHeightConstraints.constant = 60
            self.gapConstraints.constant = 0
            self.gapWidthConstraints.constant = -10
            self.gapButtonsConstraints.constant = 5
            self.gapButtons2Constraints.constant = 5
        }
        else if ( UIScreen.main.bounds.size.height >= 812 &&
                  UIScreen.main.bounds.size.height < 844){
            self.gapConstraints.constant = 0
            self.tableViewHeightConstraints.constant = 160
            self.gapButtonsConstraints.constant = 5
            self.gapButtons2Constraints.constant = 5
        }
        getAmenitiesSelected()
        updateButtons()
        
        AnalyticsManager.shared.logScreenView(screenName: ParameterName.OnBoardingAmenities.screen_view)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
       
        updateButtons()
    }

    @IBAction func onBack(_ sender: Any) {
        
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.OnBoardingAmenities.UserClick.back, screenName: ParameterName.OnBoardingAmenities.screen_view)
        self.navigationController?.popViewController(animated: true)
    }
   
    
    func getAmenitiesSelected(){
        let defaults = UserDefaults.standard
        let amenitiesSelected = defaults.stringArray(forKey: "amenitiesSelected") ?? [String]()
        if amenitiesSelected.contains(Amenities.CodingKeys.petrol.rawValue as String){
            amenityPetrol.isHidden = false
        }
        if amenitiesSelected.contains(Amenities.CodingKeys.parks.rawValue as String){
            amenityPark.isHidden = false
        }
        if amenitiesSelected.contains(Amenities.CodingKeys.hawkercentre.rawValue as String){
            amenityHawker.isHidden = false
        }
        if amenitiesSelected.contains(Amenities.CodingKeys.evcharger.rawValue as String){
            amenityCharger.isHidden = false
        }
        if amenitiesSelected.contains(Amenities.CodingKeys.cafe.rawValue as String){
            amenityCafe.isHidden = false
        }
        if amenitiesSelected.contains(Amenities.CodingKeys.library.rawValue as String){
            amenityLibrary.isHidden = false
        }
        if amenitiesSelected.contains(Amenities.CodingKeys.playground.rawValue as String){
            amenityPlayground.isHidden = false
        }
        if amenitiesSelected.contains(Amenities.CodingKeys.restaurant.rawValue as String){
            amenityResto.isHidden = false
        }
        if amenitiesSelected.contains(Amenities.CodingKeys.museum.rawValue as String){
            amenityMuseum.isHidden = false
        }
        if amenitiesSelected.contains(Amenities.CodingKeys.sports.rawValue as String){
            amenitySports.isHidden = false
        }
        if amenitiesSelected.contains(Amenities.CodingKeys.walkingtrail.rawValue as String){
            amenityWalkingtrail.isHidden = false
        }
        if amenitiesSelected.contains(Amenities.CodingKeys.heritagetree.rawValue as String){
            amenityheritage.isHidden = false
        }
        
       
        
      
    }
    
    @IBAction func onDoneBtn(_ sender: Any) {
        print("done click")
        UserDefaults.standard.set(true, forKey: "isOnboardingDone")
       // self.navigationController?.popViewController(animated: false)
        
        
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.OnBoardingAmenities.UserClick.done, screenName: ParameterName.OnBoardingAmenities.screen_view)
        let storyboard = UIStoryboard(name: "MapsLandingVC", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "mainMap") as UIViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }

    @IBAction func petrolBtnClicked(_ sender: Any) {
        self.amenityPetrol.isHidden = !self.amenityPetrol.isHidden
        updateButtons()
    }
    
    @IBAction func parkBtnClicked(_ sender: Any) {
        self.amenityPark.isHidden = !self.amenityPark.isHidden
        updateButtons()
    }
    
    @IBAction func hawkerBtnClicked(_ sender: Any) {
        self.amenityHawker.isHidden = !self.amenityHawker.isHidden
        updateButtons()
    }
    
    @IBAction func chargerBtnClicked(_ sender: Any) {
        self.amenityCharger.isHidden = !self.amenityCharger.isHidden
        updateButtons()
    }
    
    @IBAction func cafeBtnClicked(_ sender: Any) {
        self.amenityCafe.isHidden = !self.amenityCafe.isHidden
        updateButtons()
    }
    
    @IBAction func libraryBtnClicked(_ sender: Any) {
        self.amenityLibrary.isHidden = !self.amenityLibrary.isHidden
        updateButtons()
    }
    
    @IBAction func playgroundBtnClicked(_ sender: Any) {
        self.amenityPlayground.isHidden = !self.amenityPlayground.isHidden
        updateButtons()
    }
    
    @IBAction func restoBtnClicked(_ sender: Any) {
        self.amenityResto.isHidden = !self.amenityResto.isHidden
        updateButtons()
    }
    
    @IBAction func museumBtnClicked(_ sender: Any) {
        self.amenityMuseum.isHidden = !self.amenityMuseum.isHidden
        updateButtons()
    }
    
    @IBAction func sportsBtnClicked(_ sender: Any) {
        self.amenitySports.isHidden = !self.amenitySports.isHidden
        updateButtons()
    }
    
    @IBAction func trailBtnClicked(_ sender: Any) {
        self.amenityWalkingtrail.isHidden = !self.amenityWalkingtrail.isHidden
        updateButtons()
    }
    
    @IBAction func heritageBtnClicked(_ sender: Any) {
        self.amenityheritage.isHidden = !self.amenityheritage.isHidden
        updateButtons()
    }
    
    
    func updateButtons(){
        self.amenityPetrol.isHidden ?  self.petrolBtn.setImage(UIImage.init(named: "petrolUnselected"), for: .normal) :  self.petrolBtn.setImage(UIImage.init(named: "petrolSelected"), for: .normal)
        self.amenityPark.isHidden ?  self.parksBtn.setImage(UIImage.init(named: "parksUnselected"), for: .normal) :  self.parksBtn.setImage(UIImage.init(named: "parksSelected"), for: .normal)
        self.amenityHawker.isHidden ?  self.hawkerBtn.setImage(UIImage.init(named: "hawkerUnselected"), for: .normal) :  self.hawkerBtn.setImage(UIImage.init(named: "hawkerSelected"), for: .normal)
        self.amenityCharger.isHidden ?  self.chargerBtn.setImage(UIImage.init(named: "evChargerUnselected"), for: .normal) :  self.chargerBtn.setImage(UIImage.init(named: "evChargerSelected"), for: .normal)
        self.amenityCafe.isHidden ?  self.cafeBtn.setImage(UIImage.init(named: "cafeUnselected"), for: .normal) :  self.cafeBtn.setImage(UIImage.init(named: "cafeSelected"), for: .normal)
        self.amenityLibrary.isHidden ?  self.libraryBtn.setImage(UIImage.init(named: "libraryUnseelcted"), for: .normal) :  self.libraryBtn.setImage(UIImage.init(named: "librarySelecetd"), for: .normal)
        self.amenityPlayground.isHidden ?  self.playgroundBtn.setImage(UIImage.init(named: "playgroundUnselected"), for: .normal) :  self.playgroundBtn.setImage(UIImage.init(named: "playgroundSelected"), for: .normal)
        self.amenityResto.isHidden ?  self.restoBtn.setImage(UIImage.init(named: "restoUnselected"), for: .normal) :  self.restoBtn.setImage(UIImage.init(named: "restoSelected"), for: .normal)
        self.amenityMuseum.isHidden ?  self.museumBtn.setImage(UIImage.init(named: "museumUnselected"), for: .normal) :  self.museumBtn.setImage(UIImage.init(named: "museumSelected"), for: .normal)
        self.amenitySports.isHidden ?  self.sportsBtn.setImage(UIImage.init(named: "sportsUnselected"), for: .normal) :  self.sportsBtn.setImage(UIImage.init(named: "sportsSelected"), for: .normal)
        self.amenityWalkingtrail.isHidden ?  self.trailBtn.setImage(UIImage.init(named: "walkingUnselected"), for: .normal) :  self.trailBtn.setImage(UIImage.init(named: "walkingSelected"), for: .normal)
        self.amenityheritage.isHidden ?  self.heritageBtn.setImage(UIImage.init(named: "heritageUnselected"), for: .normal) :  self.heritageBtn.setImage(UIImage.init(named: "heritageSelected"), for: .normal)
        
        
        var amenitiesArray = [String]()
        amenitiesArray.append("carpark")
        if (!amenityPetrol.isHidden){
            amenitiesArray.append(Amenities.CodingKeys.petrol.rawValue as String)
        }
        if (!amenityPark.isHidden){
            amenitiesArray.append(Amenities.CodingKeys.parks.rawValue as String)
        }
        if (!amenityHawker.isHidden){
            amenitiesArray.append(Amenities.CodingKeys.hawkercentre.rawValue as String)
        }
        if (!amenityCharger.isHidden){
            amenitiesArray.append(Amenities.CodingKeys.evcharger.rawValue as String)
        }
        if (!amenityCafe.isHidden){
            amenitiesArray.append(Amenities.CodingKeys.cafe.rawValue as String)
        }
        if (!amenityLibrary.isHidden){
            amenitiesArray.append(Amenities.CodingKeys.library.rawValue as String)
        }
        if (!amenityPlayground.isHidden){
            amenitiesArray.append(Amenities.CodingKeys.playground.rawValue as String)
        }
        if (!amenityResto.isHidden){
            amenitiesArray.append(Amenities.CodingKeys.restaurant.rawValue as String)
        }
        if (!amenityMuseum.isHidden){
            amenitiesArray.append(Amenities.CodingKeys.museum.rawValue as String)
        }
        if (!amenitySports.isHidden){
            amenitiesArray.append(Amenities.CodingKeys.sports.rawValue as String)
        }
        if (!amenityWalkingtrail.isHidden){
            amenitiesArray.append(Amenities.CodingKeys.walkingtrail.rawValue as String)
        }
        if (!amenityheritage.isHidden){
            amenitiesArray.append(Amenities.CodingKeys.heritagetree.rawValue as String)
        }
       
        let defaults = UserDefaults.standard
        defaults.set("", forKey: "amenitiesSelected")
        defaults.set(amenitiesArray, forKey: "amenitiesSelected")
        
        
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.OnBoardingAmenities.UserClick.road_amenities_selections, screenName: ParameterName.OnBoardingAmenities.screen_view)
        
    }
}

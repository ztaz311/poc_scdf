//
//  TravelLogViewModel.swift
//  Breeze
//
//  Created by Malou Mendoza on 10/6/21.
//

import Foundation
import Combine

final class TravelLogViewModel {
        
    private var service: TripLogService!
    private var planService: TripPlanService!
    
    @Published private (set) var travellogData = [TripItem]()
    
    @Published private (set) var isFeatureUsed: Bool = true
    
    init(service: TripLogService, tripPlanService: TripPlanService) {
        self.service = service
        self.planService = tripPlanService
    }
    
    public func getTripPlan(_ tripId: Int, completion: @escaping (Result<PlannTripDetails>) -> ()) {
        DispatchQueue.global(qos: .background).async { [weak self] in
            guard let self = self else { return }
            self.planService.getTripPlan(tripId: tripId, completion)
        }
    }

    public func delete(_ tripItem: TripItem, completion: @escaping (Result<Bool>) -> ()) {
        DispatchQueue.global(qos: .background).async { [weak self] in
            guard let self = self else { return }
            self.planService.deleteUpcomingTrip(tripPlannerId: tripItem.tripId, completion)
        }
    }
    
    public func deleteTripHistory(_ tripItem: TripItem, completion: @escaping (Result<Bool>) -> ()) {
        DispatchQueue.global(qos: .background).async { [weak self] in
            guard let self = self else { return }
            self.planService.deleteTripHistory(tripPlannerId: tripItem.tripId, completion)
        }
    }
    
    public func fetchFilteredOBU(_ completion: @escaping (Result<ObuVehicleResponseModel>) -> ()) {
        DispatchQueue.global(qos: .background).async { [weak self] in
            guard let self = self else { return }
            self.service.filteredOBU { result in
                switch result {
                case .success(_):
                    completion(result)
                case .failure(let err):
                    completion(Result.failure(err))
                }
            }
        }
    }
    
    public func fetchTransactions(vehicleNumber: String, pageNumber: Int, pageSize: Int, from: String, to: String, filter: String = "", _ completion: @escaping (Result<TripTransactions>) -> ()) {
        DispatchQueue.global(qos: .background).async { [weak self] in
            guard let self = self else { return }
            self.service.getDataTransactions(vehicleNumber: vehicleNumber, pageNumber: pageNumber, pageSize: pageSize, startDate: from, endDate: to, filter: filter) { result in
                switch result {
                case .success(_):
                    completion(result)
                case .failure(let err):
                    completion(Result.failure(err))
                }
            }
        }
    }
    
    public func fetchTrips(pageNumber: Int, pageSize: Int, from: String, to: String, upcoming: Bool, searchKey: String = "", _ completion: @escaping (Result<TripsData>) -> ()) {
        
        DispatchQueue.global(qos: .background).async { [weak self] in
            guard let self = self else { return }
            
            if upcoming {
                self.isFeatureUsed = true
                self.planService.getUpcomingTrips(pageNumber: pageNumber, pageSize: pageSize, startDate: from, endDate: to, searchKey: searchKey) { [weak self] result in
                    guard let self = self else { return }
                    
                    switch result {
                    case .success(let tripBase):
                        
                        // convert data from UpcomingTripBase to TripsData
                        let trips = tripBase.trips.map { trip in
                            return TripItem(tripId: trip.tripId, tripGUID: "", tripStartTime: trip.tripStartTime, tripEndTime: trip.tripEndTime, tripStartAddress1: trip.tripStartAddress1, tripDestAddress1: trip.tripDestAddress1, totalDistance: trip.totalDistance/1000, totalExpense: trip.totalExpense, tripDuration: trip.totalDuration, tripPlanBy: trip.tripPlanBy)
                        }
                        
                        if tripBase.trips.count == 0 {
                            self.isFeatureUsed = false
                        }
//                        self.isFeatureUsed = tripBase.isFeatureUsed
                        completion(Result.success(TripsData(currentPage: tripBase.currentPage, totalItems: tripBase.totalItems, totalPages: tripBase.totalPages, trips: trips, isFeatureUsed: tripBase.isFeatureUsed)))
                        
                    case .failure(let error):
                        completion(Result.failure(error))
                    }
                }
            } else {
                self.isFeatureUsed = true
                self.service.getHistoryTrips(pageNumber: pageNumber, pageSize: pageSize, startDate: from, endDate: to, searchKey: searchKey) { [weak self] result in
                    guard let self = self else { return }
                    
                    switch result {
                    case .success(let tripBase):
                        
                        // convert data from UpcomingTripBase to TripsData
                        let trips = tripBase.trips.map { trip in
                            return TripItem(tripId: trip.tripId, tripGUID: trip.tripGUID, tripStartTime: trip.tripStartTime, tripEndTime: trip.tripEndTime, tripStartAddress1: trip.tripStartAddress1, tripDestAddress1: trip.tripDestAddress1, totalDistance: trip.totalDistance, totalExpense: trip.totalExpense, tripDuration: 0, tripPlanBy: "")
                        }
                        if tripBase.trips.count == 0 {
                            self.isFeatureUsed = false
                        }
//                        self.isFeatureUsed = tripBase.isFeatureUsed
                        
                        completion(Result.success(TripsData(currentPage: tripBase.currentPage, totalItems: tripBase.totalItems, totalPages: tripBase.totalPages, trips: trips, isFeatureUsed: tripBase.isFeatureUsed)))
                        
                    case .failure(let error):
                        completion(Result.failure(error))
                    }
                }
            }
        }
    }
    
    public func sendEmail(tripIds: [Int], _ completion: @escaping (Result<Bool>) -> () ){
        
        
        DispatchQueue.global(qos: .background).async { [weak self] in
            guard let self = self else { return }
            self.service.sendTripToEmail(tripIds: tripIds,  completion: { result in
                print(result)
                
                switch result {
                case .success(_):
                    completion(Result.success(true))

                    
                case .failure(let error):
                    completion(Result.failure(error))

                }
              
            })
        }
    }
    
    public func sendTripByMonthEmail(month: Int, year: Int, _ completion: @escaping (Result<Bool>) -> () ){
        
        
        DispatchQueue.global(qos: .background).async { [weak self] in
            guard let self = self else { return }
            self.service.sendTripByMonthToEmail(month: month, year: year, completion: { result in
                print(result)
                
                switch result {
                case .success(_):
                    completion(Result.success(true))

                    
                case .failure(let error):
                    completion(Result.failure(error))

                }
              
            })
        }
    }
    
    
    public func updateTripLog(updatedTrip:TripUpdate, tripId: Int,_ completion: @escaping (Result<Bool>) -> ()) {
        
        DispatchQueue.global(qos: .background).async { [weak self] in
            guard let self = self else { return }
            self.service.updateTripLog(updatedTrip: updatedTrip, tripId: tripId,{ result in
                print(result)
                
                switch result {
                case .success(_):
                    completion(Result.success(true))

                    
                case .failure(let error):
                    completion(Result.failure(error))

                }
              
            })
        }
    }
}

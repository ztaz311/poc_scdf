//
//  TripLogSummaryVC.swift
//  Breeze
//
//  Created by Malou Mendoza on 14/6/21.
//

import UIKit
import Combine


protocol TripLogSummaryVCDelegate: AnyObject {
    func refreshTripListOnBack()
    
}

class TripLogSummaryVC: BaseViewController {
    enum Section: Int {
        case header
        case tripSummary
        case erpExpense
        case parkingExpense
        case totalExpense
    }

    weak var delegate: TripLogSummaryVCDelegate?
    @IBOutlet weak var savetBtn: UIButton!
    private var totalExpenses: Double = 0
    private var parkingExpenses: Double = 0
    public var tripId = 0
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var topView: UIView!

    private var dataLoaded = false
    private var viewModel: TripSummaryViewModel!
    private var cancellable: Cancellable?
    private var attachCancellable: Cancellable?
    private var imageLoadCancellable: Cancellable?
    private var imageUploadCancellable: Cancellable?
    private var parkingCost: Double = 0 {
        didSet {
            enableSave()
            // Reload total expense
            self.reloadSection(.totalExpense)
        }
    }
    private var erpCost: Double = 0 {
        didSet {
            enableSave()
            // Reload total expense
            self.reloadSection(.totalExpense)
        }
    }
    /*
    private var parkingImage: UIImage? = nil {
        didSet {
            enableSave()
            DispatchQueue.main.async {
                self.tableView.reloadSections(IndexSet(integersIn: Section.parking.rawValue...Section.parking.rawValue), with: .none)
            }
        }
    }
     */
    
    private var sections: [Section] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initSessions()

        self.savetBtn.isEnabled = false
        self.savetBtn.setTitleColor(UIColor.gray, for: .normal)
        
        topView.showShadow()
        AnalyticsManager.shared.logScreenView(screenName: ParameterName.travellog_summary_screen)
        
//        #if TESTING
//        viewModel = TripSummaryViewModel(service: MockTripLogService(), tripId: 124)
//        #else
        viewModel = TripSummaryViewModel(service: TripLogService(), tripId: tripId)
//        #endif
        
        cancellable = viewModel.$summary
            .receive(on: DispatchQueue.main)
            .sink(receiveValue: { [weak self] summary in
                guard let self = self, !self.dataLoaded else { return }
                
                if summary != nil {
                    self.dataLoaded = true
                }
                
                self.tableView.reloadData()
            })
        
        /*
        attachCancellable = viewModel.$reciptAttachment
            .receive(on: DispatchQueue.main)
            .sink(receiveValue: { [weak self] attachment in
                guard let self = self, let attachment = attachment else { return }
                
                if let data = Data(base64Encoded: attachment.attachmentFile) {
                    self.parkingImage = UIImage(data: data)
                }
            })
        
        imageLoadCancellable = viewModel.$failedToLoadImage
            .receive(on: DispatchQueue.main)
            .sink(receiveValue: { [weak self] failed in
                guard let self = self else { return }
                if failed {
                    showToast(from: self.view, message: "Failed to load parking recipt image", topOffset: 20, icon: "errorIcon", messageColor: .black, duration: 5)
                }
            })
        
        imageUploadCancellable = viewModel.$failedToUploadImage
            .receive(on: DispatchQueue.main)
            .sink(receiveValue: { [weak self] failed in
                guard let self = self else { return }
                if failed {
                    showToast(from: self.view, message: "Failed to upload parking recipt image", topOffset: 20, icon: "errorIcon", messageColor: .black, duration: 5)
                }
            })
         */
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.keyboardDismissMode = .onDrag
        
//        setupDarkLightAppearance(forceLightMode: true)        
    }
    
    private func reloadSection(_ section: Section) {
        if let index = sections.firstIndex(of: section) {
            if index < self.tableView.numberOfSections {
                self.tableView.reloadSections(IndexSet(integersIn: index...index), with: .none)
            }
        }
    }

    deinit {
        NotificationCenter.default.removeObserver(self, name:UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name:UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override var useDynamicTheme: Bool {
        return true
    }
    
    func initSessions() {
        sections.removeAll()
        sections.append(contentsOf: [.header, .tripSummary, .erpExpense, .parkingExpense, .totalExpense])
    }

    func enableSave()
    {
        DispatchQueue.main.async {
            self.savetBtn.isEnabled = true
            self.savetBtn.setTitleColor(UIColor(named:"rpPurpleTextColor")!, for: .normal)
        }
    }
    
    @IBAction func onSave(_ sender: Any) {
        var erps = [ERPUpdate]()
        if let costs = viewModel.summary?.costBreakDowns, !costs.isEmpty {
            for erp in costs {
                let erpToUpdate = ERPUpdate(tripErpId: erp.tripErpId ?? 0, erpId: erp.erpId, actualAmount: erp.actualAmount ?? 0)
                erps.append(erpToUpdate)
                
            }
        }
        let tripUpdate = TripUpdate(totalExpense: self.totalExpenses, parkingExpense: self.parkingExpenses, erp: erps)
        self.viewModel.service.updateTripLog(updatedTrip: tripUpdate, tripId: tripId, { [weak self] result in
            print(result)
            switch result {
            case .success(_):
                DispatchQueue.main.async { [weak self] in
                    self?.delegate?.refreshTripListOnBack()
                    self?.dismiss(animated: true)
//                    self?.navigationController?.popViewController(animated: true)
                }
            case .failure(_):
                print("updating trip failed")
                DispatchQueue.main.async { [weak self] in
                    if let parentView = self?.view {
                        showToast(from: parentView, message: "Save unsuccessful", topOffset: 0, icon: "alertIcon")
                    }
                }
            }
            
        })
    }
    
    @IBAction func onBack(_ sender: Any) {
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.back, screenName: ParameterName.travellog_summary_screen)
        // also need to refresh if it's not saved
        self.delegate?.refreshTripListOnBack()
        self.dismiss(animated: true)
//        self.navigationController?.popViewController(animated: true)
    }
    
    
    // MARK: for keyboard show/hide
    @objc func keyboardWillShow(notification: NSNotification) {
        // only work once
        guard bottomConstraint.constant == 0 else {
            return
        }
        
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            bottomConstraint.constant = keyboardSize.height
            UIView.animate(withDuration: 0.2) {
                self.view.setNeedsLayout()
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        bottomConstraint.constant = 0
        UIView.animate(withDuration: 0.2) {
            self.view.setNeedsLayout()
        }
    }
}

extension TripLogSummaryVC: UITableViewDataSource, UITableViewDelegate {
        
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if let _ = viewModel.summary {
            return sections.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section < sections.count {
            let type = sections[section]
            switch type {
            case .header, .erpExpense, .parkingExpense, .totalExpense:
                return 1
            default:
                if let summary = viewModel.summary {
                    return summary.costBreakDowns.count + 2
                }
            }
        }
        //  Return 2 for start and end location
        return 2
        
//        if section == Section.header.rawValue || section == Section.parking.rawValue {
//            return 1
//        } else {
//            if let summary = viewModel.summary {
//                return summary.costBreakDowns.count + 2
//            }
//        }
//        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return UITableView.automaticDimension
        if indexPath.section < sections.count {
            let type = sections[indexPath.section]
            switch type {
            case .header:
                return UITableView.automaticDimension
            case .erpExpense:
                return 48
            case .parkingExpense:
                return UITableView.automaticDimension
            case .totalExpense:
                return 48
            default:
                if let summary = viewModel.summary {
                    if indexPath.row > 0 && indexPath.row < summary.costBreakDowns.count - 1 {
                        return 54
                    }
                }
                return 75
            }
        }
        return 0.0000001
        
//        if indexPath.section == Section.header.rawValue {
//            return 220
//        } else if indexPath.section == Section.parking.rawValue {
//            return 165
//        } else {
//            if let summary = viewModel.summary {
//                if indexPath.row > 0 && indexPath.row < summary.costBreakDowns.count - 1 {
//                    return 54
//                }
//            }
//            return 75
//        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section < sections.count {
            let type = sections[indexPath.section]
            switch type {
            case .header:
                let header = tableView.dequeueReusableCell(withIdentifier: String(describing: TripSummarySectionHeaderCell.self)) as! TripSummarySectionHeaderCell
                if let summary = viewModel.summary {
                    header.setdata(summary)
//                    setupHeader(header: header, summary: summary)
                }
                return header
            case .erpExpense:
                let totalExpense = tableView.dequeueReusableCell(withIdentifier: String(describing: TripExpenseCell.self)) as! TripExpenseCell
                
                var erp = erpCost
                if erpCost == 0 {
                    if let summary = viewModel.summary, summary.costBreakDowns.count > 0 {
                        erp = self.getTotalERP()
                    }
                }
                totalExpense.setData("ERP expense", unit: " ($)", value: String(format: "%.02f", erp), isTotalExpense: false)
                
                return totalExpense
                
            case .parkingExpense:
                let parkingExpense = tableView.dequeueReusableCell(withIdentifier: String(describing: TripParkExpenseSectionFooter.self)) as! TripParkExpenseSectionFooter
                if let summary = viewModel.summary {
                    parkingExpense.setData(summary, parkingCost: parkingCost)
//                    setupParkingExpense(footer: parkingExpense, summary: summary)
                }
                parkingExpense.onChanged = { [weak self] txt in
                    guard let self = self else { return }
                    if txt.isEmpty {
                        self.parkingCost = 0
                    } else {
                        self.parkingCost = txt.toDouble() ?? 0
                    }
                }
                return parkingExpense
            case .totalExpense:
                let totalExpense = tableView.dequeueReusableCell(withIdentifier: String(describing: TripExpenseCell.self)) as! TripExpenseCell
                
                //  Calculate parking cost
                self.parkingExpenses = parkingCost
                if parkingCost == 0 {
                    if let summary = viewModel.summary {
                        self.parkingExpenses =  summary.parkingExpense
                    }
                }
                
                //  Calculate erp cost
                var erp = erpCost
                if erpCost == 0 {
                    if let summary = viewModel.summary, summary.costBreakDowns.count > 0 {
                        erp = self.getTotalERP()
                    }
                }
                
                //  Total expense
                self.totalExpenses =  self.parkingExpenses + erp
                
                totalExpense.setData("Total expense", unit: " ($)", value: String(format: "%.02f", self.totalExpenses), isTotalExpense: true)
                return totalExpense
                
            default:
                guard let summary = viewModel.summary else {
                    return UITableViewCell()
                }
                return setupERPCell(indexPath: indexPath, summary: summary)
            }
        }
        
        return UITableViewCell()
        
//        if indexPath.section == Section.header.rawValue {
//            let header = tableView.dequeueReusableCell(withIdentifier: String(describing: TripSummarySectionHeaderCell.self)) as! TripSummarySectionHeaderCell
//            if let summary = viewModel.summary {
//                setupHeader(header: header, summary: summary)
//            }
//            return header
//        } else if indexPath.section == Section.parking.rawValue {
//            let footer = tableView.dequeueReusableCell(withIdentifier: String(describing: TripParkExpenseSectionFooter.self)) as! TripParkExpenseSectionFooter
//            if let summary = viewModel.summary {
//                setupParkingExpense(footer: footer, summary: summary)
//            }
//            return footer
//        }
//
//        guard let summary = viewModel.summary else {
//            return UITableViewCell()
//        }
//
//        return setupERPCell(indexPath: indexPath, summary: summary)
    }
    
    /*
    private func setupHeader(header: TripSummarySectionHeaderCell, summary: TripSummary) {
        
        self.parkingExpenses = parkingCost
        if parkingCost == 0 {
           self.parkingExpenses =  summary.parkingExpense
        }
        var erp = erpCost
        if erpCost == 0 {
           if(summary.costBreakDowns.count > 0){
               
               erp = self.getTotalERP()
           }
        }
        self.totalExpenses =  self.parkingExpenses + erp
        header.lblStartDate.text = getDate(timeStamp: summary.startTime)
        header.lblStartEndTime.text = "\(getTime(timeStamp: summary.startTime)) - \(getTime(timeStamp: summary.endTime))"
        header.lblDuration.text = getDuration(seconds: summary.endTime - summary.startTime)
        header.lblTotalExpense.text = String(format: "%.02f",self.totalExpenses)
        header.lblTotoalDistance.text = String(format: "%.02f",summary.totalDistance)
        header.lblERPExpense.text = String(format: "%.02f",erp)
    }
    
    private func setupParkingExpense(footer: TripParkExpenseSectionFooter, summary: TripSummary) {
        footer.txtParkingCost.text = String(format: "%.02f", parkingCost)
        
        if (parkingCost == 0) {
            footer.txtParkingCost.text = String(format: "%.02f",summary.parkingExpense)
        }
        
        footer.onChanged = { [weak self] txt in
            guard let self = self else { return }
            if txt.isEmpty {
                self.parkingCost = 0
            } else {
                self.parkingCost = txt.toDouble() ?? 0
            }
        }
        /*
        footer.onAddPhotoClicked = { [weak self] in
            guard let self = self else { return }
            self.addPhoto()
        }
        footer.onEditOrDeletePhotoClicked = { [weak self] in
            guard let self = self else { return }
            self.editPhoto()
        }
        footer.onPhotoView = { [weak self] in
            guard let self = self else { return }
            if let vc = self.storyboard?.instantiateViewController(identifier: String(describing: ImagePreviewVC.self)) as? ImagePreviewVC {
                vc.image = self.parkingImage
                vc.modalPresentationStyle = .overCurrentContext
                self.present(vc, animated: true, completion: {
                    //No need to handle this closure
                })
            }
        }
        if let image = parkingImage {
            footer.addImage(image)
        } else {
            footer.removeImage()
        }
         */
    }
     
     */
    
    private func setupERPCell(indexPath: IndexPath, summary: TripSummary) -> UITableViewCell {
        if indexPath.row == 0 { // starting address cell
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: TripERPStart.self), for: indexPath) as! TripERPStart
            cell.lblAddress1.text = summary.startAddress1
            cell.lblAddress2.text = summary.startAddress2
            return cell
        } else if indexPath.row == summary.costBreakDowns.count + 1 { // last cell for destination
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: TripERPDestination.self), for: indexPath) as! TripERPDestination
            cell.lblAddress1.text = summary.endAddress1
            cell.lblAddress2.text = summary.endAddress2
            return cell
        }
        
        // ERPs
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: TripERPCell.self), for: indexPath) as! TripERPCell
        var erp = viewModel.summary!.costBreakDowns[indexPath.row - 1] as ERPCost
        
        if let isEditable = erp.isERPEditable, !isEditable {
            cell.txtERPCost.isEnabled = false
            cell.txtERPCost.backgroundColor = .clear
        } else {
            cell.txtERPCost.isEnabled = true
            cell.txtERPCost.backgroundColor =  Asset.Themes.Triplog.tripSummaryTextFieldBGColor.color
        }
        
        cell.lblName.text = erp.name
        cell.txtERPCost.text = String(format: "%.02f",erp.actualAmount ?? "")
        if(erp.cost != erp.actualAmount){
            cell.lblEdited.isHidden = false
        }else{
            cell.lblEdited.isHidden = true
        }
        cell.onChanged = { [weak self] txt in
            guard let self = self else { return }
            cell.txtERPCost.layer.borderColor = UIColor.settingsTintColor.cgColor
            if txt.isEmpty {
                erp.actualAmount = 0
                self.viewModel.summary!.costBreakDowns[indexPath.row - 1] = erp
            } else {

                //check input validation
                let value = txt.toDouble() ?? 0
                if (value > 6.60 || value < 0){
                    cell.lblValidation.isHidden = false
                    cell.txtERPCost.layer.borderColor = UIColor.warningColor.cgColor
                }else{
                    cell.lblValidation.isHidden = true
                    erp.actualAmount = value
                    self.viewModel.summary!.costBreakDowns[indexPath.row - 1] = erp
                    
                }
            }
            if (erp.cost != erp.actualAmount) {
                cell.lblEdited.isHidden = false
            } else {
                cell.lblEdited.isHidden = true
            }
            self.updateTotalERP()
        }
        return cell
    }
    
    
    func getTotalERP()->Double{
        var erpTotal = 0.0
        if let costs = viewModel.summary?.costBreakDowns, !costs.isEmpty {
            for erp in costs {
                print(erp)
                erpTotal += erp.actualAmount ?? 0
            }
        }
        return erpTotal
    }
    
    
    func updateTotalERP(){
        print("update total ERP")
        self.erpCost = self.getTotalERP()
    }
    
    
    /*
    private func getDate(timeStamp: Int) -> String {
        
        let date = Date(timeIntervalSince1970: TimeInterval(timeStamp))
        
        let strDayofWeek = DateUtils.shared.getTimeDisplay(dateFormat: Date.formatEEEE, date: date)
        let strDate = DateUtils.shared.getTimeDisplay(dateFormat: Date.formatDDMMMYYYY, date: date)
        return "\(strDayofWeek), \(strDate)"
    }

    private func getTime(timeStamp: Int) -> String {
        let date = Date(timeIntervalSince1970: TimeInterval(timeStamp))
        return date.getTimeAMPM
    }
    
    private func getDuration(seconds: Int) -> String {
        return seconds.getDurationHaveSpace()
    }
     */
    
    /*
    private func addPhoto() {
        
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.travellog_add_aprking_img, screenName: ParameterName.travellog_summary_screen)
        
        let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let camreaAction = UIAlertAction(title: "Add by taking a photo", style: .default, handler: {
          [weak self] (alert: UIAlertAction!) -> Void in
            guard let self = self else { return }
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                AnalyticsManager.shared.logAddImageEvent(eventValue: ParameterName.travellog_take_photo, screenName: ParameterName.travellog_summary_screen)
                self.addPhoto(from: .camera)
            }
            
        })
        let galleryAction = UIAlertAction(title: "Add from photo gallery", style: .default, handler: {
          [weak self] (alert: UIAlertAction!) -> Void in
            guard let self = self else { return }
            AnalyticsManager.shared.logAddImageEvent(eventValue: ParameterName.travellog_select_from_gallery, screenName: ParameterName.travellog_summary_screen)
            self.addPhoto(from: .photoLibrary)
        })
        
        //
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
          (alert: UIAlertAction!) -> Void in
          // do nothing
            AnalyticsManager.shared.logAddImageEvent(eventValue: ParameterName.cancel, screenName: ParameterName.travellog_summary_screen)
        })
        optionMenu.addAction(camreaAction)
        optionMenu.addAction(galleryAction)
        optionMenu.addAction(cancelAction)
        self.present(optionMenu, animated: true, completion: nil)
    }

    private func editPhoto() {
        let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let camreaAction = UIAlertAction(title: "Replace it by taking a photo", style: .default, handler: {
          [weak self] (alert: UIAlertAction!) -> Void in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                guard let self = self else { return }
                AnalyticsManager.shared.logAddImageEvent(eventValue: ParameterName.travellog_retake_photo, screenName: ParameterName.travellog_summary_screen)
                self.addPhoto(from: .camera)
            }
            
        })
        let galleryAction = UIAlertAction(title: "Replace it from photo gallery", style: .default, handler: {
          [weak self] (alert: UIAlertAction!) -> Void in
            guard let self = self else { return }
            AnalyticsManager.shared.logAddImageEvent(eventValue: ParameterName.travellog_reselect_from_gallery, screenName: ParameterName.travellog_summary_screen)
            self.addPhoto(from: .photoLibrary)
        })

        let deleteAction = UIAlertAction(title: "Delete the receipt", style: .destructive, handler: {
          [weak self] (alert: UIAlertAction!) -> Void in
            guard let self = self else { return }
            AnalyticsManager.shared.logAddImageEvent(eventValue: ParameterName.travellog_delete_receipt, screenName: ParameterName.travellog_summary_screen)
            
            
            AnalyticsManager.shared.logOpenPopupEvent(popupName: ParameterName.travellog_confirm_delete_receipt, screenName: ParameterName.travellog_summary_screen)
            
            self.popupAlert(title: nil, message: "Are yuou sure you want to delete this image?", actionTitles: [Constants.cancel, "Delete" ], actions:[{action1 in
                AnalyticsManager.shared.logClosePopupEvent(popupName: ParameterName.travellog_confirm_delete_receipt, screenName: ParameterName.travellog_summary_screen)
                
            },{ [weak self] action2 in
                
                AnalyticsManager.shared.logClosePopupEvent(popupName: ParameterName.travellog_confirm_delete_receipt, screenName: ParameterName.travellog_summary_screen)
                guard let self = self else { return }
                self.viewModel.deleteImage { result in
                    switch result {
                    case .success(_):
                        self.parkingImage = nil
                    case .failure(let error):
                        print(error)
                    }
                }
                
            }, nil])
        })
        
        //
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
          (alert: UIAlertAction!) -> Void in
          // do nothing
            AnalyticsManager.shared.logAddImageEvent(eventValue: ParameterName.cancel, screenName: ParameterName.travellog_summary_screen)
        })
        optionMenu.addAction(camreaAction)
        optionMenu.addAction(galleryAction)
        optionMenu.addAction(deleteAction)
        optionMenu.addAction(cancelAction)
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    private func addPhoto(from source: UIImagePickerController.SourceType) {
        
        
        let picker = UIImagePickerController()
        picker.allowsEditing = false
        picker.sourceType = source
        picker.delegate = self
        present(picker, animated: true)
    }
    */

    // Second option: user header and footer for section
/*
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableCell(withIdentifier: String(describing: TripSummarySectionHeaderCell.self)) as! TripSummarySectionHeaderCell
        header.lblTotoalDistance.text = viewModel.summary == nil ? "-" : String(format: "%.02f",viewModel.summary!.totalDistance / 1000)
        header.lblERPExpense.text = viewModel.summary == nil ? "-" : String(format: "%.02f",viewModel.summary!.totalCost)
        return header
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 220.0
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 165.0
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footer = tableView.dequeueReusableCell(withIdentifier: String(describing: TripParkExpenseSectionFooter.self)) as! TripParkExpenseSectionFooter
        return footer
    }
*/
}

/*
 extension TripLogSummaryVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
 func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
 guard let image = info[.originalImage] as? UIImage else { return }
 
 let needDelete = self.parkingImage != nil // previously have an image already, need to delete it first
 self.parkingImage = image
 picker.dismiss(animated: true, completion: nil)
 
 if let jpeg = image.jpegData(compressionQuality: 0.8) {
 if needDelete {
 viewModel.deleteImage { _ in
 //No need to handle this result
 }
 }
 // upload
 print(jpeg.base64EncodedString().count)
 viewModel.uploadImage(base64: jpeg.base64EncodedString()) { result in
 switch result {
 case .success(_):
 print("Upload image successfully")
 case .failure(let err):
 print(err)
 }
 }
 }
 
 }
 
 }
 */

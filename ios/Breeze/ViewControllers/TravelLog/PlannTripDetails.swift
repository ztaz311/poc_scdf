//
//  PlannTripDetails.swift
//  Breeze
//
//  Created by Zhou Hao on 6/1/22.
//

import Foundation
import RxSwift

struct RouteStop : Codable {
    let type : String?
    let coordinates : [Double]?
    let amenityId : String?

    enum CodingKeys: String, CodingKey {
        case type = "type"
        case coordinates = "coordinates"
        case amenityId = "amenityId"
    }
    
    init(type: String,coordinates:[Double],amenityId:String){
        
        self.type = type
        self.coordinates = coordinates
        self.amenityId = amenityId
        
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        type = try values.decodeIfPresent(String.self, forKey: .type)
        coordinates = try values.decodeIfPresent([Double].self, forKey: .coordinates)
        amenityId = try values.decodeIfPresent(String.self, forKey: .amenityId)
    }
}

struct PlannTripDetails: Codable {
    let id: Int
    let startTime: TimeInterval
    let arrivalTime: TimeInterval
    let planBy: String
    let startAddress1: String
    let startAddress2: String
    let destAddress1: String
    let destAddress2: String
    let startLat: Double
    let startLong: Double
    let destLat: Double
    let destLong: Double
    let totalDistance: Double
    let totalDuration: Int
    let erpExpense: Double
    let erpEntries: [Int]
    let routeStops: [RouteStop]
    let routeCoordinates: [[Double]]
    let isEtaAvailable: Bool
    let recipientName: String?
    let recipientNumber: String?
    let tripEtaFavouriteId: Int?
    
    enum CodingKeys: String, CodingKey {
        case id = "tripPlannerId"
        case startTime = "tripEstStartTime"
        case arrivalTime = "tripEstArrivalTime"
        case planBy = "tripPlanBy"
        case startAddress1 = "tripStartAddress1"
        case startAddress2 = "tripStartAddress2"
        case destAddress1 = "tripDestAddress1"
        case destAddress2 = "tripDestAddress2"
        case startLat = "tripStartLat"
        case startLong = "tripStartLong"
        case destLat = "tripDestLat"
        case destLong = "tripDestLong"
        case totalDistance
        case totalDuration
        case erpExpense
        case erpEntries
        case routeStops
        case routeCoordinates
        case isEtaAvailable
        case recipientName
        case recipientNumber
        case tripEtaFavouriteId
    }
    
    init(id: Int,startTime:TimeInterval,arrivalTime:TimeInterval,planBy:String,startAddress1:String,startAddress2:String,destAddress1:String,destAddress2:String,startLat:Double,startLong:Double,destLat:Double,destLong:Double,totalDistance:Double,totalDuration:Int,erpExpense:Double,erpEntries:[Int],routeStops:[RouteStop],routeCoordinates:[[Double]],isEtaAvailable:Bool, recipientName:String,recipientNumber:String,tripEtaFavouriteId:Int){
        
        self.id = id
        self.startTime = startTime
        self.arrivalTime = arrivalTime
        self.planBy = planBy
        self.startAddress1 = startAddress1
        self.startAddress2 = startAddress2
        self.destAddress1 = destAddress1
        self.destAddress2 = destAddress2
        self.startLat = startLat
        self.startLong = startLong
        self.destLat = destLat
        self.destLong = destLong
        self.totalDistance = totalDistance
        self.totalDuration = totalDuration
        self.erpExpense = erpExpense
        self.erpEntries = erpEntries
        self.routeStops = routeStops
        self.routeCoordinates = routeCoordinates
        self.isEtaAvailable = isEtaAvailable
        self.recipientName = recipientName
        self.recipientNumber = recipientNumber
        self.tripEtaFavouriteId = tripEtaFavouriteId
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id) ?? 0
        startTime = try values.decodeIfPresent(TimeInterval.self, forKey: .startTime) ?? 0.0
        arrivalTime = try values.decodeIfPresent(TimeInterval.self, forKey: .arrivalTime) ?? 0.0
        planBy = try values.decodeIfPresent(String.self, forKey: .planBy) ?? ""
        startAddress1 = try values.decodeIfPresent(String.self, forKey: .startAddress1) ?? ""
        startAddress2 = try values.decodeIfPresent(String.self, forKey: .startAddress2) ?? ""
        destAddress1 = try values.decodeIfPresent(String.self, forKey: .destAddress1) ?? ""
        destAddress2 = try values.decodeIfPresent(String.self, forKey: .destAddress2) ?? ""
        startLat = try values.decodeIfPresent(Double.self, forKey: .startLat) ?? 0.0
        startLong = try values.decodeIfPresent(Double.self, forKey: .startLong) ?? 0.0
        destLat = try values.decodeIfPresent(Double.self, forKey: .destLat) ?? 0.0
        destLong = try values.decodeIfPresent(Double.self, forKey: .destLong) ?? 0.0
        totalDistance = try values.decodeIfPresent(Double.self, forKey: .totalDistance) ?? 0.0
        totalDuration = try values.decodeIfPresent(Int.self, forKey: .totalDuration)  ?? 0
        erpExpense = try values.decodeIfPresent(Double.self, forKey: .erpExpense) ?? 0.0
        erpEntries = try values.decodeIfPresent([Int].self, forKey: .erpEntries) ?? []
        routeStops = try values.decodeIfPresent([RouteStop].self, forKey: .routeStops) ?? []
        routeCoordinates = try values.decodeIfPresent([[Double]].self, forKey: .routeCoordinates) ?? []
        isEtaAvailable = try values.decodeIfPresent(Bool.self, forKey: .isEtaAvailable) ?? false
        recipientName = try values.decodeIfPresent(String.self, forKey: .recipientName)
        recipientNumber = try values.decodeIfPresent(String.self, forKey: .recipientNumber)
        tripEtaFavouriteId = try values.decodeIfPresent(Int.self, forKey: .tripEtaFavouriteId)
    }

}

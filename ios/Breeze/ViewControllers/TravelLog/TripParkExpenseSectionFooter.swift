//
//  TripParkExpenseSectionFooter.swift
//  Breeze
//
//  Created by Zhou Hao on 14/6/21.
//

import UIKit

class TripParkExpenseSectionFooter: UITableViewCell, UITextFieldDelegate {

    @IBOutlet weak var txtParkingCost: UITextField!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var guideView: UIView!
    @IBOutlet weak var guideLabel: UILabel!
    @IBOutlet weak var guideTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var guideLabelTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var guideLabelBottomConstraint: NSLayoutConstraint!
    
    var onChanged: ((String) -> Void)?

    override func awakeFromNib() {
        super.awakeFromNib()
        txtParkingCost.addTarget(self, action: #selector(textFieldDidChange(_:)),
                                  for: .editingChanged)
        txtParkingCost.delegate = self
        txtParkingCost.layer.borderWidth = 1
        txtParkingCost.layer.borderColor = UIColor.clear.cgColor
        txtParkingCost.clipsToBounds = true
        
        guideView.layer.cornerRadius = 10
        guideView.layer.masksToBounds = true
        guideLabel.text = "ERP/Parking deducted amount not available. Manually input charges. For a better experience, consider pairing and connecting to OBU for all your drives for automatic update of ERP/parking deducted amount."
    }
    
    func setShowGuideText(_ text: String) {
        
        let isShow = !text.isEmpty
        guideView.isHidden = !isShow
        guideTopConstraint.constant = isShow ? 16 : 0
        guideLabelTopConstraint.constant = isShow ? 16 : 0
        guideLabelBottomConstraint.constant = isShow ? 16 : 0
        
        guideLabel.text = isShow ? text : ""
        
        self.setNeedsLayout()
        self.layoutIfNeeded()
    }
    
    func setData(_ summary: TripSummary, parkingCost: Double) {
        
        //  Set data for displaying
        txtParkingCost.text = String(format: "%.02f", parkingCost)
        
        if (parkingCost == 0) {
            txtParkingCost.text = String(format: "%.02f",summary.parkingExpense)
        }
        
        //  Handle logic to show guide
        setShowGuideText(summary.remark ?? "")
        
        //  Handle parking editable
        if let parkingEditable = summary.isParkingEditable, !parkingEditable {
            txtParkingCost.isEnabled = false
        } else {
            txtParkingCost.isEnabled = true
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if let callback = onChanged {
            callback(textField.text ?? "")
        }
    }
    
    // get focus
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        AnalyticsManager.shared.logEditEvent(eventValue: ParameterName.travellog_edit_parking, screenName: ParameterName.travellog_summary_screen)
        
        textField.layer.borderColor = UIColor.brandPurpleColor.cgColor
        if let text = textField.text {
            if !text.isEmpty {
                let value = text.toDouble() ?? 0
                if value == 0 {
                    txtParkingCost.text = "" // clear
                }
            }
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.layer.borderColor = UIColor.clear.cgColor
        if let text = textField.text {
            if text.isEmpty {
                txtParkingCost.text = "0.00"
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let oldText = textField.text, let r = Range(range, in: oldText) else {
            return true
        }

        let newText = oldText.replacingCharacters(in: r, with: string)
        let isNumeric = newText.isEmpty || (Double(newText) != nil)
        let numberOfDots = newText.components(separatedBy: ".").count - 1

        let numberOfDecimalDigits: Int
        if let dotIndex = newText.firstIndex(of: ".") {
            numberOfDecimalDigits = newText.distance(from: dotIndex, to: newText.endIndex) - 1
        } else {
            numberOfDecimalDigits = 0
        }
        
      
        if newText.isEmpty{
            return true
        }
        
        return isNumeric && numberOfDots <= 1 && numberOfDecimalDigits <= 2  && (Double(newText)! <= 999.99)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }    
    
    /*
    func scaleAndCropImage(_ image:UIImage, toSize size: CGSize) -> UIImage {
        // Make sure the image isn't already sized.
        guard !image.size.equalTo(size) else {
            return image
        }

        let widthFactor = size.width / image.size.width
        let heightFactor = size.height / image.size.height
        var scaleFactor: CGFloat = 0.0

        scaleFactor = heightFactor

        if widthFactor > heightFactor {
            scaleFactor = widthFactor
        }

        var thumbnailOrigin = CGPoint.zero
        let scaledWidth  = image.size.width * scaleFactor
        let scaledHeight = image.size.height * scaleFactor

        if widthFactor > heightFactor {
            thumbnailOrigin.y = (size.height - scaledHeight) / 2.0
        }

        else if widthFactor < heightFactor {
            thumbnailOrigin.x = (size.width - scaledWidth) / 2.0
        }

        var thumbnailRect = CGRect.zero
        thumbnailRect.origin = thumbnailOrigin
        thumbnailRect.size.width  = scaledWidth
        thumbnailRect.size.height = scaledHeight

        // Why use `UIGraphicsBeginImageContextWithOptions` over `UIGraphicsBeginImageContext`?
        // see: http://stackoverflow.com/questions/4334233/how-to-capture-uiview-to-uiimage-without-loss-of-quality-on-retina-display#4334902
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
        image.draw(in: thumbnailRect)
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()

        return scaledImage
    }
     */

}

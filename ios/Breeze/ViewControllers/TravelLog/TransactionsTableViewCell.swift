//
//  TransactionsTableViewCell.swift
//  Breeze
//
//  Created by Tan, Tran Ngoc on 20/07/2023.
//

import UIKit


class TransactionsTableViewCell: UITableViewCell {
    
    static let identifier = "TransactionsTableViewCell"
    
    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var roadNameLabel: UILabel!
    @IBOutlet weak var startendTimeLabel: UILabel!
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var topIconConstraints: NSLayoutConstraint!
    @IBOutlet weak var topTextConstraints: NSLayoutConstraint!
    
    @IBOutlet weak var timeTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var priceCenterConstraint: NSLayoutConstraint!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    func setupUI() {
        roadNameLabel.textColor = UIColor(hexString: "#222638", alpha: 1.0)
        roadNameLabel.font = UIFont(name: fontFamilySFPro.Regular, size: 18.0)!
        startendTimeLabel.textColor = UIColor(hexString: "#778188", alpha: 1.0)
        startendTimeLabel.font = UIFont(name: fontFamilySFPro.Light, size: 14.0)!
        dayLabel.textColor = UIColor(hexString: "#778188", alpha: 1.0)
        dayLabel.font = UIFont(name: fontFamilySFPro.Light, size: 14.0)!
        statusLabel.textColor = UIColor(hexString: "#E82370", alpha: 1.0)
        statusLabel.font = UIFont(name: fontFamilySFPro.Regular, size: 16.0)!
        priceLabel.font = UIFont(name: fontFamilySFPro.Light, size: 14.0)!
        priceLabel.textColor = UIColor(hexString: "#E82370", alpha: 1.0)
    }
    
    func setupData(_ model: TransactionModel) {
        if model.type == "parking" {    //  Type is parking/erp
            iconImage.image = UIImage(named: "parkingLotsIcon")
        } else {
            iconImage.image = UIImage(named: "triplog-erpIcon")
        }
        
        let message = model.getMessage()
        statusLabel.isHidden = message.isEmpty
        statusLabel.text = message
        
        if message.isEmpty {
            //  Successful
            priceLabel.textColor = UIColor(hexString: "#222638", alpha: 1.0)
        } else {
            //  Error
            priceLabel.textColor = UIColor(hexString: "#E82370", alpha: 1.0)
        }
        
        if let roadName = model.name, !roadName.isEmpty {
            roadNameLabel.isHidden = false
            roadNameLabel.text = roadName
            timeTopConstraint.constant = 4
            topIconConstraints.constant = 20
            priceCenterConstraint.constant = 0
        } else {
            roadNameLabel.isHidden = true
            roadNameLabel.text = ""
            timeTopConstraint.constant = 0
            if message.isEmpty {
                topIconConstraints.constant = 12
            } else {
                topIconConstraints.constant = 20
            }
            priceCenterConstraint.constant = 12
        }
        
        priceLabel.text = String(format: "$%.2f", model.chargeAmount ?? 0)
        
        if model.type == "parking" {
            if let startTime = model.startTime, let endTime = model.endTime {
                startendTimeLabel.text = "\(convertTime(time: startTime)) - \(convertTime(time: endTime))"
            } else {
                startendTimeLabel.text = convertTime(time: model.chargeTimeStamp ?? 0)
            }
        } else {
            startendTimeLabel.text = convertTime(time: model.chargeTimeStamp ?? 0)
        }
        dayLabel.text = convertDay(time: model.chargeTimeStamp ?? 0)
    }
    
    func convertTime(time: Double) -> String {
        let date = Date(timeIntervalSince1970: TimeInterval(time) / 1000)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm a"
        
        let formattedDate = dateFormatter.string(from: date)
        return formattedDate.lowercased()
    }
    
    func convertDay(time: Double) -> String {
        let date = Date(timeIntervalSince1970: TimeInterval(time) / 1000)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEE, dd MMM"
        
        let formattedDate = dateFormatter.string(from: date)
        return formattedDate
    }
}

//
//  TripSummarySectionHeaderCell.swift
//  Breeze
//
//  Created by Zhou Hao on 14/6/21.
//

import UIKit

class TripSummarySectionHeaderCell: UITableViewCell {

    @IBOutlet weak var lblStartDate: UILabel!
    @IBOutlet weak var lblTotalDistanceTitle: TwoTextLable!
    @IBOutlet weak var lblTotoalDistance: UILabel!    
    @IBOutlet weak var lblStartEndTime: UILabel!
    @IBOutlet weak var lblDuration: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setdata(_ summary: TripSummary) {
        
//        self.parkingExpenses = parkingCost
//        if parkingCost == 0 {
//           self.parkingExpenses =  summary.parkingExpense
//        }
//        var erp = erpCost
//        if erpCost == 0 {
//           if(summary.costBreakDowns.count > 0){
//
//               erp = self.getTotalERP()
//           }
//        }
//        self.totalExpenses =  self.parkingExpenses + erp
        
        lblStartDate.text = getDate(timeStamp: summary.startTime)
        lblStartEndTime.text = "\(getTime(timeStamp: summary.startTime)) - \(getTime(timeStamp: summary.endTime))"
        lblDuration.text = getDuration(seconds: summary.endTime - summary.startTime)
        lblTotoalDistance.text = String(format: "%.02f",summary.totalDistance)
        
        lblTotalDistanceTitle.font = UIFont(name: fontFamilySFPro.Medium, size: 16)
        lblTotalDistanceTitle.font2 = UIFont(name: fontFamilySFPro.Regular, size: 16)
//        lblTotalExpense.text = String(format: "%.02f",self.totalExpenses)
//        lblERPExpense.text = String(format: "%.02f",erp)
    }
    
    private func getDate(timeStamp: Int) -> String {
        let date = Date(timeIntervalSince1970: TimeInterval(timeStamp))
        let strDayofWeek = DateUtils.shared.getTimeDisplay(dateFormat: Date.formatEEEE, date: date)
        let strDate = DateUtils.shared.getTimeDisplay(dateFormat: Date.formatDDMMMYYYY, date: date)
        return "\(strDayofWeek), \(strDate)"
    }

    private func getTime(timeStamp: Int) -> String {
        let date = Date(timeIntervalSince1970: TimeInterval(timeStamp))
        return date.getTimeAMPM
    }
    
    private func getDuration(seconds: Int) -> String {
        return seconds.getDurationHaveSpace()
    }

}

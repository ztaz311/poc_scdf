//
//  TripERPDestination.swift
//  Breeze
//
//  Created by Zhou Hao on 14/6/21.
//

import UIKit

class TripERPDestination: UITableViewCell {
    @IBOutlet weak var lblAddress1: UILabel!
    @IBOutlet weak var lblAddress2: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

//
//  TripExpenseCell.swift
//  Breeze
//
//  Created by Tuan, Pham Hai on 02/10/2023.
//

import Foundation
import UIKit

class TripExpenseCell: UITableViewCell {
    @IBOutlet weak var titleExpense: TwoTextLable!
    @IBOutlet weak var valueExpense: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func setData(_ title: String, unit: String, value: String, isTotalExpense: Bool) {
                
        if isTotalExpense {
            valueExpense.font = UIFont(name: fontFamilySFPro.MaxBold, size: 16)
            titleExpense.font2 = UIFont(name: fontFamilySFPro.Regular, size: 16)
            titleExpense.font = UIFont(name: fontFamilySFPro.MaxBold, size: 16)
        } else {
            valueExpense.font = UIFont(name: fontFamilySFPro.Medium, size: 16)
            titleExpense.font2 = UIFont(name: fontFamilySFPro.Regular, size: 16)
            titleExpense.font = UIFont(name: fontFamilySFPro.Medium, size: 16)
        }
        
        titleExpense.text1 = title
        titleExpense.text2 = unit
        valueExpense.text = value
    }
}

//
//  TravelLogListVC.swift
//  Breeze
//
//  Created by Malou Mendoza on 10/6/21.
//

import UIKit
import Combine
import MonthYearPicker
import SwiftyBeaver

enum TransactionFilteringType: String {
    case all
    case erpOnly
    case parkingOnly
}

class TravelLogListVC: BaseViewController, UITableViewDelegate, UITableViewDataSource,SuccessScreenControllerDelegate {
    
    enum TravelLogViewMode {
        case history
        case transactions
        case upcomming
    }
    
    let headerHeight: CGFloat = 48
    let obuSelectionHeight: CGFloat = 56
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var travelListTableView: UITableView!
   // @IBOutlet weak var SelectBtn: UIButton!
    @IBOutlet weak var emailTripBtn: UIButton!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var bottomViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var loadingActivity: UIActivityIndicatorView!
    @IBOutlet weak var searchBar: TripLogSearchBar!
    @IBOutlet weak var pairView: UIView!
    @IBOutlet weak var pairButton: UIButton! {
        didSet {
            pairButton.layer.cornerRadius = 24
            pairButton.layer.masksToBounds = true
            pairButton.titleLabel?.font = UIFont(name: fontFamilySFPro.Bold, size: 20.0)!
        }
    }
    
    let selectBtn: UIButton = UIButton()
    var headerHeightConstraint: NSLayoutConstraint!
    @IBOutlet var searchHeightConstraint: NSLayoutConstraint!
    // we set a variable to hold the contentOffSet before scroll view scrolls
    var lastContentOffset: CGFloat = 0
    
    var hideBackButton = false
    private var viewModel: TravelLogViewModel!
    private var cancellable: AnyCancellable?
    
    private var cancellableCalendar: AnyCancellable?
    
    private var cancellables: Set<AnyCancellable> = Set<AnyCancellable>()
    
    var monthLabel: String!
    var selectedDate : Date = Date()
    private var fromDate = ""
    private var toDate = ""
    private var fromDateUpcoming = ""
    private var toDateUpcoming = ""
    var fromDateHistory = ""
    var toDateHistory = ""
    var fromDateTransactions = ""
    var toDateTransactions = ""
    
    var filtering = ""  //  "erp" or "parking"
    var defaultSelectedIndex: Int?
    var defaultVehicleNumber: String?
    
    var currentMode: TravelLogViewMode {
        switch segmentControl.selectedSegmentIndex {
            case 0:
                return .history
            case 1:
                return .transactions
            case 2:
                return .upcomming
            default:
                return .history
            }
    }
    
    var isModeComming: Bool {
        return currentMode == .upcomming
    }
    
    var currentpage = 1
    var pageSize = 10
    var totalTrips = 0
    var totalPages = 0
    var isFetchInProgess = false
    var tripsArray : [TripItem] = []
    var transactions : [TransactionModel] = []
    var obusModel: ObuVehicleResponseModel?
    var selectedObu: ObuVehicleDetailModel?
    var selectedTripsToEmailArray : [TripItem] = []
    var isLoading = false
    var isSelectionMode = false
    
    private var transactionFilteringType: TransactionFilteringType = .all
        
    private var isCheckboxAllSelected = false
    private var checkBoxAllBtn:UIButton!
    
    @IBOutlet weak var noDataLabel: UILabel!
    
    @IBOutlet weak var messageView: UIView!
    
    @IBOutlet weak var messageLabel: UILabel!
    
    @IBOutlet weak var filterOptionsView: UIView?
    
    @IBOutlet weak var filterContainerView: UIView?
    
    var filteringView: FilteringView?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        filterContainerView?.roundCorners([.topLeft, .topRight], radius: 20)
        if filtering == "erp" {
            transactionFilteringType = .erpOnly
        } else if filtering == "parking" {
            transactionFilteringType = .parkingOnly
        } else {
            transactionFilteringType = .all
        }
                
        setupSegmentControl()
        searchBar.delegate = self
        selectBtn.isHidden = true
        messageView.isHidden = true
        backBtn.isHidden = hideBackButton
        allowDismissKeyboardWhenTapped()
        
        currentpage = 1
        self.emailTripBtn.isHidden = true
        self.bottomViewHeightConstraint.constant = 0
        self.emailTripBtn.applyGradient(colors: [UIColor.brandPurpleColor.cgColor,UIColor.brandPurpleColor.cgColor], withShadow: false)
        
//        let image =  UIImage(named: "arrowUp")
        self.noDataLabel.isHidden = true
        self.filterOptionsView?.isHidden = true
        
        //for testing
        //viewModel = TravelLogViewModel(service: MockTripLogService())
        viewModel = TravelLogViewModel(service: TripLogService(), tripPlanService: TripPlanService())
        
//        if self.tripsArray.count == 0{
//            self.SelectBtn.isEnabled = false
//        }
        
        setNoDataText()
        
        //  BREEZE2-775 See more erp/parking so need get vehicle from local data first
        
        
        if segmentControl.selectedSegmentIndex == 0 {
            self.fromDate = self.fromDateHistory
            self.toDate = self.toDateHistory
        }
        
        if segmentControl.selectedSegmentIndex == 1 {
            self.searchHeightConstraint.constant = 0
            self.searchBar.isHidden = true
            
            self.fromDate = self.fromDateTransactions
            self.toDate = self.toDateTransactions

            showLoadingIndicator()
            self.fetchFilteredOBU {[weak self] success in
                self?.hideLoadingIndicator()
                self?.fetchTrips()
            }
        } else {
            self.fetchFilteredOBU()
            self.fetchTrips()
        }
        
        self.subscribeData()
        
        self.travelListTableView.tableFooterView = UIView()
        self.travelListTableView.separatorStyle = .none
        if #available(iOS 15.0, *) {
            self.travelListTableView.sectionHeaderTopPadding = 0;
        }
        self.travelListTableView.register(UINib(nibName: "TransactionsTableViewCell", bundle: nil), forCellReuseIdentifier: "TransactionsTableViewCell")
       
        self.loadingActivity.style = UIActivityIndicatorView.Style.large
        self.loadingActivity.backgroundColor = UIColor.clear
        self.view.addSubview(self.loadingActivity)
       
        topView.showBottomShadow()
        setupDarkLightAppearance(forceLightMode: false)
        checkShowObuInstallIfNeeded()
        
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Home.UserClick.trip_log_tab, screenName: ParameterName.Home.screen_view)
        
//        DispatchQueue.main.async {
//            self.callAPITransactions(vehicleNumber: OBUHelper.shared.vehicleNumber)
//        }
    }
/*
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.fetchTrips()
//        #if DEMO
//          if(DemoSession.shared.isDemoRoute){
//            self.backBtn.isHidden = true
//          }else{
//            self.backBtn.isHidden = false
//          }
//        #else
//            self.backBtn.isHidden = false
//        #endif
    }
*/
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        AnalyticsManager.shared.logScreenView(screenName: ParameterName.travellog_screen)
        AnalyticsManager.shared.logScreenView(screenName: ParameterName.TripLog.screen_view)
        
    }
    
    override var useDynamicTheme: Bool {
        return true
    }
    
    private func subscribeData() {
        self.cancellable =  viewModel.$travellogData.sink(receiveValue: { [weak self] (trip) in
            guard let self = self else {
                print("update date")
                
                return }
            
            DispatchQueue.main.async { [self] in
                self.tripsArray.append(contentsOf: self.viewModel.travellogData)
                //                self.loadMoreData()
                self.travelListTableView.reloadData()
            }
            
        })
        
        self.viewModel.$isFeatureUsed.sink { [weak self] value in
            guard let self = self else { return }
            self.checkShowNoDataLabel()
        }.store(in: &cancellables)
    }

    private func setupSegmentControl() {
        let normalTextAttributes: [NSObject : AnyObject] = [
            NSAttributedString.Key.foregroundColor as NSObject: UIColor(named: "tripLogSegmentTextColor")!,
            NSAttributedString.Key.font as NSObject : UIFont.tripLogSegmentTextFont()
        ]

        let selectedTextAttributes: [NSObject : AnyObject] = [
            NSAttributedString.Key.foregroundColor as NSObject : UIColor(named: "tripLogSegmentSelectedTextColor")!,
            NSAttributedString.Key.font as NSObject : UIFont.tripLogSegmentSelectedTextFont(),
        ]

        segmentControl.setTitleTextAttributes(normalTextAttributes as? [NSAttributedString.Key : Any], for: .normal)
        segmentControl.setTitleTextAttributes(selectedTextAttributes as? [NSAttributedString.Key : Any], for: .highlighted)
        segmentControl.setTitleTextAttributes(selectedTextAttributes as? [NSAttributedString.Key : Any], for: .selected)
        
        if let index = defaultSelectedIndex {
            segmentControl.selectedSegmentIndex = index
        }
    }
        
    func resetData() {
        setNoDataText()
        currentpage = 1
        self.emailTripBtn.isHidden = true
        self.pageSize = 10
        self.totalTrips = 0
        self.totalPages = 0
        self.tripsArray = []
        self.transactions = []
        self.selectedTripsToEmailArray = []
        self.isSelectionMode = false
        self.isCheckboxAllSelected = false
        if self.isSelectionMode {
            self.emailTripBtn.isHidden = false
            self.bottomViewHeightConstraint.constant = 138
            self.selectBtn.setTitle("Cancel", for: .normal)
        }else{
            self.emailTripBtn.isHidden = true
            self.selectBtn.setTitle("Select", for: .normal)
            self.bottomViewHeightConstraint.constant = 0
        }
       // self.travelListTableView.reloadData()
    }
    
    func sendUpcomingTripsToRN(hasUpcomingTrip:Bool){
        
        let upcomingTrips = ["hasUpcomingTrips":hasUpcomingTrip]
        
        ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.FN_UPDATE_UPCOMING_TRIPS, data: upcomingTrips as? NSDictionary).subscribe(onSuccess: { result in  },
                    onFailure: { error in
        })
        
    }
    
    private func checkShowNoDataLabel() {
        DispatchQueue.main.async {
            self.noDataLabel.isHidden = self.tripsArray.count != 0 || self.viewModel.isFeatureUsed
        }
    }

    
    func fetchFilteredOBU(completion: ((Bool) -> Void)? = nil) {
        
        let pairedObuList = Prefs.shared.getObuList(AWSAuth.sharedInstance.cognitoId, key: .pairedObuList)
        if !pairedObuList.isEmpty {
            self.selectedObu = nil
            self.obusModel = nil
            
            viewModel.fetchFilteredOBU {[weak self] result in
                guard let self = self else { return }
                switch result {
                case .success(let model):
                    //  Save obus list
                    self.obusModel = model
                                        
                    //  Set default selected OBU
                    if !model.data.isEmpty {
                        
                        if OBUHelper.shared.isObuConnected() {
                            //  Prefer set default is current connected OBU
                            var obuName = OBUHelper.shared.obuName
                            if let defaultVN = self.defaultVehicleNumber, !defaultVN.isEmpty {
                                obuName = defaultVN
                            }
                            
                            for obu in model.data {
                                if obu.vehicleNumber == obuName {
                                    self.selectedObu = obu
                                    break
                                }
                            }
                            
                            if self.selectedObu == nil {
                                self.selectedObu = self.obusModel?.data.first
                            }
                            
                        } else {
                            self.selectedObu = self.obusModel?.data.first
                        }
                    }
                    completion?(true)
                case .failure(_):
                    print("error fetching")
                    completion?(false)
                }
            }
        } else {
            completion?(false)
        }
    }
    
    func fetchTrips(){
        
        self.tripsArray = []
        self.transactions = []
        
        //  Handle fetch transaction list
        if currentMode == .transactions {
            if let obu = selectedObu, let vehicleNumber = obu.vehicleNumber, !vehicleNumber.isEmpty {
                viewModel.fetchTransactions(vehicleNumber: vehicleNumber, pageNumber: currentpage, pageSize: pageSize, from: fromDate, to: toDate, filter: filtering) { [weak self] result in
                    guard let self = self else { return }
                    DispatchQueue.main.async { [self] in
                        self.filteringView?.isHidden = false
                        switch result {
                        case .success(let model):
                            //  Success fetch data
                            self.totalTrips = model.totalItems ?? 0
                            self.totalPages = model.totalPages ?? 0
                            if !model.transactions.isEmpty {
                                self.searchHeightConstraint.constant = 64
                                if self.currentMode == .transactions {
                                    self.messageView.isHidden = false
                                } else {
                                    self.messageView.isHidden = true
                                }
                                
                                self.setupMessageView()
                                self.tripsArray = []
                                self.transactions.append(contentsOf: model.transactions)
                            } else {
                                self.searchHeightConstraint.constant = 0
                                self.messageView.isHidden = true
                            }
                            
                            
                            self.selectBtn.isEnabled = false
                            self.travelListTableView.reloadData()
                            
                            //  Handle show empty data
                            if !model.transactions.isEmpty {
                                self.noDataLabel.isHidden = true
                                let topRow = IndexPath(row: 0, section: 0)
                                if self.travelListTableView.numberOfRows(inSection: 0) > 0 {
                                    self.travelListTableView.scrollToRow(at: topRow, at: .top,animated: true)
                                }
                            } else {
                                self.noDataLabel.isHidden = false
                                self.setNoDataText()
                            }
                            
                            self.isLoading = false
                            self.loadingActivity.stopAnimating()
                            
                        case .failure(_):
                            //  Need handle error
                            self.isLoading = false
                            self.selectBtn.isEnabled = false
                            self.loadingActivity.stopAnimating()
                            print("error fetching")
                            self.setNoDataText()
                            self.travelListTableView.reloadData()
                        }
                    }
                }
            } else {

                //  User can start to pair from here
                self.pairView.isHidden = false
                self.noDataLabel.isHidden = false
                self.filterOptionsView?.isHidden = true
                noDataLabel.text = "no_obu_connection".localed
                
                self.isLoading = false
                self.selectBtn.isEnabled = false
                self.loadingActivity.stopAnimating()
                self.travelListTableView.reloadData()
            }
            
        } else {
            //  Same current implementation
            viewModel.fetchTrips(pageNumber: currentpage, pageSize: pageSize, from: fromDate, to: toDate, upcoming: isModeComming, searchKey: searchBar.text ?? "") { [weak self] result in
                guard let self = self else { return }
                
                switch result {
                case .success(let tripBase):
                    
                    self.totalTrips = tripBase.totalItems
                    self.totalPages = tripBase.totalPages
//                    self.checkShowNoDataLabel()

                    DispatchQueue.main.async {
                        
                        var hasComingTrip = false
                        //self.loadingActivity.stopAnimating()
                        if(tripBase.trips.count > 0) {
                            self.tripsArray.append(contentsOf: tripBase.trips)
                            
                            if(self.isModeComming){
                                
                                for tripItem in self.tripsArray {
                                    
                                    let startDateOftrip = Date(timeIntervalSince1970: TimeInterval(tripItem.tripStartTime))
                                   
                                    let tripStartDateStr = startDateOftrip.dateToStringWithDay()
                                    
                                    let currentDateStr = Date().dateToStringWithDay()
                                    
                                    if(tripStartDateStr == currentDateStr){
                                        
                                        hasComingTrip = true
                                        self.sendUpcomingTripsToRN(hasUpcomingTrip: true)
                                        break
                                    }
                                    
                                }
                            }
                        }
                        
                        if(self.isModeComming){
                            if(hasComingTrip == false){
                                self.sendUpcomingTripsToRN(hasUpcomingTrip: false)
                            }
                        }
                        if self.tripsArray.count == 0 {
                            self.selectBtn.isEnabled = false
                        } else {
                            self.selectBtn.isEnabled = true
                        }
                        
                        
                        
                        print("tripsArray count: \(self.tripsArray.count) self.viewModel.isFeatureUsed: \(self.viewModel.isFeatureUsed)")
                        
                        self.checkShowNoDataLabel()
                        
                        self.travelListTableView.reloadData()
                        
                        if(tripBase.trips.count > 0) {
                            let topRow = IndexPath(row: 0, section: 0)
                            if self.travelListTableView.numberOfRows(inSection: 0) > 0 {
                                self.travelListTableView.scrollToRow(at: topRow, at: .top, animated: true)
                            }
                        }
                        
                        self.isLoading = false
                        self.loadingActivity.stopAnimating()
                    }
                    
                case .failure(_):
                    print("error fetching")
                    self.checkShowNoDataLabel()
                }
            }
        }
    }
        
    func changeEditMode(){
        if let _ = self.view.viewWithTag(1000) {
        } else {
            self.selectedTripsToEmailArray = []
            self.isSelectionMode = !self.isSelectionMode
            self.isCheckboxAllSelected = false
            if self.isSelectionMode{
                self.emailTripBtn.isHidden = false
                self.bottomViewHeightConstraint.constant = 138
                self.selectBtn.setTitle("Cancel", for: .normal)
            }else{
                self.emailTripBtn.isHidden = true
                self.selectBtn.setTitle("Select", for: .normal)
                self.bottomViewHeightConstraint.constant = 0
            }
            
            self.travelListTableView.reloadData()
        }
    }
    
    private func setNoDataText() {
        noDataLabel.numberOfLines = 0
        let mode = currentMode
        switch mode {
        case .history:
            noDataLabel.text = "no_data_history".localed
        case .transactions:
            if (!filtering.isEmpty || (!fromDate.isEmpty && !toDate.isEmpty)) {
                noDataLabel.text = "not_found_transaction".localed
            } else {
                noDataLabel.text = "no_transaction_history".localed
            }
            
        case .upcomming:
            noDataLabel.text = "no_data_upcomming".localed
            
        }
    }
    
    @IBAction func closeFilteringOptions(_ sender: Any) {
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Transactions.UserClick.filter_type_close, screenName: ParameterName.Transactions.screen_view)
        showFilteringOptions(false)
    }
    
    @IBAction func chooseFilteringOptions(_ sender: UIButton) {
        
        showFilteringOptions(false)
        
        var needRefreshData = false
        switch sender.tag {
        case 1001:
            //  Select All
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Transactions.UserClick.filter_type_all, screenName: ParameterName.Transactions.screen_view)
            if self.transactionFilteringType != .all {
                self.transactionFilteringType = .all
                filtering = ""
                needRefreshData = true
            }
        case 1002:
            //  Select ERP only
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Transactions.UserClick.filter_type_erp_only, screenName: ParameterName.Transactions.screen_view)
            if self.transactionFilteringType != .erpOnly {
                self.transactionFilteringType = .erpOnly
                filtering = "erp"
                needRefreshData = true
            }
        case 1003:
            //  Select Parking only
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Transactions.UserClick.filter_type_parking_only, screenName: ParameterName.Transactions.screen_view)
            if self.transactionFilteringType != .parkingOnly {
                self.transactionFilteringType = .parkingOnly
                filtering = "parking"
                needRefreshData = true
            }
        default:
            break
        }
        
        if needRefreshData {
            self.resetData()
            self.fetchTrips()
            self.travelListTableView.reloadData()
        }
    }
    
    @IBAction func startToPair(_ sender: Any) {
        
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Transactions.UserClick.transactions_start_to_pair, screenName: ParameterName.Transactions.screen_view)
        
        navigateToHome(Values.OBU_MODE)
        
        /*
        let storyboard = UIStoryboard(name: "OBU", bundle: nil)
        if let vc = storyboard.instantiateViewController(withIdentifier: "ObuPairStatusViewController") as? ObuPairStatusViewController {
            self.navigationController?.pushViewController(vc, animated: true)
        }
         */
    }
    
    @IBAction func onModeChanged(_ sender: Any) {
//        SelectBtn.isHidden = true
//        if isModeUpcomming {
//            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.TripLog.UserToggle.upcoming, screenName: ParameterName.TripLog.screen_view)
//            self.loadingActivity.startAnimating()
//            self.fromDate = self.fromDateUpcoming
//            self.toDate = self.toDateUpcoming
//
//        } else {
//            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.TripLog.UserToggle.history, screenName: ParameterName.TripLog.screen_view)
//            self.loadingActivity.startAnimating()
//            self.fromDate = self.fromDateHistory
//            self.toDate = self.toDateHistory
//        }
//
////        if isTransactionMode {
////            self.loadingActivity.startAnimating()
////
////        }
//
//        if currentMode == .history {
//
//        }
        
        let mode = currentMode
        switch mode {
        case .upcomming:
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.TripLog.UserToggle.upcoming, screenName: ParameterName.TripLog.screen_view)
            self.filteringView?.isHidden = true
            self.loadingActivity.startAnimating()
            self.searchBar.isHidden = false
            self.pairView.isHidden = true
            self.fromDate = self.fromDateUpcoming
            self.toDate = self.toDateUpcoming            
            searchHeightConstraint.constant = 64
            self.messageView.isHidden = true
            self.view.layoutIfNeeded()
        case .history:
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.TripLog.UserToggle.history, screenName: ParameterName.TripLog.screen_view)
            self.filteringView?.isHidden = true
            self.loadingActivity.startAnimating()
            self.searchBar.isHidden = false
            self.pairView.isHidden = true
            self.fromDate = self.fromDateHistory
            self.toDate = self.toDateHistory
            searchHeightConstraint.constant = 64
            self.messageView.isHidden = true
            self.view.layoutIfNeeded()
        case .transactions:
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Transactions.UserClick.transactions_tab, screenName: ParameterName.Transactions.screen_view)
            self.filteringView?.isHidden = false
            self.loadingActivity.startAnimating()
            self.searchBar.isHidden = true
            self.fromDate = self.fromDateTransactions
            self.toDate = self.toDateTransactions
            self.view.layoutIfNeeded()
        }
        
        self.searchBar.text = ""
        self.resetData()
        self.fetchTrips()
    }
    
        
    @IBAction func onSelect(_ sender: Any) {
        
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.travellog_select, screenName: ParameterName.travellog_screen)
        
        self.changeEditMode()
    }
    
    @objc func onSelectBtnActn() {
        print("select")
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.travellog_select, screenName: ParameterName.travellog_screen)
        
        self.changeEditMode()
    }
    
    @IBAction func onBack(_ sender: Any) {
        
        if defaultSelectedIndex == 1 {
            if filtering == "erp" {
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.DriveTrend.UserClick.total_erp_fee_back, screenName: ParameterName.DriveTrend.screen_view)
            } else if filtering == "parking" {
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.DriveTrend.UserClick.total_parking_fee_back, screenName: ParameterName.DriveTrend.screen_view)
            }
        } else if defaultSelectedIndex == 2 {
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.DriveTrend.UserClick.driving_time_back, screenName: ParameterName.DriveTrend.screen_view)
        } else {
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.TripLog.UserClick.back, screenName: ParameterName.TripLog.screen_view)
        }
        
        self.ShowAndHideFloatingView(value: true)
        self.moveBack()
    }
    
    
    @IBAction func onBackToTop(_ sender: Any) {
        
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.travellog_back_to_top, screenName: ParameterName.settings_account_screen)
        
        if (self.tripsArray.count > 0){
            let topRow = IndexPath(row: 0, section: 0)
            if self.travelListTableView.numberOfRows(inSection: 0) > 0 {
                self.travelListTableView.scrollToRow(at: topRow, at: .top,animated: true)
            }
        }
    }
    
    
    @IBAction func onEmailTravelSummaryAction(_ sender: Any) {
        
        
        if (self.selectedTripsToEmailArray.count > 0){
            //submit trips selected for email
            let tripIds = self.selectedTripsToEmailArray.map { $0.tripId }
            
            //This is required to check if custom email is not entered by user in settings and  provider name is empty
            if(AWSAuth.sharedInstance.customEmail == "" && AWSAuth.sharedInstance.providerName == "")
            {
                let storyboard = UIStoryboard(name: "UserUpdate", bundle: nil)
                if let vc = storyboard.instantiateViewController(withIdentifier: String.init(describing: ChangeViewContainer.self)) as? ChangeViewContainer {
                    vc.emailChangeHeaderName = "Enter email address"
                    self.modalPresentationStyle = .formSheet
                    self.present(vc, animated: true, completion: nil)
                }
            }
            else
            {
                if ( isCheckboxAllSelected){
                    self.viewModel.sendTripByMonthEmail(month: Date().getMonthFromComponents(fromDate: selectedDate), year: Date().getYearFromComponents(fromDate: selectedDate),  { [self] result in
                        print(result)
                        
                        switch result {
                        case .success(_):
                            DispatchQueue.main.async { [self] in
                                
                                self.showConfirmationEmail()
                                self.changeEditMode()
                            }
                        case .failure(_):
                            print("sending email tirp failed")
                        }
                        
                    })
                }else{
                    self.viewModel.sendEmail(tripIds: tripIds,  { [self] result in
                        print(result)
                        
                        switch result {
                        case .success(_):
                            DispatchQueue.main.async { [self] in
                                
                                self.showConfirmationEmail()
                                self.changeEditMode()
                            }
                        case .failure(_):
                            print("sending email tirp failed")
                        }
                        
                    })
                }
            }
        }
        
    }
    
    func  customizeFontStyle(string: String, font: UIFont) -> NSMutableAttributedString {
        return NSMutableAttributedString(string: string, attributes:
                                            [NSAttributedString.Key.font : font ])
    }
    
    func showConfirmationEmail(){
        
        AnalyticsManager.shared.logOpenPopupEvent(popupName: ParameterName.travellog_email_success, screenName: ParameterName.travellog_screen)
        
        
        var summaryTExt = "summaries"
        if (self.selectedTripsToEmailArray.count == 1){
            summaryTExt = "summary"
        }
        let header = String(format:"Travel %@ sent!\n\n\n",summaryTExt)
        var textConfirm = String(format:"%d travel %@ have been sent to your email successfully.",self.selectedTripsToEmailArray.count, summaryTExt)
        if( self.isCheckboxAllSelected){
            textConfirm = String(format:"%d travel %@ have been sent to your email successfully.",self.totalTrips, summaryTExt)
        }
        let attributedString = customizeFontStyle(string: header, font:  UIFont(name: fontFamilySFPro.Medium, size: 26)!)
        attributedString.append(customizeFontStyle(string: textConfirm, font:  UIFont(name: fontFamilySFPro.Regular, size: 18)!))
        
        let childViewController = SuccessViewController()
        childViewController.delegate = self
        childViewController.successText = attributedString
        childViewController.goToLanding = false
        childViewController.successImage = Images.travelSummariesSent
        self.addChildViewControllerWithView(childViewController: childViewController as UIViewController,toView: self.view)
        self.view.endEditing(true)
    }
    
    
    func SuccessScreenControllerDismiss() {
        AnalyticsManager.shared.logClosePopupEvent(popupName: ParameterName.travellog_email_success, screenName: ParameterName.travellog_screen)
        
        let viewControllers:[UIViewController] = self.children
        for viewContoller in viewControllers{
            if viewContoller.isKind(of: SuccessViewController.self){
                viewContoller.willMove(toParent: nil)
                viewContoller.view.removeFromSuperview()
                viewContoller.removeFromParent()                
            }
        }
        
    }
    
    
    func loadMoreData() {
        if !self.isLoading {
            self.isLoading = true
            
            if currentMode == .transactions {
                if (self.transactions.count < self.totalTrips - 1 ){
                    if (self.currentpage < self.totalPages){
                        self.currentpage += 1
                        
                        if let obu = selectedObu, let vehicleNumber = obu.vehicleNumber, !vehicleNumber.isEmpty {
                            self.viewModel.fetchTransactions(vehicleNumber: vehicleNumber, pageNumber: currentpage, pageSize: pageSize, from: fromDate, to: toDate, filter: filtering) { [weak self] result in
                                guard let self = self else { return }
                                
                                switch result {
                                case .success(let model):
                                    
                                    self.totalTrips = model.totalItems ?? 0
                                    
                                    DispatchQueue.main.async { [self] in
                                        if !model.transactions.isEmpty {
                                            self.transactions.append(contentsOf: model.transactions)
                                            self.travelListTableView.reloadData()
                                            self.isLoading = false
                                        }
                                    }
                                case .failure(_):
                                    print("error fetching")
                                    self.isLoading = false
                                }
                            }
                        } else {
                            self.isLoading = false
                        }
                    }
                }
            } else {
                if (self.tripsArray.count < self.totalTrips - 1 ){
                    if ( self.currentpage < self.totalPages ){
                        self.currentpage += 1
                        
                        self.viewModel.fetchTrips(pageNumber: currentpage,
                                                  pageSize: pageSize,
                                                  from: fromDate,
                                                  to: toDate,
                                                  upcoming: isModeComming,
                                                  searchKey: searchBar.text ?? "") { [weak self] result in
                            guard let self = self else { return }
                            
                            switch result {
                            case .success(let tripBase):
                                
                                self.totalTrips = tripBase.totalItems
                                
                                DispatchQueue.main.async { [self] in
                                    
                                    if(tripBase.trips.count > 0)
                                    {
                                        self.tripsArray.append(contentsOf: tripBase.trips)
                                        self.travelListTableView.reloadData()
                                        self.isLoading = false
                                    }
                                }
                                
                            case .failure(_):
                                print("error fetching")
                                
                            }

                        }

                    }
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        if currentMode == .transactions {
            return nil
        }
        
        if (self.isSelectionMode && !self.isModeComming) {
            return nil
        }
        
        // delete action
        let delAction = UIContextualAction(style: .destructive,
                                           title: "Delete") { [weak self] (action, view, completionHandler) in
            guard let self = self else { return }
            self.handleDelete(indexPath.row)
            completionHandler(true)
        }
        delAction.image = UIImage(named: "deleteIcon")
        // if we don't want full swipe
        //        delAction.performsFirstActionWithFullSwipe = false
        delAction.backgroundColor = UIColor.easyBreezyDeleteColor
        
        return UISwipeActionsConfiguration(actions: [delAction])
    }
    
    private func handleDelete(_ row: Int) {
//        self.popupAlert(title: nil, message: Constants.easyBreezyDeleteMessage, actionTitles: [Constants.cancel,Constants.remove], actions:[{action1 in
//            AnalyticsManager.shared.logOpenPopupEvent(popupName: ParameterName.favourites_delete_confirmation_close, screenName: ParameterName.favourites_screen_name)
//        },{[weak self] action2 in
//            AnalyticsManager.shared.logOpenPopupEvent(popupName: ParameterName.favourites_delete_confirmation_close, screenName: ParameterName.favourites_screen_name)
        guard row >= 0 && row < tripsArray.count else { return }
        let tripItem = tripsArray[row]
        if (isModeComming) {
            deleteUpcomingTrip(tripItem, row: row)
        } else {
            deleteTripHistory(tripItem, row: row)
        }
//        }, nil])

    }
    
    private func deleteTripSuccess(_ tripItem: TripItem, row: Int) {
        if (row >= self.tripsArray.count) {
            return
        }
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.TripLog.UserClick.delete, screenName: ParameterName.TripLog.screen_view)
        self.tripsArray.remove(at: row)
        // check if there is any upcoming trip for today
        let todayArray = self.tripsArray.filter { tripItem in
            return tripItem.tripStartTime >= Int(Date().timeIntervalSince1970) && tripItem.tripStartTime <= Int(Date().endOfDay.timeIntervalSince1970)
        }
        if todayArray.count == 0 {
            let _ = ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.FN_UPDATE_UPCOMING_TRIPS, data: ["hasUpcomingTrips":false] as? NSDictionary).subscribe(onSuccess: {_ in },                 onFailure: {_ in })
        }
        self.travelListTableView.deleteRows(at: [IndexPath(row: row, section: 0) ], with: .fade)
    }
    
    private func deleteTripHistory(_ tripItem: TripItem, row: Int) {
        viewModel.deleteTripHistory(tripItem) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(_):
                DispatchQueue.main.async { [weak self] in
                    guard let self = self else { return }
                    self.deleteTripSuccess(tripItem, row: row)
                    if self.tripsArray.count == 0 {
                        self.travelListTableView.reloadData()
                    }
                }
                
            case .failure(let error):
                SwiftyBeaver.error("Failed to deleteUpcomingTrip: \(error.localizedDescription)")
            }
        }
    }
    
    private func deleteUpcomingTrip(_ tripItem: TripItem, row: Int) {
        viewModel.delete(tripItem) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(_):
                DispatchQueue.main.async { [weak self] in
                    guard let self = self else { return }
                    self.deleteTripSuccess(tripItem, row: row)
                    if self.tripsArray.count == 0 {
                        self.travelListTableView.reloadData()
                    }
                }
            case .failure(let error):
                SwiftyBeaver.error("Failed to deleteUpcomingTrip: \(error.localizedDescription)")
            }
        }
    }
    
    func tableView(_ tableView: UITableView,heightForHeaderInSection section: Int) -> CGFloat {
         if currentMode == .transactions {
             if self.selectedObu != nil {
                 return headerHeight + obuSelectionHeight
             } else {
                 return 0.000001
             }
        } else {
            return headerHeight
        }
    }
    
    open func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if currentMode == .transactions {
            
            if let obu = self.selectedObu{
                
                let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: headerHeight + obuSelectionHeight))
                headerView.backgroundColor = UIColor(named: "topViewBGColor")!
                
                //  Add calendar button
                let imageX: CGFloat = self.isSelectionMode ? 64: 24
                let calendarBtn = UIButton.init(frame: CGRect.init(x: imageX, y: 6.5, width: 16, height: 25))
                calendarBtn.setImage(UIImage.init(named: "calendarIcon"), for: .normal)
                calendarBtn.addTarget(self, action: #selector(self.calendarBtnAction), for: .touchUpInside)
                headerView.addSubview(calendarBtn)
                
                //  Add filtering section
                let filterView = FilteringView(frame: CGRect(x: tableView.frame.width - 145, y: 6.5, width: 140, height: 25))
                filterView.setFilteringType(self.transactionFilteringType)
                filterView.delegate = self
                headerView.addSubview(filterView)
                self.filteringView = filterView
                
                //  Setup calendar selection title
                let titleX = 46
                let calendarLabel = UILabel()
                calendarLabel.frame =  CGRect.init(x: titleX,y: 10,width: 240,height: 21)
                calendarLabel.font =  UIFont(name: fontFamilySFPro.Medium, size: 16.0)!
                if fromDate.isEmpty && toDate.isEmpty {
                    calendarLabel.text = "All dates"
                } else {
                    calendarLabel.text = "\(convertDateFormat(dateString: fromDate)) - \(convertDateFormat(dateString: toDate))"
                }
                calendarLabel.textColor = UIColor(named:"rpPurpleTextColor")!
                calendarLabel.isUserInteractionEnabled = true
                let tapGestureRecognizer = UITapGestureRecognizer(target: self,action: #selector(calendarBtnAction))
                calendarLabel.addGestureRecognizer(tapGestureRecognizer)
                headerView.addSubview(calendarLabel)
                
                let pairedVehicleView = UIView(frame: CGRect(x: 0, y: headerHeight, width: tableView.frame.width, height: obuSelectionHeight))
                pairedVehicleView.backgroundColor = UIColor(hexString: "#F5F5F5", alpha: 1.0)
                
                //  Add paired vehicle title
                let titleLabel = UILabel(frame: CGRect(x: 24, y: 12, width: tableView.frame.width - 100, height: 32))
                titleLabel.text = "Paired Vehicle"
                titleLabel.textColor = .black
                titleLabel.font = UIFont.carparkTitleFont()
                pairedVehicleView.addSubview(titleLabel)

                //  Add select OBU button
                let numberVehicalButton = UIButton(frame: CGRect(x: tableView.frame.width - 126 - 24, y: 12, width: 126, height: 32))
                let title = OBUHelper.shared.getLocalVehicleNameToDisplay(obu.vehicleNumber)
                numberVehicalButton.setTitle(title, for: .normal)
                numberVehicalButton.setTitleColor(UIColor(hexString: "#222638", alpha: 1.0), for: .normal)
                numberVehicalButton.titleLabel?.textAlignment = .center
                numberVehicalButton.titleLabel?.numberOfLines = 0
                numberVehicalButton.layer.cornerRadius = 8
                numberVehicalButton.layer.masksToBounds = true
                numberVehicalButton.layer.borderColor = UIColor.gray.cgColor
                numberVehicalButton.titleLabel?.font = UIFont.carparkTypeFont()
                numberVehicalButton.backgroundColor = UIColor(hexString: "#E2E2E2", alpha: 1.0)
                numberVehicalButton.addTarget(self, action: #selector(showChooseVehiclePopup), for: .touchUpInside)
                numberVehicalButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 20)
                pairedVehicleView.addSubview(numberVehicalButton)
                
                let triangleIcon = UIImageView(frame: CGRect(x: tableView.frame.width - 20 - 24, y: 22, width: 12, height: 12))
                triangleIcon.contentMode = .scaleAspectFit
                triangleIcon.image = UIImage(named: "chooseVehicleIcon")
                triangleIcon.isUserInteractionEnabled = false
                pairedVehicleView.addSubview(triangleIcon)
                
                headerView.addSubview(pairedVehicleView)
                
                return headerView
            } else {
                return nil
            }
            
        } else {
            let headerFrame = tableView.frame
            let headerView: UIView = UIView(frame: CGRect.init(x: 0, y: 0, width: headerFrame.size.width, height: headerHeight))
            
            //  Setup calendar button
            let imageX: CGFloat = self.isSelectionMode ? 64: 24
            let calendarBtn = UIButton.init(frame: CGRect.init(x: imageX, y: 6.5, width: 16, height: 25))
            calendarBtn.setImage(UIImage.init(named: "calendarIcon"), for: .normal)
            calendarBtn.addTarget(self, action: #selector(self.calendarBtnAction), for: .touchUpInside)
            headerView.addSubview(calendarBtn)
            
            //  Setup selection mode
            var titleX = 46
            if self.isSelectionMode {
                titleX = 91
                self.checkBoxAllBtn = UIButton.init(frame: CGRect.init(x: 17, y: 6.5, width: 25, height: 25))
                if self.isCheckboxAllSelected{
                    self.checkBoxAllBtn.setImage(UIImage.init(named: "selected"), for: .normal)
                }else{
                    self.checkBoxAllBtn.setImage(UIImage.init(named: "unselected"), for: .normal)
                }
                self.checkBoxAllBtn.addTarget(self, action: #selector(self.selectedAction), for: .touchUpInside)
                headerView.addSubview(self.checkBoxAllBtn)
            }
            
            //  Setup calendar selection title
            let title = UILabel()
            title.frame =  CGRect.init(x: titleX,y: 10,width: 240,height: 21)
            title.font =  UIFont(name: fontFamilySFPro.Medium, size: 16.0)!
            if fromDate.isEmpty && toDate.isEmpty {
                title.text = "All dates"
            } else {
                title.text = "\(convertDateFormat(dateString: fromDate)) - \(convertDateFormat(dateString: toDate))"
            }
            title.textColor = UIColor(named:"rpPurpleTextColor")!
            title.isUserInteractionEnabled = true
            let tapGestureRecognizer = UITapGestureRecognizer(target: self,action: #selector(calendarBtnAction))
            title.addGestureRecognizer(tapGestureRecognizer)
            headerView.addSubview(title)
            
            //  Setup select button
            self.selectBtn.frame =  CGRect.init(x: headerView.frame.size.width - 150,y: title.frame.origin.y - 6,width: 200,height: 32)
            self.selectBtn.setTitle("Select", for: .normal)
            self.selectBtn.titleLabel?.font = (UIFont(name: fontFamilySFPro.Medium, size: 16.0)!)
            self.selectBtn.setTitleColor(UIColor(named:"rpPurpleTextColor")!, for: .normal)
            self.selectBtn.setTitleColor(UIColor.disableSelect, for: .disabled)
            self.selectBtn.addTarget(self, action:#selector(self.onSelectBtnActn), for: .touchUpInside)
            headerView.addSubview(self.selectBtn)
            
            //  Background color
            headerView.backgroundColor = UIColor(named: "topViewBGColor")!
            
            //  Add separator line
            let border = UIView(frame: CGRect.init(x: 0,y: headerHeight ,width: self.view.bounds.width,height: 1))
            border.backgroundColor = UIColor(named:"tripLogLineColor")!
            headerView.addSubview(border)
                                    
            return headerView
        }
    }
    
    @objc func showChooseVehiclePopup() {
        let storyboard: UIStoryboard = UIStoryboard(name: "OBU", bundle: nil)
        
        if let chooseVC = storyboard.instantiateViewController(withIdentifier: "ChooseVehicleViewController") as? ChooseVehicleViewController {
            chooseVC.vehicleList = self.obusModel?.data
            chooseVC.selectedObu = selectedObu
            chooseVC.onCompletion = { [weak self] model in
                if self?.selectedObu?.vehicleNumber != model.vehicleNumber {
                    self?.selectedObu = model
                    //  Reset transaction history
                    self?.resetData()
                    self?.fetchTrips()
                }
            }
            self.addChildViewControllerWithView(childViewController: chooseVC)
        }
    }
    
    func setupSelectBtn()
    {
        self.selectBtn.isHidden = true
        if self.tripsArray.count == 0{
            self.selectBtn.isEnabled = false
        }
        if self.isSelectionMode{
           
            self.selectBtn.setTitle("Cancel", for: .normal)
        }else{
            
            self.selectBtn.setTitle("Select", for: .normal)
            
        }
    }
    
    // Convert string date from GMT + 0 to current timezone
    private func convertDateFormat(dateString: String) -> String {
        if let date = DateUtils.shared.getDateGTM0From(dateString: dateString) {
            return DateUtils.shared.getTimeDisplay(dateFormat: Date.formatDMMMYYYY, date: date)
        }else {
            return ""
        }
    }
    
    @objc func calendarBtnAction(){
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.travellog_multiselect, screenName: ParameterName.travellog_screen)
        if currentMode == .transactions {
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Transactions.UserClick.filter_date, screenName: ParameterName.Transactions.screen_view)
        } else {
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.TripLog.UserClick.date_selector_icon, screenName: ParameterName.TripLog.screen_view)
        }
        
        
        if let _ = self.view.viewWithTag(1000) {
            //viewWithTag.removeFromSuperview()
            
        } else {
            
            let disableFutureDates = !isModeComming
            let data = [
                "toScreen": "DateRangePicker", // Screen name you want to display when start RN
                "navigationParams": [ // Object data to be used by that screen
                    "start_date": fromDate,
                    "end_date": toDate,
                    "disable_future_dates": disableFutureDates
                ]
            ] as [String : Any]
            let rootView = RNViewManager.sharedInstance.viewForModule(
                "Breeze",
                initialProperties: data)

            self.cancellableCalendar = NotificationCenter.default.publisher(for: Notification.Name(Values.NotificationCalendarSelect), object: nil)
                .receive(on: DispatchQueue.main)
                .sink { [weak self] notification in
                    guard let self = self else { return }
//                    self.fromDate = (notification.userInfo?["startDate"] as? String) ?? ""
//                    self.toDate = (notification.userInfo?["endDate"] as? String) ?? ""
                    if (self.isModeComming){
                        self.fromDateUpcoming = (notification.userInfo?["startDate"] as? String) ?? ""
                        self.toDateUpcoming = (notification.userInfo?["endDate"] as? String) ?? ""
                        self.fromDate = self.fromDateUpcoming
                        self.toDate = self.toDateUpcoming
                    } else {
                        if self.segmentControl.selectedSegmentIndex == 1 {
                            self.fromDateTransactions = (notification.userInfo?["startDate"] as? String) ?? ""
                            self.toDateTransactions = (notification.userInfo?["endDate"] as? String) ?? ""
                            self.fromDate = self.fromDateTransactions
                            self.toDate = self.toDateTransactions
                        } else {
                            self.fromDateHistory = (notification.userInfo?["startDate"] as? String) ?? ""
                            self.toDateHistory = (notification.userInfo?["endDate"] as? String) ?? ""
                            self.fromDate = self.fromDateHistory
                            self.toDate = self.toDateHistory
                        }
                    }
                    self.calendarChanged()
                    self.cancellableCalendar?.cancel() // only get one event
                    self.cancellableCalendar = nil
            }
            let reactNativeVC = UIViewController()
            reactNativeVC.view = rootView
            reactNativeVC.modalPresentationStyle = .pageSheet
            self.present(reactNativeVC, animated: true, completion: nil)
        }
    }
    
    @objc func dismissCalendarBtnAction(){
        if let viewWithTag = self.view.viewWithTag(1000) {
            viewWithTag.removeFromSuperview()
        }
        
    }
    
    func calendarChanged() {
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.TripLog.UserClick.date_clearall, screenName: ParameterName.TripLog.screen_view)
        self.isSelectionMode = false
        self.selectBtn.isEnabled = false
        self.resetData()
        self.fetchTrips()
        self.travelListTableView.reloadData()
    }
    
    @objc func dateChanged(_ picker: MonthYearPickerView) {
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.TripLog.UserClick.date_confirm, screenName: ParameterName.TripLog.screen_view)
        self.isSelectionMode = false
        self.selectBtn.isEnabled = false
        print("date changed: \(picker.date)")
        self.selectedDate = picker.date
        self.monthLabel = Date().getMonthName(date:self.selectedDate)
        self.resetData()
        self.fetchTrips()
        self.travelListTableView.reloadData()
    }
    
    
    @objc func selectedAction(){
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.TripLog.UserClick.select, screenName: ParameterName.TripLog.screen_view)
        self.isCheckboxAllSelected = !self.isCheckboxAllSelected
        if self.isCheckboxAllSelected{
            self.selectedTripsToEmailArray = self.tripsArray
            self.checkBoxAllBtn.setImage(UIImage.init(named: "selected"), for: .normal)
        }else{
            self.selectedTripsToEmailArray = []
            self.checkBoxAllBtn.setImage(UIImage.init(named: "unselected"), for: .normal)
        }
        self.travelListTableView.reloadData()
    }
            
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if currentMode == .transactions {
            return self.transactions.count
        } else {
            return self.tripsArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if currentMode == .transactions {
            return UITableView.automaticDimension
        } else {
            return 98.0
        }
    }
        
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        if currentMode == .transactions {
            let cell = tableView.dequeueReusableCell(withIdentifier: TransactionsTableViewCell.identifier) as! TransactionsTableViewCell
            cell.selectionStyle = .none
            cell.setupUI()
            let dataTransactions = self.transactions
            if dataTransactions.count > indexPath.row {
                cell.setupData(dataTransactions[indexPath.row])
            }
            return cell
        } else {
            guard let cell : TravelLogCell = tableView.dequeueReusableCell(withIdentifier: "TravelLogCell") as? TravelLogCell else {
                preconditionFailure("reusable cell not found")
            }
                    
            let tripLog =  self.tripsArray//viewModel.travellogData
            cell.delegate = self
            if (tripLog.count > 0){

                cell.startAddress.text = tripLog[indexPath.row].tripStartAddress1.isEmpty ? "-" : tripLog[indexPath.row].tripStartAddress1
                cell.destinationAddress.text = tripLog[indexPath.row].tripDestAddress1
                cell.distanceTrip.text = String(format: "%.2fkm", ceil(tripLog[indexPath.row].totalDistance*100)/100)
                
                let startDateOftrip = Date(timeIntervalSince1970: TimeInterval(tripLog[indexPath.row].tripStartTime))
                
                let endDateOftrip = Date(timeIntervalSince1970: TimeInterval(tripLog[indexPath.row].tripEndTime))
                
                cell.dateTrip.text = startDateOftrip.dateToStringWithDay()
                cell.timeTrip.text = startDateOftrip.getTimeFromDate() + " - " + endDateOftrip.getTimeFromDate()
                cell.fareTrip.isHidden = true
//                cell.fareTrip.text = String(format: "$%.2f", ceil(tripLog[indexPath.row].totalExpense*100)/100)
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.travellog_view_trip, screenName: ParameterName.travellog_screen)
        
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.TripLog.UserClick.select, screenName: ParameterName.TripLog.screen_view)
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        if isModeComming {
            let trip = tripsArray[indexPath.row]
            goToRoutePlanning(trip: trip)
        } else {
            tableView.deselectRow(at: indexPath, animated: true)
            if !self.isSelectionMode {
                let storyboard: UIStoryboard = UIStoryboard(name: "TravelLog", bundle: nil)
                let vc: TripLogSummaryVC = storyboard.instantiateViewController(withIdentifier: "TripLogSummaryVC") as! TripLogSummaryVC
                vc.delegate = self
                if(indexPath.row < self.tripsArray.count){
                    vc.tripId = self.tripsArray[indexPath.row].tripId
                    //  self.navigationController?.pushViewController(vc, animated: true)
                    self.navigationController?.present(vc, animated: true)
                }
               
            }
        }
        
    }
    
    private func goToRoutePlanning(trip: TripItem) {
        
        // retrieve the trip planner details first
        viewModel.getTripPlan(trip.tripId) { result in
            switch result {
            case .success(let details):
                // show routeplanning
                DispatchQueue.main.async { [weak self] in
                    guard let self = self else { return }
                   
                    let storybard = UIStoryboard(name: "NewRoutePlanning", bundle: nil)
                    if let vc = storybard.instantiateViewController(withIdentifier: String(describing: NewRoutePlanningVC.self)) as? NewRoutePlanningVC {
                        vc.planTrip = details
                        vc.addressReceived = true
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
        
            case .failure(let err):
                SwiftyBeaver.error("Failed to getTripPlan: \(err.localizedDescription)")
            }
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {

        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        if (offsetY > contentHeight - scrollView.frame.height * 4) && !isLoading {
            loadMoreData()
        }
        
//        if (self.travelListTableView.contentOffset.y >= (self.travelListTableView.contentSize.height - self.travelListTableView.frame.size.height) ) && self.travelListTableView.contentOffset.y > 0 && (self.travelListTableView.contentSize.height - self.travelListTableView.frame.size.height) > 0{
//            //you reached end of the table
//            if (self.totalTrips >= self.tripsArray.count && self.tripsArray.count > 0){
//                if ( !self.isSelectionMode){
//                }
//
//            }
//        }
//
//        if (offsetY > 0 && self.travelListTableView.contentSize.height > self.travelListTableView.frame.size.height ){
//            if (self.totalTrips >= self.tripsArray.count && self.tripsArray.count > 0){
//                if (!self.isSelectionMode){
//                }
//            }
//        }

        //move up
        if (self.lastContentOffset > scrollView.contentOffset.y || offsetY < 0 ){
            view.layoutIfNeeded()
            if currentMode != .transactions {
                searchHeightConstraint.constant = 64
                UIView.animate(withDuration: 0.5, delay: 0, options: [.allowUserInteraction], animations: {
                    self.view.layoutIfNeeded()
                }, completion: nil)
            }
            
        }else if (self.lastContentOffset < scrollView.contentOffset.y  || offsetY > 0){ //move down
            
            view.layoutIfNeeded()
            if currentMode == .transactions && !self.transactions.isEmpty {
                searchHeightConstraint.constant = 64
            } else {
                searchHeightConstraint.constant = 0
            }
            
            
            UIView.animate(withDuration: 0.5, delay: 0, options: [.allowUserInteraction], animations: {
                self.view.layoutIfNeeded()
            }, completion: nil)
        }
        else{
            
        }
    }
    
    
    func setupMessageView() {
        messageLabel.text = "Keep OBU connected to Breeze when you pass ERP and carpark gantries to have these transactions automatically recorded."
        messageLabel.font =  UIFont.amenityToolTipsAddressFont()
        messageLabel.textColor = UIColor(hexString: "#778188", alpha: 1.0)
        messageView.backgroundColor = .white
    }
    
    // this delegate is called when the scrollView (i.e your UITableView) will start scrolling
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.lastContentOffset = scrollView.contentOffset.y
    }
    
}


extension TravelLogListVC: TripLogSummaryVCDelegate{
    func refreshTripListOnBack() {
        currentpage = 1
        totalPages = 0
        totalTrips = 0
        //isLoading = false
        self.bottomViewHeightConstraint.constant = 0
        self.fetchTrips()
    }
}

extension TravelLogListVC:TravelLogCellDelegate{
    
    func UnSelectAll() {
        self.isCheckboxAllSelected = false
        if self.isCheckboxAllSelected{
            self.checkBoxAllBtn.setImage(UIImage.init(named: "selected"), for: .normal)
        }else{
            self.checkBoxAllBtn.setImage(UIImage.init(named: "unselected"), for: .normal)
        }
    }
    
    
    func isSeelctedTrip(trip: TripItem) -> Bool{
        var isSelected : Bool = false
        for thisTrip in self.selectedTripsToEmailArray{
            if (thisTrip.tripId == trip.tripId){
               isSelected = true
            }
        }
        return isSelected
    }
    
    func SelectedTripItem(tripIndex: Int, isToBeAdded: Bool){
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.travellog_singleselect, screenName: ParameterName.travellog_screen)
        
        
        let tripSelected = self.tripsArray[tripIndex] as TripItem
        print(self.selectedTripsToEmailArray)
        if isToBeAdded{
            self.selectedTripsToEmailArray.append(tripSelected)
        }else{
            
            let  indexSelected = self.selectedTripsToEmailArray.firstIndex(where: { $0.tripId ==  tripSelected.tripId})
            if indexSelected != NSNotFound{
                self.selectedTripsToEmailArray.remove(at: indexSelected!)
            }
        }
        print(self.selectedTripsToEmailArray)
    }
    
    
    
    
}

extension TravelLogListVC: UISearchBarDelegate {
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.TripLog.UserEdit.triplog_search, screenName: ParameterName.TripLog.screen_view)
        return true
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        resetData()
        fetchTrips()
    }
}

extension TravelLogListVC: FilteringViewDelegate {
    
    func changeFilteringOption() {
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Transactions.UserClick.filter_type, screenName: ParameterName.Transactions.screen_view)
        showFilteringOptions(true)
    }
    
    private func showFilteringOptions(_ isShow: Bool) {
        if isShow {
            filterOptionsView?.alpha = 0
            self.filterOptionsView?.isHidden = false
            UIView.animate(withDuration: Values.standardAnimationDuration) { [weak self] in
                self?.filterOptionsView?.alpha = 1
            }
        } else {
            filterOptionsView?.alpha = 1
            UIView.animate(withDuration: Values.standardAnimationDuration) { [weak self] in
                self?.filterOptionsView?.alpha = 0
                self?.filterOptionsView?.isHidden = true
            }
        }
    }
}



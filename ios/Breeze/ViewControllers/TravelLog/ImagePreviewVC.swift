//
//  ImagePreviewVC.swift
//  Breeze
//
//  Created by Zhou Hao on 16/6/21.
//

import UIKit

class ImagePreviewVC: UIViewController, UIGestureRecognizerDelegate {

    @IBOutlet weak var zoomableImageView: ZoomableImageView!
    
    var image: UIImage!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.gray.withAlphaComponent(0.8)

        AnalyticsManager.shared.logOpenPopupEvent(popupName: ParameterName.travellog_preview_receipt, screenName: ParameterName.travellog_summary_screen)
        
        
        zoomableImageView.setup()
        zoomableImageView.display(image: image)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(onTapGesture(_:)))
        tapGesture.delegate = self
        self.view.addGestureRecognizer(tapGesture)
    }
        
    @objc func onTapGesture(_ recognizer: UITapGestureRecognizer) {
        AnalyticsManager.shared.logClosePopupEvent(popupName: ParameterName.travellog_preview_receipt, screenName: ParameterName.travellog_summary_screen)
        guard recognizer.state == .ended else { return }
        self.dismiss(animated: true, completion: nil)
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        return !zoomableImageView.bounds.contains(touch.location(in: zoomableImageView))
    }
        
}

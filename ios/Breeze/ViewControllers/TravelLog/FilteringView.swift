//
//  FilteringView.swift
//  Breeze
//
//  Created by Tuan, Pham Hai on 01/11/2023.
//

import Foundation
import UIKit

protocol FilteringViewDelegate {
    func changeFilteringOption()
}

class FilteringView: UIView {
    
    @IBOutlet weak var contentView: UIView!
    
    @IBOutlet weak var filteringIcon: UIImageView!
    @IBOutlet weak var filteringLabel: UILabel!
    @IBOutlet weak var filteringButton: UIButton!
    
    var delegate: FilteringViewDelegate?
    var type: TransactionFilteringType?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupView()
    }
    
    @IBAction func onFilteringClicked(_ sender: Any) {
        delegate?.changeFilteringOption()
    }
    
    func setFilteringType(_ type: TransactionFilteringType) {
        self.type = type
        
        switch type {
        case .all:
            filteringLabel.text = "All"
        case .erpOnly:
            filteringLabel.text = "ERP only"
        case .parkingOnly:
            filteringLabel.text = "Parking only"
        }
    }
    
    private func setupView() {
        Bundle.main.loadNibNamed("FilteringView", owner:self, options:nil)
        addSubview(contentView)
        contentView.frame = bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.layer.masksToBounds = false
    }
}

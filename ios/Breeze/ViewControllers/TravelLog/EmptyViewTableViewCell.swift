//
//  EmptyViewTableViewCell.swift
//  Breeze
//
//  Created by Tan, Tran Ngoc on 20/07/2023.
//

import UIKit

class EmptyViewTableViewCell: UITableViewCell {
    
    static let identifier = "EmptyViewTableViewCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

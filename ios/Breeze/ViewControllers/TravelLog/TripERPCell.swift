//
//  TripERPCell.swift
//  Breeze
//
//  Created by Zhou Hao on 14/6/21.
//

import UIKit

class TripERPCell: UITableViewCell, UITextFieldDelegate {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblEdited: UILabel!
    @IBOutlet weak var lblValidation: UILabel!
    @IBOutlet weak var txtERPCost: UITextField!
    var onChanged: ((String) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        txtERPCost.addTarget(self, action: #selector(textFieldDidChange(_:)),
                                  for: .editingChanged)
        txtERPCost.delegate = self
        lblEdited.layer.cornerRadius = 8
        lblEdited.clipsToBounds = true
        txtERPCost.layer.borderWidth = 1
        txtERPCost.layer.borderColor = UIColor.clear.cgColor
        txtERPCost.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @objc func textFieldDidChange(_ textField: UITextField) {
        if let callback = onChanged {
            callback(textField.text ?? "")
        }
    }
    
    // get focus
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        AnalyticsManager.shared.logEditEvent(eventValue: ParameterName.travellog_edit_erp, screenName: ParameterName.travellog_summary_screen)
        
        textField.layer.borderColor = UIColor.brandPurpleColor.cgColor
        if let text = textField.text {
            if !text.isEmpty {
                let value = text.toDouble() ?? 0
                if value == 0 {
                    txtERPCost.text = "" // clear
                }
            }
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.layer.borderColor = UIColor.clear.cgColor
        if let text = textField.text {
            if text.isEmpty {
                txtERPCost.text = "0.00"
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let oldText = textField.text, let r = Range(range, in: oldText) else {
            return true
        }

        let newText = oldText.replacingCharacters(in: r, with: string)
        let isNumeric = newText.isEmpty || (Double(newText) != nil)
        let numberOfDots = newText.components(separatedBy: ".").count - 1

        let numberOfDecimalDigits: Int
        if let dotIndex = newText.firstIndex(of: ".") {
            numberOfDecimalDigits = newText.distance(from: dotIndex, to: newText.endIndex) - 1
        } else {
            numberOfDecimalDigits = 0
        }
        
      
        if newText.isEmpty{
            return true
        }
        return isNumeric && numberOfDots <= 1 && numberOfDecimalDigits <= 2  && (Double(newText)! <= 999.99)
    }
    
}

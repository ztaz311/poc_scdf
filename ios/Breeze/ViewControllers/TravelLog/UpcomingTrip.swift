//
//  UpcomingTrip.swift
//  
//
//  Created by Zhou Hao on 5/1/22.
//

import Foundation

struct UpcomingTripBase : Codable {
    let currentPage : Int
    let totalItems: Int
    let totalPages: Int
    let trips : [UpcomingTrip]
    let isFeatureUsed: Bool
    
    enum CodingKeys: String, CodingKey {
        
        case currentPage = "currentPage"
        case totalItems = "totalItems"
        case totalPages = "totalPages"
        case trips = "trips"
        case isFeatureUsed = "isFeatureUsed"
    }
    
    init(currentPage: Int, totalItems: Int, totalPages: Int, trips: [UpcomingTrip], isFeatureUsed: Bool) {
        self.currentPage = currentPage
        self.totalItems = totalItems
        self.totalPages = totalPages
        self.trips = trips
        self.isFeatureUsed = isFeatureUsed
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        currentPage = try values.decodeIfPresent(Int.self, forKey: .currentPage) ?? 0
        totalItems = try values.decodeIfPresent(Int.self, forKey: .totalItems) ?? 0
        totalPages = try values.decodeIfPresent(Int.self, forKey: .totalPages) ?? 0
        trips = try values.decodeIfPresent([UpcomingTrip].self, forKey: .trips) ?? []
        isFeatureUsed = try values.decodeIfPresent(Bool.self, forKey: .isFeatureUsed) ?? false
    }
}

struct UpcomingTrip : Codable {
    var tripId : Int
    var tripStartTime : Int
    var tripEndTime : Int
    var tripStartAddress1 : String
    var tripDestAddress1 : String
    var totalDistance : Double
    var totalDuration: Int
    var totalExpense : Double
    var tripPlanBy: String
    
    enum CodingKeys: String, CodingKey {
        
        case tripId = "tripPlannerId"
        case tripStartTime = "tripEstStartTime"
        case tripEndTime = "tripEstArrivalTime"
        case tripStartAddress1 = "tripStartAddress1"
        case tripDestAddress1 = "tripDestAddress1"
        case totalDistance = "totalDistance"
        case totalDuration = "totalDuration"
        case totalExpense = "erpExpense"
        case tripPlanBy = "tripPlanBy"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        tripId = try values.decodeIfPresent(Int.self, forKey: .tripId)!
        tripStartTime = try values.decodeIfPresent(Int.self, forKey: .tripStartTime)!
        tripEndTime = try values.decodeIfPresent(Int.self, forKey: .tripEndTime)!
        tripStartAddress1 = try values.decodeIfPresent(String.self, forKey: .tripStartAddress1)!
        tripDestAddress1 = try values.decodeIfPresent(String.self, forKey: .tripDestAddress1)!
        totalDistance = try values.decodeIfPresent(Double.self, forKey: .totalDistance)!
        totalDuration = try values.decodeIfPresent(Int.self, forKey: .totalDuration)!
        totalExpense = try values.decodeIfPresent(Double.self, forKey: .totalExpense)!
        tripPlanBy = try values.decodeIfPresent(String.self, forKey: .tripPlanBy)!
    }
    
}

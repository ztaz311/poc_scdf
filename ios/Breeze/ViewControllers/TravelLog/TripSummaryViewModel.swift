//
//  TripSummaryViewModel.swift
//  Breeze
//
//  Created by Zhou Hao on 14/6/21.
//

import Foundation
import Combine

final class TripSummaryViewModel {
    private (set) var service: TripLogServiceProtocol!
    @Published var summary: TripSummary?
//    @Published var reciptAttachment: AttachmentFile?
//    @Published var failedToLoadImage: Bool = false
//    @Published var failedToUploadImage: Bool = false
    private var tripId: Int
    private static var imageCache = NSCache<AnyObject, AnyObject>()
    
    // Trip id is different from tripGUID
    init(service: TripLogServiceProtocol, tripId: Int) {
        self.service = service
        self.tripId = tripId
        load(tripId: tripId)
    }
    
    private func load(tripId: Int) {
        DispatchQueue.global(qos: .background).async { [weak self] in
            guard let self = self else { return }
            
            self.service.getTripDetails(tripId: tripId) { result in
                switch result {
                case .success(let summary):
                    self.summary = summary
//                    self.loadParkingRecipt(tripId: tripId)
                case .failure(let err):
                    print(err)
                    self.summary = nil
                }
            }
        }
    }
    
    /*
    private func loadParkingRecipt(tripId: Int) {
        DispatchQueue.global(qos: .background).async { [weak self] in
            guard let self = self, let filePath = self.summary?.attachementFilename, !filePath.isEmpty else { return }
                            
            if let base64 = TripSummaryViewModel.imageCache.object(forKey: tripId as AnyObject), let base64String = base64 as? String {
                self.reciptAttachment = AttachmentFile(tripId: tripId, attachmentFile: base64String, fileType: "image/jpeg", filePath: filePath)
                return
            }
            
            self.service.retrieveFile(tripId: tripId, filePath: filePath) { result in
                switch result {
                case .success(let attachment):
                    self.reciptAttachment = attachment
                    TripSummaryViewModel.imageCache.setObject(attachment.attachmentFile as AnyObject, forKey: tripId as AnyObject)
                    self.failedToLoadImage = false
                case .failure(let err):
                    self.failedToLoadImage = true
                    print(err)
                }
            }
        }
    }
    
    func uploadImage(base64: String, fileType: String = "image/jpeg", completion: @escaping (Result<Bool>) -> ()) {
        DispatchQueue.global(qos: .background).async { [weak self] in
            guard let self = self else { return }
            self.service.uploadFile(tripId: self.tripId, base64: base64, fileType: fileType) { result in
                switch result {
                case .success(_):
                    self.failedToUploadImage = false
                    completion(Result.success(true))
                    // save to cache
                    TripSummaryViewModel.imageCache.setObject(base64 as AnyObject, forKey: self.tripId as AnyObject)
                case .failure(let error):
                    self.failedToUploadImage = true
                    completion(Result.failure(error))
                }
            }
        }
    }
    
    func deleteImage(completion: @escaping (Result<Bool>) -> ()) {
        DispatchQueue.global(qos: .background).async { [weak self] in
            guard let self = self else { return }
            
            self.service.deleteFile(tripId: self.tripId) { result in
                switch result {
                case .success(_):
                    completion(.success(true))
                    TripSummaryViewModel.imageCache.removeObject(forKey: self.tripId as AnyObject)
                case .failure(let err):
                    completion(.failure(err))
                }
            }
        }
    }
     
     */
        
    public func updateTripLog(updatedTrip:TripUpdate, tripId: Int,_ completion: @escaping (Result<Bool>) -> ()) {
        DispatchQueue.global(qos: .background).async { [weak self] in
            guard let self = self else { return }
            self.service.updateTripLog(updatedTrip: updatedTrip, tripId: tripId,{ result in
                print(result)
                
                switch result {
                case .success(_):
                    completion(Result.success(true))
                case .failure(let error):
                    completion(Result.failure(error))

                }
              
            })
        }
    }
}

//
//  MaintenanceVC.swift
//  Breeze
//
//  Created by Malou Mendoza on 1/3/21.
//

import UIKit

class MaintenanceVC: UIViewController {

    @IBOutlet var detailLabel: UILabel!
    @IBOutlet var gotItBtn: UIButton!
    var buttonGradient: CAGradientLayer?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        setupGradientColor()
        
        detailLabel.attributedText =
            NSMutableAttributedString()
            .normal("Dear customers, we are doing some essential maintenance work right now, the App service will be available from ")
            .bold("13:00, 2 Feb 2020")
            .normal(", thank you for your understanding. ")
        
        setupDarkLightAppearance(forceLightMode: true)
    }
    
    private func setupGradientColor() {
        buttonGradient?.removeFromSuperlayer()
        buttonGradient = gotItBtn.applyGradient(colors: [UIColor.brandPurpleColor.cgColor, UIColor.brandPurpleColor.cgColor], withShadow: false)
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        guard previousTraitCollection != traitCollection else {
            return
        }
        setupGradientColor()
    }

}

//
//  FeatureNotReadyVC.swift
//  Breeze
//
//  Created by Zhou Hao on 3/3/21.
//

import UIKit

class FeatureNotReadyVC: UIViewController {
    
    @IBOutlet weak var gotItButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        gotItButton.applyGradient(colors: [UIColor.brandPurpleColor.cgColor, UIColor.brandPurpleColor.cgColor], withShadow: false)

        setupDarkLightAppearance(forceLightMode: true)
    }
    
    @IBAction func onGotIt(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}

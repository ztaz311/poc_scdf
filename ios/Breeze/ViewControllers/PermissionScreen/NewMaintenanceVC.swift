//
//  NewMaintenanceVC.swift
//  Breeze
//
//  Created by Tuan, Pham Hai on 05/12/2023.
//

import Foundation
import UIKit

class NewMaintenanceVC: UIViewController {
    @IBOutlet weak var messageLabel: UILabel!
    var message: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        messageLabel.text = message
    }
}

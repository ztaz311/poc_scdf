//
//  PermissionVC.swift
//  Breeze
//
//  Created by Malou Mendoza on 9/2/21.
//

import Foundation
import UIKit


class PermissionVC: UIViewController {
    
    @IBOutlet var gotoSettingsBtn: UIButton!
    @IBOutlet var detailLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.gotoSettingsBtn.applyGradient(colors: [UIColor.brandPurpleColor.cgColor,UIColor.brandPurpleColor.cgColor], withShadow: false)
        
        
        
        detailLabel.attributedText =
            NSMutableAttributedString()
            .normal("1. Open your ")
            .bold("phone settings\n\n")
            .normal("2. Tap ")
            .bold("Breeze\n\n")
            .normal("3. Tap ")
            .bold("Locations\n\n")
            .normal("4. Select ")
            .bold("While Using or Always\n\n")
        
        setupDarkLightAppearance(forceLightMode: true)
    }
    
    @IBAction func gotoSettingActn(){
        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
            return
        }
        
        if UIApplication.shared.canOpenURL(settingsUrl) {
            UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                print("Settings opened: \(success)") // Prints true
            })
        }
    }
    
}

extension NSMutableAttributedString {
    var fontSize:CGFloat { return 18 }
    var boldFont:UIFont { return UIFont(name: "Roboto-Bold", size: fontSize) ?? UIFont.boldSystemFont(ofSize: fontSize) }
    var normalFont:UIFont { return UIFont(name: "Roboto-Regular", size: fontSize) ?? UIFont.systemFont(ofSize: fontSize)}
    
    func bold(_ value:String) -> NSMutableAttributedString {
        
        let attributes:[NSAttributedString.Key : Any] = [
            .font : boldFont
        ]
        
        self.append(NSAttributedString(string: value, attributes:attributes))
        return self
    }
    
    func normal(_ value:String) -> NSMutableAttributedString {
        
        let attributes:[NSAttributedString.Key : Any] = [
            .font : normalFont,
        ]
        
        self.append(NSAttributedString(string: value, attributes:attributes))
        return self
    }
}

//
//  SuccessViewController.swift
//  Breeze
//
//  Created by VishnuKanth on 04/12/20.

//Description: This viewcontroller will be used for to show successview for AccountCreationSuccess, ChangePasswordSuccess, OBU Pair Success(In future). Make use of SuccessControllerDelegate to dismiss the childViewController

/* How to use(display as ChildViewController) SuccessViewController
   
 let childViewController = SuccessViewController()
 childViewController.delegate = self // This is the delegate to dismiss the SuccessViewController
 childViewController.successText = ""
 childViewController.goToLanding = true // This is to pass boolean value, if goToLanding is true or go to some other screen upon dismiss
 childViewController.successImage = UIImage(named:"")
 self.addChildViewControllerWithView(childViewController: childViewController as UIViewController,toView: self.view) // This is the extension of UIViewController declared in CommonExtensions.swift file to display as ChildViewController
 
 */

import UIKit

protocol SuccessScreenControllerDelegate: AnyObject {
    func SuccessScreenControllerDismiss()
}

class SuccessViewController: UIViewController,SuccessViewDelegate {
    
    var successImage = UIImage()
    var successText = NSAttributedString()
    var goToLanding = false

    override func viewDidLoad() {
        super.viewDidLoad()

//        setupDarkLightAppearance(forceLightMode: true)
    }
    
    weak var delegate: SuccessScreenControllerDelegate?
    
    lazy var successView: SuccessView = {
        
        let successView = SuccessView()
        successView.translatesAutoresizingMaskIntoConstraints = false
        successView.delegate = self
        return successView
    }()
    
    override func loadView() {
        super.loadView()
        
        self.view.backgroundColor = UIColor.viewTransparentBackground.withAlphaComponent(0.5)
        view.addSubview(successView)
        
        successView.snp.makeConstraints { make in
            if(self.view.bounds.size.height >= 812)
            {
                if(self.view.bounds.size.height < 844)//iPhone 12/ 12 Pro
                {
                    make.top.bottom.equalTo(self.view).inset(70)
                }
                else
                {
                    make.top.bottom.equalTo(self.view).inset(98)
                }
                
            }
            else
            {
                make.top.bottom.equalTo(self.view).inset(20)
            }
            make.left.right.equalTo(self.view).inset(16)
            
            
        }
        
        
        if(self.view.bounds.size.height < 736)
        {
            successView.okayBtnBottomConstraint.constant = 72
            successView.successTextTopConstraint.constant = 64
        }
        
        else
        {
            if(self.view.bounds.size.height >= 896)
            {
                successView.okayBtnBottomConstraint.constant = 72
                successView.successTextTopConstraint.constant = 64
                successView.successImageTopConstraint.constant = 109
            }
            else
            {
                successView.okayBtnBottomConstraint.constant = 72
                successView.successTextTopConstraint.constant = 64
                successView.successImageTopConstraint.constant = 80
            }
            
        }
        
        
        
        successView.successText = successText
        successView.successImage = successImage
        
    }
    
    // MARK: - SuccessViewDelegate implementation
    
    func SuccessScreenDimiss() {
        
        successView.okayBtn.isEnabled = false
        delegate?.SuccessScreenControllerDismiss()
    }
    
}

//
//  CarPlayStack.swift
//  Breeze
//
//  Created by VishnuKanth on 11/05/21.
//

import Foundation
import UIKit
import MapboxNavigation
#if canImport(CarPlay)
import CarPlay
import MapboxCoreNavigation
import MapboxDirections

extension UIViewController {
    
//    func carPlayMapView() -> UIView? {
//
//        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate,
//              let mapView = appDelegate.carPlayManager.currentNavigator?.mapView  else { return nil }
//
//        return mapView
//    }
//
    func carPlayRootMapView() -> UIView? {

        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate,
              let mapView = appDelegate.carPlayManager.carWindow  else { return nil }

        return mapView
    }
    
    func getRootMapTemplate() -> CPMapTemplate? {
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate,
              let mapTemplate = appDelegate.carPlayManager.interfaceController?.rootTemplate else { return nil }
        
        return mapTemplate as? CPMapTemplate
        
    }
    
    func getRootNavigationMapTemplate() -> CPMapTemplate? {
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate,
              let mapTemplate = appDelegate.navigationRootTemplate else { return nil }
        
        return mapTemplate
        
    }
    

    
    func beginCarPlayNavigation() {
        
        let delegate = UIApplication.shared.delegate as? AppDelegate
        
        if #available(iOS 12.0, *),
            let service = delegate?.navigationController?.navigationService,
            let location = service.router.location {
            delegate?.carPlayManager.beginNavigationWithCarPlay(using: location.coordinate,
                                                                navigationService: service)
        }
    }
    
    func endCarPlayNavigation(canceled: Bool) {
        if #available(iOS 12.0, *), let delegate = UIApplication.shared.delegate as? AppDelegate {
            delegate.carPlayManager.carPlayNavigationViewController?.exitNavigation(byCanceling: canceled)
        }
    }
    
//    func showTrailingNavigationButtons() {
//        if #available(iOS 12.0, *), let delegate = UIApplication.shared.delegate as? AppDelegate {
////            delegate.carPlayManager(delegate.carPlayManager, trailingNavigationBarButtonsCompatibleWith: UITraitCollection, in: CPTemplate, for: <#T##CarPlayActivity#>)
//        }
//    }
}
#endif

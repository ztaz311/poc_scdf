//
//  AlertTemplate.swift
//  Breeze
//
//  Created by VishnuKanth on 21/05/21.
//

import Foundation
import CarPlay
import MapboxNavigation
import MapboxCoreNavigation


public class InformationAlertTemplate{
    
    
    func showGenericAlertTemplate(carPlayManager: CarPlayManager, title: String, buttonText: String) -> CPAlertTemplate{
        var action: CPAlertAction?
        if !buttonText.isEmpty {
            action = CPAlertAction(title: buttonText, style: .default) { (action) in
                if #available(iOS 14.0, *) {
                    carPlayManager.interfaceController?.dismissTemplate(animated: false, completion: { _, _ in
                        //  No-op
                    })
                }else {
                    carPlayManager.interfaceController?.dismissTemplate(animated: false)
                }
            }
        }
        
        let alertTermPlate = CPAlertTemplate(titleVariants: [title,"s"],actions: action != nil ? [action!]:[])
        return alertTermPlate
    }
    
    func showAddDestinationAlertTemplate(carPlayManager: CarPlayManager) -> CPAlertTemplate{
        
        let action = CPAlertAction(title: "Got it", style: .default) { (action) in
            
            if #available(iOS 14.0, *) {
                carPlayManager.interfaceController?.dismissTemplate(animated: false, completion: { _, _ in
                    //  No-op
                })
            }else {
                carPlayManager.interfaceController?.dismissTemplate(animated: false)
            }
            
        }
        
        let text = "Please add the destination on BeBeep Mobile App"
        let alertTermPlate = CPAlertTemplate(titleVariants: [text,"s"],actions: [action])
        return alertTermPlate
    }
    
    func showSearchEmptyView(carPlayManager: CarPlayManager, title: String, buttonText: String) -> CPAlertTemplate{
        var action: CPAlertAction?
        if !buttonText.isEmpty {
            action = CPAlertAction(title: buttonText, style: .default) { (action) in
                if #available(iOS 14.0, *) {
                    carPlayManager.interfaceController?.dismissTemplate(animated: false, completion: { _, _ in
                        //  No-op
                    })
                }else {
                    carPlayManager.interfaceController?.dismissTemplate(animated: false)
                }
            }
        }
        
        let alertTermPlate = CPAlertTemplate(titleVariants: [title,"s"],actions: action != nil ? [action!]:[])
        return alertTermPlate
    }
    
    
    func showNotFoundRouteLocationView(carPlayManager: CarPlayManager, title: String, buttonText: String) -> CPAlertTemplate{
        var action: CPAlertAction?
        AnalyticsManager.shared.logOpenPopupEventV2(popupName: ParameterName.RoutePlanningMode.UserPopup.no_drivable_route, screenName: ParameterName.RoutePlanningMode.screen_view)
        
        if !buttonText.isEmpty {
            action = CPAlertAction(title: buttonText, style: .default) { (action) in
                
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.RoutePlanningMode.UserClick.popup_no_drivable_route_got_it, screenName: ParameterName.RoutePlanningMode.screen_view)
                
                if #available(iOS 14.0, *) {
                    carPlayManager.interfaceController?.dismissTemplate(animated: false, completion: { _, _ in
                        //  No-op
                    })
                }else {
                    carPlayManager.interfaceController?.dismissTemplate(animated: false)
                }
            }
        }
        
        let alertTermPlate = CPAlertTemplate(titleVariants: [title],actions: action != nil ? [action!]:[])
        return alertTermPlate
    }

    func showOBUTemplate(carPlayManager: CarPlayManager, title: String, buttonText: String, isReportIssue: Bool = false) -> CPAlertTemplate{
        var action: CPAlertAction?
        if !buttonText.isEmpty {
            if isReportIssue {
                action = CPAlertAction(title: buttonText, style: .default) { (action) in
                    if #available(iOS 14.0, *) {
                        carPlayManager.interfaceController?.dismissTemplate(animated: false, completion: { _, _ in
                            //  No-op
                        })
                    }else {
                        carPlayManager.interfaceController?.dismissTemplate(animated: false)
                    }
                    appDelegate().mapLandingViewModel?.mobileMapViewUpdatable?.appearInstabug()
                }
            } else {
                // Handler open Instabug on Phone
                action = CPAlertAction(title: buttonText, style: .default) { (action) in
                    if #available(iOS 14.0, *) {
                        carPlayManager.interfaceController?.dismissTemplate(animated: false, completion: { _, _ in
                            //  No-op
                        })
                    }else {
                        carPlayManager.interfaceController?.dismissTemplate(animated: false)
                    }
                    appDelegate().mapLandingViewModel?.mobileMapViewUpdatable?.appearInstabug()
                }
            }
          
        }
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .center
        paragraphStyle.lineBreakMode = .byWordWrapping
        let titleAttributes: [NSAttributedString.Key: Any] = [.paragraphStyle: paragraphStyle]
        
        // Apply the custom text attributes
        let attributedTitle = NSAttributedString(string: title, attributes: titleAttributes)
        
        let alertTermPlate = CPAlertTemplate(titleVariants: [title],actions: action != nil ? [action!]:[])
        return alertTermPlate
    }
    
    
    func showTemplateOBUNotconnected(carPlayManager: CarPlayManager, title: String, buttonText: String) -> CPAlertTemplate{
        var action: CPAlertAction?
        if !buttonText.isEmpty {
            action = CPAlertAction(title: buttonText, style: .default) { (action) in
                if #available(iOS 14.0, *) {
                    carPlayManager.interfaceController?.dismissTemplate(animated: false, completion: { _, _ in
                        appDelegate().mapLandingViewModel?.mobileMapViewUpdatable?.tryConnectedOBU()
                    })
                }else {
                    appDelegate().mapLandingViewModel?.mobileMapViewUpdatable?.tryConnectedOBU()
                    carPlayManager.interfaceController?.dismissTemplate(animated: false)
                }
            }
        }
        
        let alertTermPlate = CPAlertTemplate(titleVariants: [title],actions: action != nil ? [action!]:[])
        return alertTermPlate
    }
}


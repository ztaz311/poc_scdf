//
//  SearchController.swift
//  Breeze
//
//  Created by VishnuKanth on 09/05/21.
//

#if canImport(CarPlay)
import CarPlay
import MapboxDirections
import RxSwift
import Combine

@available(iOS 12.0, *)
public protocol SearchControllerDelegate: AnyObject {
    func searchPreviewRoutes(to waypoint: Waypoint, address:SearchAddresses)
    func resetSearchPanButtons(_ mapTemplate: CPMapTemplate)
    func pushSearchTemplate(_ template: CPTemplate, animated: Bool)
    func popFromSearchTemplate(animated: Bool)
   
}


@available(iOS 12.0, *)
public class SearchController: NSObject {
    /**
     The completion handler that will process the list of search results initiated on CarPlay.
     */
    var searchCompletionHandler: (([CPListItem]) -> Void)?
    
    /**
     Search Result from Search Address API
     */
    var searchResult = [AddressBaseModel]()
    
    /**
     The most recent search results.
     */
    var recentSearchItems: [CPListItem]?
    
    /**
     The most recent search text.
     */
    var recentSearchText: String? {
        didSet {
            publishedSearchText = recentSearchText
        }
    }
    
    
    /**
     The `SearchController` delegate.
     */
    public weak var delegate: SearchControllerDelegate?
    
    
    /**
     The `subscriptionSearch` to manage dispose lastest search subscription if need.
     */
    var subscriptionSearch: Disposable?
    
    
    /**
     The `publishedSearchText` to published search text change
     */
    @Published private(set) var publishedSearchText: String?
    
    
    /**
     The `lastestcompletionHandler` refer to lastest cmopletion handle
     */
    var lastestcompletionHandler: (([CPListItem]) -> Void)?
    
    /**
     The `cancellableSet` to cancel any sink of `publishedSearchText`
     */
    var cancellableSet = Set<AnyCancellable>()
    
    override init() {
        super.init()
        
        $publishedSearchText.debounce(for: .milliseconds(SearchController.MilisecondSearchDebounce), scheduler: RunLoop.main)
            .sink { [weak self] _ in
                guard let self = self else { return }
                self.performSearch()
            }.store(in: &cancellableSet)
    }
}
#else

public class SearchController: NSObject {}
#endif

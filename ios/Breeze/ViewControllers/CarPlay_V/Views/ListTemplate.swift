//
//  ListTemplate.swift
//  Breeze
//
//  Created by Malou Mendoza on 13/10/21.
//

import Foundation
import CarPlay


extension CPListTemplate {
    
    static func recentSearchListTemplate(compatibleWith traitCollection: UITraitCollection,
                                         title: String,
                                         interfaceController: CPInterfaceController?,
                                         listDataArray: [SavedPlaces]) -> CPListTemplate {
        
        var listItems = [CPListItem]()
        for recentItem  in listDataArray {
            if recentItem.placeName == "Home" {
                let item = CPListItem(text: recentItem.placeName, detailText: recentItem.address, image:  UIImage(named: "homeCarplayImg"), showsDisclosureIndicator: true)
                item.userInfo = [SearchController.CarPlayRNSearchAPIKey: recentItem]
                listItems.append(item)
            } else if recentItem.placeName == "Work" {
                let item = CPListItem(text: recentItem.placeName, detailText: recentItem.address, image:  UIImage(named: "workCarplayImg"), showsDisclosureIndicator: true)
                item.userInfo = [SearchController.CarPlayRNSearchAPIKey: recentItem]
                listItems.append(item)
            } else {
                let item = CPListItem(text: recentItem.placeName, detailText: recentItem.address, image:  UIImage(named: "searchList"), showsDisclosureIndicator: true)
                item.userInfo = [SearchController.CarPlayRNSearchAPIKey: recentItem]
                listItems.append(item)
            }
        }
        
        let section = CPListSection(items:listItems)
        let listTemplate = CPListTemplate(title: title, sections: [section])
        
        AnalyticsManager.shared.logScreenView(screenName: ParameterName.CarplayRecentSearch.screen_view)
        
        
        return listTemplate
    }
    
    static func favouriteListTemplate(compatibleWith traitCollection: UITraitCollection,
                                      title: String,
                                      interfaceController: CPInterfaceController?,
                                      listDataArray: [EasyBreezyAddresses]) -> CPListTemplate {
        
        var listItems = [CPListItem]()
        for faveItem  in listDataArray {
            let item = CPListItem(text: faveItem.alias, detailText: faveItem.address1, image:  UIImage(named: "faveList"), showsDisclosureIndicator: true)
            item.userInfo = [SearchController.CarPlayRNSearchAPIKey: faveItem]
            listItems.append(item)
            
        }
        
        let section = CPListSection(items:listItems)
        let listTemplate = CPListTemplate(title: title, sections: [section])
        AnalyticsManager.shared.logScreenView(screenName: ParameterName.CarplayFavourite.screen_view)
        return listTemplate
    }
    
    // TODO: BREEZES-6190: New Search ShortCut For CarPlay
    static func shortcutListTemplate(compatibleWith traitCollection: UITraitCollection,
                                     title: String,
                                     interfaceController: CPInterfaceController?,
                                     listDataArray: [EasyBreezyAddresses]) -> CPListTemplate {
        
        var listItems = [CPListItem]()
        for searchItem  in listDataArray {
            if searchItem.alias == "Home" {
                let item = CPListItem(text: searchItem.alias, detailText: searchItem.address1, image:  UIImage(named: "homeCarplayImg"), showsDisclosureIndicator: true)
                item.userInfo = [SearchController.CarPlayRNSearchAPIKey: searchItem]
                listItems.append(item)
            } else if searchItem.alias == "Work" {
                let item = CPListItem(text: searchItem.alias, detailText: searchItem.address1, image:  UIImage(named: "workCarplayImg"), showsDisclosureIndicator: true)
                item.userInfo = [SearchController.CarPlayRNSearchAPIKey: searchItem]
                listItems.append(item)
            } else {
                let item = CPListItem(text: searchItem.alias, detailText: searchItem.address1, image:  UIImage(named: "shortcutCarplayImg"), showsDisclosureIndicator: true)
                item.userInfo = [SearchController.CarPlayRNSearchAPIKey: searchItem]
                listItems.append(item)
            }
            
        }
        
        let section = CPListSection(items:listItems)
        let listTemplate = CPListTemplate(title: title, sections: [section])
        return listTemplate
    }
    
    
    
    static func etaFavouriteListTemplate(compatibleWith traitCollection: UITraitCollection,
                                         title: String,
                                         interfaceController: CPInterfaceController?,
                                         listDataArray: [Favourites],isNav:Bool = false) -> CPListTemplate {
        
        var listItems = [CPListItem]()
        for faveItem  in listDataArray {
            let item = CPListItem(text: faveItem.recipientName, detailText: isNav ? "" : faveItem.destination, image:  UIImage.generateImageWithTextForCP(text: getAbbreviation(name: faveItem.recipientName!), font: UIFont(name: fontFamilySFPro.Bold, size: 20.0)!), showsDisclosureIndicator: true)
            item.userInfo = [SearchController.CarPlayRNSearchAPIKey: faveItem]
            listItems.append(item)
            
        }
        
        let section = CPListSection(items:listItems)
        let listTemplate = CPListTemplate(title: title, sections: [section])
        return listTemplate
    }
    
    static func recentContactsListTemplate(compatibleWith traitCollection: UITraitCollection,
                                           title: String,
                                           interfaceController: CPInterfaceController?,
                                           listDataArray: [RecentContact]) -> CPListTemplate {
        
        var listItems = [CPListItem]()
        for contact  in listDataArray {
            let item = CPListItem(text: contact.recipientName, detailText: "", image:  UIImage.generateImageWithTextForCP(text: getAbbreviation(name: contact.recipientName!),font: UIFont(name: fontFamilySFPro.Bold, size: 20.0)!), showsDisclosureIndicator: true)
            item.userInfo = contact
            listItems.append(item)
        }
        
        let section = CPListSection(items:listItems)
        let listTemplate = CPListTemplate(title: title, sections: [section])
        //        AnalyticsManager.shared.logScreenView(screenName: ParameterName.CarplayFavourite.screen_view)
        return listTemplate
    }
    
    
    // MARK: List Template For CarPlay
    static func obuConnectedState(compatibleWith traitCollection: UITraitCollection,
                                  title: String,
                                  interfaceController: CPInterfaceController?) -> CPListTemplate {
        let listTemplate = CPListTemplate(title: title, sections: [])
        return listTemplate
    }
    
    static func travelTimeListTemplate(compatibleWith traitCollection: UITraitCollection,
                                       title: String,
                                       interfaceController: CPInterfaceController?,
                                       listDataArray: [TravelTime]) -> CPListTemplate {
        
        //  TODO need sorting base on color: Red/Yellow/Green
        //  Sorting by available lots if same color
        //  Filter out green one
        
        var listItems = [CPListItem]()
        for travelItem in listDataArray {
            
            let item = CPListItem(text: travelItem.location, detailText: travelItem.min, image:  UIImage(named: travelItem.getIconName()), showsDisclosureIndicator: false)
            listItems.append(item)
            if #available(iOS 15.0, *) {
                item.isEnabled = false
            } else {
                // Fallback on earlier versions
            }
        }
        
        let section = CPListSection(items:listItems)
        let listTemplate = CPListTemplate(title: title, sections: [section])
        DispatchQueue.main.asyncAfter(deadline: .now() + 15) {
            appDelegate().popTemplate(animated: true)
        }
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CarPlayObuState.UserClick.dhu_travel_time_back, screenName: ParameterName.CarPlayObuState.dhu_navigation)
        return listTemplate
    }
    
    
    static func parkingListTemplate(compatibleWith traitCollection: UITraitCollection,
                                    title: String,
                                    interfaceController: CPInterfaceController?,
                                    listDataArray: [Parking]) -> CPListTemplate {
        
        var listItems = [CPListItem]()
        
        for parkingItem in listDataArray {
            let item = CPListItem(text: parkingItem.location, detailText: (Int(parkingItem.lots) ?? 0 > 0) ? parkingItem.lots : "Full", image:  UIImage(named: parkingItem.getIconName()), showsDisclosureIndicator: false)
            listItems.append(item)
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CarPlayObuState.UserClick.dhu_parking_availability_select_carpark, screenName: ParameterName.CarPlayObuState.dhu_navigation)
            if #available(iOS 15.0, *) {
                item.isEnabled = false
            } else {
                // Fallback on earlier versions
            }
        }
        
        let section = CPListSection(items:listItems)
        let listTemplate = CPListTemplate(title: title, sections: [section])
        DispatchQueue.main.asyncAfter(deadline: .now() + 15) {
            appDelegate().popTemplate(animated: true)
        }
        
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CarPlayObuState.UserClick.dhu_parking_availability_back, screenName: ParameterName.CarPlayObuState.dhu_navigation)
        return listTemplate
    }
    
}

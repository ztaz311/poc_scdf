//
//  GridTemplate.swift
//  Breeze
//
//  Created by Malou Mendoza on 12/10/21.
//

import Foundation
import CarPlay




extension CPGridTemplate {
    
    private typealias GridButton = (title: String, imageName: String)

    static func showGridTemplate(compatibleWith traitCollection: UITraitCollection,
                                      _ buttonHandler: @escaping (CPGridButton) -> Void) -> CPGridTemplate {
        let butttons = [
            GridButton(title: Constants.cpNewSearch, imageName: "newSearch"),
            GridButton(title: Constants.cpFavourite, imageName: "Favourite"),
            GridButton(title: Constants.cpRecentSearch, imageName: "recentSearches"),
            
           
        ]

        
        
        let gridTemplate = CPGridTemplate(title: Constants.cpAddDestination, gridButtons:
            butttons.map { ( button ) -> CPGridButton in
                return CPGridButton(titleVariants: [button.title],
                                    image: UIImage(named: button.imageName, in: Bundle.main, compatibleWith: traitCollection)!,
                                    handler: buttonHandler)
            })

        return gridTemplate
    }
}




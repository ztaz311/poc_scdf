//
//  SearchController+CPSearchTemplateDelegate.swift
//  Breeze
//
//  Created by VishnuKanth on 09/05/21.
//
import CarPlay
import MapboxDirections
import SwiftyBeaver
extension SearchController: CPSearchTemplateDelegate {
    
    public static let CarPlaySearchAPIKey: String = "SearchAddress"
    public static let CarPlayRNSearchAPIKey: String = "RNSearchAddress"
    
    static let MaximumInitialSearchResults: UInt = 5
    static let MaximumExtendedSearchResults: UInt = 10
    
    static let MinSearchTextCount: UInt = 3
    static let MilisecondSearchDebounce: Int = 300
    
    public func searchTemplate(_ searchTemplate: CPSearchTemplate, updatedSearchText searchText: String, completionHandler: @escaping ([CPListItem]) -> Void) {
        
//        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CarplaySearch.UserEdit.search_input, screenName: ParameterName.CarplaySearch.screen_view)
        
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CarplaySearchPage.UserEdit.textbox_location_input, screenName: ParameterName.CarplaySearch.screen_view)
        
        lastestcompletionHandler = completionHandler
        
        recentSearchText = searchText
    }
    
    public func searchTemplateSearchButtonPressed(_ searchTemplate: CPSearchTemplate) {
        
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CarplaySearch.UserClick.search, screenName: ParameterName.CarplaySearch.screen_view)
        
        guard let items = recentSearchItems else { return }
        let extendedItems = resultsOrNoResults(items, limit: SearchController.MaximumExtendedSearchResults)
        
        let section = CPListSection(items: extendedItems)
        let template = CPListTemplate(title: recentSearchText, sections: [section])
        template.delegate = self
        delegate?.pushSearchTemplate(template, animated: true)
        
    }
    
    public func searchTemplate(_ searchTemplate: CPSearchTemplate, selectedResult item: CPListItem, completionHandler: @escaping () -> Void) {
        
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CarplaySearchPage.UserClick.textbox_result_select, screenName: ParameterName.CarplaySearch.screen_view)
        
        guard let userInfo = item.userInfo as? [String: Any],
              let address = userInfo[SearchController.CarPlaySearchAPIKey] as? AddressBaseModel else {
            completionHandler()
            return
        }

        if(address.source == "GOOGLE" && address.lat == nil)
        {
           // let model = ["placeId":address?.placeId,"source": address?.source, "token": address?.token,"address1":address?.address1,"address2":address?.address2,"distance":address?.distance,"type":address?.type]
            
            let data = ["placeId": address.placeId ?? "", "source": address.source ?? "", "token": address.token ?? ""]
            
            ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.FN_GET_GOOGLE_LAT_LNG_IF_NEED, data: data as NSDictionary)
                .subscribe(onSuccess: { result in
                    print("onSuccess: ", result!["data"] as? NSDictionary)
                    
                    guard let dict = result?["data"] as? NSDictionary,
                          let lat = dict["lat"] as? String,
                          let long = dict["long"] as? String else {
                        return
                    }
                    
                    let model = ["placeId":address.placeId,
                                 "source": address.source,
                                 "token": address.token,
                                 "address1":address.address1,
                                 "address2":address.address2,
                                 "distance":address.distance,
                                 "type":address.type,
                                 "lat":lat,
                                 "long":long]
                    
                    ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.FN_SAVE_RECENT_LIST, data: model as NSDictionary)
                        .subscribe(onSuccess: { result in
                            print("onSuccess: ", result!["data"] as? NSDictionary)
                            
                            let searchAddress = SearchAddresses.init(alias: "", address1: address.address1 ?? "", lat: lat, long: long, address2: address.address2 ?? "", distance: address.distance ?? "")
                            
                            self.goToSearchPreview(address: searchAddress, completionHandler: completionHandler)
                        },
                                   onFailure: { error in
                           
                            let searchAddress = SearchAddresses.init(alias: "", address1: address.address1 ?? "", lat: lat, long: long, address2: address.address2 ?? "", distance: address.distance ?? "")
                            
                            self.goToSearchPreview(address: searchAddress, completionHandler: completionHandler)
                        })
                    
                },
                           onFailure: { error in
                    print("onFailure: ", error)
                })

        }
        else
        {
            let model = ["placeId":address.placeId,
                         "source": address.source,
                         "token": address.token,
                         "address1":address.address1,
                         "address2":address.address2,
                         "distance":address.distance,
                         "type":address.type,
                         "lat":address.lat ?? "",
                         "long":address.long ?? ""]
            
             ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.FN_SAVE_RECENT_LIST, data: model as NSDictionary)
                .subscribe(onSuccess: { result in
                    print("onSuccess: ", result!["data"] as? NSDictionary)
            
              let searchAddress = SearchAddresses.init(alias: "", address1: address.address1 ?? "", lat: address.lat ?? "", long: address.long ?? "", address2: address.address2 ?? "", distance: address.distance ?? "")
            
              self.goToSearchPreview(address: searchAddress, completionHandler: completionHandler)
            },
                           onFailure: { error in
                    
                    print("onFailure: ", error)
                })
            
        }
        
    }
    
    public func searchTemplateButton(searchTemplate: CPSearchTemplate, interfaceController: CPInterfaceController, traitCollection: UITraitCollection) -> CPBarButton {
        let searchTemplateButton = CPBarButton(type: .image) { [weak self] button in
            guard let self = self else { return }
            
            if let mapTemplate = interfaceController.topTemplate as? CPMapTemplate {
                self.delegate?.resetSearchPanButtons(mapTemplate)
            }
            
            self.delegate?.pushSearchTemplate(searchTemplate, animated: false)
        }
        
       
        searchTemplateButton.image = UIImage(named: "search", in: nil, compatibleWith: traitCollection)
        
        return searchTemplateButton
    }
    
    func listItem(searchAddress:AddressBaseModel) -> CPListItem {
        let item = CPListItem(text: searchAddress.address1, detailText: searchAddress.address2, image: nil, showsDisclosureIndicator: true)
        item.userInfo = [SearchController.CarPlaySearchAPIKey: searchAddress]
        return item
    }
    
    func resultsOrNoResults(_ items: [CPListItem], limit: UInt? = nil) -> [CPListItem] {
        
        recentSearchItems = items
        if items.count > 0 {
            if let limit = limit {
                return Array<CPListItem>(items.prefix(Int(limit)))
            }
            
            return items
        } else {
            let title = "Oops we can’t find this location, try another location?"
            let noResult = CPListItem(text: title, detailText: nil, image: nil, showsDisclosureIndicator: false)
            return [noResult]
        }
    }
}

extension SearchController: CPListTemplateDelegate {
    public func listTemplate(_ listTemplate: CPListTemplate, didSelect item: CPListItem, completionHandler: @escaping () -> Void) {
        
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CarplaySearch.UserClick.select_destination, screenName: ParameterName.CarplaySearch.screen_view)
        guard let userInfo = item.userInfo as? [String: Any],
              let address = userInfo[SearchController.CarPlaySearchAPIKey] as? AddressBaseModel else {
            completionHandler()
            return
        }
        
        if(address.source == "GOOGLE" && address.lat == nil)
        {
            // let model = ["placeId":address?.placeId,"source": address?.source, "token": address?.token,"address1":address?.address1,"address2":address?.address2,"distance":address?.distance,"type":address?.type]
            
            let data = ["placeId": address.placeId ?? "", "source": address.source ?? "", "token": address.token ?? ""]
            
            ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.FN_GET_GOOGLE_LAT_LNG_IF_NEED, data: data as NSDictionary)
                .subscribe(onSuccess: { result in
                    guard let result = result,
                          let dict = result["data"] as? NSDictionary,
                          let lat = dict["lat"] as? String,
                          let long = dict["long"] as? String else { return }
                    
                    print("onSuccess: \(dict)")
                    
                    
                    let model = ["placeId":address.placeId,
                                 "source": address.source,
                                 "token": address.token,
                                 "address1":address.address1,
                                 "address2":address.address2,
                                 "distance":address.distance,
                                 "type":address.type,
                                 "lat":lat,
                                 "long":long]
                    
                    ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.FN_SAVE_RECENT_LIST, data: model as NSDictionary)
                        .subscribe(onSuccess: { result in
                            guard let result = result,
                                  let dict = result["data"] as? NSDictionary else { return }
                            print("onSuccess: \(dict)")
                            
                            let searchAddress = SearchAddresses.init(alias: "",
                                                                     address1: address.address1 ?? "",
                                                                     lat: lat,
                                                                     long: long,
                                                                     address2: address.address2 ?? "",
                                                                     distance: address.distance ?? "")
                            
                            self.goToSearchPreview(address: searchAddress, completionHandler: completionHandler)
                        },
                                   onFailure: { error in
                            
                            let searchAddress = SearchAddresses.init(alias: "",
                                                                     address1: address.address1 ?? "",
                                                                     lat: lat,
                                                                     long: long,
                                                                     address2: address.address2 ?? "",
                                                                     distance: address.distance ?? "")
                            
                            self.goToSearchPreview(address: searchAddress, completionHandler: completionHandler)
                        })
                },
                           onFailure: { error in
                    print("onFailure: ", error)
                })
            
        } else {
            let model = ["placeId":address.placeId,
                         "source": address.source,
                         "token": address.token,
                         "address1":address.address1,
                         "address2":address.address2,
                         "distance":address.distance,
                         "type":address.type,
                         "lat":address.lat ?? "",
                         "long":address.long ?? ""]
            
            ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.FN_SAVE_RECENT_LIST, data: model as NSDictionary)
                .subscribe(onSuccess: { result in
                    print("onSuccess: \(String(describing: result?["data"] as? NSDictionary))")
                    
                    let searchAddress = SearchAddresses.init(alias: "",
                                                             address1: address.address1 ?? "",
                                                             lat: address.lat ?? "",
                                                             long: address.long ?? "",
                                                             address2: address.address2 ?? "",
                                                             distance: address.distance ?? "")
                    
                    self.goToSearchPreview(address: searchAddress, completionHandler: completionHandler)
                },
                           onFailure: { error in
                    
                    print("onFailure: ", error)
                })
            
        }
    }
    
    private func goToSearchPreview(address:SearchAddresses, completionHandler: @escaping () -> Void) {
        guard  let lat = address.lat?.toDouble(),
                   let long = address.long?.toDouble() else {
            return
        }
        DispatchQueue.main.async {
            SwiftyBeaver.debug("Car Play search template moving ti Car Play Route Planning")
            let destLocation = CLLocation(latitude: lat, longitude: long)
            let destinationWaypoint = Waypoint(location: destLocation)
            self.delegate?.popFromSearchTemplate(animated: false)
            self.delegate?.searchPreviewRoutes(to: destinationWaypoint, address: address)
        }
    }
    
    func performSearch() {
        guard let completionHandler = self.lastestcompletionHandler,
              let searchText = recentSearchText else { return }
        
        if (searchText.count >= SearchController.MinSearchTextCount) {
            
            let lat = LocationManager.shared.location.coordinate.latitude
            let long = LocationManager.shared.location.coordinate.longitude
            
            DispatchQueue.global(qos: .background).async {
                // Cancel lastest subsciption if need
                if let oldSubscription = self.subscriptionSearch {
                    oldSubscription.dispose()
                }
                self.subscriptionSearch = ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.FN_SEARCH_LOCATION, data: ["searchTerm":searchText,"currentLocation":["lat":lat,"long":long]])
                    .subscribe(onSuccess: { result in
                        
                        print("onSuccess: ", result ?? "")
                        
                        do {
                            guard let data = result?["data"] else {
                                DispatchQueue.main.async {
                                    completionHandler(self.resultsOrNoResults([]))
                                }
                                return
                            }
                            
                            print("onSuccess result data: ", data)
                            
                            let json = try JSONSerialization.data(withJSONObject: data)
                            let decoder = JSONDecoder()
                            decoder.keyDecodingStrategy = .convertFromSnakeCase
                            let addresses = try decoder.decode([AddressBaseModel].self, from: json)
                            addresses.forEach{print($0)}
                            
                            DispatchQueue.main.async {
                                if (addresses.count > 0) {
                                    self.searchResult = addresses
                                    let results = self.searchResult.map { self.listItem(searchAddress: $0)}
                                    completionHandler(self.resultsOrNoResults(results, limit:SearchController.MaximumInitialSearchResults))
                                } else {
                                    DispatchQueue.main.async {
                                        completionHandler(self.resultsOrNoResults([]))
                                    }
                                }
                            }
                        } catch {
                            print(error)
                            DispatchQueue.main.async {
                                completionHandler(self.resultsOrNoResults([]))
                            }
                        }
                    },
                               onFailure: { error in
                        print("onFailure: ", error)
                        DispatchQueue.main.async {
                            completionHandler(self.resultsOrNoResults([]))
                        }
                    })
            }
        } else {
            if(searchText.count >= SearchController.MinSearchTextCount){
                completionHandler(self.resultsOrNoResults([]))
            } else {
                completionHandler([])
            }
        }
    }
}



//
//  CruiseViewModel+CarPlay.swift
//  Breeze
//
//  Created by Zhou Hao on 29/9/21.
//

import Foundation
import Turf
import MapboxCoreNavigation
import MapboxMaps
import MapboxCoreMaps
import MapboxNavigation

extension CruiseViewModel {
    
    // MARK: - Public methods
    func startCPCruise() {
        if(carPlayMapView != nil) {
            setCPCamera(at: self.carPlayMapView.mapView.location.latestLocation?.coordinate, isStarted: true)
            if let cpVC = appDelegate().carPlayManager.carPlayMapViewController {
                cpVC.speedLimitView.isHidden = false
                // customize Mapbox speedLimitView
                cpVC.speedLimitView.regulatoryBorderColor = .gray
                cpVC.speedLimitView.textColor = .black
                setupCPCustomUserLocationIcon()
                // TODO: doesn't work well. Probably because of xib file?
//                setupCPSpeedLimit()
            }
        }
    }
    
    func stopCPCruise() {
        if(carPlayMapView != nil) {
            setCPCamera(at: self.carPlayMapView.mapView.location.latestLocation?.coordinate, isStarted: false)
            removeCPRoadPanel()
            setupCPCustomUserLocationIcon()
            removeCPSpeedLimit()
        }
    }
    
    func resetCPCamera() {
        if(carPlayMapView != nil) {
            if let coordinate = self.carPlayMapView.mapView.location.latestLocation?.coordinate {
                setCPCamera(at: coordinate, isStarted: self.isStarted, heading: carPlayMapView.mapView.cameraState.bearing, animated: true, reset: true)
            }
        }
    }
        
    func setCPCamera(at coordinate: CLLocationCoordinate2D?, isStarted: Bool, heading: Double = 0, animated: Bool = true, reset: Bool = false){
        if(carPlayMapView != nil) {
            carPlayMapView.navigationCamera.stop()
            carPlayMapView.mapView.setCruiseCamera(at: coordinate, heading: heading, animated: animated, reset: reset, isStarted: isStarted, isCarPlay: true)
        }
    }
    
    func setupCPCustomUserLocationIcon() {
        if(carPlayMapView != nil) {
            carPlayMapView.mapView.setUpCruiseCustomUserLocationIcon(isStarted: isStarted)
        }
    }
    
    func showCPRoadName(_ name: String) {
        if(carPlayMapView != nil) {
            if(cpRoadNamePanel == nil) {
                createCPRoadNamePanel()
            }
            cpRoadNamePanel!.set(name)
        }
    }
    
    func updateCPSpeedLimit(_ value: Double) {
        if(carPlayMapView != nil) {
            cpSpeedLimitView?.speed = value
        }
    }
    
    func removeCPRoadPanel() {
        if(carPlayMapView != nil) {
            if cpRoadNamePanel != nil {
                cpRoadNamePanel?.removeFromSuperview()
                cpRoadNamePanel = nil
            }
        }
    }
    
    // MARK: - CarPlay Road name
    private func createCPRoadNamePanel() {
        cpRoadNamePanel = RoadNamePanel(frame: CGRect(x: 0, y: 0, width: 170, height: 40))
        self.carPlayMapView.addSubview(cpRoadNamePanel!)
        self.carPlayMapView.bringSubviewToFront(cpRoadNamePanel!)
                
//        let minW = UIScreen.main.bounds.width > 748 ? 220 : 190
        cpRoadNamePanel!.translatesAutoresizingMaskIntoConstraints = false
        
        cpRoadNamePanel!.widthAnchor.constraint(equalToConstant: 190).isActive = true
        cpRoadNamePanel!.heightAnchor.constraint(equalToConstant: 40).isActive = true
        cpRoadNamePanel!.centerXAnchor.constraint(equalTo: carPlayMapView.centerXAnchor).isActive = true
        cpRoadNamePanel!.centerYAnchor.constraint(equalTo: carPlayMapView.centerYAnchor).isActive = true
    }
    
    private func setupCPSpeedLimit() {
        guard cpSpeedLimitView == nil else { return }
                
        cpSpeedLimitView = CustomSpeedLimitView(frame: CGRect(x: 0, y: 0, width: 80, height: 80))
        cpSpeedLimitView!.translatesAutoresizingMaskIntoConstraints = false
        
        carPlayMapView.addSubview(cpSpeedLimitView!)
        
        cpSpeedLimitView!.topAnchor.constraint(equalTo: carPlayMapView.safeAreaLayoutGuide.topAnchor, constant: 8).isActive = true
        cpSpeedLimitView!.rightAnchor.constraint(equalTo: carPlayMapView.rightAnchor, constant: -8).isActive = true
        cpSpeedLimitView!.widthAnchor.constraint(equalToConstant: 30).isActive = true
        cpSpeedLimitView!.heightAnchor.constraint(equalToConstant: 30).isActive = true
    }
    
    private func removeCPSpeedLimit() {
        if cpSpeedLimitView != nil {
            cpSpeedLimitView?.removeFromSuperview()
            cpSpeedLimitView = nil
        }
    }

}

//
//  CPTrip+Preview.swift
//  Breeze
//
//  Created by VishnuKanth on 13/10/21.
//

import Foundation
import MapboxDirections
import CarPlay
import MapboxNavigation

extension CPTrip {
    
    static let shortDateComponentsFormatter: DateComponentsFormatter = {
        let formatter = DateComponentsFormatter()
        formatter.unitsStyle = .short
        formatter.allowedUnits = [.day, .hour, .minute]
        return formatter
    }()
    
    convenience init(routeResponse: RouteResponse) {
        var waypoints: [Waypoint]?
        var options: DirectionsOptions?
        switch routeResponse.options {
        case .route(let routeOptions):
            waypoints = routeOptions.waypoints
            options = routeOptions
        case .match(let matchOptions):
            waypoints = matchOptions.waypoints
            options = matchOptions
        }
        let routeChoices = routeResponse.routes?.enumerated().map { (routeIndex, route) -> CPRouteChoice in
            
            var erpValue = "$0.00"
            if(appDelegate().routePlanningViewModel?.carPlayMapViewUpdatable?.erpPriceUpdater != nil)
            {
                
                erpValue = (appDelegate().routePlanningViewModel?.carPlayMapViewUpdatable?.erpPriceUpdater.newComputeTollsForCarPlayRP(route: route, erpTolls: DataCenter.shared.getAllERPFeatureCollection()))!
            }
            
            let selectionSummaryVariants = [
                "\(CPTrip.shortDateComponentsFormatter.string(from: route.expectedTravelTime)!) | \(erpValue)"
//                DateComponentsFormatter.shortDateComponentsFormatter.string(from: route.expectedTravelTime)!,
//                DateComponentsFormatter.briefDateComponentsFormatter.string(from: route.expectedTravelTime)!
            ]
            let routeChoice = CPRouteChoice(summaryVariants: [route.description],
                                            additionalInformationVariants: [route.description],
                                            selectionSummaryVariants: selectionSummaryVariants)
            
           /* let key: String = CPRouteChoice.RouteResponseUserInfo.key
            let info: (RouteResponse, Int, DirectionsOptions) = (routeResponse: routeResponse, routeIndex: routeIndex, directionsOptions: options!)
            routeChoice.userInfo = info
            
            let value: CPRouteChoice.RouteResponseUserInfo = .init(response: routeResponse,
                                                                   routeIndex: routeIndex,
                                                                   options: options!)
            let userInfo: CarPlayUserInfo = [key: value]
            routeChoice.userInfo = userInfo*/
            
            let info: (RouteResponse, Int, DirectionsOptions) = (routeResponse: routeResponse, routeIndex: routeIndex, directionsOptions: options!)
            routeChoice.userInfo = info
            
            return routeChoice
        } ?? []
        
        let origin = MKMapItem(placemark: MKPlacemark(coordinate: waypoints?.first?.coordinate ?? CLLocationCoordinate2D()))
        origin.name = waypoints?.first?.name
        let destination = MKMapItem(placemark: MKPlacemark(coordinate: waypoints?.last?.coordinate ?? CLLocationCoordinate2D()))
        destination.name = waypoints?.last?.name
        
        self.init(origin: origin, destination: destination, routeChoices: routeChoices)
        userInfo = options!
    }
    
    static func cpGetRouteChoice(routeResponse: RouteResponse) -> Any?{
        
        //var waypoints: [Waypoint]?
        var options: DirectionsOptions?
        switch routeResponse.options {
        case .route(let routeOptions):
            //waypoints = routeOptions.waypoints
            options = routeOptions
        case .match(let matchOptions):
            //waypoints = matchOptions.waypoints
            options = matchOptions
        }
        let info: (RouteResponse, Int, DirectionsOptions) = (routeResponse: routeResponse, routeIndex: 0, directionsOptions: options!)
        return info
    }
}

@available(iOS 12.0, *)
extension CPRouteChoice {
    
    static func getUpdatedRouteChoice(response:RouteResponse) -> [CPRouteChoice] {
        
        let routeChoices = response.routes?.enumerated().map { (routeIndex, route) -> CPRouteChoice in
            
            var erpValue = "$0.00"
            if(appDelegate().routePlanningViewModel?.carPlayMapViewUpdatable?.erpPriceUpdater != nil)
            {
                erpValue = (appDelegate().routePlanningViewModel?.carPlayMapViewUpdatable?.erpPriceUpdater.computeTollsForCarPlayRP(route: route, erpTolls: DataCenter.shared.getAllERPFeatureCollection()))!
            }
            
            let selectionSummaryVariants = [
                "\(CPTrip.shortDateComponentsFormatter.string(from: route.expectedTravelTime)!) | \(erpValue)"
//                DateComponentsFormatter.shortDateComponentsFormatter.string(from: route.expectedTravelTime)!,
//                DateComponentsFormatter.briefDateComponentsFormatter.string(from: route.expectedTravelTime)!
            ]
            let routeChoice = CPRouteChoice(summaryVariants: [route.description],
                                            additionalInformationVariants: [route.description],
                                            selectionSummaryVariants: selectionSummaryVariants)
            
            
            return routeChoice
        } ?? []
        
        return routeChoices
        
    }
    
}

//
//  CPPreviewButtons.swift
//  Breeze
//
//  Created by VishnuKanth on 19/10/21.
//

import Foundation
import CarPlay
import MapboxNavigation

protocol CPPreviewBarButtonActionDelegate: AnyObject {
    func CPPreviewNavigationButtonSelected(type:String)
    
}

final class CPPreviewNavigationButtons{
    weak var delegate: CPPreviewBarButtonActionDelegate?
    init(){
        //This is just a empty constructor
    }
    
    func createRPPreviewLeadingNavigationButtons(interfaceController: CPInterfaceController, traitCollection: UITraitCollection) -> [CPBarButton] {
        
        var cpBarButton = [CPBarButton]()
        
        // MARK: - ShowCarPark button
        let showCarParkBtn = CPBarButton(type: .text) { (barButton) in
            
//            if(barButton.title == Constants.cpCarParkTitle)
//            {
//                barButton.title = Constants.cpCarParkHideTitle
//            }
//            else
//            {
//                barButton.title = Constants.cpCarParkTitle
//            }
            
            self.delegate?.CPPreviewNavigationButtonSelected(type: barButton.title ?? Constants.cpCarParkTitle)
        }
        showCarParkBtn.title = Constants.cpCarParkTitle
        
        cpBarButton.append(showCarParkBtn)
        return cpBarButton
    }
    
    func createRPPreviewTralingNavigationButtons(interfaceController: CPInterfaceController, traitCollection: UITraitCollection) -> [CPBarButton] {
        
        var cpBarButton = [CPBarButton]()
        
        // MARK: - Notify Arrival button
        let notifyArrivalBtn = CPBarButton(type: .text) { (barButton) in
            self.delegate?.CPPreviewNavigationButtonSelected(type: barButton.title ?? Constants.cpShareDrive)
        }
        notifyArrivalBtn.title = Constants.cpShareDrive
        
        cpBarButton.append(notifyArrivalBtn)
        return cpBarButton
    }
}

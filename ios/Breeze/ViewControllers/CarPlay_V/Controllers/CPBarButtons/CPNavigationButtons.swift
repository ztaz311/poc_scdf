//
//  CPNavigationButtons.swift
//  Breeze
//
//  Created by VishnuKanth on 19/10/21.
//

import Foundation
import CarPlay
import MapboxNavigation
import MapboxCoreNavigation

protocol CPNavigationBarButtonActionDelegate: AnyObject {
    func CPNavigationNavigationButtonSelected(type:String)
    func CPCruiseNavigationButtonSelected(type:String)
    func checkStateOBUConnect()
}

final class CPNavigationButtons{
    
    weak var delegate: CPNavigationBarButtonActionDelegate?
    init(){
        //This is just a empty constructor
    }
    func createCruiseTralingNavigationButtons(interfaceController: CPInterfaceController, traitCollection: UITraitCollection, isSharingDrive: Bool = false) -> [CPBarButton] {
        
        var cpBarButton = [CPBarButton]()

        // MARK: - End Cruise button
        let endBtn = CPBarButton(type: .text) { (barButton) in
            self.delegate?.CPCruiseNavigationButtonSelected(type: barButton.title ?? Constants.cpNotifyArrivalTitle)
        }
        endBtn.title = Constants.cpEndCruise
        cpBarButton.append(endBtn)

        // Add Share Drive button
//        let shareDriveBtn = CPBarButton(type: .text) { (barButton) in
//            self.delegate?.CPCruiseNavigationButtonSelected(type: barButton.title ?? Constants.cpShareDrive)
//        }
//        
//        // decide the title
//        if ETATrigger.shared.tripCruiseETA != nil {
//            if isSharingDrive {
//                shareDriveBtn.title = Constants.cpETAPause
//            } else {
//                shareDriveBtn.title = Constants.cpETAResume
//            }
//        } else {
//            shareDriveBtn.title = Constants.cpShareDrive
//        }
//        cpBarButton.append(shareDriveBtn)
        // MARK: - OBU SDK
        let obuModeBtn = CPBarButton(type: .text) { (barButton) in
            
            if OBUHelper.shared.stateOBU == .CONNECTED
            {
                barButton.title = "$ \(OBUHelper.shared.getCardBalance())"
            } else {
                barButton.title = "OBU"
            }

            
            self.delegate?.checkStateOBUConnect()
        }
        
        
        if OBUHelper.shared.stateOBU == .CONNECTED
        {
            obuModeBtn.title = "$ \(OBUHelper.shared.getCardBalance())"
        } else {
            obuModeBtn.title = "OBU"
        }
        cpBarButton.append(obuModeBtn)
        return cpBarButton
    }
    
    func createCruiseLeadingNavigationButtons(interfaceController: CPInterfaceController, traitCollection: UITraitCollection, isMute: Bool = false) -> [CPBarButton] {
        
        var cpBarButton = [CPBarButton]()
       
        
         // MARK: - Search button
//        let searchBtn = CPBarButton(type: .image) { (barButton) in
//
//            self.delegate?.CPCruiseNavigationButtonSelected(type: barButton.title ?? Constants.cpAddDestination)
//            self.delegate?.openSearchScreenCarPlay()
//        }
//
//        searchBtn.image = UIImage(named: "search", in: nil, compatibleWith: traitCollection)
//
//        cpBarButton.append(searchBtn)
        
        
        
        // MARK: - Mute button
        let muteBtn = CPBarButton(type: .image) { (barButton) in
            let isMute = !Prefs.shared.getBoolValue(.enableAudio)
            self.delegate?.CPCruiseNavigationButtonSelected(type: isMute ? Constants.cpUnmute : Constants.cpMute)
        }
        
        let isMute = !Prefs.shared.getBoolValue(.enableAudio)
        if isMute {
            muteBtn.image = UIImage(named: "carplayMutedIcon", in: nil, compatibleWith: traitCollection)
        } else {
            muteBtn.image = UIImage(named: "carplayUnmutedIcon", in: nil, compatibleWith: traitCollection)
        }
        cpBarButton.append(muteBtn)
        
//        let notifyArrivalBtn = CPBarButton(type: .text) { (barButton) in
//            self.delegate?.CPCruiseNavigationButtonSelected(type: barButton.title ?? Constants.cpMute)
//        }
//
//        if isMute {
//            notifyArrivalBtn.title = Constants.cpUnmute
//        } else {
//            notifyArrivalBtn.title = Constants.cpMute
//        }
//
//        cpBarButton.append(notifyArrivalBtn)
/*
        // MARK: - ETA Pause/Resume button
        if(ETATrigger.shared.tripCruiseETA != nil && ETATrigger.shared.tripCruiseETA?.shareLiveLocation == "Y"){
            
            let etaBtn = CPBarButton(type: .text) { (barButton) in
                
                if(barButton.title == Constants.cpETAPause)
                {
                    barButton.title = Constants.cpETAResume
                }
                else
                {
                    barButton.title = Constants.cpETAPause
                }
                self.delegate?.CPCruiseNavigationButtonSelected(type: barButton.title ?? Constants.cpETAPause)
            }
            etaBtn.title = Constants.cpETAPause
            cpBarButton.append(etaBtn)
            
        }
*/
        
        let showCarParkBtn = CPBarButton(type: .text) { (barButton) in
            
            barButton.title = "Carparks"
            
            
            self.delegate?.CPCruiseNavigationButtonSelected(type: barButton.title ?? "Carparks")
            
            
        }
        showCarParkBtn.title = "Carparks"
        
        cpBarButton.append(showCarParkBtn)
        
        
        
        return cpBarButton
        
    }
    
    func createNavTralingNavigationButtons(interfaceController: CPInterfaceController, traitCollection: UITraitCollection) -> [CPBarButton] {
        
        var cpBarButton = [CPBarButton]()
        
        // MARK: - end button
        let endBtn = CPBarButton(type: .text) { (barButton) in
            self.delegate?.CPNavigationNavigationButtonSelected(type: barButton.title ?? Constants.cpEndNav)
        }
        endBtn.title = Constants.cpEndNav
        cpBarButton.append(endBtn)
        
        // Share Drive button
//        let shareDriveBtn = CPBarButton(type: .text) { (barButton) in
//            self.delegate?.CPNavigationNavigationButtonSelected(type: barButton.title ?? Constants.cpShareDrive)
//        }
//        shareDriveBtn.title = Constants.cpShareDrive
//        cpBarButton.append(shareDriveBtn)
        
        // MARK: - OBU SDK
        let obuModeBtn = CPBarButton(type: .text) { (barButton) in
            
            if OBUHelper.shared.stateOBU == .CONNECTED
            {
                barButton.title = "$ \(OBUHelper.shared.getCardBalance())"
            } else {
                barButton.title = "OBU"
            }

            
            self.delegate?.checkStateOBUConnect()
        }
        
        
        if OBUHelper.shared.stateOBU == .CONNECTED
        {
            obuModeBtn.title = "$ \(OBUHelper.shared.getCardBalance())"
        } else {
            obuModeBtn.title = "OBU"
        }
        
        cpBarButton.append(obuModeBtn)

        
        // MARK: - Public Message Announce
#if DEMO
        let pmButton = CPBarButton(type: .text) { (barButton) in
            self.delegate?.CPNavigationNavigationButtonSelected(type: barButton.title ?? Constants.cpPbMessage)
        }
        pmButton.title = Constants.cpPbMessage
        
        cpBarButton.append(pmButton)
#endif
        
        return cpBarButton
    }
    
    func createNavLeadingNavigationButtons(interfaceController: CPInterfaceController, traitCollection: UITraitCollection) -> [CPBarButton] {
        
        var cpBarButton = [CPBarButton]()
        
        // MARK: - Mute button
        let muteBtn = CPBarButton(type: .image) { (barButton) in
            let isMute = !Prefs.shared.getBoolValue(.enableAudio)
            self.delegate?.CPNavigationNavigationButtonSelected(type: isMute ?  Constants.cpUnmute : Constants.cpMute)
        }
        
        if(NavigationSettings.shared.voiceMuted){
            muteBtn.image = UIImage(named: "carplayMutedIcon", in: nil, compatibleWith: traitCollection)
        }
        else
        {
            muteBtn.image = UIImage(named: "carplayUnmutedIcon", in: nil, compatibleWith: traitCollection)
        }
        
        cpBarButton.append(muteBtn)
        
        // MARK: - Carpark button
        
/*
        // MARK: - ETA Pause/Resume button
        if(appDelegate().tripETA != nil && appDelegate().tripETA?.shareLiveLocation == "Y"){
            
            let etaBtn = CPBarButton(type: .text) { (barButton) in
                
                if(barButton.title == Constants.cpETAPause)
                {
                    barButton.title = Constants.cpETAResume
                }
                else
                {
                    barButton.title = Constants.cpETAPause
                }
                self.delegate?.CPNavigationNavigationButtonSelected(type: barButton.title ?? Constants.cpETAPause)
            }
            etaBtn.title = Constants.cpETAPause
            cpBarButton.append(etaBtn)
            
        }
*/
        // Show CarPark  Button
        let showCarParkBtn = CPBarButton(type: .text) { (barButton) in
            
            barButton.title = "Carparks"
            
            
            self.delegate?.CPNavigationNavigationButtonSelected(type:  barButton.title ?? "Carparks")
            
            
        }
        showCarParkBtn.title = "Carparks"
        
        cpBarButton.append(showCarParkBtn)
        
        
        
        return cpBarButton
    }
    
}

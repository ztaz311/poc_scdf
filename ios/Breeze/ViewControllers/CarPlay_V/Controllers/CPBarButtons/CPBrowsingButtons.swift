//
//  CPBrowsingButtons.swift
//  Breeze
//
//  Created by VishnuKanth on 19/10/21.
//

import Foundation
import CarPlay
import MapboxNavigation

protocol CPBrowseBarButtonActionDelegate: AnyObject {
    func CPBrowsingNavigationButtonSelected(type:String)
    func openSearchScreenCarPlay()
    func checkStateOBUConnect()
}

final class CPBrowsingNavigationButtons{
    weak var delegate: CPBrowseBarButtonActionDelegate?
    init(){
    
        //This is just a empty constructor
    }
    
    func createLeadingNavigationButtons(interfaceController: CPInterfaceController, traitCollection: UITraitCollection) -> [CPBarButton]{
        
        var cpBarButton = [CPBarButton]()
        
        // MARK: - Search button
        let searchBtn = CPBarButton(type: .image) { (barButton) in
            
//            self.delegate?.CPBrowsingNavigationButtonSelected(type: barButton.title ?? Constants.cpAddDestination)
            self.delegate?.openSearchScreenCarPlay()
        }
        
        searchBtn.image = UIImage(named: "search", in: nil, compatibleWith: traitCollection)
        
        cpBarButton.append(searchBtn)
        
        // MARK: - ShowCarPark button
        let showCarParkBtn = CPBarButton(type: .text) { (barButton) in
            
//            if(barButton.title == Constants.cpCarParkTitle)
//            {
//                barButton.title = Constants.cpCarParkHideTitle
//            }
//            else
//            {
//                barButton.title = Constants.cpCarParkTitle
//            }
            
            self.delegate?.CPBrowsingNavigationButtonSelected(type: barButton.title ?? Constants.cpCarParkTitle)
        }
        showCarParkBtn.title = Constants.cpCarParkTitle
        
        cpBarButton.append(showCarParkBtn)
        
        return cpBarButton
        
    }
    
    func createTrailingNavigationButtons(interfaceController: CPInterfaceController, traitCollection: UITraitCollection) -> [CPBarButton] {
        
        var cpBarButton = [CPBarButton]()

        // don't show Notify Arrival button in map landing
/*
        // MARK: - Notify Arrival button
        let notifyArrivalBtn = CPBarButton(type: .text) { (barButton) in
            self.delegate?.CPBrowsingNavigationButtonSelected(type: barButton.title ?? Constants.cpNotifyArrivalTitle)
        }
        notifyArrivalBtn.title = Constants.cpNotifyArrivalTitle
        
        cpBarButton.append(notifyArrivalBtn)
*/
        
        // MARK: - OBU SDK
        let obuModeBtn = CPBarButton(type: .text) { (barButton) in
//            
            if OBUHelper.shared.stateOBU == .CONNECTED
            {
                barButton.title = "$ \(OBUHelper.shared.getCardBalance())"
            } else {
                barButton.title = "OBU"
            }
            
            
            self.delegate?.checkStateOBUConnect()
        }
        
        
//        obuModeBtn.title = Constants.cpStartObu
        if OBUHelper.shared.stateOBU == .CONNECTED
        {
            obuModeBtn.title = "$ \(OBUHelper.shared.getCardBalance())"
        } else {
            obuModeBtn.title = "OBU"
        }
        cpBarButton.append(obuModeBtn)
        
        // MARK: - Cruise mode button
//        let cruiseModeBtn = CPBarButton(type: .text) { (barButton) in
//
//            self.delegate?.CPBrowsingNavigationButtonSelected(type: barButton.title ?? Constants.cpStartCruise)
//
//        }
//        cruiseModeBtn.title = Constants.cpStartCruise
//
//        cpBarButton.append(cruiseModeBtn)
        
      
        
        return cpBarButton
    }
}

//
//  MapTemplateProvider.swift
//  Breeze
//
//  Created by VishnuKanth on 13/10/21.
//

import CarPlay
import MapboxNavigation


class MapTemplateProvider: NSObject {
    
    
    func mapTemplate(forPreviewing trip: CPTrip,
                     traitCollection: UITraitCollection,
                     carPlayManager: CarPlayManager) -> CPMapTemplate {
        
        let mapTemplate = createMapTemplate()
        mapTemplate.mapDelegate = carPlayManager
        mapTemplate.automaticallyHidesNavigationBar = true
    
        if let leadingButtons = carPlayManager.delegate?.carPlayManager(carPlayManager, leadingNavigationBarButtonsCompatibleWith: traitCollection, in: mapTemplate, for: .previewing){
            mapTemplate.leadingNavigationBarButtons = leadingButtons
        }
       
        if let trailingButtons = carPlayManager.delegate?.carPlayManager(carPlayManager, trailingNavigationBarButtonsCompatibleWith: traitCollection, in: mapTemplate, for: .previewing){
            mapTemplate.trailingNavigationBarButtons = trailingButtons
        }
        
        return mapTemplate
    }
    
    func createMapTemplate() -> CPMapTemplate {
        return CPMapTemplate()
    }
    
    func mapTemplate(forCarpark carPlayManager: CarPlayManager,
                         traitCollection: UITraitCollection,
                         mapDelegate: CPMapTemplateDelegate) -> CPMapTemplate {
            
           //let mapViewController = carPlayManager.carPlayMapViewController
            let mapTemplate = createMapTemplate()
            //mapTemplate.mapDelegate = mapDelegate
            
//            let zoomInButton = CPMapButton { _ in
//                carPlayManager.navigationMapView?.carPlayZoomIn()
//            }
//
//            zoomInButton.image = UIImage(named: "zoom in", in: nil, compatibleWith: traitCollection)
//
//
//            let zoomOutButton = CPMapButton { _ in
//                carPlayManager.navigationMapView?.carPlayZoomOut()
//            }
//
//            zoomOutButton.image = UIImage(named: "zoom out", in: nil, compatibleWith: traitCollection)
//
//            var mapButtons = [mapViewController!.recenterButton,
//                              zoomInButton,
//                              zoomOutButton]
//
//            mapButtons.insert(mapViewController!.panningInterfaceDisplayButton(for: mapTemplate), at: 1)
//            mapTemplate.mapButtons = mapButtons
            mapTemplate.automaticallyHidesNavigationBar = true
            
           
            
            return mapTemplate
        }
        

}

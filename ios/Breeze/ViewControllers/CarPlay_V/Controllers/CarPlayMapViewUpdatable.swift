//
//  CarPlayMapViewUpdatable.swift
//  Breeze
//
//  Created by Zhou Hao on 30/9/21.
//

import Foundation
import CoreLocation
import Turf
import MapboxNavigation
@_spi(Restricted) import MapboxMaps
import CarPlay
import MapboxDirections
import SwiftyBeaver

final class CarPlayMapViewUpdatable: MapViewUpdatable {
    
    weak var navigationMapView: NavigationMapView?
    weak var delegate: MapViewUpdatableDelegate?
    private var cpRoadNamePanel: RoadNamePanel?
    
    // for notification
    private var cpFeatureImage:UIImage!
    private var cpFeatureNotificationAlert:CPNavigationAlert?
    private var cpFeatureTitleVariants = [String]()
    private var cpFeatureSubTitleVariants = [String]()
    var cpFeatureDisplay:FeatureDetectable?
    var cpFeatureDisplayClose = false

    init(navigationMapView: NavigationMapView) {
        self.navigationMapView = navigationMapView
        self.navigationMapView?.mapView.ornaments.options.attributionButton.visibility = .hidden
        setInitialMapCamera()
    }

    func cruiseStatusUpdate(isCruise: Bool) {
        setCamera(at: navigationMapView?.mapView.location.latestLocation?.coordinate, isCruise: isCruise)
        updateCameraLayer(show: isCruise)
        if isCruise {
            cruiseStarted()
        } else {
            cruiseStopped()
        }
    }
     
    func updateRoadName(_ name: String) {
        guard let roadNamePanel = self.cpRoadNamePanel else { return }
        
        let hide = name.isEmpty        
        let needUpdate = roadNamePanel.text() != name
        
        roadNamePanel.set(name)
        // animation
        if needUpdate {
            UIView.animate(withDuration: Values.standardAnimationDuration) {
                [weak self] in
                guard let self = self else { return }
                self.updateRoadPanelConstraint(isHidden: hide)
            }
        }
    }
    
    func updateSpeedLimit(_ value: Double) {
        
        //Not required
    }

    func overSpeed(value: Bool) {
        
        //Not required
    }
    
    func updateParkingAvailability(carpark: Carpark?) {
        
        //Not required
    }
    
    func updateNotificationWhenUserSoonArrive(carpark: Carpark?, message: String) {
        
        //Not required
    }

    func updateNotification(featureId: String?, currentFeature: FeatureDetectable?) {
        if let feature = currentFeature {
            
            if let cpFeatureDisplay = self.cpFeatureDisplay {
                
                //If already shown feature is equal to current incomingfeature and user hasn't closed the notification
                if(cpFeatureDisplay == feature && self.cpFeatureDisplayClose == false){
                    
                    showFirstFeature(feature)
                    showCPNotification(hide: false)
                }
                else{
                    
                    if(cpFeatureDisplay != feature){ //If previous feature not equal to current
                        
                        self.cpFeatureDisplay = feature
                        showFirstFeature(feature)
                        showCPNotification(hide: false)
                        
                    }
                }
            }
            else{
                self.cpFeatureDisplay = feature
                showFirstFeature(feature)
                showCPNotification(hide: false)
            }
         
            DispatchQueue.main.asyncAfter(deadline: .now() + Values.notificationDuration) { [weak self] in
                guard let self = self else { return }
                self.showCPNotification(hide: true)
            }

            
//        } else {
//            showCPNotification(hide: true)
        }
    }
    
    // MARK: - Tunnel
    func updateTunnel(isInTunnel: Bool) {
        if isInTunnel {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {[weak self] in
                if let navMapView = self?.navigationMapView {
                    showToast(from: navMapView, message: Constants.toastInTunnel, topOffset: 15, icon: "noInternetIcon", duration: 0)
                }
            }
        } else {
            DispatchQueue.main.async { [weak self] in
                if let navMapView = self?.navigationMapView {
                    hideToast(from: navMapView)
                }
            }
        }
    }

    // MARK: - ETA
    func updateETA(eta: TripCruiseETA?) {
        //Not required
    }

    func updateETA(eta: TripETA?) {
        //Not required
    }
    
    func updateLiveLocationEnabled(_ isEnabled: Bool) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) { [weak self] in
            guard let self = self else { return }
              
            if let mapView = self.navigationMapView?.mapView {
                if isEnabled {
                    showToast(from: mapView, message: Constants.toastLiveLocationResumed, topOffset: 5, icon: "toastLiveLocationResumed", messageColor: .black, duration: 3)
                } else {
                    showToast(from: mapView, message: Constants.toastLiveLocationPaused, topOffset: 5, icon: "toastLiveLocationPaused", messageColor: .black, duration: 3)
                }
            }
        }
    }
    
    func shareDriveButtonEnabled(_ isEnabled: Bool){
        //No need to handle this
    }
    
    func updateMuteStatus(isEnabled:Bool){
        
        if(appDelegate().carPlayMapController != nil)
        {
            appDelegate().carPlayMapController?.updateCPCruiseToggleTitle(activity: .navigating,isEnabled:isEnabled)
        }
    }
    
    
    func showETAMessage(recipient: String, message: String) {
        let imageWithText = UIImage.generateImageWithText(text: getAbbreviation(name: recipient), font: UIFont(name: fontFamilySFPro.Bold, size: 20.0)!)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) { [weak self] in
            if let mapView = self?.navigationMapView?.mapView {
                showToast(from: mapView, message: message, topOffset: 5, image: imageWithText, messageColor: .black, font: UIFont(name: fontFamilySFPro.Regular, size: 20.0)!, duration: 5)
            }
        }
    }
    
    func showNotifyToastMessage(message:String){
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) { [weak self] in
            if let mapView = self?.navigationMapView?.mapView {
                showToast(from: mapView, message: message, topOffset: 5, icon: "toastLiveLocationPaused", messageColor: .black, duration: 3)
            }
        }
        
    }
    
    // MARK: - Finding ERP's on route line
    func initERPUpdater(response:RouteResponse?){
        
        //Not required
    }
    
    // MARK: - Private methods
    private func cruiseStarted() {
        setupRoadNamePanel()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) { [weak self] in
            
            if let mapView = self?.navigationMapView?.mapView {
                showToast(from: mapView, message: Constants.toastStartCruise, topOffset: 5, fromCarPlay: false)
            }
        }
    }
    
    private func cruiseStopped() {
        removeRoadNamePanel()
        if let navMapView = self.navigationMapView {
            hideToast(from: navMapView)
        }
        if let mapview = self.navigationMapView?.mapView {
            hideToast(from: mapview)
        }
    }
    
    private func setupRoadNamePanel() {
        cpRoadNamePanel = RoadNamePanel(frame: CGRect(x: 0, y: 0, width: 170, height: 40))
        navigationMapView?.addSubview(cpRoadNamePanel!)
        navigationMapView?.bringSubviewToFront(cpRoadNamePanel!)
                
//        let minW = UIScreen.main.bounds.width > 400 ? 220 : 190
        let minW = 170
        if let navMapView = self.navigationMapView {
            cpRoadNamePanel?.translatesAutoresizingMaskIntoConstraints = false
            cpRoadNamePanel?.snp.makeConstraints { (make) in
                make.centerX.equalToSuperview().offset(-minW/2)
                make.bottom.equalTo(navMapView.snp.bottom).offset(-10)
                make.width.greaterThanOrEqualTo(170)
                make.width.lessThanOrEqualTo(minW)
                make.height.equalTo(40)
            }
        }
        
        cpRoadNamePanel?.label.font = UIFont.carPlayRoadNameBoldFont()
        
        updateRoadPanelConstraint(isHidden: true)
        cpRoadNamePanel?.setNeedsLayout()
        cpRoadNamePanel?.layoutIfNeeded()

    }
    
    func removeRoadNamePanel() {
        if cpRoadNamePanel != nil {
            cpRoadNamePanel?.removeFromSuperview()
            cpRoadNamePanel = nil
        }    
    }

    private func updateRoadPanelConstraint(isHidden: Bool) {
        if let navMapView = self.navigationMapView {
            cpRoadNamePanel?.snp.updateConstraints { (make) in
                make.bottom.equalTo(navMapView.snp.bottom).offset(isHidden ? 40 : -10)
            }
        }
    }

    private func showCPNotification(hide:Bool){
                
        let mapTemplate = getRootMapTemplate()
        if(mapTemplate != nil)
        {
            if(hide)
            {
                
                mapTemplate?.dismissNavigationAlert(animated: true) { dismissed in
                    self.cpFeatureNotificationAlert = nil
                }
            }
            else
            {
                
                if(cpFeatureNotificationAlert == nil)
                {
                    let action = CPAlertAction(title: Constants.cpNavAlert,
                                               style: .default,
                                               handler: { _ in
                        self.cpFeatureDisplayClose = true
                        self.cpFeatureNotificationAlert = nil
                    })
                    cpFeatureNotificationAlert = CPNavigationAlert(titleVariants: cpFeatureTitleVariants, subtitleVariants: cpFeatureSubTitleVariants, image: cpFeatureImage, primaryAction: action, secondaryAction: nil, duration: 0)

                    mapTemplate?.present(navigationAlert: cpFeatureNotificationAlert!, animated: true)
                }
                else
                {
                    cpFeatureNotificationAlert?.updateTitleVariants(cpFeatureTitleVariants, subtitleVariants: cpFeatureSubTitleVariants)
                    
                }
                
            }
        }
        
            
        //}
    }
    
    private func showFirstFeature(_ feature: FeatureDetectable) {
        let distance = Int(feature.distance)
        let distanceText = String(format: "%d m", distance)
        let typeText = FeatureNotification.getTypeText(for: feature,isCarPlay: true)
        
        SwiftyBeaver.debug("\(feature), distance = \(distance), type = \(typeText)")
        
        //Setting Title variants for Car Play Notification
        cpFeatureTitleVariants = []
        cpFeatureSubTitleVariants = []
        cpFeatureTitleVariants.append(typeText)
        cpFeatureSubTitleVariants.append(distanceText)
        
        cpFeatureImage = FeatureNotification.getTypeImage(feature, isCarPlay: true)
    }

    private func getRootMapTemplate() -> CPMapTemplate? {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate,
              let mapTemplate = appDelegate.carPlayManager.interfaceController?.rootTemplate else { return nil }
        
        return mapTemplate as? CPMapTemplate
    }

}

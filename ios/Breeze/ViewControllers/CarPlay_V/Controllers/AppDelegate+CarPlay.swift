import UIKit
import MapboxNavigation
import CarPlay
import MapboxCoreNavigation
import MapboxDirections
import MapboxMaps
import Turf
import SwiftyBeaver
import CoreLocation

let CarPlayWaypointKey: String = "MBCarPlayWaypoint"

@available(iOS 12.0, *)
extension AppDelegate: CarPlayManagerDelegate {
    
    // Beta.21
    private func carPlayManager(_ carPlayManager: CarPlayManager, navigationServiceFor routeResponse: RouteResponse, routeIndex: Int, routeOptions: RouteOptions, desiredSimulationMode: SimulationMode) -> NavigationService {
        
        //        if let nvc = self.window?.rootViewController?.presentedViewController as? NavigationViewController, let service = nvc.navigationService {
        //            //Do not set simulation mode if we already have an active navigation session.
        //            return service
        //        }
        //        return MapboxNavigationService(route: route, routeIndex: routeIndex, routeOptions: routeOptions,  simulating: desiredSimulationMode)
        
        if let nvc = self.navigationController, let service = nvc.navigationService {
            //Do not set simulation mode if we already have an active navigation session.
            return service
        }
        
        // Beta.21
        return MapboxNavigationService(routeResponse: routeResponse, routeIndex: routeIndex, routeOptions: routeOptions, simulating: desiredSimulationMode)
    }
    
    // MARK: CarPlayManagerDelegate
    func carPlayManager(_ carPlayManager: CarPlayManager, didBeginNavigationWith service: NavigationService) {
        //currentAppRootViewController?.beginNavigationWithCarPlay(navigationService: service)
        //carPlayManager.simulatesLocations = true
        RoundingTable.metric = RoundingTable(thresholds: [
            .init(maximumDistance: Measurement(value: 999, unit: .meters), roundingIncrement: 1, maximumFractionDigits: 0),
            .init(maximumDistance: Measurement(value: 999, unit: .kilometers), roundingIncrement: 0.0001, maximumFractionDigits: 1)
        ])
        
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CarplayRoutePreview.UserClick.lets_go, screenName: ParameterName.CarplayRoutePreview.screen_view)
        
        NavigationSettings.shared.distanceUnit = .kilometer
        carPlayManager.carPlayNavigationViewController?.compassView.isHidden = true
        carPlayManager.carPlayNavigationViewController?.speedLimitView.regulatoryBorderColor = .gray
        
        navigationRootTemplate = carPlayManager.interfaceController?.rootTemplate as? CPMapTemplate
        
        guard let navigationRootTemplate = navigationRootTemplate else { return }
        // Render part of the route that has been traversed with full transparency, to give the illusion of a disappearing route.
        carPlayManager.carPlayNavigationViewController?.routeLineTracksTraversal = true
        
        navigationRootTemplate.hideTripPreviews()
        
        // get estimate arrival time, update tripETA if it's not nil (no way to get the estimate time when contact name is selected)
        // TODO: Is there a better way?
        var newTripETA: TripETA?
        if let eta = self.tripETA {
            
            var finalETAMessage = eta.message
            if let cpRP = self.carRPController,let route = cpRP.route, let trip = self.trip {
                
                let estimatedTime = estimatedArrivalTime((route.expectedTravelTime))
                finalETAMessage = "\(AWSAuth.sharedInstance.userName) is on the way to \(trip.destination.name ?? "") and will approximately arrive by \(estimatedTime.time)\(estimatedTime.format)."
                
            }
            newTripETA = TripETA(type: eta.type, message: finalETAMessage, recipientName: eta.recipientName, recipientNumber: eta.recipientNumber, shareLiveLocation: eta.shareLiveLocation, tripStartTime: eta.tripStartTime, tripEndTime: Int(Date().timeIntervalSince1970 + service.route.expectedTravelTime), tripDestLat: eta.tripDestLat, tripDestLong: eta.tripDestLat, destination: eta.destination, tripEtaFavouriteId: eta.tripEtaFavouriteId)
            self.tripETA = newTripETA
        }
        carRPController = nil
        
        guard let mapboxService = carPlayManager.carPlayNavigationViewController?.navigationService as? MapboxNavigationService,
              let traitCollection =  carPlayManager.carPlayMapViewController?.traitCollection else {
            return
        }
        
        self.enterIntoNavigation(from: .carPlay, navigationService: mapboxService, with: self.tripETA)
        
        
        if let leadingButtons = self.carPlayManager(carPlayManager, leadingNavigationBarButtonsCompatibleWith: traitCollection, in: navigationRootTemplate, for: .navigating),
           let trailingButtons = self.carPlayManager(carPlayManager, trailingNavigationBarButtonsCompatibleWith: traitCollection, in: navigationRootTemplate, for: .navigating) {
            navigationRootTemplate.leadingNavigationBarButtons = leadingButtons
            navigationRootTemplate.trailingNavigationBarButtons = trailingButtons
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2)  {
//            NotificationCenter.default.post(name: .carplayConnectionShowFullListStepTable, object: true)
            NotificationCenter.default.post(name: .carplayConnectionDidChangeStatus, object: true)
        }
        
    }
    
    func carPlayManagerDidEndNavigation(_ carPlayManager: CarPlayManager) {
        // Dismiss NavigationViewController if it's present in the navigation stack
        //currentAppRootViewController?.dismissActiveNavigationViewController()
        //        if let navVC = self.navigationController {
        //            navVC.navigationService.stop()
        //            navVC.dismiss(animated: true,completion: nil)
        //            navigationController = nil
        //        }
        
        carPlayManager.carPlayNavigationViewController?.navigationService.stop()
//        DispatchQueue.main.async {
//            NotificationCenter.default.post(name: .carplayConnectionShowFullListStepTable, object: false)
//        }
        
        appDelegate().navigationViewModel?.removeCarparks()
        appDelegate().mapLandingViewModel?.carPlayMapViewUpdatable?.cruiseStatusUpdate(isCruise: false)
        self.enterHome()
    }
    
    /// NOTICE: This is triggered by mobile `    func navigationViewController(_ navigationViewController: NavigationViewController, didArriveAt waypoint: Waypoint) -> Bool {` in TurnByTurnNavigationVC
    func carPlayManager(_ carPlayManager: CarPlayManager, shouldPresentArrivalUIFor waypoint: Waypoint) -> Bool {
        
        SwiftyBeaver.debug("Car Play arrival")
        
        DispatchQueue.main.async {
            
//            carPlayManager.carPlayNavigationViewController.snapsUserLocationAnnotationToRoute = false
            carPlayManager.carPlayNavigationViewController?.navigationService.start()
        }
        // Navigation end, we'll end navigation automatically and we don't care if the rating screen shown on mobile or not
        let action = CPAlertAction(title: Constants.cpNavEndAlert,
                                   style: .destructive,
                                   handler: {_ in
            
            self.tripETA = nil
            if let navigationViewModel = self.navigationViewModel,let carPlayUpdatable = navigationViewModel.carPlayMapViewUpdatable{
                carPlayUpdatable.cpFeatureDisplay = nil
                carPlayUpdatable.cpFeatureDisplayClose = false
            }
            carPlayManager.carPlayNavigationViewController?.navigationService.stop()
            self.enterHome()
#if DEMO
            if(DemoSession.shared.isDemoRoute){
                let manager = LocalNotificationManager()
                manager.requestPermission()
                manager.addNotification(title: "Let's make parking a Breeze!",subTitle: "Start your parking!")
            }
#endif
        })
        
        let arrivalAlert = CPNavigationAlert(titleVariants: [Constants.cpArrival], subtitleVariants: nil, image: nil, primaryAction: action, secondaryAction: nil, duration: 0)
        self.navigationRootTemplate?.present(navigationAlert: arrivalAlert, animated: true)
        
        return false
        
    }
    
    func carPlayManager(_ carPlayManager: CarPlayManager, didPresent navigationViewController: CarPlayNavigationViewController) {
        
        print("Car Play Present Delegate")
        //        carPlayManager.carPlayNavigationViewController?.styleManager?.automaticallyAdjustsStyleForTimeOfDay = false
        //        carPlayManager.carPlayNavigationViewController?.styleManager?.styles = isDarkMode() ? [CustomNightStyle()] :[CustomDayStyle()]
        
        // here
        
        //        navigationViewController.wayNameView.isHidden = true
        //        carPlayManager.carPlayNavigationViewController?.navigationMapView?.mapView.mapboxMap.onNext(.mapLoaded, handler: { [weak self] _ in
        //            guard let self = self else { return }
        //
        //
        //        })
        
    }
    
    
    func isDarkMode() -> Bool
    {
        if UITraitCollection.current.userInterfaceStyle == .dark {
            return true
        }
        else {
            return false
        }
    }
    /*func favoritesListTemplate() -> CPListTemplate {
     let mapboxSFItem = CPListItem(text: FavoritesList.POI.mapboxSF.rawValue,
     detailText: FavoritesList.POI.mapboxSF.subTitle)
     let timesSquareItem = CPListItem(text: FavoritesList.POI.timesSquare.rawValue,
     detailText: FavoritesList.POI.timesSquare.subTitle)
     mapboxSFItem.userInfo = [CarPlayWaypointKey: Waypoint(location: FavoritesList.POI.mapboxSF.location)]
     timesSquareItem.userInfo = [CarPlayWaypointKey: Waypoint(location: FavoritesList.POI.timesSquare.location)]
     let listSection = CPListSection(items: [mapboxSFItem, timesSquareItem])
     return CPListTemplate(title: "Favorites List", sections: [listSection])
     }*/
    
    
    
    func carPlayManager(_ carPlayManager: CarPlayManager, leadingNavigationBarButtonsCompatibleWith traitCollection: UITraitCollection, in template: CPTemplate, for activity: CarPlayActivity) -> [CPBarButton]? {
        guard let interfaceController = self.carPlayManager.interfaceController else {
            return nil
        }
        
        switch activity {
        case .browsing:
            
            if cruiseViewModel == nil {
                self.cpBarBrowseButtons.delegate = self
                return self.cpBarBrowseButtons.createLeadingNavigationButtons(interfaceController: interfaceController, traitCollection: traitCollection)
            } else {
                self.cpBarNavButtons.delegate = self
                return self.cpBarNavButtons.createCruiseLeadingNavigationButtons(interfaceController: interfaceController, traitCollection: traitCollection, isMute: cruiseViewModel!.isMute)
            }
            
        case .previewing:
            // route planning
            guard let mapTemplate = template as? CPMapTemplate else { return nil }
            
            mapTemplate.automaticallyHidesNavigationBar = false
            
            self.cpBarPreviewButtons.delegate = self
            return self.cpBarPreviewButtons.createRPPreviewLeadingNavigationButtons(interfaceController: interfaceController, traitCollection: traitCollection)
            
        case .navigating:
            
            if(self.navigationViewModel == nil)
            {
                if(self.cruiseViewModel != nil){
                    self.cpBarNavButtons.delegate = self
                    return self.cpBarNavButtons.createCruiseLeadingNavigationButtons(interfaceController: interfaceController, traitCollection: traitCollection)
                }
                else{
                    self.cpBarNavButtons.delegate = self
                    return self.cpBarNavButtons.createNavLeadingNavigationButtons(interfaceController: interfaceController, traitCollection: traitCollection)
                }
            }
            else
            {
                self.cpBarNavButtons.delegate = self
                return self.cpBarNavButtons.createNavLeadingNavigationButtons(interfaceController: interfaceController, traitCollection: traitCollection)
            }
            
        case  .panningInBrowsingMode:
            return nil
        case .panningInNavigationMode:
            return nil
        }
    }
    
    func carPlayManager(_ carPlayManager: CarPlayManager,
                        didAdd finalDestinationAnnotation: PointAnnotation,
                        to parentViewController: UIViewController,
                        pointAnnotationManager: PointAnnotationManager) {
        
        var finalDestinationAnnotation = finalDestinationAnnotation
        if let image = UIImage(named: "destinationMark") {
            
            finalDestinationAnnotation.image = PointAnnotation.Image.init(image: image, name: "carplayNavigtaion")
        }
        
        pointAnnotationManager.annotations = [finalDestinationAnnotation]
        
    }
    
    func carPlayManager(_ carPlayManager: CarPlayManager, didFailToFetchRouteBetween waypoints: [Waypoint]?, options: RouteOptions, error: DirectionsError) -> CPNavigationAlert? {
        let okTitle = NSLocalizedString("CARPLAY_OK", bundle: .main, value: "OK", comment: "CPAlertTemplate OK button title")
        let action = CPAlertAction(title: okTitle, style: .default, handler: {_ in
            
            //We don't need CPNavigationAlert action here
        })
        let alert = CPNavigationAlert(titleVariants: [error.localizedDescription],
                                      subtitleVariants: [error.failureReason ?? ""],
                                      image: nil,
                                      primaryAction: action,
                                      secondaryAction: nil,
                                      duration: 5)
        return alert
    }
    
    func carPlayManager(_ carPlayManager: CarPlayManager, trailingNavigationBarButtonsCompatibleWith traitCollection: UITraitCollection, in template: CPTemplate, for activity: CarPlayActivity) -> [CPBarButton]? {
        
        guard let interfaceController = self.carPlayManager.interfaceController else {
            return nil
        }
        switch activity {
        case .previewing:
            
//            self.cpBarPreviewButtons.delegate = self
//            return self.cpBarPreviewButtons.createRPPreviewTralingNavigationButtons(interfaceController: interfaceController, traitCollection: traitCollection)
            return nil
        case .browsing:
            
            if cruiseViewModel == nil {
                self.cpBarBrowseButtons.delegate = self
                return self.cpBarBrowseButtons.createTrailingNavigationButtons(interfaceController: interfaceController, traitCollection: traitCollection)
            }else {
                self.cpBarNavButtons.delegate = self
                return self.cpBarNavButtons.createCruiseTralingNavigationButtons(interfaceController: interfaceController, traitCollection: traitCollection, isSharingDrive: cruiseViewModel!.isLiveLocationSharingEnabled)
            }
            
        case .navigating:
            
            if(self.navigationViewModel == nil)
            {
                if(cruiseViewModel != nil){
                    self.cpBarNavButtons.delegate = self
                    return self.cpBarNavButtons.createCruiseTralingNavigationButtons(interfaceController: interfaceController, traitCollection: traitCollection)
                }
                else
                {
                    self.cpBarNavButtons.delegate = self
                    return self.cpBarNavButtons.createNavTralingNavigationButtons(interfaceController: interfaceController, traitCollection: traitCollection)
                }
            }
            
            else
            {
                self.cpBarNavButtons.delegate = self
                return self.cpBarNavButtons.createNavTralingNavigationButtons(interfaceController: interfaceController, traitCollection: traitCollection)
            }
            
        case .panningInBrowsingMode:
            return nil
        case .panningInNavigationMode:
            return nil
        }
    }
    
    
    func carPlayManager(_ carPlayManager: CarPlayManager, mapButtonsCompatibleWith traitCollection: UITraitCollection, in template: CPTemplate, for activity: CarPlayActivity) -> [CPMapButton]? {
        switch activity {
        case .browsing:
            guard let mapViewController = carPlayManager.carPlayMapViewController,
                  let mapTemplate = template as? CPMapTemplate else {
                return nil
            }
            
            let zoomInButton = CPMapButton { [weak self] button in
                guard let self = self else { return }
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CarplayHomepage.UserClick.mapbox_zoomin, screenName: ParameterName.CarplayHomepage.screen_view)
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CarplayCruise.UserClick.mapbox_zoomin, screenName: ParameterName.CarplayCruise.screen_view)
                
                self.carPlayManager.navigationMapView?.carPlayZoomIn()
            }
            
            zoomInButton.image = UIImage(named: "zoom in", in: nil, compatibleWith: traitCollection)
            
            
            let zoomOutButton = CPMapButton { [weak self] button in
                guard let self = self else { return }
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CarplayHomepage.UserClick.mapbox_zoomout, screenName: ParameterName.CarplayHomepage.screen_view)
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CarplayCruise.UserClick.mapbox_zoomout, screenName: ParameterName.CarplayCruise.screen_view)
                
                self.carPlayManager.navigationMapView?.carPlayZoomOut()
            }
            
            zoomOutButton.image = UIImage(named: "zoom out", in: nil, compatibleWith: traitCollection)
            
            var mapButtons = [mapViewController.recenterButton,
                              zoomInButton,
                              zoomOutButton]
            mapButtons.insert(mapViewController.panningInterfaceDisplayButton(for: mapTemplate), at: 1)
            mapTemplate.automaticallyHidesNavigationBar = false
            return mapButtons
        case .previewing, .panningInBrowsingMode:
            guard let mapViewController = carPlayManager.carPlayMapViewController,
                  let mapTemplate = template as? CPMapTemplate else {
                return nil
            }
            mapTemplate.automaticallyHidesNavigationBar = false
            return nil
        case .navigating:
            guard let mapTemplate = template as? CPMapTemplate else {
               
                return nil
            }
            mapTemplate.automaticallyHidesNavigationBar = false
            let zoomInButton: CPMapButton = {
                let zoomInButton = CPMapButton { button in
                    
                    //                    let mapTemplate = template as? CPMapTemplate
                    //                    mapTemplate?.showPanningInterface(animated: true)
                    
                    guard let navigationMapView = carPlayManager.carPlayNavigationViewController?.navigationMapView,
                          let mapView = navigationMapView.mapView else { return }
                    AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CarplayNavigation.UserClick.mapbox_zoomin, screenName: ParameterName.CarplayNavigation.screen_view)
                    
                    navigationMapView.carPlayZoomIn()
                    //                    navigationMapView.navigationCamera.stop()
                    //
                    //                    var cameraOptions = CameraOptions(cameraState: mapView.cameraState)
                    //                    cameraOptions.zoom = mapView.cameraState.zoom + 1.0
                    //                    mapView.mapboxMap.setCamera(to: cameraOptions)
                }
                
                zoomInButton.image = UIImage(named: "zoom in", in: nil, compatibleWith: traitCollection)
                return zoomInButton
                
            }()
            
            let zoomOutButton: CPMapButton = {
                let zoomOutButton = CPMapButton { button in
                    
                    //                    let mapTemplate = template as? CPMapTemplate
                    //                    mapTemplate?.showPanningInterface(animated: true)
                    
                    guard let navigationMapView = carPlayManager.carPlayNavigationViewController?.navigationMapView,
                          let mapView = navigationMapView.mapView else { return }
                    AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CarplayNavigation.UserClick.mapbox_zoomout, screenName: ParameterName.CarplayNavigation.screen_view)
                    
                    navigationMapView.carPlayZoomOut()
                    //
                    //                    navigationMapView.navigationCamera.stop()
                    //
                    //                    var cameraOptions = CameraOptions(cameraState: mapView.cameraState)
                    //                    cameraOptions.zoom = mapView.cameraState.zoom - 1.0
                    //                    mapView.mapboxMap.setCamera(to: cameraOptions)
                }
                
                zoomOutButton.image = UIImage(named: "zoom out", in: nil, compatibleWith: traitCollection)
                return zoomOutButton
            }()
            
            let recenterButton: CPMapButton = {
                let recenter = CPMapButton { button in
                    guard let navigationMapView = carPlayManager.carPlayNavigationViewController?.navigationMapView,
                          let mapView = navigationMapView.mapView else { return }
                    
                    navigationMapView.navigationCamera.follow()
                }
                
                let bundle = Bundle.mapboxNavigation
                recenter.image = UIImage(named: "carplay_locate", in: bundle, compatibleWith: traitCollection)
                
                return recenter
            }()
            
            // Create pan button to be able to use it during active-guidance navigation.
            let panButton = CPMapButton { [weak mapTemplate] _ in
                guard let mapTemplate = mapTemplate else { return }
                if !mapTemplate.isPanningInterfaceVisible {
                    mapTemplate.showPanningInterface(animated: true)
                }
            }
            
            let bundle = Bundle.mapboxNavigation
            panButton.image = UIImage(named: "carplay_pan", in: bundle, compatibleWith: traitCollection)
            
            return [
                recenterButton,
                zoomInButton,
                zoomOutButton,
                panButton
            ]
            
        case .panningInNavigationMode:
            return nil
        }
    }
    
    func carPlayManager(_ carPlayManager: CarPlayManager, willPreview trip: CPTrip) -> CPTrip {
        
        guard let routePlanningModel = self.routePlanningViewModel else { return trip }
        var routeTempIndex = 0
        var routeResponse:RouteResponse?
        var routeChoices: [CPRouteChoice] = []
        trip.routeChoices.forEach {
            if routePlanningModel.carPlayMapViewUpdatable == nil && routePlanningModel.mobileRPMapViewUpdatable != nil {
                
                routeResponse = routePlanningModel.mobileRPMapViewUpdatable?.response
            }
            else if(routePlanningModel.carPlayMapViewUpdatable != nil){
                
                routeResponse = routePlanningModel.carPlayMapViewUpdatable?.response
                
            }
            
            guard let finalResponse = routeResponse,
                  let routes = finalResponse.routes, !routes.isEmpty, routeTempIndex < routes.count else { return }
            
            let route =  routes[routeTempIndex]
            
            var erpValue = "$0.00"
            
            if(appDelegate().routePlanningViewModel?.carPlayMapViewUpdatable?.erpPriceUpdater != nil)
            {
                erpValue = appDelegate().routePlanningViewModel?.carPlayMapViewUpdatable?.erpPriceUpdater.newComputeTollsForCarPlayRP(route: route, erpTolls: DataCenter.shared.getAllERPFeatureCollection()) ?? "$0.00"
            }
            
            let selectionSummaryVariants = [
                "\(CPTrip.shortDateComponentsFormatter.string(from: route.expectedTravelTime) ?? "") | \(erpValue)"
            ]
            let routeChoice = CPRouteChoice(summaryVariants: [route.description],
                                            additionalInformationVariants: [route.description],
                                            selectionSummaryVariants: selectionSummaryVariants)
            
            routeChoice.userInfo = $0.userInfo
            
            routeChoices.append(routeChoice)
            
            routeTempIndex = routeTempIndex + 1
        }
        
        let modifiedTrip = CPTrip(origin: trip.origin,
                                  destination: trip.destination,
                                  routeChoices: routeChoices)
        
        return modifiedTrip
        
    }
    
    func carPlayManager(_ carPlayManager: CarPlayManager, willPreview trip: CPTrip, with previewTextConfiguration: CPTripPreviewTextConfiguration) -> CPTripPreviewTextConfiguration {
        
        return defaultTripPreviewTextConfiguration()
    }
    
    private func needUpdateERPPrice(_ routes: [Route]) -> Bool {
        var retValue: Bool = false
        
        var counter = 0
        for route in routes {
            
            if counter < self.trip?.routeChoices.count ?? 0 {
                let routeChoice = self.trip?.routeChoices[counter]
                
                let erpValue: String = self.routePlanningViewModel?.carPlayMapViewUpdatable?.erpPriceUpdater.newComputeTollsForCarPlayRP(route: route, erpTolls: DataCenter.shared.getAllERPFeatureCollection()) ?? "$0.0"
                
                let summaryVariants = "\(CPTrip.shortDateComponentsFormatter.string(from: route.expectedTravelTime) ?? "") | \(erpValue)"
                
                if routeChoice?.selectionSummaryVariants?.first != summaryVariants {
                    retValue = true
                    break
                }
            }
            
            counter += 1
        }
        
        return retValue
    }
    
    func carPlayManager(_ carPlayManager: CarPlayManager, selectedPreviewFor trip: CPTrip, using routeChoice: CPRouteChoice) {
        
        AnalyticsManager.shared.logScreenView(screenName:ParameterName.CarplayRoutePreview.screen_view)
        
        guard let routePlanningModel = self.routePlanningViewModel,
              let carPlayMapViewUpdatable = routePlanningModel.carPlayMapViewUpdatable,
              let routeResponse = carPlayMapViewUpdatable.response,
              let routes = routeResponse.routes else { return }
        
        guard  let routeIndex = trip.routeChoices.firstIndex(where: { $0 == routeChoice }) else { return }
        self.isCarPlayPreview = true
        self.trip = trip
        let route = routes[routeIndex]
        self.carRPController?.tripSelected(route: route)
        
        if(self.routePlanningViewModel?.carPlayMapViewUpdatable?.erpPriceUpdater != nil)
        {
            let _ = self.routePlanningViewModel?.carPlayMapViewUpdatable?.erpPriceUpdater.newComputeTollsForCarPlayRP(route: route, erpTolls: DataCenter.shared.getAllERPFeatureCollection()) ?? "$0.0"
            
            self.routePlanningViewModel?.carPlayMapViewUpdatable?.erpPriceUpdater.addERPItemsToMapOnCarPlay()
        }
        
        let routeDistance = Measurement(distance: route.distance).localized(into: Locale.enGBLocale())
        
        let estimates = CPTravelEstimates(distanceRemaining: routeDistance, timeRemaining: route.expectedTravelTime)
        
        if(self.trip != nil)
        {
            if let mapTemplate = self.carPlayManager.interfaceController?.topTemplate as? CPMapTemplate {
                mapTemplate.update(estimates, for: self.trip!, with: CPTimeRemainingColor.default)
            }
        }
    }
    
    private func defaultTripPreviewTextConfiguration() -> CPTripPreviewTextConfiguration{
        
        let goTitle = "Let's go!"
        
        let alternativeRoutesTitle = "Other routes"
        
        let overviewTitle = "Overview"
        
        let defaultPreviewText = CPTripPreviewTextConfiguration(startButtonTitle: goTitle,
                                                                additionalRoutesButtonTitle: alternativeRoutesTitle,
                                                                overviewButtonTitle: overviewTitle)
        return defaultPreviewText
        
    }
    
    func carPlayManager(_ carPlayManager: CarPlayManager, templateDidAppear template: CPTemplate, animated: Bool) {
        
        if let mapTemplate = template as? CPMapTemplate {
            if(mapTemplate.mapButtons.count > 0){
                self.isCarPlayPreview = false
                self.insideCP = false
                
                let templates = carPlayManager.interfaceController?.templates ?? []
                if let routePlanningViewModel = routePlanningViewModel,
                   let carPlayMapViewUpdatable = routePlanningViewModel.carPlayMapViewUpdatable,
                   let erpPriceUpdater = carPlayMapViewUpdatable.erpPriceUpdater,
                   templates.count > 1 {
                    
                    erpPriceUpdater.removeLayer()
                }
                
            }
        }
    }
    
    func carPlayManager(_ carPlayManager: CarPlayManager, templateWillAppear template: CPTemplate, animated: Bool) {
        
        //This is just a templateAppear method
    }
    
    func carPlayManager(_ carPlayManager: CarPlayManager, templateDidDisappear template: CPTemplate, animated: Bool) {
        
        //This is just a templateDisAppear method
    }
    
    func carPlayManager(_ carPlayManager: CarPlayManager, templateWillDisappear template: CPTemplate, animated: Bool) {
        
        if(template as? CPMapTemplate) != nil {
            
            let mapTemplate = template as? CPMapTemplate
            
            if(mapTemplate?.trailingNavigationBarButtons.count == 2 && mapTemplate?.mapButtons.count == 0)
            {
                if let cpRPPark = self.cpRPCarparkVC{
                    
                    if(self.notifyPreview == false)
                    {
                        if cpRPPark.carparkViewModel != nil{
                            
                            cpRPPark.hideCarparks(mapTopTemplate: mapTemplate)
                        }
                        
                        self.cpRPCarparkVC = nil
                        self.insideCP = false
                        carPlayManager.navigationMapView?.removeRoutes()
                        carPlayManager.navigationMapView?.removeWaypoints()
                    }
                    
                    
                }
                else{
                    
                    if let routePlanningViewModel = routePlanningViewModel, let carPlayMapViewUpdatable = routePlanningViewModel.carPlayMapViewUpdatable, let erpPriceUpdater = carPlayMapViewUpdatable.erpPriceUpdater {
                        
                        erpPriceUpdater.removeLayer()
                    }
                }
            }
            else{
                
                self.notifyPreview = false
            }
        }
        
    }
    
    func fitCamera(){
        
        let routes:[Route] = (appDelegate().routePlanningViewModel?.carPlayMapViewUpdatable?.response?.routes)!
        for route in routes {
            guard let routeShape = route.shape, !routeShape.coordinates.isEmpty else { return }
        }
        
        let multiLineString = MultiLineString(routes.map({ route in route.shape!.coordinates
            
        }))
        let edgeInsets = self.carPlayManager.navigationMapView!.safeViewArea + UIEdgeInsets().centerCameraEdgeInsets
        if let cameraOptions = self.carPlayManager.navigationMapView?.mapView?.mapboxMap.camera(for: .multiLineString(multiLineString),
                                                                                                padding: edgeInsets,
                                                                                                bearing: nil,
                                                                                                pitch: nil) {
            self.carPlayManager.navigationMapView?.mapView?.mapboxMap.setCamera(to: cameraOptions)
        }
    }
    
    // MARK: Overspeed related
    // TODO: Make it better by using generics to reuse the code
    func cruiseOverSpeed(value: Bool) {
        value ? startCruiseOverSpeedAnimation() : stopCruiseOverSpeedAnimation()
    }
    
    func navigationOverSpeed(value: Bool) {
        value ? startNavigationOverSpeedAnimation() : stopNavigationOverSpeedAnimation()
    }
    
    func startCruiseOverSpeedAnimation() {
        guard let cpViewController = self.carPlayManager.carPlayMapViewController else { return }
        if cpViewController.speedLimitView.regulatoryBorderColor != .red {
            cpViewController.speedLimitView.regulatoryBorderColor = .red
        }
    }
    
    func stopCruiseOverSpeedAnimation() {
        guard let cpViewController = self.carPlayManager.carPlayMapViewController else { return }
        if cpViewController.speedLimitView.regulatoryBorderColor != .gray {
            cpViewController.speedLimitView.regulatoryBorderColor = .gray
        }
        /*
         guard let cpViewController = self.carPlayManager.carPlayMapViewController, let overSpeedCancellable = self.overSpeedCancellable else { return }
         
         cpViewController.speedLimitView.regulatoryBorderColor = .gray
         overSpeedCancellable.cancel()
         self.overSpeedCancellable = nil
         */
    }
    
    func startNavigationOverSpeedAnimation() {
        guard let cpViewController = self.carPlayManager.carPlayNavigationViewController else { return }
        if cpViewController.speedLimitView.regulatoryBorderColor != .red {
            cpViewController.speedLimitView.regulatoryBorderColor = .red
        }
        /*
         guard let cpViewController = self.carPlayManager.carPlayNavigationViewController, navigationOverSpeedCancellable == nil else { return }
         
         navigationOverSpeedAnimatedCount = 0
         
         self.navigationOverSpeedCancellable = Timer.publish(every: 1, on: .main, in: .default)
         .autoconnect()
         .sink(receiveValue: { _ in
         
         self.navigationOverSpeedAnimatedCount += 1
         if self.navigationOverSpeedAnimatedCount > 3 {
         self.navigationOverSpeedCancellable?.cancel()
         return
         }
         
         //                UIView.transition(with: cpViewController.view, duration: Values.standardAnimationDuration, options: .transitionCrossDissolve, animations: {
         //                    cpViewController.speedLimitView.regulatoryBorderColor = .gray
         //                }, completion: { _ in
         //                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
         //                        cpViewController.speedLimitView.regulatoryBorderColor = .red
         //                    }
         //                })
         
         cpViewController.speedLimitView.regulatoryBorderColor = .gray
         DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
         cpViewController.speedLimitView.regulatoryBorderColor = .red
         }
         
         })
         */
    }
    
    func stopNavigationOverSpeedAnimation() {
        guard let cpViewController = self.carPlayManager.carPlayNavigationViewController else { return }
        if cpViewController.speedLimitView.regulatoryBorderColor != .gray {
            cpViewController.speedLimitView.regulatoryBorderColor = .gray
        }
        /*
         guard let cpViewController = self.carPlayManager.carPlayNavigationViewController, let navigationOverSpeedCancellable = self.navigationOverSpeedCancellable else { return }
         
         cpViewController.speedLimitView.regulatoryBorderColor = .gray
         navigationOverSpeedCancellable.cancel()
         self.navigationOverSpeedCancellable = nil
         */    }
    
}


//@available(iOS 12.0, *)
extension AppDelegate: SearchControllerDelegate {
    
    func searchPreviewRoutes(to waypoint: Waypoint, address: SearchAddresses) {
        SwiftyBeaver.debug("CarPlay Route Planning fetch routes")
        carRPController =  CarPlayRPController(navigationMapView: carPlayManager.navigationMapView!, carPlayManager: carPlayManager, address: address)
        carRPController?.getRoutes()
    }
    
    
    func resetSearchPanButtons(_ mapTemplate: CPMapTemplate) {
        
        carPlayManager.resetPanButtons(mapTemplate)
        
    }
    
    func pushSearchTemplate(_ template: CPTemplate, animated: Bool) {
        
        if let listTemplate = template as? CPListTemplate {
            listTemplate.delegate = carPlaySearchController
        }
        carPlayManager.interfaceController?.pushTemplate(template, animated: animated)
        
    }
    
    func popFromSearchTemplate(animated: Bool) {
        
        if let templates = appDelegate().carPlayManager.interfaceController?.templates{
            if(templates.count > 1){
                
                if #available(iOS 14, *) {
                    
                    // pop(to targetTemplate: CPTemplate, animated: Bool, completion: ((Bool, Error?) -> Void)? = nil)
                    carPlayManager.interfaceController?.pop(to: templates[1], animated: true,completion: { value, error in
                        if(!value){
                            SwiftyBeaver.debug("Pop from Search template failed", "\(value)","\(String(describing: error))")
                        }
                    })
                    //                    carPlayManager.interfaceController?.popTemplate(animated: true, completion: { value, error in
                    //                        if(!value){
                    //                            SwiftyBeaver.debug("Pop from Search template failed", "\(value)","\(String(describing: error))")
                    //                        }
                    //
                    //                    })
                }
                else{
                    carPlayManager.interfaceController?.popTemplate(animated: true)
                }
                
            }
        }
        
    }
    
    func resetPanButtons(_ mapTemplate: CPMapTemplate) {
        carPlayManager.resetPanButtons(mapTemplate)
    }
    
    func pushTemplate(_ template: CPTemplate, animated: Bool) {
        if let listTemplate = template as? CPListTemplate {
            listTemplate.delegate = carPlaySearchController as? CPListTemplateDelegate
        }
        carPlayManager.interfaceController?.pushTemplate(template, animated: animated)
    }
    
    func popTemplate(animated: Bool, completion: ((Bool, Error?) -> Void)? = nil) {
        if let templates = appDelegate().carPlayManager.interfaceController?.templates{
            if(templates.count > 1){
                
                if #available(iOS 14, *) {
                    
                    carPlayManager.interfaceController?.popTemplate(animated: true, completion: { value, error in
                        if(!value){
                            SwiftyBeaver.debug("Pop from Search template failed", "\(value)","\(String(describing: error))")
                        }
                        completion?(value, error)
                        
                    })
                }
                else{
                    carPlayManager.interfaceController?.popTemplate(animated: true)
                    completion?(true, nil)
                }
                
            } else {
                completion?(true, nil);
            }
        }
    }
}

@available(iOS 12.0, *)
extension AppDelegate: CPListTemplateDelegate {
    func listTemplate(_ listTemplate: CPListTemplate, didSelect item: CPListItem, completionHandler: @escaping () -> Void) {
        // Selected a favorite
        if let contact = item.userInfo as? RecentContact {
            showShareDriveConfirmation(contact: contact, listTemplate: listTemplate)
        } else {
            let userInfo = item.userInfo as? [String: Any]
            let address = userInfo![SearchController.CarPlayRNSearchAPIKey]
            if (address as? EasyBreezyAddresses) != nil {
                
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CarplayFavourite.UserClick.select_destination, screenName: ParameterName.CarplayFavourite.screen_view)
                self.insideCP = false
                let addressEB = address as? EasyBreezyAddresses
                
                let searchAddress = SearchAddresses.init(alias: "", address1: (addressEB?.address1)!, lat: (addressEB?.lat)!, long: (addressEB?.long)!, address2: (addressEB?.address2)!, distance: "")
                
                let destLocation = CLLocation(latitude: (searchAddress.lat?.toDouble())!, longitude: (searchAddress.long?.toDouble())!)
                let destinationWaypoint = Waypoint(location: destLocation)
                self.popFromSearchTemplate(animated: false)
                self.searchPreviewRoutes(to: destinationWaypoint, address: searchAddress)
            }
            
            else if (address as? SavedPlaces) != nil {
                
                let addressLocal = address as? SavedPlaces
                
                let searchAddress = SearchAddresses.init(alias: "", address1: (addressLocal?.placeName)!, lat: (addressLocal?.lat)!, long: (addressLocal?.long)!, address2: (addressLocal?.address)!, distance: "")
                self.insideCP = false
                let destLocation = CLLocation(latitude: (searchAddress.lat?.toDouble())!, longitude: (searchAddress.long?.toDouble())!)
                let destinationWaypoint = Waypoint(location: destLocation)
                self.popFromSearchTemplate(animated: false)
                self.searchPreviewRoutes(to: destinationWaypoint, address: searchAddress)
            }
            
            else if (address as? Favourites) != nil {
                
                if(self.isCarPlayPreview)
                {
                    guard let etaFav = address as? Favourites else { return }
                    
                    self.carRPController?.getTripETA(etaFav: etaFav)
                    self.popFromSearchTemplate(animated: false)
                }
                else
                {
                    guard let etaFav = address as? Favourites else { return }
                    self.mapLandingViewModel?.startCruise(fav: etaFav)
                    self.popFromSearchTemplate(animated: false)
                }
                
            }
        }
    }
    
    private func showShareDriveConfirmation(contact: RecentContact,listTemplate: CPListTemplate) {
        
        let isCruise = appDelegate().cruiseViewModel != nil
        let isNavigation = appDelegate().navigationViewModel != nil
        let message = (isCruise || isNavigation) ? "You will share your drive with:: \(contact.recipientName ?? "")" :
        "You will share your drive with:: \(contact.recipientName ?? "") once you start navigation."
        
        // 1. Show action sheet
        let template = CPActionSheetTemplate(title: Constants.cpShareDriveConfirmationTitle, message: message , actions: [CPAlertAction(title: "Cancel", style: CPAlertAction.Style.default, handler: { action in
            self.carPlayManager.interfaceController?.popTemplate(animated: true)
        }), CPAlertAction(title: "OK", style: CPAlertAction.Style.cancel, handler: { [weak self] action in
            guard let self = self else { return }
            self.carPlayManager.interfaceController?.popTemplate(animated: true)
            // cruise mode
            if let vm = appDelegate().cruiseViewModel {
                // 2. start share drive
                ETATrigger.shared.tripCruiseETA = TripCruiseETA(type: TripETA.ETAType.Init, message: "\(AWSAuth.sharedInstance.userName) is sharing real-time location to you on Breeze", recipientName: contact.recipientName ?? "", recipientNumber: contact.recipientNumber ?? "", shareLiveLocation: "Y", tripStartTime: Int(Date().timeIntervalSince1970), tripDestLat: 0, tripDestLong: 0, destination: "", tripEtaFavouriteId: contact.tripEtaFavouriteId ?? 0)
                vm.setupETA() // This will trigger share drive button changed
            } else if let vm = appDelegate().navigationViewModel { // navigation mode
                
                vm.addETA(recipientName: contact.recipientName ?? "", recipientNumber: contact.recipientNumber ?? "", message: "", etaFavoriteId: contact.tripEtaFavouriteId ?? 0)
            } else { // route planning
                self.tripETA = TripETA(type: TripETA.ETAType.Init, message: message, recipientName: contact.recipientName ?? "", recipientNumber: contact.recipientNumber ?? "", shareLiveLocation: "Y", tripStartTime: Int(Date().timeIntervalSince1970), tripEndTime: 0, tripDestLat: 0, tripDestLong: 0, destination: "", tripEtaFavouriteId: contact.tripEtaFavouriteId ?? 0)
                self.carPlayMapController?.updateCPCruiseToggleTitle(activity: .previewing, isEnabled: true)
            }
        })])
        
        self.tryToDismissTemplateIfNeed()
        self.safePresentTemplate(template, animated: true)
    }
    
    // cancel sharing confirmation (in route planning only)
    private func showCancelSharingConfirmation() {
        
        let name = self.tripETA?.recipientName ?? "unknown"
        let message = "\(Constants.cancelShareDriveConfirmation) \(name)"
        
        // 1. Show action sheet
        let template = CPActionSheetTemplate(title: Constants.cpCancelSharingConfirmationTitle, message: message , actions: [CPAlertAction(title: "Yes", style: CPAlertAction.Style.destructive, handler: { [weak self] action in
            guard let self = self else { return }
            self.tripETA = nil
            self.carPlayMapController?.updateCPCruiseToggleTitle(activity: .previewing, isEnabled: false)
        }), CPAlertAction(title: "No", style: CPAlertAction.Style.default, handler: { action in
            // Do nothing
        })])
        self.tryToDismissTemplateIfNeed()
        self.safePresentTemplate(template, animated: true)
    }
    
    
    func showOBUConnectedTemplate() {
        var title = OBUHelper.shared.vehicleNumber
        if !title.isEmpty {
            title = OBUHelper.shared.vehicleNumber
        } else {
            if let lastObu = Prefs.shared.getLastPairedObu(AWSAuth.sharedInstance.cognitoId, key: .pairedObuList) {
                let nameOBU = lastObu.name ?? ""
                title = maskStringWithAsterisks(nameOBU)
            }
        }
        
//        let cardBalance = OBUHelper.shared.getCardBalance()
        let stringTitle = title + " Connected "
        
//        var messsageString = "Card balance:" + " $\(cardBalance)"
//        if OBUHelper.shared.getCardStatus() != .detected || cardBalance.isEmpty {
//            messsageString = "Card balance:" + " $-"
//        }
        let template = CPActionSheetTemplate(title: stringTitle , message: nil , actions: [CPAlertAction(title: "Close", style: CPAlertAction.Style.`default`, handler: { [weak self] action in
            guard let self = self else { return }
            self.carPlayManager.interfaceController?.dismissTemplate(animated: false)
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CarPlayObuState.UserClick.dhu_popup_obu_connected_back, screenName: ParameterName.CarPlayObuState.dhu_homepage)
            
        })])
        self.tryToDismissTemplateIfNeed()
        self.safePresentTemplate(template, animated: true)
        
    }
    
    
    func maskStringWithAsterisks(_ originalString: String) -> String {
        let lastFourDigits = String(originalString.suffix(4)) // Extract the last 4 digits
        let maskedString = String(repeating: "*", count: originalString.count - 4) + lastFourDigits
        return maskedString
    }
    
    func showOBUReportIssue() {
        let template = CPActionSheetTemplate(title: "Connection Error" , message: "Tap to report issue to Breeze Support." , actions: [CPAlertAction(title: "Report Issue", style: CPAlertAction.Style.`default`, handler: { [weak self] action in
            guard let self = self else { return }
            self.carPlayManager.interfaceController?.dismissTemplate(animated: false)
            appDelegate().mapLandingViewModel?.mobileMapViewUpdatable?.appearInstabug()
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CarPlayObuState.UserClick.dhu_popup_obu_connection_error_report_issue, screenName: ParameterName.CarPlayObuState.dhu_homepage)
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CarPlayObuState.UserClick.dhu_popup_obu_connection_error_back, screenName: ParameterName.CarPlayObuState.dhu_homepage)
            
        })])
        self.tryToDismissTemplateIfNeed()
        self.safePresentTemplate(template, animated: true)
    }
    
    
    func showOBUNotconnected() {
        let template = CPActionSheetTemplate(title: "You have not paired Breeze to OBU." , message: "Use Breeze mobile app to start pairing" , actions: [CPAlertAction(title: "Okay", style: CPAlertAction.Style.`default`, handler: { [weak self] action in
            guard let self = self else { return }
            self.carPlayManager.interfaceController?.dismissTemplate(animated: false)
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CarPlayObuState.UserClick.dhu_popup_obu_not_connected_back, screenName: ParameterName.CarPlayObuState.dhu_homepage)
        })])
        self.tryToDismissTemplateIfNeed()
        self.safePresentTemplate(template, animated: true)
    }
    
    func showOBUConnecting() {
        let template = CPActionSheetTemplate(title: "OBU is connecting..." , message: "" , actions: [CPAlertAction(title: "Okay", style: CPAlertAction.Style.`default`, handler: { [weak self] action in
            guard let self = self else { return }
            self.carPlayManager.interfaceController?.dismissTemplate(animated: false)
        })])
        self.tryToDismissTemplateIfNeed()
        self.safePresentTemplate(template, animated: true)
    }
    
    func showPremissonBluetoothOBU() {
        let template = CPActionSheetTemplate(title: "Not Connected" , message: "Check Bluetooth is enabled and try again." , actions: [CPAlertAction(title: "Retry", style: CPAlertAction.Style.`default`, handler: { [weak self] action in
            guard let self = self else { return }
            self.carPlayManager.interfaceController?.dismissTemplate(animated: false)
            appDelegate().mapLandingViewModel?.retryConnectOBUCarPlay()
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CarPlayObuState.UserClick.dhu_popup_obu_not_connected_retry, screenName: ParameterName.CarPlayObuState.dhu_homepage)
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CarPlayObuState.UserClick.dhu_popup_obu_not_connected_back, screenName: ParameterName.CarPlayObuState.dhu_homepage)
            self.updateStateObuOnCarplay()
        })])
        self.tryToDismissTemplateIfNeed()
        self.safePresentTemplate(template, animated: true)
        
    }
    
    //_ carPlayManager: CarPlayManager, trailingNavigationBarButtonsCompatibleWith traitCollection: UITraitCollection, in template: CPTemplate, for activity: CarPlayActivity
    
    
    
    func updateStateObuOnCarplay() {
        if let interfaceController = self.carPlayManager.interfaceController {
            if let mapTemplate = interfaceController.rootTemplate as? CPMapTemplate {
                if let traitCollection = self.carPlayManager.carPlayMapViewController?.traitCollection {
                    if appDelegate().navigationViewModel != nil {
                        self.cpBarNavButtons.delegate = self
                        mapTemplate.trailingNavigationBarButtons = self.cpBarNavButtons.createNavTralingNavigationButtons(interfaceController: interfaceController, traitCollection: traitCollection)
                    } else if appDelegate().cruiseViewModel  != nil {
                        mapTemplate.trailingNavigationBarButtons = self.cpBarNavButtons.createCruiseTralingNavigationButtons(interfaceController: interfaceController, traitCollection: traitCollection)
                    } else {
                        self.cpBarBrowseButtons.delegate = self
                        mapTemplate.trailingNavigationBarButtons = self.cpBarBrowseButtons.createTrailingNavigationButtons(interfaceController: interfaceController, traitCollection: traitCollection)
                    }
                }
            }
        }
    }
    
    
}

extension AppDelegate {
    
    func carPlayMapStyles() {
        
        carPlayManager.styles = carPlayManager.carPlayMapViewController?.isDarkMode ?? false ? [CustomDayStyle()] : [CustomNightStyle()]
        //        carPlayManager.carPlayMapViewController?.speedLimitView.isHidden = true
        
        //        carPlayManager.navigationMapView?.mapView.mapboxMap.onNext(.mapLoaded, handler: { [weak self] _ in //B
        //
        //            guard let self = self else { return }
        //
        //        })
        
        self.carPlayManager.navigationMapView?.mapView.ornaments.options.compass.visibility = .hidden
        //beta.14 we will use NavigationMapView userLocation Style for Puck
        self.carPlayManager.navigationMapView?.userLocationStyle = .puck2D(configuration: Puck2DConfiguration(topImage: UIImage(named: "puckArrow"), bearingImage: UIImage(named: "puckIcon"), shadowImage: nil, scale: nil))
        
    }
    
    func roadNamePanelFrame() -> CGRect {
        if let vc = carPlayManager.carPlayNavigationViewController {
            let l = vc.view.safeAreaInsets.left
            let r = vc.view.safeAreaInsets.right
            let w = vc.view.frame.width - (l + r)
            let c = w / 2 + l
            
            let h = 30.0
            let x = c - w / 4
            let y = vc.view.frame.height - h
            
            return CGRect(x: x, y: y, width: w/2, height: h)
        }
        return CGRect.zero
    }
    
    func roadNamePanelFrameCruise() -> CGRect {
        if let vc = carPlayManager.carPlayMapViewController {
            let l = vc.view.safeAreaInsets.left
            let r = vc.view.safeAreaInsets.right
            let w = vc.view.frame.width - (l + r)
            let c = w / 2 + l
            
            let h = 30.0
            let x = c - w / 4
            let y = vc.view.frame.height - h
            
            return CGRect(x: x, y: y, width: w/2, height: h)
        }
        return CGRect.zero
    }
    
    func showListTemplate(title: String) {
        if (title == Constants.cpNewSearch){
            
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CarplaySearchMenu.UserClick.new_search, screenName: ParameterName.CarplaySearchMenu.screen_view)
            
            let searchTemplate = CPSearchTemplate()
            searchTemplate.delegate = self.carPlaySearchController
            pushSearchTemplate(searchTemplate, animated: true)
            
            
        }else if (title == Constants.cpFavourite){
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CarplaySearchMenu.UserClick.favourite, screenName: ParameterName.CarplaySearchMenu.screen_view)
            
            ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.FN_GET_FAV_LIST, data: nil)
                .subscribe(onSuccess: { result in
                    print("onSuccess: ", result!["data"])
                    
                    if(SavedSearchManager.shared.easyBreezy.count > 0)
                    {
                        SavedSearchManager.shared.easyBreezy.removeAll()
                    }
                    if let data = result!["data"] as? [Any]{
                        
                        for rData in data as! [[String : Any]]{
                            
                            let fav = EasyBreezyAddresses.init(addressid: rData["addressid"]! as! Int, lat: rData["lat"]! as! String, long: rData["long"]! as! String, alias: rData["name"]! as! String, address1: rData["address1"]! as! String, address2: rData["address2"]! as! String)
                            
                            SavedSearchManager.shared.easyBreezy.append(fav)
                        }
                    }
                    DispatchQueue.main.async {
                        
                        let favorites = SavedSearchManager.shared.easyBreezy
                        let listTemplate = CPListTemplate.favouriteListTemplate( compatibleWith: self.carPlayManager.carPlayMapViewController!.traitCollection,
                                                                                 title: title,interfaceController: self.carPlayManager.interfaceController,
                                                                                 listDataArray: favorites)
                        listTemplate.delegate = self
                        self.carPlayManager.interfaceController?.pushTemplate(listTemplate, animated: true) //{ (_, _) in }
                    }
                    
                    
                },
                           onFailure: { error in
                    print("onFailure: ", error)
                })
            
        }
        else if(title == Constants.cpNotifyArrivalTitle){
            
            
            let etaFavorites = SavedSearchManager.shared.etaFav
            let listTemplate = CPListTemplate.etaFavouriteListTemplate( compatibleWith: carPlayManager.carPlayMapViewController!.traitCollection,
                                                                        title: Constants.cpNotifyListTitle,
                                                                        interfaceController: carPlayManager.interfaceController,
                                                                        listDataArray: etaFavorites,isNav: isCarPlayPreview)
            listTemplate.delegate = self
            carPlayManager.interfaceController?.pushTemplate(listTemplate, animated: true)
            
        }
        else{
            
            ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.FN_GET_RECENT_LIST, data: nil)
                .subscribe(onSuccess: { result in
                    print("onSuccess: ", result!["data"])
                    
                    if(SavedSearchManager.shared.places.count > 0)
                    {
                        SavedSearchManager.shared.places.removeAll()
                    }
                    if let data = result!["data"] as? [Any]{
                        
                        for rData in data as! [[String : Any]]{
                            
                            
                            let savedPlace = SavedPlaces(placeName: rData["address1"]! as! String, address: rData["address2"]! as! String , lat: rData["lat"]! as! String, long: rData["long"]! as! String, time: "")
                            SavedSearchManager.shared.places.append(savedPlace)
                        }
                    }
                    DispatchQueue.main.async {
                        
                        let recentSearches =  SavedSearchManager.shared.places
                        let listTemplate = CPListTemplate.recentSearchListTemplate( compatibleWith: self.carPlayManager.carPlayMapViewController!.traitCollection,
                                                                                    title: title,
                                                                                    interfaceController: self.carPlayManager.interfaceController,
                                                                                    listDataArray: recentSearches)
                        listTemplate.delegate = self
                        self.carPlayManager.interfaceController?.pushTemplate(listTemplate, animated: true)
                    }
                    
                    
                },
                           onFailure: { error in
                    print("onFailure: ", error)
                })
            
            
        }
    }
    
    //  MARK: - BREEZE2-1733 Revamp UI for search
    func openSearchRevampPage() {
        
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CarplayHome.UserClick.search, screenName: ParameterName.CarplayHome.screen_view)
        
        ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.FN_GET_RECENT_LIST, data: nil)
            .subscribe(onSuccess: { result in
                
                //  Clean up save places
                if !SavedSearchManager.shared.places.isEmpty {
                    SavedSearchManager.shared.places.removeAll()
                }
                
                SavedSearchManager.shared.searchModels.removeAll()
                
                //  Insert History
                SavedSearchManager.shared.searchModels.append(CarplaySearchModel(type: .history))
                
                //  Parser data from RN to be readiness for showing history screen
                if let data = result?["data"] as? [String: Any] {
                    if let workList = data["work"], let workListSearch = workList as? [String:Any] {
//                        guard let workListSearch = workList as? [String:Any] else { return }
                        let workPlace = SavedPlaces(placeName: workListSearch["name"] as? String ?? "", address: workListSearch["address1"] as? String ?? "", lat: workListSearch["lat"] as? String ?? "", long: workListSearch["long"] as? String ?? "", time: "")
                        SavedSearchManager.shared.places.insert(workPlace, at: 0)
                        
                        //  Insert Work
                        SavedSearchManager.shared.searchModels.insert(CarplaySearchModel(type: .work, name: workListSearch["name"] as? String ?? "", address: workListSearch["address1"] as? String ?? "", lat: workListSearch["lat"] as? String ?? "", long: workListSearch["long"] as? String ?? ""), at: 1)
                    }
                    
                    if let homeList = data["home"], let homeListSearch = homeList as? [String:Any] {
//                        guard let homeListSearch = homeList as? [String:Any] else { return }
                        let homePlace = SavedPlaces(placeName: homeListSearch["name"] as? String ?? "", address: homeListSearch["address1"] as? String ?? "", lat: homeListSearch["lat"] as? String ?? "", long: homeListSearch["long"] as? String ?? "", time: "")
                        SavedSearchManager.shared.places.insert(homePlace, at: 0)
                        
                        //  Insert Home
                        SavedSearchManager.shared.searchModels.insert(CarplaySearchModel(type: .home, name: homeListSearch["name"] as? String ?? "", address: homeListSearch["address1"] as? String ?? "", lat: homeListSearch["lat"] as? String ?? "", long: homeListSearch["long"] as? String ?? ""), at: 1)
                    }
                    
                    
                    if let recentList = data["recent_search"] {
                        for data in recentList as? [[String: Any]] ?? [] {
                            let recentPlace = SavedPlaces(placeName: data["address1"] as? String ?? "", address: data["address2"] as? String ?? "", lat: data["lat"] as? String ?? "", long: data["long"] as? String ?? "", time: "")
                            SavedSearchManager.shared.places.append(recentPlace)
                        }
                    }
                }
                
                //  Insert Petrol
                SavedSearchManager.shared.searchModels.append(CarplaySearchModel(type: .petrol))
                //  Insert EV
                SavedSearchManager.shared.searchModels.append(CarplaySearchModel(type: .evcharger))
                
                //  Create CPGridTemplate for search screen
                var gridButtons: [CPGridButton] = []
                for model in SavedSearchManager.shared.searchModels {
                    if let image = UIImage(named: model.getImageName()) {
                        let gridButton = CPGridButton(titleVariants: [model.getTitle()], image: image) { [weak self] button in
                            switch button.titleVariants.first {
                            case SearchType.history.getTitle():
                                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CarplaySearchPage.UserClick.recent_searches, screenName: ParameterName.CarplaySearchPage.screen_view)
                                //  Open search history
                                self?.showSearchHistory()
                            case SearchType.home.getTitle():
                                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CarplaySearchPage.UserClick.home, screenName: ParameterName.CarplaySearchPage.screen_view)
                                //  Preview route to home
                                if let model = SavedSearchManager.shared.getModelWithType(.home) {
                                    self?.navigateToHomeOrWork(model)
                                }
                                break
                            case SearchType.work.getTitle():
                                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CarplaySearchPage.UserClick.work, screenName: ParameterName.CarplaySearchPage.screen_view)
                                //  Preview route to work
                                if let model = SavedSearchManager.shared.getModelWithType(.work) {
                                    self?.navigateToHomeOrWork(model)
                                }
                                break
                            case SearchType.petrol.getTitle():
                                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CarplaySearchPage.UserClick.petrol, screenName: ParameterName.CarplaySearchPage.screen_view)
                                //  Show petrol
                                self?.showMapAmenity(Values.PETROL)
                                break
                            case SearchType.evcharger.getTitle():
                                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CarplaySearchPage.UserClick.evcharger, screenName: ParameterName.CarplaySearchPage.screen_view)
                                //  Show EV
                                self?.showMapAmenity(Values.EV_CHARGER)
                                break
                            default:
                                break
                            }
                        }
                        
                        gridButtons.append(gridButton)
                    }
                }
                
                DispatchQueue.main.async {
                    let traitCollection = self.carPlayManager.carPlayMapViewController!.traitCollection
                    let searchTemplate = CPGridTemplate(title: "Search", gridButtons: gridButtons)
                    
                    let backBtn = CPBarButton(type: .text) { [weak self] (barButton) in
                        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CarplaySearchPage.UserClick.back, screenName: ParameterName.CarplaySearchPage.screen_view)
                        self?.carPlayManager.interfaceController?.popTemplate(animated: true)
                    }
                    backBtn.title = "Back"
                    
                    searchTemplate.backButton = backBtn
                    searchTemplate.trailingNavigationBarButtons = self.createKeyboardNavigationButtons(interfaceController: self.carPlayManager.interfaceController!, traitCollection: traitCollection)
                    self.carPlayManager.interfaceController?.pushTemplate(searchTemplate, animated: true)
                }
            }, onFailure: { error in
                print("onFailure: ", error)
            })
    }
    
    func showSearchHistory() {
        DispatchQueue.main.async {
            let recentSearches =  SavedSearchManager.shared.places
            let traitCollection = self.carPlayManager.carPlayMapViewController!.traitCollection
            let listTemplate = CPListTemplate.recentSearchListTemplate( compatibleWith: self.carPlayManager.carPlayMapViewController!.traitCollection,
                                                                        title: Constants.searchTitle,
                                                                        interfaceController: self.carPlayManager.interfaceController,
                                                                        listDataArray: recentSearches)
            listTemplate.trailingNavigationBarButtons = self.createShortcutsNavigationButtons(interfaceController: self.carPlayManager.interfaceController!, traitCollection: traitCollection)
            if SavedSearchManager.shared.places.isEmpty {
                if #available(iOS 14.0, *) {
                    listTemplate.emptyViewSubtitleVariants = [Constants.cpShortcutRecentSreachMsg]
                } else {
                    self.showNoListSreach(title: Constants.cpShortcutRecentSreachMsg)
                }
            }
            listTemplate.delegate = self
            self.carPlayManager.interfaceController?.pushTemplate(listTemplate, animated: true)
        }
    }
    
    // TODO: BREEZES-6190: New Search For CarPlay
    func searchCarPlay() {
        ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.FN_GET_RECENT_LIST, data: nil)
            .subscribe(onSuccess: { result in
                
                if(SavedSearchManager.shared.places.count > 0)
                {
                    SavedSearchManager.shared.places.removeAll()
                }
                if let data = result {
                    
                    let listData = data["data"] as? [String: Any]
                    
//                    if let workList = listData?["work"] {
//                        guard let workListSearch = workList as? [String:Any] else { return }
//                        let workPlace = SavedPlaces(placeName: workListSearch["name"] as? String ?? "", address: workListSearch["address1"] as? String ?? "", lat: workListSearch["lat"] as? String ?? "", long: workListSearch["long"] as? String ?? "", time: "")
//                        SavedSearchManager.shared.places.insert(workPlace, at: 0)
//                    }
                    
//                    if let homeList = listData?["home"] {
//                        guard let homeListSearch = homeList as? [String:Any] else { return }
//                        let homePlace = SavedPlaces(placeName: homeListSearch["name"] as? String ?? "", address: homeListSearch["address1"] as? String ?? "", lat: homeListSearch["lat"] as? String ?? "", long: homeListSearch["long"] as? String ?? "", time: "")
//                        SavedSearchManager.shared.places.insert(homePlace, at: 0)
//                    }
                    
                    
                    if let recentList = listData?["recent_search"] as? Any{
                        for data in recentList as? [[String: Any]] ?? [] {
                            let recentPlace = SavedPlaces(placeName: data["address1"] as? String ?? "", address: data["address2"] as? String ?? "", lat: data["lat"] as? String ?? "", long: data["long"] as? String ?? "", time: "")
                            SavedSearchManager.shared.places.append(recentPlace)
                        }
                    }
                    
                }
                
                DispatchQueue.main.async {
                    
                    let recentSearches =  SavedSearchManager.shared.places
                    let traitCollection = self.carPlayManager.carPlayMapViewController!.traitCollection
                    let listTemplate = CPListTemplate.recentSearchListTemplate( compatibleWith: self.carPlayManager.carPlayMapViewController!.traitCollection,
                                                                                title: Constants.searchTitle,
                                                                                interfaceController: self.carPlayManager.interfaceController,
                                                                                listDataArray: recentSearches)
                    listTemplate.trailingNavigationBarButtons = self.createShortcutsNavigationButtons(interfaceController: self.carPlayManager.interfaceController!, traitCollection: traitCollection)
                    if SavedSearchManager.shared.places.isEmpty {
                        if #available(iOS 14.0, *) {
                            listTemplate.emptyViewSubtitleVariants = [Constants.cpShortcutRecentSreachMsg]
                        } else {
                            self.showNoListSreach(title: Constants.cpShortcutRecentSreachMsg)
                        }
                    }
                    listTemplate.delegate = self
                    self.carPlayManager.interfaceController?.pushTemplate(listTemplate, animated: true)
                }
                
                
            },
                       onFailure: { error in
                print("onFailure: ", error)
            })
    }
    
    // Open ShortsCut Screen
    func pushToShortsCutScreen() {
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CarplaySearchMenu.UserClick.favourite, screenName: ParameterName.CarplaySearchMenu.screen_view)
        
        ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.FN_GET_SHORTCUT_LIST, data: nil)
            .subscribe(onSuccess: { result in
                
                if(SavedSearchManager.shared.easyBreezy.count > 0)
                {
                    SavedSearchManager.shared.easyBreezy.removeAll()
                }
                
                if let data = result {
                    
                    let listData = data["data"] as? [String: Any]
                    
                    let workList = listData?["work"]
                    for data in workList as? [[String: Any]] ?? [] {
                        let workPlace = EasyBreezyAddresses.init(addressid: data["addressid"] as? Int ?? 0, lat: data["lat"] as? String ?? "", long: data["long"] as? String ?? "", alias: data["name"]as? String ?? "", address1: data["address1"] as? String ?? "", address2: data["address2"] as? String ?? "")
                        SavedSearchManager.shared.easyBreezy.insert(workPlace, at: 0)
                    }
                    
                    
                    
                    let homeList = listData?["home"]
                    for data in homeList as? [[String: Any]] ?? [] {
                        let homePlace = EasyBreezyAddresses.init(addressid: data["addressid"] as? Int ?? 0, lat: data["lat"] as? String ?? "", long: data["long"] as? String ?? "", alias: data["name"]as? String ?? "", address1: data["address1"] as? String ?? "", address2: data["address2"] as? String ?? "")
                        SavedSearchManager.shared.easyBreezy.insert(homePlace, at: 0)
                    }
                    
                    
                    let shortcutList = listData?["shortcutList"]
                    for data in shortcutList as? [[String: Any]] ?? []{
                        let shortcutPlace = EasyBreezyAddresses.init(addressid: data["addressid"] as? Int ?? 0, lat: data["lat"] as? String ?? "", long: data["long"] as? String ?? "", alias: data["address1"] as? String ?? "", address1: data["address2"] as? String ?? "", address2: data["address2"] as? String ?? "")
                        SavedSearchManager.shared.easyBreezy.append(shortcutPlace)
                    }
                    
                    
                    
                }
                DispatchQueue.main.async {
                    
                    let search = SavedSearchManager.shared.easyBreezy
                    let listTemplate = CPListTemplate.shortcutListTemplate( compatibleWith: self.carPlayManager.carPlayMapViewController!.traitCollection,
                                                                            title: Constants.shortcutsTitle,interfaceController: self.carPlayManager.interfaceController,
                                                                            listDataArray: search)
                    listTemplate.delegate = self
                    
                    if SavedSearchManager.shared.easyBreezy.isEmpty {
                        if #available(iOS 14.0, *) {
                            listTemplate.emptyViewSubtitleVariants = [Constants.cpShortcutMsg]
                        } else {
                            self.showNoListSreach(title: Constants.cpShortcutMsg)
                        }
                    }
                    self.carPlayManager.interfaceController?.pushTemplate(listTemplate, animated: true) //{ (_, _) in }
                }
                
                
            },
                       onFailure: { error in
                print("onFailure: ", error)
            })
    }
    
    func showNoListSreach(title:String) {
        let alert = InformationAlertTemplate().showSearchEmptyView(carPlayManager: carPlayManager, title: title, buttonText: "Got it")
        self.tryToDismissTemplateIfNeed()
        self.safePresentTemplate(alert, animated: true)
    }
    
    
    func showNotFoundRouteLocation(title:String) {
        let alert = InformationAlertTemplate().showNotFoundRouteLocationView(carPlayManager: carPlayManager, title: title, buttonText: "Got it")
        self.popTemplate(animated: true)
        self.safePresentTemplate(alert, animated: true)
    }
    
    func showOUBTemplate(title:String,isReportIssue:Bool = false) {
        let alert = InformationAlertTemplate().showOBUTemplate(carPlayManager: carPlayManager, title: title, buttonText: "Got It", isReportIssue: isReportIssue)
        self.popTemplate(animated: true)
        self.safePresentTemplate(alert, animated: true)
    }
    
    // Create keyboard bar button
    private func createKeyboardNavigationButtons(interfaceController: CPInterfaceController, traitCollection: UITraitCollection) -> [CPBarButton] {
        var cpBarButton = [CPBarButton]()
        
        let keyboardBtn = CPBarButton(type: .image) { (barButton) in
            let searchTemplate = CPSearchTemplate()
            searchTemplate.delegate = self.carPlaySearchController
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CarplaySearchPage.UserClick.keyboard, screenName: ParameterName.CarplaySearchPage.screen_view)
            self.pushSearchTemplate(searchTemplate, animated: true)
        }
        
        keyboardBtn.image = UIImage(named: "keyboradCP", in: nil, compatibleWith: traitCollection)
        cpBarButton.append(keyboardBtn)
        return cpBarButton
    }
    
    func createSearchBackButton(interfaceController: CPInterfaceController, traitCollection: UITraitCollection) -> CPBarButton {
        let backBtn = CPBarButton(type: .text) { [weak self] (barButton) in
            self?.carPlayManager.interfaceController?.popTemplate(animated: true)
        }
        backBtn.title = "Back"
        return backBtn
    }
    
    // Create Shortcuts Bar Button Carplay
    private func createShortcutsNavigationButtons(interfaceController: CPInterfaceController, traitCollection: UITraitCollection) -> [CPBarButton] {
        
        var cpBarButton = [CPBarButton]()
        
        let keyboardBtn = CPBarButton(type: .image) { (barButton) in
            let searchTemplate = CPSearchTemplate()
            searchTemplate.delegate = self.carPlaySearchController
            self.pushSearchTemplate(searchTemplate, animated: true)
        }
        keyboardBtn.image = UIImage(named: "keyboradCP", in: nil, compatibleWith: traitCollection)
        cpBarButton.append(keyboardBtn)
        
        let shortcurtBtn = CPBarButton(type: .text) { (barButton) in
            self.pushToShortsCutScreen()
        }
        shortcurtBtn.title = Constants.shortcutsTitle
        cpBarButton.append(shortcurtBtn)
        
        return cpBarButton
    }
    
    
    func hideCarParks(){
        
        if let cpCarPark = self.cpCarparkVC{
            cpCarPark.hideCarparks()
        }
    }
    
    // show recent contact list
    // TODO: This will be deprecated (ListTemplate delegate). May use item.handler to handle this after iOS 14
    func showRecentContactList(contacts: [RecentContact], isCruise: Bool) {
        DispatchQueue.main.async {
            let listTemplate = CPListTemplate.recentContactsListTemplate( compatibleWith: self.carPlayManager.carPlayMapViewController!.traitCollection, title: Constants.cpRecentContactTitle,interfaceController: self.carPlayManager.interfaceController,
                                                                          listDataArray: contacts)
            listTemplate.delegate = self
            self.carPlayManager.interfaceController?.pushTemplate(listTemplate, animated: true) //{ (_, _) in }
        }
    }
    
    func showNoContacts() {
        let alert = InformationAlertTemplate().showGenericAlertTemplate(carPlayManager: carPlayManager, title: Constants.cpNoContactAlert, buttonText: "OK")
        self.tryToDismissTemplateIfNeed()
        self.safePresentTemplate(alert, animated: true)
    }
    
    func showObuLite() {
        let alert = InformationAlertTemplate().showGenericAlertTemplate(carPlayManager: carPlayManager, title: Constants.cpObuDisplayMessage, buttonText: "")
        self.tryToDismissTemplateIfNeed()
        self.safePresentTemplate(alert, animated: true)
    }
    
    // MARK: OBU FEATURE
    func showCarparkList(_ model: [Parking]) {
        
        DispatchQueue.main.async {
            let traitCollection = self.carPlayManager.carPlayMapViewController!.traitCollection
            let listTemplate = CPListTemplate.parkingListTemplate( compatibleWith: self.carPlayManager.carPlayMapViewController!.traitCollection,
                                                                   title: "Parking Updates",
                                                                   interfaceController: self.carPlayManager.interfaceController,
                                                                   listDataArray: model)
            if model.isEmpty {
                if #available(iOS 14.0, *) {
                    listTemplate.emptyViewSubtitleVariants = []
                } else {
                    self.showNoListSreach(title: "")
                }
            }
            listTemplate.delegate = self
            self.carPlayManager.interfaceController?.pushTemplate(listTemplate, animated: true)
        }
        
    }
    
    func showTravelTimeList(_ model: [TravelTime]) {
        DispatchQueue.main.async {
            let traitCollection = self.carPlayManager.carPlayMapViewController!.traitCollection
            let listTemplate = CPListTemplate.travelTimeListTemplate( compatibleWith: self.carPlayManager.carPlayMapViewController!.traitCollection,
                                                                      title: "Travel Time Alert - Est Time to",
                                                                      interfaceController: self.carPlayManager.interfaceController,
                                                                      listDataArray: model)
            if model.isEmpty {
                if #available(iOS 14.0, *) {
                    listTemplate.emptyViewSubtitleVariants = []
                } else {
                    self.showNoListSreach(title: "")
                }
            }
            listTemplate.delegate = self
            self.carPlayManager.interfaceController?.pushTemplate(listTemplate, animated: true)
        }
    }
}

@available(iOS 13.0, *)
class CarPlaySceneDelegate: NSObject, CPTemplateApplicationSceneDelegate {
    
    
    func templateApplicationScene(_ templateApplicationScene: CPTemplateApplicationScene,
                                  didConnect interfaceController: CPInterfaceController, to window: CPWindow) {
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        
        SwiftyBeaver.info("CarPlay didConnect")
        //        if(AWSAuth.sharedInstance.idToken != "")
        //        {
        appDelegate.carPlayManager.delegate = appDelegate
        appDelegate.carPlaySearchController.delegate = appDelegate
        appDelegate.carPlayManager.templateApplicationScene(templateApplicationScene, didConnectCarInterfaceController: interfaceController, to: window)
        appDelegate.carPlayManager.application(UIApplication.shared,
                                               didConnectCarInterfaceController: interfaceController,
                                               to: window)
        appDelegate.carPlayMapStyles()
        appDelegate.carPlayConnected = CarPlayManager.isConnected
        
        AnalyticsManager.shared.logScreenView(screenName: ParameterName.CarplaySignIn.screen_view)
        AnalyticsManager.shared.logScreenView(screenName: ParameterName.CarplaySignIn.CarDisplayConnect.carconnect)
        
        if let naviMapView = appDelegate.carPlayManager.navigationMapView {
            // Need to wait for the mapLoaded otherwise the symbol layer will be failed to add
            naviMapView.mapView.mapboxMap.onNext(event: .mapLoaded) { _ in
                
                SwiftyBeaver.info("CarPlay did map loaded")
                
                AnalyticsManager.shared.logSystemEvent(eventValue: ParameterName.SystemAnalytics.SystemEvent.carconnect, screenName: ParameterName.SystemAnalytics.screen_view)
                
                appDelegate.carPlayMapController = CarPlayMapController(navigationMapView: naviMapView, carPlayManager: appDelegate.carPlayManager)
                appDelegate.updateCarPlayLoggedInStatus(loggedIn: AWSAuth.sharedInstance.isSignedIn)
                appDelegate.getETAFav()
                
                if (appDelegate.isObuLiteDisplay ?? false) {
                    NotificationCenter.default.post(name: .carplayConnectionDidChangeStatus, object: appDelegate.carPlayConnected)
                }
                
                NotificationCenter.default.post(name: .carplayMapLoaded, object: appDelegate.carPlayConnected)
            }
            if !(appDelegate.isObuLiteDisplay ?? false) {
                NotificationCenter.default.post(name: .carplayConnectionDidChangeStatus, object: appDelegate.carPlayConnected)
            }
            
        } else {
            NotificationCenter.default.post(name: .carplayConnectionDidChangeStatus, object: appDelegate.carPlayConnected)
        }
        
        //        if appDelegate.carPlayConnected && appDelegate.isObuLiteDisplay == true {
        //            appDelegate.isObuLiteDisplay = true
        //        }
        
        if OBUHelper.shared.stateOBU == .CONNECTED {
            // Handler template Connected OBU on CarPlay
            NotificationCenter.default.post(name: NSNotification.Name("OBUConnectedNotification"), object: nil)
        }
        
        //        appDelegate.carPlayManager.carPlayMapViewController?.start()
        
        //        }
        //        else
        //        {
        //            
        //            
        //            if #available(iOS 14.0, *) {
        //                let info = CPInformationItem(title: "Please take a note", detail: "Please create account/login first on mobile app")
        //                let infoTemplate = CPInformationTemplate(title: "Sorry please check the information before proceeding", layout: .twoColumn, items: [info], actions: [])
        //                interfaceController.setRootTemplate(infoTemplate, animated: true)
        //            } else {
        ////                // Fallback on earlier versions
        ////                let alertTermPlate = CP(titleVariants: ["Please create account/login first on mobile app"],actions:[])
        ////                interfaceController.setRootTemplate(infoTemplate, animated: true)
        //            }
        //            
        //            
        //        }
        
        //appDelegate.carPlayManager.carPlayMapViewController?.navigationController
    }
    
    
    func templateApplicationScene(_ templateApplicationScene: CPTemplateApplicationScene,
                                  didDisconnect interfaceController: CPInterfaceController, from window: CPWindow) {
        
        SwiftyBeaver.info("CarPlay didDisconnect")
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        appDelegate.carPlayManager.templateApplicationScene(templateApplicationScene, didDisconnectCarInterfaceController: interfaceController, from: window)
        appDelegate.carPlayConnected = CarPlayManager.isConnected
        appDelegate.carPlayMapController = nil
        AnalyticsManager.shared.logSystemEvent(eventValue: ParameterName.SystemAnalytics.SystemEvent.cardisconnect, screenName: ParameterName.SystemAnalytics.screen_view)
        
        
        // Naviagtion Mode
        DispatchQueue.main.asyncAfter(deadline: .now() + 1)  {
//            NotificationCenter.default.post(name: .carplayConnectionShowFullListStepTable, object: true)
            NotificationCenter.default.post(name: .carplayConnectionDidChangeStatus, object: false)
        }
        
        
        
    }
    
    public func sceneWillEnterForeground(_ scene: UIScene) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        
        appDelegate.carplayInBackground = false
        SwiftyBeaver.debug("CarPlay sceneWillEnterForeground \(scene)")
        
        //  TODO need to handle auto connect OBU here
        if !OBUHelper.shared.isObuConnected() && OBUHelper.shared.hasLastObuData() && OBUHelper.shared.willTriggerAutoConnect {
            
        }
    }
    
    public func sceneDidEnterBackground(_ scene: UIScene) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        
        SwiftyBeaver.debug("CarPlay sceneDidEnterBackground \(scene)")
        appDelegate.carplayInBackground = true
    }
    
}

extension AppDelegate:CPPreviewBarButtonActionDelegate{
    func CPPreviewNavigationButtonSelected(type: String) {
        
        if type == Constants.cpShareDrive {
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CarplayRoutePreview.UserClick.notify, screenName: ParameterName.CarplayRoutePreview.screen_view)
            self.carPlayMapController?.carplayShareDrive(isCruise: false)
            if let cpRPCarPark = self.cpRPCarparkVC{
                cpRPCarPark.hideCarparks()
            }
            self.notifyPreview = true
            
        } else if type == Constants.cpCancelShareDrive {
            // show confirmation
            showCancelSharingConfirmation()
        }
        /*
         if(type == Constants.cpNotifyArrivalTitle){
         
         AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CarplayRoutePreview.UserClick.notify, screenName: ParameterName.CarplayRoutePreview.screen_view)
         if(SavedSearchManager.shared.etaFav.count == 0){
         self.mapLandingViewModel!.carPlayMapViewUpdatable?.showNotifyToastMessage(message: Constants.cpNotifyToast)
         }
         else
         {
         //                 if let cpRPCarPark = self.cpRPCarparkVC{
         //                     cpRPCarPark.hideCarparks()
         //                 }
         self.notifyPreview = true
         //                 self.showListTemplate(title: type)
         
         }
         
         
         }
         */
        /*
        else if(type == Constants.cpCarParkHideTitle){
            if let trip = self.trip {
                
                AnalyticsManager.shared.logToggleEvent(eventValue: ParameterName.CarplayRoutePreview.UserToggle.display_carpark, screenName: ParameterName.CarplayRoutePreview.screen_view)
                cpRPCarparkVC  =  CPRoutePlanningCarParkVC(navigationMapView: carPlayManager.navigationMapView!, carPlayManager: carPlayManager, trip: trip)
                cpRPCarparkVC?.showCarparks()
            }
        }
         */
        else if(type == Constants.cpCarParkTitle){
            
            if let trip = self.trip {
                
                AnalyticsManager.shared.logToggleEvent(eventValue: ParameterName.CarplayRoutePreview.UserToggle.display_carpark, screenName: ParameterName.CarplayRoutePreview.screen_view)
                cpRPCarparkVC  =  CPRoutePlanningCarParkVC(navigationMapView: carPlayManager.navigationMapView!, carPlayManager: carPlayManager, trip: trip)
                cpRPCarparkVC?.showCarparks()
                
            } else {
                if let cpRPCarPark = self.cpRPCarparkVC{
                    cpRPCarPark.hideCarparks(toPop:true)
                }
                if let traitCollection = (self.carPlayManager.carWindow?.rootViewController as? CarPlayMapViewController)?.traitCollection,
                   let interfaceController = carPlayManager.interfaceController,
                   let carRPController = self.carRPController{
                    carRPController.calculateTrip(traitCollection: traitCollection, interfaceController: interfaceController)
                    //self.fitCamera()
                }
            }
            
//            AnalyticsManager.shared.logToggleEvent(eventValue: ParameterName.CarplayRoutePreview.UserToggle.hide_carpark, screenName: ParameterName.CarplayRoutePreview.screen_view)
        }
        
        //            cpCarparkVC  =  CPCarParkVC(navigationMapView: carPlayManager.navigationMapView!, carPlayManager: carPlayManager,trip:self.trip!)
        //            cpCarparkVC?.showCarparks()
        
        
        
    }
    
}

extension AppDelegate:CPNavigationBarButtonActionDelegate{
    func CPNavigationNavigationButtonSelected(type: String) {
        
        if (type == "$ \(OBUHelper.shared.getCardBalance())") || (type == "OBU") {
//            self.carPlayMapController?.carplayShareDrive(isCruise: false)
            self.checkStateOBUConnect()
        } else if(type == Constants.cpEndNav){
            
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CarplayNavigation.UserClick.end, screenName: ParameterName.CarplayNavigation.screen_view)
            self.tripETA = nil
            // Move this to TurnByTurnNavigationViewModel
            //            NotificationCenter.default.post(name: Notification.Name(Values.notificationNavigationStop), object: nil)
            if let userLocation = carPlayManager.navigationMapView?.mapView.location.latestLocation {
                self.navigationViewModel?.saveTripLog(location: CLLocation(latitude: (userLocation.coordinate.latitude), longitude: (userLocation.coordinate.longitude)), fromUser: true,completion: { _ in
                    
                    self.navigationViewModel?.endTrip(location:CLLocation(latitude: (userLocation.coordinate.latitude), longitude: (userLocation.coordinate.longitude)), manually: true)
                    self.carPlayManagerDidEndNavigation(self.carPlayManager)
                })
            }
            
            
#if DEMO
            if(DemoSession.shared.isDemoRoute){
                let manager = LocalNotificationManager()
                manager.requestPermission()
                manager.addNotification(title: "Let's make parking a Breeze!",subTitle: "Start your parking!")
            }
#endif
            
        }
        else if(type == Constants.cpPbMessage){
            self.navigationViewModel?.voicInstructionOtherText(text: Constants.pbVoiceMessage)
        }
        else if(type == Constants.cpMute){
            
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CarplayNavigation.UserClick.mute, screenName: ParameterName.CarplayNavigation.screen_view)
            self.navigationViewModel?.carPlayMapViewUpdatable?.delegate?.onMuteEnabled(true)
        }
        
        else if(type == Constants.cpUnmute){
            
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CarplayNavigation.UserClick.unmute, screenName: ParameterName.CarplayNavigation.screen_view)
            self.navigationViewModel?.carPlayMapViewUpdatable?.delegate?.onMuteEnabled(false)
        }
        
        else if(type == Constants.cpETAPause){
            
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CarplayNavigation.UserClick.pause_sharing, screenName: ParameterName.CarplayNavigation.screen_view)
            self.navigationViewModel?.carPlayMapViewUpdatable?.delegate?.onLiveLocationEnabled(false)
            
        }
        else if(type == Constants.cpETAResume){
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CarplayNavigation.UserClick.resume_sharing, screenName: ParameterName.CarplayNavigation.screen_view)
            self.navigationViewModel?.carPlayMapViewUpdatable?.delegate?.onLiveLocationEnabled(true)
        }
        else if(type == "Carparks") {
            if let navigationMapView = carPlayManager.navigationMapView {
                carPlayCarParkNavigation = CPNavigationModeCarpark(navigationMapView: navigationMapView, carPlayManager: carPlayManager, destination: self.trip)
                carPlayCarParkNavigation?.showCarparks()
            }
        }
//        else  if(type == Constants.cpCarParkHideTitle)
//        {
//            if let navigationMapView = carPlayManager.navigationMapView {
//                if let trip = self.trip {
//                    carPlayCarParkNavigation = CPNavigationModeCarpark(navigationMapView: navigationMapView, carPlayManager: carPlayManager, trip: trip)
//                    carPlayCarParkNavigation?.showCarparks()
//                }
//            }
//
//
//        }
//        else  if(type == Constants.cpCarParkTitle)
//        {
//            if let navigationMapView = carPlayManager.navigationMapView {
//                if let trip = self.trip {
//                    carPlayCarParkNavigation = CPNavigationModeCarpark(navigationMapView: navigationMapView, carPlayManager: carPlayManager, trip: trip)
//                    carPlayCarParkNavigation?.hideCarparks()
//                }
//            }
//        }
        
    }
    
    func CPCruiseNavigationButtonSelected(type: String) {
        
        if(type == Constants.cpEndCruise){
            
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CarplayCruise.UserClick.end_cruise, screenName: ParameterName.CarplayCruise.screen_view)
            self.carPlayMapController?.stopCruise()
//            self.hideCarParks()
            self.carPlayManager.carPlayNavigationViewController?.dismissAnyAlertControllerIfPresent()
            
        }
        else if(type == Constants.cpMute){
            
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CarplayCruise.UserClick.mute, screenName: ParameterName.CarplayCruise.screen_view)
            self.cruiseViewModel?.carPlayMapViewUpdatable?.delegate?.onMuteEnabled(true)
        }
        
        else if(type == Constants.cpUnmute){
            
            self.cruiseViewModel?.carPlayMapViewUpdatable?.delegate?.onMuteEnabled(false)
        }
        else if(type == Constants.cpETAPause){
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CarplayCruise.UserClick.pause_sharing, screenName: ParameterName.CarplayCruise.screen_view)
            self.cruiseViewModel?.carPlayMapViewUpdatable?.delegate?.onLiveLocationEnabled(false)
        }
        else if(type == Constants.cpETAResume){
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CarplayCruise.UserClick.resume_sharing, screenName: ParameterName.CarplayCruise.screen_view)
            self.cruiseViewModel?.carPlayMapViewUpdatable?.delegate?.onLiveLocationEnabled(true)
        } else if (type == "$ \(OBUHelper.shared.getCardBalance())") || (type == "OBU")  {
//            self.carPlayMapController?.carplayShareDrive(isCruise: true)
            self.checkStateOBUConnect()
        } else  if(type == "Carparks")
        {
            if let navigationMapView = carPlayManager.navigationMapView {
                carPlayCarParkCruise =  CPCruiseModeCarPark(navigationMapView: navigationMapView, carPlayManager: carPlayManager)
                carPlayManager.navigationMapView?.mapView.removeCarparkLayer()
                carPlayCarParkCruise?.showCarparks()
                appDelegate().cruiseViewModel?.carPlayMapViewUpdatable?.removeRoadNamePanel()
                appDelegate().cruiseViewModel?.noTracking()
            }
           
            
        }
//        else  if(type == Constants.cpCarParkTitle)
//        {
//
//            AnalyticsManager.shared.logToggleEvent(eventValue: ParameterName.CarplayHomepage.UserToggle.hide_carpark, screenName: ParameterName.CarplayHomepage.screen_view)
//            self.hideCarParks()
//        }
        
    }
  
    
}

extension AppDelegate:CPBrowseBarButtonActionDelegate{
    
    func CPBrowsingNavigationButtonSelected(type: String) {
        print("Button clicked", type)
        
        if(type == Constants.cpAddDestination)
        {
            
            
            self.hideCarParks()
            
            let gridTemplate = CPGridTemplate.showGridTemplate(compatibleWith: carPlayManager.carPlayMapViewController!.traitCollection) {
                // Set title if it exists, otherwise name it "Favorites".
                button in
                self.showListTemplate(title: button.titleVariants.first ?? Constants.cpAddDestination)
            }
            
            carPlayManager.interfaceController?.pushTemplate(gridTemplate, animated: true)// { (_, _) in }
        }
        
        // MARK: Remove cruise mode on CarPlay BREEZE2-773
//        else if(type == Constants.cpStartCruise)
//        {
//            self.hideCarParks()
//            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CarplayHomepage.UserClick.cruise, screenName: ParameterName.CarplayHomepage.screen_view)
//            self.carPlayMapController?.startCruise()
//        }
        else if(type == Constants.cpNotifyArrivalTitle){
            
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CarplayHomepage.UserClick.notify, screenName: ParameterName.CarplayHomepage.screen_view)
            if(SavedSearchManager.shared.etaFav.count == 0){
                self.mapLandingViewModel?.carPlayMapViewUpdatable?.showNotifyToastMessage(message: Constants.cpNotifyToast)
            }
            else
            {
                self.hideCarParks()
                self.showListTemplate(title: type)
            }
            
        }
        /*
        else  if(type == Constants.cpCarParkHideTitle)
        {
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CarplayHome.UserClick.carparks, screenName: ParameterName.CarplayHome.screen_view)
//            AnalyticsManager.shared.logToggleEvent(eventValue: ParameterName.CarplayHomepage.UserToggle.display_carpark, screenName: ParameterName.CarplayHomepage.screen_view)
            cpCarparkVC  =  CPCarParkVC(navigationMapView: carPlayManager.navigationMapView!, carPlayManager: carPlayManager)
            cpCarparkVC?.showCarparks()
            
        }
        */
        else  if(type == Constants.cpCarParkTitle)
        {
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CarplayHome.UserClick.carparks, screenName: ParameterName.CarplayHome.screen_view)
            
            cpCarparkVC  =  CPCarParkVC(navigationMapView: carPlayManager.navigationMapView!, carPlayManager: carPlayManager)
            cpCarparkVC?.showCarparks()
            
//            AnalyticsManager.shared.logToggleEvent(eventValue: ParameterName.CarplayHomepage.UserToggle.hide_carpark, screenName: ParameterName.CarplayHomepage.screen_view)
//            self.hideCarParks()
        }
    }

    func navigateToHomeOrWork(_ place: CarplaySearchModel) {
                
        let searchAddress = SearchAddresses.init(alias: "", address1: place.name, lat: place.lat, long: place.long, address2: place.address, distance: "")
        self.insideCP = false
        let destLocation = CLLocation(latitude: (searchAddress.lat?.toDouble())!, longitude: (searchAddress.long?.toDouble())!)
        let destinationWaypoint = Waypoint(location: destLocation)
        self.popFromSearchTemplate(animated: false)
        self.searchPreviewRoutes(to: destinationWaypoint, address: searchAddress)
    }
    
    func showMapAmenity(_ amenityID: String) {
//        AnalyticsManager.shared.logToggleEvent(eventValue: ParameterName.CarplayHomepage.UserToggle.display_carpark, screenName: ParameterName.CarplayHomepage.screen_view)
        
        carPlayManager.interfaceController?.popTemplate(animated: true)
        cpAmenityVC = CPAmenityVC(navigationMapView: carPlayManager.navigationMapView!, carPlayManager: carPlayManager)
        cpAmenityVC?.loadAmenityNearby(amenityID)
        
    }
    
    func openSearchScreenCarPlay() {
//        self.searchCarPlay()
        if let amenityVC = cpAmenityVC {
            amenityVC.hideAmenities()
        }
        self.openSearchRevampPage()
    }
    
    func obuNotConnected() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [weak self] in
            if let mapView = self?.carPlayManager.navigationMapView?.mapView {
                showToast(from: mapView, message: "OBU not connected", topOffset: 5, fromCarPlay: false)
            }
        }
    }
    
    func obuConnected() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [weak self] in
            if let mapView = self?.carPlayManager.navigationMapView?.mapView {
                showToast(from: mapView, message: "OBU connected", topOffset: 5, fromCarPlay: false)
            }
        }
    }
    
    func showCommonToastWithMessage(_ message: String) {
        if !message.isEmpty {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [weak self] in
                if let mapView = self?.carPlayManager.navigationMapView?.mapView {
                    showToast(from: mapView, message: message, topOffset: 5, fromCarPlay: false)
                }
            }
        }
    }
    
    func checkStateOBUConnect() {
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CarPlayObuState.UserClick.dhu_obu_button, screenName: ParameterName.CarPlayObuState.dhu_homepage)
        if OBUHelper.shared.stateOBU == .CONNECTED {
            self.showOBUConnectedTemplate()
            AnalyticsManager.shared.logOpenPopupEventV2CarPlay(popupName: ParameterName.ObuPairing.UserPopup.obu_connected, screenName: ParameterName.CarPlayObuState.dhu_homepage)
        } else if !OBUHelper.shared.isObuConnected() && !OBUHelper.shared.obuName.isEmpty {
            self.showPremissonBluetoothOBU()
            AnalyticsManager.shared.logOpenPopupEventV2CarPlay(popupName: ParameterName.ObuPairing.UserPopup.obu_not_paired, screenName: ParameterName.CarPlayObuState.dhu_homepage)
        } else if OBUHelper.shared.stateOBU == .IDLE {
            self.showOBUNotconnected()
            AnalyticsManager.shared.logOpenPopupEventV2CarPlay(popupName: ParameterName.ObuPairing.UserPopup.obu_not_connected, screenName: ParameterName.CarPlayObuState.dhu_homepage)
        } else if OBUHelper.shared.stateOBU == .PAIRING {
            self.showOBUConnecting()
        } else {
            self.showOBUReportIssue()
            AnalyticsManager.shared.logOpenPopupEventV2CarPlay(popupName: ParameterName.ObuPairing.UserPopup.obu_connection_error, screenName: ParameterName.CarPlayObuState.dhu_homepage)
        }
    }
}

extension AppDelegate {
    
    func tryToDismissTemplateIfNeed() {
        if self.carPlayManager.interfaceController?.presentedTemplate != nil {
            if #available(iOS 14.0, *) {
                self.carPlayManager.interfaceController?.dismissTemplate(animated: false, completion: { _, _ in
                    //  No-op
                })
            }else {
                self.carPlayManager.interfaceController?.dismissTemplate(animated: false)
            }
        }
    }
    
    func safePresentTemplate(_ template: CPTemplate?, animated: Bool) {
        
        if let cpTemplate = template {
            if #available(iOS 14.0, *) {
                self.carPlayManager.interfaceController?.presentTemplate(cpTemplate, animated: animated, completion: { _, _ in
                    //  No-op
                })
            } else {
                // Fallback on earlier versions
                self.carPlayManager.interfaceController?.presentTemplate(cpTemplate, animated: animated)
            }
        }
    }
    
    
    
    func updateCarPlayLoggedInStatus(loggedIn: Bool) {
        
        if loggedIn {
            if signInAlertTemplate != nil {
                if #available(iOS 14, *) {
                    self.carPlayManager.interfaceController?.dismissTemplate(animated: true, completion: { _, _ in
                        //  No-op
                    })
                }else {
                    carPlayManager.interfaceController?.dismissTemplate(animated: true)
                }
                
                signInAlertTemplate = nil
            }
        } else {
            if signInAlertTemplate == nil && carPlayConnected {
                signInAlertTemplate = InformationAlertTemplate().showGenericAlertTemplate(carPlayManager: carPlayManager, title: Constants.cpLoginMsg, buttonText: "")
                
                DispatchQueue.main.async {
                    if let carRPController = self.carRPController{
                        
                        carRPController.clearPreviousRouteResponse()
                        self.carRPController = nil
                        self.routePlanningViewModel = nil
                        self.updateMapCancellable?.cancel()
                        self.carPlayManager.navigationMapView?.removeRoutes()
                        self.carPlayManager.navigationMapView?.removeWaypoints()
                        
                    }
                    
                    if let cpCarPark = self.cpCarparkVC{
                        cpCarPark.hideCarparks()
                    }
                    
                    if let cpRPCarparkVC = self.cpRPCarparkVC{
                        let topTemplate = self.carPlayManager.interfaceController?.topTemplate as? CPMapTemplate
                        cpRPCarparkVC.hideCarparks(mapTopTemplate: topTemplate)
                        self.cpRPCarparkVC = nil
                        self.insideCP = false
                    }
                    
                }
                
                if let templates = self.carPlayManager.interfaceController?.templates{
                    
                    if(templates.count > 1){
                        
                        if #available(iOS 14, *) {
                            
                            self.carPlayManager.interfaceController?.popToRootTemplate(animated: true,completion: { value, error in
                                
                                self.tryToDismissTemplateIfNeed()
                                self.safePresentTemplate(self.signInAlertTemplate, animated: false)
                                if(!value){
                                    SwiftyBeaver.debug("Pop to root template failed on logout", "\(value)","\(String(describing: error))")
                                }
                            })
                        } else {
                            
                            self.tryToDismissTemplateIfNeed()
                            self.safePresentTemplate(self.signInAlertTemplate, animated: false)
                            self.carPlayManager.interfaceController?.popToRootTemplate(animated: true)
                        }
                        
                    }
                    else{
                        if self.carPlayManager.interfaceController?.presentedTemplate != nil {
                            if #available(iOS 14.0, *) {
                                self.carPlayManager.interfaceController?.dismissTemplate(animated: false, completion: { _, _ in
                                    self.safePresentTemplate(self.signInAlertTemplate, animated: false)
                                })
                            } else {
                                // Fallback on earlier versions
                                self.carPlayManager.interfaceController?.dismissTemplate(animated: false)
                                self.safePresentTemplate(self.signInAlertTemplate, animated: false)
                            }
                        } else {
                            self.safePresentTemplate(self.signInAlertTemplate, animated: false)
                        }
                    }
                }
                
                
            }
        }
    }
}

extension AppDelegate {
    func getVisiblePadding() -> UIEdgeInsets {
        var padding = UIEdgeInsets.zero
#if targetEnvironment(simulator)
        padding = UIEdgeInsets(top: 75, left: 270, bottom:  50, right: 50)
#else
        padding = UIEdgeInsets(top: 75, left: 50, bottom:  50, right: 270)
#endif
        return padding
    }
}

//
//  CPCarParkVC.swift
//  Breeze
//
//  Created by Malou Mendoza on 15/10/21.
//

import Foundation
import MapboxDirections
import Turf
import MapboxCoreNavigation
@_spi(Restricted) import MapboxMaps
import MapboxCoreMaps
import MapboxNavigation
import Combine
import SwiftyBeaver
import CarPlay


class CPCarParkVC: NSObject {
    private weak var navigationMapView: NavigationMapView?
    private weak var carPlayManager: CarPlayManager?

    //for carpark annotations
    var carparkViewModel: CarparksViewModel?

    var disposables = Set<AnyCancellable>()
    var currentSelectedCarpark: Carpark? // callout

    private var destinationCarPark: Carpark?// the destination has a carpark
    private var cheapestCarPark: Carpark?
//    private var nearestCarPark: Carpark?
    private var filteredCarParks = [Carpark]()
    private var trips = [CPTrip]()
    private var carparkCoordinates: [CLLocationCoordinate2D] = []
    private var selectedCarpark: Carpark?
    var response: RouteResponse?

    var waypoints: [Waypoint] = [] {
        didSet {
            waypoints.forEach {
                $0.coordinateAccuracy = -1
            }
        }
    }

    typealias RouteRequestSuccess = ((RouteResponse) -> Void)
    typealias RouteRequestFailure = ((Error) -> Void)

    init(navigationMapView: NavigationMapView, carPlayManager: CarPlayManager) {
        SwiftyBeaver.debug("CPCarParkVC init")
        self.navigationMapView = navigationMapView
        self.carPlayManager = carPlayManager
        self.navigationMapView?.mapView.ornaments.options.attributionButton.visibility = .hidden
    }

    func showMap() {
        
        DispatchQueue.main.async {
//            if let mapTemplate = self.carPlayManager?.interfaceController?.rootTemplate as? CPMapTemplate {
//                mapTemplate.mapDelegate = self
//                mapTemplate.showTripPreviews(self.trips, textConfiguration: self.defaultTripPreviewTextConfiguration())
//            }
            self.pushToNewMapTemplateWithTrip(self.trips)
        }
    }

    private func defaultTripPreviewTextConfiguration() -> CPTripPreviewTextConfiguration {

        let goTitle = "Navigate here"

        let alternativeRoutesTitle = "Other routes"

        let overviewTitle = "Overview"

        let defaultPreviewText = CPTripPreviewTextConfiguration(startButtonTitle: goTitle,
                                                                additionalRoutesButtonTitle: alternativeRoutesTitle,
                                                                overviewButtonTitle: overviewTitle)
        return defaultPreviewText
    }

    func showCarparks() {
        
        //call api carpark nearby
        carparkViewModel = CarparksViewModel(service: CarparkService(), arrivalTime: Date(), destName: "")
        loadCarparks()
        navigationMapView?.pointAnnotationManager?.delegate = self
    }

    func hideCarparks() {
        //call api carpark nearby
        carparkViewModel = CarparksViewModel(service: CarparkService(), arrivalTime: Date(), destName: "")
        self.navigationMapView?.mapView.removeCarparkLayer()
        let annotations = [PointAnnotation]()
        self.navigationMapView?.pointAnnotationManager?.annotations = annotations

        carparkViewModel = nil

        let mapTemplate = self.carPlayManager?.interfaceController?.rootTemplate as? CPMapTemplate
        mapTemplate?.hideTripPreviews()

        for button in mapTemplate?.leadingNavigationBarButtons ?? [] {
//            if(button.title == Constants.cpCarParkHideTitle) {
                button.title = Constants.cpCarParkTitle
//            }
        }
        self.carPlayManager?.navigationMapView?.navigationCamera.follow()
    }

    private func loadCarparks() {

        if let loc = self.navigationMapView?.mapView.location.latestLocation {
            carparkViewModel?.load(at: loc.coordinate, count: UserSettingsModel.sharedInstance.carparkCount, radius: UserSettingsModel.sharedInstance.carparkDistance, maxRadius: Values.carparkMonitorMaxDistanceInLanding)
            carparkViewModel?.$carparks.sink { [weak self] carparks in
                guard let self = self, let carparks = carparks else { return }

                let cheapest = carparks.filter ({ $0.isCheapest! })
                if(cheapest.count > 0)
                {
                    self.cheapestCarPark = cheapest[0]
                }

                if let cheapestCarPark = self.cheapestCarPark {

                    self.filteredCarParks.append(cheapestCarPark)
                }

                carparks.forEach { carPark in

                    if let cheapestCarPark = self.cheapestCarPark {

                        if(cheapestCarPark.name != carPark.name) {

                            if(self.filteredCarParks.count != 12)
                            {
                                self.filteredCarParks.append(carPark)
                            }
                        }
                    }
                }
                if(self.filteredCarParks.count > 0) {

                    let destinationCoordinate = CLLocationCoordinate2D(latitude: self.filteredCarParks[0].lat ?? 0, longitude: self.filteredCarParks[0].long ?? 0)
                    let waypoint = Waypoint(coordinate: destinationCoordinate, name: "Dropped Pin #\(self.waypoints.endIndex + 1)")

                    waypoint.targetCoordinate = destinationCoordinate
                    self.waypoints.append(waypoint)

                    self.requestRoute()
                    
                } else {
                    //  Show no carpark toast message
                    appDelegate().showCommonToastWithMessage("No carparks nearby")
                }
                
            }.store(in: &disposables)
        }
    }

    private func addCarparkAnnotation(carpark: Carpark, selected: Bool = false) -> PointAnnotation? {

        if let carparkId = carpark.id {
            var annotation = PointAnnotation(id: carparkId, coordinate: CLLocationCoordinate2D(latitude: carpark.lat ?? 0, longitude: carpark.long ?? 0))
            annotation.image = getAnnotationImage(carpark: carpark, selected: selected, ignoreDestination: true)
            annotation.userInfo = [
                "carpark": carpark
            ]

            if let layerId = navigationMapView?.pointAnnotationManager?.layerId, !layerId.isEmpty {
                try? self.navigationMapView?.mapView.mapboxMap.style.setLayerProperty(for: layerId, property: "icon-allow-overlap", value: true)
            }
            return annotation
        }
        return nil
    }

    private func getNearestCarpark(carparks: [Carpark]) -> Carpark? {
        // get the cheapest (which is the nearest carpark)
        if let nearest = (carparks.filter { $0.isCheapest ?? false }).first {
            return nearest
        }
        return nil
    }
    
    private func calculateFurthestDistance(coordinates:[CLLocationCoordinate2D],defaultFurthestDistance:Double,nearestCoordinate:CLLocationCoordinate2D) -> Double {
        
        var finalFurthestDistance = defaultFurthestDistance
        for coordinate in coordinates {
            
            let toLocation = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
            
            let fromLocation = CLLocation(latitude: nearestCoordinate.latitude, longitude: nearestCoordinate.longitude)
            
            let distance = fromLocation.distance(from: toLocation)
            
            if(distance > finalFurthestDistance){
                
                finalFurthestDistance = distance
            }
             
        }
        
        return finalFurthestDistance
    }
    
    private func setZoomLevelCarparksInCarplay(location: CLLocationCoordinate2D, coordinates: [CLLocationCoordinate2D]) {
        self.navigationMapView?.navigationCamera.stop()
        
        var allCoordinates: [CLLocationCoordinate2D] = []
        allCoordinates.append(location)
        allCoordinates.append(contentsOf: coordinates)
        
        let padding = appDelegate().getVisiblePadding()
        if let cameraOptions = self.navigationMapView?.mapView.mapboxMap.camera(for: allCoordinates,
                                                                                padding: padding,
                                                                                bearing: 0,
                                                                                pitch: 0), let mapView = self.navigationMapView?.mapView {
            mapView.camera.ease(to: cameraOptions, duration: 0.4, curve: .easeInOut, completion: nil)
        }
    }
    
    private func setZoomLevelWhenUserSelectCapark(coordinate1: CLLocationCoordinate2D, coordinate2: CLLocationCoordinate2D) {
        self.navigationMapView?.navigationCamera.stop()
        
        var allCoordinates: [CLLocationCoordinate2D] = []
        allCoordinates.append(contentsOf: [coordinate1, coordinate2])
        let padding = appDelegate().getVisiblePadding()
        
        if let cameraOptions = self.navigationMapView?.mapView.mapboxMap.camera(for: allCoordinates,
                                                                                padding: padding,
                                                                                bearing: 0,
                                                                                pitch: 0), let mapView = self.navigationMapView?.mapView {
            mapView.camera.ease(to: cameraOptions, duration: 0.4, curve: .easeInOut, completion: nil)
        }
        
    }

    private func setupZoomelevel(coordinate1: CLLocationCoordinate2D, coordinate2: CLLocationCoordinate2D) {

        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }

            self.navigationMapView?.navigationCamera.stop()
            // use coordinate1 as center point calcuate the symmetric coordinate3
            let degree = coordinate2.direction(to: coordinate1)
            let distance = coordinate2.distance(to: coordinate1)
            let coordinate3 = coordinate1.coordinate(at: distance, facing: degree)

            if let cameraOptions = self.navigationMapView?.mapView.mapboxMap.camera(for: [coordinate2, coordinate1, coordinate3], padding: UIEdgeInsets(top: 50, left: 150, bottom: 50, right: 50), bearing: 0, pitch: 0) {
                self.navigationMapView?.mapView.camera.ease(to: cameraOptions, duration: Values.standardAnimationDuration, curve: .easeInOut, completion: nil)
            }
           
        }
    }

    func requestRoute() {
        guard waypoints.count > 0 else { return }
        guard let coordinate = navigationMapView?.mapView.location.latestLocation?.coordinate else {
            print("User location is not valid. Make sure to enable Location Services.")
            return
        }

        let location = CLLocation(latitude: coordinate.latitude,
                                  longitude: coordinate.longitude)
        let userWaypoint = Waypoint(location: location)
        waypoints.insert(userWaypoint, at: 0)

        let navigationRouteOptions = NavigationRouteOptions(waypoints: waypoints)
        navigationRouteOptions.locale = Locale.enSGLocale()

        requestRoute(with: navigationRouteOptions, success: defaultSuccess, failure: defaultFailure)
    }

    private func getPriceDetails(rate: Double, firstHour: Bool = true) -> String {

        var priceString = ""
        if Int(rate) == 0 {

            if firstHour {
                priceString = "Free(1st hr) ."
            } else {

                priceString = "Free(next half)"
            }
        } else if Int(rate) == -2 {
            if firstHour {
                priceString = "Season(1st hr) ."
            } else {

                priceString = "Season(next half)"
            }
        } else if Int(rate) == -1 {
            if(firstHour) {
                priceString = "No Info(1st hr) ."
            } else {
                priceString = "No Info(next half)"
            }
        } else {
            priceString = String(format: "$%.2f", rate)

            if firstHour {
                priceString = "\(priceString)(1st hr) . "
            } else {

                priceString = "\(priceString)(next half)"
            }
        }
        return priceString
    }

    fileprivate lazy var defaultSuccess: RouteRequestSuccess = { [weak self] (response) in
        
        guard let routes = response.routes, !routes.isEmpty, case let .route(options) = response.options else { return }
        guard let self = self else { return }
        self.navigationMapView?.removeWaypoints()
        self.response = response

        // Waypoints which were placed by the user are rewritten by slightly changed waypoints
        // which are returned in response with routes.
        if let waypoints = response.waypoints {
            self.waypoints = waypoints
        }

        let info = CPTrip.cpGetRouteChoice(routeResponse: response)
        guard let coordinate = self.navigationMapView?.mapView.location.latestLocation?.coordinate else {
            print("User location is not valid. Make sure to enable Location Services.")
            return
        }
        let currentLocMapItem = MKMapItem(placemark: MKPlacemark(coordinate: coordinate))
        self.filteredCarParks.forEach { carPark in

            let carParkLocMapItem = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: carPark.lat ?? 0, longitude: carPark.long ?? 0)))
            carParkLocMapItem.name = carPark.name

            var carParkAvailable = ""
            var carParkCurrentHrRate = ""
            var carParkNextHrRate = ""

            if carPark.parkingType == Carpark.typeSeason {

                carParkAvailable = "All-day season parking"
            } else if(carPark.parkingType == Carpark.typeCustomer) {

                if(carPark.availablelot?.rawValue == "NA") {

                    carParkAvailable = "Lots No Info"
                    carParkCurrentHrRate = "Strictly for customers only."
                } else {
                    if let distance = carPark.distance {
                        if let carParkAvail = carPark.availablelot?.intValue {
                            if carParkAvail != -1 {
                                carParkAvailable = "\(distance)m . \(carParkAvail) avail lots"
                            } else {
                                carParkAvailable = "\(distance)m"
                            }
                            
                        }
                        
                    }
                   
                    carParkCurrentHrRate = "Strictly for customers only."
                }

            } else {
                if(carPark.availablelot?.rawValue == "NA") {
                    if let distance = carPark.distance {
                        carParkAvailable = "\(distance)m \(carPark.availablelot?.intValue ?? 0) avail lots"
                    }
                } else {
                    if let distance = carPark.distance {
                        if let carParkAvail = carPark.availablelot?.intValue {
                            if carParkAvail != -1 {
                                carParkAvailable = "\(distance)m . \(carParkAvail) avail lots"
                            } else {
                                carParkAvailable = "\(distance)m"
                            }
                            
                        }
                        
                    }
                }

                if let rate = carPark.currentHrRate?.oneHrRate {
                    carParkCurrentHrRate = self.getPriceDetails(rate: rate) ?? "No Info(1st hr) ."
                } else {
                    carParkCurrentHrRate = "No Info(1st hr) ."
                }

                if let nextRate = carPark.nextHrRate?.oneHrRate {

                    carParkNextHrRate = self.getPriceDetails(rate: nextRate, firstHour: false) ?? "No Info(next half)"
                } else {
                    carParkNextHrRate = "No Info(next half)"
                }
            }

            let detailsCP = "\(carParkAvailable) \n \(carParkCurrentHrRate)"

            let routeChoice = CPRouteChoice(summaryVariants: [carParkAvailable],
                                            additionalInformationVariants: [detailsCP],
                                            selectionSummaryVariants: [carParkNextHrRate])
            routeChoice.userInfo = info
            let trip = CPTrip.init(origin: currentLocMapItem, destination: carParkLocMapItem, routeChoices: [routeChoice])
            self.trips.append(trip)
        }
        
        self.showMap()
        self.updateSelectedCarpark(self.selectedCarpark)

    }
    
    fileprivate func updateSelectedCarpark(_ carpark: Carpark?, needUpdateZoomLevel: Bool = true) {
        
        guard let coordinate = self.navigationMapView?.mapView.location.latestLocation?.coordinate else {
            print("User location is not valid. Make sure to enable Location Services.")
            return
        }
        
        self.navigationMapView?.mapView.removeCarparkLayer()
        self.navigationMapView?.pointAnnotationManager?.annotations = [PointAnnotation]()
        self.selectedCarpark = carpark
        
        let carParks = self.filteredCarParks
        self.carparkCoordinates.removeAll()
        //  Show all carparks in Home
        if !carParks.isEmpty {
            self.navigationMapView?.mapView.addCarparks(carParks, showOnlyWithCostSymbol: true, belowPuck: false)
            
            var annotations = [PointAnnotation]()
            for carpark in carParks {
                var isSelected: Bool = false
                if let theCarpark = self.selectedCarpark {
                    if theCarpark.name == carpark.name {
                        isSelected = true
                    }
                }
                
                //  Save the selected carpark and change status of selected
                if let annotation = self.addCarparkAnnotation(carpark: carpark, selected: isSelected) {
                    annotations.append(annotation)
                }
                
                self.carparkCoordinates.append(CLLocationCoordinate2D(latitude: carpark.lat ?? 0.0, longitude: carpark.long ?? 0.0))
            }
            
            self.navigationMapView?.pointAnnotationManager?.annotations = annotations
            
            if needUpdateZoomLevel {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    self.setZoomLevelCarparksInCarplay(location: coordinate, coordinates: self.carparkCoordinates)
                }
            }
            
        } else {
            //  Need to handle no carpark nearby
        }
    }

    fileprivate lazy var defaultFailure: RouteRequestFailure = { [weak self] (error) in
        guard let self = self else { return }
        // Clear routes from the map
        self.response = nil
    }

    func requestRoute(with options: RouteOptions, success: @escaping RouteRequestSuccess, failure: RouteRequestFailure?) {
        NavigationSettings.shared.directions.calculateWithCache(options: options) { (session, result) in
            switch result {
            case let .success(response):
                success(response)
            case let .failure(error):
                failure?(error)
            }
        }
    }
    
    func pushToNewMapTemplateWithTrip(_ trips: [CPTrip]) {
        
        
        let newMapTemplate = CPMapTemplate()
        newMapTemplate.trailingNavigationBarButtons = []
        newMapTemplate.leadingNavigationBarButtons = []
        newMapTemplate.mapDelegate = self
        
        let backBtn = CPBarButton(type: .text) { (barButton) in
            newMapTemplate.hideTripPreviews()
            self.carPlayManager?.interfaceController?.popTemplate(animated: true)
            self.hideCarparks()
        }
        backBtn.title = "Back"
        newMapTemplate.backButton = backBtn
        
        self.carPlayManager?.interfaceController?.pushTemplate(newMapTemplate, animated: true)
        newMapTemplate.showTripPreviews(trips, textConfiguration: self.defaultTripPreviewTextConfiguration())
    }
}

extension CPCarParkVC: AnnotationInteractionDelegate {
    func annotationManager(_ manager: AnnotationManager, didDetectTappedAnnotations annotations: [Annotation]) {

        // possible multiple tapped if there are overlapped together, only show the first one
        if let annotation = annotations.first {
            didSelectAnnotation(annotation: annotation, tapped: true)
        }
    }

    func didSelectAnnotation(annotation: Annotation, tapped: Bool = false) {

        DispatchQueue.main.async { [weak self] in

            guard let self = self else { return }

            if let carpark = annotation.userInfo?["carpark"] as? Carpark {

                if carpark.id == self.currentSelectedCarpark?.id {
                    self.currentSelectedCarpark = nil
                    // click the same one, don't show callout
                    return
                }
                self.currentSelectedCarpark = carpark

                // set camera (actually should not set if not because of tapped
                if tapped {
                    let cameraOptions = CameraOptions(center: CLLocationCoordinate2D(latitude: carpark.lat ?? 0, longitude: carpark.long ?? 0), zoom: self.navigationMapView?.mapView.mapboxMap.cameraState.zoom) // using current zoom level
                }
            }
        }
    }
}

extension CPCarParkVC: CPMapTemplateDelegate {

    func mapTemplate(_ mapTemplate: CPMapTemplate, selectedPreviewFor trip: CPTrip, using routeChoice: CPRouteChoice) {

        let matchedCarPark = self.filteredCarParks.filter { $0.name == trip.destination.placemark.name }
        if matchedCarPark.count > 0 {
            self.updateSelectedCarpark(matchedCarPark.first)
        }
    }

    func mapTemplate(_ mapTemplate: CPMapTemplate, startedTrip trip: CPTrip, using routeChoice: CPRouteChoice) {
        
        let matchedCarPark = self.filteredCarParks.filter ({ $0.name == trip.destination.placemark.name })
        if let carpark = matchedCarPark.first {
            let location = CLLocation(latitude: carpark.lat ?? 0.0, longitude: carpark.long ?? 0.0)
            getAddressFromLocation(location: location) { address in

                let address = SearchAddresses(alias: "", address1: carpark.name ?? "", lat: "\(carpark.lat ?? 0.0)", long: "\(carpark.long ?? 0.0)", address2: address, distance: "\(carpark.distance ?? 0)")

                self.hideCarparks()

                let appDelegate = appDelegate()
                if let carPlayManager = self.carPlayManager,
                   let navigationMapView = self.carPlayManager?.navigationMapView {
                    appDelegate.carRPController = CarPlayRPController(navigationMapView: navigationMapView,
                                                                      carPlayManager: carPlayManager, address: address)
                    appDelegate.carRPController?.getRoutes()
                }
            }
        }
    }
}

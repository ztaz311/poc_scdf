//
//  CarPlayRPController.swift
//  Breeze
//
//  Created by VishnuKanth on 15/10/21.
//

import Foundation
import MapboxDirections
import Turf
import MapboxCoreNavigation
@_spi(Restricted) import MapboxMaps
import MapboxCoreMaps
import MapboxNavigation
import Combine
import SwiftyBeaver
import CarPlay

// I'm using this one for both MapLanding and Navigation
final class CarPlayRPController {
    private weak var routePlanningViewModel: RoutePlanningViewModel?
    private weak var navigationMapView: NavigationMapView!
    private weak var carPlayManager: CarPlayManager!
    private var  mapTemplateProvider: MapTemplateProvider
    public var address:SearchAddresses?
    var updateMapCancellable: AnyCancellable?
    var route:Route?
    
    init(navigationMapView: NavigationMapView, carPlayManager: CarPlayManager,address:SearchAddresses) {
        SwiftyBeaver.debug("CarPlayRPController init")
        
        self.navigationMapView = navigationMapView
        self.carPlayManager = carPlayManager
        self.address = address
        if(appDelegate().routePlanningViewModel == nil)
        {
            appDelegate().routePlanningViewModel = RoutePlanningViewModel()
            
            
        }
        appDelegate().carPlayMapController?.stopCruise()
        self.mapTemplateProvider = MapTemplateProvider()
        self.routePlanningViewModel = appDelegate().routePlanningViewModel
        self.routePlanningViewModel?.isCarPlay = true
        if let routePlanningViewModel = routePlanningViewModel {
            
            if(routePlanningViewModel.wayPointsAdded.count > 0){
                
                routePlanningViewModel.wayPointsAdded.removeAll()
            }
        }
        
        self.navigationMapView.mapView.ornaments.options.attributionButton.visibility = .hidden
        
        self.routePlanningViewModel?.carPlayMapViewUpdatable = CarPlayRPMapViewUpdatable(navigationMapView: carPlayManager.navigationMapView!)
        
        
        
    }
    
    deinit{
        print("Car Play RP Controller")
    }
    
    func clearPreviousRouteResponse(){
        
        self.routePlanningViewModel?.carPlayMapViewUpdatable?.defaultResponse = nil
        self.routePlanningViewModel?.carPlayMapViewUpdatable?.bestOfRouteResponse = nil
        self.routePlanningViewModel?.carPlayMapViewUpdatable?.cheapestResponse = nil
        self.routePlanningViewModel?.carPlayMapViewUpdatable?.shortestResponse = nil
        self.routePlanningViewModel?.carPlayMapViewUpdatable?.updateMap = false
    }

    
    func getRoutes(){
        
        if let traitCollection = (self.carPlayManager.carWindow?.rootViewController as? CarPlayMapViewController)?.traitCollection,
           let interfaceController = carPlayManager.interfaceController {
            
    
            self.routePlanningViewModel?.setUpRouteColors(isDarkMode: self.carPlayManager.carPlayMapViewController?.isDarkMode ?? false)
            self.routePlanningViewModel?.carPlayBaseAddress = self.address
            self.routePlanningViewModel?.setCarPlayRPAddress()
            self.routePlanningViewModel?.selectionSource = SourceAddressSelection.AddressSearch
            self.routePlanningViewModel?.rankingIndex = 1
            self.routePlanningViewModel?.carPlayMapViewUpdatable?.setRouteLineWidth()
            self.routePlanningViewModel?.setupRouteForCarPlay(fromMobile: false)
            
            appDelegate().updateMapCancellable = self.routePlanningViewModel?.carPlayMapViewUpdatable!.$updateMap
            .receive(on: DispatchQueue.main)
            .sink(receiveValue: { [weak self] updateMap in
            guard let self = self else { return }
            if updateMap {
                
                if(appDelegate().insideCP == true)
                {
                    self.calculateTrip(traitCollection: traitCollection, interfaceController: interfaceController)
                }
                else
                {
                    SwiftyBeaver.debug("Going to Show Route Preview method")
                    self.showRoutePreview(traitCollection: traitCollection, interfaceController: interfaceController)
                }
                
                
            }
            else {
            }
        })
            
            
        }
    }
    
    private func showRoutePreview(traitCollection:UITraitCollection,interfaceController:CPInterfaceController){
        
        DispatchQueue.main.async {
            
//            if(UserSettingsModel.sharedInstance.routePref == Constants.fast)
//            {
//                self.routePlanningViewModel?.response = self.routePlanningViewModel?.carPlayMapViewUpdatable.defaultResponse
//            }
////
//            if(UserSettingsModel.sharedInstance.routePref == Constants.short)
//            {
//                //To fix - As of now we are using fastest route for shortest as well and filtering by distance
//                self.routePlanningViewModel?.response = self.routePlanningViewModel?.shortestResponse
//            }
////
//            if(UserSettingsModel.sharedInstance.routePref == Constants.cheap)
//            {
//                self.routePlanningViewModel?.response = self.routePlanningViewModel?.cheapestResponse
//            }
//            
//            if(UserSettingsModel.sharedInstance.routePref == nil)
//            {
//                self.routePlanningViewModel?.response = self.routePlanningViewModel?.defaultResponse
//            }
        
            AnalyticsManager.shared.logScreenView(screenName:  ParameterName.CarplayRoutePreview.screen_view)
            
            if let carPlayUpdatable = self.routePlanningViewModel?.carPlayMapViewUpdatable,
               let cheapestResponse = carPlayUpdatable.cheapestResponse,
               let defaultResponse = carPlayUpdatable.defaultResponse,
               let shortestResponse = carPlayUpdatable.shortestResponse {
                carPlayUpdatable.response = carPlayUpdatable.erpPriceUpdater.findBestOfRoutesForCarPlay(fastestResponse: defaultResponse, shortestResponse: shortestResponse, cheapestResponse: cheapestResponse)
            } else {
                SwiftyBeaver.error("Route response empty")
            }
            
//            self.routePlanningViewModel?.carPlayMapViewUpdatable?.response =  self.routePlanningViewModel?.carPlayMapViewUpdatable?.erpPriceUpdater.findBestOfRoutesForCarPlay(fastestResponse: (self.routePlanningViewModel?.carPlayMapViewUpdatable?.defaultResponse)!, shortestResponse: (self.routePlanningViewModel?.carPlayMapViewUpdatable?.shortestResponse)!, cheapestResponse: (self.routePlanningViewModel?.carPlayMapViewUpdatable?.cheapestResponse)!)
            
            SwiftyBeaver.debug("Show Route Preview")
            appDelegate().popTemplate(animated: true) { [weak self] _, _ in
                guard let self = self else { return }
                if let res = self.routePlanningViewModel?.carPlayMapViewUpdatable?.response {
                    self.carPlayManager.previewRoutes(for: res)
                }
            }
            
//            var trip = CPTrip(routeResponse: (self.routePlanningViewModel?.carPlayMapViewUpdatable!.response)!)
//            trip = self.carPlayManager.delegate?.carPlayManager(self.carPlayManager, willPreview: trip) ?? trip
//
//            let previewMapTemplate = self.mapTemplateProvider.mapTemplate(forPreviewing: trip, traitCollection: traitCollection, carPlayManager: self.carPlayManager)
//            var previewText = self.defaultTripPreviewTextConfiguration()
//            if let customPreviewText = self.carPlayManager.delegate?.carPlayManager(self.carPlayManager, willPreview: trip, with: previewText) {
//            previewText = customPreviewText
//        }
//        previewMapTemplate.showTripPreviews([trip], textConfiguration: previewText)
//        interfaceController.pushTemplate(previewMapTemplate, animated: true)
        
            //self.fitCamera(routes: (self.routePlanningViewModel?.carPlayMapViewUpdatable?.response?.routes)!)
    }
    }
    
    func calculateTrip(traitCollection:UITraitCollection,interfaceController:CPInterfaceController)
    {
        
//        let mapTemplate = self.carPlayManager.interfaceController?.topTemplate as? CPMapTemplate
        self.routePlanningViewModel?.carPlayMapViewUpdatable?.response =  self.routePlanningViewModel?.carPlayMapViewUpdatable?.erpPriceUpdater.findBestOfRoutesForCarPlay(fastestResponse: (self.routePlanningViewModel?.carPlayMapViewUpdatable?.defaultResponse)!, shortestResponse: (self.routePlanningViewModel?.carPlayMapViewUpdatable?.shortestResponse)!, cheapestResponse: (self.routePlanningViewModel?.carPlayMapViewUpdatable?.cheapestResponse)!)
        
        appDelegate().popTemplate(animated: true) { [weak self] _, _ in
            guard let self = self else { return }
            if let res = self.routePlanningViewModel?.carPlayMapViewUpdatable?.response {
                self.carPlayManager.previewRoutes(for: res)
            }
        }
        
        
        
//        var trip = CPTrip(routeResponse: (self.routePlanningViewModel?.carPlayMapViewUpdatable!.response)!)
//        trip = self.carPlayManager.delegate?.carPlayManager(self.carPlayManager, willPreview: trip) ?? trip
//
//
//        var previewText = self.defaultTripPreviewTextConfiguration()
//        if let customPreviewText = self.carPlayManager.delegate?.carPlayManager(self.carPlayManager, willPreview: trip, with: previewText) {
//        previewText = customPreviewText
//       }
//        mapTemplate!.showTripPreviews([trip], textConfiguration: previewText)
    }
    
//    func refreshRoutes(){
//        carPlayManager.navigationMapView.removeRoutes()
//        car
//    }
    
    private func fitCamera(routes:[Route]){
        
        for route in routes {
            guard let routeShape = route.shape, !routeShape.coordinates.isEmpty else { return }
        }
        
        let multiLineString = MultiLineString(routes.map({route in route.shape!.coordinates}))
        let edgeInsets = (carPlayManager.navigationMapView?.safeViewArea ?? 0.0) + UIEdgeInsets().centerCameraEdgeInsets
        if let cameraOptions = carPlayManager.navigationMapView?.mapView.mapboxMap.camera(for: .multiLineString(multiLineString),
                                                                                          padding: edgeInsets,
                                                                                          bearing: nil,
                                                                                          pitch: nil) {
            carPlayManager.navigationMapView?.mapView.mapboxMap.setCamera(to: cameraOptions)
        }
    }
    
    private func defaultTripPreviewTextConfiguration() -> CPTripPreviewTextConfiguration{
        
        let goTitle = "Let's go!"
        
        let alternativeRoutesTitle = "Other routes"
        
        let overviewTitle = "Overview"

        let defaultPreviewText = CPTripPreviewTextConfiguration(startButtonTitle: goTitle,
                                                                additionalRoutesButtonTitle: alternativeRoutesTitle,
                                                                overviewButtonTitle: overviewTitle)
        return defaultPreviewText
        
    }
    
    func getRouteDistance()->String{
        
        if (route?.distance ?? 0.0) > 1000 {
            let unit = " km"
            return "\(String(format: "%.1f", (route?.distance ?? 0.0)/1000))\(unit)"
        } else {
             let unit = " m"
            return "\(Int((route?.distance ?? 0.0)))\(unit)"
        }
        
    }
    
    func getEstTime() -> String{
        
        let time = self.calEstimatedArrivalTime((route?.expectedTravelTime ?? 0))
        return "\(time.time)\(time.format)"
        
    }
    
    func getDestinationName()->String {
    
        return route?.description ?? ""
    }
    
    func getTripETA(etaFav:Favourites){
        
        let etaMessage = "Hey, \(AWSAuth.sharedInstance.userName) here! I will be at \(address?.address1 ?? "") around \(getEstTime())."
       
        let tripETA = TripETA(type: TripETA.ETAType.Init, message: etaMessage, recipientName: etaFav.recipientName ?? "", recipientNumber: etaFav.recipientNumber ?? "", shareLiveLocation: etaFav.shareLiveLocation ?? "", tripStartTime: Int(Date().timeIntervalSince1970), tripEndTime: Int(Date().timeIntervalSince1970 + (route?.expectedTravelTime ?? 0)), tripDestLat: address?.lat?.toDouble() ?? 0, tripDestLong: address?.long?.toDouble() ?? 0, destination: address?.address1 ?? "", tripEtaFavouriteId: etaFav.tripEtaFavouriteId ?? 0)
        appDelegate().tripETA = tripETA
    }
    
    private func calEstimatedArrivalTime(_ duration: TimeInterval) -> (time: String,format: String) {
        let arrivalDate = Date(timeInterval: duration, since: Date())
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: arrivalDate)
        let t = hour > 12 ? hour - 12: hour
        let hourString = t < 10 ? "0\(t)" : t.description
        let minute = calendar.component(.minute, from: arrivalDate)
        let minString = minute < 10 ? "0\(minute)" : minute.description
        return (time: "\(hourString):\(minString)", format: hour >= 12 ? " PM" : " AM")
    }
    
    
}

extension CarPlayRPController{
    
    func tripSelected(route:Route){
        
        self.route = route
    }
}

//
//  CarParkRoutePlanning.swift
//  Breeze
//
//  Created by VishnuKanth on 02/11/21.
//

import Foundation
import MapboxDirections
import Turf
import MapboxCoreNavigation
@_spi(Restricted) import MapboxMaps
import MapboxCoreMaps
import MapboxNavigation
import Combine
import SwiftyBeaver
import CarPlay

// Keep CPCarParkVC not changed. This is for route planning carpark for CarPlay
class CPRoutePlanningCarParkVC:NSObject {
    private weak var navigationMapView: NavigationMapView!
    private weak var carPlayManager: CarPlayManager!
    private var trips = [CPTrip]()
    private var currentTrip: CPTrip!

    //for carpark annotations
    var carparkViewModel: CarparksViewModel!
    var disposables = Set<AnyCancellable>()
    private var currentSelectedCarpark: Carpark?
    private var destinationCarPark: Carpark?// the destination has a carpark
    private var cheapestCarPark: Carpark?
    private var nearestCarPark: Carpark?
    private var filteredCarParks = [Carpark]()
    private var currentCarpark: Carpark?
    private var carparkCoordinates = [CLLocationCoordinate2D]()
    
    init(navigationMapView: NavigationMapView, carPlayManager: CarPlayManager, trip: CPTrip) {
        SwiftyBeaver.debug("CPCarParkVC init")
        
        self.navigationMapView = navigationMapView
        self.carPlayManager = carPlayManager
        self.currentTrip = trip
        self.navigationMapView.mapView.ornaments.options.attributionButton.visibility = .hidden
    
    }
    
    func showMap(){
        
        DispatchQueue.main.async {
            appDelegate().insideCP = true
            self.pushToNewMapTemplateWithTrip(self.trips)
            /*
            if let mapTemplate = self.carPlayManager.interfaceController?.topTemplate as? CPMapTemplate {
                mapTemplate.mapDelegate = self
                mapTemplate.hideTripPreviews()
                mapTemplate.showTripPreviews(self.trips, textConfiguration: self.defaultTripPreviewTextConfiguration())
            }*/
        }
    }
    
  
    
    
    func recalculateTrip() {
        self.hideCarparks()
        if let traitCollection = (self.carPlayManager.carWindow?.rootViewController as? CarPlayMapViewController)?.traitCollection,
           let interfaceController = carPlayManager.interfaceController,
           let carRPController = appDelegate().carRPController {
            carRPController.calculateTrip(traitCollection: traitCollection, interfaceController: interfaceController)
        }
    }
    
    func cleanUpMap() {
        guard let carplayRouteMapView = appDelegate().routePlanningViewModel?.carPlayMapViewUpdatable?.navigationMapView else {return}
        carplayRouteMapView.removeRoutes()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            carplayRouteMapView.navigationCamera.stop()
            self.setZoomLevelAmenityInCarplay(location: self.currentTrip.destination.placemark.location!.coordinate, coordinates: self.carparkCoordinates)
        }
    }
    
    func pushToNewMapTemplateWithTrip(_ trips: [CPTrip]) {
        
        self.cleanUpMap()
        
        let newMapTemplate = CPMapTemplate()
        newMapTemplate.trailingNavigationBarButtons = []
        newMapTemplate.leadingNavigationBarButtons = []
        newMapTemplate.mapDelegate = self
        
        let backBtn = CPBarButton(type: .text) { (barButton) in
            newMapTemplate.hideTripPreviews()
            self.carPlayManager.interfaceController?.popTemplate(animated: true)
            //  Should calculate route here again
            self.recalculateTrip()
        }
        backBtn.title = "Back"
        newMapTemplate.backButton = backBtn
        
        self.carPlayManager.interfaceController?.pushTemplate(newMapTemplate, animated: true)
        newMapTemplate.showTripPreviews(trips, textConfiguration: self.defaultTripPreviewTextConfiguration())
        
    }
    
    
    private func defaultTripPreviewTextConfiguration() -> CPTripPreviewTextConfiguration{
        
        let goTitle = "Navigate Here"
        
        let alternativeRoutesTitle = "Other routes"
        
        let overviewTitle = "Overview"

        let defaultPreviewText = CPTripPreviewTextConfiguration(startButtonTitle: goTitle,
                                                                additionalRoutesButtonTitle: alternativeRoutesTitle,
                                                                overviewButtonTitle: overviewTitle)
        return defaultPreviewText
        
    }
    
    func showCarparks(){
        
        //call api carpark nearby
        if let expectedTravelTime = appDelegate().routePlanningViewModel?.carPlayMapViewUpdatable?.response?.routes?.first?.expectedTravelTime, let name = self.currentTrip.destination.placemark.name {
            let arrivalTime = Date(timeInterval: expectedTravelTime, since: Date())
            carparkViewModel = CarparksViewModel(service: CarparkService(), arrivalTime: arrivalTime, destName:  name)
        }
        loadCarparks()
        //navigationMapView.pointAnnotationManager?.delegate = self
    }
    
    func hideCarparks(mapTopTemplate:CPMapTemplate? = nil,toPop:Bool = false){
             
            //call api carpark nearby
            carparkViewModel = CarparksViewModel(service: CarparkService(), arrivalTime: Date(), destName:  "")
            self.navigationMapView.mapView.removeCarparkLayer()
            let annotations = [PointAnnotation]()
            //self.navMapView.pointAnnotationManager?.syncAnnotations(annotations)
            self.navigationMapView.pointAnnotationManager?.annotations = annotations
//            if let callout = self.calloutView {
//                callout.dismiss(animated: true)
//                callout.removeFromSuperview()
//                self.calloutView = nil
//            }
            carparkViewModel = nil
           
        if let mapTemplate = self.carPlayManager.interfaceController?.topTemplate as? CPMapTemplate{
            mapTemplate.hideTripPreviews()
            
            for button in mapTemplate.trailingNavigationBarButtons{
//                if(button.title == Constants.cpCarParkHideTitle){
                    
                    button.title = Constants.cpCarParkTitle
//                }
            }
            mapTemplate.mapDelegate = self.carPlayManager
            
            if(toPop){
                
                appDelegate().popTemplate(animated: true)
            }
            
        }
        else{
            if let mapTemplate = mapTopTemplate{
                mapTemplate.hideTripPreviews()
                
                for button in mapTemplate.trailingNavigationBarButtons{
//                    if(button.title == Constants.cpCarParkHideTitle){
                        button.title = Constants.cpCarParkTitle
//                    }
                }
                
                mapTemplate.mapDelegate = self.carPlayManager
            }
            
        }
          
          //self.navigationMapView.navigationCamera.follow()
    }
    
    private func getPriceDetails(rate:Double,firstHour:Bool = true) -> String{
        
        var priceString = ""
        if Int(rate) == 0 {
            
            if(firstHour){
                priceString =  "Free(1st hr) ."
            }
            else{
                
                priceString =  "Free(next half)"
            }
            
            
        } else if Int(rate) == -2 {
            
            if(firstHour){
                priceString = "Season(1st hr) ."
            }
            else{
                
                priceString = "Season(next half)"
            }
            
           
        } else if Int(rate) == -1 {
            if(firstHour){
                priceString = "No Info(1st hr) ."
            }
            else{
                
                priceString = "No Info(next half)"
            }
        } else {
            priceString = String(format: "$%.2f", rate)
            
            if(firstHour){
                priceString =  "\(priceString)(1st hr) . "
            }
            else{
                
                priceString =  "\(priceString)(next half)"
            }
            
        }
        
        return priceString
    }
    
    
    private func loadCarparks() {
        
        //var filteredCarParks = [Carpark]()
        
        
        if let loc = self.navigationMapView.mapView.location.latestLocation {
            carparkViewModel.load(at: self.currentTrip.destination.placemark.coordinate, count: UserSettingsModel.sharedInstance.carparkCount, radius: UserSettingsModel.sharedInstance.carparkDistance, maxRadius: Values.carparkMonitorMaxDistanceInLanding)
            //carparkViewModel.load(at: self.currentTrip.destination.placemark.coordinate, radius: Values.carparkMonitorDistanceInRoutePlanning)
            carparkViewModel.$carparks.sink { [weak self] carparks in
                guard let self = self, let carparks = carparks else { return }
                self.carparkCoordinates.removeAll()
                let cheapest = carparks.filter ({ $0.isCheapest!})
                if(cheapest.count > 0)
                {
                    self.cheapestCarPark = cheapest[0]
                }
               
                
                let destination = carparks.filter ({ $0.destinationCarPark!})
                
                if(destination.count > 0)
                {
                    if let cheapCarPark = self.cheapestCarPark{
                        
                        if(destination[0].name != cheapCarPark.name)
                        {
                            self.destinationCarPark = destination[0]
                        }
                    }
                    else{
                        self.destinationCarPark = destination[0]
                    }
                    
                }
                    
                if let destinationCarPark = self.destinationCarPark{
                    
                    self.filteredCarParks.append(destinationCarPark)
                }
                
                if let cheapestCarPark = self.cheapestCarPark{
                    
                    self.filteredCarParks.append(cheapestCarPark)
                }
                
                carparks.forEach { carPark in
                    
                    if let destinationCarPark = self.destinationCarPark{
                        
                        if(destinationCarPark.name != carPark.name){
                            
                            if let cheapCarPark = self.cheapestCarPark{
                                
                                if(cheapCarPark.name != carPark.name){
                                    
                                    if(self.filteredCarParks.count != 12)
                                    {
                                        self.filteredCarParks.append(carPark)
                                    }
                                }
                            }
                            else
                            {
                                if(self.filteredCarParks.count != 12)
                                {
                                    self.filteredCarParks.append(carPark)
                                }
                            }
                            
                        }
                    }
                    
                    else if let cheapestCarPark = self.cheapestCarPark{
                        
                        if(cheapestCarPark.name != carPark.name){
                            
                            if(self.filteredCarParks.count != 12)
                            {
                                self.filteredCarParks.append(carPark)
                            }
                        }
                    }
                }
                

                
                let info = CPTrip.cpGetRouteChoice(routeResponse:(appDelegate().routePlanningViewModel?.carPlayMapViewUpdatable?.response)!)
//                var annotations = [PointAnnotation]()
                let currentLocMapItem = MKMapItem(placemark: (self.currentTrip?.destination.placemark)!)
                self.filteredCarParks.forEach { carPark in
                   
                    let carParkLocMapItem = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: carPark.lat!, longitude: carPark.long!)))
                    carParkLocMapItem.name = carPark.name
                   /* let carParkAvailable = "\(carPark.availablelot?.intValue ?? 0) lots . Available"
                    let carParkCurrentHrRate = "\(carPark.currentHrRate?.oneHrRate ?? 0.0)"
                    
                    
                    let carParkNextHrRate = "\(carPark.nextHrRate?.oneHrRate ?? 0.0)"*/
                    
                    var carParkAvailable = ""
                    var carParkCurrentHrRate = ""
                    var carParkNextHrRate = ""
                    
                    if carPark.parkingType == Carpark.typeSeason {
                        
                        carParkAvailable = "All-day season parking"
                    }
                    else if(carPark.parkingType == Carpark.typeCustomer){
                        
                        if(carPark.availablelot?.rawValue == "NA") {
                            
                            carParkAvailable = "No Info"
                            carParkCurrentHrRate = "Strictly for customers only."
                        }
                        else{
                            
                            if let distance = carPark.distance {
                                if let carParkAvail = carPark.availablelot?.intValue {
                                    if carParkAvail != -1 {
                                        carParkAvailable = "\(distance)m . \(carParkAvail) avail lots"
                                    } else {
                                        carParkAvailable = "\(distance)m"
                                    }
                                    
                                }
                                
                            }
                            carParkCurrentHrRate = "Strictly for customers only."
                            
                        }
                        
                    }
                    else{
                        
                        if(carPark.availablelot?.rawValue == "NA") {
                            
                            carParkAvailable = "No Info"
                        }
                        else{
                            if let distance = carPark.distance {
                                if let carParkAvail = carPark.availablelot?.intValue {
                                    if carParkAvail != -1 {
                                        carParkAvailable = "\(distance)m . \(carParkAvail) avail lots"
                                    } else {
                                        carParkAvailable = "\(distance)m"
                                    }
                                    
                                }
                                
                            }
                            
                            
                        }
                        
                        if let rate = carPark.currentHrRate?.oneHrRate {
                            
                            carParkCurrentHrRate =  self.getPriceDetails(rate: rate)
                        }
                        else{
                            
                            carParkCurrentHrRate = "No Info(1st hr) ."
                        }
                        
                        if let nextRate = carPark.nextHrRate?.oneHrRate {
               
                            carParkNextHrRate = self.getPriceDetails(rate: nextRate,firstHour:false)
                        } else {
                            
                            carParkNextHrRate = "No Info(next half)"
                        }
                        
                    }
                    
                    //let detailsCP = "\(carParkAvailable) \n $\(carParkCurrentHrRate)(1st hr) . $\(carParkNextHrRate)(next half)"
                    
                    let detailsCP = "\(carParkAvailable) \n \(carParkCurrentHrRate)"
                    
                    let routeChoice = CPRouteChoice(summaryVariants:[carParkAvailable],
                                                    additionalInformationVariants: [detailsCP],
                                                    selectionSummaryVariants: [carParkNextHrRate])
                    routeChoice.userInfo = info
                    let trip = CPTrip.init(origin: currentLocMapItem, destination: carParkLocMapItem, routeChoices: [routeChoice])
                    self.trips.append(trip)
                    
                }
                
                if !self.trips.isEmpty {
                    self.showMap()
                    self.updateSelectedCarpark(nil, needUpdateZoomLevel: false)
                }
            }.store(in: &disposables)
        }
    }
    
    
    fileprivate func updateSelectedCarpark(_ carpark: Carpark?, needUpdateZoomLevel: Bool = true) {
        
        guard let carplayNavigationMapView = appDelegate().routePlanningViewModel?.carPlayMapViewUpdatable?.navigationMapView else {return}
        
        self.currentCarpark = carpark
        
        let carParks = self.filteredCarParks
        self.carparkCoordinates.removeAll()
        
        if !carParks.isEmpty {
            carplayNavigationMapView.mapView.addCarparks(carParks, showOnlyWithCostSymbol: true, belowPuck: false)
            
            var annotations = [PointAnnotation]()
            for carpark in carParks {
                var isSelected: Bool = false
                if let theCarpark = self.currentCarpark {
                    if theCarpark.name == carpark.name {
                        isSelected = true
                    }
                }
                
                //  Save the selected carpark and change status of selected
                if let annotation = self.addCarparkAnnotation(carpark: carpark, selected: isSelected) {
                    let finalDestCoord = self.currentTrip.destination.placemark.coordinate
                    var destinationAnnotation = PointAnnotation(coordinate: finalDestCoord)
                    destinationAnnotation.image = .init(image: UIImage(named: "destinationMark")!, name: "navigationDest")
                    annotations.append(annotation)
                    annotations.append(destinationAnnotation)
                }
                
                
                self.carparkCoordinates.append(CLLocationCoordinate2D(latitude: carpark.lat ?? 0.0, longitude: carpark.long ?? 0.0))
            }
            
            carplayNavigationMapView.pointAnnotationManager?.annotations = annotations
            
            if needUpdateZoomLevel {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    self.setZoomLevelAmenityInCarplay(location: self.currentTrip.destination.placemark.coordinate, coordinates: self.carparkCoordinates)
                }
            }
            
        } else {
            //  Need to handle no carpark nearby
        }
    }
    

    
    
    private func addCarparkAnnotation(carpark: Carpark, selected: Bool = false) -> PointAnnotation? {

        if let carparkId = carpark.id, let carplayNavigationMapView = appDelegate().routePlanningViewModel?.carPlayMapViewUpdatable?.navigationMapView {
            var annotation = PointAnnotation(id: carparkId, coordinate: CLLocationCoordinate2D(latitude: carpark.lat ?? 0, longitude: carpark.long ?? 0))
            annotation.image = getAnnotationImage(carpark: carpark, selected: selected, ignoreDestination: true)
            annotation.userInfo = [
                "carpark": carpark
            ]

            if let layerId = carplayNavigationMapView.pointAnnotationManager?.layerId, !layerId.isEmpty {
                try? carplayNavigationMapView.mapView.mapboxMap.style.setLayerProperty(for: layerId, property: "icon-allow-overlap", value: true)
            }
            return annotation
        }
        return nil
    }
    
    private func getNearestCarpark(carparks: [Carpark]) -> Carpark? {
        // get the cheapest (which is the nearest carpark)
        if let nearest = (carparks.filter { $0.isCheapest ?? false }).first {
            return nearest
        }
        return nil
    }
    
    
    private func setupZoomelevel(coordinate1: CLLocationCoordinate2D, coordinate2: CLLocationCoordinate2D) {
        
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
                                    
            // use coordinate1 as center point calcuate the symmetric coordinate3
            let degree = coordinate2.direction(to: coordinate1)
            let distance = coordinate2.distance(to: coordinate1)
            let coordinate3 = coordinate1.coordinate(at: distance, facing: degree)
            
            let cameraOptions = self.navigationMapView.mapView.mapboxMap.camera(for: [coordinate2, coordinate1, coordinate3], padding: UIEdgeInsets(top: 50, left: 50, bottom:50, right: 50), bearing: 0, pitch: 0)
            self.navigationMapView.mapView.camera.ease(to: cameraOptions, duration: Values.standardAnimationDuration, curve: .easeInOut, completion: nil)
        }
    }
    
    private func setDefaultCamera(coordinate: CLLocationCoordinate2D) {
        
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            
            // Stop navigation camera
            self.navigationMapView.navigationCamera.stop()

            let coordinate2 = coordinate.coordinate(at: 500, facing: -90)
            let coordinate3 = coordinate.coordinate(at: 500, facing: 90)
            let cameraOptions = self.navigationMapView.mapView.mapboxMap.camera(for: [coordinate2, coordinate, coordinate3], padding: UIEdgeInsets(top: 50, left: 50, bottom:50, right: 50), bearing: 0, pitch: 0)
            self.navigationMapView.mapView.camera.ease(to: cameraOptions, duration: Values.standardAnimationDuration, curve: .easeInOut, completion: nil)
        }
    }
    
    
    private func setZoomLevelAmenityInCarplay(location: CLLocationCoordinate2D, coordinates: [CLLocationCoordinate2D]) {
        
        guard let navMapView = appDelegate().routePlanningViewModel?.carPlayMapViewUpdatable?.navigationMapView else {return}
        
        var allCoordinates: [CLLocationCoordinate2D] = []
        allCoordinates.append(location)
        allCoordinates.append(contentsOf: coordinates)
        let padding = appDelegate().getVisiblePadding()
        let cameraOptions = navMapView.mapView.mapboxMap.camera(for: allCoordinates,
                                                                                padding: padding,
                                                                                bearing: 0,
                                                                                pitch: 0)
        navMapView.mapView.camera.ease(to: cameraOptions, duration: 0.4, curve: .easeInOut, completion: nil)
    }

    
    
}


extension CPRoutePlanningCarParkVC: AnnotationInteractionDelegate {
    func annotationManager(_ manager: AnnotationManager, didDetectTappedAnnotations annotations: [Annotation]) {
        
        // possible multiple tapped if there are overlapped together, only show the first one
        if let annotation = annotations.first {
            didSelectAnnotation(annotation: annotation, tapped: true)
        }
    }

    func didSelectAnnotation(annotation: Annotation, tapped: Bool = false) {

        DispatchQueue.main.async { [weak self] in
                        
            guard let self = self else { return }
            
            if let carpark = annotation.userInfo?["carpark"] as? Carpark {
            
                if carpark.id == self.currentSelectedCarpark?.id {
                    self.currentSelectedCarpark = nil
                    // click the same one, don't show callout
                    return
                }
                self.currentSelectedCarpark = carpark
                
                // set camera (actually should not set if not because of tapped
                if tapped {
                    _ = CameraOptions(center: CLLocationCoordinate2D(latitude: carpark.lat ?? 0, longitude: carpark.long ?? 0), zoom: self.navigationMapView.mapView.mapboxMap.cameraState.zoom) // using current zoom level
//                    self.navigationMapView.mapView.camera.ease(to: cameraOptions, duration: Values.standardAnimationDuration, curve: .easeInOut, completion: nil)
                }
                
               
            }
        }
    }

}

extension CPRoutePlanningCarParkVC:CPMapTemplateDelegate{
    
    func mapTemplate(_ mapTemplate: CPMapTemplate, selectedPreviewFor trip: CPTrip, using routeChoice: CPRouteChoice) {
        
        let matchedCarPark = self.filteredCarParks.filter ({ $0.name == trip.destination.placemark.name})
        
        if(matchedCarPark.count > 0)
        {
            self.navigationMapView.mapView.removeCarparkLayer()

            updateSelectedCarpark(matchedCarPark.first, needUpdateZoomLevel: true)

        }
    }
    
    func mapTemplate(_ mapTemplate: CPMapTemplate, startedTrip trip: CPTrip, using routeChoice: CPRouteChoice) {
                
        let matchedCarPark = self.filteredCarParks.filter ({ $0.name == trip.destination.placemark.name})
        
        if let carpark = matchedCarPark.first {
            let location = CLLocation(latitude: carpark.lat ?? 0, longitude: carpark.long ?? 0)
            getAddressFromLocation(location: location) { address in
                
                let address = SearchAddresses(alias: "", address1: carpark.name ?? "", lat: "\(carpark.lat ?? 0)", long: "\(carpark.long ?? 0)", address2: address, distance: "\(carpark.distance ?? 0)")
                
                self.hideCarparks()
                
                if let templates = appDelegate().carPlayManager.interfaceController?.templates{
                    if(templates.count > 1){
                        
                        if #available(iOS 14, *) {
                            
                            self.carPlayManager.interfaceController?.popToRootTemplate(animated: true, completion: { value, error in
                                if(!value){
                                    SwiftyBeaver.debug("CarPlay CarPark Pop to root template failed", "\(value)","\(String(describing: error))")
                                }
                                
                            })
                        }
                        else{
                            self.carPlayManager.interfaceController?.popToRootTemplate(animated: true)
                        }
                    }
                }
                
                if let _ = (self.carPlayManager.carWindow?.rootViewController as? CarPlayMapViewController)?.traitCollection,
                   let _ = self.carPlayManager.interfaceController,
                   let carRPController = appDelegate().carRPController {
                    carRPController.clearPreviousRouteResponse()
                    carRPController.address = address
                    carRPController.getRoutes()
                }
            }
        }
    }
}

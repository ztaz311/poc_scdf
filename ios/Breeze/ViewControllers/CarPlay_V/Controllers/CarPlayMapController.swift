//
//  CarPlayMapController.swift
//  Breeze
//
//  This class is used for CarPlay in its whole life cycle
//
//  Created by Zhou Hao on 30/9/21.
//

import Foundation
import MapboxDirections
import Turf
import MapboxCoreNavigation
@_spi(Restricted) import MapboxMaps
import MapboxCoreMaps
import MapboxNavigation
import Combine
import SwiftyBeaver
import CarPlay

// I'm using this one for both MapLanding and Navigation
final class CarPlayMapController {
    private weak var mapLandingViewModel: MapLandingViewModel?
    private weak var cruiseViewModel: CruiseViewModel?
    private weak var navigationViewModel: TurnByTurnNavigationViewModel?
    private weak var navigationMapView: NavigationMapView!
    private var disposables = Set<AnyCancellable>()
    private weak var carPlayManager: CarPlayManager!
    private var navigationStarted = false
    
    init(navigationMapView: NavigationMapView, carPlayManager: CarPlayManager) {
        
        AnalyticsManager.shared.logScreenView(screenName: ParameterName.CarplayHomepage.screen_view)
        SwiftyBeaver.debug("CarPlayMapController init")
        
        self.navigationMapView = navigationMapView
        self.carPlayManager = carPlayManager
        NotificationCenter.default.post(name: NSNotification.Name("OBUConnectedNotification"), object: nil)
        self.navigationMapView.mapView.ornaments.options.attributionButton.visibility = .hidden
        
        // To make sure the navigation will start
        if appDelegate().navigationViewModel == nil {
            appDelegate().enterHome()
        }
        
        appDelegate().$mapLandingViewModel.sink(receiveValue: { [weak self] mapLandingViewModel in
            guard let self = self else { return }
            self.mapLandingViewModel = mapLandingViewModel
            if self.mapLandingViewModel != nil {
                self.updateStateConnectOBU()
                self.setupSpeedLimit()
                
                self.mapLandingViewModel?.startUpdating()
                // map loaded already
                let carPlayMapViewUpdatable = CarPlayMapViewUpdatable(navigationMapView: navigationMapView)
                carPlayMapViewUpdatable.delegate = self
                self.mapLandingViewModel?.carPlayMapViewUpdatable = carPlayMapViewUpdatable
            }
        }).store(in: &disposables)
                
        appDelegate().$cruiseViewModel
            .receive(on: DispatchQueue.main)
            .sink { [weak self] viewModel in
                guard let self = self else { return }
                
                if viewModel != nil {
                    let carplayMapViewUpdatable = CruiseCarPlayMapUpdatable(navigationMapView: navigationMapView)
                    viewModel!.carPlayMapViewUpdatable = carplayMapViewUpdatable
                    appDelegate().carPlayMapController?.updateCPToggleButtons(activity: .navigating)
                    self.cruiseStatusChanged(isCruise: true)
                } else {
                    self.cruiseStatusChanged(isCruise: false)
                    self.updateCPToggleButtons(activity: .browsing)
                }
                self.cruiseViewModel = viewModel
                
        }.store(in: &self.disposables)
        
        appDelegate().$navigationViewModel
            .receive(on: DispatchQueue.main)
            .sink { [weak self] viewModel in
                guard let self = self else { return }
                self.navigationViewModel = viewModel
                if viewModel != nil {
                    //self.updateCPToggleButtons(activity: .navigating)
                    self.startNavigation(from: viewModel!.fromDeviceType)
                } else {
                    self.stopNavigation()
                }
        }.store(in: &self.disposables)
        
        //  appDelegate().$isObuLiteDisplay
        //            .receive(on: DispatchQueue.main)
        //            .sink { [weak self] isObuLite in
        //                if let isOBU = isObuLite {
        //                    if isOBU {
        //                        appDelegate().showObuLite()
        //                    }else {
        //                        appDelegate().tryToDismissTemplateIfNeed()
        //                    }
        //                }
        //            }.store(in: &self.disposables)
        
        appDelegate().$needShowMinimumCashcardBalance
            .receive(on: DispatchQueue.main)
            .sink { [weak self] isNeeded in
                if isNeeded == true {
                    self?.handleShowMinimumCardBalance()
                }
            }.store(in: &self.disposables)
    }
        
    private func startNavigation(from deviceType: DeviceType) {
        if let vm = self.navigationViewModel {
            if let location = vm.navigationService.router.location {
                if deviceType != .carPlay { // no need to start twice
                    SwiftyBeaver.debug("CarPlayMapController startNavigation")
                    carPlayManager.beginNavigationWithCarPlay(using: location.coordinate,
                                                              navigationService: vm.navigationService)
                }
                // IMPORTANT: After start, the navigateMapView will be valid then!!!
                // And this navigationMapView is different from the main navigationMapView
                
                //Crash to be fixed - Some times CarPlayNavigationController is nil
                if let navController = self.carPlayManager.carPlayNavigationViewController{
                    navController.showsContinuousAlternatives = false
                    navController.annotatesIntersectionsAlongRoute = false
                    //We should initialise CarPlayNavigationMapUPdatable class only after Map is loaded
                    navController.navigationMapView?.mapView.mapboxMap.onNext(event: .mapLoaded) { _ in
                        
                        vm.carPlayMapViewUpdatable = CarPlayNavigationMapViewUpdatable(navigationMapView: navController.navigationMapView!)
                        self.navigationStarted = true
                    }
                }
                else{
                    
                    navigationStarted = true
                    carPlayManager.beginNavigationWithCarPlay(using: location.coordinate,
                                                              navigationService: vm.navigationService)
                }
                
                
            }
        }
    }
    
    // Stopped from the phone
    private func stopNavigation() {
        if navigationStarted {
            SwiftyBeaver.debug("CarPlayMapController stopNavigation")
            /// The original code always using ``false
            carPlayManager.carPlayNavigationViewController?.exitNavigation(byCanceling: false)
            self.updateCPToggleButtons(activity: .browsing)
            navigationStarted = false
        }
    }
    
    
    
    // MARK: OBU CONNECT
    func updateStateConnectOBU() {
        if OBUHelper.shared.stateOBU == .CONNECTED {
            obuConnected()
        } else if !OBUHelper.shared.isObuConnected() && !OBUHelper.shared.obuName.isEmpty {
            obuNotConnected()
        } else {
            // Do nothing
        }
    }
    
    func obuConnected() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) { [weak self] in
            if let mapView = self?.navigationMapView?.mapView {
                showToast(from: mapView, message: "OBU Connected", topOffset: 5, fromCarPlay: false)
            }
        }
    }
    
    func handleShowMinimumCardBalance() {
        
        DispatchQueue.main.async { [weak self] in
            if let mapView = self?.navigationMapView?.mapView {
                showToast(from: mapView, message: "Cashcard value is low. \nPlease top up soon.", topOffset: 20, fromCarPlay: false)
            }
        }
    }
    
    func obuNotConnected() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) { [weak self] in
            if let mapView = self?.navigationMapView?.mapView {
                showToast(from: mapView, message: "OBU not connected", topOffset: 5, fromCarPlay: false)
            }
        }
    }
    
    
    func startCruise(){
        appDelegate().hideCarParks()
        self.mapLandingViewModel?.startCruise()
    }
    
    func stopCruise(){
        
        self.cruiseViewModel?.stopLocationUpdate()
        self.cruiseViewModel?.endCruiseStatus()
        appDelegate().enterHome()
        
    }
        
    // MARK: functions related to cruise share drive
    func carplayShareDrive(isCruise: Bool) {
        // 1. Get recent contacts from BE
        retrieveRecentContacts { contacts in
            if contacts.count == 0 {
                // 2. If no contacts, show a message screen
                appDelegate().showNoContacts()
            } else {
                // 3. If has some contacts, show contact list
                // TODO: Consider to do refactor to move this to CarPlayMapController. Currently code is in AppDelegate and CarPlayMapController. It's not very easy to organize.
                appDelegate().showRecentContactList(contacts: contacts, isCruise: isCruise)
            }
        }
    }
    
    private func retrieveRecentContacts(_ completion: @escaping ([RecentContact]) -> Void) {
        RecentContactsService().getRecentContacts { result in
            switch result {
            case .success(let contacts):
                completion(contacts)
            case .failure(let error ):
                SwiftyBeaver.error("Failed to retrieveRecentContacts: \(error.localizedDescription)")
                completion([])
            }
        }
    }
        
    func updateCPToggleButtons(activity: CarPlayActivity) {
        guard let carplayMapVC = self.carPlayManager.carWindow?.rootViewController as? CarPlayMapViewController else { return }
        
        let traitCollection = carplayMapVC.traitCollection
        if let template = carPlayManager.interfaceController?.rootTemplate as? CPMapTemplate {
            template.leadingNavigationBarButtons = []
            template.trailingNavigationBarButtons = []
            
            if let leadingButtons =  self.carPlayManager.delegate?.carPlayManager(carPlayManager, leadingNavigationBarButtonsCompatibleWith: traitCollection, in: template, for: activity) {
                template.leadingNavigationBarButtons = leadingButtons
            }
            
            if let trailingButtons =  self.carPlayManager.delegate?.carPlayManager(carPlayManager, trailingNavigationBarButtonsCompatibleWith: traitCollection, in: template, for: activity) {
                template.trailingNavigationBarButtons = trailingButtons
            }
        }
    }
    
    func showAndHideETAButton(enable:Bool = false){
        if let template = carPlayManager.interfaceController?.rootTemplate as? CPMapTemplate {
            for button in template.leadingNavigationBarButtons{
                
                if(button.title == Constants.cpETAPause){
                    template.leadingNavigationBarButtons.remove(at: 1)
                }
            }
        }
    }
        
    /// Updat cruise mode top bar button title
    /// isEnabled - toggled enabled or not
    /// isLiveLoc - is for live location enabled or not
    func updateCPCruiseToggleTitle(activity:CarPlayActivity,isEnabled:Bool, isLiveLoc:Bool = false){
        
        if(activity == .navigating) {
            
            if let template = carPlayManager.interfaceController?.rootTemplate as? CPMapTemplate {
                // for leading button get mute/unmuted button
                if let button = template.leadingNavigationBarButtons.first {
                    if !isLiveLoc {
                        if let traitCollection = appDelegate().carPlayManager.carPlayMapViewController?.traitCollection {
                            //  isEnable == true mean isMute = true
                            if(isEnabled == true) {
                                button.image = UIImage(named: "carplayMutedIcon", in: nil, compatibleWith: traitCollection)
                            } else {
                                button.image = UIImage(named: "carplayUnmutedIcon", in: nil, compatibleWith: traitCollection)
                            }
                        }
                    }
                }
                
                // for trailing buttons
//                for button in template.trailingNavigationBarButtons {
//                    if button.title == Constants.cpShareDrive {
//                        button.title = isLiveLoc ? Constants.cpETAPause : Constants.cpShareDrive
//                    } else if button.title == Constants.cpETAPause || button.title == Constants.cpETAResume {
//                        if isLiveLoc {
//                            button.title = isEnabled ? Constants.cpETAPause : Constants.cpETAResume
//                        }
//                    }
//                }
            }
            
        } else if activity == .previewing {
            // for share drive button
            if let template = carPlayManager.interfaceController?.topTemplate as? CPMapTemplate {
                for button in template.trailingNavigationBarButtons {
                    if button.title == Constants.cpShareDrive || button.title == Constants.cpCancelShareDrive {
                        button.title = isEnabled ? Constants.cpCancelShareDrive : Constants.cpShareDrive
                    }
                }
            }
        }
    }
        
    private func setupSpeedLimit() {
        self.carPlayManager.carPlayMapViewController?.speedLimitView.isAlwaysHidden = true
        self.carPlayManager.carPlayMapViewController?.speedLimitView.regulatoryBorderColor = .gray
        self.carPlayManager.carPlayMapViewController?.wayNameView.isHidden = true
    }
    
    private func cruiseStatusChanged(isCruise: Bool) {
        self.carPlayManager.carPlayMapViewController?.speedLimitView.isAlwaysHidden = !isCruise
        self.carPlayManager.navigationMapView?.mapView.setUpCruiseCustomUserLocationIcon(isStarted: isCruise)
    }

    deinit {
        SwiftyBeaver.debug("CarPlayMapController deinit")
        self.mapLandingViewModel?.carPlayMapViewUpdatable = nil
        disposables.removeAll()
    }
    
}

extension CarPlayMapController: MapViewUpdatableDelegate {
    func onParkingEnabled(_ value: Bool) {
        //Not required
    }
    
    func onLiveLocationEnabled(_ value: Bool) {
        //Not required
    }
    func onMuteEnabled(_ value: Bool) {
        //Not required
    }
    
    func onShareDriveEnabled(_ value: Bool) {
        //Not required
    }
    
    func onSearchAction() {
        //Not required
    }
    
    func onEndAction() {
        //Not required
    }
    
    func onEvChargerAction(_ value: Bool) {
        //Not required
    }
    
    func onPetrolAction(_ value: Bool) {
        //Not required
    }
    func onParkingAction() {
        //Not required
    }
    
}

//extension CarPlayMapLandingController: MapLandingViewModelDelegate {
//    func cruiseStatusChanged(isCruise: Bool) {
//        self.carPlayManager.carPlayMapViewController?.speedLimitView.isAlwaysHidden = !isCruise
//    }
//}

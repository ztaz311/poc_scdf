//
//  CPAmenityVC.swift
//  Breeze
//
//  Created by Tuan, Pham Hai on 01/12/2023.
//

import Foundation
import MapboxDirections
import Turf
import MapboxCoreNavigation
@_spi(Restricted) import MapboxMaps
import MapboxCoreMaps
import MapboxNavigation
import Combine
import SwiftyBeaver
import CarPlay

class CPAmenityVC: NSObject {
    
    private weak var navigationMapView: NavigationMapView?
    private weak var carPlayManager: CarPlayManager?
    
    var lastLocation: CLLocation?
    var features: [Turf.Feature] = []
    var selectedAmenity: Turf.Feature?
    var amenityCoordinates: [CLLocationCoordinate2D] = []
    var currentAmenityID: String = ""
    var selectedIndex: Int = -1
    
    private var trips = [CPTrip]()
    var response: RouteResponse?
    var waypoints: [Waypoint] = [] {
        didSet {
            waypoints.forEach {
                $0.coordinateAccuracy = -1
            }
        }
    }
    
    private let types = [
                         Values.SHARE_EV_ICON,
                         Values.SHARE_PETROL_ICON,
                         Values.SHARE_PETROL_BOOKMARKED,
                         Values.SHARE_EV_BOOKMARKED
    ]
    
    typealias RouteRequestSuccess = ((RouteResponse) -> Void)
    typealias RouteRequestFailure = ((Error) -> Void)
    
    init(navigationMapView: NavigationMapView, carPlayManager: CarPlayManager) {
        SwiftyBeaver.debug("CPAmenityVC init")
        self.navigationMapView = navigationMapView
        self.carPlayManager = carPlayManager
        self.navigationMapView?.mapView.ornaments.options.attributionButton.visibility = .hidden
    }
    
    private func addImageAssetsOnMap() {
        //  add all Petrol/EV icons on map
        navigationMapView?.mapView.addBookmarkImagesToMapStyle(types: types)
    }
    
    private func removeAllLayers() {
        for type in self.types {
            if let mapView = navigationMapView?.mapView {
                mapView.removeAmenitiesLayerOfType(type: type)
            }
        }
    }
    
//    private func updateNavigationButton(_ isShowingAmenity: Bool) {
//        if isShowingAmenity {
//            DispatchQueue.main.async {
//                if let mapTemplate = self.carPlayManager?.interfaceController?.rootTemplate as? CPMapTemplate {
//                    mapTemplate.trailingNavigationBarButtons = []
//                    
//                    let backBtn = CPBarButton(type: .text) { [weak self] (barButton) in
//                        
//                        if self?.currentAmenityID == Values.PETROL {
//                            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CarplaySearchPage.UserClick.nearby_petrol_back, screenName: ParameterName.CarplaySearchPage.screen_view)
//                        } else {
//                            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CarplaySearchPage.UserClick.nearby_ev_back, screenName: ParameterName.CarplaySearchPage.screen_view)
//                        }
//                        self?.updateNavigationButton(false)
//                    }
//                    backBtn.title = "Back"
//                    mapTemplate.leadingNavigationBarButtons = [backBtn]
//                }
//            }
//        } else {
//            
//            self.hideAmenities()
//            
//            if let mapTemplate = self.carPlayManager?.interfaceController?.rootTemplate as? CPMapTemplate, let traitCollection = self.carPlayManager?.carPlayMapViewController?.traitCollection, let interface = self.carPlayManager?.interfaceController {
//                mapTemplate.leadingNavigationBarButtons = appDelegate().cpBarBrowseButtons.createLeadingNavigationButtons(interfaceController: interface, traitCollection: traitCollection)
//                mapTemplate.trailingNavigationBarButtons = appDelegate().cpBarBrowseButtons.createTrailingNavigationButtons(interfaceController: interface, traitCollection: traitCollection)
//            }
//        }
//    }
    
    func loadAmenityNearby(_ amenity: String) {
        
        self.currentAmenityID = amenity
        self.selectedIndex = -1
        
        //  Add image assets on map
        self.addImageAssetsOnMap()
        
        //  Remove other amenity
        if amenity == Values.PETROL {
            navigationMapView?.mapView.removeAmenitiesPetrolAndEVLayer(type: Values.SHARE_EV_ICON)
            navigationMapView?.mapView.removeAmenitiesPetrolAndEVLayer(type: Values.SHARE_EV_BOOKMARKED)
        } else if amenity == Values.EV_CHARGER {
            navigationMapView?.mapView.removeAmenitiesPetrolAndEVLayer(type: Values.SHARE_PETROL_ICON)
            navigationMapView?.mapView.removeAmenitiesPetrolAndEVLayer(type: Values.SHARE_PETROL_BOOKMARKED)
        }
        
        lastLocation = self.navigationMapView?.mapView.location.latestLocation?.location
        if let lastLocation = self.lastLocation {
            let lat = lastLocation.coordinate.latitude
            let long = lastLocation.coordinate.longitude
            let location2D =  CLLocationCoordinate2D(latitude: lat, longitude: long)
            AmenitiesSharedInstance.shared.addRequestForSelectedAmenityInCarplay(location: location2D, selectedAmenity: amenity) {[weak self] success in
                DispatchQueue.main.async { [weak self] in
                    self?.updateMapWithAmenitiesForEVandPetrol(selectedID: amenity)
                }
            }
        }
    }
    
    private func showNoData(_ amenityId: String) {
        if amenityId == Values.PETROL {
            appDelegate().showCommonToastWithMessage("No petrol stations nearby")
        } else if amenityId == Values.EV_CHARGER {
            appDelegate().showCommonToastWithMessage("No EV charging points nearby")
        }
    }
    
    private func updateMapWithAmenitiesForEVandPetrol(selectedID: String = "", isUpdateSelected: Bool = false) {
        
        let allKeys = AmenitiesSharedInstance.shared.carplaySymbolLayer.keys
        amenityCoordinates.removeAll()
        features.removeAll()
         
        if allKeys.isEmpty {
            //  Handle no data when user select Petrol/EV
            showNoData(selectedID)
            return
        }
        
        var needShowNodata = true
        
        for key in allKeys {
            if key == selectedID {
                let array = AmenitiesSharedInstance.shared.carplaySymbolLayer[key] as! [Any]
                
                var turfArray = [Turf.Feature]()
                print(array)
                var type = key
                for item in array {
                    
                    let itemDict = item as! [String:Any]
                    if key == Values.PETROL {
                        let isBookmarked = itemDict["isBookmarked"] as? Bool ?? false
                        if isBookmarked {
                            type = Values.SHARE_PETROL_BOOKMARKED
                        } else {
                            type = Values.SHARE_PETROL_ICON
                        }
                    } else if key == Values.EV_CHARGER {
                        let isBookmarked = itemDict["isBookmarked"] as? Bool ?? false
                        if isBookmarked {
                            type = Values.SHARE_EV_BOOKMARKED
                        } else {
                            type = Values.SHARE_EV_ICON
                        }
                    }
                                    
                    let feature = self.getAmenitiesFeatures(location: itemDict, type: key, selectedID: selectedID)
                    turfArray.append(feature)
                }
                
                let amenityFeatureCollection = FeatureCollection(features: turfArray)
                
                self.updateAmenitiesOnMap(collection: amenityFeatureCollection,type: type)
                
                //  Handle show there is no data for parking/ev/petrol
                if turfArray.isEmpty {
                    needShowNodata = true
                } else {
                    
                    //  Keep all feature data for petrol / EV
                    self.features.append(contentsOf: turfArray)
                    
                    //  Only run for the first time
                    if !isUpdateSelected {
                        
                        //  Try to fetch waypoit to the first destination
                        if let properties = self.features.first?.properties, case let .point(point) = self.features.first?.geometry {
                            if case let .string(name) = properties["name"] {
                                let waypoint = Waypoint(coordinate: point.coordinates, name: name)
                                waypoint.targetCoordinate = point.coordinates
                                self.waypoints.append(waypoint)
                                self.requestRoute()
                            }
                        }
                    }
                    
                    if let location = self.lastLocation {
                        let currentLocation = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                            self.setZoomLevelAmenityInCarplay(location: currentLocation, coordinates: self.amenityCoordinates)
                        }
                    }
                    
                    needShowNodata = false
                }
            }
        }
        
        if needShowNodata {
            showNoData(selectedID)
            self.hideAmenities()
        }
    }
    
    private func getAmenitiesFeatures(location:[String:Any],type:String, selectedID: String = "")-> Turf.Feature {
        
        let coordinate = CLLocationCoordinate2D(latitude: location["lat"] as! Double, longitude: location["long"] as! Double)
        amenityCoordinates.append(coordinate)
        
        let name = location["name"] as! String
        let address = location["address"] as! String
        let id = location["id"] as! String
        let poiID = location["poiID"] as? String ?? ""
        let startDate = location["startDate"] as! Double
        let endDate = location["endDate"] as! Double
        let clusterID = location["clusterID"] as! Int
        let clusterColor = location["clusterColor"] as! String
        let originalType = location["originalType"] as! String
        let textAdditionalInfo = location["textAdditionalInfo"] as? String ?? ""
        let styleLightColor = location["styleLightColor"] as? String ?? ""
        let styleDarkColor = location["styleDarkColor"] as? String ?? ""
        let isBookmarked = location["isBookmarked"] as? Bool ?? false
        let bookmarkId = location["bookmarkId"] as? Int ?? -1
        let typeDisplay = location["typeDisplay"] as? String ?? ""
        let providerName = location["providerName"] as? String ?? ""
        
        //  Handle selected state
        var isSelected: Bool = false
        if let properties = self.selectedAmenity?.properties,
            case let .string(selectedName) = properties["name"],
            case let .string(selectedId) = properties["id"],
            case let .string(selectedAddress) = properties["address"] {
            if selectedName == name && selectedId == id && selectedAddress == address {
                isSelected = true
            }
        }
        
        var feature = Turf.Feature(geometry: .point(Point(coordinate)))
        feature.properties = [
            "id":.string(id),
            "type":.string(type),
            "name":.string(name),
            "address":.string(address),
            "startDate": .number(startDate),
            "endDate": .number(endDate),
            "isSelected": .boolean(isSelected),
            "clusterID":.string("\(clusterID)"),
            "clusterColor":.string(clusterColor),
            "bookmarkId": .number(Double(bookmarkId)),
            "isBookmarked": .boolean(isBookmarked),
            "poiID": .string(poiID),
            "textAdditionalInfo": .string(textAdditionalInfo),
            "styleLightColor": .string(styleLightColor),
            "styleDarkColor": .string(styleDarkColor),
            "originalType":.string(originalType),
            "typeDisplay": .string(typeDisplay),
            "providerName": .string(providerName)
        ]
        return feature
    }
    
    private func updateAmenitiesOnMap(collection:Turf.FeatureCollection, type:String){
        navigationMapView?.mapView.removeAmenitiesPetrolAndEVLayer(type: "\(type)")
        navigationMapView?.mapView.addAmenityOnCarplay(features: collection, type: type, isAbovePuck: false)
    }
    
    private func setZoomLevelAmenityInCarplay(location: CLLocationCoordinate2D, coordinates: [CLLocationCoordinate2D]) {
        self.navigationMapView?.navigationCamera.stop()
        
        var allCoordinates: [CLLocationCoordinate2D] = []
        allCoordinates.append(location)
        allCoordinates.append(contentsOf: coordinates)
        
        let padding = appDelegate().getVisiblePadding()
        if let cameraOptions = self.navigationMapView?.mapView.mapboxMap.camera(for: allCoordinates,
                                                                                padding: padding,
                                                                                bearing: 0,
                                                                                pitch: 0), let mapView = self.navigationMapView?.mapView {
            mapView.camera.ease(to: cameraOptions, duration: 0.4, curve: .easeInOut, completion: nil)
        }
    }
    
    fileprivate lazy var defaultSuccess: RouteRequestSuccess = { [weak self] (response) in
        
        guard let routes = response.routes, !routes.isEmpty, case let .route(options) = response.options else { return }
        guard let self = self else { return }
        
        //  Remove existing waypoint
        self.navigationMapView?.removeWaypoints()
        self.response = response
        
        //  Change waypoint to new one
        if let waypoints = response.waypoints {
            self.waypoints = waypoints
        }
        
        let info = CPTrip.cpGetRouteChoice(routeResponse: response)
        guard let coordinate = self.navigationMapView?.mapView.location.latestLocation?.coordinate else {
            print("User location is not valid. Make sure to enable Location Services.")
            return
        }
        let currentLocMapItem = MKMapItem(placemark: MKPlacemark(coordinate: coordinate))
                
        for feature in self.features {
            if let properties = feature.properties, case let .point(point) = feature.geometry {

                if case let .string(name) = properties["name"],
                   case let .string(address) = properties["address"] {
                    
                    let amenityMapMapItem = MKMapItem(placemark: MKPlacemark(coordinate: point.coordinates, addressDictionary: ["address": address]))
                    amenityMapMapItem.name = name
                    
                    var detailAmenity = "\(address)"
                    if self.currentAmenityID == Values.EV_CHARGER {
                        if case let .string(typeDisplay) = properties["typeDisplay"] {
                            detailAmenity = "\(address)\n\n\(typeDisplay)"
                        }
                    }
                    /*
                    else if self.currentAmenityID == Values.PETROL {
                        if case let .string(providerName) = properties["providerName"] {
                            detailAmenity = "\(address)\n\n\(providerName)"
                        }
                    }
                     */
                    
                    let routeChoice = CPRouteChoice(summaryVariants: [name],
                                                    additionalInformationVariants: [detailAmenity],
                                                    selectionSummaryVariants: [])
                    routeChoice.userInfo = info
                    let trip = CPTrip.init(origin: currentLocMapItem, destination: amenityMapMapItem, routeChoices: [routeChoice])
                    self.trips.append(trip)
                }
            }
        }
        
        if !self.trips.isEmpty {
//            self.updateNavigationButton(true)
            self.showAmenityList()
        } else {
            self.hideAmenities()
        }
    }
    
    fileprivate lazy var defaultFailure: RouteRequestFailure = { [weak self] (error) in
        guard let self = self else { return }
        // Clear routes from the map
        self.response = nil
    }
    
    func requestRoute() {
        guard waypoints.count > 0 else { return }
        guard let coordinate = navigationMapView?.mapView.location.latestLocation?.coordinate else {
            print("User location is not valid. Make sure to enable Location Services.")
            return
        }

        let location = CLLocation(latitude: coordinate.latitude,
                                  longitude: coordinate.longitude)
        let userWaypoint = Waypoint(location: location)
        waypoints.insert(userWaypoint, at: 0)

        let navigationRouteOptions = NavigationRouteOptions(waypoints: waypoints)
        navigationRouteOptions.locale = Locale.enSGLocale()

        requestRoute(with: navigationRouteOptions, success: defaultSuccess, failure: defaultFailure)
    }
    
    func requestRoute(with options: RouteOptions, success: @escaping RouteRequestSuccess, failure: RouteRequestFailure?) {
        NavigationSettings.shared.directions.calculateWithCache(options: options) { (session, result) in
            switch result {
            case let .success(response):
                success(response)
            case let .failure(error):
                failure?(error)
            }
        }
    }
    
    func showAmenityList() {
        DispatchQueue.main.async {
            self.pushToNewMapTemplateWithTrip(self.trips)
            
        }
    }

    private func defaultTripPreviewTextConfiguration() -> CPTripPreviewTextConfiguration {

        let goTitle = "Navigate here"

        let alternativeRoutesTitle = "Other routes"

        let overviewTitle = "Overview"

        let defaultPreviewText = CPTripPreviewTextConfiguration(startButtonTitle: goTitle,
                                                                additionalRoutesButtonTitle: alternativeRoutesTitle,
                                                                overviewButtonTitle: overviewTitle)
        return defaultPreviewText
    }
    
    /*
    private func addAmenityAnnotation(feature: Turf.Feature) -> PointAnnotation? {

        if let properties = feature.properties, case let .point(point) = feature.geometry {
            
            if case let .string(name) = properties["name"],
               case let .string(address) = properties["address"],
               case let .string(id) = properties["id"] {
                
                var annotation = PointAnnotation(id: id, coordinate: point.coordinates)
                
                //annotation.image = //getAnnotationImage(carpark: carpark, selected: false, ignoreDestination: true)
                annotation.userInfo = [
                    "feature": feature
                ]

                if let layerId = navigationMapView?.pointAnnotationManager?.layerId, !layerId.isEmpty {
                    try? self.navigationMapView?.mapView.mapboxMap.style.setLayerProperty(for: layerId, property: "icon-allow-overlap", value: true)
                }
                return annotation
            }
        }
        
        return nil
    }
     */
    
    func hideAmenities() {

        //  Remove all carpark and Petrol/EV
        self.navigationMapView?.mapView.removeCarparkLayer()
        self.navigationMapView?.mapView.removePetrolAndEVLayers()
        
        //  Reset anotation
        let annotations = [PointAnnotation]()
        self.navigationMapView?.pointAnnotationManager?.annotations = annotations

        //  Hide preview
        if let mapTemplate = self.carPlayManager?.interfaceController?.rootTemplate as? CPMapTemplate {
            mapTemplate.hideTripPreviews()
        }
        self.carPlayManager?.navigationMapView?.navigationCamera.follow()
    }
    
    
    func pushToNewMapTemplateWithTrip(_ trips: [CPTrip]) {
        
        let newMapTemplate = CPMapTemplate()
        newMapTemplate.trailingNavigationBarButtons = []
        newMapTemplate.leadingNavigationBarButtons = []
        newMapTemplate.mapDelegate = self
        
        let backBtn = CPBarButton(type: .text) { (barButton) in
            newMapTemplate.hideTripPreviews()
            
            if self.currentAmenityID == Values.PETROL {
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CarplaySearchPage.UserClick.nearby_petrol_back, screenName: ParameterName.CarplaySearchPage.screen_view)
            } else {
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CarplaySearchPage.UserClick.nearby_ev_back, screenName: ParameterName.CarplaySearchPage.screen_view)
            }
            
            self.hideAmenities()
            self.carPlayManager?.interfaceController?.popTemplate(animated: true)
        }
        backBtn.title = "Back"
        newMapTemplate.backButton = backBtn
        
        self.carPlayManager?.interfaceController?.pushTemplate(newMapTemplate, animated: true)
        newMapTemplate.showTripPreviews(trips, textConfiguration: self.defaultTripPreviewTextConfiguration())
    }
    
}

extension CPAmenityVC: CPMapTemplateDelegate {

    func mapTemplate(_ mapTemplate: CPMapTemplate, selectedPreviewFor trip: CPTrip, using routeChoice: CPRouteChoice) {
        
        if let index = self.features.firstIndex(where: { feature in
            if let properties = feature.properties {
                if case let .string(name) = properties["name"], case let .string(address) = properties["address"], let tripAddress = routeChoice.additionalInformationVariants?.first as? String {
                    return (name == trip.destination.placemark.name && tripAddress.hasPrefix(address))
                }
            }
            return false
        }) {
            
            if self.currentAmenityID == Values.PETROL {
                if selectedIndex == -1 || (selectedIndex < index) {
                    AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CarplaySearchPage.UserClick.nearby_petrol_right_arrow, screenName: ParameterName.CarplaySearchPage.screen_view)
                } else {
                    AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CarplaySearchPage.UserClick.nearby_petrol_left_arrow, screenName: ParameterName.CarplaySearchPage.screen_view)
                }
                
            } else if self.currentAmenityID == Values.EV_CHARGER {
                if selectedIndex == -1 || (selectedIndex < index) {
                    AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CarplaySearchPage.UserClick.nearby_ev_right_arrow, screenName: ParameterName.CarplaySearchPage.screen_view)
                } else {
                    AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CarplaySearchPage.UserClick.nearby_ev_left_arrow, screenName: ParameterName.CarplaySearchPage.screen_view)
                }
            }
            
            selectedIndex = index
        }
        
        //  Find the selected feature by name
        let matchedAmenities = self.features.filter { feature in
            if let properties = feature.properties {
                if case let .string(name) = properties["name"], case let .string(address) = properties["address"], let tripAddress = routeChoice.additionalInformationVariants?.first as? String {
                    return (name == trip.destination.placemark.name && tripAddress.hasPrefix(address))
                }
            }
            return false
        }
        
        guard let coordinate = self.navigationMapView?.mapView.location.latestLocation?.coordinate else {
            print("User location is not valid. Make sure to enable Location Services.")
            return
        }
        if !matchedAmenities.isEmpty {
            //  Remove all layers
            self.navigationMapView?.mapView.removeCarparkLayer()
//            self.removeAllLayers()
            
            //  Create new anotation
//            var annotations = [PointAnnotation]()
//            self.navigationMapView?.pointAnnotationManager?.annotations = annotations

            //  TODO Add feature on map
//            self.navigationMapView?.mapView.addAmenityOnCarplay(features: <#T##FeatureCollection#>, type: <#T##String#>, isAbovePuck: <#T##Bool#>)
            
//            self.navigationMapView?.mapView.addCarparks(matchedCarPark, showOnlyWithCostSymbol: true)
            
            if let feature = matchedAmenities.first, case let .point(point) = feature.geometry {
                //  Add amenity anotation to map
//                if let annotation = self.addAmenityAnnotation(feature: feature) {
//                    annotations.append(annotation)
//                }
//                self.navigationMapView?.pointAnnotationManager?.annotations = annotations
                
                //  Select amenity
                self.selectedAmenity = feature
                
                //  Handle selected state
                self.updateMapWithAmenitiesForEVandPetrol(selectedID: self.currentAmenityID, isUpdateSelected: true)
            }

        }
    }

    func mapTemplate(_ mapTemplate: CPMapTemplate, startedTrip trip: CPTrip, using routeChoice: CPRouteChoice) {

//        self.updateNavigationButton(false)
        
        if self.currentAmenityID == Values.PETROL {
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CarplaySearchPage.UserClick.nearby_petrol_navigate_here, screenName: ParameterName.CarplaySearchPage.screen_view)
        } else if self.currentAmenityID == Values.EV_CHARGER {
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CarplaySearchPage.UserClick.nearby_ev_navigate_here, screenName: ParameterName.CarplaySearchPage.screen_view)
        }
        
        //  Find the selected feature by name
        let matchedAmenities = self.features.filter { feature in
            if let properties = feature.properties {
                if case let .string(name) = properties["name"], case let .string(address) = properties["address"], let tripAddress = routeChoice.additionalInformationVariants?.first as? String {
                    return (name == trip.destination.placemark.name && tripAddress.hasPrefix(address))
                }
            }
            return false
        }
        
        if let feature = matchedAmenities.first, case let .point(point) = feature.geometry, case let .string(name) = feature.properties?["name"] {
            
            getAddressFromLocation(location: CLLocation(latitude: point.coordinates.latitude, longitude: point.coordinates.longitude)) { address in
                let address = SearchAddresses(alias: "", address1: name, lat: "\(point.coordinates.latitude)", long: "\(point.coordinates.longitude)", address2: address, distance: "0")
                self.hideAmenities()

                let appDelegate = appDelegate()
                
                if let carPlayManager = self.carPlayManager,
                   let navigationMapView = self.carPlayManager?.navigationMapView {
                    appDelegate.carRPController = CarPlayRPController(navigationMapView: navigationMapView,
                                                                      carPlayManager: carPlayManager, address: address)
                    appDelegate.carRPController?.getRoutes()
                }
            }
        }
    }
}

extension CPAmenityVC: AnnotationInteractionDelegate {
    func annotationManager(_ manager: AnnotationManager, didDetectTappedAnnotations annotations: [Annotation]) {
        // Possible multiple tapped if there are overlapped together, only show the first one
        if let annotation = annotations.first {
            didSelectAnnotation(annotation: annotation, tapped: true)
        }
    }

    func didSelectAnnotation(annotation: Annotation, tapped: Bool = false) {

        DispatchQueue.main.async { [weak self] in

            guard let self = self else { return }

            if let feature = annotation.userInfo?["feature"] as? Turf.Feature, case let .point(point) = feature.geometry {

                //  Handle click on the same amenity
                if let p1 = self.selectedAmenity?.properties, let p2 = feature.properties {
                    if case let .string(name1) = p1["name"],
                       case let .string(id1) = p1["id"],
                       case let .string(name2) = p2["name"],
                       case let .string(id2) = p2["id"] {
                        // click the same one, don't show callout
                        if name1 == name2 && id1 == id2 {
                            return
                        }
                    }
                }
                
                //  Keep selected amenity
                self.selectedAmenity = feature

                // set camera (actually should not set if not because of tapped
                if tapped {
//                    let cameraOptions = CameraOptions(center: point.coordinates, zoom: self.navigationMapView?.mapView.mapboxMap.cameraState.zoom) // using current zoom level
                }
            }
        }
    }
}

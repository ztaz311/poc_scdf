//
//  CPCruiseModeCarPark.swift
//  Breeze
//
//  Created by Tan, Tran Ngoc on 05/12/2023.
//

import Foundation
import MapboxDirections
import Turf
import MapboxCoreNavigation
@_spi(Restricted) import MapboxMaps
import MapboxCoreMaps
import MapboxNavigation
import Combine
import SwiftyBeaver
import CarPlay

class CPCruiseModeCarPark:NSObject {
    private weak var navigationMapView: NavigationMapView!
    private weak var carPlayManager: CarPlayManager!
    private var trips = [CPTrip]()
    // Use car park model
    
    var carparkViewModel: CarparksViewModel!
    var disposables = Set<AnyCancellable>()
    private var currentSelectedCarpark: Carpark?
    private var destinationCarPark: Carpark?// the destination has a carpark
    private var filteredCarPark = [Carpark]()
    private var carparkCoordinates = [CLLocationCoordinate2D]()
    private var currentCarpark: Carpark?
    
    var response: RouteResponse?
    typealias RouteRequestSuccess = ((RouteResponse) -> Void)
    typealias RouteRequestFailure = ((Error) -> Void)
    
    var waypoints: [Waypoint] = [] {
        didSet {
            waypoints.forEach {
                $0.coordinateAccuracy = -1
            }
        }
    }
    
    init(navigationMapView: NavigationMapView, carPlayManager: CarPlayManager) {
        SwiftyBeaver.debug("CPCruiseModeCarPark init")
        
        self.navigationMapView = navigationMapView
        self.carPlayManager = carPlayManager
        self.navigationMapView.mapView.ornaments.options.attributionButton.visibility = .hidden
        
    }
    
    func showMapCarPark() {
        DispatchQueue.main.async {
            self.pushToNewMapTemplateWithTrip(self.trips)
        }
    }
    
    fileprivate lazy var defaultSuccess: RouteRequestSuccess = { [weak self] (response) in
        guard let routes = response.routes, !routes.isEmpty, case let .route(options) = response.options else { return }
        guard let self = self else { return }
        self.navigationMapView?.removeWaypoints()
        self.response = response

        // Waypoints which were placed by the user are rewritten by slightly changed waypoints
        // which are returned in response with routes.
        if let waypoints = response.waypoints {
            self.waypoints = waypoints
        }

        let info = CPTrip.cpGetRouteChoice(routeResponse: response)
        guard let coordinate = self.navigationMapView?.mapView.location.latestLocation?.coordinate else {
            print("User location is not valid. Make sure to enable Location Services.")
            return
        }
        
        let currentLocMapItem = MKMapItem(placemark: MKPlacemark(coordinate: coordinate))
        
        self.trips.removeAll()
        self.filteredCarPark.forEach { carPark in
            let carParkLocMapItem = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: carPark.lat ?? 0.0, longitude: carPark.long ?? 0.0)))
            carParkLocMapItem.name = carPark.name
            
            var carParkAvailable = ""
            var carParkCurrentHrRate = ""
            var carParkNextHrRate = ""
            
            if carPark.parkingType == Carpark.typeSeason {
                
                carParkAvailable = "All-day season parking"
            }
            else if(carPark.parkingType == Carpark.typeCustomer){
                
                if(carPark.availablelot?.rawValue == "NA") {
                    
                    carParkAvailable = "No Info"
                    carParkCurrentHrRate = "Strictly for customers only."
                }
                else{
                    
                    if let distance = carPark.distance {
                        if let carParkAvail = carPark.availablelot?.intValue {
                            if carParkAvail != -1 {
                                carParkAvailable = "\(distance)m \(carParkAvail) avail lots"
                            } else {
                                carParkAvailable = "\(distance)m"
                            }
                            
                        }
                        
                    }
                    carParkCurrentHrRate = "Strictly for customers only."
                    
                }
                
            }
            else{
                
                if(carPark.availablelot?.rawValue == "NA") {
                    
                    carParkAvailable = "No Info"
                }
                else{
                    if let distance = carPark.distance {
                        if let carParkAvail = carPark.availablelot?.intValue {
                            if carParkAvail != -1 {
                                carParkAvailable = "\(distance)m . \(carParkAvail) avail lots"
                            } else {
                                carParkAvailable = "\(distance)m"
                            }
                            
                        }
                        
                    }
                }
                
                if let rate = carPark.currentHrRate?.oneHrRate {
                    
                    carParkCurrentHrRate =  self.getPriceDetails(rate: rate)
                }
                else{
                    
                    carParkCurrentHrRate = "No Info(1st hr) ."
                }
                
                if let nextRate = carPark.nextHrRate?.oneHrRate {
                    
                    carParkNextHrRate = self.getPriceDetails(rate: nextRate,firstHour:false)
                } else {
                    
                    carParkNextHrRate = "No Info(next half)"
                }
            }
                                
            let detailsCP = "\(carParkAvailable) \n \(carParkCurrentHrRate)"
            let routeChoice = CPRouteChoice(summaryVariants:[carParkAvailable],
                                            additionalInformationVariants: [detailsCP],
                                            selectionSummaryVariants: [carParkNextHrRate])
            routeChoice.userInfo = info
            let trip = CPTrip.init(origin: currentLocMapItem, destination: carParkLocMapItem, routeChoices: [routeChoice])
            self.trips.append(trip)
        }
        
        let carParks = self.filteredCarPark
        
        //  Show preview trips
        if !self.trips.isEmpty {
            self.updateSelectedCarparkCruiseMode(nil)
            self.showMapCarPark()
        }
    }

    fileprivate lazy var defaultFailure: RouteRequestFailure = { [weak self] (error) in
        guard let self = self else { return }
        // Clear routes from the map
        self.response = nil
    }

    func requestRoute(with options: RouteOptions, success: @escaping RouteRequestSuccess, failure: RouteRequestFailure?) {
        NavigationSettings.shared.directions.calculateWithCache(options: options) { (session, result) in
            switch result {
            case let .success(response):
                success(response)
            case let .failure(error):
                failure?(error)
            }
        }
    }
    
    func requestRoute() {
        guard waypoints.count > 0 else { return }
        guard let coordinate = navigationMapView?.mapView.location.latestLocation?.coordinate else {
            print("User location is not valid. Make sure to enable Location Services.")
            return
        }

        let location = CLLocation(latitude: coordinate.latitude,
                                  longitude: coordinate.longitude)
        let userWaypoint = Waypoint(location: location)
        waypoints.insert(userWaypoint, at: 0)

        let navigationRouteOptions = NavigationRouteOptions(waypoints: waypoints)
        navigationRouteOptions.locale = Locale.enSGLocale()

        requestRoute(with: navigationRouteOptions, success: defaultSuccess, failure: defaultFailure)
    }
    
    private func defaultTripPreviewTextConfiguration() -> CPTripPreviewTextConfiguration {
        
        let goTitle = "Navigate here"
        
        let alternativeRoutesTitle = "Other routes"
        
        let overviewTitle = "Overview"
        
        let defaultPreviewText = CPTripPreviewTextConfiguration(startButtonTitle: goTitle,
                                                                additionalRoutesButtonTitle: alternativeRoutesTitle,
                                                                overviewButtonTitle: overviewTitle)
        return defaultPreviewText
    }
    
    
    private func createCruiseButtons(interfaceController: CPInterfaceController, traitCollection: UITraitCollection) -> [CPBarButton] {
        
        var cpBarButton = [CPBarButton]()
        
        
        
        let backBtn = CPBarButton(type: .text) { (barButton) in
            self.hideCarparks()
        }
        backBtn.title = "Back"
        cpBarButton.append(backBtn)
        
        return cpBarButton
    }
    
    
    
    
    func createNavTralingCruiseButtons(interfaceController: CPInterfaceController, traitCollection: UITraitCollection) -> [CPBarButton] {
        
        var cpBarButton = [CPBarButton]()
        
        // MARK: - end button
        let endBtn = CPBarButton(type: .text) { (barButton) in
            self.endCruise()
        }
        endBtn.title = Constants.cpEndCruise
        cpBarButton.append(endBtn)
        
        
        // MARK: - OBU SDK
        let obuModeBtn = CPBarButton(type: .text) { (barButton) in
            
            if OBUHelper.shared.stateOBU == .CONNECTED
            {
                barButton.title = "$ \(OBUHelper.shared.getCardBalance())"
            } else {
                barButton.title = "OBU"
            }
            
            
            appDelegate().checkStateOBUConnect()
        }
        
        
        if OBUHelper.shared.stateOBU == .CONNECTED
        {
            obuModeBtn.title = "$ \(OBUHelper.shared.getCardBalance())"
        } else {
            obuModeBtn.title = "OBU"
        }
        
        cpBarButton.append(obuModeBtn)
        
        
        return cpBarButton
    }
    
    func createNavLeadingCruiseButtons(interfaceController: CPInterfaceController, traitCollection: UITraitCollection) -> [CPBarButton] {
        
        var cpBarButton = [CPBarButton]()
        
        // MARK: - Mute button
        let muteBtn = CPBarButton(type: .image) { (barButton) in
            if NavigationSettings.shared.voiceMuted {
                self.unmuteCruise()
            } else  {
                self.muteCruise()
            }
        }
        if(NavigationSettings.shared.voiceMuted){
            muteBtn.image = UIImage(named: "carplayMutedIcon", in: nil, compatibleWith: traitCollection)
        } else {
            muteBtn.image = UIImage(named: "carplayUnmutedIcon", in: nil, compatibleWith: traitCollection)
        }
        
        cpBarButton.append(muteBtn)
        
        let showCarParkBtn = CPBarButton(type: .text) { (barButton) in
            
            barButton.title = "Carparks"
            self.showCarparks()
        }
        showCarParkBtn.title = "Carparks"
        
        cpBarButton.append(showCarParkBtn)
        
        return cpBarButton
    }
    
    
    func showCarparks(){
        self.removeCarPark()
        self.trips.removeAll()
        self.filteredCarPark.removeAll()
        if self.carparkViewModel == nil {
            carparkViewModel = CarparksViewModel(service: CarparkService(), arrivalTime: Date(), destName: "")
        }
        self.navigationMapView.navigationCamera.stop()
        navigationMapView?.pointAnnotationManager?.delegate = self
        loadCarparks()
        
    }
    
    func hideCarparks() {
        
        appDelegate().cruiseViewModel?.showCarparkWhenUserClickCarparkButton = false
        
        updateHideCarPark()
        let mapTemplate = self.carPlayManager?.interfaceController?.rootTemplate as? CPMapTemplate
        mapTemplate?.hideTripPreviews()
        
        if appDelegate().cruiseViewModel?.showCarparkWhenSpeedIsZero == false {
            appDelegate().cruiseViewModel?.carPlayMapViewUpdatable?.cruiseStatusUpdate(isCruise: true)
            self.navigationMapView.navigationCamera.follow()
        }
    }
    
    private func setZoomLevelAmenityInCarplay(location: CLLocationCoordinate2D, coordinates: [CLLocationCoordinate2D]) {
        self.navigationMapView?.navigationCamera.stop()
        var allCoordinates: [CLLocationCoordinate2D] = []
        allCoordinates.append(location)
        allCoordinates.append(contentsOf: coordinates)
        let padding = appDelegate().getVisiblePadding()
        let bearing = self.navigationMapView?.mapView.cameraState.bearing
        if let cameraOptions = self.navigationMapView?.mapView.mapboxMap.camera(for: allCoordinates,
                                                                                padding: padding,
                                                                                bearing: bearing,
                                                                                pitch: 0), let mapView = self.navigationMapView?.mapView {
            mapView.camera.ease(to: cameraOptions, duration: 0.4, curve: .easeInOut, completion: nil)
        }
    }
    
    private func loadCarparks() {
        
        if let location = self.navigationMapView.mapView.location.latestLocation {
            let currentLocation = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
            carparkViewModel.load(at: currentLocation, count: UserSettingsModel.sharedInstance.carparkCount, radius: UserSettingsModel.sharedInstance.carparkDistance, maxRadius: Values.carparkMonitorMaxDistanceInLanding)
            carparkViewModel.$carparks.sink { [weak self] carparks in
                guard let self = self, let carparks = carparks else { return }
                
                self.filteredCarPark.removeAll()
                
                if !carparks.isEmpty {
                    self.filteredCarPark.append(contentsOf: carparks)
                }
                
                //  Get route from current location to the first carpark
                if !self.filteredCarPark.isEmpty, let carpark = self.filteredCarPark.first {
                    
                    let destinationCoordinate = CLLocationCoordinate2D(latitude: carpark.lat ?? 0.0, longitude: carpark.long ?? 0.0)
                    let waypoint = Waypoint(coordinate: destinationCoordinate, name: carpark.name)
                    waypoint.targetCoordinate = destinationCoordinate
                    self.waypoints.append(waypoint)

                    self.requestRoute()
                    
                } else {
                    //  TODO handle there is no carpark nearby
                }
                
            }.store(in: &disposables)
        }
    }
 
    private func getPriceDetails(rate:Double,firstHour:Bool = true) -> String{
        
        var priceString = ""
        if Int(rate) == 0 {
            
            if(firstHour){
                priceString =  "Free(1st hr) ."
            }
            else{
                
                priceString =  "Free(next half)"
            }
            
            
        } else if Int(rate) == -2 {
            
            if(firstHour){
                priceString = "Season(1st hr) ."
            }
            else{
                
                priceString = "Season(next half)"
            }
            
            
        } else if Int(rate) == -1 {
            if(firstHour){
                priceString = "No Info(1st hr) ."
            }
            else{
                
                priceString = "No Info(next half)"
            }
        } else {
            priceString = String(format: "$%.2f", rate)
            
            if(firstHour){
                priceString =  "\(priceString)(1st hr) . "
            }
            else{
                
                priceString =  "\(priceString)(next half)"
            }
            
        }
        
        return priceString
    }
    
    private func addCarparkAnnotation(carpark: Carpark, selected: Bool = false) -> PointAnnotation? {

        if let carparkId = carpark.id, let carplayNavigationMapView = appDelegate().cruiseViewModel?.carPlayMapViewUpdatable?.navigationMapView {
            var annotation = PointAnnotation(id: carparkId, coordinate: CLLocationCoordinate2D(latitude: carpark.lat ?? 0, longitude: carpark.long ?? 0))
            annotation.image = getAnnotationImage(carpark: carpark, selected: selected, ignoreDestination: true)
            annotation.userInfo = [
                "carpark": carpark
            ]

            if let layerId = carplayNavigationMapView.pointAnnotationManager?.layerId, !layerId.isEmpty {
                try? carplayNavigationMapView.mapView.mapboxMap.style.setLayerProperty(for: layerId, property: "icon-allow-overlap", value: true)
            }
            return annotation
        }
        return nil
    }
    
    
    private func updateSelectedCarparkCruiseMode(_ carpark: Carpark?, needUpdateZoomLevel: Bool = true) {
        
        guard let carplayNavigationMapView = appDelegate().cruiseViewModel?.carPlayMapViewUpdatable?.navigationMapView else {return}
        
        guard let coordinate = self.navigationMapView?.mapView.location.latestLocation?.coordinate else {
            print("User location is not valid. Make sure to enable Location Services.")
            return
        }
        
        self.cleanUpMap()
        
        self.currentCarpark = carpark
        
        let carParks = self.filteredCarPark
        self.carparkCoordinates.removeAll()
        //  Show all carparks in Home
        if !carParks.isEmpty {
            
            appDelegate().cruiseViewModel?.showCarparkWhenUserClickCarparkButton = true
            appDelegate().cruiseViewModel?.showCarparkWhenSpeedIsZero = false
            
            carplayNavigationMapView.mapView.addCarparks(carParks, showOnlyWithCostSymbol: true, belowPuck: false)
            
            var annotations = [PointAnnotation]()
            for carpark in carParks {
                var isSelected: Bool = false
                if let theCarpark = self.currentCarpark {
                    if theCarpark.name == carpark.name {
                        isSelected = true
                    }
                }
                
                //  Save the selected carpark and change status of selected
                if let annotation = self.addCarparkAnnotation(carpark: carpark, selected: isSelected) {
                    annotations.append(annotation)
                }
                
                self.carparkCoordinates.append(CLLocationCoordinate2D(latitude: carpark.lat ?? 0.0, longitude: carpark.long ?? 0.0))
            }
            
            carplayNavigationMapView.pointAnnotationManager?.annotations = annotations
            
            if needUpdateZoomLevel {
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    self.setZoomLevelAmenityInCarplay(location: coordinate, coordinates: self.carparkCoordinates)
                }
            }
            
        } else {
            //  Need to handle no carpark nearby
        }
    }
    
    
    private func cleanUpMap() {
        guard let carplayNavigationMapView = appDelegate().cruiseViewModel?.carPlayMapViewUpdatable?.navigationMapView else {return}
        carplayNavigationMapView.pointAnnotationManager?.annotations = [PointAnnotation]()
        carplayNavigationMapView.navigationCamera.stop()
    }
    
    
    func endCruise() {
        appDelegate().cruiseViewModel?.showCarparkWhenUserClickCarparkButton = false
        appDelegate().cruiseViewModel?.showCarparkWhenSpeedIsZero = false
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CarplayCruise.UserClick.end_cruise, screenName: ParameterName.CarplayCruise.screen_view)
        appDelegate().carPlayMapController?.stopCruise()
        appDelegate().hideCarParks()
        appDelegate().carPlayManager.carPlayMapViewController?.dismissAnyAlertControllerIfPresent()
    }
    
    
    func muteCruise() {
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CarplayNavigation.UserClick.mute, screenName: ParameterName.CarplayNavigation.screen_view)
        appDelegate().cruiseViewModel?.carPlayMapViewUpdatable?.delegate?.onMuteEnabled(true)
    }
    
    func unmuteCruise() {
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CarplayNavigation.UserClick.unmute, screenName: ParameterName.CarplayNavigation.screen_view)
        appDelegate().cruiseViewModel?.carPlayMapViewUpdatable?.delegate?.onMuteEnabled(false)
    }
    
    func navigateCarPark() {
        endCruise()
        self.removeCarPark()
        self.navigationMapView.mapView.removeCarparkLayer()
        let mapTemplate = self.carPlayManager?.interfaceController?.rootTemplate as? CPMapTemplate
        mapTemplate?.hideTripPreviews()
        
    }
    
    func removeCarPark() {
        if let navigationMapview = appDelegate().cruiseViewModel?.carparkUpdater {
            self.navigationMapView.mapView.removeCarparkLayer()
            navigationMapview.removeCarParks(keepDestination: true)
        }
    }
    
    func updateHideCarPark() {
        guard let carplayNavigationMapView = appDelegate().cruiseViewModel?.carPlayMapViewUpdatable?.navigationMapView else {return}
        carplayNavigationMapView.removeCarParks(self.filteredCarPark)
        let annotations = [PointAnnotation]()
        self.navigationMapView?.pointAnnotationManager?.annotations = annotations
        self.navigationMapView?.mapView.removeCarparkLayer()
        self.trips.removeAll()
        self.removeCarPark()
//        updatePuckIcon(isStarted: true)
    }
    
    
    func pushToNewMapTemplateWithTrip(_ trips: [CPTrip]) {
        
        let newMapTemplate = CPMapTemplate()
        newMapTemplate.trailingNavigationBarButtons = []
        newMapTemplate.leadingNavigationBarButtons = []
        newMapTemplate.mapDelegate = self
        
        let backBtn = CPBarButton(type: .text) { (barButton) in
            newMapTemplate.hideTripPreviews()
            self.carPlayManager?.interfaceController?.popTemplate(animated: true)
            self.hideCarparks()
        }
        backBtn.title = "Back"
        newMapTemplate.backButton = backBtn
        
        self.carPlayManager?.interfaceController?.pushTemplate(newMapTemplate, animated: true)
        newMapTemplate.showTripPreviews(trips, textConfiguration: self.defaultTripPreviewTextConfiguration())
    }
}


extension CPCruiseModeCarPark:CPMapTemplateDelegate {
    
    // Selected CarPark on TripPreview
    func mapTemplate(_ mapTemplate: CPMapTemplate, selectedPreviewFor trip: CPTrip, using routeChoice: CPRouteChoice) {
        let matchedCarPark = self.filteredCarPark.filter ({ $0.name == trip.destination.placemark.name})
        
        if(matchedCarPark.count > 0)
        {
            self.updateSelectedCarparkCruiseMode(matchedCarPark.first, needUpdateZoomLevel: false)
        }
        
    }
    
    // MARK: Start Navigation -> Pop to MapView -> Push Routeplaning -> Push Navigation
    func mapTemplate(_ mapTemplate: CPMapTemplate, startedTrip trip: CPTrip, using routeChoice: CPRouteChoice) {
        
        let matchedCarPark = self.filteredCarPark.filter ({ $0.name == trip.destination.placemark.name })
        if let carpark = matchedCarPark.first {
            let location = CLLocation(latitude: carpark.lat ?? 0.0, longitude: carpark.long ?? 0.0)
            getAddressFromLocation(location: location) { address in
                
                let address = SearchAddresses(alias: "", address1: carpark.name ?? "", lat: "\(carpark.lat ?? 0)", long: "\(carpark.long ?? 0)", address2: address, distance: "\(carpark.distance ?? 0)")
                
                let appDelegate = appDelegate()
                if let carPlayManager = self.carPlayManager,
                   let navigationMapView = self.carPlayManager?.navigationMapView {
                    appDelegate.carRPController = CarPlayRPController(navigationMapView: navigationMapView,
                                                                      carPlayManager: carPlayManager, address: address)
                    appDelegate.carRPController?.clearPreviousRouteResponse()
                    appDelegate.carRPController?.address = address
                    self.navigateCarPark()
                    DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                        appDelegate.carRPController?.getRoutes()
                    }
                }
            }
        }
        
    }
    
}


extension CPCruiseModeCarPark: AnnotationInteractionDelegate {
    func annotationManager(_ manager: AnnotationManager, didDetectTappedAnnotations annotations: [Annotation]) {

    }

    func didSelectAnnotation(annotation: Annotation, tapped: Bool = false) {

    }
}


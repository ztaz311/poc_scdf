//
//  CPNavigationModeCarpark.swift
//  Breeze
//
//  Created by Tan, Tran Ngoc on 30/11/2023.
//

import Foundation
import MapboxDirections
import Turf
import MapboxCoreNavigation
@_spi(Restricted) import MapboxMaps
import MapboxCoreMaps
import MapboxNavigation
import Combine
import SwiftyBeaver
import CarPlay

class CPNavigationModeCarpark:NSObject {
    private weak var navigationMapView: NavigationMapView!
    private weak var carPlayManager: CarPlayManager!
    private var trips = [CPTrip]()
    // Use car park model
    
    var carparkViewModel: CarparksViewModel!
    var disposables = Set<AnyCancellable>()
    private var currentSelectedCarpark: Carpark?
    private var filteredCarParks = [Carpark]()
    private var currentCarpark: Carpark?
    private var carparkCoordinates = [CLLocationCoordinate2D]()
    
    var response: RouteResponse?
    typealias RouteRequestSuccess = ((RouteResponse) -> Void)
    typealias RouteRequestFailure = ((Error) -> Void)
    
    var isShowingCarparks: Bool = false
    
    private var destination: CPTrip?
    
    var waypoints: [Waypoint] = [] {
        didSet {
            waypoints.forEach {
                $0.coordinateAccuracy = -1
            }
        }
    }
    
    init(navigationMapView: NavigationMapView, carPlayManager: CarPlayManager, destination: CPTrip?) {
        SwiftyBeaver.debug("CPNavigationModeCarpark init")
        self.destination = destination
        self.navigationMapView = navigationMapView
        self.carPlayManager = carPlayManager
        self.navigationMapView.mapView.ornaments.options.attributionButton.visibility = .hidden
        
    }
    
    func showMapCarPark() {
        DispatchQueue.main.async {
            //  Temporary for testing new map
            if !self.isShowingCarparks {
                self.isShowingCarparks = true
                self.pushToNewMapTemplateWithTrip(self.trips)
            }
            
//            if let mapTemplate = self.carPlayManager?.interfaceController?.rootTemplate as? CPMapTemplate {
//                mapTemplate.mapDelegate = self
//                mapTemplate.showTripPreviews(self.trips, textConfiguration: self.defaultTripPreviewTextConfiguration())
//                mapTemplate.leadingNavigationBarButtons = []
//                mapTemplate.trailingNavigationBarButtons = []
//                self.carPlayManager.carPlayNavigationViewController?.speedLimitView.isHidden = true
//                if let interfaceController = self.carPlayManager.interfaceController {
//                    mapTemplate.leadingNavigationBarButtons = self.createNavigationButtons(interfaceController: interfaceController, traitCollection:  self.carPlayManager.carPlayMapViewController!.traitCollection)
//                }
//            }
        }
    }
    
    func pushToNewMapTemplateWithTrip(_ trips: [CPTrip]) {
        
        self.updatePuckIcon(isStarted: false)
        
        let newMapTemplate = CPMapTemplate()
        newMapTemplate.trailingNavigationBarButtons = []
        newMapTemplate.leadingNavigationBarButtons = []
        newMapTemplate.mapDelegate = self
        
        let backBtn = CPBarButton(type: .text) { (barButton) in
            self.isShowingCarparks = false
            newMapTemplate.hideTripPreviews()
            self.updatePuckIcon(isStarted: true)
            self.carPlayManager.interfaceController?.popTemplate(animated: true)
            self.hideCarparks()
        }
        backBtn.title = "Back"
        newMapTemplate.backButton = backBtn
        
        self.carPlayManager.interfaceController?.pushTemplate(newMapTemplate, animated: false)
        newMapTemplate.showTripPreviews(trips, textConfiguration: self.defaultTripPreviewTextConfiguration())
    }
    
    fileprivate lazy var defaultSuccess: RouteRequestSuccess = { [weak self] (response) in
        guard let routes = response.routes, !routes.isEmpty, case let .route(options) = response.options else { return }
        guard let self = self else { return }
        self.navigationMapView?.removeWaypoints()
        self.response = response

        // Waypoints which were placed by the user are rewritten by slightly changed waypoints
        // which are returned in response with routes.
        if let waypoints = response.waypoints {
            self.waypoints = waypoints
        }

        let info = CPTrip.cpGetRouteChoice(routeResponse: response)
        guard let coordinate = self.navigationMapView?.mapView.location.latestLocation?.coordinate else {
            print("User location is not valid. Make sure to enable Location Services.")
            return
        }
        
        let currentLocMapItem = MKMapItem(placemark: MKPlacemark(coordinate: coordinate))
        
        self.trips.removeAll()
        self.filteredCarParks.forEach { carPark in
            let carParkLocMapItem = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: carPark.lat ?? 0.0, longitude: carPark.long ?? 0.0)))
            carParkLocMapItem.name = carPark.name
            
            var carParkAvailable = ""
            var carParkCurrentHrRate = ""
            var carParkNextHrRate = ""
            
            if carPark.parkingType == Carpark.typeSeason {
                
                carParkAvailable = "All-day season parking"
            }
            else if(carPark.parkingType == Carpark.typeCustomer){
                
                if(carPark.availablelot?.rawValue == "NA") {
                    
                    carParkAvailable = "No Info"
                    carParkCurrentHrRate = "Strictly for customers only."
                }
                else{
                    
                    if let distance = carPark.distance {
                        if let carParkAvail = carPark.availablelot?.intValue {
                            if carParkAvail != -1 {
                                carParkAvailable = "\(distance)m . \(carParkAvail) avail lots"
                            } else {
                                carParkAvailable = "\(distance)m"
                            }
                            
                        }
                        
                    }
                    carParkCurrentHrRate = "Strictly for customers only."
                    
                }
                
            }
            else{
                
                if(carPark.availablelot?.rawValue == "NA") {
                    
                    carParkAvailable = "No Info"
                }
                else{
                    if let distance = carPark.distance {
                        if let carParkAvail = carPark.availablelot?.intValue {
                            if carParkAvail != -1 {
                                carParkAvailable = "\(distance)m . \(carParkAvail) avail lots"
                            } else {
                                carParkAvailable = "\(distance)m"
                            }
                            
                        }
                        
                    }
                }
                
                if let rate = carPark.currentHrRate?.oneHrRate {
                    
                    carParkCurrentHrRate =  self.getPriceDetails(rate: rate)
                }
                else{
                    
                    carParkCurrentHrRate = "No Info(1st hr) ."
                }
                
                if let nextRate = carPark.nextHrRate?.oneHrRate {
                    
                    carParkNextHrRate = self.getPriceDetails(rate: nextRate,firstHour:false)
                } else {
                    
                    carParkNextHrRate = "No Info(next half)"
                }
            }
                                
            let detailsCP = "\(carParkAvailable) \n \(carParkCurrentHrRate)"
            let routeChoice = CPRouteChoice(summaryVariants:[carParkAvailable],
                                            additionalInformationVariants: [detailsCP],
                                            selectionSummaryVariants: [carParkNextHrRate])
            routeChoice.userInfo = info
            let trip = CPTrip.init(origin: currentLocMapItem, destination: carParkLocMapItem, routeChoices: [routeChoice])
            self.trips.append(trip)
        }
        
        let carParks = self.filteredCarParks
        
        //  Show preview trips
        if !self.trips.isEmpty {
            self.updateSelectedCarpark(nil)
            self.showMapCarPark()
        }
    }

    fileprivate lazy var defaultFailure: RouteRequestFailure = { [weak self] (error) in
        guard let self = self else { return }
        // Clear routes from the map
        self.response = nil
    }

    func requestRoute(with options: RouteOptions, success: @escaping RouteRequestSuccess, failure: RouteRequestFailure?) {
        NavigationSettings.shared.directions.calculateWithCache(options: options) { (session, result) in
            switch result {
            case let .success(response):
                success(response)
            case let .failure(error):
                failure?(error)
            }
        }
    }
    
    func requestRoute() {
        guard waypoints.count > 0 else { return }
        guard let coordinate = navigationMapView?.mapView.location.latestLocation?.coordinate else {
            print("User location is not valid. Make sure to enable Location Services.")
            return
        }

        let location = CLLocation(latitude: coordinate.latitude,
                                  longitude: coordinate.longitude)
        let userWaypoint = Waypoint(location: location)
        waypoints.insert(userWaypoint, at: 0)

        let navigationRouteOptions = NavigationRouteOptions(waypoints: waypoints)
        navigationRouteOptions.locale = Locale.enSGLocale()

        requestRoute(with: navigationRouteOptions, success: defaultSuccess, failure: defaultFailure)
    }
    
    private func defaultTripPreviewTextConfiguration() -> CPTripPreviewTextConfiguration {
        
        let goTitle = "Navigate here"
        
        let alternativeRoutesTitle = "Other routes"
        
        let overviewTitle = "Overview"
        
        let defaultPreviewText = CPTripPreviewTextConfiguration(startButtonTitle: goTitle,
                                                                additionalRoutesButtonTitle: alternativeRoutesTitle,
                                                                overviewButtonTitle: overviewTitle)
        return defaultPreviewText
    }
    
    
    private func createNavigationButtons(interfaceController: CPInterfaceController, traitCollection: UITraitCollection) -> [CPBarButton] {
        
        var cpBarButton = [CPBarButton]()
        
        
        
        let backBtn = CPBarButton(type: .text) { (barButton) in
            self.hideCarparks()
        }
        backBtn.title = "Back"
        cpBarButton.append(backBtn)
        
        return cpBarButton
    }
    
    func createNavTralingNavigationButtons(interfaceController: CPInterfaceController, traitCollection: UITraitCollection) -> [CPBarButton] {
        
        var cpBarButton = [CPBarButton]()
        
        // MARK: - end button
        let endBtn = CPBarButton(type: .text) { (barButton) in
            self.endNavigation()
        }
        endBtn.title = Constants.cpEndNav
        cpBarButton.append(endBtn)
        
        
        // MARK: - OBU SDK
        let obuModeBtn = CPBarButton(type: .text) { (barButton) in
            
            if OBUHelper.shared.stateOBU == .CONNECTED
            {
                barButton.title = "$ \(OBUHelper.shared.getCardBalance())"
            } else {
                barButton.title = "OBU"
            }
            
            
            appDelegate().checkStateOBUConnect()
        }
        
        
        if OBUHelper.shared.stateOBU == .CONNECTED
        {
            obuModeBtn.title = "$ \(OBUHelper.shared.getCardBalance())"
        } else {
            obuModeBtn.title = "OBU"
        }
        
        cpBarButton.append(obuModeBtn)
        
        
        // MARK: - Public Message Announce
#if DEMO
        let pmButton = CPBarButton(type: .text) { (barButton) in
            //            self.delegate?.CPNavigationNavigationButtonSelected(type: barButton.title ?? Constants.cpPbMessage)
        }
        pmButton.title = Constants.cpPbMessage
        
        cpBarButton.append(pmButton)
#endif
        
        return cpBarButton
    }
    
    func createNavLeadingNavigationButtons(interfaceController: CPInterfaceController, traitCollection: UITraitCollection) -> [CPBarButton] {
        
        var cpBarButton = [CPBarButton]()
        
        // MARK: - Mute button
        let muteBtn = CPBarButton(type: .image) { (barButton) in
            if NavigationSettings.shared.voiceMuted {
                self.unmuteNavigation()
            } else  {
                self.muteNavigation()
            }
        }
        if(NavigationSettings.shared.voiceMuted){
            muteBtn.image = UIImage(named: "carplayMutedIcon", in: nil, compatibleWith: traitCollection)
        } else {
            muteBtn.image = UIImage(named: "carplayUnmutedIcon", in: nil, compatibleWith: traitCollection)
        }
        
        cpBarButton.append(muteBtn)
        
        let showCarParkBtn = CPBarButton(type: .text) { (barButton) in
            barButton.title = "Carparks"
            self.showCarparks()
        }
        showCarParkBtn.title = "Carparks"
        cpBarButton.append(showCarParkBtn)
        
        return cpBarButton
    }
    
    private func updateNavigationButton(_ isShowingAmenity: Bool) {
        if isShowingAmenity {            
            DispatchQueue.main.async {
                if let mapTemplate = self.carPlayManager?.interfaceController?.rootTemplate as? CPMapTemplate {
                    mapTemplate.trailingNavigationBarButtons = []
                    
                    let backBtn = CPBarButton(type: .text) { [weak self] (barButton) in
                        self?.updateNavigationButton(false)
                    }
                    backBtn.title = "Back"
                    mapTemplate.leadingNavigationBarButtons = [backBtn]
                }
            }
        } else {
            
            self.hideCarparks()
            if let mapTemplate = self.carPlayManager?.interfaceController?.rootTemplate as? CPMapTemplate, let traitCollection = self.carPlayManager?.carPlayMapViewController?.traitCollection, let interface = self.carPlayManager?.interfaceController {
                mapTemplate.leadingNavigationBarButtons = appDelegate().cpBarBrowseButtons.createLeadingNavigationButtons(interfaceController: interface, traitCollection: traitCollection)
                mapTemplate.trailingNavigationBarButtons = appDelegate().cpBarBrowseButtons.createTrailingNavigationButtons(interfaceController: interface, traitCollection: traitCollection)
            }
        }
    }
    
    func showCarparks(){
        self.removeCarPark()
        self.trips.removeAll()
        self.filteredCarParks.removeAll()
        if self.carparkViewModel == nil {
            carparkViewModel = CarparksViewModel(service: CarparkService(), arrivalTime: Date(), destName: "")
        }
        navigationMapView?.pointAnnotationManager?.delegate = self
        loadCarparks()
//        self.carPlayManager.carPlayNavigationViewController?.navigationService.stop()
        
    }
    
    func hideCarparks() {
        updateWhenNavigation()
        carparkViewModel = CarparksViewModel(service: CarparkService(), arrivalTime: Date(), destName: "")
        let mapTemplate = self.carPlayManager?.interfaceController?.rootTemplate as? CPMapTemplate
        mapTemplate?.hideTripPreviews()
        
        if let interfaceController = self.carPlayManager.interfaceController {
            mapTemplate?.leadingNavigationBarButtons = self.createNavLeadingNavigationButtons(interfaceController: interfaceController, traitCollection:  self.carPlayManager.carPlayMapViewController!.traitCollection)
            mapTemplate?.trailingNavigationBarButtons = self.createNavTralingNavigationButtons(interfaceController: interfaceController, traitCollection:  self.carPlayManager.carPlayMapViewController!.traitCollection)
        }
        
        carPlayManager.carPlayNavigationViewController?.speedLimitView.isHidden = false
//        self.carPlayManager.carPlayNavigationViewController?.navigationService.start()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.updateDestinationWhenHideCarPark()
        }
        self.resumeNavigation()
    }
    
    private func setZoomLevelAmenityInCarplay(location: CLLocationCoordinate2D, coordinates: [CLLocationCoordinate2D]) {
        
        guard let navMapView = appDelegate().navigationViewModel?.carPlayMapViewUpdatable?.navigationMapView else {return}
        
        var allCoordinates: [CLLocationCoordinate2D] = []
        allCoordinates.append(location)
        allCoordinates.append(contentsOf: coordinates)
        let padding = appDelegate().getVisiblePadding()
        let cameraOptions = navMapView.mapView.mapboxMap.camera(for: allCoordinates,
                                                                                padding: padding,
                                                                                bearing: 0,
                                                                                pitch: 0)
        navMapView.mapView.camera.ease(to: cameraOptions, duration: 0.4, curve: .easeInOut, completion: nil)
    }
    
    private func loadCarparks() {
        
        if let location = self.navigationMapView.mapView.location.latestLocation {
            let currentLocation = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
            carparkViewModel.load(at: currentLocation, count: UserSettingsModel.sharedInstance.carparkCount, radius: UserSettingsModel.sharedInstance.carparkDistance, maxRadius: Values.carparkMonitorMaxDistanceInLanding)
            carparkViewModel.$carparks.sink { [weak self] carparks in
                guard let self = self, let carparks = carparks else { return }
                
                self.filteredCarParks.removeAll()
                
                if !carparks.isEmpty {
                    self.filteredCarParks.append(contentsOf: carparks)
                }
                
                //  Get route from current location to the first carpark
                if !self.filteredCarParks.isEmpty, let carpark = self.filteredCarParks.first {
                    
                    let destinationCoordinate = CLLocationCoordinate2D(latitude: carpark.lat ?? 0.0, longitude: carpark.long ?? 0.0)
                    let waypoint = Waypoint(coordinate: destinationCoordinate, name: carpark.name)
                    waypoint.targetCoordinate = destinationCoordinate
                    self.waypoints.append(waypoint)

                    self.requestRoute()
                    
                } else {
                    //  TODO handle there is no carpark nearby
                }
                
            }.store(in: &disposables)
        }
    }
 
    private func getPriceDetails(rate:Double,firstHour:Bool = true) -> String{
        
        var priceString = ""
        if Int(rate) == 0 {
            
            if(firstHour){
                priceString =  "Free(1st hr) ."
            }
            else{
                
                priceString =  "Free(next half)"
            }
            
            
        } else if Int(rate) == -2 {
            
            if(firstHour){
                priceString = "Season(1st hr) ."
            }
            else{
                
                priceString = "Season(next half)"
            }
            
            
        } else if Int(rate) == -1 {
            if(firstHour){
                priceString = "No Info(1st hr) ."
            }
            else{
                
                priceString = "No Info(next half)"
            }
        } else {
            priceString = String(format: "$%.2f", rate)
            
            if(firstHour){
                priceString =  "\(priceString)(1st hr) . "
            }
            else{
                
                priceString =  "\(priceString)(next half)"
            }
            
        }
        
        return priceString
    }
    
    private func cleanUpMap() {
        guard let carplayNavigationMapView = appDelegate().navigationViewModel?.carPlayMapViewUpdatable?.navigationMapView else {return}
                
        carplayNavigationMapView.removeRoutes()
        carplayNavigationMapView.removeWaypoints()
        carplayNavigationMapView.removeArrow()
        
        carplayNavigationMapView.pointAnnotationManager?.annotations = [PointAnnotation]()
        carplayNavigationMapView.navigationCamera.stop()
    }
    
    private func resumeNavigation() {
        guard let carplayNavMapView = appDelegate().navigationViewModel?.carPlayMapViewUpdatable?.navigationMapView else {return}
        carplayNavMapView.showsRelativeDurationOnContinuousAlternativeRoutes = true
        carplayNavMapView.navigationCamera.follow()
        
        if let routeProgress = self.carPlayManager.carPlayNavigationViewController?.navigationService.routeProgress, let coordinate = self.navigationMapView?.mapView.location.latestLocation?.coordinate {
            carplayNavMapView.updateRouteLine(routeProgress: routeProgress, coordinate: coordinate, shouldRedraw: true)
        }
        
    }
  
    fileprivate func updateSelectedCarpark(_ carpark: Carpark?, needUpdateZoomLevel: Bool = true) {
        
        guard let carplayNavigationMapView = appDelegate().navigationViewModel?.carPlayMapViewUpdatable?.navigationMapView else {return}
        
        guard let coordinate = self.navigationMapView?.mapView.location.latestLocation?.coordinate else {
            print("User location is not valid. Make sure to enable Location Services.")
            return
        }
        
        if needUpdateZoomLevel {
            self.cleanUpMap()
        }
        
        self.currentCarpark = carpark
        
        let carParks = self.filteredCarParks
        self.carparkCoordinates.removeAll()
        
        if !carParks.isEmpty {
            carplayNavigationMapView.mapView.addCarparks(carParks, showOnlyWithCostSymbol: true, belowPuck: false)
            
            var annotations = [PointAnnotation]()
            for carpark in carParks {
                var isSelected: Bool = false
                if let theCarpark = self.currentCarpark {
                    if theCarpark.name == carpark.name {
                        isSelected = true
                    }
                }
                
                //  Save the selected carpark and change status of selected
                if let annotation = self.addCarparkAnnotation(carpark: carpark, selected: isSelected) {
                    annotations.append(annotation)
                }
                
                self.carparkCoordinates.append(CLLocationCoordinate2D(latitude: carpark.lat ?? 0.0, longitude: carpark.long ?? 0.0))
            }
            
            carplayNavigationMapView.pointAnnotationManager?.annotations = annotations
            
            if needUpdateZoomLevel {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    self.setZoomLevelAmenityInCarplay(location: coordinate, coordinates: self.carparkCoordinates)
                }
            }
            
        } else {
            //  Need to handle no carpark nearby
        }
    }
    
    private func addCarparkAnnotation(carpark: Carpark, selected: Bool = false) -> PointAnnotation? {

        if let carparkId = carpark.id, let carplayNavigationMapView = appDelegate().navigationViewModel?.carPlayMapViewUpdatable?.navigationMapView {
            var annotation = PointAnnotation(id: carparkId, coordinate: CLLocationCoordinate2D(latitude: carpark.lat ?? 0, longitude: carpark.long ?? 0))
            annotation.image = getAnnotationImage(carpark: carpark, selected: selected, ignoreDestination: false)
            annotation.userInfo = [
                "carpark": carpark
            ]

            if let layerId = carplayNavigationMapView.pointAnnotationManager?.layerId, !layerId.isEmpty {
                try? carplayNavigationMapView.mapView.mapboxMap.style.setLayerProperty(for: layerId, property: "icon-allow-overlap", value: true)
            }
            return annotation
        }
        return nil
    }

    
    
    func endNavigation() {
            
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CarplayNavigation.UserClick.end, screenName: ParameterName.CarplayNavigation.screen_view)
            appDelegate().tripETA = nil
            if let userLocation = self.carPlayManager.navigationMapView?.mapView.location.latestLocation {
                appDelegate().navigationViewModel?.saveTripLog(location: CLLocation(latitude: (userLocation.coordinate.latitude), longitude: (userLocation.coordinate.longitude)), fromUser: true,completion: { _ in
                    
                    appDelegate().navigationViewModel?.endTrip(location:CLLocation(latitude: (userLocation.coordinate.latitude), longitude: (userLocation.coordinate.longitude)), manually: true)
                    appDelegate().carPlayManagerDidEndNavigation(self.carPlayManager)
                })
            }
    }
    
    
    func muteNavigation() {
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CarplayNavigation.UserClick.mute, screenName: ParameterName.CarplayNavigation.screen_view)
        appDelegate().navigationViewModel?.carPlayMapViewUpdatable?.delegate?.onMuteEnabled(true)
    }
    
    func unmuteNavigation() {
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CarplayNavigation.UserClick.unmute, screenName: ParameterName.CarplayNavigation.screen_view)
        appDelegate().navigationViewModel?.carPlayMapViewUpdatable?.delegate?.onMuteEnabled(false)
    }
    
    func navigateCarPark() {
        endNavigation()
        self.removeCarPark()
        self.navigationMapView?.mapView.removeCarparkLayer()
        let mapTemplate = self.carPlayManager?.interfaceController?.rootTemplate as? CPMapTemplate
        mapTemplate?.hideTripPreviews()
    }
    
    
    func removeCarPark() {
        if let navigationMapview = appDelegate().navigationViewModel?.carParkUpdater {
            self.navigationMapView.mapView.removeCarparkLayer()
            navigationMapview.removeCarParks(keepDestination: true)
        }
    }
    
    func updatePuckIcon(isStarted:Bool) {
        guard let carplayNavigationMapView = appDelegate().navigationViewModel?.carPlayMapViewUpdatable?.navigationMapView else {return}
        if let traitCollection = self.carPlayManager.carPlayMapViewController?.traitCollection {
            if isStarted {
                carplayNavigationMapView.userLocationStyle = UserLocationStyle.courseView()
                UserPuckCourseView.appearance(for: traitCollection).puckColor = UIColor(named:"naviPuckColor")!
                UserPuckCourseView.appearance(for: traitCollection).shadowColor = .clear
                UserPuckCourseView.appearance(for: traitCollection).stalePuckColor = UIColor(named:"naviPuckBGColor")!.withAlphaComponent(0.2)
                UserPuckCourseView.appearance(for: traitCollection).fillColor = UIColor(named:"naviPuckBGColor")!
            } else {
                carplayNavigationMapView.userLocationStyle = .puck2D(configuration: Puck2DConfiguration(topImage: UIImage(named: "puckArrow",in:nil,compatibleWith: traitCollection), bearingImage: UIImage(named: "puckIcon"), shadowImage: nil, scale: nil))
            }
        }
        carplayNavigationMapView.mapView.location.options.puckBearingSource = .heading
    }
    
    func updateWhenNavigation() {
        guard let carplayNavigationMapView = appDelegate().navigationViewModel?.carPlayMapViewUpdatable?.navigationMapView else {return}
        carplayNavigationMapView.removeCarParks(self.filteredCarParks)
        let annotations = [PointAnnotation]()
        self.navigationMapView?.pointAnnotationManager?.annotations = annotations
        self.navigationMapView?.mapView.removeCarparkLayer()
        self.trips.removeAll()
        self.removeCarPark()
    }
    
    func updateDestinationWhenHideCarPark() {
        var annotations = [PointAnnotation]()
        if let finalDestCoord = self.destination?.destination.placemark.coordinate {
            var destinationAnnotation = PointAnnotation(coordinate: finalDestCoord)
            destinationAnnotation.image = .init(image: UIImage(named: "destinationMark")!, name: "navigationDest")
            annotations.append(destinationAnnotation)
        }
    }
    
}


extension CPNavigationModeCarpark:CPMapTemplateDelegate {
    
    // Selected CarPark on TripPreview 
    func mapTemplate(_ mapTemplate: CPMapTemplate, selectedPreviewFor trip: CPTrip, using routeChoice: CPRouteChoice) {
        let matchedCarPark = self.filteredCarParks.filter ({ $0.name == trip.destination.placemark.name})
        
        if(matchedCarPark.count > 0)
        {
            self.updateSelectedCarpark(matchedCarPark.first, needUpdateZoomLevel: false)
        }
        
    }
    
    // MARK: Start Navigation -> Pop to MapView -> Push Routeplaning -> Push Navigation
    func mapTemplate(_ mapTemplate: CPMapTemplate, startedTrip trip: CPTrip, using routeChoice: CPRouteChoice) {
        
        let matchedCarPark = self.filteredCarParks.filter ({ $0.name == trip.destination.placemark.name })
        if let carpark = matchedCarPark.first {
            let location = CLLocation(latitude: carpark.lat ?? 0.0, longitude: carpark.long ?? 0.0)
            getAddressFromLocation(location: location) { address in
                
                let address = SearchAddresses(alias: "", address1: carpark.name ?? "", lat: "\(carpark.lat ?? 0)", long: "\(carpark.long ?? 0)", address2: address, distance: "\(carpark.distance ?? 0)")
                
                let appDelegate = appDelegate()
                if let carPlayManager = self.carPlayManager,
                   let navigationMapView = self.carPlayManager?.navigationMapView {
                    appDelegate.carRPController = CarPlayRPController(navigationMapView: navigationMapView,
                                                                      carPlayManager: carPlayManager, address: address)
                    appDelegate.carRPController?.clearPreviousRouteResponse()
                    appDelegate.carRPController?.address = address
                    self.navigateCarPark()
                    DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                        appDelegate.carRPController?.getRoutes()
                    }
                }
            }
        }
        
    }
    
}


extension CPNavigationModeCarpark: AnnotationInteractionDelegate {
    func annotationManager(_ manager: AnnotationManager, didDetectTappedAnnotations annotations: [Annotation]) {

    }

    func didSelectAnnotation(annotation: Annotation, tapped: Bool = false) {

    }
}

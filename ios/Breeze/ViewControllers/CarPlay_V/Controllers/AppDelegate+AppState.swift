//
//  AppDelegate+AppState.swift
//  Breeze
//
//  Created by Zhou Hao on 12/10/21.
//

import Foundation
import SwiftyBeaver
import MapboxCoreNavigation

enum DeviceType {
    case mobile
    case carPlay
}

// TODO: Consider to use State Machine if the state get more and more complicated
// Actually this is very similar to State Machine already. We can add checking on previous state if needed
extension AppDelegate {
    
    func startAppStateMonitor() {
        appStateCancellable = $appState.sink(receiveValue: { [weak self] state in
            guard let self = self else { return }
            SwiftyBeaver.info("App state: \(state) previous state: \(self.previousAppState)")
            
            switch state {
            case .loggedIn:
                break
            case .initial:
                self.mapLandingViewModel = nil
            case .home:
                if self.mapLandingViewModel == nil {    // MapLandding only need to init once
                    self.mapLandingViewModel = MapLandingViewModel()
                }
                if case .navigation = self.previousAppState {
                    self.navigationViewModel?.mobileMapViewUpdatable = nil
                    self.navigationViewModel?.carPlayMapViewUpdatable = nil
                    self.navigationViewModel?.notifyArrivalAction = nil
                    self.navigationViewModel = nil
                    self.navigationOverSpeed(value: false)
                    self.stopNavigationOverSpeedAnimation()
                }
                if case .cruise = self.previousAppState {
                    self.cruiseViewModel = nil
//                    self.mapLandingViewModel?.cruiseStoppedByUser() // previous state is cruise means it's ended by user
                    self.cruiseOverSpeed(value: false)
                    self.stopCruiseOverSpeedAnimation()

                }
            case .cruise(let etaFav):
                if self.cruiseViewModel == nil {
                    self.cruiseViewModel = CruiseViewModel(etaFav: etaFav)
                }
            case .routePlanning(_):
                //                if self.routePlanningViewModel == nil {
                //                    self.routePlanningViewModel = RoutePlanningViewModel()
                //                }
                if case .navigation = self.previousAppState {
                    self.navigationViewModel?.notifyArrivalAction = nil
                    self.navigationViewModel = nil
                }
            case .navigation(let deviceType, let service, let tripETA, let carpark):
                if self.navigationViewModel == nil {
#if targetEnvironment(simulator)
                    if let navService = service {
                        navService.simulationMode = .always
                        navService.simulationSpeedMultiplier = 2
                    }
#endif
                    if let navService = service {
                        self.navigationViewModel = TurnByTurnNavigationViewModel(navigationService: navService, tripETA: tripETA, from: deviceType, carpark: carpark)
                    }
                    
                }
            }
        })
    }
    
    // This only in mobile
    func enterLogin() {
        self.appState = .loggedIn
        self.previousAppState = .initial
    }
        
    func enterLogout() {
        self.previousAppState = self.appState
        self.appState = .initial
    }
    
    func enterHome() {        
        //self.previousAppState = self.appState
        if case .navigation(let deviceType,_,_,_) = self.appState {
            
            self.previousAppState = .navigation(deviceType, nil, nil, nil)
        }
        else{
            
            self.previousAppState = self.appState
        }
        
        self.appState = .home
    }
        
    func enterCruise(etaFav: Favourites?) {
        // To prevent from keeping the navigation service in memory
        // Normally this won't happen because enterHome() should happen first. But in our navigation we send navigationStop notification before the TurnByTurnNavigationVC dismissed then Cruise mode started
        if case .navigation(let deviceType,_,_,_) = self.appState {
            self.previousAppState = .navigation(deviceType, nil, nil, nil)
        } else{
            self.previousAppState = self.appState
        }
        self.appState = .cruise(etaFav)
    }
    
    func enterIntoRoutePlanning(from device: DeviceType) {
        self.previousAppState = self.appState
        self.appState = .routePlanning(device)
    }

    func enterIntoNavigation(from device: DeviceType, navigationService: MapboxNavigationService, with tripETA: TripETA?, to carpark: Carpark? = nil) {
        self.previousAppState = self.appState
        self.appState = .navigation(device, navigationService, tripETA, carpark)
    }
}

extension AppDelegate {
    func checkRivaHasAudioInQueue() -> Bool {
        guard let riva = rivaSpeech else { return false }
        return riva.hasAudioInQueue
    }
}


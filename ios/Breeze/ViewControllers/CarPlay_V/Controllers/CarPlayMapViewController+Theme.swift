//
//  CarPlayMapViewController+Theme.swift
//  Breeze
//
//  Created by Zhou Hao on 30/9/21.
//

import UIKit
import MapboxNavigation
import CarPlay
import MapboxCoreNavigation
import MapboxDirections
import MapboxMaps
import SwiftyBeaver

protocol CarPlayThemeUpdatable {
    func didThemeUpdate()
}

extension CarPlayThemeUpdatable where Self: UIViewController {
    func toggleDarkLightMode(with styleManager: MapboxNavigation.StyleManager?) {
        
        var styles = [CustomDayStyle()]
        if Settings.shared.theme == 0 {
            styles = isDarkMode ? [CustomNightStyle()] : [CustomDayStyle()]
        } else if Settings.shared.theme == 2 {
            styles = [CustomNightStyle()]
        }
        styleManager?.styles = styles
    }
}

extension CarPlayMapViewController: CarPlayThemeUpdatable {
    
    func didThemeUpdate() {
        toggleDarkLightMode(with: styleManager)
    }
    
    // MARK: - Theme updated
    public override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        guard previousTraitCollection != traitCollection else {
            return
        }
       toggleDarkLightMode(with: styleManager)
    }
}

extension CarPlayNavigationViewController: CarPlayThemeUpdatable {
    func didThemeUpdate() {
        toggleDarkLightMode(with: styleManager)
    }
    
    public override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        guard previousTraitCollection != traitCollection else {
            return
        }
        toggleDarkLightMode(with: styleManager)
    }
}

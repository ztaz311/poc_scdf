//
//  SplashVC.swift
//  Breeze
//
//  Created by Zhou Hao on 15/3/21.
//

import UIKit
import SwiftyGif
import Amplify
import AWSCognitoAuthPlugin
import CoreLocation
import ReachabilityManager
import Reachability
import AWSMobileClient
import Instabug
import SwiftyBeaver

class SplashVC: BaseViewController {
    
    // MARK: - Constants
    static let delay = 1.0
    
    // MARK: - IBOutlet
    @IBOutlet weak var retryButton: UIButton!
    
    // MARK: - Properties
    var globalFloatingView: FloatingView!
    var isSignedIn = false
    
    // MARK: - UIViewController life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        setupDarkLightAppearance(forceLightMode: true)
        
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        retryButton.addTarget(self, action: #selector(onRetry), for: .touchUpInside)
        _ =  remoteConfig.configValue(forKey: Values.download_link).stringValue ?? ""
        start()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.isHidden = true
        
        retryButton.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    @objc func onRetry() {
        self.start()
    }
    
    private func start() {
        
//        if let appdel = UIApplication.shared.delegate as? AppDelegate {
//            
//            appdel.instaBugCustomisation()
//        }
        
        ReachabilityManager.shared.isReachable {
            let status = LocationManager.shared.getStatus()
            if(status != .notDetermined && status != .denied) {
                self.sessionCheck()
            } else {
                LocationManager.shared.requestLocationAuthorization()
                LocationManager.shared.requestLocationAuthorizationCallback = { status in
                    self.locationAuthorization(status: status)
                }
            }
        } failure: {
            self.retryButton.isHidden = false
        }
    }    
    
    // Only call this after loading everything
    private func afterLoading(isSignedIn: Bool) {
        
        SwiftyBeaver.debug("afterLoading isSignedIn = \(isSignedIn)")
        self.isSignedIn = isSignedIn        

        // no matter user logged in or not, always show onboarding
        // delay for some time so that user can see the splash
        DispatchQueue.main.asyncAfter(deadline: .now() + SplashVC.delay) { [weak self] in
            guard let self = self else { return }
            
            let isOnboardingShown = Prefs.shared.getBoolValue(.isOnboardingShown)
            if !isOnboardingShown {
                self.showOnboarding()
            } else {
                self.hideOnboardingScreen()
            }
        }
    }
        
    @objc func showMapLoadingVC() {
/*
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc: UIViewController = storyboard.instantiateViewController(withIdentifier: "RootNavigationController") as UIViewController
        
        if let window = UIApplication.shared.windows.first {
            window.rootViewController = vc
            window.makeKeyAndVisible()
            
            let options: UIView.AnimationOptions = .transitionCrossDissolve

            UIView.transition(with: window, duration: 0.3, options: options, animations: {}, completion: nil)
        }
 */
        
        let storyboard = UIStoryboard(name: "MapsLandingVC", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "mainMap") as UIViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func showLoginVC() {
        
        AWSAuth.sharedInstance.signOut()
        
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as UIViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func showOnboarding() {
        let storyboard: UIStoryboard = UIStoryboard(name: "Onboarding", bundle: nil)
        let vc: OnboardingVC = storyboard.instantiateViewController(withIdentifier: "OnboardingVC") as! OnboardingVC
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
                
    func setFloatingButtons(){
        
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
        let window = UIApplication.shared.keyWindow
        
        if let foundView = UIApplication.shared.keyWindow?.viewWithTag(200) {
            foundView.removeFromSuperview()
        }
        globalFloatingView = FloatingView(frame: CGRect(x: screenWidth-90, y: (window?.safeAreaInsets.top)! + 16, width: 90, height: 120))

        globalFloatingView.tag = 200
        window?.addSubview(globalFloatingView)
        
        let panGesture = CustompanGestureRecognizer(target: self, action: #selector(floatingViewGestureHandler))
        panGesture.floatingView = globalFloatingView
        globalFloatingView.addGestureRecognizer(panGesture)
        
        
        globalFloatingView.feedbackBtn.addTarget(self, action: #selector(feedbackTapped), for: .touchUpInside)
        globalFloatingView.courseList.addTarget(self, action: #selector(tileListTapped), for: .touchUpInside)
        
    }
    
    // MARK: - location authorization
    func locationAuthorization(status: CLAuthorizationStatus) {
        
        //Swift
        switch status {
        case .notDetermined:
            break
        case .authorizedAlways:
            self.sessionCheck()
        case .authorizedWhenInUse:
            self.sessionCheck()
        case .restricted:
            break
        case .denied:
            break
        @unknown default:
            break
        }
    }
    
    // MARK: - Auth related
    func sessionCheck() {
    
//        self.getAuthSession()
        
        let didLaunchAppFirstTime = Prefs.shared.getBoolValue(.didLaunchAppFirstTime)
        //  Check if user refresh install new version 5.0.0 then force user sign out otherwise should continue the prev session
        if(didLaunchAppFirstTime == false){
            //  Specify check for guest mode in case user kill app at T&C or in set username screen then dont force sign out user
            if AWSAuth.sharedInstance.isGuestMode() {
                let isAppDidLaunch = Prefs.shared.getBoolValue(.isAppDidLaunch)
                if isAppDidLaunch == false {
                    Prefs.shared.setValue(true, forkey: .isAppDidLaunch)
                    AWSAuth.sharedInstance.forceSignOut { success in
                        self.getAuthSession()
                    }
                }else {
                    self.getAuthSession()
                }
            }else {
                //  Need force signout
                AWSAuth.sharedInstance.forceSignOut { success in
                    self.getAuthSession()
                }
            }
        }
        else
        {
            self.getAuthSession()
        }
    }
    
    func getAuthSession(){
        
        AWSAuth.sharedInstance.fetchCurrentSession(isLaunchingApp: true) { [weak self] (session) in
            
            guard let self = self else { return }
            
            if let session = session as? AWSAuthCognitoSession {
                
                if session.isSignedIn {
                    
                    AWSAuth.sharedInstance.fetchAttributes { result in
                        
                        //  Requirement change force signout user if user not yet set a username
                        if AWSAuth.sharedInstance.userName.isEmpty {
                            //  Force user to
                            AWSAuth.sharedInstance.signOut()
                            self.afterLoading(isSignedIn: false)
                        } else {
                            self.setUserPrefEmpty()
                        }
                        
                        /*
                        if AWSAuth.sharedInstance.isGuestMode() {
                            self.checkAcceptedTCInSignin()
                        }else {
                            if(AWSAuth.sharedInstance.userName == "")
                            {
                                DispatchQueue.main.async {
                                    let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                    let vc: SetUserNameVC = storyboard.instantiateViewController(withIdentifier: "SetUserNameVC")  as! SetUserNameVC
                                    self.navigationController?.pushViewController(vc, animated: true)
                                }
                            }
                            else
                            {
                                self.setUserPrefEmpty()
                            }
                        }*/
                        
                        return
                        
                    } onFailure: { error in
                        self.afterLoading(isSignedIn: false)
                    }
                    
                } else {
                    self.afterLoading(isSignedIn: false)
                }
            }
        } onFailure: { (error) in
            self.afterLoading(isSignedIn: false)
            SwiftyBeaver.error(error.localizedDescription)
        }
    }
    
    private func checkAcceptedTCInSignin() {
        AWSAuth.sharedInstance.getAcceptTCStatus { [weak self] isSuccess in
            guard let self = self else { return }
            DispatchQueue.main.async {
                if (isSuccess && !AWSAuth.sharedInstance.acceptTC) {
                    let storyboard: UIStoryboard = UIStoryboard(name: "Web", bundle: nil)
                    let vc: WebVC = storyboard.instantiateViewController(withIdentifier: "WebVC") as! WebVC
                    vc.isAccountCreation = true
                    vc.urlString = Configuration.termsOfUseURL
                    vc.delegate = self
                    vc.type = .onlyShow
                    self.navigationController?.pushViewController(vc, animated: true)
                    return
                }
                self.pushScreenAfterSignin()
            }
        }
    }
    
    private func pushScreenAfterSignin() {
        DispatchQueue.main.async {
            if(AWSAuth.sharedInstance.userName == "") {
                DispatchQueue.main.async {
                    let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc: SetUserNameVC = storyboard.instantiateViewController(withIdentifier: "SetUserNameVC")  as! SetUserNameVC
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            } else {
                self.setUserPrefEmpty()
            }
        }
    }
    
    func setUserPrefEmpty() {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) { [self] in
            triggerResetAmenities { _ in
                
                DispatchQueue.main.async { [weak self] in
                    guard let self = self else { return }
                    
                    self.afterLoading(isSignedIn: true)
                }
            }
        }
    }    
}

extension SplashVC: WebVCDelegate {
    func didAccepted() {
        
        AWSAuth.sharedInstance.changeAcceptTC { [weak self] success in
            guard let self = self else { return }
            // Update value to server
            if success {
                self.pushScreenAfterSignin()
            }
        }
    }
    
    func didDeclined() {
        AWSAuth.sharedInstance.forceSignOut { success in
            if success {
                appDelegate().forceSignoutUser()
            }
        }
    }
    
    func didBacked() {
        
    }
}

extension SplashVC: OnboardingVCDelegate{
    func hideOnboardingScreen() {
        
        if isSignedIn {
            BugReporting.bugReportingOptions = [.emailFieldHidden]

            //If onBoarding state is not completed then open OnBoaridngRN screen
//            if(AWSAuth.sharedInstance.onboardingState == nil || AWSAuth.sharedInstance.onboardingState == UserOnboardingState.ONBOARDING_STEP_1){
//
//                let state = AWSAuth.sharedInstance.onboardingState == nil ? UserOnboardingState.ONBOARDING_INIT : UserOnboardingState.ONBOARDING_STEP_1
//               let params = [ // Object data to be used by that screen
//                    "onboardingState":state,
//                    "baseURL": appDelegate().backendURL,
//                    "idToken":AWSAuth.sharedInstance.idToken,
//                    "deviceos":parameters!["deviceos"],
//                    "deviceosversion":parameters!["deviceosversion"],
//                    "devicemodel":parameters!["devicemodel"],
//                    "appversion":parameters!["appversion"],
//               ]
//                self.goToRNScreen(toScreen: RNScreeNames.FIRST_APP_TUTORIAL,navigationParams: params as [String:Any])
//            }
//            else{
                self.showMapLoadingVC()
//            }
            
        } else {
            BugReporting.bugReportingOptions = [.none]
            self.showLoginVC()
        }

    }
}

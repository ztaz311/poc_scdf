//
//  CollectionDetailViewModel.swift
//  Breeze
//
//  Created by Tuyen, Le Xuan  on 27/07/2022.
//

import Foundation
import Combine

final class CollectionDetailViewModel {
    var collectionId: Int
    var rntag: String
    var collectionName: String
    private(set) var bookMarks: [Bookmark] = []
    
    let bookmarksUpdatedAction = PassthroughSubject<Void, Never>()
    
    init(collectionId: Int, rntag: String, collectionName: String) {
        self.collectionId = collectionId
        self.rntag = rntag
        self.collectionName = collectionName
    }
    
    // Return false if same
    func setData(bookMarks: [Bookmark]) -> Bool {
        if self.compareBookmark(bookMarks: bookMarks) {
            return false
        }
        self.bookMarks = bookMarks
        bookmarksUpdatedAction.send()
        return true
    }
    
    func selectedAmenity(id: String, isSelected: Bool) {
        for bookMark in bookMarks {
            if bookMark.bookmarkId == Int(id) {
                bookMark.isSelected = isSelected
                return
            }
        }
    }
    
    func updateCodeForNewBookMarks(){
        
        for bookMark in bookMarks {
            bookMark.code = Values.NEWBOOKMARKS
        }
    }
    
    func compareBookmark(bookMarks: [Bookmark]) -> Bool {
        if bookMarks.count != self.bookMarks.count {
            return false
        }
        for i in 0..<bookMarks.count {
            if bookMarks[i] != self.bookMarks[i] {
                return false
            }
        }
        return true
    }
    
    func clearBookMarks(){
        
        if bookMarks.count > 0{
            bookMarks.removeAll()
        }
    }
}

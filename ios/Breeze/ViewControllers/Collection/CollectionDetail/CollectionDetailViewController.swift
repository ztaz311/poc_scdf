//
//  CollectionDetailViewController.swift
//  Breeze
//
//  Created by Tuyen, Le Xuan  on 27/07/2022.
//

import UIKit
@_spi(Restricted) import MapboxMaps
import MapboxCoreMaps
import MapboxNavigation
import Combine
import SwiftyBeaver

class CollectionDetailViewController: BaseViewController {
    
    @IBOutlet weak var mapSavedView: UIView! {
        didSet {
            mapSavedView.isHidden = true
            mapSavedView.layer.cornerRadius = 24
            mapSavedView.layer.masksToBounds = true
            mapSavedView.layer.shadowColor = UIColor.black.withAlphaComponent(0.5).cgColor
            mapSavedView.layer.shadowOpacity = 1
            mapSavedView.layer.shadowRadius = 24
            mapSavedView.layer.shadowOffset = CGSize(width: 4.0, height: 4.0)
        }
    }
    @IBOutlet weak var mapSavedLabel: UILabel! {
        didSet {
            mapSavedLabel.font = UIFont.fontWithName(fontFamilySFPro.Regular, size: 18)
            if appDelegate().isDarkMode() {
                mapSavedLabel.textColor = .white
            }
        }
    }
    
    var screenCode: String = ""
    var collectionDesc: String = ""
    var errorMessage: String = ""
    var isSavingCollection: Bool = false
    var bookmarkId: Int = 0
    
    var anyCancelable: Set<AnyCancellable> = Set<AnyCancellable>()
    private var disposables = Set<AnyCancellable>()
    private(set) var contentViewModel: ContentViewModel?
    var searchModel: SearchResult?
    
    override var trackingScreenName: String {
        switch screenCode {
        case "SHORTCUTS":
            return ParameterName.CollectionDetailStar.saved_collection_shortcut
        case "SAVED_PLACES":
            return ParameterName.CollectionDetailStar.saved_collection_my_saved_places
        default:
            return ParameterName.CollectionDetailStar.saved_collection_custom
        }
    }
        
    var fromScreen:QueryRenderFromScreen{
        
        switch screenCode {
        case "SHORTCUTS":
            return .saved_collection_shortcut
        case "SAVED_PLACES":
            return .saved_collection_my_saved_places
        default:
            return .saved_collection_custom
        }
    }
    
    var analyticScreen: String {
        switch screenCode {
        case "SHORTCUTS":
            return ParameterName.CollectionDetail.shortcut_collection
        case "SAVED_PLACES":
            return ParameterName.CollectionDetail.my_saved_places_collection_screen
        default:
            return ParameterName.CollectionDetail.custom_saved_places_collection
        }
    }
    
    private(set) var viewModel: CollectionDetailViewModel! {
        didSet {
            bindViewModel()
        }
    }
    
    var navMapView: NavigationMapView! {
        didSet {
            if navMapView != nil {
                setupMapView()
            }
        }
    }
    
    private lazy var pointAnnotationManager: PointAnnotationManager = {
        return navMapView.mapView.annotations.makePointAnnotationManager()
    }()
    
    var isTrackingUser = true {
        didSet {
            userLocationButton?.isHidden = isTrackingUser
        }
    }
    
    private var userLocationButton: UserLocationButton!
    private var bottomViewController: UIViewController!
    
    //  MARK: - DROP PIN
    var dropPinModel: DropPinModel? {
        didSet {
            if let pinModel  = self.dropPinModel {
                
            } else {
                removePinLocation()
            }
        }
    }
    
    private func isSearching() -> Bool {
        return (self.searchModel != nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        contentViewModel = ContentViewModel()
        contentViewModel?.delgate = self
        contentViewModel?.bottomSheetHeight = 500
        
        self.navigationController?.isNavigationBarHidden = true
        
        addNotification()
        
        navMapView = NavigationMapView(frame: view.bounds)
        
        if !errorMessage.isEmpty {
            DispatchQueue.main.async {
                self.showAlert(title: "", message: self.errorMessage, confirmTitle: Constants.gotIt, handler: nil)
            }
        }
        
        Prefs.shared.removeValueForKey(.latSavePlace)
        Prefs.shared.removeValueForKey(.longSavePlace)

        NotificationCenter.default.addObserver(self, selector: #selector(onReceivedSearchLocation(_:)), name: .onReceivedSearchLocationInCollectionDetail, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(onClearSearchCollectionDetails), name: .onClearSearchCollectionDetails, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    @objc func onClearSearchCollectionDetails() {
        //  Remove destination search location on map
        resetDropPinAndSearch()
    }
    
    func resetDropPinAndSearch() {
        self.dropPinModel = nil
        self.resetCamera()
    }
    
    @objc func onReceivedSearchLocation(_ notification: Notification) {
        if let locationDic = notification.object as? [String: Any] {
            //  On receive search location then auto remove previous drop pin
            self.searchModel = SearchResult(locationDic)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                if let search = self.searchModel {
                    self.dropPinModel = search.getDropPinModel()
                    self.openToolTipWhenReceivedSearch()
                }
            }
        }
    }
    
    func setCollectionId(_ collectionId: Int, rntag: String, collectionName: String) {
        viewModel = CollectionDetailViewModel(collectionId: collectionId, rntag: rntag, collectionName: collectionName)
    }
    
    override var useDynamicTheme: Bool {
        return true
    }
    
    private func setupView() {
        let accessToken = ResourceOptionsManager.default.resourceOptions.accessToken
        
        guard !accessToken.isEmpty else {
            fatalError("Empty access token")
        }
        
        bottomViewController = getRNViewBottomSheet(toScreen: viewModel.rntag,
                                                    navigationParams: [Values.COLLECTIONID: viewModel.collectionId,
                                                                       Values.TORNSCREEN : viewModel.rntag,
                                                                       Values.COLLECTION_NAME: viewModel.collectionName,
                                                                       Values.COLLECTION_DESC: collectionDesc,
                                                                       Values.COLLECTION_CODE: screenCode,
                                                                       Values.BOOKMARK_ID: bookmarkId])
        
        bottomViewController.view.frame = UIScreen.main.bounds;
        
        addChildViewControllerWithView(childViewController: bottomViewController,toView: self.view)
        
    }
    
    private func addNotification() {
        
        let arr = [Values.NotificationCloseCollectionDetail,
                   Values.NotificationUpdateCollectionDetail,
                   Values.NotificationShowToast]
        for name in arr {
            NotificationCenter.default.publisher(for: Notification.Name(name), object: nil)
                .receive(on: DispatchQueue.main)
                .sink { [weak self] notification in
                    guard let self = self else { return }
                    self.processNotification(name: name, info: notification.userInfo as NSDictionary?)
                }.store(in: &anyCancelable)
        }
        
        contentViewModel!.onAdjustCenter = { [weak self] (y, index) in
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                
                self.updateLocationButton(y: y)
                
                if self.dropPinModel == nil {
                    if let contentViewModel = self.contentViewModel {
                        contentViewModel.resetCameraBasedOnBottomSheet()
                    }
                }
            }
        }
        
        contentViewModel!.$isTrackingUser
            .receive(on: DispatchQueue.main)
            .sink(receiveValue: { [weak self] isTrackingUser in
                guard let self = self else { return }
                self.isTrackingUser = isTrackingUser
        }).store(in: &disposables)
    }
    
    private func processNotification(name: String, info: NSDictionary?) {
        switch name {
        case Values.NotificationCloseCollectionDetail:
            self.navigationController?.popViewController(animated: true)
        case Values.NotificationUpdateCollectionDetail:
            self.processData(info)
        case Values.NotificationShowToast:
            self.processShowToast(info)
        default:
            break
        }
    }
    
    private func processShowToast(_ info: NSDictionary?) {
        guard let message = info?["message"] as? String else {
            return
        }
        
        showToast(from: self.view, message: message, topOffset: 20, icon: "greenTick", messageColor: .black, duration: 5)
    }
    
    private func processData(_ info: NSDictionary?) {
        guard let data = info?["bookmarks"], let fromScreen = info?["fromScreen"] as? String,
        let json = try? JSONSerialization.data(withJSONObject: data, options: .prettyPrinted),
              let bookmarks = try? JSONDecoder().decode([Bookmark].self, from: json) else { return }
        
        switch fromScreen {
        case Values.Collection_From_Screen_Details, Values.Collection_From_Screen_Shortcut:
            setData(bookMarks: bookmarks)
        case Values.CollectionDetailsBookmarkEditor:
            viewModel.clearBookMarks()
            if (viewModel.setData(bookMarks: bookmarks)){
                viewModel.updateCodeForNewBookMarks()
                contentViewModel?.updateBookMark(bookmarks: viewModel.bookMarks)
            }
            
        default:
            break
        }
    }
    
    private func showLocationOnly(bookMarks: [Bookmark]) {
        guard let location = bookMarks.first?.location,
              let name = bookMarks.first?.address1 else {
            return
        }
        self.navMapView.mapView.mapboxMap.setCamera(to: CameraOptions(center: location, zoom: 13))
        
        //self.mapView.camera.setCamera(to: CameraOptions(center: coordinate, zoom: 13), animated: true, completion: nil)
        
        self.setupAnnotation(location: location, name: name)
    }
    
    private func setupAnnotation(location: CLLocationCoordinate2D, name: String) {
        pointAnnotationManager.annotations = []
        var customPointAnnotation = PointAnnotation(coordinate: location)
        
        let traitCollection = getTraitCollection()
        customPointAnnotation.image = .init(image: UIImage(named: "collection", in: nil, compatibleWith: traitCollection)!, name: "CallOut")
        pointAnnotationManager.annotations = [customPointAnnotation]
    }
    
    private func showSavedCollection() {
        if isSavingCollection && errorMessage.isEmpty {
            
            self.view.bringSubviewToFront(mapSavedView)
            
            mapSavedView.isHidden = false
            mapSavedView.alpha = 0
            UIView.animate(withDuration: 0.3, delay: 1) { [weak self] in
                guard let self = self else { return }
                self.mapSavedView.alpha = 1
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                self.mapSavedView.alpha = 1
                UIView.animate(withDuration: 0.5, delay: 0) { [weak self] in
                    guard let self = self else { return }
                    self.mapSavedView.isHidden = true
                    self.mapSavedView.alpha = 1
                }
            }
        }
    }
    
    private func setupMapView() {
        view.addSubview(navMapView)
        navMapView.isUserInteractionEnabled = true
        
        navMapView.translatesAutoresizingMaskIntoConstraints = false
        
        navMapView.mapView.gestures.delegate = self
        
        navMapView.snp.makeConstraints { (make) in
            make.bottom.equalToSuperview()
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.top.equalToSuperview()
        }
        
        navMapView.mapView.mapboxMap.onNext(event: .styleLoaded) { [weak self] _ in
            guard let self = self else { return }
            
            self.contentViewModel?.mobileContentMapViewUpdatable = ContentViewUpdatable(navigationMapView: self.navMapView)
            self.setupUI()
            self.setupView()
            self.addImage()
            self.bindBookmark()
            self.showSavedCollection()
        }
        
        navMapView.mapView.mapboxMap.onEvery(event: .cameraChanged) { [weak self] _ in
            guard let self = self else { return }
            self.adjustCallout()
        }
        
        //setupPuck()
        toggleDarkLightMode()
    }
    
    private func bindBookmark() {
        viewModel.bookmarksUpdatedAction
            .sink { [weak self] in
                guard let self = self else { return }
                self.updateBookMark()
            }.store(in: &anyCancelable)
    }
    
    private func addImage() {
        let arr = [Values.COLLECTION,
                   Values.COLLECTIONHOME,
                   Values.COLLECTIONWORK,
                   Values.COLLECTIONNORMAL,
                   Values.NEWBOOKMARKS]
        navMapView.mapView.addBookmarkImagesToMapStyle(types: arr)
    }
    
    private func updateBookMark() {
        
    }
    
    private func setupPuck() {
        //beta.14 change
        
        var traitCollection = UITraitCollection(userInterfaceStyle: .light)
        if(Settings.shared.theme == 0){
            
            if(appDelegate().isDarkMode()){
                
                traitCollection = UITraitCollection(userInterfaceStyle: .dark)
                
            }
            
        }
        else{
            
            traitCollection = UITraitCollection(userInterfaceStyle: UIUserInterfaceStyle(rawValue: Settings.shared.theme) ?? .unspecified)
        }
        
        let puck2d = Puck2DConfiguration(topImage: UIImage(named: "puckArrow",in:nil,compatibleWith: traitCollection), bearingImage: UIImage(named: "puckIcon"), shadowImage: nil, scale: nil)
        
        navMapView.userLocationStyle = .puck2D(configuration: puck2d)
        navMapView.mapView.ornaments.options.compass.visibility = .hidden
        navMapView.mapView.ornaments.options.attributionButton.visibility = .hidden
    }
    
    private func bindViewModel() {
        
    }
    
    private func adjustCallout() {
        
        contentViewModel?.adjustCallout()
    }
    
    // MARK: - Theme updated
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        guard previousTraitCollection != traitCollection else {
            return
        }
        toggleDarkLightMode()
    }
    
    private func toggleDarkLightMode() {
        navMapView.mapView.mapboxMap.style.uri = StyleURI(url: URL(string: isDarkMode ? Constants.Map.basicNightStyle: Constants.Map.basicStyle)!)
    }
    
    func setData(bookMarks: [Bookmark],fromScreen:String = "") {
        if viewModel.setData(bookMarks: bookMarks) {
            
            resetDropPinAndSearch()
            
            if let firstBookmark = bookMarks.first(where: {
                return $0.isSelected ?? false
            }) {
                DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1)) {
                    
                    self.contentViewModel?.showTooltipfor(bookmark: firstBookmark,fromScreen: self.fromScreen, view: self.view)
                    
                }
            } else {
                DispatchQueue.main.async {
                    self.contentViewModel?.deselectTooltip()
                    self.contentViewModel?.hideParking()
                }
            }
            
            contentViewModel?.updateBookMark(bookmarks: bookMarks)
            
        }
        pointAnnotationManager.annotations = []
    }
    
    @objc func locationButtonTapped(sender: UserLocationButton) {
        resetCamera()
    }
    
    private func resetCamera() {
        
        // TODO: Analytics
        //        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Home.UserClick.recenter, screenName: ParameterName.Home.screen_view)
        isTrackingUser = true
        
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            
            if let contentViewModel = self.contentViewModel{
                
                contentViewModel.resetCamera()
            }
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: .onReceivedSearchLocationInCollectionDetail, object: nil)
    }
}

extension CollectionDetailViewController{
    
    private func setupUI() {
        
        setupLocationButton()
    }
    
    private func updateLocationButton(y: Int) {
        userLocationButton.snp.updateConstraints { make in
            make.bottom.equalToSuperview().offset(-(CGFloat(y) + 20))
        }
    }
    
    
        
    private func setupLocationButton() {
        let userLocationButton = UserLocationButton(buttonSize: (Images.locationImage?.size.width)!)
        userLocationButton.addTarget(self, action: #selector(locationButtonTapped), for: .touchUpInside)
        
        userLocationButton.translatesAutoresizingMaskIntoConstraints = false
        navMapView.mapView.addSubview(userLocationButton)
        userLocationButton.snp.makeConstraints { make in
            make.trailing.equalTo(self.view.safeAreaLayoutGuide.snp.trailing).offset(-20)
            // TODO: Need to adjust when bottom sheet is ready
            make.bottom.equalToSuperview().offset(-(322 + 20))
        }

        self.userLocationButton = userLocationButton
        userLocationButton.isHidden = true // hide it by default
    }
}

extension CollectionDetailViewController: GestureManagerDelegate {
    
    func gestureManager(_ gestureManager: GestureManager, didEnd gestureType: GestureType, willAnimate: Bool) {
        
        //Not required to handle this gesture
    }
    
    func gestureManager(_ gestureManager: GestureManager, didEndAnimatingFor gestureType: GestureType) {
        
        //Not required to handle this gesture
    }
    
    func gestureManager(_ gestureManager: GestureManager, didBegin gestureType: GestureType) {
        
        var name = ParameterName.CollectionDetail.MapInteraction.drag
        switch gestureType {
        case .pinch:
            name = ParameterName.CustomMap.MapInteraction.pinch
        case .singleTap:
            let point = gestureManager.singleTapGestureRecognizer.location(in: self.navMapView)
            if let contentViewModel = contentViewModel {
                contentViewModel.queryFeaturesOnTap(point: point,view: self.view,fromScreen: self.fromScreen) { [weak self] needAddDropPin, shouldRemovePreviousPin in
                    guard let self = self else { return }
                    if needAddDropPin {
                        
                        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CollectionDetail.UserClick.tap_map_drop_pin, screenName: self.analyticScreen)
                        
                        //  Handle show location pin tool tip when user tap on map
                        let pinLocation = self.navMapView.mapView.mapboxMap.coordinate(for: point)
                        self.getDropPinLocationDetails(pinLocation) {[weak self] droppin in
                            if let dropPinModel = droppin {
                                
                                //  Reset search model
                                self?.searchModel = nil
                                
                                //  Show drop pin location
                                self?.dropPinModel = dropPinModel
                                self?.checkDropPinBookmark()
                            }
                        }
                    } else {
                        
                    }

                    if shouldRemovePreviousPin {
                        self.dropPinModel = nil
                    }
                }
            }
            
        default:
            name = ParameterName.CustomMap.MapInteraction.drag
        }
        AnalyticsManager.shared.logMapInteraction(eventValue: name, screenName: self.trackingScreenName)
        
        if gestureType == .pan || gestureType == .pinch || gestureType == .doubleTapToZoomIn || gestureType == .doubleTouchToZoomOut {
            isTrackingUser = false
        }
    }
}


extension CollectionDetailViewController: ContentViewModelDelegate {
    
    func closeDropPin() {
        //  Should toggle off droppin mode
        self.dropPinModel = nil
    }
    
    func toolTipOpen() {
        
        if let contentViewModel = self.contentViewModel {
            contentViewModel.showParking()
        }
    }
    
    func dismissTBRBeforeAddingNew(){
        //Not needed
        
    }
    
    func navigateHere(address:SearchAddresses?, carPark:Carpark?, layerCode: String?, amenityId: String?, amenityType: String?, amenityCarPark: String?) {
        
        let storybard = UIStoryboard(name: "NewRoutePlanning", bundle: nil)
        if let vc = storybard.instantiateViewController(withIdentifier: String(describing: NewRoutePlanningVC.self)) as? NewRoutePlanningVC {
            vc.originalAddress = address
            vc.addressReceived = true
            vc.carparkId = amenityCarPark
            if let carPark = carPark {
                vc.selectedCarPark = carPark
            }
            vc.bookmarkId = amenityId
            vc.bookmarkName = address?.address1
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
            
    }
    func toolTipMoreInfo(poi:POI?,carPark:Carpark?,screenName:String){
        let lat = Prefs.shared.getStringValue(.latSavePlace)
        let long = Prefs.shared.getStringValue(.longSavePlace)
        
        if let carPark = carPark {
            let carParkData = screenName == "ParkingCalculator" ? [
                "toScreen": screenName, // Screen name you want to display when start RN
                "navigationParams": [ // Object data to be used by that screen
                    "parkingID": carPark.id ?? "",
                    "fromNative":true,
                    Values.FOCUSED_LAT: lat,
                    Values.FOCUSED_LONG: long
                                    ],
            ] as [String : Any] :[
                "toScreen": screenName, // Screen name you want to display when start RN
                "navigationParams": [ // Object data to be used by that screen
                    "parkingId": carPark.id,
                    Values.FOCUSED_LAT: lat,
                    Values.FOCUSED_LONG: long
                                    ],
            ] as [String : Any]
            let rootView = RNViewManager.sharedInstance.viewForModule(
                "Breeze",
                initialProperties: carParkData)
            
            let reactNativeVC = UIViewController()
            reactNativeVC.view = rootView
            reactNativeVC.modalPresentationStyle = .pageSheet
            self.present(reactNativeVC, animated: true, completion: nil)
        }
    }
    
    func selectedAmenity(id: String, isSelected: Bool) {
        if id.count == 0 {
            return
        }
        viewModel.selectedAmenity(id: id, isSelected: isSelected)
        sendSelectActionToRN(id: id, selected: isSelected)
        
        if let contetModel = self.contentViewModel{
            
            contetModel.deSelectBookMarkAmenityOnMap(bookmarks: viewModel.bookMarks)
        }
    }
    
    func startWalkingPath(poi:POI?, trackNavigation: Bool, amenityId: String?, name: String?, canReroute: Bool?) {
        //no need
    }
    
}

extension CollectionDetailViewController {
    
    func goToSavedMapCollection() {
        let _ = ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.OPEN_SAVED_FROM_NATIVE, data: [:]).subscribe(onSuccess: {_ in }, onFailure: {_ in })
    }
}

extension CollectionDetailViewController {
    
    private func getDropPinLocationDetails(_ location: CLLocationCoordinate2D, completion: @escaping (DropPinModel?) -> ()) {
        getPinLocationDetails(location: CLLocation(latitude: location.latitude, longitude: location.longitude)) { placemark in
            if let foundLocation = placemark {
                print("getDropPinLocationDetails: name: \(foundLocation.name ?? "") address: \(foundLocation.formattedAddress ?? "")")
                let name: String = foundLocation.name ?? ""
                let address = (foundLocation.formattedAddress ?? "").replacingOccurrences(of: "\n", with: ", ")
                
                if !name.isEmpty && !address.isEmpty {
                    SwiftyBeaver.debug("Address not found")
                }
                let dropPin = DropPinModel(name, address: address, lat: location.latitude.toString(), long: location.longitude.toString())
                completion(dropPin)
            } else {
                completion(nil)
            }
        }
    }
    
    private func checkDropPinBookmark() {
        self.addPinLocation()
    }
    
    private func addPinLocation() {
        if let mapView = navMapView.mapView, let pinModel = self.dropPinModel {
            //  Remove ole location pin before add new one
            removePinLocation()
            
            let feature = getPinLocationFeature(pinModel)
            let featureCollection = FeatureCollection(features: [feature])
            mapView.addDropPinLocation(features: featureCollection, type: Values.DROPPIN, isAbovePuck: true, isSearch: pinModel.isSearch)
            
            let location = CLLocationCoordinate2D(latitude: Double(pinModel.lat) ?? 0.0, longitude: Double(pinModel.long) ?? 0.0)
            self.contentViewModel?.showDropPinToolTip(feature, location: location, view: self.view, fromScreen: self.fromScreen)
            
            //  Sending data to RN for saving pin location
            if !isSearching() {
                toggleDropPinDetailCollection(pinModel, isDropPin: true)
            }
        }
    }
    
    private func removePinLocation() {
        if !isSearching() {
            toggleDropPinDetailCollection(nil, isDropPin: false)
        }
        if let mapView = navMapView.mapView {
            mapView.removeDropPinLayer()
        }
    }
    
    private func openToolTipWhenReceivedSearch() {
        if let mapView = navMapView.mapView, let pinModel = self.dropPinModel {
            mapView.removeDropPinLayer()
            let feature = getPinLocationFeature(pinModel)
            let featureCollection = FeatureCollection(features: [feature])
            mapView.addDropPinLocation(features: featureCollection, type: Values.DROPPIN, isAbovePuck: true, isSearch: pinModel.isSearch)
            
            let location = CLLocationCoordinate2D(latitude: Double(pinModel.lat) ?? 0.0, longitude: Double(pinModel.long) ?? 0.0)
            self.contentViewModel?.showDropPinToolTip(feature, location: location, view: self.view, fromScreen: self.fromScreen)
        }
    }
    
    private func getPinLocationFeature(_ model: DropPinModel) -> Turf.Feature {
        let coordinate = CLLocationCoordinate2D(latitude: Double(model.lat) ?? 0.0, longitude: Double(model.long) ?? 0.0)
        var feature = Turf.Feature(geometry: .point(Point(coordinate)))
        
        var properties: JSONObject = [
            "type":.string(Values.DROPPIN),
            "name":.string(model.name ?? ""),
            "address":.string(model.address ?? ""),
            "isBookmarked": .boolean(false),
            "lat": .string(model.lat),
            "long": .string(model.long),
            "isSearch": .boolean(model.isSearch),
            "isDetailCollection": .boolean(true),
            "analyticScreen": .string(analyticScreen),
            "address2": .string(model.address2 ?? ""),
            "fullAddress": .string(model.fullAddress ?? "")
        ]
        
        if let amenityId = model.amenityId, !amenityId.isEmpty {
            properties["amenityId"] = .string(amenityId)
        }
        
        feature.properties = properties
        return feature
    }
}

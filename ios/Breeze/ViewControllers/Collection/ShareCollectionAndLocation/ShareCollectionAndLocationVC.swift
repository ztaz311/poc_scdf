//
//  ShareCollectionAndLocationVC.swift
//  Breeze
//
//  Created by Tuan, Pham Hai on 17/11/2023.
//

import UIKit
@_spi(Restricted) import MapboxMaps
import MapboxCoreMaps
import MapboxNavigation
import Combine
import SwiftyBeaver

enum SharingMode: String {
    case collection
    case location
}

class ShareCollectionAndLocationVC: BaseViewController {
    var anyCancelable: Set<AnyCancellable> = Set<AnyCancellable>()
    private var disposables = Set<AnyCancellable>()
    private(set) var contentViewModel: ContentViewModel?
    
    var locationData: SaveLocationDataModel?
    var bookmarkId: Int?
    var isShareLocationSelected: Bool = true
    var sharingMode: SharingMode?
    var token: String?
    
    var collectionName: String = ""
    var collectionDesc: String = ""
    var screenCode: String = ""
    var selectedIndex: Int = -1 {
        didSet {
            //  Show tooltip of bookmark at index
            if selectedIndex < 0 {
                self.dismissToolTip()
            }
        }
    }
    
    private let types = [Values.COLLECTION,
                         Values.COLLECTIONHOME,
                         Values.COLLECTIONWORK,
                         Values.COLLECTIONNORMAL,
                         Values.NEWBOOKMARKS,
                         Values.SHARE_CARPARK_FULL,
                         Values.SHARE_CARPARK_FILLING_UP,
                         Values.SHARE_CARPARK_AVAILABLE,
                         Values.SHARE_BOOKMARK_ICON,
                         Values.SHARE_EV_ICON,
                         Values.SHARE_PETROL_ICON,
                         Values.SHARE_HOME_LOCATION,
                         Values.SHARE_WORK_LOCATION,
                         Values.SHARE_PETROL_BOOKMARKED,
                         Values.SHARE_EV_BOOKMARKED,
                         Values.SHARE_CARPARK_FULL_BOOKMARKED,
                         Values.SHARE_CARPARK_FILLING_UP_BOOKMARKED,
                         Values.SHARE_CARPARK_AVAILABLE_BOOKMARKED,
                         Values.SHARE_DESTINATION_ICON
    ]
    
    var fromScreen: QueryRenderFromScreen = .shareCollectionAndLocation
    
    private(set) var viewModel: ShareCollectionAndLocationViewModel! {
        didSet {
            bindViewModel()
        }
    }
    
    var navMapView: NavigationMapView! {
        didSet {
            if navMapView != nil {
                setupMapView()
            }
        }
    }
    
    private lazy var pointAnnotationManager: PointAnnotationManager = {
        return navMapView.mapView.annotations.makePointAnnotationManager()
    }()
    
    var isTrackingUser = true {
        didSet {
            userLocationButton?.isHidden = isTrackingUser
        }
    }
    
    private var userLocationButton: UserLocationButton!
    private var bottomViewController: UIViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //  TODO send shared token to RN
        contentViewModel = ContentViewModel()
        contentViewModel?.delgate = self
        contentViewModel?.bottomSheetHeight = 500
        
        addNotification()
        
        self.navigationController?.isNavigationBarHidden = true
        
        navMapView = NavigationMapView(frame: view.bounds)
        
    }
    
    override var useDynamicTheme: Bool {
        return true
    }
    
    private func addNotification() {
        
        let arr = [Values.NotificationUpdateCollectionDetail,
                   Values.NotificationShowToast,
                   Values.NotificationDidSaveLocationToCollection
        ]
        for name in arr {
            NotificationCenter.default.publisher(for: Notification.Name(name), object: nil)
                .receive(on: DispatchQueue.main)
                .sink { [weak self] notification in
                    guard let self = self else { return }
                    self.processNotification(name: name, info: notification.userInfo as NSDictionary?)
                }.store(in: &anyCancelable)
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(onSelectAmenity(_:)), name: .shareCollectionSelectAmenity, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(onReloadShareLocation(_:)), name: .onReloadShareLocation, object: nil)
        
        contentViewModel!.onAdjustCenter = { [weak self] (y, index) in
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                
                self.updateLocationButton(y: y)
                
                if let contentViewModel = self.contentViewModel {
                    contentViewModel.resetCameraBasedOnBottomSheet()
                }
            }
        }
        
        contentViewModel!.$isTrackingUser
            .receive(on: DispatchQueue.main)
            .sink(receiveValue: { [weak self] isTrackingUser in
                guard let self = self else { return }
                self.isTrackingUser = isTrackingUser
        }).store(in: &disposables)
    }
    
    @objc func onSelectAmenity(_ notification: Notification) {
        if let model = notification.object as? DropPinModel {
            if let index = viewModel.bookMarks.firstIndex(where: { bookmark in
                return (model.name == bookmark.name && model.lat == bookmark.lat && model.long == bookmark.long && model.address == bookmark.address1)
            }) {
                if self.sharingMode == .collection {
                    self.selectedIndex = index
                    selectIndexShareCollection(index)
                }
                //  Show tooltip of collection bookmark
                self.selectedAmenity(id: "", isSelected: true)
            } else {
                //  Show tooltip of share location
                self.selectedAmenity(id: "", isSelected: true)
            }
        }
    }
    
    @objc func onReloadShareLocation(_ notification: Notification) {
        if let token = self.token, sharingMode == .location {
            SaveCollectionService().saveLocationWithToken(token) {[weak self] result in
                switch result {
                case .success(let content):
                    DispatchQueue.main.async {
                        //  Handle error if it is happen
                        if let code = content.data?.error_code, !code.isEmpty, let message = content.data?.message, !message.isEmpty {
                            self?.showAlert(title: "", message: message, confirmTitle: Constants.gotIt)
                        } else {
                            //  Navigate to save location and send JSON to RN
                            self?.locationData = content
                            
                            if let shareLocationData = self?.locationData?.data, let isResetBookmark = notification.object as? Bool {
                                self?.addShareLocation(shareLocationData, needResetBookmark: isResetBookmark)
                            }
                        }
                    }
                case .failure(let error):
                    SwiftyBeaver.error("Failed to get collection: \(error.localizedDescription)")
                }
            }
        }
    }
    
    func setRNTag(_ rntag: String) {
        viewModel = ShareCollectionAndLocationViewModel(rntag)
    }
    
    private func setupView() {
        let accessToken = ResourceOptionsManager.default.resourceOptions.accessToken
        
        guard !accessToken.isEmpty else {
            fatalError("Empty access token")
        }
        
        if sharingMode == .collection {
    
            bottomViewController = getRNViewBottomSheet(toScreen: viewModel.rntag,
                                                        navigationParams: [Values.COLLECTION_TOKEN: token ?? "",
                                                                           Values.TORNSCREEN : viewModel.rntag])
            
            bottomViewController.view.frame = UIScreen.main.bounds;
            
            addChildViewControllerWithView(childViewController: bottomViewController,toView: self.view)
        } else {
            if let sharedLocation = locationData?.data?.getDic4RN() {
                bottomViewController = getRNViewBottomSheet(toScreen: viewModel.rntag,
                                                            navigationParams: [Values.SHARED_LOCATION: sharedLocation,
                                                                               Values.TORNSCREEN : viewModel.rntag])
                
                bottomViewController.view.frame = UIScreen.main.bounds;
                
                addChildViewControllerWithView(childViewController: bottomViewController,toView: self.view)
            }
            
        }
    }
    
    private func processNotification(name: String, info: NSDictionary?) {
        switch name {
        case Values.NotificationUpdateCollectionDetail:
            self.processData(info)
        case Values.NotificationShowToast:
            self.processShowToast(info)
        case Values.NotificationDidSaveLocationToCollection:
            if let bookmarkId = info?["bookmarkId"] as? Int {
                print("bookmarkId: \(bookmarkId)")
                self.bookmarkId = bookmarkId
            }
//            self.onReloadShareLocation()
            NotificationCenter.default.post(name: .onReloadShareLocation, object: false)
        default:
            break
        }
    }
    
    private func showToolTip(_ bookmark: Bookmark) {
        DispatchQueue.main.async {
            let feature = self.getBookmarkFeature(bookmark)
            self.contentViewModel?.showDropPinToolTip(feature, location: CLLocationCoordinate2D(latitude: Double(bookmark.lat ?? "0") ?? 0.0, longitude: Double(bookmark.long ?? "0") ?? 0.0), view: self.view, fromScreen: .shareCollectionAndLocation)
        }
    }
    
    private func dismissToolTip() {
        DispatchQueue.main.async {
            self.contentViewModel?.deselectTooltip()
        }
    }
    
    private func processShowToast(_ info: NSDictionary?) {
        guard let message = info?["message"] as? String else {
            return
        }
        
        showToast(from: self.view, message: message, topOffset: 20, icon: "greenTick", messageColor: .black, duration: 5)
    }
    
    private func processData(_ info: NSDictionary?) {
        
        /*
        if let data = info?["bookmarks"] as? [String: Any], let fromScreen = info?["fromScreen"] as? String, fromScreen == Values.Collection_From_Screen_Choose {
            if let json = try? JSONSerialization.data(withJSONObject: data, options: .prettyPrinted),
               let bookmarks = try? JSONDecoder().decode([Bookmark].self, from: json) {
                if bookmarks.count == 1 { // only need to handle one tooltip click save
                    self.updateBookmark(bookmarks[0])
                }
            }
        }
         */
        
        if let dataDic = info?["data"] as? [String: Any] {
            guard let data = dataDic["bookmarks"], let fromScreen = info?["fromScreen"] as? String,
                  let json = try? JSONSerialization.data(withJSONObject: data, options: .prettyPrinted),
                  let bookmarks = try? JSONDecoder().decode([Bookmark].self, from: json) else { return }
            
            var selectedIndex: Int = -1
            if let index = dataDic["selectedIndex"] as? Int, index >= 0 {
                selectedIndex = index
            }
            self.selectedIndex = selectedIndex
            
            switch fromScreen {
            case Values.Collection_From_Share_Collection_Screen:
                setData(bookMarks: bookmarks, selectedIndex: selectedIndex)
            default:
                break
            }
        }
    }
    
    private func showLocationOnly(bookMarks: [Bookmark]) {
        guard let location = bookMarks.first?.location,
              let name = bookMarks.first?.address1 else {
            return
        }
        self.navMapView.mapView.mapboxMap.setCamera(to: CameraOptions(center: location, zoom: 13))
        self.setupAnnotation(location: location, name: name)
    }
    
    private func setupAnnotation(location: CLLocationCoordinate2D, name: String) {
        pointAnnotationManager.annotations = []
        var customPointAnnotation = PointAnnotation(coordinate: location)
        let traitCollection = getTraitCollection()
        customPointAnnotation.image = .init(image: UIImage(named: "collection", in: nil, compatibleWith: traitCollection)!, name: "CallOut")
        pointAnnotationManager.annotations = [customPointAnnotation]
    }
    
    private func setupMapView() {
        view.addSubview(navMapView)
        navMapView.isUserInteractionEnabled = true
        
        navMapView.translatesAutoresizingMaskIntoConstraints = false
        
        navMapView.mapView.gestures.delegate = self
        
        navMapView.snp.makeConstraints { (make) in
            make.bottom.equalToSuperview()
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.top.equalToSuperview()
        }
        
        navMapView.mapView.mapboxMap.onNext(event: .styleLoaded) { [weak self] _ in
            guard let self = self else { return }
            
            self.contentViewModel?.mobileContentMapViewUpdatable = ContentViewUpdatable(navigationMapView: self.navMapView)
            self.setupUI()
            self.setupView()
            self.addImage()
            
            if let shareLocationData = self.locationData?.data {
                self.addShareLocation(shareLocationData, needResetBookmark: true)
            }
        }
        
        navMapView.mapView.mapboxMap.onEvery(event: .cameraChanged) { [weak self] _ in
            guard let self = self else { return }
            self.adjustCallout()
        }
        
        //setupPuck()
        toggleDarkLightMode()
    }
    
    private func addImage() {
        //  add all other bookmark icons
        DispatchQueue.main.async {
            
            var traitCollection = UITraitCollection(userInterfaceStyle: .light)
            if(Settings.shared.theme == 0){
                if(appDelegate().isDarkMode()){
                    traitCollection = UITraitCollection(userInterfaceStyle: .dark)
                }
            }
            else{
                traitCollection = UITraitCollection(userInterfaceStyle: UIUserInterfaceStyle(rawValue: Settings.shared.theme) ?? .unspecified)
            }
            do {
                if let mapView = self.navMapView.mapView {
                    try mapView.mapboxMap.style.addImage(UIImage(named: "\(Values.SHARE_DESTINATION_ICON)", in: nil, compatibleWith: traitCollection)!, id: "\(Values.SHARE_DESTINATION_ICON)", stretchX: [], stretchY: [])
                }
                SwiftyBeaver.debug("Added Amenity Image to map style.")
            }catch {
                SwiftyBeaver.error("Failed to Add Amenity Image.")
            }
        }
        
        let arr = [Values.COLLECTION,
                   Values.COLLECTIONHOME,
                   Values.COLLECTIONWORK,
                   Values.COLLECTIONNORMAL,
                   Values.NEWBOOKMARKS,
                   Values.SHARE_CARPARK_FULL,
                   Values.SHARE_CARPARK_FILLING_UP,
                   Values.SHARE_CARPARK_AVAILABLE,
                   Values.SHARE_BOOKMARK_ICON,
                   Values.SHARE_EV_ICON,
                   Values.SHARE_PETROL_ICON,
                   Values.SHARE_HOME_LOCATION,
                   Values.SHARE_WORK_LOCATION,
                   Values.SHARE_PETROL_BOOKMARKED,
                   Values.SHARE_EV_BOOKMARKED,
                   Values.SHARE_CARPARK_FULL_BOOKMARKED,
                   Values.SHARE_CARPARK_FILLING_UP_BOOKMARKED,
                   Values.SHARE_CARPARK_AVAILABLE_BOOKMARKED
        ]
        navMapView.mapView.addBookmarkImagesToMapStyle(types: arr)
    }
    
    private func setupPuck() {
        //beta.14 change
        
        var traitCollection = UITraitCollection(userInterfaceStyle: .light)
        if(Settings.shared.theme == 0){
            
            if(appDelegate().isDarkMode()){
                traitCollection = UITraitCollection(userInterfaceStyle: .dark)
            }
            
        } else {
            traitCollection = UITraitCollection(userInterfaceStyle: UIUserInterfaceStyle(rawValue: Settings.shared.theme) ?? .unspecified)
        }
        
        let puck2d = Puck2DConfiguration(topImage: UIImage(named: "puckArrow",in:nil,compatibleWith: traitCollection), bearingImage: UIImage(named: "puckIcon"), shadowImage: nil, scale: nil)
        
        navMapView.userLocationStyle = .puck2D(configuration: puck2d)
        navMapView.mapView.ornaments.options.compass.visibility = .hidden
        navMapView.mapView.ornaments.options.attributionButton.visibility = .hidden
    }
    
    private func bindViewModel() {
        
    }
    
    private func adjustCallout() {
        contentViewModel?.adjustCallout()
    }
    
    // MARK: - Theme updated
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        guard previousTraitCollection != traitCollection else {
            return
        }
        toggleDarkLightMode()
    }
    
    private func toggleDarkLightMode() {
        navMapView.mapView.mapboxMap.style.uri = StyleURI(url: URL(string: isDarkMode ? Constants.Map.basicNightStyle: Constants.Map.basicStyle)!)
    }
    
//    func updateBookmark(_ bookmark: Bookmark) {
//        if var foundItem = viewModel.bookMarks.first { model in
//            return (model.name == bookmark.name && model.lat == bookmark.lat && model.long == bookmark.long && model.address1 == bookmark.address1)
//        }  {
//            foundItem.updateBookmarkId(bookmark.bookmarkId ?? -1)
//        }
//        setData(bookMarks: viewModel.bookMarks, selectedIndex: -1)
//    }
    
    func setData(bookMarks: [Bookmark], selectedIndex: Int, fromScreen: String = "") {
        if viewModel.setData(bookMarks: bookMarks) {
//            contentViewModel?.updateBookMark(bookmarks: bookMarks)
            self.addBookmarks(bookMarks)
        }
        
        if selectedIndex != -1, selectedIndex < bookMarks.count {
            let bookmark = bookMarks[selectedIndex]
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                self.showToolTip(bookmark)
            }
        } else {
            DispatchQueue.main.async {
                self.contentViewModel?.deselectTooltip()
            }
        }
        
        pointAnnotationManager.annotations = []
    }
    
    @objc func locationButtonTapped(sender: UserLocationButton) {
//        resetCamera()
        self.selectedAmenity(id: "", isSelected: false)
        self.dismissToolTip()
        if let locationData = self.locationData?.data {
            self.contentViewModel?.setCameraDefaultCameraPositionForShareLocationWhenClickRecenter(shareLocation: locationData)
        } else {
            if let shareCollection = self.viewModel.bookMarks.first {
                self.contentViewModel?.setCameraDefaultCameraPositionForShareCollectionWhenClickRecenter(shareCollection: shareCollection)
            }
        }
        
        
    }
    
    private func resetCamera() {
        
        isTrackingUser = true
        
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            
            if let contentViewModel = self.contentViewModel{
                
                contentViewModel.resetCamera()
            }
        }
    }
    
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
}

extension ShareCollectionAndLocationVC {
    
    private func setupUI() {
        
        setupLocationButton()
    }
    
    private func updateLocationButton(y: Int) {
        userLocationButton.snp.updateConstraints { make in
            make.bottom.equalToSuperview().offset(-(CGFloat(y) + 20))
        }
    }
    
    
        
    private func setupLocationButton() {
        let userLocationButton = UserLocationButton(buttonSize: (Images.locationImage?.size.width)!)
        userLocationButton.addTarget(self, action: #selector(locationButtonTapped), for: .touchUpInside)
        
        userLocationButton.translatesAutoresizingMaskIntoConstraints = false
        navMapView.mapView.addSubview(userLocationButton)
        userLocationButton.snp.makeConstraints { make in
            make.trailing.equalTo(self.view.safeAreaLayoutGuide.snp.trailing).offset(-20)
            // TODO: Need to adjust when bottom sheet is ready
            make.bottom.equalToSuperview().offset(-(322 + 20))
        }

        self.userLocationButton = userLocationButton
        userLocationButton.isHidden = true // hide it by default
    }
}

extension ShareCollectionAndLocationVC: GestureManagerDelegate {
    
    func gestureManager(_ gestureManager: GestureManager, didEnd gestureType: GestureType, willAnimate: Bool) {
        
        //Not required to handle this gesture
    }
    
    func gestureManager(_ gestureManager: GestureManager, didEndAnimatingFor gestureType: GestureType) {
        
        //Not required to handle this gesture
    }
    
    func gestureManager(_ gestureManager: GestureManager, didBegin gestureType: GestureType) {
        
        var name = ParameterName.CollectionDetail.MapInteraction.drag
        switch gestureType {
        case .pinch:
            name = ParameterName.CustomMap.MapInteraction.pinch
        case .singleTap:
            let point = gestureManager.singleTapGestureRecognizer.location(in: self.navMapView)
            contentViewModel?.queryShareCollectionFeatureOnTap(point: point, view: self.view, fromScreen: .shareCollectionAndLocation, shareLinkData: self.locationData?.data)
//            contentViewModel?.queryFeaturesOnTap(point: point, view: self.view, fromScreen: .shareCollectionAndLocation)
            
        default:
            name = ParameterName.CustomMap.MapInteraction.drag
        }
        AnalyticsManager.shared.logMapInteraction(eventValue: name, screenName: self.trackingScreenName)
        
        if gestureType == .pan || gestureType == .pinch || gestureType == .doubleTapToZoomIn || gestureType == .doubleTouchToZoomOut {
            isTrackingUser = false
        }
    }
}

extension ShareCollectionAndLocationVC: ContentViewModelDelegate {
    
    func closeDropPin() {
        //  Not needed
    }
    
    func toolTipOpen() {
        //Not needed
        if let _ = locationData {
            //  If share location then set it center focus
        } else {
            contentViewModel?.focusOnDropPinToolTip()
        }
    }
    
    func dismissTBRBeforeAddingNew(){
        //Not needed
    }
    
    func navigateHere(address:SearchAddresses?, carPark:Carpark?, layerCode: String?, amenityId: String?, amenityType: String?, amenityCarPark: String?) {
        
        //  TODO -> Just need to implement navigate to a location
        let storybard = UIStoryboard(name: "NewRoutePlanning", bundle: nil)
        if let vc = storybard.instantiateViewController(withIdentifier: String(describing: NewRoutePlanningVC.self)) as? NewRoutePlanningVC {
            vc.originalAddress = address
            vc.addressReceived = true
            vc.carparkId = amenityCarPark
            if let carPark = carPark {
                vc.selectedCarPark = carPark
            }
            vc.bookmarkId = amenityId
            vc.bookmarkName = address?.address1
            self.navigationController?.pushViewController(vc, animated: true)
            
            self.selectedAmenity(id: "", isSelected: false)
        }
        
    }
    
    func toolTipMoreInfo(poi:POI?, carPark:Carpark?, screenName:String){
    
        //  Not needed because there are no view more in this page
        /*
        let lat = Prefs.shared.getStringValue(.latSavePlace)
        let long = Prefs.shared.getStringValue(.longSavePlace)
        
        if let carPark = carPark {
            let carParkData = screenName == "ParkingCalculator" ? [
                "toScreen": screenName, // Screen name you want to display when start RN
                "navigationParams": [ // Object data to be used by that screen
                    "parkingID": carPark.id ?? "",
                    "fromNative":true,
                    Values.FOCUSED_LAT: lat,
                    Values.FOCUSED_LONG: long
                                    ],
            ] as [String : Any] :[
                "toScreen": screenName, // Screen name you want to display when start RN
                "navigationParams": [ // Object data to be used by that screen
                    "parkingId": carPark.id,
                    Values.FOCUSED_LAT: lat,
                    Values.FOCUSED_LONG: long
                                    ],
            ] as [String : Any]
            let rootView = RNViewManager.sharedInstance.viewForModule(
                "Breeze",
                initialProperties: carParkData)
            
            let reactNativeVC = UIViewController()
            reactNativeVC.view = rootView
            reactNativeVC.modalPresentationStyle = .pageSheet
            self.present(reactNativeVC, animated: true, completion: nil)
        }
         */
    }
    
    func selectedAmenity(id: String, isSelected: Bool) {
        if self.sharingMode == .collection {
            DispatchQueue.main.async {
                if !isSelected {
                    //  Deselect all bookmark
                    self.selectedIndex = -1
                    selectIndexShareCollection(self.selectedIndex)
                }
                
                //  Update bookmark selected status
                self.viewModel.updateSelectedBookmark(self.selectedIndex)
                
                //  Update map amenity icon on map
                self.removeAllLayers()
                for type in self.types {
                    self.addBookmarkWithType(self.viewModel.bookMarks, type: type)
                }
            }
        } else {
            isShareLocationSelected = isSelected
            DispatchQueue.main.async {
                if let model = self.locationData?.data {
                    self.removeAllLayers()
                    if let mapView = self.navMapView.mapView , let feature = self.contentViewModel?.getShareLocationFeature(shareLocation: model, isSelected: self.isShareLocationSelected, bookmarkId: self.bookmarkId) {
                        let featureCollection = FeatureCollection(features: [feature])
                        mapView.addShareCollectionBookmark(features: featureCollection, type: model.getType(), isAbovePuck: true)
                    }
                }
            }
        }
    }
    
    func startWalkingPath(poi:POI?, trackNavigation: Bool, amenityId: String?, name: String?, canReroute: Bool?) {
        //no need
    }
    
}

extension ShareCollectionAndLocationVC {
    private func removeAllLayers() {
        for type in self.types {
            if let mapView = navMapView.mapView {
                mapView.removeAmenitiesLayerOfType(type: type)
            }
        }
    }
    
    private func addBookmarks(_ bookmarks: [Bookmark]) {
        //  Remove all old layer
        contentViewModel?.deselectTooltip()
        removeAllLayers()
        
        for type in self.types {
            addBookmarkWithType(bookmarks, type: type)
        }
        
        contentViewModel?.setCameraDefaultCameraPositionForBookmarks(bookmarks: bookmarks)
//        if let shareCollection = self.viewModel.bookMarks.first {
//            self.contentViewModel?.setCameraDefaultCameraPositionForShareCollectionWhenClickRecenter(shareCollection: shareCollection)
//        }
    }
    
    private func addShareLocation(_ model: SaveLocationModel, needResetBookmark: Bool) {
        contentViewModel?.deselectTooltip()
        removeAllLayers()
        
        if needResetBookmark {
            self.bookmarkId = nil
        }
        
        if let mapView = navMapView.mapView , let feature = contentViewModel?.getShareLocationFeature(shareLocation: model, bookmarkId: self.bookmarkId) {
            let featureCollection = FeatureCollection(features: [feature])
            mapView.addShareCollectionBookmark(features: featureCollection, type: model.getType(), isAbovePuck: true)
            contentViewModel?.setCameraDefaultCameraPositionForShareLocation(shareLocation: model)
            DispatchQueue.main.async {
                let location = CLLocationCoordinate2D(latitude: model.lat?.toDouble()
                                                      ?? 0.0, longitude: model.long?.toDouble() ?? 0.0 )
                self.contentViewModel?.showDropPinToolTip(feature, location: location, view: self.view, fromScreen: .shareCollectionAndLocation)
            }
        }
    }
    
    private func addBookmarkWithType(_ bookmarks: [Bookmark], type: String) {
        let filtered = bookmarks.filter { bookmark in
            return bookmark.getType() == type
        }
        
        if let mapView = navMapView.mapView {
            var features: [Turf.Feature] = []
            for bookmark in filtered {
                let feature = getBookmarkFeature(bookmark)
                features.append(feature)
            }
            if !features.isEmpty {
                let featureCollection = FeatureCollection(features: features)
                mapView.addShareCollectionBookmark(features: featureCollection, type: type, isAbovePuck: true)
               
            }
        }
    }
    
    private func removePinLocation() {
        if let mapView = navMapView.mapView {
            mapView.removeDropPinLayer()
        }
    }
    
    private func getBookmarkFeature(_ model: Bookmark, isShareLocationSelected: Bool = false) -> Turf.Feature {
        let coordinate = CLLocationCoordinate2D(latitude: Double(model.lat ?? "0") ?? 0.0, longitude: Double(model.long ?? "0") ?? 0.0)
        var feature = Turf.Feature(geometry: .point(Point(coordinate)))
        
        var isSelected: Bool = false
        if sharingMode == .collection {
            isSelected = model.isSelected ?? false
        } else {
            isSelected = isShareLocationSelected
        }
        
        var properties: JSONObject = [
            "type":.string(Values.DROPPIN),
            "name":.string(model.name ?? ""),
            "address":.string(model.address1 ?? ""),
            "isBookmarked": .boolean(model.saved ?? false),
            "isSelected": .boolean(isSelected),
            "lat": .string(model.lat ?? "0"),
            "long": .string(model.long ?? "0"),
            "isShareCollection": .boolean(true),
            "bookmarkSnapshotId": .string("\(model.bookmarkSnapshotId ?? -1)"),
            "bookmarkId": .string("\(model.bookmarkId ?? 0)"),
            "address2": .string(model.address2 ?? ""),
            "fullAddress": .string(model.fullAddress ?? "")
        ]
        if let amenityId = model.amenityId, !amenityId.isEmpty {
            properties["amenityId"] = .string(amenityId)
        }
        
        feature.properties = properties
        return feature
    }
}


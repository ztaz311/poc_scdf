//
//  ShareCollectionAndLocationViewModel.swift
//  Breeze
//
//  Created by Tuan, Pham Hai on 17/11/2023.
//

import Foundation
import Combine

final class ShareCollectionAndLocationViewModel {
    var rntag: String
    private(set) var bookMarks: [Bookmark] = []
    
    let bookmarksUpdatedAction = PassthroughSubject<Void, Never>()
    
    init(_ rntag: String) {
        self.rntag = rntag
    }
    
    // Return false if same
    func setData(bookMarks: [Bookmark]) -> Bool {
        if self.compareBookmark(bookMarks: bookMarks) {
            return false
        }
        self.bookMarks = bookMarks
        bookmarksUpdatedAction.send()
        return true
    }
    
    func updateSelectedBookmark(_ index: Int) {        
        var counter = 0
        for bookMark in bookMarks {
            bookMark.isSelected = (counter == index)
            counter += 1
        }
    }
    
    func selectedAmenity(id: String, isSelected: Bool) {
        for bookMark in bookMarks {
            if bookMark.bookmarkId == Int(id) {
                bookMark.isSelected = isSelected
                return
            }
        }
    }
    
    func updateCodeForNewBookMarks(){
        for bookMark in bookMarks {
            bookMark.code = Values.NEWBOOKMARKS
        }
    }
    
    func compareBookmark(bookMarks: [Bookmark]) -> Bool {
        if bookMarks.count != self.bookMarks.count {
            return false
        }
        
        for i in 0..<bookMarks.count {
            if bookMarks[i] != self.bookMarks[i] {
                return false
            }
        }
        return true
    }
    
    func clearBookMarks(){
        if bookMarks.count > 0{
            bookMarks.removeAll()
        }
    }
}

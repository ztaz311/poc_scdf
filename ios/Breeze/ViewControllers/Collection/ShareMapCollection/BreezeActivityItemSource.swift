//
//  BreezeActivityItemSource.swift
//  Breeze
//
//  Created by Tuan, Pham Hai on 18/11/2022.
//

import LinkPresentation

class BreezeActivityItemSource: NSObject, UIActivityItemSource {
    var text: String
    
    init(text: String) {
        self.text = text
        super.init()
    }
    
    func activityViewControllerPlaceholderItem(_ activityViewController: UIActivityViewController) -> Any {
        return text
    }
    
    func activityViewController(_ activityViewController: UIActivityViewController, itemForActivityType activityType: UIActivity.ActivityType?) -> Any? {
        return text
    }
    
    func activityViewController(_ activityViewController: UIActivityViewController, subjectForActivityType activityType: UIActivity.ActivityType?) -> String {
        return ""
    }

    func activityViewControllerLinkMetadata(_ activityViewController: UIActivityViewController) -> LPLinkMetadata? {
        let metadata = LPLinkMetadata()
        metadata.title = text
        metadata.iconProvider = NSItemProvider(object: UIImage(named: "shareAppIcon")!)
        //This is a bit ugly, though I could not find other ways to show text content below title.
        //https://stackoverflow.com/questions/60563773/ios-13-share-sheet-changing-subtitle-item-description
        //You may need to escape some special characters like "/".
//        metadata.originalURL = URL(fileURLWithPath: text)
        return metadata
    }

}

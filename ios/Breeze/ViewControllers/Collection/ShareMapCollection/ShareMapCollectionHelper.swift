//
//  ShareMapCollectionHelper.swift
//  Breeze
//
//  Created by Tuan, Pham Hai on 23/11/2022.
//

import Foundation
import FirebaseDynamicLinks

class ShareMapCollectionHelper: NSObject {
    static let shared = ShareMapCollectionHelper()
    
    typealias ShareManagerhandle = (String?) -> Void
    
    let baseLink = "https://ncstest.link/"
    let uriPrefix = "https://ncstest.page.link"
}

extension ShareMapCollectionHelper {
    // Create dymanic link with params
    func newDynamicLink(params: [String: Any], completion: @escaping ShareManagerhandle) {
        
        let link = String(format: "%@?token=1", arguments: [baseLink])
        guard let url = URL(string: link) else {
            completion(nil)
            return
        }

        guard let linkBuilder = DynamicLinkComponents(link: url, domainURIPrefix: uriPrefix),
              let bundleID = Bundle.main.bundleIdentifier else {
            completion(nil)
            return
        }
        
        linkBuilder.iOSParameters = DynamicLinkIOSParameters(bundleID: bundleID)
        
        // webview open URL when app is not install
        // We can't add https://ncstest.link?idTrip=1 => don't pass firebase whitelist
        linkBuilder.iOSParameters?.fallbackURL = URL(string: "https://ncstest.link/?token=1")
        
        // TODO: Add android package Name & property here
//        linkBuilder.androidParameters = DynamicLinkAndroidParameters(packageName: "")
//        linkBuilder.androidParameters?.minimumVersion = 1
        
        // Add params to show on social media
//        let metaTagParameters = DynamicLinkSocialMetaTagParameters()
//        metaTagParameters.title =
//        metaTagParameters.descriptionText = model.descriptionText
//        metaTagParameters.imageURL =
//        linkBuilder.socialMetaTagParameters = metaTagParameters

        guard linkBuilder.url != nil else {
            completion(nil)
            return
        }

        let option = DynamicLinkComponentsOptions()
        option.pathLength = .unguessable

        linkBuilder.options = option
        linkBuilder.shorten { url, warnings, error in
            guard let url = url else {
                completion(nil)
                return
            }
            print("The short URL is: \(url) \(String(describing: warnings)) \(String(describing: error))")
            completion(url.absoluteString)
        }
    }
}

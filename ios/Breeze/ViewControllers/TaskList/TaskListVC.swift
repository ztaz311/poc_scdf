//
//  TaskListVC.swift
//  Breeze
//
//  Created by VishnuKanth on 19/03/21.
//

import UIKit
import ImageScrollView

class TaskListVC: UIViewController {
    
    @IBOutlet weak var imageScrollView: ImageScrollView!
    @IBOutlet weak var topView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.ShowAndHideFloatingView(value: true)
        topView.showBottomShadow()
        imageScrollView.setup()

        let myImage = UIImage(named: "TaskList")
        imageScrollView.imageScrollViewDelegate = self
        imageScrollView.imageContentMode = .widthFill
        imageScrollView.initialOffset = .begining
        //imageScrollView.isScrollEnabled = false
        imageScrollView.display(image: myImage!)
        // Do any additional setup after loading the view.
        setupDarkLightAppearance(forceLightMode: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.ShowAndHideFloatingView(value: false)
    }
    
    @IBAction func backbtnAction(){
        self.navigationController?.popViewController(animated: true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension TaskListVC: ImageScrollViewDelegate {
    func imageScrollViewDidChangeOrientation(imageScrollView: ImageScrollView) {
        print("Did change orientation")
    }
    
    func scrollViewDidEndZooming(_ scrollView: UIScrollView, with view: UIView?, atScale scale: CGFloat) {
        print("scrollViewDidEndZooming at scale \(scale)")
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        print("scrollViewDidScroll at offset \(scrollView.contentOffset)")
    }
}

//
//  MapDisplayTVC.swift
//  Breeze
//
//  Created by Malou Mendoza on 28/5/21.
//

import UIKit



class MapDisplayTVC: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.tableFooterView = UIView()
        self.tableView.tintColor = UIColor.brandPurpleColor
        
        var rowSelected = 0
       
        if UserSettingsModel.sharedInstance.mapDisplay == "Auto"{
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.settings_map_display_auto, screenName: ParameterName.settings_map_display_screen)
            rowSelected = 0
        }
        else if UserSettingsModel.sharedInstance.mapDisplay == "Light"{
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.settings_map_display_light, screenName: ParameterName.settings_map_display_screen)
           rowSelected = 1
        }
        else if UserSettingsModel.sharedInstance.mapDisplay == "Dark"{
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.settings_map_display_dark, screenName: ParameterName.settings_map_display_screen)
            rowSelected = 2
        }
        let index = NSIndexPath(row: rowSelected, section: 0)
        self.tableView.selectRow(at: index as IndexPath, animated: true, scrollPosition: UITableView.ScrollPosition.middle)
        self.tableView.delegate?.tableView!(self.tableView, didSelectRowAt: index as IndexPath)
        
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        AnalyticsManager.shared.logScreenView(screenName: ParameterName.settings_map_display_screen)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.cellForRow(at:indexPath)?.selectionStyle = .none
        tableView.cellForRow(at:indexPath)?.accessoryType = UITableViewCell.AccessoryType.checkmark

         for cellPath in tableView.visibleCells{
            if cellPath == tableView.cellForRow(at: indexPath){
                continue
            }
            cellPath.accessoryType = UITableViewCell.AccessoryType.none
         }
        if indexPath.row == 0{
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.SettingsMap.UserClick.auto, screenName: ParameterName.SettingsMap.screen_view)
            UserSettingsModel.sharedInstance.mapDisplay = "Auto"
            changeMode(mode: 0)
        }else if indexPath.row == 1{
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.SettingsMap.UserClick.light, screenName: ParameterName.SettingsMap.screen_view)
            UserSettingsModel.sharedInstance.mapDisplay = "Light"
            changeMode(mode: 1)
        }else if indexPath.row == 2{
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.SettingsMap.UserClick.dark, screenName: ParameterName.SettingsMap.screen_view)
            UserSettingsModel.sharedInstance.mapDisplay = "Dark"
            changeMode(mode: 2)
        }
        updateUserSettings()
    }
    
    func changeMode(mode:Int){
        UIApplication.shared.windows.forEach { window in
            if mode > 0 {
                window.overrideUserInterfaceStyle = mode == 1 ? .light : .dark
            } else {
                window.overrideUserInterfaceStyle = .unspecified
            }
        }
        
//        let window = UIApplication.shared.windows[0]
//        if mode > 0 {
//            window.overrideUserInterfaceStyle = mode == 1 ? .light : .dark
//        } else {
//            window.overrideUserInterfaceStyle = .unspecified
//        }
        sendThemeChangeEvent(mode == 2)
    }
    
    
    func updateUserSettings() {
        guard let value = UserSettingsModel.sharedInstance.mapDisplay else { return }
        
        SettingsService.shared.updateSettingAttribute(UserSettingsModel.UserSettingAttributes.mapDisplayKey, value: value) { result in
            print(result)
        }
    }

}

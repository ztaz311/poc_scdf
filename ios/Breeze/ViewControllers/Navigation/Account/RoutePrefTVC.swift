//
//  RoutePrefTVC.swift
//  Breeze
//
//  Created by Malou Mendoza on 28/5/21.
//

import UIKit

class RoutePrefTVC: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
       
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.tableFooterView = UIView()
        self.tableView.tintColor = UIColor.settingsTintColor
        
        var rowSelected = 0
       
        if UserSettingsModel.sharedInstance.routePref == "Fastest"{
            rowSelected = 0
        }
        else if UserSettingsModel.sharedInstance.routePref == "Cheapest"{
           rowSelected = 1
        }
       
        let index = NSIndexPath(row: rowSelected, section: 0)
        self.tableView.selectRow(at: index as IndexPath, animated: true, scrollPosition: UITableView.ScrollPosition.middle)
        self.tableView.delegate?.tableView!(self.tableView, didSelectRowAt: index as IndexPath)
       
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        AnalyticsManager.shared.logScreenView(screenName: ParameterName.settings_route_screen)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.cellForRow(at:indexPath)?.selectionStyle = .none
        tableView.cellForRow(at:indexPath)?.accessoryType = UITableViewCell.AccessoryType.checkmark

         for cellPath in tableView.visibleCells{
            if cellPath == tableView.cellForRow(at: indexPath){
                continue
            }
            cellPath.accessoryType = UITableViewCell.AccessoryType.none
         }
        
        //set Route Preference
        if indexPath.row == 0{
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.settings_route_fast, screenName: ParameterName.settings_route_screen)
            UserSettingsModel.sharedInstance.routePref = "Fastest"
        }else if indexPath.row == 1{
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.settings_route_cheap, screenName: ParameterName.settings_route_screen)
            UserSettingsModel.sharedInstance.routePref = "Cheapest"
        }
        updateUserSettings()
    }
    
    func updateUserSettings() {
        
        guard let value = UserSettingsModel.sharedInstance.routePref else { return }
        
        SettingsService.shared.updateSettingAttribute(UserSettingsModel.UserSettingAttributes.routePreference, value: value) { result in
            print(result)
        }
    }

}

//
//  SettingsVC.swift
//  Breeze
//
//  Created by Zhou Hao on 7/4/21.
//

import UIKit

class SettingsVC: BaseViewController {

    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var backBtn: UIButton!
    
    var settingsValuesTVC: SettingsValuesTVC!
    var settingsTVC : SettingsTVC!
    var hideBackButton = false
    private var tapCount = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        topView.showBottomShadow()
//        setupDarkLightAppearance(forceLightMode: true)
        let touchGesture = UITapGestureRecognizer(target: self, action: #selector(tapGesture(_:)))
                
        topView.addGestureRecognizer(touchGesture)
        
        backBtn.isHidden = hideBackButton
        checkShowObuInstallIfNeeded()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupDarkLightAppearance()
        
    }
    
    // to show different color for status bar in different theme
    override var useDynamicTheme: Bool {
        return true
    }

    @objc func tapGesture(_ gesture: UITapGestureRecognizer){
        
        if !UserDefaults.standard.bool(forKey: Values.debugEnabled) {
            if gesture.state == .ended {
                tapCount += 1
                if tapCount == 7 {
                    UserDefaults.standard.setValue(true, forKey: Values.debugEnabled)
                }
            }
        }
    }
    
    @IBAction func onBack(_ sender: Any) {
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.back, screenName: ParameterName.BreezeSettings.screen_view)
        self.ShowAndHideFloatingView(value: false)
        self.moveBack()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        switch segue.destination {
        
        case let vc as SettingsValuesTVC:
            settingsValuesTVC = vc
        case let vc as SettingsTVC:
            settingsTVC = vc
        default:
            break
        }
    }
}

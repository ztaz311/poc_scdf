//
//  SettingsDetailVC.swift
//  Breeze
//
//  Created by Malou Mendoza on 28/5/21.
//

import UIKit

class SettingsDetailVC: UIViewController {

    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var lblHeader: UILabel!
    var headerString: String = ""
    var segueID: String!
    @IBOutlet weak var containerView:UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.topView.showBottomShadow()
        setupDarkLightAppearance(forceLightMode: true)
        self.lblHeader.text = headerString
        
        let storyboard = UIStoryboard(name: "Account", bundle: nil)
        if self.segueID == "mapDisplaySegue"
        {
            if let vc = storyboard.instantiateViewController(withIdentifier: String.init(describing: MapDisplayTVC.self)) as? MapDisplayTVC {
                
                loadEmbededView(viewController: vc)
            }
        }
        else
        {
            if let vc = storyboard.instantiateViewController(withIdentifier: String.init(describing: RoutePrefTVC.self)) as? RoutePrefTVC {
                
                loadEmbededView(viewController: vc)
            }
        }
    }
    
    func loadEmbededView(viewController:UIViewController){
        
        viewController.willMove(toParent: self)
        viewController.view.frame = containerView.bounds
        self.containerView.addSubview(viewController.view)
        addChild(viewController)
        viewController.didMove(toParent: self)
        
    }
    

 
    @IBAction func onBack(_ sender: Any) {
       if self.segueID == "mapDisplaySegue" {
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.SettingsMap.UserClick.back, screenName: ParameterName.SettingsMap.screen_view)
//            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.back, screenName: ParameterName.settings_map_display_screen)
        }else{
//            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.back, screenName: ParameterName.settings_route_screen)
        }
        self.moveBack()
    }

    func setLabelHeader( headerLbl:String, segueID: String){
        self.headerString = headerLbl
        self.segueID = segueID
    }
    
    

}

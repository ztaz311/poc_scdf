//
//  SettingsValuesTVC.swift
//  Breeze
//
//  Created by Zhou Hao on 7/4/21.
//

import UIKit

final class SettingsValuesTVC: UITableViewController {
    
    @IBOutlet weak var txtNotificationDuration: UITextField!
    @IBOutlet weak var switchNotificationMute: UISwitch!
    @IBOutlet weak var themeSegment: UISegmentedControl!
    @IBOutlet weak var routeOptionSegment: UISegmentedControl!
    
    var userId: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        userId = AWSAuth.sharedInstance.userName
        Settings.shared.load(userId: userId)

        txtNotificationDuration.text = "\(Int(Settings.shared.notificationDuration))"
        switchNotificationMute.isOn = Settings.shared.muteNotification
        themeSegment.selectedSegmentIndex = Settings.shared.theme
        routeOptionSegment.selectedSegmentIndex = Settings.shared.routeOption
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // for simplify, save here. May need to change if we need to save in real time
        if let value = Double(txtNotificationDuration.text!) {
            Settings.shared.notificationDuration = value
        }
        Settings.shared.muteNotification = switchNotificationMute.isOn
        Settings.shared.setTheme(value: themeSegment.selectedSegmentIndex)
        Settings.shared.routeOption = routeOptionSegment.selectedSegmentIndex
        Settings.shared.save(userId: userId)
    }

    @IBAction func didChanged(_ sender: Any) {
        
//        if sender as? UITextField == txtNotificationDuration {
//            if let value = Double(txtNotificationDuration.text!) {
//                settings.notificationDuration = value
//            }
//        } else if sender as? UISwitch == switchNotificationMute {
//            settings.muteNotification = switchNotificationMute.isOn
//        }
        if let segment = sender as? UISegmentedControl {
            
            if(segment.tag == 1) //This tag is for ThemeSegement Control
            {
                changeMode(mode: segment.selectedSegmentIndex)
            }
            
        }
    }
    
    private func changeMode(mode: Int) {
        
        let window = UIApplication.shared.windows[0]
        if mode > 0 {
            window.overrideUserInterfaceStyle = mode == 1 ? .light : .dark
        } else {
            window.overrideUserInterfaceStyle = .unspecified
        }
        
    }
    
    // Hide other settings first
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
}

//
//  PrivacypolicyVC.swift
//  Breeze
//
//  Created by Malou Mendoza on 3/6/21.
//

import UIKit

class PrivacypolicyVC: UIViewController {

    @IBOutlet weak var topView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.topView.showBottomShadow()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        AnalyticsManager.shared.logScreenView(screenName: ParameterName.settings_policy_screen)
    }

    @IBAction func onBack(_ sender: Any) {
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.SettingsPrivacy.UserClick.back, screenName: ParameterName.SettingsPrivacy.screen_view)
        self.moveBack()
    }
}

//
//  UserAccountContainer.swift
//  Breeze
//
//  Created by VishnuKanth on 30/05/21.
//

import UIKit
import Instabug

protocol AccountEmbeddedDelegate: NSObjectProtocol {
    func openCarDetail()
}

protocol AccountEmbeddedProtocol {
    var accountData: UserDataModel? {get}
    
    var isHaveCarDetail: Bool {get}
    
    func fillCarDetail(userData: UserDataModel?)
}

extension AccountEmbeddedProtocol {
    var isHaveCarDetail: Bool {
        return accountData?.haveCardetail ?? false
    }
}

// This screen is simple => we don't use MVVM here
enum SectionItem: Int {
    case guestInfo
    case socialMedia
    case username
    case phoneNumber
    case cardetail
    case triplogEmail
}

class UserAccountContainer: BaseViewController {

    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var containerView:UIView!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var signOutButton: UIButton!
    @IBOutlet weak var createAccountButton: UIButton!
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var bottomView: UIView!
    
    var sections: [SectionItem] = []
    var userData: UserDataModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        topView.showBottomShadow()
        setupDarkLightAppearance(forceLightMode: true)
        self.updateUI()
        self.configTableView()
    }
    
    private func configTableView() {
        tableView.register(UINib(nibName: "GuestInfoCell", bundle: nil), forCellReuseIdentifier: "GuestInfoCell")
        tableView.register(UINib(nibName: "ProfileTableViewCell", bundle: nil), forCellReuseIdentifier: "ProfileTableViewCell")
        tableView.register(UINib(nibName: "ProfileSocialMediaCell", bundle: nil), forCellReuseIdentifier: "ProfileSocialMediaCell")
        self.tableView.dataSource = self
        self.tableView.delegate = self
    }
    
    private func updateUI() {
        createAccountButton.layer.cornerRadius = 24
        createAccountButton.layer.masksToBounds = true
        
        signInButton.layer.cornerRadius = 24
        signInButton.layer.masksToBounds = true
        signInButton.layer.borderWidth = 1
        signInButton.layer.borderColor = UIColor(hexString: "782EB1", alpha: 1).cgColor
    }
    
    private func configSections() {
        //  Config sections to display
        
        sections.removeAll()
        
        if AWSAuth.sharedInstance.isGuestMode() {
            bottomView.isHidden = false
            signOutButton.isHidden = true

            sections.append(.guestInfo)
            sections.append(.username)
            sections.append(.cardetail)
//            sections.append(.triplogEmail)

        }else {
            
            bottomView.isHidden = true
            signOutButton.isHidden = false
            
            //  If user signin with social media
            if !AWSAuth.sharedInstance.providerName.isEmpty {
                sections.append(.socialMedia)
                sections.append(.username)
                sections.append(.cardetail)
//                sections.append(.triplogEmail)
            }else {
                //  If user signin with phone number
                sections.append(.username)
                if !AWSAuth.sharedInstance.phoneNumber.isEmpty {
                    sections.append(.phoneNumber)
                }
                sections.append(.cardetail)
//                sections.append(.triplogEmail)
            }
        }
        
        self.tableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getUserData()
    }
    
    override var useDynamicTheme: Bool {
        return true
    }

    private func getUserData() {
        self.showLoadingIndicator()
        AWSAuth.sharedInstance.getUserData { isSuccess in
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                self.userData = AWSAuth.sharedInstance.userData
                self.hideLoadingIndicator()
                
                self.configSections()
            }
        }
    }
    
    func loadEmbededView(viewController: UIViewController){
        
        viewController.willMove(toParent: self)
        viewController.view.frame = containerView.bounds
        self.containerView.addSubview(viewController.view)
        addChild(viewController)
        viewController.didMove(toParent: self)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        AnalyticsManager.shared.logScreenView(screenName: ParameterName.settings_account_screen)
    }
    
    @IBAction func onCreateAccount(_ sender: Any) {
        //  TODO handle create new account part 2 -> Guest Mode Part 1 force signout
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.SettingAccountGuest.UserClick.createAcc, screenName: ParameterName.SettingAccountGuest.screen_view)
                
        AWSAuth.sharedInstance.forceSignOut { success in
            if success {
                appDelegate().forceSignoutUser(.createAccount)
            }
        }
    }
    
    @IBAction func onSignIn(_ sender: Any) {
        //  TODO handle sign in part 2 -> Guest Mode Part 1 force signout
        
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.SettingAccountGuest.UserClick.signin, screenName: ParameterName.SettingAccountGuest.screen_view)
                
        AWSAuth.sharedInstance.forceSignOut { success in
            if success {
                appDelegate().forceSignoutUser(.signin)
            }
        }
    }
    
    @IBAction func onLogout(_ sender: Any) {
        
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.SettingsAccount.UserClick.logout, screenName: ParameterName.SettingsAccount.screen_view)
        self.popupAlert(title: nil, message: Constants.logoutConfirmationMessage, actionTitles: [Constants.cancel,Constants.logout], actions:[
            { action1 in
                //We don't to process this
            },
            { [weak self] action2 in
                self?.signOutUser()
            },
            nil])
    }
    
    @IBAction func onBack(_ sender: Any) {
        if AWSAuth.sharedInstance.isGuestMode() {
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.SettingAccountGuest.UserClick.back, screenName: ParameterName.SettingAccountGuest.screen_view)
        }else {
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.SettingsAccount.UserClick.back, screenName: ParameterName.SettingsAccount.screen_view)
        }
        self.moveBack()
    }
    
    private func signOutUser() {
        
        AWSAuth.sharedInstance.forceSignOut { success in
            if success {
                appDelegate().forceSignoutUser()
            }
        }
        
        /*
        AWSAuth.sharedInstance.signOut { [weak self] (result) in
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                EventEmitter.sharedInstance.dispatch(name: "logout", body: [])
                
                if(SavedSearchManager.shared.easyBreezy.count > 0)
                {
                    SavedSearchManager.shared.easyBreezy.removeAll()
                }
                
                if (AmenitiesSharedInstance.shared.amenitiesSymbolLayer.count > 0){
                    
                    AmenitiesSharedInstance.shared.amenitiesSymbolLayer.removeAll()
                }
                
                SelectedProfiles.shared.clearProfile()
                
                Instabug.logOut()
                
                DataCenter.shared.unload()
                
                RNViewManager.sharedInstance.clearBridge()
                
                // this one will fix the MapLandVC memory leak so that the deinit of MapLandingVC will be called.
                self.navigationController?.popToRootViewController(animated: true)
                self.goToLoginVC()
            }
        } onFailure: { (error) in
            DispatchQueue.main.async {
                //Not required to handle error message at the moment
            }
        }
         */
    }
}

extension UserAccountContainer: AccountEmbeddedDelegate {
    func openCarDetail() {
        self.goToRNScreen(toScreen: "CarDetailScreen", navigationParams: [:])
    }
}

extension UserAccountContainer: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = sections[indexPath.row]
        
        switch item {
        case .guestInfo:
            if let cell = tableView.dequeueReusableCell(withIdentifier: "GuestInfoCell") as? GuestInfoCell {
                cell.setupUI()
                return cell
            }
        case .socialMedia:
            if let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileSocialMediaCell") as? ProfileSocialMediaCell {
                return cell
            }
        case .username:
            if let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileTableViewCell") as? ProfileTableViewCell {
                cell.setData(title: "Username", desc: AWSAuth.sharedInstance.userName, showInfoIcon: false)
                return cell
            }
        case .phoneNumber:
            if let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileTableViewCell") as? ProfileTableViewCell {
                cell.setData(title: "Phone Number", desc: AWSAuth.sharedInstance.phoneNumber, showInfoIcon: false)
                return cell
            }
        case .cardetail:
            if let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileTableViewCell") as? ProfileTableViewCell {
                if AWSAuth.sharedInstance.hasCarDetailInfo() {
                    cell.setData(title: "Car details", desc: AWSAuth.sharedInstance.getCarDetailDisplay(), showInfoIcon: false)
                    cell.infoIcon.image = UIImage(named: "e_verify")
                }else {
                    cell.setData(title: "Car details", desc: "Tap to add carplate no. and IU no.", showInfoIcon: true)
                    cell.infoIcon.image = UIImage(named: "e_unverify")
                }
                return cell
            }
        case .triplogEmail:
            if let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileTableViewCell") as? ProfileTableViewCell {
                
//                if !AWSAuth.sharedInstance.customEmail.isEmpty && AWSAuth.sharedInstance.emailVerifyStatus == true {
//                    cell.infoIcon.image = UIImage(named: "e_verify")
//                }else {
//                    cell.infoIcon.image = UIImage(named: "e_unverify")
//                }
                
                if AWSAuth.sharedInstance.customEmail.isEmpty {
                    cell.setData(title: "TripLog email", desc: Constants.emailNotAddedMsg, showInfoIcon: false)
                }else {
                    cell.setData(title: "TripLog email", desc: AWSAuth.sharedInstance.customEmail, showInfoIcon: false)
                }
                
                return cell
            }
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = sections[indexPath.row]
        
        switch item {
        case .username:
            //  Edit username
            if AWSAuth.sharedInstance.isGuestMode() {
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.SettingAccountGuest.UserClick.editName, screenName: ParameterName.SettingAccountGuest.screen_view)
            } else {
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.SettingsAccount.UserClick.edit_name, screenName: ParameterName.settings_account_screen)
            }
            
            goToChangeView(changeName: true)
        case .cardetail:
            //  Show car IU detail screen
            if AWSAuth.sharedInstance.isGuestMode() {
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.SettingAccountGuest.UserClick.editCarDetails, screenName: ParameterName.SettingAccountGuest.screen_view)
            } else {
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.SettingsAccount.UserClick.edit_car_detail, screenName: ParameterName.settings_account_screen)
            }
            
            self.openCarDetail()
        case .triplogEmail:
            //  Show popup enter email
            if AWSAuth.sharedInstance.isGuestMode() {
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.SettingAccountGuest.UserClick.editEmail, screenName: ParameterName.SettingAccountGuest.screen_view)
            } else {
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.SettingsAccount.UserClick.edit_email, screenName: ParameterName.settings_account_screen)
            }
            
            goToChangeView()
        default:
            print("Do nothing")
        }
    }
}

extension UserAccountContainer {
    func goToChangeView(changeName:Bool = false) {
        let storyboard = UIStoryboard(name: "UserUpdate", bundle: nil)
        if let vc = storyboard.instantiateViewController(withIdentifier: String.init(describing: ChangeViewContainer.self)) as? ChangeViewContainer {
            vc.isChangeName = changeName
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

//
//  UpdateEmailVC.swift
//  Breeze
//
//  Created by VishnuKanth on 06/08/21.
//

import UIKit
import Instabug

class UpdateEmailVC: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var emailField: DesignableTextField!
    @IBOutlet weak var errorMsg: UILabel!
    @IBOutlet weak var emailVerifyImageView: UIImageView!
    @IBOutlet weak var resendLinkBtn:UIButton!
    var otpCodeToVerify = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        
        resendLinkBtn.isHidden = true
        emailField.layer.cornerRadius = 25
        emailField.clipsToBounds = true
        emailField.layer.borderColor = UIColor.clear.cgColor
        emailField.layer.borderWidth = 1
        emailField.delegate = self
        emailField.tag = 1
        emailField.returnKeyType = .done
        emailField.enablesReturnKeyAutomatically = true
        
        
        emailField.text  = AWSAuth.sharedInstance.customEmail
        if(AWSAuth.sharedInstance.customEmail == "")
        {
            errorMsg.text = Constants.emailRequired
            emailVerifyImageView.image = UIImage(named: "e_unverify")
        }
        
        if(AWSAuth.sharedInstance.customEmail != "" && AWSAuth.sharedInstance.emailVerifyStatus == false)
        {
            errorMsg.isHidden = true
            emailField.rightImage = UIImage(named: "e_unverify_large")
            emailField.rightPadding = 0
            emailVerifyImageView.isHidden = true
            if(otpCodeToVerify == "")
            {
                resendOTPLink()
            }
            
        }
        
        if(AWSAuth.sharedInstance.customEmail != "" && AWSAuth.sharedInstance.emailVerifyStatus == true)
        {
            errorMsg.isHidden = true
            emailField.rightImage = UIImage(named: "e_verify_large")
            emailField.rightPadding = 0
            emailVerifyImageView.isHidden = true
        }
        
//        NotificationCenter.default.addObserver(self, selector: #selector(receiveOTP), name: Notification.Name(Values.NotificationEmailVerify), object: nil)

        // Do any additional setup after loading the view.
        DispatchQueue.main.async {
            self.emailField.becomeFirstResponder()
        }
    }
        
//    deinit {
//        NotificationCenter.default.removeObserver(self, name: Notification.Name(Values.NotificationEmailVerify), object: nil)
//    }
    
//    @objc func receiveOTP(_ notification: Notification) {
//        
//        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.AccountEditEmail.UserClick.save, screenName: ParameterName.AccountEditEmail.screen_view)
//        
//        if let dictionary = notification.userInfo {
//            
//            otpCodeToVerify = dictionary["OTP"] as! String
//        }
//        if(otpCodeToVerify != ""){
//            
//            self.navigationController?.navigationBar.isHidden = true
//            AWSAuth.sharedInstance.verifyAttributeEmail(code: otpCodeToVerify) { result in
//                
//                
//                DispatchQueue.main.async {
//                    
//                    AWSAuth.sharedInstance.emailVerifyStatus = true
//                    self.resendLinkBtn.isHidden = true
//                    self.errorMsg.isHidden = false
//                    self.errorMsg.text = Constants.emailVerifySuccess
//                    self.errorMsg.textColor = UIColor.instructionViewBackgroundColor2
//                    self.emailField.rightImage = UIImage(named: "e_verify_large")
//                    self.emailField.rightPadding = 0
//                }
//                
//                DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
//                    
//                    self.navigationController?.popViewController(animated: true)
//                }
//                
//            } onFailure: { error in
//                
//                DispatchQueue.main.async {
//                    
//                    AWSAuth.sharedInstance.emailVerifyStatus = true
//                    self.resendLinkBtn.isHidden = false
//                    self.errorMsg.isHidden = false
//                    self.errorMsg.text = AWSAuth.sharedInstance.getRelevantErrorMessage(error: error)
//                    self.errorMsg.textColor = UIColor.instructionViewBackgroundColor2
//                    self.emailField.rightImage = UIImage(named: "e_unverify_large")
//                    self.emailField.rightPadding = 0
//                }
//                
//            }
//
//        }
//    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
//        if(otpCodeToVerify != ""){
//
//            self.navigationController?.navigationBar.isHidden = true
//            AWSAuth.sharedInstance.verifyAttributeEmail(code: otpCodeToVerify) { result in
//
//
//                DispatchQueue.main.async {
//
//                    AWSAuth.sharedInstance.emailVerifyStatus = true
//                    self.resendLinkBtn.isHidden = true
//                    self.errorMsg.isHidden = false
//                    self.errorMsg.text = Constants.emailVerifySuccess
//                    self.errorMsg.textColor = UIColor.instructionViewBackgroundColor2
//                    self.emailField.rightImage = UIImage(named: "e_verify_large")
//                    self.emailField.rightPadding = 0
//                }
//
//            } onFailure: { error in
//
//                DispatchQueue.main.async {
//
//                    AWSAuth.sharedInstance.emailVerifyStatus = true
//                    self.resendLinkBtn.isHidden = false
//                    self.errorMsg.isHidden = false
//                    self.errorMsg.text = AWSAuth.sharedInstance.getRelevantErrorMessage(error: error)
//                    self.errorMsg.textColor = UIColor.instructionViewBackgroundColor2
//                    self.emailField.rightImage = UIImage(named: "e_unverify_large")
//                    self.emailField.rightPadding = 0
//                }
//
//            }
//
//        }
    }
    
    @IBAction func resendClicked(_ sender: Any) {
        
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.AccountEditEmail.UserClick.resend, screenName: ParameterName.AccountEditEmail.screen_view)
        resendOTPLink()
    }
    
    
    // MARK: - UITextFieldDelegate methods
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        //This is just a text field delegate. No harm in keeping this empty
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        AnalyticsManager.shared.logEditEvent(eventValue: ParameterName.AccountEditEmail.UserEdit.input, screenName: ParameterName.AccountEditEmail.screen_view)
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        
        textField.layer.borderColor = isDarkMode ? UIColor.clear.cgColor : UIColor.brandPurpleColor.cgColor
        
        
        if textField == emailField{
            
            //Vishnu - Fix me for updating analytics
//            AnalyticsManager.shared.logEditEvent(eventValue: ParameterName.settings_account_reset_pw_new_input, screenName: ParameterName.settings_account_reset_pw_screen)
            
        }
        
        return true
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if(textField == emailField)
        {
            if !(validateEmail(enteredEmail: emailField.text!)) && emailField.text != ""{
                
                self.errorMsg.isHidden = false
                self.errorMsg.text = Constants.invalidEmail
                // self.alertImg2.isHidden = false
                self.emailVerifyImageView.isHidden = true
                self.errorMsg.textColor = UIColor.passionPinkColor
                
            }
            else
            {
                updateEmail()
            }
        }
        
        
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        // get the current text, or use an empty string if that failed
        let currentText = textField.text ?? ""
        
        // attempt to read the range they are trying to change, or exit if we can't
        guard let stringRange = Range(range, in: currentText) else { return false }
        
        // add their new text to the existing text
        let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
        
        self.errorMsg.isHidden = true
        self.emailVerifyImageView.isHidden = true
        self.emailField.rightImage = UIImage(named: "")
        if(AWSAuth.sharedInstance.customEmail == updatedText && updatedText != "")
        {
            self.emailField.rightImage = UIImage(named: "e_verify_large")
            self.emailField.rightPadding = 0
        }
        return true
    }
    
    func resendOTPLink(){
        
//        AWSAuth.sharedInstance.triggerConfirmationCode { result in
//
//            DispatchQueue.main.async {
//
//                AWSAuth.sharedInstance.customEmail = self.emailField.text!
//                AWSAuth.sharedInstance.emailVerifyStatus = false
//                self.resendLinkBtn.isHidden = false
//                self.errorMsg.isHidden = false
//                self.errorMsg.text = Constants.emailLinkSent
//                self.errorMsg.textColor = UIColor.instructionViewBackgroundColor2
//                self.emailField.rightImage = UIImage(named: "e_unverify_large")
//                self.emailField.rightPadding = 0
//            }
//
//
//        } onFailure: { error in
//
//            DispatchQueue.main.async { [self] in
//
//                self.errorMsg.isHidden = false
//                self.resendLinkBtn.isHidden = true
//                self.errorMsg.text = AWSAuth.sharedInstance.getRelevantErrorMessage(error: error)
//                self.errorMsg.textColor = UIColor.passionPinkColor
//            }
//        }
    }
    
    func updateEmail(){
        
        if(AWSAuth.sharedInstance.customEmail != emailField.text!)
        {
            //  Since version 5.0 will only save data locally
            AWSAuth.sharedInstance.customEmail = self.emailField.text!
            AWSAuth.sharedInstance.emailVerifyStatus = true
            Prefs.shared.setValueWithSub(AWSAuth.sharedInstance.cognitoId, key: .triplogEmail, value: self.emailField.text!)            
            self.resendLinkBtn.isHidden = true
            self.errorMsg.isHidden = false
            self.errorMsg.text = Constants.emailVerifySuccess
            self.errorMsg.textColor = UIColor.instructionViewBackgroundColor2
            self.emailField.rightImage = UIImage(named: "e_verify_large")
            self.emailField.rightPadding = 0
            
            self.navigationController?.popViewController(animated: true)
        }
        else
        {
            self.emailField.rightImage = UIImage(named: "e_verify_large")
            self.emailField.rightPadding = 0
        }
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */    
}

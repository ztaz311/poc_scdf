//
//  EmailPopupVC.swift
//  Breeze
//
//  Created by Zhou Hao on 6/8/21.
//

import UIKit

protocol EmailPopupVCDelegate: AnyObject {
    func onDismiss()
    func onConfirm()
}

class EmailPopupVC: UIViewController {

    @IBOutlet weak var popupView: UIView!
    
    // MARK: - Public properties
    weak var delegate: EmailPopupVCDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

//        setupDarkLightAppearance(forceLightMode: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        popupView.layer.cornerRadius = 24
        popupView.layer.masksToBounds = true
    }

    @IBAction func onConfirm(_ sender: Any) {
        delegate?.onConfirm()
        dismiss()
    }

    @IBAction func onCancel(_ sender: Any) {
        delegate?.onDismiss()
        dismiss()
    }
    
    private func dismiss() {
        AnalyticsManager.shared.logClosePopupEvent(popupName: ParameterName.Home.UserPopup.email, screenName: ParameterName.Home.screen_view)
        removeChildViewController(self)
    }
    
}

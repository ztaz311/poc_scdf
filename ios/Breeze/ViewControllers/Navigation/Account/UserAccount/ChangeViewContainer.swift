//
//  ChangeViewContainer.swift
//  Breeze
//
//  Created by VishnuKanth on 30/05/21.
//

import UIKit

class ChangeViewContainer: BaseViewController {
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var containerView:UIView!
    var changeNameVC: ChangeNameVC!
    @IBOutlet weak var backButton:UIButton!
    @IBOutlet weak var closeBtn:UIButton!
    var isChangeName = false
    var emailChangeHeaderName = ""
    var otpCodeToVerify = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        topView.showBottomShadow()
//        setupDarkLightAppearance(forceLightMode: true)
        
        let storyboard = UIStoryboard(name: "UserUpdate", bundle: nil)
        if(isChangeName == true)
        {
            closeBtn.isHidden = true
            headerLabel.text = "Name"
            if let vc = storyboard.instantiateViewController(withIdentifier: String.init(describing: ChangeNameVC.self)) as? ChangeNameVC {
                vc.delegate = self
                loadEmbededView(viewController: vc)
            }
        }
        else
        {
            
            headerLabel.text = "Email"
            if(emailChangeHeaderName != "")
            {
                headerLabel.text = emailChangeHeaderName
                backButton.isHidden = true
                
            }
            else
            {
                closeBtn.isHidden = true
            }
            if let vc = storyboard.instantiateViewController(withIdentifier: String.init(describing: UpdateEmailVC.self)) as? UpdateEmailVC {
                
                vc.otpCodeToVerify = otpCodeToVerify
                loadEmbededView(viewController: vc)
            }
            
            
            //Vishnu - Commenting this as we don't need changed password any more
            /*headerLabel.text = "Set new password"
            if let vc = storyboard.instantiateViewController(withIdentifier: String.init(describing: ChangePwdVC.self)) as? ChangePwdVC {
                vc.delegate = self
                loadEmbededView(viewController: vc)
            }*/
        }
        
    }
    
    override var useDynamicTheme: Bool {
        return true
    }

    func loadEmbededView(viewController:UIViewController){
        
        viewController.willMove(toParent: self)
        viewController.view.frame = containerView.bounds
        self.containerView.addSubview(viewController.view)
        addChild(viewController)
        viewController.didMove(toParent: self)
        
    }
    
    
    @IBAction func onBack(_ sender: Any) {
        
        if ( isChangeName){
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.AccountEditName.UserClick.back, screenName: ParameterName.AccountEditName.screen_view)
//            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.back, screenName: ParameterName.settings_account_reset_name_screen)
        }else{
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.AccountEditEmail.UserClick.back, screenName: ParameterName.AccountEditEmail.screen_view)
            
            //Vishnu - Fix me - Analytics
            //AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.back, screenName: ParameterName.settings_account_reset_pw_screen)
        }
        if(emailChangeHeaderName != "")
        {
            self.dismiss(animated: true, completion: nil)
        }
        else
        {
            self.moveBack()
        }
        
    }
    
    
    /*override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     
     switch segue.destination {
     
     case let vc as ChangeNameVC:
     changeNameVC = vc
     changeNameVC.delegate = self
     default:
     break
     }
     }*/
    
}

extension ChangeViewContainer:ChangeDetailsDelegate{
    func onChangeNameSuccess() {
        self.moveBack()
    }
    
    
    
}

extension ChangeViewContainer:SuccessScreenControllerDelegate{
    
    func SuccessScreenControllerDismiss() {
        
        self.moveBack()
    }
}

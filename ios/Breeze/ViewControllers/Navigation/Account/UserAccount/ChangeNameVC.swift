//
//  ChangeNameVC.swift
//  Breeze
//
//  Created by VishnuKanth on 30/05/21.
//

import UIKit
import Instabug

protocol ChangeDetailsDelegate: NSObjectProtocol {
    func onChangeNameSuccess()
}

class ChangeNameVC: BaseViewController,
                    UITextFieldDelegate {
    
    static let maxNameLength: Int = 14
    
    weak var delegate: ChangeDetailsDelegate?
    @IBOutlet var userNameField: DesignableTextField!
    
    @IBOutlet var saveBtn: UIButton!
    @IBOutlet var alertLbl: UILabel!
    
    var textCounterLbl: UILabel!
    var textCounterView: UIView!
    var nameText = ""
    
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        userNameField.layer.cornerRadius = 25
        userNameField.clipsToBounds = true
        userNameField.layer.borderColor = UIColor.clear.cgColor
        userNameField.layer.borderWidth = 1
        userNameField.delegate = self
        userNameField.tag = 0
        
        
        textCounterView = UIView(frame: CGRect(x: 0, y: 0, width: 90, height: 25))
        self.textCounterLbl = UILabel(frame: CGRect(x: 0, y: 0, width: 90, height: 20))
        self.textCounterLbl.text = "0"
        self.textCounterLbl.textColor = UIColor.rgba(r: 119, g: 129, b: 136, a: 1.0)
        
        self.textCounterLbl.font =  UIFont(name:"Quicksand",size:14)
        textCounterView.addSubview(self.textCounterLbl)
        
        userNameField.rightView = textCounterView
        userNameField.rightViewMode = .always
        self.textCounterLbl.textAlignment = .center
        
        self.alertLbl.isHidden = true
        
        userNameField.enablesReturnKeyAutomatically = true
        userNameField.text = AWSAuth.sharedInstance.userName
        nameText = AWSAuth.sharedInstance.userName
        
        updateCountLabel(text: nameText)
        
        userNameField.returnKeyType = .done
        enableSaveBtn()

       
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(handle(keyboardWillShowNotification:)),
                                               name: UIResponder.keyboardWillShowNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(handle(keyboardWillHideNotification:)),
                                               name: UIResponder.keyboardWillHideNotification,
                                               object: nil)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(passwordDismiss))
        view.addGestureRecognizer(tap)
        // Do any additional setup after loading the view.
        
        DispatchQueue.main.async {
            self.userNameField.becomeFirstResponder()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        AnalyticsManager.shared.logScreenView(screenName: ParameterName.settings_account_reset_name_screen)
    }
    
    @objc private func handle(keyboardWillShowNotification notification: Notification) {
       
        if let userInfo = notification.userInfo,
          
            let keyboardRectangle = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect {
            print(keyboardRectangle.height)
            self.bottomConstraint.constant = keyboardRectangle.height + 30
        }
    }
    
    @objc private func handle(keyboardWillHideNotification notification: Notification) {
        
         self.bottomConstraint.constant = 30
      
    }
    
    @objc func passwordDismiss() {
        
        view.endEditing(true)
        
    }
    
    func enableSaveBtn(){
        saveBtn.isEnabled = true
        
        self.saveBtn.applyGradient(colors: [UIColor.brandPurpleColor.cgColor,UIColor.brandPurpleColor.cgColor], withShadow: false)
    }
    
    func disableSaveButton(){
        saveBtn.applyGradient(colors: [UIColor.defaultGreyGradientColor1.cgColor,UIColor.defaultGreyGradientColor2.cgColor], withShadow: false)
        saveBtn.isEnabled = false
    }
    

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        AnalyticsManager.shared.logEditEvent(eventValue: ParameterName.AccountEditName.UserEdit.input, screenName: ParameterName.AccountEditName.screen_view)
        
        userNameField.layer.borderColor = UIColor.clear.cgColor
       
        textField.layer.borderColor = isDarkMode ? UIColor.clear.cgColor : UIColor.brandPurpleColor.cgColor
        
        verifyInputs()
        
        return true
    }
    
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == userNameField{
            if !(validateUsername(enteredUsername: userNameField.text!)) {
                self.alertLbl.text = Constants.usernameInvalid
                self.alertLbl.isHidden = false
                self.userNameField.layer.borderColor = UIColor.red.cgColor
                
                return false
            }
            else
            {
                signUPAwsAuth()
            }
        }
        
        
        // Do not add a line break
        return false
    }
    
    
    
    // Use this if you have a UITextField
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        // get the current text, or use an empty string if that failed
        let currentText = textField.text ?? ""
        
        // attempt to read the range they are trying to change, or exit if we can't
        guard let stringRange = Range(range, in: currentText) else { return false }
        
        // add their new text to the existing text
        let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
        
        if(textField == userNameField){
            nameText = updatedText
        }
        
        updateCountLabel(text: updatedText)
        
        if updatedText.count > ChangeNameVC.maxNameLength {
            nameText = userNameField.text ?? ""
        }
        if updatedText.count >= ChangeNameVC.maxNameLength {
            self.textCounterLbl.text = "0"
        }
       
        validateInput()
        verifyInputs()
        
        return updatedText.count <= ChangeNameVC.maxNameLength
        
    }
    
    func verifyInputs(){
        self.saveBtn.removeGradient()
        if(nameText != "")
        {
            enableSaveBtn()
            /*if (self.alertLbl.isHidden){
                enableSaveBtn()
            }else{
                disableSaveButton()
            }*/

           
        }
        else{
            disableSaveButton()
        }
        
    }
    
    private func updateCountLabel(text: String) {
        let remainCount = ChangeNameVC.maxNameLength - text.count
        self.textCounterLbl.text = "\(remainCount)"
    }
    

    func processSignUp(){
        
        if (validateUsername(enteredUsername: nameText)) && nameText != ""
        {
            //  Since version 5.0 just save user name in local storage
            AWSAuth.sharedInstance.userName = nameText
            Prefs.shared.setValueWithSub(AWSAuth.sharedInstance.cognitoId, key: .username, value: nameText)
            delegate?.onChangeNameSuccess()
        }
        
    }
    
    @IBAction func signUPAwsAuth()
    {
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.AccountEditName.UserClick.save, screenName: ParameterName.AccountEditName.screen_view)
        
        ReachabilityManager.shared.isReachable(success: {
            self.processSignUp()
          }, failure: {
            self.popupAlert(title: Constants.noInternetTitle, message: Constants.noInternetMessage, actionTitles: [Constants.gotIt], actions:[{action1 in
                self.dismissAnyAlertControllerIfPresent()

            },{action2 in

                //No need to handle this
            }, nil])
          
          })
        
    }
    
    
    func validateInput() {
        
        if !(validateUsername(enteredUsername: nameText)) && nameText != ""{
            self.alertLbl.text = Constants.usernameInvalid
            //self.alertImg.isHidden = false
            self.alertLbl.isHidden = false
            self.userNameField.layer.borderColor = UIColor.red.cgColor
            
            
        }
        else
        {
            self.alertLbl.isHidden = true
            userNameField.layer.borderColor = UIColor.clear.cgColor
            userNameField.layer.borderColor = UIColor.brandPurpleColor.cgColor
            verifyInputs()
        }
        
    }
    
    
}



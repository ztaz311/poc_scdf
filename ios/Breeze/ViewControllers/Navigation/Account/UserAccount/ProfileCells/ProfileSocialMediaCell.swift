//
//  ProfileSocialMediaCell.swift
//  Breeze
//
//  Created by Tuan, Pham Hai on 08/02/2023.
//

import UIKit

class ProfileSocialMediaCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var providerLabel: UILabel!
    @IBOutlet weak var providerLogo: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()

        self.titleLabel.text = "Signed in with"
        self.providerLabel.text = (AWSAuth.sharedInstance.providerName)
        if (providerLabel.text == "Google") {
            self.providerLogo.image = UIImage(named: "googleIcon")
        }else if (providerLabel.text == "SignInWithApple") {
            self.providerLogo.image = UIImage(named: "appleIcon")
        }else  {
            self.providerLogo.image = UIImage(named: "fbBlue")
        }
        
        if appDelegate().isDarkMode() {
            titleLabel.textColor = .white
            providerLabel.textColor = .white
        }
    }
}

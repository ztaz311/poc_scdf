//
//  GuestInfoCell.swift
//  Breeze
//
//  Created by Tuan, Pham Hai on 07/02/2023.
//

import UIKit

class GuestInfoCell: UITableViewCell {
    @IBOutlet weak var infoLabel: UILabel!
    
    @IBOutlet weak var content: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setupUI() {
        infoLabel.text = "You are currently using Breeze without a registered account. \n\nYour data will only be saved on this device. Breeze does not store any data. When you delete Breeze or change to a new mobile device and download Breeze again, all previously saved information will not be reinstated."
        if appDelegate().isDarkMode() {
            infoLabel.textColor = .white
            content.backgroundColor = UIColor(hexString: "393F58", alpha: 1)
        }
    }
}

//
//  ProfileTableViewCell.swift
//  Breeze
//
//  Created by Tuan, Pham Hai on 07/02/2023.
//

import UIKit

class ProfileTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var infoIcon: UIImageView!
    @IBOutlet weak var arrowIcon: UIImageView!
    
    func setData(title: String, desc: String, showInfoIcon: Bool = false, showArrowIcon: Bool = true) {
        titleLabel.text = title
        descLabel.text = desc
        infoIcon.isHidden = !showInfoIcon
        arrowIcon.isHidden = !showArrowIcon
        
        if appDelegate().isDarkMode() {
            titleLabel.textColor = .white
        }
    }
}

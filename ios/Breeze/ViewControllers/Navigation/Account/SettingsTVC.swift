//
//  SettingsTVC.swift
//  Breeze
//
//  Created by Malou Mendoza on 28/5/21.
//

import UIKit
import Instabug
import Combine

protocol SettingsTVCDelegate: AnyObject {

    func onAccount()
    func onMapDisplay()
    func onRoutePreference()
    func onAbout()
   
}

extension UserDefaults {
    @objc dynamic var debugEnabled: Bool {
        return bool(forKey: Values.debugEnabled)
    }
}

final class SettingsTVC: UITableViewController {

    enum TableRows: Int, CaseIterable {
        case account = 0
        case preference = 1
        case privacy = 2
        case terms = 3
        case faq = 4
//        case feedback = 5
        case about = 5
        case debug = 6
    }
    
    @IBOutlet weak var lblVersion: UILabel!
//    @IBOutlet weak var mapDisplay: UILabel!
    @IBOutlet weak var routePreference: UILabel!
    
    weak var delegate: SettingsTVCDelegate?
    
    private var subscriber: AnyCancellable?
    private var disposables = Set<AnyCancellable>()
    private var preferencesVC: UIViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView()
        self.ShowAndHideFloatingView(value: true)
        self.lblVersion.text = "V\(UIApplication.appVersion())"
        
        NotificationCenter.default.publisher(for: Notification.Name(Values.NotificationOpenMapSettings), object: nil)
            .receive(on: DispatchQueue.main)
            .sink { [weak self] notification in
                guard let self = self, let preferencesVC = self.preferencesVC else { return }
                let storyboard = UIStoryboard(name: "Account", bundle: nil)
                if let vc = storyboard.instantiateViewController(withIdentifier: String.init(describing: SettingsDetailVC.self)) as? SettingsDetailVC {
                    vc.setLabelHeader(headerLbl: "Map Display", segueID: "mapDisplaySegue")
                    preferencesVC.navigationController?.pushViewController(vc, animated: true)
                }
                
        }.store(in: &disposables)

        subscriber = UserDefaults.standard
            .publisher(for: \.debugEnabled)
            .sink() { [weak self] _ in
                guard let self = self else { return }
                self.tableView.reloadData()
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        AnalyticsManager.shared.logScreenView(screenName: ParameterName.settings_screen)
        setupDarkLightAppearance()

//        self.mapDisplay.text = UserSettingsModel.sharedInstance.mapDisplay
        //self.routePreference.text = UserSettingsModel.sharedInstance.routePref
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //self.ShowAndHideFloatingView(value: false)
        preferencesVC = nil
    }
    
    // MARK: - functions for each setting
    private func onSettings() {
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Settings.UserClick.account, screenName: ParameterName.Settings.screen_view)

        let storyboard = UIStoryboard(name: "Account", bundle: nil)
        if let vc = storyboard.instantiateViewController(withIdentifier: String.init(describing: UserAccountContainer.self)) as? UserAccountContainer {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    private func onPreferences() {
        // TODO: To check if the the name of the parameter is correct
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Settings.UserClick.preferences, screenName: ParameterName.Settings.screen_view)
        // Call RN function to show preference screen
        preferencesVC = nil
        preferencesVC = self.openRNScreen(toScreen: RNScreeNames.PREFERENCE_SETTING,navigationParams: [:] as [String:Any])
        
//        let storyboard = UIStoryboard(name: "Account", bundle: nil)
//        if let vc = storyboard.instantiateViewController(withIdentifier: String.init(describing: SettingsDetailVC.self)) as? SettingsDetailVC {
//            vc.setLabelHeader(headerLbl: "Map Display", segueID: "mapDisplaySegue")
//
//            self.navigationController?.pushViewController(vc, animated: true)
//        }
    }
    
    private func onPrivacy() {
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Settings.UserClick.privacy_policy, screenName: ParameterName.Settings.screen_view)
        onWeb(urlString: Configuration.privacyPolicyURL, header: "Privacy Policy")
    }
    
    private func onTermsAndConditions() {
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Settings.UserClick.terms_condition, screenName: ParameterName.Settings.screen_view)
        onWeb(urlString: Configuration.termsOfUseURL, header: "Terms and Conditions")
    }
    
    private func onFAQ() {
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Settings.UserClick.faq, screenName: ParameterName.Settings.screen_view)
        onWeb(urlString: Configuration.faqURL, header: "FAQ")
    }
    
    private func onAbout() {
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Settings.UserClick.about, screenName: ParameterName.Settings.screen_view)
        onWeb(urlString: Configuration.aboutUsURL, header: "About")
    }
    
    private func onWeb(urlString: String, header: String) {
        let storyboard: UIStoryboard = UIStoryboard(name: "Web", bundle: nil)
        let vc: WebVC = storyboard.instantiateViewController(withIdentifier: "WebVC") as! WebVC
        vc.isAccountCreation = false
        vc.headerString = header
        vc.urlString = urlString
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    private func onDebug() {
        let storyboard: UIStoryboard = UIStoryboard(name: "Account", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "DebugVC") as! DebugVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // MARK: - Table view Delegate
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if UserDefaults.standard.bool(forKey: Values.debugEnabled) {
            return TableRows.allCases.count
        }
        return TableRows.allCases.count - 1
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.row == TableRows.account.rawValue {
            self.onSettings()
        }else if indexPath.row == TableRows.preference.rawValue {
            self.onPreferences()
        }
        else if indexPath.row == TableRows.privacy.rawValue {
            self.onPrivacy()
        } else if indexPath.row == TableRows.terms.rawValue {
            self.onTermsAndConditions()
        } else if indexPath.row == TableRows.faq.rawValue {
            self.onFAQ()
        } else if indexPath.row == TableRows.about.rawValue {
            self.onAbout()
        } else if indexPath.row == TableRows.debug.rawValue {
            self.onDebug()
        }
    }

}

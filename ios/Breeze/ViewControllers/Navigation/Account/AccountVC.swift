//
//  AccountVC.swift
//  Breeze
//
//  Created by Zhou Hao on 3/3/21.
//

import UIKit
import Instabug

// FIXME: To be removed (including related code in MapLandingVC and storyboard)!!!
class AccountVC: UIViewController {
    
    @IBOutlet weak var topView: UIView!
    var accountActionVC: AccountActionTVC!
    
    override func viewDidLoad() {
        super.viewDidLoad()
                
        topView.showBottomShadow()
        setupDarkLightAppearance(forceLightMode: true)
    }
    
    @IBAction func onBack(_ sender: Any) {
        self.moveBack()
    }
    
    @IBAction func onLogout(_ sender: Any) {
        
        self.popupAlert(title: nil, message: Constants.logoutConfirmationMessage, actionTitles: [Constants.cancel,Constants.logout], actions:[{action1 in
            
            //We don't need to process
            
        },{[weak self] action2 in
            AWSAuth.sharedInstance.signOut(onSuccess: { (result) in
                DispatchQueue.main.async { [weak self] in
                    guard let self = self else { return }
                    if(SavedSearchManager.shared.easyBreezy.count > 0)
                    {
                        SavedSearchManager.shared.easyBreezy.removeAll()
                    }
                    if (AmenitiesSharedInstance.shared.amenitiesSymbolLayer.count > 0){
                        
                        AmenitiesSharedInstance.shared.amenitiesSymbolLayer.removeAll()
                    }
                    
                    SelectedProfiles.shared.clearProfile()
                    Settings.shared.reset()
                    Instabug.logOut()
                    // TODO: Probably we need to adjust this function by replace rootViewController instead of pushViewController!!!
                    self.goToLoginVC()
                }
            }, onFailure: nil)            
        }, nil])
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        switch segue.destination {
        
        case let vc as AccountActionTVC:
            accountActionVC = vc
            accountActionVC.delegate = self
        default:
            break
        }
    }
}

extension AccountVC: AccountActionTVCDelegate {
    func onAvatar() {
        featureNotReady()
    }
    
    func onVehicleType() {
        featureNotReady()
    }
    
    func onSettings() {
        let storyboard = UIStoryboard(name: "Account", bundle: nil)
        if let vc = storyboard.instantiateViewController(withIdentifier: String.init(describing: SettingsVC.self)) as? SettingsVC {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func onAbout() {
        featureNotReady()
    }
    
    private func featureNotReady() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let vc = storyboard.instantiateViewController(withIdentifier: String(describing: FeatureNotReadyVC.self)) as? FeatureNotReadyVC {
            self.present(vc, animated: true, completion: nil)
        }
    }
}

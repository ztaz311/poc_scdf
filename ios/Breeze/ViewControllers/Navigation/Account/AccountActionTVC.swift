//
//  AccountActionTVC.swift
//  Breeze
//
//  Created by Zhou Hao on 3/3/21.
//

import UIKit

protocol AccountActionTVCDelegate: class {

    func onAvatar()
    func onVehicleType()
    func onSettings()
    func onAbout()
}

class AccountActionTVC: UITableViewController {

    weak var delegate: AccountActionTVCDelegate?
    
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblVersion: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.tableFooterView = UIView()
        self.lblUserName.text = AWSAuth.sharedInstance.userName
        
        self.lblVersion.text = "V\(UIApplication.appVersion())"
    }

    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.row == 0 {
            delegate?.onAvatar()
        } else if indexPath.row == 1 {
            delegate?.onVehicleType()
        } else if indexPath.row == 2 {
            delegate?.onSettings()
        } else {
            delegate?.onAbout()
        }
    }
}

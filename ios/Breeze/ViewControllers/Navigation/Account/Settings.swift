//
//  Settings.swift
//  Breeze
//
//  Created by Zhou Hao on 7/4/21.
//

import Foundation
import UIKit
import Combine
import SwiftyBeaver

class Settings: Codable {
    // MARK: default values
    static let defautNotificationDuration = 3.0
    static let defaultMuteNotification = false
    static let defaultTheme = UIUserInterfaceStyle.unspecified.rawValue
    static let defaultRouteOption = 0
    
    static let shared = Settings()
        
    var notificationDuration: Double = defautNotificationDuration
    var muteNotification: Bool = defaultMuteNotification
    
    // UIUserInterfaceStyle
    private(set) var theme: Int = defaultTheme {
        didSet {
            themeUpdatedAction.send()
        }
    }
    
    var routeOption: Int = defaultRouteOption // fastest Route
    let themeUpdatedAction = PassthroughSubject<Void, Never>()
    
    enum CodingKeys: CodingKey {
        case notificationDuration
        case muteNotification
        case theme
        case routeOption
    }
    
    private init() {
        //Empty constructor, we don't need any implementation here
        getSettingAction
            .debounce(for: .seconds(1), scheduler: RunLoop.main)
            .sink { [weak self] _ in
                guard let self = self else { return }
                self.performGetUserSetting()
            }.store(in: &anyCancellables)
    }
    
    private var anyCancellables = Set<AnyCancellable>()
    let getSettingAction = PassthroughSubject<Void, Never>()
}

extension Settings {
    func setTheme(key: String?) {
        if key == UserSettingsModel.MapDisplayValues.mapDisplayKeyAuto {
            theme = 0
        }
        else if key == UserSettingsModel.MapDisplayValues.mapDisplayKeyLight{
            theme = 1
        }
        else if key == UserSettingsModel.MapDisplayValues.mapDisplayKeyDark {
            theme = 2
        }
    }
    
    func setTheme(value: Int) {
        self.theme = value
    }
}

extension Settings {

    public func load(userId: String) {
        if let json = UserDefaults.standard.data(forKey: Values.settingsKey + userId) {
            if let settings = try? JSONDecoder().decode(Settings.self, from: json) {
                self.notificationDuration = settings.notificationDuration
                self.muteNotification = settings.muteNotification
                self.theme = settings.theme
                self.routeOption = settings.routeOption
            }
        }
    }
    
    public func save(userId: String) {
        if let json = try? JSONEncoder().encode(self) {
            UserDefaults.standard.setValue(json, forKey: Values.settingsKey + userId)
        }
    }
    
    public func loadFromSettingsFromAPI(){
        self.getUserSettings()
    }
    
    public func processSettingsData(masterLists: MasterListData, settings:UserSettings){
        
        // have to set the master list first before pass the user setting values 
        UserSettingsModel.sharedInstance.master = masterLists
        UserSettingsModel.sharedInstance.userSettings = settings
        
        sendTriggerUserPreferenceDataEvent()
        
//        UserSettingsModel.sharedInstance.settingsArr = settings.settings!
        if (UserSettingsModel.sharedInstance.mapDisplay != nil) {
            themeUpdatedAction.send()
        }
    }
    
    private func performGetUserSetting() {
        DispatchQueue.global(qos: .background).async {
            // get master list first
            SettingsService.shared.getMasterLists("USER_PREFERENCE") { result in
                switch result {
                case .success(let masterLists):
                    // get user settings
                    SettingsService.shared.getUserSettings { (result) in
                        switch result {
                        case .success(let json):
//                            print("SETTING FETCHED FIRST TIME",json)
                            self.processSettingsData(masterLists: masterLists, settings: json)
                        case .failure(let error):
                            SwiftyBeaver.error("SETTING UPDATED ERROR: \(error.localizedDescription)")
                        }
                    }
                    
                case .failure(let error):
                    SwiftyBeaver.error("Failed to performGetUserSetting: \(error.localizedDescription)")
                }
            }
        }
    }
    
    func getUserSettings() {
        getSettingAction.send()
    }
    
    // reset to default settings when logout
    public func reset() {        
        self.notificationDuration = Settings.defautNotificationDuration
        self.muteNotification = Settings.defaultMuteNotification
        self.theme = Settings.defaultTheme
    }
}

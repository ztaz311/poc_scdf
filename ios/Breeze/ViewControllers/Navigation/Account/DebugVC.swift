//
//  DebugVC.swift
//  Breeze
//
//  Created by Zhou Hao on 27/8/21.
//

import UIKit
import SwiftyBeaver

class DebugVC: UIViewController {

    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var obuButton: UIButton!
    @IBOutlet weak var obuMethodButton: UIButton!
    @IBOutlet weak var obuCardBalanceTF: UITextField!
    @IBOutlet weak var obuCentLabel: UILabel!
    @IBOutlet weak var cardTypeButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupDarkLightAppearance(forceLightMode: true)
        progressView.isHidden = true
        obuButton.isHidden = false //   !OBUHelper.shared.isObuConnected()
        obuButton.setTitle("Change OBU mode: \(OBUHelper.shared.getObuMode().rawValue)", for: .normal)
        
        obuCardBalanceTF.addTarget(self, action: #selector(onChangeObuCardBalance(_:)), for: .editingChanged)
        obuCardBalanceTF.delegate = self
        obuCardBalanceTF.layer.cornerRadius = 24
        obuCardBalanceTF.layer.masksToBounds = true
        obuCardBalanceTF.layer.borderColor = UIColor.brandPurpleColor.cgColor
        obuCardBalanceTF.layer.borderWidth = 1
        obuCardBalanceTF.enablesReturnKeyAutomatically = true
        obuCardBalanceTF.returnKeyType = .done
        obuCardBalanceTF.textColor = .black
        obuCardBalanceTF.placeholder = "Input card balance in cents"
        obuCardBalanceTF.text = "\(Int(OBUHelper.shared.CARD_BALANCE))"
        
        if OBUHelper.shared.getObuMode() != .device {
            obuCardBalanceTF.isHidden = false
            obuCentLabel.isHidden = false
            cardTypeButton.isHidden = false
            cardTypeButton.setTitle((OBUHelper.shared.getPaymentMode() == .backend ? "Payment Type: Debit/Credit Card" : "Payment Type: Frontend Card"), for: .normal)
        }
        
#if APPSTORE
    //  Prod
        obuButton.isHidden = true
        obuCardBalanceTF.isHidden = true
        obuCentLabel.isHidden = true
        cardTypeButton.isHidden = true
        obuMethodButton.isHidden = true
#else
    //  Others
    
#endif
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if self.obuCardBalanceTF.isFirstResponder {
            self.obuCardBalanceTF.resignFirstResponder()
        }
        super.viewWillDisappear(animated)
    }
    
    @IBAction func onBack(_ sender: Any) {
//        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.back, screenName: ParameterName.settings_screen)
//        self.ShowAndHideFloatingView(value: false)
        self.moveBack()
    }

    @IBAction func onUpload(_ sender: Any) {
        
        let ac = UIAlertController(title: "Submit debug log", message: "Please add a short description before submission", preferredStyle: .alert)
        ac.addTextField()

        let submitAction = UIAlertAction(title: "Submit", style: .default) { [weak ac, weak self] _ in
            guard let ac = ac, let self = self else { return }
            
            let description = ac.textFields![0].text
            
            let fm = FileManager.default
            
            do{
                var logurl = try fm.url(for:.documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
                logurl.appendPathComponent("/breeze.log")

                self.progressView.isHidden = false
                self.progressView.progress = 0
                
                var userName = "\(AWSAuth.sharedInstance.cognitoId)"
                if userName.isEmpty {
                    userName = "UnknownUser"
                }
                
                FirebaseFeedbackService().submit(userId: userName, description: description ?? "NA", fromLog: true, url: logurl) { [weak self] progress in
                    guard let self = self else { return }
                    self.progressView.progress = progress / 100
                } completion: { result in
                    self.progressView.isHidden = true
                    switch result {
                    case .failure(let error):
                        showToast(from: self.view, message: "Failed to submit feedback", topOffset: 0, icon: "alertIcon")
                        SwiftyBeaver.error(error)
                        
                    case .success(_):
                        SwiftyBeaver.info("log submitted by \(AWSAuth.sharedInstance.cognitoId)")
                        // delete after send
                        try? FileManager.default.removeItem(at: logurl)
                        showToast(from: self.view, message: "Submit successful", topOffset: 0, icon: "successFeedback")
                        break
                    }
                }
            }
            catch(let error){
                print(error.localizedDescription)
            }

        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { _ in
            
            //No need to handle this action
        }
        ac.addAction(submitAction)
        ac.addAction(cancelAction)

        present(ac, animated: true)
    }
    
    @IBAction func onExitDebug(_ sender: Any) {
        UserDefaults.standard.setValue(false, forKey: Values.debugEnabled)
        
        moveBack()
    }
    
    @IBAction func onChangeObuSetting(_ sender: Any) {
        self.showDemoOptions()
    }
    
    @IBAction func onChangePaymentMode(_ sender: Any) {
        self.showDemoCardTypes()
    }
    
    @objc func onChangeObuCardBalance(_ sender: Any) {
        if let balance = Float(obuCardBalanceTF.text ?? "10000") {
            OBUHelper.shared.CARD_BALANCE = balance
            Prefs.shared.setValue(balance, forkey: .obuCardBalance)
        }
    }
    
    private func showDemoCardTypes() {
        let alertVC = UIAlertController(title: "Card Type", message: "Please choose card type", preferredStyle: .alert)
        
        // create a cancel action
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            // handle cancel response here. Doing nothing will dismiss the view.
        }
        // add the cancel action to the alertController
        alertVC.addAction(cancelAction)
        
        let backendAction = UIAlertAction(title: "Debit/Credit Card", style: .default) { (action) in
            OBUHelper.shared.setPaymentMode(.backend)
            self.cardTypeButton.setTitle("Payment Type: Debit/Credit Card", for: .normal)
        }
        
        // create payment mode
        let frontendAction = UIAlertAction(title: "Frontend Card", style: .default) { (action) in
            OBUHelper.shared.setPaymentMode(.frontend)
            self.cardTypeButton.setTitle("Payment Type: Frontend card", for: .normal)
        }
        
        alertVC.addAction(backendAction)
        alertVC.addAction(frontendAction)
        self.present(alertVC, animated: true)
    }
    
    private func showDemoOptions() {
        
        let alertVC = UIAlertController(title: "Mock Data Alert", message: "Please choose mock data source", preferredStyle: .alert)
        // create a cancel action
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            // handle cancel response here. Doing nothing will dismiss the view.
        }
        // add the cancel action to the alertController
        alertVC.addAction(cancelAction)
        
        let deviceAction = UIAlertAction(title: "Use real OBU device", style: .default) { (action) in
            OBUHelper.shared.setMode(.device)
            self.obuCardBalanceTF.isHidden = true
            self.obuCentLabel.isHidden = true
            self.obuButton.setTitle("Change OBU mode: \(OBUHelper.shared.getObuMode().rawValue)", for: .normal)
            self.cardTypeButton.isHidden = true
        }

        // create mock data from SDK
//        let mockManagerAction = UIAlertAction(title: "Use OBU mock manager", style: .default) { (action) in
//            OBUHelper.shared.setMode(.mockSDK)
//            self.obuButton.setTitle("Change OBU mode: \(OBUHelper.shared.getObuMode().rawValue)", for: .normal)
//            self.cardTypeButton.isHidden = false
//            cardTypeButton.setTitle((OBUHelper.shared.getPaymentMode() == .backend ? "Payment Type: Debit/Credit Card" : "Payment Type: Frontend Card"), for: .normal)
//        }
        
        // create mock data from server
        let serverBreezeAction = UIAlertAction(title: "Use Breeze Server", style: .default) { (action) in
            OBUHelper.shared.setMode(.mockServer)
            self.obuCardBalanceTF.isHidden = false
            self.obuCentLabel.isHidden = false
            self.obuButton.setTitle("Change OBU mode: \(OBUHelper.shared.getObuMode().rawValue)", for: .normal)
            self.cardTypeButton.isHidden = false
            self.cardTypeButton.setTitle((OBUHelper.shared.getPaymentMode() == .backend ? "Payment Type: Debit/Credit Card" : "Payment Type: Frontend Card"), for: .normal)
        }
        
        // add the OK action to the alert controller
        alertVC.addAction(deviceAction)
//        alertVC.addAction(mockManagerAction)
        alertVC.addAction(serverBreezeAction)
        
        self.present(alertVC, animated: true)

    }
    
    @IBAction func onChangeOBUConnectMethod(_ sender: Any) {
        let alertVC = UIAlertController(title: "OBU connect method", message: "Please choose method to connect OBU", preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            // handle cancel response here. Doing nothing will dismiss the view.
        }
        alertVC.addAction(cancelAction)
        
        let withoutVehicleNumberAction = UIAlertAction(title: "Connect OBU without Vehicle Number", style: .default) { (action) in
            OBUHelper.shared.setConnectMethod(.withoutVehicleNumber)
            self.obuMethodButton.setTitle("Connect OBU without Vehicle Number", for: .normal)
        }
        // create mock data from server
        let withVehicleNumberAction = UIAlertAction(title: "Connect OBU with Vehicle Number", style: .default) { (action) in
            OBUHelper.shared.setConnectMethod(.withVehicleNumber)
            self.obuMethodButton.setTitle("Connect OBU with Vehicle Number", for: .normal)
        }
        
        alertVC.addAction(withoutVehicleNumberAction)
        alertVC.addAction(withVehicleNumberAction)
        self.present(alertVC, animated: true)
    }
}

extension DebugVC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        obuCardBalanceTF.resignFirstResponder()
        return true
    }
}

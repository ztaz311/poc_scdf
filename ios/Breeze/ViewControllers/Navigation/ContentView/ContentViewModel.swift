//
//  ContentViewModel.swift
//  Breeze
//
//  Created by VishnuKanth on 27/07/22.
//

import Foundation
import CoreGraphics
import CoreLocation
import UIKit
import Combine
import SwiftyBeaver
import Turf
import MapboxNavigation
import MapboxCoreNavigation
import MapboxMaps
import MapboxCoreMaps

protocol ContentViewModelDelegate: NSObjectProtocol {
    func navigateHere(address:SearchAddresses?, carPark:Carpark?, layerCode: String?, amenityId: String?, amenityType: String?, amenityCarPark: String?)
    func dismissTBRBeforeAddingNew()
    func toolTipMoreInfo(poi:POI?,carPark:Carpark?,screenName:String)
    func toolTipOpen()
    func selectedAmenity(id: String, isSelected: Bool)
    func startWalkingPath(poi:POI?, trackNavigation: Bool, amenityId: String?, name: String?, canReroute: Bool?)
    func closeDropPin()
}

final class ContentViewModel: NSObject {
    
    var bottomSheetIndex = 1
    var bottomSheetHeight = 463
    let offset = 80
    var carparkUpdater: CarParkUpdater?
    var queryRenderLayer: QueryRenderLayer?
    var contentGlobalPOICoordinates = [CLLocationCoordinate2D]()
    var shareCoordinates = [CLLocationCoordinate2D]()
    var showZoneAndCurrentLocation = false
    weak var delgate: ContentViewModelDelegate?

    private var contentType = ""
    private var disposables = Set<AnyCancellable>()
    private var bookMarks:[Bookmark] = []
    
    var currentToolTip: ToolTipsView? {
        if let callout = queryRenderLayer?.callOutView {
            return callout
        }
        
        if let carparkTooltip = carparkUpdater?.calloutView {
            return carparkTooltip
        }
        return nil
    }

    // MARK: - Public properties
    var mobileContentMapViewUpdatable: ContentViewUpdatable? {
        didSet {
            if mobileContentMapViewUpdatable != nil {
                syncUpdate(mapViewUpdatable: mobileContentMapViewUpdatable!)
            }
        }
    }
    
    @Published var isTrackingUser = true
    @Published var currentSelectedContent: ContentBase?
    var onAdjustCenter: ((_ y: Int, _ index: Int) -> Void)?
    var onBottomTabClicked: ((_ screenName: String) -> Void)?
    var dismissChildView: ((_ screenName:String) -> Void)?
    
    func adjustCallout() {
         if let callout = currentToolTip {
             let location = callout.tooltipCoordinate
             
             guard let screenCoordinate =  mobileContentMapViewUpdatable?.navigationMapView.mapView.mapboxMap.point(for: location) else { return }
             
             let point = CGPoint(x: screenCoordinate.x, y: screenCoordinate.y)
             callout.center = CGPoint(x: point.x, y: point.y - 20)
         }
    }
    
    func deselectTooltip() {
        
        queryRenderLayer?.carparkUpdater = carparkUpdater
        queryRenderLayer?.didDeselectAmenityAnnotation()
        self.hideParking()
        carparkUpdater?.didDeselectCarparkSymbol()
    }
    
    // MARK: - Private methods
    /// Sync up UI based on the current status
    private func syncUpdate(mapViewUpdatable: ContentMapViewUpdatable) {
        startMonitorContentSelectionChange()
        startMonitorTBRBackClick()
        startMonitorAdjectRecenter()
        startMonitorBottomTabClicked()
        startMonitorAdjectRecenterCollectionDetails()
        mapViewUpdatable.setupCustomUserLocationIcon()
        mapViewUpdatable.defaultCameraWithCurrentLocation(bottomSheetHeight: bottomSheetHeight)
    }
    private func startMonitorTBRBackClick(){
        
        NotificationCenter.default.publisher(for: Notification.Name(Values.NotificationExploreTBRDetailsBack), object: nil)
            .sink { [weak self] notification in
                
            guard let self = self else { return }
                
            self.dismissChildView?("")

        }.store(in: &disposables)
    }
    private func startMonitorContentSelectionChange() {

        NotificationCenter.default.publisher(for: Notification.Name(Values.NotificationSelectContent), object: nil)
            .sink { [weak self] notification in
            
            guard let self = self else { return }
                
            if let id = notification.userInfo?[Values.ContentIdKey] as? Int {
                self.contentChanged(id: id)
            }
        }.store(in: &disposables)
    }
    
    private func startMonitorAdjectRecenter() {
        NotificationCenter.default.publisher(for: Notification.Name(Values.NotificationAdjustRecenterExploreMap), object: nil)
            .sink { [weak self] notification in
                
            guard let self = self else { return }
                
            if let y = notification.userInfo![Values.AdjustRecentKey] as? Int, let index = notification.userInfo![Values.BottomSheetIndex] as? Int {
                self.bottomSheetHeight = y
                self.bottomSheetIndex = index
                self.onAdjustCenter?(y, index)
            }

        }.store(in: &disposables)
    }
    
    private func startMonitorAdjectRecenterCollectionDetails() {
        NotificationCenter.default.publisher(for: Notification.Name(Values.NotificationAdjustRecenterCollection), object: nil)
            .sink { [weak self] notification in
                
            guard let self = self else { return }
                
            if let y = notification.userInfo![Values.AdjustRecentKey] as? Int, let index = notification.userInfo![Values.BottomSheetIndex] as? Int {
                self.bottomSheetHeight = y
                self.bottomSheetIndex = index
                self.onAdjustCenter?(y, index)
            }

        }.store(in: &disposables)
    }
    
    private func startMonitorBottomTabClicked() {
        NotificationCenter.default.publisher(for: Notification.Name(Values.NotificationExploreMapBottomTabClicked), object: nil)
            .sink { [weak self] notification in
                
            guard let self = self else { return }
                
            if let screenName = notification.userInfo![Values.ScreenNameKey] as? String {
                self.onBottomTabClicked?(screenName)
            }

        }.store(in: &disposables)

    }
    
    private func contentChanged(id: Int) {
        ContentService().getContentDetails(id: id) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let content):
                self.currentSelectedContent = content
                break
            case .failure(let error):
                SwiftyBeaver.error("Failed to get content details: \(error.localizedDescription)")
                break
            }
        }
    }
    
    private func getBookmarkAmenitiesFeatures(bookmark: Bookmark,type:String = "") -> Turf.Feature? {
        guard let lat = bookmark.lat?.toDouble(),
              let long = bookmark.long?.toDouble(),
              let idBookmark = type != "" ? 0 : bookmark.bookmarkId,
              let amenityCarPark = bookmark.amenityId,
              let description = bookmark.welcomeDescription,
              let name = type != "" ? "" : bookmark.name else {
            return nil
        }
        let address = bookmark.bookmarkDes
        
        let selected = bookmark.isSelected ?? false
        var feature = Turf.Feature(geometry: .point(Point(CLLocationCoordinate2D(latitude: lat, longitude: long))))
        feature.properties = [
            "type": .string(type != "" ? type : Values.COLLECTION),
            "name": .string(name),
            "address": .string(address),
            "id": .string("\(idBookmark)"),
            "amenityCarPark": .string(amenityCarPark),
            "isBookmarked": .boolean(true),
            "isSelected":.boolean(type != "" ? false : selected),
            "toolTipType":.string(""),
            "description": .string(description)
        ]
        return feature
    }
    
    private func getBookmarkAmenitiesFeatureCollection(bookMarks: [Bookmark]) -> Turf.FeatureCollection {
        let arrayFeature = bookMarks.compactMap { bookmark in
            
            return bookmark.code == Values.NEWBOOKMARKS ? getBookmarkAmenitiesFeatures(bookmark: bookmark, type: Values.NEWBOOKMARKS) :  getBookmarkAmenitiesFeatures(bookmark: bookmark)
            
            
        }
        return Turf.FeatureCollection(features: arrayFeature)
    }
    
    
   
    
    func updateBookMark(bookmarks: [Bookmark]) {
        
        self.bookMarks = bookmarks
        self.deselectTooltip()
        addBookmakrToMap(bookmarks, code: BookmarkCode.custom.rawValue, type: Values.COLLECTION)
        addBookmakrToMap(bookmarks, code: BookmarkCode.normal.rawValue, type: Values.COLLECTIONNORMAL)
        addBookmakrToMap(bookmarks, code: BookmarkCode.work.rawValue, type: Values.COLLECTIONWORK)
        addBookmakrToMap(bookmarks, code: BookmarkCode.home.rawValue, type: Values.COLLECTIONHOME)
        addBookmakrToMap(bookmarks, code: Values.NEWBOOKMARKS, type: Values.NEWBOOKMARKS)
        
        setCameraDefaultCameraPositionForBookmarks(bookmarks: bookmarks)
    }
    
    func deSelectBookMarkAmenityOnMap(bookmarks: [Bookmark]){
        
        addBookmakrToMap(bookmarks, code: BookmarkCode.custom.rawValue, type: Values.COLLECTION)
        addBookmakrToMap(bookmarks, code: BookmarkCode.normal.rawValue, type: Values.COLLECTIONNORMAL)
        addBookmakrToMap(bookmarks, code: BookmarkCode.work.rawValue, type: Values.COLLECTIONWORK)
        addBookmakrToMap(bookmarks, code: BookmarkCode.home.rawValue, type: Values.COLLECTIONHOME)
        
        if let queryRenderLayer = queryRenderLayer {
            self.adjustZoomWhenOpenToolip(carParks: queryRenderLayer.carparkUpdater?.carparks)
        }
        else{
            
            setCameraDefaultCameraPositionForBookmarks(bookmarks: bookmarks)
        }
    }
    
    private func addBookmakrToMap(_ bookmarks: [Bookmark], code: String?, type: String) {
        let filtered = bookmarks.filter { bookmark in
            return bookmark.code == code
        }
        let features = getBookmarkAmenitiesFeatureCollection(bookMarks: filtered)
        mobileContentMapViewUpdatable?.addBookmarkAmentiesToMapOfType(features: features, type: type)
    }
    
    func setCameraDefaultCameraPositionForBookmarks(bookmarks: [Bookmark]) {
        if bookmarks.count == 0 {
            return
        }
    
        for bookmark in bookmarks {
            contentGlobalPOICoordinates.append(bookmark.location)
        }
        
        contentGlobalPOICoordinates.append(LocationManager.shared.location.coordinate)
        self.adjustCameraForGlobalPOIs()
        
    }
    
    func cameraPadding() -> UIEdgeInsets {
        
        let padding = UIEdgeInsets(top: 30, left: 30, bottom: CGFloat(self.bottomSheetHeight) + 30, right: 30)
        
        return padding
    }
    
    /**
     Keep Profile data in SelectedProfile singleton class
     */
    private func addProfileName(profileName:String) {
        
        SelectedProfiles.shared.selectProfile(name: profileName)
    }
    
    
    private func addProfileData(zoneDetails:[ContentZone]) {
        
        if zoneDetails.count > 0 {
            
            SelectedProfiles.shared.setAllZones(allZones: zoneDetails)
        }
    }
    
    func loadAllImages(content:ContentBase,imageCompletionHandler: @escaping (Bool) -> Void){
        
        var imageCount = 0
        for contentDetail in content.details{
            
            let map_icon_light_selected_url = contentDetail.mapLightSelectedURL ?? ""
            let map_icon_light_unselected_url = contentDetail.mapLightUnselectedURL ?? ""
            let map_icon_dark_selected_url = contentDetail.mapDarkSelectedURL ?? ""
            let map_icon_dark_unselected_url = contentDetail.mapDarkUnselectedURL ?? ""
            
            if let mobileContentMapViewUpdatable = mobileContentMapViewUpdatable {
                
                mobileContentMapViewUpdatable.navigationMapView.mapView.removeExistingAmenityImages(type: "\(contentDetail.code)")
                
                mobileContentMapViewUpdatable.navigationMapView.mapView.addDynamicImagesMapStyleFromContentScreen(type: "\(contentDetail.code)", urlString: map_icon_light_unselected_url, darkModeUrlString: map_icon_dark_unselected_url) { unSelectedImageSuccess in
                    
                    mobileContentMapViewUpdatable.navigationMapView.mapView.removeExistingAmenityImages(type: "\(contentDetail.code)-selected")
                    
                    mobileContentMapViewUpdatable.navigationMapView.mapView.addDynamicImagesMapStyleFromContentScreen(type: "\(contentDetail.code)-selected", urlString: map_icon_light_selected_url, darkModeUrlString: map_icon_dark_selected_url) { selectedImageSuccess in
                        
                        // TODO: Probably need refactor later
                        // To make sure completion handler only called once
                        imageCount += 1
                        if imageCount == content.details.count {
                            imageCompletionHandler(true)
                        }
                    }
                }
//                mobileContentMapViewUpdatable.navigationMapView.mapView.addDynamicImagesToMapStyle(type: "\(contentDetail.code)", urlString: map_icon_light_unselected_url, darkModeUrlString: map_icon_dark_unselected_url)
//
//                mobileContentMapViewUpdatable.navigationMapView.mapView.removeExistingAmenityImages(type: "\(contentDetail.code)-selected")
//                mobileContentMapViewUpdatable.navigationMapView.mapView.addDynamicImagesToMapStyle(type: "\(contentDetail.code)-selected", urlString: map_icon_light_selected_url, darkModeUrlString: map_icon_dark_selected_url)
            }
            
        }
                
        if let mobileContentMapViewUpdatable = mobileContentMapViewUpdatable {
            
            mobileContentMapViewUpdatable.navigationMapView.mapView.addCircle()
        }
        
    }
    
    func loadImageWalkthon(type: String, urlModeLightString: String, darkModeUrlString: String) {
        if let mobileContentMapViewUpdatable = mobileContentMapViewUpdatable {
            mobileContentMapViewUpdatable.navigationMapView.mapView.addDynamicImagesToMapStyle(type: type, urlString: urlModeLightString, darkModeUrlString: darkModeUrlString)
        }
    }
    
    func addContentToMap(content:ContentBase,
                         selectedID: String = ""){
        
        contentType = content.layer_code
        
        if contentGlobalPOICoordinates.count > 0 {
            contentGlobalPOICoordinates.removeAll()
        }
        if content.type == "ZONE" {
            
            var contentFeature = [Turf.Feature]()
            
            self.addProfileName(profileName: content.layer_code)
            
            self.addProfileData(zoneDetails: content.zones)
            
            AmenitiesSharedInstance.shared.callZoneColor(profileName: content.layer_code)
            
            AmenitiesSharedInstance.shared.amenitiesZoneDelegate = self
            
            for contentDetail in content.details{
                
                for contentData in contentDetail.data{
                    
                    let contentName = contentData.name
                    let contentAddress = contentData.address
                    let id = contentData.dataID
                    let provider =  !selectedID.isEmpty && id == selectedID ? "\(contentDetail.code)-selected" : contentDetail.code
                    let startDate = contentData.startDate ?? -1
                    let endDate = contentData.endDate ?? -1
                    let toolTipType = contentDetail.tooltipType
                    let coordinate = CLLocationCoordinate2D(latitude:contentData.lat, longitude: contentData.long)
                    var feature = Turf.Feature(geometry: .point(Point(coordinate)))
                    contentGlobalPOICoordinates.append(coordinate)
                    var properties: JSONObject = [
                        "type": .string(Values.POI),
                        "name": .string(contentName),
                        "address": .string(contentAddress),
                        "id": .string(id),
                        "startDate": .number(startDate),
                        "endDate": .number(endDate),
                        "provider":.string(provider),
                        "toolTipType":.string(toolTipType),
                        "enableWalking":.boolean(content.getEnableWalking()),
                        "enableZoneDrive":.boolean(content.getEnableZoneDrive()),
                        "trackNavigation":.boolean(content.getTrackNavigation()),
                        "layerCode":.string(content.layer_code),
                        "canReroute":.boolean(content.getCanReroute()),
                        "contentMapName":.string(content.name),
                        "textAdditionalInfo": .string(contentData.additionalInfo?.text ?? ""),
                        "styleLightColor": .string(contentData.additionalInfo?.style?.lightColor?.color ?? ""),
                        "styleDarkColor": .string(contentData.additionalInfo?.style?.darktColor?.color ?? "")
                    ]
//                    feature.properties = [
//                        "type": .string(Values.POI),
//                        "name": .string(contentName),
//                        "address": .string(contentAddress),
//                        "id": .string(id),
//                        "startDate": .number(startDate),
//                        "endDate": .number(endDate),
//                        "provider":.string(provider),
//                        "toolTipType":.string(toolTipType),
//                        "enableWalking":.boolean(content.getEnableWalking()),
//                        "enableZoneDrive":.boolean(content.getEnableZoneDrive()),
//                        "trackNavigation":.boolean(content.getTrackNavigation()),
//                        "layerCode":.string(content.layer_code),
//                        "canReroute":.boolean(content.getCanReroute()),
//                        "contentMapName":.string(content.name),
//                        "textAdditionalInfo": .string(contentData.additionalInfo?.text ?? ""),
//                        "styleLightColor": .string(contentData.additionalInfo?.style?.lightColor?.color ?? ""),
//                        "styleDarkColor": .string(contentData.additionalInfo?.style?.darktColor?.color ?? "")
//                    ]
                    if let cs = contentData.availabilityCSData {
                        if let availablePercentImageBand = contentData.availablePercentImageBand, !availablePercentImageBand.isEmpty {
                            properties["availablePercentImageBand"] = .string(availablePercentImageBand)
                        }
                        var csObject: JSONObject = [
                            "updateTS": .number(cs.updateTS ?? 0),
                            "title": .string(cs.title ?? ""),
                            "desc": .string(cs.desc ?? ""),
                            "themeColor": .string(cs.themeColor ?? ""),
                            "primaryDesc": .string(cs.primaryDesc ?? ""),
                            "secondaryDesc": .string(cs.secondaryDesc ?? "")
                        ]
                        properties["availabilityCSData"] = .object(csObject)
                    }
                    
                    feature.properties = properties
                   
                    contentFeature.append(feature)
                }
            }
            
            if let mobileContentMapViewUpdatable = mobileContentMapViewUpdatable {
                
                if let higherRadiusZone = SelectedProfiles.shared.getHigherZoneRadius(){
                    
                    contentGlobalPOICoordinates.append(CLLocationCoordinate2D(latitude: higherRadiusZone.centerPointLat.toDouble() ?? 0, longitude: higherRadiusZone.centerPointLong.toDouble() ?? 0))
                }
                
                let poiFeature = contentFeature.filter { f in
                    if let properties = f.properties,
                       case let .string(type) = properties["toolTipType"] {
                        return type == "POI"
                    }else {
                        return false
                    }
                }
                let otherFeature = contentFeature.filter { f in
                    if let properties = f.properties,
                       case let .string(type) = properties["toolTipType"] {
                        return type != "POI"
                    }else {
                        return true
                    }
                }
                mobileContentMapViewUpdatable.addPOIToMap(Turf.FeatureCollection(features: poiFeature))
                mobileContentMapViewUpdatable.addAmenitiesToMap(Turf.FeatureCollection(features: otherFeature), hasPOI: !poiFeature.isEmpty)
                self.adjustZoneCamera()
            }
            
        }
        else{

            if contentGlobalPOICoordinates.count > 0 {
                contentGlobalPOICoordinates.removeAll()
            }
            var contentFeature = [Turf.Feature]()
            for contentDetail in content.details{
                
                contentType = contentDetail.code
                for contentData in contentDetail.data{
                    
                    let contentName = contentData.name
                    let contentAddress = contentData.address
                    let toolTipType = contentDetail.tooltipType
                    let startDate = contentData.startDate ?? -1
                    let endDate = contentData.endDate ?? -1
                    let id = contentData.id
                    let coordinate = CLLocationCoordinate2D(latitude:contentData.lat, longitude: contentData.long)
                    contentGlobalPOICoordinates.append(coordinate)
                    var feature = Turf.Feature(geometry: .point(Point(coordinate)))
                    var properties: JSONObject = [
                        "id":.string(id),
                        "type":.string(contentType),
                        "name":.string(contentName),
                        "address":.string(contentAddress),
                        "startDate": .number(startDate),
                        "endDate": .number(endDate),
                        "isSelected": .boolean(!selectedID.isEmpty && id == selectedID),
                        "toolTipType":.string(toolTipType),
                        "enableWalking":.boolean(content.getEnableWalking()),
                        "enableZoneDrive":.boolean(content.getEnableZoneDrive()),
                        "trackNavigation":.boolean(content.getTrackNavigation()),
                        "layerCode":.string(content.layer_code),
                        "canReroute":.boolean(content.getCanReroute()),
                        "contentMapName":.string(content.name),
                        "textAdditionalInfo": .string(contentData.additionalInfo?.text ?? ""),
                        "styleLightColor": .string(contentData.additionalInfo?.style?.lightColor?.color ?? ""),
                        "styleDarkColor": .string(contentData.additionalInfo?.style?.darktColor?.color ?? "")
                    ]
                    
//                                        feature.properties = [
//                                            "id":.string(id),
//                                            "type":.string(contentType),
//                                            "name":.string(contentName),
//                                            "address":.string(contentAddress),
//                                            "startDate": .number(startDate),
//                                            "endDate": .number(endDate),
//                                            "isSelected": .boolean(!selectedID.isEmpty && id == selectedID),
//                                            "toolTipType":.string(toolTipType),
//                                            "enableWalking":.boolean(content.getEnableWalking()),
//                                            "enableZoneDrive":.boolean(content.getEnableZoneDrive()),
//                                            "trackNavigation":.boolean(content.getTrackNavigation()),
//                                            "layerCode":.string(content.layer_code),
//                                            "canReroute":.boolean(content.getCanReroute()),
//                                            "contentMapName":.string(content.name),
//                                            "textAdditionalInfo": .string(contentData.additionalInfo?.text ?? ""),
//                                            "styleLightColor": .string(contentData.additionalInfo?.style?.lightColor?.color ?? ""),
//                                            "styleDarkColor": .string(contentData.additionalInfo?.style?.darktColor?.color ?? "")
//                                        ]
                    if let cs = contentData.availabilityCSData {
                        if let availablePercentImageBand = contentData.availablePercentImageBand, !availablePercentImageBand.isEmpty {
                            properties["availablePercentImageBand"] = .string(availablePercentImageBand)
                        }
                        var csObject: JSONObject = [
                            "updateTS": .number(cs.updateTS ?? 0),
                            "title": .string(cs.title ?? ""),
                            "desc": .string(cs.desc ?? ""),
                            "themeColor": .string(cs.themeColor ?? ""),
                            "primaryDesc": .string(cs.primaryDesc ?? ""),
                            "secondaryDesc": .string(cs.secondaryDesc ?? "")
                        ]
                        properties["availabilityCSData"] = .object(csObject)
                    }
                    
                    feature.properties = properties
                    contentFeature.append(feature)
                }
            }
            
            if let mobileContentMapViewUpdatable = mobileContentMapViewUpdatable {
                
                contentGlobalPOICoordinates.append(LocationManager.shared.location.coordinate)
                self.adjustCameraForGlobalPOIs()
                mobileContentMapViewUpdatable.addContentAmenitiesToMap(Turf.FeatureCollection(features: contentFeature), type: contentType)
                
            }
        }
        
    }
    
    func clearContentsFromMap(){
        
        if let mobileContentMapViewUpdatable = self.mobileContentMapViewUpdatable{
            
            mobileContentMapViewUpdatable.removeContetAmenityFromMap(type: contentType)
            mobileContentMapViewUpdatable.removeContetAmenityFromMap(type: Values.POI)
            mobileContentMapViewUpdatable.removeContetAmenityFromMap(type: Values.AMENITIES)
            mobileContentMapViewUpdatable.navigationMapView.mapView.removeProfileZonesDynamically()
            self.resetToDefaults()
            SelectedProfiles.shared.clearProfile()
        }
    }
    
    func calculateFurthestDistance(coordinates:[CLLocationCoordinate2D],defaultFurthestDistance:Double,nearestCoordinate:CLLocationCoordinate2D) -> Double {
        
        var finalFurthestDistance = defaultFurthestDistance
        for coordinate in coordinates {
            
            let toLocation = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
            
            let fromLocation = CLLocation(latitude: nearestCoordinate.latitude, longitude: nearestCoordinate.longitude)
            
            let distance = fromLocation.distance(from: toLocation)
            
            if(distance > finalFurthestDistance){
                
                finalFurthestDistance = distance
            }
             
        }
        
        return finalFurthestDistance
    }

    func resetCamera(){
        
        if let mobileContentMapViewUpdatable = mobileContentMapViewUpdatable {
            mobileContentMapViewUpdatable.defaultCameraWithCurrentLocation()
            resetToDefaults()
            if let currentSelectedContent = currentSelectedContent {
                
                self.addContentToMap(content: currentSelectedContent)
                
                if currentSelectedContent.type == "ZONE"{
                    
                    self.showZoneAndCurrentLocation = true
                    if let higherRadiusZone = SelectedProfiles.shared.getHigherZoneRadius() {
                        
                        let centerCoordinate = CLLocationCoordinate2D(latitude: higherRadiusZone.centerPointLat.toDouble() ?? 0, longitude: higherRadiusZone.centerPointLong.toDouble() ?? 0)
                        
                        let distance = getDistanceBetweenTwoLocations(toLocation: centerCoordinate, fromLocation: LocationManager.shared.location.coordinate)
                        
                        
                        if distance >= Double(currentSelectedContent.recentre_zone_radius * 1000){
                            
                            self.adjustZoneCameraWhenLocationIsFar()
                        }
                    }
                            
                }
            }
           
        }
    }
    
    private func adjustCameraForGlobalPOIs(){
        
        if let mobileContentMapViewUpdatable = mobileContentMapViewUpdatable {
            
            if let queryRenderLayer = queryRenderLayer, let carParkupdater = self.carparkUpdater, let callOut = queryRenderLayer.callOutView{
                
                self.adjustZoomWhenOpenToolip(carParks: carParkupdater.carparkViewModel.carparks)
                
                self.isTrackingUser = false
            }
            else{
                
                self.cameraAndZoomForglobaPOIs(mobileContentMapViewUpdatable: mobileContentMapViewUpdatable)
            }
            
        }
        
    }
    
    private func cameraAndZoomForglobaPOIs(mobileContentMapViewUpdatable:ContentViewUpdatable){
        
        let padding = cameraPadding()
        
        let distanceFilter = 0.0
        
        if contentGlobalPOICoordinates.count > 0 {
            
            let finalFarthestDistance = self.calculateFurthestDistance(coordinates: contentGlobalPOICoordinates, defaultFurthestDistance: distanceFilter, nearestCoordinate: contentGlobalPOICoordinates[0])
            
    //        var closestLocation: CLLocationCoordinate2D?
    //        var smallestDistance: CLLocationDistance?
    //
    //        for location in contentGlobalPOICoordinates {
    //
    //            let fromLocation = CLLocation(latitude: location.latitude, longitude: location.longitude)
    //            let distance = LocationManager.shared.location.distance(from: fromLocation)
    //
    //            if let smallest = smallestDistance {
    //                if distance < smallest{
    //
    //                    closestLocation = location
    //                    smallestDistance = distance
    //                }
    //            }
    //            else{
    //
    //                closestLocation = location
    //                smallestDistance = distance
    //            }
    //
    //        }
            
            mobileContentMapViewUpdatable.navigationMapView.mapView.setDynamicZoomLevelBasedonRadius(coordinate1: contentGlobalPOICoordinates[0], dynamicRadius: Int(finalFarthestDistance), padding: padding, coordinate2: LocationManager.shared.location.coordinate,openToolTip: true)
            
            self.isTrackingUser = true
        }
    }
    
    private func adjustZoomWhenOpenToolip(carParks: [Carpark]?){
        
        DispatchQueue.main.async { [weak self] in
            
            guard let self = self else { return }
            
            let carparks = carParks ?? []
            var coordinates = [CLLocationCoordinate2D]()
            let distanceFilter = 0.0
            
            carparks.forEach { carPark in
                
              coordinates.append(CLLocationCoordinate2D(latitude: carPark.lat ?? 0, longitude: carPark.long ?? 0))
                
            }
            
            if let mobileContentMapViewUpdatable = self.mobileContentMapViewUpdatable,let queryRenderLayer = self.queryRenderLayer, let callOut = queryRenderLayer.callOutView{
                
                coordinates.append(callOut.tooltipCoordinate)
                
                let finalFarthestDistance = self.calculateFurthestDistance(coordinates: coordinates, defaultFurthestDistance: distanceFilter, nearestCoordinate: coordinates[0])
                
                if let currentSelectedContent = self.currentSelectedContent {
                    
                    if currentSelectedContent.type == "ZONE"{
                        
                        mobileContentMapViewUpdatable.adjustZommWhenToolTipOpen(location: callOut.tooltipCoordinate, callOut: callOut, bottomSheetHeight: self.bottomSheetHeight - self.offset, dynamicRadius: Values.toolTipDistance, coordinate2: coordinates[0],callOutView: callOut)
                    }
                    else{
                        
                        mobileContentMapViewUpdatable.adjustZommWhenToolTipOpen(location: callOut.tooltipCoordinate, callOut: callOut, bottomSheetHeight: self.bottomSheetHeight - self.offset, dynamicRadius: Int(finalFarthestDistance), coordinate2: coordinates[0],callOutView: callOut)
                    }
                }
                else{
                    
                    mobileContentMapViewUpdatable.adjustZommWhenToolTipOpen(location: callOut.tooltipCoordinate, callOut: callOut, bottomSheetHeight: self.bottomSheetHeight - self.offset, dynamicRadius: Int(finalFarthestDistance), coordinate2: coordinates[0],callOutView: callOut)
                }
                
                self.isTrackingUser = false
                
            }
            else{
                
                if let mobileContentMapViewUpdatable = self.mobileContentMapViewUpdatable{
                    
                    if let currentSelectedContent = self.currentSelectedContent {
                        
                        self.isTrackingUser = true
                        self.resetToDefaults()
                        self.addContentToMap(content: currentSelectedContent)
                    }
                    if let currentSelectedContent = self.currentSelectedContent {
                        
                        if currentSelectedContent.type == "ZONE"{
                            
                            self.adjustZoneCamera()
                        }
                        else{
                            
                            self.cameraAndZoomForglobaPOIs(mobileContentMapViewUpdatable: mobileContentMapViewUpdatable)
                        }
                    }
                    else{
                        self.cameraAndZoomForglobaPOIs(mobileContentMapViewUpdatable: mobileContentMapViewUpdatable)
                    }
                    
                }
                
            }
        }
        
    }
    
    private func adjustZoneCameraWhenLocationIsFar(){
        
        if let higherRadiusZone = SelectedProfiles.shared.getHigherZoneRadius(),let mobileContentMapViewUpdatable = self.mobileContentMapViewUpdatable {
            
            let centerCoordinate = CLLocationCoordinate2D(latitude: higherRadiusZone.centerPointLat.toDouble() ?? 0, longitude: higherRadiusZone.centerPointLong.toDouble() ?? 0)
            
            let distance = getDistanceBetweenTwoLocations(toLocation: centerCoordinate, fromLocation: LocationManager.shared.location.coordinate)
            
            var padding = UIEdgeInsets(top: 30, left: 0, bottom: mobileContentMapViewUpdatable.navigationMapView.frame.size.height - CGFloat(self.bottomSheetHeight), right: 0)
            if self.bottomSheetIndex == 0 {
                
                padding = UIEdgeInsets(top: 100, left: 100, bottom: CGFloat(self.bottomSheetHeight)+150, right: 100)
                
            }
            
            mobileContentMapViewUpdatable.navigationMapView.mapView.setDynamicZoomLevelBasedonRadius(coordinate1: LocationManager.shared.location.coordinate, dynamicRadius: Int(distance), padding: padding, coordinate2: CLLocationCoordinate2D(latitude: higherRadiusZone.centerPointLat.toDouble() ?? 0, longitude: higherRadiusZone.centerPointLong.toDouble() ?? 0),openToolTip: false,multiplyValue: 5.7)
            
            isTrackingUser = true
        }
        
    }
    
    private func getDistanceBetweenTwoLocations(toLocation:CLLocationCoordinate2D,fromLocation:CLLocationCoordinate2D) -> CLLocationDistance{
        
        let toLocation = CLLocation(latitude: toLocation.latitude, longitude: toLocation.longitude)
        let fromLocation = CLLocation(latitude: fromLocation.latitude, longitude: fromLocation.longitude)
        
        let distance = fromLocation.distance(from: toLocation)
        
        return distance
    }
    
    private func adjustZoneCamera(){
        
        if let higherRadiusZone = SelectedProfiles.shared.getHigherZoneRadius() {
            
            let centerCoordinate = CLLocationCoordinate2D(latitude: higherRadiusZone.centerPointLat.toDouble() ?? 0, longitude: higherRadiusZone.centerPointLong.toDouble() ?? 0)
            
            let distance = getDistanceBetweenTwoLocations(toLocation: centerCoordinate, fromLocation: LocationManager.shared.location.coordinate)
            
            if let currentSelectedContent = currentSelectedContent {
                
                if distance >= Double(currentSelectedContent.recentre_zone_radius * 1000){
                    
                    if self.showZoneAndCurrentLocation {
                        
                        isTrackingUser = true
                        if let queryRenderLayer = queryRenderLayer {
                            
                            if queryRenderLayer.callOutView != nil{
                                
                                self.adjustZoomWhenOpenToolip(carParks: queryRenderLayer.carparkUpdater?.carparks)
                            }
                            else{
                                
                                self.adjustZoneCameraWhenLocationIsFar()
                            }
                        }
                        else{
                            
                            self.adjustZoneCameraWhenLocationIsFar()
                        }
                        
                    }
                    else{
                        
                        isTrackingUser = false
                        self.zoneCamera(centreCoordinate: centerCoordinate,radius: higherRadiusZone.radius)
                    }
                }
                else{
                    
                    isTrackingUser = true
                    self.zoneCamera(centreCoordinate: centerCoordinate,radius: higherRadiusZone.radius)
                }
                
            }
        }
    }
    
    private func zoneCamera(centreCoordinate:CLLocationCoordinate2D,radius:Int){
        
        if let mobileContentMapViewUpdatable = self.mobileContentMapViewUpdatable {
            
            if let queryRenderLayer = queryRenderLayer {
                
                if queryRenderLayer.callOutView != nil{
                    
                    self.adjustZoomWhenOpenToolip(carParks: queryRenderLayer.carparkUpdater?.carparks)
                }
                else{
                    
                    var padding = UIEdgeInsets(top: 30, left: 0, bottom: mobileContentMapViewUpdatable.navigationMapView.frame.size.height - CGFloat(self.bottomSheetHeight), right: 0)
                    if self.bottomSheetIndex == 0 {
                        
                        padding = UIEdgeInsets(top: 100, left: 100, bottom: CGFloat(self.bottomSheetHeight)+150, right: 100)
                        
                    }
                    if contentGlobalPOICoordinates.count > 0 {
                        
                        mobileContentMapViewUpdatable.navigationMapView.navigationCamera.stop()
                        mobileContentMapViewUpdatable.navigationMapView.mapView.setDynamicZoomLevelBasedonRadius(coordinate1: contentGlobalPOICoordinates[0], dynamicRadius: radius, padding: padding, coordinate2: centreCoordinate,openToolTip: false)
                        
                        
                    }
                    
                }
            }
            else{
                
                var padding = UIEdgeInsets(top: 30, left: 0, bottom: mobileContentMapViewUpdatable.navigationMapView.frame.size.height - CGFloat(self.bottomSheetHeight), right: 0)
                if self.bottomSheetIndex == 0 {
                    
                    padding = UIEdgeInsets(top: 100, left: 100, bottom: CGFloat(self.bottomSheetHeight)+150, right: 100)
                    
                }
                if contentGlobalPOICoordinates.count > 0 {
                    mobileContentMapViewUpdatable.navigationMapView.navigationCamera.stop()
                    mobileContentMapViewUpdatable.navigationMapView.mapView.setDynamicZoomLevelBasedonRadius(coordinate1: contentGlobalPOICoordinates[0], dynamicRadius: radius, padding: padding, coordinate2: centreCoordinate,openToolTip: false)
                }
                
            }
            
        }
    }
    
    func resetCameraBasedOnBottomSheet(){
        
        if let currentSelectedContent = currentSelectedContent {
            
            if currentSelectedContent.type == "ZONE"{
                
                self.adjustZoneCamera()
            }
            else{
                
                self.adjustCameraForGlobalPOIs()
            }
        }
        else if bookMarks.count > 0 {
            
            self.adjustCameraForGlobalPOIs()
        }
    }
    
    func showParking(){
        
        if let queryRenderLayer = queryRenderLayer {
            
            var coordinate:CLLocationCoordinate2D = LocationManager.shared.location.coordinate
            if let callout = queryRenderLayer.callOutView {
                coordinate = callout.tooltipCoordinate
                
            }
            loadCarparks(location: coordinate, discoverMode: false, arrivalTime: Date(), destName: "")
        }
    }
    
    func hideParking(){
        
        if let mobileContentMapViewUpdatable = mobileContentMapViewUpdatable {
            mobileContentMapViewUpdatable.navigationMapView.mapView.removeCarparkLayer()
            carparkUpdater = nil
        }
    }
    
    func saveParkingViewType() {
        guard let code = UserSettingsModel.sharedInstance.carparkAvailabilityCode else {
            return
        }
        SettingsService.shared.updateSettingAttribute(UserSettingsModel.UserSettingAttributes.carparkAvailability, value: code) { _ in
            // Don't need process here
        }
    }
    
    func showShareCollectionToolTip(bookmark: Bookmark, fromScreen:QueryRenderFromScreen = .content, view: UIView) {
        if let mobileContentMapViewUpdatable = mobileContentMapViewUpdatable {
            if queryRenderLayer == nil {
                queryRenderLayer = QueryRenderLayer(mobileNavigationMapView: mobileContentMapViewUpdatable.navigationMapView, fromScreen: fromScreen, currentView: view)
                queryRenderLayer?.delegate = self
            }
        }
    }
    
    func showTooltipfor(bookmark: Bookmark, fromScreen:QueryRenderFromScreen = .content, view: UIView) {
        if let mobileContentMapViewUpdatable = mobileContentMapViewUpdatable {
            if queryRenderLayer == nil {
                queryRenderLayer = QueryRenderLayer(mobileNavigationMapView: mobileContentMapViewUpdatable.navigationMapView, fromScreen: fromScreen, currentView: view)
                queryRenderLayer?.delegate = self
            }
            var description =  bookmark.welcomeDescription
            if description?.count ?? 0 == 0 {
                description = nil
            }
            
            let address = bookmark.address1 ?? ""
            self.queryRenderLayer?.openAmenityTooltip(featureInformation: bookmark.name ?? "",
                                                      featureAddress: address,
                                                      type: Values.COLLECTION,
                                                      isBookmarked: true,
                                                      id: bookmark.bmId,
                                                      location: bookmark.location,
                                                      amenityCarPark: bookmark.amenityId ?? "",
                                                      selectedFeatureProperties: nil,
                                                      frommap: false,
                                                      welcomeDescription: description,
                                                      address2: bookmark.address2 ?? "",
                                                      fullAddress: bookmark.fullAddress ?? "",
                                                      availablePercentImageBand: bookmark.availablePercentImageBand ?? "",
                                                      availabilityCSData: bookmark.availabilityCSData,
                                                      placeId: bookmark.placeId,
                                                      userLocationLinkIdRef: bookmark.userLocationLinkIdRef)
        }
    }
    
    func showDropPinToolTip(_ feature: Turf.Feature, location: CLLocationCoordinate2D, view:UIView?,fromScreen: QueryRenderFromScreen = .content) {
        
        if let mobileContentMapViewUpdatable = mobileContentMapViewUpdatable {
            if let queryRenderLayer = queryRenderLayer {
                queryRenderLayer.carparkUpdater = carparkUpdater
            } else {
                queryRenderLayer = QueryRenderLayer(mobileNavigationMapView: mobileContentMapViewUpdatable.navigationMapView, fromScreen: fromScreen, currentView: view)
                queryRenderLayer?.carparkUpdater = carparkUpdater
                queryRenderLayer?.delegate = self
            }
            self.queryRenderLayer?.openDropPinToolTipView(feature: feature, location: location)
        }
    }
    
    func focusOnDropPinToolTip() {
        if let mobileContentMapViewUpdatable = self.mobileContentMapViewUpdatable,let queryRenderLayer = self.queryRenderLayer, let callOut = queryRenderLayer.callOutView {
            
            var coordinates = [CLLocationCoordinate2D]()
            coordinates.append(callOut.tooltipCoordinate)
            let finalFarthestDistance = 500
            
            mobileContentMapViewUpdatable.adjustZommWhenToolTipOpen(location: callOut.tooltipCoordinate, callOut: callOut, bottomSheetHeight: self.bottomSheetHeight, dynamicRadius: Int(finalFarthestDistance), coordinate2: coordinates[0],callOutView: callOut)
        }
            
        self.isTrackingUser = false
    }
    
    func queryShareCollectionFeatureOnTap(point:CGPoint,view:UIView?,fromScreen: QueryRenderFromScreen = .content, fromMap: Bool = true, shareLinkData: SaveLocationModel?, completion: ((Bool, Bool) -> Void)? = nil) {
        if let mobileContentMapViewUpdatable = mobileContentMapViewUpdatable {
            
            if let queryRenderLayer = queryRenderLayer {
                queryRenderLayer.queryShareCollectionMapLayer(point: point,contentType: contentType, frommap: fromMap, completion: completion)
                queryRenderLayer.carparkUpdater = carparkUpdater
                if fromScreen == .shareCollectionAndLocation {
                    if let model = shareLinkData {
                        self.setCameraDefaultCameraPositionForShareLocation(shareLocation: model)
                    }
                }
            } else{
                
                queryRenderLayer = QueryRenderLayer(mobileNavigationMapView: mobileContentMapViewUpdatable.navigationMapView, fromScreen: fromScreen, currentView: view)
                
                queryRenderLayer?.carparkUpdater = carparkUpdater
                
                queryRenderLayer?.delegate = self
                
                queryRenderLayer?.queryShareCollectionMapLayer(point: point,contentType: contentType, frommap: fromMap, completion: completion)
            }
            
        }
    }
    
    func queryFeaturesOnTap(point:CGPoint,view:UIView?,fromScreen:QueryRenderFromScreen = .content, fromMap: Bool = true, completion: ((Bool, Bool) -> Void)? = nil) {
        print("11111111 \(point)")
        if let mobileContentMapViewUpdatable = mobileContentMapViewUpdatable {
            
            if let queryRenderLayer = queryRenderLayer {
                
                queryRenderLayer.queryMapLayer(point: point,contentType: contentType, frommap: fromMap, completion: completion)
                
                queryRenderLayer.carparkUpdater = carparkUpdater
                
                
            }
            else{
                
                queryRenderLayer = QueryRenderLayer(mobileNavigationMapView: mobileContentMapViewUpdatable.navigationMapView, fromScreen: fromScreen, currentView: view)
                
                queryRenderLayer?.carparkUpdater = carparkUpdater
                
                queryRenderLayer?.delegate = self
                
                queryRenderLayer?.queryMapLayer(point: point,contentType: contentType, frommap: fromMap, completion: completion)
            }
        }
    }
    
    private func resetToDefaults(){
        
        self.hideParking()
        
        if let queryRenderLayer = queryRenderLayer {
            
            if (queryRenderLayer.fromScreen != .content){
                if let callOut = queryRenderLayer.callOutView{
                    
                    if callOut is AmenityToolTipsView{
                        
                        self.delgate?.selectedAmenity(id: callOut.tooltipId, isSelected: true)
                    }
                    
                }
                
            }
            queryRenderLayer.carparkUpdater = nil
            queryRenderLayer.didDeselectAmenityAnnotation()
            
        }
        
        
        if bookMarks.count > 0 {
            
            self.updateBookMark(bookmarks: bookMarks)
        }
        
    }
    
   private func loadCarparks(location: CLLocationCoordinate2D, discoverMode: Bool, arrivalTime: Date = Date(), destName: String = "") {
        
       if let mobileContentMapViewUpdatable = mobileContentMapViewUpdatable {
           
           carparkUpdater = CarParkUpdater(destName: "",
                                           carPlayNavigationMapView: nil,
                                           mobileNavigationMapView: mobileContentMapViewUpdatable.navigationMapView,
                                           fromScreen: .content)
           
           carparkUpdater?.delegate = self
           
           carparkUpdater?.configure(arrivalTime: arrivalTime, destName: destName)
           
           carparkUpdater?.setDiscoveryMode(discoverMode: discoverMode)
           
           carparkUpdater?.carparkAvailability = .all
           
           carparkUpdater?.setCPClusterMode(isCluster: false)
           
           carparkUpdater?.loadCarParks(maxCarParksCount: UserSettingsModel.sharedInstance.carparkCount,
                                        radius:UserSettingsModel.sharedInstance.carparkDistance,
                                        maxCarParkRadius: Values.carparkMonitorMaxDistanceInLanding, location: location)
           if let queryRenderLayer = queryRenderLayer {
               queryRenderLayer.carparkUpdater = carparkUpdater
           }
           return
       }
        
    }
    
    // Add Share Location on Map

    func getShareLocationFeature(shareLocation: SaveLocationModel, isSelected: Bool = false, bookmarkId: Int?) -> Turf.Feature {
        let coordinate = CLLocationCoordinate2D(latitude: shareLocation.lat?.toDouble() ?? 0.0, longitude: shareLocation.long?.toDouble() ?? 0.0)
        let name = shareLocation.name ?? ""
        let address = shareLocation.address1 ?? ""
        let amenityId = shareLocation.amenityId ?? ""
        let description = shareLocation.description
        var locationBookmarkId = 0
        var saved = shareLocation.saved ?? false
        if let ID = shareLocation.bookmarkId {
            locationBookmarkId = ID
        } else if let ID = bookmarkId {
            locationBookmarkId = ID
            saved = true
        }
        let placeId = shareLocation.placeId ?? ""
        
        var feature = Turf.Feature(geometry: .point(Point(coordinate)))
        var properties: JSONObject = [
            "type":.string(Values.DROPPIN),
            "name":.string(name),
            "address":.string(address),
            "isBookmarked": .boolean(saved),
            "isSelected": .boolean(isSelected),
            "lat": .string(coordinate.latitude.toString()),
            "long": .string(coordinate.longitude.toString()),
            "isShareCollection": .boolean(true),
            "userLocationLinkId": .string("\(shareLocation.userLocationLinkId ?? 0)"),
            "bookmarkId": .string("\(locationBookmarkId)"),
            "address2": .string(shareLocation.address2 ?? ""),
            "fullAddress": .string(shareLocation.fullAddress ?? ""),
            "description": .string(description ?? "")
        ]
        
        if !amenityId.isEmpty {
            properties["amenityId"] = .string(amenityId)
        }
        if !placeId.isEmpty {
            properties["placeId"] = .string(placeId)
        }
        if let userLocationLinkIdRef = shareLocation.userLocationLinkIdRef {
            properties["userLocationLinkIdRef"] = .string("\(userLocationLinkIdRef)")
        }

        if let cs = shareLocation.availabilityCSData {
            if let availablePercentImageBand = shareLocation.availablePercentImageBand, !availablePercentImageBand.isEmpty {
                properties["availablePercentImageBand"] = .string(availablePercentImageBand)
            }
            var csObject: JSONObject = [
                "updateTS": .number(cs.updateTS ?? 0),
                "title": .string(cs.title ?? ""),
                "desc": .string(cs.desc ?? ""),
                "themeColor": .string(cs.themeColor ?? ""),
                "primaryDesc": .string(cs.primaryDesc ?? ""),
                "secondaryDesc": .string(cs.secondaryDesc ?? "")
            ]
            properties["availabilityCSData"] = .object(csObject)
        }
        
        feature.properties = properties
        
        return feature
    }
    
    
    
    func setCameraDefaultCameraPositionForShareLocation(shareLocation: SaveLocationModel) {
        
        let location = CLLocationCoordinate2D(latitude: shareLocation.lat?.toDouble()
                                              ?? 0.0, longitude: shareLocation.long?.toDouble() ?? 0.0 )
        contentGlobalPOICoordinates.append(location)
        if contentGlobalPOICoordinates.count > 0 {
            
//            let finalFarthestDistance = self.calculateFurthestDistance(coordinates: contentGlobalPOICoordinates, defaultFurthestDistance: 0.0, nearestCoordinate: LocationManager.shared.location.coordinate)
            
            mobileContentMapViewUpdatable?.navigationMapView.mapView.setDynamicZoomLevelBasedonRadius(coordinate1: location, dynamicRadius: Values.toolTipDistance , padding: UIEdgeInsets(top: 300, left: 40, bottom:  150, right: 40), coordinate2: LocationManager.shared.location.coordinate,openToolTip: true)
        }
    }
    
    
    func setCameraDefaultCameraPositionForShareLocationWhenClickRecenter(shareLocation: SaveLocationModel) {
        
        let location = CLLocationCoordinate2D(latitude: shareLocation.lat?.toDouble()
                                              ?? 0.0, longitude: shareLocation.long?.toDouble() ?? 0.0 )
        contentGlobalPOICoordinates.append(location)
        if contentGlobalPOICoordinates.count > 0 {
            
            let finalFarthestDistance = self.calculateFurthestDistance(coordinates: contentGlobalPOICoordinates, defaultFurthestDistance: 0.0, nearestCoordinate: LocationManager.shared.location.coordinate)
            
            mobileContentMapViewUpdatable?.navigationMapView.mapView.setDynamicZoomLevelBasedonRadius(coordinate1: location, dynamicRadius: Int(finalFarthestDistance) , padding: UIEdgeInsets(top: 300, left: 40, bottom:  150, right: 40), coordinate2: LocationManager.shared.location.coordinate,openToolTip: true)
        }
    }
    
    
    func setCameraDefaultCameraPositionForShareCollectionWhenClickRecenter(shareCollection: Bookmark) {
        
        let location = CLLocationCoordinate2D(latitude: shareCollection.lat?.toDouble()
                                              ?? 0.0, longitude: shareCollection.long?.toDouble() ?? 0.0 )
        contentGlobalPOICoordinates.append(location)
        if contentGlobalPOICoordinates.count > 0 {
            
            let finalFarthestDistance = self.calculateFurthestDistance(coordinates: contentGlobalPOICoordinates, defaultFurthestDistance: 0.0, nearestCoordinate: LocationManager.shared.location.coordinate)
            
            let offset = UIApplication.topSafeAreaHeight + 100
            mobileContentMapViewUpdatable?.navigationMapView.mapView.setDynamicZoomLevelBasedonRadius(coordinate1: location, dynamicRadius: Int(finalFarthestDistance) , padding: UIEdgeInsets(top: offset + 80, left: 120, bottom: CGFloat(self.bottomSheetHeight + 80), right: 120), coordinate2: LocationManager.shared.location.coordinate,openToolTip: true)
        }
    }
    
//    func setCameraDefaultCameraPositionForShareCollection(shareCollection: Bookmark) {
//        
//        let location = CLLocationCoordinate2D(latitude: shareCollection.lat?.toDouble()
//                                              ?? 0.0, longitude: shareCollection.long?.toDouble() ?? 0.0 )
//        contentGlobalPOICoordinates.append(location)
//        if contentGlobalPOICoordinates.count > 0 {
//            
////            let finalFarthestDistance = self.calculateFurthestDistance(coordinates: contentGlobalPOICoordinates, defaultFurthestDistance: 0.0, nearestCoordinate: LocationManager.shared.location.coordinate)
//            
//            mobileContentMapViewUpdatable?.navigationMapView.mapView.setDynamicZoomLevelBasedonRadius(coordinate1: location, dynamicRadius: Values.toolTipDistance , padding: UIEdgeInsets(top: 240, left: 40, bottom:  320, right: 40), coordinate2: LocationManager.shared.location.coordinate,openToolTip: true)
//        }
//    }
    

    deinit {

        SwiftyBeaver.debug("ContentViewModel deinit")

    }
    
}

extension ContentViewModel: CarParkUpdaterDelegate {
    func carParkNavigateHere(carPark: Carpark?) {
        
    }
    
    // Process after carpark loaded
    func didUpdateCarparks(carParks: [Carpark]?, location: CLLocationCoordinate2D) {
        
        self.adjustZoomWhenOpenToolip(carParks: carParks)
    }
}

extension ContentViewModel:QueryRenderDelegate {
    
    func closeDropPin() {
        self.delgate?.closeDropPin()
    }
    
    func navigateHere(address:SearchAddresses?, carPark:Carpark?, layerCode: String?, amenityId: String?, amenityType: String?, amenityCarPark: String?){
        
        self.delgate?.navigateHere(address: address, carPark: carPark, layerCode: layerCode, amenityId: amenityId, amenityType: amenityType, amenityCarPark: amenityCarPark)
    }
    
    func toolTipMoreInfo(poi:POI?,carPark:Carpark?,screenName:String){
        
        if poi != nil{
            
            self.delgate?.dismissTBRBeforeAddingNew()
        }
        self.delgate?.toolTipMoreInfo(poi: poi, carPark: carPark,screenName: screenName)
    }
    
    func toolTipOpenAdjustZoom(location:LocationCoordinate2D,callOut:ToolTipsView){
        
        isTrackingUser = false
        
        if callOut is AmenityToolTipsView || callOut is POIToolTipsView {
            
            if let currentSelectedContent = self.currentSelectedContent {
                /// when tool tip is open we just need to get current tool tip id and pass it to the addContentToMap to chage the selected icon
                self.addContentToMap(content: currentSelectedContent, selectedID: callOut.tooltipId)
            }
        }
        if callOut is AmenityToolTipsView || callOut is POIToolTipsView || callOut is DropPinToolTipView {
            self.delgate?.toolTipOpen()
        } else {
            if let queryLayer = self.queryRenderLayer,let carParkUpdater = queryLayer.carparkUpdater {
                adjustZoomWhenOpenToolip(carParks: carParkUpdater.carparkViewModel.carparks)
            }
        }
    }
    
    func selectedAmenity(id: String, isSelected: Bool) {
    
        self.delgate?.selectedAmenity(id: id, isSelected: isSelected)
    }
    
    func poiWalkingPath(poi:POI?, trackNavigation: Bool, amenityId: String? = nil, name: String?, canReroute: Bool?){
        self.delgate?.startWalkingPath(poi: poi, trackNavigation: trackNavigation, amenityId: amenityId, name: name, canReroute: canReroute)
    }
    
 
}

extension ContentViewModel: AmenitiesZoneDelegate {
    func didGetZoneColor() {
        DispatchQueue.main.async { [weak self] in
            
            guard let self = self else { return }
            if let mobileContentMapViewUpdatable = self.mobileContentMapViewUpdatable{
                
                mobileContentMapViewUpdatable.navigationMapView.mapView.addProfileZonesDynamically(zoneColors: SelectedProfiles.shared.getZoneColors(), zoneDetails: SelectedProfiles.shared.getAllZones())
                
            }
            
        }
    }
}




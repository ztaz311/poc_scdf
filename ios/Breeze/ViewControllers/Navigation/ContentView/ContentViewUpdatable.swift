//
//  ContentViewUpdatable.swift
//  Breeze
//
//  Created by VishnuKanth on 27/07/22.
//

import Foundation
import Turf
import MapboxNavigation
@_spi(Restricted) import MapboxMaps
import SnapKit
import SwiftyBeaver
import MapboxDirections

final class ContentViewUpdatable: ContentMapViewUpdatable {
    
    var navigationMapView: NavigationMapView
    
    init(navigationMapView: NavigationMapView) {
        self.navigationMapView = navigationMapView
        self.navigationMapView.mapView.ornaments.options.scaleBar.visibility = .hidden
        self.navigationMapView.mapView.ornaments.options.compass.visibility = .hidden
        self.navigationMapView.mapView.ornaments.options.attributionButton.visibility = .hidden
        self.navigationMapView.mapView.gestures.options.pitchEnabled = false
    }
    
    deinit {
        SwiftyBeaver.debug("ContentViewUpdatable deinit")
    }
    
    func adjustDynamicZoomLevel(_ coordinates: [CLLocationCoordinate2D], currentLocation: CLLocationCoordinate2D?) {
        if let location = currentLocation {
            
            var allCoordinates: [CLLocationCoordinate2D] = []
            allCoordinates.append(contentsOf: coordinates)
            allCoordinates.append(location)
            
            let finalFarthestDistance = self.calculateFurthestDistance(coordinates: allCoordinates, defaultFurthestDistance: 0.0, nearestCoordinate: location)
            
            let padding = UIEdgeInsets(top: UIApplication.topSafeAreaHeight, left: 24, bottom: 24, right: 24)
            
            self.navigationMapView.mapView.setDynamicZoomLevelBasedonRadius(coordinate1: coordinates[0], dynamicRadius: Int(finalFarthestDistance), padding: padding, coordinate2: location)
        }
    }
    
    private func calculateFurthestDistance(coordinates:[CLLocationCoordinate2D],defaultFurthestDistance:Double,nearestCoordinate:CLLocationCoordinate2D) -> Double {
        
        var finalFurthestDistance = defaultFurthestDistance
        for coordinate in coordinates {
            
            let toLocation = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
            
            let fromLocation = CLLocation(latitude: nearestCoordinate.latitude, longitude: nearestCoordinate.longitude)
            
            let distance = fromLocation.distance(from: toLocation)
            
            if(distance > finalFurthestDistance) {
                
                finalFurthestDistance = distance
            }
             
        }
        
        return finalFurthestDistance
    }
}

//
//  ContentMapViewUpdatable.swift
//  Breeze
//
//  Created by VishnuKanth on 27/07/22.
//

import Foundation
import CoreLocation
import MapboxDirections
import Turf
import MapboxCoreNavigation
import MapboxMaps
import MapboxCoreMaps
import MapboxNavigation
import UIKit
@_spi(Experimental) import MapboxMaps


protocol ContentMapViewUpdatable: AnyObject {

    var navigationMapView: NavigationMapView { get }
    
    init(navigationMapView: NavigationMapView)
}

// MARK: Extensions - common function
extension ContentMapViewUpdatable {
    
    func setupCustomUserLocationIcon() {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            
            self.navigationMapView.mapView.setUpCruiseCustomUserLocationIcon(isStarted: false)
        }
    }
    
    func defaultCameraWithCurrentLocation(bottomSheetHeight:Int = 0){
        
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            
            self.setCamera(location: LocationManager.shared.location.coordinate,bottomSheetHeight: bottomSheetHeight)
            
        }
        
    }
    
    func setCamera(location:CLLocationCoordinate2D,bottomSheetHeight:Int = 0){
        
        self.navigationMapView.navigationCamera.stop()
        self.navigationMapView.mapView.setCruiseCamera(at: location, heading: 0.0, animated: true, reset: false, isStarted: false, isCarPlay: false,bottomSheetHeight: bottomSheetHeight)
    }
    
    func removeContetAmenityFromMap(type:String) {
        navigationMapView.mapView.removeAmenitiesLayerOfCluster(type: type)
    }
    
    func addContentAmenitiesToMap(_ featureCollection: FeatureCollection,type:String){
        
        DispatchQueue.main.async {
            
            self.removeContetAmenityFromMap(type: type)
            
            self.navigationMapView.mapView.addAmentiesToMapOfType(features: featureCollection, isBelowPuck: true, type: type)
        }
    }
    
    func addBookmarkAmentiesToMapOfType(features:Turf.FeatureCollection, type:String) {
        if Thread.isMainThread {
            self.removeBookmark(type: Values.NEWBOOKMARKS)
            self.removeBookmark(type: type)
            self.navigationMapView.mapView.addBookmarkAmentiesToMapOfType(features: features, isBelowPuck: true, type: type)
        } else {
            DispatchQueue.main.async {
                self.removeBookmark(type: Values.NEWBOOKMARKS)
                self.removeBookmark(type: type)
                self.navigationMapView.mapView.addBookmarkAmentiesToMapOfType(features: features, isBelowPuck: true, type: type)
            }
        }
    }
    
    func addAmenitiesToMap(_ featureCollection: FeatureCollection, hasPOI: Bool = false){
        DispatchQueue.main.async {
            self.removeContetAmenityFromMap(type: Values.AMENITIES)
            self.navigationMapView.mapView.addContentAmentiesToMapOfType(features: featureCollection, type: Values.AMENITIES, hasPOI: hasPOI)
        }
    }
    
    func addPOIToMap(_ featureCollection: FeatureCollection){
        DispatchQueue.main.async {
            self.removeContetAmenityFromMap(type: Values.POI)
            self.navigationMapView.mapView.addPOIAmentiesToMapOfType(features: featureCollection, isBelowPuck: true, type: Values.POI)
        }
    }
    
    func removeBookmark(type: String) {
        if Thread.isMainThread {
            self.navigationMapView.mapView.removeBookmark(type: type)
        } else {
            DispatchQueue.main.async {
                self.navigationMapView.mapView.removeBookmark(type: type)
            }
        }
    }
    
    func adjustZommWhenToolTipOpen(location:LocationCoordinate2D,callOut:ToolTipsView,bottomSheetHeight:Int,dynamicRadius:Int,coordinate2:CLLocationCoordinate2D,openToolTip:Bool = true,callOutView:ToolTipsView?){
        
        var padding = UIEdgeInsets(top: 340, left: 40, bottom:  CGFloat(bottomSheetHeight) + 50, right: 40)
        if let callOutView = callOutView {
            
            padding = UIEdgeInsets(top: callOutView.frame.size.height + callOutView.tipHeight + 30, left: 40, bottom:  CGFloat(bottomSheetHeight) + 30, right: 40)
        }
        
        self.navigationMapView.mapView.setDynamicZoomLevelBasedonRadius(coordinate1: location, dynamicRadius: dynamicRadius, padding: padding, coordinate2: coordinate2,openToolTip: openToolTip)
        
        callOut.adjust(to: location, in: self.navigationMapView)
        
    }
    
    
}

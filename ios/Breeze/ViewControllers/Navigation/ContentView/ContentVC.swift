//
//  ContentVC.swift
//  Breeze
//
//  Created by VishnuKanth on 26/07/22.
//

import UIKit
import CoreLocation
import MapboxDirections
import Turf
import MapboxCoreNavigation
@_spi(Restricted) import MapboxMaps
import MapboxCoreMaps
import MapboxNavigation
import Combine
import SwiftyBeaver
import SnapKit
import Kingfisher

class ContentVC: BaseViewController {
    
    // MARK: - Private properties
    private var userLocationButton: UserLocationButton!
    private var exploreView:ExploreView?
    private var tooltip: ToolTipsView? // current tooltip
    private var disposables = Set<AnyCancellable>()
    private var tooltipShows = [Int:Bool]() // only show once for each content
    private var tbrDetailsVC:UIViewController?
    private var progressTrackingVc:UIViewController?
    private var backBtn:UIButton?
    @IBOutlet weak var navMapView: NavigationMapView!
    var walkingResponse: RouteResponse?
    var contentViewModel: ContentViewModel?
    private var isFirstTime = true
    var contentID = 0
    var data: String? // json data string from push notification
    var titleProgress: String?
    var messageCategory: String?
    var messageType: String?
    
    override var useDynamicTheme: Bool {
        return true
    }

    var isTrackingUser = true {
        didSet {
            userLocationButton?.isHidden = isTrackingUser
            
            // Check Hidden Walkathon Button
            updateWalkathonButton()
        }
    }
    
    deinit {
        SelectedProfiles.shared.clearProfile()
        SwiftyBeaver.debug("ContentVC deinit")
    }
    
    private var progressWalkathonButton: ProgressWalkathonButton? 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Prefs.shared.removeValueForKey(.walkathonContentId)
        
        setupDarkLightAppearance(forceLightMode: true)
        
        toggleDarkLightMode()
        
        // Do any additional setup after loading the view.
        self.setPuckAndAddCameraRNScreen()
        contentViewModel = ContentViewModel()
        contentViewModel?.delgate = self
        contentViewModel!.$currentSelectedContent
            .receive(on: DispatchQueue.main)
            .sink(receiveValue: { [weak self] content in
                guard let self = self else { return }
                if let currentContent = content,let contentViewModel = self.contentViewModel {
                    
                    contentViewModel.clearContentsFromMap()

                    contentViewModel.loadAllImages(content: currentContent) { imagesDownloadSuccess in
                        
                        contentViewModel.showZoneAndCurrentLocation = false
                        contentViewModel.addContentToMap(content: currentContent)
                    }
                    
                    print(currentContent)
                    
                    if(!self.isFirstTime){
                        self.showYouAreHereToolTip(force: false)
                    }
                    else{
                        
                        self.isFirstTime = false
                    }
                    let isShowWalkathon = currentContent.trackNavigation
                    var url = currentContent.statsIconLight
                    if self.isDarkMode {
                        url = currentContent.statsIconDark
                    }
                    
                    if isShowWalkathon == true {
                        self.downloadTrackProgressIcon(iconLink: url)
                        self.progressWalkathonButton?.isHidden = false
                    } else {
                        self.progressWalkathonButton?.isHidden = true
                    }
                   
                    
                    self.messageType = currentContent.messageType
                    self.messageCategory = currentContent.messageCategory
                    self.titleProgress = currentContent.messageTitle
                }
                
        }).store(in: &disposables)
        
        
        contentViewModel!.onBottomTabClicked = { [weak self] screenName in
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                
                self.onBackBtnClicked() // dismiss first 
                exploreSwitchBottomTab(screenName: screenName)
            }
        }
        
        contentViewModel!.dismissChildView = { [weak self] screenName in
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                
                self.backBtn?.isHidden = false
                self.removeTBRAmenity()
            }
        }
        
        contentViewModel!.$isTrackingUser
            .receive(on: DispatchQueue.main)
            .sink(receiveValue: { [weak self] isTrackingUser in
                guard let self = self else { return }
                self.isTrackingUser = isTrackingUser
        }).store(in: &disposables)
        
        
        
        contentViewModel!.onAdjustCenter = { [weak self] (y, index) in
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                
                self.updateLocationButton(y: y)
                                
                if let contentViewModel = self.contentViewModel {
                    contentViewModel.resetCameraBasedOnBottomSheet()
                }
            }
        }
        

        // only call once
        navMapView.mapView.mapboxMap.onNext(event: .mapLoaded) { [weak self] _ in
            guard let self = self else { return }
            
            if let navMapView = self.navMapView{
                
                self.contentViewModel?.mobileContentMapViewUpdatable = ContentViewUpdatable(navigationMapView: navMapView)
                
                // We should show tooltip when luanch the view controller the first time
                self.showYouAreHereToolTip(force: true)
                self.setupBottomSheet()
            }
            
            self.backBtn?.isUserInteractionEnabled = true
            
        }
        
        navMapView.mapView.mapboxMap.onEvery(event: .cameraChanged) { [weak self] _ in
            guard let self = self else { return }
            if let tooltip = self.tooltip, let navMapView = self.navMapView {
                tooltip.adjust(to: tooltip.tooltipCoordinate, in: navMapView)
            }
            self.processCameraChanged()
        }
        
        setupUI()
        
        Prefs.shared.removeValueForKey(.latPoi)
        Prefs.shared.removeValueForKey(.longPoi)
//        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
//            let userInfo = [Values.ContentIdKey:3] as [String : Any]
//            NotificationCenter.default.post(name: Notification.Name(Values.NotificationSelectContent), object: nil,userInfo: userInfo)
//        }

    }
    
    func removeTBRAmenity(){
        
        if let tbrDetailsVC = self.tbrDetailsVC,let contentViewModel = self.contentViewModel {
            
            if let backBtn = backBtn {
                backBtn.isHidden = false
            }
            contentViewModel.deselectTooltip()
            self.removeChildViewController(tbrDetailsVC)
            self.tbrDetailsVC = nil
            updateBottomSheetIndex(index: 1, toScreen: "Explore")
            
        }
    }
    
    func closeProgressScreen() {
        if let progressTrackingVc = self.progressTrackingVc {
            self.removeChildViewController(progressTrackingVc)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        setupDarkLightAppearance()
        setupGestureRecognizers()
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        guard previousTraitCollection != traitCollection else {
            return
        }
        toggleDarkLightMode()
    }
        
    func setPuckAndAddCameraRNScreen(){
        
    }
    
    func startWalking(poi: POI, trackNavigation: Bool = false, amenityId: String? = nil, name: String?, canReroute: Bool?) {
        walkingResponse = nil
//        calculateRoute(poi: poi)
        
        // 1. current location
        let currentLocation = LocationManager.shared.location
        // 2. destination
        let destinationCoordinate = LocationCoordinate2D(latitude: poi.lat, longitude: poi.long)
        
        let plannedDestAddress1 = poi.name
        let plannedDestAddress2 = poi.address
        let plannedDestLat = poi.lat
        let plannedDestLong = poi.long
        
        requestWalkingRoute(location1: currentLocation.coordinate, location2: destinationCoordinate, name: poi.name) { [weak self] response in
            guard let self = self, let response = response else { return }
            self.walkingResponse = response
            let screenshot = UIApplication.shared.keyWindow?.capture()
            if let controllers = self.navigationController?.viewControllers, !controllers.isEmpty {
                for controller in controllers {
                    if let vc = controller as? MapLandingVC {
                        DispatchQueue.main.async {
                            vc.startWalking(response: response, screenshot: screenshot, trackNavigation: trackNavigation, walkathonId: self.contentViewModel?.currentSelectedContent?.contentID, amenityId: amenityId, layerCode: self.contentViewModel?.currentSelectedContent?.layer_code, checkpointName: name, canReroute: canReroute, plannedDestAddress1: plannedDestAddress1, plannedDestAddress2: plannedDestAddress2, plannedDestLat: plannedDestLat, plannedDestLong: plannedDestLong)
                        }
                        break

                    }
                }
            }
        }
    }
    
    private func processCameraChanged() {
        guard let viewModel = self.contentViewModel, let queryFeature = viewModel.queryRenderLayer, let navMapView = navMapView else { return }
        if let calloutView = queryFeature.callOutView {
            calloutView.adjust(to: calloutView.tooltipCoordinate, in: navMapView)
        }
    }
        
    private func showYouAreHereToolTip(force: Bool = false) {
        self.tooltip?.dismiss(animated: true)
        if let coordinate = self.navMapView.mapView.location.latestLocation?.coordinate {
            
            if force {
                self.tooltip = CurrentUserToolTips(currentLocation: coordinate)
                self.tooltip?.present(from: self.view.frame, in: self.navMapView, constrainedTo: self.tooltip?.frame ?? CGRect.zero, animated: true)
                return
            }
            
            if let vm = contentViewModel, let content = vm.currentSelectedContent {
                if !(tooltipShows[content.contentID] ?? false) {
                    self.tooltip = CurrentUserToolTips(currentLocation: coordinate)
                    self.tooltip?.present(from: self.view.frame, in: self.navMapView, constrainedTo: self.tooltip?.frame ?? CGRect.zero, animated: true)
                    tooltipShows[content.contentID] = true
                }
            }
        }
    }
    
    private func setupGestureRecognizers() {
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(panGesture(_:)))
        panGesture.maximumNumberOfTouches = 1
        let pinchGesture = UIPinchGestureRecognizer(target: self, action: #selector(pinchGesture(_:)))
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapGesture(_:)))
        navMapView.addGestureRecognizer(panGesture)
        navMapView.addGestureRecognizer(pinchGesture)
        navMapView.addGestureRecognizer(tapGesture)
        navMapView.mapView.gestures.delegate = self
    }
    
    private func dismissToolTip(){
        
        if let tooltip = self.tooltip, tooltip is CurrentUserToolTips {
            tooltip.dismiss(animated: true)
            self.tooltip = nil
        }
    }
    
    @objc func panGesture(_ gesture: UIPanGestureRecognizer){
       
        if let contentViewModel = contentViewModel {
            let point = gesture.location(in: self.navMapView)
            contentViewModel.queryFeaturesOnTap(point: point,view: self.view)
        }
    }
        
    @objc func pinchGesture(_ gesture: UIPinchGestureRecognizer){
        
        //Not required
    }
    
    @objc func tapGesture(_ gesture: UIPinchGestureRecognizer){
        
        
        
    }
    
    private func toggleDarkLightMode() {
        navMapView.mapView.mapboxMap.style.uri = StyleURI(url: URL(string: false ? Constants.Map.basicNightStyle: Constants.Map.basicStyle)!)
        sendThemeChangeEvent(false)
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ContentVC:ExploreViewDelegate{
    
    func exploreSelected() {
        
        self.navMapView = nil
        self.navigationController?.popViewController(animated: true)
    }
}

extension ContentVC: GestureManagerDelegate {
   
    func gestureManager(_ gestureManager: GestureManager, didEnd gestureType: GestureType, willAnimate: Bool) {
        
        //Not required
    }
    
    func gestureManager(_ gestureManager: GestureManager, didEndAnimatingFor gestureType: GestureType) {
        
        //Not required
    }
    
    func gestureManager(_ gestureManager: GestureManager, didBegin gestureType: GestureType) {
        
        var name = ParameterName.CustomMap.MapInteraction.drag
        switch gestureType {
        case .pinch:
            
            name = ParameterName.CustomMap.MapInteraction.pinch
        case .singleTap:
          
            let point = gestureManager.singleTapGestureRecognizer.location(in: self.navMapView)
            
            if let contentViewModel = contentViewModel {
                
                contentViewModel.queryFeaturesOnTap(point: point,view: self.view)
            }
            
        default:
            name = ParameterName.CustomMap.MapInteraction.drag
        }
        
        AnalyticsManager.shared.logMapInteraction(eventValue: name, screenName: ParameterName.CustomMap.screen_view)

        if gestureType == .pan || gestureType == .pinch || gestureType == .doubleTapToZoomIn || gestureType == .doubleTouchToZoomOut {
            
            self.dismissToolTip()
            isTrackingUser = false
        }        
    }
}

extension ContentVC:ContentViewModelDelegate{
    
    func closeDropPin() {
        //  Not needed
    }
    
    func selectedAmenity(id: String, isSelected: Bool) {
        //No need
    }
    
    func toolTipOpen() {
        
        if let contentViewModel = self.contentViewModel {
            contentViewModel.showParking()
            self.dismissToolTip()
        }
        
        updateBottomSheetIndex(index: 0, toScreen: ParameterName.Content.screen_view)
    }
    
    func navigateHere(address:SearchAddresses?, carPark:Carpark?, layerCode: String?, amenityId: String?, amenityType: String?, amenityCarPark: String?) {
        
        let storybard = UIStoryboard(name: "NewRoutePlanning", bundle: nil)
        if let vc = storybard.instantiateViewController(withIdentifier: String(describing: NewRoutePlanningVC.self)) as? NewRoutePlanningVC {
            vc.originalAddress = address
            vc.layerCode = layerCode
            vc.amenityId = amenityId
            vc.amenityType = amenityType
            vc.addressReceived = true
            if let carPark = carPark {
                
                vc.selectedCarPark = carPark
            }
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
            
    }
    
    func removePreviousRN(){
        
        if let tbrDetailsVC = self.tbrDetailsVC{
            
            self.removeChildViewController(tbrDetailsVC)
            self.tbrDetailsVC = nil
        }
    }
    
    func dismissTBRBeforeAddingNew(){
        
        self.removePreviousRN()
    }
    
    
    private func openProgressScreen(){
        
        let url = "\(appDelegate().backendURL)/user/inbox/messages/progressTracker?category=\(messageCategory ?? "")&type=\(messageType ?? "")&mode=\(isDarkMode ? "dark" : "light")"
        let params = [ // Object data to be used by that screen
            "title": titleProgress,
            "webviewUrl": url,
        ]
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { [weak self] in
            guard let self = self else { return }
            let progressTrackingVc = self.getRNViewBottomSheet(toScreen: RNScreeNames.PROGRESS_TRACKING, navigationParams: params as [String:Any])
            progressTrackingVc.view.frame = UIScreen.main.bounds;
            self.addOnlyChildWithoutRemoving(childViewController: progressTrackingVc, toView: self.view)
            self.progressTrackingVc = progressTrackingVc
        }
    }
    
    
    func toolTipMoreInfo(poi:POI?,carPark:Carpark?,screenName:String){
        
        if let poi = poi {
            self.backBtn?.isHidden = true
            let params = [ // Object data to be used by that screen
                "id": poi.id
            ]
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [weak self] in
                guard let self = self else { return }
                let tbrDetailsVC = self.getRNViewBottomSheet(toScreen: "TBRAmenityDetails", navigationParams: params as [String:Any])
                tbrDetailsVC.view.frame = UIScreen.main.bounds;
                self.addOnlyChildWithoutRemoving(childViewController: tbrDetailsVC, toView: self.view)
                self.tbrDetailsVC = tbrDetailsVC
            }
        }
        else if let carPark = carPark {
            
            let carParkData = screenName == "ParkingCalculator" ? [
                "toScreen": screenName, // Screen name you want to display when start RN
                "navigationParams": [ // Object data to be used by that screen
                    "parkingID": carPark.id ?? "",
                    "fromNative":true,
                    Values.FOCUSED_LAT : Prefs.shared.getStringValue(.latPoi),
                    Values.FOCUSED_LONG : Prefs.shared.getStringValue(.longPoi)
                                    ],
            ] as [String : Any] :[
                "toScreen": screenName, // Screen name you want to display when start RN
                "navigationParams": [ // Object data to be used by that screen
                    "parkingId": carPark.id,
                    Values.FOCUSED_LAT : Prefs.shared.getStringValue(.latPoi),
                    Values.FOCUSED_LONG : Prefs.shared.getStringValue(.longPoi)
                                    ],
            ] as [String : Any]
            let rootView = RNViewManager.sharedInstance.viewForModule(
                "Breeze",
                initialProperties: carParkData)
            
            let reactNativeVC = UIViewController()
            reactNativeVC.view = rootView
            reactNativeVC.modalPresentationStyle = .pageSheet
            self.present(reactNativeVC, animated: true, completion: nil)
        }
        
    }
    
    func startWalkingPath(poi:POI?, trackNavigation: Bool, amenityId: String?, name: String?, canReroute: Bool?){
        if let poi = poi {
            self.startWalking(poi: poi, trackNavigation: trackNavigation, amenityId: amenityId, name: name, canReroute: canReroute)
        }
    }
}

// MARK: - UI setup
extension ContentVC{
    
    private func setupUI() {
        //setUpExploreButton()
        
        
        // MARK: - BREEZES-7624
        //        setupBackButton()
        setupLocationButton()
    }
    
    private func updateWalkathonButton() {
        if userLocationButton.isHidden {
            progressWalkathonButton?.snp.updateConstraints { make in
                make.bottom.equalTo(self.userLocationButton.snp.top).offset(self.userLocationButton.frame.size.height)
            }
        }else {
            progressWalkathonButton?.snp.updateConstraints { make in
                make.bottom.equalTo(self.userLocationButton.snp.top).offset(-10)
            }
        }
    }
    
    private func updateLocationButton(y: Int) {
        userLocationButton.snp.updateConstraints { make in
            make.bottom.equalToSuperview().offset(-(CGFloat(y) + 20))
        }
    }
    
    func setUpExploreButton(){
        
        guard exploreView == nil else { return }
        
        exploreView = ExploreView()
        
        exploreView?.delegate = self
        
        guard let exploreView = exploreView else { return }
        
        navMapView.mapView.addSubview(exploreView)
        
        exploreView.snp.makeConstraints { make in
            make.trailing.equalToSuperview().offset(0)
            make.top.equalTo(self.view.safeAreaLayoutGuide.snp.top).offset(10)
            
        }
        
        // by default, no new content red dot shown in ContentVC
        exploreView.isNewContent = false
        
        exploreView.updatedExploreButtonImage() // make it selected
        
    }
    
    private func setupLocationButton() {
        let userLocationButton = UserLocationButton(buttonSize: (Images.locationImage?.size.width)!)
        userLocationButton.addTarget(self, action: #selector(locationButtonTapped), for: .touchUpInside)
        
        userLocationButton.translatesAutoresizingMaskIntoConstraints = false
        navMapView.mapView.addSubview(userLocationButton)
        userLocationButton.snp.makeConstraints { make in
            make.trailing.equalTo(self.view.safeAreaLayoutGuide.snp.trailing).offset(-20)
            // TODO: Need to adjust when bottom sheet is ready
            make.bottom.equalToSuperview().offset(-(322 + Values.bottomTabHeight + 20))
        }
        
        self.userLocationButton = userLocationButton
        userLocationButton.isHidden = true // hide it by default
    }
    
    private func setupBackButton() {
        // add back button
        let backBtn = UIButton(type: .custom)
        backBtn.frame = CGRect(x: 0, y: 0, width: 48, height: 48)
        backBtn.setImage(UIImage(named: "contentBackButton"), for: .normal)
        backBtn.addTarget(self, action: #selector(onBackBtnClicked), for: .touchUpInside)
        
        backBtn.translatesAutoresizingMaskIntoConstraints = false
        navMapView.mapView.addSubview(backBtn)
        self.backBtn = backBtn
        self.backBtn?.isUserInteractionEnabled = false
        backBtn.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(24)
            make.top.equalTo(self.view.safeAreaLayoutGuide.snp.top).offset(10)
        }
    }
    
    @objc func onBackBtnClicked(){
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CustomMap.UserClick.back, screenName: ParameterName.CustomMap.screen_view)
        SelectedProfiles.shared.clearProfile()
        AmenitiesSharedInstance.shared.amenitiesZoneDelegate = nil
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @objc func locationButtonTapped(sender: UserLocationButton) {
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CustomMap.UserClick.recenter, screenName: ParameterName.CustomMap.screen_view)
        resetCamera()
    }
    
    @objc func pushToProgressTrackingScreen(sender: ProgressWalkathonButton) {
        if let content = self.contentViewModel?.currentSelectedContent {
            let screenName = "custom_map_\(content.name)"
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CustomMap.UserClick.tampines_green_progress_tracking_open, screenName: screenName)
        }
        openProgressScreen()
    }
    
    private func resetCamera() {
        
        isTrackingUser = true
        
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            
            if let contentViewModel = self.contentViewModel{
                
                self.removeTBRAmenity()
                self.closeProgressScreen()
                contentViewModel.resetCamera()
            }
        }
    }
    
    private func setupBottomSheet() {
        let params = data == nil ? [
            "isDarkMode": isDarkMode
        ] : [
            "isDarkMode": isDarkMode,
            "data":"\(data!)"
        ]

        let rnBottomSheetVC = getRNViewBottomSheet(toScreen: "Explore", navigationParams: params as [String:Any])
        rnBottomSheetVC.view.frame = UIScreen.main.bounds;
        //        bottomVC = rnBottomSheetVC.view
        self.addChildViewControllerWithView(childViewController: rnBottomSheetVC,toView: self.view)
    }
    
    private func setupWalkathonButton(_ icon: UIImage) {
        if self.progressWalkathonButton == nil {
            let progressWalkathonButton = ProgressWalkathonButton(buttonSize: (Images.locationImage?.size.width)!)
            progressWalkathonButton.setImage(icon, for: .normal)
            progressWalkathonButton.addTarget(self, action: #selector(pushToProgressTrackingScreen), for: .touchUpInside)
            
            progressWalkathonButton.translatesAutoresizingMaskIntoConstraints = false
            navMapView.mapView.addSubview(progressWalkathonButton)
            progressWalkathonButton.snp.makeConstraints { make in
                make.width.height.equalTo((Images.locationImage?.size.width)!)
                make.trailing.equalTo(self.view.safeAreaLayoutGuide.snp.trailing).offset(-20)
                // TODO: Need to adjust when bottom sheet is ready
                make.bottom.equalTo(self.userLocationButton.snp.top)
            }
            self.progressWalkathonButton = progressWalkathonButton
        }
    }
    
    
    private func downloadTrackProgressIcon(iconLink: String?) {
        
        if let url = URL.init(string: iconLink ?? "") {
            let resource = ImageResource.init(downloadURL: url)
            KingfisherManager.shared.retrieveImage(with: resource, options: nil, progressBlock: nil) {[weak self] result in
                                
                switch result {
                case .success(let value):
                    print("Image: \(value.image). Got from: \(value.cacheType)")
                    self?.setupWalkathonButton(value.image)
                case .failure(let error):
                    if let image = Images.progressWalkathonImage {
                        self?.setupWalkathonButton(image)
                    }
                    print("Error: \(error)")
                }
            }
        }
    }
    
}



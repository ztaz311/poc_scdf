//
//  CruiseViewModel.swift
//  Breeze
//
//  Created by Zhou Hao on 16/12/21.
//

import Foundation
import Turf
import CoreLocation
import Combine
import SwiftyBeaver
import CloudKit
import MapboxMaps
import MapboxNavigation
import MapboxCoreNavigation
import MapboxDirections
import AVFAudio
import OBUSDK

protocol CruiseViewModelDelegate: NSObjectProtocol {
    func showNoCarpark(_ model: CruiseViewModel)
    func hideNoCarpark(_ model: CruiseViewModel)
    func getBearing() -> String
}

final class CruiseViewModel: BreezeObuProtocol {
    
    // MARK: - Public properties
    var mobileMapViewUpdatable: CruiseMobileMapUpdatable? {
        didSet {
            if mobileMapViewUpdatable != nil {
                mobileMapViewUpdatable?.delegate = self
                syncUpdate(mapViewUpdatable: mobileMapViewUpdatable!)
            }
        }
    }
    var carPlayMapViewUpdatable: CruiseCarPlayMapUpdatable? {
        didSet {
            if carPlayMapViewUpdatable != nil {
                carPlayMapViewUpdatable?.delegate = self
                syncUpdate(mapViewUpdatable: carPlayMapViewUpdatable!,override: false)
            }
        }
    }
    
    weak var delegate: CruiseViewModelDelegate?

    // MARK: - Private properties
    private var tripLogger: TripLogger!
    private (set) var isLiveLocationSharingEnabled = false
    @Published private (set) var isTrackingUser = true
    private var currentTrafficIncidentsFeatures: Turf.FeatureCollection?
    private var currentCostERPFeatures: Turf.FeatureCollection?
    private var featureCollection3km = Turf.FeatureCollection(features: [])
    private var featureDetector: FeatureDetector?
    private var currentFeature: FeatureDetectable? {
        
        willSet {
            if newValue?.id != currentFeature?.id {
                if (newValue is DetectedSchoolZone) || (newValue is DetectedSilverZone) || (newValue is DetectedSpeedCamera) || (newValue is DetectedERP) {
                    if !OBUHelper.shared.isObuConnected() {
                        self.onNewFeatureUpdated(newValue)
                    }
                } else {
                    self.onNewFeatureUpdated(newValue)
                }
            }
        }
        didSet {
            var needHandleEHorizon: Bool = false
            if (currentFeature is DetectedSchoolZone) || (currentFeature is DetectedSilverZone) || (currentFeature is DetectedSpeedCamera) || (currentFeature is DetectedERP) {
                if !OBUHelper.shared.isObuConnected() {
                    needHandleEHorizon = true
                }
            } else {
                needHandleEHorizon = true
            }
            
            if needHandleEHorizon {
                DispatchQueue.main.async { [weak self] in
                    guard let self = self else { return }
                    self.updateNotification(featureId: self.currentFeature == nil ? oldValue?.id : self.currentFeature!.id)
                }
            }
        }
    }
    
    private var lastLocation: CLLocation?
    private var lastCarparkLocation: CLLocation?
    private (set) var locationTrackManager: LocationTrackingManager!
    private (set) var isMute = !Prefs.shared.getBoolValue(.enableAudio)
    private var voiceInstructor: VoiceInstructor!
    var carparkUpdater: CarParkUpdater?
//    var disposables = Set<AnyCancellable>()
    private var zoneMonitor: ZoneMonitor?
    
    var notifyArrivalAction: (()->Void)?
    var lastTimeUpdateLocation = Date()
    private let TIME_DURATION: TimeInterval = 1.0
    private var roadName: String?
    
    private var idleTime: TimeInterval = 0
    private var watchdogIdleTime: Date?
    private var currentSpeed: Double = 0.0
    private var limitSpeed: Double = 0.0
    
    var totalTravelTime: Int = -1
    var totalTravelDistance: Int = -1
    
    var userIsMoving: Bool = false
    
    
    // For Carplay
    var carparkViewModel: CarparksViewModel!
    private var filteredCarPark = [Carpark]()
    private var disposables = Set<AnyCancellable>()
    var showCarparkWhenSpeedIsZero: Bool = false
    var showCarparkWhenUserClickCarparkButton: Bool = false
    private var carparkCoordinates = [CLLocationCoordinate2D]()
    
    // MARK: - Methods
    init(etaFav: Favourites?) {
        SwiftyBeaver.debug("startCruise")
        
        //  Setup mock server by default
        OBUHelper.shared.setMode(OBUHelper.shared.getObuMode(), delegate: self)

        voiceInstructor = VoiceInstructor()
        voiceInstructor.synthesizer.delegate = self
        voiceInstructor.synthesizer.managesAudioSession = false // We need to make this false in order to control the volume when navigation is not running
        
        // init PassiveLocationManager
        locationTrackManager = LocationTrackingManager()
        locationTrackManager.delegate = self
        
        // Adding below based on map box suggestions to over come GPS issues
         //https://github.com/mapbox-collab/ncs-collab/issues/159#issuecomment-1034548036
        locationTrackManager.passiveLocationDataSource.systemLocationManager.activityType = .otherNavigation

        // 1. Start Trip logger
        tripLogger = TripLogger(service: TripLogService(), isNavigation: false)
        
        // 2. Log the start coordinate
        let coordinate = LocationManager.shared.location.coordinate
        getAddressFromLocation(location: CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)) { address in
            let obuStatus = OBUHelper.shared.getTripSummaryObuStatus()
            self.tripLogger?.start(startLocation: coordinate, address1: address, address2: "", obuStatus: obuStatus)
            OBUHelper.shared.setTripGUID(self.tripLogger?.getTripGUID() ?? "")
        }
                
        // 3. Disable the idle timer
        UIApplication.shared.isIdleTimerDisabled = true

        // 4. Update camera and custom puck
        mapUpdate()
        
        // since set customized puck will recenter
        isTrackingUser = true

        // 5. Setup feature detector
        setupFeatureDetector()
        
        // 6. Start history data recording
        appDelegate().startCruiseRecordingHistoryData()
        
        // 7. Setup ETA if needed
        if let fav = etaFav {
            ETATrigger.shared.setUpTripETA(etaFav: fav)
            setupETA()
        }
        
        //  8. Handle mock event via appsync
        if OBUHelper.shared.getObuMode() == .mockServer {
            OBUHelper.shared.startBreezeMockEvent()
        } else {
            OBUHelper.shared.startObuEvent()
        }
        
        // Speeding check initial status
        LocationManager.shared.startSpeedCheck()
        
        // MARK: - For Testing: Overspeed

        #if STAGING || TESTING
        // For testing, don't comment out previous test case
        // Test case 1
//        self.userIsMoving = true
//        LocationManager.shared.startSimulateOverspeed(after: 1, speed: 20)
//
//        // Test case 2 - overspeed, during blinking, below the speed limit
//        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
//            LocationManager.shared.startSimulateOverspeed(after: 1, speed: 0)
//            self.userIsMoving = false
//        }
//
//        DispatchQueue.main.asyncAfter(deadline: .now() + 15) {
//            LocationManager.shared.startSimulateOverspeed(after: 1, speed: 100)
//            self.userIsMoving = true
//        }

        // Test case 3 - overspeed, not overspeed, overspeed again
//        DispatchQueue.main.asyncAfter(deadline: .now() + 30) {
//            LocationManager.shared.startSimulateOverspeed(after: 1, speed: 0)
//        }
        #endif
        
/*
        // MARK: - For Testing: Notifications
        DispatchQueue.main.asyncAfter(deadline: .now() + 10) {
            // FlashFlood
            self.currentFeature = DetectedTrafficIncident(id: "1", distance: 10, priority: FeatureDetection.cruisePriorityTraffic, category: TrafficIncidentCategories.FlashFlood, message: "Blk 35 Mandalay Road Mandalay Towers.")
//            self.currentFeature = DetectedERP(id: "2", distance: 282.3, priority: FeatureDetection.cruisePriorityErp, address: "Blk 35 Mandalay Road Mandalay Towers", price: 1.50, zoneId: "A")
//            self.currentFeature = DetectedSchoolZone(id: "3", distance: 299.0, priority: FeatureDetection.cruisePrioritySchoolZone, feature: Turf.Feature(geometry: .point(Point(CLLocationCoordinate2D(latitude: 0, longitude: 0)))))
        }
        
        // MAKRK: - For Testing: Toast
        DispatchQueue.main.asyncAfter(deadline: .now() + 12) {
            self.mobileMapViewUpdatable?.updateTunnel(isInTunnel: true)
        }
*/
    }
    
    deinit {
        SwiftyBeaver.debug("CruiseViewModel deinit: stopCruise")
//        disposables.removeAll()
    
        // clear tripCruiseETA if any
        ETATrigger.shared.tripCruiseETA = nil
        notifyArrivalAction = nil
        
        // Move this to CarPlayMapController
//        carPlayMapViewUpdatable?.setupCustomUserLocationIcon(isCruise: false)
        OBUHelper.shared.stopObuMockEvent()
        OBUHelper.shared.stopBreezeMockEvent()
        OBUHelper.shared.stopObuEvent()
    }
    
    //  Update trip obu status
    func updateTripOBUStatus(_ status: TripOBUStatus) {
        self.tripLogger.updateOBUStatus(status)
    }

    // NOTICE: These two functions may affect navigation updating
    func startLocationUpdate() {
        locationTrackManager.startUpdating()
        recenter()  // need to recenter after viewWillAppear
    }
    
    func stopLocationUpdate() {
    
        //To do: When we have a global location tracking manager
        //locationTrackManager.stopUpdating()
    }
    
    private func startZoneMonitor() {
        guard zoneMonitor == nil else { return } // only start once

        // don't need to check selected profile
        zoneMonitor = ZoneMonitor(origin:LocationManager.shared.location.coordinate)
        zoneMonitor?.start()
//            zoneMonitor?.innerZoneAction = { [weak self] reached in
//                guard let self = self else { return }
//                if reached {
//                    // enable parking button to show carpark
//                    self.onParkingEnabled(true)
//                    // call api to show audio message if the area congested
//                    self.zoneMonitor?.handleZoneAlert(type: "CRUISE", zoneId: SelectedProfiles.shared.getInnerZoneId(), completion: { alert in
//                        guard let alert = alert else { return }
//                        if let message = alert.alertMessage, !message.isEmpty, !self.isMute {
//                            self.voiceInstructor.speak(text: message)
//                        }
//                    })
//                }
//            }
        zoneMonitor?.outterZoneAction = { [weak self] reached in
            guard let self = self else { return }
            if reached {
                // call api to show audio message if the area congested
                self.zoneMonitor?.handleZoneAlert(type: "CRUISE", zoneId: SelectedProfiles.shared.getOuterZoneId(), targetZoneId: SelectedProfiles.shared.getInnerZoneId(), completion: {[weak self] alert in
                    guard let alert = alert else { return }
                    guard let self = self else { return }
                    if let message = alert.alertMessage, !message.isEmpty, !self.isMute {
                        self.voiceInstructor.speak(text: message)
                    }
                })
            }
        }
    }
        
    /*
     To query action on mobileNavigationMapView
     Only for show tooltip at carpark updater
     */
    func queryAmenitiesLayer(point:CGPoint) {
        carparkUpdater?.queryAmenitiesLayer(point: point) { [weak self] detected in
            guard let self = self else { return }
            if detected {
                self.noTracking()
            }
        }        
    }
    
    
    func endCruiseStatus() {
        
        appDelegate().cruiseEndDate = Date()
        carPlayMapViewUpdatable?.removeRoadNamePanel()
        DataCenter.shared.removeERPUpdaterListner(listner: self)
        DataCenter.shared.removeTrafficUpdaterListener(listener: self)
        
        OBUHelper.shared.isCruiseModeFromObuDisplay = false
        
        UIApplication.shared.isIdleTimerDisabled = false
        OBUHelper.shared.setTripGUID("")
        
        // 3. Log
        let coordinate = LocationManager.shared.location.coordinate
        getAddressFromLocation(location: CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)) {[weak self] address in
            // cruise mode is always stopped by user
            self?.tripLogger?.stop(endLocation: coordinate, address1: address, address2: "", stoppedByUser: true, amenityType: "", amenityId: "", layerCode: "", plannedDestAddress1: "", plannedDestAddress2: "", plannedDestLat: 0.0, plannedDestLong: 0.0, idleTime: self?.idleTime ?? 0, bookmarkId: "", bookmarkName: "")
            self?.tripLogger = nil
            
        }
        
        if ETATrigger.shared.tripCruiseETA != nil {
            // In a scenario, force wrapping the lastLocation may cause crash. For example, after starting cruise
            // mode, there is no location update from PassiveLocationManager for some reason, then user end the
            // the cruise mode
            ETATrigger.shared.sendLiveLocationCoordinates(latitude: (self.lastLocation?.coordinate.latitude) ?? 0, longitude: (self.lastLocation?.coordinate.longitude) ?? 0, status: .cancel)
            
            ETATrigger.shared.resetETA() //Clearing the old values
        }
        
        locationTrackManager.stopUpdatingEHorizon()
        
        // 7. Stop history data recording
        appDelegate().cruiseSubmitHistoryData(description: "CruiseMode-End")
        
        if(appDelegate().carPlayMapController != nil){
            
            appDelegate().carPlayMapController?.updateCPToggleButtons(activity: .browsing)
        }
        showCarparkWhenSpeedIsZero = true
        removeCarPark()
        carPlayMapViewUpdatable?.navigationMapView?.navigationCamera.follow()
        
    }
    
    func recenter() {
        isTrackingUser = true
        
        if !showCarparkWhenSpeedIsZero && !showCarparkWhenUserClickCarparkButton {
            // update camera
            mobileMapViewUpdatable?.resetCamera(isCruise: true)
            carPlayMapViewUpdatable?.resetCamera(isCruise: true)
        }
    }
    
    func noTracking() {
        isTrackingUser = false
    }
    
    func dismissTooltips() {
        carparkUpdater?.didDeselectCarparkSymbol()
        carparkUpdater?.calloutView = nil
    }

    func updateTooltipsPosition(naviMapView: NavigationMapView) {
        if let calloutView = carparkUpdater?.calloutView {
            calloutView.adjust(to: calloutView.tooltipCoordinate, in: naviMapView)
        }

    }

    // MARK: Private methods
    private func loadData() {
        DataCenter.shared.addERPUpdaterListner(listner: self)
        DataCenter.shared.addTrafficUpdaterListener(listener: self)
    }

    // call by init or when added a share drive in CruiseModeVC
    func setupETA() {
            
        if let tripETA = ETATrigger.shared.tripCruiseETA {
            ETATrigger.shared.lastLiveLocationUpdateTime = Date().timeIntervalSince1970
            TripETAService().sendTripCruiseETA(tripETA) { [weak self] result in
                guard let self = self else { return }
                                
                switch result {
                case .success(let tripETAResponse):
                    DispatchQueue.main.async {
                        if !self.isMute {
                            self.voiceInstructor.speak(text: "Message sent to \(tripETA.recipientName).")
                        }
                        self.updateETAUI(eta: tripETA)
                        ETATrigger.shared.liveLocTripId = tripETAResponse.tripEtaUUID
                        self.isLiveLocationSharingEnabled = true
                        print("tripETAUUID: \(tripETAResponse.tripEtaUUID)")
                        DataCenter.shared.subscribeToMessageInbox(tripETAId: ETATrigger.shared.liveLocTripId) { [weak self] message in
                            guard let self = self, !message.isEmpty else { return }

                            self.showETAMessage(recipient: tripETA.recipientName, message: message)
                        }
                    }
                case .failure(let error):
                    DispatchQueue.main.async {
                        
                        self.hideShareDriveButton(true)
                    }
                    print(error.localizedDescription)
                }
            }
            // Update CarPlay Toggle buttons for ETA too
            if(appDelegate().carPlayMapController != nil) {
                appDelegate().carPlayMapController?.updateCPToggleButtons(activity: .navigating)
            }

        } else {
            updateETAUI(eta: nil)
            // No ETA, so should not send Reminder
            ETATrigger.shared.isReminder1Sent = true
            isLiveLocationSharingEnabled = false
        }
    }
    
    func hideShareDriveButton(_ isEnabled:Bool){
        mobileMapViewUpdatable?.shareDriveButtonEnabled(isEnabled)
    }
    
    private func updateETAUI(eta: TripCruiseETA?) {
        mobileMapViewUpdatable?.updateETA(eta: eta)
        carPlayMapViewUpdatable?.updateETA(eta: eta)
    }

    private func showETAMessage(recipient: String, message: String) {
        mobileMapViewUpdatable?.showETAMessage(recipient: recipient, message: message)
        carPlayMapViewUpdatable?.showETAMessage(recipient: recipient, message: message)
    }

    private func setupFeatureDetector() {
        featureDetector = FeatureDetector(roadObjectsStore: locationTrackManager.passiveLocationDataSource.roadObjectStore, roadObjectMatcher: locationTrackManager.passiveLocationDataSource.roadObjectMatcher, roadGraph: locationTrackManager.roadGraph)
        currentFeature = nil
        featureDetector?.delegate = self
        featureDetector?.addERPObjectsToRoadMatcher()
        featureDetector?.addSchoolObjectsToRoadMatcher()
        featureDetector?.addSilverZoneObjectsToRoadMatcher()
        featureDetector?.addTrafficIncidentObjectsToRoadMatch()
        featureDetector?.addSpeedCameraObjectsToRoadMatcher()
        featureDetector?.addSeasonParkingsObjectsToRoadMatch()
        locationTrackManager.startUpdatingEHorizon() //Explicitly start eHorizon
    }

    // this will only be called when cruise start
    private func mapUpdate() {
        
        // Update camera
        mobileMapViewUpdatable?.cruiseStatusUpdate(isCruise: true)
        carPlayMapViewUpdatable?.cruiseStatusUpdate(isCruise: true)
        // Update puck
        mobileMapViewUpdatable?.setupCustomUserLocationIcon(isCruise: true)
        carPlayMapViewUpdatable?.setupCustomUserLocationIcon(isCruise: true)
        
        //Update close Notification Alert boolean value and make cpDisplayFeature to nil
        carPlayMapViewUpdatable?.cpFeatureDisplayClose = false
        carPlayMapViewUpdatable?.cpFeatureDisplay = nil
        
        if let templates = appDelegate().carPlayManager.interfaceController?.templates{
            if(templates.count > 1){
                
                if #available(iOS 14, *) {
                    
                    appDelegate().carPlayManager.interfaceController?.popToRootTemplate(animated: true, completion: { value, error in
                        if(!value){
                            SwiftyBeaver.debug("Cruise map update Pop to root template failed", "\(value)","\(String(describing: error))")
                        }
                        
                    })
                }
                else{
                    appDelegate().carPlayManager.interfaceController?.popToRootTemplate(animated: true)
                }
                
            }
        }
    }
    
    /// Sync up UI based on the current status
    private func syncUpdate(mapViewUpdatable: MapViewUpdatable, override: Bool = true) {
        // only start zone monitor when the map is attached, otherwise the park icon will not be added
        startZoneMonitor()
        mapViewUpdatable.setupCustomUserLocationIcon(isCruise: true)
        if override {
            mapViewUpdatable.overridePassiveLocationProvider(locationTrackManager.passiveLocationProvider)
        }
        mapViewUpdatable.cruiseStatusUpdate(isCruise: true)
        if let tripETA = ETATrigger.shared.tripCruiseETA {
            mapViewUpdatable.updateETA(eta: tripETA)
        }
        loadData()
        #if DEBUG
        mapViewUpdatable.navigationMapView?.mapView.addProfileZonesDynamically(zoneColors: SelectedProfiles.shared.getZoneColors(), zoneDetails: SelectedProfiles.shared.getAllZones())
        #endif
    }
    
    private func onNewFeatureUpdated(_ feature: FeatureDetectable?) {
        if let feature = feature {
            SwiftyBeaver.debug("onNewFeatureUpdated: \(feature.id) \(feature.type())")
            
            //  Should ignore RoadClosure when OBU connected
            if OBUHelper.shared.isObuConnected() {
                if feature.type() == TrafficIncidentCategories.RoadClosure {
                    return
                }
            }
            
            // only speak when there is new feature
//            if !OBUHelper.shared.isObuConnected() {
                if feature is DetectedSeasonParking {
                    // voice alert
                    if !isMute {
                        self.voiceInstructor.speak(text: Constants.seasonParkingVoice)
                    }
                    return
                } else {
                    self.voiceInstruction(for: feature)
                }
//            }
            
            if let feature = feature as? DetectedSchoolZone {
                
                DispatchQueue.main.async { [weak self] in
                    
                    guard let self = self else { return }
                    
                    self.mobileMapViewUpdatable?.addSchoolZoneSymbol(feature)
                    self.carPlayMapViewUpdatable?.addSchoolZoneSymbol(feature)
                }
                
            } else if let feature = feature as? DetectedSilverZone {
                DispatchQueue.main.async { [weak self] in
                    
                    guard let self = self else { return }
                    
                    self.mobileMapViewUpdatable?.addSilverZoneSymbol(feature)
                    self.carPlayMapViewUpdatable?.addSilverZoneSymbol(feature)
                }
                
            }
        }
    }
    
    private func onOldFeatureRemoved(_ feature: FeatureDetectable) {
        
        if let feature = feature as? DetectedSchoolZone {
            mobileMapViewUpdatable?.removeSymbol(feature.id)
            carPlayMapViewUpdatable?.removeSymbol(feature.id)
        } else if let feature = feature as? DetectedSilverZone {
            mobileMapViewUpdatable?.removeSymbol(feature.id)
            carPlayMapViewUpdatable?.removeSymbol(feature.id)
        }
    }
    
    func voiceInstruction(for feature: FeatureDetectable) {
        if !isMute {
            SwiftyBeaver.debug("CruieViewModel Voice Instructor speak \(feature)")
            voiceInstructor.speak(feature)
        }
    }

    private func updateNotification(featureId: String?) {
        mobileMapViewUpdatable?.updateNotification(featureId: featureId, currentFeature: currentFeature)
        carPlayMapViewUpdatable?.updateNotification(featureId: featureId, currentFeature: currentFeature)
    }
        
    private func obuTrackLocation(_ location: CLLocation) {
        
//        if !OBUHelper.shared.isObuConnected() {
//            return
//        }
        
        var data = ""
        let tripId = tripLogger?.getTripGUID() ?? ""
        if let name = self.roadName, !name.isEmpty {
            data = "{\"roadName\" : \"\(self.roadName ?? "")\", \"tripGUID\":\"\(tripId)\"}"
        } else {
            data = "{\"tripGUID\":\"\(tripId)\"}"
        }
        
        var bearing = ""
        let userId = AWSAuth.sharedInstance.cognitoId
        
        if let mobileUpdatable = mobileMapViewUpdatable {
            bearing = mobileUpdatable.navigationMapView?.mapView.cameraState.bearing.toString() ?? ""
        } else {
            bearing = carPlayMapViewUpdatable?.navigationMapView?.mapView.cameraState.bearing.toString() ?? ""
        }
        
        if OBUHelper.shared.getObuMode() == .mockServer {
            DataCenter.shared.startUpdatingDeviceUserLocation(userId: userId, bearing: bearing, latitude: location.coordinate.latitude.toString(), longitude: location.coordinate.longitude.toString(), data: data)
        } else {
            DataCenter.shared.startUpdatingOBUDeviceUserLocation(userId: userId, bearing: bearing, latitude: location.coordinate.latitude.toString(), longitude: location.coordinate.longitude.toString(), data: data)
        }
    }
    
    private func calculateIdleTime() {
        if currentSpeed < OBUHelper.shared.idleSpeed {
            if let lastTime = watchdogIdleTime {
                let duration = Date().timeIntervalSince(lastTime)
                if duration > 0 {
                    idleTime += duration
                    print("Idle time: \(idleTime)")
                }
            }
            watchdogIdleTime = Date()
        } else {
            watchdogIdleTime = nil
        }
    }
    
    
    // Get carpark for Carplay
    func callApiGetCarpark() {
        if self.carparkViewModel == nil {
            carparkViewModel = CarparksViewModel(service: CarparkService(), arrivalTime: Date(), destName: "")
        }
        
        if let location = lastLocation {
            let currentLocation = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
            carparkViewModel.load(at: currentLocation, count: UserSettingsModel.sharedInstance.carparkCount, radius: UserSettingsModel.sharedInstance.carparkDistance, maxRadius: Values.carparkMonitorMaxDistanceInLanding)
            carparkViewModel.$carparks.sink { [weak self] carparks in
                guard let self = self, let carparks = carparks else { return }
                
                self.filteredCarPark.removeAll()
                
                if !carparks.isEmpty {
                    self.filteredCarPark.append(contentsOf: carparks)
                }
            }.store(in: &disposables)
        }
    }
    
    private func setZoomLevelWhenCarParkShowCarPlay(location: CLLocationCoordinate2D, coordinates: [CLLocationCoordinate2D]) {
        self.carPlayMapViewUpdatable?.navigationMapView?.navigationCamera.stop()
        var allCoordinates: [CLLocationCoordinate2D] = []
        allCoordinates.append(location)
        allCoordinates.append(contentsOf: coordinates)
        var padding = UIEdgeInsets.zero
        padding = UIEdgeInsets(top: 75, left: 50, bottom: 50, right: 50)
        
        let bearing = carPlayMapViewUpdatable?.navigationMapView?.mapView.cameraState.bearing
        
        if let cameraOptions = carPlayMapViewUpdatable?.navigationMapView?.mapView.mapboxMap.camera(for: allCoordinates,
                                                                                padding: padding,
                                                                                bearing: bearing,
                                                                                pitch: 0), let mapView = carPlayMapViewUpdatable?.navigationMapView?.mapView {
            mapView.camera.ease(to: cameraOptions, duration: 0.4, curve: .easeInOut, completion: nil)
        }
        
    }
    
    
}

// MARK: - FeatureDetectorDelegate
extension CruiseViewModel: FeatureDetectorDelegate {
    
    func featureDetector(_ detector: FeatureDetector, didEnter erp: DetectedERP) {
        // Do nothing here
    }
    
    func featureDetector(_ detector: FeatureDetector, didExit erp: DetectedERP) {
        tripLogger?.exitErp(ERPCost(erpId: Int(erp.id)!, name: erp.address, cost: erp.price, exitTime: Int(Date().timeIntervalSince1970), isOBU: "0"))
    }
    
    func featureDetector(_ detector: FeatureDetector, didUpdate feature: FeatureDetectable?) {
        SwiftyBeaver.debug("Feature detector did update called in CruiseViewModel \(String(describing: feature))")
        currentFeature = feature
    }
    
    func featureDetector(_ detector: FeatureDetector, didExit forId: String) {
        
        // Making CarPlayPrevious displayed notification variable to nil and notification alert close button value to false
        if let carPlayUpdatable = self.carPlayMapViewUpdatable {
            
            //Checking for nil
            if let feature = carPlayUpdatable.cpFeatureDisplay{
                
                //Checking if both are equal
                if( feature == currentFeature){
                    
                    carPlayUpdatable.cpFeatureDisplay = nil
                    carPlayUpdatable.cpFeatureDisplayClose = false
                }
            }
        }
        
        if currentFeature?.id == forId {
            self.currentFeature = nil
        }
        
        // remove symbol for school zone and silver zone
        mobileMapViewUpdatable?.removeSymbol(forId)
        carPlayMapViewUpdatable?.removeSymbol(forId)
    }
    
    func featureDetector(_ detector: FeatureDetector, isInTunnel: Bool) {
        SwiftyBeaver.debug("In tunnel - \(isInTunnel)")
        mobileMapViewUpdatable?.updateTunnel(isInTunnel: isInTunnel)
        carPlayMapViewUpdatable?.updateTunnel(isInTunnel: isInTunnel)
    }
}

// MARK: - LocationTrackingManagerDelegate
extension CruiseViewModel: LocationTrackingManagerDelegate {
    func didUpdate(roadName: String,speedLimit:Double, location: CLLocation?, rawLocation: CLLocation?) {
            
        self.roadName = roadName
        
        mobileMapViewUpdatable?.updateRoadName(roadName)
        mobileMapViewUpdatable?.updateSpeedLimit(speedLimit)
        
        carPlayMapViewUpdatable?.updateRoadName(roadName)
        carPlayMapViewUpdatable?.updateSpeedLimit(speedLimit)
        
        // update road name and speed limit for mobile and carPlay
        let (overSpeed, over1min, over10mins) = LocationManager.shared.checkSpeeding(speedLimit: speedLimit)
        mobileMapViewUpdatable?.overSpeed(value: overSpeed)
        carPlayMapViewUpdatable?.overSpeed(value: overSpeed)
        
        if over10mins || over1min {
            SwiftyBeaver.debug("Overspeed - playAudioCue")
            appDelegate().playAudioCue()
        }
        
        // Update the trip log when in cruise mode
        if let location = location {
            
            //  Save limit speed and current speed
            limitSpeed = speedLimit > 0 ? speedLimit : 0
            
            if Date().timeIntervalSince(lastTimeUpdateLocation) >= TIME_DURATION {
                let speed = location.speed * 3.6 // LocationManager.shared.getCurrentSpeed()
                currentSpeed = speed > 0 ? speed : 0
                calculateIdleTime()
            }
            
            updateLocation(location)
            tripLogger?.update(location: location.coordinate)
            etaCallbacks(location: location)
            
            if (!OBUHelper.shared.isObuConnected() || OBUHelper.shared.getObuMode() != .device) {
                handleShowParkingNearbyIfNeeded()
            }
            
            if let featureDetector = self.featureDetector{
                
                featureDetector.notify3kmRoadObjects(featureCollection: featureCollection3km, currentLocation: location)
                
            }
            
            self.zoneMonitor?.update(currentLocation: location.coordinate)
        }
        
        // tracking the location
        if let location = location {
            lastLocation = location

            if let provider = locationTrackManager.passiveLocationProvider {
                mobileMapViewUpdatable?.updateLocation(location, provider: provider)
                carPlayMapViewUpdatable?.updateLocation(location, provider: provider)
            }
        }
    }
    
    private func etaCallbacks(location:CLLocation){
        
        if let tripETA = ETATrigger.shared.tripCruiseETA{
            
            if  isLiveLocationSharingEnabled && ETATrigger.shared.tripCruiseETA != nil && tripETA.shareLiveLocation.uppercased() == "Y" && !ETATrigger.shared.liveLocTripId.isEmpty  {
                
                ETATrigger.shared.sendLiveLocationCoordinates(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude, status: .inprogress)
            }
            
            //We don't need this ETA reminder API call in cruise. We don't remove the code. We will uncomment when needed
            /*if(LocationManager.shared.getDistanceInMeters(fromLoc: location, toLoc: CLLocation(latitude: tripETA.tripDestLat, longitude: tripETA.tripDestLong)) <= Values.etaCruiseDistance && !ETATrigger.shared.isReminder1Sent)
            {
                ETATrigger.shared.etaReminder(reminderType: TripETA.ETAType.Reminder1)
            }*/
        }
    }
    
    
    
    
    func removeCarPark() {
        if let updater = carparkUpdater {
            self.carPlayMapViewUpdatable?.navigationMapView?.mapView.removeCarparkLayer()
            updater.removeCarParks(keepDestination: true)
        }
    }
    
    private func handleShowParkingNearbyIfNeeded() {
        SwiftyBeaver.debug("speed: \(currentSpeed) km/h")
        
        if currentSpeed < OBUHelper.shared.idleSpeedZero {
            //  When user change state from moving to stopping then show carpark nearby
            if userIsMoving {
                userIsMoving = false
                showCarparkWhenSpeedIsZero = true
                SwiftyBeaver.debug("Cruise handleShowParkingNearbyIfNeeded show carpark nearby: \(currentSpeed) km/h")
                DispatchQueue.main.async {
                    self.onParkingEnabled(true)
                }
            }
            
        } else {
            //  User is moving
            showCarparkWhenSpeedIsZero = false
            if !userIsMoving {
                //  When user change state from stop to moving immediately toggle off carpark nearby
                SwiftyBeaver.debug("Cruise handleShowParkingNearbyIfNeeded stopping to moving then toggle off: \(currentSpeed) km/h")
                DispatchQueue.main.async {
                    self.removeCarPark()
                    self.onParkingEnabled(false)
                    self.recenter()
                }
            }
            
            //  Then update moving state to true
            userIsMoving = true
        }
    }
    
//    private func showCarparkNearbyInCarplay(carparks: [Carpark]? = [], isShow: Bool) {
//        if appDelegate().carPlayConnected {
//            if isShow{
//                if let list = carparks, !list.isEmpty {
//                    for carpark in list {
//                        let carParkCooor = CLLocationCoordinate2D(latitude: carpark.lat ?? 0.0, longitude: carpark.long ?? 0.0)
//                        self.carparkCoordinates.append(carParkCooor)
//                    }
//                    let currentLocation  = CLLocationCoordinate2D(latitude: lastLocation?.coordinate.latitude ?? 0.0, longitude: lastLocation?.coordinate.longitude ?? 0.0)
//                    self.setZoomLevelWhenCarParkShowCarPlay(location: currentLocation, coordinates: self.carparkCoordinates)
//                    self.noTracking()
//                    carPlayMapViewUpdatable?.navigationMapView?.navigationCamera.stop()
//                    carPlayMapViewUpdatable?.navigationMapView?.mapView.addCarparks(list, showOnlyWithCostSymbol: true, belowPuck: false)
//                    
//                }
//            } else {
//                if let updater = carparkUpdater {
//                    carPlayMapViewUpdatable?.navigationMapView?.navigationCamera.follow()
//                    self.isTrackingUser = true
//                    updater.removeCarParks()
//                }
//
//            }
//        }
//    }
    
    // This should be done by MapBox tracking. But it doesn't work now.
    private func updateLocation(_ location: CLLocation) {
        SwiftyBeaver.self.debug("update location = \(location)")
        var heading: Double = 0
        var newLocation = location
        
        if Date().timeIntervalSince(lastTimeUpdateLocation) >= TIME_DURATION {
            lastTimeUpdateLocation = Date()
            self.obuTrackLocation(newLocation)
        }
        
#if targetEnvironment(simulator)
        if lastLocation != nil {
            heading = lastLocation!.coordinate.heading(to: location.coordinate)
        }
        SwiftyBeaver.self.debug("simulation heading = \(heading)")
        newLocation = CLLocation(coordinate: location.coordinate, altitude: location.altitude, horizontalAccuracy: location.horizontalAccuracy, verticalAccuracy: location.verticalAccuracy, course: heading, speed: location.speed, timestamp: location.timestamp)
#else
        heading = newLocation.course
        SwiftyBeaver.self.debug("device heading = \(heading)")
#endif
        
        lastLocation = newLocation
        
        if let carparkLocation = lastCarparkLocation, let location = lastLocation {
            if carparkLocation.distance(from: location) > Double(Values.carparkMonitorDistanceInCruise) {
                onParkingEnabled(false)
            }
        }
        
        // update the camera
        if isTrackingUser {
            if !showCarparkWhenSpeedIsZero && !showCarparkWhenUserClickCarparkButton {    // Not showing carpark
                mobileMapViewUpdatable?.setCamera(at: newLocation.coordinate, heading: heading, animated: true, isCruise: true)
                carPlayMapViewUpdatable?.setCamera(at: newLocation.coordinate, heading: heading, animated: true, isCruise: true, isCarPlay: true)
            }
        }
    }
}

// MARK: - ERPUpdaterListener
extension CruiseViewModel: ERPUpdaterListener {
    func shouldAddERPItems(noCostfeatures:Turf.FeatureCollection, costfeatures:Turf.FeatureCollection) {
        SwiftyBeaver.debug("\(costfeatures.features.count) ERPs added:\n \(costfeatures.features) cost erps, \(noCostfeatures.features.count) no-cost erps")
        
        currentCostERPFeatures = costfeatures
        if let mapViewUpdatable = mobileMapViewUpdatable {
            updateERPLayer(mapViewUpdatable: mapViewUpdatable)
        }
        if let mapViewUpdatable = carPlayMapViewUpdatable {
            updateERPLayer(mapViewUpdatable: mapViewUpdatable)
        }
    }
    
    private func updateERPLayer(mapViewUpdatable: MapViewUpdatable) {
        mapViewUpdatable.removeERPLayer()
        if let features = currentCostERPFeatures {
            if features.features.count > 0 {
                mapViewUpdatable.addERPLayer(features: features)
            }
        }
    }
}

// MARK: - TrafficIncidentUpdaterListener
extension CruiseViewModel: TrafficIncidentUpdaterListener {
    
    func shouldAddTrafficIncident(features:Turf.FeatureCollection) {
        SwiftyBeaver.debug("\(features.features.count) traffic added:")
        
        // Remove old road objects and matcher if it's in cruise mode and add new one
        if let detector = featureDetector, let featureCollection = self.currentTrafficIncidentsFeatures {
            detector.removeTrafficIncidentObjects(featureCollection: featureCollection)
            detector.addTrafficIncidentObjectsToRoadMatch()
        }
        
        // For 3km road objects not from eHorizon instead using Turf Distance
        for feature in features.features {
            
            if case let .string(id) = feature.properties?["id"] {
                
                if(id.contains(TrafficIncidentCategories.FlashFlood) || id.contains(TrafficIncidentCategories.MajorAccident) || id.contains(TrafficIncidentCategories.RoadClosure) || id.contains(TrafficIncidentCategories.HeavyTraffic)){
                    
                    self.featureCollection3km.features.append(feature)
                }
                
            }
        }
        
        currentTrafficIncidentsFeatures = features
        if let mapViewUpdatable = mobileMapViewUpdatable {
            updateTrafficIncidentsLayer(mapViewUpdatable: mapViewUpdatable)
        }
        if let mapViewUpdatable = carPlayMapViewUpdatable {
            updateTrafficIncidentsLayer(mapViewUpdatable: mapViewUpdatable)
        }
    }
    
    private func updateTrafficIncidentsLayer(mapViewUpdatable: MapViewUpdatable) {
        mapViewUpdatable.removeTrafficLayer()
        if let features =  currentTrafficIncidentsFeatures {
            if features.features.count > 0 {
                mapViewUpdatable.addTrafficeLayer(features: features)
            }
        }
    }

}

extension CruiseViewModel: MapViewUpdatableDelegate {
    
    // Currently this only works for mobile first
    func onParkingEnabled(_ value: Bool) {
        
        if value {
            lastCarparkLocation = lastLocation
            carparkUpdater =  CarParkUpdater(destName: "",
                                             carPlayNavigationMapView: carPlayMapViewUpdatable?.navigationMapView,
                                             mobileNavigationMapView: mobileMapViewUpdatable?.navigationMapView,
                                             fromScreen: .cruise)
            carparkUpdater?.delegate = self
            carparkUpdater?.shouldIgnoreDestination = true
            
            if let loc = lastLocation {
                
                carparkUpdater?.loadCarParks(maxCarParksCount: Values.maxNoOfCarparksInCruiseMode, radius: Values.carparkMonitorDistanceInCruise, maxCarParkRadius: Values.carparkMonitorMaxDistanceInLanding, location: loc.coordinate, shortTermOnly: true)
            }
        } else {
            lastCarparkLocation = nil
            removeCarparks()
            carparkUpdater = nil
            self.delegate?.hideNoCarpark(self)
        }
        
        mobileMapViewUpdatable?.updateParkingStatus(isEnabled: value)
        carPlayMapViewUpdatable?.updateParkingStatus(isEnabled: value)
    }
    
    private func removeCarparks() {
        guard let carparkUpdater = self.carparkUpdater else { return }
        carparkUpdater.removeCarParks()
        
        carparkUpdater.removeCallOut()
//        self.mobileMapViewUpdatable?.removeCarParks(carparkViewModel.carparks ?? [])
//        self.carPlayMapViewUpdatable?.removeCarParks(carparkViewModel.carparks ?? [])
    }
    
    func onShareDriveEnabled(_ value: Bool) {
        if value {
            notifyArrivalAction?()
        }
    }
    
    func onSearchAction() {
        //Not required
    }
    
    func onLiveLocationEnabled(_ value: Bool) {
        guard isLiveLocationSharingEnabled != value else { return } // only change onece
        
        if value {
            
            ETATrigger.shared.updateLiveLocationStatus(status: .inprogress)
            
        } else {
            if let location = self.lastLocation {
                if !ETATrigger.shared.liveLocTripId.isEmpty {
                    DataCenter.shared.startUpdatingUserTripLiveLocation(tripId: ETATrigger.shared.liveLocTripId, latitude: location.coordinate.latitude.toString(), longitude: location.coordinate.longitude.toString(), status: LiveLocationStatusType.pause.rawValue,arrivalTime: "",coordinates: "")
                    ETATrigger.shared.updateLiveLocationStatus(status: .pause)
                }
            }
        }
        isLiveLocationSharingEnabled = value
        
        // show toast
        mobileMapViewUpdatable?.updateLiveLocationEnabled(value)
        carPlayMapViewUpdatable?.updateLiveLocationEnabled(value)
    }
        
    func onMuteEnabled(_ value: Bool) {
        isMute = value
        
        Prefs.shared.setValue(!value, forkey: .enableAudio)
        
        if(mobileMapViewUpdatable != nil) {
            mobileMapViewUpdatable?.updateMuteStatus(isEnabled: value)
        }
        else {
            carPlayMapViewUpdatable?.updateMuteStatus(isEnabled: value)
        }
        
    }
    
    func onEndAction() {
        //Not required<#code#>
    }
    
    func onEvChargerAction(_ value: Bool) {
        //Not required
    }
    
    func onPetrolAction(_ value: Bool) {
        //Not required
    }
    
    func onParkingAction() {
        //Not required
    }
}

extension CruiseViewModel:CarParkUpdaterDelegate {
    func carParkNavigateHere(carPark: Carpark?) {
        if let theCarpark = carPark {
            
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Cruise.UserClick.navigate_here_parking, screenName: ParameterName.Cruise.screen_view)
            
            let carparkInfo = ["carpark":theCarpark] as [String : Any]
            NotificationCenter.default.post(name: Notification.Name("onNavigationToCarpark"), object: nil,userInfo: carparkInfo)
        }
    }
    
    func didUpdateCarparks(carParks: [Carpark]?, location: CLLocationCoordinate2D) {
        if carParks?.count == 0 {
            DispatchQueue.main.async {
                self.delegate?.showNoCarpark(self)
            }
            
        }
        else{
            
            self.delegate?.hideNoCarpark(self)
        }
        //  Handle show carparks nearby on Carplay when speed is zero
        SwiftyBeaver.debug("Cruise didUpdateCarparks count: \(carParks?.count ?? 0)")
        if showCarparkWhenSpeedIsZero {
            if let list = carParks, !list.isEmpty {
                carPlayMapViewUpdatable?.navigationMapView?.navigationCamera.stop()
                carPlayMapViewUpdatable?.navigationMapView?.mapView.addCarparks(list, showOnlyWithCostSymbol: true, belowPuck: false)
                
                self.carparkCoordinates.removeAll()
                
                for carpark in list {
                    let carParkCooor = CLLocationCoordinate2D(latitude: carpark.lat ?? 0.0, longitude: carpark.long ?? 0.0)
                    self.carparkCoordinates.append(carParkCooor)
                }
                
                let currentLocation  = CLLocationCoordinate2D(latitude: lastLocation?.coordinate.latitude ?? 0.0, longitude: lastLocation?.coordinate.longitude ?? 0.0)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    self.setZoomLevelWhenCarParkShowCarPlay(location: currentLocation, coordinates: self.carparkCoordinates)
                }
                self.noTracking()
            } 
        } else {
            carPlayMapViewUpdatable?.navigationMapView?.navigationCamera.follow()
            self.removeCarPark()
        }
    }

    
    
    private func logOpenChargingInfo(_ charging: BreezeObuChargingInfo) {
        var analyticMessage = ""
        let type = charging.getType()
        
        if type == ObuEventType.ERP_CHARGING.rawValue || type == ObuEventType.ERP_FAILURE.rawValue || type == ObuEventType.ERP_SUCCESS.rawValue {
            analyticMessage = "message_erp"
        } else if type == ObuEventType.PARKING_SUCCESS.rawValue || type == ObuEventType.PARKING_FEE.rawValue || type == ObuEventType.PARKING_FAILURE.rawValue {
            analyticMessage = "message_parking"
        }
        
        //  add charge amount
        if charging.chargingAmount > 0 {
            analyticMessage.append(", price_$\(String(format: "%.2f", Double(charging.chargingAmount) / 100.0))")
        }
        
        //  Add parking start/end time
        if type == ObuEventType.PARKING_SUCCESS.rawValue || type == ObuEventType.PARKING_FAILURE.rawValue {
            if let parkingString = charging.getParkingStartAndEnd(), !parkingString.isEmpty {
                analyticMessage.append(", \(parkingString)")
            }
        }
        //  Send :popup_open
        let extraData: NSDictionary = ["message": analyticMessage, "location_name": "location_\(self.roadName ?? "")", "longitude": lastLocation?.coordinate.longitude ?? 0.0, "latitude": lastLocation?.coordinate.latitude ?? 0.0]
        
        
        
        if appDelegate().carPlayConnected {
            AnalyticsManager.shared.logOpenPopupEventV2CarPlay(popupName: ParameterName.CruiseMode.UserPopup.cruise_notification, screenName: ParameterName.CarPlayObuState.dhu_cruise, extraData: extraData)
        } else {
            AnalyticsManager.shared.logOpenPopupEventV2(popupName: ParameterName.CruiseMode.UserPopup.cruise_notification, screenName: ParameterName.CruiseMode.screen_view, extraData: extraData)
        }
    }
    
    private func checkLowCardBalance() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 10) {
            // MARK: Check Low Card Balance
            if OBUHelper.shared.isLowBalance() {
                appDelegate().playBeepAudio()
                let cardBalance = "$\(OBUHelper.shared.getCardBalance())"
                self.voiceInstructor.speak(text: Constants.lowCardVoiceMeasage)
                self.carPlayMapViewUpdatable?.showsCPNotificationSchoolZoneTraffic(title: "Low card balance", subTitle: cardBalance, icon: UIImage(named: "Miscellaneous-Icon"))
                
                if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                    if appDelegate.appInBackground {
                        appDelegate.showLocalNotificationCharges(title: "Low card balance - \(cardBalance)", subTitle: "Please top up your card" )
                    }
//                    else {
//                        appDelegate.showOnAppErrorNotification(title: "Low card balance - \(cardBalance)", subTitle: "Please top up your card")
//                    }
                }
            }
        }
    }
    
    private func handleErpAndParkingNotification(_ charging: BreezeObuChargingInfo) {
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate, appDelegate.appInBackground {
            let type = charging.getType()
            let message = charging.getMessage()
            let price = charging.getChargeAmount()
            
            if type == ObuEventType.ERP_FAILURE.rawValue || type == ObuEventType.ERP_SUCCESS.rawValue {
                appDelegate.showLocalNotificationCharges(title: "ERP Fee - \(price)", subTitle: message)
            } else if type == ObuEventType.PARKING_FAILURE.rawValue || type == ObuEventType.PARKING_SUCCESS.rawValue {
                appDelegate.showLocalNotificationCharges(title: "Parking Fee - \(price)", subTitle: message)
            }
        }
    }
    
}

extension CruiseViewModel: SpeechSynthesizingDelegate {
    func speechSynthesizer(_ speechSynthesizer: SpeechSynthesizing, willSpeak instruction: SpokenInstruction) -> SpokenInstruction? {

        AVAudioSession.sharedInstance().tryDuckOtherAudio()
        SwiftyBeaver.debug("willSpeak: \(instruction.text)")
        return voiceInstructor.willSpeak(instruction, stepDistance: 0)
    }
        
    func speechSynthesizer(_ speechSynthesizer: SpeechSynthesizing, didSpeak instruction: SpokenInstruction, with error: SpeechError?) {
        
        SwiftyBeaver.debug("Cruise mode Instruction Played")
        AVAudioSession.sharedInstance().tryUnduckOtherAudio()
    }
    
    func speechSynthesizer(_ speechSynthesizer: SpeechSynthesizing, encounteredError error: SpeechError) {        
        AVAudioSession.sharedInstance().tryUnduckOtherAudio()
    }
}

extension CruiseViewModel: BreezeObuConnectionDelegate {
    
    func onOBUDisconnected(_ device: OBUSDK.OBU, error: NSError?) {
        //  Handle OBU disconnected
        self.tripLogger.updateOBUStatus(.notConnected)
    }
    
    func onOBUConnectionFailure(_ error: NSError) {
        //  Handle OBU connection failed
        self.tripLogger.updateOBUStatus(.notConnected)
    }
    
    func onBluetoothStateUpdated(_ state: OBUSDK.BluetoothState) {
        //  Handle bluetooth change status
    }
    
    func onOBUConnected(_ device: OBUSDK.OBU) {
        self.tripLogger.updateOBUStatus(.connected)
        //  Connect to OBU success
    }
}

extension CruiseViewModel: BreezeObuDataDelegate {
    
    func onVelocityInformation(_ velocity: Double) {
        //  Update speed OBU return velocity in m/s
        if OBUHelper.shared.getObuMode() == .device {
            currentSpeed = velocity * 3.6   //  convert m/s to km/h
            
//            updateOBUSpeed(currentSpeed, limitSpeed: limitSpeed)
            calculateIdleTime()
            handleShowParkingNearbyIfNeeded()
            
        }
    }
    
    func onChargingInformation(_ chargingInfo: [BreezeObuChargingInfo]) {
        //  Play beep sound when receive ERP charging information
        if let first = OBUHelper.shared.getPriorityChargingInfo(chargingInfo) {
            
            let notiDic = first.getNotiDic()
            print("chargingInfo: \(notiDic)")
            
            let type = first.getType()
            if type == ObuEventType.ERP_CHARGING.rawValue {
                if Prefs.shared.getBoolValue(.enableAudio) {
                    voiceInstructor.speak(text: "ERP ahead")
                }
            } else if (type == ObuEventType.ERP_SUCCESS.rawValue || type == ObuEventType.PARKING_SUCCESS.rawValue || type == ObuEventType.PARKING_FEE.rawValue ){
                //  Play beep sound when receive ERP charging success event
                appDelegate().playBeepAudio()
            }
            
            if type == ObuEventType.ERP_SUCCESS.rawValue || type == ObuEventType.ERP_CHARGING.rawValue {
                carPlayMapViewUpdatable?.showsCPNotificationObuERP(first)
            } else if type == ObuEventType.PARKING_SUCCESS.rawValue {
                carPlayMapViewUpdatable?.showsCPNotificationSchoolZoneTraffic(title: first.getMessage(), subTitle: first.getChargeAmount(), icon: UIImage(named: "cp-parkingFee")!)
            } else if type == ObuEventType.PARKING_FAILURE.rawValue {
                appDelegate().playBeepAudio()
                carPlayMapViewUpdatable?.showsCPNotificationSchoolZoneTraffic(title: first.getCardErrorMessage(), subTitle: first.getChargeAmount(), icon: UIImage(named: "ParkingRedIcon")!)
            } else if  type == ObuEventType.ERP_FAILURE.rawValue {
                appDelegate().playBeepAudio()
                carPlayMapViewUpdatable?.showsCPNotificationSchoolZoneTraffic(title: first.getCardErrorMessage(), subTitle: first.getMessage(), icon: UIImage(named: "erpRedIcon")!)
            }
            
            logOpenChargingInfo(first)
            
            if (type == ObuEventType.ERP_SUCCESS.rawValue || type == ObuEventType.PARKING_SUCCESS.rawValue || type == ObuEventType.ERP_FAILURE.rawValue || type == ObuEventType.PARKING_FAILURE.rawValue) {
                let tripId = tripLogger?.getTripGUID() ?? ""
                if !tripId.isEmpty {
                    handleErpAndParkingNotification(first)
                }
            }
            
            if (type == ObuEventType.ERP_SUCCESS.rawValue || type == ObuEventType.PARKING_SUCCESS.rawValue) {
                let tripId = tripLogger?.getTripGUID() ?? ""
                if !tripId.isEmpty {
                    self.checkLowCardBalance()
                }
            }
        }
        
        let tripId = tripLogger?.getTripGUID() ?? ""
        if !tripId.isEmpty {
            ObuDataService().sendTripObuErp(chargingInfo, tripGUID: tripId, roadName: self.roadName, lat: lastLocation?.coordinate.latitude ?? 0, long: lastLocation?.coordinate.longitude ?? 0) { result in
                switch result {
                case .success(_):
                    print("Send charging information success")
                case .failure(let error):
                    print("Send charging information failed: \(error.localizedDescription)")
                }
            }
        }
        
       
    }
    
    func onTrafficInformation(_ trafficInfo: BreezeObuTrafficInfo) {
                
        var analyticMessage = "message_traffic"
        if let parking = trafficInfo.trafficParking {
            //  Get all carpark names then send event to RN
            analyticMessage = "message_parking_availabilty"
            
            var carparks: [Any] = []
            for item in parking.dataList {
                let carparkDic = ["name": item.location, "availableLots": item.lots, "color": item.color]
                carparks.append(carparkDic)
            }
            
            //  Fake data when receive events
            if OBUHelper.shared.getObuMode() == .mockSDK {
                carparks.removeAll()
                carparks.append(["name": "NCS Hub", "availableLots": 150, "color": "Red"])
                carparks.append(["name": "Resorts World", "availableLots": 192, "color": "Yellow"])
                carparks.append(["name": "HarbourFront Centre", "availableLots": 186, "color": "Yellow"])
                carparks.append(["name": "Keppel Bay", "availableLots": 101, "color": "Red"])
                carparks.append(["name": "Mount Faber Park", "availableLots": 122, "color": "Red"])
            }
            
            if !carparks.isEmpty {
                
                if UserSettingsModel.sharedInstance.obuParkingAvailabilityAudioEnable {
                    appDelegate().playBeepAudio()
                    playSpokenText(text: parking.getSpokenText())
                }
                
                if UserSettingsModel.sharedInstance.obuParkingAvailabilityDisplayEnable {
                    //  Send :popup_open
                    let extraData: NSDictionary = ["message": analyticMessage, "location_name": "location_\(self.roadName ?? "")", "longitude": lastLocation?.coordinate.longitude ?? 0.0, "latitude": lastLocation?.coordinate.latitude ?? 0.0]
                    if appDelegate().carPlayConnected {
                        AnalyticsManager.shared.logOpenPopupEventV2CarPlay(popupName: ParameterName.CruiseMode.UserPopup.cruise_notification, screenName: ParameterName.CarPlayObuState.dhu_cruise, extraData: extraData)
                    } else {
                        AnalyticsManager.shared.logOpenPopupEventV2(popupName: ParameterName.CruiseMode.UserPopup.cruise_notification, screenName: ParameterName.CruiseMode.screen_view, extraData: extraData)
                    }
                    
                    //  Show alert on Carplay and see more
                    carPlayMapViewUpdatable?.displayOBUNotificationCarPark(parking.dataList, item: parking.getInfoToDisplayOnCarplay())
                }
                
            }
        } else if let travel = trafficInfo.travelTime {
            //  Get travel time info then send event to RN
            
            analyticMessage = "message_traveltime"
            
            var params: [[String: Any]] = []
            for item in travel.dataList {
                let dic: [String: Any] = ["name": item.location, "estTime": item.min, "color": item.color]
                params.append(dic)
            }
            
            //  Fake data when receive events
            if OBUHelper.shared.getObuMode() == .mockSDK {
                params.removeAll()
                params.append(["name": "Woodlands Ave 2", "estTime": "29 min", "color": "Red"])
                params.append(["name": "SLE", "estTime": "20 min", "color": "Yellow"])
                params.append(["name": "AMK AV1", "estTime": "13 min", "color": "Yellow"])
                params.append(["name": "Yishun AV2", "estTime": "12 min", "color": "Yellow"])
            }
            
            if !params.isEmpty {
                if UserSettingsModel.sharedInstance.obuEstimatedTravelTimeAudioEnable {
                    appDelegate().playBeepAudio()
                    //  Play voice instruction
                    self.playSpokenText(text: travel.getSpokenText())
                }
                
                if UserSettingsModel.sharedInstance.obuEstimatedTravelTimeDisplayEnable {
                    
                    //  Send :popup_open
                    let extraData: NSDictionary = ["message": analyticMessage, "location_name": "location_\(self.roadName ?? "")", "longitude": lastLocation?.coordinate.longitude ?? 0.0, "latitude": lastLocation?.coordinate.latitude ?? 0.0]
                    if appDelegate().carPlayConnected {
                        AnalyticsManager.shared.logOpenPopupEventV2CarPlay(popupName: ParameterName.CruiseMode.UserPopup.cruise_notification, screenName: ParameterName.CarPlayObuState.dhu_cruise, extraData: extraData)
                    } else {
                        AnalyticsManager.shared.logOpenPopupEventV2(popupName: ParameterName.CruiseMode.UserPopup.cruise_notification, screenName: ParameterName.CruiseMode.screen_view, extraData: extraData)
                    }
                    
                    //  Show alert on Carplay and see more
                    carPlayMapViewUpdatable?.displayOBUNotificationTravelTime(travel.dataList)
                }
            }
        }else {
            
            var needDisplayAlert = true
            var needVoiceAlert = true
            if let text = trafficInfo.trafficText {
                if text.icon == .some("2002") {
                    needDisplayAlert = UserSettingsModel.sharedInstance.schoolZoneDisplayAlert
                    needVoiceAlert = UserSettingsModel.sharedInstance.schoolZoneVoiceAlert
                } else if text.icon == .some("2003") {
                    needDisplayAlert = UserSettingsModel.sharedInstance.silverZoneDisplayAlert
                    needVoiceAlert = UserSettingsModel.sharedInstance.silverZoneVoiceAlert
                } else if text.icon == .some("2004") {
                    needDisplayAlert = UserSettingsModel.sharedInstance.busLaneDisplayAlert
                    needVoiceAlert = UserSettingsModel.sharedInstance.busLaneVoiceAlert
                }
            }
            
            let traficNotiDic = trafficInfo.getTrafficNotiDic(roadName ?? "")
            print("traficNotiDic: \(traficNotiDic)")
            
            if let message = traficNotiDic["message"] as? String {
                analyticMessage = "message_\(message)"
            }
            
            let title = traficNotiDic["message"] as? String
            let distance = traficNotiDic["distance"] as? String
            
            //  Play beep
            if let text = trafficInfo.trafficText {
                if needDisplayAlert {
                    if text.icon == .some("1412") {
                        //  Show map when accident happen
                        carPlayMapViewUpdatable?.showsCPNotificationObuTraffic(title: title ?? "", subTitle: distance ?? "")
                    } else if text.icon == .some("2002") {
                        //  Show School zone / Silver zone alert on Carplay
                        carPlayMapViewUpdatable?.showsCPNotificationSchoolZoneTraffic(title: title ?? "", subTitle: distance ?? "", icon: UIImage(named: "carplay-schoolZoneIcon")!)
                    } else if text.icon == .some("2003") {
                        carPlayMapViewUpdatable?.showsCPNotificationSchoolZoneTraffic(title: title ?? "", subTitle: distance ?? "", icon: UIImage(named: "silverZoneIcon")!)
                    } else if text.icon == .some("9011") {
                        carPlayMapViewUpdatable?.showsCPNotificationSchoolZoneTraffic(title: title ?? "", subTitle: distance ?? "", icon: UIImage(named: "cp-speedCamera")!)
                    } else if text.icon == .some("1410") {
                        carPlayMapViewUpdatable?.showsCPNotificationSchoolZoneTraffic(title: title ?? "", subTitle: distance ?? "" , icon: UIImage(named: "roadClosureIcon")!)
                    } else if text.icon == .some("2004") {
                        let detailTitle = traficNotiDic["detailTitle"] as? String
                        carPlayMapViewUpdatable?.showsCPNotificationSchoolZoneTraffic(title: title ?? "", subTitle: detailTitle ?? "", icon: UIImage(named: "bus-lane-carPlay")!, timeDismiss: Int(10.0))
                    } else {
                        carPlayMapViewUpdatable?.showsCPNotificationSchoolZoneTraffic(title: title ?? "", subTitle: "", icon: UIImage(named: "carplay-Miscellaneous-Icon")!)
                    }
                }
                
                if text.icon == .some("2004") {
                    //  Play beep sound for bus lane
                    if needVoiceAlert {
                        appDelegate().playBeepAudio()
                    }
                } else {
                    if needVoiceAlert {
                        let spokenText = trafficInfo.getSpokenText()
                        if !isMute && !spokenText.isEmpty {
                            voiceInstructor.speak(text: spokenText)
                        }
                    }
                }
            }
            
            //  Send :popup_open
            if needDisplayAlert {
                let extraData: NSDictionary = ["message": analyticMessage, "location_name": "location_\(self.roadName ?? "")", "longitude": lastLocation?.coordinate.longitude ?? 0.0, "latitude": lastLocation?.coordinate.latitude ?? 0.0]
                if appDelegate().carPlayConnected {
                    AnalyticsManager.shared.logOpenPopupEventV2CarPlay(popupName: ParameterName.CruiseMode.UserPopup.cruise_notification, screenName: ParameterName.CarPlayObuState.dhu_cruise, extraData: extraData)
                } else {
                    AnalyticsManager.shared.logOpenPopupEventV2(popupName: ParameterName.CruiseMode.UserPopup.cruise_notification, screenName: ParameterName.CruiseMode.screen_view, extraData: extraData)
                }
            }
        }
    }
    
    func onErpChargingAndTrafficInfo(trafficInfo: BreezeObuTrafficInfo?, chargingInfo: [BreezeObuChargingInfo]?) {
        //  Nothing to be done here, will handle in separated callback
    }
    
    func onPaymentHistories(_ histories: [BreezePaymentHistory]) {
        let tripId = tripLogger?.getTripGUID() ?? ""
        if !tripId.isEmpty {
            ObuDataService().sendObuTransactions(histories, tripGUID: tripId) { result in
                switch result {
                case .success(_):
                    print("Send transactions success")
                case .failure(let error):
                    print("Send transactions failed: \(error.localizedDescription)")
                }
            }
        }
    }
    
    func onTravelSummary(_ travelSummary: BreezeTravelSummary) {
        let tripId = tripLogger?.getTripGUID() ?? ""
        if !tripId.isEmpty {
            
            if let time = travelSummary.totalTravelTime, let distance = travelSummary.totalTravelDistance, (totalTravelTime != Int(time) || totalTravelDistance != Int(distance)) {
                
                totalTravelTime = Int(time)
                totalTravelDistance = Int(distance)
                
                ObuDataService().sendObuTripSummary(travelSummary, tripGUID: tripId) { result in
                    switch result {
                    case .success(_):
                        print("Send travel summary success")
                    case .failure(let error):
                        print("Send travel summary failed: \(error.localizedDescription)")
                    }
                }
            }
        }
    }
    
    func onTotalTripCharged(_ totalCharged: TravelCharge) {
        //  Already covered in onTravelSummary
    }
    
    func onError(_ errorCode: NSError) {
        //  Nothing to be done here
    }
    
    func onCardInformation(_ status: BreezeCardStatus?, paymentMode: BreezePaymentMode?, chargingPaymentMode: BreezeChargingPaymentMode?, balance: Int) {
        //  Nothing to be done here -> will remove cruise mode later on
    }
}

extension CruiseViewModel: BreezeServerMockProtocol {
    
    private func playSpokenText(text: String?) {
        if !isMute {
            if let spokenText = text, !spokenText.isEmpty {
                self.voiceInstructor.speak(text: spokenText)
            }
        }
    }

    func onReceiveObuEvent(_ event: ObuMockEventModel) {
        if let eventType = ObuEventType(rawValue: event.eventType) {
            
            var analyticMessage = "message_\(eventType.rawValue)"
            
            //  These two variables only apply for school zone, silver zone and bus lane event
            var needDisplayAlert = true
            var needVoiceAlert = true
            if eventType == .SCHOOL_ZONE {
                needDisplayAlert = UserSettingsModel.sharedInstance.schoolZoneDisplayAlert
                needVoiceAlert = UserSettingsModel.sharedInstance.schoolZoneVoiceAlert
            } else if eventType == .SILVER_ZONE {
                needDisplayAlert = UserSettingsModel.sharedInstance.silverZoneDisplayAlert
                needVoiceAlert = UserSettingsModel.sharedInstance.silverZoneVoiceAlert
            } else if eventType == .BUS_LANE {
                needDisplayAlert = UserSettingsModel.sharedInstance.busLaneDisplayAlert
                needVoiceAlert = UserSettingsModel.sharedInstance.busLaneVoiceAlert
            }
            
            switch eventType {
            case .PARKING_AVAILABILITY:
                if UserSettingsModel.sharedInstance.obuParkingAvailabilityAudioEnable {
                    appDelegate().playBeepAudio()
                }
                
                if UserSettingsModel.sharedInstance.obuParkingAvailabilityDisplayEnable {
                        if appDelegate().carPlayConnected {
                            let extraData: NSDictionary = ["message": analyticMessage, "location_name": "location_\(self.roadName ?? "")", "longitude": lastLocation?.coordinate.longitude ?? 0.0, "latitude": lastLocation?.coordinate.latitude ?? 0.0]
                            if OBUHelper.shared.getObuMode() == .device {
                                
                                AnalyticsManager.shared.logOpenPopupEventV2CarPlay(popupName: ParameterName.CruiseMode.UserPopup.cruise_notification, screenName: ParameterName.CarPlayObuState.dhu_cruise, extraData: extraData)
                            }
                        } else {
                            let extraData: NSDictionary = ["message": analyticMessage, "location_name": "location_\(self.roadName ?? "")", "longitude": lastLocation?.coordinate.longitude ?? 0.0, "latitude": lastLocation?.coordinate.latitude ?? 0.0]
                            if OBUHelper.shared.getObuMode() == .device {
                                
                                AnalyticsManager.shared.logOpenPopupEventV2(popupName: ParameterName.CruiseMode.UserPopup.cruise_notification, screenName: ParameterName.CruiseMode.screen_view, extraData: extraData)
                            }
                        }
                    //  Show alert on Carplay and see more
                    carPlayMapViewUpdatable?.displayOBUNotificationCarPark(event.getParkingList(), item: event.getInfoToDisplayOnCarplay())
                }
                
            case .TRAVEL_TIME:
                if UserSettingsModel.sharedInstance.obuEstimatedTravelTimeAudioEnable {
                    appDelegate().playBeepAudio()
                }
                
                if UserSettingsModel.sharedInstance.obuEstimatedTravelTimeDisplayEnable {
                    if appDelegate().carPlayConnected {
                        let extraData: NSDictionary = ["message": analyticMessage, "location_name": "location_\(self.roadName ?? "")", "longitude": lastLocation?.coordinate.longitude ?? 0.0, "latitude": lastLocation?.coordinate.latitude ?? 0.0]
                        if OBUHelper.shared.getObuMode() == .device {
                            
                            AnalyticsManager.shared.logOpenPopupEventV2CarPlay(popupName: ParameterName.CruiseMode.UserPopup.cruise_notification, screenName: ParameterName.CarPlayObuState.dhu_cruise, extraData: extraData)
                        }
                    } else {
                        let extraData: NSDictionary = ["message": analyticMessage, "location_name": "location_\(self.roadName ?? "")", "longitude": lastLocation?.coordinate.longitude ?? 0.0, "latitude": lastLocation?.coordinate.latitude ?? 0.0]
                        if OBUHelper.shared.getObuMode() == .device {
                            
                            AnalyticsManager.shared.logOpenPopupEventV2(popupName: ParameterName.CruiseMode.UserPopup.cruise_notification, screenName: ParameterName.CruiseMode.screen_view, extraData: extraData)
                        }
                    }
                    //  Show alert on Carplay and see more
                    carPlayMapViewUpdatable?.displayOBUNotificationTravelTime(event.getTravelTimeList())
                }
                
            case .CARD_ERROR, .CARD_EXPIRED, .CARD_INSUFFICIENT, .CARD_VALID:
                if OBUHelper.shared.getObuMode() != .device {
                    
                    if let balance = event.balance, (eventType == .CARD_INSUFFICIENT || eventType == .CARD_VALID) {
                        OBUHelper.shared.CARD_BALANCE = Float(balance)
                    }

                    if eventType != .CARD_VALID {
                        var title = ""
                        if eventType == .CARD_ERROR {
                            title = "Card Error"
                        } else if eventType == .CARD_EXPIRED {
                            title = "Card Expired"
                        } else if eventType == .CARD_INSUFFICIENT {
                            title = "Card Insufficient"
                        } else {
                            title = "System Error"
                        }
                        appDelegate().playBeepAudio()
                        carPlayMapViewUpdatable?.showsCPNotificationSchoolZoneTraffic(title: title, subTitle: "Deduction Failed", icon: UIImage(named: "erpfailed-icon-carplay")!)
                    }
                }
                break
            case .ERP_CHARGING, .ERP_SUCCESS, .ERP_FAILURE, .PARKING_FEE, .PARKING_SUCCESS, .PARKING_FAILURE:
                
                if !OBUHelper.shared.isObuConnected() {
                    return
                }
                
                //  TODO show ERP alert on Carplay
                if eventType == .ERP_SUCCESS || eventType == .ERP_FAILURE {
                    appDelegate().playBeepAudio()
                    
                    if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                        if appDelegate.appInBackground {
                            appDelegate.showLocalNotificationCharges(title: "ERP Fee - \(event.chargeAmount ?? "")", subTitle: event.message ?? "")
                        }
                    }
                    
                }
                
                if eventType == .PARKING_SUCCESS || eventType == .PARKING_FEE {
                    appDelegate().playBeepAudio()
                    if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                        if appDelegate.appInBackground {
                            appDelegate.showLocalNotificationCharges(title: "Parking Fee - \(event.chargeAmount ?? "")", subTitle: event.message ?? "")
                        }
                    }
                }
                
                if eventType == .ERP_SUCCESS || eventType == .ERP_FAILURE || eventType == .ERP_CHARGING {
                    carPlayMapViewUpdatable?.showsCPNotificationObuERP(eventDic: event.getNotiDic())
                } else if eventType == .PARKING_SUCCESS || eventType == .PARKING_FEE {
                    carPlayMapViewUpdatable?.showsCPNotificationSchoolZoneTraffic(title: event.message ?? "", subTitle: event.chargeAmount ?? "", icon: UIImage(named: "cp-parkingFee")!)
                } else if eventType == .PARKING_FAILURE {
                    carPlayMapViewUpdatable?.showsCPNotificationSchoolZoneTraffic(title: event.message ?? "", subTitle: event.chargeAmount ?? "", icon: UIImage(named: "ParkingRedIcon")!)
                }
                
                //  add charge amount
                if let amount = event.chargeAmount, !amount.isEmpty {
                    analyticMessage.append(", price_\(amount)")
                }
                
                if eventType == .ERP_SUCCESS || eventType == .PARKING_SUCCESS {
                    checkLowCardBalance()
                }
               
                if !event.chargingInfo.isEmpty {
                    var infos: [BreezeObuChargingInfo] = []
                    for item in event.chargingInfo {
                        infos.append(item.getChargingInfo())
                    }
                    let tripId = tripLogger?.getTripGUID() ?? ""
                    if !tripId.isEmpty {
                        ObuDataService().sendTripObuErp(infos, tripGUID: tripId, roadName: self.roadName, lat: lastLocation?.coordinate.latitude ?? 0, long: lastLocation?.coordinate.longitude ?? 0) { result in
                            switch result {
                            case .success(_):
                                print("Send charging information success")
                            case .failure(let error):
                                print("Send charging information failed: \(error.localizedDescription)")
                            }
                        }
                    }
                }
            case .TRAFFIC:
                //  BREEZE2-1800 - Removed
//                carPlayMapViewUpdatable?.showsCPNotificationObuTraffic(title: event.message ?? "", subTitle: event.distance ?? "")
                return
            case .SCHOOL_ZONE:
                //  show School Zone alert on Carplay, beep not required
                if needDisplayAlert {
                    carPlayMapViewUpdatable?.showsCPNotificationSchoolZoneTraffic(title: event.message ?? "", subTitle: event.distance ?? "", icon: UIImage(named: "carplay-schoolZoneIcon")!)
                }
            case .SILVER_ZONE:
                if needDisplayAlert {
                    carPlayMapViewUpdatable?.showsCPNotificationSchoolZoneTraffic(title: event.message ?? "", subTitle: event.distance ?? "", icon: UIImage(named: "silverZoneIcon")!)
                }
            case .ROAD_CLOSURE:
                //  BREEZE2-1800 - Removed
//                if !OBUHelper.shared.isObuConnected() {
//                    appDelegate().playBeepAudio()
//                    carPlayMapViewUpdatable?.showsCPNotificationSchoolZoneTraffic(title: event.message ?? "", subTitle: "\(event.detailTitle ?? "") \(event.detailMessage ?? "")", icon: UIImage(named: "roadClosureIcon")!)
//                }
                return
            case .BUS_LANE:
                if needVoiceAlert {
                    appDelegate().playBeepAudio()
                }
                
                if needDisplayAlert {
                    carPlayMapViewUpdatable?.showsCPNotificationSchoolZoneTraffic(title: event.message ?? "", subTitle: event.detailMessage ?? "In Operation", icon: UIImage(named: "bus-lane-carPlay")!,timeDismiss: Int(10.0))
                }
            case .ROAD_DIVERSION:
                //  Beep not required
                carPlayMapViewUpdatable?.showsCPNotificationSchoolZoneTraffic(title: event.message ?? "", subTitle: event.detailMessage ?? "", icon: UIImage(named: "roadDiversionIcon-cp")!)
            case .GENERAL_MESSAGE:
                //  Beep not required
                carPlayMapViewUpdatable?.showsCPNotificationSchoolZoneTraffic(title: event.message ?? "", subTitle: event.detailMessage ?? "", icon: UIImage(named: ""))
            case .SPEED_CAMERA:
                //  Beep not required
                carPlayMapViewUpdatable?.showsCPNotificationSchoolZoneTraffic(title: event.message ?? "", subTitle: event.detailMessage ?? "", icon: UIImage(named: "cp-speedCamera")!)
                // MARK: update new event BREEZE2-602
            case .FLASH_FLOOD, .HEAVY_TRAFFIC, .MAJOR_ACCIDENT, .SEASON_PARKING:
                updateOBUNoti(event.getNotiDic())
                if let image = UIImage(named: event.getIconEventOBU(true)) {
                    carPlayMapViewUpdatable?.showsCPNotificationSchoolZoneTraffic(title: event.message ?? "", subTitle: event.detailMessage ?? "", icon: image)
                }
            case .VEHICLE_BREAKDOWN, .UNATTENDED_VEHICLE, .ROAD_WORK, .ROAD_BLOCK, .MISCELLANEOUS, .TRAFFIC, .OBSTACLE, .TREE_PRUNING, .YELLOW_DENGUE_ZONE, .RED_DENGUE_ZONE:
                //  BREEZE2-1800 - Removed
                return
            case .LOW_CARD:
                appDelegate().playBeepAudio()
                voiceInstructor.speak(text: Constants.lowCardVoiceMeasage)
                let price = event.chargeAmount ?? ""
                DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
                    self.carPlayMapViewUpdatable?.showsCPNotificationSchoolZoneTraffic(title: event.message ?? "", subTitle: event.chargeAmount ?? "", icon: UIImage(named: "Miscellaneous-Icon")!)
                }
                if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                    if appDelegate.appInBackground {
                            appDelegate.showLocalNotificationCharges(title: "Low card balance - \(price)", subTitle: event.message ?? "Please top up your card")
                    }
//                    else {
//                        appDelegate.showOnAppErrorNotification(title: "Low card balance - \(price)", subTitle: event.message ?? "Please top up your card")
//                    }
                }
            default:
                break
            }
            
            if eventType == .PARKING_AVAILABILITY {
                if UserSettingsModel.sharedInstance.obuParkingAvailabilityAudioEnable {
                    playSpokenText(text: event.getParkingSpokenText())
                }
            } else if eventType == .TRAVEL_TIME {
                if UserSettingsModel.sharedInstance.obuEstimatedTravelTimeAudioEnable {
                    playSpokenText(text: event.getTravelTimeSpokenText())
                }
            } else {
                
                if needDisplayAlert {
                    let extraData: NSDictionary = ["message": analyticMessage, "location_name": "location_\(self.roadName ?? "")", "longitude": lastLocation?.coordinate.longitude ?? 0.0, "latitude": lastLocation?.coordinate.latitude ?? 0.0]
                    if appDelegate().carPlayConnected {
                        if OBUHelper.shared.getObuMode() == .device {
                            AnalyticsManager.shared.logOpenPopupEventV2CarPlay(popupName: ParameterName.CruiseMode.UserPopup.cruise_notification, screenName: ParameterName.CarPlayObuState.dhu_cruise, extraData: extraData)
                        }
                    } else {
                        if OBUHelper.shared.getObuMode() == .device {
                            AnalyticsManager.shared.logOpenPopupEventV2(popupName: ParameterName.CruiseMode.UserPopup.cruise_notification, screenName: ParameterName.CruiseMode.screen_view, extraData: extraData)
                        }
                    }
                }
                
                if needVoiceAlert {
                    playSpokenText(text: event.spokenText)
                }
            }
        }
                
        let tripId = tripLogger?.getTripGUID() ?? ""
        if !tripId.isEmpty {
            //  Send trip log travel summary and transaction history
            if let tripSummary = event.tripSummary {
                let summary = tripSummary.getTravelSummary()
                ObuDataService().sendObuTripSummary(summary, tripGUID: tripId) { result in
                    switch result {
                    case .success(_):
                        print("Send travel summary success")
                    case .failure(let error):
                        print("Send travel summary failed: \(error.localizedDescription)")
                    }
                }
            }
            
            if !event.payments.isEmpty {
                var histories: [BreezePaymentHistory] = []
                for history in event.payments {
                    let payment = history.getPaymentHistory()
                    histories.append(payment)
                }
                ObuDataService().sendObuTransactions(histories, tripGUID: tripId) { result in
                    switch result {
                    case .success(_):
                        print("Send transactions success")
                    case .failure(let error):
                        print("Send transactions failed: \(error.localizedDescription)")
                    }
                }
            }
        }
    }
    
}

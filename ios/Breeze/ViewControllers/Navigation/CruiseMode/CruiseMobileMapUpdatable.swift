//
//  CruiseMobileMapUpdatable.swift
//  Breeze
//
//  Created by Zhou Hao on 16/12/21.
//

import Foundation
import Turf
import MapboxNavigation
@_spi(Restricted) import MapboxMaps
import SnapKit
import SwiftyBeaver
import MapboxDirections
import UIKit

final class CruiseMobileMapUpdatable: MapViewUpdatable {
    
    weak var navigationMapView: NavigationMapView?
    weak var delegate: MapViewUpdatableDelegate?

    // MARK: - Constants
    private let topPadding: CGFloat = 48
    private let trailingX: CGFloat = 24
    private let iconSize: CGFloat = 48
    
    // MARK: - Private Methods
    private var speedLimitView: CustomSpeedLimitView?
    private var muteButton: ToggleButton!
    private var parkingButton: ToggleButton!
    private var roadNamePanel: RoadNamePanel?
    private var liveLocationButton: LiveLocationSharingButton! //Set it up after mute button
    private var shareDriveButton: UIButton!
    private var isTrackingUser = true
    private var lastLocation: CLLocation?
    private var notificationView: BreezeNotificationView!
        
    init(navigationMapView: NavigationMapView) {
        SwiftyBeaver.debug("CruiseMobileMapUpdatable init")
        self.navigationMapView = navigationMapView
        self.navigationMapView?.mapView.ornaments.options.attributionButton.visibility = .hidden
        setupUI()
    }
    
    deinit {
        SwiftyBeaver.debug("CruiseMobileMapUpdatable deinit")
    }

    func cruiseStatusUpdate(isCruise: Bool) {
        setCamera(at: navigationMapView?.mapView.location.latestLocation?.coordinate, isCruise: isCruise)
        if isCruise {
            cruiseStarted()
        } else {
            cruiseStopped()
        }
    }

    func updateRoadName(_ name: String) {
        self.showRoad(name)
    }
    
    func updateSpeedLimit(_ value: Double) {
        speedLimitView?.speed = value
    }
    
    func overSpeed(value: Bool) {
        speedLimitView?.updateOverSpeed(value: value)
    }
    
    // MARK: - Finding ERP's on route line
    func initERPUpdater(response:RouteResponse?){
        
        //Not needed
    }
        
    // MARK: - Private Methods
    private func setupUI() {
        setupSpeedLimitView()
        setupParkingButton()
        setupMuteButton()
        setupRoadNamePanel()
        setupShareDriveButton()
        setupLiveLocationButton()
    }
    
    private func cruiseStarted() {
        let topOffset: CGFloat = getToastOffset(isTunnel: false)
        if let mapView = self.navigationMapView?.mapView {
            showToast(from: mapView, message: Constants.toastStartCruise, topOffset: topOffset)
        }
        updateCameraLayer(show: true)
    }
    
    private func cruiseStopped() {
        removeSpeedLimitView()
        removeMuteButton()
        removeRoadNamePanel()
        removeLiveLocationButton()
        
        if let mapView = self.navigationMapView?.mapView {
            hideToast(from: mapView)
        }
        if let navMapView = self.navigationMapView {
            hideToast(from: navMapView)
        }
        
        updateCameraLayer(show: false)
    }

    // setup this after mute button
    private func setupParkingButton() {
        if self.parkingButton == nil {
//            let size = Images.toggleParkingBtn!.size
            let parkingButton = ToggleButton(frame: CGRect(x: 0, y: 0, width: 78, height: 61))
            parkingButton.onImage = Images.toggleParkingBtn
            parkingButton.offImage = Images.toggleParkingBtnOn
            
            parkingButton.onToggled = { [weak self] isSelected in
                guard let self = self else { return }
                self.delegate?.onParkingEnabled(isSelected)
                
                let event = isSelected ? ParameterName.Cruise.UserToggle.parking_icon_on : ParameterName.Cruise.UserToggle.parking_icon_off
                AnalyticsManager.shared.logClickEvent(eventValue: event, screenName: ParameterName.Cruise.screen_view)
            }
            
            navigationMapView?.mapView.addSubview(parkingButton)
            self.parkingButton = parkingButton
            
            self.parkingButton?.translatesAutoresizingMaskIntoConstraints = false
            if let navMapView = self.navigationMapView {
                self.parkingButton?.snp.makeConstraints { make in
                    make.trailing.equalToSuperview().offset(0)
                    make.top.equalTo(navMapView.snp.top).offset(topPadding)
                    make.size.equalTo(CGSize(width: 78, height: 61))
                }
            }
        }        
    }
        
    // MARK: - Speed limit view
    private func setupSpeedLimitView() {
        if speedLimitView == nil {
            speedLimitView = CustomSpeedLimitView(frame: CGRect(x: 0, y: 0, width: 70, height: 70))
            
            navigationMapView?.mapView.addSubview(speedLimitView!)
            speedLimitView?.speed = 0

            speedLimitView?.translatesAutoresizingMaskIntoConstraints = false
            
            if let navMapView = self.navigationMapView {
                speedLimitView?.snp.makeConstraints { (make) in
                    make.leading.equalToSuperview().offset(24)
                    make.top.equalTo(navMapView.snp.top).offset(topPadding)
                    make.size.equalTo(CGSize(width: 70, height: 70))
                }
            }
        }
    }
    
    private func removeSpeedLimitView() {
        guard let view = speedLimitView else {
            return
        }
        view.removeFromSuperview()
        speedLimitView = nil
    }
    
    private func setupMuteButton() {
        if self.muteButton == nil {
            let muteButton = ToggleButton(frame: CGRect(x: 0, y: 0, width: iconSize, height: iconSize))
            muteButton.isSelected = !Prefs.shared.getBoolValue(.enableAudio)
            muteButton.onImage = Images.unmuteImage
            muteButton.offImage = Images.muteImage
            self.delegate?.onMuteEnabled(muteButton.isSelected)
            
            muteButton.onToggled = { [weak self] isSelected in
                guard let self = self else { return }
                self.delegate?.onMuteEnabled(isSelected)
            }
            navigationMapView?.mapView.addSubview(muteButton)
            self.muteButton = muteButton
            
            self.muteButton?.translatesAutoresizingMaskIntoConstraints = false
            self.muteButton?.snp.makeConstraints { make in
                make.leading.equalTo(self.parkingButton.snp.leading).offset(0)
                //                make.top.equalTo(navigationMapView.snp.top).offset(topPadding)
                make.top.equalTo(self.parkingButton.snp.bottom).offset(10)
                make.size.equalTo(CGSize(width: iconSize, height: iconSize))
            }
        }
    }
    
    private func removeMuteButton() {
        guard let view = muteButton else {
            return
        }
        view.removeFromSuperview()
        muteButton = nil
    }

    private func setupRoadNamePanel() {
        
        if self.roadNamePanel == nil {
            roadNamePanel = RoadNamePanel(frame: CGRect(x: 0, y: 0, width: 170, height: 40))
            navigationMapView?.addSubview(roadNamePanel!)
            navigationMapView?.bringSubviewToFront(roadNamePanel!)
                        
            let minW = UIScreen.main.bounds.width > 400 ? 220 : 190
            roadNamePanel?.translatesAutoresizingMaskIntoConstraints = false
            if let navMapView = navigationMapView {
                roadNamePanel?.snp.makeConstraints { (make) in
                    make.centerX.equalToSuperview()
                    make.bottom.equalTo(navMapView.safeAreaLayoutGuide.snp.bottom).offset(-trailingX)
                    make.width.greaterThanOrEqualTo(170)
                    make.width.lessThanOrEqualTo(minW)
                    make.height.equalTo(40)
                }
            }
            
            updateRoadPanelConstraint(isHidden: true)
            roadNamePanel?.setNeedsLayout()
            roadNamePanel?.layoutIfNeeded()
        }
    }
    
    private func removeRoadNamePanel() {
        if let view = roadNamePanel {
            view.removeFromSuperview()
            roadNamePanel = nil
        }
    }
    
    // MARK: - Share drive button on top of the live location button
    private func setupShareDriveButton() {
        if self.shareDriveButton == nil {
            let button = UIButton(frame: CGRect(x: 0, y: 0, width: iconSize, height: iconSize))
            button.setImage(UIImage(named: "shareDriveBtnIcon"), for: .normal)
            button.addTarget(self, action: #selector(onShareDriveButtonClicked), for: .touchUpInside)
            navigationMapView?.mapView.addSubview(button)
            self.shareDriveButton = button
        }
        
        // setup auto-layout
        self.shareDriveButton.translatesAutoresizingMaskIntoConstraints = false
        self.shareDriveButton.snp.makeConstraints { make in
            make.trailing.equalToSuperview().offset(-trailingX)
            make.bottom.equalToSuperview().offset(-120)
            make.width.equalTo(iconSize)
            make.height.equalTo(iconSize)
        }
        
        self.shareDriveButton.isHidden = false
    }
    
    @objc func onShareDriveButtonClicked() {
        self.delegate?.onShareDriveEnabled(true)
    }

    // MARK: - Live location button
    private func setupLiveLocationButton() {
    
        if self.liveLocationButton == nil {
            let etaButton = LiveLocationSharingButton(frame: CGRect(x: 0, y: 0, width: 62, height: 62))
            etaButton.onImage = Images.liveLocationResumeImage
            etaButton.offImage = Images.liveLocationPauseImage
            //etaButton.backgroundColor = .white
            //etaButton.layer.cornerRadius = 31
            var isFirstTime: Bool = true
            etaButton.onToggled = { [weak self] isStarted in
                guard let self = self else { return }
            
                let name = isStarted ? ParameterName.Cruise.UserClick.eta_main_button: ParameterName.Cruise.UserClick.eta_pause
                AnalyticsManager.shared.logClickEvent(eventValue: name, screenName: ParameterName.Cruise.screen_view)
                
                // Don't show toast the first time
                if isFirstTime {
                    isFirstTime = false
                } else {
                    self.delegate?.onLiveLocationEnabled(isStarted)
                }
            }
                        
            navigationMapView?.mapView.addSubview(etaButton)
            self.liveLocationButton = etaButton

            self.liveLocationButton.translatesAutoresizingMaskIntoConstraints = false
            self.liveLocationButton.snp.makeConstraints { make in
//                make.leading.equalTo(self.parkingButton.snp.leading).offset(0)
//                make.top.equalTo(self.muteButton!.snp.bottom).offset(10)
                make.center.equalTo(self.shareDriveButton.snp.center)
                make.width.equalTo(62)
                make.height.equalTo(62)
            }
            
            //Check initially if ETA is not available, then hide the live location button
            self.liveLocationButton.isHidden = true
        }
    }
    
    private func removeLiveLocationButton() {
        guard let view = liveLocationButton else {
            return
        }
        view.removeFromSuperview()
        liveLocationButton = nil
    }
    
    private func updateRoadPanelConstraint(isHidden: Bool) {
        if let navMapView = self.navigationMapView {
            roadNamePanel?.snp.updateConstraints { (make) in
                make.bottom.equalTo(navMapView.safeAreaLayoutGuide.snp.bottom).offset(isHidden ? 80 : -trailingX)
            }
        }
    }
    
    private func showRoad(_ name: String) {
                
        guard let roadNamePanel = self.roadNamePanel else { return }
        
        let hide = name.isEmpty
        let needUpdate = roadNamePanel.text() != name
        
        roadNamePanel.set(name)
        
        // animation
        if needUpdate {
            UIView.animate(withDuration: Values.standardAnimationDuration) {
                [weak self] in
                guard let self = self else { return }
                self.updateRoadPanelConstraint(isHidden: hide)
            }
        }
    }
    
    func updateParkingAvailability(carpark: Carpark?) {
        
        //Not required
    }
    
    func updateNotificationWhenUserSoonArrive(carpark: Carpark?, message: String) {
        
        //Not required
    }
        
    // MARK: - Notification
    // featureId - Could be current feature id if currentFeature is not nil
    // or previous featureId if currentFeature is nil
    func updateNotification(featureId: String?, currentFeature: FeatureDetectable?) {
        
        if let feature = currentFeature {
            if notificationView != nil {
                notificationView.dismiss()
                notificationView = nil
            }
            notificationView = BreezeNotificationView(leading: 22, trailing: 26)
            if let navMapView = self.navigationMapView {
                FeatureNotification.showNotification(notificationView: notificationView, inView: navMapView, feature: feature, yOffset: topPadding,onCompletion: { [weak self] in
                    guard let self = self else { return }
                    self.updateIconsLayout()
                },  onDismiss: { [weak self] in
                    guard let self = self else { return }
                    
                    self.notificationView = nil
                    self.updateIconsLayout()
                })
            }
        }
    }
    
    // MARK: - Tunnel
    func updateTunnel(isInTunnel: Bool) {
        if isInTunnel {
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                let topOffset: CGFloat = self.getToastOffset(isTunnel: true)
                if let navMapView = self.navigationMapView {
                    showToast(from: navMapView, message: Constants.toastInTunnel, topOffset: topOffset, icon: "noInternetIcon", duration: 0)
                }
            }
        } else {
            DispatchQueue.main.async { [weak self] in
                if let navMapView = self?.navigationMapView {
                    hideToast(from: navMapView)
                }
            }
        }
    }
    
    // MARK: - ETA
    func updateETA(eta: TripCruiseETA?) {
        if let tripETA = eta {
            
            if tripETA.shareLiveLocation.uppercased() == "Y" {
                self.liveLocationButton.isHidden = false
                self.liveLocationButton.resume()
            } else {
                self.liveLocationButton.pause()
                self.liveLocationButton.isHidden = true
            }

        } else {
            self.liveLocationButton.pause()
            self.liveLocationButton.isHidden = true
        }
        
        self.shareDriveButton.isHidden = !self.liveLocationButton.isHidden
    }
    
    func updateETA(eta: TripETA?) {
        
        //Not needed
    }
    
    func shareDriveButtonEnabled(_ isEnabled: Bool) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.shareDriveButton.isHidden = !isEnabled
        }
    }

    func updateLiveLocationEnabled(_ isEnabled: Bool) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            let offset:CGFloat = self.getToastOffset(isTunnel: false)
              
            if let mapView = self.navigationMapView?.mapView {
                if isEnabled {
                    showToast(from: mapView, message: Constants.toastLiveLocationResumed, topOffset: offset, icon: "toastLiveLocationResumed", messageColor: .black, duration: 3)
                } else {
                    showToast(from: mapView, message: Constants.toastLiveLocationPaused, topOffset: offset, icon: "toastLiveLocationPaused", messageColor: .black, duration: 3)
                }
            }
            
            self.liveLocationButton.updateToggleWithOutCallBack(status: isEnabled)
            if(appDelegate().carPlayMapController != nil)
            {
                appDelegate().carPlayMapController?.updateCPCruiseToggleTitle(activity: .navigating,isEnabled:isEnabled,isLiveLoc: true)
            }
            
            
        }
    }

    func updateParkingStatus(isEnabled:Bool){        
        self.parkingButton?.toggleWithOutCallBack(status: !isEnabled)
    }

    func updateMuteStatus(isEnabled:Bool){
        
        self.muteButton?.toggleWithOutCallBack(status: !isEnabled)
        if(appDelegate().carPlayMapController != nil)
        {
            appDelegate().carPlayMapController?.updateCPCruiseToggleTitle(activity: .navigating,isEnabled:isEnabled)
        }
    }
    
    func showETAMessage(recipient: String, message: String) {
        
        if let mapView = self.navigationMapView?.mapView {
            let offset:CGFloat = getToastOffset(isTunnel: false)
            let imageWithText = UIImage.generateImageWithText(text: getAbbreviation(name: recipient), font: UIFont(name: fontFamilySFPro.Bold, size: 20.0)!)
            showToast(from: mapView, message: message, topOffset: offset, image: imageWithText, messageColor: .black, font: UIFont(name: fontFamilySFPro.Regular, size: 20.0)!, duration: 5)
        }
    }
    
    // MARK: - Private methods
    private func updateIconsLayout() {
        let top = UIApplication.topSafeAreaHeight + (notificationView != nil ? notificationView!.bounds.origin.y + notificationView!.bounds.height + 16 : topPadding)
        
        if let muteButton = self.muteButton {
            muteButton.snp.updateConstraints { make in
                //make.top.equalTo(self.navigationMapView).offset(top)
                make.top.equalTo(self.parkingButton.snp.bottom).offset(10)
            }
        }
//        if let liveLocButton = self.liveLocationButton {
//            liveLocButton.snp.updateConstraints { make in
//                make.leading.equalTo(self.parkingButton.snp.leading).offset(0)
//                make.top.equalTo(self.muteButton!.snp.bottom).offset(10)
//                make.width.equalTo(62)
//                make.height.equalTo(62)
//            }
//        }
        if let speedLimitIcon = self.speedLimitView {
            if let navMapView = self.navigationMapView {
                speedLimitIcon.snp.updateConstraints { make in
                    make.top.equalTo(navMapView).offset(top)
                }
            }
        }
    }
    
    // TODO: Consider to refactor the Toast implementation so that its position can be adjusted dynamically
    private func getToastOffset(isTunnel: Bool) -> CGFloat {
        var offset: CGFloat = 0
        if let view = notificationView {
            offset =  view.bounds.origin.y + view.bounds.height + 15
        }
        else {
            offset = UIApplication.bottomSafeAreaHeight > 0 ? 0 : 15
        }
        if isTunnel {
            offset += 60
        }

        return offset
    }
    
}

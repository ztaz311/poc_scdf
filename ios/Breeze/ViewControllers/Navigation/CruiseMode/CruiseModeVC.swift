//
//  CruiseModeVC.swift
//  Breeze
//
//  Created by Zhou Hao on 16/12/21.
//

import UIKit
import MapboxCoreNavigation
@_spi(Restricted) import MapboxMaps
import MapboxCoreMaps
import MapboxNavigation
import SwiftyBeaver
import Combine
import SnapKit
import AVFAudio

class CruiseModeVC: UIViewController {

    @IBOutlet weak var navigationMapView: NavigationMapView!
    @IBOutlet weak var carplayConnectedOverlay: UIView!
    weak var cruiseViewModel: CruiseViewModel! {
        didSet {
            cruiseViewModel.delegate = self
        }
    }
    private var disposables = Set<AnyCancellable>()
    
    var userLocationButton: UserLocationButton!
//    var feedbackButton: UIButton!
    var onNavigateHere: ((_ carpark: Carpark, _ screenshot: UIImage) -> Void)?
    var etaChildVC: UIViewController?
    
    var carparkToast: BreezeToast?
    
//    var isTrackingUser = true {
//        didSet {
//            userLocationButton.isHidden = isTrackingUser
//        }
//    }
    var trackingTimer: Timer?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        SwiftyBeaver.debug("CruiseModeVC viewDidLoad")
        self.carplayConnectedOverlay.isHidden = !appDelegate().carPlayConnected
        self.view.bringSubviewToFront(self.carplayConnectedOverlay)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(applicationWillEnterForeground(_:)),
            name: UIApplication.willEnterForegroundNotification,
            object: nil)
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(applicationDidEnterBackground(_:)),
            name: UIApplication.didEnterBackgroundNotification,
            object: nil)
        NotificationCenter.default.publisher(for: Notification.Name("onNavigationToCarpark"), object: nil)
            .receive(on: DispatchQueue.main)
            .sink { [weak self] notification in
                guard let self = self else { return }
                if let carpark = notification.userInfo?["carpark"] as? Carpark {
                    for disposable in self.disposables {
                        disposable.cancel()
                    }
                    if let image = UIApplication.shared.keyWindow?.capture() {
                        self.onNavigateHere?(carpark, image)
                    }
                    
                    self.endCruise(byCarparkSelection: true)
                }
            }.store(in: &disposables)
        NotificationCenter.default.publisher(for: Notification.Name(Values.NotificationCloseNotifyArrivalScreen), object: nil)
            .receive(on: DispatchQueue.main)
            .sink { [weak self] notification in
                guard let self = self, let vc = self.etaChildVC else { return }
                self.removeChildViewController(vc)
                self.etaChildVC = nil
            }.store(in: &disposables)

//        NotificationCenter.default.publisher(for: Notification.Name(Values.NotificationETASetUp), object: nil)
//            .receive(on: DispatchQueue.main)
//            .sink { [weak self] notification in
//                guard let self = self else { return }
//                self.receiveETA(notification)
//            }.store(in: &disposables)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(updateCarplayConnectionStatus(_:)),
            name: .carplayConnectionDidChangeStatus,
            object: nil)

        navigationMapView.mapView.mapboxMap.onNext(event: .mapLoaded) { [weak self] _ in
            guard let self = self, let viewmodel = self.cruiseViewModel else { return }
            // check the tracking status
            viewmodel.$isTrackingUser
                .receive(on: DispatchQueue.main)
                .sink(receiveValue: { [weak self] isTracking in
                    guard let self = self else { return }
                    self.userLocationButton.isHidden = isTracking
                    if !isTracking {
                        self.startTrackingTimer()
                    } else {
                        self.stopTrackingTimer()
                    }
                }).store(in: &self.disposables)

            viewmodel.mobileMapViewUpdatable = CruiseMobileMapUpdatable(navigationMapView: self.navigationMapView)
            // update the eta action here
            viewmodel.notifyArrivalAction = { [weak self] in
                guard let self = self else { return }
                self.etaChildVC = showShareDriveScreen(viewController: self, address: "", etaTime: "", fromScreen: "cruise")
            }
            viewmodel.startLocationUpdate()
        }
        navigationMapView.mapView.mapboxMap.onEvery(event: .cameraChanged) { [weak self] _ in
            guard let self = self, let viewmodel = self.cruiseViewModel else { return }
            viewmodel.updateTooltipsPosition(naviMapView: self.navigationMapView)
        }
                
//        setupFeedbackButton()
        setupLocationButton()
        navigationMapView.mapView.gestures.delegate = self
        navigationMapView.mapView.gestures.options.pitchEnabled = false
        navigationMapView.mapView.ornaments.options.attributionButton.visibility = .hidden
        
        appDelegate().$cruiseViewModel
            .receive(on: DispatchQueue.main)
            .sink { [weak self] viewModel in
                guard let self = self else { return }
                
                if viewModel == nil {
                    self.endCruise()
                }
                
        }.store(in: &self.disposables)        
        
        AnalyticsManager.shared.logOpenPopupEventV2(popupName: ParameterName.CruiseMode.UserPopup.projected_to_dhu, screenName: ParameterName.CruiseMode.screen_view)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupDarkLightAppearance()
        toggleDarkLightMode()
    }
    
    deinit {
        SwiftyBeaver.debug("CruiseModeVC deinit")
        NotificationCenter.default.removeObserver(self, name: UIApplication.willEnterForegroundNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIApplication.didEnterBackgroundNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: .carplayConnectionDidChangeStatus, object: nil)
    }
    
    @objc func applicationWillEnterForeground(_ notification: NSNotification) {
        // Only start when this view is visible
        if navigationController?.visibleViewController == self {
            if let vm = cruiseViewModel {
                //vm.startLocationUpdate()
            }
        }
    }
    
    @objc func applicationDidEnterBackground(_ notification: NSNotification) {
        if let vm = cruiseViewModel {
            //vm.stopLocationUpdate()
        }
    }
    
    @objc func updateCarplayConnectionStatus(_ notification: NSNotification) {
        if let status = notification.object as? Bool {
            self.carplayConnectedOverlay.isHidden = !status
            if status == false {
                self.onEnd("")
            }
        }
    }
    
    
    
    @objc func recenterPuck() {
        resetCamera()
    }
    
    private func startTrackingTimer() {
        stopTrackingTimer()
        self.trackingTimer = Timer.scheduledTimer(withTimeInterval: 10, repeats: false, block: { _ in
            self.recenterPuck()
        })
    }
    
    private func stopTrackingTimer() {
        self.trackingTimer?.invalidate()
        self.trackingTimer = nil
    }
        
    //MARK: - ETA SetUp
    @objc func receiveETA(_ notification: Notification) {
        
        if let dictionary = notification.userInfo {
            let name = dictionary[Values.etaContactName] as! String
            let number = dictionary[Values.etaPhoneNumber] as! String
            var message = dictionary[Values.etaMessage] as! String
            let favoriteId = dictionary[Values.etaFavoriteId] as! Int
            DispatchQueue.main.async { [weak self] in
                guard let self = self, let vm = self.cruiseViewModel else { return }

                // 1. Update ETATrigger.shared.tripCruiseETA by converting ETAInfo to TripCruiseETA
                // TODO: May need refactoring by not using singleton and use standard ETA object for  cruise/routeplaning/navigation
                
                message = "\(AWSAuth.sharedInstance.userName) is sharing real-time location to you on Breeze"
                ETATrigger.shared.tripCruiseETA = TripCruiseETA(type: TripETA.ETAType.Init, message: message, recipientName: name, recipientNumber: number, shareLiveLocation: "Y", tripStartTime: Int(Date().timeIntervalSince1970), tripDestLat: 0, tripDestLong: 0, destination: "", tripEtaFavouriteId: favoriteId)
                // 2. Setup ETA
                vm.hideShareDriveButton(false)
                vm.setupETA()
            }
        }
    }

    // MARK: Feedback button related
//    private func setupFeedbackButton() {
//        self.feedbackButton = NewFeedbackButton(buttonSize: 48)
//        feedbackButton.addTarget(self, action: #selector(feedbackIssue), for: .touchUpInside)
//        feedbackButton.translatesAutoresizingMaskIntoConstraints = false
//        navigationMapView.mapView.addSubview(feedbackButton)
//
//        feedbackButton.snp.makeConstraints { make in
//            make.trailing.equalToSuperview().offset(-24)
//            make.bottom.equalToSuperview().offset(-180)
//            make.height.equalTo(48)
//            make.width.equalTo(48)
//        }
//
//    }
    
//    @objc func feedbackIssue(){
//        
//        let storyboard: UIStoryboard = UIStoryboard(name: "Feedback", bundle: nil)
//        let vc: FeedbackVC = storyboard.instantiateViewController(withIdentifier: "FeedbackVC")  as! FeedbackVC
//        vc.modalPresentationStyle = .formSheet
//        vc.locCoordinate = self.navigationMapView.mapView.location.latestLocation?.coordinate
//        vc.locationTrackManager = cruiseViewModel.locationTrackManager
//        vc.feedBack = NavigationEventsManager(passiveNavigationDataSource: cruiseViewModel.locationTrackManager.passiveLocationDataSource).createFeedback()
//        self.present(vc, animated: true, completion: nil)
//    }
    
    // MARK: Location button related
    // Button creation and autolayout setup
    private func setupLocationButton() {
        userLocationButton = UserLocationButton(buttonSize: 48)
        userLocationButton.addTarget(self, action: #selector(locationButtonTapped), for: .touchUpInside)
        navigationMapView.mapView.addSubview(userLocationButton)
        userLocationButton.isHidden = true // hide it by default
        
        userLocationButton.translatesAutoresizingMaskIntoConstraints = false
        
        userLocationButton.snp.makeConstraints { make in
            make.trailing.equalToSuperview().offset(-24)
//            make.bottom.equalTo(feedbackButton.snp.top).offset(-10)
            make.bottom.equalToSuperview().offset(-180)
            make.height.equalTo(48)
            make.width.equalTo(48)
        }
    }
    
    @objc func locationButtonTapped(sender: UserLocationButton) {
        resetCamera()
    }
    
    func resetCamera() {
        
//        isTrackingUser = true
        if let cruiseViewModel = cruiseViewModel {
            cruiseViewModel.recenter()
        }
    }
    
    private func endCruise(byCarparkSelection: Bool = false){
        
        // don't trigger the call back to prevent from crashing
        navigationMapView.mapView.gestures.delegate = nil
        
        if(cruiseViewModel != nil){
            
            self.cruiseViewModel.stopLocationUpdate()
            self.cruiseViewModel.endCruiseStatus()
            
            appDelegate().enterHome()
        }
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Cruise.UserClick.end_cruise, screenName: ParameterName.Cruise.screen_view)
        self.navigationController?.popViewController(animated: !byCarparkSelection)
    }
    
    @IBAction func onEnd(_ sender: Any) {
        
        for disposable in disposables {
            disposable.cancel()
        }
        _ = AVAudioSession.sharedInstance().setAudioSessionActive()
        self.endCruise()
    }
    
    // MARK: - Theme updated
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        guard previousTraitCollection?.userInterfaceStyle != traitCollection.userInterfaceStyle else {
            return
        }
        toggleDarkLightMode()
    }
    
    private func toggleDarkLightMode() {
        #if TESTING
        navigationMapView.mapView.mapboxMap.style.uri = StyleURI(url: URL(string: Constants.Map.testURL)!)
        #else
        navigationMapView.mapView.mapboxMap.style.uri = StyleURI(url: URL(string: isDarkMode ? Constants.Map.nightStyleUrl: Constants.Map.styleUrl)!)
        #endif
    }
}

extension CruiseModeVC: CruiseViewModelDelegate {
    func showNoCarpark(_ model: CruiseViewModel) {
        if self.carparkToast == nil {
            self.carparkToast = BreezeToast(appearance: BreezeToastStyle.carparkToast, onClose: { [weak self] in
                guard let self = self else { return }
                self.carparkToast = nil
            }, actionTitle: "", onAction: nil)

            self.carparkToast?.show(in: self.navigationMapView, title: "No Carpark Nearby", message: Constants.toastNearestCarparkCruise, animated: true)
            self.carparkToast?.yOffset = getTopOffset()
        }
    }
    
    func hideNoCarpark(_ model: CruiseViewModel){
        
        if self.carparkToast != nil {
            
            self.carparkToast?.removeFromSuperview()
        }
    }
    
    func getBearing() -> String {
        if let bearing = navigationMapView?.mapView.cameraState.bearing {
            return "\(bearing)"
        }
        return ""
    }
}

//  MARK: - GestureManagerDelegate methods
extension CruiseModeVC: GestureManagerDelegate {
   
    func gestureManager(_ gestureManager: GestureManager, didEnd gestureType: GestureType, willAnimate: Bool) {
        
        //We will use this if there is an action neede for map touch
    }
    
    func gestureManager(_ gestureManager: GestureManager, didEndAnimatingFor gestureType: GestureType) {
        
        //We will use this if there is an action neede for map touch
    }
    
    func gestureManager(_ gestureManager: GestureManager, didBegin gestureType: GestureType) {
        
        var name = ParameterName.Home.MapInteraction.drag
        switch gestureType {
        case .pinch:
            name = ParameterName.Home.MapInteraction.pinch
        // rc4 - remove rotate
//        case .rotate:
//            name = ParameterName.Home.MapInteraction.rotate
        case .singleTap:
            let point = gestureManager.singleTapGestureRecognizer.location(in: navigationMapView)
            cruiseViewModel.queryAmenitiesLayer(point: point)
        default:
            name = ParameterName.Home.MapInteraction.drag
        }
        
        if gestureType == .pan || gestureType == .pinch || gestureType == .doubleTapToZoomIn || gestureType == .doubleTouchToZoomOut {

//            isTrackingUser = false
            cruiseViewModel.noTracking()
        }
        
        if gestureType == .singleTap || gestureType == .pinch || gestureType == .doubleTapToZoomIn || gestureType == .doubleTouchToZoomOut {
            cruiseViewModel.dismissTooltips()
        }
    }
}

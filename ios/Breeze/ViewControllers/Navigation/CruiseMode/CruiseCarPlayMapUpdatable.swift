//
//  CruiseCarPlayMapUpdatable.swift
//  Breeze
//
//  Created by Zhou Hao on 16/12/21.
//

import Foundation
import Turf
import MapboxNavigation
@_spi(Restricted) import MapboxMaps
import SnapKit
import SwiftyBeaver
import MapboxDirections
import CarPlay

final class CruiseCarPlayMapUpdatable: MapViewUpdatable {
    
    weak var navigationMapView: NavigationMapView?
    weak var delegate: MapViewUpdatableDelegate?
    private var cpRoadNamePanel: RoadNamePanel?
    
    // for notification
    private var cpFeatureImage:UIImage!
    private var cpFeatureNotificationAlert:CPNavigationAlert?
    private var cpFeatureTitleVariants = [String]()
    private var cpFeatureSubTitleVariants = [String]()
    var cpFeatureDisplay:FeatureDetectable?
    var cpFeatureDisplayClose = false
    var didShowToastCruiseStart: Bool = false
    
    init(navigationMapView: NavigationMapView) {
        self.navigationMapView = navigationMapView
        self.navigationMapView?.mapView.ornaments.options.attributionButton.visibility = .hidden
        setInitialMapCamera()
    }
    
    deinit {
        cruiseStatusUpdate(isCruise: false)
        removeCarParks([])
    }
    
    func cruiseStatusUpdate(isCruise: Bool) {
        setCamera(at: navigationMapView?.mapView.location.latestLocation?.coordinate, isCruise: isCruise, isCarPlay: true)
        updateCameraLayer(show: isCruise)
        if isCruise {
            cruiseStarted()
        } else {
            cruiseStopped()
        }
    }
    
    func updateRoadName(_ name: String) {
        guard let roadNamePanel = self.cpRoadNamePanel else { return }
        
        let hide = name.isEmpty
        let needUpdate = roadNamePanel.text() != name
        
        roadNamePanel.set(name)
        // animation
        if needUpdate {
            UIView.animate(withDuration: Values.standardAnimationDuration) {
                [weak self] in
                guard let self = self else { return }
                self.updateRoadPanelConstraint(isHidden: hide)
            }
        }
    }
    
    func overSpeed(value: Bool) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        
        appDelegate.cruiseOverSpeed(value: value)
    }
    
    // MARK: OBU FEATURE    
    func displayOBUNotificationTravelTime(_ listModel: [TravelTime]) {
        
        let action = CPAlertAction(title: "See More",
                                   style: .default,
                                   handler: {_ in
            //            self.cpFeatureDisplayClose = true
            //            self.cpFeatureNotificationAlert = nil
            appDelegate().showTravelTimeList(listModel)
            if OBUHelper.shared.getObuMode() == .device {
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CarPlayObuState.UserClick.dhu_popup_cruise_notification_click_notification, screenName: ParameterName.CarPlayObuState.dhu_cruise)
            }
        })
        
        if let first = listModel.first {
            
            let icon = UIImage(named: first.getIconName())
            
            cpFeatureNotificationAlert = CPNavigationAlert(titleVariants: ["Travel Time \nAlert"], subtitleVariants: [""], image: icon, primaryAction: action, secondaryAction: nil, duration: 0)
            getRootMapTemplate()?.present(navigationAlert: cpFeatureNotificationAlert!, animated: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + Constants.timeDismiss) {
//                appDelegate().tryToDismissTemplateIfNeed()
                let mapTemplate = self.getRootMapTemplate()
                if(mapTemplate != nil) {
                    if(self.cpFeatureNotificationAlert != nil)
                    {
                        mapTemplate?.dismissNavigationAlert(animated: true) { dismissed in
                            self.cpFeatureNotificationAlert = nil
                        }
                    }
                    
                }
                if OBUHelper.shared.getObuMode() == .device {
                    AnalyticsManager.shared.logClosePopupEventV2Carplay(popupName: ParameterName.ObuPairing.UserPopup.dhu_popup_close, screenName: ParameterName.CarPlayObuState.dhu_cruise)
                }
            }
        }
       
    }
    
    func displayOBUNotificationCarPark(_ listModel: [Parking], item: ParkingInfoItem?){
        
        if !appDelegate().carPlayConnected {
            return
        }
        
        let action = CPAlertAction(title: "See More",
                                   style: .default,
                                   handler: {_ in
            appDelegate().showCarparkList(listModel)
            if OBUHelper.shared.getObuMode() == .device {
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CarPlayObuState.UserClick.dhu_popup_cruise_notification_click_notification, screenName: ParameterName.CarPlayObuState.dhu_cruise)
            }
        })
        
        if let parking = item {
            
            let subTitle = parking.getSubTitle()
            let icon = UIImage(named: parking.getIconNameCarplay())
            
            cpFeatureNotificationAlert = CPNavigationAlert(titleVariants: ["Parking \nUpdate"], subtitleVariants: [subTitle], image: icon, primaryAction: action, secondaryAction: nil, duration: 0)
            getRootMapTemplate()?.present(navigationAlert: cpFeatureNotificationAlert!, animated: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + Constants.timeDismiss) {
                let mapTemplate = self.getRootMapTemplate()
                if(mapTemplate != nil) {
                    if(self.cpFeatureNotificationAlert != nil)
                    {
                        mapTemplate?.dismissNavigationAlert(animated: true) { dismissed in
                            self.cpFeatureNotificationAlert = nil
                        }
                    }
                    
                }
                if OBUHelper.shared.getObuMode() == .device {
                    AnalyticsManager.shared.logClosePopupEventV2Carplay(popupName: ParameterName.ObuPairing.UserPopup.dhu_popup_close, screenName: ParameterName.CarPlayObuState.dhu_cruise)
                }
            }
        }
        
//        if let first = listModel.first {
//            let subTitle = first.getSubTitle()
//            let icon = UIImage(named: first.getIconName())
//
//            cpFeatureNotificationAlert = CPNavigationAlert(titleVariants: ["Parking \nUpdate"], subtitleVariants: [subTitle], image: icon, primaryAction: action, secondaryAction: nil, duration: 0)
//            getRootMapTemplate()?.present(navigationAlert: cpFeatureNotificationAlert!, animated: true)
//            DispatchQueue.main.asyncAfter(deadline: .now() + Constants.timeDismiss) {
//                let mapTemplate = self.getRootMapTemplate()
//                if(self.cpFeatureNotificationAlert != nil) {
//                    mapTemplate?.dismissNavigationAlert(animated: true) { dismissed in
//                        self.cpFeatureNotificationAlert = nil
//                    }
//                }
//                if OBUHelper.shared.getObuMode() == .device {
//                    AnalyticsManager.shared.logClosePopupEventV2Carplay(popupName: ParameterName.ObuPairing.UserPopup.dhu_popup_close, screenName: ParameterName.CarPlayObuState.dhu_cruise)
//                }
//            }
//        }
    }
    
    func showsCPNotificationObuERP(_ charging: BreezeObuChargingInfo? = nil, eventDic: [String: Any]? = nil){
        
        if !appDelegate().carPlayConnected {
            return
        }
        
        let action = CPAlertAction(title: "Close",
                                   style: .default,
                                   handler: {_ in
            self.cpFeatureDisplayClose = true
            self.cpFeatureNotificationAlert = nil
            if OBUHelper.shared.getObuMode() == .device {
                
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CarPlayObuState.UserClick.dhu_pop_up_cruise_notification_close, screenName: ParameterName.CarPlayObuState.dhu_cruise)
            }
        })
        
        var title = ""
        var subTitle = ""
        var icon = UIImage(named: "cpERPIcon")
        if let notiDic = eventDic {
            if let type = eventDic?["type"] as? String {
                title = notiDic["detailMessage"] as? String ?? ""
                subTitle = notiDic["message"] as? String ?? ""
                if type == "ERP_FAILURE" {
                    icon = UIImage(named: "erpfailed-icon-carplay")
                }
            }
            
        }else if let first = charging {
            
            title = first.content1 ?? ""
            subTitle = first.getMessage()
        }
        
        let title1 = formatTitleString(from: title)
        
        if !title.isEmpty || !subTitle.isEmpty {
            cpFeatureNotificationAlert = CPNavigationAlert(titleVariants: [title1], subtitleVariants: [subTitle], image: icon, primaryAction: action, secondaryAction: nil, duration: 0)
            getRootMapTemplate()?.present(navigationAlert: cpFeatureNotificationAlert!, animated: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + Constants.timeDismiss) {
    //                appDelegate().tryToDismissTemplateIfNeed()
                let mapTemplate = self.getRootMapTemplate()
                if(mapTemplate != nil) {
                    if(self.cpFeatureNotificationAlert != nil)
                    {
                        mapTemplate?.dismissNavigationAlert(animated: true) { dismissed in
                            self.cpFeatureNotificationAlert = nil
                        }
                    }
                    
                }
                if OBUHelper.shared.getObuMode() == .device {
                    
                    AnalyticsManager.shared.logClosePopupEventV2Carplay(popupName: ParameterName.ObuPairing.UserPopup.dhu_popup_close, screenName: ParameterName.CarPlayObuState.dhu_cruise)
                }
            }
        }
    }
    
    
    
    func showsCPNotificationSchoolZoneTraffic(title:String,subTitle:String,icon:UIImage?,timeDismiss:Int = Int(5.0)){
        
        if !appDelegate().carPlayConnected {
            return
        }
        
        let action = CPAlertAction(title: "Close",
                                   style: .default,
                                   handler: {_ in
            self.cpFeatureDisplayClose = true
            self.cpFeatureNotificationAlert = nil
            if OBUHelper.shared.getObuMode() == .device {
                
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CarPlayObuState.UserClick.dhu_pop_up_cruise_notification_close, screenName: ParameterName.CarPlayObuState.dhu_cruise)
            }
        })
        
        let title1 = formatTitleString(from: title)
        
        cpFeatureNotificationAlert = CPNavigationAlert(titleVariants: [title1], subtitleVariants: [subTitle], image: icon, primaryAction: action, secondaryAction: nil, duration: 0)
        getRootMapTemplate()?.present(navigationAlert: cpFeatureNotificationAlert!, animated: true)
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(timeDismiss)) {
//            appDelegate().tryToDismissTemplateIfNeed()
            let mapTemplate = self.getRootMapTemplate()
            if(mapTemplate != nil) {
                if(self.cpFeatureNotificationAlert != nil)
                {
                    mapTemplate?.dismissNavigationAlert(animated: true) { dismissed in
                        self.cpFeatureNotificationAlert = nil
                    }
                }
                
            }
            if OBUHelper.shared.getObuMode() == .device {
                
                AnalyticsManager.shared.logClosePopupEventV2Carplay(popupName: ParameterName.ObuPairing.UserPopup.dhu_popup_close, screenName: ParameterName.CarPlayObuState.dhu_cruise)
            }
        }
    }
    
    func showsCPNotificationObuTraffic(title:String,subTitle:String){
        
        if !appDelegate().carPlayConnected {
            return
        }
        
        let action = CPAlertAction(title: "Close",
                                   style: .default,
                                   handler: {_ in
            self.cpFeatureDisplayClose = true
            self.cpFeatureNotificationAlert = nil
            if OBUHelper.shared.getObuMode() == .device {
                
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CarPlayObuState.UserClick.dhu_pop_up_cruise_notification_close, screenName: ParameterName.CarPlayObuState.dhu_cruise)
            }
        })
        let icon = UIImage(named: "cpAccidentIcon")
        
        let title1 = formatTitleString(from: title)
        
        cpFeatureNotificationAlert = CPNavigationAlert(titleVariants: [title1], subtitleVariants: [subTitle], image: icon, primaryAction: action, secondaryAction: nil, duration: 0)
        getRootMapTemplate()?.present(navigationAlert: cpFeatureNotificationAlert!, animated: true)
        DispatchQueue.main.asyncAfter(deadline: .now() + Constants.timeDismiss) {
            let mapTemplate = self.getRootMapTemplate()
            if(mapTemplate != nil) {
                if(self.cpFeatureNotificationAlert != nil)
                {
                    mapTemplate?.dismissNavigationAlert(animated: true) { dismissed in
                        self.cpFeatureNotificationAlert = nil
                    }
                }
                
            }
            if OBUHelper.shared.getObuMode() == .device {
                
                AnalyticsManager.shared.logClosePopupEventV2Carplay(popupName: ParameterName.ObuPairing.UserPopup.dhu_popup_close, screenName: ParameterName.CarPlayObuState.dhu_cruise)
            }
        }
    }
    
    
    
    func updateSpeedLimit(_ value: Double) {
        
        //Not required at the moment
    }
    
    func updateParkingAvailability(carpark: Carpark?) {
        
        //Not required
    }
    
    func updateNotificationWhenUserSoonArrive(carpark: Carpark?, message: String) {
        
        //Not required
    }
    
    func updateNotification(featureId: String?, currentFeature: FeatureDetectable?) {
        if let feature = currentFeature {
            
            if let cpFeatureDisplay = self.cpFeatureDisplay {
                
                //If already shown feature is equal to current incomingfeature and user hasn't closed the notification
                if(cpFeatureDisplay == feature && self.cpFeatureDisplayClose == false){
                    
                    showFirstFeature(feature)
                    showCPNotification(hide: false)
                }
                else{
                    
                    if(cpFeatureDisplay != feature){ //If previous feature not equal to current
                        
                        self.cpFeatureDisplay = feature
                        showFirstFeature(feature)
                        showCPNotification(hide: false)
                        
                    }
                }
            }
            else{
                self.cpFeatureDisplay = feature
                showFirstFeature(feature)
                showCPNotification(hide: false)
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + Values.notificationDuration) { [weak self] in
                guard let self = self else { return }
                self.showCPNotification(hide: true)
            }
            //        } else {
            //            showCPNotification(hide: true)
        }
    }
    
    // MARK: - Tunnel
    func updateTunnel(isInTunnel: Bool) {
        if isInTunnel {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) { [weak self] in
                if let navMapView = self?.navigationMapView {
                    showToast(from: navMapView, message: Constants.toastInTunnel, topOffset: 15, icon: "noInternetIcon", duration: 0)
                }
            }
        } else {
            DispatchQueue.main.async { [weak self] in
                if let navMapView = self?.navigationMapView {
                    hideToast(from: navMapView)
                }
            }
        }
    }
    
    // MARK: - ETA
    func updateETA(eta: TripCruiseETA?) {
        // Carplay update top menu button
        if appDelegate().carPlayMapController != nil {
            appDelegate().carPlayMapController?.updateCPCruiseToggleTitle(activity: .navigating, isEnabled:true, isLiveLoc: true)
        }        
    }
    
    func updateETA(eta: TripETA?) {
        
        //Not needed
    }
    
    func shareDriveButtonEnabled(_ isEnabled: Bool) {
        //Not required
    }
    
    func updateLiveLocationEnabled(_ isEnabled: Bool) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) { [weak self] in
            
            if let mapView = self?.navigationMapView?.mapView {
                if isEnabled {
                    showToast(from: mapView, message: Constants.toastLiveLocationResumed, topOffset: 5, icon: "toastLiveLocationResumed", messageColor: .black, duration: 3)
                } else {
                    showToast(from: mapView, message: Constants.toastLiveLocationPaused, topOffset: 5, icon: "toastLiveLocationPaused", messageColor: .black, duration: 3)
                }
            }
        }
    }
    
    func updateMuteStatus(isEnabled:Bool){
        
        if(appDelegate().carPlayMapController != nil)
        {
            appDelegate().carPlayMapController?.updateCPCruiseToggleTitle(activity: .navigating,isEnabled:isEnabled)
        }
    }
    
    func updateParkingStatus(isEnabled:Bool){
        
        //        if(appDelegate().carPlayMapController != nil) {
        //            appDelegate().carPlayMapController?.updateCPCruiseToggleTitle(activity: .navigating,isEnabled:isEnabled)
        //        }
    }
    
    func showETAMessage(recipient: String, message: String) {
        let imageWithText = UIImage.generateImageWithText(text: getAbbreviation(name: recipient), font: UIFont(name: fontFamilySFPro.Bold, size: 20.0)!)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) { [weak self] in
            if let mapView = self?.navigationMapView?.mapView {
                showToast(from: mapView, message: message, topOffset: 5, image: imageWithText, messageColor: .black, font: UIFont(name: fontFamilySFPro.Regular, size: 20.0)!, duration: 5)
            }
        }
    }
    
    func showNotifyToastMessage(message:String){
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) { [weak self] in
            if let mapView = self?.navigationMapView?.mapView {
                showToast(from: mapView, message: message, topOffset: 5, icon: "toastLiveLocationPaused", messageColor: .black, duration: 3)
            }
        }
        
    }
    
    // MARK: - Finding ERP's on route line
    func initERPUpdater(response:RouteResponse?){
        
        //Not needed
    }
    
    // MARK: - Private methods
    private func cruiseStarted() {
        setupRoadNamePanel()
        if !didShowToastCruiseStart {
            didShowToastCruiseStart = true
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) { [weak self] in
                if let mapView = self?.navigationMapView?.mapView {
                    showToast(from: mapView, message: Constants.toastStartCruise, topOffset: 5, fromCarPlay: false)
                }
            }
        }
    }
    
    private func cruiseStopped() {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.removeRoadNamePanel()
            if let mapView = self.navigationMapView?.mapView {
                hideToast(from: mapView)
            }
            if let navMapView = self.navigationMapView {
                hideToast(from: navMapView)  // this is for tunnel toast
            }
        }
    }
    
    private func setupRoadNamePanel() {
        if self.cpRoadNamePanel == nil {
            let frame = appDelegate().roadNamePanelFrameCruise()
            //            roadNamePanel = RoadNamePanel(frame: CGRect(x: 0, y: 0, width: 170, height: 30))
            cpRoadNamePanel = RoadNamePanel(frame: frame)
            navigationMapView?.addSubview(cpRoadNamePanel!)
            navigationMapView?.bringSubviewToFront(cpRoadNamePanel!)
            let minW = UIScreen.main.bounds.width > 400 ? 220 : 190
            
            if let navMapView = self.navigationMapView {
                cpRoadNamePanel?.translatesAutoresizingMaskIntoConstraints = false
                cpRoadNamePanel?.snp.makeConstraints { (make) in
                    make.centerX.equalToSuperview()
                    make.bottom.equalTo(navMapView.snp.bottom).offset(-10)
                    make.width.greaterThanOrEqualTo(170)
                    make.width.lessThanOrEqualTo(minW)
                    make.height.equalTo(34)
    //                make.leading.equalToSuperview().offset(60)
                }
                cpRoadNamePanel?.label.font = UIFont.carPlayRoadNameBoldFont()
            }
            
            updateRoadPanelConstraint(isHidden: true)
            cpRoadNamePanel?.setNeedsLayout()
            cpRoadNamePanel?.layoutIfNeeded()
        }
        
    }
    
    func removeRoadNamePanel() {
        if cpRoadNamePanel != nil {
            cpRoadNamePanel?.removeFromSuperview()
            cpRoadNamePanel = nil
        }
    }
    
    private func updateRoadPanelConstraint(isHidden: Bool) {
        if let navMapView = self.navigationMapView {
            cpRoadNamePanel?.snp.updateConstraints { (make) in
                make.bottom.equalTo(navMapView.snp.bottom).offset(isHidden ? 40 : -10)
            }
        }
    }
    
    private func showCPNotification(hide:Bool){
        
        let mapTemplate = getRootMapTemplate()
        if(mapTemplate != nil)
        {
            if(hide)
            {
                
                mapTemplate?.dismissNavigationAlert(animated: true) { dismissed in
                    self.cpFeatureNotificationAlert = nil
                }
            }
            else
            {
                
                if(cpFeatureNotificationAlert == nil)
                {
                    let action = CPAlertAction(title: Constants.cpNavAlert,
                                               style: .default,
                                               handler: { _ in
                        self.cpFeatureDisplayClose = true
                        self.cpFeatureNotificationAlert = nil
                    })
                    cpFeatureNotificationAlert = CPNavigationAlert(titleVariants: cpFeatureTitleVariants, subtitleVariants: cpFeatureSubTitleVariants, image: cpFeatureImage, primaryAction: action, secondaryAction: nil, duration: 0)
                    
                    mapTemplate?.present(navigationAlert: cpFeatureNotificationAlert!, animated: true)
                }
                else
                {
                    cpFeatureNotificationAlert?.updateTitleVariants(cpFeatureTitleVariants, subtitleVariants: cpFeatureSubTitleVariants)
                    
                }
                
            }
        }
        
        
        //}
    }
    
    private func showFirstFeature(_ feature: FeatureDetectable) {
        //let distance = Int(feature.distance)
        //let distanceText = String(format: "%d m", distance)
        let distance = FeatureNotification.getDistance(for: feature, isCarPlay: true)
        let typeText = FeatureNotification.getTypeText(for: feature,isCarPlay: true)
        
        SwiftyBeaver.debug("\(feature), distance = \(distance), type = \(typeText)")
        
        //Setting Title variants for Car Play Notification
        cpFeatureTitleVariants = []
        cpFeatureSubTitleVariants = []
        cpFeatureTitleVariants.append(typeText)
        cpFeatureSubTitleVariants.append(distance)
        //        cpFeatureSubTitleVariants.append(distanceText)
        
        cpFeatureImage = FeatureNotification.getTypeImage(feature, isCarPlay: true)
    }
    
    private func getRootMapTemplate() -> CPMapTemplate? {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate,
              let mapTemplate = appDelegate.carPlayManager.interfaceController?.rootTemplate else { return nil }
        
        return mapTemplate as? CPMapTemplate
    }
    
    
    func formatTitleString(from responseString: String) -> String {
        let components = responseString.split(separator: " ")
        if #available(iOS 16.1, *) {
            if components.count >= 2 {
                let firstComponent = String(components[0])
                let secondComponent = components.dropFirst().joined(separator: " ")
                
                return "\(firstComponent)\n\(secondComponent)"
            } else {
                return responseString
            }
        } else {
            return responseString
        }
    }
    
}

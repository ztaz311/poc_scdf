//
//  QueryRenderLayer.swift
//  Breeze
//
//  Created by VishnuKanth on 27/07/22.
//

import Foundation
import MapboxNavigation
import CoreLocation
import Combine
import RxSwift
import MapboxCoreMaps
import MapboxMaps
import SwiftyBeaver
import UIKit

protocol QueryRenderDelegate: AnyObject {
    func navigateHere(address:SearchAddresses?, carPark:Carpark?, layerCode: String?, amenityId: String?, amenityType: String?, amenityCarPark: String?)
    func toolTipMoreInfo(poi:POI?,carPark:Carpark?,screenName:String)
    func toolTipOpenAdjustZoom(location:LocationCoordinate2D,callOut:ToolTipsView)
    func selectedAmenity(id: String, isSelected: Bool)
    func poiWalkingPath(poi:POI?, trackNavigation: Bool, amenityId: String?, name: String?, canReroute: Bool?)
    func closeDropPin()
//    func onShareViaLocation(url: String)
}

enum QueryRenderFromScreen {
    case mapLanding
    case content
    case collectionDetail
    case saved_collection_shortcut
    case saved_collection_my_saved_places
    case saved_collection_custom
    case shareCollectionAndLocation
}


final class QueryRenderLayer {
    
    weak var delegate: QueryRenderDelegate?
    //var amenityCalloutView: AmenityToolTipsView?
    var callOutView: ToolTipsView?
    //var poiAmenityCalloutView: POIToolTipsView?
    var carparkUpdater:CarParkUpdater?
    private(set) var fromScreen: QueryRenderFromScreen
    private var currentSelectedCarpark: Carpark?    // callout
    {
        didSet {
            carparkUpdater?.selectCarpark(id: currentSelectedCarpark?.id)
        }
    }

    private var currentView:UIView?
    private weak var mobileNavigationMapView:NavigationMapView?
    private var disposables = Set<AnyCancellable>()
    private var previouslyTappedAmenityId: String = ""
    
    private let shareCollectionTypes = [Values.COLLECTION,
               Values.COLLECTIONHOME,
               Values.COLLECTIONWORK,
               Values.COLLECTIONNORMAL,
               Values.NEWBOOKMARKS,
               Values.SHARE_CARPARK_FULL,
               Values.SHARE_CARPARK_FILLING_UP,
               Values.SHARE_CARPARK_AVAILABLE,
               Values.SHARE_BOOKMARK_ICON,
               Values.SHARE_EV_ICON,
               Values.SHARE_PETROL_ICON,
               Values.SHARE_HOME_LOCATION,
               Values.SHARE_WORK_LOCATION,
               Values.SHARE_PETROL_BOOKMARKED,
               Values.SHARE_EV_BOOKMARKED,
               Values.SHARE_CARPARK_FULL_BOOKMARKED,
               Values.SHARE_CARPARK_FILLING_UP_BOOKMARKED,
               Values.SHARE_CARPARK_AVAILABLE_BOOKMARKED
    ]
    
    // MARK: - Public functions
    init(mobileNavigationMapView:NavigationMapView?,
         fromScreen: QueryRenderFromScreen,currentView:UIView?) {
        
        self.mobileNavigationMapView = mobileNavigationMapView
        self.currentView = currentView
        self.fromScreen = fromScreen
    }
    
    var screenName: String {
        var screenName: String = ""
        switch fromScreen {
        case .mapLanding:
            screenName = ParameterName.Home.screen_view
        case .content:
            screenName = ParameterName.CustomMap.screen_view
        case .saved_collection_shortcut:
            screenName = ParameterName.CollectionDetailStar.saved_collection_shortcut
        case .collectionDetail:
            screenName = ParameterName.CollectionDetailStar.saved_collection_shortcut
        case .saved_collection_my_saved_places:
            screenName = ParameterName.CollectionDetailStar.saved_collection_my_saved_places
        case .saved_collection_custom:
            screenName = ParameterName.CollectionDetailStar.saved_collection_custom
        case .shareCollectionAndLocation:
            screenName = ParameterName.CollectionDetailStar.saved_collection_and_location_custom
        }
        return screenName
    }
    
    var showSaveBtn:Bool {
        
        var show = false
        
        if self.screenName != ParameterName.CustomMap.screen_view /*&& self.screenName != ParameterName.CollectionDetailStar.saved_collection_shortcut && self.screenName != ParameterName.CollectionDetailStar.saved_collection_my_saved_places && self.screenName != ParameterName.CollectionDetailStar.saved_collection_custom */{
            
            show = true
        }
        
        return show
    }
    
    func queryShareCollectionMapLayer(point:CGPoint,contentType:String = "", frommap: Bool = true, completion: ((Bool, Bool) -> Void)? = nil){
        
        var layerID = [String]()
        layerID.append("_\(contentType)")
//        layerID.append("_\(Values.POI)")
//        layerID.append("_\(Values.AMENITIES)")
//        layerID.append(CarparkValues.carparkSymbolLayerClusterId)
//        layerID.append(CarparkValues.carparkSymbolLayerUnClusterId)
//        layerID.append(CarparkValues.defaultCarparkSymbolLayerIdIcon)
//        layerID.append("_\(Values.COLLECTION)")
//        layerID.append("_\(Values.COLLECTIONNORMAL)")
//        layerID.append("_\(Values.COLLECTIONWORK)")
        layerID.append("_\(Values.SHARE_DESTINATION_ICON)")
        layerID.append("_\(Values.DROPPIN)")
        for type in shareCollectionTypes {
            layerID.append("_\(type)")
        }
        
        let array = 1...5
        
        for i in array {
            
            let cpType = CarparkValues.carparkImageName + String(i)
            
            layerID.append("\(CarparkValues.carparkSymbolLayerClusterId)-\(cpType)")
            layerID.append("\(CarparkValues.carparkSymbolLayerUnClusterId)-\(cpType)")
        }
        if(layerID.count > 0){
            
            let queryOptions = RenderedQueryOptions(layerIds: layerID, filter: nil)
            //let Zone2NullOptions = RenderedQueryOptions(layerIds: ["zone2-null"], filter: nil)
            self.mobileNavigationMapView?.mapView.mapboxMap.queryRenderedFeatures(at: point, options: queryOptions) { [weak self] (result) in
                guard let self = self else { return }
                
                switch result {
                    
                case .success(let features):
                    
                    // Return the first feature at that location, then pass attributes to the alert controller.
                    if let selectedFeatureProperties = features.first?.feature.properties,
                       case let .point(point) = features.first?.feature.geometry
                    {
                            if case let .string(type) = selectedFeatureProperties["type"] {
                                if type == Values.DROPPIN {
                                    self.openDropPinToolTipView(feature: features.first!.feature, location: point.coordinates)
                                }
                                
                                if type == Values.DROPPIN {
                                    completion?(false, false)
                                } else {
                                    completion?(false, true)
                                }
                                
                            } else {
                                // If the feature is a cluster, it will have `point_count` and `cluster_id` properties. These are assigned
                                // when the cluster is created.
                                // no feature detected
                                self.delegate?.selectedAmenity(id: self.previouslyTappedAmenityId, isSelected: false)
                                self.didDeselectAmenityAnnotation()
                                completion?(true, false)
                            }
                        
                    } else {
                        // If the feature is a cluster, it will have `point_count` and `cluster_id` properties. These are assigned
                        // when the cluster is created.
                        // no feature detected
                        self.delegate?.selectedAmenity(id: self.previouslyTappedAmenityId, isSelected: false)
                        self.didDeselectAmenityAnnotation()
                        completion?(true, false)
                        
                    }
                    case .failure(_):
                        completion?(true, false)
                        break
                    }
                }
            }
    }
    
    func queryMapLayer(point:CGPoint,contentType:String = "", frommap: Bool = true, completion: ((Bool, Bool) -> Void)? = nil){
        
        var layerID = [String]()
        layerID.append("_\(contentType)")
        layerID.append("_\(Values.POI)")
        layerID.append("_\(Values.AMENITIES)")
        layerID.append(CarparkValues.carparkSymbolLayerClusterId)
        layerID.append(CarparkValues.carparkSymbolLayerUnClusterId)
        layerID.append(CarparkValues.defaultCarparkSymbolLayerIdIcon)
        layerID.append("_\(Values.COLLECTION)")
        layerID.append("_\(Values.COLLECTIONNORMAL)")
        layerID.append("_\(Values.COLLECTIONWORK)")
        layerID.append("_\(Values.COLLECTIONHOME)")
        layerID.append("_\(Values.DROPPIN)")
        
        let array = 1...5
        
        for i in array {
            
            let cpType = CarparkValues.carparkImageName + String(i)
            
            layerID.append("\(CarparkValues.carparkSymbolLayerClusterId)-\(cpType)")
            layerID.append("\(CarparkValues.carparkSymbolLayerUnClusterId)-\(cpType)")
        }
        if(layerID.count > 0){
            
            let queryOptions = RenderedQueryOptions(layerIds: layerID, filter: nil)
            //let Zone2NullOptions = RenderedQueryOptions(layerIds: ["zone2-null"], filter: nil)
            self.mobileNavigationMapView?.mapView.mapboxMap.queryRenderedFeatures(at: point, options: queryOptions) { [weak self] (result) in
                guard let self = self else { return }
                
                switch result {
                    
                case .success(let features):
                    
                    // Return the first feature at that location, then pass attributes to the alert controller.
                    if let selectedFeatureProperties = features.first?.feature.properties,
                       case let .point(point) = features.first?.feature.geometry
                    {
                            if case let .string(type) = selectedFeatureProperties["type"], case let .string(toolTipType) = selectedFeatureProperties["toolTipType"] {
                                if type == Values.DROPPIN {
                                    self.openDropPinToolTipView(feature: features.first!.feature, location: point.coordinates)
                                } else if (type == Values.POI && toolTipType != Values.toolTipType) {
                                    self.openPOIToolTipView(feature: features.first!.feature, location: point.coordinates)
                                } else {
//                                    self.openPOIToolTipView(feature: features.first!.feature, location: point.coordinates)
                                    self.openAmenityToolTipView(feature: features.first!.feature,location:point.coordinates, frommap: frommap)
                                }
                                
                                if type == Values.DROPPIN {
                                    completion?(false, false)
                                } else {
                                    completion?(false, true)
                                }
                                
                            } else if case let .string(carparkId) = selectedFeatureProperties["carparkId"]{
                                self.delegate?.selectedAmenity(id: self.previouslyTappedAmenityId, isSelected: false)
                                
                                if let carpark = self.carparkUpdater?.getDefaultCarparkFrom(id: carparkId){
                                    self.openCarparkToolTipView(carpark: carpark, tapped: true)
                                } else if let carpark = self.carparkUpdater?.getCarparkFrom(id: carparkId) {
                                    self.openCarparkToolTipView(carpark: carpark, tapped: true)
                                }
                                
                                completion?(false, true)
                                
                            } else {
                                // If the feature is a cluster, it will have `point_count` and `cluster_id` properties. These are assigned
                                // when the cluster is created.
                                // no feature detected
                                self.delegate?.selectedAmenity(id: self.previouslyTappedAmenityId, isSelected: false)
                                self.didDeselectAmenityAnnotation()
                                completion?(true, false)
                            }
                        
                    } else {
                        // If the feature is a cluster, it will have `point_count` and `cluster_id` properties. These are assigned
                        // when the cluster is created.
                        // no feature detected
                        self.delegate?.selectedAmenity(id: self.previouslyTappedAmenityId, isSelected: false)
                        self.didDeselectAmenityAnnotation()
                        completion?(true, false)
                        
                    }
                    case .failure(_):
                        completion?(true, false)
                        break
                    }
                }
            }
    }
    
    func openDropPinToolTipView(feature: Turf.Feature, location: CLLocationCoordinate2D) {
        self.didDeselectAmenityAnnotation()
        
        /*
         feature.properties = [
             "type":.string(Values.DROPPIN),
             "name":.string(model.name ?? ""),
             "address":.string(model.address ?? ""),
             "isBookmarked": .boolean(isBookmarked)
         ]
         */
        
        if let selectedFeatureProperties = feature.properties,
           case let .string(name) = selectedFeatureProperties["name"],
           case let .string(address) = selectedFeatureProperties["address"],
           case let .boolean(isBookmarked) = selectedFeatureProperties["isBookmarked"],
           case let .string(lat) = selectedFeatureProperties["lat"],
           case let .string(long) = selectedFeatureProperties["long"] {
            
            guard let navMapView = mobileNavigationMapView, let currentView = currentView else {
                return
            }
            
            var amenityId = ""
            if case let .string(ID) = selectedFeatureProperties["amenityId"] {
                amenityId = ID
            }
            
            var isSearch = false
            if case let .boolean(onSearch) = selectedFeatureProperties["isSearch"] {
                isSearch = onSearch
            }
            
            var isShareCollection = false
            if case let .boolean(isShare) = selectedFeatureProperties["isShareCollection"] {
                isShareCollection = isShare
            }
            
            var isDetailCollection = false
            if case let .boolean(isDetail) = selectedFeatureProperties["isDetailCollection"] {
                isDetailCollection = isDetail
            }
            
            var snapshotId = ""
            if case let .string(ID) = selectedFeatureProperties["bookmarkSnapshotId"] {
                snapshotId = ID
            }
            
            var bookmarkId: Int = 0
            if case let .string(ID) = selectedFeatureProperties["bookmarkId"] {
                bookmarkId = Int(ID) ?? 0
            }
            
            var userLocationLinkId: Int = 0
            if case let .string(ID) = selectedFeatureProperties["userLocationLinkId"] {
                userLocationLinkId = Int(ID) ?? 0
            }
            
            var analyticScreen: String = ""
            if case let .string(screen) = selectedFeatureProperties["analyticScreen"] {
                analyticScreen = screen
            }
            
            var address2: String = ""
            if case let .string(addr2) = selectedFeatureProperties["address2"] {
                address2 = addr2
            }
            
            var fullAddress: String = ""
            if case let .string(fullAddr) = selectedFeatureProperties["fullAddress"] {
                fullAddress = fullAddr
            }
            
            var description: String = ""
            if case let .string(desc) = selectedFeatureProperties["description"] {
                description = desc
            }
            
            var placeId: String = ""
            if case let .string(ID) = selectedFeatureProperties["placeId"] {
                placeId = ID
            }
            
            var refId: Int?
            if case let .string(ID) = selectedFeatureProperties["userLocationLinkIdRef"] {
                refId = Int(ID)
            }
            
            //  Parser CS data
            var availablePercentImageBand: String = ""
            if case let .string(imageBand) = selectedFeatureProperties["availablePercentImageBand"] {
                availablePercentImageBand = imageBand
            }
            
            var csAvailability: AvailabilityCSData?
            if case let .object(csProperties) = selectedFeatureProperties["availabilityCSData"] {
                var updateTS: Double = 0
                var title = ""
                var desc = ""
                var themeColor = ""
                var primaryDesc = ""
                var secondaryDesc = ""
                
                if case let .string(text) = csProperties["title"] {
                    title = text
                }
                if case let .string(text) = csProperties["desc"] {
                    desc = text
                }
                if case let .string(text) = csProperties["themeColor"] {
                    themeColor = text
                }
                if case let .string(text) = csProperties["primaryDesc"] {
                    primaryDesc = text
                }
                if case let .string(text) = csProperties["secondaryDesc"] {
                    secondaryDesc = text
                }
                if case let .number(timestamp) = csProperties["updateTS"] {
                    updateTS = timestamp
                }
                
                csAvailability = AvailabilityCSData(csDict: ["updateTS": updateTS, "title": title, "desc": desc, "themeColor": themeColor, "primaryDesc": primaryDesc, "secondaryDesc": secondaryDesc])
            }
            
            let model = DropPinModel(name, address: address, lat: lat, long: long, amenityId: amenityId, isSearch: isSearch, address2: address2, fullAddress: fullAddress)
            if isShareCollection {                
                NotificationCenter.default.post(name: .shareCollectionSelectAmenity, object: model)
            }
            
            let dropPinToolTip: DropPinToolTipView = DropPinToolTipView(pinModel: model, location: location, isFromDetailCollection: isDetailCollection, screen: analyticScreen, isShareCollection: isShareCollection, bookmarkId: bookmarkId, userLocationLinkId: userLocationLinkId, availablePercentImageBand: availablePercentImageBand, availabilityCSData: csAvailability)
            
            dropPinToolTip.needShowCloseButton = ((!isShareCollection && !isDetailCollection) || (isDetailCollection && !isSearch))
            dropPinToolTip.isSaved = isBookmarked
            dropPinToolTip.placeId = placeId
            dropPinToolTip.userLocationLinkIdRef = refId
            
            if let ID = Int(snapshotId), ID > 0 {
                dropPinToolTip.snapshotId = ID
            }
            
            dropPinToolTip.onSaveAction = { isSave in
                
            }
            dropPinToolTip.screenName = self.screenName
            dropPinToolTip.present(from: navMapView.mapView.frame, in: navMapView.mapView, constrainedTo: dropPinToolTip.frame, animated: true)
            dropPinToolTip.center = currentView.center
            currentView.bringSubviewToFront(dropPinToolTip)
            
            self.callOutView = dropPinToolTip
            
            self.callOutView?.adjust(to: location, in: navMapView)
            
            dropPinToolTip.onCloseAction = {[weak self] in
                self?.delegate?.closeDropPin()
                self?.didDeselectAmenityAnnotation()
            }
            
            dropPinToolTip.onNavigationHere = { [weak self] (name,address,coordinate) in
                guard let self = self else { return }
                let location = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
                getAddressFromLocation(location: location) { address in
                    let address = SearchAddresses(alias: "", address1: name, lat: "\(coordinate.latitude)", long: "\(coordinate.longitude)", address2: address, distance: "0", fullAddress: fullAddress, isCrowsourceParking: isShareCollection, placeId: placeId, userLocationLinkIdRef: refId)
                    self.delegate?.navigateHere(address: address, carPark: nil, layerCode: "", amenityId: "", amenityType: "", amenityCarPark: "")
                }
            }
            
            
            dropPinToolTip.shareViaLocation = {
                shareLocationFromNative(address1: address, address2: address2, latitude: lat, longtitude: long, code: "", is_destination: false, amenity_id: "", amenity_type: "", description: description, name: name, fullAddress: fullAddress, bookmarkId: bookmarkId, placeId: placeId, userLocationLinkIdRef: refId)
            }
            
            dropPinToolTip.inviteLocation = {
                inviteLocationFromNative(address1: address, address2: address2, latitude: lat, longtitude: long, code: "", is_destination: false, amenity_id: "", amenity_type: "", description: description, name: name, fullAddress: fullAddress, bookmarkId: bookmarkId, placeId: placeId, userLocationLinkIdRef: refId)
            }
            
            self.delegate?.toolTipOpenAdjustZoom(location: location, callOut: dropPinToolTip)
        }
    }
    
    func openPOIToolTipView(feature: Turf.Feature, location: CLLocationCoordinate2D) {
        
        if let selectedFeatureProperties = feature.properties,
           case let .string(featureInformation) = selectedFeatureProperties["name"],
           case let .string(featureAddress) = selectedFeatureProperties["address"],
           case let .string(type) = selectedFeatureProperties["type"],
           case let .string(id) = selectedFeatureProperties["id"]
        {
            guard let mobileNavigationMapView = mobileNavigationMapView, let currentView = currentView else {
                return
            }

            //TagAmenityOnMap(amenityName: type, isNavigate: false)
            
            let amenityID = id
            let shouldReturn = amenityID == self.previouslyTappedAmenityId
            self.didDeselectAmenityAnnotation()
            if shouldReturn {
                return
            }
            
            // MARK: BREEZES-7641 Track click on POI
            var layerCode = ""
            if case let .string(code) = selectedFeatureProperties["layerCode"] {
                layerCode = code
            }
            
            var textAdditionalInfo = ""
            if case let .string(text) = selectedFeatureProperties["textAdditionalInfo"] {
                textAdditionalInfo = text
            }
            
            var lightColor = ""
            if case let .string(color) = selectedFeatureProperties["styleLightColor"] {
                lightColor = color
            }
            
            var darkColor = ""
            if case let .string(color) = selectedFeatureProperties["styleDarkColor"] {
                darkColor = color
            }
            
            //  Parser CS data
            var availablePercentImageBand: String = ""
            if case let .string(imageBand) = selectedFeatureProperties["availablePercentImageBand"] {
                availablePercentImageBand = imageBand
            }
            
            var csAvailability: AvailabilityCSData?
            if case let .object(csProperties) = selectedFeatureProperties["availabilityCSData"] {
                var updateTS: Double = 0
                var title = ""
                var desc = ""
                var themeColor = ""
                var primaryDesc = ""
                var secondaryDesc = ""
                
                if case let .string(text) = csProperties["title"] {
                    title = text
                }
                if case let .string(text) = csProperties["desc"] {
                    desc = text
                }
                if case let .string(text) = csProperties["themeColor"] {
                    themeColor = text
                }
                if case let .string(text) = csProperties["primaryDesc"] {
                    primaryDesc = text
                }
                if case let .string(text) = csProperties["secondaryDesc"] {
                    secondaryDesc = text
                }
                if case let .number(timestamp) = csProperties["updateTS"] {
                    updateTS = timestamp
                }
                
                csAvailability = AvailabilityCSData(csDict: ["updateTS": updateTS, "title": title, "desc": desc, "themeColor": themeColor, "primaryDesc": primaryDesc, "secondaryDesc": secondaryDesc])
            }
            
            
            self.previouslyTappedAmenityId = amenityID
            let poi = POI(id: id, name: featureInformation, address: featureAddress, lat: location.latitude, long: location.longitude, text: textAdditionalInfo, lightColor: lightColor, darkColor: darkColor)
            
            let (enableWalking, enableNavigation, trackNavigation, canReroute) = self.handleWalkAndDriveHereAction(properties: selectedFeatureProperties, location: location)
            
            //  Track event user click on POI
            if trackNavigation && !id.isEmpty && !layerCode.isEmpty {
                AnalyticsManager.shared.logEventUserClickOnPOI(eventValue: ParameterName.CustomMap.UserClick.tap_poi_layer, screenName: self.screenName, amenityId: id, layerCode: layerCode)
            }
            
            Prefs.shared.setValue(poi.lat.toString(), forkey: .latPoi)
            Prefs.shared.setValue(poi.long.toString(), forkey: .longPoi)
            
            let poiToolTip: POIToolTipsView = POIToolTipsView(poi: poi, walkingEnabled: enableWalking, navigationEnabled: enableNavigation, availablePercentImageBand: availablePercentImageBand, availabilityCSData: csAvailability)
            
            poiToolTip.present(from: mobileNavigationMapView.mapView.frame, in: mobileNavigationMapView.mapView, constrainedTo: poiToolTip.frame, animated: enableWalking)
            poiToolTip.center = currentView.center
            currentView.bringSubviewToFront(poiToolTip)
            poiToolTip.amenityIdBookMark = id
            self.callOutView = poiToolTip
            self.callOutView?.adjust(to: location, in: mobileNavigationMapView)
            
            if type != Values.toolTipType {
                updateBottomSheetIndex(index: 0, toScreen: "Explore")
            }
            
            self.delegate?.toolTipOpenAdjustZoom(location: location, callOut: poiToolTip)
            self.delegate?.toolTipMoreInfo(poi: poi, carPark: nil,screenName: "")
                
            poiToolTip.onNavigateHere = { [weak self] poi in
                guard let self = self else { return }
               // AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Home.UserClick.tap_poi, screenName: ParameterName.Home.screen_view)
                
                let location = CLLocation(latitude: poi.lat, longitude: poi.long)
                getAddressFromLocation(location: location) { address in
                    
                    let mapIconClickName = "navigate_here_\(featureInformation)"
                    self.sendAnalyticsUserClick(eventValue: mapIconClickName)
                    self.sendLocationData(eventValue: mapIconClickName, location: CLLocationCoordinate2D(latitude: poi.lat, longitude: poi.long), locationName: featureInformation)
                    
                    let address = SearchAddresses(alias: "", address1: poi.name , lat: "\(poi.lat)", long: "\(poi.long)", address2: address, distance: "\(0)", isCrowsourceParking: true, amenityId: amenityID, layerCode: layerCode)
                    
                    self.delegate?.navigateHere(address: address, carPark: nil, layerCode: layerCode, amenityId: amenityID, amenityType: "", amenityCarPark: "")
                    }
                }
            
            poiToolTip.onWalkHere = { [weak self] poi in
                guard let self = self else { return }
                
                let mapIconClickName = type == Values.POI ? "navigate_here_\(featureInformation)" :  "navigate_here_\(type)"
                let location = CLLocationCoordinate2D(latitude: poi.lat, longitude: poi.long)
                
                self.sendLocationData(eventValue: mapIconClickName, location: location, locationName: featureInformation)
                
                self.delegate?.poiWalkingPath(poi: poi, trackNavigation: trackNavigation, amenityId: id, name: featureInformation, canReroute: canReroute)
            }
            
//            let mapIconClickName = "map_\(featureInformation)_icon_click"
            var contentMapName = featureInformation
            if case let .string(mapName) = selectedFeatureProperties["contentMapName"] {
                contentMapName = mapName
            }
            let mapIconClickName = "map_\(contentMapName.replacingOccurrences(of: " ", with: "_"))_icon_click"
            self.sendAnalyticsUserClick(eventValue: mapIconClickName)
            
            self.sendLocationData(eventValue: mapIconClickName, location: CLLocationCoordinate2D(latitude: poi.lat, longitude: poi.long), locationName: featureInformation)
            
        }
    }
    
    func openAmenityTooltip(featureInformation: String,
                            featureAddress: String,
                            type: String,
                            isBookmarked: Bool,
                            id: String,
                            location:CLLocationCoordinate2D,
                            amenityCarPark: String,
                            selectedFeatureProperties: JSONObject?,
                            frommap: Bool = true,
                            welcomeDescription: String? = nil,
                            contentMapName: String? = nil,
                            address2: String = "",
                            fullAddress: String = "",
                            availablePercentImageBand: String = "",
                            availabilityCSData: AvailabilityCSData? = nil,
                            placeId: String? = nil,
                            userLocationLinkIdRef: Int? = nil,
                            carparkId: String? = nil) {
        guard let mobileNavigationMapView = mobileNavigationMapView, let currentView = currentView else {
            return
        }
        // TagAmenityOnMap(amenityName: type, isNavigate: false)
        
        let amenityID = id
        let shouldReturn = amenityID == self.previouslyTappedAmenityId
        let description = welcomeDescription
        self.didDeselectAmenityAnnotation()
        if shouldReturn {
            delegate?.selectedAmenity(id: amenityID, isSelected: false)
            return
        }
        //self.setSelectedState(amenityID: amenityID, type: type)
        if frommap {
            delegate?.selectedAmenity(id: amenityID, isSelected: true)
        }
        self.previouslyTappedAmenityId = amenityID
        
        var amenityToolTip: AmenityToolTipsView!
        
        let isCollectionDetail = (self.screenName == ParameterName.CollectionDetailStar.saved_collection_shortcut || self.screenName == ParameterName.CollectionDetailStar.saved_collection_my_saved_places || self.screenName == ParameterName.CollectionDetailStar.saved_collection_custom)
        
        if type == "pasarmalam" {
            
            if let selectedFeatureProperties = selectedFeatureProperties,
                case let .number(startDate) = selectedFeatureProperties["startDate"],
               case let .number(endDate) = selectedFeatureProperties["endDate"] {
                amenityToolTip = PasarMalamToolTipsView(id: amenityID, name: featureInformation, address: featureAddress, startDate: startDate, endDate: endDate, location: location, isCollectionDetail: isCollectionDetail, address2: address2, fullAddress: fullAddress)
            } else {
                return
            }
            
            amenityToolTip.screenName = self.screenName
        } 
        // MARK: Confirm from Yadu no need description on the tooltip on Collection Screen
//        else if let des = welcomeDescription {
//            amenityToolTip = CustomBookmarkTooltip(id: amenityID, name: featureInformation, address: featureAddress, des: des, location: location, type: type, isCollectionDetail: isCollectionDetail, address2: address2, fullAddress: fullAddress)
//            amenityToolTip.screenName = self.screenName
//        } 
        else {
            
            // MARK: BREEZES-7641 Track click on amenity
            if let properties = selectedFeatureProperties {
                
                let (enableWalking, enableNavigation, _, _) = self.handleWalkAndDriveHereAction(properties: properties, location: location)
                amenityToolTip = AmenityToolTipsView(id: amenityID, name: featureInformation, address: featureAddress, location:location, type: type, walkingEnabled: enableWalking, navigationEnabled: enableNavigation,isShareEnable: true, isCollectionDetail: isCollectionDetail, address2: address2, fullAddress: fullAddress, availablePercentImageBand: availablePercentImageBand, availabilityCSData: availabilityCSData)
                
                
            }else {
                amenityToolTip = AmenityToolTipsView(id: amenityID, name: featureInformation, address: featureAddress, location:location, type: type,isShareEnable: true, isCollectionDetail: isCollectionDetail, address2: address2, fullAddress: fullAddress, availablePercentImageBand: availablePercentImageBand, availabilityCSData: availabilityCSData)
            }
            
            amenityToolTip.screenName = self.screenName
        }
        amenityToolTip.amenityIdBookMark = id
        if self.showSaveBtn {
            amenityToolTip.bookmarkId = Int(amenityID)
            // TODO if is collection detail
            amenityToolTip.onSaveAction = { [weak self] isSave in
                guard let self = self else { return }
                sendSaveActionToRN(amenityId: amenityToolTip.tooltipId,
                                   lat: location.latitude,
                                   long: location.longitude,
                                   name:  featureInformation,
                                   address1: featureAddress,
                                   address2:"",
                                   amenityType: type,
                                   fullAddress: fullAddress,
                                   isDestination: false,
                                   isSelected: !isBookmarked,
                                   placeId: placeId ?? "",
                                   userLocationLinkIdRef: userLocationLinkIdRef ?? nil)
                
            }
            
        }
        
        amenityToolTip.present(from: mobileNavigationMapView.mapView.frame, in: mobileNavigationMapView.mapView, constrainedTo: amenityToolTip.frame, animated: true)
        amenityToolTip.center = currentView.center
        currentView.bringSubviewToFront(amenityToolTip)
        // For content screen, issave alway too
        amenityToolTip.isSaved = isBookmarked
        self.callOutView = amenityToolTip
        self.callOutView?.adjust(to: location, in: mobileNavigationMapView)
        amenityToolTip.onNavigationHere = { [weak self] (name, address,coordinate) in
            guard let self = self else { return }
            //self.TagAmenityOnMap(amenityName: type, isNavigate: true)
            let location = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
            getAddressFromLocation(location: location) { address in
                let address = SearchAddresses(alias: "", address1: featureInformation, lat: "\(coordinate.latitude)", long: "\(coordinate.longitude)", address2: address, distance: "0", fullAddress: fullAddress, isCrowsourceParking: true, placeId: placeId, userLocationLinkIdRef: userLocationLinkIdRef, amenityId: amenityID, layerCode: type)
                //self.didDeselectAmenityAnnotation()
                
                if self.screenName != ParameterName.CustomMap.screen_view{
                    
                    self.sendAnalyticsUserClick(eventValue: ParameterName.CollectionDetailStar.UserClick.navigate_here_saved)
                    
                    self.sendLocationData(eventValue: ParameterName.CollectionDetailStar.LocationData.navigate_here_saved, location: CLLocationCoordinate2D(latitude: coordinate.latitude, longitude: coordinate.longitude), locationName: featureInformation)
                }
                else{
                    
                    let mapIconClickName = type == Values.POI ? "navigate_here_\(featureInformation)" :  "navigate_here_\(type)"
                    self.sendAnalyticsUserClick(eventValue: mapIconClickName)
                    
                    self.sendLocationData(eventValue: mapIconClickName, location: CLLocationCoordinate2D(latitude: coordinate.latitude, longitude: coordinate.longitude), locationName: featureInformation)
                }
                
                print("AmenityID: \(amenityID), bookmark name: \(address.address1 ?? "")")
                
                self.delegate?.navigateHere(address: address, carPark: nil, layerCode: type, amenityId: amenityID, amenityType: type, amenityCarPark: amenityCarPark)
            }
        }
        
        
        
        amenityToolTip.onWalkHere = { [weak self] (id, name, address,coordinate) in
            guard let self = self else { return }
            
            let mapIconClickName = type == Values.POI ? "navigate_here_\(featureInformation)" :  "navigate_here_\(type)"
            self.sendLocationData(eventValue: mapIconClickName, location: coordinate, locationName: featureInformation)
            
            let poi = POI(id: id, name: name, address: address, lat: coordinate.latitude, long: coordinate.longitude, text: "", lightColor: "", darkColor: "")
            self.delegate?.poiWalkingPath(poi: poi, trackNavigation: false, amenityId: amenityID, name: name, canReroute: nil)
        }
        
        
        let stringLat =  location.latitude.toString()
        let stringLong = location.longitude.toString()
        let bookMarkID = Int(amenityID)
        
        amenityToolTip.shareViaLocation = {
            shareLocationFromNative(address1: featureAddress, address2: "", latitude: stringLat, longtitude: stringLong, code: "", is_destination: false, amenity_id: amenityToolTip.tooltipId, amenity_type: type, description: description ?? "", name: featureInformation, bookmarkId: bookMarkID ?? 0, placeId: placeId ?? "", userLocationLinkIdRef: userLocationLinkIdRef)
        }
        
        amenityToolTip.inviteLocation = {
            inviteLocationFromNative(address1: featureAddress, address2: "", latitude: stringLat, longtitude: stringLong, code: "", is_destination: false, amenity_id: amenityToolTip.tooltipId, amenity_type: type, description: description ?? "", name: featureInformation, bookmarkId: bookMarkID ?? 0, placeId: placeId ?? "", userLocationLinkIdRef: userLocationLinkIdRef)
        }
        
        if self.screenName != ParameterName.CustomMap.screen_view{
            
            self.sendAnalyticsUserClick(eventValue: ParameterName.CollectionDetailStar.UserClick.saved_icon_click)
            
            self.sendLocationData(eventValue: ParameterName.CollectionDetailStar.LocationData.map_saved_icon_click, location: location, locationName: featureInformation)
        }
        else{
            
//            let mapIconClickName = type == Values.POI ? "map_\(featureInformation)_icon_click" :  "map_\(type)_icon_click"
            let mapIconClickName = "map_\(contentMapName?.replacingOccurrences(of: " ", with: "_") ?? "")_icon_click"
            self.sendAnalyticsUserClick(eventValue: mapIconClickName)
            
            self.sendLocationData(eventValue: mapIconClickName, location: location, locationName: featureInformation)
        }
        self.delegate?.toolTipOpenAdjustZoom(location: location, callOut: amenityToolTip)
    }
    
    private func handleWalkAndDriveHereAction(properties:JSONObject, location: CLLocationCoordinate2D) -> (Bool, Bool, Bool, Bool) {
        var enableWalking = false
        var enableNavigation = true
        var trackNavigation = false
        var canReroute = true
        
        var userInsideZone = false
        if let highestZonePoint = SelectedProfiles.shared.getHigherZoneRadius() {
            userInsideZone = identifyUserLocInOuterZone(centerCoordinate: CLLocationCoordinate2D(latitude: highestZonePoint.centerPointLat.toDouble() ?? 0, longitude: highestZonePoint.centerPointLong.toDouble() ?? 0) ,currentLocation: LocationManager.shared.location.coordinate)
        }
        
        if case let .boolean(walking) = properties["enableWalking"] {
            enableWalking = walking
        }
        if case let .boolean(navigation) = properties["enableZoneDrive"] {
            enableNavigation = navigation
        }
        if enableWalking {
            enableWalking = userInsideZone
        }
        
        if !enableNavigation {
            enableNavigation = !userInsideZone
        }
        
        if case let .boolean(track) = properties["trackNavigation"] {
            trackNavigation = track
        }
        if case let .boolean(reroute) = properties["canReroute"] {
            canReroute = reroute
        }
        
        return (enableWalking, enableNavigation, trackNavigation, canReroute)
    }
    
    func openAmenityToolTipView(feature:Turf.Feature,location:CLLocationCoordinate2D, frommap: Bool = true) {
        
        if let selectedFeatureProperties = feature.properties,
           case let .string(featureInformation) = selectedFeatureProperties["name"],
           case let .string(featureAddress) = selectedFeatureProperties["address"],
           case let .string(type) = selectedFeatureProperties["type"],
//           case let .number(bookmarkId) = selectedFeatureProperties["bookmarkId"],
//           case let .string(poiID) = selectedFeatureProperties["poiID"],
           case let .boolean(isBookmarked) = selectedFeatureProperties["isBookmarked"] ?? false,
           case let .string(id) = selectedFeatureProperties["id"]
        {
            
            let amenityID = id
            
            var amenityCarPark = ""
            if case let .string(ID) = selectedFeatureProperties["amenityCarPark"] {
                amenityCarPark = ID
            }
            
            let shouldReturn = amenityID == self.previouslyTappedAmenityId
            self.didDeselectAmenityAnnotation()
            if shouldReturn {
                return
            }
            
            var contentMapName = ""
            if case let .string(mapName) = selectedFeatureProperties["contentMapName"] {
                contentMapName = mapName
            }
            
            var description = ""
            if case let .string(desc) = selectedFeatureProperties["description"] {
                description = desc
            }
            
            self.openAmenityTooltip(featureInformation: featureInformation,
                                    featureAddress: featureAddress,
                                    type: type,
                                    isBookmarked: isBookmarked,
                                    id: id,
                                    location: location, amenityCarPark: amenityCarPark,
                                    selectedFeatureProperties: selectedFeatureProperties,
                                    frommap: frommap,
                                    welcomeDescription: description,
                                    contentMapName: contentMapName)
        }
    }
    
    func openCarparkToolTipView(carpark: Carpark, tapped: Bool = false) {
        DispatchQueue.main.async { [weak self] in
            
            guard let self = self,
                  let mobileNavigationMapView = self.mobileNavigationMapView else { return }
            
            let sameCarpark = carpark.id == self.currentSelectedCarpark?.id
            
            self.didDeselectAmenityAnnotation()
            
            //AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Home.UserClick.tap_carpark, screenName: ParameterName.Home.screen_view)
            
            if sameCarpark {
                self.currentSelectedCarpark = nil
                // click the same one, don't show callout
                return
            }
            self.currentSelectedCarpark = carpark
            
            // set camera (actually should not set if not because of tapped
            if tapped {
                let cameraOptions = CameraOptions(center: CLLocationCoordinate2D(latitude: carpark.lat ?? 0, longitude: carpark.long ?? 0), zoom: mobileNavigationMapView.mapView.mapboxMap.cameraState.zoom) // using current zoom level
                mobileNavigationMapView.mapView.camera.ease(to: cameraOptions, duration: Values.standardAnimationDuration, curve: .easeInOut, completion: nil)
                
                // every time user click the parking icon. We'll send an RN event
                if let profileLocation = SelectedProfiles.shared.getOuterZoneCentrePoint() {
                    let _ = ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.PARKING_BOTTOM_SHEET_REFRESH, data: ["parkingID":carpark.id ?? "","isDestinationHasCarPark":false, Values.FOCUSED_LAT: profileLocation.latitude.toString(), Values.FOCUSED_LONG: profileLocation.longitude.toString()] as? NSDictionary).subscribe(onSuccess: {_ in
                        //We don't need to process this response
                    },onFailure: {_ in
                        //We don't need to process this response
                    })
                }
            }
        
            let callOutView = CarparkToolTipsView(carpark: carpark, calculateFee:true, routeablePoint: nil)
            
            callOutView.screenName = self.screenName
            
            let isCollectionDetail = (self.screenName == ParameterName.CollectionDetailStar.saved_collection_shortcut || self.screenName == ParameterName.CollectionDetailStar.saved_collection_my_saved_places || self.screenName == ParameterName.CollectionDetailStar.saved_collection_custom)
            
            if self.showSaveBtn && !isCollectionDetail {
                
                callOutView.onSaveAction = { saved in
                    sendSaveActionToRN(amenityId: carpark.id ?? "",
                                       lat: carpark.lat ?? 0,
                                       long: carpark.long ?? 0,
                                       name:  carpark.name ?? "",
                                       address1: "",
                                       address2: "",
                                       amenityType: Values.AMENITYCARPARK,
                                       fullAddress: "",
                                       isDestination: false,
                                       isSelected: !(carpark.isBookmarked ?? true))
                }
            }
            
            callOutView.present(from: mobileNavigationMapView.mapView.frame, in: mobileNavigationMapView.mapView, constrainedTo: callOutView.frame, animated: true)
            self.callOutView = callOutView
            self.callOutView?.adjust(to: callOutView.tooltipCoordinate, in:mobileNavigationMapView)
            updateBottomSheetIndex(index: 0, toScreen: "TBRAmenityDetails")
//            if let viewModel = self.mapLandingViewModel{
//
//                if viewModel.landingMode != Values.PARKING_MODE{
//
//                    //Sending event to RN to collapse the bottom sheet to kee tool tip centre aligned
//                    updateBottomSheetIndex(index: 0, toScreen: "Landing")
//
//                    if(viewModel.bottomSheetIndex == 0){
//
//                        self.adjustZoomOnlyForAmenity(at: self.calloutView!.tooltipCoordinate, callout: self.calloutView!, viewModel: viewModel)
//                    }
//
//                }
//                else{
//
//                    self.adjustZoomOnlyForAmenity(at: self.calloutView!.tooltipCoordinate, callout: self.calloutView!, viewModel: viewModel)
//                }
//
//            }
            
            callOutView.onCarparkSelectedV2 = { [weak self] (theCarpark, _)in
                guard let self = self else { return }
                
                self.sendAnalyticsUserClick(eventValue: ParameterName.CollectionDetailStar.UserClick.navigate_here_parking)
                
                self.sendLocationData(eventValue: ParameterName.CollectionDetailStar.LocationData.navigate_here_parking, location: CLLocationCoordinate2D(latitude: theCarpark.lat ?? 0, longitude: theCarpark.long ?? 0), locationName: theCarpark.name ?? "")
                
                let location = CLLocation(latitude: theCarpark.lat ?? 0, longitude: theCarpark.long ?? 0)
                getAddressFromLocation(location: location) { address in
                    let address = SearchAddresses(alias: "", address1: theCarpark.name ?? "", lat: "\(theCarpark.lat ?? 0)", long: "\(theCarpark.long ?? 0)", address2: address, distance: "\(theCarpark.distance ?? 0)")
                    self.delegate?.navigateHere(address: address, carPark: theCarpark, layerCode: "", amenityId: "", amenityType: "", amenityCarPark: "")
                    self.didDeselectAmenityAnnotation()
                   
                }
            }
            callOutView.onCalculate = { [weak self] theCarpark in
                guard let self = self else { return }
                
                self.sendAnalyticsUserClick(eventValue: ParameterName.CollectionDetailStar.UserClick.parking_calculatefee)
                
                self.delegate?.toolTipMoreInfo(poi: nil, carPark: theCarpark,screenName: "ParkingCalculator")
//                let _ = ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.OPEN_CALCULATE_FEE_CARPARK, data: ["parkingID":theCarpark.id] as? NSDictionary)
//                    .subscribe(onSuccess: { _ in
//                        //We don't need to process this
//                    }, onFailure: { _ in
//                        //We don't need to process this
//                    })
                //self.didDeselectAmenityAnnotation()
            }
            callOutView.onMoreInfo = { [weak self]  theCarpark in
                guard let self = self else { return }
                
                self.sendAnalyticsUserClick(eventValue: ParameterName.CollectionDetailStar.UserClick.parking_tooltip_see_more)
                
                //self.didDeselectAmenityAnnotation()
                self.delegate?.toolTipMoreInfo(poi: nil, carPark: theCarpark,screenName: "ParkingPriceDetail")
                
            }
            
            let stringLat = carpark.lat?.toString()
            let stringLong =  carpark.long?.toString()
            
            callOutView.shareViaLocation = {
                shareLocationFromNative(address1: carpark.name ?? "", address2: "", latitude: stringLat ?? "", longtitude: stringLong ?? "", code: "", is_destination: false, amenity_id: carpark.id ?? "", amenity_type: Values.AMENITYCARPARK, description: "", name: carpark.name ?? "")
                
            }
            
            callOutView.inviteLocation = {
                inviteLocationFromNative(address1: carpark.name ?? "", address2: "", latitude: stringLat ?? "", longtitude: stringLong ?? "", code: "", is_destination: false, amenity_id: carpark.id ?? "", amenity_type: Values.AMENITYCARPARK, description: "", name: carpark.name ?? "")
            }
            
            self.sendAnalyticsUserClick(eventValue: ParameterName.CollectionDetailStar.UserClick.tap_carpark)
            
            self.sendLocationData(eventValue: ParameterName.CollectionDetailStar.LocationData.tap_carpark, location: CLLocationCoordinate2D(latitude: carpark.lat ?? 0, longitude: carpark.long ?? 0), locationName: carpark.name ?? "")
            
            self.delegate?.toolTipOpenAdjustZoom(location: callOutView.tooltipCoordinate, callOut: callOutView)

        }
    }
    
    func didDeselectAmenityAnnotation() {
        if let callout = callOutView {
            if callout is AmenityToolTipsView || callout is POIToolTipsView || callout is DropPinToolTipView {
                Prefs.shared.setValue(callout.tooltipCoordinate.latitude.toString(), forkey: .latSavePlace)
                Prefs.shared.setValue(callout.tooltipCoordinate.longitude.toString(), forkey: .longSavePlace)
//                callout.tooltipCoordinate.longitude
//                if let carparkUpdater = carparkUpdater {
//
//                    carparkUpdater.currentSelectedCarpark()
//                }
                
            }
            else if callout is CarparkToolTipsView{
                
                currentSelectedCarpark = nil
            }
            callout.dismiss(animated: true)
            callout.removeFromSuperview()
            self.callOutView = nil
        }
        
        previouslyTappedAmenityId = ""
    }
    
    func sendAnalyticsUserClick(eventValue:String){
        AnalyticsManager.shared.logClickEvent(eventValue: eventValue, screenName: self.screenName)
    }
    
    func sendLocationData(eventValue: String, location: CLLocationCoordinate2D, locationName: String) {
        AnalyticsManager.shared.sendLocationData(eventValue: eventValue, screenName: screenName, locationName: locationName, latitude: location.latitude.toString(), longitude:location.longitude.toString())
    }
}



//
//  CustomNightStyle.swift
//  Breeze
//
//  Created by VishnuKanth on 02/03/21.
//
import MapboxCoreNavigation
import MapboxMaps
import MapboxCoreMaps
import MapboxNavigation
import UIKit

class CustomNightStyleCP: CustomDayStyleCP {

    required init() {
        super.init()
#if TESTING
        mapStyleURL = URL(string: Constants.Map.testURL)!
#else
        mapStyleURL = URL(string: Constants.Map.nightStyleUrl)!
#endif
        previewMapStyleURL = mapStyleURL
        styleType = .night
    }
    
    override func apply() {
        super.apply()
        
        let traitCollection = UITraitCollection(userInterfaceIdiom: .carPlay)
        
        NavigationMapView.appearance(for: traitCollection).routeCasingColor = .clear
        NavigationMapView.appearance(for: traitCollection).trafficLowColor = UIColor.trafficLowColorDark
        NavigationMapView.appearance(for: traitCollection).trafficHeavyColor = UIColor.trafficHeavyColorDark
        NavigationMapView.appearance(for: traitCollection).trafficSevereColor = UIColor.trafficSevereColorDark
        NavigationMapView.appearance(for: traitCollection).trafficModerateColor = UIColor.trafficModerateColorDark
        NavigationMapView.appearance(for: traitCollection).trafficUnknownColor = UIColor.trafficUnknownColorDark
        NavigationMapView.appearance(for: traitCollection).alternativeTrafficLowColor = UIColor.alternateTrafficLowColorDark
        NavigationMapView.appearance(for: traitCollection).alternativeTrafficModerateColor = UIColor.alternatTrafficModerateColorDark
        NavigationMapView.appearance(for: traitCollection).alternativeTrafficHeavyColor = UIColor.alternateTrafficHeavyColorDark
        NavigationMapView.appearance(for: traitCollection).alternativeTrafficSevereColor = UIColor.alternateTrafficSevereColorDark
        NavigationMapView.appearance(for: traitCollection).alternativeTrafficUnknownColor = UIColor.alternateTrafficUnknownColorDark
    }
}


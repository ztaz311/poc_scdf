//
//  MapView.swift
//  Breeze
//
//  Created by VishnuKanth on 25/05/21.
//

import Foundation
@_spi(Experimental) import MapboxMaps
import MapboxCoreMaps
import MapboxNavigation
import MapboxCoreNavigation
import SwiftyBeaver
import UIKit
import CoreLocation
import Turf
import MapKit
import SwiftUI
extension MapView{
    
    func setCruiseCamera(at coordinate: CLLocationCoordinate2D?, heading: Double = 0, animated: Bool = true, reset: Bool = false,isStarted:Bool = false, isCarPlay: Bool = false,bottomSheetHeight:Int = 0) {

        let pitch: CGFloat = isStarted ? Values.defaultCruiseModePitch : 0
        var zoomLevel: CGFloat = isStarted ? (isCarPlay ? Values.cruiseModeCarPlayZoomLevel : Values.cruiseModeMobileZoomLevel) : Values.defaultMapZoomLevel
        
        if SelectedProfiles.shared.selectedProfileName() != "" && !isStarted {
            zoomLevel = Values.defaultProfileZoomLevl
        }
        var bottomHeight = 0
        if (bottomSheetHeight == 0){
            
            bottomHeight = 416
        }
        else{
            bottomHeight = bottomSheetHeight
        }
        let bottom: CGFloat = isStarted ? (isCarPlay ? -(self.bounds.height * 0.35) : -(self.bounds.height * 0.70)) : (200 + CGFloat(bottomHeight))
        // TODO: Decide to use right or left based on CarPlay
        let right: CGFloat = 0
        let left: CGFloat =  0
        // MARK: Update puck in the midle screen new requirement for Shiva and Letha
       // isStarted && isCarPlay ? -(self.bounds.width * 0.3) :
        let cameraOptions = CameraOptions(center: coordinate, padding: UIEdgeInsets(top: isStarted ? 0 : 200, left: left, bottom: bottom, right: right), anchor: nil, zoom: zoomLevel, bearing: heading, pitch: pitch)
        
        // since only zoom level/pitch is animatable
        if reset {
            let options = CameraOptions(center: coordinate, padding: UIEdgeInsets(top: 0, left: left, bottom: bottom, right: right), anchor: nil, zoom: zoomLevel - 0.1, bearing: heading, pitch: pitch)
            self.camera.ease(to: options, duration: 1) { (complete) in
                //We don't need to handle this response
            }
            
        }
        self.camera.ease(to: cameraOptions, duration: 1) { (complete) in
            //We don't need to handle this response
        }

    }
    
    func calculateDynamicZoomLevel(coordinate2:CLLocationCoordinate2D,padding:UIEdgeInsets){
        
       
        let kilometers: Double = 3.0
        let halfMeters = (kilometers * 1000) / 2
        let region = MKCoordinateRegion(center: coordinate2, latitudinalMeters: halfMeters, longitudinalMeters: halfMeters)
        let southWest = CLLocationCoordinate2D(
            latitude: coordinate2.latitude - (region.span.latitudeDelta  / 2),
            longitude: coordinate2.longitude - (region.span.longitudeDelta / 2)
        )
        let northEast = CLLocationCoordinate2D(
            latitude: coordinate2.latitude + (region.span.latitudeDelta  / 2),
            longitude: coordinate2.longitude + (region.span.longitudeDelta / 2)
        )

        let bounds = CoordinateBounds(southwest: southWest, northeast: northEast)
       // let bounds = MGLCoordinateBounds(sw: southWest, ne: northEast)
        let cameraOptions = mapboxMap.camera(for: bounds, padding: padding, bearing: 0, pitch: 0)
        self.camera.ease(to: cameraOptions, duration: Values.standardAnimationDuration, curve: .easeInOut, completion: nil)
        //mapView.setVisibleCoordinateBounds(bounds, edgePadding: .zero, animated: false)
    }
    
    func setDynamicZoomLevelBasedonRadius(coordinate1:CLLocationCoordinate2D,dynamicRadius:Int,padding:UIEdgeInsets,coordinate2:CLLocationCoordinate2D,openToolTip:Bool = false,multiplyValue:Double = 2.8){
        
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            
            
            let kilometers: Double = Double(dynamicRadius) * multiplyValue
            
            let mapWidth = UIScreen.main.bounds.size.width
            
            // https://docs.mapbox.com/help/glossary/zoom-level/
            let equator = getMetersPerPixelAtLatitude(coordinate1.latitude, 0.0)
            let arg = equator * mapWidth / (kilometers)
            let valToZoom = log2(arg)
            
            let cameraOptions = CameraOptions(center: openToolTip == true ? coordinate1 : coordinate2, padding: padding, zoom: valToZoom)
            self.camera.ease(to: cameraOptions, duration: 0.4)            
        }
        
//
//            // use coordinate1 as center point calcuate the symmetric coordinate3
//            //let degree = coordinate2.direction(to: coordinate1)
//            let distance = coordinate2.distance(to: coordinate1)
//
//            let bearingList =
//               [ 0.0,
//                45.0,
//                90.0,
//                135.0,
//                180.0,
//                -90.0,
//                -45.0,
//                -135.0]
//
//            var coordinates = [CLLocationCoordinate2D]()
//            for bearing in bearingList {
//
//                let coordinate3 = coordinate2.coordinate(at: distance, facing: bearing)
//                coordinates.append(coordinate3)
//            }
//
////            let coordinate3 = coordinate1.coordinate(at: distance, facing: degree)
//
//            var cameraOptions = self.mapboxMap.camera(for: coordinates, padding: padding, bearing: 0, pitch: 0)
//            cameraOptions.zoom = 14
//            self.camera.ease(to: cameraOptions, duration: Values.standardAnimationDuration, curve: .easeInOut, completion: nil)
//        }
    }
    
    func setDynamicZoomLevelBasedonRadiusForProfile(coordinate1:CLLocationCoordinate2D,dynamicRadius:Int,padding:UIEdgeInsets){
        
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            
            let kilometers: Double = Double(dynamicRadius)*0.8
            
            let mapWidth = UIScreen.main.bounds.size.width
            
            // https://docs.mapbox.com/help/glossary/zoom-level/
            let equator = getMetersPerPixelAtLatitude(coordinate1.latitude, 0.0)
            let arg = equator * mapWidth / (kilometers)
            let valToZoom = log2(arg)
            
            let cameraOptions = CameraOptions(center:coordinate1, padding: padding, zoom: valToZoom)
            self.camera.ease(to: cameraOptions, duration: 0.4)
            

        }
    }
    
    
    func setUpCruiseCustomUserLocationIcon(isStarted:Bool) {
        
        self.ornaments.options.compass.visibility = .hidden
        
        var traitCollection = UITraitCollection(userInterfaceStyle: .light)
        if(Settings.shared.theme == 0){
            
            if(appDelegate().isDarkMode()){
                
                traitCollection = UITraitCollection(userInterfaceStyle: .dark)
                
            }
            
        }
        else{
            
            traitCollection = UITraitCollection(userInterfaceStyle: UIUserInterfaceStyle(rawValue: Settings.shared.theme) ?? .unspecified)
        }
        
        if isStarted {
            self.location.options.puckType = .puck2D(Puck2DConfiguration(topImage: UIImage(named: "userPuckArrow",in:nil,compatibleWith: traitCollection), bearingImage: UIImage(named: "userPuck",in:nil,compatibleWith: traitCollection), shadowImage: nil, scale: .constant(1.0)))
        } else {
            self.location.options.puckType = .puck2D(Puck2DConfiguration(topImage: UIImage(named: "puckArrow",in:nil,compatibleWith: traitCollection), bearingImage: UIImage(named: "puckIcon"), shadowImage: nil, scale: nil))
        }
        self.location.options.puckBearingSource = .course        
    }
    
    func removeAllPreferenceLayers(){
        
        //let type = [Constants.FASTEST,"Shortest","Cheapest","Shortest&Cheapest","Fastest&Cheapest","Fastest&Shortest","Fastest,Shortest&Cheapest"]
        self.removeFastLayer(type: Constants.FASTEST)
        self.removeFastLayer(type: Constants.FASTEST_CHEAPEST)
        self.removeFastLayer(type: Constants.FASTEST_SHORTEST)
        self.removeFastLayer(type: Constants.FASTEST_SHORTEST_CHEAPEST)
        self.removeFastLayer(type: Constants.SHORTEST)
        self.removeFastLayer(type: Constants.SHORTEST_CHEAPEST)
        self.removeFastLayer(type: Constants.CHEAPEST)
        
    }
    func removeFastLayer(type:String) {
        do {
            try self.mapboxMap.style.removeLayer(withId: type)
            try self.mapboxMap.style.removeSource(withId: "_\(type)")
            
        } catch {
            SwiftyBeaver.error("Failed to remove FSC layer.")
        }
    }
    
    func removeShortLayer() {
        do {
            try self.mapboxMap.style.removeLayer(withId: FSCLayer.SHORT_SYMBOL_LAYER)
            try self.mapboxMap.style.removeSource(withId: FSCLayer.SHORT_GEOJSON_SOURCE)
            
        } catch {
            SwiftyBeaver.error("Failed to remove FSC layer.")
        }
    }
    
    func removeCheapestLayer() {
        do {
            try self.mapboxMap.style.removeLayer(withId: FSCLayer.CHEAPEST_SYMBOL_LAYER)
            try self.mapboxMap.style.removeSource(withId: FSCLayer.CHEAPEST_GEOJSON_SOURCE)
            
        } catch {
            SwiftyBeaver.error("Failed to remove FSC layer.")
        }
    }
    
    func removeTwLabelLayer(){
        
        do {
            try self.mapboxMap.style.removeLayer(withId: "TwoLabels")
            try self.mapboxMap.style.removeSource(withId: "TwoLabel_Source")
            
        } catch {
            SwiftyBeaver.error("Failed to remove FSC layer.")
        }
    }
    
    func addTwoLabelLayerToMap(features: Turf.FeatureCollection){
        
        do {
            try self.mapboxMap.style.addImage(UIImage(named: "Union")!, id: "Selected",stretchX: [],stretchY: [])
            
            // Create a GeoJSON data source.
            var geoJSONSource = GeoJSONSource()
            geoJSONSource.data = .featureCollection(features)
            
            let label = Exp(.get){
                "text"
            }
            
//            let iconSize = Exp(.interpolate){
//                Exp(.linear)
//                Exp(.zoom)
//                0
//                0
//                9
//                0.9
//                12.9
//                0.9
//                13
//                0.9
//                16.5
//                0.9
//            }
//
//            let textSize = Exp(.interpolate){
//                Exp(.linear)
//                Exp(.zoom)
//                0
//                0
//                9
//                14
//                12.9
//                14
//                13
//                14
//                16.5
//                14
//            }
            
            var symbolLayer = SymbolLayer(id: "TwoLabels")
            symbolLayer.source = "TwoLabel_Source"
            symbolLayer.iconSize = .constant(3.0)
            symbolLayer.iconImage = .constant(ResolvedImage.name("Selected"))
            symbolLayer.textField = .expression(label)
            symbolLayer.textSize = .constant(14)
            symbolLayer.textAnchor = .constant(.center)
            symbolLayer.textOffset = .constant([0.0,0.0])
            symbolLayer.maxZoom = 22
            symbolLayer.minZoom = 8
            symbolLayer.iconAllowOverlap = .constant(true)
            symbolLayer.iconIgnorePlacement = .constant(true)
            symbolLayer.textLineHeight = .constant(19.0)
            symbolLayer.textFont = .constant([fontFamilySFProFromStudio.Medium])
            //symbolLayer.textColor = .constant(ColorRepresentable.init(color: UIColor(named: "erpPriceColor")!))
            symbolLayer.textColor = .constant(.init(UIColor.white))
            try self.mapboxMap.style.addSource(geoJSONSource, id: "TwoLabel_Source")
            
            try self.mapboxMap.style.addLayer(symbolLayer, layerPosition: .below("puck"))
            SwiftyBeaver.debug("add FSCLayer \(features.features.count)")
        } catch {
            SwiftyBeaver.error("Failed to perform operation with error: \(error.localizedDescription).")
        }
        
    }
    
    func removeFSCImagesToMapStyle(){
        
        DispatchQueue.main.async {
            
            do {
                //try self.mapboxMap.style.removeImage(withId: "FSC_select")
                //try self.mapboxMap.style.removeImage(withId: "FSC_unselect")
                try self.mapboxMap.style.removeImage(withId: "SF_select")
                try self.mapboxMap.style.removeImage(withId: "SF_unselect")
                try self.mapboxMap.style.removeImage(withId: "CS_select")
                try self.mapboxMap.style.removeImage(withId: "CS_unselect")
                try self.mapboxMap.style.removeImage(withId: "CF_select")
                try self.mapboxMap.style.removeImage(withId: "CF_unselect")
                try self.mapboxMap.style.removeImage(withId: "C_select")
                try self.mapboxMap.style.removeImage(withId: "C_unselect")
                
                try self.mapboxMap.style.removeImage(withId: "F_select")
                try self.mapboxMap.style.removeImage(withId: "F_unselect")
                try self.mapboxMap.style.removeImage(withId: "S_select")
                try self.mapboxMap.style.removeImage(withId: "S_unselect")
            }catch {
                SwiftyBeaver.error("Failed to remove Amenity Image.")
            }
            
        }
        
        
    }
    
    func addFSCImagesToMapStyle(){
        
        DispatchQueue.main.async {
        do {
            
            var traitCollection = UITraitCollection(userInterfaceStyle: .light)
            if(Settings.shared.theme == 0){
                
                if(appDelegate().isDarkMode()){
                    
                    traitCollection = UITraitCollection(userInterfaceStyle: .dark)
                    
                }
                
            }
            else{
                
                traitCollection = UITraitCollection(userInterfaceStyle: UIUserInterfaceStyle(rawValue: Settings.shared.theme) ?? .unspecified)
            }
            
            //try self.mapboxMap.style.addImage(UIImage(named: "S&C&F",in:nil,compatibleWith: traitCollection)!, id: "FSC_select",stretchX: [],stretchY: [])
            
            //try self.mapboxMap.style.addImage(UIImage(named: "S&C&F_unselected",in:nil,compatibleWith: traitCollection)!, id: "FSC_unselect",stretchX: [],stretchY: [])
            
            try self.mapboxMap.style.addImage(UIImage(named: "S&F",in:nil,compatibleWith: traitCollection)!, id: "SF_select",stretchX: [],stretchY: [])
            
            try self.mapboxMap.style.addImage(UIImage(named: "S&F_unselected",in:nil,compatibleWith: traitCollection)!, id: "SF_unselect",stretchX: [],stretchY: [])
            
            try self.mapboxMap.style.addImage(UIImage(named: "C&S",in:nil,compatibleWith: traitCollection)!, id: "CS_select",stretchX: [],stretchY: [])
            
            try self.mapboxMap.style.addImage(UIImage(named: "C&S_unselected",in:nil,compatibleWith: traitCollection)!, id: "CS_unselect",stretchX: [],stretchY: [])
            
            try self.mapboxMap.style.addImage(UIImage(named: "C&F",in:nil,compatibleWith: traitCollection)!, id: "CF_select",stretchX: [],stretchY: [])
            
            try self.mapboxMap.style.addImage(UIImage(named: "C&F_unselected",in:nil,compatibleWith: traitCollection)!, id: "CF_unselect",stretchX: [],stretchY: [])
            
            try self.mapboxMap.style.addImage(UIImage(named: "C_selected",in:nil,compatibleWith: traitCollection)!, id: "C_select",stretchX: [],stretchY: [])
            
            try self.mapboxMap.style.addImage(UIImage(named: "C_unselected",in:nil,compatibleWith: traitCollection)!, id: "C_unselect",stretchX: [],stretchY: [])
            
            try self.mapboxMap.style.addImage(UIImage(named: "F_selected",in:nil,compatibleWith: traitCollection)!, id: "F_select",stretchX: [],stretchY: [])
            
            try self.mapboxMap.style.addImage(UIImage(named: "F_unselected",in:nil,compatibleWith: traitCollection)!, id: "F_unselect",stretchX: [],stretchY: [])
            
            try self.mapboxMap.style.addImage(UIImage(named: "S_selected",in:nil,compatibleWith: traitCollection)!, id: "S_select",stretchX: [],stretchY: [])
            
            try self.mapboxMap.style.addImage(UIImage(named: "S_unselected",in:nil,compatibleWith: traitCollection)!, id: "S_unselect",stretchX: [],stretchY: [])
        }catch {
            SwiftyBeaver.error("Failed to Add FSC Images.")
        }
        }
    }
    
    func addFastestLayerToMap(features: Turf.FeatureCollection,type:String,isSelected:Bool = false, currentSelectedSymbolID: String = ""){
        
        do {
            
            
            // Create a GeoJSON data source.
            var geoJSONSource = GeoJSONSource()
            geoJSONSource.data = .featureCollection(features)
            
            
            let iconSize = Exp(.interpolate){
                Exp(.linear)
                Exp(.zoom)
                0
                0
                9
                0.8
                12.9
                1.4
                13
                1.4
                16.5
                1.2
            }
//
//            let textSize = Exp(.interpolate){
//                Exp(.linear)
//                Exp(.zoom)
//                0
//                0
//                9
//                14
//                12.9
//                14
//                13
//                14
//                16.5
//                14
//            }
            
            var symbolLayer = SymbolLayer(id: type)
            symbolLayer.source = "_\(type)"
            
            
            symbolLayer.iconImage = .expression(Exp(.get) {
                    "imageName"
            })
            symbolLayer.textAnchor = .constant(.center)
            symbolLayer.maxZoom = 22
            symbolLayer.minZoom = 9
            symbolLayer.iconSize = .expression(iconSize)
            symbolLayer.iconAnchor = .constant(.bottom)
            symbolLayer.iconAllowOverlap = .constant(true)
            symbolLayer.iconIgnorePlacement = .constant(true)
            try self.mapboxMap.style.addSource(geoJSONSource, id: "_\(type)")
            
            try self.mapboxMap.style.addLayer(symbolLayer, layerPosition: currentSelectedSymbolID.isEmpty ? .below("puck") : .below(currentSelectedSymbolID))
            SwiftyBeaver.debug("add FSCLayer \(features.features.count)")
        } catch {
            SwiftyBeaver.error("Failed to perform operation with error: \(error.localizedDescription).")
        }
        
    }
    
    func addShortestLayerToMap(features: Turf.FeatureCollection){
        
        do {
            try self.mapboxMap.style.addImage(UIImage(named: "Union")!, id: "Selected",stretchX: [],stretchY: [])
            
            // Create a GeoJSON data source.
            var geoJSONSource = GeoJSONSource()
            geoJSONSource.data = .featureCollection(features)
            
            let label = Exp(.get){
                "text"
            }
            
//            let iconSize = Exp(.interpolate){
//                Exp(.linear)
//                Exp(.zoom)
//                0
//                0
//                9
//                0.9
//                12.9
//                0.9
//                13
//                0.9
//                16.5
//                0.9
//            }
//
//            let textSize = Exp(.interpolate){
//                Exp(.linear)
//                Exp(.zoom)
//                0
//                0
//                9
//                14
//                12.9
//                14
//                13
//                14
//                16.5
//                14
//            }
            
            var symbolLayer = SymbolLayer(id: FSCLayer.SHORT_SYMBOL_LAYER)
            symbolLayer.source = FSCLayer.SHORT_GEOJSON_SOURCE
            symbolLayer.iconSize = .constant(1.1)
            symbolLayer.iconImage = .constant(ResolvedImage.name("Selected"))
            symbolLayer.textField = .expression(label)
            symbolLayer.textSize = .constant(14)
            symbolLayer.textAnchor = .constant(.center)
            symbolLayer.textOffset = .constant([0.0,0.0])
            symbolLayer.maxZoom = 22
            symbolLayer.minZoom = 8
            symbolLayer.iconAllowOverlap = .constant(true)
            symbolLayer.iconIgnorePlacement = .constant(true)
            symbolLayer.textLineHeight = .constant(19.0)
            symbolLayer.textFont = .constant([fontFamilySFProFromStudio.Medium])
            //symbolLayer.textColor = .constant(ColorRepresentable.init(color: UIColor(named: "erpPriceColor")!))
            symbolLayer.textColor = .constant(.init(UIColor.white))
            try self.mapboxMap.style.addSource(geoJSONSource, id: FSCLayer.SHORT_GEOJSON_SOURCE)
            
            try self.mapboxMap.style.addLayer(symbolLayer, layerPosition: .below("puck"))
            SwiftyBeaver.debug("add FSCLayer \(features.features.count)")
        } catch {
            SwiftyBeaver.error("Failed to perform operation with error: \(error.localizedDescription).")
        }
        
    }
    
    func addCheapestLayerToMap(features: Turf.FeatureCollection){
        
        do {
            try self.mapboxMap.style.addImage(UIImage(named: "Union")!, id: "Selected",stretchX: [],stretchY: [])
            
            // Create a GeoJSON data source.
            var geoJSONSource = GeoJSONSource()
            geoJSONSource.data = .featureCollection(features)
            
            let label = Exp(.get){
                "text"
            }
            
//            let iconSize = Exp(.interpolate){
//                Exp(.linear)
//                Exp(.zoom)
//                0
//                0
//                9
//                0.9
//                12.9
//                0.9
//                13
//                0.9
//                16.5
//                0.9
//            }
//
//            let textSize = Exp(.interpolate){
//                Exp(.linear)
//                Exp(.zoom)
//                0
//                0
//                9
//                14
//                12.9
//                14
//                13
//                14
//                16.5
//                14
//            }
            
            var symbolLayer = SymbolLayer(id: FSCLayer.CHEAPEST_SYMBOL_LAYER)
            symbolLayer.source = FSCLayer.CHEAPEST_GEOJSON_SOURCE
            symbolLayer.iconSize = .constant(1.1)
            symbolLayer.iconImage = .constant(ResolvedImage.name("Selected"))
            symbolLayer.textField = .expression(label)
            symbolLayer.textSize = .constant(14)
            symbolLayer.textAnchor = .constant(.center)
            symbolLayer.textOffset = .constant([0.0,0.0])
            symbolLayer.maxZoom = 22
            symbolLayer.minZoom = 8
            symbolLayer.iconAllowOverlap = .constant(true)
            symbolLayer.iconIgnorePlacement = .constant(true)
            symbolLayer.textLineHeight = .constant(19.0)
            symbolLayer.textFont = .constant([fontFamilySFProFromStudio.Medium])
            //symbolLayer.textColor = .constant(ColorRepresentable.init(color: UIColor(named: "erpPriceColor")!))
            symbolLayer.textColor = .constant(.init(UIColor.white))
            try self.mapboxMap.style.addSource(geoJSONSource, id: FSCLayer.CHEAPEST_GEOJSON_SOURCE)
            
            try self.mapboxMap.style.addLayer(symbolLayer, layerPosition: .below("puck"))
            SwiftyBeaver.debug("add FSCLayer \(features.features.count)")
        } catch {
            SwiftyBeaver.error("Failed to perform operation with error: \(error.localizedDescription).")
        }
        
    }
    
    private func getPolygonVertices(zoneCoordinate:CLLocationCoordinate2D?,radius:Int) -> Turf.Polygon?{
        
        if let zoneCoordinate = zoneCoordinate{
            
            let turfPolygon = Turf.Polygon(center: zoneCoordinate, radius: LocationDistance(radius), vertices: 100)
            
            return turfPolygon
        }
        
        return nil
    }
    
    private func addProfileLayers(contentZoneId:String,zoneCoordinate:CLLocationCoordinate2D,zoneRadius:Int,zoneColor:UIColor,minZoom:Double){
        
        let style = self.mapboxMap.style
        if let getPolygonVertices = self.getPolygonVertices(zoneCoordinate: zoneCoordinate, radius: zoneRadius){
            
            // Create a `GeoJSONSource` from a Turf geometry.
            var source = GeoJSONSource()
            let point = Turf.Feature(geometry: getPolygonVertices)
            
            // Set the source's data property to the feature.
            source.data = .feature(point)

            var fillInnerLayer = FillLayer(id: contentZoneId)
            fillInnerLayer.source = contentZoneId
            fillInnerLayer.fillColor = .constant(StyleColor(zoneColor))
            fillInnerLayer.maxZoom = minZoom
            fillInnerLayer.minZoom = 0.0

            // Add the source and layer to the map's style.
            do{
                try style.addSource(source, id: contentZoneId)
                try style.addPersistentLayer(fillInnerLayer)
            }
            catch{
                SwiftyBeaver.error("Failed to add inner zone layer: \(error.localizedDescription).")
            }
            
        }
    }

    
    private func addFillOuterLayer() {
        let style = self.mapboxMap.style
        
        if let outerPolygon = getOuterPloygon(), let innerPolygon = getInnerPloygon(),
           let color = SelectedProfiles.shared.getOuterZoneColor() {

            let outerRing = Turf.Ring(coordinates: outerPolygon.coordinates[0])
            let innerRing = Turf.Ring(coordinates: innerPolygon.coordinates[0])
            let turfPolygon = Turf.Polygon(outerRing: outerRing, innerRings: [innerRing])
            
            // Create a `GeoJSONSource` from a Turf geometry.
            var source = GeoJSONSource()
            let point = Turf.Feature(geometry: turfPolygon)
            
            // Set the source's data property to the feature.
            source.data = .feature(point)

            var fillOuterLayer = FillLayer(id: AmenitiesLayer.outerZoneLayer)
            
            fillOuterLayer.source = AmenitiesLayer.outerZoneSource
            fillOuterLayer.fillColor = .constant(StyleColor(color))
            fillOuterLayer.maxZoom = SelectedProfiles.shared.getOuterZoneInActiveZoom()
            fillOuterLayer.minZoom = SelectedProfiles.shared.getOuteZoneActiveZoom()

            // Add the source and layer to the map's style.
            do {
                
                try style.addSource(source, id: AmenitiesLayer.outerZoneSource)
                try style.addPersistentLayer(fillOuterLayer)
            }
            catch {
                
                SwiftyBeaver.error("Failed to add outer zone layer: \(error.localizedDescription).")
            }
        }
    }
    
    private func getInnerPloygon() -> Turf.Polygon?{
        
        if let innerZoneCoordinate = SelectedProfiles.shared.getInnerZoneCentrePoint()
        {
            
            let turfPolygon = Turf.Polygon(center: innerZoneCoordinate, radius: LocationDistance(SelectedProfiles.shared.getInnerZoneRadius()), vertices: 100)
            
            return turfPolygon
        }
        
        return nil
    }
    
    private func getOuterPloygon() -> Turf.Polygon?{
        
        if let higherZoneRadius = SelectedProfiles.shared.getHigherZoneRadius(){
            
            let turfPolygon = Turf.Polygon(center: CLLocationCoordinate2D(latitude: higherZoneRadius.centerPointLat.toDouble() ?? 0, longitude: higherZoneRadius.centerPointLong.toDouble() ?? 0), radius: LocationDistance(SelectedProfiles.shared.getOuterZoneRadius()), vertices: 100)
            
            return turfPolygon
        }
        
        return nil
    }
    
    private func addFillInnerLayer() {
        let style = self.mapboxMap.style
        
            if let getInnerPolygon = self.getInnerPloygon(),let color = SelectedProfiles.shared.getInnerZoneColor(){
                
                // Create a `GeoJSONSource` from a Turf geometry.
                var source = GeoJSONSource()
                let point = Turf.Feature(geometry: getInnerPolygon)
                
                // Set the source's data property to the feature.
                source.data = .feature(point)

                var fillInnerLayer = FillLayer(id: AmenitiesLayer.innerZoneLayer)
                fillInnerLayer.source = AmenitiesLayer.innerZoneSource
                fillInnerLayer.fillColor = .constant(StyleColor(color))
                fillInnerLayer.maxZoom = SelectedProfiles.shared.getInnerZoneInActiveZoom()
                fillInnerLayer.minZoom = SelectedProfiles.shared.getInnerZoneActiveZoom()

                // Add the source and layer to the map's style.
                do{
                    try style.addSource(source, id: AmenitiesLayer.innerZoneSource)
                    try style.addPersistentLayer(fillInnerLayer)
                }
                catch{
                    SwiftyBeaver.error("Failed to add inner zone layer: \(error.localizedDescription).")
                }
            
            }
    }
    
    func addProfileZones(){
        
        self.addFillOuterLayer()
//        self.addFillInnerLayer()
        
    }
    
    func removeProfileZones() {
        
        let layerIds = [
            AmenitiesLayer.outerZoneLayer,
            AmenitiesLayer.innerZoneLayer
        ]
        
        let sourceIds = [
            AmenitiesLayer.outerZoneSource,
            AmenitiesLayer.innerZoneSource
        ]
        
        self.removeLayerIds(layerIds: layerIds)
        self.removeSourceIds(sourceIds: sourceIds)
    }
    
    func removeProfileZonesDynamically(){
        
        var layerIds = [String]()
        var sourceIds = [String]()
        
        let zoneDetails = SelectedProfiles.shared.getAllZones()
        
        for zoneDetail in zoneDetails {
            
            layerIds.append("\(zoneDetail.contentZoneID)")
            sourceIds.append("\(zoneDetail.contentZoneID)")
        }
        
        self.removeLayerIds(layerIds: layerIds)
        self.removeSourceIds(sourceIds: sourceIds)
    }
    
    func addProfileZonesDynamically(zoneColors:[ZoneColor],zoneDetails:[ContentZone]){
        
        for zoneColor in zoneColors {
            
            for zoneDetail in zoneDetails {
                
                if zoneColor.name == zoneDetail.zoneID{
                    
                    let zoneCoordinate = CLLocationCoordinate2D(latitude: zoneDetail.centerPointLat.toDouble() ?? 0, longitude: zoneDetail.centerPointLong.toDouble() ?? 0)
                    self.addProfileLayers(contentZoneId: "\(zoneDetail.contentZoneID)", zoneCoordinate: zoneCoordinate, zoneRadius: zoneDetail.radius, zoneColor: UIColor(hexString: zoneColor.color,alpha: zoneColor.opacity ?? 0.1),minZoom: Double(zoneDetail.deactiveZoomLevel))
                }
            }
        }
    }
    
}

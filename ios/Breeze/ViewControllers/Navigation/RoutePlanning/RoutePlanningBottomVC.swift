//
//  RoutePlanningBottomVC.swift
//  Breeze
//
//  Created by Zhou Hao on 3/2/21.
//

import UIKit
import MapboxDirections
import MapboxCoreNavigation
import MapboxNavigation
import SwiftyBeaver

protocol RouteBottomVCDelegate: AnyObject {

    func onRouteChange(index:Int)
    func onRouteConfirmed(index: Int)
    func onShareMyETA(index: Int)
    func onCarparksNearby(index: Int)
    func onReset()
    
}

class RoutePlanningBottomVC: UIViewController {
    
    weak var delegate: RouteBottomVCDelegate?
    var onConfirmed: (() -> Void)?
    var onCarparks: (() -> Void)?
    var onShareETA: (() -> Void)?
    var response: RouteResponse?
    var route:Route?
    var erpPrice = ""
    var previousIndex = 0
    var needToReset = false {
        didSet {
            setupCarparkButton()
//            setupShareETAButton()
        }
    }
    var isETAShared = false {
        didSet {
            setupShareETAButton()
        }
    }
    
    var hideButtons = false {
        didSet {
            carparksButton.isHidden = hideButtons
            confirmButton.isHidden = hideButtons
            shareETAButton.isHidden = hideButtons
            bgView.isHidden = hideButtons
        }
    }
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var carparksButton: UIButton!
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var shareETAButton: UIButton!
    @IBOutlet weak var bgView: UIView!
    
    
//    private var buttonGradientLayer: CAGradientLayer?
    private let cellHeight: CGFloat = 120
    private let cellPercentWidth: CGFloat = 0.9
        
    private var centeredCollectionViewFlowLayout: CenteredCollectionViewFlowLayout!
    private var currentCenteredPage = 0

    override func viewDidLoad() {
        super.viewDidLoad()

        setupCollectionView()
        setupCarparkButton()
        setupShareETAButton()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        carparksButton.layer.cornerRadius = carparksButton.bounds.height / 2
        carparksButton.layer.masksToBounds = true
        
        shareETAButton.layer.cornerRadius = shareETAButton.bounds.height / 2
        shareETAButton.layer.masksToBounds = true
        
        confirmButton.layer.cornerRadius = confirmButton.bounds.height / 2
        confirmButton.layer.masksToBounds = true
    }
        
    deinit {
        SwiftyBeaver.debug("RoutePlanningBottomVC deinit")
    }
    
    private func setupCollectionView() {
        centeredCollectionViewFlowLayout = (collectionView.collectionViewLayout as! CenteredCollectionViewFlowLayout)
        collectionView.decelerationRate = UIScrollView.DecelerationRate.fast
        
        collectionView.dataSource = self
        collectionView.delegate = self
        
        centeredCollectionViewFlowLayout.itemSize = CGSize(
            width: view.bounds.width * cellPercentWidth,
            height: cellHeight
        )
        
        centeredCollectionViewFlowLayout.minimumLineSpacing = 12
//        centeredCollectionViewFlowLayout.sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 15, right: 10)

        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
        
//        let btnColor1 = UIColor.navBottomButtonColor1.resolvedColor(with: self.traitCollection)
//        let btnColor2 = UIColor.navBottomButtonColor2.resolvedColor(with: self.traitCollection)
//        self.confirmButton.applyGradient(colors: [btnColor1.cgColor,btnColor2.cgColor], withShadow: false)
    
        setupCarparkButton()
        setupShareETAButton()
    }
    
    public func reload(){
        collectionView.reloadData()
    }
    
    private func setupCarparkButton() {
        // TODO: Adjust the text with image automatically instead of using spaces
        // This is because moveImageLeftTextCenter is not accurate. But it's used in a few places
        let text = needToReset ? "Reset" : "Parking"
        let textColor = needToReset ? UIColor.white : .white
        let image = needToReset ? UIImage(named: "reset") : UIImage(named: "nearby")
        
//        self.carparksButton.borderWidth = needToReset ? 1: 0
//        self.carparksButton.borderColor = UIColor.carparkResetTextColor
        self.carparksButton.setTitle(text, for: .normal)
        self.carparksButton.setTitleColor(textColor, for: .normal)
        self.carparksButton.sizeToFit()
        self.carparksButton.moveImageLeftTextCenter(image: image!, imagePadding: 25, renderingMode: .alwaysOriginal, alignment: .left)

//        let color1 = needToReset ? UIColor.carparkResetBgColor1.resolvedColor(with: self.traitCollection) : UIColor.cruiseRoadNameColor1.resolvedColor(with: self.traitCollection)
//        let color2 = needToReset ? UIColor.carparkResetBgColor2.resolvedColor(with: self.traitCollection): UIColor.cruiseRoadNameColor2.resolvedColor(with: self.traitCollection)

//        self.carparksButton.removeGradient()
//        self.carparksButton.applyGradient(colors: [color1.cgColor,color2.cgColor], withShadow: false)
    }
    
    
    private func setupShareETAButton() {
        // TODO: Adjust the text with image automatically instead of using spaces
        // This is because moveImageLeftTextCenter is not accurate. But it's used in a few places
        let text = isETAShared ? "Edit message" : "Notify my arrival"
        let textColor = UIColor.white
        let image = UIImage(named: "shareETA")
        
        self.shareETAButton.setTitle(text, for: .normal)
        
        self.shareETAButton.setTitleColor(textColor, for: .normal)
        self.shareETAButton.sizeToFit()
        self.shareETAButton.moveImageLeftTextCenter(image: image!, imagePadding: 25, renderingMode: .alwaysOriginal, alignment: .left)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.swipe_card, screenName: ParameterName.routeplanning_screen)

        
        let visibleRect = CGRect(origin: collectionView.contentOffset, size: collectionView.bounds.size)
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        let visibleIndexPath = collectionView.indexPathForItem(at: visiblePoint)
//        if(visibleIndexPath!.row != previousIndex)
//        {
//            previousIndex = visibleIndexPath!.row
//
//            self.response?.routes?.swapAt(visibleIndexPath!.row, 0)
//          
            delegate?.onRouteChange(index: visibleIndexPath!.row)
        //}
             
        //}
       
//        guard let routes = response?.routes else { return }
//        print("Route BottomVC",routes)
//        if(routes[visibleIndexPath!.row] != route)
//        {
//
//            print("Index Route BottomVC",visibleIndexPath?.row)
//            delegate?.onRouteChange(index: visibleIndexPath!.row)
//        }
        
        
        
        
//        guard let centeredPage = centeredCollectionViewFlowLayout.currentCenteredPage else {
//            return
//        }
//
//        if centeredPage != currentCenteredPage && centeredPage < (response?.routes!.count)! {
//            currentCenteredPage = centeredPage
//            delegate?.onRouteChange(index: centeredPage)
//        }
        
/*
            let point = view.convert(collectionView.center, to: collectionView)

            guard
                let indexPath = collectionView.indexPathForItem(at: point),
                indexPath.item < (response?.routes!.count)!
            else {
                return
            }

        let route = response?.routes![indexPath.item]
        delegate?.onRouteChange(index: indexPath.row)
*/
    }

    @IBAction func onConfirm(_ sender: Any) {
        if let callback = onConfirmed {
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.routeplanning_lets_go, screenName: ParameterName.routeplanning_screen)
            
            callback()
        }
    }
    
    @IBAction func onCarparks(_ sender: Any) {
        if let callback = onCarparks {
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.routeplanning_carpark_nearby, screenName: ParameterName.routeplanning_screen)
            
            
            callback()
        }
    }
    
    @IBAction func shareMyETA(_ sender: Any) {
        
        if let callback = onShareETA {
            
            
            callback()
        }
        
    }
}

extension RoutePlanningBottomVC: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return response?.routes?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: RouteCell.self), for: indexPath) as! RouteCell
        
        //Adding current Locale
        //let locale = Locale(identifier: "en_SG")
        
        //Setting locale to DistanceFormatter inside SDK
        //DistanceFormatter().locale = locale
        
        //Converting route distance value to Km using DistanceFormatter in String Format
        
        print(response?.routes![indexPath.row].distance)
        
        if (response?.routes![indexPath.row].distance)! > 1000 {
            cell.distance.set("\(String(format: "%.1f", (response?.routes![indexPath.row].distance)!/1000))", " km")
        } else {
            let roundedDistance = response?.routes![indexPath.row].distance.rounded()
            cell.distance.set("\(Int(roundedDistance!))", " m")
        }
//
//        let distanceValue = DistanceFormatter().string(from: (response?.routes![indexPath.row].distance)!)
//        
//        
//        //Coverting distance string value to array by spearating with " ", to separate Value and Units
//        let distanceArray = distanceValue.components(separatedBy: " ")
//        
//        //Showing the distance value from array
//        let distanceString = distanceArray[0].contains(".") ? distanceArray[0]: "\(distanceArray[0]).0"
//        cell.distance.set(distanceString, " \(distanceArray[1])")
        
        //Setting duration by converting route.expectedTravelTime/60
        cell.duration.set((response?.routes![indexPath.row].expectedTravelTime.stringTime)!, " \((response?.routes![indexPath.row].expectedTravelTime.stringTimeUnit)!)")

        
        //This code is calculate arrivale time from expected travel time
        /*let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = .short
        guard let arrivalDate = NSCalendar.current.date(byAdding: .second, value: Int((response?.routes![indexPath.row].expectedTravelTime)!), to: Date()) else {preconditionFailure("not found") }
        let arrivalTimeValue = dateFormatter.string(from: arrivalDate)
        let arrivalTimeValueArray = arrivalTimeValue.components(separatedBy: " ")
        if(arrivalTimeValueArray.count > 1)
        {
            cell.arrivalTime.set(arrivalTimeValueArray[0], " \(arrivalTimeValueArray[1].lowercased())")
        }
        else
        {
            cell.arrivalTime.set(arrivalTimeValueArray[0], "")
        }*/
        

        let estimatedTime = estimatedArrivalTime((response?.routes![indexPath.row].expectedTravelTime)!)
        cell.arrivalTime.set(estimatedTime.time, estimatedTime.format)
        cell.erpPrice.text = erpPrice
        
//        cell.onConfirmed = { [weak self] in
//            self?.delegate?.onRouteConfirmed(index: indexPath.row)
//        }
//        cell.onCarparks = { [weak self] in
//            guard let self = self else { return }
//            if self.needToReset {
//                self.delegate?.onReset()
//            } else {
//                self.delegate?.onCarparksNearby(index: indexPath.row)
//            }
//        }
        
        self.onConfirmed = { [weak self] in
            guard let self = self else { return }
            self.delegate?.onRouteConfirmed(index: indexPath.row)
        }
        self.onShareETA = { [weak self] in
            guard let self = self else { return }
            self.delegate?.onShareMyETA(index: indexPath.row)
        }
        self.onCarparks = { [weak self] in
            guard let self = self else { return }
            if self.needToReset {
                self.delegate?.onReset()
            } else {
                self.delegate?.onCarparksNearby(index: indexPath.row)
            }
        }

        return cell
    }
    
    // TODO: Move as a common function
    private func estimatedArrivalTime(_ duration: TimeInterval) -> (time: String,format: String) {
        let arrivalDate = Date(timeInterval: duration, since: Date())
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: arrivalDate)
        let t = hour > 12 ? hour - 12: hour
        let hourString = t < 10 ? "0\(t)" : t.description
        let minute = calendar.component(.minute, from: arrivalDate)
        let minString = minute < 10 ? "0\(minute)" : minute.description
        return (time: "\(hourString):\(minString)", format: hour >= 12 ? " pm" : " am")
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

      // check if the currentCenteredPage is not the page that was touched
      let currentCenteredPage = centeredCollectionViewFlowLayout.currentCenteredPage
      if currentCenteredPage != indexPath.row {
        // trigger a scrollToPage(index: animated:)
        centeredCollectionViewFlowLayout.scrollToPage(index: indexPath.row, animated: true)
      }
    }
    
//    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
//        if collectionView == collectionView {
//
//            guard let visible = collectionView.visibleCells.first else { return }
//            guard let index = collectionView.indexPath(for: visible)?.row else { return }
//
//            print(index)
//            //guard let routes = response?.routes else { return }
//
//            delegate?.onRouteChange(index: index)
//
//        }
//    }
    
    
}


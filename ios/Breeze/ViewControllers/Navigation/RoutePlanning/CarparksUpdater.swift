//
//  CarparksUpdater.swift
//  Breeze
//
//  Created by Zhou Hao on 27/7/21.
//

import Foundation
@_spi(Experimental) import MapboxMaps
import MapboxCoreMaps
import Turf
import CoreLocation
import Amplify
import UIKit
import SwiftyBeaver

struct CarparkValues {
    static let carparkBackgroundImg = "carparkBackgroundImg"
    static let cheapestCarparkBackgroundImg = "cheapestCarparkBackgroundImg"
    static let carparkPriceGeoJsonId = "carparkGeoJsonId-price"
    static let carparkPriceSymbolLayerId = "carparkSymbolLayerId-price"
    
    static let carparkSymbolLayerClusterId = "carparkSymbolLayerId-cluster"
    static let carparkSymbolLayerUnClusterId = "carparkSymbolLayerId-uncluster"
    static let carparkClusterPriceSymbolLayerId = "carparkSymbolLayerId-clusterprice"
    static let carparkUnClusterPriceSymbolLayerId = "carparkSymbolLayerId-unclusterprice"
    
    static let carparkSymbolLayerIdIcon = "carparkSymbolLayerId-icon"
    static let carparkGeoJsonIdIcon = "carpark-GeoJsonId-icon"
    
    static let defaultCarparkSymbolLayerIdIcon = "deafultCarparkSymbolLayerId-icon"
    static let deafaultCarparkGeoJsonIdIcon = "default-carpark-GeoJsonId-icon"
    static let defaultCarparkPriceGeoJsonId = "defaultCarparkGeoJsonId-price"
    static let defaultCarparkPriceSymbolLayerId = "defaultCarparkSymbolLayerId-price"
    
    static let carparkImageName: String = "carparkType"
    
    static let destinationLayerIdentifier: String = "id_dest"
}

extension MapView {
    
    /*
     convert Carpark to Turf.Feature
     */
    func getCarparkFeature(carpark: Carpark, ignoreDestination: Bool = false, selected: Bool) -> Turf.Feature {
        var feature = Turf.Feature(geometry: .point(Point(CLLocationCoordinate2D(latitude: carpark.lat ?? 0, longitude: carpark.long ?? 0))))
        let cost = carpark.currentHrRate == nil ? "------" : (carpark.currentHrRate!.oneHrRate == nil ? "------" : carpark.currentHrRate!.oneHrRate! > 0 ? String(format: "$%.2f", carpark.currentHrRate!.oneHrRate!) : "Free")
        let colorCodeAndImageName = getcarparkImageName(carpark: carpark, selected: carpark.selected, ignoreDestination: ignoreDestination)
        feature.properties = [
            "distance": .number(Double(carpark.distance ?? 0)),
            "iconOffset": .array(carpark.hasVoucher ? [0, -65] : [0, -72]),
            "textOffset": .array(carpark.hasVoucher ? [0, -2.3] : [0, -2.7]),
            "cost": .string(cost),
            "isCheapest": .boolean(carpark.isCheapest ?? false),
            "carparkId": .string(carpark.id ?? ""),
            "imageName": .string(colorCodeAndImageName.0),
            "clusterColor":.string(colorCodeAndImageName.1)
        ]
        return feature
    }
    
    /*
     Get all carpark image name from resource
     Image name from 1 to 5 type
     */
    func getImagesName() -> [String] {
        let array = 1...5
        var images: [String] = []
        
        // Destination carpark
        images.append(contentsOf: array.map { i in
            return getImageName(name: CarparkValues.carparkImageName + String(i), isDestination: true, isSelected: true).0
        })
        
        images.append(contentsOf: array.map { i in
            return getImageName(name: CarparkValues.carparkImageName + String(i), isDestination: true, isSelected: false).0
        })
        
        // Destination carpark voucher
        images.append(contentsOf: array.map { i in
            return getImageName(name: CarparkValues.carparkImageName + String(i), isDestination: true, isSelected: false, hasVoucher: true).0
        })
        
        // Carpark selected
        images.append(contentsOf: array.map { i in
            return getImageName(name: CarparkValues.carparkImageName + String(i), isDestination: false, isSelected: true).0
        })
        
        // Carpark unselected
        images.append(contentsOf: array.map { i in
            return getImageName(name: CarparkValues.carparkImageName + String(i), isDestination: false, isSelected: false).0
        })
        
        // Carpark voucher unselected
        images.append(contentsOf: array.map { i in
            return getImageName(name: CarparkValues.carparkImageName + String(i), isDestination: false, isSelected: false, hasVoucher: true).0
        })
        
        // Carpark voucher selected
        images.append(contentsOf: array.map { i in
            return getImageName(name: CarparkValues.carparkImageName + String(i), isDestination: false, isSelected: true, hasVoucher: true).0
        })
        
        return images
    }
    
    /*
     Add all image resource to mapbox Map
     add once time
     */
    func addCarparkResource() {
        
        if Thread.isMainThread {
            addCarparkResourceInMain()
        } else {
            DispatchQueue.main.async {
                self.addCarparkResourceInMain()
            }
        }
    }
    
    private func addCarparkResourceInMain() {
        let traitCollection = getTraitCollection()
        let images = getImagesName()
        
        do {
            for imageName in images {
                if let image = UIImage(named: imageName, in:nil, compatibleWith: traitCollection) {
                    try self.mapboxMap.style.addImage(image, id: imageName,stretchX: [],stretchY: [])
                }
            }
        } catch {
            NSLog("Failed to add carpark layer with error: \(error.localizedDescription).")
        }
    }
    
    /*
     Remove carpark image resouce from map style
     Don't need call now
     */
    func removeCarparkResource() {
        let images = getImagesName()
        do {
            for imageName in images {
                try self.mapboxMap.style.removeImage(withId: imageName)
            }
        } catch {
            NSLog("Failed to add carpark layer with error: \(error.localizedDescription).")
        }
    }
    
    /*
       Adding carparks as a clustered layer
       only with circle layer
     */
    
    func addCarParksClusterLayerToMap(featureCollection: Turf.FeatureCollection,
                                      belowPuck: Bool,cpType:String){
        if Thread.isMainThread {
            do {
               
               // let image = self.mapboxMap.style.image(withId: type)
                var geoJSONSource = GeoJSONSource()
                geoJSONSource.cluster = true
                geoJSONSource.clusterMaxZoom = Values.clusterMaxZoom
                geoJSONSource.clusterRadius = Values.clusterRadius
                geoJSONSource.data = .featureCollection(featureCollection)
                
                let clusterColor = Exp(.get){
                    "cluserColor"
                }
                
                var clusteredCPLayer = self.createClusteredCPLayer(type: cpType)
                clusteredCPLayer.source = "\(CarparkValues.carparkGeoJsonIdIcon)-\(cpType)"
                clusteredCPLayer.circleColor = .constant(.init(getRGBCPColor(type: cpType)))
                
                var unClusteredSymboLayer = self.createUnclusteredCPLayer(type: cpType)
                unClusteredSymboLayer.source = "\(CarparkValues.carparkGeoJsonIdIcon)-\(cpType)"
                
                // Filter out clusters by checking for `point_count`.
                unClusteredSymboLayer.filter = Exp(.not) {
                    Exp(.has) { "point_count" }
                }
                
                // Add the source and style layers to the map style.
                try self.mapboxMap.style.addSource(geoJSONSource, id: "\(CarparkValues.carparkGeoJsonIdIcon)-\(cpType)")
                try self.mapboxMap.style.addLayer(clusteredCPLayer,layerPosition: .below("puck"))

                try self.mapboxMap.style.addLayer(unClusteredSymboLayer, layerPosition: .below("puck"))
                
            }
            catch (let error) {
                
                print("Car Park cluster failed",error.localizedDescription)
            }
            return
        }
        DispatchQueue.main.async {
            do {
               
               // let image = self.mapboxMap.style.image(withId: type)
                var geoJSONSource = GeoJSONSource()
                geoJSONSource.cluster = true
                geoJSONSource.clusterMaxZoom = Values.clusterMaxZoom
                geoJSONSource.clusterRadius = Values.clusterRadius
                geoJSONSource.data = .featureCollection(featureCollection)
                
                let clusterColor = Exp(.get){
                    "cluserColor"
                }
                
                var clusteredCPLayer = self.createClusteredCPLayer(type: cpType)
                clusteredCPLayer.source = "\(CarparkValues.carparkGeoJsonIdIcon)-\(cpType)"
                clusteredCPLayer.circleColor = .constant(.init(getRGBCPColor(type: cpType)))
                
                var unClusteredSymboLayer = self.createUnclusteredCPLayer(type: cpType)
                unClusteredSymboLayer.source = "\(CarparkValues.carparkGeoJsonIdIcon)-\(cpType)"
                
                // Filter out clusters by checking for `point_count`.
                unClusteredSymboLayer.filter = Exp(.not) {
                    Exp(.has) { "point_count" }
                }
                
                // Add the source and style layers to the map style.
                try self.mapboxMap.style.addSource(geoJSONSource, id: "\(CarparkValues.carparkGeoJsonIdIcon)-\(cpType)")
                try self.mapboxMap.style.addLayer(clusteredCPLayer,layerPosition: .below("puck"))

                try self.mapboxMap.style.addLayer(unClusteredSymboLayer, layerPosition: .below("puck"))
                
            }
            catch (let error) {
                
                print("Car Park cluster failed",error.localizedDescription)
            }
        }
    }
    
    /*
       Adding carparks price as a clustered layer
       only with circle layer
     */
    
    func addCarParksPriceClusterLayerToMap(featureCollection: Turf.FeatureCollection,
                                      belowPuck: Bool){
        if Thread.isMainThread {
            do {
               
               // let image = self.mapboxMap.style.image(withId: type)
                var geoJSONSource = GeoJSONSource()
                geoJSONSource.cluster = true
                geoJSONSource.clusterMaxZoom = Values.clusterMaxZoom
                geoJSONSource.clusterRadius = Values.clusterRadius
                geoJSONSource.data = .featureCollection(featureCollection)
                
                var clusteredCPLayer = self.createClusteredCPPriceLayer()
                clusteredCPLayer.source = CarparkValues.carparkPriceGeoJsonId
                
                var unClusteredSymboLayer = self.createUnclusteredCPPriceLayer()
                unClusteredSymboLayer.source = CarparkValues.carparkPriceGeoJsonId
                
                // Filter out clusters by checking for `point_count`.
                unClusteredSymboLayer.filter = Exp(.not) {
                    Exp(.has) { "point_count" }
                }
                
                // Add the source and style layers to the map style.
                try self.mapboxMap.style.addSource(geoJSONSource, id: CarparkValues.carparkPriceGeoJsonId)
                try self.mapboxMap.style.addLayer(clusteredCPLayer,layerPosition: .below("puck"))

                try self.mapboxMap.style.addLayer(unClusteredSymboLayer, layerPosition: .below("puck"))
                
            }
            catch (let error) {
                
                print("Car Park cluster failed",error.localizedDescription)
            }
            return
        }
        DispatchQueue.main.async {
            
            do {
               
               // let image = self.mapboxMap.style.image(withId: type)
                var geoJSONSource = GeoJSONSource()
                geoJSONSource.cluster = true
                geoJSONSource.clusterMaxZoom = Values.clusterMaxZoom
                geoJSONSource.clusterRadius = Values.clusterRadius
                geoJSONSource.data = .featureCollection(featureCollection)
                
                var clusteredCPLayer = self.createClusteredCPPriceLayer()
                clusteredCPLayer.source = CarparkValues.carparkPriceGeoJsonId
                
                var unClusteredSymboLayer = self.createUnclusteredCPPriceLayer()
                unClusteredSymboLayer.source = CarparkValues.carparkPriceGeoJsonId
                
                // Filter out clusters by checking for `point_count`.
                unClusteredSymboLayer.filter = Exp(.not) {
                    Exp(.has) { "point_count" }
                }
                
                // Add the source and style layers to the map style.
                try self.mapboxMap.style.addSource(geoJSONSource, id: CarparkValues.carparkPriceGeoJsonId)
                try self.mapboxMap.style.addLayer(clusteredCPLayer,layerPosition: .below("puck"))

                try self.mapboxMap.style.addLayer(unClusteredSymboLayer, layerPosition: .below("puck"))
                
            }
            catch (let error) {
                
                print("Car Park cluster failed",error.localizedDescription)
            }
        }
    }
    
    /*
       Adding cluster circle layer
     */
    
    func createClusteredCPLayer(type:String = "") -> CircleLayer{
        
        var clusteredLayer = CircleLayer(id: type == "" ? CarparkValues.carparkSymbolLayerClusterId : "\(CarparkValues.carparkSymbolLayerClusterId)-\(type)")

        // Filter out unclustered features by checking for `point_count`. This
        // is added to clusters when the cluster is created. If your source
        // data includes a `point_count` property, consider checking
        // for `cluster_id`.
        clusteredLayer.filter = Exp(.has) {
            "point_count"
        }
        clusteredLayer.circleRadius = .constant(Values.circleRadius)

        return clusteredLayer
    }
    
    /*
       Adding uncluster carpark symbol layer
     */
    
    func createUnclusteredCPLayer(type:String = "") -> SymbolLayer {
        
        // Create a symbol layer to represent the points that aren't clustered.
        var symbolLayer = SymbolLayer(id: type == "" ? CarparkValues.carparkSymbolLayerUnClusterId : "\(CarparkValues.carparkSymbolLayerUnClusterId)-\(type)")
        
        let iconImage = Exp(.get){
            "imageName"
        }
        
        let iconSize = Exp(.interpolate){
            Exp(.linear)
            Exp(.zoom)
            0
            0
            9
            1.0
            10.9
            1.0
            11
            1.0
            22
            1.0
          }
        
        
        symbolLayer.symbolSortKey = .expression(
            Exp(.get) { "distance" }
        )
        //symbolLayer.minZoom = 11
        symbolLayer.iconSize = .expression(iconSize)
        symbolLayer.iconAllowOverlap = .constant(true)
        symbolLayer.iconImage = .expression(iconImage)
        symbolLayer.iconAnchor = .constant(.center)
        symbolLayer.iconOffset = .constant([0, 0])
        
        return symbolLayer
        
    }
    
    /*
       Adding cluster carpark price layer
     */
    
    func createClusteredCPPriceLayer() -> CircleLayer {
        
        var clusteredLayer = CircleLayer(id: CarparkValues.carparkClusterPriceSymbolLayerId)

        // Filter out unclustered features by checking for `point_count`. This
        // is added to clusters when the cluster is created. If your source
        // data includes a `point_count` property, consider checking
        // for `cluster_id`.
        clusteredLayer.filter = Exp(.has) { "point_count" }

        
        clusteredLayer.circleColor = .constant(.init(UIColor.clear))
        clusteredLayer.circleRadius = .constant(Values.circleRadius)

        return clusteredLayer
    }
    
    /**
       Create unClustered CarPark price layer
     */
    
    func createUnclusteredCPPriceLayer() -> SymbolLayer {
        
        // TODO: To consider use system traitColletion directly?
        var traitCollection = UITraitCollection(userInterfaceStyle: .light)
        if(Settings.shared.theme == 0){

            // This is get mobile's traitCollection
            if(appDelegate().isDarkMode()){

                traitCollection = UITraitCollection(userInterfaceStyle: .dark)

            }

        }
        else{

            traitCollection = UITraitCollection(userInterfaceStyle: UIUserInterfaceStyle(rawValue: Settings.shared.theme) ?? .unspecified)
        }

        do {
            
            try self.mapboxMap.style.addImage(UIImage(named: "carparkSymbol", in:nil, compatibleWith: traitCollection)!, id: CarparkValues.carparkBackgroundImg,stretchX: [],stretchY: [])
            try self.mapboxMap.style.addImage(UIImage(named: "cheapestCarparkSymbol", in:nil, compatibleWith: traitCollection)!, id: CarparkValues.cheapestCarparkBackgroundImg,stretchX: [],stretchY: [])
        }
        catch(_){
            
        }
        
        let cost = Exp(.get){
            "cost"
        }
        
        let iconImage = Exp(.switchCase) { // Switching on a value
            Exp(.eq) { // Evaluates if conditions are equal
                Exp(.get) { "isCheapest" }
                true
            }
            CarparkValues.cheapestCarparkBackgroundImg
            Exp(.eq) {
                Exp(.get) { "isCheapest" }
                false
            }
            CarparkValues.carparkBackgroundImg
            ""
        }
        
        
        
        let iconSize = Exp(.interpolate){
            Exp(.linear)
            Exp(.zoom)
            0
            0
            7
            0
            13
            0.6
            15
            0.8
            22
            0.8
        }
        
        let textSize = Exp(.interpolate){
            Exp(.linear)
            Exp(.zoom)
            0
            0
            7
            0
            13
            12
            15
            16
            22
            16
        }
        
        let textColor = Exp(.switchCase) { // Switching on a value
            Exp(.eq) { // Evaluates if conditions are equal
                Exp(.get) { "isCheapest" }
                true
            }
            UIColor.white
            Exp(.eq) {
                Exp(.get) { "isCheapest" }
                false
            }
            (UIColor(named: "carparkPriceTextColor") ?? (appDelegate().isDarkMode() ? UIColor.white : UIColor.black)).resolvedColor(with: traitCollection)
            UIColor.black
        }
        
        let iconOffset = Exp(.get){
            "iconOffset"
        }
        
        let textOffset = Exp(.get){
            "textOffset"
        }
        
        var symbolLayer = SymbolLayer(id: CarparkValues.carparkUnClusterPriceSymbolLayerId)
        symbolLayer.symbolSortKey = .expression(
            Exp(.get) { "distance" }
        )
        //symbolLayer.minZoom = 11
        symbolLayer.iconSize = .expression(iconSize)
        symbolLayer.iconAllowOverlap = .constant(true)
        symbolLayer.iconImage = .expression(iconImage)
        symbolLayer.iconAnchor = .constant(.top)
        symbolLayer.iconOffset = .expression(iconOffset)
        symbolLayer.textField = .expression(cost)
        symbolLayer.textFont = .constant([fontFamilySFProFromStudio.Regular])
        symbolLayer.textSize = .expression(textSize)
        symbolLayer.textColor = .expression(textColor)
        symbolLayer.textAllowOverlap = .constant(true)
        symbolLayer.textAnchor = .constant(.center)
        symbolLayer.textOffset = .expression(textOffset)
        
        return symbolLayer
    }
    
    /*
     Add carparks feature collection to map
     Add carpark icon only
     */
    func addCarparksSymbolToMap(featureCollection: Turf.FeatureCollection,
                                belowPuck: Bool) {
        if Thread.isMainThread {
            do {
                
                // Create a GeoJSON data source.
                var geoJSONSource = GeoJSONSource()
                geoJSONSource.data = .featureCollection(featureCollection)
                
                var symbolLayer = self.createUnclusteredCPLayer()
                symbolLayer.source = CarparkValues.carparkGeoJsonIdIcon

                try self.mapboxMap.style.addSource(geoJSONSource, id: CarparkValues.carparkGeoJsonIdIcon)
                // #197 - add layer persistently
                try self.mapboxMap.style.addPersistentLayer(with: symbolLayer.jsonObject(),
                                                            layerPosition: belowPuck ? .below("puck") : .above(Constants.Layer.illegalparkingcamera))
                
            } catch {
                SwiftyBeaver.debug("addCarparksSymbolToMap failed: \(error.localizedDescription)")
                NSLog("Failed to add carpark layer with error: \(error.localizedDescription).")
            }
            return
        }
        DispatchQueue.main.async {
            do {
                
                // Create a GeoJSON data source.
                var geoJSONSource = GeoJSONSource()
                geoJSONSource.data = .featureCollection(featureCollection)
                
                var symbolLayer = self.createUnclusteredCPLayer()
                symbolLayer.source = CarparkValues.carparkGeoJsonIdIcon
                
                try self.mapboxMap.style.addSource(geoJSONSource, id: CarparkValues.carparkGeoJsonIdIcon)
                // No need to add above illegalparkingcamera
                // #197 - add layer persistently
                try self.mapboxMap.style.addPersistentLayer(with: symbolLayer.jsonObject(),
                                                            layerPosition: belowPuck ? .below("puck") : .above(Constants.Layer.illegalparkingcamera))
            } catch {
                SwiftyBeaver.debug("addCarparksSymbolToMap failed: \(error.localizedDescription)")
                NSLog("Failed to add carpark layer with error: \(error.localizedDescription).")
            }
        }
    }
    
    /**
        If profile is selected adding default carparks
     */
    func addDefaultProfileCarParks( carparks: [Carpark], showOnlyWithCostSymbol: Bool = false, belowPuck: Bool = true, ignoreDestination: Bool = false){
        
        var filteredCarparks = [Carpark]()
        if showOnlyWithCostSymbol {
            // filter out the price not available or 0
            filteredCarparks = carparks.filter { carpark in
                guard let rate = carpark.currentHrRate, let oneHrRate = rate.oneHrRate else {
                    return false
                }
                return oneHrRate >= 0
            }
        } else {
            filteredCarparks = carparks
        }
        
        let carparkFilterredFeatures: [Turf.Feature] = filteredCarparks.map { carpark in
            return getCarparkFeature(carpark: carpark, ignoreDestination: ignoreDestination, selected: false)
        }
        
        let carparkFeatures: [Turf.Feature] = carparks.map { carpark in
            return getCarparkFeature(carpark: carpark, ignoreDestination: ignoreDestination, selected: false)
        }
        let collection = Turf.FeatureCollection(features: carparkFeatures)
        let collectionFilterred = Turf.FeatureCollection(features: carparkFilterredFeatures)
        
        self.addDefaultCarparksSymbolToMap(featureCollection: collection, belowPuck: belowPuck)
        self.addDefaultCarparkLayer(collectionFilterred, belowPuck: belowPuck)
    }
    
    /*
     Add default carparks feature collection to map
     Add carpark icon only
     */
    func addDefaultCarparksSymbolToMap(featureCollection: Turf.FeatureCollection,
                                belowPuck: Bool) {
        if Thread.isMainThread {
            do {
                
                // Create a GeoJSON data source.
                var geoJSONSource = GeoJSONSource()
                geoJSONSource.data = .featureCollection(featureCollection)
                
                var symbolLayer = SymbolLayer(id: CarparkValues.defaultCarparkSymbolLayerIdIcon)
                symbolLayer.source = CarparkValues.deafaultCarparkGeoJsonIdIcon
                let iconImage = Exp(.get){
                    "imageName"
                }
                
                let iconSize = Exp(.interpolate){
                    Exp(.linear)
                    Exp(.zoom)
                    0
                    0
                    9
                    1.0
                    10.9
                    1.0
                    11
                    1.0
                    22
                    1.0
                  }
                
                
                symbolLayer.symbolSortKey = .expression(
                    Exp(.get) { "distance" }
                )
                //symbolLayer.minZoom = 11
                symbolLayer.iconSize = .expression(iconSize)
                symbolLayer.iconAllowOverlap = .constant(true)
                symbolLayer.iconImage = .expression(iconImage)
                symbolLayer.iconAnchor = .constant(.center)
                symbolLayer.iconOffset = .constant([0, 0])
                

                try self.mapboxMap.style.addSource(geoJSONSource, id: CarparkValues.deafaultCarparkGeoJsonIdIcon)
                // No need to add above illegalparkingcamera
                // #197 - add layer persistently
                try self.mapboxMap.style.addPersistentLayer(with: symbolLayer.jsonObject(),
                                                            layerPosition: belowPuck ? .below("puck") : .above(Constants.Layer.illegalparkingcamera))
                
            } catch {
                NSLog("Failed to add carpark layer with error: \(error.localizedDescription).")
            }
            return
        }
        DispatchQueue.main.async {
            do {
                
                // Create a GeoJSON data source.
                var geoJSONSource = GeoJSONSource()
                geoJSONSource.data = .featureCollection(featureCollection)
                
                var symbolLayer = SymbolLayer(id: CarparkValues.defaultCarparkSymbolLayerIdIcon)
                symbolLayer.source = CarparkValues.deafaultCarparkGeoJsonIdIcon
                let iconImage = Exp(.get){
                    "imageName"
                }
                
                let iconSize = Exp(.interpolate){
                    Exp(.linear)
                    Exp(.zoom)
                    0
                    0
                    9
                    1.0
                    10.9
                    1.0
                    11
                    1.0
                    22
                    1.0
                  }
                
                
                symbolLayer.symbolSortKey = .expression(
                    Exp(.get) { "distance" }
                )
                //symbolLayer.minZoom = 11
                symbolLayer.iconSize = .expression(iconSize)
                symbolLayer.iconAllowOverlap = .constant(true)
                symbolLayer.iconImage = .expression(iconImage)
                symbolLayer.iconAnchor = .constant(.center)
                symbolLayer.iconOffset = .constant([0, 0])
                

                try self.mapboxMap.style.addSource(geoJSONSource, id: CarparkValues.deafaultCarparkGeoJsonIdIcon)
                // No need to add above illegalparkingcamera
                // #197 - add layer persistently
                try self.mapboxMap.style.addPersistentLayer(with: symbolLayer.jsonObject(),
                                                            layerPosition: belowPuck ? .below("puck") : .above(Constants.Layer.illegalparkingcamera))
                
            } catch {
                NSLog("Failed to add carpark layer with error: \(error.localizedDescription).")
            }
        }
    }
    
    /*
     Add default carpark price only
     */
    /// only show carpark price > 0 if `showOnlyWithCostSymbol = true
    func addDefaultCarparkLayer(_ featureCollection: Turf.FeatureCollection, belowPuck: Bool = true) {
        if Thread.isMainThread {
            do {
                
                // TODO: TO BE IMPLEMENTED by showing carpark icon in symbol
                
                var traitCollection = UITraitCollection(userInterfaceStyle: .light)
                if(Settings.shared.theme == 0){
                    
                    if(appDelegate().isDarkMode()){
                        
                        traitCollection = UITraitCollection(userInterfaceStyle: .dark)
                        
                    }
                    
                }
                else{
                    
                    traitCollection = UITraitCollection(userInterfaceStyle: UIUserInterfaceStyle(rawValue: Settings.shared.theme) ?? .unspecified)
                }
                
                try self.mapboxMap.style.addImage(UIImage(named: "carparkSymbol", in:nil, compatibleWith: traitCollection)!, id: CarparkValues.carparkBackgroundImg,stretchX: [],stretchY: [])
                try self.mapboxMap.style.addImage(UIImage(named: "cheapestCarparkSymbol", in:nil, compatibleWith: traitCollection)!, id: CarparkValues.cheapestCarparkBackgroundImg,stretchX: [],stretchY: [])
                
                // Create a GeoJSON data source.
                var geoJSONSource = GeoJSONSource()
                geoJSONSource.data = .featureCollection(featureCollection)
                
                let cost = Exp(.get){
                    "cost"
                }
                
                let iconImage = Exp(.switchCase) { // Switching on a value
                    Exp(.eq) { // Evaluates if conditions are equal
                        Exp(.get) { "isCheapest" }
                        true
                    }
                    CarparkValues.cheapestCarparkBackgroundImg
                    Exp(.eq) {
                        Exp(.get) { "isCheapest" }
                        false
                    }
                    CarparkValues.carparkBackgroundImg
                    ""
                }
                
                let iconSize = Exp(.interpolate){
                    Exp(.linear)
                    Exp(.zoom)
                    0
                    0
                    7
                    0
                    13
                    0.6
                    15
                    0.8
                    22
                    0.8
                }
                
                let textSize = Exp(.interpolate){
                    Exp(.linear)
                    Exp(.zoom)
                    0
                    0
                    7
                    0
                    13
                    12
                    15
                    16
                    22
                    16
                }
                
                let textColor = Exp(.switchCase) { // Switching on a value
                    Exp(.eq) { // Evaluates if conditions are equal
                        Exp(.get) { "isCheapest" }
                        true
                    }
                    UIColor.white
                    Exp(.eq) {
                        Exp(.get) { "isCheapest" }
                        false
                    }
                    UIColor(named: "carparkPriceTextColor")!.resolvedColor(with: traitCollection)
                    UIColor.black
                }
                
                var symbolLayer = SymbolLayer(id: CarparkValues.defaultCarparkPriceSymbolLayerId)
                symbolLayer.source = CarparkValues.defaultCarparkPriceGeoJsonId
                symbolLayer.symbolSortKey = .expression(
                    Exp(.get) { "distance" }
                )
                //symbolLayer.minZoom = 11
                symbolLayer.iconSize = .expression(iconSize)
                symbolLayer.iconAllowOverlap = .constant(true)
                symbolLayer.iconImage = .expression(iconImage)
                symbolLayer.iconAnchor = .constant(.top)
                symbolLayer.iconOffset = .constant([0, -72])
                symbolLayer.textField = .expression(cost)
                symbolLayer.textFont = .constant([fontFamilySFProFromStudio.Regular])
                symbolLayer.textSize = .expression(textSize)
                symbolLayer.textColor = .expression(textColor)
                symbolLayer.textAllowOverlap = .constant(true)
                symbolLayer.textAnchor = .constant(.center)
                symbolLayer.textOffset = .constant([0,-2.7])
                
                try self.mapboxMap.style.addSource(geoJSONSource, id: CarparkValues.defaultCarparkPriceGeoJsonId)
                // No need to add above illegalparkingcamera
                // #197 - add layer persistently
                try self.mapboxMap.style.addPersistentLayer(with: symbolLayer.jsonObject(), layerPosition: belowPuck ? .below("puck") : .above(Constants.Layer.illegalparkingcamera))
                
            } catch {
                NSLog("Failed to add carpark layer with error: \(error.localizedDescription).")
            }
            return
        }
        DispatchQueue.main.async {
            do {
                
                // TODO: TO BE IMPLEMENTED by showing carpark icon in symbol
                
                var traitCollection = UITraitCollection(userInterfaceStyle: .light)
                if(Settings.shared.theme == 0){
                    
                    if(appDelegate().isDarkMode()){
                        
                        traitCollection = UITraitCollection(userInterfaceStyle: .dark)
                        
                    }
                    
                }
                else{
                    
                    traitCollection = UITraitCollection(userInterfaceStyle: UIUserInterfaceStyle(rawValue: Settings.shared.theme) ?? .unspecified)
                }
                
                try self.mapboxMap.style.addImage(UIImage(named: "carparkSymbol", in:nil, compatibleWith: traitCollection)!, id: CarparkValues.carparkBackgroundImg,stretchX: [],stretchY: [])
                try self.mapboxMap.style.addImage(UIImage(named: "cheapestCarparkSymbol", in:nil, compatibleWith: traitCollection)!, id: CarparkValues.cheapestCarparkBackgroundImg,stretchX: [],stretchY: [])
                
                // Create a GeoJSON data source.
                var geoJSONSource = GeoJSONSource()
                geoJSONSource.data = .featureCollection(featureCollection)
                
                let cost = Exp(.get){
                    "cost"
                }
                
                let iconImage = Exp(.switchCase) { // Switching on a value
                    Exp(.eq) { // Evaluates if conditions are equal
                        Exp(.get) { "isCheapest" }
                        true
                    }
                    CarparkValues.cheapestCarparkBackgroundImg
                    Exp(.eq) {
                        Exp(.get) { "isCheapest" }
                        false
                    }
                    CarparkValues.carparkBackgroundImg
                    ""
                }
                
                let iconSize = Exp(.interpolate){
                    Exp(.linear)
                    Exp(.zoom)
                    0
                    0
                    7
                    0
                    13
                    0.6
                    15
                    0.8
                    22
                    0.8
                }
                
                let textSize = Exp(.interpolate){
                    Exp(.linear)
                    Exp(.zoom)
                    0
                    0
                    7
                    0
                    13
                    12
                    15
                    16
                    22
                    16
                }
                
                let textColor = Exp(.switchCase) { // Switching on a value
                    Exp(.eq) { // Evaluates if conditions are equal
                        Exp(.get) { "isCheapest" }
                        true
                    }
                    UIColor.white
                    Exp(.eq) {
                        Exp(.get) { "isCheapest" }
                        false
                    }
                    UIColor(named: "carparkPriceTextColor")!.resolvedColor(with: traitCollection)
                    UIColor.black
                }
                
                var symbolLayer = SymbolLayer(id: CarparkValues.defaultCarparkPriceSymbolLayerId)
                symbolLayer.source = CarparkValues.defaultCarparkPriceGeoJsonId
                symbolLayer.symbolSortKey = .expression(
                    Exp(.get) { "distance" }
                )
                //symbolLayer.minZoom = 11
                symbolLayer.iconSize = .expression(iconSize)
                symbolLayer.iconAllowOverlap = .constant(true)
                symbolLayer.iconImage = .expression(iconImage)
                symbolLayer.iconAnchor = .constant(.top)
                symbolLayer.iconOffset = .constant([0, -72])
                symbolLayer.textField = .expression(cost)
                symbolLayer.textFont = .constant([fontFamilySFProFromStudio.Regular])
                symbolLayer.textSize = .expression(textSize)
                symbolLayer.textColor = .expression(textColor)
                symbolLayer.textAllowOverlap = .constant(true)
                symbolLayer.textAnchor = .constant(.center)
                symbolLayer.textOffset = .constant([0,-2.7])
                
                try self.mapboxMap.style.addSource(geoJSONSource, id: CarparkValues.defaultCarparkPriceGeoJsonId)
                // No need to add above illegalparkingcamera
                // #197 - add layer persistently
                try self.mapboxMap.style.addPersistentLayer(with: symbolLayer.jsonObject(), layerPosition: belowPuck ? .below("puck") : .above(Constants.Layer.illegalparkingcamera))
                
            } catch {
                NSLog("Failed to add carpark layer with error: \(error.localizedDescription).")
            }
        }
    }
    
    /*
     Add carpark to map
     Will add both price and icon
     */
    /// only show carpark price > 0 if `showOnlyWithCostSymbol = true
    func addCarparks(_ carparks: [Carpark], showOnlyWithCostSymbol: Bool = false, belowPuck: Bool = true, ignoreDestination: Bool = false,isCluster:Bool = false) {
        
        // Add carpark icon
        var filteredCarparks = [Carpark]()
        if showOnlyWithCostSymbol {
            // filter out the price not available or 0
            filteredCarparks = carparks.filter { carpark in
                guard let rate = carpark.currentHrRate, let oneHrRate = rate.oneHrRate else {
                    return false
                }
                return oneHrRate >= 0
            }
        } else {
            filteredCarparks = carparks
        }

        let carparkFilterredFeatures: [Turf.Feature] = filteredCarparks.map { carpark in
            return getCarparkFeature(carpark: carpark, ignoreDestination: ignoreDestination, selected: false)
        }
        let collectionFilterred = Turf.FeatureCollection(features: carparkFilterredFeatures)
        
        if(isCluster){
            
            let array = 1...5
            
            for i in array {
                
                if let collection = getCPFeaturesOfType(type: CarparkValues.carparkImageName + String(i), carParks: carparks,ignoreDestination: ignoreDestination){
                    
                    if collection.features.count > 0 {
                        
                        self.addCarParksClusterLayerToMap(featureCollection: collection, belowPuck: belowPuck, cpType: CarparkValues.carparkImageName + String(i))
                    }
                    
                }
            }
            
            self.addCarParksPriceClusterLayerToMap(featureCollection: collectionFilterred, belowPuck: belowPuck)
        }
        else{
            
            let carparkFeatures: [Turf.Feature] = carparks.map { carpark in
                return getCarparkFeature(carpark: carpark, ignoreDestination: ignoreDestination, selected: false)
            }
            let collection = Turf.FeatureCollection(features: carparkFeatures)
            
            addCarparksSymbolToMap(featureCollection: collection, belowPuck: belowPuck)

            addCarparkLayer(collectionFilterred, belowPuck: belowPuck)
        }
    }
    
    /*
     Add carpark price only
     */
    /// only show carpark price > 0 if `showOnlyWithCostSymbol = true
    func addCarparkLayer(_ featureCollection: Turf.FeatureCollection, belowPuck: Bool = true) {
        DispatchQueue.main.async {
            do {
                // Create a GeoJSON data source.
                var geoJSONSource = GeoJSONSource()
                geoJSONSource.data = .featureCollection(featureCollection)
                
                var symbolLayer = self.createUnclusteredCPPriceLayer()
                symbolLayer.source = CarparkValues.carparkPriceGeoJsonId
                
                try self.mapboxMap.style.addSource(geoJSONSource, id: CarparkValues.carparkPriceGeoJsonId)
                
                // #197 - add layer persistently
                try self.mapboxMap.style.addPersistentLayer(with: symbolLayer.jsonObject(), layerPosition: belowPuck ? .below("puck") : .above(Constants.Layer.illegalparkingcamera))
                
            } catch {
                SwiftyBeaver.debug("addCarparkLayer failed \(error.localizedDescription)")
                NSLog("Failed to add carpark layer with error: \(error.localizedDescription).")
            }
        }
    }
    
    /*
       Before removing check if layer exists on the map
     */
    
    func checkIfLayerExists(layerID:String) -> Bool {
        
        return self.mapboxMap.style.layerExists(withId: layerID)
        
    }
    
    /*
       Before removing check if GeoJsonSource exists on the map
     */
    
    func checkIfSourceExists(layerID:String) -> Bool {
        
        return self.mapboxMap.style.sourceExists(withId: layerID)
        
    }
    
    private func doRemoveSourceIds(sourceIds: [String]) {
        do {
            let allSourceIdsInMap = self.mapboxMap.style.allSourceIdentifiers.map { sourceInfo in
                return sourceInfo.id
            }
            
            for id in sourceIds {
                if allSourceIdsInMap.contains(id) {
                    try self.mapboxMap.style.removeSource(withId: id)
                }
            }
        } catch {
            NSLog("Failed to remove source \(sourceIds)")
        }
    }
    
    private func doRemoveLayerIds(layerIds: [String]) {
        let allLayerIdentifierId = self.mapboxMap.style.allLayerIdentifiers.map { identifier in
            return identifier.id
        }
        
        do {
            for id in layerIds {
                if allLayerIdentifierId.contains(id) {
                    try self.mapboxMap.style.removeLayer(withId: id)
                }
            }
        } catch {
            NSLog("Failed to remove layer \(layerIds)")
        }
    }
    
    /*
     Remove all sourceids from map style
     */
    func removeSourceIds(sourceIds: [String]) {
        if Thread.isMainThread {
            doRemoveSourceIds(sourceIds: sourceIds)
        } else {
            DispatchQueue.main.async {
                self.doRemoveSourceIds(sourceIds: sourceIds)
            }
        }
    }
    
    /*
     Remove all layerids from map style
     */
    func removeLayerIds(layerIds: [String]) {
        if Thread.isMainThread {
            doRemoveLayerIds(layerIds: layerIds)
        } else {
            DispatchQueue.main.async {
                self.doRemoveLayerIds(layerIds: layerIds)
            }
        }
    }
    
    /*
     Remove all carparklayer and geoJson resouce
     */
    func removeCarparkLayer() {
        var arrayIdsNeedRemove: [String] = []
        var arraySourceIdNeedRemove: [String] = []
        
        let array = 1...5
        
        for i in array {
            
            let cpType = CarparkValues.carparkImageName + String(i)
            
            arrayIdsNeedRemove.append("\(CarparkValues.carparkSymbolLayerClusterId)-\(cpType)")
            arrayIdsNeedRemove.append("\(CarparkValues.carparkSymbolLayerUnClusterId)-\(cpType)")
            
            arraySourceIdNeedRemove.append("\(CarparkValues.carparkGeoJsonIdIcon)-\(cpType)")
        }
        arrayIdsNeedRemove.append(CarparkValues.carparkPriceSymbolLayerId)
        arrayIdsNeedRemove.append(CarparkValues.carparkSymbolLayerClusterId)
        arrayIdsNeedRemove.append(CarparkValues.carparkSymbolLayerUnClusterId)
        arrayIdsNeedRemove.append(CarparkValues.carparkClusterPriceSymbolLayerId)
        arrayIdsNeedRemove.append(CarparkValues.carparkUnClusterPriceSymbolLayerId)
        arrayIdsNeedRemove.append(CarparkValues.defaultCarparkSymbolLayerIdIcon)
        arrayIdsNeedRemove.append(CarparkValues.defaultCarparkPriceSymbolLayerId)
        
        self.removeLayerIds(layerIds: arrayIdsNeedRemove)
        
        arraySourceIdNeedRemove.append(CarparkValues.carparkGeoJsonIdIcon)
        arraySourceIdNeedRemove.append(CarparkValues.carparkPriceGeoJsonId)
        arraySourceIdNeedRemove.append(CarparkValues.deafaultCarparkGeoJsonIdIcon)
        arraySourceIdNeedRemove.append(CarparkValues.defaultCarparkPriceGeoJsonId)
        
        self.removeSourceIds(sourceIds: arraySourceIdNeedRemove)
    }
    
    func removePetrolAndEVLayers() {
        let types = [
                             Values.SHARE_EV_ICON,
                             Values.SHARE_PETROL_ICON,
                             Values.SHARE_PETROL_BOOKMARKED,
                             Values.SHARE_EV_BOOKMARKED
        ]
        
        for type in types {
            self.removeAmenitiesLayerOfType(type: type)
        }
    }
}

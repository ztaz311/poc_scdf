//
//  CarparksVC.swift
//  Breeze
//
//  Created by Zhou Hao on 10/5/21.
//

import UIKit
import CoreLocation
import MapboxDirections
import Turf
import MapboxCoreNavigation
@_spi(Restricted) import MapboxMaps
import MapboxCoreMaps
import MapboxNavigation
import SwiftyBeaver
import MapKit
import Combine

protocol CarparksVCDelegate: AnyObject {
    func onCarparkAddressSelected(address: BaseAddress)
}

final class CarparksVC: UIViewController  {

    var address: BaseAddress!
    var response: RouteResponse?
    var arrivalTime: Date?
   
    
    weak var delegate: CarparksVCDelegate?

    private var location:CLLocationCoordinate2D!
    private var carparkViewModel: CarparksViewModel!
    private var cancellable: AnyCancellable?
    private var calloutView: CarparkCalloutView?
    private var currentSelectedCarpark: Carpark?    // callout
    private var destinationCarPark: Carpark?        // the destination has a carpark
    private var nearestCarPark: Carpark?            // It could be the nearest carpark if the destination has no carpark, or it's the second nearest if hte destination has a carpark
    
    @IBOutlet weak var naviMapView: NavigationMapView!
    @IBOutlet weak var topView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UIColor.navBottomBackgroundColor1
        topView.backgroundColor = UIColor.navBottomBackgroundColor1
        topView.showBottomShadow()
        
        location = CLLocationCoordinate2D(latitude: address.lat?.toDouble() ?? 0.0, longitude: address.long?.toDouble() ?? 0.0)
        
        naviMapView.mapView.isUserInteractionEnabled = true
        
        //beta.9 changed
        naviMapView.mapView.ornaments.options.scaleBar.visibility = .hidden
        naviMapView.mapView.ornaments.options.compass.visibility = .hidden
        
        /*mapOptions.ornaments.scaleBarVisibility = .hidden
         mapOptions.ornaments.compassVisibility = .hidden*/
        naviMapView.mapView.ornaments.options.attributionButton.visibility = .hidden
        naviMapView.delegate = self
        naviMapView.setupRouteColor(isDarkMode: isDarkMode)
        naviMapView.mapView.mapboxMap.onEvery(event: .cameraChanged) { [weak self] _ in
            guard let self = self, let calloutView = self.calloutView else { return }
            self.adjust(calloutView: calloutView)
        }
         
        let puck2d = Puck2DConfiguration(topImage: UIImage(named: "puckArrow"), bearingImage: UIImage(named: "puckIcon"), shadowImage: nil, scale: nil)
        
        naviMapView.userLocationStyle = .puck2D(configuration: puck2d)
        
        // only set the defaut camera at the first time
        naviMapView.mapView.mapboxMap.onNext(event: .mapLoaded) { [weak self] _ in
            guard let self = self else { return }

            // setup default camera first
            if let coordinate = self.location {
                self.setDefaultCamera(coordinate: coordinate)
                guard let routes = self.response?.routes, let currentRoute = routes.first else {
                    return
                }
                self.naviMapView.show(routes)
                self.naviMapView.showWaypoints(on: currentRoute)
            }
        }
        
//        #if TESTING
//        carparkViewModel = CarparksViewModel(service: MockCarparkService(), arrivalTime: self.arrivalTime!)
//        #else
        carparkViewModel = CarparksViewModel(service: CarparkService(), arrivalTime: self.arrivalTime!, destName:  self.address.address1!)
//        #endif
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        AnalyticsManager.shared.logScreenView(screenName: ParameterName.routeplanning_carpark_screen)
        
        
        
        setupDarkLightAppearance()
        toggleDarkLightMode()
        setupGestureRecognizers()
    }
    
    deinit {
        
        //We don't need to handle this
    }
    
    @IBAction func onBack(_ sender: Any) {
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Carpark.UserClick.back, screenName: ParameterName.Carpark.screen_view)
//        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.back, screenName: ParameterName.routeplanning_carpark_screen)
        moveBack()
    }
    
    private func setupGestureRecognizers() {
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(panGesture(_:)))
        let pinchGesture = UIPinchGestureRecognizer(target: self, action: #selector(pinchGesture(_:)))
        naviMapView.addGestureRecognizer(panGesture)
        naviMapView.addGestureRecognizer(pinchGesture)
    }
        
    @objc func panGesture(_ gesture: UIPanGestureRecognizer){
        AnalyticsManager.shared.logMapInteraction(eventValue: ParameterName.Carpark.MapInteraction.drag, screenName: ParameterName.Carpark.screen_view)
    }
        
    @objc func pinchGesture(_ gesture: UIPinchGestureRecognizer){
        AnalyticsManager.shared.logMapInteraction(eventValue: ParameterName.Carpark.MapInteraction.pinch, screenName: ParameterName.Carpark.screen_view)
    }
    
    private func setDefaultCamera(coordinate: CLLocationCoordinate2D) {
        
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            
            // Stop navigation camera
            self.naviMapView.navigationCamera.stop()

            let coordinate2 = coordinate.coordinate(at: 500, facing: -90)
            let coordinate3 = coordinate.coordinate(at: 500, facing: 90)
            let cameraOptions = self.naviMapView.mapView.mapboxMap.camera(for: [coordinate2, coordinate, coordinate3], padding: UIEdgeInsets(top: self.topView.frame.height+50, left: 50, bottom:50, right: 50), bearing: 0, pitch: 0)
            self.naviMapView.mapView.camera.ease(to: cameraOptions, duration: Values.standardAnimationDuration, curve: .easeInOut, completion: nil)
        }
    }
    
    private func loadCarparks(destinationAnnotation:PointAnnotation) {
        
        var destinationCarparkAnnotation: Annotation? = nil
        self.destinationCarPark = nil
        
        carparkViewModel.load(at: location, radius: Values.carparkMonitorDistanceInRoutePlanning)
        cancellable = carparkViewModel.$carparks.sink { [weak self] carparks in
            guard let self = self, let carparks = carparks else { return }
            
            var annotations = [PointAnnotation]()
            carparks.forEach { carPark in
                
                if let annotation = self.addCarparkAnnotation(carpark: carPark) {
                    if carPark.destinationCarPark ?? false  {
                        destinationCarparkAnnotation = annotation
                        self.destinationCarPark = carPark
                    }
                    annotations.append(annotation)
                }
            }
            if destinationCarparkAnnotation == nil { // add original destination icon
                annotations.append(destinationAnnotation)
            } else {
                // show the callout
                self.didSelectAnnotation(annotation: destinationCarparkAnnotation!)
            }
            //self.naviMapView.pointAnnotationManager?.syncAnnotations(annotations)
            self.naviMapView.pointAnnotationManager?.annotations = annotations
            if carparks.count > 0 {
                self.naviMapView.mapView.removeCarparkLayer()
                self.naviMapView.mapView.addCarparks(carparks)
                
                // show default callout
                if destinationCarparkAnnotation == nil { // will show the nearest cheapest
                    let cheapest = annotations.filter { annotation in
                        if let carpark = annotation.userInfo?["carpark"] as? Carpark {
                            return carpark.isCheapest ?? false
                        }
                        return false
                    }
                    // get the confirmation from Srikanth/Chethana, there is only one isCheapest = true which is the nearest
                    if let annotation = cheapest.first {
                        self.didSelectAnnotation(annotation: annotation)
                    }
                }
                
                // get the nearest carpark
                self.nearestCarPark = self.getNearestCarpark(carparks: carparks)
                self.setupZoomlevel()
            }
        }
    
    }
    
    private func getNearestCarpark(carparks: [Carpark]) -> Carpark? {
        // if there is a destination carpark
        if let destinationCarpark = self.destinationCarPark {
            
            // if it's cheapest
            if (destinationCarpark.isCheapest ?? false) {
                // get the next car park (no need to do the sorting since backend sorted already
                return getNextNearest(destinationCarpark: destinationCarpark, carparks: carparks)
                
            } else {
                // get the cheapest
                if let cheapest = (carparks.filter { $0.isCheapest ?? false }).first {
                    return cheapest
                } else {
                    // get next nearest
                    return getNextNearest(destinationCarpark: destinationCarpark, carparks: carparks)
                }
            }
            
        } else {
            // get the cheapest (which is the nearest carpark)
            if let nearest = (carparks.filter { $0.isCheapest ?? false }).first {
                return nearest
            }
            // TODO: else - should I return the next nearest?
        }
        return nil
    }
    
    private func getNextNearest(destinationCarpark: Carpark, carparks: [Carpark]) -> Carpark? {
        for carpark in carparks {
            let carparkId = carpark.id ?? ""
            let destinationCarparkId = destinationCarpark.id ?? ""
            
            if carparkId == destinationCarparkId {
                continue
            } else {
                return carpark
            }
        }
        return nil
    }
    
    private func toggleDarkLightMode() {
        naviMapView.setupRouteColor(isDarkMode: isDarkMode)
        naviMapView.mapView.mapboxMap.style.uri = StyleURI(url: URL(string: isDarkMode ? Constants.Map.basicNightStyle: Constants.Map.basicStyle)!)
    }
    
    private func setupZoomlevel() {

        // destination has a carpark
        if let destinationCarpark = self.destinationCarPark {
            if let nearest = self.nearestCarPark {
                setupZoomelevel(coordinate1: CLLocationCoordinate2D(latitude: destinationCarpark.lat ?? 0, longitude: destinationCarpark.long ?? 0), coordinate2: CLLocationCoordinate2D(latitude: nearest.lat ?? 0, longitude: nearest.long ?? 0))
            } else {
                setDefaultCamera(coordinate: CLLocationCoordinate2D(latitude: destinationCarpark.lat ?? 0, longitude: destinationCarpark.long ?? 0))
            }
        } else {
            // destination has no carpark, so use nearest carpark as the coordinate1
            if let nearest = self.nearestCarPark {
                setupZoomelevel(coordinate1:CLLocationCoordinate2D(latitude: nearest.lat ?? 0, longitude: nearest.long ?? 0), coordinate2: location)
            }
        }
    }
        
    private func setupZoomelevel(coordinate1: CLLocationCoordinate2D, coordinate2: CLLocationCoordinate2D) {
        
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
                                    
            // use coordinate1 as center point calcuate the symmetric coordinate3
            let degree = coordinate2.direction(to: coordinate1)
            let distance = coordinate2.distance(to: coordinate1)
            let coordinate3 = coordinate1.coordinate(at: distance, facing: degree)
            
            let cameraOptions = self.naviMapView.mapView.mapboxMap.camera(for: [coordinate2, coordinate1, coordinate3], padding: UIEdgeInsets(top: self.topView.frame.height+50, left: 50, bottom:50, right: 50), bearing: 0, pitch: 0)
            self.naviMapView.mapView.camera.ease(to: cameraOptions, duration: Values.standardAnimationDuration, curve: .easeInOut, completion: nil)
        }
    }
    
    // MARK: - Theme updated
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        guard previousTraitCollection != traitCollection else {
            return
        }
        toggleDarkLightMode()
    }
    
    private func setDestinationAnnotationImage(noLots:Bool) -> UIImage{
        
        if(noLots){
            
            return UIImage(named: "destinationWithCarparkNoLots")!
        }
        else{
            
            return UIImage(named: "destinationWithCarpark")!
            
        }
            
    }
    
    private func setAnnotationImage(noLots:Bool) -> UIImage{
        
        if(noLots){
            
            return UIImage(named: "carparkNoLotsIcon")!
        }
        else{
            
            return UIImage(named: "carparkIcon")!
            
        }
            
    }
        
    // TODO: Need to remove this?
    private func addCarparkAnnotation(carpark: Carpark) -> PointAnnotation? {

//        let isDestination = carpark.destinationCarPark ?? false
        //let availableLots = carpark.availablelot?.intValue ?? 0
        guard let carparkId = carpark.id, !carparkId.isEmpty else {return nil}
        
        var annotation = PointAnnotation(id:carparkId, coordinate: CLLocationCoordinate2D(latitude: carpark.lat ?? 0, longitude: carpark.long ?? 0))
        
        annotation.image = getAnnotationImage(carpark: carpark, selected: false)
        
        //annotation.textField = carpark.id
        annotation.userInfo = [
            "carpark": carpark
        ]
        
        if let layerId = naviMapView.pointAnnotationManager?.layerId, !layerId.isEmpty {
            try? self.naviMapView.mapView.mapboxMap.style.setLayerProperty(for: layerId, property: "icon-allow-overlap", value: true)
        }
        annotation.symbolSortKey = Double(carpark.distance ?? 0)
        return annotation
        
        
        /// We should not use same annotation name for availableLots and noLots.  Next time when we set annotation image it loads from cache
        //annotation.image = .init(image: UIImage(named: availableLots == 0 ? "carparkNoLotsIcon" : "carparkIcon")!, name: "carparkIcon")
/*
        if isDestination {
            
            if let availableLots = carpark.availablelot?.intValue {
                
                if(availableLots == 0){
                    annotation.image = .init(image: setDestinationAnnotationImage(noLots: true), name: "routePlan")
                }
                else{
                    
                    annotation.image = .init(image: setDestinationAnnotationImage(noLots: false), name: "destinationCarPark")
                }
                
            }
            
            else{
                
                annotation.image = .init(image: setDestinationAnnotationImage(noLots: false), name: "destinationCarPark")
            }
            
        }
        
        else {
            
            if let availableLots = carpark.availablelot?.intValue {
                
                if(availableLots == 0){
                    annotation.image = .init(image: setAnnotationImage(noLots: true), name: "carParkNoLots")
                }
                else{
                    annotation.image = .init(image: setAnnotationImage(noLots: false), name: "carparkIcon")
                }
            }
            else{
                
                annotation.image = .init(image: setAnnotationImage(noLots: false), name: "carparkIcon")
            }
            
        }
*/
        
       
        //Old code before beta.13
       /* let annotations = naviMapView.mapView.annotations.annotations.values.filter({ $0.title == carpark.id })
        naviMapView.mapView.annotations.removeAnnotations(annotations)
                                
        var annotation = PointAnnotation(coordinate: location,image:
                                                        UIImage(named: "carparkIcon"))
        annotation.title = carpark.id
        annotation.coordinate = CLLocationCoordinate2D(latitude: carpark.lat ?? 0, longitude: carpark.long ?? 0)
        annotation.userInfo = [
            "carpark": carpark
        ]
        
        // To allow overlap
        if let layerId = self.naviMapView.mapView.annotations.layerId(for: PointAnnotation.self) {
            
            self.naviMapView.mapView.mapboxMap.style.styleManager.setStyleLayerPropertyForLayerId(layerId, property: "icon-allow-overlap", value: true)
            
            try? self.naviMapView.mapView.mapboxMap.style.setLayerProperty(for: layerId, property: "icon-allow-overlap", value: true)
        }

        naviMapView.mapView.annotations.interactionDelegate = self
        naviMapView.mapView.annotations.addAnnotation(annotation)*/
    }
}

extension CarparksVC: NavigationMapViewDelegate {

    func navigationMapView(_ navigationMapView: NavigationMapView, didAdd finalDestinationAnnotation: PointAnnotation, pointAnnotationManager: PointAnnotationManager)
    {
        //beta.15 change
//        let isFavorite = address is EasyBreezyAddresses
//        let point = finalDestinationAnnotation.feature.geometry.value as! Point
//        var destinationAnnotation = PointAnnotation(coordinate: point.coordinates)
//        destinationAnnotation.image = .custom(image: (isFavorite ? UIImage(named: "destinationMark") : UIImage(named: "newAddressPin"))!, name: "routePlan")

        //let point = finalDestinationAnnotation.feature.geometry.value as! Point
        navigationMapView.pointAnnotationManager?.annotations = []
        let point = finalDestinationAnnotation.point
        var destinationAnnotation = PointAnnotation(coordinate: point.coordinates)
        destinationAnnotation.image = .init(image: UIImage(named: "destinationWithoutCarpark")!, name: "routePlan")

        // purposely load carparks after the destination is set
        loadCarparks(destinationAnnotation: destinationAnnotation)
        navigationMapView.pointAnnotationManager?.delegate = self
        
     }
}

extension CarparksVC: AnnotationInteractionDelegate {
    func annotationManager(_ manager: AnnotationManager, didDetectTappedAnnotations annotations: [Annotation]) {
        
        // possible multiple tapped if there are overlapped together, only show the first one
        if let annotation = annotations.first {
            didSelectAnnotation(annotation: annotation, tapped: true)
        }
    }

    func didSelectAnnotation(annotation: Annotation, tapped: Bool = false) {

        DispatchQueue.main.async { [weak self] in
                        
            guard let self = self else { return }
            
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Carpark.UserClick.tap_carpark, screenName: ParameterName.Carpark.screen_view)

            // dismiss if any
            self.didDeselectAnnotation(annotation: annotation)

            if let carpark = annotation.userInfo?["carpark"] as? Carpark {
            
                if carpark.id == self.currentSelectedCarpark?.id {
                    self.currentSelectedCarpark = nil
                    // click the same one, don't show callout
                    return
                }
                self.currentSelectedCarpark = carpark
                
                // set camera (actually should not set if not because of tapped
                if tapped {
                    let cameraOptions = CameraOptions(center: CLLocationCoordinate2D(latitude: carpark.lat ?? 0, longitude: carpark.long ?? 0), zoom: self.naviMapView.mapView.mapboxMap.cameraState.zoom) // using current zoom level
                    self.naviMapView.mapView.camera.ease(to: cameraOptions, duration: Values.standardAnimationDuration, curve: .easeInOut, completion: nil)
                }
                
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.routeplanning_tap_carpark, screenName: ParameterName.routeplanning_carpark_screen)
                
                let callOutView = CarparkCalloutView(carpark: carpark, isNavigateBtnEnabled: true, isDarkMode: self.isDarkMode)
                callOutView.present(from: self.naviMapView.mapView.frame, in: self.naviMapView.mapView, constrainedTo: callOutView.frame, animated: true)
                self.calloutView = callOutView
                callOutView.onCarparkSelected = { [weak self] theCarpark in
                    guard let self = self else { return }
                    
                    let location = CLLocation(latitude: theCarpark.lat ?? 0, longitude: theCarpark.long ?? 0)
                    getAddressFromLocation(location: location) { address in
                        let address = SearchAddresses(alias: "", address1: theCarpark.name ?? "", lat: "\(theCarpark.lat ?? 0)", long: "\(theCarpark.long ?? 0)", address2: address, distance: "\(theCarpark.distance ?? 0)")
                        self.delegate?.onCarparkAddressSelected(address: address)
                    }
                    self.navigationController?.popViewController(animated: true)
                }
                callOutView.onMoreInfo = { [weak self]  theCarpark in
                    guard let self = self else { return }
                    
                    let carParkData = [
                                "toScreen": "ParkingPriceDetail", // Screen name you want to display when start RN
                                "navigationParams": [ // Object data to be used by that screen
                                    "parkingId": theCarpark.id,
                                    "idToken":AWSAuth.sharedInstance.idToken,
                                    "baseURL": appDelegate().backendURL
                                ],
                            ] as [String : Any]
                            let rootView = RNViewManager.sharedInstance.viewForModule(
                                "Breeze",
                                initialProperties: carParkData)
                            
                            let reactNativeVC = UIViewController()
                            reactNativeVC.view = rootView
                            reactNativeVC.modalPresentationStyle = .pageSheet
                          self.present(reactNativeVC, animated: true, completion: nil)

                }
                
                self.adjust(calloutView: callOutView)
            }
        }
    }

    private func adjust(calloutView: CarparkCalloutView) {
        guard let screenCoordinate = naviMapView.mapView?.mapboxMap.point(for: calloutView.location) else { return }
        
        let point = CGPoint(x: screenCoordinate.x, y: screenCoordinate.y)
        calloutView.center = CGPoint(x: point.x, y: point.y - 23) // this is actually based on the icon size
    }

    func didDeselectAnnotation(annotation: Annotation) {
        if let callout = self.calloutView {
            callout.dismiss(animated: true)
            callout.removeFromSuperview()
            self.calloutView = nil
        }
    }
}

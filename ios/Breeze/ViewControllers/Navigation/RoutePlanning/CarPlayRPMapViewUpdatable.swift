//
//  CarPlayRPMapViewUpdatable.swift
//  Breeze
//
//  Created by VishnuKanth on 13/10/21.
//

import Foundation
import CoreLocation
import Turf
import MapboxNavigation
import MapboxMaps
import MapboxDirections
import Combine

final class CarPlayRPMapViewUpdatable:MapViewUpdatable{
    
    weak var navigationMapView: NavigationMapView?
    
    var delegate: MapViewUpdatableDelegate?
    var erpPriceUpdater:ERPPriceCal!
    var cheapestResponse:RouteResponse?
    var shortestResponse:RouteResponse?
    var defaultResponse:RouteResponse?
    var bestOfRouteResponse:RouteResponse?
    var response:RouteResponse?
    var originalResponse = [RouteResponse]()
    @Published var updateMap = false
    init(navigationMapView: NavigationMapView) {
        self.navigationMapView = navigationMapView
    }
    
    func updateParkingAvailability(carpark: Carpark?) {
        //No need to handle this
    }
    
    func updateNotificationWhenUserSoonArrive(carpark: Carpark?, message: String) {
        
        //No need to handle this
    }
    
    func updateNotification(featureId: String?, currentFeature: FeatureDetectable?) {
        
        //No need to handle this
    }
    
    func updateTunnel(isInTunnel: Bool) {
        //No need to handle this
    }
    
    func cruiseStatusUpdate(isCruise: Bool) {
        //No need to handle this
    }
    
    func updateETA(eta: TripCruiseETA?) {
        //No need to handle this
    }

    func updateETA(eta: TripETA?) {
        //No need to handle this
    }

    func showETAMessage(recipient: String, message: String) {
        //No need to handle this
    }
    
    func updateRoadName(_ name: String) {
        //No need to handle this
    }
    
    func updateSpeedLimit(_ value: Double) {
        //No need to handle this
    }
    
    // Not used
    func overSpeed(value: Bool) {
        //No need to handle this
    }

    func initERPUpdater(response:RouteResponse?){
        
        if let mapView = self.navigationMapView?.mapView {
            self.erpPriceUpdater = ERPPriceCal(mapView: mapView, belowPuck: true,response: response!)
        }
    }
    
    func updateLiveLocationEnabled(_ isEnabled: Bool) {
        //No need to handle this
    }
    
    func shareDriveButtonEnabled(_ isEnabled: Bool) {
        //No need to handle this
    }
    
}


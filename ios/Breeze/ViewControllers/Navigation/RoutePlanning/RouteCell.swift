//
//  RouteCell.swift
//  Breeze
//
//  Created by Zhou Hao on 3/2/21.
//

import UIKit
import SwiftyBeaver

class RouteCell: UICollectionViewCell {
    
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var arrivalTime: TwoTextLable!
    //@IBOutlet weak var arrivalTimeUnit: UILabel!
    @IBOutlet weak var duration: TwoTextLable!
   // @IBOutlet weak var durationUnit: UILabel!
    @IBOutlet weak var distance: TwoTextLable!
    //@IBOutlet weak var distanceUnit: UILabel!
    @IBOutlet weak var erpPrice: UILabel!
    @IBOutlet weak var carparksButton: UIButton!
    @IBOutlet weak var bgView: UIView!
    var onConfirmed: (() -> Void)?
    var onCarparks: (() -> Void)?
        
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
        
    deinit {
        SwiftyBeaver.debug("RouteCell deinit")
    }
    
    override func didMoveToSuperview() {
        setupGradientColor()
        initialSetup()
    }
    
    private func setupGradientColor() {
        DispatchQueue.main.async {
//            let bgcolor1 = UIColor.navBottomBackgroundColor1.resolvedColor(with: self.traitCollection)
//            let bgcolor2 = UIColor.navBottomBackgroundColor2.resolvedColor(with: self.traitCollection)
//            self.bgView.applyBackgroundGradient(with: "RouteCellBackground", colors: [bgcolor1.cgColor,bgcolor2.cgColor], vertical: false, cornerRadius: 24)
            self.bgView.backgroundColor = UIColor(named: "navBottomBGColor")
        }
    }
    
    private func initialSetup() {
     
        
        bgView.layer.cornerRadius = 16
        bgView.layer.masksToBounds = true
        
        
      //  showShadow()
    }
    
    @IBAction func onConfirm(_ sender: Any) {
        if let callback = onConfirmed {
            callback()
        }
    }
    
    @IBAction func onCarparks(_ sender: Any) {
        if let callback = onCarparks {
            callback()
        }
    }
    
    // MARK: - Theme updated
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        guard previousTraitCollection != traitCollection else {
            return
        }
        setupGradientColor()
    }

}

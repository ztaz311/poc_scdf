//
//  DirectionService.swift
//  Breeze
//
//  Created by Malou Mendoza on 10/1/22.
//

import Foundation
import Turf
import MapboxNavigation
import MapboxCoreNavigation
import MapboxMaps
import MapboxCoreMaps
import MapboxDirections
import SwiftyBeaver
import RxSwift
import CoreLocation

final class DirectionService: NSObject {

    static let shared = DirectionService()
    
    // cached data
    var erpFullRouteData: ERPFullRouteData?
    var  timeStamp = 0
    private override init() {
       
        //This is just an empty constructor
    }
    
    func carParkRouteResponse(origin:Waypoint,destination:Waypoint,otherWayPoints:[Waypoint]? = nil,completion: ((_ response: RouteResponse?,_ routeType:String) -> Void)?) {
        
        var finalWayPoints = [Waypoint]()
        
        if let otherWayPoints = otherWayPoints {
            
            if(otherWayPoints.count > 0){
                
                finalWayPoints = otherWayPoints
                finalWayPoints.insert(origin, at: 0) //adding origin at index 0
                finalWayPoints.append(destination)
            }
            else{
                
                finalWayPoints.append(origin)
                finalWayPoints.append(destination)
            }
            
        }
        else{
            
            finalWayPoints.append(origin)
            finalWayPoints.append(destination)
        }
        
        let routeOptions = NavigationRouteOptions(waypoints: finalWayPoints, profileIdentifier: .automobileAvoidingTraffic)
        routeOptions.includesSteps = true
        routeOptions.attributeOptions = [.congestionLevel, .expectedTravelTime, .maximumSpeedLimit]
        routeOptions.shapeFormat = .polyline6
        routeOptions.locale = Locale.enSGLocale() //Setting this local to announce voice instructions that are more suitable to Singapore.
        routeOptions.distanceMeasurementSystem = .metric // This setting is used by voice instruction only

        NavigationSettings.shared.distanceUnit = .kilometer
        
        self.returnDirectionsAPIResponse(routeOptions: routeOptions, routeType: Constants.FASTEST) { response, routeType in
            if let callback = completion {
                callback(response,routeType)
            }
        }
    }
        
    // Add a callback here
    private func returnDirectionsAPIResponse(routeOptions:NavigationRouteOptions,routeType:String, completion: ((_ response: RouteResponse?,_ routeType:String) -> Void)?) {
        
        SwiftyBeaver.debug("returnDirectionsAPIResponse start")
        // Generate the route object and draw it on the map
        Directions.shared.calculate(routeOptions) {(session, result) in
            
          switch result {
            case .failure(let error):
              print(error.localizedDescription)
              completion?(nil, "No route found")
              
            case .success(let response):
              if let callback = completion {
                  callback(response,routeType)
              }
          }
        }
        
    }
    
    
    func getRouteResponse(source:CLLocationCoordinate2D,destination:CLLocationCoordinate2D,completion: ((_ response: RouteResponse,_ routeType:String) -> Void)?) {
        
        let sourceWayPoint = Waypoint(coordinate: source, coordinateAccuracy: -1, name: "")
        let destWayPoint = Waypoint(coordinate:destination, coordinateAccuracy: -1, name: "")
        self.calculateRoute(wayPoints: [sourceWayPoint, destWayPoint], completion: completion)
    }
    
    private func calculateRoute(wayPoints: [Waypoint],completion:((_ response: RouteResponse,_ routeType:String) -> Void)?) {
        guard wayPoints.count >= 2 else { return }
        
        // Specify that the route is intended for automobiles avoiding traffic
        let routeOptions = setRouteOptions(routeType: Constants.fast, waypoints: wayPoints)
       

        //let shortRouteOptions = setRouteOptions(routeType: Constants.short, waypoints: [origin,destination])
        
        // Get periodic updates regarding changes in estimated arrival time and traffic congestion segments along the route line.
        //RouteControllerProactiveReroutingInterval = 30
        
        self.returnDirectionsAPIResponse(routeOptions: routeOptions,routeType: Constants.fast) { [weak self] response, routeType in
            guard let self = self, let response = response else { return }
            /// Sort by Expected Travel time
            let sortByFastest = response.routes?.sorted()
            let sortedResponse = RouteResponse(httpResponse: nil, identifier: response.identifier, routes: sortByFastest, waypoints: response.waypoints, options: response.options, credentials: response.credentials)
            
            completion?(sortedResponse, routeType)
            
            // TODO:?
            self.configureResponse(sortedResponse, routeType)
        }
    }
    
    private func setRouteOptions(routeType:String,waypoints:[Waypoint]) -> NavigationRouteOptions{
        
        var routeOptions:NavigationRouteOptions?
        if(routeType == Constants.fast)
        {
            let selectedDate = Date(timeIntervalSince1970: TimeInterval(timeStamp/1000))
            let departTime = DateUtils.shared.getTimeDisplay(dateFormat: Date.formatYYYYMMddTHHmm, date: selectedDate)
            routeOptions = MapedRouteOptions(waypoints: waypoints, departTime: departTime, arriveTime: "", identifier: .automobileAvoidingTraffic)
        }
    
        routeOptions?.attributeOptions = [.congestionLevel, .expectedTravelTime, .maximumSpeedLimit]
        routeOptions?.shapeFormat = .polyline6
        routeOptions?.locale = Locale.enSGLocale() //Setting this local to announce voice instructions that are more suitable to Singapore.
        routeOptions?.distanceMeasurementSystem = .metric // This setting is used by voice instruction only

        NavigationSettings.shared.distanceUnit = .kilometer
        return routeOptions!
    }

    private func configureResponse(_ response: RouteResponse,_ type:String) {
        
        //No need to handle this

    }
    
    func amenityEVandPetrolRouteResponse(origin:Waypoint,destination:Waypoint,otherWayPoints:[Waypoint]? = nil,completion: ((_ response: RouteResponse?,_ routeType:String) -> Void)?) {
        
        var finalWayPoints = [Waypoint]()
        
        if let otherWayPoints = otherWayPoints {
            if(otherWayPoints.count > 0){
                    finalWayPoints = otherWayPoints
                    finalWayPoints.insert(origin, at: 0) //adding origin at index 0
                    finalWayPoints.append(contentsOf: otherWayPoints)
                    finalWayPoints.append(destination)
               
            } else {
                finalWayPoints.append(origin)
                finalWayPoints.append(contentsOf: otherWayPoints)
                finalWayPoints.append(destination)
            }
        }
        else{
            finalWayPoints.append(origin)
            finalWayPoints.append(destination)
        }
        
        let routeOptions = NavigationRouteOptions(waypoints: finalWayPoints, profileIdentifier: .automobileAvoidingTraffic)
        routeOptions.includesSteps = true
        routeOptions.attributeOptions = [.congestionLevel, .expectedTravelTime, .maximumSpeedLimit]
        routeOptions.shapeFormat = .polyline6
        routeOptions.locale = Locale.enSGLocale() //Setting this local to announce voice instructions that are more suitable to Singapore.
        routeOptions.distanceMeasurementSystem = .metric // This setting is used by voice instruction only

        NavigationSettings.shared.distanceUnit = .kilometer
        
        self.returnDirectionsAPIResponse(routeOptions: routeOptions, routeType: Constants.FASTEST) { response, routeType in
            if let callback = completion {
                callback(response,routeType)
            }
        }
    }
    
}

extension RouteOptions {
    
    func waypoints(fromLegAt legIndex: Int) -> ([Waypoint], [Waypoint]) {
        // The first and last waypoints always separate legs. Make exceptions for these waypoints instead of modifying them by side effect.
        let legSeparators = waypoints.filterKeepingFirstAndLast { $0.separatesLegs }
        let viaPointsByLeg = waypoints.splitExceptAtStartAndEnd(omittingEmptySubsequences: false) { $0.separatesLegs }
            .dropFirst()// No leg precedes first separator.
            .dropLast() // No leg precedes last separator.
        
        let reconstitutedWaypoints = zip(legSeparators, viaPointsByLeg).dropFirst(legIndex).map { [$0.0] + $0.1 }
        let legWaypoints = reconstitutedWaypoints.first ?? []
        let subsequentWaypoints = reconstitutedWaypoints.dropFirst()
        return (legWaypoints, subsequentWaypoints.flatMap { $0 })
    }
}



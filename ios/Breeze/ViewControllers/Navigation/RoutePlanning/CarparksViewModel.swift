//
//  CarparksViewModel.swift
//  Breeze
//
//  Created by Zhou Hao on 10/5/21.
//

import Foundation
import Combine
import CoreLocation
import SwiftyBeaver

final class CarparksViewModel {
    @Published private(set) var carparks: [Carpark]?
    var service: CarparkServiceProtocol!
    var arrivalTime: Date? //1627386608
    var destName: String?
    var defaultProfileCarParks = [Carpark]()
    init(service: CarparkServiceProtocol, arrivalTime: Date, destName: String) {
        self.service = service
        self.arrivalTime = arrivalTime
        self.destName = destName
    }
    
    func configure(arrivalTime: Date, destName: String) {
        self.arrivalTime = arrivalTime
        self.destName = destName
    }
    
    func selectCarpark(id: String?) {
        
        if let carparks = carparks {
            for i in 0..<carparks.count {
                let isSameID = carparks[i].id == id
                carparks[i].selected = isSameID
            }
        }
        
        for carpark in defaultProfileCarParks {
            let isSameID = carpark.id == id
            carpark.selected = isSameID
        }
    }
    
    func load(at coordinate: CLLocationCoordinate2D,
              count: Int = 0,
              radius: Int,
              maxRadius: Int = 0,
              shortTermOnly: Bool = false,
              isVoucher: Bool = true,
              parkingAvaibility: String? = nil,
              carparkId: String? = nil, handler: ((Error?) -> Void)? = nil) {
        let timeInterval = arrivalTime?.timeIntervalSince1970 ?? 0
       
        service.getCarparks(lat: coordinate.latitude, long: coordinate.longitude, radius: radius, maxRadius: maxRadius, count: count, arrivalTime: Int(timeInterval), destName: self.destName ?? "", shortTermOnly: shortTermOnly, isVoucher: isVoucher, parkingAvaibility: parkingAvaibility, carparkId: carparkId) { result in
            switch result {
            case .success(let carparks):
//                if count > 0 {
//                    let array = carparks.value!.prefix(count)
//                    self.carparks = [Carpark]()
//                    self.carparks?.append(contentsOf: array)
//                } else {
                if(self.defaultProfileCarParks.count > 0){
                    self.defaultProfileCarParks.removeAll()
                }
                let theCarparks = carparks.amenities?.first?.data ?? []
                if maxRadius > radius {
                    if let first = theCarparks.first {
                        if (first.distance ?? 0) > Values.carparkMonitorDistanceInLanding {
//                            self.carparks = [Carpark]()
                            self.carparks = [first]
                            return
                        }
                    }
                }
                
            self.carparks = theCarparks
                
            case .failure(let error):
                SwiftyBeaver.debug("Load carpark error: \(error.localizedDescription)")
                handler?(error)
                print(error)
            }
        }
    }
    
    func removeCarPark(carPark:Carpark) -> [Carpark]?{
        
        guard var carparks = carparks else {
            return nil
        }

        if let index = carparks.firstIndex(where: {$0.id == carPark.id}){
            carparks.remove(at: index)
            return carparks
        }
        
        return carparks
    }
    
    func getCarPark(parkingID:String) -> Carpark? {
        
        for carPark in carparks ?? [] {
            
            if carPark.id == parkingID {
                
                return carPark
            }
        }
        
        return nil
    }
    
}

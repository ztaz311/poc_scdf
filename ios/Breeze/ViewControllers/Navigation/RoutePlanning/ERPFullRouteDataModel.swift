//
//  ERPFullRouteDataModel.swift
//  Breeze
//
//  Created by Malou Mendoza on 11/1/22.
//

import Foundation
import MapboxDirections

public struct ERPFullRouteData: Codable {
    
    var id: String
    var erpRoutes: [ERPRoute]
    
    // properties not Codable
    var srcLat: Double = 0
    var srcLong: Double = 0
    var destLat: Double = 0
    var destLong: Double = 0
    var srcAddress1:String = ""
    var srcAddress2:String = ""
    var destAddress1:String = ""
    var destAddress2:String = ""
    var routeResponse:RouteResponse?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case erpRoutes = "erpRoutes"
    }
    
    public init() {
        self.id = ""
        self.erpRoutes = []
    }
    
    public init(id: String, erpRoutes: [ERPRoute]) {
        self.id = id
        self.erpRoutes = erpRoutes
    }
    
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decode(String.self, forKey: .id)
        erpRoutes = try values.decode([ERPRoute].self, forKey: .erpRoutes)
    }

    // This will use every fields including the one not codable
//    var asDictionary : [String:Any] {
//        let mirror = Mirror(reflecting: self)
//        let dict = Dictionary(uniqueKeysWithValues: mirror.children.lazy.map({ (label:String?, value:Any) -> (String, Any)? in
//          guard let label = label else { return nil }
//          return (label, value)
//        }).compactMap { $0 })
//        return dict
//    }
    
    var dictionary : [String: Any]? {
        guard let data = try? JSONEncoder().encode(self) else { return nil }
        guard let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String:Any] else { return nil }
        return json
    }
}

public struct ERPRoute: Codable {
    
    let routeId: String
    let erpCharges: [ERPCharge]
        
    enum CodingKeys: String, CodingKey {
        case routeId = "routeId"
        case erpCharges = "eRPCharges"
    }
}

public struct ERPCharge: Codable {
    
    let erpId: String
    let chargeAmount: Double
   
    enum CodingKeys: String, CodingKey {
        case erpId = "erpId"
        case chargeAmount = "chargeAmount"
    }

}

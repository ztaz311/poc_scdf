//
//  RoutePreferenceModel.swift
//  Breeze
//
//  Created by VishnuKanth on 14/01/22.
//

import Foundation
import MapboxDirections

public struct RoutePreferenceData: Codable {
    
    var routes: [PreferenceRoute]
    
    var routeResponse:RouteResponse?

    enum CodingKeys: String, CodingKey {
        case routes = "preferenceRoutes"
        case routeResponse = "preferenceResponse"
    }
    
}

public struct PreferenceRoute: Codable {
    
    let routeId: Int
    let routePreferenceType: String
        
    enum CodingKeys: String, CodingKey {
        case routeId = "routeId"
        case routePreferenceType = "preferenceType"
    }
}

public struct RouteERPList:Codable {
    
    let routeERPRate:Int
    
    
    enum CodingKeys: String, CodingKey {
        case routeERPRate = "erpRate"
    }
}


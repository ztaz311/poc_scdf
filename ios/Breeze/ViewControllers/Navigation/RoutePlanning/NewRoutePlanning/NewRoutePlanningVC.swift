//
//  NewRoutePlanningVC.swift
//  Breeze
//
//  Created by Malou Mendoza on 21/12/21.
//

import UIKit
import CoreLocation
import MapboxDirections
import Turf
import MapboxCoreNavigation
@_spi(Restricted) import MapboxMaps
import MapboxCoreMaps
import MapboxNavigation
import SwiftyBeaver
import SnapKit
import ContactsUI
import Combine
import react_native_date_picker
import AWSAppSync
import Amplify
import UserNotifications
import RxSwift

class NewRoutePlanningVC: BaseViewController {
    
    struct NewRoutePlanningVCConst {
        static let bottomConstOff: Double = -81
        static let bottomConstOn: Double = -20
        
        static let bottomVoucherOff: Double = -65
        static let bottomVoucherOn: Double = -20
        
        static let bottomBroadcastMessageOff: Double = -65
        static let bottomBroadcastMessageOn: Double = -20
        
        static let timeShowCongestionView: Double = 5
    }

    @IBOutlet var topView: UIView!
    @IBOutlet var backButton: UIButton!
    @IBOutlet var reverseButton: UIButton!
    @IBOutlet var sourceLbl: UILabel!
    @IBOutlet var destLbl: UILabel!
    
    @IBOutlet var bottomView: UIView!
    @IBOutlet var directionLbl: UILabel!
    @IBOutlet var notifyLbl: UILabel!
    @IBOutlet var directionBtn: UIButton!
    @IBOutlet var notifyBtn: UIButton!
    @IBOutlet var laterBtn: UIButton!
    @IBOutlet var letsGoBtn: UIButton!
    @IBOutlet var petrolBtn: UIButton!
    @IBOutlet var evchargrBtn: UIButton!
    @IBOutlet var saveBtn:UIButton!
    @IBOutlet var erpView: UIView!
    @IBOutlet var routeView: UIView!
    
    @IBOutlet var arrivalLbl: TwoTextLable!
    @IBOutlet var durationLbl: TwoTextLable!
    @IBOutlet var distanceLbl: TwoTextLable!
    
    @IBOutlet var departArriveLbl: TwoTextLable!
    
    @IBOutlet var arrivalHeaderLbl: UILabel!
    @IBOutlet var durationHeaderLbl: UILabel!
    @IBOutlet var distanceHeaderLbl: UILabel!
    @IBOutlet var erpLbl: UILabel!
    
    @IBOutlet weak var viewCongested: UIView!
    @IBOutlet weak var imvCongestion: UIImageView!
    @IBOutlet weak var lbCongestion: UILabel!
    @IBOutlet weak var ctrBottomCongestion: NSLayoutConstraint!
    
    @IBOutlet weak var viewVoucher: UIView!
    @IBOutlet weak var lbVoucher: UILabel!
    @IBOutlet weak var imvVoucher: UIImageView!
    @IBOutlet weak var imvVoucherArrow: UIImageView!
    @IBOutlet weak var ctrBottomVoucherView: NSLayoutConstraint!
    
    @IBOutlet weak var imageBroadCast: UIImageView!
    @IBOutlet weak var broadcastMessageView: UIView!
    @IBOutlet weak var broadcastMessageLabel: UILabel!
    @IBOutlet weak var broadcastMessageBottomConstraint: NSLayoutConstraint!
    
    
    
    @IBOutlet weak var desLabelMessage: UILabel!
    var timerCheckCongestion: Timer?
    
    @IBOutlet weak var leadingDirectionButtonCtr: NSLayoutConstraint!
    
    var isSourceSelected = false
    var isDestinationSelected = false
    var isRefreshScreen = false
    private var tripUploadRequest:TripPlanUploadRequest?
    @IBOutlet weak var activityIndicator:UIActivityIndicatorView!
    
    @IBOutlet weak var arrowButton: UIButton!
    var isAnimating: Bool = false {
        didSet {
            if Thread.isMainThread {
                self.threadSafeActivity(animate: isAnimating)
            } else {
                DispatchQueue.main.async {
                    self.threadSafeActivity(animate: self.isAnimating)
                }
            }
        }
    }
    
    var callOutView: ToolTipsView? {
        willSet {
            if newValue == nil {
                self.callOutView?.removeFromSuperview()
            }
        }
    }
    
    var isAddressReset = false
    var disposables = Set<AnyCancellable>()
    private var amenityPetrolCancellable: AnyCancellable?
    private var amenityEVCancellable: AnyCancellable?
    private var updateMapCancellable: AnyCancellable?
    
    private var anyCancellables: Set<AnyCancellable> = Set<AnyCancellable>()
    
    private var petrolKiosk:KioskBase?
    private var evItems:KioskBase?
    private(set) var viewModel: RoutePlanningViewModel!
    private var etaChildVC: UIViewController?
    private var datePickerView: RPDatePickerView?
    
    @IBOutlet weak var heightBottomConstraint: NSLayoutConstraint!
    var petrolKioskView:PetrolView?
    var evChargerView:EVChargerView?
    var amenitySymbolThemeType = Values.LIGHT_MODE_UN_SELECTED
    var selectedPetrolKioskItems = [String]()
    var selectedEVItems = [String]()
    var lineString:LineString?
    var lineCoordinates: [CLLocationCoordinate2D] = []
    var midPoints = Turf.FeatureCollection(features: [])
    var routeStopsAdded = [Waypoint]() //Waypoints that are added to the route
    var isDepartSelected = true
    var currentRouteIsFastest = false
    var isPlanTripFromERPDetails = false //FROM ERP DETAILS SCREEN
    var carParkToast:BreezeToast?
    
    //  MARK: - Update carpark when user pans the map
    var needUpdateCarparks: Bool = false
    var updateCarparkTimer: Timer?
    
    //  Variable for pin location select carpark
    var locationPinCarpark: Carpark?
    
    // MARK: Carpark NCSHUB - Special Parking alert
    var carparkId: String?
    
    //  OBU trip summary
    var bookmarkId: String?
    var bookmarkName: String?
    
    var carParkID: String?
    
    override var useDynamicTheme: Bool {
        return true
    }
    
    var navMapView: NavigationMapView! {
        didSet {
            if let navigationMapView = oldValue {
                uninstall(navigationMapView)
            }
            
            if let navigationMapView = navMapView {
                LocationManager.shared.getLocation()
                configure(navigationMapView)
            }
        }
    }
    
    var tripEstArrivalInterval = 0.0
    var tripEstDepartureInterval = 0.0
    // MARK: - CarPark toggle feature
//    var parkingButton: ToggleButton!
    var parkingView: ParkingView!
    var recenterButton: UserLocationButton!
    var isTrackingUser = true {
        didSet {
            recenterButton?.isHidden = isTrackingUser
            if !isTrackingUser {
                recenterButton.snp.updateConstraints { make in
                    if !self.isHiddenCongestion {
                        make.bottom.equalToSuperview().offset(-(269 + 20 + 61))
                    }else if ctrBottomVoucherView.constant == NewRoutePlanningVCConst.bottomVoucherOn {
                        make.bottom.equalToSuperview().offset(-(269 + 20 + 40))
                    }else if broadcastMessageView.isHidden == false {
                        make.bottom.equalToSuperview().offset(-(269 + 100))
                    } else {
                        make.bottom.equalToSuperview().offset(-(269 + 20))
                    }
                }
            }
        }
    }
    
    var parkingMode: ParkingType {
        if let view = parkingView {
            return view.parkingType
        }
        return .all
    }
    
    var isParkingOn: Bool {
        return parkingMode != .hide
    }
    
    /*
     For update carpark
     Will replace CarparksViewModel
    */
    var carparkUpdater: CarParkUpdater?
    
    var currentSelectedCarpark: Carpark? {
        didSet {
            carparkUpdater?.selectCarpark(id: currentSelectedCarpark?.id)
        }
    }
    var currentSelectedParkAnnotation: Annotation?
    
    var destinationAnnotation: PointAnnotation?
        
    var destinationCarPark: Carpark?        // the destination has a carpark

    // TODO: Not used. Consider to remove this
    // ------------------------
    var nearestCarPark: Carpark?            // It could be the nearest carpark if the destination has no carpark, or it's the second nearest if the destination has a carpark
    var carparkAnnotations = [PointAnnotation]() // to save the current point annotation
    // ------------------------
    
    var lastDestinationCoordinate: CLLocationCoordinate2D?    
    //var currentERPTime:Date?
    var tripEditDepartChanged = false
    var tripEditArriveChanged = false
    var tripETATime = ""
    var waypoints: [Waypoint] = [] {
        didSet {
            waypoints.forEach {
                $0.coordinateAccuracy = -1
            }
        }
    }
    var currentRouteSelection = ""
    var currentRouteIndex = 0 {
        didSet {
            showCurrentRoute()
        }
    }
    var tripEstStartTime = 0.0
    var currentSelectedRouteSymbolID = ""
        
    var layerCode: String?
    var amenityType: String?
    var amenityId: String?
    
    var selectedTimeselectedTime: String?
    
    var selectedTime: String?
    var selectedTimeErp: String?
    
    var inviteLocationVC: UIViewController?
    
    func showCurrentRoute() {
        guard var prioritizedRoutes = routes else { return }
        
        prioritizedRoutes.insert(prioritizedRoutes.remove(at: currentRouteIndex),
                                 at: 0)
        
        /// Show congestion levels on alternative route lines if there're multiple routes in the response.
        navMapView.showsCongestionForAlternativeRoutes = true
        
        //navMapView.showsRestrictedAreasOnRoute = true
        navMapView.show(prioritizedRoutes)
        navMapView.showWaypoints(on: prioritizedRoutes.first!)
        //navMapView.showRouteDurations(along: prioritizedRoutes)
        navMapView.mapView.ornaments.options.attributionButton.visibility = .hidden
    }
    
    var currentRoute: Route? {
        return routes?[currentRouteIndex]
    }
    
    var routes: [Route]? {
        return response?.routes
    }
        
    var response: RouteResponse? {
        didSet {
            guard let routes = response?.routes, !routes.isEmpty else {
                clearMap()
                return
            }
            
            //  If the routes is not empty then need check and show broad cast message
            displayBroadcastMessage()
            
            var routeIndex = 0
            if let trip = self.planTrip {
                
                var coordinateArray: [CLLocationCoordinate2D] = []
                for coordinate in trip.routeCoordinates{

                    let coordinate = CLLocationCoordinate2D(latitude: coordinate[0], longitude: coordinate[1])
                    coordinateArray.append(coordinate)
                   
                         
                }
                
                let lineString = Turf.LineString(coordinateArray)
                let geometry = Geometry(lineString)
                
                
                for (index, route) in routes.enumerated() {
                    
                    if route.shape == lineString{
                        routeIndex = index
                        SwiftyBeaver.debug("Both routes are equal \(route)")
                    }
                }
                
            }
            
            if(self.isPlanTripFromERPDetails){
                
                routeIndex = self.routeIndexFromERPDetails
            }
            
            currentRouteIndex = routeIndex
        }
    }
    
    func clearMap() {
        
        // remove annotation and callout
        /*if let calloutView = self.calloutView {
            calloutView.dismissCallout(animated: false)
        }
        self.calloutView = nil*/
        
        self.navMapView?.removeWaypoints()
        self.navMapView?.removeRoutes()
    }
    
    private var etaInfo: ETAInfo? {
        didSet {
            // change the bottom panel button text
            // TO DO - :
            //routePlanningBottomVC.isETAShared = etaInfo != nil
        }
    }
    
    var erpPriceUpdater: ERPPriceCal!
    
    //MARK: - Selected Address Variables passed from caller
    var selectedIndex = 0
    var selectionSource = ""
    var rankingIndex = 0
    var addressReceived = false {
        didSet {
            if addressReceived { // To force refreshing
                if(viewModel == nil)
                {
                    self.viewModel = RoutePlanningViewModel()
//                    appDelegate().routePlanningViewModel = self.viewModel
                }
                tripEstStartTime = 0
                setupMap()
                setupDarkLightAppearance()
                toggleDarkLightMode()
            }
        }
    }
    var selectedCarPark:Carpark?
    var isRouteSelectedDuringPark = false
    var enableSave = false {
        didSet {
            
            if(self.planTrip != nil){
                
                self.laterBtn.setTitle("Save", for: .normal)
                
                if(enableSave){
                    
                    self.laterBtn.isEnabled = true
                    //self.laterBtn.backgroundColor = UIColor(named: "ratingBtnColor")!
                    self.laterBtn.layer.borderColor = UIColor(named: "rpPurpleTextColor")!.cgColor
                    self.laterBtn.setTitleColor(UIColor(named: "rpPurpleTextColor")!, for: .normal)
                }
                else{
                    
//                    self.laterBtn.backgroundColor = UIColor(named: "addStopDisableColor")!
                    self.laterBtn.backgroundColor = isDarkMode ? .clear : .white
                    self.laterBtn.layer.borderColor = UIColor(red: 0.757, green: 0.757, blue: 0.757, alpha: 1).cgColor
                    self.laterBtn.isEnabled = false
                    self.laterBtn.setTitleColor(UIColor(red: 0.757, green: 0.757, blue: 0.757, alpha: 1), for: .normal)
                    
                }
            }
            else{
                
                if(self.isPlanTripFromERPDetails){
                    
                    self.laterBtn.setTitle("Save", for: .normal)
                    
                    if(enableSave){
                        
                        self.laterBtn.isEnabled = true
                        //self.laterBtn.backgroundColor = UIColor(named: "ratingBtnColor")!
                        self.laterBtn.layer.borderColor = UIColor(named: "rpPurpleTextColor")!.cgColor
                        self.laterBtn.setTitleColor(UIColor(named: "rpPurpleTextColor")!, for: .normal)
                    }
                    else{
                        
    //                    self.laterBtn.backgroundColor = UIColor(named: "addStopDisableColor")!
                        self.laterBtn.backgroundColor = isDarkMode ? .clear : .white
                        self.laterBtn.layer.borderColor = UIColor(red: 0.757, green: 0.757, blue: 0.757, alpha: 1).cgColor
                        self.laterBtn.isEnabled = false
                        self.laterBtn.setTitleColor(UIColor(red: 0.757, green: 0.757, blue: 0.757, alpha: 1), for: .normal)
                        
                    }
                }
            }
            
            
        }
    }
    var routeIndexFromERPDetails = 0
    var currentAddressFromERPDetails: BaseAddress! //FROM ERP DETAILS SCREEN
    var originalAddress: BaseAddress! // This is passed from search or favorite
    var planTrip: PlannTripDetails? // passed from upcoming trip log
    var previousSelectedDate:Date?
        
    var selectedAddressFromERPDetails:BaseAddress!{
        didSet{
            
            viewModel.currentBaseAddress = selectedAddressFromERPDetails
            viewModel.setRPCurrentAddress()
        }
    }
    var selectedAddress: BaseAddress! {
        didSet {
            
            if(isSourceSelected == false && isDestinationSelected == false)
            {
                if(selectedAddress != nil){
                    
                    viewModel.baseAddress = selectedAddress
                    viewModel.setRPAddress()
                }
            }
            else if(isSourceSelected == true && selectedAddress != nil){
                
                isSourceSelected = false
                viewModel.currentBaseAddress = selectedAddress
                viewModel.setRPCurrentAddress()
            }
            
            else if(isDestinationSelected == true && selectedAddress != nil){
                
                isDestinationSelected = false
                viewModel.baseAddress = selectedAddress
                viewModel.setRPAddress()
            }
        }
    }
    
    
    //MARK: - Class Override methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.stopObuDisplayIfNeeded()
        
        //Force crash
       /*let numbers = [0]
       let _ = numbers[1]*/
        self.saveBtn.isHidden = true
        //ETA Notify notification that receives event from RN
//        NotificationCenter.default.addObserver(self, selector: #selector(receiveETA), name: Notification.Name(Values.NotificationETASetUp), object: nil)
        
        NotificationCenter.default.publisher(for: Notification.Name(Values.NotificationCloseNotifyArrivalScreen), object: nil)
            .receive(on: DispatchQueue.main)
            .sink { [weak self] notification in
                guard let self = self, let vc = self.etaChildVC else { return }
                self.removeChildViewController(vc)
                self.etaChildVC = nil
            }.store(in: &disposables)
        
//        letsGoBtn.applyGradient(colors: [UIColor.brandPurpleColor.cgColor,UIColor.brandPurpleColor.cgColor], withShadow: false)
//        laterBtn.layer.borderColor = UIColor.brandPurpleColor.cgColor
        laterBtn.layer.cornerRadius = 25
//        laterBtn.layer.borderWidth = 1
        laterBtn.clipsToBounds = true
        
//        bottomView.layer.borderColor =  UIColor.rgba(r: 221, g: 221, b: 221, a: 1.0).cgColor
       // bottomView.layer.cornerRadius = 25
        
        routeView.layer.cornerRadius = 10
//        bottomView.layer.borderWidth = 1
//        bottomView.clipsToBounds = true
        
        erpView.layer.cornerRadius = 16
        erpView.layer.borderWidth = 0
        erpView.clipsToBounds = true
        
        
        let sourcetap = UITapGestureRecognizer(target: self, action: #selector(sourceTapped(_:)))
        sourceLbl.isUserInteractionEnabled = true
        
        sourcetap.numberOfTouchesRequired = 1
        sourceLbl.addGestureRecognizer(sourcetap)
        
        destLbl.text = ""
        let destinationtap = UITapGestureRecognizer(target: self, action: #selector(destinationTapped(_:)))
        destLbl.isUserInteractionEnabled = true
        destinationtap.numberOfTouchesRequired = 1
        destLbl.addGestureRecognizer(destinationtap)
                
        let directiontap = UITapGestureRecognizer(target: self, action: #selector(directionTapped(_:)))
        directionLbl.isUserInteractionEnabled = true
        
        directiontap.numberOfTouchesRequired = 1
        directionLbl.addGestureRecognizer(directiontap)
        
        let notifytap = UITapGestureRecognizer(target: self, action: #selector(notifyTapped(_:)))
        notifyLbl.isUserInteractionEnabled = true
        
        notifytap.numberOfTouchesRequired = 1
        notifyLbl.addGestureRecognizer(notifytap)
        
        let departTap = UITapGestureRecognizer(target: self, action: #selector(self.openDatePicker(_:)))
        departTap.numberOfTouchesRequired = 1
        self.departArriveLbl.isUserInteractionEnabled = true
        self.departArriveLbl.addGestureRecognizer(departTap)
        
        // While coming from ERP details screen, don't show Later/Let's go button if ERP Checker selected time > current time
        if(self.isPlanTripFromERPDetails){
            
            self.selectedAddressFromERPDetails = self.currentAddressFromERPDetails
            
            if  TimeInterval(DirectionService.shared.timeStamp/1000) > Date().timeIntervalSince1970 {
                
                self.laterBtn.isHidden = true
                self.letsGoBtn.isHidden = true
                self.saveBtn.isHidden = false
                self.isDepartSelected = true
                self.heightBottomConstraint.constant = 294
                self.departArriveLbl.isHidden = false
                
                let selectedDate = Date(timeIntervalSince1970: TimeInterval(DirectionService.shared.timeStamp/1000))
                
                self.previousSelectedDate = selectedDate
                
                let formattedDateString = " \(DateUtils.shared.getTimeDisplay(dateFormat: Date.formatHHmmdMMME, date: selectedDate))"
                self.departArriveLbl.set("Depart at ", formattedDateString)
                
                self.viewModel.departTime = DateUtils.shared.getTimeDisplay(dateFormat: Date.formatYYYYMMddTHHmm, date: selectedDate)
                self.viewModel.departDate = selectedDate
                
                self.viewModel.currentERPTime = selectedDate
                self.tripEstStartTime = TimeInterval(DirectionService.shared.timeStamp/1000)
            }
            else{
                self.isPlanTripFromERPDetails = false
            }
            
        }
                
//        setupDarkLightAppearance(forceLightMode: true)
        self.updateMapWithAmenities()
        setupView()
        
        checkShowObuInstallIfNeeded()
        
//        showBroadcastMessageCarPark()
        NotificationCenter.default.addObserver(self, selector: #selector(onCloseInviteLocationVC), name: .onCloseInviteScreen, object: nil)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if self.tripEstStartTime == 0 {
            self.heightBottomConstraint.constant = 269
            self.departArriveLbl.isHidden = true
        }
        if(planTrip != nil){
            AnalyticsManager.shared.logScreenView(screenName: ParameterName.routeplanning_screen_edit)
        }
        else{
            
            AnalyticsManager.shared.logScreenView(screenName: ParameterName.routeplanning_screen)
        }
        
        if(viewModel == nil)
        {
            self.viewModel = RoutePlanningViewModel()
//            appDelegate().routePlanningViewModel = self.viewModel
        }
        
        displayBroadcastMessage()
        
        //Commenting below line, as we don't need routePlanning state. When we sync route planning in both phone and car play we will enable this
//        appDelegate().enterIntoRoutePlanning(from: .mobile)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        addShadowToRouteView()
        
        
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        guard previousTraitCollection != traitCollection else {
            return
        }
        toggleDarkLightMode()
    }
    
    private func handleShowingBroadcastMessage(_ carparkId: String) {
        self.viewModel.fetchBroadcastMessage(carparkId, mode: .planning) {[weak self] result in
            if let model = result, model.hasBroadcastMessage() {
                DispatchQueue.main.async {
                    self?.showCarparkBroadcastMessage(true, title: model.getBroadcastMessage())
                }
            }
        }
        
        //  Fetch carpark detail
        CarparkDetailService().getCarparkDetail(carparkId) {[weak self] result in
            //  Fix crashed app issue by serialize processing to a safe thread
            DispatchQueue.main.async {
                guard let self = self else { return }
                switch result {
                case .success(let searchCarpark):
                    self.locationPinCarpark = searchCarpark.data
                case .failure(let error):
                    self.locationPinCarpark = nil
                    print(error)
                }
            }
        }
    }
    
    func displayBroadcastMessage() {
//        guard let route = self.currentRoute else {
//
//            return
//        }
        var routeArray: [[String: Any]] = []
        var postDic: [String: Any] = [:]
                
        var arrivalTime: Int64 = 0
        var departureTime: Int64 = Date().currentTimeInMiliseconds()
        
        if let allRoutes = self.routes, !allRoutes.isEmpty {
            
            let sortedList = allRoutes.sorted { r1, r2 in
                return r1.expectedTravelTime > r2.expectedTravelTime
            }
            //  Set arrival time by default
            if let longestRoute = sortedList.first {
                arrivalTime = departureTime + Int64(longestRoute.expectedTravelTime * 1000)
                
                if let date = self.viewModel.departDate {
                    departureTime = date.currentTimeInMiliseconds()
                    arrivalTime = departureTime + Int64(longestRoute.expectedTravelTime * 1000)
                } else if let date = self.viewModel.arrivalDate {
                    arrivalTime = date.currentTimeInMiliseconds()
                    departureTime = arrivalTime - Int64(longestRoute.expectedTravelTime * 1000)
                }
            }
            routeArray.removeAll()
            for route in allRoutes {
                var coordinateDic: [String: Any] = [:]
                var coordinates:[[Double]] = []
                for coordinate in route.shape?.coordinates ?? [] {
                    print(coordinate)
                    coordinates.append([Double(coordinate.latitude),Double(coordinate.longitude)])
                }
                coordinateDic["coordinates"] = coordinates
                routeArray.append(coordinateDic)
            }
        }
        
        // Update new paramester when call api get broadcast message
        
        var latDes: Double
        var longDes: Double
        
        if destinationCarPark != nil {
            latDes = destinationCarPark?.lat ?? 0.0
            longDes = destinationCarPark?.long ?? 0.0
        } else {
            latDes = viewModel.address?.lat ?? 0.0
            longDes = viewModel.address?.long ?? 0.0
        }
        
        let destination: [String: Any] = [
            "lat": latDes,
            "long": longDes
        ]
        
        if !routeArray.isEmpty {
            postDic["routes"] = routeArray
            postDic["arrivalTimestamp"] = arrivalTime
            postDic["departureTimestamp"] = departureTime
            postDic["destination"] = destination
            callApiBroadcastMessage(postDic)
            
            SwiftyBeaver.debug("data post  \(postDic)")
        }
    }
   
    
    private func callApiBroadcastMessage(_ routes: [String: Any]) {
        self.viewModel.getBroadcastMessageRoutes(routes) {[weak self] result in
            if let model = result, model.success == true {
                DispatchQueue.main.async {
                    if let modelDBroadcastData = model.broadcast.first, !model.broadcast.isEmpty {
                        if modelDBroadcastData.type != "carpark" {
                            self?.showBroadcastMessageRoutes(true, title: modelDBroadcastData.message ?? "")
                        } else {
                            self?.carParkID = modelDBroadcastData.action?.data?.id
                            self?.showBroadcastMessageCarPark(alertLevel: modelDBroadcastData.alertLevel ?? "", title: modelDBroadcastData.title ?? "", message: modelDBroadcastData.message ?? "")
                        }
                        
                    } else {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                            self?.broadcastMessageView.isHidden = true
                        }
                    }
                }
            } else {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    self?.broadcastMessageView.isHidden = true
                }
            }
        }
    }
    
    
    func showBroadcastMessageRoutes(_ isShow: Bool, title: String) {
        if !title.isEmpty {
            broadcastMessageView.isHidden = !isShow
            broadcastMessageLabel.text = title
            broadcastMessageView.backgroundColor = UIColor(hexString: "#FCFCFC", alpha: 1.0)
            broadcastMessageLabel.textColor = UIColor(hexString: "#E82370", alpha: 1.0)
            imageBroadCast.image = UIImage(named: "ic_congested_red")
            desLabelMessage.isHidden = true
            arrowButton.isHidden = true
            broadcastMessageBottomConstraint.constant = -20
        }else {
            broadcastMessageView.isHidden = true
        }
    }
    
    func showBroadcastMessageCarPark(alertLevel: String, title: String, message: String) {
        
        if alertLevel == "WARN" {//INFO
            broadcastMessageView.isHidden = false
            broadcastMessageLabel.text = title
            desLabelMessage.text = message
            broadcastMessageView.backgroundColor = UIColor(hexString: "#FDEDF3", alpha: 1.0)
            desLabelMessage.textColor = UIColor(hexString: "#E82370", alpha: 1.0)
            broadcastMessageLabel.textColor = UIColor(hexString: "#E82370", alpha: 1.0)
            desLabelMessage.font = UIFont.parkingAvailableTextFont()
            broadcastMessageLabel.font = UIFont.roadNameBoldFont()
            imageBroadCast.image = UIImage(named: "ic_congested_red")
            arrowButton.setImage(UIImage(named: "arrow-pink-icon"), for: .normal)
        } else if alertLevel == "INFO" {
            broadcastMessageView.isHidden = false
            broadcastMessageLabel.text = title
            desLabelMessage.text = message
            broadcastMessageView.backgroundColor = UIColor(hexString: "#FEF2EC", alpha: 1.0)
            desLabelMessage.textColor = UIColor(hexString: "#F26415", alpha: 1.0)
            broadcastMessageLabel.textColor = UIColor(hexString: "#F26415", alpha: 1.0)
            desLabelMessage.font = UIFont.parkingAvailableTextFont()
            broadcastMessageLabel.font = UIFont.roadNameBoldFont()
            imageBroadCast.image = UIImage(named: "icon-waring-oragen")
            arrowButton.setImage(UIImage(named: "aarrow-icon"), for: .normal)
        } else {
            broadcastMessageView.isHidden = true
        }
        
        
    }
    
    
    
    private func setupView() {
        viewCongested.layer.cornerRadius = 10
        
        ctrBottomCongestion.constant = NewRoutePlanningVCConst.bottomConstOff
        ctrBottomVoucherView.constant = NewRoutePlanningVCConst.bottomVoucherOff
        
        viewModel.$showTBRCongestion.sink { [weak self] needShow in
            DispatchQueue.main.async {
                guard let self = self else { return }
                self.checkShowCongestion()
            }
        }.store(in: &anyCancellables)
                
        viewModel.$routeNotFound
            .receive(on: DispatchQueue.main)
            .sink { [weak self] notFound in
                
                if notFound {
                    // TODO: UI need to be confirmed from production team
                    self?.animateActivity(animate: false)
                    
                    AnalyticsManager.shared.logOpenPopupEventV2(popupName: ParameterName.RoutePlanningMode.UserPopup.no_drivable_route, screenName: ParameterName.RoutePlanningMode.screen_view)
                    
                    self?.popupAlert(title: Constants.ncsNotFoundRouteLocation, message: "", actionTitles: [Constants.gotIt], actions:[{action1 in
                        
                        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.RoutePlanningMode.UserClick.popup_no_drivable_route_got_it, screenName: ParameterName.RoutePlanningMode.screen_view)
                        
                        self?.dismissAnyAlertControllerIfPresent()
                        appDelegate().tryToDismissTemplateIfNeed()
                        self?.navigationController?.popViewController(animated: true)
                    },{action2 in
                        //No need to handle this
                    }, nil])
                }
            }.store(in: &anyCancellables)
        broadcastMessageLabel.lineBreakMode = .byWordWrapping
        viewVoucher.layer.cornerRadius = 10
        broadcastMessageView.layer.cornerRadius = 10
        
    }
    
    private func addShadowToRouteView() {
        // Add shadow to routeView
        let shadowPath0 = UIBezierPath(roundedRect: routeView.bounds, cornerRadius: 10)

        let layer0 = CALayer()

        layer0.shadowPath = shadowPath0.cgPath

        layer0.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.15).cgColor

        layer0.shadowOpacity = 1

        layer0.shadowRadius = 2

        layer0.shadowOffset = CGSize(width: 0, height: -5)

        layer0.bounds = routeView.bounds

        layer0.position = routeView.center
        routeView.layer.addSublayer(layer0)
        routeView.layer.borderWidth = 0.5
        routeView.layer.borderColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.15).cgColor
    }
    
    var isHiddenCongestion: Bool {
        return !self.viewModel.showTBRCongestion || self.isParkingOn
    }
    
    func checkShowCongestion() {
        
        let hidden = self.isHiddenCongestion
        
        if !hidden {
            self.showCarparkBroadcastMessage(false, title: "")
        }
        
        let const: Double = hidden ? NewRoutePlanningVCConst.bottomConstOff : NewRoutePlanningVCConst.bottomConstOn
        if (self.ctrBottomCongestion.constant == const) {
            return
        }
        UIView.animate(withDuration: Values.standardAnimationDuration) {
            self.ctrBottomCongestion.constant = const
            self.view.layoutIfNeeded()
        }
    }
    
    //MARK: - Theme update method
    private func toggleDarkLightMode() {
        if viewModel != nil {
            viewModel.setUpRouteColors(isDarkMode: isDarkMode)
        }
        amenitySymbolThemeType = isDarkMode ? Values.DARK_MODE_UN_SELECTED : Values.LIGHT_MODE_UN_SELECTED
        if let mapview = self.navMapView?.mapView {
            mapview.mapboxMap.style.uri = StyleURI(url: URL(string: isDarkMode ? Constants.Map.basicNightStyle: Constants.Map.basicStyle)!)
        }
        sendThemeChangeEvent(isDarkMode)
    }
    
    // check if the current location the same as the saved (planned) trip
    private func checkSavedLocation(plannedTrip: PlannTripDetails) {
        if let currentLocation = self.navMapView?.mapView.location.latestLocation {
            let plannedStart = CLLocationCoordinate2D(latitude: plannedTrip.startLat, longitude: plannedTrip.startLong)
            if plannedStart.distance(to: currentLocation.coordinate) < Values.distanceCheckThreshold {
                self.originalAddress = SearchAddresses(alias: "", address1: plannedTrip.destAddress1, lat: "\(plannedTrip.destLat)", long: "\(plannedTrip.destLong)", address2: plannedTrip.destAddress2, distance: "\(plannedTrip.totalDistance)")
                self.setupRoute()
            } else {
                self.popupAlert(title: "", message: Constants.differentStartLocationMessage, actionTitles: [Constants.cancel,Constants.yes], actions:[{action1 in
                    // return to trip log
                    self.navigationController?.popViewController(animated: true)
                },{action2 in
                    // set start/destination address
                    self.originalAddress = SearchAddresses(alias: "", address1: plannedTrip.destAddress1, lat: "\(plannedTrip.destLat)", long: "\(plannedTrip.destLong)", address2: plannedTrip.destAddress2, distance: "\(plannedTrip.totalDistance)")
                    self.setupRoute()
                }, nil])
            }
        }
    }
    
    private func changeNotifyETAIcon(){
        
        self.notifyBtn.titleLabel?.text = Constants.cpShareDestination
        self.notifyLbl.text = Constants.cpShareDestination
        // TODO: Use localized string file later.
//                self.notifyBtn.titleLabel?.text = "RP-ShareDrive-Cancel".localized
//                self.notifyLbl.text = "RP-ShareDrive-Cancel".localized
        self.notifyBtn.setImage(UIImage(named: "share_destination_icon"), for: .normal)
    }
    
    private func useSavedLocation(plannedTrip: PlannTripDetails) {

        if(!isSourceSelected && !isDestinationSelected){
            
            if let currentLocation = self.navMapView?.mapView.location.latestLocation {
                let plannedStart = CLLocationCoordinate2D(latitude: plannedTrip.startLat, longitude: plannedTrip.startLong)
                if plannedStart.distance(to: currentLocation.coordinate) < Values.distanceCheckThreshold {
                    // current location is the same as the planned trip
                } else {
                    self.viewModel.currentBaseAddress = SearchAddresses(alias: "", address1: plannedTrip.startAddress1, lat: "\(plannedTrip.startLat)", long: "\(plannedTrip.startLong)", address2: plannedTrip.startAddress2, distance: "\(plannedTrip.totalDistance)")
                    self.viewModel.setRPCurrentAddress()
                }
                self.originalAddress = SearchAddresses(alias: "", address1: plannedTrip.destAddress1, lat: "\(plannedTrip.destLat)", long: "\(plannedTrip.destLong)", address2: plannedTrip.destAddress2, distance: "\(plannedTrip.totalDistance)")
                self.setupRoute(tripPlanFirstTime: true)
                self.enableSave = false
            }
        }
        else{
            
            self.setupRoute(tripPlanFirstTime: false)
        }
        
    }
    
    private func updateRouteBasedOnAddressMatch(isSameAddress:Bool,tripPlanFirstTime:Bool = false) {
        
        guard let viewmodel = self.viewModel,
              let mobileRPMapViewUpdatable = viewmodel.mobileRPMapViewUpdatable,
        let navMapView = self.navMapView else { return }
        
        mobileRPMapViewUpdatable.defaultResponse = nil
        mobileRPMapViewUpdatable.bestOfRouteResponse = nil
        mobileRPMapViewUpdatable.cheapestResponse = nil
        mobileRPMapViewUpdatable.shortestResponse = nil
        mobileRPMapViewUpdatable.erpPriceUpdater = nil
        
        
        
        if addressReceived == true {
            if !isSameAddress {
                if planTrip != nil && !tripPlanFirstTime {
                    self.enableSave = true
                }
                self.selectedAddress = self.originalAddress
            }
            addressReceived = false
        }
        
        if self.originalAddress != nil {
            self.animateActivity(animate: true)
            self.setSourceAndDestinationAddress()
            clearMap()
            
            viewModel.wayPointsAdded.removeAll()
            
            if self.routeStopsAdded.count > 0 {
                
                self.viewModel.wayPointsAdded = routeStopsAdded
                
                if planTrip != nil && !tripPlanFirstTime {
                    self.enableSave = true
                }
            }
            
            // Check add profile zone or remove it
            self.viewModel.getZoneColors()
            
            navMapView.mapView.removeRoutePlanningERPRouteLayer()
            navMapView.mapView.removeRoutePlanningERPRouteAlternateLayer()
            
            DispatchQueue.main.async {
                
                if(self.isPlanTripFromERPDetails) {
                    var erpList = [RouteERPList]()
                    if let data = DirectionService.shared.erpFullRouteData {
                        
                        for routeData in data.erpRoutes {
                            
                            var erpCharge  = 0
                            for charges in routeData.erpCharges {
                                
                                erpCharge = erpCharge + Int(charges.chargeAmount)
                                
                            }
                            
                            erpList.append(RouteERPList(routeERPRate: erpCharge))
                        }
                        
                        if let routeResponse = data.routeResponse {
                            
                            if(erpList.count > 0){
                                
                                if let index = erpList.firstIndex(where: {$0.routeERPRate == 0}){
                                    erpList.remove(at: index)
                                }
                            }
                            self.viewModel.displayRoutesFromERPDetails(response: routeResponse, erpList: erpList)
                        }
                        
                    }
                } else {
                    self.viewModel.setupRoute()
                }
            }
        }
    }
    
    private func checkShowBroadcastMessage(_ baseAddress: BaseAddress? = nil) {
        
        if let _ = baseAddress {
            var newCarparkId: String?
            if let address = baseAddress as? SearchAddresses {
                if address.isCarpark() {
                    newCarparkId = address.carparkId
                }
            }else if let address = baseAddress as? EasyBreezyAddresses {
                if address.isCarpark() {
                    newCarparkId = address.carparkId
                }
            }
            
            if let idCarPark = carparkId, !idCarPark.isEmpty {
                newCarparkId = carparkId
            }
            
            if let ID = newCarparkId, !ID.isEmpty {
                handleShowingBroadcastMessage(ID)
            }
            
        }else {
            if let carpark = self.selectedCarPark {
                self.viewModel.fetchBroadcastMessage(carpark.id, mode: .planning) {[weak self] result in
                    if let model = result, model.hasBroadcastMessage() {
                        DispatchQueue.main.async {
                            self?.showCarparkBroadcastMessage(true, title: model.getBroadcastMessage())
                        }
                    }
                }
            } else if let ID = self.carparkId, !ID.isEmpty {
                handleShowingBroadcastMessage(ID)
            }
        }
    }
    
    private func setupRoute(tripPlanFirstTime:Bool = false) {
        
        if(addressReceived == true){
            
            self.destinationCarPark = nil
                        
            if(isSourceSelected){
                self.isPlanTripFromERPDetails = false
                
                if(self.originalAddress.address1 == self.destLbl.text || self.originalAddress.address1 == viewModel.address.address){
                    
                    DispatchQueue.main.async {
                        
                        self.popupAlert(title: "", message: "You have input the same location for both origin and destination. Please change to a different location.", actionTitles: [Constants.okay], actions:[{action1 in
                                
                            //No need to handle this action
                                
                            },{action2 in
                                //No need to handle this action
                            }, nil])
                    }
                    
                    self.isSourceSelected = false
                    self.updateRouteBasedOnAddressMatch(isSameAddress: true,tripPlanFirstTime: tripPlanFirstTime)
                }
                else{
                    self.updateRouteBasedOnAddressMatch(isSameAddress: false,tripPlanFirstTime: tripPlanFirstTime)
                }
            }
            else if(isDestinationSelected){
                self.isPlanTripFromERPDetails = false
                
                if(self.originalAddress.address1 == self.sourceLbl.text || self.originalAddress.address1 == viewModel.currentAddress.address){
                    DispatchQueue.main.async {
                        
                        self.popupAlert(title: "", message: "You have input the same location for both origin and destination. Please change to a different location.", actionTitles: [Constants.okay], actions:[{action1 in
                                
                            //No need to handle this action
                            },{action2 in
                                //No need to handle this action
                            }, nil])
                    }
                    self.isDestinationSelected = false
                    self.updateRouteBasedOnAddressMatch(isSameAddress: true,tripPlanFirstTime:tripPlanFirstTime)
                }
                else{
                    self.checkShowBroadcastMessage(self.originalAddress)
                    self.updateRouteBasedOnAddressMatch(isSameAddress: false,tripPlanFirstTime: tripPlanFirstTime)
                }
            }
            else{
                self.checkShowBroadcastMessage(self.originalAddress)
                self.updateRouteBasedOnAddressMatch(isSameAddress: false,tripPlanFirstTime:tripPlanFirstTime)
            }
            
            
        }
        
        else{
            self.checkShowBroadcastMessage(self.originalAddress)
            self.updateRouteBasedOnAddressMatch(isSameAddress: false,tripPlanFirstTime: tripPlanFirstTime)
        }
        
    }
    
    //MARK: - MapView SetUp
    private func setupMap() {
        if navMapView == nil {
            navMapView = NavigationMapView()
            
        }
        else
        {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                //self.viewModel.setupRoute()
                // reset the parking button
//                self.isParkingOn = false
                self.parkingView.setParkingMode(type: .all)
//                self.parkingButton.select(false)
            }
        }
    }
    
    private func uninstall(_ navigationMapView: NavigationMapView) {
        navigationMapView.removeFromSuperview()
    }
    
    private func configure(_ navigationMapView: NavigationMapView) {
        
        view.addSubview(navigationMapView)
        view.sendSubviewToBack(navigationMapView)

        
        navigationMapView.snp.makeConstraints { (make) in
            make.leading.equalToSuperview()
            make.trailing.equalToSuperview()
            make.bottom.equalToSuperview()
            make.top.equalToSuperview().offset(60)
        }
        
        navigationMapView.delegate = self
        navMapView.mapView.gestures.options.pitchEnabled = false
        
        //DataCenter.shared.addERPUpdaterListner(listner: self)
    
        //beta.14 change
        
        var traitCollection = UITraitCollection(userInterfaceStyle: .light)
        if(Settings.shared.theme == 0){
            
            if(appDelegate().isDarkMode()){
                
                traitCollection = UITraitCollection(userInterfaceStyle: .dark)
                
            }
            
        }
        else{
            
            traitCollection = UITraitCollection(userInterfaceStyle: UIUserInterfaceStyle(rawValue: Settings.shared.theme) ?? .unspecified)
        }
        
        let puck2d = Puck2DConfiguration(topImage: UIImage(named: "puckArrow",in:nil,compatibleWith: traitCollection), bearingImage: UIImage(named: "puckIcon"), shadowImage: nil, scale: nil)
        
        navigationMapView.userLocationStyle = .puck2D(configuration: puck2d)
        self.viewModel?.mobileRPMapViewUpdatable = RoutePlanningViewMap(navigationMapView: self.navMapView)
        self.viewModel?.mobileRPMapViewUpdatable?.setRouteLineWidth()
        self.viewModel.setUpRouteColors(isDarkMode: self.isDarkMode)
        self.updateMapCancellable = self.viewModel.mobileRPMapViewUpdatable!.$updateMap
            .receive(on: DispatchQueue.main)
            .sink(receiveValue: { [weak self] updateMap in
                guard let self = self, self.navMapView != nil, self.navMapView?.mapView != nil else { return }
                if updateMap {
                    SwiftyBeaver.debug("Update Map response came from Model class")
                    self.navMapView?.mapView.removeAllPreferenceLayers()
                    if(self.selectedPetrolKioskItems.count > 0){
                        self.selectedPetrolKioskItems.removeAll()
                    }
                    
                    if(self.selectedEVItems.count > 0){
                        self.selectedEVItems.removeAll()
                    }
                    self.updateMapWithRouteResponse()
                    DispatchQueue.global(qos: .background).async {
                        self.callAmenitiesAPIForPetrol()
                        self.callAmenitiesAPIForEV()
                        
                        // If parking is off => load carpark voucher here
//                        if (!self.isParkingOn) {
//                            self.loadCarparks()
//                        }
                        self.loadCarparks()
                    }
                    
                } else {
                    
                }
        })
                
        navigationMapView.mapView.mapboxMap.onEvery(event: .styleLoaded, handler: { [weak self] _ in
            guard let self = self,
                  self.navMapView != nil else { return }
            
            AmenitiesSharedInstance.shared.loadAllImagesToTheMapStyle(navMapView: self.navMapView)
            self.navMapView.mapView.removeFSCImagesToMapStyle()
            self.navMapView.mapView.addFSCImagesToMapStyle()
            self.navMapView.mapView.removERPBoundImagesFromMapStyle()
            self.navMapView.mapView.addERPBoundImagesToMapStyle()
            //we will enable this after map box fix current location issue in the beta.14 SDK
            /*guard let address = self!.viewModel.address, let coordinate = self!.navView.mapView.location.latestLocation?.coordinate else {
                print("return")

            guard let address = self.viewModel.address, let coordinate = self.navView.mapView.location.latestLocation?.coordinate else {
                return
            }*/
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) { [weak self] in
                
                guard let self = self, self.navMapView != nil, self.viewModel != nil else { return }
                self.viewModel.wayPointsAdded.removeAll()
                
                //TODO: Tuyenlx (consider to refactor in the future)
                    
                self.viewModel.wayPointsAdded = self.routeStopsAdded
                
                if self.viewModel.mobileRPMapViewUpdatable?.originalResponse != nil {
                    
                    self.viewModel.mobileRPMapViewUpdatable?.originalResponse.removeAll()
                }
                
                
                // check if the planned trip start location is the same as the current location
                if let trip = self.planTrip {
                                        
                    if(trip.planBy == Values.DEPART_AT){
                                            
                        let selectedDate = Date(timeIntervalSince1970: trip.startTime)
                        
                        self.viewModel.departTime = DateUtils.shared.getTimeDisplay(dateFormat: Date.formatYYYYMMddTHHmm, date: selectedDate)
                        self.viewModel.departDate = selectedDate
                        self.viewModel.currentERPTime = selectedDate
                        self.tripEstStartTime = trip.startTime
                    }
                    else{
                        
                        let selectedDate = Date(timeIntervalSince1970: trip.arrivalTime)
                        
                        self.viewModel.arrivalTime = DateUtils.shared.getTimeDisplay(dateFormat: Date.formatYYYYMMddTHHmm, date: selectedDate)
                        self.viewModel.arrivalDate = selectedDate
                        self.viewModel.currentERPTime = selectedDate
                        self.tripEstStartTime = trip.startTime
                    }
                    
                    /// Getting existing route stops
                    let routeStopDetails = AmenitiesSharedInstance.shared.getExistingStopsAdded(plan: trip,themeType: self.amenitySymbolThemeType)
                    if let wayPoints = routeStopDetails.wayPoints{
                        
                        self.routeStopsAdded = wayPoints
                        self.viewModel.wayPointsAdded.append(contentsOf: wayPoints)
                    }
                    
                    if let wayPointFeatures = routeStopDetails.wayPointFeatures {
                        self.viewModel.wayPointFeatures = wayPointFeatures
                        
                        for feature in self.viewModel.wayPointFeatures {
                            
                            if case let .string(featureType) = feature.properties?["type"]{
                                
                                self.addWayPointToMap(type: featureType)
                            }
                        }
                    }
                   
                    //Check if ETA is already set up for this trip
                    if self.etaInfo == nil {
                        
                        if trip.isEtaAvailable{
                            
                            self.etaInfo = ETAInfo(message: "", recipientName: trip.recipientName ?? "", recipientNumber: trip.recipientNumber ?? "", etaFavoriteId: trip.tripEtaFavouriteId ?? -1)
                            
                            self.changeNotifyETAIcon()
                        }
                        
                    }
                    else{
                        
                        self.changeNotifyETAIcon()
                    }

                    self.useSavedLocation(plannedTrip: trip)
                    
                    
                } else {
                    // no planned trip, keep the original logic
                    
                    self.laterBtn.setTitle("Later", for: .normal)
                    self.viewModel.wayPointsAdded.removeAll()
                    if(self.routeStopsAdded.count > 0){
                        self.viewModel.wayPointsAdded = self.routeStopsAdded
                    }
                    self.setupRoute()
                    
                }
            }
            
            //self!.setupAnnotation(location: CLLocationCoordinate2D(latitude: address.lat, longitude: address.long), name: address.address1)
        })
        
        navMapView.mapView.ornaments.options.compass.visibility = .hidden
        navigationMapView.mapView.mapboxMap.onEvery(event: .cameraChanged) { [weak self] _ in
            guard let self = self else { return }
            
            if let callout = self.callOutView {
                callout.adjust(to: callout.tooltipCoordinate, in: self.navMapView)
            }
        }
        
        setupGestureRecognizers()
        
        setupParkingButton()
        
        setupRecenterButton(isTrack: isTrackingUser)
    }
    
    //MARK: - Gesture recognizers
    private func setupGestureRecognizers() {
#if TESTING
        let longPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress(_:)))
        navMapView.gestureRecognizers?.filter({ $0 is UILongPressGestureRecognizer }).forEach(longPressGestureRecognizer.require(toFail:))
        navMapView.addGestureRecognizer(longPressGestureRecognizer)
        let topViewPanGesture = UIPanGestureRecognizer(target: self, action: #selector(topViewPanGesture(_:)))
        topView.addGestureRecognizer(topViewPanGesture)
#endif
        
        navMapView.mapView.gestures.delegate = self
        
        // disable the mapbox map gesture
        let index = navMapView.mapView.gestureRecognizers!.endIndex - 1
        navMapView.mapView.removeGestureRecognizer(navMapView.mapView.gestureRecognizers![index])

//        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(panGesture(_:)))
//        let pinchGesture = UIPinchGestureRecognizer(target: self, action: #selector(pinchGesture(_:)))
//        let touchGesture = UITapGestureRecognizer(target: self, action: #selector(tapGesture(_:)))
//        navMapView.mapView.addGestureRecognizer(touchGesture)
//        navMapView.addGestureRecognizer(panGesture)
//        navMapView.addGestureRecognizer(pinchGesture)
    }
    
/*
    @objc func tapGesture(_ gesture: UITapGestureRecognizer){
        
        self.didDeselectAmenityAnnotation()
        self.didDeselectCarparkAnnotation()
        
        let point = gesture.location(in: navMapView)
        self.queryAmenitiesPetrolLayer(point: point)
        
    }
    
    @objc func panGesture(_ gesture: UIPanGestureRecognizer){
        AnalyticsManager.shared.logMapInteraction(eventValue: ParameterName.RoutePlanning.MapInteraction.drag, screenName: ParameterName.RoutePlanning.screen_view)
        
        self.didDeselectCarparkAnnotation()
    }
        
    @objc func pinchGesture(_ gesture: UIPinchGestureRecognizer){
        AnalyticsManager.shared.logMapInteraction(eventValue: ParameterName.RoutePlanning.MapInteraction.pinch, screenName: ParameterName.RoutePlanning.screen_view)
        
        self.didDeselectCarparkAnnotation()
    }
*/
    
#if TESTING
    @objc func handleLongPress(_ gesture: UILongPressGestureRecognizer) {
        guard gesture.state == .began else { return }
        
        
    }
    
    @objc func topViewPanGesture(_ gesture: UIPanGestureRecognizer){
        guard gesture.state == .ended else {
            return
        }
        
        if case .Down = gesture.verticalDirection(target: self.topView) {
            let docPicker = UIDocumentPickerViewController(documentTypes: ["public.json"], in: .import)
            docPicker.delegate = self
            docPicker.allowsMultipleSelection = false
            present(docPicker, animated: true, completion: nil)
        }
    }

#endif        
    
    func setSourceAndDestinationAddress(){
        
        guard let address = viewModel.address else {
            return
        }
        
        if (viewModel.baseAddress as? EasyBreezyAddresses) != nil {
            
            destLbl.text = address.name
        }
        else{
            
            destLbl.text = address.address
        }
        
        guard let currentAddress = viewModel.currentAddress else {
            return
        }
        
        if (viewModel.currentBaseAddress as? EasyBreezyAddresses) != nil {
            
            sourceLbl.text = currentAddress.name
        }
        else{
            
            sourceLbl.text = currentAddress.address
        }
        
    }
    
    func setRoutInfo(){
        
        guard let route = self.currentRoute else {
            
            return
        }
        
        if let planTrip = planTrip {
            
            var tripStartTime = 0
            var tripArriveTime = 0
            self.departArriveLbl.isHidden = false
            self.heightBottomConstraint.constant = 294
            
            let distanceSet = viewModel.getRouteDistance(route: route)
            //distanceLbl.text = "\(distanceSet.0) \(distanceSet.1)"
            distanceLbl.set(distanceSet.0,distanceSet.1)
            var planBy = planTrip.planBy
            if(planTrip.planBy == Values.DEPART_AT){
                
                if(self.tripEditDepartChanged){
                    
                    planBy = Values.DEPART_AT
                    
                    if let previousSelectedDate = previousSelectedDate {
                        
                        let string = previousSelectedDate.formatted
                        
                        let timerIntervalDate = Date.getDate(str: string).timeIntervalSince1970
                        
                        self.tripEstArrivalInterval =  route.expectedTravelTime + timerIntervalDate
                        
                        self.viewModel.currentERPTime = previousSelectedDate
                        
                        self.arrivalHeaderLbl.text = "Est.Arrival"
                        self.durationHeaderLbl.text = "Est.Duration"
                        
                        let durationSet = self.viewModel.getRouteDurationFromTimeInterval(expectedTravelTime: currentRoute?.expectedTravelTime ?? 0.0)
                        
                        self.durationLbl.set(durationSet.0,durationSet.1)
                        
                        let arrivalSet = estimatedArrivalTime(currentRoute?.expectedTravelTime ?? 0.0, date: previousSelectedDate)
                      
                        self.arrivalLbl.set(arrivalSet.0, arrivalSet.1)
                        
                        tripETATime = "\(arrivalSet.0)\(arrivalSet.1)"
                        
                        tripStartTime = Int(timerIntervalDate)
                        
                        tripArriveTime =  Int(self.tripEstArrivalInterval)
                       
                    }
                
                    
                }
                else if(self.tripEditArriveChanged){
                    
                    planBy = Values.ARRIVE_BY
                    
                    if let previousSelectedDate = previousSelectedDate {
                        
                        let string = previousSelectedDate.formatted
                        
                        self.tripETATime = previousSelectedDate.getTimeFromDate()
                        
                        let timerIntervalDate = Date.getDate(str: string).timeIntervalSince1970
                        
                        self.tripEstDepartureInterval = timerIntervalDate - route.expectedTravelTime
                        
                        self.arrivalHeaderLbl.text = "Est.Departure"
                        self.durationHeaderLbl.text = "Est.Duration"
                        
                        let durationSet = self.viewModel.getRouteDurationFromTimeInterval(expectedTravelTime: route.expectedTravelTime)
                        
                        self.durationLbl.set(durationSet.0,durationSet.1)
                        
                        let arrivalSet = estimatedArrivalTime(-(currentRoute?.expectedTravelTime ?? 0.0), date: previousSelectedDate)
                      
                        self.arrivalLbl.set(arrivalSet.0, arrivalSet.1)
                        
                        //currentERPTime = Date(timeIntervalSince1970: -(currentRoute?.expectedTravelTime ?? 0.0))
                        self.viewModel.currentERPTime = previousSelectedDate
                        
                       tripStartTime =  Int(self.tripEstDepartureInterval)
                        
                        tripArriveTime = Int(timerIntervalDate)
                    }
                    
                }
                else{
                    
                    let selectedDateString = Date(timeIntervalSince1970: planTrip.startTime).formatted
                    let selectedDate = Date.getDate(str: selectedDateString)
                    
                    self.viewModel.currentERPTime = selectedDate
                    self.previousSelectedDate = selectedDate
                    
                    self.arrivalHeaderLbl.text = "Est.Arrival"
                    self.durationHeaderLbl.text = "Est.Duration"
                    
                    let durationSet = self.viewModel.getRouteDurationFromTimeInterval(expectedTravelTime: currentRoute?.expectedTravelTime ?? 0.0)
                    
                    self.durationLbl.set(durationSet.0,durationSet.1)
                    
                    let arrivalSet = estimatedArrivalTime(currentRoute?.expectedTravelTime ?? 0.0, date: selectedDate)
                  
                    self.arrivalLbl.set(arrivalSet.0, arrivalSet.1)
                    
                    self.tripETATime = "\(arrivalSet.0)\(arrivalSet.1)"
                    
                    let startDate = Date(timeIntervalSince1970: planTrip.startTime)
                    let formattedDateString = " \(DateUtils.shared.getTimeDisplay(dateFormat: Date.formatHHmmdMMME, date: startDate))"
                    
                    self.departArriveLbl.set("Depart at ", formattedDateString)
                    
                    self.tripEstArrivalInterval =  route.expectedTravelTime + planTrip.startTime
                    
                    tripArriveTime =  Int(self.tripEstArrivalInterval)
                }
                
            }
            else{
                
                if(self.tripEditDepartChanged){
                    
                    planBy = Values.DEPART_AT
                    
                    if let previousSelectedDate = previousSelectedDate {
                        
                        let string = previousSelectedDate.formatted
                        
                        let timerIntervalDate = Date.getDate(str: string).timeIntervalSince1970
                        
                        self.tripEstArrivalInterval =  route.expectedTravelTime + timerIntervalDate
                        
                        self.viewModel.currentERPTime = previousSelectedDate
                        
                        self.arrivalHeaderLbl.text = "Est.Arrival"
                        self.durationHeaderLbl.text = "Est.Duration"
                        
                        let durationSet = self.viewModel.getRouteDurationFromTimeInterval(expectedTravelTime: currentRoute?.expectedTravelTime ?? 0.0)
                        
                        self.durationLbl.set(durationSet.0,durationSet.1)
                        
                        let arrivalSet = estimatedArrivalTime(currentRoute?.expectedTravelTime ?? 0.0, date: previousSelectedDate)
                      
                        self.arrivalLbl.set(arrivalSet.0, arrivalSet.1)
                        
                        self.tripETATime = "\(arrivalSet.0)\(arrivalSet.1)"
                        
                        tripStartTime = Int(timerIntervalDate)
                        
                        tripArriveTime =  Int(self.tripEstArrivalInterval)
                        
                       
                    }
                
                    
                }
                else if(self.tripEditArriveChanged){
                    
                    planBy = Values.ARRIVE_BY
                    
                    if let previousSelectedDate = previousSelectedDate {
                        
                        let string = previousSelectedDate.formatted
                        
                        self.tripETATime = previousSelectedDate.getTimeFromDate()
                        
                        let timerIntervalDate = Date.getDate(str: string).timeIntervalSince1970
                        
                        self.tripEstDepartureInterval = timerIntervalDate - route.expectedTravelTime
                        
                        self.arrivalHeaderLbl.text = "Est.Departure"
                        self.durationHeaderLbl.text = "Est.Duration"
                        
                        let durationSet = self.viewModel.getRouteDurationFromTimeInterval(expectedTravelTime: route.expectedTravelTime)
                        
                        self.durationLbl.set(durationSet.0,durationSet.1)
                        
                        let arrivalSet = estimatedArrivalTime(-(currentRoute?.expectedTravelTime ?? 0.0), date: previousSelectedDate)
                      print(arrivalSet)
                        self.arrivalLbl.set(arrivalSet.0, arrivalSet.1)
                        
                        self.viewModel.currentERPTime = previousSelectedDate
                        //currentERPTime = Date(timeIntervalSince1970: -(currentRoute?.expectedTravelTime ?? 0.0))
                        
                        tripStartTime =  Int(self.tripEstDepartureInterval)
                         
                         tripArriveTime = Int(timerIntervalDate)
                    }
                    
                }
                else{
                    
                    let selectedDateString = Date(timeIntervalSince1970: planTrip.arrivalTime).formatted
                    let selectedDate = Date.getDate(str: selectedDateString)
                    self.tripETATime = selectedDate.getTimeFromDate()
                    self.previousSelectedDate = selectedDate
                    
                    self.arrivalHeaderLbl.text = "Est.Departure"
                    self.durationHeaderLbl.text = "Est.Duration"
                    
                    let durationSet = self.viewModel.getRouteDurationFromTimeInterval(expectedTravelTime: route.expectedTravelTime)
                    
                    self.durationLbl.set(durationSet.0,durationSet.1)
                    
                    let arrivalSet = estimatedArrivalTime(-(currentRoute?.expectedTravelTime ?? 0.0), date: selectedDate)
                  
                    self.arrivalLbl.set(arrivalSet.0, arrivalSet.1)
                    
                    let arrivalDate = Date(timeIntervalSince1970: planTrip.arrivalTime)
                    let formattedDateString = " \(DateUtils.shared.getTimeDisplay(dateFormat: Date.formatHHmmdMMME, date: arrivalDate))"
                    
                    self.departArriveLbl.set("Arrive by ", formattedDateString)
                    
                    self.viewModel.currentERPTime = selectedDate
                    
                    self.tripEstDepartureInterval = planTrip.arrivalTime - route.expectedTravelTime
                    
                    tripStartTime =  Int(self.tripEstDepartureInterval)
                    //currentERPTime = Date(timeIntervalSince1970: -(currentRoute?.expectedTravelTime ?? 0.0))
                    
                }
                
            }
            
            var coordinates:[[Double]] = []
            
            for coordinate in route.shape?.coordinates ?? [] {
                
                coordinates.append([Double(coordinate.latitude ),Double(coordinate.longitude)])
                
            }
//            for i in 0...(((route.shape?.coordinates.count)!) - 2) {
//
//                let coordinate = route.shape?.coordinates[i]
//
//                coordinates.append([Double(coordinate?.latitude ?? 0),Double(coordinate?.longitude ?? 0)])
//
//            }
            
            var routeStops = [RouteStops]()
            if(self.routeStopsAdded.count > 0){
                
                for feature in self.viewModel.wayPointFeatures{
                    
                    if case let .point(point) = feature.geometry,
                       case let .string(type) = feature.properties?["type"],
                        case let .string(amenityID) = feature.properties?["amenityID"] {
                        
                        let routeStop = RouteStops(amenityId: amenityID, coordinates: [point.coordinates.latitude,point.coordinates.longitude], type: type)
                        routeStops.append(routeStop)
                        
                    }
                }
            }
            var erpEntries = [Int]()
            if let viewModel = self.viewModel{
                
                if let priceUpdater = viewModel.mobileRPMapViewUpdatable?.erpPriceUpdater{
                    
                    if(priceUpdater.allERPInSelectedRoute.count > 0){
                        
                        for feature in priceUpdater.allERPInSelectedRoute {
                            
                            if let selectedFeatureProperties = feature.properties,
                               case let .string(erpId) = selectedFeatureProperties["erpid"]
                            {
                                erpEntries.append(Int(erpId)!)
                            }
                        }
                    }
                }
            }
            var erpPrice = 0.0
            if let erpText = self.erpLbl.text{
                
                let priceArray = erpText.components(separatedBy: "$")
                let doublePrice = Float(priceArray[1]) ?? 0.0
                erpPrice = Double(doublePrice)
            }
            
            
            self.tripUploadRequest = TripPlanUploadRequest(tripEstStartTime:tripStartTime == 0 ? Int(planTrip.startTime) : tripStartTime, tripEstArrivalTime:tripArriveTime == 0 ? Int(planTrip.arrivalTime) : tripArriveTime, tripStartAddress1: self.viewModel.currentAddress.address, tripStartAddress2: self.viewModel.currentAddress.address1, tripStartLat: self.viewModel.currentAddress.lat, tripStartLong: self.viewModel.currentAddress.long, tripDestAddress1: self.viewModel.address.address, tripDestAddress2: self.viewModel.address.address1, tripDestLat: self.viewModel.address.lat, tripDestLong: self.viewModel.address.long, totalDuration: Int(self.currentRoute?.expectedTravelTime ?? 0), totalDistance: Int(self.currentRoute?.distance ?? 0), erpExpense: erpPrice, tripPlanBy: planBy, erpEntries: erpEntries, routeStops:routeStops, routeCoordinates: coordinates,tripEtaFavouriteId: self.etaInfo == nil ? nil : self.etaInfo?.etaFavoriteId)
        }
        else{
            
            if viewModel.departTime == "" && viewModel.arrivalTime == "" {
                
                self.arrivalHeaderLbl.text = "Arrival"
                self.durationHeaderLbl.text = "Duration"
                
                let distanceSet = viewModel.getRouteDistance(route: route)
                //distanceLbl.text = "\(distanceSet.0) \(distanceSet.1)"
                distanceLbl.set(distanceSet.0,distanceSet.1)
                
                let durationSet = viewModel.getRouteDuration(route: route)
                //durationLbl.text = "\(durationSet.0) \(durationSet.1)"
                durationLbl.set(durationSet.0,durationSet.1)
                
                let arrivalSet = viewModel.getArrivalTime(route: route)
               // arrivalLbl.text = "\(arrivalSet.0) \(arrivalSet.1)"
                arrivalLbl.set(arrivalSet.0, arrivalSet.1)
                
                self.tripETATime = "\(arrivalSet.0)\(arrivalSet.1)"
            }
            else{
                
                
                let distanceSet = viewModel.getRouteDistance(route: route)
                distanceLbl.set(distanceSet.0,distanceSet.1)
                
                if let previousSelectedDate = previousSelectedDate {
                    
                    let string = previousSelectedDate.formatted
                    
                    let timerIntervalDate = Date.getDate(str: string).timeIntervalSince1970
                    
                    if (isDepartSelected) {
                        
                        self.tripEstArrivalInterval =  route.expectedTravelTime + timerIntervalDate
                        
                        let durationSet = self.viewModel.getRouteDurationFromTimeInterval(expectedTravelTime: route.expectedTravelTime)
                        //durationLbl.text = "\(durationSet.0) \(durationSet.1)"
                        self.durationLbl.set(durationSet.0,durationSet.1)
                        
                        let arrivalSet = estimatedArrivalTime(route.expectedTravelTime, date: previousSelectedDate)
                        
                        self.arrivalLbl.set(arrivalSet.0, arrivalSet.1)
                        
                        self.viewModel.currentERPTime = previousSelectedDate
                        
                        self.tripETATime = "\(arrivalSet.0)\(arrivalSet.1)"
                    }
                    else{
                        
                        self.tripEstDepartureInterval = timerIntervalDate - route.expectedTravelTime
                        
                        self.tripETATime = previousSelectedDate.getTimeFromDate()
                        
                        let durationSet = self.viewModel.getRouteDurationFromTimeInterval(expectedTravelTime:route.expectedTravelTime)
                        
                        self.durationLbl.set(durationSet.0,durationSet.1)
                        
                        let arrivalSet = estimatedArrivalTime(-(route.expectedTravelTime), date: previousSelectedDate)
                      
                        self.arrivalLbl.set(arrivalSet.0, arrivalSet.1)
                        
                        //currentERPTime = Date(timeIntervalSince1970: -(route.expectedTravelTime))
                        self.viewModel.currentERPTime = previousSelectedDate
                        
                    }
                    
                    let tripEstStartTime = self.isDepartSelected ? Int(timerIntervalDate) : Int(self.tripEstDepartureInterval)
                    
                    let tripEstArrivalTime = self.isDepartSelected ? Int(self.tripEstArrivalInterval) : Int(timerIntervalDate)
                    let routeStops = [RouteStops]()
                    let coordinates:[[Double]] = []
                    let erpEntries = [Int]()
                    if let tripUploadRequest = tripUploadRequest {
                        
                        self.tripUploadRequest = TripPlanUploadRequest(tripEstStartTime: tripEstStartTime, tripEstArrivalTime: tripEstArrivalTime, tripStartAddress1: self.viewModel.currentAddress.address, tripStartAddress2: self.viewModel.currentAddress.address1, tripStartLat: self.viewModel.currentAddress.lat, tripStartLong: self.viewModel.currentAddress.long, tripDestAddress1: self.viewModel.address.address, tripDestAddress2: self.viewModel.address.address1, tripDestLat: self.viewModel.address.lat, tripDestLong: self.viewModel.address.long, totalDuration: Int(route.expectedTravelTime), totalDistance: Int(route.distance), erpExpense: tripUploadRequest.erpExpense, tripPlanBy: tripUploadRequest.tripPlanBy, erpEntries: tripUploadRequest.erpEntries, routeStops:tripUploadRequest.routeStops, routeCoordinates: tripUploadRequest.routeCoordinates,tripEtaFavouriteId: self.etaInfo == nil ? nil : self.etaInfo?.etaFavoriteId)
                    }
                    else{
                        
                        self.tripUploadRequest = TripPlanUploadRequest(tripEstStartTime: tripEstStartTime, tripEstArrivalTime: tripEstArrivalTime, tripStartAddress1: self.viewModel.currentAddress.address, tripStartAddress2: self.viewModel.currentAddress.address1, tripStartLat: self.viewModel.currentAddress.lat, tripStartLong: self.viewModel.currentAddress.long, tripDestAddress1: self.viewModel.address.address, tripDestAddress2: self.viewModel.address.address1, tripDestLat: self.viewModel.address.lat, tripDestLong: self.viewModel.address.long, totalDuration: Int(route.expectedTravelTime), totalDistance: Int(route.distance), erpExpense: 0.0, tripPlanBy: Values.DEPART_AT, erpEntries: erpEntries, routeStops:routeStops, routeCoordinates: coordinates,tripEtaFavouriteId: self.etaInfo == nil ? nil : self.etaInfo?.etaFavoriteId)
                    }
                }
                
            }
            
        }
        
    }
    
    
    //MARK: - AddingWaypoints to the existing route
    func addWayPoints(location:CLLocationCoordinate2D,sName:String,feature:Turf.Feature,type:String){
        
        let wayPoint = Waypoint(coordinate: location, coordinateAccuracy: -1, name: sName)
        wayPoint.separatesLegs = Values.wayPointSeparateLegs
        
        //if(routeStopsAdded.count > 0){
            
            routeStopsAdded.append(wayPoint)
            viewModel.wayPointsAdded = routeStopsAdded
        //}
        
        viewModel.wayPointFeatures.append(feature)
        self.clearMap()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) { [weak self] in
            guard let self = self else { return }
            self.enableSave = true
            self.animateActivity(animate: true)
            self.viewModel.setupRoute()
            self.checkShowBroadcastMessage()
        }
        
        
        //self.addWayPointToMap(type: type)
        
    }
    
    private func addWayPointToMap(type:String){
        
        if let petrolKioskView = petrolKioskView {
            petrolKioskView.removeFromSuperview()
            self.petrolKioskView = nil
            self.removeLayerTypeFrom(type: Values.PETROL)
            if(self.selectedPetrolKioskItems.count > 0){
                
                self.selectedPetrolKioskItems.removeAll()
            }
        }
        
        if let evChargerView = evChargerView {
            evChargerView.removeFromSuperview()
            self.evChargerView = nil
            self.removeLayerTypeFrom(type: Values.EV_CHARGER)
            
            if(self.selectedEVItems.count > 0){
                
                self.selectedEVItems.removeAll()
            }
        }
        
        DispatchQueue.main.async {
        self.petrolBtn.setImage(UIImage.init(named: "petrolKiosk"), for: .normal)
        
        self.petrolBtn.setImage(UIImage.init(named: "petrolKiosk"), for: .normal)
        self.evchargrBtn.setImage(UIImage.init(named: "evChargr"), for: .normal)
        }
        if(viewModel.wayPointFeatures.count > 0){
            
            var turfArray = [Turf.Feature]()
            for feature in viewModel.wayPointFeatures {
                
                if case let .string(featureType) = feature.properties?["type"]{
                    
                    if(type == featureType){
                        
                        turfArray.append(feature)
                       
                    }
                }
            }
            
            if(turfArray.count > 0){
                
                let amenityFeatureCollection = FeatureCollection(features: turfArray)
                self.updateAmenitiesOnMap(collection: amenityFeatureCollection,type: "\(type)")
            }
           
        }
        
       
    }
    
    //MARK: - Removing waypoints from existing route
    func removeWayPoints(location:CLLocationCoordinate2D,sName:String,feature:Turf.Feature,type:String){
        
       
        if(routeStopsAdded.count > 0){
            
            if let index = routeStopsAdded.firstIndex(where: {$0.name == sName}){
                
                routeStopsAdded.remove(at: index)
                viewModel.wayPointFeatures.remove(at: index)
                viewModel.wayPointsAdded = routeStopsAdded
            }
        }
        
        self.clearMap()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) { [weak self] in
            guard let self = self else { return }
            self.enableSave = true
            self.animateActivity(animate: true)
            self.viewModel.setupRoute()
            
        }
        self.removeLayerTypeFrom(type: type)
        self.addWayPointToMap(type: type)
        
    }
    
    //MARK: - Action methods
    
    func removeLayerTypeFrom(type:String){
        
//        self.viewModel.mobileRPMapViewUpdatable?.navigationMapView.mapView.removeAmenitiesPetrolAndEVLayer(type: "\(amenitySymbolThemeType)\(type)")
        self.viewModel.mobileRPMapViewUpdatable?.navigationMapView?.mapView.removeAmenitiesPetrolAndEVLayer(type: "\(type)")
    }
    
    private func parseAmenityImage(){
        
        let allKeys = AmenitiesSharedInstance.shared.petrolSymbolLayers.keys
        
        for key in allKeys {
            
            if AmenitiesSharedInstance.shared.petrolSymbolLayers.keys.contains(key){
                
                let array = AmenitiesSharedInstance.shared.petrolSymbolLayers[key] as! [Any]
                
                
                for item in array {

                  let itemDict = item as! [String:Any]
                    
                    if(itemDict["mapImageURL"] != nil){
                        
                        self.viewModel.mobileRPMapViewUpdatable?.navigationMapView?.mapView.addImagesToMapStyle(type: key, urlString: itemDict["mapImageURL"] as! String)
                        break;
                    }
                    
                   
                    
                }
                
            }
        }
    }
    
    private func getAmenitiesFeatures(location:[String:Any],type:String, selectedId: String = "")-> Turf.Feature {
        
        var addStop = false //Default value of add stop is false
        var isHideStopButton = false
        let coordinate = CLLocationCoordinate2D(latitude: location["lat"] as! Double, longitude: location["long"] as! Double)
        let name = location["name"] as! String
        let address = location["address"] as! String
        let sName = location["stationName"] as! String
        let amenityID = location["id"] as! String
        
        
        /// check if there are any existing waypoints added by comparing with station name
        print("check add petrol \(routeStopsAdded.count)")
        if(routeStopsAdded.count > 0){
            
            addStop = routeStopsAdded.contains(where: { $0.name == sName })
            
            if(addStop == false){
                
                if(routeStopsAdded.count == 3){
                    isHideStopButton = true
                }
                
            }
        }
        
        let isSelected = !selectedId.isEmpty && selectedId == amenityID
        var feature = Turf.Feature(geometry: .point(Point(coordinate)))
        feature.properties = [
            "type":.string(type),
            "name":.string(name),
            "address":.string(address),
            "stationName":.string(sName),
            "addStop":.boolean(!addStop), //Here we keep opposite value
            "addStopButton":.boolean(isHideStopButton),
            "image_id":.string("\(amenitySymbolThemeType)\(type)"),
            "amenityID":.string(amenityID),
            "isSelected": .boolean(isSelected)
        ]
        return feature
        
    }
    
    private func updateAmenitiesOnMap(collection:Turf.FeatureCollection,type:String){
        
//        self.viewModel.mobileRPMapViewUpdatable?.navigationMapView.mapView.removeAmenitiesPetrolAndEVLayer(type: "\(amenitySymbolThemeType)\(type)")
//        self.viewModel.mobileRPMapViewUpdatable?.navigationMapView.mapView.addPetrolAmentiesToMap(features: collection, isBelowPuck: true, type: "\(amenitySymbolThemeType)\(type)")
        
        self.viewModel.mobileRPMapViewUpdatable?.navigationMapView?.mapView.removeAmenitiesPetrolAndEVLayer(type: "\(type)")
        self.viewModel.mobileRPMapViewUpdatable?.navigationMapView?.mapView.addPetrolAmentiesToMap(features: collection, isBelowPuck: true, type: "\(type)")
    }
    
    private func callAmenitiesAPIForPetrol() {
        
        AmenitiesSharedInstance.shared.displayPetrolKioskItems()
        
        /// Call API for petrol Amenity with current location i.e Origin and destination
    
        if let viewModel = self.viewModel {
            
            if let currentAddress = viewModel.currentAddress, let address = viewModel.address {
                
                AmenitiesSharedInstance.shared.callAmenitiesApiForPetrol(location: CLLocationCoordinate2D(latitude: currentAddress.lat, longitude: currentAddress.long),destination:CLLocationCoordinate2D(latitude: address.lat, longitude: address.long))
                
            }
            
            
        }
        
        amenityPetrolCancellable = AmenitiesSharedInstance.shared.$isDisplayPetrolAmenities.sink { [weak self] isDisplayPetrolAmenities in
            guard let self = self else { return }
            
            if(isDisplayPetrolAmenities){
                
                self.petrolKiosk = AmenitiesSharedInstance.shared.itemsToDisplay
                
                if(self.routeStopsAdded.count > 0){
                    
                    DispatchQueue.main.async {
                      self.addWayPointToMap(type: Values.PETROL)
                    }
                }
            }
            
        }
    }
    
    /*
     To show all Petrol charger in map
     Petrol amenity have id == selectedID will be selected
     */
    func updateMapWithAmenities(selectedID: String = ""){
        
        if(self.selectedPetrolKioskItems.count > 0){
            self.filterNearBySelectedKiosk(selectedID: selectedID)
        }
        else{
            
            var turfArray = [Turf.Feature]()
            let allKeys = AmenitiesSharedInstance.shared.petrolSymbolLayers.keys
             
             //self.parseAmenityImage()
             for key in allKeys {
                 
                 if AmenitiesSharedInstance.shared.petrolSymbolLayers.keys.contains(key){
                     
                     let array = AmenitiesSharedInstance.shared.petrolSymbolLayers[key] as! [Any]
        
                     print(array)
                     for item in array {

                        let itemDict = item as! [String:Any]
                         
                         if(turfArray.count < 3){
                             
                             let feature = self.getAmenitiesFeatures(location: itemDict, type: key, selectedId: selectedID)
                             turfArray.append(feature)
                             
                         }
                        
                         
                     }
                     
                 }
             }
             
            let allDestKeys = AmenitiesSharedInstance.shared.petrolLayersNearDestination.keys
             
             //self.parseAmenityImage()
             for key in allDestKeys {
                 
                 if AmenitiesSharedInstance.shared.petrolLayersNearDestination.keys.contains(key){
                     
                     let array = AmenitiesSharedInstance.shared.petrolLayersNearDestination[key] as! [Any]
                     
                     
                     print(array)
                     for item in array {

                        let itemDict = item as! [String:Any]
                         
                         if(turfArray.count < 6){
                             
                             let feature = self.getAmenitiesFeatures(location: itemDict, type: key, selectedId: selectedID)
                             turfArray.append(feature)
                             
                         }
                        
                         
                     }
                     
                 }
             }
            
            
                
                
                let amenityFeatureCollection = FeatureCollection(features: turfArray)
                
                self.updateAmenitiesOnMap(collection: amenityFeatureCollection,type: Values.PETROL)
            
        }
        
    }
    
    private func filterNearBySelectedKiosk(selectedID: String = ""){
        
        if(selectedPetrolKioskItems.count > 0){
            
            var turfArray = [Turf.Feature]()
            let allKeys = AmenitiesSharedInstance.shared.petrolSymbolLayers.keys
            for key in allKeys {
                
                if AmenitiesSharedInstance.shared.petrolSymbolLayers.keys.contains(key){
                    
                    let array = AmenitiesSharedInstance.shared.petrolSymbolLayers[key] as! [Any]
                    
                    print(array)
                    for item in array {

                      let itemDict = item as! [String:Any]
                        let name = itemDict["name"] as! String
                        
                        for eName in self.selectedPetrolKioskItems {
                            
                            if(name.uppercased() == eName.uppercased()){
                                
                                let sName = itemDict["id"] as! String
                                if(routeStopsAdded.count > 0){
                                    
                                   let isContains = routeStopsAdded.contains(where: { $0.name == sName })
                                    if(!isContains){
                                        if(turfArray.count < 3){
                                          let feature = self.getAmenitiesFeatures(location: itemDict, type: key,selectedId: selectedID)
                                          turfArray.append(feature)
                                        }
                                    }
                                }
                                else{
                                    
                                    if(turfArray.count < 3){
                                        let feature = self.getAmenitiesFeatures(location: itemDict, type: key,selectedId: selectedID)
                                        turfArray.append(feature)
                                    }
                                    
                                }
                                
                            }
                            
                        }
                       
                       
                    }
                    
                }
            }
            
            let allDestKeys = AmenitiesSharedInstance.shared.petrolLayersNearDestination.keys
            for key in allDestKeys {
                
                if AmenitiesSharedInstance.shared.petrolLayersNearDestination.keys.contains(key){
                    
                    let array = AmenitiesSharedInstance.shared.petrolLayersNearDestination[key] as! [Any]
            
                    print(array)
                    for item in array {

                      let itemDict = item as! [String:Any]
                        let name = itemDict["name"] as! String
                        
                        for eName in self.selectedPetrolKioskItems {
                            
                            if(name.uppercased() == eName.uppercased()){
                                
                                let sName = itemDict["id"] as! String
                                if(routeStopsAdded.count > 0){
                                    
                                   let isContains = routeStopsAdded.contains(where: { $0.name == sName })
                                    if(!isContains){
                                        if(turfArray.count < 6){
                                          let feature = self.getAmenitiesFeatures(location: itemDict, type: key,selectedId: selectedID)
                                          turfArray.append(feature)
                                        }
                                    }
                                }
                                else{
                                    
                                    if(turfArray.count < 6){
                                        let feature = self.getAmenitiesFeatures(location: itemDict, type: key,selectedId: selectedID)
                                        turfArray.append(feature)
                                    }
                                    
                                }
                                
                            }
                            
                        }
                       
                       
                    }
                    
                }
            }
            
           
                
            let amenityFeatureCollection = FeatureCollection(features: turfArray)
                
            self.updateAmenitiesOnMap(collection: amenityFeatureCollection,type: Values.PETROL)
            
        }
        else{
            
            self.updateMapWithAmenities()
        }
        
    }
    
    private func callAmenitiesAPIForEV(){
        
        AmenitiesSharedInstance.shared.displayEVKioskItems()
        
        /// Call API for petrol Amenity with current location i.e Origin and destination
        if let viewModel = self.viewModel {
            
            if let currentAddress = viewModel.currentAddress, let address = viewModel.address {
                
                AmenitiesSharedInstance.shared.callAmenitiesApiForEV(location: CLLocationCoordinate2D(latitude: currentAddress.lat, longitude: currentAddress.long),destination:CLLocationCoordinate2D(latitude: address.lat, longitude: address.long))
                
            }
            
            
        }
       
        amenityEVCancellable = AmenitiesSharedInstance.shared.$isDisplayEVAmenities.sink { [weak self] isDisplayEVAmenities in
            guard let self = self else { return }
            
            if(isDisplayEVAmenities){
                
                self.evItems = AmenitiesSharedInstance.shared.evItemsToDisplay
                if(self.routeStopsAdded.count > 0){
                    
                    DispatchQueue.main.async {
                      self.addWayPointToMap(type: Values.EV_CHARGER)
                    }
                }
            }
        }
        
    }
    
    /*
     To show all EV charger in map
     EV amenity have id == selectedID will be selected
     */
    func updateMapWithAmenitiesForEV(selectedID: String = ""){
        
        if(selectedEVItems.count > 0){
            self.filterNearBySelectedEV(selectedID: selectedID)
        }
        else{
            
            var turfArray = [Turf.Feature]()
            let allKeys = AmenitiesSharedInstance.shared.evSymbolLayers.keys
             
             //self.parseAmenityImage()
             for key in allKeys {
                 
                 if AmenitiesSharedInstance.shared.evSymbolLayers.keys.contains(key){
                     
                     let array = AmenitiesSharedInstance.shared.evSymbolLayers[key] as! [Any]
                     
                     print(array)
                     for item in array {

                        let itemDict = item as! [String:Any]
                         if(turfArray.count < 3){
                             
                             let feature = self.getAmenitiesFeaturesForEV(location: itemDict, type: key, selectedId: selectedID)
                             turfArray.append(feature)
                         }
                         
                         
                     }
                    
                 }
             }
            
            let allDestKeys = AmenitiesSharedInstance.shared.evLayersNearDestination.keys
             
             //self.parseAmenityImage()
             for key in allDestKeys {
                 
                 if AmenitiesSharedInstance.shared.evLayersNearDestination.keys.contains(key){
                     
                     let array = AmenitiesSharedInstance.shared.evLayersNearDestination[key] as! [Any]
                
                     print(array)
                     for item in array {

                        let itemDict = item as! [String:Any]
                         if(turfArray.count < 6){
                             
                             let feature = self.getAmenitiesFeaturesForEV(location: itemDict, type: key, selectedId: selectedID)
                             turfArray.append(feature)
                         }
                         
                         
                     }
                     
                 }
             }
            
            let amenityFeatureCollection = FeatureCollection(features: turfArray)
            
            self.updateAmenitiesOnMap(collection: amenityFeatureCollection,type: Values.EV_CHARGER)
        }
        
    }
    
    private func getAmenitiesFeaturesForEV(location:[String:Any],type:String, selectedId: String = "")-> Turf.Feature {
        
        var addStop = false //Default value of add stop is false
        var isHideStopButton = false
        let coordinate = CLLocationCoordinate2D(latitude: location["lat"] as! Double, longitude: location["long"] as! Double)
        let name = location["name"] as! String
        let address = location["address"] as! String
        let sName = location["stationName"] as! String
        let amenityID = location["id"] as! String
        
        let multiplePlugTypes = location["plugType"] as! Array<String>
        
        var jsonArray = JSONArray()
        for type in multiplePlugTypes {

            let value = JSONValue(type)
            jsonArray.append(value)
        }
        
        /// check if there are any existing waypoints added by comparing with station name
        if(routeStopsAdded.count > 0){
            
            addStop = routeStopsAdded.contains(where: { $0.name == sName })
            
            if(addStop == false){
                
                if(routeStopsAdded.count == 3){
                    isHideStopButton = true
                }
                
            }
            
        }
        
        let isSelected = !selectedId.isEmpty && selectedId == amenityID
        var feature = Turf.Feature(geometry: .point(Point(coordinate)))
        feature.properties = [
            "type":.string(type),
            "name":.string(name),
            "address":.string(address),
            "stationName":.string(sName),
            "addStop":.boolean(!addStop), //Here we keep opposite value
            "image_id":.string("\(amenitySymbolThemeType)\(type)"),
            "addStopButton":.boolean(isHideStopButton),
            "plugTypes":.array(jsonArray),
            "amenityID":.string(amenityID),
            "isSelected": .boolean(isSelected)
        ]
        return feature
    }
    
    private func filterNearBySelectedEV(selectedID: String = ""){
        
        if(selectedEVItems.count > 0){
            
            var turfArray = [Turf.Feature]()
            var displayItemIDs = [String]()
            let allKeys = AmenitiesSharedInstance.shared.evSymbolLayers.keys
            for key in allKeys {
                
                if AmenitiesSharedInstance.shared.evSymbolLayers.keys.contains(key){
                    
                    let array = AmenitiesSharedInstance.shared.evSymbolLayers[key] as! [Any]
                
                    print(array)
                    for item in array {

                      let itemDict = item as! [String:Any]
                    
                      let displayTextList = itemDict["displayText"] as! [String]
                        
                        for eName in self.selectedEVItems {
                            
                            for name in displayTextList{
                                
                                if(name.uppercased() == eName.uppercased()){
                                    
                                    let sName = itemDict["id"] as! String
                                    if(routeStopsAdded.count > 0){
                                        
                                       let isContains = routeStopsAdded.contains(where: { $0.name == sName })
                                        if(!isContains){
                                            
                                            if(turfArray.count < 3){
                                                
                                                let feature = self.getAmenitiesFeaturesForEV(location: itemDict, type: key,selectedId: selectedID)
                                                if(displayItemIDs.count > 0){
                                                    
                                                    let isIDContains = displayItemIDs.contains(where: { $0 == sName })
                                                    
                                                    if(!isIDContains){
                                                        displayItemIDs.append(sName)
                                                        turfArray.append(feature)
                                                    }
                                                    
                                                }
                                                else{
                                                    displayItemIDs.append(sName)
                                                    turfArray.append(feature)
                                                }
                                                
                                            }
                                           
                                        }
                                    }
                                    else{
                                        
                                        if(turfArray.count < 3){
                                            let feature = self.getAmenitiesFeaturesForEV(location: itemDict, type: key,selectedId: selectedID)
                                            if(displayItemIDs.count > 0){
                                                
                                                let isIDContains = displayItemIDs.contains(where: { $0 == sName })
                                                
                                                if(!isIDContains){
                                                    displayItemIDs.append(sName)
                                                    turfArray.append(feature)
                                                }
                                                
                                            }
                                            else{
                                                displayItemIDs.append(sName)
                                                turfArray.append(feature)
                                            }
                                        }
                                       
                                    }
                                    
                                }
                            }
                            
                            
                        }
                        
                       
                    }
                    
                }
            }
            
            let allDestKeys = AmenitiesSharedInstance.shared.evLayersNearDestination.keys
            for key in allDestKeys {
                
                if AmenitiesSharedInstance.shared.evLayersNearDestination.keys.contains(key){
                    
                    let array = AmenitiesSharedInstance.shared.evLayersNearDestination[key] as! [Any]
                
                    print(array)
                    for item in array {

                      let itemDict = item as! [String:Any]
                        
                        
                        let displayTextList = itemDict["displayText"] as! [String]
                        
                        for eName in self.selectedEVItems {
                            
                            for name in displayTextList{
                                
                                if(name.uppercased() == eName.uppercased()){
                                    
                                    let sName = itemDict["id"] as! String
                                    if(routeStopsAdded.count > 0){
                                        
                                       let isContains = routeStopsAdded.contains(where: { $0.name == sName })
                                        if(!isContains){
                                            
                                            if(turfArray.count < 6){
                                                
                                                let feature = self.getAmenitiesFeaturesForEV(location: itemDict, type: key,selectedId: selectedID)
                                                if(displayItemIDs.count > 0){
                                                    
                                                    let isIDContains = displayItemIDs.contains(where: { $0 == sName })
                                                    
                                                    if(!isIDContains){
                                                        displayItemIDs.append(sName)
                                                        turfArray.append(feature)
                                                    }
                                                    
                                                }
                                                else{
                                                    displayItemIDs.append(sName)
                                                    turfArray.append(feature)
                                                }
                                            }
                                           
                                        }
                                    }
                                    else{
                                        
                                        if(turfArray.count < 6){
                                            let feature = self.getAmenitiesFeaturesForEV(location: itemDict, type: key,selectedId: selectedID)
                                            if(displayItemIDs.count > 0){
                                                
                                                let isIDContains = displayItemIDs.contains(where: { $0 == sName })
                                                
                                                if(!isIDContains){
                                                    displayItemIDs.append(sName)
                                                    turfArray.append(feature)
                                                }
                                                
                                            }
                                            else{
                                                displayItemIDs.append(sName)
                                                turfArray.append(feature)
                                            }
                                        }
                                       
                                    }
                                    
                                }
                            }
                            
                            
                        }
                        
                    }
                    
                }
            }
            
            
                let amenityFeatureCollection = FeatureCollection(features: turfArray)
                
                self.updateAmenitiesOnMap(collection: amenityFeatureCollection,type: Values.EV_CHARGER)
            
            
        }
        else{
            
            self.updateMapWithAmenitiesForEV()
        }
        
    }
    
    @IBAction func backbtnAction(){
        DispatchQueue.main.async {
         self.navigationController?.popViewController(animated: true)
        }
    }
    
    func clearValuesWhileRouteRefreshes(){
        
        self.navMapView?.mapView.removeAllPreferenceLayers()
        self.viewModel.mobileRPMapViewUpdatable?.defaultResponse = nil
        self.viewModel.mobileRPMapViewUpdatable?.bestOfRouteResponse = nil
        self.viewModel.mobileRPMapViewUpdatable?.cheapestResponse = nil
        self.viewModel.mobileRPMapViewUpdatable?.shortestResponse = nil
        self.viewModel.mobileRPMapViewUpdatable?.erpPriceUpdater = nil
        if(self.planTrip == nil){
            
            self.planTrip = nil
            self.viewModel.currentERPTime = nil
            self.viewModel.departTime = ""
            self.viewModel.arrivalTime = ""
            self.viewModel.departDate = nil
            self.viewModel.arrivalDate = nil
            self.tripEstArrivalInterval = 0.0
            self.tripEstDepartureInterval = 0.0
            self.heightBottomConstraint.constant = 269
            self.laterBtn.setTitle("Later", for: .normal)
            self.laterBtn.isHidden = false
            self.letsGoBtn.isHidden = false
            self.saveBtn.isHidden = true
            self.departArriveLbl.isHidden = true
            self.isDepartSelected = false
            self.tripEditArriveChanged = false
            self.tripEditDepartChanged = false
        }
        
        clearMap()
        self.parkingView.setParkingMode(type: .hide)
//        self.parkingButton.select(false)
        
        self.ctrBottomCongestion.constant = NewRoutePlanningVCConst.bottomConstOff
        self.ctrBottomVoucherView.constant = NewRoutePlanningVCConst.bottomVoucherOff
    }
    
    @IBAction func reversebtnAction() {
       
        guard currentRoute != nil else { return }
        
        guard !self.isAnimating else { return }
        
        self.destinationCarPark = nil
        
        if(planTrip != nil){
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.EditRoutePlanning.UserClick.origin_swap, screenName: ParameterName.EditRoutePlanning.screen_view)
        }
        else{
            
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.RoutePlanning.UserClick.origin_swap, screenName: ParameterName.RoutePlanning.screen_view)
        }
        
        self.isPlanTripFromERPDetails = false
        let tempLbl = sourceLbl.text
        sourceLbl.text = destLbl.text
        destLbl.text = tempLbl
        
        guard let destinationAddress = viewModel.baseAddress else {
            
            return
        }
        
//        didDeselectCarparkAnnotation()
//        isParkingOn = false
//        self.parkingButton.setImage(UIImage(named: "parkingOff"), for: .normal)

        /// setting current address to destination if there is an existing base address
        clearValuesWhileRouteRefreshes()
        if let currentAddress = viewModel.currentBaseAddress{
            self.viewModel.baseAddress = currentAddress
            var needShowBroadcast: Bool = false
            if let address = self.viewModel.baseAddress as? SearchAddresses {
                if address.isCarpark() {
                    needShowBroadcast = true
                }
            }else if let address = self.viewModel.baseAddress as? EasyBreezyAddresses {
                if address.isCarpark() {
                    needShowBroadcast = true
                }
            }
            self.showCarparkBroadcastMessage(needShowBroadcast, title: self.viewModel.broadcastMessage?.getBroadcastMessage() ?? "")
            
            self.viewModel.setRPAddress()
        } else {
            /// else set current location as base address which is destination
            getAddressFromLocation(location: LocationManager.shared.location) { address in
                let address = SearchAddresses(alias: "", address1: address, lat: "\(LocationManager.shared.location.coordinate.latitude)", long: "\(LocationManager.shared.location.coordinate.longitude)", address2: address, distance: "")
                
                self.originalAddress = address
                self.viewModel.baseAddress = self.originalAddress
                self.viewModel.setRPAddress()
                
            }
        }
        
        /// setting destination as current address
        self.viewModel.currentBaseAddress = destinationAddress
        self.viewModel.setRPCurrentAddress()
        self.viewModel.wayPointsAdded.removeAll()
        
        DispatchQueue.global(qos: .background).async {
            
            DispatchQueue.main.async {
                
                self.enableSave = true
                self.animateActivity(animate: true)
                //if self.routeStopsAdded.count > {
                    
                if(self.routeStopsAdded.count > 0){
                    
                       self.viewModel.wayPointsAdded = self.routeStopsAdded
                    }
                //}
                self.viewModel.setupRoute()
                
                self.viewModel.getZoneColors()
                
            }
        }
    }
    
    private func animateActivity(animate: Bool) {
        isAnimating = animate
    }
    
    private func threadSafeActivity(animate: Bool) {
        if animate {
            self.activityIndicator.startAnimating()
        } else {
            self.activityIndicator.stopAnimating()
        }
    }
    
    private func setSourceAnnotationIcon(){
        
        guard let currentAddress = viewModel.currentAddress else {
            
            return
        }
        let currentSearchLoc: CLLocation = CLLocation(latitude: currentAddress.lat,
                                                       longitude: currentAddress.long)
        let currentUserLoc = LocationManager.shared.location
        
        let distance = LocationManager.shared.getDistanceInMeters(fromLoc: currentUserLoc, toLoc: currentSearchLoc)
        
        if(distance > 5){
            
            var customPointAnnotation = PointAnnotation(coordinate: currentSearchLoc.coordinate)
            customPointAnnotation.image = .init(image: UIImage(named: "source_ann")!, name: "source")
            navMapView.pointAnnotationManager?.annotations.append(customPointAnnotation)
        }
    }
    
    private func openRNScreen(){
        
        let params = [ // Object data to be used by that scree
             "fromScreen":"RoutePlanning",
             "baseURL": appDelegate().backendURL,
             "idToken":AWSAuth.sharedInstance.idToken,
             "deviceos":parameters!["deviceos"],
             "deviceosversion":parameters!["deviceosversion"],
             "devicemodel":parameters!["devicemodel"],
             "appversion":parameters!["appversion"],
        ]
         self.goToRNScreen(toScreen: RNScreeNames.SERACH_LOCATION,navigationParams: params as [String:Any])
    }
    
   @objc func sourceTapped(_ sender: UITapGestureRecognizer) {
       print("source tapped")
       
       guard currentRoute != nil else { return }
       
       if(planTrip != nil){
           AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.EditRoutePlanning.UserClick.search_origin, screenName: ParameterName.EditRoutePlanning.screen_view)
       }
       else{
           AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.RoutePlanning.UserClick.search_origin, screenName: ParameterName.RoutePlanning.screen_view)
       }
              
       isSourceSelected = true
       isDestinationSelected = false
       self.openRNScreen()
       
    }
    
    @objc func destinationTapped(_ sender: UITapGestureRecognizer) {
        print("destination tapped")
        
        guard currentRoute != nil else { return }
        
        if(planTrip != nil){
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.EditRoutePlanning.UserClick.search_destination, screenName: ParameterName.EditRoutePlanning.screen_view)
        }
        else{
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.RoutePlanning.UserClick.search_destination, screenName: ParameterName.RoutePlanning.screen_view)
        }
        
        isSourceSelected = false
        isDestinationSelected = true
        self.openRNScreen()
    }

    @objc func directionTapped(_ sender: UITapGestureRecognizer) {
             print("direction tapped")
        
        self.directionbtnAction()
        
     }
    
    @objc func notifyTapped(_ sender: UITapGestureRecognizer) {
             print("notify arrival tapped")
        
//        if(planTrip != nil){
//            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.EditRoutePlanning.UserClick.notify_arrival, screenName: ParameterName.EditRoutePlanning.screen_view)
//        }
//        else{
//            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.RoutePlanning.UserClick.notify_arrival, screenName: ParameterName.RoutePlanning.screen_view)
//        }
        
            
        self.notifybtnAction()
        
     }
    
    
    @IBAction func directionbtnAction(){
        print("Direction")
        
        self.didDeselectAnnotation()
        if(isPlanTripFromERPDetails){
            
            guard let currentRoute = currentRoute,
                  let firstLegh = currentRoute.legs.first else {
                return
            }
            
            
            guard let responseOptions = viewModel.mobileRPMapViewUpdatable?.routePreferenceData?.routeResponse?.options,
                  case let .route(routeOptions) = responseOptions,
                  case _ = viewModel.mobileRPMapViewUpdatable?.routePreferenceData?.routeResponse else { return }
            
            if (planTrip != nil) {
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.EditRoutePlanning.UserClick.direction, screenName: ParameterName.EditRoutePlanning.screen_view)
            } else {
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.RoutePlanning.UserClick.direction, screenName: ParameterName.RoutePlanning.screen_view)
            }
            
            //print(currentRoute?.legs[0].steps)
            let routeProgress = RouteProgress(route: currentRoute, options: routeOptions)
            let storyboard: UIStoryboard = UIStoryboard(name: "NewRoutePlanning", bundle: nil)
            let vc: DirectionsVC = storyboard.instantiateViewController(withIdentifier: "DirectionsVC")  as! DirectionsVC
            vc.routeProgress = routeProgress
            vc.leg = firstLegh
            vc.modalPresentationStyle = .formSheet
            self.present(vc, animated: true, completion: nil)
        }
        else{
            
            var selectedResponseIndex = -1
            //var selectedRouteIndex = 0
            
            //Checking nil condition for original response
            if let orignalRouteResponses = viewModel.mobileRPMapViewUpdatable?.originalResponse{
                
                guard let currentRoute = currentRoute else {
                    return
                }

                for (i,r) in orignalRouteResponses.enumerated() {
        
                        if let _ = r.routes?.firstIndex(of: currentRoute) {
                            selectedResponseIndex = i
                            break
                        }
                }
            }
            
            
            guard selectedResponseIndex >= 0,
                  let route = viewModel.mobileRPMapViewUpdatable?.originalResponse[selectedResponseIndex].routes?.first,
                  let responseOptions = viewModel.mobileRPMapViewUpdatable?.originalResponse[selectedResponseIndex].options,
                  case let .route(routeOptions) = responseOptions else { return }
            
            if(planTrip != nil){
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.EditRoutePlanning.UserClick.direction, screenName: ParameterName.EditRoutePlanning.screen_view)
            }
            else{
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.RoutePlanning.UserClick.direction, screenName: ParameterName.RoutePlanning.screen_view)
            }
            
            
            //print(currentRoute?.legs[0].steps)
            let routeProgress = RouteProgress(route: route, options: routeOptions)
            let storyboard: UIStoryboard = UIStoryboard(name: "NewRoutePlanning", bundle: nil)
            let vc: DirectionsVC = storyboard.instantiateViewController(withIdentifier: "DirectionsVC")  as! DirectionsVC
            vc.routeProgress = routeProgress
            if let firstLegh = currentRoute?.legs.first {
                vc.leg = firstLegh
            }
            
            vc.modalPresentationStyle = .formSheet
            self.present(vc, animated: true, completion: nil)
        }
        
    }
    
    @IBAction func notifybtnAction(){
        
        //  Call API to share destination
        let destination = self.getDestAddress()
        shareLocationFromNative(address1: destination?.address1 ?? "", address2: destination?.address2 ?? "", latitude: (destination?.lat ?? 0.0).toString(), longtitude: (destination?.long ?? 0.0).toString(), amenity_id: destination?.amenityId ?? "", amenity_type: destination?.amenityType ?? "", name: destination?.address1 ?? "", fullAddress: destination?.fullAddress ?? "", type: "ROUTE_PLANNING", placeId: destination?.placeId ?? "", userLocationLinkIdRef: destination?.userLocationLinkIdRef)
        
        /*
        guard currentRoute != nil else { return }
        
        if self.etaInfo != nil {
            
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.RoutePlanning.UserClick.share_drive_close, screenName: ParameterName.RoutePlanning.screen_view)
            DispatchQueue.main.async {
                // show confirmation
                self.popupAlert(title: nil, message: "\(Constants.cancelShareDriveConfirmation) \(self.etaInfo?.recipientName ?? "")", actionTitles: ["No",Constants.yes], actions:[{action1 in
                    
//                    AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Navigation.UserClick.pop_user_end_no, screenName: ParameterName.Navigation.screen_view)
                    
                },{ [weak self] action2 in
                    guard let self = self else { return }

                    self.notifyLbl.text = Constants.cpShareDrive
                    self.enableSave = true
                    self.notifyBtn.setImage(UIImage(named: "notifyArrvl"), for: .normal)
                    self.etaInfo = nil
                    
//                    AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Navigation.UserClick.pop_user_end_yes, screenName: ParameterName.Navigation.screen_view)
                    
                }, nil])
//                self.notifyLbl.text = "RP-ShareDrive".localized
            }
        } else {
            // show share drive screen
            
            let address = viewModel.address != nil ? viewModel.address.address : "destination"
            etaChildVC = showShareDriveScreen(viewController: self,address: address, etaTime: self.tripETATime, fromScreen: "routePlanning")
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.RoutePlanning.UserClick.share_drive_open, screenName: ParameterName.RoutePlanning.screen_view)
        }
         */
    }
    
    @objc func openDatePicker(_ sender: UITapGestureRecognizer) {
             print("notify arrival tapped")
        
        if planTrip != nil {
            
            self.updateTripTime()
        }
        else{
            
            self.laterbtnAction()
        }
        
     }
    
    private func updateTripTime(){
        
        if let planTrip = planTrip {
            
            var planBy = planTrip.planBy
            let datePicker = RPDatePickerView()
            
            self.datePickerView = datePicker
            
            self.view.addSubview(datePicker)
            datePicker.snp.makeConstraints { (make) in
                make.bottom.equalTo(self.view.snp.bottom).offset(0)
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(0)
                make.height.equalTo(410)
            }
             tripEstArrivalInterval = 0.0
             tripEstDepartureInterval = 0.0
            
            if(planBy == Values.DEPART_AT){
                
                let selectedDateString = Date(timeIntervalSince1970: planTrip.startTime).formatted
                let selectedDate = Date.getDate(str: selectedDateString)
                self.previousSelectedDate = selectedDate
                
                self.arrivalHeaderLbl.text = "Est.Arrival"
                self.durationHeaderLbl.text = "Est.Duration"
                
                tripEstArrivalInterval = planTrip.arrivalTime
                
                let durationSet = self.viewModel.getRouteDurationFromTimeInterval(expectedTravelTime: currentRoute?.expectedTravelTime ?? 0.0)
                
                self.durationLbl.set(durationSet.0,durationSet.1)
                
                let arrivalSet = estimatedArrivalTime(currentRoute?.expectedTravelTime ?? 0.0, date: selectedDate)
                
                self.tripETATime = "\(arrivalSet.0)\(arrivalSet.1)"
              
                self.arrivalLbl.set(arrivalSet.0, arrivalSet.1)
              
                self.arrivalLbl.set(arrivalSet.0, arrivalSet.1)
            }
            else{
                
                let selectedDateString = Date(timeIntervalSince1970: planTrip.arrivalTime).formatted
                let selectedDate = Date.getDate(str: selectedDateString)
                
                tripETATime = selectedDate.getTimeFromDate()
                
                self.previousSelectedDate = selectedDate
                
                tripEstDepartureInterval = planTrip.startTime
                
                self.arrivalHeaderLbl.text = "Est.Departure"
                self.durationHeaderLbl.text = "Est.Duration"
                
                let durationSet = self.viewModel.getRouteDurationFromTimeInterval(expectedTravelTime: currentRoute?.expectedTravelTime ?? 0.0)
                
                self.durationLbl.set(durationSet.0,durationSet.1)
                
                let arrivalSet = estimatedArrivalTime(-(currentRoute?.expectedTravelTime ?? 0.0), date: selectedDate)
              
                self.arrivalLbl.set(arrivalSet.0, arrivalSet.1)
              
                self.arrivalLbl.set(arrivalSet.0, arrivalSet.1)
            }
           
            self.datePickerView?.cancelDatePicker = { [weak self] (cancel) in
                guard let self = self else { return }
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.EditRoutePlanning.UserClick.datetime_cancel, screenName: ParameterName.EditRoutePlanning.screen_view)
                self.datePickerView?.removeFromSuperview()
            }
            
            self.datePickerView?.nextDatePicker = { [weak self] (formattedStr,timeInterval) in
                guard let self = self else { return }
               
                let selectedDateString = Date(timeIntervalSince1970: timeInterval).formatted
                let selectedDate = Date.getDate(str: selectedDateString)
                self.previousSelectedDate = selectedDate
                self.heightBottomConstraint.constant = 294
                self.departArriveLbl.isHidden = false
                print(formattedStr)
                self.enableSave = true
                if(self.datePickerView!.isDepartSelected){
                    
                    self.tripEditDepartChanged = true
                    self.tripEditArriveChanged = false
                    self.departArriveLbl.set("Depart at ", formattedStr)
                    self.arrivalHeaderLbl.text = "Est.Arrival"
                    self.durationHeaderLbl.text = "Est.Duration"
                    let interval = self.currentRoute?.expectedTravelTime
                    self.tripEstArrivalInterval =  interval! + timeInterval
                    
                    let arrivalDate = Date(timeIntervalSince1970: self.tripEstArrivalInterval)
                    
                    print("est arrival time",arrivalDate.getTimeFromDate())
                    
                    let durationSet = self.viewModel.getRouteDurationFromTimeInterval(expectedTravelTime: interval!)
                    //durationLbl.text = "\(durationSet.0) \(durationSet.1)"
                    self.durationLbl.set(durationSet.0,durationSet.1)
                    
                    let arrivalSet = estimatedArrivalTime(interval!, date: selectedDate)
                    
                    self.arrivalLbl.set(arrivalSet.0, arrivalSet.1)
                    
                    self.arrivalLbl.set(arrivalSet.0, arrivalSet.1)
                    
                    self.tripETATime = "\(arrivalSet.0)\(arrivalSet.1)"
                                        
                    self.viewModel.departTime = DateUtils.shared.getTimeDisplay(dateFormat: Date.formatYYYYMMddTHHmm, date: selectedDate)
                    self.viewModel.arrivalTime = ""
                    
                    self.viewModel.departDate = selectedDate
                    self.viewModel.arrivalDate = nil
                    
                    self.viewModel.currentERPTime = self.previousSelectedDate
                    
                    self.viewModel.mobileRPMapViewUpdatable?.erpPriceUpdater.currentERPTime = self.viewModel.currentERPTime
                    
                    AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.EditRoutePlanning.UserToggle.depart_at, screenName: ParameterName.EditRoutePlanning.screen_view)
                   
                }
                else{
                    
                    self.tripEditDepartChanged = false
                    self.tripEditArriveChanged = true
                    self.arrivalHeaderLbl.text = "Est.Departure"
                    self.durationHeaderLbl.text = "Est.Duration"
                    self.departArriveLbl.set("Arrive by ", formattedStr)
                    self.tripEstDepartureInterval = timeInterval - (self.currentRoute?.expectedTravelTime)!
                    
                    self.tripETATime = selectedDate.getTimeFromDate()
                    
                    _ = self.viewModel.getRouteDurationFromTimeInterval(expectedTravelTime: self.tripEstDepartureInterval)
                    
                    //self.durationLbl.set(durationSet.0,durationSet.1)
                    
                    let arrivalSet = estimatedArrivalTime(-((self.currentRoute?.expectedTravelTime ?? 0)), date: selectedDate)
                  
                    self.arrivalLbl.set(arrivalSet.0, arrivalSet.1)
                    
                    let arrivalDate = Date(timeIntervalSince1970: self.tripEstDepartureInterval)
                    
                    self.viewModel.currentERPTime = Date(timeIntervalSince1970: timeInterval)
                    
                    self.viewModel.mobileRPMapViewUpdatable?.erpPriceUpdater.currentERPTime = self.viewModel.currentERPTime
                    
                    print("est departure time",arrivalDate.getTimeFromDate())
                    
                    self.viewModel.arrivalTime = DateUtils.shared.getTimeDisplay(dateFormat: Date.formatYYYYMMddTHHmm, date: selectedDate)
                    self.viewModel.departTime = ""
                    
                    self.viewModel.arrivalDate = selectedDate
                    self.viewModel.departDate = nil
                    
                    AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.EditRoutePlanning.UserToggle.arrive_by, screenName: ParameterName.EditRoutePlanning.screen_view)
                    
                }
                
                let tripEstStartTime = self.datePickerView!.isDepartSelected ? Int(timeInterval) : Int(self.tripEstDepartureInterval)
                self.tripEstStartTime = Double(tripEstStartTime)
                
                let tripEstArrivalTime = self.datePickerView!.isDepartSelected ? Int(self.tripEstArrivalInterval) : Int(timeInterval)
                
                 planBy = self.datePickerView!.isDepartSelected ? Values.DEPART_AT : Values.ARRIVE_BY
                
                var coordinates:[[Double]] = []
                for coordinate in self.currentRoute?.shape?.coordinates ?? [] {
                    
                    coordinates.append([Double(coordinate.latitude ),Double(coordinate.longitude)])
                    
                }
//                for i in 0...(((self.currentRoute?.shape?.coordinates.count)!) - 2) {
//
//                    let coordinate = self.currentRoute?.shape?.coordinates[i]
//
//                    coordinates.append([Double(coordinate?.latitude ?? 0),Double(coordinate?.longitude ?? 0)])
//
//                }
                
                var routeStops = [RouteStops]()
                if(self.routeStopsAdded.count > 0){
                    
                    for feature in self.viewModel.wayPointFeatures{
                        
                        if case let .point(point) = feature.geometry,
                           case let .string(type) = feature.properties?["type"],
                            case let .string(amenityID) = feature.properties?["amenityID"] {
                            
                            let routeStop = RouteStops(amenityId: amenityID, coordinates: [point.coordinates.latitude,point.coordinates.longitude], type: type)
                            routeStops.append(routeStop)
                            
                        }
                    }
                }
                var erpEntries = [Int]()
                if let viewModel = self.viewModel{
                    
                    if let priceUpdater = viewModel.mobileRPMapViewUpdatable?.erpPriceUpdater{
                        
                        if(priceUpdater.allERPInSelectedRoute.count > 0){
                            
                            for feature in priceUpdater.allERPInSelectedRoute {
                                
                                if let selectedFeatureProperties = feature.properties,
                                   case let .string(erpId) = selectedFeatureProperties["erpid"]
                                {
                                    erpEntries.append(Int(erpId)!)
                                }
                            }
                        }
                    }
                }
                var erpPrice = 0.0
                if let erpText = self.erpLbl.text{
                    
                    let priceArray = erpText.components(separatedBy: "$")
                    let doublePrice = Float(priceArray[1]) ?? 0.0
                    erpPrice = Double(doublePrice)
                }
                
                
                 
                self.tripUploadRequest = TripPlanUploadRequest(tripEstStartTime: Int(tripEstStartTime), tripEstArrivalTime: Int(tripEstArrivalTime), tripStartAddress1: self.viewModel.currentAddress.address, tripStartAddress2: self.viewModel.currentAddress.address1, tripStartLat: self.viewModel.currentAddress.lat, tripStartLong: self.viewModel.currentAddress.long, tripDestAddress1: self.viewModel.address.address, tripDestAddress2: self.viewModel.address.address1, tripDestLat: self.viewModel.address.lat, tripDestLong: self.viewModel.address.long, totalDuration: Int(self.currentRoute?.expectedTravelTime ?? 0), totalDistance: Int(self.currentRoute?.distance ?? 0), erpExpense: erpPrice, tripPlanBy: planBy, erpEntries: erpEntries, routeStops:routeStops, routeCoordinates: coordinates,tripEtaFavouriteId: self.etaInfo == nil ? nil : self.etaInfo?.etaFavoriteId)
                 
               
                self.datePickerView?.removeFromSuperview()
                
                //DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) { [weak self] in
                    self.setupRoute()
                //}
                
            }
            
            if let previousSelectedDate = previousSelectedDate {
                datePicker.previousSelectedDate = previousSelectedDate
            }
            
            datePicker.isDepartSelected = planBy == Values.DEPART_AT ? true : false
            
            
        }
        
    }
    
    @IBAction func laterbtnAction(){
        print("Later")
        guard currentRoute != nil else { return }
        
        
        if planTrip != nil {
            
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.EditRoutePlanning.UserClick.savelater, screenName: ParameterName.EditRoutePlanning.screen_view)
            self.saveBtnAction()
        }
        else{
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.RoutePlanning.UserClick.savelater, screenName: ParameterName.RoutePlanning.screen_view)
            self.saveTrip()
        }
        
    }
/*
    private func estimatedArrivalTime(_ duration: TimeInterval,date:Date) -> (time: String,format: String) {
        let arrivalDate = Date(timeInterval: duration, since: date)
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: arrivalDate)
//        let t = hour > 12 ? hour - 12: hour
//        let hourString = t < 10 ? "0\(t)" : t.description
        let minute = calendar.component(.minute, from: arrivalDate)
        let minString = minute < 10 ? "0\(minute)" : minute.description
//        return (time: "\(hourString):\(minString)", format: hour >= 12 ? " pm" : " am")
        // comment out showing in 12 hour format for the time being
        return (time: "\(hour):\(minString)", format: "")
    }
*/
//    private func estimatedDepartureTime(_ duration: TimeInterval,date:Date) -> (time: String,format: String) {
//        let arrivalDate = Date(timeInterval: <#T##TimeInterval#>, since: <#T##Date#>)
//        let calendar = Calendar.current
//        let hour = calendar.component(.hour, from: arrivalDate)
//        let t = hour > 12 ? hour - 12: hour
//        let hourString = t < 10 ? "0\(t)" : t.description
//        let minute = calendar.component(.minute, from: arrivalDate)
//        let minString = minute < 10 ? "0\(minute)" : minute.description
//        return (time: "\(hourString):\(minString)", format: hour >= 12 ? " pm" : " am")
//    }
    
    private func saveTrip(){
        
        let datePicker = RPDatePickerView()
        
        self.datePickerView = datePicker
        
        self.view.addSubview(datePicker)
        datePicker.snp.makeConstraints { (make) in
            make.bottom.equalTo(self.view.snp.bottom).offset(0)
            make.left.equalToSuperview().offset(0)
            make.right.equalToSuperview().offset(0)
            make.height.equalTo(410)
        }
        
       
        self.datePickerView?.cancelDatePicker = { [weak self] (cancel) in
            guard let self = self else { return }
           
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.RoutePlanning.UserClick.datetime_cancel, screenName: ParameterName.RoutePlanning.screen_view)
                        
            self.datePickerView?.removeFromSuperview()
        }
        
        self.datePickerView?.nextDatePicker = { [weak self] (formattedStr,timeInterval) in
            guard let self = self else { return }
           
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.RoutePlanning.UserClick.datetime_next, screenName: ParameterName.RoutePlanning.screen_view)
                        
            let selectedDateString = Date(timeIntervalSince1970: timeInterval).formatted
            let selectedDate = Date.getDate(str: selectedDateString)
            print("TIME",time)
            self.previousSelectedDate = selectedDate
            self.heightBottomConstraint.constant = 294
            self.departArriveLbl.isHidden = false
            print(formattedStr)
            self.tripEstArrivalInterval = 0.0
            self.tripEstDepartureInterval = 0.0
            if(self.datePickerView!.isDepartSelected){
                
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.RoutePlanning.UserToggle.depart_at, screenName: ParameterName.RoutePlanning.screen_view)
                               
                self.isDepartSelected = true
                self.departArriveLbl.set("Depart at ", formattedStr)
                self.arrivalHeaderLbl.text = "Est.Arrival"
                self.durationHeaderLbl.text = "Est.Duration"
                let interval = self.currentRoute?.expectedTravelTime
                self.tripEstArrivalInterval =  interval! + timeInterval
                self.viewModel.currentERPTime = self.previousSelectedDate
                self.viewModel.mobileRPMapViewUpdatable?.erpPriceUpdater.currentERPTime = self.viewModel.currentERPTime
                let durationSet = self.viewModel.getRouteDurationFromTimeInterval(expectedTravelTime: interval!)
                //durationLbl.text = "\(durationSet.0) \(durationSet.1)"
                self.durationLbl.set(durationSet.0,durationSet.1)
                
                let arrivalSet = estimatedArrivalTime(interval!, date: selectedDate)
                
                self.arrivalLbl.set(arrivalSet.0, arrivalSet.1)
                
                self.tripETATime = "\(arrivalSet.0)\(arrivalSet.1)"
                
                let arrivalDate = Date(timeIntervalSince1970: self.tripEstArrivalInterval)
                
                print("est arrival time",arrivalDate.getTimeFromDate())
            
                self.viewModel.departTime = DateUtils.shared.getTimeDisplay(dateFormat: Date.formatYYYYMMddTHHmm, date: selectedDate)
                self.viewModel.arrivalTime = ""
                
                self.viewModel.departDate = selectedDate
                self.viewModel.arrivalDate = nil
                
                self.saveBtn.isEnabled = true
                self.saveBtn.backgroundColor = UIColor(named: "ratingBtnColor")!
                self.displayBroadcastMessage()
                
            }
            else{
                
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.RoutePlanning.UserToggle.arrive_by, screenName: ParameterName.RoutePlanning.screen_view)
                                
                self.tripETATime = selectedDate.getTimeFromDate()
                
                self.isDepartSelected = false
                self.arrivalHeaderLbl.text = "Est.Departure"
                self.durationHeaderLbl.text = "Est.Duration"
                self.departArriveLbl.set("Arrive by ", formattedStr)
                
                self.tripEstDepartureInterval = timeInterval - (self.currentRoute?.expectedTravelTime)!
                
                let durationSet = self.viewModel.getRouteDurationFromTimeInterval(expectedTravelTime: self.currentRoute?.expectedTravelTime ?? 0.0)
                
                self.durationLbl.set(durationSet.0,durationSet.1)
                
                let arrivalSet = estimatedArrivalTime(-((self.currentRoute!.expectedTravelTime)), date: selectedDate)
              
                self.arrivalLbl.set(arrivalSet.0, arrivalSet.1)
                
                let arrivalDate = Date(timeIntervalSince1970: self.tripEstDepartureInterval)
            
                print("est departure time",arrivalDate.getTimeFromDate())
                self.selectedTime = arrivalDate.getTimeFromDate()
                self.viewModel.currentERPTime = Date(timeIntervalSince1970: timeInterval)
                
                self.viewModel.mobileRPMapViewUpdatable?.erpPriceUpdater.currentERPTime = self.viewModel.currentERPTime
                
                self.viewModel.arrivalTime = DateUtils.shared.getTimeDisplay(dateFormat: Date.formatYYYYMMddTHHmm, date: selectedDate)
                self.viewModel.departTime = ""
                self.viewModel.arrivalDate = selectedDate
                self.viewModel.departDate = nil
                
                self.saveBtn.isEnabled = true
                self.saveBtn.backgroundColor = UIColor(named: "ratingBtnColor")!
                self.displayBroadcastMessage()
            }
            
            let tripEstStartTime = self.datePickerView!.isDepartSelected ? Int(timeInterval) : Int(self.tripEstDepartureInterval)
            self.tripEstStartTime = Double(tripEstStartTime)
            
            let tripEstArrivalTime = self.datePickerView!.isDepartSelected ? Int(self.tripEstArrivalInterval) : Int(timeInterval)
            
            let tripPlanBy = self.datePickerView!.isDepartSelected ? Values.DEPART_AT : Values.ARRIVE_BY
            
            var coordinates:[[Double]] = []
            
            for coordinate in self.currentRoute?.shape?.coordinates ?? [] {
                
                coordinates.append([Double(coordinate.latitude ),Double(coordinate.longitude)])
                
            }
//            for i in 0...(((self.currentRoute?.shape?.coordinates.count)!) - 2) {
//
//                let coordinate = self.currentRoute?.shape?.coordinates[i]
//
//                coordinates.append([Double(coordinate?.latitude ?? 0),Double(coordinate?.longitude ?? 0)])
//
//            }
            
            var routeStops = [RouteStops]()
            if(self.routeStopsAdded.count > 0){
                
                for feature in self.viewModel.wayPointFeatures{
                    
                    if case let .point(point) = feature.geometry,
                       case let .string(type) = feature.properties?["type"],
                        case let .string(amenityID) = feature.properties?["amenityID"] {
                        
                        let routeStop = RouteStops(amenityId: amenityID, coordinates: [point.coordinates.latitude,point.coordinates.longitude], type: type)
                        routeStops.append(routeStop)
                        
                    }
                }
            }
            var erpEntries = [Int]()
            if let viewModel = self.viewModel{
                
                if let priceUpdater = viewModel.mobileRPMapViewUpdatable?.erpPriceUpdater{
                    
                    if(priceUpdater.allERPInSelectedRoute.count > 0){
                        
                        for feature in priceUpdater.allERPInSelectedRoute {
                            
                            if let selectedFeatureProperties = feature.properties,
                               case let .string(erpId) = selectedFeatureProperties["erpid"]
                            {
                                erpEntries.append(Int(erpId)!)
                            }
                        }
                    }
                }
            }
            var erpPrice = 0.0
            if let erpText = self.erpLbl.text{
                
                let priceArray = erpText.components(separatedBy: "$")
                let doublePrice = Float(priceArray[1]) ?? 0.0
                erpPrice = Double(doublePrice)
            }
           
            
            self.tripUploadRequest = TripPlanUploadRequest(tripEstStartTime: tripEstStartTime, tripEstArrivalTime: tripEstArrivalTime, tripStartAddress1: self.viewModel.currentAddress.address, tripStartAddress2: self.viewModel.currentAddress.address1, tripStartLat: self.viewModel.currentAddress.lat, tripStartLong: self.viewModel.currentAddress.long, tripDestAddress1: self.viewModel.address.address, tripDestAddress2: self.viewModel.address.address1, tripDestLat: self.viewModel.address.lat, tripDestLong: self.viewModel.address.long, totalDuration: Int(self.currentRoute?.expectedTravelTime ?? 0), totalDistance: Int(self.currentRoute?.distance ?? 0), erpExpense: erpPrice, tripPlanBy: tripPlanBy, erpEntries: erpEntries, routeStops:routeStops, routeCoordinates: coordinates,tripEtaFavouriteId: self.etaInfo == nil ? nil : self.etaInfo?.etaFavoriteId)
            
            
            
            self.laterBtn.isHidden = true
            self.letsGoBtn.isHidden = true
            self.saveBtn.isHidden = false
            self.datePickerView?.removeFromSuperview()
            
            //DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) { [weak self] in
                self.setupRoute()
            //}
            
        }
        
        if let previousSelectedDate = previousSelectedDate {
            datePicker.previousSelectedDate = previousSelectedDate
        }
        
        datePicker.isDepartSelected = self.isDepartSelected
    }
    
    private func getUpdatedRoutesAndERPInfo(){
        
        if let currentRoute = currentRoute {
            
            var coordinates:[[Double]] = []
            for coordinate in currentRoute.shape?.coordinates ?? [] {
                
                coordinates.append([Double(coordinate.latitude ),Double(coordinate.longitude)])
                
            }
//            for i in 0...(((currentRoute.shape?.coordinates.count)!) - 2) {
//
//                let coordinate = currentRoute.shape?.coordinates[i]
//
//                coordinates.append([Double(coordinate?.latitude ?? 0),Double(coordinate?.longitude ?? 0)])
//
//            }
            
            var routeStops = [RouteStops]()
            if(self.routeStopsAdded.count > 0){
                
                for feature in self.viewModel.wayPointFeatures{
                    
                    if case let .point(point) = feature.geometry,
                       case let .string(type) = feature.properties?["type"],
                        case let .string(amenityID) = feature.properties?["amenityID"] {
                        
                        let routeStop = RouteStops(amenityId: amenityID, coordinates: [point.coordinates.latitude,point.coordinates.longitude], type: type)
                        routeStops.append(routeStop)
                        
                        
                    }
                }
            }
            var erpEntries = [Int]()
            if let viewModel = self.viewModel{
                
                if let priceUpdater = viewModel.mobileRPMapViewUpdatable?.erpPriceUpdater{
                    
                    if(priceUpdater.allERPInSelectedRoute.count > 0){
                        
                        for feature in priceUpdater.allERPInSelectedRoute {
                            
                            if let selectedFeatureProperties = feature.properties,
                               case let .string(erpId) = selectedFeatureProperties["erpid"]
                            {
                                erpEntries.append(Int(erpId)!)
                            }
                        }
                    }
                }
            }
            var erpPrice = 0.0
            if let erpText = self.erpLbl.text{
                
                let priceArray = erpText.components(separatedBy: "$")
                let doublePrice = Float(priceArray[1]) ?? 0.0
                erpPrice = Double(doublePrice)
            }
            
            if let tripUploadRequest = tripUploadRequest {
                
                self.tripUploadRequest = TripPlanUploadRequest(tripEstStartTime: tripUploadRequest.tripEstStartTime, tripEstArrivalTime: tripUploadRequest.tripEstArrivalTime, tripStartAddress1: self.viewModel.currentAddress.address, tripStartAddress2: self.viewModel.currentAddress.address1, tripStartLat: self.viewModel.currentAddress.lat, tripStartLong: self.viewModel.currentAddress.long, tripDestAddress1: self.viewModel.address.address, tripDestAddress2: self.viewModel.address.address1, tripDestLat: self.viewModel.address.lat, tripDestLong: self.viewModel.address.long, totalDuration: Int(self.currentRoute?.expectedTravelTime ?? 0), totalDistance: Int(self.currentRoute?.distance ?? 0), erpExpense: erpPrice, tripPlanBy: tripUploadRequest.tripPlanBy, erpEntries: erpEntries, routeStops:routeStops, routeCoordinates: coordinates,tripEtaFavouriteId: self.etaInfo == nil ? nil : self.etaInfo?.etaFavoriteId)
            }
        }
       
    }
    
    @IBAction func saveBtnAction(){
        
        if(self.planTrip != nil){
            
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.EditRoutePlanning.UserClick.savelater, screenName: ParameterName.EditRoutePlanning.screen_view)
        }
        else{
            
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.RoutePlanning.UserClick.savelater, screenName: ParameterName.RoutePlanning.screen_view)
        }
        
        
        if self.viewModel != nil {
            
            if let currentAddress = self.viewModel.currentAddress{
                
                if(self.planTrip != nil){
                    AnalyticsManager.shared.sendLocationData(eventValue: ParameterName.EditRoutePlanning.LocationData.savelater_origin, screenName: ParameterName.EditRoutePlanning.screen_view, locationName: self.sourceLbl.text ?? "", latitude: currentAddress.lat.toString(), longitude: currentAddress.long.toString())
                }
                else{
                    AnalyticsManager.shared.sendLocationData(eventValue: ParameterName.RoutePlanning.LocationData.savelater_origin, screenName: ParameterName.RoutePlanning.screen_view, locationName: self.sourceLbl.text ?? "", latitude: currentAddress.lat.toString(), longitude: currentAddress.long.toString())
                }
                
            }
            
            if let address = self.viewModel.address {
                
                if(self.planTrip != nil){
                    AnalyticsManager.shared.sendLocationData(eventValue: ParameterName.EditRoutePlanning.LocationData.savelater_destination, screenName: ParameterName.EditRoutePlanning.screen_view, locationName: self.destLbl.text ?? "", latitude: address.lat.toString(), longitude: address.long.toString())
                }
                else{
                    AnalyticsManager.shared.sendLocationData(eventValue: ParameterName.RoutePlanning.LocationData.savelater_destination, screenName: ParameterName.RoutePlanning.screen_view, locationName: self.destLbl.text ?? "", latitude: address.lat.toString(), longitude: address.long.toString())
                }
                
                
            }
        }
        
        if(self.planTrip != nil){
            AnalyticsManager.shared.sendUserSelection(eventValue: ParameterName.EditRoutePlanning.UserSelection.savelater_route_type, screenName: ParameterName.EditRoutePlanning.screen_view, selectionValue: self.currentRouteSelection)
        }
        else{
            AnalyticsManager.shared.sendUserSelection(eventValue: ParameterName.RoutePlanning.UserSelection.savelater_route_type, screenName: ParameterName.RoutePlanning.screen_view, selectionValue: self.currentRouteSelection)
        }
        
        
        if(self.routeStopsAdded.count > 0){
            
            for feature in self.viewModel.wayPointFeatures{
                
                if case let .point(point) = feature.geometry,
                   case let .string(type) = feature.properties?["type"]{
                    
                    if(self.planTrip != nil){
                        AnalyticsManager.shared.sendLocationData(eventValue: ParameterName.EditRoutePlanning.LocationData.savelater_added_stop, screenName: ParameterName.EditRoutePlanning.screen_view, locationName: type, latitude: point.coordinates.latitude.toString(), longitude:point.coordinates.longitude.toString())
                    }
                    else{
                        AnalyticsManager.shared.sendLocationData(eventValue: ParameterName.RoutePlanning.LocationData.savelater_added_stop, screenName: ParameterName.RoutePlanning.screen_view, locationName: type, latitude: point.coordinates.latitude.toString(), longitude:point.coordinates.longitude.toString())
                    }
            
                }
            }
        }
        
        
        self.getUpdatedRoutesAndERPInfo()
        if let tripUploadRequest = tripUploadRequest {
            
            let currenTime = Int(Date().timeIntervalSince1970)
            let estDepartTime = tripUploadRequest.tripEstStartTime ?? 0
            let diff = estDepartTime - currenTime
            let minutes = diff / 60
            //let minutes = (diff - hours * 3600) / 60
            if let trip = self.planTrip{
                
                if(estDepartTime < currenTime){
                    
                    var displayErrorMsg = "Your departure time has passed. Please change to a future time."
                    if(trip.planBy == Values.ARRIVE_BY){
                        
                        let arrivalDate = Date(timeIntervalSince1970: TimeInterval(tripUploadRequest.tripEstArrivalTime ?? 0))
                        
                        let arrivalTime = arrivalDate.getTimeFromDate()
                        displayErrorMsg = "There is insufficient time to arrive by \(arrivalTime). Please change your arrival time."
                        
                        print("est arrival time",arrivalDate.getTimeFromDate())
                        
                    }
                    DispatchQueue.main.async { [weak self] in
                        guard let self = self else { return }
                        self.showTripErrorToast(message: displayErrorMsg)
                    }
                }
                else{
                    
                    TripPlanService().updateTripPlan(request: tripUploadRequest,tripPlannerId:trip.id) { result in
                        switch result {
                        case .failure(let error):
                            SwiftyBeaver.error("Failed to uploadTripPlan: \(error.localizedDescription)")
                        case .success(_):
                            self.retrieveUpcomingTrips()
                            DispatchQueue.main.async { [weak self] in
                                
                                guard let self = self else { return }
                                var routeStopsForTripPlan = [RouteStop]()
                                if(self.routeStopsAdded.count > 0){
                                    
                                    for feature in self.viewModel.wayPointFeatures{
                                        
                                        if case let .point(point) = feature.geometry,
                                           case let .string(type) = feature.properties?["type"],
                                            case let .string(amenityID) = feature.properties?["amenityID"] {
                                           
                                            let routeStopExistingtripPlan = RouteStop(type: type, coordinates: [point.coordinates.latitude,point.coordinates.longitude], amenityId: amenityID)
                                            
                                            routeStopsForTripPlan.append(routeStopExistingtripPlan)
                                            
                                        }
                                    }
                                }
                                let isETAavailable = self.etaInfo != nil ? true : false
                                let rName = self.etaInfo != nil ? self.etaInfo?.recipientName : ""
                                let rNumber = self.etaInfo != nil ? self.etaInfo?.recipientNumber : ""
                                let etaFavId = self.etaInfo != nil ? self.etaInfo?.etaFavoriteId : -1
                                self.planTrip = PlannTripDetails(id: trip.id, startTime: TimeInterval(tripUploadRequest.tripEstStartTime ?? 0), arrivalTime: TimeInterval(tripUploadRequest.tripEstArrivalTime ?? 0), planBy: tripUploadRequest.tripPlanBy ?? "", startAddress1: tripUploadRequest.tripStartAddress1 ?? "", startAddress2: tripUploadRequest.tripStartAddress2 ?? "", destAddress1: tripUploadRequest.tripDestAddress1 ?? "", destAddress2: tripUploadRequest.tripDestAddress2 ?? "", startLat: tripUploadRequest.tripStartLat ?? 0.0, startLong: tripUploadRequest.tripStartLong ?? 0.0, destLat: tripUploadRequest.tripDestLat ?? 0.0, destLong: tripUploadRequest.tripDestLong ?? 0.0, totalDistance: Double(tripUploadRequest.totalDistance ?? 0), totalDuration: tripUploadRequest.totalDuration ?? 0, erpExpense: tripUploadRequest.erpExpense ?? 0.0, erpEntries: tripUploadRequest.erpEntries ?? [], routeStops: routeStopsForTripPlan, routeCoordinates: tripUploadRequest.routeCoordinates ?? [[]],isEtaAvailable: isETAavailable,recipientName:rName!,recipientNumber:rNumber!,tripEtaFavouriteId:etaFavId!)
                                
                                self.enableSave = false
                                self.showTripUpdateToast(diffMins: minutes,estDuration: trip.totalDuration)
                            }
                        }
                    }
                }
                
            }
            else{
                
                if(estDepartTime < currenTime){
                    
                    
                    var displayErrorMsg = "Your departure time has passed. Please change to a future time."
                    if(tripUploadRequest.tripPlanBy == Values.ARRIVE_BY){
                        
                        let interval = TimeInterval(tripUploadRequest.tripEstArrivalTime ?? 0)
                        let arrivalDate = Date(timeIntervalSince1970: interval)
                        
                        let arrivalTime = arrivalDate.getTimeFromDate()
                        displayErrorMsg = "There is insufficient time to arrive by \(arrivalTime). Please change your arrival time."
                        
                        print("est arrival time",arrivalDate.getTimeFromDate())
                        
                    }
                    DispatchQueue.main.async { [weak self] in
                        guard let self = self else { return }
                        self.showTripErrorToast(message: displayErrorMsg)
                    }
                }
                else{
                    
                    TripPlanService().uploadTripPlan(request: tripUploadRequest) { result in
                        switch result {
                        case .failure(let error):
                            SwiftyBeaver.error("Failed to uploadTripPlan: \(error.localizedDescription)")
                        case .success(_):
                            self.retrieveUpcomingTrips()
                            DispatchQueue.main.async { [weak self] in
                                guard let self = self else { return }
                                self.goHomeAndShowTripLog(diffMins: minutes,estDuration: tripUploadRequest.totalDuration ?? 0)
                            }
                        }
                    }
                }
                
            }
            
        }
        
    }
    
    private func showTripErrorToast(message:String){
        
        /*let globalNotificationView =  GlobalNotificationView(appearance: BreezeToastStyle.globalNotification)
        globalNotificationView.show(in: self.view, title: "Unable to save trip", message: message, icon: UIImage(named: "trip_save_error")!, onClose: {
            
        }, actionTitle: "", onAction:nil,dismissInSeconds: 0,inHtml: false)
        globalNotificationView.yOffset = 40*/
        
        
        AnalyticsManager.shared.logPopUpEvent(popupName: ParameterName.RoutePlanning.UserPopUp.unable_to_save, eventName: ParameterName.RoutePlanning.UserPopUp.popup_open)
        
        self.popupAlert(title:"Unable to save trip\n", message: message, actionTitles: [Constants.gotIt], actions:[{action1 in
            AnalyticsManager.shared.logPopUpEvent(popupName: ParameterName.RoutePlanning.UserPopUp.unable_to_save, eventName: ParameterName.RoutePlanning.UserPopUp.popup_close)
            
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.RoutePlanning.UserClick.got_it_unable_to_save, screenName: ParameterName.RoutePlanning.screen_view)
            
        }, nil])
    }
    
    // TODO: reuse here and maplanding
    private func retrieveUpcomingTrips(){
        let planService = TripPlanService()
        let today = DateUtils.shared.getTimeDisplay(date: Date())
        planService.getUpcomingTrips(pageNumber: 1, pageSize: 1, startDate: today, endDate: today, searchKey: "") { result in
            
            switch result {
            case .success(let tripBase):
                //check if has upcomingtrips send to RN
                self.sendUpcomingTripsToRN(tripBase:tripBase)
                
            case .failure(let error):
                SwiftyBeaver.error("error retrieveTrip: \(error.localizedDescription)")
                break
            }
        }
    }
    
    private func sendUpcomingTripsToRN(tripBase: UpcomingTripBase ){
        var upcomingTrips = ["hasUpcomingTrips":true]
            if (tripBase.totalItems == 0)
            {
                upcomingTrips = ["hasUpcomingTrips":false]
            }
        let _ = ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.FN_UPDATE_UPCOMING_TRIPS, data: upcomingTrips as NSDictionary).subscribe(onSuccess: { _ in
            //We don't need to process this
        },
                    onFailure: { _ in
            //We don't need to process this
        })
    }
        
    private func showTripUpdateToast(diffMins:Int,estDuration:Int){
        
        self.checkNotificationsPermission { isEnabled in
            
            DispatchQueue.main.async {
                
                if(isEnabled){
                    
                    let tripUpdate = BreezeToast(appearance: BreezeToastStyle.tripSaveToast, onClose: {
                        
                        AnalyticsManager.shared.logPopUpEvent(popupName: ParameterName.RoutePlanning.UserPopUp.trip_been_updated, eventName: ParameterName.RoutePlanning.UserPopUp.popup_close)
                        
                        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.RoutePlanning.UserClick.close_trip_updated_popup, screenName: ParameterName.RoutePlanning.screen_view)
                        

                    }, actionTitle: "", onAction: nil)
                    
                    AnalyticsManager.shared.logPopUpEvent(popupName: ParameterName.RoutePlanning.UserPopUp.trip_been_updated, eventName: ParameterName.RoutePlanning.UserPopUp.popup_open)
                    
                    
                    tripUpdate.show(in: self.view, title: "Trip has been updated!", message: diffMins < 30 ? "Your trip will take around \(estDuration/60) mins based on current traffic conditions. Please start your trip soon.": (self.isDarkMode ? Constants.toastTripSave_dark : Constants.toastTripSave), animated: true, dismissInSeconds: 5, inHtml: diffMins < 30 ? false: true)
                    self.view.bringSubviewToFront(tripUpdate)

                    tripUpdate.yOffset = getTopOffset()
                }
                else{
                    
                    let tripUpdate = BreezeToast(appearance: BreezeToastStyle.tripSaveToast, onClose: {
                        AnalyticsManager.shared.logPopUpEvent(popupName: ParameterName.RoutePlanning.UserPopUp.trip_been_updated, eventName: ParameterName.RoutePlanning.UserPopUp.popup_close)
                        

                    }, actionTitle: "Enable notification"){
                        
                        if let url = URL(string: UIApplication.openSettingsURLString) {
                            if UIApplication.shared.canOpenURL(url) {
                                UIApplication.shared.open(url, options: [:], completionHandler: nil)
                            }
                        }
                    }
                    
                    AnalyticsManager.shared.logPopUpEvent(popupName: ParameterName.RoutePlanning.UserPopUp.trip_been_updated, eventName: ParameterName.RoutePlanning.UserPopUp.popup_open)
                    tripUpdate.show(in: self.view, title: "Trip has been updated!", message: Constants.toastEnableNotification, animated: true, dismissInSeconds: 5, inHtml: false)
                    self.view.bringSubviewToFront(tripUpdate)
                    tripUpdate.yOffset = getTopOffset()
                }
                
            }
        
        }
        
    }

    private func goHomeAndShowTripLog(diffMins:Int,estDuration:Int) {
        
        let storyboard = UIStoryboard(name: "TripSaved", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: String(describing: TripSavedPopupVC.self)) as! TripSavedPopupVC
        
        self.checkNotificationsPermission { isEnabled in
            
            DispatchQueue.main.async {
                if(isEnabled){
                    

                    if(diffMins < 30){
                        
                        vc.secondaryString = "It will take around \(estDuration/60) mins based on current traffic conditions. Please start your trip soon."
                        vc.mainButtonTitle = "Let's go"
                        //vc.secondaryString = Constants.toastTripSave
                    }
                    else{
                        
                        vc.secondaryString = "Your trip plan has been saved to Trip Log successfully. You will receive a notification 30 min before your trip starts."
                        
                        //vc.secondaryString = Constants.toastTripSave
                    }
                    

                }
                else{
                    
                    if(diffMins < 30){
                        
                        vc.secondaryString = "It will take around \(estDuration/60) mins based on current traffic conditions. Please start your trip soon."
                        vc.mainButtonTitle = "Let's go"
                    }
                    else{
                        
                        vc.secondaryString = "Please Enable notification to receive set-off alerts."
                       // vc.secondaryString =  Constants.toastEnableNotification
                    }
                    
                }
                
                vc.view.layer.zPosition = 101
                vc.delegate = self
                
                self.addChildViewControllerWithView(childViewController: vc as UIViewController,toView: self.view)
            }
        }
        
        
    }
    
    private func showUpdateRoutePopUpForNormalTrip(){
        
        if let viewModel = self.viewModel, let currentAddress = viewModel.currentAddress, let destAddress = viewModel.address {
            let currentLocation = LocationManager.shared.location
//            let plannedStart = CLLocationCoordinate2D(latitude: plannedTrip.startLat, longitude: plannedTrip.startLong)
            let plannedStart = CLLocationCoordinate2D(latitude: currentAddress.lat, longitude: currentAddress.long)
            if plannedStart.distance(to: currentLocation.coordinate) > Values.distanceCheckThreshold {
                // current location is the not the same as the planned trip
                // popup
                self.popupAlert(title: "", message: "You are not at your chosen start location. Would you like to update the route based on your current location?", actionTitles: [Constants.cancel,Constants.updateRoute], actions:[{action1 in
                    // cancel, doing nothing
                    AnalyticsManager.shared.logPopUpActionEvent(popupName: ParameterName.EditRoutePlanning.UserPopUp.popupName, eventName: ParameterName.EditRoutePlanning.UserPopUp.cancel, screenName: ParameterName.EditRoutePlanning.screen_view)
                                       
                },{action2 in
                    // set the current location as the address
                    AnalyticsManager.shared.logPopUpActionEvent(popupName: ParameterName.EditRoutePlanning.UserPopUp.popupName, eventName: ParameterName.EditRoutePlanning.UserPopUp.yes, screenName: ParameterName.EditRoutePlanning.screen_view)
                                        
                    getAddressFromLocation(location: currentLocation) { [weak self] address in
                        guard let self = self, self.viewModel != nil else { return }
                        self.viewModel.currentBaseAddress = SearchAddresses(alias: "", address1: address, lat: "\(currentLocation.coordinate.latitude)", long: "\(currentLocation.coordinate.longitude)", address2: "", distance: "0")
                        self.viewModel.setRPCurrentAddress()
                    
                        self.originalAddress = SearchAddresses(alias: "", address1: destAddress.address, lat: "\(destAddress.lat)", long: "\(destAddress.long)", address2: destAddress.address1, distance: "\(0)")
                        self.setupRoute()
                        // since start from current location, it will not be the planTrip anymore
                        //self.planTrip = nil
                        // reset button to later
                        //self.laterBtn.setTitle("Later", for: .normal)
                    }
                }, nil])
            } else {
                startNavigation()
            }
        } else {
            
            
            startNavigation()
        }
    }
    
    @IBAction func letsGobtnAction(){
        print("Lets go")
        
        self.startNavigation()
       
        /*if let plannedTrip = planTrip, let viewModel = self.viewModel, let currentAddress = viewModel.currentAddress {
            let currentLocation = LocationManager.shared.location
//            let plannedStart = CLLocationCoordinate2D(latitude: plannedTrip.startLat, longitude: plannedTrip.startLong)
            let plannedStart = CLLocationCoordinate2D(latitude: currentAddress.lat, longitude: currentAddress.long)
            if plannedStart.distance(to: currentLocation.coordinate) > Values.distanceCheckThreshold {
                // current location is the not the same as the planned trip
                // popup
                self.popupAlert(title: "", message: "You are not at your saved location. Would you like to update the route based on your current location?", actionTitles: [Constants.cancel,Constants.updateRoute], actions:[{action1 in
                    // cancel, doing nothing
                    AnalyticsManager.shared.logPopUpActionEvent(popupName: ParameterName.EditRoutePlanning.UserPopUp.popupName, eventName: ParameterName.EditRoutePlanning.UserPopUp.cancel, screenName: ParameterName.EditRoutePlanning.screen_view)
                                       
                },{action2 in
                    // set the current location as the address
                    AnalyticsManager.shared.logPopUpActionEvent(popupName: ParameterName.EditRoutePlanning.UserPopUp.popupName, eventName: ParameterName.EditRoutePlanning.UserPopUp.yes, screenName: ParameterName.EditRoutePlanning.screen_view)
                                        
                    getAddressFromLocation(location: currentLocation) { [weak self] address in
                        guard let self = self else { return }
                        self.viewModel.currentBaseAddress = SearchAddresses(alias: "", address1: address, lat: "\(currentLocation.coordinate.latitude)", long: "\(currentLocation.coordinate.longitude)", address2: "", distance: "0")
                        self.viewModel.setRPCurrentAddress()
                    
                        self.originalAddress = SearchAddresses(alias: "", address1: plannedTrip.destAddress1, lat: "\(plannedTrip.destLat)", long: "\(plannedTrip.destLong)", address2: plannedTrip.destAddress2, distance: "\(plannedTrip.totalDistance)")
                        self.setupRoute()
                        // since start from current location, it will not be the planTrip anymore
                        //self.planTrip = nil
                        // reset button to later
                        //self.laterBtn.setTitle("Later", for: .normal)
                    }
                }, nil])
            } else {
                startNavigation()
            }
        } else {
            
            
            self.showUpdateRoutePopUpForNormalTrip()
        }*/
    }
    
    // Handler click message broardCast
    @IBAction func clickBroardCast(_ sender: Any) {
        DispatchQueue.main.async {
            self.setupRouteForNCSCarpark()
           
        }
    }
    
    
    private func startNavigation() {
        //Getting first route from filtered array
        var selectedResponseIndex = 0
        var selectedRouteIndex = 0
        
        if self.viewModel != nil {
            
            if let currentAddress = self.viewModel.currentAddress{
                
                if planTrip != nil {
                    
                    AnalyticsManager.shared.sendLocationData(eventValue: ParameterName.EditRoutePlanning.LocationData.lets_go_origin, screenName: ParameterName.EditRoutePlanning.screen_view, locationName: self.sourceLbl.text ?? "", latitude: currentAddress.lat.toString(), longitude: currentAddress.long.toString())
                }
                else{
                    
                    AnalyticsManager.shared.sendLocationData(eventValue: ParameterName.RoutePlanning.LocationData.lets_go_origin, screenName: ParameterName.RoutePlanning.screen_view, locationName: self.sourceLbl.text ?? "", latitude: currentAddress.lat.toString(), longitude: currentAddress.long.toString())
                }
               
            }
            
            if let address = self.viewModel.address {
                
                if planTrip != nil {
                    
                    AnalyticsManager.shared.sendLocationData(eventValue: ParameterName.EditRoutePlanning.LocationData.lets_go_destination, screenName: ParameterName.EditRoutePlanning.screen_view, locationName: self.destLbl.text ?? "", latitude: address.lat.toString(), longitude: address.long.toString())
                }
                else{
                    
                    AnalyticsManager.shared.sendLocationData(eventValue: ParameterName.RoutePlanning.LocationData.lets_go_destination, screenName: ParameterName.RoutePlanning.screen_view, locationName: self.destLbl.text ?? "", latitude: address.lat.toString(), longitude: address.long.toString())
                }
                
                
            }
        }
        
        if(planTrip != nil){
            
            AnalyticsManager.shared.sendUserSelection(eventValue: ParameterName.EditRoutePlanning.UserSelection.lets_go_route_type, screenName: ParameterName.RoutePlanning.screen_view, selectionValue: self.currentRouteSelection)
        }
        else{
            
            AnalyticsManager.shared.sendUserSelection(eventValue: ParameterName.RoutePlanning.UserSelection.lets_go_route_type, screenName: ParameterName.RoutePlanning.screen_view, selectionValue: self.currentRouteSelection)
        }
        
        
        if(self.routeStopsAdded.count > 0){
            
            for feature in self.viewModel.wayPointFeatures{
                
                if case let .point(point) = feature.geometry,
                   case let .string(type) = feature.properties?["type"],
                    case let .string(amenityID) = feature.properties?["amenityID"] {
                    
                    if(planTrip != nil){
                        
                        AnalyticsManager.shared.sendLocationData(eventValue: ParameterName.EditRoutePlanning.LocationData.lets_go_added_stop, screenName: ParameterName.EditRoutePlanning.screen_view, locationName: type, latitude: point.coordinates.latitude.toString(), longitude:point.coordinates.longitude.toString())
                    }
                    else{
                        
                        AnalyticsManager.shared.sendLocationData(eventValue: ParameterName.RoutePlanning.LocationData.lets_go_added_stop, screenName: ParameterName.RoutePlanning.screen_view, locationName: type, latitude: point.coordinates.latitude.toString(), longitude:point.coordinates.longitude.toString())
                    }
                    
                }
            }
        }
        
        if(planTrip != nil){
            
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.EditRoutePlanning.UserClick.lets_go, screenName: ParameterName.EditRoutePlanning.screen_view)
                    
        }
        else{
            
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.RoutePlanning.UserClick.lets_go, screenName: ParameterName.RoutePlanning.screen_view)
        }
        
        
        //Checking nil condition for original response
        if let orignalRouteResponses = viewModel.mobileRPMapViewUpdatable?.originalResponse{
            if orignalRouteResponses.count > 0
            {
                // don't start navigation before route is set
                guard let currentRoute = currentRoute else { return }
                
                for (i,r) in orignalRouteResponses.enumerated() {
                    
                    if let j = r.routes?.firstIndex(of: currentRoute) {
                        selectedResponseIndex = i
                        selectedRouteIndex = j
                        let responseOptions = orignalRouteResponses[selectedResponseIndex].options
                        if case let .route(routeOptions) = responseOptions{
                            
                            SwiftyBeaver.debug("Comparing the current route with Original Response, \(selectedResponseIndex)\(routeOptions.profileIdentifier) \(routeOptions.roadClassesToAvoid.rawValue)\(routeOptions.attributeOptions)")
                        }
                        
                        break;
                    }
                }
            }
            else{
                
                return
            }
        }
        
        guard let route = viewModel.mobileRPMapViewUpdatable?.originalResponse[selectedResponseIndex].routes?.first, let responseOptions = viewModel.mobileRPMapViewUpdatable?.originalResponse[selectedResponseIndex].options, case let .route(routeOptions) = responseOptions, let response = viewModel.mobileRPMapViewUpdatable?.originalResponse[selectedResponseIndex] else { return }
        
    
        // Issue #250 - to stop cruise mode here so that it won't affect navigation idle time
//        NotificationCenter.default.post(name: Notification.Name(Values.notificationNavigationStart), object: nil)
        // We'll post notificationStart in TurnByTurnNavigationViewModel

//        routeOptions.locale = Locale(identifier: "en_SG") // HERE
        // #273 based on what Ochi suggested
        routeOptions.locale = Locale.enSGLocale() // HERE
        
        // method 1: Inherit from NavigationViewController
        
        //Vishnu - Fix Me - Once route Line disappear fixe from Map box on github #121, we will use Day and Night style in viewWillAppear() of Turn-by-Turn screen
        let styles = isDarkMode ? [CustomNightStyle()] : [CustomDayStyle()]
        #if targetEnvironment(simulator)
        let simulation = SimulationMode.always
        #else
        let simulation = SimulationMode.never
        #endif
        
        let navigationService = MapboxNavigationService(routeResponse: (viewModel.mobileRPMapViewUpdatable?.originalResponse[selectedResponseIndex])!, routeIndex: selectedRouteIndex, routeOptions: routeOptions, routingProvider: MapboxRoutingProvider(.hybrid), credentials: NavigationSettings.shared.directions.credentials,simulating: simulation)
        
        self.viewModel.mobileRPMapViewUpdatable?.response = viewModel.mobileRPMapViewUpdatable?.originalResponse[selectedResponseIndex]
        
        SwiftyBeaver.debug("Navigation Service Initialized Upon clicking on Let's go, \(navigationService)\(selectedRouteIndex)\(routeOptions.profileIdentifier) \(routeOptions.roadClassesToAvoid.rawValue)\(routeOptions.attributeOptions) \(currentRouteSelection)")
       // let navigationService = MapboxNavigationService(routeResponse:(viewModel.mobileRPMapViewUpdatable?.originalResponse[selectedResponseIndex])!, routeIndex: selectedRouteIndex, routeOptions: routeOptions, simulating: simulation)
#if DEMO || STAGING
        navigationService.simulationSpeedMultiplier = 4
#endif
        let options = NavigationOptions(styles: styles, navigationService: navigationService, predictiveCacheOptions: PredictiveCacheOptions())
        
        let progressVC = UIStoryboard(name: "Navigation", bundle: nil).instantiateViewController(withIdentifier: String(describing: NavProgressVC.self)) as! NavProgressVC
        options.bottomBanner = progressVC
        let topBannerVC = TopBannerViewController()
        topBannerVC.shouldShowInstructionFullList = appDelegate().carPlayConnected
        options.topBanner = topBannerVC
        
        // Beta.21
//        let navigationViewController = TurnByTurnNavigationVC(for: route, routeIndex: 0, routeOptions: routeOptions, navigationOptions: options)
        let navigationViewController = TurnByTurnNavigationVC(for: (viewModel.mobileRPMapViewUpdatable?.originalResponse[selectedResponseIndex])!, routeIndex: selectedRouteIndex, routeOptions: routeOptions, navigationOptions: options)
        navigationViewController.routeLineTracksTraversal = true
        navigationViewController.showsContinuousAlternatives = false
        navigationViewController.endRouteDelegate = self
        navigationViewController.topBannerVC = topBannerVC
        
        navigationViewController.destAddress = self.getDestAddress()
        navigationViewController.broadcastMessage = viewModel.broadcastMessage
        if let carpark = locationPinCarpark {
            navigationViewController.locationPinCarpark = carpark
        }
        navigationViewController.bookmarkId = bookmarkId
        navigationViewController.bookmarkName = bookmarkName
//        navigationViewController.planTrip = planTrip
        
//        navigationViewController.viewModel?.amenityType = amenityType
//        navigationViewController.viewModel?.amenityId = amenityId
//        navigationViewController.viewModel?.layerCode = layerCode
//
//        navigationViewController.viewModel?.plannedDestAddress1 = plannedDestAddress1
//        navigationViewController.viewModel?.plannedDestAddress2 = plannedDestAddress2
//        navigationViewController.viewModel?.plannedDestLat = plannedDestLat
//        navigationViewController.viewModel?.plannedDestLong = plannedDestLong
        progressVC.delegate = navigationViewController

        // MOVE this code to MapViewUpdatable already
//        if let viewportDataSource = (navigationViewController.navigationMapView?.navigationCamera.viewportDataSource as? NavigationViewportDataSource) {
////                    viewportDataSource.options.followingCameraOptions.minimumZoomLevel = 15
////                    viewportDataSource.options.followingCameraOptions.maximumZoomLevel = 16.5
//            viewportDataSource.options.followingCameraOptions.zoomRange = 15...16.5
//                    viewportDataSource.options.followingCameraOptions.pitchUpdatesAllowed = false
//                   viewportDataSource.followingMobileCamera.pitch = 50 // Your pitch here
//
//            //This fix is from here -> https://github.com/mapbox-collab/ncs-collab/issues/293
//            viewportDataSource.options.followingCameraOptions.bearingSmoothing.maximumBearingSmoothingAngle = 30.0
//            }

        progressVC.navigationViewController = navigationViewController
        navigationViewController.selectedAddress = self.selectedAddress
        // don't show end of route feedback screen
        navigationViewController.showsEndOfRouteFeedback = false
        // don't show floating buttons
        navigationViewController.floatingButtons = []
        navigationViewController.waypointStyle = .building
        navigationViewController.topBannerVC = topBannerVC
        
        RoundingTable.metric = RoundingTable(thresholds: [
            .init(maximumDistance: Measurement(value: 999, unit: .meters), roundingIncrement: 1, maximumFractionDigits: 0),
            .init(maximumDistance: Measurement(value: 999, unit: .kilometers), roundingIncrement: 0.0001, maximumFractionDigits: 1)
        ])
        
        navigationViewController.automaticallyAdjustsStyleForTimeOfDay = false
        
        navigationViewController.modalPresentationStyle = .fullScreen
                
        if let appdel = UIApplication.shared.delegate as? AppDelegate {
            appdel.navigationController = navigationViewController
        }
        
        var tripETA: TripETA? = nil
        // pass ETA data
        if let eta = etaInfo {
            
            var finalETAMessage = eta.message
            let estimatedArrivalTime = estimatedArrivalTime(route.expectedTravelTime)
            
            finalETAMessage =  "\(AWSAuth.sharedInstance.userName) is on the way to \(viewModel.address.address) and will approximately arrive by \(estimatedArrivalTime.time)\(estimatedArrivalTime.format)."
            
            // TODO: To change the TripETA struct later since it may affect Navigation if I change now
            tripETA = TripETA(type: TripETA.ETAType.Init, message: finalETAMessage, recipientName: eta.recipientName, recipientNumber: eta.recipientNumber, shareLiveLocation: "Y", tripStartTime: Int(Date().timeIntervalSince1970), tripEndTime: Int(Date().timeIntervalSince1970 + route.expectedTravelTime), tripDestLat: selectedAddress.lat?.toDouble() ?? 0, tripDestLong: selectedAddress.long?.toDouble() ?? 0, destination: viewModel.address.address, tripEtaFavouriteId: eta.etaFavoriteId)
            appDelegate().tripETA = tripETA
        }
        
        // change app state and pass navigation/eta info
        if let selectedCarPark = selectedCarPark {
            
            appDelegate().enterIntoNavigation(from: .mobile, navigationService: navigationService, with: tripETA,to:selectedCarPark)
        } else if let carpark = locationPinCarpark {
            appDelegate().enterIntoNavigation(from: .mobile, navigationService: navigationService, with: tripETA,to:carpark)
        } else {
            appDelegate().enterIntoNavigation(from: .mobile, navigationService: navigationService, with: tripETA)
        }
        present(navigationViewController, animated: true) {
//            if(self.viewModel != nil){
//                self.viewModel.mobileRPMapViewUpdatable = nil
//                self.viewModel.carPlayMapViewUpdatable = nil
//                self.viewModel = nil
//            }
            if self.navMapView != nil {
                self.navMapView.removeFromSuperview()
                self.navMapView = nil
            }
            // navigation started
//            appDelegate().cruiseSubmitHistoryData(description: "CruiseMode-End")
//            appDelegate().startNavigationRecordingHistoryData()
        }
    }
    
    func removeOtherLayerFromParkingOn() {
        if petrolKioskView != nil {
            self.petrolKioskbtnAction()
            self.removeLayerTypeFrom(type: Values.PETROL)
        }
        if evChargerView != nil {
            self.evChargerbtnAction()
            self.removeLayerTypeFrom(type: Values.EV_CHARGER)
        }
    }
    
    // MARK: Call API for reroute NCS Group
    func setupRouteForNCSCarpark() {
        if let carParkID = self.carParkID {
            CarparkDetailService().getCarparkDetail(carParkID) {[weak self] result in
                DispatchQueue.main.async {
                    guard let self = self else { return }
                    switch result {
                    case .success(let model):
                        if let dataCarpark =  model.data {
                            let location = CLLocation(latitude: dataCarpark.lat ?? 0, longitude: dataCarpark.long ?? 0)
                            getAddressFromLocation(location: location) { address in
                                let carParkAddress = SearchAddresses(alias: "", address1: dataCarpark.name ?? "", lat: "\(dataCarpark.lat ?? 0)", long: "\(dataCarpark.long ?? 0)", address2: address, distance: "\(dataCarpark.distance ?? 0)")
                                self.reset(address: carParkAddress)
                            }
                        }
                    case .failure(let error):
                        print(error)
                    }
                }
            }
        }
    }
        
    
    @IBAction func petrolKioskbtnAction() {
       
        print("Petrol kiosk")
        guard currentRoute != nil else { return }
        self.didDeselectAnnotation()
        if(petrolKioskView != nil){
            
            if(planTrip != nil){
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.EditRoutePlanning.UserToggle.petrol_off, screenName: ParameterName.EditRoutePlanning.screen_view)
            }
            else{
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.RoutePlanning.UserToggle.petrol_off, screenName: ParameterName.RoutePlanning.screen_view)
            }
            
            
            self.petrolBtn.setImage(UIImage.init(named: "petrolKiosk"), for: .normal)
            self.addWayPointToMap(type: Values.PETROL)
            self.addWayPointToMap(type: Values.EV_CHARGER)
            return
        }else{
//            if self.isParkingOn {
//                self.parkingButton.toggle()
//            }
            if parkingMode != .hide {
                self.parkingView.setParkingMode(type: .hide)
            }
            
            if(planTrip != nil){
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.EditRoutePlanning.UserToggle.petrol_on, screenName: ParameterName.EditRoutePlanning.screen_view)
            }
            else{
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.RoutePlanning.UserToggle.petrol_on, screenName: ParameterName.RoutePlanning.screen_view)
            }
            
        }
        self.petrolBtn.setImage(UIImage.init(named: "petrolKioskSelected"), for: .normal)
        self.evchargrBtn.setImage(UIImage.init(named: "evChargr"), for: .normal)
        
        print("PETROL KIOSK SUB ITEMS",petrolKiosk?.sub_items ?? "")
        guard let kiosk = petrolKiosk else { return }
        guard let subItems = petrolKiosk?.sub_items else { return }
        
        if let evChargerView = evChargerView {
            evChargerView.removeFromSuperview()
            self.evChargerView = nil
        }
        self.removeLayerTypeFrom(type: Values.EV_CHARGER)
        self.updateMapWithAmenities()
        let view = PetrolView()
        self.view.addSubview(view)
        view.layer.cornerRadius = 10
        view.petrolKioskSubItems = subItems
        
        for subItem in subItems {
            
            if(subItem.is_selected ?? false){
                
                self.selectedPetrolKioskItems.append(subItem.display_text ?? "")
            }
            
        }
        self.petrolKioskView = view
        view.snp.makeConstraints { (make) in
            make.bottom.equalTo(self.view.snp.bottom).offset(0)
            make.left.equalToSuperview().offset(0)
            make.right.equalToSuperview().offset(0)
            make.height.equalTo(280)
        }
        
        view.selectedItem = { [weak self] (name,isSelected,element_id) in
            guard let self = self else { return }
           
            if(isSelected){
                            
                let eventValue = "\(name)_on".lowercased()
                if(self.planTrip != nil){
                    AnalyticsManager.shared.logClickEvent(eventValue: eventValue, screenName: ParameterName.EditRoutePlanning.screen_view)
                }
                else{
                    AnalyticsManager.shared.logClickEvent(eventValue: eventValue, screenName: ParameterName.RoutePlanning.screen_view)
                }
                
                self.selectedPetrolKioskItems.append(name)
                
                AmenitiesSharedInstance.shared.updateSelectedPetrolAndEvItems(kiosItems: kiosk, subItemId: element_id, isSelected: isSelected,parentSelected: true)
            }
            else{
                
                let eventValue = "\(name)_off".lowercased()
                if(self.planTrip != nil){
                    AnalyticsManager.shared.logClickEvent(eventValue: eventValue, screenName: ParameterName.EditRoutePlanning.screen_view)
                }
                else{
                    AnalyticsManager.shared.logClickEvent(eventValue: eventValue, screenName: ParameterName.RoutePlanning.screen_view)
                }
                
                var tempSubItems = [Sub_items]()
                for subItem in self.petrolKiosk?.sub_items ?? [] {
                    
                    if(subItem.display_text == name){
                        
                        let subItem = Sub_items( element_name: subItem.element_name,count:subItem.count, display_text: subItem.display_text, is_selected: false,activeImageURL: "",inActiveImageURL: "",element_id: subItem.element_id)
                        tempSubItems.append(subItem)
                    }
                    else{
                        
                        let subItem = Sub_items( element_name: subItem.element_name,count:subItem.count, display_text: subItem.display_text, is_selected: subItem.is_selected,activeImageURL: "",inActiveImageURL: "",element_id: subItem.element_id)
                        tempSubItems.append(subItem)
                    }
                    
                }
                
                if(tempSubItems.count > 0){
                    
                    self.petrolKiosk = KioskBase(element_name: self.petrolKiosk?.element_name,element_id: self.petrolKiosk?.element_id, sub_items: tempSubItems)
                }
                if(self.selectedPetrolKioskItems.count > 0){
                    
                    if let index = self.selectedPetrolKioskItems.firstIndex(where: {$0 == name}){
                        
                        self.selectedPetrolKioskItems.remove(at: index)
                    }
                }
                
                AmenitiesSharedInstance.shared.updateSelectedPetrolAndEvItems(kiosItems: kiosk, subItemId: element_id, isSelected: isSelected,parentSelected: self.selectedPetrolKioskItems.count > 0 ? true : false)
            }
            self.didDeselectAnnotation()
            self.filterNearBySelectedKiosk()
            
        }
    
        view.closePretrolView = { [weak self] (cancel) in
            guard let self = self else { return }
           
            view.removeFromSuperview()
            //self.removeLayerTypeFrom(type: Values.PETROL)
            self.addWayPointToMap(type: Values.PETROL)
            self.addWayPointToMap(type: Values.EV_CHARGER)
        }
        
        self.filterNearBySelectedKiosk()
    }
    
    
    @IBAction func voucherBannerTouched(_ sender: Any) {
        
        guard let updater = carparkUpdater else { return }
        switch updater.type {
        case .searchOnly:
            lbVoucher.text = "Back to route options"
            imvVoucher.image = UIImage(named: "ic_arrow_left_white")
            imvVoucherArrow.isHidden = true
            
            updater.showAllCarparkVoucher()
            
            self.adjustZoomForCarParks(carParks: updater.getCarparkVoucher())
            
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.RoutePlanning.UserClick.view_nearby_voucher_carparks, screenName: ParameterName.RoutePlanning.screen_view)
        case .voucher, .parking:
            self.hideVoucherView()
            updater.removeAllCarparkVoucher()
            
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.RoutePlanning.UserClick.back_route_options, screenName: ParameterName.RoutePlanning.screen_view)
            
            self.setCameraPosition(routes: self.response?.routes ?? [])
//            if (self.isParkingOn) {
//                self.parkingButton.toggle()
//            }
            if parkingMode != .hide {
                self.parkingView.setParkingMode(type: .hide)
            }
        }
    }
    
    @IBAction func evChargerbtnAction(){
        print("ev charegr")
        
        guard currentRoute != nil else { return }
        self.didDeselectAnnotation()
        if evChargerView != nil{
            
            if(self.planTrip != nil){
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.EditRoutePlanning.UserToggle.ev_off, screenName: ParameterName.EditRoutePlanning.screen_view)
            }
            else{
                
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.RoutePlanning.UserToggle.ev_off, screenName: ParameterName.RoutePlanning.screen_view)
            }
            
            self.evchargrBtn.setImage(UIImage.init(named: "evChargr"), for: .normal)
            self.addWayPointToMap(type: Values.PETROL)
            self.addWayPointToMap(type: Values.EV_CHARGER)
            return
        }else{
            if(self.planTrip != nil){
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.EditRoutePlanning.UserToggle.ev_on, screenName: ParameterName.EditRoutePlanning.screen_view)
            }
            else{
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.RoutePlanning.UserToggle.ev_on, screenName: ParameterName.RoutePlanning.screen_view)
            }
            
        }
        
//        if self.isParkingOn {
//            self.parkingButton.toggle()
//        }
        
        if parkingMode != .hide {
            self.parkingView.setParkingMode(type: .hide)
        }
        
        self.petrolBtn.setImage(UIImage.init(named: "petrolKiosk"), for: .normal)
        self.evchargrBtn.setImage(UIImage.init(named: "evChargrSelected"), for: .normal)
        
        guard let kiosk = evItems else { return }
        guard let subItems = evItems?.sub_items else { return }
        
        if let petrolKiosk = petrolKioskView {
            petrolKiosk.removeFromSuperview()
            self.petrolKioskView = nil
        }
        
        self.removeLayerTypeFrom(type: Values.PETROL)
        self.updateMapWithAmenitiesForEV()
        let view = EVChargerView()
        self.view.addSubview(view)
        view.layer.cornerRadius = 10
        view.evSubItems = subItems
        
        for subItem in subItems {
            
            if(subItem.is_selected ?? false){
                
                self.selectedEVItems.append(subItem.display_text ?? "")
            }
            
        }
        
        self.evChargerView = view
        view.snp.makeConstraints { (make) in
            make.bottom.equalTo(self.view.snp.bottom).offset(0)
            make.left.equalToSuperview().offset(0)
            make.right.equalToSuperview().offset(0)
            make.height.equalTo(280)
        }
        
        view.selectedItem = { [weak self] (name,isSelected,element_id) in
            guard let self = self else { return }
           
            if(isSelected){
                
                
                let eventValue = "\(name)_on"
                
                if(self.planTrip != nil){
                    AnalyticsManager.shared.logClickEvent(eventValue: eventValue, screenName: ParameterName.EditRoutePlanning.screen_view)
                }
                else{
                    AnalyticsManager.shared.logClickEvent(eventValue: eventValue, screenName: ParameterName.RoutePlanning.screen_view)
                }
                
                self.selectedEVItems.append(name)
                
                AmenitiesSharedInstance.shared.updateSelectedPetrolAndEvItems(kiosItems: kiosk, subItemId: element_id, isSelected: isSelected,parentSelected: true)
                
            }
            else{
                
                self.didDeselectAnnotation()
                let eventValue = "\(name)_off"
                if(self.planTrip != nil){
                    
                    AnalyticsManager.shared.logClickEvent(eventValue: eventValue, screenName: ParameterName.EditRoutePlanning.screen_view)
                }
                else{
                    
                    AnalyticsManager.shared.logClickEvent(eventValue: eventValue, screenName: ParameterName.RoutePlanning.screen_view)
                }
                
                var tempSubItems = [Sub_items]()
                for subItem in self.evItems?.sub_items ?? [] {
                    
                    if(subItem.display_text == name){
                        
                        let subItem = Sub_items( element_name: subItem.element_name,count:subItem.count, display_text: subItem.display_text, is_selected: false,activeImageURL: subItem.activeImageURL,inActiveImageURL: subItem.inActiveImageURL,element_id: subItem.element_id)
                        tempSubItems.append(subItem)
                    }
                    else{
                        
                        let subItem = Sub_items( element_name: subItem.element_name,count:subItem.count, display_text: subItem.display_text, is_selected: subItem.is_selected,activeImageURL: subItem.activeImageURL,inActiveImageURL: subItem.inActiveImageURL,element_id: subItem.element_id)
                        tempSubItems.append(subItem)
                    }
                    
                }
                
                if(tempSubItems.count > 0){
                    
                    self.evItems = KioskBase(element_name: self.evItems?.element_name,element_id: self.evItems?.element_id, sub_items: tempSubItems)
                }
                
                if(self.selectedEVItems.count > 0){
                    
                    if let index = self.selectedEVItems.firstIndex(where: {$0 == name}){
                        
                        self.selectedEVItems.remove(at: index)
                    }
                }
                
                AmenitiesSharedInstance.shared.updateSelectedPetrolAndEvItems(kiosItems: kiosk, subItemId: element_id, isSelected: isSelected,parentSelected: self.selectedEVItems.count > 0 ? true : false)
            }
            self.didDeselectAnnotation()
            self.filterNearBySelectedEV()
            
        }
        
        view.closePretrolView = { [weak self] (cancel) in
            guard let self = self else { return }
           
            view.removeFromSuperview()
            //self.removeLayerTypeFrom(type: Values.EV_CHARGER)
            self.addWayPointToMap(type: Values.EV_CHARGER)
            self.addWayPointToMap(type: Values.PETROL)
        }
       
        self.filterNearBySelectedEV()
    }
    
    //MARK: - ETA SetUp
    @objc func receiveETA(_ notification: Notification) {
        
        if let dictionary = notification.userInfo {
            print("receiveETA - \(dictionary)")
/*
            let status = dictionary[Values.etaStatus] as! String
            if status.lowercased() == ETAInfo.Status.INIT {
                DispatchQueue.main.async { [weak self] in
                    guard let self = self else { return }
                    self.etaInfo = nil
                    // TODO: Localization doesn't work for some reason. Need to find out later
//                    self.notifyLbl.text = "RP-ShareDrive".localized
                    self.notifyLbl.text = "Share Drive"
                    self.notifyBtn.setImage(UIImage(named: "notifyArrvl"), for: .normal)
                    self.enableSave = true
                }
                return
            }
*/
            let name = dictionary[Values.etaContactName] as! String
            let number = dictionary[Values.etaPhoneNumber] as! String
//            let liveLocation = dictionary[Values.etaShareLiveLocation] as! Bool
            let message = dictionary[Values.etaMessage] as! String
            let favoriteId = dictionary[Values.etaFavoriteId] as! Int
//            let sendVoice = dictionary[Values.etaSendVoice] as! Bool
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
//                self.etaInfo = ETAInfo(message: message, recipientName: name, recipientNumber: number, shareLiveLocation: liveLocation, voiceCallToRecipient: sendVoice)
                self.etaInfo = ETAInfo(message: message, recipientName: name, recipientNumber: number, etaFavoriteId: favoriteId)

                self.notifyBtn.titleLabel?.text = Constants.cpShareDestination
                self.notifyLbl.text = Constants.cpShareDestination
                // TODO: Use localized string file later.
//                self.notifyBtn.titleLabel?.text = "RP-ShareDrive-Cancel".localized
//                self.notifyLbl.text = "RP-ShareDrive-Cancel".localized
                self.notifyBtn.setImage(UIImage(named: "share_destination_icon"), for: .normal)
                self.enableSave = true
            }
        }
//        else{
//
//            DispatchQueue.main.async {
////                self.notifyLbl.text = "RP-ShareDrive".localized
//                self.notifyLbl.text = "Share Drive"
//                self.enableSave = true
//                self.notifyBtn.setImage(UIImage(named: "notifyArrvl"), for: .normal)
//            }
//        }
    }
    
    //MARK: - Deinit
    deinit {
        SwiftyBeaver.debug("RoutePlanningVC deinit")
        amenityPetrolCancellable?.cancel()
        amenityEVCancellable?.cancel()
//        NotificationCenter.default.removeObserver(self, name: Notification.Name(Values.NotificationETASetUp), object: nil)
        
        //  Stop timer when release object - causes crashed app
        stopUpdateCarparkTimer()
        
        // We should not make this to nil, as there is no sync between car play and phone Route Planning
        /*if(appDelegate().routePlanningViewModel != nil)
        {
            appDelegate().routePlanningViewModel = nil
        }*/
         if(self.viewModel != nil){
             self.viewModel.mobileRPMapViewUpdatable = nil
            self.viewModel = nil
        }
        disposables.forEach { $0.cancel() }
    }
    
    
    @objc func onCloseInviteLocationVC() {
        if self.inviteLocationVC != nil {
            self.inviteLocationVC?.view.removeFromSuperview()
            self.inviteLocationVC?.removeFromParent()
            self.inviteLocationVC = nil
        }
    }

    

}

extension NewRoutePlanningVC:ERPPriceUpdateDelegate{
    func sendERPCost(cost: String) {
        self.erpLbl.text = cost
    }
    
    
}

extension NewRoutePlanningVC {
    
    func updateMapWithRouteResponse(){
        
        DispatchQueue.main.async {
            self.displayBroadcastMessage()
        }
            
        if let routePreferenceData = self.viewModel.mobileRPMapViewUpdatable?.routePreferenceData{
            
            if let response = routePreferenceData.routeResponse{
                
                self.updateMapWithResponse(response: response)
                // DispatchQueue.global(qos: .userInteractive).async {
                SwiftyBeaver.debug("Before calculating Route Intersection points")
                //self.midPoints =  self.calculateRouteIntersections()
                SwiftyBeaver.debug("After calculating Route Intersection points")
                
                if let routes = self.routes {
                    if let routes = self.routes {
                        DispatchQueue.main.async {
                            self.addFSCToMap(routes: routes)
                            self.animateActivity(animate: false)
                            SwiftyBeaver.debug("Loading stopped")
                        }
                    }
                }
                //}
            }
        }
//            self.updateMapWithResponse(response: (self.viewModel.mobileRPMapViewUpdatable?.bestOfRouteResponse)!)
            
        //}
    }
    
    func createFSCFeatures(label:String,route:Route,selected:String = "false") -> Turf.Feature {
        
        let lineFeature = [Turf.Feature(geometry: .lineString(LineString(route.shape!.coordinates)))]

        let routCollection = FeatureCollection(features: lineFeature)

        guard case let .lineString(routeGeometry) = routCollection.features[0].geometry else { return Turf.Feature(geometry: nil) }
        
        var imageName = ""
        var coordinate: CLLocationCoordinate2D?
        var routeBreaker = 0.5
       // mid(<#T##LocationCoordinate2D#>, <#T##LocationCoordinate2D#>)
        if(label == Constants.FASTEST_SHORTEST_CHEAPEST){

            routeBreaker = 0.5
            
            if(selected == "true"){
                
                imageName = "FSC_select"
            }
            else{
                
                imageName = "FSC_unselect"
            }
            
        }
        else if(label == Constants.FASTEST){
            
            routeBreaker = 0.3
            
            if(selected == "true"){
                
                imageName = "F_select"
            }
            else{
                
                imageName = "F_unselect"
            }
        }
        
        else if(label == Constants.FASTEST_SHORTEST){
            
            routeBreaker = 0.5
            
            if(selected == "true"){
                
                imageName = "SF_select"
            }
            else{
                
                imageName = "SF_unselect"
            }
        }
        
        else if(label == Constants.FASTEST_CHEAPEST){
            
            routeBreaker = 0.3
            
            if(selected == "true"){
                
                imageName = "CF_select"
            }
            else{
                
                imageName = "CF_unselect"
            }
        }
        
        else if(label == Constants.SHORTEST){
            
            routeBreaker = 0.3
            
            if(selected == "true"){
                
                imageName = "S_select"
            }
            else{
                
                imageName = "S_unselect"
            }
        }
        
        else if(label == Constants.SHORTEST_CHEAPEST){
            
            routeBreaker = 0.8
            
            if(selected == "true"){
                
                imageName = "CS_select"
            }
            else{
                
                imageName = "CS_unselect"
            }
        }
        
        else if(label == Constants.CHEAPEST){
            
            routeBreaker = 0.8
            
            if(selected == "true"){
                
                imageName = "C_select"
            }
            else{
                
                imageName = "C_unselect"
            }
        }
        
        if let distance = routeGeometry.distance(),
           let sampleCoordinate = routeGeometry.indexedCoordinateFromStart(distance: distance * (routeBreaker))?.coordinate,
                            let routeShape = route.shape,
                            let snappedCoordinate = routeShape.closestCoordinate(to: sampleCoordinate) {
            coordinate = snappedCoordinate.coordinate

        }

        let point = Turf.Point(LocationCoordinate2D(latitude: coordinate?.latitude ?? 0.0, longitude: coordinate?.longitude ?? 0.0))
            var feature = Turf.Feature(geometry: .point(point))

                feature.properties = [
                    "text":.string(label),
                    "selected":.string(selected),
                    "imageName":.string(imageName)
                ]
            
            return feature
        
        
    }
    
    //func createFSCFeatures(label:String,route:Route,selected:String = "false",coordinate:CLLocationCoordinate2D) -> Turf.Feature {
        
//        let lineFeature = [Turf.Feature(geometry: .lineString(LineString(route.shape!.coordinates)))]
//
//        let routCollection = FeatureCollection(features: lineFeature)
//
//        guard case let .lineString(routeGeometry) = routCollection.features[0].geometry else { return Turf.Feature(geometry: nil) }
        
        //var coordinate: CLLocationCoordinate2D?
        
       // mid(<#T##LocationCoordinate2D#>, <#T##LocationCoordinate2D#>)
//        if(label.contains("Fastest")){
//
//            if let distance = routeGeometry.distance(),
//               let sampleCoordinate = routeGeometry.indexedCoordinateFromStart(distance: distance/2 * (0.3))?.coordinate,
//                                let routeShape = route.shape,
//                                let snappedCoordinate = routeShape.closestCoordinate(to: sampleCoordinate) {
//                coordinate = snappedCoordinate.coordinate
//
//            }
//        }
//
//        if(label.contains("Cheapest")){
//
//            if let distance = routeGeometry.distance(),
//               let sampleCoordinate = routeGeometry.indexedCoordinateFromStart(distance: distance/2 * (0.8))?.coordinate,
//                                let routeShape = route.shape,
//                                let snappedCoordinate = routeShape.closestCoordinate(to: sampleCoordinate) {
//                coordinate = snappedCoordinate.coordinate
//
//            }
//        }
//
//        if(label.contains("Shortest")){
//
//            if let distance = routeGeometry.distance(),
//               let sampleCoordinate = routeGeometry.indexedCoordinateFromStart(distance: distance/2 * (0.5))?.coordinate,
//                                let routeShape = route.shape,
//                                let snappedCoordinate = routeShape.closestCoordinate(to: sampleCoordinate) {
//                coordinate = snappedCoordinate.coordinate
//
//            }
//        }
        
            
//        let point = Turf.Point(LocationCoordinate2D(latitude: coordinate.latitude, longitude: coordinate.longitude))
//            var feature = Turf.Feature(geometry: .point(point))
//
//                feature.properties = [
//                    "text":.string(label),
//                    "selected":.string(selected)
//                ]
//
//            return feature
//
//
//    }
    
    func addFSCToMap(routes:[Route]) {
        // Check crash because navMapView will be nil in case present Navigation
        guard self.viewModel != nil,
              self.navMapView != nil else { return }
        
//        var coordinate1:CLLocationCoordinate2D?
//        var coordinate2:CLLocationCoordinate2D?
//        var coordinate3:CLLocationCoordinate2D?
//
//        if(midPoints.features.count == 3){
//
//            if case let .point(pointData) = midPoints.features[0].geometry{
//
//                coordinate1 = pointData.coordinates
//            }
//
//            if case let .point(pointData) = midPoints.features[1].geometry{
//
//                coordinate2 = pointData.coordinates
//            }
//
//            if case let .point(pointData) = midPoints.features[2].geometry{
//
//                coordinate3 = pointData.coordinates
//            }
//
//        }
//
//        if(midPoints.features.count == 2){
//
//            if case let .point(pointData) = midPoints.features[0].geometry{
//
//                coordinate1 = pointData.coordinates
//            }
//
//            if case let .point(pointData) = midPoints.features[1].geometry{
//
//                coordinate2 = pointData.coordinates
//            }
//
//
//        }
//
//        if(midPoints.features.count == 1){
//
//            if case let .point(pointData) = midPoints.features[0].geometry{
//
//                coordinate1 = pointData.coordinates
//            }
//
//
//
//        }
        
        currentSelectedRouteSymbolID = ""
        
        // TODO: This might not be the best way. Need to review when we have time
        // To draw the current route first and save the current selected route symbol id
        for route in routes {
            
            if let routePreference = self.viewModel.mobileRPMapViewUpdatable?.routePreferenceData{
                
                if let responseRoutes = routePreference.routeResponse?.routes{
                    
                    if let i = responseRoutes.firstIndex(where: { $0 == route }){
                        
                        if let currentRoute = self.currentRoute {

                            if route == currentRoute {
                                self.currentRouteSelection = routePreference.routes[i].routePreferenceType
                                let routePreferencText = routePreference.routes[i].routePreferenceType
                                self.navMapView?.mapView.removeFastLayer(type: routePreferencText)
                                if(routePreference.routes[i].routePreferenceType != ""){
                                    let featureLabels = FeatureCollection(features: [self.createFSCFeatures(label: routePreference.routes[i].routePreferenceType, route: currentRoute,selected:"true")])
                                    currentSelectedRouteSymbolID = routePreferencText
                                    self.navMapView?.mapView.addFastestLayerToMap(features: featureLabels,type:routePreferencText,isSelected: true)
                                }
                                break
                            }
                        }
                    }
                    
                }
                
            }
            
        }
        
        // To draw the alternate route
        for route in routes {
            
            if let routePreference = self.viewModel.mobileRPMapViewUpdatable?.routePreferenceData{
                
                if let responseRoutes = routePreference.routeResponse?.routes{
                    
                    if let i = responseRoutes.firstIndex(where: { $0 == route }){
                        
                        if let currentRoute = self.currentRoute {
                            
//                            var coordiante:CLLocationCoordinate2D?
//                            if i == 0{
//
//                                if let coordinate1 = coordinate1 {
//                                    coordiante = coordinate1
//                                }
//
//                            }
//
//                            if( i == 1){
//
//                                if let coordinate2 = coordinate2 {
//                                    coordiante = coordinate2
//                                }
//
//                            }
//
//                            if(i == 2){
//
//                                if let coordinate3 = coordinate3 {
//                                    coordiante = coordinate3
//                                }
//                            }
                            if route == currentRoute {
                                continue
                            }
                            else{
                                
                                let routePreferencText = routePreference.routes[i].routePreferenceType
                                self.navMapView?.mapView.removeFastLayer(type: routePreferencText)
                                if(routePreference.routes[i].routePreferenceType != ""){
                                    
                                    let featureLabels = FeatureCollection(features: [self.createFSCFeatures(label: routePreference.routes[i].routePreferenceType, route: route)])
                                    self.navMapView?.mapView.addFastestLayerToMap(features: featureLabels,type:routePreferencText, isSelected: false, currentSelectedSymbolID: currentSelectedRouteSymbolID)
                                }
                                
                            }
                        }
                    }
                    
                }
                
            }
            
        }
    }
    
    func updateMapWithResponse(response:RouteResponse){
        
        DispatchQueue.main.async {
            self.dismissERPToolTipIfNeed()
        }
        
        self.navMapView?.removeWaypoints()
        self.response = response
        if let waypoints = response.waypoints {
            self.waypoints = waypoints
        }
        self.setRoutInfo()
        if self.viewModel.mobileRPMapViewUpdatable?.erpPriceUpdater != nil {
            self.viewModel.mobileRPMapViewUpdatable?.erpPriceUpdater.currentSelectedRoute = self.currentRoute
                //This method is find the available ERP on the route line and display their prices on to the map
            if let currentERPTime = self.viewModel.currentERPTime {
                
                self.viewModel.mobileRPMapViewUpdatable?.erpPriceUpdater.currentERPTime = currentERPTime
            }
            else{
                
                self.viewModel.mobileRPMapViewUpdatable?.erpPriceUpdater.currentERPTime = nil
            }
            
            viewModel.mobileRPMapViewUpdatable!.erpPriceUpdater.delegate = self
            viewModel.mobileRPMapViewUpdatable!.erpPriceUpdater.fetchERPTolls(erpTolls: DataCenter.shared.getAllERPFeatureCollection(),response: response)
        }
        
        self.setCameraPosition(routes: response.routes ?? [])
    }
    
    func setCameraPosition(routes:[Route]){
        
        SwiftyBeaver.debug("Set Camera Position")
        
        var coordinates: [[LocationCoordinate2D]] = [] //Here you create an array of arrays for route coordinates
        for route in routes {
            if let shape = route.shape {
                coordinates.append(shape.coordinates)
            }
//            coordinates.append(route.shape!.coordinates)
        }
        
        let multilLine = MultiLineString(coordinates)
        
        let routeGeoMetry = Geometry.multiLineString(multilLine)
        navMapView.navigationCamera.stop()
        let bearings = [0.0, 9.0, 18.0, 27.0, 36.0, 45.0, 54.0, 63.0, 72.0, 81.0, 90.0]
        
        var cameraOptions = [CameraOptions(center: nil, padding: .zero, zoom: 0.0, bearing: nil, pitch: 0.0)]
        for bearing in bearings {
            
            let cameraOption = navMapView.mapView.mapboxMap.camera(for: routeGeoMetry, padding: cameraPadding(), bearing: bearing, pitch: 0)
            cameraOptions.append(cameraOption)
        }
        
        
        let sortedRouteArray = cameraOptions.sorted(by: { c1, c2 in
            if let zoom1 = c1.zoom, let zoom2 = c2.zoom {
                return zoom1 > zoom2
            }else {
                return false
            }
        })
        
        //beta.9 change
        //let cameraOptions = navMapView.mapView.mapboxMap.camera(for: routeGeoMetry, padding: cameraPadding(), bearing: 0, pitch: 0)
        if let firstObject = sortedRouteArray.first {
            navMapView.mapView.camera.ease(to: firstObject, duration: 0.8, curve: .easeInOut, completion: nil)
            print(navMapView.mapView.cameraState.zoom)
        }
      
        
    }
    
    func cameraPadding() -> UIEdgeInsets {
        
        return UIEdgeInsets(top: topView.frame.origin.y+topView.frame.size.height+30, left: 40, bottom:routeView.frame.size.height+30, right: parkingView.frame.width + 10)
    }
}

extension NewRoutePlanningVC: NavigationMapViewDelegate {
    
    func navigationMapView(_ navigationMapView: NavigationMapView, didAdd finalDestinationAnnotation: PointAnnotation, pointAnnotationManager: PointAnnotationManager)
    {
        let point = finalDestinationAnnotation.point
        lastDestinationCoordinate = point.coordinates
        refreshAnnotations(at: point.coordinates)
        
//        var annotations = isParkingOn ? carparkAnnotations : [PointAnnotation]()
//        var destinationAnnotation = PointAnnotation(coordinate: point.coordinates)
//        destinationAnnotation.image = .init(image: UIImage(named: "destinationMark")!, name: "routePlan")
//        annotations.append(destinationAnnotation)
//
//        if  let currentAddress = viewModel.currentAddress {
//
//            let currentSearchLoc: CLLocation = CLLocation(latitude: currentAddress.lat,
//                                                           longitude: currentAddress.long)
//            let currentUserLoc = LocationManager.shared.location
//
//            let distance = LocationManager.shared.getDistanceInMeters(fromLoc: currentUserLoc, toLoc: currentSearchLoc)
//
//            if(distance > 5){
//
//                var customPointAnnotation = PointAnnotation(coordinate: currentSearchLoc.coordinate)
//                customPointAnnotation.image = .init(image: UIImage(named: "source_ann")!, name: "source")
//                annotations.append(customPointAnnotation)
//            }
//
//        }
//
//        self.destinationAnnotation = destinationAnnotation
//        navigationMapView.pointAnnotationManager?.annotations = annotations
    }
    
    func dismissERPToolTipIfNeed() {
        if let callout = self.callOutView, callout is ERPToolTipsView {
            callout.dismiss(animated: true)
            callout.removeFromSuperview()
            self.callOutView = nil
        }
    }
    
    func dismissTooltipIfNeeded() {
        if let callout = self.callOutView {
            callout.dismiss(animated: true)
            callout.removeFromSuperview()
            self.callOutView = nil
        }
    }
    
    func navigationMapView(_ mapView: NavigationMapView, didSelect route: Route) {
        guard let index = routes?.firstIndex(where: { $0 === route }) else { return }
        let prevIndex = currentRouteIndex
        currentRouteIndex = index
                
        guard let currentRoute = self.currentRoute,
              let viewModel = self.viewModel,
              let mobileRPMapViewUpdatable = viewModel.mobileRPMapViewUpdatable,
              let erpPriceUpdater = mobileRPMapViewUpdatable.erpPriceUpdater,
              let response = self.response,
              viewModel.address != nil else { return }
        
        erpPriceUpdater.currentSelectedRoute = currentRoute
        erpPriceUpdater.fetchERPTolls(erpTolls: DataCenter.shared.getAllERPFeatureCollection(),response: response)
        
        self.setRoutInfo()
        
        if(!self.isRouteSelectedDuringPark){
            self.enableSave = true
        }
        self.isRouteSelectedDuringPark = false
        self.addFSCToMap(routes: response.routes ?? [])
        
        if (prevIndex > currentRouteIndex){
            UIView.animate(withDuration: 0.0, animations: {
                
                let moveFirst = CGAffineTransform(translationX: -(self.erpView.bounds.width), y: 0.0)
                self.erpView.transform = moveFirst
                
            })
            UIView.animate(withDuration: 0.35, animations: {
                
                let moveLeft = CGAffineTransform(translationX: 0.0, y: 0.0)
                self.erpView.transform = moveLeft
                
            })
        }else if (prevIndex < currentRouteIndex){
            UIView.animate(withDuration: 0.0, animations: {
                
                let moveFirst = CGAffineTransform(translationX: -(self.erpView.bounds.width), y: 0.0)
                self.erpView.transform = moveFirst
                
            })
            UIView.animate(withDuration: 0.35, animations: {
                
                let moveRight = CGAffineTransform(translationX: 0.0, y: 0.0)
                self.erpView.transform = moveRight
                
            })
        }
    }
    
    func refreshAnnotations(at coordinate: CLLocationCoordinate2D) {
        var annotations = [PointAnnotation]()   // after converting to symbol layer. Only one annotation for the destination
        self.navMapView?.pointAnnotationManager?.annotations = annotations  // clear it first
        var destinationAnnotation = PointAnnotation(coordinate: coordinate)
        
        var imgName = (self.destinationCarPark != nil && isParkingOn) ? "destinationDot" : "destinationMark"
        
        // draw the dotted line if needed
        if let destinationCarpark = self.destinationCarPark, isParkingOn {
            // add dotted line
            if isParkingOn {
                self.navMapView?.addDottedLine(coordinate1: CLLocationCoordinate2D(latitude: destinationCarpark.lat ?? 0, longitude: destinationCarpark.long ?? 0), coordinate2: coordinate)
                
                destinationAnnotation.image = .init(image: UIImage(named: imgName, in: nil, compatibleWith: traitCollection)!, name: imgName)
                annotations.append(destinationAnnotation)
            }
            else{
                if let destinationCP = destinationCarPark, destinationCP.hasVoucher {
                    imgName = getcarparkImageName(carpark: destinationCP, selected: false, ignoreDestination: false).0
                }
                destinationAnnotation.image = .init(image: UIImage(named: imgName, in: nil, compatibleWith: traitCollection)!, name: imgName)
                annotations.append(destinationAnnotation)
            }
        } else {
            self.navMapView?.removeDottedLine()
            if let destinationCP = destinationCarPark, destinationCP.hasVoucher {
                imgName = getcarparkImageName(carpark: destinationCP, selected: false, ignoreDestination: false).0
            }
            destinationAnnotation.image = .init(image: UIImage(named: imgName, in: nil, compatibleWith: traitCollection)!, name: imgName)
            annotations.append(destinationAnnotation)
        }
        
        if  let currentAddress = viewModel.currentAddress {
            
            let currentSearchLoc: CLLocation = CLLocation(latitude: currentAddress.lat,
                                                           longitude: currentAddress.long)
            let currentUserLoc = LocationManager.shared.location
            
            let distance = LocationManager.shared.getDistanceInMeters(fromLoc: currentUserLoc, toLoc: currentSearchLoc)
            
            if(distance > 5) {
                if let currentRoute = self.currentRoute {
                    if let firstCoordinate = currentRoute.shape?.coordinates.first {
                        var customPointAnnotation = PointAnnotation(coordinate: firstCoordinate)
                        customPointAnnotation.image = .init(image: UIImage(named: "source_ann")!, name: "source")
                        annotations.append(customPointAnnotation)
                    }
                }
                else{
                    
                    var customPointAnnotation = PointAnnotation(coordinate: currentSearchLoc.coordinate)
                    customPointAnnotation.image = .init(image: UIImage(named: "source_ann")!, name: "source")
                    annotations.append(customPointAnnotation)
                }
            }
        }
        
        self.destinationAnnotation = destinationAnnotation
        self.navMapView?.pointAnnotationManager?.annotations = annotations
    }    
}

// MARK: - for Contact list
/*
extension NewRoutePlanningVC: CNContactPickerDelegate {
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
        print(contact.phoneNumbers)
        let numbers = contact.phoneNumbers.first
        let phoneNumber = (numbers?.value)?.stringValue ?? ""
        
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.eta_contact_select, screenName: ParameterName.eta_contact_screen)
        
        onContactSelected(phoneNumber: phoneNumber, name: "\(contact.givenName) \(contact.familyName)")
    }
    
    func contactPickerDidCancel(_ picker: CNContactPickerViewController) {
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.eta_contact_cancel, screenName: ParameterName.eta_contact_screen)
        self.dismiss(animated: true, completion: nil)
    }
    
    private func onContactSelected(phoneNumber: String, name: String) {
//        self.selectedETAContact = ETAContact(name: name, phone: phoneNumber)
        self.dismiss(animated: true) { [self] in
            
            if !PhoneValidator.isValidSingaporeMobile(number: phoneNumber) {
                self.popupAlert(title: nil, message: Constants.contactSelectError, actionTitles: [Constants.gotIt], actions:[{action1 in
                 //No need to handle this action
                },{action2 in
                //No need to handle this action
                }, nil])
                return
            }
            
            guard let address = viewModel.address else {
                return
            }
            //let estimatedTime = self.viewModel.getArrivalTime(route: self.currentRoute!)
            let etaMessage = "Hey, \(AWSAuth.sharedInstance.userName) here! I will be at \(address.address) around \(self.tripETATime)."
            let rootView = RNViewManager.sharedInstance.viewForModule(
                                // String name that you use as appname in index.js
                      "Breeze",
                                // Pass data to React Native module when it launches
                      initialProperties: ["toScreen":"ETAScreen",
                                          "navigationParams"
                                          : [Values.etaContactName: name,Values.etaPhoneNumber:phoneNumber,Values.etaMessage:etaMessage,Values.etaShareLiveLocation:true, Values.etaSendVoice:false,Values.etaStatus:ETAInfo.Status.INIT,"etaMode":"Navigation","destination":"","lat":"","long":""]                                   ])
                    
                 let reactNativeVC = UIViewController()
                 reactNativeVC.view = rootView
                 reactNativeVC.modalPresentationStyle = .fullScreen
                self.present(reactNativeVC, animated: true, completion: nil)
        }
    }
}
*/

extension NewRoutePlanningVC: TurnByTurnNavigationVCDelegate {
    func onArrival() {
                
        if let controllers = self.navigationController?.viewControllers, !controllers.isEmpty {
            //  Handle navigate to Bottomsheet ContentVC
            for controller in controllers {
                if let vc = controller as? ContentVC {
                    self.navigationController?.popToViewController(vc, animated: true)
                    return
                }
            }
            
            //  Handle move to MapLandingVC
            for controller in controllers {
                if let vc = controller as? MapLandingVC {
                    
                    let screenshot = UIApplication.shared.keyWindow?.capture()
                    let imageView = UIImageView(image: screenshot)
                    self.view.addSubview(imageView)
                    
                    DispatchQueue.main.async {
                        vc.resetPanAndSearchLocation()
                    }
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                        self.navigationController?.popToViewController(vc, animated: false)
                        imageView.removeFromSuperview()
                    }
                    break
                }
            }
        }
        
/*
        // Don't delete this !!! Since it could be reused when car park feature is available in the future
        // navigation stopped
        NotificationCenter.default.post(name: Notification.Name(Values.notificationNavigationStop), object: nil)
        self.endCarPlayNavigation(canceled: false)
        let storyboard = UIStoryboard(name: "Navigation", bundle: nil)
        if let vc = storyboard.instantiateViewController(withIdentifier: String(describing: ArrivalVC.self)) as? ArrivalVC {
            vc.selectedAddress = selectedAddress
            self.navigationController?.pushViewController(vc, animated: true)
        }
*/
    }
    
    func onWalking(response: RouteResponse, screenshot: UIImage?)  {
        
        if let controllers = self.navigationController?.viewControllers, !controllers.isEmpty {
            
            let imageView = UIImageView(image: screenshot)
            self.view.addSubview(imageView)
            
            var contentVC: ContentVC?
            for controller in controllers {
                if let vc = controller as? ContentVC {
                    contentVC = vc
                    break
                }
            }
            
            for controller in controllers {
                if let vc = controller as? MapLandingVC {
                    DispatchQueue.main.async {
                        vc.startWalking(response: response, screenshot: screenshot)
                    }
                    //  Fix issue BREEZES-7457 delay pop to previous screen after showing walking path
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                        if contentVC == nil {
                            self.navigationController?.popToViewController(vc, animated: false)
                        } else {
                            self.navigationController?.popToViewController(contentVC!, animated: false)
                        }
                        imageView.removeFromSuperview()
                    }
                    break
                }
            }
        }
    }
    
    func goToParking() {
        
        
        if let topController = UIApplication.shared.keyWindow?.rootViewController {
            if topController.isKind(of:UINavigationController.self) {
                
                let navigationController = topController as! UINavigationController
                let storyboard = UIStoryboard(name: "ParkingSGWeb", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "ParkingWebVC") as UIViewController
                navigationController.pushViewController(vc, animated: true)
                
            }
            
        }
        
        
    }
}


extension NewRoutePlanningVC:TripSavedPopupVCDelegate{
    func onDismiss(goTonavigation:Bool) {
        
        if(goTonavigation){
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.RoutePlanning.UserClick.lets_go_trip_saved_popup, screenName: ParameterName.RoutePlanning.screen_view)
            self.startNavigation()
        }
        else{
            if let controllers = self.navigationController?.viewControllers, !controllers.isEmpty {
                for controller in controllers {
                    if controller.isKind(of: MapLandingVC.self) {
                        self.navigationController!.popToViewController(controller, animated: true)
                        break
                    }
                }
            }
        }
        
    }
    
    func onConfirm() {
        if let controllers = self.navigationController?.viewControllers, !controllers.isEmpty {
            for controller in controllers {
                if controller.isKind(of: MapLandingVC.self) {
                    
                    AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.RoutePlanning.UserClick.check_in_triplog_trip_saved_popup, screenName: ParameterName.RoutePlanning.screen_view)
                    
                    self.navigationController!.popToViewController(controller, animated: false)
                    let storyboard = UIStoryboard(name: "TravelLog", bundle: nil)
                    if let vc = storyboard.instantiateViewController(withIdentifier: String(describing: TravelLogListVC.self)) as? TravelLogListVC {
                        vc.defaultSelectedIndex = 2
                        controller.navigationController?.pushViewController(vc, animated: true)
                    }
                    break
                }
            }
        }
    }
    
    
    
}

extension NewRoutePlanningVC {
    
    func checkNotificationsPermission( checkNotificationStatus isEnable : ((Bool)->())? = nil) {
        
        
        let current = UNUserNotificationCenter.current()
                current.getNotificationSettings(completionHandler: { permission in
                    switch permission.authorizationStatus  {
                    case .authorized:
                        print("User granted permission for notification")
                        isEnable?(true)
                    case .denied:
                        print("User denied notification permission")
                        isEnable?(false)
                    case .notDetermined:
                        print("Notification permission haven't been asked yet")
                        isEnable?(false)
                    case .provisional:
                        // @available(iOS 12.0, *)
                        print("The application is authorized to post non-interruptive user notifications.")
                    case .ephemeral:
                        // @available(iOS 14.0, *)
                        print("The application is temporarily authorized to post notifications. Only available to app clips.")
                    @unknown default:
                        print("Unknow Status")
                    }
                })
        
        
    }
    
    func calculateRouteIntersections() -> Turf.FeatureCollection{
        
        var midCollectionPoints = Turf.FeatureCollection(features: [])
        var allIntersectionsPoint = [CLLocationCoordinate2D]()
        var geometries = [Turf.Feature]()
        

        if let response = self.response{
            
            for route in response.routes ?? [] {
                
                lineCoordinates.append(contentsOf: route.shape!.coordinates)
                
            }
            lineString = LineString(lineCoordinates)
            geometries = (response.routes?.map({ r in
                Turf.Feature(geometry: r.shape?.geometry)
                
            }))!
            
            if(geometries.count > 1){
                
                for (index, _) in geometries.enumerated() {
                    
                    let secondIndex = (index + 1) / geometries.count
                    
                    if case let .lineString(data) = geometries[index].geometry,
                       case let .lineString(data1) = geometries[secondIndex].geometry{
                        
                        /// getting the intersection points
                        let intersection =  data.intersections(with: data1)
                        
                        //let feature = Turf.Feature(geometry: .lineString(LineString(intersection)))
                        
                        //self.removeSources(index:index)
                        
                        ///adding those intersections to map for testing
                        //self.addIntersectionPointsToMap(featureCollection: Turf.FeatureCollection(features: [feature]),index:index)
                        
                        /// for each intersection get the longest distance
                        let collection =  self.findLongestDistanceBetweenPoints(allIntersections: intersection)
                        
                        if(collection.features.count > 0){
                            
                            // from the longest distance collection get the slice point
                            if case let .point(pointData) = collection.features[0].geometry,
                               case let .point(pointData1) = collection.features[1].geometry
                            {
                                let slicedLineString = data.sliced(from: pointData.coordinates, to: pointData1.coordinates)
                                
                                if let slicedLineString = slicedLineString {
                                    let feature = Turf.Feature(geometry: .lineString(slicedLineString))
                                    
                                    /// add slice point feature to map for testing
                                    //self.addSlicePointToMap(featureCollection: Turf.FeatureCollection(features: [feature]),index:index)
                                    
                                    
                                    let coordinate =  self.getMidPointFromSlices(slicesCollection: Turf.FeatureCollection(features: [feature]))
                                    
                                    if let coordinate = coordinate {
                                        
                                        let midePointFeature = Turf.Feature(geometry: .point(Point(coordinate)))
                                        
                                        midCollectionPoints.features.append(midePointFeature)
                                        
                                        //self.addMidPointsToMap(featureCollection: Turf.FeatureCollection(features: [midePointFeature]),index:index)
                                        
                                    }
                                    
                                    
                                }
                            }
                        }
                        
                    }
                    
                }
            }
            
            
//            if(geometries.count > 2){
//
//                if case let .lineString(data) = geometries[0].geometry,
//                   case let .lineString(data1) = geometries[2].geometry{
//
//                  let intersection =  data.intersections(with: data1)
//                    //print(intersection)
//                    allIntersectionsPoint.append(contentsOf: intersection)
//
//                }
//
//                if case let .lineString(data) = geometries[1].geometry,
//                   case let .lineString(data1) = geometries[2].geometry{
//
//                    let intersection =  data.intersections(with: data1)
//                      //print(intersection)
//                    allIntersectionsPoint.append(contentsOf: intersection)
//                }
//
//                if case let .lineString(data) = geometries[0].geometry,
//                   case let .lineString(data1) = geometries[1].geometry{
//
//                    let intersection =  data.intersections(with: data1)
//                     // print(intersection)
//                    allIntersectionsPoint.append(contentsOf: intersection)
//
//                }
//            }
//            else{
//
//                if(geometries.count == 2){
//
//                    if case let .lineString(data) = geometries[0].geometry,
//                       case let .lineString(data1) = geometries[1].geometry{
//
//                        let intersection =  data.intersections(with: data1)
//                          //print("Intersections", intersection)
//                        allIntersectionsPoint.append(contentsOf: intersection)
//
//                    }
//                }
//
//            }
            
        }
        
        
//        if(allIntersectionsPoint.count > 0){
//
//            //self.removeSources()
//           let collection =  self.findLongestDistanceBetweenPoints(allIntersections: allIntersectionsPoint)
//
//
//            var intersectionFeatures = [Turf.Feature]()
//            for instersction in allIntersectionsPoint{
//
//                let feature = Turf.Feature(geometry: .point(Point(instersction)))
//                intersectionFeatures.append(feature)
//
//            }
//
////            self.addIntersectionPointsToMap(featureCollection: Turf.FeatureCollection(features: intersectionFeatures))
//            var slicedFeatures = [Turf.Feature]()
//
//            if case let .point(pointData) = collection.features[0].geometry,
//               case let .point(pointData1) = collection.features[1].geometry
//            {
//                for geometry in geometries {
//
//
//                    if case let .lineString(geoLineString) = geometry.geometry{
//
//                        let slicedLineString = geoLineString.sliced(from: pointData.coordinates, to: pointData1.coordinates)
//                        if let slicedLineString = slicedLineString {
//                            let feature = Turf.Feature(geometry: .lineString(slicedLineString))
//                            slicedFeatures.append(feature)
//                        }
//
//                    }
//                }
//
//
//                }
//
//            //print("Slices",slicedFeatures)
////            self.addSlicePointToMap(featureCollection: Turf.FeatureCollection(features: slicedFeatures))
////             midCollectionPoints =  self.getMidPointFromSlices(slicesCollection: Turf.FeatureCollection(features: slicedFeatures))
////            self.addMidPointsToMap(featureCollection: midCollection)
//
//
//        }
        
        return midCollectionPoints
    }
    
    func findLongestDistanceBetweenPoints(allIntersections:[CLLocationCoordinate2D]) -> Turf.FeatureCollection {
        
        var longestDistance = 0.0
        var foundP1:CLLocationCoordinate2D?
        var foundP2:CLLocationCoordinate2D?
        var features = [Turf.Feature]()
        
        for i in 1 ..< allIntersections.count {
            let p1 = allIntersections[i - 1]
            let p2 = allIntersections[i]
            
            // use turf.length instead
        

            let dist = lineString!.distance(from: p1, to: p2)
            //let dist = turf.distance(p1, p2)
            
            
            if (dist! > longestDistance) {
                foundP1 = p1
                foundP2 = p2
                longestDistance = dist!
            }
        }
        
        if foundP1 != nil && foundP2 != nil {
            
            let feature = Turf.Feature(geometry: .point(Point(foundP1!)))
            
            let feature1 = Turf.Feature(geometry: .point(Point(foundP2!)))
            
            features.append(feature)
            features.append(feature1)
            
        }
       
        return Turf.FeatureCollection(features: features)
    }
    
    func getMidPointFromSlices(slicesCollection:Turf.FeatureCollection) -> CLLocationCoordinate2D? {
        
       //var midFeature = [Turf.Feature]()
        
        
        for feature in slicesCollection.features {
            
            if case let .lineString(line) = feature.geometry
            {
                let distance = line.distance()
                
                if let distance = distance{
                    
                    return line.indexedCoordinateFromStart(distance: distance/2)?.coordinate
                    //let feature = Turf.Feature(geometry: .point(Point(midPoint!.coordinate)))
                    //midFeature.append(feature)
                }
                
            }
            
        }
        
        return nil
        //return Turf.FeatureCollection(features: midFeature)
    
    }
    
    func addIntersectionPointsToMap(featureCollection:Turf.FeatureCollection,index:Int){
        
        let geoJSONDataSourceIdentifier = "\(index)geoJSON-data-source"

        // Create a GeoJSON data source.
        var geoJSONSource = GeoJSONSource()
        geoJSONSource.data = .featureCollection(featureCollection)
        var circleLayer = CircleLayer(id: "\(index)circle-layer")
        circleLayer.source = geoJSONDataSourceIdentifier
        circleLayer.circleColor = .constant(StyleColor(.yellow))
        circleLayer.circleOpacity = .constant(0.6)
        circleLayer.circleRadius = .constant(8.0)
        
        do{
            try self.navMapView?.mapView.mapboxMap.style.addSource(geoJSONSource, id: geoJSONDataSourceIdentifier)
            try self.navMapView?.mapView.mapboxMap.style.addLayer(circleLayer)
        }
        catch(let error){
            print(error.localizedDescription)
        }
        
    }
    
    func addSlicePointToMap(featureCollection:Turf.FeatureCollection,index:Int){
        
        
        let geoJSONDataSourceIdentifier = "\(index)geoJSON-data-source-slice"

        // Create a GeoJSON data source.
        var geoJSONSource = GeoJSONSource()
        geoJSONSource.data = .featureCollection(featureCollection)
        var circleLayer = LineLayer(id: "\(index)circle-layer-slice")
        circleLayer.source = geoJSONDataSourceIdentifier
        circleLayer.lineColor = .constant(StyleColor(.clear))
        circleLayer.lineWidth = .constant(10)
        circleLayer.lineJoin = .constant(.round)
        circleLayer.lineCap = .constant(.round)
        
        do{
            try self.navMapView?.mapView.mapboxMap.style.addSource(geoJSONSource, id: geoJSONDataSourceIdentifier)
            try self.navMapView?.mapView.mapboxMap.style.addLayer(circleLayer)
        }
        catch(let error){
            print(error.localizedDescription)
        }
    }
    
    func addMidPointsToMap(featureCollection:Turf.FeatureCollection,index:Int){
        
        let geoJSONDataSourceIdentifier = "\(index)geoJSON-data-source-mid"

        // Create a GeoJSON data source.
        var geoJSONSource = GeoJSONSource()
        geoJSONSource.data = .featureCollection(featureCollection)
        var circleLayer = CircleLayer(id: "\(index)circle-layer-mid")
        circleLayer.source = geoJSONDataSourceIdentifier
        circleLayer.circleColor = .constant(StyleColor(.brown))
        circleLayer.circleOpacity = .constant(0.6)
        circleLayer.circleRadius = .constant(8.0)
        
        do{
            try self.navMapView?.mapView.mapboxMap.style.addSource(geoJSONSource, id: geoJSONDataSourceIdentifier)
            try self.navMapView?.mapView.mapboxMap.style.addLayer(circleLayer)
        }
        catch(let error){
            print(error.localizedDescription)
        }
        
    }
    
    func removeSources(index:Int){
        
        do {

            
            try self.navMapView?.mapView.mapboxMap.style.removeLayer(withId: "\(index)circle-layer")
            try self.navMapView?.mapView.mapboxMap.style.removeSource(withId: "\(index)geoJSON-data-source")
            
            try self.navMapView?.mapView.mapboxMap.style.removeLayer(withId: "\(index)circle-layer-slice")
            try self.navMapView?.mapView.mapboxMap.style.removeSource(withId: "\(index)geoJSON-data-source-slice")
            
            try self.navMapView?.mapView.mapboxMap.style.removeLayer(withId: "\(index)circle-layer-mid")
            try self.navMapView?.mapView.mapboxMap.style.removeSource(withId: "\(index)geoJSON-data-source-mid")
        } catch {
            NSLog("Failed to remove main Layers.")
        }
    }
    
}

class MapedRouteOptions: NavigationRouteOptions {
    var departureTime = ""
    var arrivalTime:String = ""
    
    // add departureTime to URLQueryItems
    override var urlQueryItems: [URLQueryItem] {
        var items = super.urlQueryItems
        
        if(departureTime != ""){
            items.append(URLQueryItem(name: "depart_at", value: departureTime))
            SwiftyBeaver.debug("Directions API appending depart_at \(departureTime) \(items)")
        }
        
        if(arrivalTime != ""){
            
            items.append(URLQueryItem(name: "arrive_by", value: arrivalTime))
            SwiftyBeaver.debug("Directions API appending arrive_by \(arrivalTime) \(items)")
        }
        
        return items
    }
    
    // create initializer to take in the departure time
    public init(waypoints: [Waypoint], departTime: String = "",arriveTime:String = "",identifier:ProfileIdentifier) {
        if(departTime != ""){
            
            departureTime = departTime
        }
        
        if(arriveTime != ""){
            
            arrivalTime = arriveTime
        }
        
        super.init(waypoints: waypoints,profileIdentifier: identifier)
    }
    
    required init(from decoder: Decoder) throws {
        super.init(waypoints: [])
    }

    required init(waypoints: [Waypoint], profileIdentifier: ProfileIdentifier? = .automobileAvoidingTraffic) {
        super.init(waypoints: [])
    }
    
    required init(waypoints: [Waypoint], profileIdentifier: ProfileIdentifier? = .automobileAvoidingTraffic, queryItems: [URLQueryItem]? = nil) {
        fatalError("init(waypoints:profileIdentifier:queryItems:) has not been implemented")
    }
}

extension NewRoutePlanningVC {
    func getDestAddress() -> AnalyticDestAddress? {
        
        if let carpark = self.selectedCarPark {
            return AnalyticDestAddress(address1: carpark.name, address2: "", lat: carpark.lat, long: carpark.long, amenityType: "carpark", amenityId: carpark.id ?? "", layerCode: self.layerCode)
        } else if let address = self.originalAddress as? SearchAddresses {
            return AnalyticDestAddress(address1: address.address1, address2: address.address2, lat: Double(address.lat ?? "0") ?? 0, long: Double(address.long ?? "0") ?? 0, amenityType: self.amenityType, amenityId: self.amenityId, layerCode: self.layerCode, fullAddress: address.fullAddress, isCrowdsourceParking: address.isCrowsourceParking, placeId: address.placeId, userLocationLinkIdRef: address.userLocationLinkIdRef)
        }else if let address = self.originalAddress as? EasyBreezyAddresses {
            return AnalyticDestAddress(address1: address.address1, address2: address.address2, lat: Double(address.lat ?? "0") ?? 0, long: Double(address.long ?? "0") ?? 0, amenityType: self.amenityType, amenityId: self.amenityId, layerCode: self.layerCode, isCrowdsourceParking: address.isCrowsourceParking, placeId: address.placeId)
        } else if let address = self.originalAddress {
            return AnalyticDestAddress(address1: address.address1, address2: address.address2, lat: Double(address.lat ?? "0") ?? 0, long: Double(address.long ?? "0") ?? 0, amenityType: self.amenityType, amenityId: self.amenityId, layerCode: self.layerCode)
        }else if let planTrip = self.planTrip {
            return AnalyticDestAddress(address1: planTrip.destAddress1, address2: planTrip.destAddress2, lat: planTrip.destLat, long: planTrip.destLong, amenityType: self.amenityType, amenityId: self.amenityId, layerCode: self.layerCode)
        }
        
        return nil
    }
}

#if TESTING
extension NewRoutePlanningVC: UIDocumentPickerDelegate {
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        if let url = urls.first {
            if let jsonData = try? Data(contentsOf: url) {
                if let route: BZRoute = try? JSONDecoder().decode(BZRoute.self, from: jsonData) {
                    let wayPoints = route.wayPoints.map { Waypoint(coordinate: CLLocationCoordinate2D(latitude: $0.latitude, longitude: $0.longitude), coordinateAccuracy: -1, name: $0.name) }
                    viewModel.calculateRoute(wayPoints: wayPoints)
                }
            }
        }
    }
        
}
#endif


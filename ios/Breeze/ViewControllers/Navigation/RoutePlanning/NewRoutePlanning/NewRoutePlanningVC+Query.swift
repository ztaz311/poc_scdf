//
//  NewRoutePlanningVC+Query.swift
//  Breeze
//
//  Created by VishnuKanth on 02/01/22.
//

import Foundation
import MapboxNavigation
import MapboxCoreNavigation
@_spi(Restricted) import MapboxMaps
import Turf
import Amplify
import UIKit

extension NewRoutePlanningVC{
    
    func querySymbolLayer(point:CGPoint, completion: @escaping ((_ selected: Bool)-> Void)){
        
      let location =  self.navMapView.mapView.mapboxMap.coordinate(for: point)
        var layerID = [String]()
//        layerID.append("_\(amenitySymbolThemeType)\(Values.PETROL)")
//        layerID.append("_\(amenitySymbolThemeType)\(Values.EV_CHARGER)")
        layerID.append("_\(Values.PETROL)")
        layerID.append("_\(Values.EV_CHARGER)")
        layerID.append("All") //layer for ERP symbol in Routeplanning screen
        layerID.append(CarparkValues.carparkSymbolLayerUnClusterId)
        if(layerID.count > 0){
            
            let queryOptions = RenderedQueryOptions(layerIds: layerID, filter: nil)
            //let Zone2NullOptions = RenderedQueryOptions(layerIds: ["zone2-null"], filter: nil)
            self.navMapView.mapView.mapboxMap.queryRenderedFeatures(at: point, options: queryOptions) { [weak self] (result) in
                
                guard let self = self else { return }

                switch result {

                case .success(let features):
                    
                    // Return the first feature at that location, then pass attributes to the alert controller.
                   // self.navMapView.mapView.mapboxMap.style.updateImageSource(withId: <#T##String#>, image: <#T##UIImage#>)
                    
                    if let selectedFeatureProperties = features.first?.feature.properties,
                       let feature = features.first?.feature,
                       case let .point(point) = feature.geometry {
                        if case let .string(type) = selectedFeatureProperties["type"] {
                            if(type == "ERP"){
                                self.openERPToolTipView(feature: feature,location:point.coordinates)
                                completion(true)
                                self.setupRecenterButton(isTrack: false)
                            }
                            else if(type == Values.EV_CHARGER){
                                self.openEvChargerToolTipView(feature: feature,location:point.coordinates)
                                completion(true)
                            }
                            else{
                                self.openPetrolToolTipView(feature: feature, location: point.coordinates)
                                completion(true)
                            }
                        } else if case let .string(carparkId) = selectedFeatureProperties["carparkId"],
                                  let carpark = self.carparkUpdater?.getCarparkFrom(id: carparkId) {
                            self.openCarparkToolTipView(carpark: carpark, tapped: true)
                            completion(true)
                        }
                        
                    } else {
                        // If the feature is a cluster, it will have `point_count` and `cluster_id` properties. These are assigned
                        // when the cluster is created.
                        self.didDeselectAnnotation()
                        self.didDeselectCarparkSymbol()
                        
                        completion(false)
                    }
                        
                case .failure(_):
                    completion(false)
                    break
                }
            }
        } else {
            completion(false)
        }
    }
    
    func openEvChargerToolTipView(feature:Turf.Feature,location:CLLocationCoordinate2D) {
        if let selectedFeatureProperties = feature.properties,
           case let .string(type) = selectedFeatureProperties["type"],
           case let .string(name) = selectedFeatureProperties["name"],
           case let .string(address) = selectedFeatureProperties["address"],
           case let .string(stationName) = selectedFeatureProperties["stationName"],
           case let .boolean(addStop) = selectedFeatureProperties["addStop"],
           case let .string(amenityID) = selectedFeatureProperties["amenityID"],
           case let .boolean(addStopButton) = selectedFeatureProperties["addStopButton"],
           case let .array(plugTypes) = selectedFeatureProperties["plugTypes"]
        {
            if(self.planTrip != nil){
                TagAmenityOnMap(event_value:ParameterName.EditRoutePlanning.UserClick.ev_map_icon)
            }
            else{
                TagAmenityOnMap(event_value:ParameterName.RoutePlanning.UserClick.ev_map_icon)
            }
            
            
            let isSameAmenity = amenityID == self.callOutView?.tooltipId
            
            self.didDeselectAnnotation()
            self.didDeselectCarparkSymbol()
            setSelectedState(amenityID: isSameAmenity ? "" : amenityID, type: type)
            
            if isSameAmenity {
                return
            }
            var arrayOfPlugTypes = Array<String>()
            for type in plugTypes {
                
                if case let .string(name) = type{
                    
                    arrayOfPlugTypes.append(name)
                }
            }
  
            // For testing
//            arrayOfPlugTypes = ["CCS/SAE","Type 2"]

            let evChargerToolTipView = EVChargerToolTips(id: amenityID, address: address, type: arrayOfPlugTypes, location: location, isAdding: addStop,displayText:name,isShowAddBtn: addStopButton)
            evChargerToolTipView.showShadow = false
            evChargerToolTipView.present(from: self.navMapView.mapView.frame, in: self.navMapView.mapView, constrainedTo: evChargerToolTipView.frame, animated: true)
            evChargerToolTipView.center = self.view.center
            self.view.bringSubviewToFront(evChargerToolTipView)
            self.callOutView = evChargerToolTipView
            evChargerToolTipView.adjust(to: location, in: self.navMapView)
            self.adjustCameraWhenAmenityClicked(at: location, callout: evChargerToolTipView)
            evChargerToolTipView.onAdd = { [weak self] (address,coordinate) in
                guard let self = self else { return }
                
                if(self.planTrip != nil){
                    self.TagAmenityOnMap(event_value:ParameterName.EditRoutePlanning.UserClick.add_stop_ev)
                }
                else{
                    self.TagAmenityOnMap(event_value:ParameterName.RoutePlanning.UserClick.add_stop_ev)
                }
                
                
                let location = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
                
                /// Added only 3 route stops
                if(self.routeStopsAdded.count < 3){
                    
                    var feature = Turf.Feature(geometry: .point(Point(location.coordinate)))
                    feature.properties = [
                        "type":.string(type),
                        "name":.string(name),
                        "address":.string(address),
                        "stationName":.string(stationName),
                        "addStop":.boolean(false),
                        "image_id":.string("\(self.amenitySymbolThemeType)\(type)"),
                        "amenityID":.string(amenityID),
                        "addStopButton":.boolean(addStopButton),
                        "plugTypes":.array(plugTypes)
                    ]
                    self.addWayPoints(location: location.coordinate, sName:amenityID,feature:feature, type:type)
                }
                
            }
            
            evChargerToolTipView.onRemove = { [weak self] (address,coordinate) in
                guard let self = self else { return }
                
                if(self.planTrip != nil){
                    self.TagAmenityOnMap(event_value:ParameterName.EditRoutePlanning.UserClick.remove_stop_ev)
                }
                else{
                    self.TagAmenityOnMap(event_value:ParameterName.RoutePlanning.UserClick.remove_stop_ev)
                }
                
                
                let location = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
                
                /// remove stop
                if(self.routeStopsAdded.count <= 3){
                    
                    var feature = Turf.Feature(geometry: .point(Point(location.coordinate)))
                    feature.properties = [
                        "type":.string(type),
                        "name":.string(name),
                        "address":.string(address),
                        "stationName":.string(stationName),
                        "addStop":.boolean(true),
                        "image_id":.string("\(self.amenitySymbolThemeType)\(type)"),
                        "amenityID":.string(amenityID),
                        "addStopButton":.boolean(addStopButton),
                        "plugTypes":.array(plugTypes)
                    ]
                    self.removeWayPoints(location: location.coordinate, sName:amenityID,feature:feature, type:type)
                }
                
            }
        }
    }
    
    func openERPToolTipView(feature:Turf.Feature,location:CLLocationCoordinate2D){
        
        self.didDeselectAnnotation()
        self.didDeselectCarparkSymbol()
        
        if let selectedFeatureProperties = feature.properties,
           case let .string(currentprice) = selectedFeatureProperties["currentprice"],
           case let .string(upcomingtime) = selectedFeatureProperties["upcomingtime"],
           case let .string(upcomingprice) = selectedFeatureProperties["upcomingprice"],
           case let .string(erpId) = selectedFeatureProperties["erpid"],   // ???
           case let .string(name) = selectedFeatureProperties["name"],
//           case let .string(startTime) = selectedFeatureProperties["startTime"],
//           case let .string(endTime) = selectedFeatureProperties["endTime"]
           case let .string(selectedTime) = selectedFeatureProperties["selectedTime"]
        {
            
            let isSameAmenity = erpId == self.callOutView?.tooltipId
            
            self.didDeselectAnnotation()
            self.didDeselectCarparkSymbol()
            setSelectedState(amenityID: isSameAmenity ? "" : erpId, type: "ERP")
            
            if isSameAmenity {
                return
            }
            
            let currentPriceArray = currentprice.components(separatedBy: "$")
            let currentPriceStr = currentPriceArray[1]
            
            let nextPriceArray = upcomingprice.components(separatedBy: "$")
            let nextPriceStr = nextPriceArray[1]
            
            //  If selected time is not empty -> currentTime = Start time - End time of selected time slot
            //  If selected time is empty -> currentTime = Start time - End time of current time slot
            if departArriveLbl.isHidden == true {
                self.selectedTimeErp = "Current"
            }else {
                self.selectedTimeErp = selectedTime
            }
            
            let erpToolTipView = ERPToolTipsView(id: erpId, name: name, rate: Double(currentPriceStr) ?? 0.0, nextText: upcomingtime, nextRate: Double(nextPriceStr) ?? 0.0, location: location, showMoreInfo: true, selectedTimeErp: self.selectedTimeErp ?? "")
            erpToolTipView.showShadow = false
            erpToolTipView.present(from: self.navMapView.mapView.frame, in: self.navMapView.mapView, constrainedTo: erpToolTipView.frame, animated: true)
            erpToolTipView.center = self.view.center
            erpToolTipView.onMoreInfo = { [weak self] erpId in
                guard let self = self else { return }
                // open more info for ERP in RN
                // TODO: To make it a func to reuse
                var selectedTime = 0
                if let vModel = self.viewModel{
                    
                    if let selectedERPTime = vModel.currentERPTime{
                        selectedTime =  Int(selectedERPTime.currentTimeInMiliseconds())
                    }
                    else{
                        
                        selectedTime = Int(Date().currentTimeInMiliseconds())
                    }
                    
                }
                else{
                    
                    selectedTime = Int(Date().currentTimeInMiliseconds())
                }
                let data = [
                    "toScreen": "ERP", // Screen name you want to display when start RN
                    "navigationParams": [ // Object data to be used by that screen
                        "erpId": erpId,
                        "isSelected": true,
                        "erpSelTS":NSNumber(value: selectedTime)
                                        ]] as [String : Any]
                let rootView = RNViewManager.sharedInstance.viewForModule(
                    "Breeze",
                    initialProperties: data)
                
                let reactNativeVC = UIViewController()
                reactNativeVC.view = rootView
                reactNativeVC.modalPresentationStyle = .pageSheet
                self.present(reactNativeVC, animated: true, completion: nil)
                
                // Passing data erpId for React Screen 
                ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.SEND_EVENT_SELECTED_ERP, data: ["erpid":Int(erpId)] as? NSDictionary).subscribe(onSuccess: {_ in
                    
                    //No need to look for response
                    
                },                 onFailure: {_ in
                    
                    //No need to look for response
                })
            }
            self.view.bringSubviewToFront(erpToolTipView)
            self.callOutView = erpToolTipView
            erpToolTipView.adjust(to: location, in: self.navMapView)
            self.adjustCameraWhenAmenityClicked(at: location, callout: erpToolTipView)
        }
    }
    
    
//    func checkCurrentTimeWithSelectedTime() {
//        let formatter = DateFormatter()
//        formatter.dateFormat = "HH:mm"
//
//        let currentTime = formatter.string(from: Date())
//        let time = formatter.date(from: currentTime)
//        formatter.dateFormat = "HH:mm"
//
//        let timeString = formatter.string(from: time!)
//        let startTime = timeString
//
//        let endTime = erpPriceUpdater.endTime ?? ""
//        let isCurrentTimeBetwen = Date().currentTimeisBetween(startTime: startTime, endTime: endTime)
//        if(isCurrentTimeBetwen == true){
//            self.currentTime = "\(startTime)-\(endTime)"
//        }
//    }
    
    func openCarparkToolTipView(carpark: Carpark, tapped: Bool = false) {
        DispatchQueue.main.async { [weak self] in
            
            guard let self = self else { return }
            
            let sameCarpark = carpark.id == self.currentSelectedCarpark?.id
            
            self.didDeselectAnnotation()
            self.didDeselectCarparkSymbol()
            
            self.setSelectedState(amenityID: "", type: Values.PARKS)
            
            if sameCarpark {
                self.currentSelectedCarpark = nil
                self.viewModel.fetchBroadcastMessage(self.currentSelectedCarpark?.id, mode: .planning) { result in
                    if let model = result, model.hasBroadcastMessage() {
                        DispatchQueue.main.async {
                            self.showCarparkBroadcastMessage(false, title: "")
                        }
                    }
                }
                return
            }
            
            self.currentSelectedCarpark = carpark
            
            self.viewModel.fetchBroadcastMessage(self.currentSelectedCarpark?.id, mode: .planning) { result in
                if let model = result, model.hasBroadcastMessage() {
                    DispatchQueue.main.async {
                        self.showCarparkBroadcastMessage(true, title: model.getBroadcastMessage())
                    }
                } else {
                    DispatchQueue.main.async {
                        self.showCarparkBroadcastMessage(false, title: "")
                    }
                }
            }
            
            //            self.currentSelectedParkAnnotation = annotation
            //            self.updateAnnotationImage(annotation, select: true)
            
            // set camera (actually should not set if not because of tapped
            if tapped {
                let cameraOptions = CameraOptions(center: CLLocationCoordinate2D(latitude: carpark.lat ?? 0, longitude: carpark.long ?? 0), zoom: self.navMapView.mapView.mapboxMap.cameraState.zoom) // using current zoom level
                self.navMapView.mapView.camera.ease(to: cameraOptions, duration: Values.standardAnimationDuration, curve: .easeInOut, completion: nil)
            }
            
            // TODO: TO BE IMPLEMENTED
            /*
             AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.routeplanning_tap_carpark, screenName: ParameterName.routeplanning_carpark_screen)
             */
            let callOutView = CarparkToolTipsView(carpark: carpark, calculateFee: true, routeablePoint: nil)
            callOutView.showShadow = false
            callOutView.present(from: self.navMapView.mapView.frame, in: self.navMapView.mapView, constrainedTo: callOutView.frame, animated: true)
            self.callOutView = callOutView
            
            self.adjustCameraWhenAmenityClicked(at: callOutView.tooltipCoordinate, callout: callOutView)
            
            callOutView.onCalculate = { [weak self]  theCarpark in
                guard let self = self else { return }
                
                if(self.planTrip != nil){
                    self.TagAmenityOnMap(event_value:ParameterName.EditRoutePlanning.UserClick.parking_calculatefee)
                }
                else{
                    self.TagAmenityOnMap(event_value:ParameterName.RoutePlanning.UserClick.parking_calculatefee)
                }
                
                var locationLat: Double = 0
                var locationLong: Double = 0
                
                if self.shouldUpdateCarparks() {
                    locationLat = self.viewModel.centerMapLocation?.latitude ?? 0
                    locationLong = self.viewModel.centerMapLocation?.longitude ?? 0
                }else {
                    locationLat = self.viewModel.address.lat
                    locationLong = self.viewModel.address.long
                }
                
                let carParkData = [
                    "toScreen": "ParkingCalculator", // Screen name you want to display when start RN
                    "navigationParams": [ // Object data to be used by that screen
                        "parkingID": theCarpark.id ?? "",
                        "start_timestamp": self.tripEstStartTime  == 0 ? NSNumber(value:Int(Date().timeIntervalSince1970 * 1000)) : NSNumber(value: Int(self.tripEstStartTime * 1000)),
                        "fromNative":true,
                        Values.FOCUSED_LAT: locationLat.toString() ,
                        Values.FOCUSED_LONG: locationLong.toString()
                                        ],
                ] as [String : Any]
                let rootView = RNViewManager.sharedInstance.viewForModule(
                    "Breeze",
                    initialProperties: carParkData)
                
                let reactNativeVC = UIViewController()
                reactNativeVC.view = rootView
                reactNativeVC.modalPresentationStyle = .fullScreen
                self.present(reactNativeVC, animated: true, completion: nil)
                
            }
            callOutView.onMoreInfo = { [weak self]  theCarpark in
                guard let self = self else { return }
                
                if(self.planTrip != nil){
                    self.TagAmenityOnMap(event_value:ParameterName.EditRoutePlanning.UserClick.parking_moreinfo)
                }
                else{
                    self.TagAmenityOnMap(event_value:ParameterName.RoutePlanning.UserClick.parking_moreinfo)
                }
                
                
                let carParkData = [
                    "toScreen": "ParkingPriceDetail", // Screen name you want to display when start RN
                    "navigationParams": [ // Object data to be used by that screen
                        "parkingId": theCarpark.id,
                        "idToken":AWSAuth.sharedInstance.idToken,
                        "baseURL": appDelegate().backendURL,
                        Values.FOCUSED_LAT :self.viewModel.address.lat.toString(),
                        Values.FOCUSED_LONG :self.viewModel.address.long.toString()
                                        ],
                ] as [String : Any]
                let rootView = RNViewManager.sharedInstance.viewForModule(
                    "Breeze",
                    initialProperties: carParkData)
                
                let reactNativeVC = UIViewController()
                reactNativeVC.view = rootView
                reactNativeVC.modalPresentationStyle = .pageSheet
                self.present(reactNativeVC, animated: true, completion: nil)
            }
            callOutView.onCarparkSelectedV2 = { [weak self] (theCarpark, _) in
                guard let self = self else { return }
                
                self.selectedCarPark = theCarpark
                if(self.planTrip != nil){
                    self.TagAmenityOnMap(event_value:ParameterName.EditRoutePlanning.UserClick.parking_navigatehere)
                }
                else{
                    
                    self.TagAmenityOnMap(event_value:ParameterName.RoutePlanning.UserClick.parking_navigatehere)
                }
                
                let location = CLLocation(latitude: theCarpark.lat ?? 0, longitude: theCarpark.long ?? 0)
                getAddressFromLocation(location: location) { address in
                    let address = SearchAddresses(alias: "", address1: theCarpark.name ?? "", lat: "\(theCarpark.lat ?? 0)", long: "\(theCarpark.long ?? 0)", address2: address, distance: "\(theCarpark.distance ?? 0)")
                    self.reset(address: address)
                }
            }
            
            callOutView.shareViaLocation = {
                shareLocationFromNative(address1: carpark.name ?? "", latitude: carpark.lat?.toString() ?? "", longtitude: carpark.long?.toString() ?? "", name: carpark.name ?? "")
            }
            
            callOutView.inviteLocation = {
                self.openLocationInviteScreen(address1: carpark.name ?? "", latitude: carpark.lat?.toString() ?? "", longtitude: carpark.long?.toString() ?? "", name: carpark.name ?? "")
            }
           
            
        }
    }
    
    
    func openLocationInviteScreen(address1: String = "", latitude: String = "", longtitude: String = "", name: String = "", carparkId: String = ""){
        var data = [
            "fromScreen": "RoutePlanning",
            "data": [
                "address1": address1,
                "latitude": latitude,
                "longitude": longtitude,
                "name": name
            ]] as [String : Any]
        
        if !carparkId.isEmpty {
            data["carparkId"] = carparkId
        }
        
        onCloseInviteLocationVC()
        
        self.inviteLocationVC = self.getRNViewBottomSheet(toScreen: "LocationInviteScreen", navigationParams: data)
        self.inviteLocationVC?.view.frame = UIScreen.main.bounds
        
        if let vc = self.inviteLocationVC {
            self.addChildViewControllerWithView(childViewController: vc, toView: self.view)
            self.view.bringSubviewToFront(vc.view)
        }
    }
    
    
    func openPetrolToolTipView(feature:Turf.Feature,location:CLLocationCoordinate2D){
        
        if let selectedFeatureProperties = feature.properties,
           case let .string(name) = selectedFeatureProperties["name"],
           case let .string(address) = selectedFeatureProperties["address"],
           case let .string(type) = selectedFeatureProperties["type"],
           case let .string(stationName) = selectedFeatureProperties["stationName"],
           case let .boolean(addStop) = selectedFeatureProperties["addStop"],
           case let .boolean(addStopButton) = selectedFeatureProperties["addStopButton"],
           case let .string(amenityID) = selectedFeatureProperties["amenityID"]
            {
            
            if(self.planTrip != nil){
                TagAmenityOnMap(event_value:ParameterName.EditRoutePlanning.UserClick.petrol_map_icon)
            }
            else{
                TagAmenityOnMap(event_value:ParameterName.RoutePlanning.UserClick.petrol_map_icon)
            }
            
            let isSameAmenity = amenityID == self.callOutView?.tooltipId
            
            self.didDeselectAnnotation()
            self.didDeselectCarparkSymbol()
            setSelectedState(amenityID: isSameAmenity ? "" : amenityID, type: type)
            
            if isSameAmenity {
                return
            }
            
            
            let petrolToolTip: PetrolToolTipsView = PetrolToolTipsView(id: amenityID, name: name, address: address, location: location, isAdding: addStop,isShowButton: addStopButton)
            petrolToolTip.showShadow = false
            petrolToolTip.present(from: self.navMapView.mapView.frame, in: self.navMapView.mapView, constrainedTo: petrolToolTip.frame, animated: true)
            petrolToolTip.center = self.view.center
            self.view.bringSubviewToFront(petrolToolTip)
            self.callOutView = petrolToolTip
            petrolToolTip.adjust(to: location, in: self.navMapView)
            self.adjustCameraWhenAmenityClicked(at: location, callout: petrolToolTip)
            petrolToolTip.onAdd = { [weak self] (address,coordinate) in
                guard let self = self else { return }
                
                if(self.planTrip != nil){
                    self.TagAmenityOnMap(event_value:ParameterName.EditRoutePlanning.UserClick.add_stop_petrol)
                }
                else{
                    self.TagAmenityOnMap(event_value:ParameterName.RoutePlanning.UserClick.add_stop_petrol)
                }
                
                
                let location = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
                
                /// Added only 3 route stops
                if(self.routeStopsAdded.count < 3){
                    
                    var feature = Turf.Feature(geometry: .point(Point(location.coordinate)))
                    feature.properties = [
                        "type":.string(type),
                        "name":.string(name),
                        "address":.string(address),
                        "stationName":.string(stationName),
                        "addStop":.boolean(false),
                        "image_id":.string("\(self.amenitySymbolThemeType)\(type)"),
                        "addStopButton":.boolean(addStopButton),
                        "amenityID":.string(amenityID)
                    ]
                    self.addWayPoints(location: location.coordinate, sName:amenityID,feature:feature, type:type)
                }
                
            }
            
            petrolToolTip.onRemove = { [weak self] (address,coordinate) in
                guard let self = self else { return }
                
                if(self.planTrip != nil){
                    self.TagAmenityOnMap(event_value:ParameterName.EditRoutePlanning.UserClick.remove_stop_petrol)
                }
                else{
                    self.TagAmenityOnMap(event_value:ParameterName.RoutePlanning.UserClick.remove_stop_petrol)
                }
                
                
                let location = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
                
                /// remove stop
                if(self.routeStopsAdded.count <= 3){
                    
                    var feature = Turf.Feature(geometry: .point(Point(location.coordinate)))
                    feature.properties = [
                        "type":.string(type),
                        "name":.string(name),
                        "address":.string(address),
                        "stationName":.string(stationName),
                        "addStop":.boolean(true),
                        "image_id":.string("\(self.amenitySymbolThemeType)\(type)"),
                        "addStopButton":.boolean(addStopButton),
                        "amenityID":.string(amenityID)
                    ]
                    self.removeWayPoints(location: location.coordinate, sName:amenityID,feature:feature, type:type)
                }
                
            }
        }
    }
    
    func TagAmenityOnMap(event_value: String){
        
        if(self.planTrip != nil){
            AnalyticsManager.shared.logClickEvent(eventValue: event_value, screenName: ParameterName.EditRoutePlanning.screen_view)
        }
        else{
            
            AnalyticsManager.shared.logClickEvent(eventValue: event_value, screenName: ParameterName.RoutePlanning.screen_view)
        }
        
    }
    
    private func setSelectedState(amenityID: String, type: String) {
        if type == "ERP" {
//            updateMapWithAmenities(selectedID: "")
//            updateMapWithAmenitiesForEV(selectedID: "")
        } else if type == Values.EV_CHARGER { // reset selection for amenity and traffic camera
            if evChargerView != nil {
                updateMapWithAmenitiesForEV(selectedID: amenityID)
            }
        } else if type == Values.PETROL {
            if petrolKioskView != nil {
                updateMapWithAmenities(selectedID: amenityID)
            }
        }
    }
    
     func adjustCameraWhenAmenityClicked(at location: LocationCoordinate2D, callout: ToolTipsView) {
         
         let cameraOptions = CameraOptions(center: location, padding:UIEdgeInsets(top: 200, left: 10, bottom: 10, right: 10), zoom: self.navMapView.mapView.mapboxMap.cameraState.zoom) // using current zoom level
         
        self.navMapView.mapView.camera.ease(to: cameraOptions, duration: Values.standardAnimationDuration, curve: .easeInOut, completion: nil)
        
       
        callout.adjust(to: location, in: self.navMapView)
    }
    
    func didDeselectAnnotation() {
       if let callout = callOutView {
           if callout is PetrolToolTipsView {
               setSelectedState(amenityID: "", type: Values.PETROL)
           } else if callout is EVChargerToolTips {
               setSelectedState(amenityID: "", type: Values.EV_CHARGER)
           }
           callout.dismiss(animated: true)
           callout.removeFromSuperview()
           self.callOutView = nil
       }
   }
}

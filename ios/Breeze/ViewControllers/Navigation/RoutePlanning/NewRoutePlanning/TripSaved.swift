//
//  TripSaved.swift
//  Breeze
//
//  Created by VishnuKanth on 24/01/22.
//

import UIKit

protocol TripSavedPopupVCDelegate: AnyObject {
    func onDismiss(goTonavigation:Bool)
    func onConfirm()
}

class TripSavedPopupVC: UIViewController {

    @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var secondaryLabel:UILabel!
    @IBOutlet weak var mainButton:UIButton!
    var secondaryString = ""
    var mainButtonTitle = ""
    // MARK: - Public properties
    weak var delegate: TripSavedPopupVCDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        AnalyticsManager.shared.logPopUpEvent(popupName: ParameterName.RoutePlanning.UserPopUp.trip_been_saved, eventName: ParameterName.RoutePlanning.UserPopUp.popup_open)
        
        
        
        if(mainButtonTitle != ""){
            
            mainButton.setTitle(mainButtonTitle, for: .normal)
        }

        let secondaryLabelTextColor = UIColor(named:"successTitleColor")!
        if(secondaryString.contains("Enable notification")){
            
            let attributedString = NSMutableAttributedString(string: secondaryString, attributes: [
                .font: UIFont(name: fontFamilySFPro.Regular, size: 16)!,
                .foregroundColor: secondaryLabelTextColor
            ])
            attributedString.addAttributes([
                .font: UIFont(name: fontFamilySFPro.Bold, size: 18)!,
                .foregroundColor: UIColor(named: "rpPurpleTextColor")!,
            ], range: NSRange(location: 7, length: 19))
            
            secondaryLabel.attributedText = attributedString
            
            secondaryLabel.isUserInteractionEnabled = true
            let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(tappedOnLabel(_:)))
            tapGesture.numberOfTouchesRequired = 1
            secondaryLabel.addGestureRecognizer(tapGesture)
        }
        else{
            
            let attributedString = NSMutableAttributedString(string: secondaryString, attributes: [
                .font: UIFont(name: fontFamilySFPro.Regular, size: 18)!,
                .foregroundColor: secondaryLabelTextColor
            ])
            secondaryLabel.attributedText = attributedString
        }
//        setupDarkLightAppearance(forceLightMode: true)
    }
    
    @objc func tappedOnLabel(_ gesture: UITapGestureRecognizer) {
        //        guard let text = Constants.termsAndConditions else { return }
        let notificationRange = (secondaryString as NSString).range(of: "Enable notification")
        
        if gesture.didTapAttributedTextInLabel(label: secondaryLabel, inRange: notificationRange) {
            //AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.term_of_service, screenName: ParameterName.createAccount_step1)
            
            if let url = URL(string: UIApplication.openSettingsURLString) {
                if UIApplication.shared.canOpenURL(url) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                }
            }
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        popupView.layer.cornerRadius = 24
        popupView.layer.masksToBounds = true
    }

    @IBAction func onConfirm(_ sender: Any) {
        delegate?.onConfirm()
        dismiss()
    }

    @IBAction func onCancel(_ sender: Any) {
        delegate?.onDismiss(goTonavigation: self.mainButtonTitle == "" ? false : true)
        dismiss()
    }
    
    private func dismiss() {
        //AnalyticsManager.shared.logClosePopupEvent(popupName: ParameterName.Home.UserPopup.email, screenName: ParameterName.Home.screen_view)
        removeChildViewController(self)
        
        AnalyticsManager.shared.logPopUpEvent(popupName: ParameterName.RoutePlanning.UserPopUp.trip_been_saved, eventName: ParameterName.RoutePlanning.UserPopUp.popup_close
        )
    }
    
}

//
//  NewRoutePlanningVC+Carpark.swift
//  Breeze
//
//  Created by Zhou Hao on 4/1/22.
//

import UIKit
import SnapKit
import CoreLocation
@_spi(Restricted) import MapboxMaps
import MapboxDirections

extension NewRoutePlanningVC: ParkingViewDelegate {
    func didUpdate(_ view: ParkingView, type: ParkingType) {
        if parkingMode != .hide {
            self.checkShowCongestion()
            self.removeOtherLayerFromParkingOn()
        }
                
        didUpdateCarParkState()
    }
    
    func didChangeViewType(_ view: ParkingView, viewType: ParkingViewType) {
        switch viewType {
        case .hidden, .collapse:
            appDelegate().window?.removeTapView(Values.tapViewTag)
        case .expanded:
            isTrackingUser = true
            processExpandedParkingView()
        }
    }
    
    private func processExpandedParkingView() {
        appDelegate().window?.addTapView(tag: Values.tapViewTag, handle: { [weak self] in
            guard let self = self else { return }
            self.parkingView?.collapseIfNeed(delay: 0.5)
        })
    }
}

// TODO: Move to RoutePlanningViewModel if need to sync with CarPlay
extension NewRoutePlanningVC {

    func setupParkingButton() {
        
//        if self.parkingButton == nil {
//            let size = Images.toggleParkingBtn!.size
//            let parkingButton = ToggleButton(frame: CGRect(x: 0, y: 0, width: size.width, height: size.height))
//            parkingButton.onImage = Images.toggleParkingBtn
//            parkingButton.offImage = Images.toggleParkingBtnOn
//
//            parkingButton.onToggled = { [weak self] isSelected in
//                guard let self = self else { return }
//
//                self.hideCongestionAndVoucher()
//
//                self.onParking(isParking: isSelected)
//            }
//            navMapView.mapView.addSubview(parkingButton)
//            self.parkingButton = parkingButton
//
//            self.parkingButton!.translatesAutoresizingMaskIntoConstraints = false
//            self.parkingButton!.snp.makeConstraints { make in
//                make.trailing.equalToSuperview().offset(0)
//                make.top.equalTo(self.topView.snp.bottom).offset(60)
//                make.size.equalTo(CGSize(width: size.width, height: size.height))
//            }
//        }
        
        guard parkingView == nil else { return }
        
        parkingView = ParkingView()
        
        parkingView?.delegate = self
        
        guard let parkingView = parkingView else { return }
        
        navMapView.mapView.addSubview(parkingView)
        
        parkingView.snp.makeConstraints { make in
            make.trailing.equalToSuperview().offset(0)
            make.top.equalTo(self.topView.snp.bottom).offset(60)
        }
        parkingView.parkingViewType = .collapse
        parkingView.setParkingMode(type: .all, updateDate: false)
    }
    
    //MARK: - UI SetUp
    func setupRecenterButton(isTrack: Bool) {
        let recenterButton = UserLocationButton(buttonSize: (Images.locationImage?.size.width)!)
        recenterButton.addTarget(self, action: #selector(tapRecenterButton), for: .touchUpInside)
        
        // Setup constraints such that the button is placed within
        // the upper left corner of the view.
        recenterButton.translatesAutoresizingMaskIntoConstraints = false
        navMapView.mapView.addSubview(recenterButton)
        recenterButton.snp.makeConstraints { make in
            make.leading.equalTo(self.parkingView!).offset(0)
//            make.bottom.equalTo(self.feedbackButton.snp.top).offset(-20)
            make.bottom.equalToSuperview().offset(-(269 + 20))
        }

        self.recenterButton = recenterButton
        recenterButton.isHidden = isTrack // hide it by default
    }
    
    func recenterMapAccordingToRoute() {                
        if let routePreferenceData = self.viewModel.mobileRPMapViewUpdatable?.routePreferenceData{
            if let response = routePreferenceData.routeResponse {
                setCameraPosition(routes: response.routes ?? [])
            }
        }
    }
    
    @objc func tapRecenterButton(sender: UserLocationButton) {
        isTrackingUser = true
        
        recenterMapAccordingToRoute()
        
        self.dismissTooltipIfNeeded()
        
        if parkingMode != .hide {
            self.resumeParking()
        }
    }
    
    private func didUpdateCarParkState() {
        
        isTrackingUser = true
        
        switch parkingMode {
        case .all, .available:
            if(self.planTrip != nil){
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.EditRoutePlanning.UserToggle.parking_on, screenName: ParameterName.EditRoutePlanning.screen_view)
            }
            else{
                
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.RoutePlanning.UserToggle.parking_on, screenName: ParameterName.RoutePlanning.screen_view)
            }
            navMapView.pointAnnotationManager?.delegate = self
            if self.shouldUpdateCarparks() {
                viewModel.centerMapLocation = navMapView.getMapCenterCoordinate()
            }
            loadCarparks()
        case .hide:
            if(self.planTrip != nil){
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.EditRoutePlanning.UserToggle.parking_off, screenName: ParameterName.EditRoutePlanning.screen_view)
            }
            else{
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.RoutePlanning.UserToggle.parking_off, screenName: ParameterName.RoutePlanning.screen_view)
            }
            
            self.navMapView.mapView.removeCarparkLayer()
            
            if let callout = self.callOutView, callout is CarparkToolTipsView {
                callout.dismiss(animated: true)
                callout.removeFromSuperview()
                self.callOutView = nil
            }
            
            if self.carParkToast != nil {
                
                self.carParkToast?.removeFromSuperview()
            }
            carparkUpdater?.removeCarParks()
            carparkUpdater = nil
            
            let annotations = [PointAnnotation]()
            self.navMapView.pointAnnotationManager?.annotations = annotations
            
            if let destinationCarPark = self.destinationCarPark{
                
                if !destinationCarPark.hasVoucher{
                    
                    self.destinationCarPark = nil
                }
            }
            if let destination = lastDestinationCoordinate {
                refreshAnnotations(at: destination)
            } else if let destination = viewModel.address {
                let location = CLLocationCoordinate2D(latitude: destination.lat, longitude: destination.long)
                refreshAnnotations(at: location)
            }
            
            self.setCameraPosition(routes: self.response?.routes ?? [])
        }
    }
    
//    private func onParking(isParking: Bool) {
//
//        // TODO: Analytics
////        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Home.UserClick.parking, screenName: ParameterName.Home.screen_view)
//        guard let currentRoute = self.currentRoute else { return }
//        self.isParkingOn = isParking
//        if (self.isParkingOn){
//
//            if(self.planTrip != nil){
//                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.EditRoutePlanning.UserToggle.parking_on, screenName: ParameterName.EditRoutePlanning.screen_view)
//            }
//            else{
//
//                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.RoutePlanning.UserToggle.parking_on, screenName: ParameterName.RoutePlanning.screen_view)
//            }
//
//            self.parkingButton.setImage(UIImage(named: "parkingOn"), for: .normal)
//
////            loadCarparks()
//            navMapView.pointAnnotationManager?.delegate = self
//            loadCarparks()
//        } else {
//            if(self.planTrip != nil){
//                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.EditRoutePlanning.UserToggle.parking_off, screenName: ParameterName.EditRoutePlanning.screen_view)
//            }
//            else{
//                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.RoutePlanning.UserToggle.parking_off, screenName: ParameterName.RoutePlanning.screen_view)
//            }
//
//            self.parkingButton.setImage(UIImage(named: "parkingOff"), for: .normal)
//            self.navMapView.mapView.removeCarparkLayer()
//            let annotations = [PointAnnotation]()
//            self.navMapView.pointAnnotationManager?.annotations = annotations
//            self.destinationCarPark = nil
//            if let destination = lastDestinationCoordinate {
//                refreshAnnotations(at: destination)
//            } else if let destination = viewModel.address {
//                let location = CLLocationCoordinate2D(latitude: destination.lat, longitude: destination.long)
//                refreshAnnotations(at: location)
//            }
//
//            if let callout = self.callOutView, callout is CarparkToolTipsView {
//                callout.dismiss(animated: true)
//                callout.removeFromSuperview()
//                self.callOutView = nil
//            }
//
//            if self.carParkToast != nil {
//
//                self.carParkToast?.removeFromSuperview()
//            }
//            carparkUpdater = nil
//            self.setCameraPosition(routes: self.response?.routes ?? [])
//        }
//
//    }
    
    func removeCarparks(){
        
        
        if let callout = self.callOutView, callout is CarparkToolTipsView {
            callout.dismiss(animated: true)
            callout.removeFromSuperview()
            self.callOutView = nil
        }
        
        if self.carParkToast != nil {
            
            self.carParkToast?.removeFromSuperview()
        }
        carparkUpdater?.removeCarParks()
        carparkUpdater = nil
        
    }
    
    func loadCarparks() {
        guard let destination = viewModel.address, let destinationAnnotation = self.destinationAnnotation else { return }
        
        self.carparkAnnotations = []
        if let destinationCarPark = self.destinationCarPark{
            
            if !destinationCarPark.hasVoucher{
                
                self.destinationCarPark = nil
            }
        }
        var location = CLLocationCoordinate2D(latitude: destination.lat, longitude: destination.long)
        
        let arrivalTime  = Date(timeInterval: (response?.routes![self.selectedIndex].expectedTravelTime)!, since: Date())
        
        self.hideVoucherView()
        
        carparkUpdater = CarParkUpdater(destName: destination.name,
                                        carPlayNavigationMapView: nil,
                                        mobileNavigationMapView: self.navMapView,
                                        fromScreen: .routePlaning)
        
        carparkUpdater?.delegate = self
        
        carparkUpdater?.configure(arrivalTime: arrivalTime, destName: destination.address)
        carparkUpdater?.configSearchVoucher(isParkingOn ? .parking : .searchOnly)
        
        carparkUpdater?.carparkAvailability = parkingMode == .hide ? .all : parkingMode
        
        if self.shouldUpdateCarparks(), let centerLocation = viewModel.centerMapLocation {
            location = centerLocation
        }
        
        carparkUpdater?.loadCarParks(maxCarParksCount: UserSettingsModel.sharedInstance.carparkCount,
                                     radius: UserSettingsModel.sharedInstance.carparkDistance,
                                     maxCarParkRadius: Values.carparkMonitorMaxDistanceInLanding,
                                     location: location)
        
        
//        carparkViewModel = CarparksViewModel(service: CarparkService(), arrivalTime: arrivalTime, destName:  destination.address1 )
//        carparkViewModel.load(at: location,
//                              count: Values.maxNoOfCarparksInRoutePlanning,
//                              radius: Values.carparkMonitorDistanceInRoutePlanning,
//                              maxRadius: Values.carparkMonitorMaxDistanceInLanding)
//        carparkViewModel.$carparks
//            .sink { [weak self] carparks in
//            guard let self = self, let carparks = carparks else { return }
//
//            self.updateCarparks(carparks, destinationAnnotation: destinationAnnotation, location: location)
//
//        }.store(in: &disposables)
    }
    
    // TODO: Refactor. To make it reusable in MapLanding and RoutePlanning
//    private func updateCarparks(_ carparks: [Carpark], destinationAnnotation:PointAnnotation, location: CLLocationCoordinate2D) {
//        var annotations = [PointAnnotation]()
//        var destinationCarparkAnnotation: Annotation? = nil
//
//        carparks.forEach { carPark in
//            let annotation = self.addCarparkAnnotation(carpark: carPark)
//            if carPark.destinationCarPark ?? false  {
//                destinationCarparkAnnotation = annotation
//                self.destinationCarPark = carPark
//            }
//            annotations.append(annotation)
//        }
//        if destinationCarparkAnnotation == nil { // add original destination icon
//            annotations.append(destinationAnnotation)
//        } else {
//            // No need to show the callout by default
////                self.didSelectCarparkAnnotation(annotation: destinationCarparkAnnotation!)
//        }
//        self.navMapView.pointAnnotationManager?.annotations = annotations
//        self.carparkAnnotations = annotations
//        if carparks.count > 0 {
//            self.navMapView.mapView.removeCarparkLayer()
//            self.navMapView.mapView.addCarparks(carparks, showOnlyWithCostSymbol: true)
//
//            // get the nearest carpark
//            self.nearestCarPark = self.getNearestCarpark(carparks: carparks)
//            self.adjustZoomForCarParks(carParks: carparks)
//            //self.setupZoomlevel(location: location)
//
//            if let first = carparks.first {
//                if (first.distance ?? 0) > Values.carparkMonitorDistanceInLanding {
//
//                    DispatchQueue.main.async {
//                        let carparkToast = BreezeToast(appearance: BreezeToastStyle.carparkToast, onClose: {
//                            //No need to handle this action
//                        }, actionTitle: "", onAction: nil)
//
//                        carparkToast.show(in: self.view, title: "Nearby Carpark", message: Constants.toastNearestCarpark, animated: true)
//                        self.view.bringSubviewToFront(carparkToast)
//                        carparkToast.yOffset = 40
//                    }
//                }
//            }
//        }
//    }
    
    func adjustZoomForCarParks(carParks:[Carpark]?){
        
        var distanceMonitor = 0
        var carParkCoordinate = [CLLocationCoordinate2D]()
        if let prevCarPark = carParks {
            
            prevCarPark.forEach { carPark in
                
                let distance = carPark.distance ?? 0
                if(distanceMonitor == 0){
                    
                    distanceMonitor = carPark.distance ?? 0
                    
                    if(carParkCoordinate.count > 0){
                        carParkCoordinate.removeAll()
                        carParkCoordinate.append(CLLocationCoordinate2D(latitude: carPark.lat ?? 0.0, longitude: carPark.long ?? 0.0))
                    }
                    else{
                        
                        carParkCoordinate.append(CLLocationCoordinate2D(latitude: carPark.lat ?? 0.0, longitude: carPark.long ?? 0.0))
                    }
                }
                    
                    if(distanceMonitor < distance){
                        
                        distanceMonitor = distance
                        if(carParkCoordinate.count > 0){
                            carParkCoordinate.removeAll()
                            carParkCoordinate.append(CLLocationCoordinate2D(latitude: carPark.lat ?? 0.0, longitude: carPark.long ?? 0.0))
                        }
                        else{
                            
                            carParkCoordinate.append(CLLocationCoordinate2D(latitude: carPark.lat ?? 0.0, longitude: carPark.long ?? 0.0))
                        }
                    }
                
            }
            
            if carParkCoordinate.count > 0 {
                
                distanceMonitor = max(distanceMonitor, 50)
                
                DispatchQueue.main.async {
                  self.navMapView.mapView.setDynamicZoomLevelBasedonRadius(coordinate1: carParkCoordinate[0], dynamicRadius: distanceMonitor, padding: self.cameraPadding(), coordinate2: CLLocationCoordinate2D(latitude: self.viewModel.address.lat, longitude: self.viewModel.address.long))
                    print(self.navMapView.mapView.cameraState.zoom)
                }
                
            }
            
            
        }
    }
    
//    private func setupZoomlevel(location: CLLocationCoordinate2D) {
//
//        // destination has a carpark
//        if let destinationCarpark = self.destinationCarPark {
//            if let nearest = self.nearestCarPark {
//                setupZoomelevel(coordinate1: CLLocationCoordinate2D(latitude: destinationCarpark.lat ?? 0, longitude: destinationCarpark.long ?? 0), coordinate2: CLLocationCoordinate2D(latitude: nearest.lat ?? 0, longitude: nearest.long ?? 0))
//            } else {
//                DispatchQueue.main.async { [weak self] in
//                    guard let self = self else { return }
//                    self.setCameraPosition(routes: self.response?.routes ?? [])
//                }
//            }
//        } else {
//            // destination has no carpark, so use nearest carpark as the coordinate1
//            if let nearest = self.nearestCarPark {
//                setupZoomelevel(coordinate1:CLLocationCoordinate2D(latitude: nearest.lat ?? 0, longitude: nearest.long ?? 0), coordinate2: location)
//            }
//        }
//    }
        
//    private func setupZoomelevel(coordinate1: CLLocationCoordinate2D, coordinate2: CLLocationCoordinate2D) {
//
//        DispatchQueue.main.async { [weak self] in
//            guard let self = self else { return }
//
//            // use coordinate1 as center point calcuate the symmetric coordinate3
//            let degree = coordinate2.direction(to: coordinate1)
//            let distance = coordinate2.distance(to: coordinate1)
//            let coordinate3 = coordinate1.coordinate(at: distance, facing: degree)
//
//            let cameraOptions = self.navMapView.mapView.mapboxMap.camera(for: [coordinate2, coordinate1, coordinate3], padding: self.cameraPadding(), bearing: 0, pitch: 0)
//            self.navMapView.mapView.camera.ease(to: cameraOptions, duration: Values.standardAnimationDuration, curve: .easeInOut, completion: nil)
//        }
//    }
    
    private func getNearestCarpark(carparks: [Carpark]) -> Carpark? {
        // get the cheapest (which is the nearest carpark)
        if let nearest = (carparks.filter { $0.isCheapest ?? false }).first {
            return nearest
        }
        return nil
    }
    
    private func getDestinationCarpark(carparks: [Carpark]) -> Carpark? {
        for carpark in carparks {
            if carpark.destinationCarPark ?? false {
                return carpark
            }
        }
        return nil
    }
    
    private func checkDestinationCarParkHasVoucher(destCarPark: Carpark?) -> Carpark? {
        if let destCarPark = destCarPark {
            
            if(destCarPark.hasVoucher){
                
                return destCarPark
            }
        }
        
        return nil
    }
    

    
//    private func addCarparkAnnotation(carpark: Carpark) -> PointAnnotation {
//
//        let isDestination = carpark.destinationCarPark ?? false
//        //let availableLots = carpark.availablelot?.intValue ?? 0
//        var annotation = PointAnnotation(id:carpark.id!,coordinate: CLLocationCoordinate2D(latitude: carpark.lat ?? 0, longitude: carpark.long ?? 0))
//
///*
//        /// We should not use same annotation name for availableLots and noLots.  Next time when we set annotation image it loads from cache
//        //annotation.image = .init(image: UIImage(named: availableLots == 0 ? "carparkNoLotsIcon" : "carparkIcon")!, name: "carparkIcon")
//        if isDestination {
//
//            if let availableLots = carpark.availablelot?.intValue {
//
//                if(availableLots == 0){
//                    annotation.image = .init(image: setDestinationAnnotationImage(noLots: true), name: "routePlan")
//                }
//                else{
//
//                    annotation.image = .init(image: setDestinationAnnotationImage(noLots: false), name: "destinationCarPark")
//                }
//
//            }
//
//            else{
//
//                annotation.image = .init(image: setDestinationAnnotationImage(noLots: false), name: "destinationCarPark")
//            }
//
//        }
//
//        else {
//
//            if let availableLots = carpark.availablelot?.intValue {
//
//                if(availableLots == 0){
//                    annotation.image = .init(image: setAnnotationImage(noLots: true), name: "carParkNoLots")
//                }
//                else{
//                    annotation.image = .init(image: setAnnotationImage(noLots: false), name: "carparkIcon")
//                }
//            }
//            else{
//
//                annotation.image = .init(image: setAnnotationImage(noLots: false), name: "carparkIcon")
//            }
//
//        }
//*/
//        annotation.image = getAnnotationImage(carpark: carpark, selected: false, ignoreDestination: false)
//
//        //annotation.textField = carpark.id
//        annotation.userInfo = [
//            "carpark": carpark
//        ]
//
//        let layerId = navMapView.pointAnnotationManager!.layerId
//
//        try? self.navMapView.mapView.mapboxMap.style.setLayerProperty(for: layerId, property: "icon-allow-overlap", value: true)
//
//        annotation.symbolSortKey = Double(carpark.distance ?? 0)
//        return annotation
//    }
/*
    private func setDestinationAnnotationImage(noLots:Bool) -> UIImage{
        
        if(noLots){
            
            return UIImage(named: "destinationWithCarparkNoLots")!
        }
        else{
            
            return UIImage(named: "destinationWithCarpark")!
            
        }
            
    }
    
    private func setAnnotationImage(noLots:Bool) -> UIImage{
        
        if(noLots){
            
            return UIImage(named: "carparkNoLotsIcon")!
        }
        else{
            
            return UIImage(named: "carparkIcon")!
            
        }
            
    }
*/
}

extension NewRoutePlanningVC: AnnotationInteractionDelegate {
    
    func annotationManager(_ manager: AnnotationManager, didDetectTappedAnnotations annotations: [Annotation]) {
        
        // possible multiple tapped if there are overlapped together, only show the first one
        if let annotation = annotations.first {
            didSelectCarparkAnnotation(annotation: annotation, tapped: true)
        }

//        if let annotation = annotations.first {
//            if let carpark = annotation.userInfo?["carpark"] as? Carpark {
//                
//                if carpark.id == self.currentSelectedCarpark?.id {
//                    
//                    self.currentSelectedCarpark = nil
//                }
//            }
//            didSelectCarparkAnnotation(annotation: annotation, tapped: true)
//        }
    }

    func didSelectCarparkAnnotation(annotation: Annotation, tapped: Bool = false) {

        DispatchQueue.main.async { [weak self] in
                        
            guard let self = self else { return }
            
            self.didDeselectAnnotation()
//            self.didDeselectCarparkAnnotation()
            if let carpark = annotation.userInfo?["carpark"] as? Carpark {
            
                if carpark.id == self.currentSelectedCarpark?.id {
                    self.currentSelectedCarpark = nil
                    return
                }

                self.currentSelectedCarpark = carpark
                self.currentSelectedParkAnnotation = annotation
                self.updateAnnotationImage(annotation, select: true)
                
                // set camera (actually should not set if not because of tapped
                if tapped {
                    let cameraOptions = CameraOptions(center: CLLocationCoordinate2D(latitude: carpark.lat ?? 0, longitude: carpark.long ?? 0), zoom: self.navMapView.mapView.mapboxMap.cameraState.zoom) // using current zoom level
                    self.navMapView.mapView.camera.ease(to: cameraOptions, duration: Values.standardAnimationDuration, curve: .easeInOut, completion: nil)
                }
                
                // TODO: TO BE IMPLEMENTED
/*
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.routeplanning_tap_carpark, screenName: ParameterName.routeplanning_carpark_screen)
*/
                let callOutView = CarparkToolTipsView(carpark: carpark, calculateFee: true, routeablePoint: nil)
                callOutView.showShadow = false
                callOutView.present(from: self.navMapView.mapView.frame, in: self.navMapView.mapView, constrainedTo: callOutView.frame, animated: true)
                self.callOutView = callOutView
                callOutView.adjust(to: callOutView.tooltipCoordinate, in: self.navMapView)
                self.adjustCameraWhenAmenityClicked(at: callOutView.tooltipCoordinate, callout: callOutView)
                callOutView.onCalculate = { [weak self]  theCarpark in
                    guard let self = self else { return }
                    
                    if(self.planTrip != nil){
                        self.TagAmenityOnMap(event_value:ParameterName.EditRoutePlanning.UserClick.parking_calculatefee)
                    }
                    else{
                        self.TagAmenityOnMap(event_value:ParameterName.RoutePlanning.UserClick.parking_calculatefee)
                    }
                    
                    
                    let carParkData = [
                            "toScreen": "ParkingCalculator", // Screen name you want to display when start RN
                            "navigationParams": [ // Object data to be used by that screen
                            "parkingID": theCarpark.id,
                            "start_timestamp": self.tripEstStartTime  == 0 ? NSNumber(value:Int(Date().timeIntervalSince1970 * 1000)) : NSNumber(value: Int(self.tripEstStartTime * 1000)),
                            "fromNative":true,
                            Values.FOCUSED_LAT :self.viewModel.address.lat.toString(),
                            Values.FOCUSED_LONG :self.viewModel.address.long.toString()
                            ],
                    ] as [String : Any]
                    let rootView = RNViewManager.sharedInstance.viewForModule(
                        "Breeze",
                        initialProperties: carParkData)
                    
                    let reactNativeVC = UIViewController()
                    reactNativeVC.view = rootView
                    reactNativeVC.modalPresentationStyle = .fullScreen
                    self.present(reactNativeVC, animated: true, completion: nil)
                    
                }
                callOutView.onMoreInfo = { [weak self]  theCarpark in
                    guard let self = self else { return }
                    
                    if(self.planTrip != nil){
                        self.TagAmenityOnMap(event_value:ParameterName.EditRoutePlanning.UserClick.parking_moreinfo)
                    }
                    else{
                        self.TagAmenityOnMap(event_value:ParameterName.RoutePlanning.UserClick.parking_moreinfo)
                    }
                    
                    var locationLat: Double = 0
                    var locationLong: Double = 0
                 
                    if self.shouldUpdateCarparks() {
                        locationLat = self.viewModel.centerMapLocation?.latitude ?? 0
                        locationLong = self.viewModel.centerMapLocation?.longitude ?? 0
                    }else {
                        locationLat = self.viewModel.address.lat
                        locationLong = self.viewModel.address.long
                    }
                    
                    let carParkData = [
                            "toScreen": "ParkingPriceDetail", // Screen name you want to display when start RN
                            "navigationParams": [ // Object data to be used by that screen
                            "parkingId": theCarpark.id,
                            "idToken":AWSAuth.sharedInstance.idToken,
                            "baseURL": appDelegate().backendURL,
                            Values.FOCUSED_LAT: locationLat.toString(),
                            Values.FOCUSED_LONG :locationLong.toString()
                            ],
                    ] as [String : Any]
                    let rootView = RNViewManager.sharedInstance.viewForModule(
                        "Breeze",
                        initialProperties: carParkData)
                    
                    let reactNativeVC = UIViewController()
                    reactNativeVC.view = rootView
                    reactNativeVC.modalPresentationStyle = .pageSheet
                    self.present(reactNativeVC, animated: true, completion: nil)
                }
                callOutView.onCarparkSelectedV2 = { [weak self] (theCarpark, _)in
                    guard let self = self else { return }
                    
                    self.selectedCarPark = theCarpark
                    if(self.planTrip != nil){
                        self.TagAmenityOnMap(event_value:ParameterName.EditRoutePlanning.UserClick.parking_navigatehere)
                    }
                    else{
                        
                        self.TagAmenityOnMap(event_value:ParameterName.RoutePlanning.UserClick.parking_navigatehere)
                    }
                    
                    let location = CLLocation(latitude: theCarpark.lat ?? 0, longitude: theCarpark.long ?? 0)
                    getAddressFromLocation(location: location) { address in
                        let address = SearchAddresses(alias: "", address1: theCarpark.name ?? "", lat: "\(theCarpark.lat ?? 0)", long: "\(theCarpark.long ?? 0)", address2: address, distance: "\(theCarpark.distance ?? 0)")
                        self.reset(address: address)
                    }
                }
                
                callOutView.shareViaLocation = {
                    shareLocationFromNative(address1: carpark.name ?? "", latitude: carpark.lat?.toString() ?? "", longtitude: carpark.long?.toString() ?? "", name: carpark.name ?? "")
                }
                
                callOutView.inviteLocation = { 
                    inviteLocationFromNative(address1: carpark.name ?? "", latitude: carpark.lat?.toString() ?? "", longtitude: carpark.long?.toString() ?? "", name: carpark.name ?? "")
                }
            }
        }
    }

    func reset(address: SearchAddresses) {
        
        isAddressReset = true
        didDeselectCarparkSymbol()
//        isParkingOn = false
        self.parkingView.setParkingMode(type: .all)
        self.originalAddress = address
        selectedAddress = address
        self.isDestinationSelected = true
        self.addressReceived = true
        self.enableSave = true
//        self.parkingButton.setImage(UIImage(named: "parkingOff"), for: .normal)
        
        // rerequest route
        //clearMap()
        
        viewModel.wayPointsAdded.removeAll()
        //if let routeStops = routeStopsAdded {
            
            if(routeStopsAdded.count > 0){
            
               viewModel.wayPointsAdded = routeStopsAdded
            }
        //}
        //viewModel.setupRoute()
        self.broadcastMessageView.isHidden = true
    }
    
    // deselect carpark callout
    func didDeselectCarparkSymbol() {
        if let callout = self.callOutView,
        callout is CarparkToolTipsView {
            if let annotation = currentSelectedParkAnnotation {
                updateAnnotationImage(annotation, select: false)
            }
            self.currentSelectedParkAnnotation = nil
            callout.dismiss(animated: true)
            callout.removeFromSuperview()
            self.callOutView = nil
        }
        if(isAddressReset == false){
            
            currentSelectedCarpark = nil
        }
        else{
            
            isAddressReset = false
        }
        
    }
}

//  MARK: - CarParkUpdaterDelegate methods
extension NewRoutePlanningVC: CarParkUpdaterDelegate {
    func carParkNavigateHere(carPark: Carpark?) {
        
    }
    
    func didUpdateCarparks(carParks: [Carpark]?, location: CLLocationCoordinate2D) {
        guard let carparks = carParks else { return }
        if carparks.count > 0 {
//            self.navMapView.mapView.addCarparks(carparks, showOnlyWithCostSymbol: true)

            // get the nearest carpark
            self.nearestCarPark = self.getNearestCarpark(carparks: carparks)
            if (carparkUpdater?.type != .searchOnly) {
                
                if let _ = viewModel.centerMapLocation {
                    //  Do nothing
                }else {
//                    self.adjustZoomForCarParks(carParks: carparks)
                }
            }
            self.destinationCarPark = self.getDestinationCarpark(carparks: carparks)
            
            if isParkingOn {
                if let destination = lastDestinationCoordinate {
                    refreshAnnotations(at: destination)
                }
            }
            
            if self.destinationCarPark != nil && self.destinationCarPark?.hasVoucher ?? false {
                
                if let destination = lastDestinationCoordinate {
                    
                    refreshAnnotations(at: destination)
                }
                
            }

            //self.setupZoomlevel(location: location)
            if let first = carparks.first {
                if (first.distance ?? 0) > Values.carparkMonitorDistanceInLanding {

                    DispatchQueue.main.async { [weak self] in
                        
                        guard let self = self else { return }
                        if self.isParkingOn {
                            
                            if(self.carParkToast != nil){
                                self.carParkToast?.removeFromSuperview()
                            }
                            
                            self.carParkToast = BreezeToast(appearance: BreezeToastStyle.carparkToast, onClose: {
                                //No need to handle this action
                            }, actionTitle: "", onAction: nil)

                            self.carParkToast!.show(in: self.view, title: "Nearby Carpark", message: Constants.toastNearestCarpark, animated: true)
                            self.view.bringSubviewToFront(self.carParkToast!)
                            self.carParkToast!.yOffset = getTopOffset()
                            
                        }
                        
                    }
                }
            }
            // because we have delay 0.1 second in carpark updater
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(101), execute: {
                if (self.carparkUpdater?.type == .searchOnly) {
                    self.checkShowVoucherView()
                }
            })
        }
    }
}

//  MARK: - RoutePlaning + Voucher
extension NewRoutePlanningVC {
    /*
     check show voucher view or not
     if congestion view is showing, we need wait 5 seconds before show voucher banner
     */
    func checkShowVoucherView() {
        // We have to wait for zone color api get succesful, after that. congestion is show or hide then we check voucher banner
        if !self.viewModel.havingZoneColor {
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1)) { [weak self] in
                guard let self = self else { return }
                self.checkShowVoucherView()
            }
            return
        }
        if let updater = carparkUpdater {
            if !self.isHiddenCongestion {
                if updater.needShowVoucherView {
                    timerCheckCongestion?.invalidate()
                    timerCheckCongestion = nil
                    timerCheckCongestion = Timer.scheduledTimer(withTimeInterval: NewRoutePlanningVCConst.timeShowCongestionView, repeats: false, block: { [weak self] timer in
                        guard let self = self else { return }
                        self.hideCongestAndShowVoucher()
                    })
                }
            } else {
                if carparkUpdater?.needShowVoucherView ?? false {
                    self.hideCongestAndShowVoucher()
                } else {
                    self.hideVoucherView()
                }
            }
        }
    }
    
    func hideCongestAndShowVoucher() {
        
        //  Hide broadcast message when showing voucher
        self.showCarparkBroadcastMessage(false, title: "")
        
        if ctrBottomVoucherView.constant == NewRoutePlanningVCConst.bottomVoucherOn {
            return
        }
        
        ctrBottomVoucherView.constant = NewRoutePlanningVCConst.bottomVoucherOff
        viewVoucher.isHidden = false
        
        lbVoucher.text =  (self.destinationCarPark != nil && self.destinationCarPark?.hasVoucher ?? false) ? "Collect parking voucher at destination" : "View nearby carparks with parking voucher"
        imvVoucher.image = UIImage(named: "ic_money_green")
        imvVoucherArrow.isHidden =  (self.destinationCarPark != nil && self.destinationCarPark?.hasVoucher ?? false) ? true : false
        
        viewVoucher.isUserInteractionEnabled = (self.destinationCarPark != nil && self.destinationCarPark?.hasVoucher ?? false) ? false : true
        
        self.view.layoutIfNeeded()
        UIView.animate(withDuration: Values.standardAnimationDuration) {
            self.ctrBottomCongestion.constant = NewRoutePlanningVCConst.bottomConstOff
            self.view.layoutIfNeeded()
        } completion: { isCompleted in
            if isCompleted {
                UIView.animate(withDuration: Values.standardAnimationDuration) {
                    self.ctrBottomVoucherView.constant = NewRoutePlanningVCConst.bottomVoucherOn
                    self.view.layoutIfNeeded()
                }
            }
        }
    }        
    
    func showCarparkBroadcastMessage(_ isShow: Bool, title: String) {
        if !title.isEmpty {
            broadcastMessageView.isHidden = !isShow
            broadcastMessageLabel.text = title
        }else {
            broadcastMessageView.isHidden = true
        }
    }
    
    func hideVoucherView() {
        DispatchQueue.main.async {
            UIView.animate(withDuration: Values.standardAnimationDuration) {
                self.ctrBottomVoucherView.constant = NewRoutePlanningVCConst.bottomVoucherOff
                self.view.layoutIfNeeded()
            }
        }
        
        //  TODO check show broadcast message
    }
    
    func hideCongestionAndVoucher() {
        self.ctrBottomCongestion.constant = NewRoutePlanningVCConst.bottomConstOff
        self.ctrBottomVoucherView.constant = NewRoutePlanningVCConst.bottomVoucherOff
        
        //  TODO check show broadcast message
    }
}

//  MARK: - GestureManagerDelegate methods
extension NewRoutePlanningVC: GestureManagerDelegate {
   
    func gestureManager(_ gestureManager: GestureManager, didEnd gestureType: GestureType, willAnimate: Bool) {
        
        //  Handle explore carparks
        if !willAnimate && gestureType != .pinch && gestureType != .singleTap {
            //  Then consider to update carpark nearby center location
            self.triggerEndPanGesture()
        }
    }
    
    func gestureManager(_ gestureManager: GestureManager, didEndAnimatingFor gestureType: GestureType) {
        
        //  Handle explore carparks
        if gestureType != .pinch && gestureType != .singleTap {
            //  Then consider to update carpark nearby center location
            self.triggerEndPanGesture()
        }
    }
    
    func gestureManager(_ gestureManager: GestureManager, didBegin gestureType: GestureType) {
        
        var name = ""
        if(self.planTrip != nil){
            name = ParameterName.EditRoutePlanning.MapInteraction.drag
        }
        else{
            name = ParameterName.RoutePlanning.MapInteraction.drag
        }
        switch gestureType {
        case .pinch:
            if(self.planTrip != nil){
                name = ParameterName.EditRoutePlanning.MapInteraction.pinch
            }
            else{
                name = ParameterName.RoutePlanning.MapInteraction.pinch
            }
            
        case .singleTap:
            let point = gestureManager.singleTapGestureRecognizer.location(in: navMapView)
            self.querySymbolLayer(point: point) { [weak self] selected in
                guard let self = self else { return }
                if !selected { // if no feature selected
                    self.selectRoute(point)
                }
            }
            
        default:
            if(self.planTrip != nil){
                name = ParameterName.EditRoutePlanning.MapInteraction.drag
            }
            else{
                name = ParameterName.RoutePlanning.MapInteraction.drag
            }
        }

       
        if(self.planTrip != nil){
            AnalyticsManager.shared.logMapInteraction(eventValue: name, screenName: ParameterName.EditRoutePlanning.screen_view)
        }
        else{
            AnalyticsManager.shared.logMapInteraction(eventValue: name, screenName: ParameterName.RoutePlanning.screen_view)
        }
        
        if gestureType == .pan || gestureType == .pinch || gestureType == .doubleTapToZoomIn || gestureType == .doubleTouchToZoomOut {

            isTrackingUser = false            
       }
    }
    
    private func selectRoute(_ tapPoint: CGPoint) {
        guard routes != nil else { return }
        // Disabled the route select in navigationMapView and implement it by ourselves
        if let routes = self.routes(closeTo: tapPoint) {
            guard let selectedRoute = routes.first else { return }
            self.isRouteSelectedDuringPark = true
//            self.displayBroadcastMessage()
            navMapView.delegate?.navigationMapView(navMapView, didSelect: selectedRoute)
        }
    }
    
    private func routes(closeTo point: CGPoint) -> [Route]? {
        let tapCoordinate = navMapView.mapView.mapboxMap.coordinate(for: point)
        // Filter routes with at least 2 coordinates.
        guard let routes = routes?.filter({ $0.shape?.coordinates.count ?? 0 > 1 }) else { return nil }
        
        // Sort routes by closest distance to tap gesture.
        let closest = routes.sorted { (left, right) -> Bool in
            // Existence has been assured through use of filter.
            let leftLine = left.shape!
            let rightLine = right.shape!
            let leftDistance = leftLine.closestCoordinate(to: tapCoordinate)!.coordinate.distance(to: tapCoordinate)
            let rightDistance = rightLine.closestCoordinate(to: tapCoordinate)!.coordinate.distance(to: tapCoordinate)
            
            return leftDistance < rightDistance
        }
        
        // Filter closest coordinates by which ones are under threshold.
        let candidates = closest.filter {
            let closestCoordinate = $0.shape!.closestCoordinate(to: tapCoordinate)!.coordinate
            let closestPoint = navMapView.mapView.mapboxMap.point(for: closestCoordinate)
            
            return closestPoint.distance(to: point) < navMapView.tapGestureDistanceThreshold
        }
        
        return candidates
    }
    
    func updateAnnotationImage(_ selectedAnnotation: Annotation, select: Bool) {
        if let carpark = selectedAnnotation.userInfo?["carpark"] as? Carpark {
            if let annotationMgr = navMapView.pointAnnotationManager {
                let id = selectedAnnotation.id
                for i in 0...annotationMgr.annotations.count-1 {
                    if annotationMgr.annotations[i].id == id {
                        annotationMgr.annotations[i].image = getAnnotationImage(carpark: carpark, selected: select)
                    }
                }
            }
        }
    }

}

extension NewRoutePlanningVC {
    
    func shouldUpdateCarparks() -> Bool {
        return needUpdateCarparks && self.parkingMode != .hide
    }
    
    func resumeParking() {
        needUpdateCarparks = false
        viewModel.centerMapLocation = nil
        self.loadCarparks()
    }
    
    func triggerEndPanGesture() {
        needUpdateCarparks = false
        if self.parkingMode != .hide {
            startUpdateCarparkTimer()
        }
    }
    
    func startUpdateCarparkTimer() {
        stopUpdateCarparkTimer()
        self.updateCarparkTimer = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(self.updateCenterMapCarparks), userInfo: nil, repeats: false)
    }
    
    func stopUpdateCarparkTimer() {
        if updateCarparkTimer?.isValid == true {
            updateCarparkTimer?.invalidate()
            updateCarparkTimer = nil
        }
    }
    
    @objc func updateCenterMapCarparks() {
        needUpdateCarparks = true
        if self.shouldUpdateCarparks(),  let navigationMapView = navMapView {
            viewModel?.centerMapLocation = navigationMapView.getMapCenterCoordinate()
            self.loadCarparks()
        }
    }
}

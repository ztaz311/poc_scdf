//
//  KioskBase.swift
//  Breeze
//
//  Created by VishnuKanth on 31/12/21.
//


import Foundation

struct KioskBase : Codable {
	let element_name : String?
    let element_id:String?
	let sub_items : [Sub_items]?
	

	enum CodingKeys: String, CodingKey {

		case element_name = "element_name"
        case element_id = "element_id"
		case sub_items = "sub_items"
		
	}

}

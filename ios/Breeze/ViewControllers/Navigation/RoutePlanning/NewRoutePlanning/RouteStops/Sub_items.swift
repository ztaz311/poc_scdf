
//
//  Sub_items.swift
//  Breeze
//
//  Created by VishnuKanth on 31/12/21.
//

/*

 This sub_items will be used for both Petrol Kiosk and EVChargers for Route Stops in RoutePlanning screen

*/

import Foundation
struct Sub_items : Codable {
	let element_name : String?
    let count:Int?
	let display_text : String?
	let is_selected : Bool?
    let activeImageURL:String?
    let inActiveImageURL:String?
    let element_id:String?

	enum CodingKeys: String, CodingKey {

		case element_name = "element_name"
		case display_text = "display_text"
		case is_selected = "is_selected"
        case count = "count"
        case activeImageURL = "active_url"
        case inActiveImageURL = "inactive_url"
        case element_id = "element_id"
	}

}

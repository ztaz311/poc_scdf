//
//  DirectionsVC.swift
//  Breeze
//
//  Created by VishnuKanth on 04/01/22.
//

import UIKit
import MapboxNavigation
import MapboxDirections
import MapboxCoreNavigation

class DirectionsVC: UIViewController {
    
    @IBOutlet weak var directionsTableView:UITableView!
    @IBOutlet weak var closeBtn:UIButton!
    
    let cellId = "StepTableViewCellId"
        var routeProgress: RouteProgress!
        typealias StepSection = [RouteStep]
        var sections = [StepSection]()
        var leg:RouteLeg?
        var previousLegIndex: Int = NSNotFound
        var previousStepIndex: Int = NSNotFound

    override func viewDidLoad() {
        super.viewDidLoad()
        directionsTableView.estimatedRowHeight = 110
//        setupDarkLightAppearance(forceLightMode: true)
        
        rebuildDataSourceIfNecessary()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func closebtnAction(){
        
        self.dismiss(animated: true)
    }
    
    func rebuildDataSourceIfNecessary() -> Bool {
//            let legIndex = routeProgress.legIndex
//            let stepIndex = routeProgress.currentLegProgress.stepIndex
//            let didProcessCurrentStep = previousLegIndex == legIndex && previousStepIndex == stepIndex
//
        guard let leg = leg else {
            return false
        }

            sections.removeAll()

//            let currentLeg = routeProgress.currentLeg

           let currentLeg = leg
            // Add remaining steps for current leg
            var section = [RouteStep]()
            for (index, step) in currentLeg.steps.enumerated() {
                //guard index > stepIndex else { continue }
                // Don't include the last step, it includes nothing
                guard index < currentLeg.steps.count - 1 else { continue }
                section.append(step)
            }

            if !section.isEmpty {
                sections.append(section)
            }

            // Include all steps on any future legs
            //if !routeProgress.isFinalLeg {
//                routeProgress.route.legs.suffix(from: routeProgress.legIndex + 1).forEach {
//                    var steps = $0.steps
//                    // Don't include the last step, it includes nothing
//                    _ = steps.popLast()
//                    sections.append(steps)
//                }
            //}

//            previousStepIndex = stepIndex
//            previousLegIndex = legIndex

            return true
        }
        
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        guard previousTraitCollection?.userInterfaceStyle != traitCollection.userInterfaceStyle else {
            return
        }
        directionsTableView.reloadData()
    }

}

extension DirectionsVC:UITableViewDelegate {
    func updateCell(_ cell: StepTableViewCell, at indexPath: IndexPath) {

            let step = sections[indexPath.section][indexPath.row]
            if let instructions = step.instructionsDisplayedAlongStep?.last {
                            
                let visualInstruction = VisualInstructionBanner(distanceAlongStep: step.distance, primary: instructions.primaryInstruction, secondary: instructions.secondaryInstruction, tertiary: instructions.tertiaryInstruction, quaternary: instructions.quaternaryInstruction, drivingSide: instructions.drivingSide)
                cell.manueverView.visualInstruction = visualInstruction.primaryInstruction
                cell.manueverView.drivingSide = visualInstruction.drivingSide
                cell.manueverView.primaryColor = isDarkMode ? .white : .black
                cell.manueverView.secondaryColor = isDarkMode ? .lightGray : .darkGray
                cell.instructionLabel.text = visualInstruction.primaryInstruction.text
                
                if (step.distance) > 1000 {
                    cell.distanceLabel.text =  String(format: "%.1f %@", (step.distance)/1000, "km")
                } else {
                    let roundedDistance = step.distance.rounded()
                    cell.distanceLabel.text =  "\(Int(roundedDistance)) m"
                }
            }
        }
}

extension DirectionsVC: UITableViewDataSource {
    public func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }

    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let steps = sections[section]
        return steps.count
    }

    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! StepTableViewCell
        updateCell(cell, at: indexPath)
        return cell
    }
}

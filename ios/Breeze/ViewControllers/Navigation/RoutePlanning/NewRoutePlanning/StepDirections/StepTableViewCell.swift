//
//  StepTableViewCell.swift
//  Breeze
//
//  Created by VishnuKanth on 05/01/22.
//

import UIKit
import MapboxNavigation

class StepTableViewCell: UITableViewCell {
    
    @IBOutlet weak var manueverView:ManeuverView!
    @IBOutlet weak var distanceLabel:UILabel!
    @IBOutlet weak var instructionLabel:UILabel!
//    @IBOutlet weak var instructionsView:StepInstructionsView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

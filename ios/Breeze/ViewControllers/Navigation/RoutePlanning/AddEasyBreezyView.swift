//
//  AddEasyBreezyView.swift
//  Breeze
//
//  Created by Zhou Hao on 8/2/21.
//

import MapboxMaps
import SnapKit

protocol AddEasyBreezyViewDelegate: class {
    func onAddEditEasyBreezy(place: SavedPlaces, addressId: Int, ebName: String)
}

final class AddEasyBreezyView: UIView {
        
    // MARK: - Constants
    let iconSize: CGFloat = 24
    let padding: CGFloat = 10
    let yPadding: CGFloat = 5
    let gap: CGFloat = 2 // gap between label and icon

    // MARK: - Properties
    weak var delegate: AddEasyBreezyViewDelegate?
    
    private lazy var lblName: UILabel  =  {
        let label = UILabel()
        label.font = UIFont.routePlanningTitleBoldFont()
        label.textColor = UIColor.routePlanningTitleColor
        self.addSubview(label)
        return label
    }()
    
    private lazy var lblAddress: UILabel = {
        let label = UILabel()
        label.font = UIFont.routePlanningAddressFont()
        label.textColor = UIColor.routePlanningAddressColor
        self.addSubview(label)
        return label
    }()
    
    private lazy var addBtn: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "plusIcon"), for: .normal)
        self.addSubview(button)
        button.addTarget(self, action: #selector(onAdd(_:)), for: .touchUpInside)
        return button
    }()
    
    var representedObject: PointAnnotation!
    var address: RoutePlanningViewModel.RPAddress! // passed from RoutePlanning screen

    // https://github.com/mapbox/mapbox-gl-native/issues/9228
    override var center: CGPoint {
        set {
            var newCenter = newValue
            newCenter.y -= bounds.midY
            super.center = newCenter
        }
        get {
            return super.center
        }
    }

    required init(representedObject: PointAnnotation, address: RoutePlanningViewModel.RPAddress) {
        self.representedObject = representedObject
        self.address = address
        super.init(frame: .zero)
                
        lblName.text = address.name
        lblAddress.text = address.address1
        
        self.frame = CGRect(x: 0, y: 0, width: 240, height: 50)
        self.backgroundColor = .white
        
        addBtn.setImage(address.isEasyBreezy ? UIImage(named: "penIcon") : UIImage(named: "plusIcon"), for: .normal)
        setupLayout()
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = bounds.height / 2
        layer.masksToBounds = true
        showShadow()
    }
    
    override func sizeThatFits(_ size: CGSize) -> CGSize {
                
        let lblNameSize = lblName.intrinsicContentSize
        let lblAddrSize = lblAddress.intrinsicContentSize
        
        let w = min(max(lblNameSize.width, lblAddrSize.width) + iconSize + padding*2 + gap, UIScreen.main.bounds.width / 2)
        let h = lblNameSize.height + 2 + lblAddrSize.height + padding
        
        return CGSize(width: w, height: h)
    }
    
    @objc func onAdd(_ sender: Any) {

        let place = SavedPlaces(placeName: address.address, address: address.address1, lat: "\(address.lat)", long: "\(address.long)", time: address.date)
                
        let ebName = address.name
        let addressID = Int(address.id) ?? 0

        delegate?.onAddEditEasyBreezy(place: place, addressId: addressID, ebName: ebName)
    }
        
    private func setupLayout() {
        lblName.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(yPadding)
            make.leading.equalToSuperview().offset(padding)
            make.trailing.equalTo(addBtn.snp.leading).offset(gap)
        }
        
        lblAddress.snp.makeConstraints { (make) in
            make.bottom.equalToSuperview().offset(-yPadding)
            make.leading.equalToSuperview().offset(padding)
            make.top.equalTo(lblName.snp.bottom).offset(gap)
            make.trailing.equalTo(addBtn.snp.leading).offset(gap)
        }
        
        addBtn.snp.makeConstraints { (make) in
            make.trailing.equalToSuperview().offset(-padding)
            make.height.equalTo(iconSize)
            make.width.equalTo(iconSize)
            make.centerY.equalToSuperview()
        }
    }
    
    // MARK: - MGLCalloutView API
    func presentCallout(from rect: CGRect, in view: UIView, constrainedTo constrainedRect: CGRect, animated: Bool) {

        //delegate?.calloutViewWillAppear?(self)

        
        view.addSubview(self)
        self.sizeToFit()

        if animated {
            alpha = 0

            UIView.animate(withDuration: 0.2) { [weak self] in
                guard let strongSelf = self else {
                    return
                }

                strongSelf.alpha = 1
                //strongSelf.delegate?.calloutViewDidAppear?(strongSelf)
            }
        } else {
            //delegate?.calloutViewDidAppear?(self)
        }
    }

    func dismissCallout(animated: Bool) {
        if (superview != nil) {
            if animated {
                UIView.animate(withDuration: 0.2, animations: { [weak self] in
                    guard let self = self else { return }
                    self.alpha = 0
                }, completion: { [weak self] _ in
                    guard let self = self else { return }
                    self.removeFromSuperview()
                })
            } else {
                removeFromSuperview()
            }
        }
    }

    // MARK: - Callout interaction handlers

    func isCalloutTappable() -> Bool {
//        if let delegate = delegate {
//            if delegate.responds(to: #selector(MGLCalloutViewDelegate.calloutViewShouldHighlight)) {
//                return delegate.calloutViewShouldHighlight!(self)
//            }
//        }
        return true
    }

    @objc func calloutTapped() {
//        if isCalloutTappable() && delegate!.responds(to: #selector(MGLCalloutViewDelegate.calloutViewTapped)) {
//            delegate!.calloutViewTapped!(self)
//        }
    }
}

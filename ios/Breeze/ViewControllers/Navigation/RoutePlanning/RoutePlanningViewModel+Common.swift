//
//  RoutePlanningViewModel+Common.swift
//  Breeze
//
//  Created by VishnuKanth on 12/10/21.
//

import Foundation
import Turf
import MapboxNavigation
import MapboxCoreNavigation
import MapboxMaps
import MapboxCoreMaps
import MapboxDirections
import SwiftyBeaver
import RxSwift
import CoreLocation

extension RoutePlanningViewModel {
    
    // MARK: - Private Methods
   func loadData() {
        DataCenter.shared.addERPUpdaterListner(listner: self)
       
    }
    
    func getRouteDistance(route:Route) -> (String,String){
        
        if (route.distance) > 1000 {
            return ("\(String(format: "%.1f", (route.distance)/1000))", " km")
        } else {
            let roundedDistance = route.distance.rounded()
            return ("\(Int(roundedDistance))", " m")
        }
    }
    
    func getRouteDuration(route:Route) -> (String,String){
        
        return ((route.expectedTravelTime.stringTime), " \((route.expectedTravelTime.stringTimeUnit))")
    }
    
    func getRouteDurationFromTimeInterval(expectedTravelTime:TimeInterval) -> (String,String){
        
        return ((expectedTravelTime.stringTime), " \((expectedTravelTime.stringTimeUnit))")
    }
    
    
    func getArrivalTimeFromTimeInterval(expectedTravelTime:TimeInterval) -> (String,String){
        
        let estimatedTime = estimatedArrivalTime((expectedTravelTime))
        return (estimatedTime.time, estimatedTime.format)
    }
    
    func getArrivalTime(route:Route) -> (String,String){
        
        let estimatedTime = estimatedArrivalTime((route.expectedTravelTime))
        SwiftyBeaver.debug("expectedTravelTime start \(route.expectedTravelTime)")
        
        return (estimatedTime.time, estimatedTime.format)
    }
/*
    private func estimatedArrivalTime(_ duration: TimeInterval) -> (time: String,format: String) {
        let arrivalDate = Date(timeInterval: duration, since: Date())
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: arrivalDate)
//        let t = hour > 12 ? hour - 12: hour
//        let hourString = t < 10 ? "0\(t)" : t.description
        let minute = calendar.component(.minute, from: arrivalDate)
        let minString = minute < 10 ? "0\(minute)" : minute.description
//        return (time: "\(hourString):\(minString)", format: hour >= 12 ? " pm" : " am")
        // for the time being all the date time format is in 24 hours format
        return (time: "\(hour):\(minString)", format: "")
    }
*/
    func setupRouteForCarPlay(fromMobile:Bool = true){
        
        if(fromMobile){
            self.isCarPlay = false
        }
        
        let coordinate = LocationManager.shared.location.coordinate
                guard let address = self.carPlayDestAddress else {
                    return
                }
                /*guard let address = viewModel.address, let coordinate = self.navView.mapView.location.latestLocation?.coordinate else {
                    return
                }*/
                
                print("source carplay", coordinate)
                print("destination carplay",address)
                // draw routes
        //        activityIndicator.startAnimating()
                
                let origin = Waypoint(coordinate: coordinate, coordinateAccuracy: -1, name: "Start")
                let destination = Waypoint(coordinate: CLLocationCoordinate2D(latitude: address.lat, longitude: address.long), coordinateAccuracy: -1, name: address.address)

                calculateRoute(wayPoints: [origin, destination])
                
                // not call selectAddress api if it's a calendar address
                if (self.baseAddress as? CalendarAddress) != nil {
                    return
                }

                //Selection Address API
                let location = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
                getAddressFromLocation(location: location) { (reverseaddress) in
                    var selectAddressDetail =  [String:Any]()
                    selectAddressDetail = ["startLatitude":coordinate.latitude.toString(),"startLongtitude":coordinate.longitude.toString(),"startAddress1":reverseaddress,"destLatitude":address.lat.toString(), "destLongtitude":address.long.toString(), "destAddress1":address.address,"destAddress2":address.address1, "ranking":self.rankingIndex,"addressid":self.addressId, "source":self.selectionSource]

                    SwiftyBeaver.debug("select Address Detail carplay \(selectAddressDetail)")
                    
                    self.selectAddress(address: selectAddressDetail)
                }
    }
    
    func setupRoute(fromMobile:Bool = true) {
        
        if(fromMobile){
            self.isCarPlay = false
        }
        var coordinate:CLLocationCoordinate2D?
        if let currentAddress = self.currentAddress {
            
            coordinate = CLLocationCoordinate2D(latitude: currentAddress.lat, longitude: currentAddress.long)
            
        } else {
            
             coordinate = LocationManager.shared.location.coordinate
        }
        
        guard let address = self.address else {
            return
        }
        
        guard let coordinate = coordinate else {
            
            return
        }
        
        print("source", coordinate)
        print("destination",address)
        // draw routes
//        activityIndicator.startAnimating()
        
        let origin = Waypoint(coordinate: coordinate, coordinateAccuracy: -1, name: "Start")
        let destination = Waypoint(coordinate: CLLocationCoordinate2D(latitude: address.lat, longitude: address.long), coordinateAccuracy: -1, name: address.address)
        
    #if DEMO
        
        if(DemoSession.shared.status == .none || DemoSession.shared.status == .inFirstLeg)
        {
            DemoSession.shared.findUserInLeg1OrLeg2(start: origin.coordinate, destination: destination.coordinate)
            
            if(DemoSession.shared.status == .inFirstLeg){
                DemoSession.shared.startFirstLeg(start: origin.coordinate, destination: destination.coordinate)
            }
            else{
                DemoSession.shared.startSecondLeg(start: origin.coordinate, destination: destination.coordinate)
            }
            
            
            var waypoints = [Waypoint]()
            
            if(DemoSession.shared.isDemoRoute)
            {
                let originWayPoint = Waypoint(coordinate: CLLocationCoordinate2D(latitude: (DemoSession.shared.config?.start.latitude)!, longitude: (DemoSession.shared.config?.start.longitude)!), coordinateAccuracy: -1, name: DemoSession.shared.config?.start.name)
                originWayPoint.separatesLegs = Values.wayPointSeparateLegs
                waypoints.append(originWayPoint)
                
                for demoWayPoint in DemoSession.shared.config!.waypoints{
                    let wayPoint = Waypoint(coordinate: CLLocationCoordinate2D(latitude: demoWayPoint.latitude, longitude: demoWayPoint.longitude), coordinateAccuracy: -1, name: demoWayPoint.name)
                    wayPoint.separatesLegs = Values.wayPointSeparateLegs
                    waypoints.append(wayPoint)
                }
                
                let destinationWayPoint = Waypoint(coordinate: CLLocationCoordinate2D(latitude: (DemoSession.shared.config?.destination.latitude)!, longitude: (DemoSession.shared.config?.destination.longitude)!), coordinateAccuracy: -1, name: DemoSession.shared.config?.destination.name)
                destinationWayPoint.separatesLegs = Values.wayPointSeparateLegs
                waypoints.append(destinationWayPoint)
                calculateRoute(wayPoints: waypoints)
            }
            else
            {
                calculateRoute(wayPoints: [origin, destination])
            }
        }
        else
        {
            if(DemoSession.shared.status == .firstLegComplete || DemoSession.shared.status == .inSecondLeg)
            {
                DemoSession.shared.startSecondLeg(start: origin.coordinate, destination: destination.coordinate)
                
                var waypoints = [Waypoint]()
                
                if(DemoSession.shared.isDemoRoute)
                {
                    let originWayPoint = Waypoint(coordinate: CLLocationCoordinate2D(latitude: (DemoSession.shared.config?.start.latitude)!, longitude: (DemoSession.shared.config?.start.longitude)!), coordinateAccuracy: -1, name: DemoSession.shared.config?.start.name)
                    originWayPoint.separatesLegs = Values.wayPointSeparateLegs
                    waypoints.append(originWayPoint)
                    
                    for demoWayPoint in DemoSession.shared.config!.waypoints{
                        let wayPoint = Waypoint(coordinate: CLLocationCoordinate2D(latitude: demoWayPoint.latitude, longitude: demoWayPoint.longitude), coordinateAccuracy: -1, name: demoWayPoint.name)
                        wayPoint.separatesLegs = Values.wayPointSeparateLegs
                        waypoints.append(wayPoint)
                    }
                    
                    let destinationWayPoint = Waypoint(coordinate: CLLocationCoordinate2D(latitude: (DemoSession.shared.config?.destination.latitude)!, longitude: (DemoSession.shared.config?.destination.longitude)!), coordinateAccuracy: -1, name: DemoSession.shared.config?.destination.name)
                    destinationWayPoint.separatesLegs = Values.wayPointSeparateLegs
                    waypoints.append(destinationWayPoint)
                    calculateRoute(wayPoints: waypoints)
                }
                else
                {
                    if(wayPointsAdded.count > 0){
                        
                        wayPointsAdded.insert(origin, at: 0) //adding origin at index 0
                        wayPointsAdded.append(destination)
                    }
                    else{
                        wayPointsAdded.append(origin)
                        wayPointsAdded.append(destination)
                    }
                    calculateRoute(wayPoints: wayPointsAdded)
                }
            }
            
        }
        
        #else
       
        if(wayPointsAdded.count > 0){
            
            wayPointsAdded.insert(origin, at: 0) //adding origin at index 0
            wayPointsAdded.append(destination)
        }
        else{
            wayPointsAdded.append(origin)
            wayPointsAdded.append(destination)
        }

        #endif
        
        // not call selectAddress api if it's a calendar address
        if (self.baseAddress as? CalendarAddress) != nil {
            return
        }

        //Selection Address API
        if self.currentBaseAddress != nil {
            self.calculateRoute(wayPoints: self.wayPointsAdded)
            
            print("select Address Detail %@",self.address.lat,self.address.long,self.address.address)
        }
        else{
            
            let location = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
            getAddressFromLocation(location: location) { (reverseaddress) in
                
                if self.currentBaseAddress != nil{
                }
                else{
                    
                    let address = SearchAddresses(alias: "", address1: reverseaddress, lat: "\(LocationManager.shared.location.coordinate.latitude)", long: "\(LocationManager.shared.location.coordinate.longitude)", address2: reverseaddress, distance: "")
                    
                    self.currentBaseAddress = address
                    self.setRPCurrentAddress()
                    self.calculateRoute(wayPoints: self.wayPointsAdded)
                }
                
                var selectAddressDetail =  [String:Any]()
                selectAddressDetail = ["startLatitude":coordinate.latitude.toString(),"startLongtitude":coordinate.longitude.toString(),"startAddress1":reverseaddress,"destLatitude":address.lat.toString(), "destLongtitude":address.long.toString(), "destAddress1":address.address,"destAddress2":address.address1, "ranking":self.rankingIndex,"addressid":self.addressId, "source":self.selectionSource]

                print("select Address Detail %@",selectAddressDetail)
                
                self.selectAddress(address: selectAddressDetail)
            }
        }
        
    }
    
    
    func selectAddress(address:[String:Any]){
        
        DispatchQueue.global(qos: .background).async {
            
            SelectAddressService.shared.selectAddress(address: address) { (result) in
                print(result)
                switch result {
                case .success(_):
                    print("successSelectAddress")
                    
                case .failure(_):
                    DispatchQueue.main.async {
                        print("failedSelectAddress")
                    }
                }
            }
        }
    }
    
    func setRouteOptions(routeType:String,waypoints:[Waypoint], identifier: ProfileIdentifier = .automobileAvoidingTraffic) -> NavigationRouteOptions{
        
        var routeOptions:NavigationRouteOptions?
        if(routeType == Constants.fast)
        {
            
            if(departTime != "" || arrivalTime != ""){
                
                if(self.isCarPlay){
                    
                    routeOptions =  NavigationRouteOptions(waypoints: waypoints, profileIdentifier: identifier)
                }
                else{
                    
                    routeOptions = MapedRouteOptions(waypoints: waypoints, departTime: departTime, arriveTime: arrivalTime, identifier: identifier)
                    SwiftyBeaver.debug("calling Directions API with MapedRouteOptions for first call \(String(describing: routeOptions))")
                }
                
            }
            else{
                
                routeOptions =  NavigationRouteOptions(waypoints: waypoints, profileIdentifier: identifier)
            }
            
            
        }
        
        if(routeType == Constants.cheap){
            
            if(departTime != "" || arrivalTime != ""){
                
                if(self.isCarPlay){
                    
                    routeOptions =  NavigationRouteOptions(waypoints: waypoints, profileIdentifier: identifier)
                }
                else{
                    
                    routeOptions = MapedRouteOptions(waypoints: waypoints, departTime: departTime, arriveTime: arrivalTime, identifier: identifier)
                    SwiftyBeaver.debug("calling Directions API with MapedRouteOptions for second call \(String(describing: routeOptions))")
                }
                
            }
            else{
                
                routeOptions =  NavigationRouteOptions(waypoints: waypoints, profileIdentifier: identifier)
            }
            
            routeOptions?.roadClassesToAvoid = [.toll] //This will give the route avoiding the toll
        }
        
        if(routeType == Constants.short){
            
            routeOptions = NavigationRouteOptions(waypoints: waypoints, profileIdentifier: .automobile)
            
        }
        // This will also be passed to TurnByTurnNavigationViewController
        routeOptions?.includesSteps = true
        routeOptions?.attributeOptions = [.congestionLevel, .expectedTravelTime, .maximumSpeedLimit]
        routeOptions?.shapeFormat = .polyline6
        routeOptions?.locale = Locale.enSGLocale() //Setting this local to announce voice instructions that are more suitable to Singapore.
        routeOptions?.distanceMeasurementSystem = .metric // This setting is used by voice instruction only

        NavigationSettings.shared.distanceUnit = .kilometer
        return routeOptions!
    }
    
    // Add a callback here
    func returnDirectionsAPIResponse(routeOptions:NavigationRouteOptions,routeType:String, completion: ((_ response: RouteResponse?,_ routeType:String) -> Void)?) {
        
        SwiftyBeaver.debug("returnDirectionsAPIResponse start")
        // Generate the route object and draw it on the map
        Directions.shared.calculate(routeOptions) { (session, result) in
          switch result {
            case .failure(let error):
                print(error.localizedDescription)
              // no route found
              completion?(nil, "No route found")
              
            case .success(let response):
              
              if let callback = completion {
                  callback(response,routeType)
              }
          }
        }
        
    }
    
    func secondCallCalculateRoute(wayPoints: [Waypoint]) {
        calculateRoute(wayPoints: wayPoints, identifier: .automobile)
    }
    
    func calculateRoute(wayPoints: [Waypoint], identifier: ProfileIdentifier = .automobileAvoidingTraffic, isSecondCall: Bool = false) {
        guard wayPoints.count >= 2 else { return }
        
        // Specify that the route is intended for automobiles avoiding traffic
        let routeOptions = setRouteOptions(routeType: Constants.fast, waypoints: wayPoints, identifier: identifier)
        let cheapRouteOptions = setRouteOptions(routeType: Constants.cheap, waypoints: wayPoints, identifier: identifier)

        //let shortRouteOptions = setRouteOptions(routeType: Constants.short, waypoints: [origin,destination])
        
        // Get periodic updates regarding changes in estimated arrival time and traffic congestion segments along the route line.
        RouteControllerProactiveReroutingInterval = 30
        /*
        self.returnDirectionsAPIResponse(routeOptions: routeOptions,routeType: Constants.fast) { [weak self] response, routeType in

            guard let self = self else { return }

            self.configureResponse(response, routeType)

            self.returnDirectionsAPIResponse(routeOptions: cheapRouteOptions,routeType: Constants.cheap) { [weak self] response, routeType in

                guard let secondSelf = self else { return }

                secondSelf.configureResponse(response, routeType)
            }
        }
         */
        
        var currentRouteResponse: [String: RouteResponse] = [:]
        var noRouteFound = false
        
        let dispatchGroupResponse = DispatchGroup()
        
        dispatchGroupResponse.enter()
        self.returnDirectionsAPIResponse(routeOptions: routeOptions,routeType: Constants.fast) { response, routeType in
            if response != nil {
                currentRouteResponse[Constants.fast] = response
            } else {
                noRouteFound = true
            }
            dispatchGroupResponse.leave()
        }
        
        dispatchGroupResponse.enter()
        self.returnDirectionsAPIResponse(routeOptions: cheapRouteOptions,routeType: Constants.cheap) { response, routeType in
            if response != nil {
                currentRouteResponse[Constants.cheap] = response
            } else {
                noRouteFound = true
            }
            dispatchGroupResponse.leave()
        }
        
        dispatchGroupResponse.notify(queue: .main) { [weak self] in
            guard let self = self else { return }
            if noRouteFound {
                //  Only handle show not found route if identifier is driving
                if identifier == .automobileAvoidingTraffic {
                    self.secondCallCalculateRoute(wayPoints: wayPoints)
                } else if identifier == .automobile {
                    
                    if let origin = wayPoints.first {
                        AnalyticsManager.shared.sendLocationData(eventValue: ParameterName.RoutePlanningMode.LocationData.no_drivable_route_origin, screenName: ParameterName.RoutePlanningMode.screen_view, locationName: origin.name ?? "", latitude: origin.coordinate.latitude.toString(), longitude: origin.coordinate.longitude.toString())
                    }
                    if let destination = wayPoints.last {
                        AnalyticsManager.shared.sendLocationData(eventValue: ParameterName.RoutePlanningMode.LocationData.no_drivable_route_destination, screenName: ParameterName.RoutePlanningMode.screen_view, locationName: destination.name ?? "", latitude: destination.coordinate.latitude.toString(), longitude: destination.coordinate.longitude.toString())
                    }
                    self.routeNotFound = true
                    appDelegate().showNotFoundRouteLocation(title: Constants.ncsNotFoundRouteLocation)
                }
            } else {
                self.routeNotFound = false
                self.configureFullRouteResponse(currentRouteResponse)
            }
        }
/*
        self.callBackResponse = { [weak self]  response,type in
            guard let self = self else { return }
            
            if(type == Constants.fast)
            {
                SwiftyBeaver.debug("Fastest Route")
                if let mapViewUpdatable = self.mobileRPMapViewUpdatable{
                    mapViewUpdatable.defaultResponse = response
                }
                
                if let cpMapViewUpdatable = self.carPlayMapViewUpdatable{
                    cpMapViewUpdatable.defaultResponse = response
                }
                //self.defaultResponse = response
                
                if let mapViewUpdatable = self.mobileRPMapViewUpdatable{
                    
                    if(mapViewUpdatable.cheapestResponse != nil){
                        self.getAllFilteredRoutes()
                    }
                }
                else if let  mapViewUpdatable = self.carPlayMapViewUpdatable{
                    
                    if(mapViewUpdatable.cheapestResponse != nil){
                        self.getAllFilteredRoutes()
                    }
                }
                
            }
            
            if(type == Constants.cheap){
                
                SwiftyBeaver.debug("Cheapest Route")
                if let mapViewUpdatable = self.mobileRPMapViewUpdatable{
                    mapViewUpdatable.cheapestResponse = response
                }
                
                if let cpMapViewUpdatable = self.carPlayMapViewUpdatable{
                    cpMapViewUpdatable.cheapestResponse = response
                }
                
                if let mapViewUpdatable = self.mobileRPMapViewUpdatable {
                    mapViewUpdatable.initERPUpdater(response: mapViewUpdatable.defaultResponse)
                }

                //MARK: To DO Init ERPCal for Car Play RoutePlanning
                if let mapViewUpdatable = self.carPlayMapViewUpdatable {
                    mapViewUpdatable.initERPUpdater(response: mapViewUpdatable.defaultResponse)
                }
                
                if let mapViewUpdatable = self.mobileRPMapViewUpdatable{
                    
                    if(mapViewUpdatable.defaultResponse != nil){
                        self.getAllFilteredRoutes()
                    }
                }
                else if let  mapViewUpdatable = self.carPlayMapViewUpdatable{
                    
                    if(mapViewUpdatable.defaultResponse != nil){
                        self.getAllFilteredRoutes()
                    }
                }
                
                
            }
            
        }
*/
    }
    
    private func configureFullRouteResponse(_ currentRouteResponse: [String: RouteResponse]) {
        if let cheapestResponse = currentRouteResponse[Constants.cheap],
           let defaultResponse = currentRouteResponse[Constants.fast] {
            if self.isCarPlay {
                if let cpMapViewUpdatable = self.carPlayMapViewUpdatable {
                    cpMapViewUpdatable.defaultResponse = defaultResponse
                    cpMapViewUpdatable.cheapestResponse = cheapestResponse
                    
                    //Adding original Fastest response to another array to use it in later part when user selects the route to match the routeIndex
                    cpMapViewUpdatable.originalResponse.append(defaultResponse)
                    cpMapViewUpdatable.originalResponse.append(cheapestResponse)
                    
                    if let erpUpdater = cpMapViewUpdatable.erpPriceUpdater{
                        
                        erpUpdater.response = cpMapViewUpdatable.defaultResponse
                    } else {
                        cpMapViewUpdatable.initERPUpdater(response: cpMapViewUpdatable.defaultResponse)
                    }
                }
            } else {
                if let mapViewUpdatable = self.mobileRPMapViewUpdatable {
                    mapViewUpdatable.defaultResponse = defaultResponse
                    mapViewUpdatable.cheapestResponse = cheapestResponse
                    
                    //Adding original Fastest response to another array to use it in later part when user selects the route to match the routeIndex
                    mapViewUpdatable.originalResponse.append(defaultResponse)
                    mapViewUpdatable.originalResponse.append(cheapestResponse)
                    
                    if let erpUpdater = mapViewUpdatable.erpPriceUpdater {
                        erpUpdater.response = mapViewUpdatable.defaultResponse
                    } else {
                        mapViewUpdatable.initERPUpdater(response: mapViewUpdatable.defaultResponse)
                        
                        if let currentERPTime = self.currentERPTime {
                            mapViewUpdatable.erpPriceUpdater.currentERPTime = currentERPTime
                        }
                    }
                }
            }
            self.getAllFilteredRoutes()
        }
    }
    
    /*
    private func configureResponse(_ response: RouteResponse,_ type:String) {
        if(type == Constants.fast)
        {
            SwiftyBeaver.debug("Fastest Route")
            if(self.isCarPlay){
                
                if let cpMapViewUpdatable = self.carPlayMapViewUpdatable {
                    cpMapViewUpdatable.defaultResponse = response
                    
                    //Adding original Fastest response to another array to use it in later part when user selects the route to match the routeIndex
                    cpMapViewUpdatable.originalResponse.append(response)
                }
            }
            else{
                
                if let mapViewUpdatable = self.mobileRPMapViewUpdatable{
                    mapViewUpdatable.defaultResponse = response
                    
                    //Adding original Fastest response to another array to use it in later part when user selects the route to match the routeIndex
                    mapViewUpdatable.originalResponse.append(response)
                }
                
                
            }
            
            if(self.isCarPlay){
                
                if let  mapViewUpdatable = self.carPlayMapViewUpdatable{
                    
                    if(mapViewUpdatable.cheapestResponse != nil){
                        self.getAllFilteredRoutes()
                    }
                }
            }
            else{
                
                if let mapViewUpdatable = self.mobileRPMapViewUpdatable{
                    
                    if(mapViewUpdatable.cheapestResponse != nil){
                        SwiftyBeaver.debug("Calling Filtered routes")
                        self.getAllFilteredRoutes()
                    }
                }
                
                
            }
            
        }
        
        if(type == Constants.cheap){
            
            SwiftyBeaver.debug("Cheapest Route")
            
            if(self.isCarPlay){
                
                if let cpMapViewUpdatable = self.carPlayMapViewUpdatable{
                    cpMapViewUpdatable.cheapestResponse = response
                    
                    //Adding original Cheapest response to another array to use it in later part when user selects the route to match the routeIndex
                    cpMapViewUpdatable.originalResponse.append(response)
                    
                }
            }
            else{
                
                if let mapViewUpdatable = self.mobileRPMapViewUpdatable{
                    mapViewUpdatable.cheapestResponse = response
                    
                    //Adding original Cheapest response to another array to use it in later part when user selects the route to match the routeIndex
                    mapViewUpdatable.originalResponse.append(response)
                }
                
                
                
            }
            
            if(self.isCarPlay){
                
                //MARK: To DO Init ERPCal for Car Play RoutePlanning
                if let mapViewUpdatable = self.carPlayMapViewUpdatable {
                    if let erpUpdater = mapViewUpdatable.erpPriceUpdater{
                        
                        erpUpdater.response = mapViewUpdatable.defaultResponse
                    }
                    else{
                        
                        mapViewUpdatable.initERPUpdater(response: mapViewUpdatable.defaultResponse)
                    }
                }
                
                if let  mapViewUpdatable = self.carPlayMapViewUpdatable{
                    
                    if(mapViewUpdatable.defaultResponse != nil){
                        self.getAllFilteredRoutes()
                    }
                }
            }
            else{
                
                if let mapViewUpdatable = self.mobileRPMapViewUpdatable {
                    
                    if let erpUpdater = mapViewUpdatable.erpPriceUpdater{
                        
                        erpUpdater.response = mapViewUpdatable.defaultResponse
                    }
                    else{
                        
                        mapViewUpdatable.initERPUpdater(response: mapViewUpdatable.defaultResponse)
                        
                        if let currentERPTime = self.currentERPTime {
                            
                            mapViewUpdatable.erpPriceUpdater.currentERPTime = currentERPTime
                        }
                        
                    }
                    
                }
                
                if let mapViewUpdatable = self.mobileRPMapViewUpdatable{
                    
                    if(mapViewUpdatable.defaultResponse != nil){
                        SwiftyBeaver.debug("Calling Filtered routes after cheapest")
                        self.getAllFilteredRoutes()
                    }
                }
            }

        }
    }
    */
    
    func getAllFilteredRoutes(){
        /*
         Call calculation route and assign to mapUptable, apter calculate success we chose best routes
         After that we call update map
         1. fastest
         2. Shortest
         3. Cheapest
         We must call on same thread with chose best route
         */
        DispatchQueue.global(qos: .default).async {
            self.filterFastestRoute()
            self.filterShortestRoute()
            self.filterCheapestRoute()
            
            if(!self.isCarPlay){
                self.chooseBestOfRoutes()
            }
            
            if(self.isCarPlay){
                
                if let mapViewUpdatable = self.carPlayMapViewUpdatable{
                    
                    mapViewUpdatable.updateMap = true
                }
            }
            else{
                
                if let mapViewUpdatable = self.mobileRPMapViewUpdatable{
                    
                    mapViewUpdatable.updateMap = true
                }
                
            }
            
            
        }
    }
    
    private func filterFastestRoute() {
        if(self.isCarPlay){
            if let cpMapViewUpdatable = self.carPlayMapViewUpdatable{
                cpMapViewUpdatable.response = cpMapViewUpdatable.defaultResponse
            }
        }
        else{
            if let mapViewUpdatable = self.mobileRPMapViewUpdatable{
                mapViewUpdatable.response = mapViewUpdatable.defaultResponse
            }
        }
    }
    
    func setUpRouteColors(isDarkMode:Bool){
        
        if(self.isCarPlay){
            
            //MARK: To DO for CarPlay RoutePlanning
            if let mapViewUpdatable = self.carPlayMapViewUpdatable {
                mapViewUpdatable.setRouteLineColors(isDarkMode: isDarkMode)
            }
        }
        else{
            if let mapViewUpdatable = self.mobileRPMapViewUpdatable {
                mapViewUpdatable.setRouteLineColors(isDarkMode: isDarkMode)
            }
            
            
        }
        
    }
    
    private func filterShortestRoute(){
        
        if(self.isCarPlay){
            if let mapViewUpdatable = self.carPlayMapViewUpdatable,
            let defaultResponse = mapViewUpdatable.defaultResponse {
                if let updatedRouteResponse = mapViewUpdatable.erpPriceUpdater?.findShortest(responseFast: defaultResponse) {
                    mapViewUpdatable.shortestResponse = updatedRouteResponse
                }
            }
        }
        else{
            
            if let mapViewUpdatable = self.mobileRPMapViewUpdatable,
               let defaultResponse = mapViewUpdatable.defaultResponse {
                if let updatedRouteResponse = mapViewUpdatable.erpPriceUpdater?.findShortest(responseFast: defaultResponse) {
                    mapViewUpdatable.shortestResponse = updatedRouteResponse
                }
            }
        }
        
    }
    
    private func filterCheapestRoute(){
            
        if self.isCarPlay {
            if let mapViewUpdatable = self.carPlayMapViewUpdatable,
               let cheapestResponse = mapViewUpdatable.cheapestResponse,
               let defaultResponse = mapViewUpdatable.defaultResponse {
                let getActiveInactiveTolls = mapViewUpdatable.erpPriceUpdater.findActiveAndInActiveTollsFromFastRoute(erpTolls: DataCenter.shared.getAllERPFeatureCollection(), response: defaultResponse)
                
                if let inActiveToll = getActiveInactiveTolls.inActiveToll {
                    let findCommonRoutes = mapViewUpdatable.erpPriceUpdater.filterOutCommonRoutes(firstResponse: inActiveToll, secondResponse: cheapestResponse)
                    mapViewUpdatable.cheapestResponse = findCommonRoutes
                }
            }
            
        } else {
            if let mapViewUpdatable = self.mobileRPMapViewUpdatable,
               let cheapestResponse = mapViewUpdatable.cheapestResponse,
               let defaultResponse = mapViewUpdatable.defaultResponse {
                let getActiveInactiveTolls = mapViewUpdatable.erpPriceUpdater.findActiveAndInActiveTollsFromFastRoute(erpTolls: DataCenter.shared.getAllERPFeatureCollection(), response: defaultResponse)
                
                if let inActiveToll = getActiveInactiveTolls.inActiveToll {
                    let findCommonRoutes = mapViewUpdatable.erpPriceUpdater.filterOutCommonRoutes(firstResponse: inActiveToll, secondResponse: cheapestResponse)
                    mapViewUpdatable.cheapestResponse = findCommonRoutes
                }
            }
        }
    }
    
    private func chooseBestOfRoutes(){
        if let mapViewUpdatable = self.mobileRPMapViewUpdatable,
           let defaultResponse = mapViewUpdatable.defaultResponse,
           let shortestResponse = mapViewUpdatable.shortestResponse,
           let cheapestResponse = mapViewUpdatable.cheapestResponse {
            
            let bestRoutes = mapViewUpdatable.erpPriceUpdater.findBestOfRoutesForMobile(fastestResponse: defaultResponse, shortestResponse: shortestResponse, cheapestResponse: cheapestResponse)
            mapViewUpdatable.routePreferenceData = bestRoutes
        }
    }
    
    func displayRoutesFromERPDetails(response:RouteResponse,erpList:[RouteERPList]){
        
        if let mapViewUpdatable = self.mobileRPMapViewUpdatable {
            
            mapViewUpdatable.initERPUpdater(response: response)
            
            if let currentERPTime = self.currentERPTime {
                
                mapViewUpdatable.erpPriceUpdater.currentERPTime = currentERPTime
            }
                
            let bestRoutes = mapViewUpdatable.erpPriceUpdater.findRoutePreferencesForSingleRouteResponse(response: response, erpList: erpList)
            mapViewUpdatable.routePreferenceData = bestRoutes
            if let response = bestRoutes?.routeResponse {
                
                mapViewUpdatable.originalResponse.append(response)
                mapViewUpdatable.defaultResponse = response
            }
            
            mapViewUpdatable.updateMap = true
            
        }
        
    }
    
    
}

// MARK: - ERPUpdaterListener
extension RoutePlanningViewModel: ERPUpdaterListener {
    func shouldAddERPItems(noCostfeatures: FeatureCollection, costfeatures: FeatureCollection) {
        
        
//        if(self.isCarPlay){
            
            if let mapViewUpdatable = self.carPlayMapViewUpdatable {
                
                if(mapViewUpdatable.defaultResponse != nil)
                {
                    SwiftyBeaver.debug("Calling CarPlay UpdateMap")
                    mapViewUpdatable.updateMap = true
                }
            }
            
//        }
//        else{
            
            if let mapViewUpdatable = self.mobileRPMapViewUpdatable {
                
                if(mapViewUpdatable.defaultResponse != nil)
                {
                    if departTime == "" && arrivalTime == "" {
                        mapViewUpdatable.updateMap = true
                    }
                }
            }
            
//        }
            
//            if let mapViewUpdatable = self.mobileRPMapViewUpdatable {
//                
//                if(mapViewUpdatable.shortestResponse != nil)
//                {
//                    if departTime == "" && arrivalTime == "" {
//                        mapViewUpdatable.updateMap = true
//                    }
//                }
//            }
//            
//            if let mapViewUpdatable = self.carPlayMapViewUpdatable {
//                
//                if(mapViewUpdatable.shortestResponse != nil)
//                {
//                    mapViewUpdatable.updateMap = true
//                }
//            }
//            
//            
//        
//        
//            if let mapViewUpdatable = self.mobileRPMapViewUpdatable {
//                
//                if(mapViewUpdatable.cheapestResponse != nil)
//                {
//                    if departTime == "" && arrivalTime == "" {
//                        mapViewUpdatable.updateMap = true
//                    }
//                }
//            }
//            
//            if let mapViewUpdatable = self.carPlayMapViewUpdatable {
//                
//                if(mapViewUpdatable.cheapestResponse != nil)
//                {
//                    mapViewUpdatable.updateMap = true
//                }
//            }
            
            
        
    }
    
    /*
     Check destination in outer zone, add profile else remove profile
     */
    func addProfileZoneIfNeeded() {
        let origin = currentAddress != nil ? CLLocationCoordinate2D(latitude: currentAddress.lat, longitude: currentAddress.long) : LocationManager.shared.location.coordinate
        let destinationInZone = checkDestinationIsTBR(destinationLocation: CLLocationCoordinate2D(latitude: address.lat, longitude: address.long))
        let ignoreCongestionCheck = shouldIgnoreCongestionCheck(origin: origin, destinationLocation: CLLocationCoordinate2D(latitude: address.lat, longitude: address.long))
        showTBRCongestion = destinationInZone && SelectedProfiles.shared.isZoneCongestion() && !ignoreCongestionCheck
        
        if (destinationInZone) {
            // Add profile here
//            self.mobileRPMapViewUpdatable?.navigationMapView.mapView.addProfileZones()
            self.mobileRPMapViewUpdatable?.navigationMapView?.mapView.addProfileZonesDynamically(zoneColors: SelectedProfiles.shared.getZoneColors(), zoneDetails: SelectedProfiles.shared.getAllZones())
        } else {
            // Remove profile here
//            self.mobileRPMapViewUpdatable?.navigationMapView.mapView.removeProfileZones()
            self.mobileRPMapViewUpdatable?.navigationMapView?.mapView.removeProfileZonesDynamically()
        }
    }
    
    /*
     Check destination is in TBR zone or not
     */
    func checkDestinationIsTBR(destinationLocation: CLLocationCoordinate2D) -> Bool {
        guard let outerZoneCenter = SelectedProfiles.shared.getOuterZoneCentrePoint() else { return false}
        return identifyUserLocInOuterZone(centerCoordinate: outerZoneCenter, currentLocation: destinationLocation)
    }
    
    /*
     Ignore congestion alert if user inside the inner zone but destination is in outer zone
     */
    func shouldIgnoreCongestionCheck(origin: CLLocationCoordinate2D, destinationLocation: CLLocationCoordinate2D) -> Bool {
        
        let currentLocation = CLLocation(latitude: origin.latitude, longitude: origin.longitude)
        let destinationLocation = CLLocation(latitude: destinationLocation.latitude, longitude: destinationLocation.longitude)
        
        for zone in SelectedProfiles.shared.getAllZones(){

            let currentLocToZonedistance = currentLocation.distance(from: CLLocation(latitude: zone.centerPointLat.toDouble() ?? 0, longitude: zone.centerPointLong.toDouble() ?? 0))
            
            let destToZoneDistance = destinationLocation.distance(from: CLLocation(latitude: zone.centerPointLat.toDouble() ?? 0, longitude: zone.centerPointLong.toDouble() ?? 0))
            
            //That means user's current location is inside the zone and destination also inside the zone. It doesn't matter whether profile has 1 or more than 1 zone. we will return true and doesn't show the congestion
            
            //Defintion: Ignoring congestion alert if user's current location is outside the zone as well as destination location is inside the zone
            if currentLocToZonedistance >= Double(zone.radius) && destToZoneDistance <= Double(zone.radius){
                
                return false
            }
            
        }
        
        return true
    }
            
            
//        guard let outerZoneCenter = SelectedProfiles.shared.getOuterZoneCentrePoint() else { return true }
//        return identifyUserLocInInnerZone(centerCoordinate: outerZoneCenter, currentLocation: origin) && identifyUserLocInOuterZone(centerCoordinate: outerZoneCenter, currentLocation: destinationLocation)
    
        
}

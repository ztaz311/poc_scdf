//
//  RoutePlanningViewMap.swift
//  Breeze
//
//  Created by VishnuKanth on 12/10/21.
//

import Foundation
import CoreLocation
import Turf
import MapboxNavigation
import MapboxMaps
import MapboxDirections
import Combine
import SwiftyBeaver

final class RoutePlanningViewMap:MapViewUpdatable{
    func shareDriveButtonEnabled(_ isEnabled: Bool) {
        //No need to handle this
    }
    
    func updateETA(eta: TripETA?) {
        //No need to handle this
    }
    
    weak var navigationMapView: NavigationMapView?
    
    var delegate: MapViewUpdatableDelegate?
//    var routePlanningBottomVC = RoutePlanningBottomVC()
    var erpPriceUpdater:ERPPriceCal!
    var cheapestResponse:RouteResponse?
    var shortestResponse:RouteResponse?
    var defaultResponse:RouteResponse?
    var bestOfRouteResponse:RouteResponse?
    var routePreferenceData:RoutePreferenceData?
    var response:RouteResponse?
    var originalResponse = [RouteResponse]()
    @Published var updateMap = false
    init(navigationMapView: NavigationMapView) {
        self.navigationMapView = navigationMapView
    }
    
    deinit {
        SwiftyBeaver.debug("RoutePlanningViewMap deinit")
    }
    
    func updateParkingAvailability(carpark: Carpark?) {
        //No need to handle this
    }
    
    func updateNotificationWhenUserSoonArrive(carpark: Carpark?, message: String) {        
        //No need to handle this
    }
    
    func updateNotification(featureId: String?, currentFeature: FeatureDetectable?) {
        //No need to handle this
    }
    
    func updateTunnel(isInTunnel: Bool) {
        //No need to handle this
    }
    
    func cruiseStatusUpdate(isCruise: Bool) {
        //No need to handle this
    }
    
    func updateETA(eta: TripCruiseETA?) {
        //No need to handle this
    }
    
    func showETAMessage(recipient: String, message: String) {
        //No need to handle this
    }
    
    func updateRoadName(_ name: String) {
        //No need to handle this
    }
    
    func updateSpeedLimit(_ value: Double) {
        //No need to handle this
    }

    // Not used
    func overSpeed(value: Bool) {
        //No need to handle this
    }

    func initERPUpdater(response:RouteResponse?){
        if let mapview = self.navigationMapView?.mapView {
            self.erpPriceUpdater = ERPPriceCal(mapView: mapview, belowPuck: true,response: response!)
        }
    }
    
    func updateLiveLocationEnabled(_ isEnabled: Bool) {
        //No need to handle this
    }
}

extension MapViewUpdatable{
    
    func setRouteLineColors(isDarkMode:Bool){
        navigationMapView?.setupRouteColor(isDarkMode: isDarkMode)
    }
    
    func setRouteLineWidth(multipliedBy: Double = 1.4){
        
        RouteLineWidthByZoomLevel = [
            10.0: 4 * multipliedBy,
            13.0: 5 * multipliedBy,
            16.0: 8 * multipliedBy,
            19.0: 12 * multipliedBy,
            22.0: 15 * multipliedBy
        ]
    }

}

//
//  RoutePlanningViewModel.swift
//  Breeze
//
//  Created by Zhou Hao on 8/2/21.
//

import Foundation
import MapboxDirections
import Combine
import SwiftyBeaver
import Turf
import CoreLocation

final class RoutePlanningViewModel: NSObject {
    
    struct RPAddress {
        let id: String
        let name: String
        let address: String
        let address1:String
        let lat: Double
        let long: Double
        let date: String
        var isEasyBreezy: Bool = false
    }
    
    // MARK: property passed by caller
    var addressId: String = ""
    
    // MARK: - Internal used - Route Planning Address
    var address: RPAddress! //Destination
    var baseAddress:BaseAddress! //Mobile
    
    var currentAddress:RPAddress! //Mobile
    var currentBaseAddress:BaseAddress! //Mobile
    
    var carPlayBaseAddress:BaseAddress!
    var carPlayDestAddress:RPAddress!
    var isCarPlay = false

    //MARK: - for SelectAddress API
    var selectionSource = ""
   // var addressID = ""          // TODO: Remove it latter. Only use originalAddress
    var rankingIndex = 0
    var wayPointsAdded = [Waypoint]()
    var wayPointFeatures = [Turf.Feature]()
    var departTime = ""
    var arrivalTime = ""
    
    var departDate: Date?
    var arrivalDate: Date?
    
    var currentERPTime:Date?
    var callBackResponse: ((_ response: RouteResponse,_ routeType:String) -> Void)?
    
    var havingZoneColor = false
    
    @Published var showTBRCongestion = false
    @Published var routeNotFound = false
    
    // MARK: - Public properties
    var mobileRPMapViewUpdatable: RoutePlanningViewMap? {
        didSet {
            if oldValue == nil && mobileRPMapViewUpdatable != nil {
                syncUpdate(mapViewUpdatable: mobileRPMapViewUpdatable!)
            }
            
        }
    }
    var carPlayMapViewUpdatable: CarPlayRPMapViewUpdatable? {
        didSet {
            if oldValue == nil && carPlayMapViewUpdatable != nil {
                syncUpdate(mapViewUpdatable: carPlayMapViewUpdatable!)
            }
            
        }
    }
    
    //  MARK: - Update carpark when user pan
    var centerMapLocation: CLLocationCoordinate2D?
    
    //  MARK: - Add broadcast message
    var broadcastMessage: BroadcastMessageModel?
    
    var broadcastMessageRoutes: BroadCastModel?
    
    override init() {
        super.init()
        //Just an empty constructor
    }
    
    func setCarPlayRPAddress() {
        
        let strDate = DateUtils.shared.getTimeDisplay(dateFormat: Date.defaultFormat, date: Date())

        // TODO: Actually should consider we really need a RPAddress or not?
        if let addr = carPlayBaseAddress as? SearchAddresses, let latStr = addr.lat, let longStr = addr.long {
            carPlayDestAddress = RPAddress(id: addressId, name: addr.address1 ?? "", address: addr.address1 ?? "", address1: addr.address2 ?? "", lat: (latStr as NSString).doubleValue, long: (longStr as NSString).doubleValue, date: strDate)
        } else if let addr = carPlayBaseAddress as? EasyBreezyAddresses, let latStr = addr.lat, let longStr = addr.long {
            addressId = String("\(addr.addressid ?? 0)")
            carPlayDestAddress = RPAddress(id: addressId, name: addr.alias ?? "", address: addr.address1 ?? "",address1: addr.address2 ?? "", lat: (latStr as NSString).doubleValue, long: (longStr as NSString).doubleValue, date: strDate)
            address.isEasyBreezy = true
        } else if let addr = carPlayBaseAddress as? CalendarAddress, let latStr = addr.lat, let longStr = addr.long {
            carPlayDestAddress = RPAddress(id: addressId, name: addr.address1 ?? "", address: addr.address1 ?? "", address1: addr.address2 ?? "", lat: (latStr as NSString).doubleValue, long: (longStr as NSString).doubleValue, date: strDate)
            // TODO: If we need eventID?
        } else if let addr = carPlayBaseAddress as? Addresses, let latStr = addr.lat, let longStr = addr.long {
            carPlayDestAddress = RPAddress(id: String("\(addr.addressid)"), name: addr.address1 ?? "", address: addr.address1 ?? "", address1: addr.address2 ?? "", lat: (latStr as NSString).doubleValue, long: (longStr as NSString).doubleValue, date: strDate)
        }
    }
    
    func setRPAddress() {
        
        let strDate = DateUtils.shared.getTimeDisplay(dateFormat: Date.defaultFormat, date: Date())

        // TODO: Actually should consider we really need a RPAddress or not?
        if let addr = baseAddress as? SearchAddresses, let latStr = addr.lat, let longStr = addr.long {
            address = RPAddress(id: addressId, name: addr.address1 ?? "", address: addr.address1 ?? "", address1: addr.address2 ?? "", lat: (latStr as NSString).doubleValue, long: (longStr as NSString).doubleValue, date: strDate)
        } else if let addr = baseAddress as? EasyBreezyAddresses, let latStr = addr.lat, let longStr = addr.long {
            addressId = String("\(addr.addressid ?? 0)")
            address = RPAddress(id: addressId, name: addr.alias ?? "", address: addr.address1 ?? "",address1: addr.address2 ?? "", lat: (latStr as NSString).doubleValue, long: (longStr as NSString).doubleValue, date: strDate)
            address.isEasyBreezy = true
        } else if let addr = baseAddress as? CalendarAddress, let latStr = addr.lat, let longStr = addr.long {
            address = RPAddress(id: addressId, name: addr.address1 ?? "", address: addr.address1 ?? "", address1: addr.address2 ?? "", lat: (latStr as NSString).doubleValue, long: (longStr as NSString).doubleValue, date: strDate)
            // TODO: If we need eventID?
        } else if let addr = baseAddress as? Addresses, let latStr = addr.lat, let longStr = addr.long {
            address = RPAddress(id: String("\(addr.addressid)"), name: addr.address1 ?? "", address: addr.address1 ?? "", address1: addr.address2 ?? "", lat: (latStr as NSString).doubleValue, long: (longStr as NSString).doubleValue, date: strDate)
        }
    }
    
    func setRPCurrentAddress() {
        
        let strDate = DateUtils.shared.getTimeDisplay(dateFormat: Date.defaultFormat, date: Date())

        // TODO: Actually should consider we really need a RPAddress or not?
        if let addr = currentBaseAddress as? SearchAddresses, let latStr = addr.lat, let longStr = addr.long {
            currentAddress = RPAddress(id: addressId, name: addr.address1 ?? "", address: addr.address1 ?? "", address1: addr.address2 ?? "", lat: (latStr as NSString).doubleValue, long: (longStr as NSString).doubleValue, date: strDate)
        } else if let addr = currentBaseAddress as? EasyBreezyAddresses, let latStr = addr.lat, let longStr = addr.long {
            addressId = String("\(addr.addressid ?? 0)")
            currentAddress = RPAddress(id: addressId, name: addr.alias ?? "", address: addr.address1 ?? "",address1: addr.address2 ?? "", lat: (latStr as NSString).doubleValue, long: (longStr as NSString).doubleValue, date: strDate)
            currentAddress.isEasyBreezy = true
        } else if let addr = currentBaseAddress as? CalendarAddress, let latStr = addr.lat, let longStr = addr.long {
            currentAddress = RPAddress(id: addressId, name: addr.address1 ?? "", address: addr.address1 ?? "", address1: addr.address2 ?? "", lat: (latStr as NSString).doubleValue, long: (longStr as NSString).doubleValue, date: strDate)
            // TODO: If we need eventID?
        } else if let addr = currentBaseAddress as? Addresses, let latStr = addr.lat, let longStr = addr.long {
            currentAddress = RPAddress(id: String("\(addr.addressid)"), name: addr.address1 ?? "", address: addr.address1 ?? "", address1: addr.address2 ?? "", lat: (latStr as NSString).doubleValue, long: (longStr as NSString).doubleValue, date: strDate)
        }
    }
    
    //  Fetch broadcast message
    func fetchBroadcastMessage(_ carparkId: String?, mode: BroadcastMode, _ completion: @escaping (BroadcastMessageModel?) -> ()) {
        if let ID = carparkId, !ID.isEmpty {
            BroadcastMessageService().getBroadcastMessage(carparkId: ID, mode: .planning) { [weak self] result in
                switch result {
                case .success(let broadcast):
                    self?.broadcastMessage = broadcast
                    completion(broadcast)
                case .failure(let error):
                    self?.broadcastMessage = nil
                    completion(nil)
                    SwiftyBeaver.error("Failed to get collection: \(error.localizedDescription)")
                }
            }
        }else {
            self.broadcastMessage = nil
            completion(nil)
        }
    }
    
    // Get broadcast message on New Route
    func getBroadcastMessageRoutes(_ routes: [String: Any], _ completion: @escaping (BroadCastModel?) -> ()) {
        BroadcastMessageService().broadcastMessageRoutePlanning(routes: routes) { [weak self] result in
            switch result {
            case .success(let broadcastMessage):
                self?.broadcastMessageRoutes = broadcastMessage
                completion(broadcastMessage)
            case .failure(_):
                self?.broadcastMessageRoutes = nil
                completion(nil)
            }
        }
    }

    // TODO: We may need to update addressId and place in the future
    func update(name: String, isEasyBreezy: Bool = true) {
        var newAddress = RPAddress(id: addressId, name: name, address: address.address, address1: address.address1, lat: address.lat, long: address.long, date: address.date)
        newAddress.isEasyBreezy = isEasyBreezy
        address = newAddress
    }
    
    private func removedEasyBreezy() {
        addressId = ""
        update(name: address.address, isEasyBreezy: false )
    }
    
    func deleteEasyBreezy(completion: @escaping (Bool) -> Void) {
        guard !addressId.isEmpty else { return }
        
        DispatchQueue.global(qos: .background).async { [weak self] in
            
            guard let self = self else { return }
            
            let addressID =  ["addressid" : self.addressId]
            EasyBreezyService.shared.deleteEasyBreezyAddresses(address:[addressID as [String : Any]] as [[String : Any]]) { (result) in
                switch result {
                case .success(_):
                    for (index, ez) in SavedSearchManager.shared.easyBreezy.enumerated() {
                        if let id = ez.addressid {
                            if id == Int(self.addressId) ?? 0 {
                                SavedSearchManager.shared.easyBreezy.remove(at: index)
                            }
                        }
                    }
                    self.removedEasyBreezy()
                    DispatchQueue.main.async {
                        completion(true)
                    }
                case .failure(_):
                    DispatchQueue.main.async {
                        completion(false)
                    }
                }
            }
        }
    }
    
    func saveEasyBreezy(completion: @escaping (Result<String>) -> Void) {

        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.routeplanning_add_remove_fave, screenName: ParameterName.routeplanning_screen)
        
        
        if SavedSearchManager.shared.checkDuplicateEBName(name: address.name) {
            completion(.failure(EasyBreezyServiceError.duplicated))
            return
        }
        
        let easyBreezyDetail: [String : Any] = ["lat": "\(address.lat)","long":"\(address.long)","name":address.name,"address1":address.address,"address2":address.address1]

        DispatchQueue.global(qos: .background).async {
            
            EasyBreezyService.shared.saveEasyBreezyAddress(address: easyBreezyDetail) { [weak self] (result) in
                guard let self = self else { return }
                
                switch result {
                case .success(_):
                    self.getEasyBreezyList() { success in
                        if success {
                            // get the newly added address id
                            for easyBreezy in SavedSearchManager.shared.easyBreezy {
                                if easyBreezy.alias == self.address.name {
                                    self.addressId = "\(easyBreezy.addressid ?? 0)"
                                    self.update(name: easyBreezy.alias ?? "NA", isEasyBreezy: true)
                                    completion(.success(self.addressId))
                                    return
                                }
                            }
                        } else {
                            completion(.failure(EasyBreezyServiceError.failedToLoad))
                        }
                    }
                    
                case .failure(let err):
                    print(err)
                    DispatchQueue.main.async {
                        completion(.failure(EasyBreezyServiceError.failedToLoad))
                    }
                }
            }
        }
    }
    
    /*
     Get zone color once time
     if having zone color. Add profile to map
     else call api get zone color then add profile to map
     */
    func getZoneColors() {
        if (havingZoneColor) {
            addProfileZoneIfNeeded()
          return
        }
        guard let profileName = AmenitiesSharedInstance.shared.getDefaultProfileName() else { return }
        
//        let profileName = SelectedProfiles.shared.selectedProfileName()
            
        if(profileName != ""){
            
            AmenitiesSharedInstance.shared.amenitiesZoneDelegate = self
            AmenitiesSharedInstance.shared.callZoneColor(profileName: profileName)
        }
        
    }
    
    // TODO: consider move easy breezy to DataCenter as a DB item
    // update the easy breezy list
    private func getEasyBreezyList(completion: @escaping (Bool) -> Void){
        
        EasyBreezyService.shared.getSavedEasyBreezyAddresses() { (result) in
            switch result {
            case .success(let json):
                DispatchQueue.main.async {
                    SavedSearchManager.shared.easyBreezy = json
                    completion(true)
                }
            case .failure(_):
                DispatchQueue.main.async {
                    completion(false)
                }
            }
        }
    }
    
    /// Sync up UI based on the current status
    private func syncUpdate(mapViewUpdatable: MapViewUpdatable) {
        
        self.loadData()
    }
    
    deinit {
        SwiftyBeaver.debug("RoutePlanningViewModel deinit")
        DataCenter.shared.removeERPUpdaterListner(listner: self)
    }
    
//    private func getEasyBreezy(with aliase: String) -> EasyBreezyAddresses? {
//        
//    }
}

extension RoutePlanningViewModel: AmenitiesZoneDelegate {
    func didGetZoneColor() {
        havingZoneColor = true
        addProfileZoneIfNeeded()
    }
}

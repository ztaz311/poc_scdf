//
//  CarparkCalloutView.swift
//  Breeze
//
//  Created by Zhou Hao on 10/5/21.
//

import UIKit
import MapboxMaps
import SnapKit

final class CarparkCalloutView: UIView {
        
    // MARK: - Constants
    private let padding: CGFloat = 10
    private let yPadding: CGFloat = 8
    private let gap: CGFloat = 12 // gap between labels and button
    private let buttonWidth: CGFloat = 170.0
    private let buttonHeight: CGFloat = 24.0
    private let popupWidth: CGFloat = 190
    private let tipHeight: CGFloat = 8.0
    private let tipWidth: CGFloat = 20.0

    private lazy var lblName: UILabel  =  {
        let label = UILabel()
        label.font = UIFont.carparkTitleFont()
        label.preferredMaxLayoutWidth = popupWidth - 2 * padding
        label.textAlignment = .center
        label.numberOfLines = 3
        label.textColor = UIColor.carparkNameColor
        containerView.addSubview(label)
        return label
    }()

    private lazy var imgLotsInfo: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "parkingLotsIcon"))
        containerView.addSubview(imageView)
        return imageView
    }()
    
    private lazy var lblLotsLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.availableCarparkLotsLabelFont()
        label.textColor = UIColor.carparkLotsLabelColor
        label.textAlignment = .left
        label.text = "Avl lots"
        containerView.addSubview(label)
        return label
    }()

    private lazy var lblLotsInfo: UILabel = {
        let label = UILabel()
        label.font = UIFont.availableCarparkLotsFont()
        label.textColor = UIColor.carparkNameColor
        label.textAlignment = .right
        containerView.addSubview(label)
        return label
    }()

    private lazy var imgCost: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "parkingCostIcon"))
        containerView.addSubview(imageView)
        return imageView
    }()
    
    private lazy var lblCostLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.availableCarparkLotsLabelFont()
        label.textColor = UIColor.carparkLotsLabelColor
        label.textAlignment = .left
        label.text = "Now"
        containerView.addSubview(label)
        return label
    }()

    private lazy var lblCost: UILabel = {
        let label = UILabel()
        label.font = UIFont.availableCarparkLotsFont()
        label.textColor = UIColor.carparkNameColor
        label.textAlignment = .right
        containerView.addSubview(label)
        return label
    }()

    private lazy var lblNextHourCostLable: UILabel = {
        let label = UILabel()
        label.font = UIFont.availableCarparkLotsLabelFont()
        label.textColor = UIColor.carparkNameColor
        label.textAlignment = .left
        label.text = "Next hour"
        containerView.addSubview(label)
        return label
    }()

    private lazy var lblNextHourCost: UILabel = {
        let label = UILabel()
        label.font = UIFont.availableCarparkLotsFont()
        label.textColor = UIColor.carparkNameColor
        label.textAlignment = .right
        containerView.addSubview(label)
        label.text = "$1.20"
        return label
    }()

    private lazy var btnMoreInfo: UIButton = {
        let button = UIButton()
        button.setTitle("More Info", for: .normal)
        button.setTitleColor(UIColor.rgba(r: 109, g: 55, b: 173, a: 1), for: .normal)
        button.titleLabel?.font = UIFont.carparkButtonFont()
        button.addTarget(self, action: #selector(onMoreInfoClicked), for: .touchUpInside)
        containerView.addSubview(button)
        return button
    }()

    private lazy var btnConfirm: UIButton = {
        let button = UIButton()
        button.setTitle("Navigate here", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = UIFont.carparkButtonFont()
        button.backgroundColor = UIColor(named: "carparkNaviBtnColor")
        button.addTarget(self, action: #selector(onNavigationHere), for: .touchUpInside)
        containerView.addSubview(button)
        return button
    }()
        
    private lazy var containerView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 4.0
        addSubview(view)
        return view
    }()
    
    private var carpark: Carpark!
    
    var isNavigateBtnEnabled = true
    var onCarparkSelected: ((_ carpark: Carpark) -> Void)?
    var onMoreInfo: ((_ carpark: Carpark) -> Void)?

    // https://github.com/mapbox/mapbox-gl-native/issues/9228
    override var center: CGPoint {
        set {
            var newCenter = newValue
            newCenter.y -= bounds.midY
            super.center = newCenter
        }
        get {
            return super.center
        }
    }
    
    var location: CLLocationCoordinate2D {
        get {
            return CLLocationCoordinate2D(latitude: carpark.lat ?? 0, longitude: carpark.long ?? 0)
        }
    }
    
    required init(carpark: Carpark, isNavigateBtnEnabled: Bool,  isDarkMode: Bool) {
        super.init(frame: .zero)
       
        self.isNavigateBtnEnabled = isNavigateBtnEnabled
        self.carpark = carpark
        
        lblName.text = carpark.name

        if(carpark.availablelot?.rawValue == "NA")
        {
            lblLotsInfo.text = "No Info"
        }
        else
        {
            lblLotsInfo.text = "\(carpark.availablelot?.intValue ?? 0)"
        }

        if let rate = carpark.currentHrRate?.oneHrRate {
            lblCost.text = String(format: "$%.2f", rate)
        } else {
            lblCost.text = "No Info"
        }
        
        if let nextRate = carpark.nextHrRate?.oneHrRate {
            lblNextHourCost.text = String(format: "$%.2f", nextRate)
        } else {
            lblNextHourCost.text = "No Info"
        }
        
        self.backgroundColor = .clear
        
        
        setupLayout()
        
        if (isDarkMode)
        {
            self.backgroundColor = UIColor.rgba(r: 34, g: 38, b: 56, a: 1.0)
            self.containerView.backgroundColor = UIColor.rgba(r: 34, g: 38, b: 56, a: 1.0)
            lblName.textColor = UIColor.white
            self.lblLotsInfo.textColor = .white
            self.lblCost.textColor = .white
            self.lblNextHourCostLable.textColor = .white
            self.lblLotsLabel.textColor = .white
            self.lblCostLabel.textColor = .white
            self.lblNextHourCost.textColor = .white
            self.btnMoreInfo.setTitleColor(UIColor.rgba(r: 177, g: 85, b: 248, a: 1), for: .normal)
            self.btnConfirm.backgroundColor = UIColor.rgba(r: 177, g: 85, b: 248, a: 1)
        }
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = 10
        containerView.layer.cornerRadius = 10
        layer.masksToBounds = true
        
        btnConfirm.layer.cornerRadius = buttonHeight / 2
        btnConfirm.layer.masksToBounds = true
        
       
        
        //showShadow()
    }
    
    @objc private func onNavigationHere() {
        if let callback = onCarparkSelected {
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Carpark.UserClick.navigate_here, screenName: ParameterName.Carpark.screen_view)
            callback(carpark)
        }
    }
    
    @objc private func onMoreInfoClicked() {
        if let callback = onMoreInfo {
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Carpark.UserClick.more_info, screenName: ParameterName.Carpark.screen_view)
            callback(carpark)
        }
    }
    
    override func sizeThatFits(_ size: CGSize) -> CGSize {
                
        let lblNameSize = lblName.intrinsicContentSize
        let lblLotsSize = lblLotsInfo.intrinsicContentSize
        let lblCostSize = lblCost.intrinsicContentSize
        let lblNextHourCostSize = lblNextHourCost.intrinsicContentSize
                        
//        let w = min(max(buttonWidth,max(lblNameSize.width, lblAddrSize.width)) + padding*2, UIScreen.main.bounds.width - 50)
        let h = lblNameSize.height + gap + lblLotsSize.height + gap + lblCostSize.height + gap + lblNextHourCostSize.height + gap + buttonHeight + gap + buttonHeight + padding + tipHeight + gap
        
        return CGSize(width: popupWidth, height: h)
    }
        
    private func setupLayout() {
        lblName.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(yPadding)
            make.leading.equalToSuperview().offset(padding)
            make.trailing.equalToSuperview().offset(-padding)
        }
        
        imgLotsInfo.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(padding)
            make.top.equalTo(lblName.snp.bottom).offset(gap)
            make.height.equalTo(20)
            make.width.equalTo(20)
        }
        
        lblLotsLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(imgLotsInfo.snp.trailing).offset(padding)
            make.centerY.equalTo(imgLotsInfo.snp.centerY)
            make.width.equalTo(100)
        }
        
        lblLotsInfo.snp.makeConstraints { (make) in
            make.centerY.equalTo(imgLotsInfo.snp.centerY)
            make.trailing.equalToSuperview().offset(-padding)
        }

        imgCost.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(padding)
            make.top.equalTo(lblLotsInfo.snp.bottom).offset(gap)
            make.height.equalTo(20)
            make.width.equalTo(20)
        }

        lblCostLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(imgCost.snp.trailing).offset(padding)
            make.centerY.equalTo(imgCost.snp.centerY)
            make.width.equalTo(100)
        }

        lblCost.snp.makeConstraints { (make) in
            make.centerY.equalTo(imgCost.snp.centerY)
            make.trailing.equalToSuperview().offset(-padding)
        }

        lblNextHourCostLable.snp.makeConstraints { (make) in
            make.leading.equalTo(imgCost.snp.trailing).offset(padding)
            make.top.equalTo(lblCost.snp.bottom).offset(gap)
            make.width.equalTo(100)
        }

        lblNextHourCost.snp.makeConstraints { (make) in
            make.centerY.equalTo(lblNextHourCostLable.snp.centerY)
            make.trailing.equalToSuperview().offset(-padding)
        }

        btnMoreInfo.snp.makeConstraints { make in
            make.bottom.equalTo(btnConfirm.snp.top).offset(-gap)
            make.top.equalTo(lblNextHourCost.snp.bottom).offset(gap)
            make.centerX.equalTo(containerView.snp.centerX)
            make.width.equalTo(buttonWidth)
            make.height.equalTo(buttonHeight)
        }
        
        btnConfirm.snp.makeConstraints { make in
            make.bottom.equalToSuperview().offset(-yPadding)
            make.top.equalTo(btnMoreInfo.snp.bottom).offset(gap)
            make.centerX.equalTo(containerView.snp.centerX)
            make.width.equalTo(buttonWidth)
            make.height.equalTo(buttonHeight)
        }
       // btnConfirm.isHidden = !self.isNavigateBtnEnabled
        containerView.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
//            make.bottom.equalToSuperview().offset(-(tipHeight+gap))
            make.bottom.equalToSuperview().offset(-tipHeight)
            make.leading.equalToSuperview()
            make.width.equalTo(popupWidth)
            make.trailing.equalToSuperview()
        }
       
    }
    
    override func draw(_ rect: CGRect) {
        let rect = self.bounds
        // Draw the pointed tip at the bottom.
        let fillColor: UIColor = .white
        
        let tipLeft = rect.origin.x + (rect.size.width / 2.0) - (tipWidth / 2.0)
        let tipBottom = CGPoint(x: rect.origin.x + (rect.size.width / 2.0), y: rect.origin.y + rect.size.height)
//        let heightWithoutTip = rect.size.height - tipHeight - gap
        let heightWithoutTip = rect.size.height - tipHeight - 1
        
        let currentContext = UIGraphicsGetCurrentContext()!
        
        let tipPath = CGMutablePath()
        tipPath.move(to: CGPoint(x: tipLeft, y: heightWithoutTip))
        tipPath.addLine(to: CGPoint(x: tipBottom.x, y: tipBottom.y))
        tipPath.addLine(to: CGPoint(x: tipLeft + tipWidth, y: heightWithoutTip))
        tipPath.closeSubpath()
        
        fillColor.setFill()
        currentContext.addPath(tipPath)
        currentContext.fillPath()
    }
    
    // MARK: - MGLCalloutView API
    func present(from rect: CGRect, in view: UIView, constrainedTo constrainedRect: CGRect, animated: Bool) {

       
        
        //delegate?.calloutViewWillAppear?(self)
        view.addSubview(self)
        self.sizeToFit()

        // TODO: To support dark mode
//        let btnColor1 = UIColor.navBottomButtonColor1.resolvedColor(with: self.traitCollection)
//        let btnColor2 = UIColor.navBottomButtonColor2.resolvedColor(with: self.traitCollection)
//        btnConfirm.applyGradient(colors: [btnColor1.cgColor,btnColor2.cgColor], withShadow: false)

        if animated {
            alpha = 0

            UIView.animate(withDuration: 0.2) { [weak self] in
                guard let strongSelf = self else {
                    return
                }

                strongSelf.alpha = 1
                //strongSelf.delegate?.calloutViewDidAppear?(strongSelf)
            }
        } else {
            //delegate?.calloutViewDidAppear?(self)
        }
    }

    func dismiss(animated: Bool) {
        if (superview != nil) {
            if animated {
                UIView.animate(withDuration: 0.2, animations: { [weak self] in
                    guard let self = self else { return }
                    self.alpha = 0
                }, completion: { [weak self] _ in
                    guard let self = self else { return }
                    self.removeFromSuperview()
                })
            } else {
                removeFromSuperview()
            }
            
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Carpark.UserClick.close_bubble, screenName: ParameterName.Carpark.screen_view)

        }
    }

    // MARK: - Callout interaction handlers

    func isCalloutTappable() -> Bool {
//        if let delegate = delegate {
//            if delegate.responds(to: #selector(MGLCalloutViewDelegate.calloutViewShouldHighlight)) {
//                return delegate.calloutViewShouldHighlight!(self)
//            }
//        }
        return true
    }

    @objc func calloutTapped() {
//        if isCalloutTappable() && delegate!.responds(to: #selector(MGLCalloutViewDelegate.calloutViewTapped)) {
//            delegate!.calloutViewTapped!(self)
//        }
    }
}

//
//  RoutePlanningVC.swift
//  Breeze
//
//  Created by Zhou Hao on 3/2/21.
//

import UIKit
import CoreLocation
import MapboxDirections
import Turf
import MapboxCoreNavigation
@_spi(Restricted) import MapboxMaps
import MapboxCoreMaps
import MapboxNavigation
import SwiftyBeaver
import MapKit
import SnapKit
import ContactsUI
import Combine

class RoutePlanningVC: UIViewController, RouteBottomVCDelegate {

    // MARK: IBOutlet
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var address1: UILabel!
    @IBOutlet weak var favBtn: UIButton!
   /* @IBOutlet weak var searchBar: UISearchBar!*/
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var fscStackView:UIStackView!
    @IBOutlet weak var fastRouteBtn:UIButton!
    @IBOutlet weak var shortRouteBtn:UIButton!
    @IBOutlet weak var cheapRouteBtn:UIButton!
    @IBOutlet weak var activityIndicator:UIActivityIndicatorView!
    private var disposables = Set<AnyCancellable>()
    
    private var updateMapCancellable: AnyCancellable?
    
    private var etaInfo: ETAInfo? {
        didSet {
            // change the bottom panel button text
            routePlanningBottomVC.isETAShared = etaInfo != nil
        }
    }
    
    var selectedIndex = 0
    var erpPriceUpdater: ERPPriceCal!
//    var routePlanningBottomVC = RoutePlanningBottomVC()
    var routePlanningBottomVC: RoutePlanningBottomVC!
    
    // TODO: Need to remove this in new UI design
    var calloutView: AddEasyBreezyView! // callout to add/edit easy breezy
    
    var navView: NavigationMapView! {
        didSet {
            if let navigationMapView = oldValue {
                uninstall(navigationMapView)
            }
            
            if let navigationMapView = navView {
                LocationManager.shared.getLocation()
                configure(navigationMapView)
            }
        }
    }
    
    var waypoints: [Waypoint] = [] {
        didSet {
            waypoints.forEach {
                $0.coordinateAccuracy = -1
            }
        }
    }

    var response: RouteResponse? {
        didSet {
            guard let routes = response?.routes, let currentRoute = routes.first else {
                clearMap()
                return
            }
            navView.show(routes)
            routePlanningBottomVC.route = currentRoute
            //routePlanningBottomVC.response = response
            navView.showWaypoints(on: currentRoute)
            
        }
    }
    
    func clearMap() {
        // remove annotation and callout
        if let calloutView = self.calloutView {
            calloutView.dismissCallout(animated: false)
        }
        self.calloutView = nil
        
        self.navView.removeWaypoints()
    }
    
    // MARK: property passed by caller
    var selectionSource = ""
    var addressID = ""          // TODO: Remove it latter. Only use originalAddress
    var rankingIndex = 0
    
    var originalAddress: BaseAddress! // This is passed from search or favorite
    
    private var selectedAddress: BaseAddress! {
        didSet {
            viewModel.baseAddress = selectedAddress
            viewModel.setRPAddress()
        }
    }// TODO: To use this to replace EasyBreezyAddresses and SearchAddresses
    
    // MARK: private properties
    private  var viewModel: RoutePlanningViewModel!
//        didSet {
//            setupUI()
//        }
    
    private var routeOptions: NavigationRouteOptions?
    private var routes = [Route]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //setupSearchBox()
        NotificationCenter.default.addObserver(self, selector: #selector(receiveETA), name: Notification.Name(Values.NotificationETASetUp), object: nil)
        view.backgroundColor = UIColor.navBottomBackgroundColor1
        topView.backgroundColor = UIColor.navBottomBackgroundColor1
        topView.showBottomShadow()
        fscStackView.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        AnalyticsManager.shared.logScreenView(screenName: ParameterName.routeplanning_screen)
        
        if(viewModel == nil)
        {
            self.viewModel = RoutePlanningViewModel()
            appDelegate().routePlanningViewModel = self.viewModel
        }
        
        setupMap()
        
        //Commenting below line, as we don't need routePlanning state. When we sync route planning in both phone and car play we will enable this
        //appDelegate().enterIntoRoutePlanning(from: .mobile)
        setupDarkLightAppearance()
        toggleDarkLightMode()
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
     
        //updateMapCancellable?.cancel()
        //DataCenter.shared.removeERPUpdaterListner(listner: self)
    }
        
    deinit {
        SwiftyBeaver.debug("RoutePlanningVC deinit")
        NotificationCenter.default.removeObserver(self, name: Notification.Name(Values.NotificationETASetUp), object: nil)
        // TODO: Need to consider to create the viewModel in appDelegate otherwise we need to set it to nil manually otherwise there will be a memory leak
        
        // We should not make this to nil, as there is no sync between car play and phone Route Planning
        /*if(appDelegate().routePlanningViewModel != nil)
        {
            appDelegate().routePlanningViewModel = nil
        }*/
         if(self.viewModel != nil){
             self.viewModel.mobileRPMapViewUpdatable = nil
            self.viewModel = nil
        }
        
    }
    
#if TESTING
    @objc func topViewPanGesture(_ gesture: UIPanGestureRecognizer){
        guard gesture.state == .ended else {
            return
        }
        
        if case .Down = gesture.verticalDirection(target: self.topView) {
            let docPicker = UIDocumentPickerViewController(documentTypes: ["public.json"], in: .import)
            docPicker.delegate = self
            docPicker.allowsMultipleSelection = false
            present(docPicker, animated: true, completion: nil)
        }
    }
#endif
    
    @objc func receiveETA(_ notification: Notification) {
//        print(notification.userInfo)
        
        if let dictionary = notification.userInfo {
            print("receiveETA - \(dictionary)")
            
            let status = dictionary[Values.etaStatus] as! String
            if status.lowercased() == ETAInfo.Status.INIT {
                DispatchQueue.main.async { [weak self] in
                    guard let self = self else { return }
                    self.etaInfo = nil
                }
                return
            }
            let name = dictionary[Values.etaContactName] as! String
            let number = dictionary[Values.etaPhoneNumber] as! String
            let liveLocation = dictionary[Values.etaShareLiveLocation] as! Bool
            let message = dictionary[Values.etaMessage] as! String
            let sendVoice = dictionary[Values.etaSendVoice] as! Bool

            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                self.etaInfo = ETAInfo(message: message, recipientName: name, recipientNumber: number, shareLiveLocation: liveLocation, voiceCallToRecipient: sendVoice)
            }
        }
    }
        
    func setupUI(){
        navView.mapView.ornaments.options.attributionButton.visibility = .hidden
        favBtn.setBackgroundImage(viewModel.address.isEasyBreezy ? UIImage(named: "bookmark") : UIImage(named: "addtofavourite"), for: .normal)

        address.text = viewModel.address.isEasyBreezy ? viewModel.address.name : viewModel.address.address
        address1.text = viewModel.address.address1
        editBtn.isHidden = !viewModel.address.isEasyBreezy
    }
    
    @IBAction func backbtnAction(){
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.RoutePlanning.UserClick.back, screenName: ParameterName.RoutePlanning.screen_view)
//        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.back, screenName: ParameterName.routeplanning_screen)
        
        //When we present, we use Dismiss
//        self.dismiss(animated: true, completion: nil)
        
        //When we push, we need to use pop
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addEasyBreezy(_ sender: Any){
                
        if sender as? UIButton == favBtn && !viewModel.address.isEasyBreezy {
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.RoutePlanning.UserClick.add_remove_favourite, screenName: ParameterName.RoutePlanning.screen_view)
            
            viewModel.saveEasyBreezy { [weak self] result in
                guard let self = self else { return }
                switch result {
                case .failure(let err):
                    if err as? EasyBreezyServiceError == EasyBreezyServiceError.duplicated {
                        self.showPromptToast(Constants.eBDuplicateNameInRoutePlanning)
                        return
                    }
                    self.showPromptToast("Failed to save the address")
                case .success(_):
                    self.setupUI()
                    self.showPromptToast(Constants.ebSaved)
                }
            }
/*
            let place = SavedPlaces(placeName: viewModel.address.address, address:viewModel.address.address1, lat: "\(viewModel.address.lat)", long: "\(viewModel.address.long)", time: viewModel.address.date)
                    
            let ebName = viewModel.address.name
            let addressID = Int(viewModel.address.id) ?? 0
            
            let storyboard = UIStoryboard(name: "EasyBreezy", bundle: nil)
            if let vc = storyboard.instantiateViewController(withIdentifier: String(describing: SaveEasyBreezyVC.self)) as? SaveEasyBreezyVC {
                vc.place = place
                vc.ebName = viewModel.address.isEasyBreezy ? ebName : ""
                vc.addressID = addressID
                vc.delegate = self
                self.navigationController?.pushViewController(vc, animated: true)
            }
*/
        } else if sender as? UIButton == editBtn {
            
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.RoutePlanning.UserClick.edit_favourite, screenName: ParameterName.RoutePlanning.screen_view)
            
//            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.routeplanning_edit_fave, screenName: ParameterName.routeplanning_screen)
            
            
            let place = SavedPlaces(placeName: viewModel.address.address, address:viewModel.address.address1, lat: "\(viewModel.address.lat)", long: "\(viewModel.address.long)", time: viewModel.address.date)
                    
            let ebName = viewModel.address.name
            let addressID = Int(viewModel.address.id) ?? 0
            
            let storyboard = UIStoryboard(name: "EasyBreezy", bundle: nil)
            if let vc = storyboard.instantiateViewController(withIdentifier: String(describing: SaveEasyBreezyVC.self)) as? SaveEasyBreezyVC {
                vc.place = place
                vc.ebName = viewModel.address.isEasyBreezy ? ebName : ""
                vc.addressID = addressID
                vc.delegate = self
                self.navigationController?.pushViewController(vc, animated: true)
            }
        } else if sender as? UIButton == favBtn && viewModel.address.isEasyBreezy {
/*
            self.popupAlert(title: nil, message: Constants.easyBreezyDeleteMessage, actionTitles: [Constants.cancel,Constants.remove], actions:[{action1 in
                
            },{ action2 in
                self.deleteEasyBreezy()
            }, nil])
*/
            self.deleteEasyBreezy()
        }
    }
    
    private func deleteEasyBreezy() {
        
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.RoutePlanning.UserClick.add_remove_favourite, screenName: ParameterName.RoutePlanning.screen_view)

//        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.routeplanning_add_remove_fave, screenName: ParameterName.routeplanning_screen)
        
        viewModel.deleteEasyBreezy { [weak self] success in
            guard let self = self else { return }
            if success {
                self.showPromptToast(Constants.ebRemoved, icon: "dustBin")
                self.setupUI()
            } else {
                self.showPromptToast("Failed to deleted the Favorite")
            }
        }
    }
    
    private func configure(_ navigationMapView: NavigationMapView) {
        
        view.addSubview(navigationMapView)
        view.sendSubviewToBack(navigationMapView)

//        RouteLineWidthByZoomLevel = [
//            10.0: 4.0,
//            13.0: 5.0,
//            16.0: 8.0,
//            19.0: 12.0,
//            22.0: 15.0
//        ]
        
        navigationMapView.snp.makeConstraints { (make) in
            make.leading.equalToSuperview()
            make.trailing.equalToSuperview()
            make.bottom.equalToSuperview()
            make.top.equalToSuperview().offset(60)
        }
        
        navigationMapView.delegate = self
        
        //DataCenter.shared.addERPUpdaterListner(listner: self)
    
        //beta.14 change
        let puck2d = Puck2DConfiguration(topImage: UIImage(named: "puckArrow"), bearingImage: UIImage(named: "puckIcon"), shadowImage: nil, scale: nil)
        
        navView.userLocationStyle = .puck2D(configuration: puck2d)
                
        self.viewModel?.mobileRPMapViewUpdatable = RoutePlanningViewMap(navigationMapView: self.navView)
        self.viewModel?.mobileRPMapViewUpdatable?.setRouteLineWidth()
        self.selectedAddress = self.originalAddress
        self.setupUI()
        self.viewModel.setUpRouteColors(isDarkMode: self.isDarkMode)
        
        self.updateMapCancellable = self.viewModel.mobileRPMapViewUpdatable!.$updateMap
            .receive(on: DispatchQueue.main)
            .sink(receiveValue: { [weak self] updateMap in
            guard let self = self else { return }
            if updateMap {
                SwiftyBeaver.debug("Update Map response came from Model class")
                self.updateMapWithRouteResponse()
            } else {
                
            }
        })
                

        navigationMapView.mapView.mapboxMap.onEvery(.styleLoaded, handler: { [weak self] _ in
            guard let self = self else { return }
            

            //we will enable this after map box fix current location issue in the beta.14 SDK
            /*guard let address = self!.viewModel.address, let coordinate = self!.navView.mapView.location.latestLocation?.coordinate else {
                print("return")

            guard let address = self.viewModel.address, let coordinate = self.navView.mapView.location.latestLocation?.coordinate else {
                return
            }*/
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) { [weak self] in
                guard let self = self else { return }
                self.activityIndicator.startAnimating()
                if let response = self.viewModel.mobileRPMapViewUpdatable?.originalResponse{
                    self.viewModel.mobileRPMapViewUpdatable?.originalResponse.removeAll()
                }
                self.viewModel.setupRoute()

            }
            
            //self!.setupAnnotation(location: CLLocationCoordinate2D(latitude: address.lat, longitude: address.long), name: address.address1)
        })
        
            
            //beta.9 change
            navView.mapView.ornaments.options.compass.visibility = .hidden
            
        
        /*navView.mapView.location.options.puckType = .puck2D(Puck2DConfiguration(topImage: UIImage(named: "puckIcon"), bearingImage: UIImage(named: "puckArrow"), shadowImage: nil, scale: nil))*/
        
        
        navigationMapView.mapView.mapboxMap.onEvery(.cameraChanged) { [weak self] _ in
            guard let self = self else { return }
            self.adjustCallout()
        }
        
        setupGestureRecognizers()
    }
    
    private func toggleDarkLightMode() {
        viewModel.setUpRouteColors(isDarkMode: isDarkMode)
        navView.mapView.mapboxMap.style.uri = StyleURI(url: URL(string: isDarkMode ? Constants.Map.basicNightStyle: Constants.Map.basicStyle)!)
    }
    
    // MARK: - Theme updated
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        guard previousTraitCollection != traitCollection else {
            return
        }
        toggleDarkLightMode()
    }
    
    // adjust callout when the map camera changed
    private func adjustCallout() {
        guard let callout = calloutView else { return }
        guard let address = self.viewModel.address else {return}
        
        let padding: CGFloat = 20
        let location = CLLocationCoordinate2D(latitude: address.lat, longitude: address.long)
        
        guard let screenCoordinate = navView?.mapView.mapboxMap.point(for: location) else { return }
        let point = CGPoint(x: screenCoordinate.x, y: screenCoordinate.y)
        let halfW = callout.bounds.width / 2
        if point.x > navView.center.x {
            callout.center = CGPoint(x: point.x - halfW - padding, y: point.y + padding)
        } else {
            callout.center = CGPoint(x: point.x + halfW + padding, y: point.y + padding)
        }
    }

    private func uninstall(_ navigationMapView: NavigationMapView) {
        navigationMapView.removeFromSuperview()
    }
    
    private func setupMap() {
        if navView == nil {
            navView = NavigationMapView()
            
        }
        else
        {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                self.viewModel.setupRoute()
            }
        }
        
        routePlanningBottomVC.view.isHidden = true
        routePlanningBottomVC.hideButtons = true
    }
    
    /*private func setupAnnotation(location: CLLocationCoordinate2D, name: String) {
    
        let annotations = navView.mapView.annotations.values.filter({ $0.title == name })
        navView.mapView.annotations.removeAnnotations(annotations)
                
        var customPointAnnotation = PointAnnotation(coordinate: location,image:
                                                        viewModel.address.isEasyBreezy ? UIImage(named: "destinationMark") : UIImage(named: "newAddressPin"))
        customPointAnnotation.title = name
        
        navView.mapView.annotations.interactionDelegate = self
        navView.mapView.annotations.addAnnotation(customPointAnnotation)
    }*/
    
#if TESTING
    typealias RouteRequestSuccess = ((RouteResponse) -> Void)
    typealias RouteRequestFailure = ((Error) -> Void)
#endif
    
    private func setupGestureRecognizers() {
#if TESTING
        let longPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress(_:)))
        navView.gestureRecognizers?.filter({ $0 is UILongPressGestureRecognizer }).forEach(longPressGestureRecognizer.require(toFail:))
        navView.addGestureRecognizer(longPressGestureRecognizer)
        let topViewPanGesture = UIPanGestureRecognizer(target: self, action: #selector(topViewPanGesture(_:)))
        topView.addGestureRecognizer(topViewPanGesture)
#endif

        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(panGesture(_:)))
        let pinchGesture = UIPinchGestureRecognizer(target: self, action: #selector(pinchGesture(_:)))
        navView.addGestureRecognizer(panGesture)
        navView.addGestureRecognizer(pinchGesture)
    }
        
    @objc func panGesture(_ gesture: UIPanGestureRecognizer){
        AnalyticsManager.shared.logMapInteraction(eventValue: ParameterName.RoutePlanning.MapInteraction.drag, screenName: ParameterName.RoutePlanning.screen_view)
    }
        
    @objc func pinchGesture(_ gesture: UIPinchGestureRecognizer){
        AnalyticsManager.shared.logMapInteraction(eventValue: ParameterName.RoutePlanning.MapInteraction.pinch, screenName: ParameterName.RoutePlanning.screen_view)
    }

#if TESTING
    @objc func handleLongPress(_ gesture: UILongPressGestureRecognizer) {
        guard gesture.state == .began else { return }
        
        // TODO: Not working in beta.9
/*
        // remove the waypoints first
        waypoints.removeAll()
        
        let gestureLocation = gesture.location(in: navView)
        
        let destinationCoordinate = navView.mapView.coordinate(for: gestureLocation, in: navView)
        
        // TODO: Implement ability to get last annotation.
        // if let annotation = navigationMapView.annotations?.last, waypoints.count > 2 {
        //     mapView.removeAnnotation(annotation)
        // }
        
        if waypoints.count > 1 {
            waypoints = Array(waypoints.dropFirst())
        }
        
        // Note: The destination name can be modified. The value is used in the top banner when arriving at a destination.
        let waypoint = Waypoint(coordinate: destinationCoordinate, name: "Dropped Pin #\(waypoints.endIndex + 1)")
        // Example of building highlighting. `targetCoordinate`, in this example,
        // is used implicitly by NavigationViewController to determine which buildings to highlight.
        waypoint.targetCoordinate = destinationCoordinate
        waypoints.append(waypoint)
        
        // Example of highlighting buildings in 3d and directly using the API on NavigationMapView.
        let buildingHighlightCoordinates = waypoints.compactMap { $0.targetCoordinate }
        navView.highlightBuildings(at: buildingHighlightCoordinates)

        requestRoute()
*/
    }

    private func requestRoute() {
        guard waypoints.count > 0 else { return }
        
        // Beta.15
        guard let currentLocation = navView.mapView.location.latestLocation else {
            print("User location is not valid. Make sure to enable Location Services.")
            return
        }
        
        let userWaypoint = Waypoint(coordinate: currentLocation.coordinate)
        waypoints.insert(userWaypoint, at: 0)

        let navigationRouteOptions = NavigationRouteOptions(waypoints: waypoints)
        
        // Get periodic updates regarding changes in estimated arrival time and traffic congestion segments along the route line.
        RouteControllerProactiveReroutingInterval = 30

        requestRoute(with: navigationRouteOptions, success: defaultSuccess, failure: defaultFailure)
    }
        
    private func requestRoute(with options: RouteOptions, success: @escaping RouteRequestSuccess, failure: RouteRequestFailure?) {
        Directions.shared.calculateWithCache(options: options) { (session, result) in
            switch result {
            case let .success(response):
                success(response)
            case let .failure(error):
                failure?(error)
            }
        }
    }

    fileprivate lazy var defaultSuccess: RouteRequestSuccess = { [weak self] (response) in
        guard let routes = response.routes, !routes.isEmpty, case let .route(options) = response.options else { return }
        guard let self = self else { return }
        self.navView.removeWaypoints()
        self.response = response
        
        // Waypoints which were placed by the user are rewritten by slightly changed waypoints
        // which are returned in response with routes.
        if let waypoints = response.waypoints {
            self.waypoints = waypoints
        }
    }

    fileprivate lazy var defaultFailure: RouteRequestFailure = { [weak self] (error) in
        guard let self = self else { return }
        // Clear routes from the map
        self.response = nil
    //        self?.presentAlert(message: error.localizedDescription)
    }
#endif
    
#if TESTING
    private func setupMockRoute(route: BZRoute) {
        
    }
#endif
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

            switch segue.destination {
            case let vc as RoutePlanningBottomVC:
                
                routePlanningBottomVC = vc
//                viewModel.mobileRPMapViewUpdatable?.routePlanningBottomVC = routePlanningBottomVC
                routePlanningBottomVC.delegate = self
                
            default:

                break

            }

        }
    
    //MARK:- Fast,Short and Cheap
    //FastBtn Tag : 10
    //ShortBtn Tag :11
    //CheapBtn Tag :12
    func setColorOfFSCBtns(tag1:Int,tag2:Int,color:UIColor = .white, image1:UIImage,image2:UIImage){
        
        let btn = self.view.viewWithTag(tag1) as? UIButton
        let btn1 = self.view.viewWithTag(tag2) as? UIButton
        btn?.isSelected = false
        btn1?.isSelected = false
        btn?.backgroundColor = color
        btn1?.backgroundColor = color
        
        btn?.setImage(image1, for: .normal)
        btn1?.setImage(image2, for: .normal)
        btn?.setTitleColor(UIColor.rgba(r: 48, g: 52, b: 74, a: 1), for: .normal)
        btn1?.setTitleColor(UIColor.rgba(r: 48, g: 52, b: 74, a: 1), for: .normal)
        
        if (isDarkMode){
            btn?.backgroundColor = UIColor.rgba(r: 34, g: 38, b: 56, a: 1)
            btn1?.backgroundColor = UIColor.rgba(r: 34, g: 38, b: 56, a: 1)
            btn?.setTitleColor(UIColor.white, for: .normal)
            btn1?.setTitleColor(UIColor.white, for: .normal)
        }
    }
    
    func setUPFSCUI(){
    
        //Fix me - For now we are adjusting the font size by comparing the screen size. This needs to change in future
        if UIScreen.main.bounds.width <= 375 {
            
            fastRouteBtn.titleLabel?.font = UIFont.routePreferenceFont()
            shortRouteBtn.titleLabel?.font = UIFont.routePreferenceFont()
            cheapRouteBtn.titleLabel?.font = UIFont.routePreferenceFont()
            
            fastRouteBtn.titleEdgeInsets = UIEdgeInsets.init(top: 6, left: 7, bottom: 6, right: 5)
            fastRouteBtn.imageEdgeInsets = UIEdgeInsets.init(top: 6, left: 0, bottom: 6, right: 7)
            
            shortRouteBtn.titleEdgeInsets = UIEdgeInsets.init(top: 6, left: 7, bottom: 6, right: 5)
            shortRouteBtn.imageEdgeInsets = UIEdgeInsets.init(top: 6, left: 0, bottom: 6, right: 8)
            
            cheapRouteBtn.titleEdgeInsets = UIEdgeInsets.init(top: 6, left: 8, bottom: 6, right: 8)
            cheapRouteBtn.imageEdgeInsets = UIEdgeInsets.init(top: 6, left: 0, bottom: 6, right: 17)
        }
//        fastRouteBtn.titleLabel?.adjustsFontSizeToFitWidth = true
//        fastRouteBtn.titleLabel?.minimumScaleFactor = 0.5
//        shortRouteBtn.titleLabel?.adjustsFontSizeToFitWidth = true
//        shortRouteBtn.titleLabel?.minimumScaleFactor = 0.5
//        shortRouteBtn.titleLabel?.adjustsFontSizeToFitWidth = true
//        cheapRouteBtn.titleLabel?.minimumScaleFactor = 0.5

       
        if UserSettingsModel.sharedInstance.routePref == "Fastest"{
            
            defaultFSCUI(sender: fastRouteBtn,image: UIImage(named: "fastSelect")!)
            setColorOfFSCBtns(tag1: 11, tag2: 12,image1: UIImage(named: "shortUnselect")!,image2:UIImage(named: "cheapUnselect")!)
            
        }
        else if UserSettingsModel.sharedInstance.routePref == "Cheapest"{
            
            defaultFSCUI(sender: cheapRouteBtn,image: UIImage(named: "cheapSelect")!)
            setColorOfFSCBtns(tag1: 10, tag2: 11,image1: UIImage(named: "fastUnselect")!,image2:UIImage(named: "shortUnselect")!)
        }
        
        else if UserSettingsModel.sharedInstance.routePref == "Shortest"{
            
            defaultFSCUI(sender: shortRouteBtn,image: UIImage(named: "shortSelect")!)
            setColorOfFSCBtns(tag1: 10, tag2: 12,image1: UIImage(named: "fastUnselect")!,image2:UIImage(named: "cheapUnselect")!)
        }
        else if UserSettingsModel.sharedInstance.routePref == nil{
            
            defaultFSCUI(sender: fastRouteBtn,image: UIImage(named: "fastSelect")!)
            setColorOfFSCBtns(tag1: 11, tag2: 12,image1: UIImage(named: "shortUnselect")!,image2:UIImage(named: "cheapUnselect")!)
        }
    }
    
    func defaultFSCUI(sender:UIButton,image:UIImage){
        
        sender.isSelected = true
        sender.backgroundColor = UIColor.brandPurpleColor
        sender.setTitleColor(.white, for: .normal)
        sender.setImage(image, for: .normal)
    }
    
    @IBAction func fastRoute(sender:UIButton){
        
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.RoutePlanning.UserClick.fastest_route, screenName: ParameterName.RoutePlanning.screen_view)
        
        if(!sender.isSelected)
        {
            setColorOfFSCBtns(tag1: 11, tag2: 12,image1: UIImage(named: "shortUnselect")!,image2:UIImage(named: "cheapUnselect")!)
            defaultFSCUI(sender: sender,image: UIImage(named: "fastSelect")!)
            
            FSCPreferenceServerCall(routePrefValue: "Fastest", eventValue: ParameterName.routeplanning_route_fast)
            
            self.updateMapWithResponse(response: self.viewModel.mobileRPMapViewUpdatable!.defaultResponse!)
            routePlanningBottomVC.collectionView.scrollToItem(at:IndexPath(item: 0, section: 0), at: .right, animated: true)
        
        
        }
            
    }
    
    @IBAction func shortRoute(sender:UIButton){
        
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.RoutePlanning.UserClick.shortest_route, screenName: ParameterName.RoutePlanning.screen_view)
        
        if(!sender.isSelected)
        {
            
            setColorOfFSCBtns(tag1: 10, tag2: 12,image1: UIImage(named: "fastUnselect")!,image2:UIImage(named: "cheapUnselect")!)
            defaultFSCUI(sender: sender,image: UIImage(named: "shortSelect")!)
            
            FSCPreferenceServerCall(routePrefValue: "Shortest", eventValue: ParameterName.routeplanning_route_short)
            
            self.updateMapWithResponse(response: self.viewModel.mobileRPMapViewUpdatable!.shortestResponse!)
            routePlanningBottomVC.collectionView.scrollToItem(at:IndexPath(item: 0, section: 0), at: .right, animated: true)
        }
            
    }
    
    @IBAction func cheapRoute(sender:UIButton){
        
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.RoutePlanning.UserClick.cheapest_route, screenName: ParameterName.RoutePlanning.screen_view)
        
        if(!sender.isSelected)
        {
            setColorOfFSCBtns(tag1: 10, tag2: 11,image1: UIImage(named: "fastUnselect")!,image2:UIImage(named: "shortUnselect")!)
            defaultFSCUI(sender: sender,image: UIImage(named: "cheapSelect")!)
            
            FSCPreferenceServerCall(routePrefValue: "Cheapest", eventValue: ParameterName.routeplanning_route_cheap)
            
            self.updateMapWithResponse(response: self.viewModel.mobileRPMapViewUpdatable!.cheapestResponse!)
            routePlanningBottomVC.collectionView.scrollToItem(at:IndexPath(item: 0, section: 0), at: .right, animated: true)
        
        
        }
            
    }
    
    func FSCPreferenceServerCall(routePrefValue:String,eventValue:String){
        
        AnalyticsManager.shared.logClickEvent(eventValue: eventValue, screenName: ParameterName.settings_route_screen)
        UserSettingsModel.sharedInstance.routePref = routePrefValue
        updateRoutePreference()
    }
    
    func updateRoutePreference(){
        
        var settingsData =  [[String:Any]]()
    
        let routePref: [String: Any] = [
            "attribute": "RoutePreference",
            "value": UserSettingsModel.sharedInstance.routePref!
            ]
        settingsData.append(routePref)
        
        DispatchQueue.global(qos: .background).async {
            
            SettingsService.shared.updateUserSettings(settings:settingsData) { [weak self] (result) in
                guard let self = self else { return }
                
                print(result)
                switch result {
                case .success(_):
                    print("success")
                case .failure(_):
                    DispatchQueue.main.async {
                       
                    }
                }
            }
        }
    }

}

extension RoutePlanningVC: SaveEasyBreezyVCDelegate {
    
    func onEasyBreezySaved(name: String, place: SavedPlaces, addressId: Int) {
                
        // Currently we only care about the name
        viewModel.update(name: name)
        // refresh UI
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.setupUI()

            self.showPromptToast(Constants.ebSaved)
        }
    }
    
    func onEasyBreezySaved() {
    }
    
    func showPromptToast(_ message: String, icon: String = "greenTick") {
        showToast(from: self.view, message: message, topOffset: self.topView.frame.height + 4, icon: icon , messageColor: .black, duration: 3)
    }
}

extension RoutePlanningVC: AddEasyBreezyViewDelegate {
    // TODO: Need to be removed!!!
    func onAddEditEasyBreezy(place: SavedPlaces, addressId: Int, ebName: String) {
        let storyboard = UIStoryboard(name: "EasyBreezy", bundle: nil)
        if let vc = storyboard.instantiateViewController(withIdentifier: String(describing: SaveEasyBreezyVC.self)) as? SaveEasyBreezyVC {
            vc.place = place
            vc.ebName = ebName
            vc.addressID = addressId
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

extension RoutePlanningVC: UISearchBarDelegate {
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        return true
    }

    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.navigationController?.popViewController(animated: true)
    }
}

// TODO: To be migrated
// MARK: - NavigationMapViewDelegate methods
extension RoutePlanningVC: NavigationMapViewDelegate {
    
    func navigationMapView(_ navigationMapView: NavigationMapView, didAdd finalDestinationAnnotation: PointAnnotation, pointAnnotationManager: PointAnnotationManager)
    {
        
        //beta.15 changed
        //let point = finalDestinationAnnotation.feature.geometry.value as! Point
        let point = finalDestinationAnnotation.point
        var destinationAnnotation = PointAnnotation(coordinate: point.coordinates)
        destinationAnnotation.image = .init(image: UIImage(named: "destinationMark")!, name: "routePlan")
        //navigationMapView.pointAnnotationManager?.syncAnnotations([destinationAnnotation])
        navigationMapView.pointAnnotationManager?.annotations = [destinationAnnotation]
    }
    
//    func navigationMapView(_ navigationMapView: NavigationMapView, waypointCircleLayerWithIdentifier identifier: String, sourceIdentifier: String) -> CircleLayer? {
//        var circleLayer = CircleLayer(id: identifier)
//        circleLayer.source = sourceIdentifier
//        let opacity = Exp(.switchCase) {
//            Exp(.any) {
//                Exp(.get) {
//                    "waypointCompleted"
//                }
//            }
//            0.5
//            1
//        }
//        circleLayer.circleColor = .constant(.init(UIColor(red:0.9, green:0.9, blue:0.9, alpha:1.0)))
//        circleLayer.circleOpacity = .expression(opacity)
//        circleLayer.circleRadius = .constant(.init(10))
//        circleLayer.circleStrokeColor = .constant(.init(UIColor.black))
//        circleLayer.circleStrokeWidth = .constant(.init(1))
//        circleLayer.circleStrokeOpacity = .expression(opacity)
//        return circleLayer
//    }
//
//
//    func navigationMapView(_ navigationMapView: NavigationMapView, waypointSymbolLayerWithIdentifier identifier: String, sourceIdentifier: String) -> SymbolLayer? {
//        var symbolLayer = SymbolLayer(id: identifier)
//        symbolLayer.source = sourceIdentifier
//        symbolLayer.textField = .expression(Exp(.toString){
//                                                        Exp(.get){
//                                                            "name"
//                                                        }
//                                                    })
//        symbolLayer.textSize = .constant(.init(10))
//        symbolLayer.textOpacity = .expression(Exp(.switchCase) {
//            Exp(.any) {
//                Exp(.get) {
//                    "waypointCompleted"
//                }
//            }
//            0.5
//            1
//        })
//        symbolLayer.textHaloWidth = .constant(.init(0.25))
//        symbolLayer.textHaloColor = .constant(.init(UIColor.black))
//        return symbolLayer
//    }
//
//    func navigationMapView(_ navigationMapView: NavigationMapView, shapeFor waypoints: [Waypoint], legIndex: Int) -> FeatureCollection? {
//        var features = [Turf.Feature]()
//        for (waypointIndex, waypoint) in waypoints.enumerated() {
//            // Beta.15
////            var feature = Turf.Feature(Point(waypoint.coordinate))
//            var feature = Turf.Feature(geometry: .point(Point(waypoint.coordinate)))
//
//            let properties = [
//
//                "waypointCompleted":JSONValue.boolean(waypointIndex < legIndex),
//                "name":JSONValue.string("#\(waypointIndex + 1)")
//            ]
//
//            feature.properties = properties
//            features.append(feature)
//        }
//        return FeatureCollection(features: features)
//    }
    
    /*func navigationMapView(_ mapView: NavigationMapView, didSelect waypoint: Waypoint) {
        guard let responseOptions = response?.options, case let .route(routeOptions) = responseOptions else { return }
        let modifiedOptions = routeOptions.without(waypoint: waypoint)

//        presentWaypointRemovalAlert { _ in
//            self.requestRoute(with:modifiedOptions, success: self.defaultSuccess, failure: self.defaultFailure)
//        }
    }*/
    
    func moveToSelectedRoute(route:Route){
        
        guard let routes = response?.routes else { return }
        guard let index = routes.firstIndex(where: { $0 === route }) else { return }
        self.response?.routes?.swapAt(index, 0)
        guard viewModel.address != nil else { return }
        self.viewModel.mobileRPMapViewUpdatable!.erpPriceUpdater.fetchERPTolls(erpTolls: DataCenter.shared.getAllERPFeatureCollection(),response: self.response!)
        self.routePlanningBottomVC.reload()
    }

    func navigationMapView(_ mapView: NavigationMapView, didSelect route: Route) {
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.RoutePlanning.UserClick.tap_route, screenName: ParameterName.RoutePlanning.screen_view)
        
        guard let routes = response?.routes else { return }
        guard let index = routes.firstIndex(where: { $0 === route }) else { return }
        self.response?.routes?.swapAt(index, 0)
        guard viewModel.address != nil else { return }
        self.viewModel.mobileRPMapViewUpdatable!.erpPriceUpdater.fetchERPTolls(erpTolls: DataCenter.shared.getAllERPFeatureCollection(),response: self.response!)
        guard let bottomVCRoutes = routePlanningBottomVC.response?.routes else { return }
        guard let index1 = bottomVCRoutes.firstIndex(where: { $0 === routePlanningBottomVC.route}) else { return }
        routePlanningBottomVC.collectionView.scrollToItem(at:IndexPath(item: index1, section: 0), at: .right, animated: true)

        routePlanningBottomVC.collectionView.layoutSubviews()
    }
    
    private func estimatedArrivalTime(_ duration: TimeInterval) -> (time: String,format: String) {
        let arrivalDate = Date(timeInterval: duration, since: Date())
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: arrivalDate)
        let t = hour > 12 ? hour - 12: hour
        let hourString = t < 10 ? "0\(t)" : t.description
        let minute = calendar.component(.minute, from: arrivalDate)
        let minString = minute < 10 ? "0\(minute)" : minute.description
        return (time: "\(hourString):\(minString)", format: hour >= 12 ? " PM" : " AM")
    }

    private func presentWaypointRemovalAlert(completionHandler approve: @escaping ((UIAlertAction) -> Void)) {
        let title = NSLocalizedString("REMOVE_WAYPOINT_CONFIRM_TITLE", value: "Remove Waypoint?", comment: "Title of alert confirming waypoint removal")
        let message = NSLocalizedString("REMOVE_WAYPOINT_CONFIRM_MSG", value: "Do you want to remove this waypoint?", comment: "Message of alert confirming waypoint removal")
        let removeTitle = NSLocalizedString("REMOVE_WAYPOINT_CONFIRM_REMOVE", value: "Remove Waypoint", comment: "Title of alert action for removing a waypoint")
        let cancelTitle = NSLocalizedString("REMOVE_WAYPOINT_CONFIRM_CANCEL", value: "Cancel", comment: "Title of action for dismissing waypoint removal confirmation sheet")
        
        let waypointRemovalAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let removeAction = UIAlertAction(title: removeTitle, style: .destructive, handler: approve)
        let cancelAction = UIAlertAction(title: cancelTitle, style: .cancel, handler: nil)
        [removeAction, cancelAction].forEach(waypointRemovalAlertController.addAction(_:))
        
        self.present(waypointRemovalAlertController, animated: true, completion: nil)
    }
    
    
    // MARK: - RouteBottomVCDelegate
    func onRouteChange(index: Int) {
    
        AnalyticsManager.shared.logSwipeEvent(eventValue: ParameterName.RoutePlanning.UserSwipe.swipe, screenName: ParameterName.RoutePlanning.screen_view)
        
        let currentRoute = routePlanningBottomVC.response?.routes![index]
        self.moveToSelectedRoute(route: currentRoute!)
    }
    
    func onRouteConfirmed(index: Int) {
        
        //Getting first route from filtered array
        var selectedResponseIndex = 0
        var selectedRouteIndex = 0
        
        //Checking nil condition for original response
        if let orignalRouteResponses = viewModel.mobileRPMapViewUpdatable?.originalResponse{
            
            for (i,r) in orignalRouteResponses.enumerated() {
                
                //Getting first route object from filtered routeResponse and keeping the responseIndex and route index in a variable if the route object matches with originanRouteResponse routes array
                
                if(UserSettingsModel.sharedInstance.routePref == Constants.fast || UserSettingsModel.sharedInstance.routePref == Constants.short){
                    
                    if let j = r.routes?.firstIndex(of: (response?.routes?.first)!) {
                        selectedResponseIndex = i
                        selectedRouteIndex = j
                        break
                    }
                }
                else{

                    //We dont't need to break loop, so that routResponse and routeIndex will choose from originalCheapest route options
                    if let j = r.routes?.firstIndex(of: (response?.routes?.first)!) {
                        selectedResponseIndex = i
                        selectedRouteIndex = j
                        
                    }
                }
                
            }
        }
        
        
        guard let route = viewModel.mobileRPMapViewUpdatable?.originalResponse[selectedResponseIndex].routes?.first, let responseOptions = viewModel.mobileRPMapViewUpdatable?.originalResponse[selectedResponseIndex].options, case let .route(routeOptions) = responseOptions, let response = viewModel.mobileRPMapViewUpdatable?.originalResponse[selectedResponseIndex] else { return }
        
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.RoutePlanning.UserClick.lets_go, screenName: ParameterName.RoutePlanning.screen_view)
        
        // Issue #250 - to stop cruise mode here so that it won't affect navigation idle time
//        NotificationCenter.default.post(name: Notification.Name(Values.notificationNavigationStart), object: nil)
        // We'll post notificationStart in TurnByTurnNavigationViewModel

//        routeOptions.locale = Locale(identifier: "en_SG") // HERE
        // #273 based on what Ochi suggested
        routeOptions.locale = Locale.enSGLocale()// HERE
        
        // method 1: Inherit from NavigationViewController
        
        //Vishnu - Fix Me - Once route Line disappear fixe from Map box on github #121, we will use Day and Night style in viewWillAppear() of Turn-by-Turn screen
        let styles = isDarkMode ? [CustomNightStyle()] : [CustomDayStyle()]
        #if targetEnvironment(simulator)
        let simulation = SimulationMode.always
        #else
        let simulation = SimulationMode.never
        #endif
        let navigationService = MapboxNavigationService(routeResponse:(viewModel.mobileRPMapViewUpdatable?.originalResponse[selectedResponseIndex])!, routeIndex: selectedRouteIndex, routeOptions: routeOptions, simulating: simulation)
#if DEMO || STAGING
        navigationService.simulationSpeedMultiplier = 1.5
#endif
        let options = NavigationOptions(styles: styles, navigationService: navigationService, predictiveCacheOptions: PredictiveCacheOptions())
        
        let progressVC = UIStoryboard(name: "Navigation", bundle: nil).instantiateViewController(withIdentifier: String(describing: NavProgressVC.self)) as! NavProgressVC
        options.bottomBanner = progressVC
        let topBannerVC = TopBannerViewController()
        options.topBanner = topBannerVC
        
        // Beta.21
//        let navigationViewController = TurnByTurnNavigationVC(for: route, routeIndex: 0, routeOptions: routeOptions, navigationOptions: options)
        let navigationViewController = TurnByTurnNavigationVC(for: (viewModel.mobileRPMapViewUpdatable?.originalResponse[selectedResponseIndex])!, routeIndex: selectedRouteIndex, routeOptions: routeOptions, navigationOptions: options)
        navigationViewController.routeLineTracksTraversal = true
        navigationViewController.endRouteDelegate = self
        progressVC.delegate = navigationViewController

        // MOVE this code to MapViewUpdatable already
//        if let viewportDataSource = (navigationViewController.navigationMapView?.navigationCamera.viewportDataSource as? NavigationViewportDataSource) {
////                    viewportDataSource.options.followingCameraOptions.minimumZoomLevel = 15
////                    viewportDataSource.options.followingCameraOptions.maximumZoomLevel = 16.5
//            viewportDataSource.options.followingCameraOptions.zoomRange = 15...16.5
//                    viewportDataSource.options.followingCameraOptions.pitchUpdatesAllowed = false
//                   viewportDataSource.followingMobileCamera.pitch = 50 // Your pitch here
//
//            //This fix is from here -> https://github.com/mapbox-collab/ncs-collab/issues/293
//            viewportDataSource.options.followingCameraOptions.bearingSmoothing.maximumBearingSmoothingAngle = 30.0
//            }

        progressVC.navigationViewController = navigationViewController
        navigationViewController.selectedAddress = self.selectedAddress
        // don't show end of route feedback screen
        navigationViewController.showsEndOfRouteFeedback = false
        // don't show floating buttons
        navigationViewController.floatingButtons = []
        navigationViewController.waypointStyle = .building
        navigationViewController.topBannerVC = topBannerVC
        navigationViewController.automaticallyAdjustsStyleForTimeOfDay = false
        
        navigationViewController.modalPresentationStyle = .fullScreen
                
        if let appdel = UIApplication.shared.delegate as? AppDelegate {
            appdel.navigationController = navigationViewController
        }
        
        var tripETA: TripETA? = nil
        // pass ETA data
        if let eta = etaInfo {
            
            tripETA = TripETA(type: TripETA.ETAType.Init, message: eta.message, recipientName: eta.recipientName, recipientNumber: eta.recipientNumber, shareLiveLocation: eta.shareLiveLocation ? "Y" : "N", voiceCallToRecipient: eta.voiceCallToRecipient ? "Y" : "N", tripStartTime: Int(Date().timeIntervalSince1970), tripEndTime: Int(Date().timeIntervalSince1970 + route.expectedTravelTime), tripDestLat: selectedAddress.lat?.toDouble() ?? 0, tripDestLong: selectedAddress.long?.toDouble() ?? 0, destination: viewModel.address.address)
            appDelegate().tripETA = tripETA
        }
        // change app state and pass navigation/eta info
        appDelegate().enterIntoNavigation(from: .mobile, navigationService: navigationService, with: tripETA)
        
        present(navigationViewController, animated: true) {
            if(self.viewModel != nil){
                self.viewModel.mobileRPMapViewUpdatable = nil
                self.viewModel.carPlayMapViewUpdatable = nil
                self.viewModel = nil
            }
            self.navView.removeFromSuperview()
            self.navView = nil
            // navigation started
//            appDelegate().cruiseSubmitHistoryData(description: "CruiseMode-End")
//            appDelegate().startNavigationRecordingHistoryData()
        }
    }
    
    func onCarparksNearby(index: Int) {
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.RoutePlanning.UserClick.carpark_nearby, screenName: ParameterName.RoutePlanning.screen_view)
        
        let storyboard = UIStoryboard(name: "Carparks", bundle: nil)
        if let vc = storyboard.instantiateViewController(withIdentifier: String(describing: CarparksVC.self)) as? CarparksVC {
            vc.address = self.selectedAddress
            vc.response = self.response
            vc.delegate = self
        
            //Malou - Carpark API change
            let arrivalTime  = Date(timeInterval: (response?.routes![self.selectedIndex].expectedTravelTime)!, since: Date())
            vc.arrivalTime = arrivalTime
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func onShareMyETA(index: Int) {
        
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.RoutePlanning.UserClick.share_my_eta, screenName: ParameterName.RoutePlanning.screen_view)
        
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.ShareMyETA.UserEdit.message_input, screenName: ParameterName.ShareMyETA.screen_view)
        
        
        if let eta = etaInfo {
            let rootView = RNViewManager.sharedInstance.viewForModule(
                                // String name that you use as appname in index.js
                    "Breeze",
                                // Pass data to React Native module when it launches
                    initialProperties: ["toScreen": "ETAScreen","navigationParams":[Values.etaContactName: eta.recipientName,Values.etaPhoneNumber:eta.recipientNumber,Values.etaMessage:eta.message,Values.etaShareLiveLocation: eta.shareLiveLocation, Values.etaSendVoice: eta.voiceCallToRecipient,Values.etaStatus:ETAInfo.Status.EDIT,"etaMode":"Navigation","destination":"","lat":"","long":""]
                                       ])
                    
             let reactNativeVC = UIViewController()
             reactNativeVC.view = rootView
             reactNativeVC.modalPresentationStyle = .fullScreen
             navigationController?.pushViewController(reactNativeVC, animated: true)

        } else {
            
//            let p3 = NSPredicate(format: "ANY self.phoneNumbers.'value'.'digits' BEGINSWITH %@", "+65")
//            let p5 = NSPredicate(format: "ANY self.phoneNumbers.'value'.length == 8")
            let p4 = NSPredicate(format: "phoneNumbers.@count > 0")
            let predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [p4])
            selectedIndex = index
            let contacVC = CNContactPickerViewController()
            AnalyticsManager.shared.logScreenView(screenName: ParameterName.eta_contact_screen)
            contacVC.delegate = self
            contacVC.predicateForEnablingContact = predicate
            
            contacVC.displayedPropertyKeys = [CNContactGivenNameKey,CNContactFamilyNameKey,CNContactPhoneNumbersKey]
            self.present(contacVC, animated: true, completion: nil)
        }
    }
    
    func onReset() {
        
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.RoutePlanning.UserClick.reset_carpark, screenName: ParameterName.RoutePlanning.screen_view)
        
        clearMap()
        routePlanningBottomVC.needToReset = false
        selectedAddress = originalAddress
        setupUI()
        viewModel.setupRoute()
    }
        
}

// MARK: - for Contact list
extension RoutePlanningVC: CNContactPickerDelegate {
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
        print(contact.phoneNumbers)
        let numbers = contact.phoneNumbers.first
        let phoneNumber = (numbers?.value)?.stringValue ?? ""
        
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.eta_contact_select, screenName: ParameterName.eta_contact_screen)
        
        onContactSelected(phoneNumber: phoneNumber, name: "\(contact.givenName) \(contact.familyName)")
    }
    
    func contactPickerDidCancel(_ picker: CNContactPickerViewController) {
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.eta_contact_cancel, screenName: ParameterName.eta_contact_screen)
        self.dismiss(animated: true, completion: nil)
    }
    
    private func onContactSelected(phoneNumber: String, name: String) {
//        self.selectedETAContact = ETAContact(name: name, phone: phoneNumber)
        self.dismiss(animated: true) { [self] in
            
            if !PhoneValidator.isValidSingaporeMobile(number: phoneNumber) {
                self.popupAlert(title: nil, message: Constants.contactSelectError, actionTitles: [Constants.gotIt], actions:[{action1 in

                },{action2 in

                }, nil])
                return
            }
            
            guard let address = viewModel.address else {
                return
            }
            let estimatedTime = self.estimatedArrivalTime((response?.routes![self.selectedIndex].expectedTravelTime)!)
            let etaMessage = "Hey, \(AWSAuth.sharedInstance.userName) here! I will be at \(address.address) around \(estimatedTime.time)\(estimatedTime.format)."
            let rootView = RNViewManager.sharedInstance.viewForModule(
                                // String name that you use as appname in index.js
                      "Breeze",
                                // Pass data to React Native module when it launches
                      initialProperties: ["toScreen":"ETAScreen",
                                          "navigationParams"
                                          : [Values.etaContactName: name,Values.etaPhoneNumber:phoneNumber,Values.etaMessage:etaMessage,Values.etaShareLiveLocation:true, Values.etaSendVoice:false,Values.etaStatus:ETAInfo.Status.INIT,"etaMode":"Navigation","destination":"","lat":"","long":""]                                   ])
                    
                 let reactNativeVC = UIViewController()
                 reactNativeVC.view = rootView
                 reactNativeVC.modalPresentationStyle = .fullScreen
                self.present(reactNativeVC, animated: true, completion: nil)
        }
    }
}

extension RoutePlanningVC: TurnByTurnNavigationVCDelegate {
    func onArrival() {
        
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: MapLandingVC.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }

        
/*
        // Don't delete this !!! Since it could be reused when car park feature is available in the future
        // navigation stopped
        NotificationCenter.default.post(name: Notification.Name(Values.notificationNavigationStop), object: nil)
        self.endCarPlayNavigation(canceled: false)
        let storyboard = UIStoryboard(name: "Navigation", bundle: nil)
        if let vc = storyboard.instantiateViewController(withIdentifier: String(describing: ArrivalVC.self)) as? ArrivalVC {
            vc.selectedAddress = selectedAddress
            self.navigationController?.pushViewController(vc, animated: true)
        }
*/
    }
    
    func goToParking() {
        
        
        if let topController = UIApplication.shared.keyWindow?.rootViewController {
            if topController.isKind(of:UINavigationController.self) {
                
                let navigationController = topController as! UINavigationController
                let storyboard = UIStoryboard(name: "ParkingSGWeb", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "ParkingWebVC") as UIViewController
                navigationController.pushViewController(vc, animated: true)
                
            }
            
        }
        
        
    }
}

// MARK: - for car park
extension RoutePlanningVC: CarparksVCDelegate {
    func onCarparkAddressSelected(address: BaseAddress) {
        selectedAddress = address
        
        // rerequest route
        clearMap()
        viewModel.setupRoute()
        setupUI()
        // updates bottom panel button to reset
        routePlanningBottomVC.needToReset = true
    }
}

// MARK: - for routing
// TODO: Need to migrate

extension RoutePlanningVC: AnnotationInteractionDelegate {
    func annotationManager(_ manager: AnnotationManager, didDetectTappedAnnotations annotations: [Annotation]) {
        
    }
    
    public func didDeselectAnnotation(annotation: Annotation) {
       
    }

    public func didSelectAnnotation(annotation: Annotation) {
        
    }
    
}

extension RoutePlanningVC {
    
    func updateMapWithRouteResponse(){
        
        DispatchQueue.main.async {
            
            if(UserSettingsModel.sharedInstance.routePref == Constants.fast)
            {
                self.updateMapWithResponse(response: self.viewModel.mobileRPMapViewUpdatable!.defaultResponse!)
            }
//
            if(UserSettingsModel.sharedInstance.routePref == Constants.short)
            {
                //To fix - As of now we are using fastest route for shortest as well and filtering by distance
                self.updateMapWithResponse(response: self.viewModel.mobileRPMapViewUpdatable!.shortestResponse!)
            }
//
            if(UserSettingsModel.sharedInstance.routePref == Constants.cheap)
            {
                self.updateMapWithResponse(response: self.viewModel.mobileRPMapViewUpdatable!.cheapestResponse!)
            }
            
            if(UserSettingsModel.sharedInstance.routePref == nil)
            {
                self.updateMapWithResponse(response: self.viewModel.mobileRPMapViewUpdatable!.defaultResponse!)
            }
            
            self.setUPFSCUI()
            self.fscStackView.isHidden = false
            
        }
    }
    
    func updateMapWithResponse(response:RouteResponse){
        
        
        self.response = response
        
        self.routePlanningBottomVC.response = response
        
        if let waypoints = response.waypoints {
            self.waypoints = waypoints
        }
        
        viewModel.mobileRPMapViewUpdatable!.erpPriceUpdater.routePlanningBottomVC = self.routePlanningBottomVC

        if(UserSettingsModel.sharedInstance.routePref == Constants.fast || UserSettingsModel.sharedInstance.routePref == Constants.short)
        {
            //This method is find the available ERP on the route line and display their prices on to the map
            viewModel.mobileRPMapViewUpdatable!.erpPriceUpdater.fetchERPTolls(erpTolls: DataCenter.shared.getAllERPFeatureCollection(),response: response)
        }
        else
        {
//            self.erpPriceUpdater.fetchERPTolls(erpTolls: DataCenter.shared.getAllERPFeatureCollection(),response: response)
            viewModel.mobileRPMapViewUpdatable!.erpPriceUpdater.removeLayer()
            self.routePlanningBottomVC.erpPrice = "$0.00"
        }
       
//

        
        self.activityIndicator.stopAnimating()
        self.setCameraPosition(routes: response.routes ?? [])
    }
    
    func setCameraPosition(routes:[Route]){
        
        SwiftyBeaver.debug("Set Camera Position")
        var coordinates: [[LocationCoordinate2D]] = [] //Here you create an array of arrays for route coordinates
        for route in routes {
            
            
            coordinates.append(route.shape!.coordinates)
            
        }
        
        let multilLine = MultiLineString(coordinates)
        
        let routeGeoMetry = Geometry.multiLineString(multilLine)
        
        // #267
        navView.navigationCamera.stop()
        
        //beta.9 change
        let cameraOptions =   navView.mapView.mapboxMap.camera(for: routeGeoMetry, padding:UIEdgeInsets(top: fscStackView.frame.origin.y+fscStackView.frame.size.height-30, left: 40, bottom:routePlanningBottomVC.view.frame.size.height, right: 40), bearing: 0, pitch: 0)
        navView.mapView.camera.ease(to: cameraOptions, duration: 0.8, curve: .easeInOut, completion: nil)
        routePlanningBottomVC.view.isHidden = false
        routePlanningBottomVC.hideButtons = false
        routePlanningBottomVC.reload()
    }
    
}

#if TESTING
extension RoutePlanningVC: UIDocumentPickerDelegate {
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        if let url = urls.first {
            if let jsonData = try? Data(contentsOf: url) {
                if let route: BZRoute = try? JSONDecoder().decode(BZRoute.self, from: jsonData) {
                    let wayPoints = route.wayPoints.map { Waypoint(coordinate: CLLocationCoordinate2D(latitude: $0.latitude, longitude: $0.longitude), coordinateAccuracy: -1, name: $0.name) }
                    viewModel.calculateRoute(wayPoints: wayPoints)
                }
            }
        }
    }
        
}
#endif

//
//  CustomDayStyle.swift
//  Breeze
//
//  Created by VishnuKanth on 02/03/21.
//
import UIKit
import MapboxCoreNavigation
import MapboxMaps
import MapboxCoreMaps
import MapboxNavigation

class CustomDayStyleCP: DayStyle {
    required init() {
        super.init()
#if TESTING
        mapStyleURL = URL(string: Constants.Map.testURL)!
#else
        mapStyleURL = URL(string: Constants.Map.styleUrl)!
#endif
        previewMapStyleURL = mapStyleURL
        styleType = .day
        self.statusBarStyle = .lightContent
    }
    
    override func apply() {
        super.apply()
        
        let traitCollection = UITraitCollection(userInterfaceIdiom: .carPlay)
        //let carPlayTraitCollection = UITraitCollection(userInterfaceIdiom: .carPlay)
//        var traitCollection = UITraitCollection(userInterfaceStyle: .light)
//        if(Settings.shared.theme == 0){
//
//            if(appDelegate().isDarkMode()){
//
//                traitCollection = UITraitCollection(userInterfaceStyle: .dark)
//
//            }
//
//        }
//        else{
//
//            traitCollection = UITraitCollection(userInterfaceStyle: UIUserInterfaceStyle(rawValue: Settings.shared.theme) ?? .unspecified)
//        }
        
        // TODO: add the color to style
//        let bg = UIColor(red: 17.0/255.0, green: 20.0/255.0, blue: 33.0/255.0, alpha: 1)
        let bg = UIColor.navigationBGColor
        let bgSteps = UIColor(red: 80.0/255.0, green: 83.0/255.0, blue: 97.0/255.0, alpha: 1)
        
        PrimaryLabel.appearance(for: traitCollection).normalFont = UIFont(name: fontFamilySFPro.Regular, size: 32)!
        PrimaryLabel.appearance(for: traitCollection, whenContainedInInstancesOf:[InstructionsBannerView.self]).normalTextColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        PrimaryLabel.appearance(for: traitCollection, whenContainedInInstancesOf:[StepInstructionsView.self]).normalTextColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        

        SecondaryLabel.appearance(for: traitCollection).normalFont = UIFont(name: fontFamilySFPro.Regular, size: 26)!
        SecondaryLabel.appearance(for: traitCollection, whenContainedInInstancesOf: [InstructionsBannerView.self]).normalTextColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        SecondaryLabel.appearance(for: traitCollection,whenContainedInInstancesOf: [StepInstructionsView.self]).normalTextColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)

        ExitView.appearance(for: traitCollection).backgroundColor = .clear
        ExitView.appearance(for: traitCollection).borderWidth = 1.0
        ExitView.appearance(for: traitCollection).cornerRadius = 5.0
        ExitView.appearance(for:traitCollection).foregroundColor = .white
        ExitView.appearance(for: traitCollection).borderColor = .white

        LaneView.appearance(for: traitCollection,whenContainedInInstancesOf: [LanesView.self]).primaryColor = .white
        LaneView.appearance(for: traitCollection,whenContainedInInstancesOf: [LanesView.self]).secondaryColor = .lightGray
            
        DistanceLabel.appearance(for: traitCollection).unitFont = UIFont(name: fontFamilySFPro.Light, size: 16.0)!.adjustedFont
        DistanceLabel.appearance(for: traitCollection).valueFont = UIFont(name: fontFamilySFPro.Light, size: 24.0)!.adjustedFont
        DistanceLabel.appearance(for: traitCollection,whenContainedInInstancesOf: [InstructionsBannerView.self]).valueTextColor = .white
        DistanceLabel.appearance(for: traitCollection,whenContainedInInstancesOf: [InstructionsBannerView.self]).unitTextColor = .white

        TopBannerView.appearance(for: traitCollection).backgroundColor = bg
        InstructionsBannerView.appearance(for: traitCollection).backgroundColor = bg
        InstructionLabel.appearance(for: traitCollection).textColor = .white
//        BottomBannerView.appearance().backgroundColor = .orange
        
        LanesView.appearance(for: traitCollection).backgroundColor = UIColor(red: 50.0/255.0, green: 54.0/255.0, blue: 76.0/255.0, alpha: 1)
        
        NextBannerView.appearance(for: traitCollection).backgroundColor = bg
        
        NextInstructionLabel.appearance(for: traitCollection).normalFont = UIFont.systemFont(ofSize: 20, weight: .medium).adjustedFont
        NextInstructionLabel.appearance(for: traitCollection).normalTextColor = .white
        NextInstructionLabel.appearance(for: traitCollection,whenContainedInInstancesOf: [NextBannerView.self]).normalTextColor = .white
        NextInstructionLabel.appearance(for: traitCollection,whenContainedInInstancesOf: [NextBannerView.self]).textColorHighlighted = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        NextInstructionLabel.appearance(for: traitCollection,whenContainedInInstancesOf: [NextBannerView.self]).normalFont = UIFont(name: fontFamilySFPro.Bold, size: 16)!

        ManeuverView.appearance(for: traitCollection,whenContainedInInstancesOf: [InstructionsBannerView.self]).primaryColor = .white
        ManeuverView.appearance(for: traitCollection,whenContainedInInstancesOf: [InstructionsBannerView.self]).secondaryColor = .lightGray
        ManeuverView.appearance(for: traitCollection,whenContainedInInstancesOf: [LanesView.self]).primaryColor = .white
        ManeuverView.appearance(for: traitCollection,whenContainedInInstancesOf: [LanesView.self]).secondaryColor = .lightGray
        ManeuverView.appearance(for: traitCollection,whenContainedInInstancesOf: [NextBannerView.self]).primaryColor = .white
        ManeuverView.appearance(for: traitCollection,whenContainedInInstancesOf: [NextBannerView.self]).secondaryColor = .lightGray

        WayNameLabel.appearance().normalFont = UIFont(name: fontFamilySFPro.Bold, size: 16)!
        WayNameLabel.appearance(for: traitCollection).normalTextColor = UIColor(named: "roadNameTextColor")!
        //WayNameLabel.appearance().normalTextColor = #colorLiteral(red: 0.2235294118, green: 0.2470588235, blue: 0.3450980392, alpha: 1)
        WayNameView.appearance(for: traitCollection).backgroundColor = UIColor(named: "navigationRoadNameColor")
        WayNameView.appearance(for: traitCollection).borderColor = .clear
        
        UserPuckCourseView.appearance(for: traitCollection).puckColor = UIColor(named:"naviPuckColor")!
        UserPuckCourseView.appearance(for: traitCollection).shadowColor = .clear
        UserPuckCourseView.appearance(for: traitCollection).stalePuckColor = UIColor(named:"naviPuckBGColor")!.withAlphaComponent(0.2)
        UserPuckCourseView.appearance(for: traitCollection).fillColor = UIColor(named:"naviPuckBGColor")!
//        UserHaloCourseView.appearance().haloColor = #colorLiteral(red: 0.4745098054, green: 0.8392156959, blue: 0.9764705896, alpha: 0.5)
//        UserHaloCourseView.appearance().haloRingColor = #colorLiteral(red: 0.9098039269, green: 0.4784313738, blue: 0.6431372762, alpha: 1)
//        UserHaloCourseView.appearance().haloRadius = 100.0
        
        ResumeButton.appearance(for: traitCollection).alpha = 0.0
                
        // MARK: Steps
        StepInstructionsView.appearance(for: traitCollection).backgroundColor = bgSteps
//        StepListIndicatorView.appearance().gradientColors = [UIColor.red, UIColor.green]
        StepsBackgroundView.appearance(for: traitCollection).backgroundColor = bgSteps
        UITableView.appearance(for: traitCollection,whenContainedInInstancesOf: [StepsViewController.self]).backgroundColor = bgSteps
//        SeparatorView.appearance().backgroundColor = .red
        // This doesn't work since Mapbox using its own separatorView
//        UITableView.appearance(whenContainedInInstancesOf: [StepsViewController.self]).layoutMargins = .zero
//        UITableView.appearance(whenContainedInInstancesOf: [StepsViewController.self]).separatorInset = .zero
                
        DistanceLabel.appearance(for: traitCollection,whenContainedInInstancesOf: [StepInstructionsView.self]).normalTextColor = .white
        DistanceLabel.appearance(for: traitCollection,whenContainedInInstancesOf: [StepInstructionsView.self]).valueTextColor = .white
        DistanceLabel.appearance(for: traitCollection,whenContainedInInstancesOf: [StepInstructionsView.self]).unitTextColor = .white
        
        ManeuverView.appearance(for: traitCollection,whenContainedInInstancesOf: [StepInstructionsView.self]).primaryColor = .white
        ManeuverView.appearance(for: traitCollection,whenContainedInInstancesOf: [StepInstructionsView.self]).secondaryColor = .lightGray

        DismissButton.appearance(for: traitCollection).textColor = UIColor.brandPurpleColor
        DismissButton.appearance(for: traitCollection).textFont = UIFont(name: fontFamilySFPro.Bold, size: 26)! //UIFont.quickSandBold24()

        GenericRouteShield.appearance(for: traitCollection).foregroundColor = .white
        GenericRouteShield.appearance(for: traitCollection).borderColor = .white
        
        NavigationMapView.appearance(for: traitCollection).routeCasingColor = .clear
        // Dynamic color doesn't work for Traffic, please refer to Mapbox #281
//        NavigationMapView.appearance().trafficLowColor = UIColor(named: "trafficLowColor")!
//        NavigationMapView.appearance().trafficHeavyColor = UIColor(named: "trafficHeavyColor")!
//        NavigationMapView.appearance().trafficSevereColor = UIColor(named: "trafficSevereColor")!
//        NavigationMapView.appearance().trafficModerateColor = UIColor(named: "trafficModerateColor")!
//        NavigationMapView.appearance().trafficUnknownColor = UIColor(named: "trafficUnknownColor")!
//        NavigationMapView.appearance().alternativeTrafficLowColor = UIColor(named: "alternateTrafficLowColor")!
//        NavigationMapView.appearance().alternativeTrafficModerateColor = UIColor(named: "alternateTrafficModerateColor")!
//        NavigationMapView.appearance().alternativeTrafficHeavyColor = UIColor(named: "alternateTrafficHeavyColor")!
//        NavigationMapView.appearance().alternativeTrafficSevereColor = UIColor(named: "alternateTrafficSevereColor")!
//        NavigationMapView.appearance().alternativeTrafficUnknownColor = UIColor(named: "alternateTrafficUnknownColor")!
        NavigationMapView.appearance(for: traitCollection).trafficLowColor = UIColor.trafficLowColor
        NavigationMapView.appearance(for: traitCollection).trafficHeavyColor = UIColor.trafficHeavyColor
        NavigationMapView.appearance(for: traitCollection).trafficSevereColor = UIColor.trafficSevereColor
        NavigationMapView.appearance(for: traitCollection).trafficModerateColor = UIColor.trafficModerateColor
        NavigationMapView.appearance(for: traitCollection).trafficUnknownColor = UIColor.trafficUnknownColor
        NavigationMapView.appearance(for: traitCollection).alternativeTrafficLowColor = UIColor.alternateTrafficLowColor
        NavigationMapView.appearance(for: traitCollection).alternativeTrafficModerateColor = UIColor.alternatTrafficModerateColor
        NavigationMapView.appearance(for: traitCollection).alternativeTrafficHeavyColor = UIColor.alternateTrafficHeavyColor
        NavigationMapView.appearance(for: traitCollection).alternativeTrafficSevereColor = UIColor.alternateTrafficSevereColor
        NavigationMapView.appearance(for: traitCollection).alternativeTrafficUnknownColor = UIColor.alternateTrafficUnknownColor
    }
}


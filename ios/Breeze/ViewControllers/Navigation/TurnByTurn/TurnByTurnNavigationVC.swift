//
//  TurnByTurnNavigationVC.swift
//  Breeze
//
//  Created by Zhou Hao on 29/3/21.
//

import CoreLocation
import Foundation
import UIKit
import MapboxCoreNavigation
import MapboxDirections
import MapboxNavigation
import MapboxDirections
@_spi(Restricted) import MapboxMaps
import MapboxCoreMaps
import SnapKit
import Turf
import SwiftyBeaver
import Combine
import AVFAudio

// MARK: - delegate definition
protocol TurnByTurnNavigationVCDelegate: AnyObject {
    func onArrival()
    func goToParking()
    func onWalking(response: RouteResponse, screenshot: UIImage?) 
}

final class TurnByTurnNavigationVC: NavigationViewController {

    // MARK: - Constatns
    let xTrailing: CGFloat = 24

    // MARK: - Properties
    // MARK: - From RoutePlanningVC
    weak var endRouteDelegate: TurnByTurnNavigationVCDelegate?
    var topBannerVC: TopBannerViewController!
//    var instructionsVC: TurnByTurnInstructionsVC!
    var selectedAddress: BaseAddress!
    
    // MARK: Internal properties
    private var resumeButton: UserLocationButton!
//    private var feedbackButton: NewFeedbackButton!
    private var tempMapboxFeedbackButton:UIButton!
    
    #if DEMO
    private var notificationRemovalTimer:Timer! //only for Demo
    private var currentSpeedView: CustomSpeedLimitView!
    #endif
        
    internal lazy var pointAnnotationManager: PointAnnotationManager = {
        return navigationMapView?.mapView.annotations.makePointAnnotationManager()
    }()!
    
    var trackingTimer: Timer?

    lazy var endRouteView: UIView = {
        let h: CGFloat = Values.topPanelHeight + 20
        let offset = h + UIApplication.topSafeAreaHeight
        let view = UIView(frame: CGRect(x: 0, y: -offset, width: UIScreen.main.bounds.width, height: offset))
        view.backgroundColor = UIColor.endRouteBackgroundColor
        let img = UIImage(named: "arrivalDestinationPin")
        let imgView = UIImageView(image: img)
        let y = UIApplication.topSafeAreaHeight + h / 2 - 14
        imgView.frame = CGRect(x: 33, y: y - 4 , width: 42, height: 48)
        view.addSubview(imgView)
        let label = UILabel(frame: CGRect(x: 86, y: y, width: 260, height: 35))
        label.text = Constants.cpArrival
        label.textColor = .white
        label.font = UIFont.SFProBoldFont(size: 32)
        view.addSubview(label)
        view.showBottomShadow()
        self.view.addSubview(view)
        return view
    }()
    
    // Showing the end navigation view at the bottom when navigation ends
    lazy var endNavigationView: EndNavigationView = {
        let view = EndNavigationView(frame: .zero)
        self.view.addSubview(view)
        self.view.bringSubviewToFront(view)
        return view
    }()

    lazy var carparkEndRouteView: UIView = {
        let h: CGFloat = Values.topPanelHeight
        let offset = h + UIApplication.topSafeAreaHeight
        let view = UIView(frame: CGRect(x: 0, y: -offset, width: UIScreen.main.bounds.width, height: offset))
        view.backgroundColor = UIColor.endRouteBackgroundColor
        let img = UIImage(named: "carparkArrivalTopIcon")
        let imgView = UIImageView(image: img)
        let y = UIApplication.topSafeAreaHeight + 16
        imgView.frame = CGRect(x: 33, y: y , width: 57, height: 57)
        view.addSubview(imgView)
        let label = UILabel(frame: CGRect(x: 100, y: y, width: UIScreen.main.bounds.width - imgView.bounds.width - 40, height: 64))
        label.text = "You have reached \(viewModel?.destinationCarparkName ?? "")!"
        label.textColor = .white
        label.numberOfLines = 0
        label.font = UIFont.SFProBoldFont(size: 32)
        view.addSubview(label)
        view.showBottomShadow()
        label.sizeToFit()
        self.view.addSubview(view)
        return view
    }()
    
    private var disposables = Set<AnyCancellable>()
    weak var viewModel: TurnByTurnNavigationViewModel?
    var destAddress: AnalyticDestAddress?
    private var etaChildVC: UIViewController?
    private var parkingAvailableVC: UIViewController?
    
    private var carParkAvailability: UIViewController?
    private var carParkListVC: UIViewController?
    
    private var travelTimeList: UIViewController?
    
    var broadcastMessage: BroadcastMessageModel?
    var locationPinCarpark: Carpark?
    
    //  OBU trip summary
    var bookmarkId: String?
    var bookmarkName: String?
    
    var isCruiseMode: Bool?
            
    // MARK: - UIViewController life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SwiftyBeaver.info("Turn by turn navigation start in mobile")
        self.floatingButtons?.forEach() { button in
            button.isHidden = true
            tempMapboxFeedbackButton = button
        }
        self.showsSpeedLimits = false
        //self.showsReportFeedback = true
        //self.detailedFeedbackEnabled = true                
        
//        setupFeedbackButton()
        setupResumeButton()
        
        // Force crash for testing after 30
//        DispatchQueue.main.asyncAfter(deadline: .now() + 30) {
//            let numbers = [0]
//            let _ = numbers[1]
//        }
        self.annotatesIntersectionsAlongRoute = false
        self.delegate = self
                
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(updateCarplayConnectionStatus(_:)),
            name: .carplayConnectionDidChangeStatus,
            object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(navigationCameraStateDidChange(_:)),
                                               name: .navigationCameraStateDidChange,
                                               object: navigationMapView!.navigationCamera)
        NotificationCenter.default.addObserver(self, selector: #selector(confirmParkingAvailability), name: .yesParkingAvailableConfirmed, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showParkingSubmitPopup), name: .showSubmitParkingAvailablePopup, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(cancelParkingAvailable), name: .cancelParkingAvailable, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(startWalkingPath), name: .startWalkingPath, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(backToHomePage), name: .backToHomePage, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(didReachToCarpark), name: .endNavigationWhenReachToCarpark, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(showCarParkList), name: .showCarParkList, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showTravelTimeList), name: .showTravelTimeList, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(onCloseTravelTime), name: .obuOnCloseTravelTime, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(onCloseCarParkAvailability), name: .obuOnCloseCarparkAvailability, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(onCloseCarparkList), name: .onCloseCarparkListScreen, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(selectCarparkFromCarParkList(notification:)), name: .selectCarparkFromObuDisplay, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(navigateToAlternateCarpark(notification:)), name: .navigateToAlternateCarpark, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(parkingAvailabilityAlertDismissed(notification:)), name: .parkingAvailabilityAlertDismissed, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(onOBUDisconnected), name: .obuDisconnected, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(onNoDataAlert(notification: )), name: .onNoDataNotification, object: nil)
        
//        NotificationCenter.default.addObserver(
//            self,
//            selector: #selector(updateShowFullListStepTable(_:)),
//            name: .carplayConnectionShowFullListStepTable,
//            object: nil)
        
        NotificationCenter.default.publisher(for: Notification.Name(Values.NotificationCloseNotifyArrivalScreen), object: nil)
            .receive(on: DispatchQueue.main)
            .sink { [weak self] notification in
                guard let self = self, let vc = self.etaChildVC else { return }
                print("Close Notify")
                self.removeChildViewController(vc)
                self.etaChildVC = nil
            }.store(in: &disposables)
        
        NotificationCenter.default.publisher(for: Notification.Name(Values.NotificationShowToast), object: nil)
            .receive(on: DispatchQueue.main)
            .sink { [weak self] notification in
                guard let self = self else { return }
                
                if let info = notification.userInfo as? NSDictionary {
                    guard let message = info["message"] as? String else {
                        return
                    }
                    self.showToastFromRN(message)
                }
            }.store(in: &disposables)

//        NotificationCenter.default.publisher(for: Notification.Name(Values.NotificationETASetUp), object: nil)
//            .receive(on: DispatchQueue.main)
//            .sink { [weak self] notification in
//                guard let self = self else { return }
//                self.receiveETA(notification)
//            }.store(in: &disposables)
        
        
        // Disable the voice instruction in mobile
        if let synthesizer = voiceController.speechSynthesizer as? MultiplexedSpeechSynthesizer {
            synthesizer.speechSynthesizers = []
        }
        
        toggleDarkLightMode()
                
        // #273 - suggested by Ochi
        NavigationSettings.shared.distanceUnit = .kilometer
        navigationMapView?.mapView?.gestures.delegate = self
        navigationMapView?.mapView?.gestures.options.pitchEnabled = false
        navigationMapView?.mapView.ornaments.options.attributionButton.visibility = .hidden
        
        appDelegate().$navigationViewModel
            .receive(on: DispatchQueue.main)
            .sink { [weak self] viewModel in
                guard let self = self else { return }
                
                if let vm = viewModel {
                    self.didSetNavigationViewModel(vm)
                } else {
                    // only dismiss if previousAppState is navigation
                    if case .navigation = appDelegate().previousAppState {
                        // end from CarPlay
                        #if DEMO
                          if(DemoSession.shared.isDemoRoute){
                              if(DemoSession.shared.status == .inFirstLeg)
                              {
                                  DemoSession.shared.status = .firstLegComplete
                              }
                              
                              if(DemoSession.shared.status == .inSecondLeg)
                              {
                                  DemoSession.shared.status = .none
                              }
                              
                              self.dismiss(animated: false) {
                                  self.navigationService.stop()
                                  self.endRouteDelegate?.goToParking()
                              }
                              
                          }
                          else{
                              self.viewModel?.clearAudioQueue()
                              self.viewModel?.stopSpeakingAudioInstructions()
                              self.endRouteDelegate?.onArrival()
                              self.dismiss(animated: false, completion: nil)
                          }
                        #else
                          self.viewModel?.clearAudioQueue()
                          self.viewModel?.stopSpeakingAudioInstructions()
                          self.endRouteDelegate?.onArrival()
                          self.dismiss(animated: false, completion: nil)
                        #endif
                      
                        
                    }
                }
        }.store(in: &self.disposables)
        
        navigationMapView?.mapView.mapboxMap.onEvery(event: .cameraChanged) { [weak self] _ in
            guard let self = self, let viewmodel = self.viewModel else { return }
            if let navigationMapView = self.navigationMapView {
                viewmodel.updateTooltipsPosition(naviMapView: navigationMapView)
            }
            
        }
        
        if OBUHelper.shared.hasLastObuData() {
            AnalyticsManager.shared.logSystemEvent(eventValue: ParameterName.SystemAnalytics.SystemEvent.bottom_obu_card_shown, screenName: ParameterName.NavigationMode.screen_view)
        }
        
//        if appDelegate().carPlayConnected {
//            self.viewModel?.callApiGetCarpark()
//        }
        
    }
    
    
    private func didSetNavigationViewModel(_ vm: TurnByTurnNavigationViewModel) {
        
        self.navigationMapView?.mapView.mapboxMap.onNext(event: .mapLoaded) { [weak self] _ in
            guard let self = self else { return }
            
            vm.mobileMapViewUpdatable = MobileNavigationMapViewUpdatable(navigationMapView: self.navigationMapView!, topBanner: self.topBannerVC)
            vm.mobileMapViewUpdatable?.setRouteLineWidth(multipliedBy: 1.5)
            
            //Hide the share drive button if destionation is very close to the origin, and if user starts the navigation it will immediately show arrival
            if(vm.navigationService.route.distance < 50){
                
                vm.hideShareDriveButton(false)
            }
            vm.reRouteIfNeeded()
            
            if self.isCruiseMode == true {
                DispatchQueue.main.async { 
                    self.showToastRoutingToCarpark()
                }
            }
//            vm.notifyOnEvAmenites = { [weak self] in
//                guard let self = self else { return }
////                self.zoomOutPetrolandEvAmenity()
//            }
////
//            vm.notifyOnPetrolAmenites = { [weak self] in
//                guard let self = self else { return }
////                self.zoomOutPetrolandEvAmenity()
//            }
//
            // update the eta action here
            vm.notifyArrivalAction = { [weak self] in
                guard let self = self else { return }
                
                let destInfo = vm.getDestinationInfo()
                self.etaChildVC = showShareDriveScreen(viewController: self, address: destInfo.address, etaTime: destInfo.arrivalTime,fromScreen: "navigation")
            }
            
            //  Show steps table here
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                if appDelegate().carPlayConnected {
                    self.topBannerVC?.displayStepsTable()
                }
            }
            
        }
        
//        if let planTrip = self.planTrip {
//            let routeStopDetails = AmenitiesSharedInstance.shared.getExistingStopsAdded(plan: planTrip,themeType: vm.amenitySymbolThemeType)
//            if let wayPoints = routeStopDetails.wayPoints{
//                vm.otherWayPoints.append(contentsOf: wayPoints)
//            }
//            if let wayPointFeatures = routeStopDetails.wayPointFeatures {
//                vm.wayPointFeatures = wayPointFeatures
//                for feature in vm.wayPointFeatures {
//                    if case let .string(featureType) = feature.properties?["type"]{
//                        self.addWayPointEVandPetrolToMap(type: featureType)
//                    }
//                }
//            }
//
//        }
        
        self.viewModel = vm
        self.viewModel?.analyticsDest = self.destAddress
        self.viewModel?.broadcastPlanning = self.broadcastMessage
        self.viewModel?.bookmarkId = bookmarkId
        self.viewModel?.bookmarkName = bookmarkName
    }
        
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.topBannerVC.tapOnTopBanner = { [weak self] in
            self?.showAnimateNotAvailable()
        }

        setupDarkLightAppearance()
        
        //Vishnu - Fix Me - Once route Line disappear fixe from Map box on github #121, we will enable this again
        toggleDarkLightMode()
    }
    
    private func dismissStepsTable() {
        if appDelegate().carPlayConnected {
            self.topBannerVC?.shouldShowInstructionFullList = false
            self.topBannerVC?.dismissStepsTable()
        }
    }
    
    private func showAnimateNotAvailable() {
        
        AnalyticsManager.shared.logOpenPopupEventV2(popupName: ParameterName.NavigationMode.UserPopup.projected_to_dhu, screenName: ParameterName.NavigationMode.screen_view)
        
        let storyboard = UIStoryboard(name: "Navigation", bundle: nil)
        if let vc = storyboard.instantiateViewController(withIdentifier: String(describing: MobileNavigationNotAvailableVC.self)) as? MobileNavigationNotAvailableVC {
            vc.modalPresentationStyle = .overFullScreen
            self.present(vc, animated: true)
        }
    }
    
    deinit {
        SwiftyBeaver.info("TurnByTurnNavigationVC deinit")
        NotificationCenter.default.removeObserver(self)
//        self.viewModel?.stopTimerUpdateZoomLevel()
        self.viewModel = nil
        self.endNavigationView.removeFromSuperview()
        self.endRouteView.removeFromSuperview()
        appDelegate().enterHome()
    }
    
    //MARK: - Custom turn by turn
    /*
    func showCustomTurnByTurnList() {
        if self.instructionsVC == nil {
            self.instructionsVC = TurnByTurnInstructionsVC()
            self.instructionsVC.delegate = self
            self.addChildViewControllerWithView(childViewController: instructionsVC, toView: self.view)
            self.view.bringSubviewToFront(self.instructionsVC.view)
        }
    }
     */
    
    //MARK: - ETA SetUp
    @objc func receiveETA(_ notification: Notification) {
        
        if let dictionary = notification.userInfo {
            let name = dictionary[Values.etaContactName] as! String
            let number = dictionary[Values.etaPhoneNumber] as! String
            let message = dictionary[Values.etaMessage] as! String
            let favoriteId = dictionary[Values.etaFavoriteId] as! Int
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }

                print("ETA received")
                self.viewModel?.hideShareDriveButton(false)
                self.viewModel?.addETA(recipientName: name, recipientNumber: number, message: message, etaFavoriteId: favoriteId)
            }
        }
    }
    
    private func popupPrompt(title: String, message: String, actionTitles:[String?], actions:[((UIAlertAction) -> Void)?]) {
        
        let newMessage = "\n\(message)"
        let alert = UIAlertController(title: title, message: newMessage, preferredStyle: .alert)
        
        let attrTitle = NSMutableAttributedString(string: title, attributes:[
            NSAttributedString.Key.font: UIFont.rerouteTitleRegularFont()
        ])

        let attrMessage = NSMutableAttributedString(string: newMessage, attributes:[
            NSAttributedString.Key.font: UIFont.rerouteMessageBoldFont()
        ])

        alert.setValue(attrTitle, forKey: "attributedTitle")
        alert.setValue(attrMessage, forKey: "attributedMessage")
        
        for (index, title) in actionTitles.enumerated() {
            let action = UIAlertAction(title: title, style: .default, handler: actions[index])
            if(title == Constants.reject) {
                action.setValue(UIColor.clearSearchAlertButtonColor, forKey: "titleTextColor")
            }
            alert.addAction(action)
        }
        self.present(alert, animated: true) {
            DispatchQueue.main.asyncAfter(deadline: .now() + 10) {
                alert.dismiss(animated: true, completion: nil)
            }
        }
    }

    private func toggleDarkLightMode() {
        
        //Vishnu - As map box suggested on #121 https://github.com/mapbox-collab/ncs-collab/issues/121#issuecomment-872738336, using styleManager is the right way to set styles on the map
        styleManager.styles = isDarkMode ? [CustomNightStyle()] : [CustomDayStyle()]
        viewModel?.amenitySymbolThemeType = isDarkMode ? Values.DARK_MODE_UN_SELECTED : Values.LIGHT_MODE_UN_SELECTED
        
        sendThemeChangeEvent(isDarkMode)
    }

    // MARK: - Theme updated
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        guard previousTraitCollection != traitCollection else {
            return
        }
       toggleDarkLightMode()
    }
    
//    private func setupFeedbackButton(){
//
//        let feedbackBtn = NewFeedbackButton(buttonSize: 48)
//        feedbackBtn.addTarget(self, action: #selector(feedbackIssue), for: .touchUpInside)
//
//        // Setup constraints such that the button is placed within
//        // the upper left corner of the view.
//        feedbackBtn.translatesAutoresizingMaskIntoConstraints = false
//        navigationMapView?.mapView.addSubview(feedbackBtn)
//
//        self.feedbackButton = feedbackBtn
//
//        // setup autolayout
//        feedbackButton.snp.makeConstraints { (make) in
//            make.trailing.equalToSuperview().offset(-xTrailing)
//            make.bottom.equalTo(self.navigationMapView!.snp.bottom).offset(-(160+UIApplication.bottomSafeAreaHeight))
//            make.height.equalTo(48)
//            make.width.equalTo(48)
//        }
//
//    }
    
    private func setupResumeButton() {
        
        let userLocationButton = UserLocationButton(buttonSize: (Images.locationImage?.size.width)!)
        userLocationButton.addTarget(self, action: #selector(resumeButtonTapped), for: .touchUpInside)
        
        userLocationButton.translatesAutoresizingMaskIntoConstraints = false
        navigationMapView?.mapView.addSubview(userLocationButton)
        
        self.resumeButton = userLocationButton
        userLocationButton.isHidden = true // hide it by default
        
        let additionalCarparkListHeight:CGFloat = 78
        
        // setup autolayout
        userLocationButton.snp.makeConstraints { (make) in
            make.trailing.equalToSuperview().offset(-xTrailing)
            
            if OBUHelper.shared.hasLastObuData() {
                if OBUHelper.shared.isObuConnected() {
                    make.bottom.equalTo(self.navigationMapView!.snp.bottom).offset(-(160+UIApplication.bottomSafeAreaHeight+40+additionalCarparkListHeight))
                } else {
                    make.bottom.equalTo(self.navigationMapView!.snp.bottom).offset(-(160+UIApplication.bottomSafeAreaHeight+64+additionalCarparkListHeight))
                }
            } else {
                make.bottom.equalTo(self.navigationMapView!.snp.bottom).offset(-(160+UIApplication.bottomSafeAreaHeight+additionalCarparkListHeight))
            }
            
//            make.bottom.equalTo(self.feedbackButton.snp.top).offset(-10)
            make.height.equalTo(48)
            make.width.equalTo(48)
        }
    }
    
    @objc private func resumeButtonTapped() {
        
//        viewModel?.checkStopTimerZoomLevelIfNeeded()
        
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Navigation.UserClick.recenter, screenName: ParameterName.Navigation.screen_view)
        navigationMapView?.navigationCamera.follow()
        
        DispatchQueue.main.async { [weak self] in
            guard let self = self, let viewmodel = self.viewModel else { return }
            viewmodel.dismissTooltips()
        }
    }
    
    @objc func updateCarplayConnectionStatus(_ notification: NSNotification) {
        if let status = notification.object as? Bool {
            if viewModel?.isDestinationReached == true {
                topBannerVC.shouldShowInstructionFullList = false
                self.topBannerVC.dismissStepsTable()
            } else {
                topBannerVC.shouldShowInstructionFullList = appDelegate().carPlayConnected
                if status {
                    self.topBannerVC.displayStepsTable()
                } else {
                    self.topBannerVC.dismissStepsTable()
                }
            }
        }
    }
    
    
//    @objc func updateShowFullListStepTable(_ notification: NSNotification) {
//        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
//            if let status = notification.object as? Bool {
//                if status {
//                    self.topBannerVC.shouldShowInstructionFullList = status
//                    self.topBannerVC.displayStepsTable()
//                } else {
//                    self.topBannerVC.shouldShowInstructionFullList = status
//                    self.topBannerVC.dismissStepsTable()
//                }
//            }
//        }
//    }
    
    
//    @objc func feedbackIssue(){
//        
//        let storyboard: UIStoryboard = UIStoryboard(name: "Feedback", bundle: nil)
//        let vc: FeedbackVC = storyboard.instantiateViewController(withIdentifier: "FeedbackVC")  as! FeedbackVC
//        vc.modalPresentationStyle = .formSheet
//        vc.locCoordinate = self.navigationMapView?.mapView.location.latestLocation?.coordinate
//        vc.navigationService = self.navigationService
//        vc.feedBack = navigationService.eventsManager.createFeedback()
//        self.present(vc, animated: true, completion: nil)
//    }
    
    func didDeselectAnnotation() {
        viewModel?.dismissTooltips()
    }
    
    func adjustCameraWhenAmenityClicked(at location: LocationCoordinate2D, callout: ToolTipsView) {
        let cameraOptions = CameraOptions(center: location, padding:UIEdgeInsets(top: 200, left: 10, bottom: 10, right: 10), zoom: self.navigationMapView?.mapView.mapboxMap.cameraState.zoom) // using current zoom level
        
        self.navigationMapView?.mapView.camera.ease(to: cameraOptions, duration: Values.standardAnimationDuration, curve: .easeInOut, completion: nil)
        if let navigationMapView = self.navigationMapView {
            callout.adjust(to: location, in: navigationMapView)
        }
    }
    
    func removeWayPoints(location:CLLocationCoordinate2D,sName:String,feature:Turf.Feature,type:String){
        if(viewModel?.routeStopsAdded.count ?? 0 > 0) {
            if let index = viewModel?.routeStopsAdded.firstIndex(where: {$0.name == sName}){
                viewModel?.routeStopsAdded.remove(at: index)
                viewModel?.wayPointFeatures.remove(at: index)
            }
        }
//        if let navigationMapView = self.navigationMapView {
//            navigationMapView.removeWaypoints()
//            navigationMapView.removeRoutes()
//        }
        viewModel?.removeLayerTypeFrom(type: type)
        addWayPointEVandPetrolToMap(type: type)
    }
    
 
    
    
    //MARK: - AddingWaypoints to the existing route
    func addWayPoints(location:CLLocationCoordinate2D,sName:String,feature:Turf.Feature,type:String){
        
        let wayPoint = Waypoint(coordinate: location, coordinateAccuracy: -1, name: sName)
        wayPoint.separatesLegs = Values.wayPointSeparateLegs

        viewModel?.routeStopsAdded.append(wayPoint)
        let names = viewModel?.routeStopsAdded.map({ waypoint in
            return waypoint.name
        })
        print("routeStopsAdded: \(names)")
//        viewModel?.otherWayPoints.append(wayPoint)
        viewModel?.wayPointFeatures.append(feature)
        
//        if let navigationMapView = self.navigationMapView {
//            navigationMapView.removeWaypoints()
//            navigationMapView.removeRoutes()
//        }
        addWayPointEVandPetrolToMap(type: type)
    }
    
    func openEvChargerToolTipView(feature:Turf.Feature,location:CLLocationCoordinate2D,tapped: Bool = false) {
        if let selectedFeatureProperties = feature.properties,
           case let .string(type) = selectedFeatureProperties["type"],
           case let .string(name) = selectedFeatureProperties["name"],
           case let .string(address) = selectedFeatureProperties["address"],
           case let .string(stationName) = selectedFeatureProperties["stationName"],
           case let .string(amenityID) = selectedFeatureProperties["amenityID"],
           case let .array(plugTypes) = selectedFeatureProperties["plugTypes"]
        {
           
            let isSameAmenity = amenityID == self.viewModel?.petrolAndEVCallOutView?.tooltipId
            
            self.didDeselectAnnotation()
            
            if isSameAmenity {
                return
            }
            
            var arrayOfPlugTypes = Array<String>()
            for type in plugTypes {
                
                if case let .string(name) = type{
                    
                    arrayOfPlugTypes.append(name)
                }
            }
  
            if let navigationMapView = self.navigationMapView {
                let amenitySymbol = self.viewModel?.amenitySymbolThemeType
                                
                let isAdded = viewModel?.routeStopsAdded.contains(where: { waypoint in
                    return waypoint.name == amenityID
                }) ?? false
                
                let names = viewModel?.routeStopsAdded.map({ waypoint in
                    return waypoint.name
                })
                print("routeStopsAdded: \(names), name: \(amenityID), isAdded: \(isAdded)")
                
                let isDisableAddStop = viewModel?.routeStopsAdded.count ?? 0 >= 3
                
                let evChargerToolTipView = EVChargerTooltipV2(id: amenityID, name: name, address: address, location: location, isAdding: !isAdded, isDisableAddStop: isDisableAddStop)
                evChargerToolTipView.showShadow = false
                evChargerToolTipView.present(from: navigationMapView.mapView.frame, in: navigationMapView.mapView, constrainedTo: evChargerToolTipView.frame, animated: true)
                evChargerToolTipView.center = self.view.center
//                self.view.bringSubviewToFront(evChargerToolTipView)
                viewModel?.petrolAndEVCallOutView = evChargerToolTipView
                evChargerToolTipView.adjust(to: location, in: navigationMapView)
                self.adjustCameraWhenAmenityClicked(at: location, callout: evChargerToolTipView)
                evChargerToolTipView.onAdd = { [weak self] (address,coordinate) in
                    guard let self = self else { return }
                    
                    let location = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
                    if tapped {
                        let cameraOptions = CameraOptions(center: CLLocationCoordinate2D(latitude: coordinate.latitude, longitude: coordinate.longitude ), zoom: navigationMapView.mapView.mapboxMap.cameraState.zoom)
                        navigationMapView.mapView.camera.ease(to: cameraOptions, duration: Values.standardAnimationDuration, curve: .easeInOut, completion: nil)
                    }
                    
                    AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.NavigationMode.UserClick.map_tooltip_ev_add_stop, screenName: ParameterName.NavigationMode.screen_view)
                    
                    if(self.viewModel?.routeStopsAdded.count ?? 0 < 3) {
                        var feature = Turf.Feature(geometry: .point(Point(location.coordinate)))
                        feature.properties = [
                            "type":.string(type),
                            "name":.string(name),
                            "address":.string(address),
                            "stationName":.string(stationName),
                            "image_id":.string("\(amenitySymbol ?? "")\(type)"),
                            "amenityID":.string(amenityID),
                            "plugTypes":.array(plugTypes)
                        ]
                       
                        
                        self.addWayPoints(location: location.coordinate, sName:amenityID,feature:feature, type:type)
                        if let navigationMapView = self.viewModel?.navigationService {
                            if let locationSrouce = navigationMapView.router.location {
                                self.viewModel?.rerouteEVandPetrol(source: locationSrouce.coordinate, destName: address, type: type)
                            }
                        }
                        self.viewModel?.dismissTooltips()
                    }
                }
                
                evChargerToolTipView.onRemove = { [weak self] (address,coordinate) in
                    guard let self = self else { return }
                   
                    let location = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
                    
                    if(self.viewModel?.routeStopsAdded.count ?? 0 <= 3){
                        
                        var feature = Turf.Feature(geometry: .point(Point(location.coordinate)))
                        feature.properties = [
                            "type":.string(type),
                            "name":.string(name),
                            "address":.string(address),
                            "stationName":.string(stationName),
                            "image_id":.string("\(amenitySymbol ?? "")\(type)"),
                            "amenityID":.string(amenityID),
                            "plugTypes":.array(plugTypes)
                        ]
                    
                        self.removeWayPoints(location: location.coordinate, sName:amenityID,feature:feature, type:type)
                        if let navigationMapView = self.viewModel?.navigationService {
                            if let locationSrouce = navigationMapView.router.location {
                                self.viewModel?.rerouteEVandPetrol(source: locationSrouce.coordinate, destName: address, type: type)
                            }
                        }
                        self.viewModel?.dismissTooltips()
                    }
                    
                }
            }
        }
    }
    
    
    
    
    func openPetrolToolTipView(feature:Turf.Feature,location:CLLocationCoordinate2D, tapped: Bool = false){
        
        if let selectedFeatureProperties = feature.properties,
           case let .string(name) = selectedFeatureProperties["name"],
           case let .string(address) = selectedFeatureProperties["address"],
           case let .string(type) = selectedFeatureProperties["type"],
           case let .string(stationName) = selectedFeatureProperties["stationName"],
           case let .string(amenityID) = selectedFeatureProperties["amenityID"]
        {
            
            if let navigationMapView = self.navigationMapView {
                let amenitySymbol = self.viewModel?.amenitySymbolThemeType
                let isSameAmenity = amenityID == viewModel?.petrolAndEVCallOutView?.tooltipId
                
                self.didDeselectAnnotation()
                
                if isSameAmenity {
                    return
                }
                
                let isAdded = viewModel?.routeStopsAdded.contains(where: { waypoint in
                    return waypoint.name == amenityID
                }) ?? false
                
                let names = viewModel?.routeStopsAdded.map({ waypoint in
                    return waypoint.name
                })
                print("routeStopsAdded: \(names), name: \(amenityID), isAdded: \(isAdded)")
                
                let isDisableAddStop = viewModel?.routeStopsAdded.count ?? 0 >= 3
                
                let petrolToolTip: PetrolToolTipsView = PetrolToolTipsView(id: amenityID, name: name, address: address, location: location, isAdding: !isAdded, isShowButton: isDisableAddStop)
                petrolToolTip.showShadow = false
                petrolToolTip.present(from: navigationMapView.mapView.frame, in: navigationMapView.mapView, constrainedTo: petrolToolTip.frame, animated: true)
                petrolToolTip.center = self.view.center
                viewModel?.petrolAndEVCallOutView = petrolToolTip
                petrolToolTip.adjust(to: location, in: navigationMapView)
                self.adjustCameraWhenAmenityClicked(at: location, callout: petrolToolTip)
                petrolToolTip.onAdd = { [weak self] (address,coordinate) in
                    guard let self = self else { return }
                    let location = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
                    
                    if tapped {
                        let cameraOptions = CameraOptions(center: CLLocationCoordinate2D(latitude: coordinate.latitude, longitude: coordinate.longitude ), zoom: navigationMapView.mapView.mapboxMap.cameraState.zoom)
                        navigationMapView.mapView.camera.ease(to: cameraOptions, duration: Values.standardAnimationDuration, curve: .easeInOut, completion: nil)
                    }
                    
                    AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.NavigationMode.UserClick.map_tooltip_petrol_add_stop, screenName: ParameterName.NavigationMode.screen_view)
                    
                    /// Added only 3 route stops
                    if(self.viewModel?.routeStopsAdded.count ?? 0 < 3){
                        
                        var feature = Turf.Feature(geometry: .point(Point(location.coordinate)))
                        feature.properties = [
                            "type":.string(type),
                            "name":.string(name),
                            "address":.string(address),
                            "stationName":.string(stationName),
                            "image_id":.string("\(amenitySymbol ?? "")\(type)"),
                            "amenityID":.string(amenityID)
                        ]
                        
                        
                        self.addWayPoints(location: location.coordinate, sName:amenityID,feature:feature, type:type)
                        if let navigationMapView = self.viewModel?.navigationService {
                            if let locationSrouce = navigationMapView.router.location {
                                self.viewModel?.rerouteEVandPetrol(source: locationSrouce.coordinate, destName: address, type: type)
                            }
                        }
                        self.viewModel?.dismissTooltips()
                        
                    }
                    
                }
                
                petrolToolTip.onRemove = { [weak self] (address,coordinate) in
                    guard let self = self else { return }
                    let location = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
                    /// remove stop
                    if(self.viewModel?.routeStopsAdded.count ?? 0 <= 3){
                        
                        var feature = Turf.Feature(geometry: .point(Point(location.coordinate)))
                        feature.properties = [
                            "type":.string(type),
                            "name":.string(name),
                            "address":.string(address),
                            "stationName":.string(stationName),
                            "image_id":.string("\(amenitySymbol ?? "")\(type)"),
                            "amenityID":.string(amenityID)
                        ]
                        self.removeWayPoints(location: location.coordinate, sName:amenityID,feature:feature, type:type)
                        if let navigationMapView = self.viewModel?.navigationService {
                            if let locationSrouce = navigationMapView.router.location {
                                self.viewModel?.rerouteEVandPetrol(source: locationSrouce.coordinate, destName: address, type: type)
                            }
                        }
                        self.viewModel?.dismissTooltips()
                    }
                    
                }
            }
        }
    }
    
    
    func querySymbolLayerAmenityEvAndPetrol(point:CGPoint, completion: @escaping ((_ selected: Bool)-> Void)){
        if let navigationMapView = self.navigationMapView {
            var layerID = [String]()
            layerID.append("_\(Values.PETROL)")
            //        layerID.append("_\(amenitySymbolThemeType)\(Values.EV_CHARGER)")
            layerID.append("_\(Values.EV_CHARGER)")
            if(layerID.count > 0){
                
                let queryOptions = RenderedQueryOptions(layerIds: layerID, filter: nil)
                //let Zone2NullOptions = RenderedQueryOptions(layerIds: ["zone2-null"], filter: nil)
                navigationMapView.mapView.mapboxMap.queryRenderedFeatures(at: point, options: queryOptions) { [weak self] (result) in
                    
                    guard let self = self else { return }
                    
                    switch result {
                        
                    case .success(let features):
                        
                        // Return the first feature at that location, then pass attributes to the alert controller.
                        // self.navMapView.mapView.mapboxMap.style.updateImageSource(withId: <#T##String#>, image: <#T##UIImage#>)
                        
                        if let selectedFeatureProperties = features.first?.feature.properties,
                           let feature = features.first?.feature,
                           case let .point(point) = feature.geometry {
                            if case let .string(type) = selectedFeatureProperties["type"] {
                                if(type == Values.EV_CHARGER){
                                    self.viewModel?.startTimer()
                                    self.openEvChargerToolTipView(feature: feature,location:point.coordinates, tapped: true)
                                    completion(true)
                                }
                                else if (type == Values.PETROL){
                                    self.viewModel?.startTimer()
                                    self.openPetrolToolTipView(feature:feature,location:point.coordinates, tapped: true)
                                    completion(true)
                                }
                            }
                            
                        } else {
                            // If the feature is a cluster, it will have `point_count` and `cluster_id` properties. These are assigned
                            // when the cluster is created.
                            self.didDeselectAnnotation()
                            completion(false)
                        }
                        
                    case .failure(_):
                        completion(false)
                        break
                    }
                }
            } else {
                completion(false)
            }
        }
        
    }
    
    
    private func addWayPointEVandPetrolToMap(type:String) {
        if let viewModel = self.viewModel {
            if(viewModel.wayPointFeatures.count > 0){
                var turfArray = [Turf.Feature]()
                for feature in viewModel.wayPointFeatures {
                    if case let .string(featureType) = feature.properties?["type"]{
                        if(type == featureType){
                            turfArray.append(feature)
                        }
                    }
                }
                if(turfArray.count > 0){
                    let amenityFeatureCollection = FeatureCollection(features: turfArray)
                    viewModel.updateAmenitiesOnMap(collection: amenityFeatureCollection,type: "\(type)")
                }
            }
        }
    }

    func checkRemoveAddedStop() {
        if let viewModel = self.viewModel {
            if(viewModel.wayPointFeatures.count > 0){
                for index in (0..<viewModel.wayPointFeatures.count).reversed() {
                    let feature = viewModel.wayPointFeatures[index]
                    
                    if let selectedFeatureProperties = feature.properties,
                       case let .string(type) = selectedFeatureProperties["type"],
                       case let .string(address) = selectedFeatureProperties["address"],
                       case let .string(amenityID) = selectedFeatureProperties["amenityID"]
                    {
                        
                        if let currentLocation = viewModel.lastLocation?.coordinate {
                            switch feature.geometry {
                            case .point(let point):
                                let distance = currentLocation.distance(to: point.coordinates)
                                if distance <= 50 {
                                    self.removeWayPoints(location: point.coordinates, sName:amenityID ,feature:feature, type:type)
//                                    viewModel.routeStopsAdded.remove(at: index)
                                    viewModel.rerouteEVandPetrol(source: currentLocation, destName: address, type: type)
                                    SwiftyBeaver.debug("updatte < 50\(distance)")
                                }
                            default:
                                break
                            }
                        }
                    }
                }
            }
        }
    }
    
    
}

extension TurnByTurnNavigationVC: NavigationViewControllerDelegate {
    
//    func navigationViewController(_ navigationViewController: NavigationViewController, routeLineLayerWithIdentifier identifier: String, sourceIdentifier: String) -> LineLayer? {
//        var lineLayer = LineLayer(id: identifier)
//        lineLayer.source = sourceIdentifier
//        lineLayer.lineColor = .constant(.init(#colorLiteral(red: 0.3647058823, green: 0.6549019607, blue: 1.0, alpha: 1)))
//        lineLayer.lineWidth = .expression(lineWidthExpression(1.3))
//        lineLayer.lineJoin = .constant(.round)
//        lineLayer.lineCap = .constant(.round)
//        return lineLayer
//    }
     
//    func navigationViewController(_ navigationViewController: NavigationViewController, routeCasingLineLayerWithIdentifier identifier: String, sourceIdentifier: String) -> LineLayer? {
//        var lineLayer = LineLayer(id: identifier)
//        lineLayer.source = sourceIdentifier
//            
//        lineLayer.lineColor = .constant(.init(identifier.contains("main") ? #colorLiteral(red: 0.1843137255, green: 0.4784313725, blue: 0.7764705882, alpha: 1) : #colorLiteral(red: 0.4, green: 0.4, blue: 0.4, alpha: 1)))
//        lineLayer.lineWidth = .expression(lineWidthExpression(1.3))
//        lineLayer.lineJoin = .constant(.round)
//        lineLayer.lineCap = .constant(.square)
//         
//        return lineLayer
//    }
            
    func navigationViewController(_ navigationViewController: NavigationViewController, didAdd finalDestinationAnnotation: PointAnnotation, pointAnnotationManager: PointAnnotationManager){
            //let point = finalDestinationAnnotation.feature.geometry.value as! Point
        let point = finalDestinationAnnotation.point
        var destinationAnnotation = PointAnnotation(coordinate: point.coordinates)
        destinationAnnotation.image = .init(image: UIImage(named: "destinationMark")!, name: "navigationDest")

        navigationViewController.navigationMapView!.pointAnnotationManager?.annotations = [destinationAnnotation]   
            
        }
        
    func navigationViewController(_ navigationViewController: NavigationViewController, didUpdate progress: RouteProgress, with location: CLLocation, rawLocation: CLLocation) {
        //No need to handle this, as we are handling route progress from model class
//        if let in1Km = viewModel?.lessThan1kmFromDestination, !in1Km, progress.distanceRemaining <= 1000 {
//            self.resumeButton.isHidden = false
//        }
        checkRemoveAddedStop()
    }
            
    func navigationViewController(_ navigationViewController: NavigationViewController, didArriveAt waypoint: Waypoint) -> Bool {
                
        if navigationViewController.navigationService.routeProgress.isFinalLeg {
            if let viewModel = self.viewModel, viewModel.shouldShowCarparkArrival {
                
                //  TODO Handle show carpark status first then show arrive popup after that
//                showArriveCarpark()
                DispatchQueue.main.async {
                    self.dismissStepsTable()
                }
                self.viewModel?.confirmParkingAvailable()
                viewModel.clearAudioQueue()
                return false
            } else {
                
                //If any carpark layer presented on the map, we remove once arrived
                self.navigationMapView?.mapView.removeCarparkLayer()
//                viewModel?.clearAudioQueue()
                endRoute()
            }
            
            DispatchQueue.main.async { [weak self] in
                
                guard let self = self else { return }
                
               //Adding this based on this ticket suggestion from map box https://github.com/mapbox-collab/ncs-collab/issues/546#issuecomment-1182619982
                self.viewModel?.clearNavigationEnd(controller: self)
            }
            return false
        }
        return false
    }
    
    func navigationViewControllerDidDismiss(_ navigationViewController: NavigationViewController, byCanceling canceled: Bool) {
//        self?.dismiss(animated: true, completion: nil)
    }

    func navigationViewController(_ navigationViewController: NavigationViewController, didRerouteAlong route: Route) {
        //No need to handle this
    }
    
    func navigationViewController(_ navigationViewController: NavigationViewController, shouldRerouteFrom location: CLLocation) -> Bool{
        
         #if DEMO
        
            if(DemoSession.shared.isDemoRoute)
           {
               return false
           }
            else{
                return true
            }
        
        #else
        return !navigationViewController.navigationService.router.userIsOnRoute(location)
//        return true
        #endif
    }
    
    func endRoute(fromUser: Bool = false, fromCarparkArrival: Bool = false) {
        guard let viewModel = self.viewModel else { return }
        
        SwiftyBeaver.debug("navigation stopped! - fromUser(\(fromUser)")
        
        _ = AVAudioSession.sharedInstance().setAudioSessionActive()
        viewModel.clearAudioQueue()
        viewModel.stopSpeakingAudioInstructions()
        viewModel.endFromUser = fromUser
        viewModel.isDestinationReached = true
//        viewModel.stopTimerUpdateZoomLevel()
        
        //  Dismiss steps table if needed
        self.dismissStepsTable()
                
        self.saveTripLog(fromUser: fromUser) { _ in
            
            if !fromUser { // since if user end the Trip data updated already so that we don't need to update from here
                
                // we don't need to show this when user end the route
                // show endRouteView only if the walking path not selected
                if !viewModel.shouldShowCarparkArrival {
                    let offset = Values.topPanelHeight + UIApplication.topSafeAreaHeight
                    UIView.animate(withDuration: Values.standardAnimationDuration) { [weak self] in
                        guard let self = self else { return }
                        self.endRouteView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: offset+20)
                    } completion: { _ in
                        // no implementation
                    }
//                    DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) { [weak self] in
//                        guard let self = self else { return }
//                        self.viewModel?.announceFinalVoiceMessage()
//                    }
                    // when arrive destination without showing walking guide popup
                    self.endNavigationView.show(animated: true)
                    self.viewModel?.hideLiveLocationButton()
                    self.endNavigationView.onEndAction = { [weak self] in
                        guard let self = self else { return }
                        
                        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Navigation.UserClick.arrival_end_navigation, screenName: ParameterName.Navigation.screen_view)
                        self.dismiss(fromUser: fromUser, fromCarparkArrival: fromCarparkArrival)
                    }
                } else { // showing walking path now
                    self.dismiss(fromUser: fromUser, fromCarparkArrival: fromCarparkArrival)
                }

            } else {
                self.dismiss(fromUser: fromUser, fromCarparkArrival: fromCarparkArrival)
            }
        }
                
        // TODO: Since the logic all changed. I commented out the code for DEMO first
/*
        #if DEMO
        
        if(DemoSession.shared.isDemoRoute){
         // if appDelegate().carPlayConnected {
              
         // }
          
            self.popupAlert(title: nil, message: "Navigation End", actionTitles: ["Go to Home","Start Parking"], actions:[{ [weak self] action1 in
                guard let self = self else { return }
                if(DemoSession.shared.status == .inFirstLeg)
                {
                    DemoSession.shared.status = .firstLegComplete
                }
                
                if(DemoSession.shared.status == .inSecondLeg)
                {
                    DemoSession.shared.status = .none
                }
                
                // dismiss navigation view controller
                appDelegate().tripETA = nil
                self.endRouteDelegate?.onArrival()
                //self.endCarPlayNavigation(canceled: false)
                appDelegate().navigationSubmitHistoryData(description: "Navigation-End")
                self.dismiss(animated: false, completion:nil)
                
            },{ [weak self] action2 in
                guard let self = self else { return }
                if(DemoSession.shared.status == .inFirstLeg)
                {
                    DemoSession.shared.status = .firstLegComplete
                }
                
                if(DemoSession.shared.status == .inSecondLeg)
                {
                    DemoSession.shared.status = .none
                }
                // // dismiss navigation view controller
                //self!.endCarPlayNavigation(canceled: false)
                appDelegate().navigationSubmitHistoryData(description: "Navigation-End")
                self.dismiss(animated: true) {
                    self.endRouteDelegate?.goToParking()
                }
            }, nil])
        } else {

            // Show rating View
            let storyboard = UIStoryboard(name: "Navigation", bundle: nil)
            if let vc = storyboard.instantiateViewController(withIdentifier: String(describing: RatingVC.self)) as? RatingVC {
    //            vc.tripGUID = tripLogger.getTripGUID()
                vc.tripGUID = self.viewModel?.getTripId() ?? "NA"
                vc.onDismissed = { [weak self] in
                    
                    guard let self = self else {
                                      return }
                    
                        // dismiss navigation view controller
                        appDelegate().tripETA = nil
                        self.endRouteDelegate?.onArrival()
                        //self.endCarPlayNavigation(canceled: false)
                        appDelegate().navigationSubmitHistoryData(description: "Navigation-End")
                        self.dismiss(animated: false, completion:nil)

                }
                self.addChildViewControllerWithView(childViewController: vc as UIViewController,toView: self.view)
            }
        }
        #else
//        showRatingVC()
        #endif
*/
    }
    
    private func showArriveCarpark() {
        
//        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) { [weak self] in
//            guard let self = self else { return }
//            self.viewModel?.announceFinalVoiceMessage()
//        }
        NotificationCenter.default.post(name: .onShowCarparkListUpdates, object: false)
        
        AnalyticsManager.shared.logOpenPopupEventV2(popupName: ParameterName.NavigationMode.UserPopup.reached_destination, screenName: ParameterName.NavigationMode.screen_view)
        
        //NavigationSettings.shared.voiceMuted = true
        self.dismissStepsTable()
        
        let offset = topBannerVC.view.bounds.height + UIApplication.topSafeAreaHeight
        UIView.animate(withDuration: Values.standardAnimationDuration) { [weak self] in
            self?.carparkEndRouteView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: offset)
        } completion: { _ in
            // no implementation
        }

        let storyboard = UIStoryboard(name: "Navigation", bundle: nil)
        if let vc = storyboard.instantiateViewController(withIdentifier: String(describing: CarparkArrivalVC.self)) as? CarparkArrivalVC {
            vc.onEndTrip = { [weak self] in
                guard let self = self else { return }
                
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.NavigationMode.UserClick.popup_reached_destination_end_trip, screenName: ParameterName.NavigationMode.screen_view)
                
                self.endRoute(fromUser: false)
            }
            vc.onWalkingGuide = { [weak self] in
                guard let self = self else { return }
                
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.NavigationMode.UserClick.popup_reached_destination_walking_guide, screenName: ParameterName.NavigationMode.screen_view)
                
                self.endRoute(fromUser: false, fromCarparkArrival: true)
//                self.dismiss(animated: false, completion:nil)
                
//                let screenshot = UIApplication.shared.keyWindow?.capture()
//                self.endRouteDelegate?.onWalking(response: response, screenshot: screenshot)
            }
            
            self.addChildViewControllerWithView(childViewController: vc as UIViewController,toView: self.view)
        }
    }
    
    // TODO: To be removed. Also remove anything related to Rating screen
/*
    private func showRatingVC(fromUser: Bool) {
        // TODO: This is a work around!!!
        if fromUser { // only mute if it's ended by user
            NavigationSettings.shared.voiceMuted = true
        }
        guard let screenshot = takeScreenshot(view: self.view) else { return }
        self.view.addSubview(screenshot)
        self.view.bringSubviewToFront(screenshot)
        let storyboard = UIStoryboard(name: "Navigation", bundle: nil)
        if let vc = storyboard.instantiateViewController(withIdentifier: String(describing: RatingVC.self)) as? RatingVC {
            vc.tripGUID = self.viewModel?.getTripId() ?? "NA"
            vc.onDismissed = { [weak self] in
                guard let self = self else { return }
                self.endRouteDelegate?.onArrival()
                self.dismiss(animated: false, completion:nil)
            }
            self.addChildViewControllerWithView(childViewController: vc as UIViewController,toView: self.view)
        }
    }
*/
    
    ///1. Save trip log before dismissing the navigation screen, so that we don't record location
    private func saveTripLog(fromUser: Bool,completion: ((Bool) -> ())? = nil){

        ///https://breezeteam.atlassian.net/browse/BREEZES-6291
        ///https://breezeteam.atlassian.net/browse/BREEZES-6293
        ///Above two issues reported saying trip logs are not recorded. After checking debug logs we don't see prints for end trip. This can happen if navigationService.router.location is null. We don't when this can be null. If this location is null, we will save trip log with our default locationManager location in else
        if let location = self.navigationService.router.location{

            SwiftyBeaver.debug("Checking if Navigation Service Location is available while saving trip log \(location.coordinate.latitude)-\(location.coordinate.longitude)")

            self.viewModel?.saveTripLog(location: location, fromUser: fromUser,completion: { _ in
                completion?(true)
            })
        }
        else{

            SwiftyBeaver.debug("If Navigation Service Location is not available while saving trip log use default LocationManager location \(LocationManager.shared.location.coordinate.latitude)-\(LocationManager.shared.location.coordinate.longitude)")

            self.viewModel?.saveTripLog(location: LocationManager.shared.location, fromUser: fromUser,completion: { _ in
                completion?(true)
            })
        }

    }
    
    
    /// 2. Save history data
    private func saveHistoryData(location:CLLocation,fromUser: Bool = false){
        
        self.viewModel?.endTrip(location: location,manually: fromUser)
        
    }
    /// 3. Dismiss
    private func dismiss(fromUser: Bool = false, fromCarparkArrival: Bool = false) {
        
        // Since we are starting navigation service upon arrival, we will start again while dismissing the navigation screen
        self.navigationService.stop()
        
        ///https://breezeteam.atlassian.net/browse/BREEZES-6291
        ///https://breezeteam.atlassian.net/browse/BREEZES-6293
        ///Above two issues reported saying trip logs are not recorded and even history data is not recorded. After checking debug logs we didn't see prints for end trip. This can happen if navigationService.router.location is null. We don't when this can be null. If this location is null, we will save trip log with our default locationManager location in else
        if let location = self.navigationService.router.location {
            
            SwiftyBeaver.debug("Checking if Navigation Service Location is available while submitting history data \(location.coordinate.latitude)-\(location.coordinate.longitude)")
            
            self.saveHistoryData(location: location, fromUser: fromUser)
            
        }
        else{
            
            
            SwiftyBeaver.debug("If Navigation Service Location is not available while submitting history data, we will save it using default LocationManage location \(LocationManager.shared.location.coordinate.latitude)-\(LocationManager.shared.location.coordinate.longitude)")
            
            self.saveHistoryData(location: LocationManager.shared.location, fromUser: fromUser)
            
        }
        
        // dismiss navigation view controller
        appDelegate().tripETA = nil
        //appDelegate().navigationSubmitHistoryData(description: "Navigation-End")
        
        if fromCarparkArrival {
            guard let viewModel = self.viewModel, let response = viewModel.walkingResponse else { return }
            let screenshot = UIApplication.shared.keyWindow?.capture()
            self.endRouteDelegate?.onWalking(response: response, screenshot: screenshot)
        } else {
            self.endRouteDelegate?.onArrival()
        }
        self.dismiss(animated: false, completion:nil)
    }
    
/*
    func takeScreenshot(view: UIView) -> UIImageView? {
        
        if let image = UIApplication.shared.keyWindow?.capture() {
            return UIImageView(image: image)
        }
        return nil
//        UIGraphicsBeginImageContext(view.frame.size)
//        view.layer.render(in: UIGraphicsGetCurrentContext()!)
//        let image = UIGraphicsGetImageFromCurrentImageContext()
//        UIGraphicsEndImageContext()
////        UIImageWriteToSavedPhotosAlbum(image!, nil, nil, nil)
//
//        return UIImageView(image: image)
    }
*/
}

// NavigationMapViewCourseTrackingDelegate was removed from beta.5
extension TurnByTurnNavigationVC {
    @objc func navigationCameraStateDidChange(_ notification: Notification) {
        guard let navigationCameraState = notification.userInfo?[NavigationCamera.NotificationUserInfoKey.state] as? NavigationCameraState else { return }
        
        switch navigationCameraState {
        case .transitionToFollowing, .following:
            resumeButton?.isHidden = true
            stopTrackingTimer()
            break
        case .idle, .transitionToOverview, .overview:
            resumeButton?.isHidden = false
            startTrackingTimer()
            break
        }
    }
    
    private func startTrackingTimer() {
        stopTrackingTimer()
        self.trackingTimer = Timer.scheduledTimer(withTimeInterval: 10, repeats: false, block: { [weak self] _ in
            if !(self?.viewModel?.lessThan1kmFromDestination == true || self?.viewModel?.mobileMapViewUpdatable?.petrolButton.isSelected == true || self?.viewModel?.mobileMapViewUpdatable?.evchargerButton.isSelected == true) {
                self?.resumeButtonTapped()
            }
        })
    }
    
    private func stopTrackingTimer() {
        self.trackingTimer?.invalidate()
        self.trackingTimer = nil
    }

}
    
extension TurnByTurnNavigationVC: NavProgressVCDelegate {
    func onEnd() {
        NavigationSettings.shared.voiceMuted = true
        // For testing: to start cruise
//        LocationManager.shared.startSimulateCarMoving(after: 1)
        endRoute(fromUser: true)
    }
    
    func onConnectOBUFailure() {
        DispatchQueue.main.async {
            self.viewModel?.updateTripOBUStatus(.notConnected)
            self.viewModel?.mobileMapViewUpdatable?.updateTrafficInformationNotification(title: "Connection error", subTitle: "Retry or report issue when you have stopped safely", backgroundColor: UIColor(hexString: "#E82370", alpha: 1.0), icon: UIImage(named: "connect_obu_error")!, needAdjustFont: true)
        }
    }
    
    func onConnectOBUSuccess() {
        DispatchQueue.main.async {
            self.viewModel?.updateTripOBUStatus(.connected)
            showToast(from: self.view, message: "OBU is connected", topOffset: 20, fromCarPlay: false)
        }
    }
    
    func onShowCarparkList() {
        self.openCarparkList()
    }
    
    func onShareDestination() {
        if let destination = viewModel?.analyticsDest, let destInfo = viewModel?.getDestinationInfo() {
            self.etaChildVC = showShareDriveScreen(viewController: self, address: destInfo.address, etaTime: destInfo.arrivalTime,fromScreen: "navigation", destination: destination)
        }
    }
}

//  MARK: - GestureManagerDelegate methods
extension TurnByTurnNavigationVC: GestureManagerDelegate {
   
    func gestureManager(_ gestureManager: GestureManager, didEnd gestureType: GestureType, willAnimate: Bool) {
        
        //We will use this if there is an action neede for map touch
    }
    
    func gestureManager(_ gestureManager: GestureManager, didEndAnimatingFor gestureType: GestureType) {
        
        //We will use this if there is an action neede for map touch
    }
    
    func gestureManager(_ gestureManager: GestureManager, didBegin gestureType: GestureType) {
        
        switch gestureType {
        case .singleTap:
            let point = gestureManager.singleTapGestureRecognizer.location(in: navigationMapView)
            viewModel?.queryAmenitiesLayer(point: point)
            
            self.querySymbolLayerAmenityEvAndPetrol(point: point) { [weak self] selected in
                guard let self = self else { return }
                if !selected { // if no feature selected
//                    self.selectRoute(point)
                }
            }
        case .pinch, .doubleTapToZoomIn, .doubleTouchToZoomOut:
            DispatchQueue.main.async { [weak self] in
                guard let self = self, let viewmodel = self.viewModel else { return }
                viewmodel.dismissTooltips()
            }
        default:
            break
        }
    }
}

//  MARK: Handle carpark availability contribution
extension TurnByTurnNavigationVC {
    
    private func showThanksToast() {
        
        let offset = Values.topPanelHeight + 28
        
        showToast(from: self.view, message: "Thank you", topOffset: offset, icon: "greenTick", messageColor: UIColor(hexString: "222638", alpha: 1), duration: 5)
    }
    
    @objc func didReachToCarpark() {
        if let viewModel = self.viewModel, viewModel.shouldShowCarparkArrival {
            //  Show carpark arrival
            showArriveCarpark()
        }
    }
    
    @objc func confirmParkingAvailability() {
        //  Handle call event to native
        if let carpark = self.viewModel?.getCarpark(), let carparkId = carpark.id, !carparkId.isEmpty, let type = carpark.availablePercentImageBand {
            updateParkingAvailable(carparkId, type: type)
        }
        
        if let viewModel = self.viewModel, viewModel.shouldShowCarparkArrival {
            //  Show carpark arrival
            showArriveCarpark()
        }else {
            //  Show thanks toast message
            showThanksToast()
        }
    }
    
    @objc func showParkingSubmitPopup() {
        if let carpark = self.viewModel?.getCarpark() {
            showAvailableCarParkPopup(carpark)
        } else if let pinCarpark = locationPinCarpark {
            showAvailableCarParkPopup(pinCarpark)
        } else {
            //  Crowdsource parking POI / Drop Pin
            if let address = self.destAddress, address.isCrowdsourceParking == true {
                showAvailableCarParkPopup(location: address)
            }
        }
    }
    
    @objc func cancelParkingAvailable() {
        if let viewModel = self.viewModel, viewModel.shouldShowCarparkArrival {
            //  Show carpark arrival
            showArriveCarpark()
        }else {
            self.parkingAvailableVC?.view.removeFromSuperview()
        }
    }
    
    @objc func startWalkingPath() {
        //  Start walking
        self.endRoute(fromUser: false, fromCarparkArrival: true)
    }
    
    @objc func backToHomePage() {
        //        var isWalking = false
        //        if let viewModel = self.viewModel, viewModel.shouldShowCarparkArrival {
        //            isWalking = true
        //        }
        self.dismiss(fromUser: false, fromCarparkArrival: false)
    }
    
    func showAvailableCarParkPopup(_ model: Carpark? = nil, location: AnalyticDestAddress? = nil) {
        
        if appDelegate().carPlayConnected {return}
        
        var isWalking = false
        if let viewModel = self.viewModel, viewModel.shouldShowCarparkArrival {
            isWalking = true
        }
        
        //  POI or Drop Pin -> Confirm what need to be send to API to get parking availability
        if let carpark = model {
            self.parkingAvailableVC = self.getRNViewBottomSheet(toScreen: "ParkingAvailable", navigationParams: ["data": ["carparkId": carpark.id],
                                                                                                                 "isWalking": isWalking] as [String:Any])
            self.parkingAvailableVC?.view.frame = UIScreen.main.bounds
            if let vc = self.parkingAvailableVC {
                self.addChildViewControllerWithView(childViewController: vc,toView: self.view, belowView: endNavigationView)
                self.view.bringSubviewToFront(vc.view)
            }
            
        } else if let dest = location {
            if dest.needUpdateCrowdsource() && !dest.getCrowdsource().keys.isEmpty {
                self.parkingAvailableVC = self.getRNViewBottomSheet(toScreen: "ParkingAvailable", navigationParams: ["data": dest.getCrowdsource(),
                                                                                                                     "isWalking": isWalking] as [String: Any])
                self.parkingAvailableVC?.view.frame = UIScreen.main.bounds
                if let vc = self.parkingAvailableVC {
                    self.addChildViewControllerWithView(childViewController: vc,toView: self.view, belowView: endNavigationView)
                    self.view.bringSubviewToFront(vc.view)
                }
            }
        }
    }
    
    @objc func showTravelTimeList() {
        //  Remove before show other
        onCloseTravelTime()
        self.travelTimeList = self.getRNViewBottomSheet(toScreen: RNScreeNames.Travel_Time, navigationParams: self.viewModel?.dataTravel ?? [:])
        self.travelTimeList?.view.frame = UIScreen.main.bounds
        if let vc = self.travelTimeList {
            self.addChildViewControllerWithView(childViewController: vc, toView: self.view, belowView: endNavigationView)
            self.view.bringSubviewToFront(vc.view)
        }
    }
    
    @objc func showCarParkList() {
        //  Remove before show other
        onCloseCarParkAvailability()
        self.carParkAvailability = self.getRNViewBottomSheet(toScreen: RNScreeNames.CarPark_All_Avaiable, navigationParams: self.viewModel?.dataCarparkAvailability ?? [:])
        self.carParkAvailability?.view.frame = UIScreen.main.bounds
        if let vc = self.carParkAvailability {
            self.addChildViewControllerWithView(childViewController: vc, toView: self.view, belowView: endNavigationView)
            self.view.bringSubviewToFront(vc.view)
        }
    }
    
    @objc func onCloseTravelTime() {
        self.travelTimeList?.view.removeFromSuperview()
        self.travelTimeList?.removeFromParent()
        self.travelTimeList = nil
    }
    
    @objc func onCloseCarParkAvailability() {
        self.carParkAvailability?.view.removeFromSuperview()
        self.carParkAvailability?.removeFromParent()
        self.carParkAvailability = nil
    }
    
    @objc func onCloseCarparkList() {
        if self.carParkListVC != nil {
            self.carParkListVC?.view.removeFromSuperview()
            self.carParkListVC?.removeFromParent()
            self.carParkListVC = nil
        }
    }
    
    @objc func selectCarparkFromCarParkList(notification: Notification) {
        DispatchQueue.main.async {
            if let carparkDic = notification.object as? NSDictionary {
                //  Navigate to carpark
                do {
                    let jsonData = try JSONSerialization.data(withJSONObject: carparkDic, options: .prettyPrinted)
                    let theCarpark = try JSONDecoder().decode(Carpark.self, from: jsonData)
                    
                    let extraData: NSDictionary = ["message": (theCarpark.isCheapest == true) ? "select_cheapest" : "select_normal", "location_name": "location_\(theCarpark.name ?? "")", "longitude": theCarpark.long ?? 0.0, "latitude": theCarpark.lat ?? 0.0]
                    AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.NavigationMode.UserClick.parking_guidance_system_carpark_select, screenName: ParameterName.NavigationMode.screen_view, extraData: extraData)
                    
                    self.showToastRoutingToCarpark()
                    self.viewModel?.carParkNavigateHere(carPark: theCarpark)
                    
                    //  Reset status when user choose other carpark
                    //  Give a bit delay to make sure that its already reroute to other carpark
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [weak self] in
                        self?.viewModel?.didFetchCarparkAvailability = false
                    }
                    
                    self.onCloseCarParkAvailability()
                    self.onCloseCarparkList()
                } catch let error {
                    print(error)
                }
            }
        }
    }
    
    @objc func navigateToAlternateCarpark(notification: Notification) {
        DispatchQueue.main.async {
            if let theCarpark = notification.object as? Carpark {
                //  Navigate to alternate carpark
                self.showToastRoutingToCarpark()
                self.viewModel?.carParkNavigateHere(carPark: theCarpark)
            }
        }
    }
    
    @objc func parkingAvailabilityAlertDismissed(notification: Notification) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.viewModel?.onParkingAlertDismissed()
        }
    }
    
    @objc func onOBUDisconnected() {
        DispatchQueue.main.async {
            showToast(from: self.view, message: "OBU is disconnected", topOffset: 20, fromCarPlay: false)
        }
    }
    
    @objc func onNoDataAlert(notification: Notification) {
        DispatchQueue.main.async {
            if let index = notification.object as? Int {
                let title = "No Available Data"
                var message = ""
                let radiusInKm = Int(Double(UserSettingsModel.sharedInstance.evPetrolRange) / 1000)
                
                switch index {
                case 0: //  No parking
                    AnalyticsManager.shared.logOpenPopupEventV2(popupName: ParameterName.NavigationMode.UserPopup.no_parking_vicinity, screenName: ParameterName.NavigationMode.screen_view)
                    message = "Unable to find any carpark within 1km radius"
                case 1: //  No Petrol
                    AnalyticsManager.shared.logOpenPopupEventV2(popupName: ParameterName.NavigationMode.UserPopup.no_petrol_vicinity, screenName: ParameterName.NavigationMode.screen_view)
                    message = "Unable to find any petrol station within \(radiusInKm)km radius"
                case 2: //  No EV
                    AnalyticsManager.shared.logOpenPopupEventV2(popupName: ParameterName.NavigationMode.UserPopup.no_ev_vicinity, screenName: ParameterName.NavigationMode.screen_view)
                    message = "Unable to find any EV charger within \(radiusInKm)km radius"
                default:
                    break
                }
                if !message.isEmpty {
                    appDelegate().showOnAppErrorNotification(title: title, subTitle: message, controller: self) {
                        //  Analytics for close action
                        switch index {
                        case 0: //  No parking
                            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.NavigationMode.UserClick.parking_vicinity_close, screenName: ParameterName.NavigationMode.screen_view)
                        case 1: //  No Petrol
                            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.NavigationMode.UserClick.petrol_vicinity_close, screenName: ParameterName.NavigationMode.screen_view)
                        case 2: //  No EV
                            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.NavigationMode.UserClick.ev_vicinity_close, screenName: ParameterName.NavigationMode.screen_view)
                        default:
                            break
                        }
                    }
                    
                    //  Close carpark list if needed
                    self.onCloseCarparkList()
                }
            }
        }
    }
    
}

extension TurnByTurnNavigationVC {
    func openCarparkList() {
        
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.NavigationMode.UserClick.show_destination_carpark_list, screenName: ParameterName.NavigationMode.screen_view)
        
//        if let carparkList = viewModel?.carParkCoordinates, !carparkList.isEmpty {
            //  Get current lat,long
            if let userLocation = viewModel?.getDestinationWaypoint()?.coordinate {
                let locationDic = ["lat": userLocation.latitude, "long": userLocation.longitude]
                
                var arrivalTime = Date().timeIntervalSince1970
                if let remainingTime = viewModel?.getCurrentRemainingTime() {
                    arrivalTime += remainingTime
                }
                    
                let params: [String: Any] = [ // Object data to be used by that scree
                     "location": locationDic,
                     "fromScreen": "[navigation]",
                     "arrival_time": arrivalTime,
                     "baseURL": appDelegate().backendURL,
                     "idToken": AWSAuth.sharedInstance.idToken,
                     "deviceos":parameters!["deviceos"] as Any,
                     "deviceosversion":parameters!["deviceosversion"] as Any,
                     "devicemodel":parameters!["devicemodel"] as Any,
                     "appversion":parameters!["appversion"] as Any
                ]
                
                onCloseCarparkList()
                
                self.carParkListVC = self.getRNViewBottomSheet(toScreen: RNScreeNames.CARPARK_LIST, navigationParams: params)
                self.carParkListVC?.view.frame = UIScreen.main.bounds
                
                if let vc = self.carParkListVC {
                    self.addChildViewControllerWithView(childViewController: vc, toView: self.view, belowView: endNavigationView)
                    self.view.bringSubviewToFront(vc.view)
                }
            }
//        } else {
//            NotificationCenter.default.post(name: .onNoDataNotification, object: 0)
//        }
    }
}

extension TurnByTurnNavigationVC {
    func showToastRoutingToCarpark() {
        showToast(from: self.view, message: "Routing to carpark", topOffset: 20, duration: 3, fromCarPlay: false)
    }
    
    func showToastFromRN(_ message: String) {
        if !message.isEmpty {
            showToast(from: self.view, message: message, topOffset: 20, duration: 3, fromCarPlay: false)
        }
    }
}

/*
extension TurnByTurnNavigationVC: TurnByTurnInstructionsVCDelegate {
    func tapOnSteps() {
        showAnimateNotAvailable()
    }
}
*/

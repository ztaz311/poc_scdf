//
//  TurnByTurnNavigationViewModel.swift
//  Breeze
//
//  Created by Zhou Hao on 12/10/21.
//

import Foundation
import Turf
import MapboxDirections
import MapboxNavigation
import MapboxCoreNavigation
import MapboxMaps
import MapboxCoreMaps
import SwiftyBeaver
import CarPlay
import Accelerate
import CoreLocation
import AVFAudio
import Combine
import OBUSDK

final class TurnByTurnNavigationViewModel:BreezeObuProtocol {
    
    // MARK: - Private properties
    private var featureDetector: FeatureDetector!
    private var featureCollection3km = Turf.FeatureCollection(features: [])
    private var currentCPDisplayedFeature:FeatureDetectable?
    private var userSoonArrivingCarpark: Bool = false {
        didSet {
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                if self.userSoonArrivingCarpark {
                    if let desCarpark = self.getCarpark() {
                        self.fetchBroadcastMessage(desCarpark, mode: .navigation)
                    }
                }
            }
        }
    }
    private var userReachedToCarpark: Bool = false
    private var currentFeature: FeatureDetectable? {
        willSet {
            
            if let feature = newValue  {
                if feature is DetectedERP {
                    mobileMapViewUpdatable?.removeParkingAlert()
                }
                
                if self.needQueueEhorizontalEvent(feature) {
                    //  Do nothing
                } else {
                    if newValue?.id != currentFeature?.id {
                        if (newValue is DetectedSchoolZone) || (newValue is DetectedSilverZone) || (newValue is DetectedSpeedCamera) || (newValue is DetectedERP) {
                            if !OBUHelper.shared.isObuConnected() {
                                self.onNewFeatureUpdated(newValue)
                            }
                        } else {
                            self.onNewFeatureUpdated(newValue)
                        }
                    }
                }
            }
        }
        didSet {
            
            if let feature = currentFeature {
                if needQueueEhorizontalEvent(feature) {
                    //  Do nothing
                } else {
                    var needHandleEHorizon: Bool = false
                    if (currentFeature is DetectedSchoolZone) || (currentFeature is DetectedSilverZone) || (currentFeature is DetectedSpeedCamera) || (currentFeature is DetectedERP) {
                        if !OBUHelper.shared.isObuConnected() {
                            needHandleEHorizon = true
                        }
                    } else {
                        needHandleEHorizon = true
                    }
                    
                    if needHandleEHorizon {
                        DispatchQueue.main.async { [weak self] in
                            guard let self = self else { return }
                            self.updateNotification(featureId: self.currentFeature == nil ? oldValue?.id : self.currentFeature!.id)
                        }
                    }
                }
            }
        }
    }
    private var currentActiveERPCollection: Turf.FeatureCollection?
    
    private var tripLogger: TripLogger!
    private var voiceInstructor: VoiceInstructor!
    private var currentProgress: RouteProgress?
    private var isMute = !Prefs.shared.getBoolValue(.enableAudio)
    private var isTripEnded = false // only allow set once
    private var voiceController: RouteVoiceController!
    var analyticsDest: AnalyticDestAddress?
    var amenitySymbolThemeType = Values.LIGHT_MODE_UN_SELECTED
    
    var dataTravel: [String: Any] = [:]
    var dataCarparkAvailability: [String: Any] = [:]
    
    var broadcastPlanning: BroadcastMessageModel?
    
    private var amenityEVCancellable: AnyCancellable?
    var isSelectedAmenity: Bool = false
    
    // MARK: - ETA related
    //    private var isReminder15Sent = false
    private var isReminder1Sent = false
    var endFromUser: Bool = false
    var isDestinationReached = false {
        didSet {
            if isDestinationReached {
                
                NotificationCenter.default.post(name: .obuConnectionUpdates, object: true)
                
                //  End navigation need to hide petrol/EV and close carpark list
                NotificationCenter.default.post(name: .onShowCarparkListUpdates, object: false)
                mobileMapViewUpdatable?.hidePetrolAndEV()
                DispatchQueue.main.async {
                    NotificationCenter.default.post(name: .onCloseCarparkListScreen, object: nil)
                }
                
                if featureDetector != nil { // when origin is very close to destination, feature detector is nil when reaching the destination
                    featureDetector.delegate = nil // to prevent notification happened after user reach the destination
                }
                
                if !endFromUser {
                    self.confirmParkingAvailable()
                }
            }
        }
    }
    private var lastLiveLocationUpdateTime = 0.0
    private var tripETA: TripETA?
    private var liveLocTripId = ""
    private var isLiveLocationSharingEnabled = false
    private (set) var shouldShowCarparkArrival = false
    private (set) var walkingResponse: RouteResponse?
    private (set) var destinationCarparkName: String = ""
    private var carparkDest: Carpark?
    
    //  OBU trip summary
    var bookmarkId: String?
    var bookmarkName: String?
    //  Current road name
    var roadName: String?
    
//    private var autoZoomTimer: Timer?
    var lessThan1kmFromDestination = false
    var didFetchCarparkAvailability = false
    var didUpdateCarpark1KmWay = false
    
//    var zoomInCounter: Int = 0
//    let MAX_ZOOM_IN_COUNTER = 2
    // MARK: - Public properties
    var mobileMapViewUpdatable: MobileNavigationMapViewUpdatable? {
        didSet {
            if mobileMapViewUpdatable != nil {
                SwiftyBeaver.debug("mobileMapViewUpdatable assigned")
                mobileMapViewUpdatable?.delegate = self
                syncUpdate(mapViewUpdatable: mobileMapViewUpdatable!)
            }
        }
    }
    var carPlayMapViewUpdatable: CarPlayNavigationMapViewUpdatable? {
        didSet {
            if carPlayMapViewUpdatable != nil {
                SwiftyBeaver.debug("carPlayMapViewUpdatable assigned")
                carPlayMapViewUpdatable?.delegate = self
                syncUpdate(mapViewUpdatable: carPlayMapViewUpdatable!)
            }
        }
    }
    
    private var disposables = Set<AnyCancellable>()
    private (set) var navigationService: MapboxNavigationService
    private (set) var fromDeviceType: DeviceType
    private var currentTrafficIncidentsFeatures: Turf.FeatureCollection?
    private var carpark: Carpark?
    private var alternateCarpark: Carpark?
    private var needShowWalkingPath: Bool = false
    
    var carParkUpdater:CarParkUpdater?
    
    private var carParkTime:Timer?
    private var originlDestWayPoint: Waypoint?
    private var destWaypoint: Waypoint?
    var otherWayPoints = [Waypoint]()
    private var zoneMonitor: ZoneMonitor?
    
    var notifyArrivalAction: (()->Void)?
    var lastTimeUpdateLocation = Date()
    var lastLocation: CLLocation?
    private let TIME_DURATION: TimeInterval = 1.0
    
    private var idleTime: TimeInterval = 0
    private var watchdogIdleTime: Date?
    private var currentSpeed: Double = 0.0
    private var limitSpeed: Double = 0.0
    
    var totalTravelTime: Int = -1
    var totalTravelDistance: Int = -1
    
    var distanceCompare = 0
    var bottomSheetHeight = 0
    var amenityCancellables: AnyCancellable?
    var routeStopsAdded = [Waypoint]()
    var wayPointFeatures = [Turf.Feature]()
    
    var petrolAndEVCallOutView: ToolTipsView?
    
    var amenityTimer: Timer?
    
    var notifyOnEvAmenites: (() -> Void)?
    var notifyOnPetrolAmenites: (() -> Void)?
    
    var amenityCoordinates = [CLLocationCoordinate2D]()
     
    private var carParkCoordinates = [CLLocationCoordinate2D]()
    
    var needShowNodata = true
    
    public var userIsMoving: Bool = false
    private var showCarparkWhenSpeedIsZero: Bool = false
    
    private var retryCounter: Int = 0
    private let MAX_RETRY_LOAD_CARPARK = 2
    
    // For Carplay
//    var carparkViewModel: CarparksViewModel!
//    private var filteredCarPark = [Carpark]()
//    private var carparkCoordinates = [CLLocationCoordinate2D]()
    var queueEvents: [Any] = []
    
    
    init(navigationService: MapboxNavigationService, tripETA: TripETA?, from device: DeviceType, carpark: Carpark? = nil) {
        SwiftyBeaver.debug("TurnByTurnNavigationViewModel init - start navigation")
        
        // Navigation Start notification
        NotificationCenter.default.post(name: Notification.Name(Values.notificationNavigationStart), object: nil)
        
        self.navigationService = navigationService
        
        self.carpark = carpark
        
        self.needShowWalkingPath = !(carpark != nil)
        
        // Adding below based on map box suggestions to over come GPS issues
        //https://github.com/mapbox-collab/ncs-collab/issues/159#issuecomment-1034548036
        self.navigationService.locationManager.activityType = .otherNavigation
        
        self.tripETA = tripETA
        self.fromDeviceType = device
        //        self.navigationService.delegate = self // This will cause issue
        
        tripLogger = TripLogger(service: TripLogService())
        
        if let location = self.navigationService.router.location {
            getAddressFromLocation(location: location) { [weak self] address in
                guard let self = self else { return }
                let obuStatus = OBUHelper.shared.getTripSummaryObuStatus()
                self.tripLogger.start(startLocation: location.coordinate, address1: address, address2: "", bookmarkId: self.bookmarkId, bookmarkName: self.bookmarkName, obuStatus: obuStatus)                
                OBUHelper.shared.setTripGUID(self.tripLogger.getTripGUID())
            }
        }
        setupObservers()
#if DEMO
        if(DemoSession.shared.isDemoRoute)
        {
            Timer.scheduledTimer(timeInterval: 90, target: self, selector:  #selector(triggerETAAPI), userInfo: nil, repeats: false)
        }
        else
        {
            setupETA()
        }
#else
        setupETA()
#endif
        
        NavigationSettings.shared.voiceMuted = !Prefs.shared.getBoolValue(.enableAudio)
        
#if NEW_VOICE
        let credentials = navigationService.directions.credentials
        let multiplexedSpeechSynthesizer = MultiplexedSpeechSynthesizer([RivaSpeechSynthesizer()/*,SystemSpeechSynthesizer()*/])
        multiplexedSpeechSynthesizer.locale = Locale(identifier: "en_GB")
        voiceController = RouteVoiceController(navigationService: navigationService, speechSynthesizer: multiplexedSpeechSynthesizer)
        voiceController.speechSynthesizer.delegate = self
#else
        let credentials = navigationService.credentials
        voiceController = RouteVoiceController(navigationService: navigationService, accessToken: credentials.accessToken, host: credentials.host.absoluteString)
        voiceController.speechSynthesizer.delegate = self
#endif
        
        voiceInstructor = VoiceInstructor(synthesizer: voiceController.speechSynthesizer)
        voiceInstructor.synthesizer.managesAudioSession = false // We need to make this false in order to control the volume when navigation is not running
        
        // start history recording for navigation
        appDelegate().cruiseSubmitHistoryData(description: "CruiseMode-End")
        appDelegate().startNavigationRecordingHistoryData()
        
        OBUHelper.shared.setMode(OBUHelper.shared.getObuMode(), delegate: self)
        if OBUHelper.shared.getObuMode() == .mockServer {
            OBUHelper.shared.startBreezeMockEvent()
        } else {
            OBUHelper.shared.startObuEvent()
        }
        
#if targetEnvironment(simulator)
        // Code for simulate car moving after cruise mode stopped
        //        LocationManager.shared.startSimulateCarMoving(after: 5)
        
        /*
         #if STAGING || TESTING
         LocationManager.shared.startSimulateOverspeed(after: 10, speed: 75)
         #endif
         */
        /*
         // For testing multiple voices
         DispatchQueue.main.asyncAfter(deadline: .now()+0.1) {
         self.voiceInstructor.speak(DetectedERP(id: "1", distance: 20, priority: 0, address: "ABC", price: 1.0, zoneId: "A"), distanceAlongStep: 0)
         }
         
         DispatchQueue.main.asyncAfter(deadline: .now()+0.5) {
         self.voiceInstructor.speak(DetectedTrafficIncident(id: "2", distance: 200, priority: 0, category: "Incident", message: "incident"), distanceAlongStep: 0)
         }
         
         DispatchQueue.main.asyncAfter(deadline: .now()+1) {
         self.voiceInstructor.speak(DetectedTrafficIncident(id: "3", distance: 200, priority: 0, category: "Accident", message: "accident"), distanceAlongStep: 0)
         }
         
         DispatchQueue.main.asyncAfter(deadline: .now()+2) {
         self.voicInstructionOtherText(text: "Congestion Alert. Limited parking at this time. Parking available at HDB carparks nearby.")
         }
         */
        
#endif
        
        // MARK: - For Testing: Overspeed

//        #if STAGING || TESTING
//        // For testing, don't comment out previous test case
//        // Test case 1
//        self.userIsMoving = true
//        LocationManager.shared.startSimulateOverspeed(after: 1, speed: 20)
//        
//        // Test case 2 - overspeed, during blinking, below the speed limit
//        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
//            LocationManager.shared.startSimulateOverspeed(after: 1, speed: 0)
//            self.userIsMoving = false
//        }
//        
//        DispatchQueue.main.asyncAfter(deadline: .now() + 15) {
//            LocationManager.shared.startSimulateOverspeed(after: 1, speed: 20)
//            self.userIsMoving = true
//        }
//        
//        // Test case 3 - overspeed, not overspeed, overspeed again
////        DispatchQueue.main.asyncAfter(deadline: .now() + 30) {
////            LocationManager.shared.startSimulateOverspeed(after: 1, speed: 0)
////        }
//        #endif
        
        LocationManager.shared.startSpeedCheck()
        
        // For Testing: Notifications
        /*
         DispatchQueue.main.asyncAfter(deadline: .now() + 10) {
         // FlashFlood
         //            self.currentFeature = DetectedTrafficIncident(id: "1", distance: 10, priority: FeatureDetection.cruisePriorityTraffic, category: TrafficIncidentCategories.FlashFlood, message: "Mandalay Road Mandalay Towers")
         self.currentFeature = DetectedERP(id: "2", distance: 282.3, priority: FeatureDetection.cruisePriorityErp, address: "Blk 35 Mandalay Road Mandalay Towers", price: 1.50, zoneId: "A")
         //            self.currentFeature = DetectedSchoolZone(id: "3", distance: 299.0, priority: FeatureDetection.cruisePrioritySchoolZone, feature: Turf.Feature(geometry: .point(Point(CLLocationCoordinate2D(latitude: 0, longitude: 0)))))
         //            self.currentFeature = DetectedTrafficIncident(id: "1", distance: 10, priority: FeatureDetection.cruisePriorityTraffic, category: TrafficIncidentCategories.RoadWork, message: "Mandalay Road Mandalay Towers")
         }
         */
       
        
    }
    
#if DEMO
    @objc func triggerETAAPI(){
        setupETA()
    }
#endif
    
    /// Get destination carpark
    func getCarpark() -> Carpark? {
        if let desCarpark = self.carparkDest {
            return desCarpark
        }else if let desCarpark = self.carpark {
            return desCarpark
        }
        return nil
    }
    
    /// get destination name and coordinate
    func getDestinationInfo() -> (address: String, arrivalTime: String) {
        
        if let currentProgress = currentProgress {
            
            return (address: originlDestWayPoint?.name ?? "",arrivalTime: Date(timeIntervalSinceNow: currentProgress.durationRemaining).getTimeFromDate())
            
        }
        return (address: originlDestWayPoint?.name ?? "",arrivalTime: Date(timeIntervalSinceNow: navigationService.route.expectedTravelTime).getTimeFromDate())
    }
    
    func getDestinationWaypoint() -> Waypoint? {
        
        if let originalDest = self.destWaypoint {
            return originalDest
        }
        return originlDestWayPoint
    }
    
    func saveTripLog(location: CLLocation, fromUser: Bool,completion: ((Bool) -> ())? = nil){
        
        OBUHelper.shared.setTripGUID("")
        
        SwiftyBeaver.debug("Save Trip log during end navigation \(location.coordinate.latitude)-\(location.coordinate.longitude)")
        // how to get the destination coordinate (selectedAddress in original implementation)
        getAddressFromLocation(location: location) { [weak self] address in
            guard let self = self else { return }
            SwiftyBeaver.debug("Address Received while saving trip log \(address)")
            self.tripLogger.stop(endLocation: location.coordinate, address1: address, address2: "", stoppedByUser: fromUser, amenityType: self.analyticsDest?.amenityType ?? "", amenityId: self.analyticsDest?.amenityId ?? "", layerCode: self.analyticsDest?.layerCode ?? "", plannedDestAddress1: self.analyticsDest?.address1 ?? "", plannedDestAddress2: self.analyticsDest?.address2 ?? "", plannedDestLat: self.analyticsDest?.lat ?? 0, plannedDestLong: self.analyticsDest?.long ?? 0, idleTime: self.idleTime, bookmarkId: self.bookmarkId, bookmarkName: self.bookmarkName)
            
            completion?(true)
        }
    }
    
    func endTrip(location: CLLocation, manually: Bool = false, fromCarPlay:Bool = false) {
        guard !isTripEnded else { return }
        
        SwiftyBeaver.debug("endTrip -  manually - \(manually)")
        
        // Navigation stop notification
        NotificationCenter.default.post(name: Notification.Name(Values.notificationNavigationStop), object: nil)
        
        if !liveLocTripId.isEmpty {
            
            let estimatedTime = estimatedArrivalTime(currentProgress?.durationRemaining ?? 0)
            
            DataCenter.shared.startUpdatingUserTripLiveLocation(tripId: liveLocTripId, latitude: location.coordinate.latitude.toString(), longitude: location.coordinate.longitude.toString(), status: manually ? LiveLocationStatusType.cancel.rawValue : LiveLocationStatusType.complete.rawValue,arrivalTime: "\(estimatedTime.time)\(estimatedTime.format)",coordinates: "")
            updateLiveLocationStatus(status: manually ? .cancel : .complete)
        }
        
        if let carPlayMapViewUpdatable = carPlayMapViewUpdatable {
            
            carPlayMapViewUpdatable.cpFeatureDisplay = nil
            carPlayMapViewUpdatable.cpFeatureDisplayClose = false
        }
        appDelegate().navigationSubmitHistoryData(description: manually ? "Navigation-ManualEnd " : "Navigation-End")
        isTripEnded = true
        
    }
    
    func clearNavigationEnd(controller:NavigationViewController){
        
        //To remove arrow after reaching destination as per https://github.com/mapbox-collab/ncs-collab/issues/530#issuecomment-1166551811
        if let mobileMapViewUpdatable = mobileMapViewUpdatable {
            mobileMapViewUpdatable.navigationMapView?.removeArrow()
        }
        
        if let carPlayMapViewUpdatable = carPlayMapViewUpdatable {
            carPlayMapViewUpdatable.navigationMapView?.removeArrow()
        }
        
        ///snapUserLocationAnnotationToRoute to false makes user location will be location from the device and will not snapped to the route
        ///start navigation service again according to https://github.com/mapbox-collab/ncs-collab/issues/546#issuecomment-1182619982
        controller.snapsUserLocationAnnotationToRoute = false
        controller.navigationService.start()
    }
    
    func stopUpdatingeHorizon(){
        
        if let router = self.navigationService.router as? RouteController {
            
            router.stopUpdatingElectronicHorizon()
        }
    }
    
    func getTripId() -> String {
        return tripLogger?.getTripGUID() ?? ""
    }
    
    func updateTripOBUStatus(_ status: TripOBUStatus) {
        self.tripLogger.updateOBUStatus(status)
    }
    
    /*
     To query action on mobileNavigationMapView
     Only for show tooltip at carpark updater
     */
    func queryAmenitiesLayer(point:CGPoint) {
        // only show tooltips if the walking path not decided
        if !shouldShowCarparkArrival {
            carParkUpdater?.queryAmenitiesLayer(point: point)
        }
    }
    
    private func startZoneMonitor() {
        guard zoneMonitor == nil,
              let originlDestWayCoordinate = self.originlDestWayPoint?.coordinate else { return } // only call once
        
        zoneMonitor = ZoneMonitor(origin: LocationManager.shared.location.coordinate, destination: originlDestWayCoordinate)
        zoneMonitor?.start()
        zoneMonitor!.outterZoneAction = { [weak self] reached in
            guard let self = self else { return }
            if reached {
                // call api to show audio message if the area congested
                if let carpark = self.carpark {
                    let carparkIsDestination = true
                    let carparkId = carpark.id ?? ""
                    // call api to show audio message if the area congested
                    self.zoneMonitor?.handleZoneAlert(type: "NAVIGATION", zoneId: SelectedProfiles.shared.getOuterZoneId(),carparkIsdestination: carparkIsDestination, carparkId: carparkId,targetZoneId: SelectedProfiles.shared.getOuterZoneId(), completion: { alert in
                        guard let alert = alert else { return }
                        if let message = alert.alertMessage, !message.isEmpty {
                            self.voicInstructionOtherText(text: message)
                            //                                    self.refreshCarpark()
                        }
                        if alert.isCongestion ?? false {
                            self.refreshCarpark()
                        }
                    })
                } else {
                    // destination is not a carpark, check if zone is not congested
                    if SelectedProfiles.shared.isZoneCongestion() {
                        self.zoneMonitor?.handleZoneAlert(type: "NAVIGATION", zoneId: SelectedProfiles.shared.getOuterZoneId(),targetZoneId: SelectedProfiles.shared.getOuterZoneId(), completion: { alert in
                            guard let alert = alert else { return }
                            if let message = alert.alertMessage, !message.isEmpty {
                                self.voicInstructionOtherText(text: message)
                            }
                        })
                    }
                }
            }
        }
        
        // Commented the below code for supporting both inner and outer zones. If backend returns multiple zones then we need to think about the logic to handle the zones dynamically instead of differentiating as inner/outer
        // start zone monitor
        //        if SelectedProfiles.shared.isProfileSelected() {
        // if the destination is inside the TB zone
        /*if identifyUserLocInOuterZone(centerCoordinate: zoneCenterCoordinate, currentLocation: originlDestWayCoordinate) {
         zoneMonitor = ZoneMonitor(origin: LocationManager.shared.location.coordinate, destination: originlDestWayCoordinate, routeLine: routeLine)
         zoneMonitor!.start()
         zoneMonitor!.innerZoneAction = { [weak self] reached in
         guard let self = self else { return }
         if reached {
         if let carpark = self.carpark {
         let carparkIsDestination = true
         let carparkId = carpark.id ?? ""
         // call api to show audio message if the area congested
         self.zoneMonitor?.handleZoneAlert(type: "NAVIGATION", zoneId: SelectedProfiles.shared.getInnerZoneId(),carparkIsdestination: carparkIsDestination, carparkId: carparkId, completion: { alert in
         guard let alert = alert else { return }
         if let message = alert.alertMessage, !message.isEmpty {
         self.voicInstructionOtherText(text: message)
         //                                    self.refreshCarpark()
         }
         if alert.isCongestion ?? false {
         self.refreshCarpark()
         }
         })
         } else {
         // destination is not a carpark, check if inner zone is not congested
         // TODO: may need call congestion api to check
         if SelectedProfiles.shared.isinnerZoneCongestion() {
         self.zoneMonitor?.handleZoneAlert(type: "NAVIGATION", zoneId: SelectedProfiles.shared.getInnerZoneId(), completion: { alert in
         guard let alert = alert else { return }
         if let message = alert.alertMessage, !message.isEmpty {
         self.voicInstructionOtherText(text: message)
         }
         })
         }
         }
         }
         }
         
         zoneMonitor!.outterZoneAction = { [weak self] reached in
         guard let self = self else { return }
         if reached {
         // call api to show audio message if the area congested
         self.zoneMonitor?.handleZoneAlert(type: "NAVIGATION", zoneId: SelectedProfiles.shared.getOuterZoneId(), targetZoneId: SelectedProfiles.shared.getInnerZoneId(), completion: { alert in
         guard let alert = alert else { return }
         if let message = alert.alertMessage, !message.isEmpty {
         self.voicInstructionOtherText(text: message)
         }
         })
         }
         }
         }*/
        //        }
    }
    
    private func refreshCarpark() {
        
        guard let updater = self.carParkUpdater, let coordinate = self.originlDestWayPoint?.coordinate else { return }
        
        // don't refresh carpark if the walking path already added
        guard !shouldShowCarparkArrival else { return }
        
        updater.isRefreshCarPark = true
        updater.refreshCarParks(maxCarParksCount: Values.maxNoOfCarparksInCruiseMode, radius: SelectedProfiles.shared.getOuterZoneRadius(), location: coordinate,finalDestCoord:navigationService.route.shape?.coordinates.last ?? nil)
    }
    
    
    
    func dismissTooltips() {
        
        //  Get callout for ev and petrol then dismiss
        if let callout = self.petrolAndEVCallOutView {
            callout.dismiss(animated: true)
            callout.removeFromSuperview()
            self.petrolAndEVCallOutView = nil
        }
        
        guard let carParkUpdater = carParkUpdater, let calloutView = carParkUpdater.calloutView else {
            return
        }
        calloutView.dismiss(animated: true)
        carParkUpdater.calloutView = nil
    }
    
    func updateTooltipsPosition(naviMapView: NavigationMapView) {
        if let calloutView = carParkUpdater?.calloutView {
            calloutView.adjust(to: calloutView.tooltipCoordinate, in: naviMapView)
        } else if let calloutView = petrolAndEVCallOutView {
            calloutView.adjust(to: calloutView.tooltipCoordinate, in: naviMapView)
        }
    }
    
    @objc private func applicationWillTerminate(_ notification: NSNotification) {
        guard !isTripEnded else { return }
        
        // save the trip
        if let location = self.navigationService.router.location {
            tripLogger.save(endLocation: location.coordinate, amenityType: self.analyticsDest?.amenityType ?? "", amenityId: self.analyticsDest?.amenityId ?? "", layerCode: self.analyticsDest?.layerCode ?? "", plannedDestAddress1: self.analyticsDest?.address1 ?? "", plannedDestAddress2: self.analyticsDest?.address2 ?? "", plannedDestLat: self.analyticsDest?.lat ?? 0, plannedDestLong: self.analyticsDest?.long ?? 0, idleTime: self.idleTime, bookmarkId: self.bookmarkId, bookmarkName: self.bookmarkName)
            endTrip(location: location, manually: true)
        }
    }
    
    private func syncUpdate(mapViewUpdatable: MapViewUpdatable) {
        mapViewUpdatable.setupNavigationCamera(isCarPlay: mapViewUpdatable as? CarPlayNavigationMapViewUpdatable != nil)
        loadData()
        mapViewUpdatable.updateETA(eta: tripETA)
        // only start monitor when map attached
        startZoneMonitor()
#if DEBUG
        mapViewUpdatable.navigationMapView?.mapView.addProfileZonesDynamically(zoneColors: SelectedProfiles.shared.getZoneColors(), zoneDetails: SelectedProfiles.shared.getAllZones())
#endif
    }
    
    private func loadData() {
        
        if featureDetector == nil {
            setupFeatureDetector()
        }
        DataCenter.shared.addERPUpdaterListner(listner: self)
        DataCenter.shared.addTrafficUpdaterListener(listener: self)
        
    }
    
    private func setupFeatureDetector() {
        if let router = self.navigationService.router as? RouteController {
            router.reroutesProactively = false
            featureDetector = FeatureDetector(roadObjectsStore: router.roadObjectStore, roadObjectMatcher: router.roadObjectMatcher, roadGraph: router.roadGraph)
            featureDetector.delegate = self
            
#if DEMO
            if(DemoSession.shared.isDemoRoute){
                if(DemoSession.shared.status == .inFirstLeg)
                {
                    DemoSession.shared.parseDemoJSONItems { [weak self] demoFeatureCollection in
                        guard let self = self else { return }
                        self.mobileMapViewUpdatable?.removeDemoIncidentsLayer()
                        self.mobileMapViewUpdatable?.addDemoIncidentsLayer(features: DataCenter.shared.getDemoFeatures())
                        
                        self.carPlayMapViewUpdatable?.removeDemoIncidentsLayer()
                        self.carPlayMapViewUpdatable?.addDemoIncidentsLayer(features: DataCenter.shared.getDemoFeatures())
                        
                        self.featureDetector.addDemoObjectsToRoadMatch()
                    }
                }
                else
                {
                    DemoSession.shared.parseDemoJSONItems { [weak self] demoFeatureCollection in
                        guard let self = self else { return }
                        self.mobileMapViewUpdatable?.removeDemoLeg2IncidentsLayer()
                        self.mobileMapViewUpdatable?.addDemoLeg2IncidentsLayer(features: DataCenter.shared.getDemoFeatures())
                        
                        self.carPlayMapViewUpdatable?.removeDemoLeg2IncidentsLayer()
                        self.carPlayMapViewUpdatable?.addDemoLeg2IncidentsLayer(features: DataCenter.shared.getDemoFeatures())
                        
                        self.featureDetector.addDemoObjectsToRoadMatch()
                    }
                }
            }
            else
            {
                self.addObjectsToRoadMatcher()
            }
#else
            addObjectsToRoadMatcher()
#endif
            
            let options = ElectronicHorizonOptions(length: 1500, expansionLevel: 0, branchLength: 50, minTimeDeltaBetweenUpdates: nil)
            router.startUpdatingElectronicHorizon(with: options) //explicitly start eHorizon
        }
    }
    
    func addObjectsToRoadMatcher(){
        featureDetector.addERPObjectsToRoadMatcher()
        featureDetector.addSchoolObjectsToRoadMatcher()
        featureDetector.addSilverZoneObjectsToRoadMatcher()
        featureDetector.addSpeedCameraObjectsToRoadMatcher()
        featureDetector.addTrafficIncidentObjectsToRoadMatch()
        featureDetector.addSeasonParkingsObjectsToRoadMatch()
    }
    
    func confirmParkingAvailable() {
        if !userReachedToCarpark {
            userReachedToCarpark = true
            
            if let desCarpark = self.getCarpark() {
                mobileMapViewUpdatable?.updateParkingAvailability(carpark: desCarpark)
            } else {
                //  Handle show crowdsource parking availability updates
                NotificationCenter.default.post(name: .showSubmitParkingAvailablePopup, object: nil)
            }
            //            carPlayMapViewUpdatable?.updateParkingAvailability(carpark: carpark)
        }
    }
    
    
    // MARK: Update NOTIFICATION CarParkForm OBU
    // , params: [String: Any
    func disPlayCarParkUpdateView(title: String, subTitle: String, backgroundColor: UIColor, icon: UIImage) {
        // , params: params
        mobileMapViewUpdatable?.updateCarParkandTravelTimeNotification(title: title, subTitle: subTitle, backgroundColor: backgroundColor, icon: icon)
    }
    
    
    func displayTrafficInformation(title: String, subTitle: String, backgroundColor: UIColor, icon: UIImage?, dismissInSeconds:Int? = 5, needAdjustFont: Bool = false) {
        mobileMapViewUpdatable?.updateTrafficInformationNotification(title: title, subTitle: subTitle, backgroundColor: backgroundColor, icon: icon ?? UIImage(), dismissInSeconds: dismissInSeconds, needAdjustFont: needAdjustFont)
    }
    
    func displayCommonOBUNotification(title: String, subTitle: String, price: String, backgroundColor: UIColor, icon: UIImage, iconRate: UIImage?) {
        mobileMapViewUpdatable?.updateObuCommonNotification(title: title, subTitle: subTitle, price: price, backgroundColor: backgroundColor, icon: icon, iconRate: iconRate)
    }
    
    func displayGeneralMessageAleart(title: String, subTitle: String, backgroundColor: UIColor) {
        mobileMapViewUpdatable?.updateGeneralMessageNotification(title: title, subTitle: subTitle, backgroundColor: backgroundColor)
    }
    
    private func updateNotificationWhenUserSoonArrive(carpark: Carpark?, broadcast: BroadcastMessageModel) {
        print("updateNotificationWhenUserSoonArrive")
        if(!isDestinationReached){
            mobileMapViewUpdatable?.updateNotificationWhenUserSoonArrive(carpark: carpark, message: broadcast.getBroadcastMessage())
            carPlayMapViewUpdatable?.updateNotificationWhenUserSoonArrive(carpark: carpark, message: broadcast.getBroadcastMessage())
        }
    }
    
    private func updateNotification(featureId: String?) {
        if(!isDestinationReached){
//            checkCloseCarparkList(.TRAFFIC)
            mobileMapViewUpdatable?.updateNotification(featureId: featureId, currentFeature: currentFeature)
            carPlayMapViewUpdatable?.updateNotification(featureId: featureId, currentFeature: currentFeature)
        }
    }
    
    func addETA(recipientName: String, recipientNumber: String, message: String, etaFavoriteId: Int) {
        
        guard let destCoordinate = originlDestWayPoint?.coordinate, let address = originlDestWayPoint?.name else { return }
        
        var finalMessage = message
        //During navigation if share drive selected from CarPlay, we will pass eta message as empty and we will construct final eta Message here
        if(finalMessage == ""){
            
            let estimatedTime = estimatedArrivalTime((self.navigationService.route.expectedTravelTime))
            finalMessage = "\(AWSAuth.sharedInstance.userName) is on the way to \(address) and will approximately arrive by \(estimatedTime.time)\(estimatedTime.format)."
        }
        self.isReminder1Sent = true
        self.tripETA = TripETA(type: TripETA.ETAType.Init, message: finalMessage, recipientName: recipientName, recipientNumber: recipientNumber, shareLiveLocation: "Y", tripStartTime: Int(Date().timeIntervalSince1970), tripEndTime: Int(Date().timeIntervalSince1970 + navigationService.route.expectedTravelTime), tripDestLat: destCoordinate.latitude, tripDestLong: destCoordinate.longitude, destination: address, tripEtaFavouriteId: etaFavoriteId)
        setupETA()
    }
    
    private func setupETA() {
        
        if let tripETA = self.tripETA {
            lastLiveLocationUpdateTime = Date().timeIntervalSince1970
            TripETAService().sendTripETA(tripETA) { [weak self] result in
                guard let self = self else { return }
                
                switch result {
                case .success(let tripETAResponse):
                    DispatchQueue.main.async {
                        
#if DEMO
                        if(DemoSession.shared.isDemoRoute){
                            
                            DemoSession.shared.recipientName = tripETA.recipientName
                            let feature = DetectedETANotificationView(id: "N", distance: 0.0, priority: 1, category: Values.etaNotifyName, message: "Sure, see you soon!", rName: tripETA.recipientName)
                            self.currentFeature = feature
                            print("ETA Notify Feature",self.currentFeature)
                        }
#endif
                        
                        if let currentProgress = self.currentProgress {
                            if currentProgress.durationRemaining < Values.durationForETAReachingReminder {
                                
                                self.isReminder1Sent = true
                            }
                            else{
                                
                                self.isReminder1Sent = false
                            }
                        }
                        else{
                            
                            self.isReminder1Sent = false
                        }
                        
                        self.voicInstructionOtherText(text: "Message sent to \(tripETA.recipientName).")
                        self.updateETAUI(eta: tripETA)
                        self.liveLocTripId = tripETAResponse.tripEtaUUID
                        self.isLiveLocationSharingEnabled = true
                        DataCenter.shared.subscribeToMessageInbox(tripETAId: self.liveLocTripId) { [weak self] message in
                            guard let self = self, !message.isEmpty else { return }
                            
                            self.showETAMessage(recipient: tripETA.recipientName, message: message)
                        }
                        
                    }
                case .failure(let error):
                    DispatchQueue.main.async {
                        
                        self.hideShareDriveButton(true)
                    }
                    
                    print(error.localizedDescription)
                }
            }
        } else {
            if let currentProgress = currentProgress {
                if currentProgress.distanceRemaining < Values.durationForETAReachingReminder {
                    
                    self.isReminder1Sent = true
                }
            }
            // No ETA, so should not send Reminder
            //self.isReminder1Sent = true
            //            self.isReminder15Sent = true
        }
    }
    
    private func showETAMessage(recipient: String, message: String) {
        mobileMapViewUpdatable?.showETAMessage(recipient: recipient, message: message)
        carPlayMapViewUpdatable?.showETAMessage(recipient: recipient, message: message)
    }
    
    private func updateETAUI(eta: TripETA?) {
        mobileMapViewUpdatable?.updateETA(eta: eta)
        carPlayMapViewUpdatable?.updateETA(eta: eta)
    }
    
    private func updateLiveLocationStatus(status: LiveLocationStatusType) {
        TripETAService().updateLocationSharingStatus(LiveLocationStatus(tripEtaUUID: self.liveLocTripId, status: status.rawValue)) { result in
            switch result {
            case .failure(let err):
                print(err)
            case .success(let response):
                print("Live Location \(status): \(response.message)")
            }
        }
    }
    
    private func setupObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(applicationWillTerminate(_:)), name: Notification.Name(Values.notificationAppTerminate), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(progressDidChange(_ :)), name: .routeControllerProgressDidChange, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(rerouted(_:)), name: .routeControllerDidReroute, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(willReRouted(_:)), name: .routeControllerWillReroute, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(failedReroute(_:)), name: .routeControllerDidFailToReroute, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(didArrival(_:)), name: .didArriveAtWaypoint, object: nil)
        //        NotificationCenter.default.addObserver(self, selector: #selector(roadNameUpdated(_:)), name:      .currentRoadNameDidChange, object: nil)
    }
    
    @objc private func progressDidChange(_ notification: NSNotification) {
        guard let progress = notification.userInfo?[RouteController.NotificationUserInfoKey.routeProgressKey] as? RouteProgress,
              let location = notification.userInfo?[RouteController.NotificationUserInfoKey.locationKey] as? CLLocation,
              let rawLocation = notification.userInfo?[RouteController.NotificationUserInfoKey.rawLocationKey] as? CLLocation
        else { return }
        
        let speedLimit = progress.currentLegProgress.currentSpeedLimit?.value ?? 0
        SwiftyBeaver.debug("Navigation progressDidChange: Speed = \(speedLimit) at \(location) - \(rawLocation)")
        
        limitSpeed = speedLimit
        
        // update road name and speed limit for mobile and carPlay
        let (overSpeed, over1min, over10mins) = LocationManager.shared.checkSpeeding(speedLimit: speedLimit)
        mobileMapViewUpdatable?.overSpeed(value: overSpeed)
        carPlayMapViewUpdatable?.overSpeed(value: overSpeed)
        
        
        // We will enable this after testing - https://github.com/mapbox-collab/ncs-collab/issues/527
        /*var encodedRouteString = ""
         if let coordinates = self.navigationService.route.shape?.coordinates{
         
         let path = Turf.LineString(coordinates)
         
         guard let pathDistance = path.distance() else {
         return
         }
         
         let alongPath = path.trimmed(from: progress.distanceTraveled, to: pathDistance)
         encodedRouteString = alongPath?.polylineEncodedString(precision: 1e6)
         print("Along Path",encodedRouteString)
         
         }*/
        
        //print("Route Geometry",progress.route.shape?.polylineEncodedString(precision: 1e6) ?? "")
        //print("Route Geometry current leg progress",progress.currentLegProgress.leg.shape.polylineEncodedString())
        if over10mins || over1min {
            SwiftyBeaver.debug("Overspeed - playAudioCue")
            appDelegate().playAudioCue()
        }
        
        // update mapView
        mobileMapViewUpdatable?.updateSpeedLimit(speedLimit)
        if let featureDetector = self.featureDetector {
            
            featureDetector.notify3kmRoadObjectsForNavigation(featureCollection: featureCollection3km, currentLocation: location, lineString: self.navigationService.route.shape)
            
        }
        // No need to set speed limit in carplay now since we're using default mapbox one
        //        carPlayMapViewUpdatable?.updateSpeedLimit(speedLimit)
        
        self.roadName = notification.userInfo?[RouteController.NotificationUserInfoKey.roadNameKey] as? String ?? ""
        print("Road name: \(self.roadName ?? "")")
        //        carPlayMapViewUpdatable?.updateRoadName(roadName)
        
        lastLocation = location
        
        //  Track user location for triggering OBU event
        if Date().timeIntervalSince(lastTimeUpdateLocation) >= TIME_DURATION {
            lastTimeUpdateLocation = Date()
            self.obuTrackLocation(location)
            
            let speed = location.speed * 3.6    // LocationManager.shared.getCurrentSpeed()
            currentSpeed = speed > 0 ? speed : 0
            
            //  Handle if user using mock OBU
            if (!OBUHelper.shared.isObuConnected() || OBUHelper.shared.getObuMode() != .device) {
                //                self.callApiGetCarparkCarPlay()
                self.handleShowParkingNearbyIfNeeded()
            }
            calculateIdleTime()
        }
        
        // live location update
        let now = Date().timeIntervalSince1970
        if isLiveLocationSharing() && !liveLocTripId.isEmpty && (now - lastLiveLocationUpdateTime) > 1 {
            //            print("time = \(now)")
            lastLiveLocationUpdateTime = now
            let estimatedTime = estimatedArrivalTime(progress.durationRemaining)
            DataCenter.shared.startUpdatingUserTripLiveLocation(tripId: liveLocTripId, latitude: location.coordinate.latitude.toString(), longitude: location.coordinate.longitude.toString(), status: LiveLocationStatusType.inprogress.rawValue,arrivalTime: "\(estimatedTime.time)\(estimatedTime.format)",coordinates: progress.route.shape?.polylineEncodedString(precision: 1e6) ?? "")
            //updateLiveLocationStatus(status: .inprogress)
        }
        
        // Trip log
        if tripETA != nil {
            if progress.durationRemaining < Values.durationForETAReachingReminder && !isLiveLocationSharing() {
                isReminder1Sent = true
            }
            
            if progress.durationRemaining <= Values.durationForETAReachingReminder && !isReminder1Sent && isLiveLocationSharing() {
                TripETAService().sendReminder(TripETAReminder(tripEtaUUID: self.liveLocTripId, type: TripETA.ETAType.Reminder1)) { result in
                    switch result {
                    case .failure(let err):
                        print(err)
                    case .success(let response):
                        self.isReminder1Sent = true
                        print("1 min (\(self.liveLocTripId)): \(response.message)")
                    }
                }
            }
        }
        
        if(self.carPlayMapViewUpdatable != nil)
        {
            //            var distance = ""
            let mapTemplate = appDelegate().carPlayManager.interfaceController?.rootTemplate as? CPMapTemplate
            //            if progress.distanceRemaining > 1000 {
            //
            //                let strDistance = String(format: "%.1f", progress.distanceRemaining/1000)
            //                distance = "\(strDistance) km"
            //
            //            } else {
            //
            //                distance = "\(Int(progress.distanceRemaining)) m"
            //
            //            }
            
            let routeDistance = Measurement(distance: progress.distanceRemaining).localized(into: Locale.enGBLocale())
            
            let estimates = CPTravelEstimates(distanceRemaining: routeDistance, timeRemaining: progress.durationRemaining)
            
            if(appDelegate().trip != nil)
            {
                mapTemplate?.update(estimates, for: appDelegate().trip!, with: CPTimeRemainingColor.default)
            }
            
            
        }
        
        currentProgress = progress
        
        //  Handle status where current distance to destination less than 1 KM
        handleUserReaches1KmToDestination(progress)
//        setZoomLevelWhenUserReaches1KmToDestination(progress)
        handleUserReaches1KmToCarPark(progress)
        
        //  TODO handle current progress to sent out a notification message
        if (self.carparkDest != nil || self.carpark != nil), let criteria = self.broadcastPlanning?.criterias.first {
            if !self.userSoonArrivingCarpark && ((criteria.hasDistance() && progress.distanceRemaining <= criteria.getDistance()) || (criteria.hasTime() && progress.durationRemaining <= criteria.getTime())) {
                self.userSoonArrivingCarpark = true
            }
        }
        
        zoneMonitor?.update(currentLocation: location.coordinate)
        
        if(!isDestinationReached){
            if let mapMatcherResult = notification.userInfo?[RouteController.NotificationUserInfoKey.mapMatchingResultKey] as? MapMatchingResult {
                if mapMatcherResult.isTeleport || mapMatcherResult.isOffRoad {
                    //  Just ignore the location when this condition happen
                } else {
                    tripLogger.update(location: location.coordinate)
                }
            } else {
                tripLogger.update(location: location.coordinate)
            }
            
            // We will enable this nearByCarParks when ever requried - https://breezeteam.atlassian.net/browse/BREEZES-5307
            self.fetchAndUpdateCarParks(progress: progress)
        }
        
        // still update user location after reaching destination
        
        // We don't need to update the puck explcitly after arrival as per https://github.com/mapbox-collab/ncs-collab/issues/546#issuecomment-1185423770
        /*if isDestinationReached {
         
         carPlayMapViewUpdatable?.navigationMapView.moveUserLocation(to: location,animated: true)
         mobileMapViewUpdatable?.navigationMapView.moveUserLocation(to: location,animated: true)
         }*/
        
        if !isDestinationReached {
            carPlayMapViewUpdatable?.navigationMapView?.moveUserLocation(to: location,animated: true)
            mobileMapViewUpdatable?.navigationMapView?.moveUserLocation(to: location,animated: true)
        }
       
    }
    
    func getCurrentRemainingTime() -> Double? {
        return currentProgress?.durationRemaining ?? nil
    }
    
    func handleUserReaches1KmToDestination(_ progressDistance: RouteProgress) {
        if progressDistance.distanceRemaining <= UserSettingsModel.sharedInstance.carparkListDistance {
            lessThan1kmFromDestination = true
            mobileMapViewUpdatable?.showParkingButton(true)
        } else {
            lessThan1kmFromDestination = false
            mobileMapViewUpdatable?.showParkingButton(false)
        }
    }
    
    func handleUserReaches2KmToDestination(_ progressDistance: RouteProgress) {
        if progressDistance.distanceRemaining <= UserSettingsModel.sharedInstance.carparkAvailabilityDistance, self.mobileMapViewUpdatable != nil {
            
            if !didFetchCarparkAvailability {
                didFetchCarparkAvailability = true
                
                //  BREEZE2-2006 - If user connected to DHU then no need handle parking availability
                if !appDelegate().carPlayConnected {
                    if let desCarpark = self.getCarpark() {
                        
                        CarparkService().checkParkingAvailability(desCarpark.name ?? "", lat: desCarpark.lat ?? 0.0, long: desCarpark.long ?? 0.0) { [weak self] result in
                            DispatchQueue.main.async {
                                guard let self = self else { return }
                                switch result {
                                case .success(let model):
                                 
                                    self.handleShowParkingAvailabilityAlert(model)
                                case .failure(let error):
                                    SwiftyBeaver.debug("Fetch destination carpark availability error: \(error.localizedDescription)")
                                }
                            }
                        }
                        
                    } else {
                        if let location = self.analyticsDest, let destinationWayPoint = getDestinationWaypoint() {
                            CarparkService().checkParkingAvailability(destinationWayPoint.name ?? "", lat: location.lat ?? 0.0, long: location.long ?? 0.0) { [weak self] result in
                                DispatchQueue.main.async {
                                    guard let self = self else { return }
                                    switch result {
                                    case .success(let model):
                                        self.handleShowParkingAvailabilityAlert(model)
                                    case .failure(let error):
                                        SwiftyBeaver.debug("Fetch destination carpark availability error: \(error.localizedDescription)")
                                    }
                                }
                            }
                        }
                    }
                }
            }
            
        }
    }
    
    func handleUserReaches1KmToCarPark(_ progressDistance: RouteProgress) {
        if progressDistance.distanceRemaining <= UserSettingsModel.sharedInstance.carparkListDistance, self.mobileMapViewUpdatable != nil {
            if !didUpdateCarpark1KmWay {
                didUpdateCarpark1KmWay = true
                
                if let desCarpark = self.getCarpark() {
                    CarparkService().checkParkingAvailability(desCarpark.name ?? "", lat: desCarpark.lat ?? 0.0, long: desCarpark.long ?? 0.0) { [weak self] result in
                        DispatchQueue.main.async {
                            guard let self = self else { return }
                            switch result {
                            case .success(let model):
//                                if let voiceCarparkAlert = model.data?.navVoiceAlert {
//                                    self.voiceInstructor.speak(text: voiceCarparkAlert)
//                                }
                                if let updater = self.carParkUpdater {
                                    updater.alternateCarpark = model
                                }
                                
                                self.alternateCarpark = model.data?.getAlternateCarpark()
                                if let alternateCarpark = model.data?.getAlternateCarpark() {
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { [weak self] in
                                        guard let self = self else { return }
                                        self.addAlternateCarpark(alternateCarpark)
                                    }
                                }
                            case .failure(let error):
                                SwiftyBeaver.debug("Fetch destination carpark availability error: \(error.localizedDescription)")
                            }
                        }
                    }
                    
                } else {
                    if let location = self.analyticsDest, let destinationWayPoint = getDestinationWaypoint() {
                        CarparkService().checkParkingAvailability(destinationWayPoint.name ?? "", lat: location.lat ?? 0.0, long: location.long ?? 0.0) { [weak self] result in
                            DispatchQueue.main.async {
                                guard let self = self else { return }
                                switch result {
                                case .success(let model):
//
//                                    if let voiceCarparkAlert = model.data?.navVoiceAlert {
//                                        self.voiceInstructor.speak(text: voiceCarparkAlert)
//                                    }
                                    if let updater = self.carParkUpdater {
                                        updater.alternateCarpark = model
                                    }
                                    self.alternateCarpark = model.data?.getAlternateCarpark()
                                    if let alternateCarpark = model.data?.getAlternateCarpark() {
                                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { [weak self] in
                                            guard let self = self else { return }
                                            self.addAlternateCarpark(alternateCarpark)
                                        }
                                    }
                                case .failure(let error):
                                    SwiftyBeaver.debug("Fetch destination carpark availability error: \(error.localizedDescription)")
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    private func addAlternateCarpark(_ model: Carpark) {
        if let mobileNavigationMapView = self.mobileMapViewUpdatable?.navigationMapView {
            let finalDestCoord = CLLocationCoordinate2D(latitude: originlDestWayPoint?.coordinate.latitude ?? 0.0, longitude: originlDestWayPoint?.coordinate.longitude ?? 0.0)
            if let updater = self.carParkUpdater {
                updater.removeCarParks(keepDestination: true)
                mobileNavigationMapView.addCarparks([model], ignoreDestination: updater.shouldIgnoreDestination, showOnlyWithCostSymbol: true, finalDestCoord: finalDestCoord,
                                                    belowPuck: false)
            }
            
        }
    }
    
    
    private func handleShowParkingAvailabilityAlert(_ model: AttributesCarparkModel) {
        if let voiceCarparkAlert = model.data?.navVoiceAlert {
            self.voiceInstructor.speak(text: voiceCarparkAlert)
        }
        SwiftyBeaver.debug(" test timedisplay \(UserSettingsModel.sharedInstance.carparkAvailabilityAlertDisplayTime)")
        let timeDisplay = Int(UserSettingsModel.sharedInstance.carparkAvailabilityAlertDisplayTime)
        self.mobileMapViewUpdatable?.showParkingAlert(carpark: self.getCarpark(), attribute: model, timeDisplay: timeDisplay)
    }
    
    /*
    func checkStopTimerZoomLevelIfNeeded() {
        if let progress = currentProgress {
            if progress.distanceRemaining <= 1000 {
                stopTimerUpdateZoomLevel()
            }
        }
    }
        
    private func setZoomLevelWhenUserReaches1KmToDestination(_ progressDistance: RouteProgress) {
        if progressDistance.distanceRemaining <= 1000 {
            if !lessThan1kmFromDestination && self.zoomInCounter < MAX_ZOOM_IN_COUNTER {
                lessThan1kmFromDestination = true
                
                if !(mobileMapViewUpdatable?.evchargerButton.isSelected == true || mobileMapViewUpdatable?.petrolButton.isSelected == true) {
                    if let location = self.lastLocation, !self.carParkCoordinates.isEmpty, let destination = self.originlDestWayPoint?.coordinate {
                        self.mobileMapViewUpdatable?.navigationMapView?.navigationCamera.stop()
                        let currentLocation = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
                        self.mobileMapViewUpdatable?.adjustDynamicZoomLevel(self.carParkCoordinates, currentLocation: currentLocation, destination: destination)
                        
                    }
                }
                startTimerUpdateZoomLevel()
            }
        } else {
            if lessThan1kmFromDestination {
                stopTimerUpdateZoomLevel()
            }
            lessThan1kmFromDestination = false
            zoomInCounter = 0
        }
    }
        
    func startTimerUpdateZoomLevel() {
        self.autoZoomTimer?.invalidate()
        self.autoZoomTimer = nil
        
        if zoomInCounter < MAX_ZOOM_IN_COUNTER {
            self.autoZoomTimer = Timer.scheduledTimer(withTimeInterval: 5.0, repeats: true) { [weak self] timer in
                guard let self = self else { return }
                if let progress = self.currentProgress {
                    if progress.distanceRemaining <= 1000 {
                        if !(self.mobileMapViewUpdatable?.evchargerButton.isSelected == true || self.mobileMapViewUpdatable?.petrolButton.isSelected == true) {
                            
                            if self.zoomInCounter < self.MAX_ZOOM_IN_COUNTER {
                                self.zoomInCounter += 1
                                if let location = self.lastLocation, !self.carParkCoordinates.isEmpty, let destination = self.originlDestWayPoint?.coordinate {
                                    self.mobileMapViewUpdatable?.navigationMapView?.navigationCamera.stop()
                                    let currentLocation = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
                                    self.mobileMapViewUpdatable?.adjustDynamicZoomLevel(self.carParkCoordinates, currentLocation: currentLocation, destination: destination)
                                }
                            } else {
                                self.stopTimerUpdateZoomLevel()
                            }
                        }
                    }
                }
            }
        }
    }
    
    func stopTimerUpdateZoomLevel() {
        self.autoZoomTimer?.invalidate()
        self.autoZoomTimer = nil
        self.mobileMapViewUpdatable?.navigationMapView?.navigationCamera.follow()
    }
     */
    
    func adjustZoomForCarParks(carParks:[Carpark]?){
        
        var distanceMonitor = 0
        var carParkCoordinate = [CLLocationCoordinate2D]()
        if let prevCarPark = carParks {
            
            prevCarPark.forEach { carPark in
                
                let distance = carPark.distance ?? 0
                if(distanceMonitor == 0){
                    
                    distanceMonitor = carPark.distance ?? 0
                    
                    if(carParkCoordinate.count > 0){
                        carParkCoordinate.removeAll()
                        carParkCoordinate.append(CLLocationCoordinate2D(latitude: carPark.lat ?? 0.0, longitude: carPark.long ?? 0.0))
                    }
                    else{
                        
                        carParkCoordinate.append(CLLocationCoordinate2D(latitude: carPark.lat ?? 0.0, longitude: carPark.long ?? 0.0))
                    }
                }
                    
                    if(distanceMonitor < distance){
                        
                        distanceMonitor = distance
                        if(carParkCoordinate.count > 0){
                            carParkCoordinate.removeAll()
                            carParkCoordinate.append(CLLocationCoordinate2D(latitude: carPark.lat ?? 0.0, longitude: carPark.long ?? 0.0))
                        }
                        else{
                            
                            carParkCoordinate.append(CLLocationCoordinate2D(latitude: carPark.lat ?? 0.0, longitude: carPark.long ?? 0.0))
                        }
                    }
                
            }
            
            if carParkCoordinate.count > 0 {
                
                distanceMonitor = max(distanceMonitor, 50)
                
                DispatchQueue.main.async {
//                  self.navMapView.mapView.setDynamicZoomLevelBasedonRadius(coordinate1: carParkCoordinate[0], dynamicRadius: distanceMonitor, padding: self.cameraPadding(), coordinate2: CLLocationCoordinate2D(latitude: self.viewModel.address.lat, longitude: self.viewModel.address.long))
//                    print(self.navMapView.mapView.cameraState.zoom)
                }
                
            }
            
            
        }
    }
    
    
    private func fetchAndUpdateCarParks(progress: RouteProgress){
        
        if(progress.routeOptions.waypoints.count > 2) {
            let remainingWayPoints = remainingWaypointsForCalculatingRoute(routeOptions: progress.routeOptions, legIndex: progress.legIndex,routeProgress: progress.currentLegProgress)
            if(remainingWayPoints.count > 0){
                otherWayPoints = remainingWayPoints
            }
        }
        
        if let finalDestWayPoint = progress.routeOptions.waypoints.last {
            
            originlDestWayPoint = finalDestWayPoint
            
            startZoneMonitor()
            
            //  Santoso: BREEZE2-1545 - Remove get carpark list nearby the destinatinon on mobile but still showing on Carplay
            if carParkUpdater == nil && carParkTime == nil && (self.mobileMapViewUpdatable?.navigationMapView != nil || self.carPlayMapViewUpdatable?.navigationMapView != nil) {
                
                //  Delay 7 seconds then call API get carparks nearby the destination
//                carParkTime =   Timer.scheduledTimer(withTimeInterval: 7, repeats: false, block: { [weak self] _ in
//                    guard let self = self else { return }
                    
                    self.carParkUpdater =  CarParkUpdater(destName: finalDestWayPoint.name ?? "",
                                                          carPlayNavigationMapView: self.carPlayMapViewUpdatable?.navigationMapView,
                                                          mobileNavigationMapView: self.mobileMapViewUpdatable?.navigationMapView,
                                                          fromScreen: .navigation)
                
                    self.carParkUpdater?.shouldIgnoreDestination = true//self.carpark == nil
                    self.carParkUpdater?.delegate = self
                    
                //  Dont load carpark nearby the destination
//                    self.carParkUpdater?.loadCarParks(maxCarParksCount: Values.maxNoOfCarparksInCruiseMode, radius: Values.carparkMonitorDistanceInLanding,
//                                                      location: finalDestWayPoint.coordinate,
//                                                      checkAvailability: self.carpark != nil,
//                                                      carPark: self.carpark,
//                                                      finalDestCoord:self.navigationService.route.shape?.coordinates.last ?? nil,  shortTermOnly: true)
                    
                    
//                })
                
            } else {
                // Should not refresh again if walking path is shown
                if !shouldShowCarparkArrival {
                    
                    handleUserReaches2KmToDestination(progress)
                    
                    //  Distance reaches to <= 1km from destination then call API to get parking availability
//                    if(progress.distanceRemaining <= Values.navigationShowParkingAvailabilityDistance) {
//
//                        mobileMapViewUpdatable?.showParkingButton(true)
                        
                        /*
                        if let carParkUpdater = carParkUpdater, let currentLocation = lastLocation { //  let originlDestWayPoint = originlDestWayPoint
                            
                            //  This if condition to make sure that we handle loading carpark nearby location just once
                            if carParkUpdater.isRefreshCarPark {
                                
                                //  If final destination is CarPark
                                if let carpark = getCarpark() {
                                    
                                    carParkUpdater.isRefreshCarPark = false
                                    
//                                    carParkUpdater.loadCarParks(maxCarParksCount: Values.maxNoOfCarparksInCruiseMode + 1, radius: Values.carparkMonitorDistanceInCruise, location: originlDestWayPoint.coordinate, checkAvailability: true, carPark: carpark,finalDestCoord: navigationService.route.shape?.coordinates.last ?? nil,shortTermOnly: true)
                                    
                                    carParkUpdater.loadCarParks(maxCarParksCount: Values.maxNoOfCarparksInCruiseMode + 1, radius: Values.carparkMonitorDistanceInCruise, location: currentLocation.coordinate, checkAvailability: true, carPark: carpark,finalDestCoord: navigationService.route.shape?.coordinates.last ?? nil,shortTermOnly: true)
                                    
                                    carParkUpdater.$isLowAvailability.sink { [weak self] isLowAvailability in
                                        guard let self = self else { return }
                                        
                                        if isLowAvailability{
                                            self.voicInstructionOtherText(text: Constants.carParkLowAnnouncement)
                                        }
                                    }.store(in: &disposables)
                                    
                                } else {
                                    carParkUpdater.isRefreshCarPark = false
                                    carParkUpdater.refreshCarParks(maxCarParksCount: Values.maxNoOfCarparksInCruiseMode, radius: Values.carparkMonitorDistanceInLanding, location: currentLocation.coordinate,finalDestCoord:navigationService.route.shape?.coordinates.last ?? nil)
                                }
                            }
                            
                        }
                         */
//                    } else {
//                        mobileMapViewUpdatable?.showParkingButton(false)
//                    }
                }
                
                
            }
        }
    }
    
    private func remainingWaypointsForCalculatingRoute(routeOptions:RouteOptions,legIndex:Int,routeProgress:RouteLegProgress) -> [Waypoint] {
        
        let (currentLegViaPoints, remainingWaypoints) = routeOptions.waypoints(fromLegAt: legIndex)
        let currentLegRemainingViaPoints = self.remainingWaypoints(among: currentLegViaPoints,routeLegProgress: routeProgress)
        return currentLegRemainingViaPoints + remainingWaypoints
    }
    
    private func remainingWaypoints(among waypoints: [Waypoint], routeLegProgress:RouteLegProgress) -> [Waypoint] {
        guard waypoints.count > 1 else {
            // The leg has only a source and no via points. Save ourselves a call to RouteLeg.coordinates, which can be expensive.
            return []
        }
        let legPolyline = routeLegProgress.leg.shape
        guard let userCoordinateIndex = legPolyline.indexedCoordinateFromStart(distance: routeLegProgress.distanceTraveled)?.index else {
            // The leg is empty, so none of the waypoints are meaningful.
            return []
        }
        var slice = legPolyline
        var accumulatedCoordinates = 0
        return Array(waypoints.drop { (waypoint) -> Bool in
            let newSlice = slice.sliced(from: waypoint.coordinate)!
            accumulatedCoordinates += slice.coordinates.count - newSlice.coordinates.count
            slice = newSlice
            return accumulatedCoordinates <= userCoordinateIndex
        })
    }
    
    // reroute because click navigate here from carpark tooltip
    private func reroute(carpark: Carpark, source: CLLocationCoordinate2D, destination: CLLocationCoordinate2D,destName:String) {
        
        let origin = Waypoint(coordinate: source, coordinateAccuracy: -1, name: "")
        let destination = Waypoint(coordinate: destination, coordinateAccuracy: -1, name: destName)
        
        DirectionService.shared.carParkRouteResponse(origin: origin, destination: destination,otherWayPoints: self.otherWayPoints) { response, routeType in
            
            guard let response = response else { return }
            
            let responseOptions = response.options
            if case let .route(routeOptions) = responseOptions{
                
                self.navigationService.router.updateRoute(with: IndexedRouteResponse(routeResponse: response,routeIndex: 0), routeOptions: routeOptions) { _ in
                    
                    DispatchQueue.main.async { [weak self] in
                        
                        guard let self = self else { return }
                        
                        //self.carParkUpdater?.isRefreshCarPark = false
                        
                        
                        if let routes = response.routes {
                            
                            let firstCoordinate = routes[0].shape?.coordinates.last
                            
                            if let initalCoord = firstCoordinate {
                                // show walking path from carpark to original destination
                                // the original destination is not a carpark then show the walking path
                                
                                if self.needShowWalkingPath {
                                    
                                    if let originlDestWayPoint = self.originlDestWayPoint {
                                        if self.destWaypoint == nil {
                                            self.destWaypoint = Waypoint(coordinate: originlDestWayPoint.coordinate, coordinateAccuracy: originlDestWayPoint.coordinateAccuracy, name: originlDestWayPoint.name)
                                        }
                                        
                                        if let originalDest = self.destWaypoint {
                                            let imgName = getcarparkImageName(carpark: carpark, selected: false, ignoreDestination: true)
                                            self.updateDestinationAnnotation(imageName: imgName.0, at: initalCoord)
                                            self.showWalkingPath(carPark: carpark, source: destination.coordinate, destinationName: originalDest.name ?? "Destination", destinationCoordinate: originalDest.coordinate)
                                        }
                                        //                                        let imgName = getcarparkImageName(carpark: carpark, selected: false, ignoreDestination: true)
                                        //                                        self.updateDestinationAnnotation(imageName: imgName.0, at: initalCoord)
                                        //                                        self.showWalkingPath(carPark: carpark, source: destination.coordinate, destinationName: originlDestWayPoint.name ?? "Destination", destinationCoordinate: originlDestWayPoint.coordinate)
                                        
                                    } else {
                                        // destination is carpark, no need to show walking path and destination icon should be changed
                                        self.updateDestinationAnnotation(imageName: "destinationMark", at: initalCoord)
                                    }
                                } else {
                                    self.updateDestinationAnnotation(imageName: "destinationMark", at: initalCoord)
                                }
                            }
                        }
                        
                        // update carpark here, otherwise the walking path will not be shown
                        self.carpark = carpark
                    }
                    
                }
            }
            
        }
        
    }
    
    private func updateDestinationAnnotation(imageName: String, at coordinate: CLLocationCoordinate2D) {
        var destinationAnnotation = PointAnnotation(coordinate: coordinate)
        destinationAnnotation.image = .init(image: UIImage(named: imageName)!, name: "initial-navigationDest")
        
        if let mobileMapViewUpdatable = self.mobileMapViewUpdatable {
            mobileMapViewUpdatable.navigationMapView?.pointAnnotationManager?.annotations = [destinationAnnotation]
        }
        
        if let carPlayMapViewUpdatable = self.carPlayMapViewUpdatable {
            carPlayMapViewUpdatable.navigationMapView?.pointAnnotationManager?.annotations = [destinationAnnotation]
        }
    }
    
    
    // reroute because click add stop from ev and petrol tooltip
    func rerouteEVandPetrol(source: CLLocationCoordinate2D, destName:String, type: String) {
        let origin = Waypoint(coordinate: source, coordinateAccuracy: -1, name: "")
        let destinationWaypoint = CLLocationCoordinate2D(latitude: originlDestWayPoint?.coordinate.latitude ?? 0.0, longitude: originlDestWayPoint?.coordinate.longitude ?? 0.0)
        let destination = Waypoint(coordinate: destinationWaypoint, coordinateAccuracy: -1, name: destName)
        
        DirectionService.shared.amenityEVandPetrolRouteResponse(origin: origin, destination: destination, otherWayPoints: self.routeStopsAdded) { response, routeType in
            
            guard let response = response else { return }
            
            let responseOptions = response.options
            if case let .route(routeOptions) = responseOptions {
                
                self.navigationService.router.updateRoute(with: IndexedRouteResponse(routeResponse: response,routeIndex: 0), routeOptions: routeOptions) { _ in
                    DispatchQueue.main.async { [weak self] in
                        guard let self = self else { return }
//                        self.removeLayerTypeFrom(type: type)
                    }
                }
            }
            
        }
    }
    
    @objc func willReRouted(_ notification: NSNotification){
        
//        SwiftyBeaver.error("willReRouted")
        AVAudioSession.sharedInstance().tryDuckOtherAudio()
        appDelegate().rivaSpeech?.setRerouting(true)
//        NotificationCenter.default.post(name: .willReroute, object: nil)
    }
    
    @objc func rerouted(_ notification: NSNotification) {
        
//        SwiftyBeaver.error("rerouted")
        if let riva = appDelegate().rivaSpeech, riva.shouldUnduckOtherAudio() {
            AVAudioSession.sharedInstance().tryUnduckOtherAudio()
        }
        appDelegate().rivaSpeech?.setRerouting(false)
//        NotificationCenter.default.post(name: .reroutingEnded, object: nil)
        
        guard let progress = notification.userInfo?[RouteController.NotificationUserInfoKey.routeProgressKey] as? RouteProgress else { return }
        
        let route = progress.route
        // reroute happened, calculate the erp on the route
        updateActiveERP(routes: [route], costfeatures: DataCenter.shared.getAllERPFeatureCollection())        
    }
    
    @objc func failedReroute(_ notification: NSNotification) {
        if let riva = appDelegate().rivaSpeech, riva.shouldUnduckOtherAudio() {
            AVAudioSession.sharedInstance().tryUnduckOtherAudio()
        }
        
        appDelegate().rivaSpeech?.setRerouting(false)
//        NotificationCenter.default.post(name: .reroutingEnded, object: nil)
    }
    
//    @objc func roadNameUpdated(_ notification: NSNotification) {
//        let roadName = notification.userInfo?[RouteController.NotificationUserInfoKey.roadNameKey] as? String ?? ""
//        carPlayMapViewUpdatable?.updateRoadName(roadName)
//    }
    
    @objc func didArrival(_ notification: NSNotification) {
        if let wayPoint = notification.userInfo?[RouteController.NotificationUserInfoKey.waypointKey] as? Waypoint {
            if let carPlayNavigationViewController = appDelegate().carPlayManager.carPlayNavigationViewController {
                if carPlayNavigationViewController.navigationService.routeProgress.isFinalLeg {
                    
                    SwiftyBeaver.debug("Navigation View Model Did Arrive called")
                    if let location = self.navigationService.router.location{
                        
                        SwiftyBeaver.debug("Navigation View Model Did Arrive called and Going to Save Trip Log")
                        self.saveTripLog(location: location, fromUser: false) { _ in
                            
                            let _ = carPlayNavigationViewController.navigationService(self.navigationService, didArriveAt: wayPoint)
                        }
                    }
                    else{
                        
                        SwiftyBeaver.debug("Navigation View Model Did Arrive called and Going to Save Trip Log else condition")
                        self.saveTripLog(location: LocationManager.shared.location, fromUser: false) { _ in
                            
                            let _ = carPlayNavigationViewController.navigationService(self.navigationService, didArriveAt: wayPoint)
                        }
                    }
                   
                }
            }
        }
    }
    
    private func isLiveLocationSharing() -> Bool {
        guard tripETA != nil else { return false }
        return isLiveLocationSharingEnabled
    }
    
    // TODO: This is also can be reusable. 
    private func onNewFeatureUpdated(_ feature: FeatureDetectable?) {
        if let feature = feature {
            // only speak when there is new feature
            if(!isDestinationReached){
                
                SwiftyBeaver.debug("onNewFeatureUpdated: \(feature.type())")
                
                let extraData: NSDictionary = ["message": "message_\(feature.type())", "location_name": "location_\(self.roadName ?? "")", "longitude": lastLocation?.coordinate.longitude ?? 0.0, "latitude": lastLocation?.coordinate.latitude ?? 0.0]
                
                AnalyticsManager.shared.logOpenPopupEventV2(popupName: ParameterName.NavigationMode.UserPopup.navigation_notification, screenName: ParameterName.NavigationMode.screen_view, extraData: extraData)
                
                //  Should ignore RoadClosure when OBU connected
                if OBUHelper.shared.isObuConnected() {
                    if feature.type() == TrafficIncidentCategories.RoadClosure {
                        return
                    }
                }

//                if !OBUHelper.shared.isObuConnected() {
                    if feature is DetectedSeasonParking {
                        // voice alert
                        if !isMute {
                            self.voiceInstructor.speak(text: Constants.seasonParkingVoice)
                        }
                        return
                    } else {
                        self.voiceInstruction(for: feature)
                    }
//                }
                
                // For some reason, the navigationMapView.mapView.location.latestLocation is nil in navigation mode
                if let coordinate = self.navigationService.router.location?.coordinate {
                    if let feature = feature as? DetectedSchoolZone {
                        DispatchQueue.main.async { [weak self] in
                            
                            guard let self = self else { return }
                            
                            self.mobileMapViewUpdatable?.addSchoolZoneSymbol(feature, at: coordinate)
                            self.carPlayMapViewUpdatable?.addSchoolZoneSymbol(feature, at: coordinate)
                        }
                        
                    } else if let feature = feature as? DetectedSilverZone {
                        DispatchQueue.main.async { [weak self] in
                            
                            guard let self = self else { return }
                            
                            self.mobileMapViewUpdatable?.addSilverZoneSymbol(feature, at: coordinate)
                            self.carPlayMapViewUpdatable?.addSilverZoneSymbol(feature, at: coordinate)
                        }
                        
                    }
                }
            }
            
        }
    }
    
    private func onOldFeatureRemoved(_ feature: FeatureDetectable) {
        
        if let feature = feature as? DetectedSchoolZone {
            mobileMapViewUpdatable?.removeSymbol(feature.id)
            carPlayMapViewUpdatable?.removeSymbol(feature.id)
        } else if let feature = feature as? DetectedSilverZone {
            mobileMapViewUpdatable?.removeSymbol(feature.id)
            carPlayMapViewUpdatable?.removeSymbol(feature.id)
        }
    }
    
    func voiceInstruction(for feature: FeatureDetectable) {
        if !isMute {
            SwiftyBeaver.debug("TurnByTurn Navigation View Model Voicen Instructor speak \(feature)")
            voiceInstructor.speak(feature)
        }
    }
    
    func voicInstructionOtherText(text:String){
        if !isMute{
            voiceInstructor.speak(text: text,distanceAlongStep: 0)
        }
    }
    
    func clearAudioQueue(){
        
        if let synthesizer = voiceInstructor.synthesizer as? MultiplexedSpeechSynthesizer {
            synthesizer.speechSynthesizers.forEach { 
                if let audioQueable = $0 as? AudioQueuable {
                    audioQueable.clearAudioQueue()
                }
            }
        }
    }
    
    func stopSpeakingAudioInstructions(){
        
        voiceInstructor.synthesizer.stopSpeaking()
    }
    
    func announceFinalVoiceMessage(){
        // Disable the final voice based on Chethana's requirement
//        self.voicInstructionOtherText(text: Constants.cpArrival)
    }
    
    func reRouteIfNeeded(){
        
        if(!self.navigationService.router.userIsOnRoute(LocationManager.shared.location)){
            
            print("User is not on the route")
            
            if let currentProgress = currentProgress {
                
                self.navigationService.router.reroute(from: LocationManager.shared.location, along: currentProgress)
            }
            
        }
    }
    
    //  Fetch broadcast message
    func fetchBroadcastMessage(_ carpark: Carpark?, mode: BroadcastMode) {
        if let ID = carpark?.id, !ID.isEmpty {
            BroadcastMessageService().getBroadcastMessage(carparkId: ID, mode: mode) { [weak self] result in
                switch result {
                case .success(let broadcast):
                    DispatchQueue.main.async {
                        if mode == .planning {
                            self?.broadcastPlanning = broadcast
                        }else {
                            if let desCarpark = self?.getCarpark() {
                                self?.updateNotificationWhenUserSoonArrive(carpark: desCarpark, broadcast: broadcast)
                            }
                        }
                    }
                case .failure(let error):
                    SwiftyBeaver.error("Failed to get collection: \(error.localizedDescription)")
                }
            }
        }
    }
    
    private func obuTrackLocation(_ location: CLLocation) {
        
//        if !OBUHelper.shared.isObuConnected() {
//            return
//        }
        
        var data = ""
        let tripId = tripLogger?.getTripGUID() ?? ""
        if let name = self.roadName, !name.isEmpty {
            data = "{\"roadName\" : \"\(self.roadName ?? "")\", \"tripGUID\":\"\(tripId)\"}"
        }  else {
            data = "{\"tripGUID\":\"\(tripId)\"}"
        }
        
        var bearing = ""
        let userId = AWSAuth.sharedInstance.cognitoId
        
        if let mobileUpdatable = mobileMapViewUpdatable {
            bearing = mobileUpdatable.navigationMapView?.mapView.cameraState.bearing.toString() ?? ""
        } else {
            bearing = carPlayMapViewUpdatable?.navigationMapView?.mapView.cameraState.bearing.toString() ?? ""
        }
        
        if OBUHelper.shared.getObuMode() == .mockServer {
            DataCenter.shared.startUpdatingDeviceUserLocation(userId: userId, bearing: bearing, latitude: location.coordinate.latitude.toString(), longitude: location.coordinate.longitude.toString(), data: data)
        } else {
            DataCenter.shared.startUpdatingOBUDeviceUserLocation(userId: userId, bearing: bearing, latitude: location.coordinate.latitude.toString(), longitude: location.coordinate.longitude.toString(), data: data)
        }
        
    }
    
    private func calculateIdleTime() {
        if currentSpeed < OBUHelper.shared.idleSpeed {
            if let lastTime = watchdogIdleTime {
                let duration = Date().timeIntervalSince(lastTime)
                if duration > 0 {
                    idleTime += duration
                    print("Idle time: \(idleTime)")
                }
            }
            watchdogIdleTime = Date()
        } else {
            watchdogIdleTime = nil
        }
    }
    
//    private func showCarparkNearbyInCarplay(carparks: [Carpark]? = [], isShow: Bool) {
//        if appDelegate().carPlayConnected {
//            if isShow {
//
//                self.removeCarparks()
//                self.carparkCoordinates.removeAll()
//                if let updater = carParkUpdater {
//                    updater.removeCarParks(keepDestination: true)
//                }
//
//                SwiftyBeaver.debug("Navigation showCarparkNearbyInCarplay count \(carparks?.count ?? 0)")
//                if let list = carparks, !list.isEmpty {
//                    carPlayMapViewUpdatable?.navigationMapView?.mapView.addCarparks(list, showOnlyWithCostSymbol: true, belowPuck: false)
//                    if let lastLocation = self.lastLocation {
//                        let currentLocation = CLLocationCoordinate2D(latitude: lastLocation.coordinate.latitude, longitude: lastLocation.coordinate.longitude)
//                        for carpark in list {
//                            self.carparkCoordinates.append(CLLocationCoordinate2D(latitude: carpark.lat ?? 0.0, longitude: carpark.long ?? 0.0))
//                        }
//                        carPlayMapViewUpdatable?.adjustDynamicZoomLevelWhenCarParkDisplay(self.carparkCoordinates, currentLocation: currentLocation)
//                    }
//                    carPlayMapViewUpdatable?.navigationMapView?.navigationCamera.stop()
//
//                }
//            } else {
//                SwiftyBeaver.debug("Navigation showCarparkNearbyInCarplay hide carparks")
//                self.removeCarparks()
//                self.carparkCoordinates.removeAll()
//                if let updater = carParkUpdater {
//                    updater.removeCarParks(keepDestination: true)
//                }
////                carPlayMapViewUpdatable?.navigationMapView?.navigationCamera.follow()
//            }
//        }
//    }
    
    private func loadCarparksNearByWhenSpeedIsZero() {
        retryCounter = 0
        userIsMoving = false
        
        removeCarparks()
        //  Otherwise will hit API to show carpark nearby
        SwiftyBeaver.debug("Navigation handleShowParkingNearbyIfNeeded show carpark nearby: \(currentSpeed) km/h")
        showCarparkWhenSpeedIsZero = true
        if !isDestinationReached {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: { [weak self] in
                if let mobileUpdatable = self?.mobileMapViewUpdatable {
                    mobileUpdatable.showCarparkNearbyWhileStopDriving()
                } else {
                    //  If only carplay running
                    self?.onParkingEnabled(true)
                }
//                self?.callApiGetCarparkCarPlay()
            })
        }
        
        checkCloseCarparkList(.EVENT)
    }
    
    private func handleShowParkingNearbyIfNeeded() {
        
        // MARK: Just change value for mobile
        if !appDelegate().carPlayConnected {
            if !lessThan1kmFromDestination {
                SwiftyBeaver.debug("handleShowParkingNearbyIfNeeded mobile only")
                return
            }
        }
        
        SwiftyBeaver.debug("speed: \(currentSpeed) km/h")
                
        if currentSpeed < OBUHelper.shared.idleSpeedZero {
            //  When user change state from moving to stopping then show carpark nearby
            if userIsMoving {
                                  
                if let _ = mobileMapViewUpdatable {
                    if mobileMapViewUpdatable?.isShowingAmenity() == true {
                        //  Do nothing
                        SwiftyBeaver.debug("handleShowParkingNearbyIfNeeded stop moving but showing amenity, current speed: \(currentSpeed) km/h")
                    } else {
                        loadCarparksNearByWhenSpeedIsZero()
                    }
                } else {
                    loadCarparksNearByWhenSpeedIsZero()
                }
            }
            
        } else {
            //  User is moving
            if !userIsMoving {
                //  When user change state from stop to moving immediately toggle off carpark nearby
                SwiftyBeaver.debug("Navigation handleShowParkingNearbyIfNeeded stopping to moving then toggle off: \(currentSpeed) km/h")
                DispatchQueue.main.async { [weak self] in
//                    self?.showCarparkNearbyInCarplay(isShow: false)
                    if let _ = self?.mobileMapViewUpdatable {
                        self?.mobileMapViewUpdatable?.toggleOffCarparkNearby()
                    } else {
                        self?.onParkingEnabled(false)
                    }
                    self?.recenter()
                }
            }
            
            showCarparkWhenSpeedIsZero = false
            //  Then update moving state to true
            userIsMoving = true
        }
    }
    
    func recenter() {
        self.mobileMapViewUpdatable?.navigationMapView?.navigationCamera.follow()
        self.carPlayMapViewUpdatable?.navigationMapView?.navigationCamera.follow()
//        mobileMapViewUpdatable?.resetCamera(isCruise: false)
//        carPlayMapViewUpdatable?.resetCamera(isCruise: true)
    }
    
    deinit {
        SwiftyBeaver.info("TurnByTurnNavigationViewModel deinit")
        notifyArrivalAction = nil
//        appDelegate().navigationSubmitHistoryData(description: "Navigation-End")
//        updateNotification(featureId: nil)
        voiceInstructor.synthesizer.stopSpeaking()
        voiceInstructor.synthesizer.delegate = nil        
        DataCenter.shared.removeERPUpdaterListner(listner: self)
        DataCenter.shared.removeTrafficUpdaterListener(listener: self)
        DataCenter.shared.unsubscribeToMessageInbox()
        self.stopUpdatingeHorizon()//explicitly stop eHorizon
        carPlayMapViewUpdatable?.delegate = nil
        carPlayMapViewUpdatable = nil
        mobileMapViewUpdatable?.delegate = nil
        mobileMapViewUpdatable = nil
        
        OBUHelper.shared.stopObuMockEvent()
        OBUHelper.shared.stopBreezeMockEvent()
        OBUHelper.shared.stopObuEvent()
        
        NotificationCenter.default.removeObserver(self)
    }
    
    func fetchEVorPetrolAmenities(amenity: String) {
        if let lastLocation = self.lastLocation {
            let lat = lastLocation.coordinate.latitude
            let long = lastLocation.coordinate.longitude
            let location2D =  CLLocationCoordinate2D(latitude: lat, longitude: long)
            if amenity == Values.PETROL {
                AmenitiesSharedInstance.shared.fetchPetrolAmenities(location: location2D) { [weak self] _ in
                    DispatchQueue.main.async {
                        self?.updateMapWithAmenities(selectedID: amenity)
                    }
                }
            } else if amenity == Values.EV_CHARGER {
                AmenitiesSharedInstance.shared.fetchEVAmenities(location: location2D) { [weak self] _ in
                    DispatchQueue.main.async {
                        self?.updateMapWithAmenities(selectedID: amenity)
                    }
                }
            }
        }
    }
    
//    func getEvAmenitiesOnTurnByTurn(amenity: String) {
//        if let lastLocation = self.lastLocation {
//            let lat = lastLocation.coordinate.latitude
//            let long = lastLocation.coordinate.longitude
//            let location2D =  CLLocationCoordinate2D(latitude: lat, longitude: long)
//            AmenitiesSharedInstance.shared.callAmenitiesApiForEV(location: location2D, destination: location2D)
//            updateMapWithAmenitiesForEV(selectedID: amenity)
//        }
//
//    }
//
//    func getPetrolAmenitiesTurnByTurn(amenity: String) {
//        if let lastLocation = self.lastLocation {
//            let lat = lastLocation.coordinate.latitude
//            let long = lastLocation.coordinate.longitude
//            let location2D =  CLLocationCoordinate2D(latitude: lat, longitude: long)
//            AmenitiesSharedInstance.shared.callAmenitiesApiForPetrol(location: location2D, destination: location2D)
//            updateMapWithAmenitiesPetrol(selectedID: amenity)
//        }
//    }
    
    
    func updateMapWithAmenities(selectedID: String = "") {
        
        self.amenityCoordinates.removeAll()
        
        var turfArray = [Turf.Feature]()
        let allKeys = AmenitiesSharedInstance.shared.turnbyturnPetrolOrEVSymbolLayers.keys
         
        
        if allKeys.isEmpty {
            //  There is no data
            DispatchQueue.main.async {
                if selectedID == Values.EV_CHARGER {
                    NotificationCenter.default.post(name: .onNoDataNotification, object: 2)
                } else if selectedID == Values.PETROL {
                    NotificationCenter.default.post(name: .onNoDataNotification, object: 1)
                }
            }
           
        }
        
        var needShowNodata = true
        
        if allKeys.isEmpty {
            //  There is no data
            DispatchQueue.main.async {
                if selectedID == Values.EV_CHARGER {
                    NotificationCenter.default.post(name: .onNoDataNotification, object: 2)
                } else if selectedID == Values.PETROL {
                    NotificationCenter.default.post(name: .onNoDataNotification, object: 1)
                }
            }
        }
        
        for key in allKeys {
            
            if key == selectedID {
                let array = AmenitiesSharedInstance.shared.turnbyturnPetrolOrEVSymbolLayers[key] as! [Any]
                
                print(array)
                for item in array {
                    let itemDict = item as! [String:Any]
                    if(turfArray.count < 5){
                        if selectedID == Values.PETROL {
                            let feature = self.getAmenitiesForPetrol(location: itemDict, type: key, selectedId: selectedID)
                            turfArray.append(feature)
                        } else if selectedID == Values.EV_CHARGER {
                            let feature = self.getAmenitiesFeaturesForEV(location: itemDict, type: key, selectedId: selectedID)
                            turfArray.append(feature)
                        }
                    }
                }
                
                let amenityFeatureCollection = FeatureCollection(features: turfArray)
                if selectedID == Values.PETROL {
                    self.updateAmenitiesOnMap(collection: amenityFeatureCollection,type: Values.PETROL)
                } else if selectedID == Values.EV_CHARGER {
                    self.updateAmenitiesOnMap(collection: amenityFeatureCollection,type: Values.EV_CHARGER)
                }
                
                //  Handle show there is no data for parking/ev/petrol
                if turfArray.isEmpty {
                    needShowNodata = false
                    DispatchQueue.main.async {
                        if selectedID == Values.EV_CHARGER {
                            NotificationCenter.default.post(name: .onNoDataNotification, object: 2)
                        } else if selectedID == Values.PETROL {
                            NotificationCenter.default.post(name: .onNoDataNotification, object: 1)
                        }
                    }
                } else {
                    //  Handle zoom level
                    needShowNodata = false
                    if let location = lastLocation {
                        let currentLocation = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
                        self.mobileMapViewUpdatable?.adjustDynamicZoomLevel(self.amenityCoordinates, currentLocation: currentLocation)
                        self.mobileMapViewUpdatable?.navigationMapView?.navigationCamera.stop()
                    }
                }
                break
            }
        }
        
        if needShowNodata {
            DispatchQueue.main.async {
                if selectedID == Values.EV_CHARGER {
                    NotificationCenter.default.post(name: .onNoDataNotification, object: 2)
                } else if selectedID == Values.PETROL {
                    NotificationCenter.default.post(name: .onNoDataNotification, object: 1)
                }
            }
        }
    }
    
    func updateAmenitiesOnMap(collection:Turf.FeatureCollection,type:String){
        mobileMapViewUpdatable?.navigationMapView?.mapView.removeAmenitiesPetrolAndEVLayer(type: "\(type)")
        mobileMapViewUpdatable?.navigationMapView?.mapView.addPetrolEvToMapTurnbyTurn(features: collection, type: "\(type)", isBelowPuck: false, isSelectedAmenity: self.isSelectedAmenity)
    }
    
    
    
    private func getAmenitiesFeaturesForEV(location:[String:Any],type:String, selectedId: String = "")-> Turf.Feature {
        
        let coordinate = CLLocationCoordinate2D(latitude: location["lat"] as! Double, longitude: location["long"] as! Double)
        self.amenityCoordinates.append(coordinate)
        let name = location["name"] as! String
        let address = location["address"] as! String
        let sName = location["stationName"] as! String
        let amenityID = location["id"] as! String
        
        let multiplePlugTypes = location["plugType"] as! Array<String>
        
        var jsonArray = JSONArray()
        for type in multiplePlugTypes {
            let value = JSONValue(type)
            jsonArray.append(value)
        }
        
        let isSelected = !selectedId.isEmpty && selectedId == amenityID
        var feature = Turf.Feature(geometry: .point(Point(coordinate)))
        feature.properties = [
            "type":.string(type),
            "name":.string(name),
            "address":.string(address),
            "stationName":.string(sName),
            "image_id":.string("\(amenitySymbolThemeType)\(type)"),
            "plugTypes":.array(jsonArray),
            "amenityID":.string(amenityID),
            "isSelected": .boolean(isSelected)
        ]
        self.isSelectedAmenity = isSelected
        return feature
    }
    
    
    private func getAmenitiesForPetrol(location:[String:Any],type:String, selectedId: String = "")-> Turf.Feature {
        
        let coordinate = CLLocationCoordinate2D(latitude: location["lat"] as! Double, longitude: location["long"] as! Double)
        self.amenityCoordinates.append(coordinate)
        let name = location["name"] as! String
        let address = location["address"] as! String
        let sName = location["stationName"] as! String
        let amenityID = location["id"] as! String
        let isSelected = !selectedId.isEmpty && selectedId == amenityID
        
        var feature = Turf.Feature(geometry: .point(Point(coordinate)))
        feature.properties = [
            "type":.string(type),
            "name":.string(name),
            "address":.string(address),
            "stationName":.string(sName),
            "image_id":.string("\(amenitySymbolThemeType)\(type)"),
            "amenityID":.string(amenityID),
            "isSelected": .boolean(isSelected)
        ]
        return feature
    }
    
    
    
//    func checkToRemoveAddedStop() {
//        let navigationMapView = self.navigationService
//        if let locationSource = navigationMapView.router.location {
//            let currentLocation = CLLocationCoordinate2D(latitude: locationSource.coordinate.latitude, longitude: locationSource.coordinate.longitude)
//            let waypoint = self.routeStopsAdded
//            for wayPointAdded in waypoint {
//                let distance = currentLocation.distance(to: wayPointAdded.coordinate)
//                var feature =  Turf.Feature(geometry: .point(Point(wayPointAdded.coordinate)))
//                if distance <= 50.0 {
//                    //                            removeWayPoints(location: wayPointAdded.coordinate, sName: sNameAddStop ?? "", feature: feature, type: typeAddStop ?? "")
//                    //                            self.viewModel?.rerouteEVandPetrol(source: currentLocation, destName: sNameAddStop ?? "", type: typeAddStop ?? "")
//                }
//            }
//            
//            
//            
//           
//        }
//    }
    
    // Get carpark for Carplay
//    func callApiGetCarparkCarPlay() {
//        if self.carparkViewModel == nil {
//            carparkViewModel = CarparksViewModel(service: CarparkService(), arrivalTime: Date(), destName: "")
//        }
//
//        if let location = lastLocation {
//            let currentLocation = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
//            carparkViewModel.load(at: currentLocation, count: UserSettingsModel.sharedInstance.carparkCount, radius: UserSettingsModel.sharedInstance.carparkDistance, maxRadius: Values.carparkMonitorMaxDistanceInLanding)
//            carparkViewModel.$carparks.sink { [weak self] carparks in
//                guard let self = self, let carparks = carparks else { return }
//
//                self.filteredCarPark.removeAll()
//
//                if !carparks.isEmpty {
//                    self.filteredCarPark.append(contentsOf: carparks)
//                }
//                DispatchQueue.main.async {
//                    self.showCarparkNearbyInCarplay(carparks: self.filteredCarPark, isShow: true)
//                }
//            }.store(in: &disposables)
//        }
//    }

    
    
    func removeLayerTypeFrom(type:String){
        mobileMapViewUpdatable?.navigationMapView?.mapView.removeAmenitiesPetrolAndEVLayer(type: "\(type)")
    }
    
    func removeCarparks() {
        guard let carparkUpdater = self.carParkUpdater else { return }
        carparkUpdater.removeCarParks(keepDestination: true)
        carparkUpdater.removeCallOut()
    }
    
}

extension TurnByTurnNavigationViewModel: NavigationServiceDelegate {
    func navigationService(_ service: NavigationService, didUpdate progress: RouteProgress, with location: CLLocation, rawLocation: CLLocation) {
        //We don't need to handle this
    }
}

extension TurnByTurnNavigationViewModel: MapViewUpdatableDelegate {
    
    func retryLoadCarparksNearBy(_ delay: TimeInterval) {
        DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
            self.onParkingEnabled(true)
        }
    }
    
    func onParkingEnabled(_ value: Bool) {
        dismissTooltips()
        
        //  No need
        if value {
            self.startTimer()
            
            if carParkUpdater == nil {
                carParkUpdater =  CarParkUpdater(destName: "",
                                                 carPlayNavigationMapView: carPlayMapViewUpdatable?.navigationMapView,
                                                 mobileNavigationMapView: mobileMapViewUpdatable?.navigationMapView,
                                                 fromScreen: .navigation)
            }
            carParkUpdater?.delegate = self
            carParkUpdater?.shouldIgnoreDestination = true
            
            if let loc = lastLocation {
                carParkUpdater?.loadCarParks(maxCarParksCount: UserSettingsModel.sharedInstance.carparkCount, radius: UserSettingsModel.sharedInstance.carparkDistance, maxCarParkRadius: Values.carparkMonitorMaxDistanceInLanding, location: loc.coordinate, checkAvailability: true, carPark: getCarpark(), shortTermOnly: true, handler: {[weak self] error in
                    guard let self = self else { return }
                    if appDelegate().carPlayConnected {
                        if let _ = error {
                            if self.retryCounter < self.MAX_RETRY_LOAD_CARPARK {
                                self.retryCounter += 1
                                self.retryLoadCarparksNearBy(2.0)
                            }
                        }
                    }
                })
            }
            
            mobileMapViewUpdatable?.navigationMapView?.navigationCamera.stop()
            carPlayMapViewUpdatable?.navigationMapView?.navigationCamera.stop()
                        
        } else {
            removeCarparks()
            carParkUpdater = nil
            //            self.delegate?.hideNoCarpark(self)
            removeLayerTypeFrom(type: Values.EV_CHARGER)
            removeLayerTypeFrom(type: Values.PETROL)
        }
    }
    
    
    
  
    
    func onShareDriveEnabled(_ value: Bool) {
        if value {
            notifyArrivalAction?()
        }
    }
    
    func onLiveLocationEnabled(_ value: Bool) {
        guard isLiveLocationSharingEnabled != value else { return } // only change onece
        
        if value {
            
            self.updateLiveLocationStatus(status: .inprogress)
            
        } else {
            if let location = self.navigationService.router.location {
                if !self.liveLocTripId.isEmpty {
                    
                    
                    let estimatedTime = estimatedArrivalTime(currentProgress?.durationRemaining ?? 0)
                    
                    DataCenter.shared.startUpdatingUserTripLiveLocation(tripId: self.liveLocTripId, latitude: location.coordinate.latitude.toString(), longitude: location.coordinate.longitude.toString(), status: LiveLocationStatusType.pause.rawValue,arrivalTime: "\(estimatedTime.time)\(estimatedTime.format)",coordinates: "")
                    self.updateLiveLocationStatus(status: .pause)
                }
            }
        }
        isLiveLocationSharingEnabled = value
        
        mobileMapViewUpdatable?.updateLiveLocationEnabled(value)
        carPlayMapViewUpdatable?.updateLiveLocationEnabled(value)
    }
    
    func hideLiveLocationButton() {
        mobileMapViewUpdatable?.hideLiveLocationButton()
    }
    
    func hideShareDriveButton(_ isEnabled:Bool){
        mobileMapViewUpdatable?.shareDriveButtonEnabled(isEnabled)
    }
    
    func onMuteEnabled(_ value: Bool) {
        isMute = value
        Prefs.shared.setValue(!value, forkey: .enableAudio)
        mobileMapViewUpdatable?.updateMuteStatus(isEnabled: value)
        carPlayMapViewUpdatable?.updateMuteStatus(isEnabled: value)
    }
    
    func onSearchAction() {
        //  nothing to be done
    }
    
    func onEndAction() {
        // Do nothing
    }
    
    func onEvChargerAction(_ value: Bool) {
        if value {
            self.startTimer()
            fetchEVorPetrolAmenities(amenity: Values.EV_CHARGER)
            removeLayerTypeFrom(type: Values.PETROL)
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.NavigationMode.UserToggle.ev_button_on, screenName: ParameterName.NavigationMode.screen_view)
        } else {
            removeLayerTypeFrom(type: Values.EV_CHARGER)
//            if !lessThan1kmFromDestination {
//                mobileMapViewUpdatable?.navigationMapView?.navigationCamera.follow()
//            }
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.NavigationMode.UserToggle.ev_button_off, screenName: ParameterName.NavigationMode.screen_view)
        }
        
        //  Handle dismiss tooltip
        dismissTooltips()
    }
    
    func onPetrolAction(_ value: Bool) {
        if value {
            self.startTimer()
            fetchEVorPetrolAmenities(amenity: Values.PETROL)
            removeLayerTypeFrom(type: Values.EV_CHARGER)
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.NavigationMode.UserToggle.petrol_button_on, screenName: ParameterName.NavigationMode.screen_view)
        } else {
            removeLayerTypeFrom(type: Values.PETROL)
//            if !lessThan1kmFromDestination {
//                mobileMapViewUpdatable?.navigationMapView?.navigationCamera.follow()
//            }
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.NavigationMode.UserToggle.petrol_button_off, screenName: ParameterName.NavigationMode.screen_view)
        }
        
        //  Handle dismiss tooltip
        dismissTooltips()
    }
    
    func onParkingAction() {
        //Not required
    }
}

// MARK: - ERPUpdaterListener
extension TurnByTurnNavigationViewModel: ERPUpdaterListener {
    func shouldAddERPItems(noCostfeatures:Turf.FeatureCollection, costfeatures:Turf.FeatureCollection) {
        SwiftyBeaver.debug("\(costfeatures.features.count) ERPs added:\n \(costfeatures.features) cost erps, \(noCostfeatures.features.count) no-cost erps")

        let route = navigationService.route
        updateActiveERP(routes: [route], costfeatures: DataCenter.shared.getAllERPFeatureCollection())
        // Use the data in datacenter so that can get the correct active EPR
    
        self.currentActiveERPCollection = DataCenter.shared.getAllERPFeatureCollection()
    }
    
    private func updateActiveERP(routes: [Route], costfeatures:Turf.FeatureCollection) {
        if routes.count > 0 {
            let allERPs = getActiveERPs(in: routes, from: costfeatures)
            let features = FeatureCollection(features: allERPs)
            
            if let mapViewUpdatable = mobileMapViewUpdatable {
                updateActiveERPLayer(mapViewUpdatable: mapViewUpdatable, activeERPs: features)
            }
            if let mapViewUpdatable = carPlayMapViewUpdatable {
                updateActiveERPLayer(mapViewUpdatable: mapViewUpdatable, activeERPs: features)
            }
            
            SwiftyBeaver.debug("We add erp to roadmatcher in case have any active erp")
            featureDetector.removeERPObjectsFromRoadMatcher()
            if allERPs.count > 0 {
                featureDetector.addERPObjectsToRoadMatcher()
            }
        }
    }

    private func updateActiveERPLayer(mapViewUpdatable: MapViewUpdatable, activeERPs: FeatureCollection) {
        
        if (mapViewUpdatable.isERPLayerExists()) {
            mapViewUpdatable.removeERPLayer()
        } else {
            SwiftyBeaver.debug("ERP layer is not existing")
        }
        
        if activeERPs.features.count > 0 {
            mapViewUpdatable.addERPLayer(features: activeERPs,isNavigation: true)
        }
        
    }
}

// MARK: - TrafficIncidentUpdaterListener
extension TurnByTurnNavigationViewModel: TrafficIncidentUpdaterListener {
    
    func shouldAddTrafficIncident(features:Turf.FeatureCollection) {
        SwiftyBeaver.debug("\(features.features.count) traffic added in navigation")
        
        if let featureCollection = self.currentTrafficIncidentsFeatures {
            if featureDetector != nil {
                featureDetector.removeTrafficIncidentObjects(featureCollection: featureCollection)
                featureDetector.addTrafficIncidentObjectsToRoadMatch()
            }
        }
        // For 3km road objects not from eHorizon instead using Turf Distance
        for feature in features.features {
            
            if case let .string(id) = feature.properties?["id"] {
                
                if(id.contains(TrafficIncidentCategories.FlashFlood) || id.contains(TrafficIncidentCategories.MajorAccident) || id.contains(TrafficIncidentCategories.RoadClosure) || id.contains(TrafficIncidentCategories.HeavyTraffic)){
                    
                    featureCollection3km.features.append(feature)
                }
            }
        }
        self.currentTrafficIncidentsFeatures = features
        
        if let mapViewUpdatable = mobileMapViewUpdatable {
            updateTrafficIncidentsLayer(mapViewUpdatable: mapViewUpdatable, features: features)
        }
        if let mapViewUpdatable = carPlayMapViewUpdatable {
            updateTrafficIncidentsLayer(mapViewUpdatable: mapViewUpdatable, features: features)
        }
        
    }
    
    private func updateTrafficIncidentsLayer(mapViewUpdatable: MapViewUpdatable,features: Turf.FeatureCollection) {
        mapViewUpdatable.removeTrafficLayer()
        if features.features.count > 0 {
            mapViewUpdatable.addTrafficeLayer(features: features, isBelowPuck: false)
        }
    }

}

// MARK: - FeatureDetectorDelegate
extension TurnByTurnNavigationViewModel: FeatureDetectorDelegate {
    
    func featureDetector(_ detector: FeatureDetector, didEnter erp: DetectedERP) {
        //We don't need to handle this
    }
    
    func featureDetector(_ detector: FeatureDetector, didExit erp: DetectedERP) {
        #if DEMO
        if(DemoSession.shared.isDemoRoute)
        {
            tripLogger.exitErp(ERPCost(erpId: Int(erp.id)!, name: erp.address, cost: erp.id == "36" ? 2.00 : erp.price, exitTime: Int(Date().timeIntervalSince1970), isOBU: "0"))
            return
        }
        #endif
        tripLogger.exitErp(ERPCost(erpId: Int(erp.id)!, name: erp.address, cost: erp.price, exitTime: Int(Date().timeIntervalSince1970), isOBU: "0"))
    }

    func featureDetector(_ detector: FeatureDetector, didUpdate feature: FeatureDetectable?) {
        self.currentFeature = feature
    }
    
    func featureDetector(_ detector: FeatureDetector, didExit forId: String) {
        
        // Making CarPlayPrevious displayed notification variable to nil and notification alert close button value to false
        if let carPlayUpdatable = self.carPlayMapViewUpdatable {
            
            //Checking for nil
            if let feature = carPlayUpdatable.cpFeatureDisplay{
                
                //Checking if both are equal
                if( feature == currentFeature){
                    
                    carPlayUpdatable.cpFeatureDisplay = nil
                    carPlayUpdatable.cpFeatureDisplayClose = false
                }
                
            }
                
        }
        
        if currentFeature?.id == forId {
            self.currentFeature = nil
        }
        
        // remove symbol for school zone and silver zone
        mobileMapViewUpdatable?.removeSymbol(forId)
        carPlayMapViewUpdatable?.removeSymbol(forId)
    }
    
    func featureDetector(_ detector: FeatureDetector, isInTunnel: Bool) {        
        SwiftyBeaver.debug("In tunnel - \(isInTunnel)")
        mobileMapViewUpdatable?.updateTunnel(isInTunnel: isInTunnel)
        carPlayMapViewUpdatable?.updateTunnel(isInTunnel: isInTunnel)
    }
    
}

extension TurnByTurnNavigationViewModel:CarParkUpdaterDelegate {
    func carParkNavigateHere(carPark: Carpark?) {
        guard let carpark = carPark else { return }
        
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Navigation.UserClick.navigate_here_parking, screenName: ParameterName.Navigation.screen_view)
        
        if carpark.destinationCarPark ?? false {
            // do nothing if the selected carpark is destination
            return
        }
        
        //  Handle if selected carpark is destination
        if let theCarpark = getCarpark(), theCarpark.id == carpark.id {
            return
        }
        
        if let location = self.navigationService.router.location {
            
            let carparkCoordinate = CLLocationCoordinate2D(latitude: carpark.lat ?? 0.0, longitude: carpark.long ?? 0.0)
            self.reroute(carpark: carpark, source: location.coordinate, destination: carparkCoordinate, destName: carpark.name ?? "")
            // Don't set the carpark to new carpark after reroute otherwise it will show walking path, doing this in reroute function
//            self.carpark = carpark
        }
    }
    
    func didUpdateCarparks(carParks: [Carpark]?, location: CLLocationCoordinate2D) {
        
        //  For fixing BREEZE2-2008 - Need to remove all old coordinates before adjust zoomlevel to new location
        self.carParkCoordinates.removeAll()
        
        if carParks?.count == 0 {
            DispatchQueue.main.async {
                NotificationCenter.default.post(name: .onNoDataNotification, object: 0)
            }
        } else {
            if let carparks = carParks {
                for carpark in carparks {
                    let coordinate = CLLocationCoordinate2D(latitude: carpark.lat ?? 0.0, longitude: carpark.long ?? 0.0)
                    self.carParkCoordinates.append(coordinate)
                }
            }
            
            if let location = lastLocation {
                let currentLocation = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
                self.mobileMapViewUpdatable?.adjustDynamicZoomLevel(self.carParkCoordinates, currentLocation: currentLocation)
                self.carPlayMapViewUpdatable?.adjustDynamicZoomLevelWhenCarParkDisplay(self.carParkCoordinates, currentLocation: currentLocation)
            }
        }
        
        SwiftyBeaver.debug("didUpdateCarparks count \(carParks?.count ?? 0)")
        //  Handle show carparks nearby on Carplay when speed is rezo
//        if showCarparkWhenSpeedIsZero {
//            SwiftyBeaver.debug("showCarparkWhenSpeedIsZero \(carParks?.count ?? 0)")
//            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) { [weak self] in
//                self?.showCarparkNearbyInCarplay(carparks: carParks, isShow: true)
//            }
//
//        }
    }
    
    
    private func showWalkingPath(carPark: Carpark, source: LocationCoordinate2D, destinationName: String, destinationCoordinate: LocationCoordinate2D) {
        
        // request walking route
        requestWalkingRoute(location1: source, location2: destinationCoordinate, name: destinationName) { [weak self] response in
            guard let self = self, let response = response else { return }
            if let route = response.routes?.first {
                self.walkingResponse = response
                self.destinationCarparkName = carPark.name ?? "Carpark destination"
                self.carparkDest = carPark
                
                //  Fetch broadcast message planning in order to show broad cast message
                self.fetchBroadcastMessage(carPark, mode: .planning)
                
                // TODO: to be confirmed?
                // 1. change current destination to carpark icon
//                let image = getcarparkImageName(carpark: carPark, selected: false)
                                
                // 2. show walking path
                // 3. add a desintation icon to original destination
                self.mobileMapViewUpdatable?.addWalkingPath(route: route, finalDestination: destinationCoordinate)
                self.carPlayMapViewUpdatable?.addWalkingPath(route: route, finalDestination: destinationCoordinate)
                
                // 4. add a flag to showing different popup when route complete
                // TODO: Need specially handling when the carpark is withing inner ring, and current destination is carpark
                self.shouldShowCarparkArrival = true
            } else {
                print("")
            }
        }
    }
    
    private func logOpenChargingInfo(_ charging: BreezeObuChargingInfo) {
        var analyticMessage = ""
        let type = charging.getType()
        
        if type == ObuEventType.ERP_CHARGING.rawValue || type == ObuEventType.ERP_FAILURE.rawValue || type == ObuEventType.ERP_SUCCESS.rawValue {
            analyticMessage = "message_erp"
        } else if type == ObuEventType.PARKING_SUCCESS.rawValue || type == ObuEventType.PARKING_FEE.rawValue || type == ObuEventType.PARKING_FAILURE.rawValue {
            analyticMessage = "message_parking"
        }
        
        //  add charge amount
        if charging.chargingAmount > 0 {
            analyticMessage.append(", price_$\(String(format: "%.2f", Double(charging.chargingAmount) / 100.0))")
        }
        
        //  Add parking start/end time
        if type == ObuEventType.PARKING_SUCCESS.rawValue || type == ObuEventType.PARKING_FAILURE.rawValue {
            if let parkingString = charging.getParkingStartAndEnd(), !parkingString.isEmpty {
                analyticMessage.append(", \(parkingString)")
            }
        }
        //  Send :popup_open
        let extraData: NSDictionary = ["message": analyticMessage, "location_name": "location_\(self.roadName ?? "")", "longitude": lastLocation?.coordinate.longitude ?? 0.0, "latitude": lastLocation?.coordinate.latitude ?? 0.0]
        
        if appDelegate().carPlayConnected {
            AnalyticsManager.shared.logOpenPopupEventV2CarPlay(popupName: ParameterName.NavigationMode.UserPopup.navigation_notification, screenName: ParameterName.CarPlayObuState.dhu_navigation, extraData: extraData)
        } else {
            AnalyticsManager.shared.logOpenPopupEventV2(popupName: ParameterName.NavigationMode.UserPopup.navigation_notification, screenName: ParameterName.NavigationMode.screen_view, extraData: extraData)
        }
    }
    
    private func checkLowCardBalance() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 10) {
            // MARK: Check Low Card Balance
            if OBUHelper.shared.isLowBalance() {
                appDelegate().playBeepAudio()
                self.voicInstructionOtherText(text: Constants.lowCardVoiceMeasage)
                let cardBalance = "$\(OBUHelper.shared.getCardBalance())"
                
                if appDelegate().carPlayConnected == true  {
                    self.carPlayMapViewUpdatable?.showsCPNotificationSchoolZoneTraffic(title: "Low card balance", subTitle: cardBalance, icon: UIImage(named: "Miscellaneous-Icon"))
                    
                } else {
                    self.displayCommonOBUNotification(title: "Low card balance", subTitle: "", price: cardBalance, backgroundColor: UIColor(hexString: "#E82370", alpha: 1.0), icon: UIImage(named: "Miscellaneous-Icon")!, iconRate: UIImage(named: "Miscellaneous")!)
                }
                
                if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                    if appDelegate.appInBackground {
                        appDelegate.showLocalNotificationCharges(title: "Low card balance - \(cardBalance)", subTitle: "Please top up your card" )
                    } else {
                        appDelegate.showOnAppErrorNotification(title: "Low card balance - \(cardBalance)", subTitle: "Please top up your card")
                    }
                }
            }
        }
    }
    
    
    private func handleErpAndParkingNotification(_ charging: BreezeObuChargingInfo) {
        
        let type = charging.getType()
        let message = charging.getMessage()
        let price = charging.getChargeAmount()
        
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            if appDelegate.appInBackground {
                if type == ObuEventType.ERP_FAILURE.rawValue || type == ObuEventType.ERP_SUCCESS.rawValue {
                    appDelegate.showLocalNotificationCharges(title: "ERP Fee - \(price)", subTitle: message)
                } else if type == ObuEventType.PARKING_FAILURE.rawValue || type == ObuEventType.PARKING_SUCCESS.rawValue {
                    appDelegate.showLocalNotificationCharges(title: "Parking Fee - \(price)", subTitle: message)
                }
            }
            /*  Dont need to show in app notification in navigation
             else {
                if type == ObuEventType.ERP_FAILURE.rawValue || type == ObuEventType.ERP_SUCCESS.rawValue {
                    appDelegate.showOnAppERPNotification(title: "ERP Fee - \(price)", subTitle: message)
                } else if type == ObuEventType.PARKING_FAILURE.rawValue || type == ObuEventType.PARKING_SUCCESS.rawValue {
                    appDelegate.showOnAppParkingNotification(title: "Parking Fee - \(price)", subTitle: message)
                }
            }
             */
        }
    }
}

extension TurnByTurnNavigationViewModel: SpeechSynthesizingDelegate {
    func speechSynthesizer(_ speechSynthesizer: SpeechSynthesizing, willSpeak instruction: SpokenInstruction) -> SpokenInstruction? {
//        let fixedInstruction = SpokenInstruction(distanceAlongStep: instruction.distanceAlongStep, text: instruction.text.replacingOccurrences(of: "PIE", with: "P.I.E."), ssmlText: instruction.ssmlText.replacingOccurrences(of: "PIE", with: "<say-as interpret-as=\"characters\">PIE</say-as>"))
//        return VoiceInstructor.fixInstruction(instruction)
        guard let  stepDistance = currentProgress?.currentLegProgress.currentStep.distance else { return nil }
        
        if let riva = appDelegate().rivaSpeech, !riva.isPlaying {
            SwiftyBeaver.error("Riva - TurnByTurnNavigationViewModel - Cutoff root cause line 1646")
            AVAudioSession.sharedInstance().tryDuckOtherAudio()
        }
        
        #if DEMO
          if(DemoSession.shared.isDemoRoute && DemoSession.shared.status == .inSecondLeg){
               
              if(instruction.text.contains("mTower")){
                  let text = "Head to Level 3 for more parking Lots."
                  let ssmlText = "<speak><amazon:effect name=\"drc\"><prosody rate=\"1.08\">Head to Level 3 for more parking Lots.</prosody></amazon:effect></speak>"
                  let spokenInstruction = SpokenInstruction(distanceAlongStep: instruction.distanceAlongStep, text: text, ssmlText: ssmlText)
                  return voiceInstructor.willSpeak(spokenInstruction, stepDistance: stepDistance)
              }
              else{
                  return voiceInstructor.willSpeak(instruction, stepDistance: stepDistance)
              }
              
          }
          else{
            return voiceInstructor.willSpeak(instruction, stepDistance: stepDistance)
           }
        #else
          return voiceInstructor.willSpeak(instruction, stepDistance: stepDistance)
        #endif
        
    }
    
    func speechSynthesizer(_ speechSynthesizer: SpeechSynthesizing, didSpeak instruction: SpokenInstruction, with error: SpeechError?) {
        
        SwiftyBeaver.debug("Instruction didSpeak")
        if !appDelegate().checkRivaHasAudioInQueue() {
            AVAudioSession.sharedInstance().tryUnduckOtherAudio()
        }
    }
    
    func speechSynthesizer(_ speechSynthesizer: SpeechSynthesizing, encounteredError error: SpeechError) {
        SwiftyBeaver.debug("Instruction encounteredError")
        if !appDelegate().checkRivaHasAudioInQueue() {
            AVAudioSession.sharedInstance().tryUnduckOtherAudio()
        }
    }
    
}

extension TurnByTurnNavigationViewModel: BreezeObuConnectionDelegate {
    
    func onOBUDisconnected(_ device: OBUSDK.OBU, error: NSError?) {
        //  Handle OBU disconnected
        NotificationCenter.default.post(name: .obuConnectionUpdates, object: false)
        //  If error is nil mean app call disconnect function
        if !OBUHelper.shared.getManuallyConnectObu() {
            self.tripLogger.updateOBUStatus(.notConnected)
            NotificationCenter.default.post(name: .obuDisconnected, object: nil)
        }
    }
    
    func onOBUConnectionFailure(_ error: NSError) {
        //  Handle OBU connection failed
        if !OBUHelper.shared.getManuallyConnectObu() {
            self.tripLogger.updateOBUStatus(.notConnected)
            NotificationCenter.default.post(name: .obuDisconnected, object: nil)
        }
    }
    
    func onBluetoothStateUpdated(_ state: OBUSDK.BluetoothState) {
        //  Handle bluetooth change status
    }
    
    func onOBUConnected(_ device: OBUSDK.OBU) {
        //  Connect to OBU success
        self.tripLogger.updateOBUStatus(.connected)
        NotificationCenter.default.post(name: .obuConnectionUpdates, object: false)
    }
}

extension TurnByTurnNavigationViewModel: BreezeObuDataDelegate {
    
    func onVelocityInformation(_ velocity: Double) {
        //  Update speed OBU return velocity in m/s
        if OBUHelper.shared.getObuMode() == .device {
            currentSpeed = velocity * 3.6   //  convert m/s to km/h
            
//            updateOBUSpeed(currentSpeed, limitSpeed: limitSpeed)
            calculateIdleTime()
            handleShowParkingNearbyIfNeeded()
        }
    }
    
    func onChargingInformation(_ chargingInfo: [BreezeObuChargingInfo]) {
       
        if let first = OBUHelper.shared.getPriorityChargingInfo(chargingInfo) {
            
            mobileMapViewUpdatable?.removeParkingAlert()
            
            let type = first.getType()
            
            if let eventType = ObuEventType(rawValue: type) {
                checkCloseCarparkList(eventType)
            }
            
            if type == ObuEventType.ERP_CHARGING.rawValue {
                voiceInstructor.speak(text: "ERP ahead")
            } else if (type == ObuEventType.ERP_SUCCESS.rawValue || type == ObuEventType.PARKING_SUCCESS.rawValue || type == ObuEventType.PARKING_FEE.rawValue ) {
                //  Play beep sound when receive ERP charging success event
                appDelegate().playBeepAudio()
            }
            var messageCard = first.getMessage()
            if type == ObuEventType.ERP_FAILURE.rawValue || type == ObuEventType.PARKING_FAILURE.rawValue {
                appDelegate().playBeepAudio()
                messageCard = first.getCardErrorMessage()
            }
            let price = first.getChargeAmount()
            
            if type == ObuEventType.ERP_SUCCESS.rawValue || type == ObuEventType.ERP_FAILURE.rawValue {
                if appDelegate().carPlayConnected  {
                    if type == ObuEventType.ERP_SUCCESS.rawValue {
                        carPlayMapViewUpdatable?.showsCPNotificationObuERP(first)
                    } else {
                        carPlayMapViewUpdatable?.showsCPNotificationSchoolZoneTraffic(title: first.getCardErrorMessage(), subTitle: first.getMessage(), icon: UIImage(named: "erpRedIcon")!)
                    }
                } else {
                    self.displayCommonOBUNotification(title: first.getDistance() ?? "", subTitle: messageCard, price: price, backgroundColor: (type == ObuEventType.ERP_SUCCESS.rawValue ? UIColor(hexString: "#005FD0", alpha: 1.0) : UIColor(hexString: "#E82370", alpha: 1.0)), icon: (type == ObuEventType.ERP_CHARGING.rawValue ? UIImage(named: "erpIcon")! : UIImage(named: "erpRedIcon")!), iconRate: (type == ObuEventType.ERP_FAILURE.rawValue ? UIImage(named: "failedERPIcon")! : UIImage(named: "greenTick")!))
                    
                }
            }else if type == ObuEventType.PARKING_FEE.rawValue || type == ObuEventType.PARKING_SUCCESS.rawValue || type == ObuEventType.PARKING_FAILURE.rawValue {
                if appDelegate().carPlayConnected {
                    if type == ObuEventType.PARKING_FAILURE.rawValue {
                        carPlayMapViewUpdatable?.showsCPNotificationSchoolZoneTraffic(title: messageCard, subTitle: first.getChargeAmount(), icon: UIImage(named: "ParkingRedIcon")!)
                    } else {
                        carPlayMapViewUpdatable?.showsCPNotificationSchoolZoneTraffic(title: first.getMessage(), subTitle: (type == ObuEventType.PARKING_SUCCESS.rawValue ? first.getChargeAmount() : first.getParkingRate()) , icon: UIImage(named: "cp-parkingFee")!)
                    }
                } else {
                    var iconRate: UIImage?
                    if type == ObuEventType.PARKING_SUCCESS.rawValue {
                        iconRate = UIImage(named: "greenTick")
                    } else if type == ObuEventType.PARKING_FAILURE.rawValue {
                        iconRate = UIImage(named: "failedERPIcon")
                    }
                    
                    self.displayCommonOBUNotification(title: messageCard , subTitle: "", price: price, backgroundColor: ((type == ObuEventType.PARKING_FEE.rawValue || type == ObuEventType.PARKING_SUCCESS.rawValue) ? UIColor(hexString: "#782EB1", alpha: 1.0) : UIColor(hexString: "#E82370", alpha: 1.0)), icon: ((type == ObuEventType.PARKING_FEE.rawValue || type == ObuEventType.PARKING_SUCCESS.rawValue) ? UIImage(named: "parkingPurpleIcon")! : UIImage(named: "ParkingRedIcon")!), iconRate: iconRate)
                }
            }
            
            logOpenChargingInfo(first)
            
            if (type == ObuEventType.ERP_SUCCESS.rawValue || type == ObuEventType.PARKING_SUCCESS.rawValue || type == ObuEventType.ERP_FAILURE.rawValue || type == ObuEventType.PARKING_FAILURE.rawValue) {
                let tripId = tripLogger?.getTripGUID() ?? ""
                if !tripId.isEmpty {
                    handleErpAndParkingNotification(first)
                }
            }
            
            if (type == ObuEventType.ERP_SUCCESS.rawValue || type == ObuEventType.PARKING_SUCCESS.rawValue) {
                let tripId = tripLogger?.getTripGUID() ?? ""
                if !tripId.isEmpty {
                    checkLowCardBalance()
                }
            }
            
            
        }
        
        let tripId = tripLogger?.getTripGUID() ?? ""
        if !tripId.isEmpty {
            ObuDataService().sendTripObuErp(chargingInfo, tripGUID: tripId, roadName: self.roadName, lat: lastLocation?.coordinate.latitude ?? 0, long: lastLocation?.coordinate.longitude ?? 0) { result in
                switch result {
                case .success(_):
                    print("Send charging information success")
                case .failure(let error):
                    print("Send charging information failed: \(error.localizedDescription)")
                }
            }
        }
        
        NotificationCenter.default.post(name: .obuConnectionUpdates, object: false)
    }
    
    func onTrafficInformation(_ trafficInfo: BreezeObuTrafficInfo) {
        
        if needQueueRealOBUEvent(trafficInfo) {
            return
        }
                
        var analyticMessage = "message_traffic"
        if let parking = trafficInfo.trafficParking {
            //  Get all carpark names then send event to RN
            analyticMessage = "message_parking_availabilty"
            
            var carparks: [Any] = []
            for item in parking.dataList {
                let carparkDic = ["name": item.location, "availableLots": item.lots, "color": item.color]
                carparks.append(carparkDic)
            }
            
            //  Fake data when receive events
            if OBUHelper.shared.getObuMode() == .mockSDK {
                carparks.removeAll()
                carparks.append(["name": "NCS Hub", "availableLots": 150, "color": "Red"])
                carparks.append(["name": "Resorts World", "availableLots": 192, "color": "Yellow"])
                carparks.append(["name": "HarbourFront Centre", "availableLots": 186, "color": "Yellow"])
                carparks.append(["name": "Keppel Bay", "availableLots": 101, "color": "Red"])
                carparks.append(["name": "Mount Faber Park", "availableLots": 122, "color": "Red"])
            }
            
            self.dataCarparkAvailability = ["carparks": carparks]
            
            if !carparks.isEmpty {
                
                checkCloseCarparkList(.PARKING_AVAILABILITY)
                
                if UserSettingsModel.sharedInstance.obuParkingAvailabilityAudioEnable {
                    appDelegate().playBeepAudio()                    
                    playSpokenText(text: parking.getSpokenText())
                }
                      
                if UserSettingsModel.sharedInstance.obuParkingAvailabilityDisplayEnable {
                    //  Show alert on Carplay and see more
                    if appDelegate().carPlayConnected == true {
                        let extraData: NSDictionary = ["message": analyticMessage, "location_name": "location_\(self.roadName ?? "")", "longitude": lastLocation?.coordinate.longitude ?? 0.0, "latitude": lastLocation?.coordinate.latitude ?? 0.0]
                        AnalyticsManager.shared.logOpenPopupEventV2CarPlay(popupName: ParameterName.NavigationMode.UserPopup.navigation_notification, screenName: ParameterName.CarPlayObuState.dhu_navigation, extraData: extraData)
                        
                        carPlayMapViewUpdatable?.displayOBUNotificationCarPark(parking.dataList, item: parking.getInfoToDisplayOnCarplay())
                    } else {
                        
                        let extraData: NSDictionary = ["message": analyticMessage, "location_name": "location_\(self.roadName ?? "")", "longitude": lastLocation?.coordinate.longitude ?? 0.0, "latitude": lastLocation?.coordinate.latitude ?? 0.0]
                        AnalyticsManager.shared.logOpenPopupEventV2(popupName: ParameterName.NavigationMode.UserPopup.navigation_notification, screenName: ParameterName.NavigationMode.screen_view, extraData: extraData)
                        
                        if let dataCarParkUpdate = trafficInfo.trafficParking {
                            let displayItem = dataCarParkUpdate.getInfoToDisplayOnMobile()
                            self.disPlayCarParkUpdateView(title: displayItem.title ?? "", subTitle: displayItem.subTitle ?? "", backgroundColor: UIColor(hexString: displayItem.getBackgroundColor(), alpha: 1.0), icon: UIImage(named: displayItem.getIconName())!)
                        }
                    
                    }
                }                
            }
        } else if let travel = trafficInfo.travelTime {
            //  Get travel time info then send event to RN
            analyticMessage = "message_traveltime"
            
            var params: [[String: Any]] = []
            for item in travel.dataList {
                let dic: [String: Any] = ["name": item.location, "estTime": item.min, "color": item.color]
                params.append(dic)
            }
            
            //  Fake data when receive events
            if OBUHelper.shared.getObuMode() == .mockSDK {
                params.removeAll()
                params.append(["name": "Woodlands Ave 2", "estTime": "29 min", "color": "red"])
                params.append(["name": "SLE", "estTime": "20 min", "color": "yellow"])
                params.append(["name": "AMK AV1", "estTime": "13 min", "color": "yellow"])
                params.append(["name": "Yishun AV2", "estTime": "12 min", "color": "yellow"])
            }
            
            self.dataTravel = ["data": params]
            
            if !params.isEmpty {
                
                checkCloseCarparkList(.TRAVEL_TIME)
                
                if UserSettingsModel.sharedInstance.obuEstimatedTravelTimeAudioEnable {
                    appDelegate().playBeepAudio()
                    playSpokenText(text: travel.getSpokenText())
                }
                
                if UserSettingsModel.sharedInstance.obuEstimatedTravelTimeDisplayEnable {                    
                    //  Show alert on Carplay and see more
                    if appDelegate().carPlayConnected == true {
                        
                        let extraData: NSDictionary = ["message": analyticMessage, "location_name": "location_\(self.roadName ?? "")", "longitude": lastLocation?.coordinate.longitude ?? 0.0, "latitude": lastLocation?.coordinate.latitude ?? 0.0]
                        AnalyticsManager.shared.logOpenPopupEventV2CarPlay(popupName: ParameterName.NavigationMode.UserPopup.navigation_notification, screenName: ParameterName.CarPlayObuState.dhu_navigation, extraData: extraData)
                        
                        carPlayMapViewUpdatable?.displayOBUNotificationTravelTime(travel.dataList)
                    } else {
                        // , params: dataTravel
                        let extraData: NSDictionary = ["message": analyticMessage, "location_name": "location_\(self.roadName ?? "")", "longitude": lastLocation?.coordinate.longitude ?? 0.0, "latitude": lastLocation?.coordinate.latitude ?? 0.0]
                        AnalyticsManager.shared.logOpenPopupEventV2(popupName: ParameterName.NavigationMode.UserPopup.navigation_notification, screenName: ParameterName.NavigationMode.screen_view, extraData: extraData)
                        
                        self.disPlayCarParkUpdateView(title: "Travel Time", subTitle: "", backgroundColor: UIColor(hexString: "#E82370", alpha: 1.0), icon: UIImage(named: "TravelTimeIcon")!)
                    }
                }

            }
        }else {
            
            var needDisplayAlert = true
            var needVoiceAlert = true
            if let text = trafficInfo.trafficText {
                if text.icon == .some("2002") {
                    needDisplayAlert = UserSettingsModel.sharedInstance.schoolZoneDisplayAlert
                    needVoiceAlert = UserSettingsModel.sharedInstance.schoolZoneVoiceAlert
                } else if text.icon == .some("2003") {
                    needDisplayAlert = UserSettingsModel.sharedInstance.silverZoneDisplayAlert
                    needVoiceAlert = UserSettingsModel.sharedInstance.silverZoneVoiceAlert
                } else if text.icon == .some("2004") {
                    needDisplayAlert = UserSettingsModel.sharedInstance.busLaneDisplayAlert
                    needVoiceAlert = UserSettingsModel.sharedInstance.busLaneVoiceAlert
                }
            }
            
            let traficNotiDic = trafficInfo.getTrafficNotiDic(self.roadName ?? "")
            print("traficNotiDic: \(traficNotiDic)")
            
            if let message = traficNotiDic["message"] as? String {
                analyticMessage = "message_\(message)"
            }
            
            let title = traficNotiDic["message"] as? String
            let distance = traficNotiDic["distance"] as? String
            
            //  Play beep
            if let text = trafficInfo.trafficText {
                
                checkCloseCarparkList(.TRAFFIC)
                
                if needVoiceAlert {
                    if text.icon == .some("2004") {
                        //  Play beep sound for bus lane
                        appDelegate().playBeepAudio()
                    } else {
                        let spokenText = trafficInfo.getSpokenText()
                        if !isMute && !spokenText.isEmpty {
                            voiceInstructor.speak(text: spokenText)
                        }
                    }
                }
                
                if needDisplayAlert {
                    if text.icon == .some("1412") {
                        //  Show map when accident happen
                        if appDelegate().carPlayConnected {
                            carPlayMapViewUpdatable?.showsCPNotificationObuTraffic(title: title ?? "", subTitle: distance ?? "")
                        } else {
                            mobileMapViewUpdatable?.updateTrafficInformationNotification(title: distance, subTitle: title, backgroundColor: UIColor(hexString: "#E82370", alpha: 1.0), icon: UIImage(named: "Accident-Icon")!)
                        }
                    } else if text.icon == .some("2002") {
                        //  Show School zone / Silver zone alert on Carplay
                        if appDelegate().carPlayConnected {
                            carPlayMapViewUpdatable?.showsCPNotificationSchoolZoneTraffic(title: title ?? "", subTitle: distance ?? "", icon: UIImage(named: "carplay-schoolZoneIcon")!)
                        } else {
                            mobileMapViewUpdatable?.updateTrafficInformationNotification(title: distance, subTitle: title, backgroundColor: UIColor(hexString: "#F3712B", alpha: 1.0), icon: UIImage(named: "schoolZoneIcon")!)
                        }
                    } else if text.icon == .some("2003") {
                        if appDelegate().carPlayConnected {
                            carPlayMapViewUpdatable?.showsCPNotificationSchoolZoneTraffic(title: title ?? "", subTitle: distance ?? "", icon: UIImage(named: "silverZoneIcon")!)
                        } else {
                            mobileMapViewUpdatable?.updateTrafficInformationNotification(title: distance, subTitle: title, backgroundColor: UIColor(hexString: "#F3712B", alpha: 1.0), icon: UIImage(named: "silverZoneIcon")!)
                        }
                    } else if text.icon == .some("9011") {
                        if appDelegate().carPlayConnected {
                            carPlayMapViewUpdatable?.showsCPNotificationSchoolZoneTraffic(title: title ?? "", subTitle: distance ?? "", icon: UIImage(named: "cp-speedCamera")!)
                        } else {
                            mobileMapViewUpdatable?.updateTrafficInformationNotification(title: distance, subTitle: title, backgroundColor: UIColor(hexString: "#F3712B", alpha: 1.0), icon: UIImage(named: "cp-speedCamera")!)
                        }
                    } else if text.icon == .some("1410") {
                        if appDelegate().carPlayConnected {
                            carPlayMapViewUpdatable?.showsCPNotificationSchoolZoneTraffic(title: title ?? "", subTitle: distance ?? "", icon: UIImage(named: "roadClosureIcon")!)
                        } else {
                            mobileMapViewUpdatable?.updateTrafficInformationNotification(title: distance, subTitle: title, backgroundColor: UIColor(hexString: "#F3712B", alpha: 1.0), icon: UIImage(named: "roadClosureIcon")!, needAdjustFont: true)
                        }
                    } else if text.icon == .some("2004") {
                        
                        let detailTitle = traficNotiDic["detailTitle"] as? String
                        
                        if appDelegate().carPlayConnected {
                            carPlayMapViewUpdatable?.showsCPNotificationSchoolZoneTraffic(title: title ?? "", subTitle: detailTitle ?? "", icon: UIImage(named: "bus-lane-carPlay")!,timeDismiss: Int(10.0))
                        } else {
                            mobileMapViewUpdatable?.updateTrafficInformationNotification(title: title, subTitle: detailTitle, backgroundColor: UIColor(hexString: "#F26415", alpha: 1.0), icon: UIImage(named: "busLaneIcon")!,dismissInSeconds: 10, needAdjustFont: true)
                        }
                        
                    } else {
                        if appDelegate().carPlayConnected {
                            carPlayMapViewUpdatable?.showsCPNotificationSchoolZoneTraffic(title: title ?? "", subTitle: distance ?? "", icon: UIImage(named: "carplay-Miscellaneous-Icon")!)
                        } else {
                            mobileMapViewUpdatable?.updateTrafficInformationNotification(title: distance, subTitle: title, backgroundColor: UIColor(hexString: "#F3712B", alpha: 1.0), icon: UIImage(named: "carplay-Miscellaneous-Icon")!)
                        }
                    }
                }
            }
            
            //  Send :popup_open
            if needDisplayAlert {
                let extraData: NSDictionary = ["message": analyticMessage, "location_name": "location_\(self.roadName ?? "")", "longitude": lastLocation?.coordinate.longitude ?? 0.0, "latitude": lastLocation?.coordinate.latitude ?? 0.0]
                if appDelegate().carPlayConnected {
                    AnalyticsManager.shared.logOpenPopupEventV2CarPlay(popupName: ParameterName.NavigationMode.UserPopup.navigation_notification, screenName: ParameterName.CarPlayObuState.dhu_navigation, extraData: extraData)
                } else {
                    AnalyticsManager.shared.logOpenPopupEventV2(popupName: ParameterName.NavigationMode.UserPopup.navigation_notification, screenName: ParameterName.NavigationMode.screen_view, extraData: extraData)
                }
            }
        }
    }
    
    func onErpChargingAndTrafficInfo(trafficInfo: BreezeObuTrafficInfo?, chargingInfo: [BreezeObuChargingInfo]?) {
        //  Nothing to be done here, will handle in separated callback
    }
    
    func onPaymentHistories(_ histories: [BreezePaymentHistory]) {
        let tripId = tripLogger?.getTripGUID() ?? ""
        if !tripId.isEmpty {
            ObuDataService().sendObuTransactions(histories, tripGUID: tripId) { result in
                switch result {
                case .success(_):
                    print("Send transactions success")
                case .failure(let error):
                    print("Send transactions failed: \(error.localizedDescription)")
                }
            }
        }
    }
    
    func onTravelSummary(_ travelSummary: BreezeTravelSummary) {
        let tripId = tripLogger?.getTripGUID() ?? ""
        if !tripId.isEmpty {
            
            if let time = travelSummary.totalTravelTime, let distance = travelSummary.totalTravelDistance, (totalTravelTime != Int(time) || totalTravelDistance != Int(distance)) {
                
                totalTravelTime = Int(time)
                totalTravelDistance = Int(distance)
                
                ObuDataService().sendObuTripSummary(travelSummary, tripGUID: tripId) { result in
                    switch result {
                    case .success(_):
                        print("Send travel summary success")
                    case .failure(let error):
                        print("Send travel summary failed: \(error.localizedDescription)")
                    }
                }
            }
        }
    }
    
    func onTotalTripCharged(_ totalCharged: TravelCharge) {
        //  Already covered in onTravelSummary
    }
    
    func onError(_ errorCode: NSError) {
        //  Nothing to be done here
    }
    
    func onCardInformation(_ status: BreezeCardStatus?, paymentMode: BreezePaymentMode?, chargingPaymentMode: BreezeChargingPaymentMode?, balance: Int) {
        //  Nothing to be done here -> already handle card error onChargingInfo
//        let chargeAmount = String(format: "%.2f", Double(balance) / 100.0)
//        if status == .error {
//            self.displayCommonOBUNotification(title: "Card Error", subTitle: "", price: chargeAmount, backgroundColor: UIColor(hexString: "#E82370", alpha: 1.0), icon: UIImage(named: "erpRedIcon")!, iconRate: UIImage(named: "failedERPIcon")!)
//        } else if status == .insufficent {
//            self.displayCommonOBUNotification(title: "Insufficient Balance", subTitle: "", price: chargeAmount, backgroundColor: UIColor(hexString: "#E82370", alpha: 1.0), icon: UIImage(named: "erpRedIcon")!, iconRate: UIImage(named: "failedERPIcon")!)
//        } else if status == .expired {
//            self.displayCommonOBUNotification(title: "Card Expired", subTitle: "", price: chargeAmount, backgroundColor: UIColor(hexString: "#E82370", alpha: 1.0), icon: UIImage(named: "erpRedIcon")!, iconRate: UIImage(named: "failedERPIcon")!)
//        } else {
//            // MARK: Check Low Card Balance
////            let chargeAmount = String(format: "%.2f", Double(balance) / 100.0)
////            checkLowCardBalance(chargeAmount)
//        }
        NotificationCenter.default.post(name: .obuConnectionUpdates, object: false)
    }
    
}

extension TurnByTurnNavigationViewModel: BreezeServerMockProtocol {
    
    private func playSpokenText(text: String?) {
        if let spokenText = text, !spokenText.isEmpty {
            self.voiceInstructor.speak(text: spokenText)
        }
    }
    
    private func checkCloseCarparkList(_ type: ObuEventType) {
        print("checkCloseCarparkList: \(type)")
        switch type {
        case .CARD_VALID, .CARD_ERROR, .CARD_EXPIRED:
            //  Do nothing
            break
        default:
            DispatchQueue.main.async {
                NotificationCenter.default.post(name: .onCloseCarparkListScreen, object: nil)
            }
            break
        }
    }
    
    func onReceiveObuEvent(_ event: ObuMockEventModel) {
        if let eventType = ObuEventType(rawValue: event.eventType) {
            
            var analyticMessage = "message_\(eventType.rawValue)"
            
            if needQueueMockEvent(event) {
               return
            }
                        
            checkCloseCarparkList(eventType)
            
            //  These two variables only apply for school zone, silver zone and bus lane event
            var needDisplayAlert = true
            var needVoiceAlert = true
            if eventType == .SCHOOL_ZONE {
                needDisplayAlert = UserSettingsModel.sharedInstance.schoolZoneDisplayAlert
                needVoiceAlert = UserSettingsModel.sharedInstance.schoolZoneVoiceAlert
            } else if eventType == .SILVER_ZONE {
                needDisplayAlert = UserSettingsModel.sharedInstance.silverZoneDisplayAlert
                needVoiceAlert = UserSettingsModel.sharedInstance.silverZoneVoiceAlert
            } else if eventType == .BUS_LANE {
                needDisplayAlert = UserSettingsModel.sharedInstance.busLaneDisplayAlert
                needVoiceAlert = UserSettingsModel.sharedInstance.busLaneVoiceAlert
            }
                        
            switch eventType {
            case .PARKING_AVAILABILITY:
                //  BREEZE2-1800 - Remove parking availability from appsync
//                if UserSettingsModel.sharedInstance.obuParkingAvailabilityAudioEnable {
//                    appDelegate().playBeepAudio()
//                }
//
//                if UserSettingsModel.sharedInstance.obuParkingAvailabilityDisplayEnable {
//
//                    //  Show alert on Carplay and see more
//
//                        if appDelegate().carPlayConnected == true {
//
//                            let extraData: NSDictionary = ["message": analyticMessage, "location_name": "location_\(self.roadName ?? "")", "longitude": lastLocation?.coordinate.longitude ?? 0.0, "latitude": lastLocation?.coordinate.latitude ?? 0.0]
//                            if OBUHelper.shared.getObuMode() == .device {
//                                AnalyticsManager.shared.logOpenPopupEventV2CarPlay(popupName: ParameterName.NavigationMode.UserPopup.navigation_notification, screenName: ParameterName.CarPlayObuState.dhu_navigation, extraData: extraData)
//                            }
//                            self.carPlayMapViewUpdatable?.displayOBUNotificationCarPark(event.getParkingList(), item: event.getInfoToDisplayOnCarplay())
//
//                        } else {
//
//                            //  Send :popup_open
//                            let extraData: NSDictionary = ["message": analyticMessage, "location_name": "location_\(self.roadName ?? "")", "longitude": lastLocation?.coordinate.longitude ?? 0.0, "latitude": lastLocation?.coordinate.latitude ?? 0.0]
//                            if OBUHelper.shared.getObuMode() == .device {
//                                AnalyticsManager.shared.logOpenPopupEventV2(popupName: ParameterName.NavigationMode.UserPopup.navigation_notification, screenName: ParameterName.NavigationMode.screen_view, extraData: extraData)
//                            }
//                            self.dataCarparkAvailability = event.getNotiDic()
//
//                            let trafficParking = BreezeTrafficParking(priority: "", templateId: 0, dataList: event.getParkingList())
//                            let displayItem = trafficParking.getInfoToDisplayOnMobile()
//                            self.disPlayCarParkUpdateView(title: displayItem.title ?? "", subTitle: displayItem.subTitle ?? "", backgroundColor: UIColor(hexString: displayItem.getBackgroundColor(), alpha: 1.0), icon: UIImage(named: displayItem.getIconName())!)
//                        }
//                }
                return
                
            case .TRAVEL_TIME:
                if UserSettingsModel.sharedInstance.obuEstimatedTravelTimeAudioEnable {
                    appDelegate().playBeepAudio()
                }
                
                if UserSettingsModel.sharedInstance.obuEstimatedTravelTimeDisplayEnable {
                    
                    //  Show alert on Carplay and see more
                   
                    if appDelegate().carPlayConnected == true {
                        let extraData: NSDictionary = ["message": analyticMessage, "location_name": "location_\(self.roadName ?? "")", "longitude": lastLocation?.coordinate.longitude ?? 0.0, "latitude": lastLocation?.coordinate.latitude ?? 0.0]
                        if OBUHelper.shared.getObuMode() == .device {
                            AnalyticsManager.shared.logOpenPopupEventV2CarPlay(popupName: ParameterName.NavigationMode.UserPopup.navigation_notification, screenName: ParameterName.CarPlayObuState.dhu_navigation, extraData: extraData)
                        }
                        carPlayMapViewUpdatable?.displayOBUNotificationTravelTime(event.getTravelTimeList())
                        
                    } else {
                        
                        //  Send :popup_open
                        let extraData: NSDictionary = ["message": analyticMessage, "location_name": "location_\(self.roadName ?? "")", "longitude": lastLocation?.coordinate.longitude ?? 0.0, "latitude": lastLocation?.coordinate.latitude ?? 0.0]
                        if OBUHelper.shared.getObuMode() == .device {
                            AnalyticsManager.shared.logOpenPopupEventV2(popupName: ParameterName.NavigationMode.UserPopup.navigation_notification, screenName: ParameterName.NavigationMode.screen_view, extraData: extraData)
                        }
                        self.dataTravel = event.getNotiDic()
                        self.disPlayCarParkUpdateView(title: "Travel Time", subTitle: "", backgroundColor: UIColor(hexString: "#E82370", alpha: 1.0), icon: UIImage(named: "TravelTimeIcon")!)
                    }
                }
                
            case .CARD_ERROR, .CARD_EXPIRED, .CARD_INSUFFICIENT, .CARD_VALID:
                
                if eventType != .CARD_VALID {
                    appDelegate().playBeepAudio()
                    let message = event.message ?? ""
                    let price = event.chargeAmount ?? ""
                    if !message.isEmpty && !price.isEmpty {
                        self.displayCommonOBUNotification(title: message, subTitle: "", price: price, backgroundColor: UIColor(hexString: "#E82370", alpha: 1.0), icon: UIImage(named: "erpRedIcon")!, iconRate: UIImage(named: "failedERPIcon")!)
                    }
                } else {
                    //  Need update card balance
                    if let balance = event.balance {
                        OBUHelper.shared.CARD_BALANCE = Float(balance)
                        print("onReceiveObuEvent BALANCE: \(balance)")
                        NotificationCenter.default.post(name: .obuConnectionUpdates, object: false)
                    }
                }
            case .ERP_CHARGING, .ERP_SUCCESS, .ERP_FAILURE, .PARKING_FEE, .PARKING_SUCCESS, .PARKING_FAILURE:
                
                if !OBUHelper.shared.isObuConnected() {
                    return
                }
                
                mobileMapViewUpdatable?.removeParkingAlert()
                
                if eventType == .ERP_SUCCESS || eventType == .PARKING_SUCCESS || eventType == .PARKING_FEE {
                    appDelegate().playBeepAudio()
                }
                
                //  add charge amount
                if let amount = event.chargeAmount, !amount.isEmpty {
                    analyticMessage.append(", price_\(amount)")
                }
                
                //  TODO show ERP alert on Carplay
                if eventType == .ERP_SUCCESS || eventType == .ERP_FAILURE || eventType == .ERP_CHARGING {
                    if appDelegate().carPlayConnected == true {
                        carPlayMapViewUpdatable?.showsCPNotificationObuERP(eventDic: event.getNotiDic())
                        
                    } else {
                        
                        var iconRate: UIImage?
                        if eventType == .ERP_SUCCESS {
                            iconRate = UIImage(named: "greenTick")
                        } else if eventType == .ERP_FAILURE {
                            iconRate = UIImage(named: "failedERPIcon")
                        }
                        
                        let price = event.chargeAmount ?? "" //= String(format: "$%0.2f", (Float(event.chargeAmount ?? "0") ?? 0.0) / 100)
                        self.displayCommonOBUNotification(title: event.distance ?? "" , subTitle: (eventType == .ERP_CHARGING ? (event.detailMessage ?? "") : (event.message ?? "")) , price: price, backgroundColor: ((eventType == .ERP_SUCCESS || eventType == .ERP_CHARGING) ? UIColor(hexString: "#005FD0", alpha: 1.0) : UIColor(hexString: "#E82370", alpha: 1.0)), icon: ((eventType == .ERP_SUCCESS || eventType == .ERP_CHARGING) ? UIImage(named: "erpIcon")! : UIImage(named: "erpRedIcon")!), iconRate: iconRate)
                        
                        /*  Dont need to show in app notification in navigation
                        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                            if !appDelegate.appInBackground {
                                appDelegate.showOnAppERPNotification(title: "ERP Fee - \(price)", subTitle:  event.message ?? "")
                            }
                        }
                        */
                    }
                } else if eventType == .PARKING_FEE || eventType == .PARKING_SUCCESS || eventType == .PARKING_FAILURE {
                    if appDelegate().carPlayConnected {
                        if eventType == .PARKING_FAILURE {
                            carPlayMapViewUpdatable?.showsCPNotificationSchoolZoneTraffic(title: event.message ?? "", subTitle: event.chargeAmount ?? "", icon: UIImage(named: "ParkingRedIcon")!)
                        } else {
                            carPlayMapViewUpdatable?.showsCPNotificationSchoolZoneTraffic(title: event.message ?? "", subTitle: event.detailMessage ?? "", icon: UIImage(named: "cp-parkingFee")!)
                        }
                    } else {
                        let price = event.chargeAmount ?? ""//  String(format: "$%0.2f", (Float(event.chargeAmount ?? "0") ?? 0.0) / 100)
                        
                        var iconRate: UIImage?
                        if eventType == .PARKING_SUCCESS {
                            iconRate = UIImage(named: "greenTick")
                        } else if eventType == .PARKING_FAILURE {
                            iconRate = UIImage(named: "failedERPIcon")
                        }
                        self.displayCommonOBUNotification(title: event.message ?? "" , subTitle: "", price: price, backgroundColor: ((eventType == .PARKING_FEE || eventType == .PARKING_SUCCESS) ? UIColor(hexString: "#782EB1", alpha: 1.0) : UIColor(hexString: "#E82370", alpha: 1.0)), icon: ((eventType == .PARKING_FEE || eventType == .PARKING_SUCCESS) ? UIImage(named: "parkingPurpleIcon")! : UIImage(named: "ParkingRedIcon")!), iconRate: iconRate)
                        
                        /*  Dont need to show in app notification in navigation
                        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                            if !appDelegate.appInBackground {
                                appDelegate.showOnAppParkingNotification(title: "Parking Fee - \(price)", subTitle: event.message ?? "")
                            }
                        }
                         */
                    }
                }
                
                if let appDelegate = UIApplication.shared.delegate as? AppDelegate, appDelegate.appInBackground {
                    if let price = event.chargeAmount {
                        if eventType == .ERP_SUCCESS || eventType == .ERP_FAILURE {
                            appDelegate.showLocalNotificationCharges(title: "ERP Fee - \(price)", subTitle: event.message ?? "")
                        } else if eventType == .PARKING_SUCCESS || eventType == .PARKING_FAILURE{
                            appDelegate.showLocalNotificationCharges(title: "Parking Fee - \(price)", subTitle: event.message ?? "")
                        }
                    }
                }
                
                if eventType == .ERP_SUCCESS || eventType == .PARKING_SUCCESS {
                    checkLowCardBalance()
                }
                
                if !event.chargingInfo.isEmpty {
                    var infos: [BreezeObuChargingInfo] = []
                    for item in event.chargingInfo {
                        infos.append(item.getChargingInfo())
                    }
                    
                    let tripId = tripLogger?.getTripGUID() ?? ""
                    if !tripId.isEmpty {
                        ObuDataService().sendTripObuErp(infos, tripGUID: tripId, roadName: self.roadName, lat: lastLocation?.coordinate.latitude ?? 0, long: lastLocation?.coordinate.longitude ?? 0) { result in
                            switch result {
                            case .success(_):
                                print("Send charging information success")
                            case .failure(let error):
                                print("Send charging information failed: \(error.localizedDescription)")
                            }
                        }
                    }
                }
//            case .TRAFFIC:
                //  BREEZE2-1800 - Removed
//                if appDelegate().carPlayConnected == true {
//                    carPlayMapViewUpdatable?.showsCPNotificationObuTraffic(title: event.message ?? "" , subTitle: event.distance ?? "")
//
//                } else {
//                    self.displayTrafficInformation(title: event.distance ?? "", subTitle: event.message ?? "", backgroundColor: UIColor(hexString: "#E82370", alpha: 1.0), icon: UIImage(named: "Accident-Icon")!)
//                }
//                return
            case .SCHOOL_ZONE:
                //  Show School Zone alert on Carplay or Mobile, Beep not required
                if needDisplayAlert {
                    if appDelegate().carPlayConnected == true {
                        carPlayMapViewUpdatable?.showsCPNotificationSchoolZoneTraffic(title: event.message ?? "", subTitle: event.distance ?? "", icon: UIImage(named: "carplay-schoolZoneIcon")!)
                        
                    } else {
                        self.displayTrafficInformation(title: event.distance ?? "", subTitle: event.message ?? "", backgroundColor: UIColor(hexString: "#F3712B", alpha: 1.0), icon: UIImage(named: "schoolZoneIcon")!)
                    }
                }
            case .SILVER_ZONE:
                //  Show Silver Zone alert on Carplay or Mobile, Beep not required
                if needDisplayAlert {
                    if appDelegate().carPlayConnected {
                        carPlayMapViewUpdatable?.showsCPNotificationSchoolZoneTraffic(title: event.message ?? "", subTitle: event.distance ?? "", icon: UIImage(named: "silverZoneIcon")!)
                    } else {
                        mobileMapViewUpdatable?.updateTrafficInformationNotification(title: event.message ?? "", subTitle: event.distance ?? "", backgroundColor: UIColor(hexString: "#F3712B", alpha: 1.0), icon: UIImage(named: "silverZoneIcon")!)
                    }
                }
            case .ROAD_CLOSURE:
                //  BREEZE2-1800 - Removed
//                if !OBUHelper.shared.isObuConnected() {
//                    appDelegate().playBeepAudio()
//                    if appDelegate().carPlayConnected == true {
//                        carPlayMapViewUpdatable?.showsCPNotificationSchoolZoneTraffic(title: event.message ?? "", subTitle: "\(event.detailTitle ?? "") \(event.detailMessage ?? "")", icon: UIImage(named: "roadClosureIcon")!)
//                    } else {
//                        self.displayTrafficInformation(title: event.message ?? "", subTitle: "\(event.detailTitle ?? "") \(event.detailMessage ?? "")", backgroundColor: UIColor(hexString: "#E82370", alpha: 1.0), icon: UIImage(named: "roadClosureIcon")!, needAdjustFont: true)
//                    }
//                }
                return
            case .BUS_LANE:
                
                if needVoiceAlert {
                    appDelegate().playBeepAudio()
                }
                
                if needDisplayAlert {
                    if appDelegate().carPlayConnected {
                        carPlayMapViewUpdatable?.showsCPNotificationSchoolZoneTraffic(title: event.message ?? "", subTitle: event.detailMessage ?? "In Operation", icon: UIImage(named: "bus-lane-carPlay")!, timeDismiss: Int(10.0))
                    } else {
                        self.displayTrafficInformation(title: event.message ?? "", subTitle: event.detailMessage ?? "", backgroundColor: UIColor(hexString: "#F26415", alpha: 1.0), icon: UIImage(named: "busLaneIcon")!,dismissInSeconds: 10, needAdjustFont: true)
                    }
                }
            case .ROAD_DIVERSION:
                //  Beep not required
                if appDelegate().carPlayConnected == true {
                    carPlayMapViewUpdatable?.showsCPNotificationSchoolZoneTraffic(title: event.message ?? "", subTitle: event.detailMessage ?? "", icon: UIImage(named: "roadDiversionIcon-cp")!)
                    
                } else {
                    self.displayTrafficInformation(title: event.message ?? "", subTitle: event.detailMessage ?? "", backgroundColor: UIColor(hexString: "#E82370", alpha: 1.0), icon: UIImage(named: "roadDiversionIcon")!)
                }
                
            case .GENERAL_MESSAGE:
                //  Beep not required
                if appDelegate().carPlayConnected == true {
                    carPlayMapViewUpdatable?.showsCPNotificationSchoolZoneTraffic(title: event.message ?? "", subTitle: event.detailMessage ?? "", icon: UIImage(named: ""))
                    
                } else {
                    self.displayGeneralMessageAleart(title: event.message ?? "", subTitle: event.detailMessage ?? "", backgroundColor: UIColor(hexString: "#222638", alpha: 1.0))
                }
            case .SPEED_CAMERA:
                //  Beep not required
                if appDelegate().carPlayConnected == true  {
                    carPlayMapViewUpdatable?.showsCPNotificationSchoolZoneTraffic(title: event.message ?? "", subTitle: event.detailMessage ?? "", icon: UIImage(named: "cp-speedCamera")!)
                    
                } else {
                    self.displayTrafficInformation(title: event.distance ?? "", subTitle: event.message ?? "", backgroundColor: UIColor(hexString: "#E82370", alpha: 1.0), icon: UIImage(named: "speedCameraIcon")!)
                }
                
                // MARK: update new event BREEZE2-602
            case .FLASH_FLOOD, .HEAVY_TRAFFIC, .MAJOR_ACCIDENT, .SEASON_PARKING:
                if appDelegate().carPlayConnected == true  {
                    displayAlertEventOnCarplay(event)
                } else {
                    displayAlertEventOnMobile(event)
                }
            case .VEHICLE_BREAKDOWN, .UNATTENDED_VEHICLE, .ROAD_WORK, .ROAD_BLOCK, .MISCELLANEOUS, .TRAFFIC, .OBSTACLE, .TREE_PRUNING, .YELLOW_DENGUE_ZONE, .RED_DENGUE_ZONE:
                //  BREEZE2-1800 - Removed
                return
            case .LOW_CARD:
                appDelegate().playBeepAudio()
                let price = event.chargeAmount ?? ""
                self.voicInstructionOtherText(text: Constants.lowCardVoiceMeasage)
                DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
                    if appDelegate().carPlayConnected == true  {
                        self.carPlayMapViewUpdatable?.showsCPNotificationSchoolZoneTraffic(title: event.message ?? "", subTitle: event.chargeAmount ?? "", icon: UIImage(named: "Miscellaneous-Icon"))
                        
                    } else {
                        self.displayCommonOBUNotification(title: event.message ?? "", subTitle: "", price: event.chargeAmount ?? "", backgroundColor: UIColor(hexString: "#E82370", alpha: 1.0), icon: UIImage(named: "Miscellaneous-Icon")!, iconRate: UIImage(named: "Miscellaneous")!)
                    }
                }
                if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                    if appDelegate.appInBackground {
                            appDelegate.showLocalNotificationCharges(title: "Low card balance - \(price)", subTitle: event.message ?? "Please top up your card")
                    } else {
                        appDelegate.showOnAppErrorNotification(title: "Low card balance - \(price)", subTitle: event.message ?? "Please top up your card")
                    }
                }
            default:
                break
            }
                    
            if eventType == .PARKING_AVAILABILITY {
                if UserSettingsModel.sharedInstance.obuParkingAvailabilityAudioEnable {
                    playSpokenText(text: event.getParkingSpokenText())
                }
            } else if eventType == .TRAVEL_TIME {
                if UserSettingsModel.sharedInstance.obuEstimatedTravelTimeAudioEnable {
                    playSpokenText(text: event.getTravelTimeSpokenText())
                }
            } else {
                
                //  Send :popup_open
                if needDisplayAlert {
                    let extraData: NSDictionary = ["message": analyticMessage, "location_name": "location_\(self.roadName ?? "")", "longitude": lastLocation?.coordinate.longitude ?? 0.0, "latitude": lastLocation?.coordinate.latitude ?? 0.0]
                    if appDelegate().carPlayConnected == false {
                        if OBUHelper.shared.getObuMode() == .device {
                            AnalyticsManager.shared.logOpenPopupEventV2(popupName: ParameterName.NavigationMode.UserPopup.navigation_notification, screenName: ParameterName.NavigationMode.screen_view, extraData: extraData)
                        }
                    } else {
                        if OBUHelper.shared.getObuMode() == .device {
                            AnalyticsManager.shared.logOpenPopupEventV2CarPlay(popupName: ParameterName.NavigationMode.UserPopup.navigation_notification, screenName: ParameterName.CarPlayObuState.dhu_navigation, extraData: extraData)
                        }
                    }
                }
                
                if needVoiceAlert {
                    playSpokenText(text: event.spokenText)
                }
            }
            
        }
        
        //  Send trip log travel summary and transaction history
        let tripId = tripLogger?.getTripGUID() ?? ""
        if !tripId.isEmpty {
            if let tripSummary = event.tripSummary {
                let summary = tripSummary.getTravelSummary()
                ObuDataService().sendObuTripSummary(summary, tripGUID: tripId) { result in
                    switch result {
                    case .success(_):
                        print("Send travel summary success")
                    case .failure(let error):
                        print("Send travel summary failed: \(error.localizedDescription)")
                    }
                }
            }
            
            if !event.payments.isEmpty {
                var histories: [BreezePaymentHistory] = []
                for history in event.payments {
                    let payment = history.getPaymentHistory()
                    histories.append(payment)
                }
                ObuDataService().sendObuTransactions(histories, tripGUID: tripId) { result in
                    switch result {
                    case .success(_):
                        print("Send transactions success")
                    case .failure(let error):
                        print("Send transactions failed: \(error.localizedDescription)")
                    }
                }
            }
        }
    }
    
    func displayAlertEventOnCarplay(_ event: ObuMockEventModel) {
        carPlayMapViewUpdatable?.showsCPNotificationSchoolZoneTraffic(title: event.message ?? "", subTitle: event.distance ?? "", icon: UIImage(named: event.getIconEventOBU(true)))
    }
    
    func displayAlertEventOnMobile(_ event: ObuMockEventModel) {
        self.displayTrafficInformation(title: event.distance ?? "", subTitle: event.message ?? "", backgroundColor: UIColor(hexString: "#E82370", alpha: 1.0), icon: UIImage(named: event.getIconEventOBU(false)))
    }
}


extension TurnByTurnNavigationViewModel {
    
    func startTimer() {
        
        self.stopTimer()
        self.amenityTimer = Timer.scheduledTimer(withTimeInterval: 10, repeats: false, block: { [weak self] _ in
            
            guard let self = self else { return }
            
            if let _ = self.mobileMapViewUpdatable {
                //  If showing carpark nearby when speed is zero then do nothing
                if self.showCarparkWhenSpeedIsZero && self.mobileMapViewUpdatable?.isParkingNearByEnable() == true {
                    //  Keep showing carpark
                } else {
                    //  Toggle off amenity
                    self.mobileMapViewUpdatable?.toggleOffAmenityOnMap()
                    
                    if self.userIsMoving {
                        self.mobileMapViewUpdatable?.navigationMapView?.navigationCamera.follow()
                        self.carPlayMapViewUpdatable?.navigationMapView?.navigationCamera.follow()
                    } else {
                        if self.showCarparkWhenSpeedIsZero {
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: { [weak self] in
                                self?.mobileMapViewUpdatable?.showCarparkNearbyWhileStopDriving()
                            })
                        } else {
                            self.mobileMapViewUpdatable?.navigationMapView?.navigationCamera.follow()
                            self.carPlayMapViewUpdatable?.navigationMapView?.navigationCamera.follow()
                        }
                    }
                }
            } else {
                if self.showCarparkWhenSpeedIsZero {
                    //  Keep showing carpark
                } else {
                    if self.userIsMoving {
                        self.carPlayMapViewUpdatable?.navigationMapView?.navigationCamera.follow()
                    }
                }
            }
            
//            if (self?.lessThan1kmFromDestination == true) {
//                self?.startTimerUpdateZoomLevel()
//            }
        })
    }
    
    func stopTimer() {
        self.amenityTimer?.invalidate()
        self.amenityTimer = nil
    }
    
}

//  Queue event and show after parking availability
extension TurnByTurnNavigationViewModel {
    
    func onParkingAlertDismissed() {
        if let first = queueEvents.first {
            if let event = first as? ObuMockEventModel {
                self.onReceiveObuEvent(event)
            } else if let event = first as? BreezeObuTrafficInfo {
                self.onTrafficInformation(event)
            } else if let event = first as? FeatureDetectable {
                self.currentFeature = event
            }
        }
        queueEvents.removeAll()
    }
    
    func needQueueMockEvent(_ event: ObuMockEventModel) -> Bool {
        var retValue = false
        if let eventType = ObuEventType(rawValue: event.eventType) {
            if let parkingView = self.mobileMapViewUpdatable?.parkingAlertView, !parkingView.isDismissed() {
                switch eventType {
                case .ERP_CHARGING, .ERP_SUCCESS, .ERP_FAILURE, .PARKING_FEE, .PARKING_SUCCESS, .PARKING_FAILURE:
                    break
                default:
                    queueEvents.append(event)
                    retValue = true
                }
            }
        }
        return retValue
    }
    
    func needQueueRealOBUEvent(_ event: BreezeObuTrafficInfo) -> Bool {
        var retValue = false
        if let parkingView = self.mobileMapViewUpdatable?.parkingAlertView, !parkingView.isDismissed() {
            queueEvents.append(event)
            retValue = true
        }
        return retValue
    }
    
    func needQueueEhorizontalEvent(_ event: FeatureDetectable) -> Bool {
        var retValue = false
        if let parkingView = self.mobileMapViewUpdatable?.parkingAlertView, !parkingView.isDismissed() {
            if let _ = event as? DetectedERP {
                //  Do nothing
            } else {
                queueEvents.removeAll()
                queueEvents.append(event)
                retValue = true
            }
        }
        return retValue
    }
}


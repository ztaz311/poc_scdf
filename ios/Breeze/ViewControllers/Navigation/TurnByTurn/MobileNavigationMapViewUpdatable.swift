//
//  MobileNavigationMapViewUpdatable.swift
//  Breeze
//
//  Created by Zhou Hao on 13/10/21.
//

import Foundation
@_spi(Restricted) import MapboxMaps
import MapboxNavigation
import MapboxDirections
import MapboxCoreNavigation
import SwiftyBeaver
import Turf

final class MobileNavigationMapViewUpdatable: MapViewUpdatable {
    // MARK: - Constatns
    let yPadding: CGFloat = 280
    let iconSize: CGFloat = 70
    let xPadding: CGFloat = 24
    let xTrailing: CGFloat = 12
    let yNotificationGap: CGFloat = 16
    let yToast1: CGFloat = 140 // for toast other than tunnel
    let yToast2: CGFloat = 200 // for tunnel
    private let trailingX: CGFloat = 24
    private let iconSize2: CGFloat = 48
    private let showCarparkListHeight: CGFloat = 78
    
    // MARK: - Propertis
    weak var navigationMapView: NavigationMapView?
    weak var delegate: MapViewUpdatableDelegate?
    private weak var topBanner: TopBannerViewController!
    
    // MARK: - Private properties
    private var notificationView: BreezeNotificationView!
    private var parkingStatusView: ParkingAvailabilityView!
    var parkingAlertView: ParkingAvailabilityAlertView!
    private var obuCommonNotificationView: ObuCommonNotificationView!
    private var broadcastMessageView: BreezeBroadcastMessageView!
    private var speedLimitView: CustomSpeedLimitView!
    private var muteButton: ToggleButton!
    private var liveLocationButton: LiveLocationSharingButton!
    private var shareDriveButton: UIButton!
    private var isShowingETAToast = false
    private var showCarParkUpdateView: ParkingAndTravelTimeView!
    private var showTrafficInformationView: TrafficNotificationView!
    private var showGeneralMessageView: GeneralMessageNotificationView!
    var parkingButton: ToggleButton!
    var evchargerButton: ToggleButton!
    var petrolButton: ToggleButton!
    
    private var petrolAndEvView: UIView!
    
//    var isSelectedAmenitiesEv = false
//    var isSelectedAmenitiesPetrol = false
    
    required init(navigationMapView: NavigationMapView) {
        //        self.navigationMapView = navigationMapView
        //        self.navigationMapView.mapView.ornaments.options.attributionButton.visibility = .hidden
        fatalError("Don't init MobileNavigationMapViewUpdatable this way")
    }
    
    init(navigationMapView: NavigationMapView, topBanner: TopBannerViewController) {
        self.navigationMapView = navigationMapView
        self.navigationMapView?.mapView.ornaments.options.attributionButton.visibility = .hidden
        self.topBanner = topBanner
        self.setup()
    }
    
    deinit {
        SwiftyBeaver.debug("MobileNavigationMapViewUpdatable deinit")
        
        navigationMapView = nil
    }
    
    func hidePetrolAndEV() {
        petrolAndEvView.isHidden = true
    }
    
    func showParkingAlert(carpark: Carpark?, attribute: AttributesCarparkModel, timeDisplay: Int) {
        if let parkingStatus = attribute.data?.getParkingStatus(), parkingStatus != .notAvailable {
            
            //  Validate to show alert or not
            if carpark != nil {
                if let isValid = attribute.data?.isValidParkingLots(), isValid {
                    //  Should show alert
                } else if let hasAvailabilityCS = attribute.data?.hasAvailabilityCS, hasAvailabilityCS {
                    //  Should show alert
                } else {
                    return
                }
            }
            
            let offset = Values.topPanelHeight + UIApplication.topSafeAreaHeight + 28
            
            print("parkingStatus: \(parkingStatus), carpark name: \(attribute.data?.name ?? ""), alternate carpark: \(attribute.data?.getAlternateCarpark()?.name ?? "")")
            
            parkingAlertView.setStatus(parkingStatus, title: attribute.data?.name ?? "", carpark: attribute.data?.getAlternateCarpark())
            
            if let navMapView = self.navigationMapView {
                FeatureNotification.showParkingAlert(notificationView: parkingAlertView, inView: navMapView, carpark: carpark, yOffset: offset, timeDisplay: timeDisplay) { [weak self] in
//                    guard let self = self else { return }
                } onDismiss: { [weak self] (type, carpark) in
                    //  Navigate to the carpark
                    if let carpark = carpark {
                        NotificationCenter.default.post(name: .navigateToAlternateCarpark, object: carpark)
                    }
                    
                    NotificationCenter.default.post(name: .parkingAvailabilityAlertDismissed, object: nil)
                    
//                    guard let self = self else { return }
                }
            }
        }
    }
    
    func updateParkingAvailability(carpark: Carpark?) {
        
        //  Guest mode part 1 no need asking user for feedback carpark status
        //        if !AWSAuth.sharedInstance.isGuestMode() {
        if let carpark = carpark, carpark.shouldAskForFeedback(), carpark.currentParkingStatus != .notAvailable {
            //  Remove yes/no question
//            if carpark.needShowYesNoQuestion() {
//                let offset = Values.topPanelHeight + UIApplication.topSafeAreaHeight + 28
//
//                parkingStatusView.setStatus(carpark.currentParkingStatus, title: carpark.getCSTitle(), updateTimeText: carpark.getCSUpdatedTimeDesc(), themeColor: carpark.getThemeColor())
//
//                if let navMapView = self.navigationMapView {
//                    FeatureNotification.showParkingAvailability(notificationView: parkingStatusView, inView: navMapView, carpark: carpark, yOffset: offset) { [weak self] in
//                        guard let self = self else { return }
//                        self.speedLimitView.snp.updateConstraints { make in
//                            make.top.equalToSuperview().offset(offset + self.parkingStatusView.bounds.height + 10)
//                        }
//                    } onDismiss: { [weak self] dismissType in
//
//                        if let _ = dismissType {
//
//                            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Navigation.UserClick.navigate_verify_avail_yes, screenName: ParameterName.Navigation.screen_view)
//
//                            NotificationCenter.default.post(name: .yesParkingAvailableConfirmed, object: nil)
//                        }else {
//
//                            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Navigation.UserClick.navigate_verify_avail_no, screenName: ParameterName.Navigation.screen_view)
//
//                            NotificationCenter.default.post(name: .showSubmitParkingAvailablePopup, object: nil)
//                        }
//
//                        guard let self = self else { return }
//                        self.speedLimitView.snp.updateConstraints { make in
//                            make.top.equalToSuperview().offset(offset + 10)
//                        }
//                    }
//                }
//            } else {
                NotificationCenter.default.post(name: .showSubmitParkingAvailablePopup, object: nil)
//            }
        } else {
            NotificationCenter.default.post(name: .endNavigationWhenReachToCarpark, object: nil)
        }
        //        }else {
        //            NotificationCenter.default.post(name: .endNavigationWhenReachToCarpark, object: nil)
        //        }
    }
    
    
    // MARK: Update Data From OBU on TurnbyTurn Screen
    // , params: [String: Any]
    func updateCarParkandTravelTimeNotification(title: String, subTitle: String, backgroundColor: UIColor, icon: UIImage) {
        if let navMapView = self.navigationMapView {
            let offset = Values.topPanelHeight + UIApplication.topSafeAreaHeight + 28
            FeatureNotification.showCarparkUpdateView(notificationView: showCarParkUpdateView, title: title, subTitle: subTitle, backgroundColor: backgroundColor, icon: icon, inView: navMapView, yOffset: offset) { [weak self] in
                guard let self = self else { return }
                self.speedLimitView.snp.updateConstraints { make in
                    make.top.equalToSuperview().offset(offset + self.showCarParkUpdateView.bounds.height + 10)
                }
            } onDismiss: { [weak self] in
                guard let self = self else { return }
                
                AnalyticsManager.shared.logClosePopupEventV2(popupName: ParameterName.NavigationMode.UserPopup.navigation_notification, screenName: ParameterName.NavigationMode.screen_view)
                
                self.speedLimitView.snp.updateConstraints { make in
                    make.top.equalToSuperview().offset(offset + 10)
                }
            }
            
            showCarParkUpdateView.onReadMore = { [weak self] in
                guard let self = self else { return }
                
                var extraData: NSDictionary = [:]
                
                if title == "Travel Time" {
                    extraData = ["message": "message_traveltime"]
                } else {
                    extraData = ["message": "message_parking_availability"]
                }
                
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.NavigationMode.UserClick.popup_navigation_notification_click_notification, screenName: ParameterName.NavigationMode.screen_view, extraData: extraData)
                
                if title == "Travel Time" {
                    AnalyticsManager.shared.logOpenPopupEventV2(popupName: ParameterName.NavigationMode.UserPopup.travel_time, screenName: ParameterName.NavigationMode.screen_view)
                    NotificationCenter.default.post(name: .showTravelTimeList, object: nil)
                } else {
                    NotificationCenter.default.post(name: .showCarParkList, object: nil)
                }
                                
                self.speedLimitView.snp.updateConstraints { make in
                    make.top.equalToSuperview().offset(offset + 10)
                }
            }
            
        }
        
    }
    
    func updateObuCommonNotification(title: String, subTitle: String, price: String, backgroundColor: UIColor, icon: UIImage, iconRate: UIImage?) {
        if let navMapView = self.navigationMapView {
            let offset = Values.topPanelHeight + UIApplication.topSafeAreaHeight + 28
            
            obuCommonNotificationView.updateUI(title: title, subTitle: subTitle, icon: icon, backgroundColor: backgroundColor, rate: price, iconRate: iconRate)
            
            FeatureNotification.showOBUDataView(notificationView: obuCommonNotificationView, inview: navMapView, yOffset: offset) { [weak self] in
                guard let self = self else { return }
                self.speedLimitView.snp.updateConstraints { make in
                    make.top.equalToSuperview().offset(offset + self.obuCommonNotificationView.bounds.height + 10)
                }
            } onDismiss: { [weak self] in
                guard let self = self else { return }
                
                AnalyticsManager.shared.logClosePopupEventV2(popupName: ParameterName.NavigationMode.UserPopup.navigation_notification, screenName: ParameterName.NavigationMode.screen_view)
                
                self.speedLimitView.snp.updateConstraints { make in
                    make.top.equalToSuperview().offset(offset + 10)
                }
            }
            
        }
    }
    
    func updateTrafficInformationNotification(title: String?, subTitle: String?, backgroundColor: UIColor, icon: UIImage, dismissInSeconds:Int? = 5, needAdjustFont: Bool = false) {
        if let navMapView = self.navigationMapView {
            let offset = Values.topPanelHeight + UIApplication.topSafeAreaHeight + 28
            FeatureNotification.showTrafficStatusView(notification: showTrafficInformationView, title: title, subTitle: subTitle, backgroundColor: backgroundColor, icon: icon, inview: navMapView, yOffset: offset, dismissInSeconds: dismissInSeconds, needAdjustFont: needAdjustFont) { [weak self] in
                guard let self = self else { return }
                self.speedLimitView.snp.updateConstraints { make in
                    make.top.equalToSuperview().offset(offset + self.showTrafficInformationView.bounds.height + 10)
                }
            } onDismiss: { [weak self] in
                guard let self = self else { return }
                
                AnalyticsManager.shared.logClosePopupEventV2(popupName: ParameterName.NavigationMode.UserPopup.navigation_notification, screenName: ParameterName.NavigationMode.screen_view)
                
                self.speedLimitView.snp.updateConstraints { make in
                    make.top.equalToSuperview().offset(offset + 10)
                }
            }
            
        }
    }
    
    
    func updateGeneralMessageNotification(title: String?, subTitle: String?, backgroundColor: UIColor) {
        if let navMapView = self.navigationMapView {
            let offset = Values.topPanelHeight + UIApplication.topSafeAreaHeight + 28
            FeatureNotification.showGeneralMessageView(notification: showGeneralMessageView, title: title, subTitle: subTitle, backgroundColor: backgroundColor, inview: navMapView, yOffset: offset) { [weak self] in
                guard let self = self else {return}
                self.speedLimitView.snp.updateConstraints { make in
                    make.top.equalToSuperview().offset(offset + self.showGeneralMessageView.bounds.height + 10)
                }
            } onDismiss: { [weak self] in
                guard let self = self else {return}
                
                AnalyticsManager.shared.logClosePopupEventV2(popupName: ParameterName.NavigationMode.UserPopup.navigation_notification, screenName: ParameterName.NavigationMode.screen_view)
                
                self.speedLimitView.snp.updateConstraints { make in
                    make.top.equalToSuperview().offset(offset + 10)
                }
            }
        }
    }
    
    func updateNotificationWhenUserSoonArrive(carpark: Carpark?, message: String) {
        
        if let navMapView = self.navigationMapView {
            let offset = Values.topPanelHeight + UIApplication.topSafeAreaHeight + 28
            
            FeatureNotification.showSoonArriveNotification(notificationView: broadcastMessageView, inView: navMapView, carpark: carpark, yOffset: offset, message: message) { [weak self] in
                guard let self = self else { return }
                self.speedLimitView.snp.updateConstraints { make in
                    make.top.equalToSuperview().offset(offset + self.broadcastMessageView.bounds.height + 10)
                }
            } onDismiss: { [weak self] in
                guard let self = self else { return }
                
                AnalyticsManager.shared.logClosePopupEventV2(popupName: ParameterName.NavigationMode.UserPopup.navigation_notification, screenName: ParameterName.NavigationMode.screen_view)
                
                self.speedLimitView.snp.updateConstraints { make in
                    make.top.equalToSuperview().offset(offset + 10)
                }
            }
        }
        
    }
    
    
    
    
    // featureId - Could be current feature id if currentFeature is not nil
    // or previous featureId if currentFeature is nil
    func updateNotification(featureId: String?, currentFeature: FeatureDetectable?) {
        
        if let feature = currentFeature {
            // If view not loaded, the view could be nil, so I added this check here
            // TODO: Need to review on this to find a better way. Probably not use the topBanner
            //            let offset = topBanner == nil || topBanner.view == nil ? 128: topBanner.view.bounds.height + 10
            let offset = Values.topPanelHeight + UIApplication.topSafeAreaHeight + 28
            
            if let navMapView = self.navigationMapView  {
                FeatureNotification.showNotification(notificationView: notificationView, inView: navMapView, feature: feature, yOffset: offset, onCompletion: { [weak self] in
                    guard let self = self else { return }
                    self.speedLimitView.snp.updateConstraints { make in
                        make.top.equalToSuperview().offset(offset + self.notificationView.bounds.height + 10)
                    }
                    
                }, onDismiss: { [weak self] in
                    guard let self = self else { return }
                    
                    AnalyticsManager.shared.logClosePopupEventV2(popupName: ParameterName.NavigationMode.UserPopup.navigation_notification, screenName: ParameterName.NavigationMode.screen_view)
                    
                    self.speedLimitView.snp.updateConstraints { make in
                        make.top.equalToSuperview().offset(offset + 10)
                    }
                })
                
            }
        }
    }
    
    func overSpeed(value: Bool) {
        speedLimitView.updateOverSpeed(value: value)
    }
    
    func updateTunnel(isInTunnel: Bool) {
        if isInTunnel {
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                // we don't know topBanner here
                if let navMapView = self.navigationMapView {
                    showToast(from: navMapView, message: Constants.toastInTunnel, topOffset: self.getToastOffset(forTunnel: true), icon: "noInternetIcon", duration: 0)
                }
                
            }
        } else {
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                if let navMapView = self.navigationMapView {
                    hideToast(from: navMapView)
                }
            }
        }
    }
    
    // The initial intention of MapViewUpdatable is only for MapLandingVC. But we found it's easy to use it in navigation also.
    // But some functions such as this one is not common. So I just ignore it.
    // The best solution is taking out the functions like this and add child class for MapLanding and also Navigation.
    // TODO: Refactor is possible
    func cruiseStatusUpdate(isCruise: Bool) {
        //No need to handle this
    }
    
    func initERPUpdater(response: RouteResponse?) {
        //No need to handle this
    }
    
    // For cruise, don't need it here
    func updateETA(eta: TripCruiseETA?) {
        //No need to handle this
    }
    
    // For navigation
    func updateETA(eta: TripETA?) {
        /*
        if let tripETA = eta {
            self.liveLocationButton.setName(tripETA.recipientName)
            if tripETA.shareLiveLocation.uppercased() == "Y" {
                self.liveLocationButton.isHidden = false
                self.liveLocationButton.resume()
            } else {
                self.liveLocationButton.pause()
                self.liveLocationButton.isHidden = true
            }
        } else {
            //            self.liveLocationButton.pause()
            self.liveLocationButton.isHidden = true
        }
        self.shareDriveButton.isHidden = !self.liveLocationButton.isHidden
         */
    }
    
    func hideLiveLocationButton() {
        /*
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.shareDriveButton.isHidden = true
            self.liveLocationButton.isHidden = true
        }
         */
    }
    
    func shareDriveButtonEnabled(_ isEnabled: Bool){
        /*
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.shareDriveButton.isHidden = !isEnabled
        }
         */
    }
    
    func updateLiveLocationEnabled(_ isEnabled: Bool) {
        /*
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            // Since we don't know topView, we use a fix value here
            //                        let offset = top.view.bounds.height + top.view.frame.origin.y - self.navigationMapView!.mapView.frame.origin.y - UIApplication.topSafeAreaHeight
            //            let offset = self.yToast1
            
            self.isShowingETAToast = true
            if let mapView = self.navigationMapView?.mapView {
                let offset: CGFloat = self.getToastOffset(forTunnel: false)
                
                if isEnabled {
                    showToast(from: mapView, message: Constants.toastLiveLocationResumed, topOffset: offset, icon: "toastLiveLocationResumed", messageColor: .black, duration: 3)
                } else {
                    showToast(from: mapView, message: Constants.toastLiveLocationPaused, topOffset: offset, icon: "toastLiveLocationPaused", messageColor: .black, duration: 3)
                }
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                self.isShowingETAToast = false
            }
            
            self.liveLocationButton.updateToggleWithOutCallBack(status: isEnabled)
            if(appDelegate().carPlayMapController != nil)
            {
                appDelegate().carPlayMapController?.updateCPCruiseToggleTitle(activity: .navigating,isEnabled:isEnabled,isLiveLoc: true)
            }
        }*/
    }
    
    func showETAMessage(recipient: String, message: String) {
        //        let offset = top.view.bounds.height + top.view.frame.origin.y - self.navigationMapView!.mapView.frame.origin.y - UIApplication.topSafeAreaHeight
        
        DispatchQueue.main.async { [weak self] in
            //            let offset: CGFloat = self.yToast1
            guard let self = self else { return }
            
            let offset: CGFloat = self.getToastOffset(forTunnel: false)
            
            self.isShowingETAToast = true
            let imageWithText = UIImage.generateImageWithText(text: getAbbreviation(name: recipient), font: UIFont(name: fontFamilySFPro.Bold, size: 20.0)!)
            
            if let mapView = self.navigationMapView?.mapView {
                showToast(from: mapView, message: message, topOffset: offset, image: imageWithText, messageColor: .black, font: UIFont(name: fontFamilySFPro.Regular, size: 20.0)!, duration: 5)
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
                self.isShowingETAToast = false
            }
        }
    }
    
    private func getToastOffset(forTunnel: Bool) -> CGFloat {
        let topSafeOffset: CGFloat = UIApplication.topSafeAreaHeight == 0 ? 0 : UIApplication.topSafeAreaHeight
        let offsetY = notificationView.frame.minY < 0 ? topBanner.view.bounds.height - topSafeOffset : topBanner.view.bounds.height + notificationView.frame.height - topSafeOffset + 10
        if forTunnel {
            return isShowingETAToast ? offsetY + 60 : offsetY
        } else {
            return offsetY
        }
    }
    
    func updateRoadName(_ name: String) {
        //No need to handle this
    }
    
    func updateSpeedLimit(_ value: Double) {
        speedLimitView?.speed = value
    }
    
    func isParkingNearByEnable() -> Bool {
        return parkingButton.isSelected
    }
    
    func isShowingAmenity() -> Bool {
        return (parkingButton.isSelected || petrolButton.isSelected || evchargerButton.isSelected)
    }
    
    func showCarparkNearbyWhileStopDriving() {
        if !parkingButton.isSelected {
            parkingButton.select(true)
        }
    }
    
    private func setup() {
        setupNotification()
        setupSpeedLimitView()
        setupMuteButton()
//        setupShareDriveButton()
//        setupLiveLocationButton(show: true)  // setup it after mute button
        setupEvAndPetrolView()
        setupParkingButton()
        setupPetrolBtn()
        setupEvChargerBtn()
        showParkingButton(false)
    }
    
    private func setupNotification() {
        
        notificationView = BreezeNotificationView(leading: 24, trailing: 24)
        broadcastMessageView = BreezeBroadcastMessageView(leading: 24, trailing: 24)
        parkingStatusView = ParkingAvailabilityView(leading: 24, trailing: 24)
        parkingAlertView = ParkingAvailabilityAlertView(leading: 24, trailing: 24)
        obuCommonNotificationView = ObuCommonNotificationView(leading: 24, trailing: 24)
        showCarParkUpdateView = ParkingAndTravelTimeView(leading: 24, trailing: 24)
        showTrafficInformationView = TrafficNotificationView(leading: 24, trailing: 24)
        showGeneralMessageView = GeneralMessageNotificationView(leading: 24, trailing: 24)
        
    }
    
    func updateMuteStatus(isEnabled:Bool){
        self.muteButton.toggleWithOutCallBack(status: !isEnabled)
        NavigationSettings.shared.voiceMuted = isEnabled
        if(appDelegate().carPlayMapController != nil)
        {
            appDelegate().carPlayMapController?.updateCPCruiseToggleTitle(activity: .navigating,isEnabled:isEnabled)
        }
    }
    
    private func setupSpeedLimitView() {
        let offsetY = Values.topPanelHeight + UIApplication.topSafeAreaHeight
        if speedLimitView == nil {
            speedLimitView = CustomSpeedLimitView(frame: CGRect(x: xPadding, y: offsetY, width: iconSize, height: iconSize))
        }
        self.navigationMapView?.mapView.addSubview(speedLimitView!) // to make it behind the toast
        speedLimitView?.speed = 0
        
        speedLimitView?.translatesAutoresizingMaskIntoConstraints = false
        speedLimitView?.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().offset(xPadding)
            //            make.top.equalTo(self.topBanner.view.snp.bottom).offset(20)
            make.top.equalToSuperview().offset(offsetY + 20)
            make.size.equalTo(CGSize(width: iconSize, height: iconSize))
        }
    }
    
    private func setupMuteButton() {
        if self.muteButton == nil {
            let muteButton = ToggleButton(frame: CGRect(x: 0, y: 0, width: 48, height: 48))
            muteButton.isSelected = !Prefs.shared.getBoolValue(.enableAudio)
            muteButton.onImage = Images.unmuteImage
            muteButton.offImage = Images.muteImage
            NavigationSettings.shared.voiceMuted = muteButton.isSelected
            self.delegate?.onMuteEnabled(muteButton.isSelected)
            
            muteButton.onToggled = { [weak self] isSelected in
                guard let self = self else { return }
                NavigationSettings.shared.voiceMuted = isSelected
                self.delegate?.onMuteEnabled(isSelected)
                if isSelected{
                    AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Navigation.UserClick.unmute, screenName: ParameterName.Navigation.screen_view)
                }else{
                    AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Navigation.UserClick.mute, screenName: ParameterName.Navigation.screen_view)
                }
            }
            navigationMapView?.mapView.addSubview(muteButton)
            self.muteButton = muteButton
            self.muteButton.toggleWithOutCallBack(status: !NavigationSettings.shared.voiceMuted)
        }
        
        muteButton.translatesAutoresizingMaskIntoConstraints = false
        muteButton.snp.makeConstraints { make in
            make.trailing.equalToSuperview().offset(-trailingX)
            make.width.equalTo(48)
            make.height.equalTo(48)
            make.top.equalTo(self.speedLimitView.snp.top)
        }
    }
    
    //  MARK: - Parking button
    private func setupParkingButton() {
        
        if self.parkingButton == nil {
            let parkingButton = ToggleButton(frame: CGRect(x: 0, y: 0, width: 48, height: 48))
            parkingButton.onImage = UIImage(named: "obu_lite_parking_inactive")
            parkingButton.offImage = UIImage(named: "obu_lite_parking_active")            
            parkingButton.addTarget(self, action: #selector(onParkingButtonClicked), for: .touchUpInside)
            
            parkingButton.onToggled = { [weak self] isSelected in
                guard let self = self else { return }
                self.delegate?.onParkingEnabled(isSelected)
                if isSelected {
                    //  Need to remove other amenity
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { [weak self] in
                        guard let self = self else { return }
                        if self.evchargerButton.isSelected {
                            self.evchargerButton.select(false)
                        }
                        if self.petrolButton.isSelected {
                            self.petrolButton.select(false)
                        }
                    }
                }
                
                let event = isSelected ? ParameterName.NavigationMode.UserToggle.p_button_on : ParameterName.NavigationMode.UserToggle.p_button_off
                AnalyticsManager.shared.logToggleEvent(eventValue: event, screenName: ParameterName.NavigationMode.screen_view)
            }
            
            self.petrolAndEvView.addSubview(parkingButton)
            self.parkingButton = parkingButton
            self.parkingButton?.translatesAutoresizingMaskIntoConstraints = false
           
            parkingButton.snp.makeConstraints { make in
                make.leading.equalTo(5)
                make.width.equalTo(48)
                make.height.equalTo(48)
                make.top.equalTo(petrolAndEvView.snp.top).offset(5)
            }
        }
    }
    
    // MARK: - Ev Charger Button For Turn By Turn
    private func setupEvChargerBtn() {
        if self.evchargerButton == nil {
            let evChargerButton = ToggleButton(frame: CGRect(x: 0, y: 0, width: 48, height: 48))
            evChargerButton.onImage = UIImage(named: "obu_lite_ev_inactive")
            evChargerButton.offImage = UIImage(named: "obu_lite_ev_active")
            evChargerButton.onToggled = { [weak self] isSelected in
                guard let self = self else { return }
                self.delegate?.onEvChargerAction(isSelected)
                if isSelected {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                        
                        if self.parkingButton.isSelected {
                            self.parkingButton.select(false)
                        }
                        
                        if self.petrolButton.isSelected {
                            self.petrolButton.select(false)
                        }
                    }
                }
            }
            
//            navigationMapView?.mapView.addSubview(evChargerButton)
            petrolAndEvView.addSubview(evChargerButton)
            self.evchargerButton = evChargerButton
        }
        evchargerButton.translatesAutoresizingMaskIntoConstraints = false
        evchargerButton.snp.makeConstraints { make in
            make.leading.equalTo(5)
            make.width.equalTo(48)
            make.height.equalTo(48)
            make.top.equalTo(self.petrolButton.snp.bottom).offset(15)
            make.bottom.equalToSuperview().offset(-5)
        }
    }
    
    // MARK: - Ev Petrol Button For Turn By Turn
    private func setupPetrolBtn() {
        if self.petrolButton == nil {
            let petrolButton = ToggleButton(frame: CGRect(x: 0, y: 0, width: 48, height: 48))
            petrolButton.onImage = UIImage(named: "obu_lite_petrol_inactive")
            petrolButton.offImage = UIImage(named: "obu_lite_petrol_active")
            
            petrolButton.onToggled = { [weak self] isSelected in
                guard let self = self else { return }
                self.delegate?.onPetrolAction(isSelected)
                if isSelected {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                        
                        if self.parkingButton.isSelected {
                            self.parkingButton.select(false)
                        }
                        
                        if self.evchargerButton.isSelected {
                            self.evchargerButton.select(false)
                        }
                    }
                }
            }
            
//            navigationMapView?.mapView.addSubview(petrolButton)
            petrolAndEvView.addSubview(petrolButton)
            self.petrolButton = petrolButton
        }
        petrolButton.translatesAutoresizingMaskIntoConstraints = false
        petrolButton.snp.makeConstraints { make in
            make.leading.equalTo(5)
            make.width.equalTo(48)
            make.height.equalTo(48)
//            make.top.equalTo(parkingButton.snp.bottom).offset(15)
            make.top.equalTo(petrolAndEvView.snp.top).offset(68)
        }
    }
    
    private func setupEvAndPetrolView() {
        if self.petrolAndEvView == nil {
            let view = UIView(frame: CGRect(x: 0, y: 0, width: 58, height: 184))
            view.backgroundColor = UIColor(hexString: "#FCFCFC", alpha: 0.75)
            view.layer.cornerRadius = 29.0
            view.layer.masksToBounds = true
            navigationMapView?.mapView.addSubview(view)
            self.petrolAndEvView = view
        }
        
        petrolAndEvView.translatesAutoresizingMaskIntoConstraints = false
        
        petrolAndEvView.snp.makeConstraints { make in
            make.trailing.equalToSuperview().offset(-21)
            make.width.equalTo(58)
//            make.height.equalTo(184)
            make.top.equalTo(self.muteButton.snp.bottom).offset(12)
        }
    }
    
    // MARK: - Share drive button on top of the live location button
    private func setupShareDriveButton() {
        if self.shareDriveButton == nil {
            let button = UIButton(frame: CGRect(x: 0, y: 0, width: iconSize2, height: iconSize2))
            button.setImage(UIImage(named: "shareDriveBtnIcon"), for: .normal)
            button.addTarget(self, action: #selector(onShareDriveButtonClicked), for: .touchUpInside)
            navigationMapView?.mapView.addSubview(button)
            self.shareDriveButton = button
        }
        
        // setup auto-layout
        self.shareDriveButton.translatesAutoresizingMaskIntoConstraints = false
        self.shareDriveButton.snp.makeConstraints { make in
            make.trailing.equalToSuperview().offset(-trailingX)
            if OBUHelper.shared.hasLastObuData() {
                if OBUHelper.shared.isObuConnected() {
                    make.bottom.equalToSuperview().offset(-(110 + UIApplication.bottomSafeAreaHeight + 40 + showCarparkListHeight))
                } else {
                    make.bottom.equalToSuperview().offset(-(110 + UIApplication.bottomSafeAreaHeight + 60 + showCarparkListHeight))
                }
            } else {
                make.bottom.equalToSuperview().offset(-(110 + UIApplication.bottomSafeAreaHeight + showCarparkListHeight))
            }
            make.width.equalTo(iconSize2)
            make.height.equalTo(iconSize2)
        }
        
        self.shareDriveButton.isHidden = false
    }
    
    @objc func onShareDriveButtonClicked() {
        self.delegate?.onShareDriveEnabled(true)
        
    }
    
    @objc func onParkingButtonClicked() {
        self.delegate?.onParkingAction()
    }
    
//    @objc func actionButtonEV() {
//        isSelectedAmenitiesEv.toggle()
//        if isSelectedAmenitiesEv {
//            //            self.activeEVAmenities()
//            evchargerButton.setImage(UIImage(named: "ev_charger_icon_selected"), for: .normal)
//            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
//                //                self.notActivePetrolAmenities()
//                self.petrolButton.setImage(UIImage(named: "petrol_icon"), for: .normal)
//            }
//            isSelectedAmenitiesPetrol = false
//        } else {
//            //            self.notActiveEVAmenities()
//            evchargerButton.setImage(UIImage(named: "ev_charger_icon"), for: .normal)
//        }
//    }
    
    
//    @objc func actionButtonPetrol() {
//        isSelectedAmenitiesPetrol.toggle()
//        if isSelectedAmenitiesPetrol {
//            //            self.activePetrolAmenities()
//            petrolButton.setImage(UIImage(named: "petrol_icon_selected"), for: .normal)
//            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
//                //                self.notActiveEVAmenities()
//                self.evchargerButton.setImage(UIImage(named: "ev_charger_icon"), for: .normal)
//            }
//            isSelectedAmenitiesEv = false
//        } else {
//            //            self.notActivePetrolAmenities()
//            petrolButton.setImage(UIImage(named: "petrol_icon"), for: .normal)
//        }
//    }
    
    // MARK: - Live location button related functions
    private func setupLiveLocationButton(show: Bool) {
        guard show else { return }
        
        if self.liveLocationButton == nil {
            let etaButton = LiveLocationSharingButton(frame: CGRect(x: 0, y: 0, width: 62, height: 62))
            etaButton.onImage = Images.liveLocationResumeImage
            etaButton.offImage = Images.liveLocationPauseImage
            
            var isFirstTime: Bool = true
            etaButton.onToggled = { [weak self] isStarted in
                guard let self = self else { return }
                
                // Don't show toast the first time
                if isFirstTime {
                    isFirstTime = false
                } else {
                    self.delegate?.onLiveLocationEnabled(isStarted)
                }
            }
            
            navigationMapView?.mapView.addSubview(etaButton)
            self.liveLocationButton = etaButton
            
            liveLocationButton.translatesAutoresizingMaskIntoConstraints = false
            liveLocationButton.snp.makeConstraints { make in
                make.center.equalTo(shareDriveButton.snp.center)
                make.width.equalTo(62)
                make.height.equalTo(62)
            }
        }
    }
    
    func toggleOffCarparkNearby() {
        if self.parkingButton.isSelected {
            self.parkingButton.select(false)
        }
    }
    
    func toggleOffAmenityOnMap() {
        
        if self.parkingButton.isSelected {
            self.parkingButton.select(false)
        }
        
        if self.evchargerButton.isSelected {
            self.evchargerButton.select(false)
        }
        if self.petrolButton.isSelected {
            self.petrolButton.select(false)
        }
    }
    
    
    private func calculateFurthestDistance(coordinates:[CLLocationCoordinate2D],defaultFurthestDistance:Double,nearestCoordinate:CLLocationCoordinate2D) -> Double {
        var finalFurthestDistance = defaultFurthestDistance
        for coordinate in coordinates {
            let toLocation = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
            let fromLocation = CLLocation(latitude: nearestCoordinate.latitude, longitude: nearestCoordinate.longitude)
            let distance = fromLocation.distance(from: toLocation)
            if(distance > finalFurthestDistance) {
                finalFurthestDistance = distance
            }
        }
        return finalFurthestDistance
    }
    
    private func getBearing() -> CLLocationDirection? {
        if let bearing = navigationMapView?.mapView.cameraState.bearing {
            return bearing
        }
        return nil
    }    
    
    func adjustDynamicZoomLevel(_ coordinates: [CLLocationCoordinate2D], currentLocation: CLLocationCoordinate2D?, destination: CLLocationCoordinate2D? = nil) {
        
        if let location = currentLocation {
            var allCoordinates: [CLLocationCoordinate2D] = []
            allCoordinates.append(contentsOf: coordinates)
            allCoordinates.append(location)
            
            let offset = Values.topPanelHeight + UIApplication.topSafeAreaHeight + 8
            let padding = UIEdgeInsets(top: offset, left: 72, bottom: UIApplication.bottomSafeAreaHeight + Values.bottomPanelHeight + (OBUHelper.shared.hasLastObuData() ? 110 : 72), right: 72)

            if let cameraOptions = self.navigationMapView?.mapView.mapboxMap.camera(for: allCoordinates,
                                                                                    padding: padding,
                                                                                    bearing: getBearing(),
                                                                                    pitch: nil), let mapView = self.navigationMapView?.mapView {

                mapView.camera.ease(to: cameraOptions, duration: 0.4, curve: .easeInOut, completion: nil)
            }
            
            //  Solution 2
//            let polygon = Turf.Polygon([allCoordinates])
//            if let centerPoint = polygon.center, let mapview = navigationMapView?.mapView {
//                let option = CameraOptions(center: centerPoint, padding: padding)
//
//                var rect = mapview.frame
//                rect.origin.x += 72
//                rect.origin.y += padding.top
//                rect.size.width = mapview.frame.width - 2*72
//                rect.size.height = mapview.frame.height - padding.top - padding.bottom
//
//                print("rect: \(rect)")
//
//                if let cameraOptions = self.navigationMapView?.mapView.mapboxMap.camera(for: allCoordinates,
//                                                                                        camera: option,
//                                                                                        rect: rect), let mapView = self.navigationMapView?.mapView {
//
//                    mapView.camera.ease(to: cameraOptions, duration: 0.4, curve: .easeInOut, completion: nil)
//                }
//            }
            
            //  Solution 3
//            if let desLocation = destination {
////                let centerPoint = Turf.mid(desLocation, location)
//                allCoordinates.append(desLocation)
//                let polygon = Turf.Polygon([allCoordinates])
//
//                if let centerPoint = polygon.center {
//                    allCoordinates.append(centerPoint)
//
//                    let finalFarthestDistance = self.calculateFurthestDistance(coordinates: allCoordinates, defaultFurthestDistance: 0.0, nearestCoordinate: centerPoint)
//
//                    print("finalFarthestDistance: \(finalFarthestDistance)")
//
//                    let offset = Values.topPanelHeight + UIApplication.topSafeAreaHeight + 8
//                    let padding = UIEdgeInsets(top: offset, left: 28, bottom: UIApplication.bottomSafeAreaHeight + Values.bottomPanelHeight + 16, right: 28)
//                    self.navigationMapView?.mapView.setDynamicZoomLevelBasedonRadius(coordinate1: coordinates[0], dynamicRadius: Int(finalFarthestDistance), padding: padding, coordinate2: centerPoint, multiplyValue: 2.2)
//                }
//
//            } else {
//                let finalFarthestDistance = self.calculateFurthestDistance(coordinates: allCoordinates, defaultFurthestDistance: 0.0, nearestCoordinate: location)
//                let offset = Values.topPanelHeight + UIApplication.topSafeAreaHeight + 8
//                let padding = UIEdgeInsets(top: offset, left: 28, bottom: UIApplication.bottomSafeAreaHeight + Values.bottomPanelHeight + 16, right: 28)
//                self.navigationMapView?.mapView.setDynamicZoomLevelBasedonRadius(coordinate1: coordinates[0], dynamicRadius: Int(finalFarthestDistance), padding: padding, coordinate2: location, multiplyValue: 2.2)
//            }
            
        }
    }
    
    func showParkingButton(_ isShow: Bool) {
        self.parkingButton.isHidden = !isShow
        if isShow {
            self.petrolButton.snp.updateConstraints { make in
                make.top.equalTo(68)
            }
        } else {
            self.petrolButton.snp.updateConstraints { make in
                make.top.equalTo(5)
            }
        }
    }
    
    
    func removeParkingAlert() {
        DispatchQueue.main.async {
            self.parkingAlertView.dismiss(self.parkingAlertView.currentStatus, carpark: nil)
        }
    }
 
}

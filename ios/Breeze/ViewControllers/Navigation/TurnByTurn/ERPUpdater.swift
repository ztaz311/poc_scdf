//
//  ERPUpdater.swift
//  Breeze
//
//  Created by Zhou Hao on 9/4/21.
//

import Foundation
@_spi(Experimental) import MapboxMaps
import MapboxCoreMaps
import Turf
import SwiftyBeaver
import AWSAppSync
import UIKit
import RxSwift

protocol ERPUpdaterListener {
    func shouldAddERPItems(noCostfeatures:Turf.FeatureCollection, costfeatures:Turf.FeatureCollection)
}

//ERP Updater
final class ERPUpdater {
        
    private var erpRefreshTimer:Timer?
    private var erp30MinRefreshTimer:Timer?
    //private var parseFromRouteNav = false
    private var erpSVC: ERPServiceProtocol!
    private var listeners: MulticastDelegate<ERPUpdaterListener>!
    private var erpJsonData:ERPBaseModel?
    private var currentCostFeatureCollection = FeatureCollection(features: [])      // For displaying on map as point objects
    private var currentNoCostFeatureCollection = FeatureCollection(features: [])    // For displaying on map as point objects
    private var uniqueERPTimes = [String]()
    public private(set) var erpAllFeatureCollection = FeatureCollection(features: [])
    public private(set) var erpFeatureDetectionCostFeatures = FeatureCollection(features: [])   // For EH as lineString objects
    public private(set) var erpFeatureDetectionNoCostFeatures = FeatureCollection(features: []) // For EH as lineString objects
        
    // MARK: - Public functions
    init(erpService: ERPServiceProtocol) {
        self.erpSVC = erpService
        self.listeners = MulticastDelegate<ERPUpdaterListener>()
    }
        
    func load(_ completion: @escaping (Bool) -> Void) {
        fetchPoints() { [weak self] (noCostFeatures,costFeatures,allFeatures) in
            guard let self = self else { return completion(false) }
            
            self.currentCostFeatureCollection = costFeatures
            self.currentNoCostFeatureCollection = noCostFeatures
            self.erpAllFeatureCollection = allFeatures
            self.listeners.invokeDelegates { delegate in
                DispatchQueue.main.async {
                    SwiftyBeaver.debug("shouldAddERPItems from fetchAndProcessERPData")
                    delegate.shouldAddERPItems(noCostfeatures: noCostFeatures, costfeatures: costFeatures)
                }
            }
            // This is called when app first launch or logout/login again
            self.invalidateERPRefreshTimer()
            if self.uniqueERPTimes.count > 0 {
                self.checkCurrentIsBetweenUniqueTimes()
            }
            completion(true)
        }
    }
    
    func getERPData() -> ERPBaseModel?{
        
        if let erpJsonData = erpJsonData {
            return erpJsonData
        }
        
        return nil
    }
    
    deinit {
        listeners.delegates.forEach { unbind(listener: $0) }
    }
    
    func bind(listener: ERPUpdaterListener) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            SwiftyBeaver.debug("shouldAddERPItems from bind")
            listener.shouldAddERPItems(noCostfeatures: self.currentNoCostFeatureCollection, costfeatures: self.currentCostFeatureCollection)
        }
        listeners.removeDelegate(listener)
        listeners.addDelegate(listener)
    }
    
    func unbind(listener: ERPUpdaterListener) {
        listeners.removeDelegate(listener)
    }
    
    func update(erpData: ERPBaseModel) {

        self.sendERPDataToRN(json: erpData)
        self.parseJSONItems(json: erpData) { [weak self] (noCostFeatures, costFeatures,allFeatures) in
            guard let self = self else { return }
            SwiftyBeaver.debug("ERP update: \(noCostFeatures.features.count) noCost, \(costFeatures.features.count) cost")
            
            self.currentCostFeatureCollection = costFeatures
            self.currentNoCostFeatureCollection = noCostFeatures
            self.erpAllFeatureCollection = allFeatures
            self.listeners.invokeDelegates { delegate in
                DispatchQueue.main.async {
                    SwiftyBeaver.debug("shouldAddERPItems from update")
                    delegate.shouldAddERPItems(noCostfeatures: noCostFeatures, costfeatures: costFeatures)
                }
            }
        }
    }

    @objc private func refreshERP() {
        
        SwiftyBeaver.debug("refreshERP")
        self.invalidateERPRefreshTimer()
        if let erpJsonData = erpJsonData {
            update(erpData: erpJsonData)
            if(uniqueERPTimes.count > 0)
            {
                checkCurrentIsBetweenUniqueTimes()
            }
        }
    }
    
    @objc private func refreshJSON(){
        
        if(erp30MinRefreshTimer != nil)
        {
            
            erp30MinRefreshTimer?.invalidate()
            erp30MinRefreshTimer = nil
        }
        if let erpJsonData = erpJsonData {
            update(erpData: erpJsonData)
        }
        
    }
    
    func refreshOnlyJsonFor30MinTimer(differenceInterval:TimeInterval){
        
        if(erp30MinRefreshTimer == nil)
        {
            erp30MinRefreshTimer = Timer.scheduledTimer(timeInterval:differenceInterval, target: self, selector: #selector(refreshJSON), userInfo: nil, repeats: false)
        }
        
    }
    
    func refreshERPFromDataCenter(){
        self.refreshERP()
    }
    
    func invalidateERPRefreshTimer(){
        if(erpRefreshTimer != nil)
        {
            SwiftyBeaver.debug("ERP REFRESH TIMER INVALIDATE TO START ANOTHER:")
            erpRefreshTimer?.invalidate()
            erpRefreshTimer = nil
        }
    }
        
    private func fetchPoints(withCompletion completion: @escaping ((Turf.FeatureCollection,Turf.FeatureCollection,Turf.FeatureCollection) -> Void)) {
        DispatchQueue.global(qos: .background).async {
            
            self.getERPDataFromNetwork(withCompletion: completion)
//            self.getERPDataFromNetwork { (costFeatures, noCostFeatures,allFeatures) in
//                completion(costFeatures,noCostFeatures,allFeatures)
//            }
            //Retrieve ERP Data from local file if available
           /* retrieveERPFromJsonFile { (result) in
                switch result{
                
                case .success(let json):
                    
                    print(json)
                    if(json.publicholiday != nil)
                    {
                        
                        //Then parse the JSON items and add to the maps
                        self.parseJSONItems(json: json) { (costFeatures, noCostFeatures) in
                            completion(costFeatures,noCostFeatures)
                        }
                        
                    }
                    else
                    {
                        //else fetch ERP data from Network
                        self.getERPDataFromNetwork { (costFeatures, noCostFeatures) in
                            completion(costFeatures,noCostFeatures)
                        }
                    }
                    
                    
                case .failure(_):
                    //else fetch ERP data from Network
                    self.getERPDataFromNetwork { (costFeatures, noCostFeatures) in
                        completion(costFeatures,noCostFeatures)
                    }
                }
            }*/
        }
    }
    
    func getUniqueTimes(erpRates:[Rates]) {
        for i in 0..<erpRates.count{
            
            let startTime = erpRates[i].starttime
            if(!uniqueERPTimes.contains(startTime!))
            {
                uniqueERPTimes.append(startTime!)
            }
        }
        
//        SwiftyBeaver.debug("UNIQUE TIMES: \(uniqueERPTimes)")
    }
    
    func checkCurrentIsBetweenUniqueTimes(){
        let sortedArray = uniqueERPTimes.sorted { $0.localizedStandardCompare($1) == .orderedAscending }
            
        for (index, startTime) in sortedArray.enumerated() {
            
            
            if(index != sortedArray.endIndex-1)
            {
                if(index != sortedArray.endIndex-1 && Date().currentTimeisBetween(startTime: startTime, endTime: sortedArray[index + 1]))
                {
                    SwiftyBeaver.debug("IN BETWEEN: \(sortedArray[index])")
                    SwiftyBeaver.debug("IN BETWEEN next: \(sortedArray[index+1])")
                
                    let currentTime = Date()
                    
                    let strCurTime =  DateUtils.shared.getTimeDisplay(dateFormat: Date.formatHHmmss, date: currentTime)
                    
                    SwiftyBeaver.debug("TIME DIFF: \(currentTime.findDateDiff(time1Str: strCurTime, time2Str: sortedArray[index+1]))")
                    
                    let result = currentTime.findDateDiff(time1Str: strCurTime, time2Str: sortedArray[index+1], isSameDay: true)
                    
                    if(erpRefreshTimer == nil)
                    {
                        SwiftyBeaver.debug("ERP REFRESH TIMER: \(result.0)")
                        erpRefreshTimer = Timer.scheduledTimer(timeInterval:result.0, target: self, selector: #selector(refreshERP), userInfo: nil, repeats: false)
                    }
                   
                    break;
                }
                
            } else if( index == sortedArray.endIndex-1 ) {
                
                let currentTime = Date()
                
                let strCurTime = DateUtils.shared.getTimeDisplay(dateFormat: Date.formatHHmmss, date: currentTime)
                
                SwiftyBeaver.debug("TIME DIFF END OF THE INDEX: \(currentTime.findDateDiff(time1Str: strCurTime, time2Str: sortedArray[0]))")
                let result = currentTime.findDateDiff(time1Str: strCurTime, time2Str: sortedArray[0],isSameDay: false)
                
                if(erpRefreshTimer == nil) {
                    erpRefreshTimer = Timer.scheduledTimer(timeInterval:result.0, target: self, selector: #selector(refreshERP), userInfo: nil, repeats: false)
                }
                
                break;
            }
            
            
        }
        
        
    }
    
    
    private func parseJSONItems(json:ERPBaseModel, _ completion: @escaping ((Turf.FeatureCollection,Turf.FeatureCollection,Turf.FeatureCollection)-> Void)) {
        
        var erpCostfeatures = [Turf.Feature]()
        var erpLineCostfeatures = [Turf.Feature]()
        var erpNoCostfeatures = [Turf.Feature]()
        var erpLineNoCostfeatures = [Turf.Feature]()
        
        var erpFeatures = [Turf.Feature]()
        
        let publicHolidays = json.publicholiday
        erpJsonData = json
        let erp = json.erp
        
        let tyepOfDay = Date().isWeekend(date: Date())
        
        // set erp amount to 0 if it's a public holiday or sunday
        if(checkERPPublicHoliday(publicHoliday: publicHolidays!) || tyepOfDay == ERPValues.isSunWeekend) {
            
            //This is looping from erp array
            var erpCount = 0
            for erpItem in erp ?? [] {
                
                let (startCoordinate, endCoordinate) = erpItem.getStartEndCoordinate()
                
                let midCoordinate2D = mid(startCoordinate, endCoordinate)
                var coordinates = [CLLocationCoordinate2D]()
                coordinates.append(startCoordinate)
                coordinates.append(endCoordinate)
                
                // Beta.15
                //             var feature = Turf.Feature(LineString(coordinates))
                var lineFeature = Turf.Feature(geometry: .lineString(LineString(coordinates)))
                
                lineFeature.properties = [
                    "name":.string(erpItem.name ?? ""),
                    "cost":.string("$0.0"),
                    "erpid":.string("\(erpItem.erpid ?? 0)"),
                    "zoneid":.string(erpItem.zoneid ?? ""),
                    "erpOrder":.number(Double(erpCount)),
                ]
                erpFeatures.append(lineFeature)
                
                var pointFeature = Turf.Feature(geometry: .point(Point(midCoordinate2D)))
                
                pointFeature.properties = [
                    "name":.string(erpItem.name ?? ""),
                    "cost":.string("$0.0"),
                    "erpOrder":.number(Double(erpCount)),
                ]
                erpNoCostfeatures.append(pointFeature)
                
                let noCostlineFeature = Turf.Feature(geometry: .lineString(LineString(coordinates)))
                 
                lineFeature.properties = [
                    "name":.string(erpItem.name ?? ""),
                    "cost":.string("$0.0"),
                    "erpid":.string("\(erpItem.erpid ?? 0)"),
                    "zoneid":.string(erpItem.zoneid ?? ""),
                    "erpOrder":.number(Double(erpCount)),
                ]
                erpCount += 1
                erpLineNoCostfeatures.append(noCostlineFeature)
                
            }
            
            let currentDate = DateUtils.shared.getTimeDisplay(date: Date()) //Date().dateToString(date: Date())
            
            /// if public holiday is  not nil, find current date is matches public holiday json data
            /// if matches get the public holiday end time
            /// then get the time difference between current time to the puublic holiday end time
            
            if let publicHolidays = publicHolidays {
                var endTime = ""
                for holiday in publicHolidays{
                    
                    if(currentDate == holiday.date)
                    {
                        endTime = holiday.endTime ?? ""
                        break
                    }
                }
                
                let currentTime = Date()
                
                let strCurTime = DateUtils.shared.getTimeDisplay(dateFormat: Date.formatHHmmss, date: currentTime)
               
                if(endTime == ""){
                    
                    //Get end of the Day
                    let endOfDay = currentTime.endOfDay.timeIntervalSinceNow

                    if(erpRefreshTimer == nil)
                    {
                        SwiftyBeaver.debug("ERP REFRESH TIMER STARTED ON PUBLIC HOLIDAY/SUNDAY:")
                        erpRefreshTimer = Timer.scheduledTimer(timeInterval:endOfDay, target: self, selector: #selector(refreshERP), userInfo: nil, repeats: false)
                    }
                }
                else{
                    
                    let result = currentTime.findDateDiff(time1Str: strCurTime, time2Str: endTime)
                    if(erpRefreshTimer == nil)
                    {
                        erpRefreshTimer = Timer.scheduledTimer(timeInterval:result.0, target: self, selector: #selector(refreshERP), userInfo: nil, repeats: false)
                    }
                }
                
            }
            else{
                
                //Get end of the Day
                let endOfDay = Date().endOfDay.timeIntervalSinceNow

                if(erpRefreshTimer == nil)
                {
                    SwiftyBeaver.debug("ERP REFRESH TIMER STARTED ON PUBLIC HOLIDAY/SUNDAY:")
                    erpRefreshTimer = Timer.scheduledTimer(timeInterval:endOfDay, target: self, selector: #selector(refreshERP), userInfo: nil, repeats: false)
                }
            }
            
            
        }
        else
        {
            //This is looping from erp array
            var erpCount = 0
            for erpItem in erp ?? [] {
                
                //From root json I will erp rates and see if zone id matches
                if let i = json.getERPRates().firstIndex(where: { $0.zoneid == erpItem.getZoneId() }) {
                    
                    getUniqueTimes(erpRates: json.getERPRates()[i].getRates())
                    
                    let rates = json.getERPRates()[i].getRates()
                    
                    let (startCoordinate, endCoordinate) = erpItem.getStartEndCoordinate()
                    
                    var chargeAmount = getERPChargeAmount(erpRates:rates)
                    
                    #if DEMO
                    //if(DemoSession.shared.isDemoRoute){
                        if(erpItem.erpid == 36){
                            chargeAmount = "$2.00"
                        }
                    //}
                    #endif
                    
                    if(chargeAmount != "0")
                    {
                       let midCoordinate2D = mid(startCoordinate, endCoordinate)
                        
                        var coordinates = [CLLocationCoordinate2D]()
                                                
                            coordinates.append(startCoordinate)
                            coordinates.append(endCoordinate)
                            
                            // Beta.15
//                            var feature = Turf.Feature(LineString(coordinates))
                        var lineAllFeature = Turf.Feature(geometry: .lineString(LineString(coordinates)))
                        
                           
                        lineAllFeature.properties = [
                                
                                "name":.string(erpItem.name ?? ""),
                                "cost":.string(chargeAmount),
                                "erpid":.string("\(erpItem.erpid ?? 0)"),
                                "zoneid":.string(erpItem.zoneid ?? "" ),
                                "erpOrder":.number(Double(erpCount)),
                            ]
                            erpFeatures.append(lineAllFeature)
                        
                            var lineFeature = Turf.Feature(geometry: .lineString(LineString([startCoordinate,endCoordinate])))
                        
                            var pointFeature = Turf.Feature(geometry: .point(Point(midCoordinate2D)))

                            pointFeature.properties = [
                                
                                "name":.string(erpItem.name ?? ""),
                                "cost":.string(chargeAmount),
                                "erpid":.string("\(erpItem.erpid ?? 0)"),
                                "zoneid":.string(erpItem.zoneid ?? "" ),
                                "erpOrder":.number(Double(erpCount)),
                                
                            ]
                            
                            lineFeature.properties = [
                                
                                "name":.string(erpItem.name ?? ""),
                                "cost":.string(chargeAmount),
                                "erpid":.string("\(erpItem.erpid ?? 0)"),
                                "zoneid":.string(erpItem.zoneid ?? "" ),
                                "erpOrder":.number(Double(erpCount)),
                            ]
                                                    
                            //If the amount is available then I will add into erp cost features
                            erpCostfeatures.append(pointFeature)
                            erpLineCostfeatures.append(lineFeature)
                       
                        
                    }
                    else
                    {
                        // Beta.15
                        let (startCoordinate, endCoordinate) = erpItem.getStartEndCoordinate()
                         
                        let midCoordinate2D = mid(startCoordinate, endCoordinate)
                        var coordinates = [CLLocationCoordinate2D]()
                        coordinates.append(startCoordinate)
                        coordinates.append(endCoordinate)
                        
                        var lineFeature = Turf.Feature(geometry: .lineString(LineString(coordinates)))
                         
                        lineFeature.properties = [
                            "name":.string(erpItem.name ?? ""),
                            "cost":.string(chargeAmount),
                            "erpid":.string("\(erpItem.erpid ?? 0)"),
                            "zoneid":.string(erpItem.zoneid ?? ""),
                            "erpOrder":.number(Double(erpCount)),
                        ]
                        erpLineNoCostfeatures.append(lineFeature)
                         
                         var pointFeature = Turf.Feature(geometry: .point(Point(midCoordinate2D)))
                          
                         pointFeature.properties = [
                             "name":.string(erpItem.name ?? ""),
                             "cost":.string("$0.0"),
                             "erpOrder":.number(Double(erpCount)),
                          ]
                        erpNoCostfeatures.append(pointFeature)
                        
                        var line2Feature = Turf.Feature(geometry: .lineString(LineString(coordinates)))
                         
                        line2Feature.properties = [
                            "name":.string(erpItem.name ?? ""),
                            "cost":.string("$0.0"),
                            "erpid":.string("\(erpItem.erpid ?? 0)"),
                            "zoneid":.string(erpItem.zoneid ?? ""),
                            "erpOrder":.number(Double(erpCount)),
                         ]
                        erpFeatures.append(line2Feature)
                        

                    }
                    
                }
                
                erpCount += 1
            }
            SwiftyBeaver.debug("UNIQUE TIMES: \(uniqueERPTimes)")
        }
        
        
        DispatchQueue.main.async { [self] in

                //For Route Planning
                let erpAllFeatureCollection = FeatureCollection(features: erpFeatures)

                //To Display on MapLanding Map
                let erpNoCostFeatureCollection = FeatureCollection(features: erpNoCostfeatures)
               
                //To Display on Navigation Map
                let erpCostFeatureCollection = FeatureCollection(features: erpCostfeatures)
                
                self.erpFeatureDetectionCostFeatures = FeatureCollection(features: erpLineCostfeatures)
                self.erpFeatureDetectionNoCostFeatures = FeatureCollection(features: erpLineNoCostfeatures)
                completion(erpNoCostFeatureCollection,erpCostFeatureCollection,erpAllFeatureCollection)
            
        }
    }
    
//    private func returnNoChargeERPFeature(erpItem:Erp) -> Turf.Feature {
//        
//        let startLat = erpItem.startlat?.toDouble()!
//        let startLong = erpItem.startlong?.toDouble()!
//        let endLat = erpItem.endlat?.toDouble()!
//        let endLong = erpItem.endlong?.toDouble()!
//        let startCoordinate = CLLocationCoordinate2D(latitude: startLat!, longitude: startLong!)
//         
//        let endCoordinate = CLLocationCoordinate2D(latitude: endLat!, longitude: endLong!)
//         
//        let midCoordinate2D = mid(startCoordinate, endCoordinate)
//         
//         var coordinates = [CLLocationCoordinate2D]()
//         
////         if(parseFromRouteNav == true)
////         {
//             coordinates.append(startCoordinate)
//             coordinates.append(endCoordinate)
//             
//            // Beta.15
////             var feature = Turf.Feature(LineString(coordinates))
//            var feature = Turf.Feature(geometry: .lineString(LineString(coordinates)))
//             
//             feature.properties = [
//                "name":.string(erpItem.name ?? ""),
//                "cost":.string("$0.0"),
//             ]
//
//             return feature
//         //}
////         else
////         {
//             coordinates.append(midCoordinate2D)
//
//            // Beta.15
////             var feature = Turf.Feature(Point(midCoordinate2D))
//            var feature = Turf.Feature(geometry: .point(Point(midCoordinate2D)))
//             
//             feature.properties = [
//                "name":.string(erpItem.name ?? ""),
//                "cost":.string("$0.0"),
//             ]
//
//            return feature
//         //}
//    }
    
    func sendERPDataToRN(json:ERPBaseModel){
        
        if json.erp != nil {
            
            do {
                let jsonData = try JSONEncoder().encode(json)
                let jsonString = String(data: jsonData, encoding: .utf8)!
                print(jsonString)

//                let _ = ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.SEND_ERP_DATA_TO_REACT, data: ["ERP_DATA": jsonString] as NSDictionary )
                
                let _ = ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.SEND_ERP_DATA_TO_REACT, data: ["ERP_DATA": jsonString] as NSDictionary ).subscribe(onSuccess: {_ in
                    //No need to process this result
                },onFailure: {_ in
                    //No need to process this result
                })
                
            } catch { print(error) }
            
            
        }
    }
    
    private func getERPDataFromNetwork(withCompletion completion: @escaping ((Turf.FeatureCollection,Turf.FeatureCollection,Turf.FeatureCollection)-> Void)){
        
        erpSVC.getERPDetails() { (result) in
            
            switch result {
            case .success(let json):
                DispatchQueue.main.async {
                    #if targetEnvironment(simulator)
                    saveToJsonFile(result: json)
                    #endif
                    SwiftyBeaver.debug("getERPDataFromNetwork ERP Json: \(json)")
                    self.sendERPDataToRN(json: json)
                    self.parseJSONItems(json: json, completion)
                }
                
            case .failure(let error):
                DispatchQueue.main.async {
                    SwiftyBeaver.debug("Failed to getERPDataFromNetwork: \(error.localizedDescription)")
                    // We need to return, otherwise the DataCenter loading will never be complete
                    completion(FeatureCollection(features: []),FeatureCollection(features: []),FeatureCollection(features: []))
                }
            }
        }
    }
}

extension MapView {
    
    func removeERPLayer(){
        DispatchQueue.main.async {
            
            self.removeERPCostLayer()
            self.removeERPNoCostLayer()
        }
        
    }
    
    func removeERPCostLayer() {
        DispatchQueue.main.async {
        do {
            try self.mapboxMap.style.removeLayer(withId: ERPValues.ERPCost_Identifier_SymbolLayer)
            try self.mapboxMap.style.removeSource(withId: ERPValues.ERPCost_Identifier_GeoJsonSource)

        } catch {
            SwiftyBeaver.error("Failed to remove ERP Cost layer.")
        }
        }
    }
    
    func removeERPNoCostLayer() {
        DispatchQueue.main.async {
        do {
            try self.mapboxMap.style.removeLayer(withId: ERPValues.ERPNoCost_Identifier_SymbolLayer)
            try self.mapboxMap.style.removeSource(withId: ERPValues.ERPNoCost_Identifier_GeoJsonSource)

        } catch {
            SwiftyBeaver.error("Failed to remove ERP No cost layer.")
        }
        }
    }
    
    func addERPNoCostItems(features:Turf.FeatureCollection, isBelowPuck: Bool) {
        
        do {
            try self.mapboxMap.style.addImage(UIImage(named: "erpSymbol")!, id: ERPValues.ERPNoCostImageIdentifier,stretchX: [],stretchY: [])
                        
            let iconSize = Exp(.interpolate){
                Exp(.linear)
                Exp(.zoom)
                0
                0
                9
                0.0
                12.9
                0.0
                13
                0.8
                16.5
                0.9
            }
            
            // Create a GeoJSON data source.
            var geoJSONSource = GeoJSONSource()
            geoJSONSource.data = .featureCollection(features)
            
            var symbolLayer = SymbolLayer(id: ERPValues.ERPNoCost_Identifier_SymbolLayer)
            symbolLayer.source = ERPValues.ERPNoCost_Identifier_GeoJsonSource
            symbolLayer.iconSize = .expression(iconSize)
            symbolLayer.maxZoom = 22
            symbolLayer.minZoom = 13
            symbolLayer.iconImage = .constant(ResolvedImage.name(ERPValues.ERPNoCostImageIdentifier))
            
            //symbolLayer.layout?.symbolPlacement = .lineCenter

            // Add the source and style layers to the map style.
            try self.mapboxMap.style.addSource(geoJSONSource, id: ERPValues.ERPNoCost_Identifier_GeoJsonSource)
            // rc4 - #307 remove _

            try self.mapboxMap.style.addPersistentLayer(with: symbolLayer.jsonObject(), layerPosition: .above(Constants.Layer.illegalparkingcamera))
            SwiftyBeaver.debug("addERPNoCostItems \(features.features.count)")
        } catch {
            SwiftyBeaver.error("Failed to perform operation with error: \(error.localizedDescription).")
        }
    }
    
    func addERPCostItems(features: Turf.FeatureCollection, isBelowPuck: Bool,isNavigation:Bool = false) {
        DispatchQueue.main.async {
        do {
            
            var traitCollection = UITraitCollection(userInterfaceStyle: .light) //Default will be Light
            if(Settings.shared.theme == 0){ // Dark Theme
                
                if(appDelegate().isDarkMode()){
                    
                    traitCollection = UITraitCollection(userInterfaceStyle: .dark)
                    
                }
                
            }
            else{
                
                traitCollection = UITraitCollection(userInterfaceStyle: UIUserInterfaceStyle(rawValue: Settings.shared.theme) ?? .unspecified)
            }
            
            try self.mapboxMap.style.addImage(UIImage(named: "erpwithprice", in:nil,compatibleWith: traitCollection)!, id: ERPValues.ERPCostImageIdentifier,stretchX: [],stretchY: [])
            
            // Create a GeoJSON data source.
            var geoJSONSource = GeoJSONSource()
            geoJSONSource.data = .featureCollection(features)
            
            let cost = Exp(.get){
                "cost"
            }
            
            let iconSize = Exp(.interpolate){
                Exp(.linear)
                Exp(.zoom)
                0
                0
                9
                0.0
                12.9
                0.0
                13
                0.7
                16.5
                0.9
            }
            
            let textSize = Exp(.interpolate){
                Exp(.linear)
                Exp(.zoom)
                0
                0
                9
                0
                12.9
                0
                13
                20
                16.5
                20
            }
            
            
            
            
            var symbolLayer = SymbolLayer(id: ERPValues.ERPCost_Identifier_SymbolLayer)
            symbolLayer.source = ERPValues.ERPCost_Identifier_GeoJsonSource
            symbolLayer.iconSize = .expression(iconSize)
            symbolLayer.iconImage = .constant(ResolvedImage.name(ERPValues.ERPCostImageIdentifier))
            symbolLayer.textField = .expression(cost)
            symbolLayer.textSize = .expression(textSize)
            symbolLayer.textAnchor = .constant(.center)
            symbolLayer.textOffset = .constant([0.0,0.37])
            symbolLayer.maxZoom = 22
            symbolLayer.minZoom = 13
            
            //We will use this in feature if required
            symbolLayer.symbolSortKey = .expression(Exp(.get) {"erpOrder"})
            symbolLayer.textAllowOverlap = .constant(true)
            symbolLayer.textIgnorePlacement = .constant(true)
            symbolLayer.iconAllowOverlap = .constant(true)
            symbolLayer.iconIgnorePlacement = .constant(true)
            symbolLayer.textLineHeight = .constant(19.0)
            symbolLayer.textFont = .constant([fontFamilySFProFromStudio.Medium])
            //symbolLayer.textColor = .constant(ColorRepresentable.init(color: UIColor(named: "erpPriceColor")!))
            let texColor = UIColor(named: "erpPriceColor")!.resolvedColor(with: traitCollection)

            symbolLayer.textColor = .constant(.init(texColor))
            try self.mapboxMap.style.addSource(geoJSONSource, id: ERPValues.ERPCost_Identifier_GeoJsonSource)
            // rc4 - #307 remove _

            try self.mapboxMap.style.addPersistentLayer(with: symbolLayer.jsonObject(), layerPosition: .above(Constants.Layer.illegalparkingcamera))
            SwiftyBeaver.debug("addERPCostItems \(features.features.count) isNavigation(\(isNavigation))")
        } catch {
            SwiftyBeaver.error("Failed to addERPCostItems with error: \(error.localizedDescription).")
        }
        }
    }
    
    func addAllERPBoundItemsToMap(features:Turf.FeatureCollection,isBelowPuck:Bool = false,isERPDetail:Bool = false){
        
        DispatchQueue.main.async {
        do{
            var geoJSONSource = GeoJSONSource()
            geoJSONSource.data = .featureCollection(features)
            
            let cost = Exp(.get){
                "cost"
            }
            
            var symbolLayer = SymbolLayer(id: ERPValues.ERPCost_Identifier_SymbolLayer)
            symbolLayer.iconImage = .expression(Exp(.get) {
                    "boundValue"
            })
            
            let iconSize = Exp(.interpolate){
                Exp(.linear)
                Exp(.zoom)
                0
                0
                9
                1.1
                12.9
                1.1
                13
                1.1
                16.5
                1.1
            }
            
            
            
            let textSize = Exp(.interpolate){
                Exp(.linear)
                Exp(.zoom)
                0
                0
                9
                16
                12.9
                16
                13
                16
                16.5
                16
            }
            
            let textOffset = Exp(.interpolate){
                Exp(.linear)
                Exp(.zoom)
                0
                Exp(.literal) {
                    [0.00,0.00]
                }
                9
                Exp(.literal) {
                    [0.00,0.00]
                }

                12.9
                Exp(.switchCase) {
                    Exp(.eq) {
                        Exp(.get) { "boundValue" }
                        Values.IN_ACTIVE_NO_ARROW
                    }
                    Exp(.literal) {
                        [0.75,0.05]
                    }
                    Exp(.eq) {
                        Exp(.get) { "boundValue" }
                        Values.ACTIVE_NO_ARROW
                    }
                    Exp(.literal) {
                        [0.75,0.05]
                    }
                    Exp(.literal) {
                        [0.05,0.05]
                    }
                }
                13
                Exp(.switchCase) {
                    Exp(.eq) {
                        Exp(.get) { "boundValue" }
                        Values.IN_ACTIVE_NO_ARROW
                    }
                    Exp(.literal) {
                        [0.75,0.05]
                    }
                    Exp(.eq) {
                        Exp(.get) { "boundValue" }
                        Values.ACTIVE_NO_ARROW
                    }
                    Exp(.literal) {
                        [0.75,0.05]
                    }
                    Exp(.literal) {
                        [0.05,0.05]
                    }
                }
                16
                Exp(.switchCase) {
                    Exp(.eq) {
                        Exp(.get) { "boundValue" }
                        Values.IN_ACTIVE_NO_ARROW
                    }
                    Exp(.literal) {
                        [0.75,0.05]
                    }
                    Exp(.eq) {
                        Exp(.get) { "boundValue" }
                        Values.ACTIVE_NO_ARROW
                    }
                    Exp(.literal) {
                        [0.75,0.05]
                    }
                    Exp(.literal) {
                        [0.05,0.05]
                    }
                }
                18
                Exp(.switchCase) {
                    Exp(.eq) {
                        Exp(.get) { "boundValue" }
                        Values.IN_ACTIVE_NO_ARROW
                    }
                    Exp(.literal) {
                        [0.75,0.05]
                    }
                    Exp(.eq) {
                        Exp(.get) { "boundValue" }
                        Values.ACTIVE_NO_ARROW
                    }
                    Exp(.literal) {
                        [0.75,0.05]
                    }
                    Exp(.literal) {
                        [0.05,0.05]
                    }
                }
                22
                Exp(.switchCase) {
                    Exp(.eq) {
                        Exp(.get) { "boundValue" }
                        Values.IN_ACTIVE_NO_ARROW
                    }
                    Exp(.literal) {
                        [0.75,0.05]
                    }
                    Exp(.eq) {
                        Exp(.get) { "boundValue" }
                        Values.ACTIVE_NO_ARROW
                    }
                    Exp(.literal) {
                        [0.75,0.05]
                    }
                    Exp(.literal) {
                        [0.05,0.05]
                    }
                }
            }
            
            symbolLayer.source = ERPValues.ERPCost_Identifier_GeoJsonSource
            symbolLayer.iconSize = .expression(iconSize)
            
            symbolLayer.symbolSortKey = .expression(
                Exp(.get) { "erpOrder" }
            )
            
            var traitCollection = UITraitCollection(userInterfaceStyle: .light) //Default will be Light
            if(Settings.shared.theme == 0){ // Dark Theme
                
                if(appDelegate().isDarkMode()){
                    
                    traitCollection = UITraitCollection(userInterfaceStyle: .dark)
                    
                }
                
            }
            else{
                
                traitCollection = UITraitCollection(userInterfaceStyle: UIUserInterfaceStyle(rawValue: Settings.shared.theme) ?? .unspecified)
            }
            
            symbolLayer.iconAllowOverlap = .constant(true)
            symbolLayer.textAllowOverlap = .constant(true)
            symbolLayer.textField = .expression(cost)
            symbolLayer.maxZoom = 22
            symbolLayer.minZoom = 9
            symbolLayer.textSize = .expression(textSize)
            symbolLayer.textFont = .constant([fontFamilySFProFromStudio.Regular])
            let texColor = UIColor(named: "erpPriceColor")!.resolvedColor(with: traitCollection)
            symbolLayer.textColor = .constant(.init(texColor))
            //symbolLayer.textColor = .constant(.init(UIColor(named: "erpPriceColor", in: nil, compatibleWith: traitCollection)!))
            symbolLayer.textAnchor = .constant(.center)
            symbolLayer.textOffset = .expression(textOffset)
            
            // Add the source and style layers to the map style.
            try self.mapboxMap.style.addSource(geoJSONSource, id: ERPValues.ERPCost_Identifier_GeoJsonSource)
            try self.mapboxMap.style.addPersistentLayer(with: symbolLayer.jsonObject(), layerPosition: isBelowPuck ? .below("puck") : .above(Constants.Layer.illegalparkingcamera))
        }
        catch {
            NSLog("Failed to perform operation with error: \(error.localizedDescription).")
        }
        }
     }
    
}

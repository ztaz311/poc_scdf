//
//  ERPPriceCal.swift
//  Breeze
//
//  Created by VishnuKanth on 19/05/21.
//

import Foundation
import MapboxMaps
import MapboxCoreMaps
import MapboxDirections
import Turf
import AWSAppSync
import SwiftyBeaver
import MapboxNavigation
import UIKit

protocol ERPPriceUpdateDelegate: AnyObject {
    func sendERPCost(cost:String)
    
}
//ERP Updater
final class ERPPriceCal {
    
    private var fixedBufferPointArray = [BufferPoint]()
    private var dynamicBufferPointArray = [BufferPoint]()
    private weak var mapView: MapView?
    private var isBelowPuck = false
    weak var delegate: ERPPriceUpdateDelegate?
    var response: RouteResponse?
    private var ruler:CheapRuler?
    var currentSelectedRoute:Route?
    var currentERPTime:Date?
    private var availableERPTollsOnNoNSelectRoute = [Turf.Feature]()
    var allERPInSelectedRoute = [Turf.Feature]()
    private var availableERPTollsOnSelectRoute = [Turf.Feature]()
    


    init(mapView: MapView, belowPuck: Bool,response:RouteResponse) {
        self.mapView = mapView
        self.isBelowPuck = belowPuck
        self.response = response
    }
    
    func fetchERPTolls(erpTolls:FeatureCollection,response:RouteResponse){
        
        //ruler = CheapRuler(lat: lat, units: .kilometers)
        availableERPTollsOnNoNSelectRoute.removeAll()
        availableERPTollsOnSelectRoute.removeAll()
        self.computeTolls(routes:(response.routes)!,erpTolls: erpTolls)
        
        self.addERPItemsToMap()
        
    }
    
    func addERPItemsToMapOnCarPlay(){
        
        removeLayer()
        
        if(self.allERPInSelectedRoute.count > 0){
            
            let erpItems = FeatureCollection(features: self.allERPInSelectedRoute)
            self.addAllERPItemsToMapCarPlay(features: erpItems)
        }
    }
    
    private func addAllERPItemsToMapCarPlay(features:Turf.FeatureCollection){
            
            SwiftyBeaver.debug("ERPPriceCal class Called addAllERPItemsToMap \(features)")
            do{
                try mapView?.mapboxMap.style.addImage(UIImage(named: "erp_noarrow_active")!, id: "ERPColor",stretchX: [],stretchY: [])
                
                var geoJSONSource = GeoJSONSource()
                geoJSONSource.data = .featureCollection(features)
                
                let cost = Exp(.get){
                    "cost"
                }
                
                var symbolLayer = SymbolLayer(id: "All")
                symbolLayer.source = "GeoJsonAll"
                symbolLayer.iconSize = .constant(1.1)
                symbolLayer.iconImage = .constant(ResolvedImage.name("ERPColor"))
                symbolLayer.iconAllowOverlap = .constant(true)
                symbolLayer.textField = .expression(cost)
                symbolLayer.textSize = .constant(16.0)
                symbolLayer.textFont = .constant([fontFamilySFProFromStudio.Medium])
                symbolLayer.textColor = .constant(.init(UIColor.viewTransparentBackground))
                symbolLayer.textAnchor = .constant(.center)
                symbolLayer.textAllowOverlap = .constant(true)
                symbolLayer.textOffset = .constant([0.5,0.0])
                
                // Add the source and style layers to the map style.
                try mapView?.mapboxMap.style.addSource(geoJSONSource, id: "GeoJsonAll")
                try mapView?.mapboxMap.style.addLayer(symbolLayer, layerPosition: .below("puck"))
            }
            catch {
                SwiftyBeaver.error("ERPPriceCal class Called addAllERPItemsToMap Failed \(error.localizedDescription)")
                //NSLog("Failed to perform operation with error: \(error.localizedDescription).")
            }
    }
    
    func addERPItemsToMap(){
        
        removeLayer()
                
        if(self.allERPInSelectedRoute.count > 0)
        {

            let erpItems = FeatureCollection(features: self.allERPInSelectedRoute)
            self.addAllERPItemsToMap(features: erpItems)
        }
        
        if(self.availableERPTollsOnNoNSelectRoute.count > 0){
            
            // In certain scenario the same erp also added to the alternative routes, so we have to remove the duplicated one
            var filteredFeature = [Turf.Feature]()
            for feature in availableERPTollsOnNoNSelectRoute {
                if case let .string(id) = feature.properties?["erpid"] {
                    // check if its id is in allERPInSelectedRoute already?
                    let duplicated = allERPInSelectedRoute.contains { item in
                        if case let .string(erpId) = item.properties?["erpid"] {
                            return id == erpId
                        }
                        return false
                    }
                    if !duplicated {
                        filteredFeature.append(feature)
                    }
                }
            }
            
            let alternateRouteERPItems = FeatureCollection(features: filteredFeature)
            self.addAlternateRouteERPItemsToMap(features: alternateRouteERPItems,isBelowPuck:self.allERPInSelectedRoute.count > 0 ? false : true)
        }

        // To hide the summary bubble for ERP. Don't delete. It maybe used in the future.
//        if(self.availableERPTollsOnSelectRoute.count > 0)
//        {
//
//            let erpSelectRouteFeatureCollection = FeatureCollection(features: self.availableERPTollsOnSelectRoute)
//            self.addERPOnSelectRouteItemsToMap(features: erpSelectRouteFeatureCollection)
//        }
//
//        if(self.availableERPTollsOnNoNSelectRoute.count > 0)
//        {
//
//            let erpNonSelectRouteFeatureCollection = FeatureCollection(features: self.availableERPTollsOnNoNSelectRoute)
//            self.addERPNonSelectRouteItemsToMap(features: erpNonSelectRouteFeatureCollection)
//        }
    }
    
    private func addAlternateRouteERPItemsToMap(features:Turf.FeatureCollection,isBelowPuck:Bool=true){
        
        do{
            try mapView?.mapboxMap.style.addImage(UIImage(named: "erpSymbol")!, id: "alternateERP",stretchX: [],stretchY: [])
            
            var geoJSONSource = GeoJSONSource()
            geoJSONSource.data = .featureCollection(features)
            
            var symbolLayer = SymbolLayer(id: "AllAlternateERP")
            symbolLayer.symbolSortKey = .expression(
                Exp(.get) { "erpOrder" }
            )

            symbolLayer.source = "GeoJsonAllAlternateERP"
            symbolLayer.iconSize = .constant(0.3)
            symbolLayer.iconImage = .constant(ResolvedImage.name("alternateERP"))
            symbolLayer.iconAllowOverlap = .constant(true)
            
            // Add the source and style layers to the map style.
            try mapView?.mapboxMap.style.addSource(geoJSONSource, id: "GeoJsonAllAlternateERP")
            try mapView?.mapboxMap.style.addLayer(symbolLayer, layerPosition: .below(isBelowPuck ?  "puck" : "All"))
        }
        catch {
            NSLog("Failed to perform operation with error: \(error.localizedDescription).")
        }
}
    
    private func addAllERPItemsToMap(features:Turf.FeatureCollection){
        
        do{
            var geoJSONSource = GeoJSONSource()
            geoJSONSource.data = .featureCollection(features)
            
            let cost = Exp(.get){
                "cost"
            }
            
            let textOffset = Exp(.interpolate){
                Exp(.linear)
                Exp(.zoom)
                0
                Exp(.literal) {
                    [0.00,0.00]
                }
                9
                Exp(.literal) {
                    [0.00,0.00]
                }

                12.9
                Exp(.switchCase) {
                    Exp(.eq) {
                        Exp(.get) { "boundValue" }
                        Values.IN_ACTIVE_NO_ARROW
                    }
                    Exp(.literal) {
                        [0.75,0.05]
                    }
                    Exp(.eq) {
                        Exp(.get) { "boundValue" }
                        Values.ACTIVE_NO_ARROW
                    }
                    Exp(.literal) {
                        [0.79,0.05]
                    }
                    Exp(.literal) {
                        [0.05,0.05]
                    }
                }
                13
                Exp(.switchCase) {
                    Exp(.eq) {
                        Exp(.get) { "boundValue" }
                        Values.IN_ACTIVE_NO_ARROW
                    }
                    Exp(.literal) {
                        [0.75,0.05]
                    }
                    Exp(.eq) {
                        Exp(.get) { "boundValue" }
                        Values.ACTIVE_NO_ARROW
                    }
                    Exp(.literal) {
                        [0.79,0.05]
                    }
                    Exp(.literal) {
                        [0.05,0.05]
                    }
                }
                16
                Exp(.switchCase) {
                    Exp(.eq) {
                        Exp(.get) { "boundValue" }
                        Values.IN_ACTIVE_NO_ARROW
                    }
                    Exp(.literal) {
                        [0.75,0.05]
                    }
                    Exp(.eq) {
                        Exp(.get) { "boundValue" }
                        Values.ACTIVE_NO_ARROW
                    }
                    Exp(.literal) {
                        [0.79,0.05]
                    }
                    Exp(.literal) {
                        [0.05,0.05]
                    }
                }
                18
                Exp(.switchCase) {
                    Exp(.eq) {
                        Exp(.get) { "boundValue" }
                        Values.IN_ACTIVE_NO_ARROW
                    }
                    Exp(.literal) {
                        [0.75,0.05]
                    }
                    Exp(.eq) {
                        Exp(.get) { "boundValue" }
                        Values.ACTIVE_NO_ARROW
                    }
                    Exp(.literal) {
                        [0.79,0.05]
                    }
                    Exp(.literal) {
                        [0.05,0.05]
                    }
                }
                22
                Exp(.switchCase) {
                    Exp(.eq) {
                        Exp(.get) { "boundValue" }
                        Values.IN_ACTIVE_NO_ARROW
                    }
                    Exp(.literal) {
                        [0.75,0.05]
                    }
                    Exp(.eq) {
                        Exp(.get) { "boundValue" }
                        Values.ACTIVE_NO_ARROW
                    }
                    Exp(.literal) {
                        [0.79,0.05]
                    }
                    Exp(.literal) {
                        [0.05,0.05]
                    }
                }
            }
            
            var symbolLayer = SymbolLayer(id: "All")
            symbolLayer.iconImage = .expression(Exp(.get) {
                    "boundValue"
            })
            
            var traitCollection = UITraitCollection(userInterfaceStyle: .light)
            if(Settings.shared.theme == 0){
                
                if(appDelegate().isDarkMode()){
                    
                    traitCollection = UITraitCollection(userInterfaceStyle: .dark)
                    
                }
                
            }
            else{
                
                traitCollection = UITraitCollection(userInterfaceStyle: UIUserInterfaceStyle(rawValue: Settings.shared.theme) ?? .unspecified)
            }
            
            symbolLayer.source = "GeoJsonAll"
            symbolLayer.symbolSortKey = .expression(
                Exp(.get) { "erpOrder" }
            )
            symbolLayer.iconSize = .constant(1.1)
            symbolLayer.iconAllowOverlap = .constant(true)
            symbolLayer.textField = .expression(cost)
            symbolLayer.textSize = .constant(16.0)
            symbolLayer.textFont = .constant([fontFamilySFProFromStudio.Regular])
            let texColor = UIColor(named: "erpPriceColor")!.resolvedColor(with: traitCollection)
            symbolLayer.textColor = .constant(.init(texColor))
            symbolLayer.textAnchor = .constant(.center)
            symbolLayer.textAllowOverlap = .constant(true)
            symbolLayer.textOffset = .expression(textOffset)
            
            // Add the source and style layers to the map style.
            try mapView?.mapboxMap.style.addSource(geoJSONSource, id: "GeoJsonAll")
            try mapView?.mapboxMap.style.addLayer(symbolLayer, layerPosition: .below("puck"))
        }
        catch {
            NSLog("Failed to perform operation with error: \(error.localizedDescription).")
        }
}
    
    private  func addERPOnSelectRouteItemsToMap(features:Turf.FeatureCollection){
        
        do{
            try mapView?.mapboxMap.style.addImage(UIImage(named: "ERP_blue_down")!, id: "ERPCenterBlue",stretchX: [],stretchY: [])
        
            var geoJSONSource = GeoJSONSource()
            geoJSONSource.data = .featureCollection(features)
            
            let cost = Exp(.get){
                "cost"
            }
            
            var symbolLayer = SymbolLayer(id: "TollAlongRouteSelect")
            symbolLayer.source = "GeoJsonTollRouteSelect"
            symbolLayer.iconSize = .constant(1.2)
            symbolLayer.iconImage = .constant(ResolvedImage.name("ERPCenterBlue"))
            symbolLayer.iconAllowOverlap = .constant(true)
            symbolLayer.textField = .expression(cost)
            symbolLayer.textSize = .constant(16.0)
            symbolLayer.textFont = .constant([fontFamilySFProFromStudio.Bold])
            symbolLayer.textColor = .constant(.init(.white))
            symbolLayer.iconAnchor = .constant(.right)
            symbolLayer.textAnchor = .constant(.right)
            symbolLayer.symbolZOrder = .constant(SymbolZOrder.auto)
            symbolLayer.textAllowOverlap = .constant(true)
            symbolLayer.textOffset = .constant([-1.7,-0.2])
            
            // Add the source and style layers to the map style.
            try mapView?.mapboxMap.style.addSource(geoJSONSource, id: "GeoJsonTollRouteSelect")
            try mapView?.mapboxMap.style.addLayer(symbolLayer, layerPosition: .below("puck"))
        }
        catch {
            NSLog("Failed to perform operation with error: \(error.localizedDescription).")
        }

        
    }
    
    private func addERPNonSelectRouteItemsToMap(features:Turf.FeatureCollection)
    {
    
        //beta.9 change need to add throw call back for error handling
        do{
            try mapView?.mapboxMap.style.addImage(UIImage(named: "ERP_white_down")!, id: "ERPGrey",stretchX: [],stretchY: [])
        
            var geoJSONSource = GeoJSONSource()
            geoJSONSource.data = .featureCollection(features)
            
            let cost = Exp(.get){
                "cost"
            }
            
            var symbolLayer = SymbolLayer(id: "TollAlongRoute")
            symbolLayer.source = "GeoJsonToll"
            symbolLayer.iconSize = .constant(1.2)
            symbolLayer.iconImage = .constant(ResolvedImage.name("ERPGrey"))
            
            symbolLayer.iconAllowOverlap = .constant(true)
            symbolLayer.textField = .expression(cost)
            symbolLayer.textSize = .constant(16.0)
            symbolLayer.textFont = .constant([fontFamilySFProFromStudio.Bold])
            symbolLayer.textColor = .constant(.init(UIColor.routePlanningNonSelectedRouteTitleColor))
            symbolLayer.iconAnchor = .constant(.right)
            symbolLayer.textAnchor = .constant(.right)
            
            symbolLayer.symbolZOrder = .constant(SymbolZOrder.auto)
            symbolLayer.textAllowOverlap = .constant(true)
            symbolLayer.textOffset = .constant([-1.7,-0.2])
            
            try mapView?.mapboxMap.style.addSource(geoJSONSource, id: "GeoJsonToll")
            try mapView?.mapboxMap.style.addLayer(symbolLayer, layerPosition: .below("puck"))
        //_ = navMapView.mapView.style.addLayer(layer: lineLayer, layerPosition: nil)
        }
        catch {
            NSLog("Failed to perform operation with error: \(error.localizedDescription).")
        }
    }
    
    public func removeLayer(){
        
        mapView?.removeRoutePlanningERPRouteLayer()
        mapView?.removeRoutePlanningERPRouteAlternateLayer()
        //beta.9 change
//        do {
////            try mapView.mapboxMap.style.removeLayer(withId: "TollAlongRoute")
////            try mapView.mapboxMap.style.removeSource(withId: "GeoJsonToll")
////
////            try mapView.mapboxMap.style.removeLayer(withId: "TollAlongRouteSelect")
////            try mapView.mapboxMap.style.removeSource(withId: "GeoJsonTollRouteSelect")
//
//            try mapView.mapboxMap.style.removeLayer(withId: "All")
//            try mapView.mapboxMap.style.removeSource(withId: "GeoJsonAll")
//
//            try mapView.mapboxMap.style.removeLayer(withId: "AllAlternateERP")
//            try mapView.mapboxMap.style.removeSource(withId: "GeoJsonAllAlternateERP")
//
//        } catch {
//            NSLog("Failed to remove main Layers.")
//        }
            
       /* _ = navView.mapView.style.removeStyleLayer(forLayerId: "TollAlongRoute")
       _ = navView.mapView.style.removeSource(for: "GeoJsonToll")
        
        _ = navView.mapView.style.removeStyleLayer(forLayerId: "TollAlongRouteSelect")
       _ = navView.mapView.style.removeSource(for: "GeoJsonTollRouteSelect")
        
        _ = navView.mapView.style.removeStyleLayer(forLayerId: "All")
       _ = navView.mapView.style.removeSource(for: "GeoJsonAll")*/
            
    }
    
    /* By default Directions API returns in fastest Route Order, so we don't any other logic for this*/
    /* func fastestRoute() -> Route
    {
        
        let responses  = response?.routes.sort{$0.route.expectedTravelTime < $1.route.expectedTravelTime}
        
        let routes = response?.routes
        var selectedRoute:Route?
        
        if(routes!.count > 1)
        {
            var routeTimes = [Any]()
            for route in routes ?? [] {
                
                routeTimes.append(route.expectedTravelTime)
               
            }
            
            
        }
        else
        {
            selectedRoute = routes![0]
        }
        
        return selectedRoute!
    }*/
    
    func checkIfAllRoutesAreEqual(fastestResponse:RouteResponse,shortestResponse:RouteResponse,cheapestResponse:RouteResponse) -> Bool
    {
        
        if((fastestResponse.routes![0] == shortestResponse.routes![0] && fastestResponse.routes![0] == cheapestResponse.routes![0] && shortestResponse.routes![0] == cheapestResponse.routes![0])&&(fastestResponse.routes! == cheapestResponse.routes!)){
            return true
        }
//        else
//        {
//            if(fastestResponse.routes! == cheapestResponse.routes!){
//                return true
//            }
//        }
        
        return false
    }
    

    func getRoutePreferenceGrouping(erpList:[RouteERPList],resultCount:Int,response:RouteResponse,fastDetected:Bool,cheapestDetected:Bool,shortestDetected:Bool)->RoutePreferenceData?{
                
                var routePreference =  [PreferenceRoute]()
                
                if(resultCount == 3){
                    
                    if(erpList.count == 3){
                        
                        if(erpList[0].routeERPRate == 0 && erpList[1].routeERPRate == 0 && erpList[2].routeERPRate == 0){
                            
                            if(response.routes![0].expectedTravelTime <= response.routes![1].expectedTravelTime && response.routes![0].expectedTravelTime <= response.routes![2].expectedTravelTime && response.routes![0].distance <= response.routes![1].distance && response.routes![0].distance <= response.routes![2].distance){
                                
                                let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:Constants.FASTEST_SHORTEST)
                                   routePreference.append(routeFastType)
                                
                                let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:"")
                                   routePreference.append(routeShortType)
                                
                                let routeCheapType = PreferenceRoute(routeId: 2, routePreferenceType:"")
                                   routePreference.append(routeCheapType)
                            }
                            else {
                                
                                if(response.routes![1].expectedTravelTime <= response.routes![0].expectedTravelTime && response.routes![1].expectedTravelTime <= response.routes![2].expectedTravelTime && response.routes![1].distance <= response.routes![0].distance && response.routes![1].distance <= response.routes![2].distance){
                                    
                                    let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:"")
                                       routePreference.append(routeFastType)
                                    
                                    let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:Constants.FASTEST_SHORTEST)
                                       routePreference.append(routeShortType)
                                    
                                    let routeCheapType = PreferenceRoute(routeId: 2, routePreferenceType:"")
                                       routePreference.append(routeCheapType)
                                }
                                else{
                                    
                                    if(response.routes![2].expectedTravelTime <= response.routes![0].expectedTravelTime && response.routes![2].expectedTravelTime <= response.routes![1].expectedTravelTime && response.routes![2].distance <= response.routes![0].distance && response.routes![2].distance <= response.routes![1].distance){
                                        
                                        let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:"")
                                           routePreference.append(routeFastType)
                                        
                                        let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:"")
                                           routePreference.append(routeShortType)
                                        
                                        let routeCheapType = PreferenceRoute(routeId: 2, routePreferenceType:Constants.FASTEST_SHORTEST)
                                           routePreference.append(routeCheapType)
                                    }
                                    else{
                                        
                                        let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:Constants.FASTEST)
                                           routePreference.append(routeFastType)
                                        
                                        if(response.routes![1].distance <= response.routes![2].distance){
                                            
                                            let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:Constants.SHORTEST)
                                               routePreference.append(routeShortType)
                                            
                                            let routeCheapType = PreferenceRoute(routeId: 2, routePreferenceType:"")
                                               routePreference.append(routeCheapType)
                                        }
                                        else{
                                            
                                            let routeShortType = PreferenceRoute(routeId: 2, routePreferenceType:Constants.SHORTEST)
                                               routePreference.append(routeShortType)
                                            
                                            let routeCheapType = PreferenceRoute(routeId: 1, routePreferenceType:"")
                                               routePreference.append(routeCheapType)
                                            
                                        }
                                            
                                            
                                        }
                                    }
                                }
                            }
                        
                        else{
                            
                            if(erpList[0].routeERPRate != 0 && erpList[1].routeERPRate != 0 && erpList[2].routeERPRate != 0){
                                
                                if(erpList[0].routeERPRate >= erpList[1].routeERPRate && erpList[0].routeERPRate >= erpList[2].routeERPRate){
                                    
                                    if(response.routes![0].distance <= response.routes![1].distance && response.routes![0].distance <= response.routes![2].distance){
                                        
                                        let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:Constants.FASTEST_SHORTEST)
                                           routePreference.append(routeFastType)
                                        
                                        if(erpList[1].routeERPRate <= erpList[2].routeERPRate){
                                            
                                            let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:Constants.CHEAPEST)
                                            routePreference.append(routeShortType)
                                            
                                            let routeCheapType = PreferenceRoute(routeId: 2, routePreferenceType:"")
                                            routePreference.append(routeCheapType)
                                        }
                                        else{
                                            
                                            let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:Constants.SHORTEST)
                                            routePreference.append(routeShortType)
                                            
                                            let routeCheapType = PreferenceRoute(routeId: 2, routePreferenceType:Constants.CHEAPEST)
                                            routePreference.append(routeCheapType)
                                        }
                                    }
                                    else{
                                        
                                        let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:Constants.FASTEST)
                                           routePreference.append(routeFastType)
                                        
                                        if(erpList[1].routeERPRate < erpList[2].routeERPRate){
                                            
                                            let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:Constants.SHORTEST_CHEAPEST)
                                            routePreference.append(routeShortType)
                                            
                                            let routeCheapType = PreferenceRoute(routeId: 2, routePreferenceType:"")
                                            routePreference.append(routeCheapType)
                                        }
                                        else{
                                            
                                            if(response.routes![1].distance <= response.routes![0].distance && response.routes![1].distance <= response.routes![2].distance){
                                                
                                                let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:Constants.SHORTEST)
                                                routePreference.append(routeShortType)
                                                
                                                let routeCheapType = PreferenceRoute(routeId: 2, routePreferenceType:"")
                                                routePreference.append(routeCheapType)
                                            }
                                            else{
                                                
                                                let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:"")
                                                routePreference.append(routeShortType)
                                                
                                                let routeCheapType = PreferenceRoute(routeId: 2, routePreferenceType:Constants.SHORTEST)
                                                routePreference.append(routeCheapType)
                                            }
                                            
                                        }
                                    }
                                    
                                    
                                }
                                else{
                                    
                                    if(erpList[1].routeERPRate >= erpList[0].routeERPRate && erpList[1].routeERPRate >= erpList[2].routeERPRate){
                                        
                                        if(erpList[0].routeERPRate <= erpList[2].routeERPRate){
                                            
                                            let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:Constants.FASTEST_CHEAPEST)
                                               routePreference.append(routeFastType)
                                            
                                            let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:Constants.SHORTEST)
                                               routePreference.append(routeShortType)
                                            
                                            let routeCheapType = PreferenceRoute(routeId: 2, routePreferenceType:"")
                                               routePreference.append(routeCheapType)
                                        }
                                        else {
                                            
                                            let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:Constants.FASTEST)
                                               routePreference.append(routeFastType)
                                            
                                            let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:Constants.SHORTEST)
                                               routePreference.append(routeShortType)
                                            
                                            let routeCheapType = PreferenceRoute(routeId: 2, routePreferenceType:Constants.CHEAPEST)
                                               routePreference.append(routeCheapType)
                                        }
                                    }
                                    else{
                                        
                                        if(erpList[0].routeERPRate <= erpList[1].routeERPRate){
                                            
                                            let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:Constants.FASTEST_CHEAPEST)
                                               routePreference.append(routeFastType)
                                            
                                            let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:Constants.SHORTEST)
                                               routePreference.append(routeShortType)
                                            
                                            let routeCheapType = PreferenceRoute(routeId: 2, routePreferenceType:"")
                                               routePreference.append(routeCheapType)
                                        }
                                        else{
                                            
                                            let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:Constants.FASTEST)
                                               routePreference.append(routeFastType)
                                            
                                            let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:Constants.SHORTEST_CHEAPEST)
                                               routePreference.append(routeShortType)
                                            
                                            let routeCheapType = PreferenceRoute(routeId: 2, routePreferenceType:"")
                                               routePreference.append(routeCheapType)
                                        }
                                    }
                                }
                            }
                            else {
                                
                                if(erpList[0].routeERPRate != 0 && erpList[1].routeERPRate != 0){
                                    
                                    if(erpList[0].routeERPRate <= erpList[1].routeERPRate && erpList[0].routeERPRate <= erpList[2].routeERPRate){
                                        
                                        let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:Constants.FASTEST_CHEAPEST)
                                           routePreference.append(routeFastType)
                                        
                                        let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:Constants.SHORTEST)
                                           routePreference.append(routeShortType)
                                        
                                        let routeCheapType = PreferenceRoute(routeId: 2, routePreferenceType:"")
                                           routePreference.append(routeCheapType)
                                    }
                                    else{
                                        
                                        if(erpList[1].routeERPRate <= erpList[2].routeERPRate && erpList[1].routeERPRate <= erpList[0].routeERPRate){
                                            
                                            let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:Constants.FASTEST)
                                               routePreference.append(routeFastType)
                                            
                                            let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:Constants.SHORTEST_CHEAPEST)
                                               routePreference.append(routeShortType)
                                            
                                            let routeCheapType = PreferenceRoute(routeId: 2, routePreferenceType:"")
                                               routePreference.append(routeCheapType)
                                        }
                                        else{
                                            
                                            if(response.routes![0].distance <= response.routes![1].distance){
                                                
                                                let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:Constants.FASTEST)
                                                   routePreference.append(routeFastType)
                                                
                                                if(response.routes![1].distance <= response.routes![2].distance){
                                                    
                                                    let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:Constants.SHORTEST)
                                                       routePreference.append(routeShortType)
                                                    
                                                    let routeCheapType = PreferenceRoute(routeId: 2, routePreferenceType:Constants.CHEAPEST)
                                                       routePreference.append(routeCheapType)
                                                }
                                                else{
                                                    
                                                    let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:"")
                                                       routePreference.append(routeShortType)
                                                    
                                                    let routeCheapType = PreferenceRoute(routeId: 2, routePreferenceType:Constants.SHORTEST_CHEAPEST)
                                                       routePreference.append(routeCheapType)
                                                }
                                                
                                            }
                                            else{
                                                
                                                let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:Constants.FASTEST_SHORTEST)
                                                   routePreference.append(routeFastType)
                                                
                                                let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:"")
                                                   routePreference.append(routeShortType)
                                                
                                                let routeCheapType = PreferenceRoute(routeId: 2, routePreferenceType:Constants.CHEAPEST)
                                                   routePreference.append(routeCheapType)
                                            }
                                            
                                        }
                                    }
                                }
                                else{
                                    
                                    if(erpList[1].routeERPRate != 0 && erpList[2].routeERPRate != 0){
                                        
                                        if(erpList[1].routeERPRate <= erpList[0].routeERPRate && erpList[1].routeERPRate <= erpList[2].routeERPRate){
                                            
                                            let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:Constants.FASTEST)
                                               routePreference.append(routeFastType)
                                            
                                            let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:Constants.SHORTEST_CHEAPEST)
                                               routePreference.append(routeShortType)
                                            
                                            let routeCheapType = PreferenceRoute(routeId: 2, routePreferenceType:"")
                                               routePreference.append(routeCheapType)
                                        }
                                        else {
                                            
                                            if(erpList[2].routeERPRate <= erpList[0].routeERPRate && erpList[2].routeERPRate <= erpList[1].routeERPRate){
                                                
                                                let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:Constants.FASTEST)
                                                   routePreference.append(routeFastType)
                                                
                                                let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:Constants.SHORTEST)
                                                   routePreference.append(routeShortType)
                                                
                                                let routeCheapType = PreferenceRoute(routeId: 2, routePreferenceType:Constants.CHEAPEST)
                                                   routePreference.append(routeCheapType)
                                            }
                                        }
                                    }
                                    else{
                                        
                                        if(erpList[0].routeERPRate != 0 && erpList[1].routeERPRate == 0 && erpList[2].routeERPRate == 0 ){
                                            
                                            let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:Constants.FASTEST)
                                               routePreference.append(routeFastType)
                                            
                                            let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:Constants.SHORTEST_CHEAPEST)
                                               routePreference.append(routeShortType)
                                            
                                            let routeCheapType = PreferenceRoute(routeId: 2, routePreferenceType:"")
                                               routePreference.append(routeCheapType)
                                        }
                                        else{
                                            
                                            if(erpList[0].routeERPRate == 0 && erpList[1].routeERPRate != 0 && erpList[2].routeERPRate == 0){
                                                
                                                let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:Constants.FASTEST_CHEAPEST)
                                                   routePreference.append(routeFastType)
                                                
                                                let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:Constants.SHORTEST)
                                                   routePreference.append(routeShortType)
                                                
                                                let routeCheapType = PreferenceRoute(routeId: 2, routePreferenceType:"")
                                                   routePreference.append(routeCheapType)
                                            }
                                            else{
                                                
                                                if(erpList[0].routeERPRate != 0 && erpList[1].routeERPRate == 0 && erpList[2].routeERPRate != 0){
                                                    
                                                    if(erpList[0].routeERPRate >= erpList[2].routeERPRate){
                                                        
                                                        if(response.routes![0].distance <= response.routes![1].distance && response.routes![0].distance <= response.routes![2].distance){
                                                            
                                                            let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:Constants.FASTEST_SHORTEST)
                                                               routePreference.append(routeFastType)
                                                            
                                                            if(cheapestDetected){
                                                                
                                                                let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:Constants.CHEAPEST)
                                                                   routePreference.append(routeShortType)
                                                                
                                                                let routeCheapType = PreferenceRoute(routeId: 2, routePreferenceType:"")
                                                                   routePreference.append(routeCheapType)
                                                            }
                                                            else{
                                                                
                                                                let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:Constants.CHEAPEST)
                                                                   routePreference.append(routeShortType)
                                                                
                                                                let routeCheapType = PreferenceRoute(routeId: 2, routePreferenceType:"")
                                                                   routePreference.append(routeCheapType)
                                                            }
                                                        }
                                                        else{
                                                            
                                                            if(response.routes![1].distance <= response.routes![0].distance && response.routes![1].distance <= response.routes![2].distance){
                                                                
                                                                let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:Constants.FASTEST)
                                                                   routePreference.append(routeFastType)
                                                                
                                                                if(cheapestDetected){
                                                                    
                                                                    let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:Constants.SHORTEST_CHEAPEST)
                                                                       routePreference.append(routeShortType)
                                                                    
                                                                    let routeCheapType = PreferenceRoute(routeId: 2, routePreferenceType:"")
                                                                       routePreference.append(routeCheapType)
                                                                }
                                                                else{
                                                                    
                                                                    let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:Constants.SHORTEST_CHEAPEST)
                                                                       routePreference.append(routeShortType)
                                                                    
                                                                    let routeCheapType = PreferenceRoute(routeId: 2, routePreferenceType:"")
                                                                       routePreference.append(routeCheapType)
                                                                }
                                                            }
                                                            else{
                                                                
                                                                let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:Constants.FASTEST)
                                                                   routePreference.append(routeFastType)
                                                                
                                                                if(cheapestDetected){
                                                                    
                                                                    let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:Constants.SHORTEST_CHEAPEST)
                                                                       routePreference.append(routeShortType)
                                                                    
                                                                    let routeCheapType = PreferenceRoute(routeId: 2, routePreferenceType:"")
                                                                       routePreference.append(routeCheapType)
                                                                }
                                                                else{
                                                                    
                                                                    let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:Constants.CHEAPEST)
                                                                       routePreference.append(routeShortType)
                                                                    
                                                                    let routeCheapType = PreferenceRoute(routeId: 2, routePreferenceType:"")
                                                                       routePreference.append(routeCheapType)
                                                                }
                                                            }
                                                        }
                                                        
                                                    }
                                                    
                                                    else{
                                                        
                                                        let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:Constants.FASTEST_CHEAPEST)
                                                           routePreference.append(routeFastType)
                                                        
                                                        let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:Constants.SHORTEST)
                                                           routePreference.append(routeShortType)
                                                        
                                                        let routeCheapType = PreferenceRoute(routeId: 2, routePreferenceType:"")
                                                           routePreference.append(routeCheapType)
                                                    }
                                                }
                                                else{
                                                    
                                                    let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:Constants.FASTEST_CHEAPEST)
                                                       routePreference.append(routeFastType)
                                                    
                                                    let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:Constants.SHORTEST)
                                                       routePreference.append(routeShortType)
                                                    
                                                    let routeCheapType = PreferenceRoute(routeId: 2, routePreferenceType:"")
                                                       routePreference.append(routeCheapType)
                                                }
                                                
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        
                    }
                    
                    else
                    {
                        if(erpList.count == 2){
                            
                            if(erpList[0].routeERPRate == 0 && erpList[1].routeERPRate == 0){
                                
                                if(response.routes![0].expectedTravelTime <= response.routes![1].expectedTravelTime && response.routes![0].expectedTravelTime <= response.routes![2].expectedTravelTime && response.routes![0].distance <= response.routes![1].distance && response.routes![0].distance <= response.routes![2].distance){
                                    
                                    let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:Constants.FASTEST_SHORTEST)
                                       routePreference.append(routeFastType)
                                    
                                    let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:"")
                                       routePreference.append(routeShortType)
                                    
                                    let routeCheapType = PreferenceRoute(routeId: 2, routePreferenceType:"")
                                       routePreference.append(routeCheapType)
                                }
                                else {
                                    
                                    if(response.routes![1].expectedTravelTime <= response.routes![0].expectedTravelTime && response.routes![1].expectedTravelTime <= response.routes![2].expectedTravelTime && response.routes![1].distance <= response.routes![0].distance && response.routes![1].distance <= response.routes![2].distance){
                                        
                                        let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:"")
                                           routePreference.append(routeFastType)
                                        
                                        let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:Constants.FASTEST_SHORTEST)
                                           routePreference.append(routeShortType)
                                        
                                        let routeCheapType = PreferenceRoute(routeId: 2, routePreferenceType:"")
                                           routePreference.append(routeCheapType)
                                    }
                                    else{
                                        
                                        if(response.routes![2].expectedTravelTime <= response.routes![0].expectedTravelTime && response.routes![2].expectedTravelTime <= response.routes![1].expectedTravelTime && response.routes![2].distance <= response.routes![0].distance && response.routes![2].distance <= response.routes![1].distance){
                                            
                                            let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:"")
                                               routePreference.append(routeFastType)
                                            
                                            let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:"")
                                               routePreference.append(routeShortType)
                                            
                                            let routeCheapType = PreferenceRoute(routeId: 2, routePreferenceType:Constants.FASTEST_SHORTEST)
                                               routePreference.append(routeCheapType)
                                        }
                                        else{
                                            
                                            if(response.routes![0].distance <= response.routes![1].distance && response.routes![0].distance <=  response.routes![2].distance){
                                                
                                                let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:Constants.FASTEST_SHORTEST)
                                                   routePreference.append(routeFastType)
                                                
                                                let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:"")
                                                   routePreference.append(routeShortType)
                                                
                                                let routeCheapType = PreferenceRoute(routeId: 2, routePreferenceType:"")
                                                   routePreference.append(routeCheapType)
                                            }
                                            else{
                                                
                                                if(response.routes![1].distance <= response.routes![0].distance && response.routes![1].distance <=  response.routes![2].distance){
                                                    
                                                    let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:Constants.FASTEST)
                                                       routePreference.append(routeFastType)
                                                    
                                                    let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:Constants.SHORTEST)
                                                       routePreference.append(routeShortType)
                                                    
                                                    let routeCheapType = PreferenceRoute(routeId: 2, routePreferenceType:"")
                                                       routePreference.append(routeCheapType)
                                                }
                                                else{
                                                    
                                                    if(response.routes![2].distance <= response.routes![0].distance && response.routes![2].distance <=  response.routes![0].distance){
                                                        
                                                        let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:Constants.FASTEST)
                                                           routePreference.append(routeFastType)
                                                        
                                                        let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:"")
                                                           routePreference.append(routeShortType)
                                                        
                                                        let routeCheapType = PreferenceRoute(routeId: 2, routePreferenceType:Constants.SHORTEST)
                                                           routePreference.append(routeCheapType)
                                                    }
                                                }
                                            }
                                            
                                            
                                            
                                
                                            }
                                        }
                                    }
                            }
                            else{
                                
                                if(erpList[0].routeERPRate != 0 && erpList[1].routeERPRate != 0){
                                    
                                    if( response.routes![0].distance <= response.routes![1].distance && response.routes![0].distance <= response.routes![2].distance){
                                        
                                        if(erpList[0].routeERPRate >= erpList[1].routeERPRate){
                                            
                                            let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:Constants.FASTEST_SHORTEST)
                                            routePreference.append(routeFastType)
                                            
                                            if(cheapestDetected){
                                                
                                                
                                                let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:"")
                                                routePreference.append(routeShortType)
                                                
                                                let routeCheapType = PreferenceRoute(routeId: 2, routePreferenceType:Constants.CHEAPEST)
                                                routePreference.append(routeCheapType)
                                                
                                            }
                                            else{
                                                
                                                let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:Constants.CHEAPEST)
                                                routePreference.append(routeShortType)
                                                
                                                let routeCheapType = PreferenceRoute(routeId: 2, routePreferenceType:"")
                                                routePreference.append(routeCheapType)
                                            }
                                            
                                            
                                            
                                        }
                                        else{
                                            
                                            if(cheapestDetected){
                                                
                                                if(response.routes![1].expectedTravelTime <= response.routes![2].expectedTravelTime){
                                                    
                                                    let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:Constants.FASTEST)
                                                    routePreference.append(routeFastType)
                                                    
                                                    let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:Constants.SHORTEST)
                                                    routePreference.append(routeShortType)
                                                    
                                                    let routeCheapType = PreferenceRoute(routeId: 2, routePreferenceType:Constants.CHEAPEST)
                                                    routePreference.append(routeCheapType)
                                                }
                                                else{
                                                    
                                                    let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:Constants.FASTEST)
                                                    routePreference.append(routeFastType)
                                                    
                                                    let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:Constants.SHORTEST)
                                                    routePreference.append(routeShortType)
                                                    
                                                    let routeCheapType = PreferenceRoute(routeId: 2, routePreferenceType:Constants.CHEAPEST)
                                                    routePreference.append(routeCheapType)
                                                }
                                                
                                                
                                            }
                                            else{
                                                
                                                let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:Constants.FASTEST_CHEAPEST)
                                                routePreference.append(routeFastType)
                                                
                                                let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:Constants.SHORTEST)
                                                routePreference.append(routeShortType)
                                                
                                                let routeCheapType = PreferenceRoute(routeId: 2, routePreferenceType:"")
                                                routePreference.append(routeCheapType)
                                            }
                                        }
                                        
                                    }
                                    else{
                                        
                                        if( response.routes![1].distance <= response.routes![0].distance && response.routes![1].distance <= response.routes![2].distance){
                                            
                                            
                                            if(erpList[0].routeERPRate >= erpList[1].routeERPRate){
                                                
                                                let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:Constants.FASTEST)
                                                routePreference.append(routeFastType)
                                                
                                                if(cheapestDetected){
                                                    
                                                    
                                                    if(response.routes![1].expectedTravelTime <= response.routes![2].expectedTravelTime){
                                                        
                                                        let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:Constants.SHORTEST)
                                                        routePreference.append(routeShortType)
                                                        
                                                        let routeCheapType = PreferenceRoute(routeId: 2, routePreferenceType:Constants.CHEAPEST)
                                                        routePreference.append(routeCheapType)
                                                    }
                                                    else{
                                                        
                                                        let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:Constants.SHORTEST)
                                                        routePreference.append(routeShortType)
                                                        
                                                        let routeCheapType = PreferenceRoute(routeId: 2, routePreferenceType:Constants.CHEAPEST)
                                                        routePreference.append(routeCheapType)
                                                    }
                                                    
                                                }
                                                else{
                                                    
                                                    let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:Constants.SHORTEST_CHEAPEST)
                                                    routePreference.append(routeShortType)
                                                    
                                                    let routeCheapType = PreferenceRoute(routeId: 2, routePreferenceType:"")
                                                    routePreference.append(routeCheapType)
                                                }
                                            }
                                            else{
                                                
                                                
                                                if(cheapestDetected){
                                                    
                                                    if(response.routes![0].expectedTravelTime <= response.routes![2].expectedTravelTime){
                                                        
                                                        let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:Constants.FASTEST)
                                                        routePreference.append(routeFastType)
                                                        
                                                        let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:Constants.SHORTEST)
                                                        routePreference.append(routeShortType)
                                                        
                                                        let routeCheapType = PreferenceRoute(routeId: 2, routePreferenceType:Constants.CHEAPEST)
                                                        routePreference.append(routeCheapType)
                                                    }
                                                    else{
                                                        
                                                        let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:Constants.FASTEST)
                                                        routePreference.append(routeFastType)
                                                        
                                                        let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:Constants.SHORTEST)
                                                        routePreference.append(routeShortType)
                                                        
                                                        let routeCheapType = PreferenceRoute(routeId: 2, routePreferenceType:Constants.CHEAPEST)
                                                        routePreference.append(routeCheapType)
                                                    }
                                                    
                                                    
                                                }
                                                else{
                                                    
                                                    let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:Constants.FASTEST_CHEAPEST)
                                                    routePreference.append(routeFastType)
                                                    
                                                    let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:Constants.SHORTEST)
                                                    routePreference.append(routeShortType)
                                                    
                                                    let routeCheapType = PreferenceRoute(routeId: 2, routePreferenceType:"")
                                                    routePreference.append(routeCheapType)
                                                }
                                            }
                                        }
                                        else{
                                            
                                            if(erpList[0].routeERPRate >= erpList[1].routeERPRate)
                                            {
                                                if(cheapestDetected){
                                                    
                                                    let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:Constants.FASTEST)
                                                    routePreference.append(routeFastType)
                                                    
                                                    let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:"")
                                                    routePreference.append(routeShortType)
                                                    
                                                    let routeCheapType = PreferenceRoute(routeId: 2, routePreferenceType:Constants.SHORTEST_CHEAPEST)
                                                    routePreference.append(routeCheapType)
                                                    
                                                }
                                                else{
                                                    
                                                    let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:Constants.FASTEST)
                                                    routePreference.append(routeFastType)
                                                    
                                                    let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:Constants.SHORTEST_CHEAPEST)
                                                    routePreference.append(routeShortType)
                                                    
                                                    let routeCheapType = PreferenceRoute(routeId: 2, routePreferenceType:"")
                                                    routePreference.append(routeCheapType)
                                                }
                                            }
                                            else{
                                                
                                                if(cheapestDetected){
                                                    
                                                    let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:Constants.FASTEST)
                                                    routePreference.append(routeFastType)
                                                    
                                                    let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:"")
                                                    routePreference.append(routeShortType)
                                                    
                                                    let routeCheapType = PreferenceRoute(routeId: 2, routePreferenceType:Constants.SHORTEST_CHEAPEST)
                                                    routePreference.append(routeCheapType)
                                                    
                                                }
                                                else{
                                                    
                                                    let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:Constants.FASTEST_CHEAPEST)
                                                    routePreference.append(routeFastType)
                                                    
                                                    let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:Constants.SHORTEST)
                                                    routePreference.append(routeShortType)
                                                    
                                                    let routeCheapType = PreferenceRoute(routeId: 2, routePreferenceType:"")
                                                    routePreference.append(routeCheapType)
                                                }
                                            }
                                            
                                        }
                                    }
                                    
                                    
                                }
                                else{
                                    
                                    if(erpList[0].routeERPRate != 0 && erpList[1].routeERPRate == 0){
                                        
                                        if( response.routes![0].distance <= response.routes![1].distance && response.routes![0].distance <= response.routes![2].distance){
                                            
                                            let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:Constants.FASTEST_SHORTEST)
                                               routePreference.append(routeFastType)
                                            
                                            if(cheapestDetected){
                                                
                                                if(response.routes![1].expectedTravelTime <= response.routes![0].expectedTravelTime){
                                                    
                                                    let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:"")
                                                       routePreference.append(routeShortType)
                                                    
                                                    let routeCheapType = PreferenceRoute(routeId: 2, routePreferenceType:"")
                                                       routePreference.append(routeCheapType)
                                                    
                                                }
                                                else{
                                                    
                                                    let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:"")
                                                       routePreference.append(routeShortType)
                                                    
                                                    let routeCheapType = PreferenceRoute(routeId: 2, routePreferenceType:"")
                                                       routePreference.append(routeCheapType)
                                                }
                                                
                                            }
                                            else{
                                                
                                                let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:"")
                                                   routePreference.append(routeShortType)
                                                
                                                let routeCheapType = PreferenceRoute(routeId: 2, routePreferenceType:"")
                                                   routePreference.append(routeCheapType)
                                            }
                                        }
                                        
                                        else{
                                            
                                            if( response.routes![1].distance <= response.routes![0].distance && response.routes![1].distance <= response.routes![2].distance){
                                                
                                                let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:Constants.FASTEST)
                                                   routePreference.append(routeFastType)
                                                
                                                if(cheapestDetected){
                                                    
                                                    if(response.routes![1].expectedTravelTime <= response.routes![2].expectedTravelTime){
                                                        
                                                        
                                                        let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:Constants.SHORTEST_CHEAPEST)
                                                           routePreference.append(routeShortType)
                                                        
                                                        let routeCheapType = PreferenceRoute(routeId: 2, routePreferenceType:"")
                                                           routePreference.append(routeCheapType)
                                                    }
                                                    else{
                                                        
                                                        let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:Constants.SHORTEST_CHEAPEST)
                                                           routePreference.append(routeShortType)
                                                        
                                                        let routeCheapType = PreferenceRoute(routeId: 2, routePreferenceType:"")
                                                           routePreference.append(routeCheapType)
                                                    }
                                                    
                                                }
                                                else{
                                                    
                                                    let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:Constants.SHORTEST_CHEAPEST)
                                                       routePreference.append(routeShortType)
                                                    
                                                    let routeCheapType = PreferenceRoute(routeId: 2, routePreferenceType:"")
                                                       routePreference.append(routeCheapType)
                                                }
                                            }
                                            else{
                                                
                                                let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:Constants.FASTEST)
                                                   routePreference.append(routeFastType)
                                                
                                                if(cheapestDetected){
                                                    
                                                    if(response.routes![1].expectedTravelTime <= response.routes![2].expectedTravelTime){
                                                        
                                                        let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:Constants.SHORTEST_CHEAPEST)
                                                           routePreference.append(routeShortType)
                                                        
                                                        let routeCheapType = PreferenceRoute(routeId: 2, routePreferenceType:"")
                                                           routePreference.append(routeCheapType)
                                                    }
                                                    else{
                                                        
                                                        let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:Constants.SHORTEST_CHEAPEST)
                                                           routePreference.append(routeShortType)
                                                        
                                                        let routeCheapType = PreferenceRoute(routeId: 2, routePreferenceType:"")
                                                           routePreference.append(routeCheapType)
                                                    }
                                                   
                                                }
                                                else{
                                                    
                                                    let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:Constants.SHORTEST_CHEAPEST)
                                                       routePreference.append(routeShortType)
                                                    
                                                    let routeCheapType = PreferenceRoute(routeId: 2, routePreferenceType:"")
                                                       routePreference.append(routeCheapType)
                                                }
                                            }
                                        }
                                        
                                        
                                    }
                                    else{
                                        
                                        if(erpList[1].routeERPRate != 0 && erpList[0].routeERPRate == 0){
                                            
                                            if(response.routes![0].distance <= response.routes![1].distance && response.routes![0].distance <= response.routes![2].distance){
                                                
                                                
                                                if(cheapestDetected){
                                                    
                                                    let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:Constants.FASTEST_SHORTEST)
                                                       routePreference.append(routeFastType)
                                                    
                                                    let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:"")
                                                       routePreference.append(routeShortType)
                                                    
                                                    let routeCheapType = PreferenceRoute(routeId: 2, routePreferenceType:"")
                                                       routePreference.append(routeCheapType)
                                                    
                                                    
                                                }
                                                else{
                                                    
                                                    let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:Constants.FASTEST_SHORTEST)
                                                       routePreference.append(routeFastType)
                                                    
                                                    let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:"")
                                                       routePreference.append(routeShortType)
                                                    
                                                    let routeCheapType = PreferenceRoute(routeId: 2, routePreferenceType:"")
                                                       routePreference.append(routeCheapType)
                                                    
                                                }
                                            }
                                            
                                            else{
                                                
                                                if(response.routes![1].distance <= response.routes![0].distance && response.routes![1].distance <= response.routes![2].distance){
                                                    
                                                    
                                                    if(cheapestDetected){
                                                        
                                                        if(response.routes![0].expectedTravelTime <= response.routes![2].expectedTravelTime){
                                                            
                                                            let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:Constants.FASTEST_CHEAPEST)
                                                               routePreference.append(routeFastType)
                                                            
                                                            let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:Constants.SHORTEST)
                                                               routePreference.append(routeShortType)
                                                            
                                                            let routeCheapType = PreferenceRoute(routeId: 2, routePreferenceType:"")
                                                               routePreference.append(routeCheapType)
                                                        }
                                                        else{
                                                            
                                                            let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:Constants.FASTEST)
                                                               routePreference.append(routeFastType)
                                                            
                                                            let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:Constants.SHORTEST)
                                                               routePreference.append(routeShortType)
                                                            
                                                            let routeCheapType = PreferenceRoute(routeId: 2, routePreferenceType:"")
                                                               routePreference.append(routeCheapType)
                                                        }
                                                        
                                                        
                                                    }
                                                    else{
                                                        
                                                        let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:Constants.FASTEST_CHEAPEST)
                                                           routePreference.append(routeFastType)
                                                        
                                                        let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:Constants.SHORTEST)
                                                           routePreference.append(routeShortType)
                                                        
                                                        let routeCheapType = PreferenceRoute(routeId: 2, routePreferenceType:"")
                                                           routePreference.append(routeCheapType)
                                                        
                                                    }
                                                }
                                                else{
                                                    
                                                    
                                                    
                                                    if(cheapestDetected){
                                                        
                                                        let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:Constants.FASTEST)
                                                           routePreference.append(routeFastType)
                                                        
                                                        let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:"")
                                                           routePreference.append(routeShortType)
                                                        
                                                        let routeCheapType = PreferenceRoute(routeId: 2, routePreferenceType:Constants.SHORTEST_CHEAPEST)
                                                           routePreference.append(routeCheapType)
                                                        
                                                        
                                                    }
                                                    else{
                                                        
                                                        let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:Constants.FASTEST_CHEAPEST)
                                                           routePreference.append(routeFastType)
                                                        
                                                        let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:"")
                                                           routePreference.append(routeShortType)
                                                        
                                                        let routeCheapType = PreferenceRoute(routeId: 2, routePreferenceType:Constants.SHORTEST)
                                                           routePreference.append(routeCheapType)
                                                        
                                                    }
                                                }
                                            }
                                            
                                        }
                                        
                                        
                                    }
                                }
                            }
                        }
                        else{
                            
                            if(erpList.count == 1){
                                
                                if(erpList[0].routeERPRate == 0){
                                    
                                    if(response.routes![0].expectedTravelTime <= response.routes![1].expectedTravelTime && response.routes![0].expectedTravelTime <= response.routes![2].expectedTravelTime && response.routes![0].distance <= response.routes![1].distance && response.routes![0].distance <= response.routes![2].distance){
                                        
                                        let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:Constants.FASTEST_SHORTEST)
                                           routePreference.append(routeFastType)
                                        
                                        let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:"")
                                           routePreference.append(routeShortType)
                                        
                                        let routeCheapType = PreferenceRoute(routeId: 2, routePreferenceType:"")
                                           routePreference.append(routeCheapType)
                                    }
                                    else {
                                        
                                        if(response.routes![1].expectedTravelTime <= response.routes![0].expectedTravelTime && response.routes![1].expectedTravelTime <= response.routes![2].expectedTravelTime && response.routes![1].distance <= response.routes![0].distance && response.routes![1].distance <= response.routes![2].distance){
                                            
                                            let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:"")
                                               routePreference.append(routeFastType)
                                            
                                            let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:Constants.FASTEST_SHORTEST)
                                               routePreference.append(routeShortType)
                                            
                                            let routeCheapType = PreferenceRoute(routeId: 2, routePreferenceType:"")
                                               routePreference.append(routeCheapType)
                                        }
                                        else{
                                            
                                            if(response.routes![2].expectedTravelTime <= response.routes![0].expectedTravelTime && response.routes![2].expectedTravelTime <= response.routes![1].expectedTravelTime && response.routes![2].distance <= response.routes![0].distance && response.routes![2].distance <= response.routes![1].distance){
                                                
                                                let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:"")
                                                   routePreference.append(routeFastType)
                                                
                                                let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:"")
                                                   routePreference.append(routeShortType)
                                                
                                                let routeCheapType = PreferenceRoute(routeId: 2, routePreferenceType:Constants.FASTEST_SHORTEST)
                                                   routePreference.append(routeCheapType)
                                            }
                                            else{
                                                
                                                if(response.routes![0].distance <= response.routes![1].distance && response.routes![0].distance <=  response.routes![2].distance){
                                                    
                                                    let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:Constants.FASTEST_SHORTEST)
                                                       routePreference.append(routeFastType)
                                                    
                                                    let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:"")
                                                       routePreference.append(routeShortType)
                                                    
                                                    let routeCheapType = PreferenceRoute(routeId: 2, routePreferenceType:"")
                                                       routePreference.append(routeCheapType)
                                                }
                                                else{
                                                    
                                                    if(response.routes![1].distance <= response.routes![0].distance && response.routes![1].distance <=  response.routes![2].distance){
                                                        
                                                        let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:Constants.FASTEST)
                                                           routePreference.append(routeFastType)
                                                        
                                                        let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:Constants.SHORTEST)
                                                           routePreference.append(routeShortType)
                                                        
                                                        let routeCheapType = PreferenceRoute(routeId: 2, routePreferenceType:"")
                                                           routePreference.append(routeCheapType)
                                                    }
                                                    else{
                                                        
                                                        if(response.routes![2].distance <= response.routes![0].distance && response.routes![2].distance <=  response.routes![0].distance){
                                                            
                                                            let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:Constants.FASTEST)
                                                               routePreference.append(routeFastType)
                                                            
                                                            let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:"")
                                                               routePreference.append(routeShortType)
                                                            
                                                            let routeCheapType = PreferenceRoute(routeId: 2, routePreferenceType:Constants.SHORTEST)
                                                               routePreference.append(routeCheapType)
                                                        }
                                                    }
                                                }
                                                    
                                                    
                                                }
                                            }
                                        }
                                }
                                else{
                                    
                                    let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:Constants.FASTEST)
                                       routePreference.append(routeFastType)
                                    
                                    if(cheapestDetected){
                                        
                                        let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:Constants.SHORTEST)
                                           routePreference.append(routeShortType)
                                        
                                        let routeCheapType = PreferenceRoute(routeId: 2, routePreferenceType:Constants.CHEAPEST)
                                           routePreference.append(routeCheapType)
                                    }
                                    else{
                                        
                                        let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:Constants.SHORTEST_CHEAPEST)
                                           routePreference.append(routeShortType)
                                        
                                        let routeCheapType = PreferenceRoute(routeId: 2, routePreferenceType:"")
                                           routePreference.append(routeCheapType)
                                    }
                                }
                            }
                        }
                        
                        
                    }
                    
                }
                
                else {
                    
                    if(resultCount == 2){
                        
                        if(erpList.count == 3){
                            
                            if(erpList[0].routeERPRate == 0 && erpList[1].routeERPRate == 0 && erpList[2].routeERPRate == 0){
                                
                                if(response.routes![0].expectedTravelTime <= response.routes![1].expectedTravelTime &&  response.routes![0].distance <= response.routes![1].distance){
                                    
                                    let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:Constants.FASTEST_SHORTEST)
                                       routePreference.append(routeFastType)
                                    
                                    let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:"")
                                       routePreference.append(routeShortType)
                                    
                                }
                                else {
                                    
                                    if(response.routes![1].expectedTravelTime <= response.routes![0].expectedTravelTime && response.routes![1].distance <= response.routes![0].distance){
                                        
                                        let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:"")
                                           routePreference.append(routeFastType)
                                        
                                        let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:Constants.FASTEST_SHORTEST)
                                           routePreference.append(routeShortType)
                                        
                                    }
                                    else{
                                        
                                        let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:Constants.FASTEST)
                                           routePreference.append(routeFastType)
                                        
                                        if(response.routes![1].distance <= response.routes![2].distance){
                                            
                                            let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:Constants.SHORTEST)
                                               routePreference.append(routeShortType)
                                            
                                            let routeCheapType = PreferenceRoute(routeId: 2, routePreferenceType:"")
                                               routePreference.append(routeCheapType)
                                        }
                                        else{
                                            
                                            let routeShortType = PreferenceRoute(routeId: 2, routePreferenceType:Constants.SHORTEST)
                                               routePreference.append(routeShortType)
                                            
                                            let routeCheapType = PreferenceRoute(routeId: 1, routePreferenceType:"")
                                               routePreference.append(routeCheapType)
                                            
                                        }
                                        
                                        
                                        
                                        }
                                    }
                                }
                            
                            else{
                                
                                if(erpList[0].routeERPRate != 0 && (erpList[0].routeERPRate >= erpList[1].routeERPRate || erpList[0].routeERPRate >= erpList[2].routeERPRate)){
                                    
                                    let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:Constants.FASTEST)
                                       routePreference.append(routeFastType)
                                    
                                    let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:Constants.SHORTEST_CHEAPEST)
                                       routePreference.append(routeShortType)
                                }
                                else{
                                    
                                    let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:Constants.FASTEST_CHEAPEST)
                                       routePreference.append(routeFastType)
                                    
                                    let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:Constants.SHORTEST)
                                       routePreference.append(routeShortType)
                                }
                            }
                            
                        }
                        
                        else
                        {
                            if(erpList.count == 2){
                                
                                if(erpList[0].routeERPRate == 0 && erpList[1].routeERPRate == 0){
                                    
                                    if(response.routes![0].expectedTravelTime <= response.routes![1].expectedTravelTime &&  response.routes![0].distance <= response.routes![1].distance){
                                        
                                        let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:Constants.FASTEST_SHORTEST)
                                           routePreference.append(routeFastType)
                                        
                                        let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:"")
                                           routePreference.append(routeShortType)
                                        
                                        
                                    }
                                    else {
                                        
                                        if(response.routes![1].expectedTravelTime <= response.routes![0].expectedTravelTime &&  response.routes![1].distance <= response.routes![0].distance ){
                                            
                                            let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:"")
                                               routePreference.append(routeFastType)
                                            
                                            let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:Constants.FASTEST_SHORTEST)
                                               routePreference.append(routeShortType)
                                            
                                           
                                        }
                                        else{
                                            
                                            
                                            if(response.routes![0].distance <= response.routes![1].distance){
                                                
                                                let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:Constants.FASTEST_SHORTEST)
                                                   routePreference.append(routeFastType)
                                                
                                                let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:"")
                                                   routePreference.append(routeShortType)
                                                
                                               
                                            }
                                            else{
                                                
                                                let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:Constants.FASTEST)
                                                   routePreference.append(routeFastType)
                                                
                                                let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:Constants.SHORTEST)
                                                   routePreference.append(routeShortType)
                                                
                                                
                                                
                                            }
                                            
                                            }
                                        }
                                }
                                else{
                                    
                                    //To DO: Need to re-think about this scenario
                                    if(erpList[0].routeERPRate != 0 && erpList[1].routeERPRate != 0){
                                        
                                        if( response.routes![0].distance <= response.routes![1].distance ){
                                            
                                            if(erpList[0].routeERPRate >= erpList[1].routeERPRate){
                                                
                                                let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:Constants.FASTEST_SHORTEST)
                                                routePreference.append(routeFastType)
                                                
                                                let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:Constants.CHEAPEST)
                                                routePreference.append(routeShortType)
                                                
                                            }
                                            else{
                                                
                                                let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:Constants.FASTEST_CHEAPEST)
                                                routePreference.append(routeFastType)
                                                
                                                let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:Constants.SHORTEST)
                                                routePreference.append(routeShortType)
                                                
                                                
                                            }
                                            
                                        }
                                        else{
                                            
                                           
                                                if(erpList[0].routeERPRate >= erpList[1].routeERPRate){
                                                    
                                                    let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:Constants.FASTEST)
                                                    routePreference.append(routeFastType)
                                                    
                                                    let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:Constants.SHORTEST_CHEAPEST)
                                                    routePreference.append(routeShortType)
                                                    
                                                    
                                                }
                                                else{
                                                    
                                                    let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:Constants.FASTEST_CHEAPEST)
                                                    routePreference.append(routeFastType)
                                                    
                                                    let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:Constants.SHORTEST)
                                                    routePreference.append(routeShortType)
                                                }
                                            
                                            
                                        }
                                        
                                        
                                    }
                                    else{
                                        
                                        if(erpList[0].routeERPRate != 0 && erpList[1].routeERPRate == 0){
                                            
                                            if( response.routes![0].distance <= response.routes![1].distance){
                                                
                                                let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:Constants.FASTEST_SHORTEST)
                                                   routePreference.append(routeFastType)
                                                
                                                let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:Constants.CHEAPEST)
                                                   routePreference.append(routeShortType)
                                                
                                            }
                                            
                                            else{
                                                
                                                let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:Constants.FASTEST)
                                                   routePreference.append(routeFastType)
                                                
                                                let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:Constants.SHORTEST_CHEAPEST)
                                                   routePreference.append(routeShortType)
                                                
                                                
                                            }
                                            
                                            
                                        }
                                        else{
                                            
                                            if(erpList[1].routeERPRate != 0 && erpList[0].routeERPRate == 0){
                                                
                                                if(response.routes![0].distance <= response.routes![1].distance){
                                                    
                                                    
                                                    let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:Constants.FASTEST_SHORTEST)
                                                       routePreference.append(routeFastType)
                                                    
                                                    let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:"")
                                                       routePreference.append(routeShortType)
                                                    
                                                  
                                                }
                                                
                                                else{
                                                    
                                                    let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:Constants.FASTEST_CHEAPEST)
                                                       routePreference.append(routeFastType)
                                                    
                                                    let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:Constants.SHORTEST)
                                                       routePreference.append(routeShortType)
                                                    
                                                    
                                                }
                                                
                                            }
                                            
                                            
                                        }
                                    }
                                }
                            }
                            else{
                                
                                if(erpList.count == 1){
                                    
                                    if(erpList[0].routeERPRate == 0){
                                        
                                        if(response.routes![0].expectedTravelTime <= response.routes![1].expectedTravelTime &&  response.routes![0].distance <= response.routes![1].distance){
                                            
                                            let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:Constants.FASTEST_SHORTEST)
                                               routePreference.append(routeFastType)
                                            
                                            let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:"")
                                               routePreference.append(routeShortType)
                                            
                                           
                                        }
                                        else {
                                            
                                            if(response.routes![1].expectedTravelTime <= response.routes![0].expectedTravelTime &&  response.routes![1].distance <= response.routes![0].distance){
                                                
                                                let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:"")
                                                   routePreference.append(routeFastType)
                                                
                                                let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:Constants.FASTEST_SHORTEST)
                                                   routePreference.append(routeShortType)
                                                
                                              
                                            }
                                            else{
                                                
                                                let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:Constants.FASTEST)
                                                   routePreference.append(routeFastType)
                                                
                                                let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:Constants.SHORTEST)
                                                   routePreference.append(routeShortType)
                                                
                                                
                                                }
                                            }
                                    }
                                    else{
                                        
                                        if(cheapestDetected){
                                            
                                            let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:Constants.FASTEST_SHORTEST)
                                               routePreference.append(routeFastType)
                                            
                                            let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:Constants.CHEAPEST)
                                               routePreference.append(routeShortType)
                                            
                                        }
                                        else{
                                            
                                            let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:Constants.FASTEST)
                                               routePreference.append(routeFastType)
                                            
                                            let routeShortType = PreferenceRoute(routeId: 1, routePreferenceType:Constants.SHORTEST_CHEAPEST)
                                               routePreference.append(routeShortType)
                                        }
                                        
                                        
                                    }
                                }
                            }
                            
                            
                        }
                    }
                    
                    if(resultCount == 1){
                        
                        let routeFastType = PreferenceRoute(routeId: 0, routePreferenceType:"")
                           routePreference.append(routeFastType)
                    }
                }
                
                if(routePreference.count > 0){
                    
                    let finalRoutePreference = RoutePreferenceData(routes: routePreference, routeResponse: response)
                    return finalRoutePreference
                }
                return nil
            }
            
            func findBestOfRoutesForMobile(fastestResponse:RouteResponse,shortestResponse:RouteResponse,cheapestResponse:RouteResponse) -> RoutePreferenceData? {
                
                
                var erpList = [RouteERPList]()
                var fastestDetected = false
                var shortestDetected = false
                var cheapestDetected = false

                let fRoutes  = fastestResponse.routes?.sorted()
                
                let sRoutes  = shortestResponse.routes?.sortedDistance()
                
                let cRoutes  = cheapestResponse.routes?.sorted()
                
                var result = [Route]()
                
                if let routes = fRoutes, routes.count > 0 {
                    result.append(routes[0])
                    let erpValue = getERPRate(erpTolls: DataCenter.shared.getAllERPFeatureCollection(), routes: result)
                    erpList.append(RouteERPList(routeERPRate: Int(erpValue)))
                    fastestDetected = true
                }
                
                for shortRoute in sRoutes ?? [] {
                    
                    var distinctRoute = true;
                    
                    for route in result{
                        
                        if(route == shortRoute)
                        {
                            distinctRoute = false
                            break;
                            
                        }
                        
        //                if( shortRoute.distance > route.distance){
        //                    distinctRoute = false
        //                    break
        //                }
                        
                    }
                    
                    if(distinctRoute == true){
                        
                        if(result.count < 3){
                            
                            if(!shortestDetected){
                                
                                result.append(shortRoute)
                                let erpValue = getERPRate(erpTolls: DataCenter.shared.getAllERPFeatureCollection(), routes: [shortRoute])
                                erpList.append(RouteERPList(routeERPRate: Int(erpValue)))
                                shortestDetected = true
                                break
                            }
                            
                        }
                    }
                    
                }
                
                //if(erpList[0].routeERPRate != 0 || erpList[1].routeERPRate != 0){
                    
                    for cheapRoute in cRoutes ?? [] {
                        
                        var distinctRoute = true;
                        
                        for route in result{
                            
                            if(route == cheapRoute)
                            {
                                distinctRoute = false
                                break;
                                
                            }
                            
        //                        if(cheapRoute.expectedTravelTime > route.expectedTravelTime){
        //                            distinctRoute = false
        //                            break
        //                        }
                            
                        }
                        
                        if(distinctRoute == true){
                            
                            if(result.count < 3){
                                if(!result.contains(cheapRoute)){
                                    result.append(cheapRoute)
        //                            let erpValue = getERPRate(erpTolls: DataCenter.shared.getAllERPFeatureCollection(), routes: [cheapRoute])
        //                            erpList.append(RouteERPList(routeERPRate: Int(erpValue)))
                                    cheapestDetected = true
                                    
                                }
                                
                            }
                        }
                    }
                
                
                if(result.count < 3){
                    
                    for fastRoute in fRoutes ?? [] {
                        
                        var distinctRoute = true;
                        
                        for route in result{
                            
                            if(route == fastRoute)
                            {
                                distinctRoute = false
                                break;
                                
                            }
                            
                        }
                        
                        if(distinctRoute == true){
                            
                            if(result.count < 3){
                                
                                result.append(fastRoute)
                                let erpValue = getERPRate(erpTolls: DataCenter.shared.getAllERPFeatureCollection(), routes: [fastRoute])
                                erpList.append(RouteERPList(routeERPRate: Int(erpValue)))
                                break
                                
                            }
                        }
                        
                    }
                }
                //result = result.sorted()
                let response = RouteResponse(httpResponse: nil, identifier: fastestResponse.identifier, routes: result, waypoints: fastestResponse.waypoints, options: fastestResponse.options, credentials: fastestResponse.credentials)
                
                
               //let routePreference =  self.getFSCPreferences(response: response, erpList: erpList,fastDetected: fastestDetected,cheapestDetected: cheapestDetected,shortestDetected: shortestDetected)
                let routePreference = self.getRoutePreferenceGrouping(erpList: erpList, resultCount: response.routes?.count ?? 0 , response: response, fastDetected: fastestDetected, cheapestDetected: cheapestDetected, shortestDetected: shortestDetected)
                return routePreference
                
            }
    
    func findRoutePreferencesForSingleRouteResponse(response:RouteResponse,erpList:[RouteERPList]) -> RoutePreferenceData? {
        
        let routePreference = self.getRoutePreferenceGrouping(erpList: erpList, resultCount: response.routes?.count ?? 0, response: response, fastDetected: false, cheapestDetected: true, shortestDetected: false)
        return routePreference
    }
    
    func findBestOfRoutesForCarPlay(fastestResponse:RouteResponse,shortestResponse:RouteResponse,cheapestResponse:RouteResponse) -> RouteResponse {
        
        let fRoutes  = fastestResponse.routes?.sorted()
        
        //let sortedRouteArray = response!.sorted { $0.distance < $1.distance }
        let sRoutes  = shortestResponse.routes?.sortedDistance()
        
        
        let cRoutes  = cheapestResponse.routes?.sorted()
        
        
        
        var result = [Route]()
        
        if let routes = fRoutes, routes.count > 0 {
            result.append(routes[0])
        }
        
        for shortRoute in sRoutes ?? [] {
            
            var distinctRoute = true;
            var shortestDetected = false
            for route in result{
                
                if(route == shortRoute)
                {
                    distinctRoute = false
                    break;
                    
                }
                
                
            }
            
            if(distinctRoute == true){
                
                if(result.count < 3){
                    
                    if(!shortestDetected){
                        
                        result.append(shortRoute)
                        
                        shortestDetected = true
                        break
                    }
                    
                }
            }
            
        }
        
        
        for cheapRoute in cRoutes ?? [] {
            
            var distinctRoute = true;
            
            for route in result{
                
                if(route == cheapRoute)
                {
                    distinctRoute = false
                    break;
                    
                }
                
            }
            
            if(distinctRoute == true){
                
                if(result.count < 3){
                    if(!result.contains(cheapRoute)){
                        result.append(cheapRoute)
                    }
                }
            }
        }
        
        result = result.sorted()
        let response = RouteResponse(httpResponse: nil, identifier: fastestResponse.identifier, routes: result, waypoints: fastestResponse.waypoints, options: fastestResponse.options, credentials: fastestResponse.credentials)
        
        return response
        
    }
    
   
    func filterOutCommonRoutes(firstResponse:RouteResponse,secondResponse:RouteResponse) -> RouteResponse{
        
        var result: [Route] = []

        let cheapRoutes = secondResponse.routes?.sorted()
        for route in firstResponse.routes ?? [] {
            for cheaprRoute in cheapRoutes ?? [] {
                if(!result.contains(cheaprRoute) && route != cheaprRoute){
                    if(result.count != 3)
                    {
                        result.append(cheaprRoute)
                        break
                    }
                }
            }
        }
        
    
        let response = RouteResponse(httpResponse: nil, identifier: secondResponse.identifier, routes: result, waypoints: secondResponse.waypoints, options: secondResponse.options, credentials: secondResponse.credentials)
        
          return response
    }
    
    //Finding shortest route between "mapbox/driving-traffic" profile and "mapbox/driving"
    func findShortest(responseFast:RouteResponse) -> RouteResponse{
                
        var sortedRouteArray: [Route] = []
        if let response = responseFast.routes, !response.isEmpty {
            sortedRouteArray = response.sorted { $0.distance < $1.distance }
            for route in sortedRouteArray{
                print("Distance",route.distance)
            }
        }
        let finalResponse = RouteResponse(httpResponse: nil, identifier: responseFast.identifier, routes: sortedRouteArray, waypoints: nil, options: responseFast.options, credentials: responseFast.credentials)
        
        return finalResponse
    }
    
    func findActiveAndInActiveTollsFromFastRoute(erpTolls:FeatureCollection, response:RouteResponse)->(activeToll:RouteResponse?,inActiveToll:RouteResponse?){
        
        var inActiveTollRoute:[Route] = []
        self.response = response
        var inActiveTollResponse:RouteResponse?
        let routes = response.routes
        var arrayOfPrices = [Float]()
        for route in routes ?? [] {

            var tempERPPrice = Float()

            let routeLineSegment = routesAsLineSegments(route: route)

            //Looping for each erpTolls which is a LineString
            for erpToll in erpTolls.features{

                //Looping for each route Segment
                for routeSegment in routeLineSegment{

                    //Getting geometry from erpToll
                    guard case let .lineString(erpGeometry) = erpToll.geometry else { return (nil,nil) }
                    //print("Toll",boothGeometry?.coordinates.latitude)

                    //creating a LingSegment for E R P
                    let erpLineSegment = (erpGeometry.coordinates[0],erpGeometry.coordinates[1])

                    ///Finding intersection with two Linesegments
                    if let _ = intersection(routeSegment , (erpLineSegment as? LineSegment)!) {
                        guard case let .string(stringPrice)  = erpToll.properties!["cost"] else  { return (nil,nil) }
                        let priceArray = stringPrice.components(separatedBy: "$")
                        if priceArray.count > 1, let additionalPrice = Float(priceArray[1]) {
                            tempERPPrice = tempERPPrice + additionalPrice
                        }
                    }
                    
                }
            }
            
            if(tempERPPrice == 0.0)
            {
                inActiveTollRoute.append(route)//Adding this route inActiveTollArray
            }
            else
            {
                //Adding ERP prices to array
                arrayOfPrices.append(tempERPPrice)
            }
        }
        
        if(arrayOfPrices.count > 0)
        {
            //Finding min of ERP prices
            let mins = arrayOfPrices.min()
            
            //Finsing Index
            let index = arrayOfPrices.firstIndex(of: mins!)
            
            //Swapping the response route with Index
            self.response?.routes?.swapAt(index!, 0)

        }
    
        if(inActiveTollRoute.count > 0){
            
            inActiveTollResponse = RouteResponse(httpResponse: nil, identifier: self.response?.identifier, routes: inActiveTollRoute, waypoints: self.response?.waypoints ?? [], options: self.response!.options, credentials: self.response!.credentials)
        }
    
        return (response,inActiveTollResponse)
        
    }
    
    func getERPRate(erpTolls:FeatureCollection,routes:[Route]) -> Float {

        var tempERPPrice = Float()
            for route in routes {

                let routeLineSegment = routesAsLineSegments(route: route)

                //Looping for each erpTolls which is a LineString
                for erpToll in erpTolls.features{

                    //Looping for each route Segment
                    for routeSegment in routeLineSegment{

                        //Getting geometry from erpToll
                        guard case let .lineString(erpGeometry) = erpToll.geometry else { return  0.0}
                        //print("Toll",boothGeometry?.coordinates.latitude)

                        //creating a LingSegment for E R P
                        let erpLineSegment = (erpGeometry.coordinates[0],erpGeometry.coordinates[1])

                        ///Finding intersection with two Linesegments
                        if let _ = intersection(routeSegment , erpLineSegment as! LineSegment) {
                            if case let .string(stringPrice) = erpToll.properties!["cost"],
                               case let .string(id) = erpToll.properties?["erpid"]{
                                
                                if let erpValues =  self.findERPIsInWithIn30m(erpID:Int(id) ?? 0){
                                    
                                    print("ERP Bound Values",erpValues)
                                    
                                    let priceArray = erpValues.currentPrice.components(separatedBy: "$")
                                    let doublePrice = priceArray[1]
                                   
                                    tempERPPrice = tempERPPrice + Float(doublePrice)!
                                    
                                }
                                
                            }
                            
                        }
                    }
                }
                
                
            }
        
        return tempERPPrice
            
    }
    
    //Vishnu - Fix this for Turn by Turn Navigation
    func cheapestRoute(erpTolls:FeatureCollection,isFastestRouteOption:Bool) -> RouteResponse {

            let routes = response?.routes
            var arrayOfPrices = [Float]()
            for route in routes ?? []{

                var tempERPPrice = Float()

                let routeLineSegment = routesAsLineSegments(route: route)

                //Looping for each erpTolls which is a LineString
                for erpToll in erpTolls.features{

                    //Looping for each route Segment
                    for routeSegment in routeLineSegment{

                        //Getting geometry from erpToll
                        guard case let .lineString(erpGeometry) = erpToll.geometry else { return response! }
                        //print("Toll",boothGeometry?.coordinates.latitude)

                        //creating a LingSegment for E R P
                        let erpLineSegment = (erpGeometry.coordinates[0],erpGeometry.coordinates[1])

                        ///Finding intersection with two Linesegments
                        if let _ = intersection(routeSegment , erpLineSegment as! LineSegment) {
                            guard case let .string(stringPrice)  = erpToll.properties!["cost"] else { return response! }
                            let priceArray = stringPrice.components(separatedBy: "$")
                            let doublePrice = priceArray[1]
                            tempERPPrice = tempERPPrice + Float(doublePrice)!
                        }
                    }
                }
                
                //Adding ERP prices to array
                arrayOfPrices.append(tempERPPrice)
                
                
            }
            
            if(arrayOfPrices.count > 0)
            {
                //Finding min of ERP prices
                let mins = arrayOfPrices.min()
                
                //Finsing Index
                let index = arrayOfPrices.firstIndex(of: mins!)
                
                //Swapping the response route with Index
                self.response?.routes?.swapAt(index!, 0)

            }
        
        
        return response!
    }
    
    private func routesAsLineSegments(route:Route) -> [LineSegment]
    {
        var routeLineSegment = [LineSegment]()
        if let coordinates = route.shape?.coordinates, coordinates.count >= 2 {
            for i in 0...(coordinates.count - 2) {
                let segment = (coordinates[i], coordinates[i + 1])
                routeLineSegment.append(segment)
            }
        }
        return routeLineSegment
    }
    
     func computeTollsForCarPlayRP(route:Route,erpTolls:FeatureCollection) -> String{
        
         if(allERPInSelectedRoute.count > 0)
         {
             allERPInSelectedRoute.removeAll()
         }
         
         if let shape = route.shape {
             let feature = [Turf.Feature(geometry: .lineString(LineString(shape.coordinates)))]
             let routCollection = FeatureCollection(features: feature)
             
             guard case .lineString(_) = routCollection.features.first?.geometry else { return "$0.0" }
             var tempERPPrice = Float()
             let routeLineSegment = routesAsLineSegments(route: route)
             
             //Looping for each erpTolls which is a LineString
             for erpToll in erpTolls.features{
                 
                 //Looping for each route Segment
                 for routeSegment in routeLineSegment{
                     
                     //Getting geometry from erpToll
                     guard case let .lineString(erpGeometry) = erpToll.geometry else { return "$0.0" }
                     //print("Toll",boothGeometry?.coordinates.latitude)
                     
                     //creating a LingSegment for E R P
                     let erpLineSegment =  (erpGeometry.coordinates[0],erpGeometry.coordinates[1])
                     
                     
                     ///Finding intersection with two Linesegments
                     if let intersectCoordinate = intersection(routeSegment , erpLineSegment) {
                         //print(erpToll)
                         
                         guard case let .string(stringPrice)  = erpToll.properties?["cost"] else { return "$0.0" }
                         let priceArray = stringPrice.components(separatedBy: "$")
                         
                         if priceArray.count > 1, let additionalPrice = Float(priceArray[1]) {
                             tempERPPrice = tempERPPrice + additionalPrice
                             
                             if additionalPrice > 0.0 {
                                 var feature = Turf.Feature(geometry: .point(Point(intersectCoordinate)))
                                 
                                 feature.properties = [
                                    "cost":.string(stringPrice),
                                 ]
                                 allERPInSelectedRoute.append(feature)
                             }
                         }
                     }
                 }
             }
             
             return "$\(tempERPPrice.clean)"
         }else {
             return "$0.0"
         }
    }
    
    
    func newComputeTollsForCarPlayRP(route:Route,erpTolls:FeatureCollection) -> String {
        
        if(allERPInSelectedRoute.count > 0)
        {
            allERPInSelectedRoute.removeAll()
        }
        
        var erpCount = 0
        let defaultCost = String(format: "$%.2f", 0.00)
        let feature = [Turf.Feature(geometry: .lineString(LineString(route.shape!.coordinates)))]
        let routCollection = FeatureCollection(features: feature)
        
        guard case .lineString(_) = routCollection.features.first?.geometry else { return defaultCost}
        
        var tempERPPrice = Float()
        let routeLineSegment = routesAsLineSegments(route: route)
        
        //Looping for each erpTolls which is a LineString
        for erpToll in erpTolls.features{
            
            //Looping for each route Segment
            for routeSegment in routeLineSegment{
                
                //Getting geometry from erpToll
                guard case let .lineString(erpGeometry) = erpToll.geometry else { return defaultCost }
                
                //creating a LingSegment for E R P
                let erpLineSegment =  (erpGeometry.coordinates[0],erpGeometry.coordinates[1])
                
                ///Finding intersection with two Linesegments
                if let intersectCoordinate = intersection(routeSegment , erpLineSegment) {
                    print(erpToll)
                    
                    if case let .string(stringPrice) = erpToll.properties?["cost"],
                       case let .string(id) = erpToll.properties?["erpid"] {
                        
                        if let erpValues =  self.findERPIsInWithIn30m(erpID:Int(id) ?? 0){
                            
                            print("ERP Bound Values",erpValues)
                            erpCount = erpCount + 1
                            let priceArray = erpValues.currentPrice.components(separatedBy: "$")
                            
                            if priceArray.count > 1, let additionalPrice = Float(priceArray[1]) {
                                tempERPPrice = tempERPPrice + additionalPrice
                                
                                if additionalPrice > 0.0 {
                                    
                                    var feature = Turf.Feature(geometry: .point(Point(intersectCoordinate)))
                                    feature.properties = [
                                       "cost":.string(erpValues.currentPrice)
                                    ]
                                    allERPInSelectedRoute.append(feature)
                                    print("Current price: \(erpValues.currentPrice), cost: \(stringPrice)")
                                }
                            }
                        }
                    }
                }
            }
        }
        
        return "$\(tempERPPrice.clean)"
    }
    

    
    private func computeTolls(routes:[Route],erpTolls:FeatureCollection){
        
        //This is route generated from Directions API
        var erpCount = 0
        if(allERPInSelectedRoute.count > 0)
        {
            allERPInSelectedRoute.removeAll()
        }
        
        if(availableERPTollsOnNoNSelectRoute.count > 0)
        {
            availableERPTollsOnNoNSelectRoute.removeAll()
        }
        
        //Keeping default value of ERP to 0.0
        let defaultCost = String(format: "$%.2f", 0.00)
        self.delegate?.sendERPCost(cost: defaultCost)
        for route in routes{
            
            let feature = [Turf.Feature(geometry: .lineString(LineString(route.shape!.coordinates)))]
            let routCollection = FeatureCollection(features: feature)
            guard case .lineString(_) = routCollection.features[0].geometry else { return }
            var tempERPPrice = Float()
            let routeLineSegment = routesAsLineSegments(route: route)
            /*var routeLineSegment = [Any]()
             
             //Iterating to pair from the route coordinates
             for (left, right) in stride(from: 0, to: (routeGeometry?.coordinates.count)! - 1, by: 1)
             .lazy
             .map( { (routeGeometry?.coordinates[$0], routeGeometry?.coordinates[$0+1]) } ) {
             
             //creating a line segment with the pair of coordinate
             let lineSegment = (left,right)
             
             //Adding linesegment tp routeLineSegment Array
             routeLineSegment.append(lineSegment)
             
             }*/
            
            //Looping for each erpTolls which is a LineString
            for erpToll in erpTolls.features{
                
                //Looping for each route Segment
                for routeSegment in routeLineSegment{
                    
                    //Getting geometry from erpToll
                    guard case let .lineString(erpGeometry) = erpToll.geometry else { return }
                    //print("Toll",boothGeometry?.coordinates.latitude)
                    
                    //creating a LingSegment for E R P
                    let erpLineSegment =  (erpGeometry.coordinates[0],erpGeometry.coordinates[1])
                    
                    ///Finding intersection with two Linesegments
                    if let intersectCoordinate = intersection(routeSegment , (erpLineSegment as? LineSegment)!) {
                        print(erpToll)
                        
                        if case let .string(stringPrice) = erpToll.properties!["cost"],
                           case let .string(id) = erpToll.properties?["erpid"],
                           case let .string(name) = erpToll.properties?["name"]{
                            
                            if let erpValues =  self.findERPIsInWithIn30m(erpID:Int(id) ?? 0){
                                
                                print("ERP Bound Values",erpValues)
                                erpCount = erpCount + 1
                                let priceArray = erpValues.currentPrice.components(separatedBy: "$")
                                
                                if priceArray.count > 1, let additionalPrice = Float(priceArray[1]) {
                                    tempERPPrice = tempERPPrice + additionalPrice
                                    
                                    if(currentSelectedRoute == route)
                                    {
                                        if(additionalPrice > 0.0)
                                        {
                                            var feature = Turf.Feature(geometry: .point(Point(intersectCoordinate)))
                                            
                                            feature.properties = [
                                                "erpid": .string(id),
                                                "cost":.string(erpValues.currentPrice),
                                                "currentprice":.string(erpValues.currentPrice),
                                                "upcomingtime":.string(erpValues.starteAndEndTimeCombine),
                                                "upcomingprice":.string(erpValues.upcomingPrice),
                                                "boundValue":.string(erpValues.bound),
                                                "type":.string("ERP"),
                                                "name":.string(name),
                                                "erpOrder":.number(Double(erpCount)),
                                                "selectedTime": .string(erpValues.selectedTimeERP)
                                            ]
                                            
                                            allERPInSelectedRoute.append(feature)
                                            self.delegate?.sendERPCost(cost: "$\(tempERPPrice.clean)")
                                            
                                        }
                                        else
                                        {
                                            self.delegate?.sendERPCost(cost: "$\(tempERPPrice.clean)")
                                        }
                                        
                                    }
                                    else{ // for alternative route
                                        if(additionalPrice > 0.0)
                                        {
                                            var feature = Turf.Feature(geometry: .point(Point(intersectCoordinate)))
                                            
                                            feature.properties = [
                                                "erpid": .string(id),
                                                "cost":.string(stringPrice),
                                                "currentprice":.string(erpValues.currentPrice),
                                                "upcomingtime":.string(erpValues.starteAndEndTimeCombine),
                                                "upcomingprice":.string(erpValues.upcomingPrice),
                                                "boundValue":.string(erpValues.bound),
                                                "type":.string("ERP"),
                                                "erpOrder":.number(Double(erpCount)),
                                                "name":.string(name),
                                                "selectedTime": .string(erpValues.selectedTimeERP)

                                            ]
                                            availableERPTollsOnNoNSelectRoute.append(feature)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    func geCenterOfTheRouteLine(routeGeometry:LineString) -> CLLocationCoordinate2D{
        
        let bBox = try? BoundingBox(from: routeGeometry.coordinates)
        let longitude = ((bBox?.northEast.longitude ?? 0.0) + (bBox?.southWest.longitude ?? 0.0))/2
        let latitude = ((bBox?.northEast.latitude ?? 0.0) + (bBox?.southWest.latitude ?? 0.0))/2
        
        print(latitude)
        print(longitude)
        
        //Checking for Cheap Ruler insideBBox always return false
        for point in fixedBufferPointArray
        {
            print(point)
            let bool =  ruler?.insideBBox(p: (latitude,longitude), bbox: point) ?? false
            print(bool)
        }
        
        
        if let coord = routeGeometry.closestCoordinate(to: CLLocationCoordinate2D(latitude: latitude, longitude: longitude))?.coordinate {

            let coordinates = CLLocationCoordinate2D(latitude: coord.latitude, longitude: coord.longitude)
            
            
            return coordinates
        }
        
        return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
    
    func findERPIsInWithIn30m(erpID:Int) -> (bound:String, iconType:String,starteAndEndTimeCombine:String,currentPrice:String,upcomingPrice:String,selectedTimeERP:String)?{
        
        if let erpData = DataCenter.shared.getERPJsonData(){
            
            let publicHolidays = erpData.publicholiday
            
            let erp = erpData.erp

            var typeOfDay = ""
            if let currentERPTime = currentERPTime {
                typeOfDay = Date().isWeekend(date: currentERPTime)
            } else {
                typeOfDay = Date().isWeekend(date: Date())
            }
                        
            if(!checkERPPublicHoliday(publicHoliday: publicHolidays!,selectedDate: currentERPTime) && typeOfDay != ERPValues.isSunWeekend){
                
                for erpItem in erp ?? [] {
                    
                    if(erpItem.erpid == erpID){
                        
                        if let i = erpData.getERPRates().firstIndex(where: { $0.zoneid == erpItem.getZoneId() }) {
                            
                            if let currentERPTime = currentERPTime {
                                
                                let selectedDay = DateUtils.shared.exportDateString(format: Date.formatEEEE, date: currentERPTime)
                                let selectedTime = DateUtils.shared.exportDateString(format: Date.formatHHmm, date: currentERPTime)
                                if let values = returnERPBoundValueWithIconType(erpRates: erpData.getERPRates()[i].getRates(),selectedTime: selectedTime,selectedDay: selectedDay){
                                    
                                    return values
                                }
                                else if let inActiveValues = returnInActiveERPBoundValue(erpRates: erpData.getERPRates()[i].getRates(),selectedTime: selectedTime,selectedDay: selectedDay){
                                    
                                    return inActiveValues
                                }
                            }
                            else{
                                
                                if let values = returnERPBoundValueWithIconType(erpRates: erpData.getERPRates()[i].getRates()){
                                    
                                    return values
                                }
                                else if let inActiveValues = returnInActiveERPBoundValue(erpRates: erpData.getERPRates()[i].getRates()){
                                    
                                    return inActiveValues
                                }
                            }
                            
                        }
                    }
                }
                
            }
        }
        
        return nil
    }
    
    
    
}


typealias Points = (Double, Double)
typealias BufferPoint = (Double, Double, Double, Double)
typealias BoundingBoxSet = (Double, Double, Double, Double)
typealias Buffer = Double

// implementation
enum Factor: Double {
    case kilometers
    case miles
    case nauticalmiles
    case meters = 1000
    case yards
    case feet
    case inches
}

var factors: [Factor: Double] = [
    Factor.kilometers: 1,
    Factor.miles: (1000 / 1609.344),
    Factor.nauticalmiles: 1000 / 1852,
    Factor.meters: 1000,
    Factor.yards: 1000 / 0.9144,
    Factor.feet: 1000 / 0.3048,
    Factor.inches: 1000 / 0.0254,
]

class CheapRuler{
    
    var kx: Double
        var ky: Double
        
        init(lat: Double, units: Factor) {
            let m = factors[units]!
            
            let cos1 = cos(lat * Double.pi / 180)
            let cos2 = 2 * cos1 * cos1 - 1
            let cos3 = 2 * cos1 * cos2 - cos1
            let cos4 = 2 * cos1 * cos3 - cos2
            let cos5 = 2 * cos1 * cos4 - cos3
            
            self.kx = m * (111.41513 * cos1 - 0.09455 * cos3 + 0.00012 * cos5) // longitude correction
            self.ky = m * (111.13209 - 0.56605 * cos2 + 0.0012 * cos4)        // latitude correction
        }
        
        convenience init(y: Double, z: Double, units: Factor) {
            let n = Double.pi * (1 - 2 * (y + 0.5) / pow(2, z));
            let lat = atan(0.5 * (exp(n) - exp(-n))) * 180 / Double.pi;
            self.init(lat: lat, units: units)
        }
    
    func fixedPointBuffer(p: Points, buffer: Buffer) -> BufferPoint {
            let v = buffer / self.ky
            let h = buffer / self.kx
            return (
                p.0 - h,
                p.1 - v,
                p.0 + h,
                p.1 + v
            )
        }
    
    func dynamicPointBuffer(p: Points, buffer: Buffer) -> BufferPoint {
            let v = buffer / self.ky
            let h = buffer / self.kx
            return (
                p.0 - h,
                p.1 - v,
                p.0 + h,
                p.1 + v
            )
        }
    
    func insideBBox(p: Points, bbox: BoundingBoxSet) -> Bool {
            return (
                p.0 >= bbox.0 &&
                p.0 <= bbox.2 &&
                p.1 >= bbox.1 &&
                p.1 <= bbox.3
            )
        }
}

extension Array where Element == Route {

    mutating func sort() {
         return sort(by: { $0.expectedTravelTime < $1.expectedTravelTime})
    }

    func sorted() -> [Route] {
        return sorted(by: { $0.expectedTravelTime < $1.expectedTravelTime })
    }
    
    func sortedDistance() -> [Route] {
        return sorted(by: { $0.distance < $1.distance })
    }
    
    
}

extension Array where Element == NearByERPFilter {

    
    func sortedDistance() -> [NearByERPFilter] {
        return sorted(by: { $0.distance < $1.distance })
    }
    
    
}

extension MapView{
    
    func addERPBoundImagesToMapStyle(){
        
       // removERPBoundImagesFromMapStyle()
        do {
           
            var traitCollection = UITraitCollection(userInterfaceStyle: .light) //Default will be Light
            if(Settings.shared.theme == 0){ // Dark Theme
                
                if(appDelegate().isDarkMode()){
                    
                    traitCollection = UITraitCollection(userInterfaceStyle: .dark)
                    
                }
                
            }
            else{
                
                traitCollection = UITraitCollection(userInterfaceStyle: UIUserInterfaceStyle(rawValue: Settings.shared.theme) ?? .unspecified)
            }
            
            try self.mapboxMap.style.addImage(UIImage(named: "erp_up", in: nil, compatibleWith: traitCollection)!, id: Values.INCREASE, stretchX: [], stretchY: [])
            try self.mapboxMap.style.addImage(UIImage(named: "erp_down", in:nil, compatibleWith: traitCollection)!, id: Values.DECREASE, stretchX: [], stretchY: [])
            try self.mapboxMap.style.addImage(UIImage(named: "erp_grey_up", in:nil, compatibleWith: traitCollection)!, id: Values.INCREASE_GREY, stretchX: [], stretchY: [])
            try self.mapboxMap.style.addImage(UIImage(named: "erp_noarrow_active", in:nil, compatibleWith: traitCollection)!, id: Values.ACTIVE_NO_ARROW, stretchX: [], stretchY: [])
            try self.mapboxMap.style.addImage(UIImage(named: "erp_noarrow", in:nil, compatibleWith: traitCollection)!, id: Values.IN_ACTIVE_NO_ARROW, stretchX: [], stretchY: [])
            SwiftyBeaver.debug("Added ERP Image to map style.")
        }catch {
            SwiftyBeaver.error("Failed to Add ERP Image.")
        }
    }
    
    func removERPBoundImagesFromMapStyle(){
        
        do {
            try self.mapboxMap.style.removeImage(withId: Values.INCREASE)
            try self.mapboxMap.style.removeImage(withId: Values.INCREASE_GREY)
            try self.mapboxMap.style.removeImage(withId: Values.DECREASE)
            try self.mapboxMap.style.removeImage(withId: Values.DECREASE_GREY)
            try self.mapboxMap.style.removeImage(withId: Values.ACTIVE_NO_ARROW)
            try self.mapboxMap.style.removeImage(withId: Values.IN_ACTIVE_NO_ARROW)
        }catch {
            SwiftyBeaver.error("Failed to remove ERP Image.")
        }
    }
}

extension MapView{
    
    func removeRoutePlanningERPRouteLayer(){
        
        do {
            try self.mapboxMap.style.removeLayer(withId: "All")
            try self.mapboxMap.style.removeSource(withId: "GeoJsonAll")
            
        } catch {
            NSLog("Failed to remove main ERP route Layers.")
        }
    }
    
    func removeRoutePlanningERPRouteAlternateLayer(){
        
        do {
            try self.mapboxMap.style.removeLayer(withId: "AllAlternateERP")
            try self.mapboxMap.style.removeSource(withId: "GeoJsonAllAlternateERP")
        } catch {
            NSLog("Failed to remove main ERP alternate Layers.")
        }
    }
}

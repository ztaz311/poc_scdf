//
//  RatingVC.swift
//  Breeze
//
//  Created by Zhou Hao on 13/8/21.
//

import UIKit
import Instabug

final class RatingVC: UIViewController {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var lblRating: UILabel!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var topContraint: NSLayoutConstraint!
    
    // MARK: - Public properties
    var onDismissed: (() -> Void)?
    var tripGUID: String!   // must be passed by turn by turn navigation
    
    override func viewDidLoad() {
        super.viewDidLoad()

        ratingView.rating = 0
        self.lblRating.text = "Very poor"
        self.lblRating.isHidden = true
        btnSubmit.isEnabled = false
        btnSubmit.backgroundColor = .lightGray
        
        ratingView.didFinishTouchingCosmos = { [weak self] rating in
            guard let self = self else { return }
            
            self.lblRating.text = self.getRatingText(rating: rating)
            self.btnSubmit.isEnabled = rating > 0
            self.lblRating.isHidden = rating == 0
            self.btnSubmit.backgroundColor = rating > 0 ? UIColor.rgba(r: 120, g: 46, b: 177, a: 1) : .lightGray
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        AnalyticsManager.shared.logOpenPopupEvent(popupName: ParameterName.Navigation.UserPopUp.rating, screenName: ParameterName.Navigation.screen_view)
        
        if UIScreen.main.bounds.height > 780 {
            topContraint.constant = 200
        } else {
            topContraint.constant = 120
        }
    }
        
    private func getRatingText(rating: Double) -> String {
        if rating <= 1 {
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Navigation.UserClick.pop_star1, screenName: ParameterName.Navigation.screen_view)
            return "Very poor"
        } else if rating <= 2 {
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Navigation.UserClick.pop_star2, screenName: ParameterName.Navigation.screen_view)
            return "Poor"
        } else if rating <= 3 {
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Navigation.UserClick.pop_star3, screenName: ParameterName.Navigation.screen_view)
            return "Good"
        } else if rating <= 4 {
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Navigation.UserClick.pop_star4, screenName: ParameterName.Navigation.screen_view)
            return "Very good"
        }
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Navigation.UserClick.pop_star5, screenName: ParameterName.Navigation.screen_view)
        return "Excellent"
    }
    
    @IBAction func onSubmit(_ sender: Any) {
        // submit through API
        
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Navigation.UserClick.pop_rating_submit, screenName: ParameterName.Navigation.screen_view)
        RatingService().post(tripId: tripGUID, rating: Int(ratingView.rating)) { result in
            switch result {
            case .success(_):
                print("submit success")
            case .failure(let error):
                print(error.localizedDescription)
            }
            DispatchQueue.main.async {
                self.onClose(sender)
            }            
        }
    }
    
    @IBAction func onReport(_ sender: Any) {
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Navigation.UserClick.pop_instabug, screenName: ParameterName.Navigation.screen_view)
        Instabug.show()
    }
    
    @IBAction func onClose(_ sender: Any) {
        
        AnalyticsManager.shared.logClosePopupEvent(popupName: ParameterName.Navigation.UserPopUp.rating, screenName: ParameterName.Navigation.screen_view)
        
        
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Navigation.UserClick.pop_rating_close, screenName: ParameterName.Navigation.screen_view)
        
        removeChildViewController(self)
        if let callback = onDismissed {
            callback()
        }
    }
    
}

//
//  CarPlayNavigationMapViewUpdatable.swift
//  Breeze
//
//  Created by Zhou Hao on 13/10/21.
//

import Foundation
@_spi(Restricted) import MapboxMaps
import MapboxNavigation
import MapboxDirections
import MapboxCoreNavigation
import SwiftyBeaver
import CarPlay

final class CarPlayNavigationMapViewUpdatable: MapViewUpdatable {
    weak var navigationMapView: NavigationMapView?
    weak var delegate: MapViewUpdatableDelegate?
    
    private var cpFeatureImage:UIImage!
    private var cpFeatureNotificationAlert:CPNavigationAlert?
    private var cpFeatureTitleVariants = [String]()
    private var cpFeatureSubTitleVariants = [String]()
    private var cpOBUImage: UIImage!
    
    private var title: String = ""
    private var subTitle: String = ""
    
    
    var cpFeatureDisplay:FeatureDetectable?
    var cpFeatureDisplayClose = false
    private var roadNamePanel: RoadNamePanel?
    
    required init(navigationMapView: NavigationMapView) {
        self.navigationMapView = navigationMapView
        self.navigationMapView?.mapView.ornaments.options.attributionButton.visibility = .hidden
        if (!DemoSession.shared.parkAnyWhere){
            setupRoadNamePanel()
        }
        
    }
    
    func updateParkingAvailability(carpark: Carpark?) {
        //  TODO need to confirm if we need show parking status on Carplay
    }
    
    func updateNotificationWhenUserSoonArrive(carpark: Carpark?, message: String) {
        //  TODO need to confirm if we need show soon arriving CP in Carplay
    }
    
    func updateNotification(featureId: String?, currentFeature: FeatureDetectable?) {
        if let feature = currentFeature {
            
            if let cpFeatureDisplay = self.cpFeatureDisplay{
                
                //If already shown feature is equal to current incomingfeature and user hasn't closed the notification
                if(cpFeatureDisplay == feature && self.cpFeatureDisplayClose == false){
                    
                    self.cpFeatureDisplay = feature
                    showFirstFeature(feature)
                    showCPNotification(hide: false)
                    
                    //                    if feature is DetectedSchoolZone || feature is DetectedSilverZone {
                    //                        cpFeatureImage = UIImage(named: "speedIcon")!
                    //                    }
                }
                else  if(cpFeatureDisplay != feature){ //If previous feature not equal to current
                    
                    self.cpFeatureDisplay = feature
                    showFirstFeature(feature)
                    showCPNotification(hide: false)
                    
                    //                    if feature is DetectedSchoolZone || feature is DetectedSilverZone {
                    //                        cpFeatureImage = UIImage(named: "speedIcon")!
                    //                    }
                }
            }
            else{
                
                self.cpFeatureDisplay = feature
                showFirstFeature(feature)
                showCPNotification(hide: false)
                
                if feature is DetectedSchoolZone || feature is DetectedSilverZone {
                    cpFeatureImage = UIImage(named: "speedIcon")!
                }
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + Values.notificationDuration) { [weak self] in
                guard let self = self else { return }
                self.showCPNotification(hide: true)
            }
            
        }
        //         else {
        //            showCPNotification(hide: true)
        //        }
    }
    
    func updateMuteStatus(isEnabled:Bool){
        
        NavigationSettings.shared.voiceMuted = isEnabled
        if(appDelegate().carPlayMapController != nil)
        {
            appDelegate().carPlayMapController?.updateCPCruiseToggleTitle(activity: .navigating,isEnabled:isEnabled)
        }
    }
    
    
    func updateTunnel(isInTunnel: Bool) {
        if isInTunnel {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) { [weak self] in
                if let navMapView = self?.navigationMapView {
                    let offset: CGFloat = 5
                    showToast(from: navMapView, message: Constants.toastInTunnel, topOffset: offset + 60, icon: "noInternetIcon", duration: 0)
                }
            }
        } else {
            DispatchQueue.main.async { [weak self] in
                if let navMapView = self?.navigationMapView {
                    hideToast(from: navMapView)
                }
            }
        }
    }
    
    // The initial intention of MapViewUpdatable is only for MapLandingVC. But we found it's easy to use it in navigation also.
    // But some functions such as this one is not common. So I just ignore it.
    // The best solution is taking out the functions like this and add child class for MapLanding and also Navigation.
    // TODO: Refactor is possible
    func cruiseStatusUpdate(isCruise: Bool) {
        //No need to handle this
    }
    
    func initERPUpdater(response: RouteResponse?) {
        //No need to handle this
    }
    
    func updateETA(eta: TripCruiseETA?) {
        //No need to handle this
    }
    
    func updateETA(eta: TripETA?) {
        // Carplay update top menu button
        if appDelegate().carPlayMapController != nil {
            appDelegate().carPlayMapController?.updateCPCruiseToggleTitle(activity: .navigating, isEnabled:NavigationSettings.shared.voiceMuted, isLiveLoc: eta != nil)
        }
    }
    
    func shareDriveButtonEnabled(_ isEnabled: Bool){
    }
    
    func updateLiveLocationEnabled(_ isEnabled: Bool) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) { [weak self] in
            guard let self = self else { return }
            
            if let navMapView = self.navigationMapView {
                let offset: CGFloat = 5.0
                if isEnabled {
                    showToast(from: navMapView, message: Constants.toastLiveLocationResumed, topOffset: offset, icon: "toastLiveLocationResumed", messageColor: .black, duration: 3)
                } else {
                    showToast(from: navMapView, message: Constants.toastLiveLocationPaused, topOffset: offset, icon: "toastLiveLocationPaused", messageColor: .black, duration: 3)
                }
            }
        }
    }
    
    func showETAMessage(recipient: String, message: String) {
        let offset: CGFloat = 110
        
        let imageWithText = UIImage.generateImageWithText(text: getAbbreviation(name: recipient), font: UIFont(name: fontFamilySFPro.Bold, size: 20.0)!)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) { [weak self] in
            guard let self = self else { return }
            if let mapView = self.navigationMapView?.mapView  {
                showToast(from: mapView, message: message, topOffset: offset, image: imageWithText, messageColor: .black, font: UIFont(name: fontFamilySFPro.Regular, size: 20.0)!, duration: 3)
            }
        }
    }
    
    func showNotifyToastMessage(message:String){
        
        let offset: CGFloat = 5
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) { [weak self] in
            guard let self = self else { return }
            if let mapView = self.navigationMapView?.mapView {
                showToast(from: mapView, message: message, topOffset: offset, icon: "toastLiveLocationPaused", messageColor: .black, duration: 3)
            }
        }
        
    }
    
    func updateRoadName(_ name: String) {
        showRoad(name)
    }
    
    func updateSpeedLimit(_ value: Double) {
        //No need to handle this
    }
    
    func overSpeed(value: Bool) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        
        appDelegate.navigationOverSpeed(value: value)
    }
    
    // MARK: OBU FEATURE
    func displayOBUNotificationTravelTime(_ listModel: [TravelTime]) {
        
        if !appDelegate().carPlayConnected {
            return
        }
        
        let action = CPAlertAction(title: "See More",
                                   style: .default,
                                   handler: {_ in
            //            self.cpFeatureDisplayClose = true
            //            self.cpFeatureNotificationAlert = nil
            appDelegate().showTravelTimeList(listModel)
            if OBUHelper.shared.getObuMode() == .device {
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CarPlayObuState.UserClick.dhu_popup_navigation_notification_click_notification, screenName: ParameterName.CarPlayObuState.dhu_navigation)
            }
        })
        
        if let first = listModel.first {
            
            //            let title = first.location
            //            let subTitle = first.min
            let icon = UIImage(named: first.getIconName())
            
            cpFeatureNotificationAlert = CPNavigationAlert(titleVariants: ["Travel Time Alert"], subtitleVariants: [""], image: icon, primaryAction: action, secondaryAction: nil, duration: 0)
            getRootMapTemplate()?.present(navigationAlert: cpFeatureNotificationAlert!, animated: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + Constants.timeDismiss) {
                //                appDelegate().tryToDismissTemplateIfNeed()
                let mapTemplate = self.getRootMapTemplate()
                if(mapTemplate != nil) {
                    if(self.cpFeatureNotificationAlert != nil)
                    {
                        mapTemplate?.dismissNavigationAlert(animated: true) { dismissed in
                            self.cpFeatureNotificationAlert = nil
                        }
                    }
                }
                if OBUHelper.shared.getObuMode() == .device {
                    AnalyticsManager.shared.logClosePopupEventV2Carplay(popupName: ParameterName.ObuPairing.UserPopup.dhu_popup_close, screenName: ParameterName.CarPlayObuState.dhu_navigation)
                }
            }
        }
       
    }
    
    func displayOBUNotificationCarPark(_ listModel: [Parking], item: ParkingInfoItem?){
        
        if !appDelegate().carPlayConnected {
            return
        }
        
        let action = CPAlertAction(title: "See More",
                                   style: .default,
                                   handler: {_ in
            appDelegate().showCarparkList(listModel)
            if OBUHelper.shared.getObuMode() == .device {
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CarPlayObuState.UserClick.dhu_popup_navigation_notification_click_notification, screenName: ParameterName.CarPlayObuState.dhu_navigation)
            }
        })
        
        if let parking = item {
            
            let subTitle = parking.getSubTitle()
            let icon = UIImage(named: parking.getIconNameCarplay())
            
            cpFeatureNotificationAlert = CPNavigationAlert(titleVariants: ["Parking \nUpdate"], subtitleVariants: [subTitle], image: icon, primaryAction: action, secondaryAction: nil, duration: 0)
            getRootMapTemplate()?.present(navigationAlert: cpFeatureNotificationAlert!, animated: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + Constants.timeDismiss) {
                //                appDelegate().tryToDismissTemplateIfNeed()
                let mapTemplate = self.getRootMapTemplate()
                if(self.cpFeatureNotificationAlert != nil)
                {
                    mapTemplate?.dismissNavigationAlert(animated: true) { dismissed in
                        self.cpFeatureNotificationAlert = nil
                    }
                }
                
                if OBUHelper.shared.getObuMode() == .device {
                    AnalyticsManager.shared.logClosePopupEventV2Carplay(popupName: ParameterName.ObuPairing.UserPopup.dhu_popup_close, screenName: ParameterName.CarPlayObuState.dhu_navigation)
                }
            }
        }
        
//        if let first = listModel.first {
//
//            let subTitle = first.getSubTitle()
//            let icon = UIImage(named: first.getIconName())
//
//            cpFeatureNotificationAlert = CPNavigationAlert(titleVariants: ["Parking \nUpdate"], subtitleVariants: [subTitle], image: icon, primaryAction: action, secondaryAction: nil, duration: 0)
//            getRootMapTemplate()?.present(navigationAlert: cpFeatureNotificationAlert!, animated: true)
//            DispatchQueue.main.asyncAfter(deadline: .now() + Constants.timeDismiss) {
//                //                appDelegate().tryToDismissTemplateIfNeed()
//                let mapTemplate = self.getRootMapTemplate()
//                mapTemplate?.dismissNavigationAlert(animated: true) { dismissed in
//                    self.cpFeatureNotificationAlert = nil
//                }
//                if OBUHelper.shared.getObuMode() == .device {
//                    AnalyticsManager.shared.logClosePopupEventV2Carplay(popupName: ParameterName.ObuPairing.UserPopup.dhu_popup_close, screenName: ParameterName.CarPlayObuState.dhu_navigation)
//                }
//            }
//
//        }
    }
    
    //    func showsCPNotificationObuERP(_ listCharging: [BreezeObuChargingInfo]){
    //        if !appDelegate().carPlayConnected {
    //            return
    //        }
    //
    //        let action = CPAlertAction(title: "Close",
    //                                   style: .default,
    //                                   handler: {_ in
    //            self.cpFeatureDisplayClose = true
    //            self.cpFeatureNotificationAlert = nil
    //        })
    //
    //        if let first = listCharging.first {
    //
    //            let title = first.content1 ?? ""
    //            let subTitle = first.getType()
    //            let icon = UIImage(named: "cpERPIcon")
    //
    //            cpFeatureNotificationAlert = CPNavigationAlert(titleVariants: [title], subtitleVariants: [subTitle], image: icon, primaryAction: action, secondaryAction: nil, duration: 0)
    //            getRootMapTemplate()?.present(navigationAlert: cpFeatureNotificationAlert!, animated: true)
    //            DispatchQueue.main.asyncAfter(deadline: .now() + Constants.timeDismiss) {
    //                //                appDelegate().tryToDismissTemplateIfNeed()
    //                let mapTemplate = self.getRootMapTemplate()
    //                mapTemplate?.dismissNavigationAlert(animated: true) { dismissed in
    //                    self.cpFeatureNotificationAlert = nil
    //                }
    //            }
    //        }
    //
    //    }
    
    func showsCPNotificationObuERP(_ charging: BreezeObuChargingInfo? = nil, eventDic: [String: Any]? = nil){
        
        if !appDelegate().carPlayConnected {
            return
        }
        
        let action = CPAlertAction(title: "Close",
                                   style: .default,
                                   handler: {_ in
            self.cpFeatureDisplayClose = true
            self.cpFeatureNotificationAlert = nil
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CarPlayObuState.UserClick.dhu_popup_navigation_notification_close, screenName: ParameterName.CarPlayObuState.dhu_navigation)
        })
        
        var title = ""
        var subTitle = ""
        let icon = UIImage(named: "carplay-erpIcon")
        if let notiDic = eventDic {
            if let type = eventDic?["type"] as? String {
                if type == "ERP_CHARGING" {
                    title = notiDic["detailMessage"] as? String ?? ""
                    subTitle = notiDic["distance"] as? String ?? ""
                } else {
                    title = notiDic["detailMessage"] as? String ?? ""
                    subTitle = notiDic["message"] as? String ?? ""
                }
            }
            
        }else if let first = charging {
            title = first.content1 ?? ""
            subTitle = first.getMessage()
        }
        
        if !title.isEmpty || !subTitle.isEmpty {
            cpFeatureNotificationAlert = CPNavigationAlert(titleVariants: [formatTitleString(from: title)], subtitleVariants: [subTitle], image: icon, primaryAction: action, secondaryAction: nil, duration: 0)
            getRootMapTemplate()?.present(navigationAlert: cpFeatureNotificationAlert!, animated: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + Constants.timeDismiss) {
                //                appDelegate().tryToDismissTemplateIfNeed()
                let mapTemplate = self.getRootMapTemplate()
                if(self.cpFeatureNotificationAlert != nil)
                {
                    mapTemplate?.dismissNavigationAlert(animated: true) { dismissed in
                        self.cpFeatureNotificationAlert = nil
                    }
                }
                AnalyticsManager.shared.logClosePopupEventV2Carplay(popupName: ParameterName.ObuPairing.UserPopup.dhu_popup_close, screenName: ParameterName.CarPlayObuState.dhu_navigation)
            }
        }
       
    }
    
    func addCarParkOnMap(carpark: [Carpark]) {
        if let carplayNavigationMapView = appDelegate().navigationViewModel?.carPlayMapViewUpdatable?.navigationMapView {
            if let location = self.navigationMapView?.mapView.location.latestLocation {
                let currentLocation = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
                if let updater = appDelegate().navigationViewModel?.carParkUpdater {
                    updater.removeCarParks(keepDestination: true)
                    carplayNavigationMapView.addCarparks(carpark, ignoreDestination: updater.shouldIgnoreDestination, showOnlyWithCostSymbol: true, finalDestCoord: currentLocation,
                                                        belowPuck: false)
                }
            }
        }
    }
    
    
    
    func showsCPNotificationSchoolZoneTraffic(title:String,subTitle:String, icon:UIImage?,timeDismiss:Int = Int(5.0)){
        if !appDelegate().carPlayConnected {
            return
        }
        
        let action = CPAlertAction(title: "Close",
                                   style: .default,
                                   handler: {_ in
            self.cpFeatureDisplayClose = true
            self.cpFeatureNotificationAlert = nil
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CarPlayObuState.UserClick.dhu_popup_navigation_notification_close, screenName: ParameterName.CarPlayObuState.dhu_navigation)
        })
        
        let title1 = formatTitleString(from: title)
        
        cpFeatureNotificationAlert = CPNavigationAlert(titleVariants: [title1], subtitleVariants: [subTitle], image: icon, primaryAction: action, secondaryAction: nil, duration: 0)
        getRootMapTemplate()?.present(navigationAlert: cpFeatureNotificationAlert!, animated: true)
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(timeDismiss)) {
            //            appDelegate().tryToDismissTemplateIfNeed()
            let mapTemplate = self.getRootMapTemplate()
            if(self.cpFeatureNotificationAlert != nil)
            {
                mapTemplate?.dismissNavigationAlert(animated: true) { dismissed in
                    self.cpFeatureNotificationAlert = nil
                }
            }
            AnalyticsManager.shared.logClosePopupEventV2Carplay(popupName: ParameterName.ObuPairing.UserPopup.dhu_popup_close, screenName: ParameterName.CarPlayObuState.dhu_navigation)
        }
        
    }
    
    
    func showsCPNotificationObuTraffic(title:String,subTitle:String){
        if !appDelegate().carPlayConnected {
            return
        }
        
        let action = CPAlertAction(title: "Close",
                                   style: .default,
                                   handler: {_ in
            self.cpFeatureDisplayClose = true
            self.cpFeatureNotificationAlert = nil
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CarPlayObuState.UserClick.dhu_popup_navigation_notification_close, screenName: ParameterName.CarPlayObuState.dhu_navigation)
        })
        let icon = UIImage(named: "cpAccidentIcon")
        let title1 = formatTitleString(from: title)
        
        cpFeatureNotificationAlert = CPNavigationAlert(titleVariants: [title1], subtitleVariants: [subTitle], image: icon, primaryAction: action, secondaryAction: nil, duration: 0)
        getRootMapTemplate()?.present(navigationAlert: cpFeatureNotificationAlert!, animated: true)
        DispatchQueue.main.asyncAfter(deadline: .now() + Constants.timeDismiss) {
            //            appDelegate().tryToDismissTemplateIfNeed()
            let mapTemplate = self.getRootMapTemplate()
            if(mapTemplate != nil) {
                if(self.cpFeatureNotificationAlert != nil)
                {
                    mapTemplate?.dismissNavigationAlert(animated: true) { dismissed in
                        self.cpFeatureNotificationAlert = nil
                    }
                }
                
            }
            
            AnalyticsManager.shared.logClosePopupEventV2Carplay(popupName: ParameterName.ObuPairing.UserPopup.dhu_popup_close, screenName: ParameterName.CarPlayObuState.dhu_navigation)
            
        }
    }
    
    
    private func showsCPNotificationSingleAction(mapTemplate:CPMapTemplate?){
        
        let action = CPAlertAction(title: Constants.cpNavAlert,
                                   style: .default,
                                   handler: {_ in
            
            self.cpFeatureDisplayClose = true
            self.cpFeatureNotificationAlert = nil
        })
        cpFeatureNotificationAlert = CPNavigationAlert(titleVariants: cpFeatureTitleVariants, subtitleVariants: cpFeatureSubTitleVariants, image: cpFeatureImage, primaryAction: action, secondaryAction: nil, duration: 0)
        
        print("IF NAVIGATION ALERT IS NOT NIL OTHER THAN ERP %@", cpFeatureTitleVariants,cpFeatureSubTitleVariants)
        mapTemplate?.present(navigationAlert: cpFeatureNotificationAlert!, animated: true)
        
    }
    
    // TODO: Make it reusable too
    private func showCPNotification(hide:Bool){
        
        let mapTemplate = getRootMapTemplate()
        if(mapTemplate != nil)
        {
            if(hide)
            {
                
                if(self.cpFeatureNotificationAlert != nil)
                {
                    print("DISMISS NAVIGATION ALERT")
                    mapTemplate?.dismissNavigationAlert(animated: true) { dismissed in
                        self.cpFeatureNotificationAlert = nil
                    }
                }
                
            }
            else
            {
                
                if(cpFeatureNotificationAlert == nil)
                {
                    print("CHECKING IF NAVIGATION ALERT IS NOT NIL")
#if DEMO
                    if(DemoSession.shared.isDemoRoute && DemoSession.shared.status == .inFirstLeg)
                    {
                        if let feature = self.cpFeatureDisplay as? DetectedERP
                        {
                            print("DIFFERENT NAVIGATION ALERT FOR ERP",feature)
                            if(feature.id == "2")
                            {
                                print("DIFFERENT NAVIGATION ALERT FOR ERP INSIDE",feature)
                                let action1 = CPAlertAction(title: Constants.cpNavAlert,
                                                            style: .destructive,
                                                            handler: {_ in
                                    
                                    self.cpFeatureDisplayClose = true
                                    self.cpFeatureNotificationAlert = nil
                                })
                                
                                let action = CPAlertAction(title: Constants.okay,
                                                           style: .default,
                                                           handler: {_ in
                                    
                                    self.cpFeatureDisplayClose = true
                                    self.cpFeatureNotificationAlert = nil
                                })
                                cpFeatureNotificationAlert = CPNavigationAlert(titleVariants: cpFeatureTitleVariants, subtitleVariants: cpFeatureSubTitleVariants, image: cpFeatureImage, primaryAction: action, secondaryAction: action1, duration: 0)
                                
                                mapTemplate?.present(navigationAlert: cpFeatureNotificationAlert!, animated: true)
                            }
                            else{
                                
                                self.showsCPNotificationSingleAction(mapTemplate: mapTemplate)
                            }
                        }
                        else{
                            
                            self.showsCPNotificationSingleAction(mapTemplate: mapTemplate)
                        }
                    }
                    else{
                        
                        self.showsCPNotificationSingleAction(mapTemplate: mapTemplate)
                    }
#else
                    self.showsCPNotificationSingleAction(mapTemplate: mapTemplate)
#endif
                    
                }
                else
                {
                    cpFeatureNotificationAlert?.updateTitleVariants(cpFeatureTitleVariants, subtitleVariants: cpFeatureSubTitleVariants)
                    
                }
                
            }
        }
        
        
        //}
    }
    
    private func showFirstFeature(_ feature: FeatureDetectable) {
        let distance = FeatureNotification.getDistance(for: feature, isCarPlay: true)
        let distanceText = distance
        let typeText = FeatureNotification.getTypeText(for: feature,isCarPlay: true)
        
        SwiftyBeaver.debug("\(feature), distance = \(distance), type = \(typeText)")
        
        print("SHOW FIRST FEATURE CALLED MULTIPLE TIMES %@",feature)
        
        
        //Setting Title variants for Car Play Notification
        cpFeatureTitleVariants = []
        cpFeatureSubTitleVariants = []
        cpFeatureTitleVariants.append(typeText)
        cpFeatureSubTitleVariants.append(distanceText)
        
        cpFeatureImage = FeatureNotification.getTypeImage(feature, isCarPlay: true)
        
#if DEMO
        cpFeatureTitleVariants = []
        cpFeatureSubTitleVariants = []
        if(DemoSession.shared.isDemoRoute || DemoSession.shared.parkAnyWhere)
        {
            if feature is DetectedSchoolZone || feature is DetectedSilverZone {
                
                cpFeatureSubTitleVariants = []
                cpFeatureSubTitleVariants.append("")
            }
            else if let feature = feature as? DetectedERP {
                
                print("DETCTED FEATURE",feature)
                if(feature.id == "36")
                {
                    print("DETCTED FEATURE INSIDE",feature)
                    cpFeatureTitleVariants = []
                    cpFeatureSubTitleVariants = []
                    cpFeatureTitleVariants.append("Paid")
                    cpFeatureSubTitleVariants.append("$\(String(format: "%.2f", feature.price))")
                    
                }
                else{
                    
                    print("DETCTED FEATURE ERP",feature)
                    cpFeatureTitleVariants = []
                    cpFeatureSubTitleVariants = []
                    cpFeatureTitleVariants.append(feature.address)
                    cpFeatureSubTitleVariants.append("$\(String(format: "%.2f", feature.price))")
                }
            }
            else if let feature = feature as? DetectedTrafficIncident {
                
                cpFeatureTitleVariants = []
                cpFeatureSubTitleVariants = []
                if(feature.category == Values.wayPointSpeed)
                {
                    print("DETCTED FEATURE SPEED",feature)
                    cpFeatureTitleVariants.append("Speed limit")
                    cpFeatureSubTitleVariants.append(feature.message)
                    
                }
                else
                {
                    if(feature.category == Values.wayPointRoad)
                    {
                        print("DETCTED FEATURE ROADSIDE",feature)
                        cpFeatureTitleVariants.append("Jalan Pelepah")
                        cpFeatureSubTitleVariants.append("\(DemoSession.shared.carParkCapacity) parking lots")
                        
                    }
                    else if(feature.category == Values.wayPointCarparkLots)
                    {
                        print("DETCTED FEATURE MORE CARPARKLOTS LEG2",feature)
                        cpFeatureTitleVariants.append("Destination")
                        cpFeatureSubTitleVariants.append("470 parking lots")
                        
                    }
                    else if(feature.category == Values.wayPointETAExpress){
                        
                        cpFeatureTitleVariants.append("Public message")
                        cpFeatureSubTitleVariants.append("5 min | Faber Park \n 8 min | Sentosa")
                    }
                    else if(feature.category == Values.wayPointCarParkLevel3)
                    {
                        print("DETCTED FEATURE LEVLEL3 LEG2",feature)
                        cpFeatureTitleVariants.append("Destination")
                        cpFeatureSubTitleVariants.append("for more available parking lots")
                        
                        
                    } else if(feature.category == Values.wayPointCyclist1 || feature.category == Values.wayPointCyclist2) {
                        
                        print("DETCTED FEATURE CYCLISTS",feature)
                        cpFeatureTitleVariants.append("Cyclists")
                        cpFeatureSubTitleVariants.append("200m")
                        
                    }
                    
                    else
                    {
                        print("DETCTED FEATURE ACCIDENT",feature)
                        cpFeatureTitleVariants.append("Accident")
                        //cpFeatureSubTitleVariants.append("")
                        
                    }
                }
            }
            else if let feature = feature as? DetectedETANotificationView {
                
                if(feature.category == Values.carPark)
                {
                    print("DETCTED FEATURE AUTO CARPARK",feature)
                    cpFeatureTitleVariants = []
                    cpFeatureSubTitleVariants = []
                    cpFeatureSubTitleVariants.append("\(feature.message)")
                    //                        if(feature.message.contains("entered"))
                    //                        {
                    //                            cpFeatureSubTitleVariants.append("\(featue.message)")
                    //                        }
                    //
                    //                        if(feature.message.contains("ended"))
                    //                        {
                    //                            cpFeatureSubTitleVariants.append("\(feature.recipientName)")
                    //                        }
                    //
                    //                        if(feature.message.contains("started"))
                    //                        {
                    //                            cpFeatureSubTitleVariants.append("\(feature.message)")
                    //                        }
                    
                }
                else
                {
                    print("DETCTED FEATURE NOTIFY",feature)
                    cpFeatureTitleVariants = []
                    cpFeatureSubTitleVariants = []
                    cpFeatureTitleVariants.append(feature.message)
                    cpFeatureSubTitleVariants.append(feature.recipientName)
                }
                
            }
            
        }
#endif
        
    }
    
    private func getRootMapTemplate() -> CPMapTemplate? {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate,
              let mapTemplate = appDelegate.carPlayManager.interfaceController?.rootTemplate else { return nil }
        
        return mapTemplate as? CPMapTemplate
    }
    
    private func setupRoadNamePanel() {
        if self.roadNamePanel == nil {
            let frame = appDelegate().roadNamePanelFrame()
            //            roadNamePanel = RoadNamePanel(frame: CGRect(x: 0, y: 0, width: 170, height: 30))
            roadNamePanel = RoadNamePanel(frame: frame)
            navigationMapView?.addSubview(roadNamePanel!)
            navigationMapView?.bringSubviewToFront(roadNamePanel!)
            let minW = UIScreen.main.bounds.width > 400 ? 220 : 190
            
            if let navMapView = self.navigationMapView {
                roadNamePanel?.translatesAutoresizingMaskIntoConstraints = false
                roadNamePanel?.snp.makeConstraints { (make) in
                    make.bottom.equalTo(navMapView.snp.bottom).offset(-5)
                    make.width.greaterThanOrEqualTo(170)
                    make.width.lessThanOrEqualTo(minW)
                    make.height.equalTo(40)
                    make.leading.equalToSuperview().offset(frame.origin.x)
                    
                }
            }
            roadNamePanel?.label.font = UIFont.carPlayRoadNameBoldFont()
            
            updateRoadPanelConstraint(isHidden: true)
            roadNamePanel?.setNeedsLayout()
            roadNamePanel?.layoutIfNeeded()
        }
    }
    
    private func updateRoadPanelConstraint(isHidden: Bool) {
        let frame = appDelegate().roadNamePanelFrame()
        if let navMapView = self.navigationMapView {
            roadNamePanel?.snp.updateConstraints { (make) in
                make.leading.equalToSuperview().offset(frame.origin.x)
                make.bottom.equalTo(navMapView.snp.bottom).offset(isHidden ? 40 : -5)
            }
        }
    }
    
    private func showRoad(_ name: String) {
        guard let roadNamePanel = self.roadNamePanel else { return }
        
        let hide = name.isEmpty
        //        let needUpdate = roadNamePanel.text() != name
        let needUpdate = true
        roadNamePanel.set(name)
        
        // animation
        if needUpdate {
            UIView.animate(withDuration: Values.standardAnimationDuration) {
                [weak self] in
                guard let self = self else { return }
                self.updateRoadPanelConstraint(isHidden: hide)
            }
        }
    }
    
    func formatTitleString(from responseString: String) -> String {
        let components = responseString.split(separator: " ")
        if #available(iOS 16.1, *) {
            if components.count >= 2 {
                let firstComponent = String(components[0])
                let secondComponent = components.dropFirst().joined(separator: " ")
                
                return "\(firstComponent)\n\(secondComponent)"
            } else {
                return responseString
            }
        } else {
            return responseString
        }
    }
    
    
    func adjustDynamicZoomLevelWhenCarParkDisplay
    (_ coordinates: [CLLocationCoordinate2D], currentLocation: CLLocationCoordinate2D) {
        
        guard let navMapView = navigationMapView else {return}
        
        var allCoordinates: [CLLocationCoordinate2D] = []
        allCoordinates.append(currentLocation)
        allCoordinates.append(contentsOf: coordinates)
        let padding = appDelegate().getVisiblePadding()
        let bearing = navMapView.mapView.cameraState.bearing
        
        let cameraOptions = navMapView.mapView.mapboxMap.camera(for: allCoordinates,
                                                                                padding: padding,
                                                                                bearing: bearing,
                                                                                pitch: 0)
        navMapView.mapView.camera.ease(to: cameraOptions, duration: 0.4, curve: .easeInOut, completion: nil)
    }
    
}

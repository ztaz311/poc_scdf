//
//  NavProgressVC.swift
//  Breeze
//
//  Created by Zhou Hao on 17/3/21.
//

import UIKit
import CoreLocation
import MapboxCoreNavigation
import MapboxDirections
import MapboxNavigation
import CoreLocation

protocol NavProgressVCDelegate: AnyObject {
    func onEnd() // end button clicked
    func onConnectOBUFailure()
    func onConnectOBUSuccess()
    func onShowCarparkList()
    func onShareDestination()
}

final class NavProgressVC: ContainerViewController {

    @IBOutlet weak var totalDistance: TwoTextLable!
    @IBOutlet weak var totalTravellingTime: TwoTextLable!
    @IBOutlet weak var estimateArriveTime: TwoTextLable!
    @IBOutlet weak var endButton: UIButton!
    @IBOutlet weak var progressSlider: UISlider!
    
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var obuPlaceholderView: UIView!
    @IBOutlet weak var obuPlaceholderHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var progressView: UIView!
    
    private var obuConnectionView: OBUConnectView!
    
    @IBOutlet weak var showCarparkListButton: UIButton!
    @IBOutlet weak var shareDestButton: UIButton!
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var carparkListHeightConstraint: NSLayoutConstraint!
        
    weak var navigationViewController: NavigationViewController?
    weak var delegate: NavProgressVCDelegate?
    var isNavigation: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if OBUHelper.shared.hasLastObuData() {
            NotificationCenter.default.addObserver(self, selector: #selector(onOBUConnectionUpdates), name: .obuConnectionUpdates, object: nil)
        }
        NotificationCenter.default.addObserver(self, selector: #selector(onShowCarparkListUpdates), name: .onShowCarparkListUpdates, object: nil)
        
        //  Set carpark list border
        let color = UIColor(hexString: "#782EB1", alpha: 1.0)
        showCarparkListButton.roundedBorder(cornerRadius: 24.0, borderColor: color.cgColor, borderWidth: 1.0)
        shareDestButton.roundedBorder(cornerRadius: 24.0, borderColor: color.cgColor, borderWidth: 1.0)
        
        if !isNavigation {
            separatorView.isHidden = true
            showCarparkListButton.isHidden = true
            shareDestButton.isHidden = true
            carparkListHeightConstraint.constant = 0
        }
        
        if isNavigation {
            self.addObuConnectionViewIfNeeded()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        self.view.roundCorners([.topLeft, .topRight], radius: 30)
        self.view.shadowToTopOfView()
        
        self.progressView.roundCorners([.topLeft, .topRight], radius: 30)
        
        // setup slider style
        progressSlider.minimumTrackTintColor = UIColor(named: "navBottomProgressMinColor")
        progressSlider.maximumTrackTintColor = UIColor(named: "navBottomProgressMaxColor")
        
        progressSlider.isUserInteractionEnabled = false
        
        progressSlider.setThumbImage(UIImage(), for: .normal)
        
//        self.endButton.applyGradient(colors: [UIColor.gradient3.cgColor, UIColor.gradient1.cgColor], withShadow: false)
        
        heightConstraint.constant = 90
        
        setupDarkLightAppearance()
        setupGradientColor()
                
        if isNavigation {
            self.updateUI(false)
        } else {
            self.updateUI(true)
        }
    }
    
    @objc func onOBUConnectionUpdates(_ notification: Notification) {
        DispatchQueue.main.async {
            if let isEndNavigation = notification.object as? Bool {
                self.updateUI(isEndNavigation)
            }
        }
    }
    
    @objc func onShowCarparkListUpdates(_ notification: Notification) {
        DispatchQueue.main.async {
            if let isShow = notification.object as? Bool {
                self.showCarparkListButton(isShow)
            }
        }
    }
    
    func showCarparkListButton(_ isShow: Bool) {
        showCarparkListButton.isHidden = !isShow
        shareDestButton.isHidden = !isShow
        separatorView.isHidden = !isShow
        if !isShow {
            carparkListHeightConstraint.constant = 0
        } else {
            carparkListHeightConstraint.constant = 78
        }
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
    }
    
    func updateUI(_ isEndNavigation: Bool) {
        
        if OBUHelper.shared.hasLastObuData() && !isEndNavigation {
            let isObuConnected = OBUHelper.shared.isObuConnected()
                    
            if isObuConnected {
                topConstraint.constant = 40
                obuPlaceholderHeightConstraint.constant = 30 + 40
            } else {
                topConstraint.constant = 64
                obuPlaceholderHeightConstraint.constant = 30 + 60
            }
            
            self.obuConnectionView?.updateUI()
        } else {
            topConstraint.constant = 2
            obuPlaceholderHeightConstraint.constant = 0
        }
        
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
    }
    
    private func addObuConnectionViewIfNeeded() {
        
        if OBUHelper.shared.hasLastObuData() {
            obuConnectionView = OBUConnectView(frame: .zero)
            obuPlaceholderView.addSubview(obuConnectionView)
            
            obuConnectionView.snp.makeConstraints { make in
                make.top.bottom.leading.trailing.equalToSuperview()
            }
            obuConnectionView.updateUI()
            
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
            
            obuConnectionView.callback = {[weak self] success in
                if !success {
                    //  Send notification to show alert connection error
                    self?.delegate?.onConnectOBUFailure()
                } else {
                    self?.updateUI(false)
                    self?.delegate?.onConnectOBUSuccess()
                }
            }
        }
    }
    
    @IBAction func onEnd(_ sender: Any) {
        
        if isNavigation {
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Navigation.UserClick.end_navigation, screenName: ParameterName.Navigation.screen_view)
        } else {
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.NavigationMode.UserClick.walking_guide_end_trip, screenName: ParameterName.NavigationMode.walking_guide_screen)
        }
        
        // add prompt?
        self.popupAlert(title: nil, message: Constants.endNavigationMessage, actionTitles: [Constants.cancel,Constants.end], actions:[{action1 in
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Navigation.UserClick.pop_user_end_no, screenName: ParameterName.Navigation.screen_view)
            
        },{ [weak self] action2 in
            guard let self = self else { return }
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Navigation.UserClick.pop_user_end_yes, screenName: ParameterName.Navigation.screen_view)
            
            self.navigationViewController?.navigationService.stop()
            self.delegate?.onEnd()
        }, nil])
    }
    
    @IBAction func onShowCarparkAction(_ sender: Any) {
        self.delegate?.onShowCarparkList()
    }
    
    @IBAction func onShareDestinationAction(_ sender: Any) {
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.NavigationMode.UserClick.share_destination, screenName: ParameterName.NavigationMode.screen_view)
        self.delegate?.onShareDestination()
    }
    
    private func setupGradientColor() {
        DispatchQueue.main.async {
//            self.endButton.removeGradient()
//            let btnColor1 = UIColor.navBottomButtonColor1.resolvedColor(with: self.traitCollection)
//            let btnColor2 = UIColor.navBottomButtonColor2.resolvedColor(with: self.traitCollection)
//            self.endButton.applyGradient(colors: [btnColor1.cgColor,btnColor2.cgColor], withShadow: false)
            
//            let color1 = UIColor.navBottomBackgroundColor1.resolvedColor(with: self.traitCollection)
//            let color2 = UIColor.navBottomBackgroundColor2.resolvedColor(with: self.traitCollection)
//            self.view.applyBackgroundGradient(with: "NavBottomBackground", colors: [color1.cgColor,color2.cgColor])
            
            self.view.backgroundColor = UIColor.navBottomBackgroundColor1
        }
    }

    // MARK: - NavigationServiceDelegate implementation
    func navigationService(_ service: NavigationService, didUpdate progress: RouteProgress, with location: CLLocation, rawLocation: CLLocation) {
        
        if progress.distanceRemaining > 1000 {
            totalDistance.set("\(String(format: "%.1f", progress.distanceRemaining/1000))", " km")
        } else {
            totalDistance.set("\(Int(progress.distanceRemaining))", " m")
        }
        totalTravellingTime.set("\(Int(progress.durationRemaining/60))", " min")
        progressSlider.value = Float(progress.fractionTraveled)
        
        let estimatedTime = estimatedArrivalTime(progress.durationRemaining)
        estimateArriveTime.set(estimatedTime.time, estimatedTime.format)
    }
    
    func navigationService(_ service: NavigationService, didArriveAt waypoint: Waypoint) -> Bool {
        return true
    }
    
    func navigationService(_ service: NavigationService, shouldRerouteFrom location: CLLocation) -> Bool {
        return !service.router.userIsOnRoute(location)
//        return true
    }
    
    func navigationService(_ service: NavigationService, didRefresh routeProgress: RouteProgress) {
//        print(routeProgress.route.legs[0].segmentCongestionLevels)
//        print(routeProgress.route.legs[0].steps[0].shape)
    }
    
/*
    private func estimatedArrivalTime(_ duration: TimeInterval) -> (time: String,format: String) {
        let arrivalDate = Date(timeInterval: duration, since: Date())
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: arrivalDate)
        let t = hour > 12 ? hour - 12: hour
        let hourString = t < 10 ? "0\(t)" : t.description
        let minute = calendar.component(.minute, from: arrivalDate)
        let minString = minute < 10 ? "0\(minute)" : minute.description
        return (time: "\(hourString):\(minString)", format: hour >= 12 ? " PM" : " AM")
    }
*/
    // MARK: - Theme updated
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        guard previousTraitCollection != traitCollection else {
            return
        }
        setupGradientColor()
    }
}

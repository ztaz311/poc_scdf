//
//  TrafficIncidentUpdater.swift
//  Breeze
//
//  Created by VishnuKanth on 04/05/21.
//

import Foundation
@_spi(Experimental) import MapboxMaps
import MapboxCoreMaps
import Turf
import SwiftyBeaver

protocol TrafficIncidentUpdaterListener {
    func shouldAddTrafficIncident(features:Turf.FeatureCollection)
}

final class TrafficIncidentUpdater {
    
    private weak var mapView: MapView!
    private var isBelowPuck = false
    private var service: TrafficIncidentServiceProtocol!
    private var listeners: MulticastDelegate<TrafficIncidentUpdaterListener>!
    
    private(set) var currentTrafficFeatureCollection = FeatureCollection(features: [])
    
    // MARK: - Life cycle
    init(service: TrafficIncidentServiceProtocol) {
        self.service = service
        self.listeners = MulticastDelegate<TrafficIncidentUpdaterListener>()
    }
    
    func load(_ completion: @escaping (Bool) -> Void) {
        fetchPoints() { [weak self] (incidentFeatures) in
            guard let self = self else { return completion(false) }
            
            self.currentTrafficFeatureCollection = incidentFeatures
            
            self.listeners.invokeDelegates { delegate in
                DispatchQueue.main.async {
                    delegate.shouldAddTrafficIncident(features: incidentFeatures)
                }
            }
            completion(true)
        }
    }
    
    deinit {
        listeners.delegates.forEach { unbind(listener: $0) }
    }
    
    // MARK: - Public methods
    func bind(listener: TrafficIncidentUpdaterListener) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            
            listener.shouldAddTrafficIncident(features: self.currentTrafficFeatureCollection)
        }
        listeners.removeDelegate(listener)
        listeners.addDelegate(listener)
    }
    
    func unbind(listener: TrafficIncidentUpdaterListener) {
        listeners.removeDelegate(listener)
    }
    
    func update(incidetns: Incidents) {
        self.parseJSONItems(incidents: incidetns) { (incidentFeatures) in
            self.currentTrafficFeatureCollection = incidentFeatures
            
            self.listeners.invokeDelegates { delegate in
                DispatchQueue.main.async {
                    delegate.shouldAddTrafficIncident(features: incidentFeatures)
                }
            }
        }
    }

/*
    private func fetchAndProcessTrafficIncidentData(){
                
        fetchPoints() { [weak self] (incidentFeatures) in
            guard let self = self else { return }
            
            self.currentTrafficFeatureCollection = incidentFeatures
            
            self.listeners.invokeDelegates { delegate in
                DispatchQueue.main.async {
                    delegate.shouldAddTrafficIncident(features: incidentFeatures)
                }
            }
        }
        
    }
    
    @objc private func refreshIncidents() {
        fetchAndProcessTrafficIncidentData()
    }
*/
    private func fetchPoints(withCompletion completion: @escaping ((Turf.FeatureCollection) -> Void)) {
        
        DispatchQueue.global(qos: .background).async {
            
            self.service.getTrafficIncidents { result in
                switch result {
                case .success(let json):
                    //Parsing and find list of traffic incidents
                    DispatchQueue.main.async {
                        self.parseJSONItems(incidents: json) { (incidentFeatures) in
                            completion(incidentFeatures)
                        }
                    }

                case .failure(let error):
                    DispatchQueue.main.async {
                        SwiftyBeaver.error("Failed to fetchPoints for TrafficIncident:\(error.localizedDescription)")
                        // Return an empty array
                        completion(FeatureCollection(features: []))
                    }
                }
            }
        }
    }
    
    private func parseJSONItems(incidents:Incidents, _ completion: @escaping ((Turf.FeatureCollection)-> Void)) {
        
        var roadIncidents = [Turf.Feature]()
        
        //Adding traffic incident categories to an Array
        let categories = [TrafficIncidentCategories.RoadWork,TrafficIncidentCategories.Obstacle,TrafficIncidentCategories.VehicleBreakDown,TrafficIncidentCategories.Accident,TrafficIncidentCategories.Miscellaneous,TrafficIncidentCategories.UnattendVehicle,TrafficIncidentCategories.RoadBlock,TrafficIncidentCategories.FlashFlood,TrafficIncidentCategories.MajorAccident,TrafficIncidentCategories.RoadClosure, TrafficIncidentCategories.HeavyTraffic, TrafficIncidentCategories.TreePruning]
        //  TrafficIncidentCategories.YellowDengueZone, TrafficIncidentCategories.RedDengueZone
        
        //Loop each category
        for category in categories
        {
            //Loop each traffic incident that came from server
            for incident in incidents.incidentArray{
                
                //Find the category matching against the server response
                if(incident.key == category)
                {
                    //Looping each value in array if categories
                    for value in incident.value{
                        //Adding each point coordinate to Turf.Feature and appending to Array of Features
                        let incidentCoord = CLLocationCoordinate2D(latitude: value.Latitude!, longitude: value.Longitude!)
                        
                        // Beta.15 fix
//                        var feature = Turf.Feature(Point(incidentCoord))
                        var feature = Turf.Feature(geometry: .point(Point(incidentCoord)))
                        
                        feature.properties = [
                            "Type":.string(category),
                            "Message":.string(value.Message ?? ""),
                            "id": .string(value.id ?? "")
                        ]
                        
                        roadIncidents.append(feature)
                    }
                   
                }
            }
        }
        

        DispatchQueue.main.async {
           
            let incidentFeatureCollection = FeatureCollection(features: roadIncidents)
           
            completion(incidentFeatureCollection)
            //completion(erpNoCostfeatures)
        }
    }

}

extension MapView {
    
    func removeTrafficIncidentLayer(){
        DispatchQueue.main.async {
        do {
            try self.mapboxMap.style.removeLayer(withId: TrafficIncidentCategories.TrafficIncident_Identifier_SymbolLayer)
            try self.mapboxMap.style.removeSource(withId: TrafficIncidentCategories.TrafficIncident_GeoJsonSource)
        } catch {
            SwiftyBeaver.error("Failed to remove TrafficIncidentLayer.")
        }
        }
    }
    
    func addIncidentsToMap(features:Turf.FeatureCollection, isBelowPuck: Bool) {
        DispatchQueue.main.async {
        do {
            //Setting style Image for Roadwork
            try self.mapboxMap.style.addImage(UIImage(named: TrafficIncidentCategories.RoadWork)!, id: TrafficIncidentCategories.RoadWork,stretchX: [],stretchY: [])
            
            
            //Setting style Image for Vehicle Breakdown
            try self.mapboxMap.style.addImage(UIImage(named: TrafficIncidentCategories.VehicleBreakDown)!, id: TrafficIncidentCategories.VehicleBreakDown,stretchX: [],stretchY: [])
            
            //Setting style Image for Obstacle
            try self.mapboxMap.style.addImage(UIImage(named: TrafficIncidentCategories.Obstacle)!, id: TrafficIncidentCategories.Obstacle,stretchX: [],stretchY: [])
            
            //Setting style Image for Accident
            try self.mapboxMap.style.addImage(UIImage(named: TrafficIncidentCategories.Accident)!, id: TrafficIncidentCategories.Accident,stretchX: [],stretchY: [])
            
            //Setting style Image for Road Block
            try self.mapboxMap.style.addImage(UIImage(named: TrafficIncidentCategories.RoadBlock)!, id: TrafficIncidentCategories.RoadBlock,stretchX: [],stretchY: [])
            
            
            //Setting style Image for Misc
            try self.mapboxMap.style.addImage(UIImage(named: TrafficIncidentCategories.Miscellaneous)!, id: TrafficIncidentCategories.Miscellaneous,stretchX: [],stretchY: [])
            
            
            //Setting style Image for Unattended Vehicle
            try self.mapboxMap.style.addImage(UIImage(named: TrafficIncidentCategories.UnattendVehicle)!, id: TrafficIncidentCategories.UnattendVehicle,stretchX: [],stretchY: [])
            
            //Setting style Image for Major Accident
            try self.mapboxMap.style.addImage(UIImage(named: TrafficIncidentCategories.MajorAccident)!, id: TrafficIncidentCategories.MajorAccident,stretchX: [],stretchY: [])
            
            //Setting style Image for Flash Flood
            try self.mapboxMap.style.addImage(UIImage(named: TrafficIncidentCategories.FlashFlood)!, id: TrafficIncidentCategories.FlashFlood,stretchX: [],stretchY: [])
            
            //Setting style Image for Road Closure
            try self.mapboxMap.style.addImage(UIImage(named: TrafficIncidentCategories.RoadClosure)!, id: TrafficIncidentCategories.RoadClosure,stretchX: [],stretchY: [])
            
            //  Add more type incidents Heavy Traffic
            try self.mapboxMap.style.addImage(UIImage(named: TrafficIncidentCategories.HeavyTraffic)!, id: TrafficIncidentCategories.HeavyTraffic,stretchX: [],stretchY: [])
            
            try self.mapboxMap.style.addImage(UIImage(named: TrafficIncidentCategories.TreePruning)!, id: TrafficIncidentCategories.TreePruning,stretchX: [],stretchY: [])
            
//            try self.mapboxMap.style.addImage(UIImage(named: "dengueIcon")!, id: TrafficIncidentCategories.YellowDengueZone,stretchX: [],stretchY: [])
//
//            try self.mapboxMap.style.addImage(UIImage(named: "dengueIcon")!, id: TrafficIncidentCategories.RedDengueZone,stretchX: [],stretchY: [])
            
            // Create a GeoJSON data source.
            var geoJSONSource = GeoJSONSource()
            geoJSONSource.data = .featureCollection(features)
            
            
            let iconSize = Exp(.interpolate){
                Exp(.linear)
                Exp(.zoom)
                0
                0
                9
                0.0
                12.9
                0.0
                13
                0.85
                22
                1.0
            }
            
            //Expression for different types of images based on incident Type
            let iconExpression = Exp(.switchCase) { // Switching on a value
                Exp(.eq) { // Evaluates if conditions are equal
                    Exp(.get) { "Type" } // Get the current value for incident `Type`
                    TrafficIncidentCategories.RoadWork // returns true for the equal expression if the type is equal to "Roadwork"
                }
                TrafficIncidentCategories.RoadWork // Use the icon named "Roadwork" on the map style if the above condition is true
                Exp(.eq) {
                    Exp(.get) { "Type" }
                    TrafficIncidentCategories.VehicleBreakDown
                }
                TrafficIncidentCategories.VehicleBreakDown
                Exp(.eq) {
                    Exp(.get) { "Type" }
                    TrafficIncidentCategories.Obstacle
                }
                TrafficIncidentCategories.Obstacle
                Exp(.eq) {
                    Exp(.get) { "Type" }
                    TrafficIncidentCategories.Accident
                }
                TrafficIncidentCategories.Accident
                Exp(.eq) {
                    Exp(.get) { "Type" }
                    TrafficIncidentCategories.RoadBlock
                }
                TrafficIncidentCategories.RoadBlock
                Exp(.eq) {
                    Exp(.get) { "Type" }
                    TrafficIncidentCategories.Miscellaneous
                }
                TrafficIncidentCategories.Miscellaneous
                Exp(.eq) {
                    Exp(.get) { "Type" }
                    TrafficIncidentCategories.UnattendVehicle
                }
                TrafficIncidentCategories.UnattendVehicle
                Exp(.eq) {
                    Exp(.get) { "Type" }
                    TrafficIncidentCategories.MajorAccident
                }
                TrafficIncidentCategories.MajorAccident
                Exp(.eq) {
                    Exp(.get) { "Type" }
                    TrafficIncidentCategories.FlashFlood
                }
                TrafficIncidentCategories.FlashFlood
                Exp(.eq) {
                    Exp(.get) { "Type" }
                    TrafficIncidentCategories.RoadClosure
                }
                TrafficIncidentCategories.RoadClosure
                Exp(.eq) {
                    Exp(.get) { "Type" }
                    TrafficIncidentCategories.HeavyTraffic
                }
                TrafficIncidentCategories.HeavyTraffic
                Exp(.eq) {
                    Exp(.get) { "Type" }
                    TrafficIncidentCategories.TreePruning
                }
                TrafficIncidentCategories.TreePruning
                "" // default case is to return an empty string so no icon will be loaded
            }
            
            //                Exp(.eq) {
            //                    Exp(.get) { "Type" }
            //                    TrafficIncidentCategories.YellowDengueZone
            //                }
            //                "dengueIcon"
            //                Exp(.eq) {
            //                    Exp(.get) { "Type" }
            //                    TrafficIncidentCategories.RedDengueZone
            //                }
            //                "dengueIcon"
            
            var symbolLayer = SymbolLayer(id: TrafficIncidentCategories.TrafficIncident_Identifier_SymbolLayer)
            symbolLayer.source = TrafficIncidentCategories.TrafficIncident_GeoJsonSource
            symbolLayer.minZoom = 13.0
            symbolLayer.iconSize = .expression(iconSize)
            symbolLayer.iconImage = .expression(iconExpression)
            symbolLayer.iconAllowOverlap = .constant(true)
            symbolLayer.iconIgnorePlacement = .constant(true)
            // Add the source and style layers to the map style.
            try self.mapboxMap.style.addSource(geoJSONSource, id: TrafficIncidentCategories.TrafficIncident_GeoJsonSource)
            // rc4 - #307 remove _

            try self.mapboxMap.style.addPersistentLayer(with: symbolLayer.jsonObject(), layerPosition: isBelowPuck ? .below("puck") : .above(Constants.Layer.illegalparkingcamera))
            SwiftyBeaver.debug("addIncidentsToMap \(features.features.count)")
        }
        catch (let error) {
            SwiftyBeaver.error("Failed to addIncidentsToMap: \(error.localizedDescription)")
        }
        }
    }
    
    func removeDemoIncidentLayer(){
        DispatchQueue.main.async {
        do {
            try self.mapboxMap.style.removeLayer(withId: DemoIncidentLayer.DemoIncident_Identifier_SymbolLayer)
            try self.mapboxMap.style.removeSource(withId: DemoIncidentLayer.DemoIncident_GeoJsonSource)
        } catch {
            SwiftyBeaver.error("Failed to remove DemoIncidentLayer.")
        }
        }
    }
    
    func addDemoIncidentsToMap(features:Turf.FeatureCollection, isBelowPuck: Bool) {
        DispatchQueue.main.async {
        do {
            //Setting style Image for ERP
            try self.mapboxMap.style.addImage(UIImage(named: "erpSymbol")!, id: Values.wayPointERP,stretchX: [],stretchY: [])
            
            
            //Setting style Image for Paid
            try self.mapboxMap.style.addImage(UIImage(named: "erpSymbol")!, id: Values.wayPointPaid,stretchX: [],stretchY: [])
            
            //Setting style Image for ETA ExpressWay
            try self.mapboxMap.style.addImage(UIImage(named: "cyclistSymbol")!, id: Values.wayPointETAExpress,stretchX: [],stretchY: [])
            
            //Setting style Image for Obstacle
            try self.mapboxMap.style.addImage(UIImage(named: "SchoolZoneSymbol")!, id: Values.wayPointSchool,stretchX: [],stretchY: [])
            
            //Setting style Image for Accident
            try self.mapboxMap.style.addImage(UIImage(named: Values.wayPointAccident)!, id: Values.wayPointAccident,stretchX: [],stretchY: [])
            
            //Setting style Image for Road Block
            try self.mapboxMap.style.addImage(UIImage(named: "SchoolZoneSymbol")!, id: Values.wayPointSpeed,stretchX: [],stretchY: [])
            
            
            //Setting style Image for Misc
            try self.mapboxMap.style.addImage(UIImage(named: "SchoolZoneSymbol")!, id: Values.wayPointRoad,stretchX: [],stretchY: [])
            
            try self.mapboxMap.style.addImage(UIImage(named: "cyclistSymbol")!, id: Values.wayPointCyclist1,stretchX: [],stretchY: [])
            try self.mapboxMap.style.addImage(UIImage(named: "cyclistSymbol")!, id: Values.wayPointCyclist2,stretchX: [],stretchY: [])
            
            // Create a GeoJSON data source.
            var geoJSONSource = GeoJSONSource()
            geoJSONSource.data = .featureCollection(features)
            
            
            let iconSize = Exp(.interpolate){
                Exp(.linear)
                Exp(.zoom)
                0
                0
                9
                0.0
                12.9
                0.0
                13
                0.65
                22
                1.0
            }
            
            //Expression for different types of images based on incident Type
            let iconExpression = Exp(.switchCase) { // Switching on a value
                Exp(.eq) { // Evaluates if conditions are equal
                    Exp(.get) { "Type" } // Get the current value for incident `Type`
                    Values.wayPointAccident
                }
                Values.wayPointAccident
                Exp(.eq) {
                    Exp(.get) { "Type" }
                    Values.wayPointERP
                }
                ""
                Exp(.eq) {
                    Exp(.get) { "Type" }
                    Values.wayPointPaid
                }
                ""
                Exp(.eq) {
                    Exp(.get) { "Type" }
                    Values.wayPointETAExpress
                }
                ""
                
                Exp(.eq) {
                    Exp(.get) { "Type" }
                    Values.wayPointSchool
                }
                Values.wayPointSchool
                Exp(.eq) {
                    Exp(.get) { "Type" }
                    Values.wayPointSpeed
                }
                ""
                Exp(.eq) {
                    Exp(.get) { "Type" }
                    Values.wayPointRoad
                }
                ""
                Exp(.eq) {
                    Exp(.get) { "Type" }
                    Values.wayPointCyclist1
                }
                Values.wayPointCyclist1
                Exp(.eq) {
                    Exp(.get) { "Type" }
                    Values.wayPointCyclist2
                }
                Values.wayPointCyclist2
                "" // default case is to return an empty string so no icon will be loaded
            }
            
            var symbolLayer = SymbolLayer(id: DemoIncidentLayer.DemoIncident_Identifier_SymbolLayer)
            symbolLayer.source = DemoIncidentLayer.DemoIncident_GeoJsonSource
            symbolLayer.minZoom = 13.0
            symbolLayer.iconSize = .expression(iconSize)
            symbolLayer.iconImage = .expression(iconExpression)
            symbolLayer.iconAllowOverlap = .constant(true)
            symbolLayer.iconIgnorePlacement = .constant(true)
            // Add the source and style layers to the map style.
            try self.mapboxMap.style.addSource(geoJSONSource, id: DemoIncidentLayer.DemoIncident_GeoJsonSource)
            // #197
            try self.mapboxMap.style.addPersistentLayer(with: symbolLayer.jsonObject(), layerPosition: .above(Constants.Layer.illegalparkingcamera))
            SwiftyBeaver.debug("addDemoIncidentsToMap \(features.features.count)")
        }
        catch (let error) {
            SwiftyBeaver.error("Failed to addDemoIncidentsToMap: \(error.localizedDescription)")
        }
        }
    }
    
    func removeDemoLeg2IncidentLayer(){
        DispatchQueue.main.async {
        do {
            try self.mapboxMap.style.removeLayer(withId: DemoIncidentLayer.DemoIncident2_Identifier_SymbolLayer)
            try self.mapboxMap.style.removeSource(withId: DemoIncidentLayer.DemoIncident2_GeoJsonSource)
        } catch {
            SwiftyBeaver.error("Failed to remove DemoIncidentLayer.")
        }
        }
    }
    
    func addDemoLeg2IncidentsToMap(features:Turf.FeatureCollection, isBelowPuck: Bool) {
        DispatchQueue.main.async {
        do {
            //Setting style Image for Leg2 car park lots
            try self.mapboxMap.style.addImage(UIImage(named: "Miscellaneous")!, id: Values.wayPointCarparkLots,stretchX: [],stretchY: [])
            
            
            //Setting style Image for Leg2 level3
            try self.mapboxMap.style.addImage(UIImage(named: "Miscellaneous")!, id: Values.wayPointCarParkLevel3,stretchX: [],stretchY: [])
            
            
            // Create a GeoJSON data source.
            var geoJSONSource = GeoJSONSource()
            geoJSONSource.data = .featureCollection(features)
            
            
            let iconSize = Exp(.interpolate){
                Exp(.linear)
                Exp(.zoom)
                0
                0
                9
                0.0
                12.9
                0.0
                13
                0.0
                22
                0.0
            }
            
            //Expression for different types of images based on incident Type
            let iconExpression = Exp(.switchCase) { // Switching on a value
                Exp(.eq) { // Evaluates if conditions are equal
                    Exp(.get) { "Type" } // Get the current value for incident `Type`
                    Values.wayPointCarparkLots
                }
                Values.wayPointCarparkLots
                Exp(.eq) {
                    Exp(.get) { "Type" }
                    Values.wayPointCarParkLevel3
                }
                ""
                "" // default case is to return an empty string so no icon will be loaded
            }
            
            var symbolLayer = SymbolLayer(id: DemoIncidentLayer.DemoIncident2_Identifier_SymbolLayer)
            symbolLayer.source = DemoIncidentLayer.DemoIncident2_GeoJsonSource
            symbolLayer.minZoom = 13.0
            symbolLayer.iconSize = .expression(iconSize)
            symbolLayer.iconImage = .expression(iconExpression)
            symbolLayer.iconAllowOverlap = .constant(true)
            symbolLayer.iconIgnorePlacement = .constant(true)
            // Add the source and style layers to the map style.
            try self.mapboxMap.style.addSource(geoJSONSource, id: DemoIncidentLayer.DemoIncident2_GeoJsonSource)
            // #197
            try self.mapboxMap.style.addPersistentLayer(with: symbolLayer.jsonObject(), layerPosition: .above(Constants.Layer.illegalparkingcamera))
            SwiftyBeaver.debug("addDemoIncidentsToMap \(features.features.count)")
        }
        catch (let error) {
            SwiftyBeaver.error("Failed to addDemoIncidentsToMap: \(error.localizedDescription)")
        }
        }
    }
    
}

//
//  MobileNavigationNotAvailableVC.swift
//  Breeze
//
//  Created by Tuan, Pham Hai on 16/06/2023.
//

import Foundation
import UIKit

class MobileNavigationNotAvailableVC: UIViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var okayButton: UIButton!
    
    private var didDismissed: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        okayButton.layer.cornerRadius = 24
        okayButton.layer.masksToBounds = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) { [weak self] in
            if self?.didDismissed == false {
                self?.dismiss(animated: true)
            }
        }
    }
    
    @IBAction func okayAction(_ sender: Any) {
        
        AnalyticsManager.shared.logOpenPopupEventV2(popupName: ParameterName.NavigationMode.UserClick.popup_projected_to_dhu_okey, screenName: ParameterName.NavigationMode.screen_view)
        
        didDismissed = true
        self.dismiss(animated: true)
    }
    
}

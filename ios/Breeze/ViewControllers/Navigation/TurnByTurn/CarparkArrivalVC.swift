//
//  CarparkArrivalVC.swift
//  Breeze
//
//  Created by Zhou Hao on 20/5/22.
//

import UIKit

class CarparkArrivalVC: UIViewController {

    @IBOutlet weak var containerView: UIView!
    
    // MARK: - Public properties
    var onEndTrip: (() -> Void)?
    var onWalkingGuide: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    

    @IBAction func onEndTrip(_ sender: Any) {
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Navigation.UserClick.end_no_walking_guide, screenName: ParameterName.Navigation.screen_view)
        removeChildViewController(self)
        onEndTrip?()
    }
    
    @IBAction func onWalkingPath(_ sender: Any) {
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Navigation.UserClick.use_walking_guide, screenName: ParameterName.Navigation.screen_view)
        removeChildViewController(self)
        onWalkingGuide?()
    }
    
}

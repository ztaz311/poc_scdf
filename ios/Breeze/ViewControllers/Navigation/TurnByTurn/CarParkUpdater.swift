//
//  CarParkUpdater.swift
//  Breeze
//
//  Created by VishnuKanth on 22/04/22.
//

import Foundation
import MapboxNavigation
import CoreLocation
import Combine
import RxSwift
import MapboxCoreMaps
import MapboxMaps
import SwiftyBeaver
import UIKit

protocol CarParkUpdaterDelegate: AnyObject {
    func carParkNavigateHere(carPark:Carpark?)
    
    func didUpdateCarparks(carParks: [Carpark]?, location: CLLocationCoordinate2D)
}

enum CarparkType {
    case parking
    case voucher
    case searchOnly
}

enum CarParkUpdaterFromScreen {
    case cruise
    case mapLanding
    case navigation
    case routePlaning
    case content
}

final class CarParkUpdater {
    
    weak var delegate: CarParkUpdaterDelegate?
    let carparkViewModel: CarparksViewModel!
    private weak var carPlayNavigationMapView:NavigationMapView?
    private weak var mobileNavigationMapView:NavigationMapView?
    private var disposables = Set<AnyCancellable>()
    
    private(set) var fromScreen: CarParkUpdaterFromScreen
    
    private var currentSelectedCarpark: Carpark? {
        didSet {
            selectCarpark(id: currentSelectedCarpark?.id)
        }
    }
    var calloutView: CarparkToolTipsView2?
    var isVoucher = true
    
    // TODO: need to remove
    private var currentSelectedParkAnnotation: Annotation?
    var isRefreshCarPark = true
    //private var finalDestCoord:CLLocationCoordinate2D?
//    @Published var isLowAvailability = false
    var shouldIgnoreDestination = false
    var shouldCluster = false {
        didSet {
            self.addCarparkToUI()
        }
    }
    
    private (set) var type: CarparkType = .parking
    
    var carparkAvailability: ParkingType?
    
    private var isNavigation: Bool {
        return fromScreen == .navigation
    }
    
    var checkAvailability: Bool = false
    var carPark: Carpark? = nil
    var finalDestCoord: CLLocationCoordinate2D? = nil
    
    var carparks: [Carpark] {
        return carparkViewModel.carparks ?? []
    }
    
    var needShowVoucherView: Bool {
        return carparks.first { cp in
            return cp.hasVoucher
        } != nil
    }
    
    var alternateCarpark: AttributesCarparkModel?
    
    func configSearchVoucher(_ type: CarparkType) {
        self.type = type
    }
    
    private var lastSearchCarparkLocation: CLLocationCoordinate2D?
    
    
    // MARK: - Public functions
    init(destName:String,
         carPlayNavigationMapView:NavigationMapView?,
         mobileNavigationMapView:NavigationMapView?,
         fromScreen: CarParkUpdaterFromScreen) {
        
        self.carPlayNavigationMapView = carPlayNavigationMapView
        self.mobileNavigationMapView = mobileNavigationMapView
        carparkViewModel = CarparksViewModel(service: CarparkService(), arrivalTime: Date(), destName:destName)
        
        self.fromScreen = fromScreen
        
        self.carPlayNavigationMapView?.mapView.addCarparkResource()
        self.mobileNavigationMapView?.mapView.addCarparkResource()
        
        bindViewModel()
    }
    
    /*
     To query action on mobileNavigationMapView
     Only for show tooltip at carpark updater
     */
    func queryAmenitiesLayer(point:CGPoint, completion: ((_ detected:Bool)->Void)? = nil) {
        var layerID = [String]()
        layerID.append(CarparkValues.carparkSymbolLayerUnClusterId)
        layerID.append(CarparkValues.defaultCarparkSymbolLayerIdIcon)
        
        guard let mobileNavigationMapView = self.mobileNavigationMapView else { return }
        
        let queryOptions = RenderedQueryOptions(layerIds: layerID, filter: nil)
        
        mobileNavigationMapView.mapView.mapboxMap.queryRenderedFeatures(at: point, options: queryOptions) { [weak self] (result) in
            guard let self = self else { return }
            
            switch result {
                
            case .success(let features):
                
                // Return the first feature at that location, then pass attributes to the alert controller.
                if let selectedFeatureProperties = features.first?.feature.properties,
                   case let .string(carparkId) = selectedFeatureProperties["carparkId"] {
                    if let carpark = self.getCarparkFrom(id: carparkId) {
                        if let model = self.delegate as? OBUDisplayViewModel {
                            model.startTimer()
                        }
                        self.openCarparkToolTipView(carpark: carpark, tapped: true)
                        completion?(true)
                    } else if let carpark = self.getDefaultCarparkFrom(id: carparkId) {
                        if let model = self.delegate as? OBUDisplayViewModel {
                            model.startTimer()
                        }
                        self.openCarparkToolTipView(carpark: carpark, tapped: true)
                        completion?(true)
                    }
                    else if let carPark = self.alternateCarpark?.data?.getAlternateCarpark() {
                        self.openCarparkToolTipView(carpark: carPark, tapped: true)
                    }
                } else {
                    // If the feature is a cluster, it will have `point_count` and `cluster_id` properties. These are assigned
                    // when the cluster is created.
                    // no feature detected
                    self.didDeselectCarparkSymbol()
                    completion?(false)
                }
                
                
            case .failure(_):
                completion?(false)
                break
            }
        }
    }
    
    /*
     Set discoveryMode
     Use to Maplanding
     
     */
    func setDiscoveryMode(discoverMode: Bool) {
        self.shouldIgnoreDestination = !discoverMode
    }
    
    /*
     set value if car parks to be clustered ot not.
     Used in Maplanding
     */
    func setCPClusterMode(isCluster: Bool) {
        self.shouldCluster = isCluster
    }
    
    /*
     Add arrrive time and destName to view model
     */
    func configure(arrivalTime: Date, destName: String) {
        carparkViewModel.arrivalTime = arrivalTime
        carparkViewModel.destName = destName
    }
    
    /*
     Get carpark with carparkId from viewmodel
     */
    func getCarparkFrom(id: String) -> Carpark? {
        
        return carparkViewModel.carparks?.first(where: { carpark in
            carpark.id == id
        })
    }
    
    
    /*
     Get default carpark item with carparkId from viewmodel
     */
    func getDefaultCarparkFrom(id: String) -> Carpark? {
        
        return carparkViewModel.defaultProfileCarParks.first(where: { carpark in
            carpark.id == id
        })
    }
    
    
    /*
     Get all current carpark has voucher
     */
    func getCarparkVoucher() -> [Carpark] {
        return self.carparkViewModel.carparks?.filter { carpark in
            return carpark.hasVoucher
        } ?? []
    }
    
    /*
     Show all carpark has voucher
     */
    func showAllCarparkVoucher() {
        let voucherCarparks = getCarparkVoucher()
        self.type = .voucher
        addCarparkToUI(carparks: voucherCarparks)
    }
    
    /*
     remove all voucher
     */
    func removeAllCarparkVoucher() {
        self.type = .parking
        self.removeCarParks(keepDestination: true)
    }
    
    /*
     update selected carpark
     only one carpark selected at the same time
     call update ui after the selection was changed
     */
    func selectCarpark(id: String?) {
        carparkViewModel.selectCarpark(id: id)
        
        //Checking if there are any default carparks avaialble when the profile is selected and the car prak tool tip is open
        var defaultCarParks = [Carpark]()
        if SelectedProfiles.shared.isProfileSelected(){
            
            if self.carparkViewModel.defaultProfileCarParks.count > 0 {
                
                defaultCarParks = self.carparkViewModel.defaultProfileCarParks
            }
        }
        addCarparkToUI(defaultCarparks:defaultCarParks.count > 0 ? defaultCarParks : nil)
    }
    
    /*
     Add carpark to mapview
     In case isSearchVoucher. Don't add anythings
     */
    private func addCarparkToUI(carparks: [Carpark]? = nil,
                                defaultCarparks: [Carpark]? = nil) {
        
        if (type == .searchOnly) {
            return
        }
        guard var carparks = carparks ?? carparkViewModel.carparks else {
            
            self.removeCarParks(keepDestination: self.shouldIgnoreDestination)
            return
        }
        /*
         only show one carpark
         spec from home screen
         need check on other screen follow this spec or not
         */
        if let first = carparks.first {
            if (first.distance ?? 0) > Values.carparkMonitorDistanceInLanding {
                carparks = [first]
            }
        }
        
        if (checkAvailability) {
            
            self.removeCarParks(keepDestination: true)
            
            //  If current destinatino is carpark then should remove it from the carpark list to remove duplicated showing carpark
            if let carPark = carPark {
                
//               let isLowAvailability = self.checkParkingAvailability(from: carPark)
//
//                if(isLowAvailability){
                    
                    //Commenting below line as we don't need to announce low availability voice instruction as per the user story https://breezeteam.atlassian.net/browse/BREEZES-5293. So making isLowAvailability to false
//                    self.isLowAvailability = false
                    
                    let updatedCarparks = self.carparkViewModel.removeCarPark(carPark: carPark)

                    guard let updatedCarparks = updatedCarparks else {
                        return
                    }

                    if let mobileNavigationMapView = self.mobileNavigationMapView {
                        
                        //mobileNavigationMapView.pointAnnotationManager?.delegate = self
                        mobileNavigationMapView.addCarparks(updatedCarparks,
                                                            ignoreDestination: self.shouldIgnoreDestination,
                                                            showOnlyWithCostSymbol: true,
                                                            finalDestCoord: finalDestCoord,
                                                            belowPuck:!self.isNavigation)
                    }
                    
                    if let carPlayNavigationMapView = self.carPlayNavigationMapView {
                        //carPlayNavigationMapView.pointAnnotationManager?.delegate = self
                        carPlayNavigationMapView.addCarparks(updatedCarparks,
                                                             ignoreDestination: self.shouldIgnoreDestination,
                                                             showOnlyWithCostSymbol: true,
                                                             finalDestCoord: finalDestCoord,
                                                             belowPuck:!self.isNavigation)
                    }
//                }
            } else {
                                
                if let mobileNavigationMapView = self.mobileNavigationMapView {
                    
                    mobileNavigationMapView.addCarparks(carparks,
                                                        ignoreDestination: self.shouldIgnoreDestination,
                                                        showOnlyWithCostSymbol: true,
                                                        finalDestCoord: finalDestCoord,
                                                        belowPuck:!self.isNavigation)
                }
                
                if let carPlayNavigationMapView = self.carPlayNavigationMapView {
                    carPlayNavigationMapView.addCarparks(carparks,
                                                         ignoreDestination: self.shouldIgnoreDestination,
                                                         showOnlyWithCostSymbol: true,
                                                         finalDestCoord: finalDestCoord,
                                                         belowPuck:!self.isNavigation)
                }
            }
        } else {
            
            if let mobileNavigationMapView = self.mobileNavigationMapView {
                
                //mobileNavigationMapView.pointAnnotationManager?.delegate = self
                mobileNavigationMapView.addCarparks(carparks,defaultCarpaks: defaultCarparks != nil ? defaultCarparks:nil, ignoreDestination: self.shouldIgnoreDestination, showOnlyWithCostSymbol: true,finalDestCoord: finalDestCoord,belowPuck:!self.isNavigation,isCluster: self.shouldCluster)
            }
            
            if let carPlayNavigationMapView = self.carPlayNavigationMapView {
                //carPlayNavigationMapView.pointAnnotationManager?.delegate = self
                carPlayNavigationMapView.addCarparks(carparks, ignoreDestination: self.shouldIgnoreDestination, showOnlyWithCostSymbol: true,finalDestCoord: finalDestCoord,belowPuck:!self.isNavigation)
            }
           
        }
    }
    
    private func bindViewModel() {
        carparkViewModel.$carparks.sink { [weak self] carparks in
            
            guard let self = self, let carparks = carparks else { return }
            
            //We need to append both default carpks and original carparks if profile is selected and user is in discover
            var totalCarParks = [Carpark]()
            totalCarParks.append(contentsOf: carparks)
            
            print("CarPark updater",carparks)
            
            var defaultCarParks = [Carpark]()
            if SelectedProfiles.shared.isProfileSelected(){
                
                if self.carparkViewModel.defaultProfileCarParks.count > 0 {
                    
                    defaultCarParks = self.carparkViewModel.defaultProfileCarParks
                }
            }
            
            if(defaultCarParks.count > 0){
            
                totalCarParks.append(contentsOf: defaultCarParks)
            }
            if let location = self.lastSearchCarparkLocation {
                DispatchQueue.main.async { [weak self] in
                    self?.delegate?.didUpdateCarparks(carParks: totalCarParks, location: location)
                }
            }
            
            // need to delay for a short while so that the carparkViewModel carparks property will updated
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { [weak self] in
                self?.addCarparkToUI(carparks: carparks,defaultCarparks: defaultCarParks.count > 0 ? defaultCarParks : nil)
            }
        }.store(in: &disposables)
    }
    
    func loadCarParks(maxCarParksCount: Int,
                      radius: Int,
                      maxCarParkRadius: Int = 0,
                      location: CLLocationCoordinate2D,
                      checkAvailability: Bool = false,
                      carPark: Carpark? = nil,
                      finalDestCoord: CLLocationCoordinate2D? = nil,
                      shortTermOnly: Bool = false,
                      carParkId: String = "", handler: ((Error?) -> Void)? = nil){
        
        self.lastSearchCarparkLocation = location
        self.checkAvailability = checkAvailability
        self.carPark = carPark
        self.finalDestCoord = finalDestCoord
        
        carparkViewModel.load(at: location,
                              count: maxCarParksCount,
                              radius: radius,
                              maxRadius: maxCarParkRadius,
                              shortTermOnly: shortTermOnly,isVoucher: isVoucher,
                              parkingAvaibility: carparkAvailability?.stringValue,
                              carparkId: carParkId, handler: handler)
    }
    
    func checkParkingAvailability(from selectedCarPark: Carpark) -> Bool {
        
        let newCarParkData = carparkViewModel.getCarPark(parkingID: selectedCarPark.id ?? "")
        
        if let newCarParkData = newCarParkData, let availablePercentage = newCarParkData.availablePercentImageBand {
            
            if(availablePercentage == "TYPE0" || availablePercentage == "TYPE1"){
                
                return true
            }
        }
        
        return false
    }
    
    func refreshCarParks(maxCarParksCount: Int,
                         radius: Int,
                         location: CLLocationCoordinate2D,
                         finalDestCoord: CLLocationCoordinate2D? = nil,
                         shortTermOnly: Bool = false) {
        
        if(self.isRefreshCarPark){
            
            self.removeCarParks()
            self.loadCarParks(maxCarParksCount: maxCarParksCount,
                              radius: radius,
                              location: location,
                              finalDestCoord: finalDestCoord,
                              shortTermOnly: shortTermOnly)
        }
    }
    
    deinit {
        
        disposables.removeAll()
    }
    
    func removeCarParks(keepDestination: Bool = false){
        
        guard let carparkViewModel = carparkViewModel else { return }
        
        if let mobileNavigationMapView = self.mobileNavigationMapView {
            if alternateCarpark == nil {
                mobileNavigationMapView.removeCarParks(carparkViewModel.carparks ?? [], keepDestination: keepDestination)

            }
        }
        
        if let carPlayNavigationMapView = self.carPlayNavigationMapView {
            carPlayNavigationMapView.removeCarParks(carparkViewModel.carparks ?? [], keepDestination: keepDestination)
        }
        
    }
}
/*
 For and hide tooltip
 */

extension CarParkUpdater {
    
    var screenName: String {
        var screenName: String = ""
        switch fromScreen {
        case .navigation:
            screenName = ParameterName.Navigation.screen_view
        case .cruise:
            screenName = ParameterName.Cruise.screen_view
        case .mapLanding:
            screenName = ParameterName.Home.screen_view
        case .routePlaning:
            screenName = ParameterName.RoutePlanning.screen_view
        case .content:
            screenName = ""
        }
        
        return screenName
    }
    
    private func logTapCarpark(carpark: Carpark) {
        let eventValue = carpark.hasVoucher ? ParameterName.Carpark.UserClick.tap_voucher_carpark : ParameterName.Carpark.UserClick.tap_carpark
        AnalyticsManager.shared.logClickEvent(eventValue: eventValue, screenName: self.screenName)
    }
    
    private func logNavigateHere(carpark: Carpark) {
        let eventValue = ParameterName.Home.UserClick.navigate_here_parking
        AnalyticsManager.shared.logClickEvent(eventValue: eventValue, screenName: self.screenName)
    }
    
    func openCarparkToolTipView(carpark: Carpark, tapped: Bool = false) {
        
        DispatchQueue.main.async { [weak self] in
            
            guard let self = self, let mobileNavMap = self.mobileNavigationMapView else { return }
            
            let sameCarpark = carpark.id == self.currentSelectedCarpark?.id
            
            self.didDeselectCarparkSymbol()
            
            if sameCarpark {
                self.currentSelectedCarpark = nil
                // click the same one, don't show callout
                return
            }
            self.currentSelectedCarpark = carpark
            
            self.logTapCarpark(carpark: carpark)
            
            if tapped {
                let cameraOptions = CameraOptions(center: CLLocationCoordinate2D(latitude: carpark.lat ?? 0, longitude: carpark.long ?? 0), zoom: mobileNavMap.mapView.mapboxMap.cameraState.zoom) // using current zoom level
                mobileNavMap.mapView.camera.ease(to: cameraOptions, duration: Values.standardAnimationDuration, curve: .easeInOut, completion: nil)
            }
            
            let callOutView = CarparkToolTipsView2(carpark: carpark)
//            callOutView.onSaveAction = { [weak self] saved in
//                guard let self = self else { return }
//                // TODO:
//            }
            
//            callOutView.isSaved = carpark.isBookmarked ?? false
            callOutView.present(from: mobileNavMap.mapView.frame, in: mobileNavMap.mapView, constrainedTo: callOutView.frame, animated: true)
            self.calloutView = callOutView
            self.calloutView?.adjust(to: callOutView.tooltipCoordinate, in: mobileNavMap)
            self.adjustCameraCarParkClicked(at: callOutView.tooltipCoordinate, callout: callOutView)
            
            callOutView.onCarparkSelected = { [weak self] theCarpark in
                guard let self = self else { return }
                self.delegate?.carParkNavigateHere(carPark: theCarpark)
                self.didDeselectCarparkSymbol()
            }
        }
    }
    
    func adjustCameraCarParkClicked(at location: LocationCoordinate2D, callout: ToolTipsView) {
        
        guard let mobileNavMap = self.mobileNavigationMapView else { return }
        
        let cameraOptions = CameraOptions(center: location, padding:UIEdgeInsets(top: 200, left: 10, bottom: 10, right: 10), zoom: mobileNavMap.mapView.mapboxMap.cameraState.zoom) // using current zoom level
        
        mobileNavMap.mapView.camera.ease(to: cameraOptions, duration: Values.standardAnimationDuration, curve: .easeInOut, completion: nil)
       
      
       callout.adjust(to: location, in: mobileNavMap)
        
        

   }

    func didDeselectCarparkSymbol() {
        self.removeCallOut()
        currentSelectedCarpark = nil
    }
    
    func removeCallOut() {
        if let callout = self.calloutView {
            self.currentSelectedParkAnnotation = nil
            callout.dismiss(animated: true)
            callout.removeFromSuperview()
            self.calloutView = nil
        }
    }
}

//extension CarParkUpdater: AnnotationInteractionDelegate {
//    func annotationManager(_ manager: AnnotationManager, didDetectTappedAnnotations annotations: [Annotation]) {
//
//        // possible multiple tapped if there are overlapped together, only show the first one
//        if let annotation = annotations.first {
//            didSelectAnnotation(annotation: annotation, tapped: true)
//        }
//    }
//
//    func didSelectAnnotation(annotation: Annotation, tapped: Bool = false) {
//
//        DispatchQueue.main.async { [weak self] in
//
//            guard let self = self, let mobileNavMap = self.mobileNavigationMapView else { return }
//            self.didDeselectAnnotation()
//
//            if let carpark = annotation.userInfo?["carpark"] as? Carpark {
//
//                if carpark.id == self.currentSelectedCarpark?.id {
//                    self.currentSelectedCarpark = nil
//                    // click the same one, don't show callout
//                    return
//                }
//                self.currentSelectedCarpark = carpark
//                self.currentSelectedParkAnnotation = annotation
//                self.updateAnnotationImage(annotation, select: true)
//
//                if tapped {
//                    let cameraOptions = CameraOptions(center: CLLocationCoordinate2D(latitude: carpark.lat ?? 0, longitude: carpark.long ?? 0), zoom: mobileNavMap.mapView.mapboxMap.cameraState.zoom) // using current zoom level
//                    mobileNavMap.mapView.camera.ease(to: cameraOptions, duration: Values.standardAnimationDuration, curve: .easeInOut, completion: nil)
//
//
//                }
//
//                let callOutView = CarparkToolTipsView2(carpark: carpark)
//                callOutView.present(from: mobileNavMap.mapView.frame, in: mobileNavMap.mapView, constrainedTo: callOutView.frame, animated: true)
//                self.calloutView = callOutView
//                self.calloutView?.adjust(to: self.calloutView!.coordinate, in: mobileNavMap)
//                self.adjustCameraCarParkClicked(at: self.calloutView!.coordinate, callout: callOutView)
//
//                self.calloutView!.onCarparkSelected = { [weak self] theCarpark in
//                    guard let self = self else { return }
//
//                    self.delegate?.carParkNavigateHere(carPark: theCarpark)
//                    self.didDeselectAnnotation()
//
//                    }
//                }
//            }
//
//    }
    
//    func adjustCameraCarParkClicked(at location: LocationCoordinate2D, callout: ToolTipsView) {
//
//        guard let mobileNavMap = self.mobileNavigationMapView else { return }
//
//        let cameraOptions = CameraOptions(center: location, padding:UIEdgeInsets(top: 200, left: 10, bottom: 10, right: 10), zoom: mobileNavMap.mapView.mapboxMap.cameraState.zoom) // using current zoom level
//
//        mobileNavMap.mapView.camera.ease(to: cameraOptions, duration: Values.standardAnimationDuration, curve: .easeInOut, completion: nil)
//
//
//       callout.adjust(to: location, in: mobileNavMap)
//
//
//
//   }
//
//    func didDeselectCarparkSymbol() {
//        if let callout = self.calloutView {
//            self.currentSelectedParkAnnotation = nil
//            callout.dismiss(animated: true)
//            callout.removeFromSuperview()
//            self.calloutView = nil
//        }
//        currentSelectedCarpark = nil
//    }
    
//    func updateAnnotationImage(_ selectedAnnotation: Annotation, select: Bool) {
//        if let carpark = selectedAnnotation.userInfo?["carpark"] as? Carpark{
//            if let mobileNavMap = mobileNavigationMapView {
//
//                if let annotationMgr = mobileNavMap.pointAnnotationManager {
//                    let id = selectedAnnotation.id
//
//                    /// There could be a possiblity where annotations count be 0. So to avoid range crash issue, we can check if count > 0
//                    if(annotationMgr.annotations.count > 0){
//
//                        for i in 0...annotationMgr.annotations.count-1 {
//                            if annotationMgr.annotations[i].id == id {
//
//                                annotationMgr.annotations[i].image = getAnnotationImage(carpark: carpark, selected: select, ignoreDestination: shouldIgnoreDestination)
//                            }
//                        }
//                    }
//
//                }
//            }
//        }
//    }
    
//}

extension NavigationMapView {
    
    func getMapCenterCoordinate(_ bottomSheetHeight: CGFloat = 0) -> CLLocationCoordinate2D {
        let centerPoint = CGPoint(x: self.frame.size.width / 2, y: (self.frame.size.height - bottomSheetHeight) / 2)
        return self.mapView.mapboxMap.coordinate(for: centerPoint)
    }
    
    func addCarparks(_ carparks: [Carpark],defaultCarpaks:[Carpark]? = nil, ignoreDestination: Bool, showOnlyWithCostSymbol: Bool = false,finalDestCoord:CLLocationCoordinate2D?,belowPuck: Bool = true,isCluster:Bool = false) {

//        var annotations = [PointAnnotation]()
//        carparks.forEach { carPark in
//
//            let annotation = self.addCarparkAnnotation(carpark: carPark,ignoreDestination: ignoreDestination)
//            annotations.append(annotation)
//        }
//
//        if let finalDestCoord = finalDestCoord{
//
//            var destinationAnnotation = PointAnnotation(coordinate: finalDestCoord)
//            destinationAnnotation.image = .init(image: UIImage(named: "destinationMark")!, name: "navigationDest")
//            annotations.append(destinationAnnotation)
//        }
//
//        self.pointAnnotationManager?.annotations = annotations
        self.mapView.removeCarparkLayer()
        if carparks.count > 0 {
            
            self.mapView.addCarparks(carparks,
                                     showOnlyWithCostSymbol: showOnlyWithCostSymbol,
                                     belowPuck: belowPuck,
                                     ignoreDestination: ignoreDestination,isCluster: isCluster)
            
            // need to add destination annotation if not ignoreDestination
            if !ignoreDestination {
                if let finalDestCoord = finalDestCoord{
                    var annotations = [PointAnnotation]()
                    var destinationAnnotation = PointAnnotation(coordinate: finalDestCoord)
                    destinationAnnotation.image = .init(image: UIImage(named: "destinationMark")!, name: "navigationDest")
                    annotations.append(destinationAnnotation)
                    self.pointAnnotationManager?.annotations = annotations
                }
            }
            
            if let defaultCarpaks = defaultCarpaks {
                
                self.mapView.addDefaultProfileCarParks(carparks: defaultCarpaks, showOnlyWithCostSymbol: showOnlyWithCostSymbol, belowPuck: belowPuck, ignoreDestination: ignoreDestination)
            }
            
        }
    }
    
    func removeCarParks(_ carparks: [Carpark], keepDestination: Bool = false) {
        
        self.mapView.removeCarparkLayer()
        if (!keepDestination) {
            let annotations = [PointAnnotation]()
            
            self.pointAnnotationManager?.annotations = annotations
        }
    }
    
    private func addCarparkAnnotation(carpark: Carpark, ignoreDestination: Bool) -> PointAnnotation? {

        guard let carparkId = carpark.id, !carparkId.isEmpty else {return nil}
        var annotation = PointAnnotation(id:carparkId,coordinate: CLLocationCoordinate2D(latitude: carpark.lat ?? 0, longitude: carpark.long ?? 0))
        annotation.image = getAnnotationImage(carpark: carpark, selected: false,ignoreDestination: ignoreDestination)
        
        annotation.userInfo = [
            "carpark": carpark
        ]
        if let layerId = self.pointAnnotationManager?.layerId, !layerId.isEmpty {
            try? self.mapView.mapboxMap.style.setLayerProperty(for: layerId, property: "icon-allow-overlap", value: true)
            
            return annotation
        }
        
        return nil
    }
}

//
//  LiveLocationSharingButton.swift
//  Breeze
//
//  Created by Zhou Hao on 9/7/21.
//

import Foundation
import UIKit

@IBDesignable
final class LiveLocationSharingButton: XibView {
    
    @IBOutlet weak var pulseView: UIView!
    @IBOutlet weak var toggleButton: ToggleButton!
    
    private var pulseLayer: CAShapeLayer?
    @IBOutlet weak var toggleWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var toggleHeightConstraint: NSLayoutConstraint!
    
    // MARK: - Properties
    @IBInspectable var onImage:UIImage? {
        didSet {
            toggleButton.onImage = onImage
        }
    }
    @IBInspectable var offImage:UIImage? {
        didSet {
            toggleButton.offImage = offImage
        }
    }
    var onToggled: ((Bool) -> Void)?
    
    var isStarted: Bool {
        get {
            return !toggleButton.isSelected
        }
    }
    
    // MARK: - Init
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
//        setup()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
//        setup()
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        setup()
    }
    
    func setSizeToggleButton(_ size: CGFloat) {
        toggleWidthConstraint.constant = size
        toggleHeightConstraint.constant = size
        self.setNeedsLayout()
        self.layoutIfNeeded()
    }
    
    func resume() {
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Navigation.UserClick.eta_resume, screenName: ParameterName.Navigation.screen_view)
        
        guard !toggleButton.isSelected else {
            return
        }
        perform(#selector(deselectToggleButton), with: nil, afterDelay: 1)
    }
    
    func pause() {
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Navigation.UserClick.eta_pause, screenName: ParameterName.Navigation.screen_view)
        guard !toggleButton.isSelected else {
            return
        }
        toggleButton.select(true)
    }
    
    // This is call from carplay
    func updateToggleWithOutCallBack(status:Bool){
        toggleButton.toggleWithOutCallBack(status:status)
        // should update animation also
        self.animation(start: status)
    }
    
    func setName(_ name: String) {
//        labelName.text = getAbbreviation(name: name)
    }
    
    // MARK: - Private methods
    private func setup() {
        
        toggleButton.onToggled = { isSelected in
            self.animation(start: !isSelected)
            if let callback = self.onToggled {
                callback(!isSelected)
            }
        }
    }

    private func animation(start: Bool) {
        DispatchQueue.main.async {
            if start {
                self.createPulse()
                self.animatePulse()
            } else {
                self.removePulse()
            }
        }
    }
    
    private func createPulse() {
        removePulse()   // don't duplicate
        
        // create pulse
        let circularPath = UIBezierPath(arcCenter: .zero, radius: pulseView.bounds.size.width/2.0, startAngle: 0, endAngle: 2 * .pi, clockwise: true)
        let pulseLayer = CAShapeLayer()
        pulseLayer.path = circularPath.cgPath
        pulseLayer.lineWidth = 1
        pulseLayer.fillColor = UIColor.liveLocationSharingPulseColor.cgColor
        pulseLayer.lineCap = CAShapeLayerLineCap.round
        pulseLayer.position = pulseView.center
        self.pulseLayer = pulseLayer
        pulseView.layer.addSublayer(pulseLayer)
    }
            
    private func animatePulse() {
        guard let pulseLayer = self.pulseLayer else { return }
        
        pulseLayer.strokeColor = UIColor.liveLocationSharingPulseColor.cgColor
        
        let scaleAnimation = CABasicAnimation(keyPath: "transform.scale")
        scaleAnimation.duration = 2.0
        scaleAnimation.fromValue = 0.0
        scaleAnimation.toValue = 1
        scaleAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
        scaleAnimation.repeatCount = .greatestFiniteMagnitude
        scaleAnimation.isRemovedOnCompletion = false
        pulseLayer.add(scaleAnimation, forKey: "scale")
        
        let opacityAnimation = CABasicAnimation(keyPath: #keyPath(CALayer.opacity))
        opacityAnimation.duration = 2.0
        opacityAnimation.fromValue = 1.0
        opacityAnimation.toValue = 0.0
        opacityAnimation.isRemovedOnCompletion = false
        opacityAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
        opacityAnimation.repeatCount = .greatestFiniteMagnitude
        pulseLayer.add(opacityAnimation, forKey: "opacity")
        
    }
    
    @objc private func deselectToggleButton() {
        toggleButton.select(false)
    }
    
    private func removePulse() {
        if let layer = self.pulseLayer {
            layer.removeAllAnimations()
            layer.removeFromSuperlayer()
        }
    }
}

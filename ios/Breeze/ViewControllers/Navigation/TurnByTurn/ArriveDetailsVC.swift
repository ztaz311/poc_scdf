//
//  ArriveDetailsVC.swift
//  Breeze
//
//  Created by Malou Mendoza on 24/3/21.
//

import UIKit

protocol ArriveDetailsVCDelegate: AnyObject {
    func onComplete(for detailsVC: ArriveDetailsVC)
    func onStartPark(for detailsVC: ArriveDetailsVC)
}

class ArriveDetailsVC: UIViewController {

    @IBOutlet var destinationNameLabel: UILabel!
    @IBOutlet var destinationAddressLabel: UILabel!
    @IBOutlet var parkBtn: UIButton!
    @IBOutlet var doneBtn: UIButton!
    @IBOutlet weak var easyBreezyBtn: UIButton!    
    @IBOutlet weak var destinationImgView: UIImageView!
    
    weak var delegate: ArriveDetailsVCDelegate?
    
    // MARK: - Properties to decide if it's a new easybreezy or not
    var selectedAddress: BaseAddress!
    
    var isNewEasyBreezy = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        AnalyticsManager.shared.logCompletedNavigationEvent(eventValue: ParameterName.navigation_completed, screenName: ParameterName.navigation)
        

        easyBreezyBtn.setImage(isNewEasyBreezy ? UIImage(named: "addtofavourite") : UIImage(named:"edit"), for: .normal)
        destinationImgView.image = isNewEasyBreezy ? UIImage(named: "newAddressPin") : UIImage(named: "destinationMark")
        
        if let address = selectedAddress {
            destinationNameLabel.text = address.address1
            destinationAddressLabel.text = address.address2
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        self.view.applyBackgroundGradient(with: "ArrivalBackground", colors: [UIColor.cruiseNotificationColor1.cgColor,UIColor.cruiseNotificationColor2.cgColor,UIColor.cruiseNotificationColor3.cgColor])
        self.view.roundCorners([.bottomLeft, .bottomRight], radius: 30)
        self.view.setNeedsLayout()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
       
        self.layoutView()
        parkBtn.setNeedsLayout()
        doneBtn.setNeedsLayout()
        self.view.setNeedsLayout()
    }
 
    func layoutView(){
        parkBtn.removeGradient()
        parkBtn.applyGradient(colors: [UIColor.brandPurpleColor.cgColor,UIColor.brandPurpleColor.cgColor], withShadow: false)
        doneBtn.layer.borderColor = UIColor.brandPurpleColor.cgColor
        doneBtn.layer.cornerRadius = 25
        doneBtn.clipsToBounds = true
        
        self.view.applyBackgroundGradient(with: "ArrivalBackground", colors: [UIColor.cruiseNotificationColor1.cgColor,UIColor.cruiseNotificationColor2.cgColor,UIColor.cruiseNotificationColor3.cgColor])
        self.view.roundCorners([.bottomLeft, .bottomRight], radius: 30)
        self.view.showBottomShadow()
    }
    
    @IBAction func onPark(_ sender: Any) {
        delegate?.onStartPark(for: self)
    }
    
    @IBAction func onDone(_ sender: Any) {
        delegate?.onComplete(for: self)
    }
}

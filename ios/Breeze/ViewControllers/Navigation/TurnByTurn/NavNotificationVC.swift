//
//  NavNotificationVC.swift
//  Breeze
//
//  Created by Zhou Hao on 23/3/21.
//

import UIKit
import SwiftyBeaver
import CarPlay

protocol NotificationUpdatable: AnyObject {
    func update(_ feature: FeatureDetectable?)
}

protocol NavNotificationVCDelegate: AnyObject {

    func notificationWillAppear()
    func notificationWillDisappear()
    func onNearFeatureUpdated(feature: Feature) // can be used for voice instruction
}

final class NavNotificationVC: UIViewController {

    // MARK: - IBOutlet
    @IBOutlet weak var firstIcon: UIImageView!
    @IBOutlet weak var secondIcon: UIImageView!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var rateLabel: UILabel!
    @IBOutlet weak var speedLabel: UILabel!
//    @IBOutlet weak var leadingConstraintValue: NSLayoutConstraint!
    
    // TODO: TO BE FIXED! Move to carPlay part
    var cpFeatureImage:UIImage!
    var cpFeatureNotificationAlert:CPNavigationAlert?
    var cpFeatureTitleVariants = [String]()
    var cpFeatureSubTitleVariants = [String]()
    
    // MARK: - Properties
    
    weak var delegate: NavNotificationVCDelegate?
    
    // MARK: - Properties
    var currentFeature: FeatureDetectable? { // current feature (the first feature)
        willSet {
            if newValue?.id != currentFeature?.id {
                self.onFeatureUpdated?(newValue)
            }
        }
    }
    var onFeatureUpdated: ((_ feature: FeatureDetectable?) -> Void)?

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.view.layer.cornerRadius = 25
        self.view.layer.masksToBounds = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
                                
//        self.view.roundCorners([.bottomLeft, .bottomRight], radius: Values.Instruction.roundedCornerRadius)
//        self.view.backgroundColor = UIColor.navigationNotificationBGColor
        setupLayout()
        self.rateLabel.isHidden = true
        
        self.view.showBottomShadow()
    }
    
    private func setupLayout() {
        self.view.translatesAutoresizingMaskIntoConstraints = false
        self.view.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(-Values.navigationNotificationHeight)
            make.leading.equalToSuperview().offset(22)
            make.trailing.equalToSuperview().offset(-26)
            make.height.equalTo(Values.navigationNotificationHeight)
        }
        
        firstIcon.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().offset(32)
            make.top.equalToSuperview().offset(16)
            make.height.equalTo(80)
            make.width.equalTo(80)
        }
        secondIcon.snp.makeConstraints { (make) in
            make.leading.equalTo(firstIcon.snp.trailing).offset(-16)
            make.bottom.equalTo(firstIcon.snp.bottom)
            make.height.equalTo(72)
            make.width.equalTo(72)
        }
        distanceLabel.font = UIFont.cruiseNotificationDistanceFont()
        distanceLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(firstIcon.snp.trailing).offset(18)
            make.top.equalToSuperview().offset(16)
            make.trailing.equalToSuperview().offset(-16)
        }
        addressLabel.font = UIFont.cruiseNotificationTypeFont()
        addressLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(distanceLabel.snp.leading)
            make.top.equalTo(distanceLabel.snp.bottom).offset(5)
            make.trailing.equalToSuperview().offset(-16)
        }
        speedLabel.snp.makeConstraints { (make) in
            make.centerX.equalTo(secondIcon.snp.centerX)
            make.centerY.equalTo(secondIcon.snp.centerY)
        }
        rateLabel.font = UIFont.instructionRateFont()
        rateLabel.snp.makeConstraints { (make) in
            make.trailing.equalToSuperview().offset(-24)
            make.width.equalTo(81)
            make.height.equalTo(36)
            make.top.equalToSuperview().offset(16)
        }
    }

}

// TODO: NotificationUpdatable not used, need to be removed!!!
extension NavNotificationVC: NotificationUpdatable {

    func showFeature(_ feature: FeatureDetectable) {
        clear()
        
        self.currentFeature = feature

        showFirstFeature(feature)
        
        setIcon(imageView: firstIcon, for: feature)
        // if the first feature is school zone/silver zone show second one with speed
        if feature is DetectedSchoolZone || feature is DetectedSilverZone {
            setSpeedIcon(imageView: secondIcon)
            secondIcon.image = UIImage()
            speedLabel.isHidden = true
            rateLabel.isHidden = true
//            leadingConstraintValue.constant = 18
            distanceLabel.snp.updateConstraints { make in
                make.leading.equalTo(firstIcon.snp.trailing).offset(18)
            }
            rateLabel.isHidden = true
            
          #if DEMO
            if(DemoSession.shared.isDemoRoute)
            {
                distanceLabel.text = ""
            }
         #endif
            
            self.view.backgroundColor = UIColor.notificationSchoolAndSilverZoneBG
        } else if let feature = feature as? DetectedERP {
            secondIcon.image = UIImage()
            speedLabel.isHidden = true
            rateLabel.isHidden = false
            rateLabel.text = "$\(String(format: "%.2f", feature.price))"
            addressLabel.text = feature.address
//            leadingConstraintValue.constant = 18
            distanceLabel.snp.updateConstraints { make in
                make.leading.equalTo(firstIcon.snp.trailing).offset(18)
            }
            self.view.backgroundColor = UIColor.notificationERPBG
            
            #if DEMO
            if(DemoSession.shared.isDemoRoute)
            {
                if(feature.id == "36")
                {
                    distanceLabel.text = "Paid"
                }
            }
            #endif
            
        } else if let feature = feature as? DetectedTrafficIncident {
            secondIcon.image = UIImage()
            speedLabel.isHidden = true
            rateLabel.isHidden = true
            addressLabel.text = feature.category
//            leadingConstraintValue.constant = 18
            distanceLabel.snp.updateConstraints { make in
                make.leading.equalTo(firstIcon.snp.trailing).offset(18)
            }
            self.view.backgroundColor = UIColor.notificationTrafficBG
            
            #if DEMO
            if(DemoSession.shared.isDemoRoute)
            {
                if(feature.category == Values.wayPointSpeed)
                {
                   
                    distanceLabel.text = "Speed limit"
                    addressLabel.text = feature.message

                }
                else
                {
                    if(feature.category == Values.wayPointRoad)
                    {
                        self.view.backgroundColor = UIColor.notificationERPBG
                        distanceLabel.text = "Jalan Pelepah"
                        addressLabel.text = "\(DemoSession.shared.carParkCapacity) parking lots"

                    }
                    else if(feature.category == Values.wayPointETAExpress){
                        
                        self.view.backgroundColor = UIColor.notificationETAExpressWay
                        distanceLabel.text = "Public message"
                        addressLabel.text = "5 min | Faber Park\n8 min | Sentosa"
                        
                    }
                    else if(feature.category == Values.wayPointCarparkLots)
                    {
                        self.view.backgroundColor = UIColor.notificationERPBG
                        distanceLabel.text = "Destination"
                        addressLabel.text = "470 parking lots"
                    }
                    else if(feature.category == Values.wayPointCarParkLevel3)
                    {
                        self.view.backgroundColor = UIColor.notificationERPBG
                        distanceLabel.text = "Head to level 3"
                        addressLabel.text = "for more available parking lots"

                    } else if(feature.category == Values.wayPointCyclist1 || feature.category == Values.wayPointCyclist2) {
                        self.view.backgroundColor = UIColor.notificationTrafficBG
                        distanceLabel.text = "200 m"
                        addressLabel.text = "Cyclists"
                    }
                    else
                    {
                        distanceLabel.text = ""
                    }
                    
                }
            }
            #endif
            
        }
        else if let feature = feature as? DetectedSpeedCamera {
            secondIcon.image = UIImage()
            speedLabel.isHidden = true
            rateLabel.isHidden = true
            addressLabel.text = feature.category
//            leadingConstraintValue.constant = 18
            distanceLabel.snp.updateConstraints { make in
                make.leading.equalTo(firstIcon.snp.trailing).offset(18)
            }
            self.view.backgroundColor = UIColor.notificationTrafficBG
        }
        #if DEMO
        if(DemoSession.shared.isDemoRoute || DemoSession.shared.parkAnyWhere)
        {
            if let feature = feature as? DetectedETANotificationView {
                
                if(feature.category == Values.carPark)
                {
                    setSpeedIcon(imageView: secondIcon)
                    secondIcon.image = UIImage()
                    speedLabel.isHidden = true
                    rateLabel.isHidden = true
                    distanceLabel.snp.updateConstraints { make in
                        make.leading.equalTo(firstIcon.snp.trailing).offset(18)
                    }
                    rateLabel.isHidden = true
                    self.view.backgroundColor = UIColor.notificationERPBG
                    distanceLabel.text = "Parking"
                    addressLabel.text = feature.message
                }
                else
                {
                    setSpeedIcon(imageView: secondIcon)
                    secondIcon.image = UIImage()
                    speedLabel.isHidden = true
                    rateLabel.isHidden = true
                    distanceLabel.snp.updateConstraints { make in
                        make.leading.equalTo(firstIcon.snp.trailing).offset(18)
                    }
                    rateLabel.isHidden = true
                    self.view.backgroundColor = UIColor.notificationETABG
                    distanceLabel.text = feature.recipientName
                    addressLabel.text = feature.message
                }
                
            }
        }
        else
        {
            secondIcon.image = UIImage()
            speedLabel.isHidden = true
            rateLabel.isHidden = true
//            leadingConstraintValue.constant = 18
            distanceLabel.snp.updateConstraints { make in
                make.leading.equalTo(firstIcon.snp.trailing).offset(18)
            }
        }
        #else
        secondIcon.image = UIImage()
        speedLabel.isHidden = true
        distanceLabel.snp.updateConstraints { make in
            make.leading.equalTo(firstIcon.snp.trailing).offset(18)
        }
        #endif
        
        // update height
        let h = distanceLabel.intrinsicContentSize.height + addressLabel.intrinsicContentSize.height + 38 // top + margin + bottom
        self.view.snp.updateConstraints { make in
            make.height.equalTo(h > Values.navigationNotificationHeight ? h : Values.navigationNotificationHeight)
        }
    }

    // TODO: Review it. Should be removed?
    func update(_ feature: FeatureDetectable?) {
        clear()
        
        self.currentFeature = feature
        
        if let feature = feature {
            showFirstFeature(feature)
            setIcon(imageView: firstIcon, for: feature)
            // if the first feature is school zone/silver zone show second one with speed
            if feature is DetectedSchoolZone || feature is DetectedSilverZone {
                setSpeedIcon(imageView: secondIcon)
                secondIcon.image = UIImage()
                speedLabel.isHidden = true
                rateLabel.isHidden = true
                distanceLabel.snp.updateConstraints { make in
                    make.leading.equalTo(firstIcon.snp.trailing).offset(18)
                }
                rateLabel.isHidden = true
                
                #if DEMO
                if(DemoSession.shared.isDemoRoute)
                {
                    distanceLabel.text = ""
                }
                #endif
                
                self.view.backgroundColor = UIColor.notificationSchoolAndSilverZoneBG
            } else if let feature = feature as? DetectedERP {
                secondIcon.image = UIImage()
                speedLabel.isHidden = true
                rateLabel.isHidden = false
                rateLabel.text = "$\(String(format: "%.2f", feature.price))"
                addressLabel.text = feature.address
                distanceLabel.snp.updateConstraints { make in
                    make.leading.equalTo(firstIcon.snp.trailing).offset(18)
                }
                
                self.view.backgroundColor = UIColor.notificationERPBG
                
                #if DEMO
                if(DemoSession.shared.isDemoRoute)
                {
                    if(feature.id == "36")
                    {
                        distanceLabel.text = "Paid"
                    }
                }
                #endif
                
            } else if let feature = feature as? DetectedTrafficIncident {
                secondIcon.image = UIImage()
                speedLabel.isHidden = true
                rateLabel.isHidden = true
                addressLabel.text = feature.category
//                leadingConstraintValue.constant = 18
                distanceLabel.snp.updateConstraints { make in
                    make.leading.equalTo(firstIcon.snp.trailing).offset(18)
                }
                self.view.backgroundColor = UIColor.notificationTrafficBG
                
                #if DEMO
                if(DemoSession.shared.isDemoRoute)
                {
                    if(feature.category == Values.wayPointSpeed)
                    {
                       
                        distanceLabel.text = "Speed limit"
                        addressLabel.text = feature.message
                    }
                    else
                    {
                        if(feature.category == Values.wayPointRoad)
                        {
                            self.view.backgroundColor = UIColor.notificationERPBG
                            distanceLabel.text = "Jalan Pelepah"
                            addressLabel.text = "\(DemoSession.shared.carParkCapacity) parking lots"
                        }
                        else if(feature.category == Values.wayPointCarparkLots)
                        {
                            self.view.backgroundColor = UIColor.notificationERPBG
                            distanceLabel.text = "Destination"
                            addressLabel.text = "470 parking lots"
                        }
                        
                        else if(feature.category == Values.wayPointETAExpress)
                        {
                            self.view.backgroundColor = UIColor.notificationETAExpressWay
                            distanceLabel.text = "Public message"
                            addressLabel.text = "5 min | Faber Park \n 8 min | Sentosa"
                        }
                        
                        else if(feature.category == Values.wayPointCarParkLevel3)
                        {
                            self.view.backgroundColor = UIColor.notificationERPBG
                            distanceLabel.text = "Head to level 3"
                            addressLabel.text = "for more available parking lots"
                            
                        } else if(feature.category == Values.wayPointCyclist1 || feature.category == Values.wayPointCyclist2) {
                            self.view.backgroundColor = UIColor.notificationTrafficBG
                            distanceLabel.text = "200 m"
                            addressLabel.text = "Cyclists"
                        }
                        else
                        {
                            distanceLabel.text = ""
                        }
                        
                    }
                }
                #endif
                
            }
            #if DEMO
            if(DemoSession.shared.isDemoRoute || DemoSession.shared.parkAnyWhere)
            {
                if let feature = feature as? DetectedETANotificationView {
                    
                    if(feature.category == Values.carPark)
                    {
                        setSpeedIcon(imageView: secondIcon)
                        secondIcon.image = UIImage()
                        speedLabel.isHidden = true
                        rateLabel.isHidden = true
                        distanceLabel.snp.updateConstraints { make in
                            make.leading.equalTo(firstIcon.snp.trailing).offset(18)
                        }
                        rateLabel.isHidden = true
                        self.view.backgroundColor = UIColor.notificationERPBG
                        
                        distanceLabel.text = "Parking"
                        addressLabel.text = feature.message
                    }
                    else
                    {
                        setSpeedIcon(imageView: secondIcon)
                        secondIcon.image = UIImage()
                        speedLabel.isHidden = true
                        rateLabel.isHidden = true
                        distanceLabel.snp.updateConstraints { make in
                            make.leading.equalTo(firstIcon.snp.trailing).offset(18)
                        }
                        rateLabel.isHidden = true
                        self.view.backgroundColor = UIColor.notificationETABG
                        
                        distanceLabel.text = feature.recipientName
                        addressLabel.text = feature.message
                    }
                    
                }
            }
            else
            {
                secondIcon.image = UIImage()
                speedLabel.isHidden = true
                rateLabel.isHidden = true
                distanceLabel.snp.updateConstraints { make in
                    make.leading.equalTo(firstIcon.snp.trailing).offset(18)
                }
            }
            #else
            secondIcon.image = UIImage()
            speedLabel.isHidden = true
            distanceLabel.snp.updateConstraints { make in
                make.leading.equalTo(firstIcon.snp.trailing).offset(18)
            }
            #endif
        
            // update height
            let h = distanceLabel.intrinsicContentSize.height + addressLabel.intrinsicContentSize.height + 38 // top + margin + bottom
            self.view.snp.updateConstraints { make in
                make.height.equalTo(h > Values.navigationNotificationHeight ? h : Values.navigationNotificationHeight)
            }
        }
    }

    private func clear() {
        firstIcon.image = nil
        secondIcon.image = nil
        distanceLabel.text = ""
        addressLabel.text = ""
        rateLabel.text = ""
        rateLabel.isHidden = true
        
//        cpFeatureTitleVariants = []
//        cpFeatureSubTitleVariants = []
    }
    
    private func showFirstFeature(_ feature: FeatureDetectable) {
        let distance = FeatureNotification.getDistance(for: feature)
        distanceLabel.text = distance
        let type = FeatureNotification.getTypeText(for: feature)
        addressLabel.text = type
        
        SwiftyBeaver.debug("\(feature), Mobile distance = \(distance), type = \(type)")
    }
    
    private func setIcon(imageView: UIImageView, for feature: FeatureDetectable) {
        imageView.image = FeatureNotification.getTypeImage(feature)
        
        //setting image on Car Play Notification
//        cpFeatureImage = getTypeImage(feature)
    }
    
    private func setSpeedIcon(imageView: UIImageView) {
        imageView.image = UIImage(named: "speedIcon")!
        
        //setting image on Car Play Notification
//        cpFeatureImage = UIImage(named: "speedIcon")!
    }
/*
    private func text(for feature: FeatureDetectable) -> String {
        if feature is DetectedSchoolZone {
            return FeatureDetection.featureTypeSchoolZone
        } else if feature is DetectedSilverZone {
            return FeatureDetection.featureTypeSilverZone
        } else if feature is DetectedERP {
            return FeatureDetection.featureTypeErp
        } else if let feature = feature as? DetectedTrafficIncident {
            return feature.category
        }
        return ""
    }
    
    private func getTypeImage(_ feature: FeatureDetectable) -> UIImage {
        if feature is DetectedSchoolZone {
            return appDelegate().carPlayConnected ? UIImage(named: "carplay-schoolZoneIcon")! : UIImage(named: "schoolZoneIcon")!
        } else if feature is DetectedSilverZone {
            return appDelegate().carPlayConnected ? UIImage(named: "carplay-silverZoneIcon")! : UIImage(named: "silverZoneIcon")!
        } else if feature is DetectedERP {
            return appDelegate().carPlayConnected ? UIImage(named: "carplay-erpIcon")! : UIImage(named: "erpIcon")!
        } else if let feature = feature as? DetectedTrafficIncident {
            
            //For Demo
            if(feature.category == Values.wayPointSpeed)
            {
                return appDelegate().carPlayConnected ? UIImage(named: "carplay-Miscellaneous-Icon")! : UIImage(named: "Miscellaneous-Icon")!
            }
            //For Demo
            else if(feature.category == Values.wayPointRoad || feature.category == Values.wayPointCarparkLots || feature.category == Values.wayPointCarParkLevel3 )
            {
                return UIImage(named: "Parking-Icon")!
            } else if (feature.category == Values.wayPointCyclist1 || feature.category == Values.wayPointCyclist2) {
                return UIImage(named: "Cyclist-Icon")!
            }
            else{
                return appDelegate().carPlayConnected ? UIImage(named: "carplay-\(feature.category)-Icon")! : UIImage(named: "\(feature.category)-Icon")!
            }
            
        }
        #if DEMO
        if(DemoSession.shared.isDemoRoute || DemoSession.shared.parkAnyWhere)
        {
             if let feature = feature as? DetectedETANotificationView {
                
                 if(feature.category == Values.carPark)
                 {
                     return UIImage(named: "Parking-Icon")!
                 }
                 else
                 {
                     return UIImage(named: "eta-Icon")!
                 }
                
            }
        }
        
        #endif
        return UIImage()
    }
*/
}

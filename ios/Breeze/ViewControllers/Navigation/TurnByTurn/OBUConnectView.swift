//
//  OBUConnectView.swift
//  Breeze
//
//  Created by Tuan, Pham Hai on 29/08/2023.
//

import Foundation
import UIKit

typealias OBUConnectCallback = (Bool)-> (Void)

class OBUConnectView: UIView {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var balanceLabel: UILabel!
    @IBOutlet var connectLabel: UILabel!
    @IBOutlet var cardImageView: UIImageView!
    @IBOutlet var connectImageView: UIImageView!
        
    @IBOutlet var disconnectView: UIView!
    @IBOutlet var exclamImageView: UIImageView!
    @IBOutlet var disconnectLabel: UILabel!
    @IBOutlet var connectButton: UIButton!
    
    @IBOutlet var cardIconBottomConstraint: NSLayoutConstraint!
    
    var callback: OBUConnectCallback?
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.roundCorners([.topLeft, .topRight], radius: 30)
        self.shadowToTopOfView()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configUI()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        configUI()
    }
    
    @IBAction @objc func onConnectOBU(_ sender: Any) {
        //  Try connect current paired OBU
        self.connectButton.isEnabled = false
        self.connectButton.setTitle("Connecting", for: .normal)
        self.connectButton.backgroundColor = .lightGray
        
        OBUHelper.shared.autoConnectOBUFromCarplay = false
        OBUHelper.shared.setManuallyConnectObu(true)
        OBUHelper.shared.autoConnectOBU { [weak self] success, error in
            DispatchQueue.main.async {
                self?.callback?(success)
                self?.connectButton.isEnabled = true
                self?.connectButton.setTitle("Connect", for: .normal)
                
                OBUHelper.shared.setManuallyConnectObu(false)
                self?.updateUI()
            }
        }
    }
    
    private func configUI() {
        Bundle.main.loadNibNamed("OBUConnectView", owner:self, options:nil)
        addSubview(contentView)
        contentView.frame = bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        layer.cornerRadius = 30
        disconnectView.layer.cornerRadius = 30
        
        connectButton.layer.cornerRadius = 14
        connectButton.layer.masksToBounds = true
        
        titleLabel.textColor = UIColor(hexString: "#778188", alpha: 1.0)
        balanceLabel.textColor = UIColor(hexString: "#15B765", alpha: 1.0)
        connectLabel.textColor = UIColor(hexString: "#778188", alpha: 1.0)
        disconnectLabel.textColor = UIColor(hexString: "#778188", alpha: 1.0)
        
        titleLabel.font = UIFont.SFProRegularFont()
        balanceLabel.font = UIFont.SFProRegularFont()
        connectLabel.font = UIFont.SFProRegularFont()
        disconnectLabel.font = UIFont.SFProRegularFont()
        
        updateUI()
    }
    
    func updateUI() {
                
        let isConnnected = OBUHelper.shared.isObuConnected()
        
        disconnectView.isHidden = isConnnected
        titleLabel.isHidden = !isConnnected
        balanceLabel.isHidden = !isConnnected
        cardImageView.isHidden = !isConnnected
        connectImageView.isHidden = !isConnnected
       
        cardImageView.image = (OBUHelper.shared.getPaymentMode() == .backend && OBUHelper.shared.isNoCard) ? UIImage(named: "obu_bank_card") : UIImage(named: "obu_card")
        connectImageView.image = UIImage(named: "greenTick")
        exclamImageView.image = UIImage(named: "exclamation_circle")
        
        self.connectButton.backgroundColor = UIColor(hexString: "#782EB1", alpha: 1.0)
        
        if isConnnected {
            //  If payment mode is backend then not show card balance
            balanceLabel.isHidden = (OBUHelper.shared.getPaymentMode() == .backend && OBUHelper.shared.isNoCard)
            
            //  If balance less than $5
            if OBUHelper.shared.isLowBalance() {
                balanceLabel.textColor = UIColor(hexString: "#E82370", alpha: 1.0)
            } else {
                balanceLabel.textColor = UIColor(hexString: "#15B765", alpha: 1.0)
            }
            
            let balance = OBUHelper.shared.getCardBalance()
            if OBUHelper.shared.getCardStatus() == .detected {
                if !balance.isEmpty {
                    balanceLabel.text = "$\(balance)"
                } else {
                    balanceLabel.text = "$-"
                }
                
            } else {
                if OBUHelper.shared.getObuMode() != .device {
                    balanceLabel.text = "$\(OBUHelper.shared.getCardBalance())"
                } else {
                    balanceLabel.textColor = UIColor(hexString: "#E82370", alpha: 1.0)
                    balanceLabel.text = "$-"
                }
                
            }
        }
        
        titleLabel.text = (OBUHelper.shared.getPaymentMode() == .backend && OBUHelper.shared.isNoCard) ? "Bankcard" : "Card Balance"
        connectLabel.text = "Connected"
        disconnectLabel.text = "Not Connected"
        connectButton.setTitle("Connect", for: .normal)
        
        self.setNeedsLayout()
        self.layoutIfNeeded()
    }
}


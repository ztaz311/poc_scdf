//
//  ArrivalVC.swift
//  Breeze
//
//  Created by Zhou Hao on 23/3/21.
//

import UIKit
@_spi(Restricted) import MapboxMaps
import MapboxCoreMaps

class ArrivalVC: UIViewController {

    @IBOutlet weak var arrivedContainer: UIView!
    
    weak var arriveDetailsVC: ArriveDetailsVC!

    // MARK: - Properties from RoutePlanningVC to decide if it's a new easybreezy or not
//    var destinationAddress: SearchAddresses?
//    var easyBreezyAddress: EasyBreezyAddresses?
//    var addressID = ""
    var selectedAddress: BaseAddress!
    
    var mapView: MapView!
    var location:CLLocationCoordinate2D!
    var calloutView: CustomCalloutView!
    
    //beta.14 change
    internal lazy var pointAnnotationManager: PointAnnotationManager = {
        return mapView.annotations.makePointAnnotationManager()
        }()
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //beta.14 change
        let accessToken = ResourceOptionsManager.default.resourceOptions.accessToken
//        guard let accessToken = CredentialsManager.default.accessToken else {
//            fatalError("Access token not set")
//        }

        guard !accessToken.isEmpty else {
            fatalError("Empty access token")
        }

        let resourceOptions = ResourceOptions(accessToken: accessToken )  //till here beta.14 change
                
        mapView = MapView(frame: view.bounds, mapInitOptions: MapInitOptions(resourceOptions: resourceOptions))
        view.addSubview(mapView)
        
        mapView.layer.zPosition = -1
        mapView.isUserInteractionEnabled = false

    
            //beta.9 change
            mapView.ornaments.options.scaleBar.visibility = .hidden
            mapView.ornaments.options.compass.visibility = .hidden
            
            /*mapOptions.ornaments.scaleBarVisibility = .hidden
            mapOptions.ornaments.compassVisibility = .hidden*/
       
        mapView.ornaments.options.attributionButton.visibility = .hidden
        mapView.mapboxMap.onNext(event: .styleLoaded) { [weak self] _ in
            
            guard let self = self else { return }
            
            var lat = 0.0
            var long = 0.0
            var name = ""
            
            if let address = self.selectedAddress {
                lat = address.lat?.toDouble() ?? 0
                long = address.long?.toDouble() ?? 0
                name = address.address1 ?? ""
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.location = CLLocationCoordinate2D(latitude: lat, longitude: long)
                
                //beta.9 change
                self.mapView.mapboxMap.setCamera(to: CameraOptions(center: self.location, zoom: 13))
                //self.mapView.camera.setCamera(to: CameraOptions(center: self.location, zoom: 13), animated: true, completion: nil)
                                
                self.setupAnnotation(location: self.location, name: name)
            }
        }
        
        mapView.mapboxMap.onEvery(event: .cameraChanged) { [weak self] _ in
            guard let self = self else { return }
            self.adjustCallout()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupDarkLightAppearance()
        toggleDarkLightMode()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        let frame = CGRect(x: 0, y: arriveDetailsVC.view.bounds.height - 20, width: self.view.bounds.width, height: self.view.bounds.height - arriveDetailsVC.view.bounds.height + 20)
        self.mapView.frame = frame
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        if let vc = segue.destination as? ArriveDetailsVC {
            arriveDetailsVC = vc
            arriveDetailsVC.delegate = self
            arriveDetailsVC.selectedAddress = selectedAddress
        }
    }
    
    private func toggleDarkLightMode() {
        mapView.mapboxMap.style.uri = StyleURI(url: URL(string: isDarkMode ? Constants.Map.basicNightStyle: Constants.Map.basicStyle)!)
    }

    // MARK: - Theme updated
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        guard previousTraitCollection != traitCollection else {
            return
        }
        toggleDarkLightMode()
    }
    
    private func adjustCallout() {
        guard let callout = calloutView else { return }
        
        guard let screenCoordinate = mapView?.mapboxMap.point(for: location) else { return }
        let point = CGPoint(x: screenCoordinate.x, y: screenCoordinate.y)
        callout.center = CGPoint(x: point.x, y: point.y - 30)
    }

    private func setupAnnotation(location: CLLocationCoordinate2D, name: String) {
    
        pointAnnotationManager.annotations = []
        let isFavorite = selectedAddress is EasyBreezyAddresses
        var customPointAnnotation = PointAnnotation(coordinate: location)
        customPointAnnotation.image = .init(image: (!isFavorite ? UIImage(named: "newAddressPin") : UIImage(named: "destinationMark"))!, name: "CallOut")

        pointAnnotationManager.annotations = [customPointAnnotation]
        
        if let calloutView = self.calloutView {
            calloutView.removeFromSuperview()
        }
        let callOutView = CustomCalloutView.init(representedObject: customPointAnnotation)
        callOutView.presentCallout(from: self.mapView.frame, in: self.mapView, constrainedTo: callOutView.frame, animated: false)
        self.calloutView = callOutView
        self.adjustCallout()
    }
}

extension ArrivalVC: ArriveDetailsVCDelegate {
    func onComplete(for detailsVC: ArriveDetailsVC) {
        if let controllers = self.navigationController?.viewControllers {
            for controller in controllers {
                if controller.isKind(of: MapLandingVC.self) {
                    self.navigationController?.popToViewController(controller, animated: true)
                    break
                }
            }
        }
    }
    
    func onStartPark(for detailsVC: ArriveDetailsVC) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let vc = storyboard.instantiateViewController(withIdentifier: String(describing: FeatureNotReadyVC.self)) as? FeatureNotReadyVC {
            self.present(vc, animated: true, completion: nil)
        }
    }
}

extension ArrivalVC: AnnotationInteractionDelegate {
    func annotationManager(_ manager: AnnotationManager, didDetectTappedAnnotations annotations: [Annotation]) {
        
        //No need to handle this
    }
    
    
    public func didDeselectAnnotation(annotation: Annotation) {
        //No need to handle this
    }

    public func didSelectAnnotation(annotation: Annotation) {
        //No need to handle this
    }
}

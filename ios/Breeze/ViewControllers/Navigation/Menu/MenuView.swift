//
//  MenuView.swift
//  Breeze
//
//  Created by Malou Mendoza on 21/12/20.
//



import UIKit
import Instabug

protocol MenuViewDelegate: AnyObject {
//    func signOutSuccess()
    func accountClicked()
    func settingsClicked()
    func closeMenu()
    func travelLogClicked()
}

class MenuView: UIViewController,UICollectionViewDataSource, UICollectionViewDelegate {
    
    weak var delegate: MenuViewDelegate?
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrayOfMenuItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! MenuColelctionViewCell
        
        let menuDetail:[String:String] = self.arrayOfMenuItems[indexPath.row] as! [String : String]
        cell.itemLabel.text = menuDetail["itemName"]!
        cell.itemImg.image = (UIImage(named:menuDetail["itemImg"]!))
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout,sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        return CGSize(width: CGFloat((95)), height: CGFloat(97))
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if (collectionView.cellForItem(at: indexPath) as? MenuColelctionViewCell) != nil {
            
            if(indexPath.row == 0){
                
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.favorite, screenName: ParameterName.home_menu)
                
                let aComplexData = [
                    "toScreen": "SearchLocation", // Screen name you want to display when start RN
                    "navigationParams": [ // Object data to be used by that screen
                        "baseURL": appDelegate().backendURL,
                        "idToken":AWSAuth.sharedInstance.idToken,
                        "deviceos":parameters!["deviceos"],
                        "deviceosversion":parameters!["deviceosversion"],
                        "devicemodel":parameters!["devicemodel"],
                        "appversion":parameters!["appversion"]
                        
                    ],
                    "any other data": "Hi there",
                    "number": 100,
                    "arrayNumber": [1, 2, 3],
                    "arrayString": ["a", "b", "c"],
                    "object": [
                        "key1": "nice value",
                        "key2": [1, 2, 3]
                    ],
                    "arrayObject": []
                ] as [String : Any]
                let rootView = RNViewManager.sharedInstance.viewForModule(
                    "Breeze",
                    initialProperties: aComplexData)

                let reactNativeVC = UIViewController()
                reactNativeVC.view = rootView
                reactNativeVC.modalPresentationStyle = .fullScreen
                navigationController?.pushViewController(reactNativeVC, animated: true)
//                let storyboard: UIStoryboard = UIStoryboard(name: "MenuView", bundle: nil)
//                let vc: UIViewController = storyboard.instantiateViewController(withIdentifier: "EasyBreezyVC") as UIViewController
//                self.navigationController?.pushViewController(vc, animated: true)
                //close()
                
            }
            if(indexPath.row == 2){
                
                //self.delegate?.accountClicked()
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.settings, screenName: ParameterName.home_menu)
                self.delegate?.settingsClicked()
                //close()
            }
            
            if(indexPath.row == 1)
            {
                #if STAGING || TESTING
                self.delegate?.travelLogClicked()
                
                #else
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                if let vc = storyboard.instantiateViewController(withIdentifier: String(describing: FeatureNotReadyVC.self)) as? FeatureNotReadyVC {
                    self.present(vc, animated: true, completion: nil)
                }
                //close()
                #endif
                                
            }
            
        }
    }
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var menuCollectionView: UICollectionView!
    @IBOutlet var closeMenuBtn: UIButton!
    @IBOutlet var menuTopConstraint: NSLayoutConstraint!
    
    //temporary storage
    var arrayOfMenuItems : NSArray = [
        ["itemName": "Favourite", "itemImg": "easyBreezieIcon"],
        ["itemName": "Trip Log", "itemImg": "travelLogIcon"],
        ["itemName": "Settings", "itemImg": "settingssIcon"]
        
    ]
    let reuseIdentifier = "menuCell"
    
    var screenSize: CGRect!
    var screenWidth: CGFloat!
    var screenHeight: CGFloat!
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.view.backgroundColor =  UIColor.clear.withAlphaComponent(0.5)

        setupGradientColor()
        
        contentView.roundCorners([.topLeft, .topRight], radius: 30)
        
        screenSize = UIScreen.main.bounds
        screenWidth = screenSize.width
        screenHeight = screenSize.height
        
        self.menuCollectionView.delegate = self
        self.menuCollectionView.dataSource = self
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        let width = UIScreen.main.bounds.width
        layout.sectionInset = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
        layout.itemSize = CGSize(width: width / 4, height: width / 3)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        menuCollectionView!.collectionViewLayout = layout
        
        show()
        
    }
    
    func setupGradientColor() {
        let color1 = UIColor.menuGradientColor1.resolvedColor(with: traitCollection)
//        let color2 = UIColor.menuGradientColor2.resolvedColor(with: traitCollection)
        
        self.contentView.applyBackgroundGradient(with: "MenuBackground", colors: [color1.cgColor,color1.cgColor])
    }

    @IBAction func cloeMenu(_ sender: Any) {
        
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.close, screenName: ParameterName.home_menu)
       close()
    }
    
    func close() {
        hide()
        if (self.delegate != nil){
            self.delegate?.closeMenu()
            removeChildViewController(self)
        }
    }
    
    
    func show() {
        menuTopConstraint.constant = 50
        UIView.animate(withDuration: Values.standardAnimationDuration) {
            self.view.setNeedsLayout()
        }

//        UIView.animate(withDuration: 0.2) { [weak self] in
//            let frame = self?.contentView.frame
//            if let theSelf = self {
//                let yComponent = theSelf.menuTopConstraint.constant
//                theSelf.contentView.frame = CGRect(x: 0, y: yComponent, width: frame!.width, height: frame!.height)
////                theSelf.delegate?.sizeUpdates(at: yComponent)
//            }
//        }
      
    }
    
    // hide it in cruise mode
    func hide() {
        menuTopConstraint.constant =  UIScreen.main.bounds.height - closeMenuBtn.frame.maxY
        UIView.animate(withDuration: Values.standardAnimationDuration) {
            self.view.setNeedsLayout()
        }

//        UIView.animate(withDuration: 0.2) { [weak self] in
//            if let theSelf = self {
//                let frame = self?.contentView.frame
////                let yComponent = UIScreen.main.bounds.height - theSelf.panelMinHeight
//                let yComponent = UIScreen.main.bounds.height
//                theSelf.contentView.frame = CGRect(x: 0, y: yComponent, width: frame!.width, height: frame!.height)
////                theSelf.delegate?.sizeUpdates(at: yComponent)
//            }
//        }
    }
    
    // MARK: - Theme updated
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        guard previousTraitCollection != traitCollection else {
            return
        }
        setupGradientColor()
    }

}

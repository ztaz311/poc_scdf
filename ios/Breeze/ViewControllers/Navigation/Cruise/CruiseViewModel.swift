//
//  CruiseViewModel.swift
//  NewMapSDK
//
//  Created by Zhou Hao on 22/2/21.
//

import Foundation
import Turf
import MapboxCoreNavigation
import MapboxMaps
import MapboxCoreMaps
import MapboxNavigation
import Combine
import SwiftyBeaver

protocol CruiseViewModelDelegate: AnyObject {
    func cruiseModeStarted(for viewModel: CruiseViewModel)
    func cruiseModeStopped(for viewModel: CruiseViewModel)
    func feature(detected: Bool)
}

final class CruiseViewModel {
        
    // MARK: - Constants
    private let topPadding: CGFloat = 48
    
    // MARK: - Properties
    weak var navMapView: NavigationMapView!
    private var roadNamePanel: RoadNamePanel?
    private var cruiseNotificationVC: CruiseNotificationVC?
    private var currentFeature: FeatureDetectable? {
        didSet {
            DispatchQueue.main.async {
                self.updateNotification(featureId: self.currentFeature == nil ? oldValue?.id : self.currentFeature!.id)
            }
        }
    }
    
    weak var delegate: CruiseViewModelDelegate?
    private var speedLimitView: CustomSpeedLimitView?
    private var muteButton: ToggleButton?
    private var liveLocationButton: LiveLocationSharingButton! //Set it up after mute button
    private (set) var isStarted: Bool = false
    private var lastLocation: CLLocation?
    private var voiceInstructor: VoiceInstructor!
    private var isMute = false
    private var tripLogger: TripLogger!
    var liveLocTripId = ""
    var isTrackingUser = true
    
    // MARK: - CarPlay related properties
    weak var carPlayMapView:NavigationMapView!
    private var carPlayCancellable: AnyCancellable?    
    var cpRoadNamePanel: RoadNamePanel? //For Car Play adding road name panel
    var cpSpeedLimitView: CustomSpeedLimitView?
    
//    init(navMapView: NavigationMapView,carPlayMapView:NavigationMapView? = nil) {
    init(navMapView: NavigationMapView) {
        self.navMapView = navMapView
//        self.carPlayMapView = carPlayMapView
        SwiftyBeaver.self.debug("CruiseMode init")
        
        voiceInstructor = VoiceInstructor()
    }
    
    deinit {
        SwiftyBeaver.self.debug("CruiseMode deinit")
    }
    
    public func start() {
        guard isStarted == false else {
            return
        }
        isStarted = true
        isTrackingUser = true
        // TODO: change the style will make addLayer not work
//        navMapView.mapView.style.styleURL = .custom(url: URL(string: "mapbox://styles/breezemap/ckie20a1s38oa19od98q65jii")!)

        SwiftyBeaver.self.info("Cruise mode started!")
        tripLogger = TripLogger(service: TripLogService(), isNavigation: false)
        
        // Beta.15
        if let location = self.navMapView.mapView.location.latestLocation {
            getAddressFromLocation(location: CLLocation(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)) { address in
                self.tripLogger.start(startLocation: location.coordinate, address1: address, address2: "")
            }
        }
        
        UIApplication.shared.isIdleTimerDisabled = true
        
        setCamera(at: self.navMapView.mapView.location.latestLocation?.coordinate)
        
        carPlayCancellable = appDelegate().$carPlayConnected.sink(receiveValue: { [weak self] isConnected in
            guard let self = self else { return }
            if isConnected {
                self.carPlayMapView = appDelegate().carPlayManager.navigationMapView
                self.startCPCruise()
            } else {
                self.carPlayMapView = nil
                self.stopCPCruise()
            }
        })
            
        setupCustomUserLocationIcon()
        
//        AnalyticsManager.shared.logEvent(eventName: ParameterName.user_notification, eventValue: ParameterName.notification_open, screenName: ParameterName.home_cruise_mode)
        
        let topOffset: CGFloat = UIApplication.bottomSafeAreaHeight > 0 ? 0 : 15
        AnalyticsManager.shared.logEvent(eventName: ParameterName.Cruise.UserNotification.cruise_start, eventValue: ParameterName.Cruise.UserNotification.cruise_start, screenName: ParameterName.Cruise.screen_view)
        showToast(from: navMapView.mapView, message: Constants.toastStartCruise, topOffset: topOffset)
        setupSpeedLimitView()
        setupMuteButton()
        setupLiveLocationButton(show: true)
        delegate?.cruiseModeStarted(for: self)
    }
    
    public func stop() {
        guard isStarted == true else {
            return
        }
        isStarted = false
        
        UIApplication.shared.isIdleTimerDisabled = false
        SwiftyBeaver.info("Cruise mode stopped!")
        
        setCamera(at: self.navMapView.mapView.location.latestLocation?.coordinate)
        stopCPCruise()
        setupCustomUserLocationIcon()
                
        removeRoadNamePanel()
        hideToast(from: navMapView.mapView)
        hideToast(from: navMapView)
        removeSpeedLimitView()
        removeMuteButton()
        removeLiveLocationButton()
        removeCruiseNotification()
        
        ETATrigger.shared.sendLiveLocationCoordinates(latitude: (self.navMapView.mapView.location.latestLocation?.coordinate.latitude)!, longitude: (self.navMapView.mapView.location.latestLocation?.coordinate.longitude)!, status: .cancel)
        
        ETATrigger.shared.resetETA() //Clearing the old values
        
        // Beta.15
        if let location = self.navMapView.mapView.location.latestLocation {
            getAddressFromLocation(location: CLLocation(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)) { address in
                self.tripLogger.stop(endLocation: location.coordinate, address1: address, address2: "")
            }
        }
        delegate?.cruiseModeStopped(for: self)
        
        carPlayCancellable?.cancel()
    }
    
    public func updateTunnel(isTunnel:Bool){
                
        if(isTunnel == true)
        {
            let topOffset: CGFloat = UIApplication.bottomSafeAreaHeight > 0 ? 60 : 75
            DispatchQueue.main.async {
                showToast(from: self.navMapView, message: Constants.toastInTunnel, topOffset: topOffset, icon: "noInternetIcon", duration: 0)
            }
        } else {
            DispatchQueue.main.async {
                hideToast(from: self.navMapView)
            }
        }
    }
    
    public func update(roadName: String,speedLimit:Double, location: CLLocation?, rawLocation: CLLocation?) {
        
        self.showRoad(roadName)
        speedLimitView?.speed = speedLimit
        
        SwiftyBeaver.debug("Cruise Speed = \(speedLimit)")
        
        updateCPSpeedLimit(speedLimit)
        
        if let location = location {
            // Based on what Mapbox suggested, we're using location instead of rawLocation
            updateLocation(location)
            
            etaCallbacks(location: location)
            
            tripLogger.update(location: location.coordinate)
        }
    }
    
    public func etaCallbacks(location:CLLocation){
        
        if let tripETA = ETATrigger.shared.tripCruiseETA{
            
            if  isLiveLocationSharing() && tripETA.shareLiveLocation.uppercased() == "Y" && !ETATrigger.shared.liveLocTripId.isEmpty  {
                
//                if(ETATrigger.shared.lastSendLocation == nil || ETATrigger.shared.lastSendLocation != location)
//                {
                    //ETATrigger.shared.lastSendLocation = location
                    ETATrigger.shared.sendLiveLocationCoordinates(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude, status: .inprogress)
                //}
                
                
            }
            
            if(LocationManager.shared.getDistanceInMeters(fromLoc: location, toLoc: CLLocation(latitude: tripETA.tripDestLat, longitude: tripETA.tripDestLong)) <= Values.etaCruiseDistance && !ETATrigger.shared.isReminder1Sent)
            {
                ETATrigger.shared.etaReminder(reminderType: TripETA.ETAType.Reminder1)
            }
            
            
        }
        
    }
    
    private func isLiveLocationSharing() -> Bool {
        guard ETATrigger.shared.tripCruiseETA != nil || liveLocationButton != nil else { return false }
//        return eta.shareLiveLocation.uppercased() == "Y"
        return liveLocationButton.isStarted
    }
    
    public func updateFeature(feature: FeatureDetectable?) {
        if isStarted {
            self.currentFeature = feature
        }
    }
    
    public func removeFeature(featureId: String) {
        if isStarted {
            if currentFeature?.id == featureId {
                self.currentFeature = nil
            }
        }
    }
    
    public func enteredERP(_ erp: DetectedERP) {
//        tripLogger.exitErp(ERPCost(erpId: erp.id, zoneId: erp.zoneId, name: erp.address, cost: erp.price, enterTime: Date().timeIntervalSince1970, exitTime: 0))
    }
    
    public func exitedERP(_ erp: DetectedERP) {
        tripLogger.exitErp(ERPCost(erpId: Int(erp.id)!, name: erp.address, cost: erp.price, exitTime: Int(Date().timeIntervalSince1970), isOBU: "0"))
    }
    
    public func resetCamera() {
        if let coordinate = self.navMapView.mapView.location.latestLocation?.coordinate {
            setCamera(at: coordinate, heading: navMapView.mapView.cameraState.bearing, animated: true, reset: true)
        }

        resetCPCamera()
    }
    
    public func toggleMute() {
        
//        AnalyticsManager.shared.logClickEvent(eventValue: isMute ? ParameterName.unmute:ParameterName.mute, screenName: ParameterName.home_cruise_mode)
        isMute = !isMute
        let name = isMute ? ParameterName.Cruise.UserClick.mute: ParameterName.Cruise.UserClick.unmute
        AnalyticsManager.shared.logClickEvent(eventValue: name, screenName: ParameterName.Cruise.screen_view)
    }
                    
    private func setCamera(at coordinate: CLLocationCoordinate2D?, heading: Double = 0, animated: Bool = true, reset: Bool = false) {
//        print("new coordinate \(coordinate)")

        self.navMapView.navigationCamera.stop()
        navMapView.mapView.setCruiseCamera(at: coordinate, heading: heading, animated: animated, reset: reset, isStarted: isStarted)
        //Commenting this part of code as this can be achieved from MapView Extension
        /*let pitch: CGFloat = isStarted ? Values.defaultCruiseModePitch : 0
        let zoomLevel: CGFloat = isStarted ? Values.defaultCruiseModeZoomeLevel : Values.defaultMapZoomeLevel
                
        // TODO: This doesn't work any more in beta.16. Reopen #21 in Github
        // The top positive value move the cursor down. Use it as a workaround
        let bottom: CGFloat = -(self.navMapView.bounds.height * 0.5)
        let cameraOptions = CameraOptions(center: coordinate, padding: UIEdgeInsets(top: -bottom, left: 0, bottom: 0, right: 0), anchor: nil, zoom: zoomLevel, bearing: heading, pitch: pitch)

        // since only zoom level/pitch is animatable
        if reset {
            let options = CameraOptions(center: coordinate, padding: UIEdgeInsets(top: -bottom, left: 0, bottom: 0, right: 0), anchor: nil, zoom: zoomLevel - 0.1, bearing: heading, pitch: pitch)
            navMapView.mapView.camera.ease(to: options, duration: 1) { (complete) in
                
            }
            
        }
        navMapView.mapView.camera.ease(to: cameraOptions, duration: 1) { (complete) in
            
        }*/
 
    }
    
//    private func cameraAnimationComplete() {
//        // Do whatever we need after camera animation
//
//        // check immediately
//        checkFeatures(at: navMapView.mapView.centerCoordinate.location)
//    }
    
    func setupCustomUserLocationIcon() {
        self.navMapView.mapView.setUpCruiseCustomUserLocationIcon(isStarted: isStarted)
        setupCPCustomUserLocationIcon()
    }
            
    // MARK: - Road name
    private func createRoadNamePanel() {
        roadNamePanel = RoadNamePanel(frame: CGRect(x: 0, y: 0, width: 170, height: 40))
        self.navMapView.addSubview(roadNamePanel!)
        self.navMapView.bringSubviewToFront(roadNamePanel!)
        
        let minW = UIScreen.main.bounds.width > 400 ? 220 : 190
        roadNamePanel!.translatesAutoresizingMaskIntoConstraints = false
        roadNamePanel!.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.bottom.equalTo(self.navMapView.snp.bottom).offset(-90)
            make.width.greaterThanOrEqualTo(170)
            make.width.lessThanOrEqualTo(minW)
            make.height.equalTo(40)
        }
        
        updateRoadPanelConstraint(isHidden: true)
        roadNamePanel?.setNeedsLayout()
        roadNamePanel?.layoutIfNeeded()
    }
    
    private func removeRoadNamePanel() {
        if let view = roadNamePanel {
            view.removeFromSuperview()
            roadNamePanel = nil
        }
    }
    
    private func showRoad(_ name: String) {
        if roadNamePanel == nil {
            createRoadNamePanel()
        }
                
        let hide = name.isEmpty
        let needUpdate = roadNamePanel!.text() != name
        
        showCPRoadName(name)

        roadNamePanel!.set(name)        
        
        // animation
        if needUpdate {
            UIView.animate(withDuration: Values.standardAnimationDuration) {
                [weak self] in
                self?.updateRoadPanelConstraint(isHidden: hide)
            }
        }
    }
    
    private func updateRoadPanelConstraint(isHidden: Bool) {
        roadNamePanel!.snp.updateConstraints { (make) in
           // make.centerY.equalTo(speedLimitView!.snp.centerY).offset(isHidden ? 120 : 0)
            make.bottom.equalTo(self.navMapView.snp.bottom).offset(isHidden ? 0 : -90)
        }
        
        if(carPlayMapView != nil)
        {
            cpRoadNamePanel?.isHidden = isHidden
        }
    }
        
    // MARK: - Speed limit view
    private func setupSpeedLimitView() {

        if speedLimitView == nil {
            speedLimitView = CustomSpeedLimitView(frame: CGRect(x: 0, y: 0, width: 70, height: 70))
        
            navMapView.mapView.addSubview(speedLimitView!)
            speedLimitView?.speed = 0
            
            speedLimitView!.translatesAutoresizingMaskIntoConstraints = false
            speedLimitView!.snp.makeConstraints { (make) in
                make.leading.equalToSuperview().offset(24)
                make.top.equalTo(navMapView.snp.top).offset(topPadding)
                make.size.equalTo(CGSize(width: 70, height: 70))
            }
        }
    }
    
    // MARK: Mute button
    private func setupMuteButton() {
        if self.muteButton == nil {
            let muteButton = ToggleButton(frame: CGRect(x: 0, y: 0, width: 48, height: 48))
            muteButton.onImage = Images.unmuteImage
            muteButton.offImage = Images.muteImage
            
            muteButton.onToggled = { isSelected in
                self.toggleMute()
            }
            navMapView.mapView.addSubview(muteButton)
            self.muteButton = muteButton
            
            self.muteButton!.translatesAutoresizingMaskIntoConstraints = false
            self.muteButton!.snp.makeConstraints { make in
                make.trailing.equalToSuperview().offset(-24)
                make.top.equalTo(navMapView.snp.top).offset(topPadding)
                make.size.equalTo(CGSize(width: 48, height: 48))
            }
        }
    }
    
    //MARK: - LiveLocationButton and actions
    private func setupLiveLocationButton(show: Bool) {
        guard show else { return }

        if self.liveLocationButton == nil {
            let etaButton = LiveLocationSharingButton(frame: CGRect(x: 0, y: 0, width: 62, height: 62))
            etaButton.onImage = Images.liveLocationResumeImage
            etaButton.offImage = Images.liveLocationPauseImage
            //etaButton.backgroundColor = .white
            etaButton.layer.cornerRadius = 31
            var isFirstTime: Bool = true
            etaButton.onToggled = { [weak self] isStarted in
                guard let self = self else { return }
            
                let name = isStarted ? ParameterName.Cruise.UserClick.eta_main_button: ParameterName.Cruise.UserClick.eta_pause
                AnalyticsManager.shared.logClickEvent(eventValue: name, screenName: ParameterName.Cruise.screen_view)
                
                // Don't show toast the first time
                if isFirstTime {
                    isFirstTime = false
                } else {
                    DispatchQueue.main.async { [self] in
                        var  offset:CGFloat = 0
                        if self.currentFeature != nil {
                            offset =  self.topPadding + 95
                        }
                             
                        else
                        {
                            offset = UIApplication.bottomSafeAreaHeight > 0 ? 0 : 15
                        }
                           

                            if isStarted {
                                showToast(from: self.navMapView.mapView, message: Constants.toastLiveLocationResumed, topOffset: offset, icon: "toastLiveLocationResumed", messageColor: .black, duration: 3)
                                
            //                    AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.navigation_munute, screenName: ParameterName.navigation)

                            } else {
                                showToast(from: self.navMapView.mapView, message: Constants.toastLiveLocationPaused, topOffset: offset, icon: "toastLiveLocationPaused", messageColor: .black, duration: 3)
                                
                                if let location = self.lastLocation {
                                    if !ETATrigger.shared.liveLocTripId.isEmpty {
                                        DataCenter.shared.startUpdatingUserTripLiveLocation(tripId: self.liveLocTripId, latitude: location.coordinate.latitude.toString(), longitude: location.coordinate.longitude.toString(), status: LiveLocationStatusType.pause.rawValue)
                                        ETATrigger.shared.updateLiveLocationStatus(status: .pause)
//                                        self.updateLiveLocationStatus(status: .pause)
                                    }
                                }

                    //                    AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.navigation_mute, screenName: ParameterName.navigation)
                            //}
                        }
                    }
                }
            }
                        
            navMapView.mapView.addSubview(etaButton)
            self.liveLocationButton = etaButton

            liveLocationButton.translatesAutoresizingMaskIntoConstraints = false
            liveLocationButton.snp.makeConstraints { make in
//                make.trailing.equalToSuperview().offset(-xTrailing+7)
                make.trailing.equalTo(muteButton!.snp.trailing).offset(7)
                make.top.equalTo(muteButton!.snp.bottom).offset(10)
                make.width.equalTo(62)
                make.height.equalTo(62)
            }
            
            //Check initially if ETA is not available, then hide the live location button
                  self.liveLocationButton.isHidden = true
                
        }
        
    }
    
    func setupETA() {
            
        if let tripETA = ETATrigger.shared.tripCruiseETA {
            
        
            ETATrigger.shared.lastLiveLocationUpdateTime = Date().timeIntervalSince1970
            TripETAService().sendTripCruiseETA(tripETA) { [weak self] result in
                guard let self = self else { return }
                                
                switch result {
                case .success(let tripETAResponse):
                    DispatchQueue.main.async {
                        // TODO: use parameter passed from route planning to decide if live location sharing is enabled and decide the liveLocationButton
                        // TODO: default based on the ETA session passed from Route planning
                        if tripETA.shareLiveLocation.uppercased() == "Y" {
                            self.liveLocationButton.isHidden = false
                            self.liveLocationButton.resume()
                        } else {
                            self.liveLocationButton.pause()
                            self.liveLocationButton.isHidden = true
                        }
                        print("tripETAUUID: \(tripETAResponse.tripEtaUUID)")
                        ETATrigger.shared.liveLocTripId = tripETAResponse.tripEtaUUID
                        
                        DataCenter.shared.subscribeToMessageInbox(tripETAId: ETATrigger.shared.liveLocTripId) { [weak self] message in
                            guard let self = self, !message.isEmpty else { return }
                            
//                            print("Receiving message: \(message)")
                           // if let top = self.cruiseNotificationVC {
                            var  offset:CGFloat = 0
                            if self.currentFeature != nil {
                                offset =  self.topPadding + 95
                            }
                            else
                            {
                                offset = UIApplication.bottomSafeAreaHeight > 0 ? 0 : 15
                            }
//                                let offset = top.view.bounds.height + top.view.frame.origin.y - self.navMapView.mapView.frame.origin.y - UIApplication.topSafeAreaHeight


                                let imageWithText = UIImage.generateImageWithText(text: getAbbreviation(name: tripETA.recipientName), font: UIFont(name: fontFamilySFPro.Bold, size: 20.0)!)
                                showToast(from: self.navMapView.mapView, message: message, topOffset: offset, image: imageWithText, messageColor: .black, font: UIFont(name: fontFamilySFPro.Regular, size: 20.0)!, duration: 5)
                            //}
                        }
                        // TODO: No start status
//                            DataCenter.shared.startUpdatingUserTripLiveLocation(tripId: tripETAResponse.tripEtaUUID, latitude: location.coordinate.latitude.toString(), longitude: location.coordinate.longitude.toString(), status: LiveLocationStatusType.inprogress.rawValue)
//                            self.updateLiveLocationStatus(status: .inprogress)
                    }
                case .failure(let error):
                    print(error.localizedDescription)
                }
            }
        } else {
            self.liveLocationButton.pause()
            self.liveLocationButton.isHidden = true
            ETATrigger.shared.isReminder1Sent = true
            // No ETA, so should not send Reminder
            
        }
    }

    private func removeSpeedLimitView() {
        guard let view = speedLimitView else {
            return
        }
        view.removeFromSuperview()
        speedLimitView = nil
    }
    
    private func removeLiveLocationButton() {
        guard let view = liveLocationButton else {
            return
        }
        view.removeFromSuperview()
        liveLocationButton = nil
    }
    
    private func removeMuteButton() {
        guard let view = muteButton else {
            return
        }
        view.removeFromSuperview()
        muteButton = nil
    }
    
    // MARK: - Notification and feature detection
    private func setupCruiseNotification() {
        let storyboard = UIStoryboard(name: "MapsLandingVC", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: String(describing: CruiseNotificationVC.self)) as! CruiseNotificationVC
        if let parentVC = navMapView.getViewController() {
            parentVC.addChildViewControllerWithView(childViewController: vc as UIViewController,toView: parentVC.view)
        }
        cruiseNotificationVC = vc
        cruiseNotificationVC?.onFeatureUpdated = { [weak self] (feature) in
            guard let self = self else { return }
            if let feature = feature {
                // only speak when there is new feature
                self.voiceInstruction(for: feature)
            }
        }
    }
    
    private func removeCruiseNotification() {
        if let vc = cruiseNotificationVC {
//            vc.update(nil)
            if let parentVC = navMapView.getViewController() {
                parentVC.removeChildViewController(vc)
            }
            cruiseNotificationVC = nil
        }
    }
            
    // featureId - Could be current feature id if currentFeature is not nil
    // or previous featureId if currentFeature is nil
    private func updateNotification(featureId: String?) {
        
        if cruiseNotificationVC == nil {
            setupCruiseNotification()
        }
        
        if let vc = self.cruiseNotificationVC {

//            vc.update(currentFeature)
            delegate?.feature(detected: currentFeature != nil)
            
            if currentFeature == nil && featureId != nil {
                // TODO: Should we check we only delete certain feature
                navMapView.mapView.removeSymbol(id: featureId!)
            }
            
            updateIconsLayout(withNotification: currentFeature != nil)
        }
    }
    
    private func updateIconsLayout(withNotification: Bool) {
        let top = withNotification ? topPadding + 128 + 16: topPadding 
        if let muteButton = self.muteButton {
            muteButton.snp.updateConstraints { make in
                make.top.equalTo(self.navMapView).offset(top)
            }
        }
        if let speedLimitIcon = self.speedLimitView {
            speedLimitIcon.snp.updateConstraints { make in
                make.top.equalTo(self.navMapView).offset(top)
            }
        }
    }
    
    func voiceInstruction(for feature: FeatureDetectable) {

        guard let coordinate = navMapView.mapView.location.latestLocation?.coordinate else {
            return
        }
        
        // only add once
        if let feature = feature as? DetectedSchoolZone {
            if case let .polygon(polygon) = feature.feature.geometry,
             let line = getNearestLine(coordinate: coordinate, polygon: polygon) {
                let midpoint = mid(line.coordinates[0], line.coordinates[1])
                navMapView.mapView.addSymbol(id: feature.id, name: "SchoolZoneSymbol", coordinate: midpoint)
            }
        } else if let feature = feature as? DetectedSilverZone {
            if case let .polygon(polygon) = feature.feature.geometry,
             let line = getNearestLine(coordinate: coordinate, polygon: polygon) {
                let midpoint = mid(line.coordinates[0], line.coordinates[1])
                navMapView.mapView.addSymbol(id: feature.id, name: "SilverZoneSymbol", coordinate: midpoint)
            }
        }

        if !isMute {
            voiceInstructor.speak(feature)
        }
    }
}

extension CruiseViewModel {

    // This should be done by MapBox tracking. But it doesn't work now.
    private func updateLocation(_ location: CLLocation) {
        SwiftyBeaver.self.debug("update location = \(location)")
        var heading: Double = 0
        var newLocation = location
        
#if targetEnvironment(simulator)
        if lastLocation != nil {
            heading = lastLocation!.coordinate.heading(to: location.coordinate)            
        }
        SwiftyBeaver.self.debug("simulation heading = \(heading)")
        newLocation = CLLocation(coordinate: location.coordinate, altitude: location.altitude, horizontalAccuracy: location.horizontalAccuracy, verticalAccuracy: location.verticalAccuracy, course: heading, speed: location.speed, timestamp: location.timestamp)        
#else
        heading = newLocation.course
        SwiftyBeaver.self.debug("device heading = \(heading)")
#endif
        
        lastLocation = newLocation
        
        if isTrackingUser {
            setCamera(at: newLocation.coordinate, heading: heading, animated: true)
            setCPCamera(at: newLocation.coordinate, isStarted: self.isStarted, heading: heading, animated: true)
        }
    }
}


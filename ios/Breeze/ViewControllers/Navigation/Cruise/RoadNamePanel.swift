//
//  RoadNamePanel.swift
//  AutoSizeView
//
//  Created by Zhou Hao on 8/3/21.
//

import UIKit
import SnapKit

class RoadNamePanel: UIView {

    var label:UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
                
        self.label = UILabel()
        self.label.textColor = UIColor(named: "roadNameTextColor")
        self.label.font = UIFont.roadNameBoldFont()
        self.label.textAlignment = .center
        self.label.numberOfLines = 1
        self.label.translatesAutoresizingMaskIntoConstraints = false
        addSubview(self.label)
        
        self.label.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().inset(8)
            make.trailing.equalToSuperview().inset(8)
            make.top.equalToSuperview().offset(10)
            make.bottom.equalToSuperview().offset(-10)
        }
    }
    
    override func layoutSubviews() {
        layer.cornerRadius = self.bounds.height / 2.0
        self.clipsToBounds = true
        self.backgroundColor = UIColor(named: "cruiseRoadNameColor")
//        applyViewgradient(colors: [UIColor.cruiseRoadNameColor1.cgColor,UIColor.cruiseRoadNameColor2.cgColor])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func set(_ text: String) {
        label.text = text.getLatinString()
        
//        self.setNeedsLayout()
        //self.layoutIfNeeded()
    }
        
    func isEmpty() -> Bool {
        guard let text = label.text else {
            return true
        }
        return text.isEmpty
    }
    
    func text() -> String {
        return label.text ?? ""
    }
}

//
//  FeatureDetector.swift
//  Breeze
//
//  Created by Zhou Hao on 8/1/21.
//

import Foundation
import MapboxMaps
import MapboxCoreNavigation
import MapKit
import Turf
import SwiftyBeaver

protocol FeatureDetectorDelegate: AnyObject {
    func featureDetector(_ detector: FeatureDetector, didUpdate feature: FeatureDetectable?)
    func featureDetector(_ detector: FeatureDetector, didExit forId: String)
    func featureDetector(_ detector: FeatureDetector, didEnter erp: DetectedERP)
    func featureDetector(_ detector: FeatureDetector, didExit erp: DetectedERP)
    func featureDetector(_ detector: FeatureDetector, isInTunnel: Bool)
}

// TODO: When to store.removeAllUserDefinedRoadObjects()???

final class FeatureDetector {
    
    // MARK: - Public properties
    weak var delegate: FeatureDetectorDelegate?
    
    // MARK: - Private properties
    private weak var matcher: RoadObjectMatcher!
    private weak var store: RoadObjectStore!
    private weak var roadGraph: RoadGraph!
    private var queue = PriorityQueue<FeatureDetectable>()
    
    #if DEMO
    var isApproaching = false
    var isAccidentShown = false
    var isERPShown = false
    var isPaidShown = false
    var isETAExpressShown = false
    var isSpeedShown = false
    var isSchoolShown = false
    var isParkingShown = false
    var isLeg2ParkingShown = false
    var isLeg2Level3Shown = false
    var isCyclist1Shown = false
    var isCyclist2Shown = false
    
    var isApproachingAccident = false
    var isApproachingERP = false
    var isApproachingPaid = false
    var isApproachingETAExpress = false
    var isApproachingSpeed = false
    var isApproachingSchool = false
    var isApproachingParking = false
    var isApproachingLeg2Parking = false
    var isApproachingLeg2Level3 = false
    var isApproachingCyclist1 = false
    var isApproachingCyclist2 = false

    private var lastUpdatedTime: TimeInterval = 0
    
    #endif

    private var notified300 = [String]()
    private var notified1000 = [String]()
    private var notified3000 = [String]()
    private var notifiedSeasonParking = [String]()
//    private var notified150 = [String]()

    // MARK: - Life circle
    init(roadObjectsStore: RoadObjectStore, roadObjectMatcher: RoadObjectMatcher, roadGraph: RoadGraph) {
        
        SwiftyBeaver.debug("FeatureDetector init")
        self.store = roadObjectsStore
        self.store.delegate = self
        self.matcher = roadObjectMatcher
        self.matcher.delegate = self
        self.roadGraph = roadGraph
        // start observer
        NotificationCenter.default.addObserver(self,
           selector: #selector(didUpdateElectronicHorizonPosition),
           name: .electronicHorizonDidUpdatePosition,
           object: nil)
        
        startUpdatingRoadObjects()
    }
    
    deinit {
        SwiftyBeaver.debug("FeatureDetector deinit")
        
        // stop observer
        NotificationCenter.default.removeObserver(self, name: .electronicHorizonDidUpdatePosition, object: nil)
        #if DEMO
        if(DemoSession.shared.isDemoRoute)
        {
            let featureCollection = DataCenter.shared.getDemoFeatures()
            for feature in featureCollection.features {
                if case let .string(id) = feature.properties?["id"] {
                    if case let .point(point) = feature.geometry {
                        store.removeAllUserDefinedRoadObjects()
                        store.onRoadObjectRemoved(forId: "\(FeatureDetection.featureTypeDemo)-\(id)")
                        
                    }
                }
            }
        }
        #endif
        stopUpdatingRoadObjects()
    }
    
    // MARK: - Private methods
    private func startUpdatingRoadObjects() {
        SwiftyBeaver.debug("FeatureDetector startUpdatingRoadObjects")
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(didEnterRoadObject),
                                               name: .electronicHorizonDidEnterRoadObject,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(didExitRoadObject),
                                               name: .electronicHorizonDidExitRoadObject,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(didRoadObjectPassed),
                                               name: .electronicHorizonDidPassRoadObject,
                                               object: nil)
    }
    
    private func stopUpdatingRoadObjects() {
        SwiftyBeaver.debug("FeatureDetector stopUpdatingRoadObjects")
        NotificationCenter.default.removeObserver(self, name: .electronicHorizonDidEnterRoadObject, object: nil)
        NotificationCenter.default.removeObserver(self, name: .electronicHorizonDidExitRoadObject, object: nil)
        NotificationCenter.default.removeObserver(self, name: .electronicHorizonDidPassRoadObject, object: nil)
    }
    
    @objc func didUpdateElectronicHorizonPosition(_ notification: Notification) {
                                
        if let roadObjects = notification.userInfo?[RoadGraph.NotificationUserInfoKey.distancesByRoadObjectKey] as? [DistancedRoadObject] {
            featureDetection(roadObjects: roadObjects)
        }
        
        // check tunnel
        if let horizon = notification.userInfo?[RoadGraph.NotificationUserInfoKey.treeKey] as? RoadGraph.Edge {
            checkTunnel(edge: horizon)
        }
    }
    
    private func checkTunnel(edge: RoadGraph.Edge) {
        guard let roadGraph = roadGraph,
            let metadata = roadGraph.edgeMetadata(edgeIdentifier: edge.identifier) else {
            self.delegate?.featureDetector(self, isInTunnel: false)
            return
        }
        
        let rDC = metadata.roadClasses
        self.delegate?.featureDetector(self, isInTunnel: rDC.contains(.tunnel))
    }
    
    private func featureDetection(roadObjects: [DistancedRoadObject]) {
        SwiftyBeaver.debug("FeatureDetector: \(roadObjects.count) objects, \(roadObjects)")
        queue.clear()
        for roadObject in roadObjects {
            switch roadObject {
            case .polygon(let identifier, _, let distanceToNearestEntry, _, _):
                // Beta.21 - distanceToNearestEntry is optional now. What should we deal with it? (#249)!!!
                // Based on what mapbox suggested, we should ignore the object if the distanceToNearestEntry is nil
                if let distanceToNearestEntry = distanceToNearestEntry {
                    if distanceToNearestEntry < FeatureDetection.range {
                        let components = identifier.components(separatedBy: "-")
                        if components.count > 1 {
                            let name = components[0]
                            if name == FeatureDetection.featureTypeSchoolZone {
                                SwiftyBeaver.debug("FeatureDetector: School zone detected in \(distanceToNearestEntry)" )
    //                            if let schoolZone = DataCenter.shared.getItem(SchoolZoneData.self, id: identifier) {
                                if let schoolZone = DataCenter.shared.getSchoolZoneItem(id: identifier) {
                                    if let feature = try? JSONDecoder().decode(Turf.Feature.self, from: schoolZone.featureJson.data(using: .utf8)!) {
                                        let distance = distanceToNearestEntry < 0 ? 0 : distanceToNearestEntry
                                        if shouldNotifiy(id: identifier, distance: distance) {
                                            queue.push(DetectedSchoolZone(id: identifier, distance: distanceToNearestEntry < 0 ? 0 : distanceToNearestEntry, priority: FeatureDetection.cruisePrioritySchoolZone, feature: feature))
                                        }
                                    }
                                }
                                
                            } else if name == FeatureDetection.featureTypeSilverZone {
                                SwiftyBeaver.debug("FeatureDetector: Silver zone detected in \(distanceToNearestEntry)" )
    //                            if let silverzone = DataCenter.shared.getItem(SilverZoneData.self, id: identifier) {
                                if let silverzone = DataCenter.shared.getSilverZoneItem(id: identifier) {
                                    if let feature = try? JSONDecoder().decode(Turf.Feature.self, from: silverzone.featureJson.data(using: .utf8)!) {
                                        let distance = distanceToNearestEntry < 0 ? 0 : distanceToNearestEntry
                                        if shouldNotifiy(id: identifier, distance: distance) {
                                            queue.push(DetectedSilverZone(id: identifier, distance: distanceToNearestEntry < 0 ? 0 : distanceToNearestEntry, priority: FeatureDetection.cruisePrioritySilverZone,feature: feature))
                                        }
                                    }
                                }
                            } /* else if name == FeatureDetection.featureTypeTraffic {
                                let idString = components[1]
                                let featureCollection = DataCenter.shared.getTrafficIncidentFeatures()
                                for feature in featureCollection.features {
                                    if let id = feature.properties?["id"] as? String {
                                        if id == idString {
                                            // 2. get the properties
                                            let message = feature.properties?["Message"] as? String ?? ""
                                            let type = feature.properties?["Type"] as? String ?? ""
                                            queue.push(DetectedTrafficIncident(id: id, distance: distanceToNearestEntry, priority: FeatureDetection.cruisePriorityTraffic, category: type, message: message))
                                            break
                                        }
                                    }
                                }

                            } */
                        }
                    }
                }
            case .point(identifier: let id, kind: _, distance: let distance):
                SwiftyBeaver.debug("FeatureDetector: EH point detected: \(id) : \(distance)m")
                
                if distance < FeatureDetection.range {
                    
                    let components = id.components(separatedBy: "-")
                    if components.count > 1 {
                        let name = components[0]
                        if name == FeatureDetection.featureTypeSpeedcamera {
                            let idString = components[1]
                            let featureCollection = DataCenter.shared.getAllSpeedCamera()
                            for feature in featureCollection.features {
                                if case let .string(id) = feature.properties?["id"] {
                                    if id == idString {
                                        // 2. get the properties
                                        if case let .string(type) = feature.properties?["Type"]{
                                            let distance = distance < 0 ? 0 : distance
                                            if shouldNotifiy(id: id, distance: distance) {
                                                queue.push(DetectedSpeedCamera(id: id, distance: distance < 0 ? 0 : distance, priority: FeatureDetection.cruisePrioritySpeedCamera, category: name))
                                                
                                            }
                                        break
                                        }
                                    }
                                }
                            }
                        } else if name == FeatureDetection.featureTypeSeasonParking {
                            guard let featureCollection = SelectedProfiles.shared.getSeasonParkingsFeatureCollection() else { return }
                            
                            for feature in featureCollection.features {
                                if case let .string(seasonParkingId) = feature.properties?["id"] {
                                    if seasonParkingId == id {
                                        let distance = distance < 0 ? 0 : distance
                                        // check only 15m for season parking
                                        if shouldNotifiySeasonParking(id: id, distance: distance) {
                                            queue.push(DetectedSeasonParking(id: id, distance: distance < 0 ? 0 : distance, priority: FeatureDetection.cruisePrioritySeasonParking))
                                        }
                                        break
                                    }
                                }
                            }
                        }
                        
                        //For Demo
                        #if DEMO
                        if name == FeatureDetection.featureTypeDemo {
                            
                            let idString = components[1]
                            let featureCollection = DataCenter.shared.getDemoFeatures()
                            for feature in featureCollection.features {
                                if case let .string(id) = feature.properties?["id"] {
                                    if id == idString {
                                        
                                        // 2. get the properties
                                        if case let .string(message) = feature.properties?["Message"],
                                           case let .string(type) = feature.properties?["Type"]{
                                        
                                        print("Feature Type %@",type)
                                        if(type == Values.wayPointParkingAPI)
                                        {
                                            //print("Feature Type %@",Values.wayPointParkingAPI)
                                            
                                              DemoSession.shared.startTimerToGetParkingLots()
                                            
                                        }
                                        
                                        if distance <= 200 {
                                            if type == Values.wayPointCyclist1 {
                                                isApproachingCyclist1 = true
                                            } else if type == Values.wayPointCyclist2 {
                                                isApproachingCyclist2 = true
                                            }
                                        }
                                        
                                        if(distance < 50)
                                        {
                                           
                                            if(type == Values.wayPointAccident)
                                            {
                                                
                                              isApproachingAccident = true
                                            }
                                            else if(type == Values.wayPointERP)
                                            {
                                                isApproachingERP = true
                                            }
                                            
                                            else if(type == Values.wayPointPaid)
                                            {
                                                isApproachingPaid = true
                                            }
                                            
                                            else if(type == Values.wayPointETAExpress)
                                            {
                                                print("Feature Type ETA Express %@",type)
                                                isApproachingETAExpress = true
                                            }
                                            
                                            else if(type == Values.wayPointSchool)
                                            {
                                                isApproachingSchool = true
                                            }
                                            
                                            else if(type == Values.wayPointSpeed)
                                            {
                                                isApproachingSpeed = true
                                            }
                                            
                                            else if(type == Values.wayPointRoad)
                                            {
                                                isApproachingParking = true
                                            }
                                            
                                            else if(type == Values.wayPointCarparkLots)
                                            {
                                                isApproachingLeg2Parking = true
                                            }
                                            
                                            else if(type == Values.wayPointCarParkLevel3)
                                            {
                                                isApproachingLeg2Level3 = true
                                            }
                                        }
                                        
                                        break
                                        }
                                    }
                                }
                            }
                        }
                        #endif
                    }
                }
                
                if distance < FeatureDetection.accidentRange {
                    let components = id.components(separatedBy: "-")
                    if components.count > 1 {
                        let name = components[0]
                        if name == FeatureDetection.featureTypeTraffic {
                            let idString = components[1]
                            let featureCollection = DataCenter.shared.getTrafficIncidentFeatures()
                            for feature in featureCollection.features {
                                if case let .string(id) = feature.properties?["id"] {
                                    if id == idString {
                                        // 2. get the properties
                                        if case let .string(message) = feature.properties?["Message"],
                                           case let .string(type) = feature.properties?["Type"],
                                           case let .point(point) = feature.geometry{
                                            let distance = distance < 0 ? 0 : distance
                                            if shouldNotify1km(id: id, distance: distance) {
                                                queue.push(DetectedTrafficIncident(id: id, distance: distance < 0 ? 0 : distance, priority: FeatureDetection.cruisePriorityTraffic, category: type, message: message))
                                                
                                            }
                                            break
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                break
            case .gantry(identifier: let identifier, kind: _, distance: let distance):
                if distance < FeatureDetection.range {
                    let components = identifier.components(separatedBy: "-")
                    if components.count > 1 {
                        let name = components[0]
                        let idString = components[1]
                        if name == FeatureDetection.featureTypeErp {
                            SwiftyBeaver.debug("FeatureDetector: ERP detected in \(distance)" )
                            let featureCollection = DataCenter.shared.getERPCostFeatures()
                            // 1. get the feature from the featureCollection
                            for feature in featureCollection.features {
                                if case let .string(id) = feature.properties?["erpid"] {
                                    let erpid = "\(id)"
                                    if erpid == idString {
                                        // 2. get the properties
                                        if case let .string(address) = feature.properties?["name"],
                                           case let .string(strCost) = feature.properties?["cost"],
                                           case let .string(zoneId) = feature.properties?["zoneid"]{
                                            let cost = strCost.replacingOccurrences(of: "$", with: "").toDouble() ?? 0.0
                                        if cost > 0 {
                                            let erp = DetectedERP(id: erpid, distance: distance < 0 ? 0 : distance, priority: FeatureDetection.cruisePriorityErp, address: address, price: cost, zoneId: zoneId)
                                            let distance = distance < 0 ? 0 : distance
                                            if shouldNotifiy(id: erpid, distance: distance) {
                                                queue.push(erp)
                                            }
                                            delegate?.featureDetector(self, didEnter: erp)
                                        }
                                        break
                                        }
                                    }
                                }
                            }
                            
                            // TODO: Reuse
                            let noCostFeatureCollection = DataCenter.shared.getERPNoCostFeatures()
                            for feature in noCostFeatureCollection.features {
                                if case let .string(id) = feature.properties?["erpid"] {
                                    let erpid = "\(id)"
                                    if erpid == idString {
                                        // 2. get the properties
                                        if case let .string(address) = feature.properties?["name"],
                                           case let .string(zoneId) = feature.properties?["zoneid"]{
                                        let erp = DetectedERP(id: erpid, distance: distance, priority: FeatureDetection.cruisePriorityErp, address: address, price: 0, zoneId: zoneId)
                                        delegate?.featureDetector(self, didEnter: erp)
                                        break
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                break
            case .subgraph(identifier: _, kind: _, distanceToNearestEntry: _, distanceToNearestExit: _, isInside: _):
                break
            case .line(identifier: _, kind: _, distanceToEntry: _, distanceToExit: _, distanceToEnd: _, isEntryFromStart: _, length: _):
                break
            }
        }
        // we only need to update one
        SwiftyBeaver.debug("FeatureDetector: didUpdate(\(queue.count)) \(queue.peek())")
        #if DEMO
        if(!DemoSession.shared.isDemoRoute)
        {
            self.delegate?.featureDetector(self, didUpdate: queue.peek())
        }
        #else
        self.delegate?.featureDetector(self, didUpdate: queue.peek())
        #endif
    }
    
    public func shouldNotify3km(id:String,distance:Double) -> Bool {
        
        if(distance <= 3000){
            if !notified3000.contains(id) {
                notified3000.append(id)
                return true
            }
            
        }
        return false
    }
    
    private func shouldNotifiySeasonParking(id: String, distance: Double) -> Bool {
        if distance < 15 {
            if !notifiedSeasonParking.contains(id) {
                notifiedSeasonParking.append(id)
                return true
            }
        }
        return false
    }
    
    private func shouldNotifiy(id: String, distance: Double) -> Bool {
        
//        if distance < 300 && distance > 150 {
        if distance < 300 {
            if !notified300.contains(id) {
                notified300.append(id)
                return true
            }
        }
//        else if distance < 150 {
//            if !notified150.contains(id) {
//                notified150.append(id)
//                return true
//            }
//        }
        return false
    }
    
    private func shouldNotify1km(id:String,distance:Double) -> Bool {
        if(distance <= 1000/* && distance >= 700*/){
            if !notified1000.contains(id) {
                notified1000.append(id)
                return true
            }
            
        }
        return false
    }
    
    
    @objc func didEnterRoadObject(_ notification: Notification) {
        guard
            let identifier = notification.userInfo?[RoadGraph.NotificationUserInfoKey.roadObjectIdentifierKey] as? RoadObject.Identifier,
            let didTransitionAtEndpoint = (notification.userInfo?[RoadGraph.NotificationUserInfoKey.didTransitionAtEndpointKey] as? NSNumber)?.boolValue
        else { return }
        
        SwiftyBeaver.debug("FeatureDetector: didEnterRoadObject \(identifier)" )
    }
    
    @objc func didExitRoadObject(_ notification: Notification) {
        guard
            let identifier = notification.userInfo?[RoadGraph.NotificationUserInfoKey.roadObjectIdentifierKey] as? RoadObject.Identifier,
            let didTransitionAtEndpoint = (notification.userInfo?[RoadGraph.NotificationUserInfoKey.didTransitionAtEndpointKey] as? NSNumber)?.boolValue
                
        else { return }
                
        // remove from notified array
        if let i = notified300.firstIndex(of: identifier) {
            notified300.remove(at: i)
        }
//        if let i = notified150.firstIndex(of: identifier) {
//            notified150.remove(at: i)
//        }

        SwiftyBeaver.debug("FeatureDetector: didExitRoadObject \(identifier)" )
        self.delegate?.featureDetector(self, didExit: identifier)

    }
    
    @objc func didRoadObjectPassed(_ notification: Notification) {
        guard
            let identifier = notification.userInfo?[RoadGraph.NotificationUserInfoKey.roadObjectIdentifierKey] as? RoadObject.Identifier
        else { return }

        // remove from notified array
        if let i = notified300.firstIndex(of: identifier) {
            notified300.remove(at: i)
        }
        
//        if let i = notified150.firstIndex(of: identifier) {
//            notified150.remove(at: i)
//        }

        SwiftyBeaver.debug("FeatureDetector: didRoadObjectPassed \(identifier)" )
        
        #if DEMO
        if(DemoSession.shared.isDemoRoute)
        {
            // TODO: Workaround to avoid multiple voice in notification. test on demo first
            // Multiple didRoadObjectPassed called by EH within one second
            let now = Date().timeIntervalSince1970
            guard now - lastUpdatedTime > 0.5 else { return }
            
            lastUpdatedTime = now
            
            findDemoObjectsPassed(identifier: identifier)
            findERPObjectsForDemo(identifier: identifier)
        }
        else
        {
            self.delegate?.featureDetector(self, didExit: identifier)
            findERPObjectsPassed(identifier: identifier)
        }
        #else
          self.delegate?.featureDetector(self, didExit: identifier)
          if identifier.contains(FeatureDetection.featureTypeErp){
            
              findERPObjectsPassed(identifier: identifier)
          }
          
        #endif
    }
    
    #if DEMO
    func findDemoObjectsPassed(identifier:RoadObject.Identifier){
        queue.clear()
        let components = identifier.components(separatedBy: "-")
        if components.count > 1 {
            let idString = components[1]
            let featureCollection = DataCenter.shared.getDemoFeatures()
            for feature in featureCollection.features {
                if case let .string(id) = feature.properties?["id"] {
                    if id == idString {
                        
                        if case let .string(message) = feature.properties?["Message"],
                           case let .string(type) = feature.properties?["Type"]{
                            if(type == Values.wayPointAccident || type == Values.wayPointSpeed)
                            {
                                if(type == Values.wayPointSpeed)
                                {
                                    if(!isSpeedShown && isApproachingSpeed)
                                    {
                                        queue.push(DetectedTrafficIncident(id: id, distance: 0, priority: FeatureDetection.cruisePriorityTraffic, category: type, message: "60 km/h"))
                                        isSpeedShown = true
                                        self.delegate?.featureDetector(self, didUpdate: queue.peek())
                                    }
                                    
                                }
                                else
                                {
                                    if(!isAccidentShown && isApproachingAccident)
                                    {
                                        queue.push(DetectedTrafficIncident(id: id, distance: 0, priority: FeatureDetection.cruisePriorityTraffic, category: type, message: message))
                                        isAccidentShown = true
                                        self.delegate?.featureDetector(self, didUpdate: queue.peek())
                                    }
                                    
                                }
                                
                                
                            }
                            else if(type == Values.wayPointERP || type == Values.wayPointPaid){
                                
                               
                                if(type == Values.wayPointERP)
                                {
                                    if(!isERPShown && isApproachingERP)
                                    {
                                        let erp = DetectedERP(id: id, distance: 150, priority: FeatureDetection.cruisePriorityErp, address: "Ayer Rajah ExpressWay", price: 2.0, zoneId: "")
                                        queue.push(erp)
                                        isERPShown = true
                                        self.delegate?.featureDetector(self, didUpdate: queue.peek())
                                    }
                                    
                                }
                                else
                                {
                                    if(!isPaidShown && isApproachingPaid){
                                        
                                        let erp = DetectedERP(id: id, distance: 150, priority: FeatureDetection.cruisePriorityErp, address: "Paid", price: 2.0, zoneId: "")
                                        queue.push(erp)
                                        isPaidShown = true
                                        self.delegate?.featureDetector(self, didUpdate: queue.peek())
                                    }
                                }
                                
                               
                            }
                            
                            else if(type == Values.wayPointETAExpress){
                                
                                if(!isETAExpressShown && isApproachingETAExpress)
                                {
                                    queue.push(DetectedTrafficIncident(id: id, distance: 0, priority: FeatureDetection.cruisePriorityTraffic, category: type, message: "Testing ETA Expressway"))
                                    isETAExpressShown = true
                                    self.delegate?.featureDetector(self, didUpdate: queue.peek())
                                }
                            }
                            else if(type == Values.wayPointSchool){
                                
                                if(!isSchoolShown && isApproachingSchool)
                                {
                                    queue.push(DetectedSchoolZone(id: id, distance:  0, priority: FeatureDetection.cruisePrioritySchoolZone, feature: feature))
                                    isSchoolShown = true
                                    self.delegate?.featureDetector(self, didUpdate: queue.peek())
                                }
                                
                            }
                            
                            else if(type == Values.wayPointRoad){
                                
                                if(!isParkingShown && isApproachingParking)
                                {
                                    queue.push(DetectedTrafficIncident(id: id, distance: 0, priority: FeatureDetection.cruisePriorityTraffic, category: type, message: message))
                                    isParkingShown = true
                                    DemoSession.shared.stopParkingTimer()
                                    self.delegate?.featureDetector(self, didUpdate: queue.peek())
                                }
                                
                            }
                        
                        else if(type == Values.wayPointCarparkLots){
                            
                            if(!isLeg2ParkingShown && isApproachingLeg2Parking)
                            {
                                queue.push(DetectedTrafficIncident(id: id, distance: 0, priority: FeatureDetection.cruisePriorityTraffic, category: type, message: message))
                                isLeg2ParkingShown = true
                                self.delegate?.featureDetector(self, didUpdate: queue.peek())
                            }
                            self.delegate?.featureDetector(self, didUpdate: queue.peek())
                            
                        }
                        
                        else if(type == Values.wayPointCarParkLevel3){
                            
                            if(!isLeg2Level3Shown && isApproachingLeg2Level3)
                            {
                                queue.push(DetectedTrafficIncident(id: id, distance: 0, priority: FeatureDetection.cruisePriorityTraffic, category: type, message: message))
                                isLeg2Level3Shown = true
                                self.delegate?.featureDetector(self, didUpdate: queue.peek())
                            }
                        }
                        
                        else if type == Values.wayPointCyclist1 {
                            if !isCyclist1Shown && isApproachingCyclist1 {
                                queue.push(DetectedTrafficIncident(id: id, distance: 0, priority: FeatureDetection.cruisePriorityTraffic, category: type, message: message))
                                isCyclist1Shown = true
                                self.delegate?.featureDetector(self, didUpdate: queue.peek())
                            }
                        } else if type == Values.wayPointCyclist2 {
                            if !isCyclist2Shown && isApproachingCyclist2 {
//                                queue.push(DetectedTrafficIncident(id: id, distance: 0, priority: FeatureDetection.cruisePriorityTraffic, category: type, message: message))
                                isCyclist2Shown = true
//                                self.delegate?.featureDetector(self, didUpdate: queue.peek())
                            }
                        }
                        break
                        }

                    }
                }
            }
        }
        
        
    }
    #endif
    
    func findERPObjectsPassed(identifier:RoadObject.Identifier){
        
        let components = identifier.components(separatedBy: "-")
        if components.count > 1 {
            let idString = components[1]
        
            let featureCollection = DataCenter.shared.getERPCostFeatures()
            for feature in featureCollection.features {
                if case let .string(id) = feature.properties?["erpid"] {
                    let erpid = "\(id)"
                    if erpid == idString {
                        // 2. get the properties
                        if case let .string(address) = feature.properties?["name"],
                           case let .string(zoneId) = feature.properties?["zoneid"],
                           case let .string(strCost) = feature.properties?["cost"]
                        {
                        let cost = strCost.replacingOccurrences(of: "$", with: "").toDouble() ?? 0.0
                        let erp = DetectedERP(id: erpid, distance: 0, priority: FeatureDetection.cruisePriorityErp, address: address, price: cost, zoneId: zoneId)
                        delegate?.featureDetector(self, didExit: erp)
                        break
                        }
                    }
                }
            }

            // TODO: Reuse
            let noCostFeatureCollection = DataCenter.shared.getERPNoCostFeatures()
            for feature in noCostFeatureCollection.features {
                if case let .string(id) = feature.properties?["erpid"] {
                    let erpid = "\(id)"
                    if erpid == idString {
                        // 2. get the properties
                        if case let .string(address) = feature.properties?["name"],
                           case let .string(zoneId) = feature.properties?["zoneid"]{
                        let erp = DetectedERP(id: erpid, distance: 0, priority: FeatureDetection.cruisePriorityErp, address: address, price: 0, zoneId: zoneId)
                        delegate?.featureDetector(self, didExit: erp)
                        break
                        }
                    }
                }
            }
        }
    }

    // MARK: - Public methods
    func addERPObjectsToRoadMatcher(){
                
        // TODO: remove old matched objects
        let featureCollection = DataCenter.shared.getERPCostFeatures()
        
        SwiftyBeaver.debug("FeatureDetector: addERPObjectsToRoadMatcher \(featureCollection.features.count) cost ERP" )
        
        for feature in featureCollection.features {
            if case let .string(id) = feature.properties?["erpid"] {
                if case let .lineString(line) = feature.geometry {
                    matcher.match(gantry: MultiPoint(line.coordinates), identifier: "\(FeatureDetection.featureTypeErp)-\(id)")
                }
            }
        }
        
        // TODO: Reuse
        let noCostFeatureCollection = DataCenter.shared.getERPNoCostFeatures()
        
        SwiftyBeaver.debug("FeatureDetector: addERPObjectsToRoadMatcher \(noCostFeatureCollection.features.count) no cost ERP" )
        
        for feature in noCostFeatureCollection.features {
            if case let .string(id) = feature.properties?["erpid"] {
                if case let .lineString(line) = feature.geometry{
                    matcher.match(gantry: MultiPoint(line.coordinates), identifier: "\(FeatureDetection.featureTypeErp)-\(id)")
                }
            }
        }
    }
    
    func removeERPObjectsFromRoadMatcher(){
        
        let featureCollection = DataCenter.shared.getERPCostFeatures()
        
        SwiftyBeaver.debug("FeatureDetector: removeERPObjectsToRoadMatcher \(featureCollection.features.count) cost ERP" )
        
        for feature in featureCollection.features {
            if case let .string(id) = feature.properties?["erpid"] {
                let identifier = "\(FeatureDetection.featureTypeErp)-\(id)"
                store.removeUserDefinedRoadObject(identifier: identifier)
                matcher.cancel(identifier: identifier)
                
            }
        }
        
        let noCostFeatureCollection = DataCenter.shared.getERPNoCostFeatures()
        
        SwiftyBeaver.debug("FeatureDetector: removeERPObjectsToRoadMatcher \(noCostFeatureCollection.features.count) no cost ERP" )
        
        for feature in noCostFeatureCollection.features {
            if case let .string(id) = feature.properties?["erpid"] {
                
                let identifier = "\(FeatureDetection.featureTypeErp)-\(id)"
                store.removeUserDefinedRoadObject(identifier: identifier)
                matcher.cancel(identifier: identifier)
                
            }
        }
        
    }
    
    func addSilverZoneObjectsToRoadMatcher(){
        // TODO: remove old matched objects
        
        let silverzones = DataCenter.shared.getAllSilverZone()
        
        SwiftyBeaver.debug("FeatureDetector: addSilverZoneObjectsToRoadMatcher \(silverzones.count) silverzones" )
        
        for silverzone in silverzones {
            let feature = try? JSONDecoder().decode(Turf.Feature.self, from: silverzone.featureJson.data(using: .utf8)!)
            if case let .polygon(polygon) = feature?.geometry {
                matcher.match(polygon: polygon, identifier: "\(silverzone.id)")
            }
        }
    }

    func addSchoolObjectsToRoadMatcher(){
        // TODO: remove old matched objects
        let schoolzones = DataCenter.shared.getAllSchoolZone()
        
        SwiftyBeaver.debug("FeatureDetector: addSchoolObjectsToRoadMatcher \(schoolzones.count) schoolzones" )
        
        for schoolzone in schoolzones {
            let feature = try? JSONDecoder().decode(Turf.Feature.self, from: schoolzone.featureJson.data(using: .utf8)!)
            if case let .polygon(polygon) = feature?.geometry {
                matcher.match(polygon: polygon, identifier: "\(schoolzone.id)")
            }
        }
        
//        let schoolzones = DataCenter.shared.getAllData(SchoolZoneData.self)
//        for schoolzone in schoolzones {
//            let feature = try? JSONDecoder().decode(Turf.Feature.self, from: schoolzone.featureJson.data(using: .utf8)!)
//            if let polygon = feature?.geometry.value as? Polygon {
//                matcher.match(polygon: polygon, identifier: "\(schoolzone.id)")
//            }
//        }
    }
    
    func addSpeedCameraObjectsToRoadMatcher(){
                
        // TODO: remove old matched objects
        let speedCamCollection = DataCenter.shared.getAllSpeedCamera()
        
        
        SwiftyBeaver.debug("FeatureDetector: addSpeedCameraObjectsToRoadMatch \(speedCamCollection.features.count) cameras" )
        
        for feature in speedCamCollection.features {
            
            
            if case let .string(speedCamID) = feature.properties?["id"] {
                if case let .point(point) = feature.geometry {
                    matcher.match(point: point.coordinates, identifier: "\(FeatureDetection.featureTypeSpeedcamera)-\(speedCamID)")
                }
            }
        }
    }
        
    func addTrafficIncidentObjectsToRoadMatch() {
        let featureCollection = DataCenter.shared.getTrafficIncidentFeatures()
        
        SwiftyBeaver.debug("FeatureDetector: addTrafficIncidentObjectsToRoadMatch \(featureCollection.features.count) incidents" )
        
        for feature in featureCollection.features {
            if case let .string(id) = feature.properties?["id"] {
                if case let .point(point) = feature.geometry {
                    
                    // We don't need to add 3km road objects to eHorizon as we are using Turf to measure the distance
                    if(!id.contains(TrafficIncidentCategories.FlashFlood) && !id.contains(TrafficIncidentCategories.RoadClosure) && !id.contains(TrafficIncidentCategories.MajorAccident) && !id.contains(TrafficIncidentCategories.HeavyTraffic)){
                        
                        //SwiftyBeaver.debug("FeatureDetector match: \(FeatureDetection.featureTypeTraffic)-\(id)")
                        matcher.match(point: point.coordinates, identifier: "\(FeatureDetection.featureTypeTraffic)-\(id)")
                    }
                    
                }
            }
        }
    }
    
    func addSeasonParkingsObjectsToRoadMatch() {
        guard let featureCollection = SelectedProfiles.shared.getSeasonParkingsFeatureCollection() else { return }
        
        SwiftyBeaver.debug("FeatureDetector: addSeasonParkingsObjectsToRoadMatch \(featureCollection.features.count) season parkings" )
        
        for feature in featureCollection.features {
            if case let .string(id) = feature.properties?["id"] {
                if case let .point(point) = feature.geometry {
                    matcher.match(point: point.coordinates, identifier: "\(id)")
                }
            }
        }
    }
    
    #if DEMO
    func addDemoObjectsToRoadMatch() {
        // TODO: remove old matched objects
        
        let featureCollection = DataCenter.shared.getDemoFeatures()
        
        SwiftyBeaver.debug("FeatureDetector: addDemoObjectsToRoadMatch \(featureCollection.features.count) incidents" )
        
        for feature in featureCollection.features {
            if case let .string(id) = feature.properties?["id"] {
                if case let .point(point) = feature.geometry {
//                    SwiftyBeaver.debug("FeatureDetector match: \(FeatureDetection.featureTypeTraffic)-\(id)")
                    matcher.match(point: point.coordinates, identifier: "\(FeatureDetection.featureTypeDemo)-\(id)")
                }
            }
        }
    }
    #endif
    
    func removeTrafficIncidentObjects(featureCollection: FeatureCollection) {
        SwiftyBeaver.debug("FeatureDetector: removeTrafficIncidentObjects \(featureCollection.features.count) incidents" )

        for feature in featureCollection.features {
            if case let .string(id) = feature.properties?["id"] {
                // We don't need to remove 3km road objects from road matcher as we haven't added them at all
                if(!id.contains(TrafficIncidentCategories.FlashFlood) && !id.contains(TrafficIncidentCategories.RoadClosure) && !id.contains(TrafficIncidentCategories.MajorAccident) && !id.contains(TrafficIncidentCategories.HeavyTraffic)){
                    
                    let identifier = "\(FeatureDetection.featureTypeTraffic)-\(id)"
                    store.removeUserDefinedRoadObject(identifier: identifier)
                    matcher.cancel(identifier: identifier)
                }
                
            }
        }
    }
    
    func removeSeasonParkingsObjects(featureCollection: FeatureCollection) {
        SwiftyBeaver.debug("FeatureDetector: removeSeasonParkingsObjects \(featureCollection.features.count) season parkings" )

        for feature in featureCollection.features {
            if case let .string(id) = feature.properties?["id"] {
                    
                let identifier = "\(id)"
                store.removeUserDefinedRoadObject(identifier: identifier)
                matcher.cancel(identifier: identifier)
            }
        }
    }

}

extension FeatureDetector: RoadObjectMatcherDelegate {
    func roadObjectMatcher(_ matcher: RoadObjectMatcher, didMatch roadObject: RoadObject) {
        SwiftyBeaver.debug("FeatureDetector didMatch: \(roadObject.identifier),\(roadObject.location)")
        
//        let featureCollection = DataCenter.shared.getDemoFeatures()
//
//        let components = roadObject.identifier.components(separatedBy: "-")
//        if components.count > 1 {
//            let idString = components[1]
//
//            for feature in featureCollection.features {
//                if let id = feature.properties?["id"] as? String {
//                    if(id == idString)
//                    {
//                        if let point = feature.geometry.value as? Point {
//                            SwiftyBeaver.debug("FeatureDetector Raw: \(point.coordinates) coordinates" )
//                        }
//                    }
//
//                }
//            }
//        }
        
        print("ROAD OBJECTS MATCH",roadObject)
        store.addUserDefinedRoadObject(roadObject)
    }
    
    func roadObjectMatcher(_ matcher: RoadObjectMatcher, didFailToMatchWith error: RoadObjectMatcherError) {
        SwiftyBeaver.debug("FeatureDetector didFailToMatchWith: \(error.localizedDescription)")
    }
    
    // Beta.15
    func roadObjectMatcher(_ matcher: RoadObjectMatcher, didCancelMatchingFor id: String) {
        SwiftyBeaver.debug("FeatureDetector didCancelMatchingFor: \(id)")
    }
    

}

extension FeatureDetector: RoadObjectStoreDelegate {
    func didAddRoadObject(identifier: RoadObject.Identifier) {
        SwiftyBeaver.debug("FeatureDetector didAddRoadObject: \(identifier)")
    }
    
    func didUpdateRoadObject(identifier: RoadObject.Identifier) {
        SwiftyBeaver.debug("FeatureDetector didUpdateRoadObject: \(identifier)")
    }
    
    func didRemoveRoadObject(identifier: RoadObject.Identifier) {
        SwiftyBeaver.debug("FeatureDetector didRemoveRoadObject: \(identifier)")
    }
}

extension FeatureDetector {
    
    func pushingToFeatureDetectorQueue(id:String,appendingID:String,distance:CLLocationDistance,point:CLLocationCoordinate2D){
        
        var category = ""
        var priority = 0
        if(id.contains(TrafficIncidentCategories.FlashFlood)){
            
            category = TrafficIncidentCategories.FlashFlood
            priority = FeatureDetection.cruisePriorityTrafficFlashFlood
        }
        
        if(id.contains(TrafficIncidentCategories.RoadClosure)){
            category = TrafficIncidentCategories.RoadClosure
            priority = FeatureDetection.cruisePriorityTrafficRoadClosure
        }
        
        if(id.contains(TrafficIncidentCategories.HeavyTraffic)){
            category = TrafficIncidentCategories.HeavyTraffic
            priority = FeatureDetection.cruisePriorityTrafficHeavyTraffic
        }
        
        if(id.contains(TrafficIncidentCategories.MajorAccident)){
            
            category = TrafficIncidentCategories.MajorAccident
            priority = FeatureDetection.cruisePriorityTrafficMajorAccident
        }
        getAddressFromLocation(location: CLLocation(latitude: point.latitude, longitude: point.longitude)) {address in
            
            self.queue.push(DetectedTrafficIncident(id: appendingID, distance: distance, priority: priority, category: category, message: address))
            
            self.delegate?.featureDetector(self, didUpdate: self.queue.peek())
            
            SwiftyBeaver.debug("FeatureDetector: didUpdate Major Incidents(\(self.queue.count)) \(self.queue.peek())")
        }
    }
    
    func notify3kmRoadObjects(featureCollection:Turf.FeatureCollection?,currentLocation:CLLocation){
        
        if let featureCollection = featureCollection {
            
            for feature in featureCollection.features {
                
                if case let .point(point) = feature.geometry,
                   case let .string(id) = feature.properties?["id"]{
                    
                   let distanceFromCurrentLocation = currentLocation.coordinate.distance(to: point.coordinates)
                    let appendingID = "\(FeatureDetection.featureTypeTraffic)-\(id)"
                    if(distanceFromCurrentLocation <= FeatureDetection.majorRoadObject3kmDistance){
                        
                        //For 3km
                        if(shouldNotify3km(id: appendingID, distance: distanceFromCurrentLocation)){
                            
                            SwiftyBeaver.debug("FeatureDetector: didUpdate Cruise Major Incidents 3km (\(appendingID))")
                            
                            self.pushingToFeatureDetectorQueue(id: id, appendingID: appendingID, distance: distanceFromCurrentLocation, point: point.coordinates)
                            
                        }
                        //For 1000m
                        else if (distanceFromCurrentLocation <= FeatureDetection.majorRoadObject1kmDistance){
                            
                            if(shouldNotify1km(id: appendingID, distance: distanceFromCurrentLocation)){
                                
                                SwiftyBeaver.debug("FeatureDetector: didUpdate Cruise Major Incidents 300m (\(appendingID))")
                                
                                self.pushingToFeatureDetectorQueue(id: id, appendingID: appendingID, distance: distanceFromCurrentLocation, point: point.coordinates)
                                
                            }
                        }
                        //For 300m
                        else if (distanceFromCurrentLocation <= FeatureDetection.majorRoadObjectShortDistance){
                            
                            if(shouldNotifiy(id: appendingID, distance: distanceFromCurrentLocation)){
                                
                                SwiftyBeaver.debug("FeatureDetector: didUpdate Cruise Major Incidents 300m (\(appendingID))")
                                
                                self.pushingToFeatureDetectorQueue(id: id, appendingID: appendingID, distance: distanceFromCurrentLocation, point: point.coordinates)
                                
                            }
                        }
                        else{
                            
                            //If distance is > 300, then remove the ID from notify300 array
                            if let i = notified300.firstIndex(of: appendingID) {
                                notified300.remove(at: i)
                                SwiftyBeaver.debug("FeatureDetector: didUpdate Cruise Major Incidents 300m remove (\(appendingID))")
                            }
                        }
                    }
                    else{
                        
                        //If distance is > 3000, then remove the ID from notify3000 array
                        if let i = notified3000.firstIndex(of: appendingID) {
                            notified3000.remove(at: i)
                            SwiftyBeaver.debug("FeatureDetector: didUpdate Cruise Major Incidents 3000m remove (\(appendingID))")
                        }
                    }
                }
                
            }
        }
    }
    
    func notify3kmRoadObjectsForNavigation(featureCollection:Turf.FeatureCollection?,currentLocation:CLLocation,lineString:LineString?){
        
        if let featureCollection = featureCollection, let lineString = lineString {
            
            for feature in featureCollection.features {
                
                if case let .point(point) = feature.geometry,
                   case let .string(id) = feature.properties?["id"]{
                    
                    // calculating distance from current location to point object
                    let distanceFromCurrentLocation = currentLocation.coordinate.distance(to: point.coordinates)
                    
                    let appendingID = "\(FeatureDetection.featureTypeTraffic)-\(id)"
                    
                    if(distanceFromCurrentLocation <= FeatureDetection.majorRoadObject3kmDistance){
                        
                        // getting closest coordinate from line string to point object
                        if let coord = lineString.closestCoordinate(to: point.coordinates)?.coordinate {
                            
                            // finding distance from point object to closest coordinate
                            let distance = point.coordinates.distance(to: coord)
                            
                            let closestCoordinateDistance = id.contains(TrafficIncidentCategories.FlashFlood) ? FeatureDetection.roadObjectCCDFlashFlood : FeatureDetection.roadObjectCCDOthers
                            
                             if(distance <= closestCoordinateDistance){
                                 
                                 if(shouldNotify3km(id: appendingID, distance: distanceFromCurrentLocation)){
                                     
                                     SwiftyBeaver.debug("FeatureDetector: didUpdate Major Incidents 3km (\(appendingID) \(distance)")
                                     
                                     self.pushingToFeatureDetectorQueue(id: id, appendingID: appendingID, distance: distanceFromCurrentLocation, point: point.coordinates)
                                 }
                                 //For 1000m
                                 else if (distanceFromCurrentLocation <= FeatureDetection.majorRoadObject1kmDistance){
                                     
                                     if(shouldNotify1km(id: appendingID, distance: distanceFromCurrentLocation)){
                                         
                                         SwiftyBeaver.debug("FeatureDetector: didUpdate Major Incidents 1000m (\(appendingID) \(distance)")
                                         
                                         self.pushingToFeatureDetectorQueue(id: id, appendingID: appendingID, distance: distanceFromCurrentLocation, point: point.coordinates)
                                         
                                     }
                                 }
                                 //For 300m
                                 else if (distanceFromCurrentLocation <= FeatureDetection.majorRoadObjectShortDistance){
                                     
                                     if(shouldNotifiy(id: appendingID, distance: distanceFromCurrentLocation)){
                                         
                                         SwiftyBeaver.debug("FeatureDetector: didUpdate Major Incidents 300m (\(appendingID) \(distance)")
                                         
                                         self.pushingToFeatureDetectorQueue(id: id, appendingID: appendingID, distance: distanceFromCurrentLocation, point: point.coordinates)
                                         
                                     }
                                 }
                                 else{
                                     
                                     //If distance is > 300, then remove the ID from notify300 array
                                     if let i = notified300.firstIndex(of: appendingID) {
                                         notified300.remove(at: i)
                                         SwiftyBeaver.debug("FeatureDetector: didUpdate Major Incidents 300m remove (\(appendingID))")
                                     }
                                 }
                             }
                        }
                    }
                    else{
                        
                        //If distance is > 3000, then remove the ID from notify3000 array
                        if let i = notified3000.firstIndex(of: appendingID) {
                            notified3000.remove(at: i)
                            SwiftyBeaver.debug("FeatureDetector: didUpdate Major Incidents 3000m remove (\(appendingID))")
                        }
                    }
                   
                   
                }
            }
            
        }
    }
}

/*
class FeatureDetector {
    weak var mapView: MapView!    
    
    typealias ParseFunction = (_ feature: QueriedFeature, _ coordinate: CLLocationCoordinate2D, _ isInside: Bool) -> Feature?
    
    init(mapView: MapView) {
        self.mapView = mapView
    }
    
//    func check(name: String, at location: CLLocation, parse: ParseFunction) -> Future<Turf.Feature> {
//        let checkAtPointFuture = mapView.visibleFeatures(at: location, layerName: name)
//        checkAtPointFuture.observe { (result) in
//            print(result)
//        }
//        return checkAtPointFuture
//    }
    
    func check(name: String, at location: CLLocation,complete:@escaping (Feature?) -> Void, parse: @escaping ParseFunction) {
        guard let mapView = mapView else {
            complete(nil)
            return
        }
        
        let coordinate = location.coordinate
        // check point first
        let point = mapView.mapboxMap.point(for: coordinate)
        mapView.visibleFeatures(at: point, styleLayers: [name], filter: nil) { (result) in
            
            switch result {
            case .success(let queriedFeatures):
                if queriedFeatures.count > 1 {
                    SwiftyBeaver.self.warning("\(queriedFeatures.count) founded")
                }
                if let feature = queriedFeatures.first {
                    complete(parse(feature, coordinate, true))
                    return
                } else {
                    
                }
            case .failure(let error):
                SwiftyBeaver.self.error(error)
            }
            
            self.checkInRect(name: name, at: location, complete: complete, parse: parse)
        }
    }
    
    private func checkInRect(name: String,at location: CLLocation, complete:@escaping (Feature?) -> Void, parse: @escaping ParseFunction) {
        
        let rect = getMonitorRange(at: location)
        mapView!.visibleFeatures(in: rect, styleLayers: [name], filter: nil) { (result) in
            switch result {
            case .success(let queriedFeatures):
                if queriedFeatures.count > 1 {
                    SwiftyBeaver.self.warning("\(queriedFeatures.count) features found in rect.")
                }
                
                /* When EHorizon Custom addCustomRaodObject is available we will remove this code
                let sorted = queriedFeatures.sorted {
                    FeatureDetector.nearestDistance(at: location.coordinate, from: $0) < FeatureDetector.nearestDistance(at: location.coordinate, from: $1)
                }
                if let feature = sorted.first {
                    SwiftyBeaver.self.debug("Feature found in rect. \(feature)")
                    // TODO: To make it more effecient, should pass distance to the parse function so that don't need to calculate again
                    
                    complete(parse(feature, location.coordinate, false))
                }*/
                
            case .failure(let error):
                SwiftyBeaver.self.error(error)
            }
            complete(nil)
        }
    }
    
    private func getMonitorRange(at location: CLLocation) -> CGRect {
        return FeatureDetector.createMonitorRectangle(at: location, in: mapView!)
    }
    
    static func createMonitorRectangle(at location: CLLocation, in map: MapView) -> CGRect {

        // method 1
        let point = map.screenCoordinate(for: location.coordinate)
        let u = map.metersPerPointAtLatitude(latitude: location.altitude)
//        let dy = (Values.detectionRange / u) * Double(cos(map.pitch))
        let dy = (Values.detectionRange / u) * cos(Double(map.cameraState.pitch.degreesToRadians))
        
        let dx = Values.detectionRange / u
        return CGRect(x: point.x -  dx / 2, y: point.y - dy, width: dx, height: dy)
//        return CGRect(x: point.x -  dx / 2, y: point.y - dx, width: dx, height: dx)

        // method 2: seems not accurate
/*
        let coordinate = location.coordinate
        
        // TODO: for the time being, we assume we always point to north by the tracking system
        let coordinate2 = coordinate.coordinate(at: 150, facing: map.bearing)
        
        let center = CLLocationCoordinate2D.geographicMidpoint(betweenCoordinates: [coordinate, coordinate2])
        
//        let region = MKCoordinateRegion(center: coordinate, latitudinalMeters: 150, longitudinalMeters: 40)
        let region = MKCoordinateRegion(center: center, latitudinalMeters: 150, longitudinalMeters: 150)
        
        let northEast = CLLocationCoordinate2D(latitude: region.center.latitude + region.span.latitudeDelta / 2,
                                               longitude: region.center.longitude + region.span.longitudeDelta / 2)
        
        let southWest = CLLocationCoordinate2D(latitude: region.center.latitude - region.span.latitudeDelta / 2,
                                               longitude: region.center.longitude - region.span.longitudeDelta / 2)
        
        let bounds = CoordinateBounds(southwest: southWest, northeast: northEast)
        return map.rect(for: bounds)
*/
    }
    
    private func calculateDistance(at coordinate: CLLocationCoordinate2D, from currentCoordinate: CLLocationCoordinate2D) -> Double {
        let coordinate0 = CLLocation(latitude: currentCoordinate.latitude, longitude: currentCoordinate.longitude)
        let coordinate1 = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
        return coordinate0.distance(from: coordinate1)
    }
    
    static func nearestDistance(at coordinate: CLLocationCoordinate2D, from feature: Turf.Feature) -> Double {

        if let line = feature.geometry.value as? LineString {
            return line.closestCoordinate(to: coordinate)?.distance ?? 0
        } else if let polygon = feature.geometry.value as? Polygon {
            
            // only check the outer ring
            var min = Double.infinity
            let outer = polygon.outerRing.coordinates
            if outer.count >= 3 {
                for i in 0...outer.count - 1 {
                    let first = outer[i]
                    let j = (i+1) % outer.count
                    let second = outer[j]
                    let line = LineString([CLLocationCoordinate2D(latitude: first.latitude, longitude: first.longitude), CLLocationCoordinate2D(latitude: second.latitude, longitude: second.longitude)])
                    if let coord = line.closestCoordinate(to: coordinate)?.coordinate {
                        let d = coord.distance(to: coordinate)
                        SwiftyBeaver.self.debug("distance(\(i),\(j))=\(d)")
                        if d < min {
                            min = d
                        }
                    }
                }
            }
            return min
        }
        
        // TODO: other type
        return 0.0
    }
    
    static private func nearestDistance(at coordinate: CLLocationCoordinate2D, from coordinates: [CLLocationCoordinate2D]) -> Double {
        let array = coordinates.map { $0.distance(to: coordinate) }
        let distances = array.sorted { $0 < $1 }
        if let d = distances.first {
            return d
        }
        return 0.0
    }
    
    
}
*/

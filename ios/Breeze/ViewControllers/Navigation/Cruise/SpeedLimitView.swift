//
//  SpeedLimitView.swift
//  Breeze
//
//  Created by Zhou Hao on 1/2/21.
//

import UIKit
import Combine
import SwiftyBeaver

final class CustomSpeedLimitView: XibView {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak private var labelSpeed: UILabel!
    private var isOverSpeed = false {
        didSet {
            isOverSpeed ? startAnimation() : stopAnimation()
        }
    }
    private var cancellable: Cancellable?
    private var animatedCount = 0
    private var animationStarted = false
    private let notOverSpeedImage = UIImage(named: "speedWithKMIcon")
    private let overSpeedImage = UIImage(named: "overSpeedWithKMIcon")
    
    var speed: Double = 0 {
        didSet {
            DispatchQueue.main.async {
                if self.isFromObuDisplay {
                    if self.isShowingMapViewFromObu {
                        self.isHidden = Int(self.speed) <= 0
                    } else {
                        self.isHidden = true
                    }
                } else {
                    self.isHidden = Int(self.speed) <= 0
                }
                
                self.labelSpeed?.text = String(format: "%.0f", self.speed)
            }
        }
    }
    
    var isFromObuDisplay: Bool = false
    var isShowingMapViewFromObu: Bool = false
    
    func updateOverSpeed(value: Bool) {
        isOverSpeed = value
        // TODO: This is for demo
        //        if isOverSpeed {
        //            self.labelSpeed.adjustsFontSizeToFitWidth = true
        //            self.labelSpeed.textColor = .red
        //        } else {
        //            self.labelSpeed.adjustsFontSizeToFitWidth = true
        //            self.labelSpeed.textColor = UIColor.rgba(r: 0, g: 120, b: 0, a: 1)
        //        }
    }
        
    private func startAnimation() {
        guard !animationStarted, self.imageView.image != overSpeedImage else { return } // animation is in progress and not already showing the overspeed
        
        SwiftyBeaver.debug("Overspeed - startAnimation")
        
        animationStarted = true
        animatedCount = 0
        cancellable = Timer.publish(every: 1, on: .main, in: .default)
            .autoconnect()
            .sink(receiveValue: { _ in
                
                self.animatedCount += 1
                if self.animatedCount > 4 {
                    self.cancellable?.cancel()
                    self.cancellable = nil
                    self.animationStarted = false
                    return
                }

                UIView.transition(with: self, duration: Values.standardAnimationDuration, options: .transitionCrossDissolve, animations: { [weak self] in
                    guard let self = self else { return }
                    self.imageView.image = self.notOverSpeedImage
                    
                }, completion: { _ in
                    SwiftyBeaver.debug("overspeed flickering \(self.animatedCount) times")
                    self.imageView.image = self.overSpeedImage
                })
            })
    }
    
    private func stopAnimation() {
        guard !animationStarted else { return } // if animation in progress, just ignore it
        
        // cancel the timer first to prevent it changes the icon again.
        // This will not stop timer
        cancellable?.cancel()
        cancellable = nil

        if notOverSpeedImage != self.imageView.image {
            SwiftyBeaver.debug("Not overspeed - stopAnimation")
            self.imageView.image = notOverSpeedImage
        }
    }
}

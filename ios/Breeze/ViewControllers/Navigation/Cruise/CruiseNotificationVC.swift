//
//  CruiseNotificationVC.swift
//  Breeze
//
//  Created by Zhou Hao on 5/1/21.
//

import UIKit
import SwiftyBeaver
import RxSwift

class CruiseNotificationVC: UIViewController {

    // MARK: - Constants
    let panelHeight: CGFloat = 161

    // MARK: - IBOutlet
    @IBOutlet weak var firstIcon: UIImageView!
    @IBOutlet weak var secondIcon: UIImageView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var lblSpeed: UILabel!
    @IBOutlet weak var leadingConstraintValue: NSLayoutConstraint!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var distanceLabelHeightConstraintValue: NSLayoutConstraint!
    @IBOutlet weak var containerHeightConstraintValue: NSLayoutConstraint!
    
    //    var cpFeatureImage:UIImage!
//    var cpFeatureNotificationAlert:CPNavigationAlert?
//    var cpFeatureTitleVariants = [String]()
//    var cpFeatureSubTitleVariants = [String]()
    
    // MARK: - Properties
    var currentFeature: FeatureDetectable? { // current feature (the first feature)
        willSet {
            if newValue?.id != currentFeature?.id {
                self.onFeatureUpdated?(newValue)
            }
        }
    }
    var onFeatureUpdated: ((_ feature: FeatureDetectable?) -> Void)?
    
    // MARK: UIViewController life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.roundCorners([.bottomLeft, .bottomRight], radius: 20)
        
//        self.containerView.applyViewgradient(colors: [UIColor.cruiseNotificationColor1.cgColor,UIColor.cruiseNotificationColor2.cgColor,UIColor.cruiseNotificationColor3.cgColor])
                
        // make sure the second icon will be behind the first one
        view.sendSubviewToBack(secondIcon)
    }
    
    override func viewWillAppear(_ animated: Bool) {
       super.viewWillAppear(animated)
        
        self.view.alpha = 0
    }
    
    func showFeature(_ feature: FeatureDetectable) {
        clear()
        
        self.currentFeature = feature
        
        showFirstFeature(feature)
        show(hide: false)

        setIcon(imageView: firstIcon, for: feature)
        // if the first feature is school zone/silver zone show second one with speed
        if feature is DetectedSchoolZone || feature is DetectedSilverZone {
            setSpeedIcon(imageView: secondIcon)
            secondIcon.image = UIImage()
            lblSpeed.isHidden = true
            lblPrice.isHidden = true
            leadingConstraintValue.constant = 18
            containerView.backgroundColor = UIColor.notificationSchoolAndSilverZoneBG
            lblPrice.isHidden = true
        } else if let feature = feature as? DetectedERP {
            // TODO: Show price
            secondIcon.image = UIImage()
            lblSpeed.isHidden = true
            lblPrice.isHidden = false
            lblPrice.text = "$\(String(format: "%.2f", feature.price))"
            lblType.text = feature.address
            leadingConstraintValue.constant = 18
            containerView.backgroundColor = UIColor.notificationERPBG
        } else if let feature = feature as? DetectedTrafficIncident {
            secondIcon.image = UIImage()
            lblSpeed.isHidden = true
            lblPrice.isHidden = true
            lblType.text = feature.category
            leadingConstraintValue.constant = 18
            containerView.backgroundColor = UIColor.notificationTrafficBG
        }else if let feature = feature as? DetectedSpeedCamera {
            secondIcon.image = UIImage()
            lblSpeed.isHidden = true
            lblPrice.isHidden = true
            lblType.text = feature.category
            leadingConstraintValue.constant = 18
            containerView.backgroundColor = UIColor.notificationTrafficBG
        }
        else {
            secondIcon.image = UIImage()
            lblSpeed.isHidden = true
            lblPrice.isHidden = true
            leadingConstraintValue.constant = 18
        }
        
        // add a timer to dismiss it
        DispatchQueue.main.asyncAfter(deadline: .now() + Values.notificationDuration) { [weak self] in
            guard let self = self else { return }
            self.show(hide: true)
        }
    }
/*
    func update(_ feature: FeatureDetectable?) {
        clear()
        
        self.currentFeature = feature
        
        if let feature = feature {
            show(hide: false)
            showFirstFeature(feature)
            setIcon(imageView: firstIcon, for: feature)
//            showCPNotification(hide:false)
            // if the first feature is school zone/silver zone show second one with speed
            if feature is DetectedSchoolZone || feature is DetectedSilverZone {
                setSpeedIcon(imageView: secondIcon)
                // TODO: To be removed
//                lblSpeed.isHidden = false
//                leadingConstraintValue.constant = 81
                secondIcon.image = UIImage()
                lblSpeed.isHidden = true
                lblPrice.isHidden = true
                leadingConstraintValue.constant = 18
                containerView.backgroundColor = UIColor.notificationSchoolAndSilverZoneBG
                lblPrice.isHidden = true
            } else if let feature = feature as? DetectedERP {
                // TODO: Show price
                secondIcon.image = UIImage()
                lblSpeed.isHidden = true
                lblPrice.isHidden = false
                lblPrice.text = "$\(String(format: "%.2f", feature.price))"
                lblType.text = feature.address
                leadingConstraintValue.constant = 18
                containerView.backgroundColor = UIColor.notificationERPBG
            } else if let feature = feature as? DetectedTrafficIncident {
                secondIcon.image = UIImage()
                lblSpeed.isHidden = true
                lblPrice.isHidden = true
                lblType.text = feature.category
                leadingConstraintValue.constant = 18
                containerView.backgroundColor = UIColor.notificationTrafficBG
            } else {
                secondIcon.image = UIImage()
                lblSpeed.isHidden = true
                lblPrice.isHidden = true
                leadingConstraintValue.constant = 18
            }
        } else {
            show(hide: true)
//            showCPNotification(hide: true)
        }
    }
*/
    func hide() {
        show(hide: true)
    }
    
    func isHidden() -> Bool {
        return view.alpha == 0
    }
    
    private func clear() {
        firstIcon.image = nil
        secondIcon.image = nil
        lblType.text = ""
        lblDistance.text = ""
//        cpFeatureTitleVariants = []
//        cpFeatureSubTitleVariants = []
    }
            
    private func show(hide: Bool) {
        setNeedsStatusBarAppearanceUpdate()
        
        let distanceLabelHeight = lblDistance.intrinsicContentSize.height
        let dh = distanceLabelHeight - 40
        let width  = UIScreen.main.bounds.width
        distanceLabelHeightConstraintValue.constant = distanceLabelHeight
        containerHeightConstraintValue.constant = 128 + dh
        self.view.frame = CGRect(x: 20, y: 48, width: width-34, height: 128 + dh)
        self.view.layer.cornerRadius = 25
        self.view.layer.masksToBounds = true

        UIView.animate(withDuration: 0.3) { [weak self] in
            guard let self = self else { return }
            self.view.alpha = hide ? 0: 1
//            self?.view.frame = CGRect(x: 0, y: y, width: frame.width, height: frame.height)
        }
        
    }
/*
    private func showCPNotification(hide:Bool){
        
//        //To dismiss Car Play notification if present
//        if(cpNotificationAlert != nil)
//        {
        let mapTemplate = getRootMapTemplate()
        if(mapTemplate != nil)
        {
            if(hide)
            {
                
                mapTemplate?.dismissNavigationAlert(animated: true) { dismissed in
                    self.cpFeatureNotificationAlert = nil
                }
            }
            else
            {
                
                if(cpFeatureNotificationAlert == nil)
                {
                    let action = CPAlertAction(title: "",
                                               style: .default,
                                               handler: { _ in })
                    cpFeatureNotificationAlert = CPNavigationAlert(titleVariants: cpFeatureTitleVariants, subtitleVariants: cpFeatureSubTitleVariants, image: cpFeatureImage, primaryAction: action, secondaryAction: nil, duration: 0)

                    mapTemplate!.present(navigationAlert: cpFeatureNotificationAlert!, animated: true)
                }
                else
                {
                    cpFeatureNotificationAlert?.updateTitleVariants(cpFeatureTitleVariants, subtitleVariants: cpFeatureSubTitleVariants)
                    
                }
                
            }
        }
        
            
        //}
    }
*/
    private func showFirstFeature(_ feature: FeatureDetectable) {
        let distance = FeatureNotification.getDistance(for: feature)
        lblDistance.text = distance
        let type = FeatureNotification.getTypeText(for: feature)
        lblType.text = type
        
        SwiftyBeaver.debug("\(feature), distance = \(distance), type = \(type)")
    }
    
    private func setIcon(imageView: UIImageView, for feature: FeatureDetectable) {
        imageView.image = FeatureNotification.getTypeImage(feature)
        
        //setting image on Car Play Notification
//        cpFeatureImage = getTypeImage(feature)
    }
    
    private func setSpeedIcon(imageView: UIImageView) {
        imageView.image = UIImage(named: "speedIcon")!
        
        //setting image on Car Play Notification
//        cpFeatureImage = UIImage(named: "speedIcon")!
    }
/*
    public static func text(for feature: FeatureDetectable) -> String {
        if feature is DetectedSchoolZone {
            return FeatureDetection.featureTypeSchoolZone
        } else if feature is DetectedSilverZone {
            return FeatureDetection.featureTypeSilverZone
        } else if feature is DetectedERP {
            return FeatureDetection.featureTypeErp
        }
        return ""
    }
    
    private func getTypeImage(_ feature: FeatureDetectable) -> UIImage {
        if feature is DetectedSchoolZone {
            return UIImage(named: "schoolZoneIcon")!
        } else if feature is DetectedSilverZone {
            return UIImage(named: "silverZoneIcon")!
        } else if feature is DetectedERP {
            return UIImage(named: "erpIcon")!
        } else if let feature = feature as? DetectedTrafficIncident {
            return UIImage(named: "\(feature.category)-Icon")!
        }
        return UIImage()
    }
*/
}

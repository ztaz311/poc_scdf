//
//  CustomTopBannerViewController.swift
//  Breeze
//
//  Created by VishnuKanth on 25/11/20.
//

import UIKit
import MapboxCoreNavigation
import MapboxNavigation
import MapboxMaps
import MapboxCoreMaps
import MapboxDirections
// MARK: - CustomTopBarViewController

class CustomTopBarViewController: ContainerViewController {
    
    // Or you can implement your own UI elements
    lazy var bannerView: CustomTopBannerView = {
        let banner = CustomTopBannerView()
        banner.translatesAutoresizingMaskIntoConstraints = false
       
        return banner
    }()
    
    override func loadView() {
        super.loadView()
        
        view.addSubview(bannerView)
        
        _ = view.layoutMarginsGuide
        NSLayoutConstraint.activate([
            bannerView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            bannerView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            bannerView.heightAnchor.constraint(equalToConstant: 166),
            bannerView.topAnchor.constraint(equalTo: view.topAnchor)
        ])
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupConstraints()
    }
    
    private func setupConstraints() {
        if let superview = view.superview?.superview {
            view.topAnchor.constraint(equalTo: superview.topAnchor).isActive = true
        }
    }
    
//    // MARK: - NavigationServiceDelegate implementation
//
//    func navigationService(_ service: NavigationService, didUpdate progress: RouteProgress, with location: CLLocation, rawLocation: CLLocation) {
//        // Update your controls manually
//        //bannerView.progress = Float(progress.fractionTraveled)
//        bannerView.eta = "~\(Int(round(progress.durationRemaining / 60))) min"
//
//
//    }
    
    // MARK: - NavigationServiceDelegate implementation

    public func navigationService(_ service: NavigationService, didUpdate progress: RouteProgress, with location: CLLocation, rawLocation: CLLocation) {
        // pass updated data to sub-views which also implement `NavigationServiceDelegate`
//        instructionsBannerView.updateDistance(for: progress.currentLegProgress.currentStepProgress)
        let distanceRemaining = progress.currentLegProgress.currentStepProgress.distanceRemaining
        let distance = distanceRemaining > 5 ? distanceRemaining : 0
        if(distance > 0)
        {
            let distanceString = DistanceFormatter().attributedString(for: distance)
            bannerView.distanceLabel.attributedText = distanceString
        }
        
        
    }

    public func navigationService(_ service: NavigationService, didPassVisualInstructionPoint instruction: VisualInstructionBanner, routeProgress: RouteProgress) {
        
        print("Visual Instruction %@",instruction.primaryInstruction.text as Any)
        bannerView.primaryLabel.text = instruction.primaryInstruction.text
        
        //instructionsBannerView.update(for: instruction)
    }

    public func navigationService(_ service: NavigationService, didRerouteAlong route: Route, at location: CLLocation?, proactive: Bool) {
//        instructionsBannerView.updateDistance(for: service.routeProgress.currentLegProgress.currentStepProgress)
        let distanceRemaining = service.routeProgress.currentLegProgress.currentStepProgress.distanceRemaining
        let distance = distanceRemaining > 5 ? distanceRemaining : 0
        if(distance > 0)
        {
            let distanceString = DistanceFormatter().attributedString(for: distance)
            bannerView.distanceLabel.attributedText = distanceString
        }
    }
    
}
//    private lazy var instructionsBannerTopOffsetConstraint = {
//        return instructionsBannerView.topAnchor.constraint(equalTo: view.topAnchor, constant: 0)
//    }()
//    private lazy var centerOffset: CGFloat = calculateCenterOffset(with: view.bounds.size)
//    private lazy var instructionsBannerCenterOffsetConstraint = {
//        return instructionsBannerView.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 0)
//    }()
//
//    // You can Include one of the existing Views to display route-specific info
//    lazy var instructionsBannerView: InstructionsBannerView = {
//        let banner = InstructionsBannerView()
//        banner.translatesAutoresizingMaskIntoConstraints = false
//        banner.widthAnchor.constraint(equalToConstant: self.view.bounds.size.width).isActive = true
//        banner.heightAnchor.constraint(equalToConstant: 150.0).isActive = true
//        banner.layer.cornerRadius = 5
//        banner.layer.opacity = 1.0
//
//
//
//        banner.distanceLabel.topAnchor.constraint(greaterThanOrEqualTo: banner.primaryLabel.bottomAnchor, constant: 10).isActive = true
////        banner.distanceLabel.leadingAnchor.constraint(greaterThanOrEqualTo: banner.maneuverView.trailingAnchor, constant: 150 / 2).isActive = true
//
//
//
//        let firstColumnWidth = banner.maneuverView.bounds.width + 16 * 5
//
////        banner.maneuverView.centerXAnchor.constraint(equalTo: banner.leadingAnchor, constant: firstColumnWidth / 2).isActive = true
////        banner.maneuverView.centerYAnchor.constraint(equalTo: banner.bottomAnchor, constant: banner.maneuverView.bounds.height - 16 * 3.5).isActive = true
//
//
//        banner.primaryLabel.topAnchor.constraint(equalTo: banner.topAnchor, constant: 60).isActive = true
//        banner.primaryLabel.leadingAnchor.constraint(greaterThanOrEqualTo: banner.maneuverView.trailingAnchor, constant: 50).isActive = true
//
//
//
//        banner.maneuverView.primaryColor = UIColor( red: CGFloat(255/255.0), green: CGFloat(138/255.0), blue: CGFloat(25/255.0), alpha: CGFloat(1.0) )
//        banner.maneuverView.secondaryColor = UIColor( red: CGFloat(255/255.0), green: CGFloat(138/255.0), blue: CGFloat(25/255.0), alpha: CGFloat(1.0) )
//        banner.primaryLabel.normalTextColor = .white
//        banner.distanceLabel.valueTextColor = .white
//        banner.distanceLabel.unitTextColor = .white
//
//        let primaryLabelFont = UIFont(font: .SFUIDisplay, weight: .regular, size: 36)
//        banner.primaryLabel.normalFont = primaryLabelFont!
//
//        let secondaryLabelFont = UIFont(font: .SFUIDisplay, weight: .bold, size: 28)
//        banner.distanceLabel.valueFont = secondaryLabelFont!
//        banner.distanceLabel.unitFont = secondaryLabelFont!
//
//        banner.backgroundColor = UIColor( red: CGFloat(34/255.0), green: CGFloat(38/255.0), blue: CGFloat(56/255.0), alpha: CGFloat(1.0) )
//        //banner.backgroundColor = UIColor(red: 34, green: 38, blue: 56, alpha: 1.0)
//        return banner
//    }()
//
//    override func viewDidLoad() {
//        view.addSubview(instructionsBannerView)
//
//        setupConstraints()
//    }
//
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//
//        updateConstraints()
//    }
//
//    private func setupConstraints() {
//        instructionsBannerCenterOffsetConstraint.isActive = true
//        instructionsBannerTopOffsetConstraint.isActive = true
//    }
//
//    private func updateConstraints() {
//        instructionsBannerCenterOffsetConstraint.constant = centerOffset
//        instructionsBannerTopOffsetConstraint.constant = (traitCollection.verticalSizeClass == .compact ? 10 : 0)
//    }
//
//    // MARK: - Device rotation
//
//    private func calculateCenterOffset(with size: CGSize) -> CGFloat {
//        return (size.height < size.width ? -size.width / 4 : 0)
//    }
//
//    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
//        super.viewWillTransition(to: size, with: coordinator)
//        centerOffset = calculateCenterOffset(with: size)
//    }
//
//    open override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
//        super.traitCollectionDidChange(previousTraitCollection)
//        updateConstraints()
//    }
//
//    // MARK: - NavigationServiceDelegate implementation
//
//    public func navigationService(_ service: NavigationService, didUpdate progress: RouteProgress, with location: CLLocation, rawLocation: CLLocation) {
//        // pass updated data to sub-views which also implement `NavigationServiceDelegate`
//        instructionsBannerView.updateDistance(for: progress.currentLegProgress.currentStepProgress)
//    }
//
//    public func navigationService(_ service: NavigationService, didPassVisualInstructionPoint instruction: VisualInstructionBanner, routeProgress: RouteProgress) {
//        instructionsBannerView.update(for: instruction)
//    }
//
//    public func navigationService(_ service: NavigationService, didRerouteAlong route: Route, at location: CLLocation?, proactive: Bool) {
//        instructionsBannerView.updateDistance(for: service.routeProgress.currentLegProgress.currentStepProgress)
//    }
//}

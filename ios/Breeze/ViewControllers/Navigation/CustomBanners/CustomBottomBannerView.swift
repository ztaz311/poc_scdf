//
//  CustomBannerView.swift
//  Breeze
//
//  Created by VishnuKanth on 25/11/20.
//

import UIKit
import MapboxNavigation

protocol CustomBottomBannerViewDelegate: class {
    func customBottomBannerDidCancel()
}

class CustomBottomBannerView: UIView {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var etaLabel: UILabel!
    @IBOutlet weak var timeLeftLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var cancelButton: UIButton!
    
    @IBOutlet weak var etaHeaderLabel: UILabel!
    @IBOutlet weak var timeLeftHeaderLabel: UILabel!
    @IBOutlet weak var distanceHeaderLabel: UILabel!
    
    
    var timeLeft: String? {
        get {
            return timeLeftLabel.text
        }
        set {
            timeLeftLabel.text = newValue
        }
    }
    
    var distance: String? {
        get {
            return distanceLabel.text
        }
        set {
            distanceLabel.text = newValue
        }
    }
    
    var eta: String? {
        get {
            return etaLabel.text
        }
        set {
            
            etaLabel.text = newValue
        }
    }
    
    weak var delegate: CustomBottomBannerViewDelegate?
    
    private func initFromNib() {
        Bundle.main.loadNibNamed("CustomBottomBannerView",owner:self,options:nil)

        addSubview(contentView)
        contentView.frame = bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        backgroundColor = UIColor.black.withAlphaComponent(0.3)
        layer.cornerRadius = 10
        
        etaHeaderLabel.textColor = UIColor.bottomBannerArrivalHeaderTextColor
        etaHeaderLabel.font = UIFont.bottomBannerHeaderTextFont()
        etaLabel.textColor = UIColor.bottomBannerArrivalValueColor
        etaLabel.font = UIFont.bottomBannerTextFont()
        
        timeLeftHeaderLabel.textColor = UIColor.bottomBannerTimeLeftHeaderTextColor
        timeLeftHeaderLabel.font = UIFont.bottomBannerHeaderTextFont()
        timeLeftLabel.textColor = UIColor.bottomBannerTimeLeftValueColor
        timeLeftLabel.font = UIFont.bottomBannerTextFont()
        
        distanceHeaderLabel.textColor = UIColor.bottomBannerDistanceHeaderTextColor
        distanceHeaderLabel.font = UIFont.bottomBannerHeaderTextFont()
        distanceLabel.textColor = UIColor.bottomBannerDistanceValueColor
        distanceLabel.font = UIFont.bottomBannerTextFont()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.roundCorners([.topLeft, .topRight], radius: 30)
        self.shadowToTopOfView()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initFromNib()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initFromNib()
    }
    
    @IBAction @objc func onCancel(_ sender: Any) {
        delegate?.customBottomBannerDidCancel()
    }
}

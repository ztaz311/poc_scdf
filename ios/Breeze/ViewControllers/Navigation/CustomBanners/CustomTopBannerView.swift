//
//  CustomTopBannerView.swift
//  Breeze
//
//  Created by VishnuKanth on 26/11/20.
//



import UIKit
import MapboxNavigation

class CustomTopBannerView: UIView {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var manueverImage: UIImageView!
    @IBOutlet weak var primaryLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    
    
    var primaryText: String? {
        get {
            return primaryLabel.text
        }
        set {
            primaryLabel.text = newValue
        }
    }
    
    var distance: String? {
        get {
            return distanceLabel.text
        }
        set {
            distanceLabel.text = newValue
        }
    }
    
    
    
    weak var delegate: CustomBottomBannerViewDelegate?
    
    private func initFromNib() {
        Bundle.main.loadNibNamed("CustomTopBannerView",owner:self,options:nil)
//        Bundle.main.loadNibNamed(String(describing: CustomBottomBannerView.self),
//                                 owner: self,
//                                 options: nil)
        addSubview(contentView)
        contentView.frame = bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        backgroundColor = UIColor.red.withAlphaComponent(0.3)
//        layer.cornerRadius = 10
        
        primaryLabel.textColor = UIColor.topBannerPrimaryTextColor
        distanceLabel.textColor = UIColor.topBannerDistanceTextColor
        
        primaryLabel.font = UIFont.topBannerPrimaryTextFont()
        distanceLabel.font = UIFont.topBannerDistanceTextFont()
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initFromNib()
    }
    
    override func layoutSubviews() {
            super.layoutSubviews()

        
//        self.roundCorners([.topLeft, .topRight], radius: 30)
//            self.roundCorners([.bottomLeft, .bottomRight], radius: 50)
            
        }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initFromNib()
    }
}

//
//  AddressCalloutView.swift
//  Breeze
//
//  Created by Zhou Hao on 21/4/21.
//

import UIKit
import MapboxMaps
import SnapKit

final class AddressCalloutView: UIView {
        
    // MARK: - Constants
    let padding: CGFloat = 10
    let yPadding: CGFloat = 8
    let gap: CGFloat = 2 // gap between label and icon
    
    // MARK: - Public Properties
    var bgColor: UIColor = .white
    let tipHeight: CGFloat = 10.0
    let tipWidth: CGFloat = 20.0
    
    private lazy var lblName: UILabel  =  {
        let label = UILabel()
        label.font = UIFont.routePlanningTitleBoldFont()
        label.textColor = UIColor.routePlanningTitleColor
        containerView.addSubview(label)
        return label
    }()
    
    private lazy var lblAddress: UILabel = {
        let label = UILabel()
        label.font = UIFont.routePlanningAddressFont()
        label.textColor = UIColor.routePlanningAddressColor
        containerView.addSubview(label)
        return label
    }()
        
    private lazy var containerView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 4.0
        addSubview(view)
        return view
    }()
    
    var representedObject: PointAnnotation!
    
    

    // https://github.com/mapbox/mapbox-gl-native/issues/9228
    override var center: CGPoint {
        set {
            var newCenter = newValue
            newCenter.y -= bounds.midY
            super.center = newCenter
        }
        get {
            return super.center
        }
    }

    required init(representedObject: PointAnnotation, name: String, address: String, isDarkMode: Bool) {
        self.representedObject = representedObject
        super.init(frame: .zero)
                
        lblName.text = name
        lblAddress.text = address
        
//        self.frame = CGRect(x: 0, y: 0, width: 240, height: 50)
        self.backgroundColor = .clear
        
        setupLayout()
        
        if (isDarkMode)
        {
            self.bgColor = UIColor.rgba(r: 57, g: 63, b: 88, a: 1.0)
            self.backgroundColor = .clear
            self.containerView.backgroundColor = self.bgColor
            lblName.textColor = UIColor.white
           
        }
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = 5
        layer.masksToBounds = true
       // showShadow()
    }
    
    override func sizeThatFits(_ size: CGSize) -> CGSize {
                
        let lblNameSize = lblName.intrinsicContentSize
        let lblAddrSize = lblAddress.intrinsicContentSize
        
        let w = min(max(lblNameSize.width, lblAddrSize.width) + padding*2, UIScreen.main.bounds.width - 50)
        let h = lblNameSize.height + 6 + lblAddrSize.height + padding + tipHeight + gap
        
        return CGSize(width: w, height: h)
    }
    
    override func draw(_ rect: CGRect) {
        let rect = self.bounds
        // Draw the pointed tip at the bottom.
        let fillColor: UIColor = bgColor
        
        let tipLeft = rect.origin.x + (rect.size.width / 2.0) - (tipWidth / 2.0)
        let tipBottom = CGPoint(x: rect.origin.x + (rect.size.width / 2.0), y: rect.origin.y + rect.size.height)
        let heightWithoutTip = rect.size.height - tipHeight - 3
        
        let currentContext = UIGraphicsGetCurrentContext()!
        
        let tipPath = CGMutablePath()
        tipPath.move(to: CGPoint(x: tipLeft, y: heightWithoutTip))
        tipPath.addLine(to: CGPoint(x: tipBottom.x, y: tipBottom.y))
        tipPath.addLine(to: CGPoint(x: tipLeft + tipWidth, y: heightWithoutTip))
        tipPath.closeSubpath()
        
        fillColor.setFill()
        currentContext.addPath(tipPath)
        currentContext.fillPath()
    }
        
    private func setupLayout() {
        lblName.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(yPadding)
            make.leading.equalToSuperview().offset(padding)
            make.trailing.equalToSuperview().offset(padding)
        }
        
        lblAddress.snp.makeConstraints { (make) in
            make.bottom.equalToSuperview().offset(-yPadding)
            make.leading.equalToSuperview().offset(padding)
            make.top.equalTo(lblName.snp.bottom).offset(gap)
            make.width.equalTo(lblName)
        }
        
        containerView.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.bottom.equalToSuperview().offset(-(tipHeight+gap))
            make.leading.equalToSuperview()
            make.trailing.equalToSuperview()
        }
    }
    
//    override func draw(_ rect: CGRect) {
//        let rect = self.bounds
//        // Draw the pointed tip at the bottom.
//        let fillColor: UIColor = .white
//
//        let tipLeft = rect.origin.x + (rect.size.width / 2.0) - (tipWidth / 2.0)
//        let tipBottom = CGPoint(x: rect.origin.x + (rect.size.width / 2.0), y: rect.origin.y + rect.size.height)
//        let heightWithoutTip = rect.size.height - tipHeight - 3
//
//        let currentContext = UIGraphicsGetCurrentContext()!
//
//        let tipPath = CGMutablePath()
//        tipPath.move(to: CGPoint(x: tipLeft, y: heightWithoutTip))
//        tipPath.addLine(to: CGPoint(x: tipBottom.x, y: tipBottom.y))
//        tipPath.addLine(to: CGPoint(x: tipLeft + tipWidth, y: heightWithoutTip))
//        tipPath.closeSubpath()
//
//        fillColor.setFill()
//        currentContext.addPath(tipPath)
//        currentContext.fillPath()
//    }
    
    // MARK: - MGLCalloutView API
    func presentCallout(from rect: CGRect, in view: UIView, constrainedTo constrainedRect: CGRect, animated: Bool) {

        //delegate?.calloutViewWillAppear?(self)
        view.addSubview(self)
        self.sizeToFit()

        if animated {
            alpha = 0

            UIView.animate(withDuration: 0.2) { [weak self] in
                guard let strongSelf = self else {
                    return
                }

                strongSelf.alpha = 1
                //strongSelf.delegate?.calloutViewDidAppear?(strongSelf)
            }
        } else {
            //delegate?.calloutViewDidAppear?(self)
        }
    }

    func dismissCallout(animated: Bool) {
        if (superview != nil) {
            if animated {
                UIView.animate(withDuration: 0.2, animations: { [weak self] in
                    guard let self = self else { return }
                    self.alpha = 0
                }, completion: { [weak self] _ in
                    guard let self = self else { return }
                    self.removeFromSuperview()
                })
            } else {
                removeFromSuperview()
            }
        }
    }

    // MARK: - Callout interaction handlers

    func isCalloutTappable() -> Bool {
//        if let delegate = delegate {
//            if delegate.responds(to: #selector(MGLCalloutViewDelegate.calloutViewShouldHighlight)) {
//                return delegate.calloutViewShouldHighlight!(self)
//            }
//        }
        return true
    }

    @objc func calloutTapped() {
//        if isCalloutTappable() && delegate!.responds(to: #selector(MGLCalloutViewDelegate.calloutViewTapped)) {
//            delegate!.calloutViewTapped!(self)
//        }
    }
}

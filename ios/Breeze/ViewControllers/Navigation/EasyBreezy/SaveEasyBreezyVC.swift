//
//  SaveEasyBreezyVC.swift
//  Breeze
//
//  Created by VishnuKanth on 07/01/21.
//

import UIKit
@_spi(Restricted) import MapboxMaps
import MapboxCoreMaps

protocol SaveEasyBreezyVCDelegate: AnyObject {
    // the reason I update the place is because we may need to change coordinates by dragging the destination point in the future
    func onEasyBreezySaved(name: String, place: SavedPlaces, addressId: Int)
    func onEasyBreezySaved()
}

final class SaveEasyBreezyVC: BaseViewController {
    
    // MARK: - IBOutlet
    @IBOutlet weak var topView: UIView!
    @IBOutlet  var mapView: MapView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var destinationNameText: CountedTextView!
    @IBOutlet weak var locationNameLabel: UILabel!
    @IBOutlet weak var locationAddressLabel: UILabel!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var lableTitle: UILabel!
    
    @IBOutlet weak var lbNameLocation: UILabel!
    
    @IBOutlet weak var buttonShortcut: UIButton!
    @IBOutlet weak var viewShortcut: UIView!
    
    // MARK: - Properties
    var place: SavedPlaces!
    var ebName:String! //Passing Exisitng name during editing easy breezy
    var bookmark: Bookmark?
    var destinationAnnotation: PointAnnotation!
    var addressID  = 0 //Passing Exisitng easybreezy address id during editing easy breezy
    var location:CLLocationCoordinate2D!
    var calloutView: AddressCalloutView!
    weak var delegate: SaveEasyBreezyVCDelegate? // Currently only needed by RoutePlanningVC since it needs the updated data
    
    override var useDynamicTheme: Bool {
        return true
    }
    
    //beta.14 change
    internal lazy var pointAnnotationManager: PointAnnotationManager = {
        return mapView.annotations.makePointAnnotationManager()
        }()
        
    // For testing only
    //let location = CLLocationCoordinate2D(latitude: 1.3667, longitude: 103.8664)
    
    // MARK: - UIViewController life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
//        topView.showBottomShadow()
//        bottomView.roundCorners( [.topLeft, .topRight], radius: 25.0)
//        self.view.setNeedsLayout()
        
        locationNameLabel.text = place.placeName
        locationAddressLabel.text = place.address
        
        if let placeLocation = self.place {
            location = CLLocationCoordinate2D(latitude: placeLocation.lat.toDouble() ?? 0, longitude: placeLocation.long.toDouble() ?? 0)
        }
                        
        //beta.14 change
        let accessToken = ResourceOptionsManager.default.resourceOptions.accessToken
//        guard let accessToken = CredentialsManager.default.accessToken else {
//            fatalError("Access token not set")
//        }

        guard !accessToken.isEmpty else {
            fatalError("Empty access token")
        }

        let resourceOptions = ResourceOptions(accessToken: accessToken )  //till here beta.14 change
        mapView = MapView(frame: view.bounds, mapInitOptions: MapInitOptions(resourceOptions:resourceOptions))
        view.addSubview(mapView)
        mapView.isUserInteractionEnabled = true
        //mapView.bringSubviewToFront(bottomView)
        view.addSubview(bottomView)
        view.addSubview(topView)
        
            //beta.9 changes
            mapView.ornaments.options.compass.visibility = .hidden
            mapView.ornaments.options.scaleBar.visibility = .hidden
            
            /*mapOptions.ornaments.scaleBarVisibility = .hidden
            mapOptions.ornaments.compassVisibility = .hidden*/
        
        
        self.setupLayout()
        
        mapView.ornaments.options.attributionButton.visibility = .hidden
        
        mapView.mapboxMap.onEvery(event: .cameraChanged) { [weak self] _ in
            guard let self = self else { return }
            self.adjustCallout(amenityId: self.bookmark?.amenityId ?? "", layerCode: "", amenityType: self.bookmark?.amenityType ?? "")
        }
                
        mapView.mapboxMap.onNext(event: .styleLoaded) { [weak self] _ in
            guard let self = self else { return }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                if let coordinate = self.location {
                    //beta.9 change
                    self.mapView.mapboxMap.setCamera(to: CameraOptions(center: coordinate, zoom: 13))
                    
                    //self.mapView.camera.setCamera(to: CameraOptions(center: coordinate, zoom: 13), animated: true, completion: nil)
                    
                    self.setupAnnotation(location: self.location, name: self.place.placeName)
                }
            }
        }
        
        destinationNameText.text = ebName ?? ""
        
        destinationNameText.onActionCalled = destinationTextAction
                
//        allowDismissKeyboardWhenTapped()
        
        // To show/hide keyboard
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        /*
        let tapGR = UITapGestureRecognizer(target: self,
                        action:#selector(mapTapped(sender:)))
        for recognizer in mapView.gestureRecognizers!
        where recognizer is UITapGestureRecognizer {
            tapGR.require(toFail: recognizer)
        }
        mapView.addGestureRecognizer(tapGR)*/
    }
    
    private func setupLayout() {
        // align the navMapView bottom to the top of the bottom sheet
        mapView.translatesAutoresizingMaskIntoConstraints = false
        
        mapView.snp.makeConstraints { (make) in
            make.bottom.equalTo(bottomView.snp.top).offset(30)
            make.left.equalTo(self.view).inset(0)
            make.right.equalTo(self.view).inset(0)
            make.top.equalTo(topView.snp.bottom).inset(0)
        }
        
        
        buttonShortcut.layer.cornerRadius = 24
        
        buttonShortcut.backgroundColor = UIColor(named: "saveFavoriteColor")
//        buttonShortcut.setTitleColor(UIColor(named: "saveFavoriteBackgroundColor"), for: .normal)
        buttonShortcut.setTitleColor(.white, for: .normal)
        
    }
    
    @IBAction func actionSaveShortCut(_ sender: Any) {
        destinationTextAction()
        var name = ""
        if let bookmark = bookmark, let code = bookmark.bookmarkCode {
            switch  code {
            case .normal:
                break
            case .home:
                name = ParameterName.CollectionDetailStar.UserClick.add_home_save
            case .work:
                name = ParameterName.CollectionDetailStar.UserClick.add_work_save
            case .custom:
                break
            }
        }
        logUserClick(event: name)
    }
    
    private func setupAnnotation(location: CLLocationCoordinate2D, name: String) {
    
      
        //beta.14 change
        //pointAnnotationManager.syncAnnotations([])
        pointAnnotationManager.annotations = []
        var customPointAnnotation = PointAnnotation(coordinate: location)
        customPointAnnotation.image = .init(image: UIImage(named: "destinationMark")!, name: "CallOut")
        //customPointAnnotation.textField = name
        
//        mapView.annotationManager.interactionDelegate = self
        //pointAnnotationManager.syncAnnotations([customPointAnnotation])
        pointAnnotationManager.annotations = [customPointAnnotation]
        
        if let calloutView = self.calloutView {
            calloutView.removeFromSuperview()
        }
        let callOutView = AddressCalloutView(representedObject: customPointAnnotation, name: place.placeName, address: place.address, isDarkMode: self.isDarkMode)
        callOutView.presentCallout(from: self.mapView.frame, in: self.mapView, constrainedTo: callOutView.frame, animated: false)
        self.calloutView = callOutView
        self.adjustCallout(amenityId: bookmark?.amenityId ?? "", layerCode: "", amenityType: bookmark?.amenityType ?? "")
        
        /*let annotations = mapView.annotations.annotations.values.filter({ $0.text == name })
        mapView.annotations.removeAnnotations(annotations)
                
        var customPointAnnotation = PointAnnotation(coordinate: location,image: UIImage(named: "newAddressPin"))
        customPointAnnotation.title = name
        
//        mapView.annotationManager.interactionDelegate = self
        mapView.annotations.addAnnotation(customPointAnnotation)
        
        if let calloutView = self.calloutView {
            calloutView.removeFromSuperview()
        }
        let callOutView = AddressCalloutView(representedObject: customPointAnnotation, name: place.placeName, address: place.address)
        callOutView.presentCallout(from: self.mapView.frame, in: self.mapView, constrainedTo: callOutView.frame, animated: false)
        self.calloutView = callOutView
        self.adjustCallout()*/
    }
    
    // adjust callout when the map camera changed
    private func adjustCallout(amenityId: String, layerCode: String, amenityType: String) {
        
        if addressID != 0 {
            AnalyticsManager.shared.logEvent(eventName: ParameterName.map_interaction, eventValue: ParameterName.drag_map, screenName: ParameterName.favourites_edit_screen, amenityId: amenityId, layerCode: layerCode, amenityType: amenityType)
        }else{
            AnalyticsManager.shared.logEvent(eventName: ParameterName.map_interaction, eventValue: ParameterName.drag_map, screenName: ParameterName.favourites_new_screen, amenityId: amenityId, layerCode: layerCode, amenityType: amenityType)
        }
        
        guard let callout = calloutView else { return }
        
        guard let screenCoordinate = mapView?.mapboxMap.point(for: location) else { return }
        let point = CGPoint(x: screenCoordinate.x, y: screenCoordinate.y)
        callout.center = CGPoint(x: point.x, y: point.y - 20)
    }
    
    /*public func resourceOptions() -> ResourceOptions {
        let accessToken = ResourceOptionsManager.default.resourceOptions.accessToken
//        guard let accessToken = CredentialsManager.default.accessToken else {
//            fatalError("Access token not set")
//        }

        guard !accessToken.isEmpty else {
            fatalError("Empty access token")
        }

        let resourceOptions = ResourceOptions(accessToken: accessToken )
       
        //let resourceOptions = ResourceOptions(accessToken: accessToken)
        return resourceOptions
    }*/
    
    override var trackingScreenName: String {
        if bookmark != nil {
            if let vc = self.navigationController?.viewControllers.first(where: { vc in
                return vc is CollectionDetailViewController
            }) as? BaseViewController {
                return vc.trackingScreenName
            }
            return ""
        }
        
        if(addressID != 0){
            return ParameterName.favourites_edit_screen
        }else{
            return ParameterName.favourites_new_screen
        }
        
    }
    
    @IBAction func backbtnAction(){
        var name = ""
        if let bookmark = bookmark, let code = bookmark.bookmarkCode {
            switch  code {
            case .normal:
                name = ParameterName.CollectionDetailStar.UserClick.edit_shortcut_back
            case .home:
                name = ParameterName.CollectionDetailStar.UserClick.add_home_back
            case .work:
                name = ParameterName.CollectionDetailStar.UserClick.add_work_back
            case .custom:
                break
            }
        } else {
            name = ParameterName.back
        }
        logUserClick(event: name)
        
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        bottomView.roundCorners( [.topLeft, .topRight], radius: 25.0)
        self.view.setNeedsLayout()
        
        if ebName != nil && !ebName.isEmpty {
            AnalyticsManager.shared.logScreenView(screenName: ParameterName.favourites_edit_screen)
            self.lableTitle.text = Constants.editEBtitle
            destinationNameText.text = ebName
        } else {
            AnalyticsManager.shared.logScreenView(screenName: ParameterName.favourites_edit_screen)
            self.lableTitle.text = Constants.newEBtitle
            destinationNameText.text = place.placeName
        }
        setupDarkLightAppearance()
        setupGradientColor()
        toggleDarkLightMode()
        
        
        if let bookmark = bookmark, let code = bookmark.bookmarkCode {
            switch  code {
            case .normal:
                let text = bookmark.bookmarkId != nil ? "Edit " : "New "
                viewShortcut.isHidden = true
                lableTitle.text = text + "Shortcut"
                lbNameLocation.text = "Name of shortcut location"
                lbNameLocation.font = UIFont(name: fontFamilySFPro.Bold, size: 18)
                lbNameLocation.textColor = UIColor(named: "nameLocationShortcutColor")
                _ = destinationNameText.becomeFirstResponder()
            case .home:
                bottomView.isHidden = true
                lableTitle.text = "Home"
            case .work:
                bottomView.isHidden = true
                lableTitle.text = "Work"
            case .custom:
                break
            }
        } else {
            _ = destinationNameText.becomeFirstResponder()
            viewShortcut.isHidden = true
        }
        
        self.view.bringSubviewToFront(viewShortcut)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
        bottomView.roundCorners( [.topLeft, .topRight], radius: 25.0)
        viewShortcut.roundCorners( [.topLeft, .topRight], radius: 16.0)
        self.view.setNeedsLayout()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name:UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name:UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    // MARK: - Theme updated
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        guard previousTraitCollection != traitCollection else {
            return
        }
        toggleDarkLightMode()
        setupGradientColor()
    }
        
    private func toggleDarkLightMode() {
        mapView.mapboxMap.style.uri = StyleURI(url: URL(string: isDarkMode ? Constants.Map.basicNightStyle: Constants.Map.basicStyle)!)
    }
    
    func setupGradientColor() {
        
//        let color1 = UIColor.navBottomBackgroundColor1.resolvedColor(with: traitCollection)
//        let color2 = UIColor.navBottomBackgroundColor2.resolvedColor(with: traitCollection)
//
//        self.bottomView.applyBackgroundGradient(with: "EasyBreezyBackground", colors: [color2.cgColor,color1.cgColor])
        self.bottomView.backgroundColor = UIColor.navBottomBackgroundColor1
    }

    private func destinationTextAction() {
        if ReachabilityManager.shared.connectionStatus == .unavailable {
            popupAlert(title: Constants.noInternetTitle, message: Constants.noInternetMessage, actionTitles: [Constants.gotIt], actions:[{ [weak self] action1 in
                guard let self = self else { return }
                self.dismissAnyAlertControllerIfPresent()
            },{action2 in

                //Not needed
                
            }, nil])
        } else {
            
            if !destinationNameText.text.isEmpty {
                
                if let bookmark = bookmark {
                    bookmark.name = destinationNameText.text
                    if bookmark.bookmarkId != nil {
                        BookmarkService.shared.editBookmark(bookmark: bookmark) { [weak self] result in
                            guard let self = self else { return }
                            
                            DispatchQueue.main.async { [weak self] in
                                guard let self = self else { return }
                                self.popToViewController()
                            }
                            print("save okie")
                        }
                    } else {
                        BookmarkService.shared.createBookmark(bookmark: bookmark) { [weak self] result  in
                            guard let self = self else { return }
                            
                            DispatchQueue.main.async { [weak self] in
                                guard let self = self else { return }
                                self.popToViewController()
                            }
                            print("save okie")
                        }
                    }
                    return
                }
                
            } else {
                if let bookmark = bookmark,
                let code = bookmark.bookmarkCode {
                    switch code {
                    case .home, .work, .normal:
                        self.logUserClick(event: ParameterName.CollectionDetailStar.UserClick.edit_shortcut_name_clear)
                    case .custom:
                        break
                    }
                 } else {
                     self.logUserClick(event: ParameterName.favourites_name_clear)
                 }
            }
        }
    }
    
    func popToViewController(){
        
        EventEmitter.sharedInstance.dispatch(name: "favouriteListChange", body: [
        ])
        
        self.navigationController?.popViewController(animated: false)
    }
    
    func getEasyBreezyList() {
        EasyBreezyService.shared.getSavedEasyBreezyAddresses() { (result) in
            switch result {
            case .success(let json):
                SavedSearchManager.shared.easyBreezy = json
            case .failure(_):
                print("Easy Breezy fetch Failed")
            }
            
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                self.popToViewController()
            }
        }
    }
    
    func updateEasyBreezy(name: String, address:[String:Any]){
        
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.save, screenName: ParameterName.favourites_edit_screen)
        
        DispatchQueue.global(qos: .background).async {
            
            EasyBreezyService.shared.updateEasyBreezyAddress(address:address) { [weak self] (result) in
                guard let self = self else { return }
                
                print(result)
                switch result {
                case .success(_):
                    self.delegate?.onEasyBreezySaved(name: name, place: self.place, addressId: self.addressID)
                    self.getEasyBreezyList()

                case .failure(_):
                    DispatchQueue.main.async {
                        self.popToViewController()
                    }
                }
            }
        }
    }
    
    func saveEasyBreezy(name: String, address:[String:Any]){
        
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.save, screenName: ParameterName.favourites_new_screen)
        
        DispatchQueue.global(qos: .background).async {
            
            EasyBreezyService.shared.saveEasyBreezyAddress(address:address) { [weak self] (result) in
                guard let self = self else { return }
                print(result)
                switch result {
                case .success(_):
                    self.delegate?.onEasyBreezySaved(name: name, place: self.place, addressId: self.addressID)
                    self.getEasyBreezyList()
                    
                case .failure(_):
                    DispatchQueue.main.async {
                        self.popToViewController()
                    }
                }
            }
        }
    }
    
    // MARK: for keyboard show/hide
    @objc func keyboardWillShow(notification: NSNotification) {
        // only work once
        guard bottomConstraint.constant == 0 else {
            return
        }
        
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            
            bottomConstraint.constant = -keyboardSize.height
            UIView.animate(withDuration: 0.2) {
                self.view.setNeedsLayout()
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        bottomConstraint.constant = 0
        UIView.animate(withDuration: 0.2) {
            self.view.setNeedsLayout()
        }
    }
    
    /*
    private func addAnnotation(at coordinate: CLLocationCoordinate2D) {
        let point = MGLPointAnnotation()
        point.title = place.placeName
        point.subtitle = place.address
        point.coordinate = coordinate
        destinationAnnotation = point
        mapView.addAnnotation(point)
        mapView.selectAnnotation(destinationAnnotation, animated: true, completionHandler: nil)
    }
    
    @objc func mapTapped(sender: UITapGestureRecognizer) {
        guard sender.state == .ended else {
            return
        }
        mapView.removeAnnotation(destinationAnnotation)
        
        let mapPoint = sender.location(in: self.view)
        let coordinate = mapView.convert(mapPoint, toCoordinateFrom: nil)
        addAnnotation(at: coordinate)
        
        // TODO: need to update place address by Geocoding API
    }
    
    // MARK: - IBAction
    @IBAction func onBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: for keyboard show/hide
    @objc func keyboardWillShow(notification: NSNotification) {
        // only work once
        guard bottomConstraint.constant == 0 else {
            return
        }
        
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            
            bottomConstraint.constant = -keyboardSize.height
            UIView.animate(withDuration: 0.2) {
                self.view.setNeedsLayout()
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        bottomConstraint.constant = 0
        UIView.animate(withDuration: 0.2) {
            self.view.setNeedsLayout()
        }
    }
}

extension SaveEasyBreezyVC: MGLMapViewDelegate {
    func mapView(_ mapView: MGLMapView, didFinishLoading style: MGLStyle) {
        addAnnotation(at: location)
    }
    
    func mapView(_ mapView: MGLMapView, imageFor annotation: MGLAnnotation) -> MGLAnnotationImage? {
        var annotationImage = mapView.dequeueReusableAnnotationImage(withIdentifier: "newAddressPin")
        if annotationImage == nil {
            let image = UIImage(named: "newAddressPin")!
            //image = image.withAlignmentRectInsets(UIEdgeInsets(top: 0, left: 0, bottom: image.size.height/2, right: 0))
            annotationImage = MGLAnnotationImage(image: image, reuseIdentifier: "newAddressPin")
        }
        return annotationImage
    }

    func mapView(_ mapView: MGLMapView, annotationCanShowCallout annotation: MGLAnnotation) -> Bool {
        return true
    }*/
}

class CustomCalloutView: UIView {
    var representedObject: PointAnnotation
    
    // Allow the callout to remain open during panning.
    let dismissesAutomatically: Bool = false
    let isAnchoredToAnnotation: Bool = true
    
    // https://github.com/mapbox/mapbox-gl-native/issues/9228
    override var center: CGPoint {
        set {
            var newCenter = newValue
            newCenter.y -= bounds.midY
            super.center = newCenter
        }
        get {
            return super.center
        }
    }
    
    lazy var leftAccessoryView = UIView() /* unused */
    lazy var rightAccessoryView = UIView() /* unused */
    
    //weak var delegate: MGLCalloutViewDelegate?
    
    let tipHeight: CGFloat = 10.0
    let tipWidth: CGFloat = 20.0
    
    let mainBody: UIButton
    
    required init(representedObject: PointAnnotation) {
        self.representedObject = representedObject
        self.mainBody = UIButton(type: .system)
        
        super.init(frame: .zero)
        
        backgroundColor = .clear
        
        mainBody.backgroundColor = .white
        mainBody.tintColor = .black
        mainBody.contentEdgeInsets = UIEdgeInsets(top: 10.0, left: 10.0, bottom: 10.0, right: 10.0)
        mainBody.layer.cornerRadius = 4.0
        
        addSubview(mainBody)
    }
    
    required init?(coder decoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - We call this explicitly
    func presentCallout(from rect: CGRect, in view: UIView, constrainedTo constrainedRect: CGRect, animated: Bool) {
        
        //delegate?.calloutViewWillAppear?(self)
        
        view.addSubview(self)
        
        // Prepare title label.
        mainBody.setTitle(representedObject.textField!, for: .normal)
        mainBody.sizeToFit()
        
        if isCalloutTappable() {
            // Handle taps and eventually try to send them to the delegate (usually the map view).
            mainBody.addTarget(self, action: #selector(CustomCalloutView.calloutTapped), for: .touchUpInside)
        } else {
            // Disable tapping and highlighting.
            mainBody.isUserInteractionEnabled = false
        }
        
        // Prepare our frame, adding extra space at the bottom for the tip.
        let frameWidth = mainBody.bounds.size.width
        let frameHeight = mainBody.bounds.size.height// + tipHeight
        let frameOriginX = rect.origin.x + (rect.size.width/2.0) - (frameWidth/2.0)
        let frameOriginY = rect.origin.y - frameHeight
        frame = CGRect(x: frameOriginX, y: frameOriginY+10, width: frameWidth, height: frameHeight)
        
        if animated {
            alpha = 0
            
            UIView.animate(withDuration: 0.2) { [weak self] in
                guard let strongSelf = self else {
                    return
                }
                
                strongSelf.alpha = 1
                //strongSelf.delegate?.calloutViewDidAppear?(strongSelf)
            }
        } else {
            //delegate?.calloutViewDidAppear?(self)
        }
    }
    
    func dismissCallout(animated: Bool) {
        if (superview != nil) {
            if animated {
                UIView.animate(withDuration: 0.2, animations: { [weak self] in
                    guard let self = self else { return }
                    self.alpha = 0
                }, completion: { [weak self] _ in
                    guard let self = self else { return }
                    self.removeFromSuperview()
                })
            } else {
                removeFromSuperview()
            }
        }
    }
    
    // MARK: - Callout interaction handlers
    
    func isCalloutTappable() -> Bool {
//        if let delegate = delegate {
//            if delegate.responds(to: #selector(MGLCalloutViewDelegate.calloutViewShouldHighlight)) {
//                return delegate.calloutViewShouldHighlight!(self)
//            }
//        }
        return true
    }
    
    @objc func calloutTapped() {
//        if isCalloutTappable() && delegate!.responds(to: #selector(MGLCalloutViewDelegate.calloutViewTapped)) {
//            delegate!.calloutViewTapped!(self)
//        }
    }
    
    // MARK: - Custom view styling
    
//    override func draw(_ rect: CGRect) {
//        // Draw the pointed tip at the bottom.
//        let fillColor: UIColor = .white
//
//        let tipLeft = rect.origin.x + (rect.size.width / 2.0) - (tipWidth / 2.0)
//        let tipBottom = CGPoint(x: rect.origin.x + (rect.size.width / 2.0), y: rect.origin.y + rect.size.height)
//        let heightWithoutTip = rect.size.height - tipHeight - 1
//
//        let currentContext = UIGraphicsGetCurrentContext()!
//
//        let tipPath = CGMutablePath()
//        tipPath.move(to: CGPoint(x: tipLeft, y: heightWithoutTip))
//        tipPath.addLine(to: CGPoint(x: tipBottom.x, y: tipBottom.y))
//        tipPath.addLine(to: CGPoint(x: tipLeft + tipWidth, y: heightWithoutTip))
//        tipPath.closeSubpath()
//
//        fillColor.setFill()
//        currentContext.addPath(tipPath)
//        currentContext.fillPath()
//    }
}

extension SaveEasyBreezyVC: AnnotationInteractionDelegate {
    
    public func annotationManager(_ manager: AnnotationManager, didDetectTappedAnnotations annotations: [Annotation]) {
        
        //Not needed
    }
    public func didDeselectAnnotation(annotation: Annotation) {
        
        //Not neede
    }

    public func didSelectAnnotation(annotation: Annotation) {
        
       //Not needed
    }
}


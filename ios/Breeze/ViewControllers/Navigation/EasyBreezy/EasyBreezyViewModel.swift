//
//  EasyBreezyViewModel.swift
//  Breeze
//
//  Created by Zhou Hao on 8/2/21.
//

import Foundation

class EasyBreezyListViewModel {
    
    weak var savedSearchMgr: SavedSearchManager!
    var ezSvc: EasyBreezyService!
    
    // Using dependency rejection here to make it convinent if need to replace it or for testing
    init(manager: SavedSearchManager, easyBreezySvc: EasyBreezyService) {
        self.savedSearchMgr = manager
        self.ezSvc = easyBreezySvc
    }
    
    func count() -> Int {
        return savedSearchMgr.easyBreezy.count
    }
    
    func edit(index: Int, complete: (SavedPlaces?) -> Void) {
        guard index >= 0 && index < savedSearchMgr.easyBreezy.count else {
            complete(nil)
            return
        }
        
        let address = savedSearchMgr.easyBreezy[index].address1
        let fullAddress = savedSearchMgr.easyBreezy[index].address2
        let lat = savedSearchMgr.easyBreezy[index].lat
        let long = savedSearchMgr.easyBreezy[index].long
        
        let strDate = DateUtils.shared.getTimeDisplay(dateFormat: Date.defaultFormat, date: Date())
        
        let savedPlace = SavedPlaces(placeName: address!, address: fullAddress!, lat: lat!, long: long!, time: strDate)
        complete(savedPlace)
    }
    
    func delete(index: Int, complete: @escaping (Bool) -> Void) {
        guard index >= 0 && index < savedSearchMgr.easyBreezy.count else {
            complete(false)
            return
        }

        let addressID =  ["addressid":savedSearchMgr.easyBreezy[index].addressid]
        
        DispatchQueue.global(qos: .background).async {
            [weak self] in
            
            guard let strongSelf = self else {
                complete(false)
                return
            }
            
            strongSelf.ezSvc.deleteEasyBreezyAddresses(address:[addressID as [String : Any]] as [[String : Any]]) { (result) in
                switch result {
                case .success(_):
                    strongSelf.savedSearchMgr.easyBreezy.remove(at: index)
                    DispatchQueue.main.async {
                        complete(true)
                    }
                case .failure(_):
                    DispatchQueue.main.async {
                        complete(false)
                    }
                }
            }
        }
    }
    
    func easyBreezyViewModel(index: Int) -> EasyBreezyViewModel? {
        guard index >= 0 && index < savedSearchMgr.easyBreezy.count else {
            return nil
        }
        
        let eazyBreezy = savedSearchMgr.easyBreezy[index]
        return EasyBreezyViewModel(ezAddress: eazyBreezy)
    }
    
}

class EasyBreezyViewModel {
    var name: String
    var address: String
    var location: String
    
    init(ezAddress: EasyBreezyAddresses) {
        self.name = ezAddress.alias ?? ""
        self.address = ezAddress.address1 ?? ""
        self.location = ezAddress.address2 ?? ""
    }
}

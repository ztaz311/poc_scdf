//
//  MapLandingVC+Testing.swift
//  Breeze
//
//  Only for TESTING scheme
//
//  Created by Zhou Hao on 12/10/21.
//

import Foundation
import MapboxDirections
import MapboxCoreNavigation
import MapboxMaps
import MapboxCoreMaps
import MapboxNavigation
import Amplify

extension MapLandingVC {
    func requestRoute() {
        guard waypoints.count > 0 else { return }
        // Beta.15
        guard let currentLocation = navMapView.mapView.location.latestLocation else {
            print("User location is not valid. Make sure to enable Location Services.")
            return
        }
        
        let userWaypoint = Waypoint(coordinate: currentLocation.coordinate)
        waypoints.insert(userWaypoint, at: 0)
        
        let navigationRouteOptions = NavigationRouteOptions(waypoints: waypoints)
        
        // Get periodic updates regarding changes in estimated arrival time and traffic congestion segments along the route line.
        RouteControllerProactiveReroutingInterval = 30
        
        requestRoute(with: navigationRouteOptions, success: defaultSuccess, failure: defaultFailure)
    }
        
    func requestRoute(with options: RouteOptions, success: @escaping RouteRequestSuccess, failure: RouteRequestFailure?) {
        Directions.shared.calculate(options) { (session, result) in
            switch result {
            case let .success(response):
                success(response)
            case let .failure(error):
                failure?(error)
            }
        }
    }
}

// MARK: - NavigationMapViewDelegate methods
extension MapLandingVC: NavigationMapViewDelegate {
    
    func navigationMapView(_ navigationMapView: NavigationMapView, waypointCircleLayerWithIdentifier identifier: String, sourceIdentifier: String) -> CircleLayer? {
        var circleLayer = CircleLayer(id: identifier)
        circleLayer.source = sourceIdentifier
        let opacity = Exp(.switchCase) {
            Exp(.any) {
                Exp(.get) {
                    "waypointCompleted"
                }
            }
            0.5
            1
        }
        circleLayer.circleColor = .constant(.init(UIColor(red:0.9, green:0.9, blue:0.9, alpha:1.0)))
        circleLayer.circleOpacity = .expression(opacity)
        circleLayer.circleRadius = .constant(.init(10))
        circleLayer.circleStrokeColor = .constant(.init(UIColor.black))
        circleLayer.circleStrokeWidth = .constant(.init(1))
        circleLayer.circleStrokeOpacity = .expression(opacity)
        return circleLayer
    }
    
    func navigationMapView(_ navigationMapView: NavigationMapView, waypointSymbolLayerWithIdentifier identifier: String, sourceIdentifier: String) -> SymbolLayer? {
        var symbolLayer = SymbolLayer(id: identifier)
        symbolLayer.source = sourceIdentifier
        symbolLayer.textField = .expression(Exp(.toString){
            Exp(.get){
                "name"
            }
        })
        symbolLayer.textSize = .constant(.init(10))
        symbolLayer.textOpacity = .expression(Exp(.switchCase) {
            Exp(.any) {
                Exp(.get) {
                    "waypointCompleted"
                }
            }
            0.5
            1
        })
        symbolLayer.textHaloWidth = .constant(.init(0.25))
        symbolLayer.textHaloColor = .constant(.init(UIColor.black))
        return symbolLayer
    }
    
    func navigationMapView(_ navigationMapView: NavigationMapView, shapeFor waypoints: [Waypoint], legIndex: Int) -> FeatureCollection? {
        var features = [Turf.Feature]()
        for (_, waypoint) in waypoints.enumerated() {
            
            let feature = Turf.Feature(geometry: .point(Point(waypoint.coordinate)))
            features.append(feature)
        }
        return FeatureCollection(features: features)
    }
    
    func navigationMapView(_ mapView: NavigationMapView, didSelect waypoint: Waypoint) {
        guard let responseOptions = response?.options, case let .route(routeOptions) = responseOptions else { return }
        // Beta.19
        let modifiedOptions = routeOptions.without(waypoint)
        presentWaypointRemovalAlert { [weak self] _ in
            guard let self = self else {return}
            if let options = modifiedOptions as? RouteOptions {
                self.requestRoute(with:options, success: self.defaultSuccess, failure: self.defaultFailure)
            }
        }
    }
    
    func navigationMapView(_ mapView: NavigationMapView, didSelect route: Route) {
        guard let routes = response?.routes else { return }
        guard let index = routes.firstIndex(where: { $0 === route }) else { return }
        self.response?.routes?.swapAt(index, 0)
    }
    
    private func presentWaypointRemovalAlert(completionHandler approve: @escaping ((UIAlertAction) -> Void)) {
        let title = NSLocalizedString("REMOVE_WAYPOINT_CONFIRM_TITLE", value: "Remove Waypoint?", comment: "Title of alert confirming waypoint removal")
        let message = NSLocalizedString("REMOVE_WAYPOINT_CONFIRM_MSG", value: "Do you want to remove this waypoint?", comment: "Message of alert confirming waypoint removal")
        let removeTitle = NSLocalizedString("REMOVE_WAYPOINT_CONFIRM_REMOVE", value: "Remove Waypoint", comment: "Title of alert action for removing a waypoint")
        let cancelTitle = NSLocalizedString("REMOVE_WAYPOINT_CONFIRM_CANCEL", value: "Cancel", comment: "Title of action for dismissing waypoint removal confirmation sheet")
        
        let waypointRemovalAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let removeAction = UIAlertAction(title: removeTitle, style: .destructive, handler: approve)
        let cancelAction = UIAlertAction(title: cancelTitle, style: .cancel, handler: nil)
        [removeAction, cancelAction].forEach(waypointRemovalAlertController.addAction(_:))
        
        self.present(waypointRemovalAlertController, animated: true, completion: nil)
    }
}


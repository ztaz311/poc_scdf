//
//  TrafficCameraView.swift
//  Breeze
//
//  Created by VishnuKanth on 09/01/22.
//

import UIKit
import MapboxNavigation
import MapboxCoreNavigation
import MapboxCoreMaps
@_spi(Restricted) import MapboxMaps
import Turf
import CoreLocation

class TrafficCameraView: BaseViewController {
    
    @IBOutlet weak var naviMapView: NavigationMapView!
    @IBOutlet weak var topView: UIView!
    var cameraView: UIView!
    var trafficCalloutView:TrafficToolTipsView?
    var trafficReceivedData: [Any]?
    var trafficFeatureCollection = FeatureCollection(features: [])
    var trafficCoordinates = [CLLocationCoordinate2D]()
    var expressWayCode = ""
    
    override var useDynamicTheme: Bool {
        return true
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        toggleDarkLightMode()
        // disable pitch gesture
        naviMapView.mapView.gestures.options.pitchEnabled = false
        view.backgroundColor = UIColor.navBottomBackgroundColor1
        topView.backgroundColor = UIColor.navBottomBackgroundColor1
        topView.showBottomShadow()
        naviMapView.mapView.isUserInteractionEnabled = true
        
        naviMapView.mapView.ornaments.options.scaleBar.visibility = .hidden
        naviMapView.mapView.ornaments.options.compass.visibility = .hidden
        naviMapView.mapView.ornaments.options.attributionButton.visibility = .hidden
        
        self.setPuckAndAddCameraRNScreen()
        
        //naviMapView.userLocationStyle = .puck2D(configuration: nil)
        naviMapView.mapView.mapboxMap.onEvery(event: .styleLoaded) { [weak self] _ in
            guard let self = self else { return }
            self.naviMapView.navigationCamera.stop()
            self.naviMapView.mapView.removeTrafficImagesToMapStyle(type: Values.TRAFFIC)
            self.naviMapView.mapView.addTrafficImagesToMapStyle(type: Values.TRAFFIC)
            
            NotificationCenter.default.addObserver(self, selector: #selector(self.onTrafficDataReceived), name: Notification.Name(Values.receivedTrafficData), object: nil)
            
            NotificationCenter.default.addObserver(self, selector: #selector(self.onTrafficIDSelection), name: Notification.Name(Values.receivedTrafficID), object: nil)
            
            self.getCameraOptions()
           
        }
        
        naviMapView.mapView.mapboxMap.onEvery(event: .cameraChanged) { [weak self] _ in
            guard let self = self else { return }
            if let calloutView = self.trafficCalloutView {
                calloutView.adjust(to: calloutView.tooltipCoordinate, in: self.naviMapView)
            }
        }
        
        // Do any additional setup after loading the view.
    }
    
    func setPuckAndAddCameraRNScreen(){
        
        var traitCollection = UITraitCollection(userInterfaceStyle: .light)
        if(Settings.shared.theme == 0){
            
            if(appDelegate().isDarkMode()){
                
                traitCollection = UITraitCollection(userInterfaceStyle: .dark)
                
            }
            
        }
        else{
            
            traitCollection = UITraitCollection(userInterfaceStyle: UIUserInterfaceStyle(rawValue: Settings.shared.theme) ?? .unspecified)
        }
        
        let puck2d = Puck2DConfiguration(topImage: UIImage(named: "puckArrow",in:nil,compatibleWith: traitCollection), bearingImage: UIImage(named: "puckIcon"), shadowImage: nil, scale: nil)
        
        naviMapView.userLocationStyle = .puck2D(configuration: puck2d)
        self.addExpressWayView()
    }
    
    func getCameraOptions(){
        
        if(trafficCoordinates.count > 0){
        
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                
                let options =  self.naviMapView.mapView.mapboxMap.camera(for: self.trafficCoordinates, padding:UIEdgeInsets(top: self.topView.frame.origin.y+self.topView.frame.size.height+30, left: 90, bottom: self.cameraView.frame.size.height + 40, right: 100), bearing: 0, pitch: 0)
                self.naviMapView.mapView.camera.ease(to: options, duration: 0)
                
                    if let selectedFeatureProperties = self.trafficFeatureCollection.features[0].properties,
                       case let .string(name) = selectedFeatureProperties["trafficCameraName"],
                       case let .string(trafficID) = selectedFeatureProperties["trafficID"]
                    {
                        self.showCallout(id: trafficID, name: name, location: self.trafficCoordinates[0])
                    }
                
                
            }
            
        }
    }
    
    func addExpressWayView(){
        
        guard self.cameraView == nil else { return }
        let params = [ // Object data to be used by that screen
            "code_expressway":expressWayCode,
        ]
        let expressWayVC = getRNViewBottomSheet(toScreen: "ExpressWayDetail", navigationParams: params as [String:Any])
        //self.cameraView.backgroundColor = .clear
        //expressWayVC.view.frame = self.cameraView.bounds
        //self.cameraView.addSubview(expressWayVC.view)
        self.addChildViewControllerWithView(childViewController: expressWayVC,toView: self.view)
        
        expressWayVC.view.snp.makeConstraints { make in
            make.bottom.equalToSuperview().offset(0)
            make.leading.equalToSuperview()
            make.trailing.equalToSuperview()
            make.width.equalTo(UIScreen.main.bounds.width)
            make.height.equalTo(UIScreen.main.bounds.height / 2.73)
        }
        
        self.cameraView = expressWayVC.view
        //self.cameraView.bringSubviewToFront(self.view)
        //self.cameraView.addSubview(expressWayVC.view)
        print(expressWayVC.view.frame.size.height)
        //expressWayVC.view.frame = self.view.bounds)
        
    }
    
    @objc
    func onTrafficDataReceived(_ notification:NSNotification){
        
        if let trafficData = notification.userInfo {
            
            trafficReceivedData = trafficData["data"] as? [Any]
            
            getExistingTrafficDataAndUpdateMap(selectedId: "", selectFirst: true)
            //print(trafficData["data"] as? NSDictionary)
            self.getCameraOptions()
        }
    }
    
    private func getExistingTrafficDataAndUpdateMap(selectedId: String = "", selectFirst: Bool = false) {
        var turfTrafficArray = [Turf.Feature]()
        var isFirst = true
        for trafficReceivedData in self.trafficReceivedData ?? [] {
            let itemDict = trafficReceivedData as! [String:Any]
            
            let trafficID = itemDict["id"] as! String
            let trafficCameraID = itemDict["cameraID"] as! Int
            let trafficCameralat = itemDict["lat"] as! Double
            let trafficCameralong = itemDict["long"] as! Double
            let trafficCameraName = itemDict["name"] as! String
            
            let coordinate = CLLocationCoordinate2D(latitude: trafficCameralat, longitude: trafficCameralong)
            
            //let options = CameraOptions(center:coordinate)
            
            trafficCoordinates.append(coordinate)
            
            let isSelected = selectFirst ? isFirst : (!selectedId.isEmpty && selectedId == trafficID)
            var feature = Turf.Feature(geometry: .point(Point(coordinate)))
            feature.properties = [
                "type":.string(Values.TRAFFIC),
                "trafficID":.string(trafficID),
                "trafficCameraID":.string("\(trafficCameraID)"),
                "trafficCameraName":.string(trafficCameraName),
                "isSelected": .boolean(isSelected)
            ]
            isFirst = false
            turfTrafficArray.append(feature)
           
        }
       
         trafficFeatureCollection = FeatureCollection(features: turfTrafficArray)
        
        self.addTrafficDataOnMap(trafficFeatureCollection, type: Values.TRAFFIC)
        
//        self.getCameraOptions()
    }
    
    @objc
    func onTrafficIDSelection(_ notification:NSNotification){
        
        if let trafficIDData = notification.userInfo {
            
            let trafficIDSelected = trafficIDData["id"] as! String
            
            if let trafficReceivedData = trafficReceivedData {
                
                for traffic in trafficReceivedData {
                    let itemDict = traffic as! [String:Any]
                    let trafficID = itemDict["id"] as! String
                    
                    if(trafficID == trafficIDSelected){
                        
                        let trafficCameralat = itemDict["lat"] as! Double
                        let trafficCameralong = itemDict["long"] as! Double
                        let trafficCameraName = itemDict["name"] as! String
                        let coordinate = CLLocationCoordinate2D(latitude: trafficCameralat, longitude: trafficCameralong)
                        DispatchQueue.main.async {
                        let options = CameraOptions(center:coordinate)
                        self.naviMapView.mapView.camera.ease(to: options, duration: 0)
                        self.getExistingTrafficDataAndUpdateMap(selectedId: trafficID, selectFirst: false)
                            self.showCallout(id: trafficIDSelected, name: trafficCameraName, location: coordinate)
                        }
                    }
                }
            
            }
            
        }
    }
    
    func addTrafficDataOnMap(_ featureCollection: FeatureCollection,type:String){
        
        naviMapView.mapView.removeAmenitiesLayerOfType(type: type)
        naviMapView.mapView.addAmentiesToMapOfType(features: featureCollection, isBelowPuck: true, type: type,aboveID:false)
    }
    
    deinit {
        
        print("deinit called")
        NotificationCenter.default.removeObserver(self, name: Notification.Name(Values.receivedTrafficID), object: nil)
        
        NotificationCenter.default.removeObserver(self, name: Notification.Name(Values.receivedTrafficData), object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self, name: Notification.Name(Values.receivedTrafficID), object: nil)
        
        NotificationCenter.default.removeObserver(self, name: Notification.Name(Values.receivedTrafficData), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        setupDarkLightAppearance()
        setupGestureRecognizers()
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        guard previousTraitCollection != traitCollection else {
            return
        }
        toggleDarkLightMode()
    }

    @IBAction func onBack(_ sender: Any) {
    
        moveBack()
    }
    
    private func setupGestureRecognizers() {
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(panGesture(_:)))
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapGesture(_:)))
        naviMapView.addGestureRecognizer(panGesture)
        naviMapView.addGestureRecognizer(tapGesture)
        naviMapView.mapView.gestures.delegate = self
    }
        
    @objc func panGesture(_ gesture: UIPanGestureRecognizer){
       
        let point = gesture.location(in: self.naviMapView)
        self.queryTrafficLayer(point: point)
    }
        
    @objc func tapGesture(_ gesture: UIPinchGestureRecognizer){
        
        self.queryTrafficLayer(point: gesture.location(in: self.naviMapView))
        
    }
    
    private func toggleDarkLightMode() {
        naviMapView.setupRouteColor(isDarkMode: isDarkMode)
        naviMapView.mapView.mapboxMap.style.uri = StyleURI(url: URL(string: isDarkMode ? Constants.Map.basicNightStyle: Constants.Map.basicStyle)!)
        sendThemeChangeEvent(isDarkMode)
        self.setPuckAndAddCameraRNScreen()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension TrafficCameraView{
    
    func queryTrafficLayer(point:CGPoint){
        
      let location =  self.naviMapView.mapView.mapboxMap.coordinate(for: point)
        var layerID = [String]()
        
        layerID.append("_\(Values.TRAFFIC)")
        if(layerID.count > 0){
            
            let queryOptions = RenderedQueryOptions(layerIds: layerID, filter: nil)
            //let Zone2NullOptions = RenderedQueryOptions(layerIds: ["zone2-null"], filter: nil)
            self.naviMapView.mapView.mapboxMap.queryRenderedFeatures(at: point, options: queryOptions) { [weak self] (result) in
                
                guard let self = self else { return }

                switch result {

                case .success(let features):
                    
                    // Return the first feature at that location, then pass attributes to the alert controller.
                    
                    if let feature = features.first?.feature,
                       let selectedFeatureProperties = feature.properties,
                       case let .string(type) = selectedFeatureProperties["type"]
                    {
                        
                        if (type == Values.TRAFFIC) {
                            
                            if case let .point(point) = feature.geometry {
                                self.showTrafficCallout(feature: features.first!.feature, location: point.coordinates)
                            }
                        }
                    } else {
                        // If the feature is a cluster, it will have `point_count` and `cluster_id` properties. These are assigned
                        // when the cluster is created.
                    }
                    
                case .failure(_):
                    break
                }
            }
        }
    }
    
    func showTrafficCallout(feature:Turf.Feature,location:CLLocationCoordinate2D){
        
        self.didDeselectAnnotation()
        if let selectedFeatureProperties = feature.properties,
           case let .string(name) = selectedFeatureProperties["trafficCameraName"],
           case let .string(trafficID) = selectedFeatureProperties["trafficID"]
           
         {
            getExistingTrafficDataAndUpdateMap(selectedId: trafficID, selectFirst: false)
           
            self.showCallout(id: trafficID ,name: name, location: location)
            ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.SELECT_TRAFFIC_CAMERA, data: ["cameraID":trafficID] as? NSDictionary).subscribe(onSuccess: {_ in
                //We don't need to process this
            },                 onFailure: {_ in
                //We don't need to process this
            })
        }
    }
    
    func didDeselectAnnotation() {
       if let callout = self.trafficCalloutView {
           callout.dismiss(animated: true)
           callout.removeFromSuperview()
           self.trafficCalloutView = nil
       }
        
   }
    

    func showCallout(id: String,
                     name: String,
                     location: CLLocationCoordinate2D) {
        
        self.didDeselectAnnotation()
        let trafficToolTip:TrafficToolTipsView = TrafficToolTipsView(id: id, name: name,location: location)
        trafficToolTip.present(from: self.naviMapView.mapView.frame, in: self.naviMapView.mapView, constrainedTo: trafficToolTip.frame, animated: true)
        trafficToolTip.center = self.view.center
        trafficToolTip.showShadow = false
        self.view.bringSubviewToFront(trafficToolTip)
        self.trafficCalloutView = trafficToolTip
        self.trafficCalloutView?.showShadow = false
        self.trafficCalloutView?.adjust(to: location, in: self.naviMapView)
        //self.adjustCameraWhenAmenityClicked(at: location, callout: trafficToolTip)

        
    }
    
    private func adjustCameraWhenAmenityClicked(at location: LocationCoordinate2D, callout: ToolTipsView) {
        
        let cameraOptions = CameraOptions(center: location, zoom: self.naviMapView.mapView.mapboxMap.cameraState.zoom) // using current zoom level
        self.naviMapView.mapView.camera.ease(to: cameraOptions, duration: Values.standardAnimationDuration, curve: .easeInOut, completion: nil)
        
        callout.adjust(to: location, in: self.naviMapView)
        

    }
}

extension TrafficCameraView: GestureManagerDelegate {
   
    func gestureManager(_ gestureManager: GestureManager, didEnd gestureType: GestureType, willAnimate: Bool) {
        
        //We don't need to add any logic here
    }
    
    func gestureManager(_ gestureManager: GestureManager, didEndAnimatingFor gestureType: GestureType) {
        
        //We don't need to add any logic here
    }
    
    func gestureManager(_ gestureManager: GestureManager, didBegin gestureType: GestureType) {
        
        var name = ParameterName.Home.MapInteraction.drag
        switch gestureType {
        case .pinch:
            name = ParameterName.Home.MapInteraction.pinch
        case .singleTap:
            self.didDeselectAnnotation()
            let point = gestureManager.singleTapGestureRecognizer.location(in: self.naviMapView)
            self.queryTrafficLayer(point: point)
            
        default:
            name = ParameterName.Home.MapInteraction.drag
        }

    }
}


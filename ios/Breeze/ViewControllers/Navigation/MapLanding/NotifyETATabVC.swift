//
//  NotifyETATabVC.swift
//  Breeze
//
//  Created by VishnuKanth on 11/08/21.
//

import UIKit

class NotifyETATabVC: UIViewController,UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    // MARK: - IBOutlet
    @IBOutlet weak var etaFavAdd: UIButton!
    @IBOutlet weak var viewAllBtn: UIButton!
    @IBOutlet weak var etaLabel:UILabel!
    @IBOutlet weak var etaViewHeight:UIView!
    @IBOutlet weak var notifyETACollectionView:UICollectionView!
    
    // MARK: - Public properties
    @Published private (set) var preferredHeight: CGFloat = 300
    var etaContactSelected: ((_ fav:Favourites) -> Void)?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setUpUI()
        // Do any additional setup after loading the view.
    }
    

    func setUpUI(){
        
        if(SavedSearchManager.shared.etaFav.count > 0)
        {
            etaFavAdd.isHidden =  true
            etaLabel.isHidden = true
            viewAllBtn.isHidden = false
            viewAllBtn.setTitle("Edit", for: .normal)
            notifyETACollectionView.isHidden = false
            notifyETACollectionView.dataSource = self
            notifyETACollectionView.delegate = self
            notifyETACollectionView.register(UINib.init(nibName: "NotifyCell", bundle: nil), forCellWithReuseIdentifier: "NotifyCell")
            
            guard let collectionView = notifyETACollectionView, let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout else { return }

                flowLayout.minimumInteritemSpacing = 10
                flowLayout.minimumLineSpacing = 10
                flowLayout.sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
            
            
            if(SavedSearchManager.shared.etaFav.count <= 2)
            {
                self.preferredHeight = 330
            }
            else
            {
                if(SavedSearchManager.shared.etaFav.count == 0)
                {
                    self.preferredHeight = 300
                }
                else
                {
                    self.preferredHeight = 535
                }
                
            }
            
            
            
        }
        else
        {
            self.preferredHeight = 300
            viewAllBtn.isHidden = true
            etaFavAdd.isHidden =  false
            etaLabel.isHidden = false
            notifyETACollectionView.isHidden = true
        }
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Home.UserClick.notify_arrival_tab, screenName: ParameterName.Home.screen_view)
        getETAFav()
    }
    
    func getETAFav(){
        
        DispatchQueue.global(qos: .background).async {
            
            ETAFavList.shared.getETAFavList() { [weak self] (result) in
                guard let self = self else { return }
//                print(result)
                switch result {
                case .success(let json):
                    DispatchQueue.main.async {
                        SavedSearchManager.shared.etaFav = json
                        self.setUpUI()
                        self.notifyETACollectionView.reloadData()
                    }
                    
                    
                case .failure(_):
                    DispatchQueue.main.async {
                        if(SavedSearchManager.shared.etaFav.count > 0)
                        {
                            SavedSearchManager.shared.etaFav.removeAll()
                            self.setUpUI()
                            self.notifyETACollectionView.reloadData()
                        }
                        
                    }
                }
            }
        }
    }
    
    func reloadViews(){
        
        self.getETAFav()
    }
    
    //MARK: - Collection View
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        var count = 0
        if(SavedSearchManager.shared.etaFav.count > 4)
        {
            count = 4
        }
        else
        {
            count = SavedSearchManager.shared.etaFav.count
        }
        
        return count
        
    }
    
//     func collectionView(_ collectionView: UICollectionView,
//                        layout collectionViewLayout: UICollectionViewLayout,
//                        insetForSectionAt section: Int) -> UIEdgeInsets {
//
//        return UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
//    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let noOfCellsInRow = 2   //number of column you want
            let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
            let totalSpace = flowLayout.sectionInset.left
                + flowLayout.sectionInset.right
                + (flowLayout.minimumLineSpacing * CGFloat(noOfCellsInRow - 1))

            let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(noOfCellsInRow))
        return CGSize(width: size, height: 210)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NotifyCell", for: indexPath) as! NotifyETACell
        
        let contactName = SavedSearchManager.shared.etaFav[indexPath.row].recipientName
        
        let address = SavedSearchManager.shared.etaFav[indexPath.row].destination
        
        let index = contactName!.index(contactName!.startIndex, offsetBy: 0)
        
        let contactLetter  = contactName![index]
        
        cell.configure(with: NotifyETAModel(contactName: contactName!, address: address!, contactFLetter: String(contactLetter)))
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if let callback = etaContactSelected {            
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Home.UserClick.fav_arrival, screenName: ParameterName.Home.screen_view)
            callback(SavedSearchManager.shared.etaFav[indexPath.row])
        }
        
    }

    
    //MARK: - Actions
    
    @IBAction func viewAll(_ sender:Any){
        
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Home.UserClick.view_all_arrival, screenName: ParameterName.Home.screen_view)
        
        let aComplexData = [
            "toScreen": "ETAFavouriteList", // Screen name you want to display when start RN
            "navigationParams": [ // Object data to be used by that screen
                "baseURL": appDelegate().backendURL,
                "idToken":AWSAuth.sharedInstance.idToken,
                "deviceos":parameters!["deviceos"],
                "deviceosversion":parameters!["deviceosversion"],
                "devicemodel":parameters!["devicemodel"],
                "appversion":parameters!["appversion"],
                "etaMode":"Cruise",
                "etaMessage":"Hello, \(AWSAuth.sharedInstance.userName) here! I am on my way to..."
                
            ]
        ] as [String : Any]
        let rootView = RNViewManager.sharedInstance.viewForModule(
            "Breeze",
            initialProperties: aComplexData)

        let reactNativeVC = UIViewController()
        reactNativeVC.view = rootView
        reactNativeVC.modalPresentationStyle = .fullScreen
        navigationController?.pushViewController(reactNativeVC, animated: true)
        
    }
    
    @IBAction func etaFavAdd(_ sender: Any) {
    
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Home.UserClick.add_arrival, screenName: ParameterName.Home.screen_view)
        
        let aComplexData = [
            "toScreen": "ETAFavouriteList", // Screen name you want to display when start RN
            "navigationParams": [ // Object data to be used by that screen
                "baseURL": appDelegate().backendURL,
                "idToken":AWSAuth.sharedInstance.idToken,
                "deviceos":parameters!["deviceos"],
                "deviceosversion":parameters!["deviceosversion"],
                "devicemodel":parameters!["devicemodel"],
                "appversion":parameters!["appversion"],
                "etaMode":"Cruise",
                "etaMessage":"Hello, \(AWSAuth.sharedInstance.userName) here! I am on my way to..."
                
            ]
        ] as [String : Any]
        let rootView = RNViewManager.sharedInstance.viewForModule(
            "Breeze",
            initialProperties: aComplexData)

        let reactNativeVC = UIViewController()
        reactNativeVC.view = rootView
        reactNativeVC.modalPresentationStyle = .fullScreen
        navigationController?.pushViewController(reactNativeVC, animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

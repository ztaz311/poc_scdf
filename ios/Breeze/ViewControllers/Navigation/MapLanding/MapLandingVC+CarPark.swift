//
//  MapLandingVC+CarPark.swift
//  Breeze
//
//  Functions related to the CarPlay in MapLanding
//
//  Created by Zhou Hao on 12/10/21.
//

import UIKit
import CoreLocation
import MapboxMaps
import RxSwift
import SwiftyBeaver

//extension MapLandingVC: ParkingViewDelegate {
//    func didUpdate(_ view: ParkingView, type: ParkingType) {
//        switch type {
//        case .all:
//            UserSettingsModel.sharedInstance.carparkAvailability = .all
//        case .available:
//            UserSettingsModel.sharedInstance.carparkAvailability = .available
//        case .hide:
//            UserSettingsModel.sharedInstance.carparkAvailability = .hide
//        }
//        didChangeParkingType()
//
//        mapLandingViewModel?.saveParkingViewType()
//    }
//
//    func didChangeViewType(_ view: ParkingView, viewType: ParkingViewType) {
//        switch viewType {
//        case .hidden:
//            appDelegate().window?.removeTapView(Values.tapViewTag)
//        case .collapse:
////            self.dismissNudgeToolTip()
//            appDelegate().window?.removeTapView(Values.tapViewTag)
//            //processRecenterMapview(fromCarpark: true)
//        case .expanded:
//            updateBottomSheetIndex(index: 0, toScreen: "Landing")
//            isTrackingUser = true
//            processExpandedParkingView()
//        }
//
////        if let button = feedbackButton, let viewModel = mapLandingViewModel {
////            if viewModel.bottomSheetIndex != 0 {
////
////                button.isHidden = viewType == .expanded
////            }
////        }
//    }
//
//    private func processExpandedParkingView() {
//        appDelegate().window?.addTapView(tag: Values.tapViewTag, handle: { [weak self] in
//            guard let self = self else { return }
//            self.parkingView?.collapseIfNeed(delay: 0.5)
//        })
//    }
//}

extension MapLandingVC: ParkingAndAmenitiesViewDelegate {
    
    func addAmenityType(_ type: String) {
        mapLandingViewModel?.addOrRemoveAmenityType(type, isAdding: true)
    }
    
    func removeAmenityType(_ type: String) {
        mapLandingViewModel?.addOrRemoveAmenityType(type, isAdding: false)
    }
    
    func didUpdate(_ view: ParkingAndAmenitiesView, type: ParkingAndAmenitiesType) {
        switch type {
        case .all:
            UserSettingsModel.sharedInstance.carparkAvailability = .all
        case .available:
            UserSettingsModel.sharedInstance.carparkAvailability = .available
        case .hide:
            UserSettingsModel.sharedInstance.carparkAvailability = .hide
        }
        didChangeParkingType()
        
        mapLandingViewModel?.saveParkingViewType()
    }
    
    func didChangeViewType(_ view: ParkingAndAmenitiesView, viewType: ParkingAndAmenitiesViewType) {
        switch viewType {
        case .hidden:
            appDelegate().window?.removeTapView(Values.tapViewTag)
        case .collapse:
//            self.dismissNudgeToolTip()
            appDelegate().window?.removeTapView(Values.tapViewTag)
            //processRecenterMapview(fromCarpark: true)
        case .expanded:
            updateBottomSheetIndex(index: 0, toScreen: "Landing")
            isTrackingUser = true
            processExpandedParkingView()
        }
        
//        if let button = feedbackButton, let viewModel = mapLandingViewModel {
//            if viewModel.bottomSheetIndex != 0 {
//
//                button.isHidden = viewType == .expanded
//            }
//        }
    }
    
    private func processExpandedParkingView() {
        appDelegate().window?.addTapView(tag: Values.tapViewTag, handle: { [weak self] in
            guard let self = self else { return }
            self.parkingAmenities?.collapseIfNeed(delay: 0.5)
        })
    }
}


// TODO: Move the carpark function to MapLandingViewModel so that can reuse in car play
extension MapLandingVC {
    
    func setUpExploreButton(){
        
        guard exploreView == nil else { return }
        
        exploreView = ExploreView()
        
        exploreView?.delegate = self
        
        guard let exploreView = exploreView else { return }
        
        exploreView.updatedExploreButtonImage()
        
        navMapView.mapView.addSubview(exploreView)
        
        exploreView.snp.makeConstraints { make in
            make.leading.equalTo(self.parkingView!).offset(0)
            make.top.equalTo(self.parkingView!.snp.bottom).offset(16)
        }
        
//        // update read status
//        self.updateExploreStatus()
    }
    
//    func setupParkingButton() {
////        guard parkingButton == nil else {
////            return
////        }
//////
////        let size = Images.toggleParkingBtn!.size
////        let button = ToggleButton(frame: CGRect(x: 0, y: 0, width: size.width, height: size.height))
////        button.onImage = Images.toggleParkingBtn
////        button.offImage = Images.toggleParkingBtnOn
////        button.onToggled = { [weak self] isSelected in
////            guard let self = self else { return }
////            self.onParking(isParking: isSelected)
////        }
////        navMapView.mapView.addSubview(button)
////        button.translatesAutoresizingMaskIntoConstraints = false
////        button.snp.makeConstraints { make in
////            make.trailing.equalToSuperview().offset(0)
////            make.top.equalToSuperview().offset(44)
////            make.size.equalTo(CGSize(width: size.width, height: size.height))
////        }
////
////        self.parkingButton = button
//
//        guard parkingView == nil else { return }
//
//        parkingView = ParkingView()
//
//        parkingView?.delegate = self
//
//        guard let parkingView = parkingView else { return }
//
//        parkingView.parkingViewType = .collapse
//
//        navMapView.mapView.addSubview(parkingView)
//
//        parkingView.snp.makeConstraints { make in
//            make.trailing.equalToSuperview().offset(0)
//            make.top.equalToSuperview().offset(getTopOffset() + 4)
//        }
//    }
    
    func setUpParkingAndAmenities() {
        guard parkingAmenities == nil else { return }
        
        parkingAmenities = ParkingAndAmenitiesView()
        
        parkingAmenities?.delegate = self
        
        guard let parkingView = parkingAmenities else { return }
        
        parkingView.parkingViewType = .collapse
        
        navMapView.mapView.addSubview(parkingView)
        
        parkingView.snp.makeConstraints { make in
            make.trailing.equalToSuperview().offset(-8)
            make.top.equalToSuperview().offset(getTopOffset() + 4)
        }
        
    }
    
    func updateExploreStatus() {
        ContentService().getContentsV2 { [weak self] result in
            guard let self = self, let exploreView = self.exploreView else { return }
            
            switch result {
            case .success(let content):
                self.mapLandingViewModel?.newContentIds.removeAll()
                let newContent = content.contentDetails.filter { $0.isNewLayer }
                if !newContent.isEmpty {
                    for item in newContent {
                        self.mapLandingViewModel?.newContentIds.append(item.contentID)
                    }
                } else {
                    exploreView.isNewContent = newContent.count > 0
                }
                // get read content ids
                let ids = Prefs.shared.getSeenCustomMapIds(.seenCustomMapIDs)
                if !ids.isEmpty {
                    var hasNewContent = false
                    for item in newContent {
                        if !ids.contains(item.contentID) {
                            // there is a new content
                            hasNewContent = true
                            break
                        }
                    }
                    // all new contents have been seen or no new content
                    exploreView.isNewContent = hasNewContent
                }else {
                    exploreView.isNewContent = false
                }
                // check new if is new user
                let currentDate = Date().currentTimeInMiliseconds()
                let thresholdDaysNewUser = Prefs.shared.getIntValue(.thresholdDaysNewUser)
                if currentDate < thresholdDaysNewUser {
                    exploreView.isNewContent = false
                }
                return
            case .failure(let error):
                SwiftyBeaver.error("Failed to get contents: \(error.localizedDescription)")
                break
            }
        }
    }
    
    private func didChangeParkingType() {
        guard let viewModel = mapLandingViewModel else { return }
        carparkFromBottomSheet = false
        
//        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Home.UserClick.parking, screenName: ParameterName.Home.screen_view)
        self.prevCarParks = nil
        
        var isShowCarParkList = true
        AmenitiesSharedInstance.shared.isUpdateCarParkLayer = false
        switch self.parkingAmenitiesMode {
        case .all:
//            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Home.UserClick.carpark_all_nearby, screenName: ParameterName.Home.screen_view)
            self.showParking(viewModel: viewModel)
        case .available:
//            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Home.UserClick.carpark_available_only, screenName: ParameterName.Home.screen_view)
            self.showParking(viewModel: viewModel)
        default:
            isShowCarParkList = false
//            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Home.UserClick.carpark_hide, screenName: ParameterName.Home.screen_view)
            self.hideParking(viewModel: viewModel)
        }
                
        shareBusinessShowCarParkList(isShowCarPark: isShowCarParkList)
    }
    
    // Send event to React Native
    func shareBusinessShowCarParkList(isShowCarPark: Bool) {
        let data = ["is_show": isShowCarPark]
        _ = ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.SHOW_OR_HIDE_BUTTON_CARPARK_LIST,
                                            data: data as NSDictionary).subscribe(onSuccess: { [weak self] _ in
            guard let self = self else { return }
            self.isShowCarPark = isShowCarPark
        },         onFailure: {_ in
            //We don’t need to process this
        })
    }
    
//    @objc func onParking(isParking: Bool) {
//        guard let viewModel = mapLandingViewModel else { return }
//
//        carparkFromBottomSheet = false
//        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Home.UserClick.parking, screenName: ParameterName.Home.screen_view)
//        self.prevCarParks = nil
//        self.isParkingOn = isParking
//        if (self.isParkingOn){
//
//            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Home.UserToggle.parking_icon_on, screenName: ParameterName.Home.screen_view)
//
//            self.showParking(viewModel: viewModel)
//        }else{
//            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Home.UserToggle.parking_icon_off, screenName: ParameterName.Home.screen_view)
//            self.hideParking(viewModel: viewModel)
//        }
//
//    }
    
    // after search or from bottom sheet, resume parking status
    // don't need to toggle parking
    func resumeParking() {
        guard let viewModel = mapLandingViewModel else { return }
        
        carparkFromBottomSheet = false
        
        dropPinButton?.isHidden = false
        if(self.nudgeCallout != nil){
            
            parkingAmenities?.parkingViewType = .expanded
        }
        else{
            parkingAmenities?.parkingViewType = .collapse
        }
        
        AmenitiesSharedInstance.shared.isUpdateCarParkLayer = false
        var isShowCarparkList = true
        switch self.parkingAmenitiesMode {
        case .all:
//            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Home.UserClick.carpark_all_nearby, screenName: ParameterName.Home.screen_view)
            self.showParking(viewModel: viewModel)
        case .available:
//            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Home.UserClick.carpark_available_only, screenName: ParameterName.Home.screen_view)
            self.showParking(viewModel: viewModel)
        default:
            isShowCarparkList = false
//            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Home.UserClick.carpark_hide, screenName: ParameterName.Home.screen_view)
            self.hideParking(viewModel: viewModel)
        }
        
        shareBusinessShowCarParkList(isShowCarPark: isShowCarparkList)
    }
    
    // showCallout - if need to show callout by default
//    private func toggleParking() {
//        guard let viewModel = mapLandingViewModel else { return }
//
//        self.isParkingOn = !self.isParkingOn
//        if (self.isParkingOn){
//            self.showParking(viewModel: viewModel)
//        }else{
//            self.hideParking(viewModel: viewModel)
//        }
//    }
    
    func clearParking(){
        self.navMapView.mapView.removeCarparkLayer()
        self.navMapView.pointAnnotationManager?.annotations = [PointAnnotation]()
    }
    
    private func showParking(viewModel: MapLandingViewModel) {
//        if !carparkFromBottomSheet {
//            self.parkingButton.setImage(UIImage(named: "parkingOn"), for: .normal)
//        }
        
        //call api carpark nearby
        if self.shouldUpdateCarparks() {
            
            let centerMapLocation = navMapView.getMapCenterCoordinate(mapLandingViewModel?.getActualBottomSheetHeight() ?? 0)
            mapLandingViewModel?.centerMapLocation = centerMapLocation
            
            //  Need update petrol/EV when user pan map
            if mapLandingViewModel?.isOnDropPin == true {
                parkingAmenities?.updateAmenitiesWhenUserPanMap()
            }
            
            if viewModel.isSearchDisocver, let _ = viewModel.searchDiscoverLocation {
                loadCarparks(location: centerMapLocation, discoverMode: true, arrivalTime: Date(), destName: self.destName, carParkId: mapLandingViewModel?.carparkId ?? "")
            }else {
                loadCarparks(location: centerMapLocation, discoverMode: false, carParkId: mapLandingViewModel?.carparkId ?? "")
            }
        }else {
            if viewModel.isSearchDisocver, let location = viewModel.searchDiscoverLocation {
                
                loadCarparks(location: location, discoverMode: true, arrivalTime: Date(), destName: self.destName, carParkId: mapLandingViewModel?.carparkId ?? "")
    
            }
            else{
                
                if SelectedProfiles.shared.selectedProfileName() != "" {
                    
                    if let location = SelectedProfiles.shared.getOuterZoneCentrePoint(){
                        
                        loadCarparks(location: location, discoverMode: false, carParkId: mapLandingViewModel?.carparkId ?? "")
                    }
                }
                else{
                    
                    loadCarparks(location: LocationManager.shared.location.coordinate, discoverMode: false, carParkId: mapLandingViewModel?.carparkId ?? "")
                }
                
            }
        }
            
        navMapView.pointAnnotationManager?.delegate = self
    }
    
    private func hideParking(viewModel: MapLandingViewModel) {
        DispatchQueue.main.async {
//            if !self.carparkFromBottomSheet {
//                self.parkingButton.setImage(UIImage(named: "parkingOff"), for: .normal)
//            }
            self.navMapView.mapView.removeCarparkLayer()
            if viewModel.isSearchDisocver, let location = viewModel.searchDiscoverLocation {
                
                // no need to check if profile selected or not otherwise the destination icon will be missed
                //            if(!SelectedProfiles.shared.isProfileSelected()){
                self.navMapView.pointAnnotationManager?.annotations = [PointAnnotation]()
                self.navMapView.pointAnnotationManager = self.navMapView.mapView.annotations.makePointAnnotationManager(id: CarparkValues.destinationLayerIdentifier, layerPosition: .above("puck"))
                var destinationAnnotation = PointAnnotation(coordinate: location)
                destinationAnnotation.image = .init(image: UIImage(named: "destinationWithoutCarpark")!, name: "routePlan")
                self.navMapView.pointAnnotationManager?.annotations = [destinationAnnotation]
                //            }
                //            else{
                //
                //                self.navMapView.pointAnnotationManager?.annotations = [PointAnnotation]()
                //            }
                
            } else {
                self.navMapView.pointAnnotationManager?.annotations = [PointAnnotation]()
            }
            
            if self.carparkToast != nil{
                self.carparkToast?.removeFromSuperview()
            }
            
            self.carparkUpdater = nil
            self.didDeselectAnnotation()
            
            self.navMapView.pointAnnotationManager?.delegate = self
            
        }
        #if DEMO
        #else
//        if(AmenitiesSharedInstance.shared.amenitiesSymbolLayer.count == 0 && viewModel.landingMode != Values.ERP_MODE){ //Reset camera only if amenities are 0
//            if currentGlobalNotification == nil && !viewModel.isEventFromInbox {
//                resetCamera()
//            }
//        }
        #endif
    }
    
    func HideAndShowExploreView(show:Bool){
        
        guard let exploreView = self.exploreView else { return }
        
        exploreView.isHidden = !show
    }
        
    func onParkingFromBottomSheet(parkingOn: Bool, showMapParkingIcon: Bool) {
        guard let viewModel = mapLandingViewModel else { return }
        
        carparkFromBottomSheet = true
        
        //  When user switch tab from bottom sheet the need reset pan state
        needUpdateCarparks = false
        viewModel.centerMapLocation = nil
        
        self.parkingAmenities?.parkingViewType = !showMapParkingIcon ? .hidden : .collapse
        self.dropPinButton?.isHidden = !showMapParkingIcon
        
        if(!showMapParkingIcon){
//            self.dismissNudgeToolTip()
        }
//        self.parkingButton.isHidden = !showMapParkingIcon
        self.isParkingOnBottom = parkingOn
        if parkingOn {
            showParking(viewModel: viewModel)
        } else {
            hideParking(viewModel: viewModel)
        }
    }
        
    func loadCarparks(location: CLLocationCoordinate2D, discoverMode: Bool, arrivalTime: Date = Date(), destName: String = "", carParkId: String = "") {
        
        if(UserSettingsModel.sharedInstance.carparkAvailability == .hide && !carparkFromBottomSheet){
            return
        }
        carparkUpdater = CarParkUpdater(destName: "",
                                        carPlayNavigationMapView: nil,
                                        mobileNavigationMapView: self.navMapView,
                                        fromScreen: .mapLanding)
        carparkUpdater?.delegate = self
        
        carparkUpdater?.configure(arrivalTime: arrivalTime, destName: destName)
        
        carparkUpdater?.setDiscoveryMode(discoverMode: discoverMode)
        
        carparkUpdater?.isVoucher = false
        
        //If user selects Parking Calculator the ParkinType value should always be .all
        carparkUpdater?.carparkAvailability = carparkFromBottomSheet == true ? .all : UserSettingsModel.sharedInstance.carparkAvailability
        
        if carparkFromBottomSheet == false && SelectedProfiles.shared.selectedProfileName() != ""{
            
            carparkUpdater?.setCPClusterMode(isCluster: false)
        }
        /*if((AmenitiesSharedInstance.shared.amenitiesSymbolLayer.count > 0 && !carparkFromBottomSheet) || (!carparkFromBottomSheet && SelectedProfiles.shared.selectedProfileName() != "")){
            
            carparkUpdater?.setCPClusterMode(isCluster: true)
        }*/
        
        carparkUpdater?.loadCarParks(maxCarParksCount: UserSettingsModel.sharedInstance.carparkCount,
                                     radius:SelectedProfiles.shared.isProfileSelected() ? SelectedProfiles.shared.getOuterZoneRadius() : UserSettingsModel.sharedInstance.carparkDistance,
                                     maxCarParkRadius: Values.carparkMonitorMaxDistanceInLanding, location: location, carParkId: carParkId)
        
        return
    }
    
    func adjustZoomForCarParks(){
        
        var distanceMonitor = 0
        var carParkCoordinate = [CLLocationCoordinate2D]()
        if let prevCarPark = self.prevCarParks {
            
            prevCarPark.forEach { carPark in
                
                let distance = carPark.distance ?? 0
                if(distanceMonitor == 0){
                    
                    distanceMonitor = carPark.distance ?? 0
                    
                    if(carParkCoordinate.count > 0){
                        carParkCoordinate.removeAll()
                        carParkCoordinate.append(CLLocationCoordinate2D(latitude: carPark.lat ?? 0.0, longitude: carPark.long ?? 0.0))
                    }
                    else{
                        
                        carParkCoordinate.append(CLLocationCoordinate2D(latitude: carPark.lat ?? 0.0, longitude: carPark.long ?? 0.0))
                    }
                }
                    
                    if(distanceMonitor < distance){
                        
                        distanceMonitor = distance
                        if(carParkCoordinate.count > 0){
                            carParkCoordinate.removeAll()
                            carParkCoordinate.append(CLLocationCoordinate2D(latitude: carPark.lat ?? 0.0, longitude: carPark.long ?? 0.0))
                        }
                        else{
                            
                            carParkCoordinate.append(CLLocationCoordinate2D(latitude: carPark.lat ?? 0.0, longitude: carPark.long ?? 0.0))
                        }
                    }
                
            }
            
            if let model = self.mapLandingViewModel {
                
                var height = 0
                if (model.bottomSheetHeight == 0 ){
                    
                    height = 416
                }
                else{
                    
                    height = model.bottomSheetHeight
                }
                let padding = UIEdgeInsets(top: 280, left: 10, bottom:  CGFloat(height) + 200, right: 10)
                if carParkCoordinate.count > 0 && currentGlobalNotification == nil && !model.isEventFromInbox
                    
                 {
                    
                    
                    if let searchDiscoverLocation = model.searchDiscoverLocation {
                        
                        if model.centerMapLocation == nil {
                            self.navMapView.mapView.setDynamicZoomLevelBasedonRadius(coordinate1: carParkCoordinate[0], dynamicRadius: distanceMonitor, padding: padding, coordinate2: searchDiscoverLocation)
                        }
                    }else if model.centerMapLocation != nil {
//                        self.navMapView.mapView.setDynamicZoomLevelBasedonRadius(coordinate1: carParkCoordinate[0], dynamicRadius: distanceMonitor, padding: padding, coordinate2: centerMapLocation)
                    }
                    else{
                        if SelectedProfiles.shared.selectedProfileName() != ""{
                            
                            if let profileCoord = SelectedProfiles.shared.getOuterZoneCentrePoint(){
                                
                                self.navMapView.mapView.setDynamicZoomLevelBasedonRadius(coordinate1: carParkCoordinate[0], dynamicRadius: SelectedProfiles.shared.getOuterZoneRadius(), padding: padding, coordinate2: profileCoord)
                            }
                        }
                        else{
                            
                            self.navMapView.mapView.setDynamicZoomLevelBasedonRadius(coordinate1: carParkCoordinate[0], dynamicRadius: distanceMonitor, padding: padding, coordinate2: LocationManager.shared.location.coordinate)
                        }
                        
                    }
                } else {
                    if let carParkCoor = carParkCoordinate.first {
                        self.navMapView.mapView.setDynamicZoomLevelBasedonRadius(coordinate1: carParkCoor, dynamicRadius: distanceMonitor, padding: padding, coordinate2: LocationManager.shared.location.coordinate)
                    }
                  
                }
            }
        }
    }
    
    // TODO: Refactor later. Should use the same logic as in RoutePlanning. The service only return one carpark if > radius and < maxRaduis
    private func updateCarparks(_ carparks: [Carpark], location: CLLocationCoordinate2D, discoverMode: Bool) {
        var annotations = [PointAnnotation]()
        var destinationCarparkAnnotation: Annotation? = nil
        var cheapestAnnotation: Annotation? = nil // should be nearest in new user story
        var firstOutOfRangeCarpark: Carpark? // carpark outside 500m
        
        if let first = carparks.first {
            if (first.distance ?? 0) > Values.carparkMonitorDistanceInLanding {
                firstOutOfRangeCarpark = first
                // only show one carpark
                if let annotation = self.addCarparkAnnotation(carpark: first,isDiscoverMode: discoverMode) {
                    if first.destinationCarPark ?? false  {
                        destinationCarparkAnnotation = annotation
                    }

                    annotations.append(annotation)
                    if carparkFromBottomSheet{
                        cheapestAnnotation = annotation
                    }
                }
            } else {
                carparks.forEach { carPark in
                    if let annotation = self.addCarparkAnnotation(carpark: carPark,isDiscoverMode: discoverMode) {
                        if carPark.destinationCarPark ?? false  {
                            destinationCarparkAnnotation = annotation
                        }
                        annotations.append(annotation)
                        
                        // for showing tooltips
                        // 1. If destination is there always showing tooltips on it
                        // 2. If no destination show the nearest which is the first element
                        if carparkFromBottomSheet {
                            if destinationCarparkAnnotation != nil {
                                self.isDestinationHasCarPark = true
                                cheapestAnnotation = destinationCarparkAnnotation
                            } else {
                                // get the first annotation which should be the nearest
                                if cheapestAnnotation == nil {
                                    self.isDestinationHasCarPark = false
                                    cheapestAnnotation = annotation
                                }
                            }
                        }
                    }
                }
            }
        }
        
        if discoverMode {
            if destinationCarparkAnnotation == nil { // add original destination icon
                var destinationAnnotation = PointAnnotation(coordinate: location)
                destinationAnnotation.image = .init(image: UIImage(named: "destinationWithoutCarpark")!, name: "routePlan")
                annotations.append(destinationAnnotation)
            }
        }
        
        self.navMapView.pointAnnotationManager?.annotations = annotations
        
        if let annotation = cheapestAnnotation {
            self.didSelectAnnotation(annotation: annotation, tapped: true)
        }

        if carparks.count > 0 {
            self.navMapView.mapView.removeCarparkLayer()
//                    print("show carparks")
            
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                
                // if there is no carpark within carparkMonitorDistanceInLanding (500m), then need to setup zoom level
                if let first = firstOutOfRangeCarpark {
                    // only need to show one carpark
                    self.navMapView.mapView.addCarparks([first], showOnlyWithCostSymbol: true)
                    
                    DispatchQueue.main.async {
                        if(self.carparkToast != nil){
                            self.carparkToast?.removeFromSuperview()
                        }
                        
                        if let model = self.mapLandingViewModel {
                            
                            if self.currentGlobalNotification == nil && !model.isEventFromInbox{
                                if model.centerMapLocation == nil, !self.shouldUpdateCarparks(), !model.didShowNearByNotification {
                                    self.mapLandingViewModel?.didShowNearByNotification = true
                                    self.carparkToast = BreezeToast(appearance: BreezeToastStyle.carparkToast, onClose: {
                                        self.carparkToast = nil
                                    }, actionTitle: "", onAction: nil)

                                    self.carparkToast!.show(in: self.navMapView, title: "Nearby Carpark", message: Constants.toastNearestCarpark, animated: true)
                                    self.carparkToast!.yOffset = getTopOffset()
                                }
                            }
                        }
                        
                    }
                    
                    //self.setupZoomelevel(coordinate1: location, coordinate2: CLLocationCoordinate2D(latitude: first.lat ?? 0, longitude: first.long ?? 0))
                    if(AmenitiesSharedInstance.shared.amenitiesSymbolLayer.count == 0){
                        
                        if self.mapLandingViewModel != nil{
                            
                            //if(viewModel.landingMode != Values.PARKING_MODE){
                                self.adjustZoomForCarParks()
                            //}
                        }
                        
                    }
                    self.isTrackingUser = true
                } else {
                    self.navMapView.mapView.addCarparks(carparks, showOnlyWithCostSymbol: true)
                    
                    if(AmenitiesSharedInstance.shared.amenitiesSymbolLayer.count == 0){
                        
                        if self.mapLandingViewModel != nil{
                            
                            //if(viewModel.landingMode != Values.PARKING_MODE){
                                self.adjustZoomForCarParks()
                            //}
                        }
                        
                    }
                    
                }
            }
        }
    }
    
    private func setupZoomelevel(coordinate1: CLLocationCoordinate2D, coordinate2: CLLocationCoordinate2D) {
        
        #if DEMO
        // Don't setup the zoom level after carpark button clicked
        #else
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
                                    
            // use coordinate1 as center point calcuate the symmetric coordinate3
            let degree = coordinate2.direction(to: coordinate1)
            let distance = coordinate2.distance(to: coordinate1)
            let coordinate3 = coordinate1.coordinate(at: distance, facing: degree)
            
            var bottom = 100
            if let vm = self.mapLandingViewModel {
                bottom = vm.bottomSheetHeight + 150
            }
            
            let cameraOptions = self.navMapView.mapView.mapboxMap.camera(for: [coordinate2, coordinate1, coordinate3], padding: UIEdgeInsets(top: 150, left: 50, bottom:CGFloat(bottom), right: 100), bearing: 0, pitch: 0)
            self.navMapView.mapView.camera.ease(to: cameraOptions, duration: Values.standardAnimationDuration, curve: .easeInOut, completion: nil)
        }
        #endif
    }
    
    private func getNearestCarpark(carparks: [Carpark]) -> Carpark? {
        // get the cheapest (which is the nearest carpark)
        if let nearest = (carparks.filter { $0.isCheapest ?? false }).first {
            return nearest
        }
        return nil
    }
        
    private func addCarparkAnnotation(carpark: Carpark,isDiscoverMode:Bool) -> PointAnnotation? {

        guard let carparkId = carpark.id, !carparkId.isEmpty else {return nil}
        
        var annotation = PointAnnotation(id:carparkId,coordinate: CLLocationCoordinate2D(latitude: carpark.lat ?? 0, longitude: carpark.long ?? 0))
        // In maplanding we should ignore the destination icon if it's not in discovermode
        annotation.image = getAnnotationImage(carpark: carpark, selected: false, ignoreDestination: !isDiscoverMode)
        
        //annotation.textField = carpark.id
        annotation.userInfo = [
            "carpark": carpark
        ]
                
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            if let layerId = self.navMapView.pointAnnotationManager?.layerId {
                try? self.navMapView.mapView.mapboxMap.style.setLayerProperty(for: layerId, property: "icon-allow-overlap", value: true)
            }
        }
        annotation.symbolSortKey = Double(carpark.distance ?? 0)
        return annotation
    }
}

extension MapLandingVC: AnnotationInteractionDelegate {
    func annotationManager(_ manager: AnnotationManager, didDetectTappedAnnotations annotations: [Annotation]) {
        
        // possible multiple tapped if there are overlapped together, only show the first one
        if let annotation = annotations.first {
            didSelectAnnotation(annotation: annotation, tapped: true)
        }
    }

    func didSelectAnnotation(annotation: Annotation, tapped: Bool = false) {

        DispatchQueue.main.async { [weak self] in
            guard let self = self, let vm = self.mapLandingViewModel, let location = vm.searchDiscoverLocation else { return }
            let address = vm.searchAddress  //  Address2
            let name = vm.destName          //  Address1
            let fullAddress = vm.fullAddress
            let placeId = vm.placeId
            
            self.didDeselectAnnotation()
            self.didDeselectAmenityAnnotation()

            // TODO: How to know if the destination is bookmarked?
            let tooltip = DestinationTooltipView(name: name, address: address, address2: address, fullAddress: fullAddress, location: location, availablePercentImageBand: vm.availablePercentImageBand, availabilityCSData: vm.availabilityCSData)
            tooltip.viewModel = vm
            
            tooltip.isSaved = vm.isSearchAddressBookmarked
            
            tooltip.bookmarkId = vm.bookmarkId
            
            tooltip.placeId = vm.placeId
            
            tooltip.onSaveAction = { isSaved in
                sendSaveActionToRN(amenityId: "",
                                   lat: location.latitude,
                                   long: location.longitude,
                                   name:  name,
                                   address1: name,
                                   address2:address,
                                   amenityType: "",
                                   fullAddress: fullAddress,
                                   isDestination: true,
                                   isSelected: vm.isSearchAddressBookmarked,
                                   placeId: placeId)
            }
            
            tooltip.present(from: self.navMapView.mapView.frame, in: self.navMapView.mapView, constrainedTo: tooltip.frame, animated: true)
            tooltip.center = self.view.center
            
            self.view.bringSubviewToFront(tooltip)
            tooltip.adjust(to: location, in: self.navMapView)
            
            let stringLat =  location.latitude.toString()
            let stringLong = location.longitude.toString()
           
            tooltip.shareViaLocation = {

            //                self.shareToolTipLocation("\(name) \(AWSAuth.sharedInstance.userName) share with you on Breeze https://breeze.com.sg")
                shareLocationFromNative(address1: name, address2: address, latitude: stringLat, longtitude: stringLong, code: "", is_destination: true, amenity_id: "", amenity_type: "", description: "", name: name, fullAddress: fullAddress, bookmarkId: vm.bookmarkId ?? 0, placeId: vm.placeId)
                
            }
            
            
            tooltip.inviteLocation = {
                inviteLocationFromNative(address1: name, address2: address, latitude: stringLat, longtitude: stringLong, code: "", is_destination: true, amenity_id: "", amenity_type: "", description: "", name: name, fullAddress: fullAddress, bookmarkId: vm.bookmarkId ?? 0, placeId: vm.placeId)
            }
            
            tooltip.onNavigationHere = { [weak self]  in
                guard let self = self else { return }
                let address = SearchAddresses(alias: "", address1: name, lat: stringLat, long: stringLong, address2: address, distance: "0", fullAddress: fullAddress, isCrowsourceParking: true, placeId: placeId)
                let storybard = UIStoryboard(name: "NewRoutePlanning", bundle: nil)
                if let vc = storybard.instantiateViewController(withIdentifier: String(describing: NewRoutePlanningVC.self)) as? NewRoutePlanningVC {
                    vc.originalAddress = address
                    vc.addressReceived = true
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
            
            
            self.toolTipCalloutView = tooltip

            // This code is not used anymore.
/*
            if let carpark = annotation.userInfo?["carpark"] as? Carpark {
                
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Home.UserClick.parking_more_info, screenName: ParameterName.Home.screen_view)
                
                if carpark.id == self.currentSelectedCarpark?.id {
                    self.currentSelectedCarpark = nil
                    // click the same one, don't show callout
                    return
                }
                self.currentSelectedCarpark = carpark
//                self.currentSelectedParkAnnotation = annotation
                self.updateAnnotationImage(annotation, select: true)
                
                // set camera (actually should not set if not because of tapped
                if tapped {
                    let cameraOptions = CameraOptions(center: CLLocationCoordinate2D(latitude: carpark.lat ?? 0, longitude: carpark.long ?? 0), zoom: self.navMapView.mapView.mapboxMap.cameraState.zoom) // using current zoom level
                    self.navMapView.mapView.camera.ease(to: cameraOptions, duration: Values.standardAnimationDuration, curve: .easeInOut, completion: nil)
                    
                    // every time user click the parking icon. We'll send an RN event
                    let _ = ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.PARKING_BOTTOM_SHEET_REFRESH, data: ["parkingID":carpark.id ?? "","isDestinationHasCarPark":self.isDestinationHasCarPark] as? NSDictionary).subscribe(onSuccess: {_ in
                        //We don't need to process this response
                    },onFailure: {_ in
                        //We don't need to process this response
                    })
                }
                
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Home.UserClick.tap_carpark, screenName: ParameterName.Home.screen_view)
                
                let callOutView = CarparkToolTipsView(carpark: carpark, calculateFee: !self.carparkFromBottomSheet)
                callOutView.present(from: self.navMapView.mapView.frame, in: self.navMapView.mapView, constrainedTo: callOutView.frame, animated: true)
                self.calloutView = callOutView
                self.calloutView?.adjust(to: self.calloutView!.tooltipCoordinate, in: self.navMapView)
                
                if let viewModel = self.mapLandingViewModel{
                    
                    self.adjustZoomOnlyForAmenity(at: self.calloutView!.tooltipCoordinate, callout: self.calloutView!, viewModel: viewModel)
                }
                
                self.calloutView!.onCarparkSelected = { [weak self] theCarpark in
                    guard let self = self else { return }
                    
                    AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Home.UserClick.tap_carpark, screenName: ParameterName.Home.screen_view)
                    
                    let location = CLLocation(latitude: theCarpark.lat ?? 0, longitude: theCarpark.long ?? 0)
                    getAddressFromLocation(location: location) { address in
                        let address = SearchAddresses(alias: "", address1: theCarpark.name ?? "", lat: "\(theCarpark.lat ?? 0)", long: "\(theCarpark.long ?? 0)", address2: address, distance: "\(theCarpark.distance ?? 0)")
                      //  self.delegate?.onCarparkAddressSelected(address: address)
                        let storybard = UIStoryboard(name: "NewRoutePlanning", bundle: nil)
                        if let vc = storybard.instantiateViewController(withIdentifier: String(describing: NewRoutePlanningVC.self)) as? NewRoutePlanningVC {
                            vc.originalAddress = address
                            vc.addressReceived = true
                            vc.addressReceived = true
                            vc.selectedCarPark = theCarpark
                            self.navigationController?.pushViewController(vc, animated: true)
                            // close the tooltips
                            self.didDeselectAnnotation()
                        }
                    }
                }
                self.calloutView!.onCalculate = { [weak self] theCarpark in
                    guard let self = self else { return }
                    AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Home.UserClick.parking_calculatefee, screenName: ParameterName.Home.screen_view)
                    
//                    let data = [
//                        "toScreen": "OpenCaculateFeeCarpark", // Screen name you want to display when start RN
//                        "navigationParams": [ // Object data to be used by that screen
//                            "parkingID": theCarpark.id
//                        ]
//                    ] as [String : Any]
//                    let rootView = RNViewManager.sharedInstance.viewForModule(
//                        "Breeze",
//                        initialProperties: data)
//
//                    let reactNativeVC = UIViewController()
//                    reactNativeVC.view = rootView
//                    reactNativeVC.modalPresentationStyle = .pageSheet
//                    self?.present(reactNativeVC, animated: true, completion: nil)
                    let _ = ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.OPEN_CALCULATE_FEE_CARPARK, data: ["parkingID":theCarpark.id] as? NSDictionary)
                        .subscribe(onSuccess: { _ in
                            //We don't need to process this
                        }, onFailure: { _ in
                            //We don't need to process this
                        })
                    self.didDeselectAnnotation()
                }
                self.calloutView!.onMoreInfo = { [weak self]  theCarpark in
                    guard let self = self else { return }
                    
                    AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Home.UserClick.parking_more_info, screenName: ParameterName.Home.screen_view)

                    self.didDeselectAnnotation()
                    let carParkData = [
                                "toScreen": "ParkingPriceDetail", // Screen name you want to display when start RN
                                "navigationParams": [ // Object data to be used by that screen
                                    "parkingId": theCarpark.id,
                                    "idToken":AWSAuth.sharedInstance.idToken,
                                    "baseURL": appDelegate().backendURL
                                ],
                            ] as [String : Any]
                            let rootView = RNViewManager.sharedInstance.viewForModule(
                                "Breeze",
                                initialProperties: carParkData)
                            
                            let reactNativeVC = UIViewController()
                            reactNativeVC.view = rootView
                            reactNativeVC.modalPresentationStyle = .pageSheet
                          self.present(reactNativeVC, animated: true, completion: nil)

                }
            }
*/
        }
    }

    func didDeselectAnnotation() {
        if let callout = self.toolTipCalloutView {
            callout.dismiss(animated: true)
            callout.removeFromSuperview()
            self.toolTipCalloutView = nil
        }
        currentSelectedCarpark = nil
   }
    
    func removeCalloutView() {
        if let callout = self.toolTipCalloutView {
            callout.dismiss(animated: true)
            callout.removeFromSuperview()
            self.toolTipCalloutView = nil
        }
    }
    
    func updateAnnotationImage(_ selectedAnnotation: Annotation, select: Bool) {
        if let carpark = selectedAnnotation.userInfo?["carpark"] as? Carpark, let viewModel = mapLandingViewModel {
            if let annotationMgr = navMapView.pointAnnotationManager {
                let id = selectedAnnotation.id
                
                /// There could be a possiblity where annotations count be 0. So to avoid range crash issue, we can check if count > 0
                if(annotationMgr.annotations.count > 0){
                    
                    for i in 0...annotationMgr.annotations.count-1 {
                        if annotationMgr.annotations[i].id == id {
                            annotationMgr.annotations[i].image = getAnnotationImage(carpark: carpark, selected: select, ignoreDestination: !viewModel.isSearchDisocver)
                        }
                    }
                }
                
            }
        }
    }
    
    func monitorCarParkDestination() {
        
        guard let viewModel = mapLandingViewModel, let carParkUpdater = self.carparkUpdater else { return }
        
        var cheapestCarpark: Carpark?
        
        if let carparks = self.prevCarParks, viewModel.isSearchDisocver, let location = viewModel.searchDiscoverLocation{
            
            var firstOutOfRangeCarpark: Carpark?
            
            var destinationCarpark: Carpark?
            
            // For destination annotation only
            var annotations = [PointAnnotation]()
            
            if let first = carparks.first {
                if (first.distance ?? 0) > Values.carparkMonitorDistanceInLanding {
                    firstOutOfRangeCarpark = first
                    // only show one carpark
                    
                    if first.destinationCarPark ?? false  {
                        destinationCarpark = first
                    }
                    
                    if carparkFromBottomSheet{
                        cheapestCarpark = first
                    }
                } else {
                    // for showing tooltips
                    // 1. If destination is there always showing tooltips on it
                    // 2. If no destination show the nearest which is the first element
                    carparks.forEach { carPark in
                        
                        if carPark.destinationCarPark ?? false  {
                            destinationCarpark = carPark
                        }
                        
                        if carparkFromBottomSheet {
                            if destinationCarpark != nil {
                                self.isDestinationHasCarPark = true
                                cheapestCarpark = destinationCarpark
                            } else {
                                if cheapestCarpark == nil {
                                    self.isDestinationHasCarPark = false
                                    cheapestCarpark = carPark
                                }
                            }
                        }
                    }
                }
            }
            
            if !(carparkUpdater?.shouldIgnoreDestination ?? false) {
                if destinationCarpark == nil { // add original destination icon
                    var destinationAnnotation = PointAnnotation(coordinate: location)
                    if self.mapLandingViewModel?.isSearchDisocver == true, let searchLocation = self.mapLandingViewModel?.searchDiscoverLocation {
                        destinationAnnotation = PointAnnotation(coordinate: searchLocation)
                    }
                    destinationAnnotation.image = .init(image: Asset.CarPark.destinationWithoutCarpark.image, name: "routePlan")
                    annotations.append(destinationAnnotation)
                }
                //If clustered and current zoom is lessthan maximum cluster zoom which is always +1
                else if (carParkUpdater.shouldCluster && self.navMapView.mapView.cameraState.zoom < Values.clusterMaxZoom + 1.0){
                    
                    var destinationAnnotation = PointAnnotation(coordinate: location)
                    if self.mapLandingViewModel?.isSearchDisocver == true, let searchLocation = self.mapLandingViewModel?.searchDiscoverLocation {
                        destinationAnnotation = PointAnnotation(coordinate: searchLocation)
                    }
                    destinationAnnotation.image = .init(image: Asset.CarPark.destinationWithoutCarpark.image, name: "routePlan")
                    annotations.append(destinationAnnotation)
                    
                    self.navMapView.pointAnnotationManager?.annotations = [PointAnnotation]()
                    self.navMapView.pointAnnotationManager = self.navMapView.mapView.annotations.makePointAnnotationManager(id: CarparkValues.destinationLayerIdentifier, layerPosition: .above("puck"))
                }
            }
            
            self.navMapView.pointAnnotationManager?.annotations = annotations
        }
        else{
            
            self.navMapView.pointAnnotationManager?.annotations = [PointAnnotation]()
        }
    }
    
}

extension MapLandingVC: CarParkUpdaterDelegate {
    func carParkNavigateHere(carPark: Carpark?) {
        
    }
    
    // Process after carpark loaded
    func didUpdateCarparks(carParks: [Carpark]?, location: CLLocationCoordinate2D) {
        
        let carparks = carParks ?? []
        
        var cheapestCarpark: Carpark?
        
        self.prevCarParks = carParks
        
        var firstOutOfRangeCarpark: Carpark?
        
        var destinationCarpark: Carpark?
        
        // For destination annotation only
        var annotations = [PointAnnotation]()
        
        if let first = carparks.first {
            if (first.distance ?? 0) > Values.carparkMonitorDistanceInLanding {
                firstOutOfRangeCarpark = first
                // only show one carpark
                
                if first.destinationCarPark ?? false  {
                    destinationCarpark = first
                }
                
                if carparkFromBottomSheet{
                    cheapestCarpark = first
                }
            } else {
                // for showing tooltips
                // 1. If destination is there always showing tooltips on it
                // 2. If no destination show the nearest which is the first element
                carparks.forEach { carPark in
                    
                    if carPark.destinationCarPark ?? false  {
                        destinationCarpark = carPark
                    }
                    
                    if carparkFromBottomSheet {
                        if destinationCarpark != nil {
                            self.isDestinationHasCarPark = true
                            cheapestCarpark = destinationCarpark
                        } else {
                            if cheapestCarpark == nil {
                                self.isDestinationHasCarPark = false
                                cheapestCarpark = carPark
                            }
                        }
                    }
                }
            }
        }
        
        if let carparkUpdater = carparkUpdater {
            if !carparkUpdater.shouldIgnoreDestination {
                if destinationCarpark == nil { // add original destination icon
                    var destinationAnnotation = PointAnnotation(coordinate: location)
                    if self.mapLandingViewModel?.isSearchDisocver == true, let searchLocation = self.mapLandingViewModel?.searchDiscoverLocation {
                        destinationAnnotation = PointAnnotation(coordinate: searchLocation)
                    }
                    destinationAnnotation.image = .init(image: Asset.CarPark.destinationWithoutCarpark.image, name: "routePlan")
                    annotations.append(destinationAnnotation)
                    
                } else if (carparkUpdater.shouldCluster && self.navMapView.mapView.cameraState.zoom < Values.clusterMaxZoom){
                    
                    var destinationAnnotation = PointAnnotation(coordinate: location)
                    if self.mapLandingViewModel?.isSearchDisocver == true, let searchLocation = self.mapLandingViewModel?.searchDiscoverLocation {
                        destinationAnnotation = PointAnnotation(coordinate: searchLocation)
                    }
                    destinationAnnotation.image = .init(image: Asset.CarPark.destinationWithoutCarpark.image, name: "routePlan")
                    annotations.append(destinationAnnotation)
                    
                    self.navMapView.pointAnnotationManager?.annotations = [PointAnnotation]()
                    self.navMapView.pointAnnotationManager = self.navMapView.mapView.annotations.makePointAnnotationManager(id: CarparkValues.destinationLayerIdentifier, layerPosition: .above("puck"))
                } else {
                    var destinationAnnotation = PointAnnotation(coordinate: location)
                    destinationAnnotation.image = .init(image: Asset.CarPark.destinationWithCarpark.image, name: "routePlan")
                    self.navMapView.pointAnnotationManager = self.navMapView.mapView.annotations.makePointAnnotationManager(id: CarparkValues.destinationLayerIdentifier, layerPosition: .above("puck"))
                }
            }
        }
        
        self.navMapView.pointAnnotationManager?.annotations = annotations
        
        // Need delay here to add all symbol layer to map then open tooltip
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(300)) { [weak self] in
            guard let self = self else { return }
            if let carpark = cheapestCarpark {
                
                if let viewModel = self.mapLandingViewModel {
                    if viewModel.landingMode == Values.PARKING_MODE {
                        if viewModel.isSearchDisocver, let location = viewModel.searchDiscoverLocation {
                            
                            if let location = self.getActualLocation() {
                                self.sentEventParkingBottomSheetData(idCarPark: carpark.id ?? "", isDestinationHasCarPark: self.isDestinationHasCarPark, focusedLat: location.latitude.toString(), focusedLong: location.longitude.toString())
                            }
                            
                            if carpark.destinationCarPark ?? false {
                                self.openCarparkToolTipView(carpark: carpark, tapped: true, actualLocation: self.getActualLocation())
                            } else {
                                // Open destination tooltip when search location
                                if let carParks = carParks {
                                    self.updateCarparks(carParks, location: location, discoverMode: true)
                                }
                            }
                        } else {
                            self.callRNToShowingCarparkInfo(carpark: carpark, actualLocation: self.getActualLocation())
                        }
                    } else {
                        self.openCarparkToolTipView(carpark: carpark, tapped: true, actualLocation: self.getActualLocation())
                    }
                }
            }
        }
        
        
        if carparks.count > 0 {
//            self.navMapView.mapView.removeCarparkLayer()
            
               // if there is no carpark within carparkMonitorDistanceInLanding (500m), then need to setup zoom level
                if firstOutOfRangeCarpark != nil {
                    // only need to show one carpark
//                    self.navMapView.mapView.addCarparks([first], showOnlyWithCostSymbol: true)
                    DispatchQueue.main.async { [weak self] in
                        guard let self = self else { return }
                        if(self.carparkToast != nil){
                            self.carparkToast?.removeFromSuperview()
                        }
                        
                        if let model = self.mapLandingViewModel {
                            
                            if self.currentGlobalNotification == nil && !model.isEventFromInbox{
                                if model.centerMapLocation == nil , !self.shouldUpdateCarparks(), !model.didShowNearByNotification {
                                    self.mapLandingViewModel?.didShowNearByNotification = true
                                    let toast = BreezeToast(appearance: BreezeToastStyle.carparkToast, onClose: {
                                        self.carparkToast = nil
                                    }, actionTitle: "", onAction: nil)

                                    toast.show(in: self.navMapView, title: "Nearby Carpark", message: Constants.toastNearestCarpark, animated: true)
                                    toast.yOffset = getTopOffset()
                                    
                                    self.carparkToast = toast
                                }
                            }
                        }
                        
                    }
                    
                    //self.setupZoomelevel(coordinate1: location, coordinate2: CLLocationCoordinate2D(latitude: first.lat ?? 0, longitude: first.long ?? 0))
                    if(AmenitiesSharedInstance.shared.amenitiesSymbolLayer.count == 0){
                        
                        if let viewModel = self.mapLandingViewModel{
                            
                            if(viewModel.landingMode != Values.PARKING_MODE){
                                self.adjustZoomForCarParks()
                            }
                        }
                        
                    }
                    self.isTrackingUser = true
                } else {
//                    self.navMapView.mapView.addCarparks(carparks, showOnlyWithCostSymbol: true)
                    
                    if(AmenitiesSharedInstance.shared.amenitiesSymbolLayer.count == 0){
                        
                        if let viewModel = self.mapLandingViewModel{
                            
                            if(viewModel.landingMode != Values.PARKING_MODE){
                                self.adjustZoomForCarParks()
                            }
                        }
                        
                    }
                }
            
            /*
             Get the nearest carpark
             1. Drop pin mode is not enable
             2. Not searching location
             3. User is panning the map should not auto focus on nearest
             */
            if let viewModel = mapLandingViewModel, !viewModel.didShowNearestCarparkAtLaunchTime {
                if viewModel.isOnDropPin == false && !(viewModel.isSearchDisocver && viewModel.searchDiscoverLocation != nil) && !(viewModel.centerMapLocation != nil) {
                    //  Show nearest carpark
                    self.nearestCarPark = self.getNearestCarpark(carparks: carparks)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [weak self] in
                        if let nersestCarPark = self?.nearestCarPark {
                            self?.mapLandingViewModel?.didShowNearestCarparkAtLaunchTime = true
                            self?.openCarparkToolTipView(carpark: nersestCarPark, tapped: true, needShowBottomSheet: true)
                        }
                    }
                }
            }
        }
    }
    
    private func callRNToShowingCarparkInfo(carpark: Carpark, tapped: Bool = false, actualLocation: CLLocationCoordinate2D?){
        let cameraOptions = CameraOptions(center: CLLocationCoordinate2D(latitude: carpark.lat ?? 0, longitude: carpark.long ?? 0), zoom: self.navMapView.mapView.mapboxMap.cameraState.zoom) // using current zoom level
        self.navMapView.mapView.camera.ease(to: cameraOptions, duration: Values.standardAnimationDuration, curve: .easeInOut, completion: nil)
        
        if let location = actualLocation {
            self.sentEventParkingBottomSheetData(idCarPark: carpark.id ?? "", isDestinationHasCarPark: self.isDestinationHasCarPark, focusedLat: location.latitude.toString(), focusedLong: location.longitude.toString())
        }
        
        let coordinate = CLLocationCoordinate2D(latitude: carpark.lat ?? 0, longitude: carpark.long ?? 0)
        
        if let viewModel = self.mapLandingViewModel{
            self.adjustZoomOnlyForAmenity(at: coordinate, callout: nil, viewModel: viewModel)
        }
        self.processRecenterMapview()
    }
}

extension MapLandingVC: MapLandingViewModelDelegate {
    func addedProfile(_ viewModel: MapLandingViewModel, name: String) {
        // #5789
        // If add TBR is on => force on parking
//        if (!isParkingOn) {
//            self.parkingButton.toggle()
//        }
        if parkingAmenitiesMode == .hide {
            parkingAmenities?.parkingViewType = .collapse
            dropPinButton?.isHidden = false
        }
    }
    
    func onTriggerCruiseMode20Km() {
        self.showOBUDisplay(true)
    }
}

extension MapLandingVC {
    
    func shouldUpdateCarparks() -> Bool {
        return needUpdateCarparks
    }
    
    func triggerEndPanGesture() {
        if let viewModel = self.mapLandingViewModel {
            if viewModel.landingMode == Values.NONE_MODE || viewModel.landingMode == Values.PARKING_MODE || viewModel.landingMode == Values.OBU_MODE {
                needUpdateCarparks = false
                if self.parkingMode != .hide {
                    self.mapLandingViewModel?.didShowNearByNotification = true
                    startUpdateCarparkTimer()
                }
            }
        }
    }
    
    func startUpdateCarparkTimer() {
        stopUpdateCarparkTimer()
        self.updateCarparkTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.updateCenterMapCarparks), userInfo: nil, repeats: false)
    }
    
    func stopUpdateCarparkTimer() {
        if updateCarparkTimer?.isValid == true {
            updateCarparkTimer?.invalidate()
            updateCarparkTimer = nil
        }
    }
    
    @objc func updateCenterMapCarparks() {
        guard let viewModel = mapLandingViewModel else { return }
        needUpdateCarparks = true
        showParking(viewModel: viewModel)
    }
}

import UIKit
import Foundation
@_spi(Experimental) import MapboxMaps
import MapboxCoreMaps
import Turf
import SwiftyBeaver
import RxSwift

protocol MockObuEventListener: AnyObject {
    func onReceiveObuEvent(_ event: ObuMockEventModel)
}

protocol GlobalNotificationListener: AnyObject {
    func shouldShowNotification(_ notifications: [NotificationModel])
}

extension MapLandingVC {
    
    static let categoryOrder = ["TRAFFIC_ALERTS","ADMIN"]
    static let typeOrder = ["MAJOR_ACCIDENT","ROAD_CLOSURE","FLASH_FLOOD"]
    
    func getNotifications() {
        let notificationService = NotificationService()
        // MARK: - NOTICE: For testing
//        let notificationService = MockNotificationService()
        
        // NO need to do house keeping anymore since some value = 1 which will cause issue if we do the house keeping
        // TODO: Refactor: house keeping 
//        removeExpiredNotificates()
        
        notificationService.getNotifications() { result in

            switch result {
            case .success(let notifications):
                // Filter out notification has been read
                let filtered = notifications.notifications.filter { !self.isNotificationDisplayed($0.notificationId) }
                let sorted = filtered.sorted { return $0.priority < $1.priority }
                
                if sorted.count == 0 {
                    self.showParking()
                    self.startMonitorAppSyncNotification()
                } else {
                    
                    self.inAppTutorial = false
                    // get the first one
                    let notification = sorted.first!
                    
                    self.showGlobalNotification(notification, totalCount: sorted.count, showOn: self, firstCheck: true)
                    
                    
                    // update display status for all these notifications
                    self.updateDisplayedNotifications(sorted)
                }
                
            case .failure(let error):
                self.showParking()
                self.startMonitorAppSyncNotification()
                SwiftyBeaver.error("Failed to getNotifications: \(error.localizedDescription)")
            }
        }
    }
    
    func startMonitorAppSyncNotification() {
        DataCenter.shared.addGlobalNotificationListner(listner: self)
    }
    
    func showGlobalNotification(_ notification: NotificationModel, totalCount: Int, showOn viewController: UIViewController, firstCheck: Bool) {
        // retrieve image
        if(inAppTutorial) { return }
        if(!notification.showInappNotification) { return }  //  show inapp notification is false then not show it
        
        let url = isDarkMode ? notification.imageLinkDark : notification.imageLinkLight
        Task.init {
            // TODO: If image not downloaded?
            if let image = await ImageLoader().retrieveImage(with: url) {
                DispatchQueue.main.async { [weak self] in
                    guard let self = self else { return }
                    if self.globalNotificationView != nil {
                        self.globalNotificationView?.dismiss(animated: true)
                    }
                    let title = totalCount > 1 ? (totalCount > 2 ? "\(notification.short_name) and \(totalCount - 1) other alerts" : "\(notification.short_name) and \(totalCount - 1) other alert") : notification.title
                    let action: (() -> Void) = (notification.category != "TRAFFIC_ALERTS" || totalCount > 1) ?
                    {
                        self.globalNotificationView?.dismiss(animated: true)
                        
                        if let navVC = viewController as? UINavigationController, !(navVC.topViewController is MapLandingVC) {
                            self.popToMapLandingVC()
                        }else if self.isTripLogOn {
                            let _ = ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.NAVIGATE_TO_HOME_TAB, data: nil).subscribe(onSuccess: { result in
                            }, onFailure: { error in
                            })
                        }
                        
                        //  Handle open screen
                        if let screen = notification.openScreen, let screenProperties = notification.screenProperties, !screen.isEmpty {
                            if screen == Values.exploreMapType {
                                //  ContentID, Content Category ID                                
                                var dataString = ""
                                if let contentId = screenProperties.contentID {
                                    dataString += "\"\(Values.contentIdKey)\" : \(contentId)"
                                }
                                
                                if let categoryId = screenProperties.contentCategoryID {
                                    if !dataString.isEmpty {
                                        dataString += ","
                                    }
                                    dataString += "\"\(Values.contentCategoryIdKey)\" : \(categoryId)"
                                }
                                
                                self.gotoContent(data: dataString.isEmpty ? nil : dataString)
                            }
                        } else {
                            if (notification.category == "TUTORIAL" || notification.category == "CUSTOM_INBOX") && totalCount == 1 {
                                self.showTutorialVideo(notification: notification)
                            } else if notification.category == "CUSTOM_INBOX" && notification.openScreen == Values.voucherType {
                                appDelegate().goToMyVoucherFromNotification()
                            }
                            else {
                                self.readMore(totalCount, notification: notification)
                            }
                        }
                    } :
                    {
                        // see on map
                        
                        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.GlobalNotification.UserClick.see_on_map_global_noti, screenName: ParameterName.GlobalNotification.screen_view)
                        
                        
                        if self.isTripLogOn {
                            let _ = ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.NAVIGATE_TO_HOME_TAB, data: nil).subscribe(onSuccess: { result in
                               }, onFailure: { error in
                            })
                        } else {
                            if let navVC = viewController as? UINavigationController, !(navVC.topViewController is MapLandingVC) {
                                self.popToMapLandingVC()
                            }
                            let _ = ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.OPEN_NOTIFICATION_FROM_NATIVE, data: ["NOTIFICATION_CATEGORY":notification.category]).subscribe(onSuccess: { result in
                               }, onFailure: { error in
                            })
                        }

                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            self.addTrafficNotificationToMap(notification)
                        }
                        self.globalNotificationView?.dismiss(animated: true)
                    }
                    var actionTitle = ((notification.category != "TRAFFIC_ALERTS" || totalCount > 1) ? "Read more" : "See on map")
                    
                    //  Explore map
                    if let screen = notification.openScreen, let _ = notification.screenProperties, !screen.isEmpty {
                        if screen == Values.exploreMapType {
                            actionTitle = "Read more"
                        } else if screen == Values.voucherType {
                            actionTitle = "My Voucher"
                        }
                    }
                    
                    var style = BreezeToastStyle.upcomingTrip
                    if notification.category == "ADMIN" {
                        style = BreezeToastStyle.adminNotification
                    } else if notification.category == "TRAFFIC_ALERTS" {
                        style = BreezeToastStyle.globalNotification
                    } else if notification.category == "CUSTOM_INBOX" && notification.openScreen == Values.voucherType {
                        style = BreezeToastStyle.voucherNotification
                        actionTitle = "My Voucher"
                        
                    }
                    // otherwise use the default style - purple
                    
                    self.globalNotificationView =  GlobalNotificationView(appearance: style)
                    AnalyticsManager.shared.logPopUpEvent(popupName: ParameterName.GlobalNotification.UserPopUp.popupName, eventName: ParameterName.GlobalNotification.UserPopUp.popup_open)
                    
                    self.globalNotificationView?.show(in: viewController.view, title: title, message: notification.message, icon: image, onClose: {
                        
                        AnalyticsManager.shared.logPopUpEvent(popupName: ParameterName.GlobalNotification.UserPopUp.popupName, eventName: ParameterName.GlobalNotification.UserPopUp.popup_close)
                        
                        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.GlobalNotification.UserClick.close_global_noti, screenName: ParameterName.GlobalNotification.screen_view)
                        
                        if self.currentGlobalNotification == nil && firstCheck { // only show show carpark when there is no global notification
                            if(self.globalNotificationView?.isAutoDismiss ?? false)
                            {
                                self.showParking()
                            }
                            
                        }
                        self.globalNotificationView = nil
                        self.startMonitorAppSyncNotification()
                        
                    }, actionTitle: actionTitle, onAction: action, dismissInSeconds: 10, inHtml: false)
                    self.globalNotificationView?.yOffset = getTopOffset()
                }
            }
        }
    }
    
    

    func showParking() {
        if currentGlobalNotification == nil { // don't show parking when the notification added to the map
            // delay it for a while so that the bottomsheet init correctly
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [weak self] in
                guard let self = self else { return }
//                self.parkingButton.select(true) // by default should show parking
                self.inAppTutorial = false
//                self.showNudgeTooltip()
                //TODO: Get default here
                
                if(!self.carparkFromBottomSheet && self.prevCarParks == nil){
                    
                    self.parkingAmenities?.setParkingMode(type: UserSettingsModel.sharedInstance.carParkAmenities)
                }
                
            }
        }
    }
    
    private func popToMapLandingVC() {
        if let controllers = self.navigationController?.viewControllers {
            for controller in controllers {
                if controller.isKind(of: MapLandingVC.self) {
                    self.navigationController?.popToViewController(controller, animated: true)
                    break
                }
            }
        }
    }
    
    func removeTrafficNotificationFromMap() {
        self.navMapView.mapView.removeTrafficNotificationLayer()
        self.currentGlobalNotification = nil
        if let model = mapLandingViewModel{
            model.isEventFromInbox = false
        }
    }

    private func showTutorialVideo(notification:NotificationModel){

        // TODO: Need to change the name of play_tutorial_video_noti later
//        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.GlobalNotification.UserClick.play_tutorial_video_noti, screenName: ParameterName.GlobalNotification.screen_view)
        
        let notificationData = ["NOTIFICATION_ID":notification.notificationId]
        
        let _ = ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.NAVIGATE_TO_INBOX_SCREEN, data: notificationData as NSDictionary).subscribe(onSuccess: {_ in }, onFailure: {_ in })
    }

    func readMore(_ totalCount:Int,notification:NotificationModel){
        
        
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.GlobalNotification.UserClick.read_more_global_noti, screenName: ParameterName.GlobalNotification.screen_view)
        
        let notificationData = ["NOTIFICATION_ID":totalCount > 1 ? -1 : notification.notificationId ]
        
        let _ = ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.NAVIGATE_TO_INBOX_SCREEN, data: notificationData as NSDictionary).subscribe(onSuccess: {_ in }, onFailure: {_ in })
    }
    
    func addTrafficNotificationToMap(_ notification: NotificationModel) {
        
        didDeselectAnnotation()
        didDeselectAmenityAnnotation()

        let _ = ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.READ_INBOX_EVENT, data: ["NOTIFICATION_ID":notification.notificationId] as? NSDictionary).subscribe(onSuccess: {_ in },                 onFailure: {_ in })
            
        // Load image. Since this function maybe called by
        let url = isDarkMode ? notification.imageLinkDark : notification.imageLinkLight
        Task.init { [weak self] in
            guard let self = self else { return }
            if let image = await ImageLoader().retrieveImage(with: url) {
                // generate a feature
                let coordinate = CLLocationCoordinate2D(latitude: notification.latitude, longitude: notification.longitude)
                let feature = Turf.Feature(geometry: .point(Point(coordinate)))
                DispatchQueue.main.async {
                    self.navMapView.mapView.removeTrafficNotificationLayer()
                    self.navMapView.mapView.addTrafficNotificationLayer(image: image, imageId: "\(notification.notificationId)", features: FeatureCollection(features: [feature]), isBelowPuck: true)
                    self.currentGlobalNotification = notification
                    self.navMapView.mapView.setCruiseCamera(at: coordinate)
                    self.isTrackingUser = false
                }
            }
        }
    }
        
}

// MARK: Handle display status
extension MapLandingVC {
    func isNotificationDisplayed(_ id: Int) -> Bool {
        if let displayedNotifications = UserDefaults.standard.object(forKey: Values.DISPLAYED_NOTIFICATIONS_KEY) as? [String: Int] {
            return displayedNotifications.keys.contains("\(id)")
        }
        return false
    }
        
    func updateDisplayedNotifications(_ notifications: [NotificationModel]) {
        var notificationDictionary = [String: Int]()
        if let displayedNotifications = UserDefaults.standard.object(forKey: Values.DISPLAYED_NOTIFICATIONS_KEY) as? [String: Int] {
        
            notificationDictionary = displayedNotifications
        }
        
        notifications.forEach { notificationDictionary["\($0.notificationId)"] = $0.expireTime }
        if !notificationDictionary.isEmpty {
            UserDefaults.standard.set(notificationDictionary, forKey: Values.DISPLAYED_NOTIFICATIONS_KEY)
        }
    }
    
    /// Update the notification as displayed (Used when user click the voucher push notification)
    func updateDisplayedNotifications(_ messageId: String) {
        var notificationDictionary = [String: Int]()
        if let displayedNotifications = UserDefaults.standard.object(forKey: Values.DISPLAYED_NOTIFICATIONS_KEY) as? [String: Int] {
        
            notificationDictionary = displayedNotifications
        }
        
        // Since we're using expire time as the value. So we're using 1 here. We won't check expire time any more.
        notificationDictionary["\(messageId)"] = 1
        UserDefaults.standard.set(notificationDictionary, forKey: Values.DISPLAYED_NOTIFICATIONS_KEY)
    }
    
    func removeExpiredNotificates() {
        if var displayedNotifications = UserDefaults.standard.object(forKey: Values.DISPLAYED_NOTIFICATIONS_KEY) as? [String: Int] {
            displayedNotifications.forEach { item in
                if item.value < Int(Date().timeIntervalSince1970) {
                    displayedNotifications.removeValue(forKey: item.key)
                }
            }
            UserDefaults.standard.set(displayedNotifications, forKey: Values.DISPLAYED_NOTIFICATIONS_KEY)
        }
    }
}

extension MapLandingVC: GlobalNotificationListener {
    
    func shouldShowNotification(_ notifications: [NotificationModel]) {
        guard let topVC = self.navigationController?.topViewController else { return }
                
        // Call RN function
        let _ = ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.RECEIVE_NEW_NOTIFICATIONS, data: nil).subscribe(onSuccess: { result in
           }, onFailure: { error in
        })
        
        if topVC is CruiseModeVC {
            self.pendingGlobalNotifications = notifications
            return
        } else if topVC is NewRoutePlanningVC {
            if topVC.presentedViewController is TurnByTurnNavigationVC {
                self.pendingGlobalNotifications = notifications
                return
            }
        }
        
        guard let navVC = self.navigationController else { return }
        checkAndShowGlobalNotification(notifications, showOn: navVC)
    }
    
    func checkAndShowGlobalNotification(_ notifications: [NotificationModel], showOn viewController: UIViewController) {
        
        let filtered = notifications.filter { !self.isNotificationDisplayed($0.notificationId) }
        
        let sorted = filtered.sorted { return $0.priority < $1.priority }
        
        if sorted.count > 0 {
            // get the first one
            let notification = sorted.first!
            self.showGlobalNotification(notification, totalCount: sorted.count, showOn: viewController, firstCheck: false)
            
            // update display status for all these notifications
            self.updateDisplayedNotifications(sorted)
        }
    }
}

extension MapView {

    func removeTrafficNotificationLayer() {
        do {
            try self.mapboxMap.style.removeLayer(withId: "TrafficNotificationSymbol")
            try self.mapboxMap.style.removeSource(withId: "TrafficNotificationSource")
        } catch {
            SwiftyBeaver.error("Failed to remove removeTrafficNotificationLayer.")
        }
    }
    
    func addTrafficNotificationLayer(image: UIImage, imageId: String, features:Turf.FeatureCollection, isBelowPuck: Bool) {
        
        do {
            try self.mapboxMap.style.addImage(image, id: imageId, stretchX: [], stretchY: [])
            
            // Create a GeoJSON data source.
            var geoJSONSource = GeoJSONSource()
            geoJSONSource.data = .featureCollection(features)
                        
            let iconSize = Exp(.interpolate){
                Exp(.linear)
                Exp(.zoom)
                0
                0
                9
                0.1
                13
                0.20
                22
                0.30
            }
            
            var symbolLayer = SymbolLayer(id: "TrafficNotificationSymbol")
            symbolLayer.source = "TrafficNotificationSource"
            symbolLayer.minZoom = 9.0
            symbolLayer.iconSize = .expression(iconSize)
            symbolLayer.iconImage = .constant(.name(imageId))
            symbolLayer.iconAllowOverlap = .constant(true)
            symbolLayer.iconIgnorePlacement = .constant(true)
            // Add the source and style layers to the map style.
            try self.mapboxMap.style.addSource(geoJSONSource, id: "TrafficNotificationSource")

            try self.mapboxMap.style.addPersistentLayer(with: symbolLayer.jsonObject(), layerPosition: isBelowPuck ? .below("puck") : .above(Constants.Layer.illegalparkingcamera))
            SwiftyBeaver.debug("addTrafficNotificationLayer \(features.features.count)")
        }
        catch (let error) {
            SwiftyBeaver.error("Failed to addTrafficNotificationLayer: \(error.localizedDescription)")
        }
    }
}

//
//  MapLandingVC+Walking.swift
//  Breeze
//
//  Created by Zhou Hao on 13/5/22.
//

import UIKit
import CoreLocation
import MapboxDirections
import Turf
import MapboxCoreNavigation
import MapboxMaps
import MapboxCoreMaps
import MapboxNavigation

extension MapLandingVC {
    
    // already have the route, from navigation screen
    func startWalking(response: RouteResponse, screenshot: UIImage?, trackNavigation: Bool = false, walkathonId: Int? = nil, amenityId: String? = nil, layerCode: String? = nil, checkpointName: String? = nil, canReroute: Bool? = true, amenityType: String? = nil, plannedDestAddress1: String? = nil, plannedDestAddress2: String? = nil, plannedDestLat: Double = 0, plannedDestLong: Double = 0) {
        self.walkingResponse = response
        startWalkingNavigation(response: response, trackNavigation: trackNavigation, walkathonId: walkathonId, amenityId: amenityId, layerCode: layerCode, checkpointName: checkpointName, canReroute: canReroute, amenityType:amenityType, plannedDestAddress1: plannedDestAddress1, plannedDestAddress2: plannedDestAddress2, plannedDestLat: plannedDestLat, plannedDestLong: plannedDestLong)
    }
    
    func startWalking(poi: POI) {
        walkingResponse = nil
//        calculateRoute(poi: poi)
        
        // 1. current location
        let currentLocation = LocationManager.shared.location
        // 2. destination
        let destinationCoordinate = LocationCoordinate2D(latitude: poi.lat, longitude: poi.long)

        requestWalkingRoute(location1: currentLocation.coordinate, location2: destinationCoordinate, name: poi.name) { [weak self] response in
            guard let self = self, let response = response else { return }
            self.walkingResponse = response
            self.startWalkingNavigation(response: response)
        }
    }
/*
    private func calculateRoute(poi: POI) {
                
        var waypoints = [Waypoint]()
        // 1. current location
        let currentLocation = LocationManager.shared.location

        let userWaypoint = Waypoint(location: currentLocation)
        if currentLocation.course >= 0 {
            userWaypoint.heading = currentLocation.course
            userWaypoint.headingAccuracy = 90
        }
        waypoints.append(userWaypoint)
        
        // 2. destination
        let destinationCoordinate = LocationCoordinate2D(latitude: poi.lat, longitude: poi.long)
        let waypoint = Waypoint(coordinate: destinationCoordinate, name: "\(poi.name)")
        waypoint.targetCoordinate = destinationCoordinate
        waypoints.append(waypoint)

        let navigationRouteOptions = NavigationRouteOptions(waypoints: waypoints, profileIdentifier: .walking)
        navigationRouteOptions.shapeFormat = .polyline6

//        self.navMapView.trafficLowColor = .gray
//        self.navMapView.trafficHeavyColor = .orange
//        self.navMapView.trafficSevereColor = .red
//        self.navMapView.trafficUnknownColor = .lightGray
//        self.navMapView.traversedRouteColor = .green
        
        // Get periodic updates regarding changes in estimated arrival time and traffic congestion segments along the route line.
        RouteControllerProactiveReroutingInterval = 30

        requestRoute(with: navigationRouteOptions, success: {
            [weak self] response in
            guard let self = self else { return }
            self.walkingResponse = response
            self.startWalkingNavigation()
            
        }, failure: {
            [weak self] _ in
            guard let self = self else { return }
            self.walkingResponse = nil
        })
    }
*/
    private func startWalkingNavigation(response: RouteResponse, trackNavigation: Bool = false, walkathonId: Int? = nil, amenityId: String? = nil, layerCode: String? = nil, checkpointName: String? = nil, canReroute: Bool? = true, amenityType: String? = nil, plannedDestAddress1: String? = nil, plannedDestAddress2: String? = nil, plannedDestLat: Double = 0, plannedDestLong: Double = 0) {
        
        if trackNavigation {
            Prefs.shared.setValue(walkathonId, forkey: .walkathonContentId)
        }
        // method 1: Using Mapbox NavigationViewController
        guard case let .route(routeOptions) = response.options else { return }
        
        // always use the first route
        let service = navigationService(response: response, routeIndex: 0, options: routeOptions)
        let options = NavigationOptions(styles: [CustomDayStyle()], navigationService: service, predictiveCacheOptions: PredictiveCacheOptions())
        
        let progressVC = UIStoryboard(name: "Navigation", bundle: nil).instantiateViewController(withIdentifier: String(describing: NavProgressVC.self)) as! NavProgressVC
        progressVC.isNavigation = false
        let endWalkathonVC = UIStoryboard(name: "Navigation", bundle: nil).instantiateViewController(withIdentifier: String(describing: EndWalkathonVC.self)) as! EndWalkathonVC
        let topBannerVC = WalkingNavTopVC()
        options.topBanner = topBannerVC
        if canReroute == false {
            topBannerVC.view.isHidden = true
            options.bottomBanner = endWalkathonVC
        } else {
            options.bottomBanner = progressVC
        }

        let navigationViewController = WalkingNavigationVC(for: response, routeIndex: 0, routeOptions: routeOptions, navigationOptions: options)
        navigationViewController.routeLineTracksTraversal = true
//        navigationViewController.endRouteDelegate = self
        if canReroute == false {
            endWalkathonVC.delegate = navigationViewController
        } else {
            progressVC.delegate = navigationViewController
        }
        navigationViewController.topVC = topBannerVC
        navigationViewController.showsEndOfRouteFeedback = false
        navigationViewController.trackNavigation = trackNavigation
        navigationViewController.walkathonId = walkathonId
        
        navigationViewController.amenityId = amenityId
        navigationViewController.layerCode = layerCode
        navigationViewController.amenityType = amenityType
        navigationViewController.plannedDestAddress1 = plannedDestAddress1
        navigationViewController.plannedDestAddress2 = plannedDestAddress2
        navigationViewController.plannedDestLat = plannedDestLat
        navigationViewController.plannedDestLong = plannedDestLong
        
        navigationViewController.checkpointName = checkpointName
        navigationViewController.canReroute = canReroute ?? true
        
        // Render part of the route that has been traversed with full transparency, to give the illusion of a disappearing route.
        navigationViewController.routeLineTracksTraversal = true
        navigationViewController.waypointStyle = .building
        navigationViewController.modalPresentationStyle = .fullScreen
        
        let puck2DConfiguration = Puck2DConfiguration(topImage: UIImage(named: "puckArrow",in:nil,compatibleWith: traitCollection), bearingImage: UIImage(named: "puckIcon"), shadowImage: nil, scale: nil)

        let userLocationStyle = UserLocationStyle.puck2D(configuration: puck2DConfiguration)
        navigationViewController.navigationMapView?.userLocationStyle = userLocationStyle
        
        self.navigationController?.present(navigationViewController, animated: true) {
//            if let destinationCoordinate = route.shape?.coordinates.last {
//                var destinationAnnotation = PointAnnotation(coordinate: destinationCoordinate)
//                let markerImage = UIImage(named: "destination")!
//                destinationAnnotation.image = .init(image: markerImage, name: "marker")
//                navigationViewController.destinationAnnotation = destinationAnnotation
//            }
        }
    }
    
    private func navigationService(response: RouteResponse, routeIndex: Int, options: RouteOptions) -> NavigationService {
        var mode: SimulationMode = .inTunnels
#if targetEnvironment(simulator)
        mode = .always
#endif
        
        return MapboxNavigationService(routeResponse: response, routeIndex: 0, routeOptions: options, routingProvider: MapboxRoutingProvider(.hybrid), credentials: NavigationSettings.shared.directions.credentials,simulating: mode)
    }

}

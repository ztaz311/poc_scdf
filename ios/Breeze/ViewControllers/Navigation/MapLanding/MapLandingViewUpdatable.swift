//
//  MapLandingViewUpdatable.swift
//  Breeze
//
//  Created by Zhou Hao on 30/9/21.
//

import Foundation
import Turf
import MapboxNavigation
@_spi(Restricted) import MapboxMaps
import SnapKit
import SwiftyBeaver
import MapboxDirections
import Instabug

final class MapLandingViewUpdatable: MapViewUpdatable {
    
    weak var navigationMapView: NavigationMapView?
    weak var delegate: MapViewUpdatableDelegate?

    // MARK: - Constants
    private let topPadding: CGFloat = 48
    
    // MARK: - Private Methods
    private var speedLimitView: CustomSpeedLimitView?
    private var muteButton: ToggleButton?
    private var roadNamePanel: RoadNamePanel?
    private var liveLocationButton: LiveLocationSharingButton! //Set it up after mute button
    private var isTrackingUser = true
    private var lastLocation: CLLocation?
    private var cruiseNotificationVC: CruiseNotificationVC?
        
    init(navigationMapView: NavigationMapView) {
        self.navigationMapView = navigationMapView
        self.navigationMapView?.mapView.ornaments.options.attributionButton.visibility = .hidden
//        setInitialMapCamera()
        
        // always hide camera icon in map
        updateCameraLayer(show: false)
    }
    
    deinit {
        SwiftyBeaver.debug("MapLandingViewUpdatable deinit")
    }

    func cruiseStatusUpdate(isCruise: Bool) {
        setCamera(at: navigationMapView?.mapView.location.latestLocation?.coordinate, isCruise: isCruise)
        if isCruise {
            cruiseStarted()
        } else {
            cruiseStopped()
        }
    }

    func updateRoadName(_ name: String) {
        self.showRoad(name)
    }
    
    func updateSpeedLimit(_ value: Double) {
        speedLimitView?.speed = value
    }
    
    func overSpeed(value: Bool) {
        
        //We don't need to use this method
    }

    // MARK: - Finding ERP's on route line
    func initERPUpdater(response:RouteResponse?){
        //Not required
    }
        
    // MARK: - Private Methods
    private func cruiseStarted() {
        setupSpeedLimitView()
        setupMuteButton()
        setupRoadNamePanel()
        setupLiveLocationButton()
        
        if let mapView = navigationMapView?.mapView {
            let topOffset: CGFloat = UIApplication.bottomSafeAreaHeight > 0 ? 0 : 15
            showToast(from: mapView, message: Constants.toastStartCruise, topOffset: topOffset)
        }
        
//        updateCameraLayer(show: true)
    }
    
    private func cruiseStopped() {
        removeSpeedLimitView()
        removeMuteButton()
        removeRoadNamePanel()
        removeLiveLocationButton()
        removeCruiseNotification()
        
        if let mapView = navigationMapView?.mapView {
            hideToast(from: mapView)
        }
        if let navMapView = navigationMapView {
            hideToast(from: navMapView)
        }
    }

    // MARK: - Speed limit view
    private func setupSpeedLimitView() {
        if speedLimitView == nil {
            speedLimitView = CustomSpeedLimitView(frame: CGRect(x: 0, y: 0, width: 70, height: 70))
            
            navigationMapView?.mapView.addSubview(speedLimitView!)
            speedLimitView?.speed = 0
            
            speedLimitView?.translatesAutoresizingMaskIntoConstraints = false
            if let navMapView = self.navigationMapView {
                speedLimitView?.snp.makeConstraints { (make) in
                    make.leading.equalToSuperview().offset(24)
                    make.top.equalTo(navMapView.snp.top).offset(topPadding)
                    make.size.equalTo(CGSize(width: 70, height: 70))
                }
            }
        }
    }
    
    private func removeSpeedLimitView() {
        guard let view = speedLimitView else {
            return
        }
        view.removeFromSuperview()
        speedLimitView = nil
    }
    
    private func setupMuteButton() {
        if self.muteButton == nil {
            let muteButton = ToggleButton(frame: CGRect(x: 0, y: 0, width: 48, height: 48))
            muteButton.isSelected = !Prefs.shared.getBoolValue(.enableAudio)
            muteButton.onImage = Images.unmuteImage
            muteButton.offImage = Images.muteImage
            self.delegate?.onMuteEnabled(muteButton.isSelected)
            
            muteButton.onToggled = { [weak self] isSelected in
                guard let self = self else { return }
                self.delegate?.onMuteEnabled(isSelected)
            }
            navigationMapView?.mapView.addSubview(muteButton)
            self.muteButton = muteButton
            
            self.muteButton?.translatesAutoresizingMaskIntoConstraints = false
            if let navMapView = self.navigationMapView {
                self.muteButton?.snp.makeConstraints { make in
                    make.trailing.equalToSuperview().offset(-24)
                    make.top.equalTo(navMapView.snp.top).offset(topPadding)
                    make.size.equalTo(CGSize(width: 48, height: 48))
                }
            }
        }
    }
    
    private func removeMuteButton() {
        guard let view = muteButton else {
            return
        }
        view.removeFromSuperview()
        muteButton = nil
    }

    private func setupRoadNamePanel() {
        
        if self.roadNamePanel == nil {
            roadNamePanel = RoadNamePanel(frame: CGRect(x: 0, y: 0, width: 170, height: 40))
            navigationMapView?.addSubview(roadNamePanel!)
            navigationMapView?.bringSubviewToFront(roadNamePanel!)
            
            let minW = UIScreen.main.bounds.width > 400 ? 220 : 190
            roadNamePanel?.translatesAutoresizingMaskIntoConstraints = false
            if let navMapView = self.navigationMapView {
                roadNamePanel?.snp.makeConstraints { (make) in
                    make.centerX.equalToSuperview()
                    make.bottom.equalTo(navMapView.snp.bottom).offset(-90)
                    make.width.greaterThanOrEqualTo(170)
                    make.width.lessThanOrEqualTo(minW)
                    make.height.equalTo(40)
                }
            }
            
            updateRoadPanelConstraint(isHidden: true)
            roadNamePanel?.setNeedsLayout()
            roadNamePanel?.layoutIfNeeded()
        }
    }
    
    private func removeRoadNamePanel() {
        if let view = roadNamePanel {
            view.removeFromSuperview()
            roadNamePanel = nil
        }
    }
    
    // MARK: - Live location button
    private func setupLiveLocationButton() {
    
        if self.liveLocationButton == nil {
            let etaButton = LiveLocationSharingButton(frame: CGRect(x: 0, y: 0, width: 62, height: 62))
            etaButton.onImage = Images.liveLocationResumeImage
            etaButton.offImage = Images.liveLocationPauseImage
            //etaButton.backgroundColor = .white
            etaButton.layer.cornerRadius = 31
            var isFirstTime: Bool = true
            etaButton.onToggled = { [weak self] isStarted in
                guard let self = self else { return }
            
                let name = isStarted ? ParameterName.Cruise.UserClick.eta_main_button: ParameterName.Cruise.UserClick.eta_pause
                AnalyticsManager.shared.logClickEvent(eventValue: name, screenName: ParameterName.Cruise.screen_view)
                
                // Don't show toast the first time
                if isFirstTime {
                    isFirstTime = false
                } else {
                    self.delegate?.onLiveLocationEnabled(isStarted)
                }
            }
                        
            navigationMapView?.mapView.addSubview(etaButton)
            self.liveLocationButton = etaButton

            liveLocationButton.translatesAutoresizingMaskIntoConstraints = false
            liveLocationButton.snp.makeConstraints { make in
                make.trailing.equalTo(muteButton!.snp.trailing).offset(7)
                make.top.equalTo(muteButton!.snp.bottom).offset(10)
                make.width.equalTo(62)
                make.height.equalTo(62)
            }
            
            //Check initially if ETA is not available, then hide the live location button
            self.liveLocationButton.isHidden = true
        }
    }
    
    private func removeLiveLocationButton() {
        guard let view = liveLocationButton else {
            return
        }
        view.removeFromSuperview()
        liveLocationButton = nil
    }
    
    private func updateRoadPanelConstraint(isHidden: Bool) {
        if let navMapView = self.navigationMapView {
            roadNamePanel?.snp.updateConstraints { (make) in
                make.bottom.equalTo(navMapView.snp.bottom).offset(isHidden ? 0 : -90)
            }
        }
    }
    
    private func showRoad(_ name: String) {
                
        guard let roadNamePanel = self.roadNamePanel else { return }
        
        let hide = name.isEmpty
        let needUpdate = roadNamePanel.text() != name
        
        roadNamePanel.set(name)
        
        // animation
        if needUpdate {
            UIView.animate(withDuration: Values.standardAnimationDuration) {
                [weak self] in
                guard let self = self else { return }
                self.updateRoadPanelConstraint(isHidden: hide)
            }
        }
    }
    
    func updateParkingAvailability(carpark: Carpark?) {
        
        //Not required
    }
    
    func updateNotificationWhenUserSoonArrive(carpark: Carpark?, message: String) {
        
        //Not required
    }
        
    // MARK: - Notification
    // featureId - Could be current feature id if currentFeature is not nil
    // or previous featureId if currentFeature is nil
    func updateNotification(featureId: String?, currentFeature: FeatureDetectable?) {
        
        if cruiseNotificationVC == nil {
            setupCruiseNotification()
        }
        
        if let vc = self.cruiseNotificationVC {

//            vc.update(currentFeature)
//
//            if currentFeature == nil && featureId != nil {
//                navigationMapView.mapView.removeSymbol(id: featureId!)
//            }

            if let feature = currentFeature {
                vc.showFeature(feature)
            }
            updateIconsLayout(withNotification: currentFeature != nil)
        }
    }
    
    // MARK: - Tunnel
    func updateTunnel(isInTunnel: Bool) {
        if isInTunnel {            
            DispatchQueue.main.async { [weak self] in
                if let navMapView = self?.navigationMapView {
                    let topOffset: CGFloat = UIApplication.bottomSafeAreaHeight > 0 ? 60 : 75
                    showToast(from: navMapView, message: Constants.toastInTunnel, topOffset: topOffset, icon: "noInternetIcon", duration: 0)
                }
            }
        } else {
            DispatchQueue.main.async { [weak self] in
                if let navMapView = self?.navigationMapView {
                    hideToast(from: navMapView)
                }
            }
        }
    }
    
    // MARK: - ETA
    func updateETA(eta: TripCruiseETA?) {
        if let tripETA = eta {
            
            if(self.liveLocationButton != nil){
                
                if tripETA.shareLiveLocation.uppercased() == "Y" {
                    self.liveLocationButton.isHidden = false
                    self.liveLocationButton.resume()
                } else {
                    self.liveLocationButton.pause()
                    self.liveLocationButton.isHidden = true
                }
            }
                        

        } else {
            if(self.liveLocationButton != nil){
             self.liveLocationButton.pause()
             self.liveLocationButton.isHidden = true
            }
        }
    }
    
    func updateETA(eta: TripETA?) {
        
        //Not required
    }
    
    func shareDriveButtonEnabled(_ isEnabled: Bool) {
        //Not required
    }
    
    func updateLiveLocationEnabled(_ isEnabled: Bool) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            var offset:CGFloat = 0
            if self.cruiseNotificationVC != nil && self.cruiseNotificationVC!.isHidden() {
                offset =  self.topPadding + 95
            } else {
                offset = UIApplication.bottomSafeAreaHeight > 0 ? 0 : 15
            }
             
            if let mapView = self.navigationMapView?.mapView {
                if isEnabled {
                    showToast(from: mapView, message: Constants.toastLiveLocationResumed, topOffset: offset, icon: "toastLiveLocationResumed", messageColor: .black, duration: 3)
                } else {
                    showToast(from: mapView, message: Constants.toastLiveLocationPaused, topOffset: offset, icon: "toastLiveLocationPaused", messageColor: .black, duration: 3)
                }
            }
            
            self.liveLocationButton.updateToggleWithOutCallBack(status: isEnabled)
            if(appDelegate().carPlayMapController != nil)
            {
                appDelegate().carPlayMapController?.updateCPCruiseToggleTitle(activity: .navigating,isEnabled:isEnabled,isLiveLoc: true)
            }
        }
    }
    
    func updateMuteStatus(isEnabled:Bool){
        
        self.muteButton!.toggleWithOutCallBack(status: !isEnabled)
        if(appDelegate().carPlayMapController != nil)
        {
            appDelegate().carPlayMapController?.updateCPCruiseToggleTitle(activity: .navigating,isEnabled:isEnabled)
        }
    }
    
    func showETAMessage(recipient: String, message: String) {

        var  offset:CGFloat = 0
        if self.cruiseNotificationVC != nil && !self.cruiseNotificationVC!.isHidden() {
            offset =  self.topPadding + 95
        }
        else {
            offset = UIApplication.bottomSafeAreaHeight > 0 ? 0 : 15
        }
        
        if let mapView = self.navigationMapView?.mapView {
            let imageWithText = UIImage.generateImageWithText(text: getAbbreviation(name: recipient), font: UIFont(name: fontFamilySFPro.Bold, size: 20.0)!)
            showToast(from: mapView, message: message, topOffset: offset, image: imageWithText, messageColor: .black, font: UIFont(name: fontFamilySFPro.Regular, size: 20.0)!, duration: 5)
        }
    }
    
    // MARK: - Private methods
    private func updateIconsLayout(withNotification: Bool) {
        if let navMapView = self.navigationMapView {
            let top = withNotification ? topPadding + 128 + 16: topPadding
            if let muteButton = self.muteButton {
                muteButton.snp.updateConstraints { make in
                    make.top.equalTo(navMapView).offset(top)
                }
            }
            if let speedLimitIcon = self.speedLimitView {
                speedLimitIcon.snp.updateConstraints { make in
                    make.top.equalTo(navMapView).offset(top)
                }
            }
        }
    }
    
    // MARK: Notification
    private func setupCruiseNotification() {
        let storyboard = UIStoryboard(name: "MapsLandingVC", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: String(describing: CruiseNotificationVC.self)) as! CruiseNotificationVC
        if let parentVC = navigationMapView?.getViewController() {
            parentVC.addChildViewControllerWithView(childViewController: vc as UIViewController,toView: parentVC.view)
        }
        cruiseNotificationVC = vc
        
        cruiseNotificationVC?.onFeatureUpdated = { [weak self] (feature) in
            guard let self = self else { return }
            if let feature = feature {
                // 1. Update feature
                self.showFeature(feature)
                
                // 2. only speak when there is new feature
                // TO BE IMPLEMENTED
//                self.voiceInstruction(for: feature)
            }
        }
    }
    
    private func removeCruiseNotification() {
        if let vc = cruiseNotificationVC {
//            vc.update(nil)
            vc.hide()
            if let parentVC = navigationMapView?.getViewController() {
                parentVC.removeChildViewController(vc)
            }
            cruiseNotificationVC = nil
        }
    }
    
    
    
    // MARK: Handler state action on OBU from CarPlay
     func appearInstabug() {
        DispatchQueue.main.async {
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.settings_feedback, screenName: ParameterName.settings_screen)
            Instabug.show()
        }
    }
    
    func tryConnectedOBU() {
        DispatchQueue.main.async {
            
            if let mapVC = appDelegate().getMapLandingVCInTop() {
                if !OBUHelper.shared.isOnBluetooth() {
                    mapVC.showBluetoothAlert()
                }else if !OBUHelper.shared.isBluetoothPermissionGranted() {
                    mapVC.showPermissionAlert()
                } else {
                    SwiftyBeaver.debug("OBU - tryConnectedOBU autoConnectOBU")
                    OBUHelper.shared.autoConnectOBU { (success, error) in
                        if success {
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) { [weak self] in
                                if let mapView = self?.navigationMapView?.mapView {
                                    showToast(from: mapView, message: "OBU Connected", topOffset: 5, fromCarPlay: false)
                                }
                            }
                            if appDelegate().carPlayConnected {
                                appDelegate().obuConnected()
                            }
                        } else {
                            
                            if let err = error {
                                SwiftyBeaver.debug("OBU - tryConnectedOBU error: \(err.code) des:\(err.localizedDescription)")
                                
                                let message = "err:\(err.code) des:\(err.localizedDescription)"
                                AnalyticsManager.shared.logSystemEvent(eventValue: ParameterName.SystemAnalytics.SystemEvent.obu_verbose_error, screenName: ParameterName.SystemAnalytics.screen_view, eventMessage: message)
                            }
                            
                            if appDelegate().carPlayConnected {
                                appDelegate().showOBUReportIssue()
                            }
                        }
                        
                    }
                }
            }
        }
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CarPlayObuState.UserClick.dhu_popup_obu_not_connected_retry, screenName: ParameterName.CarPlayObuState.dhu_homepage)
    }
}

//
//  TipsCalloutView.swift
//  Breeze
//
//  Created by Zhou Hao on 12/8/21.
//

import UIKit
import SnapKit

final class TipsCalloutView: ToolTipsView {
    
    // MARK: - Constants
    private let xPadding: CGFloat = 25
    private let yPadding: CGFloat = 23
    private let gap: CGFloat = 20 // gap between labels and button
    private let buttonWidth: CGFloat = 170.0
    private let buttonHeight: CGFloat = 24.0
//    private let bgColor = UIColor(named: "tipsCalloutBGColor")!
    private var popupWidth: CGFloat = 366
    
    private lazy var lblText: UILabel  =  {
        let label = UILabel()
        label.font = UIFont.tipsTextFont()
        label.preferredMaxLayoutWidth = popupWidth
        label.textAlignment = .left
        label.text = "Don’t need navigation? Cruise freely and still receive traffic alerts and notifications of ERP!"
        label.numberOfLines = 3
        label.textColor = .white
        containerView.addSubview(label)
        return label
    }()

    private lazy var btnOK: UIButton = {
        let button = UIButton()
        button.setTitle("Ok,got it!", for: .normal)
        button.setTitleColor(UIColor.rgba(r: 177, g: 85, b: 248, a: 1), for: .normal)
        button.titleLabel?.font = UIFont.tipsTextFont()
        button.addTarget(self, action: #selector(onOKClicked), for: .touchUpInside)
        containerView.addSubview(button)
        return button
    }()
    
    init(preferredWidth: CGFloat) {
        super.init(frame: .zero)
        self.backgroundColor = .clear
        self.bgColor = UIColor(named: "tipsCalloutBGColor")!
        self.tipHeight = 12.0
        self.tipWidth = 20.0

        self.cornerRadius = 16.0
        self.popupWidth = preferredWidth
        self.showShadow = false
//        setupLayout()
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
        
    @objc private func onOKClicked() {
        UserDefaults.standard.set("true", forKey: Values.tipsViewed)
        dismiss(animated: true)
    }
    
    override func sizeThatFits(_ size: CGSize) -> CGSize {
                
        let textSize = lblText.intrinsicContentSize
        let h = yPadding + textSize.height + gap + buttonHeight + yPadding + tipHeight + gap
        
        return CGSize(width: popupWidth, height: h)
    }
        
    internal override func setupLayout() {
        lblText.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(yPadding)
            make.leading.equalToSuperview().offset(xPadding)
            make.trailing.equalToSuperview().offset(-xPadding)
        }
        
        btnOK.snp.makeConstraints { make in
            make.bottom.equalToSuperview().offset(-yPadding)
            make.top.equalTo(lblText.snp.bottom).offset(gap)
            make.centerX.equalTo(containerView.snp.centerX)
            make.width.equalTo(buttonWidth)
            make.height.equalTo(buttonHeight)
        }
        
        containerView.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.bottom.equalToSuperview().offset(-tipHeight)
            make.leading.equalToSuperview()
            make.width.equalTo(popupWidth)
            make.trailing.equalToSuperview()
        }
    }
}

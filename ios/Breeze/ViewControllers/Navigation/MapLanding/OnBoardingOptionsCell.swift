//
//  OnBoardingOptionsCell.swift
//  Breeze
//
//  Created by Zhou Hao on 24/11/21.
//

import UIKit

final class OnBoardingOptionsCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    var option: OnboardingOption!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        clipsToBounds = true
        layer.borderWidth = 1
        layer.cornerRadius = 14
        
    }

}

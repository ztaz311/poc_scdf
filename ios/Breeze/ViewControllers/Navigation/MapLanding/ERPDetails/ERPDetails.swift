//
//  ERPDetails.swift
//  Breeze
//
//  Created by VishnuKanth on 12/01/22.
//

import UIKit
import MapboxNavigation
import MapboxDirections
import MapboxCoreNavigation
@_spi(Restricted) import MapboxMaps
import SnapKit

class ERPDetails: BaseViewController {
    
    static let viewErpDetailBottomTag = 1001
    
    @IBOutlet weak var naviMapView: NavigationMapView!
    @IBOutlet weak var topView: UIView!
    var erpDetailsBottomView: UIView!
    var selectedRoute:Route?
    var selectedRouteResponse:RouteResponse?
    var selectedERPId = -1
    var selectedRouteIndex = 0
    var selectedERPTime = 0
    var isFirstTime = false
    var erpFeatureCollection = FeatureCollection(features: [])
    var erpCalloutView:ERPToolTipsView?
    var erpType: String?
    var selectedTimeErp: String?
    var selectedTimeChanged: Bool?
    
    
    override var useDynamicTheme: Bool {
        return true
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        naviMapView.delegate = self
        toggleDarkLightMode()
        isFirstTime = true
        naviMapView.mapView.ornaments.options.scaleBar.visibility = .hidden
        naviMapView.mapView.ornaments.options.compass.visibility = .hidden
        naviMapView.mapView.ornaments.options.attributionButton.visibility = .hidden
        // disable pitch gesture
        naviMapView.mapView.gestures.options.pitchEnabled = false

        self.setPuckAndAddERPDetailsRNScreen()
        naviMapView.mapView.mapboxMap.onEvery(event: .styleLoaded) { [weak self] _ in
            guard let self = self else { return }

            /// setup route for selected route in ERP bottomsheet
            if let selectedRoute = self.selectedRoute {
                self.naviMapView.setupRouteColor(isDarkMode: self.isDarkMode)
                self.naviMapView.show([selectedRoute])
                self.naviMapView.showWaypoints(on: selectedRoute)
            }
            self.isFirstTime = true
            self.didDeselectAnnotation()
            self.setCamera()
        }
        
        naviMapView.mapView.mapboxMap.onEvery(event: .cameraChanged) { [weak self] _ in
            guard let self = self else { return }
            print(self.naviMapView.mapView.cameraState.zoom)
            if let calloutView = self.erpCalloutView {
                calloutView.adjust(to: calloutView.tooltipCoordinate, in: self.naviMapView)
            }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(goToRoutePlanning), name: Notification.Name(Values.NotificationOpenRPFromERPDetails), object: nil)

        // Do any additional setup after loading the view.
    }
    
    @objc
    func goToRoutePlanning(_ notification:NSNotification){
        
        DispatchQueue.main.async {
            
            if let data = DirectionService.shared.erpFullRouteData {
                
                let destinationAddress = SearchAddresses(alias: "", address1: data.destAddress1, lat:data.destLat.toString() , long: data.destLong.toString(), address2: data.destAddress2, distance: "\(0)")
                
                let currentAddress = SearchAddresses(alias: "", address1: data.srcAddress1, lat:data.srcLat.toString() , long: data.srcLong.toString(), address2: data.srcAddress2, distance: "\(0)")
                
                let storybard = UIStoryboard(name: "NewRoutePlanning", bundle: nil)
                if let vc = storybard.instantiateViewController(withIdentifier: String(describing: NewRoutePlanningVC.self)) as? NewRoutePlanningVC {
                    vc.originalAddress = destinationAddress
                    vc.amenityType = self.erpType
                    vc.isPlanTripFromERPDetails = true
                    vc.routeIndexFromERPDetails = self.selectedRouteIndex
                    vc.currentAddressFromERPDetails = currentAddress
                    vc.addressReceived = true
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
        
    }
    
    func setPuckAndAddERPDetailsRNScreen(){
        
        var traitCollection = UITraitCollection(userInterfaceStyle: .light)
        if(Settings.shared.theme == 0){
            
            if(appDelegate().isDarkMode()){
                
                traitCollection = UITraitCollection(userInterfaceStyle: .dark)
                
            }
            
        }
        else{
            
            traitCollection = UITraitCollection(userInterfaceStyle: UIUserInterfaceStyle(rawValue: Settings.shared.theme) ?? .unspecified)
        }
        
        let puck2d = Puck2DConfiguration(topImage: UIImage(named: "puckArrow",in:nil,compatibleWith: traitCollection), bearingImage: UIImage(named: "puckIcon"), shadowImage: nil, scale: nil)
        
        naviMapView.userLocationStyle = .puck2D(configuration: puck2d)
        self.addERPDetailsView()
    }
    
    func setCamera(){
        
        let bearings = [0.0, 9.0, 18.0, 27.0, 36.0, 45.0, 54.0, 63.0, 72.0, 81.0, 90.0]
        var cameraOptions = [CameraOptions]()
        if let selectedRoute = selectedRoute {
            
            var coordinates: [[LocationCoordinate2D]] = []
            if let items = selectedRoute.shape?.coordinates {
                coordinates.append(items)
            }
            let multilLine = MultiLineString(coordinates)
            
            let routeGeoMetry = Geometry.multiLineString(multilLine)
            naviMapView.navigationCamera.stop()
            
            //beta.9 change
            
            for bearing in bearings {
                
                let cameraOption = naviMapView.mapView.mapboxMap.camera(for: routeGeoMetry, padding: cameraPadding(), bearing: bearing, pitch: 0)
                cameraOptions.append(cameraOption)
            }
            
            
            let sortedRouteArray = cameraOptions.sorted { $0.zoom! > $1.zoom! }
            //let cameraOptions = naviMapView.mapView.mapboxMap.camera(for: routeGeoMetry, padding: cameraPadding(), bearing: 0, pitch: 0)
            naviMapView.mapView.camera.ease(to: sortedRouteArray[0], duration: 0.8, curve: .easeInOut, completion: nil)
            self.naviMapView.mapView.removERPBoundImagesFromMapStyle()
            self.naviMapView.mapView.addERPBoundImagesToMapStyle()
            self.naviMapView.mapView.removeERPCostLayer()
            self.naviMapView.mapView.addAllERPBoundItemsToMap(features: erpFeatureCollection,isBelowPuck: true,isERPDetail: false)
            
            if erpFeatureCollection.features.count > 0 {
                
                if case let .point(point) = erpFeatureCollection.features[0].geometry {
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) { [weak self] in
                        guard let self = self else { return }
                        self.openERPToolTipView(feature: self.erpFeatureCollection.features[0], location: point.coordinates)
                    }
                    
                }
            }
        }
        else{
            
            var coordinates = [CLLocationCoordinate2D]()
            
            for feature in erpFeatureCollection.features{
                if case let .point(point) = feature.geometry {
                    
                    coordinates.append(point.coordinates)
                }
            }
            naviMapView.navigationCamera.stop()
            
            if(coordinates.count > 0){
                
                let options = CameraOptions(center: coordinates[0], padding: cameraPadding(), zoom: 16, bearing: nil, pitch: nil)
    //            let options =  self.naviMapView.mapView.mapboxMap.camera(for: coordinates, padding:cameraPadding(), bearing: 0, pitch: 0)
                self.naviMapView.mapView.camera.ease(to: options, duration: 0)
                self.naviMapView.mapView.removERPBoundImagesFromMapStyle()
                self.naviMapView.mapView.addERPBoundImagesToMapStyle()
                self.naviMapView.mapView.removeERPCostLayer()
                self.naviMapView.mapView.addAllERPBoundItemsToMap(features: erpFeatureCollection,isBelowPuck: true,isERPDetail: true)
                
                if(coordinates.count > 0){
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) { [weak self] in
                        guard let self = self else { return }
                        self.openERPToolTipView(feature: self.erpFeatureCollection.features[0], location: coordinates[0])
                    }
                }
                
            }
            
        }
        
    }
    
    func cameraPadding() -> UIEdgeInsets {
        return UIEdgeInsets(top: topView.frame.origin.y+topView.frame.size.height+30, left: 40, bottom:(erpDetailsBottomView.frame.size.height/2.2)+30, right: 40)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func addERPDetailsView() {
        guard view.viewWithTag(ERPDetails.viewErpDetailBottomTag) == nil else { return }
        
        let params = [ // Object data to be used by that screen
             "erpId":selectedERPId,
             "erpSelTS":NSNumber(value: selectedERPTime),
             "showPlanTrip":selectedRoute == nil ? false : true
             
        ] as [String : Any]
        let erpDetailsVC = getRNViewBottomSheet(toScreen: "ErpDetail", navigationParams: params as [String:Any])
        erpDetailsVC.view.frame = UIScreen.main.bounds
        
        erpDetailsVC.view.tag = ERPDetails.viewErpDetailBottomTag
        self.addChildViewControllerWithView(childViewController: erpDetailsVC,toView: self.view)
        
//        erpDetailsVC.view.snp.makeConstraints { make in
//            make.bottom.equalToSuperview().offset(0)
//            make.leading.equalToSuperview()
//            make.trailing.equalToSuperview()
//            make.height.equalTo(UIScreen.main.bounds.width)
//            make.height.equalTo(UIScreen.main.bounds.height/2.2)
//        }
        
        self.erpDetailsBottomView = erpDetailsVC.view
        
        print(erpDetailsVC.view.frame.size.height)
        
        
    }
    
    @IBAction func onBack(_ sender: Any) {
    
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.ErpDetails.UserClick.back, screenName: ParameterName.ErpDetails.screen_view)
        moveBack()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        AnalyticsManager.shared.logScreenView(screenName: ParameterName.ErpDetails.screen_view)
        setupDarkLightAppearance()
        setupGestureRecognizers()
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        guard previousTraitCollection != traitCollection else {
            return
        }
        toggleDarkLightMode()
    }
    
    private func setupGestureRecognizers() {
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(panGesture(_:)))
        let pinchGesture = UIPinchGestureRecognizer(target: self, action: #selector(pinchGesture(_:)))
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapGesture(_:)))
        naviMapView.addGestureRecognizer(panGesture)
        naviMapView.addGestureRecognizer(pinchGesture)
        naviMapView.addGestureRecognizer(tapGesture)
        naviMapView.mapView.gestures.delegate = self
    }
    
    @objc func panGesture(_ gesture: UIPanGestureRecognizer){
       
      
        let point = gesture.location(in: self.naviMapView)
        self.queryERPLayer(point: point)
    }
        
    @objc func pinchGesture(_ gesture: UIPinchGestureRecognizer){
        
        //Not required
    }
    
    @objc func tapGesture(_ gesture: UIPinchGestureRecognizer){
        
        self.queryERPLayer(point: gesture.location(in: self.naviMapView))
        
    }
    
    private func toggleDarkLightMode() {
        
        naviMapView.mapView.mapboxMap.style.uri = StyleURI(url: URL(string: isDarkMode ? Constants.Map.basicNightStyle: Constants.Map.basicStyle)!)
        sendThemeChangeEvent(isDarkMode)
        self.setPuckAndAddERPDetailsRNScreen()
    }

}

extension ERPDetails{
    
    func queryERPLayer(point:CGPoint){
        
        _ =  self.naviMapView.mapView.mapboxMap.coordinate(for: point)
        var layerID = [String]()
        
        layerID.append("\(ERPValues.ERPCost_Identifier_SymbolLayer)")
        if(layerID.count > 0){
            
            let queryOptions = RenderedQueryOptions(layerIds: layerID, filter: nil)
            //let Zone2NullOptions = RenderedQueryOptions(layerIds: ["zone2-null"], filter: nil)
            self.naviMapView.mapView.mapboxMap.queryRenderedFeatures(at: point, options: queryOptions) { [weak self] (result) in
                
                guard let self = self else { return }

                switch result {

                case .success(let features):
                    
                    // Return the first feature at that location, then pass attributes to the alert controller.
                    
                    if let selectedFeatureProperties = features.first?.feature.properties,
                       case let .string(type) = selectedFeatureProperties["type"]
                    {
                        
                        if(type == "ERP"){
                            
                            if case let .point(point) = features.first!.feature.geometry {
                                self.openERPToolTipView(feature: features.first!.feature, location: point.coordinates)
                            }
                        }
                    } else {
                        // If the feature is a cluster, it will have `point_count` and `cluster_id` properties. These are assigned
                        // when the cluster is created.
                    }
                    
                case .failure(_):
                    break
                }
            }
        }
    }
    
//    func showTrafficCallout(feature:Turf.Feature,location:CLLocationCoordinate2D){
//
//        self.didDeselectAnnotation()
//        if let selectedFeatureProperties = feature.properties,
//           case let .string(name) = selectedFeatureProperties["trafficCameraName"],
//           case let .string(trafficID) = selectedFeatureProperties["trafficID"]
//
//         {
//
//
//            self.showCallout(name: name, location: location)
//            ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.SELECT_TRAFFIC_CAMERA, data: ["cameraID":trafficID] as? NSDictionary).subscribe(onSuccess: {_ in },                 onFailure: {_ in })
//        }
//    }
    
    func didDeselectAnnotation() {
       if let callout = self.erpCalloutView {
           callout.dismiss(animated: true)
           callout.removeFromSuperview()
           self.erpCalloutView = nil
       }
        
   }
    

    func openERPToolTipView(feature:Turf.Feature,location:CLLocationCoordinate2D){
        
        self.didDeselectAnnotation()
        if let selectedFeatureProperties = feature.properties,
           case let .string(currentprice) = selectedFeatureProperties["currentprice"],
           case let .string(upcomingtime) = selectedFeatureProperties["upcomingtime"],
           case let .string(upcomingprice) = selectedFeatureProperties["upcomingprice"],
           case let .string(erpId) = selectedFeatureProperties["erpid"],
           case let .string(name) = selectedFeatureProperties["name"],
//           case let .string(endTime) = selectedFeatureProperties["endTime"],
            case let .string(selectedTime) = selectedFeatureProperties["selectedTime"]
        {
            
            let currentPriceArray = currentprice.components(separatedBy: "$")
            let currentPriceStr = currentPriceArray[1]
            
            var nextPriceStr = "0.0"
            if(upcomingprice != "")
            {
                let nextPriceArray = upcomingprice.components(separatedBy: "$")
                nextPriceStr = nextPriceArray[1]
            }
            
            if self.selectedTimeChanged == false {
                self.selectedTimeErp = "Current"
            }else {
                self.selectedTimeErp = selectedTime
            }
            
            let erpToolTipView = ERPToolTipsView(id: erpId, name: name, rate: Double(currentPriceStr) ?? 0.0, nextText: upcomingtime, nextRate: Double(nextPriceStr) ?? 0.0, location: location, selectedTimeErp:  self.selectedTimeErp ?? "")
            erpToolTipView.showShadow = false
            erpToolTipView.present(from: self.naviMapView.frame, in: self.naviMapView, constrainedTo: erpToolTipView.frame, animated: true)
            erpToolTipView.center = self.view.center
            self.naviMapView.bringSubviewToFront(erpToolTipView)
            self.erpCalloutView = erpToolTipView
            self.erpCalloutView?.adjust(to: location, in: self.naviMapView)
            if(isFirstTime){
                
                isFirstTime = false
            }
            else{
                self.adjustCameraWhenAmenityClicked(at: location, callout: erpToolTipView)
            }
            
            
            ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.SEND_EVENT_SELECTED_ERP, data: ["erpid":Int(erpId)] as? NSDictionary).subscribe(onSuccess: {_ in
                
                //No need to look for response
                
            },                 onFailure: {_ in
                
                //No need to look for response
            })
        }
    }
    
    func adjustCameraWhenAmenityClicked(at location: LocationCoordinate2D, callout: ToolTipsView) {
        
        
        let cameraOptions = CameraOptions(center: location, padding:cameraPadding(), zoom: self.naviMapView.mapView.mapboxMap.cameraState.zoom) // using current zoom level
        
       self.naviMapView.mapView.camera.ease(to: cameraOptions, duration: Values.standardAnimationDuration, curve: .easeInOut, completion: nil)
       
      
        callout.adjust(to: location, in: self.naviMapView)
        
        

   }
    
}
extension ERPDetails:NavigationMapViewDelegate{
    
    func navigationMapView(_ navigationMapView: NavigationMapView, didAdd finalDestinationAnnotation: PointAnnotation, pointAnnotationManager: PointAnnotationManager)
    {
        
        var annotations = [PointAnnotation]()
        let point = finalDestinationAnnotation.point
        var destinationAnnotation = PointAnnotation(coordinate: point.coordinates)
        destinationAnnotation.image = .init(image: UIImage(named: "destinationWithoutCarpark")!, name: "routePlan")
        annotations.append(destinationAnnotation)
        
        
        if let selectedRoute = selectedRoute{
            
            let coordinate = selectedRoute.shape?.coordinates[0]
            
            var customPointSourceAnnotation = PointAnnotation(coordinate: coordinate!)
            customPointSourceAnnotation.image = .init(image: UIImage(named: "source_ann")!, name: "source")
            annotations.append(customPointSourceAnnotation)
        }
        
        navigationMapView.pointAnnotationManager?.annotations = annotations
    }
}


extension ERPDetails: GestureManagerDelegate {
   
    func gestureManager(_ gestureManager: GestureManager, didEnd gestureType: GestureType, willAnimate: Bool) {
        
        //Not required
    }
    
    func gestureManager(_ gestureManager: GestureManager, didEndAnimatingFor gestureType: GestureType) {
        
        //Not required
    }
    
    func gestureManager(_ gestureManager: GestureManager, didBegin gestureType: GestureType) {
        
        
        
        var name = ParameterName.ErpDetails.MapInteraction.drag_map
        switch gestureType {
        case .pinch:
            
            name = ParameterName.ErpDetails.MapInteraction.pinch_map
        case .singleTap:
            self.didDeselectAnnotation()
            let point = gestureManager.singleTapGestureRecognizer.location(in: self.naviMapView)
            self.queryERPLayer(point: point)
            
        default:
            name = ParameterName.ErpDetails.MapInteraction.drag_map
        }

        AnalyticsManager.shared.logMapInteraction(eventValue: name, screenName: ParameterName.ErpDetails.screen_view)
    }
}

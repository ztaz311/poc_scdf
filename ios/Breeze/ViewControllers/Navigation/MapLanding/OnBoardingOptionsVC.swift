//
//  OnBoardingOptionsVC.swift
//  Breeze
//
//  Created by Zhou Hao on 24/11/21.
//

import UIKit

struct OnboardingOption {
    let type: String
    let title: String
    let bgColr: UIColor
}

final class OnBoardingOptionsVC: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    let onboardingOptions = [
        OnboardingOption(type: "CarPark", title: "Carpark", bgColr: .blue),
        OnboardingOption(type: "Petrol", title: "Petrol", bgColr: .purple),
        OnboardingOption(type: "Hawker", title: "Hawker", bgColr: .systemPink),
        OnboardingOption(type: "Cafe", title: "Cafe", bgColr: .systemPink),
        OnboardingOption(type: "Cafe", title: "Long Beach Cafe", bgColr: .green),
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupOptionsUI()
    }
    
    @IBAction func onAdd(_ sender: Any) {
        
        //Not required
    }

    private func setupOptionsUI() {
        collectionView.delegate = self
        collectionView.dataSource = self
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        flowLayout.minimumLineSpacing = 4
        flowLayout.minimumInteritemSpacing = 10
        flowLayout.estimatedItemSize = CGSize(width: 80, height: 28)
        collectionView.isPagingEnabled = true
        collectionView.collectionViewLayout = flowLayout
    }
}

extension OnBoardingOptionsVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return onboardingOptions.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OptionsCell", for: indexPath) as! OnBoardingOptionsCell
        let option = onboardingOptions[indexPath.row]
        cell.backgroundColor = option.bgColr
        cell.lblTitle.text = option.title
        cell.imageView.image = UIImage(named: "option\(option.type)")
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        return CGSize(width: itemsArray[indexPath.item].size(withAttributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 17)]).width + 25, height: 28)
        print("hi")
        return CGSize(width: 50, height: 28)
    }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
//            return UIEdgeInsets(top: 16, left: 16, bottom: 16, right: 16)
//    }
}


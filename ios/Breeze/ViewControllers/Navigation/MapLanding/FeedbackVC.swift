//
//  FeedbackVC.swift
//  Breeze
//
//  Created by Malou Mendoza on 13/8/21.
//

import UIKit
import Instabug
import CoreLocation
import MapboxCoreNavigation
import MapboxDirections
import MapboxNavigation
import MapboxDirections
import MapboxMaps
import MapboxCoreMaps

class FeedbackVC: UIViewController, UITextViewDelegate {

    
    public var locCoordinate: CLLocationCoordinate2D!
    
    @IBOutlet weak var successView: UIView!
    @IBOutlet weak var descText: UITextView!
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var routeBtn: UIButton!
    @IBOutlet weak var voiceBtn: UIButton!
    @IBOutlet weak var parkingBtn: UIButton!
    @IBOutlet weak var erpBtn: UIButton!
    @IBOutlet weak var uiBtn: UIButton!
    @IBOutlet weak var trafficBtn: UIButton!
    @IBOutlet weak var leadingContainerViewCtr: NSLayoutConstraint!
    
    var locationTrackManager: LocationTrackingManager!
    var navigationService:NavigationService!
    var feedBack:FeedbackEvent!
    var isRouteIssue: Bool = false
    var isVoiceIssue: Bool = false
    var isParkingIssue: Bool = false
    var isERPIssue: Bool = false
    var isUIIssue: Bool = false
    var isTrafficIssue: Bool = false
    var selectedIssuesArray : [FeedbackIssueModel] = [] {
        didSet {
            updateSubmitButton()
        }
    }
    private var isSubmitting = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        AnalyticsManager.shared.logScreenView(screenName: ParameterName.Feedback.screen_view)
        self.successView.isHidden = true
        
        // Cause issue #1876
//        self.descText.roundCorners([.topLeft, .topRight, .bottomLeft, .bottomRight], radius: 8)        
        descText.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        updateSubmitButton()
        
        if (Screen_Width <= 375) {
            leadingContainerViewCtr.constant = 10
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        self.view.roundCorners([.topLeft, .topRight], radius: 30)
        self.view.shadowToTopOfView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupDarkLightAppearance()
        if(isDarkMode){
            self.successView.backgroundColor = UIColor.rgba(r: 34, g: 38, b: 56, a: 1)
            self.view.backgroundColor = UIColor.rgba(r: 34, g: 38, b: 56, a: 1)
        }else{
            self.successView.backgroundColor = .white
            self.view.backgroundColor = .white
        }
    }

    @objc func dismissKeyboard() {
        
        view.endEditing(true)
    }
    
    private func updateSubmitButton() {
        self.submitBtn.backgroundColor = isAnyButtonSelected() ? UIColor(named: "ratingBtnColor")! : UIColor(named: "disableBtnColor")!
        let titleColor = isAnyButtonSelected() ? .white : UIColor(named: "disableBtnTextColor")!
        self.submitBtn.setTitleColor(titleColor, for: .normal)
        self.submitBtn.isEnabled = isAnyButtonSelected()
    }
    
    private func isAnyButtonSelected() -> Bool {
        return isRouteIssue || isVoiceIssue || isParkingIssue || isERPIssue || isUIIssue || isTrafficIssue
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == text.trimmingCharacters(in: .newlines) {
                let currentText = textView.text ?? ""
                guard let stringRange = Range(range, in: currentText) else { return false }
                let updatedText = currentText.replacingCharacters(in: stringRange, with: text)
                return updatedText.count <= 160
           }else{
                textView.resignFirstResponder()
                return false
           }
       }

    func textViewDidBeginEditing(_ textView: UITextView) {
        AnalyticsManager.shared.logEditEvent(eventValue: ParameterName.Feedback.UserEdit.description, screenName: ParameterName.Feedback.screen_view)
    }
    
    func updateIssueItems(){
        if (self.isRouteIssue){
            routeBtn.setImage(UIImage(named: "routeIssueSelected"), for: .normal)
        }else{
            routeBtn.setImage(UIImage(named: "routeIssue"), for: .normal)
        }
        
        if (self.isVoiceIssue){
            voiceBtn.setImage(UIImage(named: "voiceIssueSelected"), for: .normal)
        }else{
            voiceBtn.setImage(UIImage(named: "voiceIssue"), for: .normal)
        }
        
        if (self.isParkingIssue){
            parkingBtn.setImage(UIImage(named: "parkingIssueSelected"), for: .normal)
        }else{
            parkingBtn.setImage(UIImage(named: "parkingIssue"), for: .normal)
        }
        
        if (self.isERPIssue){
            erpBtn.setImage(UIImage(named: "erpIssueSelected"), for: .normal)
        }else{
            erpBtn.setImage(UIImage(named: "erpIssue"), for: .normal)
        }
        
        if (self.isUIIssue){
            uiBtn.setImage(UIImage(named: "UIissueSelected"), for: .normal)
        }else{
            uiBtn.setImage(UIImage(named: "UIissue"), for: .normal)
        }
        
        if (self.isTrafficIssue){
            trafficBtn.setImage(UIImage(named: "trafficIssueSelected"), for: .normal)
        }else{
            trafficBtn.setImage(UIImage(named: "trafficIssue"), for: .normal)
        }
        updateSubmitButton()
    }
    
    
    @IBAction func routeIssuebtnAction(){
        AnalyticsManager.shared.logToggleEvent(eventValue: ParameterName.Feedback.UserToggle.route, screenName: ParameterName.Feedback.screen_view)
        isRouteIssue = !isRouteIssue
        self.updateIssueItems()
    }
    
    @IBAction func voiceIssuebtnAction(){
        AnalyticsManager.shared.logToggleEvent(eventValue: ParameterName.Feedback.UserToggle.voice, screenName: ParameterName.Feedback.screen_view)
        isVoiceIssue = !isVoiceIssue
        self.updateIssueItems()
    }
    
    @IBAction func parkingIssuebtnAction(){
        AnalyticsManager.shared.logToggleEvent(eventValue: ParameterName.Feedback.UserToggle.parking, screenName: ParameterName.Feedback.screen_view)
        isParkingIssue = !isParkingIssue
        self.updateIssueItems()
    }
    
    @IBAction func erpIssuebtnAction(){
        AnalyticsManager.shared.logToggleEvent(eventValue: ParameterName.Feedback.UserToggle.erp, screenName: ParameterName.Feedback.screen_view)
        isERPIssue = !isERPIssue
        self.updateIssueItems()
    }
    
    @IBAction func UIIssuebtnAction(){
        AnalyticsManager.shared.logToggleEvent(eventValue: ParameterName.Feedback.UserToggle.interface, screenName: ParameterName.Feedback.screen_view)
        isUIIssue = !isUIIssue
        self.updateIssueItems()
    }
    
    @IBAction func trafficIssuebtnAction(){
        AnalyticsManager.shared.logToggleEvent(eventValue: ParameterName.Feedback.UserToggle.traffic, screenName: ParameterName.Feedback.screen_view)
        isTrafficIssue = !isTrafficIssue
        self.updateIssueItems()
    }
    
    
    
    @IBAction func backbtnAction(){
       
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Feedback.UserClick.close, screenName: ParameterName.Feedback.screen_view)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func submitbtnAction(){
        guard !isSubmitting else {
            return
        }

        isSubmitting = true
        
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Feedback.UserClick.submit, screenName: ParameterName.Feedback.screen_view)
                
        self.view.endEditing(true)
        
        if(locCoordinate == nil) //This location coordinate is nil and it is crashing, good to check nil condition before proceeding
        {
            locCoordinate = LocationManager.shared.location.coordinate
        }
        if(self.isRouteIssue){
            let issue = FeedbackIssueModel.init(issuetype: "RouteIssue", lat: locCoordinate.latitude, long: locCoordinate.longitude)
            self.selectedIssuesArray.append(issue)
            
            //For Cruise mode
            if(locationTrackManager != nil)
            {
                if(feedBack != nil)
                {
                   // locationTrackManager.cruiseFeedback(type: .routeQuality(subtype: .other),feedBack: feedBack)
                    locationTrackManager.cruiseFeedback(type: .roadIssue(subtype: .none), feedBack: feedBack)
                                    
                }
                
            }
            else
            {
                //This is for Navigation
                if(navigationService != nil)
                {
                    //let feedBack = navigationService.eventsManager.createFeedback()
                    if(feedBack != nil)
                    {
//                        navigationService.eventsManager.sendFeedback(feedBack, type: .routeQuality(subtype: .other))
                        navigationService.eventsManager.sendActiveNavigationFeedback(feedBack,type: .routeQuality(subtype: .none))
                    }
                    
                }
                
            }
            
        }
        if(self.isVoiceIssue){
            let issue = FeedbackIssueModel.init(issuetype: "VoiceIssue", lat: locCoordinate.latitude, long: locCoordinate.longitude)
            self.selectedIssuesArray.append(issue)
            
            //For Cruise mode
            if(locationTrackManager != nil)
            {
                if(feedBack != nil)
                {
                    locationTrackManager.cruiseFeedback(type: .roadIssue(subtype: .none), feedBack: feedBack)
                }
                
            }
            else
            {
                //For Navigation
                if(navigationService != nil)
                {
                    if(feedBack != nil)
                    {
                        navigationService.eventsManager.sendActiveNavigationFeedback(feedBack, type: .confusingAudio(subtype: .none))
                    }
                    
                }
               
            }
            
        }
        if(self.isParkingIssue){
            let issue = FeedbackIssueModel.init(issuetype: "ParkingIssue", lat: locCoordinate.latitude, long: locCoordinate.longitude)
            self.selectedIssuesArray.append(issue)
        }
        if(self.isERPIssue){
            let issue = FeedbackIssueModel.init(issuetype: "ERPIssue", lat: locCoordinate.latitude, long: locCoordinate.longitude)
            self.selectedIssuesArray.append(issue)
        }
        if(self.isUIIssue){
            let issue = FeedbackIssueModel.init(issuetype: "UI Issue", lat: locCoordinate.latitude, long: locCoordinate.longitude)
            self.selectedIssuesArray.append(issue)
        }
        if(self.isTrafficIssue){
            let issue = FeedbackIssueModel.init(issuetype: "TrafficIssue", lat: locCoordinate.latitude, long: locCoordinate.longitude)
            self.selectedIssuesArray.append(issue)
        }
        let feedbackType = FeedbackIssue.init(issuetype: self.selectedIssuesArray, description: self.descText.text)
        FeedbackIssueService().post(feedbackIssue: feedbackType) { [weak self] result in
            guard let self = self else { return }
            
            switch result {
            case .success(_):
                DispatchQueue.main.asyncAfter(deadline: .now()+0.1) {
                    self.successView.isHidden = false
                    self.isSubmitting = false
                }
                
            case .failure(let error):
                print(error.localizedDescription)
            }
           
        }
    }
    
    @IBAction func otherIssuebtnAction(){
       
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Feedback.UserClick.instabug, screenName: ParameterName.Feedback.screen_view)
        //self.dismiss(animated: true, completion: nil)
        Instabug.show()
       // self.backbtnAction()

    }

    
    // MARK: for keyboard show/hide
    @objc func keyboardWillShow(notification: NSNotification) {
        
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if (self.view.frame.origin.y == 0){
                self.view.frame.origin.y -= keyboardSize.height
            }

        }
        
        
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
       
        self.view.frame.origin.y = 0
        self.view.setNeedsLayout()
    }
}

//
//  MapLandingVC+OBU.swift
//  Breeze
//
//  Created by Tuan, Pham Hai on 04/07/2023.
//

import Foundation
import UIKit
import SwiftyBeaver

extension MapLandingVC {
    //  Handle connect/disconnect from Paired vehicle list
    
    func addOBUErrorObservers() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(obuConnectFailure(_:)),
            name: .obuConnectFailure,
            object: nil)
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(obuDisconnectFailure(_:)),
            name: .obuDisconnectFailure,
            object: nil)
    }
    
    func removeOBUErrorObservers() {
        NotificationCenter.default.removeObserver(self, name: .obuConnectFailure, object: nil)
        NotificationCenter.default.removeObserver(self, name: .obuDisconnectFailure, object: nil)
    }
    
    @objc func obuConnectFailure(_ notification: NSNotification) {
        //  Define error come from what screen the decide what to do
        if let model = notification.object as? ObuErrorModel {
            DispatchQueue.main.async {
                self.handleObuConnectionError(model)
            }
        }
    }
    
    @objc func obuDisconnectFailure(_ notification: NSNotification) {
        if let model = notification.object as? ObuErrorModel {
            DispatchQueue.main.async {
                self.handleObuConnectionError(model)
            }
        }
    }
    
    private func handleObuConnectionError(_ model: ObuErrorModel) {
        DispatchQueue.main.async {
            if let errorCode = model.error?.code {
                
                SwiftyBeaver.debug("OBU - handleObuConnectionError: OBU name \(model.name ?? ""), Vehicle No.\(model.vehicleNumber ?? ""), error code: \(errorCode)")
                
                switch errorCode {
                case 1000:
                    self.showAlert(title: "Connection Error", message: "Make sure your mobile data is enable", confirmTitle: "Got It") 
                    AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.ObuPairing.UserPopup.popup_obu_permission_turn_on_internet, screenName: ParameterName.ObuPairing.screen_view)
                case 1001:
                    self.showAlert(title: "Connection Error", message: "Bluetooth needs to be enabled to connect", confirmTitle: "Got It")
                    AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.ObuPairing.UserPopup.popup_obu_connection_error_bluetooth, screenName: ParameterName.ObuPairing.screen_view)
                case 1003, 1004, 1005, 1006, 2002, 2003, 2009, 2010, 400, 401, 408, 500, 616:
                    self.showObuError(.SystemError, model: model)
                case 2000:
                    self.showObuError(.PairingError, model: model)
                case 2001, 1002:
                    self.showObuError(.OBUConnectioLost, model: model)
                case 404:
                    self.showObuError(.OBUNotDetected, model: model)
                case 2004:
                    self.showAlert(title: "Pairing failed", message: "Permission is needed to Pair OBU to \"Breeze\"", confirmTitle: "Close") { action in
                        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.ObuPairing.UserClick.popup_obu_connection_error_obu_close, screenName: ParameterName.ObuPairing.screen_view)
                    }
                    AnalyticsManager.shared.logOpenPopupEventV2(popupName: ParameterName.ObuPairing.UserPopup.popup_obu_connection_error_obu_permission, screenName: ParameterName.ObuPairing.screen_view)
                case 2006:
                    AnalyticsManager.shared.logOpenPopupEventV2(popupName: ParameterName.ObuPairing.UserPopup.popup_obu_mac_not_match, screenName: ParameterName.ObuPairing.screen_view)
                    self.showAlert(title: "", message: "Mac address entered did not match Mac address registered with LTA", confirmTitle: "Close") { action in
                        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.ObuPairing.UserClick.popup_obu_mac_not_match_close, screenName: ParameterName.ObuPairing.screen_view)
                    }
                default:
                    break
                }
            }
        }
    }
    
    private func showObuError(_ errorCase: ConnectError, model: ObuErrorModel) {
        let storyboard: UIStoryboard = UIStoryboard(name: "OBU", bundle: nil)
        if let errorConnectVC = storyboard.instantiateViewController(withIdentifier: "OBUConnectErrorViewController") as? OBUConnectErrorViewController {
            errorConnectVC.modalPresentationStyle = .overCurrentContext
            errorConnectVC.errorCase = errorCase
            errorConnectVC.errorModel = model
            errorConnectVC.retryCompletion = { [weak self] code in
                if code == 2000 {
                    //  Show input vehicle number step
                    self?.obuStepConnectScreen(1)
                }
            }
            self.present(errorConnectVC, animated: true)
        }
    }
    
    //  Handle pairing error
    @objc func updateOBUPairingStatus(_ notification: NSNotification) {
        //  Define error come from what screen the decide what to do
        if let model = notification.object as? ObuErrorModel {
            DispatchQueue.main.async {
                self.handleObuConnectionError(model)
            }
        }
        self.stopObservingOBUPairingStatus()
    }
    
    func startObservingOBUPairingStatus () {
        self.stopObservingOBUPairingStatus()
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(updateOBUPairingStatus(_:)),
            name: .obuUpdatePairingStatus,
            object: nil)
    }
    
    func stopObservingOBUPairingStatus () {
        NotificationCenter.default.removeObserver(self, name: .obuUpdatePairingStatus, object: nil)
    }
    
    func startCruiseMode() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            appDelegate().carPlayMapController?.startCruise()
        }
    }
    
    func checkEnableDebugIfNeeded() {
        if !UserDefaults.standard.bool(forKey: Values.debugEnabled) {
            UserService().getUserData { result in
                switch result {
                case .success(let userData):
                    let isEnableLog = userData.isLogEnabled ?? false
                    UserDefaults.standard.setValue(isEnableLog, forKey: Values.debugEnabledRemotely)
                    break
                case .failure(_):
                    //  By default dont change the debug mode -> User have to go to setting to disable it
                    UserDefaults.standard.setValue(false, forKey: Values.debugEnabledRemotely)
                    break
                }
                
                UserDefaults.standard.setValue(false, forKey: Values.debugEnabled)
            }
        }
    }
    
    func showCardErrorPopup() {
        
        AnalyticsManager.shared.logOpenPopupEventV2(popupName: ParameterName.ObuPairing.UserPopup.popup_obu_card_error, screenName: ParameterName.SystemAnalytics.screen_view)
        
        let alert = UIAlertController(title: "Card Error", message: "Check your cashcard and try connecting OBU again", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Got it", style: .default, handler: { _ in
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.ObuPairing.UserClick.popup_obu_card_error_gotit, screenName: ParameterName.SystemAnalytics.screen_view)
            OBUHelper.shared.didShowCardErrorPopup = false
        }))
        self.navigationController?.present(alert, animated: true)
    }
}

//
//  MapLandingVC+ShareMapCollection.swift
//  Breeze
//
//  Created by Tuan, Pham Hai on 01/12/2022.
//

import UIKit
import Foundation

extension MapLandingVC {
    
    func goToSavedMapCollection() {
        let _ = ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.OPEN_SAVED_FROM_NATIVE, data: [:]).subscribe(onSuccess: {_ in }, onFailure: {_ in })
    }
//    
//    func shareActivityMapCollection(_ shareLink: String) {
//        let shareItems: [Any] = [BreezeActivityItemSource(text: shareLink)]
//        let activityViewController = self.share(items: shareItems, excludedActivityTypes: [.postToFacebook, .airDrop])
//        
//        self.present(activityViewController,
//                     animated: true)
//        activityViewController.completionWithItemsHandler = {
//            (activity, success, items, error) in
//            ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.DISMISS_SHARING_DIALOG, data: [:]).subscribe(onSuccess: {_ in
//                //  No-op
//            },         onFailure: {_ in
//                //  No-op
//            })
//        }
//    }
//    
//    private func share(items: [Any],
//                       excludedActivityTypes: [UIActivity.ActivityType]? = nil,
//                       ipad: (forIpad: Bool, view: UIView?) = (false, nil)) -> UIActivityViewController {
//        let activityViewController = UIActivityViewController(activityItems: items,
//                                                              applicationActivities: nil)
//        if ipad.forIpad {
//            activityViewController.popoverPresentationController?.sourceView = ipad.view
//        }
//        
//        if let excludedActivityTypes = excludedActivityTypes {
//            activityViewController.excludedActivityTypes = excludedActivityTypes
//        }
//        
//        return activityViewController
//    }
//    
//    
//    func shareToolTipLocation(_ shareLink: String) {
//        let shareItems: [Any] = [BreezeActivityItemSource(text: shareLink)]
//        let activityViewController = self.share(items: shareItems, excludedActivityTypes: [.postToFacebook, .airDrop, .message, .mail])
//        
//        self.present(activityViewController,
//                     animated: true)
//        activityViewController.completionWithItemsHandler = {
//            (activity, success, items, error) in
//            ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.DISMISS_SHARING_DIALOG, data: [:]).subscribe(onSuccess: {_ in
//                //  No-op
//            },         onFailure: {_ in
//                //  No-op
//            })
//        }
//    }
    
}

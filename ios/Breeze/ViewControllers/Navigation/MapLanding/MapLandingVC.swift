//
//  MapLandingVC.swift
//  Breeze
//
//  Created by VishnuKanth on 25/11/20.
//

import UIKit
import CoreLocation
import MapboxDirections
import Turf
import MapboxCoreNavigation
@_spi(Restricted) import MapboxMaps
import MapboxCoreMaps
import MapboxNavigation
import EventKit
import Combine
import SwiftyBeaver
import SnapKit
import AVFAudio
import OBUSDK
import CoreBluetooth

class MapLandingVC: BaseViewController, UISearchBarDelegate {
    
    // MARK: - Properties
    var userLocationButton: UserLocationButton?
    
    var inAppTutorial = false {
        didSet {
            
            //  self.showEmailPopUpVC() -> No longer use this popup in version 5.0
        }
    }
    var isTrackingUser = true {
        didSet {
            userLocationButton?.isHidden = isTrackingUser
        }
    }
    
    override var useDynamicTheme: Bool {
        return true
    }
    
    var toDisplayNudge = true // TODO: Need to review this
    
    // MapVC can have more than one children VC.
    // in case have bottom vc and another child VC like triplog => useAlwaydarkMode = true
    override var useAlwaydarkMode: Bool {
        return alwayDarkMode || children.count > 1
    }

    @IBOutlet weak var optionsViewTopConstraints: NSLayoutConstraint!
    var optionsVC: OnBoardingOptionsVC!
    var isCameraTapped = false
    var amenityCancellable: AnyCancellable?
    var updateCarParkCancellable: AnyCancellable?
    var updateMapThemeCancellable: AnyCancellable?
    
    var prevCarParks:[Carpark]?
    @IBOutlet weak var bottomContainerView: UIView!
    @IBOutlet weak var heightConstraints: NSLayoutConstraint! // for bottom panel
    var bottomVC: UIView!
    @IBOutlet weak var bottomConstraintMap:NSLayoutConstraint!
    var destName = ""
    
    var bottomConstraint: NSLayoutConstraint! // bottom constraint for User location button
//    var bottomConstraintFeedback: NSLayoutConstraint! // bottom constraint for Feedback button
    private var trafficIncidentUpdater: TrafficIncidentUpdater!
//    var destSearchBar: DestinationSearchBar?
            
    var disposables = Set<AnyCancellable>()
    private var travelLogVC: TravelLogListVC? 
    private var settingsVC: SettingsVC? // use it for the time being
    
    // MARK: - Global notification
    var currentGlobalNotification: NotificationModel? // notification shown on the map
    var pendingGlobalNotifications: [NotificationModel]? // sent by AppSync but the Cruise/Navigation is on
    var isTripLogOn: Bool {
        return travelLogVC != nil
    }
    
    // MARK: - CarPark toggle feature
//    var parkingButton: ToggleButton!
    var parkingMode: ParkingType {
        if let view = parkingView {
            return view.parkingType
        }
        return .all
    }
    
    var parkingAmenitiesMode: ParkingAndAmenitiesType {
        if let view = parkingAmenities {
            return view.parkingType
        }
        return .all
    }
    
    var isParkingOnBottom = false   // bottom sheet parking on state
    
    var parkingView: ParkingView?
    var exploreView:ExploreView?
    var nudgeCallout: NudgeTooltipView?
    var parkingAmenities: ParkingAndAmenitiesView?
    var dropPinButton: ToggleButton?
    
    /*
     For update carpark
     Will replace CarparksViewModel
    */
    var carparkUpdater: CarParkUpdater?
    
    var currentSelectedCarpark: Carpark?    // callout
    {
        didSet {
            carparkUpdater?.selectCarpark(id: currentSelectedCarpark?.id)
        }
    }
    
    var isDestinationHasCarPark = false
//    var calloutView: CarparkToolTipsView? {
//        didSet {
//            calloutView?.screenName = ParameterName.Home.screen_view
//        }
//    }
    
    var carparkFromBottomSheet = false
    
    // TODO: Refactor - tooltip since all tooltips inherit from ToolTipsView
//    var amenityCalloutView: AmenityToolTipsView? {
//        didSet {
//            amenityCalloutView?.screenName = ParameterName.Home.screen_view
//        }
//    }
//    var trafficCalloutView: TrafficToolTipsView?
//    var erpToolTipView: ERPToolTipsView?
//    var destinationTooltip: DestinationTooltipView?
//    var poiAmenityCalloutView: POIToolTipsView?
//
    var toolTipCalloutView: ToolTipsView?
    
    var previouslyTappedAmenityId: String = ""

    private var activityIndicator: UIActivityIndicatorView?

    var globalNotificationView: GlobalNotificationView?
          
    var carparkToast: BreezeToast?
//    var feedbackButton: UIButton!
    
//    private var cruiseStoppedByUser = false // this flag to tell app don't start cruise automatically any more if the speed is greater than 20km
                    
    private var tipsCallout: TipsCalloutView?
        
    // MARK: Properties
    @IBOutlet weak var navMapView: NavigationMapView!
    
    var waypoints: [Waypoint] = [] {
        didSet {
            waypoints.forEach {
                $0.coordinateAccuracy = -1
            }
        }
    }
    
    lazy var defaultSuccess: RouteRequestSuccess = { [weak self] (response) in
        guard let routes = response.routes, !routes.isEmpty, case let .route(options) = response.options else { return }
        guard let self = self else { return }
        self.navMapView.removeWaypoints()
        self.response = response
        
        // Waypoints which were placed by the user are rewritten by slightly changed waypoints
        // which are returned in response with routes.
        if let waypoints = response.waypoints {
            self.waypoints = waypoints
        }
    }
    
    lazy var defaultFailure: RouteRequestFailure = { [weak self] (error) in
        guard let self = self else { return }
        // Clear routes from the map
        self.response = nil
        //        self?.presentAlert(message: error.localizedDescription)
    }
    
    weak var mapLandingViewModel: MapLandingViewModel? {
        didSet {
            mapLandingViewModel?.delegate = self
        }
    }
    
    typealias RouteRequestSuccess = ((RouteResponse) -> Void)
    typealias RouteRequestFailure = ((Error) -> Void)
    
    var response: RouteResponse? {
        didSet {
            guard let routes = response?.routes, let currentRoute = routes.first else {
                return
            }
            
            navMapView.show(routes)
            navMapView.showWaypoints(on: currentRoute)
        }
    }
    
    var walkingResponse: RouteResponse?
    
//    private var carPlayCancellable: AnyCancellable?
    
    //  MARK: - Update carpark when user pans the map
    var needUpdateCarparks: Bool = false
    var updateCarparkTimer: Timer?
    var isShowCarPark: Bool = false
    
    var timerTest: Timer?
    
    var isShowSuccessConnected: Bool = false
    
    // MARK: - BREEZE2-1352 Share via Tooltip
    private var etaChildVC: UIViewController?
    
    var nearestCarPark: Carpark? 
        
    // MARK: - UIViewController lifecycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //        var array = [String]()
        //        array.remove(at: 0)
        
        AppUpdater.shared.sendAppVersionToServer()
        LocationManager.shared.getLocation()
        
        if(appDelegate().deviceTokenStr == "")
        {
            NotificationCenter.default.addObserver(self, selector: #selector(registerDevice), name: Notification.Name(Values.deviceToken), object: nil)
        }
        else
        {
            /// save device token to User defaults
            UserDefaults.standard.setValue(appDelegate().deviceTokenStr, forKey: Values.push_notification_token)
            self.registerDevice()
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(onSearchClear), name: Notification.Name("SearchClearFromMapModel"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(etaFavMessageReceived), name: Notification.Name(Values.NotificationETASelect), object: nil)
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(showMinimumCardBalance), name: .obuDidGetMinimumCardBalance, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(openDropPinToolTip(_:)), name: .openDropPinToolTip, object: nil)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(applicationWillEnterForeground(_:)),
            name: UIApplication.willEnterForegroundNotification,
            object: nil)
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(applicationDidEnterBackground(_:)),
            name: UIApplication.didEnterBackgroundNotification,
            object: nil)
        
        NotificationCenter.default.publisher(for: Notification.Name(Values.NotificationCloseNotifyArrivalScreen), object: nil)
            .receive(on: DispatchQueue.main)
            .sink { [weak self] notification in
                guard let self = self, let vc = self.etaChildVC else { return }
                self.removeChildViewController(vc)
                self.etaChildVC = nil
            }.store(in: &disposables)
        
        amenityCancellable = AmenitiesSharedInstance.shared.$isAdjustCarPark.sink { [weak self] isAdjustCarPark in
            guard let self = self else { return }
            
            if(isAdjustCarPark){
                DispatchQueue.main.async {
                    
                    if(SelectedProfiles.shared.selectedProfileName() == ""){
                        self.carparkUpdater?.setCPClusterMode(isCluster: false)
                    }
                    self.resetCamera()
                    self.adjustZoomForCarParks()
                }
            }
        }
        
        updateCarParkCancellable = AmenitiesSharedInstance.shared.$isUpdateCarParkLayer.sink { [weak self] isUpdateCarParkLayer in
            guard let self = self else { return }
            if(isUpdateCarParkLayer){
                DispatchQueue.main.async {
                    if(SelectedProfiles.shared.selectedProfileName() != ""){
                        self.carparkUpdater?.setCPClusterMode(isCluster: false)
                    }
                }
            }
        }
        
        view.backgroundColor = UIColor.mapLandingBGColor
        
        // Recover after login
        TripLogger.recover()
                
        setupUI()
        
        self.retrieveUpcomingTrips()
        
        appDelegate().$mapLandingViewModel.sink(receiveValue: { [weak self] mapLandingViewModel in
            guard let self = self else { return }
            
            self.mapLandingViewModel = mapLandingViewModel
            if self.mapLandingViewModel != nil {
//                self.mapLandingViewModel?.delegate = self
                self.mapLandingViewModel?.startUpdating()
                // Don't need this anymore since cruise in a new screen
//                self.mapLandingViewModel?.$isCruise
//                    .receive(on: DispatchQueue.main)    // Must be in main thread
//                    .sink(receiveValue: { [weak self] isCruise in
//                    guard let self = self else { return }
//                        self.cruiseModeStatusChanged(isCruise: isCruise)
//                }).store(in: &self.disposables)
                self.navMapView.mapView.mapboxMap.onNext(event: .mapLoaded) { [weak self] _ in
                    guard let self = self else { return }
                    // create point annotation manager and to show annotation below puck
                    self.navMapView.pointAnnotationManager?.annotations = [PointAnnotation]()
                    self.navMapView.pointAnnotationManager = self.navMapView.mapView.annotations.makePointAnnotationManager(id: CarparkValues.destinationLayerIdentifier, layerPosition: .below("puck"))
                    
                    self.mapLandingViewModel?.mobileMapViewUpdatable = MapLandingViewUpdatable(navigationMapView: self.navMapView)
                    
                    //call API for Notifications only when Map Landing is re created. This can happen in two scenarios. Signout and Sign in and App Launch. We should not call this during app launch. That's why the condition is !=isAppLaunch. if AppLaunch is true this API will be called from startTutorial callBack
//                    let isAppLaunch = UserDefaults.standard.bool(forKey: Values.isAppLaunch)
//                    if(!isAppLaunch){
//                        
//                        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) { [weak self] in
//                            
//                            guard let self = self else { return }
//                            
//                            //To avoid multiple call backs
//                            if(self.globalNotificationView == nil && !self.inAppTutorial){
//                                
//                                self.getNotifications()
//                            }
//                            
//                        }
//                        
//                    }
                    
                    // Move this after getNotifications
//                    // delay it for a while so that the bottomsheet init correctly
//                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [weak self] in
//                        self?.parkingButton.select(true) // by default should show parking
//                    }
                    
                    /// First time load
                    AmenitiesSharedInstance.shared.isDisplayAmenities = true
                    AmenitiesSharedInstance.shared.loadAllImagesToTheMapStyle(navMapView: self.navMapView)
                    self.navMapView.mapView.addERPBoundImagesToMapStyle()
                   
                }
                
                self.navMapView.mapView.mapboxMap.onEvery(event: .mapLoaded) { [weak self] _ in
                    guard let self = self else { return }
                    if let updatable = self.mapLandingViewModel?.mobileMapViewUpdatable {
                        DispatchQueue.main.async {
                            updatable.updateCameraLayer(show: false)
                        }
                    }
                }
                
            }
        }).store(in: &disposables)
        
        appDelegate().$navigationViewModel
            .receive(on: DispatchQueue.main)
            .sink { [weak self] viewModel in
                guard let self = self else { return }
                if viewModel != nil {
                    // should go to navigation only when the the TurnByTurnNavigationViewModel is not created from mobile
                    if viewModel!.fromDeviceType != .mobile {
                        self.startNavigation(viewModel: viewModel!)
                    }
                }
        }.store(in: &self.disposables)
            
        appDelegate().$cruiseViewModel.sink { [weak self] cruiseViewModel in
            guard let self = self else { return }

            if let viewModel = cruiseViewModel {
                let storyboard = UIStoryboard(name: "Cruise", bundle: nil)
                if let vc = storyboard.instantiateViewController(identifier: "CruiseModeVC") as? CruiseModeVC {
                    vc.cruiseViewModel = viewModel // to make sure cruiseViewModel is not empty
                    vc.onNavigateHere = { [weak self] (carpark, screenshot) in
                        guard let self = self else { return }
                        let imageView = UIImageView(image: screenshot)
                        self.view.addSubview(imageView)
                        self.activityIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.large)
                        imageView.addSubview(self.activityIndicator!)
                        self.activityIndicator?.center = self.view.center
                        self.activityIndicator?.hidesWhenStopped = true
                        self.activityIndicator?.startAnimating()
                        self.startNavigation(carpark: carpark, isCruiseMode: true) {
                            self.activityIndicator?.stopAnimating()
                            self.activityIndicator?.removeFromSuperview()
                            imageView.removeFromSuperview()
                        }
                    }
                    self.navigationController?.pushViewController(vc, animated: true)                    
                }
            }
        }.store(in: &self.disposables)
        
        // Only call once
        if case .initial = appDelegate().previousAppState {
            appDelegate().enterHome()
        }
        
        appDelegate().$aps
            .receive(on: DispatchQueue.main)
            .sink { [weak self] theAps in
                guard let self = self else { return }

                // only get this when aps type is upcoming trip
                if let theAps = theAps,
                let type = getAPNSType(aps: theAps) {
                    //go to routeplanning screen not triplog
                    print("PAYLOAD UPCOMING TRIP")
                    //self.openTripLog(fromPush: true)
                    switch type {
                    case Values.upcomingTripType:
                        if let tripId = theAps["tripPlannerId"] as? Int{
                            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.GlobalNotification.UserClick.start_trip_global_noti, screenName: ParameterName.GlobalNotification.screen_view)
                            
                            self.gotoRoutePlanningFromNotification(tripId: tripId)
                        }
                    case Values.voucherType:
                        if let voucherId = theAps["voucherId"] as? String {
                            self.navigationController?.popToViewController(self, animated: false)
                            let messageId = theAps["messageId"] as? Int
                            self.goToMyVoucherFromNotification(voucherId: voucherId, messageId: messageId)
                        }
                        break
                    case Values.exploreMapType:
                        var dataString = ""
                        if let contentId = theAps[Values.contentIdKey] as? String {
                            dataString += "\"\(Values.contentIdKey)\" : \(contentId)"
                        }
                        
                        if let categoryId = theAps[Values.contentCategoryIdKey] as? String {
                            if !dataString.isEmpty {
                                dataString += ","
                            }
                            dataString += "\"\(Values.contentCategoryIdKey)\" : \(categoryId)"
                        }
                        
                        if let inboxId = theAps["messageId"] as? Int {
                            self.updateDisplayedNotifications("\(inboxId)")
                        }
                        self.navigationController?.popToViewController(self, animated: false)
                        self.gotoContent(data: dataString.isEmpty ? nil : dataString)
                        break
                    case Values.inboxType:
//                        if let inboxId = theAps["messageId"] as? Int, let expireTime = theAps["expireTime"] as? Int {
                        if let inboxId = theAps["messageId"] as? Int {
                            self.navigationController?.popToViewController(self, animated: false)
                            self.gotoInboxDetail(messageId: inboxId, expireTime:1) // Don't check expireTime anymore. But house keeping will not working
                        }
                        break
                    default:
                        break
                    }
                    
                    appDelegate().aps = nil // don't allow multiple trip log per notification
                }
        }.store(in: &self.disposables)
        
        // hide the onboarding buttons. Will delete it if needed in the future
        //optionsVC.view.isHidden = true
  
//#if STAGING || TESTING
//        // for testing overspeed to auto start cruise
////        LocationManager.shared.startSimulateCarMoving(after: 10)
//        
//        //  Test case 1
//        LocationManager.shared.startSimulateOverspeed(after: 1, speed: 10)
//
//        DispatchQueue.main.asyncAfter(deadline: .now() + 20) {
//            LocationManager.shared.startSimulateOverspeed(after: 1, speed: 21)
//        }
//#endif
                
        // for user setting updates
        UserSettingsModel.sharedInstance.carparkUpdatedAction
            .debounce(for: .seconds(1), scheduler: RunLoop.main)
            .sink { [weak self] _ in
                guard let self = self else { return }
                self.resumeParking()
            }.store(in: &disposables)

//        UserSettingsModel.sharedInstance.$carparkUpdated
//            .receive(on: DispatchQueue.main)
//            .sink { [weak self] isUpdated in
//                if isUpdated { // any property of carpark updated
//                    self?.resumeParking()
//                }
//            }.store(in: &self.disposables)
        
        // When bookmark is saved
        NotificationCenter.default.publisher(for: Notification.Name(Values.NotificationUpdateCollectionDetail), object: nil)
            .receive(on: DispatchQueue.main)
            .sink { [weak self] notification in
                guard let self = self, let data = notification.userInfo?["bookmarks"] else { return }
                // handle bookmark saved event from RN (it could send from other View instead of Maplanding)
                
                if let fromScreen = notification.userInfo?["fromScreen"] as? String {                 
                    if fromScreen == Values.Collection_From_Screen_Choose {
                        if let json = try? JSONSerialization.data(withJSONObject: data, options: .prettyPrinted),
                           let bookmarks = try? JSONDecoder().decode([Bookmark].self, from: json) {
                            if bookmarks.count == 1 { // only need to handle one tooltip click save
                                self.processBookmarkUpdate(bookmark: bookmarks[0])
                            }
                        }
                    }
                }
                
            }.store(in: &disposables)
        
        DataCenter.shared.subscribeToUserNotification(cognitoId: AWSAuth.sharedInstance.cognitoId) {[weak self] data in
            
            if let jsonData = data.0.data(using: .utf8) {
                if data.1 == "ON_NEW_VOUCHER_ADDED" { // notification type
                    newVoucherAdded()
                } else if data.1 == "INBOX_GLOBAL_NOTIFICATION" {
                    if let notifications = try? JSONDecoder().decode([NotificationModel].self, from: jsonData) {
                        self?.shouldShowNotification(notifications)
                    }
                }
            }
        }
        
        
        // Get ShowNudge From Api maintenance/masterlist
        self.getShowNudge {[weak self] success in
            //  Handle show nudge in 2rd times
            //  Remove all nudge BREEZE2-1008
            self?.checkShowNudgeIfNeed()

        }
        //  BREEZE2-1008 Show OBU instalation popup immediately when user login
        showObuInstallIfNeeded()
        
        //  When app launch and auto login success then update paired vehicle list to RN
        OBUHelper.shared.updatePairedVehicle()
        
        if !OBUHelper.shared.obuName.isEmpty {
            
            //  Start mornitoring bluetooth connection
            OBUHelper.shared.startMonitoringBluetooth()
            
            // Auto connect OBU when user already connect the first time
            if Prefs.shared.isAutoConnectObu(AWSAuth.sharedInstance.cognitoId) {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) { [weak self] in
                    SwiftyBeaver.debug("MapLanding viewDidLoad auto connect OBU")
                    self?.checkAutoConnectObu()
                }
            }
        }
        
        //  Check enable debug mode if need
        self.checkEnableDebugIfNeeded()
    }
    
    func verifyBluetoothPermission() -> Bool {
        var retValue = true
        if !OBUHelper.shared.isOnBluetooth() {
            showBluetoothAlert()
            retValue = false
        } else if !OBUHelper.shared.isBluetoothPermissionGranted() {
            showPermissionAlert()
            retValue = false
        }
        return retValue
    }
    
    private func checkAutoConnectObu() {
                
        if !OBUHelper.shared.isObuConnected() {
            
            if !verifyBluetoothPermission() {
                return
            }
            
            OBUHelper.shared.willTriggerAutoConnect = false
            SwiftyBeaver.debug("OBU - checkAutoConnectObu")
            OBUHelper.shared.autoConnectOBU {  (success, error) in
                if success {
                    self.showToastOBUConnected()
                } else {
                    if let err = error {
                        
                        SwiftyBeaver.debug("OBU - checkAutoConnectObu error: \(err.code) des:\(err.localizedDescription)")

                        let message = "err:\(err.code) des:\(err.localizedDescription)"
                        AnalyticsManager.shared.logSystemEvent(eventValue: ParameterName.SystemAnalytics.SystemEvent.obu_verbose_error, screenName: ParameterName.SystemAnalytics.screen_view, eventMessage: message)
                    }
                }
            }
        } else {
            self.showToastOBUConnected()
        }
        
        //  Remove before add new observer
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("OBUConnectedNotification"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleOBUConnectedNotification), name: NSNotification.Name("OBUConnectedNotification"), object: nil)
    }
    
    @objc func handleOBUConnectedNotification() {
        // Show Toast When CArPlay already sucsses connect OBU
        showToastOBUConnected()
    }
    
    func showToastOBUConnected() {
        self.hideLoadingIndicator()
        showToast(from: self.navigationController?.view ?? self.view, message: "OBU Connected", topOffset: 20, icon: "greenTick", messageColor: .black, duration: 3)
        
    }
    
    func showToastOBUDisconnected() {
        self.hideLoadingIndicator()
        showToast(from: self.navigationController?.view ?? self.view, message: "OBU is disconnected", topOffset: 20, fromCarPlay: false)
    }
    
    @objc func showMinimumCardBalance() {
        showToast(from: self.navigationController?.view ?? self.view, message: "Cashcard value is low. \nPlease top up soon.", topOffset: 20, fromCarPlay: false)
    }
    
    @objc func openDropPinToolTip(_ notification: Notification) {
        DispatchQueue.main.async {
            if let feature = notification.object as? Turf.Feature {
                if case let .point(point) = feature.geometry {
                    self.openDropPinToolTipView(feature: feature, location: point.coordinates)
                }
            }
        }
    }
    
    func updateDropPinToolTip(_ notification: NSDictionary?) {
        
        if let pinData = notification {
            let bookmarkId = pinData["bookmarkId"] as? Int ?? 0
            let lat = pinData["lat"] as? String ?? ""
            let long = pinData["long"] as? String ?? ""
//            let address1 = pinData["address1"] as? String ?? ""
            let name = pinData["name"] as? String ?? ""
            
            if let pinModel = mapLandingViewModel?.dropPinModel {
                if bookmarkId > 0 && lat == pinModel.lat && long == pinModel.long && name == pinModel.name/* && pinModel.address == address1*/ {
                    //  Current pin location already bookmarked then should update tooltip
                    if let dropPinToolTip = self.toolTipCalloutView as? DropPinToolTipView {
                        dropPinToolTip.onSavedDropPinLocation(true, bookmarkId: bookmarkId)
                    }
                }
            }
        }
    }
    
    private func gotoInboxDetail(messageId: Int, expireTime: Int) {
        let notificationData = ["NOTIFICATION_ID":messageId]
        
        // inform RN to open inbox details screen
        let _ = ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.NAVIGATE_TO_INBOX_SCREEN, data: notificationData as NSDictionary).subscribe(onSuccess: {_ in }, onFailure: {_ in })

        // update user default
        var notificationDictionary = [String: Int]()
        if let displayedNotifications = UserDefaults.standard.object(forKey: Values.DISPLAYED_NOTIFICATIONS_KEY) as? [String: Int] {
            notificationDictionary = displayedNotifications
        }
        
        notificationDictionary["\(messageId)"] = expireTime
        if !notificationDictionary.isEmpty {
            UserDefaults.standard.set(notificationDictionary, forKey: Values.DISPLAYED_NOTIFICATIONS_KEY)
        }
    }
    
    
    /// call by push notification
    /// data - Json string with contentID and contentCategoryID
    /// only pass the data if receive push notification or from Custom inbox
    public func gotoContent(data: String?) {
        // no need to add a redDot. The contentVC will add the read flag anyway
        // exploreView?.isNewContent = true
        let storybard = UIStoryboard(name: "ContentVC", bundle: nil)
        if let vc = storybard.instantiateViewController(withIdentifier: String(describing: ContentVC.self)) as? ContentVC {
            vc.data = data != nil ? "{\(data!)}" : nil
            self.navigationController?.pushViewController(vc, animated: true)
            if let ids = mapLandingViewModel?.newContentIds {
                Prefs.shared.setValue(ids, forkey: .seenCustomMapIDs)
            }
        }
    }
    
    // if topVC is not self => pop to Maplanding
    private func goToMyVoucherFromNotification(voucherId: String, messageId: Int?) {
        if !(self.navigationController?.viewControllers.last is MapLandingVC) {
            self.navigationController?.popToViewController(self, animated: false)
            
        }
        // update inbox display status
        if let id = messageId {
            self.updateDisplayedNotifications("\(id)")
        }
        
        _ = ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.OPEN_MY_VOUCHER_FROM_NOTIFICATION, data: ["voucherId": voucherId]).subscribe(onSuccess: { result in  },
                    onFailure: { error in
        })
    }
    
    func gotoRoutePlanningFromNotification(tripId: Int){
           // retrieve the trip planner details first
           
           TripPlanService().getTripPlan(tripId: tripId) { [weak self] result in
               guard let self = self else { return }
               switch result {
               case .success(let details):
                   // show routeplanning
                   DispatchQueue.main.async { [weak self] in
                       guard let self = self else { return }

                       let storybard = UIStoryboard(name: "NewRoutePlanning", bundle: nil)
                       if let vc = storybard.instantiateViewController(withIdentifier: String(describing: NewRoutePlanningVC.self)) as? NewRoutePlanningVC {
                           vc.planTrip = details
                           vc.addressReceived = true
                           self.navigationController?.pushViewController(vc, animated: true)
                       }
                   }

               case .failure(let err):
                   SwiftyBeaver.error("Failed to getTripPlan: \(err.localizedDescription)")
               }
           }
       }
    
    func relaodMapLayersonThemeChange(){
        
        if let mapLandingViewModel = self.mapLandingViewModel {
            if let mobileMapUpdatable = mapLandingViewModel.mobileMapViewUpdatable{
                
                self.navMapView.mapView.removERPBoundImagesFromMapStyle()
                self.navMapView.mapView.addERPBoundImagesToMapStyle()
                
                // reset if any notification symbol here
                if let notification = currentGlobalNotification {
                    addTrafficNotificationToMap(notification)
                }
                
                mobileMapUpdatable.navigationMapView?.mapView.removeTrafficImagesToMapStyle(type: Values.TRAFFIC)
                mobileMapUpdatable.navigationMapView?.mapView.addTrafficImagesToMapStyle(type: Values.TRAFFIC)
                
                if(mapLandingViewModel.landingMode == Values.ERP_MODE){
                    
                    
                    mapLandingViewModel.removeERPLayerFromMap()
                    mapLandingViewModel.updateERPOnMap()
                }
                else if(mapLandingViewModel.landingMode == Values.TRAFFIC_MODE){
                
                    mobileMapUpdatable.removeAmenitiesFromMap(type: Values.TRAFFIC)
                    mapLandingViewModel.getExistingTrafficAndUpdateOnMap(selectedID: "", selectFirstOne: true)
                }
                
                else{
                    
                    if self.prevCarParks != nil {
                        
                        if(self.parkingMode != .hide){
                            self.navMapView.mapView.removeCarparkLayer()
                            self.resumeParking()
                        }
                        
                    }
                    
                }
                
                
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if OBUHelper.shared.autoConnectOBUFromCarplay {
            OBUHelper.shared.autoConnectOBUFromCarplay = false
            if OBUHelper.shared.isObuConnected() {
                OBUHelper.shared.stateOBU = .CONNECTED
            }
        }
        
        self.addOBUErrorObservers()
                    
        AnalyticsManager.shared.logScreenView(screenName: ParameterName.home_screen_name)
                
        self.mapLandingViewModel?.startUpdating()
        self.resetCamera()
        //self.toggleDarkLightMode() //We don't need to check this in viewWillAppear, we can add this in viewDidLoad() and also there will be a callback when there is change in traitCollection
        setupDarkLightAppearance()
        sendThemeChangeEvent(isDarkMode)

        if let notifications = pendingGlobalNotifications {
            checkAndShowGlobalNotification(notifications, showOn: self)
            self.pendingGlobalNotifications = nil
        }
        
        if(AmenitiesSharedInstance.shared.isAmenitiesChanged == true){
            
            sendTriggerUserPrefEvent()
            
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.updateExploreStatus()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.removeOBUErrorObservers()
        mapLandingViewModel?.stopUpdating()
        
    }
    
    /*  Comment this in version 5.0
    func showEmailPopUpVC(){
        
        // check if need to show user email popup
        // 1. Not launched before
        // 2. User has no email or Email is not verified
        // 3. Show email pop up only once
        // 4. Email pop up should not collide with Tutorial/Parking Nudge/Global notifications
        
        let isNudgeShown = UserDefaults.standard.bool(forKey: Values.isParkingNudgeShown)
        if(!inAppTutorial && isNudgeShown == true){
            let emailPopupConfirmed = UserDefaults.standard.bool(forKey: Values.emailPopupConfirmed)
            let isAppLaunch = UserDefaults.standard.bool(forKey: Values.isAppLaunch)
            let emailPopupDismissedCount = UserDefaults.standard.integer(forKey: Values.emailPopupDismissedCount)
            if !emailPopupConfirmed && emailPopupDismissedCount < 1 && isAppLaunch {
                if (AWSAuth.sharedInstance.customEmail.isEmpty && AWSAuth.sharedInstance.providerName.isEmpty) {
                    AnalyticsManager.shared.logOpenPopupEvent(popupName: ParameterName.Home.UserPopup.email, screenName: ParameterName.Home.screen_view)
                    DispatchQueue.main.async { [weak self] in
                        guard let self = self else { return }
                        let storyboard = UIStoryboard(name: "UserUpdate", bundle: nil)
                        let vc = storyboard.instantiateViewController(withIdentifier: String(describing: EmailPopupVC.self)) as! EmailPopupVC
                        vc.view.layer.zPosition = 101
                        vc.delegate = self
                        self.addChildViewControllerWithView(childViewController: vc as UIViewController,toView: self.view)
                    }
                }
            }
            UserDefaults.standard.set(false, forKey: Values.isAppLaunch)
        }
        else{
            
            UserDefaults.standard.set(false, forKey: Values.isAppLaunch)
        }
        
    }
     */
    
    func triggerAppUpdater(){
        
        if(!isCruiseModeRunning() && !isNavigationRunning()){
            appDelegate().findAndShowAppUpdateAlerts(controller: self)
        }
    }
    
//    func triggerNudgeUI(){        
//        self.showNudgeTooltip()
//    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if !appDelegate().checkShowSaveMapCollection() {
            appDelegate().checkShowSaveMapLocation()
        }
        
        DispatchQueue.main.async {
            CLLocationManager().requestWhenInUseAuthorization()
        }
        
        //AmenitiesSharedInstance.shared.requestAmenities(location: LocationManager.shared.location.coordinate)
        
        /// show app update alerts only when not in cruise mode
        if(!isCruiseModeRunning() && !isNavigationRunning()){
            appDelegate().findAndShowAppUpdateAlerts(controller: self)
        }
        
        //bottomVC.statusChange()
        #if DEMO
        onParking()
        #endif

        //  Temporary remove this for demo batch 1
//        startTimerTutorial()
        self.getNotifications()
    }
    
    @objc func applicationWillEnterForeground(_ notification: NSNotification) {
        
        // Only start when this view is visible
        
//        if SelectedProfiles.shared.isProfileSelected(){
//
//            if let mapLandingViewModel = self.mapLandingViewModel {
//                if let mobileMapUpdatable = mapLandingViewModel.mobileMapViewUpdatable{
//
//                    mobileMapUpdatable.navigationMapView.mapView.removeProfileZones()
//                    mobileMapUpdatable.addProfileZones()
//                }
//            }
//
//        }
        if navigationController?.visibleViewController == self {
        }
        
        if(appDelegate().cruiseViewModel == nil && !isNavigationRunning())
        {
            //When app comes to foreground check again the app version
            appDelegate().findAndShowAppUpdateAlerts(controller: self)
        }
        
        //  Confirmed with Shiva still need auto connect when app change state background to foreground
        //  The auto connect OBU in setting only apply for first time launch app
//        if Prefs.shared.isAutoConnectObu(AWSAuth.sharedInstance.cognitoId) {
        if !OBUHelper.shared.isObuConnected() && OBUHelper.shared.hasLastObuData() && OBUHelper.shared.willTriggerAutoConnect {
                //  Need to check if current view controller is not PairingVC
                if let vc = self.navigationController?.topViewController, vc is ObuPairStatusViewController {
                    
                } else {
                    SwiftyBeaver.debug("applicationWillEnterForeground auto connect")
                    self.checkAutoConnectObu()
                }
            }
//        }
        
//        if let mapLandingViewModel = mapLandingViewModel {
//
//            if !mapLandingViewModel.isNavigationStarted && appDelegate().cruiseViewModel == nil{
//
//                self.navMapView.mapView.location.locationProvider.stopUpdatingLocation()
//            }
//        }
//        if let vm = mapLandingViewModel {
//            if appDelegate().cruiseViewModel == nil {
//                vm.startUpdating()
//            }
//        }
        
    }
    
    @objc func applicationDidEnterBackground(_ notification: NSNotification) {
        
//        if let mapLandingViewModel = mapLandingViewModel {
//
//            if !mapLandingViewModel.isNavigationStarted && appDelegate().cruiseViewModel == nil{
//
//                self.navMapView.mapView.location.locationProvider.stopUpdatingLocation()
//            }
//        }
        
    }
            
    func openTripLog(fromPush: Bool = true, navigationParams: [String: Any] = [:]) {
        let storyboard = UIStoryboard(name: "TravelLog", bundle: nil)
        if let vc = storyboard.instantiateViewController(withIdentifier: String(describing: TravelLogListVC.self)) as? TravelLogListVC {
            if fromPush {
                if !(UIApplication.topViewController() is CruiseModeVC) && !(UIApplication.topViewController() is TurnByTurnNavigationVC) && !(UIApplication.topViewController() is TravelLogListVC) {
                    
                    //  Parser nav params
                    if let tripLogTab = navigationParams["tripLogTab"] as? String, let selectedWeek = navigationParams["selectedWeek"] as? String {
                        
                        let splits = selectedWeek.components(separatedBy: CharacterSet(charactersIn: "-"))
                        
                        // Handle select history or transactions
                        if tripLogTab == "history" {
                            //  Select history tap
                            vc.defaultSelectedIndex = 0
                            if splits.count == 2 {
                                if let fromDate = DateUtils.shared.getDate(dateFormat: "yyyyMMdd", dateString: splits.first ?? ""), let toDate = DateUtils.shared.getDate(dateFormat: "yyyyMMdd", dateString: splits.last ?? "") {
                                    vc.fromDateHistory = DateUtils.shared.getTimeDisplay(date: fromDate)
                                    vc.toDateHistory = DateUtils.shared.getTimeDisplay(date: toDate)
                                }
                            }
                        } else if tripLogTab == "transactions" {
                            vc.defaultSelectedIndex = 1
                            if splits.count == 2 {
                                if let fromDate = DateUtils.shared.getDate(dateFormat: "yyyyMMdd", dateString: splits.first ?? ""), let toDate = DateUtils.shared.getDate(dateFormat: "yyyyMMdd", dateString: splits.last ?? "") {
                                    vc.fromDateTransactions = DateUtils.shared.getTimeDisplay(date: fromDate)
                                    vc.toDateTransactions = DateUtils.shared.getTimeDisplay(date: toDate)
                                }
                            }
                            if let filter = navigationParams["filter"] as? String, !filter.isEmpty {
                                vc.filtering = filter
                            }
                        }
                        
                        if let vehicleNumber = navigationParams["vehicleNumber"] as? String, !vehicleNumber.isEmpty {
                            vc.defaultVehicleNumber = vehicleNumber
                        }
                    }
                    
                    self.navigationController?.pushViewController(vc, animated: true)
                    travelLogVC = vc
                }
            } else {
                travelLogVC = vc
                // adjust the size of the trip log view
                vc.hideBackButton = true
                addChildViewControllerWithView(childViewController: vc)
                vc.view.frame = CGRect(x: 0, y: 0, width: bottomVC.frame.width, height: UIScreen.main.bounds.height - Values.bottomTabHeight)
            }
        }
    }
    
    func closeTripLog() {
        guard let vc = travelLogVC else { return }
        removeChildViewController(vc)
        travelLogVC = nil
    }
    
    func openMore() {
        
        let storyboard = UIStoryboard(name: "Account", bundle: nil)
        if let vc = storyboard.instantiateViewController(withIdentifier: String(describing: SettingsVC.self)) as? SettingsVC {
            self.settingsVC = vc
            vc.hideBackButton = false
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
//        let storyboard = UIStoryboard(name: "Account", bundle: nil)
//        if let vc = storyboard.instantiateViewController(withIdentifier: String(describing: SettingsVC.self)) as? SettingsVC {
//            self.settingsVC = vc
//            vc.hideBackButton = true
//            addChildViewControllerWithView(childViewController: vc)
//            vc.view.frame = CGRect(x: 0, y: 0, width: bottomVC.frame.width, height: UIScreen.main.bounds.height - Values.bottomTabHeight)
//        }
    }
    
    func closeMore() {
        guard let vc = settingsVC else { return }
        removeChildViewController(vc)
    }
    
    func processLocationReceived(notification: Notification) {
        if notification.userInfo != nil {
            if let mapLandingViewModel = self.mapLandingViewModel {
                
                if(self.carparkToast != nil) {
                    self.carparkToast?.removeFromSuperview()
                }
                
                if let locationDictionary = notification.userInfo {
                    
                    let lat = locationDictionary["lat"] as! Double
                    let long = locationDictionary["long"] as! Double
                    self.destName = locationDictionary["destName"] as! String
                    
                    var location = CLLocationCoordinate2D(latitude: lat, longitude: long)
                    
                    if let selectedRoutablePoint = parseSelectedRoutablePoint(locationDictionary),
                       let coordinate = selectedRoutablePoint.coordinate {
                        location = coordinate
                    }
                    
                    /// If ERP/Parking/Traffic is selected we don't need to resume parking
                    mapLandingViewModel.isSearchDisocver = true
                    mapLandingViewModel.searchDiscoverLocation = location
                    needUpdateCarparks = false
                    mapLandingViewModel.centerMapLocation = nil
                    if(mapLandingViewModel.landingMode == Values.NONE_MODE || mapLandingViewModel.landingMode == Values.OBU_MODE || mapLandingViewModel.landingMode == Values.PARKING_MODE){
                        self.resumeParking()
                    }
                }
                
            }
        }
    }
    
    func processProfileSelection(notification: Notification){
        
        self.resumeParking()
        
    }
    
    func refreshAmenities(){
        
        if let mapLandingViewModel = mapLandingViewModel {
            if SelectedProfiles.shared.selectedProfileName() != ""{
                
                if let centreLocation = SelectedProfiles.shared.getOuterZoneCentrePoint(){
                    
                    AmenitiesSharedInstance.shared.reloadAmenityRequest(location: centreLocation)
                }
            }
            else{
                
                AmenitiesSharedInstance.shared.reloadAmenityRequest(location: mapLandingViewModel.isSearchDisocver ? mapLandingViewModel.searchDiscoverLocation! : LocationManager.shared.location.coordinate)
            }
            
        }
    }
    
    func handleRefreshAmenities(notification:Notification){
        
        self.refreshAmenities()
    }
    
    func processOpenRouterPlaning(notification: Notification) {
        // TODO: TuyenLX (refactor in the future)
        if let vm = self.carparkUpdater {
            if let carParkDetails = notification.userInfo {
                
                let lat = carParkDetails["lat"] as! Double
                let long = carParkDetails["long"] as! Double
                let carParkName = carParkDetails["name"] as! String
                let parkingID = carParkDetails["parkingID"] as! String
                
                let carPark = vm.getCarparkFrom(id: parkingID)
                
                var isRoutePlanningSeen = false
                for controller in self.navigationController?.viewControllers ?? [] {
                    
                    if let rpVC = controller as? NewRoutePlanningVC {
                        
                        isRoutePlanningSeen = true
                        let address = SearchAddresses(alias: "", address1: carParkName, lat: lat.toString() , long: long.toString(), address2: "", distance: "\(0)")
                        rpVC.originalAddress = address
                        rpVC.isRefreshScreen = true
                        rpVC.addressReceived = true
                        rpVC.selectedCarPark = carPark
                        rpVC.removeCarparks()
                        self.navigationController?.popToViewController(rpVC, animated: true)
                    }
                }
                
                if(isRoutePlanningSeen == false) {
                    
                    let address = SearchAddresses(alias: "", address1: carParkName, lat:lat.toString() , long: long.toString(), address2: "", distance: "\(0)")
                    let storybard = UIStoryboard(name: "NewRoutePlanning", bundle: nil)
                    if let vc = storybard.instantiateViewController(withIdentifier: String(describing: NewRoutePlanningVC.self)) as? NewRoutePlanningVC {
                        vc.originalAddress = address
                        vc.addressReceived = true
                        vc.selectedCarPark = carPark
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
            }
        }
    }
    
    func processRecenter(notification: Notification) {
        if let y = notification.userInfo![Values.AdjustRecentKey] as? Int, let index = notification.userInfo![Values.BottomSheetIndex] as? Int, let fromScreen = notification.userInfo![Values.BottomSheetFromScreen] as? String {
            DispatchQueue.main.async { [weak self] in
                
                guard let self = self else { return }
                self.adjustRecenter(y: y,index: index,fromScreen: fromScreen)
            }
            
        }
    }
    
    func retrieveUpcomingTrips(){
        let planService = TripPlanService()
        let today = DateUtils.shared.getTimeDisplay(date: Date())
        planService.getUpcomingTrips(pageNumber: 1, pageSize: 1, startDate: today, endDate: today, searchKey: "") { result in
            
            switch result {
            case .success(let tripBase):
                print("success retrieveTrip")
                //check if has upcomingtrips send to RN
                self.sendUpcomingTripsToRN(tripBase:tripBase)
                
                
            case .failure(let error):
                print("error retrieveTrip")
                break
            }
        }
    }
    
    func sendUpcomingTripsToRN(tripBase: UpcomingTripBase ){
        
        
        var upcomingTrips = ["hasUpcomingTrips":true]
            if (tripBase.totalItems == 0)
            {
                upcomingTrips = ["hasUpcomingTrips":false]
            }
        ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.FN_UPDATE_UPCOMING_TRIPS, data: upcomingTrips as? NSDictionary).subscribe(onSuccess: { result in  },
                    onFailure: { error in
        })
        
    }
    
    // initial puck
    private func setupPuck() {
        //beta.14 change
        
//        var traitCollection = UITraitCollection(userInterfaceStyle: .light)
//        if(Settings.shared.theme == 0){
//
//            if(appDelegate().isDarkMode()){
//
//                traitCollection = UITraitCollection(userInterfaceStyle: .dark)
//
//            }
//
//        }
//        else{
//
//            traitCollection = UITraitCollection(userInterfaceStyle: UIUserInterfaceStyle(rawValue: Settings.shared.theme) ?? .unspecified)
//        }
        
        let puck2d = Puck2DConfiguration(topImage: UIImage(named: "puckArrow",in:nil,compatibleWith: traitCollection), bearingImage: UIImage(named: "puckIcon", in: nil, compatibleWith: traitCollection), shadowImage: nil, scale: nil)
        
        navMapView.userLocationStyle = .puck2D(configuration: puck2d)
        navMapView.mapView.ornaments.options.compass.visibility = .hidden
    }
    
    @objc func playFlipAnimation(_ isOpen: Bool) {
        if isOpen {
            let transitionOptions: UIView.AnimationOptions = [.transitionFlipFromRight, .showHideTransitionViews]
            self.navMapView.isHidden = false
            UIView.transition(with: navMapView, duration: 0.4, options: transitionOptions, animations: {
                self.navMapView.isHidden = true
            })
        }else {
            let transitionOptions: UIView.AnimationOptions = [.transitionFlipFromLeft, .showHideTransitionViews]
            self.navMapView.isHidden = true
            UIView.transition(with: navMapView, duration: 0.4, options: transitionOptions, animations: {
                self.navMapView.isHidden = false
            })
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: Notification.Name(Values.NotificationETASelect), object: nil)
        NotificationCenter.default.removeObserver(self, name: UIApplication.willEnterForegroundNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIApplication.didEnterBackgroundNotification, object: nil)
        NotificationCenter.default.removeObserver(self)
                
        self.mapLandingViewModel?.mobileMapViewUpdatable = nil
        disposables.removeAll()
    }
    
    // MARK: - send deviceTokenToAPI
    @objc func registerDevice() {
        
        RegisterDeviceService().registerDevice(deviceID: UIDevice.current.identifierForVendor!.uuidString, token: appDelegate().deviceTokenStr, deviceos: "iOS") { result in
            print("DEVICE REGISTER RESULT",result)
            switch result {
            case .success(let value):
                break;
                
                
            case .failure(_):
                break;
            }
        }
    }
        
    @objc func etaFavMessageReceived(notification:NSNotification) {
        
        if let dictionary = notification.userInfo {
        
            let etaID = dictionary["tripEtaFavouriteId"] as! Int
            if let favSelectedIndex = SavedSearchManager.shared.etaFav.firstIndex(where: {$0.tripEtaFavouriteId == etaID}) {
                
                etaSetUp(fav: SavedSearchManager.shared.etaFav[favSelectedIndex])
            }
        }
    }

    func setupUI() {
        
        setupDarkLightAppearance()
        
        updateMapThemeCancellable = Settings.shared.themeUpdatedAction
            .receive(on: DispatchQueue.main)
            .sink(receiveValue: { [weak self] _ in
                self?.setupDarkLightAppearance()
            })
        
        toggleDarkLightMode()
        if !(self.parkingMode == .hide) {
            self.isShowCarPark = true
        }
        
        self.setUpParkingAndAmenities()
        self.setupDropPinButton()
        self.setupLocationButton()
        //self.setUpDestinationSearchBar()
        let params = [ // Object data to be used by that screen
             "baseURL": appDelegate().backendURL,
             "idToken":AWSAuth.sharedInstance.idToken,
             "deviceos":parameters!["deviceos"],
             "deviceosversion":parameters!["deviceosversion"],
             "devicemodel":parameters!["devicemodel"],
             "appversion":parameters!["appversion"],
             "isDarkMode": isDarkMode,
             "isShowCarparkListButton" : isShowCarPark
        ]
        let rnBottomSheetVC = getRNViewBottomSheet(toScreen: "Landing", navigationParams: params as [String:Any])
        rnBottomSheetVC.view.frame = UIScreen.main.bounds;
        bottomVC = rnBottomSheetVC.view
        self.addChildViewControllerWithView(childViewController: rnBottomSheetVC,toView: self.view)
        self.bottomVC.layer.zPosition = 10
//        //for parking toggle
//        self.setupParkingButton()
//        self.setUpExploreButton()
//        self.setupFeedbackButton()
//        self.setupLocationButton()
       
        
//        navigationMapView.delegate = self
        self.setupPuck()
        
        navMapView.mapView.gestures.options.pitchEnabled = false
        navMapView.mapView.tintColor = UIColor.brandPurpleColor
        navMapView.navigationCamera.stop()  // workaround for issue #149
        setupGestureRecognizers()
        
        
        navMapView.mapView.mapboxMap.onEvery(event: .cameraChanged) { [weak self] _ in
            guard let self = self else { return }
            self.processMapboxCameraChanged()
        }
        
        navMapView.mapView.ornaments.options.attributionButton.visibility = .hidden
        // _ = getRNViewBottomSheet(toScreen: "Landing", navigationParams: params as [String:Any])
    }
    
    // MARK: - Theme updated
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        guard previousTraitCollection?.userInterfaceStyle != traitCollection.userInterfaceStyle else {
            return
        }
        toggleDarkLightMode()
    }
    
    private func processMapboxCameraChanged() {
        guard let viewModel = self.mapLandingViewModel else { return }
        
        if (viewModel.landingMode == Values.NONE_MODE || viewModel.landingMode == Values.OBU_MODE || viewModel.landingMode == Values.PARKING_MODE) {
            self.monitorCarParkDestination()
        }
        // TODO: Refactor - should only have one tooltip since all tooltips inherit from ToolTipsView
        if let calloutView = self.toolTipCalloutView {
            calloutView.adjust(to: calloutView.tooltipCoordinate, in: self.navMapView)
        }
    }
    
    private func processBookmarkUpdate(bookmark: Bookmark) {
        // TODO: Refactor - only need one tooltip instance
        //  showToast(from: self.view, message: "Address saved", topOffset: 20, icon: "greenTick", messageColor: .black, duration: 5)
    }
    
    private func toggleDarkLightMode() {
        #if TESTING
        navMapView.mapView.mapboxMap.style.uri = StyleURI(url: URL(string: Constants.Map.testURL)!)
        #else
        
        navMapView.mapView.mapboxMap.style.uri = StyleURI(url: URL(string: isDarkMode ? Constants.Map.nightStyleUrl: Constants.Map.styleUrl)!)
        #endif
        
        setupPuck()
        sendThemeChangeEvent(isDarkMode)
        
        //Checking maplanding view model is initialized or not
        if self.mapLandingViewModel != nil {
            
            DispatchQueue.main.async {
              self.relaodMapLayersonThemeChange()
            }
            //Then set amenitySymbol theme type
            self.mapLandingViewModel?.amenitySymbolThemeType = isDarkMode ? Values.DARK_MODE_UN_SELECTED : Values.LIGHT_MODE_UN_SELECTED
            
        }
        
        setNeedsStatusBarAppearanceUpdate()
        
    }
    
//    @objc func feedbackIssue(){
//        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Home.UserClick.report_issue, screenName: ParameterName.Home.screen_view)
//        showFeedback(from: self, coordinate: self.navMapView.mapView.location.latestLocation?.coordinate ?? CLLocationCoordinate2D())
//    }
    
//    private func setupFeedbackButton(){
//
//        let feedbackBtn = NewFeedbackButton(buttonSize: (Images.feedbackBtn?.size.width)!)
//        feedbackBtn.addTarget(self, action: #selector(feedbackIssue), for: .touchUpInside)
//
//        // Setup constraints such that the button is placed within
//        // the upper left corner of the view.
//        feedbackBtn.translatesAutoresizingMaskIntoConstraints = false
//        navMapView.mapView.addSubview(feedbackBtn)
//        feedbackBtn.snp.makeConstraints { make in
//            make.leading.equalTo(self.parkingView!).offset(0)
////            make.bottom.equalToSuperview().offset(-(322 + Values.bottomTabHeight + 20))
//        }
//
//        self.feedbackButton = feedbackBtn
//    }
            
    private func startCruise(_ fav: Favourites? = nil) {
        guard let vm = mapLandingViewModel else {
            return
        }

        vm.startCruise(fav: fav)
    }
            
    // MARK: Location button related
    // Button creation and autolayout setup
     func setupLocationButton() {
        let userLocationButton = UserLocationButton(buttonSize: (Images.locationImage?.size.width)!)
        userLocationButton.addTarget(self, action: #selector(locationButtonTapped), for: .touchUpInside)
        
        // Setup constraints such that the button is placed within
        // the upper left corner of the view.
        userLocationButton.translatesAutoresizingMaskIntoConstraints = false
        navMapView.mapView.addSubview(userLocationButton)
        userLocationButton.snp.makeConstraints { make in
            if let parkingAmenitiesView = self.parkingAmenities {
                make.leading.equalTo(parkingAmenitiesView).offset(-8)
    //            make.bottom.equalTo(self.feedbackButton.snp.top).offset(-20)
                make.bottom.equalToSuperview().offset(-(322 + Values.bottomTabHeight + 10))
            } 
        }

        self.userLocationButton = userLocationButton
        userLocationButton.isHidden = true // hide it by default
    }
    
    private func adjustRecenter(y: Int,index:Int,fromScreen:String) {
        
//        feedbackButton.snp.updateConstraints { make in
//            make.bottom.equalToSuperview().offset(-(CGFloat(y) + Values.bottomTabHeight + 20))
//        }
        
//        if let button = feedbackButton {
//            button.isHidden = false
//        }

        self.userLocationButton?.snp.updateConstraints { make in
            make.bottom.equalToSuperview().offset(-(CGFloat(y) + Values.bottomTabHeight + 20))
        }

        if let mapLandingViewModel = mapLandingViewModel  {
            
            //if(y > mapLandingViewModel.bottomSheetHeight){
            
                mapLandingViewModel.bottomSheetHeight = y
                
            if(fromScreen == "Landing" || fromScreen == "ChooseCollection")
            {
                mapLandingViewModel.bottomSheetIndex = index
                self.adjustTooltipToCentre()
            }
            //}
            
            
            
        }
    }
    
    @objc func onSearchClear() {
        self.needUpdateCarparks = false
        self.mapLandingViewModel?.centerMapLocation = nil
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) { [self] in
         if let mapLandingViewModel = self.mapLandingViewModel {
             
             if(mapLandingViewModel.landingMode == Values.NONE_MODE || mapLandingViewModel.landingMode == Values.OBU_MODE){
                 
                 self.navMapView.pointAnnotationManager?.annotations = []
                 self.prevCarParks = nil
                 if currentGlobalNotification == nil {
                     self.resumeParking()
                 }
             }
         }
         
        }
    }
    
    @objc func locationButtonTapped(sender: UserLocationButton) {
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Home.UserClick.recenter, screenName: ParameterName.Home.screen_view)
        processRecenterMapview()
    }
    
    func showOBUDisplay(_ isOverSpeed: Bool = false) {
        if appDelegate().carPlayConnected && appDelegate().carPlayMapController != nil {
            //  Start cruise mode
            AnalyticsManager.shared.logOpenPopupEventV2(popupName: ParameterName.ObuDisplayMode.UserPopup.projected_to_dhu, screenName: ParameterName.ObuDisplayMode.screen_view)
            
            OBUHelper.shared.isCruiseModeFromObuDisplay = true
            
             startCruiseMode()
            
        } else {
            
            //  This code to make sure we dont call present obu cruise twice
            /*
            if let nav = self.navigationController?.presentedViewController as? UINavigationController {
                if let _ = nav.viewControllers.first as? OBUDisplayViewController {
                    return
                }
            }
             */
            if let vcs = self.navigationController?.viewControllers {
                for vc in vcs {
                    if let _ = vc as? OBUDisplayViewController {
                        return
                    }
                }
            }

            let storyboard = UIStoryboard(name: "OBUDisplay", bundle: nil)
            if let vc = storyboard.instantiateViewController(withIdentifier: "OBUDisplayViewController") as? OBUDisplayViewController {

                vc.isOverSpeed = isOverSpeed
//                let nav = UINavigationController(rootViewController: vc)
//                nav.modalPresentationStyle = .fullScreen
                vc.modalPresentationStyle = .fullScreen
                vc.onNavigateHere = { [weak self] (carpark) in
                    guard let self = self else { return }
                    self.startNavigation(carpark: carpark, isCruiseMode: true) {}
                }

                vc.onNavigateToAmenity = { [weak self] (name, address, coordinate, amenityId, amenityType) in
                    guard let self = self else { return }
                    self.startNavigationToLocation(name: name, address: address, location: coordinate, amenityId: amenityId, amenityType: amenityType) { success in
                        print("Success navigate to \(address), success: \(success)")
                    }
                }
                self.navigationController?.pushViewController(vc, animated: true)// .present(nav, animated: true)
            }
        }
    }
    
    
    func obuStepConnectScreen(_ stepIndex: Int = 0) {
        
        DispatchQueue.main.async {
            let storyboard = UIStoryboard(name: "OBU", bundle: nil)
            if let vc = storyboard.instantiateViewController(withIdentifier: "ObuPairStatusViewController") as? ObuPairStatusViewController {
                vc.currentStep = stepIndex
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    
    func resetPanAndSearchLocation() {
        self.needUpdateCarparks = false
        self.mapLandingViewModel?.centerMapLocation = nil
        mapLandingViewModel?.searchDiscoverLocation = nil
        mapLandingViewModel?.isSearchDisocver = false
    }
    
    func processRecenterMapview(fromCarpark: Bool = false) {
        //  Invalid pan update carparks
        if let _ = mapLandingViewModel?.centerMapLocation {
            mapLandingViewModel?.centerMapLocation = nil
            needUpdateCarparks = false
            stopUpdateCarparkTimer()
            self.prevCarParks = nil
            self.resumeParking()
        }
        
        resetCamera()
        if !fromCarpark {
            didDeselectAnnotation()
            didDeselectAmenityAnnotation()
        }
    }
    
    func resetCamera() {
        
        isTrackingUser = true
        
        DispatchQueue.main.async {
            
            if let mapLandingViewModel = self.mapLandingViewModel {
                
                if let mapViewUpdatable = mapLandingViewModel.mobileMapViewUpdatable {
                    
                    if mapLandingViewModel.landingMode == Values.ERP_MODE{
                        
                        mapLandingViewModel.adjustZoomeLevelForERP(mapViewUpdatable: mapViewUpdatable)
                        
                    }
                    
                    else if(mapLandingViewModel.landingMode == Values.TRAFFIC_MODE){
                        
                        mapLandingViewModel.getExistingTrafficAndUpdateOnMap( selectFirstOne: true)
                    }
                    else if(mapLandingViewModel.landingMode == Values.PARKING_MODE){
                        
                        if self.prevCarParks != nil {
                            
                            //mapLandingViewModel.bottomSheetHeight = 416
                            self.adjustZoomForCarParks()
                        }
                        else{
                            
                            mapLandingViewModel.recenter()
                        }
                    }
                    else{
                        //if self.currentGlobalNotification == nil && !mapLandingViewModel.isEventFromInbox {
                            if(AmenitiesSharedInstance.shared.amenitiesSymbolLayer.count == 0){
                                
                                if self.prevCarParks != nil {
                                    self.removeTrafficNotificationFromMap()
                                    self.adjustZoomForCarParks()
                                }
                                else{
                                    self.removeTrafficNotificationFromMap()
                                    mapLandingViewModel.recenter()
                                }
                            }
                            else{
                                
                                if(self.currentGlobalNotification != nil){
                                    
                                    if self.prevCarParks != nil {
                                        self.adjustZoomForCarParks()
                                    }
                                }
                                self.removeTrafficNotificationFromMap()
                                mapLandingViewModel.recenter()
                            }
                       // }
                        
                    }
                    
                }
                
            }
            
            
        }
        
        
    }
        
    // MARK: - UIGestureRecognizer methods
    private func setupGestureRecognizers() {
        let longPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress(_:)))
//        let touchGesture = UITapGestureRecognizer(target: self, action: #selector(tapGesture(_:)))
        navMapView.addGestureRecognizer(longPressGestureRecognizer)
//        navMapView.addGestureRecognizer(touchGesture)
        
        navMapView.mapView.gestures.delegate = self

        // we'll use mapbox GestureManagerDelegate to handle this
    }
        
    @objc func handleLongPress(_ gesture: UILongPressGestureRecognizer) {
        
        guard gesture.state == .began else {
            return
        }
        let gestureLocation = gesture.location(in: navMapView)
//        let destinationCoordinate = navMapView.mapView.mapboxMap.coordinate(for: gestureLocation)
        didDeselectAnnotation()
    }
    
    
    func shareTooltipViaLocation(_ address: String) {
        self.etaChildVC = showShareDriveScreen(viewController: self, address: address, etaTime: "", fromScreen: "cruise")
    }
    
}

//using work around
//  MARK: - GestureManagerDelegate methods
extension MapLandingVC: GestureManagerDelegate {
   
    func gestureManager(_ gestureManager: GestureManager, didEnd gestureType: GestureType, willAnimate: Bool) {
        
        if(!self.carparkFromBottomSheet){
            
            if(self.navMapView.mapView.cameraState.zoom <= Values.clusterMaxZoom + 1.0){
                
                if self.toolTipCalloutView != nil  { //|| self.calloutView != nil || self.poiAmenityCalloutView != nil
                    self.didDeselectAnnotation()
                    self.didDeselectAmenityAnnotation()
                }
            }
            
        }
        
        //  Handle explore carparks
        if !willAnimate && gestureType != .pinch && gestureType != .singleTap {
            //  Then consider to update carpark nearby center location
            self.triggerEndPanGesture()
        }
        
    }
    
    func gestureManager(_ gestureManager: GestureManager, didEndAnimatingFor gestureType: GestureType) {
        //  Handle explore carparks
        if gestureType != .pinch && gestureType != .singleTap {
            //  Then consider to update carpark nearby center location
            self.triggerEndPanGesture()
        }
    }
    
    func gestureManager(_ gestureManager: GestureManager, didBegin gestureType: GestureType) {
        
        var name = ParameterName.Home.MapInteraction.drag
        switch gestureType {
        case .pinch:
            name = ParameterName.Home.MapInteraction.pinch
        // rc4 - remove rotate
//        case .rotate:
//            name = ParameterName.Home.MapInteraction.rotate
        case .singleTap:
            let point = gestureManager.singleTapGestureRecognizer.location(in: navMapView)            
            self.queryAmenitiesLayer(point: point) { [weak self] (needAddDropPin, shouldRemovePreviousPin) in
                guard let self = self else { return }
                if needAddDropPin {
                    //  Handle show location pin tool tip when user tap on map
                    if self.mapLandingViewModel?.isOnDropPin == true {
                        let pinLocation = self.navMapView.mapView.mapboxMap.coordinate(for: point)
                        self.getDropPinLocationDetails(pinLocation) {[weak self] droppin in
                            if let dropPinModel = droppin {
                                self?.mapLandingViewModel?.dropPinModel = dropPinModel
                            }
                        }
                    }
                }
                
                if shouldRemovePreviousPin {
                    self.mapLandingViewModel?.dropPinModel = nil
                }
            }
        default:
            name = ParameterName.Home.MapInteraction.drag
        }
        
//        didDeselectAnnotation()
        
        AnalyticsManager.shared.logMapInteraction(eventValue: name, screenName: ParameterName.Home.screen_view)

        // rc4
//        if gestureType == .pan || gestureType == .pinch || gestureType == .tap(numberOfTaps: 2, numberOfTouches: 1) {
         if gestureType == .pan || gestureType == .pinch || gestureType == .doubleTapToZoomIn || gestureType == .doubleTouchToZoomOut {

             isTrackingUser = false
             mapLandingViewModel?.noTracking()
        }
    }
}

extension MapLandingVC: EmailPopupVCDelegate {
    
    func onDismiss() {
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Home.UserClick.popup_add_email_later, screenName: ParameterName.Home.screen_view)
        
        // increase the ignore count
        let count = UserDefaults.standard.integer(forKey: Values.emailPopupDismissedCount)
        UserDefaults.standard.set(count+1, forKey: Values.emailPopupDismissedCount)
    }
    
    func onConfirm() {
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Home.UserClick.popup_add_email_now, screenName: ParameterName.Home.screen_view)

        // Show account page
        let storyboard = UIStoryboard(name: "Account", bundle: nil)
        if let vc = storyboard.instantiateViewController(withIdentifier: String(describing: UserAccountContainer.self)) as? UserAccountContainer {
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        // In this implementation, we just assume the user add the email and validate when user click the Sure button so that we'll not popup again!!!
        UserDefaults.standard.set(true, forKey: Values.emailPopupConfirmed)
    }
}

// This is delegate when navigation not start from route planning screen
extension MapLandingVC:TurnByTurnNavigationVCDelegate{
    
    // This is call when navigation start from carplay
    func onArrival() {
//        showRatingVC()
    }
    
    func goToParking(){
        
        if let topController = UIApplication.shared.keyWindow?.rootViewController {
            if topController.isKind(of:UINavigationController.self) {
                
                let navigationController = topController as! UINavigationController
                let storyboard = UIStoryboard(name: "ParkingSGWeb", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "ParkingWebVC") as UIViewController
                navigationController.pushViewController(vc, animated: true)
                
            }
            
        }
    }
    
    func onWalking(response: RouteResponse, screenshot: UIImage?)  {
        let imageView = UIImageView(image: screenshot)
        self.view.addSubview(imageView)
        startWalking(response: response, screenshot: screenshot)
        DispatchQueue.main.asyncAfter(deadline: .now()+0.5) {
            imageView.removeFromSuperview()
        }
    }
    
//    func showRatingVC() {
//        let storyboard = UIStoryboard(name: "Navigation", bundle: nil)
//        if let vc = storyboard.instantiateViewController(withIdentifier: String(describing: RatingVC.self)) as? RatingVC {
////            vc.tripGUID = self.viewModel?.getTripId() ?? "NA"
//            vc.onDismissed = {
//                // no implementation
//            }
//            self.addChildViewControllerWithView(childViewController: vc as UIViewController,toView: self.view)
//        }
//    }
    
    private func animation(to y: CGFloat, animated: Bool) {
        if animated {
            UIView.animate(withDuration: 0.2) { [weak self] in
                guard let self = self else { return }
                let frame = self.bottomContainerView.frame
                self.bottomContainerView.frame = CGRect(x: frame.origin.x, y: y, width: frame.width, height: frame.height)
            }
        } else {
            let frame = self.bottomContainerView.frame
            self.bottomContainerView.frame = CGRect(x: frame.origin.x, y: y, width: frame.width, height: frame.height)
        }
    }
    
    func isCruiseModeRunning() -> Bool {
        
        if(appDelegate().cruiseViewModel != nil){
            return true
        }
        
        return false
    }
    
    func isNavigationRunning() -> Bool {
        guard let vm = mapLandingViewModel else { return false }
        return vm.isNavigationStarted
    }
    
    func etaSetUp(fav:Favourites){
//        guard let vm = mapLandingViewModel else { return }
        if(ETATrigger.shared.tripCruiseETA == nil)
        {
            startCruise(fav)
            //bottomVC.statusChange()
            //bottomVC.collapse()

        }
    }
    
}

extension MapLandingVC:ExploreViewDelegate{
    
    func exploreSelected(){
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Home.UserClick.tap_custom_map, screenName: ParameterName.Home.screen_view)
        
        // contentId is empty since it's not from push notification
        gotoContent(data: nil)
    }
}




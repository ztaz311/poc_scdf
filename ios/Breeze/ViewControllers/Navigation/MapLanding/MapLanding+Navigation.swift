//
//  MapLanding+Navigation.swift
//  Breeze
//
//  To start navigation directory from MapLandingVC since the navigation started from  CarPlay
//
//  Created by Zhou Hao on 18/10/21.
//

import UIKit
import CoreLocation
import MapboxDirections
import Turf
import MapboxCoreNavigation
@_spi(Restricted) import MapboxMaps
import MapboxCoreMaps
import MapboxNavigation
import SwiftyBeaver

extension MapLandingVC {
    
    // Start navigation by CarPlay
    func startNavigation(viewModel: TurnByTurnNavigationViewModel) {
        // Issue #250 - to stop cruise mode here so that it won't affect navigation idle time
//        NotificationCenter.default.post(name: Notification.Name(Values.notificationNavigationStart), object: nil)
        // We'll post notificationStart in TurnByTurnNavigationViewModel

        let navigationService = viewModel.navigationService
        let routeOptions = viewModel.navigationService.router.routeProgress.routeOptions
        // #273 based on what Ochi suggested
        routeOptions.locale = Locale.enSGLocale() // HERE
                        
        let styles = isDarkMode ? [CustomNightStyle()] : [CustomDayStyle()]
        #if targetEnvironment(simulator)
        let simulation = SimulationMode.always
        #else
        let simulation = SimulationMode.never
        #endif

        navigationService.simulationMode = simulation
#if DEMO || STAGING
        navigationService.simulationSpeedMultiplier = 4
#endif
        let options = NavigationOptions(styles: styles, navigationService: navigationService, predictiveCacheOptions: PredictiveCacheOptions())
        
        let progressVC = UIStoryboard(name: "Navigation", bundle: nil).instantiateViewController(withIdentifier: String(describing: NavProgressVC.self)) as! NavProgressVC
        options.bottomBanner = progressVC
        let topBannerVC = TopBannerViewController()
        topBannerVC.shouldShowInstructionFullList = appDelegate().carPlayConnected
        options.topBanner = topBannerVC
                
        let navigationViewController = TurnByTurnNavigationVC(for: navigationService.router.indexedRouteResponse.routeResponse, routeIndex: 0, routeOptions: routeOptions, navigationOptions: options)
        navigationViewController.routeLineTracksTraversal = true
        navigationViewController.showsContinuousAlternatives = false
        navigationViewController.endRouteDelegate = self
        progressVC.delegate = navigationViewController
//
        progressVC.navigationViewController = navigationViewController
//        navigationViewController.selectedAddress = self.selectedAddress
        // don't show end of route feedback screen
        navigationViewController.showsEndOfRouteFeedback = false
        // don't show floating buttons
        navigationViewController.floatingButtons = []
        navigationViewController.waypointStyle = .building
        navigationViewController.topBannerVC = topBannerVC
        navigationViewController.automaticallyAdjustsStyleForTimeOfDay = false
        
        navigationViewController.modalPresentationStyle = .fullScreen
                        
        present(navigationViewController, animated: true) {
            // navigation started
            
            appDelegate().cruiseSubmitHistoryData(description: "CruiseMode-End")
            appDelegate().startNavigationRecordingHistoryData()
        }
    }
    
    // start cruise to a car park
    func startNavigation(carpark: Carpark, isCruiseMode: Bool, completion: @escaping (()->Void)) {
        
        // setup route from direction api
        let coordinate = LocationManager.shared.location.coordinate
        let origin = Waypoint(coordinate: coordinate, coordinateAccuracy: -1, name: "Origin")
        let destination = Waypoint(coordinate: CLLocationCoordinate2D(latitude: carpark.lat ?? 0, longitude: carpark.long ?? 0), coordinateAccuracy: -1, name: carpark.name ?? "Carpark")

        DirectionService.shared.carParkRouteResponse(origin: origin, destination:destination) { [weak self] response, routeType in
            
            guard let self = self, let response = response, case let .route(routeOptions) = response.options else { return }
            
            routeOptions.locale = Locale.enSGLocale() // HERE
            
            let styles = self.isDarkMode ? [CustomNightStyle()] : [CustomDayStyle()]
            #if targetEnvironment(simulator)
            let simulation = SimulationMode.always
            #else
            let simulation = SimulationMode.never
            #endif
            
            let navigationService = MapboxNavigationService(routeResponse: response, routeIndex: 0, routeOptions: routeOptions, routingProvider: MapboxRoutingProvider(.hybrid), credentials: NavigationSettings.shared.directions.credentials,simulating: simulation)
                        
#if DEMO || STAGING
            navigationService.simulationSpeedMultiplier = 1.5
            
#endif
            let options = NavigationOptions(styles: styles, navigationService: navigationService, predictiveCacheOptions: PredictiveCacheOptions())
        
            let progressVC = UIStoryboard(name: "Navigation", bundle: nil).instantiateViewController(withIdentifier: String(describing: NavProgressVC.self)) as! NavProgressVC
            options.bottomBanner = progressVC
            let topBannerVC = TopBannerViewController()
            topBannerVC.shouldShowInstructionFullList = appDelegate().carPlayConnected
            options.topBanner = topBannerVC
            
            let navigationViewController = TurnByTurnNavigationVC(for: response, routeIndex: 0, routeOptions: routeOptions, navigationOptions: options)
            navigationViewController.routeLineTracksTraversal = true
            navigationViewController.showsContinuousAlternatives = false
            navigationViewController.endRouteDelegate = self
            navigationViewController.topBannerVC = topBannerVC
            navigationViewController.isCruiseMode = isCruiseMode
            progressVC.delegate = navigationViewController

            progressVC.navigationViewController = navigationViewController
            
            navigationViewController.selectedAddress = SearchAddresses(alias: carpark.name ?? "", address1: carpark.name ?? "", lat: "\(carpark.lat ?? 0)", long: "\(carpark.long ?? 0)", address2: "", distance: "")
            
            navigationViewController.destAddress = AnalyticDestAddress(address1: carpark.name, address2: "", lat: carpark.lat, long: carpark.long, amenityType: "carpark", amenityId: carpark.id ?? "", layerCode: "")
                        
            // don't show end of route feedback screen
            navigationViewController.showsEndOfRouteFeedback = false
            // don't show floating buttons
            navigationViewController.floatingButtons = []
            navigationViewController.waypointStyle = .building
            navigationViewController.topBannerVC = topBannerVC
            
            RoundingTable.metric = RoundingTable(thresholds: [
                .init(maximumDistance: Measurement(value: 999, unit: .meters), roundingIncrement: 1, maximumFractionDigits: 0),
                .init(maximumDistance: Measurement(value: 999, unit: .kilometers), roundingIncrement: 0.0001, maximumFractionDigits: 1)
            ])
            
            navigationViewController.automaticallyAdjustsStyleForTimeOfDay = false
            
            navigationViewController.modalPresentationStyle = .fullScreen
                    
            if let appdel = UIApplication.shared.delegate as? AppDelegate {
                appdel.navigationController = navigationViewController
            }
            
            // change app state and pass navigation/eta info
            appDelegate().enterIntoNavigation(from: .mobile, navigationService: navigationService, with: nil, to: carpark)
            
            self.present(navigationViewController, animated: true) {
                completion()
            }
        }
    }
    
    // Start navigate to EV or Petrol
    func startNavigationToLocation(name: String, address: String, location: CLLocationCoordinate2D, amenityId: String, amenityType: String , completion: ((Bool) -> Void)? = nil) {
        
        // setup route from direction api
        let coordinate = LocationManager.shared.location.coordinate
        let origin = Waypoint(coordinate: coordinate, coordinateAccuracy: -1, name: "Origin")
        let destination = Waypoint(coordinate: location, coordinateAccuracy: -1, name: name)

        DirectionService.shared.carParkRouteResponse(origin: origin, destination:destination) { [weak self] response, routeType in
            
            guard let self = self, let response = response, case let .route(routeOptions) = response.options else {
                completion?(false)
                return
            }
            
            routeOptions.locale = Locale.enSGLocale() // HERE
            
            let styles = self.isDarkMode ? [CustomNightStyle()] : [CustomDayStyle()]
            #if targetEnvironment(simulator)
            let simulation = SimulationMode.always
            #else
            let simulation = SimulationMode.never
            #endif
            
            let navigationService = MapboxNavigationService(routeResponse: response, routeIndex: 0, routeOptions: routeOptions, routingProvider: MapboxRoutingProvider(.hybrid), credentials: NavigationSettings.shared.directions.credentials,simulating: simulation)
                        
#if DEMO || STAGING
            navigationService.simulationSpeedMultiplier = 1.5
#endif
            let options = NavigationOptions(styles: styles, navigationService: navigationService, predictiveCacheOptions: PredictiveCacheOptions())
        
            let progressVC = UIStoryboard(name: "Navigation", bundle: nil).instantiateViewController(withIdentifier: String(describing: NavProgressVC.self)) as! NavProgressVC
            options.bottomBanner = progressVC
            let topBannerVC = TopBannerViewController()
            topBannerVC.shouldShowInstructionFullList = appDelegate().carPlayConnected
            options.topBanner = topBannerVC
            
            let navigationViewController = TurnByTurnNavigationVC(for: response, routeIndex: 0, routeOptions: routeOptions, navigationOptions: options)
            navigationViewController.routeLineTracksTraversal = true
            navigationViewController.showsContinuousAlternatives = false
            navigationViewController.endRouteDelegate = self
            navigationViewController.topBannerVC = topBannerVC
            progressVC.delegate = navigationViewController

            progressVC.navigationViewController = navigationViewController
            
            navigationViewController.selectedAddress = SearchAddresses(alias: "", address1: name, lat: "\(location.latitude)", long: "\(location.longitude)", address2: address, distance: "")
            
            navigationViewController.destAddress = AnalyticDestAddress(address1: name, address2: address, lat: location.latitude, long: location.longitude, amenityType: "", amenityId: "", layerCode: "")
            
            // don't show end of route feedback screen
            navigationViewController.showsEndOfRouteFeedback = false
            // don't show floating buttons
            navigationViewController.floatingButtons = []
            navigationViewController.waypointStyle = .building
            navigationViewController.topBannerVC = topBannerVC
            
            RoundingTable.metric = RoundingTable(thresholds: [
                .init(maximumDistance: Measurement(value: 999, unit: .meters), roundingIncrement: 1, maximumFractionDigits: 0),
                .init(maximumDistance: Measurement(value: 999, unit: .kilometers), roundingIncrement: 0.0001, maximumFractionDigits: 1)
            ])
            
            navigationViewController.automaticallyAdjustsStyleForTimeOfDay = false
            
            navigationViewController.modalPresentationStyle = .fullScreen
                    
            if let appdel = UIApplication.shared.delegate as? AppDelegate {
                appdel.navigationController = navigationViewController
            }
            
            // change app state and pass navigation/eta info
            appDelegate().enterIntoNavigation(from: .mobile, navigationService: navigationService, with: nil)
            
            self.present(navigationViewController, animated: true) {
                completion?(true)
            }
        }
    }
}

//
//  MapLandingVC+Tutorial.swift
//  Breeze
//
//  Created by Tuyen, Le Xuan  on 18/04/2022.
//

import UIKit
import SwiftyBeaver

extension MapLandingVC {
    /*
     Call at viewdidAppear
     Start tutorial timer
     */
    func startTimerTutorial() {
        appDelegate().window?.addTapView(tag: Values.tutorialTag, handle: { [weak self] in
            guard let self = self else {return}
            self.removeTutorialGesture()
        })
        mapLandingViewModel?.startTimerTutorial { [weak self] result in
            guard let self = self else { return }
            
            if(result){
                self.inAppTutorial = true
                self.addTutorialView()
            }
            else{
                //Update this only when the app is launched
                self.getNotifications()
                //}
            }
        }
    }
    
    /*
     Call RN to showing tutorial screen
     */
    func addTutorialView() {
        sendOpenOnbardingTutorial()
    }
    
    /*
     Remove tutorial timer when user click to screen
     */
    func removeTutorialGesture() {
        mapLandingViewModel?.stopTimerTutorial()
        appDelegate().window?.removeTapView(Values.tutorialTag)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        removeTutorialGesture()
        super.touchesBegan(touches, with: event)
    }
    
    /*
     Call api to update user setting
     */
    
    func processCloseTutorial(notification: Notification) {
        
        //Fetching and shpowing global notifications after in-app tutorial
        //self.inAppTutorial = false
        self.getNotifications()
        
        mapLandingViewModel?.didCloseTutorialSuccess()
    }
    
    func getShowNudge(completion: @escaping (Bool) -> ()) {
        DispatchQueue.global(qos: .background).async {
            // get master list first
            SettingsService.shared.getMasterLists("USER_PREFERENCE") { [weak self] result in
                guard self != nil else { return }
                switch result {
                case .success(let masterLists):
                    let showNudge = masterLists.masterlists.showNudge
                    let newUserThresholdDays = masterLists.masterlists.newUserThresholdDays
                    if let firstData = showNudge.first {
                        Prefs.shared.setValue(firstData.value, forkey: .valueShowNudge)
                    }
                    
                    if let firstDay = newUserThresholdDays.first {
                        Prefs.shared.setValue(firstDay.value, forkey: .thresholdDays)
                    }
                    completion(true)
                    
                case .failure(let error):
                    SwiftyBeaver.error("Failed to getUserData: \(error.localizedDescription)")
                    completion(false)
                }
            }
        }
    }
    
    func showObuInstallIfNeeded() {
//        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            //  If obu not yet install in Breeze app then show it up for the first time
            let didShowObuInstallation = Prefs.shared.getBoolValueWithSub(AWSAuth.sharedInstance.cognitoId, key: .didShowObuInstallation)
            if !didShowObuInstallation {
                Prefs.shared.setBoolValueWithSub(AWSAuth.sharedInstance.cognitoId, key: .didShowObuInstallation, value: true)
                openObuInstallation()
            }
//        }
    }
    
    func checkShowNudgeIfNeed() {
        
        //  Handle case when user update from old version
        if UserDefaults.standard.object(forKey: PrefsKey.didLaunchAppFirstTime.rawValue) == nil {
            //  didLaunchAppFirstTime now is only use for checking user update app or reinstall app
            Prefs.shared.setValue(true, forkey: .didLaunchAppFirstTime)
            //  If there is no value set in old version then set didLaunchAppAppearNudge by default is false to ignore first time
            Prefs.shared.setBoolValueWithSub(AWSAuth.sharedInstance.cognitoId, key: .didLaunchAppAppearNudge, value: false)
        } else if (Prefs.shared.getBoolValue(.didLaunchAppFirstTime) == false) {
            Prefs.shared.setValue(true, forkey: .didLaunchAppFirstTime)
            //  If value is true then set didLaunchAppAppearNudge is true to continue show nudge
            Prefs.shared.setBoolValueWithSub(AWSAuth.sharedInstance.cognitoId, key: .didLaunchAppAppearNudge, value: true)
//            showObuInstallIfNeeded()
            return
        }
        
        //  New implementation ignore the first time per each user
        let didLaunchAppAppearNudge = Prefs.shared.getBoolValueWithSub(AWSAuth.sharedInstance.cognitoId, key: .didLaunchAppAppearNudge)
        if !didLaunchAppAppearNudge {
            Prefs.shared.setBoolValueWithSub(AWSAuth.sharedInstance.cognitoId, key: .didLaunchAppAppearNudge, value: true)
//            showObuInstallIfNeeded()
            return
        }
        
        //  If 2rd time now on
        if didLaunchAppAppearNudge == true {
            //  If user update Breeze app we need transfer the old value then remove it from prefs
            if UserDefaults.standard.object(forKey: PrefsKey.didShowNudge.rawValue) != nil {
                let didShowNudge = Prefs.shared.getBoolValue(.didShowNudge)
                Prefs.shared.setBoolValueWithSub(AWSAuth.sharedInstance.cognitoId, key: .didShowNudge, value: didShowNudge)
                Prefs.shared.removeValueForKey(.didShowNudge)
            }
            
            if  Prefs.shared.getBoolValueWithSub(AWSAuth.sharedInstance.cognitoId, key: .didShowNudge) == false {
                UserService().getUserData { [weak self] result in
                    guard let self = self else { return }
                    switch result {
                    case .success(let userData):
                        let createdDate = userData.createdTimeStamp
                        let dateCreatedMili = Int64(createdDate)
                        let currentDate = Date().currentTimeInMiliseconds()
                        let thresholdDaysUser = Prefs.shared.getStringValue(.thresholdDays)
                        let dateInt = Int64(thresholdDaysUser) ?? 0
                        let thresholdDays = dateCreatedMili + dateInt*24*60*60*1000

                        Prefs.shared.setValue(thresholdDays, forkey: .thresholdDaysNewUser)
                        let isShowNudge = Prefs.shared.getStringValue(.valueShowNudge)
                        if isShowNudge == "Yes" && currentDate < thresholdDays {
                            self.showNudge()
                            Prefs.shared.setBoolValueWithSub(AWSAuth.sharedInstance.cognitoId, key: .didShowNudge, value: true)
                        } else {
//                            self.showObuInstallIfNeeded()
                        }
                    case .failure(let error):
                        SwiftyBeaver.error("Failed to getUserData: \(error.localizedDescription)")
                    }
                }
            }
        }
    }
    
//    func checkNewUser(isNewUser: Bool){
//        let userID = AWSAuth.sharedInstance.getUserID()
//        Prefs.shared.setValue(userID, forkey: .userID)
//        
//    }
    

    
//    func showNudgeTooltip() {
//
//        /// check if ParkingNudge is shown previously or not
//        let isNudgeShown = UserDefaults.standard.bool(forKey: Values.isParkingNudgeShown)
//        if UserDefaults.standard.bool(forKey: Values.isParkingNudgeShown) {
//            readyToShowVouchAnimation()
//        }
//
//        let topVC = self.navigationController?.topViewController
//        if let mapLandigVC = topVC as? MapLandingVC {
//
//            if(isNudgeShown == false && self.globalNotificationView == nil && topVC is MapLandingVC && mapLandigVC.toDisplayNudge){
//
//                if nudgeCallout != nil {
//                    return
//                }
//
//                UserService().getUserData { result in
//                    switch result {
//                    case .success(let userData):
//                        readyToShowVouchAnimation()
//                        print(userData)
//                    case .failure(let error):
//                        SwiftyBeaver.error("Failed to getUserData: \(error.localizedDescription)")
//                    }
//                }
//            }
//        }
//    }
    
    /// This function is call after all the check
    private func showNudge() {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            //self.parkingView?.parkingViewType = .expanded
            /// set parking nudge value to true in UserDefaults
            //UserDefaults.standard.set(true, forKey: Values.isParkingNudgeShown)
            let overlay = UIView(frame: self.view.bounds)
            overlay.backgroundColor = UIColor.black.withAlphaComponent(0.6)
            self.bottomVC.addSubview(overlay)
            let tooltip = NudgeTooltipView(name: Values.maplandingNudgeCarparkTitle, address: Values.maplandingNudgeCarparkSubTitle, steps: [
                    NudgeStep(title:Values.maplandingNudgeCarparkTitle, subTitle:Values.maplandingNudgeCarparkSubTitle,offsetX:0, offsetY: 20, arrowDirection: .right),
                    NudgeStep(title:Values.maplandingNudgeSaveTitle, subTitle:Values.maplandingNudgeSaveSubTitle,offsetX: 45, offsetY:0,  arrowDirection: .down),
                    NudgeStep(title:Values.maplandingNudgeExploreTitle, subTitle:Values.maplandingNudgeExploreSubTitle,offsetX:0, offsetY: 60, arrowDirection: .right)
            ])
            tooltip.onClose = { [weak self] in
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Home.UserClick.nudge_close, screenName: ParameterName.Home.screen_view)
                guard let self = self else { return }
                overlay.removeFromSuperview()
                self.dismissNudgeToolTip()
                
//                self.showObuInstallIfNeeded()
            }
            // TODO: To make it like UITableView by adding DataSource/Delegate if needed
            tooltip.onProgress = { [weak self] complete in
                
                guard let self = self else { return }
                if complete {
                    AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Home.UserClick.nudge_done, screenName: ParameterName.Home.screen_view)
                    overlay.removeFromSuperview()
                    self.dismissNudgeToolTip()
                } else {
                    AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Home.UserClick.nudge_next, screenName: ParameterName.Home.screen_view)
                    if tooltip.currentStep == 1 {
                        self.adjustToolTipFrameForSaveButton()
                    } else { // the tooltip for explore button
                        self.adjustNudgeTooltipFrameForExploreButton()
                    }
                }
            }
//            tooltip.present(from: self.view.frame, in: self.view, constrainedTo: tooltip.frame, animated: true)
//            tooltip.present(from: self.bottomVC.frame, in: self.bottomVC, constrainedTo: tooltip.frame, animated: true)
            tooltip.present(from: overlay.frame, in: overlay, constrainedTo: tooltip.frame, animated: true)
            self.nudgeCallout = tooltip
            self.view.bringSubviewToFront(tooltip)
//            if self.bottomVC != nil{
//                self.view.bringSubviewToFront(self.bottomVC)
//            }
            self.nudgeCallout?.layer.zPosition = 20
            AnalyticsManager.shared.logPopUpEvent(popupName: ParameterName.Home.UserPopup.nudge, eventName: ParameterName.RoutePlanning.UserPopUp.popup_open)
            self.adjustNudgeTooltipFrame()
        }
    }

    func dismissNudgeToolTip(){
        
        if let nudgeCallout = nudgeCallout {

//            readyToShowVouchAnimation()
            UserDefaults.standard.set(true, forKey: Values.isParkingNudgeShown)
            nudgeCallout.dismiss(animated: true)
            self.nudgeCallout = nil

            AnalyticsManager.shared.logPopUpEvent(popupName: ParameterName.Home.UserPopup.nudge, eventName: ParameterName.RoutePlanning.UserPopUp.popup_close)
        }
    }

    private func adjustNudgeTooltipFrame() {
        
        guard let parkingView = self.parkingAmenities,
              let tooltip = self.nudgeCallout else { return }
        
        let y = parkingView.frame.origin.y - 5
        let x = Screen_Width - parkingView.frame.width - tooltip.frame.width - tooltip.xPadding
        tooltip.frame = CGRect(origin: CGPoint(x: x, y: y), size: tooltip.frame.size)
    }

    private func adjustNudgeTooltipFrameForExploreButton() {
        
        guard let exploreView = self.exploreView,
              let tooltip = self.nudgeCallout else { return }
        
        let y = exploreView.frame.origin.y - 5
        let x = Screen_Width - exploreView.frame.width - tooltip.frame.width - tooltip.xPadding
        tooltip.frame = CGRect(origin: CGPoint(x: x, y: y), size: tooltip.frame.size)
    }

    private func adjustToolTipFrameForSaveButton(){
        
        guard let tooltip = self.nudgeCallout else { return }
        let screenWidth = self.view.frame.size.width
        
        let y = UIScreen.main.bounds.height - Values.bottomTabHeight - tooltip.frame.size.height + 10
        
        let x = screenWidth * (1/4)
        
        tooltip.frame = CGRect(origin: CGPoint(x: x, y: y), size: tooltip.frame.size)
    }
}


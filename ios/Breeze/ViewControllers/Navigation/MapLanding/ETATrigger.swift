//
//  ETATrigger.swift
//  Breeze
//
//  Created by VishnuKanth on 13/08/21.
//

import Foundation
import UIKit
import CoreLocation
class ETATrigger{
    
    static let shared = ETATrigger()
    
    var liveLocTripId = ""
    var lastSendLocation: CLLocation?
    var tripCruiseETA: TripCruiseETA?
    var lastLiveLocationUpdateTime = 0.0
    var isReminder1Sent = false
    //Initializer access level change now
    private init(){
        
        //This is just an empty constructor
    }
    
    func sendLiveLocationCoordinates(latitude:Double,longitude:Double,status:LiveLocationStatusType){
        let now = Date().timeIntervalSince1970
        
        if(status == .inprogress)
        {
            if tripCruiseETA!.shareLiveLocation == "Y" && !liveLocTripId.isEmpty && (now - lastLiveLocationUpdateTime) > 1 {
                print("time = \(now)")
                lastLiveLocationUpdateTime = now
                DataCenter.shared.startUpdatingUserTripLiveLocation(tripId: liveLocTripId, latitude: latitude.toString(), longitude: longitude.toString(), status: status.rawValue,arrivalTime: "",coordinates: "")
                
            }
        }
        else
        {
            if(!liveLocTripId.isEmpty){
                DataCenter.shared.startUpdatingUserTripLiveLocation(tripId: liveLocTripId, latitude: latitude.toString(), longitude: longitude.toString(), status: status.rawValue,arrivalTime: "",coordinates: "")
                updateLiveLocationStatus(status: status)
            }
            
        }
        
    }
    
     func updateLiveLocationStatus(status: LiveLocationStatusType) {
        TripETAService().updateLocationSharingStatus(LiveLocationStatus(tripEtaUUID: self.liveLocTripId, status: status.rawValue)) { result in
            switch result {
            case .failure(let err):
                print(err)
            case .success(let response):
                print("Live Location \(status): \(response.message)")
            }
        }
    }
    
    func setUpTripETA(etaFav:Favourites){
        
//        tripCruiseETA = TripCruiseETA(type: TripETA.ETAType.Init, message: etaFav.message ?? "", recipientName: etaFav.recipientName!, recipientNumber: etaFav.recipientNumber!, shareLiveLocation: etaFav.shareLiveLocation!, voiceCallToRecipient: etaFav.voiceCallToRecipient!, tripStartTime: Int(Date().timeIntervalSince1970), tripDestLat: etaFav.tripDestLat?.toDouble() ?? 0, tripDestLong: etaFav.tripDestLong?.toDouble() ?? 0, destination: etaFav.destination!,tripEtaFavouriteId: etaFav.tripEtaFavouriteId!)
        tripCruiseETA = TripCruiseETA(type: TripETA.ETAType.Init, message: etaFav.message ?? "", recipientName: etaFav.recipientName!, recipientNumber: etaFav.recipientNumber!, shareLiveLocation: "Y", tripStartTime: Int(Date().timeIntervalSince1970), tripDestLat: etaFav.tripDestLat?.toDouble() ?? 0, tripDestLong: etaFav.tripDestLong?.toDouble() ?? 0, destination: etaFav.destination ?? "",tripEtaFavouriteId: etaFav.tripEtaFavouriteId!)

    }
    
    func etaReminder(reminderType:String)
    {
        TripETAService().sendReminder(TripETAReminder(tripEtaUUID: self.liveLocTripId, type: reminderType)) { result in
            switch result {
            case .failure(let err):
                print(err)
            case .success(let response):
                self.isReminder1Sent = true
                print("1 min Cruise (\(self.liveLocTripId)): \(response.message)")
            }
        }
    }
    
    func resetETA(){
        
        liveLocTripId = ""
        tripCruiseETA = nil
        lastLiveLocationUpdateTime = 0.0
        isReminder1Sent = false
    }
    
}


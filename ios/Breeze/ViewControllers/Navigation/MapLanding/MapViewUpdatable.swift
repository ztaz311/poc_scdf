//
//  MapViewUpdatable.swift
//  Breeze
//
//  Created by Zhou Hao on 30/9/21.
//

import Foundation
import CoreLocation
import MapboxDirections
import Turf
import MapboxCoreNavigation
import MapboxMaps
import MapboxCoreMaps
import MapboxNavigation
import UIKit
@_spi(Experimental) import MapboxMaps

protocol MapViewUpdatableDelegate: AnyObject {
    func onLiveLocationEnabled(_ value: Bool)
    func onMuteEnabled(_ value: Bool)
    func onParkingEnabled(_ value: Bool)
    func onShareDriveEnabled(_ value: Bool)
    func onSearchAction()
    func onEndAction()
    func onEvChargerAction(_ value: Bool)
    func onPetrolAction(_ value: Bool)
    func onParkingAction()
}

protocol MapViewUpdatable: AnyObject {

    var navigationMapView: NavigationMapView? { get }
    var delegate: MapViewUpdatableDelegate? { get set }
    
    init(navigationMapView: NavigationMapView)
    
    // MARK: - Methods
    
    ///  Parking availability popup
    func updateParkingAvailability(carpark: Carpark?)
    
    ///  Detect user is soon arriving then show notification
    func updateNotificationWhenUserSoonArrive(carpark: Carpark?, message: String)

    /// Feature detection notification
    func updateNotification(featureId: String?, currentFeature: FeatureDetectable?)
    
    /// Tunnel
    func updateTunnel(isInTunnel: Bool)
    
    /// For cruise mode
    func cruiseStatusUpdate(isCruise: Bool)
    
    /// ETA
    func updateETA(eta: TripCruiseETA?)
    func updateETA(eta: TripETA?)
    func updateLiveLocationEnabled(_ isEnabled: Bool)
    func shareDriveButtonEnabled(_ isEnabled: Bool)
    
    func showETAMessage(recipient: String, message: String)
    
    //Identifying available ERP's on route line in RoutePlanningScreen
    func initERPUpdater(response:RouteResponse?)
        
    /// Location update
//    func update(roadName: String,speedLimit:Double, location: CLLocation?, rawLocation: CLLocation?)
    func updateRoadName(_ name: String)
    func updateSpeedLimit(_ value: Double)
        
    func overSpeed(value: Bool)
}

// MARK: Extensions - common function
extension MapViewUpdatable {
    
    var currentLocation: CLLocationCoordinate2D? {
        get {
            return navigationMapView?.mapView.location.latestLocation?.coordinate
        }
    }
    
    func setInitialMapCamera() {
        
        // #306. It seems ok. But need to test if it will affect anything else?
        if let navigationViewportDataSource = self.navigationMapView?.navigationCamera.viewportDataSource as? NavigationViewportDataSource {
            navigationViewportDataSource.options.followingCameraOptions.zoomUpdatesAllowed = false
            navigationViewportDataSource.followingMobileCamera.zoom = Values.defaultMapZoomLevel
            navigationViewportDataSource.followingCarPlayCamera.zoom = Values.defaultMapZoomLevel
        }
        
        // Original code to set the initial map camera
//        if let coordinate = self.navigationMapView?.mapView.location.latestLocation?.coordinate {
//            self.navigationMapView?.mapView.mapboxMap.setCamera(to: CameraOptions(center: coordinate, zoom: Values.defaultMapZoomLevel))
//        }
    }
    
    func setCameraOptions(_ options: MapboxMaps.CameraOptions,isEase:Bool = false) {
        
        if(isEase){
            navigationMapView?.mapView.camera.ease(to: options, duration: 1.5)
        }
        else{
            navigationMapView?.mapView.mapboxMap.setCamera(to: options)
        }
        
    }
    
    func setGroupCameraOptions(coordinates:[CLLocationCoordinate2D]){
        
        if let options =  navigationMapView?.mapView.mapboxMap.camera(for: coordinates, padding: .zero, bearing: 0, pitch: 0) {
            navigationMapView?.mapView.mapboxMap.setCamera(to: options)
        }
    }
    
    func setCameraAfterSearch(location:CLLocationCoordinate2D,bottomSheetHeight:Int, isParkingOn: Bool = true){
        
//        let options = CameraOptions(center: location, padding: UIEdgeInsets(top: 0, left: 0, bottom: CGFloat(bottomSheetHeight) + 180, right: 0), bearing: 0.0, pitch: 0.0)
        
//        if(SelectedProfiles.shared.selectedProfileName() != ""){
//
//            let options = CameraOptions(center: location, padding: UIEdgeInsets(top: 0, left: 0, bottom: self.navigationMapView?.frame.size.height - CGFloat(bottomSheetHeight), right: 0),zoom:Values.defaultProfileZoomLevl, bearing: 0.0, pitch: 0.0)
//            setCameraOptions(options, isEase: true)
//        }
//        else{
            
        let options = CameraOptions(center: location, padding: UIEdgeInsets(top: 0, left: 0, bottom: (self.navigationMapView?.frame.size.height ?? 0.0) - CGFloat(bottomSheetHeight), right: 0), bearing: 0.0, pitch: 0.0)
        setCameraOptions(options, isEase: true)
        //}
        
        
        if !isParkingOn {
            self.navigationMapView?.pointAnnotationManager?.annotations = [PointAnnotation]()
            self.navigationMapView?.pointAnnotationManager = self.navigationMapView?.mapView.annotations.makePointAnnotationManager(id: CarparkValues.destinationLayerIdentifier, layerPosition: .above("puck"))

            var destinationAnnotation = PointAnnotation(coordinate: location)
            destinationAnnotation.image = .init(image: UIImage(named: "destinationWithoutCarpark")!, name: "routePlan")
            navigationMapView?.pointAnnotationManager?.annotations = [destinationAnnotation]
        }
    }
    
    func resetCamera(isCruise: Bool,bottomSheetHeight:Int = 0) {
        if SelectedProfiles.shared.selectedProfileName() != ""{
            
            if let profileLocation = SelectedProfiles.shared.getOuterZoneCentrePoint(), let bearing = navigationMapView?.mapView.cameraState.bearing {
                
                setCamera(at: profileLocation, heading: bearing, animated: true, reset: true, isCruise: isCruise,bottomSheetHeight: bottomSheetHeight)
            }
        }
        else{
            
            if let coordinate = self.navigationMapView?.mapView.location.latestLocation?.coordinate, let bearing = navigationMapView?.mapView.cameraState.bearing {
                setCamera(at: coordinate, heading: bearing, animated: true, reset: true, isCruise: isCruise,bottomSheetHeight: bottomSheetHeight)
            }
        }
        
    }
    
    func setCamera(at coordinate: CLLocationCoordinate2D?, heading: Double = 0, animated: Bool = true, reset: Bool = false, isCruise: Bool, isCarPlay: Bool = false,bottomSheetHeight:Int = 0) {

        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            
            self.navigationMapView?.navigationCamera.stop()
            self.navigationMapView?.mapView.setCruiseCamera(at: coordinate, heading: heading, animated: animated, reset: reset, isStarted: isCruise, isCarPlay: isCarPlay,bottomSheetHeight: bottomSheetHeight)
        }
    }
    
    func setupCustomUserLocationIcon(isCruise: Bool) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            
            self.navigationMapView?.mapView.setUpCruiseCustomUserLocationIcon(isStarted: isCruise)
        }
    }
    
    func removeTrafficLayer() {
        navigationMapView?.mapView.removeTrafficIncidentLayer()
    }
    
    func addTrafficeLayer(features:Turf.FeatureCollection, isBelowPuck: Bool = true) {
        navigationMapView?.mapView.addIncidentsToMap(features: features, isBelowPuck: isBelowPuck)
    }
    
    private func removeExistingAmenityImage(type:String){
        
        navigationMapView?.mapView.removeExistingAmenityImages(type: type)
    }
    
    func addAmenityImageToMapStyle(type:String,urlString:String){
        
        self.removeExistingAmenityImage(type: type)
        navigationMapView?.mapView.addImagesToMapStyle(type: type, urlString: urlString)
    }
    
    func removeAmenitiesFromMap(type:String){
        
        if(type == "clinic"){
            
            self.removeAmenitiesFromMap(type: "24Clinic")
            self.removeAmenitiesFromMap(type: "AnE")
        }
        navigationMapView?.mapView.removeAmenitiesLayerOfCluster(type: type)
        //navigationMapView?.mapView.removeAmenitiesLayerOfType(type: type)
    }
    
    func addAmenitiesToMap(_ featureCollection: FeatureCollection,type:String){
        
        DispatchQueue.main.async {
            self.removeAmenitiesFromMap(type: type)

            if(type == Values.TRAFFIC){
                
                self.navigationMapView?.mapView.addAmentiesToMapOfType(features: featureCollection, isBelowPuck: true, type: type)
            }
            else{
                
                self.navigationMapView?.mapView.addAmenitiesToMapOfTypeWithClustering(features: featureCollection, isBelowPuck: true, type: type)
            }
        }
    }
    
    func addPOIAmenitiesToMap(_ featureCollection: FeatureCollection){
        
        DispatchQueue.main.async {
            self.removeAmenitiesFromMap(type: Values.POI)
            
            self.navigationMapView?.mapView.addPOIAmentiesToMapOfType(features: featureCollection, isBelowPuck: true, type: Values.POI)
        }
    }
    
    func addBookMarkToMap(_ featureCollection: FeatureCollection) {
        DispatchQueue.main.async {
            self.removeAmenitiesFromMap(type: Values.COLLECTION)
            
            self.navigationMapView?.mapView.addPOIAmentiesToMapOfType(features: featureCollection, isBelowPuck: true, type: Values.POI)
        }
    }
    
    #if DEMO

    func updateAmenities(_ featureCollection: FeatureCollection) {
        navigationMapView?.mapView.removeAmenitiesLayer()
        navigationMapView?.mapView.addAmenitiesToMap(features: featureCollection, isBelowPuck: true)
    }

    func removeDemoIncidentsLayer() {
        navigationMapView?.mapView.removeDemoIncidentLayer()
    }
    
    func addDemoIncidentsLayer(features:Turf.FeatureCollection) {
        navigationMapView?.mapView.addDemoIncidentsToMap(features: features, isBelowPuck: false)
    }
    
    func removeDemoLeg2IncidentsLayer() {
        navigationMapView?.mapView.removeDemoLeg2IncidentLayer()
    }
    
    func addDemoLeg2IncidentsLayer(features:Turf.FeatureCollection) {
        navigationMapView?.mapView.addDemoLeg2IncidentsToMap(features: features, isBelowPuck: false)
    }    
    #endif
    
    func removeERPLayer() {
        navigationMapView?.mapView.removeERPCostLayer()
    }
    
    func addERPLayer(features:Turf.FeatureCollection,isNavigation:Bool = false) {
        navigationMapView?.mapView.addERPCostItems(features: features, isBelowPuck: true,isNavigation:isNavigation)
    }
    
    func addERPBoundLayer(features:Turf.FeatureCollection){
        
        navigationMapView?.mapView.addAllERPBoundItemsToMap(features: features, isBelowPuck: true)
    }
    
    func isERPLayerExists() -> Bool {
        
        return navigationMapView?.mapView.mapboxMap.style.layerExists(withId: ERPValues.ERPCost_Identifier_SymbolLayer) ?? false
    }

    /// Location update
    func updateLocation(_ location: CLLocation, provider: LocationProvider) {
//        navigationMapView?.mapView.location.locationProvider(provider, didUpdateLocations: [location])
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            
            self.navigationMapView?.moveUserLocation(to: location)
        }
    }

    func updateLocation(_ location: CLLocation) {
        navigationMapView?.moveUserLocation(to: location)
    }

    func overridePassiveLocationProvider(_ provider: PassiveLocationProvider) {
        navigationMapView?.mapView.location.overrideLocationProvider(with: provider)
    }
    
    func addProfileZones(){
    
        navigationMapView?.mapView.addProfileZones()
    }
    
    /// Update detected feature
    func showFeature(_ feature: FeatureDetectable) {
        guard let coordinate = navigationMapView?.mapView.location.latestLocation?.coordinate else {
            return
        }

        // only add once
        if let feature = feature as? DetectedSchoolZone {
            if case let .polygon(polygon) = feature.feature.geometry,
             let line = getNearestLine(coordinate: coordinate, polygon: polygon) {
                let midpoint = mid(line.coordinates[0], line.coordinates[1])
                navigationMapView?.mapView.addSymbol(id: feature.id, name: "SchoolZoneSymbol", coordinate: midpoint)
            }
        } else if let feature = feature as? DetectedSilverZone {
            if case let .polygon(polygon) = feature.feature.geometry,
             let line = getNearestLine(coordinate: coordinate, polygon: polygon) {
                let midpoint = mid(line.coordinates[0], line.coordinates[1])
                navigationMapView?.mapView.addSymbol(id: feature.id, name: "SilverZoneSymbol", coordinate: midpoint)
            }
        }

    }
    
    func updateCameraLayer(show: Bool) {
        
        let visible = show ? Visibility.visible : Visibility.none
        
        // illegal parking camera
        do {
            try navigationMapView?.mapView.mapboxMap.style.updateLayer(withId: Constants.Layer.illegalparkingcamera, type:SymbolLayer.self) { layer in
                layer.visibility = .constant(visible)
            }
            
            try navigationMapView?.mapView.mapboxMap.style.updateLayer(withId: Constants.Layer.speedcamera, type: SymbolLayer.self) { layer in
                layer.visibility = .constant(visible)
            }

            try navigationMapView?.mapView.mapboxMap.style.updateLayer(withId: Constants.Layer.redlightcamera, type: SymbolLayer.self) { layer  in
                layer.visibility = .constant(visible)
            }

        } catch (_) {
            
        }
    }
    
    func addSchoolZoneSymbol(_ feature: DetectedSchoolZone, at coordinate:CLLocationCoordinate2D) {
        if case let .polygon(polygon) = feature.feature.geometry,
         let line = getNearestLine(coordinate: coordinate, polygon: polygon) {
            let midpoint = mid(line.coordinates[0], line.coordinates[1])
            navigationMapView?.mapView.addSymbol(id: feature.id, name: "SchoolZoneSymbol", coordinate: midpoint)
        }
    }
    
    func addSchoolZoneSymbol(_ feature: DetectedSchoolZone) {
        guard let coordinate = navigationMapView?.mapView.location.latestLocation?.coordinate else {
            return
        }

        addSchoolZoneSymbol(feature, at: coordinate)
    }

    func addSilverZoneSymbol(_ feature: DetectedSilverZone, at coordinate:CLLocationCoordinate2D) {
        if case let .polygon(polygon) = feature.feature.geometry,
           let line = getNearestLine(coordinate: coordinate, polygon: polygon) {
            let midpoint = mid(line.coordinates[0], line.coordinates[1])
            navigationMapView?.mapView.addSymbol(id: feature.id, name: "SilverZoneSymbol", coordinate: midpoint)
        }
    }
    
    func addSilverZoneSymbol(_ feature: DetectedSilverZone) {
        guard let coordinate = navigationMapView?.mapView.location.latestLocation?.coordinate else {
            return
        }

        addSilverZoneSymbol(feature, at: coordinate)
    }
    
    /// Used to remove symbol for SchoolZone and SilverZone after feature detection
    func removeSymbol(_ id: String) {
        DispatchQueue.main.async {
            self.navigationMapView?.mapView.removeSymbol(id: id)
        }
    }
    
    /// setup the navigation camera to make it sink between mobile and CarPlay
    func setupNavigationCamera(isCarPlay: Bool = false) {
        if let viewportDataSource = (navigationMapView?.navigationCamera.viewportDataSource as? NavigationViewportDataSource) {
            if isCarPlay {
                viewportDataSource.options.followingCameraOptions.zoomRange = 16.5...16.5
            }else {
                viewportDataSource.options.followingCameraOptions.zoomRange = Values.defaultNavigationMinZoomLevel...Values.defaultNavigationMaxZoomLevel
                
                // Disable any updates to `CameraOptions.padding` in `NavigationCameraState.following` state
                // to prevent overlapping. [You can try out to see different behaviour between paddingUpdatesAllowed = true or false]
                viewportDataSource.options.followingCameraOptions.paddingUpdatesAllowed = false
                // TODO: Work around for iPhone 14 pro max only
//                let bottom = UIScreen.main.bounds.height == 932 ? -(UIScreen.main.bounds.height/2 + 40) : -UIScreen.main.bounds.height/2
                //  Add 102 for show carpark list view
                var bottom = -(UIScreen.main.bounds.height - 340 - 142) + 2 * UIApplication.bottomSafeAreaHeight
                if OBUHelper.shared.hasLastObuData() {
                    if OBUHelper.shared.isObuConnected() {
                        bottom = -(UIScreen.main.bounds.height - 440 - 132) + 2 * UIApplication.bottomSafeAreaHeight
                    } else {
                        bottom = -(UIScreen.main.bounds.height - 460 - 147) + 2 * UIApplication.bottomSafeAreaHeight
                    }
                }
                viewportDataSource.followingMobileCamera.padding = UIEdgeInsets(top: 0.0,
                                                                                left: 0.0,
                                                                                bottom: bottom,
                                                                                right: 0.0)
            }
            
            viewportDataSource.options.followingCameraOptions.pitchUpdatesAllowed = false
            if isCarPlay {
                viewportDataSource.followingCarPlayCamera.pitch = 50 // Your pitch here
            } else {
                viewportDataSource.followingMobileCamera.pitch = 50
                viewportDataSource.options.followingCameraOptions.pitchNearManeuver.enabled = false
            }
            
            //This fix is from here -> https://github.com/mapbox-collab/ncs-collab/issues/293
            viewportDataSource.options.followingCameraOptions.bearingSmoothing.maximumBearingSmoothingAngle = 30.0
        }
    }
    
    func addCarparks(_ carparks: [Carpark],showOnlyWithCostSymbol: Bool = false) {
        var annotations = [PointAnnotation]()
        carparks.forEach { carPark in
            if let annotation = self.addCarparkAnnotation(carpark: carPark) {
                annotations.append(annotation)
            }
        }
        
        self.navigationMapView?.pointAnnotationManager?.annotations = annotations
        
        if carparks.count > 0 {
            self.navigationMapView?.mapView.addCarparks(carparks, showOnlyWithCostSymbol:showOnlyWithCostSymbol)
        }
    }
    
    func removeCarParks(_ carparks: [Carpark]) {
        self.navigationMapView?.mapView.removeCarparkLayer()
        let annotations = [PointAnnotation]()
        
        self.navigationMapView?.pointAnnotationManager?.annotations = annotations
    }
    
    /// If need to append a destination icon, set the finalDestination here
    func addWalkingPath(route: Route, finalDestination: LocationCoordinate2D? = nil) {
        guard let shape = route.shape else { return }
        
        //  Try to remove the previous layer
        try? navigationMapView?.mapView.mapboxMap.style.removeLayer(withId: "line-layer")
        try? navigationMapView?.mapView.mapboxMap.style.removeSource(withId: "WalkingPathLine")
        
        let sourceIdentifier = "WalkingPathLine"
        // Create a GeoJSON data source.
        var routeLineSource = GeoJSONSource()
        routeLineSource.data = .feature(Turf.Feature(geometry: LineString(shape.coordinates)))
        
        // Create a line layer
        var lineLayer = LineLayer(id: "line-layer")
        lineLayer.source = sourceIdentifier
        lineLayer.lineColor = .constant(.init(#colorLiteral(red: 0.3647058823, green: 0.6549019607, blue: 1.0, alpha: 1)))
        lineLayer.lineWidth = .expression(lineWidthExpression(0.4))
        lineLayer.lineJoin = .constant(.round)
        lineLayer.lineCap = .constant(.square)
        lineLayer.lineDasharray = .constant([1.0, 1.0])

        // Add the lineLayer to the map.
        try? navigationMapView?.mapView.mapboxMap.style.addSource(routeLineSource, id: sourceIdentifier)
        try? navigationMapView?.mapView.mapboxMap.style.addPersistentLayer(lineLayer)
        
        // append a destination icon
        if let coordinate = finalDestination {
            var destinationAnnotation = PointAnnotation(coordinate: coordinate)
            destinationAnnotation.image = .init(image: UIImage(named: "destinationMark")!, name: "navigationDest")
            var annotations = navigationMapView?.pointAnnotationManager?.annotations ?? []
            annotations.append(destinationAnnotation)
            navigationMapView?.pointAnnotationManager?.annotations = annotations
        }
        
    }
    
    func removeWalkingPath(mapView: MapView) {
        
    }
    
    private func addCarparkAnnotation(carpark: Carpark) -> PointAnnotation? {

        if let carparkId = carpark.id, !carparkId.isEmpty {
            var annotation = PointAnnotation(id:carpark.id!,coordinate: CLLocationCoordinate2D(latitude: carpark.lat ?? 0, longitude: carpark.long ?? 0))
            annotation.image = getAnnotationImage(carpark: carpark, selected: false, ignoreDestination: true)
            annotation.userInfo = [
                "carpark": carpark
            ]
            guard let annoManager = navigationMapView?.pointAnnotationManager else { return annotation }
                
            let layerId = annoManager.layerId
            try? self.navigationMapView?.mapView.mapboxMap.style.setLayerProperty(for: layerId, property: "icon-allow-overlap", value: true)
            return annotation
        }
        return nil
    }

}

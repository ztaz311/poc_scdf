//
//  AmenitiesSharedInstance.swift
//  Breeze
//
//  Created by VishnuKanth on 28/12/21.
//

import Foundation
import UIKit
import StoreKit
import CoreLocation
import SwiftyBeaver
import Turf
import Combine
import MapboxNavigation
import MapboxDirections

struct Message : Codable {
    
    let profiles : [Profiles]?
    
    enum CodingKeys: String, CodingKey {
        case profiles = "profiles"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        profiles = try values.decodeIfPresent([Profiles].self, forKey: .profiles)
    }
    
}

struct Default_amenities : Codable {
    let type : String?
    let ids : [String]?
    
    enum CodingKeys: String, CodingKey {
        case type = "type"
        case ids = "ids"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        type = try values.decodeIfPresent(String.self, forKey: .type)
        ids = try values.decodeIfPresent([String].self, forKey: .ids)
    }
    
}

public protocol AmenitiesZoneDelegate: NSObjectProtocol {
    func didGetZoneColor()
}

struct AmenityClusteredColors : Codable {
    
    let clusteredValues : ClusteredValues?
    
    enum CodingKeys: String, CodingKey {
        
        case clusteredValues = "values"
    }
    
}

struct ClusteredValues : Codable {
    let cluster_colors : [Cluster_colors]?
    
    enum CodingKeys: String, CodingKey {
        
        case cluster_colors = "cluster_colors"
    }
    
}

struct Cluster_colors : Codable {
    let type : String?
    let clustered_color : String?
    
    enum CodingKeys: String, CodingKey {
        
        case type = "type"
        case clustered_color = "clustered_color"
    }
    
}

public class AmenitiesSharedInstance {
    
    private init() {
        
        //Empty intialisation
    }
    
    var requests = [AmenityRequest]()
    var amenitiesSymbolLayer = [String:Any]()
    
    var cruiseSymbolLayer = [String:Any]()
    var carplaySymbolLayer = [String:Any]()
    
    private var amenityService = AmenityService()
    var amenitiesPreference:NSArray?
    var profilePreference:Message?
    @Published var isDisplayAmenities = false
    @Published var isAdjustCarPark = false
    
    var isAmenitiesChanged = false
    ///These variables used only for Petrol and EVCharger in route planning screen
    var petrolrequests = [AmenityRequest]()
    var petrolSymbolLayers = [String:Any]() //NearOrigin
    var petrolLayersNearDestination = [String:Any]()
    var itemsToDisplay:KioskBase?
    var finalAmenitiesWithInSourceAndDestForPetrol = [String:Any]()
    
    var evRequests = [AmenityRequest]()
    var evItemsToDisplay:KioskBase?
    var evSymbolLayers = [String:Any]() //NearOrigin
    var evLayersNearDestination = [String:Any]()
    var finalAmenitiesWithInSourceAndDestForEv = [String:Any]()
    var amenityClusteredColors:AmenityClusteredColors?
    
    var turnbyturnPetrolOrEVSymbolLayers = [String:Any]()   //  Near user current location
    
    @Published var isDisplayPetrolAmenities = false
    @Published var isDisplayEVAmenities = false
    @Published var isUpdateCarParkLayer = false
    
    private var lastLocation: CLLocationCoordinate2D?
    
    private let zoneService: ZoneServiceProtocol = ZoneService()
    
    weak var amenitiesZoneDelegate: AmenitiesZoneDelegate?
    
    public static let shared = AmenitiesSharedInstance()
    
    /**
     Request UserPreferences from RN.
     */
    func requestAmenities(location:CLLocationCoordinate2D){
        
        if(requests.count > 0){
            requests.removeAll()
        }
        ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.FN_GET_LOCAL_USER_PREFERENCES, data: nil)
            .subscribe(onSuccess: { result in
                print("onSuccess: ", result!["data"] as? NSDictionary)
                let amenities = result!["data"] as? NSDictionary
                
                
                self.parseUserPreferences(preferences: amenities,location: location)
                
                
            },
                       onFailure: { error in
                
                //Not required to handle this
            })
    }
    
    /**
     Append Amenity API request to request variable and call Amenities API
     */
    func parseUserPreferences(preferences:NSDictionary?,location:CLLocationCoordinate2D){
        
        if(requests.count > 0){
            requests.removeAll()
        }
        
        guard let theJSONData = try? JSONSerialization.data(withJSONObject: preferences ?? [],
                                                            options: [.prettyPrinted]) else {
            return
        }
        
        do {
            let jsonString = String(decoding: theJSONData, as: UTF8.self)
            print(jsonString)
            profilePreference = try JSONDecoder().decode(Message.self, from: theJSONData)
        } catch let DecodingError.dataCorrupted(context) {
            print(context)
        } catch let DecodingError.keyNotFound(key, context) {
            print("Key '\(key)' not found:", context.debugDescription)
            print("codingPath:", context.codingPath)
        } catch let DecodingError.valueNotFound(value, context) {
            print("Value '\(value)' not found:", context.debugDescription)
            print("codingPath:", context.codingPath)
        } catch let DecodingError.typeMismatch(type, context)  {
            print("Type '\(type)' mismatch:", context.debugDescription)
            print("codingPath:", context.codingPath)
        } catch {
            print("error: ", error)
        }
        if let amenitiesPreference = preferences?["amenities_preference"] as? NSArray {
            self.amenitiesPreference = amenitiesPreference
        }
        
        for amenity in self.amenitiesPreference ?? []{
            
            guard let amenityDict = amenity as? NSDictionary else { return }
            
            guard let elementName = amenityDict["element_name"] as? String else { return }
            
            if(amenityDict.count > 0){
                
                let isSelected = amenityDict["is_selected"] as? Bool ?? false
                if(isSelected == true){
                    
                    if(elementName != Values.AMENITYCARPARK && elementName != Values.POI){
                        
                        print("Amenities Element Name",elementName)
                        var filter: AmenityFilter? = nil
                        
                        if elementName == Values.EV_CHARGER {
                            var plugTypes = [String]()
                            if let subItems = amenityDict["sub_items"] as? NSArray {
                                for subItem in subItems {
                                    if let dict = subItem as? NSDictionary,
                                       let element_name = dict["element_name"] as? String {
                                        if dict["is_selected"] as? Bool ?? false {
                                            plugTypes.append(element_name)
                                        }
                                    }
                                }
                            }
                            
                            if plugTypes.count > 0 {
                                filter = AmenityFilter(provider: nil, services: nil, plugTypes: plugTypes)
                            }
                            
                        } else if elementName == Values.PETROL {
                            var provider = [String]()
                            if let subItems = amenityDict["sub_items"] as? NSArray {
                                for subItem in subItems {
                                    if let dict = subItem as? NSDictionary,
                                       let element_name = dict["element_name"] as? String {
                                        if dict["is_selected"] as? Bool ?? false {
                                            provider.append(element_name)
                                        }
                                    }
                                }
                            }
                            
                            if provider.count > 0 {
                                filter = AmenityFilter(provider: provider, services: nil, plugTypes: nil)
                            }
                        }
                        
                        self.requests.append(AmenityRequest(type: elementName, filter: filter))
                    }
                    
                }
            }
            
            
        }
        
        if(self.requests.count > 0){
            
            self.callAmenitiesAPI(location: location)
        }
    }
    
    func reloadAmenityRequestInCaseUpdate() {
        if self.requests.count > 0,
           let location = lastLocation {
            
            self.callAmenitiesAPI(location: location)
        }
    }
    
    func reloadAmenityRequest(location:CLLocationCoordinate2D){
        
        if(requests.count > 0){
            requests.removeAll()
        }
        
        for amenity in self.amenitiesPreference ?? []{
            
            guard let amenityDict = amenity as? NSDictionary else { return }
            
            guard let elementName = amenityDict["element_name"] as? String else { return }
            
            if(amenityDict.count > 0){
                
                let isSelected = amenityDict["is_selected"] as? Bool ?? false
                if(isSelected == true){
                    
                    if(elementName != Values.AMENITYCARPARK && elementName != Values.POI){
                        
                        print("Amenities Element Name",elementName)
                        var filter: AmenityFilter? = nil
                        
                        if elementName == Values.EV_CHARGER {
                            var plugTypes = [String]()
                            if let subItems = amenityDict["sub_items"] as? NSArray {
                                for subItem in subItems {
                                    if let dict = subItem as? NSDictionary,
                                       let element_name = dict["element_name"] as? String{
                                        if dict["is_selected"] as? Bool ?? false {
                                            plugTypes.append(element_name)
                                        }
                                    }
                                }
                            }
                            
                            if plugTypes.count > 0 {
                                filter = AmenityFilter(provider: nil, services: nil, plugTypes: plugTypes)
                            }
                            
                        } else if elementName == Values.PETROL {
                            var provider = [String]()
                            if let subItems = amenityDict["sub_items"] as? NSArray {
                                for subItem in subItems {
                                    if let dict = subItem as? NSDictionary,
                                       let element_name = dict["element_name"] as? String {
                                        if dict["is_selected"] as? Bool ?? false {
                                            provider.append(element_name)
                                        }
                                    }
                                }
                            }
                            
                            if provider.count > 0 {
                                filter = AmenityFilter(provider: provider, services: nil, plugTypes: nil)
                            }
                        }
                        
                        self.requests.append(AmenityRequest(type: elementName, filter: filter))
                    }
                    
                }
            }
            
            
        }
        
        if(self.requests.count > 0){
            
            self.callAmenitiesAPI(location: location)
        }
    }
    
    /**
     Get all the image map symbol layer images and add to the respective map style
     */
    
    func loadAllImagesToTheMapStyle(navMapView:NavigationMapView){
        
        guard let amenitiesPreference = amenitiesPreference else {
            return
        }
        
        for preference in amenitiesPreference {
            
            guard let amenityDict = preference as? NSDictionary else { return }
            
            guard let elementName = amenityDict["element_name"] as? String else { return }
            //            let elementName = amenityDict["element_name"] as! String
            /*
             let map_icon_light_selected_url = amenityDict!["map_icon_light_selected_url"] as! String
             navMapView.mapView.removeExistingAmenityImages(type: "\(Values.LIGHT_MODE_SELECTED)\(elementName)")
             navMapView.mapView.addImagesToMapStyle(type: "\(Values.LIGHT_MODE_SELECTED)\(elementName)", urlString: map_icon_light_selected_url)
             
             let map_icon_light_unselected_url = amenityDict!["map_icon_light_unselected_url"] as! String
             navMapView.mapView.removeExistingAmenityImages(type: "\(Values.LIGHT_MODE_UN_SELECTED)\(elementName)")
             navMapView.mapView.addImagesToMapStyle(type: "\(Values.LIGHT_MODE_UN_SELECTED)\(elementName)", urlString: map_icon_light_unselected_url)
             
             
             let map_icon_dark_selected_url = amenityDict!["map_icon_dark_selected_url"] as! String
             navMapView.mapView.removeExistingAmenityImages(type: "\(Values.DARK_MODE_SELECTED)\(elementName)")
             navMapView.mapView.addImagesToMapStyle(type: "\(Values.DARK_MODE_SELECTED)\(elementName)", urlString: map_icon_dark_selected_url)
             
             let map_icon_dark_unselected_url = amenityDict!["map_icon_dark_unselected_url"] as! String
             navMapView.mapView.removeExistingAmenityImages(type: "\(Values.DARK_MODE_UN_SELECTED)\(elementName)")
             navMapView.mapView.addImagesToMapStyle(type: "\(Values.DARK_MODE_UN_SELECTED)\(elementName)", urlString: map_icon_dark_unselected_url)
             */
            
            if(elementName == Values.POI){
                
                //Downloading poi sub items images and adding to the map style
                guard  let subItems = amenityDict["sub_items"] as? NSArray else { return }
                if let profilePreference = self.profilePreference, let profile = profilePreference.profiles?.first,let poiAmenities = profile.amenities{
                    
                    for poiAmenity in poiAmenities{
                        
                        for subItem in subItems {
                            let dict = subItem as! NSDictionary
                            let subElementName = dict["element_name"] as! String
                            if subElementName == poiAmenity{
                                
                                let map_icon_light_unselected_url = dict["map_icon_light_unselected_url"] as! String
                                let map_icon_dark_unselected_url = dict["map_icon_dark_unselected_url"] as! String
                                navMapView.mapView.removeExistingAmenityImages(type: "\(subElementName)")
                                navMapView.mapView.addDynamicImagesToMapStyle(type: "\(subElementName)", urlString: map_icon_light_unselected_url, darkModeUrlString: map_icon_dark_unselected_url)
                                
                                let map_icon_light_selected_url = dict["map_icon_light_selected_url"] as! String
                                let map_icon_dark_selected_url = dict["map_icon_dark_selected_url"] as! String
                                navMapView.mapView.removeExistingAmenityImages(type: "\(subElementName)-selected")
                                navMapView.mapView.addDynamicImagesToMapStyle(type: "\(subElementName)-selected", urlString: map_icon_light_selected_url, darkModeUrlString: map_icon_dark_selected_url)
                            }
                        }
                    }
                    
                }
                
            }
            else{
                
                //Downloading images from imageTypes if any
                if let imageTypes = amenityDict["imageTypes"] as? NSArray {
                    
                    for imageType in imageTypes {
                        
                        let dict = imageType as! NSDictionary
                        let imageName = dict["imageType"] as! String
                        let map_icon_light_unselected_url = dict["map_icon_light_unselected_url"] as! String
                        let map_icon_dark_unselected_url = dict["map_icon_dark_unselected_url"] as! String
                        navMapView.mapView.removeExistingAmenityImages(type: "\(imageName)")
                        navMapView.mapView.addDynamicImagesToMapStyle(type: "\(imageName)", urlString: map_icon_light_unselected_url, darkModeUrlString: map_icon_dark_unselected_url)
                        
                        let map_icon_light_selected_url = dict["map_icon_light_selected_url"] as! String
                        let map_icon_dark_selected_url = dict["map_icon_dark_selected_url"] as! String
                        navMapView.mapView.removeExistingAmenityImages(type: "\(imageName)-selected")
                        navMapView.mapView.addDynamicImagesToMapStyle(type: "\(imageName)-selected", urlString: map_icon_light_selected_url, darkModeUrlString: map_icon_dark_selected_url)
                    }
                    
                }
                else{
                    
                    let map_icon_light_unselected_url = amenityDict["map_icon_light_unselected_url"] as! String
                    let map_icon_dark_unselected_url = amenityDict["map_icon_dark_unselected_url"] as! String
                    navMapView.mapView.removeExistingAmenityImages(type: "\(elementName)")
                    navMapView.mapView.addDynamicImagesToMapStyle(type: "\(elementName)", urlString: map_icon_light_unselected_url, darkModeUrlString: map_icon_dark_unselected_url)
                    
                    let map_icon_light_selected_url = amenityDict["map_icon_light_selected_url"] as! String
                    let map_icon_dark_selected_url = amenityDict["map_icon_dark_selected_url"] as! String
                    navMapView.mapView.removeExistingAmenityImages(type: "\(elementName)-selected")
                    navMapView.mapView.addDynamicImagesToMapStyle(type: "\(elementName)-selected", urlString: map_icon_light_selected_url, darkModeUrlString: map_icon_dark_selected_url)
                }
                
            }
        }
        
        navMapView.mapView.removeExistingAmenityImages(type: Values.TRAFFIC)
        navMapView.mapView.addTrafficImagesToMapStyle(type: Values.TRAFFIC)
    }
    
    /*
     
     */
    func callZoneColor(profileName: String) {
        DispatchQueue.global(qos: .background).async {
            self.zoneService.getZoneCongestionDetail(profileName: profileName) { [weak self] result in
                switch result {
                case .success(let zoneColors):
                    SelectedProfiles.shared.setZoneColors(zoneColors: zoneColors)
                    guard let self = self else {return }
                    self.amenitiesZoneDelegate?.didGetZoneColor()
                    break
                case .failure(let error):
                    SwiftyBeaver.error("Failed to getAmenities: \(error.localizedDescription)")
                }
            }
        }
    }
    
    /**
     fetch each amenity type clustered colors into a model class
     */
    func updateAmenityClusteredColors() {
        
        var clusteredColors = [Cluster_colors]()
        guard let amenitiesPreference = self.amenitiesPreference else {
            return
        }
        
        for preference in amenitiesPreference {
            
            guard let amenityDict = preference as? NSDictionary else { return }
            
            guard let elementName = amenityDict["element_name"] as? String else { return }
            
            if(elementName != Values.POI){
                
                guard let clusterColor = amenityDict["cluster_color"] as? String else { return }
                if let imageTypes = amenityDict["imageTypes"] as? NSArray {
                    
                    for imageType in imageTypes {
                        
                        let dict = imageType as! NSDictionary
                        let imageName = dict["imageType"] as! String
                        clusteredColors.append(Cluster_colors(type: imageName, clustered_color: clusterColor))
                        
                    }
                }
                else{
                    
                    clusteredColors.append(Cluster_colors(type: elementName, clustered_color: clusterColor))
                }
                
                
                print("AMENITY CLUSTERED COLORS INSIDE",clusteredColors)
            }
            
            
        }
        
        amenityClusteredColors = AmenityClusteredColors(clusteredValues: ClusteredValues(cluster_colors: clusteredColors))
        print("AMENITY CLUSTERED COLORS",amenityClusteredColors)
    }
    
    /**
     fetch each color of type and convert to UIColot
     We will call this method from class Amenity Updater for clustering
     */
    func fetchClusteredColor(type:String) -> UIColor? {
        
        print("AMENITY CLUSTERED COLORS",amenityClusteredColors)
        guard let amenityClusteredColors = amenityClusteredColors, let clusteredValues = amenityClusteredColors.clusteredValues, let clusterColors = clusteredValues.cluster_colors else {
            return nil
        }
        
        
        for clusteredColor in clusterColors{
            
            if type == clusteredColor.type{
                
                guard let hexColor = clusteredColor.clustered_color else { return nil }
                return UIColor(hexString: hexColor,alpha:1.0)
            }
        }
        
        return nil
    }
    /**
     Amenities API method
     fetching amenities Data
     Adding publish event isDisplayAmenities to display amenities data on to map
     */
    func callAmenitiesAPI(location: CLLocationCoordinate2D, carparkId: String? = nil) {
        
        lastLocation = location
        
        if(self.requests.count > 0){
            
//            let request = AmenitiesRequest(latitude: location.latitude, longitude: location.longitude, radius: SelectedProfiles.shared.selectedProfileName() != "" ? Double(SelectedProfiles.shared.getOuterZoneRadius()/1000):2, arrivalTime: 1, destinationName: "", amenities: self.requests,resultCount: nil,maxRadius: nil, isVoucher: false, carparkId: carparkId)
            
            let evPetrolCount = UserSettingsModel.sharedInstance.evPetrolCount
            let evPetrolRange = Double(UserSettingsModel.sharedInstance.evPetrolRange) / 1000
            let maxEVPetrolRange = Double(UserSettingsModel.sharedInstance.maxEVPetrolRange) / 1000
            
            let request = AmenitiesRequest(latitude: location.latitude, longitude: location.longitude, radius: evPetrolRange, arrivalTime: 1, destinationName: "", amenities: self.requests,resultCount: evPetrolCount, maxRadius: maxEVPetrolRange, isVoucher: false, carparkId: carparkId)
            
            if !self.amenitiesSymbolLayer.isEmpty {
                self.amenitiesSymbolLayer.removeAll()
            }
            
            self.amenityService.getAmenitiesFromV2(request: request) { result in
                switch result {
                case .failure(let error):
                    SwiftyBeaver.error("Failed to getAmenities: \(error.localizedDescription)")
                case .success(let amenities):
                    guard let amenitiesPreference = self.amenitiesPreference else {
                        return
                    }
                    print(amenities)
                    
                    var mapImageURL = ""
                    var lat = 0.0
                    var long = 0.0
                    var type = ""
                    
                    for amenity in amenities.amenities ?? [] {
                        mapImageURL = ""
                        lat = 0.0
                        long = 0.0
                        type = ""
                        
                        var array = [Any]()
                        
                        for preference in amenitiesPreference {
                            
                            guard let amenityDict = preference as? NSDictionary else { return }
                            
                            guard let elementName = amenityDict["element_name"] as? String else { return }
                            
                            if elementName == amenity.type {
                                
                                type = elementName
                                let clusterID = amenityDict["cluster_id"] as? Int ?? -1
                                let clusterColor = (amenityDict["cluster_color"] as? String) ?? ""
                                mapImageURL = (amenityDict["map_icon_light_selected_url"] as? String) ?? ""
                                
                                for amenityData in amenity.data ?? [] {
                                    
                                    if let coordinates = amenityData.location?.coordinates, coordinates.count >= 2 {
                                        long = coordinates[0]
                                        lat = coordinates[1]
                                    }
                                    
                                    //This is applicable to only the data that return image types. at the moment for 24hr clinic
                                    let imageType = amenityData.imageType ?? ""
                                    if (imageType != "") {
                                        type = imageType
                                        //Since we are taking imageType as the key for some of the amenities we should clear the array to avoid duplicate values
                                        if !self.amenitiesSymbolLayer.keys.contains(type){
                                            array = [Any]()
                                        }
                                    }
                                    
                                    let typeName = amenityData.name ?? ""
                                    let address = amenityData.address ?? ""
                                    let amenityId = amenityData.id ?? ""
                                    let poiID = amenityData.id ?? ""
                                    let providerName = amenityData.provider ?? ""
                                    let startDate = Double(amenityData.startDate ?? -1)
                                    let endDate = Double(amenityData.endDate ?? -1)
                                    let bookmarkId = amenityData.bookmarkId ?? -1
                                    let isBookmarked = amenityData.isBookmarked ?? false
                                    let textAdditionalInfo = amenityData.additionalInfo?.text
                                    let styleLightColor = amenityData.additionalInfo?.style?.lightColor?.color
                                    let styleDarkColor = amenityData.additionalInfo?.style?.darktColor?.color
                                    
                                    let amenties = ["lat":lat,
                                                    "long":long,
                                                    "mapImageURL":mapImageURL,
                                                    "name":typeName,
                                                    "address":address,
                                                    "id": amenityId,
                                                    "distance":amenityData.distance ?? 0,
                                                    "startDate": startDate,
                                                    "endDate": endDate,
                                                    "clusterID":clusterID,
                                                    "clusterColor":clusterColor,
                                                    "poiID": poiID,
                                                    "providerName":providerName,
                                                    "bookmarkId": bookmarkId,
                                                    "isBookmarked": isBookmarked,
                                                    "textAdditionalInfo": textAdditionalInfo ?? "",
                                                    "styleLightColor": styleLightColor ?? "",
                                                    "styleDarkColor": styleDarkColor ?? "",
                                                    "originalType":elementName] as [String : Any]
                                    
                                    array.append(amenties)
                                    
                                    if self.amenitiesSymbolLayer.keys.contains(type) {
                                        self.amenitiesSymbolLayer.updateValue(array, forKey: type)
                                    } else {
                                        self.amenitiesSymbolLayer[type] = array
                                    }
                                }
                                break
                            }
                        }
                    }
                    
                    if !self.isUpdateCarParkLayer {
                        self.isUpdateCarParkLayer = true
                    }
                    
                    self.isDisplayAmenities = true  //  Update this to handle empty petrol/EV alert
                    if !self.amenitiesSymbolLayer.isEmpty {
//                        self.isDisplayAmenities = true
                    } else{
                        self.isAdjustCarPark = true
                    }
                }
            }
        } else {
            if !self.amenitiesSymbolLayer.isEmpty {
                self.amenitiesSymbolLayer.removeAll()
                self.isAdjustCarPark = true
                self.isUpdateCarParkLayer = false
            } else {
                self.isUpdateCarParkLayer = false
            }
        }
    }
    
    /**
     Checking selected amenity is a proile object or not
     */
    func checkAmenityProfile(selectedAmenity:String) -> Bool {
        
        guard let profilePreference = profilePreference else {
            return false
        }
        
        
        for profile in profilePreference.profiles ?? [] {
            
            if(profile.element_name == selectedAmenity){
                
                return true
            }
        }
        
        return false
    }
    
    /**
     Keep Profile data in SelectedProfile singleton class
     */
    func addProfileName(profileName:String) -> Bool {
        
        guard let profilePreference = profilePreference else {
            return false
        }
        
        for profile in profilePreference.profiles ?? [] {
            
            if(profile.element_name == profileName){
                SelectedProfiles.shared.selectProfile(name: profileName)
                return true
            }
        }
        return false
    }
    
    
    func addProfileData() {
        guard let profilePreference = profilePreference else {
            return
        }
        
        for profile in profilePreference.profiles ?? [] {
            if profile.isActive ?? false {
                if let zones = profile.zones?.sorted(by: { $0.radius < $1.radius }) {
                    SelectedProfiles.shared.setInnerZone(zone: zones.first)
                    SelectedProfiles.shared.setOuterZone(zone: zones.last)
                    SelectedProfiles.shared.setAllZones(allZones: zones)
                }
                
                if let seasonParkings = profile.season_parking {
                    SelectedProfiles.shared.setSeasonParkings(seasonParkings)
                }
                
                if let toolTipRadius = profile.tooltip_radius{
                    SelectedProfiles.shared.setToolTipRadius(radius: toolTipRadius)
                }
            }
        }
        self.callZoneColor(profileName: getDefaultProfileName() ?? "")
    }
    
    func getDefaultProfileName() -> String? {
        if let profile = profilePreference?.profiles?.first {
            return profile.element_name
        }
        return nil
    }
    
    /**
     Checking selected amenity with original amenities preference sub items to send filter items to API
     */
    func addRequestForSelectedAmenity(location:CLLocationCoordinate2D,selectedAmenity:String){
        
        guard let amenitiesPreference = amenitiesPreference else {
            return
        }
        
        for amenity in amenitiesPreference {
            
            let amenityDict = amenity as? NSDictionary
            
            if(amenityDict?.count ?? 0 > 0){
                
                let elementName = amenityDict!["element_name"] as! String
                
                var filter: AmenityFilter? = nil
                
                if (selectedAmenity == elementName) {
                    if let subItems = amenityDict!["sub_items"] as? NSArray {
                        switch elementName {
                        case Values.EV_CHARGER:
                            var plugTypes = [String]()
                            
                            for subItem in subItems {
                                let dict = subItem as! NSDictionary
                                if dict["is_selected"] as? Bool ?? false {
                                    plugTypes.append(dict["element_name"] as! String)
                                }
                            }
                            if plugTypes.count > 0 {
                                filter = AmenityFilter(provider: nil, services: nil, plugTypes: plugTypes)
                            }
                        case Values.PETROL:
                            var provider = [String]()
                            let subItems = amenityDict!["sub_items"] as! NSArray
                            for subItem in subItems {
                                let dict = subItem as! NSDictionary
                                if dict["is_selected"] as? Bool ?? false {
                                    provider.append(dict["element_name"] as! String)
                                }
                            }
                            if provider.count > 0 {
                                filter = AmenityFilter(provider: provider, services: nil, plugTypes: nil)
                            }
                        case Values.POI:
                            
                            if let profilePreference = self.profilePreference, let profile = profilePreference.profiles?.first,let poiAmenities = profile.amenities{
                                
                                var provider = [String]()
                                let subItems = amenityDict!["sub_items"] as! NSArray
                                
                                // check poi Amenity in the profile preferences and send only those amenities to API as provide
                                for poiAmenity in poiAmenities{
                                    
                                    for subItem in subItems {
                                        let dict = subItem as! NSDictionary
                                        let subElementName = dict["element_name"] as! String
                                        if subElementName == poiAmenity{
                                            
                                            provider.append(dict["element_name"] as! String)
                                        }
                                        
                                    }
                                    if provider.count > 0 {
                                        filter = AmenityFilter(provider: provider, services: nil, plugTypes: nil)
                                    }
                                }
                                
                            }
                            
                        default:
                            break
                        }
                        
                        self.requests.append(AmenityRequest(type: elementName, filter: filter))
                    }
                }
            }
        }
        
        if(self.requests.count > 0){
            self.callAmenitiesAPI(location: location)
        }
    }
    
    /**
     Add more amenities method
     fetching amenities Data for a particulat amenity when selected from Bottom sheet in MapLanding screen
     */
    func addMoreAmenities(amenityName:String,location:CLLocationCoordinate2D){
        
        self.requests.append(AmenityRequest(type: amenityName, filter: nil))
        
        self.callAmenitiesAPI(location: location)
        
    }
    
    /**
     remove added amenities
     refreshing amenities Data when an amenity is unselected from Bottom sheet in MapLanding screen
     */
    func removeAddedAmenties(amenityName:String,location:CLLocationCoordinate2D){
        
        if let index = requests.firstIndex(where: {$0.type == amenityName}) {
            requests.remove(at: index)
            self.callAmenitiesAPI(location: location)
        }
    }
    
    /**
     removing specific amenity from added request
     */
    func removeSpecificAmenity(amenityName:String){
        
        if let index = requests.firstIndex(where: {$0.type == amenityName}) {
            requests.remove(at: index)
        }
    }
    
    /**
     Thsi is called from Routeplanning controller
     To display Petrol in bottom sheet of Route Planning when click on the item
     */
    func displayPetrolKioskItems(){
        
        var subItems = [Sub_items]()
        for amenity in self.amenitiesPreference ?? [] {
          
            guard let amenityDict = amenity as? NSDictionary else { return }
            
            guard let elementName = amenityDict["element_name"] as? String else { return }
            
            guard let elementID = amenityDict["element_id"] as? String else { return }
            
            var filters = [String]()
            //let elementName = amenityDict!["element_name"] as! String
            if(elementName == Values.PETROL){
                
                let subItemsArray = amenityDict["sub_items"] as? NSArray
                for sub_item in subItemsArray ?? [] {
                    
                    let subItemDict = sub_item as? NSDictionary
                    let elementName = subItemDict!["element_name"] as! String
                    let subElementId = subItemDict!["element_id"] as! String
                    let isSubItemSelected = subItemDict!["is_selected"] as! Bool
                    let displayText = subItemDict!["display_text"] as! String
                    filters.append(elementName)
                    
                    let subItem = Sub_items(element_name: elementName, count: 0, display_text:displayText, is_selected: isSubItemSelected,activeImageURL: "",inActiveImageURL: "",element_id: subElementId)
                    subItems.append(subItem)
                }
                
                self.itemsToDisplay = KioskBase(element_name: elementName,element_id: elementID, sub_items: subItems)
                if(petrolrequests.count > 0){
                    self.petrolrequests.removeAll()
                }
                self.petrolrequests.append(AmenityRequest(type: elementName, filter:AmenityFilter(provider: filters, services: nil, plugTypes: nil)))
                
                break
                
            }
        }
        
    }
    
    /**
     Petrol API request from Routeplanning controller for Destination coordinate
     
     */
    func callAmenitiesForPetrolForDestination(location:CLLocationCoordinate2D){
        
        if(self.petrolrequests.count > 0){
            
            let evPetrolRange = Double(UserSettingsModel.sharedInstance.evPetrolRange) / 1000
            
            let request = AmenitiesRequest(latitude: location.latitude, longitude: location.longitude, radius: evPetrolRange, arrivalTime: 1, destinationName: "", amenities: petrolrequests, resultCount: 1000,maxRadius: nil, isVoucher: false, carparkId: nil)
            
            self.amenityService.getAmenitiesFromV2(request: request) { result in
                switch result {
                case .failure(let error):
                    SwiftyBeaver.error("Failed to getAmenities: \(error.localizedDescription)")
                case .success(let amenities):
                    
                    print(amenities)
                    var mapImageURL = ""
                    var lat = 0.0
                    var long = 0.0
                    var type = ""
                    //var name = ""
                    var subItems = self.itemsToDisplay?.sub_items ?? []
                    
                    guard let amenitiesPreference = self.amenitiesPreference else {
                        return
                    }
                    
                    for amenity in amenities.amenities ?? []{
                        
                        mapImageURL = ""
                        lat = 0.0
                        long = 0.0
                        type = ""
                        //name = ""
                        var array = [Any]()
                        for preference in amenitiesPreference {
                            
                            guard let amenityDict = preference as? NSDictionary else { return }
                            
                            guard let elementName = amenityDict["element_name"] as? String else { return }
                            
                            guard let elementId = amenityDict["element_id"] as? String else { return }
                            
                            if elementName == amenity.type{
                                type = elementName
                                mapImageURL = amenityDict["map_icon_light_selected_url"] as! String
                                let subItemsArray = amenityDict["sub_items"] as? NSArray
                                
                                for amenityData in amenity.data ?? [] {
                                    
                                    //var count = 0
                                    
                                    for sub_item in subItemsArray ?? [] {
                                        
                                        
                                        let subItemDict = sub_item as? NSDictionary
                                        let elementName = subItemDict!["element_name"] as! String
                                        let isSubItemSelected = subItemDict!["is_selected"] as! Bool
                                        let subElementID = subItemDict!["element_id"] as! String
                                        if(elementName == amenityData.provider){
                                            
                                            //                                            count = count + 1
                                            long =   amenityData.location?.coordinates?[0] ?? 0
                                            
                                            lat =   amenityData.location?.coordinates?[1] ?? 0
                                            
                                            let typeName = amenityData.provider ?? ""
                                            let address = amenityData.address ?? ""
                                            let stationName = amenityData.stationname ?? ""
                                            let amenityId = amenityData.id ?? ""
                                            let displayText = subItemDict!["display_text"] as! String
                                            
                                            /// Check if same element is already present in subItems array
                                            if let row = subItems.firstIndex(where: {$0.element_name == typeName}) {
                                                
                                                //                                                let prevCount = subItems[row].count ?? 0
                                                //                                                count =  count + prevCount
                                                let subItem = Sub_items( element_name: typeName,count:0, display_text: displayText, is_selected: isSubItemSelected,activeImageURL: "",inActiveImageURL: "",element_id: subElementID)
                                                subItems[row] = subItem
                                            }
                                            else{
                                                
                                                let subItem = Sub_items( element_name: typeName,count:0, display_text: displayText, is_selected: isSubItemSelected,activeImageURL: "",inActiveImageURL: "",element_id: subElementID)
                                                subItems.append(subItem)
                                            }
                                            
                                            
                                            let amenties = ["lat":lat,"long":long,"mapImageURL":mapImageURL,"name":typeName,"address":address,"stationName":stationName,"id":amenityId,"startDate": -1.0, "endDate": -1.0] as [String : Any]
                                            
                                            array.append(amenties)
                                            
                                            
                                            if self.petrolLayersNearDestination.keys.contains(type){
                                                
                                                self.petrolLayersNearDestination.updateValue(array, forKey: type)
                                            }
                                            else{
                                                
                                                self.petrolLayersNearDestination[type] = array
                                            }
                                            
                                            
                                        }
                                        else{
                                            
                                            
                                        }
                                        
                                        
                                    }
                                    
                                }
                                
                                
                                
                                self.itemsToDisplay = KioskBase(element_name: elementName,element_id: elementId, sub_items: subItems)
                            }
                        }
                        
                    }
                    
                    if(self.petrolLayersNearDestination.count > 0 || self.petrolSymbolLayers.count > 0){
                        self.findNearest3ItemsFromPetrol()
                        //                        print(self.petrolAndEVSymbolLayers)
                        //                        print(self.itemsToDisplay?.sub_items)
                        //                        self.isDisplayPetrolEVAmenities = true
                        
                    }
                    
                }
            }
        }
        
    }
    
    /**
     Petrol  API request from Routeplanning controller for Origin coordinate
     */
    func callAmenitiesApiForPetrol(location:CLLocationCoordinate2D,destination:CLLocationCoordinate2D){
        
        if(self.petrolrequests.count > 0){
            
            let evPetrolRange = Double(UserSettingsModel.sharedInstance.evPetrolRange) / 1000
            
            let request = AmenitiesRequest(latitude: location.latitude, longitude: location.longitude, radius: evPetrolRange, arrivalTime: 1, destinationName: "", amenities: petrolrequests,resultCount: 1000,maxRadius: nil, isVoucher: false, carparkId: nil)
            
            self.amenityService.getAmenitiesFromV2(request: request) { result in
                switch result {
                case .failure(let error):
                    SwiftyBeaver.error("Failed to getAmenities: \(error.localizedDescription)")
                case .success(let amenities):
                    
                    print(amenities)
                    var mapImageURL = ""
                    var lat = 0.0
                    var long = 0.0
                    var type = ""
                    //var name = ""
                    var subItems = self.itemsToDisplay?.sub_items ?? []
                    for amenity in amenities.amenities ?? []{
                        
                        mapImageURL = ""
                        lat = 0.0
                        long = 0.0
                        type = ""
                        //name = ""
                        var array = [Any]()
                        for preference in self.amenitiesPreference ?? []{
                            
                            guard let amenityDict = preference as? NSDictionary else { return }
                            
                            guard let elementName = amenityDict["element_name"] as? String else { return }
                            
                            guard let elementId = amenityDict["element_id"] as? String else { return }
                            
                            if elementName == amenity.type{
                                
                                type = elementName
                                mapImageURL = amenityDict["map_icon_light_selected_url"] as! String
                                let subItemsArray = amenityDict["sub_items"] as? NSArray
                                
                                for amenityData in amenity.data ?? [] {
                                    
                                    //var count = 0
                                    for sub_item in subItemsArray ?? [] {
                                        
                                        let subItemDict = sub_item as? NSDictionary
                                        let elementName = subItemDict!["element_name"] as! String
                                        let isSubItemSelected = subItemDict!["is_selected"] as! Bool
                                        let subElementID = subItemDict!["element_id"] as! String
                                        let typeName = amenityData.provider ?? ""
                                        let address = amenityData.address ?? ""
                                        let amenityId = amenityData.id ?? ""
                                        let stationName = amenityData.stationname ?? ""
                                        let displayText = subItemDict!["display_text"] as! String
                                        
                                        
                                        if(elementName == amenityData.provider){
                                            
                                            //count = count + 1
                                            long =   amenityData.location?.coordinates?[0] ?? 0
                                            
                                            lat =   amenityData.location?.coordinates?[1] ?? 0
                                            
                                            /// Check if same element is already present in subItems array
                                            if let row = subItems.firstIndex(where: {$0.element_name == typeName}) {
                                                
                                                //                                                let prevCount = subItems[row].count ?? 0
                                                //                                                count =  count + prevCount
                                                let subItem = Sub_items( element_name: typeName,count:0, display_text: displayText, is_selected: isSubItemSelected,activeImageURL: "",inActiveImageURL: "",element_id: subElementID)
                                                subItems[row] = subItem
                                            }
                                            else{
                                                
                                                let subItem = Sub_items( element_name: typeName,count:0, display_text: displayText, is_selected: isSubItemSelected,activeImageURL: "",inActiveImageURL: "",element_id: subElementID)
                                                subItems.append(subItem)
                                            }
                                            
                                            
                                            
                                            let amenties = ["lat":lat,"long":long,"mapImageURL":mapImageURL,"name":typeName,"address":address,"stationName":stationName,"id":amenityId,"startDate": -1.0, "endDate": -1.0] as [String : Any]
                                            
                                            array.append(amenties)
                                            
                                            
                                            if self.petrolSymbolLayers.keys.contains(type){
                                                
                                                self.petrolSymbolLayers.updateValue(array, forKey: type)
                                            }
                                            else{
                                                
                                                self.petrolSymbolLayers[type] = array
                                            }
                                            
                                            
                                        }
                                        
                                        
                                    }
                                    
                                }
                                
                                
                                self.itemsToDisplay = KioskBase(element_name: elementName,element_id: elementId, sub_items: subItems)
                            }
                        }
                        
                    }
                    
                    self.callAmenitiesForPetrolForDestination(location: destination)
                    //                    if(self.petrolAndEVSymbolLayers.count > 0){
                    //
                    //                        self.isDisplayPetrolEVAmenities = true
                    //                    }
                    
                }
            }
        }
        
    }
    
    /**
     If none of the item is selected from Bottom Sheet for Petrol and, display 3 items from origin and 3 from destination
     */
    func findNearest3ItemsFromPetrol(){
        
        var subItems = self.itemsToDisplay?.sub_items ?? []
        var elementId = self.itemsToDisplay?.element_id ?? ""
        //var newSubItems = [Sub_items]()
        if(finalAmenitiesWithInSourceAndDestForPetrol.count > 0){
            finalAmenitiesWithInSourceAndDestForPetrol.removeAll()
        }
        if(self.petrolSymbolLayers.count > 0){
            
            let allKeys = self.petrolSymbolLayers.keys
            
            for key in allKeys {
                
                var locationArray = [Any]()
                
                if self.petrolSymbolLayers.keys.contains(key){
                    
                    let array = AmenitiesSharedInstance.shared.petrolSymbolLayers[key] as! [Any]
                    
                    for item in array {
                        
                        var count = 0
                        let itemDict = item as! [String:Any]
                        let name = itemDict["name"] as! String
                        
                        let lat = itemDict["lat"] as! Double
                        let long = itemDict["long"] as! Double
                        let address = itemDict["address"] as! String
                        let stationName = itemDict["stationName"] as! String
                        let amenityId = itemDict["id"] as! String
                        let imageURL = itemDict["mapImageURL"] as! String
                        
                        let amenties = ["lat":lat,"long":long,"mapImageURL":imageURL,"name":name,"address":address,"stationName":stationName,"id":amenityId] as [String : Any]
                        
                        /// Check if same element is already present in subItems array
                        
                        if let row = subItems.firstIndex(where: {$0.element_name == name}) {
                            
                            let prevCount = subItems[row].count ?? 0
                            count =  count + 1 + prevCount
                            let subItem = Sub_items( element_name: subItems[row].element_name,count:count, display_text: subItems[row].display_text, is_selected: subItems[row].is_selected,activeImageURL: "",inActiveImageURL: "",element_id: subItems[row].element_id)
                            subItems[row] = subItem
                        }
                        
                        // if(locationArray.count < 3){
                        
                        locationArray.append(amenties)
                        
                        if self.finalAmenitiesWithInSourceAndDestForPetrol.keys.contains(key){
                            
                            self.finalAmenitiesWithInSourceAndDestForPetrol.updateValue(locationArray, forKey: key)
                        }
                        else{
                            
                            self.finalAmenitiesWithInSourceAndDestForPetrol[key] = locationArray
                        }
                        
                        //}
                        
                    }
                }
            }
        }
        
        /// Get 3 near from destination
        if(self.petrolLayersNearDestination.count > 0 ){
            
            var totalAppends = 0
            let allKeys = self.petrolLayersNearDestination.keys
            
            for key in allKeys {
                
                if self.petrolLayersNearDestination.keys.contains(key){
                    
                    let array = self.petrolLayersNearDestination[key] as! [Any]
                    
                    for item in array {
                        
                        var count = 0
                        var newlocationArray = [Any]()
                        let itemDict = item as! [String:Any]
                        let name = itemDict["name"] as! String
                        
                        let lat = itemDict["lat"] as! Double
                        let long = itemDict["long"] as! Double
                        let address = itemDict["address"] as! String
                        let stationName = itemDict["stationName"] as! String
                        let amenityId = itemDict["id"] as! String
                        let imageURL = itemDict["mapImageURL"] as! String
                        
                        let amenties = ["lat":lat,"long":long,"mapImageURL":imageURL,"name":name,"address":address,"stationName":stationName,"id":amenityId] as [String : Any]
                        
                        if let row = subItems.firstIndex(where: {$0.element_name == name}) {
                            
                            let prevCount = subItems[row].count ?? 0
                            count =  count + 1 + prevCount
                            let subItem = Sub_items( element_name: subItems[row].element_name,count:count, display_text: subItems[row].display_text, is_selected: subItems[row].is_selected,activeImageURL: "",inActiveImageURL: "",element_id: subItems[row].element_id)
                            subItems[row] = subItem
                        }
                        
                        // if(totalAppends < 3){
                        
                        totalAppends = totalAppends + 1
                        
                        newlocationArray.append(amenties)
                        
                        /// Check if same element is already present in subItems array
                        
                        if(self.finalAmenitiesWithInSourceAndDestForPetrol[key] != nil){
                            
                            var prevlocationArray = self.finalAmenitiesWithInSourceAndDestForPetrol[key] as! [Any]
                            if(prevlocationArray.count > 0){
                                
                                prevlocationArray.append(contentsOf: newlocationArray)
                                if self.finalAmenitiesWithInSourceAndDestForPetrol.keys.contains(key){
                                    
                                    self.finalAmenitiesWithInSourceAndDestForPetrol.updateValue(prevlocationArray, forKey: key)
                                }
                                else{
                                    
                                    self.finalAmenitiesWithInSourceAndDestForPetrol[key] = newlocationArray
                                }
                            }
                            else{
                                
                                self.finalAmenitiesWithInSourceAndDestForPetrol[key] = newlocationArray
                            }
                            
                        }
                        else{
                            
                            self.finalAmenitiesWithInSourceAndDestForPetrol[key] = newlocationArray
                        }
                        
                        //}
                    }
                }
                
                self.itemsToDisplay = KioskBase(element_name: key,element_id: elementId, sub_items: subItems)
            }
        }
        
        if(self.finalAmenitiesWithInSourceAndDestForPetrol.count > 0){
            
            self.isDisplayPetrolAmenities = true
        }
    }
    
    /**
     Thsi is called from Routeplanning controller
     To display EV items in bottom sheet of Route Planning when click on the item
     */
    
    func displayEVKioskItems(){
        
        guard let amenitiesPreference = amenitiesPreference else {
            return
        }
        
        var subItems = [Sub_items]()
        for amenity in amenitiesPreference {
            
            guard let amenityDict = amenity as? NSDictionary else { return }
            
            guard let elementName = amenityDict["element_name"] as? String else { return }
            
            guard let elementID = amenityDict["element_id"] as? String else { return }
            
            var filters = [String]()
            if(elementName == Values.EV_CHARGER){
                let subItemsArray = amenityDict["sub_items"] as? NSArray
                for sub_item in subItemsArray ?? [] {
                    
                    let subItemDict = sub_item as? NSDictionary
                    let elementName = subItemDict!["element_name"] as! String
                    let subElementId = subItemDict!["element_id"] as! String
                    let isSubItemSelected = subItemDict!["is_selected"] as! Bool
                    let displayText = subItemDict!["display_text"] as! String
                    let activeImageURL = subItemDict!["button_active_image_url"] as! String
                    let inActiveImageURL = subItemDict!["button_inactive_image_url"] as! String
                    filters.append(elementName)
                    
                    let subItem = Sub_items(element_name: elementName, count: 0, display_text:displayText, is_selected: isSubItemSelected,activeImageURL: activeImageURL,inActiveImageURL: inActiveImageURL,element_id: subElementId)
                    subItems.append(subItem)
                }
                
                self.evItemsToDisplay = KioskBase(element_name: elementName,element_id: elementID, sub_items: subItems)
                if(evRequests.count > 0){
                    self.evRequests.removeAll()
                }
                self.evRequests.append(AmenityRequest(type: elementName, filter:AmenityFilter(provider: nil, services: nil, plugTypes: filters)))
                
                break
                
            }
        }
    }
    
    /**
     EV API request from Routeplanning controller for Destination coordinate
     
     */
    func callAmenitiesForEVForDestination(location:CLLocationCoordinate2D){
        
        if(self.evRequests.count > 0){
            
            let evPetrolRange = Double(UserSettingsModel.sharedInstance.evPetrolRange) / 1000
            
            let request = AmenitiesRequest(latitude: location.latitude, longitude: location.longitude, radius: evPetrolRange, arrivalTime: 1, destinationName: "", amenities: evRequests,resultCount: 1000,maxRadius: nil, isVoucher: false, carparkId: nil)
            
            self.amenityService.getAmenitiesFromV2(request: request) { result in
                switch result {
                case .failure(let error):
                    SwiftyBeaver.error("Failed to getAmenities: \(error.localizedDescription)")
                case .success(let amenities):
                    
                    print(amenities)
                    var mapImageURL = ""
                    var displayText = ""
                    var lat = 0.0
                    var long = 0.0
                    var type = ""
                    //var name = ""
                    var subItems = self.evItemsToDisplay?.sub_items ?? []
                    
                    guard let amenitiesPreference = self.amenitiesPreference else {
                        return
                    }
                    
                    
                    for amenity in amenities.amenities ?? []{
                        
                        mapImageURL = ""
                        lat = 0.0
                        long = 0.0
                        type = ""
                        //name = ""
                        var array = [Any]()
                        
                        for preference in amenitiesPreference {
                            
                            guard let amenityDict = preference as? NSDictionary else { return }
                            
                            guard let elementName = amenityDict["element_name"] as? String else { return }
                            
                            guard let elementId = amenityDict["element_id"] as? String else { return }
                            
                            if elementName == amenity.type{
                                type = elementName
                                mapImageURL = amenityDict["map_icon_light_selected_url"] as! String
                                let subItemsArray = amenityDict["sub_items"] as? NSArray
                                
                                for amenityData in amenity.data ?? [] {
                                    
                                    _ = amenityData.provider ?? ""
                                    let address = amenityData.address ?? ""
                                    let stationName = amenityData.stationname ?? ""
                                    let amenityId = amenityData.id ?? ""
                                    //var count = 0
                                    var plugTypes = [String]()
                                    var displayTextList = [String]()
                                    for plugType in amenityData.plugTypes ?? [] {
                                        
                                        let plugTypeName = plugType.name
                                        
                                        for sub_item in subItemsArray ?? [] {
                                            
                                            
                                            let subItemDict = sub_item as? NSDictionary
                                            let elementName = subItemDict!["element_name"] as! String
                                            let subElementId = subItemDict!["element_id"] as! String
                                            let isSubItemSelected = subItemDict!["is_selected"] as! Bool
                                            let activeImageURL = subItemDict!["button_active_image_url"] as! String
                                            let inActiveImageURL = subItemDict!["button_inactive_image_url"] as! String
                                            
                                            if(elementName == plugTypeName){
                                                
                                                //                                            count = count + 1
                                                long =   amenityData.location?.coordinates?[0] ?? 0
                                                
                                                lat =   amenityData.location?.coordinates?[1] ?? 0
                                                
                                                
                                                displayText = subItemDict!["display_text"] as! String
                                                
                                                /// Check if same element is already present in subItems array
                                                if let row = subItems.firstIndex(where: {$0.element_name == plugTypeName}) {
                                                    
                                                    //                                                let prevCount = subItems[row].count ?? 0
                                                    //                                                count =  count + prevCount
                                                    let subItem = Sub_items( element_name: plugTypeName,count:0, display_text: displayText, is_selected: isSubItemSelected,activeImageURL: activeImageURL,inActiveImageURL: inActiveImageURL,element_id: subElementId)
                                                    subItems[row] = subItem
                                                }
                                                else{
                                                    
                                                    let subItem = Sub_items( element_name: plugTypeName,count:0, display_text: displayText, is_selected: isSubItemSelected,activeImageURL: activeImageURL,inActiveImageURL: inActiveImageURL,element_id: subElementId)
                                                    subItems.append(subItem)
                                                }
                                                
                                                plugTypes.append(plugTypeName ?? "")
                                                displayTextList.append(displayText)
                                                
                                                
                                            }
                                            
                                            
                                            
                                        }
                                        
                                    }
                                    
                                    let amenties = ["lat":lat,"long":long,"mapImageURL":mapImageURL,"name":displayText,"address":address,"stationName":stationName,"plugType":plugTypes,"id":amenityId,"displayText":displayTextList,"startDate": -1.0, "endDate": -1.0] as [String : Any]
                                    
                                    array.append(amenties)
                                    
                                    
                                    if self.evLayersNearDestination.keys.contains(type){
                                        
                                        self.evLayersNearDestination.updateValue(array, forKey: type)
                                    }
                                    else{
                                        
                                        self.evLayersNearDestination[type] = array
                                    }
                                    
                                }
                                
                                
                                
                                self.evItemsToDisplay = KioskBase(element_name: elementName,element_id: elementId, sub_items: subItems)
                            }
                        }
                        
                    }
                    
                    if(self.evLayersNearDestination.count > 0 || self.evSymbolLayers.count > 0){
                        self.findNearest3ItemsFromEV()
                        //                        print(self.petrolAndEVSymbolLayers)
                        //                        print(self.itemsToDisplay?.sub_items)
                        //                        self.isDisplayPetrolEVAmenities = true
                        
                    }
                    
                }
            }
        }
        
    }
    
    /**
     EV  API request from Routeplanning controller for Origin coordinate
     */
    func callAmenitiesApiForEV(location:CLLocationCoordinate2D,destination:CLLocationCoordinate2D){
        
        if(self.evRequests.count > 0){
            
            let evPetrolRange = Double(UserSettingsModel.sharedInstance.evPetrolRange) / 1000
            
            let request = AmenitiesRequest(latitude: location.latitude, longitude: location.longitude, radius: evPetrolRange, arrivalTime: 1, destinationName: "", amenities: evRequests,resultCount: 1000,maxRadius: nil, isVoucher: false, carparkId: nil)
            self.amenityService.getAmenitiesFromV2(request: request) { result in
                switch result {
                case .failure(let error):
                    SwiftyBeaver.error("Failed to getAmenities: \(error.localizedDescription)")
                case .success(let amenities):
                    
                    guard let amenitiesPreference = self.amenitiesPreference else {
                        return
                    }
                    
                    print(amenities)
                    var mapImageURL = ""
                    var displayText = ""
                    var lat = 0.0
                    var long = 0.0
                    var type = ""
                    var subItems = self.evItemsToDisplay?.sub_items ?? []
                    for amenity in amenities.amenities ?? []{
                        
                        mapImageURL = ""
                        lat = 0.0
                        long = 0.0
                        type = ""
                        //name = ""
                        var array = [Any]()
                        for preference in amenitiesPreference {
                            
                            guard let amenityDict = preference as? NSDictionary else { return }
                            
                            guard let elementName = amenityDict["element_name"] as? String else { return }
                            
                            guard let elementId = amenityDict["element_id"] as? String else { return }
                            
                            if elementName == amenity.type{
                                
                                type = elementName
                                mapImageURL = amenityDict["map_icon_light_selected_url"] as! String
                                let subItemsArray = amenityDict["sub_items"] as? NSArray
                                
                                for amenityData in amenity.data ?? [] {
                                    
                                    _ = amenityData.provider ?? ""
                                    let address = amenityData.address ?? ""
                                    let amenityId = amenityData.id ?? ""
                                    let stationName = amenityData.stationname ?? ""
                                    //var count = 0
                                    var plugTypes = [String]()
                                    var displayTextList = [String]()
                                    for plugType in amenityData.plugTypes ?? [] {
                                        
                                        let plugTypeName = plugType.name
                                        
                                        for sub_item in subItemsArray ?? [] {
                                            
                                            let subItemDict = sub_item as? NSDictionary
                                            let elementName = subItemDict!["element_name"] as! String
                                            let subElementId = subItemDict!["element_id"] as! String
                                            let isSubItemSelected = subItemDict!["is_selected"] as! Bool
                                            displayText = subItemDict!["display_text"] as! String
                                            let activeImageURL = subItemDict!["button_active_image_url"] as! String
                                            let inActiveImageURL = subItemDict!["button_inactive_image_url"] as! String
                                            
                                            if(elementName == plugTypeName){
                                                
                                                //count = count + 1
                                                long =   amenityData.location?.coordinates?[0] ?? 0
                                                
                                                lat =   amenityData.location?.coordinates?[1] ?? 0
                                                
                                                /// Check if same element is already present in subItems array
                                                if let row = subItems.firstIndex(where: {$0.element_name == plugTypeName}) {
                                                    
                                                    //                                                let prevCount = subItems[row].count ?? 0
                                                    //                                                count =  count + prevCount
                                                    let subItem = Sub_items( element_name: plugTypeName,count:0, display_text: displayText, is_selected: isSubItemSelected,activeImageURL: activeImageURL,inActiveImageURL: inActiveImageURL,element_id: subElementId)
                                                    subItems[row] = subItem
                                                }
                                                else{
                                                    
                                                    let subItem = Sub_items( element_name: plugTypeName,count:0, display_text: displayText, is_selected: isSubItemSelected,activeImageURL: activeImageURL,inActiveImageURL: inActiveImageURL,element_id: subElementId)
                                                    subItems.append(subItem)
                                                }
                                                
                                                
                                                plugTypes.append(plugTypeName ?? "")
                                                displayTextList.append(displayText)
                                                
                                            }
                                            
                                            
                                        }
                                        
                                    }
                                    
                                    let amenties = ["lat":lat,"long":long,"mapImageURL":mapImageURL,"name":displayText,"address":address,"stationName":stationName,"plugType":plugTypes,"id":amenityId,"displayText":displayTextList,"startDate": 0.0, "endDate": 0.0] as [String : Any]
                                    
                                    array.append(amenties)
                                    
                                    
                                    if self.evSymbolLayers.keys.contains(type){
                                        
                                        self.evSymbolLayers.updateValue(array, forKey: type)
                                    }
                                    else{
                                        
                                        self.evSymbolLayers[type] = array
                                    }
                                    
                                    
                                    
                                }
                                
                                
                                self.evItemsToDisplay = KioskBase(element_name: elementName,element_id: elementId, sub_items: subItems)
                            }
                        }
                        
                    }
                    
                    self.callAmenitiesForEVForDestination(location: destination)
                    
                }
            }
        }
        
    }
    
    /**
     If none of the item is selected from Bottom Sheet for Petrol and, display 3 items from origin and 3 from destination for EVCharging
     */
    func findNearest3ItemsFromEV(){
        
        var subItems = self.evItemsToDisplay?.sub_items ?? []
        var elementId = self.evItemsToDisplay?.element_id ?? ""
        
        //var newSubItems = [Sub_items]()
        if(finalAmenitiesWithInSourceAndDestForEv.count > 0){
            finalAmenitiesWithInSourceAndDestForEv.removeAll()
        }
        if(self.evSymbolLayers.count > 0){
            
            let allKeys = self.evSymbolLayers.keys
            
            for key in allKeys {
                
                var locationArray = [Any]()
                
                if self.evSymbolLayers.keys.contains(key){
                    
                    let array = AmenitiesSharedInstance.shared.evSymbolLayers[key] as! [Any]
                    for item in array {
                        
                        var count = 0
                        let itemDict = item as! [String:Any]
                        let name = itemDict["name"] as! String
                        
                        let lat = itemDict["lat"] as! Double
                        let long = itemDict["long"] as! Double
                        let address = itemDict["address"] as! String
                        let stationName = itemDict["stationName"] as! String
                        let amenityId = itemDict["id"] as! String
                        
                        let imageURL = itemDict["mapImageURL"] as! String
                        
                        let amenties = ["lat":lat,"long":long,"mapImageURL":imageURL,"name":name,"address":address,"stationName":stationName,"plugType":itemDict["plugType"]!,"id":amenityId] as [String : Any]
                        
                        /// Check if same element is already present in subItems array
                        
                        // if(locationArray.count < 3){
                        
                        locationArray.append(amenties)
                        
                        if self.finalAmenitiesWithInSourceAndDestForEv.keys.contains(key){
                            
                            self.finalAmenitiesWithInSourceAndDestForEv.updateValue(locationArray, forKey: key)
                        }
                        else{
                            
                            self.finalAmenitiesWithInSourceAndDestForEv[key] = locationArray
                        }
                        
                        if let row = subItems.firstIndex(where: {$0.display_text == name}) {
                            
                            let prevCount = subItems[row].count ?? 0
                            count =  count + 1 + prevCount
                            let subItem = Sub_items( element_name: subItems[row].element_name,count:count, display_text: subItems[row].display_text, is_selected: subItems[row].is_selected,activeImageURL:subItems[row].activeImageURL,inActiveImageURL: subItems[row].inActiveImageURL,element_id: subItems[row].element_id)
                            subItems[row] = subItem
                        }
                        //}
                        
                    }
                }
            }
        }
        
        /// Get 3 near from destination
        if(self.evLayersNearDestination.count > 0 ){
            
            var totalAppends = 0
            let allKeys = self.evLayersNearDestination.keys
            
            for key in allKeys {
                
                if self.evLayersNearDestination.keys.contains(key){
                    
                    let array = self.evLayersNearDestination[key] as! [Any]
                    
                    for item in array {
                        
                        var count = 0
                        var newlocationArray = [Any]()
                        let itemDict = item as! [String:Any]
                        let name = itemDict["name"] as! String
                        
                        let lat = itemDict["lat"] as! Double
                        let long = itemDict["long"] as! Double
                        let address = itemDict["address"] as! String
                        let stationName = itemDict["stationName"] as! String
                        let imageURL = itemDict["mapImageURL"] as! String
                        let amenityId = itemDict["id"] as! String
                        
                        let amenties = ["lat":lat,"long":long,"mapImageURL":imageURL,"name":name,"address":address,"stationName":stationName,"plugType":itemDict["plugType"]!,"id":amenityId] as [String : Any]
                        
                        //if(totalAppends < 3){
                        
                        totalAppends = totalAppends + 1
                        
                        newlocationArray.append(amenties)
                        
                        /// Check if same element is already present in subItems array
                        if(self.finalAmenitiesWithInSourceAndDestForEv[key] != nil){
                            
                            var prevlocationArray = self.finalAmenitiesWithInSourceAndDestForEv[key] as! [Any]
                            if(prevlocationArray.count > 0){
                                
                                prevlocationArray.append(contentsOf: newlocationArray)
                                if self.finalAmenitiesWithInSourceAndDestForEv.keys.contains(key){
                                    
                                    self.finalAmenitiesWithInSourceAndDestForEv.updateValue(prevlocationArray, forKey: key)
                                }
                                else{
                                    
                                    self.finalAmenitiesWithInSourceAndDestForEv[key] = newlocationArray
                                }
                            }
                            else{
                                
                                self.finalAmenitiesWithInSourceAndDestForEv[key] = newlocationArray
                            }
                            
                        }
                        else{
                            
                            self.finalAmenitiesWithInSourceAndDestForEv[key] = newlocationArray
                        }
                        
                        if let row = subItems.firstIndex(where: {$0.display_text == name}) {
                            
                            let prevCount = subItems[row].count ?? 0
                            count =  count + 1 + prevCount
                            let subItem = Sub_items( element_name: subItems[row].element_name,count:count, display_text: subItems[row].display_text, is_selected: subItems[row].is_selected,activeImageURL: subItems[row].activeImageURL,inActiveImageURL: subItems[row].inActiveImageURL,element_id: subItems[row].element_id)
                            subItems[row] = subItem
                        }
                        //}
                    }
                }
                
                self.evItemsToDisplay = KioskBase(element_name: key,element_id: elementId, sub_items: subItems)
            }
        }
        
        if(self.finalAmenitiesWithInSourceAndDestForEv.count > 0){
            
            self.isDisplayEVAmenities = true
        }
    }
    
    
    func getExistingStopsAdded(plan:PlannTripDetails?,themeType:String)-> (wayPoints:[Waypoint]?,wayPointFeatures:[Turf.Feature]?){
        
        var wayPoints = [Waypoint]()
        var wayPointFeatures = [Turf.Feature]()
        if let plan = plan {
            
            for stop in plan.routeStops {
                
                let allKeys = AmenitiesSharedInstance.shared.finalAmenitiesWithInSourceAndDestForPetrol.keys
                
                //self.parseAmenityImage()
                for key in allKeys {
                    
                    if AmenitiesSharedInstance.shared.finalAmenitiesWithInSourceAndDestForPetrol.keys.contains(key){
                        
                        let array = AmenitiesSharedInstance.shared.finalAmenitiesWithInSourceAndDestForPetrol[key] as! [Any]
                        
                        print(array)
                        for item in array {
                            
                            let itemDict = item as! [String:Any]
                            let amenityID = itemDict["id"] as! String
                            let sName = itemDict["stationName"] as! String
                            if(amenityID == stop.amenityId){
                                
                                let stopCoordinate = CLLocationCoordinate2D(latitude: stop.coordinates?[0] ?? 0.0, longitude: stop.coordinates?[1] ?? 0.0)
                                let wayPoint = Waypoint(coordinate:stopCoordinate , coordinateAccuracy: -1, name: amenityID)
                                wayPoint.separatesLegs = Values.wayPointSeparateLegs
                                
                                if(wayPoints.count > 0){
                                    
                                    let isWayPointContainsID = wayPoints.contains(where: { $0.name == amenityID })
                                    if(!isWayPointContainsID){
                                        wayPoints.append(wayPoint)
                                        
                                        let name = itemDict["name"] as! String
                                        let address = itemDict["address"] as! String
                                        
                                        var addStop = false //Default value of add stop is false
                                        var isHideStopButton = false
                                        
                                        /// check if there are any existing waypoints added by comparing with station name
                                        if(wayPoints.count > 0){
                                            
                                            addStop = wayPoints.contains(where: { $0.name == amenityID })
                                            
                                            if(addStop == false){
                                                
                                                if(wayPoints.count == 3){
                                                    isHideStopButton = true
                                                }
                                                
                                            }
                                        }
                                        
                                        
                                        var feature = Turf.Feature(geometry: .point(Point(stopCoordinate)))
                                        feature.properties = [
                                            "type":.string(stop.type ?? ""),
                                            "name":.string(name),
                                            "address":.string(address),
                                            "stationName":.string(sName),
                                            "addStop":.boolean(!addStop), //Here we keep opposite value
                                            "image_id":.string("\(themeType)\(stop.type ?? "")"),
                                            "amenityID":.string(amenityID),
                                            "addStopButton":.boolean(isHideStopButton),
                                        ]
                                        
                                        wayPointFeatures.append(feature)
                                    }
                                    
                                }
                                else{
                                    
                                    wayPoints.append(wayPoint)
                                    
                                    let name = itemDict["name"] as! String
                                    let address = itemDict["address"] as! String
                                    
                                    var addStop = false //Default value of add stop is false
                                    var isHideStopButton = false
                                    
                                    /// check if there are any existing waypoints added by comparing with station name
                                    if(wayPoints.count > 0){
                                        
                                        addStop = wayPoints.contains(where: { $0.name == amenityID })
                                        
                                        if(addStop == false){
                                            
                                            if(wayPoints.count == 3){
                                                isHideStopButton = true
                                            }
                                            
                                        }
                                    }
                                    
                                    
                                    var feature = Turf.Feature(geometry: .point(Point(stopCoordinate)))
                                    feature.properties = [
                                        "type":.string(stop.type ?? ""),
                                        "name":.string(name),
                                        "address":.string(address),
                                        "stationName":.string(sName),
                                        "addStop":.boolean(!addStop), //Here we keep opposite value
                                        "image_id":.string("\(themeType)\(stop.type ?? "")"),
                                        "amenityID":.string(amenityID),
                                        "addStopButton":.boolean(isHideStopButton),
                                    ]
                                    
                                    wayPointFeatures.append(feature)
                                }
                                
                                
                            }
                            
                        }
                        
                    }
                }
                
            }
            
            for stop in plan.routeStops {
                
                let allKeys = AmenitiesSharedInstance.shared.finalAmenitiesWithInSourceAndDestForEv.keys
                
                //self.parseAmenityImage()
                for key in allKeys {
                    
                    if AmenitiesSharedInstance.shared.finalAmenitiesWithInSourceAndDestForEv.keys.contains(key){
                        
                        let array = AmenitiesSharedInstance.shared.finalAmenitiesWithInSourceAndDestForEv[key] as! [Any]
                        
                        print(array)
                        for item in array {
                            
                            let itemDict = item as! [String:Any]
                            let amenityID = itemDict["id"] as! String
                            let sName = itemDict["stationName"] as! String
                            
                            if(amenityID == stop.amenityId){
                                
                                let multiplePlugTypes = itemDict["plugType"] as! Array<String>
                                var jsonArray = JSONArray()
                                for type in multiplePlugTypes {
                                    
                                    let value = JSONValue(type)
                                    jsonArray.append(value)
                                }
                                
                                let stopCoordinate = CLLocationCoordinate2D(latitude: stop.coordinates?[0] ?? 0.0, longitude: stop.coordinates?[1] ?? 0.0)
                                let wayPoint = Waypoint(coordinate:stopCoordinate , coordinateAccuracy: -1, name: amenityID)
                                wayPoint.separatesLegs = Values.wayPointSeparateLegs
                                if(wayPoints.count > 0){
                                    
                                    let isWayPointContainsID = wayPoints.contains(where: { $0.name == amenityID })
                                    if(!isWayPointContainsID){
                                        wayPoints.append(wayPoint)
                                        
                                        let name = itemDict["name"] as! String
                                        let address = itemDict["address"] as! String
                                        
                                        var addStop = false //Default value of add stop is false
                                        var isHideStopButton = false
                                        
                                        /// check if there are any existing waypoints added by comparing with station name
                                        if(wayPoints.count > 0){
                                            
                                            addStop = wayPoints.contains(where: { $0.name == amenityID })
                                            
                                            if(addStop == false){
                                                
                                                if(wayPoints.count == 3){
                                                    isHideStopButton = true
                                                }
                                                
                                            }
                                        }
                                        
                                        var feature = Turf.Feature(geometry: .point(Point(stopCoordinate)))
                                        feature.properties = [
                                            "type":.string(stop.type ?? ""),
                                            "name":.string(name),
                                            "address":.string(address),
                                            "stationName":.string(sName),
                                            "addStop":.boolean(!addStop), //Here we keep opposite value
                                            "image_id":.string("\(themeType)\(stop.type ?? "")"),
                                            "amenityID":.string(amenityID),
                                            "addStopButton":.boolean(isHideStopButton),
                                            "plugTypes":.array(jsonArray)
                                        ]
                                        
                                        wayPointFeatures.append(feature)
                                    }
                                    
                                }
                                else{
                                    
                                    wayPoints.append(wayPoint)
                                    
                                    let name = itemDict["name"] as! String
                                    let address = itemDict["address"] as! String
                                    
                                    var addStop = false //Default value of add stop is false
                                    var isHideStopButton = false
                                    
                                    /// check if there are any existing waypoints added by comparing with station name
                                    if(wayPoints.count > 0){
                                        
                                        addStop = wayPoints.contains(where: { $0.name == amenityID })
                                        
                                        if(addStop == false){
                                            
                                            if(wayPoints.count == 3){
                                                isHideStopButton = true
                                            }
                                            
                                        }
                                    }
                                    
                                    var feature = Turf.Feature(geometry: .point(Point(stopCoordinate)))
                                    feature.properties = [
                                        "type":.string(stop.type ?? ""),
                                        "name":.string(name),
                                        "address":.string(address),
                                        "stationName":.string(sName),
                                        "addStop":.boolean(!addStop), //Here we keep opposite value
                                        "image_id":.string("\(themeType)\(stop.type ?? "")"),
                                        "amenityID":.string(amenityID),
                                        "addStopButton":.boolean(isHideStopButton),
                                        "plugTypes":.array(jsonArray)
                                    ]
                                    
                                    wayPointFeatures.append(feature)
                                }
                                
                                
                            }
                            
                        }
                        
                    }
                }
                
            }
        }
        
        return (wayPoints,wayPointFeatures)
    }
    
    /**
     Update selected Petrol/Ev charger sub element Id and parent element Id with selected status to RN user preferences
     Update local amenitiesPreferences
     */
    func updateSelectedPetrolAndEvItems(kiosItems:KioskBase,subItemId:String,isSelected:Bool,parentSelected:Bool){
        
        let parentElementId = kiosItems.element_id ?? ""
        for subItem in kiosItems.sub_items ?? [] {
            
            if subItem.element_id == subItemId {
                
                isAmenitiesChanged = true
                let updateJson = ["element_id":parentElementId,"is_selected":parentSelected,
                                  "sub_item"
                                  : ["element_id":subItemId,"is_selected":isSelected]] as [String : Any]
                
                _ = ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.UPDATE_AMENITIES_PREF, data: updateJson as NSDictionary).subscribe(onSuccess: {_ in
                    // ...
                },                 onFailure: {_ in
                    // ...
                })
                
                break
            }
        }
        
        
        //        guard let amenitiesPreference = self.amenitiesPreference else {
        //            return
        //        }
        //        var array = [Any]()
        //        var subItemsArray = [Any]()
        //        for amenity in amenitiesPreference {
        //
        //            var amenityDict = amenity as? NSMutableDictionary
        //
        //            if let eId =  amenityDict?["element_id"] as? String {
        //
        //                if eId == parentElementId {
        //
        //                    amenityDict?["is_selected"] = parentSelected
        //                    let subItems = amenityDict?["sub_items"] as? NSArray
        //                    for subItem in subItems ?? [] {
        //
        //                        let subItemDict = subItem as? NSMutableDictionary
        //                        if let subId =  subItemDict?["element_id"] as? String {
        //
        //                            if subId == subItemId{
        //
        //                                subItemDict?["is_selected"] = isSelected
        //                                subItemsArray.append(subItemDict ?? [])
        //                            }
        //                            else{
        //
        //                                subItemsArray.append(subItemDict ?? [])
        //                            }
        //                        }
        //                    }
        //                    if(subItemsArray.count > 0){
        //
        //                        amenityDict?["sub_items"] = subItemsArray
        //                    }
        //                    array.append(amenityDict ?? [])
        //                }
        //                else{
        //
        //                    array.append(amenityDict ?? [])
        //                }
        //
        //
        //            }
        //
        //        }
        //
        //        self.amenitiesPreference = NSArray(array: array)
        //
        //        NotificationCenter.default.post(name: Notification.Name(Values.NotificationRefreshAmenities), object: nil,userInfo: nil)
        
        //        for (index, item) in self.amenitiesPreference!.enumerated() {
        //
        //            var dict = item as? NSMutableDictionary
        //        }
        
    }
    
    func updateSelectedEvItems(kiosItems:KioskBase?,subItemId:String,isSelected:Bool,parentSelected:Bool){
        
        
    }
}

//  Implement for OBU/Cruise
extension AmenitiesSharedInstance {
    /**
     Checking selected amenity with original amenities preference sub items to send filter items to API
     */
    func addRequestForSelectedAmenityInCruise(location: CLLocationCoordinate2D, selectedAmenity: String, completion: ((Bool) -> Void)? = nil){
        
        guard let amenitiesPreference = amenitiesPreference else {
            return
        }
        
        for amenity in amenitiesPreference {
            
            let amenityDict = amenity as? NSDictionary
            
            if(amenityDict?.count ?? 0 > 0){
                
                let elementName = amenityDict!["element_name"] as! String
                
                var filter: AmenityFilter? = nil
                
                if (selectedAmenity == elementName) {
                    if let subItems = amenityDict!["sub_items"] as? NSArray {
                        switch elementName {
                        case Values.EV_CHARGER:
                            var plugTypes = [String]()
                            
                            for subItem in subItems {
                                let dict = subItem as! NSDictionary
                                if dict["is_selected"] as? Bool ?? false {
                                    plugTypes.append(dict["element_name"] as! String)
                                }
                            }
                            if plugTypes.count > 0 {
                                filter = AmenityFilter(provider: nil, services: nil, plugTypes: plugTypes)
                            }
                        case Values.PETROL:
                            var provider = [String]()
                            let subItems = amenityDict!["sub_items"] as! NSArray
                            for subItem in subItems {
                                let dict = subItem as! NSDictionary
                                if dict["is_selected"] as? Bool ?? false {
                                    provider.append(dict["element_name"] as! String)
                                }
                            }
                            if provider.count > 0 {
                                filter = AmenityFilter(provider: provider, services: nil, plugTypes: nil)
                            }
                        case Values.POI:
                            
                            if let profilePreference = self.profilePreference, let profile = profilePreference.profiles?.first,let poiAmenities = profile.amenities{
                                
                                var provider = [String]()
                                let subItems = amenityDict!["sub_items"] as! NSArray
                                
                                // check poi Amenity in the profile preferences and send only those amenities to API as provide
                                for poiAmenity in poiAmenities{
                                    
                                    for subItem in subItems {
                                        let dict = subItem as! NSDictionary
                                        let subElementName = dict["element_name"] as! String
                                        if subElementName == poiAmenity{
                                            
                                            provider.append(dict["element_name"] as! String)
                                        }
                                        
                                    }
                                    if provider.count > 0 {
                                        filter = AmenityFilter(provider: provider, services: nil, plugTypes: nil)
                                    }
                                }
                                
                            }
                            
                        default:
                            break
                        }
                        
                        self.requests.append(AmenityRequest(type: elementName, filter: filter))
                    }
                }
            }
        }
        
        if(self.requests.count > 0){
            self.callAmenitiesAPIInCruise(location: location, completion)
        }
    }
    
    /**
     Amenities API method
     fetching amenities Data
     Adding publish event isDisplayAmenities to display amenities data on to map
     */
    func callAmenitiesAPIInCruise(location: CLLocationCoordinate2D, carparkId: String? = nil, _ completion: ((Bool) -> Void)? = nil) {
        
        lastLocation = location
        
        if(self.requests.count > 0){
            
            let evPetrolCount = UserSettingsModel.sharedInstance.evPetrolCount
            let evPetrolRange = Double(UserSettingsModel.sharedInstance.evPetrolRange) / 1000
            let maxEVPetrolRange = Double(UserSettingsModel.sharedInstance.maxEVPetrolRange) / 1000
            
            let request = AmenitiesRequest(latitude: location.latitude, longitude: location.longitude, radius: evPetrolRange, arrivalTime: 1, destinationName: "", amenities: self.requests, resultCount: evPetrolCount, maxRadius: maxEVPetrolRange, isVoucher: false, carparkId: carparkId)
            
            if !self.cruiseSymbolLayer.isEmpty {
                self.cruiseSymbolLayer.removeAll()
            }
            
            self.amenityService.getAmenitiesFromV2(request: request) { result in
                switch result {
                case .failure(let error):
                    SwiftyBeaver.error("Failed to getAmenities: \(error.localizedDescription)")
                    completion?(false)
                case .success(let amenities):
                    guard let amenitiesPreference = self.amenitiesPreference else {
                        completion?(false)
                        return
                    }
                    print(amenities)
                    
                    var mapImageURL = ""
                    var lat = 0.0
                    var long = 0.0
                    var type = ""
                    
                    for amenity in amenities.amenities ?? [] {
                        mapImageURL = ""
                        lat = 0.0
                        long = 0.0
                        type = ""
                        
                        var array = [Any]()
                        
                        for preference in amenitiesPreference {
                            
                            guard let amenityDict = preference as? NSDictionary else { return }
                            
                            guard let elementName = amenityDict["element_name"] as? String else { return }
                            
                            if elementName == amenity.type {
                                
                                type = elementName
                                let clusterID = amenityDict["cluster_id"] as? Int ?? -1
                                let clusterColor = (amenityDict["cluster_color"] as? String) ?? ""
                                mapImageURL = (amenityDict["map_icon_light_selected_url"] as? String) ?? ""
                                
                                for amenityData in amenity.data ?? [] {
                                    
                                    if let coordinates = amenityData.location?.coordinates, coordinates.count >= 2 {
                                        long = coordinates[0]
                                        lat = coordinates[1]
                                    }
                                    
                                    //This is applicable to only the data that return image types. at the moment for 24hr clinic
                                    let imageType = amenityData.imageType ?? ""
                                    if (imageType != "") {
                                        type = imageType
                                        //Since we are taking imageType as the key for some of the amenities we should clear the array to avoid duplicate values
                                        if !self.amenitiesSymbolLayer.keys.contains(type){
                                            array = [Any]()
                                        }
                                    }
                                    
                                    let typeName = amenityData.name ?? ""
                                    let address = amenityData.address ?? ""
                                    let amenityId = amenityData.id ?? ""
                                    let poiID = amenityData.id ?? ""
                                    let providerName = amenityData.provider ?? ""
                                    let startDate = Double(amenityData.startDate ?? -1)
                                    let endDate = Double(amenityData.endDate ?? -1)
                                    let bookmarkId = amenityData.bookmarkId ?? -1
                                    let isBookmarked = amenityData.isBookmarked ?? false
                                    let textAdditionalInfo = amenityData.additionalInfo?.text
                                    let styleLightColor = amenityData.additionalInfo?.style?.lightColor?.color
                                    let styleDarkColor = amenityData.additionalInfo?.style?.darktColor?.color
                                    
                                    let amenties = ["lat":lat,
                                                    "long":long,
                                                    "mapImageURL":mapImageURL,
                                                    "name":typeName,
                                                    "address":address,
                                                    "id": amenityId,
                                                    "distance":amenityData.distance ?? 0,
                                                    "startDate": startDate,
                                                    "endDate": endDate,
                                                    "clusterID":clusterID,
                                                    "clusterColor":clusterColor,
                                                    "poiID": poiID,
                                                    "providerName":providerName,
                                                    "bookmarkId": bookmarkId,
                                                    "isBookmarked": isBookmarked,
                                                    "textAdditionalInfo": textAdditionalInfo ?? "",
                                                    "styleLightColor": styleLightColor ?? "",
                                                    "styleDarkColor": styleDarkColor ?? "",
                                                    "originalType":elementName] as [String : Any]
                                    
                                    array.append(amenties)
                                    
                                    if self.cruiseSymbolLayer.keys.contains(type) {
                                        self.cruiseSymbolLayer.updateValue(array, forKey: type)
                                    } else {
                                        self.cruiseSymbolLayer[type] = array
                                    }
                                }
                                break
                            }
                        }
                    }
                    
                    completion?(true)
                }
            }
        } else {
            if !self.cruiseSymbolLayer.isEmpty {
                self.cruiseSymbolLayer.removeAll()
            }
            completion?(false)
        }
    }
}

//  Fetch petrol and EV in navigation
extension AmenitiesSharedInstance {
    
    /**
     EV  API request from TurnByTurn
     */
    func fetchEVAmenities(location: CLLocationCoordinate2D, completion: ((Bool) -> Void)? = nil) {
        
        if !self.turnbyturnPetrolOrEVSymbolLayers.isEmpty {
            self.turnbyturnPetrolOrEVSymbolLayers.removeAll()
        }
        
        guard let amenitiesPreference = amenitiesPreference else {
            completion?(false)
            return
        }

        var subItems = [Sub_items]()
        for amenity in amenitiesPreference {
          
            guard let amenityDict = amenity as? NSDictionary else { return }
            
            guard let elementName = amenityDict["element_name"] as? String else { return }
            
            guard let elementID = amenityDict["element_id"] as? String else { return }
           
            var filters = [String]()
            if(elementName == Values.EV_CHARGER){
                let subItemsArray = amenityDict["sub_items"] as? NSArray
                for sub_item in subItemsArray ?? [] {
                    
                    let subItemDict = sub_item as? NSDictionary
                    let elementName = subItemDict!["element_name"] as! String
                    let subElementId = subItemDict!["element_id"] as! String
                    let isSubItemSelected = subItemDict!["is_selected"] as! Bool
                    let displayText = subItemDict!["display_text"] as! String
                    let activeImageURL = subItemDict!["button_active_image_url"] as! String
                    let inActiveImageURL = subItemDict!["button_inactive_image_url"] as! String
                    filters.append(elementName)
                    
                    let subItem = Sub_items(element_name: elementName, count: 0, display_text:displayText, is_selected: isSubItemSelected,activeImageURL: activeImageURL,inActiveImageURL: inActiveImageURL,element_id: subElementId)
                    subItems.append(subItem)
                }

                self.evItemsToDisplay = KioskBase(element_name: elementName,element_id: elementID, sub_items: subItems)
                if(evRequests.count > 0){
                    self.evRequests.removeAll()
                }
                self.evRequests.append(AmenityRequest(type: elementName, filter:AmenityFilter(provider: nil, services: nil, plugTypes: filters)))
                break
                
            }
        }
        
        if(self.evRequests.count > 0){
            self.fetchEVRequestInNavigation(location: location, completion: completion)
        }
    }
    
    private func fetchEVRequestInNavigation(location: CLLocationCoordinate2D, completion: ((Bool) -> Void)? = nil) {
        
        if !self.evRequests.isEmpty {
            
            let evPetrolCount = UserSettingsModel.sharedInstance.evPetrolCount
            let evPetrolRange = Double(UserSettingsModel.sharedInstance.evPetrolRange) / 1000
            let maxEVPetrolRange = Double(UserSettingsModel.sharedInstance.maxEVPetrolRange) / 1000
            
            let request = AmenitiesRequest(latitude: location.latitude, longitude: location.longitude, radius: evPetrolRange, arrivalTime: 1, destinationName: "", amenities: evRequests, resultCount: evPetrolCount, maxRadius: maxEVPetrolRange, isVoucher: false, carparkId: nil)
            
            self.amenityService.getAmenitiesFromV2(request: request) { result in
                switch result {
                case .failure(let error):
                    SwiftyBeaver.error("Failed to getAmenities: \(error.localizedDescription)")
                    completion?(false)
                case .success(let amenities):
                    guard let amenitiesPreference = self.amenitiesPreference else {
                        completion?(false)
                        return
                    }
                    print(amenities)
                    var mapImageURL = ""
                    var displayText = ""
                    var lat = 0.0
                    var long = 0.0
                    var type = ""
                    var subItems = self.evItemsToDisplay?.sub_items ?? []
                    for amenity in amenities.amenities ?? []{
                        
                        mapImageURL = ""
                        lat = 0.0
                        long = 0.0
                        type = ""
                        //name = ""
                        var array = [Any]()
                        for preference in amenitiesPreference {
                            
                            guard let amenityDict = preference as? NSDictionary else { return }
                            
                            guard let elementName = amenityDict["element_name"] as? String else { return }
                            
                            guard let elementId = amenityDict["element_id"] as? String else { return }
                            
                            if elementName == amenity.type{
                                
                                type = elementName
                                mapImageURL = amenityDict["map_icon_light_selected_url"] as! String
                                let subItemsArray = amenityDict["sub_items"] as? NSArray
                                
                                for amenityData in amenity.data ?? [] {
                                    
                                    _ = amenityData.provider ?? ""
                                    let address = amenityData.address ?? ""
                                    let amenityId = amenityData.id ?? ""
                                    let stationName = amenityData.stationname ?? ""
                                    //var count = 0
                                    var plugTypes = [String]()
                                    var displayTextList = [String]()
                                    for plugType in amenityData.plugTypes ?? [] {
                                        
                                        let plugTypeName = plugType.name
                                        
                                        for sub_item in subItemsArray ?? [] {
                                            
                                            let subItemDict = sub_item as? NSDictionary
                                            let elementName = subItemDict!["element_name"] as! String
                                            let subElementId = subItemDict!["element_id"] as! String
                                            let isSubItemSelected = subItemDict!["is_selected"] as! Bool
                                            displayText = subItemDict!["display_text"] as! String
                                            let activeImageURL = subItemDict!["button_active_image_url"] as! String
                                            let inActiveImageURL = subItemDict!["button_inactive_image_url"] as! String
                                            
                                            if(elementName == plugTypeName){
                                                
                                                //count = count + 1
                                                long =   amenityData.location?.coordinates?[0] ?? 0
                                                
                                                lat =   amenityData.location?.coordinates?[1] ?? 0
                                                
                                                /// Check if same element is already present in subItems array
                                                if let row = subItems.firstIndex(where: {$0.element_name == plugTypeName}) {
                                                    
                                                    let subItem = Sub_items( element_name: plugTypeName,count:0, display_text: displayText, is_selected: isSubItemSelected,activeImageURL: activeImageURL,inActiveImageURL: inActiveImageURL,element_id: subElementId)
                                                    subItems[row] = subItem
                                                }
                                                else{
                                                    
                                                    let subItem = Sub_items( element_name: plugTypeName,count:0, display_text: displayText, is_selected: isSubItemSelected,activeImageURL: activeImageURL,inActiveImageURL: inActiveImageURL,element_id: subElementId)
                                                    subItems.append(subItem)
                                                }
                                                
                                                
                                                plugTypes.append(plugTypeName ?? "")
                                                displayTextList.append(displayText)
                                                
                                            }
                                        }
                                    }
                                    
                                    let amenties = ["lat":lat,"long":long,"mapImageURL":mapImageURL,"name":displayText,"address":address,"stationName":stationName,"plugType":plugTypes,"id":amenityId,"displayText":displayTextList,"startDate": 0.0, "endDate": 0.0] as [String : Any]
                                    
                                    array.append(amenties)
                                    
                                    if self.turnbyturnPetrolOrEVSymbolLayers.keys.contains(type){
                                        self.turnbyturnPetrolOrEVSymbolLayers.updateValue(array, forKey: type)
                                    }
                                    else{
                                        self.turnbyturnPetrolOrEVSymbolLayers[type] = array
                                    }
                                }
                            }
                        }
                    }
                    
                    completion?(true)
                }
            }
        } else {
            completion?(false)
        }
    }
    
    /**
     Petrol  API request from TurnByTurn
     */
    func fetchPetrolAmenities(location: CLLocationCoordinate2D, completion: ((Bool) -> Void)? = nil) {
        
        if !self.turnbyturnPetrolOrEVSymbolLayers.isEmpty {
            self.turnbyturnPetrolOrEVSymbolLayers.removeAll()
        }
        
        var subItems = [Sub_items]()
        for amenity in self.amenitiesPreference ?? [] {
          
            guard let amenityDict = amenity as? NSDictionary else { return }
            
            guard let elementName = amenityDict["element_name"] as? String else { return }
            
            guard let elementID = amenityDict["element_id"] as? String else { return }
           
            var filters = [String]()
            //let elementName = amenityDict!["element_name"] as! String
            if(elementName == Values.PETROL){
                
                let subItemsArray = amenityDict["sub_items"] as? NSArray
                for sub_item in subItemsArray ?? [] {
                    
                    let subItemDict = sub_item as? NSDictionary
                    let elementName = subItemDict!["element_name"] as! String
                    let subElementId = subItemDict!["element_id"] as! String
                    let isSubItemSelected = subItemDict!["is_selected"] as! Bool
                    let displayText = subItemDict!["display_text"] as! String
                    filters.append(elementName)
                    
                    let subItem = Sub_items(element_name: elementName, count: 0, display_text:displayText, is_selected: isSubItemSelected,activeImageURL: "",inActiveImageURL: "",element_id: subElementId)
                    subItems.append(subItem)
                }

                self.itemsToDisplay = KioskBase(element_name: elementName,element_id: elementID, sub_items: subItems)
                if(petrolrequests.count > 0){
                    self.petrolrequests.removeAll()
                }
                self.petrolrequests.append(AmenityRequest(type: elementName, filter:AmenityFilter(provider: filters, services: nil, plugTypes: nil)))
                
                break
                
            }
        }
        
        if(self.petrolrequests.count > 0){
            self.fetchPetrolRequestInNavigation(location: location, completion: completion)
        }
    }
        
    private func fetchPetrolRequestInNavigation(location: CLLocationCoordinate2D, completion: ((Bool) -> Void)? = nil) {
        if(self.petrolrequests.count > 0){
            
            let evPetrolCount = UserSettingsModel.sharedInstance.evPetrolCount
            let evPetrolRange = Double(UserSettingsModel.sharedInstance.evPetrolRange) / 1000
            let maxEVPetrolRange = Double(UserSettingsModel.sharedInstance.maxEVPetrolRange) / 1000
            
            let request = AmenitiesRequest(latitude: location.latitude, longitude: location.longitude, radius: evPetrolRange, arrivalTime: 1, destinationName: "", amenities: petrolrequests,resultCount: evPetrolCount, maxRadius: maxEVPetrolRange, isVoucher: false, carparkId: nil)
            
            self.amenityService.getAmenitiesFromV2(request: request) { result in
                switch result {
                case .failure(let error):
                    completion?(false)
                    SwiftyBeaver.error("Failed to getAmenities: \(error.localizedDescription)")
                case .success(let amenities):
                    
                    print(amenities)
                    var mapImageURL = ""
                    var lat = 0.0
                    var long = 0.0
                    var type = ""
                    //var name = ""
                    var subItems = self.itemsToDisplay?.sub_items ?? []
                    
                    guard let amenitiesPreference = self.amenitiesPreference else {
                        return
                    }
                    
                    for amenity in amenities.amenities ?? []{
                        
                        mapImageURL = ""
                        lat = 0.0
                        long = 0.0
                        type = ""
                        //name = ""
                        var array = [Any]()
                        for preference in amenitiesPreference {
                            
                            guard let amenityDict = preference as? NSDictionary else { return }
                            
                            guard let elementName = amenityDict["element_name"] as? String else { return }
                            
                            guard let elementId = amenityDict["element_id"] as? String else { return }
                            
                            if elementName == amenity.type{
                                type = elementName
                                mapImageURL = amenityDict["map_icon_light_selected_url"] as! String
                                let subItemsArray = amenityDict["sub_items"] as? NSArray
                                
                                for amenityData in amenity.data ?? [] {
                                    
                                    //var count = 0
                                    
                                    for sub_item in subItemsArray ?? [] {
                                        
                                        
                                        let subItemDict = sub_item as? NSDictionary
                                        let elementName = subItemDict!["element_name"] as! String
                                        let isSubItemSelected = subItemDict!["is_selected"] as! Bool
                                        let subElementID = subItemDict!["element_id"] as! String
                                        if(elementName == amenityData.provider){
                                            
//                                            count = count + 1
                                            long =   amenityData.location?.coordinates?[0] ?? 0
                                            
                                            lat =   amenityData.location?.coordinates?[1] ?? 0
                                            
                                            let typeName = amenityData.provider ?? ""
                                            let address = amenityData.address ?? ""
                                            let stationName = amenityData.stationname ?? ""
                                            let amenityId = amenityData.id ?? ""
                                            let displayText = subItemDict!["display_text"] as! String
                                            
                                            /// Check if same element is already present in subItems array
                                            if let row = subItems.firstIndex(where: {$0.element_name == typeName}) {
                                                let subItem = Sub_items( element_name: typeName,count:0, display_text: displayText, is_selected: isSubItemSelected,activeImageURL: "",inActiveImageURL: "",element_id: subElementID)
                                                subItems[row] = subItem
                                            }
                                            else{
                                                
                                                let subItem = Sub_items( element_name: typeName,count:0, display_text: displayText, is_selected: isSubItemSelected,activeImageURL: "",inActiveImageURL: "",element_id: subElementID)
                                                subItems.append(subItem)
                                            }
                                            
                                            
                                            let amenties = ["lat":lat,"long":long,"mapImageURL":mapImageURL,"name":typeName,"address":address,"stationName":stationName,"id":amenityId,"startDate": -1.0, "endDate": -1.0] as [String : Any]
                                            
                                            array.append(amenties)
                                            
                                            
                                            if self.turnbyturnPetrolOrEVSymbolLayers.keys.contains(type){
                                                
                                                self.turnbyturnPetrolOrEVSymbolLayers.updateValue(array, forKey: type)
                                            }
                                            else{
                                                self.turnbyturnPetrolOrEVSymbolLayers[type] = array
                                            }
                                        }
                                    }
                                    
                                }
                            }
                        }
                    }
                    
                    completion?(true)
                }
            }
        }
    }
}

//  Implement for Carplay
extension AmenitiesSharedInstance {
    /**
     Checking selected amenity with original amenities preference sub items to send filter items to API
     */
    func addRequestForSelectedAmenityInCarplay(location: CLLocationCoordinate2D, selectedAmenity: String, completion: ((Bool) -> Void)? = nil){
        
        guard let amenitiesPreference = amenitiesPreference else {
            return
        }
        
        for amenity in amenitiesPreference {
            
            let amenityDict = amenity as? NSDictionary
            
            if(amenityDict?.count ?? 0 > 0){
                
                let elementName = amenityDict!["element_name"] as! String
                
                var filter: AmenityFilter? = nil
                
                if (selectedAmenity == elementName) {
                    if let subItems = amenityDict!["sub_items"] as? NSArray {
                        switch elementName {
                        case Values.EV_CHARGER:
                            var plugTypes = [String]()
                            
                            for subItem in subItems {
                                let dict = subItem as! NSDictionary
                                if dict["is_selected"] as? Bool ?? false {
                                    plugTypes.append(dict["element_name"] as! String)
                                }
                            }
                            if plugTypes.count > 0 {
                                filter = AmenityFilter(provider: nil, services: nil, plugTypes: plugTypes)
                            }
                        case Values.PETROL:
                            var provider = [String]()
                            let subItems = amenityDict!["sub_items"] as! NSArray
                            for subItem in subItems {
                                let dict = subItem as! NSDictionary
                                if dict["is_selected"] as? Bool ?? false {
                                    provider.append(dict["element_name"] as! String)
                                }
                            }
                            if provider.count > 0 {
                                filter = AmenityFilter(provider: provider, services: nil, plugTypes: nil)
                            }
                        default:
                            break
                        }
                        
                        self.requests.append(AmenityRequest(type: elementName, filter: filter))
                    }
                }
            }
        }
        
        if(self.requests.count > 0){
            self.callAmenitiesAPIInCarplay(location: location, completion)
        }
    }
    
    /**
     Amenities API method fetching amenities Data
     Adding publish event isDisplayAmenities to display amenities data on to map
     */
    func callAmenitiesAPIInCarplay(location: CLLocationCoordinate2D, carparkId: String? = nil, _ completion: ((Bool) -> Void)? = nil) {
        
        lastLocation = location
        
        if(self.requests.count > 0){
            
            let evPetrolCount = UserSettingsModel.sharedInstance.evPetrolCount
            let evPetrolRange = Double(UserSettingsModel.sharedInstance.evPetrolRange) / 1000
            let maxEVPetrolRange = Double(UserSettingsModel.sharedInstance.maxEVPetrolRange) / 1000
            
            let request = AmenitiesRequest(latitude: location.latitude, longitude: location.longitude, radius: evPetrolRange, arrivalTime: 1, destinationName: "", amenities: self.requests, resultCount: evPetrolCount, maxRadius: maxEVPetrolRange, isVoucher: false, carparkId: carparkId)
            
            if !self.carplaySymbolLayer.isEmpty {
                self.carplaySymbolLayer.removeAll()
            }
            
            self.amenityService.getAmenitiesFromV2(request: request) { result in
                switch result {
                case .failure(let error):
                    SwiftyBeaver.error("Failed to getAmenities: \(error.localizedDescription)")
                    completion?(false)
                case .success(let amenities):
                    guard let amenitiesPreference = self.amenitiesPreference else {
                        completion?(false)
                        return
                    }
                    print(amenities)
                    
                    var mapImageURL = ""
                    var lat = 0.0
                    var long = 0.0
                    var type = ""
                    
                    for amenity in amenities.amenities ?? [] {
                        mapImageURL = ""
                        lat = 0.0
                        long = 0.0
                        type = ""
                        
                        var array = [Any]()
                        
                        for preference in amenitiesPreference {
                            
                            guard let amenityDict = preference as? NSDictionary else { return }
                            
                            guard let elementName = amenityDict["element_name"] as? String else { return }
                            
                            if elementName == amenity.type {
                                
                                type = elementName
                                let clusterID = amenityDict["cluster_id"] as? Int ?? -1
                                let clusterColor = (amenityDict["cluster_color"] as? String) ?? ""
                                mapImageURL = (amenityDict["map_icon_light_selected_url"] as? String) ?? ""
                                
                                for amenityData in amenity.data ?? [] {
                                    
                                    if let coordinates = amenityData.location?.coordinates, coordinates.count >= 2 {
                                        long = coordinates[0]
                                        lat = coordinates[1]
                                    }
                                    
                                    //This is applicable to only the data that return image types. at the moment for 24hr clinic
                                    let imageType = amenityData.imageType ?? ""
                                    if (imageType != "") {
                                        type = imageType
                                        //Since we are taking imageType as the key for some of the amenities we should clear the array to avoid duplicate values
                                        if !self.amenitiesSymbolLayer.keys.contains(type){
                                            array = [Any]()
                                        }
                                    }
                                    
                                    let typeName = amenityData.name ?? ""
                                    let address = amenityData.address ?? ""
                                    let amenityId = amenityData.id ?? ""
                                    let poiID = amenityData.id ?? ""
                                    let providerName = amenityData.provider ?? ""
                                    let startDate = Double(amenityData.startDate ?? -1)
                                    let endDate = Double(amenityData.endDate ?? -1)
                                    let bookmarkId = amenityData.bookmarkId ?? -1
                                    let isBookmarked = amenityData.isBookmarked ?? false
                                    let textAdditionalInfo = amenityData.additionalInfo?.text
                                    let styleLightColor = amenityData.additionalInfo?.style?.lightColor?.color
                                    let styleDarkColor = amenityData.additionalInfo?.style?.darktColor?.color
                                    let typeDisplay = amenityData.getTypesDisplay()
                                    
                                    let amenties = ["lat":lat,
                                                    "long":long,
                                                    "mapImageURL":mapImageURL,
                                                    "name":typeName,
                                                    "address":address,
                                                    "id": amenityId,
                                                    "distance":amenityData.distance ?? 0,
                                                    "startDate": startDate,
                                                    "endDate": endDate,
                                                    "clusterID":clusterID,
                                                    "clusterColor":clusterColor,
                                                    "poiID": poiID,
                                                    "providerName":providerName,
                                                    "bookmarkId": bookmarkId,
                                                    "isBookmarked": isBookmarked,
                                                    "textAdditionalInfo": textAdditionalInfo ?? "",
                                                    "styleLightColor": styleLightColor ?? "",
                                                    "styleDarkColor": styleDarkColor ?? "",
                                                    "typeDisplay": typeDisplay,
                                                    "originalType":elementName] as [String : Any]
                                    
                                    array.append(amenties)
                                    
                                    if self.carplaySymbolLayer.keys.contains(type) {
                                        self.carplaySymbolLayer.updateValue(array, forKey: type)
                                    } else {
                                        self.carplaySymbolLayer[type] = array
                                    }
                                }
                                break
                            }
                        }
                    }
                    
                    completion?(true)
                }
            }
        } else {
            if !self.carplaySymbolLayer.isEmpty {
                self.carplaySymbolLayer.removeAll()
            }
            completion?(false)
        }
    }
}



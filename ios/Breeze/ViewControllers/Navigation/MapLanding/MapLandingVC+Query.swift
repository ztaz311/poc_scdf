//
//  MapLandingVC+Query.swift
//  Breeze
//
//  Created by VishnuKanth on 29/12/21.
//

import Foundation
import MapboxNavigation
import MapboxCoreNavigation
import MapboxMaps

extension MapLandingVC {
    
    func queryAmenitiesLayer(point:CGPoint, completion: ((Bool, Bool) -> Void)? = nil){
        
        _ =  self.navMapView.mapView.mapboxMap.coordinate(for: point)
        var layerID = [String]()
//        if (AmenitiesSharedInstance.shared.requests.count > 0){
//
//            for request in AmenitiesSharedInstance.shared.requests{
//
//                //                layerID.append("_\(mapLandingViewModel?.amenitySymbolThemeType ?? "")\(request.type)")
//                layerID.append("_\(request.type)-\(AmenitiesLayer.unclusteredCircleLayer)")
//                layerID.append("_\(request.type)-\(AmenitiesLayer.clusteredCircleLayer)")
//            }
//        }
        
        guard let amenitiesPreference = AmenitiesSharedInstance.shared.amenitiesPreference else {
            return
        }
        
        for preference in amenitiesPreference {
            
            guard let amenityDict = preference as? NSDictionary else { return }
            
            if let imageTypes = amenityDict["imageTypes"] as? NSArray {
                
                for imageType in imageTypes {
                    
                    let dict = imageType as! NSDictionary
                    let imageName = dict["imageType"] as! String
                    
                    layerID.append("_\(imageName)-\(AmenitiesLayer.unclusteredCircleLayer)")
                    layerID.append("_\(imageName)-\(AmenitiesLayer.clusteredCircleLayer)")
                }
            }
            else{
                
                guard let elementName = amenityDict["element_name"] as? String else { return }
                
                layerID.append("_\(elementName)-\(AmenitiesLayer.unclusteredCircleLayer)")
                layerID.append("_\(elementName)-\(AmenitiesLayer.clusteredCircleLayer)")
            }
            
            
        }
        layerID.append("_\(Values.DROPPIN)")
        layerID.append("_\(Values.TRAFFIC)")
        layerID.append("_\(Values.POI)")
        layerID.append("\(ERPValues.ERPCost_Identifier_SymbolLayer)")
        layerID.append(CarparkValues.carparkSymbolLayerClusterId)
        layerID.append(CarparkValues.carparkSymbolLayerUnClusterId)
        layerID.append(CarparkValues.defaultCarparkSymbolLayerIdIcon)
        let array = 1...5
        
        for i in array {
            
            let cpType = CarparkValues.carparkImageName + String(i)
            
            layerID.append("\(CarparkValues.carparkSymbolLayerClusterId)-\(cpType)")
            layerID.append("\(CarparkValues.carparkSymbolLayerUnClusterId)-\(cpType)")
        }
        
        //  Upgrade new mapbox sdk 2.10.0 causing issue annotation is duplicated event calls so we just fake handle anotation layer in here
        layerID.append(CarparkValues.destinationLayerIdentifier)
        
        if(layerID.count > 0){
            
            let queryOptions = RenderedQueryOptions(layerIds: layerID, filter: nil)
            //let Zone2NullOptions = RenderedQueryOptions(layerIds: ["zone2-null"], filter: nil)
            self.navMapView.mapView.mapboxMap.queryRenderedFeatures(at: point, options: queryOptions) { [weak self] (result) in
                guard let self = self else { return }
                
                switch result {
                    
                case .success(let features):
                    
                    // Return the first feature at that location, then pass attributes to the alert controller.
                    if let selectedFeatureProperties = features.first?.feature.properties,
                       case let .point(point) = features.first?.feature.geometry
                    {
                        if let sourceID =  features.first?.source,
                           sourceID == CarparkValues.destinationLayerIdentifier {
                            // tap on destination layer, we dont do anythings
                            completion?(true, false)
                            return
                        }
                        
                        if case let .boolean(isCluster) = selectedFeatureProperties["cluster"] {
                                                        
                            
                            if(isCluster){
                                guard let sourceID =  features.first?.source else { return }
                                guard let feature = features.first?.feature else { return }
                                //do{
                                
                                self.navMapView.mapView.mapboxMap.getGeoJsonClusterExpansionZoom(forSourceId: sourceID, feature: feature) { result in
                                    
                                    switch result {
                                    case .success(let feature):
                                        let zoom = CGFloat(feature.value as? Double ?? 0)
                                        let cameraOptions = CameraOptions(center: point.coordinates, zoom: zoom)
                                        self.navMapView.mapView.camera.ease(to: cameraOptions, duration: Values.standardAnimationDuration)
                                        
                                        
                                    case .failure(_): break
                                        
                                    }
                                }
                            }
                            
                            completion?(true, false)
                            
                        } else {
                            
                            if case let .string(type) = selectedFeatureProperties["type"] {
                                                                
                                if type == Values.DROPPIN {
                                    self.openDropPinToolTipView(feature: features.first!.feature, location: point.coordinates)
                                } else if(type == Values.TRAFFIC){
                                    self.showTrafficCallout(feature: features.first!.feature, location: point.coordinates)
                                }
                                else if(type == "ERP"){
                                    self.openERPToolTipView(feature: features.first!.feature,location:point.coordinates)
                                } else if(type == Values.POI) {
                                    self.openPOIToolTipView(feature: features.first!.feature, location: point.coordinates)
                                } else {
                                    self.openAmenityToolTipView(feature: features.first!.feature,location:point.coordinates)
                                }
                                
                                if type == Values.DROPPIN {
                                    completion?(false, false)
                                } else {
                                    completion?(false, true)
                                }
                                
                            } else if case let .string(carparkId) = selectedFeatureProperties["carparkId"]{
                                if let carpark = self.carparkUpdater?.getDefaultCarparkFrom(id: carparkId){
                                    self.openCarparkToolTipView(carpark: carpark, tapped: true, actualLocation: self.getActualLocation())
                                }
                                else if let carpark = self.carparkUpdater?.getCarparkFrom(id: carparkId) {
                                    self.openCarparkToolTipView(carpark: carpark, tapped: true, actualLocation: self.getActualLocation())
                                }
                                
                                completion?(false, true)
                                
                            } else {
                                // If the feature is a cluster, it will have `point_count` and `cluster_id` properties. These are assigned
                                // when the cluster is created.
                                // no feature detected
                                self.didDeselectAmenityAnnotation()
                                self.didDeselectAnnotation()
                                completion?(true, false)
                            }
                        }
                    } else {
                        // If the feature is a cluster, it will have `point_count` and `cluster_id` properties. These are assigned
                        // when the cluster is created.
                        // no feature detected
                        self.didDeselectAmenityAnnotation()
                        self.didDeselectAnnotation()
                        completion?(true, false)
                    }
                    case .failure(_):
                        completion?(true, false)
                        break
                    }
                }
            }
        }
    
    func setSelectedState(amenityID: String, type: String) {
//        self.navMapView.mapView.mapboxMap.setFeatureState(sourceId: "__\(type)",
//                                               sourceLayerId: nil,
//                                               featureId: amenityID,
//                                                          state: ["selectedImage": isDarkMode ? "\(Values.DARK_MODE_SELECTED)\(type)" : "\(Values.LIGHT_MODE_SELECTED)\(type)","isSelected":true])
        if let mapLandingModel = self.mapLandingViewModel{
            if mapLandingModel.mobileMapViewUpdatable != nil{
                if type == Values.TRAFFIC {
                    //mapLandingModel.getExistingAmenitiesAndUpdatedOnMap(selectedId: "")
                    mapLandingModel.getExistingTrafficAndUpdateOnMap(selectedID: amenityID)
                } else if type == "ERP" { // reset selection for amenity and traffic camera
                    //mapLandingModel.getExistingAmenitiesAndUpdatedOnMap(selectedId: "")
                   // mapLandingModel.getExistingTrafficAndUpdateOnMap(selectedID: "")
                } else {
                    if type != Values.POI{
                        
                        mapLandingModel.getExistingAmenitiesAndUpdatedOnMap(selectedId: amenityID)
                        //mapLandingModel.getExistingTrafficAndUpdateOnMap(selectedID: "")
                    }
                    
                }
            }
        }
    }
        
    // Resets the previously selected earthquake to be "unselected" if needed.
    func resetPreviouslySelectedStateIfNeeded(currentTappedamenityId: String,type:String) {

        if self.previouslyTappedAmenityId != ""
            && currentTappedamenityId != self.previouslyTappedAmenityId {
            // Reset a previously tapped amenity to be "unselected".
            self.navMapView.mapView.mapboxMap.setFeatureState(sourceId: "__\(type)",
                                                    sourceLayerId: nil,
                                                    featureId: self.previouslyTappedAmenityId,
                                                              state: ["selectedImage": isDarkMode ? "\(Values.DARK_MODE_SELECTED)\(type)" : "\(Values.LIGHT_MODE_SELECTED)\(type)","isSelected":false])
        }
    }
    
    func openCameraToolTip(){
        
        if let mapLandingModel = self.mapLandingViewModel{
            
            if mapLandingModel.trafficFeatureCollection.features.count > 0 {
                
                for feature in mapLandingModel.trafficFeatureCollection.features {
                    
                    if let selectedFeatureProperties = feature.properties,
                       case let .string(trafficID) = selectedFeatureProperties["trafficID"]
                        
                    {
                        if trafficID == mapLandingModel.trafficCameraId{
                            
                            if case let .point(pointData) = feature.geometry{
                                
                                self.showTrafficCallout(feature: feature, location: pointData.coordinates)
                            }
                        }
                    }
                }
            }
        }
    }
    
    func showTrafficCallout(feature:Turf.Feature,location:CLLocationCoordinate2D){
        
        self.didDeselectAnnotation()
        isCameraTapped = true
        if let selectedFeatureProperties = feature.properties,
           case let .string(name) = selectedFeatureProperties["trafficCameraName"],
           case let .string(trafficID) = selectedFeatureProperties["trafficID"]
            
        {
            let amenityID = trafficID
            self.didDeselectAmenityAnnotation()
            let shouldReturn = amenityID == self.previouslyTappedAmenityId
            if shouldReturn {
                return
            }
            mapLandingViewModel?.trafficCameraId = trafficID
            self.previouslyTappedAmenityId = amenityID
            setSelectedState(amenityID: amenityID, type: Values.TRAFFIC)

            let trafficToolTip:TrafficToolTipsView = TrafficToolTipsView(id: amenityID, name: name,location: location)
            trafficToolTip.present(from: self.navMapView.mapView.frame, in: self.navMapView.mapView, constrainedTo: trafficToolTip.frame, animated: true)
            trafficToolTip.center = self.view.center
            trafficToolTip.amenityIdBookMark = amenityID
            self.view.bringSubviewToFront(trafficToolTip)
            self.toolTipCalloutView = trafficToolTip
            self.toolTipCalloutView?.adjust(to: location, in: self.navMapView)
            
            self.adjustCameraWhenAmenityClicked(at: location, callout: trafficToolTip)
            
            ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.SELECT_TRAFFIC_CAMERA, data: ["cameraID":trafficID] as? NSDictionary).subscribe(onSuccess: {_ in
                //We don't need to process this
            },                 onFailure: {_ in
                //We don't need to process this
            })
        }
    }

    func openPOIToolTipView(feature: Turf.Feature, location: CLLocationCoordinate2D) {
        didDeselectAnnotation() // this is for carpark. Consider to move to one dismiss function
        
        if let selectedFeatureProperties = feature.properties,
           case let .string(featureInformation) = selectedFeatureProperties["name"],
           case let .string(featureAddress) = selectedFeatureProperties["address"],
           case let .string(type) = selectedFeatureProperties["type"],
           case let .string(id) = selectedFeatureProperties["id"]
        {
            TagAmenityOnMap(amenityName: type, isNavigate: false, coordinate: location, locationName: featureInformation)
            
            let amenityID = id
            let shouldReturn = amenityID == self.previouslyTappedAmenityId
            self.didDeselectAmenityAnnotation()
            if shouldReturn {
                return
            }
            
            var textAdditionalInfo = ""
            if case let .string(text) = selectedFeatureProperties["textAdditionalInfo"] {
                textAdditionalInfo = text
            }
            
            var lightColor = ""
            if case let .string(color) = selectedFeatureProperties["styleLightColor"] {
                lightColor = color
            }
            
            var darkColor = ""
            if case let .string(color) = selectedFeatureProperties["styleDarkColor"] {
                darkColor = color
            }
            
            
            self.setSelectedState(amenityID: amenityID, type: type)
            
            // Reset a previously tapped to be "unselected".
            //            self.resetPreviouslySelectedStateIfNeeded(currentTappedamenityId: amenityID, type: type)
            
            // TODO: Should use generated id for seletion of symbol
            // I use it here to identify the amenity right now. Need to change for selection of symbol later.
            self.previouslyTappedAmenityId = amenityID
            let poi = POI(id: id, name: featureInformation, address: featureAddress, lat: location.latitude, long: location.longitude, text: textAdditionalInfo, lightColor: lightColor, darkColor: darkColor)
            //TODO: check warking enable here
            //If current location in inner zone => walkingEnabled = true
            var enableWalking = true
            if let outerZoneLocation = SelectedProfiles.shared.getOuterZoneCentrePoint() {
                enableWalking = identifyUserLocInOuterZone(centerCoordinate: outerZoneLocation ,currentLocation: LocationManager.shared.location.coordinate)
            }
            
            let amenityToolTip: POIToolTipsView = POIToolTipsView(poi: poi, walkingEnabled: enableWalking)
            
            amenityToolTip.present(from: self.navMapView.mapView.frame, in: self.navMapView.mapView, constrainedTo: amenityToolTip.frame, animated: enableWalking)
            amenityToolTip.center = self.view.center
            self.view.bringSubviewToFront(amenityToolTip)
            self.toolTipCalloutView = amenityToolTip
            self.toolTipCalloutView?.adjust(to: location, in: self.navMapView)
            
            amenityToolTip.onMoreInfo = { [weak self] poi in
                guard let self = self else { return }
                
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Home.UserClick.poi_more_info, screenName: ParameterName.Home.screen_view)
                
                self.didDeselectAnnotation()
                let carParkData = [
                    "toScreen": "TBRAmenityDetails", // Screen name you want to display when start RN
                    "navigationParams": [ // Object data to be used by that screen
                        "id": poi.id,
                        "idToken":AWSAuth.sharedInstance.idToken,
                        "baseURL": appDelegate().backendURL
                                        ],
                ] as [String : Any]
                let rootView = RNViewManager.sharedInstance.viewForModule(
                    "Breeze",
                    initialProperties: carParkData)
                
                let reactNativeVC = UIViewController()
                reactNativeVC.view = rootView
                reactNativeVC.modalPresentationStyle = .pageSheet
                self.present(reactNativeVC, animated: true, completion: nil)
            }
            
            amenityToolTip.onNavigateHere = { [weak self] poi in
                guard let self = self else { return }
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Home.UserClick.tap_poi, screenName: ParameterName.Home.screen_view)
                
                let location = CLLocation(latitude: poi.lat, longitude: poi.long)
                getAddressFromLocation(location: location) { address in
                    // TODO: Add distance here
                    let address = SearchAddresses(alias: "", address1: poi.name ?? "", lat: "\(poi.lat)", long: "\(poi.long)", address2: address, distance: "\(0)")
                    //  self.delegate?.onCarparkAddressSelected(address: address)
                    let storybard = UIStoryboard(name: "NewRoutePlanning", bundle: nil)
                    if let vc = storybard.instantiateViewController(withIdentifier: String(describing: NewRoutePlanningVC.self)) as? NewRoutePlanningVC {
                        vc.originalAddress = address
                        vc.amenityId = amenityID
                        vc.amenityType = type
                        
                        vc.addressReceived = true
//                        vc.selectedCarPark = theCarpark
                        self.navigationController?.pushViewController(vc, animated: true)
                        // close the tooltips
                        self.didDeselectAnnotation()
                    }
                }
            }
            
            amenityToolTip.onWalkHere = { [weak self] poi in
                guard let self = self else { return }
                self.startWalking(poi: poi)
            }
            
            if let viewModel = self.mapLandingViewModel {
                
                if viewModel.landingMode != Values.PARKING_MODE{
                    
                    //Sending event to RN to collapse the bottom sheet to kee tool tip centre aligned
                    updateBottomSheetIndex(index: 0, toScreen: "Landing")
                }
                
                if(viewModel.bottomSheetIndex == 0){
                    
                    self.adjustZoomOnlyForAmenity(at: location, callout: amenityToolTip, viewModel: viewModel)
                }
                
            }
            else{
                
                self.adjustCameraWhenAmenityClicked(at: location, callout: amenityToolTip)
            }
            
        }
    }
    
    func openERPToolTipView(feature:Turf.Feature,location:CLLocationCoordinate2D){
        
        self.didDeselectAnnotation()
        self.didDeselectAmenityAnnotation()
        if let selectedFeatureProperties = feature.properties,
           case .string(_) = selectedFeatureProperties["currentprice"],
           case .string(_) = selectedFeatureProperties["upcomingtime"],
           case .string(_) = selectedFeatureProperties["upcomingprice"],
           case let .string(erpId) = selectedFeatureProperties["erpid"],
           case .string(_) = selectedFeatureProperties["name"]
            {
          
           
            /// navigating to erp details screen
           let erpCollection = FeatureCollection(features: [feature])
            
            let storyboard = UIStoryboard(name: "ERPDetails", bundle: nil)
            if let vc = storyboard.instantiateViewController(withIdentifier: String(describing: ERPDetails.self)) as? ERPDetails {
                if case let .string(type) = selectedFeatureProperties["type"] {
                    vc.erpType = type
                }
                vc.selectedERPId = Int(erpId) ?? 0
                vc.selectedTimeChanged = false
                vc.selectedERPTime = DirectionService.shared.timeStamp == 0 ? Int(Date().currentTimeInMiliseconds()) :DirectionService.shared.timeStamp
                vc.erpFeatureCollection = erpCollection
                self.navigationController?.pushViewController(vc, animated: true)
            }
//            
//            let amenityID = erpId
//            let shouldReturn = amenityID == self.previouslyTappedAmenityId
//            self.didDeselectAmenityAnnotation()
//            if shouldReturn {
//                return
//            }
//
//            self.previouslyTappedAmenityId = amenityID
//            self.setSelectedState(amenityID: amenityID, type: "ERP")
//
            /*let currentPriceArray = currentprice.components(separatedBy: "$")
            let currentPriceStr = currentPriceArray[1]

            var nextPriceStr = "0.0"
            if(upcomingprice != "")
            {
                let nextPriceArray = upcomingprice.components(separatedBy: "$")
                nextPriceStr = nextPriceArray[1]
            }

            let erpToolTipView = ERPToolTipsView(id: erpId, name: name, rate: Double(currentPriceStr) ?? 0.0, nextText: upcomingtime, nextRate: Double(nextPriceStr) ?? 0.0, location: location)
            erpToolTipView.present(from: self.navMapView.mapView.frame, in: self.navMapView.mapView, constrainedTo: erpToolTipView.frame, animated: true)
            erpToolTipView.center = self.view.center
            self.view.bringSubviewToFront(erpToolTipView)
            self.erpToolTipView = erpToolTipView
            self.erpToolTipView?.adjust(to: location, in: self.navMapView)
            self.adjustCameraWhenAmenityClicked(at: location, callout: erpToolTipView)*/
        }
        
    }
    
    func openDropPinToolTipView(feature: Turf.Feature, location: CLLocationCoordinate2D) {
        self.didDeselectAnnotation()
        self.didDeselectAmenityAnnotation()
        
        /*
         feature.properties = [
             "type":.string(Values.DROPPIN),
             "name":.string(model.name ?? ""),
             "address":.string(model.address ?? ""),
             "isBookmarked": .boolean(isBookmarked)
         ]
         */
        
        if let selectedFeatureProperties = feature.properties,
           case let .string(name) = selectedFeatureProperties["name"],
           case let .string(address) = selectedFeatureProperties["address"],
           case let .boolean(isBookmarked) = selectedFeatureProperties["isBookmarked"],
           case let .string(lat) = selectedFeatureProperties["lat"],
           case let .string(long) = selectedFeatureProperties["long"] {
            
            let model = DropPinModel(name, address: address, lat: lat, long: long)
            
            let dropPinToolTip: DropPinToolTipView = DropPinToolTipView(pinModel: model, location: location)
            dropPinToolTip.isSaved = isBookmarked
            dropPinToolTip.needShowCloseButton = true
            
            dropPinToolTip.onCloseAction = {[weak self] in
                self?.didDeselectAnnotation()
                self?.didDeselectAmenityAnnotation()
            }
            
            dropPinToolTip.onSaveAction = { isSave in
                
            }
            
            dropPinToolTip.present(from: self.navMapView.mapView.frame, in: self.navMapView.mapView, constrainedTo: dropPinToolTip.frame, animated: true)
            dropPinToolTip.center = self.view.center
            
            self.view.bringSubviewToFront(dropPinToolTip)
            self.toolTipCalloutView = dropPinToolTip
            self.toolTipCalloutView?.adjust(to: location, in: self.navMapView)
            
            dropPinToolTip.onNavigationHere = { [weak self] (name,address,coordinate) in
                guard let self = self else { return }
                let address = SearchAddresses(alias: "", address1: name, lat: "\(coordinate.latitude)", long: "\(coordinate.longitude)", address2: address, distance: "0")
                
                let storybard = UIStoryboard(name: "NewRoutePlanning", bundle: nil)
                if let vc = storybard.instantiateViewController(withIdentifier: String(describing: NewRoutePlanningVC.self)) as? NewRoutePlanningVC {
                    vc.originalAddress = address
                    vc.addressReceived = true
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
            
            
            dropPinToolTip.shareViaLocation = { 
                shareLocationFromNative(address1: address, address2: "", latitude: lat, longtitude: long, code: "", is_destination: false, amenity_id: "", amenity_type: "", description: "", name: name)
            }
            
            dropPinToolTip.inviteLocation = {
                inviteLocationFromNative(address1: address, address2: "", latitude: lat, longtitude: long, code: "", is_destination: false, amenity_id: "", amenity_type: "", description: "", name: name)
            }
        }
    }
    
    func openAmenityToolTipView(feature:Turf.Feature,location:CLLocationCoordinate2D){
        didDeselectAnnotation() // this is for carpark. Consider to move to one dismiss function
        
        if let selectedFeatureProperties = feature.properties,
           case let .string(featureInformation) = selectedFeatureProperties["name"],
           case let .string(featureAddress) = selectedFeatureProperties["address"],
           case let .string(type) = selectedFeatureProperties["type"],
           case let .string(id) = selectedFeatureProperties["id"],
           case let .number(startDate) = selectedFeatureProperties["startDate"],
           case let .number(endDate) = selectedFeatureProperties["endDate"],
           case let .number(bookmarkId) = selectedFeatureProperties["bookmarkId"],
           case let .string(poiID) = selectedFeatureProperties["poiID"],
           case let .string(originalType) = selectedFeatureProperties["originalType"],
           case let .string(textAdditionalInfo) = selectedFeatureProperties["textAdditionalInfo"],
           case let .string(styleLightColor) = selectedFeatureProperties["styleLightColor"],
           case let .string(styleDarkColor) = selectedFeatureProperties["styleDarkColor"],
           case let .boolean(isBookmarked) = selectedFeatureProperties["isBookmarked"] {
            TagAmenityOnMap(amenityName: type, isNavigate: false, coordinate: location, locationName: featureInformation)
            
            let amenityID = id
            let shouldReturn = amenityID == self.previouslyTappedAmenityId
            self.didDeselectAnnotation()
            self.didDeselectAmenityAnnotation()
            if shouldReturn {
                return
            }
            
            self.setSelectedState(amenityID: amenityID, type: type)
            
            // Reset a previously tapped to be "unselected".
//            self.resetPreviouslySelectedStateIfNeeded(currentTappedamenityId: amenityID, type: type)
            
            // TODO: Should use generated id for seletion of symbol
            // I use it here to identify the amenity right now. Need to change for selection of symbol later.
            self.previouslyTappedAmenityId = amenityID
            
            let amenityToolTip: AmenityToolTipsView = type == "pasarmalam" ? PasarMalamToolTipsView(id: amenityID, name: featureInformation, address: featureAddress, startDate: startDate, endDate: endDate, location: location) : AmenityToolTipsView(id: amenityID, name: featureInformation, address: featureAddress, location:location, type: type, text: textAdditionalInfo, styleLightColor: styleLightColor, styleDarkColor: styleDarkColor, isShareEnable: true)
            amenityToolTip.bookmarkId = Int(bookmarkId)
            amenityToolTip.amenityIdBookMark = poiID
            amenityToolTip.onSaveAction = { isSave in
                sendSaveActionToRN(amenityId: poiID,
                                   lat: location.latitude,
                                   long: location.longitude,
                                   name:  featureInformation,
                                   address1: featureAddress,
                                   address2:"",
                                   amenityType: originalType,
                                   fullAddress: "",
                                   isDestination: false,
                                   isSelected: !isBookmarked)
            }
            amenityToolTip.present(from: self.navMapView.mapView.frame, in: self.navMapView.mapView, constrainedTo: amenityToolTip.frame, animated: true)
            amenityToolTip.center = self.view.center
            
            self.view.bringSubviewToFront(amenityToolTip)
            self.toolTipCalloutView = amenityToolTip
            self.toolTipCalloutView?.adjust(to: location, in: self.navMapView)
            
            amenityToolTip.isSaved = isBookmarked
            
            amenityToolTip.onNavigationHere = { [weak self] (name,address,coordinate) in
                guard let self = self else { return }
                self.TagAmenityOnMap(amenityName: type, isNavigate: true, coordinate: coordinate, locationName: featureInformation)
                let location = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
                getAddressFromLocation(location: location) { address in
                    let address = SearchAddresses(alias: "", address1: featureInformation, lat: "\(coordinate.latitude)", long: "\(coordinate.longitude)", address2: address, distance: "0")
                    //  self.delegate?.onCarparkAddressSelected(address: address)
                    let storybard = UIStoryboard(name: "NewRoutePlanning", bundle: nil)
                    if let vc = storybard.instantiateViewController(withIdentifier: String(describing: NewRoutePlanningVC.self)) as? NewRoutePlanningVC {
                        vc.originalAddress = address
                        vc.amenityId = poiID
                        vc.amenityType = type
                        vc.addressReceived = true
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
                
            }
            
            let stringLat =   location.latitude.toString()
            let stringLong = location.longitude.toString()
            
            amenityToolTip.shareViaLocation = {
                shareLocationFromNative(address1: featureAddress, address2: "", latitude: stringLat, longtitude: stringLong, code: "", is_destination: false, amenity_id: poiID, amenity_type: originalType, description: "", name: featureInformation)
                
            }
            
            amenityToolTip.inviteLocation = {
                inviteLocationFromNative(address1: featureAddress, address2: "", latitude: stringLat, longtitude: stringLong, code: "", is_destination: false, amenity_id: poiID, amenity_type: originalType, description: "", name: featureInformation)
            }
            
            if let viewModel = self.mapLandingViewModel{

//                if viewModel.landingMode != Values.PARKING_MODE{
//
//                    //Sending event to RN to collapse the bottom sheet to kee tool tip centre aligned
//                    updateBottomSheetIndex(index: 0, toScreen: "Landing")
//                }
//
                updateBottomSheetIndex(index: 0, toScreen: "Landing")
                if(viewModel.bottomSheetIndex == 0){
                    
                    self.adjustZoomOnlyForAmenity(at: location, callout: amenityToolTip, viewModel: viewModel)
                }
                
            }
            else{
                
                self.adjustCameraWhenAmenityClicked(at: location, callout: amenityToolTip)
            }
        }
    }
    
    func getActualLocation() -> CLLocationCoordinate2D? {
        
        if let viewModel = self.mapLandingViewModel {
            if viewModel.landingMode == Values.NONE_MODE || viewModel.landingMode == Values.PARKING_MODE || viewModel.landingMode == Values.OBU_MODE{
                if self.shouldUpdateCarparks() {
                    return viewModel.centerMapLocation
                } else if viewModel.isSearchDisocver, let location = viewModel.searchDiscoverLocation {
                    return location
                } else {
                    if SelectedProfiles.shared.isProfileSelected(){
                        if let profileLocation = SelectedProfiles.shared.getOuterZoneCentrePoint(){
                            return profileLocation
                        }
                    } else {
                        return LocationManager.shared.location.coordinate
                    }
                }
            } else if viewModel.landingMode == Values.TRAFFIC_MODE {
                if viewModel.isSearchDisocver, let location = viewModel.searchDiscoverLocation {
                    return location
                } else {
                    if SelectedProfiles.shared.isProfileSelected(){
                        if let profileLocation = SelectedProfiles.shared.getOuterZoneCentrePoint(){
                            return profileLocation
                        }
                    } else {
                        return LocationManager.shared.location.coordinate
                    }
                }
            } else {
                if SelectedProfiles.shared.isProfileSelected(){
                    if let profileLocation = SelectedProfiles.shared.getOuterZoneCentrePoint(){
                        return profileLocation
                    }
                } else {
                    return LocationManager.shared.location.coordinate
                }
            }
        }
        return nil
    }
    
    func openCarparkToolTipView(carpark: Carpark, tapped: Bool = false, actualLocation: CLLocationCoordinate2D? = nil, needShowBottomSheet: Bool = false) {
        DispatchQueue.main.async { [weak self] in
            
            guard let self = self else { return }
            
            let sameCarpark = carpark.id == self.currentSelectedCarpark?.id
            
            self.didDeselectAnnotation()
            self.didDeselectAmenityAnnotation()
            
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Home.UserClick.tap_carpark, screenName: ParameterName.Home.screen_view)
            
            if sameCarpark {
                self.currentSelectedCarpark = nil
                // click the same one, don't show callout
                return
            }
            self.currentSelectedCarpark = carpark
            
            // set camera (actually should not set if not because of tapped
            if tapped {
                let cameraOptions = CameraOptions(center: CLLocationCoordinate2D(latitude: carpark.lat ?? 0, longitude: carpark.long ?? 0), zoom: self.navMapView.mapView.mapboxMap.cameraState.zoom) // using current zoom level
                self.navMapView.mapView.camera.ease(to: cameraOptions, duration: Values.standardAnimationDuration, curve: .easeInOut, completion: nil)
                
                if let location = actualLocation {
                    self.sentEventParkingBottomSheetData(idCarPark: carpark.id ?? "", isDestinationHasCarPark: self.isDestinationHasCarPark, focusedLat: location.latitude.toString(), focusedLong: location.longitude.toString())
                }
            }
            
            let callOutView = CarparkToolTipsView(carpark: carpark, calculateFee: !self.carparkFromBottomSheet, routeablePoint: self.mapLandingViewModel?.selectedRoutablePoint)
            
            callOutView.bookmarkId = carpark.bookmarkId
            callOutView.amenityIdBookMark = carpark.id
            callOutView.onSaveAction = { saved in
                sendSaveActionToRN(amenityId: carpark.id ?? "",
                                   lat: carpark.lat ?? 0,
                                   long: carpark.long ?? 0,
                                   name:  carpark.name ?? "",
                                   address1: carpark.name ?? "",
                                   address2:"",
                                   amenityType: Values.AMENITYCARPARK,
                                   fullAddress: "",
                                   isDestination: false,
                                   isSelected: !(carpark.isBookmarked ?? true))
            }
            
            callOutView.present(from: self.navMapView.mapView.frame, in: self.navMapView.mapView, constrainedTo: callOutView.frame, animated: true)
            self.toolTipCalloutView = callOutView
            self.toolTipCalloutView?.adjust(to: callOutView.tooltipCoordinate, in: self.navMapView)
            
            callOutView.isSaved = carpark.isBookmarked ?? false
            
            if let viewModel = self.mapLandingViewModel{
                
                if viewModel.landingMode != Values.PARKING_MODE || (viewModel.landingMode == Values.PARKING_MODE && !needShowBottomSheet) {
                    
                    //Sending event to RN to collapse the bottom sheet to kee tool tip centre aligned
                    updateBottomSheetIndex(index: 0, toScreen: "Landing")
                    
                    if(viewModel.bottomSheetIndex == 0){
                        
                        self.adjustZoomOnlyForAmenity(at: callOutView.tooltipCoordinate, callout: callOutView, viewModel: viewModel)
                    }
                    
                } else {
                    
                    self.adjustZoomOnlyForAmenity(at: callOutView.tooltipCoordinate, callout: callOutView, viewModel: viewModel)
                }
                
            }
            
            callOutView.onCarparkSelectedV2 = { [weak self] (theCarpark, routeablePoint) in
                guard let self = self else { return }
                
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Home.UserClick.navigate_here_parking, screenName: ParameterName.Home.screen_view)
                
                let newLocation = theCarpark.getLocation(routeablePoint)
                
                let eventValue = "navigate_here_\(theCarpark.name ?? "")"
                AnalyticsManager.shared.sendLocationData(eventValue: eventValue, screenName: ParameterName.Home.screen_view, locationName: theCarpark.name ?? "", latitude: (theCarpark.lat ?? 0).toString(), longitude:(theCarpark.long ?? 0).toString())
                
                let location = CLLocation(latitude: newLocation.latitude, longitude: newLocation.longitude)
                getAddressFromLocation(location: location) { address in
                    let address = SearchAddresses(alias: "", address1: theCarpark.name ?? "", lat: newLocation.latitude.toString(), long: newLocation.longitude.toString(), address2: address, distance: "\(theCarpark.distance ?? 0)", carparkId: theCarpark.id ?? "")
                    //  self.delegate?.onCarparkAddressSelected(address: address)
                    let storybard = UIStoryboard(name: "NewRoutePlanning", bundle: nil)
                    if let vc = storybard.instantiateViewController(withIdentifier: String(describing: NewRoutePlanningVC.self)) as? NewRoutePlanningVC {
                        vc.originalAddress = address
                        vc.addressReceived = true
                        vc.selectedCarPark = theCarpark
                        vc.carparkId = theCarpark.id ?? ""
                        self.navigationController?.pushViewController(vc, animated: true)
                        // close the tooltips
                        //self.didDeselectAnnotation()
                    }
                }
            }
            callOutView.onCalculate = { [weak self] theCarpark in
                guard let self = self else { return }
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Home.UserClick.parking_calculatefee, screenName: ParameterName.Home.screen_view)
                
                //                    let data = [
                //                        "toScreen": "OpenCaculateFeeCarpark", // Screen name you want to display when start RN
                //                        "navigationParams": [ // Object data to be used by that screen
                //                            "parkingID": theCarpark.id
                //                        ]
                //                    ] as [String : Any]
                //                    let rootView = RNViewManager.sharedInstance.viewForModule(
                //                        "Breeze",
                //                        initialProperties: data)
                //
                //                    let reactNativeVC = UIViewController()
                //                    reactNativeVC.view = rootView
                //                    reactNativeVC.modalPresentationStyle = .pageSheet
                //                    self?.present(reactNativeVC, animated: true, completion: nil)
                
                // BREEZES-8546 Logic to get current location in parking calculator
                if self.shouldUpdateCarparks() {
                    self.sentEventCalculateData(idCarPark: theCarpark.id ?? "", focusedLat: self.mapLandingViewModel?.centerMapLocation?.latitude.toString() ?? "", focusedLong: self.mapLandingViewModel?.centerMapLocation?.longitude.toString() ?? "")
                } else if self.mapLandingViewModel?.isSearchDisocver ?? false, let location = self.mapLandingViewModel?.searchDiscoverLocation {
                    self.sentEventCalculateData(idCarPark: theCarpark.id ?? "", focusedLat: location.latitude.toString(), focusedLong: location.longitude.toString())
                } else {
                    if let profileLocation = SelectedProfiles.shared.getOuterZoneCentrePoint(){
                        self.sentEventCalculateData(idCarPark: theCarpark.id ?? "", focusedLat: profileLocation.latitude.toString(), focusedLong: profileLocation.longitude.toString())
                    }
                }
                
            }
            callOutView.onMoreInfo = { [weak self]  theCarpark in
                guard let self = self else { return }
                
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Home.UserClick.parking_more_info, screenName: ParameterName.Home.screen_view)
                
                self.showCarparkDetail(theCarpark.id)
            }
            let stringLat =  carpark.lat?.toString()
            let stringLong =  carpark.long?.toString()
            
            
            callOutView.shareViaLocation = {
                shareLocationFromNative(address1: carpark.name ?? "", address2: "", latitude: stringLat ?? "", longtitude: stringLong ?? "", code: "", is_destination: false, amenity_id: carpark.id ?? "", amenity_type: Values.AMENITYCARPARK, description: "", name: carpark.name ?? "")
                
            }
            
            callOutView.inviteLocation = {
                inviteLocationFromNative(address1: carpark.name ?? "", address2: "", latitude: stringLat ?? "", longtitude: stringLong ?? "", code: "", is_destination: false, amenity_id: carpark.id ?? "", amenity_type: Values.AMENITYCARPARK, description: "", name: carpark.name ?? "")
            }
        }
    }
    
    func showCarparkDetail(_ carparkId: String?) {
        if let ID = carparkId, !ID.isEmpty {
            let carParkData = [
                "toScreen": "ParkingPriceDetail", // Screen name you want to display when start RN
                "navigationParams": [ // Object data to be used by that screen
                    "parkingId": ID,
                    "idToken":AWSAuth.sharedInstance.idToken,
                    "baseURL": appDelegate().backendURL
                                    ],
            ] as [String : Any]
            let rootView = RNViewManager.sharedInstance.viewForModule(
                "Breeze",
                initialProperties: carParkData)
            
            let reactNativeVC = UIViewController()
            reactNativeVC.view = rootView
            reactNativeVC.modalPresentationStyle = .pageSheet
            self.present(reactNativeVC, animated: true, completion: nil)
        }
    }

    func TagAmenityOnMap(amenityName: String, isNavigate: Bool, coordinate: CLLocationCoordinate2D?, locationName: String = ""){
        
        if(isNavigate){
            let eventValue = "navigate_here_amenities_\(amenityName)"
            AnalyticsManager.shared.logClickEvent(eventValue: eventValue, screenName: ParameterName.Home.screen_view)
            AnalyticsManager.shared.sendLocationData(eventValue: eventValue, screenName: ParameterName.Home.screen_view, locationName: locationName, latitude: (coordinate?.latitude ?? 0).toString(), longitude:(coordinate?.longitude ?? 0).toString())
        }
        else{
            let eventValue = "map_\(amenityName)_icon_click"
            AnalyticsManager.shared.logClickEvent(eventValue: eventValue, screenName: ParameterName.Home.screen_view)
            AnalyticsManager.shared.sendLocationData(eventValue: eventValue, screenName: ParameterName.Home.screen_view, locationName: locationName, latitude: (coordinate?.latitude ?? 0).toString(), longitude:(coordinate?.longitude ?? 0).toString())
        }
       
      
    }
    
    
    func didDeselectAmenityAnnotation() {
        if let callout = self.toolTipCalloutView {
            callout.dismiss(animated: true)
            callout.removeFromSuperview()
            self.isCameraTapped = false
            self.toolTipCalloutView = nil
        }
        previouslyTappedAmenityId = ""
    }
    
    
    // Show Pop-UP Sucssess OBU Connected
    
//    func showPopUpSucssessObuConnect() {
//        let storyboard = UIStoryboard(name: "OBU", bundle: nil)
//        if let vc = storyboard.instantiateViewController(withIdentifier: String(describing: PairingSucsessPopUpViewController.self)) as? PairingSucsessPopUpViewController {
//            vc.modalPresentationStyle = .fullScreen
//            self.navigationController?.present(vc, animated: true)
//        }
//    }
    
    func adjustTooltipToCentre(){
        
        var isUpdateZoom = false
        if let mapLandingModel = self.mapLandingViewModel{
            
            if let calloutView = self.toolTipCalloutView {
                isUpdateZoom = true
                self.adjustZoomOnlyForAmenity(at: calloutView.tooltipCoordinate, callout: calloutView, viewModel: mapLandingModel)
            }
            
            if(isUpdateZoom == false){
                if(self.currentGlobalNotification == nil){
                    self.resetCamera()
                }
            }
        }
    }
    
    private func adjustCameraWhenAmenityClicked(at location: LocationCoordinate2D, callout: ToolTipsView) {
        let cameraOptions = CameraOptions(center: location) // using current zoom level
        self.navMapView.mapView.camera.ease(to: cameraOptions, duration: Values.standardAnimationDuration, curve: .easeInOut, completion: nil)
        
        callout.adjust(to: location, in: self.navMapView)
        
        isTrackingUser = true

    }
    
     func adjustZoomOnlyForAmenity(at location: LocationCoordinate2D, callout: ToolTipsView?,viewModel:MapLandingViewModel){
        
        if viewModel.isSearchDisocver, let searchLocation = viewModel.searchDiscoverLocation {
            
            self.navMapView.mapView.setDynamicZoomLevelBasedonRadius(coordinate1: location, dynamicRadius: Values.toolTipDistance, padding: UIEdgeInsets(top: 340, left: 40, bottom: CGFloat(viewModel.bottomSheetHeight) + 160, right: 40), coordinate2: searchLocation,openToolTip: true)
        }
        else{
            
            if SelectedProfiles.shared.selectedProfileName() != "" {
                
                if let profileCoordinate = SelectedProfiles.shared.getOuterZoneCentrePoint(){
                    
                    self.navMapView.mapView.setDynamicZoomLevelBasedonRadius(coordinate1: location, dynamicRadius: Values.toolTipDistance , padding: UIEdgeInsets(top: 340, left: 40, bottom:  CGFloat(viewModel.bottomSheetHeight) + 160, right: 40), coordinate2: profileCoordinate,openToolTip: true)
                }
                
            }
            else{
                
                self.navMapView.mapView.setDynamicZoomLevelBasedonRadius(coordinate1: location, dynamicRadius: Values.toolTipDistance, padding: UIEdgeInsets(top: 340, left: 40, bottom:  CGFloat(viewModel.bottomSheetHeight) + 160, right: 40), coordinate2: LocationManager.shared.location.coordinate,openToolTip: true)
            }
            
        }
        
        callout?.adjust(to: location, in: self.navMapView)
//        isTrackingUser = true
    }
    
    func sentEventCalculateData(idCarPark: String, focusedLat: String, focusedLong: String) {
        let _ = ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.OPEN_CALCULATE_FEE_CARPARK, data: ["parkingID": idCarPark, Values.FOCUSED_LAT: focusedLat, Values.FOCUSED_LONG: focusedLong] as? NSDictionary)
            .subscribe(onSuccess: { _ in
                //We don't need to process this
            }, onFailure: { _ in
                //We don't need to process this
            })
    }
    
    func sentEventParkingBottomSheetData(idCarPark: String, isDestinationHasCarPark: Bool, focusedLat: String, focusedLong: String) {
        let _ = ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.PARKING_BOTTOM_SHEET_REFRESH, data: ["parkingID": idCarPark, "isDestinationHasCarPark": isDestinationHasCarPark , Values.FOCUSED_LAT: focusedLat, Values.FOCUSED_LONG: focusedLong] as? NSDictionary)
            .subscribe(onSuccess: { _ in
                //We don't need to process this
            }, onFailure: { _ in
                //We don't need to process this
            })
    }
}

//
//  MapLandingViewModel.swift
//  Breeze
//
//  Created by Zhou Hao on 30/9/21.
//

import Foundation
import Turf
import MapboxNavigation
import MapboxCoreNavigation
import MapboxMaps
import MapboxCoreMaps
import SwiftyBeaver
import Combine
import CoreLocation
import UIKit

protocol MapLandingViewModelDelegate: NSObjectProtocol {
    func addedProfile(_ viewModel: MapLandingViewModel, name: String)
    func onTriggerCruiseMode20Km()
}

final class MapLandingViewModel: NSObject {
    
    struct MapLandingViewModelConst {
        static let timeShowTutorial: TimeInterval = 3
    }
    
    // MARK: - Public properties
    var mobileMapViewUpdatable: MapLandingViewUpdatable? {
        didSet {
            if mobileMapViewUpdatable != nil {
                mobileMapViewUpdatable?.delegate = self
                syncUpdate(mapViewUpdatable: mobileMapViewUpdatable!)
            }
        }
    }
    var carPlayMapViewUpdatable: CarPlayMapViewUpdatable? {
        didSet {
            if carPlayMapViewUpdatable != nil {
                carPlayMapViewUpdatable?.delegate = self
                syncUpdate(mapViewUpdatable: carPlayMapViewUpdatable!,override: false)
            }
        }
    }
    
    weak var delegate: MapLandingViewModelDelegate?
    
    var isSearchDisocver = false
    var didShowNearByNotification = false
    var isEventFromInbox = false
    // TODO: Refactor - to create a model for search result
    var searchDiscoverLocation:CLLocationCoordinate2D?
    var centerMapLocation: CLLocationCoordinate2D?
    var destName = ""
    var placeId = ""
    var availablePercentImageBand = ""
    var availabilityCSData: AvailabilityCSData?
    var searchAddress = "" // current searched address
    var fullAddress = ""
    var isSearchAddressBookmarked = false
    var bookmarkId:Int?
    var carparkId: String? = nil
    var selectedRoutablePoint: RoutablePointModel?
        
    var amenityCancellable: AnyCancellable?
    var cruiseDelayCancellable: AnyCancellable?
    var isNavigationStarted = false
    // MARK: - Private properties
    private var currentTrafficIncidentsFeatures: Turf.FeatureCollection?
    private var currentCostERPFeatures: Turf.FeatureCollection?
    private var lastLocation: CLLocation?
    
    var bottomSheetHeight = 0
    var bottomSheetIndex = 1
    var trafficReceivedData: [Any]?
    var landingMode = Values.NONE_MODE
    var isShowingMap = true
    var trafficCameraId = ""
    var distanceCompare = 0
    
    var selectedERPTime = 0 {
        didSet {
            
            if(landingMode == Values.ERP_MODE){
                self.updateERPOnMap()
            }
            
        }
    }
    var trafficFeatureCollection = Turf.FeatureCollection(features: [])
    var amenitySymbolThemeType = Values.LIGHT_MODE_UN_SELECTED
    var newContentIds: [Int] = []
    
    private(set) var shouldAllowStartCruise = false
    private(set) var disableCruise = false // from RN
    var erpCoordinates = [CLLocationCoordinate2D]()
    var nearByERPFilter = [NearByERPFilter]()
    private var tripLogger: TripLogger? // only available when cruise mode start
    private(set) var isLiveLocationSharingEnabled = false
    private var isTrackingUser = true
//    private(set) var isCruiseStoppedByUser = false
    private var isMute = !Prefs.shared.getBoolValue(.enableAudio)
    private var disposables = Set<AnyCancellable>()
    
    #if DEMO
    private var amenityService = AmenityService()
    private var amenitiesFeatureCollection: FeatureCollection?
    #endif
    
    private var tutorialTimer: Timer?
    
    //  MARK: - Drop Pin
    var isOnDropPin: Bool = false {
        didSet {
            if !isOnDropPin {
                self.removePinLocation()
            }
        }
    }
    var dropPinModel: DropPinModel? {
        didSet {
            if let pinModel  = self.dropPinModel {
                checkDropPinBookmark(pinModel)
            } else {
                removePinLocation()
            }
        }
    }
    
    private var amenityTypes: [String] = []
    private var didRegisterAmenityUpdates = false
    
    //  MARK: - Show nearest carparks at launch time
    var didShowNearestCarparkAtLaunchTime: Bool = false
        
    // MARK: - Methods
    override init() {
        super.init()
        LocationManager.shared.delegate = self
        LocationManager.shared.getLocation()
        LocationManager.shared.getCruiseModeSpeed()        
        
        NotificationCenter.default.addObserver(self, selector: #selector(carplayMapLoaded(_:)), name: .carplayMapLoaded, object: nil)
        
        addNotifications()

        #if DEMO
        demoAmenities()
        #endif
        
        // For debug testing
//        LocationManager.shared.startSimulateCarMoving(after: 30)
    }
          
    deinit {
        SwiftyBeaver.debug("MapLandingViewModel deinit")
        removeNotifications()
        DataCenter.shared.removeERPUpdaterListner(listner: self)
//        DataCenter.shared.removeTrafficUpdaterListener(listener: self)
    }
    
    private func isOnMobile() -> Bool {
        var retValue: Bool = false
        if let _ = self.delegate as? MapLandingVC {
            SwiftyBeaver.debug("Mobile app is openning")
            retValue = true
        } else {
            SwiftyBeaver.debug("Mobile app is not openning")
            retValue = false
        }
        return retValue
    }
    
    func getActualBottomSheetHeight() -> CGFloat {
        return CGFloat(bottomSheetHeight) + Values.bottomTabHeight + 20
    }
    
    private func addNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(onNavigationStarted), name: Notification.Name(Values.notificationNavigationStart), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(onNavigationStopped), name: Notification.Name(Values.notificationNavigationStop), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(onSearchLocationReceived), name: Notification.Name(Values.searchLocationReceived), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(onSearchClear), name: Notification.Name(Values.handleClearSearch), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(onAmenitySelection), name: Notification.Name(Values.handleAmenitySelection), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(onTrafficDataReceived), name: Notification.Name(Values.receivedTrafficData), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(onTrafficIDSelection), name: Notification.Name(Values.receivedTrafficID), object: nil)
        
//        NotificationCenter.default.publisher(for: Notification.Name(Values.NotificationDisableCruise), object: nil)
//            .receive(on: DispatchQueue.main)
//            .sink { [weak self] notification in
//                guard let self = self else { return }
//                self.disableCruise = notification.userInfo![Values.DisableCruiseKey] as! Bool
//            }.store(in: &disposables)
        
        NotificationCenter.default.addObserver(self, selector: #selector(onAmenitySelection), name: Notification.Name(Values.NotificationAmenitiesPetrolAndEV), object: nil)
    }
    
    private func removeNotifications() {
        NotificationCenter.default.removeObserver(self, name: Notification.Name(Values.notificationNavigationStart), object: nil)
        NotificationCenter.default.removeObserver(self, name: Notification.Name(Values.notificationNavigationStop), object: nil)
        
        NotificationCenter.default.removeObserver(self, name: Notification.Name(Values.searchLocationReceived), object: nil)
        
        NotificationCenter.default.removeObserver(self, name: Notification.Name(Values.handleClearSearch), object: nil)
        
        NotificationCenter.default.removeObserver(self, name: Notification.Name(Values.receivedTrafficID), object: nil)
        
        NotificationCenter.default.removeObserver(self, name: Notification.Name(Values.receivedTrafficData), object: nil)
        NotificationCenter.default.removeObserver(self, name: Notification.Name(Values.NotificationAmenitiesPetrolAndEV), object: nil)
    }
    
    #if DEMO
    private func demoAmenities() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [weak self] in // To guarantee get location
            guard let self = self else { return }
            
            var requests = [AmenityRequest]()
            
            // get data from user defauts, ignore it by now
/*
            if let items = UserDefaults.standard.array(forKey: "amenitiesSelected") as? [String] {
                for item in items { // only 8 supported now. So I filter out what we don't need.
                    if item == Amenities.CodingKeys.petrol.rawValue {
                        requests.append(AmenityRequest(type: item, filter: nil))
                    } else if item == Amenities.CodingKeys.evcharger.rawValue {
                        requests.append(AmenityRequest(type: item, filter: nil))
                    } else if item == Amenities.CodingKeys.hawkercentre.rawValue {
                        requests.append(AmenityRequest(type: item, filter: nil))
                    } else if item == Amenities.CodingKeys.parks.rawValue {
                        requests.append(AmenityRequest(type: item, filter: nil))
                    } else if item == Amenities.CodingKeys.library.rawValue {
                        requests.append(AmenityRequest(type: item, filter: nil))
                    } else if item == Amenities.CodingKeys.museum.rawValue {
                        requests.append(AmenityRequest(type: item, filter: nil))
                    } else if item == Amenities.CodingKeys.heritagetree.rawValue {
                        requests.append(AmenityRequest(type: item, filter: nil))
                    }
                }
            }
 */
  
            // This is for testing only
            requests = [
                AmenityRequest(type: "petrol", filter: nil),
                AmenityRequest(type: "evcharger", filter: nil),
                AmenityRequest(type: "parks", filter: nil),
                AmenityRequest(type: "hawkercentre", filter: nil),
                AmenityRequest(type: "library", filter: nil),
                AmenityRequest(type: "museum", filter: nil),
                AmenityRequest(type: "heritagetree", filter: nil)
            ]
            
            let request = AmenitiesRequest(latitude: LocationManager.shared.location.coordinate.latitude, longitude: LocationManager.shared.location.coordinate.longitude, radius: 3, arrivalTime: 1, destinationName: "", amenities: requests)

            self.amenityService.getAmenities(request: request) { result in
                switch result {
                case .failure(let error):
                    SwiftyBeaver.error("Failed to getAmenities: \(error.localizedDescription)")
                case .success(let amenities):
                    
                    var allFeatures = [Turf.Feature]()
                    if let petrol = amenities.petrol {
                        allFeatures.append(contentsOf: self.parseAmenities(type: Amenities.CodingKeys.petrol.rawValue, amenities: petrol))
                    }
                    if let evcharger = amenities.evcharger {
                        allFeatures.append(contentsOf: self.parseAmenities(type: Amenities.CodingKeys.evcharger.rawValue, amenities: evcharger))
                    }
                    if let hawkercenter = amenities.hawkercentre {
                        allFeatures.append(contentsOf: self.parseAmenities(type: Amenities.CodingKeys.hawkercentre.rawValue, amenities: hawkercenter))
                    }
                    if let library = amenities.library {
                        allFeatures.append(contentsOf: self.parseAmenities(type: Amenities.CodingKeys.library.rawValue, amenities: library))
                    }
                    if let parks = amenities.parks {
                        allFeatures.append(contentsOf: self.parseAmenities(type: Amenities.CodingKeys.parks.rawValue, amenities: parks))
                    }
                    if let museum = amenities.museum {
                        allFeatures.append(contentsOf: self.parseAmenities(type: Amenities.CodingKeys.museum.rawValue, amenities: museum))
                    }
                    if let heritagetree = amenities.heritagetree {
                        allFeatures.append(contentsOf: self.parseAmenities(type: Amenities.CodingKeys.heritagetree.rawValue, amenities: heritagetree))
                    }
                    self.amenitiesFeatureCollection = FeatureCollection(features: allFeatures)
                    self.updateAmenities()
                }
            }
        }
    }
    #endif
    
    func startCruise(fav: Favourites? = nil) {
        
        if appDelegate().cruiseViewModel == nil {
            SwiftyBeaver.debug("startCruise - with ETA \(fav != nil ? "YES":"NO")")
            
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Cruise.UserClick.start_cruise, screenName: ParameterName.Cruise.screen_view)
            
            
            appDelegate().enterCruise(etaFav: fav)
        }
    }
    
    // MARK: - Navigation notification
    @objc func onNavigationStarted() {
        isNavigationStarted = true
    }
    
    @objc func onNavigationStopped() {
        
        isNavigationStarted = false
        landingMode = Values.PARKING_MODE
        
        ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.NAVIGATE_TO_HOME_TAB, data: nil)
           .subscribe(onSuccess: { _ in
               //We don't need to process this
           },
                      onFailure: { _ in
               //We don't need to process this
           })
    }
    
    @objc func onSearchClear() {
        
        // When onselected TBR amenity, this will be call first, then it will cause issue. So I add a 0.5 delay here.
       // DispatchQueue.main.asyncAfter(deadline: .now()+0.5) {
                        
        self.searchDiscoverLocation = nil
        self.isSearchDisocver = false
        
        if self.isSearchDisocver {
            self.mobileMapViewUpdatable?.resetCamera(isCruise: false,bottomSheetHeight: self.bottomSheetHeight)
            
            if let mobileMapViewUpdatable = self.mobileMapViewUpdatable {
                mobileMapViewUpdatable.navigationMapView?.pointAnnotationManager?.annotations = [PointAnnotation]()
            }
            if(self.landingMode == Values.ERP_MODE){
                
                self.updateERPOnMap()
            }
            else if(self.landingMode == Values.NONE_MODE || self.landingMode == Values.OBU_MODE){
                
                self.removeERPLayerFromMap()
                self.mobileMapViewUpdatable?.removeAmenitiesFromMap(type: Values.TRAFFIC)
                
                if(SelectedProfiles.shared.isProfileSelected()){
                    if let centreLocation = SelectedProfiles.shared.getOuterZoneCentrePoint(){
                        //                            AmenitiesSharedInstance.shared.removeSpecificAmenity(amenityName: Values.POI)
                        self.removeAmenitiesFromMap()
                        AmenitiesSharedInstance.shared.callAmenitiesAPI(location: centreLocation)
                    }
                }
                else{
                    self.removeAmenitiesFromMap()
                    AmenitiesSharedInstance.shared.removeSpecificAmenity(amenityName: Values.POI)
                    self.mobileMapViewUpdatable?.removeAmenitiesFromMap(type: "\(Values.POI)")
                    AmenitiesSharedInstance.shared.callAmenitiesAPI(location: LocationManager.shared.location.coordinate)
                }
                
            }
            else if(self.landingMode == Values.TRAFFIC_MODE){
                
                self.trafficReceivedData = nil
                self.mobileMapViewUpdatable?.removeAmenitiesFromMap(type: Values.TRAFFIC)
            }
            
        }
        
        NotificationCenter.default.post(name: Notification.Name("SearchClearFromMapModel"), object: nil,userInfo: nil)
            
        //}
    }
    
    @objc
    func onAmenitySelection(_ notification:NSNotification){
        
        if(self.isEventFromInbox == false){
            
            if let amenitySelection = notification.userInfo {
                
                if(amenitySelection["preferences"] != nil) {
                    
                    //RN is sending this call back when we press Near by Amenities button in bottom sheet, so we need to handle this scenario if profile is already selected
                    if let searchDiscoverLocation = searchDiscoverLocation {
                        
                        if(AmenitiesSharedInstance.shared.isAmenitiesChanged == true){
                            
                            self.removeAmenitiesFromMap()
                            AmenitiesSharedInstance.shared.parseUserPreferences(preferences: (amenitySelection["preferences"] as? NSDictionary),location: searchDiscoverLocation)
                        }
                        else{
                            
                            AmenitiesSharedInstance.shared.callAmenitiesAPI(location: searchDiscoverLocation)
                        }
                        
                    }
                    else{
                        
                        if(SelectedProfiles.shared.isProfileSelected()){
                            
                            if let profileCoordinate = SelectedProfiles.shared.getOuterZoneCentrePoint(){
                                
                                AmenitiesSharedInstance.shared.callAmenitiesAPI(location: profileCoordinate)
                            }
                        }
                        else{
                            
                            if(AmenitiesSharedInstance.shared.isAmenitiesChanged == true){
                                self.removeAmenitiesFromMap()
                            }
                            AmenitiesSharedInstance.shared.parseUserPreferences(preferences: (amenitySelection["preferences"] as? NSDictionary),location: LocationManager.shared.location.coordinate)
                        }
                        
                    }
                    
                    if let mobileMapViewUpdatable = mobileMapViewUpdatable {
                        if let navMapView = mobileMapViewUpdatable.navigationMapView {
                            AmenitiesSharedInstance.shared.loadAllImagesToTheMapStyle(navMapView: navMapView)
                        }
                    }
                    
                    AmenitiesSharedInstance.shared.updateAmenityClusteredColors()
                    
                    AmenitiesSharedInstance.shared.addProfileData()
                    
                    if(AmenitiesSharedInstance.shared.isAmenitiesChanged == false){
                        
                        self.getAmenitiesCallback()
                    }
                    
                    
                }
                else{
                   
                    let amenity = amenitySelection["amenity"] as! String
                    let isSelected = amenitySelection["isSelected"] as! Bool
                    
                    if(isSelected){
                        
                        //Verify if selected amenity is belongs to a profile or not
                        if(AmenitiesSharedInstance.shared.checkAmenityProfile(selectedAmenity: amenity)){
                            
                            DispatchQueue.main.async {
                                
                                self.removeAmenitiesFromMap()
                                //Then this particular profile details to Singleton class
                                let addedProfile = AmenitiesSharedInstance.shared.addProfileName(profileName:amenity)
                                if (addedProfile) {
                                    self.delegate?.addedProfile(self, name: amenity)
                                }
                                
                                AmenitiesSharedInstance.shared.callZoneColor(profileName: amenity)
                                
                                AmenitiesSharedInstance.shared.amenitiesZoneDelegate = self
                                
                                let amenityDetails = ["isSelected":isSelected] as [String : Any]
                                
                                NotificationCenter.default.post(name: Notification.Name(Values.handleProfileSelection), object: nil,userInfo: amenityDetails)
                                
                                //Adding Inner and Outer Circle zones
//                                self.mobileMapViewUpdatable?.addProfileZones()
//
//                                if let centreLocation = SelectedProfiles.shared.getOuterZoneCentrePoint() {
//
//                                    //Resetting to camera to profile location
//                                    self.mobileMapViewUpdatable?.setCameraAfterSearch(location: centreLocation, bottomSheetHeight: self.bottomSheetHeight)
//
//                                    //Remove existing amenities from map layer and update the map layer with near by amenities from Profile location
//                                    self.removeAmenitiesFromMap()
//                                    AmenitiesSharedInstance.shared.callAmenitiesAPI(location: centreLocation)
//
//                                }
                            }
                            
                        }
                        else{
                            
                            //If Selected profile name is not empty then request data for amenity selected around profile location
                            if SelectedProfiles.shared.selectedProfileName() != ""{
                                
                                if let centreLocation = SelectedProfiles.shared.getOuterZoneCentrePoint(){
                                    
                                    AmenitiesSharedInstance.shared.addRequestForSelectedAmenity(location: centreLocation, selectedAmenity: amenity)
                                }
                            }
                            else{
                                
                                //  If user is panning the map then should get amenity nearby the center map location
                                if let centerLocation = self.centerMapLocation {
                                    AmenitiesSharedInstance.shared.addRequestForSelectedAmenity(location: centerLocation, selectedAmenity: amenity)
                                    if(AmenitiesSharedInstance.shared.isAmenitiesChanged == false){
                                        self.getAmenitiesCallback()
                                    }
                                } else {
                                    //This is a normal flow if profile is not selected
                                    AmenitiesSharedInstance.shared.addRequestForSelectedAmenity(location: self.isSearchDisocver ? self.searchDiscoverLocation! : LocationManager.shared.location.coordinate, selectedAmenity: amenity)
                                    if(AmenitiesSharedInstance.shared.isAmenitiesChanged == false){
                                        self.getAmenitiesCallback()
                                    }
                                    
                                }
                            }
                            
                        }
                        
                    }
                    else{
                        
                        //If profile is unselected remove inner and outerzones from the map and also clear profile data from singleton class, then reset camera and update the amenities from current location
                        if(AmenitiesSharedInstance.shared.checkAmenityProfile(selectedAmenity: amenity)){
                            
                            DispatchQueue.main.async {
                                
                                SelectedProfiles.shared.clearProfile()
                                self.mobileMapViewUpdatable?.navigationMapView?.mapView.removeProfileZones()
                                self.mobileMapViewUpdatable?.resetCamera(isCruise: false,bottomSheetHeight: self.bottomSheetHeight)
                                self.removeERPLayerFromMap()
                                let amenityDetails = ["isSelected":true] as [String : Any]
                                NotificationCenter.default.post(name: Notification.Name(Values.handleProfileSelection), object: nil,userInfo: amenityDetails)
                                self.mobileMapViewUpdatable?.removeAmenitiesFromMap(type: Values.TRAFFIC)

                                AmenitiesSharedInstance.shared.isUpdateCarParkLayer = false
                                
                                /// removing amenity symbol layer from map when deselect from bottom sheet
                                self.mobileMapViewUpdatable?.removeAmenitiesFromMap(type: "\(Values.POI)")
                                self.removeAmenitiesFromMap()
                                // Romove POI amenity
                                AmenitiesSharedInstance.shared.removeAddedAmenties(amenityName: Values.POI,location:  self.isSearchDisocver ? self.searchDiscoverLocation!:LocationManager.shared.location.coordinate)
                                
                            }
                        }
                        else{
                            
                            if SelectedProfiles.shared.selectedProfileName() != ""{
                                
                                if let centreLocation = SelectedProfiles.shared.getInnerZoneCentrePoint(){
                                    
                                    AmenitiesSharedInstance.shared.removeAddedAmenties(amenityName: amenity,location: centreLocation)
                                    
                                    /// removing amenity symbol layer from map when deselect from bottom sheet
                                    mobileMapViewUpdatable?.removeAmenitiesFromMap(type: "\(amenity)")
                                }
                               
                            }
                            else{
                                
                                //else regular flow
                                AmenitiesSharedInstance.shared.removeAddedAmenties(amenityName: amenity,location: self.isSearchDisocver ? self.searchDiscoverLocation!:LocationManager.shared.location.coordinate)
                                
                                /// removing amenity symbol layer from map when deselect from bottom sheet
                                mobileMapViewUpdatable?.removeAmenitiesFromMap(type: "\(amenity)")
                            }
                            
                        }
                        
                    }
                }
                
                
            }
        }
        else{
            
            self.isEventFromInbox = false
        }
        
    }
    
    func removeAmenitiesFromMap(){
        
        if AmenitiesSharedInstance.shared.amenitiesSymbolLayer.count > 0 {
            
            let allKeys = AmenitiesSharedInstance.shared.amenitiesSymbolLayer.keys
             
             //self.parseAmenityImage()
             for key in allKeys {
                 
                 if AmenitiesSharedInstance.shared.amenitiesSymbolLayer.keys.contains(key){
                     
                     if let mapViewUpdatable = mobileMapViewUpdatable {
                         
                         mapViewUpdatable.removeAmenitiesFromMap(type: "\(key)")
                     }
                     
                 }
             }
        }
       
    }
    
    func getAmenitiesDataIfLocationHasChanged(){
        
        ///This callback comes in 2 different scenarios
         /*
             Manually switching between modes, if NONE_MODE, we call this
             When clicking on Home tab RN will send this callback automatically to NONE_MODE.
             Moving to a different thread can make this method callback response will become mutex
          */
        DispatchQueue.global(qos: .background).async {
            
            if(SelectedProfiles.shared.isProfileSelected()){
                
                if let centreLocation = SelectedProfiles.shared.getOuterZoneCentrePoint(){
                    AmenitiesSharedInstance.shared.callAmenitiesAPI(location: centreLocation)
                }
            }
            else{
                
                AmenitiesSharedInstance.shared.callAmenitiesAPI(location: self.isSearchDisocver ? self.searchDiscoverLocation! : LocationManager.shared.location.coordinate)
            }
          
        }
    }
    
    func getExistingAmenitiesAndUpdatedOnMap(selectedId: String = ""){
        
        if AmenitiesSharedInstance.shared.amenitiesSymbolLayer.count > 0 {
            
            let allKeys = AmenitiesSharedInstance.shared.amenitiesSymbolLayer.keys
             
             //self.parseAmenityImage()
             for key in allKeys {
                 
                 if AmenitiesSharedInstance.shared.amenitiesSymbolLayer.keys.contains(key){
                     
                     let array = AmenitiesSharedInstance.shared.amenitiesSymbolLayer[key] as! [Any]
                     
                     var turfArray = [Turf.Feature]()
                     print(array)
                     for item in array {

                       let itemDict = item as! [String:Any]
                         
                         let feature = self.getAmenitiesFeatures(location: itemDict, type: key, selectedID: selectedId)
                         turfArray.append(feature)
                         
                     }
                     
                     let amenityFeatureCollection = FeatureCollection(features: turfArray)
                     
                     self.updateAmenitiesOnMap(collection: amenityFeatureCollection,type: key)
                 }
             }
        }
        
    }
    
    func getExistingTrafficAndUpdateOnMap(selectedID: String = "", selectFirstOne: Bool = false) {
        var turfTrafficArray = [Turf.Feature]()
        var coordinates = [CLLocationCoordinate2D]()
        var isFirst = true
        
        var selectedCoordinate: CLLocationCoordinate2D?
        
        if let trafficReceivedData = trafficReceivedData {
            
            for trafficReceivedData in trafficReceivedData {
                let itemDict = trafficReceivedData as! [String:Any]
                
                let trafficID = itemDict["id"] as! String
                let trafficCameraID = itemDict["cameraID"] as! Int
                let trafficCameralat = itemDict["lat"] as! Double
                let trafficCameralong = itemDict["long"] as! Double
                let trafficCameraName = itemDict["name"] as! String
                
                let coordinate = CLLocationCoordinate2D(latitude: trafficCameralat, longitude: trafficCameralong)
                coordinates.append(coordinate)
                if isFirst && selectFirstOne {
                   // let options = CameraOptions(center:coordinate)
                    //if let mapViewUpdatable = mobileMapViewUpdatable {
                        
                        trafficCameraId = trafficID
                        //mapViewUpdatable.setCameraOptions(options)
                    //}
                }
                
                var feature = Turf.Feature(geometry: .point(Point(coordinate)))
                let isSelected = selectFirstOne ? isFirst : (!selectedID.isEmpty && trafficID == selectedID)
                feature.properties = [
                    "type":.string(Values.TRAFFIC),
                    "trafficID":.string(trafficID),
                    "trafficCameraID":.string("\(trafficCameraID)"),
                    "trafficCameraName":.string(trafficCameraName),
                    "isSelected": .boolean(isSelected)
                ]
                
                if (isSelected) {
                    selectedCoordinate = coordinate
                }
                
                turfTrafficArray.append(feature)
                isFirst = false
            }
            
            if let mapViewUpdatable = mobileMapViewUpdatable {
                
                let padding = UIEdgeInsets(top: 100, left: 100, bottom: CGFloat(self.bottomSheetHeight)+150, right: 100)
                
                let distanceFilter = 0.0
                
                if let searchDiscoverLocation = self.searchDiscoverLocation {
                    
                    coordinates.append(searchDiscoverLocation)
                    
                    let finalFarthestDistance = self.calculateFurthestDistance(coordinates: coordinates, defaultFurthestDistance: distanceFilter, nearestCoordinate: coordinates[0])
                    
                    mapViewUpdatable.navigationMapView?.mapView.setDynamicZoomLevelBasedonRadius(coordinate1: selectedCoordinate ?? coordinates[0], dynamicRadius: Int(finalFarthestDistance), padding: padding, coordinate2: searchDiscoverLocation,openToolTip: true)
                } else if let centerMapLocation = self.centerMapLocation {
//                    coordinates.append(centerMapLocation)
//                    mapViewUpdatable.navigationMapView.mapView.setDynamicZoomLevelBasedonRadius(coordinate1: selectedCoordinate ?? coordinates[0], dynamicRadius:SelectedProfiles.shared.getOuterZoneRadius(), padding: padding, coordinate2: centerMapLocation,openToolTip: true, multiplyValue: 5.5)
                } else {

                    coordinates.append(LocationManager.shared.location.coordinate)

                    let finalFarthestDistance = self.calculateFurthestDistance(coordinates: coordinates, defaultFurthestDistance: distanceFilter, nearestCoordinate: coordinates[0])


                    if(SelectedProfiles.shared.isProfileSelected()){
                        
                        if let profileLocation = SelectedProfiles.shared.getOuterZoneCentrePoint(){
                            
                            mapViewUpdatable.navigationMapView?.mapView.setDynamicZoomLevelBasedonRadius(coordinate1: selectedCoordinate ?? coordinates[0], dynamicRadius:SelectedProfiles.shared.getOuterZoneRadius(), padding: padding, coordinate2: profileLocation,openToolTip: true,multiplyValue: 5.5)
                        }
                    }
                    else{
                        
                        mapViewUpdatable.navigationMapView?.mapView.setDynamicZoomLevelBasedonRadius(coordinate1: selectedCoordinate ?? coordinates[0], dynamicRadius: Int(finalFarthestDistance), padding: padding, coordinate2: LocationManager.shared.location.coordinate,openToolTip: true)
                    }
                    
                }

            }
            trafficFeatureCollection = FeatureCollection(features: turfTrafficArray)
            self.updateTrafficDataOnMap(collection: trafficFeatureCollection,type: Values.TRAFFIC)
        }
        else{
            
            self.recenter()
        }
        
    }
    
    private func getCameraOptions(coordinates:[CLLocationCoordinate2D],mobileUpdatable:MapLandingViewUpdatable,padding:UIEdgeInsets,isERP:Bool = false){
        
        DispatchQueue.main.async {
            
          
            if(isERP){
                
                if let searchDiscoverLocation = self.searchDiscoverLocation {
                    
                    mobileUpdatable.navigationMapView?.mapView.setDynamicZoomLevelBasedonRadius(coordinate1: coordinates[coordinates.count - 1], dynamicRadius: 3000, padding: padding, coordinate2: searchDiscoverLocation)
                }
                else{
                    
                    mobileUpdatable.navigationMapView?.mapView.setDynamicZoomLevelBasedonRadius(coordinate1: coordinates[coordinates.count - 1], dynamicRadius: 3000, padding: padding, coordinate2: LocationManager.shared.location.coordinate)
                }
//                let cameraOptions = mobileUpdatable.navigationMapView.mapView.mapboxMap.camera(for: coordinates, padding:padding, bearing: nil, pitch: nil)
//                  self.mobileMapViewUpdatable?.setCameraOptions(cameraOptions, isEase: true)
                
            }
            else{
                
                if let cameraOptions = mobileUpdatable.navigationMapView?.mapView.mapboxMap.camera(for: coordinates, padding:padding, bearing: nil, pitch: nil) {
                    SwiftyBeaver.debug("Camera Options = \(cameraOptions) \(coordinates)")
                    self.mobileMapViewUpdatable?.setCameraOptions(cameraOptions,isEase: true)
                }
            }
            
            
        }
        
    }
    
    private func getMaxDistance(array: [[String:Any]]) -> CLLocationCoordinate2D? {
        let sortedArray = array.sorted { lhs, rhs in
            let ldistance = lhs["distance"] as? Int ?? 0
            let rdistance = lhs["distance"] as? Int ?? 0
            return ldistance > rdistance
        }
        
        if let maxObject = sortedArray.last,
           let lat = maxObject["lat"] as? Double,
           let long = maxObject["long"] as? Double {
            distanceCompare = maxObject["distance"] as? Int ?? 0
            return CLLocationCoordinate2D(latitude: lat, longitude: long)
        }
        return nil
    }
    
    private func getAmenitiesCallback(){
        
        if !didRegisterAmenityUpdates {
            didRegisterAmenityUpdates = true
            
            amenityCancellable = AmenitiesSharedInstance.shared.$isDisplayAmenities.sink { [weak self] isDisplayAmenities in
                guard let self = self else { return }
                
                self.distanceCompare = 0
                
                if(isDisplayAmenities){
                    var amenityCoordinates = [CLLocationCoordinate2D]()
                    let allKeys = AmenitiesSharedInstance.shared.amenitiesSymbolLayer.keys.filter { type in
                        return self.amenityTypes.contains(type)
                    }
                    //self.parseAmenityImage()
                                        
                    if !self.isOnDropPin && allKeys.isEmpty && self.amenityTypes.count == 1 {
                        DispatchQueue.main.async { [weak self] in
                            if self?.amenityTypes.contains(Values.EV_CHARGER) ?? false {
                                self?.showNoDataNotification(Values.EV_CHARGER)
                            } else if self?.amenityTypes.contains(Values.PETROL) ?? false {
                                self?.showNoDataNotification(Values.PETROL)
                            }
                        }
                    }
                    
                    for key in allKeys {
                        
                        if let array = AmenitiesSharedInstance.shared.amenitiesSymbolLayer[key] as? [Any], array.count > 0 {
                            switch key {
                            case Values.POI:
                                if let itemDics = array as? [[String: Any]] {
                                    let featureCollection = self.getPOIAmenitiesFeatureCollection(amenities: itemDics)
                                    self.updatePOIOnMap(collection: featureCollection)
                                }
                                
                                if let itemDics = array as? [[String: Any]],
                                   let coordinate = self.getMaxDistance(array: itemDics) {
                                    amenityCoordinates = [coordinate]
                                }
                                break
                            default:
                                var turfArray = [Turf.Feature]()
                                
                                if let itemDics = array as? [[String: Any]],
                                   let coordinate = self.getMaxDistance(array: itemDics) {
                                    amenityCoordinates = [coordinate]
                                }
                                for item in array {
                                    if let itemDict = item as? [String:Any] {
                                        let feature = self.getAmenitiesFeatures(location: itemDict, type: key)
                                        turfArray.append(feature)
                                    }
                                }
                                
                                let amenityFeatureCollection = FeatureCollection(features: turfArray)
                                
                                self.updateAmenitiesOnMap(collection: amenityFeatureCollection,type: key)
                                
                                if !self.isOnDropPin && turfArray.isEmpty {
                                    DispatchQueue.main.async { [weak self] in
                                        if self?.amenityTypes.contains(key) ?? false {
                                            self?.showNoDataNotification(key)
                                        }
                                    }
                                }
                                
                                //AmenitiesSharedInstance.shared.isDisplayAmenities = false // Make the amenities response callback variable to false, to avoid unnecessary repeated callbacks
                            }
                        } else {
                            if !self.isOnDropPin {
                                DispatchQueue.main.async { [weak self] in
                                    if self?.amenityTypes.contains(key) ?? false {
                                        self?.showNoDataNotification(key)
                                    }
                                }
                            }
                        }
                    }
                    
                    //  If user is panning to explore nearby location the should not update zoom level
                    if !self.isOnDropPin || (self.isOnDropPin && self.centerMapLocation == nil) {
                        DispatchQueue.main.async { [weak self] in
                            guard let self = self else { return }
                            
                            if let mapViewUpdatable = self.mobileMapViewUpdatable {
                                
                                if(amenityCoordinates.count > 0){
                                    
                                    var height = 0
                                    if (self.bottomSheetHeight == 0 ){
                                        
                                        height = 416
                                    }
                                    else{
                                        
                                        height = self.bottomSheetHeight
                                    }
                                    
                                    let padding = UIEdgeInsets(top: 140, left: 40, bottom:  CGFloat(height) + 190 , right: 40)
                                    
                                    if let searchDiscoverLocation = self.searchDiscoverLocation {
                                        
                                        mapViewUpdatable.navigationMapView?.mapView.setDynamicZoomLevelBasedonRadius(coordinate1: amenityCoordinates[0], dynamicRadius: self.distanceCompare, padding: padding, coordinate2: searchDiscoverLocation)
                                    } else {
                                        
                                        if SelectedProfiles.shared.selectedProfileName() != ""{
                                            if let profileLocation = SelectedProfiles.shared.getOuterZoneCentrePoint(){
                                                mapViewUpdatable.navigationMapView?.mapView.setDynamicZoomLevelBasedonRadius(coordinate1: amenityCoordinates[0], dynamicRadius: SelectedProfiles.shared.getOuterZoneRadius(), padding: padding, coordinate2: profileLocation)
                                            }
                                        } else {
                                            
                                            mapViewUpdatable.navigationMapView?.mapView.setDynamicZoomLevelBasedonRadius(coordinate1: amenityCoordinates[0], dynamicRadius: self.distanceCompare, padding: padding, coordinate2: LocationManager.shared.location.coordinate)
                                        }
                                        
                                    }
                                }
                                
                            }
                        }
                    }
                }
            }
        }
    }
    
    private func showNoDataNotification(_ amenity: String) {
        let title = "No Available Data"
        var message = ""
        let radiusInKm = Int(Double(UserSettingsModel.sharedInstance.evPetrolRange) / 1000)
        if amenity == Values.EV_CHARGER {
            message = "Unable to find any EV charger within \(radiusInKm)km radius"
        } else if amenity == Values.PETROL {            
            message = "Unable to find any petrol station within \(radiusInKm)km radius"
        }
        if !message.isEmpty {
            appDelegate().showOnAppErrorNotification(title: title, subTitle: message, controller: self.getController())
        }
    }
    
    private func getController() -> MapLandingVC? {
        if let vc = self.delegate as? MapLandingVC {
            return vc
        }
        return nil
    }
    
    
    private func parseAmenityImage(){
        
        let allKeys = AmenitiesSharedInstance.shared.amenitiesSymbolLayer.keys
        
        for key in allKeys {
            
            if AmenitiesSharedInstance.shared.amenitiesSymbolLayer.keys.contains(key){
                
                let array = AmenitiesSharedInstance.shared.amenitiesSymbolLayer[key] as! [Any]
                
                
                for item in array {

                  let itemDict = item as! [String:Any]
                    
                    if(itemDict["mapImageURL"] != nil){
                        
                        self.mobileMapViewUpdatable?.addAmenityImageToMapStyle(type: key, urlString: itemDict["mapImageURL"] as! String)
                        break;
                    }
                    
                   
                    
                }
                
            }
        }
    }
    
    @objc func onSearchLocationReceived(_ notification:NSNotification) {
        if let locationDictionary = notification.userInfo {
            
            self.didShowNearByNotification = false
            self.isSearchDisocver = true
            let lat = locationDictionary["lat"] as? Double ?? 0.0
            let long = locationDictionary["long"] as? Double ?? 0.0
            let isParkingOn = locationDictionary["isParkingOn"] as? Bool ?? false
            destName = locationDictionary["destName"] as? String ?? ""
            searchAddress = locationDictionary["address"] as? String ?? ""
            isSearchAddressBookmarked = locationDictionary["isBookmarked"] as? Bool ?? false
            bookmarkId = locationDictionary["bookmarkId"] as? Int
            fullAddress = locationDictionary["fullAddress"] as? String ?? ""
            carparkId = locationDictionary["carparkId"] as? String ?? ""
            placeId = locationDictionary["placeId"] as? String ?? ""
            availablePercentImageBand = locationDictionary["availablePercentImageBand"] as? String ?? ""
            let csDict = locationDictionary["availabilityCSData"] as? [String: Any] ?? [:]
            if !csDict.keys.isEmpty {
                availabilityCSData = AvailabilityCSData(csDict: csDict)
            }
            
            var location = CLLocationCoordinate2D(latitude: lat, longitude: long)
            
            if let selectedRoutablePoint = parseSelectedRoutablePoint(locationDictionary),
                let coordinate = selectedRoutablePoint.coordinate {
                self.selectedRoutablePoint = selectedRoutablePoint
                location = coordinate
                carparkId = selectedRoutablePoint.carparkId
            } else {
                self.selectedRoutablePoint = nil
                self.carparkId = nil
            }
                         
            //To check whether selected location is inside Outerzone of the profile or not
            if let outerZoneCenter = SelectedProfiles.shared.getOuterZoneCentrePoint() {
                self.findCurrentLocationInsideZone(currentLocation: location, profileCentreCoordinate: outerZoneCenter)
            }
            self.searchDiscoverLocation = location
            
            DispatchQueue.main.async {
                self.mobileMapViewUpdatable?.setCameraAfterSearch(location: location,bottomSheetHeight: self.bottomSheetHeight, isParkingOn: (self.landingMode == Values.NONE_MODE || self.landingMode == Values.OBU_MODE || self.landingMode == Values.PARKING_MODE) ? isParkingOn : !isParkingOn)
            }
            if(landingMode == Values.NONE_MODE || landingMode == Values.OBU_MODE){
                self.removeAmenitiesFromMap()
                AmenitiesSharedInstance.shared.isUpdateCarParkLayer = false
                AmenitiesSharedInstance.shared.callAmenitiesAPI(location: location, carparkId: carparkId)
//                AmenitiesSharedInstance.shared.callAmenitiesAPI(location: location)
                DispatchQueue.main.async {
                 self.mobileMapViewUpdatable?.removeAmenitiesFromMap(type: Values.TRAFFIC)
                  self.removeERPLayerFromMap()
                }
            }
            else if(landingMode == Values.ERP_MODE){
                
                DispatchQueue.main.async {
                  self.removeAmenitiesFromMap()
                  self.updateERPOnMap()
                  self.mobileMapViewUpdatable?.removeAmenitiesFromMap(type: Values.TRAFFIC)
                }
            }
            else if(landingMode == Values.TRAFFIC_MODE){
                
                DispatchQueue.main.async {
                  self.removeAmenitiesFromMap()
                  self.removeERPLayerFromMap()
                }
            }
                
            
        }
    }
    
    private func findCurrentLocationInsideZone(currentLocation:CLLocationCoordinate2D,profileCentreCoordinate:CLLocationCoordinate2D){
        let isInsideZone = identifyUserLocInOuterZone(centerCoordinate: profileCentreCoordinate, currentLocation: currentLocation)
        if !isInsideZone {
            sendDisableProfileSelection()
            SelectedProfiles.shared.clearProfile()
            AmenitiesSharedInstance.shared.removeSpecificAmenity(amenityName: Values.POI)
            mobileMapViewUpdatable?.navigationMapView?.mapView.removeProfileZones()
            mobileMapViewUpdatable?.navigationMapView?.mapView.removeAmenitiesLayerOfCluster(type: "\(Values.POI)")
            self.removeAmenitiesFromMap()
        }
        
    }
    
//    func cruiseStoppedByUser() {
//        isCruiseStoppedByUser = true // this is called in appDelegate
//    }
    
    @objc
    func onTrafficDataReceived(_ notification:NSNotification){
        
        if let trafficData = notification.userInfo {
            
            trafficReceivedData = trafficData["data"] as? [Any]
/*
            var turfTrafficArray = [Turf.Feature]()
            var isFirst = true
            for trafficReceivedData in self.trafficReceivedData ?? [] {
                let itemDict = trafficReceivedData as! [String:Any]
                print(itemDict["distance"])
                
                let trafficID = itemDict["id"] as! String
                let trafficCameraID = itemDict["cameraID"] as! Int
                let trafficCameralat = itemDict["lat"] as! Double
                let trafficCameralong = itemDict["long"] as! Double
                let trafficCameraName = itemDict["name"] as! String
                
                let coordinate = CLLocationCoordinate2D(latitude: trafficCameralat, longitude: trafficCameralong)
                if isFirst {
                    let options = CameraOptions(center:coordinate)
                    if let mapViewUpdatable = mobileMapViewUpdatable {
                        
                        trafficCameraId = trafficID
                        mapViewUpdatable.setCameraOptions(options)
                    }
                }

                var feature = Turf.Feature(geometry: .point(Point(coordinate)))
                feature.properties = [
                    "type":.string(Values.TRAFFIC),
                    "trafficID":.string(trafficID),
                    "trafficCameraID":.string("\(trafficCameraID)"),
                    "trafficCameraName":.string(trafficCameraName),
                    "isSelected": .boolean(isFirst)
                ]
                
                turfTrafficArray.append(feature)
               isFirst = false
            }
            
           
            trafficFeatureCollection = FeatureCollection(features: turfTrafficArray)
            
            self.updateTrafficDataOnMap(collection: trafficFeatureCollection,type: Values.TRAFFIC)
            //print(trafficData["data"] as? NSDictionary)
*/
            getExistingTrafficAndUpdateOnMap(selectedID: "", selectFirstOne: true)
        }
    }
    
    @objc
    func onTrafficIDSelection(_ notification:NSNotification){
        
        if let trafficIDData = notification.userInfo {
            
            let trafficIDSelected = trafficIDData["id"] as! String
            
            if let trafficReceivedData = trafficReceivedData {
                
                for traffic in trafficReceivedData {
                    let itemDict = traffic as! [String:Any]
                    let trafficID = itemDict["id"] as! String
                    
                    if(trafficID == trafficIDSelected){
                        
                        let trafficCameralat = itemDict["lat"] as! Double
                        let trafficCameralong = itemDict["long"] as! Double
                        let coordinate = CLLocationCoordinate2D(latitude: trafficCameralat, longitude: trafficCameralong)
                        let options = CameraOptions(center:coordinate)
                        
                        if let mapViewUpdatable = mobileMapViewUpdatable {
                            
                            if(trafficCameraId != ""){
                                
                                if(trafficID != trafficCameraId){
                                    trafficCameraId = trafficID
                                    DispatchQueue.main.async {
                                      mapViewUpdatable.setCameraOptions(options)
                                    }
                                }
                                
                            }
                            else{
                                
                                trafficCameraId = trafficID
                                DispatchQueue.main.async {
                                   mapViewUpdatable.setCameraOptions(options)
                                }
                            }
                           
                        }
                    }
                }
            
            }
            
        }
    }
    
    // NOTICE: These two functions may affect navigation updating
    func startUpdating() {
        SwiftyBeaver.debug("Ready to startUpdating")
        cruiseDelayCancellable?.cancel()
        
        //  Change timer to 3 seconds instead of 30
        cruiseDelayCancellable = Timer.publish(every: 3, on: .main, in: .default)
            .autoconnect()
            .first()
            .sink(receiveValue: { [weak self] _ in
            guard let self = self else { return }
            SwiftyBeaver.debug("startUpdating - shouldAllowStartCruise = true")
            self.shouldAllowStartCruise = true
        })
    }
    
    func stopUpdating() {
        SwiftyBeaver.debug("stopUpdating - shouldAllowStartCruise = false")
        cruiseDelayCancellable?.cancel()
        shouldAllowStartCruise = false
    }
        
    func setupETA() {
            
        if let tripETA = ETATrigger.shared.tripCruiseETA {
            ETATrigger.shared.lastLiveLocationUpdateTime = Date().timeIntervalSince1970
            TripETAService().sendTripCruiseETA(tripETA) { [weak self] result in
                guard let self = self else { return }
                                
                switch result {
                case .success(let tripETAResponse):
                    DispatchQueue.main.async {
                        self.updateETAUI(eta: tripETA)
                        ETATrigger.shared.liveLocTripId = tripETAResponse.tripEtaUUID
                        self.isLiveLocationSharingEnabled = true
                        print("tripETAUUID: \(tripETAResponse.tripEtaUUID)")
                        DataCenter.shared.subscribeToMessageInbox(tripETAId: ETATrigger.shared.liveLocTripId) { [weak self] message in
                            guard let self = self, !message.isEmpty else { return }

                            self.showETAMessage(recipient: tripETA.recipientName, message: message)
                        }
                        
//                            DataCenter.shared.startUpdatingUserTripLiveLocation(tripId: tripETAResponse.tripEtaUUID, latitude: location.coordinate.latitude.toString(), longitude: location.coordinate.longitude.toString(), status: LiveLocationStatusType.inprogress.rawValue)
//                            self.updateLiveLocationStatus(status: .inprogress)
                    }
                case .failure(let error):
                    print(error.localizedDescription)
                }
            }
        } else {
            updateETAUI(eta: nil)
            // No ETA, so should not send Reminder
            ETATrigger.shared.isReminder1Sent = true
            isLiveLocationSharingEnabled = false
        }
    }
    
    func noTracking() {
        isTrackingUser = false
    }
    
    func recenter() {
        isTrackingUser = true
        // update camera
        if(isSearchDisocver){
            DispatchQueue.main.async {
                self.mobileMapViewUpdatable?.setCamera(at: self.searchDiscoverLocation,reset:true, isCruise: false,bottomSheetHeight: self.bottomSheetHeight)
                if (self.landingMode == Values.NONE_MODE || self.landingMode == Values.OBU_MODE || self.landingMode == Values.PARKING_MODE ) {
                    if(AmenitiesSharedInstance.shared.isAmenitiesChanged == false){
                        self.getAmenitiesCallback()
                    }
                    
                }
            }
        }
        else{
            DispatchQueue.main.async {
                self.mobileMapViewUpdatable?.resetCamera(isCruise: false,bottomSheetHeight: self.bottomSheetHeight)
                self.carPlayMapViewUpdatable?.resetCamera(isCruise: false)
                if (self.landingMode == Values.NONE_MODE || self.landingMode == Values.OBU_MODE || self.landingMode == Values.PARKING_MODE) {
                    
                    if(AmenitiesSharedInstance.shared.isAmenitiesChanged == false){
                      self.getAmenitiesCallback()
                    }
                }
                
            }
        }
    }
    
    // MARK: - Private Methods
    private func loadData() {
        DataCenter.shared.addERPUpdaterListner(listner: self)
//        DataCenter.shared.addTrafficUpdaterListener(listener: self)
    }
    
    /// Sync up UI based on the current status
    private func syncUpdate(mapViewUpdatable: MapViewUpdatable,override: Bool = true) {
        mapViewUpdatable.setupCustomUserLocationIcon(isCruise: false)
        if !appDelegate().carPlayConnected {
            SwiftyBeaver.debug("syncUpdate cruiseStatusUpdate false")
            mapViewUpdatable.cruiseStatusUpdate(isCruise: false)
        }
        updateAmenitiesLayer(mapViewUpdatable: mapViewUpdatable)
        loadData()
    }
    
    /// update the amenities layer
    private func updateAmenitiesLayer(mapViewUpdatable: MapViewUpdatable!) {
        #if DEMO
        if let featureCollection = self.amenitiesFeatureCollection {
            mapViewUpdatable.updateAmenities(featureCollection)
        }
        #endif
    }
    
    private func mapUpdate() {
        // Update camera
        mobileMapViewUpdatable?.cruiseStatusUpdate(isCruise: false)
        carPlayMapViewUpdatable?.cruiseStatusUpdate(isCruise: false)
        // Update puck
        mobileMapViewUpdatable?.setupCustomUserLocationIcon(isCruise: false)
        carPlayMapViewUpdatable?.setupCustomUserLocationIcon(isCruise: false)
        
        //Update close Notification Alert boolean value and make cpDisplayFeature to nil
        carPlayMapViewUpdatable?.cpFeatureDisplayClose = false
        carPlayMapViewUpdatable?.cpFeatureDisplay = nil
        
        if let templates = appDelegate().carPlayManager.interfaceController?.templates{
            if(templates.count > 1){
                
                if #available(iOS 14, *) {
                    
                    appDelegate().carPlayManager.interfaceController?.popToRootTemplate(animated: true, completion: { value, error in
                        if(!value){
                            SwiftyBeaver.debug("MapLanding map update Pop to root template failed", "\(value)","\(String(describing: error))")
                        }
                        
                    })
                }
                else{
                    appDelegate().carPlayManager.interfaceController?.popToRootTemplate(animated: true)
                }
                
            }
        }
        
    }
        
    private func updateETAUI(eta: TripCruiseETA?) {
        mobileMapViewUpdatable?.updateETA(eta: eta)
        carPlayMapViewUpdatable?.updateETA(eta: eta)
    }
    
    private func showETAMessage(recipient: String, message: String) {
        mobileMapViewUpdatable?.showETAMessage(recipient: recipient, message: message)
        carPlayMapViewUpdatable?.showETAMessage(recipient: recipient, message: message)
    }
    
    func etaSetup(fav:Favourites)
    {
        if(ETATrigger.shared.tripCruiseETA == nil)
        {
            startCruise()
//            if(isCruise)
//            {
                ETATrigger.shared.setUpTripETA(etaFav: fav)
                setupETA()
                //8. Update CarPlay Toggle buttons
                appDelegate().carPlayMapController?.updateCPToggleButtons(activity: .navigating)
            //}
        }
    }
    
    private func getPOIAmenitiesFeatures(itemDic: [String: Any]) -> Turf.Feature {
        
        var feature = Turf.Feature(geometry: .point(Point(CLLocationCoordinate2D(latitude: itemDic["lat"] as! Double, longitude: itemDic["long"] as! Double))))
        feature.properties = [
//            "imageName": .string(itemDic["mapImageURL"] as! String
            "type": .string(Values.POI),
            "name": .string(itemDic["name"] as! String),
            "address": .string(itemDic["address"] as! String),
            "id": .string(itemDic["poiID"] as! String),
            "provider":.string(itemDic["providerName"] as! String)
        ]
        return feature
    }
    
    private func getPOIAmenitiesFeatureCollection(amenities: [[String: Any]]) -> Turf.FeatureCollection {
        let arrayFeature = amenities.map { itemDic in
            return getPOIAmenitiesFeatures(itemDic: itemDic)
        }
        return Turf.FeatureCollection(features: arrayFeature)
    }
    
    private func getAmenitiesFeatures(location:[String:Any],type:String, selectedID: String = "")-> Turf.Feature {
        
        let coordinate = CLLocationCoordinate2D(latitude: location["lat"] as! Double, longitude: location["long"] as! Double)
        let name = location["name"] as! String
        let address = location["address"] as! String
        let id = location["id"] as! String
        let poiID = location["poiID"] as? String ?? ""
        let startDate = location["startDate"] as! Double
        let endDate = location["endDate"] as! Double
        let clusterID = location["clusterID"] as! Int
        let clusterColor = location["clusterColor"] as! String
        let originalType = location["originalType"] as! String
        let textAdditionalInfo = location["textAdditionalInfo"] as? String ?? ""
        let styleLightColor = location["styleLightColor"] as? String ?? ""
        let styleDarkColor = location["styleDarkColor"] as? String ?? ""
        let isBookmarked = location["isBookmarked"] as? Bool ?? false
        let bookmarkId = location["bookmarkId"] as? Int ?? -1
        var feature = Turf.Feature(geometry: .point(Point(coordinate)))
        feature.properties = [
            "id":.string(id),
            "type":.string(type),
            "name":.string(name),
            "address":.string(address),
            "startDate": .number(startDate),
            "endDate": .number(endDate),
            "isSelected": .boolean(!selectedID.isEmpty && id == selectedID),
            "clusterID":.string("\(clusterID)"),
            "clusterColor":.string(clusterColor),
            "bookmarkId": .number(Double(bookmarkId)),
            "isBookmarked": .boolean(isBookmarked),
            "poiID": .string(poiID),
            "textAdditionalInfo": .string(textAdditionalInfo),
            "styleLightColor": .string(styleLightColor),
            "styleDarkColor": .string(styleDarkColor),
            "originalType":.string(originalType)
        ]
        return feature
    }
    
    private func updateAmenitiesOnMap(collection:Turf.FeatureCollection,type:String){
        
        AmenitiesSharedInstance.shared.isAmenitiesChanged = false
        if let mapViewUpdatable = mobileMapViewUpdatable {
            mapViewUpdatable.addAmenitiesToMap(collection, type: "\(type)")            
        }
    }
    
    private func updatePOIOnMap(collection:Turf.FeatureCollection) {
        if let mapViewUpdatable = mobileMapViewUpdatable {
            mapViewUpdatable.addPOIAmenitiesToMap(collection)
        }
    }
    
    private func updateTrafficDataOnMap(collection:Turf.FeatureCollection,type:String){
        
        if let mapViewUpdatable = mobileMapViewUpdatable {
            
            mapViewUpdatable.addAmenitiesToMap(collection, type:type)
        }
        
    }
    
    func calculateFurthestDistance(coordinates:[CLLocationCoordinate2D],defaultFurthestDistance:Double,nearestCoordinate:CLLocationCoordinate2D) -> Double {
        
        var finalFurthestDistance = defaultFurthestDistance
        for coordinate in coordinates {
            
            let toLocation = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
            
            let fromLocation = CLLocation(latitude: nearestCoordinate.latitude, longitude: nearestCoordinate.longitude)
            
            let distance = fromLocation.distance(from: toLocation)
            
            if(distance > finalFurthestDistance){
                
                finalFurthestDistance = distance
            }
             
        }
        
        return finalFurthestDistance
    }
    
    func adjustZoomeLevelForERP(mapViewUpdatable:MapLandingViewUpdatable){
        
       
        if(erpCoordinates.count > 0 && nearByERPFilter.count > 0){
            
            let distanceFilter = nearByERPFilter[nearByERPFilter.count - 1].distance
            
            if let searchDiscoverLocation = self.searchDiscoverLocation {
                
                let padding = UIEdgeInsets(top: 100, left: 100, bottom: CGFloat(self.bottomSheetHeight)+150, right: 100)
                
                erpCoordinates.append(searchDiscoverLocation)
                
                let finalFarthestDistance = self.calculateFurthestDistance(coordinates: erpCoordinates, defaultFurthestDistance: distanceFilter, nearestCoordinate: erpCoordinates[0])
                
                mapViewUpdatable.navigationMapView?.mapView.setDynamicZoomLevelBasedonRadius(coordinate1: erpCoordinates[0], dynamicRadius: Int(finalFarthestDistance), padding: padding, coordinate2: searchDiscoverLocation,openToolTip: true)
            }
            else{
                
                let padding = UIEdgeInsets(top: 100, left: 100, bottom: CGFloat(self.bottomSheetHeight)+150, right: 100)
                
                let distanceFilter = nearByERPFilter[nearByERPFilter.count - 1].distance
                
                erpCoordinates.append(LocationManager.shared.location.coordinate)
                
                let finalFarthestDistance = self.calculateFurthestDistance(coordinates: erpCoordinates, defaultFurthestDistance: distanceFilter, nearestCoordinate: erpCoordinates[0])
                
                
                if(SelectedProfiles.shared.isProfileSelected()){
                    
                    if let profileLocation = SelectedProfiles.shared.getOuterZoneCentrePoint(){
                        
                        mapViewUpdatable.navigationMapView?.mapView.setDynamicZoomLevelBasedonRadius(coordinate1: erpCoordinates[0], dynamicRadius: SelectedProfiles.shared.getOuterZoneRadius(), padding: padding, coordinate2: profileLocation,openToolTip: true,multiplyValue: 5.7)
                    }
                }
                else{
                    
                    mapViewUpdatable.navigationMapView?.mapView.setDynamicZoomLevelBasedonRadius(coordinate1: erpCoordinates[0], dynamicRadius: Int(finalFarthestDistance), padding: padding, coordinate2: LocationManager.shared.location.coordinate,openToolTip: true)
                }
               
            }
        }
        else{
            
            self.recenter()
        }
    }
    
    func updateERPOnMap(){
        
        landingMode = Values.ERP_MODE
        if(erpCoordinates.count > 0){
            erpCoordinates.removeAll()
        }
        
        if(nearByERPFilter.count > 0){
            
            nearByERPFilter.removeAll()
        }
        
        var tempSelectedTime = 0
        if(selectedERPTime == 0){
            
            tempSelectedTime =  Int(Date().currentTimeInMiliseconds())
        }
        else{
            
            tempSelectedTime = selectedERPTime
            
        }
        
        if(isSearchDisocver){
            
            
            let erpData = getNearByERPS(coordinate: searchDiscoverLocation!,timeStamp: tempSelectedTime)
            
            let currentERPFeatures =   erpData.erpCollection
            
             nearByERPFilter =   erpData.nearByData
            
            if let mapViewUpdatable = mobileMapViewUpdatable {
                
                self.updateERPLayerForMobile(mapViewUpdatable: mapViewUpdatable, erpFeatures: currentERPFeatures)
                
                for feature in currentERPFeatures.features{
                    
                    if case let .point(pointData) = feature.geometry{
                        
                        erpCoordinates.append(pointData.coordinates)
                    }
                }
                
                self.adjustZoomeLevelForERP(mapViewUpdatable: mapViewUpdatable)
            }
            
            
        }
        else{
            
            var coordinate = LocationManager.shared.location.coordinate
            if SelectedProfiles.shared.isProfileSelected(){
                
                if let profileLocation = SelectedProfiles.shared.getOuterZoneCentrePoint(){
                    
                    coordinate = profileLocation
                }
            
            }
            let erpData = getNearByERPS(coordinate:coordinate,timeStamp: tempSelectedTime)
            
            let currentERPFeatures =   erpData.erpCollection
            
            nearByERPFilter =   erpData.nearByData
            
           
            if let mapViewUpdatable = mobileMapViewUpdatable {
                
                self.updateERPLayerForMobile(mapViewUpdatable: mapViewUpdatable, erpFeatures: currentERPFeatures)
                
                for feature in currentERPFeatures.features{
                    
                    if case let .point(pointData) = feature.geometry{
                        
                        erpCoordinates.append(pointData.coordinates)
                    }
                }
                
                self.adjustZoomeLevelForERP(mapViewUpdatable: mapViewUpdatable)
            }
        }
        
        
    }
    
    func removeERPLayerFromMap(){
        
        if let mapViewUpdatable = mobileMapViewUpdatable {
            
            selectedERPTime = 0 //making seelcted time to default value 0
            mapViewUpdatable.removeERPLayer()
        }
        
    }
    
    #if DEMO
    private func parseAmenities(type: String, amenities: [Amenity]) -> [Turf.Feature] {
        // for testing
//        let array = amenities.prefix(5)
//        let features: [Turf.Feature] = array.map { amenity in
        let features: [Turf.Feature] = amenities.map { amenity in
            let coordinate = CLLocationCoordinate2D(latitude: amenity.lat, longitude: amenity.long)
            var feature = Turf.Feature(geometry: .point(Point(coordinate)))
            feature.properties = [
                "id":.string(amenity.id),
                "type":.string(type)
            ]
            return feature
        }
        return features
    }
    
    private func updateAmenities() {
        if let mapViewUpdatable = mobileMapViewUpdatable {
            updateAmenitiesLayer(mapViewUpdatable: mapViewUpdatable)
        }
        if let mapViewUpdatable = carPlayMapViewUpdatable {
            updateAmenitiesLayer(mapViewUpdatable: mapViewUpdatable)
        }
    }
    #endif
    
    
    
    func retryConnectOBUCarPlay() {
        
        if !self.isOnMobile() {
            OBUHelper.shared.autoConnectOBUFromCarplay = true
        }
        
        OBUHelper.shared.autoConnectOBU { (success, error) in
            if success {
                if appDelegate().carPlayConnected {
                    appDelegate().obuConnected()
                }
            } else {
                if let err = error {
                    SwiftyBeaver.debug("OBU - tryConnectedOBU error: \(err.code) des:\(err.localizedDescription)")
                    
                    let message = "err:\(err.code) des:\(err.localizedDescription)"
                    AnalyticsManager.shared.logSystemEvent(eventValue: ParameterName.SystemAnalytics.SystemEvent.obu_verbose_error, screenName: ParameterName.SystemAnalytics.screen_view, eventMessage: message)
                }
                if appDelegate().carPlayConnected {
                    appDelegate().showOBUReportIssue()
                }
            }
        }
    }
    
}

// MARK: - ERPUpdaterListener
extension MapLandingViewModel: ERPUpdaterListener {
    func shouldAddERPItems(noCostfeatures:Turf.FeatureCollection, costfeatures:Turf.FeatureCollection) {
        SwiftyBeaver.debug("\(costfeatures.features.count) ERPs added:\n \(costfeatures.features) cost erps, \(noCostfeatures.features.count) no-cost erps")
        
        
        /// We should not display any ERP items befroe user clicks on the ERP in the bottom sheet in mobile
        
        if mobileMapViewUpdatable != nil {
            if(landingMode == Values.ERP_MODE && selectedERPTime == 0){
                self.updateERPOnMap()
            }
            //updateERPLayer(mapViewUpdatable: mapViewUpdatable)
        }
        
        
        if let mapViewUpdatable = carPlayMapViewUpdatable {
            currentCostERPFeatures = costfeatures
            updateERPLayer(mapViewUpdatable: mapViewUpdatable)
        }
    }
    
    private func updateERPLayer(mapViewUpdatable: MapViewUpdatable) {
        
        /// To Do - Re- think this logic. As we have already separated the screen for Cruise mode
        /*if(!mapViewUpdatable.isERPLayerExists()){
            
            SwiftyBeaver.debug("Active ERP add ERP Obects to EHorizon In Cruise")
            if let features = currentCostERPFeatures {
                if features.features.count > 0 {
                    mapViewUpdatable.addERPLayer(features: features)
                }
            }
            if let detector = featureDetector{
                detector.removeERPObjectsFromRoadMatcher()
                detector.addERPObjectsToRoadMatcher()
            }
            
        }*/
        //else{
            
            //self.updateERPOnMap()
            mapViewUpdatable.removeERPLayer()
            if let features = currentCostERPFeatures {
                if features.features.count > 0 {
                    mapViewUpdatable.addERPLayer(features: features)
                }
            }
        //}
        
        
    }
    
    private func updateERPLayerForMobile(mapViewUpdatable: MapViewUpdatable,erpFeatures:Turf.FeatureCollection){
        
        mapViewUpdatable.removeERPLayer()
            if erpFeatures.features.count > 0 {
                mapViewUpdatable.addERPBoundLayer(features: erpFeatures)
            }
        
    }
}

extension MapLandingViewModel {
    
    private func etaCallbacks(location:CLLocation){
        
        if let tripETA = ETATrigger.shared.tripCruiseETA{
            
            if  isLiveLocationSharingEnabled && ETATrigger.shared.tripCruiseETA != nil && tripETA.shareLiveLocation.uppercased() == "Y" && !ETATrigger.shared.liveLocTripId.isEmpty  {
                
                ETATrigger.shared.sendLiveLocationCoordinates(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude, status: .inprogress)
            }
            
            if(LocationManager.shared.getDistanceInMeters(fromLoc: location, toLoc: CLLocation(latitude: tripETA.tripDestLat, longitude: tripETA.tripDestLong)) <= Values.etaCruiseDistance && !ETATrigger.shared.isReminder1Sent)
            {
                ETATrigger.shared.etaReminder(reminderType: TripETA.ETAType.Reminder1)
            }
        }
    }
    
    // This should be done by MapBox tracking. But it doesn't work now.
    private func updateLocation(_ location: CLLocation) {
        SwiftyBeaver.self.debug("update location = \(location)")
        var heading: Double = 0
        var newLocation = location
        
#if targetEnvironment(simulator)
        if lastLocation != nil {
            heading = lastLocation!.coordinate.heading(to: location.coordinate)
        }
        SwiftyBeaver.self.debug("simulation heading = \(heading)")
        newLocation = CLLocation(coordinate: location.coordinate, altitude: location.altitude, horizontalAccuracy: location.horizontalAccuracy, verticalAccuracy: location.verticalAccuracy, course: heading, speed: location.speed, timestamp: location.timestamp)
#else
        heading = newLocation.course
        SwiftyBeaver.self.debug("device heading = \(heading)")
#endif
        
        lastLocation = newLocation
        
        if isTrackingUser {
            SwiftyBeaver.self.debug("update location isTrackingUser = true")
            DispatchQueue.main.async {
                self.mobileMapViewUpdatable?.setCamera(at: newLocation.coordinate, heading: heading, animated: true, isCruise: false)
                self.carPlayMapViewUpdatable?.setCamera(at: newLocation.coordinate, heading: heading, animated: true, isCruise: false, isCarPlay: true)
            }
        } else {
            SwiftyBeaver.self.debug("update location isTrackingUser = false")
        }
    }
}

extension MapLandingViewModel: MapViewUpdatableDelegate {
    
    func onParkingEnabled(_ value: Bool) {
        //We don't need to process this
    }
    
    func onShareDriveEnabled(_ value: Bool) {
        // don't need to do anything in Maplanding
    }
    
    func onLiveLocationEnabled(_ value: Bool) {
//        guard isLiveLocationSharingEnabled != value else { return } // only change onece
//        
//        if value {
//            
//            ETATrigger.shared.updateLiveLocationStatus(status: .inprogress)
//            
//        } else {
//            if let location = self.lastLocation {
//                if !ETATrigger.shared.liveLocTripId.isEmpty {
//                    DataCenter.shared.startUpdatingUserTripLiveLocation(tripId: ETATrigger.shared.liveLocTripId, latitude: location.coordinate.latitude.toString(), longitude: location.coordinate.longitude.toString(), status: LiveLocationStatusType.pause.rawValue)
//                    ETATrigger.shared.updateLiveLocationStatus(status: .pause)
//                }
//            }
//        }
//        isLiveLocationSharingEnabled = value
//        
//        // show toast
//        mobileMapViewUpdatable?.updateLiveLocationEnabled(value)
//        carPlayMapViewUpdatable?.updateLiveLocationEnabled(value)
    }
        
    func onMuteEnabled(_ value: Bool) {
       isMute = value
        Prefs.shared.setValue(!value, forkey: .enableAudio)
        if(mobileMapViewUpdatable != nil)
        {
            mobileMapViewUpdatable?.updateMuteStatus(isEnabled: value)
        }
        else
        {
            carPlayMapViewUpdatable?.updateMuteStatus(isEnabled: value)
        }
        
    }
    
    func onSearchAction() {
        //  nothing to be done
    }
    
    func onEndAction() {
        //Not required<#code#>
    }
    
    func onEvChargerAction(_ value: Bool) {
        //Not required
    }
    
    func onPetrolAction(_ value: Bool) {
        //Not required
    }
    
    func onParkingAction() {
        //Not required
    }
}

// MARK: - LocationManagerDelegate
extension MapLandingViewModel: LocationManagerDelegate{
    
    func stoppedForADuration() {
        // Not implemented yet!
    }
    
    // when speed over 20km/h for certain period of time
    func getCruiseSpeedLimitSucces() {
        
        //If we stop cruise mode manually it won't trigger again, even if speed goes over 20km/h
//        if !isCruiseStoppedByUser {
        
        //  Check enable launching cruise mode if last cruise is 1 hour ago
        var shouldLaunchCruise = Prefs.shared.getBoolValue(.shouldLaunchCruiseWhenOverSpeed)
        if let endDate = appDelegate().cruiseEndDate, !shouldLaunchCruise {
            if Date().timeIntervalSince(endDate) >= appDelegate().ENABLE_CRUISE_DURATION {
                SwiftyBeaver.debug("Enable cruise mode again after 1 hour")
                Prefs.shared.setValue(true, forkey: .shouldLaunchCruiseWhenOverSpeed)
            }
        }
        
        shouldLaunchCruise = Prefs.shared.getBoolValue(.shouldLaunchCruiseWhenOverSpeed)
        if shouldLaunchCruise {
            if(!isNavigationStarted && shouldAllowStartCruise /*&& !disableCruise */&& isAppInForeground()) {
               
                SwiftyBeaver.debug("Overspeed > 20km: isNavigation \(isNavigationStarted) shouldAllowStartCruise: \(shouldAllowStartCruise)")
                Prefs.shared.setValue(false, forkey: .shouldLaunchCruiseWhenOverSpeed)
                
                if self.isOnMobile() {
                    self.delegate?.onTriggerCruiseMode20Km()
                } else {
                    if appDelegate().carPlayConnected && appDelegate().carPlayMapController != nil {
                        //  Start cruise mode
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                            appDelegate().carPlayMapController?.startCruise()
                        }
                    }
                }
            }
        }
//        }
    }
    
    func didUpdate(_ location: CLLocation) {

        lastLocation = location
        
        mobileMapViewUpdatable?.updateLocation(location)
        carPlayMapViewUpdatable?.updateLocation(location)
    }
    
    // either mobile or carplay in foreground
    private func isAppInForeground() -> Bool {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return false }
        
        if appDelegate.carPlayConnected {
            // either one is not in background
            return !appDelegate.carplayInBackground || !appDelegate.appInBackground
        } else {
            return !appDelegate.appInBackground
        }
    }
}

extension MapLandingViewModel {
    
    // only start if needShowDemo == true
    func startTimerTutorial(completer: @escaping (Bool) -> ()) {
        
        if (!UserSettingsModel.sharedInstance.needShowDemo) {
            completer(false)
        }
        else{
            
            tutorialTimer = Timer.scheduledTimer(withTimeInterval: MapLandingViewModelConst.timeShowTutorial, repeats: false, block: { _ in
                completer(true)
            })
        }
        
    }
    
    func stopTimerTutorial() {
        if let timer = tutorialTimer {
            timer.invalidate()
            tutorialTimer = nil
        }
    }
    
    func didCloseTutorialSuccess() {
        SettingsService.shared.updateSettingAttribute(UserSettingsModel.UserSettingAttributes.showDemo, value: UserSettingsModel.UserSettingConst.showDemoNo) { _ in
            // Don't need process here
        }
//        readyToShowVouchAnimation()
    }
    
    func saveParkingViewType() {
        guard let code = UserSettingsModel.sharedInstance.carparkAvailabilityCode else {
            return
        }
        SettingsService.shared.updateSettingAttribute(UserSettingsModel.UserSettingAttributes.carparkAvailability, value: code) { _ in
            // Don't need process here
        }
    }
}

extension MapLandingViewModel: AmenitiesZoneDelegate {
    func didGetZoneColor() {
        DispatchQueue.main.async {
            self.mobileMapViewUpdatable?.addProfileZones()
            
            if let centreLocation = SelectedProfiles.shared.getOuterZoneCentrePoint() {
                
                //Resetting to camera to profile location
                if let mobileMapViewUpdatable = self.mobileMapViewUpdatable{
                    
                    mobileMapViewUpdatable.setCameraAfterSearch(location: centreLocation, bottomSheetHeight: self.bottomSheetHeight)
                    
//                    mobileMapViewUpdatable.navigationMapView.mapView.setDynamicZoomLevelBasedonRadiusForProfile(coordinate1: centreLocation, dynamicRadius: SelectedProfiles.shared.getOuterZoneRadius(), padding: UIEdgeInsets(top: 0, left: 0, bottom: mobileMapViewUpdatable.navigationMapView.frame.size.height - CGFloat(self.bottomSheetHeight), right: 0))
                }
                
                
                
                //Remove existing amenities from map layer and update the map layer with near by amenities from Profile location
                AmenitiesSharedInstance.shared.addRequestForSelectedAmenity(location: centreLocation, selectedAmenity: Values.POI)
                
            }
        }
    }
}

extension MapLandingViewModel {
    
    func addOrRemoveAmenityType(_ type: String, isAdding: Bool) {
        if isAdding {
            if amenityTypes.contains(type) {
                //  Do nothing
            } else {
                amenityTypes.append(type)
            }
        } else {
            if let index = amenityTypes.firstIndex(of: type) {
                amenityTypes.remove(at: index)
            }
        }
    }
    
    func checkDropPinBookmark(_ dropPin: DropPinModel) {
        toggleDropPinLanding(dropPin, isDropPin: false) { [weak self] bookmarkId in
            SwiftyBeaver.debug("CHECK bookmark ID: \(bookmarkId ?? 0)")
            self?.addPinLocation(bookmarkId)
        }
    }
    
    func addPinLocation(_ bookmarkId: Int?) {
        if let mobileMapViewUpdatable = self.mobileMapViewUpdatable, let pinModel = self.dropPinModel {
            //  Remove ole location pin before add new one
            removePinLocation()
            
            let feature = getPinLocationFeature(pinModel, bookmarkId: bookmarkId)
            let featureCollection = FeatureCollection(features: [feature])
            mobileMapViewUpdatable.navigationMapView?.mapView.addDropPinLocation(features: featureCollection, type: Values.DROPPIN, isAbovePuck: true)
                        
            NotificationCenter.default.post(name: .openDropPinToolTip, object: feature)
            
            //  Sending data to RN for saving pin location
            toggleDropPinLanding(pinModel, isDropPin: true)
        }
    }
    
    func removePinLocation() {
        toggleDropPinLanding(nil, isDropPin: false)
        if let mobileMapViewUpdatable = self.mobileMapViewUpdatable {
            mobileMapViewUpdatable.navigationMapView?.mapView.removeDropPinLayer()
        }
    }
    
    private func getPinLocationFeature(_ model: DropPinModel, bookmarkId: Int?) -> Turf.Feature {
        let coordinate = CLLocationCoordinate2D(latitude: Double(model.lat) ?? 0.0, longitude: Double(model.long) ?? 0.0)
        var feature = Turf.Feature(geometry: .point(Point(coordinate)))
        var isBookmarked: Bool = false
        if let ID = bookmarkId, ID > 0 {
            isBookmarked = true
        }
        
        feature.properties = [
            "type":.string(Values.DROPPIN),
            "name":.string(model.name ?? ""),
            "address":.string(model.address ?? ""),
            "isBookmarked": .boolean(isBookmarked),
            "lat": .string(model.lat),
            "long": .string(model.long),
        ]
        return feature
    }
}

extension MapLandingViewModel {
    
    private func carplayAutoConnectOBU() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
            if !self.isOnMobile() {
                self.connectOBUIfNeeded()
                self.loadUserPreferenceIfNeeded()
                //  Check and show OBU not connected status
                appDelegate().carPlayMapController?.updateStateConnectOBU()
            }
            Prefs.shared.setValue(true, forkey: .shouldLaunchCruiseWhenOverSpeed)
        }
    }
    
    @objc func carplayMapLoaded(_ notification: Notification) {
        if let carplayConnected = notification.object as? Bool, carplayConnected {
            
            SwiftyBeaver.debug("carplayMapLoaded update user current location")
            
            //  Update user current location when map loaded
            DispatchQueue.main.async {
                if let location = self.lastLocation {
                    SwiftyBeaver.debug("carplayMapLoaded has lastLocation")
                    self.carPlayMapViewUpdatable?.updateLocation(location)
                }
                
                self.carplayAutoConnectOBU()
//                self.carPlayMapViewUpdatable?.cruiseStatusUpdate(isCruise: false)
            }
        }
    }
}

//  MARK: - Load user prefs from local storage
extension MapLandingViewModel {
    private func loadUserPreferenceIfNeeded() {
        let subID = AWSAuth.sharedInstance.cognitoId
        if !subID.isEmpty {
            //  Load user prefs from local user default
            if let prefs = Prefs.shared.loadUserPreferenceWithSub(subID), let location = self.lastLocation?.coordinate {
                SwiftyBeaver.debug("Prefs Loaded")
                AmenitiesSharedInstance.shared.parseUserPreferences(preferences: prefs, location: location)
            } else {
                //  Call API to get user prefs
                UserService().getUserPrefs {[weak self] result in
                    switch result {
                    case .success(let json):
                        if let data = json.data(using: .utf8) {
                            do {
                                let preferences = try JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary
                                if let prefs = preferences?["message"] as? NSDictionary {
                                    Prefs.shared.saveUserPreferenceWithSub(AWSAuth.sharedInstance.cognitoId, preference: prefs)
                                    if let location = self?.lastLocation?.coordinate {
                                        AmenitiesSharedInstance.shared.parseUserPreferences(preferences: prefs, location: location)
                                    }
                                }
                            } catch {
                                print(error.localizedDescription)
                            }
                        }
                        break
                    case .failure(_):
                        break
                    }
                }
            }
        }
    }
}

//  MARK: - Handle auto connect OBU
extension MapLandingViewModel {
    
    public func connectOBUIfNeeded() {
    
        SwiftyBeaver.debug("MapLandingViewModel - OBU - connectOBUIfNeeded")
        
        if !OBUHelper.shared.obuName.isEmpty {
            //  Start mornitoring bluetooth connection
            OBUHelper.shared.startMonitoringBluetooth()
            
            SwiftyBeaver.debug("MapLandingViewModel cognitoId: \(AWSAuth.sharedInstance.cognitoId)")
            
            // Auto connect OBU when user already connect the first time
            if Prefs.shared.isAutoConnectObu(AWSAuth.sharedInstance.cognitoId) {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) { [weak self] in
                    SwiftyBeaver.debug("MapLandingViewModel auto connect OBU")
                    self?.checkAutoConnectObu()
                }
            }
        }
    }
    
    public func checkAutoConnectObu() {
                
        SwiftyBeaver.debug("MapLandingViewModel - OBU - checkAutoConnectObu")
        
        if !OBUHelper.shared.isObuConnected() {
            OBUHelper.shared.willTriggerAutoConnect = false
            SwiftyBeaver.debug("OBU - checkAutoConnectObu")
            
            OBUHelper.shared.autoConnectOBUFromCarplay = true
            
            OBUHelper.shared.autoConnectOBU {  (success, error) in
                if success {
                    //  Carplay only dont need to do anything
                    SwiftyBeaver.debug("OBU - Success connect OBU")
                } else {
                    if let err = error {
                        SwiftyBeaver.debug("OBU - checkAutoConnectObu error: \(err.code) des:\(err.localizedDescription)")
                        let message = "err:\(err.code) des:\(err.localizedDescription)"
                        AnalyticsManager.shared.logSystemEvent(eventValue: ParameterName.SystemAnalytics.SystemEvent.obu_verbose_error, screenName: ParameterName.SystemAnalytics.screen_view, eventMessage: message)
                    }
                }
            }
        }
    }
}

//
//  MapLandingVC+DropPin.swift
//  Breeze
//
//  Created by Tuan, Pham Hai on 10/11/2023.
//

import Foundation
import UIKit
import SwiftyBeaver
import CoreLocation
import Contacts
import SnapKit

extension CLPlacemark {
    var formattedAddress: String? {
        guard let postalAddress = postalAddress else {
            return nil
        }
        let formatter = CNPostalAddressFormatter()
        return formatter.string(from: postalAddress)
    }
}

extension MapLandingVC {
    
    func enableDropPinMode(_ isEnable: Bool) {
        self.dropPinButton?.select(isEnable)
    }
    
    func showDropPinToastMessage(_ isOn: Bool) {
        self.hideLoadingIndicator()
        let textColor = UIColor(hexString: "#222638", alpha: 1.0)
        if isOn {
            showToast(from: self.navigationController?.view ?? self.view, message: "Drop pin mode on", topOffset: 20, icon: "toast_pin_icon_on", messageColor: textColor, duration: 3)
        } else {
            showToast(from: self.navigationController?.view ?? self.view, message: "Drop pin mode off", topOffset: 20, icon: "toast_pin_icon_off", messageColor: textColor, duration: 3)
        }        
    }
    
    func setupDropPinButton() {
        if self.dropPinButton == nil {
            let pinButton = ToggleButton(frame: CGRect(x: 0, y: 0, width: 48, height: 48))
            pinButton.onImage = UIImage(named: "pin_mode_off")
            pinButton.offImage = UIImage(named: "pin_mode_on")
            pinButton.onToggled = { [weak self] isSelected in
                guard let self = self else { return }
                
                self.parkingAmenities?.setOnDropPin(isSelected)                
                
                if isSelected {
                    AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Home.UserClick.drop_pin_on, screenName: ParameterName.Home.homepage_screen_view)
                } else {
                    AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Home.UserClick.drop_pin_off, screenName: ParameterName.Home.homepage_screen_view)
                }
                
                // Enable drop pin mode on and off
                //  1.
                self.mapLandingViewModel?.isOnDropPin = isSelected
                
                //  Show toast message on/off
                self.showDropPinToastMessage(isSelected)
                
                //  Call event to RN side to show/hide bottomsheet
                setOnDropPinBottomsheet(isSelected)
            }
            
            self.view.addSubview(pinButton)
            self.dropPinButton = pinButton
        }
        dropPinButton?.translatesAutoresizingMaskIntoConstraints = false
        if let topView = self.parkingAmenities {
            dropPinButton?.snp.makeConstraints { make in
                make.trailing.equalToSuperview().offset(-8)
                make.width.equalTo(48)
                make.height.equalTo(48)
                make.top.equalTo(topView.snp.bottom).offset(16)
            }
        }
    }
    
    func getDropPinLocationDetails(_ location: CLLocationCoordinate2D, completion: @escaping (DropPinModel?) -> ()) {
        getPinLocationDetails(location: CLLocation(latitude: location.latitude, longitude: location.longitude)) { placemark in
            if let foundLocation = placemark {
                print("getDropPinLocationDetails: name: \(foundLocation.name ?? "") address: \(foundLocation.formattedAddress ?? "")")
                let name: String = foundLocation.name ?? ""
                let address = (foundLocation.formattedAddress ?? "").replacingOccurrences(of: "\n", with: ", ")
                
                if !name.isEmpty && !address.isEmpty {
                    SwiftyBeaver.debug("Address not found")
                }
                let dropPin = DropPinModel(name, address: address, lat: location.latitude.toString(), long: location.longitude.toString())
                completion(dropPin)
            } else {
                completion(nil)
            }
        }
    }
}

//
//  WalkathonArrivalVC.swift
//  Breeze
//
//  Created by Santoso Pham on 20/10/2022.
//

import Foundation
import UIKit

class WalkathonArrivalVC: UIViewController {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    
    // MARK: - Public properties
    var onEndWalkathon: (() -> Void)?
    var checkpointName: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = Constants.walkathonSuccessTitle
        messageLabel.text = """
        \(Constants.walkathonSuccessMessage)
        \(checkpointName).
        """
        backButton.setTitle(Constants.walkathonSuccessBackTitle, for: .normal)
    }

    @IBAction func onEndTrip(_ sender: Any) {
        removeChildViewController(self)
        onEndWalkathon?()
    }
}

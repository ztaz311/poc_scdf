//
//  WalkingNavTopVC.swift
//  Example
//
//  Created by Zhou Hao on 6/5/22.
//  Copyright © 2022 Mapbox. All rights reserved.
//

import UIKit
import CoreLocation
import MapboxCoreNavigation
import MapboxDirections
import MapboxNavigation
import CoreLocation

final class WalkingNavTopVC: ContainerViewController {
        
    var lblInfo: UILabel!
    
    override func viewDidLoad() {
        view.backgroundColor = .black
        super.viewDidLoad()

        setupViews()
        addConstraints()
    }
        
    // TODO: Consider to use the same view with endRoute view
    private func setupViews() {
        view.backgroundColor = .black
        let h: CGFloat = Values.topPanelHeight
        let offset = h + UIApplication.topSafeAreaHeight
        let container = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: offset))
        container.backgroundColor = .black
        self.view.addSubview(container)
        // Brenda mentioned no need to show this icon during in navigation. Asked by Ankur
//        let imageView = UIImageView(image: UIImage(named: "destinationMark"))
//        imageView.frame = CGRect(x: 29, y: 64, width: 54, height: 60)
//        container.addSubview(imageView)
        
//        let label = UILabel(frame: CGRect(x: 97, y: 72, width: 260, height: 40))
        let label = UILabel(frame: CGRect(x: 29, y: 72, width: 260, height: 40))
        label.textColor = .white
        label.font = UIFont.SFProBoldFont(size: 24)
        label.text = ""
        container.addSubview(label)
        self.lblInfo = label
    }
    
    private func addConstraints() {
//        let top = topPaddingView.topAnchor.constraint(equalTo: view.topAnchor)
//        let leading = topPaddingView.leadingAnchor.constraint(equalTo: view.safeLeadingAnchor)
//        let trailing = topPaddingView.trailingAnchor.constraint(equalTo: view.safeTrailingAnchor)
//        let bottom = topPaddingView.bottomAnchor.constraint(equalTo: view.safeTopAnchor)
//        
//        NSLayoutConstraint.activate([top, leading, trailing, bottom])
    }
    
    func updateProgress(_ navigationViewController: NavigationViewController, didUpdate progress: RouteProgress, with location: CLLocation, rawLocation: CLLocation) {
//        let estimatedTime = estimatedArrivalTime(progress.durationRemaining)
        // get minutes
        let minutes = Int(progress.durationRemaining / 60)
        if minutes == 0 {
            lblInfo.text = "less than 1 min walk"
            lblInfo.font = UIFont.boldSystemFont(ofSize: 28)
        } else {
            let info = minutes > 1 ? "mins" : "min"
            lblInfo.text = "\(minutes) \(info) walk"
            lblInfo.font = UIFont.boldSystemFont(ofSize: 32)
        }
    }    
}

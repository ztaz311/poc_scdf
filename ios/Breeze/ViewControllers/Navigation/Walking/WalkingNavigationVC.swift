//
//  WalkingNavigationVC.swift
//  Breeze
//
//  Created by Zhou Hao on 6/5/21.
//

import CoreLocation
import Foundation
import UIKit
import MapboxCoreNavigation
import MapboxDirections
import MapboxNavigation
import MapboxDirections
@_spi(Restricted) import MapboxMaps
import MapboxCoreMaps
import Turf
import Combine
import AVFAudio
import SwiftyBeaver

// MARK: - delegate definition
protocol WalkingNavigationVCDelegate: AnyObject {
    func onArrival()
    func goToParking()
}

final class WalkingNavigationVC: NavigationViewController {

    // MARK: - Constatns
    let xTrailing: CGFloat = 12

    // MARK: - Properties
    
    // MARK: Internal properties
    private var resumeButton: UserLocationButton!
    private var feedbackButton: NewFeedbackButton!
    private var tempMapboxFeedbackButton:UIButton!
    private var topImageView: UIImageView?
        
    private var pointAnnotationManager: PointAnnotationManager?
    var topVC: WalkingNavTopVC!
    
    var destinationAnnotation: PointAnnotation! {
        didSet {
            pointAnnotationManager?.annotations = [destinationAnnotation]
        }
    }
    
    var trackNavigation: Bool = false
    var walkathonId: Int?
    var amenityId: String?
    var layerCode: String?
    var amenityType: String?
    var checkpointName: String?
    var plannedDestAddress1: String?
    var plannedDestAddress2: String?
    var plannedDestLat: Double = 0
    var plannedDestLong: Double = 0
    
    var canReroute: Bool = true
    private var walkathonTripLogger: WalkathonTripLogger!

    // TODO: Make it reusable like endNavigationView
    lazy var endRouteView: UIView = {
        let h: CGFloat = Values.topPanelHeight + 20
        let offset = h + UIApplication.topSafeAreaHeight
        let view = UIView(frame: CGRect(x: 0, y: -offset, width: UIScreen.main.bounds.width, height: offset))
        view.backgroundColor = .black
        let img = UIImage(named: "arrivalDestinationPin")
        let imgView = UIImageView(image: img)
        let y = UIApplication.topSafeAreaHeight + h / 2 - 14
        imgView.frame = CGRect(x: 33, y: y - 4, width: 42, height: 48)
        view.addSubview(imgView)
        let label = UILabel(frame: CGRect(x: 86, y: y, width: 260, height: 35))
        label.text = Constants.cpArrival
        label.textColor = .white
        label.font = UIFont.SFProBoldFont(size: 32)
        view.addSubview(label)
        view.showBottomShadow()
        self.view.addSubview(view)
        return view
    }()
    private var rerouteView: UIView?
    
    // Showing the end navigation view at the bottom when navigation ends
    lazy var endNavigationView: EndNavigationView = {
        let view = EndNavigationView(frame: .zero)
        self.view.addSubview(view)
        self.view.bringSubviewToFront(view)
        return view
    }()
    
    // MARK: - UIViewController life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        toggleDarkLightMode()
        hideFloatingButton()
        SwiftyBeaver.info("WalkingNavigationVC start in mobile")
        self.showsSpeedLimits = false
        setupResumeButton()

        self.delegate = self
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(navigationCameraStateDidChange(_:)),
                                               name: .navigationCameraStateDidChange,
                                               object: navigationMapView?.navigationCamera)

        // Disable the voice instruction in mobile
        if let synthesizer = voiceController.speechSynthesizer as? MultiplexedSpeechSynthesizer {
            synthesizer.speechSynthesizers = []
        }
                        
        // #273 - suggested by Ochi
        NavigationSettings.shared.distanceUnit = .kilometer
        navigationMapView?.mapView?.gestures.options.pitchEnabled = false
        navigationMapView?.mapView.ornaments.options.attributionButton.visibility = .hidden
        navigationMapView?.mapView.mapboxMap.onNext(event: .mapLoaded) { [weak self] _ in
            guard let self = self else { return }
            self.setupNavigationCamera()
            
            if self.canReroute == false {
                self.navigationMapView?.maneuverArrowColor = .clear
                self.navigationMapView?.maneuverArrowStrokeColor = .clear
                self.navigationMapView?.mapView.ornaments.options.scaleBar.visibility = .hidden
                self.navigationMapView?.mapView.ornaments.options.compass.visibility = .visible
                self.navigationMapView?.mapView.ornaments.options.attributionButton.visibility = .hidden
                self.navigationMapView?.mapView.ornaments.options.compass.position = .topRight
                self.navigationMapView?.mapView.ornaments.options.compass.margins = CGPoint(x: 16.0, y: 16)
                
                if let route = self.routeResponse.routes?.first {
                    self.addWalkingPath(route: route)
                }
            }
        }
        navigationMapView?.mapView.mapboxMap.onNext(event: .styleLoaded) { [weak self] _ in
            guard let self = self else { return }
            self.pointAnnotationManager = self.navigationMapView?.mapView.annotations.makePointAnnotationManager()
        }
        
        navigationMapView?.mapView.mapboxMap.onEvery(event: .cameraChanged) { _ in
            // Need this, otherwise navigationCameraStateDidChange will not be called            
        }
        
        appDelegate().startNavigationRecordingHistoryData()
        
        //  BREEZES-7641 walking trip log summary
        self.walkathonTripLogger = WalkathonTripLogger(service: TripWalkathonService())
        if let location = self.navigationService.router.location {
            getAddressFromLocation(location: location) { [weak self] address in
                guard let self = self else { return }
                self.walkathonTripLogger.start(startLocation: location.coordinate, address1: address, address2: "", amenityId: self.amenityId, layerCode: self.layerCode, amenityType: self.amenityType , plannedDestAddress1: self.plannedDestAddress1 ?? "", plannedDestAddress2: self.plannedDestAddress2 ?? "", plannedDestLat: self.plannedDestLat, plannedDestLong: self.plannedDestLong)
                
                self.setupObservers()
            }
        }
        
        // TODO: To check why has to call twice to make it work. We've called this in .mapLoaded event.
//        if !self.canReroute {
//            self.navigationMapView?.maneuverArrowColor = .clear
//            self.navigationMapView?.maneuverArrowStrokeColor = .clear
//        }
    }
    
    private func setupObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(progressDidChange(_ :)), name: .routeControllerProgressDidChange, object: nil)
    }
    
    @objc private func progressDidChange(_ notification: NSNotification) {
//        let rawLocation = notification.userInfo?[RouteController.NotificationUserInfoKey.rawLocationKey] as? CLLocation,
        guard let location = notification.userInfo?[RouteController.NotificationUserInfoKey.locationKey] as? CLLocation
        else { return }
        
//        if trackNavigation {
//            if let viewportDataSource = (self.navigationMapView?.navigationCamera.viewportDataSource as? NavigationViewportDataSource) {
//                var cameraOptions = viewportDataSource.followingMobileCamera
//                let coordinate = CLLocationCoordinate2D(latitude: rawLocation.coordinate.latitude, longitude: rawLocation.coordinate.longitude)
//                cameraOptions.center = coordinate
//                self.navigationMapView?.navigationCamera.cameraStateTransition.update(to: cameraOptions, state: .following)
//            }
//        }
        
        walkathonTrackLocation(location)
        
        if trackNavigation {
            self.walkathonTripLogger.update(location: location.coordinate)
        }
    }
    
    //  MARK: BREEZES-7639 - Public user location when user is walking
    private func walkathonTrackLocation(_ location: CLLocation) {
        if let id = self.walkathonId, trackNavigation {
            let userId = AWSAuth.sharedInstance.cognitoId
            let time = "\(Date().timeIntervalSince1970)"
            DataCenter.shared.startUpdatingUserWalkathonLocation(userId: userId, walkathonId: "\(id)", latitude: location.coordinate.latitude.toString(), longitude: location.coordinate.longitude.toString(), currentTime: time, data: "")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        setupDarkLightAppearance()
        toggleDarkLightMode()
    }


    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
                
        topVC.view.showBottomShadow()
    }
    
    deinit {
        SwiftyBeaver.info("WalkingNavigationVC deinit")
        NotificationCenter.default.removeObserver(self)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return isDarkMode ? .lightContent : .darkContent
    }

    private func toggleDarkLightMode() {
        self.navigationMapView?.mapView.mapboxMap.style.uri = StyleURI(url: URL(string: isDarkMode ? Constants.Map.walkathonNightStyleUrl : Constants.Map.walkathonStyleUrl)!)
    }

    // MARK: - Theme updated
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        guard previousTraitCollection != traitCollection else {
            return
        }
       toggleDarkLightMode()
    }

    private func setupNavigationCamera() {
        
        self.floatingButtons = []
        
        if trackNavigation {
//            let navigationViewportDatasource = NavigationViewportDataSource(_: (self.navigationMapView?.mapView)!, viewportDataSourceType: .raw)
            // you can keep your following camera options as it is
//            self.navigationMapView?.navigationCamera.viewportDataSource = navigationViewportDatasource
            
            if let mapView = self.navigationMapView?.mapView  {
                let customViewportDataSource = CustomViewportDataSource(mapView)
                self.navigationMapView?.navigationCamera.viewportDataSource = customViewportDataSource
                
                let customCameraStateTransition = CustomCameraStateTransition(mapView)
                self.navigationMapView?.navigationCamera.cameraStateTransition = customCameraStateTransition
            }
        }
        
        if let viewportDataSource = (self.navigationMapView?.navigationCamera.viewportDataSource as? NavigationViewportDataSource) {
            viewportDataSource.followingMobileCamera.zoom = 16.5
            viewportDataSource.options.followingCameraOptions.zoomRange = 16.5...16.5
            viewportDataSource.options.followingCameraOptions.zoomUpdatesAllowed = false
            viewportDataSource.options.followingCameraOptions.pitchUpdatesAllowed = false
            viewportDataSource.followingMobileCamera.pitch = 0
            
            //This fix is from here -> https://github.com/mapbox-collab/ncs-collab/issues/293
            viewportDataSource.options.followingCameraOptions.bearingSmoothing.maximumBearingSmoothingAngle = 30.0
        }
    }

    private func setupFeedbackButton(){
        
        let feedbackBtn = NewFeedbackButton(buttonSize: 48)
//        feedbackBtn.addTarget(self, action: #selector(feedbackIssue), for: .touchUpInside)
//
//        feedbackBtn.frame = CGRect(x: UIScreen.main.bounds.width - 60, y: UIScreen.main.bounds.height - 130, width: 48, height: 48)
        feedbackBtn.frame = CGRect(x: 100, y: 300, width: 48, height: 48)

        feedbackBtn.translatesAutoresizingMaskIntoConstraints = false
        navigationMapView?.mapView.addSubview(feedbackBtn)
        navigationMapView?.mapView.bringSubviewToFront(feedbackBtn)
//
        self.feedbackButton = feedbackBtn
//
//        // setup autolayout
//        feedbackButton.snp.makeConstraints { (make) in
//            make.trailing.equalToSuperview().offset(-xTrailing)
//            make.bottom.equalTo(self.navigationMapView!.snp.bottom).offset(-(110+UIApplication.bottomSafeAreaHeight))
//            make.height.equalTo(48)
//            make.width.equalTo(48)
//        }
        
    }
    
    private func setupResumeButton() {
        let userLocationButton = UserLocationButton(buttonSize: (Images.locationImage?.size.width)!)
        userLocationButton.addTarget(self, action: #selector(resumeButtonTapped), for: .touchUpInside)

        userLocationButton.translatesAutoresizingMaskIntoConstraints = false
        navigationMapView?.mapView.addSubview(userLocationButton)

        self.resumeButton = userLocationButton
        userLocationButton.isHidden = true // hide it by default

        // setup autolayout
        userLocationButton.snp.makeConstraints { (make) in
            make.trailing.equalToSuperview().offset(-xTrailing)
//            make.bottom.equalTo(self.feedbackButton.snp.top).offset(-10)
            make.bottom.equalToSuperview().offset(-150)
            make.height.equalTo(48)
            make.width.equalTo(48)
        }
    }
    
    @objc private func resumeButtonTapped() {        
        navigationMapView?.navigationCamera.follow()
        hideFloatingButton()
    }
    
    func hideFloatingButton() {
        if let arrayButton = floatingButtons {
            for btn in arrayButton {
                btn.isHidden = true
            }
        }
    }
    
    @objc func feedbackIssue(){
        
//        let storyboard: UIStoryboard = UIStoryboard(name: "Feedback", bundle: nil)
//        let vc: FeedbackVC = storyboard.instantiateViewController(withIdentifier: "FeedbackVC")  as! FeedbackVC
//        vc.modalPresentationStyle = .formSheet
//        vc.locCoordinate = self.navigationMapView?.mapView.location.latestLocation?.coordinate
//        vc.navigationService = self.navigationService
//        vc.feedBack = navigationService.eventsManager.createFeedback()
//        self.present(vc, animated: true, completion: nil)
    }
}

extension WalkingNavigationVC: NavigationViewControllerDelegate {
            
    func navigationViewController(_ navigationViewController: NavigationViewController, didAdd finalDestinationAnnotation: PointAnnotation, pointAnnotationManager: PointAnnotationManager){

        let point = finalDestinationAnnotation.point
          var destinationAnnotation = PointAnnotation(coordinate: point.coordinates)
          destinationAnnotation.image = .init(image: UIImage(named: "destinationMark")!, name: "navigationDest")
        navigationViewController.navigationMapView?.pointAnnotationManager?.annotations = [destinationAnnotation]
    }
        
    func navigationViewController(_ navigationViewController: NavigationViewController, didUpdate progress: RouteProgress, with location: CLLocation, rawLocation: CLLocation) {
        //No need to handle this, as we are handling route progress from model class
        self.topVC.updateProgress(navigationViewController, didUpdate: progress, with: location, rawLocation: rawLocation)
        
//        if trackNavigation {
//            self.walkathonTripLogger.update(location: location.coordinate)
//        }
    }
            
    func navigationViewController(_ navigationViewController: NavigationViewController, didArriveAt waypoint: Waypoint) -> Bool {
                
        if navigationViewController.navigationService.routeProgress.isFinalLeg {
            // end
            endRoute()
            return true
        }
        return true
    }
    
    func navigationViewControllerDidDismiss(_ navigationViewController: NavigationViewController, byCanceling canceled: Bool) {
    }

    func navigationViewController(_ navigationViewController: NavigationViewController, didRerouteAlong route: Route) {
        if canReroute {
            reroute()
        }
    }
    
    func navigationViewController(_ navigationViewController: NavigationViewController, shouldRerouteFrom location: CLLocation) -> Bool{
        return canReroute
    }
    
    func navigationViewController(_ navigationViewController: NavigationViewController, routeLineLayerWithIdentifier identifier: String, sourceIdentifier: String) -> LineLayer? {
        var lineLayer = LineLayer(id: identifier)
        lineLayer.source = sourceIdentifier
        
        // `identifier` parameter contains unique identifier of the route layer or its casing.
        // Such identifier consists of several parts: unique address of route object, whether route is
        // main or alternative, and whether route is casing or not. For example: identifier for
        // main route line will look like this: `0x0000600001168000.main.route_line`, and for
        // alternative route line casing will look like this: `0x0000600001ddee80.alternative.route_line_casing`.
//            lineLayer.lineColor = .constant(.init(identifier.contains("main") ? #colorLiteral(red: 0.337254902, green: 0.6588235294, blue: 0.9843137255, alpha: 1) : #colorLiteral(red: 0.6, green: 0.6, blue: 0.6, alpha: 1)))
        
        if trackNavigation {
            lineLayer.lineColor = .constant(.init(#colorLiteral(red: 0.3647058823, green: 0.6549019607, blue: 1.0, alpha: 0)))
            lineLayer.lineWidth = .expression(lineWidthExpression(0.0))
        }else {
            lineLayer.lineColor = .constant(.init(#colorLiteral(red: 0.3647058823, green: 0.6549019607, blue: 1.0, alpha: 1)))
            lineLayer.lineWidth = .expression(lineWidthExpression(0.4))
        }
        lineLayer.lineJoin = .constant(.round)
        lineLayer.lineCap = .constant(.square)
        lineLayer.lineDasharray = .constant([1.0, 1.0])
        
        return lineLayer    }
    
    func navigationViewController(_ navigationViewController: NavigationViewController, routeCasingLineLayerWithIdentifier identifier: String, sourceIdentifier: String) -> LineLayer? {
        var lineLayer = LineLayer(id: identifier)
        lineLayer.source = sourceIdentifier
        
        // Based on information stored in `identifier` property (whether route line is main or not)
        // route line will be colored differently.
        if trackNavigation {
            lineLayer.lineColor = .constant(.init(identifier.contains("main") ? #colorLiteral(red: 0.1843137255, green: 0.4784313725, blue: 0.7764705882, alpha: 1) : #colorLiteral(red: 0.4, green: 0.4, blue: 0.4, alpha: 0)))
            lineLayer.lineWidth = .expression(lineWidthExpression(0))
        }else {
            lineLayer.lineColor = .constant(.init(identifier.contains("main") ? #colorLiteral(red: 0.1843137255, green: 0.4784313725, blue: 0.7764705882, alpha: 1) : #colorLiteral(red: 0.4, green: 0.4, blue: 0.4, alpha: 1)))
            lineLayer.lineWidth = .expression(lineWidthExpression(1.2))
        }
        lineLayer.lineJoin = .constant(.round)
        lineLayer.lineCap = .constant(.round)
        lineLayer.lineOpacity = .constant(0)
        
        return lineLayer
    }
    
    private func lineWidthExpression(_ multiplier: Double = 1.0) -> Expression {
        let lineWidthExpression = Exp(.interpolate) {
            Exp(.linear)
            Exp(.zoom)
            // It's possible to change route line width depending on zoom level, by using expression
            // instead of constant. Navigation SDK for iOS also exposes `RouteLineWidthByZoomLevel`
            // public property, which contains default values for route lines on specific zoom levels.
            RouteLineWidthByZoomLevel.multiplied(by: multiplier)
        }
        
        return lineWidthExpression
    }
    
    private func addWalkingPath(route: Route, finalDestination: LocationCoordinate2D? = nil) {
        guard let shape = route.shape, let navigationMapView = self.navigationMapView else { return }
        
        let sourceIdentifier = "WalkingPathLine"
        // Create a GeoJSON data source.
        var routeLineSource = GeoJSONSource()
        routeLineSource.data = .feature(Turf.Feature(geometry: LineString(shape.coordinates)))
        
        // Create a line layer
        var lineLayer = LineLayer(id: "line-layer")
        lineLayer.source = sourceIdentifier
        lineLayer.lineColor = .constant(.init(#colorLiteral(red: 0.3647058823, green: 0.6549019607, blue: 1.0, alpha: 1)))
        lineLayer.lineWidth = .expression(lineWidthExpression(0.4))
        lineLayer.lineJoin = .constant(.round)
        lineLayer.lineCap = .constant(.square)
        lineLayer.lineDasharray = .constant([1.0, 1.0])

        // Add the lineLayer to the map.
        do {
            try navigationMapView.mapView.mapboxMap.style.addSource(routeLineSource, id: sourceIdentifier)
            try navigationMapView.mapView.mapboxMap.style.addPersistentLayer(lineLayer, layerPosition: .below("puck"))
        } catch(let error) {
            SwiftyBeaver.error("Failed to add walking path: \(error.localizedDescription)")
        }
    }
    
    func saveTripLog(location: CLLocation, fromUser: Bool,completion: ((Bool) -> ())? = nil){
                
        SwiftyBeaver.debug("Save walkathon trip log during end navigation \(location.coordinate.latitude)-\(location.coordinate.longitude)")
        
        // how to get the destination coordinate (selectedAddress in original implementation)
        getAddressFromLocation(location: location) { [weak self] address in
            guard let self = self else { return }
            
            SwiftyBeaver.debug("Address Received while saving trip log \(address)")
            
            self.walkathonTripLogger.stop(endLocation: location.coordinate, address1: address, address2: "", stoppedByUser: fromUser, amenityType: self.amenityType, amenityId: self.amenityId, layerCode: self.layerCode ,plannedDestAddress1: self.plannedDestAddress1 ?? "", plannedDestAddress2: self.plannedDestAddress2 ?? "", plannedDestLat: self.plannedDestLat, plannedDestLong: self.plannedDestLong, completion: completion)
        }
    }
    
    private func showArriveCheckPoint() {
        let storyboard = UIStoryboard(name: "Navigation", bundle: nil)
        if let vc = storyboard.instantiateViewController(withIdentifier: String(describing: WalkathonArrivalVC.self)) as? WalkathonArrivalVC {
            vc.checkpointName = self.checkpointName ?? ""
            vc.onEndWalkathon = { [weak self] in
                guard let self = self else { return }
                appDelegate().navigationSubmitHistoryData(description: "WalkingNavigation-End")
                self.refreshCustomMap()
                self.dismiss(animated: true, completion: nil)
            }
            self.addChildViewControllerWithView(childViewController: vc as UIViewController,toView: self.view)
        }
    }
    
    private func refreshCustomMap() {
        if trackNavigation {
            let contentId = Prefs.shared.getIntValue(.walkathonContentId)
            if contentId > 0 {
                let userInfo = [Values.ContentIdKey:contentId] as [String : Any]
                NotificationCenter.default.post(name: Notification.Name(Values.NotificationSelectContent), object: nil,userInfo: userInfo)
                
                //  Then remove contentId immediately
                Prefs.shared.removeValueForKey(.walkathonContentId)
            }
        }
    }
    
    func endRoute(fromUser: Bool = false) {
        if let location = self.navigationService.router.location {
            walkathonTrackLocation(location)
//            if trackNavigation {
                saveTripLog(location: location, fromUser: fromUser) {[weak self] success in
                    DispatchQueue.main.async {
                        self?.handleEndRoute(fromUser: fromUser, success: success)
                    }
                }
//            }else {
//                handleEndRoute(fromUser: fromUser)
//            }
        }else {
            handleEndRoute(fromUser: fromUser)
        }
    }
    
    private func handleEndRoute(fromUser: Bool = false, success: Bool = false) {
        if fromUser {
            self.refreshCustomMap()
            appDelegate().navigationSubmitHistoryData(description: "WalkingNavigation-ManuallyEnd")
            self.dismiss(animated: true, completion:nil)
        } else {
            
            UIView.animate(withDuration: 0.3) {
                let offset = Values.topPanelHeight + UIApplication.topSafeAreaHeight
                self.endRouteView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: offset+20)
            } completion: { _ in
                self.endNavigationView.show(animated: true)
                self.endNavigationView.onEndAction = { [weak self] in
                    AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Navigation.UserClick.end_walking,
                                                          screenName: ParameterName.Navigation.screen_view)
                    guard let self = self else { return }
                    appDelegate().navigationSubmitHistoryData(description: "WalkingNavigation-End")
                    self.refreshCustomMap()
                    self.dismiss(animated: true, completion: nil)
                }
                
                if self.trackNavigation && !fromUser && success {
                    self.showArriveCheckPoint()
                }
            }
        }
    }
    
    private func reroute() {

        guard rerouteView == nil else { return }
        
        // add rerouting view
        let reroutingView = UIView()
        view.backgroundColor = .lightGray
        self.navigationMapView?.addSubview(reroutingView)
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 60))
        label.text = "Rerouting..."
        label.textColor = .white
        label.textAlignment = .center
        label.font = UIFont.SFProBoldFont(size: 24)
        reroutingView.addSubview(label)
        
        let originalFrame = CGRect(x: 0, y: 138 - 60, width: topVC.view.bounds.width, height: 60)
        reroutingView.frame = originalFrame
        reroutingView.backgroundColor = .darkGray
        self.rerouteView = reroutingView
        
        let offset = Values.topPanelHeight + UIApplication.topSafeAreaHeight
        let newFrame = CGRect(x: 0, y: offset, width: self.topVC.view.bounds.width, height: 60)
        
        UIView.animate(withDuration: 0.3) { [weak self] in
            guard let self = self else { return }
            
            reroutingView.frame = newFrame
            self.view.layoutIfNeeded()
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now()+5) {
            UIView.animate(withDuration: 0.3, delay: 0, options: UIView.AnimationOptions.curveEaseOut) {
                reroutingView.frame = originalFrame
                self.view.layoutIfNeeded()

            } completion: { _ in
                reroutingView.removeFromSuperview()
                self.rerouteView = nil
            }
        }
    }

}

extension WalkingNavigationVC {
    @objc func navigationCameraStateDidChange(_ notification: Notification) {
        guard let navigationCameraState = notification.userInfo?[NavigationCamera.NotificationUserInfoKey.state] as? NavigationCameraState else { return }
        
        switch navigationCameraState {
        case .transitionToFollowing, .following:
            resumeButton?.isHidden = true
            break
        case .idle, .transitionToOverview, .overview:
            resumeButton?.isHidden = false
            break
        }
    }
}
    
extension WalkingNavigationVC: NavProgressVCDelegate {
    func onConnectOBUFailure() {
        //  Nothing to be done here
    }
    
    func onConnectOBUSuccess() {
        //  Nothing to be done here
    }
    
    func onEnd() {
        
        // For testing: to start cruise
//        LocationManager.shared.startSimulateCarMoving(after: 1)
        endRoute(fromUser: true)
    }
    
    func onShowCarparkList() {
        //  Nothing to be done here
    }
    
    func onShareDestination() {
        //  Nothing to be done here
    }
}


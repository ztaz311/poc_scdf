//
//  EndWalkathonVC.swift
//  Breeze
//
//  Created by Tan, Tran Ngoc on 02/11/2022.
//

import UIKit
import CoreLocation
import MapboxCoreNavigation
import MapboxDirections
import MapboxNavigation
import CoreLocation

protocol EndWalkathonVCDelegate: AnyObject {
    func onEnd() // end button clicked
}

class EndWalkathonVC: ContainerViewController {
    
    @IBOutlet weak var endButton: UIButton!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var estimateArriveTime: TwoTextLable!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var leadingConstraint: NSLayoutConstraint!
    
    weak var navigationViewController: NavigationViewController?
    weak var delegate: NavProgressVCDelegate?
    var timer: Timer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.roundCorners([.topLeft, .topRight], radius: 30)
        self.view.shadowToTopOfView()
        heightConstraint.constant = 90
        titleLabel.text = Constants.walkathonArrival

        // Do any additional setup after loading the view.
        startTimer()
    }
    
    private func startTimer() {
        self.timer = Timer.scheduledTimer(timeInterval: 60, target: self, selector: #selector(self.updateTimer), userInfo: nil, repeats: false)
    }
    
    
    @objc func updateTimer() {
        self.timer?.invalidate()
        self.timer = nil
        hideETATime()
    }
    
    private func setupGradientColor() {
        DispatchQueue.main.async {
            self.view.backgroundColor = UIColor.navBottomBackgroundColor1
        }
    }

    @IBAction func onEnd(_ sender: Any) {
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Navigation.UserClick.end_navigation, screenName: ParameterName.Navigation.screen_view)
        
        // add prompt?
        self.popupAlert(title: nil, message: Constants.endNavigationMessage, actionTitles: [Constants.cancel,Constants.end], actions:[{action1 in
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Navigation.UserClick.pop_user_end_no, screenName: ParameterName.Navigation.screen_view)
            
        },{ [weak self] action2 in
            guard let self = self else { return }
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Navigation.UserClick.pop_user_end_yes, screenName: ParameterName.Navigation.screen_view)
            
            self.navigationViewController?.navigationService.stop()
            self.delegate?.onEnd()
        }, nil])
    }
    
    
    func navigationService(_ service: NavigationService, didArriveAt waypoint: Waypoint) -> Bool {
        return true
    }
    
    func navigationService(_ service: NavigationService, shouldRerouteFrom location: CLLocation) -> Bool {
        return true
    }
    
    // MARK: - NavigationServiceDelegate implementation
    func navigationService(_ service: NavigationService, didUpdate progress: RouteProgress, with location: CLLocation, rawLocation: CLLocation) {
        let estimatedTime = estimatedArrivalTime(progress.durationRemaining)
        estimateArriveTime.set(estimatedTime.time, estimatedTime.format)

    }
    func hideETATime() {
        leadingConstraint.constant = 16
        estimateArriveTime.isHidden = true
        titleLabel.isHidden = true
    }
    
    // MARK: - Theme updated
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        guard previousTraitCollection != traitCollection else {
            return
        }
        setupGradientColor()
    }

}

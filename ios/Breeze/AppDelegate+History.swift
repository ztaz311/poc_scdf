//
//  AppDelegate+History.swift
//  Breeze
//
//  Created by Zhou Hao on 23/2/22.
//

import Foundation
import SwiftyBeaver

extension AppDelegate {
    func updatePendingHistoryData() {
        
        DispatchQueue.global().async { [weak self] in
            guard let self = self else { return }
            do {
                let appUrl = try FileManager.default.url(for:.documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
                let historyUrl = appUrl.appendingPathComponent("history")

                let enumerator = FileManager.default.enumerator(atPath: historyUrl.path)
                let filePaths = enumerator?.allObjects as! [String]
                let historyFileNames = filePaths.filter{ $0.contains(".gz") }
                for name in historyFileNames{
                    let url = historyUrl.appendingPathComponent(name)
                    self.submitHistory(description: "General-appLaunch", url: url)
                }
                
            } catch {
                
            }
        }
    }
    
    func submitHistory(description: String, url: URL) {
        
        var userName = "\(AWSAuth.sharedInstance.cognitoId)"
        if userName.isEmpty {
            userName = "UnknownUser"
        }
        
        FirebaseFeedbackService().submit(userId: userName, description: description, url: url, progress: { progress in
            
            SwiftyBeaver.debug("History data upload progress \(progress)")
            
        }, completion: { result in
            switch result {
            case .failure(let error):
                SwiftyBeaver.error("Failed to submitHistory: \(error.localizedDescription)")
                
            case .success(_):
                // remove the file
                SwiftyBeaver.debug("History submited: [\(description)]")
                try? FileManager.default.removeItem(at: url)
                break
            }
        }, compression: false)
    }
}

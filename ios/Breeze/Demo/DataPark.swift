
import Foundation
struct DataPark : Codable {
	let carparkNo : String?
	let lotsAvailable : String?
	let lotType : String?

	enum CodingKeys: String, CodingKey {

		case carparkNo = "carparkNo"
		case lotsAvailable = "lotsAvailable"
		case lotType = "lotType"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		carparkNo = try values.decodeIfPresent(String.self, forKey: .carparkNo)
		lotsAvailable = try values.decodeIfPresent(String.self, forKey: .lotsAvailable)
		lotType = try values.decodeIfPresent(String.self, forKey: .lotType)
	}

}

//
//  FeatureDetector+DemoFeature.swift
//  Breeze
//
//  Created by VishnuKanth on 05/10/21.
//

import Foundation
import MapboxCoreNavigation

extension FeatureDetector{
    
    func findERPObjectsForDemo(identifier:RoadObject.Identifier){
        
        let components = identifier.components(separatedBy: "-")
        if components.count > 1 {
            let idString = components[1]
        
            #if DEMO
            if(idString == "36")
            {
                if(isPaidShown && isApproachingPaid){
                    
                    let featureCollection = DataCenter.shared.getERPCostFeatures()
                    for feature in featureCollection.features {
                        if let id = feature.properties?["erpid"] as? Int {
                            let erpid = "\(id)"
                            if erpid == idString {
                                if case let .string(address) = feature.properties?["name"],
                                   case let .string(zoneId) = feature.properties?["zoneid"],
                                   case let .string(strCost) = feature.properties?["cost"]
                                {
                                let cost = strCost.replacingOccurrences(of: "$", with: "").toDouble() ?? 0.0
                                let erp = DetectedERP(id: erpid, distance: 0, priority: FeatureDetection.cruisePriorityErp, address: address, price: cost, zoneId: zoneId)
                                delegate?.featureDetector(self, didExit: erp)
                                break
                                }
                            }
                        }
                    }

                    let noCostFeatureCollection = DataCenter.shared.getERPNoCostFeatures()
                    for feature in noCostFeatureCollection.features {
                        if let id = feature.properties?["erpid"] as? Int {
                            let erpid = "\(id)"
                            if erpid == idString {
                                // 2. get the properties
                                if case let .string(address) = feature.properties?["name"],
                                   case let .string(zoneId) = feature.properties?["zoneid"]{
                                let erp = DetectedERP(id: erpid, distance: 0, priority: FeatureDetection.cruisePriorityErp, address: address, price: 0, zoneId: zoneId)
                                delegate?.featureDetector(self, didExit: erp)
                                break
                                }
                            }
                        }
                    }
                }
                
            }
            #endif
        }
        
    }
}

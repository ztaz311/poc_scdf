//
//  MonitorCarParkVC.swift
//  ParkingDemo
//
//  Created by VishnuKanth on 24/06/21.
//

import UIKit
@_spi(Restricted) import MapboxMaps
import MapboxCoreMaps
import MapboxCoreNavigation
import MapboxDirections
import MapboxNavigation
import Turf
import AVFoundation
import SnapKit
//class InfoPointAnnotation: MGLPointAnnotation {
//    
//    var info: AnnotationInfo?
//
//}

class MonitorCarParkVC: UIViewController {

    @IBOutlet var halfMapView: NavigationMapView!
    var mapView: NavigationMapView!
    @IBOutlet var fullMapView: NavigationMapView!
    @IBOutlet var autoPark: UIImageView!
    @IBOutlet var autoFill: UIImageView!
    @IBOutlet var closeBtn: UIButton!
    @IBOutlet var closeBtn1: UIButton!
    //@IBOutlet var logButton: UIButton!
    var rectangleLayer: CAShapeLayer?
    var timer: Timer?
    var startParking:Timer?
    var savedFilesDictionary = [[String: String]]()
    //var newEntryDictionary = [String: String]()
    private let locationManager = CLLocationManager()
    var switchSpeed = "KPH"
    var arrayMPH: [Double]! = []
    var arrayKPH: [Double]! = []
    var location = CLLocation()
    var isMonitored = false
    var alreadyWentToparking = false
    var zone2Detcted = false
    var zone3Detected = false
    var zone2BothNotify = false
    var zone3BothNotify = false
//    var geofencAnn: [AnnotationInfo] { get { return MapVCModel.geofencing } }
    var synthesizer: SpeechSynthesizing!
    var startTime = ""
    var endTime = ""
    var carPlayNavUpdatable:CarPlayNavigationMapViewUpdatable?
    @IBOutlet var costLbl:UILabel!
    @IBOutlet var timeLbl:UILabel!
    @IBOutlet var parkingLbl:UILabel!
    var seconds = 0
    var cost = 0.0
    @IBOutlet var parkingBtn:UIButton!
    var buttonFlashing = false
//    private var cpParkingEnd: CPParkingEnd?
//    private var phoneParkingEnd:PhoneParkingEnd?
//    private var phoneParkZone:PhoneParkZone?
//    private var cpParkZone:CPParkZone?
    @IBOutlet var lineView:UIView!
    @IBOutlet var startView:UIView!
    var audioPlayer:AVAudioPlayer = AVAudioPlayer()

    var notificationTimer:Timer!
    let yNotificationGap: CGFloat = 16
//    private lazy var carplayNotificationView: NotificationView = {
//
//        let view = NotificationView()
//        view.tag = 5000
//        return view
//
//    } ()
    private var notificationVC: NavNotificationVC!
    private var currentFeature: FeatureDetectable? {
        didSet {
            DispatchQueue.main.async {
                print("Current Feature notify %@",self.currentFeature)
                //self.updateNotification(featureId: self.currentFeature == nil ? oldValue?.id : self.currentFeature!.id)
            }
        }
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        DemoSession.shared.parkAnyWhere = true
        setUPHalfMapView()
        setUPMapView(tempMapView: fullMapView)
        self.setUPUI()
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        //locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        locationManager.startUpdatingLocation()
        carPlayNavUpdatable = CarPlayNavigationMapViewUpdatable(navigationMapView: fullMapView)

        
//        setupCPParkingEndView()
//        setupCPParkZoneview()
        
        
//        cpParkZone?.layer.opacity = 0
//        cpParkingEnd?.layer.opacity = 0
        

        // Do any additional setup after loading the view.
    }
    
    func setUPMapView(tempMapView:NavigationMapView){
        
        mapView = tempMapView
        self.setupPuck(mapView: mapView)
        mapView.mapView.mapboxMap.style.uri = StyleURI(url: URL(string:"mapbox://styles/breezemap/ckugzyzw599wz17prq28c42ri")!)
        mapView.mapView.mapboxMap.onEvery(event: .styleLoaded) { [weak self] _ in
            guard let self = self else { return }
            
            self.setupMapCamera(mapView: self.mapView)
            
        }
        mapView.navigationCamera.stop()
        mapView.mapView.gestures.options.pitchEnabled = false
        mapView.mapView.ornaments.options.attributionButton.visibility = .hidden
    }
    
    func setUPHalfMapView(){
        
        self.setupPuck(mapView: halfMapView)
        halfMapView.mapView.mapboxMap.style.uri = StyleURI(url: URL(string:"mapbox://styles/breezemap/ckugzyzw599wz17prq28c42ri")!)
        halfMapView.mapView.mapboxMap.onEvery(event: .styleLoaded) { [weak self] _ in
            guard let self = self else { return }
            
            self.setupMapCamera(mapView: self.halfMapView)
            
        }
        halfMapView.navigationCamera.stop()
        halfMapView.mapView.gestures.options.pitchEnabled = false
        halfMapView.mapView.ornaments.options.attributionButton.visibility = .hidden
    }
    
    private func setupMapCamera(mapView:NavigationMapView) {
        //align the navMapView bottom to the top of the bottom sheet
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {                        
            let coordinate = LocationManager.shared.location.coordinate
            mapView.mapView.mapboxMap.setCamera(to: CameraOptions(center: coordinate, zoom: 19))
            mapView.mapView.ornaments.options.attributionButton.visibility = .hidden
        }
    }
    
    private func setupPuck(mapView:NavigationMapView) {
        
        
        let puck2d = Puck2DConfiguration(topImage: UIImage(named: "puckArrow"), bearingImage: UIImage(named: "puckIcon"), shadowImage: nil, scale: nil)
        
        mapView.userLocationStyle = .puck2D(configuration: puck2d)
        mapView.mapView.ornaments.options.compass.visibility = .hidden
        
        
    }
    
    func hideAndShowViews(isHide:Bool){
        
        parkingBtn.isHidden = isHide
        parkingLbl.isHidden = isHide
        costLbl.isHidden = isHide
        timeLbl.isHidden = isHide
        self.autoFill.isHidden = isHide
        self.autoPark.isHidden = isHide
        halfMapView.isHidden = isHide
        lineView.isHidden = isHide
        startView.isHidden = isHide
        
    }
    
    func setUPUI(){
        
        fullMapView.isHidden = false
        closeBtn.bringSubviewToFront(self.view)
        hideAndShowViews(isHide: true)
        parkingBtn.isHidden = true
        costLbl.text = "$0.00"
        timeLbl.attributedText = NSAttributedString(string: "0 hr 0 min")
        let accessToken = Bundle.main.object(forInfoDictionaryKey: "MGLMapboxAccessToken") as? String
        let rivaSpeechSynthesizer = RivaSpeechSynthesizer()
        self.synthesizer = MultiplexedSpeechSynthesizer([rivaSpeechSynthesizer])
        self.setupNotification()
    }
    
    private func setupNotification() {
        let storyboard = UIStoryboard(name: "Navigation", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: String(describing: NavNotificationVC.self)) as! NavNotificationVC
//        addChildViewControllerWithView(childViewController: vc, toView: self.mapView.mapView)
        addChildViewControllerWithView(childViewController: vc, toView: self.view)
                
        self.notificationVC = vc
//        let offset = self.mapView.mapView.frame.origin.y + UIApplication.topSafeAreaHeight + yNotificationGap
        let offset = UIApplication.topSafeAreaHeight + yNotificationGap
        notificationVC.view.snp.makeConstraints { (make) in
//            make.top.equalTo(self.mapView.mapView.safeAreaLayoutGuide.snp.topMargin).offset(offset)
            make.top.equalToSuperview().offset(offset)
            make.leading.equalToSuperview().offset(22)
            make.trailing.equalToSuperview().offset(-26)
            make.height.equalTo(Values.navigationNotificationHeight)
        }
        
        notificationVC.view.isHidden = true
        self.view.bringSubviewToFront(notificationVC.view)
    }
    
//    private func setupPhoneParkZoneView() {
//        
//        if phoneParkZone == nil {
//            phoneParkZone = PhoneParkZone(frame: CGRect(x: 16, y: 60, width: 251, height: 251 * 0.37))
//        }
//        self.mapView!.addSubview(phoneParkZone!)
//        
//        phoneParkZone?.parkinSession = ""
//        phoneParkZone?.layer.opacity = 0
//        phoneParkZone!.translatesAutoresizingMaskIntoConstraints = false
//        phoneParkZone!.snp.makeConstraints { (make) in
//            let width = 251.0
//            make.width.equalTo(width)
//            make.height.equalTo(251 * 0.37)
//            make.trailing.equalToSuperview().offset(-24)
//            make.top.equalTo(self.mapView!.snp.top).offset(80)
//        }
//    
//    }
//    
//    private func setupCPParkZoneview() {
//        
//        if cpParkZone == nil {
//            cpParkZone = CPParkZone(frame: CGRect(x: 53, y: 153, width: 251, height: 89))
//            cpParkZone?.tag = 8000
//            
//            
//        }
//        
//        let carPlayMap = self.carPlayRootMapView()
//        if(carPlayMap != nil)
//        {
//            let oldView = carPlayMap!.viewWithTag(8000)
//            
//            if(oldView != nil)
//            {
//                oldView?.removeFromSuperview()
//            }
//        }
//        
//        if let carplayMap = self.carPlayRootMapView(),carplayMap.viewWithTag(8000) == nil
//        {
//            carplayMap.addSubview(self.cpParkZone!)
//            carplayMap.bringSubviewToFront(view)
//            
//            cpParkZone?.parkinSession = ""
//            
//            cpParkZone!.translatesAutoresizingMaskIntoConstraints = false
//            cpParkZone!.snp.makeConstraints { (make) in
//                make.leading.equalToSuperview().offset(53)
//                make.top.equalTo(carplayMap.snp.top).offset(153)
//                make.size.equalTo(CGSize(width: 251, height: 89))
//            }
//            
//        }
//    
//    }
//    
//    
//    private func setupPhoneParkingEndView() {
//        
//        if phoneParkingEnd == nil {
//            phoneParkingEnd = PhoneParkingEnd(frame: CGRect(x: 16, y: 16, width: 320, height: 320 * 0.37))
//        }
//        self.mapView!.addSubview(phoneParkingEnd!)
//        
//        phoneParkingEnd?.time = ""
//        phoneParkingEnd?.layer.opacity = 0
//        phoneParkingEnd!.translatesAutoresizingMaskIntoConstraints = false
//        phoneParkingEnd!.snp.makeConstraints { (make) in
//            let width = 320.0
//            make.width.equalTo(width)
//            make.height.equalTo(width * 0.37)
//            make.trailing.equalToSuperview().offset(-24)
//            make.top.equalTo(self.mapView!.snp.top).offset(80)
//        }
//    
//    }
//    
//    private func setupCPParkingEndView() {
//        
//        if cpParkingEnd == nil {
//            cpParkingEnd = CPParkingEnd(frame: CGRect(x: 53, y: 153, width: 320, height: 115))
//            cpParkingEnd?.tag = 6000
//            
//            
//        }
//        
//        let carPlayMap = self.carPlayRootMapView()
//        if(carPlayMap != nil)
//        {
//            let oldView = carPlayMap!.viewWithTag(6000)
//            
//            if(oldView != nil)
//            {
//                oldView?.removeFromSuperview()
//            }
//        }
//        
//        if let carplayMap = self.carPlayRootMapView(),carplayMap.viewWithTag(6000) == nil
//        {
//            carplayMap.addSubview(self.cpParkingEnd!)
//            carplayMap.bringSubviewToFront(view)
//            
//            cpParkingEnd?.time = ""
//            
//            cpParkingEnd!.translatesAutoresizingMaskIntoConstraints = false
//            cpParkingEnd!.snp.makeConstraints { (make) in
//                make.leading.equalToSuperview().offset(53)
//                make.top.equalTo(carplayMap.snp.top).offset(153)
//                make.size.equalTo(CGSize(width: 320, height: 115))
//            }
//            
//        }
//    
//    }
    
    
    func  startFlashingbutton()
    {
        if (buttonFlashing)
        {
            return;
        }
        
        buttonFlashing = true;
        self.parkingBtn.alpha = 1.0;
        parkingBtn.isHidden = false
        UIView.animate(withDuration: 1, delay: 0.8, options: [.curveEaseInOut, .autoreverse], animations: {
            UIView.modifyAnimations(withRepeatCount: 3, autoreverses: true) {
                    print("repeated stuff")
                 }
            self.parkingBtn.alpha = 0.2;
            
        }, completion: { finished in
            
        
            self.parkingBtn.alpha = 0.0
            self.autoPark.isHidden = true
            self.autoFill.isHidden = false
            
            self.fullMapView.isHidden = true
            self.removeChildViewController(self.notificationVC)
            self.mapView = self.halfMapView
            self.setupNotification()
            self.hideAndShowViews(isHide: false)
            self.parkingLbl.text = Constants.parkingStarted
            
            self.showNotifyOnCarPlayAndPhone(text:Constants.parkingStartedMsg)
            self.announceVoiceForParkingEnd(voiceMessage: Constants.parkingStartedMsg)
            
            let date = Date()
            self.startTime = date.getTimeAMPM
            self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.updateTimer), userInfo: nil, repeats: true)
        });

    }
    
    @objc func startParkingTimer(){
        
        
        startFlashingbutton()
        
        
    }
    
    @objc func updateTimer() {
            
          seconds += 1
          timeString(time: TimeInterval(seconds))
        }
        
        func timeString(time:TimeInterval) {
            let hours = Int(time) / 3600
            let minutes = Int(time) / 60 % 60
            let seconds = Int(time) % 60
            
            
            cost = cost + 0.02
            
            var date = DateUtils.shared.getDateAMPM(startTime)
            
            date?.addTimeInterval(time)
            print("START TIME",startTime)
            print("END TIME",date?.getTimeAMPM ?? "")
            endTime = date?.getTimeAMPM ?? ""
            costLbl.text = String(format: "$%.2f", cost)
            let str = String(format:"%02i:%02i:%02i", hours, minutes, seconds)
            timeLbl.attributedText = NSAttributedString(string: str)
//            return String(format:"%02i:%02i:%02i", hours, minutes, seconds)
        }
    
//    @IBAction func logDataClick(_ sender: Any) {
//
//        saveToJsonFile(result: savedFilesDictionary, logged: true)
//        retrieveERPFromJsonFile()
//
//    }
    
    @IBAction func closeClick(_ sender: Any) {
        
//        self.audioPlayer.volume = 0.0
//        self.audioPlayer.stop()
        DemoSession.shared.parkAnyWhere = false
        self.navigationController?.popViewController(animated: true)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //self.view.sendSubviewToBack(monitorLabel)
    }
    
    deinit {
        
        self.stopTimer()
        locationManager.stopUpdatingLocation()
        locationManager.delegate = nil
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.stopTimer()
        locationManager.stopUpdatingLocation()
        locationManager.delegate = nil
    }
    
    func stopTimer(){
        
        if(timer != nil)
        {
            timer?.invalidate()
            timer = nil
        }
        
        if(startParking != nil)
        {
            startParking?.invalidate()
            startParking = nil
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
   
    

    func updateLocationInfo(latitude: CLLocationDegrees, longitude: CLLocationDegrees, speed: CLLocationSpeed, direction: CLLocationDirection) {
        let speedToMPH = (speed * 2.23694)
        let speedToKPH = (speed * 3.6)
        
        let now = Date()
        var newEntryDictionary = [String: String]()
        
        let stringDate = DateUtils.shared.getTimeDisplay(dateFormat: Date.formathhmmssaonMMMddyyyy, date: now)
        
        newEntryDictionary["lat"] = latitude.toString()
        newEntryDictionary["long"] = longitude.toString()
        newEntryDictionary["timeStamp"] = "\(now.timeIntervalSince1970)"
        newEntryDictionary["realTime"] = stringDate
        if switchSpeed == "MPH" {
            // Chekcing if speed is less than zero or a negitave number to display a zero
            if (speedToMPH > 0) {
                let speed = (String(format: "%.0f mph", speedToMPH))
                print(speed)
                
            } else {
                //speedDisplay.text = "0 mph"
            }
        }
        
        if switchSpeed == "KPH" {
            // Checking if speed is less than zero
            newEntryDictionary["speed"] = "\(speedToKPH)"
            if (speedToKPH < 3) {
                
                if(zone2Detcted == true && zone2BothNotify == false)
                {
                    
                    if(startParking == nil)
                    {
                        startParking = Timer.scheduledTimer(timeInterval:6, target: self, selector: #selector(startParkingTimer), userInfo: nil, repeats: false)
                    }
                    
                }
                
                
            }
            else
            {
                stopTimer()
                
            }
            
            avgSpeed()
            //let speedDisplay = (String(format: "%.0f km/h", speedToKPH))
            
        } else {
            
            print("Zone XXXX Speed",speed)
            if(speed > 8)
            {
                
                
                
            }
            
            
        }
        
        savedFilesDictionary.append(newEntryDictionary)
        
    }
    
    
    @objc func goToParking() {
        
        if(timer != nil)
        {
            timer?.invalidate()
        }
        
        self.goToParkingScreen(via: "Speed")
        
    }


func avgSpeed(){
        if switchSpeed == "MPH" {
            let speed:[Double] = arrayMPH
            let speedAvg = speed.reduce(0, +) / Double(speed.count)
            let avgSpeed = (String(format: "%.0f", speedAvg))
            print( avgSpeed )
        } else if switchSpeed == "KPH" {
            let speed:[Double] = arrayKPH
            let speedAvg = speed.reduce(0, +) / Double(speed.count)
            let avgSpeed = (String(format: "%.0f", speedAvg))
            print( avgSpeed)
        }
    }
    
    func goToParkingScreen(via:String){
        
//        if(alreadyWentToparking == false)
//        {
//            alreadyWentToparking = true
//            var VCs = self.navigationController?.viewControllers
//            VCs?.removeLast()
//            let parkingVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "autoParkVC") as! AutoParkingStart
//            parkingVC.isDetectedThrough = via
//           VCs?.append(parkingVC)
//           self.navigationController?.setViewControllers(VCs ?? [], animated: true)
//        }
    }
    
//    func getMonitorRangeForZone3(){
//
//        let rect = getMonitorRange(at: location)
//        let zone3 = mapView.mapView.map visibleFeatures(in: rect, styleLayerIdentifiers: ["zone3"])
//        //let zone3Null = mapView.visibleFeatures(in: rect, styleLayerIdentifiers: ["zone3-null"])
//        print("ZONE 3", zone3.first)
//        //print("ZONE3 NULL", zone3Null.first)
//
//        if(zone3.count != 0)
//        {
//            if(zone3Detected == false)
//            {
//                zone2Detcted = false
//                zone3Detected = true
//                monitorLabel.text = "Your vehicle is not registered to park at handicap car lot, please move your vehicle.Your are subjected to fine of $300 for illegal parking"
//
//            }
//
//
//        }
//        else
//        {
//            if(zone3Detected == true)
//            {
//                zone2Detcted = false
//                zone2BothNotify = false
//                zone3Detected = false
//                self.monitorLabel.text = "We are fetching your parking details to start..."
//            }
//        }
////        if(zone3Null.count != 0)
////        {
//
//
//
//        //}
//
//    }
    
    
    private func drawDebugLayer(rect:CGRect) {
            if rectangleLayer != nil {
                rectangleLayer?.removeFromSuperlayer()
            }
            
//            let rect = FeatureDetector.createMonitorRectangle(at: location, in: navMapView.mapView)
            let path = UIBezierPath()
            path.move(to: CGPoint(x: rect.origin.x, y: rect.origin.y))
            path.addLine(to: CGPoint(x: rect.origin.x + rect.width, y: rect.origin.y))
            path.addLine(to: CGPoint(x: rect.origin.x + rect.width, y: rect.origin.y + rect.height))
            path.addLine(to: CGPoint(x: rect.origin.x, y: rect.origin.y + rect.height))
            path.addLine(to: CGPoint(x: rect.origin.x, y: rect.origin.y))
            
            let shapeLayer = CAShapeLayer()
            shapeLayer.path = path.cgPath
            shapeLayer.fillColor = UIColor.green.withAlphaComponent(0.8).cgColor
            rectangleLayer = shapeLayer
        
           mapView.mapView.layer.addSublayer(rectangleLayer!)
    }
    
    func updateNotification(){
        
        if currentFeature != nil {
            // show
            notificationVC.view.isHidden = false
        
            closeBtn.isHidden = true
            closeBtn1.isHidden = true
            if(notificationTimer == nil)
            {
                notificationTimer = Timer.scheduledTimer(timeInterval: 4, target: self, selector: #selector(removeNotification), userInfo: nil, repeats: false)
            }
        } else {
            
            notificationVC.view.isHidden = true
            closeBtn.isHidden = false
            closeBtn1.isHidden = false

        }

        carPlayNavUpdatable?.updateNotification(featureId: currentFeature?.id, currentFeature: currentFeature)
        self.notificationVC.update(currentFeature)

    }
    
    @objc func removeNotification(){
        notificationTimer.invalidate()
        notificationTimer = nil
        currentFeature = nil
        updateNotification()
    }
    
    func showNotifyOnCarPlayAndPhone(parkingEnded:Bool = false,text:String = ""){

       
        if(parkingEnded == false)
        {
            let feature = DetectedETANotificationView(id: "N", distance: 0.0, priority: 1, category: Values.carPark, message: text, rName: "")
            self.currentFeature = feature
            UIView.animate(withDuration: 1, delay: 0, options: .curveEaseOut, animations: {

                self.updateNotification()
            }, completion: { finished in

//                UIView.animate(withDuration: 5, delay: 0, options: .curveEaseOut, animations: {
//                    self.currentFeature = nil
//                    self.updateNotification()
//                }, completion: { finished in
//
//                });
            });
        }
        else
        {

            let parkingTime = "Jalan Pelepah - \(startTime) - \(endTime) - \(costLbl.text!)"
            
            let feature = DetectedETANotificationView(id: "N", distance: 0.0, priority: 1, category: Values.carPark, message: parkingTime, rName: costLbl.text!)
            self.currentFeature = feature
            
            UIView.animate(withDuration: 1, delay: 0, options: .curveEaseOut, animations: {

                self.updateNotification()
            }, completion: { finished in

//                UIView.animate(withDuration: 4, delay: 0, options: .curveEaseOut, animations: {
//                    self.currentFeature = nil
//                    self.updateNotification()
//                }, completion: { finished in
//
//                });
            });
        }

    }
    
    func setSpokenInstructionText(text:String,ssmlText:String, distanceAlongStep: CLLocationDistance = 0){
        
       let instruction = SpokenInstruction(distanceAlongStep: distanceAlongStep, text:text, ssmlText: ssmlText)
        

        let step = RouteStep(transportType: .automobile, maneuverLocation: CLLocationCoordinate2D(latitude: 0, longitude: 0), maneuverType: .depart, instructions: "", drivingSide: .left, distance: CLLocationDistance(0), expectedTravelTime: TimeInterval(0))
                
        self.synthesizer.speak(instruction, during: RouteLegProgress(leg: RouteLeg(steps: [step], name: "", distance: 0, expectedTravelTime: 0, profileIdentifier: .automobile)), locale: Locale.enGBLocale())
    }
    
    func announceVoiceForParkingEnd(voiceMessage:String){
        
        DispatchQueue.global(qos: .background).async {
            let ssmlText = "<speak><amazon:effect name=\"drc\"><prosody rate=\"1.08\">\(voiceMessage)</prosody></amazon:effect></speak>"
            self.setSpokenInstructionText(text: voiceMessage, ssmlText: ssmlText, distanceAlongStep: 0)
//        let accessToken = Bundle.main.object(forInfoDictionaryKey: "MGLMapboxAccessToken") as? String
//        let speechSynthesizer = SpeechSynthesizer(accessToken:accessToken)
//        let options = SpeechOptions(text: voiceMessage)
//
//
//            speechSynthesizer.audioData(with: options) { audioData, error in
//                if(error == nil)
//                {
//
//                    /*let path = Bundle.main.path(forResource: "example.mp3", ofType:nil)!
//                    let url = URL(fileURLWithPath: path)
//                    do {
//                        self.audioPlayer = try AVAudioPlayer(contentsOf: url)
//                        self.audioPlayer.play()
//                    } catch {
//                        // couldn't load file :(
//                    }*/
//                    do {
//
//                        self.audioPlayer  = try AVAudioPlayer(data: audioData!)
//                        self.audioPlayer.delegate = self
//                        self.audioPlayer.prepareToPlay()
//                        self.audioPlayer.volume = 1.0
//                        let audioSession = AVAudioSession.sharedInstance()
//                        do{
//                            //try audioSession.setCategory(.ambient)
//                            try audioSession.setCategory(.playback, mode: .voicePrompt, options: [.duckOthers, .mixWithOthers])
//
//                            try AVAudioSession.sharedInstance().setActive(true)
//                        }
//                        catch let error{
//                            print(error.localizedDescription)
//                        }
//
//                        self.audioPlayer.play()
//                    } catch let error {
//                        print(error.localizedDescription)
//                    }
//
//                    //Do something with the audioData
//                }
//            }
        }
        
        
    }
    
    func getMonitorRange(){
        
        var zone2 = [String]()
        var zone2Null = [String]()
        let rect = getMonitorRange(at: location)
        drawDebugLayer(rect: rect)
        let Zone2Options = RenderedQueryOptions(layerIds: ["zone2","zone2-null"], filter: nil)
        //let Zone2NullOptions = RenderedQueryOptions(layerIds: ["zone2-null"], filter: nil)
        mapView.mapView.mapboxMap.queryRenderedFeatures(in: rect, options: Zone2Options) { [weak self] (result) in
            
            guard let self = self else { return }

            switch result {

            case .success(let zone):
                
                let queriedFeatureIds: [String] = zone.compactMap {
                    let feature = $0.sourceLayer
                    print("source ID",feature)
                    if(feature == "Demo")
                    {
                        return "zone2"
                    }
                    else if(feature == "Demo_Zone2_null")
                    {
                        return "zone2Null"
                    }
                    
                    return feature
                    
                }
                if(queriedFeatureIds.contains("zone2"))
                {
                    zone2 = queriedFeatureIds
                    if(self.zone2Detcted == false)
                    {
                        self.zone2Detcted = true
                        
                        self.showNotifyOnCarPlayAndPhone(text:Constants.enteredParkingZone)
                        self.announceVoiceForParkingEnd(voiceMessage: Constants.enteredParkingZone)
                    }
                }
                
                if(queriedFeatureIds.contains("zone2Null"))
                {
                    zone2Null = queriedFeatureIds
                    if(self.zone2BothNotify == false && zone2.count == 0 && self.zone2Detcted == true)
                    {
                        self.zone2BothNotify = true
                        self.parkingLbl.text = "\(Constants.parkingEnded) \(String(describing: self.costLbl.text!)) successfully"
                        self.stopTimer()
                        self.showNotifyOnCarPlayAndPhone(parkingEnded: true)
                        self.announceVoiceForParkingEnd(voiceMessage: "\(Constants.parkingEndedVoiceMsg) \(String(describing: self.costLbl.text!)) successfully")
                        //VoiceHelper.instance.startSpeech(text: "Your parking session has been ended")
                    }

                }
                else
                {
                    if(self.zone2BothNotify == true)
                    {
                        if(zone2Null.count == 0)
                        {

                            DispatchQueue.main.asyncAfter(deadline: .now() + 5) {


                                self.navigationController?.popViewController(animated: true)
                            }
                        }

                    }
                }

                

            case .failure(let error):
                break
            }
        }
        
        
    }
    
        
    private func getMonitorRange(at location: CLLocation) -> CGRect {
        return MonitorCarParkVC.createMonitorRectangle(at: location, in: mapView)
    }
    
    static func createMonitorRectangle(at location: CLLocation, in map: NavigationMapView?) -> CGRect {

        // method 1

        let point = map!.mapView.mapboxMap.point(for: location.coordinate)
        let u = Projection.metersPerPoint(for: location.coordinate.latitude, zoom: 17)
        
        print("ALTI",u)
//        let dy = (Values.detectionRange / u) * Double(cos(map.pitch))
        let dy = (2 / u)
        
        let dx = 2 / u
        return CGRect(x: Double(point.x) -  dx/8, y: Double(point.y) - dy/2, width: dx, height: dy)
//        return CGRect(x: point.x -  dx / 2, y: point.y - dx, width: dx, height: dx)
    }
    
}
extension MonitorCarParkVC: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
                
        location = locations.last!
        if (location.horizontalAccuracy > 0) {
            updateLocationInfo(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude, speed: location.speed, direction: location.course)
        }
        location = locations.last!
        //if(zone2BothNotify == false && zone3Detected == false)
       // {
            
            self.getMonitorRange()
//        }
//        else
//        {
//
//            //self.getMonitorRangeForZone3()
//
//        }
        
        print("Updated - \(locations.description) ")
        
        
    }
    
    func locationManager(_ manager: CLLocationManager, didStartMonitoringFor region: CLRegion) {
        manager.requestState(for: region)
            print("Started Monitoring Region: \(region.identifier)")
        }
    
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        
       
        if(isMonitored == false)
        {
            
            isMonitored = true
//            DispatchQueue.main.async {
//                self.goToParkingScreen(via: "Geofence")
//            }
            
        }
        print("Entering - \(region)")
        
        
    }
    
    func locationManager(_ manager: CLLocationManager, didDetermineState state: CLRegionState, for region: CLRegion) {
        if state == .inside
        {
                
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        print("Exiting - \(region)")
        
        
    }
    
    func locationManager(_ manager: CLLocationManager, monitoringDidFailFor region: CLRegion?, withError error: Error) {
        print("Faile", error)
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        print("Authorized status changed")
        
    }
}

extension MonitorCarParkVC:AVAudioPlayerDelegate{
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        
        //Not required at the moment
    }
    
    func audioPlayerDecodeErrorDidOccur(_ player: AVAudioPlayer, error: Error?) {
        //Not required at the moment
    }
}

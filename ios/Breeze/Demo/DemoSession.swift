//
//  DemoSession.swift
//  Breeze
//
//  Created by Zhou Hao on 26/7/21.
//

import Foundation
import CoreLocation
import MapboxMaps
import MapboxCoreMaps
import Turf
final class DemoSession {
    static let shared = DemoSession()
    
    enum Status {
        case none
        case inFirstLeg
        case firstLegComplete
        case inSecondLeg
        case complete
    }
    
    // MARK: - Constants
    static let range = 200.0 // 50m
    
    // MARK: - Properties
    var config: DemoConfig?
    private (set) var isDemoRoute = false
    var status: Status = .none
    private(set) var currentDemoFeatureCollection = FeatureCollection(features: [])
    var parkTimer:Timer?
    var carParkCapacity = ""
    var recipientName = ""
    var parkAnyWhere = false
    
    private init() {
        do {
            let data = try Data(contentsOf: URL(fileURLWithPath: Bundle.main.path(forResource: "demo", ofType: "json")!))
            self.config = try JSONDecoder().decode(DemoConfig.self, from: data)
        } catch(let err) {
            print(err)
        }
    }
    
    // Need to call this to decide whether the navigation is in demo mode
    public func startFirstLeg(start: CLLocationCoordinate2D, destination: CLLocationCoordinate2D) {
        guard let config = self.config else {
            return
        }
        
        let demoStartCoordinates = CLLocationCoordinate2D(latitude: config.start.latitude, longitude: config.start.longitude)
        let demoDestinationCoordinates = CLLocationCoordinate2D(latitude: config.destination.latitude, longitude: config.destination.longitude)
        isDemoRoute = demoStartCoordinates.distance(to: start) < DemoSession.range && demoDestinationCoordinates.distance(to: destination) < DemoSession.range
        
        status = .inFirstLeg
    }
    
    // demo session
    public func startSecondLeg(start: CLLocationCoordinate2D, destination: CLLocationCoordinate2D) {
        
        do {
            let data = try Data(contentsOf: URL(fileURLWithPath: Bundle.main.path(forResource: "demoLeg2", ofType: "json")!))
            self.config = try JSONDecoder().decode(DemoConfig.self, from: data)
            
            guard let config = self.config else {
                return
            }
            
            let demoStartCoordinates = CLLocationCoordinate2D(latitude: config.start.latitude, longitude: config.start.longitude)
            let demoDestinationCoordinates = CLLocationCoordinate2D(latitude: config.destination.latitude, longitude: config.destination.longitude)
            isDemoRoute = demoStartCoordinates.distance(to: start) < DemoSession.range && demoDestinationCoordinates.distance(to: destination) < DemoSession.range
            
            status = .inSecondLeg
            
        } catch(let err) {
            print(err)
        }
        
    }
    
    public func findUserInLeg1OrLeg2(start:CLLocationCoordinate2D,destination:CLLocationCoordinate2D){
        guard let leg1Config = self.config else {
            return
        }
        
        let demoStartCoordinates = CLLocationCoordinate2D(latitude: leg1Config.start.latitude, longitude: leg1Config.start.longitude)
        let demoDestinationCoordinates = CLLocationCoordinate2D(latitude: leg1Config.destination.latitude, longitude: leg1Config.destination.longitude)
        if(demoStartCoordinates.distance(to: start) < DemoSession.range && demoDestinationCoordinates.distance(to: destination) < DemoSession.range){
            status = .inFirstLeg
            return
        }
        
        do {
            let data = try Data(contentsOf: URL(fileURLWithPath: Bundle.main.path(forResource: "demoLeg2", ofType: "json")!))
            self.config = try JSONDecoder().decode(DemoConfig.self, from: data)
            
            guard let leg2Config = self.config else {
                return
            }
            
            
            
            let demoLeg2StartCoordinates = CLLocationCoordinate2D(latitude: leg2Config.start.latitude, longitude: leg2Config.start.longitude)
            let demoLeg2DestinationCoordinates = CLLocationCoordinate2D(latitude: leg2Config.destination.latitude, longitude: leg2Config.destination.longitude)
            if(demoLeg2StartCoordinates.distance(to: start) < DemoSession.range && demoLeg2DestinationCoordinates.distance(to: destination) < DemoSession.range){
                status = .inSecondLeg
            }
            
        } catch(let err) {
            print(err)
        }
        
    }
    
//    func updateDemoRoadObjects(){
//        self.parseJSONItems { demoFeatureCollection in
//            self.currentDemoFeatureCollection = demoFeatureCollection
//        }
//    }
    
    func parseDemoJSONItems(_ completion: @escaping ((Turf.FeatureCollection)-> Void)) {
        
        var demoRoadObjects = [Turf.Feature]()
        
        //  Loop each waypoint
        if let waypoints = config?.waypoints, !waypoints.isEmpty {
            for demoWayPoint in waypoints {
                
                //Adding each point coordinate to Turf.Feature and appending to Array of Features
                let demoCoord = CLLocationCoordinate2D(latitude: demoWayPoint.latitude, longitude: demoWayPoint.longitude)
                
                var feature = Turf.Feature(geometry: .point(Point(demoCoord)))
                
                feature.properties = [
                    "Type":.string(demoWayPoint.name),
                    "Message":"",
                    "id": .string(demoWayPoint.id)
                ]
                demoRoadObjects.append(feature)
            }
        }
        
        DispatchQueue.main.async {
            let demoFeatureCollection = FeatureCollection(features: demoRoadObjects)
            self.currentDemoFeatureCollection = demoFeatureCollection
            completion(demoFeatureCollection)
            
        }
    }
    
    func startTimerToGetParkingLots(){
        
        DispatchQueue.main.async {
            if(self.parkTimer == nil)
          {
                self.parkTimer = Timer.scheduledTimer(timeInterval:5, target: self, selector: #selector(self.getCarParkAvailability), userInfo: nil, repeats: true)
          }
        }
        
    }
    
    func stopParkingTimer(){
        
        if(parkTimer != nil){
            parkTimer?.invalidate()
            parkTimer = nil
        }
    }
    
    func getDemoVoiceInstructions(type:String) -> (String,String){
        var text = ""
        var ssmlText = ""
        if(type == Values.wayPointERP)
        {
            text = "Approaching E R P at Ayer Rajah ExpressWay, Do you want to re-route?"
            ssmlText = "<speak><amazon:effect name=\"drc\"><prosody rate=\"1.08\">Approaching E R P at Ayer Rajah ExpressWay, Do you want to re-route?</prosody></amazon:effect></speak>"
        }
        else if(type == Values.wayPointPaid){
            text = "$2 successfully deducted for E R P"
            ssmlText = "<speak><amazon:effect name=\"drc\"><prosody rate=\"1.08\">$2 successfully deducted for E R P</prosody></amazon:effect></speak>"
            
        }
        else if(type == Values.wayPointETAExpress){
            text = Constants.pbVoiceMessage
            ssmlText = "<speak><amazon:effect name=\"drc\"><prosody rate=\"1.08\">\(Constants.pbVoiceMessage)</prosody></amazon:effect></speak>"
            
        }
        else if(type == Values.wayPointSpeed){
            text = "Speed limit is now 60km/h"
            ssmlText = "<speak><amazon:effect name=\"drc\"><prosody rate=\"1.08\">Speed limit is now 60km/h</prosody></amazon:effect></speak>"
            
        }
        
        else if(type == Values.wayPointRoad){
            text = "There are \(self.carParkCapacity) parking lots available at destination"
            ssmlText = "<speak><amazon:effect name=\"drc\"><prosody rate=\"1.08\">There are \(self.carParkCapacity) parking lots available at destination</prosody></amazon:effect></speak>"
            
        }
        else if(type == Values.etaNotifyName){
            
            text = "Message has been sent to \(recipientName)"
            ssmlText = "<speak><amazon:effect name=\"drc\"><prosody rate=\"1.08\">Message has been sent to \(recipientName)</prosody></amazon:effect></speak>"
        }
        
        else if(type == Values.wayPointCarparkLots)
        {
            text = "There are parking lots near you: at destination: 470."
            ssmlText = "<speak><amazon:effect name=\"drc\"><prosody rate=\"1.08\">There are parking lots near you: at destination: 470.</prosody></amazon:effect></speak>"
           
        }
        
        else if(type == Values.wayPointCarParkLevel3)
        {
            text = "Head to Level 3 for more parking Lots."
            ssmlText = "<speak><amazon:effect name=\"drc\"><prosody rate=\"1.08\">Head to Level 3 for more parking Lots.</prosody></amazon:effect></speak>"
        }
        
        else if(type == Values.wayPointCyclist1 || type == Values.wayPointCyclist2) {
            text = "Cyclists ahead"
            ssmlText = "<speak><amazon:effect name=\"drc\"><prosody rate=\"1.08\">Cyclists  ahead</prosody></amazon:effect></speak>"
        }
        
        else{
            text = "\(type) ahead"
            ssmlText = "<speak><amazon:effect name=\"drc\"><prosody rate=\"1.08\">\(type) ahead</prosody></amazon:effect></speak>"
        }
        
        return (text,ssmlText)
    }
    
    @objc func getCarParkAvailability(){
        
        DispatchQueue.global(qos: .background).async { [weak self] in
            guard let self = self else { return }
            DemoCarPark.shared.getDemoCarParkDetails() { (result) in
                
                switch result {
                case .success(let json):
                    DispatchQueue.main.async {
                        
                        print("CAR PARK LOTS JSON %@",json)
                        print("CAR PARK LOTS %@",json.data?.lotsAvailable ?? "")
                        self.carParkCapacity =  json.data?.lotsAvailable ?? ""
                        
                    }
                    
                case .failure(let error ):
                    print("Car Park error",error)
                }
            }
        }
    }
    
}
    



//
//  LocalNotificationManager.swift
//  Breeze
//
//  Created by Malou Mendoza on 7/10/21.
//


import Foundation
import UserNotifications

struct LocalNotification {
    var id: String
    var title: String
    var subTitle: String
    
}

class LocalNotificationManager {
    
    var notifications = [LocalNotification]()
    
    func requestPermission() -> Void {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .alert]) { granted, error in
            if granted == true && error == nil {
            let userInfo = [String:String]()
                self.scheduleNotifications(timeInterval: 3, userInfo:userInfo )
                // We have permission!
            }
            
        }
        
    }
    
    
    func addNotification(title: String, subTitle:String) -> Void {
        notifications.append(LocalNotification(id: UUID().uuidString, title: title, subTitle: subTitle))
        
    }
    
    func cancelNotifications() -> Void {
        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
    }
    
    func scheduleNotifications(timeInterval:TimeInterval,userInfo:[String:String]) -> Void {       UNUserNotificationCenter.current().removeAllDeliveredNotifications()
        
        for notification in notifications {
            
            let content = UNMutableNotificationContent()
            content.title = notification.title
            content.body = notification.subTitle
            content.userInfo = userInfo
            content.sound = .default
            let trigger = UNTimeIntervalNotificationTrigger(timeInterval: timeInterval, repeats: false)
            let request = UNNotificationRequest(identifier: notification.id, content: content, trigger: trigger)
            UNUserNotificationCenter.current().add(request) {
                error in
                guard error == nil else { return }
                print("Scheduling notification with id: \(notification.id)")
            }
        }
        
    }
    
}



//
//  DemoConfig.swift
//  Breeze
//
//  Created by Zhou Hao on 26/7/21.
//

import Foundation

struct DemoWayPoint: Codable {
    let name: String
    let latitude: Double
    let longitude: Double
    let id: String
}

struct DemoConfig: Codable {
    let start: DemoWayPoint
    let destination: DemoWayPoint
    let waypoints: [DemoWayPoint]    
}

//
//  DemoAPIManager.swift
//  Breeze
//
//  Created by VishnuKanth on 05/10/21.
//

import Foundation
import UIKit

struct DemoCarPark: DemoCarParkServiceProtocol {
    
    static let shared = DemoCarPark()
    
    let postSession = URLSession(configuration: .default)
    
    func getDemoCarParkDetails(_ completion: @escaping (Result<Json4Swift_Base>) -> ()) {
        let resource = Resource<Json4Swift_Base>(url: Configuration.carParkAvailability, parameters:parameters, body: nil, method: .get)
        postSession.load(resource, completion: completion)
    }
}

protocol DemoCarParkServiceProtocol {
    func getDemoCarParkDetails(_ completion: @escaping (Result<Json4Swift_Base>) -> ())
}


//class DemoAPIManager: NSObject {
//    
//    
//    static let sharedInstance = DemoAPIManager()
//    var userName = ""
//    var carParkCapacity = ""
//    let notifyServerURL = "http://35.247.181.204/rest/en/voicebot/receive"
//    var notifyJSON = [String : [String : Any]]()
//    
//    var isNotified = false
//    
//
//    func getAvailableCarParkInfo(notifyJSON:[String : [String : Any]],onSuccess: @escaping(String) -> Void, onFailure: @escaping(Error) -> Void){
//        let request: NSMutableURLRequest = NSMutableURLRequest(url: NSURL(string: self.notifyServerURL)! as URL)
//        request.httpMethod = "POST"
//        do {
//            let jsonData = try JSONSerialization.data(withJSONObject: notifyJSON, options: .prettyPrinted)
//            request.httpBody = jsonData
//
//        } catch let error as NSError {
//            print(error)
//        }
//
//        //HTTP Headers
//        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
//        request.addValue("application/json", forHTTPHeaderField: "Accept")
//        let session = URLSession.shared
//        let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
//            if(error != nil){
//                onFailure(error!)
//            } else{
//
//                if let httpResponse = response as? HTTPURLResponse {
//                    
//                    if(httpResponse.statusCode == 200)
//                    {
//                        do {
//                            if let result = try? JSONDecoder().decode(Json4Swift_Base.self, from: data!) {
//                                onSuccess((result.data?.lotsAvailable)!)
//                            }
//                            
//                        } catch let error  {
//                           print(error)
//                           // or display a dialog
//                        }
//                    }
//                    else
//                    {
//                        onSuccess("0")
//                        /*do {
//                           let result = try JSON(data: data!)
//                            onFailure(result as! Error)
//                        } catch {
//                           print(error)
//
//                           // or display a dialog
//                        }*/
//                    }
//                }
//                
//            }
//        })
//        task.resume()
//    }
//    
//}

//
//  Prefs.swift
//  Breeze
//
//  Created by Santoso Pham on 12/10/2022.
//

import Foundation
import SwiftyBeaver

enum PrefsKey: String {
    case enableAudio = "enableAudio"
    case seenCustomMapIDs = "seenCustomMapIDs"
    case walkathonContentId = "walkathonContentId"
    case launchCount = "launchCount"
    case valueShowNudge = "valueShowNudge"
    case thresholdDays = "ThresholdDays"
    case thresholdDaysNewUser = "ThresholdDaysNewUser"
    case shareMapCollectionToken = "shareMapCollectionToken"
    case shareMapLocationToken = "shareMapLocationToken"
    case didLaunchAppFirstTime = "didLaunchAppFirstTime"
    case didShowNudge = "didShowNudge"
    case latPoi = "latPoi"
    case longPoi = "longPoi"
    case latSavePlace = "latSavePlace"
    case longSavePlace = "longSavePlace"
    case isOnboardingShown = "isOnboardingShown"
    
    //  Store userID for guest mode only
    case userId = "breeze.ncs.userId"
    case isGuestMode = "breeze.ncs.isGuestMode"
    case isSuccessSignupAsGuestUser = "breeze.ncs.isSuccessSignupAsGuestUser"
    
    //  Login with phone number
    case phoneNumber = "breeze.ncs.phoneNumber"
    
    // Other three values username, triplog email, car IU number will be stored locally and along with user sub identifier
    case username = "breeze.ncs.username"
    case triplogEmail = "breeze.ncs.triplogEmail"
    case carplateNumber = "breeze.ncs.carplateNumber"
    case iuNumber = "breeze.ncs.iuNumber"
    
    case isAppDidLaunch = "isAppDidLaunch"
    case didLaunchAppAppearNudge = "didLaunchAppAppearNudge"
    case didShowObuInstallation = "didShowObuInstallation"
    
    case pairedObuList = "pairedObuList"
    case obuMode = "obuMode"
    case obuPaymentMode = "obuPaymentMode"
    case obuCardBalance = "obuCardBalance"
    case obuConnectMethod = "obuConnectMethod"
    case autoConnectOBU = "autoConnectOBU"
    case vehicleNumber = "vehicleNumber"
    
    case shouldLaunchCruiseWhenOverSpeed = "shouldLaunchCruiseWhenOverSpeed"
    
    case userProfilePreferences = "userProfilePreferences"
}

public class Prefs {
    
    public static var shared = Prefs()
    
    private init() {
        //  default initializer
        
        //  By default navigation audio is enable
        if UserDefaults.standard.object(forKey: PrefsKey.enableAudio.rawValue) == nil {
            UserDefaults.standard.set(true, forKey: PrefsKey.enableAudio.rawValue)
        }
    }
        
    func setValue(_ value: Any?, forkey: PrefsKey) {
        UserDefaults.standard.set(value, forKey: forkey.rawValue)
    }
    
    func getBoolValue(_ key: PrefsKey) -> Bool {
        let retValue = UserDefaults.standard.bool(forKey: key.rawValue)
        return retValue
    }
    
    func getIntValue(_ key: PrefsKey) -> Int {
        let retValue = UserDefaults.standard.integer(forKey: key.rawValue)
        return retValue
    }
    
    func getStringValue(_ key: PrefsKey) -> String {
        return UserDefaults.standard.string(forKey: key.rawValue) ?? ""
    }
    
    func getSeenCustomMapIds(_ key: PrefsKey) -> [Int] {
        return (UserDefaults.standard.array(forKey: key.rawValue) as? [Int]) ?? []
    }
    
    func removeValueForKey(_ key: PrefsKey) {
        UserDefaults.standard.removeObject(forKey: key.rawValue)
    }
    
    func setValue(_ value: Any?, forkey: String) {
        UserDefaults.standard.set(value, forKey: forkey)
    }
    
    func getIntValue(_ key: String) -> Int{
        UserDefaults.standard.integer(forKey: key)
    }
    
    func getLaunchCountKey(_ key: PrefsKey) -> String {
        return UserDefaults.standard.string(forKey: key.rawValue) ?? ""
    }
}

extension Prefs {
    func setValueWithSub(_ subID: String, key: PrefsKey, value: String) {
        let generatedKey = "\(subID)_\(key.rawValue)"
        UserDefaults.standard.set(value, forKey: generatedKey)
    }
    
    func getValueWithSub(_ subID: String, key: PrefsKey) -> String {
        let generatedKey = "\(subID)_\(key.rawValue)"
        return UserDefaults.standard.string(forKey: generatedKey) ?? ""
    }
    
    func setBoolValueWithSub(_ subID: String, key: PrefsKey, value: Bool) {
        let generatedKey = "\(subID)_\(key.rawValue)"
        UserDefaults.standard.set(value, forKey: generatedKey)
    }
    
    func getBoolValueWithSub(_ subID: String, key: PrefsKey) -> Bool {
        let generatedKey = "\(subID)_\(key.rawValue)"
        return UserDefaults.standard.bool(forKey: generatedKey) ?? false
    }
    
    func deleteValueWithSub(_ subID: String, key: PrefsKey) {
        let generatedKey = "\(subID)_\(key.rawValue)"
        UserDefaults.standard.removeObject(forKey: generatedKey)
    }
    
    func cleanDataWithSub(_ subID: String) {
        deleteValueWithSub(subID, key: .username)
        deleteValueWithSub(subID, key: .triplogEmail)
        deleteValueWithSub(subID, key: .carplateNumber)
        deleteValueWithSub(subID, key: .iuNumber)
    }
    
    func cleanAllGuestData(_ subID: String) {
        removeValueForKey(.userId)
        removeValueForKey(.isGuestMode)
        cleanDataWithSub(subID)
    }
}

extension Prefs {
    func saveUserPreferenceWithSub(_ subID: String, preference: NSDictionary) {
        let generatedKey = "\(subID)_\(PrefsKey.userProfilePreferences.rawValue)"
        do {
            let prefsData = try NSKeyedArchiver.archivedData(withRootObject: preference, requiringSecureCoding: false)
            UserDefaults.standard.setValue(prefsData, forKey: generatedKey)
        } catch (let error) {
            print("error: \(error.localizedDescription)")
        }
        
    }
    
    func loadUserPreferenceWithSub(_ subID: String) -> NSDictionary? {
        let generatedKey = "\(subID)_\(PrefsKey.userProfilePreferences.rawValue)"
        
        do {
            if let prefsData = UserDefaults.standard.data(forKey: generatedKey) {
                if let preference = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(prefsData) as? NSDictionary {
                    print("preference: \(preference)")
                    return preference
                }
            }
        } catch (let error) {
            print("error: \(error.localizedDescription)")
        }
        return nil
    }
}

extension Prefs {
    
    func setAutoConnectObu(_ subID: String, isAuto: Bool) {
        let generatedKey = "\(subID)_\(PrefsKey.autoConnectOBU.rawValue)"
        SwiftyBeaver.debug("setAutoConnectObu: \(isAuto)")
        UserDefaults.standard.setValue(isAuto, forKey: generatedKey)
    }
    
    func isAutoConnectObu(_ subID: String) -> Bool {
        let generatedKey = "\(subID)_\(PrefsKey.autoConnectOBU.rawValue)"
        let retValue = UserDefaults.standard.bool(forKey: generatedKey)
        SwiftyBeaver.debug("isAutoConnectObu: \(retValue)")
        return retValue
    }
    
    func getLastPairedObu(_ subID: String, key: PrefsKey) -> PairedObuModel? {
        //  Get current list
        let obuList = getObuList(subID, key: key)
        
        let lastObu = obuList.first { model in
            return (model.isLastPairedObu == true)
        }
        
        return lastObu
    }
    
    func addObu(_ subID: String, key: PrefsKey, obuModel: PairedObuModel) {
        //  Get current list
        var obuList = getObuList(subID, key: key)
        
        //  if obu is exising in paired list then remove it
        let index = obuList.firstIndex { model in
            return (model.name == obuModel.name /* && model.vehicleNumber == obuModel.vehicleNumber*/)
        }
        if let foundIndex = index {
            obuList.remove(at: [foundIndex])
            //  Insert object at found index
            obuList.insert(obuModel, at: foundIndex)
        } else {
            //  Add new obu to current list
            obuList.append(obuModel)
        }
        
        //  Update last obu
        var updateList: [PairedObuModel] = []
        for obu in obuList {
            updateList.append(PairedObuModel(name: obu.name, vehicleNumber: obu.vehicleNumber, isLastPairedObu: obu.name == obuModel.name /*&& obu.vehicleNumber == obuModel.vehicleNumber*/))
        }
        
        //  Then save obu list
        saveObuListWithSub(subID, key: key, obus: updateList)
        
    }
    
    func deleteObu(_ subID: String, key: PrefsKey, obuModel: PairedObuModel) {        
        //  Get current list
        var obuList = getObuList(subID, key: key)
        
        //  if obu is exising in paired list then remove it
        let index = obuList.firstIndex { model in
            return (model.name == obuModel.name /* && model.vehicleNumber == obuModel.vehicleNumber*/)
        }
        if let foundIndex = index {
            obuList.remove(at: [foundIndex])
        }
        
        //  Then save obu list
        saveObuListWithSub(subID, key: key, obus: obuList)
    }
    
    func getObuList(_ subID: String, key: PrefsKey) -> [PairedObuModel] {
        let generatedKey = "\(subID)_\(key.rawValue)"

        if let encodedData = UserDefaults.standard.data(forKey: generatedKey) {
            let decoder = JSONDecoder()
            if let obuList = try? decoder.decode([PairedObuModel].self, from: encodedData) {
                return obuList
            }
        }
        return []
    }
    
    @discardableResult private func saveObuListWithSub(_ subID: String, key: PrefsKey, obus: [PairedObuModel]) -> Bool {
        let generatedKey = "\(subID)_\(key.rawValue)"
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(obus) {
            UserDefaults.standard.setValue(encoded, forKey: generatedKey)
            return true
        }
        return false
    }
}

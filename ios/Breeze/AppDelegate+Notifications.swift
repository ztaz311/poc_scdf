//
//  AppDelegate+Notifications.swift
//  Breeze
//
//  Created by TuyenLX on 23/03/2022.
//

import Foundation
import UIKit
import SwiftyBeaver

extension AppDelegate {
    
    func getContentVC() -> ContentVC? {
        for window in UIApplication.shared.windows {
            if let nav = window.rootViewController as? UINavigationController {
                return nav.viewControllers.reversed().filter { vc in
                   return vc is ContentVC
                }.first as? ContentVC
            }
        }
        return nil
    }
    
    func getMapLandingVCInTop() -> MapLandingVC? {
        for window in UIApplication.shared.windows {
            if let nav = window.rootViewController as? UINavigationController {
                return nav.viewControllers.reversed().filter { vc in
                   return vc is MapLandingVC
                }.first as? MapLandingVC
            }
        }
        return nil
    }
    
    var mapLandingVC: MapLandingVC? {
        return getMapLandingVCInTop()
    }
    
    var baseNavigationController: BaseNavigationController? {
        for window in UIApplication.shared.windows {
            if let nav = window.rootViewController as? BaseNavigationController {
                return nav
            }
        }
        return nil
    }
    
    var isMaplandingInTop: Bool {
        if let navi = baseNavigationController,
            let vc = navi.viewControllers.last {
            return vc is MapLandingVC
        }
        return false
    }
    
    private func processNotification(name: String, notification: Notification) {
        switch name {
        case Values.NotificationOpenTripLog:
            self.processTripLogNotification(notification: notification)
        case Values.NotificationOpenMore:
            self.processMoreNotification(notification: notification)
        case Values.NotificationOpenSettings:
            self.processOpenSettingNotification(notification: notification)
        case Values.searchLocationReceived:
            self.processLocationReceivedNotification(notification: notification)
        case Values.NotificationOpenRoutePlanning:
            self.processRouterPlaningNotification(notification: notification)
        case Values.NotificationAdjustRecenter:
            self.processAdjustRecenterNotification(notification: notification)
        case Values.NotificationChangeStatusBar:
            self.processStaturBarUI(notification: notification)
        case Values.NotificationCloseOnboardingTutorial:
            self.processCloseTutorial(notification: notification)
        case Values.handleProfileSelection:
            self.handleProfileSelection(notification: notification)
        case Values.NotificationRefreshAmenities:
            self.handleRefreshAmenities(notification: notification)
        default:
            break
        }
    }
    
    private func processCloseTutorial(notification: Notification) {
        mapLandingVC?.processCloseTutorial(notification: notification)
    }
    
    private func processStaturBarUI(notification: Notification) {
        let darkmode = notification.userInfo![Values.ChangeStatusBarKey] as? Bool ?? false
        
        mapLandingVC?.toDisplayNudge = !darkmode
        
        baseNavigationController?.processStatusbarNotification(showDarkmode: darkmode)
    }
    
    private func handleProfileSelection(notification: Notification){
        
        mapLandingVC?.processProfileSelection(notification: notification)
    }
    
    private func handleRefreshAmenities(notification: Notification){
        
        mapLandingVC?.handleRefreshAmenities(notification: notification)
    }
    
    private func processTripLogNotification(notification: Notification) {
        let open = notification.userInfo![Values.OpenTripLogKey] as! Bool
        let navigationParams = notification.userInfo?[Values.tripLogNavigationParams] as? [String: Any] ?? [:]
        if open {
            mapLandingVC?.openTripLog(navigationParams: navigationParams)
        } else {
            mapLandingVC?.closeTripLog()
        }
    }
    
    private func processMoreNotification(notification: Notification) {
        let open = notification.userInfo![Values.OpenMoreKey] as! Bool
        if open {
            mapLandingVC?.openMore()
        } else {
            mapLandingVC?.closeMore()
        }
    }
    
    private func processOpenSettingNotification(notification: Notification) {
        let open = notification.userInfo![Values.settingsKey] as! Bool
        if open {
            mapLandingVC?.openMore()
        }
    }
    
    private func processLocationReceivedNotification(notification: Notification) {
        mapLandingVC?.processLocationReceived(notification: notification)
    }
    
    private func processRouterPlaningNotification(notification: Notification) {
        mapLandingVC?.processOpenRouterPlaning(notification: notification)
    }
    
    private func processAdjustRecenterNotification(notification: Notification) {
        mapLandingVC?.processRecenter(notification: notification)
    }
    
    func registerNotifications() {
        let namesNotifications = [Values.NotificationOpenTripLog,
                                  Values.NotificationOpenMore,
                                  Values.NotificationOpenSettings,
                                  Values.searchLocationReceived,
                                  Values.NotificationOpenRoutePlanning,
                                  Values.NotificationAdjustRecenter,
                                  Values.NotificationChangeStatusBar,
                                  Values.NotificationCloseOnboardingTutorial,
                                  Values.handleProfileSelection,
                                  Values.NotificationRefreshAmenities]
        namesNotifications.forEach { name in
            NotificationCenter.default.publisher(for: Notification.Name(name), object: nil)
                .receive(on: DispatchQueue.main)
                .sink { [weak self] notification in
                    guard let self = self else { return }
                    self.processNotification(name: name, notification: notification)
                }.store(in: &disposables)
        }
    }
    
    func confirmLoginAsGuestUser () {
        
        //  If guest user is already existing then we dont need to show this popup again
        if AWSAuth.sharedInstance.guestUserExisting() {
            self.processLoginAsGuestUser()
        } else {
            let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            
            if let landingVC = self.baseNavigationController?.viewControllers.last as? LandingViewController, let confirmVC = storyboard.instantiateViewController(withIdentifier: "ConfirmSigninAsGuestVC") as? ConfirmSigninAsGuestVC {
                
                landingVC.addChildViewControllerWithView(childViewController: confirmVC)
                
                confirmVC.proceedAction = { [weak self] in
                    self?.processLoginAsGuestUser()
                }
            }
        }
    }
    
    func processLoginAsGuestUser() {
        if let landingVC = self.baseNavigationController?.viewControllers.last as? LandingViewController {
            landingVC.proceedLoginAsGuest()
        }
    }
    
    func processOpenScreen(screenId: CNMScreenId, navigationParams: NSDictionary?) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            switch screenId {
            case .shareCollectionScreen:
                self.showShareCollectionAndLocationScreen(navigationParams: navigationParams)
            case .collectionDetail:
                self.showCollectionDetailScreen(navigationParams: navigationParams)
            case .signin, .createAccount:
                self.showAccountScreen(screenId: screenId)
            case .obuDisplay:
                self.showObuDisplay()
            case .obuStepConnect:
                self.obuStepConnect()
            default:
                break
            }
        }
    }
    
    private func showObuDisplay() {
        mapLandingVC?.showOBUDisplay()
    }
    
    private func obuStepConnect() {
        mapLandingVC?.obuStepConnectScreen()
    }
    
    private func showAccountScreen(screenId: CNMScreenId) {
        if let landingVC = self.baseNavigationController?.viewControllers.last as? LandingViewController {
            switch screenId {
            case .signin:
                landingVC.pushToSignIn()
            case .createAccount:
                landingVC.pushToSignUp()
            default:
                break
            }
        }
    }
    
    private func showShareCollectionAndLocationScreen(navigationParams: NSDictionary?) {
        
        guard let rnTag = navigationParams?[Values.TORNSCREEN] as? String,
              let mode = navigationParams?[Values.SHARE_MODE] as? String,
              let token = navigationParams?[Values.SHARING_TOKEN] as? String else { return }
        
        let vc = appCoordinate.makeSaveCollectionScreen(rntag: rnTag)
        vc.sharingMode = SharingMode(rawValue: mode)
        vc.token = token
        
        if let locationData = navigationParams?[Values.SHARE_LOCATION_DATA] as? SaveLocationDataModel {
            vc.locationData = locationData
        }
        if let name = navigationParams?[Values.COLLECTION_NAME] as? String {
            vc.collectionName = name
        }
        if let code = navigationParams?[Values.COLLECTION_CODE] as? String {
            vc.screenCode = code
        }
        if let collectionDesc = navigationParams?[Values.COLLECTION_DESC] as? String {
            vc.collectionDesc = collectionDesc
        }
              
        
        baseNavigationController?.pushViewController(vc, animated: true)
    }
    
    private func showCollectionDetailScreen(navigationParams: NSDictionary?) {
        guard let collectionid = navigationParams?[Values.COLLECTIONID] as? Int,
              let rnTag = navigationParams?[Values.TORNSCREEN] as? String else { return }
        
        var collectionName: String = ""
        if let name = navigationParams?[Values.COLLECTION_NAME] as? String {
            collectionName = name
        }
        let vc = appCoordinate.makeCollectionDetail(collectionID: collectionid, rntag: rnTag, collectionName: collectionName)
        if let code = navigationParams?[Values.COLLECTION_CODE] as? String {
            vc.screenCode = code
        }
        if let collectionDesc = navigationParams?[Values.COLLECTION_DESC] as? String {
            vc.collectionDesc = collectionDesc
        }
        if let isSaving = navigationParams?[Values.COLLECTION_SAVING] as? Bool {
            vc.isSavingCollection = isSaving
        }
        
        if let errorCode = navigationParams?[Values.COLLECTION_ERROR_CODE] as? String, !errorCode.isEmpty {
            vc.errorMessage = (navigationParams?[Values.COLLECTION_ERROR_MESSAGE] as? String) ?? ""
        }
        
        if let bookmarkId = navigationParams?[Values.BOOKMARK_ID] as? Int  {
            vc.bookmarkId = bookmarkId
        }
        
        baseNavigationController?.pushViewController(vc, animated: true)
    }
    
    internal func handleAppUpdateNotification(aps: [String: AnyObject]) {
        if let appDownloadLink = aps["link_url"] as? String {
            if let url = URL(string: appDownloadLink){
                SwiftyBeaver.debug("Open url:\(appDownloadLink)")
                UIApplication.shared.open(url, completionHandler: { (success) in
                    SwiftyBeaver.debug("url opened: \(success)") // Prints true
                })
            }
        }
    }
    
}

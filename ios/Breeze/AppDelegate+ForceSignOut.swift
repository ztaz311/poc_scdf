//
//  AppDelegate+ForceSignOut.swift
//  Breeze
//
//  Created by Santoso Pham on 30/9/21.
//

import Foundation
import UIKit

extension AppDelegate {    
    func forceSignoutUser(_ toScreen: CNMScreenId? = nil) {
        DispatchQueue.main.async {
            //  Handle pop to root view controller
            
            if let nav = appDelegate().baseNavigationController {
                _ = nav.popToRootViewController(animated: false)
                
                //  Push to new login screen
                let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                if let vc = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as? LandingViewController {
                    vc.toScreen = toScreen
                    var vcs = nav.viewControllers
                    vcs.append(vc)
                    
                    if let screen = toScreen {
                        vc.resetRNInstance()
                        switch screen {
                        case .signin:
                            let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                            let signInVC: UIViewController = storyboard.instantiateViewController(withIdentifier: "NewSigninGuestModeViewController") as UIViewController
                            vcs.append(signInVC)
                        case .createAccount:
                            let storyboard: UIStoryboard = UIStoryboard(name: "Web", bundle: nil)
                            let webVC: WebVC = storyboard.instantiateViewController(withIdentifier: "WebVC") as! WebVC
                            webVC.isAccountCreation = true
                            webVC.urlString = Configuration.termsOfUseURL
                            vcs.append(webVC)
                        default:
                            //  Do nothing here
                            break
                        }
                    }
                    nav.setViewControllers(vcs, animated: true)
                }
            }
        }
    }
}

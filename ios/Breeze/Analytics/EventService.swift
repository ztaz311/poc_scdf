//
//  EventService.swift
//  Breeze
//
//  Created by VishnuKanth on 13/06/21.
//

import Foundation
import Firebase
import Amplify
import SwiftyBeaver

final class EventService:EventServiceProtocol{
    
    func sendAmplifyEvents(eventName: String, eventParameters: [String : Any]) {
        
        let analyticsParameters = eventParameters as? AnalyticsProperties
        let event = BasicAnalyticsEvent(name: eventName, properties: analyticsParameters)
        Amplify.Analytics.record(event: event)
        
       
    }
    
    func sendEvents(eventName: String, eventParameters: [String : Any]) {
        
        Analytics.logEvent(eventName, parameters: eventParameters)
        SwiftyBeaver.debug("OBU - track event: \(eventName) \(eventParameters)")
        
    }
    
    
    
}

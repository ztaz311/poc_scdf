//
//  AnalyticsManager.swift
//  Breeze
//
//  Created by VishnuKanth on 16/06/21.
//
import Foundation
import Firebase
import Amplify
import SwiftyBeaver

protocol AnalyticsEventsLoggerProtocol {
    //func setUserProperties(user: UserProfileModel)
    func trackEvent(event: EventProtocol)
}

// Firebase
class FirebaseEventLogger: AnalyticsEventsLoggerProtocol {
    /*func setUserProperties(user: UserProfileModel) {
        // set user properties
    }*/

    func trackEvent(event: EventProtocol) {
        EventService().sendEvents(eventName: event.name, eventParameters: event.params)
        // add implementation of tracking events
    }
    
}

// Amplify
class AmplifyEventLogger: AnalyticsEventsLoggerProtocol {
    /*func setUserProperties(user: UserProfileModel) {
        // set user properties
    }*/

    func trackEvent(event: EventProtocol) {
        EventService().sendAmplifyEvents(eventName: event.name, eventParameters: event.params)
        // add implementation of tracking events
    }
    
}


// Manager
protocol AnalyticsManagerProtocol {
    func setUp()
    func trackEvent(_ event: EventProtocol,
                    params: [String: Any])
}

class AnalyticsManager: AnalyticsManagerProtocol {
    static let shared = AnalyticsManager()
    var loggers: [AnalyticsEventsLoggerProtocol] = []
    var ga_session_id: Int?

    func setUp() {
        
        //  Make sure to remove all logger before add new
        loggers.removeAll()
        loggers.append(FirebaseEventLogger())
        if ga_session_id == nil {
            //  Init just once when user open app
            ga_session_id = Int(Date().currentTimeInMiliseconds())
        }
        // disable analytics for all in amplify
//        loggers.append(AmplifyEventLogger())
     // Setup all the Analytics SDK
    }
    
    func trackEvent(_ event: EventProtocol,
                    params: [String: Any] = [:]) {
        // add extra params to event
        var params = event.params
        params["ga_session_id"] = ga_session_id
        let modifiedEvent = Event(name: event.name, params: params)
        
        // hit event from all tracking SDK's
        loggers.forEach({ logger in
            logger.trackEvent(event: modifiedEvent)
        })
    }
    
    func logScreenView(screenName: String) {

        trackEvent(Event(name: AnalyticsEventScreenView, params: [AnalyticsParameterScreenName:screenName]))
        
        trackEvent(Event(name: EventName.pageViewEvent, params: [AnalyticsParameterScreenName:screenName]))
    }
    
    func sendLoginEvent(method: String){
        
        trackEvent(Event(name: AnalyticsEventLogin, params: [AnalyticsParameterMethod:method]))
            
    }
    
    func logClickEvent(eventValue: String, screenName: String, extraData: NSDictionary? = nil) {
        
        var params: [String: Any] = [EventName.screenNameEvent:screenName,EventName.eventValue:eventValue]
        if let extraParams = extraData {
            for (key, value) in extraParams {
                if let paramKey = key as? String {
                    params[paramKey] = value
                }
            }
        }
        
        trackEvent(Event(name: EventName.userClickEvent, params: params))
            
    }
    
    func logClickEventSelectedObu(eventValue: String, screenName: String, obuName: String) {
        trackEvent(Event(name: EventName.userClickEvent, params: [EventName.screenNameEvent:screenName,EventName.eventValue:eventValue,EventName.obuNameEvent:obuName]))
    }
    
    func logOpenPopupEventV2(popupName: String,  screenName: String, extraData: NSDictionary? = nil) {
        
        var params: [String: Any] = [EventName.screenNameEvent: screenName, ParameterName.popup_name: popupName, EventName.eventValue: ParameterName.popupOpen]
        if let extraParams = extraData {
            for (key, value) in extraParams {
                if let paramKey = key as? String {
                    params[paramKey] = value
                }
            }
        }
        
        trackEvent(Event(name: EventName.userPopupEvent, params: params))
    }
    
    
    func logOpenPopupEventV2CarPlay(popupName: String,  screenName: String, extraData: NSDictionary? = nil) {
        
        var params: [String: Any] = [EventName.screenNameEvent: screenName, ParameterName.popup_name: popupName, EventName.eventValue: ParameterName.dhuPopupOpen]
        if let extraParams = extraData {
            for (key, value) in extraParams {
                if let paramKey = key as? String {
                    params[paramKey] = value
                }
            }
        }
        
        trackEvent(Event(name: EventName.userPopupEvent, params: params))
    }
    
    func logClosePopupEventV2Carplay(popupName: String, screenName: String) {
        trackEvent(Event(name: EventName.userPopupEvent, params: [EventName.screenNameEvent: screenName, ParameterName.popup_name: popupName, EventName.eventValue: ParameterName.dhuPopupClose]))
    }
    
    
    func logClosePopupEventV2(popupName: String, screenName: String) {
        trackEvent(Event(name: EventName.userPopupEvent, params: [EventName.screenNameEvent: screenName, ParameterName.popup_name: popupName, EventName.eventValue: ParameterName.popupClose]))
    }
    
    func logSystemEvent(eventValue: String, screenName: String, eventMessage: String = "") {
        
        if !eventMessage.isEmpty {
            trackEvent(Event(name: EventName.systemEvent, params: [EventName.screenNameEvent:screenName,EventName.eventValue:eventValue,EventName.eventMessage:eventMessage]))
            SwiftyBeaver.debug("OBU event Message: \(eventMessage)")
        } else {
            trackEvent(Event(name: EventName.systemEvent, params: [EventName.screenNameEvent:screenName,EventName.eventValue:eventValue]))
        }
    }

    func logToggleEvent(eventValue: String, screenName: String ) {
        
        trackEvent(Event(name: EventName.userToggleEvent, params: [EventName.screenNameEvent:screenName,EventName.eventValue:eventValue]))
            
    }

    func logSwipeEvent(eventValue: String, screenName: String ) {
        
        trackEvent(Event(name: EventName.userSwipeEvent, params: [EventName.screenNameEvent:screenName,EventName.eventValue:eventValue]))
            
    }
    
    func logMapInteraction(eventValue: String, screenName: String) {
        trackEvent(Event(name: EventName.mapInteraction, params: [EventName.screenNameEvent:screenName,EventName.eventValue:eventValue]))
    }

    func logEditEvent(eventValue: String, screenName: String ) {
        
        trackEvent(Event(name: EventName.userEditEvent, params: [EventName.screenNameEvent:screenName,EventName.eventValue:eventValue]))
    }
    
    func logOpenPopupEvent(popupName: String,  screenName: String) {
        trackEvent(Event(name: EventName.userPopupEvent, params: [EventName.screenNameEvent:screenName,ParameterName.popup_name:popupName,EventName.eventValue:ParameterName.open]))
    }

    func logClosePopupEvent(popupName: String,  screenName: String) {
        
        trackEvent(Event(name: EventName.userPopupEvent, params: [EventName.screenNameEvent:screenName,ParameterName.popup_name:popupName,EventName.eventValue:ParameterName.close]))
            
    }
    
    func logPopUpActionEvent(popupName: String, eventName: String, screenName: String) {
        
        trackEvent(Event(name: EventName.userPopupEvent, params: [EventName.screenNameEvent:screenName,ParameterName.popup_name:popupName,EventName.eventValue:eventName]))
            
    }
    
    func logPopUpEvent(popupName: String, eventName: String) {
        
        trackEvent(Event(name: EventName.userPopupEvent, params: [ParameterName.popup_name:popupName,EventName.eventValue:eventName]))
            
    }
    
    func logEventUserClickOnPOI(eventValue: String, screenName: String, amenityId: String, layerCode: String) {
        
        let params = [EventName.screenNameEvent:screenName,EventName.eventValue:eventValue,ParameterName.amenityId: amenityId,ParameterName.layerCode:layerCode]
        trackEvent(Event(name: EventName.userClickEvent, params: params))
        
        AnalyticsService().postPOI(analyticsEvent: AnalyticsService.POIEventData(event_name: EventName.userClickEvent, event_value: eventValue, screen_name: screenName, event_timestamp: Date().currentTimeInMiliseconds(), amenity_id: amenityId, layer_code: layerCode)) { result in
            switch result {
            case .failure(let error):
                SwiftyBeaver.error("Failed to submit analytics event: \(error.localizedDescription)")
            case .success(_):
                print("success")
            }
        }
    }
    
    func logEvent(eventName:String, eventValue: String, screenName: String, saveToLocal: Bool = false, amenityId: String, layerCode: String, amenityType: String, extraData: NSDictionary? = nil) {
        
        var params: [String: Any] = [EventName.screenNameEvent:screenName, EventName.eventValue:eventValue]
        if let extraParams = extraData {
            for (key, value) in extraParams {
                if let paramKey = key as? String {
                    params[paramKey] = value
                }
            }
        }
        trackEvent(Event(name: eventName, params: params))
        
        if saveToLocal {
            AnalyticsService().post(analyticsEvent: AnalyticsService.EventData(event_name: eventName, event_value: eventValue, screen_name: screenName, event_timestamp: Date().currentTimeInMiliseconds(), amenity_id: amenityId, layer_code: layerCode, amenity_type: amenityType)) { result in
                switch result {
                case .failure(let error):
                    SwiftyBeaver.error("Failed to submit analytics event: \(error.localizedDescription)")
                case .success(_):
                    print("success")
                }
            }
        }
    }
    
    /**
        sendLocationDate
        - Latitude
        - Longitude
        - LocationName
     */
    
    func sendLocationData(eventValue:String,screenName:String,locationName:String,latitude:String,longitude:String){
        
        trackEvent(Event(name: EventName.locationData, params: [EventName.screenNameEvent:screenName,ParameterName.locationName:locationName,ParameterName.latitude:latitude,ParameterName.longitude:longitude,EventName.eventValue:eventValue]))
    }
    
    
    func sendUserSelection(eventValue: String, screenName: String, selectionValue: String){
        trackEvent(Event(name: EventName.userSelection, params: [EventName.screenNameEvent:screenName,EventName.eventValue:eventValue, ParameterName.selection_value:selectionValue]))
    }
    
    /**
        Log statistics data, including:
        - Battery level
        - Data sent
        - Data received
     */
    func logStatistics(data: StatisticsData, screenName: String, description: String) {
        trackEvent(Event(name: EventName.statisticsData,
                         params: [EventName.screenNameEvent:screenName,
                                  ParameterName.batteryLevel:data.batteryLevel,
                                  ParameterName.dataSent: data.dataSent,
                                  ParameterName.dataReceived: data.dataReceived,
                                  ParameterName.time: Date().iso8601withFractionalSeconds,
                                  EventName.eventValue: description
                         ]))
    }
    
    func logNotificationStatisTics(eventName: String, data: Any, screenName: String, eventValue: String) {
        trackEvent(Event(name: EventName.statistics),
                   params: [EventName.screenNameEvent:screenName,eventName: data, EventName.eventValue: eventValue])
    }
    
    
    func logAddImageEvent(eventValue: String, screenName: String ) {
        
        trackEvent(Event(name: EventName.addImageEvent, params: [EventName.screenNameEvent:screenName,EventName.eventValue:eventValue]))
    }
    
    func logUpdateImageEvent(eventValue: String, screenName: String ) {
        
        trackEvent(Event(name: EventName.updateImageEvent, params: [EventName.screenNameEvent:screenName,EventName.eventValue:eventValue]))
    }
    
    func logCompletedNavigationEvent(eventValue: String, screenName: String ) {
        
        trackEvent(Event(name: EventName.completedNavigationEvent, params: [EventName.screenNameEvent:screenName,EventName.eventValue:eventValue]))
            
    }
    
    func setUserId(cognitoUserName:String)
    {
        //Firbase
        Analytics.setUserID(cognitoUserName)
        
        #if STAGING
        //Amplify
        //Amplify.Analytics.identifyUser(cognitoUserName)
        #endif
    }

    
}

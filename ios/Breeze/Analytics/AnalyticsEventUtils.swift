//
//  AnalyticsEventUtils.swift
//  Breeze
//
//  Created by VishnuKanth on 13/06/21.
//

import Foundation
import Firebase

struct AnalyticsEventNames {
    
    static let userClickEvent = "user_click"
    static let userPopupEvent = "user_popup"
    static let userEditEvent = "user_edit"
    static let screenNameEvent = "screen_name"
    static let eventValueEvent = "event_value"
    static let signin = "signin"
    static let signin_method = "signin_method"
    static let popup_name = "popup_name"
}

struct AnalyticsParameterType{
    
    struct commonParameters {
        
        static let email   = "email"
        static let password = "password"
        static let next = "next"
        static let newPwd = "new_password"
        static let otp = "otp"
        static let resend = "resend"
        static let open = "open"
        static let close = "close"
    }
    
    struct onBoarding_signin{
        static let screenName = "onboarding_signin"
        static let screenNameFP_step1 = "onboarding_signin_forget_password_step1"
        static let screenNameFP_step2 = "onboarding_signin_forget_password_step2"
        static let screeNameFP_step3 = "onboarding_signin_forget_password_step3"
        static let popUpName = "onboarding_signin_forget_password_success"
        
        struct eventValues{
            static let signin_facebook = "facebook"
            static let signin_google  = "google"
            static let signin_apple = "apple"
            static let signin_breeze = "breeze"
            static let login = "login"
            static let forget_password = "forget_password"
        }
    }
    
    struct onBoarding_signup {
        
    }
    
    struct homePage{
        
    }
    
}

//
//  EventServiceProtocol.swift
//  Breeze
//
//  Created by VishnuKanth on 13/06/21.
//

import Foundation
import Firebase
protocol EventServiceProtocol {
    
    func sendEvents( eventName:String,eventParameters:[String:Any])
    func sendAmplifyEvents(eventName:String,eventParameters:[String:Any])
    
}

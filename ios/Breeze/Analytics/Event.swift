//
//  Event.swift
//  Breeze
//
//  Created by VishnuKanth on 16/06/21.
//

import Foundation
import Firebase
protocol EventProtocol {
    var name: String { get set }
    var params: [String: Any] { get set }
}

@propertyWrapper
struct Event: EventProtocol {
    var name: String
    var params: [String: Any] = [:]

    var wrappedValue: Event {
        return Event(name: name,
                     params: params)
    }
}


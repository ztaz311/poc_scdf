//
//  EventName+ParameterName.swift
//  Breeze
//
//  Created by VishnuKanth on 16/06/21.
//

import Foundation

enum EventName {
    static let userClickEvent = "user_click"
    static let userSwipeEvent = "user_swipe"
    static let userPopupEvent = "user_popup"
    static let userEditEvent = "user_edit"
    static let userToggleEvent = "user_toggle"
    static let userNotificationEvent = "user_notification"
    static let mapInteraction = "map_interaction"
    static let screenNameEvent = "screen_name"
    static let pageViewEvent = "page_view"
    static let eventValue = "event_value"
    static let statisticsData = "statistics_data"
    static let statistics = "statistics"
    static let addImageEvent = "add_image"
    static let updateImageEvent = "update_image"
    static let completedNavigationEvent = "completed_navigation"
    static let locationData = "location_data"
    static let userSelection =  "user_selection"
    static let appTerminate = "app_terminate"
    static let systemEvent = "system_event"
    static let eventMessage = "message"
    static let obuNameEvent = "obuName"
}

enum ParameterName {
    
    enum SettingAccountGuest {
        static let screen_view = "settings_account_guest"
        
        // user click
        enum UserClick {
            static let back = "back"
            static let editName = "edit_name"
            static let editCarDetails = "car_details_edit"
            static let editEmail = "edit_email"
            static let createAcc = "create_acc"
            static let signin = "signin"
        }
    }
    
    enum OnBoardingGuest {
        static let screen_view = "onboarding_guest_signup"
        
        // user click
        enum UserClick {
            static let guestContinueClose = "guest_continue_close"
            static let guestContinueCancel = "guest_continue_cancel"
            static let guestContinueProceed = "guest_continue_proceed"
            static let guestTnCBack = "tnc_back"
            static let guestTnCDeline = "tnc_decline"
            static let guestTnCAgree = "tnc_agree"
            static let guestUsernameInput = "username_input"
            static let guestSuccessNext = "success_next"
        }
    }
    
    enum OnBoardingCreateAccount {
        static let screen_view = "onboarding_create_acc_signup"
        
        // user click
        enum UserClick {
            
            static let tncBack = "tnc_back"
            static let tncDeline = "tnc_decline"
            static let tncAgree = "tnc_agree"
            static let createAccountContinueWoAcc = "continue_wo_acc"
            static let createAccountBack = "create_acc_back"
            static let createAccountFacebook = "create_acc_facebook"
            static let createAccountGoogle = "create_account_google"
            static let createAccountApple = "create_account_apple"
            static let createAccountUsernameInput = "username_input"
            static let usernameBack = "user_name_back"
            static let createAccountSuccessNext = "success_next"
            
        }
    }
    
    enum OnBoardingSignIn {
        static let screen_view = "onboarding_signin"
        
        // user click
        enum UserClick {
            
            static let signinBack = "signin_back"
            static let signinFacebook = "signin_facebook"
            static let signinGoogle = "signin_google"
            static let signinApple = "signin_apple"
            static let signinMobile = "signin_mobile"
            static let signinMobileBack = "signin_mobile_back"
            static let signinSuccessNext = "signin_success_next"
            static let signInContinueWoAcc = "continue_wo_acc"
            static let signInMobileNumInput = "mobile_num_input"
            
        }
    }
    
    // onboarding
    enum Onboarding {
        static let screen_view = "onboarding"
        
        // user click
        enum UserClick {
            static let create_acc = "create_acc"
            static let signin = "signin"
            static let facebook = "facebook"
            static let google = "google"
            static let apple = "apple"
            static let photomedia_permission_allow = "photomedia_permission_allow"
            static let photomedia_permission_deny = "photomedia_permission_deny"
            static let location_permission_allow = "location_permission_allow"
            static let location_permission_deny = "location_permission_deny"
        }
        
        // popup
        enum UserPopup {
            static let location_permission = "location_permission"
        }
    }
    
    enum OnboardingTNC {
        static let screen_view = "onboarding_tnc"
        
        enum UserClick {
            static let back = "back"
            static let decline = "decline"
            static let accept = "accept"
        }
    }
    
    enum SignIn {
        static let screen_view = "onboarding_signin"
        enum UserClick {
            static let back = "back"
            static let done = "done"
            static let signup = "signup"
        }
        
        enum UserEdit {
            static let mobile_num_input = "mobile_num_input"
        }
        
        enum UserPopup {
            static let welcome = "welcome"
        }
    }
    
    enum MobileSignUp {
        static let screen_view = "onboarding_mobile_signup"
        enum UserClick {
            static let back = "back"
            static let done = "done"
            static let signin = "signin"
        }
        
        enum UserEdit {
            static let mobile_num_input = "mobile_num_input"
        }
    }
    
    enum SetUserName {
        static let screen_view = "onboarding_username"
        
        enum UserClick {
            static let back = "back"
        }
        
        enum UserEdit {
            static let username_input = "username_input"
        }
        
        enum UserPopup {
            static let signup_success = "signup_success"
        }
    }
    
    enum SmsVerify {
        static let screen_view = "onboarding_sms_verify"
        enum UserClick {
            static let close = "close"
            static let resend = "resend"
        }
        
        enum UserEdit {
            static let otp_input = "otp_input"
        }
    }
    
    enum SoftwareUpdate{
        
        enum PopUpName {
            static let mandatory_update = "mandatory_update"
            static let normal_update = "normal_update"
            static let  lastest_version_updated = "lastest_version_updated"
            
        }
        
        enum UserPopUp {
            static let update_now = "update_now"
            static let later = "later"
            static let  dismiss = "dismiss"
            static let install_now = "install_now"
        }
    }
    
    enum Content {
        static let screen_view = "content"
    }
    
    enum CollectionDetail {
        
        static let screen_view = "saved_collection"
        static let my_saved_places_collection_screen = "[my_saved_places_collection]"
        static let custom_saved_places_collection = "[custom_saved_places_collection]"
        static let shortcut_collection = "[shortcut_collection]"
        
        enum UserClick {
            static let tooltip_saved_navigate_here = "[map]:tooltip_saved_navigate_here"
            static let tooltip_saved_bookmark = "[map]:tooltip_saved_bookmark"
            static let tooltip_saved_unbookmark = "[map]:tooltip_saved_unbookmark"
            static let tap_map_drop_pin = "[map]:tap_map/drop_pin"
            static let tooltip_saved_share = "[map]:tooltip_saved_share"
        }
        
        enum MapInteraction {
            static let drag = "drag_map"
            static let pinch = "pinch_map"
            static let rotate = "rotate_map"
        }
    }
    
    enum CollectionDetailStar {
        static let screen_view = "saved_collection_*"
        
        static let saved_collection_shortcut = "saved_collection_shortcut"
        static let saved_collection_my_saved_places = "saved_collection_my_saved_places"
        static let saved_collection_custom = "saved_collection_custom"
        static let saved_collection_and_location_custom = "saved_collection_and_location_custom"
        
        enum UserClick {
            static let add_home_back = "add_home_back"
            static let add_home_save = "add_home_save"
            static let add_work_back = "add_work_back"
            static let add_work_save = "add_work_save"
            static let edit_shortcut_back = "edit_shortcut_back"
            static let saved_icon_click = "saved_icon_click"
            static let navigate_here_saved = "navigate_here_saved"
            static let tap_carpark = "tap_carpark"
            static let navigate_here_parking = "navigate_here_parking"
            static let parking_calculatefee = "parking_calculatefee"
            static let parking_tooltip_see_more = "parking_tooltip_see_more"
            //TODO: TuyenLX
            static let edit_shortcut_name_clear = "edit_shortcut_name_clear"
            
        }
        
        enum LocationData {
            static let tap_carpark = "tap_carpark"
            static let map_saved_icon_click = "map_saved_icon_click"
            static let navigate_here_parking = "navigate_here_parking"
            static let navigate_here_saved = "navigate_here_saved"
        }
    }
    
    enum CustomMap {
        static let screen_view = "custom_map"
        
        enum UserClick {
            static let back = "back"
            static let recenter = "recenter"
            static let select_our_tampines_hub_map = "select_our_tampines_hub_map"
            static let select_singapore_premier_league_map = "select_singapore_premier_league_map"
            static let select_tiong_bahru_map = "select_tiong_bahru_map"
            static let select_pasar_malam_map = "select_pasar_malam_map"
            static let select_national_day_fireworks_map = "select_national_day_fireworks_map"
            static let parking_calculatefee = "parking_calculatefee"
            static let parking_tooltip_see_more = "parking_tooltip_see_more"
            static let map_tampines_hub_icon_click = "map_tampines_hub_icon_click"
            static let navigate_here_tampines_hub = "navigate_here_tampines_hub"
            static let map_singapore_premier_league_icon_click = "map_singapore_premier_league_icon_click"
            static let navigate_here_singapore_premier_league = "navigate_here_singapore_premier_league"
            static let map_tiong_bahru_icon_click = "map_tiong_bahru_icon_click"
            static let navigate_here_tiong_bahru = "navigate_here_tiong_bahru"
            static let map_pasarmalam_icon_click = "map_pasarmalam_icon_click"
            static let navigate_here_pasarmalam = "navigate_here_pasarmalam"
            static let map_fireworks_icon_click = "map_fireworks_icon_click"
            static let navigate_here_fireworks = "navigate_here_fireworks"
            static let tap_carpark = "tap_carpark"
            static let navigate_here_parking = "navigate_here_parking"
            static let tap_poi_layer = "poi_layer"
            static let tampines_green_progress_tracking_open = "tampines_green_progress_tracking_open"
        }
        
        enum UserSwipe {
            static let swipe_up = "swipe_up"
            static let swipe_down = "swipe_down"
        }
        
        enum MapInteraction {
            static let drag = "drag_map"
            static let pinch = "pinch_map"
            static let rotate = "rotate_map"
        }
        
    }
    
    enum Home {
        static let screen_view = "home"
        static let homepage_screen_view = "[homepage]"
        
        enum UserClick {
            static let calendar_permission_allow = "calendar_permission_allow"
            static let calendar_permission_deny = "calendar_permission_deny"
            static let popup_add_email_now = "popup_add_email_now"
            static let popup_add_email_later = "popup_add_email_later"
            static let search = "search"
            static let parking = "parking"
            static let recenter = "recenter"
            static let report_issue = "report_issue"
            static let start_cruise = "start_cruise"
            static let add_fav_destination = "add_fav_destination"
            static let fav_destination_select = "fav_destination_select"
            static let view_all_favourites = "view_all_favourites"
            static let recent_destination = "recent_destination"
            static let add_arrival = "add_arrival"
            static let fav_arrival = "fav_arrival"
            static let view_all_arrival = "view_all_arrival"
            static let home_tab = "home_tab"
            static let notify_arrival_tab = "notify_arrival_tab"
            static let more_tab = "more_tab"
            static let travel_log = "travel_log"
            static let favourites = "favourites"
            static let settings = "settings"
            
            static let tap_poi = "tap_poi"
            static let tap_poi_walk = "tap_poi_walk"
            static let poi_more_info = "poi_more_info"
            
            static let parking_more_info = "parking_tooltip_see_more"
            static let parking_calculatefee = "parking_calculatefee"
            static let notify_add = "notify_add"
            static let notify_edit = "notify_edit"
            static let trip_log_tab = "trip_log_tab"
            static let more_tab_fav = "more_tab_fav"
            static let more_tab_settings = "more_tab_settings"
            static let view_route = "view_route"
            static let tap_carpark = "tap_carpark"
            static let map_petrol_icon_click = "map_petrol_icon_click"
            static let map_hawker_icon_click = "map_hawker_icon_click"
            static let map_library_icon_click = "map_library_icon_click"
            static let map_playground_icon_click = "map_playground_icon_click"
            static let map_charger_icon_click = "map_charger_icon_click"
            static let map_atm_icon_click = "map_atm_icon_click"
            static let map_foodforgood_icon_click = "map_foodforgood_icon_click"
            static let map_park_icon_click = "map_park_icon_click"
            static let map_pcn_icon_click = "map_pcn_icon_click"
            static let map_sportsg_icon_click = "map_sportsg_icon_click"
            static let map_museum_icon_clic = "map_museum_icon_click"
            static let navigate_here_amenities_petrol = "navigate_here_amenities_petrol"
            static let navigate_here_amenities_hawker = "navigate_here_amenities_hawker"
            static let navigate_here_amenities_library = "navigate_here_amenities_library "
            static let navigate_here_amenities_playground = "navigate_here_amenities_playground"
            static let navigate_here_amenities_charger = "navigate_here_amenities_charger"
            static let navigate_here_amenities_atm = "navigate_here_amenities_atm"
            static let navigate_here_amenities_foodforgood = "navigate_here_amenities_foodforgood"
            static let navigate_here_amenities_park = "navigate_here_amenities_park"
            static let navigate_here_amenities_pcn = "navigate_here_amenities_pcn"
            static let navigate_here_amenities_sportsg = "navigate_here_amenities_sportsg"
            static let navigate_here_amenities_museum = "navigate_here_amenities_museum"
            static let navigate_here_parking = "navigate_here_parking"
            static let carpark_hide = "carpark_hide"
            static let carpark_all_nearby = "carpark_all_nearby"
            static let carpark_available_only = "carpark_available_only"
            static let tap_custom_map = "tap_custom_map"
            static let nudge_next = "nudge_next"
            static let nudge_close = "nudge_close"
            static let nudge_done = "nudge_done"
            static let delete_save_confirm = "delete_save_confirm"
            static let delete_save_cancel = "delete_save_cancel"
            
            static let drop_pin_on = ":drop_pin_on"
            static let drop_pin_off = ":drop_pin_off"
            static let tooltip_saved_navigate_here = "[map]:tooltip_saved_navigate_here"
            static let tooltip_saved_bookmark = "[map]:tooltip_saved_bookmark"
            static let tooltip_saved_unbookmark = "[map]:tooltip_saved_unbookmark"
            static let tooltip_saved_share = "[map]:tooltip_saved_share"
            static let tooltip_saved_invite = "[map]:tooltip_saved_invite"
            static let tooltip_destination_invite = "[map]:tooltip_destination_invite"
            static let tooltip_carpark_invite = "[map]:tooltip_carpark_invite"
            static let tooltip_ev_invite = "[map]:tooltip_ev_invite"
            static let tooltip_petrol_invite = "[map]:tooltip_petrol_invite"
        }
        
        enum UserSwipe {
            static let swipe_up = "swipe_up"
            static let swipe_down = "swipe_down"
        }
        
        enum MapInteraction {
            static let drag = "drag_map"
            static let pinch = "pinch_map"
            static let rotate = "rotate_map"
        }
        
        enum UserPopup {
            static let email = "update_email"
            static let calendar = "calendar_permission"
            static let push_notification_permission = "push_notification_permission"
            static let nudge = "nudge"
            static let unsaveCollection = "unsave_collection"
        }
        
        enum UserToggle {
            static let parking_icon_on = "parking_icon_on"
            static let parking_icon_off = "parking_icon_off"
            static let amenities_petrol_on = "amenities_petrol_on"
            static let amenities_petrol_off = "amenities_petrol_off"
            static let amenities_hawker_on = "amenities_hawker_on"
            static let amenities_hawker_off = "amenities_hawker_off"
            static let amenities_library_on = "amenities_library_on"
            static let amenities_library_off = "amenities_library_off"
            static let amenities_playground_on = "amenities_playground_on"
            static let amenities_playground_off = "amenities_playground_off"
            static let amenities_charger_on = "amenities_charger_on"
            static let amenities_charger_off = "amenities_charger_off"
            static let amenities_atm_on = "amenities_atm_on"
            static let amenities_atm_off = "amenities_atm_off"
            static let amenities_foodforgood_on = "amenities_foodforgood_on"
            static let amenities_foodforgood_off = "amenities_foodforgood_off"
            static let amenities_park_on = "amenities_park_on"
            static let amenities_park_off = "amenities_park_off"
            static let amenities_pcn_on = "amenities_pcn_on"
            static let amenities_pcn_off = "amenities_pcn_off"
            static let amenities_sportsg_on = "amenities_sportsg_on"
            static let amenities_sportsg_off = "amenities_sportsg_off"
            static let amenities_museum_on = "amenities_museum_on"
            static let amenities_museum_off = "amenities_museum_off"
            static let amenities_heritagetree_on = "amenities_heritagetree_on"
            static let amenities_heritagetree_off = "amenities_heritagetree_off"
            static let carpark_tooltip_save_on = "carpark_tooltip_save_on"
            static let carpark_tooktip_save_off = "carpark_tooktip_save_off"
            static let amenities_tooltip_save_on = "amenities_tooltip_save_on"
            static let amenities_tooltip_save_off = "amenities_tooltip_save_off"
        }
    }
    
    enum Settings {
        static let screen_view = "settings"
        
        enum UserClick {
            static let back = "back"
            static let account = "account"
            //            static let map_display = "map_display"
            static let preferences = "preferences"
            static let privacy_policy = "privacy_policy"
            static let terms_condition = "terms_condition"
            static let faq = "faq"
            static let feedback = "feedback"
            static let about = "about"
        }
    }
    
    enum SettingsAccount {
        static let screen_view = "settings_account"
        
        enum UserClick {
            static let back = "back"
            static let edit_name = "edit_name"
            static let edit_email = "edit_email"
            static let edit_car_detail = "car_details_edit"
            static let logout = "logout"
        }
    }
    
    enum AccountEditName {
        static let screen_view = "settings_account_edit_username"
        
        enum UserClick {
            static let back = "back"
            static let save = "save"
        }
        
        enum UserEdit {
            static let input = "username_input"
        }
    }
    
    enum AccountEditEmail {
        static let screen_view = "settings_account_edit_email"
        
        enum UserClick {
            static let back = "back"
            static let save = "save"
            static let resend = "resend_link"
        }
        
        enum UserEdit {
            static let input = "email_input"
        }
    }
    
    enum SettingsMap {
        static let screen_view = "settings_map_display"
        
        enum UserClick {
            static let back = "back"
            static let auto = "auto"
            static let light = "light"
            static let dark = "dark"
        }
    }
    
    enum SettingsPrivacy {
        static let screen_view = "settings_privacy_policy"
        
        enum UserClick {
            static let back = "back"
        }
    }
    
    enum SettingsTnC {
        static let screen_view = "settings_terms_conditions"
        
        // Not required any implementation
    }
    
    enum SettingsFaq {
        static let screen_view = "setting_faqs"
        
        // Not required any implementation
    }
    
    enum SettingsAbpout {
        static let screen_view = "settings_about"
        
        // Not required any implementation
    }
    
    enum Cruise {
        static let screen_view = "cruise"   // actually the cruise mode and map landing is the same screen
        
        enum UserClick {
            static let mute = "mute"
            static let unmute = "unmute"
            static let eta_main_button = "eta_main_button"
            static let eta_end = "eta_end"
            static let eta_pause = "eta_pause"
            static let eta_resume = "eta_resume"
            static let end_cruise = "end_cruise"
            static let start_cruise = "start_cruise"
            static let navigate_here_parking = "navigate_here_parking"
        }
        
        enum UserNotification {
            static let cruise_start = "cruise_start"
        }
        
        enum UserToggle {
            static let parking_icon_on = "parking_icon_on"
            static let parking_icon_off = "parking_icon_on"
            static let resend = "resend_link"
        }
        
        enum MapInteraction {
            static let drag = "drag_map"
            static let pinch = "pinch_map"
            static let rotate_map = "rotate_map"
        }
    }
    
    enum RoutePlanning {
        static let screen_view = "route_planning"
        
        enum UserClick {
            static let back = "back"
            static let carpark_nearby = "carpark_nearby"
            static let reset_carpark = "reset_carpark"
            static let edit_favourite = "edit_favourite"
            static let add_remove_favourite = "add_remove_favourite"
            static let tap_route = "tap_route"
            static let share_my_eta = "share_my_eta"
            static let reset_my_eta = "reset_my_eta"
            static let fastest_route = "fastest_route"
            static let shortest_route = "shortest_route"
            static let cheapest_route = "cheapest_route"
            static let lets_go = "lets_go"
            static let search_origin = "search_origin"
            static let search_destination = "search_destination"
            static let origin_swap = "origin_swap"
            static let direction = "direction"
            static let notify_arrival = "notify_arrival"
            static let later = "later"
            static let savelater = "savelater"
            static let petrol_map_icon = "petrol_map_icon"
            static let ev_map_icon = "ev_map_icon"
            static let add_stop_petrol = "add_stop_petrol"
            static let remove_stop_petrol = "remove_stop_petrol"
            static let add_stop_ev = "add_stop_ev"
            static let remove_stop_ev = "remove_stop_ev"
            static let parking_moreinfo = "parking_moreinfo"
            static let parking_calculatefee = "parking_calculatefee"
            static let parking_navigatehere = "parking_navigatehere"
            static let datetime_cancel = "datetime_cancel"
            static let datetime_next = "datetime_next"
            static let lets_go_trip_saved_popup = "lets_go_trip_saved_popup"
            static let check_in_triplog_trip_saved_popup = "check_in_triplog_trip_saved_popup"
            static let close_trip_updated_popup = "close_trip_updated_popup"
            static let got_it_unable_to_save = "got_it_unable_to_save"
            static let view_nearby_voucher_carparks = "view_nearby_voucher_carparks"
            static let back_route_options = "back_route_options"
            static let share_drive_open = "share_drive_open"
            static let share_drive_close = "share_drive_close"
        }
        
        enum UserSwipe {
            static let swipe = "swipe_card"
        }
        
        enum MapInteraction {
            static let drag = "drag_map"
            static let pinch = "pinch_map"
            static let rotate_map = "rotate_map"
        }
        
        enum UserSelection {
            static let lets_go_route_type = "lets_go_route_type"
            static let savelater_route_type = "savelater_route_type"
            
            
        }
        
        enum LocationData {
            static let   lets_go_origin = "lets_go_origin"
            static let savelater_origin = "savelater_origin"
            static let   lets_go_destination = "lets_go_destination"
            static let savelater_destination = "savelater_destinatio"
            static let   lets_go_added_stop = "lets_go_added_stop"
            static let savelater_added_stop = "savelater_added_stop"
            
        }
        
        enum UserToggle {
            static let petrol_on = "petrol_on"
            static let petrol_off = "petrol_off"
            static let ev_on = "ev_on"
            static let ev_off = "ev_off"
            static let  parking_on = " parking_on"
            static let parking_off = "parking_off"
            static let spc_on = "spc_on"
            static let spc_off = "spc_off"
            static let esso_on = "esso_on"
            static let esso_off = "esso_off"
            static let  shell_on = "shell_on"
            static let shell_off = "shell_off"
            static let caltex_on = "caltex_on"
            static let caltex_off = "caltex_off"
            static let sinopec_on = "sinopec_on"
            static let sinopec_off = "sinopec_off"
            static let  type2_on = "type2_on"
            static let type2_off = "type2_off"
            static let combo2_on = "combo2_on"
            static let combo2_off = "combo2_off"
            static let  chademo_on = "chademo_on"
            static let chademo_off = "chademo_off"
            static let  depart_at = "depart_at"
            static let arrive_by = "arrive_by"
        }
        
        enum UserPopUp {
            static let popupName = "origin_alert"
            static let cancel = "cancel"
            static let yes = "yes"
            
            static let trip_been_saved = "trip_been_saved"
            static let trip_been_updated = "trip_been_updated"
            static let  unable_to_save = "unable_to_save"
            static let popup_open = "popup_open"
            static let popup_close = "popup_close"
            
        }
        
        
    }
    
    enum Carpark {
        static let screen_view = "route_planning_carpark"
        
        enum UserClick {
            static let back = "back"
            static let tap_carpark = "tap_carpark"
            static let tap_voucher_carpark = "tap_voucher_carpark"
            static let close_bubble = "close_bubble"
            static let navigate_here = "navigate_here"
            static let more_info = "more_info"
        }
        
        enum MapInteraction {
            static let drag = "drag_map"
            static let pinch = "pinch_map"
        }
    }
    
    enum ArrivalFavorite {
        static let screen_view = "arrival_fav"
        
        enum UserClick {
            static let contacts_permission_allow = "contacts_permission_allow"
            static let contacts_permission_deny = "contacts_permission_deny"
            static let back = "back"
            static let add_new = "add_new"
            static let entry_select = "entry_select"
            static let edit = "edit"
            static let delete = "delete"
            static let pop_confirmation_remove = "pop_confirmation_remove"
            static let pop_confirmation_cancel = "pop_confirmation_cancel"
        }
    }
    
    enum Feedback {
        static let screen_view = "report_issue"
        
        enum UserToggle {
            static let route = "route"
            static let voice = "voice"
            static let parking = "parking"
            static let erp = "erp"
            static let interface = "interface"
            static let traffic = "traffic"
        }
        
        enum UserClick {
            static let close = "close"
            static let submit = "submit"
            static let instabug = "instabug"
        }
        
        enum UserEdit {
            static let description = "description"
        }
    }
    
    enum ShareMyETA {
        static let screen_view = "share_my_eta"
        
        
        
        enum UserEdit {
            static let message_input = "message_input"
        }
    }
    
    enum Navigation {
        static let screen_view = "navigation"
        
        enum UserClick {
            static let mute = "mute"
            static let unmute = "unmute"
            static let eta_resume = "eta_resume"
            static let eta_pause = "eta_pause"
            static let recenter = "recenter"
            static let end_navigation = "end_navigation"
            static let pop_user_end_yes = "pop_user_end_yes"
            static let pop_user_end_no = "pop_user_end_no"
            static let pop_natural_end_done = "pop_natural_end_done"
            static let pop_rating_close = "pop_rating_close"
            static let pop_rating_submit = "pop_rating_submit"
            static let pop_instabug = "pop_instabug"
            static let pop_star1 = "pop_star1"
            static let pop_star2 = "pop_star2"
            static let pop_star3 = "pop_star3"
            static let pop_star4 = "pop_star4"
            static let pop_star5 = "pop_star5"
            static let use_walking_guide = "use_walking_guide"
            static let end_no_walking_guide = "end_no_walking_guide"
            static let end_walking = "end_walking"
            static let arrival_end_navigation = "arrival_end_navigation"
            static let navigate_here_parking = "navigate_here_parking"
            static let navigate_verify_avail_yes = "verify_avail_yes"
            static let navigate_verify_avail_no = "verify_avail_no"
        }
        
        
        enum UserPopUp {
            static let rating = "rating"
            static let user_end_destination_alert = "user_end_destination_alert"
        }
    }
    
    enum OutsideApp {
        static let screen_view = "outside_app"
        
        enum UserClick {
            static let notification_click = "notification_click"
        }
        
        enum Statistics {
            static let date_pushed = "date_pushed"
            static let date_clicked = "date_clicked"
            static let message = "message"
            static let main_category = "main_category"
            static let sub_category = "sub_category"
        }
    }
    
    enum CarplaySignIn {
        
        static let screen_view = "carplay_signin"
        enum CarDisplayConnect {
            static let carconnect = "carconnect"
        }
        
    }
    
    enum CarplayHomepage {
        static let screen_view = "carplay_homepage"
        
        enum UserClick {
            static let search = "search"
            static let cruise = "cruise"
            static let notify = "notify"
            static let mapbox_zoomin = "mapbox_zoomin"
            static let mapbox_zoomout = "mapbox_zoomout"
            static let mapbox_pan_up = "mapbox_pan_up"
            static let mapbox_pan_down = "mapbox_pan_down"
            static let mapbox_pan_left = "mapbox_pan_left"
            static let mapbox_pan_right = "mapbox_pan_right"
            static let mapbox_pan_close = "mapbox_pan_close"
            static let mapbox_pan_recenter = "mapbox_pan_recenter"
        }
        
        enum UserToggle {
            static let display_carpark = "display_carpark"
            static let hide_carpark = "hide_carpark"
            
        }
    }
    
    enum CarplayParkingInfo {
        static let screen_view = "carplay_parking_info"
        
        enum UserClick {
            static let back = "back"
            static let navigate = "navigate"
            
        }
        
        
    }
    
    enum CarplaySearchMenu {
        static let screen_view = "carplay_search_menu"
        
        enum UserClick {
            static let back = "back"
            static let new_search = "new_search"
            static let favourite = "favourite"
            static let recent_search = "recent_search"
        }
        
        
    }
    
    enum CarplayRecentSearch {
        static let screen_view = "carplay_recent_search"
        
        enum UserClick {
            static let back = "back"
            static let  scroll_up = "scroll_up"
            static let  scroll_down = "scroll_down"
            static let select_destination = "select_destination"
        }
        
        
    }
    
    
    enum CarplayFavourite {
        static let screen_view = "carplay_favourite"
        
        enum UserClick {
            static let back = "back"
            static let  scroll_up = "scroll_up"
            static let  scroll_down = "scroll_down"
            static let select_destination = "select_destination"
        }
        
        
    }
    
    enum CarplaySearch {
        static let screen_view = "carplay_search"
        
        enum UserClick {
            static let cancel = "cancel"
            static let search = "search"
            static let voice_input = "voice_input"
            static let select_destination = "select_destination"
        }
        
        enum UserEdit {
            static let  search_input = "search_input"
        }
    }
    
    enum CarplayRoutePlanning {
        static let screen_view = "[carplay_routeplanning]"
        
        enum UserClick {
            static let lets_go_origin = "lets_go_origin"
            static let lets_go_destination = "lets_go_destination"
        }
    }
    
    enum CarplayHome {
        static let screen_view = "[carplay_homepage]"
        
        enum UserClick {
            static let search = ":search"
            static let carparks = ":carparks"
        }
    }
    
    enum CarplaySearchPage {
        static let screen_view = "[carplay_search]"
        
        enum UserClick {
            static let back = ":back"
            static let keyboard = ":keyboard"
            static let recent_searches = ":recent_searches"
            static let home = ":home"
            static let work = ":work"
            static let petrol = ":petrol"
            static let evcharger = ":ev"
                        
            static let textbox_voice_input = "[textbox]:voice_input"    //  Not available
            static let textbox_cancel = "[textbox]:cancel"
            static let textbox_result_select = "[textbox]:result_select"
            
            static let nearby_petrol_right_arrow = "[nearby_petrol]:right_arrow"
            static let nearby_petrol_left_arrow = "[nearby_petrol]:left_arrow"
            static let nearby_petrol_back = "[nearby_petrol]:back"
            static let nearby_petrol_navigate_here = "[nearby_petrol]:navigate_here"
            
            static let nearby_ev_right_arrow = "[nearby_ev]:right_arrow"
            static let nearby_ev_left_arrow = "[nearby_ev]:left_arrow"
            static let nearby_ev_back = "[nearby_ev]:back"
            static let nearby_ev_navigate_here = "[nearby_ev]:navigate_here"
        }
        
        enum UserEdit {
            static let textbox_location_input = "[textbox]:location_input"
        }
    }
    
    
    enum CarplayRoutePreview {
        static let screen_view = "carplay_route_preview"
        
        enum UserClick {
            static let  back = "back"
            static let  parking = "parking"
            static let  notify = "notify"
            static let other_route = "other_route"
            static let lets_go = "lets_go"
        }
        
        enum UserToggle {
            static let display_carpark = "display_carpark"
            static let hide_carpark = "hide_carpark"
            
        }
        
        
    }
    
    enum CarplayRoutePreviewParking {
        static let screen_view = "carplay_route_preview_parking"
        
        enum UserClick {
            static let  back = "back"
            static let mapbox_zoomin = "mapbox_zoomin"
            static let mapbox_zoomout = "mapbox_zoomout"
            static let mapbox_pan_up = "mapbox_pan_up"
            static let mapbox_pan_down = "mapbox_pan_down"
            static let mapbox_pan_left = "mapbox_pan_left"
            static let mapbox_pan_right = "mapbox_pan_right"
            static let mapbox_pan_close = "mapbox_pan_close"
            static let mapbox_pan_recenter = "mapbox_pan_recenter"
        }
        
        
    }
    
    enum CarplayRoutePreviewOtherRoutes {
        static let screen_view = "carplay_route_preview_other_routes"
        
        enum UserClick {
            static let  cancel = "cancel"
            static let confirm = "confirm"
            static let  route_selection_radio_button =  "route_selection_radio_button"
            
        }
        
        
    }
    
    enum CarplayNavigation {
        static let screen_view = "carplay_navigation"
        
        enum UserClick {
            static let  mute = "mute"
            static let unmute = "unmute"
            static let  end =  "end"
            static let  notification_close = "notification_close"
            static let pause_sharing = "pause_sharing"
            static let  resume_sharing =  "resume_sharing"
            static let mapbox_zoomin = "mapbox_zoomin"
            static let mapbox_zoomout = "mapbox_zoomout"
            static let mapbox_pan_up = "mapbox_pan_up"
            static let mapbox_pan_down = "mapbox_pan_down"
            static let mapbox_pan_left = "mapbox_pan_left"
            static let mapbox_pan_right = "mapbox_pan_right"
            static let mapbox_pan_close = "mapbox_pan_close"
            static let mapbox_pan_recenter = "mapbox_pan_recenter"
            
        }
        
        
    }
    
    enum CarplayCruise {
        static let screen_view = "carplay_cruise"
        
        enum UserClick {
            static let  mute = "mute"
            static let unmute = "unmute"
            static let  end_cruise =  "end_cruise"
            static let  notification_close = "notification_close"
            static let pause_sharing = "pause_sharing"
            static let  resume_sharing =  "resume_sharing"
            static let mapbox_zoomin = "mapbox_zoomin"
            static let mapbox_zoomout = "mapbox_zoomout"
            static let mapbox_pan_up = "mapbox_pan_up"
            static let mapbox_pan_down = "mapbox_pan_down"
            static let mapbox_pan_left = "mapbox_pan_left"
            static let mapbox_pan_right = "mapbox_pan_right"
            static let mapbox_pan_close = "mapbox_pan_close"
            static let mapbox_pan_recenter = "mapbox_pan_recenter"
            
        }
        
        
    }
    
    
    enum CarplayNotifyArrivalList {
        static let screen_view = "carplay_notify_arrival_list"
        
        enum UserClick {
            static let  back = "back"
            static let scroll_up = "scroll_up"
            static let  scroll_down =  "scroll_down"
            static let  select_notify_list = "select_notify_list"
            
        }
        
    }
    
    
    enum CarplayNotifyArrivalOptions {
        static let screen_view = "carplay_notify_arrival_options"
        
        enum UserClick {
            static let  cancel = "cancel"
            static let choose_from_notify_list = "choose_from_notify_list"
            static let  setup_new_arrival_message =  "setup_new_arrival_message"
            
            
        }
        
        
    }
    
    enum CarplaySetupNewNotifyArrival{
        static let screen_view = "carplay_setup_new_notify_arrival"
        
        enum UserClick {
            static let  cancel = "cancel"
            static let done = "done"
            
        }
        
        
    }
    
    enum OnBoardingCruiseIntro {
        static let screen_view = "onboarding_cruise_intro"
        
        enum UserClick {
            static let  next = "next"
            
            
        }
        
        
    }
    
    enum OnBoardingRoad {
        static let screen_view = "onboarding_road_notify"
        
        enum UserClick {
            static let  next = "next"
            static let  back = "back"
            static let road_notify_selections = "road_notify_selections"
            
            
        }
        
        
    }
    
    
    enum OnBoardingAmenities {
        static let screen_view = "onboarding_amenities"
        
        enum UserClick {
            static let  done = "done"
            static let  back = "back"
            static let  road_amenities_selections = "road_amenities_selections"
            static let road_amentities_petrol_secondary = "road_amentities_petrol_secondary"
            static let road_amenities_ev_secondary = "road_amenities_ev_secondary"
            
            
            
        }
        
        
    }
    
    
    
    
    
    
    
    
    enum ErpDetails {
        static let screen_view = "erp_details"
        
        enum UserClick {
            static let back = "back"
            static let  map_erp_icon_click = "map_erp_icon_click"
            
            
            
        }
        
        enum MapInteraction {
            static let pinch_map = "pinch_map"
            static let drag_map = "drag_map"
            static let rotate_map = "rotate_map"
        }
        
        
        enum LocationData {
            static let selected_map_erp_location = "selected_map_erp_location"
            
            
        }
    }
    
    
    
    
    enum TripLog {
        static let screen_view = "[trip_log]"
        
        enum UserToggle {
            static let upcoming = "upcoming"
            static let  history = "history"
        }
        
        enum UserSwipe {
            static let swipe_left = "swipe_left"
            static let  swipe_right = " swipe_right"
            
            
        }
        enum UserEdit {
            static let triplog_search = "triplog_search"
            
        }
        
        enum UserClick {
            static let back = ":back"
            static let  select = "select"
            static let  search = "search"
            static let delete = "delete"
            static let date_selector_icon = "date_selector_icon"
            static let    date_clearall = " date_clearall"
            static let date_confirm = "date_confirm"
            static let   date_close = "date_close"
            
            
        }
    }
    
    
    enum EditRoutePlanning {
        static let screen_view = "edit_route_planning"
        
        enum UserToggle {
            static let petrol_on = "petrol_on"
            static let  petrol_off = "petrol_off"
            static let ev_on = "ev_on"
            static let  ev_off = "ev_off"
            static let parking_on = "parking_on"
            static let  parking_off = "parking_on"
            static let spc_on = "spc_on"
            static let spc_off = "spc_off"
            static let esso_on = "esso_on"
            static let esso_off = "esso_off"
            static let  shell_on = "shell_on"
            static let shell_off = "shell_off"
            static let caltex_on = "caltex_on"
            static let caltex_off = "caltex_off"
            static let sinopec_on = "sinopec_on"
            static let sinopec_off = "sinopec_off"
            static let  type2_on = "type2_on"
            static let type2_off = "type2_off"
            static let combo2_on = "combo2_on"
            static let combo2_off = "combo2_off"
            static let  chademo_on = "chademo_on"
            static let chademo_off = "chademo_off"
            static let  depart_at = "depart_at"
            static let arrive_by = "arrive_by"
            
            
        }
        
        enum UserClick {
            static let back = "back"
            static let search_origin = "search_origin"
            static let search_destination = "search_destination"
            static let origin_swap = "origin_swap"
            static let direction = "direction"
            static let notify_arrival = "notify_arrival"
            static let lets_go = "lets_go"
            static let savelater = "savelater"
            static let petrol_map_icon = "petrol_map_icon"
            static let add_stop_petrol = "add_stop_petrol"
            static let remove_stop_petrol = "remove_stop_petrol"
            static let add_stop_ev = "add_stop_ev"
            static let remove_stop_ev = "remove_stop_ev"
            static let parking_moreinfo = "parking_moreinfo"
            static let parking_calculatefee = "parking_calculatefee"
            static let parking_navigatehere = "parking_navigatehere"
            static let datetime_cancel = "datetime_cancel"
            static let datetime_next = "datetime_next"
            static let ev_map_icon = "ev_map_icon"
            
        }
        
        
        enum UserPopUp {
            static let popupName = "origin_alert"
            static let cancel = "cancel"
            static let yes = "yes"
            
        }
        
        
        enum MapInteraction {
            static let drag = "drag_map"
            static let pinch = "pinch_map"
            static let rotate = "rotate_map"
        }
        
        
        enum UserSelection {
            static let lets_go_route_type = "lets_go_route_type"
            static let savelater_route_type = "savelater_route_type"
            
            
        }
        
        enum LocationData {
            static let   lets_go_origin = "lets_go_origin"
            static let savelater_origin = "savelater_origin"
            static let   lets_go_destination = "lets_go_destination"
            static let savelater_destination = "savelater_destination"
            static let   lets_go_added_stop = "lets_go_added_stop"
            static let savelater_added_stop = "savelater_added_stop"
            
        }
    }
    
    
    enum GlobalNotification {
        static let screen_view = ""
        
        enum UserClick {
            static let see_on_map_global_noti = "see_on_map_global_noti"
            static let read_more_global_noti = "read_more_global_noti"
            static let play_tutorial_video_noti = "read_more_global_noti"
            static let start_trip_global_noti = "start_trip_global_noti"
            static let close_global_noti = "close_global_noti"
            
        }
        
        enum UserPopUp {
            static let popupName = "global_notification"
            static let popup_open = "popup_open"
            static let popup_close = "popup_close"
            
        }
    }
    
    enum NotificationInbox {
        static let screen_view = "notification_inbox"
        
        enum UserClick {
            static let back = "back"
            static let all_notification_filter = "all_notification_filter"
            static let single_notification_click = "single_notification_click"
            static let filter_close = "filter_close"
            static let filter_all_notifications = "filter_all_notifications"
            static let filter_traffic_alerts = "filter_traffic_alerts"
            static let filter_admin_msg = "filter_admin_msg"
            static let filter_upcoming_trips = "filter_upcoming_trips"
        }
        
        enum Statistics {
            static let date_pushed = "date_pushed"
            static let message_title = "message_title"
        }
    }
    
    enum InboxDetail {
        static let screen_view = "inbox_detail"
        
        enum UserClick {
            static let close = "close"
            static let update_now = "update_now"
            static let content_click = "content_click"
        }
        
        enum Statistics {
            static let link_type = "link_type"
            static let link_value = "link_value"
        }
    }
    
    enum ShareCollectionRecipient {
        static let screen_view = "[shared_collection_recipient]"
        
        enum UserClick {
            static let tooltip_saved_navigate_here = "[map]:tooltip_saved_navigate_here"
            static let tooltip_saved_bookmark = "[map]:tooltip_saved_bookmark"
            static let tooltip_saved_unbookmark = "[map]:tooltip_saved_unbookmark"
            static let tooltip_saved_share = "[map]:tooltip_saved_share"
        }
    }
    
    // MARK: - OBU Pairing
    enum ObuPairing {
        static let screen_view = "[obu_pairing]"
        enum UserClick {
            static let back = ":back"
            static let start_engine_back = "[start_engine]:back"
            static let start_engine_obu_already_paired_ok = "[start_engine]:obu_already_paired_ok"
            static let popup_obu_permission_turn_on_bluetooth_setting = "[popup_obu_permission_turn_on_bluetooth]:settings"
            static let popup_obu_permission_turn_on_bluetooth_ok = "[popup_obu_permission_turn_on_bluetooth]:ok"
            static let popup_obu_permission_turn_on_internet_setting = "[popup_obu_permission_turn_on_internet]:settings"
            static let popup_obu_permission_turn_on_internet_setting_ok = "[popup_obu_permission_turn_on_internet]:ok"
            static let popup_allow_obu_permission_access_ok = "[popup_allow_obu_access]:ok"
            static let popup_allow_obu_permission_access_setting = "[popup_allow_obu_access]:settings"
            static let popup_obu_not_found_ok = "[popup_obu_not_found]:ok"
            static let popup_obu_pairing_failed_ok = "[popup_obu_pairing_failed]:ok"
            static let popup_obu_mac_not_match_close = "[popup_obu_mac_not_match]:close"
            static let start_engine_confirm_obu_close = "[start_engine],[confirm_obu]:close"
            static let start_engine_confirm_obu_select_obu = "[start_engine],[confirm_obu]:select_obu"
            static let start_engine_confirm_obu_select_confirm = "[start_engine],[confirm_obu]:select_confirm"
            static let number_plate_back = "[number_plate]:back"
            static let number_plate_name_number_input = "[number_plate]:name_number_input"
            static let number_plate_next = "[number_plate]:next"
            static let verify_bluetooth_pairing_failed_close = "[verify_bluetooth],[pairing_failed]:close"
            static let verify_bluetooth_pairing_failed_register_mac_with_lta = "[verify_bluetooth],[pairing_failed]:register_mac_with_lta"
            static let enter_mac_skip_setup = "[enter_mac]:skip_setup"
            static let enter_mac_not_registered = "[enter_mac]:not_registered"
            static let pairing_cancel_pairing = "[pairing]:cancel_pairing"
            static let popup_obu_connection_error_bluetooth_close = "[popup_obu_connection_error_bluetooth]:close"
            static let popup_obu_connection_error_internet_close = "[popup_obu_connection_error_internet]:close"
            static let pairing_error_close = "[pairing_error]:close"
            static let pairing_error_retry = "[pairing_error]:retry"
            static let pairing_error_skip = "[pairing_error]:skip"
            static let pairing_error_report_issue = "[pairing_error]:report_issue"
            static let system_error_close = "[system_error]:close"
            static let system_error_retry = "[system_error]:retry"
            static let system_error_skip = "[system_error]:skip"
            static let system_error_report_issue = "[system_error]:report_issue"
            static let obu_not_detected_close = "[obu_not_detected]:close"
            static let obu_not_detected_retry = "[obu_not_detected]:retry"
            static let obu_not_detected_skip = "[obu_not_detected]:skip"
            static let obu_not_detected_report_issue = "[obu_not_detected]:report_issue"
            static let verify_bluetooth_next = "[verify_bluetooth]:next"
            static let pairing_explore_breeze = "[pairing]:explore_breeze"
            static let obu_connection_lost_skip = "[obu_connection_lost]:skip"
            static let obu_connection_lost_retry = "[obu_connection_lost]:retry"
            static let obu_connection_lost_close = "[obu_connection_lost]:close"
            static let obu_connection_lost_report_issue = "[obu_connection_lost]:report_issue"
            static let popup_obu_connection_error_obu_close = "[popup_obu_connection_error_obu]:close"
            static let popup_obu_card_error_gotit = "[popup_obu_card_error]:got_it"
        }
        
        enum UserPopup {
            static let popup_obu_permission_turn_on_internet = "popup_obu_permission_turn_on_internet"
            static let popup_obu_permission_turn_on_bluetooth = "popup_obu_permission_turn_on_bluetooth"
            static let popup_allow_obu_access = "popup_allow_obu_access"
            static let popup_obu_not_found = "popup_obu_not_found"
            static let popup_obu_pairing_failed = "popup_obu_pairing_failed"
            static let popup_obu_mac_not_match = "popup_obu_mac_not_match"
            static let popup_obu_connection_error_bluetooth = "popup_obu_connection_error_bluetooth"
            static let popup_obu_connection_error_obu_permission = "popup_obu_connection_error_obu"
            static let obu_connected = "obu_connected"
            static let obu_not_connected = "obu_not_connected"
            static let obu_connection_error = "obu_connection_error"
            static let obu_not_paired = "obu_not_paired"
//            static let navigation_notification_open = "[dhu]:popup_open"
//            static let navigation_notification_close = "[dhu]:popup_close"
//            static let cruise_notification_open = "[dhu]:popup_open"
//            static let cruise_notification_close = "[dhu]:popup_close"
//            static let obu_connected_dhu = "[dhu]:popup_open"
//            static let obu_not_connected_dhu = "[dhu]:popup_open"
            static let popup_obu_connection_error_internet = ":popup_open"
            static let dhu_popup_open = "[dhu]:popup_open"
            static let dhu_popup_close = "[dhu]:popup_close"
            static let popup_obu_card_error = "obu_card_error"
            
        }
    }
    
    enum CarPlayObuState {
        
        static let dhu_homepage = "[dhu_homepage]"
        static let dhu_navigation = "[dhu_navigation]"
        static let dhu_cruise = "[dhu_cruise]"
        
        enum UserClick {
            static let dhu_obu_button = "[dhu]:obu_button"
            static let dhu_popup_obu_connected_back = "[dhu],[popup_obu_connected]:back"
            static let dhu_popup_obu_not_connected_retry = "[dhu],[popup_obu_not_connected]:retry"
            static let dhu_popup_obu_not_connected_back = "[dhu],[popup_obu_not_connected]:back"
            static let dhu_popup_obu_connection_error_report_issue = "[dhu],[popup_obu_connection_error]:report_issue"
            static let dhu_popup_obu_connection_error_back = "[dhu],[popup_obu_connection_error]:back"
            static let dhu_popup_obu_not_paired_report_issue = "[dhu],[popup_obu_not_paired]:report_issue"
            static let dhu_pop_up_navigation_notification_close = "[dhu],[pop_up_navigation_notification]:close"
            static let dhu_pop_up_navigation_notification_click_notification = "[dhu],[pop_up_navigation_notification]:click_notification"
            static let dhu_travel_time_back = "[dhu],[travel_time]:back"
            static let dhu_parking_availability_back = "[dhu],[parking_availability]:back"
            static let dhu_pop_up_cruise_notification_close = "[dhu],[popup_cruise_notification]:close"
            static let dhu_popup_navigation_notification_close = "[dhu],[popup_navigation_notification]:close"
            static let dhu_popup_navigation_notification_click_notification = "[dhu],[popup_navigation_notification]:click_notification"
            static let dhu_popup_cruise_notification_click_notification = "[dhu],[popup_cruise_notification]:click_notification"
            static let dhu_parking_availability_select_carpark = "[dhu],[parking_availability]:select_carpark"
        }
    }
    
    enum SignInCreate {
        
        static let screen_view = "[signin_create]"
        
        enum UserClick {
            static let guest_mode_warning_close = ":guest_mode_warning_close"
            static let guest_mode_warning_cancel = ":guest_mode_warning_cancel"
            static let guest_mode_warning_proceed = ":guest_mode_warning_proceed"
        }
    }
    
    enum ObuDisplayMode {
        
        static let screen_view = "[obu_display_mode]"
        
        enum UserClick {
            static let cruise_start = ":cruise_start"
            static let close = ":close"
            static let search = ":search"
            static let share_drive = ":share_drive"
            static let recentre_map = "[map]:recentre_map"
            static let parking_guidance_system_carpark_select = "[parking_guidance_system]:carpark_select"  // message with select_*
            static let parking_guidance_system_close = "[parking_guidance_system]:close"
                    
            static let navigation_notification_open = ":popup_open"
            static let navigation_notification_close = ":popup_close"
            
            static let tap_carpark = "[map]:tap_carpark"
            static let tooltip_carpark_navigate_here = "[map]:tooltip_carpark_navigate_here"
            
            static let tap_petrol = "[map]:tap_petrol"
            static let tooltip_petrol_navigate_here = "[map]:tooltip_petrol_navigate_here"
            
            static let tap_ev = "[map]:tap_ev"
            static let tooltip_ev_navigate_here = "[map]:tooltip_ev_navigate_here"
            
            
            static let parking_vicinity_close = "[popup_no_parking_vicinity]:close"
            static let petrol_vicinity_close = "[popup_no_petrol_vicinity]:close"
            static let ev_vicinity_close = "[popup_no_ev_vicinity]:close"
            
            static let show_carpark_list = ":show_carpark_list"
            static let carpark_list_close = "[carpark_list]:close"
            static let carpark_list_select_carpark = "[carpark_list]:select_carpark"
            
        }
        
        enum UserToggle {
            static let mute_on = ":mute_on"
            static let mute_off = ":mute_off"
            static let cruise_on_display_off = ":cruise_on_display_off"
            static let cruise_off_display_on = ":cruise_off_display_on"
            static let p_button_off = ":P_button_off"
            static let p_button_on = ":P_button_on"
            
            static let ev_button_off = ":ev_button_off"
            static let ev_button_on = ":ev_button_on"
            static let petrol_button_off = ":petrol_button_off"
            static let petrol_button_on = ":petrol_button_on"
        }
        
        enum UserPopup {
            static let projected_to_dhu = "projected_to_dhu"
            static let obu_display_notification = "obu_display_notification"
            static let no_parking_vicinity = "no_parking_vicinity"
            static let no_petrol_vicinity = "no_petrol_5km"
            static let no_ev_vicinity = "no_ev_5km"
        }
    }
    
    enum CruiseMode {
        static let screen_view = "[cruise]"
        enum UserPopup {
            static let projected_to_dhu = "projected_to_dhu"
            static let cruise_notification = "cruise_notification"
        }
    }
    
    enum NavigationMode {
        static let screen_view = "[navigation]"
        static let walking_guide_screen = "[walking_guide]"
        
        enum UserClick {
            static let parking_guidance_system_carpark_select = "[parking_guidance_system]:carpark_select"
            static let parking_guidance_system_close = "[parking_guidance_system]:close"
            
            static let popup_travel_time_open = "popup_open"
            static let popup_travel_time_read_more = "[popup_travel_time]:read_more"
            
            static let popup_navigation_notification = "[popup_navigation_notification]:close"
            static let popup_navigation_notification_click_notification = "[popup_navigation_notification]:click_notification"
            
            static let popup_projected_to_dhu_okey = "[popup_projected_to_dhu]:ok"
            
            static let parking_vicinity_close = "[popup_no_parking_vicinity]:close"
            static let petrol_vicinity_close = "[popup_no_petrol_vicinity]:close"
            static let ev_vicinity_close = "[popup_no_ev_vicinity]:close"
        
            static let map_tooltip_ev_add_stop = "[map]:tooltip_ev_add_stop"
            static let map_tooltip_petrol_add_stop = "[map]:tooltip_petrol_add_stop"
            static let show_destination_carpark_list = ":show_destination_carpark_list"
            
            static let popup_reached_destination_walking_guide = "[popup_reached_destination]:walking_guide"
            static let popup_reached_destination_end_trip = "[popup_reached_destination]:end_trip"
            static let walking_guide_end_trip = ":end_trip"
            
            static let share_destination = ":share_destination"
            
        }
        
        enum UserToggle {
            static let p_button_off = ":P_button_off"
            static let p_button_on = ":P_button_on"
            static let ev_button_off = ":ev_button_off"
            static let ev_button_on = ":ev_button_on"
            static let petrol_button_off = ":petrol_button_off"
            static let petrol_button_on = ":petrol_button_off"
        }
        
        enum UserPopup {
            static let travel_time = "travel_time"
            static let projected_to_dhu = "projected_to_dhu"
            static let navigation_notification = "navigation_notification"
            static let no_parking_vicinity = "no_parking_vicinity"
            static let no_petrol_vicinity = "no_petrol_5km"
            static let no_ev_vicinity = "no_ev_5km"
            static let reached_destination = "reached_destination"
        }
    }
    
    enum Transactions {
        static let screen_view = "[trip_log]"
        
        enum UserClick {
            static let transactions_tab = ":transactions_tab"
            static let transactions_close = "[transactions]:close"
            static let transactions_vehicle_dropdown = "[transactions]:vehicle_dropdown"
            static let transactions_select_vehicle = "[transactions],[choose_vehicle]:select_vehicle"
            static let transactions_choose_vehicle_cancel = "[transactions],[choose_vehicle]:cancel"
            static let transactions_choose_vehicle_done = "[transactions],[choose_vehicle]:done"
            static let transactions_start_to_pair = "[transactions]:start_to_pair"
            
            static let filter_date = "[transactions]:filter_date"
            static let filter_type = "[transactions]:filter_transaction_type"
            static let filter_type_close = "[transactions],[filter_transaction_type]:close"
            static let filter_type_all = "[transactions],[filter_transaction_type]:all"
            static let filter_type_erp_only = "[transactions],[filter_transaction_type]:erp_only"
            static let filter_type_parking_only = "[transactions],[filter_transaction_type]:parking_only"
        }
    }
    
    enum SystemAnalytics {
        
        static let screen_view = "[app]"
        
        enum SystemEvent {
            static let obu_connecting = "obu_connecting"
            static let obu_connect_failed = "obu_connect_failed"
            static let obu_disconnect = "obu_disconnect"
            static let obu_card_status = "obu_card_status"
            static let obu_connected = "obu_connected"
            static let obu_verbose_error = "obu_verbose_error"
            static let carconnect = "carconnect"
            static let cardisconnect = "cardisconnect"
            static let app_start = "app_start"
            static let app_terminated = "app_terminated"
            static let app_foreground = "app_foreground"
            static let app_background = "app_background"
            static let sdk_init_success = "sdk_init_success"
            static let sdk_init_failure = "sdk_init_failure"
            static let bottom_obu_card_shown = ":bottom_obu_card_shown"
        }
    }
    
    enum RoutePlanningMode {
        
        static let screen_view = "[routeplanning]"
        
        enum UserClick {
            static let popup_no_drivable_route_got_it = "[popup_no_drivable_route]:got_it"
        }
        
        enum UserPopup {
            static let no_drivable_route = "no_drivable_route"
        }
        
        enum LocationData {
            static let no_drivable_route_origin = "no_drivable_route_origin"
            static let no_drivable_route_destination = "no_drivable_route_destination"
        }
    }
    
    enum DriveTrend {
        
        static let screen_view = "[drive_trend]"
        
        enum UserClick {
            static let driving_time_back = "[summary_tab],[driving_time_read_more]:back"
            static let total_parking_fee_back = "[summary_tab],[total_parking_fee_read_more]:back"
            static let total_erp_fee_back = "[summary_tab],[total_erp_read_more]:back"
        }
    }
    
    enum BreezeSettings {
        static let screen_view = "[breeze_settings]"
    }
    
    //commom parameters
    static let signin = "signin"
    static let signin_method = "signin_method"
    static let popup_name = "popup_name"
    static let popup_open = "popup_open"
    static let email   = "email"
    static let password = "password"
    static let next = "next"
    static let newPwd = "new_password"
    static let otp = "otp"
    static let resend = "resend"
    static let open = "open"
    static let close = "close"
    static let name = "name"
    static let term_of_service = "term_of_service"
    static let privacy_policy = "privacy_policy"
    static let agreed_radio = "agreed_radio"
    static let back = ":back"
    static let user_notification = "user_notification"
    static let mute = "mute"
    static let unmute = "unmute"
    static let save = "save"
    static let cancel = "cancel"
    static let time = "event_ts"
    static let popupOpen = ":popup_open"
    static let popupClose = ":popup_close"
    static let dhuPopupOpen = "[dhu]:popup_open"
    static let dhuPopupClose = "[dhu]:popup_close"
    
    //Data Usage Parameters
    static let batteryValue = "battery_value"
    
    //OnBoarding_signin parameters
    static let screenName = "onboarding_signin"
    static let screenNameFP_step1 = "onboarding_signin_forget_password_step1"
    static let screenNameFP_step2 = "onboarding_signin_forget_password_step2"
    static let screeNameFP_step3 = "onboarding_signin_forget_password_step3"
    static let FPSuccess = "onboarding_signin_forget_password_success"
    
    //OnBoarding_create account parameters
    static let create_Account = "onboarding_create_account"
    static let createAccount_step1 = "onboarding_create_account_step1"
    static let createAccount_step2 = "onboarding_create_account_step2"
    static let creatAccount_success = "onboarding_signup_success"

    
    static let signin_facebook = "facebook"
    static let signin_google  = "google"
    static let signin_apple = "apple"
    static let signin_breeze = "breeze"
    static let login = "login"
    static let logout = "logout"
    static let forget_password = "forget_password"
    
    //Home
    static let home_screen_name = "home"
    static let home_calendar_permission = "home_calendar_permission"
    static let menu = "menu"
    static let recenter = "recenter"
    static let select_recommended_destination = "select_recommended_destination"
    static let search = "search"
    static let map_interaction = "map_interaction"
    static let pinch_map = "pinch_map"
    static let drag_map = "drag_map"
    static let rotate_map = "rotate_map"
    static let swipe_card = "swipe_card"
    static let swipe = "swipe"
    
    //Home_menu
    static let home_menu = "home_menu"
    static let favorite  = "favorite"
    static let travel_log = "travel_log"
    static let settings  = "settings"
    
    //Home search
    static let home_search = "home_search"
    static let search_address_input  = "search_address_input "
    static let search_address_clear = "search_address_clear"
    static let favourite_select  = "favourite_select"
    static let favourite_more = "favourite_more"
    static let recent_search_select = "recent_search_select"
    static let recent_search_clear = "recent_search_clear"
    static let recent_search_single_clear = "recent_search_single_clear"
    
    //ObuLite
    static let obu_lite_mode = "obu_lite_mode"
    
    //Home Cruise mode
    static let home_cruise_mode = "home_cruise_mode"
    static let drop_down_notification = "drop_down_notification"
    static let notification_open = "notification_open"
    static let notification_close = "notification_close"
    
    //Statistics data
    static let batteryLevel = "battery_level"
    static let dataSent = "data_sent"
    static let dataReceived = "data_received"
    
    //Location data
    static let locationName = "location_name"
    static let latitude = "latitude"
    static let longitude = "longitude"
    
    //USer Selection
    static let selection_value = "selection_value"
    
    //Navigation
    static let navigation = "navigation"
    static let navigation_completed = "navigation_end"
    static let navigation_mute = "mute"
    static let navigation_munute = "unmute"
    static let navigation_recenter = "recenter"
    static let navigation_expand = "expand_instructions"
    static let navigation_collapse = "collapse_instructions"
    static let navigation_user_end = "user_end"
    static let navigation_pop_cancel = "pop_cancel"
    static let navigation_pop_end = "pop_end"
    
    
    //Favourites
    static let favourites_screen_name = "favourites"
    static let favourites_delete_confirmation_open = "favourite_delete_confirmation"
    static let favourites_delete_confirmation_close = "favourite_delete_confirmation"
    static let favourites_select = "select_favourite"
    static let favourites_add_new = "add_new"
    static let favourites_swipe = "ios_swipe_select"
    static let favourites_swipe_edit = "ios_swipe_edit"
    static let favourites_swipe_delete = "ios_swipe_delete"
    
    static let favourites_add_new_screen = "favourites_add_new"
    static let favourites_search = "search_address_input"
    static let favourites_select_address = "select_address"
   
    static let favourites_edit_screen = "favourites_edit"
    static let favourites_edit_name = "favourite_name_input"
    static let favourites_name_clear = "favourite_name_clear"
    
    static let favourites_new_screen = "favourites_new_favourite_details"
    
    //Settings
    static let settings_screen = "settings"
    static let settings_account = "account"
    static let settings_map_display = "map_display"
    static let settings_route = "ruote_preference"
    static let settings_privacy = "privacy_policy"
    static let settings_terms = "terms_condition"
    static let settings_faq = "faq"
    static let settings_feedback = "feedback"
    static let settings_about = "about"
    
    static let settings_account_screen = "settings_account"
    static let settings_account_logout = "logout"
    static let settings_account_name = "name"
    static let settings_account_password = "password"
    
    static let settings_account_reset_name_screen = "settings_account_reset_name"
    static let settings_account_reset_name_input = "name_input"
    
    static let settings_account_reset_pw_screen = "settings_account_reset_password"
    static let settings_account_reset_pw_old_input = "old_password_input"
    static let settings_account_reset_pw_new_input = "new_password_input"
    static let settings_account_reset_pw_hideunhide = "hide_unhide_password"
    static let settings_account_reset_pw_forget = "forget_password"
    static let settings_account_reset_pw_done = "done"
    
    static let settings_map_display_screen = "settings_map_display"
    static let settings_map_display_auto = "auto"
    static let settings_map_display_light = "light"
    static let settings_map_display_dark = "dark"
    
    static let settings_route_screen = "settings_route_preference"
    static let settings_route_fast = "fastest"
    static let settings_route_cheap = "cheapest"
    
    static let settings_policy_screen = "settings_privacy_policy"
    static let settings_terms_screen = "settings_terms_conditions"
    static let settings_faqs_screen = "setting_faqs"
    static let settings_about_screen = "settings_about"
    
    
    //TravelLog
    static let travellog_screen = "Travel_log"
    static let travellog_select = "select"
    static let travellog_multiselect = "month_select_travel_log"
    static let travellog_singleselect = "single_select_travel_log"
    static let travellog_email = "email_travel_summary"
    static let travellog_back_to_top = "back_to_top"
    static let travellog_view_trip = "view_single_trip"
    
    static let travellog_email_success = "email_travel_summary_success_ok"
    
    static let travellog_summary_screen = "Travel_log_single_trip_summary"
    static let travellog_edit_erp = "edit_erp_price_input"
    static let travellog_edit_parking = "edit_parking_expense_input"
    static let travellog_add_aprking_img = "add_parking_img"
    static let travellog_take_photo = "take_photo"
    static let travellog_select_from_gallery = "select_from_gallery"
    static let travellog_retake_photo = "retake_photo"
    static let travellog_reselect_from_gallery = "reselect_from_gallery"
    static let travellog_delete_receipt = "delete_receipt"
    static let travellog_preview_receipt = "preview_parking_receipt"
    static let travellog_confirm_delete_receipt = "delete_parking_receipt_confirmation"
    
    
    //Route planning
    static let routeplanning_screen = "Route Planning"
    static let routeplanning_screen_edit = "edit_route_planning"
    static let routeplanning_carpark_nearby = "carpark_nearby"
    static let routeplanning_reset_carparks = "reset_carpark"
    static let routeplanning_lets_go = "lets_go"
    static let routeplanning_edit_fave = "edit_favourite"
    static let routeplanning_add_remove_fave = "add_remove_favourite"
    static let routeplanning_tap_route = "tap_route"
    static let routeplanning_swipe_card = "swipe_card"
    static let routeplanning_carpark_screen = "route_planning_carpark"
    static let routeplanning_tap_carpark = "tap_carpark"
    static let routeplanning_navigation_here = "navigation_here"
    static let routeplanning_route_fast = "fastest"
    static let routeplanning_route_cheap = "cheapest"
    static let routeplanning_route_short = "shortest"
    
    // MARK: - ETA    
    static let eta_contact_screen = "search_contact"
    static let eta_contact_select = "contact_select"
    static let eta_contact_cancel = "cancel"
    // Not implemented
    static let eta_recent_contact_select = "recent_contact_select"
    static let eta_contact_search = "search_contact"
    static let eta_screen_name = "share_my_eta"
    static let eta_message = "message"
    static let eta_contact_remove = "contact_remove"
    static let eta_share_my_live_location_select = "share_my_live_location_select"
    static let eta_share_my_live_location_unselect = "eta_share_my_live_location_unselect"
    static let eta_send_auto_voice_message_select = "send_auto_voice_message_select"
    static let eta_send_auto_voice_message_unselect = "send_auto_voice_message_unselect"
    
    //  MARK: - POI Navigation
    static let amenityId = "amenity_id"
    static let layerCode = "layer_code"
    static let amenityType = "amenity_type"
    
    
   
}

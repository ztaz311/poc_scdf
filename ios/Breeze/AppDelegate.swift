//
//  AppDelegate.swift
//  Breeze
//
//  Created by VishnuKanth on 18/11/20.
//

import UIKit
import Amplify
import Firebase
import AWSMobileClient
import Instabug
import MapboxCoreNavigation
import MapboxNavigation
import CarPlay
import SwiftyBeaver
import MapboxMaps
import React
import Combine
import AVFAudio
import AVFoundation
import MetricKit
import FirebaseDynamicLinks
import Foundation

//@_implementationOnly import MapboxCommon_Private

extension UserDefaults {
    @objc dynamic var isSignedIn: Bool {
        return bool(forKey: Values.isLoggedIn)
    }
}

enum AppState {
    case initial
    case loggedIn
    case home
    case cruise(Favourites?)
    case routePlanning(DeviceType)
    case navigation(DeviceType, MapboxNavigationService?, TripETA?, Carpark?)
}
var remoteConfig = RemoteConfig.remoteConfig()

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,RCTBridgeDelegate {
    
    var forceSignOut: Bool = false
    weak var currentAppRootViewController: UIViewController?
    
    var appCoordinate: AppCoordinateProtocol = AppCoordinate.shared

    var window: UIWindow?
    var isTermsClicked = false
    var backendURL = ""
    var policyURL = ""
    var deviceTokenStr = ""
    weak var navigationController: TurnByTurnNavigationVC?  // TODO: Should remove this
    private var subscriber: AnyCancellable?
    var signInCancellable: AnyCancellable?
    var themeUpdateCancellable: AnyCancellable?
//    var overSpeedCancellable: AnyCancellable?
//    var navigationOverSpeedCancellable: AnyCancellable?
//    var cruiseOverSpeedAnimatedCount = 0
//    var navigationOverSpeedAnimatedCount = 0
//    var mapboxLog: MapboxLog!
    var trip: CPTrip?

    // MARK: - CarPlay related properties
    lazy var carPlaySearchController: SearchController = SearchController()
    lazy var carPlayManager: CarPlayManager = CarPlayManager()
    var signInAlertTemplate: CPAlertTemplate?
    var cpBarNavButtons:CPNavigationButtons = CPNavigationButtons()
    var cpBarBrowseButtons:CPBrowsingNavigationButtons = CPBrowsingNavigationButtons()
    var cpBarPreviewButtons:CPPreviewNavigationButtons = CPPreviewNavigationButtons()

    @Published var mapLandingViewModel: MapLandingViewModel?
    @Published var navigationViewModel: TurnByTurnNavigationViewModel?
    @Published var routePlanningViewModel: RoutePlanningViewModel?
    @Published var cruiseViewModel: CruiseViewModel?
    @Published var isObuLiteDisplay: Bool?
    @Published var needShowMinimumCashcardBalance: Bool?

    @Published var receivedFromRemoteConfig = false
    var fbRemoteCancellable: AnyCancellable?

    @Published var carPlayConnected = false
    var appInBackground = false     // this is for mobile
    var carplayInBackground = true  // by default carplay is in background
    var navigationRootTemplate:CPMapTemplate!
    
    var updateMapCancellable: AnyCancellable?
    var carRPController:CarPlayRPController?
    
    //MARK: - cor Carpark
    var cpCarparkVC: CPCarParkVC?
    var cpAmenityVC: CPAmenityVC?
    var cpRPCarparkVC: CPRoutePlanningCarParkVC?
    var carPlayCarParkNavigation: CPNavigationModeCarpark?
    var carPlayCarParkCruise: CPCruiseModeCarPark?
    var insideCP = false
    var lastERPRefreshTime = 0.0
    // MARK: - App State related
    @Published var appState: AppState = .initial
    var previousAppState: AppState = .initial
    var appStateCancellable: AnyCancellable?
    var carPlayMapController: CarPlayMapController?
    var isCarPlayPreview = false
//    var currentSelectedRouteIndex: Int = -1
    var notifyPreview = false
    var tripETA:TripETA?
    var userName = "UnknownUser"
    @Published var aps: [String : AnyObject]? // keep aps data when launched    
    
    // MARK: - MetricKit
    private var appMetricManager: AppMetricManager?
    
    var disposables = Set<AnyCancellable>()
    
    let audioDispatchQueue = DispatchQueue(label: "breeze-speech-AVAudioSession")
    
    weak var rivaSpeech: RivaSpeechSynthesizer?
    
    
    var chargeNotificationOBU: BreezeToast?
    var cruiseEndDate: Date?
    let ENABLE_CRUISE_DURATION: TimeInterval = 3600 //  In 1 hour
    
    func sourceURL(for bridge: RCTBridge!) -> URL! {
            #if DEBUG || DEMO
                  //return URL(string: "http://localhost:8088/index.bundle?platform=ios")
                return RCTBundleURLProvider.sharedSettings().jsBundleURL(forBundleRoot: "index", fallbackResource: nil)
            #else
                return Bundle.main.url(forResource: "main", withExtension: "jsbundle")
            #endif
        }


    func findAndShowAppUpdateAlerts(controller:UIViewController){
        
        self.fetchRemoteConfigAndActivate()
        fbRemoteCancellable = self.$receivedFromRemoteConfig.sink { [weak self] isReceivedRemoteConfig in
            guard let self = self else { return }
            
            if isReceivedRemoteConfig {
                self.getAppUpdateAlerts(controller: controller)
                
                //  Handle show maintenance
                if AppUpdater.shared.isMaintenancing() {
                    self.showMaintenancePage()
                }
            }
        }
    }
    
    func showMaintenancePage() {
        DispatchQueue.main.async {
            var message = AppUpdater.shared.getMaintenanceMessage()
            if message.isEmpty {
                message = "Application is under maintenance now. Kindly check back after 10 minutes."
            }
            if let nav = appDelegate().baseNavigationController {
                _ = nav.popToRootViewController(animated: false)
                
                //  Present maintenance page
                if let controller = UIStoryboard(name: "Maintenance", bundle: nil).instantiateViewController(withIdentifier: "NewMaintenanceVC") as? NewMaintenanceVC {
                    controller.message = message
                    nav.pushViewController(controller, animated: false)
                }
                
            }
        }
    }
    
    func getAppUpdateAlerts(controller:UIViewController,appUptoDate:Bool = false,fromNotification:Bool = false){
        
        DispatchQueue.main.async {
         
            if(!fromNotification){
                /// Show mandatory app update alert in both MapLanding and LandingViewController
                if(controller .isKind(of: MapLandingVC.self) || controller.isKind(of: LandingViewController.self))
                {
                    self.receivedFromRemoteConfig = false
                    //// mandatory app update checks
                    if(AppUpdater.shared.checkAppUpdateMandatoryVersion()){
                        AppUpdater.shared.showAppUpdateMandatoryAlert(controller: controller)
                    }
                    else{
                        ///optional app update checks
                        ///Show optional app update only in MapLanding
                        if(controller .isKind(of: MapLandingVC.self) || controller.isKind(of: LandingViewController.self)){
                            self.findAndShowAppOptionalUpdateAlerts(controller: controller)
                        }
                        
                    }
                }
            }
            else{
                
                if(AppUpdater.shared.checkAppUpdateMandatoryVersion()){
                    AppUpdater.shared.showAppUpdateMandatoryAlert(controller: controller)
                }
                else{
                    
                    if(AppUpdater.shared.isSameAppversion()){
                        
                        AppUpdater.shared.showAppUpToDateAlert(controller: controller)
                    }
                }
                
            }
            
        }
    }
    
    func findAndShowAppOptionalUpdateAlerts(controller:UIViewController){
        
        //optional app update checks
        if(controller .isKind(of: MapLandingVC.self)) {
            if(AppUpdater.shared.checkAppOptionalUpdateVersion()){
                self.receivedFromRemoteConfig = false
                AppUpdater.shared.showAppOptionalUpdateAlert(controller: controller)
            }
        }
        
    }
    
    func fetchRemoteConfigAndActivate(){
        
        remoteConfig.fetch { (status, error) -> Void in
            if status == .success {
                print("Config fetched!")
                remoteConfig.activate { changed, error in
                    self.receivedFromRemoteConfig = true
                }
            } else {
                print("Config not fetched")
                print("Error: \(error?.localizedDescription ?? "No error available.")")
            }
            
        }
        
    }
    
    func handleIncommingDynamicLink(_ dynamicLink: DynamicLink) {
        guard let url = dynamicLink.url else {
            print("Thats weird. My dynamic link object has no url")
            return
        }
        print("Your incoming link parameter is \(url)")
        
        let currentAppBuildNumber = Int(UIApplication.buildString()) ?? 0
        let minimumVersion = Int(dynamicLink.minimumAppVersion ?? "0") ?? 0
        if currentAppBuildNumber < minimumVersion {
            AppUpdater.shared.redirectToAppDownloadPage()
        }else {
            guard let components = URLComponents(url: url, resolvingAgainstBaseURL: false), let queryItems = components.queryItems else {return}
            print("Deeplink: \(url.absoluteString)")
            
            for queryItem in queryItems {
                print("Parameter: \(queryItem.name) has a value of \(queryItem.value ?? "")")
                if queryItem.name == "token", let token = queryItem.value, !token.isEmpty {
                    
                    if url.absoluteString.localizedCaseInsensitiveContains("location") || url.absoluteString.localizedCaseInsensitiveContains("destination") {
                        saveMapLocation(token)
                    } else {
                        saveMapCollection(token)
                        AnalyticsManager.shared.logEvent(eventName: ParameterName.map_interaction, eventValue: ParameterName.drag_map, screenName: ParameterName.favourites_edit_screen, amenityId: ParameterName.amenityId, layerCode:  ParameterName.layerCode, amenityType: ParameterName.amenityType)
                    }
                    
                    break
                }
            }
        }
    }
        
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
                                        
        Prefs.shared.setValue(true, forkey: .shouldLaunchCruiseWhenOverSpeed)
        
        RNViewManager.sharedInstance.createBridgeIfNeeded();
        
        carPlayManager.carPlayNavigationViewController?.annotatesIntersectionsAlongRoute = false
        //By default we will keep AVAudioSession active to true
        _ = AVAudioSession.sharedInstance().setAudioSessionActive()
        
        ReachabilityManager.shared.startMonitoring(withLog: false)
        AnalyticsManager.shared.setUp()
        Prefs.shared.setValue(true, forkey: .isOnboardingShown)
        UserDefaults.standard.set(true, forKey: Values.isAppLaunch)
        
//        FirebaseOptions.defaultOptions()?.deepLinkURLScheme = "breezedevapp.page.link"
        FirebaseApp.configure()
        Firebase.Analytics.setAnalyticsCollectionEnabled(true)
        backendURL = getBackendURL("Backend URL") ?? ""
        policyURL = getPolicyURL("Policy URL") ?? ""
        
        // version check. Clear cache if needed
        DispatchQueue.global().async { [weak self] in
            guard let self = self else { return }
            self.checkVersion()
        }
        setUpFirebaseRemoteConfig()
        
        subscriber = UserDefaults.standard
            .publisher(for: \.debugEnabled)
            .sink() {[weak self] enabled in
                if enabled {
                    self?.setupLogger(enabled: enabled)
                } else {
                    let debugRemotely = UserDefaults.standard.bool(forKey: Values.debugEnabledRemotely)
                    self?.setupLogger(enabled: debugRemotely)
                }
        }
        
        ///fetching push notification device tokem from user defaults
        let deviceToken = (UserDefaults.standard.value(forKey: Values.push_notification_token) as? String) ?? ""
        if(deviceToken != ""){
            self.deviceTokenStr = deviceToken
        }
        
        setupHistory()

        signInCancellable = AWSAuth.sharedInstance.$isSignedIn.sink { [weak self] isSignedIn in
            guard let self = self else { return }
            
            if isSignedIn {
                SwiftyBeaver.debug("AppDelegate SIgninCancellable")
                AWSAuth.sharedInstance.fetchAttributes { result in
                    self.userName = AWSAuth.sharedInstance.userName.isEmpty ? "UnknownUser" : AWSAuth.sharedInstance.userName
                    self.updatePendingHistoryData()
                    self.sendLogIfNeed()
                } onFailure: { error in
                    SwiftyBeaver.error("fetchAttributes error: \(error.localizedDescription)")
                }
                self.enterLogin()
            } else {
                self.enterLogout()
            }
            self.updateDataCenter(isLoggedIn: isSignedIn)
            self.updateCarPlayLoggedInStatus(loggedIn: isSignedIn)
        }
        
        // Monitor the settings change only
        themeUpdateCancellable = Settings.shared.themeUpdatedAction.receive(on: DispatchQueue.main).sink { [weak self] _ in
            guard let self = self else { return }
            if let carPlayMapViewController = self.carPlayManager.carPlayMapViewController {
                carPlayMapViewController.didThemeUpdate()
            }
            if let carPlayNavigationViewController = self.carPlayManager.carPlayNavigationViewController {
                carPlayNavigationViewController.didThemeUpdate()
            }
        }

        // monitor status
        AWSAuth.sharedInstance.startMonitoring()

        SwiftyBeaver.debug("startDataCenter")
        DataCenter.shared.setup()
        
        startAppStateMonitor()
        
        // clear the push notification badge
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        // TODO: will change later
//        UIApplication.shared.statusBarStyle = .darkContent
        
        registerForPushNotifications()
        // allow showing push notification in foreground
        UNUserNotificationCenter.current().delegate = self
        
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) { (granted, error) in
            // Handle authorization result if needed
        }
        
        //Comment this line while running in Car Play. There are was some issue with Instabug invoking with UIApplicationMain
        DispatchQueue.main.async {
            self.instaBugCustomisation()
        }
        
        AnalyticsManager.shared.logSystemEvent(eventValue: ParameterName.SystemAnalytics.SystemEvent.app_start, screenName: ParameterName.SystemAnalytics.screen_view)
       
        // launchOptions?[UIApplication.LaunchOptionsKey.remoteNotification] is always nil, we can't rely on this
        // instead should always use userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response:
        // TODO: Remove this!!!
        if let notification = launchOptions?[.remoteNotification] as? [String: AnyObject] {
            let isInstabugNotification = Replies.didReceiveRemoteNotification(notification)
        }
                               
        #if STAGING || BETARELEASE
        appMetricManager = AppMetricManager()
        #endif
        SwiftyBeaver.info("didFinishLaunchingWithOptions")
        
        registerNotifications()
        return true
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?,
                     annotation: Any) -> Bool {
      if let dynamicLink = DynamicLinks.dynamicLinks().dynamicLink(fromCustomSchemeURL: url) {
          self.handleIncommingDynamicLink(dynamicLink)
        return true
      }
      return false
    }
    
    func getETAFav(){
        
        DispatchQueue.global(qos: .background).async {
            
            ETAFavList.shared.getETAFavList() { [weak self] (result) in
                guard self != nil else { return }
//                print(result)
                switch result {
                case .success(let json):
                    DispatchQueue.main.async {
                        SavedSearchManager.shared.etaFav = json
                        
                        
                    }
                    
                    
                case .failure(_):
                    DispatchQueue.main.async {
                        if(SavedSearchManager.shared.etaFav.count > 0)
                        {
                            SavedSearchManager.shared.etaFav.removeAll()
                            
                        }
                        
                    }
                }
            }
        }
    }
    
    func getBackendURL(_ key: String) -> String? {
            return (Bundle.main.infoDictionary?[key] as? String)?
                .replacingOccurrences(of: "\\", with: "")
     }
    
    func getPolicyURL(_ key: String) -> String? {
        
        return (Bundle.main.infoDictionary?[key] as? String)?
            .replacingOccurrences(of: "\\", with: "")
    }
    
    func startNavigationRecordingHistoryData(){
        RouteController.startRecordingHistory()
    }
    
    func startCruiseRecordingHistoryData(){
        PassiveLocationManager.startRecordingHistory()
    }
    
    func navigationSubmitHistoryData(description:String){
//        var userName = AWSAuth.sharedInstance.userName
//        if userName.isEmpty {
//            userName = "UnknownUser"
//        }
        RouteController.stopRecordingHistory { [weak self] historyFileURL in
            guard let self = self, let url = historyFileURL else { return }
            
            self.submitHistory(description: description, url: url)
        }
                    
    }

    func cruiseSubmitHistoryData(description:String){
//        var userName = AWSAuth.sharedInstance.userName
//        if userName.isEmpty {
//            userName = "UnknownUser"
//        }
        PassiveLocationManager.stopRecordingHistory { [weak self]  historyFileURL in
            
            guard let self = self, let url = historyFileURL else { return }
            self.submitHistory(description: description, url: url)
        }
            
    }
    
    private func checkVersion() {
        let currentVersion = UIApplication.appVersion()
        let version = UserDefaults.standard.value(forKey: "InstalledAppVersion") as? String ?? ""
        if currentVersion != version {
            // clear mapbox cache
            let fileManager = FileManager.default
            
            guard let basePath = fileManager.urls(for: .cachesDirectory, in: .userDomainMask).first,
                  let identifier = Bundle.mapboxNavigation.bundleIdentifier else { return }
            let cacheURL = basePath.appendingPathComponent(identifier + ".downloadedFiles")
            
            // check folder exist or not
            var isDir : ObjCBool = false
            if fileManager.fileExists(atPath: cacheURL.path, isDirectory: &isDir) {
                if isDir.boolValue {
                    do {
                        try fileManager.removeItem(at: cacheURL)
//                        SwiftyBeaver.info("================> Cache cleared: \(cacheURL)")
                    } catch {
        //                    SwiftyBeaver.error("================> Failed to remove cache dir")
                    }
                }
            }
            UserDefaults.standard.set(currentVersion, forKey: "InstalledAppVersion")
        }
    }
    
    private func setupLogger(enabled: Bool) {
        
        let log = SwiftyBeaver.self

        let console = ConsoleDestination()  // log to Xcode Console
        log.addDestination(console)
        
        if enabled {
            let file = FileDestination()  // log to default swiftybeaver.log file
            file.format = "$Dyyyy-MM-dd HH:mm:ss.SSS$d $T $L: $M"

//            let fm = FileManager.default
            if let logurl = FileHelper.getLogURL() {
                file.logFileURL = logurl
                log.addDestination(file)
                
                log.info("Log saved at \(logurl)")
            }
            else{
                SwiftyBeaver.debug("Logger Documents directory url failed")
            }
            

            // This is needed if we want to enable Mapbox internal log
//            mapboxLog = MapboxLog()
//            LogConfiguration.getInstance().registerLogWriterBackend(forLogWriter: mapboxLog)
//            Log.info(forMessage: "Start mapbox log", category: "Mapbox")
        }
    }
    
    func sendLogIfNeed() {
        if let logURL = FileHelper.getLogURL(),
           let fileSize = FileHelper.fileSize(atURL: logURL),
           fileSize > 0 {
            DispatchQueue.global().async {
                self.sendLog(filePath: logURL)
            }
        }
    }
    
    // send and delete
    func sendLog(filePath: URL) {
        // send
        var userName = "\(AWSAuth.sharedInstance.cognitoId)"
        if userName.isEmpty {
            userName = "UnknownUser"
        }
        DispatchQueue.global(qos: .background).async {
            FirebaseFeedbackService().submit(userId: userName, description: "applaunch", fromLog: true, url: filePath) {_ in
                SwiftyBeaver.debug("log sent progress after app launch")
            } completion: { result in
                switch result {
                case .failure(let error):
                    SwiftyBeaver.error("log sent after app launch for \(userName) \(error)")
                    
                case .success(_):
                    SwiftyBeaver.info("log sent after app launch for \(userName)")
                    // delete after send
                    try? FileManager.default.removeItem(at: filePath)
                    break
                }
            }
        }
    }
    
    private func setupHistory() {
        
        do {
            // Put the code here is important since it needs to be run before RouteController is initialized.
            let appUrl = try FileManager.default.url(for:.documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            // TODO: I added a sub folder for history data. We may need to do house keep to delete all old history data in app sandbox later.
            let historyUrl = appUrl.appendingPathComponent("history")
            // create the folder if need
            var isDir : ObjCBool = false
            if !FileManager.default.fileExists(atPath: historyUrl.path, isDirectory: &isDir) {
                try FileManager.default.createDirectory(at: historyUrl, withIntermediateDirectories: false, attributes: nil)
            }
            RouteController.historyDirectoryURL = historyUrl

            // Enable json format for historical data
            let historyConfig = [
                customConfigFeaturesKey: [
//                "usePbf": false,
                "historyAutorecording": true
                ],
                //"navigation":["mapMatcher":["emissionCost":["routeLine":["maxBias": 20]]]]  // #159 - for rerouting issue
            ]
            UserDefaults.standard.set(historyConfig, forKey: "com.mapbox.navigation.custom-config")
            
            SwiftyBeaver.info("History data will be saved at \(historyUrl)")

        } catch (let error) {
            SwiftyBeaver.debug("Failed to enable history data - \(error.localizedDescription)")
        }
    }
    
    func registerForPushNotifications() {
//      //1
      UNUserNotificationCenter.current()
        //2
        .requestAuthorization(options: [.alert, .sound, .badge]) { granted, _ in
          //3
          print("Permission granted: \(granted)")
            DispatchQueue.main.async {
                UIApplication.shared.registerForRemoteNotifications()
            }
        
        }
        
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        deviceTokenStr = deviceToken.map { String(format: "%02.2hhx", $0) }.joined()
        print("DEVICE TOKEN",deviceTokenStr)
    
        /// Save device token to user defaults when ever we receive token from this method
        UserDefaults.standard.setValue(deviceTokenStr, forKey: Values.push_notification_token)
        
        
        let nc = NotificationCenter.default
        nc.post(name: Notification.Name(Values.deviceToken), object: nil)
        Replies.didRegisterForRemoteNotifications(withDeviceToken: deviceToken)
        
    }
        
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        let isInstabugNotification = Replies.didReceiveRemoteNotification(userInfo)
        
        guard let aps = userInfo["aps"] as? [String: AnyObject] else { return }
        if let alert = aps["alert"] as? [String: AnyObject] {
            let title = alert["title"]
            let body = alert["body"]
            
            print("\(title):\(body)")
        }
        

        completionHandler(.noData)
    }
        
    func application(_ application: UIApplication,
                didFailToRegisterForRemoteNotificationsWithError
                    error: Error) {
        SwiftyBeaver.debug("Fail to register push notification \(error)")
    }
    
    func setInstabugToken(){
        
        #if BETARELEASE
            //QA
            Instabug.start(withToken:"98724d29eb96f6471e21e54804855adc", invocationEvents: [.screenshot])
                
        #else
        #if APPSTORE
           //Prod
            Instabug.start(withToken:"bf1f929fa04c88c69631ee4b87d658b6", invocationEvents: [.screenshot])
        #else
            //Dev
            Instabug.start(withToken:"8f2647a564da3b46df6334d62aa829e2", invocationEvents: [.screenshot])
        #endif
        #endif
    }
    
    func instaBugCustomisation(){
        
        setInstabugToken()
        Surveys.appStoreURL = "https://apps.apple.com/sg/app/breeze-drive-park/id1614397931"
        BugReporting.shouldCaptureViewHierarchy = true
        BugReporting.enabledAttachmentTypes = [.extraScreenShot,.galleryImage,.screenRecording,]
        
        
        Instabug.setColorTheme(.light)
        Instabug.tintColor = .brandPurpleColor

        Instabug.welcomeMessageMode = IBGWelcomeMessageMode.disabled

        Instabug.setValue(Constants.ncsIBReportBugString, forStringWithKey: kIBGReportBugStringName)
        Instabug.setValue(Constants.ncsIBReportBugDescriptionString, forStringWithKey: kIBGReportBugDescriptionStringName)

        Instabug.setValue(Constants.ncsIBFeedbackString, forStringWithKey: kIBGReportFeedbackStringName)
        Instabug.setValue(Constants.ncsIBFeedbackDescriptionString, forStringWithKey: kIBGReportFeedbackDescriptionStringName)

        Instabug.setValue(Constants.ncsIBAskQuestionString, forStringWithKey: kIBGAskAQuestionStringName)
        Instabug.setValue(Constants.ncsIBAskQuestionDescriptionString, forStringWithKey: kIBGReportQuestionDescriptionStringName)

        Instabug.setValue(Constants.ncsIBCommentFieldString, forStringWithKey: kIBGCommentFieldPlaceholderForBugReportStringName)
        Instabug.setValue(Constants.ncsIBCommentFieldString, forStringWithKey: kIBGCommentFieldPlaceholderForFeedbackStringName)
        Instabug.setValue(Constants.ncsIBCommentFieldString, forStringWithKey: kIBGCommentFieldPlaceholderForQuestionStringName)
        
        BugReporting.willInvokeHandler = {
            getInstabugCustomAttribute()
        }
        
    }
    
    func isSmallIphone() -> Bool {
        let screenSize = UIScreen.main.bounds.size
        let maxWidth = max(screenSize.width, screenSize.height)
        SwiftyBeaver.debug("Width log \(maxWidth)")
        return maxWidth <= 812 // Adjust this value as needed
    }
}

@available(iOS 13.0, *)
extension AppDelegate: UIWindowSceneDelegate {

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {

        print("scene connecting")
        if connectingSceneSession.role == .carTemplateApplication {
            return UISceneConfiguration(name: "BreezeCarPlayApplicationConfiguration", sessionRole: connectingSceneSession.role)
        }
        return UISceneConfiguration(name: "BreezeAppConfiguration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {

        //We don't need this function at the moment
    }
    
    func sceneDidBecomeActive(_ scene: UIScene) {
        
        ReachabilityManager.shared.startMonitoring(withLog: false)
        ReachabilityManager.shared.isReachable(success: {
            let status = LocationManager.shared.getStatus()
            if(status == .denied)
            {
               
                if let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PermissionNVC") as? PermissionNVC {
                    if let window = self.window, let rootViewController = window.rootViewController {
                        var currentController = rootViewController
                        while let presentedController = currentController.presentedViewController {
                            currentController = presentedController
                        }
                        if !(currentController is PermissionNVC){
                            controller.modalPresentationStyle = .overFullScreen
                            currentController.present(controller, animated: false, completion: nil)
                        }
                    }
                }
            }else{
                if let window = self.window, let rootViewController = window.rootViewController {
                    var currentController = rootViewController
                    while let presentedController = currentController.presentedViewController {
                        currentController = presentedController
                    }
                    if (currentController is PermissionNVC){
                        currentController.dismiss(animated: false, completion: nil)
                    }
                }
            }
            
          }, failure: {
            
              SwiftyBeaver.debug("Reachability status failed")
          })
        
    }
        
    func sceneDidEnterBackground(_ scene: UIScene) {
        SwiftyBeaver.info("sceneDidEnterBackground")
        
        AnalyticsManager.shared.logSystemEvent(eventValue: ParameterName.SystemAnalytics.SystemEvent.app_background, screenName: ParameterName.SystemAnalytics.screen_view)
        
        (UIApplication.shared.delegate as! AppDelegate).appInBackground = true
        if OBUHelper.shared.isObuConnected() {
            OBUHelper.shared.willTriggerAutoConnect = true
        }
//        appInBackground = true
        //ReachabilityManager.shared.stopMonitoring()
    }
    
    func sceneWillResignActive(_ scene: UIScene) {
        SwiftyBeaver.info("sceneWillResignActive")
    }
    
    func sceneWillEnterForeground(_ scene: UIScene) {
        SwiftyBeaver.info("sceneWillEnterForeground")
        
        AnalyticsManager.shared.logSystemEvent(eventValue: ParameterName.SystemAnalytics.SystemEvent.app_foreground, screenName: ParameterName.SystemAnalytics.screen_view)
        
        (UIApplication.shared.delegate as! AppDelegate).appInBackground = false
        //Check if app is in Signed In, to refresh erpTimer when app comes from background to foreground
        if(AWSAuth.sharedInstance.isSignedIn){
            
            //Get current time interval
            let now = Date().timeIntervalSince1970
            
            //Check current time interval - lastERPRefreshTime is greater 120 i.e 2mins then only we will refresh ERPTimer from DataCenter
            if((now - lastERPRefreshTime) > 120){
                
                SwiftyBeaver.debug("Appdelegate foreground calling ERP Refresh \(lastERPRefreshTime) \(now - lastERPRefreshTime)")
                lastERPRefreshTime = now
                DataCenter.shared.callERPRefreshTimer()
            }
            
        }
        
        //ReachabilityManager.shared.stopMonitoring()
    }
    
    func sceneDidDisconnect(_ scene: UIScene) {
        SwiftyBeaver.info("sceneDidDisconnect")
        
        self.navigationSubmitHistoryData(description: "Navigation-AppExit")
        self.cruiseSubmitHistoryData(description: "CruiseMode-AppExit")
        
        AnalyticsManager.shared.logSystemEvent(eventValue: ParameterName.SystemAnalytics.SystemEvent.app_terminated, screenName: ParameterName.SystemAnalytics.screen_view)
        
        NotificationCenter.default.post(name: Notification.Name(Values.notificationAppTerminate), object: nil)
    }
    
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        // Get URL components from the incoming user activity.
        guard let userActivity = connectionOptions.userActivities.first,
              userActivity.activityType == NSUserActivityTypeBrowsingWeb,
              let incomingURL = userActivity.webpageURL else {
            return
        }
        
        _ = DynamicLinks.dynamicLinks().handleUniversalLink(incomingURL) { (dynamiclink, error) in
            
            guard error == nil else {
                print("Found an error! \(error!.localizedDescription)")
                return
            }
            
            if let dynamicLink = dynamiclink {
                self.handleIncommingDynamicLink(dynamicLink)
            }
        }
        
    }
    
    func scene(_ scene: UIScene, continue userActivity: NSUserActivity) {
        guard let url = userActivity.webpageURL else {
            return
        }
        
        _ = DynamicLinks.dynamicLinks().handleUniversalLink(url) { (dynamiclink, error) in
            
            guard error == nil else {
                print("Found an error! \(error!.localizedDescription)")
                return
            }
            
            if let dynamicLink = dynamiclink {
                self.handleIncommingDynamicLink(dynamicLink)
            }
        }
    }
    
    func scene(_ scene: UIScene, openURLContexts URLContexts: Set<UIOpenURLContext>) {
        guard let url = URLContexts.first?.url else {
                return
            }
        
        if let scheme = url.scheme,
           scheme.localizedCaseInsensitiveCompare("breeze") == .orderedSame,
           let host = url.host {
            
            var parameters: [String: String] = [:]
            URLComponents(url: url, resolvingAgainstBaseURL: false)?.queryItems?.forEach {
                parameters[$0.name] = $0.value
                print("Parameters",parameters)
            }
            
            
            if(host == "verifyEmail")
            {
                
                if let topVC = UIApplication.getTopViewController() {
                    
                    if(topVC is UpdateEmailVC || topVC is ChangeViewContainer)
                    {
                        NotificationCenter.default.post(name: Notification.Name(Values.NotificationEmailVerify), object: nil,userInfo: ["OTP":parameters["OTP"]!])
                        return
                    }
                }
                
                if self.window?.rootViewController?.presentedViewController != nil {
                    self.window?.rootViewController?.dismiss(animated: false, completion: nil)
                }
                let navController = self.window?.rootViewController?.storyboard?.instantiateInitialViewController() as? UINavigationController
                let storyboard = UIStoryboard(name: "UserUpdate", bundle: nil)
                let storyboard1 = UIStoryboard(name: "Account", bundle: nil)
                
                if let vc = storyboard.instantiateViewController(withIdentifier: String.init(describing: ChangeViewContainer.self)) as? ChangeViewContainer {
                    
                    vc.otpCodeToVerify = parameters["OTP"]!
                    
                    if let vc1 = storyboard1.instantiateViewController(withIdentifier: String.init(describing: UserAccountContainer.self)) as? UserAccountContainer {
                        
                        navController?.pushViewController(vc1, animated: true)
                        navController?.pushViewController(vc, animated: true)
                        self.window?.rootViewController? = navController!
                    }
                }
            }else if host.lowercased() == "savecollection" { // token=x1234d
                if let token = parameters["token"], !token.isEmpty {
                    saveMapCollection(token)
                }
            }
        }
    }
    
    //TODO: Add subcategory
    private func handleLogNotification(aps: [String: AnyObject], isClick: Bool) {
        if let message = getAPNSMessage(aps: aps) {
            AnalyticsManager.shared.logNotificationStatisTics(eventName: ParameterName.OutsideApp.Statistics.message, data: message,screenName: ParameterName.OutsideApp.screen_view, eventValue: ParameterName.OutsideApp.UserClick.notification_click)
        }
        
        if let category = getAPNSType(aps: aps) {
            AnalyticsManager.shared.logNotificationStatisTics(eventName: ParameterName.OutsideApp.Statistics.main_category, data: category,screenName: ParameterName.OutsideApp.screen_view, eventValue: ParameterName.OutsideApp.UserClick.notification_click)
        }
        
        let nameClick = isClick ? ParameterName.OutsideApp.Statistics.date_clicked : ParameterName.OutsideApp.Statistics.date_pushed
        AnalyticsManager.shared.logNotificationStatisTics(eventName: nameClick, data: DateUtils.shared.getTimeDisplay(dateFormat: Date.formatYYYYMMddHHmm, date: Date()),screenName: ParameterName.OutsideApp.screen_view, eventValue: ParameterName.OutsideApp.UserClick.notification_click)
    }
    
}

extension AppDelegate: UNUserNotificationCenterDelegate {
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.OutsideApp.UserClick.notification_click,
                                              screenName: ParameterName.OutsideApp.screen_view)
        
        if let aps = response.notification.request.content.userInfo["aps"] as? [String: AnyObject] {
            handleLogNotification(aps: aps, isClick: true)
        }
        guard let navigationController = (UIApplication.shared.connectedScenes.first?.delegate as? UIWindowSceneDelegate)?.window??.rootViewController as? UINavigationController else {
            return
        }
        
        SwiftyBeaver.debug("Notification received")
        
        if let topVC = navigationController.topViewController, topVC is TurnByTurnNavigationVC || topVC is CruiseModeVC {
            SwiftyBeaver.debug("The root view is TurnByTurnNavigationVC or CruiseModeVC")
        } else {
            let userInfo = response.notification.request.content.userInfo
            if let aps = userInfo["aps"] as? [String: AnyObject],
               let type = getAPNSType(aps: aps) {
                self.aps = aps
                if type == Values.upcomingTripType {
                    DispatchQueue.main.async {
                        completionHandler()
                    }
                    return
                } else if type == Values.appUpdateType {
                    handleAppUpdateNotification(aps: aps)
                }
                // no need to handle inbox and content push notification since it will be handled by MapLandingVC by monitor the aps variable
            }
        }
        
        // TODO: Should be careful about this. If app architecture changed, we may need to redesign this part
        // This only works on mobile
        // user click the notification center no matter from background or foreground
        if let topVC = navigationController.topViewController {
            
            remoteConfig.fetch { (status, error) -> Void in
                if status == .success {
                    print("Config fetched!")
                    remoteConfig.activate { changed, error in
                        
                        if let mapLandingVC = topVC as? MapLandingVC {
                            if !mapLandingVC.isCruiseModeRunning(){
                                SwiftyBeaver.debug("The root view is MapLandingVC and not in cruise mode.")
                                self.getAppUpdateAlerts(controller: topVC, fromNotification: true)
                            }
                        } else if topVC is TurnByTurnNavigationVC {
                            // Do nothing
                            SwiftyBeaver.debug("The root view is TurnByTurnNavigationVC.")
                        } else if topVC is CruiseModeVC {
                            // Do nothing
                            SwiftyBeaver.debug("The root view is CruiseModeVC.")
                        } else {
                            
                            // not login
                            SwiftyBeaver.debug("The root view is LandingViewController.")
                            self.getAppUpdateAlerts(controller: topVC,fromNotification: true)
                        }
                    }
                } else {
                    print("Config not fetched")
                    print("Error: \(error?.localizedDescription ?? "No error available.")")
                }
                
            }
            
        }
        completionHandler()
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        if let aps = notification.request.content.userInfo["aps"] as? [String: AnyObject] {
            handleLogNotification(aps: aps, isClick: false)
        }
        
        guard let navigationController = (UIApplication.shared.connectedScenes.first?.delegate as? UIWindowSceneDelegate)?.window??.rootViewController as? UINavigationController else {
            return
        }
        let userInfo = notification.request.content.userInfo
        
        if let aps = userInfo["aps"] as? [String: AnyObject],
           let alertDic = aps["alert"] as? [String: AnyObject],
           let type = getAPNSType(aps: aps),
           let titlePush = alertDic["title"] as? String,
           let bodyPush = alertDic["body"] as? String,
           let topVC = navigationController.topViewController {
            
            if type == Values.upcomingTripType {
                if !isInCruiseOrNavigation(topVC: topVC) {
                    let actionTitle = "Start Trip"
                    let globalNotificationView = GlobalNotificationView(appearance: BreezeToastStyle.upcomingTrip)
                    
                    globalNotificationView.show(in: topVC.view, title: titlePush, message: bodyPush, icon: UIImage(named: "toastUpcomingTrip")!, onClose: {
                        //We don't need specifically to handle this Action
                    }, actionTitle: actionTitle, onAction: {
                        let tripId = aps["tripPlannerId"] as? Int
                        self.gotoRoutePlanningFromNotification(tripId: tripId!, topVC: topVC)
                        globalNotificationView.dismiss(animated: true)
                    }, dismissInSeconds: 10, inHtml: false)
                    globalNotificationView.yOffset = getTopOffset()
                    pushNotificationCompletionHandler(completionHandler: completionHandler, show: false)
                }
            } /*else if type == Values.voucherType {
               if !isInCruiseOrNavigation(topVC: topVC) {
               let actionTitle = "My Vouchers"
               let globalNotificationView = GlobalNotificationView(appearance: BreezeToastStyle.voucherNotification)
               
               globalNotificationView.show(in: topVC.view, title: titlePush, message: bodyPush, icon: UIImage(named: "ic_money_tooltip")!, onClose: {
               //We don't need specifically to handle this Action
               }, actionTitle: actionTitle, onAction: {
               self.goToMyVoucherFromNotification()
               globalNotificationView.dismiss(animated: true)
               }, dismissInSeconds: 10, inHtml: false)
               globalNotificationView.yOffset = 40
               pushNotificationCompletionHandler(completionHandler: completionHandler, show: false)
               }
               }*/ else if type == Values.appUpdateType || type == Values.inboxType || type == Values.exploreMapType || type == Values.voucherType {
                   SwiftyBeaver.debug("will present push notification for Inbox, Appupdate or Explore map")
                   // should always show system notification
                   pushNotificationCompletionHandler(completionHandler: completionHandler, show: true)
               }
        }
    }
    
    private func isInCruiseOrNavigation(topVC: UIViewController) -> Bool {
        return topVC is TurnByTurnNavigationVC || topVC is CruiseModeVC
    }
    
    // should show push notification or not
    private func pushNotificationCompletionHandler(completionHandler: @escaping (UNNotificationPresentationOptions) -> Void, show: Bool) {
        if show {
            if #available(iOS 14.0, *) {
                //                completionHandler([.list])
                completionHandler([.list, .banner])
            } else {
                completionHandler([.alert])
            }
        } else {
            completionHandler([])
        }
    }
    
    // if topVC is not self => pop to Maplanding
    func goToMyVoucherFromNotification() {
        if !(baseNavigationController?.viewControllers.last is MapLandingVC),
           let mapLanding = baseNavigationController?.viewControllers.first(where: {$0 is MapLandingVC}) {
            _ = baseNavigationController?.popToViewController(mapLanding, animated: false)
        }
        _ = ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.OPEN_MY_VOUCHER_FROM_NOTIFICATION, data: nil).subscribe(onSuccess: { result in  },
                                                                                                                             onFailure: { error in
        })
    }
    
    func checkShowSaveMapCollection() -> Bool {
        let token = Prefs.shared.getStringValue(.shareMapCollectionToken)
        if !token.isEmpty {
            self.saveMapCollection(token)
            Prefs.shared.removeValueForKey(.shareMapCollectionToken)
            return true
        }
        return false
    }
    
    func checkShowSaveMapLocation() {
        let token = Prefs.shared.getStringValue(.shareMapLocationToken)
        if !token.isEmpty {
            self.saveMapLocation(token)
            Prefs.shared.removeValueForKey(.shareMapLocationToken)
        }
    }
    
    func saveMapLocation(_ token: String) {
        
        if !(baseNavigationController?.viewControllers.last is MapLandingVC),
           let mapLanding = baseNavigationController?.viewControllers.first(where: {$0 is MapLandingVC}) {
            _ = baseNavigationController?.popToViewController(mapLanding, animated: false)
        }
        
        if let mapVC = baseNavigationController?.viewControllers.last as? MapLandingVC {
            //  If user already login then hit api to save map collection
            print("token: \(token)")
            
            SaveCollectionService().saveLocationWithToken(token) { result in
                switch result {
                case .success(let content):
                    DispatchQueue.main.async {
                        //  Handle error if it is happen
                        if let code = content.error_code, !code.isEmpty, let message = content.message, !message.isEmpty {
                            mapVC.showAlert(title: "", message: message, confirmTitle: Constants.gotIt) { _ in
                                mapVC.goToSavedMapCollection()
                            }
                        } else {
                            //  Navigate to save location and send JSON to RN
                            let params: NSDictionary = [
                                Values.TORNSCREEN: CNMScreenId.shareLocationScreen.rawValue,
                                Values.SHARING_TOKEN: token as Any,
                                Values.SHARE_MODE: SharingMode.location.rawValue as Any,
                                Values.SHARE_LOCATION_DATA: content as Any
                            ]
                            appDelegate().processOpenScreen(screenId: .shareCollectionScreen, navigationParams: params)
                        }
                    }
                case .failure(let error):
                    SwiftyBeaver.error("Failed to get collection: \(error.localizedDescription)")
                }
            }
        
        }else {
            //  Try to save location token to open it later
            Prefs.shared.setValue(token, forkey: .shareMapLocationToken)
        }
    }
    
    func saveMapCollection(_ token: String) {
        
        if !(baseNavigationController?.viewControllers.last is MapLandingVC),
           let mapLanding = baseNavigationController?.viewControllers.first(where: {$0 is MapLandingVC}) {
            _ = baseNavigationController?.popToViewController(mapLanding, animated: false)
        }
        
        if let mapVC = baseNavigationController?.viewControllers.last as? MapLandingVC {
            //  If user already login then hit api to save map collection
            print("token: \(token)")
            SaveCollectionService().saveCollectionWithToken(token) { result in
                switch result {
                case .success(let content):
                    DispatchQueue.main.async {
                        //  Handle error move to save collection page and show error
                        if let code = content.error_code, !code.isEmpty, let message = content.message, !message.isEmpty {
                            mapVC.showAlert(title: "", message: message, confirmTitle: Constants.gotIt) { _ in
                                mapVC.goToSavedMapCollection()
                            }
                        } else {
                            //  Handle collection is already saved then navigate to collection detail screen
                            if let collectionId = content.data?.collectionId, collectionId > 0 {
                                let params: NSDictionary = [
                                    Values.COLLECTIONID: collectionId as Any,
                                    Values.TORNSCREEN: CNMScreenId.collectionDetail.rawValue,
                                    Values.COLLECTION_NAME: content.data?.name as Any,
                                    Values.COLLECTION_CODE: content.data?.code as Any,
                                    Values.COLLECTION_DESC: content.data?.description as Any
                                ]
                                
                                appDelegate().processOpenScreen(screenId: .collectionDetail, navigationParams: params)
                                
                            } else {
                                //  Navigate to save collection and send token to RN side
                                let params: NSDictionary = [
                                    Values.TORNSCREEN: CNMScreenId.shareCollectionScreen.rawValue,
                                    Values.COLLECTION_NAME: content.data?.name as Any,
                                    Values.COLLECTION_CODE: content.data?.code as Any,
                                    Values.COLLECTION_DESC: content.data?.description as Any,
                                    Values.SHARING_TOKEN: token as Any,
                                    Values.SHARE_MODE: SharingMode.collection.rawValue as Any
                                ]
                                appDelegate().processOpenScreen(screenId: .shareCollectionScreen, navigationParams: params)
                            }
                        }
                    }
                case .failure(let error):
                    SwiftyBeaver.error("Failed to get collection: \(error.localizedDescription)")
                }
            }
        }else {
            //  Try to save collection token to open it later
            Prefs.shared.setValue(token, forkey: .shareMapCollectionToken)
        }
    }
    
    func gotoRoutePlanningFromNotification(tripId: Int, topVC: UIViewController){
        // retrieve the trip planner details first
        
        TripPlanService().getTripPlan(tripId: tripId) { result in
            switch result {
            case .success(let details):
                // show routeplanning
                DispatchQueue.main.async { [weak self] in
                    //                    guard let self = self else { return }
                    
                    let storybard = UIStoryboard(name: "NewRoutePlanning", bundle: nil)
                    if let vc = storybard.instantiateViewController(withIdentifier: String(describing: NewRoutePlanningVC.self)) as? NewRoutePlanningVC {
                        vc.planTrip = details
                        vc.addressReceived = true
                        topVC.navigationController?.pushViewController(vc, animated: true)
                    }
                }
                
            case .failure(let err):
                SwiftyBeaver.error("Failed to getTripPlan: \(err.localizedDescription)")
            }
        }
    }
    
    // MARK: Push / In-app notification of OBU ERP & Parking charges
    func showLocalNotificationCharges(title: String, subTitle: String) {
        let content = UNMutableNotificationContent()
        
        //  adding title, subtitle, body and badge
        content.title = title
        content.body = subTitle
        content.badge = 1
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false)
        
        let request = UNNotificationRequest(identifier: "BreezeConnectOBUCharges", content: content, trigger: trigger)
        
        UNUserNotificationCenter.current().delegate = self
        
        //  adding the notification to notification center
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
    }
    
    
    func showOnAppERPNotification(title: String, subTitle: String, isSuccess: Bool = true, controller: UIViewController? = nil) {
        if let mapLandingVC = appDelegate().getMapLandingVCInTop() {
            if self.chargeNotificationOBU == nil {
                var style = BreezeToastStyle.chargeNotificationErpOBU
                if !isSuccess {
                    style = BreezeToastStyle.chargeNotificationLowCardOBU
                }
                self.chargeNotificationOBU = BreezeToast(appearance: style, onClose: { [weak self] in
                    guard let self = self else { return }
                    self.chargeNotificationOBU = nil
                }, actionTitle: "", onAction: nil)
                
                if let vc = controller {
                    self.chargeNotificationOBU?.show(in: vc.navigationController?.view ?? vc.view, title: title, message: subTitle, animated: true)
                } else {
                    self.chargeNotificationOBU?.show(in: mapLandingVC.navigationController?.view ?? mapLandingVC.view, title: title, message: subTitle, animated: true)
                }
                self.chargeNotificationOBU?.yOffset = getTopOffset()
            }
        }
    }
    
    func showOnAppParkingNotification(title: String, subTitle: String, isSuccess: Bool = true, controller: UIViewController? = nil) {
        if let mapLandingVC = appDelegate().getMapLandingVCInTop() {
            if self.chargeNotificationOBU == nil {
                
                var style = BreezeToastStyle.chargeNotificationErpOBU
                if !isSuccess {
                    style = BreezeToastStyle.chargeNotificationLowCardOBU
                }
                
                self.chargeNotificationOBU = BreezeToast(appearance: style, onClose: { [weak self] in
                    guard let self = self else { return }
                    self.chargeNotificationOBU = nil
                }, actionTitle: "", onAction: nil)
                
                if let vc = controller {
                    self.chargeNotificationOBU?.show(in: vc.navigationController?.view ?? vc.view, title: title, message: subTitle, animated: true)
                } else {
                    self.chargeNotificationOBU?.show(in: mapLandingVC.navigationController?.view ?? mapLandingVC.view, title: title, message: subTitle, animated: true)
                }
                self.chargeNotificationOBU?.yOffset = getTopOffset()
            }
        }
    }
    
    func showOnAppErrorNotification(title: String, subTitle: String, controller: UIViewController? = nil, closeAction: (() -> Void)? = nil) {
        if let mapLandingVC = appDelegate().getMapLandingVCInTop() {
            if self.chargeNotificationOBU == nil {
                self.chargeNotificationOBU = BreezeToast(appearance: BreezeToastStyle.chargeNotificationLowCardOBU, onClose: { [weak self] in
                    guard let self = self else { return }
                    self.chargeNotificationOBU = nil
                    
                    closeAction?()
                    
                }, actionTitle: "", onAction: nil)
                
                if let vc = controller {
                    self.chargeNotificationOBU?.show(in: vc.navigationController?.view ?? vc.view, title: title, message: subTitle, animated: true)
                } else {
                    self.chargeNotificationOBU?.show(in: mapLandingVC.navigationController?.view ?? mapLandingVC.view, title: title, message: subTitle, animated: true)
                }
                
                self.chargeNotificationOBU?.yOffset = getTopOffset()
            } else {
                self.chargeNotificationOBU?.updateTitle(title, message: subTitle)
            }
        }
    }
    
}


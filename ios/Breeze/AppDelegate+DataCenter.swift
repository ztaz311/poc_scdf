//
//  AppDelegate+DataCenter.swift
//  Breeze
//
//  Created by Zhou Hao on 30/9/21.
//

import Foundation
import SwiftyBeaver

extension AppDelegate {
    func updateDataCenter(isLoggedIn: Bool) {
        if isLoggedIn {
            DataCenter.shared.load { result in
                switch result {
                case .success(_):
                    SwiftyBeaver.debug("DataCenter updated")
                case .failure(let error):
                    SwiftyBeaver.error("DataCenter error: \(error.localizedDescription)")
                }
            }
        } else {
            SwiftyBeaver.debug("DataCenter unload")
            DataCenter.shared.unload()
        }
    }
}

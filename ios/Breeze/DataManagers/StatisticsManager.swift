//
//  ProfilingManager.swift
//  Breeze
//
//  Created by Zhou Hao on 22/6/21.
//

import Foundation
import UIKit

struct StatisticsData {
    let batteryLevel: Float
    let dataSent: UInt64
    let dataReceived: UInt64
}

final class StatisticsManager {
    
    static func getData() -> StatisticsData {

        let info = DataUsage.getDataUsage()
        
        UIDevice.current.isBatteryMonitoringEnabled = true
        let batteryLevel = UIDevice.current.batteryLevel

        return StatisticsData(batteryLevel: batteryLevel, dataSent: info.wifiSent + info.wirelessWanDataSent, dataReceived: info.wifiReceived + info.wirelessWanDataReceived)
    }
}

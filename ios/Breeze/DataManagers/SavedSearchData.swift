//
//  SavedSearchData.swift
//  Breeze
//
//  Created by VishnuKanth on 18/12/20.
//

import Foundation
import UIKit

class SavedSearchManager
{
    // MARK:- Properties

    public static var shared = SavedSearchManager()
    
    var easyBreezy = [EasyBreezyAddresses]()
    
    var etaFav = [Favourites]()

    var places: [SavedPlaces]
    {
        get
        {
          guard let data = UserDefaults.standard.data(forKey: "places") else { return [] }            
            do {
                    guard let placesArray = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data as Data) as? [SavedPlaces] else { return [] }
                let array = placesArray
                return array
                    
                } catch {
                    print(error.localizedDescription)
                }
            
            return []
        }
        set
        {
            do {
                    let placesData = try NSKeyedArchiver.archivedData(withRootObject: newValue, requiringSecureCoding: false)
                    UserDefaults.standard.set(placesData, forKey: "places")
                } catch {
                    print(error.localizedDescription)
                }
        }
    }
    
    var searchModels: [CarplaySearchModel] = []
    
    func checkDuplicateEBName(name:String) -> Bool{
        
        for EB in easyBreezy {
            
            let alias = EB.alias?.lowercased()
            if(alias == name.lowercased()){
                return true
            }
        }
        
        return false
    }
    
    func getModelWithType(_ type: SearchType) -> CarplaySearchModel? {
        let foundModel = self.searchModels.first { model in
            return model.type == type
        }
        return foundModel
    }
    
    func getEBIndex(name:String) -> Int{
        
        if let i = easyBreezy.firstIndex(where: { $0.alias == name }) {
            return i
        }
        
        return -1
    }
    
    func recentSearchIndex(name:String) -> Int {
        
        if let i = places.firstIndex(where: { $0.placeName == name }) {
            return i
        }
        return -1
    }
    
    func getFavouriteName(addressID:Int) -> String {
        
        for EB in easyBreezy {
            
            if EB.addressid == addressID {
                
                return EB.alias ?? ""
            }
        }
        
        return ""
        
    }
    
    func findEBEntryWithID(addressID:Int) -> Int{
        if let i = easyBreezy.firstIndex(where: { $0.addressid == addressID }) {
            return i
        }
        return -1
    }
    
    func ifRecentSearchHasName(address1:String) -> Bool {
        
        for recent in places {
            
            let name = recent.placeName.lowercased()
            if(name == address1.lowercased()){
                return true
            }
        }
        
        return false
    }

    // MARK:- Init

    private init(){
        
        //This is just a default initializer
    }
}

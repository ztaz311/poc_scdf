//
//  SavedPlaces.swift
//  Breeze
//
//  Created by VishnuKanth on 18/12/20.
//

import UIKit

class SavedPlaces: NSObject,Codable,NSCoding {

    var placeName: String
    var address:String
    var lat:String
    var long:String
    var time:String

    init(placeName: String,address:String,lat:String,long:String,time:String) {
        self.placeName = placeName
        self.address = address
        self.lat = lat
        self.long = long
        self.time = time
        
    }
    
    required init?(coder aDecoder: NSCoder) {
           
        self.placeName = aDecoder.decodeObject(forKey: "name") as? String ?? ""
        self.address = aDecoder.decodeObject(forKey: "address") as? String ?? ""
        self.lat = aDecoder.decodeObject(forKey: "lat") as? String ?? ""
        self.long = aDecoder.decodeObject(forKey: "long") as? String ?? ""
        self.time = aDecoder.decodeObject(forKey: "time") as? String ?? ""
            
        }

    func encode(with aCoder: NSCoder) {
        
        aCoder.encode(placeName, forKey: "name")
        aCoder.encode(address, forKey: "address")
        aCoder.encode(lat, forKey: "lat")
        aCoder.encode(long, forKey: "long")
        aCoder.encode(time, forKey: "time")
    }
}

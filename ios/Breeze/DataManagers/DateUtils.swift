//
//  DateUtils.swift
//  Breeze
//
//  Created by Tuan, Pham Hai on 04/01/2023.
//

import Foundation

public class DateUtils {
    
    public static var shared = DateUtils()
    
    //  Variables
    var commonDateFormatter: DateFormatter = DateFormatter()
    
    var shortTimeFormatter: DateFormatter = DateFormatter()
    
    var ampmDateFormatter: DateFormatter = DateFormatter()
    
    private init() {
        //  Short time
        shortTimeFormatter.timeStyle = .short
        shortTimeFormatter.locale = Locale(identifier: "en_US_POSIX")
        
        //  Common format
        commonDateFormatter.amSymbol = "am"
        commonDateFormatter.pmSymbol = "pm"
        commonDateFormatter.locale = Locale(identifier: "en_US_POSIX")
        
        //  AM/PM format
        ampmDateFormatter.dateFormat = "h:mm a"
        ampmDateFormatter.amSymbol = "AM"
        ampmDateFormatter.pmSymbol = "PM"
        ampmDateFormatter.locale = Locale(identifier: "en_US_POSIX")
    }
    
    //  Short time display
    func getShortTimeDisplay(_ date: Date) -> String {
        return shortTimeFormatter.string(from: date)
    }
    
    //  Common date formater
    func getTimeDisplay(dateFormat: String = Date.formatYYYYMMdd, date: Date) -> String {
        commonDateFormatter.timeZone = TimeZone.current
        commonDateFormatter.dateFormat = dateFormat
        return commonDateFormatter.string(from: date)
    }
    
    func getDate(dateFormat: String, dateString: String) -> Date? {
        commonDateFormatter.timeZone = TimeZone.current
        commonDateFormatter.dateFormat = dateFormat
        return commonDateFormatter.date(from: dateString)
    }
        
    func exportDateString(format: String = Date.formatEEEE, date: Date) -> Date? {
        commonDateFormatter.timeZone = TimeZone.current
        commonDateFormatter.dateFormat = format
        
        let selectedDayStr = commonDateFormatter.string(from: date)
        return commonDateFormatter.date(from: selectedDayStr)
    }
    
    func getTimeGTM0Display(_ dateFormat: String = Date.formatYYYYMMdd, date: Date) -> String {
        commonDateFormatter.timeZone = TimeZone(abbreviation: "GMT+0:00")
        commonDateFormatter.dateFormat = dateFormat
        return commonDateFormatter.string(from: date)
    }
    
    func getDateGTM0From(_ dateFormat: String = Date.formatYYYYMMdd, dateString: String) -> Date? {
        commonDateFormatter.timeZone = TimeZone(abbreviation: "GMT+0:00")
        commonDateFormatter.dateFormat = dateFormat
        return commonDateFormatter.date(from: dateString)
    }
    
    //  AM/PM format
    func getDateAMPM(_ dateString: String) -> Date? {
        return ampmDateFormatter.date(from: dateString)
    }
}

//
//  DataCenter.swift
//  Breeze
//
//  This is used to retreive dataSet from backend
//
//  Created by Zhou Hao on 28/5/21.
//

import Foundation
import Turf
import SwiftyBeaver
import CoreLocation
import RxSwift

final class DataCenter {
    
    enum LoadingDataType: String {
        case schoolZone
        case silverZone
        case speedCamera
        case erpService
        case trafficService
    }
    
    private var schoolZoneSVC: SchoolZoneServiceProtocol = SchoolZoneService()
    private var silveZoneSVC: SilverZoneServiceProtocol = SilverZoneService()
    private var speedCameraSVC: SpeedCameraServiceProtocol = SpeedCameraService()
    private var erpUpdater: ERPUpdater = ERPUpdater(erpService: ERPRates())
    private var trafficUpdater: TrafficIncidentUpdater = TrafficIncidentUpdater(service: TrafficIncidents())
    private weak var notificationListner: GlobalNotificationListener? // since there is only one listner throughout the whole application, I'll not use MultiDelegate
    private weak var mockObuEventListener: MockObuEventListener?
    private var appVersionService: AppUpdateService! = AppUpdateService()
//    private var dbConfig: Realm.Configuration!
    private var realtimeEngine: RealtimeEngineProtocol!
    
    // Singletone might not be the best solution, but will use it for simplicity.
    static let shared = DataCenter()

    private var schoolZones = [SchoolZoneData]()
    private var silverZones = [SilverZoneData]()
    private var speedCameras = Turf.FeatureCollection(features: [])
    
//    private var loadingProgress: [LoadingDataType: Bool] = [:]

    // MARK: - public interface
    // For dependency injection
    func setup() {
        
        // setup db
/*
        do {
            let url = try FileManager.default.url(for:.documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false).appendingPathComponent("breeze.realm")
            
            // Auto migration by setting different schemaVersion when need to be upgrading
            dbConfig = Realm.Configuration(fileURL: url, schemaVersion: 5)
        } catch {
            SwiftyBeaver.debug("Realm DB set up failed")
        }
*/
        
//        #if TESTING
//        realtimeEngine = MockAppSync()
//        #else
        realtimeEngine = AppSync()
//        #endif
    }
    
    func callERPRefreshTimer(){
        
        SwiftyBeaver.debug("Called ERPRefresh Time in DataCenter class")
        
        self.erpUpdater.refreshERPFromDataCenter()
//        self.erpUpdater.invalidateERPRefreshTimer()
//        self.erpUpdater.checkCurrentIsBetweenUniqueTimes()
    }
    
    func erp30MinRefreshTimer(differenceInterval:TimeInterval){
        
        self.erpUpdater.refreshOnlyJsonFor30MinTimer(differenceInterval: differenceInterval)
        
    }
                
    // load data in background
    func load(complete: @escaping (Result<Bool>) -> Void) {
        
        var loadError: Error?
//        self.loadingProgress = [.schoolZone: false, .silverZone: false, .speedCamera: false, .erpService: false, .trafficService: false]
        let group = DispatchGroup()

        // Service 1
        group.enter()
        self.schoolZoneSVC.getSchoolZone { result in
//            if self.loadingProgress[.schoolZone] == false {
//                self.loadingProgress[.schoolZone] = true
                switch result {
                case .success(let schoolzones):
                    SwiftyBeaver.debug("\(schoolzones.count) schoolzones loaded")
                    self.schoolZones = schoolzones
                case .failure(let error):
                    SwiftyBeaver.error("Failed to load schoolzones: \(error.localizedDescription)")
                    loadError = error
                }
                group.leave()
//            }
        }
        
        // Service 2
        group.enter()
        self.silveZoneSVC.getSilverZone { result in
//            if self.loadingProgress[.silverZone] == false {
//                self.loadingProgress[.silverZone] = true
                switch result {
                case .success(let silverzones):
                    SwiftyBeaver.debug("\(silverzones.count) silverzones loaded")
                    self.silverZones = silverzones
                case .failure(let error):
                    SwiftyBeaver.error("Failed to load silverzone: \(error.localizedDescription)")
                    loadError = error
                }
                group.leave()
//            }
        }
        
        // Service SpeedCamera
        group.enter()
        self.speedCameraSVC.getSpeedCamera { result in
//            if self.loadingProgress[.speedCamera] == false {
//                self.loadingProgress[.speedCamera] = true
//                defer { group.leave() }
                switch result {
                case .success(let speedcameras):
                    SwiftyBeaver.debug("\(speedcameras.count) speedcameras loaded")
                    let speedCameras =  self.parseSpeedCamData(speedCamData: speedcameras)
                    self.speedCameras = speedCameras
                case .failure(let error):
                    SwiftyBeaver.error("Failed to load speedcamera: \(error.localizedDescription)")
                    loadError = error
                }
                group.leave()
//            }
        }

        // 3. ERP service
        group.enter()
        self.erpUpdater.load() { result in
//            if self.loadingProgress[.erpService] == false {
//                self.loadingProgress[.erpService] = true
//                defer { group.leave() }
                if result {
                    SwiftyBeaver.debug("ERP loaded from API")
                } else {
                    SwiftyBeaver.error("Failed to load ERP from API")
                }
                group.leave()
//            }
        }

        // 4. Traffic service
        group.enter()
        self.trafficUpdater.load() { result in
//            if self.loadingProgress[.trafficService] == false {
//                self.loadingProgress[.trafficService] = true
//                defer { group.leave() }
                if result {
                    SwiftyBeaver.debug("Traffic loaded from API")
                } else {
                    SwiftyBeaver.error("Failed to load Traffic from API")
                }
                group.leave()
//            }
        }
        
        //5. AppVersionService
//        group.enter()
//        self.appVersionService.post { result in
//            SwiftyBeaver.debug("AppVersionService returned")
//            group.leave()
//        }

        // finished
        group.notify(queue: .main) {
            if loadError != nil {                
                complete(Result.failure(loadError!))
            } else {
//                self.updateDatabase()
                complete(Result.success(true))
//                self.appVersionService.post { result in
//                    SwiftyBeaver.debug("AppVersionService returned")
//                }
            }
            
            self.startRealtimeSubscription()
        }
    }
    
    func unload() {
        if realtimeEngine != nil {
            realtimeEngine.unsubscribeAll()
        }
    }
    
    public func startUpdatingUserTripLiveLocation(tripId: String, latitude: String, longitude: String, status: String, arrivalTime:String,coordinates:String){
        realtimeEngine.updateTripLiveLocation(tripId: tripId, latitude: latitude, longitude: longitude, status: status,arrivalTime: arrivalTime,coordinates: coordinates)
    }
    
    public func startUpdatingUserWalkathonLocation(userId: String, walkathonId: String, latitude: String, longitude: String, currentTime: String, data: String) {
        realtimeEngine.updateWalkathonLocation(userId: userId, walkathonId: walkathonId, latitude: latitude, longitude: longitude, currentTime: currentTime, data: data)
    }
    
    public func startUpdatingDeviceUserLocation(userId: String, bearing: String, latitude: String, longitude: String, data: String) {
        realtimeEngine.updateUserCurrentLocation(userId: userId, bearing: bearing, latitude: latitude, longitude: longitude, data: data)
    }
    
    public func startUpdatingOBUDeviceUserLocation(userId: String, bearing: String, latitude: String, longitude: String, data: String) {
        realtimeEngine.updateOBUDeviceUserLocation(userId: userId, bearing: bearing, latitude: latitude, longitude: longitude, data: data)
    }
    
    public func subscribeToUserTriplocation(tripId: String){
        realtimeEngine.subscribeToUserLiveLocation(tripId: tripId) { result in
            
            switch result {
            case .failure(let error):
                print(error.localizedDescription)
            case .success(let data):
                print(data.refreshUserTripLocation!)
        }
        }
    }
    
    public func subscribeToMessageInbox(tripETAId: String, completion: @escaping  (String) -> Void) {
        realtimeEngine.subscribeToMessageInbox(tripETAId: tripETAId) { result in
            switch result {
            case .failure(let error):
                print(error)
                completion("")
            case .success(let data):
                print(data)
                completion(data)
            }
        }
    }
    
    public func unsubscribeToMessageInbox() {
        realtimeEngine.unsubscribeToMessageInbox()
    }
    
    public func subscribeToMockObuEvent(cognitoId: String) {
        
        realtimeEngine.subscribeToMockObuEvent(cognitoId: cognitoId) { result in
            switch result {
            case .failure(let error):
                SwiftyBeaver.error("subscribeToMockObuEvent - Failed to get mock event app sync update: \(error.localizedDescription)")
            case .success(let data):
                SwiftyBeaver.debug("subscribeToMockObuEvent - Mock event app sync updated")
                if let jsonData = data.data(using: .utf8) {
                    do {
                        let event = try JSONDecoder().decode(ObuMockEventModel.self, from: jsonData)
                        self.mockObuEventListener?.onReceiveObuEvent(event)
                    } catch(let err) {
                        SwiftyBeaver.debug("Mock event parser error subscribeToMockObuEvent: \(err.localizedDescription)")
                    }
                    
                } else {
                    SwiftyBeaver.debug("Mock event json error")
                }
            }
        }
    }
    
    public func unsubcribeMockObuEvent() {
        realtimeEngine.unsubscribeMockObuEvent()
    }
    
    public func subscribeToObuEvent(cognitoId: String) {
        
        realtimeEngine.subscribeToObuEvent(cognitoId: cognitoId) { result in
            switch result {
            case .failure(let error):
                SwiftyBeaver.error("subscribeToObuEvent - Failed to get mock event app sync update: \(error.localizedDescription)")
            case .success(let data):
                SwiftyBeaver.debug("subscribeToObuEvent - Mock event app sync updated")
                if let jsonData = data.data(using: .utf8) {
                    do {
                        let event = try JSONDecoder().decode(ObuMockEventModel.self, from: jsonData)
                        self.mockObuEventListener?.onReceiveObuEvent(event)
                    } catch(let err) {
                        SwiftyBeaver.debug("Mock event parser error subscribeToObuEvent: \(err.localizedDescription)")
                    }
                    
                } else {
                    SwiftyBeaver.debug("Mock event json error")
                }
            }
        }
    }
    
    public func unsubcribeObuEvent() {
        realtimeEngine.unsubscribeObuEvent()
    }

    public func subscribeToUserNotification(cognitoId: String, completion: @escaping  ((String,String)) -> Void) {
        
        realtimeEngine.subscribeToUserNotification(cognitoId: cognitoId) { result in
            switch result {
            case .failure(let error):
                SwiftyBeaver.error(error.localizedDescription)
                completion(("",""))
            case .success(let data):
                completion(data)
            }
        }
    }

    public func unsubscribeUserNotification() {
        realtimeEngine.unsubscribeUserNotification()
    }


    private func startRealtimeSubscription() {
        realtimeEngine.subscribe(key: Values.appSyncERP) { [weak self] result in
            guard let self = self else { return }
            
            switch result {
            case .failure(let error):
                SwiftyBeaver.error("Failed to get ERP from App Sync: \(error.localizedDescription)")
            case .success(let data):
                SwiftyBeaver.debug("ERP updated in App Sync")
                if let jsonData = data.data(using: .utf8) {
                    if let result = try? JSONDecoder().decode(ERPBaseModel.self, from: jsonData) {
                        self.erpUpdater.update(erpData: result)
                        SwiftyBeaver.debug("\(String(describing: result.erprates?.count)) erps")
                    }
                }
            }
        }
        realtimeEngine.subscribe(key: Values.appSyncTrafficIncidents) {
            result in
            switch result {
            case .failure(let error):
                SwiftyBeaver.error("Failed to get traffic incidents from App Sync: \(error.localizedDescription)")
            case .success(let data):
                SwiftyBeaver.debug("traffic incidents updated in App Sync")
                if let jsonData = data.data(using: .utf8) {
                    if let result = try? JSONDecoder().decode(Incidents.self, from: jsonData) {
                        self.trafficUpdater.update(incidetns: result)
                        SwiftyBeaver.debug("\(result.incidentArray.count) traffic incidents")
                    }
                }
            }
        }
        
        realtimeEngine.subscribe(key: Values.appSyncSpeedCam) {
            result in
            switch result {
            case .failure(let error):
                SwiftyBeaver.error("Failed to get Speed Cam from App Sync: \(error.localizedDescription)")
            case .success(let data):
                SwiftyBeaver.debug("Speed cam updated in App Sync")
                if let jsonData = data.data(using: .utf8) {
                    if let result = try? JSONDecoder().decode(SpeedCameraData.self, from: jsonData) {
                        let speedCameras =  self.parseSpeedCamData(speedCamData: [result])
                        self.speedCameras = speedCameras
                        SwiftyBeaver.debug("\(speedCameras) Speed Cam")
                    }
                }
            }
        }
        
        realtimeEngine.subscribe(key: Values.appSyncSchoolZone) { [weak self] result in
            guard let self = self else { return }
            
            switch result {
            case .failure(let error):
                SwiftyBeaver.error("Failed to get school zone app sync update: \(error.localizedDescription)")
            case .success(let data):
                SwiftyBeaver.debug("SchoolZone App sync updated")
                if let jsonData = data.data(using: .utf8) {
                    if let geojson = try? JSONDecoder().decode(FeatureCollection.self, from: jsonData) {
                        self.schoolZones = SchoolZoneService.map(featureCollection: geojson)
                        SwiftyBeaver.debug("\(self.schoolZones.count) school zones")
//                        self.updateDatabase()
                    }
                }
            }
        }
        realtimeEngine.subscribe(key: Values.appSyncSilverZone) {
            result in
            switch result {
            case .failure(let error):
                SwiftyBeaver.error("Failed to get silver zone app sync update: \(error.localizedDescription)")
            case .success(let data):
                SwiftyBeaver.debug("Silver zone App sync updated")
                if let jsonData = data.data(using: .utf8) {
                    if let geojson = try? JSONDecoder().decode(FeatureCollection.self, from: jsonData) {
                        self.silverZones = SilverZoneService.map(featureCollection: geojson)
                        SwiftyBeaver.debug("\(self.silverZones.count) silver zones")
//                        self.updateDatabase()
                    }
                }
            }
        }
        
        realtimeEngine.subscribe(key: Values.appSyncInbox) { result in
            
            switch result {
            case .failure(let error):
                SwiftyBeaver.error("Failed to get Inbox app sync update: \(error.localizedDescription)")
            case .success(let data):
                SwiftyBeaver.debug("Inbox notification app sync updated")
                if let jsonData = data.data(using: .utf8) {
                    if let notifications = try? JSONDecoder().decode([NotificationModel].self, from: jsonData) {
                        self.notificationListner?.shouldShowNotification(notifications)
                    }
                }
            }
        }

    }
/*
    private func updateDatabase() {
        let localDB = try! Realm(configuration: self.dbConfig)
        try! localDB.write {
            localDB.deleteAll()
            localDB.add(self.schoolZones)
            localDB.add(self.silverZones)
        }
    }
    
    func getAllData<Element: Object>(_ type: Element.Type) -> [Element] {
        let localDB = try! Realm(configuration: dbConfig)
        return localDB.objects(type).map { $0 }
    }
    
    func getItem<Element: Object>(_ type: Element.Type, id: String) -> Element? {
        let localDB = try! Realm(configuration: dbConfig)
        return localDB.object(ofType: type, forPrimaryKey: id)
    }
*/
    func getAllSchoolZone() -> [SchoolZoneData] {
        return self.schoolZones
    }

    func getAllSilverZone() -> [SilverZoneData] {
        return self.silverZones
    }
    
    func getSchoolZoneItem(id: String) -> SchoolZoneData? {
        for schoolZone in self.schoolZones {
            if schoolZone.id == id {
                return schoolZone
            }
        }
        return nil
    }

    func getSilverZoneItem(id: String) -> SilverZoneData? {
        for silverZone in self.silverZones {
            if silverZone.id == id {
                return silverZone
            }
        }
        return nil
    }
    
    private func parseSpeedCamData(speedCamData:[SpeedCameraData]) -> Turf.FeatureCollection{
        
        var speedCollection = [Turf.Feature]()
        for speedCamDatum in speedCamData {
            
            let lat = speedCamDatum.location_latitude?.toDouble()
            let long = speedCamDatum.location_longitude?.toDouble()
            
            let speedCamID = speedCamDatum._id ?? 0
            
            let speedCoord = CLLocationCoordinate2D(latitude: lat ?? 0.0, longitude: long ?? 0.0)
            
            var feature = Turf.Feature(geometry: .point(Point(speedCoord)))
            
            feature.properties = [
                "Type":.string(speedCamDatum.type_of_speed_camera ?? ""),
                "id": .string("\(speedCamID)")
            ]
            
            speedCollection.append(feature)
            
        }
        
        return Turf.FeatureCollection(features: speedCollection)
    }
    
    func getAllSpeedCamera() -> Turf.FeatureCollection {
        
        return self.speedCameras
    }
    
    func getSpeedCameraItem(id:String) -> Turf.Feature? {
        
        for feature in speedCameras.features {
            
            if case let .string(speedCamid) = feature.properties?["id"]{
                
                if id == speedCamid {
                    return feature
                }
            }
        }
        
        
        return nil
    }

    func getAllERPFeatureCollection() -> FeatureCollection {
        return erpUpdater.erpAllFeatureCollection
    }
    
    func getERPCostFeatures() -> FeatureCollection {
        return erpUpdater.erpFeatureDetectionCostFeatures
    }
    
    func getERPNoCostFeatures() -> FeatureCollection {
        return erpUpdater.erpFeatureDetectionNoCostFeatures
    }
    
    func addGlobalNotificationListner(listner: GlobalNotificationListener) {
        self.notificationListner = listner
    }
    
    func addMockObuEventListner(listner: MockObuEventListener?) {
        self.mockObuEventListener = listner
    }
        
    func addERPUpdaterListner(listner: ERPUpdaterListener) {
        erpUpdater.bind(listener: listner)
    }
    
    func removeERPUpdaterListner(listner: ERPUpdaterListener) {
        erpUpdater.unbind(listener: listner)
    }
    
    func getTrafficIncidentFeatures() -> FeatureCollection {
        return trafficUpdater.currentTrafficFeatureCollection
    }
    
    func addTrafficUpdaterListener(listener: TrafficIncidentUpdaterListener) {
        trafficUpdater.bind(listener: listener)
    }
    
    func removeTrafficUpdaterListener(listener: TrafficIncidentUpdaterListener) {
        trafficUpdater.unbind(listener: listener)
    }
    
    func getDemoFeatures() -> FeatureCollection {
        return DemoSession.shared.currentDemoFeatureCollection
    }
    
    func getERPJsonData() -> ERPBaseModel?{
        
        return erpUpdater.getERPData()
        
    }

}

//
//  AppUpdater.swift
//  Breeze
//
//  Created by VishnuKanth on 17/12/21.
//

import Foundation
import UIKit
import StoreKit

public class AppUpdater {
  
  private init() {
      //This is just a empty initializer
  }
  
  public static let shared = AppUpdater()
    
    func isMaintenancing() -> Bool {
        let isMaintenance = remoteConfig.configValue(forKey: Values.is_maintenance_v2).boolValue
        return isMaintenance
    }
    
    func getMaintenanceMessage() -> String {
        let message = remoteConfig.configValue(forKey: Values.maintenance_message_v2).stringValue ?? ""
        return message
    }

    func sendAppVersionToServer(){
        
       AppUpdateService().post { result in
           //intentionally not implemented because this response doesn't need to handle
        }
    }
    
    func checkAppUpdateMandatoryVersion() -> Bool{
        
        let minimumVersion = remoteConfig.configValue(forKey: Values.min_build_version).numberValue.intValue
        
        let currentAppVersion = Int(UIApplication.buildString()) ?? 0
        
        if(currentAppVersion < minimumVersion){
            
            return true
        }
        return false
    }
    
    func isSameAppversion() -> Bool{
        
        let latestVersion = remoteConfig.configValue(forKey: Values.latest_build_version).numberValue.intValue
        
        let currentAppVersion = Int(UIApplication.buildString()) ?? 0
        
        if(currentAppVersion == latestVersion){
            
            return true
        }
        
        return false
    }
    
    func checkAppOptionalUpdateVersion() -> Bool {
        
        let latestVersion = remoteConfig.configValue(forKey: Values.latest_build_version).numberValue.intValue
        
        let currentAppVersion = Int(UIApplication.buildString()) ?? 0
        
        //Optional Update
        if(currentAppVersion < latestVersion){
            
            return true
        }
        else{
            
            //No app update required
            return false
            
        }
        
    }
    
    public func redirectToAppDownloadPage(){
        
        /// get app redirect link from remote config
        let appDownloadLink = remoteConfig.configValue(forKey: Values.download_link).stringValue ?? ""
        
    
        /// To check if the test flight app is already available in phone, then redirect to testflight app.
        #if BETARELEASE
        
        /// this is string for test flight
        guard let testFlightURL = URL(string: "itms-beta://") else {
            return
            
        }
        if UIApplication.shared.canOpenURL(testFlightURL){
            
            self.openAppDownloadURL(appDownloadLink: appDownloadLink)
        }
        #else
        
            /// For dev and app store, depends on url link that's been set in remote config
            self.openAppDownloadURL(appDownloadLink: appDownloadLink)
        #endif
    }
    
    private func openAppDownloadURL(appDownloadLink:String){
        
        if let url = URL(string: appDownloadLink){
            
            UIApplication.shared.open(url, completionHandler: { (success) in
                print("Settings opened: \(success)") // Prints true
            })
        }
        
    }
    
    private func saveDateAndCount(){
        
        /// Storing optional update pop up count
        let count = UserDefaults.standard.integer(forKey: Values.optionalAppUpdateLaterCount)
        UserDefaults.standard.set(count+1, forKey: Values.optionalAppUpdateLaterCount)
        
        /// Storing current date that shows app optional update pop up
        UserDefaults.standard.set(Date(), forKey: Values.optionalAppUpdateLaterDate)
    }
    
    func showAppOptionalUpdateAlert(controller:UIViewController){
        
        controller.dismissAnyAlertControllerIfPresent()
        
        /// remote config fetching total number of app update optional alerts to show
        let totalTrials = remoteConfig.configValue(forKey: Values.optional_update_check_trials).numberValue.intValue
        
        /// remote config fetching app update optional alert duration time to show
        let alertDuration = remoteConfig.configValue(forKey: Values.optional_update_check_duration_hrs).numberValue.intValue
        
        /// fetch saved app update later count from user defaults
        let optionalAppUpdateLaterCount = UserDefaults.standard.integer(forKey: Values.optionalAppUpdateLaterCount)
        
        /// fetch app update later action clicked date from user defaults
        if let storedDate = UserDefaults.standard.object(forKey: Values.optionalAppUpdateLaterDate) as? Date {
            print("Stored Date: ", storedDate)
            
            /// check if number of times alert shown is not equal to 3 and last shown is  >= 24 or somtimes check with minutes
            if(optionalAppUpdateLaterCount != totalTrials && (Date().hours(from: storedDate) >= alertDuration || Date().minutes(from: storedDate) >= alertDuration )){
                
                /// Optional App update show "Later" action if it is first time pop up, then show "Dismiss" for remaining popups
                controller.popupAlert(title: Constants.updateTitle, message: Constants.optionalUpdateMessage, actionTitles: [optionalAppUpdateLaterCount >= 1 ? Constants.optionalUpdateActionDismiss:Constants.optionalUpdateActionLater,Constants.optionaUpdateActionInstall], actions:[{action1 in
                    
                    AnalyticsManager.shared.logPopUpEvent(popupName: ParameterName.SoftwareUpdate.PopUpName.normal_update, eventName: ParameterName.SoftwareUpdate.UserPopUp.later)
                    
                    //action Reminde me later
                    self.saveDateAndCount()
                    
                },{action2 in
                    
                    
                    AnalyticsManager.shared.logPopUpEvent(popupName: ParameterName.SoftwareUpdate.PopUpName.normal_update, eventName: ParameterName.SoftwareUpdate.UserPopUp.install_now)
                    
                    //action install now
                    //self.saveDateAndCount()
                    self.redirectToAppDownloadPage()
                    
                }, nil])
            }
        }
        else{
            
            /// if user defaults date is not available, consider this is first alert
            //Optional App update
            controller.popupAlert(title: Constants.updateTitle, message: Constants.optionalUpdateMessage, actionTitles: [Constants.optionalUpdateActionLater,Constants.optionaUpdateActionInstall], actions:[{action1 in
                
                AnalyticsManager.shared.logPopUpEvent(popupName: ParameterName.SoftwareUpdate.PopUpName.normal_update, eventName: ParameterName.SoftwareUpdate.UserPopUp.later)
                
                //action remind later
                self.saveDateAndCount()
                
            },{action2 in
                
                AnalyticsManager.shared.logPopUpEvent(popupName: ParameterName.SoftwareUpdate.PopUpName.normal_update, eventName: ParameterName.SoftwareUpdate.UserPopUp.install_now)
                
                //action install now
               // self.saveDateAndCount()
                self.redirectToAppDownloadPage()
                
            }, nil])
        }
        
    }
    
    func showAppUpdateMandatoryAlert(controller:UIViewController){
        
        /// check remote config relogin value, if it's true logout user
        let userShouldLogout = remoteConfig.configValue(forKey: Values.mandatory_relogin).boolValue
        
        /// Mandatory App update Alert
        controller.dismissAnyAlertControllerIfPresent()
        controller.popupAlert(title: Constants.updateTitle, message: Constants.mandatoryUpdateMessage, actionTitles: [Constants.mandatoryUpdateActionTitle], actions:[{action1 in
            
            /// For mandatory update we need to signout first if relogin value is true
            if(userShouldLogout){
                
                AWSAuth.sharedInstance.signOut()
                
                if(controller .isKind(of: MapLandingVC.self)){
                    controller.navigationController?.popToRootViewController(animated: true)
                    controller.goToLoginVC()
                    
                }
            }
            
            AnalyticsManager.shared.logPopUpEvent(popupName: ParameterName.SoftwareUpdate.PopUpName.mandatory_update, eventName: ParameterName.SoftwareUpdate.UserPopUp.update_now)
            
            self.redirectToAppDownloadPage()
            
            
        },{action2 in
            
           //This is just a single action, we don't need to implement this block
            
        }, nil])
    }
    
    func showAppUpToDateAlert(controller: UIViewController) {        
        controller.popupAlert(title: Constants.softwareUpdateTitle, message: Constants.appIsLatestMessage, actionTitles: [Constants.optionalUpdateActionDismiss], actions:[{action1 in
            
            AnalyticsManager.shared.logPopUpEvent(popupName: ParameterName.SoftwareUpdate.PopUpName.lastest_version_updated, eventName: ParameterName.SoftwareUpdate.UserPopUp.dismiss)
            
        }, nil], autoDismissIn: 5)
    }
}

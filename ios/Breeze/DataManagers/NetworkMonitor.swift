//
//  NetworkMonitor.swift
//  Breeze
//
//  Created by VishnuKanth on 04/02/21.
//

import Foundation
import Reachability
import Combine

// MARK: - Delegate Protocol
public protocol ConnectionListener: NSObjectProtocol {
    func connectionChanged(status: Reachability.Connection)
}

public protocol ConnectionCombineListener: ConnectionListener {
    var connectionDisposables: Set<AnyCancellable> { get set }
}

extension ConnectionCombineListener {
    func subscribeNetworkChange() {
        ReachabilityManager.shared.$connectionStatusPubliser.sink { [weak self] status in
            guard let self = self else { return }
            self.connectionChanged(status: status)
        }.store(in: &connectionDisposables)
    }
    
    func cancelConnectionChange() {
        connectionDisposables.forEach { cancellable in
            cancellable.cancel()
        }
        connectionDisposables.removeAll()
    }
}

// MARK: - Delegating Object
public class ReachabilityManager {
    
    private init() {
        
        //This is just a default initializer
    }
    
    public static let shared = ReachabilityManager()
    
    private let reachability: Reachability? = try? Reachability()
    
    var consoleLog: Bool = true
    var connectionStatus: Reachability.Connection = .unavailable {
        didSet {
            connectionStatusPubliser = connectionStatus
        }
    }
    
    @Published var connectionStatusPubliser: Reachability.Connection = .unavailable
    
    @objc func reachabilityChanged(notification: Notification) {
        
        guard let reachability = notification.object as? Reachability else { return }
        
        if consoleLog {
            switch reachability.connection {
            case .cellular:
                print("ReachabilityManager: Network reachable through cellular data")
            case .wifi:
                print("ReachabilityManager: Network reachable through Wifi")
            case .none:
                print("ReachabilityManager: none")
            case .unavailable:
                print("ReachabilityManager: Network become unavailable")
            }
        }
        
        DispatchQueue.main.async {
            self.connectionStatus = reachability.connection
        }
    }
    
    
    public func startMonitoring(withLog: Bool) {
        consoleLog = withLog
        NotificationCenter.default.setObserver(self,
                                               selector: #selector(self.reachabilityChanged),
                                               name: Notification.Name.reachabilityChanged,
                                               object: reachability)
        
        
        connectionStatus = reachability?.connection ?? .unavailable
        do {
            try reachability?.startNotifier()
        } catch {
            debugPrint("Could not start reachability notifier")
        }
    }
    
    
    public func stopMonitoring() {
        reachability?.stopNotifier()
        NotificationCenter.default.removeObserver(self, name: Notification.Name.reachabilityChanged, object: reachability)
    }
    
    public func isReachable(success: @escaping (() -> Void) = {},
                            failure: @escaping (() -> Void) = {}) {
        DispatchQueue.main.async {
            (self.connectionStatus != .unavailable) ? success() : failure()
        }
    }
    
}

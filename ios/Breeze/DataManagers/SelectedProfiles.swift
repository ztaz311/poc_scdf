//
//  SelectedProfiles.swift
//  Breeze
//
//  Created by Zhou Hao on 18/5/22.
//

import Foundation
import CoreLocation
import Turf
import UIKit

// Currently it only support one profile
final class SelectedProfiles {
    static let shared = SelectedProfiles()
    private var selectedProfile = ""
    private var innerZone: ContentZone?
    private var outerZone: ContentZone?
    
    private var zones: [ContentZone] = []
    private var zoneColors: [ZoneColor] = []
    
    private var innerZoneColor: String?
    
    private var outerZoneColor: String?
    
    private var innerZoneColorOpacity: Double?
    
    private var outerZoneColorOpacity: Double?
    
    private var tooTipRadius: Int?
    
    private var seasonParkingsFeatureCollection: FeatureCollection?
        
    private init() {
        // Don't allow to create it manually
    }
    
    func setZoneColors(zoneColors: [ZoneColor]) {
        self.zoneColors = zoneColors
        
        zoneColors.forEach { zoneColor in
            
            // Set zone color for inner zone
            if zoneColor.name == innerZone?.zoneID {
                innerZoneColor = zoneColor.color
                innerZoneColorOpacity = zoneColor.opacity
            }
            
            // Set zone color for outer zone
            if zoneColor.name == outerZone?.zoneID {
                outerZoneColor = zoneColor.color
                outerZoneColorOpacity = zoneColor.opacity
            }
            
        }
    }
    
    func getHigherZoneRadius() -> ContentZone?{
        
        if(zones.count > 0){
            
            if zones.count == 1{
                
                return zones.first
            }
            else{
                
                let zones = zones.sorted(by: { $0.radius < $1.radius })
                return zones.last
            }
        }
        
        return nil
    }
    
    func getZoneColors() -> [ZoneColor] {
        
        return zoneColors
    }
    
    func selectProfile(name: String) {
        selectedProfile = name
    }
    
    func setToolTipRadius(radius: Int){
        tooTipRadius = radius
    }
    
    func setSeasonParkings(_ parkings: [SeasonParking]) {
        var features = [Turf.Feature]()
        // convert to FeatureCollection
        for (index,parking) in parkings.enumerated() {
            let coordinate = CLLocationCoordinate2D(latitude: parking.lat ?? 0, longitude: parking.long ?? 0)

            var feature = Turf.Feature(geometry: .point(Point(coordinate)))
            
            feature.properties = [
                "id": .string("\(FeatureDetection.featureTypeSeasonParking)-\(index+1)")
            ]
            
            features.append(feature)
        }
        if features.count > 0 {
            self.seasonParkingsFeatureCollection = FeatureCollection(features: features)
        }
    }
    
    func getSeasonParkingsFeatureCollection() -> FeatureCollection? {
        return self.seasonParkingsFeatureCollection
    }
    
    func setAllZones(allZones:[ContentZone]) {
        zones = allZones
    }
    
    func getAllZones() -> [ContentZone] {
        return zones
    }
    
    func setInnerZone(zone: ContentZone?) {
        innerZone = zone
    }
    
    func setOuterZone(zone: ContentZone?){
        outerZone = zone
    }
    
    func getInnerZone() -> ContentZone? {
        return innerZone
    }
    
    func getOuterZone() -> ContentZone? {
        return outerZone
    }
    
    func clearProfile() {
        selectedProfile = ""
    }

    func selectedProfileName() -> String {
        return selectedProfile
    }
    
    func isProfileSelected() -> Bool {
        return !selectedProfile.isEmpty
    }
    
    func getInnerZoneRadius() -> Int {
        return innerZone?.radius ?? 0
    }
    
    func getOuterZoneRadius() -> Int {
        return outerZone?.radius ?? 0
    }
    
    func getInnerZoneId() -> String {
        if let zone = getInnerZone() {
            return zone.zoneID
        }
        return ""
    }
    
    func getInnerZoneActiveZoom() -> Double{
        
        if let zone = getInnerZone() {
            return Double(zone.activeZoomLevel)
        }
        return 11.0
    }
    
    func getInnerZoneInActiveZoom() -> Double{
        
        if let zone = getInnerZone() {
            return Double(zone.deactiveZoomLevel)
        }
        return 11.0
    }
    
    func getOuteZoneActiveZoom() -> Double{
        
        if let zone = getOuterZone() {
            return Double(zone.activeZoomLevel)
        }
        return 11.0
    }
    
    func getOuterZoneInActiveZoom() -> Double{
        
        if let zone = getInnerZone() {
            return Double(zone.deactiveZoomLevel)
        }
        return 11.0
    }

    func getOuterZoneId() -> String {
        if let zone = getOuterZone() {
            return zone.zoneID
        }
        return ""
    }

    func getToolTipRadius() -> Int?{
        
        if let tooTipRadius = tooTipRadius {
            return tooTipRadius
        }
        
        return 91
    }

    func getInnerZoneCentrePoint() -> CLLocationCoordinate2D? {
        
        guard let lat = innerZone?.centerPointLat,
              let long = innerZone?.centerPointLong else { return nil }
        return CLLocationCoordinate2D(latitude: lat.toDouble() ?? 0, longitude: long.toDouble() ?? 0)
    }
    
    func getOuterZoneCentrePoint() -> CLLocationCoordinate2D? {
        
        guard let lat = outerZone?.centerPointLat,
              let long = outerZone?.centerPointLong else { return nil }
        return CLLocationCoordinate2D(latitude: lat.toDouble() ?? 0, longitude: long.toDouble() ?? 0)
    }
    
    func getInnerZoneColor() -> UIColor? {
        guard let colorName = innerZoneColor else {
            return nil
        }
        return UIColor(hexString: colorName,alpha: innerZoneColorOpacity ?? 0.1)
    }
    
    func getOuterZoneColor() -> UIColor? {
        guard let colorName = outerZoneColor else {
            return nil
        }
        return UIColor(hexString:colorName,alpha: outerZoneColorOpacity ?? 0.1)
    }
    
    /*
     if availablePercent less than 20, zone is congestion
     Now check with outer zone only.
     */
    func isOuterZoneCongestion() -> Bool {
        for zoneColor in zoneColors {
            if zoneColor.name == outerZone?.zoneID {
                return (zoneColor.availablePercent ?? 100) < 20
            }
        }
        return false
    }
    
    func isinnerZoneCongestion() -> Bool {
        for zoneColor in zoneColors {
            if zoneColor.name == innerZone?.zoneID {
                return (zoneColor.availablePercent ?? 100) < 20
            }
        }
        return false
    }
    
    /*
     if availablePercent less than 20, zone is congestion
     Now check with any zone.
     */
    func isZoneCongestion() -> Bool {
        for zoneColor in zoneColors {
            
            for zone in zones {
                
                if zoneColor.name == zone.zoneID {
                    return (zoneColor.availablePercent ?? 100) < 20
                }
            }
        }
        return false
    }
}

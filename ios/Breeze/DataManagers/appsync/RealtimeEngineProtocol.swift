//
//  RealtimeEngineProtocol.swift
//  Breeze
//
//  Created by Zhou Hao on 29/6/21.
//

import Foundation
import AWSAppSync

protocol RealtimeEngineProtocol {    
    func subscribe(key: String, completion: @escaping (Result<String>) -> Void)
    func subscribeToUserLiveLocation(tripId: String, completion: @escaping (Result<RefreshUserTripLocationSubscription.Data>) -> Void)
    func subscribeToMessageInbox(tripETAId: String, completion: @escaping (Result<String>) -> Void)
    func unsubscribeToMessageInbox()
    func subscribeToMockObuEvent(cognitoId: String, completion: @escaping (Result<String>) -> Void)
    func unsubscribeMockObuEvent()
    func subscribeToObuEvent(cognitoId: String, completion: @escaping (Result<String>) -> Void)
    func unsubscribeObuEvent()
    func subscribeToUserNotification(cognitoId: String, completion: @escaping (Result<(String,String)>) -> Void)
    func unsubscribeUserNotification()
    func updateTripLiveLocation(tripId:String,latitude:String,longitude:String,status: String,arrivalTime:String,coordinates:String)
    func updateWalkathonLocation(userId: String, walkathonId: String, latitude: String, longitude: String, currentTime: String, data: String)
    func updateUserCurrentLocation(userId: String, bearing: String, latitude: String, longitude: String, data: String)
    func updateOBUDeviceUserLocation(userId: String, bearing: String, latitude: String, longitude: String, data: String)
    func unsubscribe(key: String)
    func unsubscribeAll()
}

//
//  MockAppSync.swift
//  Breeze
//
//  Created by Zhou Hao on 29/6/21.
//

import Foundation

final class MockAppSync: RealtimeEngineProtocol {
    func subscribe(key: String, completion: @escaping (Result<String>) -> Void) {
        if key == Values.appSyncERP {
            let path = Bundle.main.path(forResource: "MockERP", ofType: "json")
            if let json = try? String(contentsOfFile: path!) {
                completion(.success(json))
            } else {
                completion(.failure(AppSyncError.nilResult))
            }
        } else if key == Values.appSyncSchoolZone {
            let path = Bundle.main.path(forResource: "schoolzone", ofType: "json")
            if let json = try? String(contentsOfFile: path!) {
                completion(.success(json))
            } else {
                completion(.failure(AppSyncError.nilResult))
            }
        } else if key == Values.appSyncSilverZone {
            let path = Bundle.main.path(forResource: "SILVERZONE", ofType: "geojson")
            if let json = try? String(contentsOfFile: path!) {
                completion(.success(json))
            } else {
                completion(.failure(AppSyncError.nilResult))
            }
        } else if key == Values.appSyncTrafficIncidents {
            let path = Bundle.main.path(forResource: "traffic", ofType: "json")
            if let json = try? String(contentsOfFile: path!) {
                completion(.success(json))
            } else {
                completion(.failure(AppSyncError.nilResult))
            }
        }
    }
    
    func unsubscribe(key: String) {
        
        //This is just for mock service
    }
    
    func unsubscribeAll() {
        //This is just for mock service
    }
    
    func subscribeToUserLiveLocation(tripId: String, completion: @escaping (Result<RefreshUserTripLocationSubscription.Data>) -> Void) {
        //This is just for mock service
    }
    
    func updateTripLiveLocation(tripId:String,latitude:String,longitude:String,status: String,arrivalTime:String,coordinates:String) {
        //This is just for mock service
    }
    
    func updateWalkathonLocation(userId: String, walkathonId: String, latitude: String, longitude: String, currentTime: String, data: String) {
        //This is just for mock service
    }
    
    func subscribeToMessageInbox(tripETAId: String, completion: @escaping (Result<String>) -> Void) {
        //This is just for mock service
    }
    
    func unsubscribeToMessageInbox() {
        //This is just for mock service 
    }
    
    func subscribeToMockObuEvent(cognitoId: String, completion: @escaping (Result<String>) -> Void) {
        //This is just for mock service
    }
    
    func unsubscribeMockObuEvent() {
        //This is just for mock service
    }
    
    func subscribeToObuEvent(cognitoId: String, completion: @escaping (Result<String>) -> Void) {
        //This is just for mock service
    }
    
    func unsubscribeObuEvent() {
        //This is just for mock service
    }
    
    func updateUserCurrentLocation(userId: String, bearing: String, latitude: String, longitude: String, data: String) {
        //This is just for mock service
    }
    
    func updateOBUDeviceUserLocation(userId: String, bearing: String, latitude: String, longitude: String, data: String) {
        //This is just for mock service
    }
    
    func subscribeToUserNotification(cognitoId: String, completion: @escaping (Result<(String,String)>) -> Void) {
        
    }
    
    func unsubscribeUserNotification() {
        
    }

}

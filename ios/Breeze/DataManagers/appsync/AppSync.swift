//
//  AppSync.swift
//  Breeze
//
//  Created by Zhou Hao on 29/6/21.
//

import Foundation
import AWSAppSync
import Amplify
import AWSCognitoAuthPlugin
import AWSMobileClient
import AWSCognitoAuthPlugin

extension AWSMobileClient: AWSCognitoUserPoolsAuthProviderAsync {
    
    public func getLatestAuthToken(_ callback: @escaping (String?, Error?) -> Void) {
        getTokenNew(complete: callback)
        //  Can not get idToken since upgrade Amplify V2.8.0 with existing function
        /*
        getTokens { (tokens, error) in
            if error != nil {
                callback(nil, error)
            } else {
                callback(tokens?.idToken?.tokenString, nil)
            }
        }
         */
    }
    
    public func getLatestAuthToken() -> String {
        return AWSAuth.sharedInstance.idToken
    }
    
    func getTokenNew(complete: @escaping (String?, Error?) -> Void) {
        if AWSAuth.sharedInstance.isCurrentTokenExpired() {
            AWSAuth.sharedInstance.fetchCurrentSession { (session) in
                if let session = session as? AWSAuthCognitoSession {
                    if session.isSignedIn {
                        complete(AWSAuth.sharedInstance.idToken, nil)
                        return
                    }
                }
                        
                complete("", nil)
            } onFailure: { (failure) in
                complete("", nil)
            }
        } else {
            complete(AWSAuth.sharedInstance.idToken, nil)
        }
    }
}

enum AppSyncError: Error {
    case nilResult
    case nilGraphQLResponse
}

final class AppSync: RealtimeEngineProtocol {
    private var appSyncClient: AWSAppSyncClient?
    private var subscriptionWatchers = [String: AWSAppSyncSubscriptionWatcher<RefreshDataSetSubscription>]()
    private var subscriptionWatchersLiveLocation = [String: AWSAppSyncSubscriptionWatcher<RefreshUserTripLocationSubscription>]()
    private var messageInboxWatcher: AWSAppSyncSubscriptionWatcher<MessageInboxSubscription>?
    private var notifyUserWatcher: AWSAppSyncSubscriptionWatcher<NotifyUserSubscription>?
    private var mockObuEventWatcher: AWSAppSyncSubscriptionWatcher<RefreshMockEventSubscription>?
    private var obuEventWatcher: AWSAppSyncSubscriptionWatcher<RefreshObuEventSubscription>?
    
    init() {
        do {
            let appSyncConfig = try AWSAppSyncClientConfiguration(url: URL(string: getAppSyncURL()!)!, serviceRegion: AWSRegionType.APSoutheast1,
                userPoolsAuthProvider: AWSMobileClient.default())
            appSyncClient = try AWSAppSyncClient(appSyncConfig: appSyncConfig)
            
            // FIXME: setup in awsconfiguration.json doesn't work since we're using amplifyconfiguration
//            let cacheConfiguration = try AWSAppSyncCacheConfiguration()
//            let appSyncConfig = try AWSAppSyncClientConfiguration(appSyncServiceConfig: AWSAppSyncServiceConfig(),
//                                                                  cacheConfiguration: cacheConfiguration)
//            // initialize app sync client
//            appSyncClient = try AWSAppSyncClient(appSyncConfig: appSyncConfig)

        } catch {
            print("Error initializing AppSync client. \(error)")
        }
    }
    
    deinit {
        unsubscribeAll()
    }
        
    func subscribe(key: String, completion: @escaping (Result<String>) -> Void) {
        
        if subscriptionWatchers[key] != nil { return }
        let subscriptionRequest = RefreshDataSetSubscription(notificationType: key)
        do {
            let watcher = try appSyncClient?.subscribe(subscription: subscriptionRequest) { result, transaction, error in
                                
                guard error == nil else {
                    print("Error in subscription for \(key): \(error!.localizedDescription)")
                    completion(.failure(error!))
                    return
                }

                guard let result = result else {
                    print("Result unexpectedly nil in subscription for \(key)")
                    completion(.failure(AppSyncError.nilResult))
                    return
                }

                guard let data = result.data?.refreshDataSet?.data else {
                    print("GraphQL response data unexpectedly nil in subscription for \(key)")
                    completion(.failure(AppSyncError.nilGraphQLResponse))
                    return
                }                
                completion(.success(data))
            }

            subscriptionWatchers[key] = watcher
        } catch (let err) {
            print(err)
        }
    }

    ///
    func subscribeToUserNotification(cognitoId: String, completion: @escaping (Result<(String,String)>) -> Void) {
        if notifyUserWatcher != nil {
            unsubscribeUserNotification()
            notifyUserWatcher = nil
        }
        
        let subscriptionRequest = NotifyUserSubscription(cognitoUserId: cognitoId)
            
        do {
            let watcher = try appSyncClient?.subscribe(subscription: subscriptionRequest) { result, transaction, error in
                                
                guard error == nil else {
                    print("Error in subscription for \(cognitoId): \(error!.localizedDescription)")
                    completion(.failure(error!))
                    return
                }

                guard let result = result else {
                    print("Result unexpectedly nil in subscription for \(cognitoId)")
                    completion(.failure(AppSyncError.nilResult))
                    return
                }

                guard let data = result.data?.notifyUser?.data, let type = result.data?.notifyUser?.notificationType else {
                    print("GraphQL response data unexpectedly nil in subscription for \(cognitoId)")
                    completion(.failure(AppSyncError.nilGraphQLResponse))
                    return
                }
                print("subscribeToUserNotification \(data)")
                completion(.success((data,type)))
            }

            self.notifyUserWatcher = watcher
        } catch (let err) {
            print(err)
        }
        
    }
    
    func unsubscribeUserNotification() {
        if let watcher = notifyUserWatcher {
            watcher.cancel()
        }
    }
    
    func subscribeToMockObuEvent(cognitoId: String, completion: @escaping (Result<String>) -> Void) {
        if mockObuEventWatcher != nil {
            unsubscribeMockObuEvent()
            mockObuEventWatcher = nil
        }

        let subscriptionRequest = RefreshMockEventSubscription(userId: cognitoId)

        do {

            let watcher = try appSyncClient?.subscribe(subscription: subscriptionRequest, resultHandler: { result, transaction, error in
                guard error == nil else {
                    print("Error in subscription for \(cognitoId): \(error!.localizedDescription)")
                    completion(.failure(error!))
                    return
                }

                guard let result = result else {
                    print("Result unexpectedly nil in subscription for \(cognitoId)")
                    completion(.failure(AppSyncError.nilResult))
                    return
                }
                                
                guard let eventType = result.data?.refreshMockEvent?.eventType, let data = result.data?.refreshMockEvent?.data, !data.isEmpty else {
                    print("GraphQL response data unexpectedly nil in subscription for \(cognitoId)")
                    completion(.failure(AppSyncError.nilGraphQLResponse))
                    return
                }
                print("subscribeToUserNotification event: \(eventType), data: \(data)")
                completion(.success(data))
            })
            self.mockObuEventWatcher = watcher

        } catch (let err) {
            print(err)
        }
    }

    func unsubscribeMockObuEvent() {
        if let watcher = mockObuEventWatcher {
            watcher.cancel()
        }
    }
    
    func subscribeToObuEvent(cognitoId: String, completion: @escaping (Result<String>) -> Void) {
        if obuEventWatcher != nil {
            unsubscribeObuEvent()
            obuEventWatcher = nil
        }

        let subscriptionRequest = RefreshObuEventSubscription(userId: cognitoId)

        do {

            let watcher = try appSyncClient?.subscribe(subscription: subscriptionRequest, resultHandler: { result, transaction, error in
                guard error == nil else {
                    print("Error in subscription for \(cognitoId): \(error!.localizedDescription)")
                    completion(.failure(error!))
                    return
                }

                guard let result = result else {
                    print("Result unexpectedly nil in subscription for \(cognitoId)")
                    completion(.failure(AppSyncError.nilResult))
                    return
                }
                                
                guard let eventType = result.data?.refreshObuEvent?.eventType, let data = result.data?.refreshObuEvent?.data, !data.isEmpty else {
                    print("GraphQL response data unexpectedly nil in subscription for \(cognitoId)")
                    completion(.failure(AppSyncError.nilGraphQLResponse))
                    return
                }
                print("subscribeToUserNotification event: \(eventType), data: \(data)")
                completion(.success(data))
            })
            self.obuEventWatcher = watcher

        } catch (let err) {
            print(err)
        }
    }

    func unsubscribeObuEvent() {
        if let watcher = obuEventWatcher {
            watcher.cancel()
        }
    }

    func subscribeToMessageInbox(tripETAId: String, completion: @escaping (Result<String>) -> Void) {
        if messageInboxWatcher != nil {
            unsubscribeToMessageInbox()
            messageInboxWatcher = nil
        }
        
        let subscriptionRequest = MessageInboxSubscription(tripId: tripETAId)
            
        do {
            let watcher = try appSyncClient?.subscribe(subscription: subscriptionRequest) { result, transaction, error in
                                
                guard error == nil else {
                    print("Error in subscription for \(tripETAId): \(error!.localizedDescription)")
                    completion(.failure(error!))
                    return
                }

                guard let result = result else {
                    print("Result unexpectedly nil in subscription for \(tripETAId)")
                    completion(.failure(AppSyncError.nilResult))
                    return
                }

                guard let data = result.data?.messageInbox?.message else {
                    print("GraphQL response data unexpectedly nil in subscription for \(tripETAId)")
                    completion(.failure(AppSyncError.nilGraphQLResponse))
                    return
                }
                print("subscribeToMessageInbox \(data)")
                completion(.success(data))
            }

            self.messageInboxWatcher = watcher
        } catch (let err) {
            print(err)
        }
    }
    
    func unsubscribeToMessageInbox() {
        if let watcher = messageInboxWatcher {
            watcher.cancel()
        }
    }
    
    /**
        Module: ETA
        Subscription to receive Live Location with trip ID
        The tripID of the current trip which is generated by UUID
        This method will be susbscribed when user wants to send Live Location to Recipient
        This is return for Testing Purpose. This Live Location event will be subscribed in the backend
     */
    func subscribeToUserLiveLocation(tripId: String, completion: @escaping (Result<RefreshUserTripLocationSubscription.Data>) -> Void) {
        
        if subscriptionWatchersLiveLocation[tripId] != nil { return }
        let subscriptionRequest = RefreshUserTripLocationSubscription(tripId: tripId)
        do {
            let watcher = try appSyncClient?.subscribe(subscription: subscriptionRequest) { result, transaction, error in
                                
                guard error == nil else {
                    print("Error in subscription for \(tripId): \(error!.localizedDescription)")
                    completion(.failure(error!))
                    return
                }

                guard let result = result else {
                    print("Result unexpectedly nil in subscription for \(tripId)")
                    completion(.failure(AppSyncError.nilResult))
                    return
                }

                guard let data = result.data else {
                    print("GraphQL response data unexpectedly nil in subscription for \(tripId)")
                    completion(.failure(AppSyncError.nilGraphQLResponse))
                    return
                }
                completion(.success(data))
            }

            subscriptionWatchersLiveLocation[tripId] = watcher
        } catch (let err) {
            print(err)
        }
        
    }
    
    /**
        Module: ETA
        Publishing/Updating to send Live Location with trip ID,latitude,longitude,arrivalTime
        The tripID of the current trip which is generated by UUID
        This method will be called from Turn-by-Turn when user wants to send Live Location to Recipient
     */
    func updateTripLiveLocation(tripId:String,latitude:String,longitude:String,status: String,arrivalTime:String,coordinates:String){
        
        let updateMutation = UpdateUserTripLocationMutation(tripId: tripId, latitude: latitude, longitude: longitude, status: status,arrivalTime: arrivalTime,coordinates: coordinates)
        
        appSyncClient?.perform(mutation: updateMutation, resultHandler:  {(result, error) in
            
            if(error != nil)
            {
                print("error",error?.localizedDescription)
            }
            else
            {
                print("Update",result)
            }
            
        })
        
    }
    
    func updateWalkathonLocation(userId: String, walkathonId: String, latitude: String, longitude: String, currentTime: String, data: String) {
        
        let updateMutation = UpdateWalkathonLocationMutation(userId: userId, walkathonId: walkathonId, latitude: latitude, longitude: longitude, currentTime: currentTime, data: data)
        
        appSyncClient?.perform(mutation: updateMutation, resultHandler:  {(result, error) in
            
            if(error != nil)
            {
                print("error",error?.localizedDescription)
            }
            else
            {
                print("Update",result)
            }
            
        })
    }
    
    func updateUserCurrentLocation(userId: String, bearing: String, latitude: String, longitude: String, data: String) {
        let updateMutation = UpdateDeviceUserLocationMutation(userId: userId, bearing: bearing, latitude: latitude, longitude: longitude, data: data)
        appSyncClient?.perform(mutation: updateMutation, resultHandler:  {(result, error) in
            
            if(error != nil)
            {
                print("error", error?.localizedDescription)
            }
            else
            {
                print("Update", result)
            }
            
        })
    }
    
    func updateOBUDeviceUserLocation(userId: String, bearing: String, latitude: String, longitude: String, data: String) {
        let updateMutation = UpdateObuDeviceUserLocationMutation(userId: userId, bearing: bearing, latitude: latitude, longitude: longitude, data: data)
        appSyncClient?.perform(mutation: updateMutation, resultHandler:  {(result, error) in
            
            if(error != nil)
            {
                print("error", error?.localizedDescription)
            }
            else
            {
                print("Update", result)
            }
            
        })
    }
    
    private func getAppSyncURL() -> String? {
        return (Bundle.main.infoDictionary?["App Sync Endpoint"] as? String)?
            .replacingOccurrences(of: "\\", with: "")
    }
    
    func unsubscribe(key: String) {
        if let watcher = subscriptionWatchers[key] {
            watcher.cancel()
        }
        
        if let watcher = subscriptionWatchersLiveLocation[key]{
            watcher.cancel()
        }
    }
    
    func unsubscribeAll() {
        subscriptionWatchers.forEach { (key, value) in
            value.cancel()
        }
        subscriptionWatchers.removeAll()
        
        subscriptionWatchersLiveLocation.forEach { (key, value) in
            value.cancel()
        }
        subscriptionWatchersLiveLocation.removeAll()
    }
}


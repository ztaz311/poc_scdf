//
//  RealtimeUpdater.swift
//  Breeze
//
//  Created by Zhou Hao on 29/6/21.
//

import Foundation

protocol RealtimeUpdater {
    func dataUpdated(json: String)
    func failedToUpdated()
}

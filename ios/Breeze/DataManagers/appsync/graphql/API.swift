//  This file was automatically generated and should not be edited.

import AWSAppSync

public final class UpdateBroadcastMessageMutation: GraphQLMutation {
  public static let operationString =
    "mutation UpdateBroadcastMessage($mode: String!, $data: String) {\n  updateBroadcastMessage(mode: $mode, data: $data) {\n    __typename\n    mode\n    data\n  }\n}"

  public var mode: String
  public var data: String?

  public init(mode: String, data: String? = nil) {
    self.mode = mode
    self.data = data
  }

  public var variables: GraphQLMap? {
    return ["mode": mode, "data": data]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("updateBroadcastMessage", arguments: ["mode": GraphQLVariable("mode"), "data": GraphQLVariable("data")], type: .object(UpdateBroadcastMessage.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(updateBroadcastMessage: UpdateBroadcastMessage? = nil) {
      self.init(snapshot: ["__typename": "Mutation", "updateBroadcastMessage": updateBroadcastMessage.flatMap { $0.snapshot }])
    }

    public var updateBroadcastMessage: UpdateBroadcastMessage? {
      get {
        return (snapshot["updateBroadcastMessage"] as? Snapshot).flatMap { UpdateBroadcastMessage(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "updateBroadcastMessage")
      }
    }

    public struct UpdateBroadcastMessage: GraphQLSelectionSet {
      public static let possibleTypes = ["UserBroadcastMessage"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("mode", type: .nonNull(.scalar(String.self))),
        GraphQLField("data", type: .scalar(String.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(mode: String, data: String? = nil) {
        self.init(snapshot: ["__typename": "UserBroadcastMessage", "mode": mode, "data": data])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var mode: String {
        get {
          return snapshot["mode"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "mode")
        }
      }

      public var data: String? {
        get {
          return snapshot["data"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "data")
        }
      }
    }
  }
}

public final class UpdateDataSetMutation: GraphQLMutation {
  public static let operationString =
    "mutation UpdateDataSet($notificationType: String!, $data: String!) {\n  updateDataSet(notificationType: $notificationType, data: $data) {\n    __typename\n    notificationType\n    data\n  }\n}"

  public var notificationType: String
  public var data: String

  public init(notificationType: String, data: String) {
    self.notificationType = notificationType
    self.data = data
  }

  public var variables: GraphQLMap? {
    return ["notificationType": notificationType, "data": data]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("updateDataSet", arguments: ["notificationType": GraphQLVariable("notificationType"), "data": GraphQLVariable("data")], type: .object(UpdateDataSet.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(updateDataSet: UpdateDataSet? = nil) {
      self.init(snapshot: ["__typename": "Mutation", "updateDataSet": updateDataSet.flatMap { $0.snapshot }])
    }

    public var updateDataSet: UpdateDataSet? {
      get {
        return (snapshot["updateDataSet"] as? Snapshot).flatMap { UpdateDataSet(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "updateDataSet")
      }
    }

    public struct UpdateDataSet: GraphQLSelectionSet {
      public static let possibleTypes = ["NotificationEvent"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("notificationType", type: .scalar(String.self)),
        GraphQLField("data", type: .scalar(String.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(notificationType: String? = nil, data: String? = nil) {
        self.init(snapshot: ["__typename": "NotificationEvent", "notificationType": notificationType, "data": data])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var notificationType: String? {
        get {
          return snapshot["notificationType"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "notificationType")
        }
      }

      public var data: String? {
        get {
          return snapshot["data"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "data")
        }
      }
    }
  }
}

public final class UpdateUserTripLocationMutation: GraphQLMutation {
  public static let operationString =
    "mutation UpdateUserTripLocation($tripId: String!, $latitude: String, $longitude: String, $status: String, $arrivalTime: String, $coordinates: String) {\n  updateUserTripLocation(tripId: $tripId, latitude: $latitude, longitude: $longitude, status: $status, arrivalTime: $arrivalTime, coordinates: $coordinates) {\n    __typename\n    tripId\n    latitude\n    longitude\n    status\n    arrivalTime\n    coordinates\n  }\n}"

  public var tripId: String
  public var latitude: String?
  public var longitude: String?
  public var status: String?
  public var arrivalTime: String?
  public var coordinates: String?

  public init(tripId: String, latitude: String? = nil, longitude: String? = nil, status: String? = nil, arrivalTime: String? = nil, coordinates: String? = nil) {
    self.tripId = tripId
    self.latitude = latitude
    self.longitude = longitude
    self.status = status
    self.arrivalTime = arrivalTime
    self.coordinates = coordinates
  }

  public var variables: GraphQLMap? {
    return ["tripId": tripId, "latitude": latitude, "longitude": longitude, "status": status, "arrivalTime": arrivalTime, "coordinates": coordinates]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("updateUserTripLocation", arguments: ["tripId": GraphQLVariable("tripId"), "latitude": GraphQLVariable("latitude"), "longitude": GraphQLVariable("longitude"), "status": GraphQLVariable("status"), "arrivalTime": GraphQLVariable("arrivalTime"), "coordinates": GraphQLVariable("coordinates")], type: .object(UpdateUserTripLocation.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(updateUserTripLocation: UpdateUserTripLocation? = nil) {
      self.init(snapshot: ["__typename": "Mutation", "updateUserTripLocation": updateUserTripLocation.flatMap { $0.snapshot }])
    }

    public var updateUserTripLocation: UpdateUserTripLocation? {
      get {
        return (snapshot["updateUserTripLocation"] as? Snapshot).flatMap { UpdateUserTripLocation(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "updateUserTripLocation")
      }
    }

    public struct UpdateUserTripLocation: GraphQLSelectionSet {
      public static let possibleTypes = ["UserTripLiveLocation"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("tripId", type: .nonNull(.scalar(String.self))),
        GraphQLField("latitude", type: .scalar(String.self)),
        GraphQLField("longitude", type: .scalar(String.self)),
        GraphQLField("status", type: .scalar(String.self)),
        GraphQLField("arrivalTime", type: .scalar(String.self)),
        GraphQLField("coordinates", type: .scalar(String.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(tripId: String, latitude: String? = nil, longitude: String? = nil, status: String? = nil, arrivalTime: String? = nil, coordinates: String? = nil) {
        self.init(snapshot: ["__typename": "UserTripLiveLocation", "tripId": tripId, "latitude": latitude, "longitude": longitude, "status": status, "arrivalTime": arrivalTime, "coordinates": coordinates])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var tripId: String {
        get {
          return snapshot["tripId"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "tripId")
        }
      }

      public var latitude: String? {
        get {
          return snapshot["latitude"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "latitude")
        }
      }

      public var longitude: String? {
        get {
          return snapshot["longitude"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "longitude")
        }
      }

      public var status: String? {
        get {
          return snapshot["status"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "status")
        }
      }

      public var arrivalTime: String? {
        get {
          return snapshot["arrivalTime"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "arrivalTime")
        }
      }

      public var coordinates: String? {
        get {
          return snapshot["coordinates"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "coordinates")
        }
      }
    }
  }
}

public final class SendMessageMutation: GraphQLMutation {
  public static let operationString =
    "mutation SendMessage($tripId: String!, $message: String) {\n  sendMessage(tripId: $tripId, message: $message) {\n    __typename\n    tripId\n    message\n  }\n}"

  public var tripId: String
  public var message: String?

  public init(tripId: String, message: String? = nil) {
    self.tripId = tripId
    self.message = message
  }

  public var variables: GraphQLMap? {
    return ["tripId": tripId, "message": message]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("sendMessage", arguments: ["tripId": GraphQLVariable("tripId"), "message": GraphQLVariable("message")], type: .object(SendMessage.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(sendMessage: SendMessage? = nil) {
      self.init(snapshot: ["__typename": "Mutation", "sendMessage": sendMessage.flatMap { $0.snapshot }])
    }

    public var sendMessage: SendMessage? {
      get {
        return (snapshot["sendMessage"] as? Snapshot).flatMap { SendMessage(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "sendMessage")
      }
    }

    public struct SendMessage: GraphQLSelectionSet {
      public static let possibleTypes = ["UserMessage"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("tripId", type: .nonNull(.scalar(String.self))),
        GraphQLField("message", type: .scalar(String.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(tripId: String, message: String? = nil) {
        self.init(snapshot: ["__typename": "UserMessage", "tripId": tripId, "message": message])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var tripId: String {
        get {
          return snapshot["tripId"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "tripId")
        }
      }

      public var message: String? {
        get {
          return snapshot["message"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "message")
        }
      }
    }
  }
}

public final class SendUserNotificationMutation: GraphQLMutation {
  public static let operationString =
    "mutation SendUserNotification($cognitoUserId: String!, $notificationType: String, $data: String) {\n  sendUserNotification(cognitoUserId: $cognitoUserId, notificationType: $notificationType, data: $data) {\n    __typename\n    cognitoUserId\n    notificationType\n    data\n  }\n}"

  public var cognitoUserId: String
  public var notificationType: String?
  public var data: String?

  public init(cognitoUserId: String, notificationType: String? = nil, data: String? = nil) {
    self.cognitoUserId = cognitoUserId
    self.notificationType = notificationType
    self.data = data
  }

  public var variables: GraphQLMap? {
    return ["cognitoUserId": cognitoUserId, "notificationType": notificationType, "data": data]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("sendUserNotification", arguments: ["cognitoUserId": GraphQLVariable("cognitoUserId"), "notificationType": GraphQLVariable("notificationType"), "data": GraphQLVariable("data")], type: .object(SendUserNotification.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(sendUserNotification: SendUserNotification? = nil) {
      self.init(snapshot: ["__typename": "Mutation", "sendUserNotification": sendUserNotification.flatMap { $0.snapshot }])
    }

    public var sendUserNotification: SendUserNotification? {
      get {
        return (snapshot["sendUserNotification"] as? Snapshot).flatMap { SendUserNotification(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "sendUserNotification")
      }
    }

    public struct SendUserNotification: GraphQLSelectionSet {
      public static let possibleTypes = ["UserNotification"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("cognitoUserId", type: .nonNull(.scalar(String.self))),
        GraphQLField("notificationType", type: .scalar(String.self)),
        GraphQLField("data", type: .scalar(String.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(cognitoUserId: String, notificationType: String? = nil, data: String? = nil) {
        self.init(snapshot: ["__typename": "UserNotification", "cognitoUserId": cognitoUserId, "notificationType": notificationType, "data": data])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var cognitoUserId: String {
        get {
          return snapshot["cognitoUserId"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "cognitoUserId")
        }
      }

      public var notificationType: String? {
        get {
          return snapshot["notificationType"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "notificationType")
        }
      }

      public var data: String? {
        get {
          return snapshot["data"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "data")
        }
      }
    }
  }
}

public final class UpdateWalkathonLocationMutation: GraphQLMutation {
  public static let operationString =
    "mutation UpdateWalkathonLocation($userId: String!, $walkathonId: String, $latitude: String, $longitude: String, $currentTime: String, $data: String) {\n  updateWalkathonLocation(userId: $userId, walkathonId: $walkathonId, latitude: $latitude, longitude: $longitude, currentTime: $currentTime, data: $data) {\n    __typename\n    userId\n    walkathonId\n    latitude\n    longitude\n    currentTime\n    data\n  }\n}"

  public var userId: String
  public var walkathonId: String?
  public var latitude: String?
  public var longitude: String?
  public var currentTime: String?
  public var data: String?

  public init(userId: String, walkathonId: String? = nil, latitude: String? = nil, longitude: String? = nil, currentTime: String? = nil, data: String? = nil) {
    self.userId = userId
    self.walkathonId = walkathonId
    self.latitude = latitude
    self.longitude = longitude
    self.currentTime = currentTime
    self.data = data
  }

  public var variables: GraphQLMap? {
    return ["userId": userId, "walkathonId": walkathonId, "latitude": latitude, "longitude": longitude, "currentTime": currentTime, "data": data]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("updateWalkathonLocation", arguments: ["userId": GraphQLVariable("userId"), "walkathonId": GraphQLVariable("walkathonId"), "latitude": GraphQLVariable("latitude"), "longitude": GraphQLVariable("longitude"), "currentTime": GraphQLVariable("currentTime"), "data": GraphQLVariable("data")], type: .object(UpdateWalkathonLocation.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(updateWalkathonLocation: UpdateWalkathonLocation? = nil) {
      self.init(snapshot: ["__typename": "Mutation", "updateWalkathonLocation": updateWalkathonLocation.flatMap { $0.snapshot }])
    }

    public var updateWalkathonLocation: UpdateWalkathonLocation? {
      get {
        return (snapshot["updateWalkathonLocation"] as? Snapshot).flatMap { UpdateWalkathonLocation(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "updateWalkathonLocation")
      }
    }

    public struct UpdateWalkathonLocation: GraphQLSelectionSet {
      public static let possibleTypes = ["WalkLiveLocation"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("userId", type: .nonNull(.scalar(String.self))),
        GraphQLField("walkathonId", type: .scalar(String.self)),
        GraphQLField("latitude", type: .scalar(String.self)),
        GraphQLField("longitude", type: .scalar(String.self)),
        GraphQLField("currentTime", type: .scalar(String.self)),
        GraphQLField("data", type: .scalar(String.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(userId: String, walkathonId: String? = nil, latitude: String? = nil, longitude: String? = nil, currentTime: String? = nil, data: String? = nil) {
        self.init(snapshot: ["__typename": "WalkLiveLocation", "userId": userId, "walkathonId": walkathonId, "latitude": latitude, "longitude": longitude, "currentTime": currentTime, "data": data])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: String {
        get {
          return snapshot["userId"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "userId")
        }
      }

      public var walkathonId: String? {
        get {
          return snapshot["walkathonId"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "walkathonId")
        }
      }

      public var latitude: String? {
        get {
          return snapshot["latitude"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "latitude")
        }
      }

      public var longitude: String? {
        get {
          return snapshot["longitude"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "longitude")
        }
      }

      public var currentTime: String? {
        get {
          return snapshot["currentTime"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "currentTime")
        }
      }

      public var data: String? {
        get {
          return snapshot["data"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "data")
        }
      }
    }
  }
}

public final class UpdateDeviceUserLocationMutation: GraphQLMutation {
  public static let operationString =
    "mutation UpdateDeviceUserLocation($userId: String!, $bearing: String, $latitude: String, $longitude: String, $data: String) {\n  updateDeviceUserLocation(userId: $userId, bearing: $bearing, latitude: $latitude, longitude: $longitude, data: $data) {\n    __typename\n    userId\n    bearing\n    latitude\n    longitude\n    data\n  }\n}"

  public var userId: String
  public var bearing: String?
  public var latitude: String?
  public var longitude: String?
  public var data: String?

  public init(userId: String, bearing: String? = nil, latitude: String? = nil, longitude: String? = nil, data: String? = nil) {
    self.userId = userId
    self.bearing = bearing
    self.latitude = latitude
    self.longitude = longitude
    self.data = data
  }

  public var variables: GraphQLMap? {
    return ["userId": userId, "bearing": bearing, "latitude": latitude, "longitude": longitude, "data": data]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("updateDeviceUserLocation", arguments: ["userId": GraphQLVariable("userId"), "bearing": GraphQLVariable("bearing"), "latitude": GraphQLVariable("latitude"), "longitude": GraphQLVariable("longitude"), "data": GraphQLVariable("data")], type: .object(UpdateDeviceUserLocation.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(updateDeviceUserLocation: UpdateDeviceUserLocation? = nil) {
      self.init(snapshot: ["__typename": "Mutation", "updateDeviceUserLocation": updateDeviceUserLocation.flatMap { $0.snapshot }])
    }

    public var updateDeviceUserLocation: UpdateDeviceUserLocation? {
      get {
        return (snapshot["updateDeviceUserLocation"] as? Snapshot).flatMap { UpdateDeviceUserLocation(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "updateDeviceUserLocation")
      }
    }

    public struct UpdateDeviceUserLocation: GraphQLSelectionSet {
      public static let possibleTypes = ["DeviceUserLocation"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("userId", type: .nonNull(.scalar(String.self))),
        GraphQLField("bearing", type: .scalar(String.self)),
        GraphQLField("latitude", type: .scalar(String.self)),
        GraphQLField("longitude", type: .scalar(String.self)),
        GraphQLField("data", type: .scalar(String.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(userId: String, bearing: String? = nil, latitude: String? = nil, longitude: String? = nil, data: String? = nil) {
        self.init(snapshot: ["__typename": "DeviceUserLocation", "userId": userId, "bearing": bearing, "latitude": latitude, "longitude": longitude, "data": data])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: String {
        get {
          return snapshot["userId"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "userId")
        }
      }

      public var bearing: String? {
        get {
          return snapshot["bearing"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "bearing")
        }
      }

      public var latitude: String? {
        get {
          return snapshot["latitude"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "latitude")
        }
      }

      public var longitude: String? {
        get {
          return snapshot["longitude"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "longitude")
        }
      }

      public var data: String? {
        get {
          return snapshot["data"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "data")
        }
      }
    }
  }
}

public final class UpdateObuDeviceUserLocationMutation: GraphQLMutation {
  public static let operationString =
    "mutation UpdateOBUDeviceUserLocation($userId: String!, $bearing: String, $latitude: String, $longitude: String, $data: String) {\n  updateOBUDeviceUserLocation(userId: $userId, bearing: $bearing, latitude: $latitude, longitude: $longitude, data: $data) {\n    __typename\n    userId\n    bearing\n    latitude\n    longitude\n    data\n  }\n}"

  public var userId: String
  public var bearing: String?
  public var latitude: String?
  public var longitude: String?
  public var data: String?

  public init(userId: String, bearing: String? = nil, latitude: String? = nil, longitude: String? = nil, data: String? = nil) {
    self.userId = userId
    self.bearing = bearing
    self.latitude = latitude
    self.longitude = longitude
    self.data = data
  }

  public var variables: GraphQLMap? {
    return ["userId": userId, "bearing": bearing, "latitude": latitude, "longitude": longitude, "data": data]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("updateOBUDeviceUserLocation", arguments: ["userId": GraphQLVariable("userId"), "bearing": GraphQLVariable("bearing"), "latitude": GraphQLVariable("latitude"), "longitude": GraphQLVariable("longitude"), "data": GraphQLVariable("data")], type: .object(UpdateObuDeviceUserLocation.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(updateObuDeviceUserLocation: UpdateObuDeviceUserLocation? = nil) {
      self.init(snapshot: ["__typename": "Mutation", "updateOBUDeviceUserLocation": updateObuDeviceUserLocation.flatMap { $0.snapshot }])
    }

    public var updateObuDeviceUserLocation: UpdateObuDeviceUserLocation? {
      get {
        return (snapshot["updateOBUDeviceUserLocation"] as? Snapshot).flatMap { UpdateObuDeviceUserLocation(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "updateOBUDeviceUserLocation")
      }
    }

    public struct UpdateObuDeviceUserLocation: GraphQLSelectionSet {
      public static let possibleTypes = ["DeviceUserLocation"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("userId", type: .nonNull(.scalar(String.self))),
        GraphQLField("bearing", type: .scalar(String.self)),
        GraphQLField("latitude", type: .scalar(String.self)),
        GraphQLField("longitude", type: .scalar(String.self)),
        GraphQLField("data", type: .scalar(String.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(userId: String, bearing: String? = nil, latitude: String? = nil, longitude: String? = nil, data: String? = nil) {
        self.init(snapshot: ["__typename": "DeviceUserLocation", "userId": userId, "bearing": bearing, "latitude": latitude, "longitude": longitude, "data": data])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: String {
        get {
          return snapshot["userId"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "userId")
        }
      }

      public var bearing: String? {
        get {
          return snapshot["bearing"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "bearing")
        }
      }

      public var latitude: String? {
        get {
          return snapshot["latitude"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "latitude")
        }
      }

      public var longitude: String? {
        get {
          return snapshot["longitude"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "longitude")
        }
      }

      public var data: String? {
        get {
          return snapshot["data"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "data")
        }
      }
    }
  }
}

public final class UpdateMockEventMutation: GraphQLMutation {
  public static let operationString =
    "mutation UpdateMockEvent($userId: String!, $eventType: String, $message: String, $distance: String, $chargeAmount: String, $detailMessage: String, $data: String) {\n  updateMockEvent(userId: $userId, eventType: $eventType, message: $message, distance: $distance, chargeAmount: $chargeAmount, detailMessage: $detailMessage, data: $data) {\n    __typename\n    userId\n    eventType\n    message\n    distance\n    chargeAmount\n    detailMessage\n    data\n  }\n}"

  public var userId: String
  public var eventType: String?
  public var message: String?
  public var distance: String?
  public var chargeAmount: String?
  public var detailMessage: String?
  public var data: String?

  public init(userId: String, eventType: String? = nil, message: String? = nil, distance: String? = nil, chargeAmount: String? = nil, detailMessage: String? = nil, data: String? = nil) {
    self.userId = userId
    self.eventType = eventType
    self.message = message
    self.distance = distance
    self.chargeAmount = chargeAmount
    self.detailMessage = detailMessage
    self.data = data
  }

  public var variables: GraphQLMap? {
    return ["userId": userId, "eventType": eventType, "message": message, "distance": distance, "chargeAmount": chargeAmount, "detailMessage": detailMessage, "data": data]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("updateMockEvent", arguments: ["userId": GraphQLVariable("userId"), "eventType": GraphQLVariable("eventType"), "message": GraphQLVariable("message"), "distance": GraphQLVariable("distance"), "chargeAmount": GraphQLVariable("chargeAmount"), "detailMessage": GraphQLVariable("detailMessage"), "data": GraphQLVariable("data")], type: .object(UpdateMockEvent.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(updateMockEvent: UpdateMockEvent? = nil) {
      self.init(snapshot: ["__typename": "Mutation", "updateMockEvent": updateMockEvent.flatMap { $0.snapshot }])
    }

    public var updateMockEvent: UpdateMockEvent? {
      get {
        return (snapshot["updateMockEvent"] as? Snapshot).flatMap { UpdateMockEvent(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "updateMockEvent")
      }
    }

    public struct UpdateMockEvent: GraphQLSelectionSet {
      public static let possibleTypes = ["MockEvent"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("userId", type: .nonNull(.scalar(String.self))),
        GraphQLField("eventType", type: .scalar(String.self)),
        GraphQLField("message", type: .scalar(String.self)),
        GraphQLField("distance", type: .scalar(String.self)),
        GraphQLField("chargeAmount", type: .scalar(String.self)),
        GraphQLField("detailMessage", type: .scalar(String.self)),
        GraphQLField("data", type: .scalar(String.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(userId: String, eventType: String? = nil, message: String? = nil, distance: String? = nil, chargeAmount: String? = nil, detailMessage: String? = nil, data: String? = nil) {
        self.init(snapshot: ["__typename": "MockEvent", "userId": userId, "eventType": eventType, "message": message, "distance": distance, "chargeAmount": chargeAmount, "detailMessage": detailMessage, "data": data])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: String {
        get {
          return snapshot["userId"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "userId")
        }
      }

      public var eventType: String? {
        get {
          return snapshot["eventType"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "eventType")
        }
      }

      public var message: String? {
        get {
          return snapshot["message"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "message")
        }
      }

      public var distance: String? {
        get {
          return snapshot["distance"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "distance")
        }
      }

      public var chargeAmount: String? {
        get {
          return snapshot["chargeAmount"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "chargeAmount")
        }
      }

      public var detailMessage: String? {
        get {
          return snapshot["detailMessage"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "detailMessage")
        }
      }

      public var data: String? {
        get {
          return snapshot["data"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "data")
        }
      }
    }
  }
}

public final class UpdateObuEventMutation: GraphQLMutation {
  public static let operationString =
    "mutation UpdateOBUEvent($userId: String!, $eventType: String, $data: String) {\n  updateOBUEvent(userId: $userId, eventType: $eventType, data: $data) {\n    __typename\n    userId\n    eventType\n    data\n  }\n}"

  public var userId: String
  public var eventType: String?
  public var data: String?

  public init(userId: String, eventType: String? = nil, data: String? = nil) {
    self.userId = userId
    self.eventType = eventType
    self.data = data
  }

  public var variables: GraphQLMap? {
    return ["userId": userId, "eventType": eventType, "data": data]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("updateOBUEvent", arguments: ["userId": GraphQLVariable("userId"), "eventType": GraphQLVariable("eventType"), "data": GraphQLVariable("data")], type: .object(UpdateObuEvent.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(updateObuEvent: UpdateObuEvent? = nil) {
      self.init(snapshot: ["__typename": "Mutation", "updateOBUEvent": updateObuEvent.flatMap { $0.snapshot }])
    }

    public var updateObuEvent: UpdateObuEvent? {
      get {
        return (snapshot["updateOBUEvent"] as? Snapshot).flatMap { UpdateObuEvent(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "updateOBUEvent")
      }
    }

    public struct UpdateObuEvent: GraphQLSelectionSet {
      public static let possibleTypes = ["OBUEvent"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("userId", type: .nonNull(.scalar(String.self))),
        GraphQLField("eventType", type: .scalar(String.self)),
        GraphQLField("data", type: .scalar(String.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(userId: String, eventType: String? = nil, data: String? = nil) {
        self.init(snapshot: ["__typename": "OBUEvent", "userId": userId, "eventType": eventType, "data": data])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: String {
        get {
          return snapshot["userId"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "userId")
        }
      }

      public var eventType: String? {
        get {
          return snapshot["eventType"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "eventType")
        }
      }

      public var data: String? {
        get {
          return snapshot["data"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "data")
        }
      }
    }
  }
}

public final class GetIncidentsQuery: GraphQLQuery {
  public static let operationString =
    "query GetIncidents {\n  getIncidents {\n    __typename\n    notificationType\n    data\n  }\n}"

  public init() {
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("getIncidents", type: .nonNull(.object(GetIncident.selections))),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(getIncidents: GetIncident) {
      self.init(snapshot: ["__typename": "Query", "getIncidents": getIncidents.snapshot])
    }

    public var getIncidents: GetIncident {
      get {
        return GetIncident(snapshot: snapshot["getIncidents"]! as! Snapshot)
      }
      set {
        snapshot.updateValue(newValue.snapshot, forKey: "getIncidents")
      }
    }

    public struct GetIncident: GraphQLSelectionSet {
      public static let possibleTypes = ["NotificationEvent"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("notificationType", type: .scalar(String.self)),
        GraphQLField("data", type: .scalar(String.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(notificationType: String? = nil, data: String? = nil) {
        self.init(snapshot: ["__typename": "NotificationEvent", "notificationType": notificationType, "data": data])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var notificationType: String? {
        get {
          return snapshot["notificationType"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "notificationType")
        }
      }

      public var data: String? {
        get {
          return snapshot["data"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "data")
        }
      }
    }
  }
}

public final class GetMessagesQuery: GraphQLQuery {
  public static let operationString =
    "query GetMessages {\n  getMessages {\n    __typename\n    tripId\n    message\n  }\n}"

  public init() {
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("getMessages", type: .object(GetMessage.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(getMessages: GetMessage? = nil) {
      self.init(snapshot: ["__typename": "Query", "getMessages": getMessages.flatMap { $0.snapshot }])
    }

    public var getMessages: GetMessage? {
      get {
        return (snapshot["getMessages"] as? Snapshot).flatMap { GetMessage(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "getMessages")
      }
    }

    public struct GetMessage: GraphQLSelectionSet {
      public static let possibleTypes = ["UserMessage"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("tripId", type: .nonNull(.scalar(String.self))),
        GraphQLField("message", type: .scalar(String.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(tripId: String, message: String? = nil) {
        self.init(snapshot: ["__typename": "UserMessage", "tripId": tripId, "message": message])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var tripId: String {
        get {
          return snapshot["tripId"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "tripId")
        }
      }

      public var message: String? {
        get {
          return snapshot["message"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "message")
        }
      }
    }
  }
}

public final class GetWalkLocationstQuery: GraphQLQuery {
  public static let operationString =
    "query GetWalkLocationst($userId: String!) {\n  getWalkLocationst(userId: $userId) {\n    __typename\n    userId\n    walkathonId\n    latitude\n    longitude\n    currentTime\n    data\n  }\n}"

  public var userId: String

  public init(userId: String) {
    self.userId = userId
  }

  public var variables: GraphQLMap? {
    return ["userId": userId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("getWalkLocationst", arguments: ["userId": GraphQLVariable("userId")], type: .object(GetWalkLocationst.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(getWalkLocationst: GetWalkLocationst? = nil) {
      self.init(snapshot: ["__typename": "Query", "getWalkLocationst": getWalkLocationst.flatMap { $0.snapshot }])
    }

    public var getWalkLocationst: GetWalkLocationst? {
      get {
        return (snapshot["getWalkLocationst"] as? Snapshot).flatMap { GetWalkLocationst(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "getWalkLocationst")
      }
    }

    public struct GetWalkLocationst: GraphQLSelectionSet {
      public static let possibleTypes = ["WalkLiveLocation"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("userId", type: .nonNull(.scalar(String.self))),
        GraphQLField("walkathonId", type: .scalar(String.self)),
        GraphQLField("latitude", type: .scalar(String.self)),
        GraphQLField("longitude", type: .scalar(String.self)),
        GraphQLField("currentTime", type: .scalar(String.self)),
        GraphQLField("data", type: .scalar(String.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(userId: String, walkathonId: String? = nil, latitude: String? = nil, longitude: String? = nil, currentTime: String? = nil, data: String? = nil) {
        self.init(snapshot: ["__typename": "WalkLiveLocation", "userId": userId, "walkathonId": walkathonId, "latitude": latitude, "longitude": longitude, "currentTime": currentTime, "data": data])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: String {
        get {
          return snapshot["userId"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "userId")
        }
      }

      public var walkathonId: String? {
        get {
          return snapshot["walkathonId"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "walkathonId")
        }
      }

      public var latitude: String? {
        get {
          return snapshot["latitude"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "latitude")
        }
      }

      public var longitude: String? {
        get {
          return snapshot["longitude"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "longitude")
        }
      }

      public var currentTime: String? {
        get {
          return snapshot["currentTime"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "currentTime")
        }
      }

      public var data: String? {
        get {
          return snapshot["data"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "data")
        }
      }
    }
  }
}

public final class RefreshDataSetSubscription: GraphQLSubscription {
  public static let operationString =
    "subscription RefreshDataSet($notificationType: String!) {\n  refreshDataSet(notificationType: $notificationType) {\n    __typename\n    notificationType\n    data\n  }\n}"

  public var notificationType: String

  public init(notificationType: String) {
    self.notificationType = notificationType
  }

  public var variables: GraphQLMap? {
    return ["notificationType": notificationType]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Subscription"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("refreshDataSet", arguments: ["notificationType": GraphQLVariable("notificationType")], type: .object(RefreshDataSet.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(refreshDataSet: RefreshDataSet? = nil) {
      self.init(snapshot: ["__typename": "Subscription", "refreshDataSet": refreshDataSet.flatMap { $0.snapshot }])
    }

    public var refreshDataSet: RefreshDataSet? {
      get {
        return (snapshot["refreshDataSet"] as? Snapshot).flatMap { RefreshDataSet(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "refreshDataSet")
      }
    }

    public struct RefreshDataSet: GraphQLSelectionSet {
      public static let possibleTypes = ["NotificationEvent"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("notificationType", type: .scalar(String.self)),
        GraphQLField("data", type: .scalar(String.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(notificationType: String? = nil, data: String? = nil) {
        self.init(snapshot: ["__typename": "NotificationEvent", "notificationType": notificationType, "data": data])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var notificationType: String? {
        get {
          return snapshot["notificationType"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "notificationType")
        }
      }

      public var data: String? {
        get {
          return snapshot["data"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "data")
        }
      }
    }
  }
}

public final class RefreshUserTripLocationSubscription: GraphQLSubscription {
  public static let operationString =
    "subscription RefreshUserTripLocation($tripId: String!) {\n  refreshUserTripLocation(tripId: $tripId) {\n    __typename\n    tripId\n    latitude\n    longitude\n    status\n    arrivalTime\n    coordinates\n  }\n}"

  public var tripId: String

  public init(tripId: String) {
    self.tripId = tripId
  }

  public var variables: GraphQLMap? {
    return ["tripId": tripId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Subscription"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("refreshUserTripLocation", arguments: ["tripId": GraphQLVariable("tripId")], type: .object(RefreshUserTripLocation.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(refreshUserTripLocation: RefreshUserTripLocation? = nil) {
      self.init(snapshot: ["__typename": "Subscription", "refreshUserTripLocation": refreshUserTripLocation.flatMap { $0.snapshot }])
    }

    public var refreshUserTripLocation: RefreshUserTripLocation? {
      get {
        return (snapshot["refreshUserTripLocation"] as? Snapshot).flatMap { RefreshUserTripLocation(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "refreshUserTripLocation")
      }
    }

    public struct RefreshUserTripLocation: GraphQLSelectionSet {
      public static let possibleTypes = ["UserTripLiveLocation"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("tripId", type: .nonNull(.scalar(String.self))),
        GraphQLField("latitude", type: .scalar(String.self)),
        GraphQLField("longitude", type: .scalar(String.self)),
        GraphQLField("status", type: .scalar(String.self)),
        GraphQLField("arrivalTime", type: .scalar(String.self)),
        GraphQLField("coordinates", type: .scalar(String.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(tripId: String, latitude: String? = nil, longitude: String? = nil, status: String? = nil, arrivalTime: String? = nil, coordinates: String? = nil) {
        self.init(snapshot: ["__typename": "UserTripLiveLocation", "tripId": tripId, "latitude": latitude, "longitude": longitude, "status": status, "arrivalTime": arrivalTime, "coordinates": coordinates])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var tripId: String {
        get {
          return snapshot["tripId"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "tripId")
        }
      }

      public var latitude: String? {
        get {
          return snapshot["latitude"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "latitude")
        }
      }

      public var longitude: String? {
        get {
          return snapshot["longitude"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "longitude")
        }
      }

      public var status: String? {
        get {
          return snapshot["status"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "status")
        }
      }

      public var arrivalTime: String? {
        get {
          return snapshot["arrivalTime"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "arrivalTime")
        }
      }

      public var coordinates: String? {
        get {
          return snapshot["coordinates"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "coordinates")
        }
      }
    }
  }
}

public final class MessageInboxSubscription: GraphQLSubscription {
  public static let operationString =
    "subscription MessageInbox($tripId: String!) {\n  messageInbox(tripId: $tripId) {\n    __typename\n    tripId\n    message\n  }\n}"

  public var tripId: String

  public init(tripId: String) {
    self.tripId = tripId
  }

  public var variables: GraphQLMap? {
    return ["tripId": tripId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Subscription"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("messageInbox", arguments: ["tripId": GraphQLVariable("tripId")], type: .object(MessageInbox.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(messageInbox: MessageInbox? = nil) {
      self.init(snapshot: ["__typename": "Subscription", "messageInbox": messageInbox.flatMap { $0.snapshot }])
    }

    public var messageInbox: MessageInbox? {
      get {
        return (snapshot["messageInbox"] as? Snapshot).flatMap { MessageInbox(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "messageInbox")
      }
    }

    public struct MessageInbox: GraphQLSelectionSet {
      public static let possibleTypes = ["UserMessage"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("tripId", type: .nonNull(.scalar(String.self))),
        GraphQLField("message", type: .scalar(String.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(tripId: String, message: String? = nil) {
        self.init(snapshot: ["__typename": "UserMessage", "tripId": tripId, "message": message])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var tripId: String {
        get {
          return snapshot["tripId"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "tripId")
        }
      }

      public var message: String? {
        get {
          return snapshot["message"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "message")
        }
      }
    }
  }
}

public final class RefreshBroadcastMessageSubscription: GraphQLSubscription {
  public static let operationString =
    "subscription RefreshBroadcastMessage($mode: String!) {\n  refreshBroadcastMessage(mode: $mode) {\n    __typename\n    mode\n    data\n  }\n}"

  public var mode: String

  public init(mode: String) {
    self.mode = mode
  }

  public var variables: GraphQLMap? {
    return ["mode": mode]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Subscription"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("refreshBroadcastMessage", arguments: ["mode": GraphQLVariable("mode")], type: .object(RefreshBroadcastMessage.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(refreshBroadcastMessage: RefreshBroadcastMessage? = nil) {
      self.init(snapshot: ["__typename": "Subscription", "refreshBroadcastMessage": refreshBroadcastMessage.flatMap { $0.snapshot }])
    }

    public var refreshBroadcastMessage: RefreshBroadcastMessage? {
      get {
        return (snapshot["refreshBroadcastMessage"] as? Snapshot).flatMap { RefreshBroadcastMessage(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "refreshBroadcastMessage")
      }
    }

    public struct RefreshBroadcastMessage: GraphQLSelectionSet {
      public static let possibleTypes = ["UserBroadcastMessage"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("mode", type: .nonNull(.scalar(String.self))),
        GraphQLField("data", type: .scalar(String.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(mode: String, data: String? = nil) {
        self.init(snapshot: ["__typename": "UserBroadcastMessage", "mode": mode, "data": data])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var mode: String {
        get {
          return snapshot["mode"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "mode")
        }
      }

      public var data: String? {
        get {
          return snapshot["data"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "data")
        }
      }
    }
  }
}

public final class NotifyUserSubscription: GraphQLSubscription {
  public static let operationString =
    "subscription NotifyUser($cognitoUserId: String!) {\n  notifyUser(cognitoUserId: $cognitoUserId) {\n    __typename\n    cognitoUserId\n    notificationType\n    data\n  }\n}"

  public var cognitoUserId: String

  public init(cognitoUserId: String) {
    self.cognitoUserId = cognitoUserId
  }

  public var variables: GraphQLMap? {
    return ["cognitoUserId": cognitoUserId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Subscription"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("notifyUser", arguments: ["cognitoUserId": GraphQLVariable("cognitoUserId")], type: .object(NotifyUser.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(notifyUser: NotifyUser? = nil) {
      self.init(snapshot: ["__typename": "Subscription", "notifyUser": notifyUser.flatMap { $0.snapshot }])
    }

    public var notifyUser: NotifyUser? {
      get {
        return (snapshot["notifyUser"] as? Snapshot).flatMap { NotifyUser(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "notifyUser")
      }
    }

    public struct NotifyUser: GraphQLSelectionSet {
      public static let possibleTypes = ["UserNotification"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("cognitoUserId", type: .nonNull(.scalar(String.self))),
        GraphQLField("notificationType", type: .scalar(String.self)),
        GraphQLField("data", type: .scalar(String.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(cognitoUserId: String, notificationType: String? = nil, data: String? = nil) {
        self.init(snapshot: ["__typename": "UserNotification", "cognitoUserId": cognitoUserId, "notificationType": notificationType, "data": data])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var cognitoUserId: String {
        get {
          return snapshot["cognitoUserId"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "cognitoUserId")
        }
      }

      public var notificationType: String? {
        get {
          return snapshot["notificationType"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "notificationType")
        }
      }

      public var data: String? {
        get {
          return snapshot["data"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "data")
        }
      }
    }
  }
}

public final class RefreshUserWalkathonLocationSubscription: GraphQLSubscription {
  public static let operationString =
    "subscription RefreshUserWalkathonLocation($userId: String!) {\n  refreshUserWalkathonLocation(userId: $userId) {\n    __typename\n    userId\n    walkathonId\n    latitude\n    longitude\n    currentTime\n    data\n  }\n}"

  public var userId: String

  public init(userId: String) {
    self.userId = userId
  }

  public var variables: GraphQLMap? {
    return ["userId": userId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Subscription"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("refreshUserWalkathonLocation", arguments: ["userId": GraphQLVariable("userId")], type: .object(RefreshUserWalkathonLocation.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(refreshUserWalkathonLocation: RefreshUserWalkathonLocation? = nil) {
      self.init(snapshot: ["__typename": "Subscription", "refreshUserWalkathonLocation": refreshUserWalkathonLocation.flatMap { $0.snapshot }])
    }

    public var refreshUserWalkathonLocation: RefreshUserWalkathonLocation? {
      get {
        return (snapshot["refreshUserWalkathonLocation"] as? Snapshot).flatMap { RefreshUserWalkathonLocation(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "refreshUserWalkathonLocation")
      }
    }

    public struct RefreshUserWalkathonLocation: GraphQLSelectionSet {
      public static let possibleTypes = ["WalkLiveLocation"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("userId", type: .nonNull(.scalar(String.self))),
        GraphQLField("walkathonId", type: .scalar(String.self)),
        GraphQLField("latitude", type: .scalar(String.self)),
        GraphQLField("longitude", type: .scalar(String.self)),
        GraphQLField("currentTime", type: .scalar(String.self)),
        GraphQLField("data", type: .scalar(String.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(userId: String, walkathonId: String? = nil, latitude: String? = nil, longitude: String? = nil, currentTime: String? = nil, data: String? = nil) {
        self.init(snapshot: ["__typename": "WalkLiveLocation", "userId": userId, "walkathonId": walkathonId, "latitude": latitude, "longitude": longitude, "currentTime": currentTime, "data": data])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: String {
        get {
          return snapshot["userId"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "userId")
        }
      }

      public var walkathonId: String? {
        get {
          return snapshot["walkathonId"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "walkathonId")
        }
      }

      public var latitude: String? {
        get {
          return snapshot["latitude"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "latitude")
        }
      }

      public var longitude: String? {
        get {
          return snapshot["longitude"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "longitude")
        }
      }

      public var currentTime: String? {
        get {
          return snapshot["currentTime"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "currentTime")
        }
      }

      public var data: String? {
        get {
          return snapshot["data"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "data")
        }
      }
    }
  }
}

public final class RefreshDeviceUserLocationSubscription: GraphQLSubscription {
  public static let operationString =
    "subscription RefreshDeviceUserLocation($userId: String!) {\n  refreshDeviceUserLocation(userId: $userId) {\n    __typename\n    userId\n    bearing\n    latitude\n    longitude\n    data\n  }\n}"

  public var userId: String

  public init(userId: String) {
    self.userId = userId
  }

  public var variables: GraphQLMap? {
    return ["userId": userId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Subscription"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("refreshDeviceUserLocation", arguments: ["userId": GraphQLVariable("userId")], type: .object(RefreshDeviceUserLocation.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(refreshDeviceUserLocation: RefreshDeviceUserLocation? = nil) {
      self.init(snapshot: ["__typename": "Subscription", "refreshDeviceUserLocation": refreshDeviceUserLocation.flatMap { $0.snapshot }])
    }

    public var refreshDeviceUserLocation: RefreshDeviceUserLocation? {
      get {
        return (snapshot["refreshDeviceUserLocation"] as? Snapshot).flatMap { RefreshDeviceUserLocation(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "refreshDeviceUserLocation")
      }
    }

    public struct RefreshDeviceUserLocation: GraphQLSelectionSet {
      public static let possibleTypes = ["DeviceUserLocation"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("userId", type: .nonNull(.scalar(String.self))),
        GraphQLField("bearing", type: .scalar(String.self)),
        GraphQLField("latitude", type: .scalar(String.self)),
        GraphQLField("longitude", type: .scalar(String.self)),
        GraphQLField("data", type: .scalar(String.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(userId: String, bearing: String? = nil, latitude: String? = nil, longitude: String? = nil, data: String? = nil) {
        self.init(snapshot: ["__typename": "DeviceUserLocation", "userId": userId, "bearing": bearing, "latitude": latitude, "longitude": longitude, "data": data])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: String {
        get {
          return snapshot["userId"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "userId")
        }
      }

      public var bearing: String? {
        get {
          return snapshot["bearing"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "bearing")
        }
      }

      public var latitude: String? {
        get {
          return snapshot["latitude"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "latitude")
        }
      }

      public var longitude: String? {
        get {
          return snapshot["longitude"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "longitude")
        }
      }

      public var data: String? {
        get {
          return snapshot["data"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "data")
        }
      }
    }
  }
}

public final class RefreshMockEventSubscription: GraphQLSubscription {
  public static let operationString =
    "subscription RefreshMockEvent($userId: String!) {\n  refreshMockEvent(userId: $userId) {\n    __typename\n    userId\n    eventType\n    message\n    distance\n    chargeAmount\n    detailMessage\n    data\n  }\n}"

  public var userId: String

  public init(userId: String) {
    self.userId = userId
  }

  public var variables: GraphQLMap? {
    return ["userId": userId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Subscription"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("refreshMockEvent", arguments: ["userId": GraphQLVariable("userId")], type: .object(RefreshMockEvent.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(refreshMockEvent: RefreshMockEvent? = nil) {
      self.init(snapshot: ["__typename": "Subscription", "refreshMockEvent": refreshMockEvent.flatMap { $0.snapshot }])
    }

    public var refreshMockEvent: RefreshMockEvent? {
      get {
        return (snapshot["refreshMockEvent"] as? Snapshot).flatMap { RefreshMockEvent(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "refreshMockEvent")
      }
    }

    public struct RefreshMockEvent: GraphQLSelectionSet {
      public static let possibleTypes = ["MockEvent"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("userId", type: .nonNull(.scalar(String.self))),
        GraphQLField("eventType", type: .scalar(String.self)),
        GraphQLField("message", type: .scalar(String.self)),
        GraphQLField("distance", type: .scalar(String.self)),
        GraphQLField("chargeAmount", type: .scalar(String.self)),
        GraphQLField("detailMessage", type: .scalar(String.self)),
        GraphQLField("data", type: .scalar(String.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(userId: String, eventType: String? = nil, message: String? = nil, distance: String? = nil, chargeAmount: String? = nil, detailMessage: String? = nil, data: String? = nil) {
        self.init(snapshot: ["__typename": "MockEvent", "userId": userId, "eventType": eventType, "message": message, "distance": distance, "chargeAmount": chargeAmount, "detailMessage": detailMessage, "data": data])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: String {
        get {
          return snapshot["userId"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "userId")
        }
      }

      public var eventType: String? {
        get {
          return snapshot["eventType"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "eventType")
        }
      }

      public var message: String? {
        get {
          return snapshot["message"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "message")
        }
      }

      public var distance: String? {
        get {
          return snapshot["distance"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "distance")
        }
      }

      public var chargeAmount: String? {
        get {
          return snapshot["chargeAmount"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "chargeAmount")
        }
      }

      public var detailMessage: String? {
        get {
          return snapshot["detailMessage"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "detailMessage")
        }
      }

      public var data: String? {
        get {
          return snapshot["data"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "data")
        }
      }
    }
  }
}

public final class RefreshObuEventSubscription: GraphQLSubscription {
  public static let operationString =
    "subscription RefreshOBUEvent($userId: String!) {\n  refreshOBUEvent(userId: $userId) {\n    __typename\n    userId\n    eventType\n    data\n  }\n}"

  public var userId: String

  public init(userId: String) {
    self.userId = userId
  }

  public var variables: GraphQLMap? {
    return ["userId": userId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Subscription"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("refreshOBUEvent", arguments: ["userId": GraphQLVariable("userId")], type: .object(RefreshObuEvent.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(refreshObuEvent: RefreshObuEvent? = nil) {
      self.init(snapshot: ["__typename": "Subscription", "refreshOBUEvent": refreshObuEvent.flatMap { $0.snapshot }])
    }

    public var refreshObuEvent: RefreshObuEvent? {
      get {
        return (snapshot["refreshOBUEvent"] as? Snapshot).flatMap { RefreshObuEvent(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "refreshOBUEvent")
      }
    }

    public struct RefreshObuEvent: GraphQLSelectionSet {
      public static let possibleTypes = ["OBUEvent"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("userId", type: .nonNull(.scalar(String.self))),
        GraphQLField("eventType", type: .scalar(String.self)),
        GraphQLField("data", type: .scalar(String.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(userId: String, eventType: String? = nil, data: String? = nil) {
        self.init(snapshot: ["__typename": "OBUEvent", "userId": userId, "eventType": eventType, "data": data])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: String {
        get {
          return snapshot["userId"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "userId")
        }
      }

      public var eventType: String? {
        get {
          return snapshot["eventType"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "eventType")
        }
      }

      public var data: String? {
        get {
          return snapshot["data"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "data")
        }
      }
    }
  }
}
//
//  VoiceInstructor.swift
//  Breeze
//
//  Created by Zhou Hao on 10/3/21.
//

import Foundation
import CoreLocation
import MapboxCoreNavigation
import MapboxDirections
import MapboxNavigation
import Turf
import SwiftyXMLParser
import CoreLocation
import SwiftyBeaver

final class VoiceInstructor {
    
    // NOTE: Could also load from a json file, the reason why I use dictionary instead of change automatically
    // is because we may set the value as we want
//    static let abbrWordsList = [
//        "PIE" : "P.I.E.",
//        "CTE" : "C.T.E.",
//        "KPE" : "K.P.E.",
//        "ECP" : "E.C.P.",
//        "AYE" : "A.Y.E.",
//        "TPE" : "T.P.E.",
//        "SLE" : "S.L.E.",
//        "KJE" : "K.J.E.",
//        "ERP" : "E.R.P.",
//        "MCE" : "M.C.E.",
//        "BKE" : "B.K.E.",
//        "NSC" : "N.S.C."
//    ]
    static let abbrWordsList = [String:String]()
    var synthesizer: SpeechSynthesizing!
    var spokenInstructionText: SpokenInstruction?
    
    init() {


        #if NEW_VOICE
        self.synthesizer = MultiplexedSpeechSynthesizer([RivaSpeechSynthesizer()/*,SystemSpeechSynthesizer()*/])
        self.synthesizer.locale = Locale(identifier: "en_GB")
        #else
        let accessToken = Bundle.main.object(forInfoDictionaryKey: "MGLMapboxAccessToken") as? String
        self.synthesizer = MultiplexedSpeechSynthesizer(accessToken: accessToken, host: "https://api.mapbox.com")
        self.synthesizer.locale = Locale.enGBLocale()
        //let accessToken = Bundle.main.object(forInfoDictionaryKey: "MGLMapboxAccessToken") as? String
        #endif
    }
    
    init(synthesizer: SpeechSynthesizing) {
        self.synthesizer = synthesizer
        self.synthesizer.locale = Locale.enGBLocale()
    }
            
    /// The `distanceAlongStep` will be used to decide which voice should be spoken out
    func speak(_ feature: FeatureDetectable, distanceAlongStep: CLLocationDistance = 0) {
        
        // Sample
        // let instruction = SpokenInstruction(distanceAlongStep: CLLocationDistance(0), text:"This is a sample.", ssmlText: "<speak><amazon:effect name=\"drc\"><prosody rate=\"1.08\">This is a <emphasis level=\"strong\">sample</emphasis> voice instruction</prosody></amazon:effect></speak>")
        
//        let text = "\(feature.type()) is \(String(format: "%d", Int(feature.distance))) meters ahead"
        #if DEMO
        if(DemoSession.shared.isDemoRoute)
        {
            SwiftyBeaver.debug("Feature Voice: \(feature.type())")
            let voices = DemoSession.shared.getDemoVoiceInstructions(type: feature.type())
            let text = voices.0
            let ssmlText = voices.1
            setSpokenInstructionText(text: text, ssmlText: ssmlText, distanceAlongStep: distanceAlongStep)
        }
        else
        {
            //let text = "\(feature.type()) ahead"
            let text = FeatureNotification.getVoiceText(for: feature)
            let ssmlText = "<speak><amazon:effect name=\"drc\"><prosody rate=\"1.08\">\(text)</prosody></amazon:effect></speak>"
            //let ssmlText = "<speak><amazon:effect name=\"drc\"><prosody rate=\"1.08\">\(feature.type()) ahead</prosody></amazon:effect></speak>"
            setSpokenInstructionText(text: text, ssmlText: ssmlText, distanceAlongStep: distanceAlongStep)
        }
        #else
           SwiftyBeaver.debug("Feature Voice Inside Voice Instructor class: \(feature.type())")
           let text = FeatureNotification.getVoiceText(for: feature)
           //let text = "\(feature.type()) ahead"
           let ssmlText = "<speak><amazon:effect name=\"drc\"><prosody rate=\"1.08\">\(text)</prosody></amazon:effect></speak>"
            setSpokenInstructionText(text: text, ssmlText: ssmlText, distanceAlongStep: distanceAlongStep)
        #endif
        
    }
        
    /// General speaking function
    func speak(text:String,distanceAlongStep: CLLocationDistance = 0){
        let ssmlText = "<speak><amazon:effect name=\"drc\"><prosody rate=\"1.08\">\(text)</prosody></amazon:effect></speak>"
         setSpokenInstructionText(text: text, ssmlText: ssmlText, distanceAlongStep: distanceAlongStep)
    }
    
    func setSpokenInstructionText(text:String,ssmlText:String, distanceAlongStep: CLLocationDistance = 0){
        
        //  If user disable voice then dont play instruction
        if !Prefs.shared.getBoolValue(.enableAudio) {
            return
        }
        
        let instruction = fixInstruction(SpokenInstruction(distanceAlongStep: distanceAlongStep, text:text, ssmlText: ssmlText))
        
        if self.synthesizer.isSpeaking && spokenInstructionText?.text == instruction.text {
            SwiftyBeaver.error("Current playing voice instruction is the same then ignore")
            return
        }

        let step = RouteStep(transportType: .automobile, maneuverLocation: CLLocationCoordinate2D(latitude: 0, longitude: 0), maneuverType: .depart, instructions: "", drivingSide: .left, distance: CLLocationDistance(0), expectedTravelTime: TimeInterval(0))
            
        spokenInstructionText = instruction
        
        self.synthesizer.speak(instruction, during: RouteLegProgress(leg: RouteLeg(steps: [step], name: "", distance: 0, expectedTravelTime: 0, profileIdentifier: .automobile)), locale: Locale(identifier: "en_GB"))
        
        
    }
    
    func willSpeak(_ instruction: SpokenInstruction, stepDistance: LocationDistance) -> SpokenInstruction? {

        let  instructionDistance = instruction.distanceAlongStep

        spokenInstructionText = nil
//        if spokenInstructionOverride(distance: stepDistance, disatnce1: instructionDistance) {
//
//            if spokenInstructionText != nil {
//                let tempInstruction = spokenInstructionText
//                spokenInstructionText = nil
//                return fixInstruction(tempInstruction!)
//            }
//            return nil
//        }
        
        // only check when there is a notification
        //if let tempInstruction = spokenInstructionText {
            
            if spokenInstructionOverride(distance: stepDistance, disatnce1: instructionDistance) {
                return fixInstruction(instruction)
            }
        //}
 
        return fixInstruction(instruction)
    }
    
    private func spokenInstructionOverride(distance:CLLocationDistance,disatnce1:CLLocationDistance) -> Bool{
        return distance == disatnce1
    }
    
    func fixInstruction(_ instruction: SpokenInstruction) -> SpokenInstruction {
        
        let text = replaceInText(instruction.text)
//        let ssml = instruction.ssmlText.isEmpty ? "" : replaceInSSMLText(instruction.ssmlText)
        var ssml = instruction.ssmlText
        if !instruction.ssmlText.contains("volume=") {
            /*
             https://www.w3.org/TR/speech-synthesis/#edef_prosody
             */
            ssml = instruction.ssmlText.replacingOccurrences(of: "<prosody", with: "<prosody volume=\"+6.0dB\"")
        }
        // previous foramt
//        let ssml = replaceInSSMLText("<speak><amazon:effect name=\"drc\"><prosody rate=\"1.08\">In 200 meters, Take the PIE ramp.</prosody></amazon:effect></speak>")

        // new format
//        let ssml = replaceInSSMLText("<speak><amazon:effect name=\"drc\"><prosody rate=\"1.08\">Drive northeast on <say-as interpret-as=\"address\">Adam Road</say-as>. Then, in 200 metres, Take the <say-as interpret-as=\"address\">PIE</say-as> ramp.</prosody></amazon:effect></speak>")
        
        return SpokenInstruction(distanceAlongStep: instruction.distanceAlongStep, text: text, ssmlText: ssml)
    }
    
    private func replaceInText(_ text: String, appendingDot: Bool = true) -> String {
        let split = text.components(separatedBy: CharacterSet.whitespacesAndNewlines)
        let array = split.map { theText -> String in
            // remove the leading and trailing punctuationCharacters
            var leadingPunctuations = ""
            var trailingPunctuations = ""
            var text = ""
            var isLeading = true // check leading first
            // NOTED: There is an exception for this algorithm when there are multiple punctuations in one word. But this should not happen
            theText.forEach { (c) in
                if c.isPunctuation {
                    if isLeading {
                        leadingPunctuations.append(c)
                    } else {
                        trailingPunctuations.append(c)
                    }
                } else {
                    if isLeading {
                        isLeading = false
                    }
                    text.append(c)
                }
            }
            let founded = VoiceInstructor.abbrWordsList[text]
            return founded == nil ? theText : leadingPunctuations + founded! + trailingPunctuations
        }
        print("Array: \(array)")
        let processedText = array.joined(separator: " ").trimmingCharacters(in: .whitespaces)
        if (processedText.last == "."){
            return processedText
        }
        return appendingDot ? processedText + "." : processedText
    }
    
    private func replaceInSSMLText(_ ssml: String) -> String {
        
        do {
            let xml = try XML.parse(ssml)
            var newXml = ssml
            
            // This is not the most efficient way to replace the xml text since there no way to replace only child element in this xml library
            if let text = xml["speak", "amazon:effect", "prosody"].text {
                let newText = replaceInText(text, appendingDot: false)
                newXml = newXml.replacingOccurrences(of: text, with: newText)
            }
            
            for sayAs in xml["speak", "amazon:effect", "prosody", "say-as"] {
                if let text = sayAs.text {
                    let newText = replaceInText(text, appendingDot: false)
                    newXml = newXml.replacingOccurrences(of: text, with: newText)
                }
            }
//            print(newXml)
            return newXml
        } catch(let error) {
            print("replaceInSSMText \(error.localizedDescription)")
        }
        
        return ""
    }
    

}

#if DEBUG
extension VoiceInstructor {
    func testReplaceInText(_ text: String, appendingDot: Bool = true) -> String {
        return replaceInText(text, appendingDot: appendingDot)
    }
    
    func testReplaceInSSMLText(_ ssml: String) -> String {
        return replaceInSSMLText(ssml)
    }
}
#endif


/*
// Built-in xml parser doesn't work since it's async
final class SSMLParser: NSObject, XMLParserDelegate {
    
    private var parser: XMLParser!
    private var fixedText: String = ""
    private var parsingDone = false
    let dispatchQueue = DispatchQueue(label: "xml_parsing_queue")
    let dispatchGroup  = DispatchGroup()
    
    init(ssml: String) {
        super.init()
        
        guard let data = ssml.data(using: .utf8) else { return }
        
        let parser = XMLParser(data: data)
        parser.delegate = self
        parser.parse()
    }
    
    func newText() -> String {
        
        dispatchQueue.async {
            self.dispatchGroup.enter()
            // Async function
            while parsingDone {
                self.dispatchGroup.leave()
            }
            return ""
            self.DispatchGroup.wait(timeout: .distantFuture)
        }
    }
    
    func parserDidStartDocument(_ parser: XMLParser) {
        
    }
    
    func parserDidEndDocument(_ parser: XMLParser) {
        
    }
    
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {
        print(elementName)
    }
    
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        print("\(elementName) end")
    }

    func parser(_: XMLParser, foundCharacters: String) {
        print("Characters = \(foundCharacters)")
    }
}
*/

protocol AudioQueuable {   
    func clearAudioQueue()
}

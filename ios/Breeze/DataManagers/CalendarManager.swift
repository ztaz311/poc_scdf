//
//  CalendarManager.swift
//  Breeze
//
//  Created by VishnuKanth on 28/01/21.
//

import Foundation
import UIKit
import EventKit
import Combine

// Using Combine Framework
final class CalendarManager: ObservableObject {
    
    // MARK: Properties
    var eventToBeMonitored: String = "" 
    
    // This is might not be the best to pass UIViewController here. The alternative solution is to
    // pass a protocol to showAlert. But for simplicity, we just pass a UIViewController as a weak
    // property for the time being.
    private weak var viewController: UIViewController!
    
    private let eventStore = EKEventStore()
    private var calendars: [EKCalendar]?
    private var cancellable: Cancellable?
    
    // The address from calendar for recommended address
    // Pick up the first one as the recommended address
    @Published private(set) var recommendedAddress: CalendarAddress?
    @Published private(set) var newDestinationAddress: CalendarAddress?
    
    // MARK: Initialization
    init(viewController: UIViewController) {
        self.viewController = viewController
        requestCalendarAuthorization()
        
        NotificationCenter.default.addObserver(self, selector: #selector(storeChanged), name: .EKEventStoreChanged, object: eventStore)
        
        // need a timer to check if the calend valid or not
        cancellable = Timer.publish(every: 120, on: .main, in: .default)
            .autoconnect()
            .sink(receiveValue: { _ in
                self.reloadData()
            })

    }
    
    deinit {
        cancellable?.cancel()
        NotificationCenter.default.removeObserver(self, name: .EKEventStoreChanged, object: eventStore)
    }
        
    // MARK: - public methods
    public func reloadData() {
        self.loadCalendars()
        self.loadEvents()
    }
    
//    public func isEmpty() -> Bool {
//        return addresses.count == 0
//    }

    // MARK: - private methods
    private func requestCalendarAuthorization() {
        
        let status = EKEventStore.authorizationStatus(for: EKEntityType.event)
                
        switch (status) {
        case EKAuthorizationStatus.notDetermined:
            AnalyticsManager.shared.logOpenPopupEvent(popupName: ParameterName.home_calendar_permission, screenName: ParameterName.home_screen_name)
            requestAccessToCalendar()
            
        case EKAuthorizationStatus.authorized:
            AnalyticsManager.shared.logClosePopupEvent(popupName: ParameterName.home_calendar_permission, screenName: ParameterName.home_screen_name)
            reloadData()
            
        case EKAuthorizationStatus.restricted, EKAuthorizationStatus.denied:
            break
            //showAuthorizationAlert()
        
        @unknown default:
            break
        }
    }
    
    private func requestAccessToCalendar() {
        EKEventStore().requestAccess(to: .event, completion: {
            (accessGranted: Bool, error: Error?) in
            
            if accessGranted == true {
//                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Home.UserClick.calendar_permission_allow, screenName: ParameterName.Home.screen_view)
                DispatchQueue.main.async(execute: {
                    self.reloadData()
                })
            } else {
//                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Home.UserClick.calendar_permission_deny, screenName: ParameterName.Home.screen_view)
                DispatchQueue.main.async(execute: {
                    self.showAuthorizationAlert()
                })
            }
        })
    }
    
    @objc private func storeChanged(notification: NSNotification) {
        reloadData()
    }

    private func showAuthorizationAlert(){
        
        AnalyticsManager.shared.logOpenPopupEvent(popupName: ParameterName.Home.UserPopup.calendar, screenName: ParameterName.Home.screen_view)
        self.viewController.popupAlert(title: Constants.hi, message: Constants.BreezeCalendarPermission, actionTitles: [Constants.goToSettings,Constants.cancel], actions:[{action1 in
            self.viewController.dismissAnyAlertControllerIfPresent()
            self.goToSettingsButtonTapped()
            AnalyticsManager.shared.logClosePopupEvent(popupName: ParameterName.Home.UserPopup.calendar, screenName: ParameterName.Home.screen_view)
        },{action2 in
            AnalyticsManager.shared.logClosePopupEvent(popupName: ParameterName.Home.UserPopup.calendar, screenName: ParameterName.Home.screen_view)
        }, nil])
    }
    
    private func goToSettingsButtonTapped() {
        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
            return
        }

        if UIApplication.shared.canOpenURL(settingsUrl) {
            UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                print("Settings opened: \(success)") // Prints true
            })
        }
    }
    
    private func loadCalendars() {
        self.calendars = EKEventStore().calendars(for: EKEntityType.event).sorted() { (cal1, cal2) -> Bool in
            return cal1.title < cal2.title
        }
    }
    
    private func loadEvents(){
        
        guard let calendars = calendars else { return }
        
//        self.addresses = []
        var events = [EKEvent]()
        
        for calendar in calendars {
            
            var tempEvents: [EKEvent]
//            let start = Date(timeIntervalSinceNow: -3600)
            let eventsPredicate = eventStore.predicateForEvents(withStart: Date(), end: Date().endOfDay, calendars: [calendar])
            
            tempEvents = eventStore.events(matching: eventsPredicate).sorted {
                (e1: EKEvent, e2: EKEvent) in
                
                return e1.startDate.compare(e2.startDate) == ComparisonResult.orderedAscending
            }
            .filter { return CLLocationCoordinate2DIsValid($0.structuredLocation?.geoLocation?.coordinate ?? kCLLocationCoordinate2DInvalid) }  // filter out invalid coordinates
            .filter {
                return isEventValid($0) // filter out event not within valid range
            }
               
            if !tempEvents.isEmpty {
                events.append(contentsOf: tempEvents)
            }
        }
        
        if(events.count > 0){
            // create an calendar address from the first event
            // From Apple document:
            // TODO: Please note that if you change the calendar of an event, this ID will likely change. It is currently also possible for the ID to change due to a sync operation. For example, if a user moved an event on a different client to another calendar, we'd see it as a completely new event here.
            //
            // Tested ok so far. Need to test more!!! May need to consider using event.calendarItemIdentifier?
            self.recommendedAddress = addressFrom(events[0])
            
            // Also need to monitor whether the event in navigation changed
            for event in events {
                if event.eventIdentifier == eventToBeMonitored {
                    if let address = addressFrom(event) {
                        self.newDestinationAddress = address
                    }
                    break
                }
            }
        } else {
            recommendedAddress = nil
        }
    }
    
    /**
        Check if the event is within valid range
        1. Current time > End time - invalid
        2. Current time <= End time
            - Current time >= start time - 3600 - valid
            - Current time < start time - 3600 -  invalid
        3. Current time within range but need to update after a while the current > end time
    */
    private func isEventValid(_ event: EKEvent) -> Bool {
        let start = event.startDate.timeIntervalSince1970
        let end = event.endDate.timeIntervalSince1970
        let current = Date().timeIntervalSince1970
        
        if current > end {
            return false // event ended already
        } else if current >= start - 3600 {
            return true
        }
        
        return false
    }
    
    private func addressFrom(_ event: EKEvent) -> CalendarAddress? {
        
        guard let location = event.location else { return nil }
        
        let addressArray = location.components(separatedBy: "\n")
        var address1 = ""
        var address2 = ""
        if addressArray.count > 1 {
            address1 = addressArray[0]
            address2 = addressArray[1]
        } else if addressArray.count > 0 {
            address1 = addressArray[0]
        }
        return CalendarAddress(
            eventId: event.eventIdentifier,
            alias: event.title,
            address1: address1,
            lat: event.structuredLocation?.geoLocation?.coordinate.latitude.toString() ?? "",
            long: event.structuredLocation?.geoLocation?.coordinate.longitude.toString() ?? "",
            address2: address2)
    }

}

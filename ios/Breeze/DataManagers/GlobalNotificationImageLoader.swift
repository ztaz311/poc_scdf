//
//  GlobalNotificationImageLoader.swift
//  Breeze
//
//  Created by Zhou Hao on 7/3/22.
//

import UIKit
import Kingfisher

final class ImageLoader {
    
    func retrieveImage(with urlString : String ) async -> UIImage? {
        guard let url = URL.init(string: urlString) else {
            return  nil
        }
        let resource = ImageResource(downloadURL: url)
        
        return await withCheckedContinuation { continuation in
            KingfisherManager.shared.retrieveImage(with: resource, options: nil, progressBlock: nil) { result in
                switch result {
                case .success(let value):
                    continuation.resume(returning: value.image)
                case .failure:
                    continuation.resume(returning: nil)
                }
            }
        }
    }

}

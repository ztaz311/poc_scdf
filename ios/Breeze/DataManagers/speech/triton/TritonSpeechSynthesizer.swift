//
//  TritonSpeechSynthesizer.swift
//  Breeze
//
//  Created by Anirudh Ramesh on 06/04/22.
//

import AVFoundation
import MapboxCoreNavigation
import MapboxDirections
import MapboxSpeech
import MapboxNavigation
import GRPC
import NIO
import NIOSSL
//import RxSwift
import SwiftyBeaver

struct AudioData {
    let instruction: SpokenInstruction
    let data: Data
}

@propertyWrapper public struct Synchronised<T> {
    private let lock = NSLock()

    private var _wrappedValue: T
    public var wrappedValue: T {
        get {
            lock.lock()
            defer {
                lock.unlock()
            }
            return _wrappedValue
        }
        set {
            lock.lock()
            defer {
                lock.unlock()
            }
            _wrappedValue = newValue
        }
    }

    public init(wrappedValue: T) {
        self._wrappedValue = wrappedValue
    }
}

/**
 `SpeechSynthesizing` implementation, using `MapboxSpeech` framework. Uses pre-caching mechanism for upcoming instructions.
 */
open class TritonSpeechSynthesizer: NSObject, SpeechSynthesizing {
    
    
    // MARK: Speech Configuration
    
    private static let TARGET = "breeze-voice-dev.coderuse.net"
    private static let PORT = 8001
    private static let TIME_OUT = 150000
    private static let MODEL_NAME = "nemo_model1"
    private static let INPUT_NAME = "input__0"
    private static let OUTPUT_NAME = "output__0"
    
    private let dispatchQueue = DispatchQueue(label: "breeze-speech-queue")
    private var audioQueue = Queue<AudioData>()
//    @Synchronised private var isPlaying = false
    private var isPlaying = false {
        didSet {
            if !isPlaying {
                self.checkAndPlayQueue()
            }
        }
    }
    
    // MARK: Speech Configuration
    
    public weak var delegate: SpeechSynthesizingDelegate?
    
    public var muted: Bool = false {
        didSet {
            updatePlayerVolume(audioFilePlayer)
        }
    }
    public var volume: Float = 1.0 {
        didSet {
            audioFilePlayer.volume = volume
        }
    }
    
    public var locale: Locale? = Locale.autoupdatingCurrent
    
    /// Number of upcoming `Instructions` to be pre-fetched.
    ///
    /// Higher number may exclude cases when required vocalization data is not yet loaded, but also will increase network consumption at the beginning of the route. Keep in mind that pre-fetched instuctions are not guaranteed to be vocalized at all due to re-routing or user actions. "0" will effectively disable pre-fetching.
    public var stepsAheadToCache: UInt = 3
    
    /**
     An `AVAudioPlayer` through which spoken instructions are played.
     */
    private var audioEngine: AVAudioEngine = AVAudioEngine()
    private var audioFilePlayer: AVAudioPlayerNode = AVAudioPlayerNode()
    
    /// Controls if this speech synthesizer is allowed to manage the shared `AVAudioSession`.
    /// Set this field to `false` if you want to manage the session yourself, for example if your app has background music.
    /// Default value is `true`.
    public var managesAudioSession = true
    
    /**
     Mapbox speech engine instance.
     
     The speech synthesizer uses this object to convert instruction text to audio.
     */
    //public private(set) var remoteSpeechSynthesizer: SpeechSynthesizer
    
    private var cache: BimodalDataCache
    private var audioTask: URLSessionDataTask?
    
    private var previousInstruction: SpokenInstruction?
    
    // MARK: Instructions vocalization
    
    public var isSpeaking: Bool {
//        return audioFilePlayer.isPlaying
        return isPlaying
    }
    
    public init(accessToken: String? = nil, host: String? = nil) {
        self.cache = DataCache()
    }
    
    func makeClient( group: EventLoopGroup) throws -> GRPCChannel {
      
      let certificatePath = Bundle.main.path(forResource: "triton", ofType: "pem")
      let certificates = try? NIOSSLCertificate.fromPEMFile(certificatePath!)
      let tlsConfiguration = GRPCTLSConfiguration.makeClientConfigurationBackedByNIOSSL(certificateVerification: .none)
      let channel = try GRPCChannelPool.with(
        target: .host(TritonSpeechSynthesizer.TARGET, port: TritonSpeechSynthesizer.PORT),
        transportSecurity: .tls(tlsConfiguration),
        eventLoopGroup: group
      )
      return channel
    }
    
    
    deinit {
        deinitAudioPlayer()
    }
    
    open func prepareIncomingSpokenInstructions(_ instructions: [SpokenInstruction], locale: Locale? = nil) {
        
        guard let locale = locale ?? self.locale else {
            self.delegate?.speechSynthesizer(self,
                                             encounteredError: SpeechError.undefinedSpeechLocale(instruction: instructions.first!))
            return
        }
        
        instructions
            .prefix(Int(stepsAheadToCache))
            .forEach {
                if !hasCachedSpokenInstructionForKey($0.ssmlText, with: locale) {
                    downloadAndCacheSpokenInstruction(instruction: $0, locale: locale)
                }
        }
    }
    
    open func speak(_ instruction: SpokenInstruction, during legProgress: RouteLegProgress, locale: Locale? = nil) {
        guard let locale = locale ?? self.locale else {
            self.delegate?.speechSynthesizer(self,
                                             encounteredError: SpeechError.undefinedSpeechLocale(instruction: instruction))
            return
        }
       
        
        guard let data = cachedDataForKey(instruction.ssmlText, with: locale) else {
            fetchAndSpeak(instruction: instruction, locale: locale)
            return
        }
        
        if let modifiedInstruction = delegate?.speechSynthesizer(self, willSpeak: instruction), modifiedInstruction != instruction {
            // Application changed the instruction, we need to refetch and cache it
            fetchAndSpeak(instruction: modifiedInstruction, locale: locale)
        } else {
            speak(instruction, data: data)
        }
    }
    
    open func stopSpeaking() {
        audioFilePlayer.stop()
    }
    
    open func interruptSpeaking() {
        audioFilePlayer.stop()
    }
    
    /**
     Vocalize the provided audio data.
     
     This method is a final part of a vocalization pipeline. It passes audio data to the audio player. `instruction` is used mainly for logging and reference purposes. It's text contents do not affect the vocalization while the actual audio is passed via `data`.
     - parameter instruction: corresponding instruction to be vocalized. Used for logging and reference. Modifying it's `text` or `ssmlText` does not affect vocalization.
     - parameter data: audio data, as provided by `remoteSpeechSynthesizer`, to be played.
     */
    open func speak(_ instruction: SpokenInstruction, data: Data) {
                                
        // audio is playing
//        if let previousInstruction = previousInstruction, audioFilePlayer.isPlaying{
//            delegate?.speechSynthesizer(self,
//                                        didInterrupt: previousInstruction,
//                                        with: instruction)
//        }
        
        guard !isPlaying else {
//            SwiftyBeaver.debug("Speech: \(instruction.text) added to queue")
            audioQueue.enqueue(element: AudioData(instruction: instruction, data: data))
            return
        }
                
        play(instruction, data: data)
    }
    
    private func play(_ instruction: SpokenInstruction, data: Data) {
        // no audio is playing
//        SwiftyBeaver.debug("Speech: start to play \(instruction.text)")
        isPlaying = true
        deinitAudioPlayer()
        
        switch safeInitializeAudioPlayer(data: data,
                                         instruction: instruction) {
        case .success(let player):
            audioFilePlayer = player
            previousInstruction = instruction
            //audioFilePlayer.play()
            
        case .failure(let error):
            
            safeUnduckAudio(instruction: instruction)
            delegate?.speechSynthesizer(self,
                                        didSpeak: instruction,
                                        with: error)
        }
    }
    
    // MARK: Private Methods
    
    /**
     Fetches and plays an instruction.
     */
    private func fetchAndSpeak(instruction: SpokenInstruction, locale: Locale) {
        audioTask?.cancel()
        
        let modifiedInstruction = delegate?.speechSynthesizer(self, willSpeak: instruction) ?? instruction
        let ssmlText = modifiedInstruction.ssmlText
        let options = SpeechOptions(ssml: ssmlText)
        options.locale = locale
        DispatchQueue.global(qos: .background).async { [weak self] in
            guard let self = self else { return }
            do {
                let data=try self.fetchAudioData(modifiedInstruction:modifiedInstruction)
                self.cache(data, forKey: ssmlText, with: locale)
                self.speak(modifiedInstruction,
                           data: data)
            }catch{
                print("Error info: \(error)")
                self.delegate?.speechSynthesizer(
                    self,
                    didSpeak: modifiedInstruction,
                    with: SpeechError.apiError(
                        instruction: modifiedInstruction,
                        options: options,
                        underlying: error
                    )
                )
            }
        }

    }
    
    
    private func createRequest(byteContent: [Data]) -> Inference_ModelInferRequest{
        var modelOutputs = Inference_ModelInferRequest.InferRequestedOutputTensor()
        modelOutputs.name=TritonSpeechSynthesizer.OUTPUT_NAME
        
        var modelContents=Inference_InferTensorContents()
        modelContents.bytesContents=byteContent
        var modelinput=Inference_ModelInferRequest.InferInputTensor()
        modelinput.name=TritonSpeechSynthesizer.INPUT_NAME
        modelinput.datatype="BYTES"
        modelinput.shape=[1,Int64(byteContent.count)]
        modelinput.contents=modelContents
        var modelInferRequest:Inference_ModelInferRequest=Inference_ModelInferRequest()
        modelInferRequest.modelName=TritonSpeechSynthesizer.MODEL_NAME
        modelInferRequest.inputs=[modelinput]
        modelInferRequest.outputs=[modelOutputs]
        return modelInferRequest
        
    }
    
    
    private func fetchAudioData(modifiedInstruction: SpokenInstruction) throws -> Data{
        let group = PlatformSupport.makeEventLoopGroup(loopCount: 1)
            defer {
              try? group.syncShutdownGracefully()
            }
        let inferenceChannel = try self.makeClient(group: group)
        defer {
              try? inferenceChannel.close().wait()
        }
        let inferenceClent = Inference_GRPCInferenceServiceClient(channel: inferenceChannel)
        let content=modifiedInstruction.text
        let byteContent=content.map{Data($0.utf8)}
        let modelInferRequest:Inference_ModelInferRequest=self.createRequest(byteContent:byteContent)
        let call=inferenceClent.modelInfer(modelInferRequest)
        let response = try call.response.wait()
        let outputContents=response.rawOutputContents[0]
        return outputContents
    }
    
    
    
    private func downloadAndCacheSpokenInstruction(instruction: SpokenInstruction, locale: Locale) {
        let ssmlText = instruction.ssmlText
        DispatchQueue.global(qos: .background).async { [weak self] in
            guard let self = self else { return }
            do {
                let data=try self.fetchAudioData(modifiedInstruction:instruction)
                self.cache(data, forKey: ssmlText, with: locale)
            }catch{
                return
            }
        }
        
    }
    
    func safeDuckAudio(instruction: SpokenInstruction?){
        guard managesAudioSession else { return }
        if let error = AVAudioSession.sharedInstance().tryDuckAudio() {
            delegate?.speechSynthesizer(self,
                                        encounteredError: SpeechError.unableToControlAudio(instruction: instruction,
                                                                                           action: .duck,
                                                                                           underlying: error))
        }
    }
    
    func safeUnduckAudio(instruction: SpokenInstruction?) {
        guard managesAudioSession else { return }
        if let error = AVAudioSession.sharedInstance().tryUnduckAudio() {
            delegate?.speechSynthesizer(self,
                                        encounteredError: SpeechError.unableToControlAudio(instruction: instruction,
                                                                                           action: .unduck,
                                                                                           underlying: error))
        }
    }
    
    private func cache(_ data: Data, forKey key: String, with locale: Locale) {
        cache.store(data, forKey: locale.identifier + key, toDisk: true, completion: nil)
    }
    private func cachedDataForKey(_ key: String, with locale: Locale) -> Data? {
        return cache.data(forKey: locale.identifier + key)
    }
    
    private func hasCachedSpokenInstructionForKey(_ key: String, with locale: Locale) -> Bool {
        return cachedDataForKey(key, with: locale) != nil
    }
    
    private func updatePlayerVolume(_ player: AVAudioPlayerNode?) {
        player?.volume = muted ? 0.0 : volume
    }
    
    private func safeInitializeAudioPlayer(data: Data, instruction: SpokenInstruction) -> Swift.Result<AVAudioPlayerNode, MapboxNavigation.SpeechError> {
        do {
            safeDuckAudio(instruction: instruction)
            try playPCM(data)
//            SwiftyBeaver.debug("Speech: \(instruction.text) playing")
            return .success(audioFilePlayer)
        } catch {
            SwiftyBeaver.error("Speech: Error info: \(error)")
            self.isPlaying = false
            return .failure(SpeechError.unableToInitializePlayer(playerType: AVAudioPlayer.self,
                                                                 instruction: instruction,
                                                                 synthesizer: nil,
                                                                 underlying: error))
        }
    }
    
    func playPCM(_ dataInput: Data) throws {
        do {
            let data=NSData(data:dataInput)
            let audioFormat = AVAudioFormat(commonFormat:AVAudioCommonFormat.pcmFormatInt16,sampleRate:22050,channels:1,interleaved:true)
            let audioFrameCount = UInt32(data.count)/audioFormat!.streamDescription.pointee.mBytesPerFrame
            let audioFileBuffer = AVAudioPCMBuffer(pcmFormat: audioFormat!, frameCapacity: audioFrameCount)!
            audioFileBuffer.frameLength = audioFileBuffer.frameCapacity;
            let channels = UnsafeBufferPointer(start: audioFileBuffer.int16ChannelData, count: Int(audioFileBuffer.format.channelCount))
            data.getBytes(UnsafeMutableRawPointer(channels[0]) , length: data.length)
            let mainMixer = audioEngine.mainMixerNode
            audioEngine.attach(audioFilePlayer)
            let outputFormat = audioEngine.mainMixerNode.outputFormat(forBus: 0)
            let converter = AVAudioConverter(from: audioFormat!, to: outputFormat)!
            audioEngine.connect(audioFilePlayer, to:mainMixer, format: outputFormat)
            let outputBuffer = AVAudioPCMBuffer(pcmFormat: outputFormat, frameCapacity: audioFrameCount*2)!
            converter.convert(to: outputBuffer, error: nil) { inNumPackets, outStatus in
                outStatus.pointee = .haveData
                return audioFileBuffer
            }
            try audioEngine.start()
            updatePlayerVolume(audioFilePlayer)
            guard audioEngine.isRunning, !audioFilePlayer.isPlaying else {
                SwiftyBeaver.error("Speech: audioEngine stopped or audioFilePlayer is playing")
                isPlaying = false
                return
            }
            audioFilePlayer.play()
            audioFilePlayer.scheduleBuffer(outputBuffer, at: nil, options: [], completionHandler: {

                self.safeUnduckAudio(instruction: self.previousInstruction)
                
                guard let instruction = self.previousInstruction else {
                    assert(false, "Speech Synthesizer finished speaking 'nil' instruction")
                    return
                }
                
                self.delegate?.speechSynthesizer(self,
                                            didSpeak: instruction,
                                            with: nil)

                // finished playing
//                SwiftyBeaver.debug("Speech: \(instruction.text) complete")
                self.isPlaying = false
            })
        } catch {
            isPlaying = false
            SwiftyBeaver.error("Speech error: \(error.localizedDescription)")
            throw error
        }
    }
    
    private func checkAndPlayQueue() {
        // play next audio if any
        if let audioData = self.audioQueue.dequeue() {
            self.dispatchQueue.asyncAfter(deadline: .now() + 0.2) {
//                SwiftyBeaver.debug("Speech: going to play \(audioData.instruction.text) in queue")
                self.play(audioData.instruction, data: audioData.data)
            }
        }
    }
    
    private func deinitAudioPlayer() {
        audioFilePlayer.stop()
        audioEngine.stop()
    }
}
/*
extension TritonSpeechSynthesizer: AVAudioPlayerDelegate {
    // Not used
    public func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        safeUnduckAudio(instruction: previousInstruction)
        
        guard let instruction = previousInstruction else {
            assert(false, "Speech Synthesizer finished speaking 'nil' instruction")
            return
        }
        
        delegate?.speechSynthesizer(self,
                                    didSpeak: instruction,
                                    with: nil)
    }
}
*/

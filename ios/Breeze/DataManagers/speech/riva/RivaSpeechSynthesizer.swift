//
//  RivaSppechSynthesizer.swift
//  Breeze
//
//  Created by Anirudh Ramesh on 07/06/22.
//

import AVFoundation
import MapboxCoreNavigation
import MapboxDirections
import MapboxSpeech
import MapboxNavigation
import GRPC
import NIO
import NIOSSL
//import RxSwift
import SwiftyBeaver

/**
 `SpeechSynthesizing` implementation, using `MapboxSpeech` framework. Uses pre-caching mechanism for upcoming instructions.
 */
open class RivaSpeechSynthesizer: NSObject, SpeechSynthesizing, AudioQueuable {

    // MARK: Speech Configuration Constants
//    private static let TARGET = "breeze-voice-riva-dev.coderuse.net"
    #if APPSTORE
        //  Prod
        private static let TARGET = "voice.breeze.com.sg"
    #else
        //  Others
        private static let TARGET = "voice-stage.breeze.com.sg"
    #endif

    private static let PORT = 50051
    private static let TIME_OUT = 150000
    private static let LANGUAGE_CODE = "en-US"
    private static let VOICE_NAME = "breeze"

    private var audioDispatchQueue: DispatchQueue {
        return appDelegate().audioDispatchQueue
    }
    private var audioQueue = Queue<AudioData>()
    
    public var hasAudioInQueue: Bool {
        return audioQueue.count() > 0
    }
    
    public var isPlaying = false {
        didSet {
            if !isPlaying && !isRerouting {
                self.checkAndPlayQueue()
            }
        }
    }
    private var isPlayBackInterrupted = false
    private var isRerouting = false
    
    // MARK: Speech Configuration
    
    public weak var delegate: SpeechSynthesizingDelegate?
    
    public var muted: Bool = false {
        didSet {
            updatePlayerVolume(audioFilePlayer)
        }
    }
    public var volume: Float = 1.0 {
        didSet {
            audioFilePlayer.volume = volume
        }
    }

    /// AudioQueuable
    open func clearAudioQueue() {

        audioQueue.clear()
    }

    public var locale: Locale? = Locale.autoupdatingCurrent

    /// Number of upcoming `Instructions` to be pre-fetched.
    ///
    /// Higher number may exclude cases when required vocalization data is not yet loaded, but also will increase network consumption at the beginning of the route. Keep in mind that pre-fetched instuctions are not guaranteed to be vocalized at all due to re-routing or user actions. "0" will effectively disable pre-fetching.
    public var stepsAheadToCache: UInt = 3

    /**
     An `AVAudioPlayer` through which spoken instructions are played.
     */
    private var audioEngine: AVAudioEngine = AVAudioEngine()
    private var audioFilePlayer: AVAudioPlayerNode = AVAudioPlayerNode()

    /// Controls if this speech synthesizer is allowed to manage the shared `AVAudioSession`.
    /// Set this field to `false` if you want to manage the session yourself, for example if your app has background music.
    /// Default value is `true`.
    public var managesAudioSession = true

    /**
     Mapbox speech engine instance.
     
     The speech synthesizer uses this object to convert instruction text to audio.
     */
    //public private(set) var remoteSpeechSynthesizer: SpeechSynthesizer

    private var cache: BimodalDataCache
    private var audioTask: URLSessionDataTask?

    private var previousInstruction: SpokenInstruction?
    private var currentInstruction: SpokenInstruction?

    // MARK: Instructions vocalization

    public var isSpeaking: Bool {
        return audioFilePlayer.isPlaying
    }
    
    func setRerouting(_ value: Bool) {
        if value {
            if !self.isRerouting {
                self.isRerouting = true
                self.clearAudioQueue()
            }
        } else {
            if self.isRerouting {
                self.isRerouting = false
//                self.isPlaying = false
            }
        }
    }
    
    func shouldUnduckOtherAudio() -> Bool {
        return (!isPlaying && !hasAudioInQueue)
    }

    public init(accessToken: String? = nil, host: String? = nil) {
        self.cache = DataCache()
        super.init()
        initializeAudioEngine()
        appDelegate().rivaSpeech = self
        addInterruptionsObserver()
    }

    func makeChannel(group: EventLoopGroup) throws -> GRPCChannel {

        //let certificatePath = Bundle.main.path(forResource: "triton", ofType: "pem")
        //let certificates = try! NIOSSLCertificate.fromPEMFile(certificatePath!)
        //let serverCertificate = certificates[0]
        //let tlsConfiguration = GRPCTLSConfiguration.makeClientConfigurationBackedByNIOSSL(certificateVerification: .none)
        let channel = try GRPCChannelPool.with(
            target: .host(RivaSpeechSynthesizer.TARGET, port: RivaSpeechSynthesizer.PORT),
            transportSecurity: .plaintext,
            eventLoopGroup: group
        )
        return channel
    }

    func initializeAudioEngine() {
        //let mainMixer = audioEngine.mainMixerNode
        muted = !Prefs.shared.getBoolValue(.enableAudio)
        updatePlayerVolume(audioFilePlayer)
        audioEngine.attach(audioFilePlayer)

//        let outputFormat = audioEngine.mainMixerNode.outputFormat(forBus: 0)
//        audioEngine.connect(audioFilePlayer, to:mainMixer, format: outputFormat)
    }

    deinit {
        deinitAudioPlayer()
        NotificationCenter.default.removeObserver(self)
    }

    private func addInterruptionsObserver() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(handleInterruption),
                                               name: AVAudioSession.interruptionNotification,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(routeChange),
                                               name: AVAudioSession.routeChangeNotification,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(resetMediaService),
                                               name: AVAudioSession.mediaServicesWereResetNotification,
                                               object: nil)
        
//        NotificationCenter.default.addObserver(self,
//                                               selector: #selector(reroutingEnded),
//                                               name: .reroutingEnded,
//                                               object: nil)
//        NotificationCenter.default.addObserver(self,
//                                               selector: #selector(willReroute),
//                                               name: .willReroute,
//                                               object: nil)
    }

    @objc private func handleInterruption(notification: Notification) {
        guard let userInfo = notification.userInfo,
            let typeValue = userInfo[AVAudioSessionInterruptionTypeKey] as? UInt,
            let type = AVAudioSession.InterruptionType(rawValue: typeValue) else {
            return
        }

        switch type {
        case .began:
            isPlayBackInterrupted = true
            SwiftyBeaver.error("handleInterruption : InterruptionBegan")
        case .ended:
            guard let optionsValue = userInfo[AVAudioSessionInterruptionOptionKey] as? UInt else {
                return
            }

            SwiftyBeaver.error("handleInterruption : InterruptionEnded")
            let options = AVAudioSession.InterruptionOptions(rawValue: optionsValue)
            if options.contains(.shouldResume) {
                isPlayBackInterrupted = false
                isPlaying = false
            }
        default:
            SwiftyBeaver.error("handleInterruption : InterruptionEnded")
            isPlayBackInterrupted = false
            isPlaying = false
            break
        }
    }
    
    @objc private func routeChange(notification: Notification) {
        guard let userInfo = notification.userInfo,
              let routeChangeReason = userInfo[AVAudioSessionRouteChangeReasonKey] as? AVAudioSession.RouteChangeReason else {
            return
        }
        
        print("routeChangeReason: \(routeChangeReason)")
                
        switch routeChangeReason {
        case AVAudioSession.RouteChangeReason.unknown:
            SwiftyBeaver.error("routeChangeReason : AVAudioSessionRouteChangeReasonUnknown")
        case AVAudioSession.RouteChangeReason.newDeviceAvailable:
            SwiftyBeaver.error("routeChangeReason : AVAudioSessionRouteChangeReasonNewDeviceAvailable")
        case AVAudioSession.RouteChangeReason.oldDeviceUnavailable:
            SwiftyBeaver.error("routeChangeReason : AVAudioSessionRouteChangeReasonOldDeviceUnavailable")
        case AVAudioSession.RouteChangeReason.categoryChange:
            SwiftyBeaver.error("routeChangeReason : AVAudioSessionRouteChangeReasonCategoryChange")
        case AVAudioSession.RouteChangeReason.override:
            SwiftyBeaver.error("routeChangeReason : AVAudioSessionRouteChangeReasonOverride")
        case AVAudioSession.RouteChangeReason.wakeFromSleep:
            SwiftyBeaver.error("routeChangeReason : AVAudioSessionRouteChangeReasonWakeFromSleep")
        case AVAudioSession.RouteChangeReason.noSuitableRouteForCategory:
            SwiftyBeaver.error("routeChangeReason : AVAudioSessionRouteChangeReasonNoSuitableRouteForCategory")
        default:
            break
        }
        
        isPlayBackInterrupted = false
        isPlaying = false
        
    }
    
    @objc private func resetMediaService(notification: Notification) {
        SwiftyBeaver.error("resetMediaService")
        self.isPlaying = false
    }
    
    /*
    @objc private func willReroute(notification: Notification) {
        SwiftyBeaver.error("willReroute")
        if !self.isRerouting {
            self.isRerouting = true
            self.isPlaying = false
        }
    }
    
    @objc private func reroutingEnded(notification: Notification) {
        SwiftyBeaver.error("reroutingEnded")
        if self.isRerouting {
            self.isRerouting = false
            self.isPlaying = false
        }
    }
     */

    open func prepareIncomingSpokenInstructions(_ instructions: [SpokenInstruction], locale: Locale? = nil) {

        guard let locale = locale ?? self.locale else {
            DispatchQueue.global().async {
                self.delegate?.speechSynthesizer(self,
                                                 encounteredError: SpeechError.undefinedSpeechLocale(instruction: instructions.first!))
            }
            return
        }

        instructions
            .prefix(Int(stepsAheadToCache))
            .forEach {
                let instruction = fixInstruction($0)
                if !hasCachedSpokenInstructionForKey(instruction.ssmlText, with: locale) {
                downloadAndCacheSpokenInstruction(instruction: instruction, locale: locale)
            }
        }
    }

    open func speak(_ instruction: SpokenInstruction, during legProgress: RouteLegProgress, locale: Locale? = nil) {
        guard let locale = locale ?? self.locale else {
            DispatchQueue.global().async {
                self.delegate?.speechSynthesizer(self,
                                                 encounteredError: SpeechError.undefinedSpeechLocale(instruction: instruction))
            }
            return
        }


        guard let data = cachedDataForKey(instruction.ssmlText, with: locale) else {
            fetchAndSpeak(instruction: instruction, locale: locale)
            return
        }

        if let modifiedInstruction = delegate?.speechSynthesizer(self, willSpeak: instruction), modifiedInstruction != instruction {
            // Application changed the instruction, we need to refetch and cache it
            fetchAndSpeak(instruction: modifiedInstruction, locale: locale)
        } else {
            speak(instruction, data: data)
        }
    }

    open func stopSpeaking() {
        audioFilePlayer.stop()
        isPlaying = false
    }

    open func interruptSpeaking() {
        audioFilePlayer.stop()
        isPlaying = false
    }

    /**
     Vocalize the provided audio data.
     
     This method is a final part of a vocalization pipeline. It passes audio data to the audio player. `instruction` is used mainly for logging and reference purposes. It's text contents do not affect the vocalization while the actual audio is passed via `data`.
     - parameter instruction: corresponding instruction to be vocalized. Used for logging and reference. Modifying it's `text` or `ssmlText` does not affect vocalization.
     - parameter data: audio data, as provided by `remoteSpeechSynthesizer`, to be played.
     */
    open func speak(_ instruction: SpokenInstruction, data: Data) {
        
        //  If audio is enable then play audio otherwise ignore it
        if Prefs.shared.getBoolValue(.enableAudio) {
            self.audioDispatchQueue.async(flags: .barrier, execute: { [weak self] in
                self?.play(instruction, data: data)
            })
        }
    }
    
    private func shouldAddQueue(_ audio: AudioData) -> Bool {
        if (currentInstruction != nil && previousInstruction != nil) {
            if currentInstruction?.text != audio.instruction.text && previousInstruction?.text != audio.instruction.text && !audioQueue.hasInstruction(audio) {
                return true
            }
        } else if currentInstruction != nil && previousInstruction == nil {
            if currentInstruction?.text != audio.instruction.text && !audioQueue.hasInstruction(audio) {
                return true
            }
        } else if previousInstruction != nil {
            if previousInstruction?.text != audio.instruction.text && !audioQueue.hasInstruction(audio) {
                return true
            }
        } else if !audioQueue.hasInstruction(audio) {
            return true
        }
        return false
    }

    private func play(_ instruction: SpokenInstruction, data: Data) {
        
        guard isRivaNotPlaying() else {
            //  Verify if next voice instruction is not same previous or not contains in queue then add new
            let audioData = AudioData(instruction: instruction, data: data)
            if shouldAddQueue(audioData) {
                audioQueue.enqueue(element: audioData)
            }
            return
        }
        
        isPlaying = true
        currentInstruction = instruction
        deinitAudioPlayer()

        switch safeInitializeAudioPlayer(data: data,
                                         instruction: instruction) {
        case .success(let player):
            audioFilePlayer = player
            previousInstruction = instruction

        case .failure(let error):

            safeUnduckAudio(instruction: instruction)
            SwiftyBeaver.debug("RIVA failed1 \(error.localizedDescription)- \(instruction.text)")
            DispatchQueue.global().async {
                self.delegate?.speechSynthesizer(self,
                                                 didSpeak: instruction,
                                                 with: error)
            }
        }
    }

    // MARK: Private Methods

    /**
     Fetches and plays an instruction.
     */
    private func fetchAndSpeak(instruction: SpokenInstruction, locale: Locale) {
        audioTask?.cancel()

        let modifiedInstruction = delegate?.speechSynthesizer(self, willSpeak: instruction) ?? instruction
        let fixedInstruction = fixInstruction(modifiedInstruction)
        let ssmlText = fixedInstruction.ssmlText

        let options = SpeechOptions(ssml: ssmlText)
        options.locale = locale
        DispatchQueue.global(qos: .background).async { [weak self] in
            guard let self = self else { return }
            do {
                let data = try self.fetchAudioData(fixInstruction: fixedInstruction)
                self.cache(data, forKey: ssmlText, with: locale)
                self.speak(fixedInstruction,
                           data: data)
            } catch {
                print("Error info: \(error)")
                SwiftyBeaver.debug("RIVA failed2 \(error.localizedDescription)- \(fixedInstruction.text)")
                DispatchQueue.global().async {
                    self.delegate?.speechSynthesizer(
                        self,
                        didSpeak: fixedInstruction,
                        with: SpeechError.apiError(
                            instruction: fixedInstruction,
                            options: options,
                            underlying: error
                        )
                    )
                }
            }
        }
    }

    private func createRequest(content: String) -> Nvidia_Riva_Tts_SynthesizeSpeechRequest {
        var req = Nvidia_Riva_Tts_SynthesizeSpeechRequest()
        req.text = content
        req.languageCode = RivaSpeechSynthesizer.LANGUAGE_CODE
        req.encoding = Nvidia_Riva_AudioEncoding.linearPcm
        req.sampleRateHz = 22050
        req.voiceName = RivaSpeechSynthesizer.VOICE_NAME
        return req

    }


    private func fetchAudioData(fixInstruction: SpokenInstruction) throws -> Data {
        let group = PlatformSupport.makeEventLoopGroup(loopCount: 1)
        defer {
            try? group.syncShutdownGracefully()
        }
        let inferenceChannel = try self.makeChannel(group: group)

        defer {
            try? inferenceChannel.close().wait()
        }

        let inferenceClent = Nvidia_Riva_Tts_RivaSpeechSynthesisClient(channel: inferenceChannel)
        let content = fixInstruction.text
        let modelInferRequest: Nvidia_Riva_Tts_SynthesizeSpeechRequest = self.createRequest(content: content)
        let call: UnaryCall<Nvidia_Riva_Tts_SynthesizeSpeechRequest, Nvidia_Riva_Tts_SynthesizeSpeechResponse> = inferenceClent.synthesize(modelInferRequest)
        let response: Nvidia_Riva_Tts_SynthesizeSpeechResponse = try call.response.wait()
        let outputContents = response.audio
        return outputContents
    }

    private func downloadAndCacheSpokenInstruction(instruction: SpokenInstruction, locale: Locale) {
        let ssmlText = instruction.ssmlText
        DispatchQueue.global(qos: .background).async { [weak self] in
            guard let self = self else { return }
            do {
                let data = try self.fetchAudioData(fixInstruction: instruction)
                self.cache(data, forKey: ssmlText, with: locale)
            } catch {
                return
            }
        }

    }
    
    func safeDuckAudio(instruction: SpokenInstruction?) {
        guard managesAudioSession else { return }
        
        AVAudioSession.sharedInstance().tryDuckOtherAudio {[weak self] error in
            guard let self = self else { return }
            if error != nil {
                self.delegate?.speechSynthesizer(self,
                                            encounteredError: SpeechError.unableToControlAudio(instruction: instruction,
                                                                                               action: .duck,
                                                                                               underlying: error))
            }
        }
    }

    func safeUnduckAudio(instruction: SpokenInstruction?) {
        guard managesAudioSession else { return }
        
        AVAudioSession.sharedInstance().tryUnduckOtherAudio { [weak self] error in
            guard let self = self else { return }
            if error != nil {
                self.delegate?.speechSynthesizer(self,
                                            encounteredError: SpeechError.unableToControlAudio(instruction: instruction,
                                                                                               action: .unduck,
                                                                                               underlying: error))
            }
        }
    }
    
    private func cache(_ data: Data, forKey key: String, with locale: Locale) {
        cache.store(data, forKey: locale.identifier + key, toDisk: true, completion: nil)
    }
    
    private func cachedDataForKey(_ key: String, with locale: Locale) -> Data? {
        return cache.data(forKey: locale.identifier + key)
    }
    
    private func hasCachedSpokenInstructionForKey(_ key: String, with locale: Locale) -> Bool {
        return cachedDataForKey(key, with: locale) != nil
    }
    
    private func updatePlayerVolume(_ player: AVAudioPlayerNode?) {
        player?.volume = muted ? 0.0 : volume
    }
    
    private func safeInitializeAudioPlayer(data: Data, instruction: SpokenInstruction) -> Swift.Result<AVAudioPlayerNode, MapboxNavigation.SpeechError> {
        do {
            safeDuckAudio(instruction: instruction)
            try playPCM(data)
            return .success(audioFilePlayer)
        } catch {
            SwiftyBeaver.error("Speech: Error info: \(error)")
            self.isPlaying = false
            return .failure(SpeechError.unableToInitializePlayer(playerType: AVAudioPlayer.self,
                                                                 instruction: instruction,
                                                                 synthesizer: nil,
                                                                 underlying: error))
        }
    }

    func playPCM(_ dataInput: Data) throws {
        do {
            
            //  Need handle while rerouting will not play audio , after finish rerouting then continue play voice instruction
            if isRerouting {
                SwiftyBeaver.error("Rerouting is in processing")
//                isPlaying = false
                return
            }
            
            let data = NSData(data: dataInput)
            guard let audioFormat = AVAudioFormat(commonFormat: AVAudioCommonFormat.pcmFormatInt16, sampleRate: 22050, channels: 1, interleaved: true) else {
                isPlaying = false
                let error = NSError(domain: "Format error", code: 1001)
                SwiftyBeaver.error("Speech error: \(error.localizedDescription)")
                throw error
            }

            let audioFrameCount = UInt32(data.count) / audioFormat.streamDescription.pointee.mBytesPerFrame

            guard let audioFileBuffer = AVAudioPCMBuffer(pcmFormat: audioFormat, frameCapacity: audioFrameCount) else {
                isPlaying = false
                let error = NSError(domain: "PCM buffer error", code: 1001)
                SwiftyBeaver.error("Speech error: \(error.localizedDescription)")
                throw error
            }

            audioFileBuffer.frameLength = audioFileBuffer.frameCapacity

            let channels = UnsafeBufferPointer(start: audioFileBuffer.int16ChannelData, count: Int(audioFileBuffer.format.channelCount))
            data.getBytes(UnsafeMutableRawPointer(channels[0]), length: data.length)

            let outputFormat = audioEngine.mainMixerNode.outputFormat(forBus: 0)
            let ratio = outputFormat.sampleRate / 22050.0

            guard let converter = AVAudioConverter(from: audioFormat, to: outputFormat) else {
                isPlaying = false
                let error = NSError(domain: "converter error", code: 1001)
                SwiftyBeaver.error("Speech error: \(error.localizedDescription)")
                throw error
            }
            audioEngine.connect(audioFilePlayer, to: audioEngine.mainMixerNode, format: outputFormat)

            guard let outputBuffer = AVAudioPCMBuffer(pcmFormat: outputFormat, frameCapacity: UInt32(Double(audioFrameCount) * ratio)) else {
                isPlaying = false
                let error = NSError(domain: "output buffer error", code: 1001)
                SwiftyBeaver.error("Speech error: \(error.localizedDescription)")
                throw error
            }

            converter.convert(to: outputBuffer, error: nil) { inNumPackets, outStatus in
                outStatus.pointee = .haveData
                return audioFileBuffer
            }

            audioEngine.prepare()
            try audioEngine.start()

            updatePlayerVolume(audioFilePlayer)
            
            guard !isPlayBackInterrupted, audioEngine.isRunning, !audioFilePlayer.isPlaying, AVAudioSession.sharedInstance().currentRoute.outputs.count > 0 else {
                SwiftyBeaver.error("Speech: audioEngine stopped or audioFilePlayer is playing")
                isPlaying = false
                return
            }
            
            self.audioFilePlayer.scheduleBuffer(outputBuffer, at: nil, options: [], completionHandler: {
                //  This completion handler is not working properly then isPlaying status when wrong
//                self.safeUnduckAudio(instruction: self.previousInstruction)
//
//                guard let instruction = self.previousInstruction else {
//                    assert(false, "Speech Synthesizer finished speaking 'nil' instruction")
//                    return
//                }
//
//                DispatchQueue.global().async {
//                    self.isPlaying = false
//                    self.delegate?.speechSynthesizer(self,
//                                                     didSpeak: instruction,
//                                                     with: nil)
//                }
            })
                        
            self.audioFilePlayer.play()
            
            //  Handle audio player completion by timer
            var delayInSeconds: Double = 0
            if let lastRenderTime = self.audioFilePlayer.lastRenderTime, let playerTime = self.audioFilePlayer.playerTime(forNodeTime: lastRenderTime) {
                delayInSeconds = Double(data.length - Int(playerTime.sampleTime)) / Double(outputFormat.sampleRate)
            }
            
            //  Make sure that delay time should be unsigned integer
            if delayInSeconds <= 0 {
                delayInSeconds = 0.01
            }
                        
            DispatchQueue.global().asyncAfter(deadline: .now() + delayInSeconds) {
                                
                if let instruction = self.previousInstruction {
//                    if self.currentInstruction?.text == instruction.text {
                        //  If there are no voice instruction in queue then should unduck audio
                        self.audioEngine.mainMixerNode.removeTap(onBus: 0)
                        
                        if !appDelegate().checkRivaHasAudioInQueue() {
                            self.safeUnduckAudio(instruction: instruction)
                        }
                        self.delegate?.speechSynthesizer(self,
                                                         didSpeak: instruction,
                                                         with: nil)
//                    }
                }
                self.isPlaying = false
            }

        } catch {
            isPlaying = false
            SwiftyBeaver.error("Speech error: \(error.localizedDescription)")
            throw error
        }
    }

    private func checkAndPlayQueue() {
        // play next audio if any
        self.audioDispatchQueue.asyncAfter(deadline: .now() + 0.2) { [weak self] in
            SwiftyBeaver.debug("checkAndPlayQueue")
            if let audioData = self?.audioQueue.dequeue() {
                SwiftyBeaver.debug("Speech: going to play \(audioData.instruction.text)")
                self?.speak(audioData.instruction, data: audioData.data)
            }
        }
        
    }

    private func deinitAudioPlayer() {
        audioFilePlayer.stop()
        audioEngine.stop()
    }
}

extension RivaSpeechSynthesizer {
    func fixInstruction(_ instruction: SpokenInstruction) -> SpokenInstruction {
        let text = replaceInText(instruction.text)
        var ssml = instruction.ssmlText
        if !instruction.ssmlText.contains("volume=") {
            /*
             https://www.w3.org/TR/speech-synthesis/#edef_prosody
             */
            ssml = instruction.ssmlText.replacingOccurrences(of: "<prosody", with: "<prosody volume=\"+6.0dB\"")
        }
        return SpokenInstruction(distanceAlongStep: instruction.distanceAlongStep, text: text, ssmlText: ssml)
    }
    
    private func replaceInText(_ text: String, appendingDot: Bool = true) -> String {
        let split = text.components(separatedBy: CharacterSet.whitespacesAndNewlines)
        let array = split.map { theText -> String in
            // remove the leading and trailing punctuationCharacters
            var leadingPunctuations = ""
            var trailingPunctuations = ""
            var text = ""
            var isLeading = true // check leading first
            // NOTED: There is an exception for this algorithm when there are multiple punctuations in one word. But this should not happen
            theText.forEach { (c) in
                if c.isPunctuation {
                    if isLeading {
                        leadingPunctuations.append(c)
                    } else {
                        trailingPunctuations.append(c)
                    }
                } else {
                    if isLeading {
                        isLeading = false
                    }
                    text.append(c)
                }
            }
            let founded = VoiceInstructor.abbrWordsList[text]
            return founded == nil ? theText : leadingPunctuations + founded! + trailingPunctuations
        }
        print("Array: \(array)")
        let processedText = array.joined(separator: " ").trimmingCharacters(in: .whitespaces)
        if (processedText.last == "."){
            return processedText
        }
        return appendingDot ? processedText + "." : processedText
    }
}

extension RivaSpeechSynthesizer {
    private func isRivaNotPlaying() -> Bool {
        return (!isPlaying && !isRerouting)
    }
}


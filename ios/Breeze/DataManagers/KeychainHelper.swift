//
//  KeychainHelper.swift
//  KeychainHelper
//
//  Created by Santoso Pham on 14/03/2023.
//

import Foundation
import Security
import UIKit
import KeychainSwift

enum KeychainKey: String {
    //  Store userID for guest mode only
    case userId = "breeze.ncs.userId"
    case deviceToken = "breeze.ncs.deviceToken"
    
    //  Login with phone number
    case phoneNumber = "breeze.ncs.phoneNumber"
    
    // Other three values username, triplog email, car IU number will be stored locally and along with user sub identifier
    case username = "breeze.ncs.username"
    case triplogEmail = "breeze.ncs.triplogEmail"
    case carplateNumber = "breeze.ncs.carplateNumber"
    case iuNumber = "breeze.ncs.iuNumber"
}

class KeyChain {
    
    class func getPrefix() -> String {
        
    #if BETARELEASE
    //QA
    return "qa"
    #else
        #if APPSTORE
        //Prod
        return "prod"
        #else
        //Dev
        return "dev"
        #endif
    #endif    
    }
    
    //  Get/Set value in keychain get along with user sub ID
    class func setValueWithSub(_ subID: String, key: KeychainKey, value: String){
        let generatedKey = "\(KeyChain.getPrefix())_\(subID)_\(key.rawValue)"
        let keychain = KeychainSwift()
        keychain.set(value, forKey: generatedKey)
    }
    
    class func getValueWithSub(_ subID: String, key: KeychainKey) -> String {
        let generatedKey = "\(KeyChain.getPrefix())_\(subID)_\(key.rawValue)"
        let keychain = KeychainSwift()
        return keychain.get(generatedKey) ?? ""
    }
    
    class func deleteValueWithSub(_ subID: String, key: KeychainKey) {
        let keychain = KeychainSwift()
        let generatedKey = "\(KeyChain.getPrefix())_\(subID)_\(key.rawValue)"
        keychain.delete(generatedKey)
    }
    
    //  This is for clean all datas: username, triplog email, car IU number in local storage
    class func cleanLocalDataWithSub(_ subID: String) {
        KeyChain.deleteValueWithSub(subID, key: .username)
        KeyChain.deleteValueWithSub(subID, key: .triplogEmail)
        KeyChain.deleteValueWithSub(subID, key: .carplateNumber)
        KeyChain.deleteValueWithSub(subID, key: .iuNumber)
    }
    
    //  Clean all guest user datas
    class func cleanAllGuestUser(_ subID: String) {
        KeyChain.delete(key: .userId)
        KeyChain.delete(key: .deviceToken)
        KeyChain.cleanLocalDataWithSub(subID)
    }
    
    //  Get/Set value in keychain without user sub ID
    class func save(key: KeychainKey, value: String) {
        let keychain = KeychainSwift()
        let generatedKey = "\(KeyChain.getPrefix())_\(key.rawValue)"
        keychain.set(value, forKey: generatedKey)
    }

    class func load(key: KeychainKey) -> String {
        let keychain = KeychainSwift()
        let generatedKey = "\(KeyChain.getPrefix())_\(key.rawValue)"
        return keychain.get(generatedKey) ?? ""
    }
    
    class func delete(key: KeychainKey) {
        let keychain = KeychainSwift()
        let generatedKey = "\(KeyChain.getPrefix())_\(key.rawValue)"
        keychain.delete(generatedKey)
    }
    
    //  This function will remove all data in keychain, be careful when we use it
    class func cleanAllDatas() {
        let keychain = KeychainSwift()
        keychain.clear()
    }
}

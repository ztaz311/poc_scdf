//
//  LocationManager.swift
//  Breeze
//
//  Created by VishnuKanth on 22/12/20.
//

import Foundation
import UIKit
import CoreLocation
import SwiftyBeaver

protocol LocationManagerDelegate: AnyObject {
    func getCruiseSpeedLimitSucces()
    func stoppedForADuration()  // used to stop cruise mode
    func didUpdate(_ location: CLLocation)
}

final class LocationManager: NSObject, CLLocationManagerDelegate {

    static let shared = LocationManager()
    weak var delegate: LocationManagerDelegate?
    var location = CLLocation()
    var switchSpeed = "KPH"
    var arrayMPH: [Double]! = []
    var arrayKPH: [Double]! = []
    var traveledDistance:Double = 0
    private var locationManager: CLLocationManager = CLLocationManager()
    var requestLocationAuthorizationCallback: ((CLAuthorizationStatus) -> Void)?
    
    var cruiseModeSpeedCallback: ((_ value:Bool) -> Void)?

    private var lastOverCruiseSpeed: TimeInterval = 0
    private var lastBelowCruiseSpeed: TimeInterval = Date().timeIntervalSince1970  // time stamp when speed below cruise speed -> by default launching app should init value as current time
        
    private (set) var currentSpeedToKPH = 0.0
    #if STAGING || TESTING
    private var simulatedSpeed = -1.0
    #endif
    
    // MARK: - for speeding check
    // The last time stamp before over speed
    private var lastBeforeOverSpeedTime: TimeInterval = 0
    private var isOverSpeedFor1minute = false
    private var isOverSpeedFor10minutes = false
        
    public func requestLocationAuthorization() {
        self.locationManager.delegate = self
        let currentStatus = CLLocationManager.authorizationStatus()

        // Only ask authorization if it was never asked before
        guard currentStatus == .notDetermined else { return }

        AnalyticsManager.shared.logOpenPopupEvent(popupName: ParameterName.Onboarding.UserPopup.location_permission, screenName: ParameterName.Onboarding.screen_view)
        
        // Starting on iOS 13.4.0, to get .authorizedAlways permission, you need to
        // first ask for WhenInUse permission, then ask for Always permission to
        // get to a second system alert
        if #available(iOS 13.4, *) {
            self.requestLocationAuthorizationCallback = { status in
                if status == .authorizedWhenInUse {
                    self.locationManager.requestAlwaysAuthorization()
                }
            }
            self.locationManager.requestWhenInUseAuthorization()
        } else {
            self.locationManager.requestAlwaysAuthorization()
        }
    }
    
    // MARK: Over speeding related functions
    /// Reset - must be called once before checkSpeeding
    public func startSpeedCheck() {
        isOverSpeedFor1minute = false
        isOverSpeedFor10minutes = false
        lastBeforeOverSpeedTime = Date().timeIntervalSince1970
    }
    
    public func getCurrentSpeed() -> Double {
        return currentSpeedToKPH
    }
    
    /// Check speeding
    /// Will be called in cruise mode or navigation to check against `currentSpeedToKPH` and return combination of:
    /// 1. Speed over speedLimit
    /// 2. Speed over speedLimit over 1 min
    /// 3. Speed over speedLimit over 10 minutes
    public func checkSpeeding(speedLimit: Double) -> (overSpeed: Bool, overFor1minute: Bool, overFor10minutes: Bool) {
        
        guard speedLimit > 0 else {
            isOverSpeedFor1minute = false
            isOverSpeedFor10minutes = false
            lastBeforeOverSpeedTime = Date().timeIntervalSince1970
            return (false, false, false)
        }
        
        var theSpeedLimit = speedLimit

        // For testing purpose, we reduce the speedLimt by 10 in staging or beta release
        #if STAGING
        theSpeedLimit = speedLimit - 10
        #endif
        
        if currentSpeedToKPH > theSpeedLimit {
            let now = Date().timeIntervalSince1970
            if (now - lastBeforeOverSpeedTime > Values.overspeedFirstAlertDuration) && !isOverSpeedFor1minute {
                isOverSpeedFor1minute = true
                SwiftyBeaver.debug("Over speed more than 1 min, current speed = \(currentSpeedToKPH), speed limit = \(speedLimit)")
                return (true, isOverSpeedFor1minute, false)
            }
            if (now - lastBeforeOverSpeedTime > Values.overspeedSecondAlertDuration) && !isOverSpeedFor10minutes {
                isOverSpeedFor10minutes  = true
                SwiftyBeaver.debug("Over speed more than 10 mins, current speed = \(currentSpeedToKPH), speed limit = \(speedLimit)")
                return (true, false, isOverSpeedFor10minutes)
            }
            return (true, false, false)
        } else {
            isOverSpeedFor1minute = false
            isOverSpeedFor10minutes = false
            lastBeforeOverSpeedTime = Date().timeIntervalSince1970
            return (false, false, false)
        }
    }
    
    public func getStatus() -> CLAuthorizationStatus{
        
        let currentStatus = CLLocationManager.authorizationStatus()
        return currentStatus
        
    }

    public func startSimulateCarMoving(after seconds: TimeInterval) {
        #if DEBUG
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
            Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { [weak self] timer in
                guard let self = self else { return }
                self.overCruiseSpeed(at: Date().timeIntervalSince1970)
            }            
        }
        #endif
    }
    
#if STAGING || TESTING
    public func startSimulateOverspeed(after seconds: TimeInterval, speed: Double) {
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds) { [weak self] in
            guard let self = self else { return }
            self.simulatedSpeed = speed
        }
    }
#endif
    
    // MARK: - CLLocationManagerDelegate
    public func locationManager(_ manager: CLLocationManager,
                                didChangeAuthorization status: CLAuthorizationStatus) {
        self.requestLocationAuthorizationCallback?(status)
    }
    
    public func getLocation(){
        
        self.locationManager.activityType = .otherNavigation
        self.locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
//        self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        //self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.startUpdatingLocation()
    }
    
    public func getCruiseModeSpeed(){
        
        self.locationManager.startUpdatingLocation()
        lastOverCruiseSpeed = 0
    }
    
    public func endCruiseSpeedUpdate(){
        
        arrayMPH = []
        arrayKPH = []
        self.locationManager.stopUpdatingLocation()
        
    }
        
    private func overCruiseSpeed(at time: TimeInterval) {
        lastOverCruiseSpeed = time
        // only start cruise when speed is over cruise speed more than certain duration
        if lastBelowCruiseSpeed > 0 && time - lastBelowCruiseSpeed > Values.cruiseOverSpeedCheckThreshold {
            self.delegate?.getCruiseSpeedLimitSucces()
        }
    }
    
    private func stoppedOverTime() {
        delegate?.stoppedForADuration()
        // reset
        lastOverCruiseSpeed = 0
    }
        
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        // if location not updated for certain time period, reset the currentSpeedToKPH and lastBelowCruiseSpeed
        guard Date().timeIntervalSince1970 - self.location.timestamp.timeIntervalSince1970 < Values.locationNotUpdateThreshold else {
            currentSpeedToKPH = 0
            lastBelowCruiseSpeed = Date().timeIntervalSince1970
            self.location = locations.last! // still update
            return
        }
        
        location = locations.last! // last location update
        if (location.horizontalAccuracy > 0) {
            updateLocationInfo(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude, speed: location.speed, direction: location.course)
        }
        location = locations.last!
        delegate?.didUpdate(location)
    }
    
    public func startMonitoringGeofence(location:CLLocationCoordinate2D){
        
        let geofenceRegionCenter = location
                
                /* Create a region centered on desired location,
                 choose a radius for the region (in meters)
                 choose a unique identifier for that region */
                let geofenceRegion = CLCircularRegion(
                    center: geofenceRegionCenter,
                    radius: 3000,
                    identifier: "Flash Flood"
                )
                
                
                geofenceRegion.notifyOnEntry = true
                geofenceRegion.notifyOnExit = true
                
                self.locationManager.startMonitoring(for: geofenceRegion)
    }
    
    func updateLocationInfo(latitude: CLLocationDegrees, longitude: CLLocationDegrees, speed: CLLocationSpeed, direction: CLLocationDirection) {
            let speedToMPH = (speed * 2.23694)
            let speedToKPH = (speed * 3.6)
           
        currentSpeedToKPH = speedToKPH
        
        print("Current speed: \(currentSpeedToKPH)")
        
        if lastOverCruiseSpeed > 0 { // start check only if this is not 0. This will triggered by startCruiseMode
            if speedToKPH > 1 {
                lastOverCruiseSpeed = location.timestamp.timeIntervalSince1970
            } else {
                if location.timestamp.timeIntervalSince1970 - lastOverCruiseSpeed > Values.durationToStopCruiseMode { // TODO: to decide how long?
                    stoppedOverTime()
                }
            }
        }
            
        if switchSpeed == "MPH" {
            // Chekcing if speed is less than zero or a negitave number to display a zero
            if (speedToMPH > 0) {
                let speed = (String(format: "%.0f mph", speedToMPH))
                arrayMPH.append(speedToMPH)
                print(speed)

            } else {
                //speedDisplay.text = "0 mph"
            }
        }

        if switchSpeed == "KPH" {
            // Checking if speed is less than zero
            if (speedToKPH > Values.cruiseStartSpeed) {
                //let speedDisplay = (String(format: "%.0f km/h", speedToKPH))
                arrayKPH.append(speedToKPH)
                overCruiseSpeed(at: location.timestamp.timeIntervalSince1970)
                avgSpeed()
            } else {
                lastBelowCruiseSpeed = Date().timeIntervalSince1970
            }
                            
            #if STAGING || TESTING
            if simulatedSpeed >= 0 {
                currentSpeedToKPH = simulatedSpeed
            }
            #endif
        }
            
    }
    
    func avgSpeed(){
            if switchSpeed == "MPH" {
                let speed:[Double] = arrayMPH
                let speedAvg = speed.reduce(0, +) / Double(speed.count)
                let avgSpeed = (String(format: "%.0f", speedAvg))
                print( avgSpeed )
            } else if switchSpeed == "KPH" {
                let speed:[Double] = arrayKPH
                let speedAvg = speed.reduce(0, +) / Double(speed.count)
                let avgSpeed = (String(format: "%.0f", speedAvg))
                print( avgSpeed)
            }
        }
    
    public func getDistance(lat:Double,long:Double) -> String{
        
        let toLocation = CLLocation(latitude: lat, longitude: long)
        let distance = location.distance(from: toLocation) / 1000
        
        let distanceStr = String(format: "%.01f km", distance)
        //Display the result in km
        //print(String(format: "The distance to my buddy is %.01f KM", distance))
        
        return distanceStr
        
        //return distanceStr
    }
    
    public func getDistanceInMeters(fromLoc:CLLocation,toLoc:CLLocation) -> CLLocationDistance
    {
        let distance = fromLoc.distance(from: toLoc)
        
        return distance
    }

    private func  locationManager(manager: CLLocationManager, didFailWithError error: NSError)
        {
            print ("Errors:" + error.localizedDescription)
        }
}

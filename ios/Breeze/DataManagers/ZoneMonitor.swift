//
//  ZoneMonitor.swift
//  Breeze
//
//  Created by Zhou Hao on 20/5/22.
//

import Foundation
import Foundation
import Turf
import CoreLocation
import Combine
import SwiftyBeaver
import CloudKit
import MapboxMaps
import MapboxNavigation
import MapboxCoreNavigation
import MapboxDirections
import AVFAudio
import SwiftyBeaver

final class ZoneMonitor {
    
    // MARK: - Private Properties
    private var disposables = Set<AnyCancellable>()
    @Published private var reachInnerZone: Bool = false
    @Published private var reachOutterZone: Bool = false
    private var shouldAlert = true

    // MARK: - Public Properties
    var innerZoneAction: ((Bool) -> Void)?
    var outterZoneAction: ((Bool) -> Void)?
    
    // start from cruise
    init(origin: CLLocationCoordinate2D) {
        for zone in SelectedProfiles.shared.getAllZones(){
            let zoneCoordinate = CLLocationCoordinate2D(latitude: zone.centerPointLat.toDouble() ?? 0, longitude: zone.centerPointLong.toDouble() ?? 0)
            
            let distance = origin.distance(to: zoneCoordinate)
            
            if distance < Double(zone.radius) {
                self.shouldAlert = false
            }
        }
    }
    
    // These 3 should be not nil together
    init(origin: CLLocationCoordinate2D? = nil,destination: CLLocationCoordinate2D? = nil) {
        
        for zone in SelectedProfiles.shared.getAllZones(){
            
            if let origin = origin, let destination = destination {
                
                let zoneCoordinate = CLLocationCoordinate2D(latitude: zone.centerPointLat.toDouble() ?? 0, longitude: zone.centerPointLong.toDouble() ?? 0)
                
                let distance = origin.distance(to: zoneCoordinate)
                
                // If user starts navigation in zone --> no alert (They are there. They can see if the place is congested)
                if distance < Double(zone.radius) {                    
                    // user start inside the zone, should never alert
                    self.shouldAlert = false
                } else {
                    // user start outside the zone, should only alert if the destination is inside the zone
                    self.shouldAlert = destination.distance(to: zoneCoordinate) < Double(zone.radius)
                }
            }
            
        }
        /*if let origin = origin, let destination = destination, let innerCoordinate = SelectedProfiles.shared.getInnerZoneCentrePoint(), let lineString = routeLine {
            let distance = origin.distance(to: innerCoordinate)
            let innerR = Double(SelectedProfiles.shared.getInnerZoneRadius())
            let outerR = Double(SelectedProfiles.shared.getOuterZoneRadius())
            // If user starts navigation in inner zone --> no alert (They are there. They can see if the place is congested)
            if distance < innerR {
                // user start inside inner zone
                self.shouldAlert = false
            } else if distance < outerR { // user start in outer zone
//                If user starts navigation in the outer zone with destination in the inner zone --> alert
//                If user starts navigation in the outer zone with destination outside of TB --> no alert
                let destDistance = destination.distance(to: innerCoordinate)
                if destDistance > innerR {
                    // one more condition (desitination also in outerzone and the route passthrough the inner zone)
                    if destDistance < outerR && isPassthroughInnerZone(center: innerCoordinate, radius: innerR, line: lineString) {
                        self.shouldAlert = true
                    } else {
                        // destination in outer zone and not passthrough the inner zone, no alert
                        self.shouldAlert = false
                    }
                }
            }            
        }*/
    }
    
    deinit {
        disposables.removeAll()
    }
    
    // check if the route passthrough the inner zone
    private func isPassthroughInnerZone(center: CLLocationCoordinate2D, radius: Double, line: LineString) -> Bool {
        // convet inner zone circel to a polygon
        let turfPolygon = Turf.Polygon(center: center, radius: radius, vertices: 50)
        // convet polygon to lineString
        let polygonLine = LineString(turfPolygon.coordinates[0])
        // check if intersect
        return !line.intersections(with: polygonLine).isEmpty
    }
    
    /// Monitor the selected profile zone
    func start() {
        // No need to check if the selected profile is selected or not
//        if SelectedProfiles.shared.isProfileSelected() {
        
        // Commented the below code for supporting both inner and outer zones. If backend returns multiple zones then we need to think about the logic to handle the zones dynamically instead of differentiating as inner/outer
            /*$reachInnerZone
                .receive(on: DispatchQueue.main)
                .sink(receiveValue: { [weak self] reached in
                    guard let self = self else { return }
                    if self.shouldAlert {
                        self.innerZoneAction?(reached)
                    }
                    
            }).store(in: &disposables)*/
            
            $reachOutterZone
                .receive(on: DispatchQueue.main)
                .sink(receiveValue: { [weak self] reached in
                    guard let self = self else { return }
                    if self.shouldAlert {
                        self.outterZoneAction?(reached)
                    }
                    
            }).store(in: &disposables)
//        }
    }
    
    /// check if the current location is inside the inner or outer zone
    func update(currentLocation: LocationCoordinate2D) {
        
        for zone in SelectedProfiles.shared.getAllZones(){
            
            let zoneCoordinate = CLLocationCoordinate2D(latitude: zone.centerPointLat.toDouble() ?? 0, longitude: zone.centerPointLong.toDouble() ?? 0)
            
            let distance = currentLocation.distance(to: zoneCoordinate)
            
            if distance <= Double(zone.radius) {
            
                if reachOutterZone == false {
                    SwiftyBeaver.debug("ZoneMonitor: reach outer zone - \(distance),\(zone.radius)")
                    reachOutterZone = true
                }
            }
        }
        
        // Commented the below code for supporting both inner and outer zones. If backend returns multiple zones then we need to think about the logic to handle the zones dynamically instead of differentiating as inner/outer
        // inner coordinate is the same as outer ring coordinate
       /* guard let innerCoordinate = SelectedProfiles.shared.getInnerZoneCentrePoint() else { return }
        
//        if SelectedProfiles.shared.isProfileSelected() {
            let distance = currentLocation.distance(to: innerCoordinate)
            let outerR = Double(SelectedProfiles.shared.getOuterZoneRadius())
            let innerR = Double(SelectedProfiles.shared.getInnerZoneRadius())
                                    
            // only update when value changed
            if distance <= innerR {
                if reachInnerZone == false {
                    SwiftyBeaver.debug("ZoneMonitor: reach inner zone - \(distance),\(outerR),\(innerR)")
                    reachInnerZone = true
                }
            } else if distance <= outerR {
//                if reachInnerZone {
//                    reachInnerZone = false
//                }
                if reachOutterZone == false {
                    SwiftyBeaver.debug("ZoneMonitor: reach outer zone - \(distance),\(outerR),\(innerR)")
                    reachOutterZone = true
                }
            }*/ /* else {
                if reachInnerZone {
                    reachInnerZone = false
                }
                if reachOutterZone {
                    reachOutterZone = false
                }
            } */ // only trigger for once
//        }
    }
    
    func handleZoneAlert(type: String, zoneId: String, carparkIsdestination: Bool = false, carparkId: String = "", targetZoneId: String = "", completion: @escaping (ZoneAlert?) -> Void) {
        guard let profileName = AmenitiesSharedInstance.shared.getDefaultProfileName() else { return }
        
        DispatchQueue.global(qos: .background).async {
            ZoneService().getZoneAlertDetail(profileName: profileName, zoneId: zoneId, navigationType: type, carparkIsdestination: carparkIsdestination, carparkId: carparkId, targetZoneId: targetZoneId) { result in
                switch result {
                case .success(let alert):
                    completion(alert)
                    break
                case .failure(let error):
//                    let alert = ZoneAlert(alertMessage: "Congestion Alert! Limited parking at this time. Parking available at HDB carparks nearby", isCongestion: false)
                    completion(nil)
                    SwiftyBeaver.error("Failed to getZoneAlertDetail: \(error.localizedDescription)")
                }
            }
        }
    }
}

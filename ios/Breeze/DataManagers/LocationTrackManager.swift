//
//  LocationTrackManager.swift
//  Breeze
//
//  For PassiveLocationManager
//
//  Created by Zhou Hao on 2/3/21.
//

import Foundation
import MapboxCoreNavigation
import MapboxNavigation
import CoreLocation
import Turf
import SwiftyBeaver

protocol LocationTrackingManagerDelegate: AnyObject {
    func didUpdate(roadName: String, speedLimit:Double,location: CLLocation?, rawLocation: CLLocation?)
//    func isTunnel(tunnel:Bool)
}

class LocationTrackingManager {
    
    // Beta.15
    // - change PassiveLocationManager to PassiveLocationProvider
    // - change PassiveLocationDataSource to PassiveLocationManager
    
    weak var delegate: LocationTrackingManagerDelegate?
    private (set) var passiveLocationProvider: PassiveLocationProvider!
    private (set) var roadGraph: RoadGraph!
    
    private var currentEdgeIdentifier: RoadGraph.Edge.Identifier?
    private var nextEdgeIdentifier: RoadGraph.Edge.Identifier?
    private (set) var passiveLocationDataSource = PassiveLocationManager()
    private var lastUpdatedTime: TimeInterval = 0
    
    init() {
        passiveLocationProvider = PassiveLocationProvider(locationManager: passiveLocationDataSource)
        roadGraph = passiveLocationDataSource.roadGraph
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(didUpdatePassiveLocation),
                                               name: .passiveLocationManagerDidUpdate,
                                               object: nil)                
    }
    
    func startUpdating() {
        passiveLocationProvider.startUpdatingHeading()
        passiveLocationProvider.startUpdatingLocation()        
    }
    
    func stopUpdating() {
        passiveLocationProvider.stopUpdatingHeading()
        passiveLocationProvider.stopUpdatingLocation()
    }
    
    func startUpdatingEHorizon(){
        let options = ElectronicHorizonOptions(length: 1500, expansionLevel: 0, branchLength: 50, minTimeDeltaBetweenUpdates: nil)
        PassiveLocationManager().startUpdatingElectronicHorizon(with: options)
    }
    
    func stopUpdatingEHorizon(){
        
        PassiveLocationManager().stopUpdatingElectronicHorizon()
    }
    func cruiseFeedback(type:PassiveNavigationFeedbackType,feedBack:FeedbackEvent){
        
        let eventsManager = NavigationEventsManager(passiveNavigationDataSource: passiveLocationDataSource)
        //eventsManager.sendFeedback(feedBack, type: type)
        eventsManager.sendPassiveNavigationFeedback(feedBack, type: type, description: "CruiseFeedback")
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: .passiveLocationManagerDidUpdate, object: nil)
    }
    
    @objc func didUpdatePassiveLocation(_ notification: Notification) {
        
        let now = Date().timeIntervalSince1970
        guard now - lastUpdatedTime > 0.5 else { return }
        
        lastUpdatedTime = now
        
        var road = ""
        var newLocation: CLLocation?
        var newRawLocation: CLLocation?
        var speedLimit =  0.0
        
        
        if let roadName = notification.userInfo?[PassiveLocationManager.NotificationUserInfoKey.roadNameKey] as? String {
            road = roadName
        }
        
        if let location = notification.userInfo?[PassiveLocationManager.NotificationUserInfoKey.locationKey] as? CLLocation {
            // Suggested by Mapbox to use location instead of rawLocation
            newLocation = location
        }
                
        if let rawLocation =  notification.userInfo?[PassiveLocationManager.NotificationUserInfoKey.rawLocationKey] as? CLLocation {
            newRawLocation = rawLocation
        }
        
        if let tempSpeedLimit = notification.userInfo?[PassiveLocationManager.NotificationUserInfoKey.speedLimitKey] as? Measurement<UnitSpeed> {
            
            //print(tempSpeedLimit.value)
            speedLimit = tempSpeedLimit.value
            
        }
                
        SwiftyBeaver.debug("PassiveLocation didUpdate: Speed = \(speedLimit) at \(newLocation) - \(newRawLocation)")
        delegate?.didUpdate(roadName: road,speedLimit:speedLimit, location: newLocation, rawLocation: newRawLocation)
    }

}

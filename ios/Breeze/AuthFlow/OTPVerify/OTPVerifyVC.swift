//
//  OTPVerifyVC.swift
//  Breeze
//
//  Created by VishnuKanth on 31/07/21.
//

import UIKit

protocol DismissDelegate: AnyObject {

   
    func dismissViewController(controller: UIViewController)
    
}

class OTPVerifyVC: BaseViewController,
                   OTPStackViewDelegate {
    
    weak var delegate: DismissDelegate?
    @IBOutlet weak var otpStackView: OTPStackView!
    @IBOutlet var closeBtn: UIButton!
    @IBOutlet var detailsLbl: UILabel!
    @IBOutlet var errorLbl: UILabel!
    @IBOutlet var resendBtn:UIButton!
    @IBOutlet var timerLbl:UILabel!
    var numberOfAttempts = 3
    
    var isSignUp = false //  No longer support sign up with phone number so default should be false
    var seconds = 0
    var totalTime = 180
    var mobileNumber: String!
    var timer: Timer?
    var previousOTPCode = ""
    
    
    private var previouslyAccumulatedTime: TimeInterval = 0
    private var startDate: Date? = Date()
    private var lastStopDate: Date? = Date()
    @Published var totalAccumulatedTime: TimeInterval = 0
    
    var isPulldownToDismiss: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.otpStackView.delegate = self
        let firstOTP = self.otpStackView.textFieldArray[0]
        firstOTP.becomeFirstResponder()
        setupDarkLightAppearance(forceLightMode: true)
        setUpUI()
        // Do any additional setup after loading the view.
        
        let notificationCenter = NotificationCenter.default
            notificationCenter.addObserver(self, selector: #selector(appMovedToBackground), name: UIApplication.willResignActiveNotification, object: nil)
        
        let notificationCenter2 = NotificationCenter.default
            notificationCenter2.addObserver(self, selector: #selector(appMovedToForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
    }
    
    @objc func appMovedToBackground() {
        print("App moved to background!")
        
        let accumulatedRunningTime = Date().timeIntervalSince(startDate!)
     
        previouslyAccumulatedTime += accumulatedRunningTime
        totalAccumulatedTime = previouslyAccumulatedTime
        
        print("PREV ACCUMULATED")
        print(previouslyAccumulatedTime)
        print("TOTAL ACCUMULATED")
        print(totalAccumulatedTime)
        print("SELF TOTAL TIME")
        print(self.totalTime)
        
        lastStopDate = Date()
       
        if(timer != nil){ //First we need to check if timer is null or not. If it is not null then only invalidate
            
            timer?.invalidate()
            timer = nil
        }
       
    }
    
    
    @objc func appMovedToForeground() {
        print("App moved to foreground!")
        startDate = lastStopDate
        // schedule a new timer
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: (#selector(updateFromBground)), userInfo: nil, repeats: false)
        
    }
    
    @objc func updateFromBground(){
        print("PREV ACCUMULATED")
        print(previouslyAccumulatedTime)
        totalAccumulatedTime = previouslyAccumulatedTime + Date().timeIntervalSince(startDate!)
        previouslyAccumulatedTime = 0
        
        print("TOTAL ACCUMULATED")
        print(totalAccumulatedTime)
        print("SELF TOTAL TIME")
        print(self.totalTime)
        self.totalTime -= Int(totalAccumulatedTime)
        
        if(timer != nil){ //First we need to check if timer is null or not. If it is not null then only invalidate
            
            timer?.invalidate()
            timer = nil
        }
        self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.updateTimer), userInfo: nil, repeats: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        stopResendTimer()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        if isBeingDismissed && isPulldownToDismiss {
            //  Should signout user in cognito pool
            AWSAuth.sharedInstance.signOut()
        }
        super.viewDidDisappear(animated)
        self.hideLoadingIndicator()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.startTimer()
    }
    
    func setUpUI(){
        
        //Fetching last 4 digits from mobile number
        let last4OfMobile = String(mobileNumber.suffix(4))
        resendBtn.isEnabled = false
        detailsLbl.text = "\(Constants.otpHeaderMsg)\(last4OfMobile)"
        
    }
    
    func startTimer(){
        
        startDate = Date()
        resendBtn.setTitleColor(UIColor.buttonDisableColor, for: .normal)
        self.totalTime = 180
        
        if(timer == nil){
            
            self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.updateTimer), userInfo: nil, repeats: true)
        }
        
    }
    
    @objc func updateTimer() {
        
      
       
        self.timeFormatted(self.totalTime) // will show timer
        if totalTime >= 0 {
            totalTime -= 1  // decrease counter timer
        } else {
            
            resendBtn.isEnabled = true
            timerLbl.isHidden = true
            resendBtn.setTitleColor(UIColor.brandPurpleColor, for: .normal)
            stopResendTimer()
        }
        
        
    }
    
    func stopResendTimer(){
        
        if(timer != nil)
        {
            timer?.invalidate()
            timer = nil
        }
        previouslyAccumulatedTime = 0
        totalAccumulatedTime = 0
        startDate = Date()
        lastStopDate = Date()
    }
    
    func timeFormatted(_ totalSeconds: Int) {
        
        let seconds: Int = totalSeconds % 60
        let minutes: Int = (totalSeconds / 60) % 60
        if(minutes != 0)
        {
            timerLbl.text = "in \(minutes)m \(seconds)s"
        }
        else
        {
            timerLbl.text = "in \(seconds)s"
        }
        timerLbl.isHidden = false
        
    }
    
    // MARK: - IBActions
    @IBAction func closeBtnAction(){
        
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.SmsVerify.UserClick.close, screenName: ParameterName.SmsVerify.screen_view)
        isPulldownToDismiss = false
        
        AWSAuth.sharedInstance.signOut()
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func resendOTPAction(){
        
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.SmsVerify.UserClick.resend, screenName: ParameterName.SmsVerify.screen_view)
        
        AWSAuth.sharedInstance.signOut(onSuccess: { result in
            self.triggerResend()
        }, onFailure: nil)
    }
    
    func triggerResend(){
        DispatchQueue.main.async {
        self.otpStackView.resetOTP()
        self.startTimer()
        self.numberOfAttempts = 3
        self.errorLbl.isHidden = true
        }
        if(isSignUp)
        {
            /*  Dead code
            AWSAuth.sharedInstance.resendOTPToPhone(mobile: mobileNumber) { result in
                
                //We don't need to show this success message to the user
                
            } onFailure: { error in
                DispatchQueue.main.async {
                    
                    self.errorLbl.isHidden = false
                    self.errorLbl.text = AWSAuth.sharedInstance.getRelevantErrorMessage(error: error)
                }
            }
             */
        }
        else
        {
            AWSAuth.sharedInstance.customSignIn(username: mobileNumber) { result in
                
                //We don't need to show this success message to the user
            } onFailure: { error in
                DispatchQueue.main.async {
                    
                    self.errorLbl.isHidden = false
                    self.errorLbl.text = AWSAuth.sharedInstance.getRelevantErrorMessage(error: error)
                }
            }

        }
        
    }

    // MARK: - OTPVerify
    func verifyOTPcode() {
                
        AnalyticsManager.shared.logEditEvent(eventValue: ParameterName.SmsVerify.UserEdit.otp_input, screenName: ParameterName.SmsVerify.screen_view)
        
        ReachabilityManager.shared.isReachable(success: { [self] in
            
            if(timer == nil)
            {
                DispatchQueue.main.async {
                self.errorLbl.isHidden = false
                self.errorLbl.text = "This code has expired. Please request for a new code below."
                }
            }
            else
            {
                processVerifyOTPCode()
            }
            
            
        }, failure: {
            self.popupAlert(title: Constants.noInternetTitle, message: Constants.noInternetMessage, actionTitles: [Constants.gotIt], actions:[{action1 in
                self.dismissAnyAlertControllerIfPresent()
                
                
            },{action2 in
                
                //We don't need this action
            }, nil])
        })
        
    }
    
    func processVerifyOTPCode(){
        
        self.showLoadingIndicator()
        
        if self.otpStackView.textFieldArray.count > 0
        {
            var otpCode = ""
            for codeInput in self.otpStackView.textFieldArray {
                
                otpCode = otpCode + codeInput.text!
            }
            print(otpCode)
            print("PREVIOUS OTP Before",previousOTPCode)
            if(previousOTPCode == otpCode)
            {
                print("PREVIOUS OTP Matches",previousOTPCode)
                return
            }
            
            if self.isSignUp {
                /*  Dead code will never been used
                previousOTPCode = otpCode
                AWSAuth.sharedInstance.confirmSignUpWithPhone(mobile: mobileNumber, otp: otpCode) { [self] result in
                    
                    AWSAuth.sharedInstance.customSignIn(username: mobileNumber) { result in
                        
                        AWSAuth.sharedInstance.customChallenge(response: Constants.tempPassword) { result in
                            
                            
                            AWSAuth.sharedInstance.updateUserAttributeFirstTimeSign() { result in
                                // Handle sucsses
                            } onFailure: { (error) in
                                // Handle falied
                                if((error as! String == "Not authorized") || (error as! String == "You are currently signed out") || (error as! String == "Invalid State") || (error as! String == "Session Expired"))
                                {
                                    self.signOutifNotValidate()
                                }
                            }
                            
                            DispatchQueue.main.async {
                                self.errorLbl.isHidden = true
                                
                                self.isPulldownToDismiss = false
                                self.delegate?.dismissViewController(controller: self)
                            }
                            
                        } onFailure: { error,stringMSG in
                            DispatchQueue.main.async {
                                self.hideLoadingIndicator()
                            }
                        }
                    } onFailure: { error in
                        DispatchQueue.main.async {
                            self.hideLoadingIndicator()
                        }
                    }

                    
                } onFailure: { error in
                    DispatchQueue.main.async {
                        
                        self.hideLoadingIndicator()
                        
                        if(self.numberOfAttempts != 0)
                        {
                            self.previousOTPCode = ""
                            self.errorLbl.isHidden = false
                            self.otpStackView.resetOTP()
                            self.otpStackView.textFieldArray[0].becomeFirstResponder()
                            self.numberOfAttempts = self.numberOfAttempts - 1
                            self.errorLbl.text = AWSAuth.sharedInstance.getRelevantErrorMessage(error: error,loginAttempts: self.numberOfAttempts)
                            if(self.numberOfAttempts == 0)
                            {
                              
                                DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                                            
                                            self.dismiss(animated: true, completion: nil)
                                        }
                                    
                                
                            }
                        }
                        
                    }
                }
                 */
            }
            else
            {
                print("PREVIOUS OTP",previousOTPCode)
                previousOTPCode = otpCode
                print("PREVIOUS OTP After assigning",previousOTPCode)
                AWSAuth.sharedInstance.customChallenge(response: otpCode) { result in
                    
                    DispatchQueue.main.async {
                        self.errorLbl.isHidden = true
                        
                        self.isPulldownToDismiss = false
                        self.delegate?.dismissViewController(controller: self)
                    }
                } onFailure: { error,stringMsg in
                    DispatchQueue.main.async {
                        self.hideLoadingIndicator()
                        
                        self.previousOTPCode = ""
                        self.otpStackView.resetOTP()
                        self.otpStackView.textFieldArray[0].becomeFirstResponder()
                        let errorMsg = stringMsg as! String
                        if(errorMsg == "")
                        {
                            self.errorLbl.isHidden = false
                            self.errorLbl.text = AWSAuth.sharedInstance.getRelevantErrorMessage(error: error)
                        }
                        else
                        {
                            self.errorLbl.isHidden = false
                            self.errorLbl.text = stringMsg as? String
                            
                            if(errorMsg.contains("30"))
                            {
                                DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                                    
                                    self.dismiss(animated: true, completion: nil)
                                }
                            }
                        }
                        
                    }
                }

            }
        }
    }
    
    
    
    func signOutifNotValidate() {
        DispatchQueue.main.async {
            AWSAuth.sharedInstance.forceSignOut { success in
                if success {
                    appDelegate().forceSignoutUser()
                }
            }
        }
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

class CustomPresentationController: UIPresentationController {
    
    override var frameOfPresentedViewInContainerView: CGRect {
        get {
            return CGRect(x: 0, y: (containerView?.bounds.height ?? 0)/2, width: containerView?.bounds.width ?? 0, height: (containerView?.bounds.height ?? 0)/2)
        }
    }
}

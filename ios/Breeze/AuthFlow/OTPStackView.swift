//
//  OTPStackView.swift
//  Breeze
//
//  Created by Malou Mendoza on 25/11/20.
//

import UIKit

enum ErrorType {
    case incorrectOTP
    case exceedRetry
    case exceedLimit
    
}

protocol OTPStackViewDelegate: AnyObject {
    func verifyOTPcode()
}
class OTPStackView: UIStackView {
    
    weak var delegate: OTPStackViewDelegate?
    var textFieldArray = [OTPTextField]()
    var numberOfOTPdigit = 6
    var errorType:ErrorType?
    var otpCode = ""
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupStackView()
        setTextFields()
    }
    
    required init(coder: NSCoder) {
        super.init(coder: coder)
        setupStackView()
        setTextFields()
    }
    

    
    public func resetOTP(){
        
        for fields in textFieldArray{
            fields.text  = ""
            fields.layer.borderColor = UIColor.passionPinkColor.cgColor
            fields.layer.borderWidth = 1.0
        }
        textFieldArray[0].becomeFirstResponder()
    }
    
    public func hideKeyboard(){
        for fields in textFieldArray{
            fields.resignFirstResponder()
            textFieldArray[5].resignFirstResponder()
        }
    }
    
    private func setupStackView() {
        self.backgroundColor = .clear
        self.isUserInteractionEnabled = true
        self.translatesAutoresizingMaskIntoConstraints = false
        self.contentMode = .center
        self.distribution = .fillEqually
        self.spacing = 10
    }
    
    private func setTextFields() {
        for i in 0..<numberOfOTPdigit {
            let field = OTPTextField()
            textFieldArray.append(field)
            addArrangedSubview(field)
            field.delegate = self
            field.backgroundColor = UIColor.rgba(r: 235, g: 235, b: 235, a: 1.0)
            field.layer.opacity = 1.0
            field.textAlignment = .center
            field.keyboardType = .numberPad
            field.layer.borderWidth = 1.0
            field.layer.borderColor = UIColor.clear.cgColor
            //            field.layer.shadowColor = UIColor.black.cgColor
            //            field.layer.shadowOpacity = 0.1
            field.layer.cornerRadius = 8
            
            i != 0 ? (field.previousTextField = textFieldArray[i-1]) : ()
            i != 0 ? (textFieldArray[i-1].nextTextFiled = textFieldArray[i]) : ()
        }
    }
}

extension OTPStackView: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {


        textField.layer.borderColor = UIColor.brandPurpleColor.cgColor

        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        for fields in textFieldArray{
            
            fields.layer.borderColor = UIColor.clear.cgColor
            
        }
       
        guard let field = textField as? OTPTextField else {
            return true
        }
        if !string.isEmpty {
            field.text = string
            
//            otpCode = otpCode + field.text!
//            print("OTPCode",otpCode)
//            print("OTPCode count",otpCode.count)
            
            if ((field.nextTextFiled) != nil){
                //field.resignFirstResponder()
                field.nextTextFiled?.becomeFirstResponder()
                field.nextTextFiled?.layer.borderColor = UIColor.passionPinkColor.cgColor
            }else{
                
                //verifyOTPcode
                if (delegate != nil){
                    
//                    if(otpCode.count == 6)
//                    {
                        delegate?.verifyOTPcode()
                        //otpCode = ""
                    //}
                   
                }
                return false;
            }
        }
        return true
    }
}

class OTPTextField: UITextField {
    var previousTextField: UITextField?
    var nextTextFiled: UITextField?
    
    override func deleteBackward() {
        text = ""
        previousTextField?.becomeFirstResponder()
        previousTextField?.text = ""
    }
}


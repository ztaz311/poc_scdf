//
//  ConfirmSigninAsGuestVC.swift
//  Breeze
//
//  Created by Tuan, Pham Hai on 14/03/2023.
//

import UIKit

typealias ConfirmSigninAsGuestCompletion = ()-> (Void)

class ConfirmSigninAsGuestVC: BaseViewController {
    
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var proceedButton: UIButton!
    @IBOutlet weak var contentView: UIView!
    
    var cancelAction: ConfirmSigninAsGuestCompletion?
    var proceedAction: ConfirmSigninAsGuestCompletion?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        cancelButton.layer.cornerRadius = 24
        cancelButton.layer.masksToBounds = true
        cancelButton.layer.borderWidth = 1.0
        cancelButton.layer.borderColor = UIColor(hexString: "782EB1", alpha: 1).cgColor
        
        proceedButton.layer.cornerRadius = 24
        proceedButton.layer.masksToBounds = true
        
        contentView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        contentView.layer.cornerRadius = 10.0
        contentView.layer.masksToBounds = true
    }
    
    @IBAction func clickCancelAction(_ sender: Any) {
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.SignInCreate.UserClick.guest_mode_warning_cancel, screenName: ParameterName.SignInCreate.screen_view)
//        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.OnBoardingGuest.UserClick.guestContinueCancel, screenName: ParameterName.OnBoardingGuest.screen_view)
        self.view.removeFromSuperview()
        cancelAction?()
    }
    
    @IBAction func clickProceedAction(_ sender: Any) {
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.SignInCreate.UserClick.guest_mode_warning_proceed, screenName: ParameterName.SignInCreate.screen_view)
//        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.OnBoardingGuest.UserClick.guestContinueProceed, screenName: ParameterName.OnBoardingGuest.screen_view)
        self.view.removeFromSuperview()
        self.proceedAction?()
    }
    
    @IBAction func clickCloseAction(_ sender: Any) {
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.SignInCreate.UserClick.guest_mode_warning_close, screenName: ParameterName.SignInCreate.screen_view)
//        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.OnBoardingGuest.UserClick.guestContinueClose, screenName: ParameterName.OnBoardingGuest.screen_view)
        self.view.removeFromSuperview()
        cancelAction?()
    }
}

//
//  ViewController.swift
//  Breeze
//
//  Created by VishnuKanth on 18/11/20.
//

import UIKit
import Amplify
import AWSCognitoAuthPlugin
import CoreLocation
import ReachabilityManager
import Reachability
import AWSMobileClient
import Instabug
import Firebase
import SwiftyBeaver

class LandingViewController: BaseViewController,
                             UITextFieldDelegate {

    @IBOutlet var emailField: DesignableTextField!
    @IBOutlet var passwordField: DesignableTextField!
    @IBOutlet var signUpBtn: UIButton!
    @IBOutlet var fbBtn: UIButton!
    @IBOutlet var googleBtn: UIButton!
    @IBOutlet var appleBtn: UIButton!
    
    var feedbackButton: FeedbackButton?
    var feedbackBtn = UIButton(type: .custom)
    var tileListBtn = UIButton(type: .custom)
    
    var toScreen: CNMScreenId?
    var childViewController: SuccessViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = true
        setUpLandingView()

        setupDarkLightAppearance(forceLightMode: true)
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(applicationWillEnterForeground(_:)),
            name: UIApplication.willEnterForegroundNotification,
            object: nil)
        
        addTutorialLandingRN()        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        appDelegate().findAndShowAppUpdateAlerts(controller: self)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
    }
    
    private func addTutorialLandingRN() {
        let rnBottomSheetVC = getRNViewBottomSheet(toScreen: CNMScreenId.tutorial.rawValue, navigationParams: [:])
        rnBottomSheetVC.view.frame = UIScreen.main.bounds;
        self.addChildViewControllerWithView(childViewController: rnBottomSheetVC,toView: self.view)
    }
    
    func resetRNInstance() {
        let _ = getRNViewBottomSheet(toScreen: CNMScreenId.tutorial.rawValue, navigationParams: [:])
    }
    
    @objc func applicationWillEnterForeground(_ notification: NSNotification) {
        // Only start when this view is visible
        
        //When app comes to foreground check again the app version
        appDelegate().findAndShowAppUpdateAlerts(controller: self)
    }
    
    func setUpLandingView(){
        self.setupUI()
    }
    
    func setupUI() {
        fbBtn.setTitle("Continue in with Facebook", for: .normal)
        fbBtn.setImage(UIImage(named: "fbIcon"), for: .normal)
        fbBtn.backgroundColor = UIColor.rgba(r: 24, g: 119, b: 242, a: 1.0)
        fbBtn.layer.cornerRadius = 20
        fbBtn.clipsToBounds = true
        fbBtn.layer.borderColor = UIColor.clear.cgColor
        fbBtn.layer.borderWidth = 1
        fbBtn.moveImageLeftTextCenter(image: UIImage(named: "fbIcon")!, imagePadding: 20, renderingMode: .alwaysOriginal, alignment: .center)
        
        googleBtn.setTitle("Continue in with Google", for: .normal)
        googleBtn.setImage(UIImage(named: "googleIcon"), for: .normal)
        googleBtn.backgroundColor = .white
        googleBtn.setTitleColor(UIColor.rgba(r: 0, g: 0, b: 0, a: 0.54), for: .normal)
        googleBtn.layer.cornerRadius = 20
        googleBtn.clipsToBounds = true
        googleBtn.layer.borderColor = UIColor.rgba(r: 153, g: 153, b: 153, a: 1.0).cgColor
        googleBtn.layer.borderWidth = 1
        googleBtn.moveImageLeftTextCenter(image: UIImage(named: "googleIcon")!, imagePadding: 20, renderingMode: .alwaysOriginal, alignment: .center)
        
        
        appleBtn.setTitle("Sign in with Apple", for: .normal)
        appleBtn.moveImageLeftTextCenter(image: UIImage(named: "appleIcon")!, imagePadding: 20, renderingMode: .alwaysOriginal, alignment: .center)
        appleBtn.backgroundColor = .white
        appleBtn.setTitleColor(UIColor.rgba(r: 0, g: 0, b: 0, a: 0.54), for: .normal)
        appleBtn.layer.cornerRadius = 20
        appleBtn.clipsToBounds = true
        appleBtn.layer.borderColor = UIColor.rgba(r: 153, g: 153, b: 153, a: 1.0).cgColor
        appleBtn.layer.borderWidth = 1
        
        signUpBtn.applyGradient(colors: [UIColor.brandPurpleColor.cgColor,UIColor.brandPurpleColor.cgColor], withShadow: false)
    }

    func sessionCheck() {
        AWSAuth.sharedInstance.fetchCurrentSession { [weak self] (session) in
            guard let self = self else { return }
            if let session = session as? AWSAuthCognitoSession {
                
                DispatchQueue.main.async {
                    self.hideLoadingIndicator()
                }
                
                if session.isSignedIn {
                    AWSAuth.sharedInstance.fetchAttributes { [weak self] result in
                        guard let self = self else { return }
                        BugReporting.bugReportingOptions = [.emailFieldHidden]
                        
                        if(AWSAuth.sharedInstance.userName.isEmpty)
                        {
                            DispatchQueue.main.async {
                                let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                let vc: SetUserNameVC = storyboard.instantiateViewController(withIdentifier: "SetUserNameVC")  as! SetUserNameVC
                                vc.fromAccount = true
                                self.navigationController?.pushViewController(vc, animated: true)
                                
                            }
                        } else {
                            DispatchQueue.main.async {
                                self.showWelcomeBack()
                            }
                            return
                        }
                        
                    } onFailure: { [weak self] error in
                        guard let self = self else { return }
                        BugReporting.bugReportingOptions = [.none]
                        self.inLoginPage()
                    }
                    
                } else {
                    BugReporting.bugReportingOptions = [.none]
                    self.inLoginPage()
                }
            }
        } onFailure: { [weak self] (error) in
            guard let self = self else { return }
            DispatchQueue.main.async {
                self.hideLoadingIndicator()
            }
            self.inLoginPage()
            BugReporting.bugReportingOptions = [.none]
            print(error.localizedDescription)
        }
    }

    // Still in login page
    func inLoginPage() {
        AWSAuth.sharedInstance.signOut()
    }
    
    @IBAction func loginClicked(_ sender: Any) {
        pushToSignIn()
    }
    
    func pushToSignIn() {
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Onboarding.UserClick.signin, screenName: ParameterName.Onboarding.screen_view)
        
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc: UIViewController = storyboard.instantiateViewController(withIdentifier: "NewSigninGuestModeViewController") as UIViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func pushToSignUp() {
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Onboarding.UserClick.create_acc, screenName: ParameterName.Onboarding.screen_view)
        
        let storyboard: UIStoryboard = UIStoryboard(name: "Web", bundle: nil)
        let vc: WebVC = storyboard.instantiateViewController(withIdentifier: "WebVC") as! WebVC
        vc.isAccountCreation = true
        vc.urlString = Configuration.termsOfUseURL
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func signUp(_ sender: Any) {
        pushToSignUp()
    }
    
    func locationAuthorization(status:CLAuthorizationStatus){
        
        //Swift
        if status == .authorizedAlways || status == .authorizedWhenInUse {
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Onboarding.UserClick.location_permission_allow, screenName: ParameterName.Onboarding.screen_view)
        } else {
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Onboarding.UserClick.location_permission_deny, screenName: ParameterName.Onboarding.screen_view)
        }
        
        switch status {
        case .notDetermined:
            break
        case .authorizedAlways:
            self.sessionCheck()
        case .authorizedWhenInUse:
            self.sessionCheck()
        case .restricted:
            break
        case .denied:
            break
        @unknown default:
            break
        }
    }
        
    func setUserPrefEmpty() {        
        triggerResetAmenities { [weak self] _ in
            DispatchQueue.main.async {
                guard let self = self else { return }
                self.goToLandingVC()
            }
        }
    }
    
    func startRNScreen(){
        let state = AWSAuth.sharedInstance.onboardingState == nil ? UserOnboardingState.ONBOARDING_INIT : UserOnboardingState.ONBOARDING_STEP_1
        let params = [ // Object data to be used by that screen
            "onboardingState":state,
            "baseURL": appDelegate().backendURL,
            "idToken":AWSAuth.sharedInstance.idToken,
            "deviceos":parameters!["deviceos"],
            "deviceosversion":parameters!["deviceosversion"],
            "devicemodel":parameters!["devicemodel"],
            "appversion":parameters!["appversion"],
        ]
        self.goToRNScreen(toScreen: RNScreeNames.FIRST_APP_TUTORIAL,navigationParams: params as [String:Any])
    }
    
    @IBAction func fbClicked(_ sender: Any) {
        
        ReachabilityManager.shared.isReachable(success: {
            
//            AnalyticsManager.shared.sendLoginEvent(method: ParameterName.signin_facebook)
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Onboarding.UserClick.facebook, screenName: ParameterName.Onboarding.screen_view)

            AWSAuth.sharedInstance.faceBookSignIn(presentAnchor: self.view.window!) { [weak self] (result) in
                DispatchQueue.main.async {
                    self?.checkLocationStatusBeforeFetchSession()
                }
            } onFailure: { [weak self] (error) in
                guard let self = self else { return }
                if(error as! String == "Already SignIn") {
                    self.sessionCheck()
                }
            }
        
        
        }, failure: { [weak self] in
            guard let self = self else { return }
            self.popupAlert(title: Constants.noInternetTitle, message: Constants.noInternetMessage, actionTitles: [Constants.gotIt], actions:[{ [weak self] action1 in
                guard let self = self else { return }
                self.dismissAnyAlertControllerIfPresent()
            },{ action2 in
                //We don't need to handle this action
            }, nil])
          })
    }
    
    @IBAction func googleClicked(_ sender: Any) {
        
        ReachabilityManager.shared.isReachable(success: { [weak self] in
            guard let self = self else { return }
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Onboarding.UserClick.google, screenName: ParameterName.Onboarding.screen_view)

            AWSAuth.sharedInstance.googleSignIn(presentAnchor: self.view.window!) {[weak self] (result) in
                DispatchQueue.main.async {
                    self?.checkLocationStatusBeforeFetchSession()
                }
            } onFailure: { [weak self] (error) in
                guard let self = self else { return }
                if(error as! String == "Already SignIn")
                {
                    self.sessionCheck()
                }

            }
          }, failure: { [weak self] in
              guard let self = self else { return }
              self.popupAlert(title: Constants.noInternetTitle, message: Constants.noInternetMessage, actionTitles: [Constants.gotIt], actions:[{action1 in
                  self.dismissAnyAlertControllerIfPresent()
                  
                  
              },{action2 in
                  
                  //We don't need this action
                  
              }, nil])
          })
    }
    
    @IBAction func appleClicked(_ sender: Any) {
        
        ReachabilityManager.shared.isReachable(success: { [weak self] in
            guard let self = self else { return }
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Onboarding.UserClick.apple, screenName: ParameterName.Onboarding.screen_view)

            AWSAuth.sharedInstance.appleSignIn(presentAnchor: self.view.window!) { [weak self] (result) in
                DispatchQueue.main.async {
                    self?.checkLocationStatusBeforeFetchSession()
                }
            } onFailure: { [weak self] (error) in
                guard let self = self else { return }
                if(error as! String == "Already SignIn")
                {
                    self.sessionCheck()
                }
            }
        }, failure: { [weak self] in
            guard let self = self else { return }
            self.popupAlert(title: Constants.noInternetTitle, message: Constants.noInternetMessage, actionTitles: [Constants.gotIt], actions:[{ [weak self] action1 in
                guard let self = self else { return }
                self.dismissAnyAlertControllerIfPresent()
                
                
            },{action2 in
                
                //We don't need to handle this action
                
            }, nil])
        })
    }

}

//extension LandingViewController: AWSCognitoAuthDelegate {
//    func getViewController() -> UIViewController {
//        return self
//    }
//}

//  Extension for guest mode
extension LandingViewController {
    
    func  customizeFontStyle(string: String, font: UIFont) -> NSMutableAttributedString {
        return NSMutableAttributedString(string: string, attributes:
                                            [NSAttributedString.Key.font : font ])
    }
    
    func getSuccessText() -> NSAttributedString {
        
        let hiStr = "Welcome back,\n"
        
        let userNameStr = AWSAuth.sharedInstance.userName
        
        let attributedString = customizeFontStyle(string: hiStr, font:  UIFont(name: fontFamilySFPro.Regular, size: 26)!)
        
        attributedString.append(customizeFontStyle(string: userNameStr, font:  UIFont(name: fontFamilySFPro.MaxBold, size: 26)!))
        
        return attributedString
    }
    
    private func showWelcomeBack() {
        if (self.navigationController?.viewControllers.last is WebVC) {
            self.navigationController?.popViewController(animated: false)
        }
        
        AnalyticsManager.shared.logOpenPopupEvent(popupName: ParameterName.SignIn.UserPopup.welcome, screenName: ParameterName.SignIn.screen_view)
        
        self.childViewController = SuccessViewController()
        childViewController?.delegate = self
        childViewController?.goToLanding = true
        childViewController?.successImage = Images.accountCreatedImage
        childViewController?.successText = self.getSuccessText()
        if let vc = self.childViewController {
            self.addChildViewControllerWithView(childViewController: vc as UIViewController,toView: self.view)
        }
    }
    
    func proceedLoginAsGuest() {
        
        self.showLoadingIndicator()
        AWSAuth.sharedInstance.signInAsGuestUser {[weak self] success in
            DispatchQueue.main.async {
                if success {
                    Prefs.shared.setValue(true, forkey: .isGuestMode)
                    self?.checkAcceptedTCInSignin()
                }else {
                    self?.hideLoadingIndicator()
                }
            }
        }
    }
    
    //  Always need to check location permission before navigate to home screen
    private func checkLocationStatusBeforeFetchSession() {
        
        let status = LocationManager.shared.getStatus()
        if(status != .notDetermined)
        {
            self.locationAuthorization(status: status)
        }
        else
        {
            LocationManager.shared.requestLocationAuthorization()
            LocationManager.shared.requestLocationAuthorizationCallback = { [weak self] status in
                guard let self = self else { return }
                self.locationAuthorization(status: status)
            }
        }
    }
    
    func checkAcceptedTCInSignin() {
        AWSAuth.sharedInstance.getAcceptTCStatus { [weak self] isSuccess in
            guard let self = self else { return }
            DispatchQueue.main.async {
                if (isSuccess && !AWSAuth.sharedInstance.acceptTC) {
                    
                    self.hideLoadingIndicator()
                    
                    let storyboard: UIStoryboard = UIStoryboard(name: "Web", bundle: nil)
                    let vc: WebVC = storyboard.instantiateViewController(withIdentifier: "WebVC") as! WebVC
                    vc.isAccountCreation = true
                    vc.urlString = Configuration.termsOfUseURL
                    vc.delegate = self
                    vc.type = .onlyShow
                    self.navigationController?.pushViewController(vc, animated: true)
                    return
                }
                
                self.checkLocationStatusBeforeFetchSession()
            }
        }
    }
    
    private func signOutAndPopToRoot() {
        guard let landingVC = self.navigationController?.viewControllers.first(where: { vc in
            return vc is LandingViewController
        }) else { return }
        self.navigationController?.popToViewController(landingVC, animated: true)
        AWSAuth.sharedInstance.signOut()
    }
}

extension LandingViewController: SuccessScreenControllerDelegate{
    func SuccessScreenControllerDismiss() {
        
        self.childViewController?.view.removeFromSuperview()
        
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.OnBoardingGuest.UserClick.guestSuccessNext, screenName: ParameterName.OnBoardingGuest.screen_view)
//        AnalyticsManager.shared.logClosePopupEvent(popupName: ParameterName.SignIn.UserPopup.welcome, screenName: ParameterName.SignIn.screen_view)

        DispatchQueue.global(qos: .background).async { [weak self] in
            guard let self = self else { return }
            
            self.setUserPrefEmpty()
        }
    }
}

extension LandingViewController: WebVCDelegate {
    
    func didAccepted() {
        
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.OnBoardingGuest.UserClick.guestTnCAgree, screenName: ParameterName.OnBoardingGuest.screen_view)
//        self.startRNScreen()
        AWSAuth.sharedInstance.changeAcceptTC { [weak self] success in
            guard let self = self else { return }
            // Update value to server
            if success {
                self.checkLocationStatusBeforeFetchSession()
            }
        }
    }
    
    func didDeclined() {
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.OnBoardingGuest.UserClick.guestTnCDeline, screenName: ParameterName.OnBoardingGuest.screen_view)
        self.signOutAndPopToRoot()
    }
    
    func didBacked() {
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.OnBoardingGuest.UserClick.guestTnCBack, screenName: ParameterName.OnBoardingGuest.screen_view)
    }
}

class CustompanGestureRecognizer: UIPanGestureRecognizer {
    var floatingView: FloatingView?
}


//
//  SignInMobilePhoneViewController.swift
//  Breeze
//
//  Created by Tan, Tran Ngoc on 14/03/2023.
//

import Foundation
import Amplify
import CoreLocation
import UIKit

class SignInMobilePhoneViewController: BaseViewController, UITextFieldDelegate {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var mobileField: DesignableTextField!
    @IBOutlet weak var errorMsg: UILabel!
    
    var mobileText = ""
    var isSignUp = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        setupUI()
        navigationController?.navigationBar.barTintColor = UIColor.brandPurpleColor
        
        setupDarkLightAppearance(forceLightMode: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if ( mobileText != ""){
            mobileField.text = mobileText
            donePressed()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.hideLoadingIndicator()
    }
    
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    func setupUI() {
        mobileField.layer.cornerRadius = 25
        mobileField.clipsToBounds = true
        mobileField.layer.borderColor = UIColor.brandPurpleColor.cgColor
        mobileField.layer.borderWidth = 1
        mobileField.delegate = self
        mobileField.keyboardType = UIKeyboardType.numberPad
        
        errorMsg.isHidden = true
        mobileField.enablesReturnKeyAutomatically  = true
        
        self.setToolBar()
        
        let text = "other_signup_button".localed
        let attS: [NSAttributedString.Key: Any] = [NSAttributedString.Key.font: UIFont.buttonAuthSemiBoldFont(), NSAttributedString.Key.foregroundColor: UIColor.authButtonColor]
        let attributeString = NSMutableAttributedString(string: text, attributes: attS)
        
        let createAccountRange = text.nsRange(of: "create_here".localed)
        if (createAccountRange.location != NSNotFound) {
            attributeString.addAttribute(.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: createAccountRange)
        }
        
    }
    
    func setToolBar(){
        let tooBar: UIToolbar = UIToolbar()
        tooBar.barStyle = .default
        tooBar.items = [
            UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: self, action: nil),
            UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(donePressed))]
        tooBar.sizeToFit()
        mobileField.inputAccessoryView = tooBar
    }
    
    
    @objc func donePressed() {
        
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.SignIn.UserClick.done, screenName: ParameterName.SignIn.screen_view)
        
        if(mobileField.text == "" ||  !PhoneValidator.isValidSingaporeMobile(number: mobileField.text!))
        {
            self.errorMsg.isHidden = false
            errorMsg.text = Constants.mobileNumberErrorMsg
        }
        else
        {
            self.showLoadingIndicator()
            mobileField.inputAccessoryView?.isHidden = true
            mobileField.inputAccessoryView?.isUserInteractionEnabled = false
            
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.OnBoardingSignIn.UserClick.signInMobileNumInput, screenName: ParameterName.OnBoardingSignIn.screen_view)
            
            let numberWithCountryCode = "\(Values.mobileNumberPrefix)\(mobileField.text ?? "")"
            print("Sending Mobile number for Custom Sign In",numberWithCountryCode)
            AWSAuth.sharedInstance.customSignIn(username: numberWithCountryCode) { [weak self] result in
                guard let self = self else { return }
                if(result as! String == "confirm signup")
                {
                    self.isSignUp = true
                    AWSAuth.sharedInstance.resendOTPToPhone(mobile: numberWithCountryCode) { [weak self] result in
                        guard let self = self else { return }
                        self.goToOTPScreen(isSignup: true)
                        
                    } onFailure: { [weak self] error in
                        guard let self = self else { return }
                        DispatchQueue.main.async{
                            
                            self.mobileField.inputAccessoryView?.isHidden = false
                            self.mobileField.inputAccessoryView?.isUserInteractionEnabled = true
                            self.hideLoadingIndicator()
                            self.errorMsg.isHidden = false
                            self.errorMsg.text = AWSAuth.sharedInstance.getRelevantErrorMessage(error: error)
                        }
                    }
                    
                }
                else
                {
                    
                    self.goToOTPScreen(isSignup: false)
                    
                }
                
                
            } onFailure: { [weak self] error in
                guard let self = self else { return }
                DispatchQueue.main.async{
                    self.mobileField.inputAccessoryView?.isHidden = false
                    self.mobileField.inputAccessoryView?.isUserInteractionEnabled = true
                    self.hideLoadingIndicator()
                    self.errorMsg.isHidden = false
                    self.errorMsg.text = AWSAuth.sharedInstance.getRelevantErrorMessage(error: error)
                }
                
            }
            
            
        }
    }
    
    func goToOTPScreen(isSignup:Bool){
        
        DispatchQueue.main.async {
            self.mobileField.inputAccessoryView?.isHidden = false
            self.mobileField.inputAccessoryView?.isUserInteractionEnabled = true
            self.mobileField.resignFirstResponder()
            self.hideLoadingIndicator()
            let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc: OTPVerifyVC = storyboard.instantiateViewController(withIdentifier: "OTPVerifyVC")  as! OTPVerifyVC
            let numberWithCountryCode = "\(Values.mobileNumberPrefix)\(self.mobileField.text ?? "")"
            vc.mobileNumber = numberWithCountryCode
            vc.isSignUp = isSignup
            vc.delegate = self
            vc.modalPresentationStyle = .formSheet
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    func locationAuthorization(status:CLAuthorizationStatus){
        
        //Swift
        switch status {
        case .notDetermined:
            break
        case .authorizedAlways:
            self.getCurrentSession()
        case .authorizedWhenInUse:
            self.getCurrentSession()
        case .restricted:
            break
        case .denied:
            break
        @unknown default:
            break
        }
    }
    
    func setUserPrefEmpty() {
        
        triggerResetAmenities { [weak self] _ in
            DispatchQueue.main.async {
                guard let self = self else { return }
                
                //If onBoarding state is not completed then open OnBoaridngRN screen
//                if(AWSAuth.sharedInstance.onboardingState == nil || AWSAuth.sharedInstance.onboardingState == UserOnboardingState.ONBOARDING_STEP_1){
//                    self.startRNScreen()
//                } else {
                    self.goToLandingVC()
//                }
            }
            
        }
    }
    
    func startRNScreen(){
        let state = AWSAuth.sharedInstance.onboardingState == nil ? UserOnboardingState.ONBOARDING_INIT : UserOnboardingState.ONBOARDING_STEP_1
        let params = [ // Object data to be used by that screen
            "onboardingState":state,
            "baseURL": appDelegate().backendURL,
            "idToken":AWSAuth.sharedInstance.idToken,
            "deviceos":parameters!["deviceos"],
            "deviceosversion":parameters!["deviceosversion"],
            "devicemodel":parameters!["devicemodel"],
            "appversion":parameters!["appversion"],
        ]
        self.goToRNScreen(toScreen: RNScreeNames.FIRST_APP_TUTORIAL,navigationParams: params as [String:Any])
    }
    
    func checkAcceptedTCInSignin() {
        AWSAuth.sharedInstance.getAcceptTCStatus { [weak self] isSuccess in
            guard let self = self else { return }
            DispatchQueue.main.async {
                if (isSuccess && !AWSAuth.sharedInstance.acceptTC) {
                    let storyboard: UIStoryboard = UIStoryboard(name: "Web", bundle: nil)
                    let vc: WebVC = storyboard.instantiateViewController(withIdentifier: "WebVC") as! WebVC
                    vc.isAccountCreation = true
                    vc.urlString = Configuration.termsOfUseURL
                    vc.delegate = self
                    vc.type = .onlyShow
                    self.navigationController?.pushViewController(vc, animated: true)
                    return
                }
                self.pushScreenAfterSignin()
            }
        }
    }
    
    private func signOutAndPopToRoot() {
        
        guard let landingVC = self.navigationController?.viewControllers.first(where: { vc in
            return vc is NewSigninGuestModeViewController
        }) else { return }
        self.navigationController?.popToViewController(landingVC, animated: true)
        AWSAuth.sharedInstance.signOut()
    }
    
    private func pushScreenAfterSignin() {
        DispatchQueue.main.async {
            
            if(!AWSAuth.sharedInstance.userName.isEmpty)
            {
                if (self.navigationController?.viewControllers.last is WebVC) {
                    self.navigationController?.popViewController(animated: false)
                }
                AnalyticsManager.shared.logOpenPopupEvent(popupName: ParameterName.SignIn.UserPopup.welcome, screenName: ParameterName.SignIn.screen_view)
                
                let childViewController = SuccessViewController()
                childViewController.delegate = self
                childViewController.successText = self.getSuccessText()
                childViewController.goToLanding = true
                childViewController.successImage = Images.accountCreatedImage
                self.addChildViewControllerWithView(childViewController: childViewController as UIViewController,toView: self.view)
            }
            else
            {
                let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let vc: SetUserNameVC = storyboard.instantiateViewController(withIdentifier: "SetUserNameVC")  as! SetUserNameVC
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
        }
    }
    
    func getCurrentSession(){
        self.showLoadingIndicator()
        AWSAuth.sharedInstance.fetchCurrentSession { [weak self] (result) in
            
            AWSAuth.sharedInstance.fetchAttributes { result in
                guard let self = self else { return }
                self.checkAcceptedTCInSignin()
                
            } onFailure: { error in
                guard let self = self else { return }
                DispatchQueue.main.async {
                    self.goToLandingVC()
                }
            }
            
        } onFailure: { [weak self] (error) in
            guard let self = self else { return }
            DispatchQueue.main.async {
                self.goToLandingVC()
            }
        }
    }
    
    func processSignIn(){
        //  self.errorEmailHeightConstraint.constant = 0
        if mobileField.text != "" {
            
            if(mobileField.text != ""   )
            {
                if  validateEmail(enteredEmail: mobileField.text!.lowercased()){
                    
                    AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.login, screenName: ParameterName.screenName)
                    
                }else{
                    self.mobileField.layer.borderColor = UIColor.red.cgColor
                    //  self.errorEmailHeightConstraint.constant = 17
                }
            }
            
        }else{
            if mobileField.text == "" {
                mobileField.layer.borderColor = UIColor.red.cgColor
                
            }
            
        }
    }
    
    @IBAction func loginClicked(_ sender: Any) {
        
        Prefs.shared.removeValueForKey(.phoneNumber)
        
        ReachabilityManager.shared.isReachable(success: { [weak self] in
            guard let self = self else { return }
            self.processSignIn()
        }, failure: { [weak self] in
            guard let self = self else { return }
            self.popupAlert(title: Constants.noInternetTitle, message: Constants.noInternetMessage, actionTitles: [Constants.gotIt], actions:[{ [weak self] action1 in
                guard let self = self else { return }
                self.dismissAnyAlertControllerIfPresent()
                
                
            },{action2 in
                
                //When reachability status failed, we are not displaying any error message, so at the moment we don't handle this block
            }, nil])
        })
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        self.errorMsg.isHidden = true
        mobileField.layer.borderColor = UIColor.brandPurpleColor.cgColor
        
        // get the current text, or use an empty string if that failed
        let currentText = textField.text ?? ""
        
        // attempt to read the range they are trying to change, or exit if we can't
        guard let stringRange = Range(range, in: currentText) else { return false }
        
        // add their new text to the existing text
        let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
        
        
        if textField == self.mobileField {
            
            return updatedText.count <= Values.mobileNumberCount
        }else{
            return true
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if(textField == mobileField){
            if !(validateMobile(enteredMobile: mobileField.text!)) {
                
                self.errorMsg.isHidden = false
                mobileField.layer.borderColor = UIColor.red.cgColor
                
                return false
            }
            
            
        }
        
        if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
            
        } else {
            self.loginClicked(true)
        }
        
        return false
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        
        if(textField == mobileField)
        {
            
            AnalyticsManager.shared.logEditEvent(eventValue: ParameterName.email, screenName: ParameterName.screenName)
        }
        else
        {
            AnalyticsManager.shared.logEditEvent(eventValue: ParameterName.password, screenName: ParameterName.screenName)
        }
        
        textField.layer.borderColor = UIColor.brandPurpleColor.cgColor
        mobileField.layer.borderColor = UIColor.brandPurpleColor.cgColor
        
        return true
    }
    
    func  customizeFontStyle(string: String, font: UIFont) -> NSMutableAttributedString {
        return NSMutableAttributedString(string: string, attributes:
                                            [NSAttributedString.Key.font : font ])
    }
    
    func getSuccessText() -> NSAttributedString {
        
        let hiStr = "Welcome back,\n"
        
        let userNameStr = AWSAuth.sharedInstance.userName
        
        let attributedString = customizeFontStyle(string: hiStr, font:  UIFont(name: fontFamilySFPro.Regular, size: 26)!)
        
        attributedString.append(customizeFontStyle(string: userNameStr, font:  UIFont(name: fontFamilySFPro.MaxBold, size: 26)!))
        
        
        return attributedString
    }
    
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}


extension SignInMobilePhoneViewController: WebVCDelegate {
    func didAccepted() {
        self.pushScreenAfterSignin()
        
        AWSAuth.sharedInstance.changeAcceptTC { _ in
            // Update value to server
        }
    }
    
    func didDeclined() {
        self.signOutAndPopToRoot()
    }
    
    func didBacked() {
        
    }
}

extension SignInMobilePhoneViewController: SuccessScreenControllerDelegate{
    func SuccessScreenControllerDismiss() {
        
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.OnBoardingSignIn.UserClick.signinSuccessNext, screenName: ParameterName.OnBoardingSignIn.screen_view)

//        AnalyticsManager.shared.logClosePopupEvent(popupName: ParameterName.SignIn.UserPopup.welcome, screenName: ParameterName.SignIn.screen_view)
        
        DispatchQueue.global(qos: .background).async { [weak self] in
            guard let self = self else { return }
            self.setUserPrefEmpty()
        }
    }
}

extension SignInMobilePhoneViewController: DismissDelegate{
    
    func dismissViewController(controller: UIViewController) {
        
        controller.dismiss(animated: false) { [weak self] in
            guard let self = self else { return }
            let numberWithCountryCode = "\(Values.mobileNumberPrefix)\(self.mobileField.text ?? "")"
            Prefs.shared.setValue(numberWithCountryCode, forkey: .phoneNumber)
            
            if(self.isSignUp == true)
            {
                self.isSignUp = false
                let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let vc: SetUserNameVC = storyboard.instantiateViewController(withIdentifier: "SetUserNameVC")  as! SetUserNameVC
                self.navigationController?.pushViewController(vc, animated: true)
                
            }
            else
            {
                let status = LocationManager.shared.getStatus()
                if(status != .notDetermined)
                {
                    self.locationAuthorization(status: status)
                }
                else
                {
                    LocationManager.shared.requestLocationAuthorization()
                    LocationManager.shared.requestLocationAuthorizationCallback = { [weak self] status in
                        guard let self = self else { return }
                        self.locationAuthorization(status: status)
                    }
                }
            }
        }
    }
}

//
//  NewSigninGuestModeViewController.swift
//  Breeze
//
//  Created by Tan, Tran Ngoc on 14/03/2023.
//

import UIKit
import Amplify
import AWSCognitoAuthPlugin
import CoreLocation
import ReachabilityManager
import Reachability
import AWSMobileClient
import Instabug
import Firebase
import SwiftyBeaver

class NewSigninGuestModeViewController: BaseViewController {

    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var fbSigninBtn: UIButton!
    @IBOutlet weak var googleSinginBtn: UIButton!
    @IBOutlet weak var appleSigninBtn: UIButton!
    @IBOutlet weak var numberSigninBtn: UIButton! {
        didSet {
            numberSigninBtn.isHidden = true
        }
    }
    
    @IBOutlet weak var signInBtn: UIButton!
    
    var isSignUp: Bool = false
    var childViewController: SuccessViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        // Do any additional setup after loading the view.
    }
    
    private func setupUI() {
        
        setupDarkLightAppearance(forceLightMode: true)
        
        numberSigninBtn.isHidden = true
        fbSigninBtn.isHidden = true
        
        if isSignUp == true {
            titleLabel.text = "Create account"
            titleLabel.font = UIFont.boldSystemFont(ofSize: 26)
            titleLabel.textColor = UIColor(hexString: "#222638", alpha: 1)
            setupStackView()
        } else {
            titleLabel.text = "Welcome back to Breeze!"
            titleLabel.font = UIFont.boldSystemFont(ofSize: 26)
            titleLabel.textColor = UIColor(hexString: "#222638", alpha: 1)
            setupStackView()
            numberSigninBtn.setTitle("Sign in with mobile number", for: .normal)
            numberSigninBtn.moveImageLeftTextCenter(image: UIImage(named: "mobilePhoneIcon")!, imagePadding: 20, renderingMode: .alwaysOriginal, alignment: .center)
            numberSigninBtn.backgroundColor = .white
            numberSigninBtn.setTitleColor(UIColor.rgba(r: 0, g: 0, b: 0, a: 0.54), for: .normal)
            numberSigninBtn.layer.cornerRadius = 20
            numberSigninBtn.clipsToBounds = true
            numberSigninBtn.layer.borderColor = UIColor.rgba(r: 153, g: 153, b: 153, a: 1.0).cgColor
            numberSigninBtn.layer.borderWidth = 1
        }
    }
    
    private func setupStackView() {
        
        fbSigninBtn.setTitle("Continue in with Facebook", for: .normal)
        fbSigninBtn.setImage(UIImage(named: "fbIcon"), for: .normal)
        fbSigninBtn.backgroundColor = UIColor.rgba(r: 24, g: 119, b: 242, a: 1.0)
        fbSigninBtn.layer.cornerRadius = 20
        fbSigninBtn.clipsToBounds = true
        fbSigninBtn.layer.borderColor = UIColor.clear.cgColor
        fbSigninBtn.layer.borderWidth = 1
        fbSigninBtn.moveImageLeftTextCenter(image: UIImage(named: "fbIcon")!, imagePadding: 20, renderingMode: .alwaysOriginal, alignment: .center)
        
        googleSinginBtn.setTitle("Continue in with Google", for: .normal)
        googleSinginBtn.setImage(UIImage(named: "googleIcon"), for: .normal)
        googleSinginBtn.backgroundColor = .white
        googleSinginBtn.setTitleColor(UIColor.rgba(r: 0, g: 0, b: 0, a: 0.54), for: .normal)
        googleSinginBtn.layer.cornerRadius = 20
        googleSinginBtn.clipsToBounds = true
        googleSinginBtn.layer.borderColor = UIColor.rgba(r: 153, g: 153, b: 153, a: 1.0).cgColor
        googleSinginBtn.layer.borderWidth = 1
        googleSinginBtn.moveImageLeftTextCenter(image: UIImage(named: "googleIcon")!, imagePadding: 20, renderingMode: .alwaysOriginal, alignment: .center)
        
        appleSigninBtn.setTitle("Continue with Apple", for: .normal)
        appleSigninBtn.moveImageLeftTextCenter(image: UIImage(named: "appleIcon")!, imagePadding: 20, renderingMode: .alwaysOriginal, alignment: .center)
        appleSigninBtn.backgroundColor = .white
        appleSigninBtn.setTitleColor(UIColor.rgba(r: 0, g: 0, b: 0, a: 0.54), for: .normal)
        appleSigninBtn.layer.cornerRadius = 20
        appleSigninBtn.clipsToBounds = true
        appleSigninBtn.layer.borderColor = UIColor.rgba(r: 153, g: 153, b: 153, a: 1.0).cgColor
        appleSigninBtn.layer.borderWidth = 1
        
        signInBtn.setTitle("Continue Without Account", for: .normal)
        signInBtn.setTitleColor(UIColor(hexString: "#782EB1", alpha: 1), for: .normal)
    }
    
    func silentUpdateTermAndConds() {
        AWSAuth.sharedInstance.changeAcceptTC { [weak self] success in
            // Update value to server
            if success {
                self?.pushScreenAfterSignin()
            }
        }
    }
    
    func silentUpdateGuestUserTermAndConds() {
        AWSAuth.sharedInstance.changeAcceptTC { [weak self] success in
            // Update value to server
            if success {
                self?.checkLocationStatusBeforeFetchSession()
            }
        }
    }
    
     func sessionCheck() {
         
         SwiftyBeaver.error("SIGNIN sessionCheck")
         
         self.showLoadingIndicator()
        // TODO: Consider to refactor to make this reusable with the function in SplashVC
        // this will be called after login. We still need to load easybreezy here
        AWSAuth.sharedInstance.fetchCurrentSession { [weak self] (session) in
            guard let self = self else { return }
            if let session = session as? AWSAuthCognitoSession {
                if session.isSignedIn {
                    
                    AWSAuth.sharedInstance.fetchAttributes { [weak self] result in
                        guard let self = self else { return }
                        BugReporting.bugReportingOptions = [.emailFieldHidden]
                        
                        if AWSAuth.sharedInstance.isGuestMode() {
                            if(AWSAuth.sharedInstance.userName.isEmpty)
                            {
                                DispatchQueue.main.async {
                                    self.hideLoadingIndicator()
                                    let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                    let vc: SetUserNameVC = storyboard.instantiateViewController(withIdentifier: "SetUserNameVC")  as! SetUserNameVC
                                    vc.fromAccount = true
                                    self.navigationController?.pushViewController(vc, animated: true)

                                }
                            } else {
                                DispatchQueue.main.async {
                                    self.hideLoadingIndicator()
                                    self.showWelcomeBack()
                                }
                                return
                            }
                        }else {
                            if self.isSignUp {
                                //  If create account user already see T&C then just hit api to update term and condition
                                self.silentUpdateTermAndConds()
                            } else {
                                self.checkAcceptedTCInSignin()
                            }
                        }
                        
                    } onFailure: { [weak self] error in
                        guard let self = self else { return }
                        BugReporting.bugReportingOptions = [.none]
                        self.inLoginPage()
                    }
                    
                } else {
                    BugReporting.bugReportingOptions = [.none]
                    self.inLoginPage()
                }
            }
        } onFailure: { [weak self] (error) in
            guard let self = self else { return }
            self.inLoginPage()
            BugReporting.bugReportingOptions = [.none]
            print(error.localizedDescription)
        }
    }
    
    func inLoginPage() {
        DispatchQueue.main.async {
            self.hideLoadingIndicator()
        }
        AWSAuth.sharedInstance.signOut()
    }
    
    func locationAuthorization(status:CLAuthorizationStatus){
        
        //Swift
        if status == .authorizedAlways || status == .authorizedWhenInUse {
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Onboarding.UserClick.location_permission_allow, screenName: ParameterName.Onboarding.screen_view)
        } else {
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Onboarding.UserClick.location_permission_deny, screenName: ParameterName.Onboarding.screen_view)
        }
        
        switch status {
        case .notDetermined:
            SwiftyBeaver.error("SIGNIN notDetermined")
            break
        case .authorizedAlways:
            self.sessionCheck()
        case .authorizedWhenInUse:
            self.sessionCheck()
        case .restricted:
            SwiftyBeaver.error("SIGNIN restricted")
            break
        case .denied:
            SwiftyBeaver.error("SIGNIN denied")
            break
        @unknown default:
            SwiftyBeaver.error("SIGNIN unknown")
            break
        }
    }
    
    
    func setUserPrefEmpty() {

        SwiftyBeaver.error("SIGNIN setUserPrefEmpty")
        triggerResetAmenities { [weak self] _ in
            guard let self = self else { return }
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                self.hideLoadingIndicator()

                //If onBoarding state is not completed then open OnBoaridngRN screen
//                if(AWSAuth.sharedInstance.onboardingState == nil || AWSAuth.sharedInstance.onboardingState == UserOnboardingState.ONBOARDING_STEP_1){
//                    self.startRNScreen()
//                } else {
                    SwiftyBeaver.error("SIGNIN goToLandingVC")
                    self.goToLandingVC()
//                }
            }

        }
    }
    
    func startRNScreen(){
        
        let state = AWSAuth.sharedInstance.onboardingState == nil ? UserOnboardingState.ONBOARDING_INIT : UserOnboardingState.ONBOARDING_STEP_1
        let params = [ // Object data to be used by that screen
            "onboardingState":state,
            "baseURL": appDelegate().backendURL,
            "idToken":AWSAuth.sharedInstance.idToken,
            "deviceos":parameters!["deviceos"],
            "deviceosversion":parameters!["deviceosversion"],
            "devicemodel":parameters!["devicemodel"],
            "appversion":parameters!["appversion"],
        ]
        // let stateScreen = AWSAuth.sharedInstance.onboardingState == nil ? RNScreeNames.FIRST_APP_TUTORIAL : RNScreeNames.USER_PREFERENCE
        
        self.goToRNScreen(toScreen: RNScreeNames.FIRST_APP_TUTORIAL,navigationParams: params as [String:Any])
    }
    
    @IBAction func fbClickAction(_ sender: Any) {
        
        AWSAuth.sharedInstance.forceSignOutIfNeed(false)
        
        ReachabilityManager.shared.isReachable(success: { [weak self] in
            guard let self = self else { return }
            
            if(self.isSignUp == true) {
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.OnBoardingCreateAccount.UserClick.createAccountFacebook, screenName: ParameterName.OnBoardingCreateAccount.screen_view)
            }else {
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.OnBoardingSignIn.UserClick.signinFacebook, screenName: ParameterName.OnBoardingSignIn.screen_view)
            }
            
//            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Onboarding.UserClick.facebook, screenName: ParameterName.Onboarding.screen_view)

            AWSAuth.sharedInstance.faceBookSignIn(presentAnchor: self.view.window!) { [weak self] (result) in
                guard let self = self else { return }
                DispatchQueue.main.async {

                    let status = LocationManager.shared.getStatus()
                    if(status != .notDetermined)
                    {
                        self.locationAuthorization(status: status)
                    }
                    else
                    {
                        LocationManager.shared.requestLocationAuthorization()
                        LocationManager.shared.requestLocationAuthorizationCallback = { [weak self] status in
                            guard let self = self else { return }
                            self.locationAuthorization(status: status)
                        }
                    }
                }
            } onFailure: { [weak self] (error) in
                guard let self = self else { return }
                if (error as! String == "Already SignIn") {
                    DispatchQueue.main.async {
                        self.sessionCheck()
                    }
                }
                
            }
        
        
        }, failure: { [weak self] in
            guard let self = self else { return }
            self.popupAlert(title: Constants.noInternetTitle, message: Constants.noInternetMessage, actionTitles: [Constants.gotIt], actions:[{ [weak self] action1 in
                guard let self = self else { return }
                self.dismissAnyAlertControllerIfPresent()
                
                
            },{ action2 in
                
                //We don't need to handle this action
            }, nil])
        })
    }
    
    
    @IBAction func googleClickAction(_ sender: Any) {
        
        AWSAuth.sharedInstance.forceSignOutIfNeed(false)
        
        SwiftyBeaver.error("SIGNIN GOOGLE")
        ReachabilityManager.shared.isReachable(success: { [weak self] in
            guard let self = self else { return }
            if(self.isSignUp == true) {
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.OnBoardingCreateAccount.UserClick.createAccountGoogle, screenName: ParameterName.OnBoardingCreateAccount.screen_view)
            }else {
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.OnBoardingSignIn.UserClick.signinGoogle, screenName: ParameterName.OnBoardingSignIn.screen_view)
            }

            SwiftyBeaver.error("SIGNIN googleSignIn")
            AWSAuth.sharedInstance.googleSignIn(presentAnchor: self.view.window!) { [weak self] (result) in
                
                SwiftyBeaver.error("SIGNIN googleSignIn callback")
                
                guard let self = self else { return }
                DispatchQueue.main.async {

                    let status = LocationManager.shared.getStatus()
                    if(status != .notDetermined)
                    {
                        self.locationAuthorization(status: status)
                    }
                    else
                    {
                        LocationManager.shared.requestLocationAuthorization()
                        LocationManager.shared.requestLocationAuthorizationCallback = { [weak self] status in
                            guard let self = self else { return }
                            self.locationAuthorization(status: status)
                        }
                    }
                }
            } onFailure: { [weak self] (error) in
                guard let self = self else { return }
                
                if(error as! String == "Already SignIn")
                {
                    SwiftyBeaver.error("SIGNIN googleSignIn user already signin")
                    DispatchQueue.main.async {
                        self.sessionCheck()
                    }
                } else {
                    SwiftyBeaver.error("SIGNIN googleSignIn failure")
                }

            }
        }, failure: { [weak self] in
            guard let self = self else { return }
            self.popupAlert(title: Constants.noInternetTitle, message: Constants.noInternetMessage, actionTitles: [Constants.gotIt], actions:[{ [weak self] action1 in
                guard let self = self else { return }
                self.dismissAnyAlertControllerIfPresent()
            },{action2 in
                //We don't need this action
                
            }, nil])
        })
        
    }
    
    
    @IBAction func appleClickAction(_ sender: Any) {
        
        AWSAuth.sharedInstance.forceSignOutIfNeed(false)
        
        ReachabilityManager.shared.isReachable(success: { [weak self] in
            guard let self = self else { return }
            if(self.isSignUp == true) {
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.OnBoardingCreateAccount.UserClick.createAccountApple, screenName: ParameterName.OnBoardingCreateAccount.screen_view)
            }else {
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.OnBoardingSignIn.UserClick.signinApple, screenName: ParameterName.OnBoardingSignIn.screen_view)
            }
//            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Onboarding.UserClick.apple, screenName: ParameterName.Onboarding.screen_view)

            AWSAuth.sharedInstance.appleSignIn(presentAnchor: self.view.window!) { [weak self] (result) in
                guard let self = self else { return }
                DispatchQueue.main.async {
                    
                    let status = LocationManager.shared.getStatus()
                    if(status != .notDetermined)
                    {
                        self.locationAuthorization(status: status)
                    }
                    else
                    {
                        LocationManager.shared.requestLocationAuthorization()
                        LocationManager.shared.requestLocationAuthorizationCallback = { [weak self] status in
                            guard let self = self else { return }
                            self.locationAuthorization(status: status)
                        }
                    }
                }
            } onFailure: { [weak self] (error) in
                guard let self = self else { return }
                if(error as! String == "Already SignIn")
                {
                    DispatchQueue.main.async {
                        self.sessionCheck()
                    }
                }
                
            }
        }, failure: { [weak self] in
            guard let self = self else { return }
            self.popupAlert(title: Constants.noInternetTitle, message: Constants.noInternetMessage, actionTitles: [Constants.gotIt], actions:[{ [weak self] action1 in
                guard let self = self else { return }
                self.dismissAnyAlertControllerIfPresent()
            },{action2 in
                
                //We don't need to handle this action
                
            }, nil])
        })
    }
    
    
    @IBAction func numberClickAction(_ sender: Any) {
        
        AWSAuth.sharedInstance.forceSignOutIfNeed(false)
                
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.OnBoardingSignIn.UserClick.signinMobile, screenName: ParameterName.OnBoardingSignIn.screen_view)
        
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc: UIViewController = storyboard.instantiateViewController(withIdentifier: "SignInMobilePhoneViewController") as UIViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func signInClickAction(_ sender: Any) {
        AWSAuth.sharedInstance.forceSignOutIfNeed(true)
        
        if isSignUp {
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.OnBoardingCreateAccount.UserClick.createAccountContinueWoAcc, screenName: ParameterName.OnBoardingCreateAccount.screen_view)
        } else {
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.OnBoardingSignIn.UserClick.signInContinueWoAcc, screenName: ParameterName.OnBoardingSignIn.screen_view)
        }
        
        confirmLoginAsGuestUser()
    }
    
    @IBAction func backAction(_ sender: Any) {
        if isSignUp {
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.OnBoardingCreateAccount.UserClick.createAccountBack, screenName: ParameterName.OnBoardingCreateAccount.screen_view)
        } else {
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.OnBoardingSignIn.UserClick.signinBack, screenName: ParameterName.OnBoardingSignIn.screen_view)
        }        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    private func signOutAndPopToRoot() {
        
        guard let landingVC = self.navigationController?.viewControllers.first(where: { vc in
            return vc is LandingViewController
        }) else { return }
        self.navigationController?.popToViewController(landingVC, animated: true)
        AWSAuth.sharedInstance.signOut()
    }
    
    func  customizeFontStyle(string: String, font: UIFont) -> NSMutableAttributedString {
        return NSMutableAttributedString(string: string, attributes:
                                            [NSAttributedString.Key.font : font ])
    }
    
    func getSuccessText() -> NSAttributedString {
        
        let hiStr = "Welcome back,\n"
        
        let userNameStr = AWSAuth.sharedInstance.userName
        
        let attributedString = customizeFontStyle(string: hiStr, font:  UIFont(name: fontFamilySFPro.Regular, size: 26)!)
        
        attributedString.append(customizeFontStyle(string: userNameStr, font:  UIFont(name: fontFamilySFPro.MaxBold, size: 26)!))
        
        
        return attributedString
    }
    
    private func showWelcomeBack() {
        if (self.navigationController?.viewControllers.last is WebVC) {
            self.navigationController?.popViewController(animated: false)
        }
        
        AnalyticsManager.shared.logOpenPopupEvent(popupName: ParameterName.SignIn.UserPopup.welcome, screenName: ParameterName.SignIn.screen_view)
        
        childViewController = SuccessViewController()
        childViewController?.delegate = self
        childViewController?.goToLanding = true
        childViewController?.successImage = Images.accountCreatedImage
        childViewController?.successText = self.getSuccessText()
        if let vc = self.childViewController {
            self.addChildViewControllerWithView(childViewController: vc as UIViewController,toView: self.view)
        }
    }
    
    private func pushScreenAfterSignin() {
        DispatchQueue.main.async {
            
            if(!AWSAuth.sharedInstance.userName.isEmpty)
            {
                self.showWelcomeBack()
            }
            else
            {
                let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let vc: SetUserNameVC = storyboard.instantiateViewController(withIdentifier: "SetUserNameVC")  as! SetUserNameVC
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    
    func checkAcceptedTCInSignin() {
        AWSAuth.sharedInstance.getAcceptTCStatus { [weak self] isSuccess in
            guard let self = self else { return }
            DispatchQueue.main.async {
                if (isSuccess && !AWSAuth.sharedInstance.acceptTC) {
                    let storyboard: UIStoryboard = UIStoryboard(name: "Web", bundle: nil)
                    let vc: WebVC = storyboard.instantiateViewController(withIdentifier: "WebVC") as! WebVC
                    vc.isAccountCreation = true
                    vc.urlString = Configuration.termsOfUseURL
                    vc.delegate = self
                    vc.type = .onlyShow
                    self.navigationController?.pushViewController(vc, animated: true)
                    return
                }
                
                if AWSAuth.sharedInstance.isGuestMode() {
                    self.checkLocationStatusBeforeFetchSession()
                }else {
                    self.pushScreenAfterSignin()
                }
            }
        }
    }
    
    /*
    func getCurrentSession(){
        self.showLoadingIndicator()
        AWSAuth.sharedInstance.fetchCurrentSession { [weak self] (result) in
            guard let self = self else { return }
            AWSAuth.sharedInstance.fetchAttributes { [weak self] result in
                guard let self = self else { return }
                self.checkAcceptedTCInSignin()
                
            } onFailure: { [weak self] error in
                guard let self = self else { return }
                DispatchQueue.main.async {
                    self.goToLandingVC()
                }
            }

        } onFailure: { [weak self] (error) in
            guard let self = self else { return }
            DispatchQueue.main.async {
                self.goToLandingVC()
            }
        }
    }*/
}


extension NewSigninGuestModeViewController: DismissDelegate {
    
    func dismissViewController(controller: UIViewController) {
        
        controller.dismiss(animated: false) { [weak self] in
            guard let self = self else { return }
            if(self.isSignUp == true)
            {
                self.isSignUp = false
                let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let vc: SetUserNameVC = storyboard.instantiateViewController(withIdentifier: "SetUserNameVC")  as! SetUserNameVC
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else
            {
                let status = LocationManager.shared.getStatus()
                if(status != .notDetermined)
                {
                    self.locationAuthorization(status: status)
                }
                else
                {
                    LocationManager.shared.requestLocationAuthorization()
                    LocationManager.shared.requestLocationAuthorizationCallback = { [weak self] status in
                        guard let self = self else { return }
                        self.locationAuthorization(status: status)
                    }
                }
            }
            
        }
    
    }
    
}

extension NewSigninGuestModeViewController:SuccessScreenControllerDelegate{
    func SuccessScreenControllerDismiss() {
        
        self.childViewController?.view.removeFromSuperview()        
        
        if AWSAuth.sharedInstance.isGuestMode() {
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.OnBoardingGuest.UserClick.guestSuccessNext, screenName: ParameterName.OnBoardingGuest.screen_view)
        }else {
            if(self.isSignUp == true) {
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.OnBoardingCreateAccount.UserClick.createAccountSuccessNext, screenName: ParameterName.OnBoardingCreateAccount.screen_view)
            }else {
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.OnBoardingSignIn.UserClick.signinSuccessNext, screenName: ParameterName.OnBoardingSignIn.screen_view)
            }
        }
//        AnalyticsManager.shared.logClosePopupEvent(popupName: ParameterName.SignIn.UserPopup.welcome, screenName: ParameterName.SignIn.screen_view)

        DispatchQueue.global(qos: .background).async { [weak self] in
            guard let self = self else { return }
            
            self.setUserPrefEmpty()
        }
    }
}

extension NewSigninGuestModeViewController: WebVCDelegate {
    func didAccepted() {
        
        if AWSAuth.sharedInstance.isGuestMode() {
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.OnBoardingGuest.UserClick.guestTnCAgree, screenName: ParameterName.OnBoardingGuest.screen_view)
        }else {
            if(self.isSignUp == true) {
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.OnBoardingCreateAccount.UserClick.tncAgree, screenName: ParameterName.OnBoardingCreateAccount.screen_view)
            }else {
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.OnBoardingCreateAccount.UserClick.tncAgree, screenName: ParameterName.OnBoardingSignIn.screen_view)
            }
        }
        
        self.pushScreenAfterSignin()
        
        AWSAuth.sharedInstance.changeAcceptTC { _ in
            // Update value to server
        }
    }
    
    func didDeclined() {
        
        if AWSAuth.sharedInstance.isGuestMode() {
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.OnBoardingGuest.UserClick.guestTnCDeline, screenName: ParameterName.OnBoardingGuest.screen_view)
        }else {
            if(self.isSignUp == true) {
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.OnBoardingCreateAccount.UserClick.tncDeline, screenName: ParameterName.OnBoardingCreateAccount.screen_view)
            }else {
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.OnBoardingCreateAccount.UserClick.tncDeline, screenName: ParameterName.OnBoardingSignIn.screen_view)
            }
        }
        
        self.signOutAndPopToRoot()
    }
    
    func didBacked() {
        if AWSAuth.sharedInstance.isGuestMode() {
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.OnBoardingGuest.UserClick.guestTnCBack, screenName: ParameterName.OnBoardingGuest.screen_view)
        }else {
            if(self.isSignUp == true) {
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.OnBoardingCreateAccount.UserClick.tncBack, screenName: ParameterName.OnBoardingCreateAccount.screen_view)
            }else {
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.OnBoardingCreateAccount.UserClick.tncBack, screenName: ParameterName.OnBoardingSignIn.screen_view)
            }
        }
    }
}

//  Extension for guest mode
extension NewSigninGuestModeViewController {
    
    func confirmLoginAsGuestUser () {
        
        if AWSAuth.sharedInstance.guestUserExisting() {
            self.proceedLoginAsGuest()
        } else {
            let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            if let confirmVC = storyboard.instantiateViewController(withIdentifier: "ConfirmSigninAsGuestVC") as? ConfirmSigninAsGuestVC {
                
                self.addChildViewControllerWithView(childViewController: confirmVC)
                
                confirmVC.proceedAction = { [weak self] in
                    self?.proceedLoginAsGuest()
                }
            }
        }
    }
    
    func proceedLoginAsGuest() {
        
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.SignInCreate.UserClick.guest_mode_warning_proceed, screenName: ParameterName.SignInCreate.screen_view)
        
        self.showLoadingIndicator()
        AWSAuth.sharedInstance.signInAsGuestUser { [weak self] success in
            DispatchQueue.main.async {
                if success {
                    Prefs.shared.setValue(true, forkey: .isGuestMode)
                    if self?.isSignUp == true {
                        self?.silentUpdateGuestUserTermAndConds()
                    }else {
                        self?.checkAcceptedTCInSignin()
                    }
                }
                self?.hideLoadingIndicator()
            }
        }
    }
    
    //  Always need to check location permission before navigate to home screen
    private func checkLocationStatusBeforeFetchSession() {
        
        let status = LocationManager.shared.getStatus()
        if(status != .notDetermined)
        {
            self.locationAuthorization(status: status)
        }
        else
        {
            LocationManager.shared.requestLocationAuthorization()
            LocationManager.shared.requestLocationAuthorizationCallback = { [weak self] status in
                guard let self = self else { return }
                self.locationAuthorization(status: status)
            }
        }
    }
}

//
//  SetUserNameVC.swift
//  Breeze
//
//  Created by VishnuKanth on 01/08/21.
//

import UIKit
import CoreLocation
import Amplify
import AWSCognitoAuthPlugin
import SnapKit

class SetUserNameVC: UIViewController,UITextFieldDelegate {
    
//    @IBOutlet weak var userNameField: DesignableTextField!
    @IBOutlet weak var userNameField: UITextField!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var alertLbl: UILabel!
    
    var textCounterLbl: UILabel!
    var textCounterView: UIView!
    var isSignUp = true
    var fromAccount = false
    var restrictedNames: [String] = []
    var childViewController: SuccessViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUI()
        setupDarkLightAppearance(forceLightMode: true)
        
        ReachabilityManager.shared.isReachable(success: { [weak self] in
           //   Fetch restricted name
            self?.fetchRestrictedNames(completion: nil)
        }, failure: {
            //  Nothing to be done
        })
    }
    
    
    private func fetchRestrictedNames(completion: ((Bool) -> Void)? = nil) {
        RestrictedNameService().getRestrictedNames {[weak self] result in
            switch result {
            case .success(let model):
                if !model.data.isEmpty {
                    self?.restrictedNames.append(contentsOf: model.data)
                }
                completion?(true)
            case .failure(_):
                completion?(false)
            }
        }
    }
    
    private func isNameRestricted(_ username: String) -> Bool {
        for name in restrictedNames {
            if name.caseInsensitiveCompare(username) == .orderedSame {
                return true
            }
        }
        return false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.userNameField.becomeFirstResponder()
    }
    
    func setUpUI(){
        
        textCounterView = UIView(frame: CGRect(x: 0, y: 0, width: 90, height: 25))
//        textCounterView.translatesAutoresizingMaskIntoConstraints = false
        self.textCounterLbl = UILabel(frame: CGRect(x: 0, y: 0, width: 90, height: 25))
        self.textCounterLbl.text = "0/14"
        self.textCounterLbl.textColor = UIColor.rgba(r: 119, g: 129, b: 136, a: 1.0)
        self.textCounterLbl.font =  UIFont(name:"Quicksand",size:14)
        textCounterView.addSubview(self.textCounterLbl)
//        self.textCounterLbl.snp.makeConstraints { make in
//            make.top.left.bottom.right.equalToSuperview()
//            make.width.equalTo(90)
//            make.height.equalTo(25)
//        }
        userNameField.rightView = textCounterView
        userNameField.rightViewMode = .always
        self.textCounterLbl.textAlignment = .center
        
        
        if let image = UIImage(systemName: "person")?.applyingSymbolConfiguration(.unspecified)?.withRenderingMode(.automatic) {
            let imageView = UIImageView (image: image)
            imageView.frame = CGRect( x:18, y:0, width: image.size.width, height: image.size.height)
            imageView.contentMode = .scaleAspectFit
            let view = UIView(frame: CGRect(x:0, y:0, width: 45, height: 20))
            view.addSubview(imageView)
            
            userNameField.leftView = view
            userNameField.leftViewMode = .always
        }
        
        userNameField.layer.cornerRadius = 24
        userNameField.clipsToBounds = true
        userNameField.layer.borderColor = UIColor.brandPurpleColor.cgColor
        userNameField.layer.borderWidth = 1
        userNameField.delegate = self
        userNameField.tag = 0
        userNameField.enablesReturnKeyAutomatically = true
        userNameField.returnKeyType = .done
        userNameField.textColor = .black
                
    }
    
    func  customizeFontStyle(string: String, font: UIFont) -> NSMutableAttributedString {
        return NSMutableAttributedString(string: string, attributes:
                                            [NSAttributedString.Key.font : font ])
    }
    
    func getSuccessText() -> NSAttributedString{
        
        let hiStr = "\(Constants.hi)\n"
        var userNameStr = ""
        if self.isSignUp {
            
            userNameStr = self.userNameField.text!
            
        }
        let successStr = Constants.accountCreatedSuccess
//        if !self.isSignUp {
//            hiStr = Constants.goodJob
//            successStr = Constants.passwordChangeSuccess
//        }
        
        let attributedString = customizeFontStyle(string: hiStr, font: UIFont(name: fontFamilySFPro.Regular, size: 26.0)!)
        
        attributedString.append(customizeFontStyle(string: userNameStr, font: UIFont(name: fontFamilySFPro.Bold, size: 26.0)!))
        
        if !AWSAuth.sharedInstance.isGuestMode() {
            attributedString.append(customizeFontStyle(string: successStr, font: .systemFont(ofSize: 18)))
        }
        
        return attributedString
    }
    
    func showRestrictedWarning() {
        self.alertLbl.text = Constants.usernameIsRestricted
        self.alertLbl.isHidden = false
        self.userNameField.layer.borderColor = UIColor.red.cgColor
    }
    
    func setUserName(){
                
        setUNAfterSignIn()

    }
    
    func setUNAfterSignIn(){
        
        AnalyticsManager.shared.logOpenPopupEvent(popupName: ParameterName.SetUserName.UserPopup.signup_success, screenName: ParameterName.SetUserName.screen_view)
        
        //  Since version 5.0 will remove set username attribute
        Prefs.shared.setValueWithSub(AWSAuth.sharedInstance.cognitoId, key: .username, value: userNameField.text!)
        AWSAuth.sharedInstance.userName = userNameField.text!
        
        childViewController = SuccessViewController()
        childViewController?.delegate = self
        childViewController?.successText = self.getSuccessText()
        childViewController?.goToLanding = true
        childViewController?.successView.okayBtn.setTitle("Let's go!", for: .normal)
        childViewController?.successImage = Images.accountCreatedImage
        if let vc = self.childViewController {
            self.addChildViewControllerWithView(childViewController: vc as UIViewController,toView: self.view)
        }
        self.view.endEditing(true)
    }
    
    // MARK: - IBActions
    @IBAction func backAction(){
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.SetUserName.UserClick.back, screenName: ParameterName.SetUserName.screen_view)
    }
    

    // MARK: - UITextFieldDelegate Methods
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if(textField == userNameField)
        {
            AnalyticsManager.shared.logEditEvent(eventValue: ParameterName.name, screenName: ParameterName.createAccount_step1)
        }
        textField.layer.borderColor = UIColor.brandPurpleColor.cgColor
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == userNameField {
            
            if !(validateUsername(enteredUsername: userNameField.text!)) {
                self.alertLbl.text = Constants.usernameInvalid
                //self.alertImg.isHidden = false
                self.alertLbl.isHidden = false
                self.userNameField.layer.borderColor = UIColor.red.cgColor
                return false
            }
            else
            {
                
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.OnBoardingCreateAccount.UserClick.createAccountUsernameInput, screenName: ParameterName.OnBoardingCreateAccount.screen_view)
//                AnalyticsManager.shared.logEditEvent(eventValue: ParameterName.SetUserName.UserEdit.username_input, screenName: ParameterName.SetUserName.screen_view)
                
                //  Need validate restricted name before save it
                let name = userNameField.text!
                if !restrictedNames.isEmpty {
                    if isNameRestricted(name) {
                        self.showRestrictedWarning()
                        return false
                    }else {
                        self.setUserName()
                    }
                } else {
                    self.setUserName()
                }
            }
        }
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        // get the current text, or use an empty string if that failed
        let currentText = textField.text ?? ""
        
        // attempt to read the range they are trying to change, or exit if we can't
        guard let stringRange = Range(range, in: currentText) else { return false }
        
        // add their new text to the existing text
        let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
        
       
        // make sure the result is under 16 characters
        if textField == self.userNameField{
            let str = String(format:"%d/14", updatedText.count as! CVarArg)
            self.textCounterLbl.text = str
            if updatedText.count >= 14{
                self.textCounterLbl.text = "14/14"
            }

            return updatedText.count <= 14
        }else{
            return true
        }
    }
    
    //MARK: - Authorization Methods

    func locationAuthorization(status:CLAuthorizationStatus){
        
        //Swift
        switch status {
        case .notDetermined:
            break
        case .authorizedAlways:
            self.signIn()
        case .authorizedWhenInUse:
            self.signIn()
        case .restricted:
            break
        case .denied:
            break
        @unknown default:
            break
        }
    }

    func signIn(){
        
        self.getCurrentSession()
        
    }
    
    func setUserPrefEmpty() {
        
        triggerResetAmenities { [weak self] _ in
            
            DispatchQueue.main.async {
                guard let self = self else { return }

                //If onBoarding state is not completed then open OnBoaridngRN screen
//                if(AWSAuth.sharedInstance.onboardingState == nil || AWSAuth.sharedInstance.onboardingState == UserOnboardingState.ONBOARDING_STEP_1){
//                    self.startRNScreen()
//                } else {
                    self.goToLandingVC()
//                }
            }

        }
    }

    func getCurrentSession(){
        
        AWSAuth.sharedInstance.fetchCurrentSession { [weak self] (session) in
            
            guard let self = self else { return }
            
            if let session = session as? AWSAuthCognitoSession {
                
                if session.isSignedIn {
                    AWSAuth.sharedInstance.fetchAttributes { [weak self] result in
                        guard let self = self else { return }
                        self.setUserPrefEmpty()
                    } onFailure: { [weak self] error in
                        guard let self = self else { return }
                        self.setUserPrefEmpty()
                    }
                    
                } else {
                    DispatchQueue.main.async {
                        //If onBoarding state is not completed then open OnBoaridngRN screen
//                        if(AWSAuth.sharedInstance.onboardingState == nil || AWSAuth.sharedInstance.onboardingState == UserOnboardingState.ONBOARDING_STEP_1){
//                            self.startRNScreen()
//                        } else{
                            self.goToLandingVC()
//                        }
                    }
                }
            }
        } onFailure: { [weak self] (error) in
            DispatchQueue.main.async {
                guard let self = self else { return }
                //If onBoarding state is not completed then open OnBoaridngRN screen
                if(AWSAuth.sharedInstance.onboardingState == nil || AWSAuth.sharedInstance.onboardingState == UserOnboardingState.ONBOARDING_STEP_1){
                    
//                    self.startRNScreen()
                    // do nothing
                }
                else{
                    self.goToLandingVC()
                }
                
            }
        }
    }
    
    func startRNScreen(){
        let state = AWSAuth.sharedInstance.onboardingState == nil ? UserOnboardingState.ONBOARDING_INIT : UserOnboardingState.ONBOARDING_STEP_1
       let params = [ // Object data to be used by that screen
            "onboardingState":state,
            "baseURL": appDelegate().backendURL,
            "idToken":AWSAuth.sharedInstance.idToken,
            "deviceos":parameters!["deviceos"],
            "deviceosversion":parameters!["deviceosversion"],
            "devicemodel":parameters!["devicemodel"],
            "appversion":parameters!["appversion"],
       ]
        //let stateScreen = AWSAuth.sharedInstance.onboardingState == nil ? RNScreeNames.FIRST_APP_TUTORIAL : RNScreeNames.USER_PREFERENCE
        
        self.goToRNScreen(toScreen: RNScreeNames.FIRST_APP_TUTORIAL,navigationParams: params as [String:Any])
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SetUserNameVC:SuccessScreenControllerDelegate {
    func SuccessScreenControllerDismiss() {
        
//        self.childViewController?.view.removeFromSuperview()
                
        if(isSignUp)
        {
            AnalyticsManager.shared.logClosePopupEvent(popupName: ParameterName.SetUserName.UserPopup.signup_success, screenName: ParameterName.SetUserName.screen_view)

//            AnalyticsManager.shared.logClosePopupEvent(popupName: ParameterName.creatAccount_success, screenName: ParameterName.createAccount_step2)
            let status = LocationManager.shared.getStatus()
            if(status != .notDetermined)
            {
                self.locationAuthorization(status: status)
            }
            else
            {
                LocationManager.shared.requestLocationAuthorization()
                LocationManager.shared.requestLocationAuthorizationCallback = { status in
                    
                    self.locationAuthorization(status: status)
                }
            }
        }
        else
        {
            if(fromAccount == true)
            {
                let status = LocationManager.shared.getStatus()
                if(status != .notDetermined)
                {
                    self.locationAuthorization(status: status)
                }
                else
                {
                    LocationManager.shared.requestLocationAuthorization()
                    LocationManager.shared.requestLocationAuthorizationCallback = { status in
                        
                        self.locationAuthorization(status: status)
                    }
                }
                //self.popToViewController()
            }
            else
            {
                
//                AnalyticsManager.shared.logClosePopupEvent(popupName: ParameterName.FPSuccess, screenName: ParameterName.screeNameFP_step3)
                
                goToLoginVC()
            }
            
        }
    }
    
    
}

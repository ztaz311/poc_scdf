//
//  PetrolView.swift
//  Breeze
//
//  Created by VishnuKanth on 31/12/21.
//

import Foundation
import UIKit

@IBDesignable
class PetrolView: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var closeBtn: UIButton!
    
    var closePretrolView: ((_ cancel:Bool) -> Void)?
    var selectedItem: ((_ itemName:String,_ isSelected:Bool,_ element_id:String) -> Void)?
    var petrolKioskSubItems = [Sub_items]()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        contentView.layer.cornerRadius = 10
        collectionView.layer.cornerRadius = 16
    }
    
    private func collectionViewLayout() -> UICollectionViewLayout {
        
        let layout = UICollectionViewFlowLayout()
        layout.sectionHeadersPinToVisibleBounds = true
        // Important: if direction is horizontal use minimumItemSpacing instead.
        layout.scrollDirection = .vertical
        
        let itemHeight: CGFloat = 28
        let minCellWidth :CGFloat = 80.0
        let minItemSpacing: CGFloat = 10
        let containerWidth: CGFloat = self.collectionView.frame.size.width
        let maxCellCountPerRow: CGFloat =  floor((containerWidth - minItemSpacing) / (minCellWidth+minItemSpacing ))
        
        let itemWidth: CGFloat = floor( ((containerWidth - (2 * minItemSpacing) - (maxCellCountPerRow-1) * minItemSpacing) / maxCellCountPerRow  ) )
        // Calculate the remaining space after substracting calculating cellWidth (Divide by 2 because of left and right insets)
        let inset = max(minItemSpacing, floor( (containerWidth - (maxCellCountPerRow*itemWidth) - (maxCellCountPerRow-1)*minItemSpacing) / 2 ) )

        
        layout.itemSize = CGSize(width: itemWidth, height: itemHeight)
        layout.minimumInteritemSpacing = min(minItemSpacing,inset)
        layout.minimumLineSpacing = minItemSpacing
        layout.sectionInset = UIEdgeInsets(top: minItemSpacing, left: inset, bottom: minItemSpacing, right: inset)

        
        return layout
    }
    
    private func commonInit() {
        let bundle = Bundle(for: type(of: self))
        bundle.loadNibNamed("PetrolView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        contentView.backgroundColor = UIColor(named:"rpRouteStopBackgroundColor")!
        collectionView.backgroundColor = UIColor(named:"rpRouteStopCellBackgroundColor")!
        closeBtn.setTitleColor(UIColor(named:"ratingBtnColor")!, for: .normal)
        initCollectionView()
    }
    
    private func initCollectionView() {
        let nib = UINib(nibName: "PetrolCell", bundle: nil)
        collectionView.register(nib, forCellWithReuseIdentifier: "PetrolCell")
        collectionView.dataSource = self
        collectionView.delegate = self
      // collectionView.collectionViewLayout = self.collectionViewLayout()
    }
    
    @IBAction @objc func closeBtnClicked(_ sender: Any) {
        
        if let callback = closePretrolView {
            
            callback(true)
        }
    }
    
}

extension PetrolView: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemDisplay = "\(petrolKioskSubItems[indexPath.item].display_text ?? "") (\(petrolKioskSubItems[indexPath.item].count ?? 0))"
        return CGSize(width: itemDisplay.size(withAttributes: [
            NSAttributedString.Key.font: UIFont.rerouteTitleRegularFont()]).width + 30, height: 28)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 20.0, left: 0.0, bottom: 10.0, right: 20.0)
        
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 20.0
    
    }
    
     func collectionView(_ collectionView: UICollectionView,
            layout collectionViewLayout: UICollectionViewLayout,
                         minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
                return 2.0
        }

}

extension PetrolView: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return petrolKioskSubItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PetrolCell", for: indexPath) as? PetrolCell else {
            fatalError("can't dequeue CustomCell")
        }
        cell.item.setTitle("\(petrolKioskSubItems[indexPath.item].display_text ?? "")", for: .normal)
        cell.item.tag = indexPath.item
        cell.item.addTarget(self, action: #selector(itemSelected(_:)), for: .touchUpInside)
        if(petrolKioskSubItems[indexPath.item].is_selected == true){
            
            cell.item.backgroundColor = UIColor(named: "rpRouteStopCellBtnBgColor")!
            
            cell.item.setTitleColor(.white, for: .normal)
            
            cell.item.isSelected = true
            
            
        }
        else{
            
            cell.item.backgroundColor = UIColor(named: "rpRouteStopCellBtnBgUnselectedColor")!
            cell.item.setTitleColor(UIColor(named: "rpRouteStepCellBtnTextColor")!, for: .normal)
        }
        return cell
    }
    
    @objc
    func itemSelected(_ sender:UIButton){
        
        
        if(sender.isSelected){
            
            sender.isSelected = false
            sender.backgroundColor = UIColor(named: "rpRouteStopCellBtnBgUnselectedColor")!
            sender.setTitleColor(UIColor(named: "rpRouteStepCellBtnTextColor")!, for: .normal)
        }
        else{
            sender.isSelected = true
            sender.backgroundColor = UIColor(named: "rpRouteStopCellBtnBgColor")!
            
            sender.setTitleColor(.white, for: .normal)
        }
        
        if let callback = selectedItem {
            
            if let components = sender.titleLabel?.text?.components(separatedBy: " "), !components.isEmpty {
                callback(components[0],sender.isSelected,petrolKioskSubItems[sender.tag].element_id ?? "")
            }
        }
    }
}

extension PetrolView:UICollectionViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PetrolCell", for: indexPath) as! PetrolCell

        cell.item.backgroundColor = UIColor(named: "rpRouteStopCellBtnBgColor")!
        
        cell.item.setTitleColor(.white, for: .normal)
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath){
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PetrolCell", for: indexPath) as! PetrolCell

        cell.item.backgroundColor = UIColor(named: "rpRouteStopCellBtnBgUnselectedColor")!
        cell.item.setTitleColor(UIColor(named: "rpRouteStepCellBtnTextColor")!, for: .normal)
    }
}

//
//  EVChargerView.swift
//  Breeze
//
//  Created by VishnuKanth on 03/01/22.
//

import Foundation
import UIKit
import Kingfisher

@IBDesignable
class EVChargerView: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var closeBtn: UIButton!
    
    var closePretrolView: ((_ cancel:Bool) -> Void)?
    var selectedItem: ((_ itemName:String,_ isSelected:Bool,_ element_id:String) -> Void)?
    var evSubItems = [Sub_items]()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        contentView.layer.cornerRadius = 10
        collectionView.layer.cornerRadius = 16
    }
    
    private func collectionViewLayout() -> UICollectionViewLayout {
        
        let layout = UICollectionViewFlowLayout()
        layout.sectionHeadersPinToVisibleBounds = true
        // Important: if direction is horizontal use minimumItemSpacing instead.
        layout.scrollDirection = .vertical
        
        let itemHeight: CGFloat = 70
        let minCellWidth :CGFloat = 85.0
        let minItemSpacing: CGFloat = 5
        let containerWidth: CGFloat = self.collectionView.frame.size.width
        let maxCellCountPerRow: CGFloat =  floor((containerWidth - minItemSpacing) / (minCellWidth+minItemSpacing ))
        
        let itemWidth: CGFloat = floor( ((containerWidth - (2 * minItemSpacing) - (maxCellCountPerRow-1) * minItemSpacing) / maxCellCountPerRow  ) )
        // Calculate the remaining space after substracting calculating cellWidth (Divide by 2 because of left and right insets)
        let inset = max(minItemSpacing, floor( (containerWidth - (maxCellCountPerRow*itemWidth) - (maxCellCountPerRow-1)*minItemSpacing) / 2 ) )

        
        layout.itemSize = CGSize(width: itemWidth, height: itemHeight)
        layout.minimumInteritemSpacing = min(minItemSpacing,inset)
        layout.minimumLineSpacing = minItemSpacing
        layout.sectionInset = UIEdgeInsets(top: minItemSpacing, left: inset, bottom: minItemSpacing, right: inset)

        
        return layout
    }
    
    private func commonInit() {
        let bundle = Bundle(for: type(of: self))
        bundle.loadNibNamed("EVChargerView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        contentView.backgroundColor = UIColor(named:"rpRouteStopBackgroundColor")!
        collectionView.backgroundColor = UIColor(named:"rpRouteStopCellBackgroundColor")!
        closeBtn.setTitleColor(UIColor(named:"ratingBtnColor")!, for: .normal)
        initCollectionView()
    }
    
    private func initCollectionView() {
        let nib = UINib(nibName: "EVCell", bundle: nil)
        collectionView.register(nib, forCellWithReuseIdentifier: "EVCell")
        collectionView.dataSource = self
        collectionView.delegate = self
        //collectionView.isScrollEnabled = false
        //collectionView.collectionViewLayout = self.collectionViewLayout()
    }
    
    @IBAction @objc func closeBtnClicked(_ sender: Any) {
        
        if let callback = closePretrolView {
            
            callback(true)
        }
    }
    
    private func downloadImage(with urlString : String , imageCompletionHandler: @escaping (UIImage?) -> Void){
        guard let url = URL.init(string: urlString) else {
            return  imageCompletionHandler(nil)
        }
        let resource = ImageResource(downloadURL: url)
        
        KingfisherManager.shared.retrieveImage(with: resource, options: nil, progressBlock: nil) { result in
            switch result {
            case .success(let value):
                imageCompletionHandler(value.image)
            case .failure:
                imageCompletionHandler(nil)
            }
        }
    }
    
}

extension EVChargerView: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return evSubItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EVCell", for: indexPath) as? EVCell else {
            fatalError("can't dequeue CustomCell")
        }
        
    //cell.backgroundColor = .green
       cell.item.setTitle("\(evSubItems[indexPath.item].display_text ?? "")", for: .normal)
        cell.item.tag = indexPath.item
        cell.item.addTarget(self, action: #selector(itemSelected(_:)), for: .touchUpInside)
        cell.evImageType.tag = indexPath.item
        cell.evImageType.isUserInteractionEnabled = true
        let sourcetap = UITapGestureRecognizer(target: self, action: #selector(evImageTapped(_:)))
        sourcetap.numberOfTapsRequired = 1
        sourcetap.view?.tag = indexPath.item
        cell.evImageType.addGestureRecognizer(sourcetap)
        if(evSubItems[indexPath.item].is_selected == true){

            self.downloadImage(with: evSubItems[indexPath.item].activeImageURL ?? "") { image in
                
                if let image = image {
                    cell.evImageType.image = image
                }
                else{
                    
                }
                
            }
            cell.item.backgroundColor = UIColor(named: "rpRouteStopCellBtnBgColor")!

            cell.item.setTitleColor(.white, for: .normal)
            
            cell.item.isSelected = true
        }
        else{

            self.downloadImage(with: evSubItems[indexPath.item].inActiveImageURL ?? "") { image in
                
                if let image = image {
                    cell.evImageType.image = image
                }
                else{
                    
                }
            }
            cell.item.backgroundColor = UIColor(named: "rpRouteStopCellBtnBgUnselectedColor")!
            cell.item.setTitleColor(UIColor(named: "rpRouteStepCellBtnTextColor")!, for: .normal)
        }
        return cell
    }
    
    @objc func evImageTapped(_ sender: UITapGestureRecognizer) {
        if let tag = sender.view?.tag, let evCell = collectionView.cellForItem(at: IndexPath(row: tag, section: 0)) as? EVCell {
            self.itemSelected(evCell.item)
        }
    }
    
    @objc
    func itemSelected(_ sender:UIButton){
        
        var cell:EVCell?
        if let evCell = collectionView.cellForItem(at: IndexPath(row: sender.tag, section: 0)) as? EVCell {
            
            cell = evCell
        }
        if(sender.isSelected){
            
            sender.isSelected = false
            sender.backgroundColor = UIColor(named: "rpRouteStopCellBtnBgUnselectedColor")!
            sender.setTitleColor(UIColor(named: "rpRouteStepCellBtnTextColor")!, for: .normal)
            
            self.downloadImage(with: evSubItems[sender.tag].inActiveImageURL ?? "") { image in
                
                if let image = image {
                    if let cell = cell {
                        cell.evImageType.image = image
                    }
                    
                }
                else{
                    
                }
                
            }
        }
        else{
            sender.isSelected = true
            sender.backgroundColor = UIColor(named: "rpRouteStopCellBtnBgColor")!
            
            sender.setTitleColor(.white, for: .normal)
            
            self.downloadImage(with: evSubItems[sender.tag].activeImageURL ?? "") { image in
                
                if let image = image {
                    if let cell = cell {
                        cell.evImageType.image = image
                    }
                }
                else{
                    
                }
                
            }
        }
        
        if let callback = selectedItem {
            if let components = sender.titleLabel?.text?.components(separatedBy: " "), !components.isEmpty {
                callback(components[0],sender.isSelected,evSubItems[sender.tag].element_id ?? "")
            }
        }
    }
}

extension EVChargerView:UICollectionViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EVCell", for: indexPath) as! EVCell

        cell.item.backgroundColor = UIColor(named: "rpRouteStopCellBtnBgColor")!
        
        cell.item.setTitleColor(.white, for: .normal)
        
        self.downloadImage(with: evSubItems[indexPath.item].activeImageURL ?? "") { image in
            
            if let image = image {
                cell.evImageType.image = image
            }
            else{
                
            }
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath){
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EVCell", for: indexPath) as! EVCell

        cell.item.backgroundColor = UIColor(named: "rpRouteStopCellBtnBgUnselectedColor")!
        cell.item.setTitleColor(UIColor(named: "rpRouteStepCellBtnTextColor")!, for: .normal)
        
        self.downloadImage(with: evSubItems[indexPath.item].inActiveImageURL ?? "") { image in
            
            if let image = image {
                cell.evImageType.image = image
            }
            else{
                
            }
            
        }
    }
}

//extension EVChargerView:UICollectionViewDelegateFlowLayout{
//
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
//        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
//        let cellWidth: CGFloat = flowLayout.itemSize.width
//        let cellHieght: CGFloat = flowLayout.itemSize.height
//        let cellSpacing: CGFloat = flowLayout.minimumInteritemSpacing
//        let cellCount = CGFloat(collectionView.numberOfItems(inSection: section))
//        var collectionWidth = collectionView.frame.size.width
//        var collectionHeight = collectionView.frame.size.height
//        if #available(iOS 11.0, *) {
//            collectionWidth -= collectionView.safeAreaInsets.left + collectionView.safeAreaInsets.right
//            collectionHeight -= collectionView.safeAreaInsets.top + collectionView.safeAreaInsets.bottom
//        }
//        let totalWidth = cellWidth * cellCount + cellSpacing * (cellCount - 1)
//        let totalHieght = cellHieght * cellCount + cellSpacing * (cellCount - 1)
//        if totalWidth <= collectionWidth {
//            let edgeInsetWidth = (collectionWidth - totalWidth) / 2
//
//            print(edgeInsetWidth, edgeInsetWidth)
//            return UIEdgeInsets(top: 40, left: edgeInsetWidth-20, bottom: flowLayout.sectionInset.top, right: edgeInsetWidth-20)
//        } else {
//            let edgeInsetHieght = (collectionHeight - totalHieght) / 2
//            print(edgeInsetHieght, edgeInsetHieght)
//            return UIEdgeInsets(top: edgeInsetHieght, left: flowLayout.sectionInset.top, bottom: edgeInsetHieght, right: flowLayout.sectionInset.top)
//
//        }
//    }
//}


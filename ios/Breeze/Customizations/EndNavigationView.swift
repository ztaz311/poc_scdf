//
//  EndNavigationView.swift
//  Breeze
//
//  A UIView shown when navigation ends in walking navigation or turn by turn navigation
//
//  Created by Zhou Hao on 6/6/22.
//

import UIKit

final class EndNavigationView: UIView {
    
    // MARK: - Property
    // when end button clicked
    var onEndAction: (() -> Void)?
    
    // MARK: - Init
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        setup()
    }

    private func setup() {
        // 1. setup view
        let h: CGFloat = Values.bottomPanelHeight
        let offset = h + UIApplication.bottomSafeAreaHeight
        self.frame = CGRect(x: 0, y:  UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: offset)
        self.roundCorners([.topLeft, .topRight], radius: 20)
        self.shadowToTopOfView()

        self.backgroundColor = UIColor.navBottomBackgroundColor1

        // add end button
        let y = (offset - 48) / 2
        let button = UIButton(frame: CGRect(x: 24, y: y, width: UIScreen.main.bounds.width - 48, height: 48))        
        button.backgroundColor = UIColor(hexString: "#E82370", alpha: 1)
        button.setTitle("End", for: .normal)
        button.titleLabel?.font = UIFont(name: fontFamilySFPro.Medium, size: 20.0)!
        button.setTitleColor(.white, for: .normal)
        button.layer.cornerRadius = 24
        button.layer.masksToBounds = true
        button.addTarget(self, action: #selector(onEndButtonClicked), for: .touchUpInside)
        self.addSubview(button)
    }
    
    @objc private func onEndButtonClicked() {
        onEndAction?()
    }
    
    // MARK: - Public methods
    func show(animated: Bool) {
        let h: CGFloat = Values.bottomPanelHeight
        let offset = h + UIApplication.bottomSafeAreaHeight
        
        // animation
        UIView.animate(withDuration: Values.standardAnimationDuration, delay: 0, options: .curveEaseIn) {
//            self.layoutIfNeeded()
            self.center.y -= offset
        }        
    }
}


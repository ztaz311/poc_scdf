//
//  TapView.swift
//  Breeze
//
//  Created by Tuyen, Le Xuan  on 14/07/2022.
//

import Foundation
import UIKit


typealias TapGestureHandle = () -> Void

class TapView: UIView {
     
    var tapHandle: TapGestureHandle?
    
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        tapHandle?()
        return false
    }
}

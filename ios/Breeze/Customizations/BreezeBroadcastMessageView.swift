//
//  BreezeNotificationView.swift
//  Breeze
//
//  Customizable notification view used in mobile
//
//  Created by Zhou Hao on 7/4/22.
//

import UIKit
import RxSwift

class BreezeBroadcastMessageView: UIView {

    // MARK: - Constants
    private let topOffset: CGFloat = 16
    private let bottomOffset: CGFloat = 16
    private let gap: CGFloat = 5   // between title and subtitle

    // MARK: - Properties
    private var autoDismissDuration = 0
    private var timer: Timer?
    private var preferredWidth: CGFloat = 320
    private var onCompletion: (() -> Void)? // after showing notification
    private var onDismiss: (() -> Void)?    // after notification dismissed
    
    private(set) lazy var containerView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 16
        view.backgroundColor = .white
        addSubview(view)
        return view
    }()
    
    private lazy var icon: UIImageView = {
        let imageView = UIImageView(image: UIImage())
        containerView.addSubview(imageView)
        return imageView
    }()

    private lazy var lblTitle: UILabel  =  {
        let label = UILabel()
        label.font = UIFont.broadcastMessageTitleFont()
        label.preferredMaxLayoutWidth = containerView.bounds.width - 150
        label.textAlignment = .left
        label.text = ""
        label.numberOfLines = 0
        label.textColor = .white
        containerView.addSubview(label)
        return label
    }()
    
    private lazy var lblSubtitle: UILabel  =  {
        let label = UILabel()
        label.font = UIFont.broadcastMessageSubtitleFont()
        label.preferredMaxLayoutWidth = containerView.bounds.width - 150
        label.textAlignment = .left
        label.text = ""
        label.numberOfLines = 0
        label.textColor = .white
        containerView.addSubview(label)
        return label
    }()
    
    private lazy var lblRate: UILabel  =  {
        let label = UILabel()
        label.font = UIFont.instructionRateFont()
        label.textAlignment = .center
        label.text = ""
        label.numberOfLines = 1
        label.textColor = .black
        label.backgroundColor = .white
        containerView.addSubview(label)
        return label
    }()
    
    private var leading: CGFloat = 0
    private var trailing: CGFloat = 0
    private var yOffset: CGFloat = -120
        
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    init(leading: CGFloat, trailing: CGFloat) {
        super.init(frame: .zero)
        self.leading = leading
        self.trailing = trailing
        preferredWidth = UIScreen.main.bounds.width - leading - trailing
        self.lblTitle.preferredMaxLayoutWidth = preferredWidth - 150
        self.lblSubtitle.preferredMaxLayoutWidth = preferredWidth - 150

        setupLayout()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = 16
        containerView.layer.cornerRadius = 16
        layer.masksToBounds = true
        lblRate.layer.cornerRadius = 18
        lblRate.layer.masksToBounds = true
    }

    override func sizeThatFits(_ size: CGSize) -> CGSize {
                
        let lblTitleSize = lblTitle.intrinsicContentSize
        let lblSubtitleSize = lblSubtitle.intrinsicContentSize
                        
        let iconH = topOffset + 76 + topOffset
        let textH = topOffset + lblTitleSize.height + gap + lblSubtitleSize.height + bottomOffset
        let h = max(iconH, textH)

        return CGSize(width: preferredWidth, height: h)
    }

    private func setupLayout() {
        containerView.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.bottom.equalToSuperview()
            make.leading.equalToSuperview()
            make.width.equalTo(preferredWidth)
            make.trailing.equalToSuperview()
        }

        icon.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(16)
            make.top.equalToSuperview().offset(topOffset)
            make.height.equalTo(76)
            make.width.equalTo(76)
        }

        lblTitle.snp.makeConstraints { (make) in
            make.leading.equalTo(icon.snp.trailing).offset(16)
            make.top.equalToSuperview().offset(topOffset)
            make.trailing.equalToSuperview().offset(-16)
        }
        
        lblSubtitle.snp.makeConstraints { (make) in
            make.leading.equalTo(lblTitle.snp.leading)
            make.top.equalTo(lblTitle.snp.bottom).offset(gap)
            make.trailing.equalToSuperview().offset(-16)
        }
        
        lblRate.snp.makeConstraints { (make) in
            make.trailing.equalToSuperview().offset(-24)
            make.width.equalTo(81)
            make.height.equalTo(36)
            make.top.equalToSuperview().offset(topOffset)
        }

    }
    
    func show(in view: UIView, title: String, subTitle: String, icon: UIImage, backgroundColor: UIColor, rate: String = "", yOffset: CGFloat = 40, dismissInSeconds: Int = Int(Values.notificationDuration), onCompletion: (()->Void)? = nil, onDismiss: (() -> Void)? = nil) {

        self.onCompletion = onCompletion
        self.onDismiss = onDismiss
        self.lblSubtitle.text = subTitle
        self.lblTitle.text = title
        self.icon.image = icon
        self.containerView.backgroundColor = backgroundColor
        self.lblRate.isHidden = rate.isEmpty
        self.lblRate.text = rate
        
        view.addSubview(self)
        view.bringSubviewToFront(self)
        self.sizeToFit()

        if dismissInSeconds > 0 {
            timer = Timer.scheduledTimer(timeInterval: TimeInterval(dismissInSeconds), target: self, selector: #selector(onTimer), userInfo: nil, repeats: false)
        }

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) { [weak self] in
            guard let self = self else { return }
            
            let originalFrame = self.frame
            self.frame = CGRect(x: self.leading, y: -originalFrame.height, width: originalFrame.width, height: originalFrame.height)

            UIView.animate(withDuration: Values.standardAnimationDuration,
                            delay: 0.0,
                           options: [UIView.AnimationOptions.curveEaseInOut],
                            animations: {
                                self.frame.origin.y += (yOffset + originalFrame.height)
                                self.onCompletion?()
                            },
                            completion: nil)
        }
    }

    func dismiss() {
        timer?.invalidate()
        timer = nil
        
        if (superview != nil) {
            
            UIView.animate(withDuration: Values.standardAnimationDuration,
                            delay: 0.0,
                           options: [UIView.AnimationOptions.curveEaseInOut],
                            animations: {
                self.frame.origin.y = 0
                            },
                            completion: { [weak self] _ in
                guard let self = self else { return }
                self.removeFromSuperview()
            })

        }
        onDismiss?()
        onDismiss = nil
        onCompletion = nil
    }
        
    @objc private func onTimer() {
        dismiss()
    }

}

//
//  ProgressWalkathonButton.swift
//  Breeze
//
//  Created by Tan, Tran Ngoc on 18/10/2022.
//

import UIKit

class ProgressWalkathonButton: UIButton {

    private var arrow: CAShapeLayer?
    private let buttonSize: CGFloat
    
    // Initializer to create the user tracking mode button
    init(buttonSize: CGFloat) {
        self.buttonSize = buttonSize
        super.init(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        self.setImage(Images.progressWalkathonImage, for: .normal)
        self.imageView?.contentMode = .scaleAspectFit
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

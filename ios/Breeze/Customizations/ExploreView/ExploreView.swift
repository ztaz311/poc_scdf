//
//  ParkingView.swift
//  Breeze
//
//  Created by Tuyen, Le Xuan  on 06/07/2022.
//

import Foundation
import UIKit
import Accelerate


protocol ExploreViewDelegate: NSObjectProtocol {
    func exploreSelected()
}

class ExploreView: XibView {
        
    @IBOutlet weak var statusImageView: UIImageView!
    @IBOutlet var exploreBtn: UIButton!
        
    var isNewContent: Bool = false {
        didSet {
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                self.statusImageView.isHidden = !self.isNewContent
            }
        }
    }
    
    weak var delegate: ExploreViewDelegate?
    
    override func layoutSubviews() {
        super.layoutSubviews()
        roundCorners([.topLeft, .bottomLeft], radius: 27)
    }
    
    @IBAction func actionButton(_ sender: UIButton) {
        delegate?.exploreSelected()
    }
    
    func updatedExploreButtonImage() {
        exploreBtn.setImage(UIImage(named: "explore_selected1"), for: .normal)
    }
}




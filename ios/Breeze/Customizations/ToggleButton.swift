//
//  ToggleButton.swift
//  Breeze
//
//  Created by Zhou Hao on 3/6/21.
//

import UIKit

@IBDesignable
final class ToggleButton: UIButton {
    
    @IBInspectable var onImage:UIImage? {
        didSet {
            updateImage()
        }
    }
    @IBInspectable var offImage:UIImage? {
        didSet {
            updateImage()
        }
    }
    var onToggled: ((Bool) -> Void)?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

    func toggle() {
        isSelected.toggle()
        update()
    }
    
    
    func toggleWithOutCallBack(status:Bool){
        isSelected = !status
        updateImage()
    }
    
    func select(_ selected: Bool) {
        isSelected = selected
        update()
    }
    
    private func update() {
        updateImage()
        if let callback = onToggled {
            callback(isSelected)
        }
    }

    private func setup() {
        self.addTarget(self, action: #selector(btnClicked(_:)),
                       for: .touchUpInside)
    }

    private func updateImage() {
        self.setImage(isSelected ? offImage: onImage, for: .normal)
    }
    
    @objc func btnClicked (_ sender:UIButton) {
        toggle()
    }
    
}

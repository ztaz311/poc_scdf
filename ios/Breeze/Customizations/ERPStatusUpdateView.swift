//
//  ERPStatusUpdateView.swift
//  Breeze
//
//  Created by Tan, Tran Ngoc on 26/06/2023.
//

import UIKit
import SnapKit

enum ERPStatusUpdateStatus: String {
    case normal
    case deduction
    case cardError
    case cardExpired
    case insufficient
    case systemError
}

class ERPStatusUpdateView: UIView {
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var erpIcon: UIImageView!
    @IBOutlet weak var priceView: UIView!
    @IBOutlet weak var title1Label: UILabel!
    @IBOutlet weak var warningIcon: UIImageView!
    @IBOutlet weak var priceLabel: UILabel!
    
    
    private var preferredWidth: CGFloat = 320
    private var leading: CGFloat = 0
    private var trailing: CGFloat = 0
    private var yOffset: CGFloat = -120
    var currentStatus: ERPStatusUpdateStatus = .normal
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupView()
    }
    
    init(leading: CGFloat, trailing: CGFloat) {
        self.leading = leading
        self.trailing = trailing
        preferredWidth = UIScreen.main.bounds.width - leading - trailing
        super.init(frame: CGRectMake(0, 0, preferredWidth, 194))
        setupView()
    }
    
    
    private func setupView() {
        Bundle.main.loadNibNamed("ERPStatusUpdateView", owner:self, options:nil)
        addSubview(contentView)
        contentView.frame = bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        self.layer.masksToBounds = false
        contentView.layer.cornerRadius = 16
        contentView.layer.masksToBounds = false
        contentView.layer.shadowRadius = 6
        contentView.layer.shadowOpacity = 0.4
        contentView.layer.shadowOffset = CGSize(width:0 , height:-3)
        contentView.layer.cornerRadius = 16
        contentView.layer.masksToBounds = true
        priceView.layer.cornerRadius = 20
        priceView.layer.masksToBounds = true
    }
    
    
    func setStatus(_ status: ERPStatusUpdateStatus) {
        self.currentStatus = status
        switch self.currentStatus {
        case .normal:
            title1Label.text = ""
            title1Label.font = UIFont.SFProRegularFont()
            contentView.backgroundColor = UIColor(hexString: "#005FD0", alpha: 1.0)
            warningIcon.isHidden = true
        case .deduction:
            title1Label.text = "Deduction Successful"
            title1Label.font = UIFont.SFProBoldFont32()
            contentView.backgroundColor = UIColor(hexString: "#005FD0", alpha: 1.0)
            warningIcon.image = UIImage(named: "erpGreenCheckIcon")
        case .cardError:
            title1Label.text = "Card Error"
            title1Label.font = UIFont.SFProBoldFont32()
            contentView.backgroundColor = UIColor(hexString: "#E82370", alpha: 1.0)
            warningIcon.image = UIImage(named: "failedERPIcon")
        case .cardExpired:
            title1Label.text = "Card Expired"
            title1Label.font = UIFont.SFProBoldFont32()
            contentView.backgroundColor = UIColor(hexString: "#E82370", alpha: 1.0)
            warningIcon.image = UIImage(named: "failedERPIcon")
        case .insufficient:
            title1Label.text = "Insufficient"
            title1Label.font = UIFont.SFProBoldFont32()
            contentView.backgroundColor = UIColor(hexString: "#E82370", alpha: 1.0)
            warningIcon.image = UIImage(named: "failedERPIcon")
        case .systemError:
            title1Label.text = "System Error"
            title1Label.font = UIFont.SFProBoldFont32()
            contentView.backgroundColor = UIColor(hexString: "#E82370", alpha: 1.0)
            warningIcon.image = UIImage(named: "failedERPIcon")
        }
    }
    
    func display(in view: UIView, yOffset: CGFloat = 40, onCompletion: (()->Void)? = nil, onDismiss: ((ERPStatusUpdateStatus?) -> Void)? = nil) {
        
        view.addSubview(self)
        view.bringSubviewToFront(self)
        self.sizeToFit()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) { [weak self] in
            guard let self = self else { return }
            
            let originalFrame = self.frame
            self.frame = CGRect(x: self.leading, y: -originalFrame.height, width: originalFrame.width, height: originalFrame.height)
            
            UIView.animate(withDuration: Values.standardAnimationDuration,
                           delay: 0.0,
                           options: [UIView.AnimationOptions.curveEaseInOut],
                           animations: {
                self.frame.origin.y += (yOffset + originalFrame.height)
                
            },
                           completion: nil)
        }
    }
    
    
}


//
//  AdjustableSlider.swift
//  Breeze
//
//  Created by Zhou Hao on 19/3/21.
//

import UIKit

@IBDesignable
class AdjustableSlider: UISlider {

    @IBInspectable var trackHeight: CGFloat = 2
    @IBInspectable var thumbRadius: CGFloat = 20
    @IBInspectable var borderColor: UIColor?

    private lazy var thumbView: UIView = {
        let thumb = UIView()
        thumb.backgroundColor = self.thumbTintColor
        thumb.layer.borderWidth = 1
        thumb.layer.borderColor = borderColor == nil ? thumbTintColor?.cgColor : borderColor!.cgColor
        return thumb
    }()

    override func awakeFromNib() {
        super.awakeFromNib()
        let thumb = thumbImage(radius: thumbRadius)
        setThumbImage(thumb, for: .normal)
    }

    private func thumbImage(radius: CGFloat) -> UIImage {

        let w = radius * 2
        thumbView.frame = CGRect(x: 0, y: radius, width: w, height: w)
        thumbView.layer.cornerRadius = radius

        let renderer = UIGraphicsImageRenderer(bounds: thumbView.bounds)
        return renderer.image { rendererContext in
            thumbView.layer.render(in: rendererContext.cgContext)
        }
    }

    override func trackRect(forBounds bounds: CGRect) -> CGRect {
        var newRect = super.trackRect(forBounds: bounds)
        newRect.size.height = trackHeight
        return newRect
    }

}

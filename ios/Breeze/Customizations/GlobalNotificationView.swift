//
//  GlobalNotificationView.swift
//  Breeze
//
//  Created by Zhou Hao on 3/3/22.
//

import UIKit

final class GlobalNotificationView: BreezeToast {

    init() {
        // we have to past an empty onAction to leave some space for the action button
        super.init(appearance: BreezeToastStyle.globalNotification, onClose:nil, actionTitle: "", onAction:nil)
    }
    
    init(appearance: BreezeToastAppearance) {
        super.init(appearance: appearance, onClose:nil, actionTitle: "", onAction:nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func show(in view: UIView, title: String, message: String, icon: UIImage, onClose: (() -> Void)?, actionTitle: String?, onAction: (() -> Void)?, dismissInSeconds: Int = Values.toastAutoDismissInSeconds, inHtml: Bool = false) {
        
        self.actionText = actionTitle ?? ""
        self.onClose = onClose
        self.onAction = onAction
        self.replaceIcon(icon)
        
        show(in: view, title: title, message: message, animated: true, dismissInSeconds: dismissInSeconds, inHtml: inHtml)
    }
}

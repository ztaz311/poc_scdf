//
//  DesignableTextfields.swift
//  Breeze
//
//  Created by Malou Mendoza on 27/11/20.
//

import Foundation
import UIKit
import FontAwesome_swift

@IBDesignable
class DesignableTextField: UITextField {
    
    @IBInspectable var isPwHidden: Bool = true
    @IBInspectable var leftImage: UIImage? { didSet { updateView() }}
    @IBInspectable var left2Image: UIImage? { didSet { updateView() }}
    @IBInspectable var leftPadding:CGFloat=0 {
        didSet{
            updateView()
        }
    }
    @IBInspectable var rightImage: UIImage? { didSet { updateView() }}
    @IBInspectable var rightPadding:CGFloat = -5.0 {
        didSet{
            updateView()
        }
    }
    func updateView() {
        
        
        if let image = leftImage {
            leftViewMode = .always
            
            let imageView = UIImageView (image: image)
            imageView.frame = CGRect( x:leftPadding, y:0, width: image.size.width, height: image.size.height)
            imageView.contentMode = .scaleAspectFit
            let view = UIView(frame: CGRect(x:0, y:0, width: 45, height: 20))
            view.addSubview(imageView)
            
            leftView = view
        }else if let image = left2Image {
            leftViewMode = .always
            
            let imageView = UIImageView (image: image)
            imageView.frame = CGRect( x:leftPadding, y:0, width: 78, height: 20)
            imageView.contentMode = .scaleAspectFit
            
            let view = UIView(frame: CGRect(x:0, y:0, width: 110, height: 20))
            view.addSubview(imageView)
            
            leftView = view
        }else{
            leftViewMode = .never
        }
        
        if let image = rightImage {
            rightViewMode = .always
            
            //            let imageView = UIImageView(frame: CGRect(x:rightPadding, y:0, width: 25, height: 20))
            //            imageView.image = image
            
            let imageView = UIImageView (image: image)
            imageView.frame = CGRect( x:rightPadding, y:-5, width: image.size.width, height: image.size.height)
            imageView.contentMode = .scaleAspectFit
            
            let tapShowPw = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapShowPw:)))
            imageView.isUserInteractionEnabled = false
            imageView.addGestureRecognizer(tapShowPw)
            
            let view = UIView(frame: CGRect(x:0, y:0, width: 45, height: 20))
            view.addSubview(imageView)
            
            rightView = view
            
        }else{
            rightViewMode = .never
        }
        
        
        
    }
    @objc func imageTapped(tapShowPw: UITapGestureRecognizer)
    {
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.settings_account_reset_pw_hideunhide, screenName: ParameterName.settings_account_reset_pw_screen)
        
        // let tappedImage = tapShowPw.view as! UIImageView
        if self.isPwHidden{
            self.isPwHidden = false
            let image = UIImage.fontAwesomeIcon(name: .eye, style: .regular, textColor: .lightGray, size: CGSize(width: 20, height: 20))
            
            self.rightImage  = image
            self.isSecureTextEntry = false
        }else{
            self.isPwHidden = true
            let image = UIImage.fontAwesomeIcon(name: .eyeSlash, style: .regular, textColor: .lightGray, size: CGSize(width: 20, height: 20))
            self.rightImage  = image
            self.isSecureTextEntry = true
        }
    }
}



//
//  ObuCommonNotificationView.swift
//  Breeze
//
//  Created by Tuan, Pham Hai on 11/07/2023.
//

import Foundation
import UIKit

class ObuCommonNotificationView: UIView {
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    
    @IBOutlet weak var rateView: UIView!
    @IBOutlet weak var rateLabel: UILabel!
    @IBOutlet weak var rateIcon: UIImageView!
    
    @IBOutlet weak var rateIconWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var titleCenterYConstraint: NSLayoutConstraint!
    @IBOutlet weak var titleTrailingConstraint: NSLayoutConstraint!
    
    private var onCompletion: (() -> Void)?
    private var onDismiss: (() -> Void)?
    
    private var preferredWidth: CGFloat = 320
    private var leading: CGFloat = 0
    private var trailing: CGFloat = 0
    private var yOffset: CGFloat = -120
    
    private var timer: Timer?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupView()
    }
    
    init(leading: CGFloat, trailing: CGFloat) {
        self.leading = leading
        self.trailing = trailing
        preferredWidth = UIScreen.main.bounds.width - leading - trailing
        super.init(frame: CGRectMake(0, 0, preferredWidth, 128))
        setupView()
    }
    
    private func setupView() {
        
        Bundle.main.loadNibNamed("ObuCommonNotificationView", owner:self, options:nil)
        addSubview(contentView)
        contentView.frame = bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.layer.masksToBounds = false
        
        contentView.layer.cornerRadius = 16
        contentView.layer.masksToBounds = false
        contentView.layer.shadowRadius = 6
        contentView.layer.shadowOpacity = 0.4
        contentView.layer.shadowColor = UIColor(named:"tooltipsShadowColor")!.cgColor
        contentView.layer.shadowOffset = CGSize(width:0 , height:-3)
        
        backgroundView.layer.cornerRadius = 16
        backgroundView.layer.masksToBounds = true
                
        rateView.layer.cornerRadius = 18
        rateView.layer.masksToBounds = true
        
        imageView.layer.cornerRadius = 38
        imageView.layer.masksToBounds = true

    }
    
    func updateUI(title: String = "", subTitle: String = "", icon: UIImage, backgroundColor: UIColor, rate: String = "", iconRate: UIImage?) {
        
        imageView.image = icon
        
        if appDelegate().isSmallIphone() {
            titleLabel.font = UIFont.rerouteMessageBoldFont()
            subTitleLabel.font = UIFont.nudgeToolTipsTextFont()
            if !title.isEmpty {
                setupTitleUI(title: title, subTitle: subTitle, height: -16, isHidden: false)
            } else {
                setupTitleUI(title: subTitle, subTitle: "", height: 0, isHidden: true)
            }
        } else {
            titleLabel.font = UIFont.obuCommonTitleFont()
            subTitleLabel.font = UIFont.obuCommonSubTitleFont1()
            if !title.isEmpty {
                setupTitleUI(title: title, subTitle: subTitle, height: -16, isHidden: false)
            } else {
                setupTitleUI(title: subTitle, subTitle: "", height: 0, isHidden: true)
            }
        }
        
//        if !title.isEmpty {
////            titleCenterYConstraint.constant = -30
//            titleLabel.text = title
//            subTitleLabel.text = subTitle
//            subTitleLabel.isHidden = false
//        } else {
//            titleCenterYConstraint.constant = 0
//            titleLabel.text = subTitle
//            subTitleLabel.isHidden = true
//        }
        
        backgroundView.backgroundColor = backgroundColor
        
        if !rate.isEmpty {
            titleTrailingConstraint.constant = 120
            
            rateView.isHidden = false
            
            rateLabel.text = rate
            rateLabel.font = UIFont.obuChargeAmountFont()
            
            rateIcon.isHidden = (iconRate == nil)
            rateIcon.image = iconRate
            rateIconWidthConstraint.constant = (iconRate == nil) ? 0 : 28
            
        } else {
            titleTrailingConstraint.constant = 12
            rateView.isHidden = true
        }
    }
    
    func setupTitleUI(title:String, subTitle: String, height: CGFloat, isHidden: Bool) {
        titleCenterYConstraint.constant = height
        subTitleLabel.isHidden = isHidden
        titleLabel.text = title
        subTitleLabel.text = subTitle
        titleLabel.lineBreakMode = .byWordWrapping
        subTitleLabel.lineBreakMode = .byWordWrapping
        titleLabel.textColor = .white
        subTitleLabel.textColor = .white
    }
    
    
    func show(in view: UIView, yOffset: CGFloat = 40, dismissInSeconds: Int = Int(Values.notificationDuration), onCompletion: (() -> Void)? = nil, onDismiss: (() -> Void)? = nil) {

        self.onCompletion = onCompletion
        self.onDismiss = onDismiss
        
        //  Set text
        
        view.addSubview(self)
        view.bringSubviewToFront(self)
        self.sizeToFit()
        
        if dismissInSeconds > 0 {
            timer = Timer.scheduledTimer(timeInterval: TimeInterval(dismissInSeconds), target: self, selector: #selector(onTimer), userInfo: nil, repeats: false)
        }

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) { [weak self] in
            guard let self = self else { return }
            
            let originalFrame = self.frame
            self.frame = CGRect(x: self.leading, y: -originalFrame.height, width: originalFrame.width, height: originalFrame.height)

            UIView.animate(withDuration: Values.standardAnimationDuration,
                            delay: 0.0,
                           options: [UIView.AnimationOptions.curveEaseInOut],
                            animations: {
                                self.frame.origin.y += (yOffset + originalFrame.height)
                                self.onCompletion?()
                            },
                            completion: nil)
        }
    }

    func dismiss() {
        if (superview != nil) {
            
            UIView.animate(withDuration: Values.standardAnimationDuration,
                            delay: 0.0,
                           options: [UIView.AnimationOptions.curveEaseInOut],
                            animations: {
                self.frame.origin.y = 0
                            },
                            completion: { [weak self] _ in
                guard let self = self else { return }
                self.removeFromSuperview()
            })

        }
        onDismiss?()
        onDismiss = nil
        onCompletion = nil
    }
    
    @objc private func onTimer() {
        dismiss()
    }
}



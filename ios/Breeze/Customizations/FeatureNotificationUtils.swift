//
//  FeatureNotificationUtils.swift
//  Breeze
//
//  Created by Zhou Hao on 21/10/21.
//

import UIKit

// Currently only use it to getTypeImage and getText
final class FeatureNotification {
    
    static func getDistance(for feature: FeatureDetectable,isCarPlay:Bool = false) -> String{
        
        if let feature = feature as? DetectedTrafficIncident {
            
            if feature.category == TrafficIncidentCategories.FlashFlood  || feature.category == TrafficIncidentCategories.RoadClosure || feature.category == TrafficIncidentCategories.MajorAccident || feature.category == TrafficIncidentCategories.HeavyTraffic {
                
                return feature.message
            }
        }
        
        return String(format: "%d m", Int(feature.distance))
    }
    
    static func getVoiceText(for feature: FeatureDetectable) -> String {
        
        if let feature = feature as? DetectedTrafficIncident {
            
            if feature.category == TrafficIncidentCategories.FlashFlood || feature.category == TrafficIncidentCategories.RoadClosure || feature.category == TrafficIncidentCategories.MajorAccident || feature.category == TrafficIncidentCategories.HeavyTraffic {
                
                return "\(feature.type()) at \(feature.message)"
            }
        }
        
        return getAudioMessageBasedOnType(type: feature.type())
    }
    
    static func getAudioMessageBasedOnType(type:String) -> String {
        
        var audioText = "\(type) ahead"
        
        if type == FeatureDetection.featureTypeSpeedcamera {
            audioText = "Speed camera nearby."
        } else if type == FeatureDetection.featureTypeErp {
            audioText = "E R P ahead"
        }
        
        /*if type == FeatureDetection.featureTypeSchoolZone{
         
         audioText = "Look out for children."
         }
         else if type == FeatureDetection.featureTypeSilverZone{
         
         audioText = "Go slow."
         }*/
        
        return audioText
        
        //return "\(type) ahead"
    }
    
    // TODO: Refactor to let FeatureDetectable itself decide the text and image
    static func getTypeText(for feature: FeatureDetectable,isCarPlay:Bool = false) -> String {
        if feature is DetectedSchoolZone {
            return FeatureDetection.featureTypeSchoolZone
        } else if feature is DetectedSilverZone {
            return FeatureDetection.featureTypeSilverZone
        } else if let feature = feature as? DetectedERP {
            // showing name in ERP notification instead of type
            //            return FeatureDetection.featureTypeErp
            return feature.address
        } else if let feature = feature as? DetectedTrafficIncident {
            return feature.category
        }else if let feature = feature as? DetectedSpeedCamera {
            return feature.category
        } else if let feature = feature as? DetectedSeasonParking {
            return feature.type()
        }
        return ""
    }
    
    static func getTypeBackground(_ feature: FeatureDetectable,isCarPlay:Bool = false) -> UIColor {
        if feature is DetectedSchoolZone || feature is DetectedSilverZone {
            return UIColor.notificationSchoolAndSilverZoneBG
        } else if feature is DetectedERP {
            return UIColor.notificationERPBG
        } else if feature is DetectedTrafficIncident {
            return UIColor.notificationTrafficBG
        } else if feature is DetectedSpeedCamera {
            return UIColor.notificationTrafficBG
        } else if feature is DetectedSeasonParking {
            return UIColor.notificationTrafficBG
        }
        
        // default color
        return UIColor.notificationSchoolAndSilverZoneBG
    }
    
    static func getTypeImage(_ feature: FeatureDetectable,isCarPlay:Bool = false) -> UIImage {
        if feature is DetectedSchoolZone {
            return isCarPlay ? UIImage(named: "carplay-schoolZoneIcon")! : UIImage(named: "schoolZoneIcon")!
        } else if feature is DetectedSilverZone {
            return isCarPlay ? UIImage(named: "carplay-silverZoneIcon")! : UIImage(named: "silverZoneIcon")!
        } else if feature is DetectedERP {
            return isCarPlay ? UIImage(named: "carplay-erpIcon")! : UIImage(named: "erpIcon")!
        }else if feature is DetectedSpeedCamera {
            return UIImage(named: "speedCameraIcon")!
        }
        else if let feature = feature as? DetectedTrafficIncident {
            
            //For Demo
            if(feature.category == Values.wayPointSpeed)
            {
                return isCarPlay ? UIImage(named: "carplay-Miscellaneous-Icon")! : UIImage(named: "Miscellaneous-Icon")!
            }
            //For Demo
            else if(feature.category == Values.wayPointETAExpress)
            {
                return isCarPlay ? UIImage(named: "carplay-etaExpress")! : UIImage(named: "etaExpress")!
            }
            //For Demo
            else if(feature.category == Values.wayPointRoad || feature.category == Values.wayPointCarparkLots || feature.category == Values.wayPointCarParkLevel3 )
            {
                return UIImage(named: "Parking-Icon")!
            } else if (feature.category == Values.wayPointCyclist1 || feature.category == Values.wayPointCyclist2) {
                return UIImage(named: "Cyclist-Icon")!
            }
            else{
                return isCarPlay ? UIImage(named: "carplay-\(feature.category)-Icon")! : UIImage(named: "\(feature.category)-Icon")!
            }
            
        } else if feature is DetectedSeasonParking {
            return UIImage(named: "seasonParkIcon")!
        }
#if DEMO
        if(DemoSession.shared.isDemoRoute || DemoSession.shared.parkAnyWhere)
        {
            if let feature = feature as? DetectedETANotificationView {
                
                if(feature.category == Values.carPark)
                {
                    return UIImage(named: "Parking-Icon")!
                }
                else
                {
                    return UIImage(named: "eta-Icon")!
                }
                
            }
        }
        
#endif
        return UIImage()
    }
    
    static func showNotification(notificationView: BreezeNotificationView, inView: UIView, feature: FeatureDetectable, yOffset: CGFloat, rate: String = "", onCompletion: (()->Void)?, onDismiss: (() -> Void)? = nil) {
        
        let title = FeatureNotification.getDistance(for: feature)
        let subtitle = FeatureNotification.getTypeText(for: feature)
        let icon = FeatureNotification.getTypeImage(feature)
        let background = FeatureNotification.getTypeBackground(feature)
        var rate = ""
        if let erpFeature = feature as? DetectedERP {
            if erpFeature.price > 0 {
                rate = "$\(String(format: "%.2f", erpFeature.price))"
            }
        }
        
        notificationView.show(in: inView, title: title, subTitle: subtitle, icon: icon, backgroundColor: background, rate: rate, yOffset: yOffset, onCompletion: onCompletion, onDismiss: onDismiss)
    }
    
    static func showSoonArriveNotification(notificationView: BreezeBroadcastMessageView, inView: UIView, carpark: Carpark?, yOffset: CGFloat, message: String = "",onCompletion: (()->Void)?, onDismiss: (() -> Void)? = nil) {
        
        let title = carpark?.name ?? ""
        let subtitle = message
        let icon = UIImage(named: "carparkSoonArriveIcon")!
        let background = UIColor.notificationCarparkSoonArrive
        
        notificationView.show(in: inView, title: title, subTitle: subtitle, icon: icon, backgroundColor: background, yOffset: yOffset, dismissInSeconds: 15, onCompletion: onCompletion, onDismiss: onDismiss)
    }
    
    static func showParkingAlert(notificationView: ParkingAvailabilityAlertView, inView: UIView, carpark: Carpark?, yOffset: CGFloat, timeDisplay: Int, onCompletion: (()->Void)?, onDismiss: ((ParkingAvailabilityStatus?, Carpark?) -> Void)? = nil) {
        let type = carpark?.availablePercentImageBand ?? ""
        notificationView.show(in: inView, type: type, yOffset: yOffset, dismissInSeconds: timeDisplay , onCompletion: onCompletion, onDismiss: onDismiss)
    }
    
    static func showParkingAvailability(notificationView: ParkingAvailabilityView, inView: UIView, carpark: Carpark?, yOffset: CGFloat, onCompletion: (()->Void)?, onDismiss: ((ParkingAvailabilityStatus?) -> Void)? = nil) {
        
        let type = carpark?.availablePercentImageBand ?? ""
        
        notificationView.show(in: inView, type: type, yOffset: yOffset, onCompletion: onCompletion, onDismiss: onDismiss)
    }
    
    static func showCarparkUpdateView(notificationView: ParkingAndTravelTimeView, title: String?, subTitle: String?,
                                      backgroundColor: UIColor, icon: UIImage, inView: UIView, yOffset: CGFloat, onCompletion: (()->Void)? = nil, onDismiss: (() -> Void)? = nil) {
        notificationView.show(in: inView, title: title, subTitle: subTitle, icon: icon, backgroundColor: backgroundColor, yOffset: yOffset, dismissInSeconds: 5, onCompletion: onCompletion, onDismiss: onDismiss)
    }
    
    
    static func showTrafficStatusView(notification: TrafficNotificationView, title: String?, subTitle: String?,
                                      backgroundColor: UIColor, icon: UIImage, inview: UIView, yOffset: CGFloat, dismissInSeconds:Int? = 5, needAdjustFont: Bool = false, onCompletion: (()->Void)?, onDismiss: (() -> Void)? = nil) {
        notification.show(in: inview, title: title, subTitle: subTitle, icon: icon, backgroundColor: backgroundColor, yOffset: yOffset, dismissInSeconds: dismissInSeconds ?? 5, needAdjustFont: needAdjustFont, onCompletion: onCompletion, onDismiss: onDismiss)
    }
    
    static func showOBUDataView(notificationView: ObuCommonNotificationView, inview: UIView, yOffset: CGFloat, onCompletion: (()->Void)?, onDismiss: (() -> Void)? = nil) {
        notificationView.show(in: inview, yOffset: yOffset, dismissInSeconds: 5, onCompletion: onCompletion, onDismiss: onDismiss)
    }
    
    static func showGeneralMessageView(notification: GeneralMessageNotificationView, title: String?, subTitle: String?, backgroundColor: UIColor, inview: UIView, yOffset: CGFloat, onCompletion: (()->Void)?, onDismiss: (()->Void)? = nil) {
        
        notification.show(in: inview, title: title, subTitle: subTitle, backgroundColor: backgroundColor, yOffset: yOffset, dismissInSeconds: 5, onCompletion: onCompletion, onDismiss: onDismiss)
    }
    
}

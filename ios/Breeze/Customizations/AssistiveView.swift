//
//  AssistiveView.swift
//  Breeze
//
//  Created by VishnuKanth on 08/03/21.
//

import UIKit

class FloatingView : UIView {

    var shouldSetupConstraints = true
        
      var feedbackBtn = UIButton(type: .custom)
      var courseList = UIButton(type: .custom)
      var segmentedControl: UISegmentedControl!
        
      let screenSize = UIScreen.main.bounds
      
      override init(frame: CGRect){
        super.init(frame: frame)
        
        self.backgroundColor = .clear
        feedbackBtn.frame = CGRect(x: 0, y: 0, width: (Images.feedbackImage?.size.width ?? 0), height: (Images.feedbackImage?.size.height ?? 0))
        feedbackBtn.center = CGPoint(x: self.frame.width/2, y: self.frame.height/4)
        feedbackBtn.setImage(Images.feedbackImage, for: .normal)
        self.addSubview(feedbackBtn)
            
        
        courseList.frame = CGRect(x: feedbackBtn.frame.origin.x, y:  feedbackBtn.frame.size.height+feedbackBtn.frame.origin.y , width: (Images.tileListImage?.size.width ?? 0), height: (Images.tileListImage?.size.height ?? 0))
        courseList.setImage(Images.tileListImage, for: .normal)
        self.addSubview(courseList)
      }
        
    required init?(coder decoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

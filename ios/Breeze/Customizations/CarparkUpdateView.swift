//
//  CarparkUpdateView.swift
//  Breeze
//
//  Created by Tan, Tran Ngoc on 26/06/2023.
//

import UIKit
import SnapKit

enum CarparkUpdateAndTravelTimeStatus: String {
    case allAvailable
    case fillingUpFast
    case nearbyAllFull
    case travelTime
}

typealias CarparkUpdateAndTravelTimeCompletion = ()-> (Void)


class CarparkUpdateView: UIView {
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var parkingIcon: UIImageView!
    @IBOutlet weak var readMoveview: UIView!
    @IBOutlet weak var title1Label: UILabel!
    @IBOutlet weak var title2Label: UILabel!
    @IBOutlet weak var topConstraint : NSLayoutConstraint!
    
    
    var currentStatus: CarparkUpdateAndTravelTimeStatus = .allAvailable
    var data: [String: Any] = [:]
    
    private var onCompletion: (() -> Void)?
    private var onDismiss: ((CarparkUpdateAndTravelTimeStatus?) -> Void)?
    
    private var preferredWidth: CGFloat = 320
    private var leading: CGFloat = 0
    private var trailing: CGFloat = 0
    private var yOffset: CGFloat = -120
    private var timer: Timer?
    
    var updateCompletion: CarparkUpdateAndTravelTimeCompletion?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupView()
    }
    
    init(leading: CGFloat, trailing: CGFloat) {
        self.leading = leading
        self.trailing = trailing
        preferredWidth = UIScreen.main.bounds.width - leading - trailing
        super.init(frame: CGRectMake(0, 0, preferredWidth, 194))
        setupView()
    }
   

    @IBAction func readMoreClick(_ sender: Any) {
        updateCompletion?()
    }
    
    private func setupView() {
        
        Bundle.main.loadNibNamed("CarparkUpdateView", owner:self, options:nil)
        addSubview(contentView)
        contentView.frame = bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        self.layer.masksToBounds = false
        contentView.layer.cornerRadius = 16
        contentView.layer.masksToBounds = false
        contentView.layer.shadowRadius = 6
        contentView.layer.shadowOpacity = 0.4
        contentView.layer.shadowOffset = CGSize(width:0 , height:-3)
        contentView.layer.cornerRadius = 16
        contentView.layer.masksToBounds = true
        readMoveview.layer.cornerRadius = 20
        readMoveview.layer.masksToBounds = true
    }
    
    func setStatus(_ status: CarparkUpdateAndTravelTimeStatus) {
                
        self.currentStatus = status
        switch self.currentStatus {
        case .allAvailable:
            title1Label.isHidden = false
            topConstraint.constant = 66
            title1Label.text = "Carpark update"
            title2Label.text = "Nearby all available"
            title1Label.font = UIFont.cruiseToastRegularFont()
            title2Label.font = UIFont.cruiseEndButtonFont()
            contentView.backgroundColor = UIColor(hexString: "#15B765", alpha: 1.0)
            readMoveview.backgroundColor = UIColor(hexString: "#15B765", alpha: 1.0)
            parkingIcon.image = UIImage(named: "ParkingGreenIcon")
        case .fillingUpFast:
            title1Label.isHidden = false
            topConstraint.constant = 66
            title1Label.text = "Carpark update"
            title2Label.text = "Filling Up Fast"
            title1Label.font = UIFont.cruiseToastRegularFont()
            title2Label.font = UIFont.cruiseEndButtonFont()
            contentView.backgroundColor = UIColor(hexString: "#F26415", alpha: 1.0)
            readMoveview.backgroundColor = UIColor(hexString: "#F26415", alpha: 1.0)
            parkingIcon.image = UIImage(named: "ParkingOrangeIcon")
        case .nearbyAllFull:
            title1Label.isHidden = false
            topConstraint.constant = 66
            title1Label.text = "Carpark update"
            title2Label.text = "Nearby All Full"
            title1Label.font = UIFont.cruiseToastRegularFont()
            title2Label.font = UIFont.cruiseEndButtonFont()
            contentView.backgroundColor = UIColor(hexString: "#E82370", alpha: 1.0)
            readMoveview.backgroundColor = UIColor(hexString: "#E82370", alpha: 1.0)
            parkingIcon.image = UIImage(named: "ParkingRedIcon")
        case .travelTime:
            topConstraint.constant = 42
            title1Label.isHidden = true
            title2Label.text = "Travel Time"
            title2Label.font = UIFont.SFProBoldFont32()
            contentView.backgroundColor = UIColor(hexString: "#E82370", alpha: 1.0)
            readMoveview.backgroundColor = UIColor(hexString: "#E82370", alpha: 1.0)
            parkingIcon.image = UIImage(named: "TravelTimeIcon")
        }
    }
    
    
    func display(in view: UIView, yOffset: CGFloat = 40, onCompletion: (()->Void)? = nil, onDismiss: (() -> Void)? = nil) {
        
//        self.onReadMore = onCompletion
        
        view.addSubview(self)
        view.bringSubviewToFront(self)
        self.sizeToFit()
        
        timer = Timer.scheduledTimer(timeInterval: TimeInterval(5.0), target: self, selector: #selector(onTimer), userInfo: nil, repeats: false)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) { [weak self] in
            guard let self = self else { return }
            
            let originalFrame = self.frame
            self.frame = CGRect(x: self.leading, y: -originalFrame.height, width: originalFrame.width, height: originalFrame.height)
            
            UIView.animate(withDuration: Values.standardAnimationDuration,
                           delay: 0.0,
                           options: [UIView.AnimationOptions.curveEaseInOut],
                           animations: {
                self.frame.origin.y += (yOffset + originalFrame.height)
//                self.onReadMore?()
            },
                           completion: nil)
        }
    }
    
    func dismiss() {
        if (superview != nil) {
            
            UIView.animate(withDuration: Values.standardAnimationDuration,
                            delay: 0.0,
                           options: [UIView.AnimationOptions.curveEaseInOut],
                            animations: {
                self.frame.origin.y = 0
                            },
                            completion: { [weak self] _ in
                guard let self = self else { return }
                self.removeFromSuperview()
            })

        }
        onDismiss = nil
        onCompletion = nil
    }
    
    
    @objc private func onTimer() {
        dismiss()
    }
}

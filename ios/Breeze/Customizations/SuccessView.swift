//
//  SuccessView.swift
//  Breeze
//
//  Created by VishnuKanth on 04/12/20.
//

import UIKit

protocol SuccessViewDelegate: AnyObject {
    func SuccessScreenDimiss()
}

class SuccessView: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet var innerContentView: UIView!
    @IBOutlet var successImageView:UIImageView!
    @IBOutlet var successTextLabel:UILabel!
    @IBOutlet var okayBtn:UIButton!
    @IBOutlet  var okayBtnBottomConstraint: NSLayoutConstraint!
    @IBOutlet  var successTextTopConstraint: NSLayoutConstraint!
    @IBOutlet  var successImageTopConstraint: NSLayoutConstraint!
    
    var successImage: UIImage? {
        get {
            return successImageView.image
        }
        set {
            successImageView.image = newValue
        }
    }
    
    var successText: NSAttributedString? {
        get {
            return successTextLabel.attributedText
        }
        set {
            successTextLabel.attributedText = newValue
        }
    }
    
    weak var delegate: SuccessViewDelegate?
    
    private func initFromNib() {
        
        Bundle.main.loadNibNamed("SuccessView",owner:self,options:nil)

        addSubview(contentView)
        contentView.frame = bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        innerContentView.layer.cornerRadius = 20
        self.overrideUserInterfaceStyle = UIUserInterfaceStyle(rawValue: Settings.shared.theme) ?? .unspecified
        successTextLabel.textColor = UIColor(named: "successTitleColor")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initFromNib()
    }
    
    override func layoutSubviews() {
            super.layoutSubviews()
        
        okayBtn.layer.cornerRadius = 30
        okayBtn.backgroundColor = UIColor(named: "successButtonColor")
//        okayBtn.applyGradient(colors: [UIColor.brandPurpleColor.cgColor,UIColor.brandPurpleColor.cgColor])
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initFromNib()
    }
    
    @IBAction @objc func okayBtnClicked(_ sender: Any) {
        delegate?.SuccessScreenDimiss()
    }

}

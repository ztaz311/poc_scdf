//
//  RPDatePicker.swift
//  Breeze
//
//  Created by VishnuKanth on 30/12/21.
//

import Foundation
import SnapKit
import UIKit
import MapboxNavigation

class RPDatePickerView: UIView {
    
    private var popupHeight: CGFloat = 410
    var bgColor: UIColor = .white // defaul to white
    var cornerRadius: CGFloat = 10.0
    var previousSelectedDate:Date? {
        didSet {
            DispatchQueue.main.async {
                if let previousSelectedDate = self.previousSelectedDate {
                    self.datePicker.setDate(previousSelectedDate, animated: false)
                }
                
            }
        }
    }
    var isDepartSelected = true {
        didSet {
            
            if(isDepartSelected){
                
                btnDepartAt.setTitleColor(UIColor(named:"tooltipsAmenityNaviBtnColor")!, for: .normal)
                
                btnArriveBy.setTitleColor(UIColor(named:"tabBarNotSelectedColor")!, for: .normal)
            }
            else{
                
                btnDepartAt.setTitleColor(UIColor(named:"tabBarNotSelectedColor")!, for: .normal)
                
                btnArriveBy.setTitleColor(UIColor(named:"tooltipsAmenityNaviBtnColor")!, for: .normal)
            }
        }
    }
    var departAt: ((_ date:String) -> Void)?
    var arriveBy: ((_ date:String) -> Void)?
    var cancelDatePicker: ((_ cancel:Bool) -> Void)?
    var nextDatePicker: ((_ formattedStr:String,_ unixDate:Double) -> Void)?
    private let buttonHeight: CGFloat = 48.0
   // var isDepartSelected = true
    private(set) lazy var containerView: UIView = {
        let view = UIView()
        view.backgroundColor = bgColor
        view.layer.cornerRadius = cornerRadius
        addSubview(view)
        return view
    }()
    
    private lazy var btnDepartAt: UIButton = {
        let button = UIButton()
        //button.backgroundColor = UIColor(named:"tooltipsAmenityNaviBtnColor")!
        button.setTitle("Depart at", for: .normal)
        button.setTitleColor(UIColor(named:"tooltipsAmenityNaviBtnColor")!, for: .normal)
        button.titleLabel?.font = UIFont.datePickerDepartAtTextFont()
        button.addTarget(self, action: #selector(onDepartAt), for: .touchUpInside)
        containerView.addSubview(button)
        return button
    }()
    
    private lazy var lineView: UIView = {
        let lineView = UIView()
        lineView.backgroundColor = UIColor(named:"lineColor")!
        containerView.addSubview(lineView)
        return lineView
    }()
    
    private lazy var btnArriveBy: UIButton = {
        let button = UIButton()
        //button.backgroundColor = UIColor(named:"tooltipsAmenityNaviBtnColor")!
        button.setTitle("Arrive by", for: .normal)
        button.setTitleColor(UIColor(named:"tabBarNotSelectedColor")!, for: .normal)
        button.titleLabel?.font = UIFont.datePickerDepartAtTextFont()
        button.addTarget(self, action: #selector(onArriveBy), for: .touchUpInside)
        containerView.addSubview(button)
        return button
    }()
    
    private lazy var datePickerCancel: UIButton = {
        let button = UIButton()
        button.setTitle("Cancel", for: .normal)
        button.setTitleColor(UIColor(named:"tooltipsAmenityNaviBtnColor")!, for: .normal)
        button.titleLabel?.font = UIFont.datePickerBottomButtonsTextFont()
        button.backgroundColor = .clear
        button.layer.cornerRadius = buttonHeight / 2
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor(named:"tooltipsAmenityNaviBtnColor")?.cgColor ?? UIColor.purple.cgColor
        button.addTarget(self, action: #selector(onCancel), for: .touchUpInside)
        containerView.addSubview(button)
        return button
    }()
    
    private lazy var btnNext: UIButton = {
        let button = UIButton()
        button.backgroundColor = UIColor(named:"tooltipsAmenityNaviBtnColor")!
        button.setTitle("Next", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = UIFont.datePickerBottomButtonsTextFont()
        button.addTarget(self, action: #selector(onNext), for: .touchUpInside)
        containerView.addSubview(button)
        return button
    }()
    
    private lazy var pickerBackgroundView: UIView = {
        
        let pickerBackgroundView = UIView()
        pickerBackgroundView.backgroundColor = .clear
        containerView.addSubview(pickerBackgroundView)
        return pickerBackgroundView
    }()
    
    private lazy var datePicker: UIDatePicker = {
        let datePicker = UIDatePicker()
        datePicker.locale = Locale.enSGLocale() // to set it to 24 hour format
        datePicker.backgroundColor = .white
        datePicker.datePickerMode = .dateAndTime
        datePicker.minimumDate = Date()
        let calendar = Calendar(identifier: .gregorian)

            let currentDate = Date()
            var components = DateComponents()
            components.calendar = calendar

            components.month = 3
            let maxDate = calendar.date(byAdding: components, to: currentDate)!
        datePicker.maximumDate = maxDate
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
        } else {
            // Fallback on earlier versions, system will take default one if the OS is below 13.4. So we don't specify anything
            
        }
        
        datePicker.addTarget(self, action: #selector(handleDatePicker), for: .valueChanged)
        pickerBackgroundView.addSubview(datePicker)
        return datePicker
    }()
    
    init() {
        super.init(frame: .zero)
        self.backgroundColor = .clear
        self.bgColor = UIColor(named: "tooltipsAmenityBackgroundColor")!
        
        setupLayout()
        
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        datePickerCancel.layer.cornerRadius = buttonHeight / 2
        btnNext.layer.cornerRadius = buttonHeight / 2
    }
    
    @objc private func onDepartAt() {
       
            btnDepartAt.setTitleColor(UIColor(named:"tooltipsAmenityNaviBtnColor")!, for: .normal)
            
            btnArriveBy.setTitleColor(UIColor(named:"tabBarNotSelectedColor")!, for: .normal)
        self.isDepartSelected = true
        
    }
    
    @objc private func onArriveBy() {
       
            btnDepartAt.setTitleColor(UIColor(named:"tabBarNotSelectedColor")!, for: .normal)
            
            btnArriveBy.setTitleColor(UIColor(named:"tooltipsAmenityNaviBtnColor")!, for: .normal)
        self.isDepartSelected = false
        
    }
    
    @objc private func onCancel() {
        if let callback = cancelDatePicker {
            
            callback(true)
        }
    }
    
    @objc private func onNext() {
        if let callback = nextDatePicker {
            
            let string = datePicker.date.formatted
            
            let formattedDateString = " \(DateUtils.shared.getTimeDisplay(dateFormat: Date.formatHHmmdMMME, date: datePicker.date))"
            
            let date = Date.getDate(str: string)
            print(date)
            let timerIntervalDate = Date.getDate(str: string).timeIntervalSince1970
            
            callback(formattedDateString,timerIntervalDate)
        }
    }
    
    @objc func handleDatePicker(_ datePicker: UIDatePicker) {
        // Intentionally not implemented
    }
    
    private func setupLayout() {
        
    
        btnDepartAt.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(42)
            make.leading.equalToSuperview().offset(94)
            make.width.equalTo(88)
        }
        
        btnArriveBy.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(42)
            make.leading.equalTo(btnDepartAt.snp.trailing).offset(52)
        }
        
        pickerBackgroundView.snp.makeConstraints { make in
            make.top.equalTo(btnDepartAt.snp.bottom).offset(5)
            make.leading.equalToSuperview()
            make.trailing.equalToSuperview()
            make.height.equalTo(200)
        }
        
        datePicker.snp.makeConstraints { make in
            make.leading.equalToSuperview()
            make.trailing.equalToSuperview()
            
        }
        
        lineView.snp.makeConstraints { make in
            
            make.top.equalTo(pickerBackgroundView.snp.bottom).offset(4)
            make.leading.equalToSuperview()
            make.trailing.equalToSuperview()
            make.height.equalTo(1.5)
        }
        
        datePickerCancel.snp.makeConstraints { make in
            make.top.equalTo(lineView.snp.bottom).offset(26)
            make.leading.equalToSuperview().offset(24)
            make.width.equalTo(136)
            make.height.equalTo(buttonHeight)
        }
        
        btnNext.snp.makeConstraints { make in
            make.leading.equalTo(datePickerCancel.snp.trailing).offset(23)
            make.trailing.equalToSuperview().offset(-23)
            make.centerY.equalTo(datePickerCancel.snp.centerY)
            make.height.equalTo(buttonHeight)
        }
        
        containerView.snp.makeConstraints { (make) in
            //make.top.equalToSuperview()
            make.bottom.equalToSuperview()
            make.leading.equalToSuperview()
            make.height.equalTo(popupHeight)
            make.trailing.equalToSuperview()
        }
    }
}

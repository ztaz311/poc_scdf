//
//  BreezeToast.swift
//  Breeze
//
//  Created by Zhou Hao on 24/1/22.
//

import UIKit

struct BreezeToastAppearance {
    let backgroundColor: UIColor
    let borderColor: UIColor
    let cornerRadius: CGFloat
    let primaryIconName: String
    let preferredWidth: CGFloat
    let actionTextColor: UIColor
}

class BreezeToast: UIView {
    // MARK: - Constants
    private let topOffset: CGFloat = 11
    private let bottomOffset: CGFloat = 16
    private let yPadding: CGFloat = 2
    private let xPadding: CGFloat = 14
    private let gap: CGFloat = 2

    var onAction: (() -> Void)? {
        didSet {
            if onAction != nil {
                setupLayout()
            }
        }
    }
    var isAutoDismiss = true
    var onClose: (() -> Void)?
    var actionText: String = "" {
        didSet {
            if !actionText.isEmpty {
                btnAction.setTitle(actionText, for: .normal)
            }
        }
    }

    // MARK: - Properties
    private var appearance: BreezeToastAppearance!
    private var autoDismissDuration = 0
    private var timer: Timer?
    
    private(set) lazy var containerView: UIView = {
        let view = UIView()
        view.backgroundColor = appearance.backgroundColor
        view.layer.cornerRadius = appearance.cornerRadius
        addSubview(view)
        return view
    }()
    
    private lazy var icon: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: appearance.primaryIconName)!)
        containerView.addSubview(imageView)
        return imageView
    }()

    private lazy var lblTitle: UILabel  =  {
        let label = UILabel()
        label.font = UIFont.toastTitleTextFont()
        label.preferredMaxLayoutWidth = appearance.preferredWidth - 130
        label.textAlignment = .left
        label.text = ""
        label.numberOfLines = 0
        label.textColor = UIColor(named: "toastTitleColor")!
        containerView.addSubview(label)
        return label
    }()
    
    private lazy var lblMessage: UILabel  =  {
        let label = UILabel()
        label.font = UIFont.toastMessageTextFont()
        label.preferredMaxLayoutWidth = appearance.preferredWidth - 100
        label.textAlignment = .left
        label.text = ""
        label.numberOfLines = 0
        label.textColor = UIColor(named: "toastMessageColor")!
        containerView.addSubview(label)
        return label
    }()
    
    private lazy var btnClose: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "toastClose"), for: .normal)
        button.addTarget(self, action: #selector(onCloseClicked), for: .touchUpInside)
        containerView.addSubview(button)
        return button
    }()

    private lazy var btnAction: UIButton = {
        let button = UIButton()
        button.setTitle(actionText, for: .normal)
        button.setTitleColor(appearance.actionTextColor, for: .normal)
        button.addTarget(self, action: #selector(onActionClicked), for: .touchUpInside)
        button.titleLabel?.font = UIFont.toastMessageTextFont()
        button.contentHorizontalAlignment = .left;
        containerView.addSubview(button)
        return button
    }()

    var yOffset: CGFloat {
        get {
            return self.frame.origin.y
        }
        set {
            let originalFrame = self.frame
            let x = (UIScreen.main.bounds.width - originalFrame.width)/2.0
            self.frame = CGRect(x: x, y: newValue, width: originalFrame.width, height: originalFrame.height)
        }
    }
    
    override var center: CGPoint {
        set {
            var newCenter = newValue
            newCenter.y -= bounds.midY
            super.center = newCenter
        }
        get {
            return super.center
        }
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /// onAction - we can define any action and the caller will decide the logic
    init(appearance: BreezeToastAppearance, onClose: (() -> Void)?, actionTitle: String?, onAction: (() -> Void)?) {
        super.init(frame: .zero)
        
        self.backgroundColor = .yellow
        self.onClose = onClose
        self.actionText = actionTitle ?? ""
        self.onAction = onAction
        self.appearance = appearance
        
        setupLayout()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = appearance.cornerRadius
        containerView.layer.cornerRadius = appearance.cornerRadius
        layer.masksToBounds = true
        containerView.layer.borderColor = appearance.borderColor.cgColor
        containerView.layer.borderWidth = 1
        
//        if showShadow {
//            showShadow()
//        }
    }
    
    override func sizeThatFits(_ size: CGSize) -> CGSize {
                
        let lblTitleSize = lblTitle.intrinsicContentSize
        let lblMessageSize = lblMessage.intrinsicContentSize    // TODO: the intrinsicContentSize seems not accurate for attributedString
        let lblActionH = onAction != nil ? bottomOffset + 20 : bottomOffset
                        
        let h = topOffset + lblTitleSize.height + yPadding + lblMessageSize.height + gap + lblActionH

        return CGSize(width: appearance.preferredWidth, height: h)
    }

    func replaceIcon(_ image: UIImage) {
        self.icon.image = image
    }
    
    @objc private func onCloseClicked() {
        dismiss(animated: true)
    }

    @objc private func onActionClicked() {
        if let callback = onAction {
            isAutoDismiss = false
            callback()
        }
    }
    
    @objc private func onTimer() {
        dismiss(animated: true)
    }
    
    private func setupLayout() {
        icon.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(21)
            make.top.equalToSuperview().offset(13)
            make.height.equalTo(45)
            make.width.equalTo(45)
        }

        lblTitle.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(topOffset)
            make.leading.equalTo(icon.snp.trailing).offset(xPadding)
            make.trailing.equalTo(btnClose.snp.leading).offset(-xPadding)
        }
        
        btnClose.snp.makeConstraints { make in
            make.centerY.equalTo(lblTitle.snp.centerY)
            make.trailing.equalToSuperview().offset(-xPadding)
            make.height.equalTo(16)
            make.width.equalTo(16)
        }

        lblMessage.snp.makeConstraints { (make) in
            make.top.equalTo(lblTitle.snp.bottom).offset(yPadding)
            make.leading.equalTo(lblTitle.snp.leading)
            make.trailing.equalToSuperview().offset(-xPadding)
        }
        
        if onAction != nil {
            btnAction.snp.makeConstraints { make in
                make.top.equalTo(lblMessage.snp.bottom).offset(gap)
                make.leading.equalTo(lblMessage.snp.leading)
                make.trailing.equalToSuperview().offset(-xPadding)
            }
        }
        
        containerView.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.bottom.equalToSuperview()
            make.leading.equalToSuperview()
            make.width.equalTo(appearance.preferredWidth)
            make.trailing.equalToSuperview()
        }
    }
    
    func show(in view: UIView, title: String, message: String, animated: Bool, dismissInSeconds: Int = Values.toastAutoDismissInSeconds, inHtml: Bool = false) {
        if inHtml {
            let data = Data(message.utf8)
            
            if let attributedString = try? NSMutableAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil) {
                self.lblMessage.attributedText = attributedString
            }
        } else {
            self.lblMessage.text = message
        }
        
        self.lblTitle.text = title
    
        view.addSubview(self)
        view.bringSubviewToFront(self)
        self.sizeToFit()

        if dismissInSeconds > 0 {
            timer = Timer.scheduledTimer(timeInterval: TimeInterval(dismissInSeconds), target: self, selector: #selector(onTimer), userInfo: nil, repeats: false)
        }
        if animated {
            alpha = 0

            UIView.animate(withDuration: 0.2) { [weak self] in
                guard let self = self else { return }

                self.alpha = 1
//                self.delegate?.onPresented(toolTips: self)
            }
        } else {
//            self.delegate?.onPresented(toolTips: self)
        }
    }
    
    func updateTitle(_ title: String, message: String) {
        self.lblTitle.text = title
        self.lblMessage.text = message
        startTimer()
    }
    
    private func startTimer() {
        timer?.invalidate()
        timer = nil
        timer = Timer.scheduledTimer(timeInterval: TimeInterval(Values.toastAutoDismissInSeconds), target: self, selector: #selector(onTimer), userInfo: nil, repeats: false)
    }

    func dismiss(animated: Bool) {
        timer?.invalidate()
        timer = nil

        onClose?()
        
        if (superview != nil) {
            if animated {
                UIView.animate(withDuration: 0.2, animations: { [weak self] in
                    guard let self = self else { return }
                    self.alpha = 0
                }, completion: { [weak self] _ in
                    guard let self = self else { return }
//                    self.delegate?.onDismissed(toolTips: self)
                    self.removeFromSuperview()
                })
            } else {
//                self.delegate?.onDismissed(toolTips: self)
                removeFromSuperview()
            }
        }
    }
    
}


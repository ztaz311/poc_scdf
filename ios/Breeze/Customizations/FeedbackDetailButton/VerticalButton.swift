//
//  FeedbackDetailButton.swift
//  Breeze
//
//  Created by Tuyen, Le Xuan  on 12/04/2022.
//

import UIKit

class VerticalButton: UIButton {
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentHorizontalAlignment = .center
        self.titleLabel?.textAlignment = .center
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        centerButtonImageAndTitle()
    }

    private func centerButtonImageAndTitle() {
        let titleSize = self.titleLabel?.frame.size ?? .zero
        let imageSize = self.imageView?.frame.size  ?? .zero
        let leftImage = (self.bounds.size.width - imageSize.width) / 2
        self.imageView?.frame = CGRect(x: leftImage, y: 0, width: imageSize.width, height: imageSize.height)
        let spacing: CGFloat = 5.0
        self.titleEdgeInsets = UIEdgeInsets(top: imageSize.height / 2 + titleSize.height + spacing, left: -imageSize.width, bottom: 0, right: 0)
     }
}

//
//  UserMenuButton.swift
//  Breeze
//
//  Created by Zhou Hao on 23/2/21.
//

import UIKit

class UserMenuButton: UIButton {
    
    init(buttonSize: CGFloat) {
        
        super.init(frame: CGRect(x: 0, y: 0, width: buttonSize, height: buttonSize))
        self.setImage(Images.menuImage, for: .normal)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

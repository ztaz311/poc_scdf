//
//  FeedbackButton.swift
//  Breeze
//
//  Created by Malou Mendoza on 1/3/21.
//

import UIKit

class FeedbackButton: UIButton {
    
    init(buttonSize: CGFloat) {
        
        super.init(frame: CGRect(x: 0, y: 0, width: buttonSize, height: buttonSize))
        self.setImage(Images.feedbackImage, for: .normal)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}



class NewFeedbackButton: UIButton {
    
    init(buttonSize: CGFloat) {
        
        super.init(frame: CGRect(x: 0, y: 0, width: buttonSize, height: buttonSize))
        self.setImage(Images.feedbackBtn, for: .normal)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


class ToggleParkingButton: UIButton {
    
    init(buttonSize: CGFloat) {
        
        super.init(frame: CGRect(x: 0, y: 0, width: buttonSize, height: buttonSize))
        self.setImage(Images.toggleParkingBtn, for: .normal)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var isHighlighted: Bool {
        didSet {
            super.isHighlighted = false
        }
    }
}

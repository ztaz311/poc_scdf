//
//  NudgeTooltip.swift
//  Breeze
//
//  Created by Tuyen, Le Xuan  on 20/07/2022.
//

import UIKit
import SnapKit
import CoreLocation

/// Currently only has two values
enum NudgeArrowDirection {
//    case right(CGFloat) // only need y offset
//    case down(CGFloat) // only need x offset
    case right
    case down
}

struct NudgeStep {
    let title, subTitle: String
    let offsetX: CGFloat
    let offsetY: CGFloat
    let arrowDirection: NudgeArrowDirection
}

final class NudgeTooltipView: ToolTipsView {
    
    private var popupWidth: CGFloat = 227
    private let buttonHeight: CGFloat = 28
    private let buttonWidth: CGFloat = 147
    private let yPadding: CGFloat = 10
    private var steps: [NudgeStep]!
    private(set) var currentStep = 0
    private var isLayoutSet = false
    
    let xPadding: CGFloat = 12
    private let sizeImageView: CGFloat = 16
    var tipOriginX: CGFloat = 20
    var sizeLabel: CGFloat {
        return popupWidth - xPadding * 3 - sizeImageView - tipHeight
    }
    var onProgress: ((_ complete: Bool) -> Void)?
    var onClose: (() ->Void)?
    
    private lazy var btnClose: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "ic_nudge_close"), for: .normal)
        button.backgroundColor = .clear
        button.addTarget(self, action: #selector(onCloseClicked), for: .touchUpInside)
        containerView.addSubview(button)
        return button
    }()
    
    private lazy var lblName: UILabel  =  {
        let label = UILabel()
        label.font = UIFont.nudgeToolTipsTextFont()
        label.preferredMaxLayoutWidth = sizeLabel
        label.textAlignment = .left
        label.text = ""
        label.numberOfLines = 0
        label.textColor = UIColor(named: "tooltipsNudgeTextColor")!
        containerView.addSubview(label)
        return label
    }()

    private lazy var lblAddress: UILabel  =  {
        let label = UILabel()
        label.font = UIFont.nudgeToolTipsAddressFont()
        label.preferredMaxLayoutWidth = sizeLabel
        label.textAlignment = .left
        label.text = ""
        label.numberOfLines = 0
        label.textColor = UIColor(named: "tooltipsNudgeTextColor")!
        containerView.addSubview(label)
        return label
    }()

    private lazy var lblProgress: UILabel  =  {
        let label = UILabel()
        label.font = UIFont.nudgeToolTipsProgressFont()
        label.preferredMaxLayoutWidth = sizeLabel
        label.textAlignment = .center
        label.text = "1/3"
        label.numberOfLines = 1
        label.textColor = UIColor(named: "tooltipsNudgeProgressColor")!
        containerView.addSubview(label)
        return label
    }()

    private lazy var btnProgress: UIButton = {
        let button = UIButton()
        button.setTitle("Next", for: .normal)
        button.setTitleColor(UIColor(named:"tooltipsNudgeProgressColor")!, for: .normal)
        button.titleLabel?.font = UIFont.amenityToolTipsTextFont()
        button.backgroundColor = .clear
        button.addTarget(self, action: #selector(onProgressClicked), for: .touchUpInside)
        containerView.addSubview(button)
        return button
    }()
    
    init(name: String, address: String, steps: [NudgeStep]) {
        super.init(frame: .zero)
        assert(steps.count > 0, "At least one step.")
        
        self.backgroundColor = .clear
        self.bgColor = UIColor(named: "tooltipsNudgeBackgroundColor")!
        self.tipHeight = 12.0
        self.tipWidth = 20.0
        self.cornerRadius = 16.0
        
        lblName.text = name
        lblAddress.text = address
        
        self.steps = steps
        self.showShadow = false
        self.updateProgressUI()
    }

    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    override func draw(_ rect: CGRect) {
        let rect = containerView.bounds
        // Draw the pointed tip at the bottom.
        let fillColor: UIColor = bgColor
        
        if steps[currentStep].arrowDirection == .down {
            
            let arrowOriginX = steps[currentStep].offsetX
            let arrowOriginY = frame.size.height - tipHeight - 1
            let coordinate1 = CGPoint(x: arrowOriginX, y: arrowOriginY)
            let coordinate2 = CGPoint(x: arrowOriginX + tipWidth, y: arrowOriginY)
            let coordinate3 = CGPoint(x: arrowOriginX+(tipWidth/2), y: self.frame.size.height)
            
            let currentContext = UIGraphicsGetCurrentContext()!
            
            let tipPath = CGMutablePath()
            tipPath.move(to: coordinate1)
            tipPath.addLine(to: coordinate2)
            tipPath.addLine(to: coordinate3)
            tipPath.closeSubpath()
            
            fillColor.setFill()
            currentContext.addPath(tipPath)
            currentContext.fillPath()
        }
        else{
            
            let tipTopPoint = CGPoint(x: rect.size.width, y: tipOriginX)
            let tipBottomPoint = CGPoint(x: rect.size.width, y: tipOriginX + tipWidth)
            let tipRightPoint = CGPoint(x: rect.size.width + tipHeight - 1, y: 30)
            
            let currentContext = UIGraphicsGetCurrentContext()!
            
            let tipPath = CGMutablePath()
            tipPath.move(to: tipTopPoint)
            tipPath.addLine(to: tipRightPoint)
            tipPath.addLine(to: tipBottomPoint)
            tipPath.closeSubpath()
            
            fillColor.setFill()
            currentContext.addPath(tipPath)
            currentContext.fillPath()
        }
        
    }
    
    override func sizeThatFits(_ size: CGSize) -> CGSize {
                
        let lblNameSize = lblName.intrinsicContentSize
        let lblAddrSize = lblAddress.intrinsicContentSize
//        let lblAddrHeight = lblAddress.text!.height(withConstrainedWidth: popupWidth - 2*xPadding, font: lblAddress.font)
        let lblProgressSize = lblProgress.intrinsicContentSize
                
        let h = yPadding + lblNameSize.height + yPadding + lblAddrSize.height + yPadding + lblProgressSize.height + yPadding + 2 + (steps[currentStep].arrowDirection == .down ? tipHeight : 0)
        
        return CGSize(width: popupWidth, height: h)
    }

    internal override func setupLayout() {
        
        btnClose.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(yPadding)
            make.trailing.equalToSuperview().offset(-xPadding)
            make.width.equalTo(sizeImageView)
            make.height.equalTo(sizeImageView)
        }

        lblName.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(yPadding)
            make.leading.equalToSuperview().offset(xPadding)
            make.trailing.equalTo(btnClose.snp.leading).offset(-xPadding)
        }
                
        lblAddress.snp.makeConstraints { (make) in
            make.top.equalTo(lblName.snp.bottom).offset(yPadding)
            make.leading.equalToSuperview().offset(xPadding)
            make.trailing.equalTo(lblName.snp.trailing)
        }
        
        lblProgress.snp.makeConstraints { make in
            make.top.equalTo(lblAddress.snp.bottom).offset(yPadding)
            make.height.equalTo(20)
            make.centerX.equalToSuperview()
        }
        
        btnProgress.snp.makeConstraints { make in
            make.top.equalTo(lblAddress.snp.bottom).offset(yPadding)
            make.height.equalTo(20)
            make.trailing.equalToSuperview().offset(-xPadding)
        }
        
        if isLayoutSet {
            containerView.snp.updateConstraints { make in
                self.updateContainerMaker(make)
            }
        } else {
            containerView.snp.makeConstraints { (make) in
                make.top.equalToSuperview()
                make.leading.equalToSuperview()
                make.width.equalTo(popupWidth)
                self.updateContainerMaker(make)
            }
        }
        
        isLayoutSet = true
    }
    
    private func updateContainerMaker(_ make: ConstraintMaker) {
        if steps[currentStep].arrowDirection == .down {
            make.bottom.equalToSuperview().offset(-tipHeight)
        } else {
            make.bottom.equalToSuperview()
        }
        if steps[currentStep].arrowDirection == .right {
            make.trailing.equalToSuperview().offset(-tipHeight)
        } else {
            make.trailing.equalToSuperview()
        }
    }
    
    @objc private func onCloseClicked() {
        onClose?()
    }
        
    @objc private func onProgressClicked() {
        guard currentStep < steps.count else { return }
        
        currentStep += 1
        updateProgressUI()
        onProgress?(currentStep == steps.count)
    }

    private func updateProgressUI() {
        guard currentStep < steps.count else { return }
        
        self.lblName.text = steps[currentStep].title
        self.lblAddress.text = steps[currentStep].subTitle
        self.lblProgress.text = "\(currentStep + 1)/\(steps.count)"
        let btnText = currentStep == steps.count - 1 ? "Done" : "Next"
        self.btnProgress.setTitle(btnText, for: .normal)
        sizeToFit()
        setupLayout()
        setNeedsDisplay()
    }
    
}

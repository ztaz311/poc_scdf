//
//  CarparkToolTipsView.swift
//  Breeze
//
//  Created by Zhou Hao on 24/12/21.
//

import UIKit
import CoreLocation

class CarparkToolTipsView: ToolTipsView {

    // MARK: - Constants
    private var popupWidth: CGFloat = 169
    private let xPadding: CGFloat = 10
    private let yPadding: CGFloat = 8
    private let gap: CGFloat = 12 // gap between labels and button
    private let buttonWidth: CGFloat = 145.0
    private let buttonHeight: CGFloat = 28.0
    private let labelWidth: CGFloat = 65.0
    private let parkingInfoHeight: CGFloat = 71
    private var hasParkingAvailable: Bool = false
    
    var availableLots: Int?
    
    // MARK: - Public properties
    var onCarparkSelectedV2: ((_ carpark: Carpark, _ routeablePoint: RoutablePointModel?) -> Void)?
    var onMoreInfo: ((_ carpark: Carpark) -> Void)?
    var onCalculate: ((_ carpark: Carpark) -> Void)?

    override var tooltipCoordinate: CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: carpark.lat ?? 0, longitude: carpark.long ?? 0)
    }
    
    override var tooltipId: String {
        return carpark.id ?? ""
    }

    // MARK: - Private properties
    private var carpark: Carpark!
    private var routeablePoint: RoutablePointModel?
    private var isCalculating = false
    private var isPinLocation = false
    
    var shareViaLocation: (() -> Void)?
    
    var inviteLocation: (() -> Void)?
    
    override func needShowParking() -> Bool {
        return hasParkingAvailable
    }
    
    // vouch icon
    private lazy var imgVoucher: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "voucherIcon"))
        containerView.addSubview(imageView)
        return imageView
    }()
    
    private lazy var voucherLabel: UILabel  =  {
        let label = UILabel()
        label.font = UIFont.amenityToolTipsERPTextFont()
        label.preferredMaxLayoutWidth = 90
        label.textAlignment = .left
        label.numberOfLines = 1
        label.textColor = UIColor(named: "tooltipsVoucherNameColor")
        containerView.addSubview(label)
        return label
    }()

    // such as Cheapest etc
    private lazy var lblType: UILabel  =  {
        let label = UILabel()
        label.font = UIFont.carparkTypeFont()
        label.preferredMaxLayoutWidth = popupWidth - 2 * xPadding
        label.textAlignment = .center
        label.numberOfLines = 1
        label.textColor = UIColor(named: "tooltipsCarparkTypeColor")
        label.backgroundColor = UIColor(named: "tooltipsCarparkTypeBGColor")
        label.layer.cornerRadius = 8
        label.layer.borderColor = UIColor(named: "tooltipsCarparkTypeColor")?.cgColor ?? UIColor.green.cgColor
        label.layer.masksToBounds = true
        label.layer.borderWidth = 1
        containerView.addSubview(label)
        return label
    }()

    // fully seasonal parking
    private lazy var lblSeasonInfo: UILabel = {
        let label = UILabel()
        label.font = UIFont.carparkTypeFont()
        label.textColor = UIColor(named: "tooltipsCarparkSeasonColor")
        label.preferredMaxLayoutWidth = popupWidth - 2 * xPadding
        label.textAlignment = .left
        label.numberOfLines = 0
        containerView.addSubview(label)
        return label
    }()

    // customer parking
    private lazy var lblCustomerInfo: UILabel = {
        let label = UILabel()
        label.font = UIFont.carparkTypeFont()
        label.textColor = UIColor(named: "tooltipsCarparkSeasonColor")
        label.preferredMaxLayoutWidth = popupWidth - 2 * xPadding
        label.textAlignment = .left
        label.numberOfLines = 0
        containerView.addSubview(label)
        return label
    }()

    // See More button
    private lazy var btnMoreInfo: UIButton = {
        let button = UIButton()
        button.setTitle("See More", for: .normal)
        button.setTitleColor(UIColor(named:"tooltipsAmenityNaviBtnColor")!, for: .normal)
        button.titleLabel?.font = UIFont.amenityToolTipsTextFont()
        button.backgroundColor = .clear
        button.addTarget(self, action: #selector(onMoreInfoClicked), for: .touchUpInside)
        containerView.addSubview(button)
        return button
    }()
    
    private lazy var lblName: UILabel  =  {
        let label = UILabel()
        label.font = UIFont.amenityToolTipsERPTextFont()
        label.preferredMaxLayoutWidth = popupWidth - 2 * xPadding
        label.textAlignment = .left
        label.numberOfLines = 0
        label.textColor = UIColor(named: "tooltipsAmenityNameColor")
        containerView.addSubview(label)
        return label
    }()
    
    private lazy var lblLocationPin: UILabel  =  {
        let label = UILabel()
        label.font = UIFont.amenityToolTipsERPTextFont()
        label.preferredMaxLayoutWidth = popupWidth - 2 * xPadding
        label.textAlignment = .left
        label.numberOfLines = 0
        label.textColor = UIColor(named: "tooltipsAmenityNameColor")
        containerView.addSubview(label)
        return label
    }()
    
    private lazy var imgLotsInfo: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "parkingLotsIcon"))
        containerView.addSubview(imageView)
        return imageView
    }()

    private lazy var lblLotsLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.availableCarparkLotsLabelFont()
        label.textColor = UIColor(named: "tooltipsERPLableColor")
        label.textAlignment = .left
        label.text = "Avl lots"
        containerView.addSubview(label)
        return label
    }()

    private lazy var lblLotsInfo: UILabel = {
        let label = UILabel()
        label.font = UIFont.availableCarparkLotsFont()
        label.textColor = UIColor(named:"tooltipsAmenityNameColor")
        label.textAlignment = .right
        containerView.addSubview(label)
        return label
    }()
    
    private lazy var imgCost: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "parkingCostIcon"))
        containerView.addSubview(imageView)
        return imageView
    }()
    
    private lazy var lblCostLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.availableCarparkLotsLabelFont()
        label.textColor = UIColor(named: "tooltipsERPLableColor")
        label.textAlignment = .left
        label.text = "Current"
        containerView.addSubview(label)
        return label
    }()

    private lazy var lblCost: UILabel = {
        let label = UILabel()
        label.font = UIFont.availableCarparkLotsFont()
        label.textColor = UIColor(named:"tooltipsAmenityNameColor")
        label.textAlignment = .right
        containerView.addSubview(label)
        return label
    }()

    private lazy var lblNextHourCostLable: UILabel = {
        let label = UILabel()
        label.font = UIFont.availableCarparkLotsLabelFont()
        label.textColor = UIColor(named: "tooltipsERPLableColor")
        label.textAlignment = .left
        label.text = "Next hour"
        containerView.addSubview(label)
        return label
    }()

    private lazy var lblNextHourCost: UILabel = {
        let label = UILabel()
        label.font = UIFont.availableCarparkLotsFont()
        label.textColor = UIColor(named:"tooltipsAmenityNameColor")
        label.textAlignment = .right
        containerView.addSubview(label)
        label.text = ""
        return label
    }()
    
    
    //  MARK: BREEZE2-1350 Remove “Calculate Fee” CTA button on tooltip
    private lazy var btnCalculate: UIButton = {
        let button = UIButton()
        button.setTitle("Share", for: .normal)
        button.setTitleColor(UIColor(named:"tooltipsAmenityNaviBtnColor")!, for: .normal)
        button.titleLabel?.font = UIFont.amenityToolTipsTextFont()
        button.backgroundColor = .clear
        button.layer.cornerRadius = buttonHeight / 2
        button.layer.borderWidth = 0.5
        button.layer.borderColor = UIColor(named:"tooltipsAmenityNaviBtnColor")?.cgColor ?? UIColor.purple.cgColor
        button.addTarget(self, action: #selector(onShareViaLocation), for: .touchUpInside)
        containerView.addSubview(button)
        return button
    }()
    
    
    private lazy var btnInvite: UIButton = {
        let button = UIButton()
        button.setTitle("Invite", for: .normal)
        button.setTitleColor(UIColor(named:"tooltipsAmenityNaviBtnColor")!, for: .normal)
        button.titleLabel?.font = UIFont.amenityToolTipsTextFont()
        button.backgroundColor = .clear
        button.layer.cornerRadius = buttonHeight / 2
        button.layer.borderWidth = 0.5
        button.layer.borderColor = UIColor(named:"tooltipsAmenityNaviBtnColor")?.cgColor ?? UIColor.purple.cgColor
        button.addTarget(self, action: #selector(onInviteLocation), for: .touchUpInside)
        containerView.addSubview(button)
        return button
    }()

    
    
    private lazy var btnNavigate: UIButton = {
        let button = UIButton()
        button.backgroundColor = UIColor(named:"tooltipsAmenityNaviBtnColor")!
        button.setTitle("Navigate Here", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = UIFont.amenityToolTipsTextFont()
        button.addTarget(self, action: #selector(onNavigationHere), for: .touchUpInside)
        containerView.addSubview(button)
        return button
    }()
    
    //  MARK: Carpark availability
    
    private lazy var parkingStatusContentView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(hexString: "F5EEFC", alpha: 1)
        view.layer.cornerRadius = 8
        parkingAvailability.addSubview(view)
        return view
    }()
    
    private lazy var parkingIcon: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "carpark-availability-parking-icon1"))
        parkingStatusContentView.addSubview(imageView)
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var parkingAvailableStatusLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.parkingAvailableTextFont()
        label.textColor = UIColor(hexString: "222638", alpha: 1)
        label.textAlignment = .left
        parkingStatusContentView.addSubview(label)
        label.text = ""
        return label
    }()
    
    private lazy var parkingTriangleIcon: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "carpark-availability-triangle"))
        parkingAvailability.addSubview(imageView)
        return imageView
    }()
    
    private lazy var parkingUpdatedTimeLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.parkingAvailableUpdateTimeFont()
        label.textColor = .white
        label.textAlignment = .center
        parkingAvailability.addSubview(label)
        label.text = ""
        return label
    }()
    

    @objc private func onCalculateFee() {
        if let callback = onCalculate {
            callback(carpark)
        }
    }
    
    @objc private func onNavigationHere() {
        if let callback = onCarparkSelectedV2 {
            
            callback(carpark,routeablePoint)
        }
    }
    
    @objc private func onMoreInfoClicked() {
        if let callback = onMoreInfo {
            callback(carpark)
        }
    }
    
    
    @objc private func onShareViaLocation() {
//        if let callback = shareViaLocation {
//            callback(address)
//        }
        
        shareViaLocation?()
//        dismiss(animated: true)
    }
    
    
    @objc func onInviteLocation() {
        inviteLocation?()
//        dismiss(animated: true)
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Home.UserClick.tooltip_carpark_invite, screenName: screenName)
        
    }
    
    init(carpark: Carpark, calculateFee: Bool, routeablePoint: RoutablePointModel?) {
        super.init(frame: .zero)
        
        self.routeablePoint = routeablePoint
//        if carpark.parkingType == Carpark.typeCustomer {
//            popupWidth = 180
//        }
        self.carpark = carpark
        self.hasParkingAvailable = ((self.carpark.hasAvailabilityCS ?? false) && self.carpark.currentParkingStatus != .notAvailable)
        self.isCalculating = calculateFee
        self.cornerRadius = 16.0
        self.tipHeight = 12.0
        self.lblLocationPin.isHidden = true
        
        self.backgroundColor = .clear
        self.bgColor = UIColor(named: "tooltipsAmenityBackgroundColor")!
        self.containerView.backgroundColor = self.bgColor
        
        if carpark.hasVoucher {
            let amount = carpark.amount
            if Int(exactly: amount) != nil {
                voucherLabel.text =  String(format: "$%.0f voucher", amount)
            } else {
                voucherLabel.text =  String(format: "$%.1f voucher", amount)
            }
        } else {
            voucherLabel.isHidden = true
            imgVoucher.isHidden = true
        }
        
        if carpark.isCheapest ?? false {
            lblType.text = "Cheapest"
        } else {
            lblType.isHidden = true
        }
        lblName.text = carpark.name
        
        if let name = carpark.getRouteablePointName(routeablePoint), !name.isEmpty {
            self.isPinLocation = true
            lblLocationPin.text = name
            lblLocationPin.isHidden = false
        }
        
        let availableLot = carpark.availablelot?.intValue ?? 0
        self.availableLots = availableLot
        if(availableLots != -1) {
            lblLotsInfo.text = "\(availableLot)"
        }

        if let rate = carpark.currentHrRate?.oneHrRate {
//            lblCost.text = String(format: "$%.2f", rate)
            setupPriceLabel(textLabel: lblCost, rate: rate)
        } else {
            lblCost.text = "No Info"
        }
        
        if let nextRate = carpark.nextHrRate?.oneHrRate {
//            lblNextHourCost.text = String(format: "$%.2f", nextRate)
            setupPriceLabel(textLabel: lblNextHourCost, rate: nextRate)
        } else {
            lblNextHourCost.text = "No Info"
        }

//        btnCalculate.isHidden = !calculateFee
        
        self.showShadow = true
//        setupLayout()
        
        //  Handle parking available stype here
        updateParkingAvailableStyle()
    }
    
    private func updateParkingAvailableStyle() {
        
        parkingAvailability.isHidden = !hasParkingAvailable
        parkingStatusContentView.isHidden = !hasParkingAvailable
        parkingTriangleIcon.isHidden = !hasParkingAvailable
        parkingUpdatedTimeLabel.isHidden = !hasParkingAvailable
        
        parkingUpdatedTimeLabel.text = carpark.getCSUpdatedTimeDesc()
        parkingAvailableStatusLabel.text = carpark.getCSTitle()
        
        let bgColorHexString: String = carpark.getThemeColor()
        var carparkIconName: String = "carpark-availability-parking-icon1"
        
        switch carpark.currentParkingStatus {
        case .availableLots:
            carparkIconName = "carpark-availability-parking-icon1"
        case .fewAvailableLots:
            carparkIconName = "carpark-availability-parking-icon2"
        case .fullParking:
            carparkIconName = "carpark-availability-parking-icon3"
        case .notAvailable:
            print("Do nothing")
        }
        parkingAvailability.backgroundColor = UIColor(hexString: bgColorHexString, alpha: 1)
        parkingIcon.image = UIImage(named: carparkIconName)
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupPriceLabel(textLabel: UILabel, rate: Double) {
        if Int(rate) == -2 {
            textLabel.text = "Season"
            textLabel.textColor = UIColor(named: "tooltipsCarparkSeasonColor")
        } else if Int(rate) == -1 {
            textLabel.text = "No Info"
        } else if rate < 0.00001  {
            textLabel.text = "Free"
            textLabel.textColor = UIColor(named: "tooltipsCarparkFreeColor")
        }
        else {
            textLabel.text = String(format: "$%.2f", rate)
        }
    }
    
    internal override func setupLayout() {
                
        var nextYOffset = yPadding
//        btnMoreInfo.snp.makeConstraints { make in
//            make.top.equalToSuperview().offset(yPadding)
//            make.trailing.equalToSuperview().offset(-xPadding)
//        }

        if carpark.hasVoucher {
            imgVoucher.snp.makeConstraints { make in
                make.width.equalTo(26)
                make.height.equalTo(26)
                make.leading.equalToSuperview().offset(xPadding)
                make.top.equalToSuperview().offset(nextYOffset)
            }
            
            voucherLabel.snp.makeConstraints { make in
                make.leading.equalTo(imgVoucher.snp.trailing).offset(4)
                make.trailing.equalToSuperview().offset(-xPadding)
                make.centerY.equalTo(imgVoucher.snp.centerY)
            }
            
            nextYOffset += (26 + yPadding)
        }
        
        if carpark.isCheapest ?? false {
            lblType.snp.makeConstraints { (make) in
                make.top.equalToSuperview().offset(nextYOffset)
                make.leading.equalToSuperview().offset(xPadding)
                make.width.equalTo(80)
                make.height.equalTo(21)
                nextYOffset += (21 + yPadding)
            }
        }
                
        lblName.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().offset(xPadding)
            make.top.equalToSuperview().offset(nextYOffset)
            let leftPadding = onSaveAction == nil ? xPadding : xPadding + 24
            make.trailing.equalToSuperview().offset(-leftPadding)
        }
        
        if isPinLocation {
            lblLocationPin.snp.makeConstraints { (make) in
                make.leading.equalToSuperview().offset(xPadding)
                make.top.equalTo(lblName.snp.bottom)
                make.trailing.equalToSuperview().offset(-xPadding)
            }
        }
        
        if carpark.parkingType == Carpark.typeSeason {
            
            lblSeasonInfo.text = "All-day season parking"
            if isPinLocation {
                lblSeasonInfo.snp.makeConstraints { (make) in
                    make.leading.equalToSuperview().offset(xPadding)
                    make.top.equalTo(lblLocationPin.snp.bottom).offset(yPadding)
                    make.trailing.equalToSuperview().offset(-xPadding)
                }
            }else {
                lblSeasonInfo.snp.makeConstraints { (make) in
                    make.leading.equalToSuperview().offset(xPadding)
                    make.top.equalTo(lblName.snp.bottom).offset(yPadding)
                    make.trailing.equalToSuperview().offset(-xPadding)
                }
            }
            
            btnNavigate.snp.makeConstraints { make in
                make.width.equalTo(buttonWidth)
                make.height.equalTo(buttonHeight)
                make.centerX.equalToSuperview()
                make.top.equalTo(lblSeasonInfo.snp.bottom).offset(gap)
            }
            btnMoreInfo.snp.makeConstraints { make in
                make.top.equalTo(btnNavigate.snp.bottom).offset(yPadding)
                make.width.equalTo(buttonWidth)
                make.height.equalTo(buttonHeight)
                make.centerX.equalToSuperview()
                make.bottom.equalToSuperview().offset(-yPadding)    // the last button should set the bottom constraint to make autolayout work
            }

        } else {
            imgCost.snp.makeConstraints { make in
                make.width.equalTo(16)
                make.height.equalTo(16)
                make.leading.equalToSuperview().offset(xPadding)
                if isPinLocation {
                    make.top.equalTo(lblLocationPin.snp.bottom).offset(yPadding)
                }else {
                    make.top.equalTo(lblName.snp.bottom).offset(yPadding)
                }
            }
            
            lblCostLabel.snp.makeConstraints { make in
                make.leading.equalTo(imgCost.snp.trailing).offset(7)
                make.trailing.equalTo(lblCost.snp.leading).offset(7)
                make.centerY.equalTo(imgCost.snp.centerY)
            }

            lblCost.snp.makeConstraints { make in
                make.centerY.equalTo(imgCost.snp.centerY)
                make.trailing.equalToSuperview().offset(-xPadding)
                make.width.equalTo(labelWidth)
            }
            
            if carpark.parkingType == Carpark.typeCustomer {
                lblCustomerInfo.text = "Strictly for customers only."
                lblCustomerInfo.snp.makeConstraints { make in
                    make.leading.equalToSuperview().offset(xPadding)
                    make.top.equalTo(lblCostLabel.snp.bottom).offset(yPadding)
                    make.trailing.equalToSuperview().offset(-xPadding)
                }
                btnNavigate.snp.makeConstraints { make in
                    make.width.equalTo(buttonWidth)
                    make.height.equalTo(buttonHeight)
                    make.centerX.equalToSuperview()
                    make.top.equalTo(lblCustomerInfo.snp.bottom).offset(gap)
                }
                btnMoreInfo.snp.makeConstraints { make in
                    make.top.equalTo(btnNavigate.snp.bottom).offset(yPadding)
                    make.width.equalTo(buttonWidth)
                    make.height.equalTo(buttonHeight)
                    make.centerX.equalToSuperview()
                    make.bottom.equalToSuperview().offset(-yPadding)    // the last button should set the bottom constraint to make autolayout work
                }

            } else {
            
                lblNextHourCostLable.snp.makeConstraints { make in
                    make.leading.equalTo(imgCost.snp.trailing).offset(7)
                    make.trailing.equalTo(lblNextHourCost.snp.leading).offset(7)
                    make.top.equalTo(lblCostLabel.snp.bottom).offset(yPadding)
                }
                
                lblNextHourCost.snp.makeConstraints { make in
                    make.centerY.equalTo(lblNextHourCostLable.snp.centerY)
                    make.trailing.equalToSuperview().offset(-xPadding)
                    make.leading.equalTo(lblNextHourCostLable.snp.trailing).offset(5)
                    make.width.equalTo(labelWidth)
                }

        //        lblCost.snp.makeConstraints { make in
        //            make.centerY.equalTo(imgCost.snp.centerY)
        //            make.trailing.equalToSuperview().offset(-xPadding)
        //            make.width.equalTo(60)
        //        }
                
                if(availableLots == -1) {
                    
                    btnCalculate.snp.makeConstraints { make in
                        make.width.equalTo(buttonWidth)
                        make.height.equalTo(buttonHeight)
                        make.centerX.equalToSuperview()
                        make.top.equalTo(lblNextHourCostLable.snp.bottom).offset(gap)
                    }
                    
                    
                    
                    btnInvite.snp.makeConstraints { make in
                        make.width.equalTo(buttonWidth)
                        make.height.equalTo(buttonHeight)
                        make.centerX.equalToSuperview()
                        make.top.equalTo(btnCalculate.snp.bottom).offset(gap)
                    }

                    
                    btnNavigate.snp.makeConstraints { make in
                        make.width.equalTo(buttonWidth)
                        make.height.equalTo(buttonHeight)
                        make.centerX.equalToSuperview()
                        make.top.equalTo(lblNextHourCostLable.snp.bottom).offset(gap)
                    }
                    
                } else {
                    imgLotsInfo.snp.makeConstraints { make in
                        make.width.equalTo(16)
                        make.height.equalTo(16)
                        make.leading.equalToSuperview().offset(xPadding)
                        make.top.equalTo(lblNextHourCostLable.snp.bottom).offset(yPadding)
                    }

                    lblLotsLabel.snp.makeConstraints { make in
                        make.leading.equalTo(imgLotsInfo.snp.trailing).offset(7)
                        make.trailing.equalTo(lblLotsInfo.snp.leading).offset(7)
                        make.centerY.equalTo(imgLotsInfo.snp.centerY)
                    }
                    
                    lblLotsInfo.snp.makeConstraints { make in
                        make.centerY.equalTo(imgLotsInfo.snp.centerY)
                        make.trailing.equalToSuperview().offset(-xPadding)
                        make.width.equalTo(labelWidth)
                    }
                    btnCalculate.snp.makeConstraints { make in
                        make.width.equalTo(buttonWidth)
                        make.height.equalTo(buttonHeight)
                        make.centerX.equalToSuperview()
                        make.top.equalTo(lblLotsLabel.snp.bottom).offset(gap)
                    }
                    
                    btnInvite.snp.makeConstraints { make in
                        make.width.equalTo(buttonWidth)
                        make.height.equalTo(buttonHeight)
                        make.centerX.equalToSuperview()
                        make.top.equalTo(btnCalculate.snp.bottom).offset(gap)
                    }
                }

//                btnCalculate.snp.makeConstraints { make in
//                    make.width.equalTo(buttonWidth)
//                    make.height.equalTo(buttonHeight)
//                    make.centerX.equalToSuperview()
//                    make.top.equalTo(lblLotsLabel.snp.bottom).offset(gap)
//                }
                btnNavigate.snp.makeConstraints { [weak self] make in
                    guard let self = self else { return }
                    make.width.equalTo(buttonWidth)
                    make.height.equalTo(buttonHeight)
                    make.centerX.equalToSuperview()
//                    if self.isCalculating {
//                        make.top.equalTo(btnCalculate.snp.bottom).offset(yPadding)
//                    } else {
//                        make.top.equalTo(lblLotsLabel.snp.bottom).offset(gap)
//                    }
                    make.top.equalTo(btnInvite.snp.bottom).offset(xPadding)
                }
                btnMoreInfo.snp.makeConstraints { make in
                    make.top.equalTo(btnNavigate.snp.bottom).offset(yPadding)
                    make.width.equalTo(buttonWidth)
                    make.height.equalTo(buttonHeight)
                    make.centerX.equalToSuperview()
                    make.bottom.equalToSuperview().offset(-yPadding)    // the last button should set the bottom constraint to make autolayout work
                }
            }
        }
        
        //  MARK: - Parking available
        parkingStatusContentView.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(8)
            make.leading.equalToSuperview().offset(8)
            make.height.equalTo(36)
            make.trailing.equalToSuperview().offset(-8)
        }
        
        parkingIcon.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(10)
            make.leading.equalToSuperview().offset(8)
            make.height.equalTo(17)
            make.width.equalTo(17)
        }
        
        parkingAvailableStatusLabel.snp.makeConstraints { make in
            make.centerY.equalTo(parkingIcon.snp.centerY)
            make.leading.equalToSuperview().offset(29)
            make.trailing.equalToSuperview().offset(10)
        }
        
        parkingTriangleIcon.snp.makeConstraints { make in
            make.top.equalTo(parkingStatusContentView.snp.bottom).offset(-3)
            make.leading.equalTo(parkingStatusContentView).offset(12)
            make.height.equalTo(10)
            make.width.equalTo(14)
        }
        
        parkingUpdatedTimeLabel.snp.makeConstraints { make in
            make.top.equalTo(parkingStatusContentView.snp.bottom).offset(10)
            make.leading.equalToSuperview().offset(8)
            make.trailing.equalToSuperview().offset(-8)
        }
        
        containerView.snp.makeConstraints { (make) in
            if needShowParking() {
                make.top.equalToSuperview().offset(parkingInfoHeight)
                make.bottom.equalToSuperview()
            }else {
                make.top.equalToSuperview().offset(0)
                make.bottom.equalToSuperview().offset(-tipHeight)
            }
            
            make.leading.equalToSuperview()
            make.width.equalTo(popupWidth)
            make.trailing.equalToSuperview()
        }
        
        parkingAvailability.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.bottom.equalToSuperview().offset(-tipHeight)
            make.leading.equalToSuperview()
            make.width.equalTo(popupWidth)
            make.trailing.equalToSuperview()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        btnNavigate.layer.cornerRadius = buttonHeight / 2
        btnCalculate.layer.cornerRadius = buttonHeight / 2
    }

    override func sizeThatFits(_ size: CGSize) -> CGSize {
        
                
        let typeH = (carpark.isCheapest ?? false) ? 21 + yPadding: 0
        let leftPadding = onSaveAction == nil ? xPadding : xPadding + 24
        let nameHeight = (lblName.text ?? "").height(withConstrainedWidth: popupWidth - xPadding - leftPadding, font: lblName.font)
        let locationPinHeight = (lblLocationPin.text ?? "").height(withConstrainedWidth: popupWidth - 2*xPadding, font: lblLocationPin.font)
        let voucherH = carpark.hasVoucher ? 20 + yPadding: 0
        
        if carpark.parkingType == Carpark.typeSeason {
            let seasonSize = lblSeasonInfo.intrinsicContentSize
            let h = yPadding + voucherH + typeH + nameHeight + (isPinLocation ? locationPinHeight : 0) + yPadding + seasonSize.height + gap + buttonHeight + gap + buttonHeight + yPadding + buttonHeight + yPadding + tipHeight + yPadding + 10 + (needShowParking() ? parkingInfoHeight: 0)
            return CGSize(width: popupWidth, height: h)
        } else {
            let lotsSize = lblLotsInfo.intrinsicContentSize
            
            if carpark.parkingType == Carpark.typeCustomer {
                let customerSize = lblCustomerInfo.intrinsicContentSize
                let h = yPadding + voucherH + typeH + nameHeight + (isPinLocation ? locationPinHeight : 0) + yPadding + lotsSize.height + yPadding + customerSize.height + gap + buttonHeight + gap + buttonHeight + yPadding + buttonHeight + yPadding + tipHeight + yPadding + 10 + (needShowParking() ? parkingInfoHeight: 0)
                     
                return CGSize(width: popupWidth, height: h)

            } else {
                let costSize = lblCostLabel.intrinsicContentSize
                let nextCostSize = lblCostLabel.intrinsicContentSize

                let h = yPadding + voucherH + typeH + nameHeight + (isPinLocation ? locationPinHeight : 0) + yPadding + lotsSize.height + yPadding + costSize.height + yPadding + nextCostSize.height + gap + buttonHeight +  yPadding + buttonHeight  + buttonHeight + yPadding + buttonHeight + yPadding + tipHeight + yPadding + (needShowParking() ? parkingInfoHeight: 0)
                 
                
                // isCalculating ? yPadding + buttonHeight : 0
                
                return CGSize(width: popupWidth, height: h)
            }
        }
    }
    
    override func processBookmarkUpdate(bookmark: Bookmark) {
        super.processBookmarkUpdate(bookmark: bookmark)
        
        self.carpark.bookmarkId = bookmark.bookmarkId
        self.carpark.isBookmarked = true
    }
    
    override func removeBookmark() {
        super.removeBookmark()
        self.carpark.bookmarkId = nil
        self.carpark.isBookmarked = false
    }
    
    override func onSaveButtonClicked() {
        super.onSaveButtonClicked()
        
        let value = isSaved ? ParameterName.Home.UserToggle.carpark_tooktip_save_off : ParameterName.Home.UserToggle.carpark_tooltip_save_on
        AnalyticsManager.shared.logClickEvent(eventValue: value, screenName: screenName)
    }

}

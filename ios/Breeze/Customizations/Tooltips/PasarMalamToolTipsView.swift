//
//  PasarMalamToolTipsView.swift
//  Breeze
//
//  Created by Zhou Hao on 23/12/21.
//

import UIKit
import SnapKit
import CoreLocation
import RxSwift

// TODO: Directly inherit from AmenityToolTipsView and not reuse it might not be a good idea. This is a quick implementation. May need to refactor it in the future.
final class PasarMalamToolTipsView: AmenityToolTipsView {

    // MARK: - Constants
    private var popupWidth: CGFloat = 169
    private let xPadding: CGFloat = 10
    private let yPadding: CGFloat = 10
    private let gap: CGFloat = 13 // gap between labels and button
    private let buttonWidth: CGFloat = 147.0
    private let buttonHeight: CGFloat = 28.0
    private let labelWidth: CGFloat = 80
    private var isCollectionDetail: Bool = false
    
    // MARK: - Public properties
//    var onNavigate: ((_ location: CLLocationCoordinate2D) -> Void)?

    // MARK: - Private properties
    private lazy var lblName: UILabel  =  {
        let label = UILabel()
        label.font = UIFont.amenityToolTipsERPTextFont()
        label.preferredMaxLayoutWidth = popupWidth - 2 * xPadding
        label.textAlignment = .left
        label.numberOfLines = 0
        label.textColor = UIColor(named: "tooltipsAmenityNameColor")
        containerView.addSubview(label)
        return label
    }()

    private lazy var lblAddress: UILabel  =  {
        let label = UILabel()
        label.font = UIFont.amenityToolTipsAddressFont()
        label.preferredMaxLayoutWidth = popupWidth - 2 * xPadding
        label.textAlignment = .left
        label.numberOfLines = 0
        label.textColor = UIColor(named: "tooltipsAmenityAddressColor")
        containerView.addSubview(label)
        return label
    }()

    private lazy var icon: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "dateIcon"))
        containerView.addSubview(imageView)
        return imageView
    }()

    private lazy var lblStartDateLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.availableCarparkLotsLabelFont()
        label.textColor = UIColor(named: "tooltipsPasarMalamDateColor")
        label.textAlignment = .left
        label.text = "Start"
        containerView.addSubview(label)
        return label
    }()

    private lazy var lblStartDate: UILabel = {
        let label = UILabel()
        label.font = UIFont.availableCarparkLotsFont()
        label.textColor = UIColor(named:"tooltipsAmenityNameColor")
        label.textAlignment = .right
        containerView.addSubview(label)
        return label
    }()
    
    private lazy var lblEndDateLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.availableCarparkLotsLabelFont()
        label.textColor = UIColor(named: "tooltipsPasarMalamDateColor")
        label.textAlignment = .left
        label.text = "End"
        containerView.addSubview(label)
        return label
    }()

    private lazy var lbelEndDate: UILabel = {
        let label = UILabel()
        label.font = UIFont.availableCarparkLotsFont()
        label.textColor = UIColor(named:"tooltipsAmenityNameColor")
        label.textAlignment = .right
        containerView.addSubview(label)
        label.text = ""
        return label
    }()
        
    private lazy var btnNavigate: UIButton = {
        let button = UIButton()
        button.backgroundColor = UIColor(named:"tooltipsAmenityNaviBtnColor")!
        button.setTitle("Navigate Here", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = UIFont.amenityToolTipsTextFont()
        button.addTarget(self, action: #selector(onNavigateClicked), for: .touchUpInside)
        containerView.addSubview(button)
        return button
    }()
              
    init(id: String, name: String, address: String, startDate: TimeInterval, endDate: TimeInterval, location: CLLocationCoordinate2D, isCollectionDetail: Bool = false, address2: String = "", fullAddress: String = "", availablePercentImageBand: String? = "", availabilityCSData: AvailabilityCSData? = nil) {
        
        super.init(id: id, name: name, address: address, location: location, baseUI: false, availablePercentImageBand: availablePercentImageBand, availabilityCSData: availabilityCSData)
                        
        self.tipHeight = 10
        self.backgroundColor = .clear
        self.bgColor = UIColor(named: "tooltipsAmenityBackgroundColor")!
        self.containerView.backgroundColor = self.bgColor
        self.isCollectionDetail = isCollectionDetail

        lblName.text = name
        if isCollectionDetail {
            if !fullAddress.isEmpty {
                lblAddress.text = fullAddress
            } else {
                if address == name {
                    lblAddress.text = ""
                } else {
                    lblAddress.text = address
                }
            }
        } else {
            lblAddress.text = address
        }
                
        lblStartDate.text = getDate(timeStamp: startDate)
        lbelEndDate.text = getDate(timeStamp: endDate)

        self.showShadow = false
//        setupLayout()
    }
    
    private func getDate(timeStamp: TimeInterval) -> String {
        
        let date = Date(timeIntervalSince1970: timeStamp)
        
        let strDate = DateUtils.shared.getTimeDisplay(dateFormat: Date.formatDDMMMYY, date: date)
        return "\(strDate)"
    }

    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    internal override func setupLayout() {
                        
        lblName.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().offset(xPadding)
            make.top.equalToSuperview().offset(14)
            make.trailing.equalToSuperview().offset(-xPadding)
        }

        lblAddress.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().offset(xPadding)
            make.top.equalTo(lblName.snp.bottom).offset(4)
            make.trailing.equalToSuperview().offset(-xPadding)
        }
        
        icon.snp.makeConstraints { make in
            make.width.equalTo(16)
            make.height.equalTo(16)
            make.leading.equalToSuperview().offset(xPadding)
            make.top.equalTo(lblAddress.snp.bottom).offset(16)
        }

        lblStartDateLabel.snp.makeConstraints { make in
            make.leading.equalTo(icon.snp.trailing).offset(7)
            make.trailing.equalTo(lblStartDate.snp.leading).offset(7)
            make.centerY.equalTo(icon.snp.centerY)
        }
        
        lblStartDate.snp.makeConstraints { make in
            make.centerY.equalTo(icon.snp.centerY)
            make.trailing.equalToSuperview().offset(-xPadding)
            make.width.equalTo(labelWidth)
        }
                
        lblEndDateLabel.snp.makeConstraints { make in
            make.leading.equalTo(icon.snp.trailing).offset(7)
            make.trailing.equalTo(lbelEndDate.snp.leading).offset(7)
            make.top.equalTo(lblStartDateLabel.snp.bottom).offset(5)
        }
        
        lbelEndDate.snp.makeConstraints { make in
            make.centerY.equalTo(lblEndDateLabel.snp.centerY)
            make.trailing.equalToSuperview().offset(-xPadding)
            make.width.equalTo(labelWidth)
        }
        
        btnNavigate.snp.makeConstraints { make in
            make.width.equalTo(buttonWidth)
            make.height.equalTo(buttonHeight)
            make.centerX.equalToSuperview()
            make.top.equalTo(lbelEndDate.snp.bottom).offset(gap)
        }
                
//        containerView.snp.makeConstraints { (make) in
//            make.top.equalToSuperview()
//            make.bottom.equalToSuperview().offset(-tipHeight)
//            make.leading.equalToSuperview()
//            make.width.equalTo(popupWidth)
//            make.trailing.equalToSuperview()
//        }
        
        //  MARK: - Parking available
        parkingStatusContentView.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(8)
            make.leading.equalToSuperview().offset(8)
            make.height.equalTo(36)
            make.trailing.equalToSuperview().offset(-8)
        }
        
        parkingIcon.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(10)
            make.leading.equalToSuperview().offset(8)
            make.height.equalTo(17)
            make.width.equalTo(17)
        }
        
        parkingAvailableStatusLabel.snp.makeConstraints { make in
            make.centerY.equalTo(parkingIcon.snp.centerY)
            make.leading.equalToSuperview().offset(29)
            make.trailing.equalToSuperview().offset(10)
        }
        
        parkingTriangleIcon.snp.makeConstraints { make in
            make.top.equalTo(parkingStatusContentView.snp.bottom).offset(-3)
            make.leading.equalTo(parkingStatusContentView).offset(12)
            make.height.equalTo(10)
            make.width.equalTo(14)
        }
        
        parkingUpdatedTimeLabel.snp.makeConstraints { make in
            make.top.equalTo(parkingStatusContentView.snp.bottom).offset(10)
            make.leading.equalToSuperview().offset(8)
            make.trailing.equalToSuperview().offset(-8)
        }
        
        containerView.snp.makeConstraints { (make) in
            if needShowParking() {
                make.top.equalToSuperview().offset(parkingInfoHeight)
                make.bottom.equalToSuperview()
            }else {
                make.top.equalToSuperview().offset(0)
                make.bottom.equalToSuperview().offset(-tipHeight)
            }
            
            make.leading.equalToSuperview()
            make.width.equalTo(popupWidth)
            make.trailing.equalToSuperview()
        }
        
        parkingAvailability.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.bottom.equalToSuperview().offset(-tipHeight)
            make.leading.equalToSuperview()
            make.width.equalTo(popupWidth)
            make.trailing.equalToSuperview()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        btnNavigate.layer.cornerRadius = buttonHeight / 2
    }
    
    override func onSaveButtonClicked() {
        if isCollectionDetail {
            if isSaved {
                //  Unbookmark the amenity
                if let ID = self.bookmarkId, ID > 0 {
                    let deleteData: [String: Any] = ["bookmarkId": ID, "action": "DELETE"]
                    addOrDeleteDropPinLocationInCollectionDetail(deleteData)
                }
            }
        } else {
            super.onSaveButtonClicked()
        }
    }

    override func sizeThatFits(_ size: CGSize) -> CGSize {
                
        let nameSize = lblName.intrinsicContentSize
        let addressSize = lblAddress.intrinsicContentSize
        let startDateSize = lblStartDate.intrinsicContentSize
        let endDateSize = lbelEndDate.intrinsicContentSize

        let h = 14 + nameSize.height + 4 + addressSize.height + 16 + startDateSize.height + 5 + endDateSize.height + gap + buttonHeight + yPadding + tipHeight  + (needShowParking() ? parkingInfoHeight: 0)
                
        return CGSize(width: popupWidth, height: h)
    }

}

//
//  CurrentUserToolTips.swift
//  Breeze
//
//  Created by Zhou Hao on 25/7/22.
//

import UIKit
import CoreLocation

final class CurrentUserToolTips: ToolTipsView {
    
    // MARK: - Constants
    private var popupWidth: CGFloat = 120
    private let xPadding: CGFloat = 10
    private let yPadding: CGFloat = 8

    // MARK: - private properties
    private var currentLocation: CLLocationCoordinate2D!
    
    private lazy var lblTitle: UILabel  =  {
        let label = UILabel()
        label.font = UIFont.amenityToolTipsERPTextFont()
        label.preferredMaxLayoutWidth = popupWidth - 2 * xPadding
        label.textAlignment = .center
        label.numberOfLines = 0
        label.text = "You are here"
        label.textColor = UIColor(named: "tooltipsNudgeTextColor")
        containerView.addSubview(label)
        return label
    }()

    override var tooltipCoordinate: CLLocationCoordinate2D {
        return currentLocation
    }
    
    override var tooltipId: String {
        return ""
    }

    init(currentLocation: CLLocationCoordinate2D) {
        super.init(frame: .zero)
                
        self.backgroundColor = .clear
        self.bgColor = UIColor(named: "tooltipsNudgeBackgroundColor")!

        self.currentLocation = currentLocation
        self.showShadow = false
//        setupLayout()
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    internal override func setupLayout() {
        lblTitle.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().offset(xPadding)
            make.top.equalToSuperview().offset(yPadding)
            make.trailing.equalToSuperview().offset(-xPadding)
        }

        containerView.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.bottom.equalToSuperview().offset(-tipHeight)
            make.leading.equalToSuperview()
            make.width.equalTo(popupWidth)
            make.trailing.equalToSuperview()
        }
    }
    
    override func sizeThatFits(_ size: CGSize) -> CGSize {
                
        let titleSize = lblTitle.intrinsicContentSize
        let h = yPadding + titleSize.height + yPadding + tipHeight
                
        return CGSize(width: popupWidth, height: h)
    }
}

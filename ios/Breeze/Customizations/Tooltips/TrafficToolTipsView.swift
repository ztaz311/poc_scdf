//
//  TrafficToolTipsView.swift
//  Breeze
//
//  Created by Zhou Hao on 24/12/21.
//

import UIKit
import CoreLocation

class TrafficToolTipsView: ToolTipsView {

    private let popupWidth: CGFloat = 172.0
    private let xPadding: CGFloat = 20
    private let yPadding: CGFloat = 10
    
    // MARK: - Public properties
    override var tooltipCoordinate: CLLocationCoordinate2D {
        return location
    }
    
    override var tooltipId: String {
        return trafficId
    }

    private var location: CLLocationCoordinate2D
    private var trafficId: String
    
    private lazy var lblText: UILabel  =  {
        let label = UILabel()
        label.font = UIFont.amenityToolTipsERPTextFont()
        label.preferredMaxLayoutWidth = popupWidth - 2 * xPadding
        label.textAlignment = .left
        label.text = ""
        label.numberOfLines = 0
        label.textColor = UIColor(named: "tooltipsAmenityNameColor")
        containerView.addSubview(label)
        return label
    }()

    init(id: String,
         name: String,
         location: CLLocationCoordinate2D) {
        self.location = location
        self.trafficId = id
        
        super.init(frame: .zero)
        
        self.backgroundColor = .clear
        self.bgColor = UIColor(named: "tooltipsAmenityBackgroundColor")!
        self.tipHeight = 12.0
        self.tipWidth = 20.0

        self.lblText.text = name
        
        self.showShadow = false
//        setupLayout()
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
        
    @objc private func onOKClicked() {
        UserDefaults.standard.set("true", forKey: Values.tipsViewed)
        dismiss(animated: true)
    }
    
    override func sizeThatFits(_ size: CGSize) -> CGSize {
                
        let textSize = lblText.intrinsicContentSize
        let h = yPadding + textSize.height + yPadding + tipHeight 
        
        return CGSize(width: popupWidth, height: h)
    }
        
    internal override func setupLayout() {
        lblText.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(yPadding)
            make.leading.equalToSuperview().offset(xPadding)
            make.trailing.equalToSuperview().offset(-xPadding)
        }
        
        containerView.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.bottom.equalToSuperview().offset(-tipHeight)
            make.leading.equalToSuperview()
            make.width.equalTo(popupWidth)
            make.trailing.equalToSuperview()
        }
    }
}

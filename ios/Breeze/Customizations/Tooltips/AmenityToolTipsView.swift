//
//  AmenityToolTipsView.swift
//  Breeze
//
//  Created by Zhou Hao on 23/12/21.
//

import UIKit
import SnapKit
import CoreLocation

class AmenityToolTipsView: ToolTipsView {
    
    private var popupWidth: CGFloat = 169
    private var buttonHeight: CGFloat = 28
    private var buttonWidth: CGFloat = 147
    private let yPadding: CGFloat = 14
    private let yPaddingBound: CGFloat = 15
    private let xPadding: CGFloat = 10
    private let gap: CGFloat = 14
    private var amenityId: String
    private var type: String = ""
    
    private var walkingEnabled = false
    private var navigationEnabled = false
    private var isAdditionalInfo = false
    
    private var isShareEnable = false
    private var isCollectionDetail = false
    
    let parkingInfoHeight: CGFloat = 71
    var hasParkingAvailable: Bool = false
    
    var isAnE: Bool {
        return type.lowercased() == "ane"
    }
    
    var textAdditionalInfo: String?
    var styleLightColor: String
    var styleDarkColor: String
    
    var isBiggerStyle: Bool = false
    
    // MARK: - Public properties
    override var tooltipCoordinate: CLLocationCoordinate2D {
        return location
    }
    
    override var tooltipId: String {
        return amenityId
    }
    
    private var location: CLLocationCoordinate2D!
    
    var onNavigationHere: ((_ name: String, _ address: String, _ coordinate: CLLocationCoordinate2D) -> Void)?
    var onWalkHere: ((_ id: String, _ name: String, _ address: String, _ coordinate: CLLocationCoordinate2D) -> Void)?
    
    var shareViaLocation: (() -> Void)?
    
    var inviteLocation: (() -> Void)?
    
    private lazy var imvAnE: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "ic_ane")
        containerView.addSubview(imageView)
        return imageView
    }()
    
    private lazy var lblName: UILabel  =  {
        let label = UILabel()
        if isBiggerStyle {
            label.font = UIFont.amenityToolTipsBiggerTextFont()
        } else {
            label.font = UIFont.amenityToolTipsTextFont()
        }
        label.preferredMaxLayoutWidth = popupWidth - 2 * xPadding
        label.textAlignment = .left
        label.text = ""
        label.numberOfLines = 0
        label.textColor = UIColor(named: "tooltipsAmenityNameColor")!
        containerView.addSubview(label)
        return label
    }()
    
    
    private lazy var lblAdditionalInfo: UILabel  =  {
        let label = UILabel()
        if isBiggerStyle {
            label.font = UIFont.amenityToolTipsBiggerTextFont()
        } else {
            label.font = UIFont.amenityToolTipsTextFont()
        }
        label.preferredMaxLayoutWidth = popupWidth - 2 * xPadding
        label.textAlignment = .left
        label.text = ""
        label.numberOfLines = 0
        containerView.addSubview(label)
        return label
    }()

    private lazy var lblAddress: UILabel  =  {
        let label = UILabel()
        if isBiggerStyle {
            label.font = UIFont.amenityToolTipsAddressBiggerFont()
        } else {
            label.font = UIFont.amenityToolTipsAddressFont()
        }
        
        label.preferredMaxLayoutWidth = popupWidth - 2 * xPadding
        label.textAlignment = .left
        label.text = ""
//        label.backgroundColor = .red
        label.numberOfLines = 0
        label.textColor = UIColor(named: "tooltipsAmenityAddressColor")!
        containerView.addSubview(label)
        return label
    }()

    private lazy var btnNavigate: UIButton = {
        let button = UIButton()
        button.backgroundColor = UIColor(named:"tooltipsAmenityNaviBtnColor")!
        button.setTitle("Navigate Here", for: .normal)
        button.setTitleColor(.white, for: .normal)
        if isBiggerStyle {
            button.titleLabel?.font = UIFont.carparkNavigateButtonFont()
        } else {
            button.titleLabel?.font = UIFont.amenityToolTipsTextFont()
        }
        button.addTarget(self, action: #selector(onNavigateClicked), for: .touchUpInside)
        containerView.addSubview(button)
        return button
    }()
    
    private lazy var btnWalk: UIButton = {
        let button = UIButton()
        button.setTitle("Walk Here", for: .normal)
        button.setTitleColor(UIColor(named:"tooltipsAmenityNaviBtnColor")!, for: .normal)
        if isBiggerStyle {
            button.titleLabel?.font = UIFont.carparkNavigateButtonFont()
        } else {
            button.titleLabel?.font = UIFont.amenityToolTipsTextFont()
        }
        
        button.backgroundColor = .clear
        button.layer.cornerRadius = buttonHeight / 2
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor(named:"tooltipsAmenityNaviBtnColor")?.cgColor ?? UIColor.purple.cgColor
        button.addTarget(self, action: #selector(onWalkHereClicked), for: .touchUpInside)
        containerView.addSubview(button)
        return button
    }()
    
    
    private lazy var btnShare: UIButton = {
        let button = UIButton()
        button.backgroundColor = .clear
        button.setTitle("Share", for: .normal)
        button.setTitleColor(UIColor(named:"tooltipsAmenityNaviBtnColor")!, for: .normal)
        button.layer.cornerRadius = buttonHeight / 2
        button.titleLabel?.font = UIFont.amenityToolTipsTextFont()
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor(named:"tooltipsAmenityNaviBtnColor")?.cgColor ?? UIColor.purple.cgColor
        button.addTarget(self, action: #selector(onShareViaLocation), for: .touchUpInside)
        containerView.addSubview(button)
        return button
    }()
    
    private lazy var btnInvite: UIButton = {
        let button = UIButton()
        button.backgroundColor = .clear
        button.setTitle("Invite", for: .normal)
        button.setTitleColor(UIColor(named:"tooltipsAmenityNaviBtnColor")!, for: .normal)
        button.layer.cornerRadius = buttonHeight / 2
        button.titleLabel?.font = UIFont.amenityToolTipsTextFont()
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor(named:"tooltipsAmenityNaviBtnColor")?.cgColor ?? UIColor.purple.cgColor
        button.addTarget(self, action: #selector(onInviteLocation), for: .touchUpInside)
        containerView.addSubview(button)
        return button
    }()
    
    //  MARK: Parking availability
    
    lazy var parkingStatusContentView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(hexString: "F5EEFC", alpha: 1)
        view.layer.cornerRadius = 8
        parkingAvailability.addSubview(view)
        return view
    }()
    
    lazy var parkingIcon: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "carpark-availability-parking-icon1"))
        parkingStatusContentView.addSubview(imageView)
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    lazy var parkingAvailableStatusLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.parkingAvailableTextFont()
        label.textColor = UIColor(hexString: "222638", alpha: 1)
        label.textAlignment = .left
        parkingStatusContentView.addSubview(label)
        label.text = ""
        return label
    }()
    
    lazy var parkingTriangleIcon: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "carpark-availability-triangle"))
        parkingAvailability.addSubview(imageView)
        return imageView
    }()
    
    lazy var parkingUpdatedTimeLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.parkingAvailableUpdateTimeFont()
        label.textColor = .white
        label.textAlignment = .center
        parkingAvailability.addSubview(label)
        label.text = ""
        return label
    }()

    init(id: String, name: String, address: String, location: CLLocationCoordinate2D, baseUI: Bool = true, type: String = "", text: String = "", styleLightColor: String = "", styleDarkColor: String = "", walkingEnabled: Bool = false, navigationEnabled: Bool = true, isBiggerStyle: Bool = false, isShareEnable: Bool = false, isCollectionDetail: Bool = false, address2: String = "", fullAddress: String = "", availablePercentImageBand: String? = "", availabilityCSData: AvailabilityCSData? = nil) {
        
        self.isBiggerStyle = isBiggerStyle
        self.amenityId = id
        self.location = location
        self.type = type
        self.walkingEnabled = walkingEnabled
        self.navigationEnabled = navigationEnabled
        self.textAdditionalInfo = text
        self.styleLightColor = styleLightColor
        self.styleDarkColor = styleDarkColor
        self.isShareEnable = isShareEnable
        self.isCollectionDetail = isCollectionDetail
        
        if self.isBiggerStyle {
            buttonHeight = 54
            popupWidth = 206
            buttonWidth += 40
        }
        
        if availabilityCSData != nil {
            self.hasParkingAvailable = true
        }

        super.init(frame: .zero)
        
        self.address = address
        self.availablePercentImageBand = availablePercentImageBand
        self.availabilityCSData = availabilityCSData
        
        btnWalk.isHidden = !walkingEnabled
        btnNavigate.isHidden = !navigationEnabled
        
        // BREEZES-8485 Showing-Closure-Of-Hawker-Centres Tooltip
        if let textAdditionalInfoLbl = textAdditionalInfo, !textAdditionalInfoLbl.isEmpty {
            isAdditionalInfo = true
            lblAdditionalInfo.isHidden = false
            lblAdditionalInfo.text = textAdditionalInfoLbl
            self.setupStyleColor(styleDarkColorText: styleDarkColor, styleLightColorText: styleLightColor)
        } else {
            lblAdditionalInfo.isHidden = true
        }
        
        if self.isShareEnable == true {
            btnShare.isHidden = false
            btnInvite.isHidden = false
        } else {
            btnShare.isHidden = true
            btnInvite.isHidden = true
        }
        
        if baseUI {
            self.backgroundColor = .clear
            self.bgColor = UIColor(named: "tooltipsAmenityBackgroundColor")!
            self.containerView.backgroundColor = self.bgColor
            self.tipHeight = 12.0
            self.tipWidth = 20.0
            self.cornerRadius = 16.0
            lblName.text = name
            
            if isCollectionDetail {
                if !fullAddress.isEmpty {
                    lblAddress.text = fullAddress
                } else {
                    if address == name {
                        lblAddress.text = ""
                    } else {
                        lblAddress.text = address
                    }
                }
            } else {
                lblAddress.text = address
            }
            self.showShadow = false
//            setupLayout()
            
            //  Handle parking available stype here
            updateParkingAvailableStyle()
        }
    }
    
    override func needShowParking() -> Bool {
        return hasParkingAvailable
    }
    
    private func updateParkingAvailableStyle() {
        
        parkingAvailability.isHidden = !hasParkingAvailable
        parkingStatusContentView.isHidden = !hasParkingAvailable
        parkingTriangleIcon.isHidden = !hasParkingAvailable
        parkingUpdatedTimeLabel.isHidden = !hasParkingAvailable
        
        parkingUpdatedTimeLabel.text = crowdsourceTSDesc
        parkingAvailableStatusLabel.text = availabilityCSData?.title ?? ""
        
        let bgColorHexString: String = getCSThemeColor()
        parkingAvailability.backgroundColor = UIColor(hexString: bgColorHexString, alpha: 1)
        parkingIcon.image = UIImage(named: getCSIconName())
    }
    
    private func setupStyleColor(styleDarkColorText: String, styleLightColorText: String) {
        if appDelegate().isDarkMode() {
            if !styleDarkColorText.isEmpty  {
                lblAdditionalInfo.textColor = UIColor(hexString: styleDarkColorText, alpha: 1)
            } else {
                lblAdditionalInfo.textColor = UIColor(named: "trafficHeavyColor")!
            }
        } else {
            if !styleLightColorText.isEmpty {
                lblAdditionalInfo.textColor = UIColor(hexString: styleLightColorText, alpha: 1)
            } else {
                lblAdditionalInfo.textColor = UIColor(named: "trafficHeavyColor")!
            }
        }
    }

    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        btnNavigate.layer.cornerRadius = buttonHeight / 2
    }
    
    func setButtonHeight(_ height: CGFloat) {
        buttonHeight = height
    }

    @objc func onNavigateClicked() {
        if let callback = onNavigationHere {
            callback(lblName.text ?? "", self.address ?? "", self.location)
        }

        dismiss(animated: true)
    }
    
    @objc private func onWalkHereClicked() {
        if let callback = onWalkHere {
            callback(self.amenityId, lblName.text ?? "", self.address ?? "", self.location)
        }
    }
    
    @objc func onShareViaLocation() {
//        if let callback = shareViaLocation {
//            callback(address)
//        }
        
        shareViaLocation?()
//        dismiss(animated: true)
    }
    
    
    @objc func onInviteLocation() {
        if self.type == "evcharger" {
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Home.UserClick.tooltip_ev_invite, screenName: ParameterName.Home.homepage_screen_view)
        } else if self.type == "petrol" {
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Home.UserClick.tooltip_petrol_invite, screenName: ParameterName.Home.homepage_screen_view)
        } else {
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Home.UserClick.tooltip_saved_invite, screenName: self.screenName)
            
        }
        
        inviteLocation?()
//        dismiss(animated: true)
    }
    
    override func sizeThatFits(_ size: CGSize) -> CGSize {
                
//        let lblNameSize = lblName.intrinsicContentSize
//        let lblAddrSize = lblAddress.intrinsicContentSize
        
        
        let leftPadding = onSaveAction == nil ? xPadding : xPadding + 24
        let lblNameSize = (lblName.text ?? "").height(withConstrainedWidth: popupWidth - xPadding - leftPadding, font: lblName.font)
        //let lblNameSize = lblName.text!.height(withConstrainedWidth: popupWidth - 2 * xPadding, font: lblName.font)
        let lblAdditionalInfoSize = yPadding + lblAdditionalInfo.text!.height(withConstrainedWidth: popupWidth - xPadding - leftPadding, font: lblAdditionalInfo.font)
        let lblAddrSize = (lblAddress.text ?? "").height(withConstrainedWidth: popupWidth - 2 * xPadding, font: lblAddress.font)
        let heightShareButton = buttonHeight
        let heightInviteBtn = buttonHeight + gap
        
        let aneHeight: Double = self.isAnE ? (21 + yPadding) : 0
        let h = yPaddingBound + lblNameSize + (isAdditionalInfo ? lblAdditionalInfoSize : 0) + yPadding + lblAddrSize +  (isShareEnable ? heightShareButton + gap + heightInviteBtn : 0) + ((navigationEnabled || walkingEnabled) ? gap : 0) + (navigationEnabled ? buttonHeight: 0) + yPaddingBound + (walkingEnabled ? buttonHeight : 0) + tipHeight + aneHeight + 2 + (needShowParking() ? parkingInfoHeight: 0)
        
        return CGSize(width: popupWidth, height: h)
    }

    internal override func setupLayout() {
        imvAnE.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(yPaddingBound)
            make.leading.equalToSuperview().offset(xPadding)
            make.width.equalTo(49)
            make.height.equalTo(21)
        }
        imvAnE.isHidden = !self.isAnE
        
        lblName.snp.makeConstraints { (make) in
            if self.isAnE {
                make.top.equalTo(self.imvAnE.snp.bottom).offset(yPadding)
            } else {
                make.top.equalToSuperview().offset(yPaddingBound)
            }
            
            make.leading.equalToSuperview().offset(xPadding)
            let leftPadding = onSaveAction == nil ? xPadding : xPadding + 24
            if self.isAnE {
                make.trailing.equalToSuperview().offset(-leftPadding)
//                make.trailing.equalToSuperview().offset(-xPadding)
            } else {
                make.trailing.equalToSuperview().offset(-leftPadding)
//                make.trailing.equalToSuperview().offset(-(xPadding+24))
            }
        }
        if isAdditionalInfo == true {
            lblAdditionalInfo.snp.makeConstraints { (make) in
                make.top.equalTo(lblName.snp.bottom).offset(yPaddingBound)
                make.leading.equalToSuperview().offset(xPadding)
                make.trailing.equalToSuperview().offset(-xPadding)
                
                lblAddress.snp.makeConstraints { (make) in
                    make.top.equalTo(lblAdditionalInfo.snp.bottom).offset(yPaddingBound)
                    make.leading.equalToSuperview().offset(xPadding)
                    make.trailing.equalToSuperview().offset(-xPadding)
                    
                }
            }
        } else {
            lblAddress.snp.makeConstraints { (make) in
                make.top.equalTo(lblName.snp.bottom).offset(yPaddingBound)
                make.leading.equalToSuperview().offset(xPadding)
                make.trailing.equalToSuperview().offset(-xPadding)
                //make.centerX.equalToSuperview()
            }
        }
        
        btnWalk.snp.makeConstraints { make in
            make.width.equalTo(buttonWidth)
            make.height.equalTo(buttonHeight)
            make.centerX.equalToSuperview()
            make.top.equalTo(btnInvite.snp.bottom).offset(gap)
        }

        btnNavigate.snp.makeConstraints { make in            
            make.centerX.equalTo(containerView.snp.centerX)
            make.width.equalTo(buttonWidth)
            make.bottom.equalToSuperview().offset(-yPaddingBound)
            if navigationEnabled {
                make.height.equalTo(buttonHeight)
            }else {
                make.height.equalTo(0)
            }
            
            if self.walkingEnabled {
                if navigationEnabled {
                    make.top.equalTo(btnWalk.snp.bottom).offset(yPadding)
                }else {
                    make.top.equalTo(btnWalk.snp.bottom)
                }
            } else {
                if self.isShareEnable == true {
                    if navigationEnabled  {
                        make.top.equalTo(btnInvite.snp.bottom).offset(gap)
                    }else {
                        make.top.equalTo(btnInvite.snp.bottom)
                    }
                } else {
                    if navigationEnabled  {
                        make.top.equalTo(lblAddress.snp.bottom).offset(gap)
                    }else {
                        make.top.equalTo(lblAddress.snp.bottom)
                    }
                }
                
            }
        }
        
        if self.isShareEnable == true {
            btnShare.snp.makeConstraints { make in
                make.centerX.equalTo(containerView.snp.centerX)
                make.width.equalTo(buttonWidth)
                make.top.equalTo(lblAddress.snp.bottom).offset(yPaddingBound)
                make.leading.equalToSuperview().offset(xPadding)
                make.trailing.equalToSuperview().offset(-xPadding)
                make.height.equalTo(buttonHeight)
            }
            
            btnInvite.snp.makeConstraints { make in
                make.centerX.equalTo(containerView.snp.centerX)
                make.width.equalTo(buttonWidth)
                make.top.equalTo(btnShare.snp.bottom).offset(yPaddingBound)
                make.leading.equalToSuperview().offset(xPadding)
                make.trailing.equalToSuperview().offset(-xPadding)
                make.height.equalTo(buttonHeight)
            }
            
        }
        
//        containerView.snp.makeConstraints { (make) in
//            make.top.equalToSuperview()
//            make.bottom.equalToSuperview().offset(-tipHeight)
//            make.leading.equalToSuperview()
//            make.width.equalTo(popupWidth)
//            make.trailing.equalToSuperview()
//        }
        
        //  MARK: - Parking available
        parkingStatusContentView.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(8)
            make.leading.equalToSuperview().offset(8)
            make.height.equalTo(36)
            make.trailing.equalToSuperview().offset(-8)
        }
        
        parkingIcon.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(10)
            make.leading.equalToSuperview().offset(8)
            make.height.equalTo(17)
            make.width.equalTo(17)
        }
        
        parkingAvailableStatusLabel.snp.makeConstraints { make in
            make.centerY.equalTo(parkingIcon.snp.centerY)
            make.leading.equalToSuperview().offset(29)
            make.trailing.equalToSuperview().offset(10)
        }
        
        parkingTriangleIcon.snp.makeConstraints { make in
            make.top.equalTo(parkingStatusContentView.snp.bottom).offset(-3)
            make.leading.equalTo(parkingStatusContentView).offset(12)
            make.height.equalTo(10)
            make.width.equalTo(14)
        }
        
        parkingUpdatedTimeLabel.snp.makeConstraints { make in
            make.top.equalTo(parkingStatusContentView.snp.bottom).offset(10)
            make.leading.equalToSuperview().offset(8)
            make.trailing.equalToSuperview().offset(-8)
        }
        
        containerView.snp.makeConstraints { (make) in
            if needShowParking() {
                make.top.equalToSuperview().offset(parkingInfoHeight)
                make.bottom.equalToSuperview()
            }else {
                make.top.equalToSuperview().offset(0)
                make.bottom.equalToSuperview().offset(-tipHeight)
            }
            
            make.leading.equalToSuperview()
            make.width.equalTo(popupWidth)
            make.trailing.equalToSuperview()
        }
        
        parkingAvailability.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.bottom.equalToSuperview().offset(-tipHeight)
            make.leading.equalToSuperview()
            make.width.equalTo(popupWidth)
            make.trailing.equalToSuperview()
        }
    }
    
    override func onSaveButtonClicked() {
        if isCollectionDetail {
            if isSaved {
                //  Unbookmark the amenity
                if let ID = self.bookmarkId, ID > 0 {
                    let deleteData: [String: Any] = ["bookmarkId": ID, "action": "DELETE"]
                    addOrDeleteDropPinLocationInCollectionDetail(deleteData)
                }
            }
        } else {
            super.onSaveButtonClicked()
        }
        
        let value = isSaved ? ParameterName.Home.UserToggle.amenities_tooltip_save_off : ParameterName.Home.UserToggle.amenities_tooltip_save_on
        AnalyticsManager.shared.logClickEvent(eventValue: value, screenName: screenName)
    }
    
    override func removeBookmark() {
        super.removeBookmark()
        
        
    }
}

extension String {
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil)
    
        return ceil(boundingBox.height)
    }

    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil)

        return ceil(boundingBox.width)
    }
}

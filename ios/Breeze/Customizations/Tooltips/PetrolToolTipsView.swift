//
//  PetrolToolTipsView.swift
//  Breeze
//
//  Created by Zhou Hao on 24/12/21.
//

import UIKit
import SnapKit
import CoreLocation

class PetrolToolTipsView: ToolTipsView {

    // MARK: - Constants
    private var popupWidth: CGFloat = 166
    private let buttonHeight: CGFloat = 40
    private let buttonWidth: CGFloat = 147
    private let yPadding: CGFloat = 8
    private let xPadding: CGFloat = 12
    private let gap: CGFloat = 20
    private var location: CLLocationCoordinate2D
    private var petrolId: String

    // MARK: - Public properties
    var onAdd: ((_ address: String, _ coordinate: CLLocationCoordinate2D) -> Void)?
    var onRemove: ((_ address: String, _ coordinate: CLLocationCoordinate2D) -> Void)?
    
    override var tooltipCoordinate: CLLocationCoordinate2D {
        return location
    }
    
    override var tooltipId: String {
        return petrolId
    }
    
    // MARK: - Private properties
    private var isAdding = false
    private lazy var lblName: UILabel  =  {
        let label = UILabel()
        label.font = UIFont.amenityToolTipsTextFont()
        label.preferredMaxLayoutWidth = 120
        label.textAlignment = .left
        label.text = ""
        label.numberOfLines = 1
        label.textColor = UIColor(named: "tooltipsAmenityNameColor")!
        containerView.addSubview(label)
        return label
    }()

    private lazy var lblAddress: UILabel  =  {
        let label = UILabel()
        label.font = UIFont.amenityToolTipsAddressFont()
        label.preferredMaxLayoutWidth = 120
        label.textAlignment = .left
        label.text = ""
        label.numberOfLines = 0
        label.textColor = UIColor(named: "tooltipsAmenityAddressColor")!
        containerView.addSubview(label)
        return label
    }()

    // shared by add/remove
    private lazy var btnAction: UIButton = {
        let button = UIButton()
        button.backgroundColor = UIColor(named:"tooltipsAmenityNaviBtnColor")!
        button.setTitle("Navigate Here", for: .normal) // "Add Stop" Change for pilot
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = UIFont.amenityToolTipsTextFont()
        button.addTarget(self, action: #selector(onActionClicked), for: .touchUpInside)
        containerView.addSubview(button)
        return button
    }()
    
    init(id: String, name: String, address: String, location: CLLocationCoordinate2D, isAdding: Bool, isShowButton:Bool) {
        
        self.location = location
        
        self.petrolId = id
        
        super.init(frame: .zero)
        self.backgroundColor = .clear
        self.bgColor = UIColor(named: "tooltipsAmenityBackgroundColor")!
        self.tipHeight = 12.0
        self.tipWidth = 20.0
        self.cornerRadius = 16.0
        
        lblName.text = name
        lblAddress.text = address
        
        self.isAdding = isAdding        
        
        if !isAdding {
            btnAction.isHidden = isShowButton
            btnAction.isEnabled = true
            btnAction.backgroundColor = UIColor(named:"tooltipsRemoveBtnColor")!
            btnAction.setTitle("Remove Stop", for: .normal)
        } else {
            btnAction.setTitle("Add Stop", for: .normal)
            if isShowButton {
                btnAction.backgroundColor = UIColor(named: "addStopDisableColor")!
                btnAction.isEnabled = false
            } else {
                btnAction.isHidden = false
                btnAction.isEnabled = true
                btnAction.backgroundColor = UIColor(named:"tooltipsAmenityNaviBtnColor")!
            }
        }
        
        self.showShadow = false
//        setupLayout()
    }

    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        btnAction.layer.cornerRadius = buttonHeight / 2
    }
    
    override func sizeThatFits(_ size: CGSize) -> CGSize {
                
        let lblNameSize = lblName.intrinsicContentSize
        let lblAddrSize = lblAddress.intrinsicContentSize

        let h = lblNameSize.height + yPadding + lblAddrSize.height + gap + buttonHeight + 12 + tipHeight + 10
        
        return CGSize(width: popupWidth, height: h)
    }


    @objc private func onActionClicked() {
        
        if isAdding {
            if let callback = onAdd {
                
                callback(self.lblAddress.text ?? "", self.location)
            }
        } else {
            if let callback = onRemove {
                callback(self.lblAddress.text ?? "", self.location)
            }
        }

        dismiss(animated: true)
    }
    
    internal override func setupLayout() {
        lblName.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(yPadding)
            make.leading.equalToSuperview().offset(xPadding)
            make.trailing.equalToSuperview().offset(-xPadding)
        }
                
        lblAddress.snp.makeConstraints { (make) in
            make.top.equalTo(lblName.snp.bottom).offset(yPadding)
            make.leading.equalToSuperview().offset(xPadding)
            make.trailing.equalToSuperview().offset(-xPadding)
        }

        btnAction.snp.makeConstraints { make in
            make.bottom.equalToSuperview().offset(-yPadding)
            make.top.equalTo(lblAddress.snp.bottom).offset(gap)
            make.centerX.equalTo(containerView.snp.centerX)
            make.width.equalTo(buttonWidth)
            make.height.equalTo(buttonHeight)
        }
        
        containerView.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.bottom.equalToSuperview().offset(-tipHeight)
            make.leading.equalToSuperview()
            make.width.equalTo(popupWidth)
            make.trailing.equalToSuperview()
        }
    }
}

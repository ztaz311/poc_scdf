//
//  AmenityToolTipsView.swift
//  Breeze
//
//  Created by Zhou Hao on 23/12/21.
//

import UIKit
import SnapKit
import CoreLocation

class CustomBookmarkTooltip: AmenityToolTipsView {
    
    private var popupWidth: CGFloat = 166
    private let buttonHeight: CGFloat = 28
    private let buttonWidth: CGFloat = 147
    private let yPadding: CGFloat = 14
    private let yPaddingBound: CGFloat = 15
    private let xPadding: CGFloat = 10
    private let gap: CGFloat = 14
    
    private var isCollectionDetail: Bool = false
    
    private lazy var lblName: UILabel  =  {
        let label = UILabel()
        label.font = UIFont.amenityToolTipsTextFont()
        label.preferredMaxLayoutWidth = popupWidth - 2 * xPadding
        label.textAlignment = .left
        label.text = ""
        label.numberOfLines = 0
        label.textColor = UIColor(named: "tooltipsAmenityNameColor")!
        containerView.addSubview(label)
        return label
    }()

    private lazy var lblAddress: UILabel  =  {
        let label = UILabel()
        label.font = UIFont.amenityToolTipsAddressFont()
        label.preferredMaxLayoutWidth = popupWidth - 2 * xPadding
        label.textAlignment = .left
        label.text = ""
        label.numberOfLines = 0
        label.textColor = UIColor(named: "tooltipsAmenityAddressColor")!
        containerView.addSubview(label)
        return label
    }()
    
    private lazy var lbDescription: UILabel  =  {
        let label = UILabel()
        label.font = UIFont.amenityToolTipsDesFont()
        label.preferredMaxLayoutWidth = popupWidth - 2 * xPadding
        label.textAlignment = .left
        label.text = ""
        label.numberOfLines = 0
        label.textColor = UIColor(named: "tooltipsAmenityDesColor")!
        containerView.addSubview(label)
        return label
    }()

    private lazy var btnNavigate: UIButton = {
        let button = UIButton()
        button.backgroundColor = UIColor(named:"tooltipsAmenityNaviBtnColor")!
        button.setTitle("Navigate Here", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = UIFont.amenityToolTipsTextFont()
        button.addTarget(self, action: #selector(onNavigateClicked), for: .touchUpInside)
        containerView.addSubview(button)
        return button
    }()
    
    
    private lazy var btnShare: UIButton = {
        let button = UIButton()
        button.backgroundColor = .clear
        button.setTitle("Share", for: .normal)
        button.setTitleColor(UIColor(named:"tooltipsAmenityNaviBtnColor")!, for: .normal)
        button.layer.cornerRadius = buttonHeight / 2
        button.titleLabel?.font = UIFont.amenityToolTipsTextFont()
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor(named:"tooltipsAmenityNaviBtnColor")?.cgColor ?? UIColor.purple.cgColor
        button.addTarget(self, action: #selector(onShareViaLocation), for: .touchUpInside)
        containerView.addSubview(button)
        return button
    }()
    
    private lazy var btnInvite: UIButton = {
        let button = UIButton()
        button.backgroundColor = .clear
        button.setTitle("Invite", for: .normal)
        button.setTitleColor(UIColor(named:"tooltipsAmenityNaviBtnColor")!, for: .normal)
        button.layer.cornerRadius = buttonHeight / 2
        button.titleLabel?.font = UIFont.amenityToolTipsTextFont()
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor(named:"tooltipsAmenityNaviBtnColor")?.cgColor ?? UIColor.purple.cgColor
        button.addTarget(self, action: #selector(onInviteLocation), for: .touchUpInside)
        containerView.addSubview(button)
        return button
    }()
    

    init(id: String, name: String, address: String, des: String, location: CLLocationCoordinate2D, baseUI: Bool = true, type: String = "", isCollectionDetail: Bool = false, address2: String = "", fullAddress: String = "") {
        
        super.init(id: id, name: name, address: address, location: location, baseUI: false)
        self.isCollectionDetail = isCollectionDetail
        
        if baseUI {
            self.backgroundColor = .clear
            self.bgColor = UIColor(named: "tooltipsAmenityBackgroundColor")!
            self.containerView.backgroundColor = self.bgColor
            self.tipHeight = 12.0
            self.tipWidth = 20.0
            self.cornerRadius = 16.0
            
            lblName.text = name
            if isCollectionDetail {
                if !fullAddress.isEmpty {
                    lblAddress.text = fullAddress
                } else {
                    if address == name {
                        lblAddress.text = ""
                    } else {
                        lblAddress.text = address
                    }
                }
            } else {
                lblAddress.text = address
            }
            
            lbDescription.text = "\"" + des + "\""
            
            self.showShadow = false
        }
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        btnNavigate.layer.cornerRadius = buttonHeight / 2
    }
    
    override func sizeThatFits(_ size: CGSize) -> CGSize {
        let leftPadding = onSaveAction == nil ? xPadding : xPadding + 24
        let lblNameSize = (lblName.text ?? "").height(withConstrainedWidth: popupWidth - xPadding - leftPadding, font: lblName.font)
        let lblDesSize = (lbDescription.text ?? "").height(withConstrainedWidth: popupWidth - xPadding - leftPadding, font: lbDescription.font)
        let heightShareBtn = buttonHeight + gap
        
        let heightInviteBtn = buttonHeight + gap
        //let lblNameSize = lblName.text!.height(withConstrainedWidth: popupWidth - 2 * xPadding, font: lblName.font)
        let lblAddrSize = (lblAddress.text ?? "").height(withConstrainedWidth: popupWidth - 2 * xPadding, font: lblAddress.font)
        let h = yPaddingBound + lblNameSize + yPadding + lblAddrSize + yPadding + lblDesSize + heightShareBtn + heightInviteBtn + gap + buttonHeight + yPaddingBound + tipHeight + 2
        
        return CGSize(width: popupWidth, height: h)
    }
    
    override func onSaveButtonClicked() {
        if isCollectionDetail {
            if isSaved {
                //  Unbookmark the amenity
                if let ID = self.bookmarkId, ID > 0 {
                    let deleteData: [String: Any] = ["bookmarkId": ID, "action": "DELETE"]
                    addOrDeleteDropPinLocationInCollectionDetail(deleteData)
                }
            }
        } else {
            super.onSaveButtonClicked()
        }
    }

    internal override func setupLayout() {
        
        lblName.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(yPaddingBound)
            
            make.leading.equalToSuperview().offset(xPadding)
            let leftPadding = onSaveAction == nil ? xPadding : xPadding + 24
            make.trailing.equalToSuperview().offset(-leftPadding)
        }
                
        lblAddress.snp.makeConstraints { (make) in
            make.top.equalTo(lblName.snp.bottom).offset(yPaddingBound)
            make.leading.equalToSuperview().offset(xPadding)
            make.trailing.equalToSuperview().offset(-xPadding)
            //make.centerX.equalToSuperview()
        }
        
        lbDescription.snp.makeConstraints { make in
            make.top.equalTo(lblAddress.snp.bottom).offset(yPaddingBound)
            make.leading.equalToSuperview().offset(xPadding)
            make.trailing.equalToSuperview().offset(-xPadding)
        }
        
        
        btnShare.snp.makeConstraints { make in
            make.top.equalTo(lbDescription.snp.bottom).offset(gap)
            make.centerX.equalTo(containerView.snp.centerX)
            make.width.equalTo(buttonWidth)
            make.height.equalTo(buttonHeight)
        }
        
        btnInvite.snp.makeConstraints { make in
            make.top.equalTo(btnShare.snp.bottom).offset(gap)
            make.centerX.equalTo(containerView.snp.centerX)
            make.width.equalTo(buttonWidth)
            make.height.equalTo(buttonHeight)
        }
        
        btnNavigate.snp.makeConstraints { make in
            make.bottom.equalToSuperview().offset(-yPaddingBound)
            make.top.equalTo(btnInvite.snp.bottom).offset(gap)
            make.centerX.equalTo(containerView.snp.centerX)
            make.width.equalTo(buttonWidth)
            make.height.equalTo(buttonHeight)
        }
        
        containerView.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.bottom.equalToSuperview().offset(-tipHeight)
            make.leading.equalToSuperview()
            make.width.equalTo(popupWidth)
            make.trailing.equalToSuperview()
        }
    }
}

//
//  ToolTipsView.swift
//  Breeze
//
//  Created by Zhou Hao on 23/12/21.
//

import UIKit
import MapboxNavigation
import CoreLocation
import Combine
import RxSwift
import SwiftyBeaver

protocol ToolTipsViewDelegate: AnyObject {

    func onPresented(toolTips: ToolTipsView)
    func onDismissed(toolTips: ToolTipsView)
}

protocol ToolTipsData {
    var tooltipCoordinate: CLLocationCoordinate2D { get }
    
    var tooltipId: String { get }
}

class ToolTipsView: UIView, ToolTipsData {

    // MARK: - Public Properties
    var tipHeight: CGFloat = 8.0
    var tipWidth: CGFloat = 20.0
    var bgColor: UIColor = .white // defaul to white
    var cornerRadius: CGFloat = 10.0
    var showShadow = true
    
    var screenName: String = ParameterName.Home.screen_view
    
    private var disposables = Set<AnyCancellable>()
    
    // Must override by any tooltip
    var tooltipCoordinate: CLLocationCoordinate2D {
        return CLLocationCoordinate2D()
    }
    
    var tooltipId: String {
        return ""
    }
    
    var amenityIdBookMark: String?
    
    /// onSaveAction
    /// @param **isSaved** - if has been saved
    var onSaveAction: ((_ isSaved: Bool) -> Void)? {
        didSet {
            onListenNotification()
            btnSave.isHidden = onSaveAction == nil
        }
    }
    
    var onCloseAction: (() -> Void)?
    
    var bookmarkId: Int?
    
    var address: String?
    
    var address2: String = ""
    
    var subscriptionBookmark: Disposable?
    
    /// If this is saved or not
    var isSaved = false {
        didSet {
            // update the button
            DispatchQueue.main.async {
                self.btnSave.setImage(UIImage(named: self.getImageName()), for: .normal)
            }
            
        }
    }
    
    var placeId: String?
    var userLocationLinkIdRef: Int?
    
    /// If the save icon small or not
    /// This should be only set once
    var isSaveIconSmall = true
    
    /// For drop pin tooltip - add close button
//    var needShowCloseButton = false {
//        didSet {
//            btnClose.isHidden = !needShowCloseButton
//        }
//    }
    
    var snapshotId: Int?
    
    /// Close button
//    private lazy var btnClose: UIButton = {
//        let button = UIButton()
//        button.setImage(UIImage(named: "toastClose"), for: .normal)
//        button.backgroundColor = .clear
//        button.addTarget(self, action: #selector(onCloseButtonClicked), for: .touchUpInside)
//        button.isHidden = true
//        containerView.addSubview(button)
//        return button
//    }()
    
    var availablePercentImageBand: String?
    var availabilityCSData: AvailabilityCSData?
    var currentParkingStatus: ParkingAvailabilityStatus {
        switch availablePercentImageBand {
        case "TYPE0":
            return .fullParking
        case "TYPE1":
            return .fewAvailableLots
        case "TYPE2":
            return .availableLots
        default:
            return .notAvailable
        }
    }
    
    var crowdsourceTSDesc: String {
        return availabilityCSData?.getUpdateTSDescDisplay() ?? ""
    }
    
    func getCSThemeColor() -> String {
        var bgColorHexString: String = ""
        if let csData = availabilityCSData {
            return csData.themeColor ?? ""
        } else {
            switch self.currentParkingStatus {
            case .availableLots:
                bgColorHexString = "933DD8"
            case .fewAvailableLots:
                bgColorHexString = "F26415"
            case .fullParking:
                bgColorHexString = "E82370"
            case .notAvailable:
                print("Do nothing")
            }
        }
        
        return bgColorHexString
    }
    
    func getCSIconName() -> String {
        var iconName: String = "carpark-availability-parking-icon1"
        switch self.currentParkingStatus {
        case .availableLots:
            iconName = "carpark-availability-parking-icon1"
        case .fewAvailableLots:
            iconName = "carpark-availability-parking-icon2"
        case .fullParking:
            iconName = "carpark-availability-parking-icon3"
        case .notAvailable:
            print("Do nothing")
        }
        return iconName
    }
    
    /// Save button
    private lazy var btnSave: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: getImageName()), for: .normal)
        button.backgroundColor = .clear
        button.addTarget(self, action: #selector(onSaveButtonClicked), for: .touchUpInside)
        button.isHidden = true // hide it first
        button.contentEdgeInsets = UIEdgeInsets(top: 8, left: 10, bottom: 8, right: 10)
        containerView.addSubview(button)
        return button
    }()

    private(set) lazy var containerView: UIView = {
        let view = UIView()
        view.backgroundColor = bgColor
        view.layer.cornerRadius = cornerRadius
        if needShowParking() {
            parkingAvailability.addSubview(view)
        }else {
            addSubview(view)
        }
        return view
    }()
    
    private(set) lazy var parkingAvailability: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.red
        view.layer.cornerRadius = cornerRadius
        addSubview(view)
        view.isHidden = true
        return view
    }()
    
    private func getImageName() -> String {
        return isSaved ? (isSaveIconSmall ? "saved-small-icon" : "saved-big-icon") : (isSaveIconSmall ? "unsaved-small-Icon" : "unsaved-big-Icon")
    }
    
    @objc func onCloseButtonClicked() {
        
    }

    @objc func onSaveButtonClicked() {
        if isSaved {
            
            AnalyticsManager.shared.logPopUpEvent(popupName: ParameterName.Home.UserPopup.unsaveCollection, eventName: ParameterName.RoutePlanning.UserPopUp.popup_open)
            
            self.window?.rootViewController?.popupAlert(title:"", message: Constants.titleConfirmDeselect, actionTitles: [Constants.cancelDeselect,Constants.confirmDeselect], actions:[{action1 in
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Home.UserClick.delete_save_cancel, screenName: ParameterName.Home.screen_view)
                AnalyticsManager.shared.logPopUpEvent(popupName: ParameterName.Home.UserPopup.unsaveCollection, eventName: ParameterName.RoutePlanning.UserPopUp.popup_close)
            },{action2 in
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Home.UserClick.delete_save_confirm, screenName: ParameterName.Home.screen_view)
                AnalyticsManager.shared.logPopUpEvent(popupName: ParameterName.Home.UserPopup.unsaveCollection, eventName: ParameterName.RoutePlanning.UserPopUp.popup_close)
                if let address = self.address, !address.isEmpty {
                    self.deselectBookmarkAddress(address1: address, address2: self.address2)
                } else  {
                    if let amenityId = self.amenityIdBookMark {
                        if !amenityId.isEmpty {
                            self.deselectBookmarkAmenity(amenityId: amenityId)
                        }
                    }
                }
            }, nil])
        } else if let callback = onSaveAction {
            /// let caller to decide the **isSaved** property
//            isSaved.toggle()
            callback(isSaved)
        }
    }
    
    func needShowParking() -> Bool {
        return false
    }
    
    internal func setupLayout() {
        /// Will be overrides
    }

    // https://github.com/mapbox/mapbox-gl-native/issues/9228
    override var center: CGPoint {
        set {
            var newCenter = newValue
            newCenter.y -= bounds.midY
            super.center = newCenter
        }
        get {
            return super.center
        }
    }

    weak var delegate: ToolTipsViewDelegate?
    
    // MARK: - Methods
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = cornerRadius
        containerView.layer.cornerRadius = cornerRadius
        layer.masksToBounds = true
        
        if showShadow {
            showShadow()
        }
        
//        btnSave.snp.makeConstraints { make in
//            make.top.equalToSuperview().offset(8)
//            make.trailing.equalToSuperview().offset(-10)
//        }
        btnSave.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(0)
            make.trailing.equalToSuperview().offset(0)
        }
    }
    
    override func draw(_ rect: CGRect) {
        let rect = self.bounds
        // Draw the pointed tip at the bottom.
        let fillColor: UIColor = bgColor
        
        let tipLeft = rect.origin.x + (rect.size.width / 2.0) - (tipWidth / 2.0)
        let tipBottom = CGPoint(x: rect.origin.x + (rect.size.width / 2.0), y: rect.origin.y + rect.size.height)
        let heightWithoutTip = rect.size.height - tipHeight - 1
        
        let currentContext = UIGraphicsGetCurrentContext()!
        
        let tipPath = CGMutablePath()
        tipPath.move(to: CGPoint(x: tipLeft, y: heightWithoutTip))
        tipPath.addLine(to: CGPoint(x: tipBottom.x, y: tipBottom.y))
        tipPath.addLine(to: CGPoint(x: tipLeft + tipWidth, y: heightWithoutTip))
        tipPath.closeSubpath()
        
        fillColor.setFill()
        currentContext.addPath(tipPath)
        currentContext.fillPath()
    }
            
    func present(from rect: CGRect, in view: UIView, constrainedTo constrainedRect: CGRect, animated: Bool) {
        present(in: view, animated: animated)
    }
    
    func present(in view: UIView, animated: Bool) {
        view.addSubview(self)
        
        self.setupLayout()
        self.sizeToFit()

        if animated {
            alpha = 0

            UIView.animate(withDuration: 0.2) { [weak self] in
                guard let self = self else { return }

                self.alpha = 1
                self.delegate?.onPresented(toolTips: self)
            }
        } else {
            self.delegate?.onPresented(toolTips: self)
        }
    }

    func dismiss(animated: Bool) {
        if (superview != nil) {
            if animated {
                UIView.animate(withDuration: 0.2, animations: { [weak self] in
                    guard let self = self else { return }
                    self.alpha = 0
                }, completion: { [weak self] _ in
                    guard let self = self else { return }
                    self.delegate?.onDismissed(toolTips: self)
                    self.removeFromSuperview()
                })
            } else {
                self.delegate?.onDismissed(toolTips: self)
                removeFromSuperview()
            }
        }
    }
    
    func processBookmarkUpdate(bookmark: Bookmark) {
        let lat = "\(tooltipCoordinate.latitude)"
        let long = "\(tooltipCoordinate.longitude)"
        self.isSaved = lat == bookmark.lat && long == bookmark.long
        self.bookmarkId = bookmark.bookmarkId
        
    }
    
    func removeBookmark() {
        self.isSaved = false
        self.bookmarkId = nil
        if appDelegate().isMaplandingInTop {
            self.dismiss(animated: true)
            AmenitiesSharedInstance.shared.reloadAmenityRequestInCaseUpdate()
        }
    }
}

extension ToolTipsView {
    func adjust(to location: CLLocationCoordinate2D, in map: NavigationMapView) {
        guard let screenCoordinate = map.mapView?.mapboxMap.point(for: location) else { return }
        
        let point = CGPoint(x: screenCoordinate.x, y: screenCoordinate.y)
        center = CGPoint(x: point.x, y: point.y - 23) // this is actually based on the icon size
        
    }
    
    func onListenNotification() {
        NotificationCenter.default.publisher(for: Notification.Name(Values.NotificationUpdateCollectionDetail), object: nil)
            .receive(on: DispatchQueue.main)
            .sink { [weak self] notification in
                guard let self = self, let data = notification.userInfo?["bookmarks"] else { return }
                // handle bookmark saved event from RN (it could send from other View instead of Maplanding)
                
                if let fromScreen = notification.userInfo?["fromScreen"] as? String {
                    if fromScreen == Values.Collection_From_Screen_Choose {
                        if let json = try? JSONSerialization.data(withJSONObject: data, options: .prettyPrinted),
                           let bookmarks = try? JSONDecoder().decode([Bookmark].self, from: json) {
                            if bookmarks.count == 1 { // only need to handle one tooltip click save
                                self.processBookmarkUpdate(bookmark: bookmarks[0])
                            }
                        }
                    }
                }
                
            }.store(in: &disposables)
    }
    
    private func actionRemoveBookmark() {
        guard let bookmarkId = bookmarkId else { return }
        let updateJson = ["bookmark_id": bookmarkId]
        self.subscriptionBookmark = ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.REMOVE_SHORTCUT_FROM_MAP, data: updateJson as NSDictionary).subscribe(onSuccess: { [weak self] result in
            guard let self = self else { return }
            print("DELETE Book mark",result)
            if let value = result?["result"] as? String {
                if value == "success" {
                    self.removeBookmark()
                }
            }
        },                 onFailure: {_ in
            // ...
        })
    }
    
    private func deselectBookmarkAmenity(amenityId: String) {
        BookmarkService.shared.deselectingBookmarksAmenity(amenityId: amenityId) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(_):
                DispatchQueue.main.async { [weak self] in
                    guard let self = self else { return }
                    self.removeBookmark()
                }
                
            case .failure(let error):
                SwiftyBeaver.error("Invalid bookmark amenity_id: \(error.localizedDescription)")
            }
        }
    }
    
    private func deselectBookmarkAddress(address1: String, address2: String) {
        BookmarkService.shared.deselectingBookmarksAddress(address1: address1, address2: address2) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(_):
                DispatchQueue.main.async { [weak self] in
                    guard let self = self else { return }
                    self.removeBookmark()
                }
                
            case .failure(let error):
                SwiftyBeaver.error("Invalid bookmark address: \(error.localizedDescription)")
            }
        }
    }
}

//extension ToolTipsView {
//    func showSavedIcon(saved: Bool, small: Bool = true) {
//        let name = saved ? (small ? "saved-small-icon" : "saved-big-icon") : (small ? "unsaved-small-Icon" : "unsaved-big-Icon")
//        let imageView = UIImageView(image: UIImage(named: name))
//        containerView.addSubview(imageView)
//        // update layout
//        imageView.snp.makeConstraints { make in
//            make.top.equalToSuperview().offset(8)
//            make.trailing.equalToSuperview().offset(-10)
//        }
//    }
//}

//
//  EVChargerToolTips.swift
//  Breeze
//
//  Created by Zhou Hao on 24/12/21.
//

import UIKit
import SnapKit
import CoreLocation

class EVChargerToolTips: ToolTipsView {

    // MARK: - Constants
    private var popupWidth: CGFloat = 166
    private let buttonHeight: CGFloat = 28
    private let buttonWidth: CGFloat = 147
    private let yPadding: CGFloat = 10
    private let xPadding: CGFloat = 10
    private let gap: CGFloat = 20
    private var location: CLLocationCoordinate2D
    private var evchargeId: String
    private var plugType = [String]()

    // MARK: - Public properties
    var onAdd: ((_ address: String, _ coordinate: CLLocationCoordinate2D) -> Void)?
    var onRemove: ((_ address: String, _ coordinate: CLLocationCoordinate2D) -> Void)?
    
    override var tooltipCoordinate: CLLocationCoordinate2D {
        return location
    }
    
    override var tooltipId: String {
        return evchargeId
    }
    
    // MARK: - Private properties
    private var isAdding = false
    
    private lazy var lblAddress: UILabel  =  {
        let label = UILabel()
        label.font = UIFont.amenityToolTipsTextFont()
        label.preferredMaxLayoutWidth = 120
        label.textAlignment = .center
        label.text = ""
        label.numberOfLines = 0
        label.textColor = UIColor(named: "tooltipsAmenityNameColor")!
        containerView.addSubview(label)
        return label
    }()

    private lazy var imgType: UIImageView = {
        let imageView = UIImageView()
        containerView.addSubview(imageView)
        return imageView
    }()
    
    private lazy var imgType1: UIImageView = {
        let imageView = UIImageView()
        containerView.addSubview(imageView)
        return imageView
    }()
    
    

    /*private lazy var imgEVIcon: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "evChargerTypeIcon"))
        containerView.addSubview(imageView)
        return imageView
    }()*/

    private lazy var lblTypeLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.availableCarparkLotsLabelFont()
        label.textColor = UIColor(named: "tooltipsERPLableColor")
        label.textAlignment = .center
//        label.text = "Type"
        containerView.addSubview(label)
        return label
    }()
    
    private lazy var lblTypeLabel1: UILabel = {
        let label = UILabel()
        label.font = UIFont.availableCarparkLotsLabelFont()
        label.textColor = UIColor(named: "tooltipsERPLableColor")
        label.textAlignment = .center
//        label.text = "Type"
        containerView.addSubview(label)
        return label
    }()

    /*private lazy var lblType: UILabel = {
        let label = UILabel()
        label.font = UIFont.availableCarparkLotsLabelFont()
        label.textColor = UIColor(named: "tooltipsERPLableColor")
        label.textAlignment = .right
        containerView.addSubview(label)
        return label
    }()*/
    
    // shared by add/remove
    private lazy var btnAction: UIButton = {
        let button = UIButton()
        button.backgroundColor = UIColor(named:"tooltipsAmenityNaviBtnColor")!
        button.setTitle("Add Stop", for: .normal) // "Add Stop" Change for pilot
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = UIFont.amenityToolTipsTextFont()
        button.addTarget(self, action: #selector(onActionClicked), for: .touchUpInside)
        containerView.addSubview(button)
        return button
    }()
    
    init(id: String, address: String, type: Array<String>, location: CLLocationCoordinate2D, isAdding: Bool,displayText:String,isShowAddBtn:Bool) {
        self.location = location
        self.evchargeId = id
        
        super.init(frame: .zero)
        
        self.backgroundColor = .clear
        self.bgColor = UIColor(named: "tooltipsAmenityBackgroundColor")!
        self.tipHeight = 12.0
        self.tipWidth = 20.0
        self.cornerRadius = 16.0
        
        lblAddress.text = address
        
                
        // TODO: To make it dynamically. We will make it dynamic once we change to architecture
        
        plugType = type
        
        if(type.count > 1){
            
            for type in type {
                
                if( type == "CCS/SAE"){
                    
                    self.lblTypeLabel1.text = "Combo-2"
                    self.imgType1.image = UIImage(named: "evChargerType1")
                }
                if(type == "Type 2"){
                    
                    self.lblTypeLabel.text = "type-2"
                    self.imgType.image = UIImage(named: "evchargertype2")
                }
            }

            
        }
        else{
            
            for type in type {
                
                if( type == "CCS/SAE"){
                    
                    self.lblTypeLabel.text = "Combo-2"
                    self.imgType.image = UIImage(named: "evChargerType1")
                }
                if(type == "Type 2"){
                    
                    self.lblTypeLabel.text = "type-2"
                    self.imgType.image = UIImage(named: "evchargertype2")
                }
            }

            
        }
        //lblType.text = type
        
        self.isAdding = isAdding
        
        if(isShowAddBtn){
            
            //btnAction.isHidden = isShowAddBtn
            btnAction.backgroundColor = UIColor(named: "addStopDisableColor")!
            btnAction.isEnabled = false
            
        }
        else{
            
            btnAction.isHidden = isShowAddBtn
            btnAction.isEnabled = true
            btnAction.backgroundColor = isAdding ? UIColor(named:"tooltipsAmenityNaviBtnColor")! : UIColor(named:"tooltipsRemoveBtnColor")!
            btnAction.setTitle((isAdding ? "Add Stop" : "Remove Stop"), for: .normal) // "Add Stop" Change for pilot
        }
        
        self.showShadow = false
//        setupLayout()
    }

    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        btnAction.layer.cornerRadius = buttonHeight / 2
    }
    
    override func sizeThatFits(_ size: CGSize) -> CGSize {
                
        let lblAddrSize = lblAddress.intrinsicContentSize

        let h = lblAddrSize.height + yPadding + 45 + yPadding + 45  + gap + buttonHeight + yPadding + tipHeight + 10
        
        return CGSize(width: popupWidth, height: h)
    }

    @objc private func onActionClicked() {
        
        if isAdding {
            if let callback = onAdd {
                
                callback(self.lblAddress.text ?? "", self.location)
            }
        } else {
            if let callback = onRemove {
                callback(self.lblAddress.text ?? "", self.location)
            }
        }

        dismiss(animated: true)
    }
    
    internal override func setupLayout() {
        lblAddress.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(yPadding)
            make.leading.equalToSuperview().offset(xPadding)
            make.trailing.equalToSuperview().offset(-xPadding)
        }
          
        if(plugType.count > 1){
            
            imgType.snp.makeConstraints { (make) in
                make.top.equalTo(lblAddress.snp.bottom).offset(yPadding)
                make.width.equalTo(49)
                make.height.equalTo(43)
                make.leading.equalToSuperview().offset(20)
            }
            
            imgType1.snp.makeConstraints { (make) in
                make.top.equalTo(lblAddress.snp.bottom).offset(yPadding)
                make.width.equalTo(45)
                make.height.equalTo(45)
                make.trailing.equalToSuperview().offset(-20)
            }
            
            lblTypeLabel.snp.makeConstraints { make in
                make.top.equalTo(imgType.snp.bottom).offset(9)
                make.centerX.equalTo(imgType.snp.centerX)
//                make.leading.equalToSuperview().offset(29)
                make.width.equalTo(60)
            }
            
            lblTypeLabel1.snp.makeConstraints { make in
                make.top.equalTo(imgType1.snp.bottom).offset(9)
                make.centerX.equalTo(imgType1.snp.centerX)
//                make.leading.equalTo(lblTypeLabel.snp.trailing).offset(25)
                make.width.equalTo(95)
            }
            
        }
        else{
            
            if self.lblTypeLabel.text == "type-2" {
                imgType.snp.makeConstraints { (make) in
                    make.top.equalTo(lblAddress.snp.bottom).offset(yPadding)
                    make.width.equalTo(49)
                    make.height.equalTo(43)
                    make.centerX.equalToSuperview()
                }
            } else {
                imgType.snp.makeConstraints { (make) in
                    make.top.equalTo(lblAddress.snp.bottom).offset(yPadding)
                    make.width.equalTo(45)
                    make.height.equalTo(45)
                    make.centerX.equalToSuperview()
                }
            }
            
            lblTypeLabel.snp.makeConstraints { make in
                make.top.equalTo(imgType.snp.bottom).offset(9)
                make.centerX.equalTo(imgType.snp.centerX)
//                make.leading.equalToSuperview().offset(61)
//                make.trailing.equalToSuperview().offset(-61)
                make.width.equalTo(60)
                make.height.equalTo(28)
            }
        }
        
        
       
        /*imgEVIcon.snp.makeConstraints { (make) in
            make.width.equalTo(16)
            make.height.equalTo(16)
            make.top.equalTo(imgType.snp.bottom).offset(yPadding)
            make.leading.equalToSuperview().offset(xPadding)
        }*/

        /*lblTypeLabel.snp.makeConstraints { make in
            make.centerY.equalTo(imgEVIcon.snp.centerY)
            make.leading.equalTo(imgEVIcon.snp.trailing).offset(8)
            make.width.equalTo(40)
        }
        
        lblType.snp.makeConstraints { make in
            make.centerY.equalTo(imgEVIcon.snp.centerY)
            make.trailing.equalTo(-xPadding)
            make.leading.equalTo(lblTypeLabel.snp.trailing).offset(xPadding)
        }*/
        
        btnAction.snp.makeConstraints { make in
            make.bottom.equalToSuperview().offset(-yPadding)
            make.top.equalTo(lblTypeLabel.snp.bottom).offset(gap)
            make.centerX.equalTo(containerView.snp.centerX)
            make.width.equalTo(buttonWidth)
            make.height.equalTo(buttonHeight)
        }
        
        containerView.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.bottom.equalToSuperview().offset(-tipHeight)
            make.leading.equalToSuperview()
            make.width.equalTo(plugType.count > 1 ? popupWidth + 20 : popupWidth)
            make.trailing.equalToSuperview()
        }
    }
}

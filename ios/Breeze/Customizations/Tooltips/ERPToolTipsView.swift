//
//  ERPToolTipsView.swift
//  Breeze
//
//  Created by Zhou Hao on 23/12/21.
//

import UIKit
import CoreLocation

class ERPToolTipsView: ToolTipsView {

    // MARK: - Constants
    private var popupWidth: CGFloat = 171
    private let yPadding: CGFloat = 12
    private let xPadding: CGFloat = 14
    private let gap: CGFloat = 16
    
    // MARK: - Public properties
    override var tooltipCoordinate: CLLocationCoordinate2D {
        return coordinate
    }
    
    override var tooltipId: String {
        return erpId
    }

    private lazy var lblName: UILabel  =  {
        let label = UILabel()
        label.font = UIFont.amenityToolTipsERPTextFont()
        label.preferredMaxLayoutWidth = showMoreInfo ? 114 : 145
        label.textAlignment = .left
        label.text = ""
        label.numberOfLines = 0
        label.textColor = UIColor(named: "tooltipsAmenityNameColor")!
        containerView.addSubview(label)
        return label
    }()
    
    private lazy var imgCost: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "erpDollarFree"))
        containerView.addSubview(imageView)
        return imageView
    }()

    // current label
    private lazy var lblCostLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.amenityToolTipsAddressFont()
        label.textColor = UIColor(named: "tooltipsERPLableColor")!
        label.textAlignment = .left
        label.text = "Current"
        label.numberOfLines = 0
        containerView.addSubview(label)
        return label
    }()

    // current price
    private lazy var lblCost: UILabel = {
        let label = UILabel()
        label.font = UIFont.amenityToolTipsERPTextFont()
        label.textColor = UIColor(named: "tooltipsAmenityNameColor")!
        label.textAlignment = .right
        label.text = "$0.00"
        containerView.addSubview(label)
        return label
    }()
    
    // next time string
    private lazy var lblNextTimeString: UILabel = {
        let label = UILabel()
        label.font = UIFont.amenityToolTipsAddressFont()
        label.textColor = UIColor(named: "tooltipsERPLableColor")!
        label.textAlignment = .left
        label.text = ""
        containerView.addSubview(label)
        return label
    }()

    private lazy var lblNextCost: UILabel = {
        let label = UILabel()
        label.font = UIFont.amenityToolTipsERPTextFont()
        label.textColor = UIColor(named: "tooltipsAmenityNameColor")!
        label.textAlignment = .right
        //label.text = "$0.00"
        containerView.addSubview(label)
        return label
    }()

    private lazy var btnMoreInfo: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "moreInfoIcon"), for: .normal)
        button.addTarget(self, action: #selector(onMoreInfoClicked), for: .touchUpInside)
        containerView.addSubview(button)
        return button
    }()

    private var erpId: String!
    private var showMoreInfo = false    
    private var coordinate: CLLocationCoordinate2D!
    
    var currentTime: String?
    
    var onMoreInfo: ((_ erpId: String) -> Void)?
    
    @objc private func onMoreInfoClicked() {
        if let callback = onMoreInfo {
            callback(erpId)
        }
    }

    required init(id: String, name: String, rate: Double, nextText: String, nextRate: Double, location: CLLocationCoordinate2D, showMoreInfo: Bool = false, selectedTimeErp: String) {
        super.init(frame: .zero)

        self.erpId = id
        self.showMoreInfo = showMoreInfo
        self.backgroundColor = .clear
        self.bgColor = UIColor(named: "tooltipsAmenityBackgroundColor")!
        self.tipHeight = 12.0
        self.coordinate = location
        lblName.text = name
        lblCostLabel.text = selectedTimeErp

        imgCost.image = UIImage(named: rate == 0.0 ? "erpDollarFree" : "erpDollar")
        lblCost.text = String(format: "$%.2f", rate)
            
        lblNextTimeString.text = nextText
        if(nextRate != 0){
            lblNextCost.text = String(format: "$%.2f", nextRate) 
        }
               
        self.showShadow = false
//        setupLayout()
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    internal override func setupLayout() {
        
        if showMoreInfo {
            btnMoreInfo.snp.makeConstraints { make in
                make.centerY.equalTo(lblName.snp.centerY)
                make.trailing.equalToSuperview().offset(-xPadding)
                make.width.equalTo(17)
                make.height.equalTo(17)
            }
            lblName.snp.makeConstraints { (make) in
                make.top.equalToSuperview().offset(yPadding)
                make.leading.equalToSuperview().offset(xPadding)
                make.trailing.equalTo(btnMoreInfo.snp.leading).offset(-xPadding)
            }
        } else {
            lblName.snp.makeConstraints { (make) in
                make.top.equalToSuperview().offset(yPadding)
                make.leading.equalToSuperview().offset(xPadding)
                make.trailing.equalToSuperview().offset(-xPadding)
            }
        }
        
        imgCost.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(xPadding)
            make.top.equalTo(lblName.snp.bottom).offset(yPadding)
            make.height.equalTo(17)
            make.width.equalTo(17)
        }
        
        lblCostLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(imgCost.snp.trailing).offset(5)
            make.centerY.equalTo(imgCost.snp.centerY)
            make.width.equalTo(90)
        }

        lblCost.snp.makeConstraints { (make) in
            make.centerY.equalTo(imgCost.snp.centerY)
            make.trailing.equalToSuperview().offset(-xPadding)
        }

        lblNextTimeString.snp.makeConstraints { (make) in
            make.leading.equalTo(lblCostLabel.snp.leading)
            make.top.equalTo(lblCost.snp.bottom).offset(yPadding)
            make.width.equalTo(90)
        }

        lblNextCost.snp.makeConstraints { (make) in
            make.centerY.equalTo(lblNextTimeString.snp.centerY)
            make.trailing.equalToSuperview().offset(-yPadding)
        }

        containerView.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.bottom.equalToSuperview().offset(-tipHeight)
            make.leading.equalToSuperview()
            make.width.equalTo(popupWidth)
            make.trailing.equalToSuperview()
        }
    }
    
    override func sizeThatFits(_ size: CGSize) -> CGSize {
                
        let lblNameSize = lblName.intrinsicContentSize
        let lblCostSize = lblCost.intrinsicContentSize
        let lblNextCostSize = lblNextTimeString.intrinsicContentSize
                        
        let h = lblNameSize.height + yPadding + lblCostSize.height + yPadding + lblNextCostSize.height + yPadding + gap + tipHeight

        return CGSize(width: popupWidth, height: h)
    }
}

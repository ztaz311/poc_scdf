//
//  DropPinToolTipView.swift
//  Breeze
//
//  Created by Tan, Tran Ngoc on 03/11/2023.
//

import UIKit
import SnapKit
import CoreLocation
import SwiftyBeaver

class DropPinToolTipView: ToolTipsView {
    
    private var popupWidth: CGFloat = 169
    private var buttonHeight: CGFloat = 28
//    private var buttonWidth: CGFloat = 147
    private let yPadding: CGFloat = 8
    private let yPaddingBound: CGFloat = 15
    private let xPadding: CGFloat = 12
    private let gap: CGFloat = 14
//    private var amenityId: String
    private var type: String = ""
    private var dropPinModel: DropPinModel?
    private var isFromDetailCollection: Bool = false
    private var isShareCollection: Bool = false
    private var userLocationLinkId: Int?
    private let nameFont = UIFont.amenityToolTipsTextFont()
    private var collectionDetailScreenName: String = ""
    private let parkingInfoHeight: CGFloat = 71
    private var hasParkingAvailable: Bool = false

    // MARK: - Public properties
    override var tooltipCoordinate: CLLocationCoordinate2D {
        return location
    }
    
    override var tooltipId: String {
        return ""
    }
    
    private var location: CLLocationCoordinate2D!
    
    var needShowCloseButton = false {
        didSet {
            btnClose.isHidden = !needShowCloseButton
        }
    }
    
    var onNavigationHere: ((_ name: String, _ address: String, _ coordinate: CLLocationCoordinate2D) -> Void)?
    
    var shareViaLocation: (() -> Void)?
    
    var inviteLocation: (() -> Void)?
    
    private lazy var imvAnE: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "ic_ane")
        containerView.addSubview(imageView)
        return imageView
    }()
    
    private lazy var lblName: UILabel  =  {
        let label = UILabel()
        label.font = UIFont.amenityToolTipsTextFont()
        label.preferredMaxLayoutWidth = popupWidth - 2 * xPadding - 44
        label.textAlignment = .left
        label.text = ""
        label.numberOfLines = 0
        label.textColor = UIColor(named: "tooltipsAmenityNameColor")!
        containerView.addSubview(label)
        return label
    }()

    private lazy var lblAddress: UILabel  =  {
        let label = UILabel()
        label.font = UIFont.amenityToolTipsAddressFont()
        label.preferredMaxLayoutWidth = popupWidth - 2 * xPadding
        label.textAlignment = .left
        label.text = ""
        label.numberOfLines = 0
        label.textColor = UIColor(named: "tooltipsAmenityAddressColor")!
        containerView.addSubview(label)
        return label
    }()

    private lazy var btnNavigate: UIButton = {
        let button = UIButton()
        button.backgroundColor = UIColor(named:"tooltipsAmenityNaviBtnColor")!
        button.setTitle("Navigate Here", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.layer.cornerRadius = buttonHeight / 2
        button.titleLabel?.font = UIFont.amenityToolTipsTextFont()
        button.addTarget(self, action: #selector(onNavigateClicked), for: .touchUpInside)
        containerView.addSubview(button)
        
        return button
    }()
    
    private lazy var btnShare: UIButton = {
        let button = UIButton()
        button.backgroundColor = .clear
        button.setTitle("Share", for: .normal)
        button.setTitleColor(UIColor(named:"tooltipsAmenityNaviBtnColor")!, for: .normal)
        button.layer.cornerRadius = buttonHeight / 2
        button.titleLabel?.font = UIFont.amenityToolTipsTextFont()
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor(named:"tooltipsAmenityNaviBtnColor")?.cgColor ?? UIColor.purple.cgColor
        button.addTarget(self, action: #selector(onShareViaLocation), for: .touchUpInside)
        containerView.addSubview(button)
        return button
    }()
    
    
    private lazy var btnInvite: UIButton = {
        let button = UIButton()
        button.backgroundColor = .clear
        button.setTitle("Invite", for: .normal)
        button.setTitleColor(UIColor(named:"tooltipsAmenityNaviBtnColor")!, for: .normal)
        button.layer.cornerRadius = buttonHeight / 2
        button.titleLabel?.font = UIFont.amenityToolTipsTextFont()
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor(named:"tooltipsAmenityNaviBtnColor")?.cgColor ?? UIColor.purple.cgColor
        button.addTarget(self, action: #selector(onInviteLocation), for: .touchUpInside)
        containerView.addSubview(button)
        return button
    }()
    
    
    private lazy var btnClose: UIButton = {
        let button = UIButton()
        button.backgroundColor = .clear
        button.setTitle("Close", for: .normal)
        button.setTitleColor(UIColor(named:"tooltipsAmenityNaviBtnColor")!, for: .normal)
        button.layer.cornerRadius = buttonHeight / 2
        button.titleLabel?.font = UIFont.amenityToolTipsTextFont()
        button.addTarget(self, action: #selector(onCloseButtonClicked), for: .touchUpInside)
        containerView.addSubview(button)
        return button
    }()
    
    //  MARK: Parking availability
    
    private lazy var parkingStatusContentView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(hexString: "F5EEFC", alpha: 1)
        view.layer.cornerRadius = 8
        parkingAvailability.addSubview(view)
        return view
    }()
    
    private lazy var parkingIcon: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "carpark-availability-parking-icon1"))
        parkingStatusContentView.addSubview(imageView)
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var parkingAvailableStatusLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.parkingAvailableTextFont()
        label.textColor = UIColor(hexString: "222638", alpha: 1)
        label.textAlignment = .left
        parkingStatusContentView.addSubview(label)
        label.text = ""
        return label
    }()
    
    private lazy var parkingTriangleIcon: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "carpark-availability-triangle"))
        parkingAvailability.addSubview(imageView)
        return imageView
    }()
    
    private lazy var parkingUpdatedTimeLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.parkingAvailableUpdateTimeFont()
        label.textColor = .white
        label.textAlignment = .center
        parkingAvailability.addSubview(label)
        label.text = ""
        return label
    }()

    init(pinModel: DropPinModel, location: CLLocationCoordinate2D, baseUI: Bool = true, type: String = "", isFromDetailCollection: Bool = false, screen: String = ParameterName.CollectionDetail.custom_saved_places_collection, isShareCollection: Bool = false, bookmarkId: Int? = 0, userLocationLinkId: Int? = 0, availablePercentImageBand: String? = "", availabilityCSData: AvailabilityCSData? = nil) {
        
        self.location = location
        self.type = type
        self.dropPinModel = pinModel
        self.isFromDetailCollection = isFromDetailCollection
        self.isShareCollection = isShareCollection
        self.userLocationLinkId = userLocationLinkId
        self.collectionDetailScreenName = screen
        
        if availabilityCSData != nil && isShareCollection {
            self.hasParkingAvailable = true
        }

        super.init(frame: .zero)
        
        self.bookmarkId = bookmarkId
        self.availablePercentImageBand = availablePercentImageBand
        self.availabilityCSData = availabilityCSData
       
        if baseUI {
            self.backgroundColor = .clear
            self.bgColor = UIColor(named: "tooltipsAmenityBackgroundColor")!
            self.containerView.backgroundColor = self.bgColor
            self.tipHeight = 12.0
            self.tipWidth = 20.0
            self.cornerRadius = 16.0
            self.parkingAvailability.layer.cornerRadius = 16.0
            self.containerView.layer.cornerRadius = 16.0
            
            lblName.text = dropPinModel?.name
            
            if isShareCollection {
                if let fullAddress = pinModel.fullAddress, !fullAddress.isEmpty {
                    lblAddress.text = fullAddress
                } else {
                    if dropPinModel?.address == dropPinModel?.name {
                        if let address2 = pinModel.address2, !address2.isEmpty {
                            lblAddress.text = address2
                        } else {
                            lblAddress.text = ""
                        }
                    } else {
                        if let address = dropPinModel?.address, !address.isEmpty {
                            lblAddress.text = address
                        } else if let address2 = pinModel.address2, !address2.isEmpty {
                            lblAddress.text = address2
                        } else {
                            lblAddress.text = "\(location.latitude), \(location.longitude)"
                        }
                    }
                }
            } else {
                if let address = dropPinModel?.address, !address.isEmpty {
                    lblAddress.text = address
                } else {
                    lblAddress.text = "\(location.latitude), \(location.longitude)"
                }
            }
            self.showShadow = false
            
            //  Handle parking available stype here
            updateParkingAvailableStyle()
        }
    }
    
    override func needShowParking() -> Bool {
        return hasParkingAvailable
    }
    
    private func updateParkingAvailableStyle() {
        
        parkingAvailability.isHidden = !hasParkingAvailable
        parkingStatusContentView.isHidden = !hasParkingAvailable
        parkingTriangleIcon.isHidden = !hasParkingAvailable
        parkingUpdatedTimeLabel.isHidden = !hasParkingAvailable
        
        parkingUpdatedTimeLabel.text = crowdsourceTSDesc
        parkingAvailableStatusLabel.text = availabilityCSData?.title ?? ""
        
        let bgColorHexString: String = getCSThemeColor()
        parkingAvailability.backgroundColor = UIColor(hexString: bgColorHexString, alpha: 1)
        parkingIcon.image = UIImage(named: getCSIconName())
    }

    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
    }
    
    func setButtonHeight(_ height: CGFloat) {
        buttonHeight = height
    }

    @objc func onNavigateClicked() {
        if let callback = onNavigationHere {
            
            let extraData: NSDictionary = ["location_name": "location_\(dropPinModel?.name ?? "")", "longitude": dropPinModel?.long ?? "", "latitude": dropPinModel?.lat ?? ""]
            
            if isShareCollection {
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.ShareCollectionRecipient.UserClick.tooltip_saved_navigate_here, screenName: ParameterName.ShareCollectionRecipient.screen_view, extraData: extraData)
            } else if isFromDetailCollection {
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CollectionDetail.UserClick.tooltip_saved_navigate_here, screenName: collectionDetailScreenName, extraData: extraData)
            } else {
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Home.UserClick.tooltip_saved_navigate_here, screenName: ParameterName.Home.homepage_screen_view, extraData: extraData)
            }
            
            callback(dropPinModel?.name ?? "", dropPinModel?.address ?? "", self.location)
        }

        dismiss(animated: true)
    }
    
    @objc private func onShareViaLocation() {
        if isShareCollection {
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.ShareCollectionRecipient.UserClick.tooltip_saved_share, screenName: ParameterName.ShareCollectionRecipient.screen_view)
        } else if isFromDetailCollection {
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CollectionDetail.UserClick.tooltip_saved_share, screenName: collectionDetailScreenName)
        } else {
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Home.UserClick.tooltip_saved_share, screenName: ParameterName.Home.homepage_screen_view)
        }
        
        shareViaLocation?()
    }
    
    
    @objc func onInviteLocation() {
        inviteLocation?()
//        dismiss(animated: true)
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Home.UserClick.tooltip_saved_invite, screenName: screenName)
    }
    
    
    
    @objc private func onCloseAction() {
        dismiss(animated: true)
    }
    
    override func sizeThatFits(_ size: CGSize) -> CGSize {
        let lblNameSize = lblName.intrinsicContentSize
        let lblAddrSize = lblAddress.intrinsicContentSize
        let heightShareButton = buttonHeight
        let heightInviteButton = buttonHeight + gap
        let heightCloseButton = buttonHeight
        
        let h = yPadding + lblNameSize.height + yPadding + lblAddrSize.height + gap +  heightShareButton + gap + heightInviteButton + yPaddingBound + buttonHeight + yPaddingBound + (needShowCloseButton ? heightCloseButton : 0) + tipHeight + (needShowParking() ? parkingInfoHeight: 0)
        
        return CGSize(width: popupWidth, height: h)
    }

    internal override func setupLayout() {
        lblName.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(yPadding)
            make.leading.equalToSuperview().offset(xPadding)
            let leftPadding = onSaveAction == nil ? xPadding + 20 : xPadding + 44
            make.trailing.equalToSuperview().offset(-leftPadding)
        }
        
        lblAddress.snp.makeConstraints { (make) in
            make.top.equalTo(lblName.snp.bottom).offset(yPadding)
            make.leading.equalToSuperview().offset(xPadding)
            make.trailing.equalToSuperview().offset(-xPadding)
        }
        
        
        btnShare.snp.makeConstraints { make in
            make.centerX.equalTo(containerView.snp.centerX)
            make.top.equalTo(lblAddress.snp.bottom).offset(yPaddingBound)
            make.leading.equalToSuperview().offset(xPadding)
            make.trailing.equalToSuperview().offset(-xPadding)
            make.height.equalTo(buttonHeight)
        }
        
        btnInvite.snp.makeConstraints { make in
            make.centerX.equalTo(containerView.snp.centerX)
            make.top.equalTo(btnShare.snp.bottom).offset(yPaddingBound)
            make.leading.equalToSuperview().offset(xPadding)
            make.trailing.equalToSuperview().offset(-xPadding)
            make.height.equalTo(buttonHeight)
        }
        
        if needShowCloseButton {
            btnNavigate.snp.makeConstraints { make in
                make.centerX.equalTo(containerView.snp.centerX)
                make.leading.equalToSuperview().offset(xPadding)
                make.trailing.equalToSuperview().offset(-xPadding)
                make.top.equalTo(btnInvite.snp.bottom).offset(gap)
            }
            
            btnClose.snp.makeConstraints { make in
                make.centerX.equalTo(containerView.snp.centerX)
                make.top.equalTo(btnNavigate.snp.bottom).offset(yPaddingBound)
                make.leading.equalToSuperview().offset(xPadding)
                make.trailing.equalToSuperview().offset(-xPadding)
                make.height.equalTo(buttonHeight)
                make.bottom.equalToSuperview().offset(-yPaddingBound)
            }
            
        } else {
            btnNavigate.snp.makeConstraints { make in
                make.centerX.equalTo(containerView.snp.centerX)
                make.leading.equalToSuperview().offset(xPadding)
                make.trailing.equalToSuperview().offset(-xPadding)
                make.top.equalTo(btnInvite.snp.bottom).offset(gap)
                make.bottom.equalToSuperview().offset(-yPaddingBound)
            }
        }

//        containerView.snp.makeConstraints { (make) in
//            make.top.equalToSuperview()
//            make.bottom.equalToSuperview().offset(-tipHeight)
//            make.leading.equalToSuperview()
//            make.width.equalTo(popupWidth)
//            make.trailing.equalToSuperview()
//        }
        
        //  MARK: - Parking available
        parkingStatusContentView.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(8)
            make.leading.equalToSuperview().offset(8)
            make.height.equalTo(36)
            make.trailing.equalToSuperview().offset(-8)
        }
        
        parkingIcon.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(10)
            make.leading.equalToSuperview().offset(8)
            make.height.equalTo(17)
            make.width.equalTo(17)
        }
        
        parkingAvailableStatusLabel.snp.makeConstraints { make in
            make.centerY.equalTo(parkingIcon.snp.centerY)
            make.leading.equalToSuperview().offset(29)
            make.trailing.equalToSuperview().offset(10)
        }
        
        parkingTriangleIcon.snp.makeConstraints { make in
            make.top.equalTo(parkingStatusContentView.snp.bottom).offset(-3)
            make.leading.equalTo(parkingStatusContentView).offset(12)
            make.height.equalTo(10)
            make.width.equalTo(14)
        }
        
        parkingUpdatedTimeLabel.snp.makeConstraints { make in
            make.top.equalTo(parkingStatusContentView.snp.bottom).offset(10)
            make.leading.equalToSuperview().offset(8)
            make.trailing.equalToSuperview().offset(-8)
        }
        
        containerView.snp.makeConstraints { (make) in
            if needShowParking() {
                make.top.equalToSuperview().offset(parkingInfoHeight)
                make.bottom.equalToSuperview()
            }else {
                make.top.equalToSuperview().offset(0)
                make.bottom.equalToSuperview().offset(-tipHeight)
            }
            
            make.leading.equalToSuperview()
            make.width.equalTo(popupWidth)
            make.trailing.equalToSuperview()
        }
        
        parkingAvailability.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.bottom.equalToSuperview().offset(-tipHeight)
            make.leading.equalToSuperview()
            make.width.equalTo(popupWidth)
            make.trailing.equalToSuperview()
        }
    }
    
    override func onCloseButtonClicked() {
        if let callback = onCloseAction {
            callback()
        }
    }
    
    override func onSaveButtonClicked() {
        
        if isSaved {
            //  If unbookmark from share location or detail colleciton no need to show confirmation from Native side
            
            if isShareCollection {
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.ShareCollectionRecipient.UserClick.tooltip_saved_unbookmark, screenName: ParameterName.ShareCollectionRecipient.screen_view)
            } else if isFromDetailCollection {
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CollectionDetail.UserClick.tooltip_saved_unbookmark, screenName: collectionDetailScreenName)
            } else {
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Home.UserClick.tooltip_saved_unbookmark, screenName: ParameterName.Home.homepage_screen_view)
            }
            
            if isShareCollection || isFromDetailCollection {
                //  Delete bookmark
                if let ID = self.bookmarkId, ID > 0 {
                    self.removeDropPinLocation(ID) { [weak self] deletedId in
                        if let bmId = deletedId, bmId > 0 {
                            self?.onSavedDropPinLocation(false, bookmarkId: nil)
                            
                            if self?.isShareCollection == true {
                                NotificationCenter.default.post(name: .onReloadShareLocation, object: true)
                            }
                        }
                    }
                }
            } else {
                
                self.window?.rootViewController?.popupAlert(title:"", message: Constants.titleConfirmDeselect, actionTitles: [Constants.cancelDeselect,Constants.confirmDeselect], actions:[{action1 in
                    
                },{action2 in
                    //  Delete bookmark
                    if let ID = self.bookmarkId, ID > 0 {
                        self.removeDropPinLocation(ID) { [weak self] deletedId in
                            if let bmId = deletedId, bmId > 0 {
                                self?.onSavedDropPinLocation(false, bookmarkId: nil)
                            }
                        }
                    }
                    
                }, nil])
            }
            
            
        } else if let _ = onSaveAction {
            
            if isShareCollection {
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.ShareCollectionRecipient.UserClick.tooltip_saved_bookmark, screenName: ParameterName.ShareCollectionRecipient.screen_view)
            } else if isFromDetailCollection {
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CollectionDetail.UserClick.tooltip_saved_bookmark, screenName: collectionDetailScreenName)
            } else {
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Home.UserClick.tooltip_saved_bookmark, screenName: ParameterName.Home.homepage_screen_view)
            }
            
            if let model = dropPinModel {
                saveDropPinLocation(model)
            }
        }
        
//        super.onSaveButtonClicked()
//        let value = isSaved ? ParameterName.Home.UserToggle.amenities_tooltip_save_off : ParameterName.Home.UserToggle.amenities_tooltip_save_on
//        AnalyticsManager.shared.logClickEvent(eventValue: value, screenName: screenName)
    }
    
    override func removeBookmark() {
        super.removeBookmark()
    }
    
    func onSavedDropPinLocation(_ isSaved: Bool, bookmarkId: Int?) {
        self.isSaved = isSaved
        self.bookmarkId = bookmarkId
    }
    
    func saveDropPinLocation(_ dropPin: DropPinModel) {
        //  Send save pin location event to RN
        
        let name: String = dropPin.name ?? ""
//        let address = dropPin.address ?? ""
        
        var savedData: [String: Any] = [:]
        var bookmark: [String: Any] = ["address1": name, "name": name, "lat": dropPin.lat, "long": dropPin.long]
        if let amenityId = dropPin.amenityId, !amenityId.isEmpty {
            bookmark["amenityId"] = amenityId
        }
        if let ID = snapshotId, ID > 0 {
            bookmark["bookmarkSnapshotId"] = ID
        }
        if let ID = self.bookmarkId, ID > 0 {
            bookmark["bookmarkId"] = ID
        }
        if let ID = userLocationLinkId, ID > 0 {
            bookmark["userLocationLinkId"] = ID
        }
        bookmark["saved"] = isSaved
        if let address2 = dropPin.address2, !address2.isEmpty {
            bookmark["address2"] = address2
        }
        if let fullAddress = dropPin.fullAddress, !fullAddress.isEmpty {
            bookmark["fullAddress"] = fullAddress
        }
        
        if let ID = self.placeId {
            bookmark["placeId"] = ID
        }
        
        if let refId = self.userLocationLinkIdRef {
            bookmark["userLocationLinkIdRef"] = refId
        }
        
        savedData["bookmark"] = bookmark
        savedData["action"] = "ADD"
        print("savedData: \(savedData)")
        
        if isFromDetailCollection {
            addOrDeleteDropPinLocationInCollectionDetail(savedData, completion: nil)
        } else if isShareCollection {
            addOrDeleteLocationInSaveCollectionScreen(savedData)
        } else {
            addOrDeleteDropPinLocation(savedData, completion: nil)
        }
    }
    
    func removeDropPinLocation(_ bookmarkId: Int, completion: ((Int?) -> Void)? = nil) {
        var deleteData: [String: Any] = ["bookmarkId": bookmarkId, "action": "DELETE"]
        if let ID = userLocationLinkId, ID > 0 {
            deleteData["userLocationLinkId"] = ID
        }
        
        if isFromDetailCollection {
            addOrDeleteDropPinLocationInCollectionDetail(deleteData) { bookmarkId in
                completion?(bookmarkId)
                SwiftyBeaver.debug("DELETE bookmark ID: \(bookmarkId ?? 0)")
            }
        } else if isShareCollection {
            addOrDeleteLocationInSaveCollectionScreen(deleteData) { bookmarkId in
                completion?(bookmarkId)
                SwiftyBeaver.debug("DELETE bookmark ID: \(bookmarkId ?? 0)")
            }
        } else {
            addOrDeleteDropPinLocation(deleteData) { bookmarkId in
                completion?(bookmarkId)
                SwiftyBeaver.debug("DELETE bookmark ID: \(bookmarkId ?? 0)")
            }
        }
    }
}



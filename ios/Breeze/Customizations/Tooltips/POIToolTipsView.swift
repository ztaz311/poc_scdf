//
//  POIToolTipsView.swift
//  Breeze
//
//  Created by Zhou Hao on 12/5/22.
//

import UIKit
import CoreLocation

struct POI {
    var id: String
    var name : String
    var address: String
    var lat : Double
    var long : Double
    var text: String
    var lightColor: String
    var darkColor: String
}

final class POIToolTipsView: ToolTipsView {

    // MARK: - Constants
    private var popupWidth: CGFloat = 166
    private let xPadding: CGFloat = 14
    private let yPadding: CGFloat = 10
    private let gap: CGFloat = 20 // gap between labels and button
    private let buttonWidth: CGFloat = 147.0
    private let buttonHeight: CGFloat = 28.0
    private let labelWidth: CGFloat = 65.0
    private let parkingInfoHeight: CGFloat = 71
    private var hasCrowdsourced: Bool = false
    
    // MARK: - Public properties
    var onNavigateHere: ((_ poi: POI) -> Void)?
    var onMoreInfo: ((_ poi: POI) -> Void)?
    var onWalkHere: ((_ poi: POI) -> Void)?

    // MARK: - Private properties
    private var poi: POI!
    private var walkingEnabled = false
    private var navigationEnabled = false
    
    private var isAdditionalInfoPoi = false
    var textAdditionalInfo: String?
    var styleLightColor: String
    var styleDarkColor: String
    
    private (set) var coordinate: CLLocationCoordinate2D!
    
    override func needShowParking() -> Bool {
        return hasCrowdsourced
    }
    
    private lazy var btnMoreInfo: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "moreInfoIcon"), for: .normal)
        button.addTarget(self, action: #selector(onMoreInfoClicked), for: .touchUpInside)
        containerView.addSubview(button)
        return button
    }()
    
    private lazy var lblName: UILabel  =  {
        let label = UILabel()
        label.font = UIFont.amenityToolTipsERPTextFont()
        label.preferredMaxLayoutWidth = popupWidth - 2 * xPadding
        label.textAlignment = .left
        label.numberOfLines = 0
        label.textColor = UIColor(named: "tooltipsAmenityNameColor")
        containerView.addSubview(label)
        return label
    }()
    
    private lazy var lblAdditionalInfoPoi: UILabel  =  {
        let label = UILabel()
        label.font = UIFont.amenityToolTipsERPTextFont()
        label.preferredMaxLayoutWidth = popupWidth - 2 * xPadding
        label.textAlignment = .left
        label.text = "abcnfdkjasnfdjk"
        label.numberOfLines = 0
        label.textColor = UIColor(named: "trafficHeavyColor")!
        containerView.addSubview(label)
        return label
    }()
    
    private lazy var lblAddress: UILabel = {
        let label = UILabel()
        label.font = UIFont.availableCarparkLotsLabelFont()
        label.preferredMaxLayoutWidth = popupWidth - 2 * xPadding
        label.textColor = UIColor(named: "tooltipsERPLableColor")
        label.textAlignment = .left
        label.numberOfLines = 0
        label.text = ""
        containerView.addSubview(label)
        return label
    }()

    private lazy var btnWalk: UIButton = {
        let button = UIButton()
        button.setTitle("Walk Here", for: .normal)
        button.setTitleColor(UIColor(named:"tooltipsAmenityNaviBtnColor")!, for: .normal)
        button.titleLabel?.font = UIFont.amenityToolTipsTextFont()
        button.backgroundColor = .clear
        button.layer.cornerRadius = buttonHeight / 2
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor(named:"tooltipsAmenityNaviBtnColor")?.cgColor ?? UIColor.purple.cgColor
        button.addTarget(self, action: #selector(onWalkHereClicked), for: .touchUpInside)
        containerView.addSubview(button)
        return button
    }()
    
    private lazy var btnNavigate: UIButton = {
        let button = UIButton()
        button.backgroundColor = UIColor(named:"tooltipsAmenityNaviBtnColor")!
        button.setTitle("Drive Here", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = UIFont.amenityToolTipsTextFont()
        button.addTarget(self, action: #selector(onNavigationHereClicked), for: .touchUpInside)
        containerView.addSubview(button)
        return button
    }()
    
    
    //  MARK: Carpark availability
    private lazy var parkingStatusContentView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(hexString: "F5EEFC", alpha: 1)
        view.layer.cornerRadius = 8
        parkingAvailability.addSubview(view)
        return view
    }()
    
    private lazy var parkingIcon: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "carpark-availability-parking-icon1"))
        parkingStatusContentView.addSubview(imageView)
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var parkingAvailableStatusLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.parkingAvailableTextFont()
        label.textColor = UIColor(hexString: "222638", alpha: 1)
        label.textAlignment = .left
        parkingStatusContentView.addSubview(label)
        label.text = ""
        return label
    }()
    
    private lazy var parkingTriangleIcon: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "carpark-availability-triangle"))
        parkingAvailability.addSubview(imageView)
        return imageView
    }()
    
    private lazy var parkingUpdatedTimeLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.parkingAvailableUpdateTimeFont()
        label.textColor = .white
        label.textAlignment = .center
        parkingAvailability.addSubview(label)
        label.text = ""
        return label
    }()
    

    @objc private func onWalkHereClicked() {
        if let callback = onWalkHere {
            callback(poi)
        }
    }
    
    @objc private func onNavigationHereClicked() {
        if let callback = onNavigateHere {
            callback(poi)
        }
    }
    
    @objc private func onMoreInfoClicked() {
        if let callback = onMoreInfo {
            callback(poi)
        }
    }
    
//    var coordinate: CLLocationCoordinate2D {
//        return CLLocationCoordinate2D(latitude: carpark.lat ?? 0, longitude: carpark.long ?? 0)
//    }
    
    init(poi: POI, walkingEnabled: Bool, navigationEnabled: Bool = true, availablePercentImageBand: String? = nil, availabilityCSData: AvailabilityCSData? = nil) {
        
        if availabilityCSData != nil {
            hasCrowdsourced = true
        }
        
        self.poi = poi
        self.walkingEnabled = walkingEnabled
        self.navigationEnabled = navigationEnabled
        self.textAdditionalInfo = poi.text
        self.styleLightColor = poi.lightColor
        self.styleDarkColor = poi.darkColor
        
        super.init(frame: .zero)
        
        self.availablePercentImageBand = availablePercentImageBand
        self.availabilityCSData = availabilityCSData
        
        if let textAdditionalInfoLbl = textAdditionalInfo, !textAdditionalInfoLbl.isEmpty {
            isAdditionalInfoPoi = true
            lblAdditionalInfoPoi.isHidden = false
            lblAdditionalInfoPoi.text = textAdditionalInfoLbl
            self.setupStyleColor(styleDarkColorText: styleDarkColor, styleLightColorText: styleLightColor)
        } else {
            lblAdditionalInfoPoi.isHidden = true
        }
        
        tipHeight = 10
        self.backgroundColor = .clear
        bgColor = UIColor(named: "tooltipsAmenityBackgroundColor")!
        self.containerView.backgroundColor = bgColor
        lblName.text = poi.name
        lblAddress.text = poi.address
        
        btnWalk.isHidden = !walkingEnabled
        btnNavigate.isHidden = !navigationEnabled
        
        coordinate = CLLocationCoordinate2D(latitude: poi.lat, longitude: poi.long)
        
        self.showShadow = true
        updateParkingAvailableStyle(availabilityCSData: availabilityCSData)
//        setupLayout()
    }
    
    
    private func setupStyleColor(styleDarkColorText: String, styleLightColorText: String) {
        if appDelegate().isDarkMode() {
            if !styleDarkColorText.isEmpty  {
                lblAdditionalInfoPoi.textColor = UIColor(hexString: styleDarkColorText, alpha: 1)
            } else {
                lblAdditionalInfoPoi.textColor = UIColor(named: "trafficHeavyColor")!
            }
        } else {
            if !styleLightColorText.isEmpty {
                lblAdditionalInfoPoi.textColor = UIColor(hexString: styleLightColorText, alpha: 1)
            } else {
                lblAdditionalInfoPoi.textColor = UIColor(named: "trafficHeavyColor")!
            }
        }
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    internal override func setupLayout() {
                        
//        btnMoreInfo.snp.makeConstraints { make in
//            make.top.equalToSuperview().offset(yPadding)
//            make.trailing.equalToSuperview().offset(-xPadding)
//        }
        
        lblName.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().offset(xPadding)
            make.top.equalToSuperview().offset(yPadding)
            make.trailing.equalToSuperview().offset(-xPadding)
        }
        
        if isAdditionalInfoPoi == true {
            lblAdditionalInfoPoi.snp.makeConstraints { make in
                make.top.equalTo(lblName.snp.bottom).offset(yPadding)
                make.leading.equalToSuperview().offset(xPadding)
                make.trailing.equalToSuperview().offset(-xPadding)
            }
            
            lblAddress.snp.makeConstraints { make in
                make.top.equalTo(lblAdditionalInfoPoi.snp.bottom).offset(yPadding)
                make.leading.equalToSuperview().offset(xPadding)
                make.trailing.equalToSuperview().offset(-xPadding)
            }
        } else {
            lblAddress.snp.makeConstraints { make in
                make.top.equalTo(lblName.snp.bottom).offset(yPadding)
                make.leading.equalToSuperview().offset(xPadding)
                make.trailing.equalToSuperview().offset(-xPadding)
            }
        }
                    
        btnWalk.snp.makeConstraints { make in
            make.width.equalTo(buttonWidth)
            make.height.equalTo(buttonHeight)
            make.centerX.equalToSuperview()
            make.top.equalTo(lblAddress.snp.bottom).offset(gap)
        }
        
        btnNavigate.snp.makeConstraints { [weak self] make in
            guard let self = self else { return }
            make.width.equalTo(buttonWidth)
            if navigationEnabled {
                make.height.equalTo(buttonHeight)
            }else {
                make.height.equalTo(1)
            }
            
            make.centerX.equalToSuperview()
            if self.walkingEnabled {
                make.top.equalTo(btnWalk.snp.bottom).offset(yPadding)
            } else {
                if navigationEnabled {
                    make.top.equalTo(lblAddress.snp.bottom).offset(gap)
                }else {
                    make.top.equalTo(lblAddress.snp.bottom).offset(0)
                }
            }
        }
        
//        containerView.snp.makeConstraints { (make) in
//            make.top.equalToSuperview()
//            make.bottom.equalToSuperview().offset(-tipHeight)
//            make.leading.equalToSuperview()
//            make.width.equalTo(popupWidth)
//            make.trailing.equalToSuperview()
//        }
        
        
        //  MARK: - Parking available
        parkingStatusContentView.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(8)
            make.leading.equalToSuperview().offset(8)
            make.height.equalTo(36)
            make.trailing.equalToSuperview().offset(-8)
        }
        
        parkingIcon.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(10)
            make.leading.equalToSuperview().offset(8)
            make.height.equalTo(17)
            make.width.equalTo(17)
        }
        
        parkingAvailableStatusLabel.snp.makeConstraints { make in
            make.centerY.equalTo(parkingIcon.snp.centerY)
            make.leading.equalToSuperview().offset(29)
            make.trailing.equalToSuperview().offset(10)
        }
        
        parkingTriangleIcon.snp.makeConstraints { make in
            make.top.equalTo(parkingStatusContentView.snp.bottom).offset(-3)
            make.leading.equalTo(parkingStatusContentView).offset(12)
            make.height.equalTo(10)
            make.width.equalTo(14)
        }
        
        parkingUpdatedTimeLabel.snp.makeConstraints { make in
            make.top.equalTo(parkingStatusContentView.snp.bottom).offset(10)
            make.leading.equalToSuperview().offset(8)
            make.trailing.equalToSuperview().offset(-8)
        }
        
        containerView.snp.makeConstraints { (make) in
            if needShowParking() {
                make.top.equalToSuperview().offset(parkingInfoHeight)
                make.bottom.equalToSuperview()
            }else {
                make.top.equalToSuperview().offset(0)
                make.bottom.equalToSuperview().offset(-tipHeight)
            }
            
            make.leading.equalToSuperview()
            make.width.equalTo(popupWidth)
            make.trailing.equalToSuperview()
        }
        
        parkingAvailability.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.bottom.equalToSuperview().offset(-tipHeight)
            make.leading.equalToSuperview()
            make.width.equalTo(popupWidth)
            make.trailing.equalToSuperview()
        }
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        btnNavigate.layer.cornerRadius = buttonHeight / 2
        btnWalk.layer.cornerRadius = buttonHeight / 2
    }

    override func sizeThatFits(_ size: CGSize) -> CGSize {
                
//        let typeSize = CGSize(width: 0, height: 21) // always using the same size
        let nameSize = lblName.intrinsicContentSize
        let addressSize = lblAddress.intrinsicContentSize
        let lblAdditionalInfoSize = yPadding + lblAdditionalInfoPoi.text!.height(withConstrainedWidth: popupWidth - xPadding, font: lblAdditionalInfoPoi.font)
        
        let h = yPadding + nameSize.height + (isAdditionalInfoPoi
                                              ? lblAdditionalInfoSize : 0) + yPadding + addressSize.height + ((navigationEnabled || walkingEnabled) ? gap : 0) + (navigationEnabled ? buttonHeight : 0) + (walkingEnabled ? yPadding + buttonHeight : 0) + yPadding + tipHeight + (needShowParking() ? parkingInfoHeight: 0)
        return CGSize(width: popupWidth, height: h)
    }
    
    override var tooltipCoordinate: CLLocationCoordinate2D {
        return coordinate
    }
    
    override var tooltipId: String {
        return poi.id
    }
    
    
    private func updateParkingAvailableStyle(availabilityCSData: AvailabilityCSData?) {
        
        parkingAvailability.isHidden =  !hasCrowdsourced
        parkingStatusContentView.isHidden =  !hasCrowdsourced
        parkingTriangleIcon.isHidden = !hasCrowdsourced
        parkingUpdatedTimeLabel.isHidden =  !hasCrowdsourced
        if let dataCrowdsource = availabilityCSData {
            parkingUpdatedTimeLabel.text = dataCrowdsource.getUpdateTSDescDisplay()
            parkingAvailableStatusLabel.text = dataCrowdsource.title
            parkingAvailability.backgroundColor = UIColor(hexString: getCSThemeColor(), alpha: 1)
        }
        parkingIcon.image = UIImage(named: getCSIconName())
    }
}

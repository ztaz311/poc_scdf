//
//  CarparkToolTipsView2.swift
//  Breeze
//
//  Created by Zhou Hao on 22/04/22.
//

import UIKit
import CoreLocation

class CarparkToolTipsView2: ToolTipsView {

    // MARK: - Constants
    private var popupWidth: CGFloat = 191
    private let xPadding: CGFloat = 10
    private let yPadding: CGFloat = 8
    private let gap: CGFloat = 12 // gap between labels and button
    private let buttonWidth: CGFloat = 172.0
    private let buttonHeight: CGFloat = 54.0
    private let labelWidth: CGFloat = 65.0
    private let parkingInfoHeight: CGFloat = 71
    private var hasParkingAvailable: Bool = false

    // MARK: - Public properties
    var onCarparkSelected: ((_ carpark: Carpark) -> Void)?
    
    override var tooltipCoordinate: CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: carpark.lat ?? 0, longitude: carpark.long ?? 0)
    }
    
    override var tooltipId: String {
        return carpark.id ?? ""
    }

    // MARK: - Private properties
    private var carpark: Carpark!
    
    // such as Cheapest etc
    private lazy var lblType: UILabel  =  {
        let label = UILabel()
        label.font = UIFont.carparkTypeFont()
        label.preferredMaxLayoutWidth = popupWidth - 2 * xPadding
        label.textAlignment = .center
        label.numberOfLines = 1
        label.textColor = UIColor(named: "tooltipsCarparkTypeColor")
        label.backgroundColor = UIColor(named: "tooltipsCarparkTypeBGColor")
        label.layer.cornerRadius = 8
        label.layer.borderColor = UIColor(named: "tooltipsCarparkTypeColor")!.cgColor
        label.layer.masksToBounds = true
        label.layer.borderWidth = 1
        containerView.addSubview(label)
        return label
    }()

/*
    // fully seasonal parking
    private lazy var lblSeasonInfo: UILabel = {
        let label = UILabel()
        label.font = UIFont.carparkTypeFont()
        label.textColor = UIColor(named: "tooltipsCarparkSeasonColor")
        label.preferredMaxLayoutWidth = popupWidth - 2 * xPadding
        label.textAlignment = .left
        label.numberOfLines = 0
        containerView.addSubview(label)
        return label
    }()

    // customer parking
    private lazy var lblCustomerInfo: UILabel = {
        let label = UILabel()
        label.font = UIFont.carparkTypeFont()
        label.textColor = UIColor(named: "tooltipsCarparkSeasonColor")
        label.preferredMaxLayoutWidth = popupWidth - 2 * xPadding
        label.textAlignment = .left
        label.numberOfLines = 0
        containerView.addSubview(label)
        return label
    }()
*/
    private lazy var lblName: UILabel  =  {
        let label = UILabel()
        label.font = UIFont.amenityToolTipsERPTextFont()
        label.preferredMaxLayoutWidth = popupWidth - 2 * xPadding
        label.textAlignment = .left
        label.numberOfLines = 0
        label.textColor = UIColor(named: "tooltipsAmenityNameColor")
        containerView.addSubview(label)
        return label
    }()
        
    private lazy var imgCost: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "parkingCostIcon"))
        containerView.addSubview(imageView)
        return imageView
    }()
    
    private lazy var lblCostLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.availableCarparkLotsLabelFont()
        label.textColor = UIColor(named: "tooltipsERPLableColor")
        label.textAlignment = .left
        label.text = "Current"
        containerView.addSubview(label)
        return label
    }()

    private lazy var lblCost: UILabel = {
        let label = UILabel()
        label.font = UIFont.availableCarparkLotsFont()
        label.textColor = UIColor(named:"tooltipsAmenityNameColor")
        label.textAlignment = .right
        containerView.addSubview(label)
        return label
    }()
        
    private lazy var btnNavigate: UIButton = {
        let button = UIButton()
        button.backgroundColor = UIColor(named:"tooltipsAmenityNaviBtnColor")!
        button.setTitle("Navigate Here", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = UIFont.carparkNavigateButtonFont()
        button.addTarget(self, action: #selector(onNavigationHere), for: .touchUpInside)
        containerView.addSubview(button)
        return button
    }()
    
    
    
    //  MARK: Carpark availability
    
    private lazy var parkingStatusContentView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(hexString: "F5EEFC", alpha: 1)
        view.layer.cornerRadius = 8
        parkingAvailability.addSubview(view)
        return view
    }()
    
    private lazy var parkingIcon: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "carpark-availability-parking-icon1"))
        parkingStatusContentView.addSubview(imageView)
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var parkingAvailableStatusLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.parkingAvailableTextFont()
        label.textColor = UIColor(hexString: "222638", alpha: 1)
        label.textAlignment = .left
        parkingStatusContentView.addSubview(label)
        label.text = ""
        return label
    }()
    
    private lazy var parkingTriangleIcon: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "carpark-availability-triangle"))
        parkingAvailability.addSubview(imageView)
        return imageView
    }()
    
    private lazy var parkingUpdatedTimeLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.parkingAvailableUpdateTimeFont()
        label.textColor = .white
        label.textAlignment = .center
        parkingAvailability.addSubview(label)
        label.text = ""
        return label
    }()
    
    @objc private func onNavigationHere() {
        if let callback = onCarparkSelected {
            
            callback(carpark)
        }
    }
    
    func getCarpark() -> Carpark? {
        return self.carpark
    }
    
    override func needShowParking() -> Bool {
        return hasParkingAvailable
    }
    
    
    init(carpark: Carpark) {
        super.init(frame: .zero)
                
        self.carpark = carpark
        
        self.backgroundColor = .clear
        self.bgColor = UIColor(named: "tooltipsAmenityBackgroundColor")!

        if carpark.isCheapest ?? false {
            lblType.text = "Cheapest"
        } else {
            lblType.isHidden = true
        }
        lblName.text = carpark.name
        
        if let rate = carpark.currentHrRate?.oneHrRate {
            setupPriceLabel(textLabel: lblCost, rate: rate)
        } else {
            lblCost.text = "No Info"
        }
                        
        self.isSaveIconSmall = false
//        setupLayout()
        
        self.hasParkingAvailable = ((self.carpark.hasAvailabilityCS ?? false) && self.carpark.currentParkingStatus != .notAvailable)
        updateParkingAvailableStyle()
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupPriceLabel(textLabel: UILabel, rate: Double) {
        if Int(rate) == 0 {
            textLabel.text = "Free"
            textLabel.textColor = UIColor(named: "tooltipsCarparkFreeColor")
        } else if Int(rate) == -2 {
            textLabel.text = "Season"
            textLabel.textColor = UIColor(named: "tooltipsCarparkSeasonColor")
        } else if Int(rate) == -1 {
            textLabel.text = "No Info"
        } else {
            textLabel.text = String(format: "$%.2f", rate)
        }
    }
    
    internal override func setupLayout() {
                
        if carpark.isCheapest ?? false {
            lblType.snp.makeConstraints { (make) in
                make.top.equalToSuperview().offset(yPadding)
                make.leading.equalToSuperview().offset(xPadding)
                make.width.equalTo(72)
                make.height.equalTo(17)
            }
            lblName.snp.makeConstraints { (make) in
                make.leading.equalToSuperview().offset(xPadding)
                make.top.equalTo(lblType.snp.bottom).offset(yPadding)
                make.trailing.equalToSuperview().offset(-xPadding)
            }
        } else {
            lblName.snp.makeConstraints { (make) in
                make.leading.equalToSuperview().offset(xPadding)
                make.top.equalToSuperview().offset(yPadding)
                make.trailing.equalToSuperview().offset(-xPadding)
            }
        }
                        
        imgCost.snp.makeConstraints { make in
            make.width.equalTo(16)
            make.height.equalTo(16)
            make.leading.equalToSuperview().offset(xPadding)
            make.top.equalTo(lblName.snp.bottom).offset(yPadding)
        }
        
        lblCostLabel.snp.makeConstraints { make in
            make.leading.equalTo(imgCost.snp.trailing).offset(7)
            make.trailing.equalTo(lblCost.snp.leading).offset(7)
            make.centerY.equalTo(imgCost.snp.centerY)
        }

        lblCost.snp.makeConstraints { make in
            make.centerY.equalTo(imgCost.snp.centerY)
            make.trailing.equalToSuperview().offset(-xPadding)
            make.width.equalTo(labelWidth)
        }

        btnNavigate.snp.makeConstraints { make in
            make.width.equalTo(buttonWidth)
            make.height.equalTo(buttonHeight)
            make.centerX.equalToSuperview()
            make.top.equalTo(lblCostLabel.snp.bottom).offset(gap)
        }
        
//        containerView.snp.makeConstraints { (make) in
//            make.top.equalToSuperview()
//            make.bottom.equalToSuperview().offset(-tipHeight)
//            make.leading.equalToSuperview()
//            make.width.equalTo(popupWidth)
//            make.trailing.equalToSuperview()
//        }
        
        //  MARK: - Parking available
        parkingStatusContentView.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(8)
            make.leading.equalToSuperview().offset(8)
            make.height.equalTo(36)
            make.trailing.equalToSuperview().offset(-8)
        }
        
        parkingIcon.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(10)
            make.leading.equalToSuperview().offset(8)
            make.height.equalTo(17)
            make.width.equalTo(17)
        }
        
        parkingAvailableStatusLabel.snp.makeConstraints { make in
            make.centerY.equalTo(parkingIcon.snp.centerY)
            make.leading.equalToSuperview().offset(29)
            make.trailing.equalToSuperview().offset(10)
        }
        
        parkingTriangleIcon.snp.makeConstraints { make in
            make.top.equalTo(parkingStatusContentView.snp.bottom).offset(-3)
            make.leading.equalTo(parkingStatusContentView).offset(12)
            make.height.equalTo(10)
            make.width.equalTo(14)
        }
        
        parkingUpdatedTimeLabel.snp.makeConstraints { make in
            make.top.equalTo(parkingStatusContentView.snp.bottom).offset(10)
            make.leading.equalToSuperview().offset(8)
            make.trailing.equalToSuperview().offset(-8)
        }
        
        containerView.snp.makeConstraints { (make) in
            if needShowParking() {
                make.top.equalToSuperview().offset(parkingInfoHeight)
                make.bottom.equalToSuperview()
            }else {
                make.top.equalToSuperview().offset(0)
                make.bottom.equalToSuperview().offset(-tipHeight)
            }
            
            make.leading.equalToSuperview()
            make.width.equalTo(popupWidth)
            make.trailing.equalToSuperview()
        }
        
        parkingAvailability.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.bottom.equalToSuperview().offset(-tipHeight)
            make.leading.equalToSuperview()
            make.width.equalTo(popupWidth)
            make.trailing.equalToSuperview()
        }

    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        btnNavigate.layer.cornerRadius = buttonHeight / 2
    }

    override func sizeThatFits(_ size: CGSize) -> CGSize {
                
        let typeHeigh = ((carpark.isCheapest ?? false) == true ? 20 + yPadding : 0)
        let nameSize = lblName.intrinsicContentSize
        
        let costSize = lblCostLabel.intrinsicContentSize
        
        let h = typeHeigh + yPadding + nameSize.height + yPadding + costSize.height  + gap + buttonHeight + yPadding + tipHeight + yPadding + 10 + (needShowParking() ? parkingInfoHeight: 0)
                
        return CGSize(width: popupWidth, height: h)
        
    }

    override func processBookmarkUpdate(bookmark: Bookmark) {
        super.processBookmarkUpdate(bookmark: bookmark)
        self.carpark.bookmarkId = bookmark.bookmarkId
        self.carpark.isBookmarked = true
    }
    
    override func removeBookmark() {
        super.removeBookmark()
        self.carpark.isBookmarked = false
        self.carpark.bookmarkId = nil
    }
    
    
    private func updateParkingAvailableStyle() {
        
        parkingAvailability.isHidden = !hasParkingAvailable
        parkingStatusContentView.isHidden = !hasParkingAvailable
        parkingTriangleIcon.isHidden = !hasParkingAvailable
        parkingUpdatedTimeLabel.isHidden = !hasParkingAvailable
        
        parkingUpdatedTimeLabel.text = carpark.getCSUpdatedTimeDesc()
        parkingAvailableStatusLabel.text = carpark.getCSTitle()
        
        let bgColorHexString: String = carpark.getThemeColor()
        var carparkIconName: String = "carpark-availability-parking-icon1"
        
        switch carpark.currentParkingStatus {
        case .availableLots:
            carparkIconName = "carpark-availability-parking-icon1"
        case .fewAvailableLots:
            carparkIconName = "carpark-availability-parking-icon2"
        case .fullParking:
            carparkIconName = "carpark-availability-parking-icon3"
        case .notAvailable:
            print("Do nothing")
        }
        parkingAvailability.backgroundColor = UIColor(hexString: bgColorHexString, alpha: 1)
        parkingIcon.image = UIImage(named: carparkIconName)
    }
}

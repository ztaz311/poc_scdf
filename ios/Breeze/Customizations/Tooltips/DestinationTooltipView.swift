//
//  DestinationTooltipView.swift
//  Breeze
//
//  Created by Zhou Hao on 3/8/22.
//

import Foundation
import SnapKit
import CoreLocation
import SwiftyBeaver

class DestinationTooltipView: ToolTipsView {
    
    private var popupWidth: CGFloat = 169
    private let yPadding: CGFloat = 12
    private let xPadding: CGFloat = 12
    private let gap: CGFloat = 14
    private var buttonHeight: CGFloat = 28
    private let parkingInfoHeight: CGFloat = 71
    private var hasCrowdsourced: Bool = false
    
    // MARK: - Public properties
    override var tooltipCoordinate: CLLocationCoordinate2D {
        return location
    }
    
    /// use name as the id
    override var tooltipId: String {
        return name
    }
    
    override func needShowParking() -> Bool {
        return hasCrowdsourced
    }
    
  
    weak var viewModel: MapLandingViewModel?
        
    private var location: CLLocationCoordinate2D!
    private var name = ""
    private var fullAddress = ""
    
    var shareViaLocation: (() -> Void)?
    var inviteLocation: (() -> Void)?
    var onNavigationHere: (() -> Void)?
    
    private lazy var lblName: UILabel  =  {
        let label = UILabel()
        label.font = UIFont.amenityToolTipsTextFont()
        label.preferredMaxLayoutWidth = popupWidth - 2 * xPadding
        label.textAlignment = .left
        label.text = ""
        label.numberOfLines = 0
        label.textColor = UIColor(named: "tooltipsAmenityNameColor")!
        containerView.addSubview(label)
        return label
    }()

    private lazy var lblAddress: UILabel  =  {
        let label = UILabel()
        label.font = UIFont.amenityToolTipsAddressFont()
        label.preferredMaxLayoutWidth = popupWidth - 2 * xPadding
        label.textAlignment = .left
        label.text = ""
//        label.backgroundColor = .red
        label.numberOfLines = 0
        label.textColor = UIColor(named: "tooltipsAmenityAddressColor")!
        containerView.addSubview(label)
        return label
    }()
    
    
    
    private lazy var btnShare: UIButton = {
        let button = UIButton()
        button.backgroundColor = .clear
        button.setTitle("Share", for: .normal)
        button.setTitleColor(UIColor(named:"tooltipsAmenityNaviBtnColor")!, for: .normal)
        button.layer.cornerRadius = buttonHeight / 2
        button.titleLabel?.font = UIFont.amenityToolTipsTextFont()
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor(named:"tooltipsAmenityNaviBtnColor")?.cgColor ?? UIColor.purple.cgColor
        button.addTarget(self, action: #selector(onShareViaLocation), for: .touchUpInside)
        containerView.addSubview(button)
        return button
    }()
    
    
    private lazy var btnInvite: UIButton = {
        let button = UIButton()
        button.backgroundColor = .clear
        button.setTitle("Invite", for: .normal)
        button.setTitleColor(UIColor(named:"tooltipsAmenityNaviBtnColor")!, for: .normal)
        button.layer.cornerRadius = buttonHeight / 2
        button.titleLabel?.font = UIFont.amenityToolTipsTextFont()
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor(named:"tooltipsAmenityNaviBtnColor")?.cgColor ?? UIColor.purple.cgColor
        button.addTarget(self, action: #selector(onInviteLocation), for: .touchUpInside)
        containerView.addSubview(button)
        return button
    }()
    
    private lazy var btnNavigate: UIButton = {
        let button = UIButton()
        button.backgroundColor = UIColor(named:"tooltipsAmenityNaviBtnColor")!
        button.setTitle("Navigate Here", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.layer.cornerRadius = buttonHeight / 2
        button.titleLabel?.font = UIFont.amenityToolTipsTextFont()
        button.addTarget(self, action: #selector(onNavigateClicked), for: .touchUpInside)
        containerView.addSubview(button)
        
        return button
    }()
    
    
    
    //  MARK: Carpark availability
    
    private lazy var parkingStatusContentView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(hexString: "F5EEFC", alpha: 1)
        view.layer.cornerRadius = 8
        parkingAvailability.addSubview(view)
        return view
    }()
    
    private lazy var parkingIcon: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "carpark-availability-parking-icon1"))
        parkingStatusContentView.addSubview(imageView)
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var parkingAvailableStatusLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.parkingAvailableTextFont()
        label.textColor = UIColor(hexString: "222638", alpha: 1)
        label.textAlignment = .left
        parkingStatusContentView.addSubview(label)
        label.text = ""
        return label
    }()
    
    private lazy var parkingTriangleIcon: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "carpark-availability-triangle"))
        parkingAvailability.addSubview(imageView)
        return imageView
    }()
    
    private lazy var parkingUpdatedTimeLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.parkingAvailableUpdateTimeFont()
        label.textColor = .white
        label.textAlignment = .center
        parkingAvailability.addSubview(label)
        label.text = ""
        return label
    }()
    

    init(name: String, address: String, address2: String, fullAddress: String, location: CLLocationCoordinate2D, availablePercentImageBand: String, availabilityCSData: AvailabilityCSData? = nil) {
        
        if availabilityCSData != nil {
            hasCrowdsourced = true
        }
        
        super.init(frame: .zero)
        
        self.name = name
        self.location = location
        self.fullAddress = fullAddress
        self.address2 = address2
        self.address = name
        self.backgroundColor = .clear
        self.bgColor = UIColor(named: "tooltipsAmenityBackgroundColor")!
        self.tipHeight = 15.0
        self.tipWidth = 20.0
        self.cornerRadius = 16.0
        self.availablePercentImageBand = availablePercentImageBand
        self.availabilityCSData = availabilityCSData
        lblName.text = name
        
        // Check full Address
        if !fullAddress.isEmpty {
            lblAddress.text = fullAddress
        } else {
            if address == name {
                lblAddress.text = ""
            } else {
                lblAddress.text = address
            }
        }
        
        self.showShadow = true
        updateParkingAvailableStyle(availabilityCSData: availabilityCSData)
    }

    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
        
    override func sizeThatFits(_ size: CGSize) -> CGSize {
                
//        let lblNameSize = lblName.intrinsicContentSize
//        let lblAddrSize = lblAddress.intrinsicContentSize
        
        let leftPadding = onSaveAction == nil ? xPadding : xPadding + 24
        let nameHeight = (lblName.text ?? "").height(withConstrainedWidth: popupWidth - xPadding - leftPadding, font: lblName.font)
        let lblAddrSize = (lblAddress.text ?? "").height(withConstrainedWidth: popupWidth - 2 * xPadding, font: lblAddress.font)
        let heightShareButton = buttonHeight + gap
        let heightInviteButton = buttonHeight + gap
        let heightNavigationButton = buttonHeight + gap
        
        let h = yPadding + nameHeight + yPadding + lblAddrSize + yPadding + heightShareButton + heightInviteButton + heightNavigationButton + yPadding + tipHeight + 2 + (needShowParking() ? parkingInfoHeight: 0)
        
        return CGSize(width: popupWidth, height: h)
    }

    internal override func setupLayout() {
        
        lblName.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(yPadding)
            make.leading.equalToSuperview().offset(xPadding)
            let trailingPadding = onSaveAction == nil ? xPadding : xPadding + 24
            make.trailing.equalToSuperview().offset(-trailingPadding)
        }
                
        lblAddress.snp.makeConstraints { (make) in
            make.top.equalTo(lblName.snp.bottom).offset(yPadding)
            make.leading.equalToSuperview().offset(xPadding)
            make.trailing.equalToSuperview().offset(-xPadding)
        }
        
        
        btnShare.snp.makeConstraints { (make) in
            make.top.equalTo(lblAddress.snp.bottom).offset(yPadding)
            make.leading.equalToSuperview().offset(xPadding)
            make.trailing.equalToSuperview().offset(-xPadding)
        }
        
        btnInvite.snp.makeConstraints { (make) in
            make.top.equalTo(btnShare.snp.bottom).offset(yPadding)
            make.leading.equalToSuperview().offset(xPadding)
            make.trailing.equalToSuperview().offset(-xPadding)
        }
        
        btnNavigate.snp.makeConstraints { (make) in
            make.top.equalTo(btnInvite.snp.bottom).offset(yPadding)
            make.leading.equalToSuperview().offset(xPadding)
            make.trailing.equalToSuperview().offset(-xPadding)
            make.bottom.equalToSuperview().offset(-yPadding)
        }
        
//        containerView.snp.makeConstraints { (make) in
//            make.top.equalToSuperview()
//            make.bottom.equalToSuperview().offset(-tipHeight)
//            make.leading.equalToSuperview()
//            make.width.equalTo(popupWidth)
//            make.trailing.equalToSuperview()
//        }
        
        
        //  MARK: - Parking available
        parkingStatusContentView.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(8)
            make.leading.equalToSuperview().offset(8)
            make.height.equalTo(36)
            make.trailing.equalToSuperview().offset(-8)
        }
        
        parkingIcon.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(10)
            make.leading.equalToSuperview().offset(8)
            make.height.equalTo(17)
            make.width.equalTo(17)
        }
        
        parkingAvailableStatusLabel.snp.makeConstraints { make in
            make.centerY.equalTo(parkingIcon.snp.centerY)
            make.leading.equalToSuperview().offset(29)
            make.trailing.equalToSuperview().offset(10)
        }
        
        parkingTriangleIcon.snp.makeConstraints { make in
            make.top.equalTo(parkingStatusContentView.snp.bottom).offset(-3)
            make.leading.equalTo(parkingStatusContentView).offset(12)
            make.height.equalTo(10)
            make.width.equalTo(14)
        }
        
        parkingUpdatedTimeLabel.snp.makeConstraints { make in
            make.top.equalTo(parkingStatusContentView.snp.bottom).offset(10)
            make.leading.equalToSuperview().offset(8)
            make.trailing.equalToSuperview().offset(-8)
        }
        
        containerView.snp.makeConstraints { (make) in
            if needShowParking() {
                make.top.equalToSuperview().offset(parkingInfoHeight)
                make.bottom.equalToSuperview()
            }else {
                make.top.equalToSuperview().offset(0)
                make.bottom.equalToSuperview().offset(-tipHeight)
            }
            
            make.leading.equalToSuperview()
            make.width.equalTo(popupWidth)
            make.trailing.equalToSuperview()
        }
        
        parkingAvailability.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.bottom.equalToSuperview().offset(-tipHeight)
            make.leading.equalToSuperview()
            make.width.equalTo(popupWidth)
            make.trailing.equalToSuperview()
        }
        
    }
    
    override func processBookmarkUpdate(bookmark: Bookmark) {
        super.processBookmarkUpdate(bookmark: bookmark)
        
        viewModel?.isSearchAddressBookmarked = true
    }
    
    override func removeBookmark() {
        super.removeBookmark()
        viewModel?.isSearchAddressBookmarked = false
    }
    
    
    
    @objc private func onShareViaLocation() {
//        if let callback = shareViaLocation {
//            callback(address)
//        }
        
        shareViaLocation?()
//        dismiss(animated: true)
    }
    
    @objc func onInviteLocation() {
        inviteLocation?()
//        dismiss(animated: true)
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Home.UserClick.tooltip_destination_invite, screenName: ParameterName.Home.homepage_screen_view)
    }
    
    
    @objc func onNavigateClicked() {
        onNavigationHere?()

        dismiss(animated: true)
    }
    
    
    private func updateParkingAvailableStyle(availabilityCSData: AvailabilityCSData?) {
        
        parkingAvailability.isHidden =  !hasCrowdsourced
        parkingStatusContentView.isHidden =  !hasCrowdsourced
        parkingTriangleIcon.isHidden = !hasCrowdsourced
        parkingUpdatedTimeLabel.isHidden =  !hasCrowdsourced
        if let dataCrowdsource = availabilityCSData {
            parkingUpdatedTimeLabel.text = dataCrowdsource.getUpdateTSDescDisplay()
            parkingAvailableStatusLabel.text = dataCrowdsource.title
            parkingAvailability.backgroundColor = UIColor(hexString: getCSThemeColor(), alpha: 1)
        }
        parkingIcon.image = UIImage(named: getCSIconName())
    }
}

//extension String {
//    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
//        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
//        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil)
//
//        return ceil(boundingBox.height)
//    }
//
//    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
//        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
//        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil)
//
//        return ceil(boundingBox.width)
//    }
//}

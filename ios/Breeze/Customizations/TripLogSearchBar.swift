//
//  TripLogSearchBar.swift
//  Breeze
//
//  Created by Zhou Hao on 5/1/22.
//

import UIKit

class TripLogSearchBar: UISearchBar {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setLeftImage(Images.searchIcon!, with: 1, tintColor: UIColor.brandPurpleColor)
        self.clearBackgroundColor()
        self.textField?.backgroundColor =  UIColor(named: "tripLogSegmentBGColor")!
        self.textField?.textColor = UIColor.searchBarTextColor
        self.textField?.layer.borderColor = UITraitCollection.current.userInterfaceStyle == .dark ? UIColor(red: 34, green: 38, blue: 56).cgColor : UIColor(red: 201, green: 201, blue: 206).cgColor
        
        self.textField?.layer.borderWidth = 1
//        self.showBottomShadow()
        self.textField?.font = UIFont.tripLogSearchBarTextFont()
        self.textField?.placeholder = "Search"
    }
    
//    func updateColor(isDarkMode:Bool){
//        if (isDarkMode){
//            self.textField?.layer.borderColor = UIColor.clear.cgColor
//        }else{
//            self.textField?.layer.borderColor = UIColor.rgba(r: 201, g: 201, b: 206, a: 1.0).cgColor
//        }
//    }

    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        self.textField?.layer.borderColor = UITraitCollection.current.userInterfaceStyle == .dark ? UIColor(red: 34, green: 38, blue: 56).cgColor : UIColor(red: 201, green: 201, blue: 206).cgColor
    }
}


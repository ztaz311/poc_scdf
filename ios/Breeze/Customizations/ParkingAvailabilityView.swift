//
//  ParkingAvailabilityConfirmation.swift
//  Breeze
//
//  Created by Tuan, Pham Hai on 10/02/2023.
//

import UIKit
import SnapKit

enum ParkingAvailabilityStatus: String {
    case availableLots
    case fewAvailableLots
    case fullParking
    case notAvailable
}

class ParkingAvailabilityView: UIView {
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var parkingStatusBackgroundView: UIView!
    @IBOutlet weak var confirmView: UIView!
    @IBOutlet weak var exclaimView: UIView!
    
    @IBOutlet weak var betaBackgroundImage: UIImageView!
    @IBOutlet weak var betaLabel: UILabel!
    @IBOutlet weak var parkingIcon: UIImageView!
    @IBOutlet weak var parkingConfirmIcon: UIImageView!
    @IBOutlet weak var parkingExclaimationIcon: UIImageView!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var updatedTimeLabel: UILabel!
    @IBOutlet weak var confirmLabel: UILabel!
    @IBOutlet weak var exclaimLabel: UILabel!
    
    var currentStatus: ParkingAvailabilityStatus = .notAvailable
    private var onCompletion: (() -> Void)?
    private var onDismiss: ((ParkingAvailabilityStatus?) -> Void)?
    
    private var onYes: ((ParkingAvailabilityStatus?) -> Void)?
    private var onNo: (() -> Void)?
    
    private var preferredWidth: CGFloat = 320
    private var leading: CGFloat = 0
    private var trailing: CGFloat = 0
    private var yOffset: CGFloat = -120
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupView()
    }
    
    init(leading: CGFloat, trailing: CGFloat) {
        self.leading = leading
        self.trailing = trailing
        preferredWidth = UIScreen.main.bounds.width - leading - trailing
        super.init(frame: CGRectMake(0, 0, preferredWidth, 194))
        setupView()
    }
    
//    private func setupLayout() {
//        contentView.snp.makeConstraints { (make) in
//            make.top.equalToSuperview()
//            make.bottom.equalToSuperview()
//            make.leading.equalToSuperview()
//            make.width.equalTo(preferredWidth)
//            make.trailing.equalToSuperview()
//        }
//    }
    
    private func setupView() {
        
        Bundle.main.loadNibNamed("ParkingAvailabilityView", owner:self, options:nil)
        addSubview(contentView)
        contentView.frame = bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        self.layer.masksToBounds = false
        
        contentView.layer.cornerRadius = 16
        contentView.layer.masksToBounds = false
        contentView.layer.shadowRadius = 6
        contentView.layer.shadowOpacity = 0.4
        contentView.layer.shadowColor = UIColor(named:"tooltipsShadowColor")!.cgColor
        contentView.layer.shadowOffset = CGSize(width:0 , height:-3)
        
        backgroundView.layer.cornerRadius = 16
        backgroundView.layer.masksToBounds = true
        
        parkingStatusBackgroundView.layer.cornerRadius = 8
        parkingStatusBackgroundView.layer.masksToBounds = true
        
        confirmView.layer.cornerRadius = 20
        confirmView.layer.masksToBounds = true
        
        exclaimView.layer.cornerRadius = 20
        exclaimView.layer.masksToBounds = true
    }
    
    @IBAction func onYesButton(_ sender: Any) {
        dismiss(self.currentStatus)
    }
    
    @IBAction func onNoButton(_ sender: Any) {
        dismiss()
    }
    
    
    func setStatus(_ status: ParkingAvailabilityStatus, title: String, updateTimeText: String, themeColor: String) {
                
        self.currentStatus = status
        statusLabel.text = title
        updatedTimeLabel.text = updateTimeText
        
        let bgColorHexString: String = themeColor
        var betaTagImageName: String = "carpark-availability-beta-tag1"
        var carparkIconName: String = "carpark-availability-parking-icon1"
        
        switch self.currentStatus {
        case .availableLots:
            betaTagImageName = "carpark-availability-beta-tag1"
            carparkIconName = "carpark-availability-parking-icon1"
        case .fewAvailableLots:
            betaTagImageName = "carpark-availability-beta-tag2"
            carparkIconName = "carpark-availability-parking-icon2"
        case .fullParking:
            betaTagImageName = "carpark-availability-beta-tag3"
            carparkIconName = "carpark-availability-parking-icon3"
        case .notAvailable:
            print("Do nothing")
        }
        
        backgroundView.backgroundColor = UIColor(hexString: bgColorHexString, alpha: 1)
        betaBackgroundImage.image = UIImage(named: betaTagImageName)
        parkingIcon.image = UIImage(named: carparkIconName)
        
        parkingConfirmIcon.image = UIImage(named: "carpark-availability-confirm")
        parkingExclaimationIcon.image = UIImage(named: "carpark-availability-exclamation")
    }
    
    func show(in view: UIView, type: String, yOffset: CGFloat = 40, onCompletion: (()->Void)? = nil, onDismiss: ((ParkingAvailabilityStatus?) -> Void)? = nil) {

        self.onCompletion = onCompletion
        self.onDismiss = onDismiss
        
        view.addSubview(self)
        view.bringSubviewToFront(self)
        self.sizeToFit()

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) { [weak self] in
            guard let self = self else { return }
            
            let originalFrame = self.frame
            self.frame = CGRect(x: self.leading, y: -originalFrame.height, width: originalFrame.width, height: originalFrame.height)

            UIView.animate(withDuration: Values.standardAnimationDuration,
                            delay: 0.0,
                           options: [UIView.AnimationOptions.curveEaseInOut],
                            animations: {
                                self.frame.origin.y += (yOffset + originalFrame.height)
                                self.onCompletion?()
                            },
                            completion: nil)
        }
    }

    func dismiss(_ type: ParkingAvailabilityStatus? = nil) {
        if (superview != nil) {
            
            UIView.animate(withDuration: Values.standardAnimationDuration,
                            delay: 0.0,
                           options: [UIView.AnimationOptions.curveEaseInOut],
                            animations: {
                self.frame.origin.y = 0
                            },
                            completion: { [weak self] _ in
                guard let self = self else { return }
                self.removeFromSuperview()
            })

        }
        onDismiss?(type)
        onDismiss = nil
        onCompletion = nil
    }
}

//
//  CountedTextView.swift
//  Breeze
//
//  Created by Zhou Hao on 12/01/21.
//  Copyright © 2021 Zhou Hao. All rights reserved.
//


import UIKit

@IBDesignable
class CountedTextView: XibView {
    
    @IBOutlet weak var countedText: UILabel!
    @IBOutlet weak private var textField: UITextField!
    @IBOutlet weak var countedTextWidthConstraint: NSLayoutConstraint!
    
    var onTextChanged:((String)->())?
    var onActionCalled:(()->())?
    
    @IBInspectable
    var capitalizationType: UITextAutocapitalizationType = .none {
        didSet {
            textField.autocapitalizationType = capitalizationType
        }
    }
    
    @IBInspectable
    var maxCharactersAllowed: Int = 30 {
        didSet {
            textFieldDidChange(textField)
        }
    }
    
    @IBInspectable
    var borderColor: UIColor = UIColor.clear {
        didSet {
            setupBorder()
        }
    }

    @IBInspectable
    var cursorColor: UIColor = UIColor.clear {
        didSet {
            textField.tintColor = cursorColor
        }
    }

    @IBInspectable
    var borderWidth: CGFloat = 0 {
        didSet {
            setupBorder()
        }
    }
    
    @IBInspectable
    var radius: CGFloat = 0 {
        didSet {
            setupRoundedCorner()
        }
    }
    
    @IBInspectable
    var placeholder: String = "" {
        didSet {
            textField.placeholder = placeholder
        }
    }

    @IBInspectable
    var showCount: Bool = true {
        didSet {
            countedTextWidthConstraint.constant = showCount ? 53: 0
            layoutIfNeeded()
        }
    }
    
    @IBInspectable
    var returnKeyType: Int = 0 {
        didSet {
            textField.returnKeyType = UIReturnKeyType(rawValue: returnKeyType) ?? .default
        }
    }
    
    var text: String {
        get {
            return textField.text ?? ""
        }
        set {
            textField.text = newValue
            updateCount()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    private func setup() {
        textField.delegate = self
        textField.enablesReturnKeyAutomatically  = true
        setupBorder()
        setupRoundedCorner()
        textField.returnKeyType = .done
        
        textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
    }
    
    private func setupBorder() {
        self.layer.borderColor = borderColor.cgColor
        self.layer.borderWidth = borderWidth
    }
    
    private func setupRoundedCorner() {
        self.layer.cornerRadius = radius
    }
        
    override func becomeFirstResponder() -> Bool {
        return textField.becomeFirstResponder()
    }
        
    @objc func textFieldDidChange(_ textField: UITextField) {
        if (textField.text?.trimmingCharacters(in: .whitespaces) == ""){
            textField.text = ""
        }
        if let call = onTextChanged {
            call(textField.text ?? "")
        }
                
        updateCount()
    }
    
    private func updateCount() {
        guard let text = textField.text else {
            return
        }
        countedText.text = "\(maxCharactersAllowed - text.count)/\(maxCharactersAllowed)"
    }
    
    
}

extension CountedTextView: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        guard let text = textField.text else {
            return false
        }
        
        if(text.count > maxCharactersAllowed){
            
            return true
        }
                
        // skip the non-ascii
//        for scalar in text.unicodeScalars {
//            if !scalar.isASCII {
//                return false
//            }
//        }
        
        return text.count + (string.count - range.length) <= maxCharactersAllowed
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if let callback = onActionCalled {
            callback()
        }
        return true
    }
}

//
//  ParkingView.swift
//  Breeze
//
//  Created by Tuyen, Le Xuan  on 06/07/2022.
//

import Foundation
import UIKit
import Accelerate

enum ParkingType: Int, CaseIterable {
    case all = 0
    case available
    case hide
}

enum ParkingViewType: Int, CaseIterable {
    case hidden
    case collapse
    case expanded
}

extension ParkingType {
    static func fromString(value: String) -> ParkingType? {
        if value == CarparkAvailabilityValues.all {
            return .all
        } else if value == CarparkAvailabilityValues.type2 {
            return .available
        } else if value == CarparkAvailabilityValues.hide {
            return .hide
        }
        return nil
    }
    
    var stringValue: String {
        switch self {
        case .all:
            return CarparkAvailabilityValues.all
        case .available:
            return CarparkAvailabilityValues.type2
        case .hide:
            return CarparkAvailabilityValues.hide
        }
    }
}

protocol ParkingViewDelegate: NSObjectProtocol {
    func didUpdate(_ view: ParkingView, type: ParkingType)
    func didChangeViewType(_ view: ParkingView, viewType: ParkingViewType)
}

class ParkingView: XibView {
    
    private struct ParkingViewConst {
        static let normalSpacing:           Double = 5
        static let expandFirstSpacing:      Double = 10
        static let colaplseFirstSpacing:    Double = 7
        static let containerStvSpacing:     Double = 15
    }
    
    @IBOutlet var stvAll: UIStackView!
    @IBOutlet var stvHide: UIStackView!
    @IBOutlet var stvAvailable: UIStackView!
    
    @IBOutlet var stvContainer: UIStackView!
    
    @IBOutlet weak var lbViewAll: UILabel!
    @IBOutlet weak var lbAvailableOnly: UILabel!
    @IBOutlet weak var lbHide: UILabel!
    
    @IBOutlet weak var ctrBottomContainerStv: NSLayoutConstraint!
    
    private(set) var parkingType: ParkingType = .all {
        didSet {
            sortView()
        }
    }
    
    var parkingViewType: ParkingViewType = .expanded {
        didSet {
            if parkingViewType != oldValue {
                sortView()
                delegate?.didChangeViewType(self, viewType: parkingViewType)
            }
        }
    }
    
    weak var delegate: ParkingViewDelegate?
    
    override func layoutSubviews() {
        super.layoutSubviews()
        roundCorners([.topLeft, .bottomLeft], radius: 27)
        stvContainer.spacing = ParkingViewConst.containerStvSpacing
    }
    
    @IBAction func actionButton(_ sender: UIButton) {
        switch parkingViewType {
        case .collapse:
            parkingViewType = .expanded
        case .expanded:
            guard let type = ParkingType(rawValue: sender.tag) else { return }
            if type != parkingType {
                setParkingMode(type: type)
            }
            parkingViewType = .collapse
        case .hidden:
            break
        }
    }
    
    func setParkingMode(type: ParkingType, updateDate: Bool = true) {
        parkingType = type
        if updateDate {
            delegate?.didUpdate(self, type: type)
        }
    }
    
    func collapseIfNeed(delay: Double = 0) {
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(delay)) { [weak self] in
            guard let self = self else { return }
            if self.parkingViewType == .expanded {
                self.parkingViewType = .collapse
            }
        }
    }
    
    /*
     Sort stackview follow selected type
     subview will show or hide depend by viewType
     */
    private func sortView() {
        var allType = ParkingType.allCases
        allType.remove(at: [parkingType.rawValue])
        allType.insert(parkingType, at: 0)
        
        let arraySubView = allType.compactMap { type -> UIView in
            switch type {
            case .all:
                return stvAll
            case .available:
                return stvAvailable
            case .hide:
                return stvHide
            }
        }
        
        for view in arraySubView {
            stvContainer.removeArrangedSubview(view)
            stvContainer.addArrangedSubview(view)
        }
        
        switch parkingViewType {
        case .expanded:
            self.isHidden = false
            for i in 0..<stvContainer.arrangedSubviews.count {
                if let stv = stvContainer.arrangedSubviews[i] as? UIStackView {
                    stv.isHidden = false
                    stv.arrangedSubviews.forEach({ subview in
                        subview.isHidden = false
                    })
                    stv.spacing = i == 0 ? ParkingViewConst.expandFirstSpacing : ParkingViewConst.normalSpacing
                    let label = self.getLabel(stackview: stv)
                    label?.font = i == 0 ? UIFont(name: fontFamilySFPro.Medium, size: 12) : UIFont(name: fontFamilySFPro.Regular, size: 12)
                    label?.textColor = i == 0 ? UIColor(named: "parking_all_color") : UIColor(named: "parking_available_color")
                }
            }
            backgroundColor = UIColor(hexString: "ffffff", alpha: 0.7)
            ctrBottomContainerStv.constant = 7
        case .collapse:
            self.isHidden = false
            for i in 0..<stvContainer.arrangedSubviews.count {
                if let stv = stvContainer.arrangedSubviews[i] as? UIStackView {
                    stv.isHidden = i != 0
                    stv.spacing = i == 0 ? ParkingViewConst.colaplseFirstSpacing : ParkingViewConst.normalSpacing
                }
            }
            backgroundColor = .clear
            ctrBottomContainerStv.constant = 0
        case .hidden:
            self.isHidden = true
        }
    }
    
    private func getLabel(stackview: UIStackView) -> UILabel? {
        return stackview.arrangedSubviews.first {
            $0 is UILabel
        } as? UILabel
    }
}




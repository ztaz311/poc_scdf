//
//  ParkingAndAmenitiesView.swift
//  Breeze
//
//  Created by Tan, Tran Ngoc on 13/07/2023.
//

import Foundation
import UIKit
import Accelerate

enum ParkingAndAmenitiesType: Int, CaseIterable {
    case all = 0
    case available
    case hide
}

enum ParkingAndAmenitiesViewType: Int, CaseIterable {
    case hidden
    case collapse
    case expanded
}

extension ParkingAndAmenitiesType {
    static func fromString(value: String) -> ParkingAndAmenitiesType? {
        if value == CarparkAvailabilityValues.all {
            return .all
        } else if value == CarparkAvailabilityValues.type2 {
            return .available
        } else if value == CarparkAvailabilityValues.hide {
            return .hide
        }
        return nil
    }
    
    var stringValue: String {
        switch self {
        case .all:
            return CarparkAvailabilityValues.all
        case .available:
            return CarparkAvailabilityValues.type2
        case .hide:
            return CarparkAvailabilityValues.hide
        }
    }
}

protocol ParkingAndAmenitiesViewDelegate: NSObjectProtocol {
    func didUpdate(_ view: ParkingAndAmenitiesView, type: ParkingAndAmenitiesType)
    func didChangeViewType(_ view: ParkingAndAmenitiesView, viewType: ParkingAndAmenitiesViewType)
    func addAmenityType(_ type: String)
    func removeAmenityType(_ type: String)
}

class ParkingAndAmenitiesView: XibView {
    
    private struct ParkingAndAmenitiesViewConst {
        static let normalSpacing:           Double = 5
        static let expandFirstSpacing:      Double = 10
        static let colaplseFirstSpacing:    Double = 7
        static let containerStvSpacing:     Double = 15
    }
    
    
    @IBOutlet var stvAll: UIStackView!
    @IBOutlet var stvHide: UIStackView!
    @IBOutlet var stvAvailable: UIStackView!
    
    @IBOutlet var stvContainer: UIStackView!
    
    @IBOutlet var stvAmenites: UIStackView!
    
    @IBOutlet weak var lbViewAll: UILabel!
    @IBOutlet weak var lbAvailableOnly: UILabel!
    @IBOutlet weak var lbHide: UILabel!
    
    
    @IBOutlet weak var btnEv: UIButton!
    @IBOutlet weak var btnPetrol: UIButton!
    
    private var isDropPinOn: Bool = false {
        didSet {
            if isDropPinOn {
                //  Show all amenity
                isSelectedAmenitiesEv = true
                isSelectedAmenitiesPetrol = true
                activeEVAmenities()
                activePetrolAmenities()
            } else {
                //  Turn off EV and Petrol
                isSelectedAmenitiesEv = false
                isSelectedAmenitiesPetrol = false
                delegate?.removeAmenityType(Values.EV_CHARGER)
                delegate?.removeAmenityType(Values.PETROL)
                notActiveEVAmenities()
                notActivePetrolAmenities()
            }
        }
    }
    
    private(set) var parkingType: ParkingAndAmenitiesType = .all {
        didSet {
            sortView()
        }
    }
    
    var parkingViewType: ParkingAndAmenitiesViewType = .expanded {
        didSet {
            if parkingViewType != oldValue {
                sortView()
                delegate?.didChangeViewType(self, viewType: parkingViewType)
            }
        }
    }
    
    var isSelectedAmenitiesEv = false
    var isSelectedAmenitiesPetrol = false
    
    weak var delegate: ParkingAndAmenitiesViewDelegate?
    
    override func layoutSubviews() {
        super.layoutSubviews()
        roundCorners([.topLeft, .topRight , .bottomLeft, .bottomRight], radius: 24)
        stvContainer.spacing = ParkingAndAmenitiesViewConst.containerStvSpacing
        layer.cornerRadius = 24
    }
    
    @IBAction func actionButton(_ sender: UIButton) {
        switch parkingViewType {
        case .collapse:
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Home.UserClick.parking, screenName: ParameterName.Home.screen_view)
            parkingViewType = .expanded
        case .expanded:
            guard let type = ParkingAndAmenitiesType(rawValue: sender.tag) else { return }
            if type != parkingType {
                setParkingMode(type: type)
            }
            parkingViewType = .collapse
        case .hidden:
            break
        }
    }
    
    @IBAction func actionButtonEV(_ sender: UIButton) {
        isSelectedAmenitiesEv.toggle()
        if isSelectedAmenitiesEv {
            self.activeEVAmenities()
            if !isDropPinOn {
                delegate?.removeAmenityType(Values.PETROL)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    self.notActivePetrolAmenities()
                }
                isSelectedAmenitiesPetrol = false
            }
            
//            let amenityID = ["amenity":Values.EV_CHARGER] as [String : Any]
//            NotificationCenter.default.post(name: Notification.Name(Values.NotificationSelectedPetrolEV), object: nil,userInfo: amenityID)
            
        } else {
            delegate?.removeAmenityType(Values.EV_CHARGER)
            self.notActiveEVAmenities()
        }
    }
    
    @IBAction func actionButtonPetrol(_ sender: UIButton) {
        isSelectedAmenitiesPetrol.toggle()
        if isSelectedAmenitiesPetrol {
            self.activePetrolAmenities()
            if !isDropPinOn {
                delegate?.removeAmenityType(Values.EV_CHARGER)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    self.notActiveEVAmenities()
                }
                isSelectedAmenitiesEv = false
            }
            
//            let amenityID = ["amenity":Values.PETROL] as [String : Any]
//            NotificationCenter.default.post(name: Notification.Name(Values.NotificationSelectedPetrolEV), object: nil,userInfo: amenityID)
            
        } else {
            delegate?.removeAmenityType(Values.PETROL)
            self.notActivePetrolAmenities()
        }
    }
    
    func setOnDropPin(_ isOn: Bool) {
        self.isDropPinOn = isOn
    }
    
    func updateAmenitiesWhenUserPanMap() {
        if isSelectedAmenitiesEv {
            self.activeEVAmenities()
        }
        
        if isSelectedAmenitiesPetrol {
            self.activePetrolAmenities()
        }
    }
    
    func setParkingMode(type: ParkingAndAmenitiesType, updateDate: Bool = true) {
        parkingType = type
        if updateDate {
            switch parkingType {
            case .all:
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Home.UserClick.carpark_all_nearby, screenName: ParameterName.Home.screen_view)
            case .available:
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Home.UserClick.carpark_available_only, screenName: ParameterName.Home.screen_view)
            default:
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.Home.UserClick.carpark_hide, screenName: ParameterName.Home.screen_view)
            }
            
            delegate?.didUpdate(self, type: type)
        }
    }
    
    func collapseIfNeed(delay: Double = 0) {
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(delay)) { [weak self] in
            guard let self = self else { return }
            if self.parkingViewType == .expanded {
                self.parkingViewType = .collapse
            }
        }
    }
    
    private func activeEVAmenities() {
        delegate?.addAmenityType(Values.EV_CHARGER)
        btnEv.setImage(UIImage(named: "ev_charger_icon_selected"), for: .normal)
        let amenityDetails = ["amenity":Values.EV_CHARGER,"isSelected":true] as [String : Any]
        NotificationCenter.default.post(name: Notification.Name(Values.NotificationAmenitiesPetrolAndEV), object: nil,userInfo: amenityDetails)
    }
    
    private func notActiveEVAmenities() {
        btnEv.setImage(UIImage(named: "ev_charger_icon"), for: .normal)
        if let mapLandingVC = appDelegate().getMapLandingVCInTop() {
            mapLandingVC.mapLandingViewModel?.mobileMapViewUpdatable?.removeAmenitiesFromMap(type: Values.EV_CHARGER)
            mapLandingVC.didDeselectAnnotation()
            mapLandingVC.didDeselectAmenityAnnotation()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                let amenityDetails = ["amenity":Values.EV_CHARGER,"isSelected":false] as [String : Any]
                NotificationCenter.default.post(name: Notification.Name(Values.NotificationAmenitiesPetrolAndEV), object: nil,userInfo: amenityDetails)
            }
        }
    }
    
    private func activePetrolAmenities() {
        delegate?.addAmenityType(Values.PETROL)
        btnPetrol.setImage(UIImage(named: "petrol_icon_selected"), for: .normal)
        let amenityDetails = ["amenity":Values.PETROL,"isSelected":true] as [String : Any]
        NotificationCenter.default.post(name: Notification.Name(Values.NotificationAmenitiesPetrolAndEV), object: nil,userInfo: amenityDetails)
    }
    
    private func notActivePetrolAmenities() {
        btnPetrol.setImage(UIImage(named: "petrol_icon"), for: .normal)
        if let mapLandingVC = appDelegate().getMapLandingVCInTop() {
            mapLandingVC.mapLandingViewModel?.mobileMapViewUpdatable?.removeAmenitiesFromMap(type: Values.PETROL)
            mapLandingVC.didDeselectAnnotation()
            mapLandingVC.didDeselectAmenityAnnotation()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                let amenityDetails = ["amenity":Values.PETROL,"isSelected":false] as [String : Any]
                NotificationCenter.default.post(name: Notification.Name(Values.NotificationAmenitiesPetrolAndEV), object: nil,userInfo: amenityDetails)
            }
        }
    }
    

    private func sortView() {
        var allType = ParkingAndAmenitiesType.allCases
        allType.remove(at: [parkingType.rawValue])
        allType.insert(parkingType, at: 0)
        
        let arraySubView = allType.compactMap { type -> UIView in
            switch type {
            case .all:
                return stvAll
            case .available:
                return stvAvailable
            case .hide:
                return stvHide
            }
        }
        
        for view in arraySubView {
            stvContainer.removeArrangedSubview(view)
            stvContainer.addArrangedSubview(view)
        }
        
        switch parkingViewType {
        case .expanded:
            self.isHidden = false
            UIView.animate(
                withDuration:0.3, delay: 0, options: .curveEaseIn,
                animations: {
                    for i in 0..<self.stvContainer.arrangedSubviews.count {
                        if let stv = self.stvContainer.arrangedSubviews[i] as? UIStackView {
                            stv.isHidden = false
                            stv.arrangedSubviews.forEach({ subview in
                                subview.isHidden = false
                            })
                            stv.spacing = i == 0 ? ParkingAndAmenitiesViewConst.expandFirstSpacing : ParkingAndAmenitiesViewConst.normalSpacing
                            let label = self.getLabel(stackview: stv)
                            label?.font = i == 0 ? UIFont(name: fontFamilySFPro.Medium, size: 12) : UIFont(name: fontFamilySFPro.Regular, size: 12)
                            label?.textColor = i == 0 ? UIColor(named: "parking_all_color") : UIColor(named: "parking_available_color")
                        }
                    }
                }, completion: { _ in
                    // When collapsing, wait for animation to finish before changing from "x" to "+"
                   
                })
            
            backgroundColor = UIColor(hexString: "ffffff", alpha: 0.7)
        case .collapse:
            self.isHidden = false
            for i in 0..<stvContainer.arrangedSubviews.count {
                if let stv = stvContainer.arrangedSubviews[i] as? UIStackView {
                    stv.isHidden = i != 0
                    stv.spacing = i == 0 ? ParkingAndAmenitiesViewConst.colaplseFirstSpacing : ParkingAndAmenitiesViewConst.normalSpacing
                }
            }
            backgroundColor = .clear
        case .hidden:
            self.isHidden = true
        }
    }
    
    private func getLabel(stackview: UIStackView) -> UILabel? {
        return stackview.arrangedSubviews.first {
            $0 is UILabel
        } as? UILabel
    }
}




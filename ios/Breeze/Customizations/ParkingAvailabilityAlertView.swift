//
//  ParkingAvailabilityAlertView.swift
//  Breeze
//
//  Created by Tuan, Pham Hai on 06/11/2023.
//

import Foundation
import UIKit

class ParkingAvailabilityAlertView: UIView {
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var parkingIcon: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var parkToButton: UIButton!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var arrowImage: UIImageView!
    
    private var onCompletion: (() -> Void)?
    private var onDismiss: ((ParkingAvailabilityStatus?, Carpark?) -> Void)?
    
    private var timer: Timer?
    
    var currentStatus: ParkingAvailabilityStatus = .notAvailable
    var carpark: Carpark?
    
    private var preferredWidth: CGFloat = 320
    private var leading: CGFloat = 0
    private var trailing: CGFloat = 0
    private var yOffset: CGFloat = -120
    private var startDate: Date = Date()
    private var dismissInSeconds: TimeInterval = 5
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupView()
    }
    
    init(leading: CGFloat, trailing: CGFloat) {
        self.leading = leading
        self.trailing = trailing
        preferredWidth = UIScreen.main.bounds.width - leading - trailing
        super.init(frame: CGRectMake(0, 0, preferredWidth, 174))
        setupView()
    }
    
    private func setupView() {
        
        Bundle.main.loadNibNamed("ParkingAvailabilityAlertView", owner:self, options:nil)
        addSubview(contentView)
        contentView.frame = bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        self.layer.masksToBounds = false
        
        contentView.layer.cornerRadius = 16
        contentView.layer.masksToBounds = false
        contentView.layer.shadowRadius = 6
        contentView.layer.shadowOpacity = 0.4
        contentView.layer.shadowColor = UIColor(named:"tooltipsShadowColor")!.cgColor
        contentView.layer.shadowOffset = CGSize(width:0 , height:-3)
        
        backgroundView.layer.cornerRadius = 16
        backgroundView.layer.masksToBounds = true
  
        parkToButton.layer.cornerRadius = 8
        parkToButton.layer.masksToBounds = true
        parkToButton.layer.borderWidth = 0.5
        parkToButton.layer.borderColor = UIColor.white.cgColor
        parkToButton.layer.shadowColor = UIColor.gray.cgColor
      
    }
    
    @IBAction func onNavigateClicked(_ sender: Any) {
        dismiss(self.currentStatus, carpark: carpark)
    }
    
    @IBAction func closeAction(_ sender: Any) {
        self.removeFromSuperview()
    }
    
    func setStatus(_ status: ParkingAvailabilityStatus, title: String, carpark: Carpark? = nil) {
                
        self.currentStatus = status
        self.carpark = carpark
        
        titleLabel.text = title
        
        var bgColorHexString: String = "#18C970"
        var carparkIconName: String = "ParkingGreenIcon"
        var message: String = "Available"
        var parkToButtonColor: String = "#18C970"
        var arrowIcon: String = "arrow-icon-orange"
        
        
        switch self.currentStatus {
        case .availableLots:
            bgColorHexString = "#18C970"
            carparkIconName = "ParkingGreenIcon"
            message = "Available"
        case .fewAvailableLots:
            bgColorHexString = "#F3712B"
            carparkIconName = "ParkingOrangeIcon"
            message = "Filling Up Fast"
            parkToButtonColor = "#FFFFFF"
        case .fullParking:
            bgColorHexString = "#EA347A"
            carparkIconName = "ParkingRedIcon"
            message = "Carpark Full"
            arrowIcon = "arrow-icon-pink"
        case .notAvailable:
            print("Do nothing")
        }
        descLabel.text = message
        topView.backgroundColor = UIColor(hexString: bgColorHexString, alpha: 1)
        bottomView.backgroundColor = UIColor(hexString: bgColorHexString, alpha: 1)
        parkToButton.backgroundColor = UIColor(hexString: parkToButtonColor, alpha: 1)
        parkToButton.setTitle("Park at \(self.carpark?.name ?? "")", for: .normal)
        parkToButton.setTitleColor(UIColor(hexString: bgColorHexString, alpha: 1), for: .normal)
        parkingIcon.image = UIImage(named: carparkIconName)
        arrowImage.image = UIImage(named: arrowIcon)
    }
    
    func show(in view: UIView, type: String, yOffset: CGFloat = 40, dismissInSeconds: Int = Int(Values.notificationDuration), onCompletion: (()->Void)? = nil, onDismiss: ((ParkingAvailabilityStatus?, Carpark?) -> Void)? = nil) {

        self.onCompletion = onCompletion
        self.onDismiss = onDismiss
        
        view.addSubview(self)
        view.bringSubviewToFront(self)
        
        if self.carpark != nil && (self.currentStatus == .fewAvailableLots || self.currentStatus == .fullParking) {
            self.bottomConstraint.constant = 69
        } else {
            self.bottomConstraint.constant = 0
        }
        self.setNeedsLayout()
        self.layoutIfNeeded()
        self.sizeToFit()
        
        startDate = Date()
        
        if dismissInSeconds > 0 {
            self.dismissInSeconds = Double(dismissInSeconds)
            timer = Timer.scheduledTimer(timeInterval: TimeInterval(dismissInSeconds), target: self, selector: #selector(onTimer), userInfo: nil, repeats: false)
        }

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) { [weak self] in
            guard let self = self else { return }
            
            let originalFrame = self.frame
            var height: CGFloat = 105
            if self.carpark != nil && (self.currentStatus == .fewAvailableLots || self.currentStatus == .fullParking) {
                height = 174
            }
            
            self.frame = CGRect(x: self.leading, y: -height, width: originalFrame.width, height: height)

            UIView.animate(withDuration: Values.standardAnimationDuration,
                            delay: 0.0,
                           options: [UIView.AnimationOptions.curveEaseInOut],
                            animations: {
                                self.frame.origin.y += (yOffset + height)
                                self.onCompletion?()
                            },
                            completion: nil)
        }
    }
    
    
    func dismiss(_ type: ParkingAvailabilityStatus? = nil, carpark: Carpark?) {
        if (superview != nil) {
            
            UIView.animate(withDuration: Values.standardAnimationDuration,
                            delay: 0.0,
                           options: [UIView.AnimationOptions.curveEaseInOut],
                            animations: {
                self.frame.origin.y = 0
                            },
                            completion: { [weak self] _ in
                guard let self = self else { return }
                self.removeFromSuperview()
            })

        }
        onDismiss?(type, carpark)
        onDismiss = nil
        onCompletion = nil
    }
    
    @objc private func onTimer() {
        dismiss(self.currentStatus, carpark: nil)
    }
    
    func getRemainingTime() -> Double {
        let duration = Date().timeIntervalSince(startDate)
        
        if duration >= dismissInSeconds {
            return 0
        } else {
            return dismissInSeconds - duration
        }
    }
    
    func isDismissed() -> Bool {
        return (onDismiss == nil)
    }
}

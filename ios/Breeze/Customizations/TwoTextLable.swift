//
//  TwoTextLable.swift
//  Breeze
//
//  Created by Zhou Hao on 19/3/21.
//

import UIKit

@IBDesignable
final class TwoTextLable: UILabel {

    @IBInspectable
    var fontSize1: CGFloat = 14 {
        didSet {
            set(text1, text2)
        }
    }
    
    @IBInspectable
    var fontSize2: CGFloat = 14 {
        didSet {
            set(text1, text2)
        }
    }
    
    @IBInspectable
    var fontColor1: UIColor? {
        didSet {
            set(text1, text2)
        }
    }
    
    @IBInspectable
    var fontColor2: UIColor? {
        didSet {
            set(text1, text2)
        }
    }
    
    @IBInspectable
    var font2: UIFont? {
        didSet {
            set(text1, text2)
        }
    }
    
    @IBInspectable
    var text1: String = "" {
        didSet {
            set(text1, text2)
        }
    }
    
    @IBInspectable
    var text2: String = "" {
        didSet {
            set(text1, text2)
        }
    }

    func set(_ text1: String, _ text2: String) {
                        
        let color1: UIColor = fontColor1 ?? textColor
        let color2: UIColor = fontColor2 ?? textColor
        
        let attrText = NSMutableAttributedString(string: text1, attributes:
                                                    [NSAttributedString.Key.foregroundColor : color1,
                                                     NSAttributedString.Key.font: font.withSize(fontSize1)])
        
        if let subFont = font2?.withSize(self.fontSize2) {
            attrText.append(NSMutableAttributedString(string: text2, attributes:
                                                        [NSAttributedString.Key.foregroundColor : color2,
                                                         NSAttributedString.Key.font: subFont]))
        } else {
            if let subFont = UIFont(name: fontFamilySFPro.Regular, size: self.fontSize2) {
                attrText.append(NSMutableAttributedString(string: text2, attributes:
                                                            [NSAttributedString.Key.foregroundColor : color2,
                                                             NSAttributedString.Key.font: subFont]))
            }
        }
        
        self.attributedText = attrText
    }
    
}




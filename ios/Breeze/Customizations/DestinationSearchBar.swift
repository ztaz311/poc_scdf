//
//  DestinationSearchBar.swift
//  Breeze
//
//  Created by Malou Mendoza on 14/5/21.
//

import UIKit

class DestinationSearchBar: UISearchBar {

    init() {
//        super.init(frame: CGRect(x: 0, y: 0, width: searchBarSize, height: searchBarSize))
        super.init(frame: .zero)
//        self.backgroundImage = UIImage(color: UIColor(named: "mapLandingBottomCellColor")!)
        self.setLeftImage(Images.searchIcon!, with: 1, tintColor: UIColor.brandPurpleColor)
        self.clearBackgroundColor()
        self.textField?.backgroundColor =  UIColor(named: "mapLandingBottomCellColor")!
        self.textField?.textColor = UIColor.searchBarTextColor
        self.textField?.layer.borderColor = UIColor.rgba(r: 201, g: 201, b: 206, a: 1.0).cgColor
       
        self.textField?.layer.borderWidth = 1
        self.showBottomShadow()
        self.textField?.font = UIFont.searchBarTextFont()
        self.textField?.placeholder = "Search"
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func updateColor(isDarkMode:Bool){
        if (isDarkMode){
            self.textField?.layer.borderColor = UIColor.clear.cgColor
        }else{
            self.textField?.layer.borderColor = UIColor.rgba(r: 201, g: 201, b: 206, a: 1.0).cgColor
        }
    }

}

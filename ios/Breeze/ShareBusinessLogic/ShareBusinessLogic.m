//
//  ShareBusinessLogic.m
//  Breeze
//
//  Created by VishnuKanth on 15/10/21.
//

#import <Foundation/Foundation.h>
#import <React/RCTBridgeModule.h>

@interface RCT_EXTERN_REMAP_MODULE(ShareBusinessLogic, ShareBusinessLogicModule, NSObject)

RCT_EXTERN_METHOD(finishedRequest:
                  (nonnull NSString *)requestId
                  data: (nullable NSDictionary) data)

RCT_EXTERN_METHOD(failedRequest:
                  (nonnull NSString *)requestId
                  errorMessage: (nullable NSString) errorMessage)
@end

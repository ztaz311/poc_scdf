//
//  ShareBusinessLogicModule.swift
//  Breeze
//
//  Created by VishnuKanth on 15/10/21.
//

import Foundation
import React
import RxSwift

@objc(ShareBusinessLogicModule)
class ShareBusinessLogicModule: NSObject {
    static let shared = ShareBusinessLogicModule()
    
    private override init() {
        // Don't need implement it
    }
    
    private var unSafependingRequests: [NSString: (SingleEvent<NSDictionary?>) -> Void] = [:]
    
    private var pendingRequests: [NSString: (SingleEvent<NSDictionary?>) -> Void] {
        var copyRequests: [NSString: (SingleEvent<NSDictionary?>) -> Void] = [:]
        
        concurentQueue.sync {
            copyRequests = self.unSafependingRequests
        }
        return copyRequests
    }
    
    private let concurentQueue: DispatchQueue = DispatchQueue(label: "ShareBusinessLogicModule.concurrent.queue", attributes: .concurrent)

    
    static let SHARE_BUSINESS_LOGIC_REQUEST = "SHARE_BUSINESS_LOGIC_REQUEST"
    
    // Function name to call from React Native
    static let FN_GET_RECENT_LIST = "FN_GET_RECENT_LIST"
    static let FN_GET_FAV_LIST = "FN_GET_FAV_LIST"
    static let FN_GET_SHORTCUT_LIST = "GET_SHORTCUT_LIST"
    static let FN_SEARCH_LOCATION = "FN_SEARCH_LOCATION"
    static let FN_GET_ETA_LIST = "FN_GET_ETA_LIST"
    static let FN_GET_GOOGLE_LAT_LNG_IF_NEED = "FN_GET_GOOGLE_LAT_LNG_IF_NEED"
    static let FN_SAVE_RECENT_LIST = "FN_SAVE_RECENT_LIST"
    static let FN_GET_ONBOARDING_STATE = "FN_GET_ONBOARDING_STATE"
    static let FN_SAVE_ONBOARDING_STATE = "FN_SAVE_ONBOARDING_STATE"
    static let FN_GET_LOCAL_USER_PREFERENCES = "FN_GET_LOCAL_USER_PREFERENCES"
    static let FN_SAVE_LOCAL_USER_PREFERENCES = "FN_SAVE_LOCAL_USER_PREFERENCES"
    static let FN_UPDATE_UPCOMING_TRIPS = "FN_UPDATE_UPCOMING_TRIPS"
    static let SELECT_TRAFFIC_CAMERA = "SELECT_TRAFFIC_CAMERA"
    static let OPEN_CALCULATE_FEE_CARPARK = "OPEN_CALCULATE_FEE_CARPARK"
    static let PARKING_BOTTOM_SHEET_REFRESH = "PARKING_BOTTOM_SHEET_REFRESH"
    static let SEND_ERP_DATA_TO_REACT = "SEND_ERP_DATA_TO_REACT"
    static let SEND_EVENT_SELECTED_ERP = "SEND_EVENT_SELECTED_ERP"
    static let SEND_THEME_CHANGE_EVENT = "SEND_THEME_CHANGE_EVENT"
    static let NAVIGATE_TO_HOME_TAB = "NAVIGATE_TO_HOME_TAB"
    static let NAVIGATE_TO_INBOX_SCREEN = "NAVIGATE_TO_INBOX_SCREEN"
    static let READ_INBOX_EVENT = "READ_INBOX_EVENT"
    static let OPEN_NOTIFICATION_FROM_NATIVE = "OPEN_NOTIFICATION_FROM_NATIVE"
    static let RECEIVE_NEW_NOTIFICATIONS = "RECEIVE_NEW_NOTIFICATIONS"
    static let OPEN_ONBOARDING_TUTORIAL = "OPEN_ONBOARDING_TUTORIAL"
    static let DISABLE_PROFILE_AMENITIES = "DISABLE_PROFILE_AMENITIES"
    
    static let OPEN_MY_VOUCHER_FROM_NOTIFICATION = "OPEN_MY_VOUCHER_FROM_NOTIFICATION"
    static let UPDATE_BOTTOM_SHEET_INDEX = "UPDATE_BOTTOM_SHEET_INDEX"
    static let UPDATE_AMENITIES_PREF = "UPDATE_AMENITIES_PREF"
    static let TRIGGER_INIT_USER_PREF = "TRIGGER_INIT_USER_PREF"
    static let UPDATE_SAVED_LOCATION = "UPDATE_SAVED_LOCATION"
    static let SELECT_SAVED_LOCATION = "SELECT_SAVED_LOCATION"
    static let SWITCH_BOTTOM_TAB = "SWITCH_BOTTOM_TAB"
    static let REMOVE_SHORTCUT_FROM_MAP = "REMOVE_SHORTCUT_FROM_MAP"
    static let ON_FINISHED_INITIALIZING_USER_DATA = "ON_FINISHED_INITIALIZING_USER_DATA"
    static let CAN_SHOW_VOUCHER_ANIMATION = "CAN_SHOW_VOUCHER_ANIMATION"
    static let ON_NEW_VOUCHER_ADDED = "ON_NEW_VOUCHER_ADDED"
    static let RESET_AMENITY_SELECTIONS = "RESET_AMENITY_SELECTIONS"
    static let GO_BACK_EXPLORE_MAP = "GO_BACK_EXPLORE_MAP"
    static let SHOW_OR_HIDE_BUTTON_CARPARK_LIST = "SHOW_OR_HIDE_BUTTON_CARPARK_LIST"
    static let DISMISS_SHARING_DIALOG = "DISMISS_SHARING_DIALOG"
    static let OPEN_SAVED_FROM_NATIVE = "OPEN_SAVED_FROM_NATIVE"
    static let UPDATE_PARKING_AVAILABLE_STATUS = "UPDATE_PARKING_AVAILABLE_STATUS"
    static let SHARE_LOCATION_FROM_NATIVE = "SHARE_LOCATION_FROM_NATIVE"
    static let LOCATION_INVITE_FROM_NATIVE = "LOCATION_INVITE_FROM_NATIVE"
    static let OPEN_COLLECTION_DETAIL_FROM_SHARED = "OPEN_COLLECTION_DETAIL_FROM_SHARED"
    
    //  OBU
    static let UPDATE_OBU_LITE_SPEED = "UPDATE_OBU_LITE_SPEED"
    static let UPDATE_OBU_EVENT_NOTI = "UPDATE_OBU_EVENT_NOTI"
    static let UPDATE_OBU_CASH_CARD_INFO = "UPDATE_OBU_CASH_CARD_INFO"
    static let OPEN_MODAL_OBU_INSTALL = "OPEN_MODAL_OBU_INSTALL"
    static let CHANGE_STATUS_PAIR = "CHANGE_STATUS_PAIR"
    static let OPEN_OBU_CARPARK_AVAILABILITY = "OPEN_OBU_CARPARK_AVAILABILITY"
    static let OPEN_OBU_TRAVEL_TIME = "OPEN_OBU_TRAVEL_TIME"
    static let UPDATE_PAIRED_VEHICLE_LIST = "UPDATE_PAIRED_VEHICLE_LIST"
    static let UPDATE_VALUE_AUTO_CONNECT_OBU = "UPDATE_VALUE_AUTO_CONNECT_OBU"
    static let BACK_TO_HOME_PAGE = "BACK_TO_HOME_PAGE"
    static let SET_NO_CARD = "SET_NO_CARD"
    static let DISABLE_TOGGLE_BUTTON_CARPARK_LIST = "DISABLE_TOGGLE_BUTTON_CARPARK_LIST"
    static let TOGGLE_LANDING_DROP_PIN_SCREEN = "TOGGLE_LANDING_DROP_PIN_SCREEN"
    static let SELECTED_INDEX_SHARE_COLLECTION = "SELECTED_INDEX_SHARE_COLLECTION"
    static let TOGGLE_SAVE_DROP_PIN_LOCATION_LANDING = "TOGGLE_SAVE_DROP_PIN_LOCATION_LANDING"
    static let TOGGLE_DROP_PIN_LANDING = "TOGGLE_DROP_PIN_LANDING"
    static let TOGGLE_SAVE_LOCATION_COLLECTION_DETAILS = "TOGGLE_SAVE_LOCATION_COLLECTION_DETAILS"
    static let TOGGLE_DROP_PIN_COLLECTION_DETAILS = "TOGGLE_DROP_PIN_COLLECTION_DETAILS"
    static let TOGGLE_SAVE_LOCATION_SHARED_COLLECTION = "TOGGLE_SAVE_LOCATION_SHARED_COLLECTION"
    

    static func callRN(_ functionName: String, data: NSDictionary?) -> Single<NSDictionary?>  {
        return Single.create { single in
            let requestId = NSUUID().uuidString as NSString
            let body = [
                "requestId": requestId,
                "functionName": functionName,
                "data": data as Any
            ] as [String : Any];
            
            // Send an event to RN
            EventEmitter.sharedInstance.dispatch(name: ShareBusinessLogicModule.SHARE_BUSINESS_LOGIC_REQUEST, body: body)
            
            // Store the RX event in the dictionary
            ShareBusinessLogicModule.shared.addRequest(requestId: requestId, request: single)
            
            return Disposables.create()
        }
    }
    
    func addRequest(requestId: NSString, request: @escaping (SingleEvent<NSDictionary?>) -> Void) {
        concurentQueue.async(flags: .barrier) {
            self.unSafependingRequests[requestId] = request
        }
    }
    
    func removeRequest(requestId: NSString) {
        concurentQueue.async(flags: .barrier) {
            self.unSafependingRequests[requestId] = nil
        }
    }
    
    @objc
    func finishedRequest(_ requestId: NSString, data: NSDictionary?) {
        // Get Rx event with requestId
        // Don't need weak self here, because ShareBusinessLogicModule is singleton
        guard let request = ShareBusinessLogicModule.shared.pendingRequests[requestId] else { return }
        
        self.concurentQueue.async {
            request(.success(data))
        }
        self.removeRequest(requestId: requestId)
    }
    
    @objc
    func failedRequest(_ requestId: NSString, errorMessage: NSString?) {
        // Don't need weak self here, because ShareBusinessLogicModule is singleton
        
        guard let request = ShareBusinessLogicModule.shared.pendingRequests[requestId] else { return }
        
        let message = (errorMessage as? String) ?? ""
        self.concurentQueue.async {
            request(.failure(NSError(domain: message, code: 0, userInfo: [NSLocalizedDescriptionKey: message])))
        }
        
        self.removeRequest(requestId: requestId)
    }
    
    @objc
    func constantsToExport() -> [String: Any]! {
        return [
            ShareBusinessLogicModule.SHARE_BUSINESS_LOGIC_REQUEST: ShareBusinessLogicModule.SHARE_BUSINESS_LOGIC_REQUEST,
            ShareBusinessLogicModule.FN_GET_RECENT_LIST: ShareBusinessLogicModule.FN_GET_RECENT_LIST,
            ShareBusinessLogicModule.FN_GET_FAV_LIST: ShareBusinessLogicModule.FN_GET_FAV_LIST,
            ShareBusinessLogicModule.FN_GET_SHORTCUT_LIST: ShareBusinessLogicModule.FN_GET_SHORTCUT_LIST,
            ShareBusinessLogicModule.FN_SEARCH_LOCATION: ShareBusinessLogicModule.FN_SEARCH_LOCATION,
            ShareBusinessLogicModule.FN_GET_ETA_LIST: ShareBusinessLogicModule.FN_GET_ETA_LIST,
            ShareBusinessLogicModule.FN_SAVE_RECENT_LIST: ShareBusinessLogicModule.FN_SAVE_RECENT_LIST,
            ShareBusinessLogicModule.FN_GET_GOOGLE_LAT_LNG_IF_NEED: ShareBusinessLogicModule.FN_GET_GOOGLE_LAT_LNG_IF_NEED,
            ShareBusinessLogicModule.FN_GET_ONBOARDING_STATE:ShareBusinessLogicModule.FN_GET_ONBOARDING_STATE,
            ShareBusinessLogicModule.FN_SAVE_ONBOARDING_STATE:ShareBusinessLogicModule.FN_SAVE_ONBOARDING_STATE,
            ShareBusinessLogicModule.FN_GET_LOCAL_USER_PREFERENCES:ShareBusinessLogicModule.FN_GET_LOCAL_USER_PREFERENCES,
            ShareBusinessLogicModule.FN_SAVE_LOCAL_USER_PREFERENCES:ShareBusinessLogicModule.FN_SAVE_LOCAL_USER_PREFERENCES,
            ShareBusinessLogicModule.FN_UPDATE_UPCOMING_TRIPS:ShareBusinessLogicModule.FN_UPDATE_UPCOMING_TRIPS,
            ShareBusinessLogicModule.SELECT_TRAFFIC_CAMERA:ShareBusinessLogicModule.SELECT_TRAFFIC_CAMERA,
            ShareBusinessLogicModule.OPEN_CALCULATE_FEE_CARPARK:ShareBusinessLogicModule.OPEN_CALCULATE_FEE_CARPARK,
            ShareBusinessLogicModule.PARKING_BOTTOM_SHEET_REFRESH:ShareBusinessLogicModule.PARKING_BOTTOM_SHEET_REFRESH,
            ShareBusinessLogicModule.SEND_ERP_DATA_TO_REACT:ShareBusinessLogicModule.SEND_ERP_DATA_TO_REACT,
            ShareBusinessLogicModule.SEND_EVENT_SELECTED_ERP:ShareBusinessLogicModule.SEND_EVENT_SELECTED_ERP,
            ShareBusinessLogicModule.SEND_THEME_CHANGE_EVENT:ShareBusinessLogicModule.SEND_THEME_CHANGE_EVENT,
            ShareBusinessLogicModule.NAVIGATE_TO_HOME_TAB:ShareBusinessLogicModule.NAVIGATE_TO_HOME_TAB,
            ShareBusinessLogicModule.NAVIGATE_TO_INBOX_SCREEN:ShareBusinessLogicModule.NAVIGATE_TO_INBOX_SCREEN,
            ShareBusinessLogicModule.READ_INBOX_EVENT:ShareBusinessLogicModule.READ_INBOX_EVENT,
            ShareBusinessLogicModule.OPEN_NOTIFICATION_FROM_NATIVE:ShareBusinessLogicModule.OPEN_NOTIFICATION_FROM_NATIVE,
            ShareBusinessLogicModule.RECEIVE_NEW_NOTIFICATIONS:ShareBusinessLogicModule.RECEIVE_NEW_NOTIFICATIONS,
            ShareBusinessLogicModule.OPEN_ONBOARDING_TUTORIAL: ShareBusinessLogicModule.OPEN_ONBOARDING_TUTORIAL,
            ShareBusinessLogicModule.DISABLE_PROFILE_AMENITIES: ShareBusinessLogicModule.DISABLE_PROFILE_AMENITIES,
            ShareBusinessLogicModule.OPEN_MY_VOUCHER_FROM_NOTIFICATION: ShareBusinessLogicModule.OPEN_MY_VOUCHER_FROM_NOTIFICATION,
            ShareBusinessLogicModule.UPDATE_BOTTOM_SHEET_INDEX: ShareBusinessLogicModule.UPDATE_BOTTOM_SHEET_INDEX,
            ShareBusinessLogicModule.UPDATE_AMENITIES_PREF: ShareBusinessLogicModule.UPDATE_AMENITIES_PREF,
            ShareBusinessLogicModule.TRIGGER_INIT_USER_PREF: ShareBusinessLogicModule.TRIGGER_INIT_USER_PREF,
            ShareBusinessLogicModule.UPDATE_SAVED_LOCATION: ShareBusinessLogicModule.UPDATE_SAVED_LOCATION,
            ShareBusinessLogicModule.SELECT_SAVED_LOCATION: ShareBusinessLogicModule.SELECT_SAVED_LOCATION,
            ShareBusinessLogicModule.REMOVE_SHORTCUT_FROM_MAP: ShareBusinessLogicModule.REMOVE_SHORTCUT_FROM_MAP,
            ShareBusinessLogicModule.SWITCH_BOTTOM_TAB: ShareBusinessLogicModule.SWITCH_BOTTOM_TAB,
            ShareBusinessLogicModule.ON_FINISHED_INITIALIZING_USER_DATA: ShareBusinessLogicModule.ON_FINISHED_INITIALIZING_USER_DATA,
            ShareBusinessLogicModule.CAN_SHOW_VOUCHER_ANIMATION: ShareBusinessLogicModule.CAN_SHOW_VOUCHER_ANIMATION,
            ShareBusinessLogicModule.ON_NEW_VOUCHER_ADDED: ShareBusinessLogicModule.ON_NEW_VOUCHER_ADDED,
            ShareBusinessLogicModule.RESET_AMENITY_SELECTIONS:ShareBusinessLogicModule.RESET_AMENITY_SELECTIONS,
            ShareBusinessLogicModule.SHOW_OR_HIDE_BUTTON_CARPARK_LIST:ShareBusinessLogicModule.SHOW_OR_HIDE_BUTTON_CARPARK_LIST,
            ShareBusinessLogicModule.DISMISS_SHARING_DIALOG:ShareBusinessLogicModule.DISMISS_SHARING_DIALOG,
            ShareBusinessLogicModule.OPEN_SAVED_FROM_NATIVE:ShareBusinessLogicModule.OPEN_SAVED_FROM_NATIVE,
            ShareBusinessLogicModule.UPDATE_PARKING_AVAILABLE_STATUS:ShareBusinessLogicModule.UPDATE_PARKING_AVAILABLE_STATUS,
            ShareBusinessLogicModule.UPDATE_OBU_LITE_SPEED:ShareBusinessLogicModule.UPDATE_OBU_LITE_SPEED,
            ShareBusinessLogicModule.UPDATE_OBU_EVENT_NOTI:ShareBusinessLogicModule.UPDATE_OBU_EVENT_NOTI,
            ShareBusinessLogicModule.UPDATE_OBU_CASH_CARD_INFO:ShareBusinessLogicModule.UPDATE_OBU_CASH_CARD_INFO,
            ShareBusinessLogicModule.OPEN_MODAL_OBU_INSTALL:ShareBusinessLogicModule.OPEN_MODAL_OBU_INSTALL,
            ShareBusinessLogicModule.CHANGE_STATUS_PAIR:ShareBusinessLogicModule.CHANGE_STATUS_PAIR,
            ShareBusinessLogicModule.OPEN_OBU_CARPARK_AVAILABILITY:ShareBusinessLogicModule.OPEN_OBU_CARPARK_AVAILABILITY,
            ShareBusinessLogicModule.OPEN_OBU_TRAVEL_TIME:ShareBusinessLogicModule.OPEN_OBU_TRAVEL_TIME,
            ShareBusinessLogicModule.UPDATE_PAIRED_VEHICLE_LIST:ShareBusinessLogicModule.UPDATE_PAIRED_VEHICLE_LIST,
            ShareBusinessLogicModule.UPDATE_VALUE_AUTO_CONNECT_OBU:ShareBusinessLogicModule.UPDATE_VALUE_AUTO_CONNECT_OBU,
            ShareBusinessLogicModule.BACK_TO_HOME_PAGE:ShareBusinessLogicModule.BACK_TO_HOME_PAGE,
            ShareBusinessLogicModule.SET_NO_CARD:ShareBusinessLogicModule.SET_NO_CARD,
            ShareBusinessLogicModule.DISABLE_TOGGLE_BUTTON_CARPARK_LIST:ShareBusinessLogicModule.DISABLE_TOGGLE_BUTTON_CARPARK_LIST,
            ShareBusinessLogicModule.TOGGLE_LANDING_DROP_PIN_SCREEN:ShareBusinessLogicModule.TOGGLE_LANDING_DROP_PIN_SCREEN,
            ShareBusinessLogicModule.SELECTED_INDEX_SHARE_COLLECTION:ShareBusinessLogicModule.SELECTED_INDEX_SHARE_COLLECTION,            
            ShareBusinessLogicModule.TOGGLE_SAVE_DROP_PIN_LOCATION_LANDING:ShareBusinessLogicModule.TOGGLE_SAVE_DROP_PIN_LOCATION_LANDING,
            ShareBusinessLogicModule.TOGGLE_SAVE_LOCATION_COLLECTION_DETAILS:ShareBusinessLogicModule.TOGGLE_SAVE_LOCATION_COLLECTION_DETAILS,
            ShareBusinessLogicModule.TOGGLE_SAVE_LOCATION_SHARED_COLLECTION:ShareBusinessLogicModule.TOGGLE_SAVE_LOCATION_SHARED_COLLECTION,
            ShareBusinessLogicModule.TOGGLE_DROP_PIN_LANDING:ShareBusinessLogicModule.TOGGLE_DROP_PIN_LANDING,
            ShareBusinessLogicModule.TOGGLE_DROP_PIN_COLLECTION_DETAILS:ShareBusinessLogicModule.TOGGLE_DROP_PIN_COLLECTION_DETAILS,
            ShareBusinessLogicModule.SHARE_LOCATION_FROM_NATIVE:ShareBusinessLogicModule.SHARE_LOCATION_FROM_NATIVE,
            ShareBusinessLogicModule.LOCATION_INVITE_FROM_NATIVE:ShareBusinessLogicModule.LOCATION_INVITE_FROM_NATIVE,
            ShareBusinessLogicModule.OPEN_COLLECTION_DETAIL_FROM_SHARED:ShareBusinessLogicModule.OPEN_COLLECTION_DETAIL_FROM_SHARED
        ]
    }
}


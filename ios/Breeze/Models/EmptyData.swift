//
//  EmptyModel.swift
//  Breeze
//
//  Created by Zhou Hao on 19/2/21.
//

import Foundation

// used in some cases the backend return nothing
struct EmptyData: Decodable {}

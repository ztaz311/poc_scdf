//
//  BroadcastMessage.swift
//  Breeze
//
//  Created by Tuan, Pham Hai on 09/02/2023.
//

import Foundation

struct BroadcastMessageModel: Codable {
    let success: Bool?
    let message: String?
    var criterias: [CriteriaItem]
    
    func hasBroadcastMessage() -> Bool {
        if let message = message, !message.isEmpty {
            return true
        }
        return false
    }
    
    func getBroadcastMessage() -> String {
        return message ?? ""
    }

    enum CodingKeys: String, CodingKey {
        case success
        case message
        case criterias = "notificationCriterias"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.success = try container.decodeIfPresent(Bool.self, forKey: .success)
        self.message = try container.decodeIfPresent(String.self, forKey: .message)
        self.criterias = try container.decodeIfPresent([CriteriaItem].self, forKey: .criterias) ?? []
    }
}

struct CriteriaItem: Codable {
    let distance: Double?
    let time: Double?
    var duration: Int?
    
    func hasDistance() -> Bool {
        return (distance ?? 0 > 0)
    }
    
    func hasTime() -> Bool {
        return (time ?? 0 > 0)
    }
    
    func getDistance() -> Double {
        return distance ?? 0
    }
    
    func getTime() -> Double {
        return time ?? 0
    }
    
    func getDuration() -> Int {
        return duration ?? 15   //  15 seconds as default
    }

    enum CodingKeys: String, CodingKey {
        case distance
        case time
        case duration
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.distance = try container.decodeIfPresent(Double.self, forKey: .distance)
        self.time = try container.decodeIfPresent(Double.self, forKey: .time)
        self.duration = try container.decodeIfPresent(Int.self, forKey: .duration)
    }
}


//struct RoutesBroadCastModel : Codable {
//    let coordinates : [[Double]]
//    
//    func getPostData() -> [String: Any] {
//        var retValue: [String: Any]
//        var routes: [[String: Any]] = []
//        for coordinate in coordinates {
//            
//        }
//        
//        retValue["routes"] = routes
//        return retValue
//    }
//
//    enum CodingKeys: String, CodingKey {
//        case coordinates = "coordinates"
//    }
//
//    init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        coordinates = try values.decodeIfPresent([[Double]].self, forKey: .coordinates) ?? []
//    }
//}

struct BroadCastModel: Codable {
    let success: Bool?
    var broadcast: [MessageBroadCastModel]
    
    enum CodingKeys: String, CodingKey {
        case success
        case broadcast = "broadcasts"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.success = try container.decodeIfPresent(Bool.self, forKey: .success)
        self.broadcast = try container.decodeIfPresent([MessageBroadCastModel].self, forKey: .broadcast) ?? []
    }
}


struct MessageBroadCastModel: Codable {
    let name: String?
    let title: String?
    let message: String?
    let type: String?
    let value: String?
    let action: TypeCarParkModel?
    let alertLevel: String?
    
    
    enum CodingKeys: String, CodingKey {
        case name
        case message
        case title
        case type
        case value
        case alertLevel
        case action
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.name = try container.decodeIfPresent(String.self, forKey: .name)
        self.message = try container.decodeIfPresent(String.self, forKey: .message)
        self.title = try container.decodeIfPresent(String.self, forKey: .title)
        self.type = try container.decodeIfPresent(String.self, forKey: .type)
        self.value = try container.decodeIfPresent(String.self, forKey: .value)
        self.action = try container.decodeIfPresent(TypeCarParkModel.self, forKey: .action)
        self.alertLevel = try container.decodeIfPresent(String.self, forKey: .alertLevel)
        
    }
}


struct TypeCarParkModel: Codable {
    let data: DataCarparkModel?
    let type: String?
    
    enum CodingKeys: String, CodingKey {
        case data
        case type
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.data = try container.decodeIfPresent(DataCarparkModel.self, forKey: .data)
        self.type = try container.decodeIfPresent(String.self, forKey: .type)
    }
}

struct DataCarparkModel: Codable {
    let id: String?
    let type: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case type
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decodeIfPresent(String.self, forKey: .id)
        self.type = try container.decodeIfPresent(String.self, forKey: .type)
    }
    
}

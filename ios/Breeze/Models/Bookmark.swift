//
//  Boomark.swift
//  Breeze
//
//  Created by Tuyen, Le Xuan  on 01/08/2022.
//

import Foundation
import CoreLocation

enum BookmarkCode: String {
    case home = "HOME"
    case work = "WORK"
    case normal = "NORMAL"
    case custom = "CUSTOM"

}

class SearchResult {
    
    var searchedLocation: CLLocationCoordinate2D?
    var destName = ""
    var searchAddress = ""
    var fullAddress = ""
    var isBookmarked = false
    var bookmarkId: Int?
    var carparkId: String? = nil
    var selectedRoutablePoint: RoutablePointModel?
    
    func getDropPinModel() -> DropPinModel {

        if let routeablePoint = selectedRoutablePoint {
            return DropPinModel(routeablePoint.name, address: routeablePoint.address1, lat: routeablePoint.lat?.toString() ?? "", long: routeablePoint.long?.toString() ?? "", amenityId: routeablePoint.carparkId ?? "", isSearch: true, address2: routeablePoint.address2, fullAddress: routeablePoint.fullAddress)
        } else {
            return DropPinModel(destName, address: fullAddress, lat: searchedLocation?.latitude.toString() ?? "", long: searchedLocation?.longitude.toString() ?? "", amenityId: "", isSearch: true, address2: searchAddress, fullAddress: fullAddress)
        }
    }
    
    init(_ result: [String: Any]) {
        
        let lat = result["lat"] as? Double ?? 0.0
        let long = result["long"] as? Double ?? 0.0
        
        destName = result["destName"] as? String ?? ""
        searchAddress = result["address"] as? String ?? ""
        isBookmarked = result["isBookmarked"] as? Bool ?? false
        bookmarkId = result["bookmarkId"] as? Int
        fullAddress = result["fullAddress"] as? String ?? ""
        carparkId = result["carparkId"] as? String ?? ""
        
        var location = CLLocationCoordinate2D(latitude: lat, longitude: long)
        
        if let selectedRoutablePoint = parseSelectedRoutablePoint(result),
            let coordinate = selectedRoutablePoint.coordinate {
            self.selectedRoutablePoint = selectedRoutablePoint
            location = coordinate
            carparkId = selectedRoutablePoint.carparkId
        } else {
            self.selectedRoutablePoint = nil
            self.carparkId = nil
        }
             
        self.searchedLocation = location
    }
}

class Bookmark: Codable {
    var bookmarkId: Int?
    let bookmarkSnapshotId: Int?
    let lat: String?
    let long: String?
    var name: String?
    let saved: Bool?
    let address1: String?
    let address2: String?
    let amenityType: String?
    let amenityId: String?
    var isSelected: Bool?
    let welcomeDescription: String?
    var code: String?
    let collectionId: Int?
    let fullAddress: String?
    let availablePercentImageBand: String?  //  TYPE0 / TYPE1 / TYPE2
    
    //  Additional data for crowdsource
    let placeId: String?
    let userLocationLinkIdRef: Int?
    let showAvailabilityFB: Bool?
    let hasAvailabilityCS: Bool?
    let availabilityCSData: AvailabilityCSData?
    
    func getCarparkId() -> String {
        if amenityType == Values.AMENITYCARPARK {
            return amenityId ?? ""
        }
        return ""
    }
    
    func updateBookmarkId(_ bookmarkId: Int) {
        self.bookmarkId = bookmarkId
    }
    
    func getType() -> String {
        
        var type = ""
        
        if let amenityType = amenityType, !amenityType.isEmpty {
            switch amenityType {
            case Values.AMENITYCARPARK:
                
                if saved ?? false {
                    type = Values.SHARE_CARPARK_FULL_BOOKMARKED
                    if let available = availablePercentImageBand {
                        switch available {
                        case "TYPE0":
                            type = Values.SHARE_CARPARK_FULL_BOOKMARKED
                        case "TYPE1":
                            type = Values.SHARE_CARPARK_FILLING_UP_BOOKMARKED
                        case "TYPE2":
                            type = Values.SHARE_CARPARK_AVAILABLE_BOOKMARKED
                        default:
                            break
                        }
                    }
                } else {
                    type = Values.SHARE_CARPARK_AVAILABLE
                    if let available = availablePercentImageBand {
                        switch available {
                        case "TYPE0":
                            type = Values.SHARE_CARPARK_FULL
                        case "TYPE1":
                            type = Values.SHARE_CARPARK_FILLING_UP
                        case "TYPE2":
                            type = Values.SHARE_CARPARK_AVAILABLE
                        default:
                            break
                        }
                    }
                }
                
            case Values.PETROL:
                if saved ?? false {
                    type = Values.SHARE_PETROL_BOOKMARKED
                } else {
                    type = Values.SHARE_PETROL_ICON
                }
            case Values.EV_CHARGER:
                if saved ?? false {
                    type = Values.SHARE_EV_BOOKMARKED
                } else {
                    type = Values.SHARE_EV_ICON
                }
            default:
                break
            }
        }
                
        if type.isEmpty {
            if let code = code, !code.isEmpty {
                switch code {
                case BookmarkCode.custom.rawValue, BookmarkCode.normal.rawValue:
                    if saved ?? false {
                        type = Values.SHARE_BOOKMARK_ICON
                    } else {
                        type = Values.SHARE_DESTINATION_ICON
                    }
                case BookmarkCode.home.rawValue:
                    type = Values.SHARE_HOME_LOCATION
                case BookmarkCode.work.rawValue:
                    type = Values.SHARE_WORK_LOCATION
                default:
                    break
                }
            }
        }
        
        if type.isEmpty {
            if saved ?? false {
                type = Values.SHARE_BOOKMARK_ICON
            } else {
                type = Values.SHARE_DESTINATION_ICON
            }
        }
        
        return type
    }
    
    func getDic4RN() -> [String: Any] {
        let retValue: [String: Any] = ["address1": address1 as Any,
                                       "address2": address2 as Any,
                                       "lat": lat as Any,
                                       "long": long as Any,
                                       "code": code as Any,
                                       "isDestination": welcomeDescription as Any,
                                       "amenityId": amenityId as Any,
                                       "amenityType": amenityType as Any,
                                       "description": welcomeDescription as Any,
                                       "name": name as Any,
                                       "bookmarkSnapshotId": bookmarkSnapshotId as Any,
                                       "fullAddress": fullAddress as Any
        ]
        return retValue
    }
    
    enum CodingKeys: String, CodingKey {
        case bookmarkId = "bookmarkId"
        case bookmarkSnapshotId
        case lat, long, name
        case saved
        case welcomeDescription = "description"
        case address1, code, address2, amenityType
        case amenityId
        case isSelected, collectionId
        case fullAddress
        case availablePercentImageBand
        case placeId, userLocationLinkIdRef, showAvailabilityFB, hasAvailabilityCS, availabilityCSData
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.bookmarkId = try container.decodeIfPresent(Int.self, forKey: .bookmarkId)
        self.bookmarkSnapshotId = try container.decodeIfPresent(Int.self, forKey: .bookmarkSnapshotId)
        self.lat = try container.decodeIfPresent(String.self, forKey: .lat)
        self.long = try container.decodeIfPresent(String.self, forKey: .long)
        self.name = try container.decodeIfPresent(String.self, forKey: .name)
        self.saved = try container.decodeIfPresent(Bool.self, forKey: .saved)
        self.welcomeDescription = try container.decodeIfPresent(String.self, forKey: .welcomeDescription)
        self.address1 = try container.decodeIfPresent(String.self, forKey: .address1)
        self.code = try container.decodeIfPresent(String.self, forKey: .code)
        self.address2 = try container.decodeIfPresent(String.self, forKey: .address2)
        self.amenityType = try container.decodeIfPresent(String.self, forKey: .amenityType)
        self.amenityId = try container.decodeIfPresent(String.self, forKey: .amenityId)
        self.isSelected = try container.decodeIfPresent(Bool.self, forKey: .isSelected)
        self.collectionId = try container.decodeIfPresent(Int.self, forKey: .collectionId)
        self.fullAddress = try container.decodeIfPresent(String.self, forKey: .fullAddress)
        self.availablePercentImageBand = try container.decodeIfPresent(String.self, forKey: .availablePercentImageBand)
        
        self.placeId = try container.decodeIfPresent(String.self, forKey: .placeId)
        self.userLocationLinkIdRef = try container.decodeIfPresent(Int.self, forKey: .userLocationLinkIdRef)
        self.showAvailabilityFB = try container.decodeIfPresent(Bool.self, forKey: .showAvailabilityFB)
        self.hasAvailabilityCS = try container.decodeIfPresent(Bool.self, forKey: .hasAvailabilityCS)
        self.availabilityCSData = try container.decodeIfPresent(AvailabilityCSData.self, forKey: .availabilityCSData)
    }
}

extension Bookmark: Equatable {
    static func == (lhs: Bookmark, rhs: Bookmark) -> Bool {
        return lhs.bookmarkId == rhs.bookmarkId && lhs.isSelected == rhs.isSelected
    }
    
    var bmId: String {
        return "\(bookmarkId ?? 0)"
    }
    
    var bookmarkCode: BookmarkCode? {
        return BookmarkCode(rawValue: self.code ?? "")
    }
    
    static func bookmark(from: NSDictionary?) -> Bookmark? {
        if let json = from,
           let data = try? JSONSerialization.data(withJSONObject: json, options: .prettyPrinted),
           let bookmark = try? JSONDecoder().decode(Bookmark.self, from: data) {
            return bookmark
        }
        return nil
    }
}

extension Bookmark {
    
    var location: CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: lat?.toDouble() ?? 0, longitude: long?.toDouble() ?? 0)
    }
    
    var bookmarkDes: String {
        var description = welcomeDescription ?? ""
        if let code = bookmarkCode {
            switch code {
            case .home, .work, .normal:
                description = address1 ?? ""
            case .custom:
                description = address1 ?? ""
            }
        }
        return description
    }
}

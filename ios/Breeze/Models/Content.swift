//
//  Content.swift
//  Breeze
//
//  Created by Zhou Hao on 28/7/22.
//

import Foundation

struct ContentBase: Codable {
    let contentID,recentre_zone_radius: Int
    let name, type,layer_code: String
    let isNewLayer: Bool
    let details: [ContentDetail]
    let zones: [ContentZone]
    let trackNavigation, enableWalking, enableZoneDrive, canReroute: Bool?
    let messageCategory, messageType, statsIconDark, statsIconLight, messageTitle: String?
    
    func getEnableWalking() -> Bool {
        return enableWalking ?? false
    }
    func getEnableZoneDrive() -> Bool {
        return enableZoneDrive ?? false
    }
    
    func getTrackNavigation() -> Bool {
        return trackNavigation ?? false
    }
    
    func getCanReroute() -> Bool {
        return canReroute ?? true
    }

    enum CodingKeys: String, CodingKey {
        case contentID = "content_id"
        case recentre_zone_radius = "recentre_zone_radius"
        case name, type, details, zones,layer_code
        case isNewLayer = "is_new_layer"
        case trackNavigation = "track_navigation"
        case messageCategory = "message_category"
        case messageType = "message_type"
        case enableWalking
        case enableZoneDrive = "enable_zone_drive"
        case canReroute = "can_reroute"
        case statsIconDark = "stats_icon_dark"
        case statsIconLight = "stats_icon_light"
        case messageTitle = "message_title"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.contentID = try container.decode(Int.self, forKey: .contentID)
        self.recentre_zone_radius = try container.decodeIfPresent(Int.self, forKey: .recentre_zone_radius) ?? 0
        self.name = try container.decodeIfPresent(String.self, forKey: .name) ?? ""
        self.type = try container.decodeIfPresent(String.self, forKey: .type) ?? ""
        self.details = try container.decodeIfPresent([ContentDetail].self, forKey: .details) ?? []
        self.zones = try container.decodeIfPresent([ContentZone].self, forKey: .zones) ?? []
        self.layer_code = try container.decodeIfPresent(String.self, forKey: .layer_code) ?? ""
        self.isNewLayer = try container.decodeIfPresent(Bool.self, forKey: .isNewLayer) ?? false
        self.trackNavigation = try container.decodeIfPresent(Bool.self, forKey: .trackNavigation)
        self.messageCategory = try container.decodeIfPresent(String.self, forKey: .messageCategory)
        self.messageType = try container.decodeIfPresent(String.self, forKey: .messageType)
        self.enableWalking = try container.decodeIfPresent(Bool.self, forKey: .enableWalking)
        self.enableZoneDrive = try container.decodeIfPresent(Bool.self, forKey: .enableZoneDrive)
        self.canReroute = try container.decodeIfPresent(Bool.self, forKey: .canReroute)
        self.statsIconDark = try container.decodeIfPresent(String.self, forKey: .statsIconDark)
        self.statsIconLight = try container.decodeIfPresent(String.self, forKey: .statsIconLight)
        self.messageTitle = try container.decodeIfPresent(String.self, forKey: .messageTitle)
    }
}

// MARK: - Content Detail
struct ContentDetail: Codable {
    let contentDetailsID: Int
    let name, code: String
    let mapLightSelectedURL, mapLightUnselectedURL, mapDarkSelectedURL, mapDarkUnselectedURL: String?
    let tooltipRadius: Int?
    let tooltipType: String
    let clusterEnabled:Bool
    let clusterID: Int
    let clusterColor: String?
    let data: [ContentDetailData]

    enum CodingKeys: String, CodingKey {
        case contentDetailsID = "content_details_id"
        case name, code
        case mapLightSelectedURL = "map_light_selected_url"
        case mapLightUnselectedURL = "map_light_unselected_url"
        case mapDarkSelectedURL = "map_dark_selected_url"
        case mapDarkUnselectedURL = "map_dark_unselected_url"
        case tooltipRadius = "tooltip_radius"
        case tooltipType = "tooltip_type"
        case clusterEnabled = "cluster_enabled"
        case clusterID = "cluster_id"
        case clusterColor = "cluster_color"
        case data
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.contentDetailsID = try container.decode(Int.self, forKey: .contentDetailsID)
        self.name = try container.decodeIfPresent(String.self, forKey: .name) ?? ""
        self.code = try container.decodeIfPresent(String.self, forKey: .code) ?? ""
        self.mapLightSelectedURL = try container.decodeIfPresent(String.self, forKey: .mapLightSelectedURL)
        self.mapLightUnselectedURL = try container.decodeIfPresent(String.self, forKey: .mapLightUnselectedURL)
        self.mapDarkSelectedURL = try container.decodeIfPresent(String.self, forKey: .mapDarkSelectedURL)
        self.mapDarkUnselectedURL = try container.decodeIfPresent(String.self, forKey: .mapDarkUnselectedURL)
        self.tooltipRadius = try container.decodeIfPresent(Int.self, forKey: .tooltipRadius)
        self.tooltipType = try container.decodeIfPresent(String.self, forKey: .tooltipType) ?? ""
        self.clusterEnabled = try container.decodeIfPresent(Bool.self, forKey: .clusterEnabled) ?? false
        self.clusterID = try container.decodeIfPresent(Int.self, forKey: .clusterID) ?? 0
        self.clusterColor = try container.decodeIfPresent(String.self, forKey: .clusterColor)
        self.data = try container.decodeIfPresent([ContentDetailData].self, forKey: .data) ?? []
    }
}

// MARK: - Datum
struct ContentDetailData: Codable {
    let id, dataID, name: String
    let type: String?
    let isActive: Bool
    let provider: String?
    let address: String
    let long, lat:Double
    let startDate,endDate: Double?
    let location: DataLocation?
    let additionalInfo: AdditionalInfoModel?
    let availablePercentImageBand: String?
    let showAvailabilityFB: Bool?
    let hasAvailabilityCS: Bool?
    let availabilityCSData: AvailabilityCSData?
        
    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case dataID = "id"
        case name, type, isActive, provider, address, long, lat, location, startDate, endDate, additionalInfo, availablePercentImageBand, showAvailabilityFB, hasAvailabilityCS, availabilityCSData
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decodeIfPresent(String.self, forKey: .id) ?? ""
        self.dataID = try container.decodeIfPresent(String.self, forKey: .dataID) ?? ""
        self.name = try container.decodeIfPresent(String.self, forKey: .name) ?? ""
        self.type = try container.decodeIfPresent(String.self, forKey: .type)
        self.isActive = try container.decodeIfPresent(Bool.self, forKey: .isActive) ?? false
        self.provider = try container.decodeIfPresent(String.self, forKey: .provider)
        self.address = try container.decodeIfPresent(String.self, forKey: .address) ?? ""
        self.long = try container.decodeIfPresent(Double.self, forKey: .long) ?? 0.0
        self.lat = try container.decodeIfPresent(Double.self, forKey: .lat) ?? 0.0
        self.location = try container.decodeIfPresent(DataLocation.self, forKey: .location)
        self.startDate = try container.decodeIfPresent(Double.self, forKey: .startDate)
        self.endDate = try container.decodeIfPresent(Double.self, forKey: .endDate)
        self.additionalInfo = try container.decodeIfPresent(AdditionalInfoModel.self, forKey: .additionalInfo)
        self.availablePercentImageBand = try container.decodeIfPresent(String.self, forKey: .availablePercentImageBand) ?? ""
        self.showAvailabilityFB = try container.decodeIfPresent(Bool.self, forKey: .showAvailabilityFB) ?? false
        self.hasAvailabilityCS = try container.decodeIfPresent(Bool.self, forKey: .hasAvailabilityCS) ?? false
        self.availabilityCSData = try container.decodeIfPresent(AvailabilityCSData.self, forKey: .availabilityCSData)
        
    }
}

// MARK: - Content Articles
struct ContentArticles: Codable {
    let header: String
    let list: [ContentArticlesList]
}

// MARK: - Content article List
struct ContentArticlesList: Codable {
    let thumbnailURL: String
    let redirectURL: String
    let title, linkText: String

    enum CodingKeys: String, CodingKey {
        case thumbnailURL = "thumbnail_url"
        case redirectURL = "redirect_url"
        case title
        case linkText = "link_text"
    }
}

// MARK: - Features
struct DataFeatures: Codable {
    let atmMachine, toilet, pickupPoint, taxiStand: String?
    let truckLoading, lift, wheelChairAccess: String?
    let paymentTypes: [String]?

    enum CodingKeys: String, CodingKey {
        case atmMachine = "atm_machine"
        case toilet
        case pickupPoint = "pickup_point"
        case taxiStand = "taxi_stand"
        case truckLoading = "truck_loading"
        case lift
        case wheelChairAccess = "wheel_chair_access"
        case paymentTypes = "payment_types"
    }
}

// MARK: - Location
struct DataLocation: Codable {
    let type: String
    let coordinates: [Double]
}

// MARK: - UploadedFileDetails
struct UploadedFileDetails: Codable {
    let other: [Other]
}

// MARK: - Other
struct Other: Codable {
    let otherDescription: String
    let location: String
    let type: String
    let thumbnailURL: String

    enum CodingKeys: String, CodingKey {
        case otherDescription = "description"
        case location, type
        case thumbnailURL = "thumbnail_url"
    }
}

// MARK: - Zone
struct ContentZone: Codable {
    let contentZoneID: Int?
    let centerPointLat, centerPointLong: String
    let radius: Int
    let zoneID: String
    let activeZoomLevel, deactiveZoomLevel: Int

    enum CodingKeys: String, CodingKey {
        case contentZoneID = "content_zone_id"
        case centerPointLat = "center_point_lat"
        case centerPointLong = "center_point_long"
        case radius
        case zoneID = "zone_id"
        case activeZoomLevel = "active_zoom_level"
        case deactiveZoomLevel = "deactive_zoom_level"
    }
}

//
//  RecentContacts.swift
//  Breeze
//
//  Created by Zhou Hao on 2/6/22.
//

import Foundation

struct RecentContact: Codable {
    let tripEtaFavouriteId: Int?
    let recipientName: String?
    let recipientNumber: String?
    let lastETATime: Int?
    
    enum CodingKeys: String, CodingKey {
        case tripEtaFavouriteId
        case recipientName
        case recipientNumber
        case lastETATime
    }

    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        tripEtaFavouriteId = try values.decodeIfPresent(Int.self, forKey: .tripEtaFavouriteId)
        recipientName = try values.decodeIfPresent(String.self, forKey: .recipientName)
        recipientNumber = try values.decodeIfPresent(String.self, forKey: .recipientNumber)
        lastETATime = try values.decodeIfPresent(Int.self, forKey: .lastETATime)
    }
}

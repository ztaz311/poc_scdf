//
//  CalendarAddress.swift
//  Breeze
//
//  Created by Zhou Hao on 7/5/21.
//

import Foundation

struct CalendarAddress : BaseAddress, Codable {
    let eventId:String?
    let lat : String?
    let long : String?
    let alias : String?
    let address1 : String?
    let address2 : String?

    enum CodingKeys: String, CodingKey {

        case eventId = "eventId"
        case lat = "lat"
        case long = "long"
        case alias = "name"
        case address1 = "address1"
        case address2 = "address2"
    }

    init(eventId: String, alias: String,address1:String,lat:String,long:String,address2:String) {
        self.eventId = eventId
        self.alias = alias
        self.address1 = address1
        self.lat = lat
        self.long = long
        self.address2 = address2
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        eventId = try values.decodeIfPresent(String.self, forKey: .eventId)
        lat = try values.decodeIfPresent(String.self, forKey: .lat)
        long = try values.decodeIfPresent(String.self, forKey: .long)
        alias = try values.decodeIfPresent(String.self, forKey: .alias)
        address1 = try values.decodeIfPresent(String.self, forKey: .address1)
        address2 = try values.decodeIfPresent(String.self, forKey: .address2)
        
    }

}

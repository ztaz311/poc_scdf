//
//  BZRoute.swift
//  Breeze
//
//  Created by Zhou Hao on 9/9/21.
//

import Foundation

struct BZRoute: Codable {
    let wayPoints: [BZWayPoint]
}

struct BZWayPoint: Codable {
    let latitude: Double
    let longitude: Double
    let name: String
}

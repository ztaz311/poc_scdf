//
//  Erprates.swift
//  Breeze
//
//  Created by VishnuKanth on 07/02/21.
//

import Foundation
struct Erprates : Codable {
	let zoneid : String?
	let rates : [Rates]?
    
    func getRates() -> [Rates] {
        return rates ?? []
    }

	enum CodingKeys: String, CodingKey {

		case zoneid = "zoneid"
		case rates = "rates"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		zoneid = try values.decodeIfPresent(String.self, forKey: .zoneid)
		rates = try values.decodeIfPresent([Rates].self, forKey: .rates)
	}

}

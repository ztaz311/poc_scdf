//
//  Rates.swift
//  Breeze
//
//  Created by VishnuKanth on 07/02/21.
//

import Foundation
struct Rates : Codable {
	let vehicletype : String?
	let daytype : String?
	let starttime : String?
	let endtime : String?
	let chargeamount : Float?
	let effectivedate : String?
    
    func getChargeAmount() -> Float {
        return chargeamount ?? 0.0
    }
    
    func getTimeDisplayed() -> String {
        return "\(starttime ?? "")-\(endtime ?? "")"
    }

	enum CodingKeys: String, CodingKey {

		case vehicletype = "vehicletype"
		case daytype = "daytype"
		case starttime = "starttime"
		case endtime = "endtime"
		case chargeamount = "chargeamount"
		case effectivedate = "effectivedate"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		vehicletype = try values.decodeIfPresent(String.self, forKey: .vehicletype)
		daytype = try values.decodeIfPresent(String.self, forKey: .daytype)
		starttime = try values.decodeIfPresent(String.self, forKey: .starttime)
		endtime = try values.decodeIfPresent(String.self, forKey: .endtime)
		chargeamount = try values.decodeIfPresent(Float.self, forKey: .chargeamount)
		effectivedate = try values.decodeIfPresent(String.self, forKey: .effectivedate)
	}

}

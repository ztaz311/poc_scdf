
import Foundation
struct Publicholiday : Codable {
	let date : String?
	let startTime : String?
	let endTime : String?
	let chargeAmount : Int?

	enum CodingKeys: String, CodingKey {

		case date = "date"
		case startTime = "StartTime"
		case endTime = "EndTime"
		case chargeAmount = "ChargeAmount"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		date = try values.decodeIfPresent(String.self, forKey: .date)
		startTime = try values.decodeIfPresent(String.self, forKey: .startTime)
		endTime = try values.decodeIfPresent(String.self, forKey: .endTime)
		chargeAmount = try values.decodeIfPresent(Int.self, forKey: .chargeAmount)
	}

}

//
//  Erp.swift
//  Breeze
//
//  Created by VishnuKanth on 07/02/21.
//

import Foundation
import CoreLocation

struct Erp : Codable {
	let erpid : Int?
	let zoneid : String?
	let name : String?
	let startlat : String?
	let startlong : String?
	let endlat : String?
	let endlong : String?
    
    func getZoneId() -> String {
        return zoneid ?? ""
    }
    
    func getStartEndCoordinate() -> (CLLocationCoordinate2D, CLLocationCoordinate2D) {
        let startLat = startlat?.toDouble() ?? 0
        let startLong = startlong?.toDouble() ?? 0
        let endLat = endlat?.toDouble() ?? 0
        let endLong = endlong?.toDouble() ?? 0
        let startCoordinate = CLLocationCoordinate2D(latitude: startLat, longitude: startLong)
        let endCoordinate = CLLocationCoordinate2D(latitude: endLat, longitude: endLong)
        return (startCoordinate, endCoordinate)
    }

	enum CodingKeys: String, CodingKey {

		case erpid = "erpid"
		case zoneid = "zoneid"
		case name = "name"
		case startlat = "startlat"
		case startlong = "startlong"
		case endlat = "endlat"
		case endlong = "endlong"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		erpid = try values.decodeIfPresent(Int.self, forKey: .erpid)
		zoneid = try values.decodeIfPresent(String.self, forKey: .zoneid)
		name = try values.decodeIfPresent(String.self, forKey: .name)
		startlat = try values.decodeIfPresent(String.self, forKey: .startlat)
		startlong = try values.decodeIfPresent(String.self, forKey: .startlong)
		endlat = try values.decodeIfPresent(String.self, forKey: .endlat)
		endlong = try values.decodeIfPresent(String.self, forKey: .endlong)
	}

}

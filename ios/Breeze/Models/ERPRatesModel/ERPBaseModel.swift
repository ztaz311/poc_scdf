//
//  ERPBaseModel.swift
//  Breeze
//
//  Created by VishnuKanth on 07/02/21.
//

import Foundation
struct ERPBaseModel : Codable {
    let erp : [Erp]?
    let erprates : [Erprates]?
    let publicholiday : [Publicholiday]?
    
    func getERPRates() -> [Erprates] {
        return erprates ?? []
    }
    
    func getPublicHolidayRates() -> [Publicholiday] {
        return publicholiday ?? []
    }

    enum CodingKeys: String, CodingKey {
        case erp = "erp"
        case erprates = "erprates"
        case publicholiday = "publicholiday"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        erp = try values.decodeIfPresent([Erp].self, forKey: .erp)
        erprates = try values.decodeIfPresent([Erprates].self, forKey: .erprates)
        publicholiday = try values.decodeIfPresent([Publicholiday].self, forKey: .publicholiday)
    }

}

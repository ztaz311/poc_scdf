//
//  GuestUserIdModel.swift
//  Breeze
//
//  Created by Tuan, Pham Hai on 14/03/2023.
//

import Foundation

struct GuestUserIdModel: Codable {
    let id: String?
    let success: Bool?
    
    func getID() -> String {
        return id ?? ""
    }
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case success = "success"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decodeIfPresent(String.self, forKey: .id)
        self.success = try container.decodeIfPresent(Bool.self, forKey: .success)
    }
}

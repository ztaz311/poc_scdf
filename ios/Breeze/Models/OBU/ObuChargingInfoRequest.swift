//
//  ObuChargingInfoRequest.swift
//  Breeze
//
//  Created by Tuan, Pham Hai on 05/06/2023.
//

import Foundation

struct ObuChargingInfoRequest: Codable {
    
    let tripGUID : String
    let eventTimestamp : Int?
    let roadName : String?
    let chargeAmount : Double?
    let chargingType : String?
    let chargingMessageType : String?
    let content1 : String?
    let content2 : String?
    let content3 : String?
    let content4 : String?
    let content5 : String?
    
    enum CodingKeys: String, CodingKey {
        case tripGUID = "tripGUID"
        case eventTimestamp = "eventTimestamp"
        case roadName = "roadName"
        case chargeAmount = "chargeAmount"
        case chargingType = "chargingType"
        case chargingMessageType = "chargingMessageType"
        case content1 = "content1"
        case content2 = "content2"
        case content3 = "content3"
        case content4 = "content4"
        case content5 = "content5"
    }
}

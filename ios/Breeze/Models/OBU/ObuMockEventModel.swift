//
//  ObuMockEventModel.swift
//  Breeze
//
//  Created by Tuan, Pham Hai on 10/06/2023.
//

import Foundation

struct ObuMockCarparkModel: Codable {
    let name: String?
    let lots: String?
    let color: String?

    enum CodingKeys: String, CodingKey {
        case name
        case lots = "availableLots"
        case color
    }
    
    func getParking() -> Parking {
        return Parking(location: name ?? "", lots: lots ?? "", color: color ?? "")
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.name = try container.decodeIfPresent(String.self, forKey: .name)
        self.lots = try container.decodeIfPresent(String.self, forKey: .lots)
        self.color = try container.decodeIfPresent(String.self, forKey: .color)
    }
}

struct ObuMockTravelModel: Codable {
    let name: String?
    let estTime: String?
    let color: String?
    
    enum CodingKeys: String, CodingKey {
        case name
        case estTime
        case color
    }
    
    func getTravelTime() -> TravelTime {
        return TravelTime(location: name ?? "", min: estTime ?? "", icon: "", color: color ?? "")
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.name = try container.decodeIfPresent(String.self, forKey: .name)
        self.estTime = try container.decodeIfPresent(String.self, forKey: .estTime)
        self.color = try container.decodeIfPresent(String.self, forKey: .color)
    }
}

struct ObuMockDataModel: Codable {
    let message: String?
    let distance: String?
    let detailTitle: String?
    let detailMessage: String?
    let chargeAmount: String?
    var carparks: [ObuMockCarparkModel]
    var travelTime: [ObuMockTravelModel]
    
    func getDic() -> [String: Any] {
        var retDic: [String: Any] = [:]
        
        if let mes = message, !mes.isEmpty {
            retDic["message"] = mes
        }
        if let dis = distance, !dis.isEmpty {
            retDic["distance"] = dis
        }
        if let title = detailTitle, !title.isEmpty {
            retDic["detailTitle"] = title
        }
        if let detailMes = detailMessage, !detailMes.isEmpty {
            retDic["detailMessage"] = detailMes
        }
        if let amount = chargeAmount, !amount.isEmpty {
            retDic["chargeAmount"] = amount
        }
        if !carparks.isEmpty {
            var items: [Any] = []
            for item in carparks {
                let carparkDic = ["name": item.name, "availableLots": item.lots]
                items.append(carparkDic)
            }
            retDic["carparks"] = items
        }
        
        if !travelTime.isEmpty {
            var params: [Any] = []
            for item in travelTime {
                let travelDic: [String: Any] = ["name": item.name ?? "", "estTime": item.estTime ?? "", "color": item.color ?? "Default"]
                params.append(travelDic)
            }
            retDic["data"] = params
        }
        
        return retDic
    }
    
    enum CodingKeys: String, CodingKey {
        case message
        case distance
        case detailTitle
        case detailMessage
        case chargeAmount
        case carparks
        case travelTime
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.message = try container.decodeIfPresent(String.self, forKey: .message)
        self.distance = try container.decodeIfPresent(String.self, forKey: .distance)
        self.detailTitle = try container.decodeIfPresent(String.self, forKey: .detailTitle)
        self.detailMessage = try container.decodeIfPresent(String.self, forKey: .detailMessage)
        self.chargeAmount = try container.decodeIfPresent(String.self, forKey: .chargeAmount)
        self.carparks = try container.decodeIfPresent([ObuMockCarparkModel].self, forKey: .carparks) ?? []
        self.travelTime = try container.decodeIfPresent([ObuMockTravelModel].self, forKey: .travelTime) ?? []
    }
}

struct ObuMockChargingInfoModel: Codable {
    let businessFunction: String    //  ERP, EEP, EPS
    let content1: String?
    let content2: String?
    let content3: String?
    let content4: String?
    let chargingAmount: Int
    let startTime: String?
    let endTime: String?
    let chargingType: String
    let chargingMessageType: String
    let cardStatus: String
    let roadName: String
    
    func getChargingInfo() -> BreezeObuChargingInfo {
        return BreezeObuChargingInfo(businessFunction: businessFunction, cardStatus: cardStatus, content1: content1, content2: content2, content3: content3, content4: content4, chargingAmount: chargingAmount, minChargeAmount: nil, startTime: startTime, endTime: endTime, chargingType: chargingType, chargingMessageType: chargingMessageType, roadName: roadName)
    }
    
    enum CodingKeys: String, CodingKey {
        case businessFunction
        case content1
        case content2
        case content3
        case content4
        case chargingAmount
        case startTime
        case endTime
        case chargingType
        case chargingMessageType
        case cardStatus
        case roadName
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.businessFunction = try container.decodeIfPresent(String.self, forKey: .businessFunction) ?? ""
        self.content1 = try container.decodeIfPresent(String.self, forKey: .content1)
        self.content2 = try container.decodeIfPresent(String.self, forKey: .content2)
        self.content3 = try container.decodeIfPresent(String.self, forKey: .content3)
        self.content4 = try container.decodeIfPresent(String.self, forKey: .content4)
        self.chargingAmount = try container.decode(Int.self, forKey: .chargingAmount)
        self.startTime = try container.decodeIfPresent(String.self, forKey: .startTime)
        self.endTime = try container.decodeIfPresent(String.self, forKey: .endTime)
        self.chargingType = try container.decodeIfPresent(String.self, forKey: .chargingType) ?? ""
        self.chargingMessageType = try container.decodeIfPresent(String.self, forKey: .chargingMessageType) ?? ""
        self.cardStatus = try container.decodeIfPresent(String.self, forKey: .cardStatus) ?? ""
        self.roadName = try container.decodeIfPresent(String.self, forKey: .roadName) ?? ""
    }
}

struct ObuMockTravelSummaryModel: Codable {
    let totalTravelTime: Double?
    let totalTravelDistance: Double?
    let totalTravelCharge: TravelCharge?
    
    func getTravelSummary() -> BreezeTravelSummary {
        return BreezeTravelSummary(totalTravelTime: totalTravelTime, totalTravelDistance: totalTravelDistance, totalTravelCharge: totalTravelCharge)
    }
    
    enum CodingKeys: String, CodingKey {
        case totalTravelTime
        case totalTravelDistance
        case totalTravelCharge
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.totalTravelTime = try container.decodeIfPresent(Double.self, forKey: .totalTravelTime)
        self.totalTravelDistance = try container.decodeIfPresent(Double.self, forKey: .totalTravelDistance)
        self.totalTravelCharge = try container.decodeIfPresent(TravelCharge.self, forKey: .totalTravelCharge)
    }
}

struct ObuMockPaymentModel: Codable {
    let sequentialNumber: Int?
    let paymentDate: String?
    let businessFunction: String?
    let chargeAmount: Double?
    let paymentMode: String?
    
    func getPaymentHistory() -> BreezePaymentHistory {
        return BreezePaymentHistory(sequentialNumber: sequentialNumber, paymentDate: paymentDate, businessFunction: businessFunction, chargeAmount: chargeAmount, paymentMode: paymentMode)
    }
    
    enum CodingKeys: String, CodingKey {
        case sequentialNumber
        case paymentDate
        case businessFunction
        case chargeAmount
        case paymentMode
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.sequentialNumber = try container.decodeIfPresent(Int.self, forKey: .sequentialNumber)
        self.paymentDate = try container.decodeIfPresent(String.self, forKey: .paymentDate)
        self.businessFunction = try container.decodeIfPresent(String.self, forKey: .businessFunction)
        self.chargeAmount = try container.decodeIfPresent(Double.self, forKey: .chargeAmount)
        self.paymentMode = try container.decodeIfPresent(String.self, forKey: .paymentMode)
    }
}

public struct ObuMockEventModel: Codable {
    let eventType: String
    let message: String?
    let distance: String?
    let detailTitle: String?
    let detailMessage: String?
    let chargeAmount: String?
    let balance: Double?
    let spokenText: String?
    var carparks: [ObuMockCarparkModel]
    var travelTime: [ObuMockTravelModel]
    var chargingInfo: [ObuMockChargingInfoModel]
    let tripSummary: ObuMockTravelSummaryModel?
    var payments: [ObuMockPaymentModel]
    
    func getParkingSpokenText() -> String {
        var spokenText = ""
        if isAllAvailable() {
            if appDelegate().carPlayConnected {
                spokenText = "Parking Update - Nearby Carparks are all still available."
            } else {
                spokenText = "Parking Update - Nearby Carparks are all still available. You can select one to drive there now."
            }
        } else if isAllFull() {
            spokenText = "Parking Update - Nearby Carparks are all full. Expect to wait or park further away.”"
        } else if isFillingUpFast() {
            //  Read out names of carparks with yellow status
            spokenText = "Parking Update - Carparks nearby are filling up fast. Limited lots available at \(getAllNameType("yellow"))."
        } else {
            //  Read out names of carparks with green status
            if appDelegate().carPlayConnected {
                spokenText = "Parking Update - Nearby Carparks are still available."
            } else {
                spokenText = "Parking Update - Lots are still available at \(getAllNameType("green"))."
            }
        }
        return spokenText
        
    }
    
    func getInfoToDisplayOnCarplay() -> ParkingInfoItem {
        if isAllAvailable() {
            return ParkingInfoItem(title: "Parking \nUpdate", subTitle: "Nearby all available", backgroundColor: "#15B765", iconName: "carParkGreenIcon")
        } else if isAllFull() {
            return ParkingInfoItem(title: "Parking \nUpdate", subTitle: "Nearby All Full", backgroundColor: "#E82370", iconName: "carparkRedIcon")
        } else if isFillingUpFast() {
            return ParkingInfoItem(title: "Parking \nUpdate", subTitle: "Filling Up Fast", backgroundColor: "#F26415", iconName: "carParkOrangeIcon")
        } else {
            return ParkingInfoItem(title: "Parking \nUpdate", subTitle: "Nearby still available", backgroundColor: "#15B765", iconName: "carParkGreenIcon")
        }
    }
    
    private func isAllAvailable() -> Bool {
        var retValue: Bool = true
        let parkingList = getParkingList()
        for item in parkingList {
            if let lots = Int(item.lots), lots <= 0 {
                retValue = false
                break
            }
        }
        return retValue
    }
    
    private func isAllFull() -> Bool {
        var retValue: Bool = true
        let parkingList = getParkingList()
        for item in parkingList {
            if let lots = Int(item.lots), lots > 0 {
                retValue = false
                break
            }
        }
        return retValue
    }
    
    private func isFillingUpFast() -> Bool {
        var retValue: Bool = false
        var counter: Int = 0
        let parkingList = getParkingList()
        for item in parkingList {
            if (item.color.lowercased() == "red" || item.color.lowercased() == "yellow") {
                counter += 1
            }
        }
        
        if (counter >= 3 || counter == parkingList.count) {
            retValue = true
        }else {
            retValue = false
        }
        
        return retValue
    }
    
    private func getAllNameType(_ color: String) -> String {
        var names = ""
        let parkingList = getParkingList()
        for item in parkingList {
            if item.color.lowercased() == color {
                if names.isEmpty {
                    names.append(item.location)
                }else {
                    names.append(", \(item.location)")
                }
            }
        }
        return names
    }
    
    func getTravelTimeSpokenText() -> String {
        var spokenText = ""
        let travelTimeList = getTravelTimeList()
        
        //  Updated on BREEZE2-55
        let heavyTraffics = travelTimeList.filter { model in
            return model.color.lowercased() == "red"
        }
        
        if !heavyTraffics.isEmpty {
            spokenText.append("Heavy traffic ahead. Expect delays.")
            for i in 0..<heavyTraffics.count {
                let traffic = heavyTraffics[i]
                
                //  Do convert min to minutes
                let estTime = traffic.min.replacingOccurrences(of: "min", with: "").replacingOccurrences(of: "mins", with: "").replacingOccurrences(of: "minute", with: "").replacingOccurrences(of: "minutes", with: "")
                spokenText.append("\(estTime) minutes to \(traffic.location)")
                if i == heavyTraffics.count - 1 {
                    spokenText.append(".")
                } else {
                    spokenText.append(", ")
                }
            }
        }
        
        print("getTravelTimeSpokenText: \(spokenText)")
        
        return spokenText
    }
    
    func getParkingList() -> [Parking] {
        var retValue: [Parking] = []
        for carpark in carparks {
            retValue.append(carpark.getParking())
        }
        return retValue
    }
    
    func getTravelTimeList() -> [TravelTime] {
        var retValue: [TravelTime] = []
        for time in travelTime {
            retValue.append(time.getTravelTime())
        }
        return retValue
    }
    
    func getChargingInfor() -> [BreezeObuChargingInfo] {
        var retValue: [BreezeObuChargingInfo] = []
        for charging in chargingInfo {
            retValue.append(charging.getChargingInfo())
        }
        return retValue
    }
    
    
    func getIconEventOBU(_ isCarplay: Bool = false) -> String {
        var iconText = ""
        if let eventType = ObuEventType(rawValue: eventType) {
            switch eventType {
            case .FLASH_FLOOD:
                iconText = isCarplay ? "carplay-Flash Flood-Icon" : "Flash Flood-Icon"
            case .UNATTENDED_VEHICLE:
                iconText = isCarplay ? "carplay-Unattended Vehicle-Icon" : "Unattended Vehicle-Icon"
            case .MISCELLANEOUS:
                iconText = isCarplay ? "carplay-Miscellaneous-Icon" : "Miscellaneous-Icon"
            case .ROAD_BLOCK:
                iconText =  isCarplay ? "carplay-Road Block-Icon" : "Road Block-Icon"
            case .OBSTACLE:
                iconText = isCarplay ? "carplay-Obstacle-Icon" : "Obstacle-Icon"
            case .ROAD_WORK:
                iconText = isCarplay ? "carplay-Road Work-Icon" : "Road Work-Icon"
            case .VEHICLE_BREAKDOWN:
                iconText = isCarplay ? "carplay-Vehicle breakdown-Icon" : "Vehicle breakdown-Icon"
            case .MAJOR_ACCIDENT:
                iconText =  isCarplay ? "carplay-Major Accident-Icon" : "Major Accident-Icon"
            case .SEASON_PARKING:
                iconText = "seasonParkIcon"
            case .YELLOW_DENGUE_ZONE:
                iconText = "dengueIcon"
            case .RED_DENGUE_ZONE:
                iconText = "dengueIcon"
            case .TREE_PRUNING:
                iconText = isCarplay ? "carplay-Tree Pruning-Icon" : "Tree Pruning-Icon"
            case .HEAVY_TRAFFIC:
                iconText = isCarplay ? "carplay-Heavy Traffic-Icon" : "Heavy Traffic-Icon"
            default:
                break
            }
        }
        return iconText
    }
    
    
    /*
    func getNotiDic() -> [String: Any] {
        var notiDic: [String: Any] = [:]
        if let json = data, !json.isEmpty {
            do {
                let jsonData = try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                let dataModel = try JSONDecoder().decode(ObuMockDataModel.self, from: jsonData)
                let dataDic = dataModel.getDic()
                
                if let type = ObuEventType(rawValue: eventType) {
                    switch type {
                    case .PARKING_AVAILABILITY, .TRAVEL_TIME:
                        return dataDic
                    default:
                        notiDic["type"] = type.rawValue
                        for (key, value) in dataDic {
                            notiDic[key] = value
                        }
                    }
                }
            } catch let error {
                print(error)
            }
        }
        return notiDic
    }
    */
    
    func getNotiDic() -> [String: Any] {
        var notiDic: [String: Any] = [:]
        var retDic: [String: Any] = [:]
        
        if let mes = message, !mes.isEmpty {
            retDic["message"] = mes
        }
        if let dis = distance, !dis.isEmpty {
            retDic["distance"] = "\(dis)"
        }
        if let title = detailTitle, !title.isEmpty {
            retDic["detailTitle"] = title
        }
        if let detailMes = detailMessage, !detailMes.isEmpty {
            retDic["detailMessage"] = detailMes
        }
        if let amount = chargeAmount, !amount.isEmpty {
            retDic["chargeAmount"] = amount
        }
        if !carparks.isEmpty {
            var items: [Any] = []
            for item in carparks {
                let carparkDic = ["name": item.name, "availableLots": item.lots, "color": item.color]
                items.append(carparkDic)
            }
            retDic["carparks"] = items
        }
        
        if !travelTime.isEmpty {
            var params: [Any] = []
            for item in travelTime {
                let travelDic: [String: Any] = ["name": item.name ?? "", "estTime": item.estTime ?? "", "color": item.color ?? "Default"]
                params.append(travelDic)
            }
            retDic["data"] = params
        }
        
        if let type = ObuEventType(rawValue: eventType) {
            switch type {
            case .PARKING_AVAILABILITY, .TRAVEL_TIME, .LOW_CARD:
                //  Return data for carpark and travel time
                return retDic
            default:
                notiDic["type"] = type.rawValue
                for (key, value) in retDic {
                    notiDic[key] = value
                }
            }
        }
                
        //  Return data for other noti
        return notiDic
    }

    enum CodingKeys: String, CodingKey {
        case eventType
        case message
        case distance
        case detailTitle
        case detailMessage
        case chargeAmount
        case balance
        case spokenText
        case carparks
        case travelTime
        case chargingInfo
        case tripSummary
        case payments
    }

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.eventType = try container.decode(String.self, forKey: .eventType)
        self.message = try container.decodeIfPresent(String.self, forKey: .message)
        self.distance = try container.decodeIfPresent(String.self, forKey: .distance)
        self.detailTitle = try container.decodeIfPresent(String.self, forKey: .detailTitle)
        self.detailMessage = try container.decodeIfPresent(String.self, forKey: .detailMessage)
        self.chargeAmount = try container.decodeIfPresent(String.self, forKey: .chargeAmount)
        self.balance = try container.decodeIfPresent(Double.self, forKey: .balance)
        self.spokenText = try container.decodeIfPresent(String.self, forKey: .spokenText)
        self.carparks = try container.decodeIfPresent([ObuMockCarparkModel].self, forKey: .carparks) ?? []
        self.travelTime = try container.decodeIfPresent([ObuMockTravelModel].self, forKey: .travelTime) ?? []
        self.chargingInfo = try container.decodeIfPresent([ObuMockChargingInfoModel].self, forKey: .chargingInfo) ?? []
        self.tripSummary = try container.decodeIfPresent(ObuMockTravelSummaryModel.self, forKey: .tripSummary)
        self.payments = try container.decodeIfPresent([ObuMockPaymentModel].self, forKey: .payments) ?? []
    }

}

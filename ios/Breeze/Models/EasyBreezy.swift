//
//  EasyBreezy.swift
//  Breeze
//
//  Created by VishnuKanth on 07/01/21.
//

import Foundation

struct EasyBreezyAddresses : BaseAddress, Codable {
    let addressid:Int?
    let lat : String?
    let long : String?
    let alias : String?
    let address1 : String?
    let address2 : String?
    let carparkId : String?
    let fullAddress: String?
    let isCrowsourceParking: Bool?
    let placeId: String?
    
    func isCarpark() -> Bool {
        if let ID = carparkId, !ID.isEmpty {
            return !ID.isEmpty
        }
        return false
    }

    enum CodingKeys: String, CodingKey {

        case addressid = "addressid"
        case lat = "lat"
        case long = "long"
        case alias = "name"
        case address1 = "address1"
        case address2 = "address2"
        case carparkId = "carparkId"
        case fullAddress = "fullAddress"
        case isCrowsourceParking = "isCrowsourceParking"
        case placeId = "placeId"
    }
    
    init(addressid: Int,lat:String,long:String,alias:String,address1:String,address2:String,carparkId:String? = nil, fullAddress:String? = nil, isCrowsourceParking: Bool? = false, placeId: String? = nil){
        
        self.addressid = addressid
        self.alias = alias
        self.address1 = address1
        self.lat = lat
        self.long = long
        self.address2 = address2
        self.carparkId = carparkId
        self.fullAddress = fullAddress
        self.isCrowsourceParking = isCrowsourceParking
        self.placeId = placeId
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        addressid = try values.decodeIfPresent(Int.self, forKey: .addressid)
        lat = try values.decodeIfPresent(String.self, forKey: .lat)
        long = try values.decodeIfPresent(String.self, forKey: .long)
        alias = try values.decodeIfPresent(String.self, forKey: .alias)
        address1 = try values.decodeIfPresent(String.self, forKey: .address1)
        address2 = try values.decodeIfPresent(String.self, forKey: .address2)
        carparkId = try values.decodeIfPresent(String.self, forKey: .carparkId)
        fullAddress = try values.decodeIfPresent(String.self, forKey: .fullAddress)
        isCrowsourceParking = try values.decodeIfPresent(Bool.self, forKey: .isCrowsourceParking)
        placeId = try values.decodeIfPresent(String.self, forKey: .placeId)
    }

}

struct EasyBreezyBaseAddress : Codable {
    let addresses : [EasyBreezyAddresses]?

    enum CodingKeys: String, CodingKey {
        case addresses = "address"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        addresses = try values.decodeIfPresent([EasyBreezyAddresses].self, forKey: .addresses)
    }

}

//
//  NewSearchAddressResult.swift
//  Breeze
//
//  Created by Malou Mendoza on 14/7/21.
//

import Foundation


public struct NewSearchAddressResultModel: Codable {
    let totalRecords: Int
    let source: String
    let token: String
   
    var addresses: [AddressBaseModel]
    
    enum CodingKeys: String, CodingKey {

        case totalRecords = "totalRecords"
        case source = "source"
        case token = "token"
        case addresses = "addresses"
       
    }
    
    init( totalRecords: Int, source: String,  token: String, addresses: [AddressBaseModel]) {
        self.totalRecords = totalRecords
        self.source = source
        self.token = token
        self.addresses = addresses
    }

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.totalRecords = try container.decodeIfPresent(Int.self, forKey: .totalRecords) ?? 0
        self.source = try container.decodeIfPresent(String.self, forKey: .source) ?? ""
        self.token = try container.decodeIfPresent(String.self, forKey: .token) ?? ""
        self.addresses = try container.decodeIfPresent([AddressBaseModel].self, forKey: .addresses) ?? []
    }
}

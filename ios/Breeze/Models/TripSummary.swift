//
//  TripSummary.swift
//  Breeze
//
//  Created by Zhou Hao on 11/6/21.
//

import Foundation

struct WalkathonTripSummaryResponse: Codable {
    let walking_trip_id: Int
    let message: String
}

struct TripSummaryResponse: Codable {
    let tripId: Int
    let message: String
    
    enum CodingKeys: String, CodingKey {
        case tripId
        case message
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.tripId = try container.decodeIfPresent(Int.self, forKey: .tripId) ?? 0
        self.message = try container.decodeIfPresent(String.self, forKey: .message) ?? ""
    }
}

enum BreezeNavigationType: String {
    case NAVIGATION
    case CRUISE
    case OBULITE
}

public struct WalkathonTripSummary: Codable {
    enum NavigationEndType {
        static let manually = "MANUAL"
        static let natural = "NATURAL"
    }
    
    private var tripGUID: String!
    private var type = "NAVIGATION" //  Hard code for now later on can be mutiple type in different screens
    private var tripStartTime: Int = 0
    private var tripStartAddress1 = ""
    private var tripStartAddress2 = ""
    private var plannedDestAddress1 = ""
    private var plannedDestAddress2 = ""
    private var tripStartLat: Double = 0
    private var tripStartLong: Double = 0
    private var plannedDestLat: Double = 0
    private var plannedDestLong: Double = 0
    private var tripEndTime: Int
    private var tripDestAddress1 = ""
    private var tripDestAddress2 = ""
    private var tripDestLat: Double = 0
    private var tripDestLong: Double = 0
    private var totalDistance: Double = 0
    private var isDestinationCarpark: Bool?
    private var navigationEndType: String!  //NATURAL or MANUAL
    private var carParkId: String?
    private var amenityId: String?
    private var layerCode: String?
    private var amenityType: String?
    
    
    enum CodingKeys: String, CodingKey {
        case tripGUID
        case type
        case tripStartTime
        case tripStartAddress1
        case tripStartAddress2
        case plannedDestAddress1
        case plannedDestAddress2
        case tripStartLat
        case tripStartLong
        case plannedDestLat
        case plannedDestLong
        case tripEndTime
        case tripDestAddress1
        case tripDestAddress2
        case tripDestLat
        case tripDestLong
        case totalDistance
        case isDestinationCarpark
        case navigationEndType
        case carParkId
        case amenityId
        case layerCode
        case amenityType
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(self.tripGUID, forKey: .tripGUID)
        try container.encode(self.type, forKey: .type)
        try container.encode(self.tripStartTime, forKey: .tripStartTime)
        try container.encode(self.tripStartAddress1, forKey: .tripStartAddress1)
        try container.encode(self.tripStartAddress2, forKey: .tripStartAddress2)
        try container.encode(self.plannedDestAddress1, forKey: .plannedDestAddress1)
        try container.encode(self.plannedDestAddress2, forKey: .plannedDestAddress2)
        try container.encode(self.tripStartLat, forKey: .tripStartLat)
        try container.encode(self.tripStartLong, forKey: .tripStartLong)
        try container.encode(self.plannedDestLat, forKey: .plannedDestLat)
        try container.encode(self.plannedDestLong, forKey: .plannedDestLong)
        try container.encode(self.tripEndTime, forKey: .tripEndTime)
        try container.encode(self.tripDestAddress1, forKey: .tripDestAddress1)
        try container.encode(self.tripDestAddress2, forKey: .tripDestAddress2)
        try container.encode(self.tripDestLat, forKey: .tripDestLat)
        try container.encode(self.tripDestLong, forKey: .tripDestLong)
        try container.encode(self.totalDistance, forKey: .totalDistance)
        try container.encodeIfPresent(self.isDestinationCarpark, forKey: .isDestinationCarpark)
        try container.encodeIfPresent(self.navigationEndType, forKey: .navigationEndType)
        try container.encodeIfPresent(self.carParkId, forKey: .carParkId)
        try container.encodeIfPresent(self.amenityId, forKey: .amenityId)
        try container.encodeIfPresent(self.layerCode, forKey: .layerCode)
        try container.encodeIfPresent(self.amenityType, forKey: .amenityType)
    }
    
    
    init(tripGUID: String, type: String, tripStartTime: Int,  tripStartAddress1: String, tripStartAddress2: String, plannedDestAddress1: String,
         plannedDestAddress2: String, tripStartLat: Double, tripStartLong: Double, tripEndTime: Int, tripDestAddress1: String, tripDestAddress2: String, tripDestLat: Double, tripDestLong: Double, plannedDestLat: Double, plannedDestLong: Double, totalDistance: Double, isDestinationCarpark: Bool? = false, navigationEndType: String, carParkId: String? = nil, amenityId: String? = nil, layerCode: String? = nil, amenityType: String? = nil) {
        self.tripGUID = tripGUID
        self.type = type
        self.tripStartTime = tripStartTime
        self.tripStartAddress1 = tripStartAddress1
        self.tripStartAddress2 = tripStartAddress2
        self.plannedDestAddress1 = plannedDestAddress1
        self.plannedDestAddress2 = plannedDestAddress2
        self.tripStartLat = tripStartLat
        self.tripStartLong = tripStartLong
        self.plannedDestLat = plannedDestLat
        self.plannedDestLong = plannedDestLong
        self.tripEndTime = tripEndTime
        self.tripDestAddress1 = tripDestAddress1
        self.tripDestAddress2 = tripDestAddress2
        self.tripDestLat = tripDestLat
        self.tripDestLong = tripDestLong
        self.totalDistance = totalDistance
        self.isDestinationCarpark = isDestinationCarpark
        self.navigationEndType = navigationEndType
        self.carParkId = carParkId
        self.amenityId = amenityId
        self.layerCode = layerCode
        self.amenityType = amenityType
    }

}

public struct TripSummary: Codable {
    
    enum NavigationEndType {
        static let manually = "MANUAL"
        static let natural = "NATURAL"
    }
    
    let id: String // trip id, generated by UUID
    let type: String? //  Hard code for now later on can be mutiple type in different screens
    let startTime: Int
    let endTime: Int
   // let tripType: String
    let startLatitude: Double
    let startLongitude: Double
    let startAddress1: String
    let startAddress2: String
    let plannedDestAddress1: String?
    let plannedDestAddress2: String?
    let endLatitude: Double
    let endLongitude: Double
    let plannedDestLat: Double?
    let plannedDestLong: Double?
    let endAddress1: String
    let endAddress2: String
    let totalCost: Double
    let totalDistance: Double
    let parkingExpense: Double
//    let millegeExpense: Double
    let attachementFilename: String?
    let navigationEndType: String?
    let amenityId: String?
    let layerCode: String?
    let amenityType: String?
    
    var costBreakDowns: [ERPCost]
    var isParkingEditable: Bool?
    var remark: String?
    
    //  Additional information for OBU
    let idleTime: Double?
    let vehicleNumber: String?
    let bookmarkId: String?
    let bookmarkName: String?
    
    var obuStatus: String?
    
    enum CodingKeys: String, CodingKey {

        case id = "tripGUID"
        case type = "type"
        case startTime = "tripStartTime"
        case endTime = "tripEndTime"
        case startAddress1 = "tripStartAddress1"
        case startAddress2 = "tripStartAddress2"
        case plannedDestAddress1 = "plannedDestAddress1"
        case plannedDestAddress2 = "plannedDestAddress2"
        //case tripType
        case startLatitude = "tripStartLat"
        case startLongitude = "tripStartLong"
        case endLatitude = "tripDestLat"
        case endLongitude = "tripDestLong"
        case plannedDestLat = "plannedDestLat"
        case plannedDestLong = "plannedDestLong"
        case endAddress1 = "tripDestAddress1"
        case endAddress2 = "tripDestAddress2"
        case totalCost = "totalExpense"
        case totalDistance = "totalDistance"
        case parkingExpense = "parkingExpense"
//        case millegeExpense
        case attachementFilename
        case costBreakDowns = "erp"
        case isParkingEditable = "isParkingEditable"
        case navigationEndType = "navigationEndType"
        case amenityId = "amenityId"
        case layerCode = "layerCode"
        case amenityType = "amenityType"
        case idleTime = "idleTime"
        case vehicleNumber = "vehicleNumber"
        case bookmarkId = "bookmarkId"
        case bookmarkName = "bookmarkName"
        case obuStatus = "obuStatus"
        case remark = "remark"
    }
    
    init(id: String, type: String? = BreezeNavigationType.NAVIGATION.rawValue, startTime: Int, endTime: Int,  startLatitude: Double, startLongitude: Double, startAddress1: String, startAddress2: String, plannedDestAddress1: String?, plannedDestAddress2: String?, endLatitude: Double, endLongitude: Double, plannedDestLat: Double?, plannedDestLong: Double?, endAddress1: String, endAddress2: String, totalCost: Double, totalDistance: Double, parkingExpense: Double, costBreakDowns: [ERPCost], isParkingEditable: Bool? = nil, navigationEndType: String, attachmentFilename: String? = nil, amenityId: String? = nil, layerCode: String? = nil, amenityType: String? = nil, idleTime: Double? = nil, vehicleNumber: String? = nil, bookmarkId: String? = nil, bookmarkName: String? = nil, obuStatus: String? = nil, remark: String? = nil) {
        self.id = id
        self.type = type
        self.startTime = startTime
        self.endTime = endTime
        self.startLatitude = startLatitude
        self.startLongitude = startLongitude
        self.startAddress1 = startAddress1
        self.startAddress2 = startAddress2
        self.plannedDestAddress1 = plannedDestAddress1
        self.plannedDestAddress2 = plannedDestAddress2
        self.endLatitude = endLatitude
        self.endLongitude = endLongitude
        self.plannedDestLat = plannedDestLat
        self.plannedDestLong = plannedDestLong
        self.endAddress1 = endAddress1
        self.endAddress2 = endAddress2
        self.totalCost = totalCost
        self.totalDistance = totalDistance
        self.parkingExpense = parkingExpense
        self.costBreakDowns = costBreakDowns
        self.isParkingEditable = isParkingEditable
        self.attachementFilename = attachmentFilename
        self.navigationEndType = navigationEndType
        self.amenityId = amenityId
        self.amenityType = amenityType
        self.layerCode = layerCode
        
        self.idleTime = idleTime
        self.vehicleNumber = vehicleNumber
        self.bookmarkId = bookmarkId
        self.bookmarkName = bookmarkName
        self.obuStatus = obuStatus
        self.remark = remark
    }

}


public struct TripUpdate: Codable {
    
    let totalExpense: Double
    let parkingExpense: Double
    var erp: [ERPUpdate]
    
    enum CodingKeys: String, CodingKey {
        case totalExpense = "totalExpense"
        case parkingExpense = "parkingExpense"
        case erp = "erp"
    }
}

public struct ERPUpdate: Codable {
    
    let tripErpId: Int
    let erpId: Int
    let actualAmount : Double
    
    enum CodingKeys: String, CodingKey {
        case tripErpId = "tripErpId"
        case erpId = "erpId"
        case actualAmount = "actualAmount"
    }
}

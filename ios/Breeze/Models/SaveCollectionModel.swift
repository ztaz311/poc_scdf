//
//  SaveCollectionModel.swift
//  Breeze
//
//  Created by Tuan, Pham Hai on 17/11/2022.
//

import Foundation

struct SaveCollectionDataModel: Codable {
    let success: Bool?
    let data: SaveCollectionModel?
    let error_code, message: String?
    
    enum CodingKeys: String, CodingKey {
        case success, data, error_code, message
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.success = try container.decodeIfPresent(Bool.self, forKey: .success)
        self.data = try container.decodeIfPresent(SaveCollectionModel.self, forKey: .data)
        self.error_code = try container.decodeIfPresent(String.self, forKey: .error_code)
        self.message = try container.decodeIfPresent(String.self, forKey: .message)
    }
}

struct SaveCollectionModel: Codable {
    
    let collectionSnapshotId: Int?
    let collectionId: Int?
    let code, name, description: String?
    let pinned: Bool?
    let error_code, message: String?    //  LIMIT_REACHED and COLLECTION_NOT_FOUND
    let bookmarks: [Bookmark]?
    
    enum CodingKeys: String, CodingKey {
        case collectionSnapshotId, collectionId, code, name, description, pinned, error_code, message, bookmarks
    }
    
    init(from decoder: Decoder) throws {
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.collectionSnapshotId = try container.decodeIfPresent(Int.self, forKey: .collectionSnapshotId)
        self.collectionId = try container.decodeIfPresent(Int.self, forKey: .collectionId)
        self.code = try container.decodeIfPresent(String.self, forKey: .code)
        self.name = try container.decodeIfPresent(String.self, forKey: .name)
        self.description = try container.decodeIfPresent(String.self, forKey: .description)
        self.pinned = try container.decodeIfPresent(Bool.self, forKey: .pinned)
        self.error_code = try container.decodeIfPresent(String.self, forKey: .error_code)
        self.message = try container.decodeIfPresent(String.self, forKey: .message)
        self.bookmarks = try container.decodeIfPresent([Bookmark].self, forKey: .bookmarks)
    }
}

struct SaveLocationDataModel: Codable {
    let success: Bool?
    let data: SaveLocationModel?
    let error_code, message: String?
    
    enum CodingKeys: String, CodingKey {
        case success, data, error_code, message
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.success = try container.decodeIfPresent(Bool.self, forKey: .success)
        self.data = try container.decodeIfPresent(SaveLocationModel.self, forKey: .data)
        self.error_code = try container.decodeIfPresent(String.self, forKey: .error_code)
        self.message = try container.decodeIfPresent(String.self, forKey: .message)
    }
}

struct SaveLocationModel: Codable {
        
    let userLocationLinkId: Int?
    let placeId: String?
    let userLocationLinkIdRef: Int?
    let lat, long: String?
    let error_code, message: String? //  LIMIT_REACHED and LOCATION_NOT_FOUND ???
    let amenityType, amenityId: String?
    let isDestination: Bool?
    let name, address1, address2, code, description: String?
    let availablePercentImageBand: String?  //  TYPE0 / TYPE1 / TYPE2
    let bookmarkId: Int?
    let saved: Bool?
    let fullAddress: String?
    let shareType: String?
    let availabilityCSData: AvailabilityCSData?
    let showAvailabilityFB: Bool?
    let hasAvailabilityCS: Bool?
    
    func getType() -> String {
        
        var type = ""
        
        if let amenityType = amenityType, !amenityType.isEmpty {
            switch amenityType {
            case Values.AMENITYCARPARK:
                if saved ?? false {
                    if let available = availablePercentImageBand {
                        switch available {
                        case "TYPE0":
                            type = Values.SHARE_CARPARK_FULL_BOOKMARKED
                        case "TYPE1":
                            type = Values.SHARE_CARPARK_FILLING_UP_BOOKMARKED
                        case "TYPE2":
                            type = Values.SHARE_CARPARK_AVAILABLE_BOOKMARKED
                        default:
                            break
                        }
                    }
                } else {
                    if let available = availablePercentImageBand {
                        switch available {
                        case "TYPE0":
                            type = Values.SHARE_CARPARK_FULL
                        case "TYPE1":
                            type = Values.SHARE_CARPARK_FILLING_UP
                        case "TYPE2":
                            type = Values.SHARE_CARPARK_AVAILABLE
                        default:
                            break
                        }
                    }
                }
                
            case Values.PETROL:
                if saved ?? false {
                    type = Values.SHARE_PETROL_BOOKMARKED
                } else {
                    type = Values.SHARE_PETROL_ICON
                }
            case Values.EV_CHARGER:
                if saved ?? false {
                    type = Values.SHARE_EV_BOOKMARKED
                } else {
                    type = Values.SHARE_EV_ICON
                }
            default:
                break
            }
        }
        
        if type.isEmpty {
            if let code = code, !code.isEmpty {
                switch code {
                case BookmarkCode.custom.rawValue, BookmarkCode.normal.rawValue:
                    if saved ?? false {
                        type = Values.SHARE_BOOKMARK_ICON
                    } else {
                        type = Values.SHARE_DESTINATION_ICON
                    }
                case BookmarkCode.home.rawValue:
                    type = Values.SHARE_HOME_LOCATION
                case BookmarkCode.work.rawValue:
                    type = Values.SHARE_WORK_LOCATION
                default:
                    break
                }
            }
        }
        
        if type.isEmpty {
            if saved ?? false {
                type = Values.SHARE_BOOKMARK_ICON
            } else {
                type = Values.SHARE_DESTINATION_ICON
            }
        }
        
        return type
    }
    
    func getDic4RN() -> [String: Any] {
        let retValue: [String: Any] = ["address1": address1 as Any,
                                       "address2": address2 as Any,
                                       "lat": lat as Any,
                                       "long": long as Any,
                                       "code": code as Any,
                                       "isDestination": isDestination as Any,
                                       "amenityId": amenityId as Any,
                                       "amenityType": amenityType as Any,
                                       "description": description as Any,
                                       "name": name as Any,
                                       "userLocationLinkId": userLocationLinkId as Any,
                                       "bookmarkId": bookmarkId as Any,
                                       "saved": saved as Any,
                                       "fullAddress": fullAddress as Any,
                                       "shareType": shareType as Any,
                                       "placeId": placeId as Any,
                                       "userLocationLinkIdRef": userLocationLinkIdRef as Any
        ]
        return retValue
    }
    
    func getName() -> String {
        return name ?? ""
    }
    
    func getAddress() -> String {
        return address1 ?? ""
    }
    
    enum CodingKeys: String, CodingKey {
        case userLocationLinkId, userLocationLinkIdRef, lat, long, error_code, message, amenityType, amenityId, isDestination, name, address1, address2, code, description, availablePercentImageBand, bookmarkId, saved, fullAddress, shareType, placeId,showAvailabilityFB, hasAvailabilityCS, availabilityCSData
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.userLocationLinkId = try container.decodeIfPresent(Int.self, forKey: .userLocationLinkId)
        self.lat = try container.decodeIfPresent(String.self, forKey: .lat)
        self.long = try container.decodeIfPresent(String.self, forKey: .long)
        self.error_code = try container.decodeIfPresent(String.self, forKey: .error_code)
        self.message = try container.decodeIfPresent(String.self, forKey: .message)
        self.amenityType = try container.decodeIfPresent(String.self, forKey: .amenityType)
        self.amenityId = try container.decodeIfPresent(String.self, forKey: .amenityId)
        self.isDestination = try container.decodeIfPresent(Bool.self, forKey: .isDestination)
        self.name = try container.decodeIfPresent(String.self, forKey: .name)
        self.address1 = try container.decodeIfPresent(String.self, forKey: .address1)
        self.address2 = try container.decodeIfPresent(String.self, forKey: .address2)
        self.code = try container.decodeIfPresent(String.self, forKey: .code)
        self.description = try container.decodeIfPresent(String.self, forKey: .description)
        self.availablePercentImageBand = try container.decodeIfPresent(String.self, forKey: .availablePercentImageBand)
        self.bookmarkId = try container.decodeIfPresent(Int.self, forKey: .bookmarkId)
        self.saved = try container.decodeIfPresent(Bool.self, forKey: .saved)
        self.fullAddress = try container.decodeIfPresent(String.self, forKey: .fullAddress)
        self.shareType = try container.decodeIfPresent(String.self, forKey: .shareType)
        self.placeId = try container.decodeIfPresent(String.self, forKey: .placeId)
        self.userLocationLinkIdRef = try container.decodeIfPresent(Int.self, forKey: .userLocationLinkIdRef)
        self.showAvailabilityFB = try container.decodeIfPresent(Bool.self, forKey: .showAvailabilityFB)
        self.hasAvailabilityCS = try container.decodeIfPresent(Bool.self, forKey: .hasAvailabilityCS)
        self.availabilityCSData = try container.decodeIfPresent(AvailabilityCSData.self, forKey: .availabilityCSData)
    }
}

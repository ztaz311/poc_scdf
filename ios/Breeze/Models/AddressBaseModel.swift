//
//  AddressBaseModel.swift
//  Breeze
//
//  Created by Malou Mendoza on 13/7/21.
//


import Foundation

public struct AddressBaseModel: Codable {
    
    let lat : String?
    let long : String?
    let address1 : String?
    let address2 : String?
    let placeId : String?
    let distance : String?
    let token: String?
    let type:String?
    let source:String?
    
    enum CodingKeys: String, CodingKey {
     
        case lat = "lat"
        case long = "long"
        case address1 = "address1"
        case address2 = "address2"
        case distance = "distance"
        case placeId = "placeId"
        case token = "token"
        case type = "type"
        case source = "source"
    }
    
    init(lat: String, long: String, address1:String,address2:String, placeId: String,distance:String, token: String, type:String,source:String) {
        self.lat = lat
        self.long = long
        self.address1 = address1
        self.address2 = address2
        self.placeId = placeId
        self.distance = distance
        self.token = token
        self.type = type
        self.source = source
    }

    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        lat = try values.decodeIfPresent(String.self, forKey: .lat)
        long = try values.decodeIfPresent(String.self, forKey: .long)
        address1 = try values.decodeIfPresent(String.self, forKey: .address1)
        address2 = try values.decodeIfPresent(String.self, forKey: .address2)
        placeId = try values.decodeIfPresent(String.self, forKey: .placeId)
        distance = try values.decodeIfPresent(String.self, forKey: .distance)
        token = try values.decodeIfPresent(String.self, forKey: .token)
        source = try values.decodeIfPresent(String.self, forKey: .source)
        type = try values.decodeIfPresent(String.self, forKey: .type)
    }
}


public struct AddressDetailModel: Codable {
    
    let details : DetailedAddress?
   
    enum CodingKeys: String, CodingKey {
        case details = "details"
    }
    
    init(details: DetailedAddress) {
        self.details = details
    }

    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        details = try values.decodeIfPresent(DetailedAddress.self, forKey: .details)
    }
}


public struct DetailedAddress: Codable {
    
    let lat : Double?
    let long : Double?
    let address : String?
    let placeId : String?
    let token: String?

    enum CodingKeys: String, CodingKey {
     
        case lat = "lat"
        case long = "long"
        case address = "address"
        case placeId = "placeId"
        case token = "token"
    }
    
    init(lat: Double, long: Double, address:String, placeId: String, token: String) {
        self.lat = lat
        self.long = long
        self.address = address
        self.placeId = placeId
        self.token = token
    }

    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        lat = try values.decodeIfPresent(Double.self, forKey: .lat)
        long = try values.decodeIfPresent(Double.self, forKey: .long)
        address = try values.decodeIfPresent(String.self, forKey: .address)
        placeId = try values.decodeIfPresent(String.self, forKey: .placeId)
        token = try values.decodeIfPresent(String.self, forKey: .token)
    }
}


//public struct AddressBaseModel: Codable {
//   // public var addressArray: [String: [InnerAddress]]
//
//    public struct InnerAddress: Codable {
//        public let lat : String?
//        public let long : String?
//        public let address1 : String?
//        public let address2 : String?
//        public let distance : String?
//        public let placeId: String
//        public let token: String
//
//        enum CodingKeys: String, CodingKey {
//            case lat = "lat"
//            case long = "long"
//            case address1 = "address1"
//            case address2 = "address2"
//            case distance = "distance"
//            case placeId = "placeId"
//            case token = "token"
//        }
//    }
//
//    private struct CustomCodingKeys: CodingKey {
//        var stringValue: String
//        init?(stringValue: String) {
//            self.stringValue = stringValue
//        }
//        var intValue: Int?
//        init?(intValue: Int) {
//            return nil
//        }
//    }
//    public init(from decoder: Decoder) throws {
//        let container = try decoder.container(keyedBy: CustomCodingKeys.self)
//
//    //    self.addressArray = [String: [InnerAddress]]()
//        for key in container.allKeys {
//            let value = try container.decode([InnerAddress].self, forKey: CustomCodingKeys(stringValue: key.stringValue)!)
//            let innerArray = value.enumerated().map { (index, element) in
//                return InnerAddress( lat: element.lat, long: element.long, address1: element.address1, address2: element.address2, distance: element.distance, placeId: element.placeId, token: element.token)
//                }
//           // self.addressArray[key.stringValue] = innerArray
//        }
//    }
//}

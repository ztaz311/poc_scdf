//
//  SearchRecommendations.swift
//  Breeze
//
//  Created by VishnuKanth on 19/12/20.
//

import Foundation

struct Addresses : BaseAddress, Codable {
    let lat : String?
    let long : String?
    let alias : String?
    let addressid:Int?
    let address1 : String?
    let address2 : String?

    enum CodingKeys: String, CodingKey {

        case lat = "lat"
        case long = "long"
        case alias = "name"
        case address1 = "address1"
        case address2 = "address2"
        case addressid = "addressid"
    }

    init(addressId: Int?, alias: String,address1:String,lat:String,long:String,address2:String) {
        self.alias = alias
        self.address1 = address1
        self.lat = lat
        self.long = long
        self.address2 = address2
        self.addressid = addressId
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        lat = try values.decodeIfPresent(String.self, forKey: .lat)
        long = try values.decodeIfPresent(String.self, forKey: .long)
        alias = try values.decodeIfPresent(String.self, forKey: .alias)
        address1 = try values.decodeIfPresent(String.self, forKey: .address1)
        address2 = try values.decodeIfPresent(String.self, forKey: .address2)
        addressid = try values.decodeIfPresent(Int.self, forKey: .addressid)
    }

}

struct AddressRecommendation : Codable {
    let addresses : [Addresses]?

    enum CodingKeys: String, CodingKey {

        case addresses = "addresses"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        addresses = try values.decodeIfPresent([Addresses].self, forKey: .addresses)
    }

}



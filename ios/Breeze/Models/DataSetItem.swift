//
//  DataSetItem.swift
//  Breeze
//
//  Created by Zhou Hao on 28/5/21.
//

import Foundation

protocol DataSetItem {
    var id: String { get }
    var featureJson: String { get }
}


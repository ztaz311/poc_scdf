//
//  NotificationModel.swift
//  Breeze
//
//  Created by Malou Mendoza on 4/3/22.
//

import Foundation

public struct GlobalNotification: Codable {
   
    var notifications: [NotificationModel]
    var totalItems: Int
    var totalPages: Int
    var currentPage: Int
    
    enum CodingKeys: String, CodingKey {

        case notifications = "notifications"
        case totalItems = "totalItems"
        case totalPages = "totalPages"
        case currentPage = "currentPage"
        
    }
    
    init( notifications: [NotificationModel], totalItems: Int, totalPages: Int, currentPage:Int) {
        self.notifications = notifications
        self.totalItems = totalItems
        self.totalPages = totalPages
        self.currentPage = currentPage
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.notifications = try container.decodeIfPresent([NotificationModel].self, forKey: .notifications) ?? []
        self.totalItems = try container.decodeIfPresent(Int.self, forKey: .totalItems) ?? 0
        self.totalPages = try container.decodeIfPresent(Int.self, forKey: .totalPages) ?? 0
        self.currentPage = try container.decodeIfPresent(Int.self, forKey: .currentPage) ?? 0
    }
}

public struct ScreenProperties: Codable {
    let contentID: Int?
    let contentCategoryID: Int?
    
    enum CodingKeys: String, CodingKey {
        case contentID = "contentID"
        case contentCategoryID = "contentCategoryID"
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.contentID = try container.decodeIfPresent(Int.self, forKey: .contentID)
        self.contentCategoryID = try container.decodeIfPresent(Int.self, forKey: .contentCategoryID)
    }
}

public struct NotificationModel: Codable {
    
    let notificationId: Int
    let category: String
    let type : String
    let title: String
    let message: String
    let description: String
    let short_name: String
    let latitude: Double
    let longitude: Double
    let startTime: Int
    let expireTime: Int
    let imageLinkLight: String
    let imageLinkDark: String
    let priority: Int
    let showInappNotification: Bool
    let openScreen: String?
    let screenProperties: ScreenProperties?
    
    enum CodingKeys: String, CodingKey {
        case notificationId = "notificationId"
        case category = "category"
        case type = "type"
        case title = "title"
        case message = "message"
        case description = "description"
        case short_name = "short_name"
        case latitude = "latitude"
        case longitude = "longitude"
        case startTime = "startTime"
        case expireTime = "expireTime"
        case imageLinkLight = "imageLinkLight"
        case imageLinkDark = "imageLinkDark"
        case priority = "priority"
        case showInappNotification = "showInappNotification"
        case openScreen = "openScreen"
        case screenProperties = "screenProperties"
    }
    
//    init(notificationId: Int, category: String, type: String, title: String, message: String, description: String, short_name:String, latitude: Double, longitude: Double, startTime: Int, expireTime:Int, imageLinkLight: String, imageLinkDark: String, priority:Int) {
//
//        self.notificationId = notificationId
//        self.category = category
//        self.type = type
//        self.title = title
//        self.message = message
//        self.description = description
//        self.short_name = short_name
//        self.latitude = latitude
//        self.longitude = longitude
//        self.startTime = startTime
//        self.expireTime = expireTime
//        self.imageLinkLight = imageLinkLight
//        self.imageLinkDark = imageLinkDark
//        self.priority = priority
//    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.notificationId = try container.decodeIfPresent(Int.self, forKey: .notificationId) ?? 0
        self.category = try container.decodeIfPresent(String.self, forKey: .category) ?? ""
        self.type = try container.decodeIfPresent(String.self, forKey: .type) ?? ""
        self.title = try container.decodeIfPresent(String.self, forKey: .title) ?? ""
        self.message = try container.decodeIfPresent(String.self, forKey: .message) ?? ""
        self.description = try container.decodeIfPresent(String.self, forKey: .description) ?? ""
        self.short_name = try container.decodeIfPresent(String.self, forKey: .short_name) ?? ""
        self.latitude = try container.decodeIfPresent(Double.self, forKey: .latitude) ?? 0.0
        self.longitude = try container.decodeIfPresent(Double.self, forKey: .longitude)  ?? 0.0
        self.startTime = try container.decodeIfPresent(Int.self, forKey: .startTime) ?? 0
        self.expireTime = try container.decodeIfPresent(Int.self, forKey: .expireTime) ?? 0
        self.imageLinkLight = try container.decodeIfPresent(String.self, forKey: .imageLinkLight) ?? ""
        self.imageLinkDark = try container.decodeIfPresent(String.self, forKey: .imageLinkDark) ?? ""
        self.priority = try container.decodeIfPresent(Int.self, forKey: .priority) ?? 0
        self.showInappNotification = try container.decodeIfPresent(Bool.self, forKey: .showInappNotification) ?? true
        self.openScreen = try container.decodeIfPresent(String.self, forKey: .openScreen)
        self.screenProperties = try container.decodeIfPresent(ScreenProperties.self, forKey: .screenProperties)
    }

}


//
//  CarplaySearchModel.swift
//  Breeze
//
//  Created by Tuan, Pham Hai on 30/11/2023.
//

import Foundation

enum SearchType: Int {
    
    case history
    case home
    case work
    case petrol
    case evcharger
    
    func getTitle() -> String {
        var title = ""
        switch self {
        case .history:
            title = "Recent Searches"
        case .home:
            title = "Home"
        case .work:
            title = "Work"
        case .petrol:
            title = "Petrol"
        case .evcharger:
            title = "EV"
        }
        return title
    }
    
    func getImageName() -> String {
        var imageName = ""
        switch self {
        case .history:
            imageName = "search_history_icon"
        case .home:
            imageName = "search_home_icon"
        case .work:
            imageName = "search_work_icon"
        case .petrol:
            imageName = "search_petrol_icon"
        case .evcharger:
            imageName = "search_evcharger_icon"
        }
        return imageName
    }
}

class CarplaySearchModel {

    var type: SearchType = .history
    var name: String
    var address: String
    var lat: String
    var long: String
    var time: String
    
    func getTitle() -> String {
        return type.getTitle()
    }
    
    func getImageName() -> String {
        return type.getImageName()
    }

    init(type: SearchType, name: String = "", address: String = "", lat: String = "", long: String = "", time: String = "") {
        self.type = type
        self.name = name
        self.address = address
        self.lat = lat
        self.long = long
        self.time = time
    }
}

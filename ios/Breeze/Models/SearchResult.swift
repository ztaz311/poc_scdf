//
//  SearchResult.swift
//  Breeze
//
//  Created by VishnuKanth on 23/12/20.
//

import Foundation

public struct SearchAddresses : BaseAddress, Codable {
    let lat : String?
    let long : String?
    let alias : String?
    let address1 : String?
    let address2 : String?
    let distance : String?
    let carparkId : String?
    let fullAddress: String?
    //  For crowdsource
    let isCrowsourceParking: Bool?
    let placeId: String?
    let userLocationLinkIdRef: Int?
    let amenityId: String?
    let layerCode: String?
    
    func isCarpark() -> Bool {
        if let ID = carparkId, !ID.isEmpty {
            return !ID.isEmpty
        }
        return false
    }

    enum CodingKeys: String, CodingKey {
        case lat = "lat"
        case long = "long"
        case alias = "alias"
        case address1 = "address1"
        case address2 = "address2"
        case distance = "distance"
        case carparkId = "carparkId"
        case fullAddress = "fullAddress"
        case isCrowsourceParking = "isCrowsourceParking"
        case placeId
        case userLocationLinkIdRef
        case amenityId
        case layerCode
    }
    
    init(alias: String,address1:String,lat:String,long:String,address2:String,distance:String,carparkId:String? = nil, fullAddress: String? = nil, isCrowsourceParking: Bool? = false, placeId: String? = nil, userLocationLinkIdRef: Int? = nil, amenityId: String? = nil, layerCode: String? = nil) {
        self.alias = alias
        self.address1 = address1
        self.lat = lat
        self.long = long
        self.address2 = address2
        self.distance = distance
        self.carparkId = carparkId
        self.fullAddress = fullAddress
        self.isCrowsourceParking = isCrowsourceParking
        self.placeId = placeId
        self.userLocationLinkIdRef = userLocationLinkIdRef
        self.amenityId = amenityId
        self.layerCode = layerCode
    }

    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        lat = try values.decodeIfPresent(String.self, forKey: .lat)
        long = try values.decodeIfPresent(String.self, forKey: .long)
        alias = try values.decodeIfPresent(String.self, forKey: .alias)
        address1 = try values.decodeIfPresent(String.self, forKey: .address1)
        address2 = try values.decodeIfPresent(String.self, forKey: .address2)
        distance = try values.decodeIfPresent(String.self, forKey: .distance)
        carparkId = try values.decodeIfPresent(String.self, forKey: .carparkId)
        fullAddress = try values.decodeIfPresent(String.self, forKey: .fullAddress)
        isCrowsourceParking = try values.decodeIfPresent(Bool.self, forKey: .isCrowsourceParking)
        placeId = try values.decodeIfPresent(String.self, forKey: .placeId)
        userLocationLinkIdRef = try values.decodeIfPresent(Int.self, forKey: .userLocationLinkIdRef)
        amenityId = try values.decodeIfPresent(String.self, forKey: .amenityId)
        layerCode = try values.decodeIfPresent(String.self, forKey: .layerCode)
    }

}

struct BaseSearchAddress : Codable {
    let addresses : [SearchAddresses]?

    enum CodingKeys: String, CodingKey {

        case addresses = "addresses"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        addresses = try values.decodeIfPresent([SearchAddresses].self, forKey: .addresses)
    }

}

struct AnalyticDestAddress : Codable {
    
    let lat: Double?
    let long: Double?
    let address1: String?
    let address2: String?
    
    let amenityType: String?
    let amenityId: String?
    let layerCode: String?
    let fullAddress: String?
    let isCrowdsourceParking: Bool?
    let placeId: String?
    let userLocationLinkIdRef: Int?
    
    func needUpdateCrowdsource() -> Bool {
        return isCrowdsourceParking ?? false
    }
    
    func getCrowdsource() -> [String: Any] {
        var retDict: [String: Any] = [:]
        
        if let ID = self.placeId, !ID.isEmpty {
            retDict["placeId"] = ID
        }
        
        if let ID = self.userLocationLinkIdRef {
            retDict["userLocationLinkIdRef"] = ID
        }
        
        if let ID = self.amenityId, !ID.isEmpty {
            retDict["amenityId"] = ID
        }
        
        if let ID = self.layerCode, !ID.isEmpty {
            retDict["layerCode"] = ID
        }
        return retDict
    }
    
    enum CodingKeys: String, CodingKey {
        case lat = "lat"
        case long = "long"
        case address1 = "address1"
        case address2 = "address2"
        case amenityType = "amenityType"
        case amenityId = "amenityId"
        case layerCode = "layerCode"
        case fullAddress = "fullAddress"
        case isCrowdsourceParking = "isCrowdsourceParking"
        case placeId
        case userLocationLinkIdRef
    }
    
    init(address1: String?, address2: String?, lat: Double?, long: Double?, amenityType: String?, amenityId: String?, layerCode: String?, fullAddress: String? = nil, isCrowdsourceParking: Bool? = false, placeId: String? = nil, userLocationLinkIdRef: Int? = nil) {
        self.address1 = address1
        self.address2 = address2
        self.lat = lat
        self.long = long
        self.amenityType = amenityType
        self.amenityId = amenityId
        self.layerCode = layerCode
        self.fullAddress = fullAddress
        self.isCrowdsourceParking = isCrowdsourceParking
        self.placeId = placeId
        self.userLocationLinkIdRef = userLocationLinkIdRef
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.address1 = try container.decodeIfPresent(String.self, forKey: .address1)
        self.address2 = try container.decodeIfPresent(String.self, forKey: .address2)
        self.lat = try container.decodeIfPresent(Double.self, forKey: .lat)
        self.long = try container.decodeIfPresent(Double.self, forKey: .long)
        self.amenityType = try container.decodeIfPresent(String.self, forKey: .amenityType)
        self.amenityId = try container.decodeIfPresent(String.self, forKey: .amenityId)
        self.layerCode = try container.decodeIfPresent(String.self, forKey: .layerCode)
        self.fullAddress = try container.decodeIfPresent(String.self, forKey: .fullAddress)
        self.isCrowdsourceParking = try container.decodeIfPresent(Bool.self, forKey: .isCrowdsourceParking)
        self.placeId = try container.decodeIfPresent(String.self, forKey: .placeId)
        self.userLocationLinkIdRef = try container.decodeIfPresent(Int.self, forKey: .userLocationLinkIdRef)
    }
}


/*
struct SelectedAddress  {
    var lat : String
    var long : String
    var address : String
   

   

}
*/

//
//  SchoolZoneData.swift
//  Breeze
//
//  Created by Zhou Hao on 28/5/21.
//

import Foundation
import Turf

public class SchoolZoneData: DataSetItem {
    @objc dynamic var id: String = ""
    @objc dynamic var featureJson: String = ""
    @objc dynamic var address: String = ""
    
//    public override class func primaryKey() -> String? {
//        return "id"
//    }
    
    convenience init(id: String, feature: Turf.Feature, address: String) {
        self.init()
        
        self.id = id
        self.address = address
        
        do {
            
            let jsonData = try JSONEncoder().encode(feature)
            self.featureJson = String(data: jsonData, encoding: .utf8)!
        }
        catch(let error){
            print(error)
        }
    }
}

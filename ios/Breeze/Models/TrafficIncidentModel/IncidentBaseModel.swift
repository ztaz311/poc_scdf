//
//  IncidentBaseModel.swift
//  Breeze
//
//  Created by VishnuKanth on 04/05/21.
//

import Foundation

public struct Incidents: Codable {
    public var incidentArray: [String: [Inner]]
    
    public struct Inner: Codable {
        public let id: String?
        public let Latitude: Double?
        public let Longitude: Double?
        public let Message: String?
        
        enum CodingKeys: String, CodingKey {
            case id
            case Latitude = "Latitude"
            case Longitude = "Longitude"
            case Message = "Message"
        }
    }
    
    private struct CustomCodingKeys: CodingKey {
        var stringValue: String
        init?(stringValue: String) {
            self.stringValue = stringValue
        }
        var intValue: Int?
        init?(intValue: Int) {
            return nil
        }
    }
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CustomCodingKeys.self)
        
        self.incidentArray = [String: [Inner]]()
        for key in container.allKeys {
            let value = try container.decodeIfPresent([Inner].self, forKey: CustomCodingKeys(stringValue: key.stringValue)!) ?? []
            let innerArray = value.map { element in
                return Inner(id: "\(key.stringValue)\(hashValue(inner: element))", Latitude: element.Latitude, Longitude: element.Longitude, Message: element.Message)
            }
            self.incidentArray[key.stringValue] = innerArray
        }
    }
    
    private func hashValue(inner: Inner) -> Int {
        guard let latitude = inner.Latitude, let longitude = inner.Longitude else { return 0 }
        return abs(longitude.hashValue ^ latitude.hashValue)
    }
}

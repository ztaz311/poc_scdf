//
//  Favourites.swift
//  Breeze
//
//  Created by VishnuKanth on 12/08/21.
//

import Foundation
struct Favourites : Codable {
    let tripEtaFavouriteId : Int?
    let recipientName : String?
    let recipientNumber : String?
    let shareLiveLocation : String?
    let voiceCallToRecipient : String?
    let destination : String?
    let tripDestLat : String?
    let tripDestLong : String?
    let message : String?
    let module : String?
    let count : Int?

    enum CodingKeys: String, CodingKey {

        case tripEtaFavouriteId = "tripEtaFavouriteId"
        case recipientName = "recipientName"
        case recipientNumber = "recipientNumber"
        case shareLiveLocation = "shareLiveLocation"
        case voiceCallToRecipient = "voiceCallToRecipient"
        case destination = "destination"
        case tripDestLat = "tripDestLat"
        case tripDestLong = "tripDestLong"
        case message = "message"
        case module = "module"
        case count = "count"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        tripEtaFavouriteId = try values.decodeIfPresent(Int.self, forKey: .tripEtaFavouriteId)
        recipientName = try values.decodeIfPresent(String.self, forKey: .recipientName)
        recipientNumber = try values.decodeIfPresent(String.self, forKey: .recipientNumber)
        shareLiveLocation = try values.decodeIfPresent(String.self, forKey: .shareLiveLocation)
        voiceCallToRecipient = try values.decodeIfPresent(String.self, forKey: .voiceCallToRecipient)
        destination = try values.decodeIfPresent(String.self, forKey: .destination)
        tripDestLat = try values.decodeIfPresent(String.self, forKey: .tripDestLat)
        tripDestLong = try values.decodeIfPresent(String.self, forKey: .tripDestLong)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        module = try values.decodeIfPresent(String.self, forKey: .module)
        count = try values.decodeIfPresent(Int.self, forKey: .count)
    }

}

//
//  BaseETAFavModel.swift
//  Breeze
//
//  Created by VishnuKanth on 12/08/21.
//

import Foundation

struct BaseETAFavModel : Codable {
    let favourites : [Favourites]?

    enum CodingKeys: String, CodingKey {
        case favourites
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        favourites = try values.decodeIfPresent([Favourites].self, forKey: .favourites)
    }

}

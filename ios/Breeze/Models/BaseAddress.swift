//
//  BaseAddress.swift
//  Breeze
//
//  Created by Zhou Hao on 7/5/21.
//

import Foundation

protocol BaseAddress {
    var lat : String? { get }
    var long : String? { get }
    var alias : String? { get }
    var address1 : String? { get }
    var address2 : String? { get }
}


//
//  Profiles.swift
//  Breeze
//
//  Created by Tuyen, Le Xuan  on 19/05/2022.
//

import Foundation

struct Profiles : Codable {
    let isActive: Bool?
    let element_id : String?
    let element_name : String?
    let display_text : String?
    let map_icon_light_selected_url : String?
    let map_icon_light_unselected_url : String?
    let map_icon_dark_selected_url : String?
    let map_icon_dark_unselected_url : String?
    let zones : [ContentZone]?
    let tooltip_radius: Int?
    let season_parking: [SeasonParking]?
    let amenities : [String]?
    let default_amenities : [Default_amenities]?

    enum CodingKeys: String, CodingKey {

        case isActive = "isActive"
        case element_id = "element_id"
        case element_name = "element_name"
        case display_text = "display_text"
        case map_icon_light_selected_url = "landing_amenities_light_selected_url"
        case map_icon_light_unselected_url = "landing_amenities_light_unselected_url"
        case map_icon_dark_selected_url = "landing_amenities_dark_selected_url"
        case map_icon_dark_unselected_url = "landing_amenities_dark_unselected_url"
        case zones = "zones"
        case season_parking = "season_parking"
        case amenities = "amenities"
        case default_amenities = "default_amenities"
        case tooltip_radius = "tooltip_radius"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        element_id = try values.decodeIfPresent(String.self, forKey: .element_id)
        element_name = try values.decodeIfPresent(String.self, forKey: .element_name)
        display_text = try values.decodeIfPresent(String.self, forKey: .display_text)
        map_icon_light_selected_url = try values.decodeIfPresent(String.self, forKey: .map_icon_light_selected_url)
        map_icon_light_unselected_url = try values.decodeIfPresent(String.self, forKey: .map_icon_light_unselected_url)
        map_icon_dark_selected_url = try values.decodeIfPresent(String.self, forKey: .map_icon_dark_selected_url)
        map_icon_dark_unselected_url = try values.decodeIfPresent(String.self, forKey: .map_icon_dark_unselected_url)
        zones = try values.decodeIfPresent([ContentZone].self, forKey: .zones)
        season_parking = try values.decodeIfPresent([SeasonParking].self, forKey: .season_parking)
        amenities = try values.decodeIfPresent([String].self, forKey: .amenities)
        default_amenities = try values.decodeIfPresent([Default_amenities].self, forKey: .default_amenities)
        tooltip_radius = try values.decodeIfPresent(Int.self, forKey: .tooltip_radius)
        isActive = try values.decodeIfPresent(Bool.self, forKey: .isActive)
    }

}

//
//  ZoneAlert.swift
//  Breeze
//
//  Created by Zhou Hao on 23/5/22.
//

import Foundation

struct ZoneAlert: Codable {
    let alertMessage: String?
    let isCongestion: Bool?
    
    enum CodingKeys: String, CodingKey {
        case alertMessage = "alertMessage"
        case isCongestion = "isCongestion"
    }
    
    init(alertMessage: String, isCongestion: Bool) {
        self.alertMessage = alertMessage
        self.isCongestion = isCongestion
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        alertMessage = try values.decodeIfPresent(String.self, forKey: .alertMessage)
        isCongestion = try values.decodeIfPresent(Bool.self, forKey: .isCongestion)
    }
}

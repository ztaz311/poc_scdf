//
//  SeasonParking.swift
//  Breeze
//
//  Created by Zhou Hao on 24/5/22.
//

import Foundation

struct SeasonParking : Codable {
    let lat : Double?
    let long : Double?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        lat = try values.decodeIfPresent(Double.self, forKey: .lat)
        long = try values.decodeIfPresent(Double.self, forKey: .long)
    }
}

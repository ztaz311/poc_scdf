//
//  ZoneColor.swift
//  Breeze
//
//  Created by Tuyen, Le Xuan  on 19/05/2022.
//

import Foundation

struct ZoneColor: Codable {
    
    let name, color: String
    let opacity, availablePercent: Double?
    
    enum CodingKeys: String, CodingKey {
        case name, color, opacity, availablePercent
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.name = try container.decodeIfPresent(String.self, forKey: .name) ?? ""
        self.color = try container.decodeIfPresent(String.self, forKey: .color) ?? ""
        self.opacity = try container.decodeIfPresent(Double.self, forKey: .opacity)
        self.availablePercent = try container.decodeIfPresent(Double.self, forKey: .availablePercent)
    }
}

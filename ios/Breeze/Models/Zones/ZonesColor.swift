//
//  ZonesColor.swift
//  Breeze
//
//  Created by Tuyen, Le Xuan  on 19/05/2022.
//

import Foundation

struct ZonesColor: Codable {
    let colour: String
}

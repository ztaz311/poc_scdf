//
//  Zones.swift
//  Breeze
//
//  Created by Tuyen, Le Xuan  on 19/05/2022.
//

import Foundation

struct Zones : Codable {
    let center_point_lat : Double?
    let center_point_long : Double?
    let radius : Int
    let zone_id : String
    let active_zoom_level : Int?
    let deactive_zoom_level : Int?

    enum CodingKeys: String, CodingKey {

        case center_point_lat = "center_point_lat"
        case center_point_long = "center_point_long"
        case radius = "radius"
        case zone_id = "zone_id"
        case active_zoom_level = "active_zoom_level"
        case deactive_zoom_level = "deactive_zoom_level"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        center_point_lat = try values.decodeIfPresent(Double.self, forKey: .center_point_lat)
        center_point_long = try values.decodeIfPresent(Double.self, forKey: .center_point_long)
        radius = try values.decode(Int.self, forKey: .radius)
        zone_id = try values.decode(String.self, forKey: .zone_id)
        active_zoom_level = try values.decodeIfPresent(Int.self, forKey: .active_zoom_level)
        deactive_zoom_level = try values.decodeIfPresent(Int.self, forKey: .deactive_zoom_level)
    }
}

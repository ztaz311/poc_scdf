//
//  Feature.swift
//  Breeze
//
//  Created by Zhou Hao on 6/1/21.
//

import Foundation
import CoreLocation

enum FeatureType {
    case schoolZone, speedCamera, silverZone, erpZone, dengueZone
}

struct Feature: Equatable {
    var type: FeatureType
    var title: String
    var coordinate: CLLocationCoordinate2D
    var distance: Double
    
    static func ==(lhs: Feature, rhs: Feature) -> Bool {
        return lhs.type == rhs.type && lhs.title == rhs.title
    }
    
    func getType() -> String {
        switch type {
        case .dengueZone:
            return "Dengue Zone"
        case .schoolZone:
            return "School Zone"
        case .speedCamera:
            return "Speed Camera"
        case .silverZone:
            return "Silver Zone"
        case .erpZone:
            return "ERP"
        }        
    }
    
}


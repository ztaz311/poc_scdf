//
//  FeedbackIssueModel.swift
//  Breeze
//
//  Created by Malou Mendoza on 13/8/21.
//

import Foundation


public struct FeedbackIssue: Codable {
   
    
    var issuetype: [FeedbackIssueModel]
    var description: String
    
    enum CodingKeys: String, CodingKey {

        
        case issuetype = "type"
        case description = "issuedescription"
        
    }
    
    init( issuetype: [FeedbackIssueModel], description: String) {
        self.issuetype = issuetype
        self.description = description
    }

}

public struct FeedbackIssueModel: Codable {
    
    let issuetype: String
    let lat: Double
    let long : Double
    
   
    
    enum CodingKeys: String, CodingKey {

        case issuetype = "issuetype"
        case lat = "lat"
        case long = "long"
        
    }
    
    init(issuetype: String, lat: Double, long: Double ) {
        self.issuetype = issuetype
        self.lat = lat
        self.long = long
       
    }

}

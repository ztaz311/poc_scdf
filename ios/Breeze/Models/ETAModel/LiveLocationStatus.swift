//
//  LiveLocationStatus.swift
//  Breeze
//
//  Created by Zhou Hao on 14/7/21.
//

import Foundation

enum LiveLocationStatusType: String {
    case inprogress = "INPROGRESS"
    case pause = "PAUSE"
    case complete = "COMPLETE"
    case cancel = "CANCEL"
}

struct LiveLocationStatus: Codable {        
    let tripEtaUUID: String?
    let status: String?    
}

struct LiveLocationStatusResponse: Codable {
    let message: String
    
    enum CodingKeys: String, CodingKey {
        case message
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.message = try container.decodeIfPresent(String.self, forKey: .message) ?? ""
    }
}

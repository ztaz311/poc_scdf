//
//  TripETAReminder.swift
//  Breeze
//
//  Created by Zhou Hao on 14/7/21.
//

import Foundation

struct TripETAReminder : Codable {    
    let tripEtaUUID: String
    let type: String
}

struct TripETAReminderResponse: Codable {
    let message: String
    
    enum CodingKeys: String, CodingKey {
        case message
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.message = try container.decodeIfPresent(String.self, forKey: .message) ?? ""
    }
}

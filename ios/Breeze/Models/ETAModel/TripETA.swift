//
//  TripETA.swift
//  Breeze
//
//  Created by Zhou Hao on 13/7/21.
//

import Foundation

// used by RN to pass data
struct ETAInfo {
    
//    struct Status {
//        static let INIT = "init"
//        static let EDIT = "edit"
//    }
    
    let message: String
    let recipientName: String
    let recipientNumber: String
//    let shareLiveLocation: Bool
//    let voiceCallToRecipient: Bool
    let etaFavoriteId: Int
}

// TODO: Refactor to use same struct for TripETA and TripCruiseETA later
struct TripETA : Codable {
    
    struct ETAType {
        static let Init = "INIT"
        static let Reminder15 = "15MIN_BFR"
        //static let Reminder1 = "1MIN_BFR"
        static let Reminder1 = "REMINDER_1"
    }
        
    let type: String
    let message: String
    let recipientName: String
    let recipientNumber: String
    let shareLiveLocation: String // Y/N
    let voiceCallToRecipient = "N" // Y/N
    let tripStartTime: Int
    var tripEndTime:Int
    let tripDestLat: Double
    let tripDestLong: Double
    let destination: String
    let tripEtaFavouriteId: Int
    
}

struct TripCruiseETA : Codable {
    
    struct ETAType {
        static let Init = "INIT"
        static let Reminder15 = "15MIN_BFR"
        static let Reminder1 = "1MIN_BFR"
    }
        
    let type: String
    let message: String
    let recipientName: String
    let recipientNumber: String
    let shareLiveLocation: String // Y/N
    let voiceCallToRecipient = "N" // Y/N
    let tripStartTime: Int
    let tripDestLat: Double
    let tripDestLong: Double
    let destination: String
    var tripEtaFavouriteId:Int //Only for Cruise
}

struct TripETAResponse: Codable {
    let tripEtaUUID: String
    let message: String
    
    enum CodingKeys: String, CodingKey {
        case tripEtaUUID
        case message
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.tripEtaUUID = try container.decodeIfPresent(String.self, forKey: .tripEtaUUID) ?? ""
        self.message = try container.decodeIfPresent(String.self, forKey: .message) ?? ""
    }
    
}



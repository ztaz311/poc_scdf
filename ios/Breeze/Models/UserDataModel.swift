//
//  TNCModel.swift
//  Breeze
//
//  Created by Tuyen, Le Xuan  on 26/05/2022.
//

import Foundation

struct UserDataModel: Codable {
    private var tncAcceptStatus: Bool
    private(set) var carplateNumber: String
    private(set) var iuNumber: String
    private(set) var showNudge: Bool?
    private(set) var createdTimeStamp: Int
    private(set) var isLogEnabled: Bool?
    
    func getCarDetailDisplay() -> String {
        return carplateNumber + " | " + iuNumber
    }
    
    enum CodingKeys: String, CodingKey {
        case tncAcceptStatus = "tncAcceptStatus"
        case carplateNumber = "carplateNumber"
        case iuNumber = "iuNumber"
        case showNudge = "showNudge"
        case createdTimeStamp = "createdTimeStamp"
        case isLogEnabled = "isLogEnabled"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.tncAcceptStatus = try container.decodeIfPresent(Bool.self, forKey: .tncAcceptStatus) ?? false
        self.carplateNumber = try container.decodeIfPresent(String.self, forKey: .carplateNumber) ?? ""
        self.iuNumber = try container.decodeIfPresent(String.self, forKey: .iuNumber) ?? ""
        self.showNudge = try container.decodeIfPresent(Bool.self, forKey: .showNudge)
        self.createdTimeStamp = try container.decodeIfPresent(Int.self, forKey: .createdTimeStamp) ?? 0
        self.isLogEnabled = try container.decodeIfPresent(Bool.self, forKey: .isLogEnabled)
    }
}

extension UserDataModel {
    var tncStatus: Bool {
        return tncAcceptStatus
    }
    
    var haveCardetail: Bool {
        return !(carplateNumber.isEmpty && iuNumber.isEmpty)
    }
    
    var ifShowNudge: Bool {
        return showNudge ?? false
    }
}

struct UserPref : Codable {
    let success : Bool?
    let message : String?

    enum CodingKeys: String, CodingKey {
        case success
        case message
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
        message = try values.decodeIfPresent(String.self, forKey: .message)
    }
}

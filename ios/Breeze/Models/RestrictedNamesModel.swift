//
//  RestrictedNamesModel.swift
//  Breeze
//
//  Created by Tuan, Pham Hai on 30/03/2023.
//

import Foundation

struct RestrictedNamesModel: Codable {
    var data: [String] = []
    
    enum CodingKeys: String, CodingKey {
        case data = "data"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.data = try container.decodeIfPresent([String].self, forKey: .data) ?? []
    }
}

//
//  RouteAblePoints.swift
//  Breeze
//
//  Created by Tan, Tran Ngoc on 13/02/2023.
//

import Foundation
import CoreLocation

struct RoutablePointModel : Codable {
    let type : String?
    let lat : Double?
    let long : Double?
    let icon : StyleImageModel?
    let name : String?
    let carparkId: String?
    let fullAddress: String?
    let address1: String?
    let address2: String?
    
    enum CodingKeys: String, CodingKey {
        case type = "type"
        case lat = "lat"
        case long = "long"
        case icon = "icon"
        case name = "name"
        case carparkId = "carparkId"
        case fullAddress = "fullAddress"
        case address1 = "address1"
        case address2 = "address2"
    }
    
    init(_ dict: [String: Any]) {
        type = dict["type"] as? String
        lat = dict["lat"] as? Double
        long = dict["long"] as? Double
        name = dict["name"] as? String
        carparkId = dict["carparkId"] as? String
        fullAddress = dict["fullAddress"] as? String
        address1 = dict["address1"] as? String
        address2 = dict["address2"] as? String
        let iconDict = dict["icon"] as? [String: Any]
        icon = StyleImageModel(iconDict ?? [:])
    }

//    init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        type = try values.decodeIfPresent(String.self, forKey: .type)
//        lat = try values.decodeIfPresent(Double.self, forKey: .lat)
//        long = try values.decodeIfPresent(Double.self, forKey: .long)
//        icon = try values.decodeIfPresent(StyleImageModel.self, forKey: .icon)
//        name = try values.decodeIfPresent(String.self, forKey: .name)
//        carparkId = try values.decodeIfPresent(String.self, forKey: .carparkId)
//        fullAddress = try values.decodeIfPresent(String.self, forKey: .fullAddress)
//        address1 = try values.decodeIfPresent(String.self, forKey: .address1)
//        address2 = try values.decodeIfPresent(String.self, forKey: .address2)
//    }
}

extension RoutablePointModel {
    var coordinate: CLLocationCoordinate2D? {
        guard let lat = lat,
              let long = long else {
                  return nil
              }
        return CLLocationCoordinate2D(latitude: lat, longitude: long)
    }
}

struct StyleImageModel : Codable {
    let lightImage: String?
    let darktImage: String?
    
    enum CodingKeys: String, CodingKey {
        case lightImage = "light"
        case darktImage = "dark"
    }
    
    init(_ dict: [String: Any]) {
        lightImage = dict["lightImage"] as? String
        darktImage = dict["darktImage"] as? String
    }
}

//
//  SpeedCameraData.swift
//  Breeze
//
//  Created by VishnuKanth on 04/03/22.
//

import Foundation
import Turf

struct SpeedCameraData : Codable {
    let location_latitude : String?
    let _id : Int?
    let location : String?
    let type_of_speed_camera : String?
    let location_longitude : String?

    enum CodingKeys: String, CodingKey {

        case location_latitude = "location_latitude"
        case _id = "_id"
        case location = "location"
        case type_of_speed_camera = "type_of_speed_camera"
        case location_longitude = "location_longitude"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        location_latitude = try values.decodeIfPresent(String.self, forKey: .location_latitude)
        _id = try values.decodeIfPresent(Int.self, forKey: ._id)
        location = try values.decodeIfPresent(String.self, forKey: .location)
        type_of_speed_camera = try values.decodeIfPresent(String.self, forKey: .type_of_speed_camera)
        location_longitude = try values.decodeIfPresent(String.self, forKey: .location_longitude)
    }

}

public class SpeedCameraCollectionData: DataSetItem {
    @objc dynamic var id: String = ""
    @objc dynamic var featureJson: String = ""
    
//    public override class func primaryKey() -> String? {
//        return "id"
//    }
    
    convenience init(id: String, feature: Turf.Feature) {
        self.init()
        
        self.id = id
        
        do {
            
            let jsonData = try JSONEncoder().encode(feature)
            self.featureJson = String(data: jsonData, encoding: .utf8)!
        }
        catch(let error){
            print(error)
        }
    }
}

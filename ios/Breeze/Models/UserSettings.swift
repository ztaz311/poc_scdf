//
//  UserSettings.swift
//  Breeze
//
//  Created by Malou Mendoza on 31/5/21.
//

import Foundation
import Combine
import SwiftyBeaver

struct SettingsDetail : Codable {
    let attribute:String?
    let value : String?
    
    enum CodingKeys: String, CodingKey {
        case attribute = "attribute"
        case value = "attribute_code"   // use attribute_code to replace value
    }
    
    init(attribute: String, value: String) {
        self.attribute = attribute
        self.value = value
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        attribute = try values.decodeIfPresent(String.self, forKey: .attribute)
        value = try values.decodeIfPresent(String.self, forKey: .value)
    }
}

struct UserSettings : Codable {
    
    let settings : [SettingsDetail]?
    
    enum CodingKeys: String, CodingKey {
        case settings = "settings"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        settings = try values.decodeIfPresent([SettingsDetail].self, forKey: .settings)
    }
    
    init(settings: [SettingsDetail]) {
        self.settings = settings
    }
}

// MARK: Carpark availability value constants
struct CarparkAvailabilityValues {
    static let all = "All"
    static let type2 = "TYPE2"
    static let hide = "HIDE"
}

final class UserSettingsModel:NSObject {
    
    // MARK: - Constants
    
    struct UserSettingConst {
        
        static let attribute: String = "attribute"
        static let value: String = "attribute_code"
        
        // other constants
        static let showDemoYes: String = "Yes"
        static let showDemoNo: String = "No"
        static let valueOne: String = "1"
        static let valueZero: String = "0"
    }
    
    // MARK: attribute constants
    struct UserSettingAttributes {
        static let mapDisplayKey: String = "MapDisplay"
        static let routePreference: String = "RoutePreference"
        static let showDemo: String = "ShowDemo"
        static let carparkDistance: String = "CarparkDistance"
        static let carparkCount: String = "CarparkCount"
        static let carparkAvailability: String = "CarparkAvailability"
        static let thirdPartyEmail: String = "3partyemail"
        static let mkgEmail: String = "mkgemail"
        static let collectionLimit: String = "CollectionLimit"
        // OBU additional info
        static let trafficInformation: String = "TrafficInformation"
        static let parkingAvailabilityDisplay: String = "ParkingAvailabilityDisplay"
        static let parkingAvailabilityAudio: String = "ParkingAvailabilityAudio"
        static let estimatedTravelTimeDisplay: String = "EstimatedTravelTimeDisplay"
        static let estimatedTravelTimeAudio: String = "EstimatedTravelTimeAudio"
        static let schoolZoneDisplayAlert: String = "SchoolZoneDisplayAlert"
        static let schoolZoneVoiceAlert: String = "SchoolZoneVoiceAlert"
        static let silverZoneDisplayAlert: String = "SilverZoneDisplayAlert"
        static let silverZoneVoiceAlert: String = "SilverZoneVoiceAlert"
        static let busLaneDisplayAlert: String = "BusLaneDisplayAlert"
        static let busLaneVoiceAlert: String = "BusLaneVoiceAlert"
        //  Navigation distance
        static let carparkListDistance: String = "CarparkListDistance"
        static let carparkAvailabilityDistance: String = "CarparkAvailabilityDistance"
        //  Petrol/EV distance range
        static let evPetrolCount: String = "EVPetrolCount"
        static let evPetrolRange: String = "EVPetrolRange"
        static let maxEVPetrolRange: String = "MaxEVPetrolRange"
        static let carparkAvailabilityAlertDisplayTime: String = "CarparkAvailabilityAlertDisplayTime"
    }
    
    // MARK: map display value constants
    struct MapDisplayValues {
        static let mapDisplayKeyAuto: String = "Auto"
        static let mapDisplayKeyDark: String = "Dark"
        static let mapDisplayKeyLight: String = "Light"
    }
    
    // MARK: route preference value constants
    struct RoutePreferenceValues {
        static let routePreferenceFastest: String = "Fastest"
        static let routePreferenceCheapest: String = "Cheapest"
        static let routePreferenceShortest: String = "Shortest"
    }
    
    static let sharedInstance = UserSettingsModel()
    private var cancellable: AnyCancellable?
    
    // MARK: - Public properties
    /// master data
    var master: MasterListData?
    
    var carparkAvailabilityCode: String? {
        return getCode(value: carparkAvailability.stringValue, data: master?.masterlists.carparkAvailability ?? [])
    }
    
    /// get settings details from API. Must get the master list first
    var userSettings: UserSettings? {
        didSet {
            guard let settingsArr = userSettings?.settings else { return }
            
            // parse the user data settings based on the master list
            for setting in settingsArr {
                if updatePreferenesSettings(attribute: setting.attribute ?? "", attributeCode: setting.value ?? "") {
                    // update when the carpark data has been updated
                    carparkUpdatedAction.send()
                }
                let attribute = setting.attribute
                
                // parse different attributes
                // 1. mapDisplay
                if (attribute == UserSettingAttributes.mapDisplayKey) {
                    // get value from master list by using setting's value as key - Santoso harcode always light mode on Mobile
                    UserSettingsModel.sharedInstance.mapDisplay = MapDisplayValues.mapDisplayKeyLight // getValue(code: setting.value ?? "", data: master?.masterlists.mapDisplay)
                    print("1111, get setting \(UserSettingsModel.sharedInstance.mapDisplay)")
                    
                    // TODO: Since there are many places using UserSettingsModel, consider to merge Settings model to UserSettingsModel later.
                    Settings.shared.setTheme(key: UserSettingsModel.sharedInstance.mapDisplay)
                }
                
                // 2. routePreference
                if (attribute == UserSettingAttributes.routePreference) {
                    UserSettingsModel.sharedInstance.routePref = getValue(code: setting.value ?? "", data: master?.masterlists.routePreference)
                    
                    if UserSettingsModel.sharedInstance.routePref == RoutePreferenceValues.routePreferenceFastest {
                        Settings.shared.routeOption = 0
                        
                    }
                    else if UserSettingsModel.sharedInstance.routePref == RoutePreferenceValues.routePreferenceCheapest {
                        Settings.shared.routeOption = 1
                        
                    }
                    
                    else if UserSettingsModel.sharedInstance.routePref == RoutePreferenceValues.routePreferenceShortest{
                        Settings.shared.routeOption = 2
                        
                    }
                }
                
                // 3. showDemo
                if (attribute == UserSettingAttributes.showDemo) {
                    let value = getValue(code: setting.value ?? "No", data: master?.masterlists.showDemo)
                    self.needShowDemo = value == ShowDemo.y.rawValue
                }
                
                // 4. thirdpaty email
                if attribute == UserSettingAttributes.thirdPartyEmail {
                    // TODO: do it later
                }
                
                // 5. mkgemail
                if attribute == UserSettingAttributes.mkgEmail {
                    // TODO: do it later
                }
                
                // 6. CarparkDistance
                if attribute == UserSettingAttributes.carparkDistance {
                    carparkDistance = Int(getValue(code: setting.value ?? "", data: master?.masterlists.carparkDistance)) ?? Values.carparkMonitorDistanceInLanding // should not happen
                }
                
                // 7. CarparkCount
                if attribute == UserSettingAttributes.carparkCount {
                    carparkCount = Int(getValue(code: setting.value ?? "", data: master?.masterlists.carparkCount)) ?? Values.maxNoOfCarparksInLanding
                }
                
                // 8. Carpark availability
                if attribute == UserSettingAttributes.carparkAvailability {
                    let value = getValue(code: setting.value ?? "", data: master?.masterlists.carparkAvailability)
                    if value == CarparkAvailabilityValues.all {
                        carparkAvailability = .all
                    } else if value == CarparkAvailabilityValues.type2 {
                        carparkAvailability = .available
                    } else if value == CarparkAvailabilityValues.hide {
                        carparkAvailability = .hide
                    }
                }
                
                // 9. Collection limit
                if attribute == UserSettingAttributes.collectionLimit {
                    collectionLimit = Int(getValue(code: setting.value ?? "", data: master?.masterlists.bookmarkLimit)) ?? Values.maxBookmarkLimit
                }
                
                // 10. OBU road messages
                if (attribute == UserSettingAttributes.trafficInformation) {
                    let value = getValue(code: setting.value ?? "No", data: master?.masterlists.trafficInformation)
                    self.obuTrafficInfoEnable = (value == ObuRoadMessage.y.rawValue)
                } else if (attribute == UserSettingAttributes.parkingAvailabilityDisplay) {
                    let value = getValue(code: setting.value ?? "No", data: master?.masterlists.parkingAvailabilityDisplay)
                    self.obuParkingAvailabilityDisplayEnable = (value == ObuRoadMessage.y.rawValue)
                } else if (attribute == UserSettingAttributes.parkingAvailabilityAudio) {
                    let value = getValue(code: setting.value ?? "No", data: master?.masterlists.parkingAvailabilityAudio)
                    self.obuParkingAvailabilityAudioEnable = (value == ObuRoadMessage.y.rawValue)
                } else if (attribute == UserSettingAttributes.estimatedTravelTimeDisplay) {
                    let value = getValue(code: setting.value ?? "No", data: master?.masterlists.estimatedTravelTimeDisplay)
                    self.obuEstimatedTravelTimeDisplayEnable = (value == ObuRoadMessage.y.rawValue)
                } else if (attribute == UserSettingAttributes.estimatedTravelTimeAudio) {
                    let value = getValue(code: setting.value ?? "No", data: master?.masterlists.estimatedTravelTimeAudio)
                    self.obuEstimatedTravelTimeAudioEnable = (value == ObuRoadMessage.y.rawValue)
                } else if (attribute == UserSettingAttributes.schoolZoneDisplayAlert) {
                    let value = getValue(code: setting.value ?? "No", data: master?.masterlists.schoolZoneDisplayAlert)
                    self.schoolZoneDisplayAlert = (value == ObuRoadMessage.y.rawValue)
                } else if (attribute == UserSettingAttributes.schoolZoneVoiceAlert) {
                    let value = getValue(code: setting.value ?? "No", data: master?.masterlists.schoolZoneVoiceAlert)
                    self.schoolZoneVoiceAlert = (value == ObuRoadMessage.y.rawValue)
                } else if (attribute == UserSettingAttributes.silverZoneDisplayAlert) {
                    let value = getValue(code: setting.value ?? "No", data: master?.masterlists.silverZoneDisplayAlert)
                    self.silverZoneDisplayAlert = (value == ObuRoadMessage.y.rawValue)
                } else if (attribute == UserSettingAttributes.silverZoneVoiceAlert) {
                    let value = getValue(code: setting.value ?? "No", data: master?.masterlists.silverZoneVoiceAlert)
                    self.silverZoneVoiceAlert = (value == ObuRoadMessage.y.rawValue)
                } else if (attribute == UserSettingAttributes.busLaneDisplayAlert) {
                    let value = getValue(code: setting.value ?? "No", data: master?.masterlists.busLaneDisplayAlert)
                    self.busLaneDisplayAlert = (value == ObuRoadMessage.y.rawValue)
                } else if (attribute == UserSettingAttributes.busLaneVoiceAlert) {
                    let value = getValue(code: setting.value ?? "No", data: master?.masterlists.busLaneVoiceAlert)
                    self.busLaneVoiceAlert = (value == ObuRoadMessage.y.rawValue)
                }
                
                //  11. Parking availability distance in Navigation
                if attribute == UserSettingAttributes.carparkListDistance {
                    self.carparkListDistance = Double(getValue(code: setting.value ?? "", data: master?.masterlists.carparkListDistance)) ?? Values.carparkListDistanceInNavigation
                } else if attribute == UserSettingAttributes.carparkAvailabilityDistance{
                    self.carparkAvailabilityDistance = Double(getValue(code: setting.value ?? "", data: master?.masterlists.carparkAvailabilityDistance)) ?? Values.carparkAvailabilityDistanceInNavigation
                }
                
                //  12. Petrol/EV distance in Navigation/Cruise
                if attribute == UserSettingAttributes.evPetrolCount {
                    self.evPetrolCount = Int(getValue(code: setting.value ?? "", data: master?.masterlists.evPetrolCount)) ?? Values.evPetrolCount
                } else if attribute == UserSettingAttributes.evPetrolRange {
                    self.evPetrolRange = Int(getValue(code: setting.value ?? "", data: master?.masterlists.evPetrolRange)) ?? Values.evPetrolRange
                } else if attribute == UserSettingAttributes.maxEVPetrolRange {
                    self.maxEVPetrolRange = Int(getValue(code: setting.value ?? "", data: master?.masterlists.maxEVPetrolRange)) ?? Values.maxEVPetrolRange
                }
                
                if attribute == UserSettingAttributes.carparkAvailabilityAlertDisplayTime {
                    self.carparkAvailabilityAlertDisplayTime = Int(getValue(code: setting.value ?? "", data: master?.masterlists.carparkAvailabilityAlertDisplayTime)) ?? Values.carparkAvailabilityAlertDisplayTime
                }
                
            }
//            self.carparkUpdated = ifCarparkUpdated
        }
    }
    
    /// Will be update if any values related to the carpark changed
//    @Published var carparkUpdated = false
    let carparkUpdatedAction = PassthroughSubject<Void, Never>()
    
    private override init() {
        super.init()
        self.cancellable = NotificationCenter.default.publisher(for: Notification.Name(Values.userPreferenceSettingsUpdated), object: nil)
            .receive(on: DispatchQueue.main)
            .sink { [weak self] notification in
                guard let self = self else { return }
                // check values and update if needed
                // this is call by RN. All settings data will pass back to native from RN
                if let array = notification.userInfo?["settings"] as? [Any] {
                    var settings = [SettingsDetail]()
                    
                    for data in array {
                        let itemDict = data as! [String:Any]
                        
                        let attribute = itemDict["attribute"] as! String
                        let attributeCode = itemDict["attribute_code"] as! String
                        settings.append(SettingsDetail(attribute: attribute, value: attributeCode))
                    }
                    self.userSettings = UserSettings(settings: settings)
                }
            }
    }
    
    var mapDisplay:String?
    var routePref: String?
    var carparkDistance: Int = Values.carparkMonitorDistanceInLanding
    var carparkCount: Int = Values.maxNoOfCarparksInLanding
    var carparkAvailability: ParkingType = .all
    var carParkAmenities: ParkingAndAmenitiesType = .all
    var needShowDemo = false
    var collectionLimit = Values.maxBookmarkLimit
    
    //  OBU additional params
    var obuTrafficInfoEnable: Bool = false
    var obuParkingAvailabilityDisplayEnable: Bool = false
    var obuParkingAvailabilityAudioEnable: Bool = false
    var obuEstimatedTravelTimeDisplayEnable: Bool = false
    var obuEstimatedTravelTimeAudioEnable: Bool = false
    
    //  Default is Yes BREEZE2-777
    var schoolZoneDisplayAlert: Bool = false
    var schoolZoneVoiceAlert: Bool = false
    var silverZoneDisplayAlert: Bool = false
    var silverZoneVoiceAlert: Bool = false
    var busLaneDisplayAlert: Bool = false
    var busLaneVoiceAlert: Bool = false
    
    //  BREEZE2-1545
    var carparkListDistance: Double = 1000.0
    var carparkAvailabilityDistance: Double = 2000.0
    
    //  BREEZE2-2148
    var evPetrolCount: Int = 3
    var evPetrolRange: Int = 5000
    var maxEVPetrolRange: Int = 5000
    var carparkAvailabilityAlertDisplayTime: Int = 8
    
    private func getValue(code: String, data: [MasterProperty]?) -> String {
        guard let data = data else {
            return ""
        }
        
        for item in data {
            if item.code == code {
                return item.value
            }
        }
        return "" // this should not happen
    }
    
    private func updateMapDisplaySettings(value: String) {
        UserSettingsModel.sharedInstance.mapDisplay = MapDisplayValues.mapDisplayKeyLight //value
        Settings.shared.setTheme(key: UserSettingsModel.sharedInstance.mapDisplay)
        
        DispatchQueue.main.async {
            UIApplication.shared.windows.forEach { window in
                window.overrideUserInterfaceStyle = .light
//                if value == MapDisplayValues.mapDisplayKeyAuto {
//                    window.overrideUserInterfaceStyle = .unspecified
//                } else if value == MapDisplayValues.mapDisplayKeyLight {
//                    window.overrideUserInterfaceStyle = .light
//                } else if value == MapDisplayValues.mapDisplayKeyDark {
//                    window.overrideUserInterfaceStyle = .dark
//                }
            }
            
            sendThemeChangeEvent(Settings.shared.theme == 2)
        }
    }
    
    private func updateRoutePreferenceSettings(value: String) {
        UserSettingsModel.sharedInstance.routePref = value
        
        if UserSettingsModel.sharedInstance.routePref == RoutePreferenceValues.routePreferenceFastest {
            Settings.shared.routeOption = 0
        }
        else if UserSettingsModel.sharedInstance.routePref == RoutePreferenceValues.routePreferenceCheapest {
            Settings.shared.routeOption = 1
        }
        
        else if UserSettingsModel.sharedInstance.routePref == RoutePreferenceValues.routePreferenceShortest{
            Settings.shared.routeOption = 2
        }
    }
    
    /// updateUserSettings - if need to update userSettings object (only need to update if get the user settings from RN update)
    /// return true if carpark related property updated
    private func updatePreferenesSettings(attribute: String, attributeCode: String) -> Bool {
        guard let master = self.master else {
            SwiftyBeaver.error("Failed to updatePreferenesSettings: Master list is not ready yet")
            return false
        }
        
        var updated = false
        
        if attribute == UserSettingAttributes.mapDisplayKey {
            let value = self.getValue(code: attributeCode, data: master.masterlists.mapDisplay)
            if value != UserSettingsModel.sharedInstance.mapDisplay {
                self.updateMapDisplaySettings(value: value)
            }
        } else if attribute == UserSettingAttributes.routePreference {
            let value = self.getValue(code: attributeCode, data: master.masterlists.routePreference)
            if value != UserSettingsModel.sharedInstance.routePref {
                self.updateRoutePreferenceSettings(value: value)
            }
        } else if attribute == UserSettingAttributes.showDemo {
            let value = self.getValue(code: attributeCode, data: master.masterlists.showDemo)
            let newBoolValue = value == ShowDemo.y.rawValue
            if newBoolValue != self.needShowDemo {
                self.needShowDemo = newBoolValue
            }
        } else if attribute == UserSettingAttributes.trafficInformation {
            let value = self.getValue(code: attributeCode, data: master.masterlists.trafficInformation)
            let newBoolValue = value == ObuRoadMessage.y.rawValue
            if newBoolValue != self.obuTrafficInfoEnable {
                self.obuTrafficInfoEnable = newBoolValue
            }
        } else if attribute == UserSettingAttributes.parkingAvailabilityDisplay {
            let value = self.getValue(code: attributeCode, data: master.masterlists.parkingAvailabilityDisplay)
            let newBoolValue = value == ObuRoadMessage.y.rawValue
            if newBoolValue != self.obuParkingAvailabilityDisplayEnable {
                self.obuParkingAvailabilityDisplayEnable = newBoolValue
            }
        }
        else if attribute == UserSettingAttributes.parkingAvailabilityAudio {
            let value = self.getValue(code: attributeCode, data: master.masterlists.parkingAvailabilityAudio)
            let newBoolValue = value == ObuRoadMessage.y.rawValue
            if newBoolValue != self.obuParkingAvailabilityAudioEnable {
                self.obuParkingAvailabilityAudioEnable = newBoolValue
            }
        } else if attribute == UserSettingAttributes.estimatedTravelTimeDisplay {
            let value = self.getValue(code: attributeCode, data: master.masterlists.estimatedTravelTimeDisplay)
            let newBoolValue = value == ObuRoadMessage.y.rawValue
            if newBoolValue != self.obuEstimatedTravelTimeDisplayEnable {
                self.obuEstimatedTravelTimeDisplayEnable = newBoolValue
            }
        } else if attribute == UserSettingAttributes.estimatedTravelTimeAudio {
            let value = self.getValue(code: attributeCode, data: master.masterlists.estimatedTravelTimeAudio)
            let newBoolValue = value == ObuRoadMessage.y.rawValue
            if newBoolValue != self.obuEstimatedTravelTimeAudioEnable {
                self.obuEstimatedTravelTimeAudioEnable = newBoolValue
            }
        } else if attribute == UserSettingAttributes.thirdPartyEmail {
            // TODO: to be implemented
        } else if attribute == UserSettingAttributes.mkgEmail {
            // TODO: to be implemented
        } else if attribute == UserSettingAttributes.carparkDistance {
            let distance = Int(self.getValue(code: attributeCode, data: master.masterlists.carparkDistance)) ?? Values.carparkMonitorDistanceInLanding // should not happen
            if distance != self.carparkDistance {
                self.carparkDistance = distance
                updated = true
            }
        } else if attribute == UserSettingAttributes.carparkCount {
            let count = Int(self.getValue(code: attributeCode, data: master.masterlists.carparkCount)) ?? Values.maxNoOfCarparksInLanding
            if count != self.carparkCount {
                self.carparkCount = count
                updated = true
            }
        } else if attribute == UserSettingAttributes.carparkAvailability {
            let value = self.getValue(code: attributeCode, data: master.masterlists.carparkAvailability)
            var availability: ParkingType = .all
            if value == CarparkAvailabilityValues.all {
                availability = .all
            } else if value == CarparkAvailabilityValues.type2 {
                availability = .available
            } else if value == CarparkAvailabilityValues.hide {
                availability = .hide
            }
            if availability != self.carparkAvailability {
                self.carparkAvailability = availability
                updated = true
            }
        } else if attribute == UserSettingAttributes.carparkListDistance {
            let distance = Double(self.getValue(code: attributeCode, data: master.masterlists.carparkListDistance)) ?? Values.carparkListDistanceInNavigation
            if distance != self.carparkListDistance {
                self.carparkListDistance = distance
            }
        } else if attribute == UserSettingAttributes.carparkAvailabilityDistance {
            let distance = Double(self.getValue(code: attributeCode, data: master.masterlists.carparkAvailabilityDistance)) ?? Values.carparkAvailabilityDistanceInNavigation
            if distance != self.carparkAvailabilityDistance {
                self.carparkAvailabilityDistance = distance
            }
        } else if attribute == UserSettingAttributes.evPetrolCount {
            let distance = Int(self.getValue(code: attributeCode, data: master.masterlists.evPetrolCount)) ?? Values.evPetrolCount
            if distance != self.evPetrolCount {
                self.evPetrolCount = distance
            }
        } else if attribute == UserSettingAttributes.evPetrolRange {
            let distance = Int(self.getValue(code: attributeCode, data: master.masterlists.evPetrolRange)) ?? Values.evPetrolRange
            if distance != self.evPetrolRange {
                self.evPetrolRange = distance
            }
        } else if attribute == UserSettingAttributes.maxEVPetrolRange {
            let distance = Int(self.getValue(code: attributeCode, data: master.masterlists.maxEVPetrolRange)) ?? Values.maxEVPetrolRange
            if distance != self.maxEVPetrolRange {
                self.maxEVPetrolRange = distance
            }
        } else if attribute == UserSettingAttributes.carparkAvailabilityAlertDisplayTime {
            let time = Int(self.getValue(code: attributeCode, data: master.masterlists.carparkAvailabilityAlertDisplayTime)) ?? Values.carparkAvailabilityAlertDisplayTime
            if time != self.carparkAvailabilityAlertDisplayTime {   
                self.carparkAvailabilityAlertDisplayTime = time
            }
        }
        
        return updated
    }
    
    private func getCode(value: String, data: [MasterProperty]) -> String? {
        for item in data {
            if item.value == value {
                return item.code
            }
        }
        return nil
    }
}

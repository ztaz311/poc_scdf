//
//  ContentElement.swift
//  Breeze
//
//  Created by Zhou Hao on 1/8/22.
//

import Foundation

//MARK: - Update Content V2
struct ContentV2: Codable {
    let contentDetails: [ContentElement]
    enum CodingKeys: String, CodingKey {
        case contentDetails = "content_details"
    }
}

// MARK: - ContentElement
struct ContentElement: Codable {
    let contentID: Int
    let name, layerCode, contentDescription: String
    let sequenceOrder: Int
    let defaultSelection: Bool
    let isNewLayer: Bool
    let imageURLLight, imageURLDark: String?
    let type: String
    let recenterZoneRadius: Double
    let enableWalking: Bool?
    let enableZoneDrive: Bool?
    let canReroute: Bool?
    let contentCategoriesId: Int?
    let trackNavigation: Bool?
    let messageCategory: String?
    let messageType: String?
    let statsIconDark: String?
    let statsIconLight: String?

    enum CodingKeys: String, CodingKey {
        case contentID = "content_id"
        case name
        case layerCode = "layer_code"
        case contentDescription = "description"
        case sequenceOrder = "sequence_order"
        case defaultSelection = "default_selection"
        case isNewLayer = "is_new_layer"
        case imageURLLight = "image_url_light"
        case imageURLDark = "image_url_dark"
        case type
        case recenterZoneRadius = "recentre_zone_radius"
        case enableWalking
        case enableZoneDrive = "enable_zone_drive"
        case canReroute = "can_reroute"
        case contentCategoriesId = "content_categories_id"
        case trackNavigation = "track_navigation"
        case messageCategory = "message_category"
        case messageType = "message_type"
        case statsIconDark = "stats_icon_dark"
        case statsIconLight = "stats_icon_light"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.contentID = try container.decode(Int.self, forKey: .contentID)
        self.name = try container.decodeIfPresent(String.self, forKey: .name) ?? ""
        self.layerCode = try container.decodeIfPresent(String.self, forKey: .layerCode) ?? ""
        self.contentDescription = try container.decodeIfPresent(String.self, forKey: .contentDescription) ?? ""
        self.sequenceOrder = try container.decodeIfPresent(Int.self, forKey: .sequenceOrder) ?? 0
        self.defaultSelection = try container.decodeIfPresent(Bool.self, forKey: .defaultSelection) ?? false
        self.isNewLayer = try container.decodeIfPresent(Bool.self, forKey: .isNewLayer) ?? false
        self.imageURLLight = try container.decodeIfPresent(String.self, forKey: .imageURLLight)
        self.imageURLDark = try container.decodeIfPresent(String.self, forKey: .imageURLDark)
        self.type = try container.decodeIfPresent(String.self, forKey: .type) ?? ""
        self.recenterZoneRadius = try container.decodeIfPresent(Double.self, forKey: .recenterZoneRadius) ?? 0.0
        self.enableWalking = try container.decodeIfPresent(Bool.self, forKey: .enableWalking)
        self.enableZoneDrive = try container.decodeIfPresent(Bool.self, forKey: .enableZoneDrive)
        self.canReroute = try container.decodeIfPresent(Bool.self, forKey: .canReroute)
        self.contentCategoriesId = try container.decodeIfPresent(Int.self, forKey: .contentCategoriesId) ?? 0
        self.trackNavigation = try container.decodeIfPresent(Bool.self, forKey: .trackNavigation)
        self.messageCategory = try container.decodeIfPresent(String.self, forKey: .messageCategory)
        self.messageType = try container.decodeIfPresent(String.self, forKey: .messageType)
        self.statsIconDark = try container.decodeIfPresent(String.self, forKey: .statsIconDark)
        self.statsIconLight = try container.decodeIfPresent(String.self, forKey: .statsIconLight)
    }
}

typealias Contents = [ContentElement]

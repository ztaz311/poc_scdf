//
//  AttachmentFile.swift
//  Breeze
//
//  Created by Zhou Hao on 17/6/21.
//

import Foundation

struct AttachmentFile: Codable {
    let tripId: Int
    let attachmentFile: String  // base64 string
    let fileType: String        // for example: image/jpeg or image/png
    let filePath: String
}

struct TripFileUploadResponse: Codable {
    let tripId: Int?
    let fileNamePath: String?    
}

struct TripFileRetrieveResponse: Codable {
    let tripId: Int?
    let file: String?        // base64 string
}

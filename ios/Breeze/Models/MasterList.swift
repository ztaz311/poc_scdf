//
//  MasterList.swift
//  Breeze
//
//  Created by Zhou Hao on 15/7/22.
//

import Foundation

// MARK: - MasterListModel
struct MasterListData: Codable {
    let masterlists: Masterlists
}

// MARK: - Masterlists
struct Masterlists: Codable {
    let mapDisplay, routePreference, showDemo, carparkDistance, carparkCount, carparkAvailability, bookmarkLimit,collectionLimit,showNudge, showSavedCarparkListView, newUserThresholdDays, trafficInformation, parkingAvailabilityDisplay, parkingAvailabilityAudio, estimatedTravelTimeDisplay, estimatedTravelTimeAudio, schoolZoneDisplayAlert, schoolZoneVoiceAlert, silverZoneDisplayAlert, silverZoneVoiceAlert, busLaneDisplayAlert, busLaneVoiceAlert, vehicleTypes, vehiclePowerTypes, carparkListDistance, carparkAvailabilityDistance, carparkAvailabilityAlertDisplayTime, vehicleFuelGrade, evPetrolCount, evPetrolRange, maxEVPetrolRange: [MasterProperty]

    enum CodingKeys: String, CodingKey {
        case mapDisplay = "MapDisplay"
        case routePreference = "RoutePreference"
        case showDemo = "ShowDemo"
        case carparkDistance = "CarparkDistance"
        case carparkCount = "CarparkCount"
        case carparkAvailability = "CarparkAvailability"
        case bookmarkLimit = "BookmarkLimit"
        case collectionLimit = "CollectionLimit"
        case showNudge = "ShowNudge"
        case showSavedCarparkListView = "ShowSavedCarparkListView"
        case newUserThresholdDays = "NewUserThresholdDays"
        case trafficInformation = "TrafficInformation"
        case parkingAvailabilityDisplay = "ParkingAvailabilityDisplay"
        case parkingAvailabilityAudio = "ParkingAvailabilityAudio"
        case estimatedTravelTimeDisplay = "EstimatedTravelTimeDisplay"
        case estimatedTravelTimeAudio = "EstimatedTravelTimeAudio"
        case schoolZoneDisplayAlert = "SchoolZoneDisplayAlert"
        case schoolZoneVoiceAlert = "SchoolZoneVoiceAlert"
        case silverZoneDisplayAlert = "SilverZoneDisplayAlert"
        case silverZoneVoiceAlert = "SilverZoneVoiceAlert"
        case busLaneDisplayAlert = "BusLaneDisplayAlert"
        case busLaneVoiceAlert = "BusLaneVoiceAlert"
        case vehicleTypes = "vehicleTypes"
        case vehiclePowerTypes = "vehiclePowerTypes"
        case carparkListDistance = "CarparkListDistance"
        case carparkAvailabilityDistance = "CarparkAvailabilityDistance"
        case carparkAvailabilityAlertDisplayTime = "CarparkAvailabilityAlertDisplayTime"
        case vehicleFuelGrade = "vehicleFuelGrade"
        case evPetrolCount = "EVPetrolCount"
        case evPetrolRange = "EVPetrolRange"
        case maxEVPetrolRange = "MaxEVPetrolRange"
    }
}

// MARK: - MasterProperty
struct MasterProperty: Codable {
    let code, value, module: String
    let defaultValue: Default

    enum CodingKeys: String, CodingKey {
        case code, value, module
        case defaultValue = "default"
    }
}

enum Default: String, Codable {
    case n = "N"
    case y = "Y"
}

enum ShowDemo: String, Codable {
    case n = "No"
    case y = "Yes"
}

enum ObuRoadMessage: String, Codable {
    case n = "No"
    case y = "Yes"
}

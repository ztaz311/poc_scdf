//
//  DropPinModel.swift
//  Breeze
//
//  Created by Tuan, Pham Hai on 13/11/2023.
//

import Foundation
class DropPinModel {
    
    var name : String?
    var address : String?
    var lat : String
    var long : String
    var amenityId: String?
    var isSearch: Bool = false
    var address2: String?
    var fullAddress: String?
    
    init(_ name: String?, address: String?, lat: String, long: String, amenityId: String = "", isSearch: Bool = false, address2: String? = "", fullAddress: String? = "") {
        self.name = name
        self.address = address
        self.lat = lat
        self.long = long
        self.amenityId = amenityId
        self.isSearch = isSearch
        self.address2 = address2
        self.fullAddress = fullAddress
    }
}

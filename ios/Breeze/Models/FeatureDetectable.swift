//
//  FeatureDetectable.swift
//  Breeze
//
//  Created by Zhou Hao on 24/5/21.
//

import Foundation
import CoreLocation
import Turf

class FeatureDetectable {
    let id: String
    let distance: Double
    let priority: Int
    
    init(id: String, distance: Double, priority: Int) {
        self.id = id
        self.distance = distance
        self.priority = priority
    }
    
    func type() -> String { return "" }
}

extension FeatureDetectable: Comparable {
    static func <(lhs: FeatureDetectable, rhs: FeatureDetectable) -> Bool {
        if lhs.priority == rhs.priority {
            return lhs.distance < rhs.distance
        } else {
            return lhs.priority < rhs.priority
        }
    }
}

extension FeatureDetectable {
    func isSameClass(as other: FeatureDetectable) -> Bool {
        return other is Self
    }
}

extension FeatureDetectable : Equatable {
    static func ==(lhs: FeatureDetectable, rhs: FeatureDetectable) -> Bool {
        return lhs.id == rhs.id && lhs.isSameClass(as: rhs)
    }
}

// There are a few ways to implement feature detection. The reason I use this way is it will be convinient to extend it in the future if the properties are different
class DetectedSchoolZone: FeatureDetectable {
//    let coordinate: CLLocationCoordinate2D
    let feature: Turf.Feature

    init(id: String, distance: Double, priority: Int, feature: Turf.Feature) {
//        self.coordinate = coordinate
        self.feature = feature
        super.init(id: id, distance: distance, priority: priority)
    }

    override func type() -> String {
        return FeatureDetection.featureTypeSchoolZone
    }
}

class DetectedSilverZone: FeatureDetectable {
//    let coordinate: CLLocationCoordinate2D
    let feature: Turf.Feature

    init(id: String, distance: Double, priority: Int, feature: Turf.Feature) {
//        self.coordinate = coordinate
        self.feature = feature
        super.init(id: id, distance: distance, priority: priority)
    }

    override func type() -> String {
        return FeatureDetection.featureTypeSilverZone
    }
}

class DetectedSpeedCamera: FeatureDetectable {
    
    var category: String = ""
    init(id: String, distance: Double, priority: Int,category:String) {
        
        self.category = category
        super.init(id: id, distance: distance, priority: priority)
    }

    override func type() -> String {
        return category
    }
}

class DetectedERP: FeatureDetectable {
    var address: String = ""
    var price: Double = 0.0
    var zoneId: String = ""
    
    init(id: String, distance: Double, priority: Int, address: String, price: Double, zoneId: String) {
        self.address = address
        self.price = price
        self.zoneId = zoneId
        super.init(id: id, distance: distance, priority: priority)
    }

    override func type() -> String {
        if(id == "36")
        {
            return Values.wayPointPaid
        }
        return FeatureDetection.featureTypeErp
    }
}

extension DetectedERP: CustomStringConvertible {
    var description: String {
        return "\(zoneId)-\(id): \(address),\(price)"
    }
}

class DetectedTrafficIncident: FeatureDetectable {
    var category: String = ""
    var message: String = ""
    
    init(id: String, distance: Double, priority: Int, category: String, message: String) {
        self.category = category
        self.message = message
        super.init(id: id, distance: distance, priority: priority)
    }
    
    override func type() -> String {
        return category
    }
}

class DetectedETANotificationView:FeatureDetectable{
    
    var category: String = ""
    var recipientName:String = ""
    var message: String = ""
    
    init(id: String, distance: Double, priority: Int, category: String, message: String, rName:String) {
        self.category = category
        self.message = message
        self.recipientName = rName
        super.init(id: id, distance: distance, priority: priority)
    }
    
    override func type() -> String {
        return category
    }
}

class DetectedDemoRoadObject: FeatureDetectable {
    var category: String = ""
    var message: String = ""
    
    init(id: String, distance: Double, priority: Int, category: String, message: String) {
        self.category = category
        self.message = message
        super.init(id: id, distance: distance, priority: priority)
    }
    
    override func type() -> String {
        return category
    }
}

class DetectedSeasonParking: FeatureDetectable {
    override func type() -> String {
        return FeatureDetection.featureTypeSeasonParking
    }
}

// MARK: - Feature detection constants
struct FeatureDetection {
    static let accidentRange: CLLocationDistance = 1000
    static let range: CLLocationDistance = 300
    static let majorRoadObject3kmDistance: CLLocationDistance = 3000
    static let majorRoadObject1kmDistance: CLLocationDistance = 1000
    static let majorRoadObjectShortDistance: CLLocationDistance = 300
    static let roadObjectCCDFlashFlood: CLLocationDistance = 20 // closest coordinate distance from line string for Flash Flood
    static let roadObjectCCDOthers: CLLocationDistance = 3 // closest coordinate distance from line string for Major Accident and Road Closure
    
    // Cruise mode priority
    static let cruisePriorityErp = 100
    static let cruisePriorityTrafficFlashFlood = 81
    static let cruisePriorityTrafficMajorAccident = 80
    static let cruisePrioritySpeedCamera = 75
    static let cruisePrioritySilverZone = 73
    static let cruisePrioritySchoolZone = 72
    static let cruisePriorityTraffic = 60
    static let cruisePriorityTrafficRoadClosure = 50
    static let cruisePriorityTrafficHeavyTraffic = 45
    static let cruisePrioritySeasonParking = 40
    
    // Feature type
    static let featureTypeErp = "ERP"
    static let featureTypeSchoolZone = "School Zone"
    static let featureTypeSilverZone = "Silver Zone"
    static let featureTypeSpeedcamera = "Speed Camera"
    static let featureTypeTraffic = "Traffic"
    static let featureTypeDemo = "Demo"
    static let featureTypeSeasonParking = "Season Parking Zone"
}

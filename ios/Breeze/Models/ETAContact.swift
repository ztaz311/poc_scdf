//
//  ETAContact.swift
//  Breeze
//
//  Created by Zhou Hao on 14/7/21.
//

import Foundation

struct ETAContact {
    let name: String
    let phone: String
}

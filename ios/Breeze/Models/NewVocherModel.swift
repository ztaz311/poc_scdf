//
//  NewVocherModel.swift
//  Breeze
//
//  Created by Zhou Hao on 5/9/22.
//

import Foundation

struct NewVocherModel: Codable {
    let voucherAdded: Bool
    
    enum CodingKeys: String, CodingKey {
        case voucherAdded = "voucherAdded"
    }
}

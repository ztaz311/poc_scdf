//
//  Carpark.swift
//  Breeze
//
//  Created by Zhou Hao on 10/5/21.
//

import Foundation
import CoreLocation

class AvailabilityCSData: Codable {
    
    let updateTS: Double?
    let title: String?
    let desc: String?
    let themeColor: String?
    let primaryDesc: String?
    let secondaryDesc: String?
    
    enum CodingKeys: String, CodingKey {
        case updateTS = "updateTS"
        case title = "title"
        case desc = "desc"
        case themeColor = "themeColor"
        case primaryDesc = "primaryDesc"
        case secondaryDesc = "secondaryDesc"

    }
    
    func getUpdateTSDescDisplay() -> String {
        return "\(primaryDesc ?? ""), \(getSecondaryDesc())"
    }
    
    private func getSecondaryDesc() -> String {
        if let timestamp = updateTS {
            let duration = Date().timeIntervalSince1970 - timestamp
            if duration > 0  {
                var mins = duration / 60
                if mins == 0 {
                    mins = 1
                }
                return "\(mins) mins ago"
            } else {
                return secondaryDesc ?? ""
            }
        } else {
            return secondaryDesc ?? ""
        }
    }
    
    init(csDict: [String: Any]) {
        self.updateTS = csDict["updateTS"] as? Double ?? nil
        self.title = csDict["title"] as? String ?? ""
        self.desc = csDict["desc"] as? String ?? ""
        self.themeColor = csDict["themeColor"] as? String ?? ""
        self.primaryDesc = csDict["primaryDesc"] as? String ?? ""
        self.secondaryDesc = csDict["secondaryDesc"] as? String ?? ""
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.updateTS = try container.decodeIfPresent(Double.self, forKey: .updateTS)
        self.title = try container.decodeIfPresent(String.self, forKey: .title)
        self.desc = try container.decodeIfPresent(String.self, forKey: .desc)
        self.themeColor = try container.decodeIfPresent(String.self, forKey: .themeColor)
        self.primaryDesc = try container.decodeIfPresent(String.self, forKey: .primaryDesc)
        self.secondaryDesc = try container.decodeIfPresent(String.self, forKey: .secondaryDesc)
    }
    
}

class SearchCarpark: Codable {
    let success: Bool?
    let data: Carpark?
    
    enum CodingKeys: String, CodingKey {
        case success = "success"
        case data = "data"
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.success = try container.decodeIfPresent(Bool.self, forKey: .success)
        self.data = try container.decodeIfPresent(Carpark.self, forKey: .data)
    }
}

class AttributesCarparkModel: Codable {
    let success: Bool?
    let data: AttributesCarpark?    
    
    enum CodingKeys: String, CodingKey {
        case success = "success"
        case data = "data"
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.success = try container.decodeIfPresent(Bool.self, forKey: .success)
        self.data = try container.decodeIfPresent(AttributesCarpark.self, forKey: .data)
    }
}

class AttributesCarpark: Codable {
    
    let id: String?
    let name: String?
    let availablePercentImageBand: String?
    let alternateCarparks: [Carpark]?
    let availablelot: Int?
    let hasAvailabilityCS: Bool?
    let navVoiceAlert: String?
    
    func getAlternateCarpark() -> Carpark? {
        return alternateCarparks?.first
    }
    
    func isValidParkingLots() -> Bool {
        if let lots = availablelot {
            if lots < 0 {
                return false
            } else {
                return true
            }
        } else {
            return false
        }
    }
    
    func getParkingStatus() -> ParkingAvailabilityStatus {
        var retStatus: ParkingAvailabilityStatus = .notAvailable
        if let available = availablePercentImageBand {
            switch available {
            case "TYPE0":
                retStatus = .fullParking
            case "TYPE1":
                retStatus = .fewAvailableLots
            case "TYPE2":
                retStatus = .availableLots
            default:
                break
            }
        }
        return retStatus
    }
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case availablePercentImageBand = "availablePercentImageBand"
        case alternateCarparks = "alternateCarparks"
        case availablelot = "availablelot"
        case hasAvailabilityCS = "hasAvailabilityCS"
        case navVoiceAlert = "navVoiceAlert"
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decodeIfPresent(String.self, forKey: .id)
        self.name = try container.decodeIfPresent(String.self, forKey: .name)
        self.availablePercentImageBand = try container.decodeIfPresent(String.self, forKey: .availablePercentImageBand)
        self.alternateCarparks = try container.decodeIfPresent([Carpark].self, forKey: .alternateCarparks)
        self.availablelot = try container.decodeIfPresent(Int.self, forKey: .availablelot)
        self.hasAvailabilityCS = try container.decodeIfPresent(Bool.self, forKey: .hasAvailabilityCS)
        self.navVoiceAlert = try container.decodeIfPresent(String.self, forKey: .navVoiceAlert)
        
    }
}

class Carpark : Codable {
    
    // MARK: Constants
    static let typeSeason = "SEASON_PARKING"
    static let typeShortTerm = "SHORT_TERM_PARKING"
    static let typePartialSeason = "PARTIAL_SEASON_PARKING"
    static let typeCustomer = "CUSTOMER_PARKING"
    
    let id: String?
    let name : String?
    let parkingSystem: String?
    let capacity: ValueWrapper?
    let lat : Double?
    let long : Double?
    let category: String?
    let source: String?
    let distance:Int?
    let availablelot: ValueWrapper?
    let currentHrRate: CarParkHourRate?
    let nextHrRate: CarParkHourRate?
    let isCheapest: Bool?
    let destinationCarPark: Bool?
    let parkingType: String?
    let hasRatesInfo: Bool?
    let availablePercent: Double?
    let availablePercentImageBand: String?
    private let hasVouchers: Bool?
    private let voucherAmount: Double?
    
    var bookmarkId: Int?
    var isBookmarked: Bool?
    
    //  Carpark availability
    let showAvailabilityFB: Bool?
    let hasAvailabilityCS: Bool?
    let availabilityCSData: AvailabilityCSData?
    
    var selected: Bool = false
    
    var hasVoucher: Bool {
        return hasVouchers ?? false
    }
    
    var amount: Double {
        return voucherAmount ?? 0
    }
    
    var currentParkingStatus: ParkingAvailabilityStatus {
              
        switch availablePercentImageBand {
        case "TYPE0":
            return .fullParking
        case "TYPE1":
            return .fewAvailableLots
        case "TYPE2":
            return .availableLots
        default:
            return .notAvailable
        }
        
        /*
        if let hasRate = self.hasRatesInfo, hasRate {
            if availablePercent == nil || (availablePercent != nil && availablePercent == -1) {
                return .availableLots
            }else {
                let availablePercentage = self.availablePercentImageBand
                if availablePercentage == "TYPE0" {
                    return .fullParking
                } else if availablePercentage == "TYPE1" {
                    return .fewAvailableLots
                }else {
                    return .availableLots
                }
            }
        }else {
            if availablePercent == nil || (availablePercent != nil && availablePercent == -1) {
                return .notAvailable
            }else {
                let availablePercentage = self.availablePercentImageBand
                if availablePercentage == "TYPE0" {
                    return .fullParking
                } else if availablePercentage == "TYPE1" {
                    return .fewAvailableLots
                }else {
                    return .availableLots
                }
            }
        }*/
    }
    
    func getThemeColor() -> String {
        var bgColorHexString: String = ""
        if let csData = availabilityCSData {
            return csData.themeColor ?? ""
        } else {
            switch self.currentParkingStatus {
            case .availableLots:
                bgColorHexString = "933DD8"
            case .fewAvailableLots:
                bgColorHexString = "F26415"
            case .fullParking:
                bgColorHexString = "E82370"
            case .notAvailable:
                print("Do nothing")
            }
        }
        
        return bgColorHexString
    }
    
    func getCSUpdatedTimeDesc() -> String {
        if let csData = availabilityCSData {
            return csData.desc ?? ""
        }
        
        return ""
    }
    
    func getParkingAlertMessage() -> String {
        switch currentParkingStatus {
        case .availableLots:
            return "Available"
        case .fewAvailableLots:
            return "Filling Up Fast"
        case .fullParking:
            return "Carpark Full"
        default:
            return ""
        }
    }
    
    func getCSTitle() -> String {
        
        if let csData = availabilityCSData {
            return csData.title ?? ""
        }else {
            switch currentParkingStatus {
            case .availableLots:
                return Constants.manyAvailableLots
            case .fewAvailableLots:
                return Constants.fewAvailableLots
            case .fullParking:
                return Constants.carparkFull
            default:
                return ""
            }
        }
    }
    
    func shouldAskForFeedback() -> Bool {
        return showAvailabilityFB ?? false
    }
    
    func needShowYesNoQuestion() -> Bool {
        return hasAvailabilityCS ?? false
    }

    func getLocation(_ routeablePoint: RoutablePointModel?) -> CLLocationCoordinate2D {
        var location: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: lat ?? 0, longitude: long ?? 0)
        //  Make sure that routeable point is associated with this carpark
        if let point = routeablePoint, routeablePoint?.carparkId == id {
            location = CLLocationCoordinate2D(latitude: point.lat ?? 0, longitude: point.long ?? 0)
        }
        return location
    }
    
    func getRouteablePointName(_ routeablePoint: RoutablePointModel?) -> String? {
        if let point = routeablePoint, routeablePoint?.carparkId == id {
            return point.name
        }
        return nil
    }

    enum CodingKeys: String, CodingKey {
        
        case id = "id"
        case name = "name"
        case parkingSystem = "parkingSystem"
        case capacity = "capacity"
        case category = "category"
        case lat = "lat"
        case long = "long"
        case source = "source"
        case distance = "distance"
        case availablelot = "availablelot"
        case currentHrRate = "currentHrRate"
        case nextHrRate = "nextHrRate"
        case isCheapest = "isCheapest"
        case destinationCarPark = "destinationCarPark"
        case parkingType = "parkingType"
        case hasRatesInfo = "hasRatesInfo"
        case availablePercent = "availablePercent"
        case availablePercentImageBand = "availablePercentImageBand"
        case hasVouchers = "hasVouchers"
        case voucherAmount = "voucherAmount"
        case bookmarkId, isBookmarked, showAvailabilityFB, hasAvailabilityCS, availabilityCSData
    }
    
    init(id: String, name: String, parkingSystem: String, capacity: ValueWrapper, category: String, lat:Double, long:Double, source: String, availableLots: ValueWrapper?,distance:Int, currentHrRate: CarParkHourRate, nextHrRate: CarParkHourRate, isCheapest: Bool, destinationCarPark: Bool, parkingType: String, hasRatesInfo: Bool, availablePercent: Double, availablePercentImageBand: String, hasVouchers: Bool? = false, voucherAmount: Double = 0, bookmarkId: Int = -1, isBookmarked: Bool = false, showAvailabilityFB: Bool? = false, hasAvailabilityCS: Bool? = false, availabilityCSData: AvailabilityCSData?) {
        self.id = id
        self.name = name
        self.parkingSystem = parkingSystem
        self.capacity = capacity
        self.category = category
        self.lat = lat
        self.long = long
        self.source = source
        self.availablelot = availableLots
        self.distance = distance
        self.currentHrRate = currentHrRate
        self.nextHrRate = nextHrRate
        self.isCheapest = isCheapest
        self.destinationCarPark = destinationCarPark
        self.parkingType = parkingType
        self.hasRatesInfo = hasRatesInfo
        self.availablePercent = availablePercent
        self.availablePercentImageBand = availablePercentImageBand
        self.hasVouchers = hasVouchers
        self.voucherAmount = voucherAmount
        self.isBookmarked = isBookmarked
        self.bookmarkId = bookmarkId
        self.showAvailabilityFB = showAvailabilityFB
        self.hasAvailabilityCS = hasAvailabilityCS
        self.availabilityCSData = availabilityCSData
    }

    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(String.self, forKey: .id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        parkingSystem = try values.decodeIfPresent(String.self, forKey: .parkingSystem)
        capacity = try values.decodeIfPresent(ValueWrapper.self, forKey: .capacity)
        category = try values.decodeIfPresent(String.self, forKey: .category)
        lat = try values.decodeIfPresent(Double.self, forKey: .lat)
        long = try values.decodeIfPresent(Double.self, forKey: .long)
        source = try values.decodeIfPresent(String.self, forKey: .source)
        availablelot = try values.decodeIfPresent(ValueWrapper.self, forKey: .availablelot)
        distance = try values.decodeIfPresent(Int.self, forKey: .distance)
        currentHrRate = try values.decodeIfPresent(CarParkHourRate.self, forKey: .currentHrRate)
        nextHrRate = try values.decodeIfPresent(CarParkHourRate.self, forKey: .nextHrRate)
        isCheapest = try values.decodeIfPresent(Bool.self, forKey: .isCheapest)
        destinationCarPark = try values.decodeIfPresent(Bool.self, forKey: .destinationCarPark)
        parkingType = try values.decodeIfPresent(String.self, forKey: .parkingType)
        hasRatesInfo = try values.decodeIfPresent(Bool.self, forKey: .hasRatesInfo)
        availablePercent = try values.decodeIfPresent(Double.self, forKey: .availablePercent)
        availablePercentImageBand = try values.decodeIfPresent(String.self, forKey: .availablePercentImageBand)
        hasVouchers = try values.decodeIfPresent(Bool.self, forKey: .hasVouchers)
        voucherAmount = try values.decodeIfPresent(Double.self, forKey: .voucherAmount)
        bookmarkId = try values.decodeIfPresent(Int.self, forKey: .bookmarkId)
        isBookmarked = try values.decodeIfPresent(Bool.self, forKey: .isBookmarked)
        showAvailabilityFB = try values.decodeIfPresent(Bool.self, forKey: .showAvailabilityFB)
        hasAvailabilityCS = try values.decodeIfPresent(Bool.self, forKey: .hasAvailabilityCS)
        availabilityCSData = try values.decodeIfPresent(AvailabilityCSData.self, forKey: .availabilityCSData)
    }
}

struct CarParkHourRate : Codable{
    let start_time : String?
    let end_time : String?
    let oneHrRate: Double?

    enum CodingKeys: String, CodingKey {

        case start_time = "start_time"
        case end_time = "end_time"
        case oneHrRate = "oneHrRate"
    }
    
    init(start_time: String, end_time: String, oneHrRate: Double) {
        self.start_time = start_time
        self.end_time = end_time
        self.oneHrRate  = oneHrRate
        
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        start_time = try values.decodeIfPresent(String.self, forKey: .start_time)
        end_time = try values.decodeIfPresent(String.self, forKey: .end_time)
        oneHrRate = try values.decodeIfPresent(Double.self, forKey: .oneHrRate)
    }
}


struct CarParkBase : Codable {
    
    let amenities : [AmenitiesCarPark]?

    enum CodingKeys: String, CodingKey {
        case amenities = "amenities"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        amenities = try values.decodeIfPresent([AmenitiesCarPark].self, forKey: .amenities)
    }

}

struct AmenitiesCarPark : Codable {
    let type : String?
    let icon_url : String?
    let data : [Carpark]?

    enum CodingKeys: String, CodingKey {

        case type = "type"
        case icon_url = "icon_url"
        case data = "data"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        type = try values.decodeIfPresent(String.self, forKey: .type)
        icon_url = try values.decodeIfPresent(String.self, forKey: .icon_url)
        data = try values.decodeIfPresent([Carpark].self, forKey: .data)
    }

}


enum ValueWrapper: Codable {
    case stringValue(String)
    case intValue(Int)
    case doubleValue(Double)
    case boolValue(Bool)

    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if let value = try? container.decode(String.self) {
            self = .stringValue(value)
            return
        }
        if let value = try? container.decode(Bool.self) {
            self = .boolValue(value)
            return
        }
        if let value = try? container.decode(Double.self) {
            self = .doubleValue(value)
            return
        }
        if let value = try? container.decode(Int.self) {
            self = .intValue(value)
            return
        }

        throw DecodingError.typeMismatch(ValueWrapper.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for ValueWrapper"))
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        switch self {
        case let .stringValue(value):
            try container.encode(value)
        case let .boolValue(value):
            try container.encode(value)
        case let .intValue(value):
            try container.encode(value)
        case let .doubleValue(value):
            try container.encode(value)
        }
    }

    var rawValue: String {
        var result: String
        switch self {
        case let .stringValue(value):
            result = value
        case let .boolValue(value):
            result = String(value)
        case let .intValue(value):
            result = String(value)
        case let .doubleValue(value):
            result = String(value)
        }
        return result
    }

    var intValue: Int? {
        var result: Int?
        switch self {
        case let .stringValue(value):
            result = Int(value)
        case let .intValue(value):
            result = value
        case let .boolValue(value):
            result = value ? 1 : 0
        case let .doubleValue(value):
            result = Int(value)
        }
        return result
    }

    var boolValue: Bool? {
        var result: Bool?
        switch self {
        case let .stringValue(value):
            result = Bool(value)
        case let .boolValue(value):
            result = value
        case let .intValue(value):
            result = Bool(truncating: value as NSNumber)
        case let .doubleValue(value):
            result = Bool(truncating: value as NSNumber)
        }
        return result
    }
}

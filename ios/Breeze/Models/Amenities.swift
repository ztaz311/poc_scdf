//
//  Amenities.swift
//  Breeze
//
//  Created by Zhou Hao on 29/11/21.
//

import Foundation
import MapboxMaps

// request
struct AmenityFilter: Codable {
    let provider: [String]?
    let services: [String]?
    let plugTypes: [String]?
    var category: String? = nil
    var availableBand: String? = nil
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(self.provider, forKey: .provider)
        try container.encodeIfPresent(self.services, forKey: .services)
        try container.encodeIfPresent(self.plugTypes, forKey: .plugTypes)
        try container.encodeIfPresent(self.category, forKey: .category)
        try container.encodeIfPresent(self.availableBand, forKey: .availableBand)
    }
}

struct AmenityRequest: Codable {
    let type: String
    let filter: AmenityFilter?
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(self.type, forKey: .type)
        try container.encodeIfPresent(self.filter, forKey: .filter)
    }
}

struct AmenitiesRequest: Codable {
    let latitude: Double
    let longitude: Double
    let radius: Double
    let arrivalTime: Double
    let destinationName: String
    let amenities: [AmenityRequest]
    let resultCount:Int?
    let maxRadius:Double?
    let isVoucher: Bool?
    let carparkId: String?
    
    enum CodingKeys: String, CodingKey {
        case latitude = "lat"
        case longitude = "lng"
        case radius = "rad"
        case arrivalTime = "arrivalTime"
        case destinationName = "destName"
        case amenities = "amenities"
        case resultCount = "resultcount"
        case maxRadius = "maxRad"
        case isVoucher = "isVoucher"
        case carparkId = "carparkId"
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(self.latitude, forKey: .latitude)
        try container.encodeIfPresent(self.longitude, forKey: .longitude)
        try container.encodeIfPresent(self.radius, forKey: .radius)
        try container.encodeIfPresent(self.arrivalTime, forKey: .arrivalTime)
        try container.encodeIfPresent(self.destinationName, forKey: .destinationName)
        try container.encodeIfPresent(self.amenities, forKey: .amenities)
        try container.encodeIfPresent(self.resultCount, forKey: .resultCount)
        try container.encodeIfPresent(self.maxRadius, forKey: .maxRadius)
        try container.encodeIfPresent(self.isVoucher, forKey: .isVoucher)
        try container.encodeIfPresent(self.carparkId, forKey: .carparkId)
    }
}

// response
/// Currently we only need id, lat, long and distance
struct Amenity: Codable {
    
    struct Location: Codable {
        let type: String
        let coordinates: [Double]
    }
    
    let id: String
//    let provider: String
//    let stationName: String
//    let address: String
//    let telNo: String?
//    let services: [String]?
//    let pin: String?
    let lat: Double
    let long: Double
//    let location: Location
    let distance: Double
}

struct Amenities: Codable {
    let petrol: [Amenity]?
    let evcharger: [Amenity]?
    let library: [Amenity]?
    let hawkercentre: [Amenity]?
    let parks: [Amenity]?
    let carpark: [Amenity]?
    let museum: [Amenity]?
    let heritagetree: [Amenity]?
    let erp: [Amenity]?
    let pcn: [Amenity]?
    let walkingtrail: [Amenity]?
    let cafe: [Amenity]?
    let sports: [Amenity]?
    let restaurant: [Amenity]?
    let playground: [Amenity]?
    
    // Use CodingKeys as the keys for UserDefault/Map layer id also
    enum CodingKeys: String, CodingKey {
        case petrol = "petrol"
        case evcharger = "evcharger"
        case library = "library"
        case hawkercentre = "hawkercentre"
        case parks = "parks"
        case carpark = "carpark"
        case museum = "museum"
        case heritagetree = "heritagetree"
        case erp = "erp"
        case pcn = "pcn"
        case walkingtrail = "walkingtrail"
        case cafe = "cafe"
        case sports = "sports"
        case restaurant = "restaurant"
        case playground = "playground"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.petrol = try container.decodeIfPresent([Amenity].self, forKey: .petrol)
        self.evcharger = try container.decodeIfPresent([Amenity].self, forKey: .evcharger)
        self.library = try container.decodeIfPresent([Amenity].self, forKey: .library)
        self.hawkercentre = try container.decodeIfPresent([Amenity].self, forKey: .hawkercentre)
        self.parks = try container.decodeIfPresent([Amenity].self, forKey: .parks)
        self.carpark = try container.decodeIfPresent([Amenity].self, forKey: .carpark)
        self.museum = try container.decodeIfPresent([Amenity].self, forKey: .museum)
        self.heritagetree = try container.decodeIfPresent([Amenity].self, forKey: .heritagetree)
        self.erp = try container.decodeIfPresent([Amenity].self, forKey: .erp)
        self.pcn = try container.decodeIfPresent([Amenity].self, forKey: .pcn)
        self.walkingtrail = try container.decodeIfPresent([Amenity].self, forKey: .walkingtrail)
        self.cafe = try container.decodeIfPresent([Amenity].self, forKey: .cafe)
        self.sports = try container.decodeIfPresent([Amenity].self, forKey: .sports)
        self.restaurant = try container.decodeIfPresent([Amenity].self, forKey: .restaurant)
        self.playground = try container.decodeIfPresent([Amenity].self, forKey: .playground)
    }
}

//MARK: - Amenitiesv2

struct Amenitiesv2 : Codable {
    let type : String?
    let icon_url : String?
    let data : [Amenitiyv2Data]?

    enum CodingKeys: String, CodingKey {

        case type = "type"
        case icon_url = "icon_url"
        case data = "data"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        type = try values.decodeIfPresent(String.self, forKey: .type)
        icon_url = try values.decodeIfPresent(String.self, forKey: .icon_url)
        data = try values.decodeIfPresent([Amenitiyv2Data].self, forKey: .data)
    }

}

struct Amenitiyv2Data : Codable {
    let _id : String?
    let name : String?
    let altitudemode : String?
    let x_addr : String?
    let y_addr : String?
    let fmel_upd_d : String?
    let shape : String?
    let inc_crc : String?
    let field_1 : String?
//    let long : Double?
//    let lat : Double?
    let provider:String?
    let stationname:String?
    let id : String?
    let location : AmenitiesLocation?
    let additionalInfo : AdditionalInfoModel?
    let plugTypes: [PlugType]?
    let address : String?
    let distance : Int?
    let startDate: Int?
    let endDate: Int?
    let imageType:String?
    let bookmarkId: Int?
    let isBookmarked: Bool?
    
    func getTypesDisplay() -> String {
        if let plugTypes = self.plugTypes {
            var types: [String] = []
            for type in plugTypes {
                if let name = type.name, !name.isEmpty {
                    types.append(name)
                }
            }
            if !types.isEmpty {
                return types.joined(separator: ",")
            }
        }
        return ""
    }

    enum CodingKeys: String, CodingKey {

        case _id = "_id"
        case name = "name"
        case altitudemode = "altitudemode"
        case x_addr = "x_addr"
        case y_addr = "y_addr"
        case fmel_upd_d = "fmel_upd_d"
        case shape = "shape"
        case inc_crc = "inc_crc"
        case field_1 = "field_1"
//        case long = "long"
//        case lat = "lat"
        case id = "id"
        case location = "location"
        case additionalInfo = "additionalInfo"
        case address = "address"
        case distance = "distance"
        case provider = "provider"
        case stationname = "stationName"
        case plugTypes = "plugTypes"
        case startDate = "startDate"
        case endDate = "endDate"
        case imageType = "imageType"
        case bookmarkId, isBookmarked
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        _id = try values.decodeIfPresent(String.self, forKey: ._id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        altitudemode = try values.decodeIfPresent(String.self, forKey: .altitudemode)
        x_addr = try values.decodeIfPresent(String.self, forKey: .x_addr)
        y_addr = try values.decodeIfPresent(String.self, forKey: .y_addr)
        fmel_upd_d = try values.decodeIfPresent(String.self, forKey: .fmel_upd_d)
        shape = try values.decodeIfPresent(String.self, forKey: .shape)
        inc_crc = try values.decodeIfPresent(String.self, forKey: .inc_crc)
        field_1 = try values.decodeIfPresent(String.self, forKey: .field_1)
//        long = try values.decodeIfPresent(Double.self, forKey: .long)
//        lat = try values.decodeIfPresent(Double.self, forKey: .lat)
        id = try values.decodeIfPresent(String.self, forKey: .id)
        location = try values.decodeIfPresent(AmenitiesLocation.self, forKey: .location)
        additionalInfo = try values.decodeIfPresent(AdditionalInfoModel.self, forKey: .additionalInfo)
        address = try values.decodeIfPresent(String.self, forKey: .address)
        distance = try values.decodeIfPresent(Int.self, forKey: .distance)
        provider = try values.decodeIfPresent(String.self, forKey: .provider)
        stationname = try values.decodeIfPresent(String.self, forKey: .stationname)
        plugTypes = try values.decodeIfPresent([PlugType].self, forKey: .plugTypes)
        startDate = try values.decodeIfPresent(Int.self, forKey: .startDate)
        endDate = try values.decodeIfPresent(Int.self, forKey: .endDate)
        imageType = try values.decodeIfPresent(String.self, forKey: .imageType)
        bookmarkId = try values.decodeIfPresent(Int.self, forKey: .bookmarkId)
        isBookmarked = try values.decodeIfPresent(Bool.self, forKey: .isBookmarked)
    }

}

struct AmenitiesBase : Codable {
    let amenities : [Amenitiesv2]?

    enum CodingKeys: String, CodingKey {
        case amenities = "amenities"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        amenities = try values.decodeIfPresent([Amenitiesv2].self, forKey: .amenities)
    }

}

struct AmenitiesLocation : Codable {
    let type : String?
    let coordinates : [Double]?

    enum CodingKeys: String, CodingKey {

        case type = "type"
        case coordinates = "coordinates"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        type = try values.decodeIfPresent(String.self, forKey: .type)
        coordinates = try values.decodeIfPresent([Double].self, forKey: .coordinates)
    }

}


struct AdditionalInfoModel : Codable {
    let text : String?
    let style : StyleColorModel?

    enum CodingKeys: String, CodingKey {
        case text = "text"
        case style = "style"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        text = try values.decodeIfPresent(String.self, forKey: .text)
        style = try values.decodeIfPresent(StyleColorModel.self, forKey: .style)
    }
}

struct StyleColorModel : Codable {
    let lightColor: ColorModel?
    let darktColor: ColorModel?
    
    enum CodingKeys: String, CodingKey {
        case lightColor = "light"
        case darktColor = "dark"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        lightColor = try values.decodeIfPresent(ColorModel.self, forKey: .lightColor)
        darktColor = try values.decodeIfPresent(ColorModel.self, forKey: .darktColor)
    }
}

struct ColorModel : Codable {
    let color : String?
    enum CodingKeys: String, CodingKey {
        case color = "color"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        color = try values.decodeIfPresent(String.self, forKey: .color)
    }
}

struct PlugType: Codable {
    
    let name: String?
    let capacity: String?
    //let price: [Price]?
    let plugLots: Int?
    
    enum CodingKeys: String, CodingKey {

        case name = "name"
        case capacity = "capacity"
        case plugLots = "plugLots"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        capacity = try values.decodeIfPresent(String.self, forKey: .capacity)
        plugLots = try values.decodeIfPresent(Int.self, forKey: .plugLots)
        //coordinates = try values.decodeIfPresent([Double].self, forKey: .coordinates)
    }
}

struct AmenitiesSymbolLayer : Codable {
    let type : String?
    let imageURL: String?
    let lat : Double?
    let long:Double?

    enum CodingKeys: String, CodingKey {

        case type = "type"
        case lat = "lat"
        case long = "long"
        case imageURL = "mapImageURL"
    }


}

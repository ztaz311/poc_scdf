//
//  PairingErrorViewController.swift
//  Breeze
//
//  Created by Tan, Tran Ngoc on 06/06/2023.
//

import UIKit

class PairingErrorViewController: BaseViewController {

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var titleTextView: UITextView!
    @IBOutlet weak var titleLabel2: UILabel!
    @IBOutlet weak var registerMacButton: UIButton!
    @IBOutlet weak var titleLabel3: UILabel!
    @IBOutlet weak var titleLabel4: UILabel!
    @IBOutlet weak var titleLabel5: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupDarkLightAppearance(forceLightMode: true)
    }

    func setupUI() {
        contentView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        contentView.layer.cornerRadius = 30.0
        contentView.layer.masksToBounds = true
        titleLabel.font = UIFont.rerouteMessageBoldFont()
        titleLabel.textColor = UIColor(hexString: "#222638", alpha: 1.0)
        titleLabel.text = "Check the following:"
        titleTextView.font = UIFont.rerouteTitleRegularFont()
        titleTextView.textColor = UIColor(hexString: "#222638", alpha: 1.0)
        let inputText = "Registered your device at erp.lta.gov.sg/ Manage Paired Device for OBU at least 1 day before"
        let fontSize: CGFloat = 18.0
        let formattedAttributedString = formatTextWithClickableURL(inputText, fontSize: fontSize)
        titleTextView.attributedText = formattedAttributedString
        titleTextView.isEditable = false
        titleTextView.isSelectable = true
        titleTextView.dataDetectorTypes = .link
        titleLabel2.font = UIFont.rerouteTitleRegularFont()
        titleLabel2.textColor = UIColor(hexString: "#222638", alpha: 1.0)
        titleLabel2.text = "Started your car and enabled bluetooth"
        titleLabel3.font = UIFont.rerouteTitleRegularFont()
        titleLabel3.textColor = UIColor(hexString: "#222638", alpha: 1.0)
        titleLabel3.text = "Your OBU is within 10 metres of your device"
        titleLabel4.font = UIFont.rerouteTitleRegularFont()
        titleLabel4.textColor = UIColor(hexString: "#222638", alpha: 1.0)
        titleLabel4.text = "Mobile data is turned on to enable app authentication"
        titleLabel5.font = UIFont.rerouteTitleRegularFont()
        titleLabel5.textColor = UIColor(hexString: "#222638", alpha: 1.0)
        titleLabel5.text = "Another device is not already connected to the OBU"
        registerMacButton.layer.cornerRadius = 24
        registerMacButton.layer.borderWidth = 1
        registerMacButton.layer.borderColor = UIColor(named:"tooltipsAmenityNaviBtnColor")?.cgColor ?? UIColor.purple.cgColor
        registerMacButton.setTitle("Register Mac address with LTA", for: .normal)
        registerMacButton.titleLabel?.font = UIFont.rerouteMessageBoldFont()
        registerMacButton.setTitleColor(UIColor(hexString: "#782EB1", alpha: 1.0), for: .normal)
    }
    
    @IBAction func closeAction(_ sender: Any) {
        self.view.removeFromSuperview()
    }
    
    @IBAction func registerAction(_ sender: Any) {
        if let url = URL(string: "https://datamall.lta.gov.sg/content/datamall/en.html") {
            UIApplication.shared.open(url)
        }
    }
    
    func formatTextWithClickableURL(_ text: String, fontSize: CGFloat) -> NSAttributedString {
        let attributedString = NSMutableAttributedString(string: text)
        
        if let range = text.range(of: "erp.lta.gov.sg/") {
            let nsRange = NSRange(range, in: text)
            
            attributedString.addAttribute(.link, value: "http://erp.lta.gov.sg/", range: nsRange)
            attributedString.addAttribute(.foregroundColor, value: UIColor.blue, range: nsRange)
            attributedString.addAttribute(.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: nsRange)
        }
        
        let attributes: [NSAttributedString.Key: Any] = [.font: UIFont.rerouteTitleRegularFont()]
        attributedString.addAttributes(attributes, range: NSRange(location: 0, length: attributedString.length))
        
        return attributedString
    }
}

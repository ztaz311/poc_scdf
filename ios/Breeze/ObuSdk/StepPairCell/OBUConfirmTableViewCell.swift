//
//  OBUConfirmTableViewCell.swift
//  Breeze
//
//  Created by Tan, Tran Ngoc on 01/06/2023.
//

import UIKit


class OBUConfirmTableViewCell: UITableViewCell {
    
    static let identifier = "OBUConfirmTableViewCell"
    
    
    @IBOutlet weak var numberObuLabel: UILabel!
    @IBOutlet weak var tickGreenIcon: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func setupUI(title: String, isSelected: Bool) {
        numberObuLabel.textColor = UIColor(hexString: "#222638", alpha: 1.0)
        numberObuLabel.text = title
        numberObuLabel.font = UIFont.SFProRegularFont()
        if isSelected == true {
            tickGreenIcon.isHidden = false
        } else {
            tickGreenIcon.isHidden = true
        }
        
    }
    
}

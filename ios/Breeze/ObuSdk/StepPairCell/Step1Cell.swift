//
//  Step1Cell.swift
//  Breeze
//
//  Created by Tan, Tran Ngoc on 31/05/2023.
//

import UIKit

class Step1Cell: UICollectionViewCell {
    
    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()

    }
    
    func setupUI() {
        iconImage.image = UIImage(named: "obuStep1Icon")
        titleLbl.text =  "Your OBU device will be turned on once you start your vehicle engine. \n\nMake sure your OBU is within 10 metres of your device, and that another phone is not already connected to the OBU."
        titleLbl.lineBreakMode = .byWordWrapping
        titleLbl.textColor =  UIColor(hexString: "#222638", alpha: 1)
    }
    
}

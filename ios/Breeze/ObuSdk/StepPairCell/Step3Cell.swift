//
//  Step3Cell.swift
//  Breeze
//
//  Created by Tan, Tran Ngoc on 31/05/2023.
//

import UIKit


typealias NextCompletion = ()-> (Void)

protocol Step3CellDelegate {
    func checkEmptyTextField(updateText: String)
}

class Step3Cell: UICollectionViewCell, UITextFieldDelegate {
    
    @IBOutlet weak var step3Image: UIImageView!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var inputVehicleNumbertf: DesignableTextField!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    
    var delegate: Step3CellDelegate?
    var nextAction: NextCompletion?
    var isCheckVehicle: Bool = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        inputVehicleNumbertf.delegate = self
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        self.addGestureRecognizer(tapGesture)
    }
    
    @objc func handleTap() {
        // Called when the user taps on the screen
        inputVehicleNumbertf.endEditing(true) // Dismiss the keyboard
    }
    
    
    func setupUI() {
        step3Image.image = UIImage(named: "obuStep3Icon")
        titleLabel.text =  "Enter Vehicle Number or Give A Name"
        titleLabel.font = UIFont.carparkTitleFont()
        titleLabel.lineBreakMode = .byWordWrapping
        titleLabel.textColor =  UIColor(hexString: "#222638", alpha: 1)
        descriptionLabel.text = "Enter your vehicle plate number to make it easier to identify the connected vehicle. \n\nYou can also pick another name (max 8 alphanumeric characters). \n\nIf you leave the field blank, your OBU number will be used instead."
        descriptionLabel.lineBreakMode = .byWordWrapping
        descriptionLabel.textColor =  UIColor(hexString: "#222638", alpha: 1)
        descriptionLabel.font = UIFont.availableCarparkLotsLabelFont()
        inputVehicleNumbertf.placeholder = "Vehicle plate number or name"
        inputVehicleNumbertf.delegate = self
        inputVehicleNumbertf.layer.cornerRadius = 24
        inputVehicleNumbertf.clipsToBounds = true
        inputVehicleNumbertf.layer.borderColor = UIColor.brandPurpleColor.cgColor
        inputVehicleNumbertf.layer.borderWidth = 1
        inputVehicleNumbertf.tag = 0
        inputVehicleNumbertf.enablesReturnKeyAutomatically = true
        inputVehicleNumbertf.returnKeyType = .done
        inputVehicleNumbertf.textColor = .black
        inputVehicleNumbertf.leftViewMode = .always
        addImage()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        inputVehicleNumbertf.resignFirstResponder()
        return true
    }
    
    func addImage() {
        let imageView = UIImageView(frame: CGRect(x: 12.0, y: 8.0, width: 20, height: 20))
        let image = UIImage(named: "car-obu-textfeiled")
        imageView.image = image
        imageView.contentMode = .scaleAspectFit
        
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 32, height: 40))
        view.addSubview(imageView)
        view.backgroundColor = .clear
        inputVehicleNumbertf.leftViewMode = UITextField.ViewMode.always
        inputVehicleNumbertf.leftView = view
    }
    
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let updatedText = (inputVehicleNumbertf.text as NSString?)?.replacingCharacters(in: range, with: string)
        if let text = updatedText, !text.isEmpty {
            let allowedCharacters = CharacterSet.alphanumerics
            let textCharacterSet = CharacterSet(charactersIn: text)
            let isTextAllowed = allowedCharacters.isSuperset(of: textCharacterSet) && text.count <= 8
            delegate?.checkEmptyTextField(updateText: text)
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.ObuPairing.UserClick.number_plate_name_number_input, screenName: ParameterName.ObuPairing.screen_view)
            if isCheckVehicle == true {
                descriptionLabel.text = "You have used that name before. Please pick another."
                descriptionLabel.textColor = .red
            } else {
                descriptionLabel.text = "Enter your vehicle plate number to make it easier to identify the connected vehicle. \n\nYou can also pick another name (max 8 alphanumeric characters). \n\nIf you leave the field blank, your OBU number will be used instead."
                descriptionLabel.textColor =  UIColor(hexString: "#222638", alpha: 1)
            }
            return isTextAllowed
        } else {
            return true
        }
        
    }
    
    @IBAction func saveAction(_ sender: Any) {
        if let vehicleNumber = inputVehicleNumbertf.text, !vehicleNumber.isEmpty {
            OBUHelper.shared.vehicleNumber = vehicleNumber
        }
    }
    
}





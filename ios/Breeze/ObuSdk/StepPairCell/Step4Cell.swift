//
//  Step4Cell.swift
//  Breeze
//
//  Created by Tan, Tran Ngoc on 31/05/2023.
//

import UIKit

protocol Step4CellDelegate {
    func disconnetOBU()
}

class Step4Cell: UICollectionViewCell {

    @IBOutlet weak var step4Image: UIImageView!
    @IBOutlet weak var titleTf: UITextView!
    @IBOutlet weak var cancelLabel: UILabel!
    @IBOutlet weak var gifImage: UIImageView!
    
    var delegate: Step4CellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    func setupUI() {
        step4Image.image = UIImage(named: "obuStep3Icon")
        titleTf.attributedText = attributedText()
        titleTf.textColor = UIColor(hexString: "#222638", alpha: 1)
        cancelLabel.text = "Cancel Pairing"
        cancelLabel.font = UIFont.routePlanningTitleBoldFont()
        cancelLabel.textColor = UIColor(hexString: "#E82370", alpha: 1)
        let gifImageProgress = UIImage.gifImageWithName("animation_loading")
        gifImage.image = gifImageProgress
    }
    
    
    func attributedText() -> NSAttributedString {

        let string = "LTA will take 1-2 mins to verify and pair your OBU with Breeze. \n\nPlease do not close Breeze App, You will be notified once the pairing is complete." as NSString

        let attributedString = NSMutableAttributedString(string: string as String, attributes: [NSAttributedString.Key.font:UIFont.SFProRegularFont()])

        let boldFontAttribute = [NSAttributedString.Key.font: UIFont.SFProBoldFont()]
        // Part of string to be bold
        attributedString.addAttributes(boldFontAttribute, range: string.range(of: "Please do not close Breeze App"))
        
        // 4
        return attributedString
    }
    
//    @IBAction func cancelAction(_ sender: UIButton) {
//        self.delegate?.disconnetOBU()
//    }
    
    @IBAction func cancelAction(_ sender: Any) {
        self.delegate?.disconnetOBU()
    }
}

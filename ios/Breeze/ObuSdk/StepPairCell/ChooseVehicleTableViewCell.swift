//
//  ChooseVehicleTableViewCell.swift
//  Breeze
//
//  Created by Tan, Tran Ngoc on 28/07/2023.
//

import UIKit

class ChooseVehicleTableViewCell: UITableViewCell {
    
    static let identifier = "ChooseVehicleTableViewCell"
    
    
    @IBOutlet weak var vehicleIcon: UIImageView!
    
    @IBOutlet weak var vehicleNameLabel: UILabel!
    
    @IBOutlet weak var geenTickIcon: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setupUI(model: ObuVehicleDetailModel, isSelected: Bool) {
        //  Need check type then show vehicle icon
        if model.vehicleType?.uppercased() == "MOTORCYCLE" {
            vehicleIcon.image = UIImage(named: "vehicle_motobike_icon")
        } else if model.vehicleType?.uppercased() == "HEAVY_VEHICLE" {
            vehicleIcon.image = UIImage(named: "vehicle_truck_icon")
        } else {
            vehicleIcon.image = UIImage(named: "vehicle_car_icon")
        }
        
        vehicleNameLabel.textColor = UIColor(hexString: "#222638", alpha: 1.0)
        vehicleNameLabel.text = OBUHelper.shared.getLocalVehicleNameToDisplay(model.vehicleNumber)
        vehicleNameLabel.font = UIFont(name: fontFamilySFPro.Medium, size: 16.0)!
        
        geenTickIcon.isHidden = false
        if isSelected {
            geenTickIcon.image = UIImage(named: "greenTick")
        } else {
            geenTickIcon.image = UIImage(named: "vehicle_unselected")
        }
    }
    
}

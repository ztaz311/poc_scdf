//
//  Step2Cell.swift
//  Breeze
//
//  Created by Tan, Tran Ngoc on 31/05/2023.
//

import UIKit

class Step2Cell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var textView: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setupUI() {
        imageView.image = UIImage(named: "obuStep2Icon")
        textView.attributedText = attributedText()
        textView.textColor = UIColor(hexString: "#222638", alpha: 1)
//        textView.font = UIFont.SFProRegularFont()
    }
    
    func attributedText() -> NSAttributedString {

        let string = "On the next screen, you'd need to enter your phone's Bluetooth Mac address. Make sure it is the same one you registered with LTA. \n\nThis is needed by LTA for verification and validation. \n\nThis step will skip ahead if this is not your first time pairing. \n\nYou can get it from: \nSettings > General > About Screen > Bluetooth" as NSString

        let attributedString = NSMutableAttributedString(string: string as String, attributes: [NSAttributedString.Key.font:UIFont.SFProRegularFont()])

        let boldFontAttribute = [NSAttributedString.Key.font: UIFont.SFProBoldFont()]

        // Part of string to be bold
        attributedString.addAttributes(boldFontAttribute, range: string.range(of: "You can get it from:"))
      

        // 4
        return attributedString
    }
}

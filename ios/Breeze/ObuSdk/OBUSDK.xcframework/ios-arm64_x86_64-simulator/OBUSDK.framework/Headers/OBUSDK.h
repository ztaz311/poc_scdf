//
//  Copyright (c) Land Transport Authority of Singapore.
//  All rights reserved.
//
//  The SDK Terms of Use (TOU) is found in the LICENSE file
//  in the root directory of the SDK.

#import <Foundation/Foundation.h>

//! Project version number for OBUSDK.
FOUNDATION_EXPORT double OBUSDKVersionNumber;

//! Project version string for OBUSDK.
FOUNDATION_EXPORT const unsigned char OBUSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <OBUSDK/PublicHeader.h>



// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.8.1 (swiftlang-5.8.0.124.5 clang-1403.0.22.11.100)
// swift-module-flags: -target arm64-apple-ios12.0-simulator -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -module-name OBUSDK
// swift-module-flags-ignorable: -enable-bare-slash-regex
import CommonCrypto
import Compression
import CoreBluetooth
import CoreLocation
import Foundation
@_exported import OBUSDK
import ObjectiveC
import RealmSwift
import Swift
import SystemConfiguration
import UIKit
import _Concurrency
import _StringProcessing
public struct OBUTravelSummary {
  public let totalTravelTime: Swift.Int
  public let totalTravelDistance: Swift.Int
}
@frozen public enum OBUPaymentMode {
  case frontend
  case backend
  case businessFunctionDisabled
  public static func == (a: OBUSDK.OBUPaymentMode, b: OBUSDK.OBUPaymentMode) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public typealias JSON = [Swift.String : Any]
public typealias OBUImageRef = Swift.String
public typealias CompletionHandler<T> = (OBUSDK.CompletionResult<T>) -> Swift.Void
@frozen public enum CompletionResult<Completion> {
  case completion(Completion)
}
public typealias Handler<T> = (OBUSDK.HandlerResult<T, Foundation.NSError>) -> Swift.Void
@frozen public enum HandlerResult<Success, Failure> {
  case success(Success)
  case failure(Failure)
}
public struct OBUData : Swift.Codable {
  public var acceleration: OBUSDK.OBUAcceleration? {
    get
  }
  public let velocity: Swift.Double
  public var chargingInformations: [OBUSDK.OBUChargingInformation]?
  public var travelSummary: OBUSDK.OBUTravelSummary {
    get
  }
  public var totalTripCharged: OBUSDK.OBUTotalTripCharged? {
    get
  }
  public var cardBalance: Swift.Int?
  public var cardStatus: OBUSDK.OBUCardStatus {
    get
  }
  public var paymentMode: OBUSDK.OBUPaymentMode {
    get
  }
  public var paymentHistories: [OBUSDK.OBUPaymentHistory]?
  public var trafficInformation: OBUSDK.OBUTrafficInfo? {
    get
  }
  public func encode(to encoder: any Swift.Encoder) throws
  public init(from decoder: any Swift.Decoder) throws
}
public struct OBUTotalTripCharged {
  public let totalERP2Charged: Swift.Int
  public let totalEEPCharged: Swift.Int
  public let totalREPCharged: Swift.Int
  public let totalOPCCharged: Swift.Int
  public let totalCPTCharged: Swift.Int
}
@frozen public enum ItemTTS : Swift.Int, Swift.Codable {
  case Yes
  case No
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
public protocol LoggerProtocol {
  typealias FileContentDownloadCompletion = (Swift.String) -> Swift.Void
  func getLogFileNames() -> [Swift.String]
  func getLogFileDirectory() -> [Swift.String]
  func getLogFileContent(path: Swift.String, completion: @escaping Self.FileContentDownloadCompletion)
  func info(message: Swift.String)
  func debug(message: Swift.String)
  func warn(message: Swift.String)
  func verbose(message: Swift.String)
  func error(message: Swift.String)
}
@frozen public enum OBUBusinessFunction {
  case Unknown
  case ERP
  case EEP
  case EPS
  case REP
  case OPC
  case CPT
  public static func == (a: OBUSDK.OBUBusinessFunction, b: OBUSDK.OBUBusinessFunction) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
@frozen public enum OBUTrafficDataPriority {
  case high
  case charging
  case medium
  case low
  case undefined
  public static func == (a: OBUSDK.OBUTrafficDataPriority, b: OBUSDK.OBUTrafficDataPriority) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
@frozen public enum OBUChargingPaymentMode {
  case undefined
  case frontend
  case backend
  public static func == (a: OBUSDK.OBUChargingPaymentMode, b: OBUSDK.OBUChargingPaymentMode) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
@frozen public enum OBUChargingMessageType {
  case undefined
  case alertPoint
  case enteringPricingRoad
  case charging
  case deductionSuccessful
  case deductionFailure
  case cpoInformation
  case detectAlertPointCommonAlert
  public static func == (a: OBUSDK.OBUChargingMessageType, b: OBUSDK.OBUChargingMessageType) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public struct OBUPaymentHistory : Swift.Codable, Swift.Equatable {
  public let sequentialNumber: Swift.Int
  public let paymentDate: Swift.String?
  public var businessFunction: OBUSDK.OBUBusinessFunction {
    get
  }
  public var paymentMode: OBUSDK.OBUChargingPaymentMode {
    get
  }
  public let chargeAmount: Swift.Int
  public static func == (lhs: OBUSDK.OBUPaymentHistory, rhs: OBUSDK.OBUPaymentHistory) -> Swift.Bool
  public func encode(to encoder: any Swift.Encoder) throws
  public init(from decoder: any Swift.Decoder) throws
}
@frozen public enum OBUTextColor : Swift.Int, Swift.Codable {
  case Default
  case White
  case Red
  case Blue
  case Yellow
  case Green
  case Black
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
@frozen public enum OBUTextStyle : Swift.Int, Swift.Codable {
  case Default
  case Normal
  case Bold
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
@_hasMissingDesignatedInitializers public class OBUChargingInformation : Swift.Codable, Swift.Equatable {
  public var businessFunction: OBUSDK.OBUBusinessFunction {
    get
  }
  public var cardStatus: OBUSDK.OBUCardStatus
  public var content1: Swift.String {
    get
    set
  }
  public var content2: Swift.String {
    get
    set
  }
  public var content3: Swift.String {
    get
    set
  }
  public var content4: Swift.String {
    get
    set
  }
  public var chargingAmount: Swift.Int {
    get
    set
  }
  public var minChargeAmount: Swift.Int {
    get
    set
  }
  public var startTime: Swift.String {
    get
    set
  }
  public var endTime: Swift.String {
    get
    set
  }
  public var chargingType: OBUSDK.OBUChargingType {
    get
  }
  public var chargingMessageType: OBUSDK.OBUChargingMessageType {
    get
  }
  public var paymentMode: OBUSDK.OBUChargingPaymentMode {
    get
  }
  public static func == (lhs: OBUSDK.OBUChargingInformation, rhs: OBUSDK.OBUChargingInformation) -> Swift.Bool
  @objc deinit
  public func encode(to encoder: any Swift.Encoder) throws
  required public init(from decoder: any Swift.Decoder) throws
}
@frozen public enum OBUErrorCode : Swift.Int, Swift.Error {
  case INTERNET_NOT_AVAILABLE
  case SDK_AUTHENTICATION_REQUIRED
  case BLUETOOTH_DISABLED
  case BLUETOOTH_ACCESS_DENIED
  case DEVELOPER_UNAUTHORISED
  case APPLICATION_UNAUTHORISED
  case DEVELOPER_DEACTIVATED
  case INVALID_SDK_ACCOUNT_KEY
  case OBU_NOT_PAIRED
  case CONNECTION_FAILED
  case DEVICE_DATA_ACCESS_ERROR
  case INTERNAL_SDK_ERROR
  case MAC_ADDRESS_AUTHENTICATION_FAILED
  case OBU_DATA_ACCESS_PERMISSION_DENIED
  case MOCK_ENABLE_REQUIRED
  case CODE_SIGNATURE_FAIL
  case NO_PUBLIC_KEY_AVAILABLE
  case SERVER_ERROR
  case TRANSPORT_ERROR
  case REQUEST_TIMEOUT
  case BAD_REQUEST
  case NOT_FOUND
  case FORBIDDEN
  case UNKNOWN
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
final public class MockBuilder {
  public init()
  final public func setTimeInterval(_ time: Swift.Int) -> OBUSDK.MockBuilder
  final public func setCardBalance(_ balance: Swift.Int) -> OBUSDK.MockBuilder
  final public func setSequence(_ seq: [OBUSDK.MockEvent]) -> OBUSDK.MockBuilder
  final public func setCyclicMode(_ isEnabled: Swift.Bool) -> OBUSDK.MockBuilder
  final public func build() -> OBUSDK.MockSetting
  @objc deinit
}
@frozen public enum OBUTrafficMessageType : Swift.Int {
  case unplannedIncidentsTypeI
  case unplannedIncidentsTypeII
  case freeText5Line
  case freeText6Line
  case freeText7Line
  case freeText8Line
  case plannedIncidents
  case travelTime
  case parkingGuidance
  case preloadedData
  case imageDisplay
  case undefined
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
public func getChargingHistory() -> [OBUSDK.OBUPaymentHistory]
@frozen public struct OBU {
  public var name: Swift.String
  internal var peripheral: CoreBluetooth.CBPeripheral?
}
@frozen public enum OBUChargingType {
  case pointBased
  case distanceBased
  case entryExitBased
  case erp1
  case common
  case undefined
  public static func == (a: OBUSDK.OBUChargingType, b: OBUSDK.OBUChargingType) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public struct OBUAcceleration : Swift.Codable, Swift.Equatable {
  public let x: Swift.Float
  public let y: Swift.Float
  public let z: Swift.Float
  public static func == (lhs: OBUSDK.OBUAcceleration, rhs: OBUSDK.OBUAcceleration) -> Swift.Bool
  public func encode(to encoder: any Swift.Encoder) throws
  public init(from decoder: any Swift.Decoder) throws
}
@_hasMissingDesignatedInitializers public class OBUSDKManager {
  weak public var dataDelegate: (any OBUSDK.OBUDataDelegate)?
  weak public var connectionStatusDelegate: (any OBUSDK.OBUConnectionDelegate)?
  public var logger: (any OBUSDK.LoggerProtocol)?
  public static var shared: OBUSDK.OBUSDKManager
  public func enableMockSetting(_ settings: OBUSDK.MockSetting)
  public func initializeSDK(with sdkKey: Swift.String, _ completion: @escaping OBUSDK.Handler<Swift.Bool>)
  public func requestOBUDataAccessPermission(_ completion: @escaping OBUSDK.Handler<Swift.Bool>)
  public func startSearch()
  public func stopSearch()
  public func connectOBU(obu: OBUSDK.OBU)
  public func connectToOBU(vehicleId: Swift.String)
  public func disconnectOBU()
  public func getPairedDevices(_ completion: @escaping OBUSDK.Handler<[OBUSDK.OBU]?>)
  public func getLastOBUData() -> OBUSDK.OBUData?
  public func getCardBalance() -> Swift.Int?
  public func getCardStatus() -> OBUSDK.OBUCardStatus?
  public func getPaymentMode() -> OBUSDK.OBUPaymentMode?
  public func getPaymentHistories() -> [OBUSDK.OBUPaymentHistory]?
  public func getLastTrafficInfo() -> OBUSDK.OBUTrafficInfo?
  public func getLastTripSummary() -> OBUSDK.OBUTravelSummary?
  public func isSDKInitialised() -> Swift.Bool
  public func isDataPermissionGranted() -> Swift.Bool
  @objc deinit
}
public protocol OBUDataDelegate : AnyObject {
  func onVelocityInformation(_ velocity: Swift.Double)
  func onAccelerationInformation(_ acceleration: OBUSDK.OBUAcceleration)
  func onChargingInformation(_ chargingInfo: [OBUSDK.OBUChargingInformation])
  func onTrafficInformation(_ trafficInfo: OBUSDK.OBUTrafficInfo)
  func onErpChargingAndTrafficInfo(trafficInfo: OBUSDK.OBUTrafficInfo?, chargingInfo: [OBUSDK.OBUChargingInformation]?)
  func onPaymentHistories(_ histories: [OBUSDK.OBUPaymentHistory])
  func onTravelSummary(_ travelSummary: OBUSDK.OBUTravelSummary)
  func onTotalTripCharged(_ totalCharged: OBUSDK.OBUTotalTripCharged)
  func onError(_ errorCode: Foundation.NSError)
  func onCardInformation(_ status: OBUSDK.OBUCardStatus?, paymentMode: OBUSDK.OBUPaymentMode?, chargingPaymentMode: OBUSDK.OBUChargingPaymentMode?, balance: Swift.Int)
}
extension OBUSDK.OBUDataDelegate {
  public func onVelocityInformation(_ velocity: Swift.Double)
  public func onAccelerationInformation(_ acceleration: OBUSDK.OBUAcceleration)
  public func onChargingInformation(_ chargingInfo: [OBUSDK.OBUChargingInformation])
  public func onTrafficInformation(_ trafficInfo: OBUSDK.OBUTrafficInfo)
  public func onErpChargingAndTrafficInfo(trafficInfo: OBUSDK.OBUTrafficInfo?, chargingInfo: [OBUSDK.OBUChargingInformation]?)
  public func onPaymentHistories(_ histories: [OBUSDK.OBUPaymentHistory])
  public func onTravelSummary(_ travelSummary: OBUSDK.OBUTravelSummary)
  public func onTotalTripCharged(_ totalCharged: OBUSDK.OBUTotalTripCharged)
  public func onError(_ errorCode: Foundation.NSError)
  public func onCardInformation(_ status: OBUSDK.OBUCardStatus?, paymentMode: OBUSDK.OBUPaymentMode?, chargingPaymentMode: OBUSDK.OBUChargingPaymentMode?, balance: Swift.Int)
}
@_hasMissingDesignatedInitializers public class MockSetting {
  public var timeInterval: Swift.Int {
    get
  }
  public var cardBalance: Swift.Int {
    get
  }
  public var sequence: [OBUSDK.MockEvent] {
    get
  }
  public var cyclicMode: Swift.Bool {
    get
  }
  public static let MOCK_OBU_NAME: Swift.String
  @objc deinit
}
public struct OBUTextWithStyle {
  public let text: Swift.String
  public let color: OBUSDK.OBUTextColor
  public let style: OBUSDK.OBUTextStyle
}
extension OBUSDK.OBUTextWithStyle : Swift.Equatable {
  public static func == (lhs: OBUSDK.OBUTextWithStyle, rhs: OBUSDK.OBUTextWithStyle) -> Swift.Bool
}
public struct Parking {
  public let location: OBUSDK.OBUTextWithStyle
  public let lots: OBUSDK.OBUTextWithStyle
}
public struct TravelTime {
  public let location: OBUSDK.OBUTextWithStyle
  public let min: OBUSDK.OBUTextWithStyle
  public let icon: OBUSDK.OBUImageRef
}
public protocol OBUBaseTraffic {
  associatedtype T
  var priority: OBUSDK.OBUTrafficDataPriority { get set }
  var templateId: OBUSDK.OBUTrafficMessageType { get set }
  var dataList: [Self.T]? { get set }
}
public struct OBUTrafficParking : OBUSDK.OBUBaseTraffic {
  public var priority: OBUSDK.OBUTrafficDataPriority
  public var templateId: OBUSDK.OBUTrafficMessageType
  public var dataList: [OBUSDK.Parking]?
  public typealias T = OBUSDK.Parking
}
public struct OBUTravelTime : OBUSDK.OBUBaseTraffic {
  public var priority: OBUSDK.OBUTrafficDataPriority
  public var templateId: OBUSDK.OBUTrafficMessageType
  public var dataList: [OBUSDK.TravelTime]?
  public typealias T = OBUSDK.TravelTime
}
public struct OBUTrafficText : OBUSDK.OBUBaseTraffic {
  public var priority: OBUSDK.OBUTrafficDataPriority
  public var templateId: OBUSDK.OBUTrafficMessageType
  public var dataList: [OBUSDK.OBUTextWithStyle]?
  public typealias T = OBUSDK.OBUTextWithStyle
  public let icon: OBUSDK.OBUImageRef?
}
@frozen public enum OBUTrafficInfo {
  case trafficInformation(OBUSDK.OBUTrafficText)
  case parkingData(OBUSDK.OBUTrafficParking)
  case travelTimeData(OBUSDK.OBUTravelTime)
}
@frozen public enum TdcidEvent {
  case template1A
  case template1B
  case template2A
  case template2B
  case template2C
  case template2D
  case template3A
  case template3B
  case template3C
  case template4A
  case template4B
  case template5A
  case template5B
  case template6A
  case template6B
  case template8A
  case template8B
  case template110A
  case template110B
  case template111A
  case template111B
  case template12A
  case template12B
  case template13A
  case template1C
  case template1D
  case template8C
  case template8D
  case template8E
  case template8F
  case template8G
  case template12C
  case template12D
  case template12E
  case template12F
  case template12G
  case template12H
  case template12I
  case template12J
  case template1E
  case template1F
  case template1G
  case template1H
  case template1I
  case template1J
  case template1K
  case template1L
  case template1M
  case template8H
  case template8I
  case template8J
  case template8K
  case template8L
  case template8M
  case template8N
  case template8O
  case template1N
  case template1O
  case template1P
  case template1Q
  case template1R
  case template8P
  case template1S
  case template1T
  case template12K
  case template8Q
  case template12L
  case template12M
  case template12N
  case template12O
  case template12P
  case template1U
  case template1V
  case template12Q
  case template12R
  case template2E
  public static func == (a: OBUSDK.TdcidEvent, b: OBUSDK.TdcidEvent) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
@frozen public enum BluetoothState : Swift.Int {
  case unknown
  case resetting
  case unsupported
  case unauthorized
  case poweredOff
  case poweredOn
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
@frozen public enum OBUCardStatus {
  case detected
  case blacklisted
  case expiredStored
  case invalid
  case IssuerIDError
  case noCardForFrontendPayment
  case blocked
  case faultyStoreValue
  case insufficientStoredValue
  case wrongStoredValueDebitCertificate
  case nfcAccessError
  case undefined
  public static func == (a: OBUSDK.OBUCardStatus, b: OBUSDK.OBUCardStatus) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public class MockEvent {
  public var electronicEventList: [OBUSDK.ERPEvent]?
  public var tdcidEvent: OBUSDK.TdcidEvent?
  public init(electronicEventList: [OBUSDK.ERPEvent], tdcidEvent: OBUSDK.TdcidEvent?)
  @objc deinit
}
@_hasMissingDesignatedInitializers public class ERPEvent : OBUSDK.OBUChargingInformation {
  @objc deinit
}
@_hasMissingDesignatedInitializers public class PointBasedAlertPointDetected : OBUSDK.ERPEvent {
  public init(content1: Swift.String = "Ophir Road", chargeAmount: Swift.Int = 5)
  @objc deinit
}
@_hasMissingDesignatedInitializers public class PointBasedCharging : OBUSDK.ERPEvent {
  public init(chargeAmount: Swift.Int = 5)
  @objc deinit
}
@_hasMissingDesignatedInitializers public class PointBasedDeductionSuccessful : OBUSDK.ERPEvent {
  public init(chargeAmount: Swift.Int = 5)
  @objc deinit
}
@_hasMissingDesignatedInitializers public class PointBasedDeductionFailure : OBUSDK.ERPEvent {
  public init()
  @objc deinit
}
@_hasMissingDesignatedInitializers public class DistanceBasedAlertPointDetected : OBUSDK.ERPEvent {
  public init(content1: Swift.String = "Ophir Road", chargeAmount: Swift.Int = 5)
  @objc deinit
}
@_hasMissingDesignatedInitializers public class DistanceBasedEnteringPricedRoad : OBUSDK.ERPEvent {
  public init()
  @objc deinit
}
@_hasMissingDesignatedInitializers public class DistanceBasedCharging : OBUSDK.ERPEvent {
  public init(chargeAmount: Swift.Int = 5)
  @objc deinit
}
@_hasMissingDesignatedInitializers public class DistanceBasedDeductionSuccessful : OBUSDK.ERPEvent {
  public init(chargeAmount: Swift.Int = 5)
  @objc deinit
}
@_hasMissingDesignatedInitializers public class DistanceBasedDeductionFailure : OBUSDK.ERPEvent {
  public init()
  @objc deinit
}
@_hasMissingDesignatedInitializers public class EntryExitBasedAlertPointDetected : OBUSDK.ERPEvent {
  public init(content1: Swift.String = "Ophir Road", chargeAmount: Swift.Int = 5)
  @objc deinit
}
@_hasMissingDesignatedInitializers public class EntryExitBasedEnteringPricedRoad : OBUSDK.ERPEvent {
  public init()
  @objc deinit
}
@_hasMissingDesignatedInitializers public class EntryExitBasedCharging : OBUSDK.ERPEvent {
  public init(chargeAmount: Swift.Int = 5)
  @objc deinit
}
@_hasMissingDesignatedInitializers public class EntryExitBasedDeductionSuccessful : OBUSDK.ERPEvent {
  public init(chargeAmount: Swift.Int = 5)
  @objc deinit
}
@_hasMissingDesignatedInitializers public class EntryExitBasedDeductionFailure : OBUSDK.ERPEvent {
  public init()
  @objc deinit
}
@_hasMissingDesignatedInitializers public class ERP1BasedCharging : OBUSDK.ERPEvent {
  public init(chargeAmount: Swift.Int = 5)
  @objc deinit
}
@_hasMissingDesignatedInitializers public class ERP1BasedDeductionSuccessful : OBUSDK.ERPEvent {
  public init(chargeAmount: Swift.Int = 5)
  @objc deinit
}
@_hasMissingDesignatedInitializers public class ERP1BasedDeductionFailure : OBUSDK.ERPEvent {
  public init()
  @objc deinit
}
@_hasMissingDesignatedInitializers public class EEPCPOInformation : OBUSDK.ERPEvent {
  public init()
  @objc deinit
}
@_hasMissingDesignatedInitializers public class EEPBasedCharging : OBUSDK.ERPEvent {
  public init(chargeAmount: Swift.Int = 5)
  @objc deinit
}
@_hasMissingDesignatedInitializers public class EEPBasedDeductionSuccessful : OBUSDK.ERPEvent {
  public init(chargeAmount: Swift.Int = 5)
  @objc deinit
}
@_hasMissingDesignatedInitializers public class EEPBasedDeductionFailure : OBUSDK.ERPEvent {
  public init()
  @objc deinit
}
@_hasMissingDesignatedInitializers public class EPSBasedCharging : OBUSDK.ERPEvent {
  public init(chargeAmount: Swift.Int = 5)
  @objc deinit
}
@_hasMissingDesignatedInitializers public class EPSBasedDeductionSuccessful : OBUSDK.ERPEvent {
  public init(chargeAmount: Swift.Int = 5)
  @objc deinit
}
@_hasMissingDesignatedInitializers public class EPSBasedDeductionFailure : OBUSDK.ERPEvent {
  public init()
  @objc deinit
}
@_hasMissingDesignatedInitializers public class CommonAlertPointDetected : OBUSDK.ERPEvent {
  public init(content1: Swift.String = "After Exit 11", content2: Swift.String = "(Paya lebar Rd)")
  @objc deinit
}
public protocol OBUConnectionDelegate : AnyObject {
  func onOBUConnected(_ device: OBUSDK.OBU)
  func onOBUDisconnected(_ device: OBUSDK.OBU, error: Foundation.NSError?)
  func onOBUFound(_ device: [OBUSDK.OBU])
  func onOBUConnectionFailure(_ error: Foundation.NSError)
  func onBluetoothStateUpdated(_ state: OBUSDK.BluetoothState)
  func onSearchFinished()
}
extension OBUSDK.OBUPaymentMode : Swift.Equatable {}
extension OBUSDK.OBUPaymentMode : Swift.Hashable {}
extension OBUSDK.OBUPaymentMode : Swift.Sendable {}
extension OBUSDK.ItemTTS : Swift.Equatable {}
extension OBUSDK.ItemTTS : Swift.Hashable {}
extension OBUSDK.ItemTTS : Swift.RawRepresentable {}
extension OBUSDK.ItemTTS : Swift.Sendable {}
extension OBUSDK.OBUBusinessFunction : Swift.Equatable {}
extension OBUSDK.OBUBusinessFunction : Swift.Hashable {}
extension OBUSDK.OBUBusinessFunction : Swift.Sendable {}
extension OBUSDK.OBUTrafficDataPriority : Swift.Equatable {}
extension OBUSDK.OBUTrafficDataPriority : Swift.Hashable {}
extension OBUSDK.OBUTrafficDataPriority : Swift.Sendable {}
extension OBUSDK.OBUChargingPaymentMode : Swift.Equatable {}
extension OBUSDK.OBUChargingPaymentMode : Swift.Hashable {}
extension OBUSDK.OBUChargingPaymentMode : Swift.Sendable {}
extension OBUSDK.OBUChargingMessageType : Swift.Equatable {}
extension OBUSDK.OBUChargingMessageType : Swift.Hashable {}
extension OBUSDK.OBUChargingMessageType : Swift.Sendable {}
extension OBUSDK.OBUTextColor : Swift.Equatable {}
extension OBUSDK.OBUTextColor : Swift.Hashable {}
extension OBUSDK.OBUTextColor : Swift.RawRepresentable {}
extension OBUSDK.OBUTextColor : Swift.Sendable {}
extension OBUSDK.OBUTextStyle : Swift.Equatable {}
extension OBUSDK.OBUTextStyle : Swift.Hashable {}
extension OBUSDK.OBUTextStyle : Swift.RawRepresentable {}
extension OBUSDK.OBUTextStyle : Swift.Sendable {}
extension OBUSDK.OBUErrorCode : Swift.Equatable {}
extension OBUSDK.OBUErrorCode : Swift.Hashable {}
extension OBUSDK.OBUErrorCode : Swift.RawRepresentable {}
extension OBUSDK.OBUTrafficMessageType : Swift.Equatable {}
extension OBUSDK.OBUTrafficMessageType : Swift.Hashable {}
extension OBUSDK.OBUTrafficMessageType : Swift.RawRepresentable {}
extension OBUSDK.OBUTrafficMessageType : Swift.Sendable {}
extension OBUSDK.OBUChargingType : Swift.Equatable {}
extension OBUSDK.OBUChargingType : Swift.Hashable {}
extension OBUSDK.OBUChargingType : Swift.Sendable {}
extension OBUSDK.TdcidEvent : Swift.Equatable {}
extension OBUSDK.TdcidEvent : Swift.Hashable {}
extension OBUSDK.TdcidEvent : Swift.Sendable {}
extension OBUSDK.BluetoothState : Swift.Equatable {}
extension OBUSDK.BluetoothState : Swift.Hashable {}
extension OBUSDK.BluetoothState : Swift.RawRepresentable {}
extension OBUSDK.BluetoothState : Swift.Sendable {}
extension OBUSDK.OBUCardStatus : Swift.Equatable {}
extension OBUSDK.OBUCardStatus : Swift.Hashable {}
extension OBUSDK.OBUCardStatus : Swift.Sendable {}

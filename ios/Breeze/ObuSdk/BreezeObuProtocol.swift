//
//  BreezeObuProtocol.swift
//  Breeze
//
//  Created by Tuan, Pham Hai on 05/06/2023.
//

import Foundation
import OBUSDK

enum ObuEventType: String {
    case TRAFFIC
    case ERP_CHARGING
    case ERP_SUCCESS
    case ERP_FAILURE
    case SCHOOL_ZONE
    case SILVER_ZONE
    case PARKING_FEE
    case PARKING_SUCCESS
    case PARKING_FAILURE
    case PARKING_AVAILABILITY
    case TRAVEL_TIME
    case CARD_ERROR
    case CARD_EXPIRED
    case CARD_INSUFFICIENT
    case CARD_VALID
    case ROAD_CLOSURE
    case ROAD_DIVERSION
    case GENERAL_MESSAGE
    case SPEED_CAMERA
    case EVENT
    // MARK: update new event BREEZE2-602
    case FLASH_FLOOD
    case HEAVY_TRAFFIC
    case UNATTENDED_VEHICLE
    case MISCELLANEOUS
    case ROAD_BLOCK
    case OBSTACLE
    case ROAD_WORK
    case VEHICLE_BREAKDOWN
    case MAJOR_ACCIDENT
    case SEASON_PARKING
    case TREE_PRUNING
    case YELLOW_DENGUE_ZONE
    case RED_DENGUE_ZONE
    case BUS_LANE
    case LOW_CARD
}

enum BreezeChargingPaymentMode: String {
    case frontend
    case backend
    case undefined
}

enum BreezePaymentMode: String {
    case frontend
    case backend
    case businessFunctionDisabled
    case undefined
}

enum BreezeCardStatus: String {
    case valid = "CARD_VALID"
    case error = "CARD_ERROR"
    case expired = "CARD_EXPIRED"
    case insufficent = "CARD_INSUFFICIENT"
}

//  Charging information
struct BreezeObuChargingInfo: Codable {
    let businessFunction: String    //  ERP, EEP, EPS
    let cardStatus: String?
    let content1: String?
    let content2: String?
    let content3: String?
    let content4: String?
    let chargingAmount: Int
    let minChargeAmount: Int?
    let startTime: String?
    let endTime: String?
    let chargingType: String
    let chargingMessageType: String
    let roadName: String
    
    func getParkingRate() -> String {
        let balance = Double(chargingAmount) 
        let amount = String(format: "%.2f", balance / 100)
        return "$\(amount)/hr"
    }
    
    func getChargeAmount() -> String {
        let balance = Double(chargingAmount)
        let amount = String(format: "%.2f", balance / 100)
        return "$\(amount)"
    }
    
    func getType() -> String {
        //  ERP_CHARGING, ERP_SUCCESS, ERP_FAILURE, PARKING_FEE, PARKING_SUCCESS, PARKING_FAILURE
        var retValue: String = "UNDEFINE"
        
        if businessFunction == "ERP" {
            switch chargingMessageType.lowercased() {
            case "alertpoint", "charging":
                retValue = "ERP_CHARGING"
            case "deductionsuccessful":
                retValue = "ERP_SUCCESS"
            case "deductionfailure":
                retValue = "ERP_FAILURE"
            case "lowcardbalance":
                retValue = "LOW_CARD"
            default:
                retValue = "UNDEFINE"
            }
        } else if (businessFunction == "EEP" || businessFunction == "EPS") {
            switch chargingMessageType.lowercased() {
            case "alertpoint", "charging":
                retValue = "PARKING_FEE"
            case "deductionsuccessful":
                retValue = "PARKING_SUCCESS"
            case "deductionfailure":
                retValue = "PARKING_FAILURE"
            case "lowcardbalance":
                retValue = "LOW_CARD"
            default:
                retValue = "UNDEFINE"
            }
        }
        
        return retValue
    }
    
    func getMessage() -> String {
        //  TODO: need check how to map mock data with these messages
        //  Deduction Successful, Card Error, Card Expired, Insufficient Balance, System Error, Parking Fee,Deduction Successful, Deduction Failed
        var retValue: String = ""
        if businessFunction == "ERP" {
            switch chargingMessageType.lowercased() {
            case "alertpoint", "charging":
                retValue = ""
            case "deductionsuccessful":
                retValue = "Deduction Successful"
            case "deductionfailure":
                retValue = "Deduction Failed"
            default:
                retValue = ""
            }
        } else if (businessFunction == "EEP" || businessFunction == "EPS") {
            switch chargingMessageType.lowercased() {
            case "alertpoint", "charging":
                retValue = "Parking Fee"
            case "deductionsuccessful":
                retValue = "Deduction Successful"
            case "deductionfailure":
                retValue = "Deduction Failed"
            default:
                retValue = ""
            }
        }
        return retValue
    }
    
     func getDistance() -> String? {
        var retValue: String?
        switch chargingMessageType.lowercased() {
        case "alertpoint", "charging":
            retValue = "200m"
        default:
            break
        }
        return retValue
    }
    
    func getPostDataWithTripID(_ tripGUID: String, vehicleNumber: String, roadName: String?) -> [String: Any] {
        let timestamp = Int(Date().currentTimeInMiliseconds())
        var datas: [String : Any] =
        [
            "businessFunction": businessFunction,
            "eventTimestamp": timestamp,
            "chargeAmount": chargingAmount
        ]
            
        if let status = cardStatus, !status.isEmpty {
            datas["cardStatus"] = status
        }
        
        if !chargingType.isEmpty {
            datas["chargingType"] = chargingType
        }
        
        if !chargingMessageType.isEmpty {
            datas["chargingMessageType"] = chargingMessageType
        }
        
        if let content1 = content1, !content1.isEmpty {
            datas["content1"] = content1
        }
        
        if let content2 = content2, !content2.isEmpty {
            datas["content2"] = content2
        }
        
        if let content3 = content3, !content3.isEmpty {
            datas["content3"] = content3
        }
        
        if let content4 = content4, !content4.isEmpty {
            datas["content4"] = content4
        }
        
        if let startTime = startTime, !startTime.isEmpty {
            datas["startTime"] = startTime
        }
        
        if let endTime = endTime, !endTime.isEmpty {
            datas["endTime"] = endTime
        }
        
        if !self.roadName.isEmpty {
            datas["roadName"] = self.roadName
        } else if let name = roadName, !name.isEmpty {
            datas["roadName"] = name
        }
        
        if !tripGUID.isEmpty {
            datas["tripGUID"] = tripGUID
        }
        
        if !vehicleNumber.isEmpty {
            datas["vehicleNumber"] = vehicleNumber
        }
        
        return datas
    }
    
    func getParkingStartAndEnd() -> String? {
        let type = getType()
        
        if type == "PARKING_SUCCESS" || type == "PARKING_FAILURE" {
            if let startDate = DateUtils.shared.getDate(dateFormat: "yyyyMMddHHmmss", dateString: startTime ?? ""), let endDate = DateUtils.shared.getDate(dateFormat: "yyyyMMddHHmmss", dateString: endTime ?? "") {

                let startString = DateUtils.shared.getTimeDisplay(dateFormat: "YYYYMMdd_HH:mm", date: startDate)
                let endString = DateUtils.shared.getTimeDisplay(dateFormat: "YYYYMMdd_HH:mm", date: endDate)
                
                return "parking_start_\(startString), parking_end_\(endString)"
            }
        }
        
        return nil
    }
    
    //  Define get new dic for OBUDisplay for handling card error
    func getNewNotiDic() -> [String: Any] {
        var detailTitle: String = ""
        var detailMessage: String = ""
        
        let type = getType()
        if type == "PARKING_FEE" || type == "PARKING_SUCCESS" || type == "PARKING_FAILURE" || type == "LOW_CARD" {
            detailTitle = content1 ?? ""
            if type == "PARKING_SUCCESS" || type == "PARKING_FAILURE" {
                if let startDate = DateUtils.shared.getDate(dateFormat: "yyyyMMddHHmmss", dateString: startTime ?? ""), let endDate = DateUtils.shared.getDate(dateFormat: "yyyyMMddHHmmss", dateString: endTime ?? "") {
                    let duration = endDate.timeIntervalSince(startDate) //  Parking duration in seconds
                    
                    let startString = DateUtils.shared.getTimeDisplay(dateFormat: "HH:mma", date: startDate)
                    let endString = DateUtils.shared.getTimeDisplay(dateFormat: "HH:mma", date: endDate)
                    detailMessage = "\(startString) - \(endString) \n\(Int(duration).secondsToHoursMinutes())"
                }
            }
        }
        
        let resultDic: [String: Any] = ["type": type,
                                        "message": getNewMessage(),
                                        "distance": getDistance() as Any,
                                        "detailTitle": detailTitle,
                                        "detailMessage": detailMessage,
                                        "chargeAmount": "$\(String(format: "%.2f", Double(chargingAmount) / 100.0))"
        ]
        
        print("ERP dic: \(resultDic)")
        return resultDic
    }
    
    //  Define get new message OBUDisplay
    func getNewMessage() -> String {
        //  Deduction Successful, Card Error, Card Expired, Insufficient Balance, System Error, Parking Fee,Deduction Successful, Deduction Failed
        var retValue: String = ""
        if businessFunction == "ERP" {
            switch chargingMessageType.lowercased() {
            case "alertpoint", "charging":
                retValue = ""
            case "deductionsuccessful":
                retValue = "Deduction Successful"
            case "deductionfailure":
                let errorMessage = getCardErrorMessage()
                if !errorMessage.isEmpty {
                    retValue = errorMessage
                } else {
                    retValue = "Deduction Failed"
                }
            default:
                retValue = ""
            }
        } else if (businessFunction == "EEP" || businessFunction == "EPS") {
            switch chargingMessageType.lowercased() {
            case "alertpoint", "charging":
                retValue = "Parking Fee"
            case "deductionsuccessful":
                retValue = "Deduction Successful"
            case "deductionfailure":
                let errorMessage = getCardErrorMessage()
                if !errorMessage.isEmpty {
                    retValue = errorMessage
                } else {
                    retValue = "Deduction Failed"
                }
            default:
                retValue = ""
            }
        }
        return retValue
    }
        
    func getNotiDic() -> [String: Any] {
        
        var detailTitle: String = ""
        var detailMessage: String = ""
        
        let type = getType()
        if type == "PARKING_FEE" || type == "PARKING_SUCCESS" || type == "PARKING_FAILURE" || type == "LOW_CARD" {
            detailTitle = content1 ?? ""
            if type == "PARKING_SUCCESS" || type == "PARKING_FAILURE" {
                if let startDate = DateUtils.shared.getDate(dateFormat: "yyyyMMddHHmmss", dateString: startTime ?? ""), let endDate = DateUtils.shared.getDate(dateFormat: "yyyyMMddHHmmss", dateString: endTime ?? "") {
                    let duration = endDate.timeIntervalSince(startDate) //  Parking duration in seconds
                    
                    let startString = DateUtils.shared.getTimeDisplay(dateFormat: "HH:mma", date: startDate)
                    let endString = DateUtils.shared.getTimeDisplay(dateFormat: "HH:mma", date: endDate)
                    detailMessage = "\(startString) - \(endString) \n\(Int(duration).secondsToHoursMinutes())"
                }
            }
        }
        
        let resultDic: [String: Any] = ["type": type,
                                        "message": getMessage(),
                                        "distance": getDistance() as Any,
                                        "detailTitle": detailTitle,
                                        "detailMessage": detailMessage,
                                        "chargeAmount": "$\(String(format: "%.2f", Double(chargingAmount) / 100.0))"
        ]
        
        print("ERP dic: \(resultDic)")
        return resultDic
    }
    
    func getCardErrorMessage() -> String {
        var messageCard = ""
        switch cardStatus {
        case BreezeCardStatus.error.rawValue:
            messageCard = "Card Error"
        case BreezeCardStatus.expired.rawValue:
            messageCard = "Card Expired"
        case BreezeCardStatus.insufficent.rawValue:
            messageCard = "Insufficient Balance"
        default:
            if let status = cardStatus, !status.isEmpty {
                messageCard = "System Error"
            }
            break
        }
        return messageCard
    }
    
}

//  MARK - Traffic information

struct BreezeObuTrafficInfo: Codable {
    let trafficText: BreezeTrafficText?
    let trafficParking: BreezeTrafficParking?
    let travelTime: BreezeTravelTime?
    
    func getPostDataWithTripID(_ tripGUID: String) -> [String: Any] {
        var resultDic: [String: Any] = ["tripGUID": tripGUID]
        return resultDic
    }
    
    func getTrafficNotiDic(_ roadName: String = "") -> [String: Any] {
        var retDic: [String: Any] = [:]
        
        //  TRAFFIC, SCHOOL_ZONE
        if let text = trafficText {
            
            if text.icon == .some("2002") {
                //  School zone
                retDic["type"] = ObuEventType.SCHOOL_ZONE.rawValue
                retDic["message"] = "School Zone"
            } else if text.icon == .some("2003") {
                //  Silver zone
                retDic["type"] = ObuEventType.SILVER_ZONE.rawValue
                retDic["message"] = "Silver Zone"
            } else if text.icon == .some("1412") {
                retDic["type"] = ObuEventType.TRAFFIC.rawValue
                if !roadName.isEmpty {
                    retDic["message"] = "Accident on \(roadName)"
                } else {
                    retDic["message"] = "Accident"
                }
                
            } else if text.icon == .some("1410") {
                //  General alert - Road closure, Road diversion
                retDic["type"] = ObuEventType.ROAD_CLOSURE.rawValue
                if text.dataList.count == 1 {
                    retDic["message"] = text.dataList.first
                } else if text.dataList.count == 2 {
                    retDic["message"] = text.dataList.first
                    retDic["detailTitle"] = text.dataList[1]
                } else if text.dataList.count == 3 {
                    retDic["message"] = text.dataList.first
                    retDic["detailTitle"] = text.dataList[1]
                    retDic["detailMessage"] = text.dataList[2]
                }
            } else if text.icon == .some("9011") {
                //  Speed camera
                retDic["type"] = ObuEventType.SPEED_CAMERA.rawValue
                retDic["message"] = "Speed Camera"
            } else if text.icon == .some("2004") {
                //  Bus lane
                retDic["type"] = ObuEventType.BUS_LANE.rawValue
                retDic["message"] = "Bus Lane"
                retDic["detailTitle"] = "In Operation"
            } else {
                //  General alert
                retDic["type"] = ObuEventType.ROAD_CLOSURE.rawValue
                if text.dataList.count == 1 {
                    retDic["message"] = text.dataList.first
                } else if text.dataList.count == 2 {
                    retDic["message"] = text.dataList.first
                    retDic["detailTitle"] = text.dataList[1]
                } else if text.dataList.count == 3 {
                    retDic["message"] = text.dataList.first
                    retDic["detailTitle"] = text.dataList[1]
                    retDic["detailMessage"] = text.dataList[2]
                }
            }
        }
        
        return retDic
    }
    
    func getSpokenText(_ roadName: String = "") -> String {
        var spokenText = ""
        
        if let parking = trafficParking {
            spokenText = parking.getSpokenText()
        } else if let travel = travelTime {
            spokenText = travel.getSpokenText()
        } else if let text = trafficText {
            
            if text.icon == .some("2002") {
                spokenText = "School Zone. Drive carefully"
            } else if text.icon == .some("2003") {
                spokenText = "Silver Zone. Drive carefully"
            } else if text.icon == .some("1412") {
                spokenText = "Accident on \(roadName)"
            } else if text.icon == .some("1410") {
                //  General alert - Road closure, Road diversion
                var counter = 0
                for location in text.dataList {
                    if counter == 0 {
                        spokenText.append(location)
                    } else {
                        spokenText.append(", \(location)")
                    }
                    counter += 1
                }
            } else if text.icon == .some("9011") {
                spokenText = "Speed camera nearby."
            } else {
                var counter = 0
                for location in text.dataList {
                    if counter == 0 {
                        spokenText.append(location)
                    } else {
                        spokenText.append(", \(location)")
                    }
                    counter += 1
                }
            }
        }
        
        return spokenText
    }
}

struct BreezeTrafficText: Codable {
    let priority: String
    let templateId: Int
    let dataList: [String]
    let icon: String?
}

struct ParkingInfoItem: Codable {
    let title: String?
    let subTitle: String?
    let backgroundColor: String?
    let iconName: String?
    
    func getBackgroundColor() -> String {
        return backgroundColor ?? "#15B765"
    }
    
    func getSubTitle() -> String {
        return subTitle ?? ""
    }
    
    func getIconName() -> String {
        return iconName ?? "ParkingGreenIcon"
    }
    
    func getIconNameCarplay() -> String {
        return iconName ?? "carParkGreenIcon"
    }
}

struct BreezeTrafficParking: Codable {
    let priority: String
    let templateId: Int
    let dataList: [Parking]
    
    func isAllAvailable() -> Bool {
        var retValue: Bool = true
        for item in dataList {
            if let lots = Int(item.lots), lots <= 0 {
                retValue = false
                break
            }
        }
        return retValue
    }
    
    func isAllFull() -> Bool {
        var retValue: Bool = true
        for item in dataList {
            if let lots = Int(item.lots), lots > 0 {
                retValue = false
                break
            }
        }
        return retValue
    }
    
    func isFillingUpFast() -> Bool {
        var retValue: Bool = false
        var counter: Int = 0
        for item in dataList {
            if (item.color.lowercased() == "red" || item.color.lowercased() == "yellow") {
                counter += 1
            }
        }
        
        if (counter >= 3 || counter == dataList.count) {
            retValue = true
        }else {
            retValue = false
        }
        
        return retValue
    }
    
    private func getAllNameType(_ color: String) -> String {
        var names = ""
        for item in dataList {
            if item.color.lowercased() == color {
                if names.isEmpty {
                    names.append(item.location)
                }else {
                    names.append(", \(item.location)")
                }
            }
        }
        return names
    }
    
    func getSpokenText() -> String {
        var spokenText = ""
        if isAllAvailable() {
            if appDelegate().carPlayConnected {
                spokenText = "Parking Update - Nearby Carparks are all still available."
            } else {
                spokenText = "Parking Update - Nearby Carparks are all still available. You can select one to drive there now."
            }
            
        } else if isAllFull() {
            spokenText = "Parking Update - Nearby Carparks are all full. Expect to wait or park further away.”"
        } else if isFillingUpFast() {
            //  Read out names of carparks with yellow status
            spokenText = "Carparks nearby are filling up fast. Limited lots available at \(getAllNameType("yellow"))."
        } else {
            //  Read out names of carparks with green status
            if appDelegate().carPlayConnected {
                spokenText = "Parking Update - Nearby Carparks are still available."
            } else {
                spokenText = "Parking Update - Lots are still available at \(getAllNameType("green"))."
            }
        }
        return spokenText
    }
    
    func getInfoToDisplayOnMobile() -> ParkingInfoItem {        
        
        if isAllAvailable() {
            return ParkingInfoItem(title: "Carpark update", subTitle: "Nearby all available", backgroundColor: "#15B765", iconName: "ParkingGreenIcon")
        } else if isAllFull() {
            return ParkingInfoItem(title: "Carpark update", subTitle: "Nearby All Full", backgroundColor: "#E82370", iconName: "ParkingRedIcon")
        } else if isFillingUpFast() {
            return ParkingInfoItem(title: "Carpark update", subTitle: "Filling Up Fast", backgroundColor: "#F26415", iconName: "ParkingOrangeIcon")
        } else {
            return ParkingInfoItem(title: "Carpark update", subTitle: "Nearby still available", backgroundColor: "#15B765", iconName: "ParkingGreenIcon")
        }
    }
    
    func getInfoToDisplayOnCarplay() -> ParkingInfoItem {
        if isAllAvailable() {
            return ParkingInfoItem(title: "Parking \nUpdate", subTitle: "Nearby all available", backgroundColor: "#15B765", iconName: "carParkGreenIcon")
        } else if isAllFull() {
            return ParkingInfoItem(title: "Parking \nUpdate", subTitle: "Nearby All Full", backgroundColor: "#E82370", iconName: "carparkRedIcon")
        } else if isFillingUpFast() {
            return ParkingInfoItem(title: "Parking \nUpdate", subTitle: "Filling Up Fast", backgroundColor: "#F26415", iconName: "carParkOrangeIcon")
        } else {
            return ParkingInfoItem(title: "Parking \nUpdate", subTitle: "Nearby still available", backgroundColor: "#15B765", iconName: "carParkGreenIcon")
        }
    }
}

struct BreezeTravelTime: Codable {
    let priority: String
    let templateId: Int
    let dataList: [TravelTime]
    
    func getSpokenText() -> String {
        var spokenText = ""
//        if let first = dataList.first {
//            spokenText = "Based on current traffic conditions, estimated time to \(first.location) is \(first.min)."
//        }
        
        //  Updated on BREEZE2-55
        let heavyTraffics = dataList.filter { model in
            return model.color.lowercased() == "red"
        }
        
        if !heavyTraffics.isEmpty {
            spokenText.append("Heavy traffic ahead. Expect delays. ")
            for i in 0..<heavyTraffics.count {
                let traffic = heavyTraffics[i]
                
                //  Do convert min to minutes
                let estTime = traffic.min.replacingOccurrences(of: "min", with: "").replacingOccurrences(of: "mins", with: "").replacingOccurrences(of: "minute", with: "").replacingOccurrences(of: "minutes", with: "")
                spokenText.append("\(estTime) minutes to \(traffic.location)")
                if i != heavyTraffics.count - 1 {
                    spokenText.append(", ")
                }
            }
        }
        
        print("spokenText: \(spokenText)")
        
        return spokenText
    }
    
}

struct Parking: Codable {
    let location: String
    let lots: String
    let color: String
    
    func getIconName() -> String {
        switch color.lowercased() {
        case "red":
            return "carparkRedIcon"
        case "yellow":
            return "carParkOrangeIcon"
        default:
            return "carParkGreenIcon"
        }
    }
    
//    func getSubTitle() -> String {
//        switch color.lowercased() {
//        case "red":
//            return "Nearby all full"
//        case "yellow":
//            return "Filling up fast"
//        default:
//            return "Nearby all available"
//        }
//    }
    
}

struct TravelTime: Codable {
    let location: String
    let min: String
    let icon: String
    let color: String
    
    func getIconName() -> String {
        switch color.lowercased() {
        case "red":
            return "travelTimeRedIcon"
        case "yellow":
            return "travelTimeYellowIcon"
        default:
            return "travelTimeGreenIcon"
        }
    }
}

//  MARK - Payment History
struct BreezePaymentHistory: Codable {
    let sequentialNumber: Int?
    let paymentDate: String?
    let businessFunction: String?
    let chargeAmount: Double?
    let paymentMode: String?
    
    func getPostData() -> [String: Any] {
        return [
            "obuPaymentId": sequentialNumber as Any,
            "businessFunction": businessFunction as Any,
            "paymentDate": paymentDate as Any,
            "paymentMode": paymentMode as Any,
            "chargeAmount": chargeAmount as Any
        ]
    }
}

//  MARK - Travel summary
struct BreezeTravelSummary: Codable {
    let totalTravelTime: Double?
    let totalTravelDistance: Double?
    let totalTravelCharge: TravelCharge?
    
    func getPostDataWithTripID(_ tripGUID: String, vehicleNumber: String) -> [String: Any] {
        var datas: [String : Any] = [:]
        if let  travelTime = totalTravelTime, let travelDistance = totalTravelDistance {
            datas["totalTravelTime"] = travelTime
            datas["totalTravelDistance"] = travelDistance
        }
        if let travelCharge = totalTravelCharge {
            datas["totalTravelCharge"] = travelCharge.getPostData()
        }
        
        if !tripGUID.isEmpty {
            datas["tripGUID"] = tripGUID
        }
        
        if !vehicleNumber.isEmpty {
            datas["vehicleNumber"] = vehicleNumber
        }
        
        return datas
    }
}

struct TravelCharge: Codable {
    let erp: Double?
    let eep: Double?
    let rep: Double?
    let opc: Double?
    let cpt: Double?
    
    func getPostData() -> [String: Any] {
        return [
            "erp": erp as Any,
            "eep": eep as Any,
            "rep": rep as Any,
            "opc": opc as Any,
            "cpt": cpt as Any
        ]
    }
}

protocol BreezeObuDataDelegate {
    func onVelocityInformation(_ velocity: Double)
    func onChargingInformation(_ chargingInfo: [BreezeObuChargingInfo])
    func onTrafficInformation(_ trafficInfo: BreezeObuTrafficInfo)
    func onErpChargingAndTrafficInfo(trafficInfo: BreezeObuTrafficInfo?, chargingInfo: [BreezeObuChargingInfo]?)
    func onPaymentHistories(_ histories: [BreezePaymentHistory])
    func onTravelSummary(_ travelSummary: BreezeTravelSummary)
    func onTotalTripCharged(_ totalCharged: TravelCharge)
    func onError(_ errorCode: NSError)
    func onCardInformation(_ status: BreezeCardStatus?, paymentMode: BreezePaymentMode?, chargingPaymentMode: BreezeChargingPaymentMode?, balance: Int)
}

protocol BreezeObuConnectionDelegate {
    func onOBUDisconnected(_ device: OBUSDK.OBU, error: NSError?)
    func onOBUConnectionFailure(_ error: NSError)
    func onBluetoothStateUpdated(_ state: OBUSDK.BluetoothState)
    func onOBUConnected(_ device: OBUSDK.OBU)
}

protocol BreezeServerMockProtocol {
    func onReceiveObuEvent(_ event: ObuMockEventModel)
}

protocol BreezeObuProtocol: AnyObject, BreezeObuDataDelegate, BreezeObuConnectionDelegate, BreezeServerMockProtocol {
    
}

//
//  OBUHelper.swift
//  Breeze
//
//  Created by Tan, Tran Ngoc on 30/05/2023.
//

import Foundation
import OBUSDK
import CoreBluetooth
import UserNotifications
import SwiftyBeaver

enum ObuMode: String {
    case device
    case mockSDK
    case mockServer
}

enum ObuConnectMethod: String {
    case withoutVehicleNumber
    case withVehicleNumber
}

typealias ObuSearchCompletion = ([OBU])-> (Void)
typealias ConnectObuCompletion = (Bool, NSError?)-> (Void)
typealias DisconnectObuCompletion = (OBU, NSError?)-> (Void)


enum StateOBU: String {
    case IDLE
    case PAIRING
    case CONNECTED
}

class ObuErrorModel {
    
    let name: String?
    let vehicleNumber: String?
    let error: NSError?
    let isConnecting: Bool?
    
    init(name: String? = nil, vehicleNumber: String? = nil, error: NSError? = nil, isConnecting: Bool? = true) {
        self.name = name
        self.vehicleNumber = vehicleNumber
        self.error = error
        self.isConnecting = isConnecting
    }
}

//  Model for saving paired obu in local storage
struct PairedObuModel: Codable {
    
    let name: String?
    let vehicleNumber: String?
    let isLastPairedObu: Bool?
    
    func getDic() -> [String: Any] {
        return ["obuName": name as Any, "vehicleNumber": vehicleNumber as Any]
    }
    
    enum CodingKeys: CodingKey {
        case name
        case vehicleNumber
        case isLastPairedObu
    }
    
    init(name: String? = nil, vehicleNumber: String? = nil, isLastPairedObu: Bool? = true) {
        self.name = name
        self.vehicleNumber = vehicleNumber
        self.isLastPairedObu = isLastPairedObu
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(self.name, forKey: .name)
        try container.encodeIfPresent(self.vehicleNumber, forKey: .vehicleNumber)
        try container.encodeIfPresent(self.isLastPairedObu, forKey: .isLastPairedObu)
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.name = try container.decodeIfPresent(String.self, forKey: .name)
        self.vehicleNumber = try container.decodeIfPresent(String.self, forKey: .vehicleNumber)
        self.isLastPairedObu = try container.decodeIfPresent(Bool.self, forKey: .isLastPairedObu)
    }
}

class OBUHelper: NSObject, UNUserNotificationCenterDelegate {
    
    static var shared = OBUHelper()
    let OBU_SDK_KEY = "c3b35e9056b6a4b9c2e7f393e9021935"    // Pilot key: c3b35e9056b6a4b9c2e7f393e9021935, DEV key: e5963e855224a77fcf4ac756a325f9c9
    let SDK_ACCOUNT_KEY = "xXYGR5t/QvSDQTPDQFXIkg=="
    
    weak var obuDelegate: BreezeObuProtocol?
    
    //  This is mode for fetching obu data from real OBU device / mock manager / breeze server
    private var mode: ObuMode = .device {
        didSet {
            Prefs.shared.setValue(mode.rawValue, forkey: .obuMode)
        }
    }
    
    private var connectMethod: ObuConnectMethod = .withoutVehicleNumber {
        didSet {
            Prefs.shared.setValue(connectMethod.rawValue, forkey: .obuConnectMethod)
        }
    }
    
    //  Hard code payment mode for .breezeServer and .mockSDK
    private var paymentMode: BreezePaymentMode = .frontend {
        didSet {
            Prefs.shared.setValue(paymentMode.rawValue, forkey: .obuPaymentMode)
        }
    }
    
    //  Completion call back after finish searching nearby OBUs
    var searchCompletion: ObuSearchCompletion?
    
    //  Keep update found nearby obus every search func call
    private var foundOBUs: [OBU] = []
    
    //  Callback when success connected to an OBU
    var connectObuCompletion: ConnectObuCompletion?
    
    //  Callback when disconnect obut success
    var disConnectOBU: DisconnectObuCompletion?
    
    //  Hardcode card balance for demo trip from Breeze Server unit cents
    var CARD_BALANCE: Float = 10000.0
        
    var stateOBU: StateOBU = .IDLE {
        didSet {
            
            if stateOBU == .PAIRING {
                AnalyticsManager.shared.logSystemEvent(eventValue: ParameterName.SystemAnalytics.SystemEvent.obu_connecting, screenName: ParameterName.SystemAnalytics.screen_view)
            } else if stateOBU == .CONNECTED {
                AnalyticsManager.shared.logSystemEvent(eventValue: ParameterName.SystemAnalytics.SystemEvent.obu_connected, screenName: ParameterName.SystemAnalytics.screen_view)
            }
            
            //  Update obu connection status to RN once state changes
            if !obuName.isEmpty {
                if stateOBU == .CONNECTED {
                    if !autoConnectOBUFromCarplay {
                        shareStateObuConect(stateOBU.rawValue, cardBalance: getCardBalance(), obuName: obuName, vehicleNumber: vehicleNumber, paymentMode: getPaymentMode().rawValue)
                    }
                } else {
                    shareStateObuConect(stateOBU.rawValue, cardBalance: getCardBalance(), obuName: obuName, vehicleNumber: vehicleNumber, paymentMode: getPaymentMode().rawValue)
                }
            }
            
            appDelegate().updateStateObuOnCarplay()
        }
    }
    
    //  Current paired obu name and vehicle number
    var obuName: String = ""
    var vehicleNumber: String = ""
    
    var isCancelPairing: Bool = false
    var needObserveAutoDisconnected: Bool = false
    private var isBluetoothOn: Bool = false
    
    var centralManager: CBCentralManager?
    
    private var didShowMinimumCashCardBalance = false
    
    private var tripGUID: String = ""
    
    var willTriggerAutoConnect: Bool = false
    
    var autoConnectOBUFromCarplay: Bool = false
    
    //  Fake OBU device
    var fakeObus: [PairedObuModel] = [PairedObuModel(name: "1234567890", vehicleNumber: "SG@123"), PairedObuModel(name: "2345678901", vehicleNumber: "SG@234")]
    
    //  Temporary optimization
    var totalTravelTime: Int = -1
    var totalTravelDistance: Int = -1
    
    //  Idle Speed
    var idleSpeed: Double = 1.0
    
    var idleSpeedZero: Double = 5.0
    
    //  Cruise mode while connecting to Carplay
    var isCruiseModeFromObuDisplay = false
    
    //  Fix toggle connect obu twice
    var isProcessing = false
    
    //  Handle show single card error popup
    var didShowCardErrorPopup: Bool = true
    var lastCardStatusText: String = ""
    
    //  Keep no card status
    var isNoCard: Bool = false
    
    var globalCardBalance: Double = -1
    
    //  Specify for manually connect OBU then should not handle show disconnected toast message
    private var manuallyConnectObu: Bool = false
    
    var didShowToastOBUConnected: Bool = false
    
    override init() {
        super.init()
        
        loadLastPairedObu()
        
        SwiftyBeaver.debug("OBU - App version \("V\(UIApplication.appVersion())")")
        
        OBUSDKManager.shared.dataDelegate = self
        OBUSDKManager.shared.connectionStatusDelegate = self
        
        //  Get current OBU mode
        let obuMode = Prefs.shared.getStringValue(.obuMode)
        if !obuMode.isEmpty {
            if let currentMode = ObuMode(rawValue: obuMode) {
                setMode(currentMode)
            }
        } else {
            setMode(.device)
        }
        
        //  Get current OBU payment mode
        let obuPaymentMode = Prefs.shared.getStringValue(.obuPaymentMode)
        if !obuPaymentMode.isEmpty {
            if let currentMode = BreezePaymentMode(rawValue: obuPaymentMode) {
                self.paymentMode = currentMode
            }
        } else {
            setPaymentMode(.frontend)
        }
        
        //  Get current hard code card balance
        let balance = Prefs.shared.getIntValue(.obuCardBalance)
        if balance > 0 {
            CARD_BALANCE = Float(balance)
        }
        
        /*  Remove connect with Vehicle Number
        let obuConnectMethod = Prefs.shared.getStringValue(.obuConnectMethod)
        if !obuConnectMethod.isEmpty {
            if let method = ObuConnectMethod(rawValue: obuConnectMethod) {
                connectMethod = method
            }
        }
         */
        
        //  Register observer when carplay connected and map loaded
        NotificationCenter.default.addObserver(self, selector: #selector(carplayMapLoaded(_:)), name: .carplayMapLoaded, object: nil)
    }
    
    func loadLastPairedObu(_ shouldInitState: Bool = true) {
        //  Try to get last paired obu for auto connect
        if let lastObu = Prefs.shared.getLastPairedObu(AWSAuth.sharedInstance.cognitoId, key: .pairedObuList) {
            self.obuName = lastObu.name ?? ""
            self.vehicleNumber = lastObu.vehicleNumber ?? ""
            if shouldInitState {
                SwiftyBeaver.debug("OBU - loadLastPairedObu - OBU disconnected")
                self.stateOBU = .IDLE
            }
        } else {
            SwiftyBeaver.debug("OBU - loadLastPairedObu - Not Found")
            self.obuName = ""
            self.vehicleNumber = ""
        }
    }
    
    func resetObuConnection() {
        SwiftyBeaver.debug("OBU - resetObuConnection")
        if isObuConnected() {
            OBUSDKManager.shared.disconnectOBU()
        }
        stateOBU = .IDLE
        obuName = ""
        vehicleNumber = ""
        
        totalTravelTime = -1
        totalTravelDistance = -1
        
        lastCardStatusText = ""
        
        globalCardBalance = -1
    }
    
    func updatePairedVehicle() {
        let pairedObuList = Prefs.shared.getObuList(AWSAuth.sharedInstance.cognitoId, key: .pairedObuList)
        let datas: [[String: Any]] = pairedObuList.map { obu in
            return obu.getDic()
        }
        updatePairedVehicleList(["data": datas])
    }
    
    func getTripSummaryObuStatus() -> TripOBUStatus {
        var retValue: TripOBUStatus = .notPaired
        if OBUHelper.shared.isObuConnected() {
            retValue = .connected
        } else if OBUHelper.shared.hasLastObuData() {
            retValue = .notConnected
        }
        return retValue
    }
    
    func isObuConnected() -> Bool {
        return stateOBU == .CONNECTED
    }
    
    func isPairing() -> Bool {
        return stateOBU == .PAIRING
    }
    
    func hasLastObuData() -> Bool {
        return (!obuName.isEmpty/* && !vehicleNumber.isEmpty*/)
    }
    
    func didShowTopupSoon() -> Bool {
        return didShowMinimumCashCardBalance
    }
    
    func isOnBluetooth() -> Bool {
        return isBluetoothOn
    }
    
    func setBluetoothOn(_ isOn: Bool) {
        isBluetoothOn = isOn
    }
    
    func startMonitoringBluetooth() {
        if centralManager == nil {
            centralManager = CBCentralManager(delegate: self, queue: nil)
        }
    }
    
    func isBluetoothPermissionGranted() -> Bool {
        
        if #available(iOS 13.1, *) {
            print("CBCentralManager.authorization: \(CBCentralManager.authorization)")
            return CBCentralManager.authorization == .allowedAlways
        } else if #available(iOS 13.0, *) {
            print("CBCentralManager().authorization: \(CBCentralManager().authorization)")
            return CBCentralManager().authorization == .allowedAlways
        }
        
        // Before iOS 13, Bluetooth permissions are not required
        return true
    }
    
    func stopObuMockEvent() {
        obuDelegate = nil
        if self.mode != .device {
            self.configOBUDisplayMockData(false)
        }
    }
    
    func getObuMode() -> ObuMode {
        return self.mode
    }
    
    func getConnectMethod() -> ObuConnectMethod {
        return self.connectMethod
    }
    
    func setManuallyConnectObu(_ newValue: Bool) {
        manuallyConnectObu = newValue
    }
    
    func getManuallyConnectObu() -> Bool {
        return manuallyConnectObu
    }
    
    func setConnectMethod(_ method: ObuConnectMethod) {
        self.connectMethod = method
    }
    
    func setPaymentMode(_ mode: BreezePaymentMode) {
        SwiftyBeaver.debug("OBU - setPaymentMode: \(mode.rawValue)")
        self.paymentMode = mode
        Prefs.shared.setValue(paymentMode.rawValue, forkey: .obuPaymentMode)
    }
    
    func setMode(_ mode: ObuMode, delegate: BreezeObuProtocol? = nil) {
                
        obuDelegate = delegate
        
        switch mode {
        case .mockSDK:
            self.mode = mode
            //  Config mock with default from SDK
            self.configOBUDisplayMockData()
        case .mockServer:
            self.mode = mode
            //  Fetch mock data from Breeze server
            self.configOBUDisplayMockData(false)
        default:
            //  Real device - Get event data while driving
//            stopObuMockEvent()
            stopBreezeMockEvent()
            stopObuEvent()
            //  Try to disconnect current OBU when switching mode
            if isObuConnected() && mode != self.mode {
                self.resetObuConnection()
            }
            self.mode = mode
        }
    }
    
    func initializeSDK(_ completion: ((Bool, Error?) -> Void)? = nil) {
        if !OBUSDKManager.shared.isSDKInitialised() {
            SwiftyBeaver.debug("Initialize SDK key \(OBU_SDK_KEY)")
            OBUSDKManager.shared.initializeSDK(with: OBU_SDK_KEY) { result in
                switch result {
                case .success:
                    print("OBU SDK initialized successfully")
                    SwiftyBeaver.debug("OBU SDK initialized successfully")
                    AnalyticsManager.shared.logSystemEvent(eventValue: ParameterName.SystemAnalytics.SystemEvent.sdk_init_success, screenName: ParameterName.SystemAnalytics.screen_view, eventMessage: "")
                    completion?(true, nil)
                case .failure(let error):
                    print("OBU SDK initialization failed: \(error.localizedDescription)")
                    SwiftyBeaver.debug("OBU SDK initialization failed: \(error.localizedDescription)")
                    
//                    let message = "initializeSDK error: \(error.code) description: \(error.localizedDescription)"
                    let message = "err:\(error.code) des:\(error.localizedDescription)"
//                    AnalyticsManager.shared.logSystemEvent(eventValue: ParameterName.SystemAnalytics.SystemEvent.obu_verbose_error, screenName: ParameterName.SystemAnalytics.screen_view, eventMessage: message)
                    AnalyticsManager.shared.logSystemEvent(eventValue: ParameterName.SystemAnalytics.SystemEvent.sdk_init_failure, screenName: ParameterName.SystemAnalytics.screen_view, eventMessage: message)
                    completion?(false, error)
                }
            }
        }else {
            completion?(true, nil)
        }
    }
    
    func verifyDataPermissionGranted(_ completion: @escaping (Result<Bool>) -> ()) {
        if !OBUSDKManager.shared.isDataPermissionGranted() {
            OBUSDKManager.shared.requestOBUDataAccessPermission { result in
                AnalyticsManager.shared.logOpenPopupEvent(popupName: ParameterName.ObuPairing.UserPopup.popup_allow_obu_access, screenName: ParameterName.ObuPairing.screen_view)
                switch result {
                case .success(let status):
                    print("Permission granted: \(status)")
                    completion(Result.success(true))
//                    self.startSearch({ obuList in
//
//                    })
                case .failure(let failureError):
                    
                    let message = "err:\(failureError.code) des:\(failureError.localizedDescription)"
                    SwiftyBeaver.debug("OBU - connectation failed: \(message)")
                    AnalyticsManager.shared.logSystemEvent(eventValue: ParameterName.SystemAnalytics.SystemEvent.obu_verbose_error, screenName: ParameterName.SystemAnalytics.screen_view, eventMessage: message)
                    
                    completion(Result.failure(failureError))
                }
            }
        }else {
            print("Permission granted")
            completion(Result.success(true))
        }
    }
    
    
    func startSearch(_ completion: ObuSearchCompletion?) {
        SwiftyBeaver.debug("OBU - Start searching nearby OBU")
        foundOBUs.removeAll()
        self.searchCompletion = completion
        OBUSDKManager.shared.startSearch()
    }
    
    func stopSearch() {
        SwiftyBeaver.debug("OBU - Stop searching OBU")
        self.searchCompletion = nil
        OBUSDKManager.shared.stopSearch()
    }
    
    func connect(_ obu: OBU, completion: @escaping (Bool, NSError?) -> ()) {
        SwiftyBeaver.debug("OBU - Start connect to OBU: \(obu.name)")
        
        //  When user start connect new OBU should reset this cancel pairing
        isCancelPairing = false
        
        //  Reset show minimum card balance
        self.didShowMinimumCashCardBalance = false
        self.didShowCardErrorPopup = false
        self.lastCardStatusText = ""
        self.globalCardBalance = -1
        
        self.obuName = obu.name
        self.stateOBU = .PAIRING
        self.connectObuCompletion = completion
        
        self.didShowToastOBUConnected = false
        
        //  For now just need connect to OBU directly later will check if need connect again with Vehicle Number
        OBUSDKManager.shared.connectOBU(obu: obu)
    }
    
    func disconnectOBU(completion: ((OBU, NSError?) -> Void)? = nil) {
        self.disConnectOBU = completion
        self.stateOBU = .IDLE
        OBUSDKManager.shared.disconnectOBU()
        if appDelegate().carPlayConnected {
            appDelegate().obuNotConnected()
        }
        
    }
    
    /*
    func connectVehicleNumber(_ vehicleNumber: String, obu: OBU, completion: @escaping (Bool, NSError?) -> ()) {
        SwiftyBeaver.debug("OBU - Start connect to OBU: \(obu.name)")
        
        //  Reset show minimum card balance
        self.didShowMinimumCashCardBalance = false
        
        self.obuName = obu.name
        self.vehicleNumber = vehicleNumber
        self.stateOBU = .PAIRING
        self.connectObuCompletion = completion
        
        OBUSDKManager.shared.connectToOBU(vehicleId: vehicleNumber)
    }
     */
    
    func needShowMinimumCashCardBalance() -> Bool {
        if isObuConnected(), let balance = Float(getCardBalance()), balance < 5.0 {
            return true
        }
        return false
    }
    
    func getPriorityChargingInfo(_ chargingInfo: [BreezeObuChargingInfo]) -> BreezeObuChargingInfo? {
        var retValue: BreezeObuChargingInfo?
        //  Get priority charging information
        
        //  1. Try to get deduction success
        let deductionSuccess = chargingInfo.filter { model in
            let type = model.getType()
            return (type == ObuEventType.ERP_SUCCESS.rawValue || type == ObuEventType.PARKING_SUCCESS.rawValue)
        }
        retValue = deductionSuccess.first
        
        //  If not have success deduction
        if retValue == nil {
            //  2. Try to get deduction failure
            let deductionFailure = chargingInfo.filter { model in
                let type = model.getType()
                return (type == ObuEventType.ERP_FAILURE.rawValue || type == ObuEventType.PARKING_FAILURE.rawValue)
            }
            retValue = deductionFailure.first
        }
        
        //  If not have failure deduction
        if retValue == nil {
            //  3. Get charging info
            retValue = chargingInfo.first
        }
        
        return retValue
    }
    
    func setTripGUID(_ ID: String) {
        self.tripGUID = ID
    }
    
    func getLocalVehicleNameToDisplay(_ obuName: String?) -> String {
        let pairedList = Prefs.shared.getObuList(AWSAuth.sharedInstance.cognitoId, key: .pairedObuList)
        if !pairedList.isEmpty {
            for item in pairedList {
                if item.name?.lowercased() == obuName?.lowercased() {
                    if let name = item.vehicleNumber, !name.isEmpty {
                        return name
                    }
                }
            }
        }
        
        //  Not found vehicle name
        return obuName?.maskedObuName() ?? ""
    }
}

//  MARK - OBU help functions
extension OBUHelper {
    
    func isLowBalance() -> Bool {
        if mode == .device {
            if isObuConnected() {
                if isNoCard && getPaymentMode() == .backend {
                    return false
                } else {
                    if OBUHelper.shared.getCardStatus() == .detected {
                        if let balanceValue = OBUSDKManager.shared.getCardBalance() {
                            let balance = Double(balanceValue) / 100
                            SwiftyBeaver.debug("OBU - isLowBalance - \(balance)")
                            return balance < 5.0
                        } else {
                            if globalCardBalance > 0 {
                                SwiftyBeaver.debug("OBU - isLowBalance - globalCardBalance \(globalCardBalance)")
                                return (globalCardBalance / 100) < 5.0
                            } else {
                                SwiftyBeaver.debug("OBU - isLowBalance - Can not get card balance")
                                return false
                            }
                        }
                    } else {
                        SwiftyBeaver.debug("OBU - isLowBalance - card is not detected")
                        return false
                    }
                }                
            } else {
                return false
            }
            
        }else {
            let balance = CARD_BALANCE / 100
            return balance < 5.0
        }
    }
    
    func getCardBalance() -> String {
        if mode == .device {
            if isObuConnected() {
                if let obuBalance = OBUSDKManager.shared.getCardBalance() {
                    let balance = Double(obuBalance)
                    let retValue = String(format: "%.2f", balance / 100)
                    return retValue
                } else {
                    //  Try to get global card balance from onCardInformation update
                    if globalCardBalance > 0 {
                        let retValue = String(format: "%.2f", globalCardBalance / 100)
                        SwiftyBeaver.debug("OBU - getCardBalance - globalCardBalance: \(retValue)")
                        return retValue
                    } else {
                        SwiftyBeaver.debug("OBU - getCardBalance does not return a value")
                        return ""
                    }
                }
            } else {
                return ""
            }
            
        }else {
            let retValue = String(format: "%.2f", CARD_BALANCE / 100)
            print("retValue: \(retValue)")
            return retValue
        }
    }
    
    func getPaymentMode() -> BreezePaymentMode { //OBUPaymentMode? {
        
        var retValue: BreezePaymentMode = .undefined
        
        if isObuConnected() {
            if mode == .device {
                let paymentMode = OBUSDKManager.shared.getPaymentMode()
                if let mode = paymentMode {
                    switch mode {
                    case .frontend:
                        retValue = .frontend
                    case .backend:
                        retValue = .backend
                    case .businessFunctionDisabled:
                        retValue = .businessFunctionDisabled
                    }
                }
            } else {
                retValue = self.paymentMode
            }
        }
        
        print("Payment mode: \(retValue.rawValue)")
        SwiftyBeaver.debug("OBU - getPaymentMode: \(retValue.rawValue)")
        
        return retValue
    }
    
    func getCardStatus() -> OBUCardStatus? {
        return OBUSDKManager.shared.getCardStatus()
    }
    
    func getPaymentHistories() -> [OBUPaymentHistory]? {
        return OBUSDKManager.shared.getPaymentHistories()
    }
    
    func getLastObuData() -> OBUData? {
        return OBUSDKManager.shared.getLastOBUData()
    }
    
    func getPairedDevices(_ completion: @escaping OBUSDK.Handler<[OBUSDK.OBU]?>) {
        OBUSDKManager.shared.getPairedDevices(completion)
    }
    
    func getLastTravelSummary() -> OBUTravelSummary? {
        return OBUSDKManager.shared.getLastTravelSummary()
    }
    
    func getLastTrafficInfo() -> OBUTrafficInfo? {
        return OBUSDKManager.shared.getLastTrafficInfo()
    }
    
    func isSDKInitialised() -> Bool {
        return OBUSDKManager.shared.isSDKInitialised()
    }
}

extension OBUHelper: OBUSDK.OBUConnectionDelegate {

    func onOBUDisconnected(_ device: OBUSDK.OBU, error: NSError?) {
        SwiftyBeaver.debug("OBU - Disconnected obu: \(device.name), code:\(error?.code ?? 0) desc: \(error?.localizedDescription ?? "")")
        
        AnalyticsManager.shared.logSystemEvent(eventValue: ParameterName.SystemAnalytics.SystemEvent.obu_disconnect, screenName: ParameterName.SystemAnalytics.screen_view)
        
        if let err = error {
            let message = "err:\(err.code) des:\(err.localizedDescription)"
            AnalyticsManager.shared.logSystemEvent(eventValue: ParameterName.SystemAnalytics.SystemEvent.obu_verbose_error, screenName: ParameterName.SystemAnalytics.screen_view, eventMessage: message)
        }
        
        if (UIApplication.shared.delegate as! AppDelegate).appInBackground == true && willTriggerAutoConnect {
            willTriggerAutoConnect = true
        } else {
            willTriggerAutoConnect = false
        }
        
        stateOBU = .IDLE
        print("onOBUDisconnected: \(error?.localizedDescription ?? "")")
        self.disConnectOBU?(device, error)
        
        //  Should send error because OBU suddenly disconnected
        
        if needObserveAutoDisconnected {
            if let err = error {
                let model = ObuErrorModel(name: obuName, vehicleNumber: vehicleNumber, error: err, isConnecting: true)
                NotificationCenter.default.post(name: .obuConnectFailure, object: model)
            }
        }
        
//        if mode != .mockServer {
            obuDelegate?.onOBUDisconnected(device, error: error)
//        }
    }

    func onOBUFound(_ device: [OBUSDK.OBU]) {
        let obuNames = device.map { obu in
            return obu.name
        }
        SwiftyBeaver.debug("OBU - Found nearby OBUs: \(obuNames)")
        
        //  Save found device in memory
        foundOBUs.removeAll()
        if !device.isEmpty {
            foundOBUs.append(contentsOf: device)
        }
        
        //  Send notification to keep update obu list
        NotificationCenter.default.post(name: .obuOnSearchingUpdates, object: foundOBUs)
        
        //  Callback return all found OBUs
        self.searchCompletion?(self.foundOBUs)
        self.searchCompletion = nil
    }

    func onOBUConnectionFailure(_ error: NSError) {
        
        SwiftyBeaver.debug("OBU - Connection failure: \(error.localizedDescription)")
        
        AnalyticsManager.shared.logSystemEvent(eventValue: ParameterName.SystemAnalytics.SystemEvent.obu_connect_failed, screenName: ParameterName.SystemAnalytics.screen_view)
        
//        let message = "onOBUConnectionFailure error: \(error.code) description: \(error.localizedDescription)"
        let message = "err:\(error.code) des:\(error.localizedDescription)"
        AnalyticsManager.shared.logSystemEvent(eventValue: ParameterName.SystemAnalytics.SystemEvent.obu_verbose_error, screenName: ParameterName.SystemAnalytics.screen_view, eventMessage: message)
        
#if targetEnvironment(simulator)
        self.connectObuCompletion?(true, nil)
        self.connectObuCompletion = nil
        self.stateOBU = .PAIRING
        DispatchQueue.main.asyncAfter(deadline: .now() + 10.0) {
            // Call your function here
            self.obuName = "1234567890"
            self.vehicleNumber = "VN123"
            self.stateOBU = .CONNECTED
        }
#else
        stateOBU = .IDLE
        self.connectObuCompletion?(false, error)
        self.connectObuCompletion = nil
        //  Fire notification to handle pairing error in maplanding
        let model = ObuErrorModel(name: obuName, vehicleNumber: vehicleNumber, error: error, isConnecting: true)
        NotificationCenter.default.post(name: .obuUpdatePairingStatus, object: model)
        
#endif
        print("onOBUConnectionFailure: \(error.localizedDescription)")
        
        obuDelegate?.onOBUConnectionFailure(error)
    }

    func onBluetoothStateUpdated(_ state: OBUSDK.BluetoothState) {
        
        SwiftyBeaver.debug("OBU - Bluetooth update status: \(state.rawValue)")
        
        print("onBluetoothStateUpdated: \(state)")
                
        obuDelegate?.onBluetoothStateUpdated(state)
    }

    func onSearchFinished() {
        SwiftyBeaver.debug("OBU - Search OBU finish")
        NotificationCenter.default.post(name: .obuOnSearchingFinished, object: foundOBUs)
        self.searchCompletion?(self.foundOBUs)
        self.searchCompletion = nil
    }

    func onOBUConnected(_ device: OBUSDK.OBU) {
        
        willTriggerAutoConnect = false
        
        SwiftyBeaver.debug("OBU - Connected to obu: \(device.name)")
        print("onOBUConnected: \(device.name)")
        
        if isCancelPairing {
            self.isCancelPairing = false
            //  Disconnect current OBU
            
            SwiftyBeaver.debug("OBU - Cancelling connected OBU")
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [weak self] in
                OBUSDKManager.shared.disconnectOBU()
                self?.loadLastPairedObu()
            }
            self.connectObuCompletion?(false, nil)
            self.connectObuCompletion = nil

        } else {
            
            //  When connection success save local storage
            obuName = device.name
            let pairedObu = PairedObuModel(name: device.name, vehicleNumber: self.vehicleNumber)
            Prefs.shared.addObu(AWSAuth.sharedInstance.cognitoId, key: .pairedObuList, obuModel: pairedObu)
            
            //  Handle completion callback
            self.stateOBU = .CONNECTED
            self.connectObuCompletion?((self.stateOBU == .CONNECTED), nil)
            self.connectObuCompletion = nil
            
            obuDelegate?.onOBUConnected(device)
            
            if appDelegate().carPlayConnected {
                didShowToastOBUConnected = true
                appDelegate().obuConnected()
            }
        }
        
    }
    
    func showLocalNotification() {
        if stateOBU == .CONNECTED {
            let content = UNMutableNotificationContent()
            
            //  adding title, subtitle, body and badge
            content.title = "OBU successfully paired!"
            content.body = "Tap “Connect” to manually connect with every new session or choose auto-connect in settings."
            content.badge = 1
            
            let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false)
            
            let request = UNNotificationRequest(identifier: "BreezeConnectOBUSuccessNotification", content: content, trigger: trigger)
            
            UNUserNotificationCenter.current().delegate = self
            
            //  adding the notification to notification center
            UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
        }
    }
    
}

extension OBUHelper: OBUSDK.OBUDataDelegate {

    /// Returns the velocity of the vehicle by the connected OBU.
    func onVelocityInformation(_ velocity: Double) {
        print("velocity: \(velocity)")
        if mode != .mockServer {
            obuDelegate?.onVelocityInformation(velocity)
        }
    }

    /// Returns the acceleration of the vehicle by the connected OBU.
    func onAccelerationInformation(_ acceleration: OBUSDK.OBUAcceleration) {
        print("acceleration x:\(acceleration.x) y: \(acceleration.y) z:\(acceleration.z)")
        //  Nothing to be done here
    }

    /// Returns the Electronic Road Pricing (ERP) message  by the connected OBU.
    func onChargingInformation(_ chargingInfo: [OBUSDK.OBUChargingInformation]) {
        
        let infos = chargingInfo.map { info in
            return getChargingMessageTypeText(info.chargingMessageType)
        }
        
        SwiftyBeaver.debug("OBU - onChargingInformation: \(infos)")
        
        //  Convert data
        var infoList: [BreezeObuChargingInfo] = []
        for info in chargingInfo {
            let breezeChargingInfo = BreezeObuChargingInfo(businessFunction: getBusinessFuncText(info.businessFunction), cardStatus: getCardStatusText(info.cardStatus).rawValue, content1: info.content1, content2: info.content2, content3: info.content3, content4: info.content4, chargingAmount: info.chargingAmount, minChargeAmount: info.minChargeAmount, startTime: info.startTime, endTime: info.endTime, chargingType: getChargingTypeText(info.chargingType), chargingMessageType: getChargingMessageTypeText(info.chargingMessageType), roadName: "")
            infoList.append(breezeChargingInfo)
        }
        
        for item in infoList {
            SwiftyBeaver.debug("OBU - onChargingInformation: \(item.getNotiDic())")
        }
        obuDelegate?.onChargingInformation(infoList)
        
        //  Send OBU data when obu event trigger for charging info
        if self.tripGUID.isEmpty {
            var roadName = ""
            if let chargingInfo = getPriorityChargingInfo(infoList) {
                roadName = chargingInfo.content1 ?? ""
            }
            
            let lat = LocationManager.shared.location.coordinate.latitude
            let long = LocationManager.shared.location.coordinate.longitude
            
            ObuDataService().sendTripObuErp(infoList, tripGUID: "", roadName: roadName, lat: lat, long: long) { result in
                switch result {
                case .success(_):
                    print("Send charging information success")
                case .failure(let error):
                    print("Send charging information failed: \(error.localizedDescription)")
                }
            }
            
            if let first = getPriorityChargingInfo(infoList) {
                let type = first.getType()
                if type == ObuEventType.ERP_SUCCESS.rawValue || type == ObuEventType.ERP_FAILURE.rawValue  {
                    if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                        if appDelegate.appInBackground {
                            appDelegate.showLocalNotificationCharges(title: "ERP Fee - \(first.getChargeAmount())", subTitle: first.getMessage())
                        } else {
                            if type == ObuEventType.ERP_SUCCESS.rawValue {
                                appDelegate.showOnAppERPNotification(title: "ERP Fee - \(first.getChargeAmount())", subTitle: first.getMessage())
                            } else {
                                appDelegate.showOnAppERPNotification(title: "ERP Fee - \(first.getChargeAmount())", subTitle: first.getMessage(), isSuccess: false)
                            }
                        }
                    }
                    
                } else if type == ObuEventType.PARKING_FAILURE.rawValue || type == ObuEventType.PARKING_SUCCESS.rawValue {
                    appDelegate().showLocalNotificationCharges(title: "Parking Fee - \(first.getChargeAmount())", subTitle: first.getMessage())
                    if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                        if appDelegate.appInBackground {
                            appDelegate.showLocalNotificationCharges(title: "Parking Fee - \(first.getChargeAmount())", subTitle: first.getMessage())
                        } else {
                            if type == ObuEventType.PARKING_SUCCESS.rawValue {
                                appDelegate.showOnAppParkingNotification(title: "Parking Fee - \(first.getChargeAmount())", subTitle: first.getMessage())
                            } else {
                                appDelegate.showOnAppParkingNotification(title: "Parking Fee - \(first.getChargeAmount())", subTitle: first.getMessage(), isSuccess: false)
                            }
                        }
                    }
                }
            }
            
        }
    }

    /// Traffic info of the connected OBU.
    func onTrafficInformation(_ trafficInfo: OBUSDK.OBUTrafficInfo) {
        let traffics = trafficInfo
        
        SwiftyBeaver.debug("OBU - onTrafficInformation: \(traffics)")
        print("onTrafficInformation: \(trafficInfo)")
        
        //  Convert data
        let (trafficText, trafficParking, travelTime) = getTrafficInfo(trafficInfo)
        let traffic: BreezeObuTrafficInfo = BreezeObuTrafficInfo(trafficText: trafficText, trafficParking: trafficParking, travelTime: travelTime)
        obuDelegate?.onTrafficInformation(traffic)
    }

    /// Traffic info and ERP of the connected OBU.
    func onErpChargingAndTrafficInfo(trafficInfo: OBUSDK.OBUTrafficInfo?, chargingInfo: [OBUSDK.OBUChargingInformation]?) {
        print("onErpChargingAndTrafficInfo: \(chargingInfo?.first?.chargingAmount ?? 0)")
    }

    /// Payment History of the connected OBU.
    func onPaymentHistories(_ histories: [OBUSDK.OBUPaymentHistory]) {
        for history in histories {
            SwiftyBeaver.debug("OBU - onPaymentHistories charge ammount: \(history.chargeAmount), payment mode: \(history.paymentMode), type: \(getBusinessFuncText(history.businessFunction)), paymentDate: \(history.paymentDate ?? "")")
            print("Charge amount: \(history.chargeAmount), payment mode: \(history.paymentMode)")
        }
        
        //  Convert data
        var datas: [BreezePaymentHistory] = []
        for history in histories {
            let item = BreezePaymentHistory(sequentialNumber: history.sequentialNumber, paymentDate: history.paymentDate, businessFunction: getBusinessFuncText(history.businessFunction), chargeAmount: Double(history.chargeAmount), paymentMode: getChargingPaymentModeText(history.paymentMode).rawValue)
            datas.append(item)
        }
        obuDelegate?.onPaymentHistories(datas)
        
        //  Send OBU data when obu event trigger transaction history
        if self.tripGUID.isEmpty {
            ObuDataService().sendObuTransactions(datas, tripGUID: "") { result in
                switch result {
                case .success(_):
                    print("Send transactions success")
                case .failure(let error):
                    print("Send transactions failed: \(error.localizedDescription)")
                }
            }
        }
    }

    /// Travel Summary of the last trip, from ignition OFF to ON.
    func onTravelSummary(_ travelSummary: OBUSDK.OBUTravelSummary) {
        let summary = BreezeTravelSummary(totalTravelTime: Double(travelSummary.totalTravelTime), totalTravelDistance: Double(travelSummary.totalTravelDistance), totalTravelCharge: nil)
        obuDelegate?.onTravelSummary(summary)
        SwiftyBeaver.debug("OBU - onTravelSummary: travelTime \(summary.totalTravelTime ?? 0), distance: \(summary.totalTravelDistance ?? 0), charge: \(summary.getPostDataWithTripID("", vehicleNumber: obuName))")
                
        if self.tripGUID.isEmpty {
            //  Need to handle how to reduce duplicated call API because this callback call multiple times with the same data
            if totalTravelTime != travelSummary.totalTravelTime || totalTravelDistance != travelSummary.totalTravelDistance {
                
                totalTravelTime = travelSummary.totalTravelTime
                totalTravelDistance = travelSummary.totalTravelDistance
                
                ObuDataService().sendObuTripSummary(summary, tripGUID: "") { result in
                    switch result {
                    case .success(_):
                        print("Send travel summary success")
                    case .failure(let error):
                        print("Send travel summary failed: \(error.localizedDescription)")
                    }
                }
            }
        }
    }

    /// Total Trip charged; from ignition ON to OFF.
    func onTotalTripCharged(_ totalCharged: OBUSDK.OBUTotalTripCharged) {
        
        //  Convert data
        let travelCharge = TravelCharge(erp: Double(totalCharged.totalERP2Charged), eep: Double(totalCharged.totalEEPCharged), rep: Double(totalCharged.totalREPCharged), opc: Double(totalCharged.totalOPCCharged), cpt: Double(totalCharged.totalCPTCharged))
        let summary = BreezeTravelSummary(totalTravelTime: nil, totalTravelDistance: nil, totalTravelCharge: travelCharge)
        obuDelegate?.onTravelSummary(summary)
        
        SwiftyBeaver.debug("OBU - onTotalTripCharged: \(summary.getPostDataWithTripID("", vehicleNumber: obuName))")
        
        //  Send total trip charged when connected OBU
        if tripGUID.isEmpty {
            ObuDataService().sendObuTripSummary(summary, tripGUID: "") { result in
                switch result {
                case .success(_):
                    print("Send travel summary success")
                case .failure(let error):
                    print("Send travel summary failed: \(error.localizedDescription)")
                }
            }
        }
    }

    /// Errors related to OBU data.
    func onError(_ errorCode: NSError) {
        SwiftyBeaver.debug("OBU - onError: \(errorCode.code) des: \(errorCode.localizedDescription)")
        
        let message = "onError:\(errorCode.code) des:\(errorCode.localizedDescription)"
        AnalyticsManager.shared.logSystemEvent(eventValue: ParameterName.SystemAnalytics.SystemEvent.obu_verbose_error, screenName: ParameterName.SystemAnalytics.screen_view, eventMessage: message)
        
        obuDelegate?.onError(errorCode)
    }

    /// Card information.
    func onCardInformation(_ status: OBUSDK.OBUCardStatus?, paymentMode: OBUSDK.OBUPaymentMode?, chargingPaymentMode: OBUSDK.OBUChargingPaymentMode?, balance: Int) {
        
        //  Send event to delegate
        let cardStatus = getCardStatusText(status ?? .undefined)
        let paymentMode = getPaymentModeText(paymentMode)
        obuDelegate?.onCardInformation(cardStatus, paymentMode: paymentMode, chargingPaymentMode: getChargingPaymentModeText(chargingPaymentMode ?? .undefined), balance: balance)
        SwiftyBeaver.debug("OBU - onCardInformation status: \(getCardStatusText(status ?? .undefined)), balance: \(balance) cents, mode: \(paymentMode.rawValue)")
        
        //  Trigger system event when user change card status
        let analyticCardStatus = getCardStatusTextAnalytic(status ?? .undefined)
        if analyticCardStatus != lastCardStatusText {
            lastCardStatusText = analyticCardStatus
            
            SwiftyBeaver.debug("OBU - Card status get change: \(analyticCardStatus)")
            
            AnalyticsManager.shared.logSystemEvent(eventValue: ParameterName.SystemAnalytics.SystemEvent.obu_card_status, screenName: ParameterName.SystemAnalytics.screen_view, eventMessage: lastCardStatusText)
            
            //  Handle show card error BREEZE2-825
            if self.tripGUID.isEmpty {
                //  Only handle error in other page except OBU lite/ Navigation/ Cruise
                if (cardStatus == .error && !didShowCardErrorPopup) {
                    didShowCardErrorPopup = true
                    SwiftyBeaver.debug("OBU - Show card error popup")
                    self.showCardErrorPopup()
                }
            }
            
            //  Set no card balance
//            isNoCard = status == .noCardForFrontendPayment
            isNoCard = false
            setNocard(["data": isNoCard])
        }
        
        //  Handle minimize cash card balance
        if status == .detected {
            
            globalCardBalance = Double(balance)
            
            //  If card detected
            var balanceString = String(format: "%.2f", Double(balance) / 100.0)
            
            if mode == .device {
                let cashCardParams: [String: Any] = ["status": cardStatus.rawValue, "balance": balanceString, "paymentMode": paymentMode.rawValue]
                updateCashCardInfo(cashCardParams)
            } else {
                balanceString = String(format: "%.2f", Double(CARD_BALANCE) / 100.0)
                
                let cashCardParams: [String: Any] = ["status": cardStatus.rawValue, "balance": balanceString, "paymentMode": self.paymentMode.rawValue]
                updateCashCardInfo(cashCardParams)
            }
            
            //  Check if not yet show minimum cash card balance then try to show it up once
            if isObuConnected(), let balance = Double(balanceString), balance < 5.0, !didShowMinimumCashCardBalance {
                didShowMinimumCashCardBalance = true
                if mode == .device || mode == .mockServer {
                    appDelegate().needShowMinimumCashcardBalance = true
                    NotificationCenter.default.post(name: .obuDidGetMinimumCardBalance, object: true)
                }
            }
            
            appDelegate().updateStateObuOnCarplay()
        } else {
            if mode == .device {
                var cashCardParams: [String: Any] = ["status": cardStatus.rawValue, "balance": "", "paymentMode": paymentMode.rawValue]
                updateCashCardInfo(cashCardParams)
            } else {
                let cashCardParams: [String: Any] = ["status": cardStatus.rawValue, "balance": "", "paymentMode": self.paymentMode.rawValue]
                updateCashCardInfo(cashCardParams)
            }
        }
    }
}

//  MARK - Create mock event for OBU display
extension OBUHelper {
    private func configOBUDisplayMockData(_ isEnable: Bool = true) {
                
        if isEnable {
            // Step 1:
            /*
             1. ERP charging
             2. School Zone
             3. Carpark availability
             4. Travel time
            */
            let events = [MockEvent(electronicEventList: [PointBasedAlertPointDetected(chargeAmount: 100)], tdcidEvent: nil),
                          MockEvent(electronicEventList: [PointBasedDeductionFailure()], tdcidEvent: nil),
                          MockEvent(electronicEventList: [], tdcidEvent: TdcidEvent.template12B),
                          MockEvent(electronicEventList: [], tdcidEvent: TdcidEvent.template110A),
                          MockEvent(electronicEventList: [], tdcidEvent: TdcidEvent.template111A)
            ]
            
//            let events = [MockEvent(electronicEventList: [], tdcidEvent: TdcidEvent.template110A),
//                          MockEvent(electronicEventList: [], tdcidEvent: TdcidEvent.template111A)
//            ]
            
    //        let events = [MockEvent(electronicEventList: [PointBasedAlertPointDetected(chargeAmount: 100)], tdcidEvent: TdcidEvent.template110A),
    //                      MockEvent(electronicEventList: [PointBasedCharging(chargeAmount: 100)], tdcidEvent: TdcidEvent.template111A),
    //                      MockEvent(electronicEventList: [PointBasedDeductionFailure()], tdcidEvent: TdcidEvent.template12B),
    //                      MockEvent(electronicEventList: [EEPCPOInformation()], tdcidEvent: TdcidEvent.template12F)
    //        ]
            
            //  Traffic
            //  ERP
            //  Parking
            //  Card Error
            //  School Zone: TdcidEvent.template12B
            //  Limit Speed: TdcidEvent.template12F
            //  Carpark Availability: TdcidEvent.template110A
            //  Travel Time: TdcidEvent.template111A
            
            //  Deduct event causing crashed app issue
    //        MockEvent(electronicEventList: [PointBasedDeductionSuccessful(chargeAmount:100)], tdcidEvent: nil)
                          
            // Step 2:
            let settings = MockBuilder().setCardBalance(Int(CARD_BALANCE)) //$10.00
                                        .setTimeInterval(10) //2 sec
                                        .setCyclicMode(true)
                                        .setSequence(events)                                        
                                        .build()
            // Step 3:
            OBUSDKManager.shared.enableMockSetting(settings)
        }else {
            let events = [MockEvent(electronicEventList: [PointBasedAlertPointDetected(chargeAmount: Int(CARD_BALANCE))], tdcidEvent: nil)]
            let settings = MockBuilder().setCardBalance(Int(CARD_BALANCE)) //$10.00
                                        .setTimeInterval(2) //2 sec
                                        .setCyclicMode(false)
                                        .setSequence(events)
                                        .build()
            // Step 3:
            OBUSDKManager.shared.enableMockSetting(settings)
        }
        
    }
}

//  MARK - Utils function
extension OBUHelper {
    private func getBusinessFuncText(_ bisFunc: OBUSDK.OBUBusinessFunction) -> String {
        /// Unknown, ERP, EEP, EPS, REP, OPC, CPT -> Ignore REP, OPC, CPT for now (that is prepared for the future)
        var retValue = "Unknown"
        switch bisFunc {
        case .ERP:
            retValue = "ERP"
        case .EEP:
            retValue = "EEP"
        case .EPS:
            retValue = "EPS"
        default:
            retValue = "Unknown"
        }
        return retValue
    }
    
    private func getChargingTypeText(_ chargingType: OBUSDK.OBUChargingType) -> String {
        var retValue = "Undefined"
        switch chargingType {
        case .pointBased:
            retValue = "PointBased"
        case .distanceBased:
            retValue = "DistanceBased"
        case .entryExitBased:
            retValue = "EntryExitBased"
        case .erp1:
            retValue = "Erp1"
        case .common:
            retValue = "Common"
        case .undefined:
            retValue = "Undefined"
        }
        return retValue
    }
    
    private func getChargingMessageTypeText(_ chargingMessageType: OBUSDK.OBUChargingMessageType) -> String {
        var retValue = "Undefined"
        switch chargingMessageType {
        case .undefined:
            retValue = "Undefined"
        case .alertPoint:
            retValue = "AlertPoint"
        case .enteringPricingRoad:
            retValue = "EnteringPricingRoad"
        case .charging:
            retValue = "Charging"
        case .deductionSuccessful:
            retValue = "DeductionSuccessful"
        case .deductionFailure:
            retValue = "DeductionFailure"
        case .cpoInformation:
            retValue = "CPOInformation"
        case .detectAlertPointCommonAlert:
            retValue = "DetectAlertPointCommonAlert"
        }
        return retValue
    }
    
    private func getCardStatusText(_ status: OBUSDK.OBUCardStatus) -> BreezeCardStatus {
        var retValue: BreezeCardStatus = .error
        switch status {
        case .detected:
            retValue = .valid
        case .insufficientStoredValue:
            retValue = .insufficent
        case .expiredStored:
            retValue = .expired
        default:
            retValue = .error
        }
        return retValue
        
        /*
        switch status {
        case .detected:
            retValue = "detected"
        case .blacklisted:
            retValue = "blacklisted"
        case .expiredStored:
            retValue = "expiredStored"
        case .invalid:
            retValue = "invalid"
        case .IssuerIDError:
            retValue = "IssuerIDError"
        case .noCardForFrontendPayment:
            retValue = "noCardForFrontendPayment"
        case .blocked:
            retValue = "blocked"
        case .faultyStoreValue:
            retValue = "faultyStoreValue"
        case .insufficientStoredValue:
            retValue = "insufficientStoredValue"
        case .wrongStoredValueDebitCertificate:
            retValue = "wrongStoredValueDebitCertificate"
        case .nfcAccessError:
            retValue = "nfcAccessError"
        case .undefined:
            retValue = "undefined"
        }
        return retValue
        */
        
    }
    
    private func getCardStatusTextAnalytic(_ status: OBUSDK.OBUCardStatus) -> String {
        var retValue: String = "undefined"
        
        switch status {
        case .detected:
            retValue = "detected"
        case .blacklisted:
            retValue = "blacklisted"
        case .expiredStored:
            retValue = "expiredStored"
        case .invalid:
            retValue = "invalid"
        case .IssuerIDError:
            retValue = "IssuerIDError"
        case .noCardForFrontendPayment:
            retValue = "noCardForFrontendPayment"
        case .blocked:
            retValue = "blocked"
        case .faultyStoreValue:
            retValue = "faultyStoreValue"
        case .insufficientStoredValue:
            retValue = "insufficientStoredValue"
        case .wrongStoredValueDebitCertificate:
            retValue = "wrongStoredValueDebitCertificate"
        case .nfcAccessError:
            retValue = "nfcAccessError"
        case .undefined:
            retValue = "undefined"
        }
        return retValue
    }
    
    func getPaymentModeText(_ paymentMode: OBUSDK.OBUPaymentMode?) -> BreezePaymentMode {
        var retValue: BreezePaymentMode = .undefined
        if let mode = paymentMode {
            switch mode {
            case .frontend:
                retValue = .frontend
            case .backend:
                retValue = .backend
            case .businessFunctionDisabled:
                retValue = .businessFunctionDisabled
            }
        }
        return retValue
    }
    
    private func getChargingPaymentModeText(_ paymentMode: OBUSDK.OBUChargingPaymentMode) -> BreezeChargingPaymentMode {
        var retValue: BreezeChargingPaymentMode = .undefined
        switch paymentMode {
        case .frontend:
            retValue = .frontend
        case .backend:
            retValue = .backend
        case .undefined:
            retValue = .undefined
        }
        return retValue
    }
    
    private func getTrafficInfo(_ info: OBUSDK.OBUTrafficInfo) -> (BreezeTrafficText?, BreezeTrafficParking?, BreezeTravelTime?) {
        var trafficText: BreezeTrafficText?
        var trafficParking: BreezeTrafficParking?
        var travelTime: BreezeTravelTime?
        switch info {
        case .trafficInformation(let text):
            var dataList: [String] = []
            if let texts = text.dataList, !texts.isEmpty {
                dataList = texts.map({ obuText in
                    return obuText.text
                })
            }
            trafficText = BreezeTrafficText(priority: getTrafficDataPriority(text.priority), templateId: text.templateId.rawValue, dataList: dataList, icon: text.icon)
        case .parkingData(let parking):
            var dataList: [Parking] = []
            if let parkings = parking.dataList, !parkings.isEmpty {
                for item in parkings {
                    let dataItem = Parking(location: item.location.text, lots: item.lots.text, color: getObuColorText(item.location.color))
                    dataList.append(dataItem)
                }
            }
            trafficParking = BreezeTrafficParking(priority: getTrafficDataPriority(parking.priority), templateId: parking.templateId.rawValue, dataList: dataList)
        case .travelTimeData(let travel):
            var dataList: [TravelTime] = []
            if let items = travel.dataList, !items.isEmpty {
                for item in items {
                    let dataItem = TravelTime(location: item.location.text, min: item.min.text, icon: item.icon, color: getObuColorText(item.location.color))
                    dataList.append(dataItem)
                }
            }
            travelTime = BreezeTravelTime(priority: getTrafficDataPriority(travel.priority), templateId: travel.templateId.rawValue, dataList: dataList)
        }
        
        return (trafficText, trafficParking, travelTime)
    }
    
    private func getTrafficDataPriority(_ priority: OBUSDK.OBUTrafficDataPriority) -> String {
        var retValue = ""
        switch priority {
        case .high:
            retValue = "high"
        case .charging:
            retValue = "charging"
        case .medium:
            retValue = "medium"
        case .low:
            retValue = "low"
        case .undefined:
            retValue = "undefined"
        }
        return retValue
        
    }
    
    private func getObuColorText(_ obuColor: OBUTextColor) -> String {
        var retValue = ""
        switch obuColor {
        case .Default:
            retValue = "Default"
        case .White:
            retValue = "White"
        case .Red:
            retValue = "Red"
        case .Blue:
            retValue = "Blue"
        case .Yellow:
            retValue = "Yellow"
        case .Green:
            retValue = "Green"
        case .Black:
            retValue = "Black"
        }
        return retValue
    }
}

//  MARK - Utils function Auto Connect
extension OBUHelper {
    func prepareObuConnetion(completion: @escaping (Bool, Error?) -> Void)  {
        self.initializeSDK { success, error in
            if success {
                OBUHelper.shared.verifyDataPermissionGranted { result in
                    switch result {
                    case .success(_):
                        //  Grand permission success then do auto connect obu
                        completion(true, nil)
                    case .failure(let error):
                        print("OBU SDK initialization failed")
                        completion(false, error)
                    }
                }
            } else {
                completion(false, error)
            }
        }
    }
    
    private func connectToPairedDevice(completion: @escaping (Bool, NSError?) -> Void) {
        OBUHelper.shared.getPairedDevices {[weak self] result in
            guard let self = self else {return}
            
            switch result {
            case .success(let obus):
                if let obuList = obus, !obuList.isEmpty {
                    
                    let names = obuList.map { model in
                        return model.name
                    }
                    SwiftyBeaver.debug("OBU - getPairedDevices: \(names)")
                    
                    var isFoundObu = false
                    for obu in obuList {
                        if obu.name == self.obuName {
                            SwiftyBeaver.debug("OBU - Found last obu from paired list: \(obu.name)")
                            self.connect(obu) { (success, connectError) in
                                completion(success, connectError)
                            }
                            isFoundObu = true
                            break
                        }
                    }
                    if !isFoundObu {
                        SwiftyBeaver.debug("OBU - Not found last obu from paired list: \(self.obuName)")
                        let notFoundObu = NSError(domain: "Not found OBU", code: 404)
                        completion(false, notFoundObu)
                    }
                }else {
                    SwiftyBeaver.debug("OBU - Not found last obu from paired list: \(self.obuName)")
                    let notFoundObu = NSError(domain: "Not found OBU", code: 404)
                    completion(false, notFoundObu)
                }
            case .failure(let obuError):
                let permissionError = NSError(domain: obuError.localizedDescription, code: obuError.code)
                completion(false, permissionError)
                break
            }
        }
    }
        
    func autoConnectOBU(completion: @escaping (Bool, NSError?) -> Void) {
        
        if !obuName.isEmpty {
            
            self.didShowMinimumCashCardBalance = false
            
            self.prepareObuConnetion {[weak self] (granted, error) in
                guard let self = self else {return}
                
                if granted {
                    
                    //  Try to disconnect before auto connect obu
                    SwiftyBeaver.debug("OBU - Force disconnect OBU: \(self.obuName)")
                    OBUSDKManager.shared.disconnectOBU()
                    
                    self.stateOBU = .IDLE
                    
                    SwiftyBeaver.debug("OBU - Last paired OBU: \(self.obuName)")
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
                        self.connectToPairedDevice(completion: completion)
                    }
                    
                } else {
                    let permissionError = NSError(domain: error?.localizedDescription ?? "Permission are not granted", code: 2001)
                    completion(false, permissionError)
                }
            }
        } else {
            self.isProcessing = false
        }
    }
    
    func toggleConnectOBU(isConnecting: Bool, vehicleNumber: String, obuName: String, completion: @escaping (Bool) -> ()) {
        
        if isProcessing {
            SwiftyBeaver.debug("toggleConnectOBU rejected \(isConnecting)")
            return
        }
        
        OBUHelper.shared.autoConnectOBUFromCarplay = false
        
        isProcessing = true
        SwiftyBeaver.debug("toggleConnectOBU isConnecting \(isConnecting)")
        if isConnecting {
            self.disconnectOBU { [weak self] (obu, error) in
                if let err = error {
                    let model = ObuErrorModel(name: obuName, vehicleNumber: vehicleNumber, error: err, isConnecting: false)
                    NotificationCenter.default.post(name: .obuDisconnectFailure, object: model)
                    completion(false)
                }else {
                    completion(true)
                }
                self?.isProcessing = false
            }
        } else {
                        
            self.obuName = obuName
            self.vehicleNumber = vehicleNumber
            self.autoConnectOBU { [weak self] (success, error)  in
                if success {
                    completion(true)
                } else {
                    if let err = error {
                        let model = ObuErrorModel(name: obuName, vehicleNumber: vehicleNumber, error: err, isConnecting: true)
                        NotificationCenter.default.post(name: .obuConnectFailure, object: model)
                    }
                    completion(false)
                }
                self?.isProcessing = false
            }
        }
    }
    
    func removeOBU(vehicleNumber: String, obuName: String) {
        
        //  Remove from local storage then update
        let obuModel = PairedObuModel(name: obuName, vehicleNumber: vehicleNumber)
        Prefs.shared.deleteObu(AWSAuth.sharedInstance.cognitoId, key: .pairedObuList, obuModel: obuModel)
        
        //  If removing current connected OBU
        if obuName == self.obuName /* && vehicleNumber == self.vehicleNumber */ {
            self.disconnectOBU {[weak self] (obu, error) in
                self?.resetObuConnection()
                self?.updatePairedVehicle()
            }
        } else {
            self.updatePairedVehicle()
        }
    }
}

//  Integrate App Sync
extension OBUHelper: MockObuEventListener {
    
    func startObuEvent() {
//        if !isObuConnected() {
//            return
//        }
        
        if mode == .device {
            SwiftyBeaver.debug("startObuEvent")
            DataCenter.shared.addMockObuEventListner(listner: self)
            DataCenter.shared.subscribeToObuEvent(cognitoId: AWSAuth.sharedInstance.cognitoId)
        }
    }
    
    func stopObuEvent() {
        SwiftyBeaver.debug("stopObuEvent")
        DataCenter.shared.addMockObuEventListner(listner: nil)
        DataCenter.shared.unsubcribeObuEvent()
    }
    
    func startBreezeMockEvent() {
        
//        if !isObuConnected() {
//            return
//        }
        
        if mode == .mockServer {
            SwiftyBeaver.debug("startBreezeMockEvent")
            DataCenter.shared.addMockObuEventListner(listner: self)
            DataCenter.shared.subscribeToMockObuEvent(cognitoId: AWSAuth.sharedInstance.cognitoId)
        }
    }
    
    func stopBreezeMockEvent() {
        SwiftyBeaver.debug("stopBreezeMockEvent")
        DataCenter.shared.addMockObuEventListner(listner: nil)
        DataCenter.shared.unsubcribeMockObuEvent()
    }
    
    func onReceiveObuEvent(_ event: ObuMockEventModel) {
        SwiftyBeaver.debug("RECEIVE BREEZE OBU EVENT: \(event.eventType) with data: \(event.getNotiDic())")
        print("RECEIVE BREEZE OBU EVENT: \(event.eventType) with data: \(event.getNotiDic())")
        
        //  Dont process event from Breeze server when user disconnect OBU
//        if !isObuConnected() {
//            return
//        }
        obuDelegate?.onReceiveObuEvent(event)
    }
}

// MARK: - Check Bluetooth Status
extension OBUHelper: CBCentralManagerDelegate {
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        setBluetoothOn(central.state == .poweredOn)
        
        SwiftyBeaver.debug("centralManagerDidUpdateState: \(central.state)")
        print("centralManagerDidUpdateState: \(central.state)")
        
        if central.state == .poweredOff {
            if appDelegate().carPlayConnected {
                appDelegate().showPremissonBluetoothOBU()
            }
        }
    }
}

extension OBUHelper {
    private func showCardErrorPopup() {
        if let mapLandingVC = appDelegate().getMapLandingVCInTop() {
            mapLandingVC.showCardErrorPopup()
        } else {
            didShowCardErrorPopup = false
        }
    }
}

extension OBUHelper {
    @objc func carplayMapLoaded(_ notification: Notification) {
        if let carplayConnected = notification.object as? Bool, carplayConnected {
            DispatchQueue.main.async {
                //  Show toast OBU connected if needed
                if !self.didShowToastOBUConnected && OBUHelper.shared.isObuConnected() {
                    appDelegate().obuConnected()
                }
                
                //  Update OBU connection state on Carplay
                appDelegate().updateStateObuOnCarplay()

                //  Handle show minimum card balance in Carplay in case it hasn't been shown
                if appDelegate().needShowMinimumCashcardBalance == false && !self.didShowMinimumCashCardBalance {
                    if self.globalCardBalance != -1, self.globalCardBalance / 100 < 5.0 {
                        appDelegate().needShowMinimumCashcardBalance = true
                    }
                }
            }
        }
    }
}

//
//  ObuComfirmViewController.swift
//  Breeze
//
//  Created by Tan, Tran Ngoc on 01/06/2023.
//

import UIKit
import OBUSDK
import CoreBluetooth

typealias CancelSelectObuCompletion = ()-> (Void)
typealias SelectObuCompletion = (OBU)-> (Void)

class ObuComfirmViewController: UIViewController {
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var activityView: UIActivityIndicatorView!
    
    var cancelSelectObuCompletion: CancelSelectObuCompletion?
    var selectObuCompletion: SelectObuCompletion?
    var selectedIndex: Int?
    var obus: [OBU] = []
    var obuName: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupDarkLightAppearance(forceLightMode: true)
        startUpdatingFoundOBU()
        if OBUHelper.shared.getObuMode() == .device {
            stopLoadingAfter60Seconds()
        }
    }
    
    private func stopLoadingAfter60Seconds() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 60) { [weak self] in
            self?.activityView.stopAnimating()
            self?.activityView.isHidden = true
        }
    }
    
    //  Handle pairing error
    @objc func onFoundOBU(_ notification: NSNotification) {
        //  Define error come from what screen the decide what to do
        if let foundObus = notification.object as? [OBU] {
            DispatchQueue.main.async {
                self.obus = foundObus
                self.tableView.reloadData()
            }
        }
    }
    
    @objc func onSearchingOBUFinished(_ notification: NSNotification) {
        self.onFoundOBU(notification)
        DispatchQueue.main.async { [weak self] in
            self?.activityView.stopAnimating()
            self?.activityView.isHidden = true
        }
    }
    
    func startUpdatingFoundOBU () {
        self.stopUpdatingFoundOBU()
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(onFoundOBU(_:)),
            name: .obuOnSearchingUpdates,
            object: nil)
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(onSearchingOBUFinished(_:)),
            name: .obuOnSearchingFinished,
            object: nil)
    }
    
    func stopUpdatingFoundOBU () {
        NotificationCenter.default.removeObserver(self, name: .obuOnSearchingUpdates, object: nil)
        NotificationCenter.default.removeObserver(self, name: .obuOnSearchingFinished, object: nil)
    }
    
    func setupUI() {
        
        if OBUHelper.shared.getObuMode() == .device {
            activityView.isHidden = false
            activityView.style = .large
            activityView.color = UIColor(hexString: "782EB1", alpha: 1)
            activityView.startAnimating()
        } else {
            activityView.isHidden = true
        }
        
        contentView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        contentView.layer.cornerRadius = 10.0
        contentView.layer.masksToBounds = true
        confirmButton.backgroundColor =  UIColor(hexString: "#BCBFC4", alpha: 1.0)
        tableView.delegate = self
        tableView.dataSource = self
        self.confirmButton.isEnabled = false
        confirmButton.layer.cornerRadius = 20
        confirmButton.layer.borderWidth = 1
        confirmButton.layer.borderColor = UIColor.white.cgColor
        self.tableView.register(UINib(nibName: "OBUConfirmTableViewCell", bundle: nil), forCellReuseIdentifier: "OBUConfirmTableViewCell")
        
    }
    
    private func selectedRow() {
        self.confirmButton.isEnabled = true
        self.confirmButton.backgroundColor = UIColor(hexString: "#782EB1", alpha: 1)
        tableView.reloadData()
    }
    
    @IBAction func closeAction(_ sender: Any) {
        
        stopUpdatingFoundOBU()
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.ObuPairing.UserClick.start_engine_confirm_obu_close, screenName: ParameterName.ObuPairing.screen_view)
        if let index = selectedIndex {
            self.view.removeFromSuperview()
            if OBUHelper.shared.getObuMode() == .device {
                selectObuCompletion?(obus[index])
            } else {
                if let firstObu = obus.first {
                    selectObuCompletion?(firstObu)
                }
            }
        } else {
            self.view.removeFromSuperview()
            cancelSelectObuCompletion?()
        }
    }
    
    @IBAction func confirmAction(_ sender: Any) {
        stopUpdatingFoundOBU()
        if let obuName = self.obuName {
            AnalyticsManager.shared.logClickEventSelectedObu(eventValue: ParameterName.ObuPairing.UserClick.start_engine_confirm_obu_select_obu, screenName: ParameterName.ObuPairing.screen_view, obuName: "obu_\(obuName)")
        }
        self.view.removeFromSuperview()
        if let index = selectedIndex {
            //            if OBUHelper.shared.getObuMode() == .device {
            selectObuCompletion?(obus[index])
            //            } else {
            //                if let firstObu = obus.first {
            //                    selectObuCompletion?(firstObu)
            //                }
            //            }
        }else {
            cancelSelectObuCompletion?()
        }
    }
    
    
}

extension ObuComfirmViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //        if OBUHelper.shared.getObuMode() == .device {
        return obus.count
        //        } else {
        //            return OBUHelper.shared.fakeObus.count
        //        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: OBUConfirmTableViewCell.identifier) as! OBUConfirmTableViewCell
        //        if OBUHelper.shared.getObuMode() == .device {
        cell.setupUI(title: obus[indexPath.row].name, isSelected: (selectedIndex == indexPath.row))
        self.obuName = obus[indexPath.row].name
        //        } else {
        //            cell.setupUI(title: OBUHelper.shared.fakeObus[indexPath.row].name ?? "", isSelected: (selectedIndex == indexPath.row))
        //        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.selectedIndex = indexPath.row
        self.confirmButton.isEnabled = true
        self.confirmButton.backgroundColor = UIColor(hexString: "#782EB1", alpha: 1)
        //        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.ObuPairing.UserClick.start_engine_confirm_obu_select_obu, screenName: ParameterName.ObuPairing.screen_view)
        if let obuName = self.obuName {
            AnalyticsManager.shared.logClickEventSelectedObu(eventValue: ParameterName.ObuPairing.UserClick.start_engine_confirm_obu_select_obu, screenName: ParameterName.ObuPairing.screen_view, obuName: "obu_\(obuName)")
        }
        self.tableView.reloadData()
    }
}





//
//  ObuPairStatusViewController.swift
//  Breeze
//
//  Created by Tan, Tran Ngoc on 31/05/2023.
//

import UIKit
import OBUSDK
import CoreBluetooth
import UserNotifications
import Network
import SwiftyBeaver

class ObuPairStatusViewController: UIViewController, UNUserNotificationCenterDelegate {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var titleLabel: UILabel!{
        didSet {
            titleLabel.text = "Start Engine"
        }
    }
    @IBOutlet weak var button1: UIButton!
    @IBOutlet weak var button2: UIButton!
    @IBOutlet weak var button3: UIButton!
    @IBOutlet weak var button4: UIButton!
    @IBOutlet weak var step1Label: UILabel!
    @IBOutlet weak var step2Label: UILabel!
    @IBOutlet weak var step3Label: UILabel!
    @IBOutlet weak var step4Label: UILabel!
    @IBOutlet weak var step1View: UIView!
    @IBOutlet weak var step2View: UIView!
    @IBOutlet weak var step3View: UIView!
    
    @IBOutlet weak var viewLoadingIndicator: UIView!
    @IBOutlet weak var loadingIndicatorView: UIActivityIndicatorView!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    
    var currentStep = 0
    var obuName: String?
    var obus: [OBU] = []
    var textVehicleNumber: String = ""
    private var networkMonitor: NWPathMonitor?
    
    var selectedObu: OBU?
    var confirmObuVC: ObuComfirmViewController?
    var isCheckVehicle: Bool = false
    
    var isAnimating: Bool = false {
        didSet {
            if Thread.isMainThread {
                self.threadSafeActivity(animate: isAnimating)
            } else {
                DispatchQueue.main.async {
                    self.threadSafeActivity(animate: self.isAnimating)
                }
            }
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initCollectionView()
        setupUI()
        
        OBUHelper.shared.startMonitoringBluetooth()
        
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge], completionHandler: {didAllow, error in
            
        })
        
        checkNetWorkPremission()
        
        setupDarkLightAppearance(forceLightMode: true)
        
        OBUHelper.shared.autoConnectOBUFromCarplay = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        networkMonitor?.cancel()
        networkMonitor = nil
    }
    
    private func initCollectionView() {
        
        self.collectionView.register(UINib(nibName: "Step1Cell", bundle: Bundle.main), forCellWithReuseIdentifier: "Step1Cell")
        self.collectionView.register(UINib(nibName: "Step2Cell", bundle: Bundle.main), forCellWithReuseIdentifier: "Step2Cell")
        self.collectionView.register(UINib(nibName: "Step3Cell", bundle: Bundle.main), forCellWithReuseIdentifier: "Step3Cell")
        self.collectionView.register(UINib(nibName: "Step4Cell", bundle: Bundle.main), forCellWithReuseIdentifier: "Step4Cell")
        
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.reloadData()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: { [weak self] in
            guard let self = self else { return }
            if self.currentStep > 0 {
                self.updateUIAtStep(self.currentStep)
            }
        })
    }
    
    private func setupUI() {
        button1.layer.cornerRadius = 10
        button2.layer.cornerRadius = 10
        button3.layer.cornerRadius = 10
        button4.layer.cornerRadius = 10
        self.viewLoadingIndicator.isHidden = true
        titleLabel.textColor = UIColor(hexString: "#222638", alpha: 1)
        titleLabel.font = UIFont.instructionRateFont()
        nextButton.layer.cornerRadius = 24
        nextButton.layer.borderWidth = 1
        nextButton.layer.borderColor = UIColor.white.cgColor
        nextButton.titleLabel?.font = UIFont.broadcastMessageSubtitleFont()
        nextButton.setTitleColor(UIColor(hexString: "#FFFFFF", alpha: 1), for: .normal)
        nextButton.setTitle("Next", for: .normal)
        step1Label.text = "Start Engine"
        step1Label.lineBreakMode = .byWordWrapping
        step1Label.textColor = UIColor(hexString: "#222638", alpha: 1)
        step1Label.font = UIFont.titleStepObuConnected()
        step2Label.lineBreakMode = .byWordWrapping
        step2Label.textColor = UIColor(hexString: "#222638", alpha: 1)
        step2Label.font = UIFont.titleStepObuConnected()
        step3Label.lineBreakMode = .byWordWrapping
        step3Label.textColor = UIColor(hexString: "#222638", alpha: 1)
        step3Label.font = UIFont.titleStepObuConnected()
        step4Label.lineBreakMode = .byWordWrapping
        step4Label.textColor = UIColor(hexString: "#222638", alpha: 1)
        step4Label.font = UIFont.titleStepObuConnected()
    }
    
    
    private func animateActivity(animate: Bool) {
        isAnimating = animate
    }
    
    private func threadSafeActivity(animate: Bool) {
        if animate {
            self.viewLoadingIndicator.isHidden = false
            self.loadingIndicatorView.startAnimating()
        } else {
            self.viewLoadingIndicator.isHidden = true
            self.loadingIndicatorView.stopAnimating()
        }
    }
    
    @IBAction func nextAction(_ sender: Any) {
        if currentStep == 4 {
            return
        }
        
        //  Check bluetooth is on/off then show alert or permission is on/off
        if !OBUHelper.shared.isOnBluetooth() {
            AnalyticsManager.shared.logOpenPopupEvent(popupName: ParameterName.ObuPairing.UserPopup.popup_obu_permission_turn_on_bluetooth, screenName: ParameterName.ObuPairing.screen_view)
            showBluetoothAlert()
            return
        } else if !OBUHelper.shared.isBluetoothPermissionGranted() {
            AnalyticsManager.shared.logOpenPopupEvent(popupName: ParameterName.ObuPairing.UserPopup.popup_obu_permission_turn_on_bluetooth, screenName: ParameterName.ObuPairing.screen_view)
            showPermissionAlert()
            return
        }
        
        currentStep += 1
        
//        backButton.isHidden = (currentStep == 3)
        
        if currentStep == 1 {
            checkPermissionOBU()
        } else if currentStep == 2 {
            if verifyVehicleNumber() {
                step2View.backgroundColor = UIColor(hexString: "#782EB1", alpha: 1)
                button3.backgroundColor = UIColor(hexString: "#782EB1", alpha: 1)
                titleLabel.text = "Verify Bluetooth"
                collectionView.scrollToItem(at: IndexPath(row: 0, section: currentStep), at: .centeredHorizontally, animated: true)
            } else {
                currentStep -= 1
            }
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.ObuPairing.UserClick.number_plate_next, screenName: ParameterName.ObuPairing.screen_view)
        } else if currentStep == 3 {
            checkDisconnectCurrentOBU()
            button4.backgroundColor = UIColor(hexString: "#782EB1", alpha: 1)
            step3View.backgroundColor = UIColor(hexString: "#782EB1", alpha: 1)
            titleLabel.text = "Pairing"
            collectionView.scrollToItem(at: IndexPath(row: 0, section: currentStep), at: .centeredHorizontally, animated: true)
            nextButton.isHidden = true
            nextButton.setTitle("Explore Breeze", for: .normal)
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.ObuPairing.UserClick.verify_bluetooth_next, screenName: ParameterName.ObuPairing.screen_view)
        } else {
            //  Remove Explore Breeze button for pairing page
            if let mapLandingVC = appDelegate().getMapLandingVCInTop() {
                mapLandingVC.startObservingOBUPairingStatus()
            }
            self.goToMapLanding()
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.ObuPairing.UserClick.pairing_explore_breeze, screenName: ParameterName.ObuPairing.screen_view)
        }
    }
    
    private func updateUIAtStep(_ stepIndex: Int = 0) {
        
        backButton.isHidden = (stepIndex == 3)
        
        nextButton.setTitle("Next", for: .normal)
        nextButton.isHidden = false
        self.nextButton.backgroundColor = UIColor(hexString: "#782EB1", alpha: 1)
        
        switch stepIndex {
        case 0:
            // Handler Connecte OBU
            self.titleLabel.text = "Start Engine"
            self.nextButton.backgroundColor = UIColor(hexString: "#782EB1", alpha: 1)
            
            self.button1.backgroundColor = UIColor(hexString: "#782EB1", alpha: 1)
            self.button2.backgroundColor = UIColor(hexString: "#BCBFC4", alpha: 1)
            self.button3.backgroundColor = UIColor(hexString: "#BCBFC4", alpha: 1)
            self.button4.backgroundColor = UIColor(hexString: "#BCBFC4", alpha: 1)
            
            self.step1View.backgroundColor = UIColor(hexString: "#BCBFC4", alpha: 1)
            self.step2View.backgroundColor = UIColor(hexString: "#BCBFC4", alpha: 1)
            self.step3View.backgroundColor = UIColor(hexString: "#BCBFC4", alpha: 1)
            
            self.collectionView.scrollToItem(at: IndexPath(row: 0, section: currentStep), at: .centeredHorizontally, animated: true)
        case 1:
            // Handler Connecte OBU
            titleLabel.text = "Vehicle Plate Number"
            
            if self.textVehicleNumber.isEmpty {
//                self.nextButton.isEnabled = false
                self.nextButton.backgroundColor = UIColor(hexString: "#BCBFC4", alpha: 1)
            }
            
            self.button1.backgroundColor = UIColor(hexString: "#782EB1", alpha: 1)
            self.button2.backgroundColor = UIColor(hexString: "#782EB1", alpha: 1)
            self.button3.backgroundColor = UIColor(hexString: "#BCBFC4", alpha: 1)
            self.button4.backgroundColor = UIColor(hexString: "#BCBFC4", alpha: 1)
            
            self.step1View.backgroundColor = UIColor(hexString: "#782EB1", alpha: 1)
            self.step2View.backgroundColor = UIColor(hexString: "#BCBFC4", alpha: 1)
            self.step3View.backgroundColor = UIColor(hexString: "#BCBFC4", alpha: 1)
            
            self.collectionView.scrollToItem(at: IndexPath(row: 0, section: currentStep), at: .centeredHorizontally, animated: true)
        case 2:
            titleLabel.text = "Verify Bluetooth"
            
            self.button1.backgroundColor = UIColor(hexString: "#782EB1", alpha: 1)
            self.button2.backgroundColor = UIColor(hexString: "#782EB1", alpha: 1)
            self.button3.backgroundColor = UIColor(hexString: "#782EB1", alpha: 1)
            self.button4.backgroundColor = UIColor(hexString: "#BCBFC4", alpha: 1)
            
            self.step1View.backgroundColor = UIColor(hexString: "#782EB1", alpha: 1)
            self.step2View.backgroundColor = UIColor(hexString: "#782EB1", alpha: 1)
            self.step3View.backgroundColor = UIColor(hexString: "#BCBFC4", alpha: 1)
            
            collectionView.scrollToItem(at: IndexPath(row: 0, section: currentStep), at: .centeredHorizontally, animated: true)
        case 3:
            titleLabel.text = "Pairing"
            
            self.button1.backgroundColor = UIColor(hexString: "#782EB1", alpha: 1)
            self.button2.backgroundColor = UIColor(hexString: "#782EB1", alpha: 1)
            self.button3.backgroundColor = UIColor(hexString: "#782EB1", alpha: 1)
            self.button4.backgroundColor = UIColor(hexString: "#782EB1", alpha: 1)
            
            self.step1View.backgroundColor = UIColor(hexString: "#782EB1", alpha: 1)
            self.step2View.backgroundColor = UIColor(hexString: "#782EB1", alpha: 1)
            self.step3View.backgroundColor = UIColor(hexString: "#782EB1", alpha: 1)
            
            collectionView.scrollToItem(at: IndexPath(row: 0, section: currentStep), at: .centeredHorizontally, animated: true)
            
            //  BREEZE2-1008 - Remove Explore Breeze Button
            nextButton.isHidden = true
            nextButton.setTitle("Explore Breeze", for: .normal)
        default:
            break
        }
    }
    
    //  MARK: Verify vehicle number
    private func verifyVehicleNumber () -> Bool {
        let pairedList = Prefs.shared.getObuList(AWSAuth.sharedInstance.cognitoId, key: .pairedObuList).map { localObu in
            return localObu.vehicleNumber
        }
        if !pairedList.isEmpty {
            if !self.textVehicleNumber.isEmpty {
                if pairedList.contains(self.textVehicleNumber) {
                    //  Show error alert that user already paired
                    self.showAlert(title: "", message: "Vehicle number already paired", confirmTitle: "Ok")
                    self.isCheckVehicle = true
                    return false
                }
            }
        }
        
        return true
    }
    
    // MARK: Check Network Connection
    private func checkNetWorkPremission() {
        networkMonitor = NWPathMonitor()
        AnalyticsManager.shared.logOpenPopupEvent(popupName: ParameterName.ObuPairing.UserPopup.popup_obu_permission_turn_on_internet, screenName: ParameterName.ObuPairing.screen_view)
        networkMonitor?.pathUpdateHandler = { path in
            if path.status == .satisfied {
                // Internet connection is available
            } else {
                // No internet connection
                DispatchQueue.main.async {
                    self.checkNetWorkSetting()
                }
            }
        }
        let queue = DispatchQueue(label: "NetworkMonitorQueue")
        networkMonitor?.start(queue: queue)
    }
    
    
    private func checkNetWorkSetting() {
        let alertController = UIAlertController(title: "No Internet Connection",
                                                message: "Turn on Internet to allow “Breeze” to connect to Accessories.",
                                                preferredStyle: .alert)
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { _ in
            guard let settingsURL = URL(string: UIApplication.openSettingsURLString) else { return }
            UIApplication.shared.open(settingsURL, options: [:], completionHandler: nil)
            AnalyticsManager.shared.logOpenPopupEvent(popupName: ParameterName.ObuPairing.UserClick.popup_obu_permission_turn_on_internet_setting, screenName: ParameterName.ObuPairing.screen_view)
        }
        
        let cancelAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        AnalyticsManager.shared.logOpenPopupEvent(popupName: ParameterName.ObuPairing.UserClick.popup_obu_permission_turn_on_internet_setting_ok, screenName: ParameterName.ObuPairing.screen_view)
        alertController.addAction(cancelAction)
        alertController.addAction(settingsAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {

        //displaying the ios local notification when app is in foreground
        completionHandler([.alert, .badge, .sound])
    }
    
    private func checkPermissionOBU() {
        
        OBUHelper.shared.prepareObuConnetion {[weak self] (success, error) in
            if success {
                DispatchQueue.main.async {
                    self?.startSearchObu()
                }
            } else {
                DispatchQueue.main.async {
                    //  If initialize SDK failed then should stay current step
                    self?.showAlertPremission(title: "Pairing failed", message: "Permission is needed to Pair OBU to Breeze", confirmTitle: "Close") { [weak self] _ in
                        self?.navigationController?.popViewController(animated: true)
                    }
                }
            }
        }
    }
    
    private func startSearchObu() {
        
        //  Show loading indicator
//        self.showLoadingIndicator()
        self.animateActivity(animate: true)
        OBUHelper.shared.startSearch { obuList in
            //  Temporary remove filter out connected vehicle
//            let pairedList = Prefs.shared.getObuList(AWSAuth.sharedInstance.cognitoId, key: .pairedObuList).map { localObu in
//                return localObu.name
//            }
//            //  Filter out paired vehicle
//            self.obus = obuList.filter({ obu in
//                return !pairedList.contains(obu.name)
//            })
            
            self.obus = obuList
            
            //  Stop loading indicator
            self.animateActivity(animate: false)
            
            //  If there are no obu found
            if self.obus.isEmpty {
                //  Revert back step if user do not choose any OBU
                if self.currentStep == 1 {
                    self.currentStep = 0
                }
                self.obuNotFoundError()
                
//                self.displayObuConnectErrorScreen(.OBUNotDetected, isNotFound: true)
            } else {
                self.confirmObuPopUp()
            }
        }
    }
    
    private func obuNotFoundError() {
        AnalyticsManager.shared.logOpenPopupEventV2(popupName: ParameterName.ObuPairing.UserPopup.popup_obu_not_found, screenName: ParameterName.ObuPairing.screen_view)
        
        self.showAlert(title: "OBU not found", message: "Make sure you have registered your phone's bluetooth Mac address with LTA's e-portal. Then try pairing again.", confirmTitle: "Ok") { action in
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.ObuPairing.UserClick.popup_obu_not_found_ok, screenName: ParameterName.ObuPairing.screen_view)
        }
    }
    
    private func confirmObuPopUp() {
        let storyboard: UIStoryboard = UIStoryboard(name: "OBU", bundle: nil)
        
        if let confirmVC = storyboard.instantiateViewController(withIdentifier: "ObuComfirmViewController") as? ObuComfirmViewController {
            
            self.confirmObuVC = confirmVC
            
            confirmVC.obus = obus
            confirmVC.selectObuCompletion = { [weak self] obu in
                
                //  Handle show error when OBU already paired
//                if OBUHelper.shared.getObuMode() == .device {
                    let pairedList = Prefs.shared.getObuList(AWSAuth.sharedInstance.cognitoId, key: .pairedObuList).map { localObu in
                        return localObu.name
                    }
                    
                    if !pairedList.isEmpty {
                        if pairedList.contains(obu.name) {
                            //  Show error alert that user already paired
                            self?.showAlert(title: "", message: "OBU already paired", confirmTitle: "Ok")
                            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.ObuPairing.UserClick.start_engine_obu_already_paired_ok, screenName: ParameterName.ObuPairing.screen_view)
                            //  Revert back step if user do not choose any OBU
                            if self?.currentStep == 1 {
                                self?.currentStep = 0
                            }
                            return;
                        }
                    }
//                }
                
                // Handler Connecte OBU
                self?.titleLabel.text = "Vehicle Plate Number"
//                if let textVehicle = self?.textVehicleNumber, textVehicle == "" {
////                    self?.nextButton.isEnabled = false
//                    self?.nextButton.backgroundColor = UIColor(hexString: "#BCBFC4", alpha: 1)
//                }
                self?.button2.backgroundColor = UIColor(hexString: "#782EB1", alpha: 1)
                self?.step1View.backgroundColor = UIColor(hexString: "#782EB1", alpha: 1)
                self?.collectionView.scrollToItem(at: IndexPath(row: 0, section: 1), at: .centeredHorizontally, animated: true)
                self?.selectedObu = obu
            }
            
            confirmVC.cancelSelectObuCompletion = { [weak self] in
                //  Revert back step if user do not choose any OBU
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.ObuPairing.UserClick.start_engine_confirm_obu_close, screenName: ParameterName.ObuPairing.screen_view)
                if self?.currentStep == 1 {
                    self?.currentStep = 0
                }
            }
            
            self.addChildViewControllerWithView(childViewController: confirmVC)
        }
    }
    
    private func pairingErrorPopUp() {
        let storyboard: UIStoryboard = UIStoryboard(name: "OBU", bundle: nil)
        if let errorVC = storyboard.instantiateViewController(withIdentifier: "PairingErrorViewController") as? PairingErrorViewController {
            self.addChildViewControllerWithView(childViewController: errorVC)
        }
    }
    
    private func checkDisconnectCurrentOBU() {
        if selectedObu != nil {
            if OBUHelper.shared.isObuConnected() {
                SwiftyBeaver.debug("OBU - checkDisconnectCurrentOBU - Disconnecting")
                OBUHelper.shared.disconnectOBU { [weak self] (obu, error) in
                    if let errorState = error {
                        //  If error then show error
                        self?.showConnectObuError(errorState)
                        SwiftyBeaver.debug("OBU - checkDisconnectCurrentOBU - Error")
                        
                    } else {
                        //  Otherwise connect to new OBU
                        self?.connectOBU()
                        SwiftyBeaver.debug("OBU - checkDisconnectCurrentOBU - Success")
                    }
                }
            } else {
                self.connectOBU()
            }
        }
    }
    
    private func connectOBU() {
        
//        if OBUHelper.shared.getObuMode() == .device && OBUHelper.shared.getConnectMethod() == .withVehicleNumber {
//            if let obu = selectedObu, !self.textVehicleNumber.isEmpty {
//                //  Need to test connect with Vehicle number with real device
//                OBUHelper.shared.connectVehicleNumber(self.textVehicleNumber, obu: obu) { [weak self] (sucsess, error) in
//                    self?.hideLoadingIndicator()
//                    if sucsess == true {
//                        self?.handleAppInBackgroundNotification()
//                        //  If connecting success then try to pop to maplanding
//                        self?.goToMapLanding()
//                    } else {
//                        // Handler Error
//                        if let errorState = error {
//                            self?.showConnectObuError(errorState)
//                        }
//                    }
//                }
//            }
//        } else {
            if let obu = selectedObu {
                //  Need to test connect with Vehicle number with real device
                OBUHelper.shared.connect(obu) {[weak self] (sucsess, error) in
                    self?.hideLoadingIndicator()
                    if sucsess == true {
                        self?.handleAppInBackgroundNotification()
                        //  If connecting success then try to pop to maplanding
                        self?.goToMapLanding()
                    } else {
                        // Handler Error
                        if let errorState = error {
                            self?.showConnectObuError(errorState)
                        }
                    }
                }
            }
//        }
    }
    
    private func goToMapLanding() {
        //  No need pop to map landing because already handle by RN and go back home
//        self.navigationController?.popViewController(animated: true)
        //  Call function to RN to dismiss paired vehicle list
        dismissPairedVehicleList()
        
    }
    
    private func handleAppInBackgroundNotification() {
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            if appDelegate.appInBackground {
                OBUHelper.shared.showLocalNotification()
            }
        }
    }
    
    private func showAlertPremission(title: String, message: String, confirmTitle: String, handler: ((UIAlertAction) -> Void)?) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let confirmAction = UIAlertAction(title: confirmTitle, style: .default, handler: handler)
        alertController.addAction(confirmAction)
        present(alertController, animated: true, completion: nil)
    }
    
    
    // Handler error OBU Connected State
    private func showConnectObuError(_ error: NSError) {
        
        SwiftyBeaver.debug("OBU - showConnectObuError: OBU name \(OBUHelper.shared.obuName), Vehicle No.\(OBUHelper.shared.vehicleNumber), error code: \(error.code)")
        
//        let message = "Pairing error: \(error.code) description: \(error.localizedDescription)"
        let message = "err:\(error.code) des:\(error.localizedDescription)"
        AnalyticsManager.shared.logSystemEvent(eventValue: ParameterName.SystemAnalytics.SystemEvent.obu_verbose_error, screenName: ParameterName.SystemAnalytics.screen_view, eventMessage: message)
                
        switch error.code {
        case 1000:
            self.currentStep = 2
            self.updateUIAtStep(2)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.showAlert(title: "Connection Error", message: "Make sure your mobile data is enable", confirmTitle: "Got It") {
                    action in
                    AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.ObuPairing.UserClick.popup_obu_connection_error_internet_close, screenName: ParameterName.ObuPairing.screen_view)
                }
            }
            AnalyticsManager.shared.logOpenPopupEventV2(popupName:  ParameterName.ObuPairing.UserPopup.popup_obu_connection_error_internet, screenName:  ParameterName.ObuPairing.screen_view)
        case 1001:
            self.currentStep = 2
            self.updateUIAtStep(2)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.showAlert(title: "Connection Error", message: "Bluetooth needs to be enabled to connect", confirmTitle: "Got It")
            }
        case 1003, 1004, 1005, 1006, 2002, 2003, 2009, 2010, 400, 401, 408, 500, 616:
            self.displayObuConnectErrorScreen(.SystemError)
        case 2000:
            self.displayObuConnectErrorScreen(.PairingError)
        case 2001, 1002:
            self.displayObuConnectErrorScreen(.OBUConnectioLost)
        case 404:
            self.displayObuConnectErrorScreen(.OBUNotDetected)
        case 2004:
            self.currentStep = 2
            self.updateUIAtStep(2)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.showAlert(title: "Pairing failed", message: "Permission is needed to Pair OBU to \"Breeze\"", confirmTitle: "Close")  { action in
                    AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.ObuPairing.UserClick.popup_obu_pairing_failed_ok, screenName: ParameterName.ObuPairing.screen_view)
                }
                AnalyticsManager.shared.logOpenPopupEventV2(popupName:  ParameterName.ObuPairing.UserPopup.popup_obu_pairing_failed, screenName:  ParameterName.ObuPairing.screen_view)
            }
        case 2006:
            //  If connecting success then try to pop to maplanding
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.ObuPairing.UserClick.verify_bluetooth_pairing_failed_register_mac_with_lta, screenName: ParameterName.ObuPairing.screen_view)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                //  Reset obu connection
                OBUHelper.shared.resetObuConnection()
                self.goToMapLanding()
            }
            /*
            self.currentStep = 2
            self.updateUIAtStep(2)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.showAlert(title: "", message: "Mac address entered did not match Mac address registered with LTA", confirmTitle: "Close")
            }
             */
        default:
            break
        }
    }
    
    private func displayObuConnectErrorScreen(_ error: ConnectError, isNotFound: Bool = false) {
        let storyboard: UIStoryboard = UIStoryboard(name: "OBU", bundle: nil)
        if let errorConnectVC = storyboard.instantiateViewController(withIdentifier: "OBUConnectErrorViewController") as? OBUConnectErrorViewController {
            errorConnectVC.errorCase = error
            errorConnectVC.retryCompletion = { [weak self] code in
                if isNotFound {
                    DispatchQueue.main.async {
                        self?.startSearchObu()
                    }
                } else if error == .PairingError {
                    self?.currentStep = 1
                    self?.updateUIAtStep(1)
                } else {
                    self?.checkDisconnectCurrentOBU()
                }
            }
            
            errorConnectVC.skipCompletion = { [weak self] in
                OBUHelper.shared.resetObuConnection()
                self?.goToMapLanding()
            }
            self.addChildViewControllerWithView(childViewController: errorConnectVC)
        }
    }
    
    // MARK: Demo UI Error Connection
    
    private func demoErrorConnectOBU() {
        let alertController = UIAlertController(title: "Options", message: "Choose an option", preferredStyle: .actionSheet)
         
         let option1Action = UIAlertAction(title: "Pairing Error", style: .default) { (_) in

             self.displayObuConnectErrorScreen(.PairingError)
         }
         alertController.addAction(option1Action)
         
         let option2Action = UIAlertAction(title: "System Error", style: .default) { (_) in

             self.displayObuConnectErrorScreen(.SystemError)
         }
         alertController.addAction(option2Action)
         
         let option3Action = UIAlertAction(title: "OBU not detected", style: .default) { (_) in

             self.displayObuConnectErrorScreen(.OBUNotDetected)
         }
         alertController.addAction(option3Action)
        
        let option4Action = UIAlertAction(title: "OBU Connection Lost", style: .default) { (_) in
            self.displayObuConnectErrorScreen(.OBUConnectioLost)
        }
        alertController.addAction(option4Action)
        
        let option5Action = UIAlertAction(title: "Not Yet Register MAC", style: .default) { (_) in
            self.pairingErrorPopUp()
        }
        alertController.addAction(option5Action)
        
        let cancelAction = UIAlertAction(title: "Dissmiss", style: .cancel) { (_) in
            // Handle Cancel selection
            print("Cancel selected")
        }
        alertController.addAction(cancelAction)
         
         // Present the alert controller
         present(alertController, animated: true, completion: nil)
    }
    
    
    @IBAction func backAction(_ sender: Any) {
        //  Revert back to last pair vehicle but not change connect state
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.ObuPairing.UserClick.back, screenName: ParameterName.ObuPairing.screen_view)
        if currentStep == 3 && !OBUHelper.shared.isObuConnected() {
            OBUHelper.shared.isCancelPairing = true
            OBUHelper.shared.disconnectOBU {[weak self] (obu, error) in
                OBUHelper.shared.stateOBU = .IDLE
                if let err = error {
                    SwiftyBeaver.debug("OBU - Cancelling connect to OBU \(self?.obuName), error: \(err.localizedDescription)")
                } else {
                    SwiftyBeaver.debug("OBU - Cancelling successfully: \(self?.obuName)")
                }
            }
        } else {
            
            OBUHelper.shared.loadLastPairedObu(false)
            if isAnimating {
                OBUHelper.shared.stopSearch()
            }
        }
        
        self.navigationController?.popViewController(animated: true)
    }
    
    deinit {
        confirmObuVC = nil
    }
}

    
extension ObuPairStatusViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Step1Cell", for: indexPath) as! Step1Cell
            cell.setupUI()
            return cell
        } else if indexPath.section == 1 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Step3Cell", for: indexPath) as! Step3Cell //Step3Cell
            cell.delegate = self
            cell.setupUI()
            cell.isCheckVehicle = self.isCheckVehicle
            return cell
        } else if indexPath.section == 2 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Step2Cell", for: indexPath) as! Step2Cell
            cell.setupUI()
            return cell
        } else if indexPath.section == 3 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Step4Cell", for: indexPath) as! Step4Cell
            cell.delegate = self
            cell.setupUI()
            return cell
        }
  
        return UICollectionViewCell()
    }
}


extension ObuPairStatusViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: Screen_Width, height: collectionView.frame.height)
    }
}

extension ObuPairStatusViewController: Step3CellDelegate {
    
    func checkEmptyTextField(updateText: String) {
        self.textVehicleNumber = updateText
        self.nextButton.isEnabled = true
        self.nextButton.backgroundColor = UIColor(hexString: "#782EB1", alpha: 1)

    }
}

extension ObuPairStatusViewController: Step4CellDelegate {
    
    func disconnetOBU() {
        if !OBUHelper.shared.isObuConnected() {
            OBUHelper.shared.isCancelPairing = true
            OBUHelper.shared.disconnectOBU {[weak self] (obu, error) in
                OBUHelper.shared.stateOBU = .IDLE
                if let err = error {
                    SwiftyBeaver.debug("OBU - Cancelling connect to OBU \(self?.obuName), error: \(err.localizedDescription)")
                } else {
                    SwiftyBeaver.debug("OBU - Cancelling successfully: \(self?.obuName)")
                }
            }
            self.navigationController?.popViewController(animated: true)
        }
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.ObuPairing.UserClick.pairing_cancel_pairing, screenName: ParameterName.ObuPairing.screen_view)
    }
}


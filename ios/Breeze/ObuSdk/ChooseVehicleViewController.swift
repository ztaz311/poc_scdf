//
//  ChooseVehicleViewController.swift
//  Breeze
//
//  Created by Tan, Tran Ngoc on 28/07/2023.
//

import UIKit

typealias ChooseVehicleCompletion = (ObuVehicleDetailModel)-> (Void)

class ChooseVehicleViewController: UIViewController {

    @IBOutlet weak var contentView: UIView!
    
    @IBOutlet weak var vehicleTableView: UITableView!
    
    @IBOutlet weak var closeBtn: UIButton!
    
    @IBOutlet weak var cancelBtn: UIButton!
    
    @IBOutlet weak var doneBtn: UIButton!
    
    var vehicleList: [ObuVehicleDetailModel]?
    var selectedObu: ObuVehicleDetailModel?
    
    var onCompletion: ChooseVehicleCompletion?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setupDarkLightAppearance(forceLightMode: true)
        setupUI()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
     //let pairedObuList = Prefs.shared.getObuList(AWSAuth.sharedInstance.cognitoId, key: .pairedObuList)
     let datas: [[String: Any]] = pairedObuList.map { obu in
         return obu.getDic()
     }
    }
    */
    
    
    @IBAction func closeAction(_ sender: Any) {
        self.view.removeFromSuperview()
        if let obu = selectedObu {
            onCompletion?(obu)
        }
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        self.view.removeFromSuperview()
    }
    
    @IBAction func doneAction(_ sender: Any) {
        self.view.removeFromSuperview()
        if let obu = selectedObu {
            onCompletion?(obu)
        }
    }
    
    func setupUI() {
        contentView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        contentView.layer.cornerRadius = 10.0
        contentView.layer.masksToBounds = true
        
//        doneBtn.backgroundColor =  UIColor(hexString: "#BCBFC4", alpha: 1.0)
//        self.doneBtn.isEnabled = false
        vehicleTableView.delegate = self
        vehicleTableView.dataSource = self
        
        doneBtn.isEnabled = true
        doneBtn.backgroundColor = UIColor(hexString: "#782EB1", alpha: 1)
        doneBtn.layer.cornerRadius = 24
        doneBtn.layer.borderWidth = 1
        doneBtn.layer.borderColor = UIColor.white.cgColor
        doneBtn.titleLabel?.font = UIFont(name: fontFamilySFPro.Regular, size: 20)
        
        cancelBtn.titleLabel?.textColor = UIColor(hexString: "782EB1", alpha: 1)
        cancelBtn.layer.cornerRadius = 24
        cancelBtn.layer.borderWidth = 1
        cancelBtn.layer.borderColor = UIColor(hexString: "782EB1", alpha: 1).cgColor
        cancelBtn.titleLabel?.font = UIFont(name: fontFamilySFPro.Regular, size: 20)
        
        self.vehicleTableView.register(UINib(nibName: "ChooseVehicleTableViewCell", bundle: nil), forCellReuseIdentifier: "ChooseVehicleTableViewCell")
        
    }
}

extension ChooseVehicleViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return vehicleList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ChooseVehicleTableViewCell.identifier) as! ChooseVehicleTableViewCell
        if let data = vehicleList?[indexPath.row] {
            cell.setupUI(model: data, isSelected: (self.selectedObu?.vehicleNumber == data.vehicleNumber))
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.selectedObu = vehicleList?[indexPath.row]
        self.vehicleTableView.reloadData()
    }
}

//
//  OBUConnectErrorViewController.swift
//  Breeze
//
//  Created by Tan, Tran Ngoc on 09/06/2023.
//

import UIKit
import Instabug
import SwiftyBeaver

enum ConnectError: String {
    case PairingError
    case SystemError
    case OBUNotDetected
    case OBUConnectioLost
}

typealias ObuRetryConnectCompletion = (Int)-> (Void)
typealias ObuSkipConnection = ()-> (Void)

class OBUConnectErrorViewController: UIViewController {
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var titleErrorLabel: UILabel!
    @IBOutlet weak var dissmissButton: UIButton!
    @IBOutlet weak var errorImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descError1Label: UILabel!
    @IBOutlet weak var descError2Label: UILabel!
    @IBOutlet weak var descError3Label: UILabel!
    @IBOutlet weak var retryButton: UIButton!
    @IBOutlet weak var skipButton: UIButton!
    @IBOutlet weak var reportIssueButton: UIButton!
    @IBOutlet weak var heightContentError: NSLayoutConstraint!
    
    @IBOutlet weak var viewContentError: UIView!
    
    @IBOutlet weak var dotIconImage: UIImageView!
    
    @IBOutlet weak var descError4Label: UILabel!
    var retryCompletion: ObuRetryConnectCompletion?
    var skipCompletion: ObuSkipConnection?
    var errorCase: ConnectError = .PairingError
    var errorModel: ObuErrorModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        setupUI()
        demoConnectError()
        // Do any additional setup after loading the view.
        setupDarkLightAppearance(forceLightMode: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //  Change background color
        if let _ = errorModel {
            self.view.backgroundColor = UIColor(hexString: "#222638", alpha: 0.5)
        }
    }
    
    
//    func setupUI() {
//        contentView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
//        contentView.layer.cornerRadius = 10.0
//        titleLabel.text = "Check the following and try again."
//        titleLabel.font = UIFont.instructionRateFont()
//        descError1Label.text = "Vehicle started"
//        descError1Label.font = UIFont.toastMessageTextFont()
//        descError2Label.text = "Bluetooth enabled"
//        descError2Label.font = UIFont.toastMessageTextFont()
//        descError3Label.text = "Mobile data enabled"
//        descError3Label.font = UIFont.toastMessageTextFont()
//        retryButton.layer.cornerRadius = 20
//        skipButton.layer.cornerRadius = 20
//        skipButton.layer.borderWidth = 1
//        skipButton.layer.borderColor = UIColor(named:"tooltipsAmenityNaviBtnColor")?.cgColor ?? UIColor.purple.cgColor
//    }
    
    func demoConnectError() {
        switch errorCase {
        case .PairingError:
            // Handle pairing error
            contentView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
            contentView.layer.cornerRadius = 10.0
            titleErrorLabel.text = "Pairing Error"
            titleLabel.text = "The vehicle number does not match the OBU linked to the vehicle. Please try again"
            titleLabel.font = UIFont.rerouteMessageBoldFont()
            viewContentError.isHidden = true
            heightContentError.constant = 0
            retryButton.layer.cornerRadius = 24
            skipButton.layer.cornerRadius = 24
            skipButton.layer.borderWidth = 1
            skipButton.layer.borderColor = UIColor(hexString: "#782EB1", alpha: 1).cgColor
        case .SystemError:
            print("System Error")
            contentView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
            contentView.layer.cornerRadius = 10.0
            titleErrorLabel.text = "System Error"
            titleLabel.text = "Something went wrong. Try Pairing again. Report issue if problem persist"
            titleLabel.font = UIFont.toastMessageTextFont()
            viewContentError.isHidden = true
            heightContentError.constant = 0
            retryButton.layer.cornerRadius = 24
            skipButton.layer.cornerRadius = 24
            skipButton.layer.borderWidth = 1
            skipButton.layer.borderColor = UIColor(hexString: "#782EB1", alpha: 1).cgColor
        case .OBUNotDetected:
            print("OBU Not Detected")
            // Handle OBU not detected error
            titleErrorLabel.text  = "OBU not detected"
            contentView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
            contentView.layer.cornerRadius = 10.0
            titleLabel.text = "Check the following and try again."
            titleLabel.font = UIFont.rerouteMessageBoldFont()
            descError1Label.text = "Vehicle started"
            descError1Label.font = UIFont.toastMessageTextFont()
            descError2Label.text = "OBU is within 10 metres of your device"
            descError2Label.font = UIFont.toastMessageTextFont()
            descError3Label.text = "Mobile data enabled"
            descError3Label.font = UIFont.toastMessageTextFont()
            descError4Label.text = "No other phones care connected to your OBU"
            descError4Label.font = UIFont.toastMessageTextFont()
            retryButton.layer.cornerRadius = 24
            skipButton.layer.cornerRadius = 24
            skipButton.layer.borderWidth = 1
            skipButton.layer.borderColor = UIColor(hexString: "#782EB1", alpha: 1).cgColor
        case .OBUConnectioLost:
            print("OBU Connection Lost")
            // Handle OBU connection lost error
            titleErrorLabel.text = "OBU Connection Lost"
            contentView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
            contentView.layer.cornerRadius = 10.0
            titleLabel.text = "Check the following and try again."
            titleLabel.font = UIFont.rerouteMessageBoldFont()
            descError1Label.text = "Vehicle started"
            descError1Label.font = UIFont.toastMessageTextFont()
            descError2Label.text = "Bluetooth enabled"
            descError2Label.font = UIFont.toastMessageTextFont()
            descError3Label.text = "Mobile data enabled"
            descError3Label.font = UIFont.toastMessageTextFont()
            descError4Label.text = "No other phones care connected to your OBU"
            descError4Label.font = UIFont.toastMessageTextFont()
            retryButton.layer.cornerRadius = 24
            skipButton.layer.cornerRadius = 24
            skipButton.layer.borderWidth = 1
            skipButton.layer.borderColor = UIColor(hexString: "#782EB1", alpha: 1).cgColor
        }
    }
    

    @IBAction func dissmissAction(_ sender: Any) {
        if let _ = errorModel {
            self.dismiss(animated: true)
        } else {
            self.view.removeFromSuperview()
        }
        switch self.errorCase {
        case .PairingError:
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.ObuPairing.UserClick.pairing_error_close, screenName: ParameterName.ObuPairing.screen_view)
        case .SystemError:
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.ObuPairing.UserClick.system_error_close, screenName: ParameterName.ObuPairing.screen_view)
        case .OBUNotDetected:
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.ObuPairing.UserClick.obu_not_detected_close, screenName: ParameterName.ObuPairing.screen_view)
        case .OBUConnectioLost:
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.ObuPairing.UserClick.obu_connection_lost_close, screenName: ParameterName.ObuPairing.screen_view)
        }
    }
    
    @IBAction func skipAction(_ sender: Any) {
        if let _ = errorModel {
            self.dismiss(animated: true)
        } else {
            self.view.removeFromSuperview()
        }
        
        skipCompletion?()
        switch self.errorCase {
        case .PairingError:
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.ObuPairing.UserClick.pairing_error_skip, screenName: ParameterName.ObuPairing.screen_view)
        case .SystemError:
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.ObuPairing.UserClick.system_error_skip, screenName: ParameterName.ObuPairing.screen_view)
        case .OBUNotDetected:
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.ObuPairing.UserClick.obu_not_detected_skip, screenName: ParameterName.ObuPairing.screen_view)
        case .OBUConnectioLost:
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.ObuPairing.UserClick.obu_connection_lost_skip, screenName: ParameterName.ObuPairing.screen_view)
        }
    }
    
    @IBAction func retryAction(_ sender: Any) {
        if let _ = errorModel {
            self.retryButtonClicked()
            self.dismiss(animated: true)
        } else {
            self.view.removeFromSuperview()
        }
        retryCompletion?(errorModel?.error?.code ?? -1)
        switch self.errorCase {
        case .PairingError:
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.ObuPairing.UserClick.pairing_error_retry, screenName: ParameterName.ObuPairing.screen_view)
        case .SystemError:
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.ObuPairing.UserClick.system_error_retry, screenName: ParameterName.ObuPairing.screen_view)
        case .OBUNotDetected:
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.ObuPairing.UserClick.obu_not_detected_retry, screenName: ParameterName.ObuPairing.screen_view)
        case .OBUConnectioLost:
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.ObuPairing.UserClick.obu_connection_lost_retry, screenName: ParameterName.ObuPairing.screen_view)
        }
    }
    
    @IBAction func reportIssueAction(_ sender: Any) {
        DispatchQueue.main.async {
            Instabug.show()
            switch self.errorCase {
            case .PairingError:
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.ObuPairing.UserClick.pairing_error_report_issue, screenName: ParameterName.ObuPairing.screen_view)
            case .SystemError:
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.ObuPairing.UserClick.system_error_report_issue, screenName: ParameterName.ObuPairing.screen_view)
            case .OBUNotDetected:
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.ObuPairing.UserClick.obu_not_detected_report_issue, screenName: ParameterName.ObuPairing.screen_view)
            case .OBUConnectioLost:
                AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.ObuPairing.UserClick.obu_connection_lost_report_issue, screenName: ParameterName.ObuPairing.screen_view)
            }
        }
    }
    
    private func retryButtonClicked() {
        if let model = errorModel {
            
            let connecting = model.isConnecting ?? true
            
            if let errorCode = model.error?.code {
                switch errorCode {
                case 1003, 1004, 1005, 1006, 2002, 2003, 2009, 2010, 400, 401, 408, 500, 616:
                    if connecting {
                        self.showLoadingIndicator()
                        OBUHelper.shared.autoConnectOBUFromCarplay = false
                        SwiftyBeaver.debug("OBU - Retry auto connect error \(errorCode)")
                        OBUHelper.shared.autoConnectOBU { [weak self] (success, error) in
                            self?.hideLoadingIndicator()
                            if success {
                                //  If success then just dismiss the error popup
                                self?.dismiss(animated: true)
                            } else {
                                //  Otherwise stay error
                                if let err = error {
                                    SwiftyBeaver.debug("OBU - Retry error: \(err.code) des:\(err.localizedDescription)")
                                    
                                    let message = "Retry err:\(err.code) des:\(err.localizedDescription)"
                                    AnalyticsManager.shared.logSystemEvent(eventValue: ParameterName.SystemAnalytics.SystemEvent.obu_verbose_error, screenName: ParameterName.SystemAnalytics.screen_view, eventMessage: message)
                                }
                            }
                        }
                    } else {
                        self.showLoadingIndicator()
                        OBUHelper.shared.disconnectOBU {[weak self] (obu, error) in
                            self?.hideLoadingIndicator()
                            if let _ = error {
                                //  Stay error
                            } else {
                                //  Dimiss popup if success retry
                                self?.dismiss(animated: true)
                            }
                        }
                    }
                case 2000:
                    //  Just dimiss in this case
                    self.dismiss(animated: true)
                case 2001, 1002:
                    //  If it is level 1 of error then retry once
                    if errorCase == .OBUConnectioLost {
                        if connecting {
                            self.showLoadingIndicator()
                            OBUHelper.shared.autoConnectOBUFromCarplay = false
                            SwiftyBeaver.debug("OBU - Retry auto connect error \(errorCode)")
                            OBUHelper.shared.autoConnectOBU { [weak self] (success, error) in
                                self?.hideLoadingIndicator()
                                if success {
                                    //  If success then just dismiss the error popup
                                    self?.dismiss(animated: true)
                                } else {
                                    
                                    if let err = error {
                                        
                                        SwiftyBeaver.debug("OBU - Retry error: \(err.code) des:\(err.localizedDescription)")
                                        
                                        let message = "Retry err:\(err.code) des:\(err.localizedDescription)"
                                        AnalyticsManager.shared.logSystemEvent(eventValue: ParameterName.SystemAnalytics.SystemEvent.obu_verbose_error, screenName: ParameterName.SystemAnalytics.screen_view, eventMessage: message)
                                    }
                                    
                                    //  Level 2 error
                                    self?.errorCase = .SystemError
                                    self?.demoConnectError()
                                }
                            }
                        } else {
                            self.showLoadingIndicator()
                            OBUHelper.shared.disconnectOBU {[weak self] (obu, error) in
                                self?.hideLoadingIndicator()
                                if let _ = error {
                                    //  Level 2 error
                                    self?.errorCase = .SystemError
                                    self?.demoConnectError()
                                } else {
                                    //  Dimiss popup if success retry
                                    self?.dismiss(animated: true)
                                }
                            }
                        }
                    } else {
                        //  Level 2 error then just dismiss
                        self.dismiss(animated: true)
                    }
                    
                default:
                    break
                }
            }
        }
    }
    
}

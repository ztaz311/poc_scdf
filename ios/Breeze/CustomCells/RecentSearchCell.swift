//
//  RecentSearchCell.swift
//  Breeze
//
//  Created by VishnuKanth on 17/12/20.
//

import UIKit

class RecentSearchCell: UITableViewCell {
    
    @IBOutlet weak var recentSearchText:UILabel!
    @IBOutlet weak var leftImage:UIImageView!
    @IBOutlet weak var deleteButton:UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

//
//  TravelLogCell.swift
//  Breeze
//
//  Created by Malou Mendoza on 10/6/21.
//

import UIKit

protocol TravelLogCellDelegate: AnyObject {
    func UnSelectAll()
    func SelectedTripItem(tripIndex: Int, isToBeAdded: Bool)
}

class TravelLogCell: UITableViewCell {
    
    weak var delegate: TravelLogCellDelegate?
    @IBOutlet weak var startAddress:UILabel!
    @IBOutlet weak var destinationAddress:UILabel!
    @IBOutlet weak var distanceTrip:UILabel!
    @IBOutlet weak var timeTrip:UILabel!
    @IBOutlet weak var dateTrip:UILabel!
    @IBOutlet weak var fareTrip:UILabel!
    @IBOutlet weak var widthCheckBtnConstraint: NSLayoutConstraint!
    @IBOutlet weak var rightMarginCheckbtnConstraint: NSLayoutConstraint!
    @IBOutlet weak var checkBtn :UIButton!
    private var isCheckboxSelected = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        self.isCheckBtnHidden(isHidden: true, selectedBoxValue: true)
        hiddenSelectedTripLog()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    @IBAction func onSelect(_ sender: Any) {
        if let indexPath = (self.superview as? UITableView)?.indexPath(for: self) {
            self.isCheckboxSelected = !self.isCheckboxSelected
            if self.isCheckboxSelected{
                delegate?.SelectedTripItem(tripIndex: indexPath.row, isToBeAdded: true)
                self.checkBtn.setImage(UIImage.init(named: "selected"), for: .normal)
            }else{
                delegate?.SelectedTripItem(tripIndex: indexPath.row, isToBeAdded: false)
                delegate?.UnSelectAll()
                self.checkBtn.setImage(UIImage.init(named: "unselected"), for: .normal)
            }
        }
    }
    
    
    
//    func isCheckBtnHidden(isHidden: Bool, selectedBoxValue: Bool){
//        if isHidden{
//            self.widthCheckBtnConstraint.constant = 0
//            self.rightMarginCheckbtnConstraint.constant = 0
//            self.checkBtn.isEnabled = false
//            self.checkBtn.isUserInteractionEnabled = false
//        }else{
//            self.widthCheckBtnConstraint.constant = 25
//            self.rightMarginCheckbtnConstraint.constant = 20
//            self.checkBtn.isEnabled = true
//            self.checkBtn.isUserInteractionEnabled = true
//        }
//        self.isCheckboxSelected = selectedBoxValue
//        if selectedBoxValue{
//            self.checkBtn.setImage(UIImage.init(named: "selected"), for: .normal)
//
//        }else{
//            self.checkBtn.setImage(UIImage.init(named: "unselected"), for: .normal)
//
//        }
//        self.checkBtn.isHidden = isHidden
//    }
    
    func hiddenSelectedTripLog() {
        self.widthCheckBtnConstraint.constant = 0
        self.rightMarginCheckbtnConstraint.constant = 0
        self.checkBtn.isEnabled = false
        self.checkBtn.isUserInteractionEnabled = false
        self.checkBtn.isHidden = true
    }

}

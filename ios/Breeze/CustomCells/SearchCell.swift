//
//  SearchCell.swift
//  Breeze
//
//  Created by VishnuKanth on 18/12/20.
//

import UIKit

class SearchCell: UITableViewCell {
    
    @IBOutlet weak var navigationImage:UIImageView!
    @IBOutlet weak var searchMainTitle:UILabel!
    @IBOutlet weak var mainView:UIView!
    @IBOutlet weak var searchsubTitle:UILabel!
    @IBOutlet weak var searchdistance:UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    

}

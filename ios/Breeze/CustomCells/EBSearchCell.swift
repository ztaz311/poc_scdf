//
//  EBSearchCell.swift
//  Breeze
//
//  Created by VishnuKanth on 17/02/21.
//

import UIKit

import UIKit

class EBSearchCell: UITableViewCell {
    
    @IBOutlet weak var navigationImage:UIImageView!
    @IBOutlet weak var searchMainTitle:UILabel!
    @IBOutlet weak var mainView:UIView!
    @IBOutlet weak var searchsubTitle:UILabel!
    @IBOutlet weak var searchdistance:UILabel!
    @IBOutlet weak var easyBreezyAlias:UILabel!
    @IBOutlet weak var mainTitleTopConstraint:NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    

}

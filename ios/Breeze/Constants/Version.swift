//
//  Version.swift
//  Breeze
//
//  Created by Zhou Hao on 15/3/21.
//

import UIKit

extension UIApplication {
        
    class func versionString() -> String {
        return Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
    }
    
    class func buildString() -> String {
        return Bundle.main.object(forInfoDictionaryKey: kCFBundleVersionKey as String) as! String
    }
    
    class func appVersion() -> String {
        let version = versionString(), build = buildString()
        
        return version == build ? "\(version)" : "\(version)(\(build))"
    }
    
    class func getTopViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        
        if let nav = base as? UINavigationController, let visibleVC = nav.visibleViewController {
            return getTopViewController(base: visibleVC)
            
        } else if let tab = base as? UITabBarController, let selected = tab.selectedViewController {
            return getTopViewController(base: selected)
            
        } else if let presented = base?.presentedViewController {
            return getTopViewController(base: presented)
        }
        return base
    }
}


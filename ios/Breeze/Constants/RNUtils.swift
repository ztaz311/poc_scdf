//
//  RNUtils.swift
//  Breeze
//
//  Common utils functions related to RN
//
//  Created by Zhou Hao on 19/1/22.
//

import Foundation

/// Native - RN to inform there is a new voucher added
func newVoucherAdded() {
    _ = ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.ON_NEW_VOUCHER_ADDED,
        data: nil).subscribe(onSuccess: { result in  },
                onFailure: { error in
    })
}

/// Native - RN to inform it's ready to show voucher animation
func readyToShowVouchAnimation() {
    // ready to show vouch animation if possible
    _ = ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.CAN_SHOW_VOUCHER_ANIMATION, data: nil).subscribe(onSuccess: { result in  },
                onFailure: { error in
    })
}

func sendThemeChangeEvent(_ isDarkMode: Bool) {
    _ = ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.SEND_THEME_CHANGE_EVENT, data: ["isDarkMode":isDarkMode] as? NSDictionary).subscribe(onSuccess: {_ in
        // ...
    },                 onFailure: {_ in
        // ...
    })
}

func sendOpenOnbardingTutorial() {
    _ = ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.OPEN_ONBOARDING_TUTORIAL, data: nil).subscribe(onSuccess: {_ in
        // ...
    },                 onFailure: {_ in
        // ...
    })
}

func sendDisableProfileSelection() {
    _ = ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.DISABLE_PROFILE_AMENITIES, data: nil).subscribe(onSuccess: {_ in
        // ...
    },                 onFailure: {_ in
        // ...
    })
}

func sendSelectActionToRN(id: String, selected: Bool) {
    let updateJson: [String : Any] = ["bookmark_id": Int(id),
                                      "is_selected":selected]
    _ = ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.SELECT_SAVED_LOCATION, data: updateJson as NSDictionary).subscribe(onSuccess: {_ in
        // ...
    },                 onFailure: {_ in
        // ...
    })
}

func sendSaveActionToRN(amenityId: String,
                        lat: Double,
                        long: Double,
                        name: String,
                        address1: String,
                        address2:String,
                        amenityType: String,
                        fullAddress: String,
                        isDestination: Bool,
                        isSelected: Bool,
                        placeId: String = "",
                        userLocationLinkIdRef: Int? = nil) {
    var updateJson: [String : Any] = ["amenityId":amenityId,
                                      "lat":"\(lat)",
                                      "long":"\(long)",
                                      "name":name,
                                      "address1":address1,
                                      "address2":address2,
                                      "amenityType":amenityType,
                                      "fullAddress":fullAddress,
                                      "isDestination":isDestination,
                                      "is_selected":isSelected]
    
    if !placeId.isEmpty {
        updateJson["placeId"] = placeId
    }
    
    if let refId = userLocationLinkIdRef {
        updateJson["userLocationLinkIdRef"] = refId
    }
    
    _ = ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.UPDATE_SAVED_LOCATION, data: updateJson as NSDictionary).subscribe(onSuccess: {_ in
        // ...
    },                 onFailure: {_ in
        // ...
    })
}

func updateParkingAvailable(_ carparkId: String, type: String) {
    
    let parkingData = ["availablePercentImageBand": type, "carparkId": carparkId]
    
    _ = ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.UPDATE_PARKING_AVAILABLE_STATUS, data: parkingData as NSDictionary).subscribe(onSuccess: {_ in
        // ...
    },                 onFailure: {_ in
        // ...
    })
}

func updateBottomSheetIndex(index:Int,toScreen:String){
    
    _ = ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.UPDATE_BOTTOM_SHEET_INDEX, data: ["index":index,"toScreen":toScreen] as? NSDictionary).subscribe(onSuccess: {_ in
        // ...
    },                 onFailure: {_ in
        // ...
    })
}

func exploreSwitchBottomTab(screenName: String) {
    _ = ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.SWITCH_BOTTOM_TAB, data: ["screen_name":screenName] as? NSDictionary).subscribe(onSuccess: {_ in
        // ...
    },                 onFailure: {_ in
        // ...
    })
}

func sendTriggerUserPrefEvent(){
    
    _ = ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.TRIGGER_INIT_USER_PREF, data: nil).subscribe(onSuccess: {_ in
        // ...
    },                 onFailure: {_ in
        // ...
    })
}

func sendTriggerUserPreferenceDataEvent(){
    
    _ = ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.ON_FINISHED_INITIALIZING_USER_DATA, data: nil).subscribe(onSuccess: {_ in
        // ...
    },                 onFailure: {_ in
        // ...
    })
}

func triggerResetAmenities(completion: @escaping (String) -> ()){
    
    _ = ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.RESET_AMENITY_SELECTIONS, data: nil).subscribe(onSuccess: {  result in
        if let result = result,
           let dict = result["data"] as? NSDictionary,
           let value = dict["result"] as? String{
            
            completion(value)
        } else {
            
            completion("success")
        }
              
    },                 onFailure: {_ in
        completion("success")
    })
}


func goBackExploreMap(screenName: String) {
    _ = ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.GO_BACK_EXPLORE_MAP, data: ["screen_name":screenName] as? NSDictionary).subscribe(onSuccess: {_ in
        // ...
    },                 onFailure: {_ in
        // ...
    })
}

//  MARK - OBU
func updateOBUSpeed(_ speed: Double, limitSpeed: Double) {
    
    let speedData = ["speed": speed, "limitSpeed": limitSpeed]
    
    _ = ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.UPDATE_OBU_LITE_SPEED, data: speedData as NSDictionary).subscribe(onSuccess: {_ in

    // ...
    },                 onFailure: {_ in
        // ...
    })
}

func shareStateObuConect(_ stateConect: String, cardBalance: String, obuName: String, vehicleNumber: String, paymentMode: String) {
    
    let obuData = ["status": stateConect, "cardBalance": cardBalance, "obuName": obuName, "vehicleNumber": vehicleNumber, "paymentMode": paymentMode] as [String : Any]
    
    _ = ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.CHANGE_STATUS_PAIR, data: obuData as NSDictionary).subscribe(onSuccess: {_ in
        // ...
    },                 onFailure: {_ in
        // ...
    })
}

func updateOBUNoti(_ data: [String: Any]) {
    
    _ = ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.UPDATE_OBU_EVENT_NOTI, data: data as NSDictionary).subscribe(onSuccess: {_ in
        // ...
    },                 onFailure: {_ in
        // ...
    })
}

func setNocard(_ data: [String: Any]) {
    
    _ = ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.SET_NO_CARD, data: data as NSDictionary).subscribe(onSuccess: {_ in
        // ...
    },                 onFailure: {_ in
        // ...
    })
}

func updateCashCardInfo(_ data: [String: Any]) {
    
    _ = ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.UPDATE_OBU_CASH_CARD_INFO, data: data as NSDictionary).subscribe(onSuccess: {_ in
        // ...
    },                 onFailure: {_ in
        // ...
    })
}

func openObuInstallation() {    
    _ = ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.OPEN_MODAL_OBU_INSTALL, data: [:]).subscribe(onSuccess: {_ in
        // ...
    },                 onFailure: {_ in
        // ...
    })
}

func openObuTravelTime(_ data: [String: Any]) {
    
    _ = ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.OPEN_OBU_TRAVEL_TIME, data: data as NSDictionary).subscribe(onSuccess: {_ in
        // ...
    },                 onFailure: {_ in
        // ...
    })
}

func openObuCarparkAvailability(_ data: [String: Any]) {
    
    _ = ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.OPEN_OBU_CARPARK_AVAILABILITY, data: data as NSDictionary).subscribe(onSuccess: {_ in
        // ...
    },                 onFailure: {_ in
        // ...
    })
}

func updatePairedVehicleList(_ data: [String: Any]) {
    
    _ = ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.UPDATE_PAIRED_VEHICLE_LIST, data: data as NSDictionary).subscribe(onSuccess: {_ in
        // ...
    },                 onFailure: {_ in
        // ...
    })
}

func updateAutoConnectOBU(_ isEnable: Bool) {
    let data = ["isEnable": isEnable] as [String : Any]
    _ = ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.UPDATE_VALUE_AUTO_CONNECT_OBU, data: data as NSDictionary).subscribe(onSuccess: {_ in
        // ...
    },                 onFailure: {_ in
        // ...
    })
}

func dismissPairedVehicleList() {
    _ = ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.BACK_TO_HOME_PAGE, data: [:] as NSDictionary).subscribe(onSuccess: {_ in
        // ...
    },                 onFailure: {_ in
        // ...
    })
}

func navigateToHome(_ mode: String) {
    
    let data = ["mode": mode] as [String : Any]
    _ = ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.BACK_TO_HOME_PAGE, data: data as NSDictionary).subscribe(onSuccess: {_ in
        // ...
    },                 onFailure: {_ in
        // ...
    })
}


func disableToggleButtonCarparkList(_ disable: Bool) {
    
    let data = ["disable": disable] as [String : Any]
    _ = ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.DISABLE_TOGGLE_BUTTON_CARPARK_LIST, data: data as NSDictionary).subscribe(onSuccess: {_ in
        // ...
    },                 onFailure: {_ in
        // ...
    })
}

func selectIndexShareCollection(_ selectedIndex: Int) {
    let data = ["selectedIndex": selectedIndex] as [String : Any]
    _ = ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.SELECTED_INDEX_SHARE_COLLECTION, data: data as NSDictionary).subscribe(onSuccess: {_ in
        // ...
    },                 onFailure: {_ in
        // ...
    })
}

func setOnDropPinBottomsheet(_ isOn: Bool) {
    let data = ["isPinMode": isOn] as [String : Any]
    _ = ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.TOGGLE_LANDING_DROP_PIN_SCREEN, data: data as NSDictionary).subscribe(onSuccess: {_ in
        // ...
    },                 onFailure: {_ in
        // ...
    })
}

func addOrDeleteLocationInSaveCollectionScreen(_ locationData: [String: Any], completion: ((Int?) -> Void)? = nil) {
    
    _ = ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.TOGGLE_SAVE_LOCATION_SHARED_COLLECTION, data: locationData as NSDictionary).subscribe(onSuccess: { result in
        if let data = result {
            print("onSuccess: \(data)")
            if let dataDic = data["data"] as? [String: Any], let bookmarkId = dataDic["bookmarkId"] as? Int {
                print("addOrDeleteLocationInSaveCollectionScreen bookmarkId: \(bookmarkId)")
                completion?(bookmarkId)
            } else {
                completion?(nil)
            }
        } else {
            completion?(nil)
        }
    }, onFailure: { error in
        print("onFailure: ", error)
        completion?(nil)
    })
}

func addOrDeleteDropPinLocationInCollectionDetail(_ pinData: [String: Any], completion: ((Int?) -> Void)? = nil) {
    
    _ = ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.TOGGLE_SAVE_LOCATION_COLLECTION_DETAILS, data: pinData as NSDictionary).subscribe(onSuccess: { result in
        if let data = result {
            print("onSuccess: \(data)")
            if let dataDic = data["data"] as? [String: Any], let bookmarkId = dataDic["bookmarkId"] as? Int {
                print("addOrDeleteDropPinLocation bookmarkId: \(bookmarkId)")
                completion?(bookmarkId)
            } else {
                completion?(nil)
            }
        } else {
            completion?(nil)
        }
    }, onFailure: { error in
        print("onFailure: ", error)
        completion?(nil)
    })
}

func addOrDeleteDropPinLocation(_ pinData: [String: Any], completion: ((Int?) -> Void)? = nil) {
    
    _ = ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.TOGGLE_SAVE_DROP_PIN_LOCATION_LANDING, data: pinData as NSDictionary).subscribe(onSuccess: { result in
        if let data = result {
            print("onSuccess: \(data)")
            if let dataDic = data["data"] as? [String: Any], let bookmarkId = dataDic["bookmarkId"] as? Int {
                print("addOrDeleteDropPinLocation bookmarkId: \(bookmarkId)")
                completion?(bookmarkId)
            } else {
                completion?(nil)
            }
        } else {
            completion?(nil)
        }
    }, onFailure: { error in
        print("onFailure: ", error)
        completion?(nil)
    })
}

func toggleDropPinDetailCollection(_ pinModel: DropPinModel?, isDropPin: Bool, completion: ((Int?) -> Void)? = nil) {
        
    var sendData: [String : Any] = ["isDropPin": isDropPin]
    //  If isDropPin equal to false then no need to send drop pin data
    if isDropPin {
        if let model = pinModel {
            let pinData: [String: Any] = ["lat": model.lat, "long": model.long, "name": model.name ?? "", "address1": model.address ?? "", "address2": model.address2 ?? "", "fullAddress": model.fullAddress ?? ""]
            sendData["data"] = pinData
        }
    }
    _ = ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.TOGGLE_DROP_PIN_COLLECTION_DETAILS, data: sendData as NSDictionary).subscribe(onSuccess: { result in
        if let data = result {
            print("onSuccess: \(data)")
            if let dataDic = data["data"] as? [String: Any], let bookmarkId = dataDic["bookmarkId"] as? Int {
                print("addOrDeleteDropPinLocation bookmarkId: \(bookmarkId)")
                completion?(bookmarkId)
            } else {
                completion?(nil)
            }
        } else {
            completion?(nil)
        }
    }, onFailure: { error in
        print("onFailure: ", error)
        completion?(nil)
    })
}

func toggleDropPinLanding(_ pinModel: DropPinModel?, isDropPin: Bool, completion: ((Int?) -> Void)? = nil) {
        
    var sendData: [String : Any] = ["isDropPin": isDropPin]
    //  If isDropPin equal to false then no need to send drop pin data
    if isDropPin {
        if let model = pinModel {
            let pinData: [String: Any] = ["lat": model.lat, "long": model.long, "name": model.name ?? "", "address1": model.address ?? "", "address2": model.address2 ?? "", "fullAddress": model.fullAddress ?? ""]
            sendData["data"] = pinData
        }
    }
    _ = ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.TOGGLE_DROP_PIN_LANDING, data: sendData as NSDictionary).subscribe(onSuccess: { result in
        if let data = result {
            print("onSuccess: \(data)")
            if let dataDic = data["data"] as? [String: Any], let bookmarkId = dataDic["bookmarkId"] as? Int {
                print("addOrDeleteDropPinLocation bookmarkId: \(bookmarkId)")
                completion?(bookmarkId)
            } else {
                completion?(nil)
            }
        } else {
            completion?(nil)
        }
    }, onFailure: { error in
        print("onFailure: ", error)
        completion?(nil)
    })
}


// Share via location from tooltip
/*
 By default type is DEFAULT if user share destination from navigation/route planning it will be NAVIGATION/ROUTE_PLANNING
 */

func shareLocationFromNative(address1: String, address2: String = "", latitude: String, longtitude: String, code: String = "", is_destination: Bool = false, amenity_id: String = "", amenity_type: String = "", description: String = "", name: String = "", fullAddress: String = "", bookmarkId: Int = 0, type: String = "DEFAULT", placeId: String = "", userLocationLinkIdRef: Int? = nil) {
    
    var shareData: [String: Any] = [:]
    
    if bookmarkId == 0 {
        shareData = ["address1": address1, "address2": address2, "latitude": latitude, "longitude": longtitude, "code": code, "isDestination": is_destination, "amenityId": amenity_id, "amenityType": amenity_type, "description": description, "name": name, "fullAddress": fullAddress, "type": type] as [String : Any]
    } else {
         shareData = ["address1": address1, "address2": address2, "latitude": latitude, "longitude": longtitude, "code": code, "isDestination": is_destination, "amenityId": amenity_id, "amenityType": amenity_type, "description": description, "name": name, "fullAddress": fullAddress, "type": type, "bookmarkId": bookmarkId] as [String : Any]
    }
    
    if !placeId.isEmpty {
        shareData["placeId"] = placeId
    }
    
    if let refId = userLocationLinkIdRef {
        shareData["userLocationLinkIdRef"] = refId
    }
    
    _ = ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.SHARE_LOCATION_FROM_NATIVE, data: shareData as NSDictionary).subscribe(onSuccess: {_ in
        // ...
    },                 onFailure: {_ in
        // ...
    })
}

func inviteLocationFromNative(address1: String, address2: String = "", latitude: String, longtitude: String, code: String = "", is_destination: Bool = false, amenity_id: String = "", amenity_type: String = "", description: String = "", name: String = "", fullAddress: String = "", bookmarkId: Int = 0, type: String = "DEFAULT", placeId: String = "", userLocationLinkIdRef: Int? = nil) {
    
    var shareData: [String: Any] = [:]
    
    if bookmarkId == 0 {
         shareData = ["address1": address1, "address2": address2, "latitude": latitude, "longitude": longtitude, "code": code, "isDestination": is_destination, "amenityId": amenity_id, "amenityType": amenity_type, "description": description, "name": name, "fullAddress": fullAddress, "type": type] as [String : Any]
    } else {
         shareData = ["address1": address1, "address2": address2, "latitude": latitude, "longitude": longtitude, "code": code, "isDestination": is_destination, "amenityId": amenity_id, "amenityType": amenity_type, "description": description, "name": name, "fullAddress": fullAddress, "type": type, "bookmarkId": bookmarkId] as [String : Any]
    }
    
    if !placeId.isEmpty {
        shareData["placeId"] = placeId
    }
    
    if let refId = userLocationLinkIdRef {
        shareData["userLocationLinkIdRef"] = refId
    }
    
    _ = ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.LOCATION_INVITE_FROM_NATIVE, data: shareData as NSDictionary).subscribe(onSuccess: {_ in
        // ...
    },                 onFailure: {_ in
        // ...
    })
}



//
//  CommonExtensions.swift
//  Breeze
//
//  Created by VishnuKanth on 27/11/20.
//

import Foundation
import UIKit
import Reachability
import Instabug
@_spi(Experimental) import MapboxMaps
import MapboxCoreMaps
import Turf
import SwiftyBeaver
import RxSwift
import AVFoundation
import MapboxDirections
import func Polyline.encodeCoordinates
import SwiftyBeaver
#if canImport(CoreLocation)
import typealias Polyline.LocationCoordinate2D
#else
import struct Polyline.LocationCoordinate2D
#endif

extension LineString {
    /**
     Returns a string representation of the line string in [Polyline Algorithm Format](https://developers.google.com/maps/documentation/utilities/polylinealgorithm).
     */
    func polylineEncodedString(precision: Double = 1e5) -> String {
        #if canImport(CoreLocation)
        let coordinates = self.coordinates
        #else
        let coordinates = self.coordinates.map { Polyline.LocationCoordinate2D(latitude: $0.latitude, longitude: $0.longitude) }
        #endif
        return encodeCoordinates(coordinates, precision: precision)
    }
}

enum PolyLineString {
    case lineString(_ lineString: LineString)
    case polyline(_ encodedPolyline: String, precision: Double)
    
    init(lineString: LineString, shapeFormat: RouteShapeFormat) {
        switch shapeFormat {
        case .geoJSON:
            self = .lineString(lineString)
        case .polyline, .polyline6:
            let precision = shapeFormat == .polyline6 ? 1e6 : 1e5
            let encodedPolyline = lineString.polylineEncodedString(precision: precision)
            self = .polyline(encodedPolyline, precision: precision)
        }
    }
}

extension PolyLineString: Codable {
    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        let options = decoder.userInfo[.options] as? DirectionsOptions
        switch options?.shapeFormat ?? .polyline6 {
        case .geoJSON:
            self = .lineString(try container.decode(LineString.self))
        case .polyline, .polyline6:
            let precision = options?.shapeFormat == .polyline6 ? 1e6 : 1e5
            let encodedPolyline = try container.decode(String.self)
            self = .polyline(encodedPolyline, precision: precision)
        }
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        switch self {
        case let .lineString(lineString):
            try container.encode(lineString)
        case let .polyline(encodedPolyline, precision: _):
            try container.encode(encodedPolyline)
        }
    }
}

struct LocationCoordinate2DCodable: Codable {
    var latitude: Turf.LocationDegrees
    var longitude: Turf.LocationDegrees
    var decodedCoordinates: Turf.LocationCoordinate2D {
        return Turf.LocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.unkeyedContainer()
        try container.encode(longitude)
        try container.encode(latitude)
    }
    
    init(from decoder: Decoder) throws {
        var container = try decoder.unkeyedContainer()
        longitude = try container.decode(Turf.LocationDegrees.self)
        latitude = try container.decode(Turf.LocationDegrees.self)
    }
    
    init(_ coordinate: Turf.LocationCoordinate2D) {
        latitude = coordinate.latitude
        longitude = coordinate.longitude
    }
}

extension AVAudioSession {
    
    typealias DuckAudioHandle = (Error?)-> (Void)
    
    var audioDispatchQueue: DispatchQueue {
        return appDelegate().audioDispatchQueue
    }
    
    /**
        Duck Audio - call this function when ever there is a Voice Instruction during Navigation/Cruise Mode
        If isOtherAudioPlaying - use Options
        If isOtherAudioNotPlaying - don't use any options
     */
    
    func tryDuckOtherAudio(handle: DuckAudioHandle? = nil) {
        if Prefs.shared.getBoolValue(.enableAudio) {
            audioDispatchQueue.async(flags: .barrier) { [weak self] in
                do {
                    // When other audio is playing using different options
                    try self?.setActive(false)
                    try self?.setCategory(.playback, mode: .voicePrompt, options: [.duckOthers, .interruptSpokenAudioAndMixWithOthers])
                    try self?.setActive(true)
                    handle?(nil)
                } catch {
//                    SwiftyBeaver.error("tryDuckOtherAudio: \(error.localizedDescription)")
                    handle?(error)
                }
            }
        }
    }
    
    /**
        UnDuck Audio - call this function when ever voice instructions finishes speaking during Navigation/Cruise Mode
        If isOtherAudioPlaying - then only deActivate session and notify others
     */
    
    func tryUnduckOtherAudio(handle: DuckAudioHandle? = nil) {
        audioDispatchQueue.async(flags: .barrier) { [weak self] in
            do {
                //If otherAudio is playing then set current session active to false and notify others
                try self?.setActive(false, options: [.notifyOthersOnDeactivation])
                handle?(nil)
            } catch {
//                SwiftyBeaver.error("tryUnduckOtherAudio: \(error.localizedDescription)")
                handle?(error)
            }
        }
    }
    
    /**
        Duck Audio - Use this during app launch/when ever Cruise mode ends/navigation ends
     */
    
    func setAudioSessionActive(handle: DuckAudioHandle? = nil){
        audioDispatchQueue.async(flags: .barrier) { [weak self] in
            do {
                try self?.setCategory(.ambient, options: .interruptSpokenAudioAndMixWithOthers)
                try self?.setActive(true)
                handle?(nil)
            }catch {
//                SwiftyBeaver.error("setAudioSessionActive: \(error.localizedDescription)")
                handle?(error)
            }
        }
    }
}

extension UIView{
    
    var safeViewArea: UIEdgeInsets {
        guard #available(iOS 11.0, *) else { return .zero }
        return safeAreaInsets
    }
}
extension UIEdgeInsets {
    
    var centerCameraEdgeInsets: UIEdgeInsets {
        return UIEdgeInsets(top: 10.0, left: 20.0, bottom: 10.0, right: 20.0)
    }
}

extension Locale {
    
    static func enSGLocale() -> Locale{
        
        return Locale(identifier: Values.enSGLocale)
    }
    
    static func enGBLocale() -> Locale{
        
        return Locale(identifier: Values.enGBLocale)
    }
}

extension Array {
    mutating func remove(at indexes: [Int]) {
        var lastIndex: Int? = nil
        for index in indexes.sorted(by: >) {
            guard lastIndex != index else {
                continue
            }
            if(self.indices.contains(index)){
                remove(at: index)
            }
            
            lastIndex = index
        }
    }
    
//    func indexOf<T : Equatable>(x:T) -> Int? {
//        for i in 0...self.count {
//            if self[i] as! T == x {
//                    return i
//                }
//            }
//            return nil
//        }
}

extension UIView {
    
    func getViewController() -> UIViewController? {
        if let nextResponder = self.next as? UIViewController {
            return nextResponder
        } else if let nextResponder = self.next as? UIView {
            return nextResponder.getViewController()
        } else {
            return nil
        }
    }
    
    func roundCorners(_ corners:UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask        
      }
    
    func shadowToTopOfView(){
        self.layer.masksToBounds = false
        self.layer.shadowRadius = 4
        self.layer.shadowOpacity = 1
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOffset = CGSize(width: 0 , height:4)
    }
    
    // show shadow for top and bottom
    func showShadow(){
        self.layer.masksToBounds = false
        self.layer.shadowRadius = 6
        self.layer.shadowOpacity = 0.4
        self.layer.shadowColor = UIColor(named:"tooltipsShadowColor")!.cgColor
        self.layer.shadowOffset = CGSize(width: 0 , height:-3)
    }

    // show shadow at bottom only
    func showBottomShadow() {
        // not so obvious version
        self.layer.masksToBounds = false
        self.layer.shadowRadius = 1.0
        self.layer.shadowOpacity = 0.8
        self.layer.shadowColor = UIColor.rgba(r: 0, g: 0, b: 0, a: 0.16).cgColor
        self.layer.shadowOffset = CGSize(width: 0 , height:2)

//        self.layer.masksToBounds = false
//        let height = self.bounds.height
//        let width = self.bounds.width
//        let shadowSize: CGFloat = 10
//        let contactRect = CGRect(x:-20, y: height - (shadowSize * 0.4), width: width + 40, height: shadowSize)
//
//        self.layer.shadowColor = UIColor.lightGray.cgColor
//        self.layer.shadowPath = UIBezierPath(ovalIn: contactRect).cgPath
//        self.layer.shadowRadius = 2
//        self.layer.shadowOpacity = 0.8
    }
    
    func hideBottomShadow() {
        self.layer.shadowOpacity = 0
    }
    
    @discardableResult
    func applyViewgradient(colors:[CGColor], vertical: Bool = false, cornerRadius: CGFloat = 0) -> CAGradientLayer {
        self.backgroundColor = nil
        self.layoutIfNeeded()
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = colors
        gradientLayer.startPoint = CGPoint(x: 0, y: 0)
        gradientLayer.endPoint = vertical ? CGPoint(x: 0, y: 1) : CGPoint(x: 1, y: 0)
        gradientLayer.frame = self.bounds
        gradientLayer.shadowColor = UIColor.shadowColor.cgColor
        gradientLayer.shadowOffset = CGSize(width: 0, height: 4)
        gradientLayer.shadowRadius = 7.0
        gradientLayer.shadowOpacity = 0.3
        gradientLayer.masksToBounds = false
        if cornerRadius > 0 {
            gradientLayer.cornerRadius = cornerRadius
        }
        self.layer.insertSublayer(gradientLayer, at: 0)
        return gradientLayer
    }
    
    @discardableResult
    func applyBackgroundGradient(with name: String, colors:[CGColor], vertical: Bool = false, cornerRadius: CGFloat = 0) -> CAGradientLayer {
        self.backgroundColor = nil
        self.layoutIfNeeded()
        
        removeBackgroundGradient(with: name)
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = colors
        gradientLayer.startPoint = CGPoint(x: 0, y: 0)
        gradientLayer.endPoint = vertical ? CGPoint(x: 0, y: 1) : CGPoint(x: 1, y: 0)
        gradientLayer.frame = self.bounds
        gradientLayer.shadowColor = UIColor.shadowColor.cgColor
        gradientLayer.shadowOffset = CGSize(width: 0, height: 4)
        gradientLayer.shadowRadius = 7.0
        gradientLayer.shadowOpacity = 0.3
        gradientLayer.masksToBounds = false
        gradientLayer.name = name
        if cornerRadius > 0 {
            gradientLayer.cornerRadius = cornerRadius
        }
        self.layer.insertSublayer(gradientLayer, at: 0)
        return gradientLayer
    }
    
    func removeBackgroundGradient(with name: String){
        if let layers = self.layer.sublayers {
            
            for (index, layer) in layers.enumerated() {
                
                if layer.name == name {
                    
                    self.layer.sublayers?[index].removeFromSuperlayer()
                    break
                }
            }
        }
    }
    
    func addTapView(tag: Int, handle: @escaping TapGestureHandle) {
        if let view = self.viewWithTag(tag) {
            self.bringSubviewToFront(view)
            return
        }
        
        let tapView = TapView(frame: self.bounds)
        tapView.backgroundColor = UIColor(hexString: "ffffff", alpha: 0.01)
        tapView.tag = tag
        tapView.tapHandle = handle
        
        self.addSubview(tapView)
        tapView.snp.makeConstraints { make in
            make.top.bottom.leading.trailing.equalToSuperview()
        }
    }
    
    func removeTapView(_ tag: Int) {
        if let view = self.viewWithTag(tag) {
            view.removeFromSuperview()
        }
    }
    
    func isTapViewExit() -> Bool {
        return self.viewWithTag(Values.tapViewTag) != nil
    }
    
    func showLoadingIndicator() {
        if let view = self.viewWithTag(Values.indicatorContainerTag),
           let indicator = view.viewWithTag(Values.indicatorTag) as? UIActivityIndicatorView {
            self.bringSubviewToFront(view)
            view.isHidden = false
            indicator.startAnimating()
            return
        }
        let view = UIView(frame: self.bounds)
        view.backgroundColor = .clear
        view.tag = Values.indicatorTag
        
        let indicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        indicator.style = .large
        indicator.color = UIColor(hexString: "782EB1", alpha: 1)
        indicator.isHidden = false
        indicator.center = view.center
        
        self.addSubview(view)
        
        view.addSubview(indicator)
        
        let dict = ["view": view, "indicator": indicator]
        
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[view]-0-|", metrics: nil, views: dict))
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[view]-0-|", metrics: nil, views: dict))
    
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[indicator(==40)]", options: NSLayoutConstraint.FormatOptions(rawValue: 0), metrics: nil, views: dict))
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[indicator(==40)]", options: NSLayoutConstraint.FormatOptions(rawValue: 0), metrics: nil, views: dict))
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[view]-(<=1)-[indicator]", options: .alignAllCenterX, metrics: nil, views: dict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[view]-(<=1)-[indicator]", options: .alignAllCenterY, metrics: nil, views:
        dict))
        
        indicator.startAnimating()
        view.layoutIfNeeded()
    }
    
    func hideLoadingIndicator() {
        if let view = self.viewWithTag(Values.indicatorTag) {
            view.removeFromSuperview()
        }
    }
    
    func roundedBorder(cornerRadius: CGFloat, borderColor: CGColor, borderWidth: CGFloat) {
        self.layer.cornerRadius = cornerRadius
        self.layer.masksToBounds = true
        self.layer.borderColor = borderColor
        self.layer.borderWidth = borderWidth
    }
    
    func takeScreenshot() -> UIImage {
        // Begin context
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, UIScreen.main.scale)
        // Draw view in that context
        drawHierarchy(in: self.bounds, afterScreenUpdates: true)        
        // And finally, get image
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        if (image != nil)
        {
            return image!
        }
        return UIImage()
    }

}

extension UIViewController {
    
    // MARK: -- For dark/light mode
    var isDarkMode: Bool {
        
        if Thread.isMainThread {
            return self.traitCollection.userInterfaceStyle == .dark
        }
        
        var isDark = false
        DispatchQueue.main.sync {
            isDark = self.traitCollection.userInterfaceStyle == .dark
        }
        return isDark
        
    }
    
    func setupDarkLightAppearance(forceLightMode: Bool = false) {
        if forceLightMode {
            overrideUserInterfaceStyle = .light
            return
        }

        overrideUserInterfaceStyle = UIUserInterfaceStyle(rawValue: Settings.shared.theme) ?? .unspecified
        
        sendThemeChangeEvent(isDarkMode)
    }
    
    var isFastestRouteOption:Bool{
        
        return Settings.shared.routeOption == 0 ? true:false
    }

    func allowDismissKeyboardWhenTapped() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(onTouch))
        tapGesture.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapGesture)
    }
    
    @objc private func onTouch() {
        // dismiss keyboard
        self.view.endEditing(true)
    }

    func btnActionBack(_ sender: UIButton) {
        moveBack()
    }
    
    func moveBack(){
        
        navigationController?.popViewController(animated: true)
    }
    
    func validateEmail(enteredEmail:String) -> Bool {
        
        let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        return emailPredicate.evaluate(with: enteredEmail)
        
    }
    
    func validateUsername(enteredUsername:String) -> Bool {
        
        let usernameFormat = "^[a-zA-Z ]{1,14}$"
        let usernamePredicate = NSPredicate(format:"SELF MATCHES %@", usernameFormat)
        return usernamePredicate.evaluate(with: enteredUsername)
        
    }
    
    func validatePassword(enteredPassword:String) -> Bool {
        
        let pwFormat = "^(?=.*[A-Z])(?=.*[!@#$&*])(?=.*[0-9])(?=.*[a-z]).{8,}$"
        let pwPredicate = NSPredicate(format:"SELF MATCHES %@", pwFormat)
        return pwPredicate.evaluate(with: enteredPassword)
        
    }
    
    func validateMobile(enteredMobile:String) -> Bool {
        
        let mobileFormat = "[9|8]{1}[0-9]{7}"
        let mobilePredicate = NSPredicate(format:"SELF MATCHES %@", mobileFormat)
        return mobilePredicate.evaluate(with: enteredMobile)
        
    }
    
    
    
    func goToLandingVC()
    {
        self.navigationController?.navigationBar.isHidden = true
        let storyboard = UIStoryboard(name: "MapsLandingVC", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "mainMap") as UIViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func goToLoginVC(){
        
        self.dismiss(animated: true, completion: nil)
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc: UIViewController = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as UIViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func goToSearchVC(){
        
        self.dismiss(animated: true, completion: nil)
        let storyboard: UIStoryboard = UIStoryboard(name: "MapsLandingVC", bundle: nil)
        let vc: UIViewController = storyboard.instantiateViewController(withIdentifier: "searchView") as UIViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func goToFeatureNorReadyVC(){
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let vc = storyboard.instantiateViewController(withIdentifier: String(describing: FeatureNotReadyVC.self)) as? FeatureNotReadyVC {
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    func addChildViewControllerWithView(childViewController: UIViewController, toView view: UIView? = nil, belowView: UIView? = nil) {
        
        let view: UIView = view ?? self.view
        
        childViewController.removeFromParent()
        childViewController.willMove(toParent: nil)
        childViewController.removeFromParent()
        childViewController.didMove(toParent: nil)
        childViewController.view.removeFromSuperview()
        
        addChild(childViewController)
        if let below = belowView {
            view.insertSubview(childViewController.view, belowSubview: below)
        }else {
            view.addSubview(childViewController.view)
        }
        childViewController.didMove(toParent: self)

        view.layoutIfNeeded()
        
        navigationController?.setNeedsStatusBarAppearanceUpdate()
    }
    
    func addOnlyChildWithoutRemoving(childViewController: UIViewController, toView view: UIView? = nil){
        
        let view: UIView = view ?? self.view
        print("UseEffect check")
        addChild(childViewController)
        view.addSubview(childViewController.view)
        childViewController.didMove(toParent: self)

//        view.layoutIfNeeded()
//
//        navigationController?.setNeedsStatusBarAppearanceUpdate()
    }

    func removeChildViewController(_ childViewController: UIViewController) {
    
        childViewController.willMove(toParent: nil)
        childViewController.view.removeFromSuperview()
        childViewController.removeFromParent()
//        view.layoutIfNeeded()
//
//        navigationController?.setNeedsStatusBarAppearanceUpdate()
    }
    
    // common function to showAlert
    func showAlert(title: String, message: String, confirmTitle: String, handler: ((UIAlertAction) -> Void)? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: confirmTitle, style: .default, handler: handler))
        self.present(alert, animated: true, completion: nil)
    }
    
    func popupAlert(title: String?, message: String?, actionTitles:[String?], actions:[((UIAlertAction) -> Void)?], autoDismissIn interval: TimeInterval = 0) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        for (index, title) in actionTitles.enumerated() {
            let action = UIAlertAction(title: title, style: .default, handler: actions[index])
            if(title == Constants.clearAll || title == Constants.remove || title == Constants.logout || title ==  Constants.end || title ==  "Delete")
            {
                action.setValue(UIColor.clearSearchAlertButtonColor, forKey: "titleTextColor")
            }
            alert.addAction(action)
        }
        self.present(alert, animated: true) {
            if interval > 0 {
                DispatchQueue.main.asyncAfter(deadline: .now() + interval) {
                    alert.dismiss(animated: true, completion: nil)
                }
            }
        }
    }
    
    func stopObuDisplayIfNeeded() {
        if let mapLandingVC = appDelegate().getMapLandingVCInTop() {
            //  Handle hide OBU display when user choose a search location
            /*
            if let nav = mapLandingVC.navigationController?.presentedViewController as? UINavigationController {
                if let obuDisplayVC = nav.viewControllers.first as? OBUDisplayViewController {
                    obuDisplayVC.closeAction("")
                }
            }
             */
            
            if let vcs = mapLandingVC.navigationController?.viewControllers {
                for vc in vcs {
                    if let obuVC = vc as? OBUDisplayViewController {
                        obuVC.closeAction("")
                        break
                    }
                }
            }
        }
    }
    
    func showPermissionAlert() {
        
        SwiftyBeaver.debug("Bluetooth showPermissionAlert")
        
        if let appName = Bundle.main.infoDictionary?["CFBundleDisplayName"] as? String {
            let alertController = UIAlertController(title: "\"\(appName)\" would like to use Bluetooth",
                                                    message: "Allow Breeze to use Bluetooth for connecting to onboard device in vehicle",
                                                    preferredStyle: .alert)
            let settingsAction = UIAlertAction(title: "Settings", style: .default) { _ in
                guard let settingsURL = URL(string: UIApplication.openSettingsURLString) else { return }
                
                UIApplication.shared.open(settingsURL, options: [:], completionHandler: nil)
                AnalyticsManager.shared.logOpenPopupEvent(popupName: ParameterName.ObuPairing.UserClick.popup_obu_permission_turn_on_bluetooth_setting, screenName: ParameterName.ObuPairing.screen_view)
            }

            let cancelAction = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
            AnalyticsManager.shared.logOpenPopupEvent(popupName: ParameterName.ObuPairing.UserClick.popup_obu_permission_turn_on_bluetooth_ok, screenName: ParameterName.ObuPairing.screen_view)
            alertController.addAction(settingsAction)
            alertController.addAction(cancelAction)

            present(alertController, animated: true, completion: nil)
        }
    }
    
    func showBluetoothAlert() {
        
        SwiftyBeaver.debug("Bluetooth showBluetoothAlert")
        let alertController = UIAlertController(title: "Bluetooth Off",
                                                message: "Turn on Bluetooth to allow “Breeze” to connect to Accessories.",
                                                preferredStyle: .alert)
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { _ in
            guard let settingsURL = URL(string: UIApplication.openSettingsURLString) else { return }
            UIApplication.shared.open(settingsURL, options: [:], completionHandler: nil)
            AnalyticsManager.shared.logOpenPopupEvent(popupName: ParameterName.ObuPairing.UserClick.popup_obu_permission_turn_on_bluetooth_setting, screenName: ParameterName.ObuPairing.screen_view)
        }

        let cancelAction = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
        AnalyticsManager.shared.logOpenPopupEvent(popupName: ParameterName.ObuPairing.UserClick.popup_obu_permission_turn_on_bluetooth_ok, screenName: ParameterName.ObuPairing.screen_view)
        alertController.addAction(settingsAction)
        alertController.addAction(cancelAction)

        present(alertController, animated: true, completion: nil)
    }
    
    func dismissAnyAlertControllerIfPresent() {
        guard let window :UIWindow = UIApplication.shared.keyWindow , var topVC = window.rootViewController?.presentedViewController else {return}
        while topVC.presentedViewController != nil  {
            topVC = topVC.presentedViewController!
        }
        if topVC.isKind(of: UIAlertController.self) {
            topVC.dismiss(animated: false, completion: nil)
        }
    }
    
    
    @objc func feedbackTapped(sender: UserMenuButton) {
        print("feedback action")
        Instabug.show()
        
    }
    @objc  
    func tileListTapped(sender: UserMenuButton) {
        print("tile List Action")
        
        self.navigationController?.navigationBar.isHidden = true
        let storyboard = UIStoryboard(name: "TaskListVC", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "taskVC") as UIViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func ShowAndHideFloatingView(value:Bool){
        
        if let foundView = UIApplication.shared.keyWindow?.viewWithTag(200) {
            
            foundView.isHidden = value
        }
    }
    
    func goToRNScreen(toScreen:String,navigationParams:[String:Any]){
        
        let aComplexData = [
            "toScreen": toScreen, // Screen name you want to display when start RN
            "navigationParams": navigationParams
        ] as [String : Any]
        let rootView = RNViewManager.sharedInstance.viewForModule(
            "Breeze",
            initialProperties: aComplexData)

        let reactNativeVC = UIViewController()
        reactNativeVC.view = rootView
        reactNativeVC.modalPresentationStyle = .fullScreen
        navigationController?.pushViewController(reactNativeVC, animated: true)
    }

    func openRNScreen(toScreen:String,navigationParams:[String:Any]) -> UIViewController {
        
        let aComplexData = [
            "toScreen": toScreen, // Screen name you want to display when start RN
            "navigationParams": navigationParams
        ] as [String : Any]
        let rootView = RNViewManager.sharedInstance.viewForModule(
            "Breeze",
            initialProperties: aComplexData)

        let reactNativeVC = UIViewController()
        reactNativeVC.view = rootView
        reactNativeVC.modalPresentationStyle = .fullScreen
        navigationController?.pushViewController(reactNativeVC, animated: true)
        return reactNativeVC
    }
    
    func getRNView(toScreen: String, navigationParams: [String: Any]) -> UIViewController {
        let aComplexData = [
            "toScreen": toScreen, // Screen name you want to display when start RN
            "navigationParams": navigationParams
        ] as [String : Any]
        let rootView = RNViewManager.sharedInstance.viewForModule(
            "Breeze",
            initialProperties: aComplexData)
        rootView.backgroundColor = .clear
        rootView.passThroughTouches = true
        let reactNativeVC = UIViewController()
        reactNativeVC.view = rootView
        reactNativeVC.modalPresentationStyle = .fullScreen
        return reactNativeVC
    }

    func getRNViewBottomSheet(toScreen:String,navigationParams:[String:Any]) -> UIViewController {
        
        let aComplexData = [
            "toScreen": toScreen, // Screen name you want to display when start RN
            "navigationParams": navigationParams
        ] as [String : Any]
        let rootView = RNViewManager.sharedInstance.viewForModule(
            "Breeze",
            initialProperties: aComplexData)
        rootView.backgroundColor = .clear;
        rootView.passThroughTouches = true
        let reactNativeVC = UIViewController()
        reactNativeVC.view = rootView
        reactNativeVC.modalPresentationStyle = .fullScreen
        return reactNativeVC
    }
    
    func getRNViewBottomSheetAsView(toScreen:String,navigationParams:[String:Any]) -> UIView{
        
        let aComplexData = [
            "toScreen": toScreen, // Screen name you want to display when start RN
            "navigationParams": navigationParams
        ] as [String : Any]
        let rootView = RNViewManager.sharedInstance.viewForModule(
            "Breeze",
            initialProperties: aComplexData)
        rootView.backgroundColor = .clear;
        rootView.passThroughTouches = true
        return rootView
        
    
    }
    
    func checkShowObuInstallIfNeeded() {
        let didShowObuInstallation = Prefs.shared.getBoolValueWithSub(AWSAuth.sharedInstance.cognitoId, key: .didShowObuInstallation)
        if !didShowObuInstallation {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { [weak self] in
                guard let self = self else { return }
                let obuInstallation = self.getRNViewBottomSheet(toScreen: RNScreeNames.OBUModalPair_Install, navigationParams: [:])
                obuInstallation.view.frame = UIScreen.main.bounds;
                self.addOnlyChildWithoutRemoving(childViewController: obuInstallation, toView: self.view)
            }
        }
    }
    
    @objc
    func floatingViewGestureHandler(gesture: CustompanGestureRecognizer){
        
        let location = gesture.location(in: self.view)
        let draggedView = gesture.view
        draggedView?.center = location
        
        let draggableView = gesture.floatingView
        
        if gesture.state == .ended {
           
            if draggableView!.frame.midY >= self.view.layer.frame.height / 2
            {
                
                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseIn, animations: {
                    draggableView!.center.y = self.view.layer.frame.height - 90
                }, completion: nil)
            }
            
            
            else
            {
                if draggableView!.frame.midY <= 90
                {
                    UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseIn, animations: {
                        draggableView?.center.y = 90
                    }, completion: nil)
                }
                
            }
            
            if draggableView!.frame.midX >= self.view.layer.frame.width / 2
            {
                
                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseIn, animations: {
                    draggableView!.center.x = self.view.layer.frame.width - 40
                }, completion: nil)
            }
            
            
            else
            {
                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseIn, animations: {
                    draggableView?.center.x = 40
                }, completion: nil)
            }
        }
        
    }
    
    // Share link
    func shareActivityMapCollection(_ shareLink: String, isSaved: Bool) {
        let shareItems: [Any] = [BreezeActivityItemSource(text: shareLink)]
        let activityViewController = self.share(items: shareItems, excludedActivityTypes: [.postToFacebook, .airDrop])
        
        self.present(activityViewController,
                     animated: true)
        activityViewController.completionWithItemsHandler = {
            (activity, success, items, error) in
            if isSaved {
                ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.OPEN_COLLECTION_DETAIL_FROM_SHARED, data: [:]).subscribe(onSuccess: {_ in
                    //  No-op
                },         onFailure: {_ in
                    //  No-op
                })
            }
            
            ShareBusinessLogicModule.callRN(ShareBusinessLogicModule.DISMISS_SHARING_DIALOG, data: [:]).subscribe(onSuccess: {_ in
                //  No-op
            },         onFailure: {_ in
                //  No-op
            })
            
        }
    }
    
    private func share(items: [Any],
                       excludedActivityTypes: [UIActivity.ActivityType]? = nil,
                       ipad: (forIpad: Bool, view: UIView?) = (false, nil)) -> UIActivityViewController {
        let activityViewController = UIActivityViewController(activityItems: items,
                                                              applicationActivities: nil)
        if ipad.forIpad {
            activityViewController.popoverPresentationController?.sourceView = ipad.view
        }
        
        if let excludedActivityTypes = excludedActivityTypes {
            activityViewController.excludedActivityTypes = excludedActivityTypes
        }
        
        return activityViewController
    }
    
}

extension UIDevice {
    
    var modelName: String {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        return identifier
    }
    
    func systemVersionInfo() -> String {
        return self.systemVersion
    }
    
    func deviceOS () -> String{
        return "IOS"
    }
    
    func getName () -> String{
        return UIDevice.current.model
    }
    
}

extension UIViewController: ConnectionListener {
    public func connectionChanged(status: Reachability.Connection) {
      switch status {
      case .cellular:
        statusBarRemove(color: .clear)
//        self.dismissAnyAlertControllerIfPresent()
        removeNoInternetToast()
        print("11111 Cellular")
      case .wifi:
        
        print("11111 Wifi")
        statusBarRemove(color: .clear)
        removeNoInternetToast()
//        self.dismissAnyAlertControllerIfPresent()
        
      case .unavailable:
        
        print("11111 Internet Unavailable")
        statusBarColorChange(color: UIColor.statusBarNoInternetColor)
        showNoInternetToast()
//        if(self.presentedViewController == nil)
//        {
//            self.popupAlert(title: Constants.noInternetTitle, message: Constants.noInternetMessage, actionTitles: [Constants.gotIt], actions:[{action1 in
//                self.dismissAnyAlertControllerIfPresent()
//
//
//            },{action2 in
//
//            }, nil])
//        }
        
      default:
        break
      }
    }
    
    func showNoInternetToast() {
//        ToastManager.shared.style.backgroundColor = .white
//        ToastManager.shared.style.messageColor = .black
//        ToastManager.shared.style.imageSize = CGSize(size: 32)
//        ToastManager.shared.style.messageFont = UIFont.cruiseToastBoldFont()
//        ToastManager.shared.style.cornerRadius = 25
//        ToastManager.shared.style.displayShadow = true
//        ToastManager.shared.style.horizontalPadding = 9.0
//        ToastManager.shared.style.verticalPadding = 9.0
//        ToastManager.shared.style.shadowColor = .lightGray
//        ToastManager.shared.style.shadowOpacity = 0.5
//        ToastManager.shared.style.shadowRadius = 1
//        ToastManager.shared.style.shadowOffset = CGSize(size: 1)
//        self.view.makeToast("No internet connection    ", duration: 10.0, position: .top, title: nil, image: UIImage(named: "noInternetIcon"))
        showToast(from: self.view, message: "No internet connection", topOffset: 0, icon: "noInternetIcon")
    }
    
    func removeNoInternetToast() {
//        self.view.hideAllToasts()
        hideToast(from: self.view)
    }
    
    func statusBarRemove(color:UIColor){
        
        if #available(iOS 13.0, *) {
            
            if let foundView = UIApplication.shared.keyWindow?.viewWithTag(100) {
                foundView.removeFromSuperview()
            }

        } else {

                let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
                statusBar?.backgroundColor = color

            }
        }
    
    func statusBarColorChange(color:UIColor){
        if #available(iOS 13.0, *) {
            
            let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
            statusBar.backgroundColor = color
            statusBar.tag = 100
            UIApplication.shared.keyWindow?.addSubview(statusBar)
        } else {
            let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
            statusBar?.backgroundColor = color
        }
    }
    
    func showLoadingIndicator() {
        DispatchQueue.main.async {
            self.view.showLoadingIndicator()
        }
    }
    
    func hideLoadingIndicator() {
        DispatchQueue.main.async {
            self.view.hideLoadingIndicator()
        }
    }
    
    func showNavigationLoadingIndicator() {
        self.navigationController?.view.showLoadingIndicator()
    }
    
    func hideNavigationLoadingIndicator() {
        self.navigationController?.view.hideLoadingIndicator()
    }
}

extension UIApplication {
    class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        
        return controller
    }
    
    static var topSafeAreaHeight: CGFloat {
        
        var topSafeAreaHeight: CGFloat = 0
        if UIApplication.shared.windows.count > 0 {
            let window = UIApplication.shared.windows[0]
            let safeFrame = window.safeAreaLayoutGuide.layoutFrame
            topSafeAreaHeight = safeFrame.minY
        }
        return topSafeAreaHeight
    }
    
    static var bottomSafeAreaHeight: CGFloat {
        var bottomSafeAreaHeight: CGFloat = 0
        if UIApplication.shared.windows.count > 0 {
            let window = UIApplication.shared.windows[0]
            let safeFrame = window.safeAreaLayoutGuide.layoutFrame
            bottomSafeAreaHeight = window.frame.height - safeFrame.maxY
        }
        return bottomSafeAreaHeight
    }

}

extension UIButton{
    @discardableResult
    func applyGradient(colors: [CGColor], withShadow: Bool = true) -> CAGradientLayer {

            self.backgroundColor = nil
            self.layoutIfNeeded()
            let gradientLayer = CAGradientLayer()
            gradientLayer.colors = colors
            gradientLayer.startPoint = CGPoint(x: 0, y: 0)
            gradientLayer.endPoint = CGPoint(x: 1, y: 0)
            gradientLayer.frame = self.bounds
            gradientLayer.cornerRadius = self.frame.height/2

        if withShadow {
            gradientLayer.shadowColor = UIColor.darkGray.cgColor
            gradientLayer.shadowOffset = CGSize(width: 0, height: 4)
            gradientLayer.shadowRadius = 7.0
            gradientLayer.shadowOpacity = 0.3
        }
            gradientLayer.masksToBounds = false
        gradientLayer.name = Constants.gradientName
        
            self.layer.insertSublayer(gradientLayer, at: 0)
        return gradientLayer
    }
    func removeGradient(){
        if let layers = self.layer.sublayers {
            
            for (index, layer) in layers.enumerated() {
                
                if layer.name == Constants.gradientName {
                    
                    self.layer.sublayers?[index].removeFromSuperlayer()
                    break
                }
            }
        }
    }
    
    func moveImageLeftTextCenter(image : UIImage, imagePadding: CGFloat, renderingMode: UIImage.RenderingMode, alignment: ContentHorizontalAlignment){
            self.setImage(image.withRenderingMode(renderingMode), for: .normal)
            guard let imageViewWidth = self.imageView?.frame.width else{return}
            guard let titleLabelWidth = self.titleLabel?.intrinsicContentSize.width else{return}
            self.contentHorizontalAlignment = .left
        
          
            let imageLeft = imagePadding - imageViewWidth / 2
           
            if (alignment == .center){
                let titleLeft =  (bounds.width - titleLabelWidth) / 2 - imageViewWidth// imageViewWidth - 20//
                imageEdgeInsets = UIEdgeInsets(top: 0.0, left: imageLeft, bottom: 0.0, right: 0.0)
                titleEdgeInsets = UIEdgeInsets(top: 0.0, left: titleLeft , bottom: 0.0, right: 0.0)
            }else{
                let titleLeft =  imageViewWidth - 20
                imageEdgeInsets = UIEdgeInsets(top: 0.0, left: imageLeft, bottom: 0.0, right: 0.0)
                titleEdgeInsets = UIEdgeInsets(top: 0.0, left: titleLeft , bottom: 0.0, right: 0.0)
            }
           
        }
    
    func centerVertically(withPadding: CGFloat) {
        guard let imageSize = self.imageView?.frame.size, let titleSize = self.titleLabel?.frame.size else {
            return
        }
        let totalHeight = (imageSize.height + titleSize.height + withPadding)
        self.imageEdgeInsets = UIEdgeInsets(top: -(totalHeight - imageSize.height), left: 0, bottom: 0, right: -titleSize.width)
        self.titleEdgeInsets = UIEdgeInsets(top: 0, left: -imageSize.width, bottom: -(totalHeight - titleSize.height), right: 0)
    }
    
    func topRightAligned(withPadding: CGFloat) {
        guard let imageSize = self.imageView?.frame.size, let titleSize = self.titleLabel?.frame.size else {
            return
        }
//        let totalHeight = (imageSize.height + titleSize.height + withPadding)
        imageEdgeInsets = UIEdgeInsets(top: -imageSize.height, left: (bounds.width - imageSize.width - withPadding), bottom: 0, right: 0)
        titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: (imageView?.frame.width)!)
        
//        self.imageEdgeInsets = UIEdgeInsets(top: -(totalHeight - imageSize.height), left: 100, bottom: 0, right: -100)
//        self.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: -(totalHeight - titleSize.height), right: 0)
    }
}

extension UITapGestureRecognizer {

   func didTapAttributedTextInLabel(label: UILabel, inRange targetRange: NSRange) -> Bool {
       guard let attributedText = label.attributedText else { return false }

       let mutableStr = NSMutableAttributedString.init(attributedString: attributedText)
       mutableStr.addAttributes([NSAttributedString.Key.font : label.font!], range: NSRange.init(location: 0, length: attributedText.length))

       // Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
       let layoutManager = NSLayoutManager()
       let textContainer = NSTextContainer(size: CGSize.zero)
       let textStorage = NSTextStorage(attributedString: mutableStr)

       // Configure layoutManager and textStorage
       layoutManager.addTextContainer(textContainer)
       textStorage.addLayoutManager(layoutManager)

       // Configure textContainer
       textContainer.lineFragmentPadding = 0.0
       textContainer.lineBreakMode = label.lineBreakMode
       textContainer.maximumNumberOfLines = label.numberOfLines
       let labelSize = label.bounds.size
       textContainer.size = labelSize

       // Find the tapped character location and compare it to the specified range
       let locationOfTouchInLabel = self.location(in: label)
       let textBoundingBox = layoutManager.usedRect(for: textContainer)
       let textContainerOffset = CGPoint(x: (labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x,
                                         y: (labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y);
       let locationOfTouchInTextContainer = CGPoint(x: locationOfTouchInLabel.x - textContainerOffset.x,
                                                    y: locationOfTouchInLabel.y - textContainerOffset.y);
       let indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
       return NSLocationInRange(indexOfCharacter, targetRange)
   }

}

extension UIPanGestureRecognizer {

    enum GestureDirection {
        case Up
        case Down
        case Left
        case Right
    }

    /// Get current vertical direction
    ///
    /// - Parameter target: view target
    /// - Returns: current direction
    func verticalDirection(target: UIView) -> GestureDirection {
        return self.velocity(in: target).y > 0 ? .Down : .Up
    }

    /// Get current horizontal direction
    ///
    /// - Parameter target: view target
    /// - Returns: current direction
    func horizontalDirection(target: UIView) -> GestureDirection {
        return self.velocity(in: target).x > 0 ? .Right : .Left
    }

    /// Get a tuple for current horizontal/vertical direction
    ///
    /// - Parameter target: view target
    /// - Returns: current direction
    func versus(target: UIView) -> (horizontal: GestureDirection, vertical: GestureDirection) {
        return (self.horizontalDirection(target: target), self.verticalDirection(target: target))
    }

}

extension Double {
//    func toString() -> String {
//        return String(format: "%f",self)
//    }
    
    func toString() -> String {
        return self.description
    }

    func toString(numberOfDecimal: Int) -> String {
        let format = "%.\(numberOfDecimal)f"
        return String(format: format, self)
    }

    var stringWithoutZeroFraction: String {
        return truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", self) : String(self)
    }
}

extension String
{
    /// EZSE: Converts String to Double
    public func toDouble() -> Double?
    {
        return Double(self)
    }
}

extension Float {
    var clean: String {
        return String(format: "%.2f", self)
       //return self.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.2f", self) : String(format: "%.2f", self)
    }
}

extension TimeInterval {
    private var milliseconds: Int {
        return Int((truncatingRemainder(dividingBy: 1)) * 1000)
    }

    private var seconds: Int {
        return Int(self) % 60
    }

    private var minutes: Int {
        return (Int(self) / 60 ) % 60
    }

    private var hours: Int {
        return Int(self) / 3600
    }

    var stringTime: String {
        
        /*if hours != 0{
            return "\(hours)"
        }
        else if minutes != 0 {
            return "\(minutes)"
        } else {
            return "\(seconds)"
        }*/
        
        return "\(hours*60+minutes)"
        /*if hours != 0 {
            return "\(hours)h \(minutes)min \(seconds)s"
        } else if minutes != 0 {
            return "\(minutes) m \(seconds)s"
        } else if milliseconds != 0 {
            return "\(seconds)s \(milliseconds)ms"
        } else {
            return "\(seconds) sec"
        }*/
    }
    
    var stringTimeUnit: String {
        
        return "min"
        /*if hours != 0 {
            return "hr"
        }
         else if minutes != 0 {
            return "min"
        } else {
            return "sec"
        }*/
        
        /*if hours != 0 {
            return "\(hours)h \(minutes)min \(seconds)s"
        } else if minutes != 0 {
            return "\(minutes) m \(seconds)s"
        } else if milliseconds != 0 {
            return "\(seconds)s \(milliseconds)ms"
        } else {
            return "\(seconds) sec"
        }*/
    }
}

extension MapView {
    func removeSymbol(id: String) {
        
        do {
            try self.mapboxMap.style.removeLayer(withId: "Symbol-\(id)")
            try self.mapboxMap.style.removeSource(withId: id)
        } catch {
            print("Failed to remove symbol: \(id)")
        }
    }
    
    func addSymbol(id: String, name: String, coordinate: CLLocationCoordinate2D) {
        
        do {
        try self.mapboxMap.style.addImage(UIImage(named: name)!, id: name,stretchX: [],stretchY: [])
        
        let iconSize = Exp(.interpolate){
            Exp(.linear)
            Exp(.zoom)
            0
            0
            9
            0.0
            13
            0.7
        }
        
        // Create a GeoJSON data source.
        var geoJSONSource = GeoJSONSource()
        geoJSONSource.data = .geometry(.point(Point(coordinate)))

        var symbolLayer = SymbolLayer(id: "Symbol-\(id)")
        symbolLayer.source = id
        symbolLayer.iconSize = .expression(iconSize)
        symbolLayer.iconImage = .constant(ResolvedImage.name(name))

        /// the source id should be the same as the `symbolLayer.source`
        try self.mapboxMap.style.addSource(geoJSONSource, id: id)
        // rc4 - #307 remove _
        try self.mapboxMap.style.addPersistentLayer(with: symbolLayer.jsonObject(), layerPosition: .above(Constants.Layer.illegalparkingcamera))
        SwiftyBeaver.debug("Symbol layer \(id) added")
            
        } catch {
            SwiftyBeaver.error("Failed to add symbol layer \(id)")
        }
    }
}

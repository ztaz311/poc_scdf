//
//  Utils.swift
//  Breeze
//
//  Created by Zhou Hao on 8/1/21.
//

import Foundation
import CoreLocation
import MapboxMaps
import MapboxCoreMaps
import MapKit
import MapboxNavigation
import MapboxDirections
import MapboxCoreNavigation
import Turf
import Instabug
import AWSAppSync
import FirebaseRemoteConfig
import CommonCrypto
import RxSwift
import AVFoundation
import SwiftyBeaver
import FirebaseCrashlytics
import Firebase

private let tileSize: Double = 512.0
private let M2PI = Double.pi * 2
private let MPI2 = Double.pi / 2
private let DEG2RAD = Double.pi / 180.0
private let EARTH_RADIUS_M = 6378137.0
private let LATITUDE_MAX: Double = 85.051128779806604
private let AngularFieldOfView: CLLocationDegrees = 30
private let MIN_ZOOM = 0.0
private let MAX_ZOOM = 25.5

struct SourceAddressSelection {
    static let AddressSearch = "S"
    static let EasyBreezies = "B"
    static let SearchHistory = "H"
    static let Calendar = "C"
    static let RecommendedAddress = "R"
}

struct TrafficIncidentCategories {
    static let VehicleBreakDown = "Vehicle breakdown"
    static let RoadWork =      "Road Work"
    static let Obstacle =      "Obstacle"
    static let Accident =      "Accident"
    static let RoadBlock =     "Road Block"
    static let Miscellaneous = "Miscellaneous"
    static let UnattendVehicle = "Unattended Vehicle"
    static let FlashFlood = "Flash Flood"
    static let MajorAccident = "Major Accident"
    static let RoadClosure = "Road Closure"
    static let EvCharger = "ev-charger-iconV2"
    static let HeavyTraffic = "Heavy Traffic"
    static let TreePruning = "Tree Pruning"
    static let YellowDengueZone = "Yellow Dengue Zone"
    static let RedDengueZone = "Red Dengue Zone"
    
    // MARK:- Traffic Incident layer Identifiers
    static let TrafficIncident_GeoJsonSource = "Incident_Source"
    static let TrafficIncident_Identifier_SymbolLayer = "Incident_Symbol"
}

struct AmenitiesLayer {    
    static let GeoJsonSource = "Amenities_Source"
    static let Identifier_SymbolLayer = "Amenities_Symbol"
    static let innerZoneSource = "InnerZone_Source"
    static let innerZoneLayer = "InnerZone_Layer"
    static let outerZoneSource = "OuterZone_Source"
    static let outerZoneLayer = "OuterZone_Layer"
    static let clusteredCircleLayer = "clustered-circle-layer"
    static let unclusteredCircleLayer = "unclustered-circle-layer"
}

struct DemoIncidentLayer {
    
    // MARK:- Demo Incident layer Identifiers
    static let DemoIncident_GeoJsonSource = "DemoIncident_Source"
    static let DemoIncident_Identifier_SymbolLayer = "DemoIncident_Symbol"
    
    static let DemoIncident2_GeoJsonSource = "DemoIncident2_Source"
    static let DemoIncident2_Identifier_SymbolLayer = "DemoIncident2_Symbol"
}

struct EBValues {
    
    static let MaxEBLimit = 100
}

struct UserPoolAttributes{
    
    static let ONBOARDING_STATE = "onboarding_state"
    static let FULLNAME = "fullname"
}

struct UserOnboardingState{
    static let ONBOARDING_INIT = "ONBOARDING_INIT"
    static let ONBOARDING_STEP_1 = "ONBOARDING_STEP_1"
    static let ONBOARDING_COMPLETED = "ONBOARDING_COMPLETED"
}

struct RNScreeNames{
    static let USER_PREFERENCE = "UserPreference"
    static let SERACH_LOCATION = "SearchLocation"
    static let BOTTOM_SHEET = "Landing"
    static let PREFERENCE_SETTING = "PreferenceSettings"
    static let FIRST_APP_TUTORIAL = "AppTutorial"
    static let PROGRESS_TRACKING = "WebviewPanel"
    static let OBUModalPair_Install = "OBUModalPairInstall"
    static let CarPark_All_Avaiable = "OBUCarparkAvailability"
    static let Travel_Time = "OBUTravelTime"
    static let CARPARK_LIST = "CarparkList"
}

struct NativeScreenNames {
    static let MAP_DISPLAY = "MapDisplay"
}

struct ERPValues{
    
    static let isSatWeekend = "Saturday"
    static let isWeekday = "Weekdays"
    static let isSunWeekend = "Sunday"
    
    // MARK:- ERP No Cost layer Identifiers
    static let ERPNoCost_Identifier_GeoJsonSource = "ERP_NoCost_GeoJsonSource"
    static let ERPNoCostCarPlay_Identifier_GeoJsonSource = "ERPCarPlay_NoCost_GeoJsonSource"
    static let ERPNoCost_Identifier_SymbolLayer = "ERP_NoCost_Symbol"
    static let ERPNoCost_Identifier_LineLayer = "ERP_NoCost_LineLayer"
    static let ERPNoCostImageIdentifier = "ERP_NoCostImage"
    
    // MARK:- ERP Cost layer Identifiers
    static let ERPCost_Identifier_GeoJsonSource = "ERP_Cost_GeoJsonSource"
    static let ERPCostCarPlay_Identifier_GeoJsonSource = "ERPCarPlay_Cost_GeoJsonSource"
    static let ERPCost_Identifier_SymbolLayer = "ERP_Cost_Symbol"
    static let ERPCost_Identifier_LineLayer = "ERP_Cost_LineLayer"
    static let ERPCostImageIdentifier = "ERP_CostImage"
}

struct FSCLayer{
    
    static let FAST_GEOJSON_SOURCE = "FAST_GEOJSON_SOURCE"
    static let FAST_SYMBOL_LAYER = "FAST_SYMBOL_LAYER"
    
    static let SHORT_GEOJSON_SOURCE = "SHORT_GEOJSON_SOURCE"
    static let SHORT_SYMBOL_LAYER = "SHORT_SYMBOL_LAYER"
    
    static let CHEAPEST_GEOJSON_SOURCE = "CHEAPEST_GEOJSON_SOURCE"
    static let CHEAPEST_SYMBOL_LAYER = "CHEAPEST_SYMBOL_LAYER"
}

public typealias CarPlayUserInfo = [String: Any?]

var audioPlayer:AVAudioPlayer?

func showToast(from view: UIView, message: String, topOffset: CGFloat = 0.0, icon: String = "cruiseIcon", messageColor: UIColor = .black, duration: TimeInterval = 10.0, fromCarPlay: Bool = false ) {
    let font = fromCarPlay ? UIFont.cruiseCarPlayToastRegularFont() : UIFont.cruiseToastRegularFont()
    showToast(from: view, message: message, topOffset: topOffset, image: UIImage(named: icon), messageColor: messageColor, font: font, duration: duration, fromCarPlay: fromCarPlay)
}

func showToast(from view: UIView, message: String, topOffset: CGFloat = 0.0, image: UIImage? = nil, messageColor: UIColor = .black, font: UIFont, duration: TimeInterval = 10.0 , fromCarPlay: Bool = false ) {
    hideToast(from: view)
    var style = ToastStyle()
    style.topOffset = topOffset
    style.backgroundColor = .white
    style.messageColor = messageColor
    if view.traitCollection.userInterfaceStyle == .dark {
        style.backgroundColor =  UIColor.rgba(r: 57, g: 63, b: 88, a: 1)
        style.messageColor = .white
    }
    style.imageSize = fromCarPlay ? CGSize(size: 24) : CGSize(size: 32)
    style.messageFont = font
    style.cornerRadius = fromCarPlay ? 17 : 25
    style.displayShadow = true
    style.horizontalPadding = 9.0
    style.verticalPadding = fromCarPlay ? 5.0 : 9.0
    if (message.count >= 40){
        style.verticalPadding = 22.0
    }
    
    style.shadowColor = .lightGray
    style.shadowOpacity = 0.5
    style.shadowRadius = 1
    style.shadowOffset = CGSize(size: 1)

    // TODO: using the global style for the time being since it's always using offset in global style
    //ToastManager.shared.style = style
    DispatchQueue.main.async {
        view.makeToast("\(message)     ", duration: duration, position: .top, title: nil, image: image, style: style)
    }
}

func hideToast(from view: UIView) {
    view.hideAllToasts()
}

func getTopOffset() -> CGFloat {
    return UIApplication.shared.windows.first?.safeAreaInsets.top ?? 40
}

// Return first letter of first two words seperated by white space
// If only one word, return first two letters instead
func getAbbreviation(name: String) -> String {
    var setName = name
    let names = name.components(separatedBy: CharacterSet.whitespacesAndNewlines)
    let array = names.filter { !$0.isEmpty }
    if array.count >= 2 {
        setName = "\(array[0].first!)\(array[1].first!)"
    } else {
        setName = String(array[0].prefix(2))
    }
    return setName.uppercased()
}

func calculateDistance(at coordinate: CLLocationCoordinate2D, from currentCoordinate: CLLocationCoordinate2D) -> Double {
    let coordinate0 = CLLocation(latitude: currentCoordinate.latitude, longitude: currentCoordinate.longitude)
    let coordinate1 = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
    return coordinate0.distance(from: coordinate1)
}

func createMonitorRectangle(at location: CLLocation, in map: MapView) -> CGRect {
    let coordinate = location.coordinate
    let region = MKCoordinateRegion(center: coordinate, latitudinalMeters: 150, longitudinalMeters: 150)
    let northEast = CLLocationCoordinate2D(latitude: region.center.latitude + region.span.latitudeDelta / 2,
                                           longitude: region.center.longitude + region.span.longitudeDelta / 2)
    
    let southWest = CLLocationCoordinate2D(latitude: region.center.latitude - region.span.latitudeDelta / 2,
                                           longitude: region.center.longitude - region.span.longitudeDelta / 2)
    
    let bounds = CoordinateBounds(southwest: southWest, northeast: northEast)
    
    //beta.9 change
    return map.mapboxMap.rect(for: bounds)
    //return map.rect(for: bounds)
    //return map.convert(bounds, toRectTo: map)
}


func getAddressFromLocation(location: CLLocation, completion:@escaping(String) ->Void) {
    //Select Address API
    let geocoder = CLGeocoder()
    var addressName = "No destination address"
    geocoder.reverseGeocodeLocation(location,
                                    completionHandler: { (placemarks, error) in
                                        if error == nil {
                                            let locationDetails = placemarks?[0]
                                            addressName =  (locationDetails?.name) ?? addressName
                                     
                                            print(addressName)
                                        }
                                        completion(addressName)
                                })
    
}

func getPinLocationDetails(location: CLLocation, completion:@escaping(CLPlacemark?) ->Void) {
    // Select Address API
    let geocoder = CLGeocoder()
    geocoder.reverseGeocodeLocation(location,
                                    completionHandler: { (placemarks, error) in
                                        if error == nil {
                                            let placemark = placemarks?.first
                                            completion(placemark)
                                        } else {
                                            completion(nil)
                                        }
                                })
    
}

//func calculateBoundingPoints(
//    carparkRadius: Double,
//    centreLat: Double,
//    centreLong: Double)
//{
//    var bearingList =
//       [ 0.0,
//        45.0,
//        90.0,
//        135.0,
//        180.0,
//        -90.0,
//        -45.0,
//        -135.0]
//    
//let bounds = BoundingBox(from: [CLLocationCoordinate2D(latitude: centreLat, longitude: centreLong)])
//    
//    var destinationPoint = Turf.Point(CLLocationCoordinate2D(latitude: centreLat, longitude: centreLong)
//    var distance = carparkRadius + 50
//    var listOfPoints = arrayListOf<Point>()
//
//                                      
//       
//                                      
//    for bearing in bearingList {
//        var point = TurfMeasurement.destination(
//            destinationPoint,
//            distance,
//            bearing,
//            TurfConstants.UNIT_METRES
//        )
//        listOfPoints.add(point)
//    }
//    //return listOfPoints
//}

func checkERPPublicHoliday(publicHoliday:[Publicholiday],selectedDate:Date? = nil ) -> Bool {
    
    let currentDate = selectedDate == nil ? DateUtils.shared.getTimeDisplay(date: Date()) : DateUtils.shared.getTimeDisplay(date: selectedDate!)
    
    var isSameDay = false
    for holiday in publicHoliday {
        
        if(currentDate == holiday.date)
        {
            let startTime = holiday.startTime
            let endTime = holiday.endTime
            //Checking current time is in between start and endtime of erp
            let isCurrentTimeBetwen = (selectedDate ?? Date()).currentTimeisBetween(startTime: startTime!, endTime: endTime!)
            if(isCurrentTimeBetwen == true)
            {
                isSameDay = true
                break;
            }
        }
    }
    
    return isSameDay
    
}

/*func checkIfSunday() -> Bool{
    
    let currentDate = Date()
    
    let isWeekend = Date().isWeekend(date: currentDate)
    
    if(isWeekend == ERPValues.isSunWeekend)
    {
        return true
    }
    
    return false
    
}*/

/*
    Here is the format of bound and iconType for ERP
    
    bound = Increase i.e the next ERP charge will become high comapre to current one
    bound = Decrease i.e the next ERP charge will become less comapre to current one
    
    output1 = (grey, Increase) i.e to show this icon if ERP gonna start and the price will change within 30 minutes
    output2 = (grey,"") i.e to show this icon if ERP gonna start but the price is not chhanging within 30 minutes
    
    output3 = (blue, Increase)
    output4 = (blue, decrease)
    
 
*/
func returnERPBoundValueWithIconType(erpRates:[Rates],selectedTime:Date? = nil,selectedDay:Date? = nil) -> (bound:String, iconType:String,starteAndEndTimeCombine:String,currentPrice:String,upcomingPrice:String,selectedTimeERP:String)?{
    
    
    for i in 0..<erpRates.count{
        
        /// Getting start and end time of erp from erpRates array
        let startTime = erpRates[i].starttime
        let endTime = erpRates[i].endtime
        
        /// Getting current time from Date and converting back to date from string using date formatter
        let currentTime = Date()
        
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "HH:mm"
        let dateFormat = "HH:mm"
        let strCurTime = DateUtils.shared.getTimeDisplay(dateFormat: dateFormat, date: currentTime)
        let dateCurrentTime = DateUtils.shared.getDate(dateFormat: dateFormat, dateString: strCurTime)
        
        /// converting erpStartTime to Date
//        let time2formatter = DateFormatter()
//        time2formatter.dateFormat = "HH:mm"
        
        let refresh30MinDate = selectedTime == nil ? currentTime : selectedTime
        
        var isCurrentTimeBetwen = false
        if let selectedTime = selectedTime {
            
            /// check current time is in between two times i.e startTime and endTime
             isCurrentTimeBetwen = selectedTime.currentTimeisBetweenFromSelectedDate(startTime: startTime!, endTime: endTime!, selectedTime: selectedTime)
        }
        else{
            
            /// check current time is in between two times i.e startTime and endTime
             isCurrentTimeBetwen = Date().currentTimeisBetween(startTime: startTime!, endTime: endTime!)
            
        }
        let isWeekDayCheck = selectedDay == nil ? Date() : selectedDay
        if(isCurrentTimeBetwen == true)
        {
            /// return tuple with erpBound as Increase, and erpIcon type as grey, with current amount as $0.00 and next amount as the actual ERP price
            if(erpRates[i].getChargeAmount() > 0){
                
                if(i+1 != erpRates.count)
                {
                    if((erpRates[i].daytype == ERPValues.isWeekday && isWeekDayCheck!.isWeekend(date: isWeekDayCheck!) == ERPValues.isWeekday) || (erpRates[i].daytype != ERPValues.isWeekday && erpRates[i].daytype == ERPValues.isSatWeekend && isWeekDayCheck!.isWeekend(date: isWeekDayCheck!) == ERPValues.isSatWeekend))
                    {
                        /// checking current displayed amount is less than the upcoming ERP
                        let combineStartAndEndTimeOfERP = erpRates[i+1].getTimeDisplayed()
                        if(erpRates[i].getChargeAmount() < erpRates[i+1].getChargeAmount()){
                            
                            if let erpStartTime = DateUtils.shared.getDate(dateFormat: dateFormat, dateString: erpRates[i+1].starttime ?? "") {
                                /// Getting difference between two times i.e from currentTime to erpStartTime
                                let difference = Calendar.current.dateComponents([.minute], from: erpStartTime, to: selectedTime == nil ? dateCurrentTime! : selectedTime!)
                                
                                ///if minutes <= 30, if this erp next time is under 30 min
                                if(returnTimeDifferenceIsWithIn30Min(dateComponents: difference)){
                                    return (Values.INCREASE,"blue",combineStartAndEndTimeOfERP,"$\(erpRates[i].getChargeAmount().clean)","$\(erpRates[i+1].getChargeAmount().clean)",erpRates[i].getTimeDisplayed())
                                }
                                else{
                                    
                                    if let refresh30MinDate = refresh30MinDate{
                                        start30MinRefreshInterval(nextERPStartTime: erpStartTime, currentTime: refresh30MinDate)
                                    }
                                    return (Values.ACTIVE_NO_ARROW,"blue",combineStartAndEndTimeOfERP,"$\(erpRates[i].getChargeAmount().clean)","$\(erpRates[i+1].getChargeAmount().clean)",erpRates[i].getTimeDisplayed())
                                }
                            }
                        }
                        else {
                            
                            if let erpStartTime = DateUtils.shared.getDate(dateFormat: dateFormat, dateString: erpRates[i+1].starttime ?? "") {
                                /// Getting difference between two times i.e from currentTime to erpStartTime
                                let difference = Calendar.current.dateComponents([.minute], from: erpStartTime, to: selectedTime == nil ? dateCurrentTime! : selectedTime!)
                                ///if minutes <= 30, if this erp next time is under 30 min
                                
                                if(returnTimeDifferenceIsWithIn30Min(dateComponents: difference)){
                                    return (Values.DECREASE,"blue",combineStartAndEndTimeOfERP,"$\(erpRates[i].getChargeAmount().clean)","$\(erpRates[i+1].getChargeAmount().clean)",erpRates[i].getTimeDisplayed())
                                }
                                else{
                                    
                                    if let refresh30MinDate = refresh30MinDate{
                                    
                                        start30MinRefreshInterval(nextERPStartTime: erpStartTime, currentTime: refresh30MinDate)
                                    }
                                    return (Values.ACTIVE_NO_ARROW,"blue",combineStartAndEndTimeOfERP,"$\(erpRates[i].getChargeAmount().clean)","$\(erpRates[i+1].getChargeAmount().clean)",erpRates[i].getTimeDisplayed())
                                }
                            }
                        }
                    
                    }
                    
                    
                }
                    
            }
        }
        else{
            
            
                
        }
        
            
            //print(difference.minute)
        
    }
    
    return nil
}

func returnInActiveERPBoundValue(erpRates: [Rates],
                                 selectedTime: Date? = nil,
                                 selectedDay:Date? = nil) -> (bound:String, iconType:String,starteAndEndTimeCombine:String,currentPrice:String,upcomingPrice:String,selectedTimeERP:String)? {
    

    for i in 0..<erpRates.count{
        
        /// Getting start and end time of erp from erpRates array
        guard let startTime = erpRates[i].starttime else { return nil}
        
        /// Getting current time from Date and converting back to date from string using date formatter
        let currentTime = Date()
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "HH:mm"
        let dateFormat = "HH:mm"
        let strCurTime = DateUtils.shared.getTimeDisplay(dateFormat: dateFormat, date: currentTime)
        
        guard let erpStartTime = DateUtils.shared.getDate(dateFormat: dateFormat, dateString: startTime),
              let dateCurrentTime = DateUtils.shared.getDate(dateFormat: dateFormat, dateString: strCurTime) else { return nil }
        
        /// Getting difference between two times i.e from currentTime to erpStartTime
        let difference = Calendar.current.dateComponents([.minute], from: erpStartTime, to: selectedTime ?? dateCurrentTime)
        
        let isWeekDayCheck = selectedDay ?? Date()
        /// checking if difference time is null
        if let minute = difference.minute {
            
            ///if minutes <= 30, that means this is the first erp start time of this zone id with the corresponding erpID
            if(abs(minute) <= 30),
              let chargeamount = erpRates[i].chargeamount,
              let endtime = erpRates[i].endtime {
                if(chargeamount > 0) {
                    
                    if((erpRates[i].daytype == ERPValues.isWeekday && isWeekDayCheck.isWeekend(date: isWeekDayCheck) == ERPValues.isWeekday) || (erpRates[i].daytype != ERPValues.isWeekday && erpRates[i].daytype == ERPValues.isSatWeekend && isWeekDayCheck.isWeekend(date: isWeekDayCheck) == ERPValues.isSatWeekend))
                    {
                        let combineStartAndEndTimeOfERP = erpRates[i].getTimeDisplayed()
                        return (Values.INCREASE_GREY,"grey",combineStartAndEndTimeOfERP,"$\(0.00)","$\(chargeamount.clean)",erpRates[i].getTimeDisplayed())
                    }
                    else{
                        
                        let combineStartAndEndTimeOfERP = ""
                        return (Values.IN_ACTIVE_NO_ARROW,"grey",combineStartAndEndTimeOfERP,"$\(0.00)","",erpRates[i].getTimeDisplayed())
                    }
                    
                }
                else{
                    
                    let combineStartAndEndTimeOfERP = ""
                    return (Values.IN_ACTIVE_NO_ARROW,"grey",combineStartAndEndTimeOfERP,"$\(0.00)","",erpRates[i].getTimeDisplayed())
                }
                
            }
        }
    }
    
    return nil
    
}


func getERPChargeAmount(erpRates:[Rates]) -> String{
    
    //Default charge amount will be 0
    var chargeAmount = "0"
    
    for i in 0..<erpRates.count{
        
        let startTime = erpRates[i].starttime
        let endTime = erpRates[i].endtime
        
        //Checking current time is in between start and endtime of erp
        let isCurrentTimeBetwen = Date().currentTimeisBetween(startTime: startTime!, endTime: endTime!)
        if(isCurrentTimeBetwen == true)
        {
            //If yes, then will check with Charge amount is > 0, then break the loop to return the charge amount
            if(erpRates[i].getChargeAmount() > 0)
            {
                if((erpRates[i].daytype == ERPValues.isWeekday && Date().isWeekend(date: Date()) == ERPValues.isWeekday) || (erpRates[i].daytype != ERPValues.isWeekday && erpRates[i].daytype == ERPValues.isSatWeekend && Date().isWeekend(date: Date()) == ERPValues.isSatWeekend))
                {
                    chargeAmount = "$\(erpRates[i].getChargeAmount().clean)"
                    break
                }
                else{
                    
                }
               
            }
            
        }
        else
        {
            //If currenttime is not in between start and end time of erp, then this else will execute until finishes all erp rates of the each erp zoned id
            if(i+1 == erpRates.count)
            {
                chargeAmount = "0"
            }
        }
        
    }
    
    return chargeAmount
    
}

func isHoliday(_ selectedDate: Date) -> Bool {
    var retValue = false
    if let erpData = DataCenter.shared.getERPJsonData(){
        let publicHolidays = erpData.publicholiday
        
        let tyepOfDay = selectedDate.weekDateRPM    //FIXME: Refactor isWeekend function
        
        retValue = (checkERPPublicHoliday(publicHoliday: publicHolidays!, selectedDate: selectedDate) || tyepOfDay == ERPValues.isSunWeekend)
    }
    
    return retValue
}

func getERPChargeAmountWithSelectedDate(erpRates:[Rates],selectedTime:Date,selectedDay:Date) -> String{
    
    //Default charge amount will be 0
    var chargeAmount = "0"
    let isPublicHoliday = isHoliday(selectedDay)
    if !isPublicHoliday {
        for i in 0..<erpRates.count{
            let startTime = erpRates[i].starttime
            let endTime = erpRates[i].endtime
            
            //Checking current time is in between start and endtime of erp
            let isCurrentTimeBetwen = selectedTime.currentTimeisBetweenFromSelectedDate(startTime: startTime!, endTime: endTime!,selectedTime: selectedTime)
            if(isCurrentTimeBetwen == true)
            {
                //If yes, then will check with Charge amount is > 0, then break the loop to return the charge amount
                if(erpRates[i].getChargeAmount() > 0)
                {
                    if((erpRates[i].daytype == ERPValues.isWeekday && selectedDay.isWeekend(date: selectedDay) == ERPValues.isWeekday) || (erpRates[i].daytype != ERPValues.isWeekday && erpRates[i].daytype == ERPValues.isSatWeekend && selectedDay.isWeekend(date: selectedDay) == ERPValues.isSatWeekend))
                    {
                        chargeAmount = "$\(erpRates[i].getChargeAmount().clean)"
                        break
                    }
                }
            }
            else
            {
                //If currenttime is not in between start and end time of erp, then this else will execute until finishes all erp rates of the each erp zoned id
                if(i+1 == erpRates.count)
                {
                    chargeAmount = "0"
                }
            }
        }
    }
    
    return chargeAmount
    
}

func saveToJsonFile(result:ERPBaseModel) {
    // Get the url of Persons.json in document directory
        guard let documentDirectoryUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return }
        let fileUrl = documentDirectoryUrl.appendingPathComponent("ERP.json")

        //let personArray =  [["person": ["name": "Dani", "age": "24"]], ["person": ["name": "ray", "age": "70"]]]

        // Transform array into data and save it into file
        do {
            let data = try JSONEncoder().encode(result)
            try data.write(to: fileUrl, options: [.atomic, .completeFileProtection])
        } catch {
            print(error)
        }
}

func retrieveERPFromJsonFile(_ completion: @escaping (Result<ERPBaseModel>) -> ()) {
    // Get the url of ERP.json in document directory
    guard let documentsDirectoryUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else {return completion(Result.failure("No File" as! Error)) }
   
    let fileUrl = documentsDirectoryUrl.appendingPathComponent("ERP.json")
    
    do {
            let data = try Data(contentsOf: fileUrl)
            let erpData = try JSONDecoder().decode(ERPBaseModel.self, from: data)
            completion(Result.success(erpData))
            
        } catch {
            print(error)
            completion(Result.failure(error))
        }
    
}

/*func calculateRoute(from origin: CLLocationCoordinate2D, to destination: CLLocationCoordinate2D, complete: @escaping (RouteResponse?) -> Void) {

    let origin = Waypoint(coordinate: origin, coordinateAccuracy: -1, name: "Start")
    let destination = Waypoint(coordinate: destination, coordinateAccuracy: -1, name: "Finish")
    
    // Specify that the route is intended for automobiles avoiding traffic
    let routeOptions = NavigationRouteOptions(waypoints: [origin, destination], profileIdentifier: .automobileAvoidingTraffic)
    routeOptions.attributeOptions = [.congestionLevel, .expectedTravelTime, .maximumSpeedLimit]
    routeOptions.shapeFormat = .polyline
    routeOptions.distanceMeasurementSystem = .metric
    RouteControllerProactiveReroutingInterval = 30
    
    // Generate the route object and draw it on the map
    Directions.shared.calculate(routeOptions) { (session, result) in
        switch result {
        case .failure(_):
            complete(nil)
        case .success(let response):
            complete(response)
        }
    }
}*/

func pointToPolygon(_ point: Turf.Point) -> Turf.Polygon {
    let coordinate = point.coordinates
    return Polygon(center: coordinate, radius: 5, vertices: 16)
}

func getNearestLine(coordinate: CLLocationCoordinate2D, polygon: Turf.Polygon) -> LineString? {
    var lineString: LineString? = nil
    var min = Double.infinity
    let outer = polygon.outerRing.coordinates
    if outer.count >= 3 {
        for i in 0...outer.count - 1 {
            let first = outer[i]
            let j = (i+1) % outer.count
            let second = outer[j]
            let line = LineString([CLLocationCoordinate2D(latitude: first.latitude, longitude: first.longitude), CLLocationCoordinate2D(latitude: second.latitude, longitude: second.longitude)])
            if let coord = line.closestCoordinate(to: coordinate)?.coordinate {
                let d = coord.distance(to: coordinate)
                if d < min {
                    min = d
                    lineString = line
                }
            }
        }
    } else if outer.count == 2 {
        let first = outer[0]
        let second = outer[1]
        return LineString([CLLocationCoordinate2D(latitude: first.latitude, longitude: first.longitude), CLLocationCoordinate2D(latitude: second.latitude, longitude: second.longitude)])
    }
    return lineString
}

func getTraitCollection() -> UITraitCollection {
    var traitCollection = UITraitCollection(userInterfaceStyle: .light)
    if(Settings.shared.theme == 0){

        if(appDelegate().isDarkMode()){
            
            traitCollection = UITraitCollection(userInterfaceStyle: .dark)
            
        }
    } else {
        traitCollection = UITraitCollection(userInterfaceStyle: UIUserInterfaceStyle(rawValue: Settings.shared.theme) ?? .unspecified)
    }
    return traitCollection
}

func returnERPBoundValueWithIconTypeForLanding(erpRates:[Rates],selectedTime:Date? = nil,selectedDay:Date? = nil) -> (bound:String, iconType:String,starteAndEndTimeCombine:String,currentPrice:String,upcomingPrice:String,selectedTimeERPMapLanding:String)?{
    
    
    for i in 0..<erpRates.count{
        
        /// Getting start and end time of erp from erpRates array
        let startTime = erpRates[i].starttime
        let endTime = erpRates[i].endtime
        
        /// Getting current time from Date and converting back to date from string using date formatter
        let currentTime = Date()
        
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "HH:mm"
        
        let dateFormat = "HH:mm"
        let strCurTime = DateUtils.shared.getTimeDisplay(dateFormat: dateFormat, date: currentTime)
        let dateCurrentTime = DateUtils.shared.getDate(dateFormat: dateFormat, dateString: strCurTime)
        
        /// converting erpStartTime to Date
//        let time2formatter = DateFormatter()
//        time2formatter.dateFormat = "HH:mm"
//        let erpStartTime = time2formatter.date(from: startTime!)!
        
        let refresh30MinDate = selectedTime == nil ? currentTime : selectedTime
        
        var isCurrentTimeBetwen = false
        if let selectedTime = selectedTime {
            /// check current time is in between two times i.e startTime and endTime
            isCurrentTimeBetwen = selectedTime.currentTimeisBetweenFromSelectedDate(startTime: startTime!, endTime: endTime!, selectedTime: selectedTime)
        }
        else{
            /// check current time is in between two times i.e startTime and endTime
            isCurrentTimeBetwen = currentTime.currentTimeisBetween(startTime: startTime!, endTime: endTime!)
            
        }
        let isWeekDayCheck = selectedDay == nil ? currentTime : selectedDay
        if(isCurrentTimeBetwen == true)
        {
            /// return tuple with erpBound as Increase, and erpIcon type as grey, with current amount as $0.00 and next amount as the actual ERP price
            if(erpRates[i].getChargeAmount() > 0){
                
                if(i+1 != erpRates.count)
                {
                    
                    if((erpRates[i].daytype == ERPValues.isWeekday && isWeekDayCheck!.isWeekend(date: isWeekDayCheck!) == ERPValues.isWeekday) || (erpRates[i].daytype != ERPValues.isWeekday && erpRates[i].daytype == ERPValues.isSatWeekend && isWeekDayCheck!.isWeekend(date: isWeekDayCheck!) == ERPValues.isSatWeekend))
                    {
                        /// checking current displayed amount is less than the upcoming ERP
                        
                        let combineStartAndEndTimeOfERP = erpRates[i+1].getTimeDisplayed()
                        if(erpRates[i].getChargeAmount() < erpRates[i+1].getChargeAmount()){
                           
                            if let erpStartTimeNext = DateUtils.shared.getDate(dateFormat: dateFormat, dateString: erpRates[i+1].starttime ?? "") {
                                /// Getting difference between two times i.e from currentTime to erpStartTime
                                let difference = Calendar.current.dateComponents([.minute], from: erpStartTimeNext, to: selectedTime == nil ? dateCurrentTime! : selectedTime!)
                                ///if minutes <= 30, if this erp next time is under 30 min
                                if(returnTimeDifferenceIsWithIn30Min(dateComponents: difference)){
                                    
                                    //print("Time Difference match",difference.minute,erpRates[i])
                                    return (Values.INCREASE,"blue",combineStartAndEndTimeOfERP,"$\(erpRates[i].getChargeAmount().clean)","$\(erpRates[i+1].getChargeAmount().clean)",erpRates[i].getTimeDisplayed())
                                    
                                   
                                } else {
                                    
                                    //print("Time Difference",difference.minute,erpRates[i])
                                    if let refresh30MinDate = refresh30MinDate{
                                        start30MinRefreshInterval(nextERPStartTime: erpStartTimeNext, currentTime: refresh30MinDate)
                                    }
                                    
                                    return (Values.ACTIVE_NO_ARROW,"blue",combineStartAndEndTimeOfERP,"$\(erpRates[i].getChargeAmount().clean)","$\(erpRates[i+1].getChargeAmount().clean)",erpRates[i].getTimeDisplayed())
                                }
                            }
                        }
                        else {
                            
                            if let erpStartTimeNext = DateUtils.shared.getDate(dateFormat: dateFormat, dateString: erpRates[i+1].starttime ?? "") {
                                /// Getting difference between two times i.e from currentTime to erpStartTime
                                let difference = Calendar.current.dateComponents([.minute], from: erpStartTimeNext, to: selectedTime == nil ? dateCurrentTime! : selectedTime!)
                                ///if minutes <= 30, if this erp next time is under 30 min
                                if(returnTimeDifferenceIsWithIn30Min(dateComponents: difference)){
                                    
                                   // print("Time Difference match",difference.minute,erpRates[i])
                                    return (Values.DECREASE,"blue",combineStartAndEndTimeOfERP,"$\(erpRates[i].getChargeAmount().clean)","$\(erpRates[i+1].getChargeAmount().clean)",erpRates[i].getTimeDisplayed())
                                    
                                   
                                } else {
                                    
                                    if let refresh30MinDate = refresh30MinDate{
                                        start30MinRefreshInterval(nextERPStartTime: erpStartTimeNext, currentTime: refresh30MinDate)
                                    }
                                    
                                   // print("Time Difference",difference.minute,erpRates[i])
                                    return (Values.ACTIVE_NO_ARROW,"blue",combineStartAndEndTimeOfERP,"$\(erpRates[i].getChargeAmount().clean)","$\(erpRates[i+1].getChargeAmount().clean)",erpRates[i].getTimeDisplayed())
                                }
                            }
                        }
                    }
                    else{
                        
                        let combineStartAndEndTimeOfERP = ""
                        return (Values.IN_ACTIVE_NO_ARROW,"grey",combineStartAndEndTimeOfERP,"$\(0.00)","",erpRates[i].getTimeDisplayed())
                    }
                }
            }
            else{
                
                if(i+1 != erpRates.count)
                {
                    
                    if((erpRates[i].daytype == ERPValues.isWeekday && isWeekDayCheck!.isWeekend(date: isWeekDayCheck!) == ERPValues.isWeekday) || (erpRates[i].daytype != ERPValues.isWeekday && erpRates[i].daytype == ERPValues.isSatWeekend && isWeekDayCheck!.isWeekend(date: isWeekDayCheck!) == ERPValues.isSatWeekend))
                    {
                        /// checking current displayed amount is less than the upcoming ERP
                        if(erpRates[i].getChargeAmount() < erpRates[i+1].getChargeAmount()){
                    
                           
                            if let values = returnInActiveERPBoundValueForLanding(erpRates: [erpRates[i+1]], selectedTime: selectedTime, selectedDay: selectedDay){
                                return (values.bound, values.iconType, values.starteAndEndTimeCombine, values.currentPrice, values.upcomingPrice,erpRates[i].getTimeDisplayed())
                            }
                            else{
                                
                                if let erpStartTimeNext = DateUtils.shared.getDate(dateFormat: dateFormat, dateString: erpRates[i+1].starttime ?? "") {
                                    /// Getting difference between two times i.e from currentTime to erpStartTime
                                    let difference = Calendar.current.dateComponents([.minute], from: erpStartTimeNext, to: selectedTime == nil ? dateCurrentTime! : selectedTime!)
                                    ///if minutes <= 30, if this erp next time is under 30 min
                                    
                                    let combineStartAndEndTimeOfERP = erpRates[i+1].getTimeDisplayed()
                                    if(returnTimeDifferenceIsWithIn30Min(dateComponents: difference)){
                                        
                                        return (Values.INCREASE,"blue",combineStartAndEndTimeOfERP,"$\(erpRates[i].getChargeAmount().clean)","$\(erpRates[i+1].getChargeAmount().clean)",erpRates[i].getTimeDisplayed())
                                    }
                                    else{
                                        
                                        if let refresh30MinDate = refresh30MinDate{
                                        
                                            start30MinRefreshInterval(nextERPStartTime: erpStartTimeNext, currentTime: refresh30MinDate)
                                        }
                                        return (Values.ACTIVE_NO_ARROW,"blue",combineStartAndEndTimeOfERP,"$\(erpRates[i].getChargeAmount().clean)","$\(erpRates[i+1].getChargeAmount().clean)",erpRates[i].getTimeDisplayed())
                                    }
                                }
                            }
                            
                        }
                        else {
                            
                           
                            /// If current and next amount are same
                            if(erpRates[i].chargeamount == erpRates[i+1].getChargeAmount() ){
                                
                                /// if next erp amount is also zero, show no active icon
                                if(erpRates[i+1].getChargeAmount() == 0.0){
                                    
                                    let combineStartAndEndTimeOfERP = ""
                                    return (Values.IN_ACTIVE_NO_ARROW,"grey",combineStartAndEndTimeOfERP,"$\(erpRates[i].getChargeAmount().clean)","",erpRates[i].getTimeDisplayed())
                                }
                                else{
                                    
                                    if let erpStartTimeNext = DateUtils.shared.getDate(dateFormat: dateFormat, dateString: erpRates[i+1].starttime ?? "") {
                                        /// Getting difference between two times i.e from currentTime to erpStartTime
                                        let difference = Calendar.current.dateComponents([.minute], from: erpStartTimeNext, to: selectedTime == nil ? dateCurrentTime! : selectedTime!)
                                        ///if minutes <= 30, if this erp next time is under 30 min
                                        let combineStartAndEndTimeOfERP = erpRates[i+1].getTimeDisplayed()
                                        if(returnTimeDifferenceIsWithIn30Min(dateComponents: difference)){
                                            
                                            /// else show active down arrow icon
                                            return (Values.DECREASE,"blue",combineStartAndEndTimeOfERP,"$\(erpRates[i].getChargeAmount().clean)","$\(erpRates[i+1].getChargeAmount().clean)",erpRates[i].getTimeDisplayed())
                                        }
                                        else{
                                            
                                            if let refresh30MinDate = refresh30MinDate{
                                            
                                                start30MinRefreshInterval(nextERPStartTime: erpStartTimeNext, currentTime: refresh30MinDate)
                                            }
                                            
                                            /// else show active down arrow icon
                                            return (Values.ACTIVE_NO_ARROW,"blue",combineStartAndEndTimeOfERP,"$\(erpRates[i].getChargeAmount().clean)","$\(erpRates[i+1].getChargeAmount().clean)",erpRates[i].getTimeDisplayed())
                                        }
                                    }
                                }
                            }
                            else{
                                
                                if let erpStartTimeNext = DateUtils.shared.getDate(dateFormat: dateFormat, dateString: erpRates[i+1].starttime ?? "") {
                                    /// Getting difference between two times i.e from currentTime to erpStartTime
                                    let difference = Calendar.current.dateComponents([.minute], from: erpStartTimeNext, to: selectedTime == nil ? dateCurrentTime! : selectedTime!)
                                    ///if minutes <= 30, if this erp next time is under 30 min
                                    
                                    let combineStartAndEndTimeOfERP = erpRates[i+1].getTimeDisplayed()
                                    if(returnTimeDifferenceIsWithIn30Min(dateComponents: difference)){
                                        
                                        /// else show active down arrow icon
                                        return (Values.DECREASE,"blue",combineStartAndEndTimeOfERP,"$\(erpRates[i].getChargeAmount().clean)","$\(erpRates[i+1].getChargeAmount().clean)",erpRates[i].getTimeDisplayed())
                                    }
                                    else{
                                        
                                        if let refresh30MinDate = refresh30MinDate{
                                        
                                            start30MinRefreshInterval(nextERPStartTime: erpStartTimeNext, currentTime: refresh30MinDate)
                                        }
                                        return (Values.ACTIVE_NO_ARROW,"blue",combineStartAndEndTimeOfERP,"$\(erpRates[i].getChargeAmount().clean)","$\(erpRates[i+1].getChargeAmount().clean)",erpRates[i].getTimeDisplayed())
                                    }
                                }
                            }
                        }
                    
                    }
                    else{
                        let combineStartAndEndTimeOfERP = ""
                        return (Values.IN_ACTIVE_NO_ARROW,"grey",combineStartAndEndTimeOfERP,"$\(0.00)","",erpRates[i].getTimeDisplayed())
                    }
                    
                }
            }
        }
        else{}
    }
    
    return nil
}

func returnTimeDifferenceIsWithIn30Min(dateComponents:DateComponents) -> Bool {
    
    var isWithIn30Min = false
    if let minute = dateComponents.minute{
        
        if(abs(minute) <= 30){
            
            isWithIn30Min = true
        }
    }
    
    return isWithIn30Min
}

func start30MinRefreshInterval(nextERPStartTime:Date,currentTime:Date){
    
    /* For example :
      var ERPNextStart =  9:25
      ERPNextStart - 30 * 60 = 8:55
      
     let currentTime = 8:52
     
     let differenceTime = ERPNextStart - currentTime = 3 min
       
    */
    
    let erpNextStartTimeInterval = (nextERPStartTime.timeIntervalSince1970*1000) - 30 * 60
    
    let currentTimeInterval = currentTime.timeIntervalSince1970
    
    let difference = erpNextStartTimeInterval - currentTimeInterval
    
    DataCenter.shared.erp30MinRefreshTimer(differenceInterval: difference)
}

func returnInActiveERPBoundValueForLanding(erpRates:[Rates],selectedTime:Date? = nil,selectedDay:Date? = nil)-> (bound:String, iconType:String,starteAndEndTimeCombine:String,currentPrice:String,upcomingPrice:String, inActiveTime:String)?{
    

    for i in 0..<erpRates.count{
        
        /// Getting start and end time of erp from erpRates array
        let startTime = erpRates[i].starttime
//        let endTime = erpRates[i].endtime
        
        /// Getting current time from Date and converting back to date from string using date formatter
        let currentTime = Date()
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "HH:mm"
        let dateFormat = "HH:mm"
        let strCurTime = DateUtils.shared.getTimeDisplay(dateFormat: dateFormat, date: currentTime)
        
        /// converting erpStartTime to Date
        
        // Crash fix: BREEZES-7564 need validate the date instead of force unwrapped it
        if let dateCurrentTime = DateUtils.shared.getDate(dateFormat: dateFormat, dateString: strCurTime), let erpStartTime = DateUtils.shared.getDate(dateFormat: dateFormat, dateString: startTime ?? "") {
            /// Getting difference between two times i.e from currentTime to erpStartTime
            let difference = Calendar.current.dateComponents([.minute], from: erpStartTime, to: selectedTime == nil ? dateCurrentTime : selectedTime!)
            
            let isWeekDayCheck = selectedDay == nil ? Date() : selectedDay
            /// checking if difference time is null
            if let minute = difference.minute {
                
                ///if minutes <= 30, that means this is the first erp start time of this zone id with the corresponding erpID
                if(abs(minute) <= 30){
                    
                    if((erpRates[i].chargeamount ?? 0) > 0){
                        
                        if((erpRates[i].daytype == ERPValues.isWeekday && isWeekDayCheck!.isWeekend(date: isWeekDayCheck!) == ERPValues.isWeekday) || (erpRates[i].daytype != ERPValues.isWeekday && erpRates[i].daytype == ERPValues.isSatWeekend && isWeekDayCheck!.isWeekend(date: isWeekDayCheck!) == ERPValues.isSatWeekend))
                        {
                            let combineStartAndEndTimeOfERP = erpRates[i].getTimeDisplayed()
                            return (Values.INCREASE_GREY,"grey",combineStartAndEndTimeOfERP,"$\(0.00)","$\((erpRates[i].chargeamount ?? 0.0).clean)","")
                        }
                        else{
                            
                            let combineStartAndEndTimeOfERP = ""
                            return (Values.IN_ACTIVE_NO_ARROW,"grey",combineStartAndEndTimeOfERP,"$\(0.00)","","")
                        }
                        
                    }
                    else{
                        
                        let combineStartAndEndTimeOfERP = ""
                        return (Values.IN_ACTIVE_NO_ARROW,"grey",combineStartAndEndTimeOfERP,"$\(0.00)","","")
                    }
                    
                }
                else {
                    
                    if((erpRates[i].chargeamount ?? 0) > 0){
            
                        let dateSelected = selectedTime == nil ? dateCurrentTime : selectedTime!
                            if(erpStartTime < dateSelected){
                                let combineStartAndEndTimeOfERP = ""
                                if let inActiveTime = erpRates.last?.getTimeDisplayed() {
                                    return (Values.IN_ACTIVE_NO_ARROW,"grey",combineStartAndEndTimeOfERP,"$\(0.00)","",inActiveTime)
                                }
                               
                            }
                        
                        else{
                            
                            if((erpRates[i].daytype == ERPValues.isWeekday && isWeekDayCheck!.isWeekend(date: isWeekDayCheck!) == ERPValues.isWeekday) || (erpRates[i].daytype != ERPValues.isWeekday && erpRates[i].daytype == ERPValues.isSatWeekend && isWeekDayCheck!.isWeekend(date: isWeekDayCheck!) == ERPValues.isSatWeekend))
                            {
                                let combineStartAndEndTimeOfERP = erpRates[i].getTimeDisplayed()
                                return (Values.IN_ACTIVE_NO_ARROW,"grey",combineStartAndEndTimeOfERP,"$\(0.00)","$\(erpRates[i].getChargeAmount().clean)","")
                            }
                            else{
                                
                                let combineStartAndEndTimeOfERP = ""
                                return (Values.IN_ACTIVE_NO_ARROW,"grey",combineStartAndEndTimeOfERP,"$\(0.00)","","")
                            }
                        }
                        
                    }
                    else{
                        
                        let combineStartAndEndTimeOfERP = ""
                        return (Values.IN_ACTIVE_NO_ARROW,"grey",combineStartAndEndTimeOfERP,"$\(0.00)","",erpRates[i].getTimeDisplayed())
                    }
                }
            }
        }
    }
    
    return nil
    
}

func getNearByERPS(coordinate: CLLocationCoordinate2D,timeStamp:Int) -> (erpCollection:Turf.FeatureCollection,nearByData:[NearByERPFilter]) {
    
    let date = Date(timeIntervalSince1970: TimeInterval(timeStamp/1000))
//    let dateFormatter = DateFormatter()
//    dateFormatter.dateFormat = "EEEE"
//
//    let selectedDayStr = dateFormatter.string(from: date)
//    let selectedDay = dateFormatter.date(from: selectedDayStr)
//    dateFormatter.dateFormat = "HH:mm"
    
    let dateFormat = "HH:mm"
    let selectedTimeStr = DateUtils.shared.getTimeDisplay(dateFormat: dateFormat, date: date)
    let selectedTime = DateUtils.shared.getDate(dateFormat: dateFormat, dateString: selectedTimeStr)
    
    var erpFeatureCollection = FeatureCollection(features: [])
    var finalERPArray = [NearByERPFilter]()
    var finalSortedERPArray = [NearByERPFilter]()
    
    if let erpData = DataCenter.shared.getERPJsonData(){
       
       let erp = erpData.erp
//       let publicHolidays = erpData.publicholiday
//
//       let tyepOfDay = Date().isWeekend(date: Date())
       
       // set erp amount to 0 if it's a public holiday or sunday
       
        var erpCount = 0
        for erpItem in erp ?? [] {
            
            if let i = erpData.getERPRates().firstIndex(where: { $0.zoneid == erpItem.getZoneId() }) {
                
                let rates = erpData.getERPRates()[i].getRates()
                var chargeAmount = getERPChargeAmountWithSelectedDate(erpRates:rates,selectedTime: selectedTime!,selectedDay: date)
                
                let (startCoordinate, endCoordinate) = erpItem.getStartEndCoordinate()
                
                let midCoordinate2D = mid(startCoordinate, endCoordinate)
                var coordinates = [CLLocationCoordinate2D]()
                
                coordinates.append(startCoordinate)
                coordinates.append(endCoordinate)
                
                let lineAllFeature = LineString(coordinates)
                
                
                if let coord = lineAllFeature.closestCoordinate(to: coordinate)?.coordinate {
                    
                    let toLocation = CLLocation(latitude: midCoordinate2D.latitude, longitude: midCoordinate2D.longitude)
                    
                    let fromLocation = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
                    
                    //var distance = coord.distance(to: coordinate)
                    
                    let distance = fromLocation.distance(from: toLocation)
                    
                    if distance <= 3000 {
                        
                        if let values = returnERPBoundValueWithIconTypeForLanding(erpRates: erpData.getERPRates()[i].getRates(),selectedTime: selectedTime,selectedDay: date){
                            
                            erpCount = erpCount + 1
                            if(chargeAmount == "0"){
                                
                                chargeAmount = "$0.00"
                            }
                            //let priceArray = chargeAmount.components(separatedBy: "$")
                            //let doublePrice = priceArray[1]
                            //let price = String(format: "$%.2f", doublePrice)
                            var feature = Turf.Feature(geometry: .point(Point(midCoordinate2D)))
                            
                            feature.properties = [
                                "cost":.string(chargeAmount),
                                "currentprice":.string(values.currentPrice),
                                "upcomingtime":.string(values.starteAndEndTimeCombine),
                                "upcomingprice":.string(values.upcomingPrice),
                                "boundValue":.string(values.bound),
                                "type":.string("ERP"),
                                "name":.string(erpItem.name ?? ""),
                                "erpid":.string("\(erpItem.erpid ?? 0)"),
                                "erpOrder":.number(Double(erpCount)),
                                "selectedTime": .string(values.selectedTimeERPMapLanding)
                                
                            ]
                            
                            let nearByERP = NearByERPFilter(distance: distance, erpFeatures: feature)
                            finalERPArray.append(nearByERP)                                               //erpFeatureCollection.features.append(feature)
                            
                        }
                        else if let inActiveValues = returnInActiveERPBoundValueForLanding(erpRates: erpData.getERPRates()[i].getRates(),selectedTime:selectedTime,selectedDay: date){
                            
                            erpCount = erpCount + 1
                            if(chargeAmount == "0"){
                                
                                chargeAmount = "$0.00"
                            }
                            
                            //let priceArray = chargeAmount.components(separatedBy: "$")
                            //let doublePrice = priceArray[1]
                            //let price = String(format: "$%.2f", doublePrice)
                            var feature = Turf.Feature(geometry: .point(Point(midCoordinate2D)))
                            
                            feature.properties = [
                                "cost":.string(chargeAmount),
                                "currentprice":.string(inActiveValues.currentPrice),
                                "upcomingtime":.string(inActiveValues.starteAndEndTimeCombine),
                                "upcomingprice":.string(inActiveValues.upcomingPrice),
                                "boundValue":.string(inActiveValues.bound),
                                "type":.string("ERP"),
                                "name":.string(erpItem.name ?? ""),
                                "erpid":.string("\(erpItem.erpid ?? 0)"),
                                "erpOrder":.number(Double(erpCount)),
                                "selectedTime": .string(inActiveValues.inActiveTime)
                            ]
                            
                            let nearByERP = NearByERPFilter(distance: distance, erpFeatures: feature)
                            finalERPArray.append(nearByERP)
                            //erpFeatureCollection.features.append(feature)
                        }
                        
                        
                    }
                    
                }
                
            }
            
        }
        
        let sortedERPArray = finalERPArray.sortedDistance()
        
        for erp in sortedERPArray{
            
            if let feature = erp.erpFeatures {
                
                if(erpFeatureCollection.features.count < 3){
                    
                    finalSortedERPArray.append(erp)
                    erpFeatureCollection.features.append(feature)
                }
            }
        }
    }
    
    return (erpFeatureCollection,finalSortedERPArray)
}

func getTimeSlotDisplayWithTime() {
    
}


func getERPInformationBetweenRoutes(in routes:[Route],from allERPTolls: FeatureCollection,id:String,timeStamp:Int) -> ERPFullRouteData?
{
    let date = Date(timeIntervalSince1970: TimeInterval(timeStamp/1000))
//    let dateFormatter = DateFormatter()
//    dateFormatter.dateFormat = "EEEE"
//
//    let selectedDayStr = dateFormatter.string(from: date)
//    let selectedDay = dateFormatter.date(from: selectedDayStr)
//    dateFormatter.dateFormat = "HH:mm"
    
    let dateFormat = "HH:mm"
    let selectedTimeStr = DateUtils.shared.getTimeDisplay(dateFormat: dateFormat, date: date)
    let selectedTime = DateUtils.shared.getDate(dateFormat: dateFormat, dateString: selectedTimeStr)
    
    
    var erpRoutes = [ERPRoute]()
    var erpCharges = [ERPCharge]()
    var routeCount = -1
    for route in routes{

        erpCharges = []

        routeCount = routeCount + 1
        let routeLineSegment = routesAsLineSegments(route: route)
        
        //Looping for each route Segment
        for routeSegment in routeLineSegment{
            
        
        //Looping for each allERPTolls which is a LineString
        for erpToll in allERPTolls.features{
            
                //Getting geometry from erpToll
                guard case let .lineString(erpGeometry) = erpToll.geometry else { return nil  }
                //print("Toll",boothGeometry?.coordinates.latitude)
                
                //creating a LingSegment for E R P
                let erpLineSegment =  (erpGeometry.coordinates[0],erpGeometry.coordinates[1])
                
                if let lineSegment = erpLineSegment as? LineSegment {
                
                    ///Finding intersection with two Linesegments
                    if let intersectCoordinate = intersection(routeSegment , lineSegment) {
                        //print(erpToll)
                        
                        if case let .string(stringPrice) = erpToll.properties!["cost"],
                           case let .string(erpID) = erpToll.properties?["erpid"]
                        {
                            if let erpData = DataCenter.shared.getERPJsonData(){
                                
                                if let erpIDIndex = erpData.erp!.firstIndex(where: { $0.erpid == Int(erpID) }) {
                                    
                                    if let zoneIDIndex = erpData.getERPRates().firstIndex(where: { $0.zoneid == erpData.erp![erpIDIndex].zoneid }) {
                                        
                                        let rates = erpData.getERPRates()[zoneIDIndex].getRates()
                                        let chargeAmount = getERPChargeAmountWithSelectedDate(erpRates:rates,selectedTime: selectedTime!,selectedDay: date) //  selectedDay!
                                        if(chargeAmount != "0"){
                                            
                                            let priceArray = chargeAmount.components(separatedBy: "$")
                                            let doublePrice = Float(priceArray[1]) ?? 0.00
                                           
                                           
                                            let erpCharge = ERPCharge(erpId: erpID, chargeAmount: Double(doublePrice))
                                            erpCharges.append(erpCharge)
                                        }
                                        else{
                                            
                                            let erpCharge = ERPCharge(erpId: erpID, chargeAmount:0.00)
                                            erpCharges.append(erpCharge)
                                        }
                                    }
                                }
                                
                            }
                            
                        
                        }
                        
                    }
                }
            }
        }
        
        let routes = ERPRoute(routeId: String(routeCount), erpCharges: erpCharges)
        erpRoutes.append(routes)
    }
    
    if(erpRoutes.count > 0){
        
        let erpFullRoute = ERPFullRouteData(id: id, erpRoutes: erpRoutes)
        return erpFullRoute
    }
   
    return nil
}

// get active erp in a route array
func getActiveERPs(in routes: [Route], from erpTolls: FeatureCollection) -> [Turf.Feature] {

    var allActiveERP = [Turf.Feature]()
    
    for route in routes{
        
//        let feature = [Turf.Feature(geometry: .lineString(LineString(route.shape!.coordinates)))]
//        let routCollection = FeatureCollection(features: feature)
//        let routeGeometry = routCollection.features[0].geometry.value as? LineString
//        var tempIntersectionCoordinate = CLLocationCoordinate2D()
//        var tempERPPrice = Float()
        
        let routeLineSegment = routesAsLineSegments(route: route)
        
        //Looping for each erpTolls which is a LineString
        for erpToll in erpTolls.features{
            
            //Looping for each route Segment
            for routeSegment in routeLineSegment{
                
                //Getting geometry from erpToll
                guard case let .lineString(erpGeometry) = erpToll.geometry else { return allActiveERP }
                //print("Toll",boothGeometry?.coordinates.latitude)
                
                //creating a LingSegment for E R P
                let erpLineSegment =  (erpGeometry.coordinates[0],erpGeometry.coordinates[1])
                
                if let lineSegment = erpLineSegment as? LineSegment {
                
                    ///Finding intersection with two Linesegments
                    if let intersectCoordinate = intersection(routeSegment , lineSegment) {
                        //print(erpToll)
                        
                        guard case let .string(stringPrice)  = erpToll.properties!["cost"] else { return allActiveERP }
                        let priceArray = stringPrice.components(separatedBy: "$")
                        let doublePrice = priceArray[1]
                       
    //                    tempERPPrice = tempERPPrice + Float(doublePrice)!
    //                    tempIntersectionCoordinate = intersectCoordinate

                        if(Float(doublePrice)! > 0.0) {

                            var feature = Turf.Feature(geometry: .point(Point(intersectCoordinate)))

                            feature.properties = [
                                "cost":.string(stringPrice),
                            ]
                                                            
                            allActiveERP.append(feature)
                            
                        }
                        break
                    
                    }
                }
            }
        }
    }
    return allActiveERP
}

func getERPDetailsFeature(id:String, routeId: String,timeStamp:Int) -> Turf.FeatureCollection{
    
    var erpFeatureCollection = FeatureCollection(features: [])
    
    var erpCount = 0
    ///  null check for erpFullRouteData
    if let data = DirectionService.shared.erpFullRouteData {
        
        /// check if id saved in memory is equal to id sent by RN
        if(data.id == id){
            
            /// get index value from data erpRoutes based on routeID sent by RN
            if let index = data.erpRoutes.firstIndex(where: { $0.routeId == routeId }) {
                
                for erpCharge in data.erpRoutes[index].erpCharges{
                    
                    let erpId = Int(erpCharge.erpId)
                    
                    erpCount = erpCount + 1
                    
                    let features = findERPBasedOnERPId(erpId ?? 0, timeStamp: timeStamp,erpCount: erpCount)
                    
                    for feature in features {
                        
                        erpFeatureCollection.features.append(feature)
                    }
                    
                }
            }
            
        }
    }
    
    return erpFeatureCollection
}

func findERPBasedOnERPId(_ erpId:Int,timeStamp:Int,erpCount:Int = 0) -> [Turf.Feature]{
    
    var erpFeatureCollection = FeatureCollection(features: [])
    var features = [Turf.Feature]()
    let date = Date(timeIntervalSince1970: TimeInterval(timeStamp/1000))
//    let dateFormatter = DateFormatter()
//    dateFormatter.dateFormat = "EEEE"
//
//    let selectedDayStr = dateFormatter.string(from: date)
//    let selectedDay = dateFormatter.date(from: selectedDayStr)
//    dateFormatter.dateFormat = "HH:mm"
    
    let dateFormat = "HH:mm"
    let selectedTimeStr = DateUtils.shared.getTimeDisplay(dateFormat: dateFormat, date: date)
    let selectedTime = DateUtils.shared.getDate(dateFormat: dateFormat, dateString: selectedTimeStr)
    
    let erpCount = erpCount
    if let erpData = DataCenter.shared.getERPJsonData(){
        
        for erp in erpData.erp ?? []{
            
            if(erp.erpid == erpId){
                
                //From root json I will erp rates and see if zone id matches
                if let i = erpData.getERPRates().firstIndex(where: { $0.zoneid == erp.getZoneId() }) {
                    
                    print("FINDING ERP amount based on ERPID",erpId)
                    let startLat = erp.startlat?.toDouble() ?? 0
                    let startLong = erp.startlong?.toDouble() ?? 0
                    let endLat = erp.endlat?.toDouble() ?? 0
                    let endLong = erp.endlong?.toDouble() ?? 0
                    
                    let startCoordinate = CLLocationCoordinate2D(latitude: startLat, longitude: startLong)
                    
                    let endCoordinate = CLLocationCoordinate2D(latitude: endLat, longitude: endLong)
                    
                    let midCoordinate2D = mid(startCoordinate, endCoordinate)
                    
                    if let values = returnERPBoundValueWithIconTypeForLanding(erpRates: erpData.getERPRates()[i].getRates(),selectedTime: selectedTime,selectedDay: date){
                        
                        //erpCount = erpCount + 1
                        let priceArray = values.currentPrice.components(separatedBy: "$")
                        if let price = Double(priceArray[1]){
                            
                            let priceString = String(format: "$%.2f",price)
                            var feature = Turf.Feature(geometry: .point(Point(midCoordinate2D)))
                            
                            feature.properties = [
                                "cost":.string(priceString),
                                "currentprice":.string(values.currentPrice),
                                "upcomingtime":.string(values.starteAndEndTimeCombine),
                                "upcomingprice":.string(values.upcomingPrice),
                                "boundValue":.string(values.bound),
                                "type":.string("ERP"),
                                "erpid":.string("\(erpId)"),
                                "name":.string(erp.name ?? ""),
                                "erpOrder":.number(Double(erpCount)),
                                "selectedTime": .string(values.selectedTimeERPMapLanding)
                            ]
                            
                            features.append(feature)
                        }
                       
                       
                    }
                    else if let inActiveValues = returnInActiveERPBoundValueForLanding(erpRates: erpData.getERPRates()[i].getRates(),selectedTime: selectedTime,selectedDay: date){
                        
                        //erpCount = erpCount + 1
                        var feature = Turf.Feature(geometry: .point(Point(midCoordinate2D)))
                        
                        let priceArray = inActiveValues.currentPrice.components(separatedBy: "$")
                        if let price = Double(priceArray[1]){
                            
                            let priceString = (String(format: "$%.2f",price))
                            feature.properties = [
                                "cost":.string(priceString),
                                "currentprice":.string(inActiveValues.currentPrice),
                                "upcomingtime":.string(inActiveValues.starteAndEndTimeCombine),
                                "upcomingprice":.string(inActiveValues.upcomingPrice),
                                "boundValue":.string(inActiveValues.bound),
                                "type":.string("ERP"),
                                "erpid":.string("\(erpId)"),
                                "name":.string(erp.name ?? ""),
                                "erpOrder":.number(Double(erpCount)),
                                "selectedTime": .string(inActiveValues.inActiveTime)
                            ]
                            
                            features.append(feature)
                        }
                        
                    }
                    
                }
            }
        }
    }
    
   return features
}
   

func setUpFirebaseRemoteConfig(){
    let settings = RemoteConfigSettings()
    settings.minimumFetchInterval = Values.minimumFetchInterval
    remoteConfig.configSettings = settings
}

func routesAsLineSegments(route:Route) -> [LineSegment] {
    
    var routeLineSegment = [LineSegment]()
    if let coordinates = route.shape?.coordinates, coordinates.count >= 2 {
        for i in 0...(coordinates.count - 2) {
            let segment = (coordinates[i], coordinates[i + 1])
            routeLineSegment.append(segment)
        }
    }
    return routeLineSegment
}

func getInstabugCustomAttribute(){
    
    //Checking if user group key is available in UserDefaults
    let userGroup = (UserDefaults.standard.value(forKey: Values.usergroup) as? String) ?? ""
    
    //If userGroup is empty, call Amplify API to fetch attributes
    if(userGroup.isEmpty){
        AWSAuth.sharedInstance.fetchAttributes { result in
            
            //After successfull response check if usergroup is empty
            if(AWSAuth.sharedInstance.userGroup != "")
            {
                showInstabug(setAttribute: true,userGroup: AWSAuth.sharedInstance.userGroup)
                
            }
            
        } onFailure: { error in
            
            //If API fails, then don't set custom attribute to Instabug
            
        }

    }
    //else show Instabug by setting custom attribute
    else
    {
       showInstabug(setAttribute: true, userGroup: userGroup)
    }
    
}

func showInstabug(setAttribute:Bool = false,userGroup:String = ""){
    
    if(setAttribute)
    {
        Instabug.setUserAttribute(userGroup, withKey: Values.usergroup)
    }
}

/// Setting Cognito id as user_id attribute in Instabug and Firebase
func setUserIDAttribute(cognito_id:String){
    
    Instabug.setUserAttribute(cognito_id, withKey: Values.userID)
    Crashlytics.crashlytics().setUserID(cognito_id)
}

func hashID( sourceLat: Double, sourceLong: Double, destinationLat: Double, destinationLong: Double, timestamp: Int) -> String {
    return "\(sourceLat)\(sourceLong)\(destinationLat)\(destinationLong)\(timestamp)"
}

func AltitudeForZoomLevel(_ zoomLevel: Double, _ pitch: CGFloat, _ latitude: CLLocationDegrees, _ size: CGSize) -> CLLocationDistance {
    let metersPerPixel = getMetersPerPixelAtLatitude(latitude, zoomLevel)
    let metersTall = metersPerPixel * Double(size.height)
    let altitude = metersTall / 2 / tan(RadiansFromDegrees(AngularFieldOfView) / 2)
    return altitude * sin(MPI2 - RadiansFromDegrees(CLLocationDegrees(pitch))) / sin(MPI2)
}

func ZoomLevelForAltitude(_ altitude: CLLocationDistance, _ pitch: CGFloat, _ latitude: CLLocationDegrees, _ size: CGSize) -> Double {
    let eyeAltitude = altitude / sin(MPI2 - RadiansFromDegrees(CLLocationDegrees(pitch))) * sin(MPI2)
    let metersTall = eyeAltitude * 2 * tan(RadiansFromDegrees(AngularFieldOfView) / 2)
    let metersPerPixel = metersTall / Double(size.height)
    let mapPixelWidthAtZoom = cos(RadiansFromDegrees(latitude)) * M2PI * EARTH_RADIUS_M / metersPerPixel
    return log2(mapPixelWidthAtZoom / tileSize)
}

private func clamp(_ value: Double, _ min: Double, _ max: Double) -> Double {
    return fmax(min, fmin(max, value))
}

private func worldSize(_ scale: Double) -> Double {
    return scale * tileSize
}

private func RadiansFromDegrees(_ degrees: CLLocationDegrees) -> Double {
    return degrees * Double.pi / 180
}

func getMetersPerPixelAtLatitude(_ lat: Double, _ zoom: Double) -> Double {
    let constrainedZoom = clamp(zoom, MIN_ZOOM, MAX_ZOOM)
    let constrainedScale = pow(2.0, constrainedZoom)
    let constrainedLatitude = clamp(lat, -LATITUDE_MAX, LATITUDE_MAX)
    return cos(constrainedLatitude * DEG2RAD) * M2PI * EARTH_RADIUS_M / worldSize(constrainedScale)
}
/*
func hashID( source: String, destination: String, timestamp: NSNumber) -> String {
    
    let generateIdString = source + destination + timestamp.stringValue
    let length = Int(CC_MD5_DIGEST_LENGTH)
            var digest = [UInt8](repeating: 0, count: length)

            if let d = generateIdString.data(using: String.Encoding.utf8) {
                _ = d.withUnsafeBytes { (body: UnsafePointer<UInt8>) in
                    CC_MD5(body, CC_LONG(d.count), &digest)
                }
            }

            return (0..<length).reduce("") {
                $0 + String(format: "%02x", digest[$1])
            }
}
*/

/// Calculate estimate arrival time in 24 hour format
func estimatedArrivalTime(_ duration: TimeInterval,date:Date = Date()) -> (time: String,format: String) {
    let arrivalDate = Date(timeInterval: duration, since: date)
    let calendar = Calendar.current
    let hour = calendar.component(.hour, from: arrivalDate)
    let t = hour > 12 ? hour - 12: hour
    let hourString = t < 10 ? "0\(t)" : t.description
    let minute = calendar.component(.minute, from: arrivalDate)
    let minString = minute < 10 ? "0\(minute)" : minute.description
    return (time: "\(hourString):\(minString)", format: hour >= 12 ? "pm" : "am")
    // comment out showing in 24 hour format for the time being
   // return (time: "\(hour):\(minString)", format: "")
}

func identifyUserLocInInnerZone(centerCoordinate: CLLocationCoordinate2D, currentLocation: CLLocationCoordinate2D) -> Bool {
    
    let toLocation = CLLocation(latitude: centerCoordinate.latitude, longitude: centerCoordinate.longitude)
    let currentLocation = CLLocation(latitude: currentLocation.latitude, longitude: currentLocation.longitude)
    
    let distance = currentLocation.distance(from: toLocation)
    return distance <= Double(SelectedProfiles.shared.getInnerZoneRadius())
}

func identifyUserLocInOuterZone(centerCoordinate: CLLocationCoordinate2D, currentLocation: CLLocationCoordinate2D) -> Bool {
    
    let toLocation = CLLocation(latitude: centerCoordinate.latitude, longitude: centerCoordinate.longitude)
    let currentLocation = CLLocation(latitude: currentLocation.latitude, longitude: currentLocation.longitude)
    
    let distance = currentLocation.distance(from: toLocation)
    
    if let higherZoneRadius = SelectedProfiles.shared.getHigherZoneRadius(){
        
        return distance <= Double(higherZoneRadius.radius)
    }
    else{
        
        return false
    }
    
}

func getAPNSType(aps: [String: AnyObject]?) -> String? {
    return aps?["type"] as? String
}

func getAPNSMessage(aps: [String: AnyObject]?) -> String? {
    let alertDic = aps?["alert"] as? [String: AnyObject]
    let bodyPush = alertDic?["body"] as? String
    return bodyPush
}

/// Show feedback
func showFeedback(from viewControler: UIViewController, coordinate: CLLocationCoordinate2D) {
    
    let storyboard: UIStoryboard = UIStoryboard(name: "Feedback", bundle: nil)
    let vc: FeedbackVC = storyboard.instantiateViewController(withIdentifier: "FeedbackVC")  as! FeedbackVC
    vc.modalPresentationStyle = .formSheet
    vc.locCoordinate = coordinate
    viewControler.present(vc, animated: true, completion: nil)
}

func parseSelectedRoutablePoint(_ location: [AnyHashable : Any]) -> RoutablePointModel? {
    
    let selectedPinIndex = location["selectedPinIndex"] as? Int ?? -1
    let routablePoints = location["routablePoints"] as?  [[String: Any]] ?? []
    
    var pinList: [RoutablePointModel] = []
    if !routablePoints.isEmpty {
        for routableDic in routablePoints {
            let item = RoutablePointModel(routableDic)
            pinList.append(item)
        }
    }
    
    if selectedPinIndex != -1 && selectedPinIndex < pinList.count {
        return pinList[selectedPinIndex]
    }
    
    return nil
}

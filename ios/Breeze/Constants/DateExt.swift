//
//  DateExt.swift
//  Breeze
//
//  Created by VishnuKanth on 20/12/20.
//

import UIKit
import SwiftyBeaver

extension ISO8601DateFormatter {
    convenience init(_ formatOptions: Options) {
        self.init()
        self.formatOptions = formatOptions
    }
}
extension Formatter {
    static let iso8601withFractionalSeconds = ISO8601DateFormatter([.withInternetDateTime, .withFractionalSeconds])
}

extension Date {
    var iso8601withFractionalSeconds: String { return Formatter.iso8601withFractionalSeconds.string(from: self) }
}

extension String {
    var iso8601withFractionalSeconds: Date? { return Formatter.iso8601withFractionalSeconds.date(from: self) }
    
//    var dateFromTimeAMPM: Date? {
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "h:mm a"
//        dateFormatter.amSymbol = "AM"
//        dateFormatter.pmSymbol = "PM"
//        return dateFormatter.date(from: self)
//    }
    
//    func dateGMT0(format: String = Date.formatYYYYMMdd) -> Date? {
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = format
//        dateFormatter.timeZone = TimeZone(abbreviation: "GMT+0:00")
//        return dateFormatter.date(from: self)
//    }
    
//    func date(format: String = Date.formatYYYYMMdd) -> Date? {
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = format
//        return dateFormatter.date(from: self)
//    }
    
    var serverDate: Date {
        return DateUtils.shared.getDate(dateFormat: Date.formatEEEEddMMYYYYHHmmssZ, dateString: self) ?? Date()
    }
}

extension Int {
    func getDuration() -> String {
        let seconds = self
        if (seconds / 3600 >= 1){
            return "\(seconds / 3600)h:\((seconds % 3600) / 60)min"
        }else{
            return "\((seconds % 3600) / 60)min"
        }
    }
    
    func getDurationHaveSpace() -> String {
        let seconds = self
        if (seconds / 3600 >= 1){
            return "\(seconds / 3600) h:\((seconds % 3600) / 60) min"
        }else{
            return "\((seconds % 3600) / 60) min"
        }
    }
    
    func secondsToHoursMinutes() -> String {
        let seconds = self
        return "\(seconds / 3600) hr \((seconds % 3600) / 60) min"
    }
}

extension Date {
    
    static let defaultFormat = "dd/MM/yyyy HH:mm:ss"
    static let formatYYYYMMdd = "yyyy-MM-dd"
    static let defaultStringFormat = "yyyy-mm-dd"
    static let longTimeFormatWithOn = "hh:mm:ss a 'on' MMMM dd, yyyy"
    static let formatYYYYMMddTZ = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
    static let formatYYYYMMddTHHmm = "yyyy-MM-dd'T'HH:mm"
    static let formatYYYYMMddHHmm = "yyyy-MM-dd HH:mm"
    static let formatDMMMYYYY = "d MMM yyyy"
    static let formatDDMMMYYYY = "dd MMM yyyy"
    static let formatDDMMMYY = "dd MMM yy"
    static let formatEEEE = "EEEE"
    static let formatLLLL = "LLLL"
    static let formatHHmmss = "HH:mm:ss"
    static let formatHHmm = "HH:mm"
    static let formathhmma = "hh:mma"
    static let formatEdMMMyyyy = "E, d MMM, yyyy"
    static let formatYYYYMMddHHmmss = "yyyy-MM-dd HH:mm:ss"
    static let formatEEEEddMMYYYYHHmmssZ = "EEEE, dd MMM yyyy HH:mm:ss Z"
    static let formatHHmmdMMME = "hh:mma, d MMM, E"
    static let formathhmmssaonMMMddyyyy = "hh:mm:ss a 'on' MMMM dd, yyyy"
    
    
    func endDate() -> Date{
        return Self(timeIntervalSinceNow: 24)
    }
    
//    func stringToDate(stringDate: String) -> Date {
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = Date.formatYYYYMMdd
//        dateFormatter.timeZone = TimeZone(abbreviation: "GMT+0:00")
//
//        return dateFormatter.date(from: stringDate)!
//    }
    
//    func dateToString(date: Date) -> String {
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = Date.formatYYYYMMdd
//        return dateFormatter.string(from: date)
//    }
    
//    func dateString(format: String = "yyyy-MM-dd") -> String {
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = format
//        dateFormatter.amSymbol = "am"
//        dateFormatter.pmSymbol = "pm"
//        return dateFormatter.string(from: self)
//    }
    
//    func dateStringGMT0(format: String = Date.formatYYYYMMdd) -> String {
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = format
//        dateFormatter.timeZone = TimeZone(abbreviation: "GMT+0:00")
//        return dateFormatter.string(from: self)
//    }
    
    func isSameDay(date1: Date, date2: Date) -> Bool {
        let isToday = Calendar.current.isDateInToday(date2)
        
        return isToday
    }
    
    var weekDate: String {
        return DateUtils.shared.getTimeDisplay(dateFormat: Date.formatEEEE, date: self)
    }
    
    /* weekday
     The corresponding value is an NSInteger. Equal to kCFCalendarUnitWeekday.
     The weekday units are the numbers 1 through N (where for the Gregorian calendar N=7 and 1 is Sunday).
    */
    var weekDateRPM: String {
        let calendar = Calendar(identifier: Calendar.Identifier.gregorian)
        let dateComponent = calendar.dateComponents(
            [.year, .month, .day, .weekday], from: self)
        switch dateComponent.weekday {
        case 1:
            return ERPValues.isSunWeekend
        case 7:
            return ERPValues.isSatWeekend
        default:
            return ERPValues.isWeekday
        }
    }
    
    func isWeekend(date: Date) -> String {
        return date.weekDateRPM
    }
    
//    func exportDateString(format: String = Date.formatEEEE) -> Date? {
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = format
//
//        let selectedDayStr = dateFormatter.string(from: self)
//        return dateFormatter.date(from: selectedDayStr)
//    }
    
    func getTimeFromComponents(fromDate:Date) -> Double{
        
        let components = Calendar.current.dateComponents(
            [.hour, .minute, .second], from: fromDate)
        
        guard let seconds = components.second,
              let minutes = components.minute,
              let hours = components.hour else
        {
            return 0
        }
        let time = Double(seconds + minutes * 60 + hours * 60 * 60)
        
        return time
    }
    
    func getMonthFromComponents(fromDate:Date) -> Int{
        
        let components = Calendar.current.dateComponents(
            [.month], from: fromDate)
        
        guard
              let month = components.month
               else
        {
            return 0
        }
        let mm = Int(month)
        
        return mm
    }
    
    func getYearFromComponents(fromDate:Date) -> Int{
        
        let components = Calendar.current.dateComponents(
            [.year], from: fromDate)
        
        guard let year = components.year
             else
        {
            return 0
        }
        let yr = Int(year)
        
        return yr
    }
    
    func currentTimeisBetween(startTime:String,endTime:String)-> Bool{
        
        if(startTime != "" && endTime != "")
        {
            let currentTime = getTimeFromComponents(fromDate: self) 
            
//            let dateFormatter = DateFormatter()
//            dateFormatter.dateFormat = Date.formatHHmm
            
            // In certain scenarion, the startTime and endTime is not valid which will cause crash
            guard let startTimeDate = DateUtils.shared.getDate(dateFormat: Date.formatHHmm, dateString: startTime), let endTimeDate = DateUtils.shared.getDate(dateFormat: Date.formatHHmm, dateString: endTime) else {
                SwiftyBeaver.error("Invalid start or end time: \(startTime) - \(endTime)")
                return false
            }
            let startTime = getTimeFromComponents(fromDate: startTimeDate)
            let endTime = getTimeFromComponents(fromDate: endTimeDate)
            
            return startTime <= currentTime && currentTime < endTime
        }
        return false
        
    }
    
    func currentTimeisBetweenFromSelectedDate(startTime:String,endTime:String,selectedTime:Date)-> Bool{
        
        return selectedTime.currentTimeisBetween(startTime: startTime, endTime: endTime)
    }
    
    func currentTimeInMiliseconds() -> Int64 {
       let since1970 = self.timeIntervalSince1970
       return Int64(since1970 * 1000)
    } 
    
    func findDateDiff(time1Str: String, time2Str: String, isSameDay:Bool = true) -> (TimeInterval,Int) {
//        let timeformatter = DateFormatter()
//        timeformatter.dateFormat = Date.formatHHmmss
//
//        let time2formatter = DateFormatter()
//        time2formatter.dateFormat = Date.formatHHmm

        guard let time1 = DateUtils.shared.getDate(dateFormat: Date.formatHHmmss, dateString: time1Str),
            var time2 = DateUtils.shared.getDate(dateFormat: Date.formatHHmm, dateString: time2Str) else { return (0,0) }

        //You can directly use from here if you have two dates

        if(!isSameDay)
        {
            time2  = Calendar.current.date(byAdding: .day, value: 1, to: time2)!
            print("nextDate : \(time2)")
        }
        let interval = time2.timeIntervalSince(time1)
        //let hour = interval / 3600;
        let minute = interval.truncatingRemainder(dividingBy: 3600) / 60
        
       // let intervalInt = Int(interval)
        return (interval,Int(minute))
    }
    
    var startOfDay: Date {
        return Calendar.current.startOfDay(for: self)
    }

    var endOfDay: Date {
        var components = DateComponents()
        components.day = 1
        components.second = -1
        return Calendar.current.date(byAdding: components, to: startOfDay)!
    }

    var startOfMonth: Date {
        let components = Calendar.current.dateComponents([.year, .month], from: startOfDay)
        return Calendar.current.date(from: components)!
    }

    var endOfMonth: Date {
        var components = DateComponents()
        components.month = 1
        components.second = -1
        return Calendar.current.date(byAdding: components, to: startOfMonth)!
    }
    
    func dateToStringWithDay() -> String {
        return DateUtils.shared.getTimeDisplay(dateFormat: Date.formatEdMMMyyyy, date: self)
    }
    
    func getTimeFromDate() -> String{
        return DateUtils.shared.getTimeDisplay(dateFormat: Date.formathhmma, date: self)
    }
    
    func getTimeFromPassedDate(date:Date) -> String {
        return DateUtils.shared.getTimeDisplay(dateFormat: Date.formatHHmm, date: date)
    }
    
    var getTimeAMPM: String {
        return DateUtils.shared.getTimeDisplay(dateFormat: "hh:mma", date: self)
    }
    
    public func toString() -> String {
        return DateUtils.shared.getTimeDisplay(dateFormat: Date.formatYYYYMMddHHmmss, date: self)
    }
    
    public func getMonthName(date: Date)-> String{
        return DateUtils.shared.getTimeDisplay(dateFormat: Date.formatLLLL, date: date)
    }
    
    /// Returns the amount of hours from another date
    func hours(from date: Date) -> Int {
        return Calendar.current.dateComponents([.hour], from: date, to: self).hour ?? 0
    }
    
    func minutes(from date: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
    }
    
//    static let formatter: DateFormatter = {
//        let formatter = DateFormatter()
//        formatter.dateFormat = Date.formatEEEEddMMYYYYHHmmssZ
//        return formatter
//    }()
    
    var formatted: String {
        return DateUtils.shared.getTimeDisplay(dateFormat: Date.formatEEEEddMMYYYYHHmmssZ, date: self)
    }
    
    static func getDate(str:String) -> Date {
        return DateUtils.shared.getDate(dateFormat: Date.formatEEEEddMMYYYYHHmmssZ, dateString: str) ?? Date()
    }
}

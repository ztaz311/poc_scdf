//
//  Images.swift
//  Breeze
//
//  Created by VishnuKanth on 05/12/20.
//

import Foundation
import UIKit

struct Images {
    
    static let accountCreatedImage = UIImage(named: "accntCreatd")!
    static let passwordChangeSuccess = UIImage(named:"passwordchangedsuccessfully")!
    static let locationImage = UIImage(named: "locationBtn")
    static let menuImage = UIImage(named: "menu")
    static let searchIcon = UIImage(named: "search")
    static let searchPinIcon = UIImage(named: "searchPinIcon")
    static let savedPlaceIcon = UIImage(named: "bookmark")
    static let closeMenuImage = UIImage(named: "closeMenu")
    static let feedbackImage = UIImage(named: "feedbackIcon")
    static let tileListImage = UIImage(named: "tileListIcon")
    static let muteImage = UIImage(named: "mute")
    static let unmuteImage = UIImage(named: "unmute")
    static let liveLocationPauseImage = UIImage(named: "liveLocationPause")
    static let liveLocationResumeImage = UIImage(named: "liveLocationResume")
    static let cruiseLiveLocationPauseImage = UIImage(named: "cruiseLocationPause")
    static let cruiseLiveLocationResumeImage = UIImage(named: "cruiseLocationResume")
    static let feedbackBtn = UIImage(named: "newFeedback")
    static let toggleParkingBtn = UIImage(named: "parkingOff")
    static let toggleParkingBtnOn = UIImage(named: "parkingOn")
    static let travelSummariesSent = UIImage(named: "travelEmailSent")!
    static let progressWalkathonImage = UIImage(named: "walkathonButton")
    static let evChargerOnImage = UIImage(named: "ev_charger_icon_selected")
    static let evChargerOffImage = UIImage(named: "ev_charger_icon")
    static let petrolOnImage = UIImage(named: "petrol_icon_selected")
    static let petrolOffImage = UIImage(named: "petrol_icon")
}

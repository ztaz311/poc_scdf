//
//  Strings.swift
//  Breeze
//
//  Created by VishnuKanth on 20/11/20.
//

import Foundation

struct Constants {
    static let termsAndConditions = "I agree to Breeze’s Terms of Service and Privacy Policy."
    //static let incorrectOTP = "Oops, incorrect OTP there. Let’s try again!"
    static let exceedOtpRetry = "Sorry, retry limit has exceeded, please try again after some time."
    static let exceedOtpLimit = "Sorry, resend limit has exceeded, please try again after some time."
    static let userNotFound = "Looks like you’re not a member. Join us!"
    static let existingAccount = "Email is used in another account, try another email?"
    static let passwordCriteria = "Did your password include all the criteria stated above?"
    static let usernameInvalid = "Username should only contain English alphabets."
    static let usernameIsRestricted = "This username has been taken, please choose another username."
    static let codeExpired = "Code expired"
    static let loginErroFor30Min = "Are you sure you have entered your phone number correctly?"
    static let oldPwdError = "Oops wrong password, try entering password again?"
    static let samePasswordError = "The new password is the same as the old one, please enter a different password."
    static let okay = "Okay"
    static let yes = "Yes"
    static let updateRoute = "Update route"
    static let cancel = "Cancel"
    static let gotIt = "Got it"
    static let accountCreatedSuccess = "\n\nYour account has been successfully setup!"
    static let passwordChangeSuccess = "\n\nYour password has been changed successfully"
    static let hi = "Hello there,"
    static let goodJob = "Good Job!"
    static let invalidEmail = "Email seems invalid, try entering email again?"
    static let invalidCredentials = "Are you sure you have entered your email or password correctly?"
    static let BreezeCalendarPermission = "Breeze needs permission to access your calendar in order to work"
    static let goToSettings = "Go to Settings"
    static let searchClearMessage = "Search list cleared cannot be recovered. Are you sure you want to clear all searches?"
    static let clearAll = "Clear all"
    static let emptyFiels = "You must have forgotten to fill your details in, let's try that."
    static let eBDuplicateNameMesage = "This name has been already saved in the list, please enter a new one."
    static let eBDuplicateNameInRoutePlanning = "This name has been already saved in the list."
    static let ebSaved = "Added to Favourites"
    static let ebRemoved = "Removed from Favourites"
    static let noInternetTitle = "No internet connection"
    static let noInternetMessage = "Please check your internet connection and try again."
    static let userAlreadySignIn = "There is already a user which is signed in. Please log out the user before calling showSignIn."
    
    static let ncsIBReportBugString = "Tell us about issues you found"
    static let ncsIBReportBugDescriptionString = "Something in the app is broken or doesn’t work as expected"
    static let ncsIBFeedbackString = "Tell us your suggestions to improve"
    static let ncsIBFeedbackDescriptionString = "New ideas or desired enhancements for this app"
    
    static let ncsIBAskQuestionString = "Issue on Sign up/Log in"
    static let ncsIBAskQuestionDescriptionString = "The good, the bad, and the friendliness"
    
    static let ncsIBCommentFieldString = "Please be as detailed as possible.  What did you expect and what happened instead? \n\nYou may upload up to 3 photos, total 16mb."
    
    struct Layer {
        static let dengueZone = "dengue-clusters"
        static let speedCameraLayer = "speedcameras"
        static let illegalparkingcamera = "illegalparkingcamera"
        static let redlightcamera = "redlightcamera"
        static let speedcamera = "speedcamera"
#if TESTING
        static let schoolZone = "schoolzone (1)"
        static let silverZone = "silverzoneTest"
#else
        static let schoolZone = "schoolzone"
        static let silverZone = "silverzone"
#endif
        
    }
    struct Map {
        static let testURL = "mapbox://styles/breezemap/ckme4i6mpmgi517l9fq3xfmyb"
        
        static let styleUrl = "mapbox://styles/breezemap/cl8vbyjmi000414pdy7du8x8f"
        static let nightStyleUrl = "mapbox://styles/breezemap/cl8vbz699001l15oy192x9rbw"
        
        static let basicStyle = "mapbox://styles/breezemap/cl9p2xpr2000914ps57mciobs"
        static let basicNightStyle = "mapbox://styles/breezemap/cl9p2vigc000314qvimjnw6hk"
        
        static let walkathonStyleUrl = "mapbox://styles/breezemap/cl9zdudyd001714ru5k8ehug7"
        static let walkathonNightStyleUrl = "mapbox://styles/breezemap/cl9z8s1w5001a14o4xp5nt1wl"
        
        static let maxPitch = 60
        static let minZoomLevel = 10        
    }
    static let gradientName = "gradientName"
    
    static let easyBreezyDeleteMessage = "Are you sure you want to remove this favourite location?"
    static let remove = "Remove"
    
    static let ebMaxErrorMessage = "You have reached your Favourite places limit."
    
    static let newEBtitle = "New favourite"
    static let editEBtitle = "Edit favourite"
    
    static let logoutConfirmationMessage = "Are you sure you want to sign out?"
    static let logout = "Sign out"
    
    static let endNavigationMessage = "Are you sure you want to end the navigation?"
    static let end = "End"

    static let calendarRerouteNavigationMessage = "The venue of your event on calendar has been changed to"
    static let reroute = "Reroute"
    static let reject = "Reject"

    // MARK: - toast
    static let toastStartCruise = "Cruise mode"
    static let toastInTunnel = "Weak GPS signal in tunnel"
    static let toastNearestCarpark = "This is the nearest car park that can be found."
    static let toastNearestCarparkCruise = "There are no carparks near your current location."
    static let toastTripSave = """
            <div style="color: #222638;font-family:'SFProDisplay-Regular'; font-size:16;">You will receive a notification  <span style="font-family:'SFProDisplay-Semibold'; font-size:16;">30 min</span> before your trip starts.</div>
            """
    static let toastTripSave_dark = """
            <div style="color: #FFFFFF;font-family:'SFProDisplay-Regular'; font-size:16;">You will receive a notification  <span style="font-family:'SFProDisplay-Semibold'; font-size:16;">30 min</span> before your trip starts.</div>
            """

    static let toastEnableNotification = "Please enable notification to receive set-off alerts."

    static let toastLiveLocationPaused = "Live location sharing on pause"
    static let toastLiveLocationResumed = "Live location sharing resumed"
    
    // MARK: - ETA Reminder
    static let etaReminder15 = "[Auto-generated message]\nThe driver will be\nreaching in 15 min time."
    static let etaReminder1 = "[Auto-generated message]\nThe driver is around the corner!"

    static let contactSelectError = "Please select a contact with a valid phone number"
    static let mobileNumberErrorMsg = "The number entered is incorrect. Please enter a valid mobile number."
    static let mobileNumberInUse = "This number is already in use. Please use a new number or proceed to sign in."
    static let otpHeaderMsg = "Enter the code sent to mobile number ending with -"
    static let incorrectOTP = "Incorrect code. You may try "
    static let otpMoreTimes = " more times"
    static let otpMoreTime = " more time"
    static let otpTriedError = "You’ve tried too many times. Please try again later."
    static let tempPassword = "Breeze@45678"
    
    //MARK:- Account Strings
    static let emailNotAddedMsg = "Add email for exporting triplog data"
    static let emailRequired = "Your email address is required to receive updates and travel logs."
    static let emailLinkSent = "A verification link has been sent to the email above. Please verify your email account."
    static let emailVerifySuccess = "Your email has been saved."
    
    //MARK:- RouteTypes
    static let fast = "Fastest"
    static let cheap = "Cheapest"
    static let short = "Shortest"
        
    //MARK: - DEMO
    static let enteredParkingZone = "Entered parking zone"
    static let parkingStarted = "Session started"
    static let parkingEnded = "Session ended, charged"
    
    static let parkingEndedVoiceMsg = "Parking session ended, charged"
    static let parkingStartedMsg = "Your parking session has started"
    static let pbVoiceMessage = "5 min to Faber park, 8 min to Sentosa"
    static let carParkLowAnnouncement = "The car park you're heading to is almost full. View other parking options nearby."
    static let lowCardVoiceMeasage = "Low Card Balance. Remember to top up cash card soon"

    //MARK: - Carplay Search Menu
    static let cpNewSearch = "New Search"
    static let cpFavourite = "Favourites"
    static let cpRecentSearch = "Recent Searches"
    static let cpAddDestination = "Add Destination"
  
    
    //MARK: - Carplay Toggle Menu
    static let cpCarParkTitle = "Carparks"
    static let cpNavAlert = "Close"
    static let cpNavEndAlert = "End"
    static let cpCarParkHideTitle = "Back"
    static let cpNotifyArrivalTitle = "Notify"
    static let cpStartObu = "OBU"
    static let cpStartCruise = "Cruise"
    static let cpEndCruise = "End"
    static let cpEndNav = "End"
    static let cpPbMessage = "Public message"
    static let cpArrival = "You have arrived!"
    static let cpNotifyListTitle = "Saved Notify Arrival List"
    static let cpMute = "Mute"
    static let cpUnmute = "Unmute"
    static let cpETAPause = "Pause Sharing"
    static let cpETAResume = "Resume Sharing"
    static let cpNotifyToast = "There are no existing Notify ETA"
    static let cpLoginMsg = "Please login to Breeze on your mobile phone"
    static let cpShareDrive = "Share Drive"
    static let cpCancelShareDrive = "Cancel Sharing"
    static let cpShortcutMsg = "You have no Shortcuts, please add your shortcut addresses using the Breeze app."
    static let cpShortcutRecentSreachMsg = "You have no recent searches."
    static let cpShareDestination = "Share Destination"
    
    // MARK: - CarPlay list title
    static let cpRecentContactTitle = "Select from Recent Contacts"
    static let cpShareDriveConfirmationTitle = "Share Drive"
    static let cpCancelSharingConfirmationTitle = "Cancel Sharing"    
    
    static let cpNoContactAlert = "You’ve not added any contacts. Use Breeze on your phone to add contacts when it’s safe to do so."
    static let cancelShareDriveConfirmation = "Are you sure you want to cancel your sharing drive with"
    static let cpObuDisplayMessage = "OBU Lite display is only available on Breeze mobile app."
    
    //MARK: - App Update
    static let updateTitle = "Software Update Required"
    static let softwareUpdateTitle = "Software Updated"
    static let mandatoryUpdateMessage = "An update is required to continue. Please download the latest version and install now."
    static let appIsLatestMessage = "You now have the latest version of Breeze!."
    static let mandatoryUpdateActionTitle = "Update Now"
    
    static let optionalUpdateMessage = "A new version with the latest feature is available now."
    static let optionalUpdateActionLater = "Later"
    static let optionalUpdateActionDismiss = "Dismiss"
    static let optionaUpdateActionInstall = "Install now"
    
    // MARK: - Route Planning
    static let differentStartLocationMessage = "You are not at your saved location. Would you like to start navigating from your current location instead?"
    static let ncsNotFoundRouteLocation = "There isn't a driving route to your location."
    
    // MARK: - Route Preference strings
    
    static let FASTEST_SHORTEST_CHEAPEST = "Fastest, Cheapest & Shortest"
    static let FASTEST_SHORTEST = "Fastest & Shortest"
    static let FASTEST_CHEAPEST = "Fastest & Cheapest"
    static let SHORTEST_CHEAPEST = "Shortest & Cheapest"
    static let FASTEST = "Fastest"
    static let SHORTEST = "Shortest"
    static let CHEAPEST = "Cheapest"
    
    // MARK: - Notification
    static let seasonParkingVoice = "Season Parking zone. Parking for residents only"
    
    // MARK: - OverSpeed
    static let overSpeedVoice = "Oops, slow down. You're over the speed limit."
    
    // MARK: - Walkathon Arrival
    static let walkathonSuccessTitle = "Successfully Arrived"
    static let walkathonSuccessMessage = "You have arrived at"
    static let walkathonSuccessBackTitle = "Back To Walkathon Map"
    
    
    // MARK: - CarPlay & Android Auto - Search, Recent, Home/Work*
    static let searchTitle = "Search"
    static let shortcutsTitle = "Shortcuts"
    static let homeTitle = "Home"
    static let workTitle = "Work"
    static let walkathonArrival = "Arrival"
    
    
    // MARK: - Saved Place - Deselect Tooltip bookmark (amendment)
    static let titleConfirmDeselect = "This action will remove the location from all collections where it appears. To only remove from selected collections, go to each collection and delete the location individually."
    static let confirmDeselect = "CONFIRM"
    static let cancelDeselect = "CANCEL"
    
    // MARK: Available carpark lots
    static let manyAvailableLots = "Many available lots"
    static let fewAvailableLots = "A few available lots"
    static let carparkFull = "Carpark is full"
    static let confirmAvailableLots = "Yes, thanks"
    static let exclaimationCarparkLots = "No, it’s not"
    
    // MARK: OBU
    static let timeDismiss = 5.0
}


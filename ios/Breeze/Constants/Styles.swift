//
//  Styles.swift
//  Breeze
//
//  Created by VishnuKanth on 27/11/20.
//

import Foundation
import UIKit

struct fontFamilyQuickSand {
  static let QuickSandBold = "Quicksand-Light_Bold"
  static let QuickSandRegular = "Quicksand-Light_Regular"
  static let QuickSandMedium = "Quicksand-Light_Medium"
}

struct fontFamilySFPro {
    static let MaxBold = "SFProDisplay-Bold"
    static let Bold = "SFProDisplay-Semibold"
    static let Regular = "SFProDisplay-Regular"
    static let Light = "SFProDisplay-Light"
    static let Medium = "SFProDisplay-Medium"
    
    static let BoldI = "SFProDisplay-SemiboldItalic"
}

struct fontFamilySFProFromStudio {
    static let Bold = "SF Pro Display Semibold"
    static let Regular = "SF Pro Display Regular"
    static let Light = "SF Pro Display Light"
    static let Medium = "SF Pro Display Medium"
}

extension UIFont {
    
    class func buttonTextBoldFont() -> UIFont {
        return UIFont(name: fontFamilyQuickSand.QuickSandBold, size: 20.0)!
        
    }
    
    // MARK: - Auth font
    
    class func buttonAuthSemiBoldFont() -> UIFont {
        return UIFont(name: fontFamilySFPro.Bold, size: 16.0)!
    }
    
    // MARK: - Notification font
    
    //MARK: - Cruise mode font
    class func roadNameBoldFont() -> UIFont {
        return UIFont(name: fontFamilySFPro.Bold, size: 16.0)!
    }

    class func cruiseEndButtonFont() -> UIFont {
        return UIFont(name: fontFamilySFPro.Bold, size: 18.0)!
    }

    class func cruiseToastRegularFont() -> UIFont {
        return UIFont(name: fontFamilySFPro.Regular, size: 18.0)!
    }

    class func cruiseCarPlayToastRegularFont() -> UIFont {
        return UIFont(name: fontFamilySFPro.Regular, size: 12.0)!
    }
    
    class func titleStepObuConnected() -> UIFont {
        return UIFont(name: fontFamilySFPro.Regular, size: 10.0)!
    }

    class func cruiseNotificationDistanceFont() -> UIFont {
        return UIFont(name: fontFamilySFPro.Bold, size: 28)!
    }

    class func cruiseNotificationTypeFont() -> UIFont {
        return UIFont(name: fontFamilySFPro.Medium, size: 24.0)!
    }

    class func cruiseNotificationRoadFont() -> UIFont {
        return UIFont(name: fontFamilySFPro.Regular, size: 16.0)!
    }

    class func easyBreeziesTagFont() -> UIFont {
        return UIFont(name: fontFamilySFPro.Regular, size: 16.0)!
    }

    class func quickSandRegular26() -> UIFont {
        return UIFont(name: fontFamilyQuickSand.QuickSandRegular, size: 26.0)!
        
    }
    
    class func quickSandBold26() -> UIFont {
        return UIFont(name: fontFamilyQuickSand.QuickSandBold, size: 26.0)!
    }

    class func quickSandBold24() -> UIFont {
        return UIFont(name: fontFamilyQuickSand.QuickSandBold, size: 26.0)!
    }
    
    class func broadcastMessageTitleFont() -> UIFont {
        return UIFont(name: fontFamilySFPro.Bold, size: 24.0)!
    }
    
    class func broadcastMessageSubtitleFont() -> UIFont {
        return UIFont(name: fontFamilySFPro.Medium, size: 20.0)!
    }
    
    class func obuCommonTitleFont() -> UIFont {
        return UIFont(name: fontFamilySFPro.Bold, size: 24.0)!
    }
    
    class func obuCommonSubTitleFont() -> UIFont {
        return UIFont(name: fontFamilySFPro.Medium, size: 20.0)!
    }
    
    class func obuChargeAmountFont() -> UIFont {
        return UIFont(name: fontFamilySFPro.Medium, size: 22.0)!
    }
    
    class func obuCommonSubTitleFont1() -> UIFont {
        return UIFont(name: fontFamilySFPro.Medium, size: 18.0)!
    }
    

    // MARK: - Search Bar Text Font
    class func searchBarTextFont() -> UIFont {
        return UIFont.systemFont(ofSize: 18.0, weight: .regular)
    }

    class func tripLogSearchBarTextFont() -> UIFont {
        return UIFont(name: fontFamilySFPro.Regular, size: 18.0)!
    }
    
    class func tripLogSegmentSelectedTextFont() -> UIFont {
        return UIFont(name: fontFamilySFPro.Medium, size: 16.0)!
    }

    class func tripLogSegmentTextFont() -> UIFont {
        return UIFont(name: fontFamilySFPro.Regular, size: 16.0)!
    }

    // MARK: - Map Top Banner Font style
    class func topBannerPrimaryTextFont() -> UIFont {
        return UIFont.systemFont(ofSize: 28.0, weight: .bold)
    }
    
    class func topBannerDistanceTextFont() -> UIFont {
        return UIFont.systemFont(ofSize: 36.0, weight: .regular)
    }
    
    
    // MARK: - Map Bottom Banner Font style
    class func bottomBannerHeaderTextFont() -> UIFont {
        return UIFont.systemFont(ofSize: 16.0, weight: .light)
    }
    
    class func bottomBannerTextFont() -> UIFont {
        return UIFont.systemFont(ofSize: 24.0, weight: .medium)
    }
    
    // MARK: - Navigation instruction
    class func SFProRegularFont() -> UIFont { // size doesn't matter since I will change later
        return UIFont(name: fontFamilySFPro.Regular, size: 16.0)!
    }

    class func SFProBoldFont32() -> UIFont { // size doesn't matter since I will change later
        return UIFont(name: fontFamilySFPro.Bold, size: 32.0)!
    }

    class func SFProBoldFont(size: Double = 16) -> UIFont {
        return UIFont(name: fontFamilySFPro.Bold, size: size)!
    }

    class func navigationRoadNameFont() -> UIFont {
        return UIFont(name: fontFamilyQuickSand.QuickSandBold, size: 30.0)!
    }
    
    class func instructionRegular() -> UIFont {
        return UIFont(name: fontFamilyQuickSand.QuickSandBold, size: 24.0)!
    }

    class func instructionCompact() -> UIFont {
        return UIFont(name: fontFamilyQuickSand.QuickSandBold, size: 20.0)!
    }
    
    class func instructionRateFont() -> UIFont {
        return UIFont(name: fontFamilySFPro.Bold, size: 22.0)!
    }
    
    // MARK: Route planning
    class func routePlanningTitleBoldFont() -> UIFont {
        return UIFont(name: fontFamilySFPro.Bold, size: 14.0)!
    }

    class func routePlanningAddressFont() -> UIFont {
        return UIFont(name: fontFamilySFPro.Regular, size: 12.0)!
    }
    
    // MARK: Car park
    class func carparkTypeFont() -> UIFont {
        return UIFont(name: fontFamilySFPro.Medium, size: 14.0)!
    }

    class func carparkTitleFont() -> UIFont {
        return UIFont(name: fontFamilySFPro.Bold, size: 16.0)!
    }

    class func carparkButtonFont() -> UIFont {
        return UIFont(name: fontFamilySFPro.Medium, size: 12.0)!
    }

    class func carparkNavigateButtonFont() -> UIFont {
        return UIFont(name: fontFamilySFPro.Bold, size: 20.0)!
    }

    class func availableCarparkLotsLabelFont() -> UIFont {
        return UIFont(name: fontFamilySFPro.Regular, size: 14.0)!
    }

    class func availableCarparkLotsFont() -> UIFont {
        return UIFont(name: fontFamilySFPro.Bold, size: 14.0)!
    }

    class func totalCarparkLotsFont() -> UIFont {
        return UIFont(name: fontFamilySFPro.Light, size: 16.0)!
    }

    // reroute prompt
    class func rerouteTitleRegularFont() -> UIFont {
        return UIFont(name: fontFamilySFPro.Regular, size: 18.0)!
    }

    class func rerouteMessageBoldFont() -> UIFont {
        return UIFont(name: fontFamilySFPro.Bold, size: 18.0)!
    }
    
    class func tabBarFont() -> UIFont {
        return UIFont(name: fontFamilySFPro.Regular, size: 12.0)!
    }

    // MARK: - Tips
    class func tipsTextFont() -> UIFont {
        return UIFont(name: fontFamilySFPro.Regular, size: 20.0)!
    }
    
    // MARK: - Parking Available
    class func parkingAvailableTextFont() -> UIFont {
        return UIFont(name: fontFamilySFPro.Medium, size: 14.0)!
    }
    
    class func parkingAvailableUpdateTimeFont() -> UIFont {
        return UIFont(name: fontFamilySFPro.Medium, size: 9.0)!
    }

    
    // MARK: - Amenity Tooltips
    class func amenityToolTipsTextFont() -> UIFont {
        return UIFont(name: fontFamilySFPro.Bold, size: 14.0)!
    }

    class func amenityToolTipsAddressFont() -> UIFont {
        return UIFont(name: fontFamilySFPro.Regular, size: 14.0)!
    }
    
    class func amenityToolTipsBiggerTextFont() -> UIFont {
        return UIFont(name: fontFamilySFPro.Bold, size: 20.0)!
    }

    class func amenityToolTipsAddressBiggerFont() -> UIFont {
        return UIFont(name: fontFamilySFPro.Regular, size: 18.0)!
    }
    
    class func amenityToolTipsDesFont() -> UIFont {
        return UIFont(name: fontFamilySFPro.BoldI, size: 14.0)!
    }
    
    class func amenityToolTipsERPTextFont() -> UIFont {
        return UIFont(name: fontFamilySFPro.Bold, size: 14.0)!
    }
    
    class func nudgeToolTipsTextFont() -> UIFont {
        return UIFont(name: fontFamilySFPro.Medium, size: 16.0)!
    }

    class func nudgeToolTipsAddressFont() -> UIFont {
        return UIFont(name: fontFamilySFPro.Light, size: 16.0)!
    }
    
    class func nudgeToolTipsProgressFont() -> UIFont {
        return UIFont(name: fontFamilySFPro.Medium, size: 14.0)!
    }

    // MARK: - Route planning toast
    class func toastTitleTextFont() -> UIFont {
        return UIFont(name: fontFamilySFPro.Bold, size: 20.0)!
    }

    class func toastMessageTextFont() -> UIFont {
        return UIFont(name: fontFamilySFPro.Regular, size: 16.0)!
    }

    // MARK: - Date Picker
    class func datePickerDepartAtTextFont() -> UIFont {
        return UIFont(name: fontFamilySFPro.Medium, size: 22.0)!
    }
    
    class func datePickerBottomButtonsTextFont() -> UIFont {
        return UIFont(name: fontFamilySFPro.Medium, size: 20.0)!
    }

    // MARK: - RoutePreference Font size
    class func routePreferenceFont() -> UIFont {
        return UIFont(name: fontFamilySFPro.Medium, size: 15.0)!
    }

    // MARK: - CarPlay
    // MARK: cruise mode
    class func carPlayRoadNameBoldFont() -> UIFont {
        return UIFont(name: fontFamilyQuickSand.QuickSandBold, size: 12.0)!
    }
    
    
    // MARK: - Webview
    class func buttonWebivewTitleTextFont() -> UIFont {
        return UIFont(name: fontFamilySFPro.Bold, size: 20.0)!
    }
    
    //MARK: - Saved collection
    class func fontWithName(_ name: String, size: CGFloat) -> UIFont {
        return UIFont(name: name, size: size)!
    }

}

extension UIColor {
    
    class func rgba(r: Int, g: Int, b: Int, a: Float) -> UIColor {
        return UIColor(red: CGFloat(r)/255, green: CGFloat(g)/255, blue: CGFloat(b)/255, alpha: CGFloat(a))
        }
    
      
    //MARK: - Brand Purple Color
    class var brandPurpleColor:UIColor { get { return self.rgba(r: 120, g: 46, b: 177, a: 1.0)}}
    class var passionPinkColor:UIColor { get { return self.rgba(r: 232, g: 35, b: 112, a: 1.0)}}
    
    
    //MARK: - Brand Orange Color
    class var brandOrangeColor:UIColor { get { return self.rgba(r: 232, g: 97, b: 0, a: 1.0)}}
    
    //MARK: - View Background Transparent Color
    class var viewTransparentBackground: UIColor { get { return self.rgba(r: 34, g: 38, b: 56, a: 1.0)} }
   
    //MARK: - Gradient Colors
    class var defaultOrangeGradientColor1: UIColor { get { return UIColor(named: "orangeGradientColor1")! } }
    class var defaultOrangeGradientColor2: UIColor { get { return UIColor(named: "orangeGradientColor2")! } }
    
    class var defaultGreyGradientColor1: UIColor { get { return self.rgba(r: 203, g: 203, b: 203, a: 1.0)} }
    class var defaultGreyGradientColor2: UIColor { get { return self.rgba(r: 175, g: 175, b: 175, a: 1.0)} }
    
    //MARK: - View Gradient Colors
    class var gradient3: UIColor { get { return self.rgba(r: 250, g: 173, b: 99, a: 1.0)} }
    class var gradient2: UIColor { get { return self.rgba(r: 253, g: 151, b: 53, a: 1.0)} }
    class var gradient1: UIColor { get { return self.rgba(r: 255, g: 138, b: 25, a: 1.0)} }
    
    //MARK: Shadow Color
    class var shadowColor: UIColor { get { return self.rgba(r: 40, g: 86, b: 93, a: 0.15)} }
    // MARK: - Top Banner Text Colors
    class var topBannerPrimaryTextColor: UIColor { get { return #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1) } }
    class var topBannerDistanceTextColor: UIColor { get { return #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1) } }
        
    // MARK: - Bottom Banner Header Text Colors
    class var bottomBannerArrivalHeaderTextColor: UIColor { get { return self.rgba(r: 17, g: 20, b: 33, a: 1.0)} }
    class var bottomBannerTimeLeftHeaderTextColor: UIColor { get { return self.rgba(r: 17, g: 20, b: 33, a: 1.0)} }
    class var bottomBannerDistanceHeaderTextColor: UIColor { get { return self.rgba(r: 17, g: 20, b: 33, a: 1.0)} }
    
    // MARK: - Bottom Banner Value Text Colors
    class var bottomBannerArrivalValueColor: UIColor { get { return self.rgba(r: 17, g: 20, b: 33, a: 1.0)} }
    class var bottomBannerTimeLeftValueColor: UIColor { get { return self.rgba(r: 17, g: 20, b: 33, a: 1.0)} }
    class var bottomBannerDistanceValueColor: UIColor { get { return self.rgba(r: 17, g: 20, b: 33, a: 1.0)} }
    
    //MARK: - Search Bar Text Color
    class var searchBarTextColor: UIColor { get { return UIColor(named: "searchBarTextColor")!} }
    
    //MARK: - TripLog segment text color
    class var tripLogSegementNormalColor:UIColor { get { return self.rgba(r: 119, g: 129, b: 136, a: 1.0)}}
    class var tripLogSegementSelectedColor:UIColor { get { return self.rgba(r: 34, g: 38, b: 56, a: 1.0)}}
    
    //MARK: - Search Bar TextField background Color
    class var searchBarTextFieldBackground: UIColor { get { return UIColor(named: "searchBoxBackgroundColor")!} }
    
    //MARK: - AlertController action button color
    class var clearSearchAlertButtonColor: UIColor { get { return self.rgba(r: 255, g: 44, b: 44, a: 1.0)} }

    //MARK: - SearchView Gradient Colors
    class var searchViewColor1: UIColor { get { return UIColor(named: "searchViewColor1")!} }
    class var searchViewColor2: UIColor { get { return UIColor(named: "searchViewColor2")!} }

    // MARK: - Map landing
    class var mapLandingBGColor: UIColor { get { return UIColor(named: "mapLandingBackgroundColor")!} }

    //MARK: - Menu Gradient Colors
//    class var mapLandingBottomGradientColor1: UIColor { get { return UIColor(named: "mapLandingBottomGradientColor1")!} }
//    class var mapLandingBottomGradientColor2: UIColor { get { return UIColor(named: "mapLandingBottomGradientColor2")!} }

    //MARK: - Menu Gradient Colors
    class var menuGradientColor1: UIColor { get { return UIColor(named: "menuPanelColor1")!} }
    class var menuGradientColor2: UIColor { get { return UIColor(named: "menuPanelColor2")!} }
    
    // MARK: - Easy Breezy tagView color
    class var easyBreezyTagBorderColor: UIColor { get { return self.rgba(r: 119, g: 129, b: 136, a: 1.0)} }
    class var easyBreezyTagBackgroundColor: UIColor { get { return UIColor(named: "tagListBackgroundColor")!} }
    class var easyBreezyTagFontColor: UIColor { get { return UIColor(named: "tagFontColor")!} }
    
    class var easyBreezyDeleteColor: UIColor { get { return self.rgba(r: 253, g: 45, b: 85, a: 1.0)} }
    class var easyBreezyEditColor: UIColor { get { return self.rgba(r: 240, g: 143, b: 60, a: 1.0)} }
    
    // MARK: - Splash view
    class var ncsViewColor1: UIColor { get { return self.rgba(r: 18, g: 48, b: 130, a: 1.0)} }
    class var ncsViewColor2: UIColor { get { return self.rgba(r: 12, g: 35, b: 101, a: 1.0)} }
    
    //MARK: - Button disable colors
    class var buttonDisableColor: UIColor {
        get {
            return self.rgba(r: 119, g: 129, b: 136, a: 1.0)
        }
    }

    //MARK: - Cruise mode notification colors
    class var cruiseNotificationColor1: UIColor {
        get {
            return self.rgba(r: 48, g: 52, b: 74, a: 1.0)
        }
    }
    class var cruiseNotificationColor2: UIColor {
        get {
            return self.rgba(r: 34, g: 38, b: 56, a: 1.0)
        }
    }
    class var cruiseNotificationColor3: UIColor {
        get {
            return self.rgba(r: 17, g: 20, b: 33, a: 1.0)
        }
    }
    
    class var cruiseUserLocationColor: UIColor {
        get {
            return self.rgba(r: 85, g: 133, b: 227, a: 1.0)
        }
    }
    
    class var cruiseRoadNameTextColor: UIColor {
        get {
            return self.rgba(r: 57, g: 63, b: 88, a: 1.0)
        }
    }

    class var cruiseRoadNameColor1: UIColor {
        get {
            return self.rgba(r: 72, g: 133, b: 234, a: 1.0)
        }
    }

    class var cruiseRoadNameColor2: UIColor {
        get {
            return self.rgba(r: 0, g: 95, b: 208, a: 1.0)
        }
    }
    
//    class var navigationRoadNameColor: UIColor {
//        get {
//            return self.rgba(r: 120, g: 46, b: 177, a: 1.0)
//        }
//    }

//    class var puckColor: UIColor {
//        get {
//            return self.rgba(r: 72, g: 133, b: 234, a: 1.0)
//        }
//    }

    //MARK: - Turn by Turn Navigation Colors
    class var navigationBGColor: UIColor {
        get {
            return self.rgba(r: 17, g: 20, b: 33, a: 1)
        }
    }

    class var navigationNotificationBGColor: UIColor {
        get {
            return self.rgba(r: 80, g: 83, b: 97, a: 0.93)
        }
    }

    class var routeAlternateColor: UIColor {
        get {
            return self.rgba(r: 162, g: 193, b: 244, a: 1)
        }
    }
    
    // MARK: - Traffic colors / alternate traffic colors for route planning/navigation
    class var routeAlternateCasingLight: UIColor {
        get {
            return self.rgba(r: 167, g: 57, b: 251, a: 0.62)
        }
    }
    
    class var routeAlternateCasingDark: UIColor {
        get {
            return self.rgba(r: 156, g: 71, b: 244, a: 0.4)
        }
    }
    
    class var routeCasingColorLight: UIColor {
        get {
            return self.rgba(r: 120, g: 46, b: 177, a: 1)
        }
    }
    
    class var routeCasingColorDark: UIColor {
        get {
            return self.rgba(r: 177, g: 85, b: 248, a: 1)
        }
    }
    
    // MARK: Alternate traffice colors
    class var alternateTrafficLowColor: UIColor {
        get {
            return self.rgba(r: 167, g: 57, b: 251, a: 0.62)
        }
    }

    class var alternateTrafficLowColorDark: UIColor {
        get {
            return self.rgba(r: 156, g: 71, b: 224, a: 0.8)
        }
    }

    class var alternatTrafficModerateColor: UIColor {
        get {
            return self.rgba(r: 255, g: 138, b: 25, a: 0.62)
        }
    }

    class var alternatTrafficModerateColorDark: UIColor {
        get {
            return self.rgba(r: 255, g: 150, b: 51, a: 0.8)
        }
    }

    class var alternateTrafficHeavyColor: UIColor {
        get {
            return self.rgba(r: 232, g: 35, b: 112, a: 0.62)
        }
    }

    class var alternateTrafficHeavyColorDark: UIColor {
        get {
            return self.rgba(r: 234, g: 52, b: 122, a: 0.8)
        }
    }

    class var alternateTrafficSevereColor: UIColor {
        get {
            return self.rgba(r: 232, g: 35, b: 112, a: 0.62)
        }
    }

    class var alternateTrafficSevereColorDark: UIColor {
        get {
            return self.rgba(r: 234, g: 52, b: 122, a: 0.8)
        }
    }

    class var alternateTrafficUnknownColor: UIColor {
        get {
            return self.rgba(r: 167, g: 57, b: 251, a: 0.62)
        }
    }

    class var alternateTrafficUnknownColorDark: UIColor {
        get {
            return self.rgba(r: 156, g: 71, b: 224, a: 0.8)
        }
    }

    // MARK: Traffice colors
    class var trafficLowColor: UIColor {
        get {
            UIColor.rgba(r: 120, g: 46, b: 177, a: 1)
        }
    }
    
    class var trafficLowColorDark: UIColor {
        get {
            return UIColor.rgba(r: 199, g: 125, b: 255, a: 1)
        }
    }

    class var trafficModerateColor: UIColor {
        get {
            return self.rgba(r: 255, g: 138, b: 25, a: 1)
        }
    }

    class var trafficModerateColorDark: UIColor {
        get {
            return self.rgba(r: 255, g: 150, b: 51, a: 1)
        }
    }

    class var trafficHeavyColor: UIColor {
        get {
            return self.rgba(r: 232, g: 35, b: 112, a: 1)
        }
    }

    class var trafficHeavyColorDark: UIColor {
        get {
            return self.rgba(r: 234, g: 52, b: 122, a: 1)
        }
    }

    class var trafficSevereColor: UIColor {
        get {
            return self.rgba(r: 232, g: 35, b: 112, a: 1)
        }
    }

    class var trafficSevereColorDark: UIColor {
        get {
            return self.rgba(r: 234, g: 52, b: 122, a: 1)
        }
    }

    class var trafficUnknownColor: UIColor {
        get {
            return UIColor.rgba(r: 120, g: 46, b: 177, a: 1)
        }
    }

    class var trafficUnknownColorDark: UIColor {
        get {
            return UIColor.rgba(r: 199, g: 125, b: 255, a: 1)
        }
    }

    class var instructionViewBackgroundColor1: UIColor {
        get {
            return self.rgba(r: 48, g: 52, b: 74, a: 1)
        }
    }

    class var instructionViewBackgroundColor2: UIColor {
        get {
            return self.rgba(r: 34, g: 38, b: 56, a: 1)
        }
    }

    class var instructionViewBackgroundColor3: UIColor {
        get {
            return self.rgba(r: 17, g: 20, b: 33, a: 1)
        }
    }
    
    class var routePlanningNonSelectedRouteTitleColor: UIColor {
        get {
            return self.rgba(r: 119, g: 129, b: 136, a: 1)
        }
    }
    
    class var routePlanningTitleColor: UIColor {
        get {
            return self.rgba(r: 34, g: 38, b: 56, a: 1)
        }
    }

    class var routePlanningAddressColor: UIColor {
        get {
            return self.rgba(r: 153, g: 153, b: 153, a: 1)
        }
    }

    //MARK: - Car park
    class var carparkNameColor: UIColor {
        get {
            return self.rgba(r: 16, g: 21, b: 31, a: 1)
        }
    }

    class var carparkLotsLabelColor: UIColor {
        get {
            return self.rgba(r: 34, g: 38, b: 56, a: 1)
        }
    }

    class var carparkResetTextColor: UIColor {
        get {
            return self.rgba(r: 5, g: 98, b: 210, a: 1)
        }
    }

    class var carparkResetBgColor1: UIColor {
        get {
            return UIColor(named: "resetCarpackBgColor1")!
        }
    }

    class var carparkResetBgColor2: UIColor {
        get {
            return UIColor(named: "resetCarpackBgColor2")!
        }
    }

    //MARK: - Navigation Gradient Colors
    class var navBottomBackgroundColor1: UIColor { get { return UIColor(named: "navBottomBackgroundColor1")!} }
    class var navBottomBackgroundColor2: UIColor { get { return UIColor(named: "navBottomBackgroundColor2")!} }
    class var navBottomButtonColor1: UIColor { get { return UIColor(named: "buttonColor1")!} }
    class var navBottomButtonColor2: UIColor { get { return UIColor(named: "buttonColor2")!} }
    
    class var endRouteBackgroundColor: UIColor { get { return UIColor(named: "endRouteBackgroundColor")!} }
   
    //MARK: - Status Bar Color
    class var statusBarNoInternetColor: UIColor {
        get {
            return self.rgba(r: 234, g: 78, b: 114, a: 1.0)
        }
    }
    
    //MARK: - Slider (Progress)
    class var minTrackColor: UIColor {
        get {
            return self.rgba(r: 229, g: 229, b: 229, a: 1.0)
        }
    }

    class var maxTrackColor: UIColor {
        get {
            return self.rgba(r: 89, g: 145, b: 236, a: 1.0)
        }
    }
    
    class var searchBorderColor: UIColor {
          get {
              return self.rgba(r: 201, g: 201, b: 206, a: 1.0)
          }
      }
    
    class var settingsTintColor: UIColor {
          get {
              return self.rgba(r: 237, g: 112, b: 21, a: 1.0)
          }
      }
    
    class var warningColor: UIColor {
          get {
              return self.rgba(r: 253, g: 59, b: 48, a: 1.0)
          }
      }
    
    class var liveLocationSharingPulseColor: UIColor {
          get {
            return self.rgba(r: 21, g: 183, b: 101, a: 0.8)
          }
      }

    // MARK: - Notification
    class var notificationERPBG: UIColor {
        get {
            return self.rgba(r: 0, g: 95, b: 208, a: 1)
        }
    }

    class var notificationTrafficBG: UIColor {
        get {
            return self.rgba(r: 247, g: 46, b: 105, a: 1)
        }
    }
    
    class var notificationETAExpressWay: UIColor {
        get {
            return self.rgba(r: 12, g: 156, b: 83, a: 1)
        }
    }

    class var notificationSchoolAndSilverZoneBG: UIColor {
        get {
            return self.rgba(r: 242, g: 100, b: 21, a: 1)
        }
    }
    
    class var notificationETABG: UIColor {
        get {
            return self.rgba(r: 120, g: 46, b: 177, a: 1)
        }
    }
    
    class var disableSelect: UIColor {
        get {
            return self.rgba(r: 143, g: 140, b: 140, a: 1)
        }
    }

    class var notificationCarparkSoonArrive: UIColor {
        get {
            return self.rgba(r: 0, g: 95, b: 208, a: 1)
        }
    }
}


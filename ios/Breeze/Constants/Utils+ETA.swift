//
//  Utils+ETA.swift
//  Breeze
//
//  Created by Zhou Hao on 27/5/22.
//

import UIKit

/// Show share drive screen from RN
/// Return child view controller
func showShareDriveScreen(viewController: UIViewController, address: String, etaTime: String, fromScreen: String, destination: AnalyticDestAddress? = nil) -> UIViewController {

//    let etaMessage = address.isEmpty ? "" : "{name} is on the way to {\(Values.etaLocation)} and will approximately arrive by {\(Values.etaETA)}."
    let etaMessage = address.isEmpty ? "" : "{name} shared a destination, {\(Values.etaLocation)} and will arrive at {\(Values.etaETA)}."
    
    var params: [String: Any] = [Values.etaMessage:etaMessage,
                                 Values.etaName: AWSAuth.sharedInstance.userName,
                                 Values.etaLocation: address,
                                 Values.etaFromScreen: fromScreen,
                                 Values.etaETA: etaTime]

    if let dest = destination {
        let data = ["address1": dest.address1 ?? "", "address2": dest.address2 ?? "", "latitude": (dest.lat ?? 0.0).toString(), "longitude": (dest.long ?? 0.0).toString(), "amenityId": dest.amenityId ?? "", "amenityType": dest.amenityType ?? "", "name": dest.address1 ?? "", "fullAddress": destination?.fullAddress ?? "", "arrivalTime": etaTime, "arrivalTimestamp": Int(Date().currentTimeInMiliseconds()), "type": "NAVIGATION"] as [String : Any]
        params[Values.etaData] = data
    }
    let vc = viewController.getRNViewBottomSheet(toScreen: "NotifyArrival", navigationParams: params)
    vc.view.frame = UIScreen.main.bounds
    viewController.addChildViewControllerWithView(childViewController: vc,toView: viewController.view)
    return vc
}


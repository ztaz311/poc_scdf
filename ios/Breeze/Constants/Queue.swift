//
//  Queue.swift
//  Breeze
//
//  Created by Zhou Hao on 4/6/22.
//

import Foundation

struct Queue<T> {
    
    private var items:[T] = []
    
    mutating func enqueue(element: T) {
        items.append(element)
    }
    
    mutating func dequeue() -> T? {
        if items.isEmpty {
            return nil
        } else {
            let tempElement = items.first
            items.remove(at: 0)
            return tempElement
        }
    }
    
    func count() -> Int {
        return items.count
    }
    
    mutating func clear() {
        if items.count > 0
        {
            items.removeAll()
        }
    }
    
    func hasInstruction(_ newAudio: AudioData) -> Bool {
        for item in items {
            if let audio = item as? AudioData {
                if audio.instruction.text == newAudio.instruction.text {
                    return true
                }
            }
        }
        return false
    }
    
}

//
//  Values.swift
//  Breeze
//
//  Created by Zhou Hao on 8/3/21.
//

import UIKit
import RxSwift

extension Notification.Name {
    
    // MARK: Observing update parking availability
    public static let yesParkingAvailableConfirmed: Notification.Name = .init(rawValue: "yesParkingAvailableConfirmed")
    
    public static let showSubmitParkingAvailablePopup: Notification.Name = .init(rawValue: "showSubmitParkingAvailablePopup")
    
    public static let cancelParkingAvailable: Notification.Name = .init(rawValue: "cancelParkingAvailable")
    
    public static let startWalkingPath: Notification.Name = .init(rawValue: "startWalkingPath")
    
    public static let backToHomePage: Notification.Name = .init(rawValue: "backToHomePage")
    
    public static let endNavigationWhenReachToCarpark: Notification.Name = .init(rawValue: "endNavigationWhenReachToCarpark")
    
    public static let willReroute: Notification.Name = .init(rawValue: "willReroute")
    
    public static let reroutingEnded: Notification.Name = .init(rawValue: "reroutingEnded")
    
    public static let obuOnShowCarparkAvailability: Notification.Name = .init(rawValue: "obuOnShowCarparkAvailability")
    public static let obuOnCloseCarparkAvailability: Notification.Name = .init(rawValue: "obuOnCloseCarparkAvailability")
    
    public static let obuOnShowTravelTime: Notification.Name = .init(rawValue: "obuOnShowTravelTime")
    public static let obuOnCloseTravelTime: Notification.Name = .init(rawValue: "obuOnCloseTravelTime")
    
    public static let openObuDisplay: Notification.Name = .init(rawValue: "openObuDisplay")
    public static let toggleObuDisplayTopMenu: Notification.Name = .init(rawValue: "toggleObuDisplayTopMenu")
    public static let toggleMapViewOBU: Notification.Name = .init(rawValue: "toggleMapViewOBU")
    public static let selectCarparkFromObuDisplay: Notification.Name = .init(rawValue: "selectCarparkFromObuDisplay")
    public static let navigateToAlternateCarpark: Notification.Name = .init(rawValue: "navigateToAlternateCarpark")
    public static let parkingAvailabilityAlertDismissed: Notification.Name = .init(rawValue: "parkingAvailabilityAlertDismissed")
    
    //  To update current location / obu connection status / card balance
    public static let carplayMapLoaded: Notification.Name = .init(rawValue: "carplayMapLoaded")
    
    public static let carplayConnectionDidChangeStatus: Notification.Name = .init(rawValue: "carplayConnectionDidChangeStatus")
    
    public static let obuConnectFailure: Notification.Name = .init(rawValue: "obuConnectFailure")
    public static let obuDisconnectFailure: Notification.Name = .init(rawValue: "obuDisconnectFailure")
    
    public static let obuUpdatePairingStatus: Notification.Name = .init(rawValue: "obuUpdatePairingStatus")
    public static let obuOnSearchingUpdates: Notification.Name = .init(rawValue: "obuOnSearchingUpdates")
    public static let obuOnSearchingFinished: Notification.Name = .init(rawValue: "obuOnSearchingFinished")
    public static let obuDidGetMinimumCardBalance: Notification.Name = .init(rawValue: "obuDidGetMinimumCardBalance")
    
    public static let showTravelTimeList: Notification.Name = .init(rawValue: "showTravelTimeList")
    public static let showCarParkList: Notification.Name = .init(rawValue: "showCarParkList")
    
    public static let obuConnectionUpdates: Notification.Name = .init(rawValue: "obuConnectionUpdates")
    public static let obuDisconnected: Notification.Name = .init(rawValue: "obuDisconnected")
    
    public static let onNoDataNotification: Notification.Name = .init(rawValue: "onNoDataNotification")
    public static let onCloseCarparkListScreen: Notification.Name = .init(rawValue: "onCloseCarparkListScreen")
    public static let onShowCarparkListUpdates: Notification.Name = .init(rawValue: "onShowCarparkListUpdates")
    public static let openCarparkListScreen: Notification.Name = .init(rawValue: "openCarparkListScreen")
    
    public static let openDropPinToolTip: Notification.Name = .init(rawValue: "openDropPinToolTip")
    
    public static let onReceivedSearchLocationInCollectionDetail: Notification.Name = .init(rawValue: "onReceivedSearchLocationInCollectionDetail")
    public static let onClearSearchCollectionDetails: Notification.Name = .init(rawValue: "onClearSearchCollectionDetails")
    public static let onCloseShareCollectionAndLocation: Notification.Name = .init(rawValue: "onCloseShareCollectionAndLocation")
    public static let shareCollectionSelectAmenity: Notification.Name = .init(rawValue: "shareCollectionSelectAmenity")
    public static let onReloadShareLocation: Notification.Name = .init(rawValue: "onReloadShareLocation")
    public static let carplayConnectionShowFullListStepTable: Notification.Name = .init(rawValue: "carplayConnectionShowFullListStepTable")
    public static let onCloseInviteScreen: Notification.Name = .init(rawValue: "onCloseInviteScreen")

}

enum Values {
    #if DEMO
    static let defaultMapZoomLevel: CGFloat = 13
    #else
    static let defaultMapZoomLevel: CGFloat = 16.5
    static let defaultProfileZoomLevl: CGFloat = 13.5
    
    static let cruiseModeCarPlayZoomLevel: CGFloat = 13.5
    static let cruiseModeMobileZoomLevel: CGFloat = 15
    
    static let defaultNavigationMinZoomLevel: CGFloat = 17.0
    static let defaultNavigationMaxZoomLevel: CGFloat = 18.5
    #endif
    
    static let defaultCruiseModePitch: CGFloat = 50
    
    static let detectionRange = 150.0
    static let durationToStopCruiseMode = 300.0
    static let navigationNotificationHeight: CGFloat = 120.0
    
    static let durationForETAReminder = 15.0 * 60
    static let durationForETAReachingReminder = 300.0
    
    static let standardAnimationDuration = 0.2
    
    static let distanceCheckThreshold = 300.0
    static let cruiseOverSpeedCheckThreshold = 10.0
    static let cruiseStartSpeed = 20.0
    static let locationNotUpdateThreshold = 30.0

    static let notificationDuration = DemoSession.shared.isDemoRoute ? 4.0 : 5.0
    
//    static let routePlanningNameLength = 18
//    static let routePlanningAddressLength = 22
    
    static let mobileNumberPrefix = "+65"
    static let mobileNumberCount: Int = 8
    
    static let indicatorTag = 2001
    static let indicatorContainerTag = 2002
    
    static let tapViewTag = 2003
    static let tutorialTag = 2004
    
    enum Instruction {
        static let roundedCornerRadius: CGFloat = 20.0
        
        enum regular {
            static let height: CGFloat = 90.0
            static let iconSize: CGFloat = 40.0
        }
        enum compact {
            static let height: CGFloat = 60.0
            static let iconSize: CGFloat = 22.0
        }
        enum regularMultiLane {
            static let height: CGFloat = 150.0
            static let iconSize: CGFloat = 45.0
            static let lanePadding: CGFloat = 15.0
        }
        enum compactMultiLane {
            static let height: CGFloat = 90.0
            static let iconSize: CGFloat = 26.0
            static let lanePadding: CGFloat = 7.0
        }
    }
    
    // MARK: - Map Landing
    static let bottomTabHeight: CGFloat = 90.0
    
    static let openTutorialKey: String = "openTutorialKey"
    
    // MARK: - Settings
    static let settingsKey = "Settings"
    
    // MARK: - Notification
    static let notificationNavigationStart = "navigationStart"
    static let notificationNavigationStop = "navigationStop"
    static let searchLocationReceived = "searchDiscoveryDetails"
    static let handleClearSearch = "handleClearSearch"
    static let handleAmenitySelection = "handleAmenitySelection"
    static let handleProfileSelection = "handleProfileSelection"
    static let notificationAppTerminate = "appTerminated"
    static let deviceToken = "deviceToken"
    static let NotificationETASetUp = "etaCallBack"
    static let NotificationEmailVerify = "emailVerify"
    static let NotificationETASelect = "etaSelect"
    static let NotificationCalendarSelect = "calendarSelect"
    static let NotificationOpenTripLog = "openTripLog"
    static let NotificationCloseNotifyArrivalScreen = "closeNotifyArrival"
    static let NotificationOpenSettings = "openSettings"
    static let NotificationOpenRPFromERPDetails = "openRPERPDetails"
    static let NotificationOpenMore = "openMore"
    static let NotificationAdjustRecenter = "adjustRecent"
    static let NotificationAdjustRecenterCollection = "adjustRecentCollection"
    static let NotificationAdjustRecenterExploreMap = "adjustRecentMapExplore"
    static let NotificationOpenRoutePlanning = "openRoutePlanning"
    static let NotificationDisableCruise = "disableCruise"
    static let NotificationOpenMapSettings = "openMapDisplaySettings"
    static let NotificationSelectContent = "selectContent"
    static let NotificationExploreMapBottomTabClicked = "exploreMapBottomTabClicked"
    static let NotificationExploreTBRDetailsBack = "exploreMapTBRBackClicked"
    static let NotificationExploreMapScreen = "goBackExploreMap"    
    
    static let receivedTrafficData = "receivedTrafficData"
    static let receivedTrafficID = "receivedTrafficID"
    static let upcomingTripType = "UPCOMING_TRIP"
    static let voucherType = "VOUCHER"
    static let appUpdateType = "APP_UPDATE"
    static let inboxType = "INBOX"
    static let exploreMapType = "EXPLORE_MAP"
    static let userPreferenceSettingsUpdated = "userPreferenceSettingsUpdated"
    
    static let NotificationChangeStatusBar = "NotificationOpenTripLog"
    static let NotificationCloseOnboardingTutorial = "NotificationCloseOnboardingTutorial"
    static let NotificationCloseCollectionDetail = "NotificationCloseCollectionDetail"
    static let NotificationUpdateCollectionDetail = "NotificationUpdateCollectionDetail"
    static let NotificationDidSaveLocationToCollection = "NotificationDidSaveLocationToCollection"
    static let NotificationRefreshAmenities = "NotificationRefreshAmenities"
    static let NotificationCloseProgressTracking = "NotificationCloseWebviewPanel"
    
    static let NotificationAmenitiesPetrolAndEV = "AmenitiesPetrolAndEV"
    
    static let NotificationShowToast = "NotificationShowToast"
    
    static let NotificationSelectedPetrolEV = "NotificationSelectedPetrolEV"
    
    // MARK: Notification userInfo key
    static let OpenTripLogKey = "openTripLog"
    static let tripLogNavigationParams = "navigationParams"
    static let tripLogTab = "tripLogTab"
    static let startTime = "startTime"
    static let endTime = "endTime"
    static let filtering = "filtering"
    static let vehicleNumber = "vehicleNumber"
    
    static let AdjustRecentKey = "adjustRecent"
    static let BottomSheetIndex = "bottomSheetIndex"
    static let BottomSheetFromScreen = "bottomSheetFromScreen" 
    static let OpenMoreKey = "openMore"
    static let ParkingIdKey = "parkingID"
    static let DisableCruiseKey = "disableCruise"
    static let ChangeStatusBarKey = "status_bar_black"
    static let ContentIdKey = "content_id"
    static let ScreenNameKey = "screenName"
    
    // MARK: - Clustering
    static let clusterRadius:Double = 50
    static let clusterMaxZoom:Double = 14
    static let circleRadius:Double = 5
    
    // MARK: - Car park
    static let carparkSearchRadius: Double = 0.5
    static let maxNoOfCarparksInCruiseMode: Int = 5
    static let maxNoOfCarparksInLanding: Int = 10
    static let maxNoOfCarparksInLandingForCarPlay: Int = 12
    static let maxNoOfCarparksInRoutePlanning: Int = 10
    static let carparkMonitorDistanceInCruise: Int = 1000
    static let carparkMonitorDistanceInLanding: Int = 500
    static let maxBookmarkLimit: Int = 50
    static let toolTipDistance: Int = 91
    static let carparkMonitorDistanceInRoutePlanning: Int = 500
    static let carparkMonitorMaxDistanceInLanding: Int = 3000
    static let amenitiesMonitorMaxDistanceLanding: Int = 3
    static let toastAutoDismissInSeconds: Int = 10
    static let toolTipType = "GENERAL"
    static let carparkListDistanceInNavigation: Double = 1000.0
    static let carparkAvailabilityDistanceInNavigation: Double = 2000.0
    static let carparkAvailabilityAlertDisplayTime: Int = 8
    
    static let evPetrolCount: Int = 3
    static let evPetrolRange: Int = 5000
    static let maxEVPetrolRange: Int = 5000
    
    // MARK: - AppSync keys
    static let appSyncERP = "ERP_PASSENGER_CARS"
    static let appSyncTrafficIncidents = "TRAFFIC_INCIDENTS_V2"
    static let appSyncSchoolZone = "SCHOOL_ZONE"
    static let appSyncSilverZone = "SILVER_ZONE"
    static let appSyncSpeedCam = "SPEEDLIGHT_CAMERA"
    static let appSyncInbox = "INBOX_GLOBAL_NOTIFICATION"
    
    // MARK: - ETA keys
    static let etaContactName = "contactname"
    static let etaPhoneNumber = "contactnumber"
    static let etaMessage = "etamessage"
    static let etaName = "username"
    static let etaLocation = "location"
    static let etaETA = "eta"
    static let etaFromScreen = "fromScreen"
    static let etaShareLiveLocation = "shareliveloc"
    static let etaSendVoice = "sendvoice"
    static let etaStatus = "etastatus"
    static let etaCruiseDistance:Double = 200
    static let etaFavoriteId = "tripEtaFavouriteId"
    static let etaData = "data"
    static let etaArrivalTimestamp = "arrivalTimestamp"
        
    // MARK: - Email popup key
    static let emailPopupDismissedCount = "emailPopupDismissedCount"
    static let emailPopupConfirmed = "emailPopupConfirmed"
    static let isAppLaunch = "isAppLaunch"
    
    //MARK: - Parking Nudge
    static let isParkingNudgeShown = "isParkingNudgeShown2"
    
    // MARK: - BottomSheet tips popup key
    static let tipsViewed = "tipsViewed"
    
    //MARK: - Instabug Usergroup
    static let usergroup = "user_group"
    
    //MARK: - setting user_id i.e cognito ID
    static let userID = "user_id"
    
    // MARK: - Debug
    static let debugEnabled = "debugEnabled"
    static let debugEnabledRemotely = "debugEnabledRemotely"
    
    // MARK: - Demo
    static let wayPointSeparateLegs = false
    static let wayPointAccident = "Accident"
    static let wayPointERP = "ERP"
    static let wayPointPaid = "Paid"
    static let wayPointETAExpress = "ETA Expressway"
    static let wayPointSpeed = "Speed limit"
    static let wayPointSchool = "School zone"
    static let wayPointParkingAPI = "ParkingAPI"
    static let wayPointRoad = "Roadside"
    static let wayPointCarparkLots = "Carpark Lots"
    static let wayPointCarParkLevel3 = "Car Park Level3"
    static let wayPointCyclist1 = "Cyclist 1"
    static let wayPointCyclist2 = "Cyclist 2"
    static let etaNotifyName = "Notify"
    static let carPark = "CarParkMonitor"

    // MARK: - AppState
    static let isLoggedIn = "isLoggedIn"
    
    //MARK: - FirebaseRemoteConfig Params
    static let optional_update_check_trials = "optional_update_check_trials"
    static let optional_update_check_duration_hrs = "optional_update_check_duration_hrs"
    static let maintenance_message = "maintenance_message"
    static let is_maintenance = "is_maintenance"
    static let maintenance_message_v2 = "maintenance_message_v2"
    static let is_maintenance_v2 = "is_maintenance_v2"
    static let latest_build_version = "latest_build_version"
    static let min_build_version = "min_build_version"
    static let download_link = "download_link"
    static let optionalAppUpdateLaterCount = "optionalAppUpdateLaterCount"
    static let optionalAppUpdateLaterDate = "optionalAppUpdateLaterDate"
    static let mandatory_relogin = "mandatory_relogin"
    static let minimumFetchInterval:TimeInterval = 0
    
    //MARK: - Push Notification Device tokem
    static let push_notification_token = "push_notification_token"
    
    static let contentIdKey = "contentID"
    static let contentCategoryIdKey = "contentCategoryID"
    
    //MARK: - Amenity names
    static let DROPPIN = "droppin"
    static let PETROL = "petrol"
    static let MUSEUM = "museum"
    static let HERITAGETREE = "heritagetree"
    static let PCN = "pcn"
    static let PASARMALAM = "pasarmalam"
    static let FIREWORKS = "fireworks"
    static let PARKS = "parks"
    static let greenClusterGroup = "PCNPlayParks"
    static let redClusterGroup = "HawResMalam"
    static let HAWKERCENTRE = "hawkercentre"
    static let LIBRARY = "library"
    static let PLAYGROUND = "playground"
    static let RESTAURANT = "restaurant"
    static let EV_CHARGER = "evcharger"
    static let AMENITYCARPARK = "carpark"
    static let TRAFFIC = "traffic"
    static let POI = "poi"
    static let AMENITIES = "amenities"
    static let COLLECTIONID = "collection_id"
    static let TORNSCREEN = "to_rn_screen"
    static let COLLECTION = "collection"
    static let COLLECTIONHOME = "collection-home"
    static let COLLECTIONWORK = "collection-work"
    static let COLLECTIONNORMAL = "collection-normal"
    static let NEWBOOKMARKS = "newbookmark"
    static let COLLECTION_NAME = "collection_name"
    static let SHARING_TOKEN = "sharing_token"
    static let COLLECTION_TOKEN = "collectionToken"
    static let COLLECTION_DESC = "collection_description"
    static let COLLECTION_CODE = "code"
    static let COLLECTION_ERROR_CODE = "error_code"
    static let COLLECTION_ERROR_MESSAGE = "message"
    static let COLLECTION_SAVING = "saving"
    static let SHARE_MODE = "share_mode"
    static let SHARE_LOCATION_DATA = "share_location_data"
    static let SHARED_LOCATION = "sharedLocation"
    static let BOOKMARK_ID = "bookmarkId"
    
    //MARK: - Save location / collection
    static let SHARE_CARPARK_FULL = "carpark-full-icon"
    static let SHARE_CARPARK_FILLING_UP = "carpark-filling-up-fast-icon"
    static let SHARE_CARPARK_AVAILABLE = "carpark-available-icon"
    static let SHARE_BOOKMARK_ICON = "bookmark-icon"
    static let SHARE_EV_ICON = "ev-amenity-icon"
    static let SHARE_PETROL_ICON = "petrol-amenity-icon"
    static let SHARE_HOME_LOCATION = "home-location-icon"
    static let SHARE_WORK_LOCATION = "work-location-icon"
    static let SHARE_PETROL_BOOKMARKED = "petrol-amenity-icon-bookmarked"
    static let SHARE_EV_BOOKMARKED = "ev-amenity-icon-bookmarked"
    static let SHARE_CARPARK_FULL_BOOKMARKED = "carpark-full-icon-bookmarked"
    static let SHARE_CARPARK_FILLING_UP_BOOKMARKED = "carpark-filling-up-fast-icon-bookmarked"
    static let SHARE_CARPARK_AVAILABLE_BOOKMARKED = "carpark-available-icon-bookmarked"
    static let SHARE_DESTINATION_ICON = "destinationMark"
        
    //MARK: - Bookmark Save Screen name
    static let Collection_From_Share_Collection_Screen = "ShareCollectionScreen"
    static let Collection_From_Screen_Details = "CollectionDetails"
    static let Collection_From_Screen_Shortcut = "ShortcutFullList"
    static let Collection_From_Screen_Choose = "ChooseCollection"
    static let CollectionDetailsBookmarkEditor = "CollectionDetailsBookmarkEditor"
    
    //MARK: - Bottomsheet modes
    static let ERP_MODE = "erp_mode"
    static let PARKING_MODE = "parking_mode"
    static let TRAFFIC_MODE = "traffic_mode"
    static let OBU_MODE = "obu_mode"
    static let NONE_MODE = "no_mode_sellect"
    static let FOCUSED_LAT = "lat"
    static let FOCUSED_LONG = "long"
    
    
    //MARK: - Amenity symbol types based on theme
    static let LIGHT_MODE_SELECTED = "_ls" //Light mode selected
    static let LIGHT_MODE_UN_SELECTED = "_lun" //Light mode unselected
    static let DARK_MODE_SELECTED = "_ds" //Dark mode selected
    static let DARK_MODE_UN_SELECTED = "_dun" //Dark mode unselected
    
    //MARK: - ERP Bound values
    static let INCREASE = "INCREASE"
    static let INCREASE_GREY = "INCREASE_GREY"
    static let DECREASE_GREY = "DECREASE_GREY"
    static let DECREASE = "DECREASE"
    static let NO_BOUND = "NO_BOUND"
    
    static let ACTIVE_NO_ARROW = "ACTIVE_NO_ARROW"
    static let IN_ACTIVE_NO_ARROW = "IN_ACTIVE_NO_ARROW"
    
    //MARK: - Trip Plan
    static let DEPART_AT = "DEPART_AT"
    static let ARRIVE_BY = "ARRIVE_BY"
    
    // MARK: - Notification display status key
    static let DISPLAYED_NOTIFICATIONS_KEY = "displayedNotifications"
    
    // MARK: - Explored content ids
//    static let EXPLORED_CONTENT_IDS_KEY = "exploredContentIDs"
    
    //MARK: - Locales
    static let enSGLocale = "en-SG"
    static let enGBLocale = "en-GB"
    
    // MARK: - Navigation top/bottom size
    static let bottomPanelHeight: CGFloat = 90
    static let topPanelHeight: CGFloat = 120
    
    // MARK: - Dotted line color in route planning
    static let dottedLineLightColor = #colorLiteral(red: 0.471, green: 0.180, blue: 0.694, alpha: 1)
    static let dottedLineDarkColor = #colorLiteral(red: 0.7541097999, green: 0.4520136118, blue: 0.9790839553, alpha: 1)
    
    // MARK: - Speed limit alert
    static let overspeedFirstAlertDuration = 30.0
    static let overspeedSecondAlertDuration = 600.0
    
    // MARK: - Nudge text
    static let maplandingNudgeCarparkTitle = "Carpark View Mode"
    static let maplandingNudgeCarparkSubTitle = "Choose what carparks to show on your map. Go to “Settings” to further customise."
    static let maplandingNudgeSaveTitle = "Your Personal Maps"
    static let maplandingNudgeSaveSubTitle = "Start saving locations into collections to create a personalised map of your favourite places."
    static let maplandingNudgeExploreTitle = "Explore Maps"
    static let maplandingNudgeExploreSubTitle = "Discover places sorted by themes and pinned for easy reference. Check often for updated content."


}

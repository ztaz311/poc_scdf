//
//  AnnotationUtils.swift
//  Breeze
//
//  Utils to select annotation
//
//  Created by Zhou Hao on 13/1/22.
//

import Foundation
import CoreLocation
import MapboxMaps
import UIKit

/*
 Get image from carpark
 */
func getcarparkImageName(carpark: Carpark, selected: Bool, ignoreDestination: Bool = false) -> (String,String) {
    let isDestination = (carpark.destinationCarPark ?? false) && (!ignoreDestination)
    
    
    // Session parking
    if isSessionParking(carpark: carpark) {
        let nameAndColor = getImageName(name: "carparkType1", isDestination: isDestination, isSelected: selected, hasVoucher: carpark.hasVoucher)
        return nameAndColor
    }
        
    // has info (different image bands)
    if carpark.hasRatesInfo ?? false {
        // no info / has rate
        if carpark.availablePercent == nil ||
            (carpark.availablePercent != nil && carpark.availablePercent! == -1) {
            // do nothing, use the default imageName
            var nameAndColor = getImageName(name: "carparkType5", isDestination: isDestination, isSelected: selected, hasVoucher: carpark.hasVoucher)
            switch carpark.currentParkingStatus {
            case .fullParking:
                nameAndColor = getImageName(name: "carparkType3", isDestination: isDestination, isSelected: selected, hasVoucher: carpark.hasVoucher)
            case .fewAvailableLots:
                nameAndColor = getImageName(name: "carparkType4", isDestination: isDestination, isSelected: selected, hasVoucher: carpark.hasVoucher)
            default:
                break
            }
            return nameAndColor
        } else {
            return getImageNameBasedOnBand(carpark: carpark, isDestination: isDestination, isSelected: selected)
        }
    } else {
        // has no info and no rate
        if carpark.availablePercent == nil || (carpark.availablePercent != nil && carpark.availablePercent! == -1) {
            return getImageName(name: "carparkType2", isDestination: isDestination, isSelected: selected, hasVoucher: carpark.hasVoucher)
        } else {
            return getImageNameBasedOnBand(carpark: carpark, isDestination: isDestination, isSelected: selected)
        }
    }
}

/*
     This is only for testing if destination has car park always true and contains always a voucher
 */
func getcarparkImageNameForRP(carpark: Carpark, selected: Bool, ignoreDestination: Bool = false) -> (String,String) {
    let isDestination = true
    
    
    // Session parking
    if isSessionParking(carpark: carpark) {
        let nameAndColor = getImageName(name: "carparkType1", isDestination: isDestination, isSelected: selected, hasVoucher: true)
        return nameAndColor
    }
        
    // has info (different image bands)
    if carpark.hasRatesInfo ?? false {
        // no info / has rate
        if carpark.availablePercent == nil ||
            (carpark.availablePercent != nil && carpark.availablePercent! == -1) {
            // do nothing, use the default imageName
            let nameAndColor = getImageName(name: "carparkType5", isDestination: isDestination, isSelected: selected, hasVoucher: true)
            return nameAndColor
        } else {
            return getImageNameBasedOnBand(carpark: carpark, isDestination: isDestination, isSelected: selected)
        }
    } else {
        // has no info and no rate
        if carpark.availablePercent == nil || (carpark.availablePercent != nil && carpark.availablePercent! == -1) {
            return getImageName(name: "carparkType2", isDestination: isDestination, isSelected: selected, hasVoucher: true)
        } else {
            return getImageNameBasedOnBand(carpark: carpark, isDestination: isDestination, isSelected: selected)
        }
    }
}



func getCarparkFeature(carpark: Carpark, ignoreDestination: Bool = false, selected: Bool,cpType:String) -> Turf.Feature {
    var feature = Turf.Feature(geometry: .point(Point(CLLocationCoordinate2D(latitude: carpark.lat ?? 0, longitude: carpark.long ?? 0))))
    let cost = carpark.currentHrRate == nil ? "------" : (carpark.currentHrRate!.oneHrRate == nil ? "------" : carpark.currentHrRate!.oneHrRate! > 0 ? String(format: "$%.2f", carpark.currentHrRate!.oneHrRate!) : "Free")
    let colorCodeAndImageName = getImageName(name: cpType, isDestination: ignoreDestination, isSelected: selected, hasVoucher: carpark.hasVoucher)
    feature.properties = [
        "distance": .number(Double(carpark.distance ?? 0)),
        "cost": .string(cost),
        "isCheapest": .boolean(carpark.isCheapest ?? false),
        "carparkId": .string(carpark.id ?? ""),
        "imageName": .string(colorCodeAndImageName.0),
        "clusterColor":.string(colorCodeAndImageName.1)
    ]
    return feature
}

func getCPFeaturesOfType(type:String,carParks:[Carpark], ignoreDestination: Bool = false) -> Turf.FeatureCollection?{
    
    var finalFeatureCollection = Turf.FeatureCollection(features: [])
    for carPark in carParks {
        
        let isDestination = (carPark.destinationCarPark ?? false) && (!ignoreDestination)
        
        if isSessionParking(carpark: carPark) {
            
            if(type == "carparkType1"){
                
                let feature = getCarparkFeature(carpark: carPark, ignoreDestination: isDestination, selected: carPark.selected,cpType: type)
                finalFeatureCollection.features.append(feature)
            }
        }
        else{
            
            if(type == "carparkType2"){
                
                if carPark.hasRatesInfo == false {
                    
                    // has no info and no rate
                    if carPark.availablePercent == nil || (carPark.availablePercent != nil && carPark.availablePercent! == -1) {
                        
                        let feature = getCarparkFeature(carpark: carPark, ignoreDestination: isDestination, selected: carPark.selected,cpType: type)
                        finalFeatureCollection.features.append(feature)
                    }
                }
                
                
            }
            
            else if(type == "carparkType3" || type == "carparkType4" || type == "carparkType5"){
                
                let imageName = getImageNameBasedOnBand(carpark: carPark, isDestination: isDestination, isSelected: carPark.selected).0
                if(imageName.contains(type)){
                    
                    let feature = getCarparkFeature(carpark: carPark, ignoreDestination: isDestination, selected: carPark.selected,cpType: type)
                    finalFeatureCollection.features.append(feature)
                }
                
            }
        }
    }
    
    return finalFeatureCollection
}


// Util function to get carpark annotation image for different scenarios
// In maplanding, when not in discover, should ignore destination. In cruise, also need to ignore destination
func getAnnotationImage(carpark: Carpark, selected: Bool, ignoreDestination: Bool = false) -> PointAnnotation.Image {
    let name = getcarparkImageName(carpark: carpark, selected: selected, ignoreDestination: ignoreDestination)
    return .init(image:UIImage(named: name.0)!, name: name.0)
    
/*
    // season only - scenario 3,4
    if carpark.parkingType == Carpark.typeSeason {
        if isDestination && isDiscoverMode {
            return selected ? .init(image:UIImage(named: "destinationWithCarparkNoLots-selected")!,name:"destinationWithCarparkNoLots-selected"): .init(image:UIImage(named: "destinationWithCarparkNoLots")!,name:"destinationWithCarparkNoLots")
        } else {
            return selected ? .init(image:UIImage(named: "carparkNoLotsIcon-selected")!,name:"carparkNoLotsIcon-selected"): .init(image:UIImage(named: "carparkNoLotsIcon")!,name:"carparkNoLotsIcon")
        }
    }
    
    if isDestination {
        if(isDiscoverMode){
            if let availableLots = carpark.availablelot?.intValue {
                if(availableLots == 0){
                    return setDestinationAnnotationImage(noLots: true, selected: selected)
                } else {
                    return setDestinationAnnotationImage(noLots: false, selected: selected)
                }
            } else {
                if carpark.hasRatesInfo ?? false {
                    return selected ? .init(image: UIImage(named: "destinationWithCarpark-selected")!, name: "destinationWithCarpark-selected")  : .init(image: UIImage(named: "destinationWithCarpark")!, name: "destinationWithCarpark")

                } else {
                    return selected ? .init(image:UIImage(named: "destinationWithCarparkNoData-selected")!,name:"destinationWithCarparkNoData-selected"): .init(image:UIImage(named: "destinationWithCarparkNoData")!,name:"destinationWithCarparkNoData")
                }
            }
        } else {
            return updateAnnotationImage(isDestination: ignoreDestination ? false : true, isSelected: selected, carpark: carpark)
        }
    } else { // Not destination
        return updateAnnotationImage(isDestination: false, isSelected: selected, carpark: carpark)
    }
*/
}

private func isSessionParking(carpark: Carpark) -> Bool {
    return carpark.parkingType == Carpark.typeSeason || (carpark.parkingType == Carpark.typeCustomer && carpark.availablelot != nil && carpark.availablelot?.intValue == 0)
}

private func getImageBand(carpark: Carpark) -> String {
    return carpark.availablePercentImageBand ?? "TYPE2"
}

// Destination voucher don't have select type
func getImageName(name: String, isDestination: Bool, isSelected: Bool, hasVoucher: Bool = false) -> (String,String) {
    var newName = name
    if isDestination {
        newName = "destination-\(newName)"
    }
    
    if !(hasVoucher && isDestination) {
        if isSelected {
            newName = "\(newName)-selected"
        }
    }
    
    if (hasVoucher) {
        newName = "\(newName)-voucher"
    }
    
    return (newName,getCPColorBasedOnType(cpType: newName)) //Type1
}

private func getCPColorBasedOnType(cpType:String) -> String{
    
    var colorString = "#AEB7BD" //default for type1 and type2

    if(cpType.contains("carparkType3")){
        
        colorString = "#F9454C"
    }
    
    else if(cpType.contains("carparkType4")){
        
        colorString = "#FF9633"
    }
    
    if(cpType.contains("carparkType5")){
        
        colorString = "#005FD0"
    }
    
    return colorString
}

func getRGBCPColor(type:String) -> UIColor{
    
    var colorString = "#778188" //default for type1 and type2

    if(type.contains("carparkType3")){
        
        colorString = "#E81C24"
    }
    
    else if(type.contains("carparkType4")){
        
        colorString = "#FF8A19"
    }
    
    if(type.contains("carparkType5")){
        
        colorString = "#AE5AEE"
    }
    
    return UIColor(hexString: colorString, alpha: 1.0)
}

private func getImageNameBasedOnBand(carpark: Carpark, isDestination: Bool, isSelected: Bool) -> (String,String) {
    var imageName = "carparkType5"
    let band = getImageBand(carpark: carpark)

    if band == "TYPE0" {
        imageName = "carparkType3"
    } else if band == "TYPE1" {
        imageName = "carparkType4"
    }
    let name = getImageName(name: imageName, isDestination: isDestination, isSelected: isSelected, hasVoucher: carpark.hasVoucher)
    return name
}

func setDestinationAnnotationImage(noLots:Bool, selected: Bool) -> PointAnnotation.Image {
    
    if(noLots){
        
        return selected ? .init(image: UIImage(named: "destinationWithCarparkNoLots-selected")!, name: "destinationWithCarparkNoLots-selected")  : .init(image: UIImage(named: "destinationWithCarparkNoLots")!, name: "destinationWithCarparkNoLots")
    }
    else{
        
        return selected ? .init(image:UIImage(named: "destinationWithCarpark-selected")!, name:"destinationWithCarpark-selected") : .init(image:UIImage(named: "destinationWithCarpark")!,name:"destinationWithCarpark")
        
    }
        
}
//
//func setAnnotationImage(noLots:Bool, selected: Bool) -> PointAnnotation.Image {
//
//    if(noLots){
//        return selected ? .init(image:UIImage(named: "carparkNoLotsIcon-selected")!,name:"carparkNoLotsIcon-selected"): .init(image:UIImage(named: "carparkNoLotsIcon")!,name:"carparkNoLotsIcon")
//    }
//    else{
//        return selected ? .init(image:UIImage(named: "carparkIcon-selected")!, name:"carparkIcon-selected") : .init(image:UIImage(named: "carparkIcon")!,name:"carparkIcon")
//    }
//
//}

func updateAnnotationImage(isDestination: Bool, isSelected: Bool, carpark: Carpark)-> PointAnnotation.Image {
    if let availableLots = carpark.availablelot?.intValue {
        if(availableLots == 0) { // = 0, scenario 3,4
            return getAnnotationImage(isDestination: isDestination, noLots: true, selected: isSelected)
        } else { // (> 0) scenario 1,2
            return getAnnotationImage(isDestination: isDestination, noLots: false, selected: isSelected)
        }
    } else {
        if carpark.hasRatesInfo ?? false { // No data and but has rate info - scenario 1,2
            return getAnnotationImage(isDestination: isDestination, noLots: false, selected: isSelected)
        } else { // No data and has no rate info - scenario 5,6
            return isSelected ? .init(image:UIImage(named: "carparkNoDataIcon-selected")!,name:"carparkNoDataIcon-selected"): .init(image:UIImage(named: "carparkNoDataIcon")!,name:"carparkNoDataIcon")
        }
    }
}

func getAnnotationImage(isDestination: Bool, noLots:Bool, selected: Bool) -> PointAnnotation.Image {
    if isDestination {
        if(noLots){
            return selected ? .init(image: UIImage(named: "destinationWithCarparkNoLots-selected")!, name: "destinationWithCarparkNoLots-selected")  : .init(image: UIImage(named: "destinationWithCarparkNoLots")!, name: "destinationWithCarparkNoLots")
        }
        else{
            
            return selected ? .init(image:UIImage(named: "destinationWithCarpark-selected")!, name:"destinationWithCarpark-selected") : .init(image:UIImage(named: "destinationWithCarpark")!,name:"destinationWithCarpark")
            
        }
    } else {
        if(noLots){
            return selected ? .init(image:UIImage(named: "carparkNoLotsIcon-selected")!,name:"carparkNoLotsIcon-selected"): .init(image:UIImage(named: "carparkNoLotsIcon")!,name:"carparkNoLotsIcon")
        }
        else{
            return selected ? .init(image:UIImage(named: "carparkIcon-selected")!, name:"carparkIcon-selected") : .init(image:UIImage(named: "carparkIcon")!,name:"carparkIcon")
        }
    }
}

//
//  SearchBarExt.swift
//  Breeze
//
//  Created by VishnuKanth on 15/12/20.
//

import UIKit

public var topViewLabel:UILabel?

extension UILabel{
    
    func setTopView(topView:UILabel){
        topViewLabel = topView
    }
}

extension UISearchBar {
    
    public var textField: UITextField? {
        if #available(iOS 13, *) {
            
            searchTextField.translatesAutoresizingMaskIntoConstraints = false
                    NSLayoutConstraint.activate([
                        searchTextField.heightAnchor.constraint(equalToConstant: 48),
                        searchTextField.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 2),
                        searchTextField.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0),
                        self.tag == 4 ? searchTextField.topAnchor.constraint(equalTo: topViewLabel!.bottomAnchor, constant: 24):searchTextField.topAnchor.constraint(equalTo: self.topAnchor, constant: 8),
                        searchTextField.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0)
                    ])
            searchTextField.clipsToBounds = true
            searchTextField.layer.cornerRadius = 25
            return searchTextField
        }
        let subViews = subviews.flatMap { $0.subviews }
        guard let textField = (subViews.filter { $0 is UITextField }).first as? UITextField else {
            return nil
        }
        textField.translatesAutoresizingMaskIntoConstraints = false
                NSLayoutConstraint.activate([
                    textField.heightAnchor.constraint(equalToConstant: 48),
                    textField.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 10),
                    textField.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -8),
                    self.tag == 4 ? textField.topAnchor.constraint(equalTo: topViewLabel!.bottomAnchor, constant: 24):textField.topAnchor.constraint(equalTo: self.topAnchor, constant: 8),
                    textField.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0)
                ])
        textField.clipsToBounds = true
        textField.layer.cornerRadius = 25
        return textField
    }

    func clearBackgroundColor() {
        guard let UISearchBarBackground: AnyClass = NSClassFromString("UISearchBarBackground") else { return }

        for view in subviews {
            for subview in view.subviews where subview.isKind(of: UISearchBarBackground) {
                subview.alpha = 0
            }
        }
    }

    public var activityIndicator: UIActivityIndicatorView? {
        return textField?.leftView?.subviews.compactMap { $0 as? UIActivityIndicatorView }.first
    }
    
//    public var addVerticalLine: UIView? {
//        return textField?.leftView?.subviews.compactMap { $0 as? UIView }.first
//    }
//    
//    var showVerticalLine:Bool{
//        get {
//            return addVerticalLine != nil
//        } set {
//            if newValue {
//                if addVerticalLine == nil {
//                    let newVerticalLine = UIView(frame: .zero)
//                    newVerticalLine.translatesAutoresizingMaskIntoConstraints = false
//                    newVerticalLine.widthAnchor.constraint(equalToConstant: 1).isActive = true
//                    newVerticalLine.backgroundColor = .black
//                    textField?.leftView?.addSubview(newVerticalLine)
//                    newVerticalLine.heightAnchor.constraint(equalTo: textField!.heightAnchor, multiplier: 0.6).isActive = true
//                    
//                    let leftViewSize = textField?.leftView?.frame.size ?? CGSize.zero
//
//                    newVerticalLine.center = CGPoint(x: leftViewSize.width - newVerticalLine.frame.width / 2,
//                                                          y: leftViewSize.height / 2)
//                    
//                }
//            }
//            else{
//                addVerticalLine?.removeFromSuperview()
//            }
//        }
//    }

    var isLoading: Bool {
        get {
            return activityIndicator != nil
        } set {
            if newValue {
                if activityIndicator == nil {
                    let newActivityIndicator = UIActivityIndicatorView(style: .medium)
                    newActivityIndicator.color = UIColor.gray
                    newActivityIndicator.startAnimating()
                    newActivityIndicator.backgroundColor = textField?.backgroundColor ?? UIColor.white
                    textField?.leftView?.addSubview(newActivityIndicator)
                    let leftViewSize = textField?.leftView?.frame.size ?? CGSize.zero

                    newActivityIndicator.center = CGPoint(x: leftViewSize.width - newActivityIndicator.frame.width / 2,
                                                          y: leftViewSize.height / 2)
                }
            } else {
                activityIndicator?.removeFromSuperview()
            }
        }
    }

    func changePlaceholderColor(_ color: UIColor) {
        guard let UISearchBarTextFieldLabel: AnyClass = NSClassFromString("UISearchBarTextFieldLabel"),
            let field = textField else {
            return
        }
        for subview in field.subviews where subview.isKind(of: UISearchBarTextFieldLabel) {
            (subview as! UILabel).textColor = color
        }
    }

    func setRightImage(normalImage: UIImage,
                       highLightedImage: UIImage) {
        
//        showsBookmarkButton = false
//                if let btn = textField?.rightView as? UIButton {
//                    btn.frame =  CGRect(x: -100, y: 0, width: 15, height: 15)
//                    btn.setImage(normalImage,
//                                 for: .normal)
//                    btn.setImage(highLightedImage,
//                                 for: .highlighted)
//                }

        
        //For custom clear button with adjusted y position
        let imageButton = UIButton(type: .custom)
        imageButton.frame = CGRect(x: self.frame.width - (normalImage.size.width) - 12, y: (self.frame.height - (normalImage.size.height))/2.0, width: (normalImage.size.width), height: (normalImage.size.height))
        imageButton.setImage(normalImage, for:.normal)
        
        let tapClearBtn = UITapGestureRecognizer(target: self, action: #selector(clearBtnAction(tapClearBtn:)))
        imageButton.isUserInteractionEnabled = true
        imageButton.addGestureRecognizer(tapClearBtn)
        imageButton.tag = 2000
        if let foundView = self.viewWithTag(2000) {
            
            if self.text!.count > 0{
                foundView.isHidden = false
            }else{
                foundView.isHidden = true
            }
        }else{
            self.addSubview(imageButton)
            if self.text!.count > 0{
                imageButton.isHidden = false
            }else{
                imageButton.isHidden = true
            }
        }
        
        
        

    }
    
    @objc func clearBtnAction(tapClearBtn: UITapGestureRecognizer)
    {
        textField?.text = ""
        self.delegate?.searchBar?(self, textDidChange: "")
        

    }
  
    
    func setLeftImage(_ image: UIImage,
                      with padding: CGFloat = 0,
                      tintColor: UIColor, size: CGSize = CGSize(width: 28, height: 28), textPadding: CGFloat = 9) {
        let imageView = UIImageView()
        imageView.image = image
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.widthAnchor.constraint(equalToConstant: size.width).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: size.height).isActive = true
        imageView.tintColor = tintColor

        if padding != 0 {
            let stackView = UIStackView()
            stackView.axis = .horizontal
            stackView.alignment = .center
            stackView.distribution = .fill
            stackView.spacing = 9
            stackView.translatesAutoresizingMaskIntoConstraints = false
            
            let paddingView = UIView()
            paddingView.translatesAutoresizingMaskIntoConstraints = false
            paddingView.widthAnchor.constraint(equalToConstant: padding).isActive = true
            paddingView.heightAnchor.constraint(equalToConstant: padding).isActive = true
            
//            let lineView = UIView()
//            lineView.translatesAutoresizingMaskIntoConstraints = false
//
//            lineView.widthAnchor.constraint(equalToConstant: 2).isActive = true
//            lineView.heightAnchor.constraint(equalToConstant: 20).isActive = true
            
            stackView.addArrangedSubview(paddingView)
            stackView.addArrangedSubview(imageView)
            //stackView.addArrangedSubview(lineView)
            //lineView.layer.borderWidth = 2
            //lineView.backgroundColor = UIColor.brandPurpleColor
            
            if textPadding > 8 {
                // Don't know why this doesn't work
//                stackView.setCustomSpacing(textPadding - 8, after: imageView)
                let textPaddingView = UIView()
                textPaddingView.translatesAutoresizingMaskIntoConstraints = false
                textPaddingView.widthAnchor.constraint(equalToConstant: textPadding - 8).isActive = true
                textPaddingView.heightAnchor.constraint(equalToConstant: 10).isActive = true
                stackView.addArrangedSubview(textPaddingView)
            }

            textField?.leftView = stackView

        } else {
            textField?.leftView = imageView
        }
    }
}

extension UIImage {
    convenience init(color: UIColor, size: CGSize = CGSize(width: 1, height: 1)) {
        let rect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0)
        color.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        self.init(cgImage: (image?.cgImage!)!)
    }
}

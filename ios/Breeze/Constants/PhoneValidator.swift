//
//  PhoneValidator.swift
//  Breeze
//
//  Created by Zhou Hao on 22/7/21.
//

import Foundation

class PhoneValidator {
    static func isValidSingaporeMobile(number: String) -> Bool {
        if number.count < 8 {
            return false
        }
        
        let newNumber = number.filter { $0.isNumber }
        
        if newNumber.count == 8 {
            return newNumber.first == "8" || number.first == "9"
        }
        
        if newNumber.count == 10 {
            let country = newNumber[0..<2]
            let phone = newNumber[2..<10]
            return country == "65" && (phone.first == "8" || phone.first == "9")
        }
        
        return false
    }
}

//
//  Utils+Navigation.swift
//  Breeze
//
//  Created by Zhou Hao on 17/5/22.
//

import Foundation
import CoreLocation
import MapboxMaps
import MapboxCoreMaps
import MapKit
import MapboxNavigation
import MapboxDirections
import MapboxCoreNavigation
import Turf

// TODO: Move navigation related util functions here

// MARK: Navigation related functions
func requestWalkingRoute(location1: LocationCoordinate2D, location2: LocationCoordinate2D, name: String, completion:@escaping  (RouteResponse?) -> Void) {
    var waypoints = [Waypoint]()
    
    let userWaypoint = Waypoint(coordinate: location1)
    waypoints.append(userWaypoint)
    
    // 2. destination
    let waypoint = Waypoint(coordinate: location2, name: name)
    waypoint.targetCoordinate = location2
    waypoints.append(waypoint)

    let navigationRouteOptions = NavigationRouteOptions(waypoints: waypoints, profileIdentifier: .walking)
    navigationRouteOptions.shapeFormat = .polyline6

//        self.navMapView.trafficLowColor = .gray
//        self.navMapView.trafficHeavyColor = .orange
//        self.navMapView.trafficSevereColor = .red
//        self.navMapView.trafficUnknownColor = .lightGray
//        self.navMapView.traversedRouteColor = .green
    
    // Get periodic updates regarding changes in estimated arrival time and traffic congestion segments along the route line.
    RouteControllerProactiveReroutingInterval = 30
    
    Directions.shared.calculate(navigationRouteOptions) { (session, result) in
        switch result {
        case let .success(response):
            completion(response)
        case .failure(_):
            completion(nil)
        }
    }
}

func lineWidthExpression(_ multiplier: Double = 1.0) -> Expression {
    let lineWidthExpression = Exp(.interpolate) {
        Exp(.linear)
        Exp(.zoom)
        // It's possible to change route line width depending on zoom level, by using expression
        // instead of constant. Navigation SDK for iOS also exposes `RouteLineWidthByZoomLevel`
        // public property, which contains default values for route lines on specific zoom levels.
        RouteLineWidthByZoomLevel.multiplied(by: multiplier)
    }
    
    return lineWidthExpression
}

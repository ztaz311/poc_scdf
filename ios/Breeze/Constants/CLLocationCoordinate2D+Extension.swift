//
//  CLLocationCoordinate2D+Extension.swift
//  Breeze
//
//  Created by Zhou Hao on 24/2/21.
//

import Foundation
import CoreLocation

extension FloatingPoint {
    
    var degreesToRadians: Self { return self * .pi / 180 }
    var radiansToDegrees: Self { return self * 180 / .pi }
}

extension CLLocationCoordinate2D {
    
    func heading(to: CLLocationCoordinate2D) -> Double {
        let lat1 = self.latitude.degreesToRadians
        let lon1 = self.longitude.degreesToRadians
        
        let lat2 = to.latitude.degreesToRadians
        let lon2 = to.longitude.degreesToRadians
        
        let dLon = lon2 - lon1
        let y = sin(dLon) * cos(lat2)
        let x = cos(lat1) * sin(lat2) - sin(lat1) * cos(lat2) * cos(dLon)
        
        let headingDegrees = atan2(y, x).radiansToDegrees
        if headingDegrees >= 0 {
            return headingDegrees
        } else {
            return headingDegrees + 360
        }
    }
    
    static func geographicMidpoint(betweenCoordinates coordinates: [CLLocationCoordinate2D]) -> CLLocationCoordinate2D {
        
        guard coordinates.count > 1 else {
            return coordinates.first ?? // return the only coordinate
                CLLocationCoordinate2D(latitude: 0, longitude: 0) // return null island if no coordinates were given
        }
        
        var x = Double(0)
        var y = Double(0)
        var z = Double(0)
        
        for coordinate in coordinates {
            let lat = coordinate.latitude.toRadians()
            let lon = coordinate.longitude.toRadians()
            x += cos(lat) * cos(lon)
            y += cos(lat) * sin(lon)
            z += sin(lat)
        }
        
        x /= Double(coordinates.count)
        y /= Double(coordinates.count)
        z /= Double(coordinates.count)
        
        let lon = atan2(y, x)
        let hyp = sqrt(x * x + y * y)
        let lat = atan2(z, hyp)
        
        return CLLocationCoordinate2D(latitude: lat.toDegrees(), longitude: lon.toDegrees())
    }
    
/*
     // Seems not working well
     func degreesToRadians(degrees: Double) -> Double { return degrees * .pi / 180.0 }
     func radiansToDegrees(radians: Double) -> Double { return radians * 180.0 / .pi }

     // get bearing between self and another point
     func getBearingTo(point : CLLocationCoordinate2D) -> Double {

        let lat1 = degreesToRadians(degrees: self.latitude)
        let lon1 = degreesToRadians(degrees: self.longitude)

        let lat2 = degreesToRadians(degrees: point.latitude)
        let lon2 = degreesToRadians(degrees: point.longitude)

        let dLon = lon2 - lon1

        let y = sin(dLon) * cos(lat2)
        let x = cos(lat1) * sin(lat2) - sin(lat1) * cos(lat2) * cos(dLon)
        let radiansBearing = atan2(y, x)

        return radiansToDegrees(radians: radiansBearing)
    }
            
    // get symetric coordinate to current coordinate with bearing and distance
    func getNewTargetCoordinate(userBearing: Double, distance: Double)-> CLLocationCoordinate2D {
       let r = 6378140.0
       let latitude1 = self.latitude * (Double.pi/180) // change to radiant
       let longitude1 = self.longitude * (Double.pi/180)
       let brng = Double(userBearing) * (Double.pi/180)

       var latitude2 = asin(sin(latitude1)*cos(Double(distance)/r) + cos(latitude1)*sin(Double(distance)/r)*cos(brng));
       var longitude2 = longitude1 + atan2(sin(brng)*sin(Double(distance)/r)*cos(latitude1),cos(Double(distance)/r)-sin(latitude1)*sin(latitude2));

       latitude2 = latitude2 * (180/Double.pi)// change back to degree
       longitude2 = longitude2 * (180/Double.pi)

       // return target location
       return CLLocationCoordinate2DMake(latitude2, longitude2)
    }
    
    // Seems not working well
    static func calculateCoordinateFrom(coordinate: CLLocationCoordinate2D, onBearing bearingInRadians: Double, atDistance distanceInMetres: Double) -> CLLocationCoordinate2D {
        
        let coordinateLatitudeInRadians = coordinate.latitude * Double.pi / 180;
        let coordinateLongitudeInRadians = coordinate.longitude * Double.pi / 180;
        
        let distanceComparedToEarth = distanceInMetres / 6378100;
        
        let resultLatitudeInRadians = asin(sin(coordinateLatitudeInRadians) * cos(distanceComparedToEarth) + cos(coordinateLatitudeInRadians) * sin(distanceComparedToEarth) * cos(bearingInRadians));
        let resultLongitudeInRadians = coordinateLongitudeInRadians + atan2(sin(bearingInRadians) * sin(distanceComparedToEarth) * cos(coordinateLatitudeInRadians), cos(distanceComparedToEarth) - sin(coordinateLatitudeInRadians) * sin(resultLatitudeInRadians));
                
        let latitude = resultLatitudeInRadians * 180 / Double.pi
        let longitude = resultLongitudeInRadians * 180 / Double.pi
        return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
*/
}

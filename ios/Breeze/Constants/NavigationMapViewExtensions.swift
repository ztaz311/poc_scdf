//
//  NavigationMapViewExtensions.swift
//  Breeze
//
//  Created by Zhou Hao on 17/5/21.
//

import Foundation
import MapboxNavigation
import MapboxCoreNavigation
import MapboxCoreMaps
import MapboxMaps
import SwiftyBeaver

// Since the dynamic color not working for the route traffic, please refer to Mapbox #281
extension NavigationMapView {

    // customized navigation map view route color
    func setupRouteColor(isDarkMode: Bool) {
        
        self.routeCasingColor = .clear // isDarkMode ? UIColor.routeCasingColorLight : UIColor.routeCasingColorDark // casing color
        self.routeAlternateCasingColor = .clear // isDarkMode ? UIColor.routeAlternateCasingLight : UIColor.routeAlternateCasingDark
        self.trafficLowColor = isDarkMode ? UIColor.trafficLowColorDark : UIColor.trafficLowColor
        self.trafficModerateColor = isDarkMode ? UIColor.trafficModerateColorDark : UIColor.trafficModerateColor
        self.trafficHeavyColor = isDarkMode ? UIColor.trafficHeavyColorDark : UIColor.trafficHeavyColor
        self.trafficSevereColor = isDarkMode ? UIColor.trafficSevereColorDark : UIColor.trafficSevereColor
        self.trafficUnknownColor = isDarkMode ? UIColor.trafficUnknownColorDark : UIColor.trafficUnknownColor
        self.showsCongestionForAlternativeRoutes = true
        
        self.alternativeTrafficLowColor = isDarkMode ? UIColor.alternateTrafficLowColorDark : UIColor.alternateTrafficLowColor
        self.alternativeTrafficModerateColor = isDarkMode ? UIColor.alternatTrafficModerateColorDark : UIColor.alternatTrafficModerateColor
        self.alternativeTrafficHeavyColor = isDarkMode ? UIColor.alternateTrafficHeavyColorDark : UIColor.alternateTrafficHeavyColor
        self.alternativeTrafficSevereColor = isDarkMode ? UIColor.alternateTrafficSevereColorDark : UIColor.alternateTrafficSevereColor
        self.alternativeTrafficUnknownColor = isDarkMode ? UIColor.alternateTrafficUnknownColorDark : UIColor.alternateTrafficUnknownColor
    }
    
    func carPlayZoomIn(){
        
        
        guard let mapView = self.mapView else { return }
        
        self.navigationCamera.stop()
        
        var cameraOptions = CameraOptions(cameraState: mapView.cameraState)
        cameraOptions.zoom = mapView.cameraState.zoom + 1.0
        mapView.mapboxMap.setCamera(to: cameraOptions)
    }
    
    func carPlayZoomOut(){
       
        guard let mapView = self.mapView else { return }
        
        self.navigationCamera.stop()
        
        var cameraOptions = CameraOptions(cameraState: mapView.cameraState)
        cameraOptions.zoom = mapView.cameraState.zoom - 1.0
        mapView.mapboxMap.setCamera(to: cameraOptions)
    }
    
    func userLocationPuckIcon(isStarted:Bool){
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CarplayHomepage.UserClick.mapbox_pan_recenter, screenName: ParameterName.CarplayHomepage.screen_view)
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.CarplayCruise.UserClick.mapbox_pan_recenter, screenName: ParameterName.CarplayCruise.screen_view)
        
        self.mapView.ornaments.options.compass.visibility = .hidden
        
        var traitCollection = UITraitCollection(userInterfaceStyle: .light)
        if(Settings.shared.theme == 0){
            
            if(appDelegate().isDarkMode()){
                
                traitCollection = UITraitCollection(userInterfaceStyle: .dark)
                
            }
            
        }
        else{
            
            traitCollection = UITraitCollection(userInterfaceStyle: UIUserInterfaceStyle(rawValue: Settings.shared.theme) ?? .unspecified)
        }
        
        if isStarted {
            
            self.userLocationStyle = .puck2D(configuration: Puck2DConfiguration(topImage: UIImage(named: "userPuck",in:nil,compatibleWith: traitCollection), bearingImage: UIImage(named: "userPuckArrow",in:nil,compatibleWith: traitCollection), shadowImage: nil, scale: .constant(1.0)))
        } else {
            self.userLocationStyle = .puck2D(configuration: Puck2DConfiguration(topImage: UIImage(named: "puckArrow",in:nil,compatibleWith: traitCollection), bearingImage: UIImage(named: "puckIcon"), shadowImage: nil, scale: nil))
        }
        
    }
    
    /// Add dotted line between two coordinates
    func addDottedLine(coordinate1: CLLocationCoordinate2D, coordinate2: CLLocationCoordinate2D) {
        
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            let sourceIdentifier = "DottedLine"
            // Create a GeoJSON data source.
            var routeLineSource = GeoJSONSource()
            routeLineSource.data = .feature(Turf.Feature(geometry: LineString([coordinate1, coordinate2])))
            
            // Create a line layer
            var lineLayer = LineLayer(id: "dotted-line-layer")
            lineLayer.source = sourceIdentifier
            lineLayer.lineColor = (self.traitCollection.userInterfaceStyle == .dark) ? .constant(.init(Values.dottedLineDarkColor)) : .constant(.init(Values.dottedLineLightColor))
            lineLayer.lineWidth = .expression(lineWidthExpression(0.2))
            lineLayer.lineJoin = .constant(.round)
            lineLayer.lineCap = .constant(.square)
            lineLayer.lineDasharray = .constant([1.0, 1.0])

            // Add the lineLayer to the map.
            do {
                try self.mapView.mapboxMap.style.addSource(routeLineSource, id: sourceIdentifier)
                try self.mapView.mapboxMap.style.addPersistentLayer(lineLayer, layerPosition: .below("puck"))
            } catch(let error) {
                SwiftyBeaver.error("Failed to addDottedLine: \(error.localizedDescription)")
            }
        }
    }
    
    func removeDottedLine() {
        do {
            try self.mapView.mapboxMap.style.removeLayer(withId: "dotted-line-layer")
            try self.mapView.mapboxMap.style.removeSource(withId: "DottedLine")
        } catch(let error) {
            SwiftyBeaver.error("Failed to removeDottedLine: \(error.localizedDescription)")
        }
    }
    
}

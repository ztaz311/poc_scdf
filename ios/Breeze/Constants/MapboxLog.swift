//
//  MapboxLog.swift
//  Breeze
//
//  Created by Zhou Hao on 7/9/21.
//

import Foundation
import SwiftyBeaver
@_implementationOnly import MapboxCommon_Private

class MapboxLog: LogWriterBackend {
    func writeLog(for level: LoggingLevel, message: String) {
        switch level {
        case LoggingLevel.debug:
            SwiftyBeaver.debug(message)
        case LoggingLevel.info:
            SwiftyBeaver.info(message)
        case LoggingLevel.warning:
            SwiftyBeaver.warning(message)
        case LoggingLevel.error:
            SwiftyBeaver.error(message)
        default:
            SwiftyBeaver.error("Unknow: \(message)")
        }
    }
    
    func writeLog(for level: LoggingLevel, message: String, category: String?) {
        writeLog(for: level, message: message)
    }
}

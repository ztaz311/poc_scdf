//
//  NearByERP.swift
//  Breeze
//
//  Created by VishnuKanth on 24/01/22.
//

import Foundation
import Turf

struct NearByERPFilter: Codable {
    let distance: Double
    let erpFeatures:Turf.Feature?
    
    enum CodingKeys: String, CodingKey {
        case distance = "distance"
        case erpFeatures = "erpFeatures"
    }

    
}

//
//  BreezeToastStyle.swift
//  Breeze
//
//  Created by Zhou Hao on 4/3/22.
//

import UIKit

final class BreezeToastStyle {
    static let carparkToast = BreezeToastAppearance(backgroundColor: UIColor(named:"carparkToastBGColor")!  , borderColor: UIColor(named:"carparkToastBorderColor")!, cornerRadius: 12.0, primaryIconName: "nearby", preferredWidth: UIScreen.main.bounds.width - 48, actionTextColor: UIColor(named:"carparkToastBorderColor")!)
    static let tripSaveToast = BreezeToastAppearance(backgroundColor: UIColor(named:"rpToastBGColor")!  , borderColor: UIColor(named:"rpToastBorderColor")!, cornerRadius: 12.0, primaryIconName: "greenTick", preferredWidth: UIScreen.main.bounds.width - 48, actionTextColor: UIColor(named:"rpToastBorderColor")!)
    
    static let voucherNotification = BreezeToastAppearance(backgroundColor: UIColor(named:"rpToastBGColor")!  , borderColor: UIColor(named:"rpToastBorderColor")!, cornerRadius: 12.0, primaryIconName: "ic_money_tooltip", preferredWidth: UIScreen.main.bounds.width - 48, actionTextColor: UIColor(named:"rpToastBorderColor")!)
    
    static let globalNotification = BreezeToastAppearance(backgroundColor: UIColor(named:"globalNotificationBGColor")!  , borderColor: UIColor(named:"globalNotificationBorderColor")!, cornerRadius: 12.0, primaryIconName: "Accident", preferredWidth: UIScreen.main.bounds.width - 48, actionTextColor: UIColor(named:"globalNotificationBorderColor")!)
    static let adminNotification = BreezeToastAppearance(backgroundColor: UIColor(named:"adminNotificationBGColor")!  , borderColor: UIColor(named:"adminNotificationBorderColor")!, cornerRadius: 12.0, primaryIconName: "announcement", preferredWidth: UIScreen.main.bounds.width - 48, actionTextColor: UIColor(named:"adminNotificationBorderColor")!)

    static let upcomingTrip = BreezeToastAppearance(backgroundColor: UIColor(named:"upcomingToastBGColor")!  , borderColor: UIColor(named:"upcomingToastBorderColor")!, cornerRadius: 12.0, primaryIconName: "toastUpcomingTrip", preferredWidth: UIScreen.main.bounds.width - 48, actionTextColor: UIColor(named:"upcomingToastActionColor")!)
    
    static let chargeNotificationErpOBU = BreezeToastAppearance(backgroundColor: UIColor(hexString: "#EBF4FF", alpha: 1.0), borderColor: UIColor(hexString: "#005FD0", alpha: 1.0), cornerRadius: 12.0, primaryIconName: "cpERPIcon", preferredWidth: UIScreen.main.bounds.width - 48, actionTextColor: UIColor(hexString: "#222638", alpha: 1.0))
    
    static let chargeNotificationParkingOBU = BreezeToastAppearance(backgroundColor: UIColor(hexString: "#782EB1", alpha: 1.0), borderColor: UIColor(hexString: "#F5EEFC", alpha: 1.0), cornerRadius: 12.0, primaryIconName: "cp-parkingFee", preferredWidth: UIScreen.main.bounds.width - 48, actionTextColor: UIColor(hexString: "#222638", alpha: 1.0))
    
    static let chargeNotificationLowCardOBU = BreezeToastAppearance(backgroundColor: UIColor(hexString: "#FDEDF3", alpha: 1.0), borderColor: UIColor(hexString: "#E82370", alpha: 1.0), cornerRadius: 12.0, primaryIconName: "carplay-Miscellaneous-Icon", preferredWidth: UIScreen.main.bounds.width - 48, actionTextColor: UIColor(hexString: "#FDEDF3", alpha: 1.0))
    
}

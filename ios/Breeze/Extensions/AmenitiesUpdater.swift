//
//  AmenitiesUpdater.swift
//  Breeze
//
//  Created by Zhou Hao on 30/11/21.
//

import Foundation
@_spi(Experimental) import MapboxMaps
import MapboxCoreMaps
import Turf
import SwiftyBeaver
import MapboxMaps
import Kingfisher
import UIKit

extension MapView {
    
    func removeExistingAmenityImages(type:String){
        DispatchQueue.main.async {
        do {
          try self.mapboxMap.style.removeImage(withId: type)
        }catch {
            SwiftyBeaver.error("Failed to remove Amenity Image.")
        }
        }
    }
    
    func addImagesToMapStyle(type:String,urlString:String){
        
        
        self.downloadImage(with: urlString) { image in
            
            guard let image  = image else { return}
            do {
             try self.mapboxMap.style.addImage(image, id: type, stretchX: [], stretchY: [])
                SwiftyBeaver.debug("Added Amenity Image to map style.")
            }catch {
                SwiftyBeaver.error("Failed to Add Amenity Image.")
            }
        }
    }
    
    /// Download light mode and dark mode amenity image at the same time
    func addDynamicImagesToMapStyle(type:String,urlString:String, darkModeUrlString: String){
        self.downloadImage(with: urlString) { image in
            guard let image  = image else { return}
            
            self.downloadImage(with: darkModeUrlString) { darkImage in
                guard let darkImage = darkImage else { return }
                
//                image.registerDarkImage(darkImage) // register the dark mode image to make it a dynamical image
                let image = UIImage.dynamicImage(withLight: image, dark: darkImage)
                DispatchQueue.main.async {
                do {
                 try self.mapboxMap.style.addImage(image, id: type, stretchX: [], stretchY: [])
                    SwiftyBeaver.debug("Added Amenity Image to map style.")
                }catch {
                    SwiftyBeaver.error("Failed to Add Amenity Image.")
                }
                }
            }
        }
    }
    
    func addDynamicImagesMapStyleFromContentScreen(type:String,urlString:String, darkModeUrlString: String,imageCompletionHandler: @escaping (Bool) -> Void){
        
        self.downloadImage(with: urlString) { image in
            guard let image  = image else { return}
            
            self.downloadImage(with: darkModeUrlString) { darkImage in
                guard let darkImage = darkImage else { return }
                
//                image.registerDarkImage(darkImage) // register the dark mode image to make it a dynamical image
                let image = UIImage.dynamicImage(withLight: image, dark: darkImage)
                DispatchQueue.main.async {
                do {
                 try self.mapboxMap.style.addImage(image, id: type, stretchX: [], stretchY: [])
                    imageCompletionHandler(true)
                    SwiftyBeaver.debug("Added Amenity Image to map style.")
                }catch {
                    imageCompletionHandler(true)
                    SwiftyBeaver.error("Failed to Add Amenity Image.")
                }
                }
            }
        }
    }
    
    func addBookmarkImagesToMapStyle(types:[String]){
        DispatchQueue.main.async {
            
            var traitCollection = UITraitCollection(userInterfaceStyle: .light)
            if(Settings.shared.theme == 0){
                
                if(appDelegate().isDarkMode()){
                    
                    traitCollection = UITraitCollection(userInterfaceStyle: .dark)
                    
                }
                
            }
            else{
                
                traitCollection = UITraitCollection(userInterfaceStyle: UIUserInterfaceStyle(rawValue: Settings.shared.theme) ?? .unspecified)
            }
            
        do {
            for type in types {
                try self.mapboxMap.style.addImage(UIImage(named: "\(type)-selected",in:nil,compatibleWith: traitCollection)!, id: "\(type)-selected", stretchX: [], stretchY: [])
                try self.mapboxMap.style.addImage(UIImage(named: "\(type)",in:nil,compatibleWith: traitCollection)!, id: "\(type)", stretchX: [], stretchY: [])
            }
            SwiftyBeaver.debug("Added Amenity Image to map style.")
        }catch {
            SwiftyBeaver.error("Failed to Add Amenity Image.")
        }
        }
        
    }
    
    func removeBookmarkImagesToMapStyle(type:String){
        DispatchQueue.main.async {
            
        do {
            try self.mapboxMap.style.removeImage(withId: "\(type)-selected")
            try self.mapboxMap.style.removeImage(withId: "\(type)")
            
            SwiftyBeaver.debug("Remove Traffic Image from map style.")
        }catch {
            SwiftyBeaver.error("Failed to remove traffic Image.")
        }
        }
    }
    
    func addTrafficImagesToMapStyle(type:String){
        DispatchQueue.main.async {
            
            var traitCollection = UITraitCollection(userInterfaceStyle: .light)
            if(Settings.shared.theme == 0){
                
                if(appDelegate().isDarkMode()){
                    
                    traitCollection = UITraitCollection(userInterfaceStyle: .dark)
                    
                }
                
            }
            else{
                
                traitCollection = UITraitCollection(userInterfaceStyle: UIUserInterfaceStyle(rawValue: Settings.shared.theme) ?? .unspecified)
            }
            
        do {
            try self.mapboxMap.style.addImage(UIImage(named: "traffic_camera_selected",in:nil,compatibleWith: traitCollection)!, id: "\(type)-selected", stretchX: [], stretchY: [])
            try self.mapboxMap.style.addImage(UIImage(named: "traffic_camera_unselected",in:nil,compatibleWith: traitCollection)!, id: "\(type)", stretchX: [], stretchY: [])

            SwiftyBeaver.debug("Added Amenity Image to map style.")
        }catch {
            SwiftyBeaver.error("Failed to Add Amenity Image.")
        }
        }
        
    }
    
    func addCircle(){
        DispatchQueue.main.async {
            
            var traitCollection = UITraitCollection(userInterfaceStyle: .light)
            if(Settings.shared.theme == 0){
                
                if(appDelegate().isDarkMode()){
                    
                    traitCollection = UITraitCollection(userInterfaceStyle: .dark)
                    
                }
                
            }
            else{
                
                traitCollection = UITraitCollection(userInterfaceStyle: UIUserInterfaceStyle(rawValue: Settings.shared.theme) ?? .unspecified)
            }
            
        do {
            try self.mapboxMap.style.addImage(UIImage(named: "fill",in:nil,compatibleWith: traitCollection)!, id: "fill-zone", stretchX: [], stretchY: [])
            

            SwiftyBeaver.debug("Added Amenity Image to map style.")
        }catch {
            SwiftyBeaver.error("Failed to Add Amenity Image.")
        }
        }
        
    }
    
    func removeTrafficImagesToMapStyle(type:String){
        DispatchQueue.main.async {
            
        do {
            try self.mapboxMap.style.removeImage(withId: "\(type)-selected")
            try self.mapboxMap.style.removeImage(withId: "\(type)")
            
            SwiftyBeaver.debug("Remove Traffic Image from map style.")
        }catch {
            SwiftyBeaver.error("Failed to remove traffic Image.")
        }
        }
    }
    
    private func downloadImage(with urlString : String , imageCompletionHandler: @escaping (UIImage?) -> Void){
        guard let url = URL.init(string: urlString) else {
            return  imageCompletionHandler(nil)
        }
        let resource = ImageResource(downloadURL: url)
        
        KingfisherManager.shared.retrieveImage(with: resource, options: nil, progressBlock: nil) { result in
            switch result {
            case .success(let value):
                imageCompletionHandler(value.image)
            case .failure:
                imageCompletionHandler(nil)
            }
        }
    }
    
    func removeAmenitiesLayerOfCluster(type:String) {
        let layerIds = [
            "_\(type)-clustered-circle-layer",
            "_\(type)-unclustered-circle-layer",
            "_\(type)"
        ]
        
        let sourceIds = [
            "__\(type)",
        ]
        
        self.removeLayerIds(layerIds: layerIds)
        self.removeSourceIds(sourceIds: sourceIds)
    }
    
    func removeAmenitiesLayerOfType(type:String){
        
        self.removeAmenitiesLayerOfCluster(type: type)
        do {
            try self.mapboxMap.style.removeLayer(withId: "_\(type)")
            try self.mapboxMap.style.removeSource(withId: "__\(type)")
        } catch (let error) {
            SwiftyBeaver.error("Failed to remove removeAmenitiesLayer.\(error.localizedDescription)")
        }
        
    }
    
    func addAmenitiesToMapOfTypeWithClustering(features:Turf.FeatureCollection, isBelowPuck: Bool,type:String,aboveID:Bool = true){
        
        do {
           
           // let image = self.mapboxMap.style.image(withId: type)
            var geoJSONSource = GeoJSONSource()
            geoJSONSource.cluster = true
            geoJSONSource.clusterMaxZoom = Values.clusterMaxZoom
            geoJSONSource.clusterRadius = Values.clusterRadius
            geoJSONSource.generateId = true
            geoJSONSource.data = .featureCollection(features)
            
            var clusteredLayer = createClusteredLayer(type:type)
            clusteredLayer.source = "__\(type)"
            
            var unClusteredSymboLayer = createUnclusteredLayer(type: type)
            unClusteredSymboLayer.source = "__\(type)"
            
            // Add the source and style layers to the map style.
            try self.mapboxMap.style.addSource(geoJSONSource, id: "__\(type)")
            try self.mapboxMap.style.addPersistentLayer(clusteredLayer, layerPosition: .below("puck"))

            try self.mapboxMap.style.addPersistentLayer(unClusteredSymboLayer, layerPosition: .below("puck"))
            //try self.mapboxMap.style.addLayer(clusterCountLayer)
            SwiftyBeaver.debug("addAmenitiesToMap \(features.features.count) \(type)")
        }
        catch (let error) {
            SwiftyBeaver.error("Failed to addAmenitiesToMap: \(error.localizedDescription)")
        }
    }
    
    func addBookmarkAmentiesToMapOfType(features:Turf.FeatureCollection, isBelowPuck: Bool,type:String,aboveID:Bool = true) {
        
        do {
           
           // let image = self.mapboxMap.style.image(withId: type)
            var geoJSONSource = GeoJSONSource()
            geoJSONSource.generateId = true
            geoJSONSource.data = .featureCollection(features)
            var symbolLayer = SymbolLayer(id: "_\(type)")
            let iconSize = Exp(.interpolate){
                Exp(.linear)
                Exp(.zoom)
                0
                1
                9
                1
                10.9
                1
                11
                1
                13
                1
            }
            
            
            symbolLayer.source = "__\(type)"
            symbolLayer.iconAnchor = .constant(.center)
            symbolLayer.minZoom =  0.0
            symbolLayer.iconSize = .expression(iconSize)
//            symbolLayer.iconImage = .constant(.name(type))
            
            symbolLayer.iconImage = .expression(
                Exp(.switchCase) {
                    Exp(.boolean) {
                        Exp(.get) { "isSelected" }
                        false
                    }
                    "\(type)-selected"
                    "\(type)"
                }
            )
            
            
            
            //symbolLayer.iconImage = .expression(iconExpression)
            symbolLayer.iconAllowOverlap = .constant(true)
            symbolLayer.iconIgnorePlacement = .constant(true)
            // Add the source and style layers to the map style.
            try self.mapboxMap.style.addSource(geoJSONSource, id: "__\(type)")

            try self.mapboxMap.style.addPersistentLayer(with: symbolLayer.jsonObject(), layerPosition: .below("puck"))
            SwiftyBeaver.debug("addAmenitiesToMap \(features.features.count) \(type)")
        }
        catch (let error) {
            SwiftyBeaver.error("Failed to addAmenitiesToMap: \(error.localizedDescription)")
        }
        
    }
    
    
    
    func addShareLocationOnMap(features:Turf.FeatureCollection, isBelowPuck: Bool,type:String,aboveID:Bool = true, code: String?) {
        DispatchQueue.main.async {
            do {
                if code == "HOME" {
                    try self.mapboxMap.style.addImage(UIImage(named: "home-location-icon")!, id: "home-location-icon",stretchX: [],stretchY: [])
                } else if code == "WORK" {
                    try self.mapboxMap.style.addImage(UIImage(named: "work-location-icon")!, id: "work-location-icon",stretchX: [],stretchY: [])
                }  else {
                    try self.mapboxMap.style.addImage(UIImage(named: "destinationMark")!, id: "destinationMark",stretchX: [],stretchY: [])
                }
                
                
                
                var geoJSONSource = GeoJSONSource()
                geoJSONSource.data = .featureCollection(features)
                
                var symbolLayer = SymbolLayer(id: "_\(type)")
                let iconSize = Exp(.interpolate){
                    Exp(.linear)
                    Exp(.zoom)
                    0
                    0.9
                    9
                    0.9
                    10.9
                    0.9
                    11
                    0.9
                    13
                    0.9
                }
                symbolLayer.minZoom = 0.0
                symbolLayer.iconSize = .expression(iconSize)
                symbolLayer.source = "__\(type)"
                
                symbolLayer.iconImage = .constant(ResolvedImage.name("destinationMark"))
                
                symbolLayer.iconAllowOverlap = .constant(true)
                symbolLayer.iconIgnorePlacement = .constant(true)
                symbolLayer.iconAnchor = .constant(.center)
                symbolLayer.iconOffset = .constant([0, 0])
                
                try self.mapboxMap.style.addSource(geoJSONSource, id: "__\(type)")
                try self.mapboxMap.style.addPersistentLayer(with: symbolLayer.jsonObject(), layerPosition: isBelowPuck ? .above("puck") : .above(Constants.Layer.illegalparkingcamera))
                SwiftyBeaver.debug("share location add to map \(features.features.count) \(type)")
            } catch (let error) {
                SwiftyBeaver.error("share location falied: \(error.localizedDescription)")
            }
        }
        
    }
    
    func addContentAmentiesToMapOfType(features:Turf.FeatureCollection, type:String, hasPOI: Bool = false){
        
        do {
           
           // let image = self.mapboxMap.style.image(withId: type)
            var geoJSONSource = GeoJSONSource()
            geoJSONSource.generateId = true
            geoJSONSource.data = .featureCollection(features)
            var symbolLayer = SymbolLayer(id: "_\(type)")
            let iconSize = Exp(.interpolate){
                Exp(.linear)
                Exp(.zoom)
                0
                0.35
                9
                0.35
                10.9
                0.35
                11
                0.35
                13
                0.35
            }
            
            
            symbolLayer.source = "__\(type)"
            symbolLayer.iconAnchor = .constant(.center)
            symbolLayer.minZoom =  0.0
            symbolLayer.iconSize = .expression(iconSize)
//            symbolLayer.iconImage = .constant(.name(type))
            
            let iconImage = Exp(.get){
                "provider"
            }
            symbolLayer.iconImage = .expression(iconImage)
            
            
            
            //symbolLayer.iconImage = .expression(iconExpression)
            symbolLayer.iconAllowOverlap = .constant(true)
            symbolLayer.iconIgnorePlacement = .constant(true)
            // Add the source and style layers to the map style.
            try self.mapboxMap.style.addSource(geoJSONSource, id: "__\(type)")

            if hasPOI {
                try self.mapboxMap.style.addPersistentLayer(with: symbolLayer.jsonObject(), layerPosition: .below("_\(Values.POI)"))
            }else {
                try self.mapboxMap.style.addPersistentLayer(with: symbolLayer.jsonObject(), layerPosition: .below("puck"))
            }
            SwiftyBeaver.debug("addAmenitiesToMap \(features.features.count) \(type)")
        }
        catch (let error) {
            SwiftyBeaver.error("Failed to addAmenitiesToMap: \(error.localizedDescription)")
        }
        
    }
    
    func addPOIAmentiesToMapOfType(features:Turf.FeatureCollection, isBelowPuck: Bool,type:String,aboveID:Bool = true){
        
        do {
           
           // let image = self.mapboxMap.style.image(withId: type)
            var geoJSONSource = GeoJSONSource()
            geoJSONSource.generateId = true
            geoJSONSource.data = .featureCollection(features)
            var symbolLayer = SymbolLayer(id: "_\(type)")
            let iconSize = Exp(.interpolate){
                Exp(.linear)
                Exp(.zoom)
                0
                0.35
                9
                0.35
                10.9
                0.35
                11
                0.35
                13
                0.35
            }
            
            
            symbolLayer.source = "__\(type)"
            symbolLayer.iconAnchor = .constant(.center)
            symbolLayer.minZoom =  0.0
            symbolLayer.iconSize = .expression(iconSize)
//            symbolLayer.iconImage = .constant(.name(type))
            
            let iconImage = Exp(.get){
                "provider"
            }
            symbolLayer.iconImage = .expression(iconImage)
            
            
            
            //symbolLayer.iconImage = .expression(iconExpression)
            symbolLayer.iconAllowOverlap = .constant(true)
            symbolLayer.iconIgnorePlacement = .constant(true)
            // Add the source and style layers to the map style.
            try self.mapboxMap.style.addSource(geoJSONSource, id: "__\(type)")

            try self.mapboxMap.style.addPersistentLayer(with: symbolLayer.jsonObject(), layerPosition: .below("puck"))
            SwiftyBeaver.debug("addAmenitiesToMap \(features.features.count) \(type)")
        }
        catch (let error) {
            SwiftyBeaver.error("Failed to addAmenitiesToMap: \(error.localizedDescription)")
        }
        
    }

    
    func addAmentiesToMapOfType(features:Turf.FeatureCollection, isBelowPuck: Bool,type:String,aboveID:Bool = true){
        
        do {
           
           // let image = self.mapboxMap.style.image(withId: type)
            var geoJSONSource = GeoJSONSource()
            geoJSONSource.generateId = true
            geoJSONSource.data = .featureCollection(features)
            var symbolLayer = SymbolLayer(id: "_\(type)")
            let iconSize = Exp(.interpolate){
                Exp(.linear)
                Exp(.zoom)
                0
                0
                7
                0.35
                10.9
                0.35
                11
                0.35
                22
                0.35
            }
            
            let trafficIconSize = Exp(.interpolate){
                Exp(.linear)
                Exp(.zoom)
                0
                0
                9
                1.0
                10.9
                1.0
                11
                1.0
                22
                1.0
            }
            
            //Expression for different types of images based on feature state
//            symbolLayer.iconImage = .expression(
//                Exp(.switchCase) {
//                    Exp(.boolean) {
//                        Exp(.featureState) { "selected" }
//                        false
//                    }
//                    "_dsmuseum"
//                    type
//                }
//            )
            
            
            symbolLayer.source = "__\(type)"
            symbolLayer.iconAnchor = .constant(.center)
            symbolLayer.minZoom = type == Values.TRAFFIC ? 7 : 7.0
            symbolLayer.iconSize = .expression(type == Values.TRAFFIC ? trafficIconSize : iconSize)
            symbolLayer.iconImage = .expression(
                Exp(.switchCase) {
                    Exp(.boolean) {
                        Exp(.get) { "isSelected" }
                        false
                    }
                    "\(type)-selected"
                    "\(type)"
                }
            )
            
            //symbolLayer.iconImage = .expression(iconExpression)
            symbolLayer.iconAllowOverlap = .constant(true)
            symbolLayer.iconIgnorePlacement = .constant(true)
            // Add the source and style layers to the map style.
            try self.mapboxMap.style.addSource(geoJSONSource, id: "__\(type)")

            try self.mapboxMap.style.addPersistentLayer(with: symbolLayer.jsonObject(), layerPosition: .below("puck"))
            SwiftyBeaver.debug("addAmenitiesToMap \(features.features.count) \(type)")
        }
        catch (let error) {
            SwiftyBeaver.error("Failed to addAmenitiesToMap: \(error.localizedDescription)")
        }
        
    }
    
    func createClusteredLayer(type:String) -> CircleLayer {
        // Create a symbol layer to represent the clustered points.
        var clusteredLayer = CircleLayer(id: "_\(type)-clustered-circle-layer")

        // Filter out unclustered features by checking for `point_count`. This
        // is added to clusters when the cluster is created. If your source
        // data includes a `point_count` property, consider checking
        // for `cluster_id`.
        clusteredLayer.filter = Exp(.has) { "point_count" }

        clusteredLayer.circleColor = .constant(.init(AmenitiesSharedInstance.shared.fetchClusteredColor(type: type) ?? UIColor.red))
        clusteredLayer.circleRadius = .constant(Values.circleRadius)

        return clusteredLayer
    }
    
    func createUnclusteredLayer(type:String) -> SymbolLayer {
        // Create a symbol layer to represent the points that aren't clustered.
        var symbolLayer = SymbolLayer(id: "_\(type)-unclustered-circle-layer")
        
        // Filter out clusters by checking for `point_count`.
        symbolLayer.filter = Exp(.not) {
            Exp(.has) { "point_count" }
        }
        
        let iconSize = Exp(.interpolate){
            Exp(.linear)
            Exp(.zoom)
            0
            0
            7
            0.36
            10.9
            0.35
            11
            0.35
            22
            0.35
        }
        
        let trafficIconSize = Exp(.interpolate){
            Exp(.linear)
            Exp(.zoom)
            0
            0
            9
            1.0
            10.9
            1.0
            11
            1.0
            22
            1.0
        }
        
        
        
        symbolLayer.iconAnchor = .constant(.center)
        symbolLayer.minZoom = type == Values.TRAFFIC ? 7 : 7.0
        symbolLayer.iconSize = .expression(type == Values.TRAFFIC ? trafficIconSize : iconSize)
//            symbolLayer.iconImage = .constant(.name(type))
        
        symbolLayer.iconImage = .expression(
            Exp(.switchCase) {
                Exp(.boolean) {
                    Exp(.get) { "isSelected" }
                    false
                }
                "\(type)-selected"
                "\(type)"
            }
        )

/*
        symbolLayer.iconImage = .expression(
            Exp(.switchCase) {
                Exp(.eq) {
                    Exp(.get) { "type" }
                    Values.PARKS
                }
                "\(Values.PARKS)"
                Exp(.eq) {
                    Exp(.get) { "type" }
                    Values.PCN
                }
                "\(Values.PCN)"
                Exp(.eq) {
                    Exp(.get) { "type" }
                    Values.PLAYGROUND
                }
                "\(Values.PLAYGROUND)"
                Exp(.eq) {
                    Exp(.get) { "type" }
                    Values.HAWKERCENTRE
                }
                "\(Values.HAWKERCENTRE)"
                Exp(.eq) {
                    Exp(.get) { "type" }
                    Values.RESTAURANT
                }
                "\(Values.RESTAURANT)"
                Exp(.eq) {
                    Exp(.get) { "type" }
                    Values.PASARMALAM
                }
                "\(Values.PASARMALAM)"
                Exp(.eq) {
                    Exp(.get) { "type" }
                    Values.PETROL
                }
                "\(Values.PETROL)"
                Exp(.eq) {
                    Exp(.get) { "type" }
                    Values.EV_CHARGER
                }
                "\(Values.EV_CHARGER)"
                Exp(.eq) {
                    Exp(.get) { "type" }
                    Values.MUSEUM
                }
                "\(Values.MUSEUM)"
                Exp(.eq) {
                    Exp(.get) { "type" }
                    Values.LIBRARY
                }
                "\(Values.LIBRARY)"
                Exp(.eq) {
                    Exp(.get) { "type" }
                    Values.HERITAGETREE
                }
                "\(Values.HERITAGETREE)"
                Exp(.eq) {
                    Exp(.get) { "type" }
                    Values.POI
                }
                "\(Values.POI)"
                ""
            }
            
        )
 */

//        symbolLayer.iconImage = .expression(
//            Exp(.switchCase) {
//                Exp(.boolean) {
//                    Exp(.get) { "isSelected" }
//                    false
//                }
//                "\(Values.PARKS)-selected"
//                "\(Values.PARKS)"
//            }
//        )
        
        //symbolLayer.iconImage = .expression(iconExpression)
        symbolLayer.iconAllowOverlap = .constant(true)
        symbolLayer.iconIgnorePlacement = .constant(true)

        return symbolLayer
    }
    
    func removeAmenitiesPetrolAndEVLayer(type:String){
        DispatchQueue.main.async {
            do {
                try self.mapboxMap.style.removeLayer(withId: "_\(type)")
                try self.mapboxMap.style.removeSource(withId: "__\(type)")
            } catch {
                SwiftyBeaver.error("Failed to remove removeAmenitiesLayer.")
            }
        }
    }
    
    func addPetrolAmentiesToMap(features:Turf.FeatureCollection, isBelowPuck: Bool,type:String){
        DispatchQueue.main.async {
        do {
           
           // let image = self.mapboxMap.style.image(withId: type)
            var geoJSONSource = GeoJSONSource()
            geoJSONSource.data = .featureCollection(features)
            
            var symbolLayer = SymbolLayer(id: "_\(type)")
            
//            symbolLayer.iconImage = .expression(Exp(.get) {
//                        "image_id"
//            })
            symbolLayer.iconImage = .expression(
                Exp(.switchCase) {
                    Exp(.boolean) {
                        Exp(.get) { "isSelected" }
                        false
                    }
                    "\(type)-selected"
                    "\(type)"
                }
            )

            symbolLayer.source = "__\(type)"
            symbolLayer.iconSize = .constant(0.35)
            
            //symbolLayer.iconImage = .constant(.name(type))
            symbolLayer.iconAllowOverlap = .constant(true)
            symbolLayer.iconIgnorePlacement = .constant(true)
            // Add the source and style layers to the map style.
            try self.mapboxMap.style.addSource(geoJSONSource, id: "__\(type)")

            try self.mapboxMap.style.addLayer(symbolLayer, layerPosition: .below("puck"))
            SwiftyBeaver.debug("addAmenitiesToMap \(features.features.count) \(type)")
        }
        catch (let error) {
            SwiftyBeaver.error("Failed to addAmenitiesToMap: \(error.localizedDescription)")
        }
        }
    }
    
    
    
    func addPetrolEvToMapTurnbyTurn(features:Turf.FeatureCollection, type:String, isBelowPuck: Bool, isSelectedAmenity: Bool){
        DispatchQueue.main.async {
        do {
            if type == "petrol" {
                if isSelectedAmenity {
                    try self.mapboxMap.style.addImage(UIImage(named: "petrol-iconV2-selected")!, id: "petrol-iconV2-selected",stretchX: [],stretchY: [])
                } else {
                    try self.mapboxMap.style.addImage(UIImage(named: "petrol-iconV2")!, id: "petrol-iconV2",stretchX: [],stretchY: [])
                }
            } else {
                if isSelectedAmenity {
                    try self.mapboxMap.style.addImage(UIImage(named: "ev-charger-iconV2-selected")!, id: "ev-charger-iconV2-selected",stretchX: [],stretchY: [])
                } else {
                    try self.mapboxMap.style.addImage(UIImage(named: "ev-charger-iconV2")!, id: "ev-charger-iconV2",stretchX: [],stretchY: [])
                }
            }
            
            
            var geoJSONSource = GeoJSONSource()
            geoJSONSource.data = .featureCollection(features)
            
            var symbolLayer = SymbolLayer(id: "_\(type)")
            symbolLayer.iconSize = .constant(0.7)
            symbolLayer.minZoom = 0.0
            symbolLayer.source = "__\(type)"
//            symbolLayer.iconImage = .expression(
//                Exp(.switchCase) {
//                    Exp(.boolean) {
//                        Exp(.get) { "isSelected" }
//                        false
//                    }
//                    "\(type)-selected"
//                    "\(type)"
//                }
//            )
            if type == "petrol" {
                if isSelectedAmenity {
                    symbolLayer.iconImage = .constant(ResolvedImage.name("petrol-iconV2-selected"))
                } else {
                    symbolLayer.iconImage = .constant(ResolvedImage.name("petrol-iconV2"))
                }
            } else {
                if isSelectedAmenity {
                    symbolLayer.iconImage = .constant(ResolvedImage.name("ev-charger-iconV2-selected"))
                } else {
                    symbolLayer.iconImage = .constant(ResolvedImage.name("ev-charger-iconV2"))
                }
            }
            symbolLayer.iconAllowOverlap = .constant(true)
            symbolLayer.iconIgnorePlacement = .constant(true)
            symbolLayer.iconAnchor = .constant(.center)
            symbolLayer.iconOffset = .constant([0, 0])
            
            try self.mapboxMap.style.addSource(geoJSONSource, id: "__\(type)")
            try self.mapboxMap.style.addPersistentLayer(with: symbolLayer.jsonObject(), layerPosition: isBelowPuck ? .below("puck") : .above(Constants.Layer.illegalparkingcamera))
            SwiftyBeaver.debug("addAmenitiesToMap \(features.features.count) \(type)")
        }
        catch (let error) {
            SwiftyBeaver.error("Failed to addAmenitiesToMap: \(error.localizedDescription)")
        }

        }
    }
    
    func removeAmenitiesLayer(){
        DispatchQueue.main.async {
            do {
                try self.mapboxMap.style.removeLayer(withId: AmenitiesLayer.Identifier_SymbolLayer)
                try self.mapboxMap.style.removeSource(withId: AmenitiesLayer.GeoJsonSource)
            } catch {
                SwiftyBeaver.error("Failed to remove removeAmenitiesLayer.")
            }
        }
    }
    
    func removeBookmark(type: String) {
        if Thread.isMainThread {
            do {
                try self.mapboxMap.style.removeLayer(withId: "_\(type)")
                try self.mapboxMap.style.removeSource(withId: "__\(type)")
            } catch {
                SwiftyBeaver.error("Failed to remove removeAmenitiesLayer.")
            }
        } else {
            do {
                try self.mapboxMap.style.removeLayer(withId: "_\(type)")
                try self.mapboxMap.style.removeSource(withId: "__\(type)")
            } catch {
                SwiftyBeaver.error("Failed to remove removeAmenitiesLayer.")
            }
        }
    }
    
    func addAmenitiesToMap(features:Turf.FeatureCollection, isBelowPuck: Bool) {

        do {
            // Petrol
            let petrolId = Amenities.CodingKeys.petrol.rawValue as String
            try self.mapboxMap.style.addImage(UIImage(named: "\(petrolId)_layer")!, id: petrolId, stretchX: [],stretchY: [])

            let evchargerId = Amenities.CodingKeys.evcharger.rawValue as String
            try self.mapboxMap.style.addImage(UIImage(named: "\(evchargerId)_layer")!, id: evchargerId,stretchX: [],stretchY: [])
            
            let hawkerId = Amenities.CodingKeys.hawkercentre.rawValue as String
            try self.mapboxMap.style.addImage(UIImage(named: "\(hawkerId)_layer")!, id: hawkerId,stretchX: [],stretchY: [])
            
            let libraryId = Amenities.CodingKeys.library.rawValue as String
            try self.mapboxMap.style.addImage(UIImage(named: "\(libraryId)_layer")!, id: libraryId,stretchX: [],stretchY: [])
            
            let parksId = Amenities.CodingKeys.parks.rawValue as String
            try self.mapboxMap.style.addImage(UIImage(named: "\(parksId)_layer")!, id: parksId,stretchX: [],stretchY: [])

            let museumId = Amenities.CodingKeys.museum.rawValue as String
            try self.mapboxMap.style.addImage(UIImage(named: "\(museumId)_layer")!, id: museumId,stretchX: [],stretchY: [])

            let heritagetreeId = Amenities.CodingKeys.heritagetree.rawValue as String
            try self.mapboxMap.style.addImage(UIImage(named: "\(heritagetreeId)_layer")!, id: heritagetreeId,stretchX: [],stretchY: [])

            // Create a GeoJSON data source.
            var geoJSONSource = GeoJSONSource()
            geoJSONSource.data = .featureCollection(features)
            
            let iconSize = Exp(.interpolate){
                Exp(.linear)
                Exp(.zoom)
                0
                0
                9
                0.0
                10.9
                0.0
                11
                0.85
                22
                1.0
            }
            
            //Expression for different types of images based on incident Type
            let iconExpression = Exp(.switchCase) { // Switching on a value
                Exp(.eq) { // Evaluates if conditions are equal
                    Exp(.get) { "type" } // Get the current value for incident `Type`
                    Amenities.CodingKeys.petrol.rawValue // returns true for the equal expression if the type is equal to "petrol"
                }
                petrolId // Use the icon named "petrol" on the map style if the above condition is true
                Exp(.eq) {
                    Exp(.get) { "type" }
                    Amenities.CodingKeys.evcharger.rawValue
                }
                evchargerId
                Exp(.eq) {
                    Exp(.get) { "type" }
                    Amenities.CodingKeys.hawkercentre.rawValue
                }
                hawkerId
                Exp(.eq) {
                    Exp(.get) { "type" }
                    Amenities.CodingKeys.library.rawValue
                }
                libraryId
                Exp(.eq) {
                    Exp(.get) { "type" }
                    Amenities.CodingKeys.parks.rawValue
                }
                parksId
                Exp(.eq) {
                    Exp(.get) { "type" }
                    Amenities.CodingKeys.museum.rawValue
                }
                museumId
                Exp(.eq) {
                    Exp(.get) { "type" }
                    Amenities.CodingKeys.heritagetree.rawValue
                }
                heritagetreeId
                "" // default case is to return an empty string so no icon will be loaded
            }
            
            var symbolLayer = SymbolLayer(id: AmenitiesLayer.Identifier_SymbolLayer)
            symbolLayer.source = AmenitiesLayer.GeoJsonSource
            symbolLayer.minZoom = 13.0
            symbolLayer.iconSize = .expression(iconSize)
            symbolLayer.iconImage = .expression(iconExpression)
            symbolLayer.iconAllowOverlap = .constant(true)
            symbolLayer.iconIgnorePlacement = .constant(true)
            // Add the source and style layers to the map style.
            try self.mapboxMap.style.addSource(geoJSONSource, id: AmenitiesLayer.GeoJsonSource)

            try self.mapboxMap.style.addPersistentLayer(with: symbolLayer.jsonObject(), layerPosition: .above(Constants.Layer.illegalparkingcamera))
            SwiftyBeaver.debug("addAmenitiesToMap \(features.features.count)")
        }
        catch (let error) {
            SwiftyBeaver.error("Failed to addAmenitiesToMap: \(error.localizedDescription)")
        }
    }
}

extension MapView {    
    
    func addShareCollectionBookmark(features:Turf.FeatureCollection, type:String, isAbovePuck: Bool) {
        DispatchQueue.main.async {
            do {
                
                var geoJSONSource = GeoJSONSource()
                geoJSONSource.data = .featureCollection(features)
                
                var symbolLayer = SymbolLayer(id: "_\(type)")
                let iconSize = Exp(.interpolate){
                    Exp(.linear)
                    Exp(.zoom)
                    0
                    1
                    9
                    1
                    10.9
                    1
                    11
                    1
                    13
                    1
                }
                symbolLayer.minZoom = 0.0
                symbolLayer.iconSize = .expression(iconSize)
                symbolLayer.source = "__\(type)"
                
                if type == Values.SHARE_DESTINATION_ICON {
                    symbolLayer.iconImage = .expression(
                        Exp(.switchCase) {
                            Exp(.boolean) {
                                Exp(.get) { "isSelected" }
                                false
                            }
                            "\(type)"
                            "\(type)"
                        }
                    )
                } else {
                    symbolLayer.iconImage = .expression(
                        Exp(.switchCase) {
                            Exp(.boolean) {
                                Exp(.get) { "isSelected" }
                                false
                            }
                            "\(type)-selected"
                            "\(type)"
                        }
                    )
                }
                
                
//                symbolLayer.iconImage = .constant(ResolvedImage.name(imageName))
                
                symbolLayer.iconAllowOverlap = .constant(true)
                symbolLayer.iconIgnorePlacement = .constant(true)
                symbolLayer.iconAnchor = .constant(.center)
                symbolLayer.iconOffset = .constant([0, 0])
                
                try self.mapboxMap.style.addSource(geoJSONSource, id: "__\(type)")
                try self.mapboxMap.style.addPersistentLayer(with: symbolLayer.jsonObject(), layerPosition: isAbovePuck ? .above("puck") : .above(Constants.Layer.illegalparkingcamera))
                SwiftyBeaver.debug("addShareCollectionBookmark \(features.features.count) \(type)")
            } catch (let error) {
                SwiftyBeaver.error("Failed to addShareCollectionBookmark: \(error.localizedDescription)")
            }
        }
    }
}

extension MapView {
    func removeDropPinLayer(){
        DispatchQueue.main.async {
            do {
                try self.mapboxMap.style.removeLayer(withId: "_\(Values.DROPPIN)")
                try self.mapboxMap.style.removeSource(withId: "__\(Values.DROPPIN)")
            } catch {
                SwiftyBeaver.error("Failed to remove drop pin.")
            }
        }
    }
    
    func addDropPinLocation(features:Turf.FeatureCollection, type:String, isAbovePuck: Bool, isSearch: Bool = false) {
        DispatchQueue.main.async {
            do {
                if isSearch {
                    try self.mapboxMap.style.addImage(UIImage(named: "destinationMark")!, id: "destinationMark",stretchX: [],stretchY: [])
                } else {
                    try self.mapboxMap.style.addImage(UIImage(named: "map_pin_icon")!, id: "map_pin_icon",stretchX: [],stretchY: [])
                }
                
                var geoJSONSource = GeoJSONSource()
                geoJSONSource.data = .featureCollection(features)
                
                var symbolLayer = SymbolLayer(id: "_\(type)")
                let iconSize = Exp(.interpolate){
                    Exp(.linear)
                    Exp(.zoom)
                    0
                    0.9
                    9
                    0.9
                    10.9
                    0.9
                    11
                    0.9
                    13
                    0.9
                }
                symbolLayer.minZoom = 0.0
                symbolLayer.iconSize = .expression(iconSize)
                symbolLayer.source = "__\(type)"
                
                if isSearch {
                    symbolLayer.iconImage = .constant(ResolvedImage.name("destinationMark"))
                } else {
                    symbolLayer.iconImage = .constant(ResolvedImage.name("map_pin_icon"))
                }
                
                symbolLayer.iconAllowOverlap = .constant(true)
                symbolLayer.iconIgnorePlacement = .constant(true)
                symbolLayer.iconAnchor = .constant(.center)
                symbolLayer.iconOffset = .constant([0, 0])
                
                try self.mapboxMap.style.addSource(geoJSONSource, id: "__\(type)")
                try self.mapboxMap.style.addPersistentLayer(with: symbolLayer.jsonObject(), layerPosition: isAbovePuck ? .above("puck") : .above(Constants.Layer.illegalparkingcamera))
                SwiftyBeaver.debug("addDropPinLocation \(features.features.count) \(type)")
            } catch (let error) {
                SwiftyBeaver.error("Failed to addDropPinLocation: \(error.localizedDescription)")
            }
        }
    }
}


extension MapView {
    
    func addAmenityOnCarplay(features:Turf.FeatureCollection, type:String, isAbovePuck: Bool) {
        DispatchQueue.main.async {
            do {
                
                var geoJSONSource = GeoJSONSource()
                geoJSONSource.data = .featureCollection(features)
                
                var symbolLayer = SymbolLayer(id: "_\(type)")
                let iconSize = Exp(.interpolate){
                    Exp(.linear)
                    Exp(.zoom)
                    0
                    1
                    9
                    1
                    10.9
                    1
                    11
                    1
                    13
                    1
                }
                symbolLayer.minZoom = 0.0
                symbolLayer.iconSize = .expression(iconSize)
                symbolLayer.source = "__\(type)"
                
                symbolLayer.iconImage = .expression(
                    Exp(.switchCase) {
                        Exp(.boolean) {
                            Exp(.get) { "isSelected" }
                            false
                        }
                        "\(type)-selected"
                        "\(type)"
                    }
                )
                
                symbolLayer.iconAllowOverlap = .constant(true)
                symbolLayer.iconIgnorePlacement = .constant(true)
                symbolLayer.iconAnchor = .constant(.center)
                symbolLayer.iconOffset = .constant([0, 0])
                
                try self.mapboxMap.style.addSource(geoJSONSource, id: "__\(type)")
                try self.mapboxMap.style.addPersistentLayer(with: symbolLayer.jsonObject(), layerPosition: isAbovePuck ? .above("puck") : .above(Constants.Layer.illegalparkingcamera))
                SwiftyBeaver.debug("addShareCollectionBookmark \(features.features.count) \(type)")
            } catch (let error) {
                SwiftyBeaver.error("Failed to addShareCollectionBookmark: \(error.localizedDescription)")
            }
        }
    }
}

//
//  UIImage+Text.swift
//  Breeze
//
//  Created by Zhou Hao on 14/7/21.
//

import UIKit

extension UIImage {
//    func textToImage(drawText: NSString, atPoint: CGPoint) -> UIImage {
    static func textToImage(drawText: NSString, inImage: UIImage, withFont: UIFont) -> UIImage {

        // Setup the font specific variables
        let textColor = UIColor.white
        let textFont = withFont

        // Setup the image context using the passed image
        let scale = UIScreen.main.scale
        UIGraphicsBeginImageContextWithOptions(inImage.size, false, scale)

        // Setup the font attributes that will be later used to dictate how the text should be drawn
        let textFontAttributes = [
            NSAttributedString.Key.font: textFont,
            NSAttributedString.Key.foregroundColor: textColor,
        ]

        // Put the image into a rectangle as large as the original image
        inImage.draw(in: CGRect(x: 0, y: 0, width: inImage.size.width, height: inImage.size.height))

//        let constraintRect = CGSize(width: inImage.size.width, height: CGFloat.greatestFiniteMagnitude)
//        let boundingBox = drawText.boundingRect(with: constraintRect,
//                                        options: NSStringDrawingOptions.usesLineFragmentOrigin,
//                                        attributes: [NSAttributedString.Key.font: self],
//                                        context: nil)

        // Create a point within the space that is as bit as the image
        let rect = CGRect(x: 0, y: 0, width: inImage.size.width, height: inImage.size.height)

        // Draw the text into an image
        drawText.draw(in: rect, withAttributes: textFontAttributes)

        // Create a new image out of the images we have created
        let newImage = UIGraphicsGetImageFromCurrentImageContext()

        // End the context now that we have the image we need
        UIGraphicsEndImageContext()

        //Pass the image back up to the caller
        return newImage!
    }
    
    static func generateImageWithText(text: String, font: UIFont) -> UIImage? {
//        let image = UIImage(named: "circle")!
//
//        let imageView = UIImageView(image: image)
//        imageView.backgroundColor = UIColor.clear
//        imageView.frame = CGRect(x: 0, y: 0, width: image.size.width, height: image.size.height)
        
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 46, height: 46))
        view.layer.cornerRadius = 23
        view.layer.masksToBounds = true
        view.backgroundColor = UIColor.rgba(r: 86, g: 175, b: 117, a: 1.0)
        
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: view.bounds.width, height: view.bounds.height))
        label.backgroundColor = UIColor.clear
        label.textAlignment = .center
        label.textColor = UIColor.white
        label.font = font
        label.text = text
        
        view.addSubview(label)

        UIGraphicsBeginImageContextWithOptions(label.bounds.size, false, 0)
//        imageView.layer.render(in: UIGraphicsGetCurrentContext()!)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        label.layer.render(in: UIGraphicsGetCurrentContext()!)
        let imageWithText = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return imageWithText
    }
    
    static func generateImageWithTextForCP(text: String, font: UIFont) -> UIImage? {
//        let image = UIImage(named: "circle")!
//
//        let imageView = UIImageView(image: image)
//        imageView.backgroundColor = UIColor.clear
//        imageView.frame = CGRect(x: 0, y: 0, width: image.size.width, height: image.size.height)
        
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 46, height: 46))
        view.layer.cornerRadius = 23
        view.layer.masksToBounds = true
        view.backgroundColor = UIColor.rgba(r: 93, g: 167, b: 255, a: 1.0)
        
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: view.bounds.width, height: view.bounds.height))
        label.backgroundColor = UIColor.clear
        label.textAlignment = .center
        label.textColor = UIColor.white
        label.font = font
        label.text = text
        
        view.addSubview(label)

        UIGraphicsBeginImageContextWithOptions(label.bounds.size, false, 0)
//        imageView.layer.render(in: UIGraphicsGetCurrentContext()!)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        label.layer.render(in: UIGraphicsGetCurrentContext()!)
        let imageWithText = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return imageWithText
    }
}

extension UIFont {
    func calculateHeight(text: String, width: CGFloat) -> CGFloat {
        let constraintRect = CGSize(width: width, height: CGFloat.greatestFiniteMagnitude)
        let boundingBox = text.boundingRect(with: constraintRect,
                                        options: NSStringDrawingOptions.usesLineFragmentOrigin,
                                        attributes: [NSAttributedString.Key.font: self],
                                        context: nil)
        return boundingBox.height
    }
}

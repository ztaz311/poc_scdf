//
//  UIImage+Darkmode.swift
//  Breeze
//
//  Created by Zhou Hao on 19/1/22.
//

import UIKit

extension UIImage {
    // use current image as light mode. Register a dark mode image
    func registerDarkImage(_ darkImage: UIImage) {
        let darkTC = UITraitCollection(traitsFrom: [.current, .init(userInterfaceStyle: .dark)])                    
        imageAsset?.register(darkImage, with: darkTC)
    }
    
    static func dynamicImage(withLight light: @autoclosure () -> UIImage,
                             dark: @autoclosure () -> UIImage) -> UIImage {                    
        let lightTC = UITraitCollection(traitsFrom: [.current, .init(userInterfaceStyle: .light)])
        let darkTC = UITraitCollection(traitsFrom: [.current, .init(userInterfaceStyle: .dark)])
        
        var lightImage = UIImage()
        var darkImage = UIImage()
        
        lightTC.performAsCurrent {
            lightImage = light()
        }
        darkTC.performAsCurrent {
            darkImage = dark()
        }
        
        lightImage.imageAsset?.register(darkImage, with: darkTC)
        return lightImage
    }
}

//
//  String+Extension.swift
//  Breeze
//
//  Created by Zhou Hao on 5/1/21.
//

import Foundation

extension String {
    
    var localized: String {
        NSLocalizedString(self, comment: "")
    }
    
    public func substring(_ range: CountableRange<Int>) -> String {
        let start = index(startIndex, offsetBy: max(0, range.lowerBound))
        let end = index(start, offsetBy: min(self.count - range.lowerBound,
                                             range.upperBound - range.lowerBound))
        return String(self[start..<end])
    }
    
    public subscript(_ range: CountableRange<Int>) -> String {
        return self.substring(range)
    }
    
    public subscript(_ range: CountablePartialRangeFrom<Int>) -> String {
        let start = index(startIndex, offsetBy: max(0, range.lowerBound))
        return String(self[start...])
    }
    
    // truncate the string if it's too long
    public func truncate(limit: Int) -> String {
        guard limit > 3 else {
            return self
        }
            
        let truncated = self.count > limit //truncated
        let newText = self[0..<(limit - 3)]
        return truncated ? newText + "..." : newText        
    }
    
    var localed: String {
        return NSLocalizedString(self, comment: self)
    }
    
    func nsRange(of subString: String) -> NSRange {
        return NSString(string: self).range(of: subString, options: String.CompareOptions.caseInsensitive)
    }
    
    func getLatinString() -> String {
        let regex = try! NSRegularExpression(pattern: "[\\u0b80-\\u0bFF|\\u4e00-\\u9fff]", options: [])
        let replaced = regex.stringByReplacingMatches(in: self, range: NSRange(0..<self.utf16.count), withTemplate: "")
        
        return replaced.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    
    func maskedObuName() -> String {
        var retValue = ""
        let length = self.count
        if length > 4 {
            for _ in 0..<length - 4 {
                retValue.append("*")
            }
            retValue.append(contentsOf: self.suffix(4))
        }
        print("masked: \(retValue)")
        return retValue
    }
}

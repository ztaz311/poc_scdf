//
//  UILabel+Extension.swift
//  Breeze
//
//  Created by Zhou Hao on 5/1/21.
//

import UIKit

extension UILabel {
    
    public func setMargins(_ margin: CGFloat = 10) {
        if let textString = self.text {
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.firstLineHeadIndent = margin
            paragraphStyle.headIndent = margin
            paragraphStyle.tailIndent = -margin
            paragraphStyle.alignment = .center
            let attributedString = NSMutableAttributedString(string: textString)
            attributedString.addAttribute(.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0, length: attributedString.length))
            attributedText = attributedString
        }
    }
    
    func setText(textString: String?, with margin: CGFloat) {
        self.text = textString
        self.setMargins(margin)
    }
    
    func setText(textString: String, with limit: Int) {
        self.text = suggestedName(text: textString, with: limit)
    }
    
    func suggestedName(text: String, with limit: Int) -> String {
        guard limit > 3 else {
            return text
        }
        
        let truncated = text.count > limit //truncated
        let newText = text[0..<(limit - 3)]
        return truncated ? newText + "..." : text
    }

}

/*
See LICENSE folder for this sample’s licensing information.

Abstract:
Contains static functions for file handling.
*/

import Foundation

class FileHelper {

    static let breezeLogName: String = "breeze.log"

    // Returns the URL for a given file name in the app's
    // temporary directory.
    static func urlFor(fileNameInTempDirectory: String) -> URL? {
        let tempDirURL = URL(fileURLWithPath: NSTemporaryDirectory())
        return tempDirURL.appendingPathComponent(fileNameInTempDirectory)
    }

    // Returns the size of a file, in bytes, at the specified URL.
    static func fileSize(atURL url: URL) -> UInt64? {
        let attributesOfItem = try? FileManager.default.attributesOfItem(atPath: url.path)
        let sourceLength = (attributesOfItem as NSDictionary?)?.fileSize()

        return sourceLength
    }
    
    static func getLogURL() -> URL? {
        let fm = FileManager.default
        if var logurl = try? fm.url(for:.documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false) {
            
            logurl.appendPathComponent("breeze.log")
            return logurl
        }
        return nil
    }
}

extension FileManager {
    func clearTmpDirectory() {
        do {
            let tmpDirectory = try contentsOfDirectory(atPath: NSTemporaryDirectory())
            try tmpDirectory.forEach { [unowned self] file in
                let path = String.init(format: "%@%@", NSTemporaryDirectory(), file)
                try self.removeItem(atPath: path)
            }
        } catch {
            print(error)
        }
    }
}

extension FileHandle {
    // Returns a writable file handle for a given file name in the app's
    // temporary directory.
    static func makeFileHandle(forWritingToFileNameInTempDirectory: String) -> FileHandle? {
        guard
            let url = FileHelper.urlFor(fileNameInTempDirectory: forWritingToFileNameInTempDirectory) else {
                return nil
        }
        
        FileManager.default.createFile(atPath: url.path,
                                       contents: nil,
                                       attributes: nil)
        
        return try? self.init(forWritingTo: url)
    }
}

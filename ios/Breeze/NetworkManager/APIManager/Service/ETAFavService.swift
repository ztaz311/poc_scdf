//
//  ETAFavService.swift
//  Breeze
//
//  Created by VishnuKanth on 12/08/21.
//

import Foundation

struct ETAFavList: ETAFavServiceProtocol {
    
    static let shared = ETAFavList()
    
    let postSession = URLSession(configuration: .default)
    
    func getETAFavList(_ completion: @escaping (Result<[Favourites]>) -> ()) {
        
        var allParameters : HTTPParameters = [String:Any]()
        allParameters = parameters
        
        let resource = Resource<BaseETAFavModel>(url: Configuration.etaFavList, parameters: allParameters, body: nil, method: .get)
        postSession.load(resource) { (result) in
            switch result {
            case .success(let theResult):
                completion(Result.success(theResult.favourites ?? []))
            case .failure(let error):
                completion(Result.failure(error))
            }
        }
    }
}

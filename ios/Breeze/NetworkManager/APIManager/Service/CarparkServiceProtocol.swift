//
//  CarparkServiceProtocol.swift
//  Breeze
//
//  Created by Zhou Hao on 10/5/21.
//

import Foundation

protocol CarparkServiceProtocol {    
    func getCarparks(lat: Double, long: Double, radius: Int, maxRadius: Int, count: Int, arrivalTime: Int, destName: String, shortTermOnly: Bool, isVoucher: Bool, parkingAvaibility: String?, carparkId: String?, _ completion: @escaping (Result<CarParkBase>) -> ())
}

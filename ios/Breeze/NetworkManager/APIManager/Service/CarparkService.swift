//
//  CarparkService.swift
//  Breeze
//
//  Created by Zhou Hao on 10/5/21.
//

import Foundation

struct CarparkService: CarparkServiceProtocol {
    func getCarparks(lat: Double, long:Double, radius: Int, maxRadius: Int, count: Int, arrivalTime: Int, destName: String, shortTermOnly: Bool, isVoucher: Bool, parkingAvaibility: String? = nil, carparkId: String? = nil, _ completion: @escaping (Result<CarParkBase>) -> ()) {
        
        let postSession = URLSession(configuration: .default)
//
//        var allParameters : HTTPParameters = [String:Any]()
//        allParameters?["latitude"] = lat
//        allParameters?["longitude"] = long
//        allParameters?["radius"] = Double(radius) / 1000.0
//        if maxRadius > radius {
//            allParameters?["maxRadius"] = Double(maxRadius) / 1000.0
//        }
//        allParameters?["arrivalTime"] = arrivalTime
//        allParameters?["destName"] = destName
//        allParameters?["resultcount"] = count
        let radiusConvert = Double(radius) / 1000
        
        var maxRadiusConvert:Double?
        if(maxRadius == 0){
            
            maxRadiusConvert = nil
        }
        else{
            
            if maxRadius > radius {
                maxRadiusConvert =  Double(maxRadius) / 1000
            }
        }
        
        let caterogy = shortTermOnly ? "SHORT_TERM" : nil
        
        let filterRequest = AmenityRequest(type: Values.AMENITYCARPARK,
                                           filter: AmenityFilter(provider: nil,
                                                                 services: nil,
                                                                 plugTypes: nil,
                                                                 category: caterogy,
                                                                 availableBand: parkingAvaibility))
        
        let carParkAmenitiesRequest = AmenitiesRequest(latitude: lat, longitude: long, radius: radiusConvert, arrivalTime: Double(arrivalTime), destinationName: destName, amenities: [filterRequest], resultCount: count, maxRadius: maxRadiusConvert, isVoucher: isVoucher, carparkId: carparkId)
        
        do {
            let jsonData = try JSONEncoder().encode(carParkAmenitiesRequest)
            
            let resource = Resource<CarParkBase>(url: Configuration.amenitiesV2, parameters: parameters, body: jsonData, method: .post)
            postSession.load(resource) { (result) in
                switch result {
                case .success(let theResult):
                    completion(Result.success(theResult))
                case .failure(let error):
                    print(error)
                    completion(Result.failure(error))
                }
            }
        }
        catch(let error){
            print(error)
        }
    }
    
    func getCarparkAvailability(_ carparkId: String, attributes: [String], _ completion: @escaping (Result<AttributesCarparkModel>) -> ()) {
        let session = URLSession(configuration: .default)
        let encodedString = "\(attributes)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
        let resource = Resource<AttributesCarparkModel>(url: Configuration.getCarparkDetail.appending("/\(carparkId)?attributes=\(encodedString)"), parameters: [:], body: nil, method: .get)
        session.load(resource, completion: completion)
    }
    
    func checkParkingAvailability(_ destinationName: String, lat: Double, long: Double, _ completion: @escaping (Result<AttributesCarparkModel>) -> ()) {
        let postSession = URLSession(configuration: .default)
        do {
            let params: [String: Any] = ["lat": lat, "long": long, "destinationName": destinationName]
            let data =  try JSONSerialization.data(withJSONObject: params)
            let resource = Resource<AttributesCarparkModel>(url: Configuration.checkParkingAvailabilityAlert, parameters: [:], body: data, method: .post)
            postSession.load(resource, completion: completion)
        } catch {
            print(error)
        }
    }
}

final class CarparkDetailService {
    
    func getCarparkDetail(_ carparkId: String, _ completion: @escaping (Result<SearchCarpark>) -> ()) {
        let session = URLSession(configuration: .default)
        let resource = Resource<SearchCarpark>(url: Configuration.getCarparkDetail.appending("/\(carparkId)"), parameters: [:], body: nil, method: .get)
        session.load(resource, completion: completion)
    }
}

//
//  ContentService.swift
//  Breeze
//
//  Created by Zhou Hao on 28/7/22.
//

import Foundation

final class ContentService {
        
    func getContentDetails(	id: Int, _ completion: @escaping (Result<ContentBase>) -> ()) {
        let session = URLSession(configuration: .default)
                
        let resource = Resource<ContentBase>(url: Configuration.contentDetails.appending("/\(id)"), parameters: [:], body: nil, method: .get)
        session.load(resource, completion: completion)
    }
    
    func getContents(_ completion: @escaping (Result<Contents>) -> ()) {
        let session = URLSession(configuration: .default)
                
        let resource = Resource<Contents>(url: Configuration.contents, parameters: [:], body: nil, method: .get)
        session.load(resource, completion: completion)
    }
    
    func getContentsV2(_ completion: @escaping (Result<ContentV2>) -> ()) {
        let session = URLSession(configuration: .default)
                
        let resource = Resource<ContentV2>(url: Configuration.contentsV2, parameters: [:], body: nil, method: .get)
        session.load(resource, completion: completion)
    }
}

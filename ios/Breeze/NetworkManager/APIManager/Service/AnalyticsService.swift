//
//  AnalyticsService.swift
//  Breeze
//
//  Created by Zhou Hao on 12/9/22.
//

import Foundation
import RxSwift

final class AnalyticsService {
    
    struct EventData: Codable {
        let event_name: String
        let event_value: String
        let screen_name: String
        let event_timestamp: Int64  // system time in milliseconds
        let amenity_id: String
        let layer_code: String
        let amenity_type: String
    }
    
    struct POIEventData: Codable {
        let event_name: String
        let event_value: String
        let screen_name: String
        let event_timestamp: Int64
        let amenity_id: String
        let layer_code: String
    }
    
    
    func post(analyticsEvent: EventData, completion: @escaping (Result<Bool>) -> ()) {
        
        do{
            let jsonData = try JSONEncoder().encode(analyticsEvent)
            
//            print(String(data: jsonData, encoding: .utf8))
            
            let postSession = URLSession(configuration: .default)
            let resource = Resource<EmptyData>(url: Configuration.analytics, parameters: parameters, body: jsonData, method: .post)
            postSession.load(resource) { (result) in
                switch result {
                case .success(_):
                    completion(Result.success(true))
                case .failure(let error):
                    completion(Result.failure(error))
                }
            }
        } catch(_) {
            completion(Result.failure(HTTPNetworkError.encodingFailed))
        }
    }
    
    func postPOI(analyticsEvent: POIEventData, completion: @escaping (Result<Bool>) -> ()) {
        
        do{
            let jsonData = try JSONEncoder().encode(analyticsEvent)
                        
            let postSession = URLSession(configuration: .default)
            let resource = Resource<EmptyData>(url: Configuration.analytics, parameters: parameters, body: jsonData, method: .post)
            postSession.load(resource) { (result) in
                switch result {
                case .success(_):
                    completion(Result.success(true))
                case .failure(let error):
                    completion(Result.failure(error))
                }
            }
        } catch(_) {
            completion(Result.failure(HTTPNetworkError.encodingFailed))
        }
    }
}

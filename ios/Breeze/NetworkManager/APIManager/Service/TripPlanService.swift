//
//  TripPlanService.swift
//  Breeze
//
//  Created by VishnuKanth on 30/12/21.
//

import Foundation

final class TripPlanService: TripPlanServiceProtocol {
    
    let postSession = URLSession(configuration: .default)
    
    func uploadTripPlan(request: TripPlanUploadRequest, completion: @escaping (Result<Bool>) -> ()) {
        
        do{
            let jsonData = try JSONEncoder().encode(request)
            let resource = Resource<EmptyData>(url: Configuration.uploadTripPlan, parameters: parameters, body: jsonData, method: .post)
            postSession.load(resource) { (result) in
                switch result {
                case .success(_):
                    completion(Result.success(true))
                case .failure(let error):
                    completion(Result.failure(error))
                }
            }
        
        }
        catch(let error){
            print(error.localizedDescription)
        }
        
    }
    
    func getUpcomingTrips(pageNumber: Int, pageSize: Int, startDate: String, endDate: String, searchKey: String = "", _ completion: @escaping (Result<UpcomingTripBase>) -> ()) {
        
        let postSession = URLSession(configuration: .default)
        
        var allParameters : HTTPParameters = [String:Any]()
        allParameters?["page"] = pageNumber
        allParameters?["pagesize"] = pageSize
        allParameters?["startdate"] = startDate
        allParameters?["enddate"] = endDate
        if !searchKey.isEmpty {
            allParameters?["searchkey"] = searchKey
        }
        
        let resource = Resource<UpcomingTripBase>(url: Configuration.upcomingTripApi, parameters: allParameters, body: nil, method: .get)
        postSession.load(resource) { (result) in
            switch result {
            case .success(let theResult):
                completion(Result.success(theResult))
            case .failure(let error):
                completion(Result.failure(error))
            }
        }
    }
    
    func deleteUpcomingTrip(tripPlannerId: Int, _ completion: @escaping (Result<Bool>)-> ()) {
        let postSession = URLSession(configuration: .default)
        let url = "\(Configuration.upcomingTripApi)/\(tripPlannerId)"
        let resource = Resource<EmptyData>(url: url, parameters: parameters, body: nil, method: .delete)

        postSession.load(resource) { (result) in
            switch result {
            case .success(_):
                completion(Result.success(true))
            case .failure(let error):
                completion(Result.failure(error))
            }
        }
    }
    
    func deleteTripHistory(tripPlannerId: Int, _ completion: @escaping (Result<Bool>)-> ()) {
        let postSession = URLSession(configuration: .default)
        let url = "\(Configuration.tripApi)/\(tripPlannerId)"
        let resource = Resource<EmptyData>(url: url, parameters: parameters, body: nil, method: .delete)

        postSession.load(resource) { (result) in
            switch result {
            case .success(_):
                completion(Result.success(true))
            case .failure(let error):
                completion(Result.failure(error))
            }
        }
    }
    
    func updateTripPlan(request: TripPlanUploadRequest,tripPlannerId: Int, completion: @escaping (Result<Bool>)-> ()) {
        
        do{
            let jsonData = try JSONEncoder().encode(request)
            let postSession = URLSession(configuration: .default)
            let url = "\(Configuration.upcomingTripApi)/\(tripPlannerId)"
            let resource = Resource<EmptyData>(url: url, parameters: parameters, body: jsonData, method: .put)

            postSession.load(resource) { (result) in
                switch result {
                case .success(_):
                    completion(Result.success(true))
                case .failure(let error):
                    completion(Result.failure(error))
                }
            }

        }
        catch(let error){
            print(error.localizedDescription)
        }
        
    }
    
    func getTripPlan(tripId: Int, _ completion: @escaping (Result<PlannTripDetails>) -> ()) {
        let postSession = URLSession(configuration: .default)
        let url = "\(Configuration.upcomingTripApi)/\(tripId)"
        let resource = Resource<PlannTripDetails>(url: url, parameters: parameters, body: nil, method: .get)

        postSession.load(resource, completion: completion)
    }
    
}

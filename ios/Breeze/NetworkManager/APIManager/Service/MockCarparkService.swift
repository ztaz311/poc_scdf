//
//  MockCarparkService.swift
//  Breeze
//
//  Created by Zhou Hao on 10/5/21.
//

import Foundation

struct MockCarparkService: CarparkServiceProtocol {

    func getCarparks( lat:Double, long:Double, radius: Int, maxRadius: Int, count: Int, arrivalTime: Int,destName: String, shortTermOnly: Bool, isVoucher: Bool, parkingAvaibility: String?, carparkId: String? = nil, _ completion: @escaping (Result<CarParkBase>) -> ()) {

        let url = Bundle.main.url(forResource: "carparks", withExtension: "json")!
        do {
            let jsonData = try Data(contentsOf: url)
            completion(Result.success(try JSONDecoder().decode(CarParkBase.self, from: jsonData)))
        } catch (let error) {
            completion(Result.failure(error))
        }
    }
}

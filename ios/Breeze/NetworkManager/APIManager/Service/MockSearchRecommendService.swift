//
//  MockSearchRecommendService.swift
//  Breeze
//
//  Created by Zhou Hao on 20/5/21.
//

import Foundation

struct MockSearchRecommendService: SearchRecommendServiceProtocol {
    
    func getSearchRecommendation(lat:String?, long:String? ,resultCount:Int ,_ completion: @escaping (Result<[Addresses]>) -> ()) {
        
        let theLat = lat ?? ""
        let theLong = long ?? ""
        var addresses = [Addresses]()
        
        let address1 = Addresses(addressId: nil, alias: "", address1: "Test 1", lat: theLat, long: theLong, address2: "Test address 1")
        let address2 = Addresses(addressId: nil, alias: "", address1: "Test 2", lat: theLat, long: theLong, address2: "Test address 2")
        let address3 = Addresses(addressId: nil, alias: "", address1: "Test 3", lat: theLat, long: theLong, address2: "Test address 3")

        addresses.append(address1)
        addresses.append(address2)
        addresses.append(address3)
        
//        let randomInt = Int.random(in: 0...3)
//        for i in 0..<randomInt {
//            let address = Addresses(addressId: nil, alias: "", address1: "Test \(i+1)", lat: lat ?? "", long: long ?? "", address2: "Test address \(i+1)")
//            addresses.append(address)
//        }
        
        DispatchQueue.global().asyncAfter(deadline: .now() + 2) {
            completion(Result.success(addresses))
        }
    }
}

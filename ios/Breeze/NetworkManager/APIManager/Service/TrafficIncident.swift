//
//  TrafficIncident.swift
//  Breeze
//
//  Created by VishnuKanth on 04/05/21.
//

import Foundation

struct TrafficIncidents: TrafficIncidentServiceProtocol {
    
    static let shared = TrafficIncidents()
    
    let postSession = URLSession(configuration: .default)
    
    func getTrafficIncidents(_ completion: @escaping (Result<Incidents>) -> ()) {
        
        let trafficIncidentURL = "\(Configuration.dataset)/trafficincidentv2.json"
        let resource = Resource<Incidents>(url: trafficIncidentURL, parameters:parameters, body: nil, method: .get)
        postSession.load(resource, completion: completion)
    }
}

//
//  SchoolZoneServiceProtocol.swift
//  Breeze
//
//  Created by Zhou Hao on 28/5/21.
//

import Foundation
import Turf

enum SchoolZoneServiceError: Error {
    case failedToLoad
    case failedToParseJson
}

protocol SchoolZoneServiceProtocol {
    // Get school zone features from API
    /**
     Get school zone features from API
          
     - parameter completion: The closure (block) to call with the resulting `FeatureCollection`.
     */
    func getSchoolZone(_ completion: @escaping (Result<[SchoolZoneData]>) -> ())    
}

extension SchoolZoneServiceProtocol {
    static func map(featureCollection: FeatureCollection) -> [SchoolZoneData] {        
        return featureCollection.features.enumerated().map { (index,feature) in
            let address = feature.properties?["SITENAME"] as? String ?? ""
            return SchoolZoneData(id: "\(FeatureDetection.featureTypeSchoolZone)-\(index+1)", feature: feature, address: address)
        }
    }
}

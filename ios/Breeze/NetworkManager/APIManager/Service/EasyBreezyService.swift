//
//  EasyBreezyService.swift
//  Breeze
//
//  Created by VishnuKanth on 07/01/21.
//

import Foundation

enum EasyBreezyServiceError: Error {
    case duplicated
    case failedToLoad
}

struct EasyBreezyService{
    
    static let shared = EasyBreezyService()
            
    let postSession = URLSession(configuration: .ephemeral)
    
    func getSavedEasyBreezyAddresses(_ completion: @escaping (Result<[EasyBreezyAddresses]>) -> ()) {
        var allParameters : HTTPParameters = [String:Any]()
        allParameters = parameters
        
        let resource = Resource<EasyBreezyBaseAddress>(url: Configuration.easyBreezyAddresses, parameters: allParameters, body: nil, method: .get)
        postSession.load(resource) { (result) in
            switch result {
            case .success(let theResult):
                completion(Result.success(theResult.addresses ?? []))
            case .failure(let error):
                completion(Result.failure(error))
            }
        }
    }

    func saveEasyBreezyAddress(address:[String:Any],_ completion: @escaping (Result<Bool>) -> ()) {
        deleteUpdateSaveAction(method: .post, input: address, completion)
    }
    
    func deleteEasyBreezyAddresses(address:[[String:Any]],_ completion: @escaping (Result<Bool>) -> ()) {
        deleteUpdateSaveAction(method: .delete, input: address, completion)
    }
    
    func updateEasyBreezyAddress(address:[String:Any],_ completion: @escaping (Result<Bool>) -> ()) {
        deleteUpdateSaveAction(method: .put, input: address, completion)
    }
    
    private func deleteUpdateSaveAction(method: HTTPMethod, input: Any,_ completion: @escaping (Result<Bool>) -> ()) {

        var httpBody  = [String:Any]()
        httpBody["easybreezies"] = input
        
        do {
            let data =  try JSONSerialization.data(withJSONObject: httpBody)
            print("FAV DELETE",httpBody)
            let resource = Resource<EmptyData>(url: Configuration.easyBreezyAddresses, parameters: parameters, body: data, method: method)
            
            postSession.load(resource) { (result) in
                switch result {
                case .success(_):
                    completion(Result.success(true))
                case .failure(let error):
                    completion(Result.failure(error))
                }
            }
        }
        catch(let error){
            print(error.localizedDescription)
        }
        
    }

/*
    func getSavedEasyBreezyAddresses(_ completion: @escaping (Result<[EasyBreezyAddresses]>) -> ()) {
        
        let isTokenExpiryCheck =  AWSAuth.sharedInstance.checkTokenExpiry(idToken: AWSAuth.sharedInstance.idToken)
         if(isTokenExpiryCheck == true)
         {
             AWSAuth.sharedInstance.fetchCurrentSession { (result) in
                callFetchEBAPI() { (result) in
                    
                    switch result{
                      
                    case .success(let json):
                        completion(Result.success(json))
                    case .failure(let error):
                        completion(Result.failure(error))
                    }
                }
                 
             } onFailure: { (error) in
                 DispatchQueue.main.async {
                     
                 }
             }
         }
         else
         {
            callFetchEBAPI() { (result) in
                
                switch result{
                  
                case .success(let json):
                    completion(Result.success(json))
                case .failure(let error):
                    completion(Result.failure(error))
                }
            }
         }
        
    }
    
    func callFetchEBAPI(_ completion: @escaping (Result<[EasyBreezyAddresses]>) -> ()){
        
        do{
            
            var allParameters : HTTPParameters = [String:Any]()
            allParameters = parameters
            let request = try HTTPNetworkRequest.configureHTTPRequest(url: Configuration.easyBreezyAddresses, parameters: allParameters, headers: createHeader(), body: nil, method: .get)
                postSession.dataTask(with: request) { (data, res, err) in
                
                URLCache.shared.removeAllCachedResponses()
                if let response = res as? HTTPURLResponse, let unwrappedData = data {
                    
                    let result = HTTPNetworkResponse.handleNetworkResponse(for: response)
                    switch result {
                    case .success:
                        
                        if let result = try? JSONDecoder().decode(EasyBreezyBaseAddress.self, from: unwrappedData) {
                            completion(Result.success(result.addresses!))
                        } else {
                            completion(Result.failure(HTTPNetworkError.decodingFailed))
                        }
                        
                    case .failure:
                        completion(Result.failure(HTTPNetworkError.decodingFailed))
                    }
                }
            }.resume()
        }catch{
            completion(Result.failure(HTTPNetworkError.badRequest))
        }
        
    }

    func saveEasyBreezyAddress(address:[String:Any],_ completion: @escaping (Result<Bool>) -> ()){
        
        let isTokenExpiryCheck =  AWSAuth.sharedInstance.checkTokenExpiry(idToken: AWSAuth.sharedInstance.idToken)
         if(isTokenExpiryCheck == true)
         {
             AWSAuth.sharedInstance.fetchCurrentSession { (result) in
                callSaveEBApi(address:address) { (result) in
                    
                    switch result{
                      
                    case .success(_):
                        completion(Result.success(true))
                    case .failure(let error):
                        completion(Result.failure(error))
                    }
                }
                 
             } onFailure: { (error) in
                 DispatchQueue.main.async {
                     
                 }
             }
         }
         else
         {
            callSaveEBApi(address:address) { (result) in
                
                switch result{
                  
                case .success(_):
                    completion(Result.success(true))
                case .failure(let error):
                    completion(Result.failure(error))
                }
            }
         }
        
    }
    
    func callSaveEBApi(address:[String:Any],_ completion: @escaping (Result<Bool>) -> ()){
        
        do{

            var httpBody  = [String:Any]()
            httpBody["easybreezies"] = address
          let data =  try! JSONSerialization.data(withJSONObject: httpBody)
            let request = try HTTPNetworkRequest.configureHTTPRequest(url: Configuration.easyBreezyAddresses, parameters: parameters, headers: createHeader(), body: data, method: .post)
            postSession.dataTask(with: request) { (data, res, err) in
                
                if let response = res as? HTTPURLResponse, let unwrappedData = data {
                    
                    let result = HTTPNetworkResponse.handleNetworkResponse(for: response)
                    switch result {
                    case .success:
                        
                        completion(Result.success(true))
                        
                    case .failure:
                        completion(Result.failure(HTTPNetworkError.decodingFailed))
                    }
                }
            }.resume()
        }catch{
            completion(Result.failure(HTTPNetworkError.badRequest))
        }
        
    }
    
    func deleteEasyBreezyAddresses(address:[[String:Any]],_ completion: @escaping (Result<Bool>) -> ()){
        
        let isTokenExpiryCheck =  AWSAuth.sharedInstance.checkTokenExpiry(idToken: AWSAuth.sharedInstance.idToken)
         if(isTokenExpiryCheck == true)
         {
             AWSAuth.sharedInstance.fetchCurrentSession { (result) in
                callDeleteEBApi(address:address) { (result) in
                    
                    switch result{
                      
                    case .success(_):
                        completion(Result.success(true))
                    case .failure(let error):
                        completion(Result.failure(error))
                    }
                }
                 
             } onFailure: { (error) in
                 DispatchQueue.main.async {
                     
                 }
             }
         }
         else
         {
            callDeleteEBApi(address:address) { (result) in
                
                switch result{
                  
                case .success(_):
                    completion(Result.success(true))
                case .failure(let error):
                    completion(Result.failure(error))
                }
            }
         }
        
    }
    
    func callDeleteEBApi(address:[[String:Any]],_ completion: @escaping (Result<Bool>) -> ()){
        
        do{

            var httpBody  = [String:Any]()
            httpBody["easybreezies"] = address
            print(httpBody)
          let data =  try! JSONSerialization.data(withJSONObject: httpBody)
            let request = try HTTPNetworkRequest.configureHTTPRequest(url: Configuration.easyBreezyAddresses, parameters: parameters, headers: createHeader(), body: data, method: .delete)
            postSession.dataTask(with: request) { (data, res, err) in
                
                if let response = res as? HTTPURLResponse, let unwrappedData = data {
                    
                    let result = HTTPNetworkResponse.handleNetworkResponse(for: response)
                    switch result {
                    case .success:
                        completion(Result.success(true))
                        
                    case .failure:
                        completion(Result.failure(HTTPNetworkError.decodingFailed))
                    }
                }
            }.resume()
        }catch{
            completion(Result.failure(HTTPNetworkError.badRequest))
        }
        
    }
    
    func updateEasyBreezyAddress(address:[String:Any],_ completion: @escaping (Result<Bool>) -> ()){
        
        let isTokenExpiryCheck =  AWSAuth.sharedInstance.checkTokenExpiry(idToken: AWSAuth.sharedInstance.idToken)
         if(isTokenExpiryCheck == true)
         {
             AWSAuth.sharedInstance.fetchCurrentSession { (result) in
                callUpdateEBApi(address:address) { (result) in
                    
                    switch result{
                      
                    case .success(_):
                        completion(Result.success(true))
                    case .failure(let error):
                        completion(Result.failure(error))
                    }
                }
                 
             } onFailure: { (error) in
                 DispatchQueue.main.async {
                     
                 }
             }
         }
         else
         {
            callUpdateEBApi(address:address) { (result) in
                
                switch result{
                  
                case .success(_):
                    completion(Result.success(true))
                case .failure(let error):
                    completion(Result.failure(error))
                }
            }
         }
    }
    
    func callUpdateEBApi(address:[String:Any],_ completion: @escaping (Result<Bool>) -> ()){
        
        do{

            var httpBody  = [String:Any]()
            httpBody["easybreezies"] = address
          let data =  try! JSONSerialization.data(withJSONObject: httpBody)
            let request = try HTTPNetworkRequest.configureHTTPRequest(url: Configuration.easyBreezyAddresses, parameters: parameters, headers: createHeader(), body: data, method: .put)
            postSession.dataTask(with: request) { (data, res, err) in
                
                if let response = res as? HTTPURLResponse, let unwrappedData = data {
                    
                    let result = HTTPNetworkResponse.handleNetworkResponse(for: response)
                    switch result {
                    case .success:
                        
                        completion(Result.success(true))
                        
                    case .failure:
                        completion(Result.failure(HTTPNetworkError.decodingFailed))
                    }
                }
            }.resume()
        }catch{
            completion(Result.failure(HTTPNetworkError.badRequest))
        }
        
    }
*/
}

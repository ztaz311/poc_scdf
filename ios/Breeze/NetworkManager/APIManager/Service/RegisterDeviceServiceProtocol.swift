//
//  RegisterDeviceServiceProtocol.swift
//  Breeze
//
//  Created by VishnuKanth on 18/06/21.
//

import Foundation

struct RegisterDeviceResponse: Codable {
    
    let success: Bool
    let message: String
    
    enum CodingKeys: String, CodingKey {
        case success
        case message
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.success = try container.decodeIfPresent(Bool.self, forKey: .success) ?? false
        self.message = try container.decodeIfPresent(String.self, forKey: .message) ?? ""
    }
    
}

protocol RegisterDeviceServiceProtocol {
    func registerDevice(deviceID:String,token:String,deviceos:String ,completion: @escaping (Result<RegisterDeviceResponse>)->Void)
}

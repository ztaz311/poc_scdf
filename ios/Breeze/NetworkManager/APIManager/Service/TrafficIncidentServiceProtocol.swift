//
//  TrafficIncidentServiceProtocol.swift
//  Breeze
//
//  Created by Zhou Hao on 1/6/21.
//

import Foundation

protocol TrafficIncidentServiceProtocol {
    func getTrafficIncidents(_ completion: @escaping (Result<Incidents>) -> ()) 
}

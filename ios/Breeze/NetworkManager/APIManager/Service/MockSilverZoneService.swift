//
//  MockSilverZoneService.swift
//  Breeze
//
//  Created by Zhou Hao on 28/5/21.
//

import Foundation
import Turf

struct MockSilverZoneService: SilverZoneServiceProtocol {
    
    func getSilverZone(_ completion: @escaping (Result<[SilverZoneData]>) -> ()) {
        
        let url = Bundle.main.url(forResource: "SILVERZONE", withExtension: "geojson")!
        do {
            let jsonData = try Data(contentsOf: url)
            let geojson = try JSONDecoder().decode(FeatureCollection.self, from: jsonData)
            completion(Result.success(MockSilverZoneService.map(featureCollection: geojson)))
        } catch (let error) {
            completion(Result.failure(error))
        }
    }
}

//
//  BookmarkService.swift
//  Breeze
//
//  Created by Tuyen, Le Xuan  on 03/08/2022.
//

import Foundation

struct BookmarkService {
    static let shared = BookmarkService()
    
    let postSession = URLSession(configuration: .ephemeral)
    
    func editBookmark(bookmark: Bookmark,_ completion: @escaping (Result<Bool>) -> ()) {
        actionEdit(method: .put, input: bookmark, completion)
    }
    
    func createBookmark(bookmark: Bookmark,_ completion: @escaping (Result<Bool>) -> ()) {
        action(method: .post, input: bookmark, completion)
    }
    
     func deselectingBookmarksAmenity(amenityId: String, completion: @escaping (Result<Bool>) -> ()) {
         deselectingBookmarkAmenity(amenityId: amenityId, completion)
    }
    
    func deselectingBookmarksAddress(address1: String, address2: String, completion: @escaping (Result<Bool>) -> ()) {
       deselectingBookmarkAddress(address1: address1, address2: address2, completion)
   }
    
    private func actionEdit(method: HTTPMethod, input: Bookmark,_ completion: @escaping (Result<Bool>) -> ()) {
        do {
            let data = try JSONEncoder().encode(input)
            
            let resource = Resource<EmptyData>(url: Configuration.userBookmark + "/\(input.bmId)", parameters: parameters, body: data, method: method)
            
            postSession.load(resource) { (result) in
                switch result {
                case .success(_):
                    completion(Result.success(true))
                case .failure(let error):
                    completion(Result.failure(error))
                }
            }
        } catch(let error){
            print(error.localizedDescription)
        }
    }
    
    private func action(method: HTTPMethod, input: Bookmark,_ completion: @escaping (Result<Bool>) -> ()) {
        do {
            let data = try JSONEncoder().encode(input)
            
            let resource = Resource<EmptyData>(url: Configuration.userBookmark, parameters: parameters, body: data, method: method)
            
            postSession.load(resource) { (result) in
                switch result {
                case .success(_):
                    completion(Result.success(true))
                case .failure(let error):
                    completion(Result.failure(error))
                }
            }
        } catch(let error){
            print(error.localizedDescription)
        }
    }
    
    // Deselect Tooltip Bookmark Amenity
    private func deselectingBookmarkAmenity(amenityId: String, _ completion: @escaping (Result<Bool>)-> ()) {
        let postSession = URLSession(configuration: .default)
        let url = "\(Configuration.userBookmarkDeselectingAmenity)/\(amenityId)"
        let resource = Resource<EmptyData>(url: url, parameters: parameters, body: nil, method: .delete)
        
        postSession.load(resource) { (result) in
            switch result {
            case .success(_):
                completion(Result.success(true))
            case .failure(let error):
                completion(Result.failure(error))
            }
        }
    }
    
    // Deselect Tooltip Bookmark Address
    private func deselectingBookmarkAddress(address1: String, address2: String, _ completion: @escaping (Result<Bool>)-> ()) {
        let postSession = URLSession(configuration: .default)
        
        var allParameters : HTTPParameters = [String: Any]()
        allParameters?["address1"] = address1
        if !address2.isEmpty {
            allParameters?["address2"] = address2
        }
            
        let url = "\(Configuration.userBookmarkDeselectingAddress)"
        let resource = Resource<EmptyData>(url: url, parameters: allParameters, body: nil, method: .delete)
        postSession.load(resource) { (result) in
            switch result {
            case .success(_):
                completion(Result.success(true))
            case .failure(let error):
                completion(Result.failure(error))
            }
        }
    }

}

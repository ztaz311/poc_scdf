//
//  TripETAService.swift
//  Breeze
//
//  Created by Zhou Hao on 13/7/21.
//

import Foundation

final class TripETAService: TripETAServiceProtocol {
    
    func sendTripETA(_ tripETA: TripETA, completion: @escaping (Result<TripETAResponse>)->Void) {
        
        do{
            let jsonData = try JSONEncoder().encode(tripETA)
            let postSession = URLSession(configuration: .default)
            let resource = Resource<TripETAResponse>(url: Configuration.tripETA, parameters: parameters, body: jsonData, method: .post)
            postSession.load(resource,completion: completion)
        }
        catch(let error){
            print(error.localizedDescription)
        }
    }
    
    func sendTripCruiseETA(_ tripETA: TripCruiseETA, completion: @escaping (Result<TripETAResponse>)->Void) {
        
        do{
            let jsonData = try JSONEncoder().encode(tripETA)
    //        print(String(data:jsonData, encoding: .utf8))
            let postSession = URLSession(configuration: .default)
            let resource = Resource<TripETAResponse>(url: Configuration.tripETA, parameters: parameters, body: jsonData, method: .post)
            postSession.load(resource,completion: completion)
        }
        catch(let error){
            print(error.localizedDescription)
        }
        
    }
    
    func sendReminder(_ tripETAReminder: TripETAReminder, completion: @escaping (Result<TripETAReminderResponse>)->Void) {
        
        do{
            let jsonData = try JSONEncoder().encode(tripETAReminder)
            let postSession = URLSession(configuration: .default)
            let resource = Resource<TripETAReminderResponse>(url: Configuration.tripETA, parameters: parameters, body: jsonData, method: .post)
            postSession.load(resource,completion: completion)
        }
        catch(let error){
            print(error.localizedDescription)
        }
    }
        
    
    func updateLocationSharingStatus(_ liveLocationStatus: LiveLocationStatus, completion: @escaping (Result<LiveLocationStatusResponse>)->Void) {
        
        do{
            let jsonData = try JSONEncoder().encode(liveLocationStatus)
            let postSession = URLSession(configuration: .default)
            let resource = Resource<LiveLocationStatusResponse>(url: Configuration.liveLocationStatus, parameters: parameters, body: jsonData, method: .post)
            postSession.load(resource,completion: completion)
        }
        catch(let error){
            print(error.localizedDescription)
        }
        
    }

}

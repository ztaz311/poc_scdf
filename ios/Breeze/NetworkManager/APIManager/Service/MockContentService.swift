//
//  MockContentService.swift
//  Breeze
//
//  Created by Santoso Pham on 17/10/2022.
//

import Foundation

final class MockContentService {
        
    func getContentDetails(id: Int, _ completion: @escaping (Result<ContentBase>) -> ()) {
        
        let url = Bundle.main.url(forResource: "contentdetails", withExtension: "json")!
        do {
            let jsonData = try Data(contentsOf: url)
            completion(Result.success(try JSONDecoder().decode(ContentBase.self, from: jsonData)))
        } catch (let error) {
            completion(Result.failure(error))
        }
    }
    
    func getContentsV2(_ completion: @escaping (Result<ContentV2>) -> ()) {
        
        let url = Bundle.main.url(forResource: "contentv2", withExtension: "json")!
        do {
            let jsonData = try Data(contentsOf: url)
            completion(Result.success(try JSONDecoder().decode(ContentV2.self, from: jsonData)))
        } catch (let error) {
            completion(Result.failure(error))
        }
    }
}

//
//  AmenityService.swift
//  Breeze
//
//  Created by Zhou Hao on 30/11/21.
//

import Foundation

final class AmenityService: AmenityServiceProtocol {
    
    let postSession = URLSession(configuration: .default)
    
    func getAmenities(request: AmenitiesRequest, completion: @escaping (Result<Amenities>)->Void) {
        let url = "\(Configuration.amenities)"
        do {
            
            let jsonData = try JSONEncoder().encode(request)
            let resource = Resource<Amenities>(url: url, parameters: parameters, body: jsonData, method: .post)
            postSession.load(resource, completion: completion)
            
        }
        catch(let error){
            print(error)
        }
    }
    
    func getAmenitiesFromV2(request: AmenitiesRequest, completion: @escaping (Result<AmenitiesBase>) -> Void) {
        
        let url = "\(Configuration.amenitiesV2)"
        do {
            
            let jsonData = try JSONEncoder().encode(request)
            let resource = Resource<AmenitiesBase>(url: url, parameters: parameters, body: jsonData, method: .post)
            postSession.load(resource, completion: completion)
        }
        catch(let error){
            print(error)
        }
        
    }
    
}

//
//  RecentContactsService.swift
//  Breeze
//
//  Created by Zhou Hao on 2/6/22.
//

import Foundation

protocol RecentContactsServiceProtocol {
    func getRecentContacts(_ completion: @escaping (Result<[RecentContact]>) -> ())
}

final class RecentContactsService: RecentContactsServiceProtocol {
    func getRecentContacts(_ completion: @escaping (Result<[RecentContact]>) -> ()) {
        let postSession = URLSession(configuration: .default)
        
        var allParameters : HTTPParameters = [String:Any]()
        allParameters = parameters
        
        let resource = Resource<[RecentContact]>(url: Configuration.getRecentContacts, parameters: allParameters, body: nil, method: .get)
        postSession.load(resource) { (result) in
            switch result {
            case .success(let theResult):
                completion(Result.success(theResult))
            case .failure(let error):
                completion(Result.failure(error))
            }
        }
    }
}

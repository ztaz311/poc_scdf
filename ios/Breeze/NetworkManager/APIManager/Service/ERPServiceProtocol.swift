//
//  ERPServiceProtocol.swift
//  Breeze
//
//  Created by Zhou Hao on 31/5/21.
//

import Foundation

protocol ERPServiceProtocol {
    func getERPDetails(_ completion: @escaping (Result<ERPBaseModel>) -> ()) 
}

//
//  FirebaseFeedbackService.swift
//  Breeze
//
//  Created by Zhou Hao on 21/5/21.
//

import Foundation
import Firebase
import FirebaseFirestore
import SwiftyBeaver
import Compression

final class FirebaseFeedbackService: FeedbackServiceProtocol {

    func submit(userId: String, description: String, fromLog: Bool = false, url: URL, progress: @escaping (Float) -> Void, completion: @escaping (Result<Bool>) -> Void, compression: Bool = true) {
        let db = Firestore.firestore()
        var ref: DocumentReference? = nil
        let now = Date()

        let fileName = url.lastPathComponent

        // upload data to Firebase storage
        let storage = Storage.storage()
        let storageRef = storage.reference()
        let dataRef = storageRef.child("\(userId)/\(Int(Date().timeIntervalSince1970))-\(fileName)\(compression ? ".lzfse" : "")")
        
        guard let sourceLength = FileHelper.fileSize(atURL: url), sourceLength > 0 else {
            completion(Result.success(true))
            return
        }
        
        /* in some case we upload log file
         First we clean tmp directory
         then copy file to tmp
         after upload, we need clean tmp directory again
        */
        var newURL = url
        do {
            if fromLog, let temURL = FileHelper.urlFor(fileNameInTempDirectory: FileHelper.breezeLogName) {
                newURL = temURL
                let fm = FileManager.default
                fm.clearTmpDirectory()
                try fm.copyItem(at: url, to: newURL)
            }
        } catch {
            SwiftyBeaver.debug("upload file log copy error \(error)")
        }
        
        
        let uploadTask = dataRef.putFile(from: newURL, metadata: nil) { metadata, error in
            guard let metadata = metadata else { return }
            FileManager.default.clearTmpDirectory()
            // Metadata contains file metadata such as size, content-type.
            let size = metadata.size

            dataRef.downloadURL { (url, error) in
                guard let downloadURL = url else {
                    // Uh-oh, an error occurred!
                    return
                }

                ref = db.collection(userId).addDocument(
                    data: [
                        "description": description,
                        "file": downloadURL.absoluteString,
                        "deviceos": UIDevice.current.deviceOS(),
                        "deviceosversion": UIDevice.current.systemVersionInfo(),
                        "devicemodel": UIDevice.current.getName(),
                        "appversion": UIApplication.appVersion(),
                        "time": now
                    ]) { err in
                    if let err = err {
                        completion(Result.failure(err))
                    } else {
                        SwiftyBeaver.debug("Document added with ID: \(ref!.documentID), file size = \(size), userId = \(userId)")
                        completion(Result.success(true))
                    }
                }
            }
        }

        uploadTask.observe(.progress) { snapshot in
            let percentComplete = 100.0 * Float(snapshot.progress!.completedUnitCount) / Float(snapshot.progress!.totalUnitCount)
            progress(percentComplete)
        }
        uploadTask.observe(.failure) { snapshot in
            if let error = snapshot.error as NSError? {
                completion(.failure(error))
            }
        }
    }
}

//
//  ERPRates.swift
//  Breeze
//
//  Created by VishnuKanth on 07/02/21.
//

import Foundation

struct ERPRates: ERPServiceProtocol {
    
    static let shared = ERPRates()
    
    let postSession = URLSession(configuration: .default)
    
    func getERPDetails(_ completion: @escaping (Result<ERPBaseModel>) -> ()) {
        let resource = Resource<ERPBaseModel>(url: Configuration.erpDetails, parameters:parameters, body: nil, method: .get)
        postSession.load(resource, completion: completion)        
    }
    
/*
    func getERPDetails(_ completion: @escaping (Result<ERPBaseModel>) -> ()) {
        
        let isTokenExpiryCheck =  AWSAuth.sharedInstance.checkTokenExpiry(idToken: AWSAuth.sharedInstance.idToken)
         if(isTokenExpiryCheck == true)
         {
             AWSAuth.sharedInstance.fetchCurrentSession { (result) in
                callERPApi() { (result) in
                    
                    switch result{
                      
                    case .success(let json):
                        completion(Result.success(json))
                    case .failure(let error):
                        completion(Result.failure(error))
                    }
                }
                 
             } onFailure: { (error) in
                 DispatchQueue.main.async {
                     
                 }
             }
         }
         else
         {
            callERPApi() { (result) in
                
                switch result{
                  
                case .success(let json):
                    completion(Result.success(json))
                case .failure(let error):
                    completion(Result.failure(error))
                }
            }
         }
    }
    
    func callERPApi(_ completion: @escaping (Result<ERPBaseModel>) -> ()){
        
        do{
            
            let parameters : HTTPParameters = [String:Any]()
            let request = try HTTPNetworkRequest.configureHTTPRequest(url: Configuration.erpDetails, parameters:parameters, headers: createHeader(), body: nil, method: .get)
            URLSession.shared.dataTask(with: request) { (data, res, err) in
                
                if let response = res as? HTTPURLResponse, let unwrappedData = data {
                    
                    let result = HTTPNetworkResponse.handleNetworkResponse(for: response)
                    switch result {
                    case .success:
                        
                        if let result = try? JSONDecoder().decode(ERPBaseModel.self, from: unwrappedData) {
                            completion(Result.success(result))
                        } else {
                            completion(Result.failure(HTTPNetworkError.decodingFailed))
                        }
                        
                    case .failure:
                        completion(Result.failure(HTTPNetworkError.decodingFailed))
                    }
                }
            }.resume()
        }catch{
            completion(Result.failure(HTTPNetworkError.badRequest))
        }
    }
*/
}

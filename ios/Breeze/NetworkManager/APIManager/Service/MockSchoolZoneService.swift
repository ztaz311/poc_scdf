//
//  MockSchoolZoneService.swift
//  Breeze
//
//  Created by Zhou Hao on 28/5/21.
//

import Foundation
import Turf

struct MockSchoolZoneService: SchoolZoneServiceProtocol {
    
    func getSchoolZone(_ completion: @escaping (Result<[SchoolZoneData]>) -> ()) {
        
        let url = Bundle.main.url(forResource: "schoolzone", withExtension: "json")!
        do {
            let jsonData = try Data(contentsOf: url)
            let geojson = try JSONDecoder().decode(FeatureCollection.self, from: jsonData)
            completion(Result.success(MockSchoolZoneService.map(featureCollection: geojson)))
        } catch (let error) {
            completion(Result.failure(error))
        }
    }
}

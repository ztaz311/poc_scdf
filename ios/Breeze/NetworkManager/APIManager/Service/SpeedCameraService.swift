//
//  SpeedCameraService.swift
//  Breeze
//
//  Created by VishnuKanth on 04/03/22.
//

import Foundation
import Turf

struct SpeedCameraService: SpeedCameraServiceProtocol {
    
    let postSession = URLSession(configuration: .default)
    
    func getSpeedCamera(_ completion: @escaping (Result<[SpeedCameraData]>) -> ()) {
        
        let url = "\(Configuration.dataset)/speedlightcamera.json"
        let resource = Resource<[SpeedCameraData]>(url: url, parameters:parameters, body: nil, method: .get)
        postSession.load(resource) { result in
            switch result {
            case .success(let speedData):
                completion(Result.success(speedData))
            case .failure(let error):
                completion(Result.failure(error))
            }
        }
    }
}


//
//  SearchRecommendServiceProtocol.swift
//  Breeze
//
//  Created by Zhou Hao on 20/5/21.
//

import Foundation

protocol SearchRecommendServiceProtocol {
    func getSearchRecommendation( lat:String?, long:String? ,resultCount:Int ,_ completion: @escaping (Result<[Addresses]>) -> ())
}

//
//  ETAFavServiceProtocol.swift
//  Breeze
//
//  Created by VishnuKanth on 12/08/21.
//

import Foundation

protocol ETAFavServiceProtocol {
    func getETAFavList(_ completion: @escaping (Result<[Favourites]>) -> ())
}

//
//  SettingsService.swift
//  Breeze
//
//  Created by Malou Mendoza on 31/5/21.
//

import Foundation


struct SettingsService{
    
    static let shared = SettingsService()
    
    let postSession = URLSession(configuration: .default)
    
    func getMasterLists(_ module: String, completion: @escaping (Result<MasterListData>) -> ()) {
        var allParameters : HTTPParameters = [String:Any]()
        allParameters?["module"] = module
        
        let resource = Resource<MasterListData>(url: Configuration.masterlist, parameters:allParameters, body: nil, method: .get)
        postSession.load(resource, completion: completion)
    }
    
    func getUserSettings(_ completion: @escaping (Result<UserSettings>) -> ()) {
        let resource = Resource<UserSettings>(url: Configuration.userSettings, parameters:[:], body: nil, method: .get)
        postSession.load(resource, completion: completion)
    }
    
    func updateSettingAttribute(_ attribute: String, value: String, _ completion: @escaping (Bool) -> ()) {
        var settingsData =  [[String:Any]]()
        let mapDisplay: [String: Any] = [
            UserSettingsModel.UserSettingConst.attribute : attribute,
            "value" : value
            ]
        settingsData.append(mapDisplay)
        
        DispatchQueue.global(qos: .background).async {
            
            SettingsService.shared.updateUserSettings(settings:settingsData) { (result) in
                switch result {
                case .success(_):
                    print("success")
                    Settings.shared.getUserSettings()
                    completion(true);
                case .failure(let error):
                    DispatchQueue.main.async {
                        print("SETTING UPDATED ERROR",error.localizedDescription)
                    }
                    completion(false);
                }
            }
        }
    }
    
    func updateUserSettings(settings:[[String : Any]],_ completion: @escaping (Result<Bool>) -> ()) {
        updateUserSettingsAction(method: .put, input: settings, completion)
    }
    
    private func updateUserSettingsAction(method: HTTPMethod, input: Any,_ completion: @escaping (Result<Bool>) -> ()) {

        var httpBody  = [String:Any]()
        httpBody["settings"] = input
        
        do{
            let data =  try JSONSerialization.data(withJSONObject: httpBody)
            
            let resource = Resource<EmptyData>(url: Configuration.userSettings, parameters: parameters, body: data, method: method)
            
            postSession.load(resource) { (result) in
                switch result {
                case .success(_):
                    completion(Result.success(true))
                case .failure(let error):
                    completion(Result.failure(error))
                }
            }
        }
        catch(let error){
            print(error.localizedDescription)
        }
    }

   
}

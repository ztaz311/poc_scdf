//
//  TripWalkathonServiceProtocol.swift
//  Breeze
//
//  Created by Santoso Pham on 18/10/2022.
//

import Foundation

protocol TripWalkathonServiceProtocol {
    func sendWalkathonTripSummary(summary: WalkathonTripSummary ,completion: @escaping (Result<Int>)->Void)
}

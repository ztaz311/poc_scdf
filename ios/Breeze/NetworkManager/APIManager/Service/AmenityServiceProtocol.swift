//
//  AmenityServiceProtocol.swift
//  Breeze
//
//  Created by Zhou Hao on 30/11/21.
//

import Foundation

protocol AmenityServiceProtocol {
    func getAmenities(request: AmenitiesRequest, completion: @escaping (Result<Amenities>)->Void)
    
    func getAmenitiesFromV2(request: AmenitiesRequest, completion: @escaping (Result<AmenitiesBase>)->Void)
}


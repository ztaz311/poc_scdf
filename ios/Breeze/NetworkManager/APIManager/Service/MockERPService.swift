//
//  MockERPService.swift
//  Breeze
//
//  Created by Zhou Hao on 31/5/21.
//

import Foundation

struct MockERPService: ERPServiceProtocol {
    func getERPDetails(_ completion: @escaping (Result<ERPBaseModel>) -> ()) {
        
        let fileUrl = Bundle.main.resourceURL?.appendingPathComponent("./MockERP.json")
        
        do {
            let data = try Data(contentsOf: fileUrl!)
            let erpData = try JSONDecoder().decode(ERPBaseModel.self, from: data)
            completion(Result.success(erpData))
            
        } catch {
            print(error)
            completion(Result.failure(error))
        }
    }
}

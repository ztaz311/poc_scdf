//
//  SilverZoneService.swift
//  Breeze
//
//  Created by Zhou Hao on 28/5/21.
//

import Foundation
import Turf

struct SilverZoneService: SilverZoneServiceProtocol {
    
    let postSession = URLSession(configuration: .default)
    
    func getSilverZone(_ completion: @escaping (Result<[SilverZoneData]>) -> ()) {
        
        let url = "\(Configuration.dataset)/silverzone.json"
        let resource = Resource<FeatureCollection>(url: url, parameters:parameters, body: nil, method: .get)
        postSession.load(resource) { result in
            switch result {
            case .success(let featureCollection):
                completion(Result.success(SilverZoneService.map(featureCollection: featureCollection)))
            case .failure(let error):
                completion(Result.failure(error))
            }
        }
    }
}

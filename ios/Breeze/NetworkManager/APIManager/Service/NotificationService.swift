//
//  NotificationService.swift
//  Breeze
//
//  Created by Malou Mendoza on 4/3/22.
//

import Foundation

protocol NotificationServiceProtocol {
    func getNotifications( _ completion: @escaping (Result<GlobalNotification>) -> ())
}

final class MockNotificationService: NotificationServiceProtocol {
    enum MockError: Error {
        case parseError
    }
    
    func getNotifications( _ completion: @escaping (Result<GlobalNotification>) -> ()) {
        // Test 1: multiple notifications (2 with the same priority)
//        if let n1 = testJson1() {
//            completion(Result.success(n1))
//        } else {
//            completion(Result.failure(MockError.parseError))
//        }
        
        // Test 2: one admin
//        if let n1 = testJson2() {
//            completion(Result.success(n1))
//        } else {
//            completion(Result.failure(MockError.parseError))
//        }

        // Test 3: one road closure
        if let n1 = testJson3() {
            completion(Result.success(n1))
        } else {
            completion(Result.failure(MockError.parseError))
        }
        
        // Test 4: no data
//        if let n1 = testJson3() {
//            completion(Result.success(n1))
//        } else {
//            completion(Result.failure(MockError.parseError))
//        }

    }
}

extension MockNotificationService {
    func testJson1() -> GlobalNotification? {
        let json = """
        {
            "totalItems": 5,
            "totalPages": 1,
            "currentPage": 1,
            "notifications": [
                {
                    "notificationId": 6,
                    "category": "ADMIN",
                    "type": "MAINTENANCE",
                    "title": "System maintenance",
                    "message": "Our scheduled system maintenance will run on 25 Jan 2022.",
                    "description": "Please upgrade your IU at your nearby LTA workshop. Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum",
                    "short_name": "System maintenance",
                    "latitude": null,
                    "longitude": null,
                    "startTime": 1646305476,
                    "expireTime": 1646910279,
                    "imageLinkLight": "https://dev-breeze-assets.s3.ap-southeast-1.amazonaws.com/notifications/maintenance.png",
                    "imageLinkDark": "https://dev-breeze-assets.s3.ap-southeast-1.amazonaws.com/notifications/maintenance.png",
                    "priority": 6
                },
                {
                    "notificationId": 4,
                    "category": "TRAFFIC_ALERTS",
                    "type": "ROAD_CLOSURE",
                    "title": "Road closure alert",
                    "message": "Road closure at CTE near Yio Chu Kang.",
                    "description": "test",
                    "short_name": "Road closure",
                    "latitude": 1.3021309167836832,
                    "longitude": 103.89778381511323,
                    "startTime": 1646305229,
                    "expireTime": 1646910038,
                    "imageLinkLight": "https://dev-breeze-assets.s3.ap-southeast-1.amazonaws.com/notifications/roadclouser.png",
                    "imageLinkDark": "https://dev-breeze-assets.s3.ap-southeast-1.amazonaws.com/notifications/roadclouser.png",
                    "priority": 4
                },
                {
                    "notificationId": 2,
                    "category": "TRAFFIC_ALERTS",
                    "type": "FLASH_FLOOD",
                    "title": "Flood alert",
                    "message": "(9/2)16:00 Flash Flood on West Coast Highway (towards City) at W Coast Hwy Flyover.",
                    "description": "test",
                    "short_name": "Flood",
                    "latitude": 1.2659876039931541,
                    "longitude": 103.82506330615617,
                    "startTime": 1646304890,
                    "expireTime": 1646909695,
                    "imageLinkLight": "https://dev-breeze-assets.s3.ap-southeast-1.amazonaws.com/notifications/flood.png",
                    "imageLinkDark": "https://dev-breeze-assets.s3.ap-southeast-1.amazonaws.com/notifications/flood.png",
                    "priority": 2
                },
                {
                    "notificationId": 1,
                    "category": "TRAFFIC_ALERTS",
                    "type": "MAJOR_ACCIDENT",
                    "title": "Accident Alert",
                    "message": "A serious accident happened at CTE near Yio Chu Kang.",
                    "description": "test",
                    "short_name": "Accident",
                    "latitude": 1.3443783771971678,
                    "longitude": 103.80134400737896,
                    "startTime": 1646304666,
                    "expireTime": 1646909471,
                    "imageLinkLight": "https://dev-breeze-assets.s3.ap-southeast-1.amazonaws.com/notifications/majoraccident.png",
                    "imageLinkDark": "https://dev-breeze-assets.s3.ap-southeast-1.amazonaws.com/notifications/majoraccident.png",
                    "priority": 1
                },
                {
                    "notificationId": 5,
                    "category": "ADMIN",
                    "type": "ANNOUNCEMENT",
                    "title": "IU upgrade",
                    "message": "Please upgrade your IU at your nearby LTA workshop",
                    "description": "Please upgrade your IU at your nearby LTA workshop. Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum",
                    "short_name": "IU upgrade",
                    "latitude": null,
                    "longitude": null,
                    "startTime": 1646298213,
                    "expireTime": 1646910217,
                    "imageLinkLight": "https://dev-breeze-assets.s3.ap-southeast-1.amazonaws.com/notifications/announcement.png",
                    "imageLinkDark": "https://dev-breeze-assets.s3.ap-southeast-1.amazonaws.com/notifications/announcement.png",
                    "priority": 5
                },
                {
                    "notificationId": 111,
                    "category": "TRAFFIC_ALERTS",
                    "type": "MAJOR_ACCIDENT",
                    "title": "Accident Alert",
                    "message": "A serious accident happened at CTE near Yio Chu Kang. Testing.",
                    "description": "test",
                    "short_name": "Accident",
                    "latitude": 1.3443783771971678,
                    "longitude": 103.80134400737896,
                    "startTime": 1646304666,
                    "expireTime": 1646909471,
                    "imageLinkLight": "https://dev-breeze-assets.s3.ap-southeast-1.amazonaws.com/notifications/majoraccident.png",
                    "imageLinkDark": "https://dev-breeze-assets.s3.ap-southeast-1.amazonaws.com/notifications/majoraccident.png",
                    "priority": 1
                },
            ]
        }
        """
        if let jsonData = json.data(using: .utf8) {
            if let notifications = try? JSONDecoder().decode(GlobalNotification.self, from: jsonData) {
                return notifications
            }
        }
        return nil
    }
    
    // 1 admin
    func testJson2() -> GlobalNotification? {
        let json = """
        {
            "totalItems": 5,
            "totalPages": 1,
            "currentPage": 1,
            "notifications": [
                {
                    "notificationId": 6,
                    "category": "ADMIN",
                    "type": "MAINTENANCE",
                    "title": "System maintenance",
                    "message": "Our scheduled system maintenance will run on 25 Jan 2022.",
                    "description": "Please upgrade your IU at your nearby LTA workshop. Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum",
                    "short_name": "System maintenance",
                    "latitude": null,
                    "longitude": null,
                    "startTime": 1646305476,
                    "expireTime": 1646910279,
                    "imageLinkLight": "https://dev-breeze-assets.s3.ap-southeast-1.amazonaws.com/notifications/maintenance.png",
                    "imageLinkDark": "https://dev-breeze-assets.s3.ap-southeast-1.amazonaws.com/notifications/maintenance.png",
                    "priority": 6
                }
            ]
        }
        """
        if let jsonData = json.data(using: .utf8) {
            if let notifications = try? JSONDecoder().decode(GlobalNotification.self, from: jsonData) {
                return notifications
            }
        }
        return nil
    }
    
    // 1 traffic (road closure)
    func testJson3() -> GlobalNotification? {
        let json = """
        {
            "totalItems": 5,
            "totalPages": 1,
            "currentPage": 1,
            "notifications": [
                {
                    "notificationId": 118,
                    "category": "TRAFFIC_ALERTS",
                    "type": "ROAD_CLOSURE",
                    "title": "Road closure alert",
                    "message": "Road closure at CTE near Yio Chu Kang.",
                    "description": "test",
                    "short_name": "Road closure",
                    "latitude": 1.322932,
                    "longitude": 103.823015,
                    "startTime": 1646305229,
                    "expireTime": 1646910038,
                    "imageLinkLight": "https://dev-breeze-assets.s3.ap-southeast-1.amazonaws.com/notifications/roadclouser.png",
                    "imageLinkDark": "https://dev-breeze-assets.s3.ap-southeast-1.amazonaws.com/notifications/roadclouser.png",
                    "priority": 4
                }
            ]
        }
        """
        if let jsonData = json.data(using: .utf8) {
            if let notifications = try? JSONDecoder().decode(GlobalNotification.self, from: jsonData) {
                return notifications
            }
        }
        return nil
    }
    
    // no data
    func testJson4() -> GlobalNotification? {
        let json = """
        {
            "totalItems": 5,
            "totalPages": 1,
            "currentPage": 1,
            "notifications": [
            ]
        }
        """
        if let jsonData = json.data(using: .utf8) {
            if let notifications = try? JSONDecoder().decode(GlobalNotification.self, from: jsonData) {
                return notifications
            }
        }
        return nil
    }
}

final class NotificationService: NotificationServiceProtocol {
    
    func getNotifications( _ completion: @escaping (Result<GlobalNotification>) -> ()) {
        
        let getSession = URLSession(configuration: .default)
        
        var allParameters : HTTPParameters = [String:Any]()
        allParameters?["page"] = 1
        allParameters?["pagesize"] = 50
        
        let resource = Resource<GlobalNotification>(url: Configuration.getNotifications, parameters: allParameters, body: nil, method: .get)
        getSession.load(resource) { (result) in
            switch result {
            case .success(let theResult):
                completion(Result.success(theResult))
            case .failure(let error):
                completion(Result.failure(error))
            }
        }
    }
}



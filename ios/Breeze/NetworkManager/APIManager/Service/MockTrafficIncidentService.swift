//
//  MockTrafficIncidentService.swift
//  Breeze
//
//  Created by Zhou Hao on 1/6/21.
//

import Foundation

struct MockTrafficIncidentService: TrafficIncidentServiceProtocol {
        
    func getTrafficIncidents(_ completion: @escaping (Result<Incidents>) -> ()) {
        let url = Bundle.main.url(forResource: "traffic", withExtension: "json")!
        do {
            let jsonData = try Data(contentsOf: url)
            let incidents: Incidents = try JSONDecoder().decode(Incidents.self, from: jsonData)
            completion(Result.success(incidents))
        } catch (let error) {
            completion(Result.failure(error))
        }
    }
}

//
//  BroadcastMessageService.swift
//  Breeze
//
//  Created by Tuan, Pham Hai on 09/02/2023.
//

import Foundation

final class MockBroadcastMessageService {
    
    func getBroadcastMessage(carparkId: String, mode: BroadcastMode, _ completion: @escaping (Result<BroadcastMessageModel>) -> ()) {
        
        var jsonName = "broadcastMessage"
        if mode == .navigation {
            jsonName = "broadcastNavigation"
        }
        
        let url = Bundle.main.url(forResource: jsonName, withExtension: "json")!
        do {
            let jsonData = try Data(contentsOf: url)
            completion(Result.success(try JSONDecoder().decode(BroadcastMessageModel.self, from: jsonData)))
        } catch (let error) {
            completion(Result.failure(error))
        }
    }
}

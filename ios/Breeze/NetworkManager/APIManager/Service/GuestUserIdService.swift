//
//  GuestUserIdService.swift
//  Breeze
//
//  Created by Tuan, Pham Hai on 14/03/2023.
//

import Foundation

final class GuestUserIdService {
    func getGuestUserId(_ completion: @escaping (Result<GuestUserIdModel>) -> ()) {
        let session = URLSession(configuration: .default)
        let resource = Resource<GuestUserIdModel>(url: Configuration.getGuestUserId, parameters: [:], body: nil, method: .get)
        session.loadWithoutHeader(resource, completion: completion)
    }
}

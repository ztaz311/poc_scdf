//
//  SilverZoneServiceProtocol.swift
//  Breeze
//
//  Created by Zhou Hao on 28/5/21.
//

import Foundation
import Turf

enum SilverZoneServiceError: Error {
    case failedToParseJson
    case failedToLoad
}

protocol SilverZoneServiceProtocol {
    // Get silver zone features from API
    /**
     Get silver zone features from API
          
     - parameter completion: The closure (block) to call with the resulting `FeatureCollection`.
     */
    func getSilverZone(_ completion: @escaping (Result<[SilverZoneData]>) -> ())
}

extension SilverZoneServiceProtocol {
    
    static func map(featureCollection: FeatureCollection) -> [SilverZoneData] {        
        return featureCollection.features.enumerated().map { (index,feature) in
            let address = feature.properties?["SITENAME"] as? String ?? ""
            return SilverZoneData(id: "\(FeatureDetection.featureTypeSilverZone)-\(index+1)", feature: feature, address: address)
        }
    }
}

//
//  TripWalkathonService.swift
//  Breeze
//
//  Created by Santoso Pham on 18/10/2022.
//

import Foundation
import Amplify
import AWSMobileClient
import AWSKinesis

final class TripWalkathonService: TripWalkathonServiceProtocol {
    
    func sendWalkathonTripSummary(summary: WalkathonTripSummary ,completion: @escaping (Result<Int>)->Void) {
        do{
            let jsonData = try JSONEncoder().encode(summary)
            let postSession = URLSession(configuration: .default)
            let resource = Resource<WalkathonTripSummaryResponse>(url: Configuration.walkathonWalkingTripApi, parameters: parameters, body: jsonData, method: .post)
            postSession.load(resource) { (result) in
                switch result {
                case .success(let response):
                    print(response)
                    completion(Result.success(response.walking_trip_id))
                case .failure(let error):
                    print(error)
                    completion(Result.failure(error))
                }
            }
        }
        catch(let error){
            print(error.localizedDescription)
        }
    }
}

//
//  UserService.swift
//  Breeze
//
//  Created by Tuyen, Le Xuan  on 26/05/2022.
//

import Foundation

protocol UserServiceProtocol {
    func getUserData(completion: @escaping (Result<UserDataModel>) -> ())
    func updateTNC(completion: @escaping (Result<Bool>) -> ())
    func setUserPrefEmpty(completion: @escaping (Result<UserPref>) -> ())
}

struct UserService: UserServiceProtocol {
    func getUserData(completion: @escaping (Result<UserDataModel>) -> ()) {
        
        let postSession = URLSession(configuration: .default)
        
        let resource = Resource<UserDataModel>(url: Configuration.userData, parameters: [:], body: nil, method: .get)
        postSession.load(resource) { (result) in
            switch result {
            case .success(let theResult):
                completion(Result.success(theResult))
            case .failure(let error):
                completion(Result.failure(error))
            }
        }
    }
    
    func getUserPrefs(completion: @escaping (Result<String>) -> ()) {
        
        let postSession = URLSession(configuration: .default)
        let resource = Resource<String>(url: Configuration.userPrefv4, parameters: [:], body: nil, method: .get)
        postSession.loadUserPrefsV4(resource) { (result) in
            switch result {
            case .success(let theResult):
                completion(Result.success(theResult))
            case .failure(let error):
                completion(Result.failure(error))
            }
        }
    }
    
    func updateTNC(completion: @escaping (Result<Bool>) -> ()) {
        
        let postSession = URLSession(configuration: .default)
        do {
            let params = ["tncAcceptStatus": true]
            let data =  try JSONSerialization.data(withJSONObject: params)
            
            let resource = Resource<DefaultResponse>(url: Configuration.userTNC, parameters: [:], body: data, method: .post)
            
            postSession.load(resource) { (result) in
                switch result {
                case .success(let value):
                    completion(Result.success(value.success))
                case .failure(let error):
                    completion(Result.failure(error))
                }
            }
        } catch {
            print(error)
        }
        
    }
    
    func setUserPrefEmpty(completion: @escaping (Result<UserPref>) -> ()) {
        
        let postSession = URLSession(configuration: .default)
        do {
            let params = [
                 "notification_preference" : [],
                 "amenities_preference" :[]
                 
            ]
            let data =  try JSONSerialization.data(withJSONObject: params)
            
            let resource = Resource<UserPref>(url: Configuration.userPref, parameters: [:], body: data, method: .post)
            
            postSession.load(resource) { (result) in
                switch result {
                case .success(let value):
                    completion(Result.success(value))
                case .failure(let error):
                    completion(Result.failure(error))
                }
            }
        } catch {
            print(error)
        }
        
    }
    
    
}

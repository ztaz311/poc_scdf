//
//  ObuDataServiceProtocol.swift
//  Breeze
//
//  Created by Tuan, Pham Hai on 05/06/2023.
//

import Foundation
import OBUSDK

protocol ObuDataServiceProtocol {
    /// Send charging info bis func ERP
    func sendTripObuErp(_ chargingInfo: [BreezeObuChargingInfo], tripGUID: String, roadName: String?, lat: Double?, long: Double?, completion: @escaping (Result<Bool>)->Void)
    /// Send charging info bis func EEP & EPS
//    func sendTripObuCharging(_ chargingInfo: BreezeObuChargingInfo, tripGUID: String, vehicleNumber: String, roadName: String?, completion: @escaping (Result<Bool>)->Void)
    /// Send obu trip summary
    func sendObuTripSummary(_ travelInfo: BreezeTravelSummary, tripGUID: String, completion: @escaping (Result<Bool>)->Void)
    /// Send obu transactions when connect obu success or on payment history
    func sendObuTransactions(_ transactions: [BreezePaymentHistory], tripGUID: String, completion: @escaping (Result<Bool>)->Void)
    /// Get carpark list by filter names
    func getCarparkByFilterNames(_ carparkNames: [String], completion: @escaping (Result<[Carpark]>)->Void)
    /// Get carpark list by filter names
    func sendChargeInfo(_ names: [String], tripGUID: String, completion: @escaping (Result<[Carpark]>)->Void)
}

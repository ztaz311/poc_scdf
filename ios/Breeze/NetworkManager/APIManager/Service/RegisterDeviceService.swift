//
//  RegisterDevice.swift
//  Breeze
//
//  Created by VishnuKanth on 18/06/21.
//

import Foundation

final class RegisterDeviceService:RegisterDeviceServiceProtocol{
    
    func registerDevice(deviceID:String,token:String,deviceos:String ,completion: @escaping (Result<RegisterDeviceResponse>)->Void){
        
        let postSession = URLSession(configuration: .default)
        
        let data = [
            "deviceID":deviceID,
            "token":token,
            "deviceos":deviceos
        ] as [String : Any]
        
        do {
            let body =  try JSONSerialization.data(withJSONObject: data)
            let resource = Resource<RegisterDeviceResponse>(url: Configuration.registerDevice, parameters: [:], body: body, method: .post)
            postSession.load(resource, completion: completion)
        }
        catch(let error){
            print(error.localizedDescription)
        }
        
    }
    
}

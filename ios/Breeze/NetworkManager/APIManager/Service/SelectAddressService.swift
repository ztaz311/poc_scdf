//
//  SelectAddressService.swift
//  Breeze
//
//  Created by Malou Mendoza on 3/2/21.
//

import Foundation

struct SelectAddressService {
    
    static let shared = SelectAddressService()
    let postSession = URLSession(configuration: .default)

    func selectAddress(address:[String:Any],_ completion: @escaping (Result<Bool>) -> ()) {
        
        do{
            let data =  try JSONSerialization.data(withJSONObject: address)
            let resource = Resource<EmptyData>(url: Configuration.selectAddress, parameters: parameters, body: data, method: .post)
            postSession.load(resource) { (result) in
                switch result {
                case .success(_):
                    completion(Result.success(true))
                case .failure(let error):
                    completion(Result.failure(error))
                }
            }
        }
        catch(let error){
            print(error.localizedDescription)
        }
    }

/*
    func selectAddress(address:[String:Any],_ completion: @escaping (Result<Bool>) -> ()){
        
        let isTokenExpiryCheck =  AWSAuth.sharedInstance.checkTokenExpiry(idToken: AWSAuth.sharedInstance.idToken)
         if(isTokenExpiryCheck == true)
         {
             AWSAuth.sharedInstance.fetchCurrentSession { (result) in
                allAddressSelectionApi(address:address) { (result) in
                    
                    switch result{
                      
                    case .success(_):
                        completion(Result.success(true))
                    case .failure(let error):
                        completion(Result.failure(error))
                    }
                }
                 
             } onFailure: { (error) in
                 DispatchQueue.main.async {
                     
                 }
             }
         }
         else
         {
            allAddressSelectionApi(address:address) { (result) in
                
                switch result{
                  
                case .success(_):
                    completion(Result.success(true))
                case .failure(let error):
                    completion(Result.failure(error))
                }
            }
         }
        
    }
    
    func allAddressSelectionApi(address:[String:Any],_ completion: @escaping (Result<Bool>) -> ()){
        
        do{

            
          let data =  try! JSONSerialization.data(withJSONObject: address)
            let request = try HTTPNetworkRequest.configureHTTPRequest(url: Configuration.selectAddress, parameters: parameters, headers: createHeader(), body: data, method: .post)
            postSession.dataTask(with: request) { (data, res, err) in
                
                if let response = res as? HTTPURLResponse, let unwrappedData = data {
                    
                    let result = HTTPNetworkResponse.handleNetworkResponse(for: response)
                    switch result {
                    case .success:
                        
                        completion(Result.success(true))
                        
                    case .failure:
                        completion(Result.failure(HTTPNetworkError.decodingFailed))
                    }
                }
            }.resume()
        }catch{
            completion(Result.failure(HTTPNetworkError.badRequest))
        }
        
    }
*/
}

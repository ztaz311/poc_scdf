//
//  FeedbackIssueService.swift
//  Breeze
//
//  Created by Malou Mendoza on 13/8/21.
//



import Foundation

final class FeedbackIssueService {
    let postSession = URLSession(configuration: .default)
    
    func post(feedbackIssue: FeedbackIssue, completion: @escaping (Result<Bool>) -> ()) {
        
        do{
            
            let jsonData = try JSONEncoder().encode(feedbackIssue)
            let postSession = URLSession(configuration: .default)
            let resource = Resource<EmptyData>(url: Configuration.tripFeedbackIssue, parameters: parameters, body: jsonData, method: .post)
            postSession.load(resource) { (result) in
                switch result {
                case .success(_):
                    completion(Result.success(true))
                case .failure(let error):
                    completion(Result.failure(error))
                }
            }
        }
        catch(let error){
            print(error.localizedDescription)
        }
        
    }
}



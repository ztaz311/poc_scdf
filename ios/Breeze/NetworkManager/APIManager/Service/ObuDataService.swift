//
//  ObuDataService.swift
//  Breeze
//
//  Created by Tuan, Pham Hai on 05/06/2023.
//

import Foundation
import OBUSDK
import SwiftyBeaver

final class ObuDataService: ObuDataServiceProtocol {
    
    let postSession = URLSession(configuration: .default)
    
    /// Send charging info bis func ERP
    func sendTripObuErp(_ chargingInfo: [BreezeObuChargingInfo], tripGUID: String, roadName: String?, lat: Double?, long: Double?, completion: @escaping (Result<Bool>)->Void) {
        
        do {
            var infos: [[String: Any]] = []
            for info in chargingInfo {
                infos.append(info.getPostDataWithTripID("", vehicleNumber: "", roadName: roadName))
            }
            
            var params: [String : Any] = [:]
            if tripGUID.isEmpty {
                params = ["vehicleNumber": OBUHelper.shared.obuName, "chargingInfo": infos] as [String : Any]
            } else {
                params = ["tripGUID": tripGUID, "vehicleNumber": OBUHelper.shared.obuName, "chargingInfo": infos] as [String : Any]
            }
            
            if let latitude = lat, let longitude = long {
                params["lat"] = latitude
                params["long"] = longitude
            }
            
            SwiftyBeaver.debug("sendTripObuErp params: \(params)")
            
            let jsonData = try JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
            
            let resource = Resource<EmptyData>(url: Configuration.saveTripObuChargeInfo, parameters: parameters, body: jsonData, method: .post)
            
            postSession.load(resource) { (result) in
                switch result {
                case .success(_):
                    completion(Result.success(true))
                case .failure(let error):
                    completion(Result.failure(error))
                }
            }
        }
        catch(let error){
            print(error.localizedDescription)
        }
    }
    
    /// Send charging info bis func EEP & EPS -> Confirmed Anirudh no need to send this API
    /*
    func sendTripObuCharging(_ chargingInfo: BreezeObuChargingInfo, tripGUID: String, roadName: String?, completion: @escaping (Result<Bool>)->Void) {
        do{
            let params = chargingInfo.getPostDataWithTripID(tripGUID, roadName: roadName)
            let resource = Resource<EmptyData>(url: Configuration.saveTripObuErp, parameters: parameters, body: params, method: .post)
            postSession.load(resource) { (result) in
                switch result {
                case .success(_):
                    completion(Result.success(true))
                case .failure(let error):
                    completion(Result.failure(error))
                }
            }

        }
        catch(let error){
            print(error.localizedDescription)
        }
    }
    */
    
    /// Send obu trip summary
    func sendObuTripSummary(_ travelInfo: BreezeTravelSummary, tripGUID: String, completion: @escaping (Result<Bool>)->Void) {
        do{
            let params = travelInfo.getPostDataWithTripID(tripGUID, vehicleNumber: OBUHelper.shared.obuName)
            
            SwiftyBeaver.debug("sendObuTripSummary: \(params)")
            
            let jsonData = try JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
            
            let resource = Resource<EmptyData>(url: Configuration.saveObuTripSummary, parameters: parameters, body: jsonData, method: .post)
            postSession.load(resource) { (result) in
                switch result {
                case .success(_):
                    completion(Result.success(true))
                case .failure(let error):
                    completion(Result.failure(error))
                }
            }

        }
        catch(let error){
            print(error.localizedDescription)
        }
    }
    
    /// Send obu transactions when connect obu success or on payment history
    func sendObuTransactions(_ transactions: [BreezePaymentHistory], tripGUID: String, completion: @escaping (Result<Bool>)->Void) {
        do{
            
            var payments: [[String: Any]] = []
            for transaction in transactions {
                payments.append(transaction.getPostData())
            }
            
            var params: [String : Any] = [:]
            if tripGUID.isEmpty {
                params = ["vehicleNumber": OBUHelper.shared.obuName, "payments": payments]
            } else {
                params = ["tripGUID": tripGUID, "vehicleNumber": OBUHelper.shared.obuName, "payments": payments]
            }
            
            SwiftyBeaver.debug("sendObuTransactions: \(params)")
            
            let jsonData = try JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
            
            let resource = Resource<EmptyData>(url: Configuration.saveObuTransactions, parameters: parameters, body: jsonData, method: .post)
            postSession.load(resource) { (result) in
                switch result {
                case .success(_):
                    completion(Result.success(true))
                case .failure(let error):
                    completion(Result.failure(error))
                }
            }

        }
        catch(let error){
            print(error.localizedDescription)
        }
    }
    
    /// Get carpark list by filter names
    func getCarparkByFilterNames(_ carparkNames: [String], completion: @escaping (Result<[Carpark]>)->Void) {
        //  Nothing to be done from Native side -> RN will handle it
    }
    
    /// Get carpark list by filter names
    func sendChargeInfo(_ names: [String], tripGUID: String, completion: @escaping (Result<[Carpark]>)->Void) {
        
    }
}

//
//  SpeedCameraServiceProtocol.swift
//  Breeze
//
//  Created by VishnuKanth on 04/03/22.
//

import Foundation
import Turf
import CoreLocation

enum SpeedCameraServiceError: Error {
    case failedToLoad
    case failedToParseJson
}

protocol SpeedCameraServiceProtocol {
    // Get school zone features from API
    /**
     Get school zone features from API
          
     - parameter completion: The closure (block) to call with the resulting `FeatureCollection`.
     */
    func getSpeedCamera(_ completion: @escaping (Result<[SpeedCameraData]>) -> ())
}

extension SpeedCameraServiceProtocol {
    static func map(speedCamData: [SpeedCameraData]) -> Turf.FeatureCollection {
        
        var speedCollection = [Turf.Feature]()
        for speedCamDatum in speedCamData {
            
            let lat = speedCamDatum.location_latitude?.toDouble()
            let long = speedCamDatum.location_longitude?.toDouble()
            
            let speedCamID = speedCamDatum._id ?? 0
            
            let speedCoord = CLLocationCoordinate2D(latitude: lat ?? 0.0, longitude: long ?? 0.0)
            
            var feature = Turf.Feature(geometry: .point(Point(speedCoord)))
            
            feature.properties = [
                "Type":.string(speedCamDatum.type_of_speed_camera ?? ""),
                "id": .string("\(speedCamID)")
            ]
            
            speedCollection.append(feature)
            
        }
        
        return Turf.FeatureCollection(features: speedCollection)
    }
}

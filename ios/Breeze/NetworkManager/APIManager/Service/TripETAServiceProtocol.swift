//
//  TripETAServiceProtocol.swift
//  Breeze
//
//  Created by Zhou Hao on 13/7/21.
//

import Foundation

protocol TripETAServiceProtocol {
    func sendTripETA(_ tripETA: TripETA, completion: @escaping (Result<TripETAResponse>)->Void)
    func sendTripCruiseETA(_ tripETA: TripCruiseETA, completion: @escaping (Result<TripETAResponse>)->Void)
    func sendReminder(_ tripETAReminder: TripETAReminder, completion: @escaping (Result<TripETAReminderResponse>)->Void)
    func updateLocationSharingStatus(_ liveLocationStatus: LiveLocationStatus, completion: @escaping (Result<LiveLocationStatusResponse>)->Void)
}

//
//  RatingService.swift
//  Breeze
//
//  Created by Zhou Hao on 13/8/21.
//

import Foundation

final class RatingService {
    let postSession = URLSession(configuration: .default)
    
    func post(tripId: String, rating: Int, completion: @escaping (Result<Bool>) -> ()) {
        var httpBody = [String:Any]()
        httpBody["tripGUID"] = tripId
        httpBody["rating"] = "\(rating)"
        
        do{
            let data =  try JSONSerialization.data(withJSONObject: httpBody)
            
            let resource = Resource<EmptyData>(url: Configuration.tripFeedback, parameters: parameters, body: data, method: .post)
            
            postSession.load(resource) { (result) in
                switch result {
                case .success(_):
                    completion(Result.success(true))
                case .failure(let error):
                    completion(Result.failure(error))
                }
            }
        }
        catch(let error){
            print(error.localizedDescription)
        }
        
    }
}

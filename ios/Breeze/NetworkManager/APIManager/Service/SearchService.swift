//
//  SearchService.swift
//  Breeze
//
//  Created by VishnuKanth on 19/12/20.
//

import Foundation

struct SearchRecommendService: SearchRecommendServiceProtocol {
    
    static let shared = SearchRecommendService()
    let postSession = URLSession(configuration: .default)
    
    func getSearchRecommendation( lat:String?, long:String? ,resultCount:Int ,_ completion: @escaping (Result<[Addresses]>) -> ()) {
                
        var allParameters : HTTPParameters = [String:Any]()
        allParameters = parameters
        allParameters?["lat"] = lat
        allParameters?["long"] = long
        allParameters?["resultcount"] = resultCount
        let resource = Resource<AddressRecommendation>(url: Configuration.addressRecommendations, parameters: allParameters, body: nil, method: .get)
        postSession.load(resource) { (result) in
            switch result {
            case .success(let theResult):
                completion(Result.success(theResult.addresses ?? []))
            case .failure(let error):
                completion(Result.failure(error))
            }
        }
    }
    
    func getSearchResult( lat:String?, long:String?,searchString:String,_ completion: @escaping (Result<[SearchAddresses]>) -> ()) {
        
        var allParameters : HTTPParameters = [String:Any]()
        allParameters = parameters
        allParameters?["lat"] = lat
        allParameters?["long"] = long
        allParameters?["searchstring"] = searchString
        let resource = Resource<BaseSearchAddress>(url: Configuration.searhcAddressEndPoint, parameters: allParameters, body: nil, method: .get)
        postSession.load(resource) { (result) in
            switch result {
            case .success(let theResult):
                completion(Result.success(theResult.addresses ?? []))
            case .failure(let error):
                completion(Result.failure(error))
            }
        }
    }
    
    func getNewSearchResult( lat:String?, long:String?,searchString:String, token: String,_ completion: @escaping (Result<NewSearchAddressResultModel>) -> ()) {
        
        var allParameters : HTTPParameters = [String:Any]()
        allParameters?["searchstring"] = searchString
        allParameters?["lat"] = lat
        allParameters?["long"] = long
        allParameters?["token"] = token
        
        let resource = Resource<NewSearchAddressResultModel>(url: Configuration.newSearchAddress, parameters: allParameters, body: nil, method: .get)
        postSession.load(resource) { (result) in
            switch result {
            case .success(let theResult):
                print(theResult)
                completion(Result.success(theResult))
            case .failure(let error):
                print(error)
                completion(Result.failure(error))
            }
        }
    }
    
    func getSearchDetails( placeId:String?,  token: String,_ completion: @escaping (Result<AddressDetailModel>) -> ()) {
        
        var allParameters : HTTPParameters = [String:Any]()
        allParameters?["placeId"] = placeId
        allParameters?["token"] = token
        
        let resource = Resource<AddressDetailModel>(url: Configuration.searchDetails, parameters: allParameters, body: nil, method: .get)
        postSession.load(resource) { (result) in
            switch result {
            case .success(let theResult):
                print(theResult)
                completion(Result.success(theResult))
            case .failure(let error):
                print(error)
                completion(Result.failure(error))
            }
        }
    }
        
/*
    let postSession = URLSession(configuration: .default)
    
    func getSearchRecommendation( lat:Double?, long:Double? ,resultCount:Int ,_ completion: @escaping (Result<[Addresses]>) -> ()) {
        
        let isTokenExpiryCheck =  AWSAuth.sharedInstance.checkTokenExpiry(idToken: AWSAuth.sharedInstance.idToken)
         if(isTokenExpiryCheck == true)
         {
             AWSAuth.sharedInstance.fetchCurrentSession { (result) in
                callAddressRecommendationAPI(lat: lat, long: long, resultCount: resultCount) { (result) in
                    
                    switch result{
                      
                    case .success(let json):
                        completion(Result.success(json))
                    case .failure(let error):
                        completion(Result.failure(error))
                    }
                }
                 
             } onFailure: { (error) in
                 DispatchQueue.main.async {
                     
                 }
             }
         }
         else
         {
            callAddressRecommendationAPI(lat: lat, long: long, resultCount: resultCount) { (result) in
                
                switch result{
                  
                case .success(let json):
                    completion(Result.success(json))
                case .failure(let error):
                    completion(Result.failure(error))
                }
            }
         }
        
    }
    
    func callAddressRecommendationAPI(lat:Double?, long:Double? ,resultCount:Int ,_ completion: @escaping (Result<[Addresses]>) -> ()){
        
        do{
            
            var allParameters : HTTPParameters = [String:Any]()
            allParameters?["lat"] = lat
            allParameters?["long"] = long
            allParameters?["resultcount"] = resultCount
            let request = try HTTPNetworkRequest.configureHTTPRequest(url: Configuration.addressRecommendations, parameters: allParameters, headers: getHeaders(), body: nil, method: .get)
            postSession.dataTask(with: request) { (data, res, err) in
                
                if let response = res as? HTTPURLResponse, let unwrappedData = data {
                    
                    let result = HTTPNetworkResponse.handleNetworkResponse(for: response)
                    switch result {
                    case .success:
                        
                        if let result = try? JSONDecoder().decode(AddressRecommendation.self, from: unwrappedData) {
                            completion(Result.success(result.addresses!))
                        } else {
                            completion(Result.failure(HTTPNetworkError.decodingFailed))
                        }
                        
                    case .failure(let err):
//                        completion(Result.failure(HTTPNetworkError.decodingFailed))
                        completion(.failure(err))
                    }
                }
            }.resume()
        }catch{
            completion(Result.failure(HTTPNetworkError.badRequest))
        }
        
    }
    
    
    func getSearchResult( lat:String?, long:String?,searchString:String,_ completion: @escaping (Result<[SearchAddresses]>) -> ()) {
        
        let isTokenExpiryCheck =  AWSAuth.sharedInstance.checkTokenExpiry(idToken: AWSAuth.sharedInstance.idToken)
         if(isTokenExpiryCheck == true)
         {
             AWSAuth.sharedInstance.fetchCurrentSession { (result) in
                callAddressSearchAPI(lat: lat, long: long, searchString: searchString) { (result) in
                    
                    switch result{
                      
                    case .success(let json):
                        completion(Result.success(json))
                    case .failure(let error):
                        completion(Result.failure(error))
                    }
                }
                 
             } onFailure: { (error) in
                 DispatchQueue.main.async {
                     
                 }
             }
         }
         else
         {
            callAddressSearchAPI(lat: lat, long: long, searchString: searchString) { (result) in
                
                switch result{
                  
                case .success(let json):
                    completion(Result.success(json))
                case .failure(let error):
                    completion(Result.failure(error))
                }
            }
         }
        
    }
    
    func callAddressSearchAPI(lat:String?, long:String?,searchString:String,_ completion: @escaping (Result<[SearchAddresses]>) -> ()){
        
        do{
           
            var allParameters : HTTPParameters = [String:Any]()
            allParameters = parameters
            allParameters?["lat"] = lat
            allParameters?["long"] = long
            allParameters?["searchstring"] = searchString
            let request = try HTTPNetworkRequest.configureHTTPRequest(url: Configuration.searhcAddressEndPoint, parameters: allParameters, headers: getHeaders(), body: nil, method: .get)
            postSession.dataTask(with: request) { (data, res, err) in
                
                if let response = res as? HTTPURLResponse, let unwrappedData = data {
                    
                    let result = HTTPNetworkResponse.handleNetworkResponse(for: response)
                    switch result {
                    case .success:
                        
                        if let result = try? JSONDecoder().decode(BaseSearchAddress.self, from: unwrappedData) {
                            completion(Result.success(result.addresses!))
                        } else {
                            completion(Result.failure(HTTPNetworkError.decodingFailed))
                        }
                        
                    case .failure(let error):
//                        completion(Result.failure(HTTPNetworkError.decodingFailed))
                        completion(.failure(error))
                    }
                }
            }.resume()
        }catch{
            completion(Result.failure(HTTPNetworkError.badRequest))
        }
        
    }
*/
    
}


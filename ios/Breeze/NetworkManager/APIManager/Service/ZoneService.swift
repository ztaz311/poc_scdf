//
//  ZoneService.swift
//  Breeze
//
//  Created by Tuyen, Le Xuan  on 19/05/2022.
//

import Foundation

protocol ZoneServiceProtocol {
    func getZoneCongestionDetail(profileName: String, _ completion: @escaping (Result<[ZoneColor]>) -> ())
    func getZoneAlertDetail(profileName: String, zoneId: String, navigationType: String, carparkIsdestination: Bool, carparkId: String, targetZoneId: String, _ completion: @escaping (Result<ZoneAlert>) -> ())
}

struct ZoneService: ZoneServiceProtocol {
    func getZoneCongestionDetail(profileName: String, _ completion: @escaping (Result<[ZoneColor]>) -> ()) {
        
        let postSession = URLSession(configuration: .default)
        
        var allParameters : HTTPParameters = [String:Any]()
        allParameters?["profile"] = profileName
        
        let resource = Resource<[ZoneColor]>(url: Configuration.zoneCongestionDetail, parameters: allParameters, body: nil, method: .get)
        postSession.load(resource) { (result) in
            switch result {
            case .success(let theResult):
                completion(Result.success(theResult))
            case .failure(let error):
                completion(Result.failure(error))
            }
        }
    }
    
    func getZoneAlertDetail(profileName: String, zoneId: String, navigationType: String, carparkIsdestination: Bool, carparkId: String, targetZoneId: String, _ completion: @escaping (Result<ZoneAlert>) -> ()) {
        let postSession = URLSession(configuration: .default)
        
        var allParameters : HTTPParameters = [String:Any]()
        allParameters?["profile"] = profileName
        allParameters?["navigationtype"] = navigationType
        allParameters?["zoneid"] = zoneId
        allParameters?["destinationCarpark"] = carparkIsdestination
        allParameters?["carparkId"] = carparkId
        allParameters?["targetzoneid"] = targetZoneId
        
        let resource = Resource<ZoneAlert>(url: Configuration.zoneAlertDetail, parameters: allParameters, body: nil, method: .get)
        postSession.load(resource) { (result) in
            switch result {
            case .success(let theResult):
                completion(Result.success(theResult))
            case .failure(let error):
                completion(Result.failure(error))
            }
        }
    }
}

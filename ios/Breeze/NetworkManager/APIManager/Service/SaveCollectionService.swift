//
//  SaveCollectionService.swift
//  Breeze
//
//  Created by Tuan, Pham Hai on 17/11/2022.
//

import Foundation

final class SaveCollectionService {
    func saveCollectionWithToken(_ token: String, _ completion: @escaping (Result<SaveCollectionDataModel>) -> ()) {
        let session = URLSession(configuration: .default)
        
        let resource = Resource<SaveCollectionDataModel>(url: Configuration.saveCollection.appending("/\(token)"), parameters: [:], body: nil, method: .get)
        session.loadV2(resource, completion: completion)
    }
    
    func saveLocationWithToken(_ token: String, _ completion: @escaping (Result<SaveLocationDataModel>) -> ()) {
        let session = URLSession(configuration: .default)
        let resource = Resource<SaveLocationDataModel>(url: Configuration.saveLocation.appending("/\(token)"), parameters: [:], body: nil, method: .get)
        session.loadV2(resource, completion: completion)
    }
}

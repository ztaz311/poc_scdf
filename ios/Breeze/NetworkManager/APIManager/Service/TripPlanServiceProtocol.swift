//
//  TripPlanServiceProtocol.swift
//  Breeze
//
//  Created by VishnuKanth on 30/12/21.
//

import Foundation

protocol TripPlanServiceProtocol {
    func uploadTripPlan(request: TripPlanUploadRequest, completion: @escaping (Result<Bool>) -> ())
    func updateTripPlan(request: TripPlanUploadRequest,tripPlannerId: Int, completion: @escaping (Result<Bool>) -> ())
    func getUpcomingTrips(pageNumber: Int, pageSize: Int, startDate: String, endDate: String, searchKey: String, _ completion: @escaping (Result<UpcomingTripBase>) -> ())
    func deleteUpcomingTrip(tripPlannerId: Int, _ completion: @escaping (Result<Bool>)-> ())
    func getTripPlan(tripId: Int, _ completion: @escaping (Result<PlannTripDetails>) -> ())
}

struct TripPlanUploadRequest : Codable {
    let tripEstStartTime : Int?
    let tripEstArrivalTime : Int?
    let tripStartAddress1 : String?
    let tripStartAddress2 : String?
    let tripStartLat : Double?
    let tripStartLong : Double?
    let tripDestAddress1 : String?
    let tripDestAddress2 : String?
    let tripDestLat : Double?
    let tripDestLong : Double?
    let totalDuration : Int?
    let totalDistance : Int?
    let erpExpense : Double?
    let tripPlanBy : String?
    let erpEntries : [Int]?
    let routeStops : [RouteStops]?
    let routeCoordinates : [[Double]]?
    let tripEtaFavouriteId : Int?

    enum CodingKeys: String, CodingKey {

        case tripEstStartTime = "tripEstStartTime"
        case tripEstArrivalTime = "tripEstArrivalTime"
        case tripStartAddress1 = "tripStartAddress1"
        case tripStartAddress2 = "tripStartAddress2"
        case tripStartLat = "tripStartLat"
        case tripStartLong = "tripStartLong"
        case tripDestAddress1 = "tripDestAddress1"
        case tripDestAddress2 = "tripDestAddress2"
        case tripDestLat = "tripDestLat"
        case tripDestLong = "tripDestLong"
        case totalDuration = "totalDuration"
        case totalDistance = "totalDistance"
        case erpExpense = "erpExpense"
        case tripPlanBy = "tripPlanBy"
        case erpEntries = "erpEntries"
        case routeStops = "routeStops"
        case routeCoordinates = "routeCoordinates"
        case tripEtaFavouriteId = "tripEtaFavouriteId"
    }
}

struct RouteStops : Codable {
    let amenityId : String?
    let coordinates : [Double]?
    let type : String?

    enum CodingKeys: String, CodingKey {

        case amenityId = "amenityId"
        case coordinates = "coordinates"
        case type = "type"
    }
}

//
//  BroadcastMessageService.swift
//  Breeze
//
//  Created by Tuan, Pham Hai on 09/02/2023.
//

import Foundation

enum BroadcastMode: String {
    case planning
    case navigation
}

final class BroadcastMessageService {
        
    /*
     *  carparkId
     *  mode can be "planning" or "navigation"
     */
    func getBroadcastMessage(carparkId: String, mode: BroadcastMode, _ completion: @escaping (Result<BroadcastMessageModel>) -> ()) {
        let session = URLSession(configuration: .default)
            
        var params : HTTPParameters = [String:Any]()
        params?["mode"] = mode.rawValue
        let resource = Resource<BroadcastMessageModel>(url: Configuration.broadcastMessage.replacingOccurrences(of: ":id", with: carparkId), parameters: params, body: nil, method: .get)
        session.load(resource, completion: completion)
    }
    
    func confirmCarparkAvailability(carparkId: String, type: String, completion: @escaping (Result<Bool>) -> ()) {
        
        let postSession = URLSession(configuration: .default)
        do {
            let params = ["availablePercentImageBand": type, "carparkId": carparkId]
            
            let data =  try JSONSerialization.data(withJSONObject: params)
            
            let resource = Resource<DefaultResponse>(url: Configuration.carparkAvailability, parameters: [:], body: data, method: .post)
            
            postSession.load(resource) { (result) in
                switch result {
                case .success(let value):
                    completion(Result.success(value.success))
                case .failure(let error):
                    completion(Result.failure(error))
                }
            }
        } catch {
            print(error)
        }
    }
    
    
    
    func broadcastMessageRoutePlanning(routes: [String: Any], completion: @escaping (Result<BroadCastModel>) -> ()) {
        let postSession = URLSession(configuration: .default)
        do {
            
            let data =  try JSONSerialization.data(withJSONObject: routes)
            
            let resource = Resource<BroadCastModel>(url: Configuration.broadcastTravelZoneFilter, parameters: [:], body: data, method: .post)
            
            postSession.load(resource) { (result) in
                switch result {
                case .success(let value):
                    completion(Result.success(value))
                case .failure(let error):
                    completion(Result.failure(error))
                }
            }
        } catch {
            print(error)
        }
    }
    
}

//
//  TripLogService.swift
//  Breeze
//
//  Created by VishnuKanth on 06/06/21.
//

import Foundation
import Amplify
import AWSMobileClient
import Instabug
import AWSKinesis
import SwiftUI

final class TripLogService:TripLogServiceProtocol{
    
//    func sendTripLog( tripLog: TripLog ,completion: @escaping (Result<Bool>)->Void) {
//        do{
//            let jsonData = try JSONEncoder().encode(tripLog)
//            send(jsonData: jsonData, completion: completion)
//        }
//        catch(let error){
//            print(error.localizedDescription)
//        }
//
//    }
    
    func sendTripSummary( summary: TripSummary ,completion: @escaping (Result<Int>)->Void) {
        do{
            let jsonData = try JSONEncoder().encode(summary)
            let postSession = URLSession(configuration: .default)
            let resource = Resource<TripSummaryResponse>(url: Configuration.tripApi, parameters: parameters, body: jsonData, method: .post)
            postSession.load(resource) { (result) in
                switch result {
                case .success(let tripSummaryResponse):
                    print(tripSummaryResponse)
                    completion(Result.success(tripSummaryResponse.tripId))
                case .failure(let error):
                    print(error)
                    completion(Result.failure(error))
                }
            }
        }
        catch(let error){
            print(error.localizedDescription)
        }
        
    }
    
//    private func getKenisisStreamName() -> String? {
//        return (Bundle.main.infoDictionary?["Fire Hose Stream"] as? String)?
//            .replacingOccurrences(of: "\\", with: "")
//    }
//
//    private func send(jsonData: Data, completion: @escaping (Result<Bool>)->Void) {
//        let firehoseRecorder = AWSFirehoseRecorder.default()
//        var tasks = Array<AWSTask<AnyObject>>()
//
//        tasks.append(firehoseRecorder.saveRecord(jsonData, streamName: getKenisisStreamName())!)
//
//        AWSTask(forCompletionOfAllTasks: tasks).continueOnSuccessWith(block: { (task:AWSTask<AnyObject>) -> AWSTask<AnyObject>? in
//            return firehoseRecorder.submitAllRecords()
//        }).continueWith(block: { (task:AWSTask<AnyObject>) -> Any? in
//            if let error = task.error as NSError? {
//                print("Error: \(error)")
//                completion(.failure(error))
//            } else {
//                completion(.success(true))
//            }
//            return nil
//        })
//    }
    
    func getTrips(pageNumber: Int, pageSize: Int, monthLog: Int, yearLog: Int, _ completion: @escaping (Result<TripBase>) -> ()) {
        //No need to implement this
    }
    
    func getHistoryTrips(pageNumber: Int, pageSize: Int, startDate: String, endDate: String, searchKey: String = "", _ completion: @escaping (Result<TripBase>) -> ()) {
        
        let postSession = URLSession(configuration: .default)
        
        var allParameters : HTTPParameters = [String:Any]()
        allParameters?["page"] = pageNumber
        allParameters?["pagesize"] = pageSize
        allParameters?["startdate"] = startDate
        allParameters?["enddate"] = endDate
        if !searchKey.isEmpty {
            allParameters?["searchkey"] = searchKey
        }
        
        let resource = Resource<TripBase>(url: Configuration.tripApi, parameters: allParameters, body: nil, method: .get)
        postSession.load(resource) { (result) in
            switch result {
            case .success(let theResult):
                completion(Result.success(theResult))
            case .failure(let error):
                completion(Result.failure(error))
            }
        }
    }
    
    func getTripDetails(tripId: Int, completion: @escaping (Result<TripSummary>) -> ()) {
        
        let postSession = URLSession(configuration: .default)
        
        let allParameters : HTTPParameters = [String:Any]()
        let urlString =  Configuration.tripApi + "/\(tripId)" 
        let resource = Resource<TripSummary>(url: urlString, parameters: allParameters, body: nil, method: .get)
        postSession.load(resource) { (result) in
            switch result {
            case .success(let theResult):
                completion(Result.success(theResult))
            case .failure(let error):
                print(error.localizedDescription)
                completion(Result.failure(error))
            }
        }
        
    }
    
    func sendTripToEmail( tripIds: [Int] ,completion: @escaping (Result<Bool>)->Void) {
        let data = [
            "type":"BY_TRIP",
            "tripIds":tripIds
        ] as [String : Any]
        
        do{
            let body =  try JSONSerialization.data(withJSONObject: data)
            
            let postSession = URLSession(configuration: .default)
            let resource = Resource<EmptyData>(url: Configuration.sendTripToEmail, parameters: parameters, body: body, method: .post)
            postSession.load(resource) { (result) in
                switch result {
                case .success(_):
                    completion(Result.success(true))
                case .failure(let error):
                    completion(Result.failure(error))
                }
            }
        }
        catch(let error){
            print(error.localizedDescription)
        }
    }
    
    func sendTripByMonthToEmail( month: Int , year: Int, completion: @escaping (Result<Bool>)->Void) {
        let data = [
            "type":"BY_MONTH",
            "month":month,
            "year":year
        ] as [String : Any]
        
        do{
            let body =  try JSONSerialization.data(withJSONObject: data)
            
            let postSession = URLSession(configuration: .default)
            let resource = Resource<EmptyData>(url: Configuration.sendTripToEmail, parameters: parameters, body: body, method: .post)
            postSession.load(resource) { (result) in
                switch result {
                case .success(_):
                    completion(Result.success(true))
                case .failure(let error):
                    completion(Result.failure(error))
                }
            }
        }
        catch(let error){
            print(error.localizedDescription)
        }
        
    }
    
    func updateTripLog(updatedTrip:TripUpdate, tripId: Int,_ completion: @escaping (Result<Bool>) -> ()) {
        do{
            let jsonData = try JSONEncoder().encode(updatedTrip)
            print(String(data: jsonData, encoding: .utf8))
            
            let postSession = URLSession(configuration: .default)
            //let data =  try! JSONSerialization.data(withJSONObject: httpBody)
            let urlString =  Configuration.tripApi + "/" + String(tripId)
            let resource = Resource<EmptyData>(url: urlString, parameters: parameters, body: jsonData, method: .put)
            
            postSession.load(resource) { (result) in
                switch result {
                case .success(_):
                    completion(Result.success(true))
                case .failure(let error):
                    completion(Result.failure(error))
                }
            }
        }
        catch(let error){
            print(error.localizedDescription)
        }
        
    }
    
    func uploadFile(tripId: Int, base64: String, fileType: String, completion: @escaping (Result<AttachmentFile>) -> ()) {
        
        let data = [
            "tripId":tripId,
            "attachmentFile":base64
        ] as [String : Any]
        
        do{
            let body =  try JSONSerialization.data(withJSONObject: data)
            
            let postSession = URLSession(configuration: .default)
            let urlString =  Configuration.tripApi + "/document/upload"
            let resource = Resource<TripFileUploadResponse>(url: urlString, parameters: parameters, body: body, method: .post)
            
            postSession.load(resource) { (result) in
                switch result {
                case .success(let uploadResponse):
                    completion(Result.success(TripLogService.map(base64: base64, fileType: fileType, response: uploadResponse)))
                case .failure(let error):
                    completion(Result.failure(error))
                }
            }

        }
        catch(let error){
            print(error.localizedDescription)
        }
        

    }
    
    func deleteFile(tripId: Int, completion: @escaping (Result<Bool>) -> ()) {
        let data = [
            "tripId":tripId
        ] as [String : Any]
        
        do{
            let body =  try JSONSerialization.data(withJSONObject: data)

            let postSession = URLSession(configuration: .default)
            let urlString =  Configuration.tripApi + "/document/delete"
            let resource = Resource<EmptyData>(url: urlString, parameters: parameters, body: body, method: .post)
            
            postSession.load(resource) { (result) in
                switch result {
                case .success(_):
                    completion(Result.success(true))
                case .failure(let error):
                    completion(Result.failure(error))
                }
            }
        }
        catch(let error){
            print(error.localizedDescription)
        }
        
    }
    
    func retrieveFile(tripId: Int, filePath: String, completion: @escaping (Result<AttachmentFile>) -> ()) {
        let postSession = URLSession(configuration: .default)
        let urlString =  Configuration.tripApi + "/document/\(tripId)"
        let resource = Resource<TripFileRetrieveResponse>(url: urlString, parameters: parameters, body: nil, method: .get)
        
        postSession.load(resource) { (result) in
            switch result {
            case .success(let retrieveResponse):
                completion(Result.success(TripLogService.map(filePath: filePath, fileType: "image/jpeg", response: retrieveResponse)))
            case .failure(let error):
                completion(Result.failure(error))
            }
        }
    }

    func getDataTransactions(vehicleNumber: String, pageNumber: Int, pageSize: Int, startDate: String, endDate: String, filter: String = "", completion: @escaping (Result<TripTransactions>) -> ()) {
        
        let postSession = URLSession(configuration: .default)
        var allParameters : HTTPParameters = [String: Any]()
        allParameters?["vehicleNumber"] = vehicleNumber
        allParameters?["page"] = pageNumber
        allParameters?["pagesize"] = pageSize
        allParameters?["startdate"] = startDate
        allParameters?["enddate"] = endDate
        if !filter.isEmpty {
            allParameters?["filter"] = filter
        }
        
        let resource = Resource<TripTransactions>(url: Configuration.getTriplogTransactions, parameters: allParameters, body: nil, method: .get)
        postSession.load(resource) { (result) in
            switch result {
            case .success(let theResult):
                completion(Result.success(theResult))
            case .failure(let error):
                print(error.localizedDescription)
                completion(Result.failure(error))
            }
        }
    }
    
    func filteredOBU(completion: @escaping (Result<ObuVehicleResponseModel>) -> ()) {
        
        do {
            let postSession = URLSession(configuration: .default)
            
            let pairedObuList = Prefs.shared.getObuList(AWSAuth.sharedInstance.cognitoId, key: .pairedObuList)
            let obuNames: [String] = pairedObuList.map { obu in
                return obu.name ?? ""
            }
            
            let params: [String: Any] = ["vehicleNumbers": obuNames as Any]
            let jsonData = try JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
            
            let resource = Resource<ObuVehicleResponseModel>(url: Configuration.filteredOBU, parameters: parameters, body: jsonData, method: .post)
            postSession.load(resource) { (result) in
                switch result {
                case .success(let theResult):
                    completion(Result.success(theResult))
                case .failure(let error):
                    print(error.localizedDescription)
                    completion(Result.failure(error))
                }
            }
        }
        catch(let error){
            print(error.localizedDescription)
        }
    }

}


extension Encodable {
  func asDictionary() throws -> [String: Any] {
    let data = try JSONEncoder().encode(self)
    guard let dictionary = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any] else {
      throw NSError()
    }
    return dictionary
  }
}

//
//  RestrictedNameService.swift
//  Breeze
//
//  Created by Tuan, Pham Hai on 30/03/2023.
//

import Foundation

final class RestrictedNameService {
    func getRestrictedNames(_ completion: @escaping (Result<RestrictedNamesModel>) -> ()) {
        let session = URLSession(configuration: .default)
        let resource = Resource<RestrictedNamesModel>(url: Configuration.getRestrictedNames, parameters: [:], body: nil, method: .get)
        session.loadWithoutHeader(resource, completion: completion)
    }
}

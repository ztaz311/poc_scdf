//
//  AppVersionService.swift
//  Breeze
//
//  Created by VishnuKanth on 17/12/21.
//

import Foundation
import UIKit

final class AppUpdateService {
    let postSession = URLSession(configuration: .default)
    
    func post(_ completion: @escaping (Result<Bool>) -> ()) {
        
        let httpBody  = ["deviceos":"iOS","appversion":UIApplication.buildString()]
        
        do {
            
            let jsonData = try JSONEncoder().encode(httpBody)            
            let postSession = URLSession(configuration: .default)
            let resource = Resource<EmptyData>(url: Configuration.appVersionService, parameters: [:], body: jsonData, method: .post)
            postSession.load(resource) { (result) in
                switch result {
                case .success(_):
                    completion(Result.success(true))
                case .failure(let error):
                    completion(Result.failure(error))
                }
            }
        }
        catch(let error){
            print(error.localizedDescription)
        }
        
    }
}

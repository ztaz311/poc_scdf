//
//  SchoolZoneService.swift
//  Breeze
//
//  Created by Zhou Hao on 28/5/21.
//

import Foundation
import Turf

struct SchoolZoneService: SchoolZoneServiceProtocol {
    
    let postSession = URLSession(configuration: .default)
    
    func getSchoolZone(_ completion: @escaping (Result<[SchoolZoneData]>) -> ()) {
        
        let url = "\(Configuration.dataset)/schoolzone.json"
        let resource = Resource<FeatureCollection>(url: url, parameters:parameters, body: nil, method: .get)
        postSession.load(resource) { result in
            switch result {
            case .success(let featureCollection):
                completion(Result.success(SchoolZoneService.map(featureCollection: featureCollection)))
            case .failure(let error):
                completion(Result.failure(error))
            }
        }
    }
}

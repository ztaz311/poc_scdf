//
//  FeedbackServiceProtocol.swift
//  Breeze
//
//  Created by Zhou Hao on 21/5/21.
//

import Foundation

protocol FeedbackServiceProtocol {
    func submit(userId: String, description: String, fromLog: Bool, url: URL, progress: @escaping (Float) -> Void, completion: @escaping (Result<Bool>)->Void, compression: Bool)
}

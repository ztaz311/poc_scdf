//
//  MockTripLogService.swift
//  Breeze
//
//  Created by Malou Mendoza on 10/6/21.
//

import Foundation

struct MockTripLogService: TripLogServiceProtocol {
    func sendTripLogDetails(tripLog: [String : Any], completion: @escaping (Result<Bool>) -> Void) {
        
        // Not requried. We declared this for just for mock service
    }
    
    func sendTripSummary( summary: TripSummary ,completion: @escaping (Result<Int>)->Void) {
        
        // Not requried. We declared this for just for mock service
    }
    
    func sendTripLog(tripLog: TripLog, completion: @escaping (Result<Bool>) -> Void) {
        
        // Not requried. We declared this for just for mock service
    }
    
    func updateTripLog(updatedTrip:TripUpdate, tripId: Int,_ completion: @escaping (Result<Bool>) -> ()) {
        DispatchQueue.global().asyncAfter(deadline: .now() + 0.5) {
            completion(Result.success(true))
        }
    }
    
    //func getTrips(_ completion: @escaping (Result<[Trip]>) -> ()) {
    
    
    func getTrips(pageNumber: Int, pageSize: Int,  monthLog: Int, yearLog: Int,  _ completion: @escaping (Result<TripBase>) -> ()){
        
        var trips = [Trip]()
        
//        let trip1 = Trip(tripId: 1, tripGUID: "56789-12345", tripStartTime: "2021-06-09T09:24:00.000Z", tripEndTime: "2021-06-09T09:23:46.000Z", tripStartAddress1: "1-Garden Shopping Mall", tripDestAddress1: "Suntec City Tower 1", totalDistance: 0.2, totalExpense: 11)
//        let trip2 = Trip(tripId: 2, tripGUID: "12345-12345", tripStartTime: "2021-06-07T23:46:21.000Z", tripEndTime: "2021-06-07T23:48:36.000Z", tripStartAddress1: "2-Great World", tripDestAddress1: "Garden Shopping Mall", totalDistance: 15, totalExpense: 9)
//        let trip3 = Trip(tripId: 3, tripGUID: "56789-12345", tripStartTime: "2021-06-09T09:24:00.000Z", tripEndTime: "2021-06-09T09:23:46.000Z", tripStartAddress1: "3-Somerset", tripDestAddress1: "Suntec", totalDistance: 20.6, totalExpense: 10)
//        let trip4 = Trip(tripId: 4, tripGUID: "56789-12345", tripStartTime: "2021-06-09T09:24:00.000Z", tripEndTime: "2021-06-09T09:23:46.000Z", tripStartAddress1: "4-Garden Shopping Mall", tripDestAddress1: "Suntec City Tower 1", totalDistance: 0.2, totalExpense: 11)
//        let trip5 = Trip(tripId: 5, tripGUID: "12345-12345", tripStartTime: "2021-06-07T23:46:21.000Z", tripEndTime: "2021-06-07T23:48:36.000Z", tripStartAddress1: "5-Great World", tripDestAddress1: "Garden Shopping Mall", totalDistance: 15, totalExpense: 9)
//        let trip6 = Trip(tripId: 6, tripGUID: "56789-12345", tripStartTime: "2021-06-09T09:24:00.000Z", tripEndTime: "2021-06-09T09:23:46.000Z", tripStartAddress1: "6-Somerset", tripDestAddress1: "Suntec", totalDistance: 20.6, totalExpense: 10)
//        let trip7 = Trip(tripId: 7, tripGUID: "56789-12345", tripStartTime: "2021-06-09T09:24:00.000Z", tripEndTime: "2021-06-09T09:23:46.000Z", tripStartAddress1: "7-Garden Shopping Mall", tripDestAddress1: "Suntec City Tower 1", totalDistance: 0.2, totalExpense: 11)
//        let trip8 = Trip(tripId: 8, tripGUID: "12345-12345", tripStartTime: "2021-06-07T23:46:21.000Z", tripEndTime: "2021-06-07T23:48:36.000Z", tripStartAddress1: "8-Great World", tripDestAddress1: "Garden Shopping Mall", totalDistance: 15, totalExpense: 9)
//        let trip9 = Trip(tripId: 9, tripGUID: "56789-12345", tripStartTime: "2021-06-09T09:24:00.000Z", tripEndTime: "2021-06-09T09:23:46.000Z", tripStartAddress1: "9-Somerset", tripDestAddress1: "Suntec", totalDistance: 20.6, totalExpense: 10)
//        let trip10 = Trip(tripId: 10, tripGUID: "56789-12345", tripStartTime: "2021-06-09T09:24:00.000Z", tripEndTime: "2021-06-09T09:23:46.000Z", tripStartAddress1: "10-Somerset", tripDestAddress1: "Suntec", totalDistance: 20.6, totalExpense: 10)
//
//
//
//        trips.append(trip1)
//        trips.append(trip2)
//        trips.append(trip3)
//        trips.append(trip4)
//        trips.append(trip5)
//        if (pageNumber < 4){
//        trips.append(trip6)
//        trips.append(trip7)
//        trips.append(trip8)
//        trips.append(trip9)
//        trips.append(trip10)
//        }
//
//
//        var tripBase = TripBase.init(currentPage: pageNumber, totalItems: 35, totalPages: 4, trips: trips)
//
//
//        DispatchQueue.global().asyncAfter(deadline: .now()) {
//            completion(Result.success(tripBase))
//        }
    }
    
    func getTripDetails(tripId: Int, completion: @escaping (Result<TripSummary>) -> ()) {
//        let summary = TripSummary(id: "1234", startTime: Int(Date().timeIntervalSince1970 - 5000), endTime: Int(Date().timeIntervalSince1970), tripType: "Navigation", startLatitude: 1.2345, startLongitude: 103.0123, startAddress1: "Testing Address", startAddress2: "ABC Street Blk 120", endLatitude: 1.312, endLongitude: 102.5678, endAddress1: "Testing end address", endAddress2: "ABCD Street Blk 510", totalCost: 3.5, totalDistance: 8.1,  costBreakDowns: [
//            ERPCost(erpId: "1001", name: "ERP Adress", cost: 1.2, exitTime: Int(Date().timeIntervalSince1970), isOBU: false),
//            ERPCost(erpId: "1002", name: "ERP Adress 2", cost: 2.3, exitTime: Int(Date().timeIntervalSince1970), isOBU: false),
//        ])
//        let summary = TripSummary(id: "1234", startTime: Int(Date().timeIntervalSince1970 - 5000), endTime: Int(Date().timeIntervalSince1970),  startLatitude: 1.2345, startLongitude: 103.0123, startAddress1: "Testing Address", startAddress2: "ABC Street Blk 120", plannedDestAddress1: "", plannedDestAddress2: "", endLatitude: 1.312, endLongitude: 102.5678, plannedDestLat: 0, plannedDestLong: 0, endAddress1: "Testing end address", endAddress2: "ABCD Street Blk 510", totalCost: 3.5, totalDistance: 8.1,parkingExpense: 0, costBreakDowns: [
//            ERPCost(erpId: 1001,name: "ERP Adress", cost: 1.2, exitTime: Int(Date().timeIntervalSince1970), isOBU: "0"),
//            ERPCost(erpId: 1002,name: "ERP Adress 2", cost: 2.3, exitTime: Int(Date().timeIntervalSince1970), isOBU: "0"),
//        ], navigationEndType: TripSummary.NavigationEndType.natural)
//
//        DispatchQueue.global().asyncAfter(deadline: .now() + 0.5) {
//            completion(Result.success(summary))
//        }
    }
    
    func uploadFile(tripId: Int, base64: String, fileType: String, completion: @escaping (Result<AttachmentFile>) -> ()) {
        let response = TripFileUploadResponse(tripId: tripId, fileNamePath: "parkingReceipt")
        DispatchQueue.global().asyncAfter(deadline: .now() + 0.5) {
            completion(Result.success(MockTripLogService.map(base64: base64, fileType: fileType, response: response)))
        }
    }
    
    func deleteFile(tripId: Int, completion: @escaping (Result<Bool>) -> ()) {
        DispatchQueue.global().asyncAfter(deadline: .now() + 0.5) {
            completion(Result.success(true))
        }
    }
    
    func retrieveFile(tripId: Int, filePath: String, completion: @escaping (Result<AttachmentFile>) -> ()) {
        let response = TripFileRetrieveResponse(tripId: tripId, file: "iVBORw0KGgoAAAANSUhEUgAAAJIAAACSCAMAAACZpWO8AAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAACKUExURUdwTOpnCvWMQvSIOu5zHvSIPOxuFfN2IPF8KfB3If///+ljA/aMQexrD+plBvKBMfB6JetoCvF9K/aPRe1vFfWJPO5zG+92IPOFN/aRSvXOqRQXJR0fMCsrO/317/rn2frawvatefe9kvSfYVRDQYaHkMzLzsdrLGVmcqipr5dUK5F5a9W0lb6bgOBI+QsAAAAKdFJOUwCu3cF/otwaXTl1BE6QAAAOXklEQVR42sSbiWKjuBJFM15iG4PZVxuBAK/p/v/feyUJQQkj7MROv3InPdPDtA63rkprPj5eitVmvVgu5/PZLDRns/lyuVisNx//nwCW5XJm+qbvm6YZmmEbJvsnYAOy1T/lWTMaDsNxAKNjSsKEBfw+X2z+HQ6jETwISgJ1VCz+BdZ67vtSnyEPB0qUsJNk+atQm6Wfpr4IIVNoqmkbcNnsY88Wv2Wr9cxPW4l6lcJepaFGdksEkfwKVAeEoUIOhQRSqBCTbS9XvwCEUuYjH3XOTvQ8EJ79VlNtZqnfA/m9s7lGZisQxxpQKVDe25RaLdM+ZVwh/16j1kmhViP++Vy/J2cpJ0qHGpnYRwMX3WskmGz7v9eFWs05UfotjTjMUCMRtr14g0Soow1qpCxIDzXqMsdivnrRRc9rlKgaJSM0PJwXHAUdTSG60yj0a1plZVEQQgz4Kooyq2iaKFkboXKWPyRaDICG9ajOCmM8iqpWjK1oxD7O549qFE9a6uP+32tUVyUxpoKUNNVmDj4/YZqPaSSo0qownomShqMagUqO521+RpTe+ch/lqejUqEkEXytv1+NBkkTGtHvAHFf1SMaifgO02omJcJ5A6qKGN+PgqqZczqqxTezdu9s+hOgXqmBRjF81t8h8lUiYKpL4+eRJQONYvFt883enyKN/Mp4KQgFIKQREwl+fa6erpBDjdLCeDVK28M+4omLvWeY1rIeYY1+7CLFUWlHFMeCCH5/zLThCqVqX6uMtwShSt4YURxHy2e6fz9jEwWpNN4VVYsjedj3aP3Y2r6qUfo+Iuh5SCNBFUfx5uGkViF6h7EVk3sKDyOa7HabNFVmkUyj9xL1TDFi+k+PNGvH/vS3NOJMTu8j4OGf9YOKhKfa7ydifopx3qIo0qZudV+zS+M3Iut4IvbFYqHtbb6yGvEz43eiwhoJqM1k2X53hRyJuvORjM9xb6samTX5NSQSIm+LWI97W9EoLYzfi0LliYIxmWbDVW1p/GZUsre1RCMyrYerWvpak6frEeJ00j6Q9j7iTMHnhJO4Rv5LRiKXA4s9RHO86lKHVQqCYD0USZ3Vmq/1/6MEYpGPP0MRD2cayDTvsiZ2R+rXOlSeH3qoi+YhT2GCX0ptWg9WR+ZLve0oiKRKV10RVzSCUEbf5WC3dsLbhPn2OuW0a55jpkbvcOQk/muFRzdVowmRrvlhD20djnqoiyTiTAd9p8sQDY8Fzpu60B530vFyMi7dy+dkCik/5A2Pr2yivoW9k3ggg88GO0ilziGX7uX3++O0Sn+zNiaQsp5HfO8MvhnsjoyKVEiHdLY96d0NT2ZPIBEbuxsbfDnYQRr9S065YhF95+ZIhx5pqsJVPU0QWKDVSN74jhbRIimd+zDV45oeaaJ3FgHWyAqstoKvBvt+4xXgXqW9bnhjT/55JnFQBxSRLGup9LfuRKs0XlOpuFy+/jwlEhhcamTxL/cTW6nbGk21g0Rr7z+P2yqz50QyCM4aqGSteiv1p3666S0jugDU1xOvT8rniGDK22eNM607K6GjmmKi3FxOzeWZntTp9HCszJBGTKX/pJVMdMY2VQFPOCUjKhVXmLWdjKyua1pV2RMT06LXiMdnayV0DKEfcaHcQCUqepOQkYkb9/3X+ezxTaTIewxl9xpxqhWfKpnoODvMJgZ4loYJkRrRGw8wMu8btqCNIudh5ureRzzYmDJTz2q0f8ex4SMIyXQuucr64ATBudmfGVJQP2EmhiOJmL9XpuKkUNuPTvK/FKNZ4yIxpvwcWO7OaRq+uA7IYzNZSCUXJigbLJEZplqiY9McjwVNPLuu0+reJKQlguJnudvgvOcqBY/cRHDWRJdbKxppJ91HMXNrmtuNGze46wYnXtlzWHYwlQJnL1QqH/sbM7H6vfCRRmGoKZTX1ibn2y3PhUvKkVXJPgdXcyRHqvRwYyFFGrmW60INUN2t6XBtZ2I2iW+HUeOy6V3ueQLJ6bzkPFp+UeRtQIIqsPRNdAMh1Iy5widnaG1nOSATtDV0Xc6JPJZUy4mbfSRU8h4xVUgjEMldfczbaxHtaW2hyRublJxjeJ2d6zQ31phDBrPOQ85OKJlKzjnPRV2KAOkBU9mp5PLEbTgSuu5T6FevZ54TMK5AitSpHixdbonNE+fdDg3fkGRPwR/E1QMkpBFDmrV3R1qViM5KBxgkmEogwV5sMNoK0zE/JCFXCYhu7YACAf8UR1MbQyU2N1CtefFGV6LGkSApZ5ETkGB/4+/veB5VRuUmZCpFaZOfPYkUxB4vGZb+cLEQOK7QiCH5yj2ERDOjPJwThsSJzqIxD/6A4unUGZBsD6bdnt0hRT5XCdS1dWfChUhaq5G7W3+IwbYLzYjLFXBix2vyuHUJ90235wPzYJ89c8tv7FpQp1JZCZVq7QyLyILUxuID3x1LNCpd8htHOp9BAtmY6F2yO13zL1qbtp1fxFUlqRIxypAh6Sd0BdYIVFqAl9ANxDAZTXmTJ1yBY37jArDGUtqWRXHsc+Hz3/C2/5tglRxxijqNhDViiZuhK0jwVYwudBsTkJy8OffvXxuUIwUBv1kh5r8039eKSqk8BNcjlVIjwbXlSPjGXzHq7nMNSE1etgKwxip+8gAqUT7pP/I2/+wPtaJSN+oU2qVBiXqbu3N3UCqxRsko0pUlpT7vm6p/fz6cEgoqiSlmwZdSX/tjoqjUF0l2eWd8DodKwM5FSEKjMBl7kytrrWr2xwqpVLZ3cLx2tXJiIuVHI1FUemJBUHU5YxoxpKVy33d0csKRsv3+StH7l3h5xP6FFCc28UwVlZ7YhaUoa/C1Wwmk/rbv2AKFT7f/7hujRir1GS4KnJJC7XGPNz1T1NuACJDW6p3o8b0lIGouRCC1jemXHFilx9Mlw2uNtBNEMKvc9D5ikU7OSZFKuqZUlYKH00piKRrtYO69Uu6NJ8nUiiJB769JCVHrEq9b0ysU5Gz4bNkKvLuDKLAm+kiBVdKkhCZDlSxrkilTNNrt2K7uTNytbSOc0LnCKo2nhBOxp8rMlipNM9VII4gt2zpZJsq9SP3/niWKSiNNEcr+HvYQZaaSKlnuhMcdRaPtji3AF+q90VCbNl+0lpby/QdMJEvDUKhUC1u1T1mWM2mlXiNg4lvMg5v1GjNlnMj2YMgpUvn+boWBTOEA25Z9pJIq6WXKsEZAJLaZB/fGR81UUt4cIJVibGuRuiV/mfKfRxEqFWivnSPtHDJhJeSknTizmKMLtsDkj6hLu9Yq5f1dV1T7Eoh8U6qU4Y1todKOaqsSG2qlRrutOB9Y4B/RGMsc7ZqzKd7QY20xmWhoO6nfqZSpu8gCSbODUrnYR/BpzyuUn9Cwbap6tqo7BWx0Tl8JJPb6MEVybPnM4Ci/aJE0MvmuotFWnliot7RtZeFUSQVYc8rKJwzk62fwTCRUur9WWLdIFhnvb7veR9vtVh5/zbFG8A0rX0JznkAaNFcKpC01CKgUxZp7jkWLtKXavHUabbfyqGmtZk4ZegkgBYAU0vJ+F0a+fg3PWKnmrmzdIlkj5o4VH223/YFce5O9C9w6heaC0eZKoRK8fgYqWaF+NcuRtmREJOwjlDc2pqDMyeIrGwYkt9RsVvG2QoMwlbS707V4zBGb4fjlPMVHEP3h7mb4UyMlPsbTIpVCJbOmSRS4VL/oh8cstjkQ8Z2XvnKrPur7Gz9GUZmUsZdqkdgcdRc47cabqx+v48AT+zwB21kl/YirEm3xfYqF+gMaHnZTpkciaWwn9kMkozb5+t1hKgVZL9KAaKfcOVN+RgMCdToCSLpJfeWHHdLEpIhyJHjORk4ikeojbG4hk/ITGp5X4TFBK0AmXt+jdWRNrEUq/tjgRn090Aibm8uENeK3/Qma32qdW4JKomJNnioB+d2PHJSyAHREw9uVS0Uj+HrqIgyh1TM3r8j9BgWxhxpthxcZV4pGnpq6X4j/9XJGbcqCQBRenghxXSu+j3TXC9r0///HVUibGaBSMS66Ps9hHBPOOx31qPAjqLvxaZsQjS2TZ64lQY8KP+2ZQ2rM/py3SwxeNPWoCOV094Rkwwe2adc3raOQSbY3QY+Gj6N220KCisKJWIk8ssxIu5EiRT0SMf4EeuRUNRsoug56sEdFLBifE4+GlV5T69dREc+gc+LRwIw1WygiHhUPkvo74tGw2tS75nlUPOIZsqPPjdUJNZku5NFjAEz6hNap7pIp+gp5xJ4RTSG27idNH78cvf8jTwppKidKaPV/vVK879oytGvFcx4tE6S+HQ2xGrQyPyro0SskYQYssviRI7TO64xqqsNiRUPHpBSbo1hWsASX//SL7cXSnjSNFNsN9RsJjaUtylzpV+1cRX0r8Ci22zou6OWmrdUhXEdiBjMv0ZadADVynMnJmrZSMY/ELIpfAkQT0kdVVf+bIcpcNT5lQ7s2c65AfiIUm4sruMx415iXDPrCp7VL6whrGoFIhPtoXZ2uT1Rdmk7fbthUIkVDz4RUHcZYbGb0GCH4zW/b1eRmxOvZyyZUZGzaOLhrE4FgM8j12Q1eMJ/GjlzovnWJbo/D/WjxKAg+elR7evC6R9nU/aYWnWivKmxUUCLi0SSqvEUQ9Rj4cYrKRx6tHCuyrwmhhT3CMU2Q04g/a2L16JzeqGAd4Uw09KikdYT2LcnYIx7zqBzzvhoE/lBlex6JRNOF8h2ixvDGlTCnqahLxCP+kWxJUVGyBudYA3Xke8TSToXigpA16Hm7KyqjdcTSjxnrnYJkzUyP2DYTxiSj1Q0SyEAVerPZ02O+3SS9nAtURzrkEXnTHpj82HblrPI7UokySLCOBH/HqMFM7oXWsWff3vk5PYrxN05lzCQTJdKk8Lv2vXLAfEjOrDJYR0qwdw+GDEjL8l4c51LmSaT8AaHm14rcZeKxAAAAAElFTkSuQmCC") // sample image base64
        DispatchQueue.global().asyncAfter(deadline: .now() + 0.5) {
            // TODO: No file type return?
            completion(Result.success(MockTripLogService.map(filePath: filePath, fileType: "", response: response)))
        }
    }

}

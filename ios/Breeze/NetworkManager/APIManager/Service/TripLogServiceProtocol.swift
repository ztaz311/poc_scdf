//
//  TripLogServiceProtocol.swift
//  Breeze
//
//  Created by VishnuKanth on 06/06/21.
//

import Foundation

protocol TripLogServiceProtocol {    
    func sendTripSummary( summary: TripSummary ,completion: @escaping (Result<Int>)->Void)
//    func sendTripLog( tripLog: TripLog ,completion: @escaping (Result<Bool>)->Void)
    
    //for testing
    func getTrips(pageNumber: Int, pageSize: Int, monthLog: Int, yearLog: Int,  _ completion: @escaping (Result<TripBase>) -> ())
    func getTripDetails(tripId: Int, completion: @escaping (Result<TripSummary>) -> ())
    func updateTripLog(updatedTrip:TripUpdate, tripId: Int,_ completion: @escaping (Result<Bool>) -> ())
    
    // For file
    func uploadFile(tripId: Int, base64: String, fileType: String, completion: @escaping (Result<AttachmentFile>) -> ())
    func deleteFile(tripId: Int, completion: @escaping (Result<Bool>) -> ())
    func retrieveFile(tripId: Int, filePath: String, completion: @escaping (Result<AttachmentFile>) -> ())
}

extension TripLogServiceProtocol {
    static func map(base64: String, fileType: String, response: TripFileUploadResponse) -> AttachmentFile {
        return AttachmentFile(tripId: response.tripId ?? 0, attachmentFile: base64, fileType: fileType, filePath: response.fileNamePath ?? "")
    }
    
    static func map(filePath: String, fileType: String, response: TripFileRetrieveResponse) -> AttachmentFile {
        return AttachmentFile(tripId: response.tripId ?? 0, attachmentFile: response.file ?? "", fileType: fileType, filePath: filePath)
    }
}

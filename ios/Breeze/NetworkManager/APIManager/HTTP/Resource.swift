//
//  Resource.swift
//  Breeze
//
//  Created by Zhou Hao on 9/2/21.
//

import Foundation

struct Resource<A> {
//    let parse: (Data) -> A?
    let url: String
    let parameters: HTTPParameters
    let body: Data?
    let method: HTTPMethod
}

extension Resource where A: Decodable {
    init(url: String, parameters: HTTPParameters, body: Data?, method: HTTPMethod) {
        self.url = url
        self.parameters = parameters
        self.body = body
        self.method = method
    }
}

struct DefaultResponse: Codable {
    var message: String?
    var success: Bool
    
    enum CodingKeys: String, CodingKey {
        case message
        case success
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.message = try container.decodeIfPresent(String.self, forKey: .message)
        self.success = try container.decodeIfPresent(Bool.self, forKey: .success) ?? false
    }
}


//
//  HTTPNewtorkResponse.swift
//  Breeze
//
//  Created by VishnuKanth on 18/12/20.

import Foundation
import UIKit

struct HTTPNetworkResponse {
    
    // Properly checks and handles the status code of the response
    static func handleNetworkResponse(for response: HTTPURLResponse?, forceSignOut: Bool = false) -> Result<String>{
        
        if forceSignOut {
            appDelegate().forceSignOut = false
            showError(message: HTTPNetworkError.authenticationError.rawValue)
            return .failure(HTTPNetworkError.authenticationError)
        }
        
        guard let res = response else { return Result.failure(HTTPNetworkError.UnwrappingError)}
        
        switch res.statusCode {
        case 200...299: return .success(HTTPNetworkError.success.rawValue)
        case 401:
            showError(message: HTTPNetworkError.authenticationError.rawValue)
            return .failure(HTTPNetworkError.authenticationError)
        case 400...499:
//            showError(message: HTTPNetworkError.badRequest.rawValue)
            return .failure(HTTPNetworkError.badRequest)
        case 500...599:
            showError(message: HTTPNetworkError.serverSideError.rawValue)
            return .failure(HTTPNetworkError.serverSideError)
        default:
            showError(message: HTTPNetworkError.failed.rawValue)
            return .failure(HTTPNetworkError.failed)
        }
    }
    
    static func showError(message: String) {
        
        #if STAGING || TESTING
        DispatchQueue.main.async {
            showToast(from: UIApplication.shared.keyWindow!, message: message, topOffset: 0, icon: "alertIcon")
        }
        #endif
    }
    
}

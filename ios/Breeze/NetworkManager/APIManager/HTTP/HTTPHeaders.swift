//
//  HTTPHeaders.swift
//  Breeze
//
//  Created by VishnuKanth on 19/12/20.
//

import Foundation

func getHeaders(complete: @escaping (HTTPHeaders?) -> Void) {
    if AWSAuth.sharedInstance.isCurrentTokenExpired() {
        AWSAuth.sharedInstance.fetchCurrentSession { (_) in
            complete(createHeader())
        } onFailure: { (failure) in
            complete(nil)
        }
    } else {
        complete(createHeader())
    }
}

func createHeader() -> HTTPHeaders {
    return [
        "Content-Type": "application/json",
        "Accept": "application/json",
        "Authorization": "Bearer \(AWSAuth.sharedInstance.idToken)",
        "x-breeze-platform": "ios"
    ]
}

//public let headers: HTTPHeaders = [
//    "Content-Type": "application/json",
//    "Accept": "application/json",
//    "Authorization": "Bearer \(AWSAuth.sharedInstance.idToken)"
//]

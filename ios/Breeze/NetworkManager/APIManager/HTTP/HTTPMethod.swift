//
//  HTTPMethod.swift
//  Breeze
//
//  Created by VishnuKanth on 18/12/20.

import Foundation

public enum HTTPMethod: String{
    
    case get = "GET"
    case post = "POST"
    case put = "PUT"
    case patch = "PATCH"
    case delete = "DELETE"
}

//
//  HTTPNetworkRoutes.swift
//  Breeze
//
//  Created by VishnuKanth on 18/12/20.

import Foundation

struct Configuration {

    /*
    static let isQA = true
    static let qaURL = "https://qa.bebeep.com.sg/v1"
    static let devURL = "https://dev.breeze.com.sg/api/v1"
    static let prodURL = "<your Prod URL>"*/
    
    static let baseURL = appDelegate().backendURL
    static let policyBaseURL = appDelegate().policyURL
    
    static let addressRecommendations = "\(baseURL)/address/addressrecommendations"
    static let searhcAddressEndPoint = "\(baseURL)/address/searchaddresses"
    static let userData = "\(baseURL)/user/userdata"
    static let userBookmark = "\(baseURL)/user/v3/bookmarks"
    static let userPrefv4 = "\(baseURL)/user/userprefv4"
    static let userBookmarkDeselectingAmenity = "\(baseURL)/user/v3/bookmarks/amenity"
    static let userBookmarkDeselectingAddress = "\(baseURL)/user/v3/bookmarks/address"
    static let easyBreezyAddresses = "\(baseURL)/address/easybreezies"
    static let selectAddress = "\(baseURL)/address/selectaddress"
    static let erpDetails = "\(baseURL)/maintenance/erprates"
    static let dataset = "\(baseURL)/dataset"
    static let carParkDetails = "\(baseURL)/carpark/getcarparksv2"
    static let userSettings = "\(baseURL)/user/usersetting"
    static let masterlist = "\(baseURL)/maintenance/masterlist"
    static let tripFeedback = "\(baseURL)/feedback/triprating"
    static let uploadTripHistory = "\(baseURL)/trip"
    static let registerDevice = "\(baseURL)/user/registerdevice"
    static let tripApi = "\(baseURL)/trip"
    static let walkathonWalkingTripApi = "\(baseURL)/trip/walking"
    static let upcomingTripApi = "\(baseURL)/trip/planner"
    static let tripETA = "\(baseURL)/trip/eta"
    static let liveLocationStatus = "\(baseURL)/trip/eta/status"
    static let sendTripToEmail = "\(baseURL)/trip/sendemail"
    static let newSearchAddress = "\(baseURL)/address"
    static let searchDetails = "\(baseURL)/address/details"
    static let etaFavList = "\(baseURL)/trip/eta/favourite"
    static let tripFeedbackIssue = "\(baseURL)/feedback/reportissue"
    static let amenities = "\(baseURL)/amenities"
    static let amenitiesV2 = "\(baseURL)/amenitiesv2"
    static let appVersionService = "\(baseURL)/user/appversion"
    static let uploadTripPlan = "\(baseURL)/trip/planner"
    static let getNotifications = "\(baseURL)/user/messagesv3"
    static let getRecentContacts = "\(baseURL)/trip/eta/recent"
    static let zoneCongestionDetail = "\(baseURL)/carpark/zonecongestiondetails"
    static let zoneAlertDetail = "\(baseURL)/carpark/zonealertdetails"
    static let contentDetails = "\(baseURL)/maintenance/contentlayer/details"
    static let contents = "\(baseURL)/maintenance/contentlayer"
    static let contentsV2 = "\(baseURL)/maintenance/contentlayer/v2"
    static let analytics = "\(baseURL)/user/analytics"
    static let userPref = "\(baseURL)/user/userpref"
    static let saveCollection = "\(baseURL)/user/v3/collection/token"
    static let saveLocation = "\(baseURL)/user/location/token"
    
    static let userTNC = "\(baseURL)/user/tnc"
    
    static let termsOfUseURL = "\(policyBaseURL)/terms-of-use.html"
    static let aboutUsURL = "\(policyBaseURL)/aboutus.html"
    static let privacyPolicyURL = "\(policyBaseURL)/privacy.html"
    static let faqURL = "https://breeze.com.sg/support?source=app"
    static let broadcastMessage = "\(baseURL)/amenities/:id/broadcast"
    static let carparkAvailability = "\(baseURL)/carpark/availability"
    static let getCarparkDetail = "\(baseURL)/carpark"
    static let checkParkingAvailabilityAlert = "\(baseURL)/destination/alternatives"
    static let getGuestUserId = "\(baseURL)/user/guests/generate"
    static let getRestrictedNames = "\(baseURL)/user/name/blacklists"
    static let broadcastTravelZoneFilter = "\(baseURL)/user/v2/broadcast/travelzone/filter"
//    static let faqURL = "https://www.breezebeta.com/faq"
    
    //DEMO
    static let carParkAvailability = "\(baseURL)/carpark/availability/J0100"
    
    //  OBU service
//    static let saveTripObuErp = "\(baseURL)/trip/obu/erp"
    static let saveTripObuChargeInfo = "\(baseURL)/trip/v4/obu/chargeinfo/batch"
//    static let saveTripObuParking = "\(baseURL)/trip/obu/parking"
    static let saveObuTripSummary = "\(baseURL)/trip/v4/obu/summary"
    static let saveObuTransactions = "\(baseURL)/trip/v4/obu/payment/batch"    //  Send when connected to obu and when trigger events
    static let getCarparkByFilter = "\(baseURL)/carpark/filter"
    static let getTriplogTransactions = "\(baseURL)/trip/v4/obu/transactions"
    static let filteredOBU = "\(baseURL)/user/v4/obu/vehicle/filter"
    
}


//
//  HTTPCommonParameters.swift
//  Breeze
//
//  Created by VishnuKanth on 19/12/20.
//

import Foundation
import UIKit


public var parameters: HTTPParameters = [
    "deviceos": UIDevice.current.deviceOS(),
    "deviceosversion": UIDevice.current.systemVersionInfo(),
    "devicemodel": UIDevice.current.getName(),
    "appversion":Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? "1.0.0"
]



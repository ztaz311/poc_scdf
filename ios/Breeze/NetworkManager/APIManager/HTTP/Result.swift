//
//  Result.swift
//  Breeze
//
//  Created by VishnuKanth on 19/12/20.

import Foundation

enum Result<T> {
    
    case success(T)
    case failure(Error)
}

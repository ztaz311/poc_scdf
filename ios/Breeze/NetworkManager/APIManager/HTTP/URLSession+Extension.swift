//
//  URLSession+Extension.swift
//  Breeze
//
//  Created by Zhou Hao on 9/2/21.
//

import Foundation

extension URLSession {
    
    func load<A>(_ resource: Resource<A>, completion: @escaping (Result<A>) -> ()) where A: Decodable {
        getHeaders { [weak self] (header) in
            guard let header = header else {
                completion(Result.failure(HTTPNetworkError.headersNil))
                return
            }
        
            if let request = try? HTTPNetworkRequest.configureHTTPRequest(url: resource.url, parameters: resource.parameters, headers: header, body: resource.body, method: resource.method) {
                self?.dataTask(with: request) { data, res, error in
                    
                    if error != nil {
                        print("REQUEST URL",request.url!,error)
                        completion(Result.failure(HTTPNetworkError.serverSideError))
                        return
                    }
                    guard let data = data, let response = res as? HTTPURLResponse else {
                        completion(Result.failure(HTTPNetworkError.noData))
                        return
                    }
                    let result = HTTPNetworkResponse.handleNetworkResponse(for: response, forceSignOut: appDelegate().forceSignOut)
                    
                    switch result {
                    case .success:
                        if A.self == EmptyData.self {
                            completion(Result.success(EmptyData() as! A))
                        } else {
                            do {
                                let result = try JSONDecoder().decode(A.self, from: data)
                                completion(Result.success(result))
                            } catch let error {
                                print(error)
                                completion(Result.failure(error))
                            }
                        }
                    case .failure(let error):
                        if AWSAuth.sharedInstance.isSignedIn == true, let err = error as? HTTPNetworkError, err.rawValue == HTTPNetworkError.authenticationError.rawValue {
                            //  Force sign out
                            AWSAuth.sharedInstance.forceSignOut { success in
                                if success {
                                    appDelegate().forceSignoutUser()
                                }
                            }
                        }
                        print("REQUEST URL IN CASE FAILURE",request.url!,error)
                        completion(Result.failure(error))
                    }
                    
                }.resume()
            }
        }
    }
    
    func loadV2<A>(_ resource: Resource<A>, completion: @escaping (Result<A>) -> ()) where A: Decodable {
        getHeaders { [weak self] (header) in
            guard let header = header else {
                completion(Result.failure(HTTPNetworkError.headersNil))
                return
            }
        
            if let request = try? HTTPNetworkRequest.configureHTTPRequest(url: resource.url, parameters: resource.parameters, headers: header, body: resource.body, method: resource.method) {
                self?.dataTask(with: request) { data, res, error in
                    
                    if error != nil {
                        print("REQUEST URL",request.url!,error)
                        completion(Result.failure(HTTPNetworkError.serverSideError))
                        return
                    }
                    guard let data = data, let response = res as? HTTPURLResponse else {
                        completion(Result.failure(HTTPNetworkError.noData))
                        return
                    }
                    let result = HTTPNetworkResponse.handleNetworkResponse(for: response, forceSignOut: appDelegate().forceSignOut)
                    
                    switch result {
                    case .success:
                        if A.self == EmptyData.self {
                            completion(Result.success(EmptyData() as! A))
                        } else {
                            do {
                                let result = try JSONDecoder().decode(A.self, from: data)
                                completion(Result.success(result))
                            } catch let error {
                                print(error)
                                completion(Result.failure(error))
                            }
                        }
                    case .failure(let error):
                        
                        if AWSAuth.sharedInstance.isSignedIn == true, let err = error as? HTTPNetworkError, err.rawValue == HTTPNetworkError.authenticationError.rawValue {
                            //  Force sign out
                            AWSAuth.sharedInstance.forceSignOut { success in
                                if success {
                                    appDelegate().forceSignoutUser()
                                }
                            }
                        }
                        
                        print("REQUEST URL IN CASE FAILURE",request.url!,error)
                        do {
                            let result = try JSONDecoder().decode(A.self, from: data)
                            completion(Result.success(result))
                        } catch let error {
                            completion(Result.failure(error))
                        }
                    }
                }.resume()
            }
        }
    }
    
    func loadWithoutHeader<A>(_ resource: Resource<A>, completion: @escaping (Result<A>) -> ()) where A: Decodable {
        
        if let request = try? HTTPNetworkRequest.configureHTTPRequest(url: resource.url, parameters: resource.parameters, headers: [:], body: resource.body, method: resource.method) {
            self.dataTask(with: request) { data, res, error in
                
                if error != nil {
                    print("REQUEST URL",request.url!, error)
                    completion(Result.failure(HTTPNetworkError.serverSideError))
                    return
                }
                guard let data = data, let response = res as? HTTPURLResponse else {
                    completion(Result.failure(HTTPNetworkError.noData))
                    return
                }
                let result = HTTPNetworkResponse.handleNetworkResponse(for: response)
                
                switch result {
                case .success:
                    if A.self == EmptyData.self {
                        completion(Result.success(EmptyData() as! A))
                    } else {
                        do {
                            let result = try JSONDecoder().decode(A.self, from: data)
                            completion(Result.success(result))
                        } catch let error {
                            print(error)
                            completion(Result.failure(error))
                        }
                    }
                case .failure(let error):
                    print("REQUEST URL IN CASE FAILURE",request.url!,error)
                    completion(Result.failure(error))
                }
                
            }.resume()
        }
    }
    
    func loadUserPrefsV4(_ resource: Resource<String>, completion: @escaping (Result<String>) -> ()) {
        getHeaders { [weak self] (header) in
            guard let header = header else {
                completion(Result.failure(HTTPNetworkError.headersNil))
                return
            }
        
            if let request = try? HTTPNetworkRequest.configureHTTPRequest(url: resource.url, parameters: resource.parameters, headers: header, body: resource.body, method: resource.method) {
                self?.dataTask(with: request) { data, res, error in
                    
                    if error != nil {
                        completion(Result.failure(HTTPNetworkError.serverSideError))
                        return
                    }
                    guard let data = data, let response = res as? HTTPURLResponse else {
                        completion(Result.failure(HTTPNetworkError.noData))
                        return
                    }
                    let result = HTTPNetworkResponse.handleNetworkResponse(for: response, forceSignOut: appDelegate().forceSignOut)
                    
                    switch result {
                    case .success:
                        let jsonString = String(data: data, encoding: .utf8) ?? ""
                        completion(Result.success(jsonString))
                    case .failure(let error):
                        if AWSAuth.sharedInstance.isSignedIn == true, let err = error as? HTTPNetworkError, err.rawValue == HTTPNetworkError.authenticationError.rawValue {
                            //  Force sign out
                            AWSAuth.sharedInstance.forceSignOut { success in
                                if success {
                                    appDelegate().forceSignoutUser()
                                }
                            }
                        }
                        print("REQUEST URL IN CASE FAILURE",request.url!,error)
                        completion(Result.failure(error))
                    }
                    
                }.resume()
            }
        }
    }
}

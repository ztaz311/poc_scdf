//
//  AWSAuth.swift
//  Breeze
//
//  Created by VishnuKanth on 18/11/20.
//

import UIKit
import Amplify
import AWSCognitoAuthPlugin
import AWSPinpointAnalyticsPlugin
import AWSPluginsCore
import AWSCognitoIdentityProvider
import AWSMobileClient
import Instabug
import AWSKinesis
import Combine
import SwiftyBeaver
import FirebaseCrashlytics
import AWSCognitoAuthPlugin

enum AuthState {
    case login
    case confirmSignup
    case signUp
    case session(user:AuthUser)
    case changePassword
}

enum AuthenticationErrors:Error {
    case codeMismatch
    
}

class AWSAuth: NSObject {
    
    static let sharedInstance = AWSAuth()
    var authState:AuthState = .login
    private(set) var idToken = ""
    var userName = ""
    var userGroup = "" {
        didSet {
            UserDefaults.standard.setValue(userGroup, forKey: Values.usergroup)
        }
    }
    private(set) var cognitoId = ""
    var providerName = ""
    var phoneNumber = ""
    var emailVerifyStatus = false
    var customEmail = ""
    var carplateNumber = ""
    var iuNumber = ""
    var onboardingState: String?
    
    @Published var isSignedIn = false
    
    private(set) var acceptTC: Bool = false
    
    private(set) var userData: UserDataModel?
    
    private var userService: UserServiceProtocol = UserService()
    
    private var didForceSignOutUser: Bool = false
    
    private override init() {
        super.init()
#if BETARELEASE
        let file = Bundle.main.url(forResource: "QA_Amplify/amplifyconfiguration_QA", withExtension: ".json")
        print(file)
        if let file = file{
            
            self.initAmplifyConfigure(url: file)
            
        }
        else {
            self.initAmplifyConfigure()
        }
#else
#if APPSTORE
        let file = Bundle.main.url(forResource: "Prod_Amplify/amplifyconfiguration_Prod", withExtension: ".json")
        print(file)
        if let file = file{
            
            self.initAmplifyConfigure(url: file)
            
        }
        else {
            self.initAmplifyConfigure()
        }
#else
        self.initAmplifyConfigure()
#endif
#endif
    }
    
    func initAmplifyConfigure(url:URL? = nil){
        
        if let url = url {
            do {
                try Amplify.add(plugin: AWSCognitoAuthPlugin())
                try Amplify.add(plugin: AWSPinpointAnalyticsPlugin())
                
                let configuration = try AmplifyConfiguration(configurationFile: url)
                try Amplify.configure(configuration)
                
            } catch (_) {
                // Need to handle error if Amplify init fails
            }
        }
        else
        {
            do {
                try Amplify.add(plugin: AWSCognitoAuthPlugin())
                try Amplify.add(plugin: AWSPinpointAnalyticsPlugin())
                try Amplify.configure()
            } catch {
                print("Failed to initialize Amplify with \(error)")
            }
        }
        
        
    }
    
    func hasCarDetailInfo() -> Bool {
        return !carplateNumber.isEmpty || !iuNumber.isEmpty
    }
    
    func getCarDetailDisplay() -> String {
        return carplateNumber + " | " + iuNumber
    }
    
    func getLocalData() -> [String:Any] {
        let retValue = [
            "cognitoID": cognitoId,
            "carplateNumber": carplateNumber,
            "iuNumber": iuNumber,
            "userName": userName,
            "email": customEmail,
        ]
        return retValue
    }
    
    func setAWSServiceConfiguration(){
        //  Setting default configuration for identityProvider after login
        let configuration = AWSServiceConfiguration(region: .APSoutheast1, credentialsProvider: AWSMobileClient.default())
        AWSServiceManager.default().defaultServiceConfiguration = configuration
    }
    
    func startMonitoring() {
        
//        Task {
//            await checkUserSignedIn()
//        }
        
        // Assumes `unsubscribeToken` is declared as an instance variable in your view
        _ = Amplify.Hub.listen(to: .auth) { payload in
            switch payload.eventName {
            case HubPayload.EventName.Auth.signedIn:
                print("==HUB== User signed In")
//                self.setAWSServiceConfiguration()
            case HubPayload.EventName.Auth.sessionExpired:
                print("==HUB== Session expired")
                // Re-authenticate the user
                self.isSignedIn = false
            case HubPayload.EventName.Auth.signedOut:
                print("==HUB== User signed Out")
                self.isSignedIn = false
            case HubPayload.EventName.Auth.userDeleted:
                print("==HUB== User deleted")
                self.isSignedIn = false
            default:
                break
            }
        }
    }
    
    // This is similar to fetchCurrentSession. This is only used in startMonitor
    private func checkUserSignedIn() async {
        
        do {
            let session = try await Amplify.Auth.fetchAuthSession()
            
            // Get user sub or identity id
            if let identityProvider = session as? AuthCognitoIdentityProvider {
                let usersub = try identityProvider.getUserSub().get()
                let identityId = try identityProvider.getIdentityId().get()
                print("User sub - \(usersub) and identity id \(identityId)")
            }
            
            self.setAWSServiceConfiguration()
            SwiftyBeaver.debug("checkUserSignedIn isSignedIn = \(session.isSignedIn)")
            self.isSignedIn = session.isSignedIn
            
        } catch {
            print("Fetch auth session failed with error - \(error)")
        }
    }
    
    func getRelevantErrorMessages(error:Error,errorMessage:String) -> Any {
        
        var errorString = ""
        if let authError = error as? AWSCognitoAuthError
        {
            switch authError {
            case .codeMismatch:
                errorString = Constants.incorrectOTP
            case .userNotFound:
                errorString = Constants.userNotFound
            case .codeExpired:
                errorString = Constants.codeExpired
            case .invalidParameter:
                errorString = Constants.userNotFound
            case .limitExceeded:
                errorString = Constants.exceedOtpRetry
            case .requestLimitExceeded:
                errorString = Constants.exceedOtpLimit
            default:
                errorString = errorMessage
            }
        }
        return errorString
    }
    
    func getRelevantErrorMessage(error:AWSCognitoAuthError,loginAttempts:Int = 0) -> String{
        
        var errorString = "Are you sure you have entered your phone number correctly?"
        
        switch error {
        case .codeMismatch:
            if(loginAttempts == 0) {
                errorString = Constants.otpTriedError
            } else {
                errorString = "\(Constants.incorrectOTP)\(loginAttempts)\(Constants.otpMoreTimes)"
            }
        case .userNotFound:
            errorString = Constants.userNotFound
        case .usernameExists:
            errorString = Constants.mobileNumberInUse
        case .codeExpired:
            errorString = Constants.codeExpired
        case .invalidParameter:
            errorString = Constants.userNotFound
        case .limitExceeded:
            errorString = Constants.exceedOtpRetry
        case .failedAttemptsLimitExceeded:
            errorString = Constants.loginErroFor30Min
        case .requestLimitExceeded:
            errorString = Constants.exceedOtpLimit
        default:
            errorString = ""
        }
        return errorString
        
    }
    
    func fetchAttributes(onSuccess: @escaping(Any) -> Void, onFailure: @escaping(Error) -> Void) {
        SwiftyBeaver.debug("OBU - fetchAttributes")
        self.carplateNumber = ""
        self.iuNumber = ""
        self.providerName = ""
        self.userName = ""
        self.emailVerifyStatus = false
        self.phoneNumber = ""
        self.customEmail = ""
        self.userGroup = ""
        self.onboardingState = nil
        Task.init {
            do {
                let attributes = try await Amplify.Auth.fetchUserAttributes()
                print("User attributes - \(attributes)")
                for attribute in attributes {
                    
                    if (attribute.key == AuthUserAttributeKey.custom(UserPoolAttributes.ONBOARDING_STATE)){
                        
                        print("ONBOARDiNG STATE VALUE",attribute.value)
                        self.onboardingState = attribute.value
                    }
                    else if(attribute.key == AuthUserAttributeKey.custom("user_group"))
                    {
                        print("User group",attribute.value)
                        
                        self.userGroup = attribute.value
                    }
                    else if(attribute.key == .unknown("sub"))
                    {
                        print("SUB",attribute.value)
                        self.cognitoId = attribute.value
                    }
                    else if(attribute.key == .unknown("identities"))
                    {
                        let data = attribute.value.data(using: .utf8)!
                        do {
                            if let jsonArray = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? [Dictionary<String,Any>]
                            {
                                self.providerName = jsonArray[0]["providerName"] as! String
                                
                            } else {
                                print("bad json")
                            }
                        } catch let error as NSError {
                            print(error)
                        }
                    }
                }
                
                //  Load local datas in keychain
                if !self.cognitoId.isEmpty {
                    
                    AnalyticsManager.shared.setUserId(cognitoUserName: self.cognitoId)
                    setUserIDAttribute(cognito_id: self.cognitoId)
                    
                    let email = Prefs.shared.getValueWithSub(self.cognitoId, key: .triplogEmail)
                    let username = Prefs.shared.getValueWithSub(self.cognitoId, key: .username)
                    let carplateNumber = Prefs.shared.getValueWithSub(self.cognitoId, key: .carplateNumber)
                    let iuNumber = Prefs.shared.getValueWithSub(self.cognitoId, key: .iuNumber)
                    let phoneNumber = Prefs.shared.getStringValue(.phoneNumber)
                    
                    self.customEmail = email
                    self.userName = username
                    self.carplateNumber = carplateNumber
                    self.iuNumber = iuNumber
                    self.phoneNumber = phoneNumber
                    
                    //  After user login try to load last paired obu
                    if !OBUHelper.shared.isObuConnected() {
                        SwiftyBeaver.debug("OBU - fetchAttributes - loadLastPairedObu")
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                            OBUHelper.shared.loadLastPairedObu(false)
                        }
                    }
                    
                    //  Update auto connect OBU status to RN
                    updateAutoConnectOBU(Prefs.shared.isAutoConnectObu(self.cognitoId))
                }
                
                //                Instabug.identifyUser(withEmail: self.customEmail.isEmpty ? self.email : self.customEmail, name: self.userName)
                onSuccess("success")
            } catch {
                print("Fetch auth session failed with error - \(error)")
                onFailure(error)
            }
        }
    }
    
    func signOut(onSuccess: ((Any) -> Void)? = nil, onFailure: ((String) -> Void)? = nil)
    {
        //  Always delete phone number when user signout
        Prefs.shared.removeValueForKey(.phoneNumber)
        Prefs.shared.removeValueForKey(.isGuestMode)
        Task.init {
            let result = await Amplify.Auth.signOut()
            guard let signOutResult = result as? AWSCognitoSignOutResult
            else {
                print("Signout failed")
                onFailure?("Signout failed")
                return
            }
            print("Local signout successful: \(signOutResult.signedOutLocally)")
            onSuccess?(signOutResult)
        }
    }
    
    func forceSignOutIfNeed(_ isGuestMode: Bool) {
        if isGuestMode == self.isGuestMode() {
            //  Nothing to be done
        }else {
            signOut()
        }
    }
    
    func forceSignOut(_ onComplete: @escaping(Bool) -> Void) {
        
        //  Reset OBU connection data when user signout
        OBUHelper.shared.resetObuConnection()
        
        self.didForceSignOutUser = true
        self.isSignedIn = false
        //  Always delete phone number when user signout
        Prefs.shared.removeValueForKey(.phoneNumber)
        Prefs.shared.removeValueForKey(.isGuestMode)
        
        Task.init {
            let _ = await Amplify.Auth.signOut()
            self.cleanLocalData()
            onComplete(true)
        }
    }
    
    private func cleanLocalData() {
        
        EventEmitter.sharedInstance.dispatch(name: "logout", body: [])
        
        if(SavedSearchManager.shared.easyBreezy.count > 0)
        {
            SavedSearchManager.shared.easyBreezy.removeAll()
        }
        
        if (AmenitiesSharedInstance.shared.amenitiesSymbolLayer.count > 0){
            
            AmenitiesSharedInstance.shared.amenitiesSymbolLayer.removeAll()
        }
        
        SelectedProfiles.shared.clearProfile()
        
        Instabug.logOut()
        
        DataCenter.shared.unload()
        
        RNViewManager.sharedInstance.clearBridge()
        
    }
    
    private func sessionExpiredMoveToLogin() {
        self.forceSignOut { success in
            if success {
                appDelegate().forceSignoutUser()
            }
        }
    }
    
    func fetchCurrentSession(isLaunchingApp: Bool = false, onSuccess: @escaping(Any) -> Void, onFailure: @escaping(Error)-> Void) {
        
        //  Amplify V2
        Task.init {
            do {
                let session = try await Amplify.Auth.fetchAuthSession()
                
                // Get user sub or identity id
                if let identityProvider = session as? AuthCognitoIdentityProvider {
                    let usersub = try identityProvider.getUserSub().get()
                    let identityId = try identityProvider.getIdentityId().get()
                    self.cognitoId = "\(usersub)"
                    print("User sub - \(usersub) and identity id \(identityId)")
                    SwiftyBeaver.info("User sub - \(self.cognitoId) and identity id \(identityId)")
                }
                
                // Get AWS credentials
                if let awsCredentialsProvider = session as? AuthAWSCredentialsProvider {
                    let _ = try awsCredentialsProvider.getAWSCredentials().get()
                    // Do something with the credentials
                }
                
                // Get cognito user pool token
                if let cognitoTokenProvider = session as? AuthCognitoTokensProvider {
                    let tokens = try cognitoTokenProvider.getCognitoTokens().get()
                    // Do something with the JWT tokens
                    self.idToken = tokens.idToken
                    
                    self.didForceSignOutUser = false
                                        
                    self.setAWSServiceConfiguration()
                    
                    Settings.shared.loadFromSettingsFromAPI()
                    
                    if self.idToken.isEmpty {
                        self.isSignedIn = false
                        self.forceSignOut { success in
                            onSuccess("")
                        }
                        return
                        
                    }else {
                        
                        // also need to update the signed in flag here and update the flag only if value is different
                        if session.isSignedIn != self.isSignedIn {
                            SwiftyBeaver.debug("fetchCurrentSession isSignedIn = \(session.isSignedIn)")
                            self.isSignedIn = session.isSignedIn
                        }
                        onSuccess(session)
                    }
                }
            } catch {
                idToken = ""
                print("Fetch auth session failed with error - \(error)")
                if !isLaunchingApp && !self.didForceSignOutUser {
                    self.sessionExpiredMoveToLogin()
                }
                onFailure(error)
            }
        }
    }
    
    func signIn(username: String?, password: String?, onSuccess: @escaping(Any) -> Void, onFailure: @escaping(Any) -> Void) {
        Task.init {
            do {
                let signInResult = try await Amplify.Auth.signIn(
                    username: username,
                    password: password
                )
                if signInResult.isSignedIn {
                    print("Sign in succeeded")
                    
                    switch signInResult.nextStep {
                    case .done:
                        onSuccess(signInResult)
                    case .confirmSignInWithSMSMFACode(_, _): break
                        
                    case .confirmSignInWithCustomChallenge(_): break
                        
                    case .confirmSignInWithNewPassword(_): break
                        
                    case .resetPassword(_): break
                        
                    case .confirmSignUp(_):
                        onSuccess(signInResult)
                        break
                    }
                } else {
                    onFailure("AWS sign in failure")
                }
            } catch {
                onFailure("AWS sign in failure")
            }
        }
    }
    
    func signUp(username: String, password: String, email: String, onSuccess: @escaping(Any) -> Void, onFailure: @escaping(Error) -> Void) {
        
        Task.init {
            let attributes = [AuthUserAttribute(.custom(UserPoolAttributes.FULLNAME),value: username)]
            let options = AuthSignUpRequest.Options(userAttributes: attributes)
            do {
                let signUpResult = try await Amplify.Auth.signUp(
                    username: username,
                    password: password,
                    options: options
                )
                if case let .confirmUser(deliveryDetails, _, userId) = signUpResult.nextStep {
                    print("Delivery details \(String(describing: deliveryDetails)) for userId: \(String(describing: userId))")
                    self.authState = .confirmSignup
                    onSuccess("Verification Code sent to your \(email)")
                } else {
                    print("SignUp Complete")
                }
                
            } catch let error as AuthError {
                print("An error occurred while registering a user \(error)")
                switch error {
                case .service(_, _, let errorValue):
                    onFailure(errorValue!)
                case .configuration(_, _, _):
                    break
                case .unknown(_, _):
                    break
                case .validation(_, _, _, _):
                    break
                case .notAuthorized(_, _, _):
                    break
                case .invalidState(_, _, _):
                    break
                case .signedOut(_, _, _):
                    break
                case .sessionExpired(_, _, _):
                    break
                }
                
            } catch {
                print("Unexpected error: \(error)")
                onFailure(error)
            }
        }
        
    }
    
    func confirmSignUp(username:String, confirmationCode:String,onSuccess: @escaping(Any) -> Void, onFailure: @escaping(Error) -> Void)
    {
        Task.init {
            do {
                let confirmSignUpResult = try await Amplify.Auth.confirmSignUp (
                    for: username,
                    confirmationCode: confirmationCode
                )
                print("Confirm sign up result completed: \(confirmSignUpResult.isSignUpComplete)")
                onSuccess("sign up success")
            } catch let error as AuthError {
                print("An error occurred while confirming sign up \(error)")
                switch error {
                case .service(let errorDescription, _, let errorValue):
                    if(errorValue != nil) {
                        let _ = self.getRelevantErrorMessages(error: errorValue!, errorMessage: errorDescription)
                        onFailure(errorValue!)
                    } else {
                        onFailure(errorDescription as! Error)
                    }
                    
                case .configuration(_, _, _):
                    break
                case .unknown(_, _):
                    break
                case .validation(let errorDescription, _, _, _):
                    onFailure(errorDescription as! Error)
                case .notAuthorized(_, _, _):
                    break
                case .invalidState(let errorDescription, _, _):
                    onFailure(errorDescription as! Error)
                case .signedOut(_, _, _):
                    break
                case .sessionExpired(_, _, _):
                    break
                }
            } catch {
                print("Unexpected error: \(error)")
                onFailure(error)
            }
        }
    }
    
    func resendOTPToPhone(mobile:String, onSuccess: @escaping(Any) -> Void, onFailure: @escaping(AWSCognitoAuthError) -> Void){
        Task.init {
            do {
                let signUpResult = try await Amplify.Auth.resendSignUpCode(for: mobile)
                print("signUpResult: \(signUpResult.destination)")
                onSuccess("resend code success")
                
            } catch let error as AuthError {
                print("An error occurred while registering a user \(error)")
                switch error {
                case .service(_, _, let errorValue):
                    onFailure(errorValue! as! AWSCognitoAuthError)
                case .configuration(_, _, _):
                    break
                case .unknown(_, _):
                    break
                case .validation(_, _, _, let errorValue):
                    onFailure(errorValue! as! AWSCognitoAuthError)
                case .notAuthorized(_, _, _):
                    break
                case .invalidState(_, _, let errorValue):
                    onFailure(errorValue! as! AWSCognitoAuthError)
                case .signedOut(_, _, _):
                    break
                case .sessionExpired(_, _, _):
                    break
                }
                
            } catch {
                print("Unexpected error: \(error)")
            }
        }
    }
    
    func customSignIn(username:String, onSuccess: @escaping(Any) -> Void, onFailure: @escaping(AWSCognitoAuthError) -> Void){
        
        print("Sending Mobile number for Custom Sign In to Amplify",username)
        Task.init {
            do {
                let signInResult = try await Amplify.Auth.signIn(
                    username: username,
                    password: Constants.tempPassword
                )
                if signInResult.isSignedIn {
                    print("Sign in succeeded")
                }
                
                switch signInResult.nextStep {
                case .confirmSignInWithCustomChallenge(_):
                    onSuccess("custom challenge")
                case .confirmSignUp(_):
                    onSuccess("confirm signup")
                default:
                    break
                }
            }catch let error as AuthError {
                print("Sign in failed \(error)")
                switch error {
                case .configuration(_, _, _):
                    break
                case .service(_, _, let error):
                    onFailure(error as! AWSCognitoAuthError)
                case .unknown(_, _):
                    break
                case .validation(_, _, _, let errorValue):
                    onFailure(errorValue! as! AWSCognitoAuthError)
                case .notAuthorized(_, _, let errorValue):
                    if errorValue == nil {
                        let error:AWSCognitoAuthError = .failedAttemptsLimitExceeded
                        onFailure(error)
                    } else {
                        onFailure(errorValue! as! AWSCognitoAuthError)
                    }
                case .invalidState(_, _, _):
                    break
                case .signedOut(_, _, _):
                    break
                case .sessionExpired(_, _, _):
                    break
                }
            } catch {
                print("Unexpected error: \(error)")
            }
        }
    }
    
    func customChallenge(response: String, onSuccess: @escaping(Any) -> Void, onFailure: @escaping(AWSCognitoAuthError,Any) -> Void) {
        
        Task.init {
            do {
                let signInResult = try await Amplify.Auth.confirmSignIn(challengeResponse: response)
                if signInResult.isSignedIn {
                    print("Confirm sign in succeeded. The user is signed in.")
                    print("Next step: \(signInResult.nextStep)")
                }
                // Switch on the next step to take appropriate actions.
                // If `signInResult.isSignedIn` is true, the next step
                // is 'done', and the user is now signed in.
                switch signInResult.nextStep {
                case .confirmSignInWithCustomChallenge(let info):
                    if(info!["errorCode"] == "Invalid OTP")
                    {
                        if ( info!["remainingAttempts"] == "1"){
                            onFailure(AWSCognitoAuthError.codeMismatch,"\(Constants.incorrectOTP)\(info!["remainingAttempts"] ?? "")\(Constants.otpMoreTime)")
                        }else{
                            onFailure(AWSCognitoAuthError.codeMismatch,"\(Constants.incorrectOTP)\(info!["remainingAttempts"] ?? "")\(Constants.otpMoreTimes)")
                        }
                    }
                case .confirmSignUp(_):
                    break
                case .done:
                    onSuccess("custom challenge")
                default:
                    break
                }
            } catch let error as AuthError {
                print("Confirm sign in failed \(error)")
                switch error {
                case .configuration(_, _, _):
                    break
                case .service(_, _, let error):
                    onFailure(error as! AWSCognitoAuthError,"")
                case .unknown(_, _):
                    break
                case .validation(_, _, _, let errorValue):
                    onFailure(errorValue! as! AWSCognitoAuthError,"")
                case .notAuthorized(_, _, _):
                    onFailure(AWSCognitoAuthError.limitExceeded,Constants.otpTriedError)
                case .invalidState(_, _, _):
                    onFailure(AWSCognitoAuthError.limitExceeded,Constants.otpTriedError)
                case .signedOut(_, _, _):
                    break
                case .sessionExpired(_, _, _):
                    break
                }
                
            } catch {
                print("Unexpected error: \(error)")
            }
        }
    }
    
    /*
     Save T&C acception true to backend
     */
    func changeAcceptTC(onSuccess: @escaping(Bool) -> Void) {
        userService.updateTNC { result in
            switch result {
            case .success(_):
                SwiftyBeaver.debug("changeAcceptTC success")
                self.acceptTC = true
                onSuccess(true)
            case .failure(let error):
                SwiftyBeaver.error("changeAcceptTC failed: \(error.localizedDescription)")
                onSuccess(false)
                break
            }
        }
    }
    
    /*
     Get T&C acception from backend
     */
    func getAcceptTCStatus(onSuccess: @escaping(Bool) -> Void) {
        userService.getUserData { result in
            switch result {
            case .success(let userData):
                self.userData = userData
                self.acceptTC = userData.tncStatus
                onSuccess(true)
            case .failure(let error):
                SwiftyBeaver.error(error)
                onSuccess(false)
            }
        }
    }
    
    /*
     Get UserData
     */
    func getUserData(onSuccess: @escaping(Bool) -> Void) {
        userService.getUserData { result in
            switch result {
            case .success(let userData):
                self.userData = userData
                self.acceptTC = userData.tncStatus
                onSuccess(true)
            case .failure(let error):
                SwiftyBeaver.error(error)
                onSuccess(false)
            }
        }
    }
    
    func faceBookSignIn(presentAnchor:UIWindow, onSuccess: @escaping(Any) -> Void, onFailure: @escaping(Any) -> Void){
        Task.init {
            do {
                let signInResult = try await Amplify.Auth.signInWithWebUI(for: .facebook, presentationAnchor: presentAnchor, options: .preferPrivateSession())
                if signInResult.isSignedIn {
                    print("Sign in succeeded")
                    onSuccess("Success")
                }
            } catch let error as AuthError {
                print("Sign in failed \(error)")
                switch error {
                case .invalidState(let message, _, _):
                    if(message == Constants.userAlreadySignIn) {
                        onFailure("Already SignIn")
                    }
                default:
                    onFailure("")
                }
            } catch {
                print("Unexpected error: \(error)")
                onFailure(error.localizedDescription)
            }
        }
    }
    
    func googleSignIn(presentAnchor:UIWindow, onSuccess: @escaping(Any) -> Void, onFailure: @escaping(Any) -> Void){
        Task.init {
            do {
                let signInResult = try await Amplify.Auth.signInWithWebUI(for: .google, presentationAnchor: presentAnchor, options: .preferPrivateSession())
                if signInResult.isSignedIn {
                    print("Sign in succeeded")
                    onSuccess("Success")
                }
            } catch let error as AuthError {
                print("Sign in failed \(error)")
                switch error {
                case .invalidState(let message, _, _):
                    if(message == Constants.userAlreadySignIn) {
                        onFailure("Already SignIn")
                    }
                default:
                    onFailure("")
                }
            } catch {
                print("Unexpected error: \(error)")
                onFailure(error.localizedDescription)
            }
        }
    }
    
    func appleSignIn(presentAnchor:UIWindow, onSuccess: @escaping(Any) -> Void, onFailure: @escaping(Any) -> Void){
        
        Task.init {
            do {
                let signInResult = try await Amplify.Auth.signInWithWebUI(for: .apple, presentationAnchor: presentAnchor, options: .preferPrivateSession())
                switch signInResult.nextStep {
                case .done:
                    onSuccess(signInResult)
                default:
                    onFailure("")
                    break
                }
            } catch let error as AuthError {
                print("Sign in failed \(error)")
                switch error {
                case .invalidState(let message, _, _):
                    if(message == Constants.userAlreadySignIn) {
                        onFailure("Already SignIn")
                    }
                default:
                    onFailure("")
                }
            } catch {
                print("Unexpected error: \(error)")
                onFailure(error.localizedDescription)
            }
        }
    }
    
    
    func getUserID() -> String? {
        if(idToken != "") {
            let components = idToken.components(separatedBy: ".")
            
            guard components.count >= 2 else { return nil }
            var payload64 = components[1]
            
            while payload64.count % 4 != 0 {
                payload64 += "="
            }
            
            do {
                guard let payloadData = Data(base64Encoded: payload64,
                                             options:.ignoreUnknownCharacters),
                      let json = try JSONSerialization.jsonObject(with: payloadData, options: []) as? [String:Any] else {return nil}
                
                if let sub = json["sub"] as? String {
                    print(sub)
                }
                
            } catch(let error) {
                print(error.localizedDescription)
            }
        }
        
        return nil
    }
    
    // MARK: - id token expiration related functions
    func checkTokenExpiry(idToken:String) -> Bool {
        // This might not be thread safe. Either to lock it or just remove it.
        if(idToken != "") {
            let components = idToken.components(separatedBy: ".")
            
            guard components.count >= 2 else { return false }
            var payload64 = components[1]
            
            // need to pad the string with = to make it divisible by 4,
            // otherwise Data won't be able to decode it
            while payload64.count % 4 != 0 {
                payload64 += "="
            }
            
            do {
                guard let payloadData = Data(base64Encoded: payload64,
                                             options:.ignoreUnknownCharacters),
                      let payload = String(data: payloadData, encoding: .utf8),
                      let json = try JSONSerialization.jsonObject(with: payloadData, options: []) as? [String:Any],
                      let exp = json["exp"] as? Int else { return false }
                
                print(payload)
                
                let current = Int(Date().timeIntervalSince1970)
                
                //                print("exp: \(exp), current: \(current), remaining seconds: \(exp - current)")
                
                if(exp < current){
                    return true
                } else {
                    return false
                }
            } catch(let error) {
                print(error.localizedDescription)
                return false
            }
        } else {
            return true
        }
    }
    
    func isCurrentTokenExpired() -> Bool {
        return AWSAuth.sharedInstance.checkTokenExpiry(idToken: AWSAuth.sharedInstance.idToken)
    }
    
}

//  For guest user
extension AWSAuth {
    
    func guestUserExisting() -> Bool {
        let userId = Prefs.shared.getStringValue(.userId)
        return !userId.isEmpty
    }
    
    func isGuestMode() -> Bool {
        let isGuestMode = Prefs.shared.getBoolValue(.isGuestMode)
        return isGuestMode
    }
    
    //  Fetching user ID use for logging in as a guest that generated from backend
    private func fetchGuestUserId(completion: @escaping(String) -> Void) {
        GuestUserIdService().getGuestUserId { result in
            switch result {
            case .success(let model):
                completion(model.getID())
            case .failure(_):
                completion("")
            }
        }
    }
    
    //  Try to load user ID from keychain, otherwise call API to fetch ID
    private func generateUserId(completion: @escaping(String) -> Void) {
        
        //  Always use this username for guest user
        let userId = Prefs.shared.getStringValue(.userId)
        //  For existing user compatible
        if UserDefaults.standard.object(forKey: PrefsKey.isSuccessSignupAsGuestUser.rawValue) == nil {
            if userId.isEmpty {
                self.fetchGuestUserId { userId in
                    if !userId.isEmpty {
                        Prefs.shared.setValue(userId, forkey: .userId)
                    }
                    completion(userId)
                }
            }else {
                completion(userId)
            }
        } else {
            let didSuccessSignUpGuestUser = Prefs.shared.getBoolValue(.isSuccessSignupAsGuestUser)
            if didSuccessSignUpGuestUser {
                //  Need confirm login only without signup
                if !userId.isEmpty {
                    SwiftyBeaver.debug("Guest userId is existing")
                    completion(userId)
                }
            } else {
                self.fetchGuestUserId { userId in
                    if !userId.isEmpty {
                        Prefs.shared.setValue(userId, forkey: .userId)
                    }
                    completion(userId)
                }
            }
        }
    }
    
    func customSignUpGuestUser(userId: String, options: AuthSignUpRequest.Options?, completion: ((Bool) -> Void)? = nil) {
        Task.init {
            do {
                let signUpResult = try await Amplify.Auth.signUp(username: userId, password: Constants.tempPassword, options: options)
                
                switch signUpResult.nextStep {
                case .done:
                    SwiftyBeaver.debug("Sign up as guest user completes")
                    Prefs.shared.setValue(true, forkey: .isSuccessSignupAsGuestUser)
                    self.confirmSignInAsGuestUser(completion: completion)
                case .confirmUser(let details, _, _):
                    //  Note: Login as guest user does not require confirm signup
                    print("Delivery details \(String(describing: details))")
                    SwiftyBeaver.error("Sign up as guest user requires confirmation from Amplify")
                    self.authState = .confirmSignup
                    completion?(false)
                }
                
            } catch let error as AuthError {
                print("An error occurred while registering a user \(error)")
                switch error {
                case .service( _, _, let errorValue):
                    if let authError = errorValue as? AWSCognitoAuthError, authError == .usernameExists {
                        //  Try to signin and confirm signin
                        SwiftyBeaver.debug("Guest userId is existing")
                        Prefs.shared.setValue(true, forkey: .isSuccessSignupAsGuestUser)
                        self.confirmSignInAsGuestUser(completion: completion)
                    }else {
                        SwiftyBeaver.error("Login as guest user failed: \(errorValue?.localizedDescription ?? "")")
                        completion?(false)
                    }
                default:
                    completion?(false)
                    break
                }
            } catch {
                print("Unexpected error: \(error)")
                completion?(false)
            }
        }
    }
    
    //  Try to signin as guest if success will return completion block with true value, otherwise we should force logout user
    func signInAsGuestUser(completion: ((Bool) -> Void)? = nil) {
        
        self.generateUserId { [weak self] userId in
            //  If userId is not empty
            if !userId.isEmpty {
                let userAttributes = [AuthUserAttribute(AuthUserAttributeKey.custom("guest_login"), value: "true"), AuthUserAttribute(AuthUserAttributeKey.custom("signup_device_os"), value: "iOS"), AuthUserAttribute(AuthUserAttributeKey.custom("signup_app_version"), value: "\(UIApplication.buildString())")]
                
                let options = AuthSignUpRequest.Options(userAttributes: userAttributes)
                
                //  For compatible with existing guest user
                if UserDefaults.standard.object(forKey: PrefsKey.isSuccessSignupAsGuestUser.rawValue) == nil {
                    //  Require signup once again
                    self?.customSignUpGuestUser(userId: userId, options: options, completion: completion)
                } else {
                    let didSuccessSignUpGuestUser = Prefs.shared.getBoolValue(.isSuccessSignupAsGuestUser)
                    if didSuccessSignUpGuestUser {
                        //  Try to signin and confirm signin
                        SwiftyBeaver.debug("Guest userId is existing")
                        self?.confirmSignInAsGuestUser(completion: completion)
                    } else {
                        self?.customSignUpGuestUser(userId: userId, options: options, completion: completion)
                    }
                }
                
            } else {
                //  User ID is empty
                SwiftyBeaver.error("Login as guest user failed with empty userId")
                completion?(false)
            }
        }
    }
    
    //  Confirm signin
    func confirmSignInAsGuestUser(completion: ((Bool) -> Void)? = nil) {
        let userId = Prefs.shared.getStringValue(.userId)
        if !userId.isEmpty {
            self.customSignIn(username: userId) { result in
                                
                AWSAuth.sharedInstance.customChallenge(response: Constants.tempPassword) { result in
                    print("Success confirm login")
                    
                    //                    AWSAuth.sharedInstance.updateUserAttributeFirstTimeSign() { result in
                    //                        // Handle sucsses
                    //                    } onFailure: { (error) in
                    //                        // Handle falied
                    //                    }
                    
                    AWSAuth.sharedInstance.fetchCurrentSession { (session) in
                        if let session = session as? AWSAuthCognitoSession {
                            //  If session is still valid
                            if session.isSignedIn {
                                print("Success get token")
                                completion?(true)
                            }else {
                                completion?(false)
                            }
                            
                        }else {
                            completion?(false)
                        }
                    } onFailure: { (error) in
                        print("Fetch token error")
                        completion?(false)
                    }
                } onFailure: { error,stringMSG in
                    print("Success confirm login")
                    completion?(false)
                }
            } onFailure: { error in
                print("Error signin \(error.localizedDescription)")
                completion?(false)
            }
        }else {
            print("Failed to get userId")
            completion?(false)
        }
    }
}

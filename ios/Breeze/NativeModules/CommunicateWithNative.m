//
//  CommunicateWithNative.m
//  Breeze
//
//  Created by VishnuKanth on 07/07/21.
//

#import <Foundation/Foundation.h>
#import <React/RCTBridgeModule.h>
#import <React/RCTEventEmitter.h>
@interface RCT_EXTERN_REMAP_MODULE(CommunicateWithNative, CommunicateWithNativeModule, NSObject)
RCT_EXTERN_METHOD(sendMessageToNative: (nullable NSDictionary *)rnMessage screenName: (NSString*)screenName)
RCT_EXTERN_METHOD(getAnalyticEventsFromRN: (NSString *)eventName eventValue:(NSString *)eventValue screenName:(NSString *)screenName)
RCT_EXTERN_METHOD(getAnalyticEventsFromRN: (NSString *)eventName eventValue:(NSString *)eventValue screenName:(NSString *)screenName saveToLocal:(BOOL)saveToLocal extraData: (nullable NSDictionary *)extraData)
RCT_EXTERN_METHOD(getAnalyticLogPopUpEvent: (NSString *)screenName popupName:(NSString *)popupName eventValue:(NSString *)eventValue)

RCT_EXTERN_METHOD(getAnalyticLogOpenPopUpEvent: (NSString *)screenName popupName:(NSString *)popupName)
RCT_EXTERN_METHOD(getAnalyticLogClosePopUpEvent: (NSString *)screenName popupName:(NSString *)popupName)
RCT_EXTERN_METHOD(getAnalyticEventsFromRNForScreenView: (NSString *)screenName)
RCT_EXTERN_METHOD(sendLocationDataAnalytics:(NSString *)eventValue screenName:(NSString *)screenName locationName:(NSString *)locationName latitude:(NSString*)latitude longitude:(NSString*)longitude)
RCT_EXTERN_METHOD(sendCallbackToNative: (RCTResponseSenderBlock)rnCallback)
//RCT_EXTERN_METHOD(goToSecondViewController: (nonnull NSNumber *)reactTag)
RCT_EXTERN_METHOD(backToNative: (nonnull NSNumber *)reactTag)
RCT_EXTERN_METHOD(dismissNative: (nonnull NSNumber *)reactTag)
RCT_EXTERN_METHOD(openETAContactsPage: (nonnull NSNumber *)reactTag)

RCT_EXTERN_METHOD(showRouteToLocation:
                  (nonnull NSNumber *)reactTag
                  selectedIndex:(nonnull NSNumber *)selectedIndex
                  location: (nullable NSDictionary)location
)
RCT_EXTERN_METHOD(handleSearchLocationResult:
                  (nullable NSDictionary)location)
RCT_EXTERN_METHOD(onCollectionDetailSearch:
                  (nullable NSDictionary)location)
RCT_EXTERN_METHOD(closeOBULiteScreen)
RCT_EXTERN_METHOD(continueWithoutAccount)
RCT_EXTERN_METHOD(shareCarDetailsToNative:(NSString *)carplateNumber iuNumber:(NSString *) iuNumber)
RCT_EXTERN_METHOD(getLocalUserData:
                  (RCTPromiseResolveBlock)resolve
                                     rejecter:(RCTPromiseRejectBlock)reject
)
RCT_EXTERN_METHOD(getObuConnectionStatus:
                  (RCTPromiseResolveBlock)resolve
                                     rejecter:(RCTPromiseRejectBlock)reject
)

RCT_EXTERN_METHOD(handleClearSearch:
                  (RCTPromiseResolveBlock)resolve
                                     rejecter:(RCTPromiseRejectBlock)reject
)

RCT_EXTERN_METHOD(toggleDisplayTopObuButtons:(nonnull BOOL)isShow
)
RCT_EXTERN_METHOD(toggleMapViewOBU:(nonnull BOOL)isShowingMap
)
RCT_EXTERN_METHOD(selectCarparkFromCarparkList:
                  (nullable NSDictionary)carpark)

RCT_EXTERN_METHOD(openCarkParkList:(nonnull BOOL)isOpen
)

RCT_EXTERN_METHOD(dismissZoneDetail
)

RCT_EXTERN_METHOD(selectAmenity:
                  (nonnull NSString *)amenityName
                  isSelected: (nonnull BOOL)isFullScreen
)

RCT_EXTERN_METHOD(openNativeScreen:
                  (nonnull NSNumber *)reactTag
                  screenName: (NSString) screenName
                  navigationParams: (nullable NSDictionary)navigationParams
)
RCT_EXTERN_METHOD(getValueFromInfoPlist:
                  (nonnull NSString *)key
                  callback: (RCTResponseSenderBlock) callback
)
RCT_EXTERN_METHOD(getLandingFocusedLocation: (nonnull NSString *)mode
                  callback: (RCTResponseSenderBlock) callback
)

RCT_EXTERN_METHOD(getUserLocation: (RCTResponseSenderBlock) callback
)

RCT_EXTERN_METHOD(switchMode: (NSString *)landingMode)
RCT_EXTERN_METHOD(initUserPreferenceSetting:
                  (nullable NSDictionary)navigationParams)
RCT_EXTERN_METHOD(presentParkingDetail:
                  (nullable NSDictionary)carpark)
RCT_EXTERN_METHOD(handleSharingMapCollection:
                  (nullable NSString *)shareLink
                  isSaved: (nonnull BOOL)isSaved
                  )
                    

RCT_EXTERN_METHOD(cancelParkingAvailable)
RCT_EXTERN_METHOD(startWalkingPath)
RCT_EXTERN_METHOD(backToHomePage)

RCT_EXTERN_METHOD(getCalendarDates:
                  (NSString *) startDate
                  endDate: (NSString *) endDate
)
RCT_EXTERN_METHOD(closeNotifyArrivalScreen)
RCT_EXTERN_METHOD(closeTripLogScreen)
RCT_EXTERN_METHOD(closeMoreScreen)
RCT_EXTERN_METHOD(sendTrafficCameraData:
                  (nullable NSDictionary)trafficData)

RCT_EXTERN_METHOD(selectCamera:
                  (NSString *)trafficID)

RCT_EXTERN_METHOD(openTrafficDetail:
                  (NSString *)expressWayCode)

RCT_EXTERN_METHOD(handleChangeERPSelectedTime:(nonnull NSNumber *)erpSelectedTime
)

RCT_EXTERN_METHOD(searchERPRoutes:
                  (nullable NSDictionary)source
                  destination: (nullable NSDictionary)destination
                  timestamp:(nonnull NSNumber *)timestamp
                  callback: (RCTResponseSenderBlock) callback
)

RCT_EXTERN_METHOD(selectERPRoute:
                  (nonnull NSString *)id
                  routeId: (nonnull NSString *)routeId
                  erpTimeChanged: (nonnull BOOL)erpTimeChanged
)


RCT_EXTERN_METHOD(startETANative: (nullable NSDictionary *)rnMessage)

RCT_EXTERN_METHOD(adjustRecenter: (nonnull NSNumber *)y index: (nonnull NSNumber *)index fromScreen: (nonnull NSString *)fromScreen)

RCT_EXTERN_METHOD(adjustRecenterMapExplore: (nonnull NSNumber *)y index: (nonnull NSNumber *)index fromScreen: (nonnull NSString *)fromScreen)

RCT_EXTERN_METHOD(openERPDetail:
                  (nonnull NSNumber *)erpId)
RCT_EXTERN_METHOD(showNoCarparkAlert)
RCT_EXTERN_METHOD(closeCarparkListScreen)
RCT_EXTERN_METHOD(closeLandingDropPinScreen)
RCT_EXTERN_METHOD(onSavedDropPinLanding: (nullable NSDictionary)savedPinData)
RCT_EXTERN_METHOD(cancelOBUCarparkAvailability)
RCT_EXTERN_METHOD(clearSearchCollectionDetails)
RCT_EXTERN_METHOD(closeSharedCollectionDetails)
RCT_EXTERN_METHOD(cancelOBUTravelTime)
RCT_EXTERN_METHOD(endOBUInstall)
RCT_EXTERN_METHOD(initParkingMode)

RCT_EXTERN_METHOD(toggleOBUAutoConnect:
                  (nonnull BOOL)isEnabled
)
RCT_EXTERN_METHOD(toggleConnectOBU:
                  (nonnull BOOL)isConnecting
                  vehicleNumber: (nonnull NSString *)vehicleNumber
                  obuName: (nonnull NSString *)obuName
)
RCT_EXTERN_METHOD(removeOBU:
                  (nonnull NSString *)vehicleNumber
                  obuName: (nonnull NSString *)obuName
)

RCT_EXTERN_METHOD(openTrafficDetail:
                  (NSString *)expressWayCode)

RCT_EXTERN_METHOD(disableCruise:(nonnull BOOL)disabled)

RCT_EXTERN_METHOD(changeRNScreen: (nonnull BOOL)isSelected
)

RCT_EXTERN_METHOD(handleClickTrafficInbox: (nullable NSString *)inboxTrafficAlertData)

RCT_EXTERN_METHOD(sendSeenMessageList: (nullable NSString *)seenCount)

RCT_EXTERN_METHOD(handleResetHome)

RCT_EXTERN_METHOD(closeOnboardingTutorial)

RCT_EXTERN_METHOD(closeCollectionDetail)

RCT_EXTERN_METHOD(didSaveSharedLocationToCollection: (nonnull NSNumber *)bookmarkId)

RCT_EXTERN_METHOD(closeProgressTrackingScreen)


RCT_EXTERN_METHOD(sendCollectionData:
                  (nonnull NSString *)screenName
                  data: (nullable NSDictionary)data
)

RCT_EXTERN_METHOD(showToastfromRN:
                  (nonnull NSString *)type
                  message: (nonnull NSString *)message
)

RCT_EXTERN_METHOD(getUserPreferenceSettings: (RCTResponseSenderBlock) callback
)

RCT_EXTERN_METHOD(updateUserPreferenceSettings: (nullable NSDictionary)settings)

/// When content is selected in RN content bottom sheet. **content_id** needed to be sent to native
RCT_EXTERN_METHOD(selectContent: (nonnull NSNumber *)contentId)

/// When content bottom sheet MOCK tab is clicked
RCT_EXTERN_METHOD(switchToLandingTab: (nonnull NSString *)screenName)


RCT_EXTERN_METHOD(goBackExploreMap: (nonnull NSString *)screenName)

RCT_EXTERN_METHOD(appearInstabug)

RCT_EXTERN_METHOD(popToMapLanding: (nonnull NSNumber *)reactTag)

RCT_EXTERN_METHOD(logNative: (nonnull NSString *)tag
                  message: (nonnull NSString *)message
)

RCT_EXTERN_METHOD(cancelPairing: (nonnull NSString *)obuName
)
RCT_EXTERN_METHOD(openCarparkListScreen)
RCT_EXTERN_METHOD(closeInviteScreen)


@end

@interface RCT_EXTERN_MODULE(ContactEvent, RCTEventEmitter)
RCT_EXTERN_METHOD(openETAContactsPage: (nonnull NSNumber *)reactTag)
@end

@interface RCT_EXTERN_MODULE(AnalyticsEvent, RCTEventEmitter)
@end

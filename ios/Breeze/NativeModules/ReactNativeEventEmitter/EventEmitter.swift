//
//  EventEmitter.swift
//  HelloUIKit
//
//  Created by Đức Nguyễn on 09/08/2021.
//

class EventEmitter {

    /// Shared Instance.
    public static var sharedInstance = EventEmitter()

    // ReactNativeEventEmitter is instantiated by React Native with the bridge.
    private static var eventEmitter: ReactNativeEventEmitter!

    private init() {
        
        //Empty constructor
    }

    // When React Native instantiates the emitter it is registered here.
    func registerEventEmitter(eventEmitter: ReactNativeEventEmitter) {
        EventEmitter.eventEmitter = eventEmitter
    }

    func dispatch(name: String, body: Any?) {
        if(EventEmitter.eventEmitter != nil)
        {
            EventEmitter.eventEmitter.sendEvent(withName: name, body: body)
        }
        
    }

    /// All Events which must be support by React Native.
    lazy var allEvents: [String] = {
        var allEventNames: [String] = []

        // Append all events here
        allEventNames.append("favouriteListChange")
        allEventNames.append("etaFavouriteListChange")
        allEventNames.append("logout")
        
        allEventNames.append(ShareBusinessLogicModule.SHARE_BUSINESS_LOGIC_REQUEST)
        allEventNames.append(ShareBusinessLogicModule.FN_GET_RECENT_LIST)
        allEventNames.append(ShareBusinessLogicModule.FN_GET_FAV_LIST)
        allEventNames.append(ShareBusinessLogicModule.FN_GET_SHORTCUT_LIST)
        allEventNames.append(ShareBusinessLogicModule.FN_SEARCH_LOCATION)
        allEventNames.append(ShareBusinessLogicModule.FN_GET_ETA_LIST)
        allEventNames.append(ShareBusinessLogicModule.FN_SAVE_RECENT_LIST)
        allEventNames.append(ShareBusinessLogicModule.FN_GET_GOOGLE_LAT_LNG_IF_NEED)
        allEventNames.append(ShareBusinessLogicModule.FN_GET_ONBOARDING_STATE)
        allEventNames.append(ShareBusinessLogicModule.FN_SAVE_ONBOARDING_STATE)
        allEventNames.append(ShareBusinessLogicModule.FN_GET_LOCAL_USER_PREFERENCES)
        allEventNames.append(ShareBusinessLogicModule.FN_SAVE_LOCAL_USER_PREFERENCES)
        allEventNames.append(ShareBusinessLogicModule.FN_UPDATE_UPCOMING_TRIPS)
        allEventNames.append(ShareBusinessLogicModule.SELECT_TRAFFIC_CAMERA)
        allEventNames.append(ShareBusinessLogicModule.OPEN_CALCULATE_FEE_CARPARK)
        allEventNames.append(ShareBusinessLogicModule.PARKING_BOTTOM_SHEET_REFRESH)        
        allEventNames.append(ShareBusinessLogicModule.SEND_ERP_DATA_TO_REACT)
        allEventNames.append(ShareBusinessLogicModule.SEND_EVENT_SELECTED_ERP)
        allEventNames.append(ShareBusinessLogicModule.SEND_THEME_CHANGE_EVENT)
        allEventNames.append(ShareBusinessLogicModule.NAVIGATE_TO_HOME_TAB)
        allEventNames.append(ShareBusinessLogicModule.NAVIGATE_TO_INBOX_SCREEN)
        allEventNames.append(ShareBusinessLogicModule.READ_INBOX_EVENT)
        allEventNames.append(ShareBusinessLogicModule.OPEN_NOTIFICATION_FROM_NATIVE)
        allEventNames.append(ShareBusinessLogicModule.RECEIVE_NEW_NOTIFICATIONS)
        allEventNames.append(ShareBusinessLogicModule.OPEN_ONBOARDING_TUTORIAL)
        allEventNames.append(ShareBusinessLogicModule.DISABLE_PROFILE_AMENITIES)
        allEventNames.append(ShareBusinessLogicModule.OPEN_MY_VOUCHER_FROM_NOTIFICATION)
        allEventNames.append(ShareBusinessLogicModule.UPDATE_BOTTOM_SHEET_INDEX)
        allEventNames.append(ShareBusinessLogicModule.UPDATE_AMENITIES_PREF)
        allEventNames.append(ShareBusinessLogicModule.TRIGGER_INIT_USER_PREF)
        allEventNames.append(ShareBusinessLogicModule.UPDATE_SAVED_LOCATION)
        allEventNames.append(ShareBusinessLogicModule.SELECT_SAVED_LOCATION)
        allEventNames.append(ShareBusinessLogicModule.REMOVE_SHORTCUT_FROM_MAP)
        allEventNames.append(ShareBusinessLogicModule.ON_FINISHED_INITIALIZING_USER_DATA)
        allEventNames.append(ShareBusinessLogicModule.RESET_AMENITY_SELECTIONS)
        allEventNames.append(ShareBusinessLogicModule.UPDATE_PARKING_AVAILABLE_STATUS)
        allEventNames.append(ShareBusinessLogicModule.UPDATE_OBU_LITE_SPEED)
        allEventNames.append(ShareBusinessLogicModule.UPDATE_OBU_EVENT_NOTI)
        allEventNames.append(ShareBusinessLogicModule.UPDATE_OBU_CASH_CARD_INFO)
        allEventNames.append(ShareBusinessLogicModule.OPEN_MODAL_OBU_INSTALL)        
        allEventNames.append(ShareBusinessLogicModule.CHANGE_STATUS_PAIR)
        allEventNames.append(ShareBusinessLogicModule.OPEN_OBU_CARPARK_AVAILABILITY)
        allEventNames.append(ShareBusinessLogicModule.OPEN_OBU_TRAVEL_TIME)
        allEventNames.append(ShareBusinessLogicModule.UPDATE_PAIRED_VEHICLE_LIST)
        allEventNames.append(ShareBusinessLogicModule.UPDATE_VALUE_AUTO_CONNECT_OBU)
        allEventNames.append(ShareBusinessLogicModule.BACK_TO_HOME_PAGE)
        allEventNames.append(ShareBusinessLogicModule.SET_NO_CARD)
        allEventNames.append(ShareBusinessLogicModule.DISABLE_TOGGLE_BUTTON_CARPARK_LIST)
        allEventNames.append(ShareBusinessLogicModule.TOGGLE_LANDING_DROP_PIN_SCREEN)
        allEventNames.append(ShareBusinessLogicModule.SELECTED_INDEX_SHARE_COLLECTION)
        allEventNames.append(ShareBusinessLogicModule.TOGGLE_SAVE_DROP_PIN_LOCATION_LANDING)
        allEventNames.append(ShareBusinessLogicModule.TOGGLE_SAVE_LOCATION_COLLECTION_DETAILS)
        allEventNames.append(ShareBusinessLogicModule.TOGGLE_SAVE_LOCATION_SHARED_COLLECTION)
        allEventNames.append(ShareBusinessLogicModule.TOGGLE_DROP_PIN_LANDING)
        allEventNames.append(ShareBusinessLogicModule.TOGGLE_DROP_PIN_COLLECTION_DETAILS)
        allEventNames.append(ShareBusinessLogicModule.SHARE_LOCATION_FROM_NATIVE)
        allEventNames.append(ShareBusinessLogicModule.LOCATION_INVITE_FROM_NATIVE)
        allEventNames.append(ShareBusinessLogicModule.OPEN_COLLECTION_DETAIL_FROM_SHARED)
        
        return allEventNames
    }()

}

//
//  RNAuth.m
//  Breeze
//
//  Created by VishnuKanth on 24/08/21.
//

#import <Foundation/Foundation.h>
#import <React/RCTBridgeModule.h>
@interface RCT_EXTERN_REMAP_MODULE(RNAuth, RNAuthModule, NSObject)

RCT_EXTERN_METHOD(refreshIdToken:
                   (RCTPromiseResolveBlock)resolve
                   rejecter:(RCTPromiseRejectBlock)reject)

@end

//
//  AlertiOSModule.swift
//  Breeze
//
//  Created by VishnuKanth on 20/07/21.
//

import Foundation
import React

@objc(AlertIOSModule)
class AlertIOSModule: NSObject {
    func dummy(value: Any?) {
        //We don't need this
    }

    @objc
    func alert(_ reactTag: NSNumber,
               title: String?,
               message: String?,
               buttons: NSArray,
               onButtonSelected: @escaping RCTResponseSenderBlock
    ) {

        DispatchQueue.main.async {
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            var count = 0;
            for button in buttons {
                let index = count
                let text: String? = (button as! NSDictionary).value(forKey: "text") as? String
                let styleStr: String? = (button as! NSDictionary).value(forKey: "style") as? String
                var style: UIAlertAction.Style
                switch (styleStr) {
                case "cancel":
                    style = .cancel
                    break
                case "destructive":
                    style = .destructive
                    break
                default:
                    style = .default
                    break
                }
                let onPress: RCTResponseSenderBlock?  = (button as! NSDictionary).value(forKey: "onPress") as? RCTResponseSenderBlock
                let action = UIAlertAction(
                    title: text ?? "",
                    style: style,
                    handler: { _ in
                        onButtonSelected([index])
                    })
                alert.addAction(action)
                count = count + 1;
            }

            if (alert.actions.count == 0) {
                let action = UIAlertAction(
                    title: "Okay",
                    style: .default,
                    handler: nil)
                alert.addAction(action)
            }
            
            if let view = RNViewManager.sharedInstance.getBrigde.uiManager.view(forReactTag: reactTag) {
                let reactNativeVC: UIViewController? = view.reactViewController()
                reactNativeVC?.present(alert, animated: true, completion: nil)
            }
        }
    }
}


//
//  AlertiOS.m
//  Breeze
//
//  Created by VishnuKanth on 20/07/21.
//

#import <Foundation/Foundation.h>
#import <React/RCTBridgeModule.h>

@interface RCT_EXTERN_REMAP_MODULE(AlertIOS, AlertIOSModule, NSObject)

RCT_EXTERN_METHOD(alert:
                  (nonnull NSNumber *)reactTag
                  title:(NSString *)title
                  message:(nullable NSString *)message
                  buttons:(nullable NSArray *)buttons
                  onButtonSelected:(RCTResponseSenderBlock)onButtonSelected
)
@end

//
//  ContactEvent.swift
//  Breeze
//
//  Created by VishnuKanth on 14/07/21.
//

import Foundation
import React
import ContactsUI


@objc(ContactEvent)
class ContactEvent: RCTEventEmitter {

   var hasListener: Bool = false
   var tag:NSNumber = 0

   override func startObserving() {
     hasListener = true
   }

   override func stopObserving() {
     hasListener = false
   }
    
    override func supportedEvents() -> [String]! {
      return ["SelectEvent"];
    }
    
//    override func constantsToExport() -> [AnyHashable : Any]! {
//
//    }
//
//    override static func requiresMainQueueSetup() -> Bool {
//
//    }

  @objc
  func sendContactEvent(contactName:String,contactNumber:String,eventCancel:Bool) {
    
        if !PhoneValidator.isValidSingaporeMobile(number: contactNumber){
            
            if let view = RNViewManager.sharedInstance.getBrigde.uiManager.view(forReactTag: self.tag) {
                let reactNativeVC: UIViewController? = view.reactViewController()
                reactNativeVC?.popupAlert(title: nil, message: Constants.contactSelectError, actionTitles: [Constants.gotIt], actions:[{action1 in
                    
                    //No need to handle this action
                    
                },{action2 in
                    //No need to handle this action
                }, nil])
            }
            /*var topVC = UIApplication.shared.keyWindow?.rootViewController
            while((topVC!.presentedViewController) != nil){
                 topVC = topVC!.presentedViewController
                
                topVC!.popupAlert(title: nil, message: "Please select a contact with a valid phone number?", actionTitles: [Constants.okay], actions:[{action1 in
                        
                    },{action2 in

                    }, nil])
                
            }*/
            
        }
        else
        {
            if hasListener {
                if(eventCancel == false)
                {
                    self.sendEvent(withName:"SelectEvent", body:["contactname": contactName,"contactnumber":contactNumber]);
                }
                
                
            }
            
        }

  }
    
    @objc
    func openETAContactsPage(_ reactTag:NSNumber){
        DispatchQueue.main.async {
            self.tag = reactTag
            if let view = RNViewManager.sharedInstance.getBrigde.uiManager.view(forReactTag: reactTag) {
                let reactNativeVC: UIViewController? = view.reactViewController()
                let p4 = NSPredicate(format: "phoneNumbers.@count > 0")
                let predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [p4])
                let contacVC = CNContactPickerViewController()
                AnalyticsManager.shared.logScreenView(screenName: ParameterName.eta_contact_screen)
                contacVC.delegate = self
                contacVC.predicateForEnablingContact = predicate
                
                contacVC.displayedPropertyKeys = [CNContactGivenNameKey,CNContactFamilyNameKey,CNContactPhoneNumbersKey,CNContactImageDataKey]
                reactNativeVC?.present(contacVC, animated: true, completion: nil)
                
            }
        }
    }
}

extension ContactEvent: CNContactPickerDelegate {
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
        print(contact.phoneNumbers)
        let numbers = contact.phoneNumbers.first
        let phoneNumber = (numbers?.value)?.stringValue ?? ""
        if let view = RNViewManager.sharedInstance.getBrigde.uiManager.view(forReactTag: self.tag) {
            let reactNativeVC: UIViewController? = view.reactViewController()
            reactNativeVC?.dismiss(animated: true) {
                self.onContactSelected(phoneNumber: phoneNumber, name: "\(contact.givenName) \(contact.familyName)")
            }
        }
        
    }

    func contactPickerDidCancel(_ picker: CNContactPickerViewController) {
        //sendContactEvent(contactName: "", contactNumber: "",eventCancel: true)
    }

    private func onContactSelected(phoneNumber: String, name: String) {
        
        sendContactEvent(contactName: name, contactNumber: phoneNumber,eventCancel: false)

    }
}

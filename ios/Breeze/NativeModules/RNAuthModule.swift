//
//  RNAuthModule.swift
//  Breeze
//
//  Created by VishnuKanth on 24/08/21.
//


import Foundation
import React
import Combine
@objc(RNAuthModule)
class RNAuthModule: NSObject {
    

    func getHeaders(complete: @escaping (String?) -> Void) {
        if AWSAuth.sharedInstance.isCurrentTokenExpired() {
            AWSAuth.sharedInstance.fetchCurrentSession { (_) in
                complete(AWSAuth.sharedInstance.idToken)
            } onFailure: { (failure) in
                complete(nil)
            }
        } else {
            complete(AWSAuth.sharedInstance.idToken)
        }
    }

    @objc
    func refreshIdToken(_ resolver: @escaping RCTPromiseResolveBlock, rejecter:@escaping RCTPromiseRejectBlock) {
        // Implement your logic here
        
        getHeaders { token in
            resolver(token)
        }
    }
    
    @objc
    func constantsToExport() -> [String: Any]! {
        return ["API_BASE_URL": "\(Bundle.main.infoDictionary?["Backend URL"] as? String ?? "")/"]
    }
}

//
//  RNViewManager.swift
//  Breeze
//
//  Created by VishnuKanth on 07/07/21.
//

import Foundation
import React

class RNViewManager: NSObject {
    
    private var bridge: RCTBridge?
    
    static let sharedInstance = RNViewManager()
    
    var getBrigde: RCTBridge {
        return createBridgeIfNeeded()
    }
    
    func clearBridge() {
        bridge = nil
    }
    
    //Some comment
    @discardableResult
    func createBridgeIfNeeded() -> RCTBridge {
        if bridge == nil {
            bridge = RCTBridge.init(delegate: appDelegate, launchOptions: nil)
            // Fix memory leak risk when use react navigation
            if (RCT_DEV != 0) {
                bridge?.module(for: RCTDevLoadingView.self)
            }
        }
        // Get current window. Save to appDelegate so that it will use later
       appDelegate.window = UIApplication.shared.keyWindow;

        return bridge!
    }
    
    func viewForModule(_ moduleName: String, initialProperties: [String : Any]?) -> RCTRootView {
        let viewBridge = createBridgeIfNeeded()
        let rootView: RCTRootView = RCTRootView(
            bridge: viewBridge,
            moduleName: moduleName,
            initialProperties: initialProperties)
        if #available(iOS 13.0, *) {
            rootView.backgroundColor = UIColor.systemBackground
        } else {
            rootView.backgroundColor = UIColor.white
        }
        return rootView
    }
}

//extension RNViewManager: RCTBridgeDelegate {
//    func sourceURL(for bridge: RCTBridge!) -> URL! {
//        #if DEBUG
//            return URL(string: "http://localhost:8081/index.bundle?platform=ios")
//        #else
//            return Bundle.main.url(forResource: "main", withExtension: "jsbundle")
//        #endif
////        return Bundle.main.url(forResource: "main", withExtension: "jsbundle")
//    }
//}

extension RNViewManager {
    var appDelegate: AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
}

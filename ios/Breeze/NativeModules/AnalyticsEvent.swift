//
//  AnalyticsEvent.swift
//  Breeze
//
//  Created by VishnuKanth on 22/07/21.
//

import Foundation
import React

@objc(AnalyticsEvent)
class AnalyticsEvent: RCTEventEmitter {

   var hasListener: Bool = false
   var tag:NSNumber = 0

   override func startObserving() {
     hasListener = true
   }

   override func stopObserving() {
     hasListener = false
   }
    
    override func supportedEvents() -> [String]! {
      return ["AnalyticsEvents"];
    }
    

  @objc
    func sendAnalyticsEvent(eventName:String,eventParams:[String:Any],type:String) {
     if hasListener {
        self.sendEvent(withName:"AnalyticsEvents", body:["eventname": eventName,"eventparams":eventParams,"typr":type]);
     }
  }
    
    
}


//
//  CommWithNM.swift
//  Breeze
//
//  Created by VishnuKanth on 07/07/21.
//

import Foundation
import React
import Firebase
import CoreLocation
import MapboxDirections
import MapboxMaps
import MapboxCoreMaps
import MapboxNavigation
import Turf
import UIKit
import Instabug
import SwiftyBeaver

enum CNMScreenId: String {
    case landing = "Landing"
    case tutorial = "Tutorial"
    case home = "Home"
    case notify = "Notify"
    case tripLog = "TripLog"
    case more = "More"
    case packingPriceDetail = "ParkingPriceDetail"
    case searchLocation = "SearchLocation"
    case rnSearchLocation = "RNSearchLocation"
    case favoriteList = "FavouriteList"
    
    case etaScreen = "ETAScreen"
    case etaFavoriteList = "ETAFavouriteList"
    case etaFavoriteSelect = "ETAFavouriteSelect"
    
    case userPreference = "UserPreference"
    case dateRangePicker = "DateRangePicker"
    case nearbyParkingFees = "NearbyParkingFees"
    case packingCalculator = "ParkingCalculator"
    
    case erp = "ERP"
    case erpDetail = "ErpDetail"
    
    case expressWayDetail = "ExpressWayDetail"
    case favoriteEditInfo = "FavouriteEditInfo"
    case ShortcutInfoEditor = "ShortcutInfoEditor"
    case favoriteDelete = "FavouriteDelete"
    
    case messageList = "MessageList"
    
    case createAccount = "createAccount"
    case signin = "signIn"
    case shareCollectionScreen = "ShareCollectionScreen"
    case shareLocationScreen = "SharedLocation"
    case collectionDetail = "CollectionDetails"
    case obuDisplay = "OBUDisplay"
    case obuStepConnect = "ConnectOBUDevice"
}

@objc(CommunicateWithNativeModule)
class CommunicateWithNativeModule: NSObject {
    
    var tag:NSNumber = 0
    
    
    /*
     * Send a message from React Native to native when React Native is running
     */
    @objc
    func sendMessageToNative(_ rnMessage: [String:Any], screenName:String ) {
        
        if(screenName == "ETAFavouriteSelect")
        {
            DispatchQueue.global(qos: .background).async {
                
                ETAFavList.shared.getETAFavList() { [weak self] (result) in
                    guard self != nil else { return }
                    //                print(result)
                    switch result {
                    case .success(let json):
                        DispatchQueue.main.async {
                            SavedSearchManager.shared.etaFav = json
                            NotificationCenter.default.post(name: Notification.Name(Values.NotificationETASelect), object: nil,userInfo: rnMessage)
                            
                            
                        }
                        
                        
                    case .failure(_):
                        DispatchQueue.main.async {
                            //                            if(SavedSearchManager.shared.etaFav.count > 0)
                            //                            {
                            //                                SavedSearchManager.shared.etaFav.removeAll()
                            //
                            //                            }
                            
                        }
                    }
                }
            }
            
            
        }
        else if(screenName == "FavouriteDelete")
        {
            DispatchQueue.main.async {
                
                let favID = rnMessage["addressid"] as! Int
                let itemIndex = SavedSearchManager.shared.findEBEntryWithID(addressID: favID)
                
                if(itemIndex >= 0)
                {
                    SavedSearchManager.shared.easyBreezy.remove(at: itemIndex)
                }
            }
        }
        else
        {
            //  No longer use setup ETA
            //            NotificationCenter.default.post(name: Notification.Name(Values.NotificationETASetUp), object: nil,userInfo: rnMessage)
        }
        
        //print("This log is from swift: \(rnMessage)")
        // You can do other logic here based on message content
    }
    
    @objc
    func startETANative(_ rnMessage: [String:Any]){
        
        NotificationCenter.default.post(name: Notification.Name(Values.NotificationETASelect), object: nil,userInfo: rnMessage)
    }
    
    @objc
    func getValueFromInfoPlist(_ key: NSString, callback: @escaping RCTResponseSenderBlock) {
        let value = Bundle.main.infoDictionary?[key as String] as? String
        callback([NSNull(), value])
    }
    
    @objc
    func getAnalyticLogPopUpEvent(_ screenName: String, popupName: String, eventValue: String)  {
        AnalyticsManager.shared.logPopUpActionEvent(popupName: popupName, eventName: eventValue, screenName: screenName)
    }
    
    @objc
    func getAnalyticEventsFromRN(_ eventName:String,eventValue:String,screenName:String)
    {
        /*if(eventName == EventName.userEditEvent)
         {
         AnalyticsManager.shared.logEditEvent(eventValue: eventValue, screenName: screenName)
         }
         
         if(eventName == EventName.userClickEvent){
         
         AnalyticsManager.shared.logClickEvent(eventValue: eventValue, screenName: screenName)
         }
         
         if(eventName == AnalyticsEventScreenView)
         {
         AnalyticsManager.shared.logScreenView(screenName: screenName)
         }*/
        getAnalyticEventsFromRN(eventName, eventValue: eventValue, screenName: screenName, saveToLocal: false)
    }
    
    @objc
    func getAnalyticEventsFromRN(_ eventName:String,eventValue:String,screenName:String, saveToLocal: Bool, extraData: NSDictionary? = nil) {
        AnalyticsManager.shared.logEvent(eventName: eventName, eventValue: eventValue, screenName: screenName, saveToLocal: saveToLocal, amenityId: ParameterName.amenityId, layerCode: ParameterName.layerCode, amenityType: ParameterName.amenityType)
    }
    
    @objc
    func getAnalyticEventsFromRNForScreenView(_ screenName:String)
    {
        AnalyticsManager.shared.logScreenView(screenName: screenName)
    }
    
    @objc
    func getAnalyticLogOpenPopUpEvent(_ screenName:String,popupName:String)
    {
        AnalyticsManager.shared.logOpenPopupEvent(popupName: popupName, screenName: screenName)
    }
    
    @objc
    func getAnalyticLogClosePopUpEvent(_ screenName:String,popupName:String)
    {
        AnalyticsManager.shared.logClosePopupEvent(popupName: popupName, screenName: screenName)
    }
    
    @objc
    func sendLocationDataAnalytics(_ eventValue: String, screenName: String,
                                   locationName: String, latitude: String, longitude: String)
    {
        AnalyticsManager.shared.sendLocationData(eventValue: eventValue, screenName: screenName, locationName: locationName, latitude: latitude, longitude: longitude)
    }
    
    /*
     * Save a callback to in native, call it later in your following logic
     */
    @objc
    func sendCallbackToNative(_ rnCallback: RCTResponseSenderBlock) {
        rnCallback(["A greeting from swift"])
    }
    
    @objc
    func selectAmenity(_ amenityName:String,isSelected:Bool){
        
        let amenityDetails = ["amenity":amenityName,"isSelected":isSelected] as [String : Any]
        
        NotificationCenter.default.post(name: Notification.Name(Values.handleAmenitySelection), object: nil,userInfo: amenityDetails)
        
        DispatchQueue.main.async {
            if let mapLandingVC = appDelegate().getMapLandingVCInTop() {
                mapLandingVC.didDeselectAmenityAnnotation()
            }
        }
        
    }
    
    @objc
    func handleSearchLocationResult(_ location: NSDictionary?){
        
        guard let location = location else { return }
        DispatchQueue.global(qos: .background).async {
            
            print("Location Received")
            DispatchQueue.main.async {
                
                let latStr = location["lat"] as? NSString ?? "0.0" //Keep default to 0.0
                let lat = latStr.doubleValue
                
                let longStr = location["long"] as? NSString ?? "0.0" //Keep default to 0.0
                let long = longStr.doubleValue
                let destName = location["address1"] as? NSString ?? ""
                let address = location["address2"] as? NSString ?? ""
                let fullAddress = location["fullAddress"] as? NSString ?? ""
                let isBookmarked = location["isBookmarked"] as? Bool ??  false
                let bookmarkID = location["bookmarkId"] as? Int ??  0
                let selectedPinIndex = location["selectedPinIndex"] as? Int ?? -1
                let routablePoints = location["routablePoints"] as?  [[String: Any]] ?? []
                let placeId = location["placeId"] as? NSString ?? ""
                let availablePercentImageBand = location["availablePercentImageBand"] as? NSString ?? ""
                let availabilityCSData = location["availabilityCSData"] as? [String: Any] ?? [:]
                
                if let mapLandingVC = appDelegate().getMapLandingVCInTop() {
                    //  Handle hide OBU display when user choose a search location
                    /*
                     if let nav = mapLandingVC.navigationController?.presentedViewController as? UINavigationController {
                     if let obuDisplayVC = nav.viewControllers.first as? OBUDisplayViewController {
                     obuDisplayVC.closeAction("")
                     }
                     }
                     */
                    if let vcs = mapLandingVC.navigationController?.viewControllers {
                        for vc in vcs {
                            if let obuVC = vc as? OBUDisplayViewController {
                                obuVC.closeAction("")
                                break
                            }
                        }
                    }
                    
                    mapLandingVC.didDeselectAnnotation()
                    mapLandingVC.didDeselectAmenityAnnotation()
                    let locationDetails = ["lat":lat,"long":long,"isParkingOn":mapLandingVC.parkingMode != .hide,"destName":destName,"address":address, "isBookmarked": isBookmarked,"bookmarkId": bookmarkID, "fullAddress": fullAddress, "selectedPinIndex": selectedPinIndex, "routablePoints": routablePoints, "placeId": placeId, "availablePercentImageBand": availablePercentImageBand, "availabilityCSData": availabilityCSData] as [String: Any]
                    NotificationCenter.default.post(name: Notification.Name(Values.searchLocationReceived), object: nil,userInfo: locationDetails)
                    
                    if let mapLandingModel = mapLandingVC.mapLandingViewModel{
                        
                        if(mapLandingModel.landingMode == Values.PARKING_MODE){
                            mapLandingVC.destName = destName as String
                            mapLandingVC.onParkingFromBottomSheet(parkingOn: true, showMapParkingIcon: false)
                        }
                    }
                }
            }
        }
    }
    
    @objc
    func closeOBULiteScreen() {
        DispatchQueue.main.async {
            if let mapLandingVC = appDelegate().getMapLandingVCInTop() {
                //  Handle hide OBU display when user choose a search location
                /*
                 if let nav = mapLandingVC.navigationController?.presentedViewController as? UINavigationController {
                 if let obuDisplayVC = nav.viewControllers.first as? OBUDisplayViewController {
                 obuDisplayVC.closeAction("")
                 }
                 }
                 */
                if let vcs = mapLandingVC.navigationController?.viewControllers {
                    for vc in vcs {
                        if let obuVC = vc as? OBUDisplayViewController {
                            obuVC.closeAction("")
                            break
                        }
                    }
                }
            }
        }
    }
    
    @objc
    func continueWithoutAccount() {
        DispatchQueue.main.async {
            appDelegate().confirmLoginAsGuestUser()
        }
    }
    
    @objc
    func shareCarDetailsToNative(_ carplateNumber: String, iuNumber: String) {
        DispatchQueue.main.async {
            AWSAuth.sharedInstance.carplateNumber = carplateNumber
            AWSAuth.sharedInstance.iuNumber = iuNumber
            Prefs.shared.setValueWithSub(AWSAuth.sharedInstance.cognitoId, key: .carplateNumber, value: carplateNumber)
            Prefs.shared.setValueWithSub(AWSAuth.sharedInstance.cognitoId, key: .iuNumber, value: iuNumber)
        }
    }
    
    @objc
    func getLocalUserData(_ resolver: @escaping RCTPromiseResolveBlock, rejecter:@escaping RCTPromiseRejectBlock) {
        let data = AWSAuth.sharedInstance.getLocalData()
        resolver(data)
    }
    
    @objc
    func getObuConnectionStatus(_ resolver: @escaping RCTPromiseResolveBlock, rejecter:@escaping RCTPromiseRejectBlock) {
        let data = OBUHelper.shared.stateOBU.rawValue
        resolver(data)
    }
    
    
    
    @objc
    func handleClearSearch(_ resolver: @escaping RCTPromiseResolveBlock, rejecter:@escaping RCTPromiseRejectBlock) {
        
        NotificationCenter.default.post(name: Notification.Name(Values.handleClearSearch), object: nil,userInfo: nil)
        
        DispatchQueue.main.async {
            
            if let mapLandingVC = appDelegate().getMapLandingVCInTop() {
                mapLandingVC.didDeselectAnnotation()
                mapLandingVC.didDeselectAmenityAnnotation()
                if let mapLandingModel = mapLandingVC.mapLandingViewModel{
                    
                    if(mapLandingModel.landingMode == Values.PARKING_MODE){
                        
                        mapLandingVC.destName = ""
                        mapLandingVC.clearParking()
                        mapLandingVC.onParkingFromBottomSheet(parkingOn: true, showMapParkingIcon: true)
                        
                    }
                }
                resolver(nil)
            }
        }
    }
    
    @objc
    func toggleDisplayTopObuButtons(_ isShow: Bool) {
        //  BREEZE2-1011 Remove info view
        /*
         DispatchQueue.main.async {
         NotificationCenter.default.post(name: .toggleObuDisplayTopMenu, object: isShow)
         }
         */
    }
    
    @objc
    func toggleMapViewOBU(_ isShowingMap: Bool) {
        //  BREEZE2-1011 Remove info view
        /*
         DispatchQueue.main.async {
         NotificationCenter.default.post(name: .toggleMapViewOBU, object: isShowingMap)
         }*/
    }
    
    @objc
    func selectCarparkFromCarparkList(_ carpark: NSDictionary?) {
        DispatchQueue.main.async {
            NotificationCenter.default.post(name: .selectCarparkFromObuDisplay, object: carpark)
            
        }
    }
    
    @objc
    func openCarkParkList(_ isOpen: Bool){
        DispatchQueue.main.async {
            if let mapLandingVC = appDelegate().getMapLandingVCInTop() {
                if mapLandingVC.mapLandingViewModel?.landingMode == Values.PARKING_MODE || mapLandingVC.mapLandingViewModel?.landingMode == Values.OBU_MODE {
                    mapLandingVC.playFlipAnimation(isOpen)
                    mapLandingVC.mapLandingViewModel?.isShowingMap = !isOpen
                }else {
                    if mapLandingVC.mapLandingViewModel?.isShowingMap == false {
                        mapLandingVC.playFlipAnimation(false)
                        mapLandingVC.mapLandingViewModel?.isShowingMap = true
                    }
                }
            }
        }
    }
    
    @objc
    func presentParkingDetail(_ carpark: NSDictionary?){
        if let dic = carpark, let carparkId = dic["parkingId"] as? String, !carparkId.isEmpty {
            DispatchQueue.main.async {
                if let mapLandingVC = appDelegate().getMapLandingVCInTop() {
                    mapLandingVC.showCarparkDetail(carparkId)
                }
            }
        }
    }
    
    @objc
    func handleSharingMapCollection(_ shareLink: String?, isSaved: Bool){
        if let shareLink = shareLink, !shareLink.isEmpty {
            DispatchQueue.main.async {
                if let mapLandingVC = appDelegate().getMapLandingVCInTop() {
                    
                    var didShowShareExtension = false
                    if let vcs = mapLandingVC.navigationController?.viewControllers {
                        for vc in vcs {
                            if let obuVC = vc as? OBUDisplayViewController {
                                didShowShareExtension = true
                                obuVC.shareActivityMapCollection(shareLink, isSaved: isSaved)
                                break
                            }
                        }
                    }
                    if !didShowShareExtension {
                        mapLandingVC.shareActivityMapCollection(shareLink, isSaved: isSaved)
                    }
                    
                    /*
                     if let nav = mapLandingVC.navigationController?.presentedViewController as? UINavigationController {
                     if let obuDisplayVC = nav.viewControllers.first as? OBUDisplayViewController {
                     obuDisplayVC.shareActivityMapCollection(shareLink)
                     } else {
                     mapLandingVC.shareActivityMapCollection(shareLink)
                     }
                     } else {
                     mapLandingVC.shareActivityMapCollection(shareLink)
                     }
                     */
                }
            }
        }
    }
    
    @objc
    func cancelParkingAvailable(){
        DispatchQueue.main.async {
            NotificationCenter.default.post(name: .cancelParkingAvailable, object: nil)
        }
    }
    
    @objc
    func startWalkingPath(){
        DispatchQueue.main.async {
            NotificationCenter.default.post(name: .startWalkingPath, object: nil)
        }
    }
    
    @objc
    func backToHomePage(){
        DispatchQueue.main.async {
            NotificationCenter.default.post(name: .backToHomePage, object: nil)
        }
    }
    
    @objc
    func switchMode(_ landingMode:String){
        
        
        //        let lat: Double = 1.3854
        //        let lon: Double = 103.909
        //        let sourceLoc = CLLocationCoordinate2D(latitude: lat, longitude: lon)
        //
        //
        //        let lat2: Double = 1.4068
        //        let lon2: Double = 103.9025
        //        let destLoc = CLLocationCoordinate2D(latitude: lat2, longitude: lon2)
        //
        //
        //        let directionsService = DirectionService.shared.getSourceandDestination(source:sourceLoc,destination:destLoc)
        //
        
        DispatchQueue.main.async {
            if let mapLandingVC = appDelegate().getMapLandingVCInTop() {
                
                mapLandingVC.triggerAppUpdater()
                
//                let isSameMode = mapLandingVC.mapLandingViewModel?.landingMode == landingMode
//                if !isSameMode {
                    if landingMode == Values.PARKING_MODE {
                                            
                        mapLandingVC.didDeselectAnnotation()
                        mapLandingVC.didDeselectAmenityAnnotation()
                        mapLandingVC.resumeParking()
                        mapLandingVC.HideAndShowExploreView(show: true)
                        AmenitiesSharedInstance.shared.isUpdateCarParkLayer = false
                        mapLandingVC.onParkingFromBottomSheet(parkingOn: true, showMapParkingIcon: true)
                        if let mapLandingModel = mapLandingVC.mapLandingViewModel{
                            mapLandingVC.prevCarParks = nil
                            mapLandingModel.landingMode = Values.PARKING_MODE
                            if let mobileViewUpdatable = mapLandingModel.mobileMapViewUpdatable{
                                mobileViewUpdatable.navigationMapView?.mapView.removeCarparkLayer()
                                if mapLandingVC.currentGlobalNotification == nil && !mapLandingModel.isEventFromInbox {
                                    mapLandingModel.recenter()
                                }
                                mapLandingVC.isCameraTapped = false
                                mapLandingVC.didDeselectAmenityAnnotation()
                                mobileViewUpdatable.removeAmenitiesFromMap(type: Values.TRAFFIC)
                                mapLandingModel.removeERPLayerFromMap()
                            }
                            if mapLandingVC.currentGlobalNotification == nil && !mapLandingModel.isEventFromInbox {
                                mapLandingModel.getAmenitiesDataIfLocationHasChanged()
                            }
                        }
                    }
                    else if landingMode == Values.ERP_MODE{
                        
                        mapLandingVC.removeTrafficNotificationFromMap()
                        mapLandingVC.didDeselectAnnotation()
                        mapLandingVC.didDeselectAmenityAnnotation()
                        mapLandingVC.HideAndShowExploreView(show: false)
                        if let mapLandingModel = mapLandingVC.mapLandingViewModel{
                            mapLandingModel.landingMode = Values.ERP_MODE
                            mapLandingVC.onParkingFromBottomSheet(parkingOn: false, showMapParkingIcon: false)
                            mapLandingModel.removeAmenitiesFromMap()
                            if let mobileViewUpdatable = mapLandingModel.mobileMapViewUpdatable{
                                mapLandingModel.updateERPOnMap()
                                mapLandingVC.isCameraTapped = false
                                mobileViewUpdatable.removeAmenitiesFromMap(type: Values.TRAFFIC)
                                
                            }
                            
                        }
                        AmenitiesSharedInstance.shared.isUpdateCarParkLayer = false
                    }
                    else if landingMode == Values.TRAFFIC_MODE {
                        
                        mapLandingVC.removeTrafficNotificationFromMap()
                        mapLandingVC.didDeselectAnnotation()
                        mapLandingVC.didDeselectAmenityAnnotation()
                        mapLandingVC.HideAndShowExploreView(show: false)
                        if let mapLandingModel = mapLandingVC.mapLandingViewModel{
                            mapLandingModel.landingMode = Values.TRAFFIC_MODE
                            mapLandingModel.removeERPLayerFromMap()
                            mapLandingModel.removeAmenitiesFromMap()
                        }
                        
                        AmenitiesSharedInstance.shared.isUpdateCarParkLayer = false
                        mapLandingVC.onParkingFromBottomSheet(parkingOn: false, showMapParkingIcon: false)
                        
                    }
                    else if(landingMode == Values.NONE_MODE || landingMode == Values.OBU_MODE) {
                        mapLandingVC.didDeselectAnnotation()
                        mapLandingVC.didDeselectAmenityAnnotation()
                        if let mapLandingModel = mapLandingVC.mapLandingViewModel{
                            mapLandingVC.prevCarParks = nil
                            mapLandingModel.landingMode = Values.OBU_MODE
                            if let mobileViewUpdatable = mapLandingModel.mobileMapViewUpdatable{
                                mobileViewUpdatable.navigationMapView?.mapView.removeCarparkLayer()
                                if mapLandingVC.currentGlobalNotification == nil && !mapLandingModel.isEventFromInbox {
                                    mapLandingModel.recenter()
                                }
                                mapLandingVC.isCameraTapped = false
                                mapLandingVC.didDeselectAmenityAnnotation()
                                mobileViewUpdatable.removeAmenitiesFromMap(type: Values.TRAFFIC)
                                mapLandingModel.removeERPLayerFromMap()
                            }
                            mapLandingVC.didDeselectAnnotation()
                            mapLandingVC.didDeselectAmenityAnnotation()
                            mapLandingVC.resumeParking()
                            mapLandingVC.HideAndShowExploreView(show: true)
                            AmenitiesSharedInstance.shared.isUpdateCarParkLayer = false
                            if mapLandingVC.currentGlobalNotification == nil && !mapLandingModel.isEventFromInbox {
                                mapLandingModel.getAmenitiesDataIfLocationHasChanged()
                            }
                        }
                    }
//                }
            }
        }
        
    }
    
    @objc
    func initUserPreferenceSetting(_ navigationParams:NSDictionary?)
    {
        print(navigationParams)
        let preferences = ["preferences":navigationParams]
        
        print("Cognito ID: \(AWSAuth.sharedInstance.cognitoId)")
        
        //  Save user preference to use in local in case user launches app from Carplay
        if let prefs = navigationParams {
            Prefs.shared.saveUserPreferenceWithSub(AWSAuth.sharedInstance.cognitoId, preference: prefs)
        }
        
        NotificationCenter.default.post(name: Notification.Name(Values.handleAmenitySelection), object: nil,userInfo: preferences as [AnyHashable : Any])
    }
    
    @objc
    func showRouteToLocation (_ reactTag: NSNumber, selectedIndex:NSNumber, location: NSDictionary?) {
        guard let location = location else { return }
        
        let type = location["type"] as? String ?? ""
        
        DispatchQueue.main.async {
            
            var lat = location["lat"] as? String ?? ""
            var long = location["long"] as? String ?? ""
            
            var carparkId: String = location["carparkId"] as? String ?? ""
            if let dic = location as? [AnyHashable : Any] {
                if let routablePoint = parseSelectedRoutablePoint(dic) {
                    lat = routablePoint.lat?.toString() ?? ""
                    long = routablePoint.long?.toString() ?? ""
                    carparkId = routablePoint.carparkId ?? ""
                }
            }
            let placeId = location["placeId"] as? String ?? ""
            
            var address:SearchAddresses?
            var ebAddress:EasyBreezyAddresses?
            if(type == "B")
            {
                let addressid = location["addressid"] as? NSNumber == nil ? 0 : (location["addressid"] as! NSNumber).intValue
                ebAddress = EasyBreezyAddresses.init(addressid: addressid, lat: lat, long: long, alias: location["name"] as? String ?? "", address1: location["address1"] as? String ?? "", address2: location["address2"] as? String ?? "", carparkId: carparkId, fullAddress: location["fullAddress"] as? String ?? "", isCrowsourceParking: true, placeId: placeId)
            }
            else
            {
                address = SearchAddresses.init(alias: "", address1:  location["address1"] as? String ?? "", lat:  lat, long:  long, address2:location["address2"] as? String ?? "", distance:type == "H" ? "" : location["distance"] as? String ?? "", carparkId: carparkId, fullAddress: location["fullAddress"] as? String ?? "", isCrowsourceParking: true, placeId: placeId)
            }
            
            if let view = RNViewManager.sharedInstance.getBrigde.uiManager.view(forReactTag: reactTag) {
                let reactNativeVC: UIViewController? = view.reactViewController()
                var isRoutePlanningFound = false
                if let navigationController = reactNativeVC?.navigationController{
                    let viewControllers: [UIViewController] = navigationController.viewControllers
                    for controller in viewControllers {
                        if let rpVC = controller as? NewRoutePlanningVC {
                            rpVC.selectionSource = type
                            rpVC.carparkId = carparkId
                            rpVC.rankingIndex = selectedIndex.intValue + 1
                            if(type == "B")
                            {
                                rpVC.originalAddress = ebAddress
                            }
                            else
                            {
                                rpVC.originalAddress = address
                            }
                            
                            rpVC.clearValuesWhileRouteRefreshes()
                            isRoutePlanningFound = true
                            rpVC.addressReceived = true
                            navigationController.popViewController(animated: true)
                            return
                        }
                    }
                }
                
                if(isRoutePlanningFound == false){
                    
                    if let navigationController = appDelegate().getMapLandingVCInTop()?.navigationController {
                        
                        let story = UIStoryboard(name: "NewRoutePlanning", bundle: nil)
                        if let vc = story.instantiateViewController(withIdentifier: String(describing: NewRoutePlanningVC.self)) as? NewRoutePlanningVC {
                            vc.selectionSource = type
                            vc.carparkId = carparkId
                            vc.rankingIndex = selectedIndex.intValue + 1
                            
                            if(type == "B")
                            {
                                vc.originalAddress = ebAddress
                                //let number = location!["addressid"] as! NSNumber
                                //vc.addressID = number.stringValue
                            }
                            else
                            {
                                vc.originalAddress = address
                            }
                            vc.addressReceived = true
                            vc.modalPresentationStyle = .overFullScreen
                            navigationController.pushViewController(vc, animated: true)
                        }
                    }
                }
            }
            /*
             //            let story = UIStoryboard(name: "RoutePlanning", bundle: nil)
             //            if let vc = story.instantiateViewController(withIdentifier: String(describing: RoutePlanningVC.self)) as? RoutePlanningVC {
             
             vc.selectionSource = location!["type"] as! String
             vc.rankingIndex = selectedIndex.intValue + 1
             if(location!["type"] as! String == "B")
             {
             vc.originalAddress = ebAddress
             let number = location!["addressid"] as! NSNumber
             vc.addressID = number.stringValue
             }
             else
             {
             vc.originalAddress = address
             }
             
             
             //Is there a way to change this to Push instead of present
             if let view = RNViewManager.sharedInstance.bridge?.uiManager.view(forReactTag: reactTag) {
             let reactNativeVC: UIViewController! = view.reactViewController()
             vc.modalPresentationStyle = .overFullScreen
             reactNativeVC.navigationController?.pushViewController(vc, animated: true)
             
             }
             }*/
            
            
            
        }
        
        print("searchLocationResult \(reactTag) \(location) \(selectedIndex)")
    }
    
    /*
     * Finish React native screens, back to native
     * parameter reactTag: opaque identifier of current React Native view controller
     */
    @objc
    func backToNative (_ reactTag: NSNumber) {
        DispatchQueue.main.async {
            if let view = RNViewManager.sharedInstance.getBrigde.uiManager.view(forReactTag: reactTag) {
                let reactNativeVC: UIViewController? = view.reactViewController()
                reactNativeVC?.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    
    
    /**
     * Open native screen with some params
     */
    @objc
    func openNativeScreen(_ reactTag: NSNumber,
                          screenName: NSString,
                          navigationParams: NSDictionary?) {
        
        if let screenID = CNMScreenId(rawValue: screenName as String) {
            switch screenID {
            case .signin, .createAccount, .collectionDetail, .obuDisplay, .obuStepConnect:
                appDelegate().processOpenScreen(screenId: screenID, navigationParams: navigationParams)
            case .favoriteEditInfo:
                DispatchQueue.main.async {
                    self.openFavEditScreen(reactTag, navigationParams: navigationParams)
                }
            case .ShortcutInfoEditor:
                DispatchQueue.main.async {
                    self.openShortCutScreen(reactTag, navigationParams: navigationParams)
                }
            default:
                break
            }
        }
        
        /* if(screenName == "FavouriteEditInfo")
         {
         self.openFavEditScreen(reactTag, navigationParams: navigationParams)
         }
         else */ if(screenName == "MapLanding"){
             
             DispatchQueue.main.async {
                 
                 let storyboard = UIStoryboard(name: "MapsLandingVC", bundle: nil)
                 let vc = storyboard.instantiateViewController(withIdentifier: "mainMap") as UIViewController
                 if let view = RNViewManager.sharedInstance.getBrigde.uiManager.view(forReactTag: reactTag) {
                     if let reactNativeVC = view.reactViewController() {
                         reactNativeVC.dismiss(animated: false)
                         reactNativeVC.navigationController?.pushViewController(vc, animated: true)
                     }
                 }
             }
         } else if screenName == "TripLog" {
             var data: [String: Any] = [Values.OpenTripLogKey: true]
             data["navigationParams"] = navigationParams
             /* Navigation params example
              {
              "selectedWeek": "11-17/9", yyyyMMdd-yyyyMMdd
              "tripLogTab": "history" | "transactions",
              "vehicleNumber": "1234",
              "filter": "erp" | "parking",
              }
              */
             NotificationCenter.default.post(name: Notification.Name(Values.NotificationOpenTripLog), object: nil,userInfo: data)
             
         } else if screenName == "More" {
             let data = [Values.OpenMoreKey:true]
             NotificationCenter.default.post(name: Notification.Name(Values.NotificationOpenMore), object: nil,userInfo: data)
         } else if screenName == "ParkingRoutePlanning" {
             
             let data = [Values.ParkingIdKey:navigationParams![Values.ParkingIdKey] as! String]
             let lat = navigationParams!["lat"] as! Double
             let long = navigationParams!["long"] as! Double
             let carParkAddress = navigationParams!["name"] as! NSString
             let parkingID = navigationParams!["parkingID"] as! NSString
             
             let carParkInfo = ["lat":lat,"long":long,"name":carParkAddress,"parkingID":parkingID] as [String : Any]
             NotificationCenter.default.post(name: Notification.Name(Values.NotificationOpenRoutePlanning), object: nil,userInfo: carParkInfo)
         }
        
        else if screenName == "Settings" {
            
            let data = [Values.settingsKey:true]
            NotificationCenter.default.post(name: Notification.Name(Values.NotificationOpenSettings), object: nil,userInfo: data)
        }
        else if screenName == "ERPDetailsPlanTrip"{
            
            NotificationCenter.default.post(name: Notification.Name(Values.NotificationOpenRPFromERPDetails), object: nil,userInfo: nil)
        } else if String(screenName) == NativeScreenNames.MAP_DISPLAY {
            NotificationCenter.default.post(name: Notification.Name(Values.NotificationOpenMapSettings), object: nil,userInfo: nil)
        }
        
        else if screenName == "Explore"{
            
            DispatchQueue.main.async {
                let storybard = UIStoryboard(name: "ContentVC", bundle: nil)
                if let vc = storybard.instantiateViewController(withIdentifier: String(describing: ContentVC.self)) as? ContentVC {
                    if let navigationParams = navigationParams {
                        vc.data = navigationParams["data"] as? String
                    }
                    
                    if let navigationController = appDelegate().getMapLandingVCInTop()?.navigationController {
                        navigationController.pushViewController(vc, animated: true)
                    }
                }
            }
        }
    }
    
    func openShortCutScreen(_ reactTag:NSNumber,navigationParams:NSDictionary?) {
        if let bookmark = Bookmark.bookmark(from: navigationParams?.object(forKey: "location") as? NSDictionary),
           let lat = bookmark.lat,
           let long = bookmark.long {
            let address = bookmark.address1 ?? ""
            let fullAddress = bookmark.address2 ?? ""
            
            let strDate = DateUtils.shared.getTimeDisplay(dateFormat: Date.defaultFormat, date: Date())
            
            let savedPlace = SavedPlaces(placeName: address, address: fullAddress, lat: lat, long: long, time: strDate)
            
            let storyboard = UIStoryboard(name: "EasyBreezy", bundle: nil)
            if let vc = storyboard.instantiateViewController(withIdentifier: "SaveEasyBreezyVC") as? SaveEasyBreezyVC {
                vc.place = savedPlace
                vc.ebName = bookmark.name
                vc.bookmark = bookmark
                if let view = RNViewManager.sharedInstance.getBrigde.uiManager.view(forReactTag: reactTag) {
                    let reactNativeVC: UIViewController? = view.reactViewController()
                    //vc.modalPresentationStyle = .overFullScreen
                    reactNativeVC?.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
    }
    
    func openFavEditScreen(_ reactTag:NSNumber,navigationParams:NSDictionary?) {
        if let value = navigationParams?.object(forKey: "location") as? NSMutableDictionary {
            let address = value["address1"] as! String
            let fullAddress = value["address2"] as! String
            let lat = value["lat"] as! String
            let long = value["long"] as! String
            
            let strDate = DateUtils.shared.getTimeDisplay(dateFormat: Date.defaultFormat, date: Date())
            
            let savedPlace = SavedPlaces(placeName: address, address: fullAddress, lat: lat, long: long, time: strDate)
            
            let storyboard = UIStoryboard(name: "EasyBreezy", bundle: nil)
            if let vc = storyboard.instantiateViewController(withIdentifier: "SaveEasyBreezyVC") as? SaveEasyBreezyVC {
                vc.place = savedPlace
                vc.ebName = value["name"] as? String
                if(value["addressid"] as? NSNumber != nil)
                {
                    vc.addressID = value["addressid"] as? NSNumber as! Int
                }
                
                if let view = RNViewManager.sharedInstance.getBrigde.uiManager.view(forReactTag: reactTag) {
                    let reactNativeVC: UIViewController? = view.reactViewController()
                    //vc.modalPresentationStyle = .overFullScreen
                    reactNativeVC?.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
    }
    
    @objc
    func dismissNative(_ reactTag: NSNumber) {
        DispatchQueue.main.async {
            if let view = RNViewManager.sharedInstance.getBrigde.uiManager.view(forReactTag: reactTag) {
                let reactNativeVC: UIViewController? = view.reactViewController()
                reactNativeVC?.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    @objc
    func dismissZoneDetail(){
        
        NotificationCenter.default.post(name: Notification.Name(Values.NotificationExploreTBRDetailsBack), object: nil,userInfo: nil)
    }
    
    @objc
    func getCalendarDates(_ startDate: String, endDate: String) {
        let data = [ "startDate": startDate, "endDate" : endDate]
        NotificationCenter.default.post(name: Notification.Name(Values.NotificationCalendarSelect), object: nil,userInfo: data)
    }
    
    @objc func closeNotifyArrivalScreen() {
        NotificationCenter.default.post(name: Notification.Name(Values.NotificationCloseNotifyArrivalScreen), object: nil,userInfo: nil)
    }
    
    @objc
    func closeTripLogScreen() {
        let data = [Values.OpenTripLogKey:false]
        NotificationCenter.default.post(name: Notification.Name(Values.NotificationOpenTripLog), object: nil,userInfo: data)
    }
    
    @objc func getUserLocation(_ callback: @escaping RCTResponseSenderBlock) {
        
        let location = ["lat": LocationManager.shared.location.coordinate.latitude.toString(),"long":LocationManager.shared.location.coordinate.longitude.toString()]
        callback([location])
    }
    
    /// after user preferences settings has been changed, this function will be called from RN
    @objc func updateUserPreferenceSettings(_ settings: NSDictionary?) {
        NotificationCenter.default.post(name: Notification.Name(Values.userPreferenceSettingsUpdated), object: nil,userInfo: settings as? [AnyHashable : Any])
    }
    
    /// Get user preference settings by sending **masterlists** and **settings** to RN
    @objc func getUserPreferenceSettings(_ callback: @escaping RCTResponseSenderBlock) {
        
        guard let master = UserSettingsModel.sharedInstance.master, let userSettings = UserSettingsModel.sharedInstance.userSettings else {
            return
        }
        
        let masterjson = master.toJson()
        let userjson = userSettings.toJson()
        let dictionary = ["settings_mapper": masterjson, "settings_user": userjson]
        callback([dictionary])
    }
    
    func fetchCurrentLocationFromMapLanding() -> [String:String]{
        var currentLocation = [String:String]()
        if let mapLandingVC = appDelegate().getMapLandingVCInTop() {
            if let mapLandingModel = mapLandingVC.mapLandingViewModel{
                
                if mapLandingModel.isSearchDisocver, let location = mapLandingModel.searchDiscoverLocation {
                    
                    currentLocation = ["lat": location.latitude.toString(),"long":location.longitude.toString()]
                    
                }
                else{
                    
                    if SelectedProfiles.shared.isProfileSelected(){
                        if let profileLocation = SelectedProfiles.shared.getOuterZoneCentrePoint(){
                            
                            currentLocation = ["lat": profileLocation.latitude.toString(),"long":profileLocation.longitude.toString()]
                            
                        }
                    }
                    else{
                        
                        currentLocation = ["lat": LocationManager.shared.location.coordinate.latitude.toString(),"long":LocationManager.shared.location.coordinate.longitude.toString()]
                        
                    }
                    
                }
            }
        }
        
        return currentLocation
    }
    
    // BREEZES-8546: Logic to get current location in parking calculator
    @objc
    func getLandingFocusedLocation(_ mode: String, callback: @escaping RCTResponseSenderBlock) {
        DispatchQueue.main.async {
            if let mapLandingVC = appDelegate().getMapLandingVCInTop() {
                if let mapLandingModel = mapLandingVC.mapLandingViewModel {
                    if (mode == Values.NONE_MODE || mode == Values.OBU_MODE) {
                        if mapLandingVC.shouldUpdateCarparks() {
                            let location = ["lat": mapLandingVC.mapLandingViewModel?.centerMapLocation?.latitude.toString(), "long":mapLandingVC.mapLandingViewModel?.centerMapLocation?.longitude.toString()]
                            callback([location])
                        } else if mapLandingVC.mapLandingViewModel?.isSearchDisocver ?? false, let location = mapLandingVC.mapLandingViewModel?.searchDiscoverLocation {
                            let location = ["lat": location.latitude.toString(), "long": location.longitude.toString()]
                            callback([location])
                        } else {
                            self.getCurrentLocation(callback: callback)
                        }
                    } else if mode == Values.PARKING_MODE {
                        if mapLandingVC.shouldUpdateCarparks() {
                            let location = ["lat": mapLandingVC.mapLandingViewModel?.centerMapLocation?.latitude.toString(), "long":mapLandingVC.mapLandingViewModel?.centerMapLocation?.longitude.toString()]
                            callback([location])
                        } else if mapLandingModel.isSearchDisocver, let location = mapLandingModel.searchDiscoverLocation {
                            let location = ["lat": location.latitude.toString(),"long":location.longitude.toString()]
                            callback([location])
                        } else {
                            self.getCurrentLocation(callback: callback)
                        }
                    } else if mode == Values.TRAFFIC_MODE {
                        if mapLandingModel.isSearchDisocver, let location = mapLandingModel.searchDiscoverLocation {
                            let location = ["lat": location.latitude.toString(),"long":location.longitude.toString()]
                            callback([location])
                        } else {
                            self.getCurrentLocation(callback: callback)
                        }
                    } else {
                        self.getCurrentLocation(callback: callback)
                    }
                }
            }
            
            //            guard let navigationController = UIApplication.shared.windows.first?.rootViewController as? UINavigationController else { return }
            //            if let mapLandingVC = navigationController.topViewController as? MapLandingVC {
            //
            //            } else {
            //              //  This is still required if user is not in MapLanding
            //                for controller in navigationController.viewControllers {
            //                    if let mapLandingVC = controller as? MapLandingVC {
            //                        if let mapLandingModel = mapLandingVC.mapLandingViewModel{
            //
            //                            if mapLandingModel.isSearchDisocver, let location = mapLandingModel.searchDiscoverLocation {
            //
            //                                let location = ["lat": location.latitude.toString(),"long":location.longitude.toString()]
            //                                callback([location])
            //                            } else{
            //                                self.getCurrentLocation(callback: callback)
            //                            }
            //                        }
            //                        break
            //                    }
            //                }
            //            }
        }
    }
    
    func getCurrentLocation(callback: @escaping RCTResponseSenderBlock) {
        if SelectedProfiles.shared.isProfileSelected(){
            if let profileLocation = SelectedProfiles.shared.getOuterZoneCentrePoint(){
                let location = ["lat": profileLocation.latitude.toString(),"long":profileLocation.longitude.toString()]
                callback([location])
            }
        } else {
            let location = ["lat": LocationManager.shared.location.coordinate.latitude.toString(),"long":LocationManager.shared.location.coordinate.longitude.toString()]
            callback([location])
        }
    }
    
    @objc
    func closeMoreScreen() {
        let data = [Values.OpenMoreKey:false]
        NotificationCenter.default.post(name: Notification.Name(Values.NotificationOpenMore), object: nil,userInfo: data)
    }
    
    //MARK: - Traffic React methods
    @objc
    func sendTrafficCameraData(_ trafficData:NSDictionary?){
        
        // Delay for a while to make sure the camera added to the map
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            NotificationCenter.default.post(name: Notification.Name(Values.receivedTrafficData), object: nil,userInfo: trafficData as? [AnyHashable : Any])
            
            if let mapLandingVC = appDelegate().getMapLandingVCInTop() {
                mapLandingVC.openCameraToolTip()
            }
        }
    }
    
    @objc
    func selectCamera(_ trafficID:String){
        
        let data = [ "id": trafficID]
        NotificationCenter.default.post(name: Notification.Name(Values.receivedTrafficID), object: nil,userInfo: data)
        
        DispatchQueue.main.async {
            if let mapLandingVC = appDelegate().getMapLandingVCInTop() {
                mapLandingVC.openCameraToolTip()
            }
        }
    }
    
    @objc
    func openTrafficDetail(_ expressWayCode:String){
        
        print(expressWayCode)
        
        DispatchQueue.main.async {
            
            if let navVC = appDelegate().getMapLandingVCInTop()?.navigationController {
                let storyboard = UIStoryboard(name: "TrafficCameraView", bundle: nil)
                if let vc = storyboard.instantiateViewController(withIdentifier: String(describing: TrafficCameraView.self)) as? TrafficCameraView {
                    vc.expressWayCode = expressWayCode
                    navVC.pushViewController(vc, animated: true)
                }
            }
        }
    }
    
    @objc
    func handleChangeERPSelectedTime(_ erpSelectedTime:NSNumber){
        
        DispatchQueue.global(qos: .background).async {
            DispatchQueue.main.async {
                if let mapLandingVC = appDelegate().getMapLandingVCInTop() {
                    if let mapLandingModel = mapLandingVC.mapLandingViewModel{
                        
                        if mapLandingModel.landingMode == Values.ERP_MODE {
                            
                            mapLandingModel.selectedERPTime = erpSelectedTime.intValue
                        }
                    }
                }
            }
        }
    }
    
    @objc
    func searchERPRoutes(_ source:NSDictionary?, destination: NSDictionary?, timestamp: NSNumber, callback: @escaping RCTResponseSenderBlock) {
        
        guard let source = source, let destination = destination else { return }
        
        let srcLat = source["lat"] as? Double ?? 0.0
        let srcLong = source["long"] as? Double ?? 0.0
        let destLat = destination["lat"] as? Double ?? 0.0
        let destLong = destination["long"] as? Double ?? 0.0
        
        let srcAddress1 = source["address1"] as? String ?? ""
        let srcAddress2 = source["address2"] as? String ?? ""
        
        let destAddress1 = destination["address1"] as? String ?? ""
        let destAddress2 = destination["address2"] as? String ?? ""
        
        let id = hashID(sourceLat: srcLat, sourceLong: srcLong, destinationLat: destLat, destinationLong: destLong, timestamp: timestamp.intValue)
        
        if let fullData = DirectionService.shared.erpFullRouteData {
            if id == fullData.id {
                // every thing is the same. return this cached data
                if let dic = fullData.dictionary {
                    callback([dic])
                }
                DirectionService.shared.timeStamp = timestamp.intValue
                return
            }
            
            // check if the the source and destination is the same
            if fullData.srcLat == srcLat && fullData.srcLong == srcLong && fullData.destLat == destLat && fullData.destLong == destLong && timestamp.intValue == DirectionService.shared.timeStamp {
                
                // update ERP info
                if let response = fullData.routeResponse {
                    if let erpData = self.updateERP(id:id,srcLat: srcLat, srcLong: srcLong, destLat: destLat, destLong: destLong,destAddress1: destAddress1,destAddress2: destAddress2,srcAddress1: srcAddress1,srcAddress2: srcAddress2, response: response,timeStamp: timestamp.intValue) {
                        if let dic = erpData.dictionary {
                            callback([dic])
                        }
                        // Save the erpData
                        DirectionService.shared.timeStamp = timestamp.intValue
                        DirectionService.shared.erpFullRouteData = erpData
                    }
                }
                
                return
            }
            
        }
        
        // it's new or source/destination is different
        let sourceLoc = CLLocationCoordinate2D(latitude: source["lat"] as! CLLocationDegrees, longitude: source["long"] as! CLLocationDegrees)
        let destLoc = CLLocationCoordinate2D(latitude: destination["lat"] as! CLLocationDegrees, longitude: destination["long"] as! CLLocationDegrees)
        
        // get route response
        DirectionService.shared.getRouteResponse(source: sourceLoc, destination: destLoc, completion: { response, routeType in
            if let erpData = self.updateERP(id:id,srcLat: srcLat, srcLong: srcLong, destLat: destLat, destLong: destLong,destAddress1: destAddress1,destAddress2: destAddress2,srcAddress1: srcAddress1,srcAddress2: srcAddress2, response: response,timeStamp: timestamp.intValue) {
                if let dic = erpData.dictionary {
                    callback([dic])
                }
                // Save the erpData
                DirectionService.shared.timeStamp = timestamp.intValue
                DirectionService.shared.erpFullRouteData = erpData
            }
        })
    }
    
    private func updateERP(id: String, srcLat: Double, srcLong: Double, destLat: Double, destLong: Double,destAddress1:String,destAddress2:String,srcAddress1:String,srcAddress2:String, response: RouteResponse,timeStamp:Int) -> ERPFullRouteData? {
        if var erpData = getERPInformationBetweenRoutes(in: response.routes ?? [], from: DataCenter.shared.getAllERPFeatureCollection(), id: id,timeStamp: timeStamp) {
            
            erpData.routeResponse = response
            erpData.srcLat = srcLat
            erpData.srcLong = srcLong
            erpData.srcAddress1 = srcAddress1
            erpData.srcAddress2 = srcAddress2
            erpData.destLat = destLat
            erpData.destLong = destLong
            erpData.destAddress1 = destAddress1
            erpData.destAddress2 = destAddress2
            //            if let dic = erpData.dictionary {
            //                callback([dic])
            //            }
            return erpData
        }
        return nil
    }
    
    @objc
    func selectERPRoute(_ id:String, routeId: String,erpTimeChanged: Bool) {
        
        ///  null check for erpFullRouteData
        if let data = DirectionService.shared.erpFullRouteData {
            
            /// check if id saved in memory is equal to id sent by RN
            if(data.id == id){
                
                /// convert string routeID sent  by RN to Int
                if let routeIndex = Int(routeId){
                    
                    /// check erpFullRoute has any routeResponse
                    if let routes = data.routeResponse?.routes{
                        
                        /// then get the routeObject from routeResponse routes based on routeIndex sent by RN
                        let route = routes[routeIndex]
                        
                        /// get index value from data erpRoutes based on routeID sent by RN
                        if let index = data.erpRoutes.firstIndex(where: { $0.routeId == routeId }) {
                            
                            /// get first erpID and send it to ERPDetails screen
                            if(data.erpRoutes[index].erpCharges.count > 0){
                                
                                let erpID = Int(data.erpRoutes[index].erpCharges[0].erpId)
                                DispatchQueue.main.async {
                                    
                                    if let navigationController = appDelegate().getMapLandingVCInTop()?.navigationController {
                                        let erpCollection = getERPDetailsFeature(id: id, routeId: routeId, timeStamp: DirectionService.shared.timeStamp)
                                        
                                        let storyboard = UIStoryboard(name: "ERPDetails", bundle: nil)
                                        if let vc = storyboard.instantiateViewController(withIdentifier: String(describing: ERPDetails.self)) as? ERPDetails {
                                            vc.selectedERPId = erpID ?? -1
                                            vc.selectedRoute = route
                                            vc.selectedRouteIndex = routeIndex
                                            vc.selectedERPTime = DirectionService.shared.timeStamp
                                            vc.erpFeatureCollection = erpCollection
                                            vc.selectedTimeChanged = erpTimeChanged
                                            navigationController.pushViewController(vc, animated: true)
                                        }
                                    }
                                }
                            }
                            else{
                                
                                DispatchQueue.main.async {
                                    if let navigationController = appDelegate().getMapLandingVCInTop()?.navigationController {
                                        let storyboard = UIStoryboard(name: "ERPDetails", bundle: nil)
                                        if let vc = storyboard.instantiateViewController(withIdentifier: String(describing: ERPDetails.self)) as? ERPDetails {
                                            vc.selectedRoute = route
                                            vc.selectedRouteIndex = routeIndex
                                            vc.selectedERPTime = DirectionService.shared.timeStamp
                                            vc.erpFeatureCollection = FeatureCollection(features: [])
                                            vc.selectedTimeChanged = erpTimeChanged
                                            navigationController.pushViewController(vc, animated: true)
                                        }
                                    }
                                }
                                
                            }
                            
                        }
                        
                    }
                    
                    
                }
                
            }
            print("\(id) \(routeId) \(data.routeResponse)")
        }
        
    }
    
    @objc
    func adjustRecenter(_ y: NSNumber,index:NSNumber,fromScreen:String) {
        let data = [Values.AdjustRecentKey:y.intValue,Values.BottomSheetIndex:index.intValue,Values.BottomSheetFromScreen:fromScreen] as [String : Any]
        NotificationCenter.default.post(name: Notification.Name(Values.NotificationAdjustRecenter), object: nil,userInfo: data)
        NotificationCenter.default.post(name: Notification.Name(Values.NotificationAdjustRecenterCollection), object: nil,userInfo: data)
    }
    
    @objc
    func adjustRecenterMapExplore(_ y: NSNumber,index:NSNumber,fromScreen:String) {
        let data = [Values.AdjustRecentKey:y.intValue,Values.BottomSheetIndex:index.intValue,Values.BottomSheetFromScreen:fromScreen] as [String : Any]
        NotificationCenter.default.post(name: Notification.Name(Values.NotificationAdjustRecenterExploreMap), object: nil,userInfo: data)
    }
    
    @objc
    func openERPDetail(_ erpId: NSNumber) {
        
        DispatchQueue.main.async {
            
            if let navigationController = appDelegate().getMapLandingVCInTop()?.navigationController {
                let erpCollection = FeatureCollection(features: findERPBasedOnERPId(erpId.intValue, timeStamp: DirectionService.shared.timeStamp == 0 ? Int(Date().currentTimeInMiliseconds()) :DirectionService.shared.timeStamp))
                
                let storyboard = UIStoryboard(name: "ERPDetails", bundle: nil)
                if let vc = storyboard.instantiateViewController(withIdentifier: String(describing: ERPDetails.self)) as? ERPDetails {
                    vc.selectedERPId = erpId.intValue
                    vc.selectedERPTime = DirectionService.shared.timeStamp == 0 ? Int(Date().currentTimeInMiliseconds()) :DirectionService.shared.timeStamp
                    vc.erpFeatureCollection = erpCollection
                    navigationController.pushViewController(vc, animated: true)
                }
            }
        }
    }
    
    @objc
    func showNoCarparkAlert() {
        DispatchQueue.main.async {
            NotificationCenter.default.post(name: .onNoDataNotification, object: 0)
        }
    }
    
    @objc
    func closeCarparkListScreen() {
        DispatchQueue.main.async {
            NotificationCenter.default.post(name: .onCloseCarparkListScreen, object: nil)
        }
    }
    
    
    //    @objc
    //    func closeInviteScreen() {
    //        DispatchQueue.main.async {
    //            NotificationCenter.default.post(name: .onCloseInviteScreen, object: nil)
    //        }
    //    }
    
    @objc
    func cancelOBUCarparkAvailability() {
        DispatchQueue.main.async {
            NotificationCenter.default.post(name: .obuOnCloseCarparkAvailability, object: nil)
        }
    }
    
    @objc
    func closeLandingDropPinScreen() {
        DispatchQueue.main.async {
            if let mapVC = appDelegate().getMapLandingVCInTop() {
                mapVC.enableDropPinMode(false)
            }
        }
    }
    
    @objc
    func onSavedDropPinLanding(_ savedPinData: NSDictionary?) {
        DispatchQueue.main.async {
            if let mapVC = appDelegate().getMapLandingVCInTop() {
                mapVC.updateDropPinToolTip(savedPinData)
            }
        }
    }
    
    @objc
    func onCollectionDetailSearch(_ location: NSDictionary?){
        
        guard let location = location else { return }
        DispatchQueue.global(qos: .background).async {
            print("Location Received")
            DispatchQueue.main.async {
                let latStr = location["lat"] as? NSString ?? "0.0" //Keep default to 0.0
                let lat = latStr.doubleValue
                
                let longStr = location["long"] as? NSString ?? "0.0" //Keep default to 0.0
                let long = longStr.doubleValue
                let destName = location["address1"] as? NSString ?? ""
                let address = location["address2"] as? NSString ?? ""
                let fullAddress = location["fullAddress"] as? NSString ?? ""
                let isBookmarked = location["isBookmarked"] as? Bool ??  false
                let bookmarkID = location["bookmarkId"] as? Int ??  0
                let selectedPinIndex = location["selectedPinIndex"] as? Int ?? -1
                let routablePoints = location["routablePoints"] as?  [[String: Any]] ?? []
                
                //  TODO handle show destination search and show destination tooltip
                let locationDetails = ["lat":lat,"long":long,"destName":destName,"address":address, "isBookmarked": isBookmarked,"bookmarkId": bookmarkID, "fullAddress": fullAddress, "selectedPinIndex": selectedPinIndex, "routablePoints": routablePoints] as [String: Any]
                
                NotificationCenter.default.post(name: .onReceivedSearchLocationInCollectionDetail, object: locationDetails)
            }
        }
    }
    
    @objc
    func clearSearchCollectionDetails() {
        DispatchQueue.main.async {
            NotificationCenter.default.post(name: .onClearSearchCollectionDetails, object: nil)
        }
    }
    
    @objc
    func closeSharedCollectionDetails() {
        DispatchQueue.main.async {
            NotificationCenter.default.post(name: .onCloseShareCollectionAndLocation, object: nil)
        }
    }
    
    @objc
    func cancelOBUTravelTime() {
        DispatchQueue.main.async {
            NotificationCenter.default.post(name: .obuOnCloseTravelTime, object: nil)
        }
    }
    
    @objc
    func endOBUInstall() {
        //  TODO check with Hary what need to be done here
    }
    
    @objc
    func initParkingMode() {
        DispatchQueue.main.async {
            self.switchMode(Values.PARKING_MODE)
        }
    }
    
    @objc
    func toggleOBUAutoConnect(_ isEnabled: Bool) {
        Prefs.shared.setAutoConnectObu(AWSAuth.sharedInstance.cognitoId, isAuto: isEnabled)
    }
    
    @objc
    func toggleConnectOBU(_ isConnecting: Bool, vehicleNumber: String, obuName: String) {
        
        //  Verify bluetooth connection before connect
        if !isConnecting {
            
            if !OBUHelper.shared.isOnBluetooth() {
                DispatchQueue.main.async {
                    if let mapVC = appDelegate().getMapLandingVCInTop() {
                        mapVC.showBluetoothAlert()
                    }
                }
                return
            } else if !OBUHelper.shared.isBluetoothPermissionGranted() {
                DispatchQueue.main.async {
                    if let mapVC = appDelegate().getMapLandingVCInTop() {
                        mapVC.showPermissionAlert()
                    }
                }
                return
            }
        }
        
        DispatchQueue.main.async {
            if let mapVC = appDelegate().getMapLandingVCInTop() {
                //  Handle validation
                if !mapVC.verifyBluetoothPermission() && !isConnecting {
                    //  Do nothing
                } else {
                    OBUHelper.shared.toggleConnectOBU(isConnecting: isConnecting, vehicleNumber: vehicleNumber, obuName: obuName) { success in
                        if !isConnecting && success {
                            mapVC.showToastOBUConnected()
                        }
                    }
                }
            }
        }
    }
    
    @objc
    func removeOBU(_ vehicleNumber: String, obuName: String) {
        OBUHelper.shared.removeOBU(vehicleNumber: vehicleNumber, obuName: obuName)
    }
    
    @objc func disableCruise(_ disabled: Bool) {
        //        NotificationCenter.default.post(name: Notification.Name(Values.NotificationDisableCruise), object: nil,userInfo: [Values.DisableCruiseKey: disabled])
    }
    
    @objc func handleClickTrafficInbox(_ inboxTrafficAlertData:String){
        
        print("inbox traffic alert data",inboxTrafficAlertData)
        
        if let jsonData = inboxTrafficAlertData.data(using: .utf8) {
            
            if let notifications = try? JSONDecoder().decode(NotificationModel.self, from: jsonData) {
                
                print("inbox traffic alert data notification model",notifications)
                
                DispatchQueue.main.async {
                    if let mapLandingVC = appDelegate().getMapLandingVCInTop() {
                        if let mapLandingModel = mapLandingVC.mapLandingViewModel{
                            mapLandingModel.isEventFromInbox = true
                        }
                        mapLandingVC.addTrafficNotificationToMap(notifications)
                    }
                }
            }
        }
    }
    
    @objc func sendSeenMessageList(_ seenCount:String){
        
        print("Seen messages list",seenCount)
    }
    
    @objc func handleResetHome() {
        DispatchQueue.main.async {
            if let mapLandingVC = appDelegate().getMapLandingVCInTop() {
                mapLandingVC.removeTrafficNotificationFromMap()
                mapLandingVC.toDisplayNudge = true
            }
        }
    }
    
    @objc
    func changeRNScreen(_ isSelected: Bool)
    {
        print(isSelected)
        let preferences = [Values.ChangeStatusBarKey: isSelected]
        NotificationCenter.default.post(name: Notification.Name(Values.NotificationChangeStatusBar), object: nil , userInfo: preferences as [AnyHashable : Any])
    }
    
    @objc
    func closeOnboardingTutorial()
    {
        NotificationCenter.default.post(name: Notification.Name(Values.NotificationCloseOnboardingTutorial), object: nil , userInfo: nil)
    }
    
    @objc
    func selectContent(_ contentId: NSNumber) {
        let userInfo = [Values.ContentIdKey:contentId] as [String : Any]
        NotificationCenter.default.post(name: Notification.Name(Values.NotificationSelectContent), object: nil,userInfo: userInfo)
    }
    
    @objc
    func switchToLandingTab(_ screenName: NSString) {
        let userInfo = [Values.ScreenNameKey:screenName] as [String : Any]
        NotificationCenter.default.post(name: Notification.Name(Values.NotificationExploreMapBottomTabClicked), object: nil,userInfo: userInfo)
    }
    
    @objc
    func closeCollectionDetail() {
        NotificationCenter.default.post(name: Notification.Name(Values.NotificationCloseCollectionDetail), object: nil , userInfo: nil)
        DispatchQueue.main.async {
            if let mapLandingVC = appDelegate().getMapLandingVCInTop() {
                
                var isObuDisplay = false
                //  Handle back to previous screen while in cruise
                if let vcs = mapLandingVC.navigationController?.viewControllers {
                    for vc in vcs {
                        if let _ = vc as? OBUDisplayViewController {
                            isObuDisplay = true
                            break
                        }
                    }
                }
                
                if isObuDisplay {
                    if let colelctionDetailVC = mapLandingVC.navigationController?.topViewController as? CollectionDetailViewController {
                        colelctionDetailVC.navigationController?.popViewController(animated: true)
                    }
                } else {
                    mapLandingVC.navigationController?.popToViewController(mapLandingVC, animated: false)
                    mapLandingVC.goToSavedMapCollection()
                }
            }
        }
    }
    
    @objc
    func sendCollectionData(_ screenName: NSString, data: NSDictionary?) {
        guard let data = data else {
            return
        }
        let newDict = NSMutableDictionary(dictionary: data)
        newDict["fromScreen"] = screenName
        NotificationCenter.default.post(name: Notification.Name(Values.NotificationUpdateCollectionDetail), object: nil , userInfo: newDict as? [AnyHashable : Any])
    }
    
    @objc
    func didSaveSharedLocationToCollection(_ bookmarkId: NSNumber?)  {
        let newDict = ["bookmarkId":bookmarkId?.intValue ?? 0]
        NotificationCenter.default.post(name: Notification.Name(Values.NotificationDidSaveLocationToCollection), object: nil , userInfo: newDict as? [AnyHashable : Any])
    }
    
    @objc
    func showToastfromRN(_ type: String, message: String) {
        let dict: [String: Any] = ["type": type,
                                   "message": message]
        NotificationCenter.default.post(name: Notification.Name(Values.NotificationShowToast), object: nil , userInfo: dict)
    }
    
    
    @objc
    func closeProgressTrackingScreen() {
        DispatchQueue.main.async {
            if let contentVC = appDelegate().getContentVC() {
                contentVC.closeProgressScreen()
            }
            NotificationCenter.default.post(name: Notification.Name(Values.NotificationCloseProgressTracking), object: nil , userInfo: nil)
        }
    }
    
    @objc
    func goBackExploreMap(_ screenName: NSString) {
        DispatchQueue.main.async {
            if let navigationController = appDelegate().getContentVC()?.navigationController {
                navigationController.popViewController(animated: true)
            }
        }
        SelectedProfiles.shared.clearProfile()
        NotificationCenter.default.post(name: Notification.Name(Values.NotificationExploreMapScreen), object: nil , userInfo: nil)
    }
    
    @objc
    func appearInstabug() {
        DispatchQueue.main.async {
            AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.settings_feedback, screenName: ParameterName.settings_screen)
            Instabug.show()
        }
    }
    
    @objc
    func popToMapLanding(_ reactTag: NSNumber) {
        DispatchQueue.main.async {
            if let view = RNViewManager.sharedInstance.getBrigde.uiManager.view(forReactTag: reactTag) {
                if let reactNativeVC = view.reactViewController() {
                    if let controllers = reactNativeVC.navigationController?.viewControllers {
                        for controller in controllers {
                            if controller.isKind(of: MapLandingVC.self) {
                                reactNativeVC.navigationController?.popToViewController(controller, animated: true)
                                break
                            }
                        }
                    }
                }
            }
        }
    }
    
    @objc
    func logNative(_ tag: String, message: String) {
        if tag == "debug" {
            SwiftyBeaver.debug(message)
        } else if tag == "info" {
            SwiftyBeaver.info(message)
        } else if tag == "warning" {
            SwiftyBeaver.warning(message)
        } else if tag == "error" {
            SwiftyBeaver.error(message)
        } else {
            SwiftyBeaver.debug(message)
        }
    }
    
    @objc
    func cancelPairing(_ obuName:String) {
        DispatchQueue.main.async {
            if !OBUHelper.shared.isObuConnected() {
                OBUHelper.shared.isCancelPairing = true
                OBUHelper.shared.disconnectOBU {[weak self] (obu, error) in
                    OBUHelper.shared.stateOBU = .IDLE
                    if let err = error {
                        SwiftyBeaver.debug("OBU - Cancelling connect to OBU \(OBUHelper.shared.obuName), error: \(err.localizedDescription)")
                    } else {
                        SwiftyBeaver.debug("OBU - Cancelling successfully: \(OBUHelper.shared.obuName)")
                    }
                }
            }
        }
        AnalyticsManager.shared.logClickEvent(eventValue: ParameterName.ObuPairing.UserClick.pairing_cancel_pairing, screenName: ParameterName.ObuPairing.screen_view)
    }
    
    
    @objc
    func openCarparkListScreen() {
        DispatchQueue.main.async {
            NotificationCenter.default.post(name: .openCarparkListScreen, object: nil)
        }
    }
    
    @objc
    func closeInviteScreen() {
        DispatchQueue.main.async {
            NotificationCenter.default.post(name: .onCloseInviteScreen, object: nil)
        }
    }
    
}


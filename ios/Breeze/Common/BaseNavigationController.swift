//
//  BaseNavigationViewController.swift
//  Breeze
//
//  Created by TuyenLX on 29/03/2022.
//

import Foundation
import UIKit

class BaseNavigationController: UINavigationController {
    
    private var lastBaseVC: BaseViewController? {
        return viewControllers.last as? BaseViewController
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        
        guard let lastVC = lastBaseVC else { return .darkContent }
        
        //Check view controller is a Viewcontroller that contain Mapbox
        // Turn by turn navigation controller alway use dark conteint.
        if (lastVC.useDynamicTheme) {
            
            //If vc contain any child from RN, use alwayDarkMode
            if lastVC.children.count > 0 && lastVC.useAlwaydarkMode {
                return .darkContent
            }
            
            switch Settings.shared.theme {
            case UIUserInterfaceStyle.light.rawValue:
                return .darkContent
            case UIUserInterfaceStyle.dark.rawValue:
                return .lightContent
            default:
                return lastVC.isDarkMode ? .lightContent : .darkContent
            }
        }
        return .darkContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        super.pushViewController(viewController, animated: animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override func popViewController(animated: Bool) -> UIViewController? {
        let vc = super.popViewController(animated: animated)
        setNeedsStatusBarAppearanceUpdate()
        return vc
    }
    
    override func popToRootViewController(animated: Bool) -> [UIViewController]? {
        let vc = super.popToRootViewController(animated: animated)
        setNeedsStatusBarAppearanceUpdate()
        return vc
    }
    
    override open func popToViewController(_ viewController: UIViewController, animated: Bool) -> [UIViewController]? {
        let vcs = super.popToViewController(viewController, animated: animated)
        setNeedsStatusBarAppearanceUpdate()
        return vcs
    }
    
    override func setViewControllers(_ viewControllers: [UIViewController], animated: Bool) {
        super.setViewControllers(viewControllers, animated: animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    func processStatusbarNotification(showDarkmode: Bool) {
        // Set alwayDarkMode to current VC.
        lastBaseVC?.alwayDarkMode = showDarkmode
        setNeedsStatusBarAppearanceUpdate()
    }
}
    


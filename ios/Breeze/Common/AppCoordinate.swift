//
//  AppCoordinate.swift
//  Breeze
//
//  Created by Tuyen, Le Xuan  on 27/07/2022.
//

import Foundation

protocol AppCoordinateProtocol {
    func makeCollectionDetail(collectionID: Int, rntag: String, collectionName: String) -> CollectionDetailViewController
    func makeSaveCollectionScreen(rntag: String) -> ShareCollectionAndLocationVC
}

class AppCoordinate: AppCoordinateProtocol {
    static let shared: AppCoordinate = AppCoordinate()
    
    func makeCollectionDetail(collectionID: Int, rntag: String, collectionName: String) -> CollectionDetailViewController {
        let vc = CollectionDetailViewController(nibName: CollectionDetailViewController.name, bundle: nil)
        vc.setCollectionId(collectionID, rntag: rntag, collectionName: collectionName)
        return vc
    }
    
    func makeSaveCollectionScreen(rntag: String) -> ShareCollectionAndLocationVC {
        let vc = ShareCollectionAndLocationVC(nibName: ShareCollectionAndLocationVC.name, bundle: nil)
        vc.setRNTag(rntag)
        return vc
    }
}

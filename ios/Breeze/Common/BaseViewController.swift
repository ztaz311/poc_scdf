//
//  BaseViewController.swift
//  Breeze
//
//  Created by Tuyen, Le Xuan  on 31/03/2022.
//

import UIKit
import Combine

class BaseViewController: UIViewController, ConnectionCombineListener {
    
    var alwayDarkMode: Bool = false
    
    var useAlwaydarkMode: Bool {
        return alwayDarkMode
    }
    
    // Override property and return true if want to dynamic theme (both dark mode & light mode)
    var useDynamicTheme: Bool {
        return false
    }
    
    var needSubscripNetworkChange: Bool {
        return true
    }
    
    var trackingScreenName: String {
        return ""
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (needSubscripNetworkChange) {
            subscribeNetworkChange()
        }
    }
    
    var connectionDisposables: Set<AnyCancellable> = Set<AnyCancellable>()
    
    func logUserClick(event: String) {
        if trackingScreenName.count > 0 {
            AnalyticsManager.shared.logClickEvent(eventValue: event, screenName: trackingScreenName)
        }
    }
    
}

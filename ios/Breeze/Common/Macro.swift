//
//  Macro.swift
//  Breeze
//
//  Created by Tuyen, Le Xuan  on 12/04/2022.
//

import UIKit

let Screen_Size = UIScreen.main.bounds
let Screen_Width = Screen_Size.width
let Screen_Height = Screen_Size.height

/*
 Base on mini or smaller
 https://www.ios-resolution.com/
 */
let Is_Small_Iphone = Screen_Width <= 375

func appDelegate() -> AppDelegate {
    
    if Thread.isMainThread {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    var appDelegate: AppDelegate?
    DispatchQueue.main.sync {
        appDelegate = UIApplication.shared.delegate as? AppDelegate
    }
    return appDelegate!
    
}

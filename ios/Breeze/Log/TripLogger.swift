//
//  TripLogger.swift
//  Breeze
//
//  Created by Zhou Hao on 8/6/21.
//

import Foundation
import CoreLocation
import Combine
import SwiftyBeaver

// data structure used in TravelLog

public enum TripOBUStatus: String {
    case notPaired = "NOT_PAIRED"
    case notConnected = "NOT_CONNECTED"
    case connected = "CONNECTED"
    case partiallyConnected = "PARTIALLY_CONNECTED"
}

struct TripsData {
    let currentPage : Int
    let totalItems: Int
    let totalPages: Int
    let trips : [TripItem]
    let isFeatureUsed: Bool
}

struct TripItem {
    var tripId : Int
    var tripGUID : String
    var tripStartTime : Int
    var tripEndTime : Int
    var tripStartAddress1 : String
    var tripDestAddress1 : String
    var totalDistance : Double
    var totalExpense : Double
    var tripDuration: Int
    var tripPlanBy: String
}

struct TripBase : Codable {
    let currentPage : Int
    let totalItems: Int
    let totalPages: Int
    let trips : [Trip]
    let isFeatureUsed: Bool
    
    enum CodingKeys: String, CodingKey {
        case currentPage = "currentPage"
        case totalItems = "totalItems"
        case totalPages = "totalPages"
        case trips = "trips"
        case isFeatureUsed = "isFeatureUsed"
    }
    
    init(currentPage: Int, totalItems: Int, totalPages: Int, trips: [Trip], isFeatureUsed: Bool) {
        self.currentPage = currentPage
        self.totalItems = totalItems
        self.totalPages = totalPages
        self.trips = trips
        self.isFeatureUsed = isFeatureUsed
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        currentPage = try values.decodeIfPresent(Int.self, forKey: .currentPage) ?? 0
        totalItems = try values.decodeIfPresent(Int.self, forKey: .totalItems) ?? 0
        totalPages = try values.decodeIfPresent(Int.self, forKey: .totalPages) ?? 0
        trips = try values.decodeIfPresent([Trip].self, forKey: .trips) ?? []
        isFeatureUsed = try values.decodeIfPresent(Bool.self, forKey: .isFeatureUsed) ?? false
    }
}

struct Trip : Codable {
    var tripId : Int
    var tripGUID : String
    var tripStartTime : Int
    var tripEndTime : Int
    var tripStartAddress1 : String
    var tripDestAddress1 : String
    var totalDistance : Double
    var totalExpense : Double
    var plannedDestAddress1: String?
    var plannedDestAddress2: String?
    var amenityId: String?
    var layerCode: String?
    var amenityType: String?
    var plannedDestLat: Double?
    var plannedDestLong: Double?
    
    enum CodingKeys: String, CodingKey {
        case tripId = "tripId"
        case tripGUID = "tripGUID"
        case tripStartTime = "tripStartTime"
        case tripEndTime = "tripEndTime"
        case tripStartAddress1 = "tripStartAddress1"
        case tripDestAddress1 = "tripDestAddress1"
        case totalDistance = "totalDistance"
        case totalExpense = "totalExpense"
        case plannedDestAddress1 = "plannedDestAddress1"
        case plannedDestAddress2 = "plannedDestAddress2"
        case amenityId = "amenityId"
        case layerCode = "layerCode"
        case amenityType = "amenityType"
        case plannedDestLat = "plannedDestLat"
        case plannedDestLong = "plannedDestLong"
        
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        tripId = try values.decodeIfPresent(Int.self, forKey: .tripId) ?? 0
        tripGUID = try values.decodeIfPresent(String.self, forKey: .tripGUID) ?? ""
        tripStartTime = try values.decodeIfPresent(Int.self, forKey: .tripStartTime) ?? 0
        tripEndTime = try values.decodeIfPresent(Int.self, forKey: .tripEndTime) ?? 0
        tripStartAddress1 = try values.decodeIfPresent(String.self, forKey: .tripStartAddress1) ?? ""
        tripDestAddress1 = try values.decodeIfPresent(String.self, forKey: .tripDestAddress1) ?? ""
        totalDistance = try values.decodeIfPresent(Double.self, forKey: .totalDistance) ?? 0.0
        totalExpense = try values.decodeIfPresent(Double.self, forKey: .totalExpense) ?? 0.0
        plannedDestAddress1 = try values.decodeIfPresent(String.self, forKey: .plannedDestAddress1)
        plannedDestAddress2 = try values.decodeIfPresent(String.self, forKey: .plannedDestAddress2)
        amenityId = try values.decodeIfPresent(String.self, forKey: .amenityId)
        layerCode = try values.decodeIfPresent(String.self, forKey: .layerCode)
        amenityType = try values.decodeIfPresent(String.self, forKey: .amenityType)
        plannedDestLat = try values.decodeIfPresent(Double.self, forKey: .plannedDestLat)
        plannedDestLong = try values.decodeIfPresent(Double.self, forKey: .plannedDestLong)
        
    }
    
}

struct ObuVehicleResponseModel: Codable {
    var success : Bool?
    var data : [ObuVehicleDetailModel]
    
    enum CodingKeys: String, CodingKey {
        case success = "success"
        case data = "data"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
        data = try values.decodeIfPresent([ObuVehicleDetailModel].self, forKey: .data) ?? []
    }
}


struct ObuVehicleDetailModel: Codable {
    
    var vehicleNumber : String?
    var vehicleType : String?
    var powerType : String?
    var price : Double?
    var consumptionPerKm : Double?
    
    enum CodingKeys: String, CodingKey {
        case vehicleNumber = "vehicleNumber"
        case vehicleType = "vehicleType"
        case powerType = "powerType"
        case price = "price"
        case consumptionPerKm = "consumptionPerKm"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        vehicleNumber = try values.decodeIfPresent(String.self, forKey: .vehicleNumber)
        vehicleType = try values.decodeIfPresent(String.self, forKey: .vehicleType)
        powerType = try values.decodeIfPresent(String.self, forKey: .powerType)
        price = try values.decodeIfPresent(Double.self, forKey: .price)
        consumptionPerKm = try values.decodeIfPresent(Double.self, forKey: .consumptionPerKm)
    }
}

struct TripTransactions: Codable {
    
    let totalItems : Int?
    let totalPages: Int?
    let currentPage: Int?
    let transactions : [TransactionModel]
    
    enum CodingKeys: String, CodingKey {
        case totalItems = "totalItems"
        case totalPages = "totalPages"
        case currentPage = "currentPage"
        case transactions = "transactions"
    }
    
    init(totalItems: Int, totalPages: Int, currentPage: Int, transactions: [TransactionModel]) {
        self.totalItems = totalItems
        self.totalPages = totalPages
        self.currentPage = currentPage
        self.transactions = transactions
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        totalItems = try values.decodeIfPresent(Int.self, forKey: .totalItems)
        totalPages = try values.decodeIfPresent(Int.self, forKey: .totalPages)
        currentPage = try values.decodeIfPresent(Int.self, forKey: .currentPage)
        transactions = try values.decodeIfPresent([TransactionModel].self, forKey: .transactions) ?? []
    }
    
}

struct TransactionModel : Codable {
    
    var name : String?
    var type : String?
    var cardStatus : String?
    var tripGUID : String?
    var chargeAmount : Double?
    var chargeTimeStamp : Double?
    var chargingMessageType : String?
    var startTime : Double?
    var endTime : Double?
    
    private func getCardErrorMessage() -> String {
        var retValue = ""
        
        switch cardStatus {
        case BreezeCardStatus.error.rawValue:
            retValue = "Card Error"
        case BreezeCardStatus.expired.rawValue:
            retValue = "Card Expired"
        case BreezeCardStatus.insufficent.rawValue:
            retValue = "Insufficient Balance"
        default:
            retValue = "System Error"
            break
        }
        return retValue
    }
    
    func getMessage() -> String {
                
        var retValue: String = ""
        if type == "erp" {
            if let message = chargingMessageType?.lowercased() {
                switch message {
                case "alertpoint", "charging":
                    retValue = ""
                case "deductionsuccessful":
                    retValue = ""   //Deduction Successful
                case "deductionfailure":
                    let errorMessage = getCardErrorMessage()
                    if errorMessage.isEmpty {
                        retValue = "Failed Deduction"
                    } else {
                        retValue = "Failed Deduction: \(errorMessage)"
                    }
                default:
                    retValue = ""
                }
            }
        } else if type == "parking" {
            if let message = chargingMessageType?.lowercased() {
                switch message {
                case "alertpoint", "charging":
                    retValue = "Parking Fee"
                case "deductionsuccessful":
                    retValue = ""   //Deduction Successful
                case "deductionfailure":
                    let errorMessage = getCardErrorMessage()
                    if errorMessage.isEmpty {
                        retValue = "Failed Deduction"
                    } else {
                        retValue = "Failed Deduction: \(errorMessage)"
                    }
                    
                default:
                    retValue = ""
                }
            }
        }
        return retValue
    }
    
    enum CodingKeys: String, CodingKey {
        case name = "name"
        case tripGUID = "tripGUID"
        case type = "type"
        case cardStatus = "cardStatus"
        case chargeAmount = "chargeAmount"
        case chargeTimeStamp = "chargeTimeStamp"
        case chargingMessageType = "chargingMessageType"
        case startTime = "startTime"
        case endTime = "endTime"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        tripGUID = try values.decodeIfPresent(String.self, forKey: .tripGUID)
        type = try values.decodeIfPresent(String.self, forKey: .type)
        cardStatus = try values.decodeIfPresent(String.self, forKey: .cardStatus)
        chargeAmount = try values.decodeIfPresent(Double.self, forKey: .chargeAmount)
        chargeTimeStamp = try values.decodeIfPresent(Double.self, forKey: .chargeTimeStamp)
        chargingMessageType = try values.decodeIfPresent(String.self, forKey: .chargingMessageType)
        startTime = try values.decodeIfPresent(Double.self, forKey: .startTime)
        endTime = try values.decodeIfPresent(Double.self, forKey: .endTime)
    }
}


public struct ERPCost: Codable {
    let erpId: Int
    let tripErpId: Int?
    //    let zoneId: String
    let name: String
    let cost: Double
    var actualAmount: Double?
    let exitTime: Int
    let isOBU: String
    let isERPEditable: Bool?
    
    enum CodingKeys: String, CodingKey {
        case erpId = "erpId"
        //        case zoneId
        case tripErpId = "tripErpId"
        case name = "name"
        case exitTime = "time"
        case cost = "originalAmount"
        case actualAmount = "actualAmount"
        case isOBU = "isOBU"
        case isERPEditable = "isERPEditable"
    }
    
    init(erpId: Int, name: String, cost: Double, exitTime: Int, isOBU: String, actualAmount: Double? = nil, tripErpId: Int? = nil, isERPEditable: Bool? = nil) {
        self.erpId = erpId
        self.name = name
        self.cost = cost
        self.exitTime = exitTime
        self.isOBU = isOBU
        self.actualAmount = actualAmount
        self.tripErpId = tripErpId
        self.isERPEditable = isERPEditable
    }
}

public struct TripLog: Codable {
    enum TripStatus: Int, Codable {
        case start = 0
        case end = 1
        case navigation = 2
    }
    let tripId: String
    let status: TripStatus
    let timeStamp: Int
    let latitude: Double
    let longitude: Double
    let distanceTravelled: Double
    let erps: [ERPCost]
}

open class WalkathonTripLogger {
    
    private var service: TripWalkathonServiceProtocol!
    /**
     The id of the current trip which is generated by UUID
     */
    private var type = "NAVIGATION" //  Hard code for now later on can be mutiple type in different screens
    private var tripGUID: String!
    private var startTime: Date!
    private var tripStartAddress1 = ""
    private var tripStartAddress2 = ""
    private var tripDestAddress1 = ""
    private var tripDestAddress2 = ""
    private var plannedDestAddress1 = ""
    private var plannedDestAddress2 = ""
    private var totalDistance: Double = 0
    private var plannedDestLat: Double = 0
    private var plannedDestLong: Double = 0
    private var isDestinationCarpark: Bool?
    private var navigationEndType: String!  //NATURAL or MANUAL
    private var carParkId: String?
    private var amenityId: String?
    private var layerCode: String?
    private var amenityType: String?
    //  Temporary variables
    private var startLocation: CLLocationCoordinate2D!
    private var lastLocation: CLLocationCoordinate2D!
    
    private var isStarted = false
    private var isNavigation = false
    private var stoppedByUser = false
    
    
    init(service: TripWalkathonServiceProtocol) {
        self.service = service
    }
    
    /**
     Start trip log
     1. Generate uuid for the trip. Start the trip
     2. Initialize the variables
     3. Start timer for periodically log
     4. Send the first log
     5. Log statistics data
     */
    public func start(startLocation: CLLocationCoordinate2D, address1: String, address2: String, amenityId: String?, layerCode: String?, amenityType: String?, plannedDestAddress1: String, plannedDestAddress2: String, plannedDestLat: Double, plannedDestLong: Double) {
        guard !isStarted else { return }
        
        isStarted = true
        stoppedByUser = false
        tripStartAddress1 = address1
        tripStartAddress2 = address2
        self.amenityId = amenityId
        self.layerCode = layerCode
        self.plannedDestAddress1 = plannedDestAddress1
        self.plannedDestAddress2 = plannedDestAddress2
        self.plannedDestLat = plannedDestLat
        self.plannedDestLong = plannedDestLong
        self.amenityType = amenityType

        
        // 1. Init tripID
        self.tripGUID = UUID().uuidString
        SwiftyBeaver.debug("walking trip start \(self.tripGUID!)")
        
        // 2. Init locations
        self.startLocation = startLocation
        self.lastLocation = startLocation
        self.startTime = Date()
                
        // 3. Set default values
        self.totalDistance = 0
    }
    
    /**
     Update from walking navigation Walking Navigation
     1. Calculate distance and update total distance for current log and the whole trip
     2. Set the lastLocation
     */
    public func update(location: CLLocationCoordinate2D) {
        guard isStarted else { return }
        // 1.
        let distance = location.distance(to: lastLocation)
        totalDistance += distance
        // 2.
        lastLocation = location
    }
    
    /// Stop Trip and log the `WalkathonTripSummary`
    /**
     Stop Trip
     1. Log summry
     */
    public func stop(endLocation: CLLocationCoordinate2D, address1: String, address2: String, stoppedByUser: Bool, amenityType: String?,  amenityId: String?, layerCode: String?, plannedDestAddress1: String, plannedDestAddress2: String, plannedDestLat: Double, plannedDestLong: Double, completion: ((Bool) -> ())? = nil) {
        
        guard isStarted else { return }
        
        SwiftyBeaver.debug("Walkathon trip stop \(self.tripGUID!)")
        isStarted = false
        self.stoppedByUser = stoppedByUser
        
        tripDestAddress1 = address1
        tripDestAddress2 = address2
        self.amenityId = amenityId
        self.layerCode = layerCode
        self.plannedDestAddress1 = plannedDestAddress1
        self.plannedDestAddress2 = plannedDestAddress2
        self.plannedDestLat = plannedDestLat
        self.plannedDestLong = plannedDestLong
        self.amenityType = amenityType
        
        // 1.
        logSummary(location: endLocation, navigationEndType: stoppedByUser ? WalkathonTripSummary.NavigationEndType.manually : WalkathonTripSummary.NavigationEndType.natural, completion: completion)
        
        totalDistance = 0
    }
    
    private func logSummary(location: CLLocationCoordinate2D, navigationEndType: String, completion: ((Bool) -> ())? = nil) {
        
        // 2. create summary
        let summary = WalkathonTripSummary(tripGUID: tripGUID, type: type, tripStartTime: Int(startTime.timeIntervalSince1970), tripStartAddress1: tripStartAddress1, tripStartAddress2: tripStartAddress2, plannedDestAddress1: plannedDestAddress1, plannedDestAddress2: plannedDestAddress2, tripStartLat: startLocation.latitude, tripStartLong: startLocation.longitude, tripEndTime: Int(Date().timeIntervalSince1970), tripDestAddress1: tripDestAddress1, tripDestAddress2: tripDestAddress2, tripDestLat: location.latitude, tripDestLong: location.longitude, plannedDestLat: plannedDestLat, plannedDestLong: plannedDestLong, totalDistance: totalDistance/1000.0, navigationEndType: navigationEndType, carParkId: carParkId, amenityId: amenityId, layerCode: layerCode, amenityType: amenityType)
        
        // 3. send
        service.sendWalkathonTripSummary(summary: summary) { result in
            switch result {
            case .failure(let error):
                SwiftyBeaver.error("Failed to submit walkathon trip summary: \(error.localizedDescription)")
                completion?(false)
            case .success(let tripId):
                SwiftyBeaver.debug("Walkathon trip summary submitted: \(tripId)")
                completion?(true)
            }
        }
    }
}

open class TripLogger {
    
    /**
     The id of the current trip which is generated by UUID
     */
    private var tripId: String! // current trip id
    private var startTime: Date!
    private var startLocation: CLLocationCoordinate2D!
    private var startAddress1 = ""
    private var startAddress2 = ""
    private var plannedDestAddress1 = ""
    private var plannedDestAddress2 = ""
    private var endAddress1 = ""
    private var endAddress2 = ""
    private var lastLocation: CLLocationCoordinate2D!
    private var service: TripLogServiceProtocol!
//    private var cancellable: Cancellable?
    private var totoalDistance: Double = 0
//    private var totoalDistanceForCurrentLog: Double = 0
    private var amenityId = ""
    private var layerCode = ""
    private var amenityType = ""
    private var plannedDestLat: Double = 0
    private var plannedDestLong: Double = 0
    private var currentLogERPs = [ERPCost]()
    private (set) internal var totalERPs = [ERPCost]()   // make it testable
    private var isStarted = false
    private var isNavigation = false
    private var isOBULite = false
    private var stoppedByUser = false
    
    //  For OBU data
    private var idleTime: Double?
    private var vehicleNumber: String?
    private var bookmarkId: String?
    private var bookmarkName: String?
    private var obuStatus: TripOBUStatus?
    
    init(service: TripLogServiceProtocol, isNavigation: Bool = true, isOBULite: Bool = false) {
        self.service = service
        self.isNavigation = isNavigation
        self.isOBULite = isOBULite
    }
    
    /**
     Start trip log
     1. Generate uuid for the trip. Start the trip
     2. Initialize the variables
     3. Start timer for periodically log
     4. Send the first log
     5. Log statistics data
     */
    public func start(startLocation: CLLocationCoordinate2D, address1: String, address2: String, bookmarkId: String? = nil, bookmarkName: String? = nil, obuStatus: TripOBUStatus? = .notPaired) {
        guard !isStarted else { return }
        
        isStarted = true
        stoppedByUser = false
        startAddress1 = address1
        startAddress2 = address2
        
        // 1.
        self.tripId = UUID().uuidString
        SwiftyBeaver.debug("trip start \(self.tripId!)")
        
        // 2.
        self.startLocation = startLocation
        self.lastLocation = startLocation
        self.startTime = Date()
        self.totoalDistance = 0
//        self.totoalDistanceForCurrentLog = 0
        self.totalERPs = [ERPCost]()
        self.currentLogERPs = [ERPCost]()
        
        self.bookmarkId = bookmarkId
        self.bookmarkName = bookmarkName
        
        // 3.
//        cancellable = Timer.publish(every: 120, on: .main, in: .default)
//            .autoconnect()
//            .sink(receiveValue: { _ in
//                self.log()
//            })
        
        // 4.
//        log(status: .start)
        
        // 5.
        AnalyticsManager.shared.logStatistics(data: StatisticsManager.getData(), screenName: getScreenName(), description: "start")
        
        // 6. OBU status
        self.obuStatus = obuStatus
        if obuStatus == .connected {
            self.vehicleNumber = OBUHelper.shared.obuName
        }
    }
    
    /// Update obu status
    public func updateOBUStatus(_ status: TripOBUStatus = .notPaired) {
        if obuStatus == .partiallyConnected {
            //  Do nothing
        } else {
            //  If OBU connection status changes then update partiallyConnected
            if (obuStatus == .connected && status == .notConnected) || (obuStatus == .notConnected && status == .connected){
                obuStatus = .partiallyConnected
            } else {
                obuStatus = status
            }
        }
        
        //  Update vehicle number when obu status change to connected
        if status == .connected {
            self.vehicleNumber = OBUHelper.shared.obuName
        }
    }
    
    /// Stop Trip and log the `TripSummary`
    /**
     Stop Trip
     1. Stop the timer
     2. log end of trip
     3. log summry
     4. Log statistics data
     5. Clearance
     */
    public func stop(endLocation: CLLocationCoordinate2D, address1: String, address2: String, stoppedByUser: Bool, amenityType: String?,  amenityId: String?, layerCode: String?, plannedDestAddress1: String, plannedDestAddress2: String, plannedDestLat: Double, plannedDestLong: Double, idleTime: Double? = 0, bookmarkId: String?, bookmarkName: String?) {
        
        guard isStarted else { return }
        
        SwiftyBeaver.debug("trip stop \(self.tripId!)")
        
        isStarted = false
        self.stoppedByUser = stoppedByUser
        endAddress1 = address1
        endAddress2 = address2
        self.plannedDestAddress1 = plannedDestAddress1
        self.plannedDestAddress2 = plannedDestAddress2
        self.plannedDestLat = plannedDestLat
        self.plannedDestLong = plannedDestLong
        
        self.amenityId = amenityId ?? ""
        self.amenityType = amenityType ?? ""
        self.layerCode = layerCode ?? ""
        
        self.idleTime = idleTime
        self.bookmarkId = bookmarkId
        self.bookmarkName = bookmarkName
        
        // 1.
//        cancellable?.cancel()
        
        // 2.
//        log(status: .end)
        
        // 3.
//        if isNavigation {    //   Suport send trip log all OBU Lite / Cruise / Navigation
            logSummary(location: endLocation, navigationEndType: stoppedByUser ? TripSummary.NavigationEndType.manually : TripSummary.NavigationEndType.natural)
//        }
        
        // 4.
        AnalyticsManager.shared.logStatistics(data: StatisticsManager.getData(), screenName: getScreenName(), description: "end")

        // 5.
        totalERPs.removeAll()
        totoalDistance = 0
    }
    
    private func getScreenName() -> String {
        if isOBULite {
            return ParameterName.obu_lite_mode
        } else {
            return isNavigation ? ParameterName.navigation : ParameterName.home_cruise_mode
        }
    }
    
    private func getType() -> String {
        if isOBULite {
            return BreezeNavigationType.OBULITE.rawValue
        } else {
            return isNavigation ? BreezeNavigationType.NAVIGATION.rawValue : BreezeNavigationType.CRUISE.rawValue
        }
    }
    
    /// Save current trip summary before app terminate
    /**
     Save current trip to a json in document folder
     - Create a temp TripSummary without address (since app will terminate so can't call async function)
     */
    public func save(endLocation: CLLocationCoordinate2D, amenityType: String?,  amenityId: String?, layerCode: String?, plannedDestAddress1: String, plannedDestAddress2: String, plannedDestLat: Double, plannedDestLong: Double, idleTime: Double? = 0, bookmarkId: String?, bookmarkName: String?) {
        guard let documentDirectoryUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return }
        
        //  Save other missing fields when stop navigation
        self.plannedDestAddress1 = plannedDestAddress1
        self.plannedDestAddress2 = plannedDestAddress2
        self.plannedDestLat = plannedDestLat
        self.plannedDestLong = plannedDestLong
        
        self.amenityId = amenityId ?? ""
        self.amenityType = amenityType ?? ""
        self.layerCode = layerCode ?? ""
        
        self.idleTime = idleTime
        self.bookmarkId = bookmarkId
        self.bookmarkName = bookmarkName
        
        // 1. calculate the total cost
        let totalCost = totalERPs.reduce(0) { $0 + $1.cost }
        
        // 2. create summary
        // always consider this as stopped by user
        let summary = TripSummary(id: tripId, type: getType(), startTime: Int(startTime.timeIntervalSince1970), endTime: Int(Date().timeIntervalSince1970),  startLatitude: startLocation.latitude, startLongitude: startLocation.longitude, startAddress1: startAddress1, startAddress2: startAddress2 ,plannedDestAddress1: plannedDestAddress1, plannedDestAddress2: plannedDestAddress2, endLatitude: endLocation.latitude, endLongitude: endLocation.longitude, plannedDestLat: plannedDestLat, plannedDestLong: plannedDestLong, endAddress1: endAddress1, endAddress2: endAddress2, totalCost: totalCost, totalDistance: totoalDistance/1000.0, parkingExpense: 0, costBreakDowns: totalERPs, navigationEndType: TripSummary.NavigationEndType.manually, amenityId: amenityId, layerCode: layerCode, amenityType: amenityType, idleTime: idleTime, vehicleNumber: vehicleNumber, bookmarkId: bookmarkId, bookmarkName: bookmarkName, obuStatus: obuStatus?.rawValue)
        
        let fileUrl = documentDirectoryUrl.appendingPathComponent("lasttrip.json")
        do {
            let data = try JSONEncoder().encode(summary)
            try data.write(to: fileUrl, options: [.atomic, .completeFileProtection])
        } catch {
            print(error)
        }
    }
    
    /// Recover last saved trip log
    public static func recover() {
        guard let documentDirectoryUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return }
        
        let fileUrl = documentDirectoryUrl.appendingPathComponent("lasttrip.json")
        
        do {
            let data = try Data(contentsOf: fileUrl)
            let tripSummary = try JSONDecoder().decode(TripSummary.self, from: data)
            
            // get destination address
            getAddressFromLocation(location: CLLocation(latitude: tripSummary.endLatitude, longitude: tripSummary.endLongitude) ) { address in
                
                let summary = TripSummary(id: tripSummary.id, type: tripSummary.type, startTime: tripSummary.startTime, endTime: tripSummary.endTime, startLatitude: tripSummary.startLatitude, startLongitude: tripSummary.startLongitude, startAddress1: tripSummary.startAddress1, startAddress2: tripSummary.startAddress2, plannedDestAddress1: tripSummary.plannedDestAddress1, plannedDestAddress2: tripSummary.plannedDestAddress2, endLatitude: tripSummary.endLatitude, endLongitude: tripSummary.endLongitude, plannedDestLat: tripSummary.plannedDestLat, plannedDestLong: tripSummary.plannedDestLong, endAddress1: address, endAddress2: "", totalCost: tripSummary.totalCost, totalDistance: tripSummary.totalDistance, parkingExpense: 0, costBreakDowns: tripSummary.costBreakDowns, navigationEndType: tripSummary.navigationEndType ?? "", amenityId: tripSummary.amenityId, layerCode: tripSummary.layerCode, amenityType: tripSummary.amenityType, idleTime: tripSummary.idleTime, vehicleNumber: tripSummary.vehicleNumber, bookmarkId: tripSummary.bookmarkId, bookmarkName: tripSummary.bookmarkName, obuStatus: tripSummary.obuStatus)
                
                TripLogService().sendTripSummary(summary: summary) { result in
                    switch result {
                    case .failure(let error):
                        SwiftyBeaver.error("Failed to sumbimt when recovering: \(error.localizedDescription)")
                    case .success(let tripId):
                        SwiftyBeaver.debug("Trip summary submitted \(tripId) when recovering")
                        try? FileManager.default.removeItem(at: fileUrl)
                    }
                }
                
            }

        } catch {
            print(error)
        }
    }
    
    /**
     Update from navigation (PassiveLocationManager or TurnByTurn navigation)
     1. Calculate distance and update total distance for current log and the whole trip
     2. Set the lastLocation
     */
    public func update(location: CLLocationCoordinate2D) {
        guard isStarted else { return }
        // 1.
        let distance = location.distance(to: lastLocation)
        totoalDistance += distance
//        totoalDistanceForCurrentLog += distance
        // 2.
        lastLocation = location
    }
    
    public func exitErp(_ erp: ERPCost) {
        
        SwiftyBeaver.debug("erp exit: \(erp)")
        // check if it's duplicated #232 - temp solution now
        // nav sdk 2.0.0 rc.2 should fixed this issue so we change the interval to 60s
        for existingErp in currentLogERPs {
            if existingErp.erpId == erp.erpId && abs(existingErp.exitTime - erp.exitTime) < 60 {
                SwiftyBeaver.debug("erp exit filtered out: \(erp.erpId)")
                return
            }
        }
        for existingErp in totalERPs {
            if existingErp.erpId == erp.erpId && abs(existingErp.exitTime - erp.exitTime) < 60 {
                SwiftyBeaver.debug("erp exit filtered out: \(erp.erpId)")
                return
            }
        }
        currentLogERPs.append(erp)
        totalERPs.append(erp)
    }
    
    public func getTripGUID() -> String {
        return tripId ?? "" // To prevent the destination is too close so that the trip stopped (RatingVC need this GUID even before it's started.
    }
    
    /**
     Periodically log
     1. Create a TripLog
     2. Send to server
     3. Clearance
     */
    /*
    private func log(status: TripLog.TripStatus = .navigation) {
        // 1.
        let tripLog = TripLog(tripId: tripId, status: status, timeStamp: Int(Date().timeIntervalSince1970), latitude: lastLocation.latitude, longitude: lastLocation.longitude, distanceTravelled: totoalDistanceForCurrentLog, erps: currentLogERPs)
        
        // 2.
        service.sendTripLog(tripLog: tripLog) { result in
            switch result {
            case .failure(let error):
                SwiftyBeaver.error("\(error.localizedDescription)")
            case .success(_):
                print("Trip logged:\(tripLog.tripId)")
            }
        }
        
        // 3.
        totoalDistanceForCurrentLog = 0
        currentLogERPs.removeAll()
    }
     */
    
    private func logSummary(location: CLLocationCoordinate2D, navigationEndType: String) {
        // 1. calculate the total cost
        let totalCost = totalERPs.reduce(0) { $0 + $1.cost }
        
        //  Convert idle time
        var idleTimeMins: Double = 0
        if let time = idleTime, time > 0 {
            idleTimeMins = time / 60
        }
        
        // 2. create summary
        let summary = TripSummary(id: tripId, type: getType(), startTime: Int(startTime.timeIntervalSince1970), endTime: Int(Date().timeIntervalSince1970),  startLatitude: startLocation.latitude, startLongitude: startLocation.longitude, startAddress1: startAddress1, startAddress2: startAddress2,plannedDestAddress1: plannedDestAddress1,plannedDestAddress2: plannedDestAddress2, endLatitude: location.latitude, endLongitude: location.longitude,plannedDestLat: plannedDestLat, plannedDestLong: plannedDestLong, endAddress1: endAddress1, endAddress2: endAddress2, totalCost: totalCost, totalDistance: totoalDistance/1000.0, parkingExpense: 0, costBreakDowns: totalERPs, navigationEndType: navigationEndType, amenityId: amenityId, layerCode: layerCode, amenityType: amenityType, idleTime: idleTimeMins, vehicleNumber: vehicleNumber, bookmarkId: bookmarkId, bookmarkName: bookmarkName, obuStatus: obuStatus?.rawValue)
        
        // 3. send
        service.sendTripSummary(summary: summary) { result in            
            switch result {
            case .failure(let error):
                SwiftyBeaver.error("Failed to submit trip summary: \(error.localizedDescription)")
            case .success(let tripId):
                SwiftyBeaver.debug("Trip summary submitted: \(tripId)")
            }
        }
    }
}



# Splash flow

```mermaid

graph TD
    ViewDidLoad --> load 
    load --> networkReachable{{ is network reachable? }}
    networkReachable --yes--> locationStatus{{ is location allowed? }}
    networkReachable --> showRetryButton
    locationStatus --yes--> sessionCheck{{ is user logged in }}
    locationStatus --no--> requestLocation
    requestLocation --> locationCallback
    locationCallback --> locationStatus2{{ is location allowed? }}
    locationStatus2 --yes--> sessionCheck
    locationStatus2 --no--> ?
    sessionCheck --yes--> getEasyBreezyFromServer{{getEasyBreezyFromServer success}}
    sessionCheck --no--> afterLoading1(isSignedIn=false)
    afterLoading1(isSignedIn=false) --> addFloatingButton[[add floating button]]
    getEasyBreezyFromServer --yes--> afterLoading2(isSignedIn=true)
    getEasyBreezyFromServer --no--> afterLoading2
    afterLoading2 --> addFloatingButton    
    afterLoading2 --> gotoMapLandingVC
    afterLoading1 --> gotoLandingVC

```


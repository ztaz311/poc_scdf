# App update push notification test

## Revision

**Version**: 1.0

**Created date**: 29/06/2022

**Author**: Zhou Hao

## Prerequisite:

- iPhone device
- Knuff (third party app which is able to send push notification in Mac)
- Get the device token in Xcode console, for example: f2505b014f57d653da50d6d5d820bf4fc7c15e1e0cac4599bb71e83b7b38eff8
- Need the push notification cert
- App update payload json. For example:

```
{
	"aps": {
  	"alert": {
		"title": "Software Update Available",
  		"body": "A new version with the latest features is available now."
  	},
  	"content-available": "1",
  	"type": "APP_UPDATE",
  	"sound": "default"
  	}
}
```

## Test Scenarios:

### 1. App in foreground - not in cruise or navigation

#### Steps:
- Run application
- Application is not in cruise or navigation
- Send a push notification

#### Result:

- Notification will be shown on top

### 2. App in foreground - in cruise or navigation

#### Steps:
- Run application
- Start navigation
- Send a push notification

#### Result:

- Notification will be shown on top
- But click on notification will do nothing

### 3. App in background

#### Steps:
- Run application
- Put application in background
- Send a push notification

#### Result:

- Notification will be shown on top of the screen.
- Click the notification on top of the screen or in the Notification Center will bring the application in foreground and showing the app update popup if needed. ?

### 4. App is not login

#### Steps:
- Run application
- Goto Settings > Account > Sign Out
- Send a push notification

#### Result:

- Notification will be shown on top of Sign in screen
- Click on the notification will show the app update popup if needed


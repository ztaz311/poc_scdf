# Navigation Test

## Revision

**Version**: 1.0

**Created date**: 29/06/2022

**Author**: Zhou Hao

## Test Scenarios:

### 1. Reroute to nearby carpark when destination is a carpark

#### Steps:
- Set the simulator coordinate: `1.2837942572307681, 103.82974048326801`
- Search **80 Moh Guan Terrace**. Goto route planning.
- Click CarPark button. Then selected **78/80 Moh Guan Terrace** carpark. (This carpark should be in amber color. It means it's congested)
- Start navigation
- After 7s, the nearby carparks should be shown. Select one to **Navigation here**.

#### Result:

- The destination should become destination icon instead of carpark icon.

### 2. Reroute to nearby carpark when destination is a carpark

#### Steps:

- Same as test case 1

#### Result:

- No walking path should be seen and no pop up asking whether to show walking path to another car park.

### 3. Reroute to nearby carpark when destination is not a carpark

#### Steps:

- Set the simulator coordinate: `1.2837942572307681, 103.82974048326801`
- Search **80 Moh Guan Terrace**. Goto route planning.
- Start navigation
- After 7s, the nearby carparks should be shown. Select one to **Navigation here**.

#### Result:

- Walking path should be shown from the selected carpark to original destination
- A popup will be shown to ask whether to start walking (**Walking Guide**) when reaching destination.

### 4. End navigation logic
### 4.1 Reaching destination without reroute

#### Steps:

- Set the simulator coordinate: `1.2837942572307681, 103.82974048326801`
- Search **80 Moh Guan Terrace**. Goto route planning.
- Start navigation

#### Result:

- **End** button will be shown at the bottom. **You have arrived!** is shown at the top
- Click **End** button will go back to Map Landing.

### 4.2. Reroute to nearby carpark when destination is not a carpark

#### Steps:

- Set the simulator coordinate: `1.2837942572307681, 103.82974048326801`
- Search **80 Moh Guan Terrace**. Goto route planning.
- Start navigation
- After 7s, the nearby carparks should be shown. Select one to **Navigation here**.
- A popup will be shown to ask whether to start walking (**Walking Guide**) when reaching destination.

#### Result:

- Click End to go back to Map Landing.
- Click **Walking Guide** to start walking navigation

### 4.3 Reaching destination in walking guide

#### Steps:

- Set the simulator coordinate: `1.283594, 103.83104`
- Select **@Tiong Bahru** at bottom sheet.
- Select one of the POI and click **Walk Here**

#### Result:

- When reaching destination, the **End** button should be shown at the bottom.
- **We have arrived!** shown at the top.

### 5. TBR Navigation
### 5.1. Start from inner circle and end at outside TB zone 

#### Steps:
- Set the simulator coordinate to inside the TB inner zone: `1.2837942572307681, 103.82974048326801`
- Set the destination outside the TB zone: for example, search **Tan Boon Liat Building**
- Start navigation

#### Result:

- No congestion alert should be heard.




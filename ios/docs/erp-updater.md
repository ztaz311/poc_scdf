# How ERP update works

## Class Diagram

```Mermaid
classDiagram
    class DataCenter {
        erpUpdater: ERPUpdater
        realtimeEngine: RealtimeEngineProtocol!
        setup()
        load() 
        callERPRefreshTimer()
        addERPUpdaterListner()
    }

    class ERPUpdater {
        erpJsonData: ERP Data from api/app sync
        erpService: ERPServiceProtocol
        listeners: Multiple listeners
        currentCostFeatureCollection: Points to be shown on map
        currentNoCostFeatureCollection: Points to be shown on map
        erpAllFeatureCollection: FeatureCollection
        erpFeatureDetectionCostFeatures: Line String for EH
        erpFeatureDetectionNoCostFeatures: Line String for EH

        load()
        update()
        unload()
    }

    DataCenter *-- "1" ERPUpdater: has
    ERPUpdater ..> ERPServiceProtocol
    ERPServiceProtocol <|-- ERPService
    ERPServiceProtocol <|-- MockERPService

    DataCenter ..> RealtimeEngineProtocol
    RealtimeEngineProtocol <|-- AppSync
    RealtimeEngineProtocol <|-- MockAppSync
```

## Sequence Diagram

```Mermaid
sequenceDiagram
    AppDelegate->>DataCenter: didFinishLaunchingWithOptions
    DataCenter->>ERPUpdater: setup
    AppDelegate->>DataCenter: isLogged In
    DataCenter->>ERPUpdater: load
    ERPUpdater->>ERPServiceProtocol: call API - getERPDetails()
    ERPServiceProtocol->>ERPUpdater: complete
    ERPUpdater->>ERPUpdater: parseJSONItems
    ERPUpdater->>ERPUpdater: checkCurrentIsBetweenUniqueTimes
    Note over ERPUpdater,ERPUpdater: setup next timer
    RealtimeEngineProtocol->>DataCenter: ERP updated in App Sync    
    DataCenter->>ERPUpdater: update
    ERPUpdater->>ERPUpdater: parseJSONItems
        ERPUpdater->>ERPUpdater: checkCurrentIsBetweenUniqueTimes
    Note over ERPUpdater,ERPUpdater: setup next timer        
    AppDelegate->>DataCenter: sceneWillEnterForeground
    DataCenter->>ERPUpdater: callERPRefreshTimer
    Note over DataCenter,ERPUpdater: Is last update time > 120s?
    ERPUpdater->>ERPUpdater: refreshERPFromDataCenter
    ERPUpdater->>ERPUpdater: checkCurrentIsBetweenUniqueTimes
    Note over ERPUpdater,ERPUpdater: setup next timer
    ERPUpdater->>ERPUpdater: refreshERP
    Note over ERPUpdater,ERPUpdater: triggered by timer
    ERPUpdater->>ERPUpdater: parseJSONItems
        ERPUpdater->>ERPUpdater: checkCurrentIsBetweenUniqueTimes
    Note over ERPUpdater,ERPUpdater: setup next timer    
    AppDelegate->>DataCenter: isLogged out
    DataCenter->>ERPUpdater: unload

```

## Flow Chart

### `parseJSONItems`

```Mermaid
flowchart TD
    Start --> checkDate{Is public holiday or Sunday}
    checkDate -->|Yes| C[Add all erp line string erp. \nAnd point erp: for display\nSet all costs to 0];
    C --> I{Public holiday data exists?}
    I --> |YES| J{Get next public end time?}
    I --> |NO| H
    J --> |YES| G[Refresh at next end time]
    J --> |NO| H[Refresh at end of the date]    
    checkDate -->|No| D[Loop through all erp items]
    D --> E[get first rates table \nwhose zoneId is equal to erp item]
    E --> getUniqueTimes[get array of unique times\nappend to uniqueERPTimes]
    getUniqueTimes --> getERPChargeAmount
    getERPChargeAmount --> K{Charge amount is 0?}
    K --> |YES| L[Append data to erpCostfeatures \nand erpLineCostfeatures]
    K --> |NO| M[Append data to erpCostfeatures \nand erpLineCostfeatures with cost 0]
    
```

### `checkCurrentIsBetweenUniqueTimes`



## TODO:

- Consider to add ERP data to database so that it's easier to find active ERP?
- Add Unit test to some functions, for example:
    * `isWeekend`
    * `checkERPPublicHoliday`
    * `parseJSONItems`
    * `checkCurrentIsBetweenUniqueTimes`
    * `currentTimeisBetween`
- Code cleaning, remove the follow files:
    * `CarPlayERPUpdater.swift`
- Code clearance, remove the commented codes which are not necessary
- Code optimization, for example:
    * `parseJSONItems` when is not sunday and public holiday (probably some code reuse)
- Possible duplication in `invalidateERPRefreshTimer`: - will comment out

```Swift
    update(erpData: erpJsonData!)
    
    if(uniqueERPTimes.count > 0)
    {
        checkCurrentIsBetweenUniqueTimes()
    }
```

- `parseJSONItems` when in public holiday and sunday

```Swift
    if(erpRefreshTimer == nil)
    {
        erpRefreshTimer = Timer.scheduledTimer(timeInterval:endOfDay, target: self, selector: #selector(refreshERP), userInfo: nil, repeats: false)
    }
```

- Error handling when API failed or App Sync failed.  
- Extreme case: If `uniqueERPTimes` is empty for some reason. 
- No place clear `uniqueERPTimes`. Need to clear it when get updates from AppSync or API call
- Check if AppSync working in background
- Remove `CarPlayERPUpdater.swift` from project






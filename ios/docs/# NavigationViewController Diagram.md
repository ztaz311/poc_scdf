# NavigationViewController Diagram

```Mermaid
classDiagram
class NavigationViewController {
    +voiceController: RouteVoiceController
    +navigationMapView: NavigationMapView
    +navigationService: NavigationService!
    navigationView: NavigationView!
}

class NavigationView

NavigationViewController *-- NavigationView

class RouteVoiceController {
    speechSynthesizer: SpeechSynthesizing    
}

AVSpeechSynthesizerDelegate <|-- RouteVoiceController

class SpeechSynthesizing
<<interface>> SpeechSynthesizing

SpeechSynthesizing <|-- MultiplexedSpeechSynthesizer
SpeechSynthesizing <|-- MapboxSpeechSynthesizer
SpeechSynthesizing <|-- SystemSpeechSynthesizer

```

```Mermaid
sequenceDiagram
    autonumber
    NavigationService-->RouteVoiceController: Passed to
    RouteVoiceController->>NavigationService: Observe
    RouteVoiceController->>SpeechSynthesizing: Initialize
    NavigationService->>RouteVoiceController: .routeControllerDidPassSpokenInstructionPoint
    RouteVoiceController->>SpeechSynthesizing:currentSpokenInstruction
    SpeechSynthesizing-->SpeechSynthesizing: speak
    NavigationService->>RouteVoiceController: .routeControllerWillReroute
    RouteVoiceController->>SpeechSynthesizing:pauseSpeechAndPlayReroutingDing
    SpeechSynthesizing-->SpeechSynthesizing:stopSpeaking
    NavigationService->>RouteVoiceController: .routeControllerDidReroute
    RouteVoiceController->>SpeechSynthesizing:pauseSpeechAndPlayReroutingDing
    SpeechSynthesizing-->SpeechSynthesizing:stopSpeaking
    NavigationService->>RouteVoiceController: .navigationSettingsDidChange
    RouteVoiceController->>SpeechSynthesizing:didUpdateSettings
    
```
# Trip log
    
## Sequence diagram

```Mermaid
sequenceDiagram
    autonumber
    participant Cruise/Navigation
    participant TripLogger
    participant TripLoggerServiceProtocol    
    Cruise/Navigation->>TripLogger: init
    Cruise/Navigation->>TripLogger: start
    loop every 2 minutes
        TripLogger->>TripLogger: log
    end
    TripLogger->>TripLoggerServiceProtocol: sendTripLog
    Cruise/Navigation->>TripLogger: update
    Note over Cruise/Navigation,TripLogger: Location updates
    loop 
        TripLogger->>TripLogger: calcuate distance
    end
    Cruise/Navigation->>TripLogger: stop
    TripLogger->>TripLoggerServiceProtocol: sendTripSummary 
    Note over TripLogger, TripLoggerServiceProtocol: Only send in navigation mode (API call)

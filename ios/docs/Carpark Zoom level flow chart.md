# Carpark Zoom level flow chart

```Mermaid

graph TD
    Dest{Destination has car park} -- Y --> DestCheapest{Destination is cheapest}
    Dest -- N --> B{get the cheapest carpark}
    DestCheapest -- Y --> A(Get the next carpark)
    A ----> C(Use the destination and next carpark to calculate the zoom level)
    C ----> setZoomLevel
    DestCheapest -- N --> Cheapest{get the cheapest carpark}
    Cheapest -- Y--> D(Use the destination and cheapest carpark to calculate the zoom level)
    D ----> setZoomLevel
    setZoomLevel ----> Callout
    Cheapest -- N--> Default(set default zoom level)
    Default ----> Callout(Show Destination Callout)
    B -- Y --> F(Use the destination and the cheapest carpark to calculate the zoom level)
    F ----> setZoomLevel2(setZoomLevel)
    setZoomLevel2 ----> Callout2(show Cheapest Callout)
    B -- N --> Default2(set default zoom level)
    Default2 ----> Callout2
    
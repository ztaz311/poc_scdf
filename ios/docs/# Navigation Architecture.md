# Navigation

```Mermaid
classDiagram
    class TurnByTurnNavigationViewModel {
        navigationService: NavigationService                
    }

    class TurnByTurnNavigationVC {
        viewModel: TurnByTurnNavigationViewModel
        topBanner: TopBannerViewController
        bottomBanner: NavProgressVC
    }

    class CarPlayController {
        viewModel: TurnByTurnNavigationViewModel
    }

    class MapViewUpdatable {
        <<interface>>
        navigationMapView: NavigationMapView
    }

    NavigationServiceDelegate -- TurnByTurnNavigationViewModel: Implement
    NavigationMapViewController <|-- TurnByTurnNavigationVC
    MapViewUpdatable <|-- MobileMapViewUpdatable
    MapViewUpdatable <|-- CarPlayMapViewUpdatable
    ContainerViewController <|-- NavProgressVC
    NavigationComponent <|-- ContainerViewController
    NavigationServiceDelegate <|-- NavigationComponent: Implement
    
```
# Feature Detection Diagram

## DataCenter

- Using a centralized `DataCenter` to load different data from local repository. 
- The local repository will be updated when app sync send data.

```Mermaid
classDiagram
    class DataCenter {
        +updater: ERPUpdater 
        +silverZoneSVC: SilverZoneServiceProtocol
        +schoolZoneSVC: SchoolZoneServiceProtocol

        +update(data)
    }

    class ERPUpdater {
        +ERPServiceProtocol erpService
    }

    class ERPServiceProtocol { 
        <<Protocol>>
    }

    ERPUpdater *-- ERPServiceProtocol

    class SilverZoneServiceProtocol { 
        <<Protocol>>
    }
    class SchoolZoneServiceProtocol { 
        <<Protocol>>
    }

    DataCenter *-- ERPUpdater
    DataCenter *-- SilverZoneServiceProtocol
    DataCenter *-- SchoolZoneServiceProtocol

    SilverZoneServiceProtocol <|-- MockSilverZoneService
    SilverZoneServiceProtocol <|-- SilverZoneService

    SchoolZoneServiceProtocol <|-- MockSchoolZoneService
    SchoolZoneServiceProtocol <|-- SchoolZoneService

    ERPServiceProtocol <|-- MockERPService
    ERPServiceProtocol <|-- ERPService

```

## E-Horizon

Mapbox implemented [Electronic](https://docs.mapbox.com/ios/navigation/guides/advanced/electronic-horizon/) Horizon in navigation sdk for Feature Detection.


## FeatureDetectable
A `FeatureDetectable` is a base class for all detectable features

```Mermaid
classDiagram
    class FeatureDetectable {
        <<Interface>>
        +id: String
        +distance: Double
        +priority: Int
    }

    class DetectedSchoolZone {
        +feature: Turf.Feature
    }

    class DetectedSilverZone {
        +feature: Turf.Feature
    }

    class DetectedERP {
        +address: String
        +price: Double
    }

    FeatureDetectable <|-- DetectedSchoolZone
    FeatureDetectable <|-- DetectedSilverZone
    FeatureDetectable <|-- DetectedERP
```

## FetureDetector

Will use a `FeatureDetector` for both Cruise mode navigation and Turn by turn navigation

- `PriorityQueue` is a data structure will sort objects in the order of priority
    
```Mermaid
classDiagram
    class FeatureDetector {
        +matcher: RoadObjectMatcher
        +store: RoadObjectsStore
        +queue: PriorityQueue<FeatureDetectable>

        +startObserver()
        +stopObserver()
        +addERPObjectsToRoadMatcher()
        +addSilverZoneObjectsToRoadMatcher()
        +addSchoolObjectsToRoadMatcher()
    }

    class PriorityQueue~FeatureDetectable~ {

    }

    FeatureDetector *-- PriorityQueue
```

### Sequence diagram of `FeatureDetector`

```Mermaid
sequenceDiagram
    autonumber
    participant Cruise/Navigation
    participant FeatureDetector
    participant RoadObjectsStore
    participant RoadObjectsMatcher
    participant NotificationCenter
    FeatureDetector->>RoadObjectsStore: init
    FeatureDetector->>RoadObjectsMatcher: init
    loop 
        FeatureDetector->>FeatureDetector: startObserver
    end
    
    Cruise/Navigation->>FeatureDetector: addERPObjectsToRoadMatcher
    Cruise/Navigation->>FeatureDetector: addSilverZoneObjectsToRoadMatcher
    Cruise/Navigation->>FeatureDetector: addSchoolObjectsToRoadMatcher
    
    RoadObjectsMatcher-->>FeatureDetector: RoadObjectMatcherDelegate
    NotificationCenter-->>FeatureDetector: didUpdateElectronicHorizonPosition
    FeatureDetector-->>Cruise/Navigation: didUpdate
    FeatureDetector-->>Cruise/Navigation: didExit
```
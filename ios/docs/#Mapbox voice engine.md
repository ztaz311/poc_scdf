#Mapbox voice engine

## Class diagram

```Mermaid
classDiagram

class AVSpeechSynthesizerDelegate
<<protocol>> AVSpeechSynthesizerDelegate

class SpeechSynthesizing
<<protocol>> SpeechSynthesizing

AVSpeechSynthesizerDelegate <|.. RouteVoiceController

class RouteVoiceController {
    speechSynthesizing: SpeechSynthesizing
    navigationService: NavigationService
}
RouteVoiceController --* SpeechSynthesizing

RouteVoiceController --> NavigationService

SpeechSynthesizing <|-- MultiplexedSpeechSynthesizer

SpeechSynthesizing <|-- MapboxSpeechSynthesizer
SpeechSynthesizing <|-- SystemSpeechSynthesizer

class MultiplexedSpeechSynthesizer {
    speechSynthesizers: [SpeechSynthesizing]
}
MultiplexedSpeechSynthesizer --* SystemSpeechSynthesizer
MultiplexedSpeechSynthesizer --* MapboxSpeechSynthesizer

class MapboxSpeechSynthesizer {
    audioPlayer: AVAudioPlayer?
    remoteSpeechSynthesizer: SpeechSynthesizer
}

SystemSpeechSynthesizer --* AVSpeechSynthesizer

class SystemSpeechSynthesizer {
    speechSynthesizer: AVSpeechSynthesizer
}
```


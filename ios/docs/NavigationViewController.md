# NavigationViewController

```mermaid

graph TD
    ViewDidLoad --> load 
    load --> networkReachable{{ is network reachable? }}
```

```mermaid

classDiagram
    classA --|> classB : Inheritance
    classC --* classD : Composition
    classE --o classF : Aggregation
    classG --> classH : Association
    classI -- classJ : Link(Solid)
    classK ..> classL : Dependency
    classM ..|> classN : Realization
    classO .. classP : Link(Dashed)
```

```mermaid

classDiagram
    NavigationViewController --* RouteMapViewController: mapViewController
    NavigationViewController --* ContainerViewController: topViewController
    NavigationViewController --* ContainerViewController: bottomViewController
    NavigationViewController --* "many" UIButton: floatingButtons
    NavigationViewController --* "many" NavigationComponent: navigationComponents
    RouteMapViewController --* NavigationView
    RouteMapViewController --> EndOfRouteViewController
    NavigationView --* InstructionsBannerView
    NavigationView --* InformationStackView
    NavigationView --* BottomBannerView
    NavigationView --* ResumeButton
    NavigationView --* WayNameLabel
    NavigationView --* FloatingStackView
    NavigationView --* NavigationMapView
    NavigationView --* SpeedLimitView
    
```

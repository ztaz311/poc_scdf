# App Sync class diagram

```mermaid

classDiagram
      RealtimeEngineProtocol <|-- AppSync
      RealtimeEngineProtocol <|-- MockAppSync

class RealtimeEngineProtocol {
    +subscribe(key: String, completion: @escaping (Result<String>) -> Void)
    +updata(key: String, data: String)
}
<<interface>> RealtimeEngineProtocol

class DataCenter {
    +RealtimeEngineProtocol realtimeEngine

    +load(key: String, updater: RealtimeUpdaterProtocol)
}

class RealtimeUpdaterProtocol {    
    +dataUpdated(json: String)
}
<<interface>> RealtimeUpdaterProtocol

```


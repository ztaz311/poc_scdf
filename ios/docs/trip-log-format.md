# Trip log

### Trip log

**Status**

> 0 - start, 1 - end, 2 - navigation

```
{
    "tripId":"095C25D4-4CCF-4D3F-AB83-E6CD1D50F67D",
    "status":0,
    "latitude":1.32274,
    "longitude":103.8416,
    "timeStamp":1623407525,
    "distanceTravelled":0,    
    "erps":[
        {            
            "zoneId":"DZ1",
            "erpId":"43",
            "name":"Dunearn Road \/ Wayang Satu Flyover",
            "cost":1,
            "exitTime":1623211879
        }
    ]
}
```

### Trip summary

```
{
    "id": "B13C7364-C6EE-41A3-9313-A39633ABCCB5",    
    "startLatitude":1.3205498175882184,
    "startTime":1623407983
    "startAddress1": "273 Thomson Road",
    "startAddress2": "",
    "totalCost": 1,
    "totalDistance": 0,
    "tripType": "Navigation",
    "endAddress1": "Mount Elizabeth Hospital Novena",
    "endTime":1623408141,
    "startLongitude": 103.84236976027233,
    "endAddress2": "38 Irrawaddy Road",
    "endLatitude":1.3216730000000001,
    "endLongitude":1.3216730000000001
    "costBreakDowns":
    [
        {            
            "zoneId":"DZ1",
            "erpId":"43",
            "name":"Dunearn Road \/ Wayang Satu Flyover",
            "cost":1,
            "exitTime":1623211879
        }
    ],
    }
```



# CarPlay Life Cycle

```Mermaid
sequenceDiagram
    CPApplicationDelegate-->>CarPlayManager: didConnectCarInterfaceController
    CPApplicationDelegate-->>CarPlayManager: didDisconnectCarInterfaceController
    CarPlayManager-->>CPApplicationDelegate: delegate
    CarPlaySearchController-->>CPApplicationDelegate: delegate
    CarPlayManager-->>NavigationViewController: beginNavigationWithCarPlay
    CPApplicationDelegate-->>NavigationViewController: didDisconnectFromCarPlay
    CarPlayManager-->>CarPlayManagerDelegate: navigationServiceFor
    CarPlayManager-->>CarPlayManagerDelegate: didBeginNavigationWith
    CarPlayManager-->>CarPlayManagerDelegate: carPlayManagerDidEndNavigation
```
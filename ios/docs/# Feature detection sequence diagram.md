# Feature detection sequence  diagram

```Mermaid
sequenceDiagram
    autonumber
    PassiveLocationDataSource-->PassiveLocationManager: Init    
    PassiveLocationManager-->NavigationMapView: Attached to
    PassiveLocationDataSource-->RoadObjectMatcher: delegate
    PassiveLocationDataSource-->RoadObjectsStore: delegate
    RoadObjectMatcherDelegate->>PassiveLocationDataSource: didMatch
    PassiveLocationDataSource->>RoadObjectsStore: addUserDefinedRoadObject
    NotificationCenter-->EHorizon: addObserve(.electronicHorizonDidUpdatePosition)    
    EHorizon->>NotificationCenter: didUpdateElectronicHorizonPosition
    NotificationCenter-->EHorizon: addObserve(.electronicHorizonDidEnterRoadObject)
    EHorizon->>NotificationCenter: didEnterOrExitRoadObject
    NotificationCenter-->EHorizon: addObserve(.electronicHorizonDidExitRoadObject)
    EHorizon->>NotificationCenter: didEnterOrExitRoadObject
    NotificationCenter-->EHorizon: addObserve(.electronicHorizonDidPassRoadObject)
    EHorizon->>NotificationCenter: didRoadObjectPassed


    

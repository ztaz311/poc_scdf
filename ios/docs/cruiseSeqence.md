## Sequence Diagram

## For Mobile

```Mermaid
sequenceDiagram
    autonumber
    AppDelegate->>AppDelegate: didFinishLaunchingWithOptions
    AppDelegate->>AWSAuth: startMonitor
    AppDelegate->>DataCenter: isSignedIn = true        
    DataCenter->>DataCenter: load    
    AppDelegate->>MapLandingVC: sceneWillEnterForeground
    MapLandingVC->>MapLandingVC: viewLoaded: 
    LocationManager->>MapLandingVC: speed > 20
    MapLandingVC->>NavigationMapView: startCruiseMode    
    MapLandingVC->>DataCenter: addERPListner
    MapLandingVC->>DataCenter: addTrafficeIncidentListner
    DataCenter->>MapLandingVC: ERP updated
    MapLandingVC->>NavigationMapView: addERP    
    DataCenter->>MapLandingVC: Traffice updated
    MapLandingVC->>NavigationMapView: addTraffic
    LocationTrackManager->>MapLandingVC: didUpdate
    MapLandingVC->>NavigationMapView: showNotification
```

## For CarPlay

```Mermaid
sequenceDiagram
    autonumber
    AppDelegate->>AppDelegate: didFinishLaunchingWithOptions
    AppDelegate->>AWSAuth: startMonitor
    AppDelegate->>DataCenter: isSignedIn = true        
    DataCenter->>DataCenter: load    
    AppDelegate->>CarPlayMapViewController: didConnect
    LocationManager->>CarPlayMapViewController: speed > 20
    CarPlayMapViewController->>NavigationMapView: startCruiseMode    
    CarPlayMapViewController->>DataCenter: addERPListner
    CarPlayMapViewController->>DataCenter: addTrafficeIncidentListner
    DataCenter->>CarPlayMapViewController: ERP updated
    CarPlayMapViewController->>NavigationMapView: addERP    
    DataCenter->>CarPlayMapViewController: Traffice updated
    CarPlayMapViewController->>NavigationMapView: addTraffic
    LocationTrackManager->>CarPlayMapViewController: didUpdate
    CarPlayMapViewController->>NavigationMapView: showNotification
```
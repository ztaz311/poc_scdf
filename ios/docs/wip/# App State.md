# App State

```Mermaid
stateDiagram-v2
    [*] --> initial: app launched
    initial --> loggedIn: log in
    loggedIn --> home
    home --> cruise: speed > 20km or ETA
    cruise --> home: end by user
    home --> routePlanning: search
    routePlanning --> home: Back 
    routePlanning --> navigation
    navigation --> home: arrival or cancel by user
    home --> initial: log out
```
# How notification works

## Class Diagram

```Mermaid
classDiagram
    class DataCenter {
        erpUpdater: ERPUpdater
        trafficUpdater: TrafficIncidentUpdater
        schoolZoneSVC: SchoolZoneServiceProtocol
        silveZoneSVC: SilverZoneServiceProtocol

        realtimeEngine: RealtimeEngineProtocol!
        setup()
        load() 
        callERPRefreshTimer()
        addERPUpdaterListner()
    }

    class ERPUpdater {
        erpJsonData: ERP Data from api/app sync
        erpService: ERPServiceProtocol
        listeners: Multiple listeners
        currentCostFeatureCollection: Points to be shown on map
        currentNoCostFeatureCollection: Points to be shown on map
        erpAllFeatureCollection: FeatureCollection
        erpFeatureDetectionCostFeatures: Line String for EH
        erpFeatureDetectionNoCostFeatures: Line String for EH

        load()
        update()
        unload()
    }

    class TrafficIncidentUpdater {
        service: TrafficIncidentServiceProtocol
        listeners: Multiple listeners

        load()
        unload()
    }

    class FeatureDetector {
    }

    class MapLandingViewModel {        
    }

    class TurnByTurnNavigationViewModel {        
    }

    DataCenter *-- "1" ERPUpdater: has
    DataCenter *-- "1" TrafficIncidentUpdater: has
    DataCenter *-- "1" SchoolZoneServiceProtocol: has
    DataCenter *-- "1" SilverZoneServiceProtocol: has
    ERPUpdater ..> ERPServiceProtocol
    ERPServiceProtocol <|-- ERPService
    ERPServiceProtocol <|-- MockERPService

    TrafficIncidentUpdater ..> TrafficIncidentServiceProtocol
    TrafficIncidentServiceProtocol <|-- TrafficIncidents
    TrafficIncidentServiceProtocol <|-- MockTrafficIncidentService

    SchoolZoneServiceProtocol <|-- SilverZoneService
    SchoolZoneServiceProtocol <|-- MockSilverZoneService

    SilverZoneServiceProtocol <|-- SchoolZoneService
    SilverZoneServiceProtocol <|-- MockSchoolZoneService
     
    DataCenter ..> RealtimeEngineProtocol
    RealtimeEngineProtocol <|-- AppSync
    RealtimeEngineProtocol <|-- MockAppSync

    MapLandingViewModel *-- "1" FeatureDetector: has
    TurnByTurnNavigationViewModel *-- "1" FeatureDetector: has
```

## Sequence Diagram

```Mermaid
sequenceDiagram
    AppDelegate->>DataCenter: didFinishLaunchingWithOptions
    DataCenter->>ERPUpdater: setup
    DataCenter->>TrafficUpdater: setup
    DataCenter->>SchoolZoneServiceProtocol: setup
    DataCenter->>SilverZoneServiceProtocol: setup
    AppDelegate->>DataCenter: isLogged In
    DataCenter->>ERPUpdater: load
    ERPUpdater->>ERPServiceProtocol: call API - getERPDetails()
    DataCenter->>TrafficUpdater: load
    TrafficUpdater->>TrafficIncidentServiceProtocol: call API - getTrafficIncidents()
    DataCenter->>SchoolZoneServiceProtocol: load
    Note over DataCenter,SchoolZoneServiceProtocol: getSchoolZone
    DataCenter->>SilverZoneServiceProtocol: load
    Note over DataCenter,SilverZoneServiceProtocol: getSilverZone    
    RealtimeEngineProtocol->>DataCenter: ERP updated in App Sync    
    AppDelegate->>DataCenter: sceneWillEnterForeground
    DataCenter->>ERPUpdater: callERPRefreshTimer    
    DataCenter->>ERPUpdater: update
    ERPUpdater->>ERPUpdater: refreshERP
    Note over ERPUpdater,ERPUpdater: triggered by timer
    AppDelegate->>DataCenter: isLogged out
    DataCenter->>ERPUpdater: unload
    LocationManager ->> MapLandingViewModel: startCruise
    MapLandingViewModel ->> FeatureDetector: setupFeatureDetector    

```


## TODO

- Remove `private weak var mapView: MapView!` in `TrafficIncidentUpdater.swift` since we don't update map directly here
- Remove `private var isBelowPuck = false` in `TrafficIncidentUpdater.swift`
- Rename the service `TrafficIncidents` to `TrafficIncidentsService`
- Comment out `Realm database`
- Error handling when api call failed or App Sync failed
- Using the same notification UI in both **Map landing** and **navigation**
- Do the same refresh algorithm for `TrafficUpdater`

# App Archtecture





Onboarding
OnboardingTNC - mobile_signup(NewSignUpMobileVC) - mobile_signin(SignInViewController)


setupUsername(SetUserNameVC)


OTPVerifyVC - onboarding_sms_verify


```Mermaid

flowchart TD
    SplashVC --> A{isOnboardingShown?};
    A -->|Yes| B{isSignedIn?};
    B -->|Yes| MapsLandingVC
    B -->|NO| LandingViewController;
    A -->|NO| OnboardingVC
    OnboardingVC --> B
    LandingViewController --> |loginClicked| SignInViewController
    LandingViewController --> |signUp| WebVC
    WebVC --> |acceptBtnAction| NewSignUpMobileVC
    WebVC --> |backbtnAction| LandingViewController
    SettingsTVC --> |row = 0| UserAccountContainer
    SettingsTVC --> |row = 1| SettingsDetailVC
    SettingsDetailVC -.- |segue=mapDisplaySegue| MapDisplayTVC
    SettingsDetailVC -.- |embed| RoutePrefTVC
    MapsLandingVC --> |ReactNative| Search[[UIViewController]]
    MapsLandingVC -.- BottomContainerVC
    BottomContainerVC -.- |Tab| HomeTabVC
    BottomContainerVC -.- |Tab| NotifyETATabVC
    BottomContainerVC -.- |Tab| OthersVC
    OthersVC --> SettingsVC
    OthersVC --> |ReactNative| Favorite[[UIViewController]]
    OthersVC --> TrvaelLogListVC
    SettingsVC -.- SettingsTVC
    SettingsVC -.- |Not used?| SettingsValuesTVC
```
# Expected amenities API for dynamically add new amenity

## Prerequirement

- Keep the same data structure for amenity so that we're able to add it dynamically.

```
{
    "amenities": [
        {
            "type": "evcharger",
            "icon_url": "https://address/evchager.png",
            "items": [
                {
                    ...                    
                },
                {
                    ...
                }
            ]
        },
        {
            "type": "petrol",
            "icon_url": "https://address/petrol.png",
            "items": [
                {
                    ...
                },
                {
                    ...
                },
                {
                    ...
                }
            ]
        }
    ]
}
```
# PassiveLocationManager

```Mermaid
classDiagram
class PassiveLocationManager{
    +String owner
    +BigDecimal balance
    +deposit(amount)
    +withdrawl(amount)
}

class PassiveLocationProvider{
    locationManager
}
```
# App 

```Mermaid
classDiagram
SplashVC .. OnboardingVC: onboard not shown
SplashVC .. MapLandingVC: is logged in
OnboardingVC ..> SplashVC: delegate
SplashVC .. LandingViewController: not logged in
LandingViewController .. SignInViewController : Sign In
LandingViewController .. TermsAndCondVC: Create account
TermsAndCondVC .. NewSignUpMobileVC: Accept
NewSignUpMobileVC .. SignInViewController: Account exist
NewSignUpMobileVC .. OTPVerifyVC: New user
SignInViewController .. SetUserNameVC: No name
SignInViewController .. MapLandingVC: User has a name
MapLandingVC .. Search: Search
MapLandingVC .. CruiseVC: speed over 20km/h
MapLandingVC .. WalkingNavigationVC
CruiseVC ..> MapLandingVC: End
Search .. NewRoutePlanningVC
NewRoutePlanningVC .. CarParkVC
NewRoutePlanningVC .. ETA
NewRoutePlanningVC .. TurnByTurnNavigationVC
NavigationViewController <|-- TurnByTurnNavigationVC
TurnByTurnNavigationVC ..* NavProgressVC
MapLandingVC .. UIViewController: getRNViewBottomSheet
UIViewController ..* HomeTab: Home (Embedded)
UIViewController ..* SavedTab: Saved (Embedded)
UIViewController ..* MoreTab: More (Embedded)
UIViewController ..* InboxTab: Inbox (Embedded)
HomeTab .. NewRoutePlanningVC: Click destination or favorite
MoreTab .. SettingsVC: Settings
MoreTab .. TravelLogListVC
TravelLogListVC .. TripLogSummaryVC
SettingsVC ..* SettingsTVC: Contains
SettingsTVC .. UserAccountContainer: Account
UserAccountContainer ..* UserAccountSocial: providerName != ""
UserAccountContainer ..* UserAccountTVC: providerName == ""
UserAccountTVC .. ChangeViewContainer: Clicked the tableViewCell
ChangeViewContainer ..* ChangeNameVC: isChangeName = true
ChangeViewContainer ..* UpdateEmailVC: isChangeName = false
SettingsTVC .. SettingsDetailVC: Map Display
SettingsDetailVC ..* MapDisplayTVC
SettingsDetailVC ..* RoutePrefTVC
SettingsTVC .. PrivacypolicyVC: Privacy

class MapLandingVC {
    bottomVC: UIView
    viewModel: MapLandingViewModel
}

class CruiseVC {
    viewModel: CruiseViewModel
}

class TurnByTurnNavigationVC {
    viewModel: TurnByTurnNavigationViewModel
}

class Search {
    RN Component
}

```
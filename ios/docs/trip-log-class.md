# Trip log
    
## Class diagram

```Mermaid
classDiagram
    class TripLogServiceProtocol { 
        <<Protocol>>
        +sendTripSummary(summary: TripSummary)
        +sendTripLog(tripLog: TripLog)
    }

    TripLogServiceProtocol *-- TripLogService
    TripLogServiceProtocol *-- MockTripLogService

    class TripLogger {
        +init(service: TripLogServiceProtocol)
        +update(location)
        +start(location)
        +stop(location)
        -log(status)
        -logSummary()
    }

    class TripLog {

    }

    class TripSummary {

    }

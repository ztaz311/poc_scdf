import { observer } from 'mobx-react'
import React, { memo, useCallback, useEffect, useRef, useState } from 'react'
import { Platform, StyleSheet, View } from 'react-native'
import 'react-native-gesture-handler'
import { enableScreens } from 'react-native-screens'
import Toast from 'react-native-toast-message'
import uuid from 'uuid-random'
import AlertAndroidComponent from './app/src/components/AlertAndroid'
import SimpleLoadingIndicatorComponent from './app/src/components/SimpleLoadingIndicator'
import './app/src/constants/appImages'
import { AppThemeProvider } from './app/src/contexts/ThemeContext'
import LandingRouter from './app/src/screens/LandingRouter'
import AlertService from './app/src/services/alert.service'
import AuthService from './app/src/services/auth.service'
import NavigationDataService, { ParamsFromNative } from './app/src/services/navigation-data.service'

export const RootTagContext = React.createContext<string>('RootTagContext')

interface AppProps {
  rootTag: number
  toScreen?: string
  navigationParams?: ParamsFromNative
  screenParams?: any
}

const App: React.FC<AppProps> = props => {
  const [isLoading, setIsLoading] = useState(true)
  const screenID = useRef(uuid())
  useEffect(() => {
    console.log('React Native created, rootTag', props.rootTag, props.toScreen, screenID.current)

    if (Platform.OS === 'android') {
      // Disable when using Android fragment
      // Because RN screens conflicts with outer fragment in native activity
      // Disable to support Change theme https://developer.android.com/guide/topics/ui/look-and-feel/darktheme#change-themes
      enableScreens(false)
    }

    AuthService.setIdToken(props.navigationParams?.idToken)

    NavigationDataService.addNativeData({
      rootTag: props.rootTag,
      toScreen: props.toScreen,
      paramsFromNative: props.navigationParams,
    })
    setIsLoading(false)

    return () => {
      NavigationDataService.popNavigationNativeData()
      if (props.toScreen) AlertService.removeRef(props.toScreen, screenID.current)
    }
  }, [props.navigationParams, props.rootTag, props.toScreen])

  const renderScreen = useCallback(() => {
    return <LandingRouter />
    // if (props.toScreen === reactScreens.Landing) {
    //   return <LandingRouter />
    // }
    // if (props.toScreen && props.toScreen in hybridScreens) {
    //   return (
    //     <View style={styles.app} pointerEvents="box-none">
    //       <HybridRouter initialRouteName={props.toScreen} navigationParams={props.navigationParams} />
    //     </View>
    //   )
    // }
    // return <HybridComponents routeName={props.toScreen} navigationParams={props.navigationParams} />
  }, [props])

  return (
    <View style={[styles.app]} pointerEvents="box-none">
      <AppThemeProvider isDarkMode={props.navigationParams?.isDarkMode}>
        {isLoading ? (
          <SimpleLoadingIndicatorComponent />
        ) : (
          <View pointerEvents="box-none" style={[styles.app]}>
            {renderScreen()}
            <AlertAndroidComponent
              ref={_ref => {
                if (_ref && props.toScreen) {
                  AlertService.addRef(props.toScreen, _ref, screenID.current)
                }
              }}
            />
          </View>
        )}
      </AppThemeProvider>
      <Toast />
    </View>
  )
}

const styles = StyleSheet.create({
  app: {
    flex: 1,
  },
})

export default memo(observer(App))

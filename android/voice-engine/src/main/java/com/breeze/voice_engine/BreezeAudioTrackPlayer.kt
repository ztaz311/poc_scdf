package com.breeze.voice_engine

import android.content.Context
import android.media.AudioAttributes
import android.media.AudioFormat
import android.media.AudioManager
import android.media.AudioTrack
import com.breeze.voice_engine.constant.Constants
import com.breeze.voice_engine.service.exception.UnsupportedFormatException
import java.util.Queue
import java.util.concurrent.ConcurrentLinkedQueue
import kotlin.math.floor

class BreezeAudioTrackPlayer<T>(
    private var context: Context,
    audioChannel: Int,
    audioEncoding: Int,
    sampleRate: Int,
    var isMuted: Boolean = false,
) {
    init {
        isMuted =
            context.getSharedPreferences(Constants.SHARE_USER_PREFERENCE_NAME, Context.MODE_PRIVATE)
                .getBoolean(Constants.SharePreferenceKeys.KEY_VOICE_STATE, false)
    }

    data class AudioData<T>(
        val data: T,
        val distortRate: Float = 1.0f,
    )

    private val audioFocusDelegate: BreezeAudioFocusDelegate = buildBreezeAudioFocusDelegate()
    private val playBackQueue: Queue<AudioData<T>> = ConcurrentLinkedQueue()
    private val audioTrackBuffer =
        AudioTrack.getMinBufferSize(sampleRate, audioChannel, audioEncoding)
    private val audioTrack =
        AudioTrack(
            AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_ASSISTANCE_NAVIGATION_GUIDANCE)
                .setContentType(AudioAttributes.CONTENT_TYPE_SPEECH)
                .build(),
            AudioFormat.Builder()
                .setEncoding(audioEncoding)
                .setSampleRate(sampleRate)
                .setChannelMask(audioChannel)
                .build(),
            audioTrackBuffer * 2,
            AudioTrack.MODE_STREAM,
            AudioManager.AUDIO_SESSION_ID_GENERATE
        )

    fun startAudioStream() {
        audioTrack.play()
    }

    fun setVolume(volumeGain: Float) {
        audioTrack.setVolume(volumeGain)
    }


    fun playAudio(
        audioBuffer: T,
        distortRate: Float = 1.0f
    ) {
        if (context.getSharedPreferences(Constants.SHARE_USER_PREFERENCE_NAME, Context.MODE_PRIVATE)
                .getBoolean(Constants.SharePreferenceKeys.KEY_VOICE_STATE, false)
        ) {
            if (playBackQueue.size > 0)
                playBackQueue.clear()
            return
        }
        val audioData = AudioData(data = audioBuffer, distortRate = distortRate)
        if (audioData.data !is ShortArray && audioData.data !is FloatArray) {
            throw UnsupportedFormatException("Buffer format not supported")
        }
        playBackQueue.add(audioData)
        if (playBackQueue.size == 1) {
            playAudioInternal()
        }
    }

    fun destroy() {
        stopPlayBack()
        audioTrack.release()
    }

    fun stop() {
        stopPlayBack()
    }

    private fun stopPlayBack() {
        playBackQueue.clear()
        audioFocusDelegate.abandonAudioFocusRequest()
        audioTrack.flush()
        audioTrack.stop()
    }

    private fun playAudioInternal() {
        if (playBackQueue.isEmpty())
            return

        val audioData = playBackQueue.peek()
        (audioData?.data as? ShortArray)?.let { data ->
            if (audioFocusDelegate.requestAudioFocus()) {
                playAudioBuffer(data, audioData.distortRate)
            }
        }
        (audioData?.data as? FloatArray)?.let { data ->
            if (audioFocusDelegate.requestAudioFocus()) {
                playAudioBuffer(data, audioData.distortRate)
            }
        }

        pollAndPlayNextItem()
    }

    //TODO: verify below functionality
    private fun playAudioBuffer(audioBuffer: ShortArray, distortRate: Float) {
        val croppedLength = audioBuffer.size - (audioBuffer.size % audioTrackBuffer)
        val dataToCopy = ShortArray(audioTrackBuffer)
        for (i in 0 until croppedLength step audioTrackBuffer) {
            if (isMuted) {
                audioFocusDelegate.abandonAudioFocusRequest()
                audioTrack.flush()
                playBackQueue.clear()
                break
            }
            for (index in dataToCopy.indices) {
                dataToCopy[index] = floor(audioBuffer[index + i] * distortRate).toInt().toShort()
            }
            audioTrack.write(
                dataToCopy,
                0,
                audioTrackBuffer,
                AudioTrack.WRITE_BLOCKING
            )
        }
        if (croppedLength < audioBuffer.size) {
            audioTrack.write(
                audioBuffer.copyOfRange(croppedLength, audioBuffer.size),
                0,
                audioBuffer.size - croppedLength,
                AudioTrack.WRITE_BLOCKING
            )
        }
    }

    private fun playAudioBuffer(audioBuffer: FloatArray, distortRate: Float) {
        var lastInd = 0
        for (i in audioBuffer.indices step audioTrackBuffer) {
            lastInd = i
            if (i + audioTrackBuffer >= audioBuffer.size) {
                break
            }
            audioTrack.write(
                audioBuffer.copyOfRange(i, i + audioTrackBuffer), 0, audioTrackBuffer,
                AudioTrack.WRITE_BLOCKING
            )
        }
        audioTrack.write(
            audioBuffer.copyOfRange(lastInd, audioBuffer.size), 0, audioBuffer.size - lastInd,
            AudioTrack.WRITE_BLOCKING
        )
    }

    private fun pollAndPlayNextItem() {
        playBackQueue.poll()
        audioFocusDelegate.abandonAudioFocusRequest()
        if (playBackQueue.size > 0) {
            playAudioInternal()
        }
    }


    private fun buildBreezeAudioFocusDelegate() = BreezeAudioFocusDelegate(
        context.getSystemService(
            Context.AUDIO_SERVICE
        ) as AudioManager
    )


}
package com.breeze.voice_engine.service

import android.content.Context

interface BreezeSpeechService {
    fun setup(context: Context)
    fun playVoiceInstructions(
        voiceStringPlain: String,
        voiceStringSSML: String?,
        distanceAlongGeometry: Double = -1.0
    )

    fun stopSpeech()
    fun toggleMute(isVoiceMuted: Boolean)
    fun destroy()
}
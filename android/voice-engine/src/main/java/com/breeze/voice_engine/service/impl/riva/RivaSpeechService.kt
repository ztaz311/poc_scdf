package com.breeze.voice_engine.service.impl.riva

import android.content.Context
import android.media.AudioFormat
import android.util.Log
import com.breeze.voice_engine.BreezeAudioTrackPlayer
import com.breeze.voice_engine.BuildConfig
import com.breeze.voice_engine.service.BreezeSpeechService
import com.breeze.voice_engine.utils.VoiceInstructionsHelper
import com.google.protobuf.ByteString
import io.grpc.ManagedChannel
import io.grpc.ManagedChannelBuilder
import io.grpc.stub.StreamObserver
import nvidia.riva.RivaAudio
import nvidia.riva.tts.RivaSpeechSynthesisGrpc
import nvidia.riva.tts.RivaTts
import java.lang.Math.max
import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import kotlin.math.abs

class RivaSpeechService : BreezeSpeechService {
    private var audioTrackPlayer: BreezeAudioTrackPlayer<ShortArray>? = null
    private var singleThreadExecutor: ExecutorService? = null
    private val channel: ManagedChannel =
        ManagedChannelBuilder.forTarget(BuildConfig.TARGET_RIVA_SPEECH).usePlaintext().build()


    override fun setup(context: Context) {
        singleThreadExecutor = Executors.newSingleThreadExecutor()
        audioTrackPlayer = BreezeAudioTrackPlayer(
            context,
            AudioFormat.CHANNEL_OUT_MONO,
            AudioFormat.ENCODING_PCM_16BIT,
            RIVA_SAMPLE_RATE,
        )
    }

    override fun toggleMute(isVoiceMuted: Boolean) {
        audioTrackPlayer?.isMuted = isVoiceMuted
//        if (isVoiceMuted) {
//            audioTrackPlayer?.setVolume(0.0F)
//        } else {
//            audioTrackPlayer?.setVolume(1.0F)
//        }
    }

    override fun destroy() {
        stopSynthesizing()
        if (audioTrackPlayer != null) {
            singleThreadExecutor!!.shutdownNow()
            audioTrackPlayer!!.destroy()
        }
        channel.shutdown()
    }


    override fun playVoiceInstructions(
        voiceStringPlain: String,
        voiceStringSSML: String?,
        distanceAlongGeometry: Double
    ) {
        val speakingRunnable =
            SpeakingRunnable(audioTrackPlayer!!, channel)
        speakingRunnable.setContent(
            voiceStringPlain,
            false
        ) //ToDo - Find a way to pass SSML string here
        //speakingRunnable!!.synthesizer.SpeakSsml(voiceString) ??!!
        singleThreadExecutor?.let {
            if (!it.isShutdown) {
                it.execute(speakingRunnable)
            }
        }
    }

    override fun stopSpeech() {
        stopSynthesizing()
    }


    internal class SpeakingRunnable(
        val audioTrack: BreezeAudioTrackPlayer<ShortArray>,
        val channel: ManagedChannel
    ) : Runnable {
        private var content: String? = null
        private var isSSML: Boolean = false

        fun setContent(content: String?, isSSML: Boolean) {
            this.content = content
            this.isSSML = isSSML
        }

        override fun run() {
            if (content.isNullOrEmpty()) return
            kotlin.runCatching {
                audioTrack.startAudioStream()
                val blockingStub = RivaSpeechSynthesisGrpc.newStub(channel);
                val req = RivaTts.SynthesizeSpeechRequest.newBuilder()
                    .setText(VoiceInstructionsHelper.addFullStop(content!!))
                    .setLanguageCode(LANGUAGE_CODE)
                    .setEncoding(RivaAudio.AudioEncoding.LINEAR_PCM)
                    .setSampleRateHz(RIVA_SAMPLE_RATE)
                    .setVoiceName(VOICE_NAME).build()
                blockingStub.synthesize(req,
                    object : StreamObserver<RivaTts.SynthesizeSpeechResponse> {
                        var completeBytes: ByteString = ByteString.EMPTY
                        override fun onNext(value: RivaTts.SynthesizeSpeechResponse?) {

                            value?.audio?.let { byteString ->
                                completeBytes = completeBytes.concat(byteString)
                            }
                        }

                        override fun onError(t: Throwable?) {
                            //No op
                            Log.e("Speech Synthesis Demo", "unexpected " + t?.message)
                        }

                        override fun onCompleted() {
                            val byteArray = completeBytes.toByteArray()
                            val shortBuf =
                                ByteBuffer.wrap(byteArray).order(ByteOrder.LITTLE_ENDIAN)
                                    .asShortBuffer()
                            val out = ShortArray(shortBuf.capacity())
                            shortBuf.get(out)

                            val maxVal = max(abs(out.max().toInt()), abs(out.min().toInt()))
                            val distortionRate =
                                if (maxVal == 0) 1.0f else (Short.MAX_VALUE.toFloat()) / maxVal
                            audioTrack.playAudio(out, distortionRate)

//                                audioTrack.playAudio(ByteBuffer.wrap(byteArray).order(ByteOrder.LITTLE_ENDIAN))
                        }
                    })
            }.onFailure { e -> Log.e("Speech Synthesis Demo", "unexpected " + e.message) }
        }
    }

    private fun stopSynthesizing() {
        if (audioTrackPlayer != null) {
            audioTrackPlayer!!.stop()
        }
    }

    companion object {
        private const val LANGUAGE_CODE = "en-US"
        private const val VOICE_NAME = "breeze"
        const val RIVA_SAMPLE_RATE = 22050
    }

}
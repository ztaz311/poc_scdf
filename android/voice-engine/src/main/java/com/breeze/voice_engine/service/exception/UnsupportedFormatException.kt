package com.breeze.voice_engine.service.exception

class UnsupportedFormatException : Exception {
    constructor() : super()
    constructor(message: String) : super(message)
    constructor(message: String, cause: Throwable) : super(message, cause)
}
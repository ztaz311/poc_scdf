package com.breeze.voice_engine.service.impl

import android.content.Context
import android.os.Handler
import android.os.HandlerThread
import com.breeze.voice_engine.R
import com.breeze.voice_engine.constant.Constants
import com.breeze.voice_engine.service.BreezeSpeechService

import com.mapbox.api.directions.v5.models.VoiceInstructions
import com.mapbox.bindgen.Expected
import com.mapbox.navigation.ui.base.util.MapboxNavigationConsumer
import com.mapbox.navigation.ui.voice.api.MapboxSpeechApi
import com.mapbox.navigation.ui.voice.api.MapboxVoiceInstructionsPlayer
import com.mapbox.navigation.ui.voice.model.SpeechAnnouncement
import com.mapbox.navigation.ui.voice.model.SpeechError
import com.mapbox.navigation.ui.voice.model.SpeechValue
import com.mapbox.navigation.ui.voice.model.SpeechVolume
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.*

class MapboxSpeechService : BreezeSpeechService {

    private var voiceInstructionsPlayer: MapboxVoiceInstructionsPlayer? = null
    private lateinit var speechAPI: MapboxSpeechApi
    private lateinit var handlerThread: HandlerThread
    private lateinit var backgroundHandler: Handler

    private val voiceInstructionsPlayerCallback =
        MapboxNavigationConsumer<SpeechAnnouncement> { consumer -> speechAPI.clean(consumer) }

    private val speechCallback =
        MapboxNavigationConsumer<Expected<SpeechError, SpeechValue>> { state ->
            if (state.isValue) {
                val currentSpeechValue = state.value
                currentSpeechValue?.announcement?.let {
                    backgroundHandler.post {
                        voiceInstructionsPlayer?.play(
                            it,
                            voiceInstructionsPlayerCallback
                        )
                    }
                }

            } else {
                val currentSpeechError = state.error
                currentSpeechError?.fallback?.let {
                    backgroundHandler.post {
                        voiceInstructionsPlayer?.play(it, voiceInstructionsPlayerCallback)
                    }
                }
            }
        }

    override fun setup(context: Context) {
        handlerThread = HandlerThread(VOICE_BACKGROUND_THREAD_NAME)
        handlerThread.start()
        backgroundHandler = Handler(this.handlerThread.looper)
        speechAPI = MapboxSpeechApi(
            context,
            context.getString(R.string.mapbox_access_token),
            Constants.MAPBOX_SPEECH_API_LOCALE
        )

        voiceInstructionsPlayer = MapboxVoiceInstructionsPlayer(
            context,
            context.getString(R.string.mapbox_access_token),
            Locale.UK.language
        )
    }

    override fun playVoiceInstructions(
        voiceStringPlain: String,
        voiceStringSSML: String?,
        distanceAlongGeometry: Double
    ) {
        if (::speechAPI.isInitialized) {
            CoroutineScope(Dispatchers.IO).launch {
                val instructionBuilder = VoiceInstructions.builder()
                instructionBuilder.ssmlAnnouncement(voiceStringSSML)
                instructionBuilder.announcement(voiceStringPlain)
                if (distanceAlongGeometry != -1.0) {
                    instructionBuilder.distanceAlongGeometry(distanceAlongGeometry)
                }
                val voiceInstructions = instructionBuilder.build()
                speechAPI.generate(
                    voiceInstructions,
                    speechCallback
                )
            }
        }
    }

    override fun toggleMute(isVoiceMuted: Boolean) {
        if (isVoiceMuted) {
            changeVoiceInstructionsPlayer(SpeechVolume(0.0f))
        } else {
            changeVoiceInstructionsPlayer(SpeechVolume(1.0f))
        }
    }

    override fun stopSpeech() {
        speechAPI.cancel()
        voiceInstructionsPlayer?.shutdown()
    }

    override fun destroy() {
        speechAPI.cancel()
        voiceInstructionsPlayer?.shutdown()
        handlerThread.quitSafely()
    }

    private fun changeVoiceInstructionsPlayer(speechVolume: SpeechVolume) {
        voiceInstructionsPlayer?.volume(speechVolume)
    }

    companion object {
        private const val VOICE_BACKGROUND_THREAD_NAME = "SpeechHandlerThread"
    }
}
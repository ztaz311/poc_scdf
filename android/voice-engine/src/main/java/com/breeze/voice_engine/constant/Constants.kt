package com.breeze.voice_engine.constant

object Constants {

    const val MAPBOX_SPEECH_API_LOCALE = "en_GB"
    const val SHARE_USER_PREFERENCE_NAME = "Breeze_Preference"
    object SharePreferenceKeys
    {
        const val KEY_VOICE_STATE = "KEY_VOICE_STATE"
    }


    val LIST_VOICE_TEXT = mapOf(
        "PIE" to "Pee Aai Eee",
        "CTE" to "See Tee Eee",
        "KPE" to "Ke Pee Eee",
        "ECP" to "Ee See Pee",
        "AYE" to "Ae Vai Eee",
        "TPE" to "Tee Pee Eee",
        "SLE" to "Es Ell Eee",
        "KJE" to "Kai Jey Eee",
        "ERP" to "Eee Aar Pee",
        "MCE" to "Em See Eee",
        "BKE" to "Bee Ke Eee",
        "NSC" to "En Es See"
    )

    val LIST_VOICE_TEXT_SPACES = mapOf(
        "PIE" to "P I E ",
        "CTE" to "C T E ",
        "KPE" to "K P E ",
        "ECP" to "E C P ",
        "AYE" to "A Y E ",
        "TPE" to "T P E ",
        "SLE" to "S L E ",
        "KJE" to "K J E ",
        "ERP" to "E R P ",
        "MCE" to "M C E ",
        "BKE" to "B K E ",
        "NSC" to "N S C "
    )

    val LIST_VOICE_TEXT_IOS = mapOf(
        "PIE" to "P.I.E.",
        "CTE" to "C.T.E.",
        "KPE" to "K.P.E.",
        "ECP" to "E.C.P.",
        "AYE" to "A.Y.E.",
        "TPE" to "T.P.E.",
        "SLE" to "S.L.E.",
        "KJE" to "K.J.E.",
        "ERP" to "E.R.P.",
        "MCE" to "M.C.E.",
        "BKE" to "B.K.E.",
        "NSC" to "N.S.C."
    )

    val LIST_VOICE_TEXT_ORIGINAL = mapOf(
        "PIE" to "P.I.E",
        "CTE" to "C.T.E",
        "KPE" to "K.P.E",
        "ECP" to "E.C.P",
        "AYE" to "A.Y.E",
        "TPE" to "T.P.E",
        "SLE" to "S.L.E",
        "KJE" to "K.J.E",
        "ERP" to "E.R.P",
        "MCE" to "M.C.E",
        "BKE" to "B.K.E",
        "NSC" to "N.S.C"
    )
}

package com.breeze.voice_engine.service.impl

import android.content.Context
import android.media.AudioAttributes
import android.media.AudioManager
import android.media.MediaPlayer
import com.amazonaws.mobile.client.AWSMobileClient
import com.amazonaws.services.polly.AmazonPollyPresigningClient
import com.amazonaws.services.polly.model.DescribeVoicesRequest
import com.amazonaws.services.polly.model.OutputFormat
import com.amazonaws.services.polly.model.SynthesizeSpeechPresignRequest
import com.amazonaws.services.polly.model.Voice
import com.breeze.voice_engine.service.BreezeSpeechService
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import timber.log.Timber
import java.io.IOException
import java.net.URL
import java.util.*
import java.util.concurrent.ConcurrentLinkedQueue


class AwsPollySpeechService : BreezeSpeechService {

    private val mutex = Mutex()
    // Initialize the Amazon Cognito credentials provider.

    private lateinit var mediaPlayer: MediaPlayer
    private lateinit var client: AmazonPollyPresigningClient
    private var voices: List<Voice>? = null
    private val playBackQueue: Queue<PlayBack> = ConcurrentLinkedQueue()

    override fun setup(context: Context) {

        // Create a client that supports generation of presigned URLs.
        client = AmazonPollyPresigningClient(AWSMobileClient.getInstance())

        // Initialize media player
        initializeMediaPlayer()

    }

    private fun initializeMediaPlayer() {
        mediaPlayer = MediaPlayer()
        mediaPlayer.setOnPreparedListener { mp ->
            mp.start()
        }
        mediaPlayer.setOnCompletionListener { mp ->
            mp.release()
            initializeMediaPlayer()
            playBackQueue.poll()
            if (playBackQueue.size > 0) {
                play()
            }
        }
        mediaPlayer.setOnErrorListener { mp: MediaPlayer, what: Int, extra: Int ->
            return@setOnErrorListener false;
        }
    }

    override fun toggleMute(isVoiceMuted: Boolean) {
        //ToDo
    }

    override fun destroy() {
        cancelSpeech()
        client.shutdown()
    }

    fun preConnect() {
        //TODO
    }

    override fun playVoiceInstructions(
        voiceStringPlain: String,
        voiceStringSSML: String?,
        distanceAlongGeometry: Double
    ) {
        if (client == null) {
            Timber.v("Please initialize the AmazonPollyPresigningClient first\n")
            return
        }

        CoroutineScope(Dispatchers.IO).launch {
            // Create speech synthesis request.
            var synthesizeSpeechPresignRequest: SynthesizeSpeechPresignRequest =
                SynthesizeSpeechPresignRequest() // Set the text to synthesize.
                    .withText(voiceStringSSML ?: voiceStringPlain) // Select voice for synthesis.
                    .withVoiceId("Joanna") //
                    // Set format to MP3.
                    .withOutputFormat(OutputFormat.Mp3)

            // Get the presigned URL for synthesized speech audio stream.
            var presignedSynthesizeSpeechUrl: URL =
                client.getPresignedSynthesizeSpeechUrl(synthesizeSpeechPresignRequest)


            val audioAttributes: AudioAttributes =
                AudioAttributes.Builder().setLegacyStreamType(AudioManager.STREAM_MUSIC).build()
            mutex.withLock {
                playBackQueue.add(
                    PlayBack(
                        audioAttributes,
                        presignedSynthesizeSpeechUrl.toString()
                    )
                )
                if (playBackQueue.size == 1) {
                    play()
                }
            }
        }

    }

    suspend private fun fetchVoices() {
        if (voices == null) {
            val describeVoicesRequest = DescribeVoicesRequest()
            val describeVoicesResult = client.describeVoices(describeVoicesRequest)
            voices = describeVoicesResult.getVoices()
        }
    }

    override fun stopSpeech() {
        cancelSpeech()
        initializeMediaPlayer()
    }

    private fun cancelSpeech() {
        mediaPlayer.stop()
        mediaPlayer.release()
    }

    private fun play() {
        //TODO: audio focus should be used
        if (playBackQueue.isNotEmpty()) {
            val currentPlayCallback = playBackQueue.peek() ?: return
            mediaPlayer.setAudioAttributes(currentPlayCallback.audioAttributes)
            try {
                // Set media player's data source to previously obtained URL.
                mediaPlayer.setDataSource(currentPlayCallback.speechUrl)
            } catch (e: IOException) {
                Timber.e(e, "Unable to set data source for the media player! ")
            }
            mediaPlayer.prepareAsync()
        }
    }


    data class PlayBack(val audioAttributes: AudioAttributes, val speechUrl: String) {}

}
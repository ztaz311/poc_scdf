package com.breeze.voice_engine.utils

import android.util.Log
import com.breeze.voice_engine.constant.Constants
import org.w3c.dom.Document
import org.w3c.dom.Node
import org.w3c.dom.NodeList
import java.io.StringWriter
import java.io.Writer
import javax.xml.XMLConstants
import javax.xml.parsers.DocumentBuilder
import javax.xml.parsers.DocumentBuilderFactory
import javax.xml.transform.OutputKeys
import javax.xml.transform.Transformer
import javax.xml.transform.TransformerFactory
import javax.xml.transform.dom.DOMSource
import javax.xml.transform.stream.StreamResult

/**
 * Created by Aiswarya on 30,June,2021
 *
 * Voice instructions helps to find and replace mispronounced text with expected pause. eg: PIE to P.I.E
 *
 *
 */
object VoiceInstructionsHelper {
    const val XML_TAG_PROSODY = "prosody"
    const val XML_TAG_SAYAS = "say-as"
    const val TAG = "VoiceInstructionsHelper"
    suspend fun getModifiedVoiceInstruction(isXml: Boolean, instruction: String): String {
        if (isXml) {
            return parseXML(instruction)
        }
        return replaceVoiceInstruction(instruction)
    }

    private suspend fun replaceVoiceInstruction(instruction: String): String {
        if (instruction.isNullOrEmpty())
            return instruction
        var modifiedInstruction = instruction

        //split sentence to words
        val words = instruction.split("\\s+".toRegex()).map { word ->
            word.replace("""^[,\.]|[,\.]$""".toRegex(), "")
        }.toMutableList()

        // replace text
        for (i in words.indices) {
            val value = words[i]
            words[i] = replaceText(value, Constants.LIST_VOICE_TEXT_IOS)
        }

        //join replaced text to sentence
        modifiedInstruction = words.joinToString(" ")



        return modifiedInstruction
    }

    private suspend fun replaceText(text: String, map: Map<String, String>): String {
        var result = text
        map.forEach { t, u ->
            result = result.replace(t, u)
        }
        Log.d(TAG, "Replaced text=" + result)
        return result
    }

    /**
     * pareXML helps to parse the ssml text using DOM parser
     * (instruction) sample ssml: <speak><amazon:effect name="drc"><prosody rate="1.08">Drive towards PIE.</prosody></amazon:effect></speak>
     */
    private suspend fun parseXML(instruction: String): String {
        Log.d("testing-voice", "Processing instruction: " + instruction)
        val factory: DocumentBuilderFactory = DocumentBuilderFactory.newInstance()
        val builder: DocumentBuilder = factory.newDocumentBuilder()
        val document: Document = builder.parse(instruction.byteInputStream())

        traverseAndReplace(document.getDocumentElement(), XML_TAG_SAYAS)

        val outFactory: TransformerFactory = TransformerFactory.newInstance()
        outFactory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true)
        val xformer: Transformer = outFactory.newTransformer()
        xformer.setOutputProperty(OutputKeys.INDENT, "yes")
        val output: Writer = StringWriter()
        xformer.transform(DOMSource(document), StreamResult(output))
        val result: String = output.toString()

        return result
    }

    var speechText = ""
    private suspend fun traverse(node: Node): String {
        val list: NodeList = node.getChildNodes()
        loop@ for (i in 0 until list.getLength()) {
            val currentNode: Node = list.item(i)
            if (speechText.isNotEmpty()) {
                break@loop;
            } else {
                traverse(currentNode)
            }
        }
        if (node.getNodeName().equals(XML_TAG_SAYAS)) { // contains actual speechtext
            if (node.textContent.isNotEmpty()) {
                speechText = node.textContent
            }
        }
        return speechText;
    }

    private suspend fun traverseAndReplace(node: Node, searchFor: String) {
        val list: NodeList = node.getChildNodes()
        loop@ for (i in 0 until list.getLength()) {
            val currentNode: Node = list.item(i)
            traverseAndReplace(currentNode, searchFor)
        }
        if (node.getNodeName().equals(searchFor)) {
            if (node.textContent.isNotEmpty()) {
                node.textContent = replaceVoiceInstruction(node.textContent)
            }
        }
    }

    fun addFullStop(originalContent: String): String {
        val trimmedContent = originalContent.trim()
        if (trimmedContent.isEmpty()) {
            return originalContent
        }
        val lastChar = trimmedContent.last()
        return if (lastChar != '.') {
            "$trimmedContent."
        } else {
            trimmedContent
        }
    }

}
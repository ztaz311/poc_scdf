package com.breeze.voice_engine.service.impl.triton

import android.content.Context
import android.media.AudioFormat
import android.util.Log
import com.breeze.voice_engine.BreezeAudioTrackPlayer
import com.breeze.voice_engine.service.BreezeSpeechService
import com.google.protobuf.ByteString
import io.grpc.ManagedChannel
import io.grpc.ManagedChannelBuilder
import io.grpc.stub.StreamObserver
import nvidia.triton.GRPCInferenceServiceGrpc
import nvidia.triton.GrpcService
import okio.ByteString.Companion.encode
import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

class TritonGrpcSpeechService : BreezeSpeechService {


    private var audioTrackPlayer: BreezeAudioTrackPlayer<ShortArray>? = null
    private var singleThreadExecutor: ExecutorService? = null
    private val channel: ManagedChannel =
        ManagedChannelBuilder.forTarget(TARGET).usePlaintext().build()

    override fun setup(context: Context) {


        singleThreadExecutor = Executors.newSingleThreadExecutor()
        audioTrackPlayer = BreezeAudioTrackPlayer(
            context,
            AudioFormat.CHANNEL_OUT_MONO,
            AudioFormat.ENCODING_PCM_16BIT,
            22050
        )
        createSynthesizer(context)
    }

    override fun toggleMute(isVoiceMuted: Boolean) {
        if (isVoiceMuted) {
            audioTrackPlayer?.setVolume(0.0F)
        } else {
            audioTrackPlayer?.setVolume(1.0F)
        }
    }

    override fun destroy() {
        stopSynthesizing()

        if (audioTrackPlayer != null) {
            singleThreadExecutor!!.shutdownNow()
            audioTrackPlayer!!.destroy()
        }
        channel.shutdown()
    }


    override fun playVoiceInstructions(
        voiceStringPlain: String,
        voiceStringSSML: String?,
        distanceAlongGeometry: Double
    ) {
        val speakingRunnable =
            SpeakingRunnable(audioTrackPlayer!!, channel)
        speakingRunnable.setContent(
            voiceStringPlain,
            false
        ) //ToDo - Find a way to pass SSML string here
        //speakingRunnable!!.synthesizer.SpeakSsml(voiceString) ??!!
        singleThreadExecutor!!.execute(speakingRunnable)
    }

    override fun stopSpeech() {
        stopSynthesizing()
    }

    private fun createSynthesizer(context: Context) {
    }

    internal class SpeakingRunnable(
        val audioTrack: BreezeAudioTrackPlayer<ShortArray>,
        val channel: ManagedChannel
    ) : Runnable {
        private var content: String? = null
        private var isSSML: Boolean = false

        fun setContent(content: String?, isSSML: Boolean) {
            this.content = content
            this.isSSML = isSSML
        }

        override fun run() {
            content?.let { content ->
                try {
                    audioTrack.startAudioStream()

                    val trimmedContent = content.trim()
                    val lastChar = trimmedContent.last()
                    val formattedContent = if (lastChar != '.') {
                        "$trimmedContent."
                    } else {
                        trimmedContent
                    }
                    val grpcStub = GRPCInferenceServiceGrpc.newStub(channel)
                    val byteContent = formattedContent.toCharArray().map {
                        ByteString.copyFrom(
                            it.toString().encode(Charsets.UTF_8).toByteArray()
                        )
                    }
                    val modelReq = GrpcService.ModelInferRequest.newBuilder()
                    modelReq.modelName = MODEL_NAME
                    val input0Data = GrpcService.InferTensorContents.newBuilder()
                    input0Data.addAllBytesContents(byteContent)

                    val input0 = GrpcService.ModelInferRequest.InferInputTensor
                        .newBuilder()
                    input0.name = INPUT_NAME
                    input0.datatype = "BYTES"
                    input0.addShape(1)
                    input0.addShape(byteContent.size.toLong())
                    input0.setContents(input0Data)
                    modelReq.addAllInputs(listOf(input0.build()));
                    val output0 = GrpcService.ModelInferRequest.InferRequestedOutputTensor
                        .newBuilder()
                    output0.name = OUTPUT_NAME
                    modelReq.addAllOutputs(listOf(output0.build()));
                    grpcStub.modelInfer(modelReq.build(), object :
                        StreamObserver<nvidia.triton.GrpcService.ModelInferResponse> {
                        var completeBytes: ByteString = ByteString.EMPTY
                        override fun onNext(value: GrpcService.ModelInferResponse?) {
                            value?.rawOutputContentsList?.get(0)?.let {
                                completeBytes = completeBytes.concat(it)
                            }
                        }

                        override fun onError(t: Throwable?) {
                            //no op
                        }

                        override fun onCompleted() {
                            val byteArray = completeBytes.toByteArray()
                            val shortBuf = ByteBuffer.wrap(byteArray).order(ByteOrder.LITTLE_ENDIAN)
                                .asShortBuffer()
                            val out = ShortArray(shortBuf.capacity())
                            shortBuf.get(out)
                            audioTrack.playAudio(out)
                        }
                    })
                } catch (ex: Exception) {
                    Log.e("Speech Synthesis", "unexpected " + ex.stackTraceToString())
                }
            }
        }
    }

    private fun stopSynthesizing() {
        if (audioTrackPlayer != null) {
            audioTrackPlayer!!.stop()
        }
    }

    companion object {
        private const val TARGET = "breeze-dev.coderuse.net:8001"
        private const val TIME_OUT = 150000
        private const val MODEL_NAME = "nemo_model1"
        private const val INPUT_NAME = "input__0"
        private const val OUTPUT_NAME = "output__0"
    }
}
package com.breeze.voice_engine.service.impl

import android.content.Context
import android.media.AudioAttributes
import android.media.AudioFormat
import android.media.AudioManager
import android.media.AudioTrack
import android.util.Log
import com.breeze.voice_engine.service.BreezeSpeechService
import com.microsoft.cognitiveservices.speech.*
import com.microsoft.cognitiveservices.speech.util.EventHandler
import timber.log.Timber
import java.util.*
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

class AzureSpeechService : BreezeSpeechService {

    private val speechSubscriptionKey = "1933617640504feab3f920855f1da019"
    private val serviceRegion = "southeastasia"
    private var speechConfig: SpeechConfig? = null
    private var synthesizer: SpeechSynthesizer? = null
    private var connection: Connection? = null
    private var audioTrack: AudioTrack? = null
    private var speakingRunnable: SpeakingRunnable? = null
    private var singleThreadExecutor: ExecutorService? = null
    private val synchronizedObj = Any()


    override fun setup(context: Context) {
        singleThreadExecutor = Executors.newSingleThreadExecutor()

        audioTrack = AudioTrack(
            AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_MEDIA)
                .setContentType(AudioAttributes.CONTENT_TYPE_SPEECH)
                .build(),
            AudioFormat.Builder()
                .setEncoding(AudioFormat.ENCODING_PCM_16BIT)
                .setSampleRate(24000)
                .setChannelMask(AudioFormat.CHANNEL_OUT_MONO)
                .build(),
            AudioTrack.getMinBufferSize(
                24000,
                AudioFormat.CHANNEL_OUT_MONO,
                AudioFormat.ENCODING_PCM_16BIT
            ) * 2,
            AudioTrack.MODE_STREAM,
            AudioManager.AUDIO_SESSION_ID_GENERATE
        )

        createSynthesizer(context)
        speakingRunnable = SpeakingRunnable(audioTrack!!, synthesizer!!)
    }

    override fun toggleMute(isVoiceMuted: Boolean) {
        //ToDo
    }

    override fun destroy() {
        stopSynthesizing()
        // Release speech synthesizer and its dependencies
        if (synthesizer != null) {
            synthesizer!!.close()
            connection!!.close()
        }
        if (speechConfig != null) {
            speechConfig!!.close()
        }

        if (audioTrack != null) {
            singleThreadExecutor!!.shutdownNow()
            audioTrack!!.flush()
            audioTrack!!.stop()
            audioTrack!!.release()
        }
    }

    fun preConnect() {
        // This method could pre-establish the connection to service to lower the latency
        // This method is useful when you want to synthesize audio in a short time, but the text is
        // not available. E.g. for speech bot, you can warm up the TTS connection when the user is speaking;
        // then call speak() when dialogue utterance is ready.
        if (connection == null) {
            Timber.v("Please initialize the speech synthesizer first\n")
            return
        }
        connection!!.openConnection(true)
        Timber.v("Opening connection.\n")
    }

    override fun playVoiceInstructions(
        voiceStringPlain: String,
        voiceStringSSML: String?,
        distanceAlongGeometry: Double
    ) {
        if (synthesizer == null) {
            Timber.v("Please initialize the speech synthesizer first\n")
            return
        }

        speakingRunnable!!.setContent(
            voiceStringPlain,
            false
        ) //ToDo - Find a way to pass SSML string here
        //speakingRunnable!!.synthesizer.SpeakSsml(voiceString) ??!!
        singleThreadExecutor!!.execute(speakingRunnable)
    }

    override fun stopSpeech() {
        if (synthesizer == null) {
            Timber.v("Please initialize the speech synthesizer first\n")
            return
        }
        stopSynthesizing()
    }

    private fun createSynthesizer(context: Context) {
        if (synthesizer != null) {
            speechConfig!!.close()
            synthesizer!!.close()
            connection!!.close()
        }

        // Reuse the synthesizer to lower the latency.
        // I.e. create one synthesizer and speak many times using it.
        speechConfig = SpeechConfig.fromSubscription(
            speechSubscriptionKey,
            serviceRegion
        )
        // Use 24k Hz format for higher quality.
        speechConfig!!.setSpeechSynthesisOutputFormat(SpeechSynthesisOutputFormat.Raw24Khz16BitMonoPcm)
        // Set voice name.
        speechConfig!!.setSpeechSynthesisVoiceName("en-US-ChristopherNeural")
        synthesizer = SpeechSynthesizer(speechConfig!!, null)
        connection = Connection.fromSpeechSynthesizer(synthesizer)
        val current: Locale = context.resources.getConfiguration().locale
        connection!!.connected.addEventListener(EventHandler { o: Any?, e: ConnectionEventArgs? ->
            Timber.v(
                "Connection established.\n"
            )
        })
        connection!!.disconnected.addEventListener(EventHandler { o: Any?, e: ConnectionEventArgs? ->
            Timber.v(
                "Disconnected.\n"
            )
        })
        synthesizer!!.SynthesisStarted.addEventListener { o: Any?, e: SpeechSynthesisEventArgs ->
            Timber.v(
                String.format(
                    current,
                    "Synthesis started. Result Id: ${e.result.resultId}.\n"
                )
            )
            e.close()
        }
        synthesizer!!.Synthesizing.addEventListener { o: Any?, e: SpeechSynthesisEventArgs ->
            Timber.v(
                String.format(
                    current,
                    "Synthesizing. received ${e.result.audioLength} bytes.\n",
                )
            )
            e.close()
        }
        synthesizer!!.SynthesisCompleted.addEventListener { o: Any?, e: SpeechSynthesisEventArgs ->
            Timber.v("Synthesis finished.\n")
            Timber.v(
                "\tFirst byte latency: " + e.result.properties
                    .getProperty(PropertyId.SpeechServiceResponse_SynthesisFirstByteLatencyMs) + " ms.\n"
            )
            Timber.v(
                "\tFinish latency: " + e.result.properties
                    .getProperty(PropertyId.SpeechServiceResponse_SynthesisFinishLatencyMs) + " ms.\n"
            )
            e.close()
        }
        synthesizer!!.SynthesisCanceled.addEventListener { o: Any?, e: SpeechSynthesisEventArgs ->
            val cancellationDetails =
                SpeechSynthesisCancellationDetails.fromResult(e.result).toString()
            Timber.v(
                "Error synthesizing. Result ID: " + e.result.resultId +
                        ". Error detail: " + System.lineSeparator() + cancellationDetails +
                        System.lineSeparator() + "Did you update the subscription info?\n"
            )
            e.close()
        }
        synthesizer!!.WordBoundary.addEventListener { o: Any?, e: SpeechSynthesisWordBoundaryEventArgs ->
            Timber.v(
                String.format(
                    current,
                    "Word boundary. Text offset ${e.textOffset}, length ${e.wordLength}; audio offset ${e.audioOffset / 10000} ms.\n"
                )
            )
        }
    }

    internal class SpeakingRunnable(
        val audioTrack: AudioTrack,
        val synthesizer: SpeechSynthesizer
    ) : Runnable {
        private var content: String? = null
        private var isSSML: Boolean = false

        fun setContent(content: String?, isSSML: Boolean) {
            this.content = content
            this.isSSML = isSSML
        }

        override fun run() {
            try {
                audioTrack.play()
                val result: SpeechSynthesisResult
                if (isSSML) {
                    result = synthesizer.StartSpeakingSsmlAsync(content).get()
                } else {
                    result = synthesizer.StartSpeakingTextAsync(content).get()
                }
                val audioDataStream = AudioDataStream.fromResult(result)

                // Set the chunk size to 50 ms. 24000 * 16 * 0.05 / 8 = 2400
                val buffer = ByteArray(2400)
                while (true) {
                    val len = audioDataStream.readData(buffer)
                    if (len == 0L) {
                        break
                    }
                    audioTrack.write(buffer, 0, len.toInt())
                }
                audioDataStream.close()
            } catch (ex: Exception) {
                Log.e("Speech Synthesis Demo", "unexpected " + ex.message)
                ex.printStackTrace()
                assert(false)
            }
        }
    }

    private fun stopSynthesizing() {
        if (synthesizer != null) {
            synthesizer!!.StopSpeakingAsync()
        }
        if (audioTrack != null) {
            audioTrack!!.pause()
            audioTrack!!.flush()
        }
    }
}
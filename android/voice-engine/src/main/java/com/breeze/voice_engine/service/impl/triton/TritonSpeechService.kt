package com.breeze.voice_engine.service.impl.triton

import android.content.Context
import android.media.AudioFormat
import android.util.Log
import com.breeze.voice_engine.BreezeAudioTrackPlayer
import com.breeze.voice_engine.service.BreezeSpeechService
import com.breeze.voice_engine.triton_client.InferInput
import com.breeze.voice_engine.triton_client.InferRequestedOutput
import com.breeze.voice_engine.triton_client.InferResult
import com.breeze.voice_engine.triton_client.InferenceServerClient
import com.breeze.voice_engine.triton_client.pojo.DataType
import com.breeze.voice_engine.utils.VoiceInstructionsHelper
import com.google.common.collect.Lists
import okio.ByteString.Companion.encode
import timber.log.Timber
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import kotlin.system.measureTimeMillis

class TritonSpeechService : BreezeSpeechService {

    private var audioTrackPlayer: BreezeAudioTrackPlayer<ShortArray>? = null
    private val inferenceClient = InferenceServerClient(
        TARGET,
        TIME_OUT,
        TIME_OUT
    )

    private var singleThreadExecutor: ExecutorService? = null

    override fun setup(context: Context) {
        //TODO: enable threadpool -> singleThreadExecutor = Executors.newFixedThreadPool(5)
        singleThreadExecutor = Executors.newSingleThreadExecutor()
        audioTrackPlayer = BreezeAudioTrackPlayer(
            context,
            AudioFormat.CHANNEL_OUT_MONO,
            AudioFormat.ENCODING_PCM_16BIT,
            22050
        )
    }

    override fun toggleMute(isVoiceMuted: Boolean) {
        if (isVoiceMuted) {
            audioTrackPlayer?.setVolume(0.0F)
        } else {
            audioTrackPlayer?.setVolume(1.0F)
        }
    }

    override fun destroy() {
        stopSynthesizing()
        audioTrackPlayer?.let { audioTrackPlayer ->
            singleThreadExecutor?.shutdownNow()
            audioTrackPlayer.destroy()
        }
    }


    override fun playVoiceInstructions(
        voiceStringPlain: String,
        voiceStringSSML: String?,
        distanceAlongGeometry: Double
    ) {
        val speakingRunnable = SpeakingRunnable(audioTrackPlayer!!, inferenceClient)
        speakingRunnable.setContent(
            voiceStringPlain,
            false
        ) //ToDo - Find a way to pass SSML string here
        //speakingRunnable!!.synthesizer.SpeakSsml(voiceString) ??!!
        singleThreadExecutor!!.execute(speakingRunnable)
    }

    override fun stopSpeech() {
        stopSynthesizing()
    }

    internal class SpeakingRunnable(
        private val audioTrackPlayer: BreezeAudioTrackPlayer<ShortArray>,
        val inferenceClient: InferenceServerClient
    ) : Runnable {
        private var content: String? = null
        private var isSSML: Boolean = false

        fun setContent(content: String?, isSSML: Boolean) {
            this.content = content
            this.isSSML = isSSML
        }

        override fun run() {
            content?.let { originalContent ->
                try {
                    audioTrackPlayer.startAudioStream()
                    val byteContent =
                        VoiceInstructionsHelper.addFullStop(originalContent).toCharArray()
                            .map { String(it.toString().encode(Charsets.UTF_8).toByteArray()) }
                    val inputs: List<InferInput> = listOf(
                        InferInput(
                            INPUT_NAME,
                            longArrayOf(1L, byteContent.size.toLong()),
                            DataType.BYTES
                        ).apply {
                            setData(byteContent.toTypedArray(), true)
                        })
                    val outputs: List<InferRequestedOutput> =
                        Lists.newArrayList(InferRequestedOutput(OUTPUT_NAME, true))
                    var result: InferResult
                    var shortArray: ShortArray
                    val timeElapsed = measureTimeMillis {
                        result = inferenceClient.infer(MODEL_NAME, inputs, outputs)
                        shortArray = result.getOutputAsShort(OUTPUT_NAME)
                    }
                    Log.d("elapsed-time", timeElapsed.toString())
                    Timber.d(
                        "---type=triton--" + content + "--time-elapsed=%s",
                        timeElapsed.toString()
                    )
                    audioTrackPlayer.playAudio(shortArray)
                } catch (ex: Exception) {
                    Log.e("triton-error", ex.stackTraceToString())
                }
            }
        }
    }

    private fun stopSynthesizing() {
        audioTrackPlayer?.let { audioTrackPlayer ->
            audioTrackPlayer.stop()
        }
    }

    companion object {
        private const val TARGET = "breeze-voice-dev.coderuse.net:8000"
        private const val TIME_OUT = 150000
        private const val MODEL_NAME = "nemo_model1"
        private const val INPUT_NAME = "input__0"
        private const val OUTPUT_NAME = "output__0"
    }
}
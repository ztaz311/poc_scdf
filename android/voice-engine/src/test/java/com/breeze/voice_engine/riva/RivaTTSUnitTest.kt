package com.breeze.voice_engine.riva

import com.google.protobuf.ByteString
import io.grpc.ManagedChannel
import io.grpc.ManagedChannelBuilder
import io.grpc.stub.StreamObserver
import nvidia.riva.RivaAudio
import nvidia.riva.tts.RivaSpeechSynthesisGrpc
import nvidia.riva.tts.RivaTts
import org.junit.Test
import java.io.FileOutputStream
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit

class RivaTTSUnitTest {
    @Test
    fun tts_test() {
        val latch = CountDownLatch(1)
        runtts()
        latch.await(5000, TimeUnit.MILLISECONDS)
    }

    fun runtts() {
        try {
            val target = "18.141.185.129:50051"
            val channel: ManagedChannel = ManagedChannelBuilder.forTarget(target).usePlaintext().build()
            val blockingStub = RivaSpeechSynthesisGrpc.newStub(channel);
            val req= RivaTts.SynthesizeSpeechRequest.newBuilder().
            setText("this is a test").setLanguageCode("en-US"). setEncoding(RivaAudio.AudioEncoding.LINEAR_PCM).
            setSampleRateHz(22050).setVoiceName("breeze").build()
            blockingStub.synthesize(req,object: StreamObserver<RivaTts.SynthesizeSpeechResponse> {
                var completeBytes : ByteString = ByteString.EMPTY
                override fun onNext(value: RivaTts.SynthesizeSpeechResponse?) {
                    value?.audio?.let {
                        completeBytes=completeBytes.concat(it)


                    }

                }

                override fun onError(t: Throwable?) {
                }

                override fun onCompleted() {
                    val byteArray=completeBytes.toByteArray()
//                    val byteArray=completeBytes.toByteArray()
//                    val byteArrayBuf= ByteBuffer.wrap(byteArray)
//                    val floatArrL: ArrayList<Short>  = arrayListOf()
//                    try{
//                        while (true) {
//                            floatArrL.add(byteArrayBuf.short)
//                        }
//                    }catch(e: Exception){
//
//                    }
//                    val floatArr=floatArrL.toShortArray()
//                    println(floatArr.size.toString())
                    val out: FileOutputStream = FileOutputStream("E:\\java_grpc.wav")
                    out.write(byteArray)
                    out.close()
                }

            })
        } catch (ex: Exception) {
            println("unexpected " + ex.message)
            ex.printStackTrace()
        }
    }

}
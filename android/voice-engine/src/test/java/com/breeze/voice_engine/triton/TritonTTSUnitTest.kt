package com.breeze.voice_engine.triton

import com.breeze.voice_engine.triton_client.InferInput
import com.breeze.voice_engine.triton_client.InferRequestedOutput
import com.breeze.voice_engine.triton_client.InferenceServerClient
import com.breeze.voice_engine.triton_client.pojo.DataType
import com.google.common.collect.Lists
import com.google.protobuf.ByteString
import io.grpc.ManagedChannel
import io.grpc.ManagedChannelBuilder
import io.grpc.stub.StreamObserver
import nvidia.riva.tts.RivaTts
import nvidia.triton.GRPCInferenceServiceGrpc
import nvidia.triton.GrpcService
import nvidia.triton.GrpcService.*
import nvidia.triton.GrpcService.ModelInferRequest.InferInputTensor
import nvidia.triton.GrpcService.ModelInferRequest.InferRequestedOutputTensor
import okio.ByteString.Companion.encode
import org.junit.Test
import java.io.FileOutputStream
import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.nio.FloatBuffer
import java.nio.ShortBuffer
import java.nio.channels.FileChannel
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit
import kotlin.system.measureTimeMillis


class TritonTTSUnitTest {
    @Test
    fun tts_test() {
        val latch = CountDownLatch(1)
        runTTSGrpc()
        latch.await(4000, TimeUnit.MILLISECONDS)
    }

    @Test
    fun tts_test2() {
        val latch = CountDownLatch(1)
        runTTS()
        latch.await(15000, TimeUnit.MILLISECONDS)
    }

    @Test
    fun tts_test3() {
//        val latch = CountDownLatch(1)
//        runHttpClientTTS()
//        latch.await(15000, TimeUnit.MILLISECONDS)
    }


    private fun runTTS() {
        try {
            val content: String="In 200 meters turn left to Central Express Way and then In 200 meters turn right  to Little India"
            val byteContent=content.toCharArray().map { String(it.toString().encode(Charsets.UTF_8).toByteArray()) }
            val inputs : List<InferInput> = listOf(
                InferInput("input__0", longArrayOf(1L, byteContent.size.toLong()),
                    DataType.BYTES).apply {
                    byteContent.toTypedArray().forEach {
                        print(it.toString())
                    }

                setData(byteContent.toTypedArray(),true)
            })
            val outputs: List<InferRequestedOutput> =
                Lists.newArrayList(InferRequestedOutput("output__0", true))

            val target = "aa1a71df3bff04397b3081465bdbcbd7-1987021710.ap-southeast-1.elb.amazonaws.com:8000"
            val timeElasped=measureTimeMillis {
                val inferenceClient = InferenceServerClient(
                    target,
                    150000,
                    150000
                )
                val result = inferenceClient.infer("nemo_model1", inputs, outputs)
            }
            println("timeElasped$timeElasped")
//            val byteArray=result.getOutputAsShort("output__0")
//           print("byteArray" + byteArray.size)
//            print("byteArray" + byteArray.size)
//            val myByteBuffer: ByteBuffer = ByteBuffer.allocate(byteArray.size*2)
//            myByteBuffer.order(ByteOrder.LITTLE_ENDIAN)
//            val myShortBuffer: ShortBuffer = myByteBuffer.asShortBuffer()
//            myShortBuffer.put(byteArray)
//            val out: FileChannel = FileOutputStream("E:\\out.wav").channel
//            out.write(myByteBuffer)
//            out.close()

        } catch (ex: Exception) {
            print( "unexpected " + ex.message)
            print( "unexpected " + ex.stackTraceToString())
            ex.printStackTrace()
        }
    }

    private fun runTTSGrpc() {
        try {
            val TARGET = "aa1a71df3bff04397b3081465bdbcbd7-1987021710.ap-southeast-1.elb.amazonaws.com:8000:8001"
            val TIME_OUT = 150000
            val MODEL_NAME = "nemo_model1"
            val INPUT_NAME = "input__0"
            val OUTPUT_NAME = "output__0"
            val content: String="In 200 meters turn left to Central Express Way and then In 200 meters turn right  to Little India"
            val channel: ManagedChannel = ManagedChannelBuilder.forAddress(
                "52.76.155.81",
                8001
            ).usePlaintext().build()
            val grpc_stub = GRPCInferenceServiceGrpc.newStub(channel)

            //val serverLiveRequest = ServerLiveRequest.getDefaultInstance()
            //val r = grpc_stub.serverLive(serverLiveRequest)
            //println("server" +r)


            val byteContent=content.toCharArray().map { ByteString.copyFrom(it.toString().encode(Charsets.UTF_8).toByteArray()) }
            //val byteContent=ByteString.copyFrom(content.toByteArray(Charsets.UTF_8))
            val modelReq = ModelInferRequest.newBuilder()
            modelReq.modelName = MODEL_NAME


            val input0_data = InferTensorContents.newBuilder()
            input0_data.addAllBytesContents(byteContent)

            val input0 = InferInputTensor
                .newBuilder()
            input0.name = INPUT_NAME
            input0.datatype = "BYTES"
            input0.addShape(1)
            input0.addShape(byteContent.size.toLong())
            input0.setContents(input0_data)


            modelReq.addAllInputs(listOf(input0.build()));

            val output0 = InferRequestedOutputTensor
                .newBuilder()
            output0.name = OUTPUT_NAME
            modelReq.addAllOutputs( listOf(output0.build()));
            println("here:1")
            val time=System.currentTimeMillis()
            grpc_stub.modelInfer(modelReq.build(),object:
                StreamObserver<nvidia.triton.GrpcService.ModelInferResponse> {
                var completeBytes : ByteString = ByteString.EMPTY
                override fun onNext(value: ModelInferResponse?) {
                    println("here:2")
                    value?.rawOutputContentsList?.get(0)?.let{
                        completeBytes=completeBytes.concat(it)
                    }
                }
                override fun onError(t: Throwable?) {
                    println("here:3")
                    println(t?.stackTraceToString())
                }

                override fun onCompleted() {

                    println("Time: "+(System.currentTimeMillis()-time).toString())

                    val byteArray=completeBytes.toByteArray()
//                    val byteArrayBuf= ByteBuffer.wrap(byteArray)
//                    val floatArrL: ArrayList<Float>  = arrayListOf()
//                    try{
//                        while (true) {
//                            floatArrL.add(byteArrayBuf.float)
//                        }
//                    }catch(e: Exception){
//
//                    }
////                    val floatArr=floatArrL.toShortArray()
////                    println("completed:" +floatArr.size.toString())
//
//                    //            val byteArray=result.getOutputAsShort("output__0")
//
//                    val myByteBuffer: ByteBuffer = ByteBuffer.allocate(byteArray.size*4)
//                    myByteBuffer.order(ByteOrder.BIG_ENDIAN)
//                    val myShortBuffer: FloatBuffer = myByteBuffer.asFloatBuffer()
//                    myShortBuffer.put(floatArrL.toFloatArray())
                    val out: FileOutputStream = FileOutputStream("E:\\java_grpc.wav")
                    out.write(byteArray)
                    out.close()
                }
            })

        } catch (ex: Exception) {
            print( "unexpected " + ex.message)
            print( "unexpected " + ex.stackTraceToString())
            ex.printStackTrace()
        }
    }
}
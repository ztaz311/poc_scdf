package com.breeze.customization.view

import android.content.Context
import android.util.AttributeSet
import com.breeze.customization.R
import java.util.Calendar

class DropPinToggleView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : androidx.appcompat.widget.AppCompatImageView(context, attrs, defStyle) {
    /**
     * save last user click timestamp in milliseconds to prevent user click too fast
     * */
    private var lastClickTimestamp = -1L

    var isOn = false
        private set(value) {
            field = value
            isSelected = value
        }

    var onToggleDropPin: (isOn: Boolean, isFromUser: Boolean) -> Unit = { _, _ -> }

    init {
        setImageResource(R.drawable.btn_pin_toggle)
        setOnClickListener {
            if (Calendar.getInstance().timeInMillis - lastClickTimestamp < 1000L)
                return@setOnClickListener
            lastClickTimestamp = Calendar.getInstance().timeInMillis
            toggleDropPinModeInternal(!isOn, true)
        }
    }

    fun setDropPinMode(isOn: Boolean) {
        toggleDropPinModeInternal(isOn, false)
    }

    private fun toggleDropPinModeInternal(isOn: Boolean, isFromUser: Boolean) {
        this.isOn = isOn
        onToggleDropPin.invoke(this.isOn, isFromUser)
    }
}
package com.breeze.customization.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.breeze.customization.R


class CustomCarIDView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : ConstraintLayout(context, attrs, defStyle) {


    var txtCarDetails: TextView? = null
    var ivCarDetailsWarning: ImageView? = null

    init {
        LayoutInflater.from(context).inflate(R.layout.layout_car_details, this, true)
        txtCarDetails = findViewById(R.id.txt_car_details)
        ivCarDetailsWarning = findViewById(R.id.iv_car_details_warning)
    }

    fun fillData(iuNumber: String, carplateNumber: String) {
        txtCarDetails?.text =
            if (iuNumber.isEmpty() || carplateNumber.isEmpty()) context.getString(R.string.car_details_tap) else
                "$carplateNumber | $iuNumber"
        ivCarDetailsWarning?.visibility = View.GONE
    }
}
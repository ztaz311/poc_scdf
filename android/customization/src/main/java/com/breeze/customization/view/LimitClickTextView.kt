package com.breeze.customization.view

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatTextView

class LimitClickTextView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null
) : AppCompatTextView(context, attrs) {
    private var lastClickTimeMillis = System.currentTimeMillis()

    override fun performClick(): Boolean {
        if (System.currentTimeMillis() - lastClickTimeMillis < 1000) {
            return true
        }
        lastClickTimeMillis = System.currentTimeMillis()
        return super.performClick()
    }

    override fun callOnClick(): Boolean {
        if (System.currentTimeMillis() - lastClickTimeMillis < 1000) {
            return true
        }
        lastClickTimeMillis = System.currentTimeMillis()
        return super.callOnClick()
    }
}
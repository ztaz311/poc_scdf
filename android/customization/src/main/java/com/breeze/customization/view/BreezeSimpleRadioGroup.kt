package com.breeze.customization.view

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout

class BreezeSimpleRadioGroup @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : LinearLayout(context, attrs, defStyle) {


    override fun addView(child: View?, index: Int, params: ViewGroup.LayoutParams?) {
        if (child is BreezeSimpleRadioButton) {
            child.setOnClickListener {
                var radioButton = child
                setAllButtonsToUnselectedState()
                radioButton.setSelectedState()
            }
        }
        super.addView(child, index, params)
    }

    fun setAllButtonsToUnselectedState() {
        var container = this
        for (i in 0 until container.childCount) {
            val child = container.getChildAt(i)
            if (child is BreezeSimpleRadioButton) {
                val containerView: BreezeSimpleRadioButton = child
                containerView.setUnselectedState()
            }
        }
    }
}
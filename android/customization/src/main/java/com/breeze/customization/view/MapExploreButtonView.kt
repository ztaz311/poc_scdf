package com.breeze.customization.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import com.breeze.customization.R
import com.breeze.customization.databinding.ExploreMapCustomViewBinding
import com.breeze.model.extensions.dpToPx


class MapExploreButtonView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : ConstraintLayout(context, attrs, defStyle) {
    private var binding: ExploreMapCustomViewBinding

    var isEnable = true
        set(value) {
            field = value
            binding.imgExploreIcon.setImageResource(if (value) R.drawable.ic_selected_explore_map else R.drawable.ic_unselected_exlore_map)
        }


    init {
        binding = ExploreMapCustomViewBinding.inflate(LayoutInflater.from(context), this)
        layoutParams = LayoutParams(
            LayoutParams.WRAP_CONTENT,
            LayoutParams.WRAP_CONTENT
        )
        clipToPadding = false
        elevation = 2.0f.dpToPx()
    }

    fun updateNotificationDot(hasNewContent: Boolean) {
        binding.imgDotNotification.visibility = if (hasNewContent) VISIBLE else GONE
    }
}
package com.breeze.customization.view.navigationalert

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import com.breeze.customization.R
import com.breeze.customization.databinding.LayoutObuAlertMessageGeneralBinding
import com.breeze.model.extensions.dpToPx

class OBUAlertMessageGeneralView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null
) : CardView(context, attrs) {
    private val binding: LayoutObuAlertMessageGeneralBinding

    init {
        binding = LayoutObuAlertMessageGeneralBinding.inflate(LayoutInflater.from(context))
        addView(binding.root)
        radius = 20.dpToPx().toFloat()
        setCardBackgroundColor(ContextCompat.getColor(context, R.color.text_black))
    }

    fun setData(message: String, detailMessage: String) {
        binding.tvOBUGeneralMessage.text = message
        binding.tvOBUGeneralDetailMessage.text = detailMessage
    }
}
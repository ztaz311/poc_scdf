package com.breeze.customization.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.breeze.customization.R
import kotlin.properties.Delegates

class BreezeVehicleSelectionRadioButton @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : RelativeLayout(context, attrs, defStyle) {


    var mainText: String = ""
    var mSelectedLeftIcon by Delegates.notNull<Int>()
    var mSelectedRightIcon by Delegates.notNull<Int>()
    var mUnselectedRightIcon by Delegates.notNull<Int>()

    lateinit var mTextView: TextView
    lateinit var layout: RelativeLayout
    lateinit var mImageView: ImageView

    init {
        LayoutInflater.from(context)
            .inflate(R.layout.breeze_vehicle_selection_radio_button, this, true)

        initDefaults()
        initializeAttributes(attrs)
        initializeView()
    }


    private fun initDefaults() {
        mSelectedLeftIcon = R.drawable.obu_light_vehicle
        mSelectedRightIcon = R.drawable.radio_selected
        mUnselectedRightIcon = R.drawable.radio_unselected
    }

    private fun initializeAttributes(attrs: AttributeSet?) {

        attrs?.let {
            val typedArray = context.obtainStyledAttributes(
                it,
                R.styleable.BreezeVehicleSelectionRadioButton, 0, 0
            )

            mainText = resources.getString(
                typedArray.getResourceId(
                    R.styleable.BreezeVehicleSelectionRadioButton_vehicle_name_text,
                    R.string.empty_text
                )
            )

            mSelectedLeftIcon = typedArray.getResourceId(
                R.styleable.BreezeVehicleSelectionRadioButton_selected_left_icon,
                -1
            )

            mSelectedRightIcon = typedArray.getResourceId(
                R.styleable.BreezeVehicleSelectionRadioButton_selected_right_icon,
                -1
            )

            mUnselectedRightIcon = typedArray.getResourceId(
                R.styleable.BreezeVehicleSelectionRadioButton_unselected_right_icon,
                -1
            )
            typedArray.recycle()
        }
    }

    private fun initializeView() {
        mImageView = this.findViewById(R.id.radio_btn_img)
        mTextView = this.findViewById(R.id.vehicle_name)

        mTextView.text = mainText
        setUnselectedState()
    }

    fun setUnselectedState() {
        mImageView.setImageResource(mUnselectedRightIcon)

    }

    fun setSelectedState() {
        mImageView.setImageResource(mSelectedRightIcon)
    }

    fun updateImageDrawable() {

        mTextView.setCompoundDrawablesWithIntrinsicBounds(
            ContextCompat.getDrawable(
                context,
                mSelectedLeftIcon
            ), null, null, null
        )
    }

}
package com.breeze.customization.view.nudge

import android.content.Context
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import android.widget.LinearLayout
import android.widget.PopupWindow
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintLayout.LayoutParams.PARENT_ID
import androidx.constraintlayout.widget.ConstraintLayout.LayoutParams.UNSET
import androidx.core.content.ContextCompat
import com.breeze.customization.R
import com.breeze.customization.databinding.BreezeNudgeViewBinding
import com.breeze.model.extensions.dpToPx
import timber.log.Timber


class DashboardNudge(context: Context) {

    private var actionTaken: Boolean = false
    private var tipWindow: PopupWindow? = null
    private var viewBinding: BreezeNudgeViewBinding = BreezeNudgeViewBinding.inflate(
        context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater,
        null, false
    )
    private var tipMarginOutside: Int = 20.dpToPx()
    private var tipMarginInside: Int = (-8).dpToPx()
    private var mOnDismissListener: ((actionTaken: Boolean) -> Unit)? = null
    private var mOnActionListener: (() -> Unit)? = null

    fun showToolTip(
        anchor: View,
        alignment: Int,
        strTitle: String,
        strDesc: String,
        screenName: String,
        strIndex: String? = null,
        strAction: String? = null,
        onDismissListener: ((actionTaken: Boolean) -> Unit)? = null,
        onActionListener: (() -> Unit)? = null
    ) {
        viewBinding.nudgeContentView.renderContent(
            strTitle = strTitle,
            strDesc = strDesc,
            strIndex = strIndex,
            strAction = strAction,
            screenName = screenName,
            onClose = {
                dismissTooltip(isAction = false)
            },
            onAction = {
                dismissTooltip(isAction = true)
            }
        )

        mOnDismissListener = onDismissListener
        mOnActionListener = onActionListener

        setTipPosition(alignment)

        putTipNearAnchor(
            anchorView = anchor,
            alignment = alignment,
        )
    }

    private fun setTipPosition(
        alignment: Int
    ) {
        val contentLayoutParam =
            viewBinding.nudgeContentView.layoutParams as ConstraintLayout.LayoutParams
        val pointLayoutParam =
            viewBinding.imgTipBottom.layoutParams as ConstraintLayout.LayoutParams

        //Set Vertical Alignment
        pointLayoutParam.resetVerticalAlignments()
        contentLayoutParam.resetVerticalAlignments()
        when (alignment) {
            ANCHOR_TOP_CENTER,
            ANCHOR_TOP_RIGHT,
            ANCHOR_TOP_LEFT -> {
                pointLayoutParam.topToTop = PARENT_ID
                pointLayoutParam.bottomMargin = tipMarginInside
                contentLayoutParam.topToBottom = R.id.img_tip_bottom
            }

            ANCHOR_BOTTOM_LEFT,
            ANCHOR_BOTTOM_CENTER,
            ANCHOR_BOTTOM_RIGHT -> {
                pointLayoutParam.topMargin = tipMarginInside
                pointLayoutParam.topToBottom = viewBinding.nudgeContentView.id
                contentLayoutParam.topToTop = PARENT_ID
            }

            ANCHOR_LEFT_TOP,
            ANCHOR_RIGHT_TOP -> {
                pointLayoutParam.topMargin = tipMarginOutside
                pointLayoutParam.topToTop = viewBinding.nudgeContentView.id
                contentLayoutParam.topToTop = PARENT_ID
            }

            ANCHOR_LEFT_CENTER,
            ANCHOR_RIGHT_CENTER -> {
                pointLayoutParam.topToTop = viewBinding.nudgeContentView.id
                pointLayoutParam.bottomToBottom = viewBinding.nudgeContentView.id
                contentLayoutParam.topToTop = PARENT_ID
            }

            ANCHOR_LEFT_BOTTOM,
            ANCHOR_RIGHT_BOTTOM -> {
                pointLayoutParam.bottomMargin = tipMarginOutside
                pointLayoutParam.bottomToBottom = viewBinding.nudgeContentView.id
                contentLayoutParam.topToTop = PARENT_ID
            }
        }

        //Set Horizontal Alignment
        pointLayoutParam.resetHorizontalAlignments()
        contentLayoutParam.resetHorizontalAlignments()
        when (alignment) {
            ANCHOR_LEFT_TOP,
            ANCHOR_LEFT_CENTER,
            ANCHOR_LEFT_BOTTOM -> {
                pointLayoutParam.marginEnd = tipMarginInside
                pointLayoutParam.startToStart = PARENT_ID
                contentLayoutParam.startToEnd = R.id.img_tip_bottom
                contentLayoutParam.endToEnd = PARENT_ID
            }

            ANCHOR_RIGHT_TOP,
            ANCHOR_RIGHT_CENTER,
            ANCHOR_RIGHT_BOTTOM -> {
                pointLayoutParam.marginStart = tipMarginInside
                contentLayoutParam.startToStart = PARENT_ID
                contentLayoutParam.endToStart = R.id.img_tip_bottom
                pointLayoutParam.startToEnd = viewBinding.nudgeContentView.id
                pointLayoutParam.endToEnd = PARENT_ID
            }

            ANCHOR_TOP_LEFT,
            ANCHOR_BOTTOM_LEFT -> {
                pointLayoutParam.marginStart = tipMarginOutside
                contentLayoutParam.startToStart = PARENT_ID
                contentLayoutParam.endToEnd = PARENT_ID
                pointLayoutParam.startToStart = viewBinding.nudgeContentView.id
            }

            ANCHOR_TOP_CENTER,
            ANCHOR_BOTTOM_CENTER -> {
                contentLayoutParam.startToStart = PARENT_ID
                contentLayoutParam.endToEnd = PARENT_ID
                pointLayoutParam.startToStart = viewBinding.nudgeContentView.id
                pointLayoutParam.endToEnd = viewBinding.nudgeContentView.id
            }

            ANCHOR_TOP_RIGHT,
            ANCHOR_BOTTOM_RIGHT -> {
                pointLayoutParam.marginEnd = tipMarginOutside
                contentLayoutParam.startToStart = PARENT_ID
                contentLayoutParam.endToEnd = PARENT_ID
                pointLayoutParam.endToEnd = viewBinding.nudgeContentView.id
            }
        }

        //Set Rotation
        when (alignment) {
            ANCHOR_TOP_CENTER,
            ANCHOR_TOP_RIGHT,
            ANCHOR_TOP_LEFT -> {
                viewBinding.imgTipBottom.rotation = 270f
            }

            ANCHOR_BOTTOM_LEFT,
            ANCHOR_BOTTOM_CENTER,
            ANCHOR_BOTTOM_RIGHT -> {
                viewBinding.imgTipBottom.rotation = 90f
            }

            ANCHOR_LEFT_TOP,
            ANCHOR_LEFT_CENTER,
            ANCHOR_LEFT_BOTTOM -> {
                viewBinding.imgTipBottom.rotation = 180f
            }

            ANCHOR_RIGHT_TOP,
            ANCHOR_RIGHT_CENTER,
            ANCHOR_RIGHT_BOTTOM -> {
                viewBinding.imgTipBottom.rotation = 0f
            }
        }
    }

    private fun putTipNearAnchor(
        anchorView: View,
        alignment: Int
    ) {
        val screen_pos = IntArray(2)
        anchorView.getLocationOnScreen(screen_pos)
        val anchorPosX = screen_pos[0]
        val anchorPosY = screen_pos[1]

        val anchorWidth = anchorView.measuredWidth
        val anchorHeight = anchorView.measuredHeight

        viewBinding.root.measure(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )

        var contentViewWidth = viewBinding.nudgeContentView.measuredWidth
        var contentViewHeight = viewBinding.nudgeContentView.measuredHeight
        val pointWidth = 16.dpToPx()
        val pointHeight = 16.dpToPx()


        Timber.d("-- anchor position [${anchorPosX}, ${anchorPosY}] --")
        Timber.d("-- anchor size [${anchorWidth} x ${anchorHeight}] --")
        Timber.d("-- content size [${contentViewWidth} x ${contentViewHeight}] --")


        when (alignment) {
            ANCHOR_TOP_CENTER,
            ANCHOR_TOP_RIGHT,
            ANCHOR_TOP_LEFT,
            ANCHOR_BOTTOM_LEFT,
            ANCHOR_BOTTOM_CENTER,
            ANCHOR_BOTTOM_RIGHT -> {
                contentViewHeight =
                    viewBinding.nudgeContentView.measuredHeight + pointHeight + tipMarginInside
            }

            ANCHOR_LEFT_TOP,
            ANCHOR_LEFT_CENTER,
            ANCHOR_LEFT_BOTTOM,
            ANCHOR_RIGHT_TOP,
            ANCHOR_RIGHT_CENTER,
            ANCHOR_RIGHT_BOTTOM -> {
                contentViewWidth =
                    viewBinding.nudgeContentView.measuredWidth + pointWidth + tipMarginInside
            }
        }
        viewBinding.mainView.x = (when (alignment) {
            ANCHOR_LEFT_TOP,
            ANCHOR_LEFT_CENTER,
            ANCHOR_LEFT_BOTTOM -> {
                anchorPosX
            }

            ANCHOR_RIGHT_TOP,
            ANCHOR_RIGHT_CENTER,
            ANCHOR_RIGHT_BOTTOM -> {
                anchorPosX - contentViewWidth + tipMarginInside
            }

            ANCHOR_TOP_LEFT,
            ANCHOR_BOTTOM_LEFT -> {
                anchorPosX - tipMarginOutside - (pointWidth / 2)
            }

            ANCHOR_TOP_CENTER,
            ANCHOR_BOTTOM_CENTER -> {
                anchorPosX + anchorWidth / 2 - (contentViewWidth / 2)
            }

            ANCHOR_TOP_RIGHT,
            ANCHOR_BOTTOM_RIGHT -> {
                anchorPosX - (contentViewWidth) + tipMarginOutside + (pointWidth / 2)
            }

            else -> 0
        }).toFloat()

        viewBinding.mainView.y = (when (alignment) {
            ANCHOR_TOP_CENTER,
            ANCHOR_TOP_RIGHT,
            ANCHOR_TOP_LEFT -> {
                anchorPosY
            }

            ANCHOR_BOTTOM_LEFT,
            ANCHOR_BOTTOM_CENTER,
            ANCHOR_BOTTOM_RIGHT -> {
                anchorPosY - contentViewHeight - tipMarginOutside - pointHeight
            }

            ANCHOR_LEFT_TOP,
            ANCHOR_RIGHT_TOP -> {
                anchorPosY - tipMarginOutside - (pointHeight / 2)
            }

            ANCHOR_LEFT_CENTER,
            ANCHOR_RIGHT_CENTER -> {
                anchorPosY - (contentViewHeight / 2)
            }

            ANCHOR_LEFT_BOTTOM,
            ANCHOR_RIGHT_BOTTOM -> {
                anchorPosY - contentViewHeight + tipMarginOutside + (pointHeight / 2)
            }

            else -> 0
        }).toFloat()

        tipWindow = PopupWindow(
            viewBinding.root,
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.MATCH_PARENT, false
        ).apply {
            setBackgroundDrawable(
                ContextCompat.getDrawable(
                    this.contentView.context,
                    R.drawable.bg_nudge_pupup_window
                )
            )
            isOutsideTouchable = false
            elevation = 20f
            animationStyle = R.style.popup_animation
            setOnDismissListener {
                handleNudgeDismissed()
            }
            if (anchorView.isShown)
                showAtLocation(anchorView, Gravity.NO_GRAVITY, 0, 0)
        }

    }

    private fun handleNudgeDismissed() {
        mOnDismissListener?.invoke(actionTaken)
        if (actionTaken) {
            mOnActionListener?.invoke()
        }
    }

    private fun ConstraintLayout.LayoutParams.resetVerticalAlignments() {
        topToTop = UNSET
        topToBottom = UNSET
        bottomToTop = UNSET
        bottomToBottom = UNSET
        topMargin = 0
        bottomMargin = 0
    }

    private fun ConstraintLayout.LayoutParams.resetHorizontalAlignments() {
        startToStart = UNSET
        startToEnd = UNSET
        endToStart = UNSET
        endToEnd = UNSET
        marginStart = 0
        marginEnd = 0
    }

    fun dismissTooltip(isAction: Boolean = false) {
        actionTaken = isAction
        tipWindow?.let { it ->
            if (it.isShowing) {
                it.dismiss()
            }
        }
        tipWindow = null
        actionTaken = false
    }

    companion object {

        const val ANCHOR_TOP_LEFT = 1
        const val ANCHOR_TOP_CENTER = 2
        const val ANCHOR_TOP_RIGHT = 3
        const val ANCHOR_RIGHT_TOP = 4
        const val ANCHOR_RIGHT_CENTER = 5
        const val ANCHOR_RIGHT_BOTTOM = 6
        const val ANCHOR_BOTTOM_LEFT = 7
        const val ANCHOR_BOTTOM_CENTER = 8
        const val ANCHOR_BOTTOM_RIGHT = 9
        const val ANCHOR_LEFT_TOP = 10
        const val ANCHOR_LEFT_CENTER = 11
        const val ANCHOR_LEFT_BOTTOM = 12
    }
}
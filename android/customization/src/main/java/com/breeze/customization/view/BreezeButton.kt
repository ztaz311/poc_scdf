package com.breeze.customization.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import com.breeze.customization.R


class BreezeButton @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : RelativeLayout(context, attrs, defStyle) {

    lateinit var mTextView: TextView
    lateinit var mDisplayText: String
    lateinit var layout: RelativeLayout
    var mSupportBlackTheme: Boolean = false
    var whiteBGInputType: Boolean = false
    var normalBGLayout: Int = R.drawable.breeze_button_bg

    init {
        LayoutInflater.from(context)
            .inflate(R.layout.breeze_button, this, true)
        initializeAttributes(attrs)
        initializeView()
    }

    fun initializeAttributes(attrs: AttributeSet?) {
        attrs?.let {
            val typedArray = context.obtainStyledAttributes(
                it,
                R.styleable.breeze_button, 0, 0
            )
            mDisplayText = resources.getString(
                typedArray.getResourceId(
                    R.styleable.breeze_button_display_text,
                    R.string.confirm_the_route
                )
            )
            whiteBGInputType = typedArray.getBoolean(R.styleable.breeze_button_white_bg, false)
            mSupportBlackTheme =
                typedArray.getBoolean(R.styleable.breeze_button_support_dark_theme, false)

            typedArray.recycle()
        }
    }

    private fun initializeView() {
        mTextView = this.findViewById(R.id.button)
        mTextView.setText(mDisplayText)
        layout = this.findViewById(R.id.rl_button_bg)

        setBGNormal()
        if (mSupportBlackTheme) {
            setBGThemed()
        }

        if (whiteBGInputType) {
            setBGWhite()
        }

    }

    fun updateText(input: String) {
        mTextView.text = input
    }

    fun updateText(@StringRes strRes: Int) {
        mTextView.setText(strRes)
    }

    fun setBGPink() {
        var drawable =
            ContextCompat.getDrawable(context, R.drawable.breeze_button_pink_bg)
        layout.background = drawable
    }

    fun setBGWhite() {
        var drawable =
            ContextCompat.getDrawable(context, R.drawable.breeze_button_white_bg)
        layout.background = drawable
//        mTextView.background = ContextCompat.getDrawable(context, R.drawable.breeze_button_text_rounded)
        mTextView.setTextColor(ContextCompat.getColor(context, R.color.breeze_primary))

    }

    fun setBGThemed() {
        var drawable =
            ContextCompat.getDrawable(context, R.drawable.breeze_button_themed_bg)
        layout.background = drawable
    }

    fun setBGNormal() {
        var drawable =
            ContextCompat.getDrawable(context, R.drawable.breeze_button_bg)
        layout.background = drawable
    }

    fun setBGGrey() {
        var drawable =
            ContextCompat.getDrawable(context, R.drawable.breeze_button_grey_bg)
        layout.background = drawable
    }
}
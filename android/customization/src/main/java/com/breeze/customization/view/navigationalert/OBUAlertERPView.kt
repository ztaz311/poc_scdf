package com.breeze.customization.view.navigationalert

import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import android.view.View
import androidx.cardview.widget.CardView
import androidx.core.view.isVisible
import com.breeze.customization.R
import com.breeze.customization.databinding.LayoutObuAlertErpBinding
import com.breeze.model.extensions.dpToPx
import com.breeze.model.obu.OBURoadEventData

class OBUAlertERPView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null
) : CardView(context, attrs) {
    private val binding: LayoutObuAlertErpBinding

    init {
        binding = LayoutObuAlertErpBinding.inflate(android.view.LayoutInflater.from(context), this)
        cardElevation = 0f
        radius = 16.dpToPx().toFloat()
    }

    fun setERPContent(eventData: OBURoadEventData){

        var backgroundColor = 0
        var iconResourceId = 0
        var title = ""
        var detail = ""
        var fee = ""
        var statusIndicator = 0

        when (eventData.eventType) {
            OBURoadEventData.EVENT_TYPE_ERP_CHARGING -> {
                backgroundColor = Color.parseColor("#005FD0")
                iconResourceId = R.drawable.erp_icon
                title = eventData.distance ?: ""
                detail = eventData.detailMessage ?: ""
                fee = eventData.chargeAmount ?: ""
            }

            OBURoadEventData.EVENT_TYPE_ERP_SUCCESS -> {
                backgroundColor = Color.parseColor("#005FD0")
                iconResourceId = R.drawable.erp_icon
                title = eventData.message ?: ""
                fee = eventData.chargeAmount ?: ""
                statusIndicator =
                    R.drawable.ic_obu_alert_message_status_indicator_success
            }

            OBURoadEventData.EVENT_TYPE_ERP_FAILURE -> {
                backgroundColor = Color.parseColor("#E82370")
                iconResourceId = R.drawable.ic_obu_alert_message_erp_failure
                title = eventData.message ?: ""
                fee = eventData.chargeAmount ?: ""
                statusIndicator =
                    R.drawable.ic_obu_alert_message_status_indicator_failure
            }
        }

        setContent(backgroundColor, iconResourceId, title, detail, fee, statusIndicator)
    }

    private fun setContent(
        backgroundColor: Int,
        iconResourceId: Int,
        title: String,
        detail: String,
        fee: String,
        statusIndicator: Int
    ) {
        with(binding){

            layoutOBUAlert.setBackgroundColor(backgroundColor)
            imgOBUAlert.setImageResource(iconResourceId)
            tvOBUAlertTitle.text = title
            tvOBUAlertMessage.text = detail
            tvOBUAlertMessage.isVisible = detail.isNotEmpty()

            tvOBUFee.text = fee
            if (statusIndicator > 0) {
                imgOBUIcon.setImageResource(statusIndicator)
                imgOBUIcon.isVisible = true
            } else {
                imgOBUIcon.isVisible = false
            }
        }
    }
}
package com.breeze.customization.utils

import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.method.DigitsKeyListener
import timber.log.Timber
import java.util.*

/**
 * Amount input filter, limit the number of digits after the decimal point
 *
 * Default limit of 2 decimal places
 * The default first digit is converted to 0 when the decimal point is entered.
 * If the starting position is 0 and the second digit is not ".", then you cannot enter it later.
 */
internal class DigitsInputFilter(var digitsBeforeDecimal: Int, var digitsAfterDecimal: Int) :
    DigitsKeyListener(Locale.getDefault(), false, true) {

    override fun filter(
        source: CharSequence, start: Int, end: Int,
        dest: Spanned, dstart: Int, dend: Int
    ): CharSequence {
        var source = source
        var start = start
        var end = end
        val out = super.filter(source, start, end, dest, dstart, dend)
        Timber.d("-------source:$source")
        Timber.d("-------start:$start")
        Timber.d("-------end:$end")
        Timber.d("-------dest:$dest")
        Timber.d("-------dstart:$dstart")
        Timber.d("-------dend:$dend")


        // if changed, replace the source
        if (out != null) {
            source = out
            start = 0
            end = out.length
        }
        val len = end - start

        // if deleting, source is empty
        // and deleting can't break anything
        if (len == 0) {
            return source
        }

        // When starting with a point, automatically add 0 in front
        if (source.toString() == "." && dstart == 0) {
            return "0."
        }
        // If the starting position is 0, and the second bit is not followed by ".", then you can not enter
        if (source.toString() != "." && dest.toString() == "0") {
            return ""
        }
        val dlen = dest.length

        // Find the position of the decimal .
        for (i in 0 until dstart) {
            if (dest[i] == '.') {
                // being here means, that a number has
                // been inserted after the dot
                // check if the amount of digits is right
                return if (dlen - (i + 1) + len > digitsAfterDecimal) "" else SpannableStringBuilder(
                    source,
                    start,
                    end
                )
            }
        }

        // Below logic is to limit the number of digits before .
        var subString = dest.toString().substring(0, dstart)
        if (!subString.contains(".") && source.toString() != ".") {
            // If substring does not contain '.' user is trying to type before dot
            var noOfDigits = 0
            if (!dest.toString().contains(".")) {
                noOfDigits = dest.length
            } else {
                noOfDigits = dest.toString().split(".")[0].length
            }

            if (noOfDigits >= digitsBeforeDecimal) {
                return ""
            }
        }


        Timber.d("Printing dstart: ${dstart} dend: ${dend}")

        for (i in start until end) {
            if (source[i] == '.') {
                // being here means, dot has been inserted
                // check if the amount of digits is right
                return if (dlen - dend + (end - (i + 1)) > digitsAfterDecimal) "" else break // return new SpannableStringBuilder(source, start, end);
            }
        }
        Timber.d("-------return:" + SpannableStringBuilder(source, start, end).toString())


        // if the dot is after the inserted part,
        // nothing can break
        return SpannableStringBuilder(source, start, end)
    }

    companion object {
        private const val TAG = "MoneyValueFilter"
    }
}
package com.breeze.customization.view

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.content.res.AppCompatResources
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.breeze.customization.R
import com.breeze.model.enums.AmenityBand
import com.breeze.model.api.response.amenities.AvailabilityCSData

/**
 * for carpark status validate view
 */
class CarparkStatusValidateView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : ConstraintLayout(context, attrs, defStyle) {

    companion object {
        var isShowing = false
    }

    var mContext: Context? = null
    var mCardView: CardView? = null
    var mContrainTopView: ConstraintLayout? = null
    var mTagBeta: TextView? = null
    var mLlBoxStatusCarpark: LinearLayout? = null
    var mIcCarparkStatus: ImageView? = null
    var mTvStatusCarpark: TextView? = null
    var mImgArrow: ImageView? = null
    var mTvTimeUpdateCarparkStatus: TextView? = null
    var mLlYesThank: LinearLayout? = null
    var mLlNoItNot: LinearLayout? = null

    sealed class Theme(
        var iconCpStt: Int,
        var bgTagBeta: Int,
        var bgBoxSttCP: Int,
    )

    object THEME_CP_FULL : Theme(
        iconCpStt = R.drawable.ic_cp_stt_full,
        bgTagBeta = R.color.bg_tag_beta__carpark_status_full,
        bgBoxSttCP = R.color.bg_carpark_status_full,
    )

    object THEME_CP_AVAILABLE : Theme(
        iconCpStt = R.drawable.ic_cp_stt_available,
        bgTagBeta = R.color.bg_tag_beta_carpark_status_available,
        bgBoxSttCP = R.color.bg_carpark_status_available,
    )

    object THEME_CP_FEW : Theme(
        iconCpStt = R.drawable.ic_cp_stt_few,
        bgTagBeta = R.color.bg_tag_beta__carpark_status_few_lot,
        bgBoxSttCP = R.color.bg_carpark_status_few_lot,
    )

    init {
        LayoutInflater.from(context).inflate(R.layout.layout_status_carpark_validate, this, true)
        mContext = context
        initializeView()
    }

    fun gone() {
        this.visibility = View.GONE
    }

    fun show() {
        this.visibility = View.VISIBLE
    }


    private fun initializeView() {
        mCardView = findViewById(R.id.cardView)
        mContrainTopView = findViewById(R.id.contrainTopView)
        mTagBeta = findViewById(R.id.tagBeta)
        mLlBoxStatusCarpark = findViewById(R.id.llBoxStatusCarpark)
        mIcCarparkStatus = findViewById(R.id.ic_carpark_status)
        mTvStatusCarpark = findViewById(R.id.tvStatusCarpark)
        mImgArrow = findViewById(R.id.imgArrow)
        mTvTimeUpdateCarparkStatus = findViewById(R.id.tvTimeUpdateCarparkStatus)
        mLlYesThank = findViewById(R.id.llYesThank)
        mLlNoItNot = findViewById(R.id.llNoItNot)
    }

    fun setData(pAvailablePercentImageBand: String, availabilityCSData: AvailabilityCSData) {
        val statusCarpark = availabilityCSData.title

        /**
         * calculate time
         */
        val timeUpdate =
            "${availabilityCSData.primaryDesc ?: context.getString(R.string.update_carpark_stt_time)} ${availabilityCSData.generateDisplayedLastUpdate()}"

        var tempTheme: Theme = THEME_CP_FULL
        when (pAvailablePercentImageBand) {
            AmenityBand.TYPE0.type -> {
                tempTheme = THEME_CP_FULL
            }

            AmenityBand.TYPE1.type -> {
                tempTheme = THEME_CP_FEW
            }

            AmenityBand.TYPE2.type -> {
                tempTheme = THEME_CP_AVAILABLE
            }
        }
        mTvStatusCarpark?.text = statusCarpark
        mTvTimeUpdateCarparkStatus?.text = timeUpdate

        val drawableTagBeta: GradientDrawable =
            AppCompatResources.getDrawable(context, R.drawable.bg_beta_tag) as GradientDrawable

        drawableTagBeta.setColor(ContextCompat.getColor(context, tempTheme.bgTagBeta))
        mTagBeta?.background = drawableTagBeta
        mIcCarparkStatus?.setImageResource(tempTheme.iconCpStt)
        mContrainTopView?.setBackgroundColor(Color.parseColor(availabilityCSData.themeColor))
    }

    fun setListeners(onYes: () -> Unit = {}, onNo: () -> Unit = {}) {
        mLlNoItNot?.setOnClickListener {
            this.visibility = View.GONE
            onNo.invoke()
        }
        mLlYesThank?.setOnClickListener {
            this.visibility = View.GONE
            onYes.invoke()
        }
    }
}
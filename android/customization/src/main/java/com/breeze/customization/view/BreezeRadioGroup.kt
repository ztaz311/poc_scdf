package com.breeze.customization.view

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout

class BreezeRadioGroup @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : LinearLayout(context, attrs, defStyle) {

    lateinit var onClickListener: BreezeRadioButtonListener

    fun initClickListener(listener: BreezeRadioButtonListener) {
        onClickListener = listener
    }


    override fun addView(child: View?, index: Int, params: ViewGroup.LayoutParams?) {
        if (child is BreezeRadioButton) {
            child.setOnClickListener {
                var radioButton = child as BreezeRadioButton

                setAllButtonsToUnselectedState()
                onRadioButtonClick(radioButton)
                radioButton.setSelectedState()
            }
        }
        super.addView(child, index, params)
    }

    fun setAllButtonsToUnselectedState() {
        var container = this
        for (i in 0 until container.childCount) {
            val child = container.getChildAt(i)
            if (child is BreezeRadioButton) {
                val containerView: BreezeRadioButton = child as BreezeRadioButton
                containerView.setUnselectedState()
            }
        }
    }

    fun onRadioButtonClick(clickedButton: View) {
        if (::onClickListener.isInitialized) {
            onClickListener.onButtonClick(clickedButton)
        }
    }

    interface BreezeRadioButtonListener {
        fun onButtonClick(selectedButton: View)
    }
}
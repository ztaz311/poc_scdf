package com.breeze.customization.view

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import android.widget.RelativeLayout


/**
 * bangnv handle touch out side
 */
class DispatchTouchLayout(context: Context, attrs: AttributeSet) : RelativeLayout(context, attrs) {

    private var dispatchListener: OnDispatchListener? = null

    override fun dispatchTouchEvent(ev: MotionEvent): Boolean {
        val action = ev.action
        when (action) {
            MotionEvent.ACTION_DOWN -> if (dispatchListener != null) {
                dispatchListener!!.onTouchDown(ev.x, ev.y)
            }
            else -> {
            }
        }
        return super.dispatchTouchEvent(ev)
    }

    fun setDispatchListener(listener: OnDispatchListener) {
        dispatchListener = listener
    }

    fun removeDispatchListener() {
        dispatchListener = null
    }

    interface OnDispatchListener {
        fun onTouchDown(x: Float, y: Float)
    }

}
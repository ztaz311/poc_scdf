package com.breeze.customization.view.navigationalert

import android.content.Context
import android.os.Handler
import android.os.Looper
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import com.breeze.customization.R
import com.breeze.customization.databinding.LayoutObuAlertMessageViewBinding

class OBUAlertMessageView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : ConstraintLayout(context, attrs, defStyle) {

    companion object {
        var isShowing = false
    }

    private var mCardView: CardView? = null
    private var mContainerLinearLayoutCompat: LinearLayoutCompat? = null
    private var mIconImageView: ImageView? = null
    private var mTitleTextView: TextView? = null
    private var mDetailTextView: TextView? = null
    private var mFeeTextView: TextView? = null
    private var mStatusIndicatorImageView: ImageView? = null
    private val binding: LayoutObuAlertMessageViewBinding

    init {
        binding = LayoutObuAlertMessageViewBinding.inflate(LayoutInflater.from(context), this, true)
        initializeView()
    }

    fun setContent(
        backgroundColor: Int,
        iconResourceId: Int,
        title: String,
        detail: String,
        fee: String,
        statusIndicator: Int
    ) {
        mContainerLinearLayoutCompat?.setBackgroundColor(backgroundColor)
        mIconImageView?.setImageResource(iconResourceId)
        mTitleTextView?.text = title
        mDetailTextView?.text = detail
        if (mDetailTextView?.text.isNullOrEmpty()) {
            mDetailTextView?.visibility = View.GONE
        } else {
            mDetailTextView?.visibility = View.VISIBLE
        }
        mFeeTextView?.text = fee
        if (statusIndicator > 0) {
            mStatusIndicatorImageView?.setImageResource(statusIndicator)
            mStatusIndicatorImageView?.visibility = View.VISIBLE
        } else {
            mStatusIndicatorImageView?.visibility = View.GONE
        }
    }

    fun gone() {
        this.visibility = View.GONE
    }

    fun show(callback: (() -> Unit)? = null) {
        if (isShowing) return

        isShowing = true
        this.visibility = View.VISIBLE
        Handler(Looper.getMainLooper()).postDelayed({
            this.visibility = View.GONE
            isShowing = false
            callback?.invoke()
        }, 5000)
    }

    private fun initializeView() {
        mCardView = findViewById(R.id.cardView)
        mContainerLinearLayoutCompat = findViewById(R.id.obu_alert_message_container_linearlayout)
        mIconImageView = findViewById(R.id.obu_alert_message_icon_image_view)
        mTitleTextView = findViewById(R.id.obu_alert_message_title_text_view)
        mDetailTextView = findViewById(R.id.obu_alert_message_detail_text_view)
        mFeeTextView = findViewById(R.id.obu_alert_message_fee_text_view)
        mStatusIndicatorImageView = findViewById(R.id.obu_alert_message_status_indicator_image_view)
    }

}
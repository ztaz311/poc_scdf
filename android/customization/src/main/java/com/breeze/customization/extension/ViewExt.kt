package com.breeze.customization.extension

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.view.View

/**
 * @param duration: duration of animation in milliseconds
 * @param onEnd: callback when animation ends
 * */
fun View.showViewSlideUp(duration: Long, onEnd: () -> Unit = {}){
    visibility = View.VISIBLE
    measure(
        View.MeasureSpec.makeMeasureSpec(
            0,
            View.MeasureSpec.UNSPECIFIED
        ), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
    )
    translationY = measuredHeight.toFloat()
    this.animate()
        .setDuration(duration)
        .translationY(0.0F)
        .setListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                super.onAnimationEnd(animation)
                onEnd.invoke()
            }
        })
}
package com.breeze.customization.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.isVisible
import com.breeze.customization.R
import com.breeze.customization.databinding.AmenitiesOnMapCustomViewBinding
import com.breeze.model.enums.CarParkViewOption

class AmenitiesOnMapStateView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : ConstraintLayout(context, attrs, defStyle) {

    interface AmenitiesOnMapListener {
        fun onCarParkSelected(selectedOption: CarParkViewOption)
        fun onPetrolSelected(currentSelected: Boolean)
        fun onEVChargerSelected(currentSelected: Boolean)
    }

    var onCarParkOptionsVisibilityChangedListener: (isShowing: Boolean) -> Unit = {}
    private var isShowingCarParkOptions = false
        set(value) {
            field = value
            binding.tvAllNearby.isVisible = value
            binding.tvAvailableOnly.isVisible = value
            binding.tvHide.isVisible = value
        }
    var carParkViewOption: CarParkViewOption = CarParkViewOption.ALL_NEARBY
        set(value) {
            field = value
            val selectedOptionTextColor = context.getColor(R.color.themed_text_black)
            val normalTextColor = context.getColor(R.color.themed_light_grey)
            binding.carParkAll.isVisible =
                field == CarParkViewOption.ALL_NEARBY
            binding.tvAllNearby.setTextColor(if (value == CarParkViewOption.ALL_NEARBY) selectedOptionTextColor else normalTextColor)
            binding.carParkAvailableOnly.isVisible =
                field == CarParkViewOption.AVAILABLE_ONLY
            binding.tvAvailableOnly.setTextColor(if (value == CarParkViewOption.AVAILABLE_ONLY) selectedOptionTextColor else normalTextColor)
            binding.carParkHidden.isVisible =
                field == CarParkViewOption.HIDE
            binding.tvHide.setTextColor(if (value == CarParkViewOption.HIDE) selectedOptionTextColor else normalTextColor)
        }

    var isPetrolShown: Boolean = false
        private set(value) {
            field = value
            binding.togglePetrol.isSelected = value
        }
    var isEVChargerShown: Boolean = false
        private set(value) {
            field = value
            binding.toggleEVCharger.isSelected = value
        }

    private val binding: AmenitiesOnMapCustomViewBinding
    private var listener: AmenitiesOnMapListener? = null

    init {
        binding =
            AmenitiesOnMapCustomViewBinding.inflate(LayoutInflater.from(context), this)

        binding.carParkAll.setOnClickListener {
            if (isShowingCarParkOptions) {
                carParkViewOption = CarParkViewOption.ALL_NEARBY
                listener?.onCarParkSelected(carParkViewOption)
            }
            toggleCarParkOptionsVisibility()
        }

        binding.carParkAvailableOnly.setOnClickListener {
            if (isShowingCarParkOptions) {
                carParkViewOption = CarParkViewOption.AVAILABLE_ONLY
                listener?.onCarParkSelected(carParkViewOption)
            }
            toggleCarParkOptionsVisibility()
        }

        binding.carParkHidden.setOnClickListener {
            if (isShowingCarParkOptions) {
                carParkViewOption = CarParkViewOption.HIDE
                listener?.onCarParkSelected(carParkViewOption)
            }
            toggleCarParkOptionsVisibility()
        }


        binding.togglePetrol.setOnClickListener {
            isPetrolShown = !isPetrolShown
            if (isPetrolShown) {
                isEVChargerShown = false
            }
            listener?.onPetrolSelected(isPetrolShown)
        }

        binding.toggleEVCharger.setOnClickListener {
            isEVChargerShown = !isEVChargerShown
            if (isEVChargerShown) {
                isPetrolShown = false
            }
            listener?.onEVChargerSelected(isEVChargerShown)
        }

        carParkViewOption = CarParkViewOption.ALL_NEARBY
        isEVChargerShown = false
        isPetrolShown = false
    }

    fun setListener(listener: AmenitiesOnMapListener) {
        this.listener = listener
    }

    private fun toggleCarParkOptionsVisibility() {
        isShowingCarParkOptions = !isShowingCarParkOptions
        binding.carParkAll.isVisible =
            isShowingCarParkOptions || carParkViewOption == CarParkViewOption.ALL_NEARBY
        binding.carParkAvailableOnly.isVisible =
            isShowingCarParkOptions || carParkViewOption == CarParkViewOption.AVAILABLE_ONLY
        binding.carParkHidden.isVisible =
            isShowingCarParkOptions || carParkViewOption == CarParkViewOption.HIDE
        onCarParkOptionsVisibilityChangedListener.invoke(isShowingCarParkOptions)
    }

    fun hideCarParkOptionsView() {
        if (isShowingCarParkOptions) {
            toggleCarParkOptionsVisibility()
        }
    }

    fun showCarParkOptionsView() {
        if (!isShowingCarParkOptions) {
            toggleCarParkOptionsVisibility()
        }
    }

    fun setPetrolMode(isOn: Boolean) {
        isPetrolShown = isOn
    }

    fun setEVChargerMode(isOn: Boolean) {
        isEVChargerShown = isOn
    }
}
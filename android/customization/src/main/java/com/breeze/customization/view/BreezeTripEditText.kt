package com.breeze.customization.view

import android.content.Context
import android.graphics.drawable.GradientDrawable
import android.text.InputFilter
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.breeze.customization.R
import com.breeze.customization.utils.DigitsInputFilter


class BreezeTripEditText @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : RelativeLayout(context, attrs, defStyle) {

    var mContext: Context? = null
    lateinit var mTVPlaceholder: TextView
    lateinit var mEditText: EditText
    lateinit var mPlaceHolderText: String
    lateinit var mHintText: String
    lateinit var layout: RelativeLayout

    companion object {
        const val DIGITS_BEFORE_DECIMAL = 3
        const val DIGITS_AFTER_DECIMAL = 2
    }

    init {
        LayoutInflater.from(context)
            .inflate(R.layout.breeze_trip_edit_text, this, true)

        mContext = context
        initializeAttributes(attrs)
        initializeView()
    }

    fun initializeAttributes(attrs: AttributeSet?) {
        attrs?.let {
            val typedArray = context.obtainStyledAttributes(
                it,
                R.styleable.breeze_trip_edit_text, 0, 0
            )

            typedArray.recycle()
        }
    }

    private fun initializeView() {
        layout = this.findViewById(R.id.trip_edit_text_layout)
        mEditText = this.findViewById(R.id.custom_edit_text)
        mEditText.filters =
            arrayOf<InputFilter>(DigitsInputFilter(DIGITS_BEFORE_DECIMAL, DIGITS_AFTER_DECIMAL))

        mEditText.setOnEditorActionListener(TextView.OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                mEditText.clearFocus()
                return@OnEditorActionListener false
            }
            false
        })
    }

    fun getEditText(): EditText {
        return mEditText
    }
    fun enableEditText(enabled: Boolean) {
        mEditText.isEnabled = enabled
        if (enabled) {
            layout.background = ContextCompat.getDrawable(context, R.drawable.trip_edit_text_bg)
            mEditText.background =
                ContextCompat.getDrawable(context, R.drawable.breeze_trip_edit_bg)
        } else {
            layout.background = null
            mEditText.background = null
        }
    }

    fun setBGNormal() {
        var drawable =
            ContextCompat.getDrawable(context, R.drawable.trip_edit_text_bg) as GradientDrawable
        drawable.setStroke(4, resources.getColor(R.color.trip_edittext_bg, null))
        layout.background = drawable
    }

    fun setBGError() {
        var drawable =
            ContextCompat.getDrawable(context, R.drawable.trip_edit_text_bg) as GradientDrawable
        drawable.setStroke(4, resources.getColor(R.color.edit_text_red, null))
        layout.background = drawable
    }

}

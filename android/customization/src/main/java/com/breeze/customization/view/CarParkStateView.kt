package com.breeze.customization.view

import android.animation.LayoutTransition
import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.breeze.customization.R
import com.breeze.customization.databinding.CarparkOptionCustomViewBinding
import com.breeze.model.enums.CarParkViewOption
import com.breeze.model.extensions.dpToPx

/**
 * for carpark option custom view
 */
class CarParkStateView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : ConstraintLayout(context, attrs, defStyle) {

    private var isOpen = false
    private var listCarParkOption: ArrayList<ObjectCarParkOption>
    private var listener: ListenerFilterCarParkOption? = null
    private var listenTransition: ListenTransition? = null

    var currentState = CarParkViewOption.ALL_NEARBY
    private val binding: CarparkOptionCustomViewBinding

    init {
        binding = CarparkOptionCustomViewBinding.inflate(
            LayoutInflater.from(context), this
        )
        layoutParams = LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)
        background =
            ContextCompat.getDrawable(context, R.drawable.bg_wrapper_option_carpark)
        clipToPadding = false
        layoutTransition = LayoutTransition()
        elevation = 2.0f.dpToPx()
        setPadding(0, 0, 0, 20.dpToPx())

        listCarParkOption = arrayListOf(
            ObjectCarParkOption(
                CarParkViewOption.ALL_NEARBY,
                getContext().getString(R.string.all_nearby),
                R.drawable.cp_all
            ),
            ObjectCarParkOption(
                CarParkViewOption.AVAILABLE_ONLY,
                getContext().getString(R.string.available_only),
                R.drawable.cp_available
            ),
            ObjectCarParkOption(
                CarParkViewOption.HIDE,
                getContext().getString(R.string.hide),
                R.drawable.cp_off
            )
        )
        currentState = listCarParkOption[0].viewOption
        reloadData()
        collapse()
    }

    fun setState(pState: CarParkViewOption) {
        val tempCarParkObjectCarParkOption: ObjectCarParkOption? = listCarParkOption.find {
            (it.viewOption == pState)
        }?.also {
            currentState = it.viewOption
        }
        tempCarParkObjectCarParkOption?.let { tempObject ->

            if (tempObject.viewOption == CarParkViewOption.HIDE) {
                listCarParkOption.sortBy {
                    when (it.viewOption) {
                        tempObject.viewOption -> {
                            0
                        }

                        CarParkViewOption.ALL_NEARBY -> {
                            1
                        }

                        else -> {
                            2
                        }
                    }
                }

            } else {
                listCarParkOption.sortBy {
                    when (it.viewOption) {
                        tempObject.viewOption -> {
                            0
                        }

                        CarParkViewOption.HIDE -> {
                            2
                        }

                        else -> {
                            1
                        }
                    }
                }
            }
            reloadData()
        }
    }


    /**
     * check carpark available or not
     */
    fun isCarParksShown(): Boolean {
        return currentState == CarParkViewOption.ALL_NEARBY || currentState == CarParkViewOption.AVAILABLE_ONLY
    }


    fun setListener(pListener: ListenerFilterCarParkOption) {
        listener = pListener
    }


    fun setListenerTransition(pListenTransition: ListenTransition?) {
        listenTransition = pListenTransition
    }

    private fun reloadData() {
        listCarParkOption.forEachIndexed { index, objectCarParkOption ->
            when (index) {
                0 -> {
                    binding.imgTop.setImageResource(objectCarParkOption.imageID)
                    binding.tvTop.text = objectCarParkOption.name
                    binding.imgTop.setOnClickListener {
                        toggle()
                    }
                }

                1 -> {
                    binding.imgSecond.setImageResource(objectCarParkOption.imageID)
                    binding.tvSecond.text = objectCarParkOption.name
                    binding.imgSecond.setOnClickListener {
                        handleClickFilter(objectCarParkOption)
                        reloadData()
                        collapse()
                    }
                }

                2 -> {
                    binding.imgThree.setImageResource(objectCarParkOption.imageID)
                    binding.tvThree.text = objectCarParkOption.name
                    binding.imgThree.setOnClickListener {
                        handleClickFilter(objectCarParkOption)
                        reloadData()
                        collapse()
                    }
                }
            }
        }
    }

    private fun handleClickFilter(pObjectCarParkOption: ObjectCarParkOption) {

        if (pObjectCarParkOption.viewOption == CarParkViewOption.HIDE) {
            listCarParkOption.sortBy {
                when (it.viewOption) {
                    pObjectCarParkOption.viewOption -> {
                        0
                    }

                    CarParkViewOption.ALL_NEARBY -> {
                        1
                    }

                    else -> {
                        2
                    }
                }
            }
        } else {
            listCarParkOption.sortBy {
                when (it.viewOption) {
                    pObjectCarParkOption.viewOption -> {
                        0
                    }

                    CarParkViewOption.HIDE -> {
                        2
                    }

                    else -> {
                        1
                    }
                }
            }
        }
        currentState = pObjectCarParkOption.viewOption
        postListener()
    }

    fun gone() {
        this.visibility = GONE
    }

    fun show() {
        // this.visibility = View.VISIBLE
        this.visibility = GONE
    }

    /**
     * handle check post event
     */
    private fun postListener() {
        when (currentState) {
            CarParkViewOption.ALL_NEARBY -> {
                listener?.all()
            }

            CarParkViewOption.AVAILABLE_ONLY -> {
                listener?.available()
            }

            CarParkViewOption.HIDE -> {
                listener?.hide()
            }
        }
    }

    private fun toggle() {
        if (isOpen) {
            collapse()
        } else {
            expand()
        }
    }

    fun collapse() {
        binding.root.setBackgroundColor(Color.TRANSPARENT)
        binding.wrapperExpand.visibility = GONE
        isOpen = false
        listenTransition?.collapse()
    }

    fun expand() {
        binding.root.setBackgroundResource(R.drawable.bg_wrapper_option_carpark)
        binding.wrapperExpand.visibility = VISIBLE
        isOpen = true
        listenTransition?.expand()
    }

    interface ListenerFilterCarParkOption {
        fun all()
        fun available()
        fun hide()
    }

    interface ListenTransition {
        fun collapse()
        fun expand()
    }

    data class ObjectCarParkOption(
        var viewOption: CarParkViewOption,
        var name: String,
        var imageID: Int
    )
}
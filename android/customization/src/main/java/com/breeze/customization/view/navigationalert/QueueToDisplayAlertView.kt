package com.breeze.customization.view.navigationalert

import android.content.Context
import android.os.Handler
import android.os.Looper
import android.os.SystemClock
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import com.breeze.customization.R
import com.breeze.model.api.response.amenities.BaseAmenity
import com.breeze.model.extensions.dpToPx
import com.breeze.model.obu.OBURoadEventData

class QueueToDisplayAlertView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null
) : FrameLayout(context, attrs) {
    companion object {
        private const val NO_AUTO_DISMISS = -99L
    }

    private var handler: Handler? = null
    private val viewMap = HashMap<View, Pair<Long, Boolean>>()
    private val handlerCallback: Handler.Callback = Handler.Callback {
        val currentTime = SystemClock.elapsedRealtime()
        val listRemovedView = mutableListOf<View>()
        viewMap.forEach { (view, pair) ->
            val time = pair.first
            if (time != NO_AUTO_DISMISS && time < currentTime) {
                removeView(view)
                listRemovedView.add(view)
            }
        }
        listRemovedView.forEach { view ->
            viewMap.remove(view)
        }
        it.target.sendEmptyMessageDelayed(999, 1000)
        true
    }

    init {
        handler = Handler(Looper.getMainLooper(), handlerCallback)
    }


    fun addGeneralMessageView(message: String, detailMessage: String, delay: Long = 5000) {
        val view = OBUAlertMessageGeneralView(context)
        view.setData(message, detailMessage)
        addAlertView(view, delay)
    }

    fun addOBUConnectionErrorView(delay: Long = 5000) {
        val view = LayoutInflater.from(context)
            .inflate(R.layout.layout_navigation_obu_connect_error, this, false)
        addAlertView(view, delay)
    }

    fun addCarparkAvailabilityView(
        name: String,
        type: String,
        amenity: BaseAmenity?,
        navigateCallback: (BaseAmenity, String?) -> Unit,
        closeCallback: () -> Unit,
        delay: Long = 8000,
    ) {
        val view = CarparkAvailabilityAlertView(context)
        view.closeAlertClickedListener = {
            closeCallback.invoke()
            viewMap[view] = Pair(0L, false)
            removeView(view)
        }
        view.navigateClickedListener = { a, band ->
            navigateCallback.invoke(a, band)
            viewMap[view] = Pair(0L, false)
            removeView(view)
        }
        view.setData(name, type, amenity)
        addAlertView(view, delay, true)
    }

    fun addOBUAlertERPView(eventData: OBURoadEventData, delay: Long = 5000) {
        val view = OBUAlertERPView(context)
        viewMap.forEach { (v, time) ->
            if (v is CarparkAvailabilityAlertView) {
                viewMap[v] = Pair(0L, false)
                removeViewInLayout(v)
            }
        }
        view.setERPContent(eventData)

        addAlertView(view, delay)
    }

    private fun addAlertView(view: View, delay: Long, isPriority: Boolean = false) {
        val timeToDismiss =
            if (delay != NO_AUTO_DISMISS) SystemClock.elapsedRealtime() + delay else NO_AUTO_DISMISS
        val hasPriorityView = viewMap.asIterable()
            .find { it.value.second } != null
        if (!hasPriorityView) {
            viewMap[view] = Pair(timeToDismiss, isPriority)
            addView(
                view,
                LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT).apply {
                    topMargin = 20.dpToPx()
                })
        }
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        handler?.sendEmptyMessage(999)
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        handler?.removeCallbacksAndMessages(null)
    }
}
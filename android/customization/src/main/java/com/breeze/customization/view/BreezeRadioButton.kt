package com.breeze.customization.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import com.breeze.customization.R
import kotlin.properties.Delegates

class BreezeRadioButton @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : RelativeLayout(context, attrs, defStyle) {


    var mContext: Context? = null
    var mRightText: String? = null
    var mSelectedLeftIcon by Delegates.notNull<Int>()
    var mUnselectedLeftIcon by Delegates.notNull<Int>()
    var mSelectedBackground by Delegates.notNull<Int>()
    var mUnselectedBackground by Delegates.notNull<Int>()

    lateinit var mTextView: TextView
    lateinit var layout: RelativeLayout
    lateinit var mImageView: ImageView

    init {
        LayoutInflater.from(context)
            .inflate(R.layout.breeze_radio_button, this, true)

        mContext = context
        initializeAttributes(attrs)
        initializeView()
    }


    private fun initializeAttributes(attrs: AttributeSet?) {
        attrs?.let {
            val typedArray = context.obtainStyledAttributes(
                it,
                R.styleable.breeze_radio_button, 0, 0
            )

            mRightText = resources.getString(
                typedArray.getResourceId(
                    R.styleable.breeze_radio_button_display_radio_btn_text,
                    R.string.empty_text
                )
            )

            mSelectedLeftIcon = typedArray.getResourceId(
                R.styleable.breeze_radio_button_selected_left_icon,
                -1
            )

            mUnselectedLeftIcon = typedArray.getResourceId(
                R.styleable.breeze_radio_button_unselected_left_icon,
                -1
            )

            mSelectedBackground = typedArray.getResourceId(
                R.styleable.breeze_radio_button_selected_bg,
                -1
            )

            mUnselectedBackground = typedArray.getResourceId(
                R.styleable.breeze_radio_button_unselected_bg,
                -1
            )

            typedArray.recycle()
        }
    }

    private fun initializeView() {
        layout = this.findViewById(R.id.radio_button)
        mImageView = this.findViewById(R.id.radio_btn_img)
        mTextView = this.findViewById(R.id.radio_btn_name)

        mTextView.text = mRightText
        setUnselectedState()
    }

    fun setUnselectedState() {
        layout.setBackgroundResource(mUnselectedBackground)
        mImageView.setImageResource(mUnselectedLeftIcon)
        mTextView.setTextColor(
            context.resources.getColor(
                R.color.themed_route_preference_text_not_selected,
                null
            )
        )
    }

    fun setSelectedState() {
        layout.setBackgroundResource(mSelectedBackground)
        mImageView.setImageResource(mSelectedLeftIcon)
        mTextView.setTextColor(
            context.resources.getColor(
                R.color.themed_route_preference_text_selected,
                null
            )
        )
    }

}
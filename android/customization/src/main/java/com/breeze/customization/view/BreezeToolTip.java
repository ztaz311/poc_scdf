package com.breeze.customization.view;

import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.PopupWindow;

import androidx.annotation.LayoutRes;

import com.breeze.customization.R;

public class BreezeToolTip {

    private PopupWindow tipWindow;
    private View contentView;
    private LayoutInflater inflater;

    public BreezeToolTip(Context ctx, @LayoutRes int resource) {
        tipWindow = new PopupWindow(ctx);

        inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        contentView = inflater.inflate(resource, null);
    }

    public BreezeToolTip(Context ctx, View contentView) {
        tipWindow = new PopupWindow(ctx);
        inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.contentView = contentView;
    }

    public void showToolTip(View anchor) {

        tipWindow.setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
        tipWindow.setWidth(LinearLayout.LayoutParams.WRAP_CONTENT);
        tipWindow.setElevation(20f);
        tipWindow.setAnimationStyle(R.style.popup_animation);
        tipWindow.setBackgroundDrawable(new BitmapDrawable());
        tipWindow.setContentView(contentView);

        int[] screen_pos = new int[2];
        // Get location of anchor view on screen
        anchor.getLocationOnScreen(screen_pos);

        // Get rect for anchor view
        Rect anchor_rect = new Rect(screen_pos[0], screen_pos[1], screen_pos[0]
                + anchor.getWidth(), screen_pos[1] + anchor.getHeight());

        // Call view measure to calculate how big your view should be.
        contentView.measure(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);

        int contentViewHeight = contentView.getMeasuredHeight();
        int contentViewWidth = contentView.getMeasuredWidth();
        // In this case , i dont need much calculation for x and y position of
        // tooltip
        // For cases if anchor is near screen border, you need to take care of
        // direction as well
        // to show left, right, above or below of anchor view
        int position_x = anchor_rect.centerX() - (contentViewWidth / 2);
        int position_y = (int) (anchor_rect.top - (contentViewHeight * 1.15));

        tipWindow.showAtLocation(anchor, Gravity.NO_GRAVITY, position_x, position_y);
    }

    public void dismissTooltip() {
        if (tipWindow != null && tipWindow.isShowing())
            tipWindow.dismiss();
    }

}

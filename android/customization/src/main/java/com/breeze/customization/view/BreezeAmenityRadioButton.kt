package com.breeze.customization.view

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import com.breeze.customization.R
import kotlin.properties.Delegates

class BreezeAmenityRadioButton @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : AppCompatTextView(context, attrs, defStyle) {

    var amenitySelected = false
    var onAmenityStateChange: OnAmenityStateChange? = null

    var mSelectedLeftIcon by Delegates.notNull<Int>()
    var mUnselectedLeftIcon by Delegates.notNull<Int>()
    var mSelectedBackground by Delegates.notNull<Int>()
    var mUnselectedBackground by Delegates.notNull<Int>()

    init {
        initializeAttributes(attrs)
        initializeView()
        initializeListeners()
    }

    private fun initializeAttributes(attrs: AttributeSet?) {
        attrs?.let {
            val typedArray = context.obtainStyledAttributes(
                it,
                R.styleable.BreezeAmenityRadioButton, 0, 0
            )


            mSelectedLeftIcon = typedArray.getResourceId(
                R.styleable.BreezeAmenityRadioButton_selected_left_icon,
                -1
            )

            mUnselectedLeftIcon = typedArray.getResourceId(
                R.styleable.BreezeAmenityRadioButton_unselected_left_icon,
                -1
            )

            mSelectedBackground = typedArray.getResourceId(
                R.styleable.BreezeAmenityRadioButton_selected_bg,
                -1
            )

            mUnselectedBackground = typedArray.getResourceId(
                R.styleable.BreezeAmenityRadioButton_unselected_bg,
                -1
            )

            typedArray.recycle()
        }
    }


    private fun initializeView() {
        setUnselectedState()
    }

    private fun initializeListeners() {
        setOnClickListener {
            if (amenitySelected) {
                setUnselectedState()
            } else {
                setSelectedState()
            }
        }
    }


    fun setUnselectedState(isResetState: Boolean = false) {
        val lastState = amenitySelected
        amenitySelected = false
        setBackgroundResource(mUnselectedBackground)
        setCompoundDrawablesWithIntrinsicBounds(
            ContextCompat.getDrawable(context, mUnselectedLeftIcon),
            null,
            null,
            null
        )
//        setTextColor(getResources().getColor(R.color.black, null))
        setTextColor(resources.getColor(R.color.themed_text_black, null))
        if (!lastState) return
        onAmenityStateChange?.onUnSelectedState(isResetState)
    }

    fun setSelectedState() {
        amenitySelected = true
        setBackgroundResource(mSelectedBackground)
        setCompoundDrawablesWithIntrinsicBounds(
            ContextCompat.getDrawable(context, mSelectedLeftIcon),
            null,
            null,
            null
        )
        setTextColor(resources.getColor(R.color.white, null))
        onAmenityStateChange?.onSelectedState()
    }

    interface OnAmenityStateChange {
        fun onSelectedState()
        fun onUnSelectedState(isResetState: Boolean)
    }
}
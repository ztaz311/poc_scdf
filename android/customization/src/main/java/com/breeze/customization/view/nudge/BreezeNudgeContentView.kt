package com.breeze.customization.view.nudge

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import com.breeze.customization.databinding.BreezeNudgeContentViewBinding

class BreezeNudgeContentView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : FrameLayout(context, attrs, defStyle) {

    private val viewBinding =
        BreezeNudgeContentViewBinding.inflate(LayoutInflater.from(context), this, true)

    fun renderContent(
        strTitle: String,
        strDesc: String,
        screenName: String,
        strIndex: String? = null,
        strAction: String? = null,
        onClose: (() -> Unit)? = null,
        onAction: (() -> Unit)? = null,
    ) {
        viewBinding.txtTitle.text = strTitle
        viewBinding.txtDesc.text = strDesc
        if (strIndex.isNullOrEmpty()) {
            viewBinding.txtIndex.visibility = View.GONE
        } else {

            viewBinding.txtIndex.visibility = View.VISIBLE
            viewBinding.txtIndex.text = strIndex
        }
        if (strAction.isNullOrEmpty()) {
            viewBinding.txtAction.visibility = View.GONE
        } else {

            viewBinding.txtAction.visibility = View.VISIBLE
            viewBinding.txtAction.text = strAction
            viewBinding.txtAction.setOnClickListener {
//                when (strAction) {
//                    "Next" -> Analytics.logClickEvent(Event.NUDGE_NEXT, screenName)
//                    "Done" -> Analytics.logClickEvent(Event.NUDGE_DONE, screenName)
//                }
                onAction?.invoke()
            }
        }
        viewBinding.imgCloseNudge.setOnClickListener {
//            Analytics.logClickEvent(Event.NUDGE_CLOSE, Screen.HOME)
            onClose?.invoke()
        }
    }

}
package com.breeze.customization.view.navigationalert

import android.content.Context
import android.graphics.Typeface
import android.os.Handler
import android.os.Looper
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import com.breeze.customization.R
import com.breeze.customization.extension.setDrawableColor

class OBUAlertMessageViewWithActions @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : ConstraintLayout(context, attrs, defStyle) {

    enum class OBUAlertMessageType {
        CAR_PARK_UPDATE,
        TRAVEL_TIME
    }

    interface OBUAlertMessageViewListener {
        fun onReadMoreSelected()
    }

    companion object {
        var isShowing = false
    }

    private var mContext: Context? = null
    private var mCardView: CardView? = null
    private var mContainerLinearLayoutCompat: LinearLayoutCompat? = null
    private var mIconImageView: ImageView? = null
    private var mTitleTextView: TextView? = null
    private var mDetailTextView: TextView? = null
    private var mReadMoreTextView: TextView? = null
    private var listener: OBUAlertMessageViewListener? = null

    init {
        LayoutInflater.from(context)
            .inflate(R.layout.layout_obu_alert_message_view_with_actions, this, true)
        mContext = context
        initializeView()
    }

    fun setContent(
        alertType: OBUAlertMessageType,
        backgroundColor: Int,
        iconResourceId: Int,
        title: String,
        detail: String
    ) {
        mContainerLinearLayoutCompat?.setBackgroundColor(backgroundColor)
        mIconImageView?.setImageResource(iconResourceId)
        mTitleTextView?.text = title
        mDetailTextView?.text = detail
        mDetailTextView?.textSize = 22.0F
        mReadMoreTextView?.background?.setDrawableColor(backgroundColor)
        when (alertType) {
            OBUAlertMessageType.CAR_PARK_UPDATE -> {
                mDetailTextView?.visibility = View.VISIBLE
                mTitleTextView?.typeface = Typeface.DEFAULT
                mTitleTextView?.textSize = 24.0F
            }

            OBUAlertMessageType.TRAVEL_TIME -> {
                mDetailTextView?.visibility = View.GONE
                mTitleTextView?.typeface = Typeface.DEFAULT_BOLD
                mTitleTextView?.textSize = 28.0F
            }
        }
    }

    fun gone() {
        this.visibility = View.GONE
    }

    fun show(callback: (() -> Unit)? = null) {
        if (isShowing) return

        isShowing = true
        this.visibility = View.VISIBLE
        Handler(Looper.getMainLooper()).postDelayed({
            this.visibility = View.GONE
            isShowing = false
            callback?.invoke()
        }, 5000)
    }

    fun setListener(listener: OBUAlertMessageViewListener) {
        this.listener = listener
    }

    private fun initializeView() {
        mCardView = findViewById(R.id.cardView)
        mContainerLinearLayoutCompat =
            findViewById(R.id.obu_alert_message_with_actions_container_linearlayout)
        mIconImageView = findViewById(R.id.obu_alert_message_icon_image_view)
        mTitleTextView = findViewById(R.id.obu_alert_message_title_text_view)
        mDetailTextView = findViewById(R.id.obu_alert_message_detail_text_view)
        mReadMoreTextView = findViewById(R.id.obu_alert_message_read_more_text_view)
        mReadMoreTextView?.setOnClickListener {
            listener?.onReadMoreSelected()
        }
    }

}
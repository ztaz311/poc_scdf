package com.breeze.customization.view

import android.content.Context
import android.graphics.Rect
import android.graphics.drawable.GradientDrawable
import android.text.*
import android.text.InputFilter.LengthFilter
import android.text.method.LinkMovementMethod
import android.text.method.PasswordTransformationMethod
import android.text.method.TransformationMethod
import android.util.AttributeSet
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnFocusChangeListener
import android.widget.EditText
import android.widget.RelativeLayout
import androidx.core.content.ContextCompat
import com.breeze.customization.R
import com.breeze.customization.databinding.BreezeEditTextBinding
import timber.log.Timber
import kotlin.properties.Delegates


class BreezeEditText @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : RelativeLayout(context, attrs, defStyle) {

    lateinit var mHintText: String

    var mLeftIconDrawable by Delegates.notNull<Int>()
    lateinit var tooltip: BreezeToolTip
    var breezeEditTextListener: BreezeEditTextListener? = null
    var analyticsScreenName: String? = null
    var mSupportBlackTheme: Boolean = false
    var inputLimit: Int = -1
    var isTypePassword: Boolean = false
    var showPassword: Boolean = false
    var showPasswordTooltip: Boolean = false
    var showCrossIcon: Boolean = false
    var emailInputType: Boolean = false
    var verificationEmailInputType: Boolean = false
    var phoneInputType: Boolean = false
    var searchInputType: Boolean = false
    var editTextBGLayout: Int = -1
    var editTextBGColour: Int = -1
    var normalBG: Int = -1

    var focusChangeListener: OnFocusChangeListener = OnFocusChangeListener { view, b ->
        if (b) {
            breezeEditTextListener?.onBreezeEditTextFocusChange(true)
            hideErrorView()
            setBGFocused()
            if (isTypePassword) {
                if (showPasswordTooltip) tooltip.showToolTip(binding.edittextLayout)
            }
        } else {
            setBGNormal()
            if (!isTypePassword) {
                binding.rightIcon.visibility = View.GONE
            }
            showPassword = false
            if (showPasswordTooltip) tooltip.dismissTooltip()
            breezeEditTextListener?.onBreezeEditTextFocusChange(false)
        }
    }
    val binding = BreezeEditTextBinding.inflate(LayoutInflater.from(context), this, true)

    init {
        initializeAttributes(attrs)
        initializeView()
    }

    private fun initializeAttributes(attrs: AttributeSet?) {
        attrs?.let {
            val typedArray = context.obtainStyledAttributes(
                it,
                R.styleable.breeze_edit_text, 0, 0
            )
            mHintText = resources.getString(
                typedArray.getResourceId(
                    R.styleable.breeze_edit_text_hint_text,
                    R.string.empty_text
                )
            )

            isTypePassword = typedArray.getBoolean(R.styleable.breeze_edit_text_isPassword, false)
            showCrossIcon = typedArray.getBoolean(R.styleable.breeze_edit_text_showCrossIcon, false)
            emailInputType =
                typedArray.getBoolean(R.styleable.breeze_edit_text_emailInputType, false)
            phoneInputType =
                typedArray.getBoolean(R.styleable.breeze_edit_text_phoneInputType, false)
            searchInputType =
                typedArray.getBoolean(R.styleable.breeze_edit_text_searchInputType, false)
            verificationEmailInputType = typedArray.getBoolean(
                R.styleable.breeze_edit_text_verificationEmailInputType,
                false
            )
            inputLimit = typedArray.getInt(R.styleable.breeze_edit_text_inputLimit, 0)

            mLeftIconDrawable = typedArray.getResourceId(
                R.styleable.breeze_edit_text_left_drawable,
                0
            )
            showPasswordTooltip = typedArray.getBoolean(
                R.styleable.breeze_edit_text_showToolTip,
                false
            )
            editTextBGColour = typedArray.getColor(
                R.styleable.breeze_edit_text_editText_Background,
                resources.getColor(R.color.edit_text_grey, null)
            )

            mSupportBlackTheme =
                typedArray.getBoolean(R.styleable.breeze_edit_text_edit_support_dark_theme, false)

            typedArray.recycle()
        }
    }

    fun redrawTooltipIfShown() {
        if (isTypePassword && showPasswordTooltip) {
            tooltip.dismissTooltip()
            tooltip = BreezeToolTip(context, R.layout.layout_pwd_tooltip)
            tooltip.showToolTip(binding.edittextLayout)
        }
    }

    fun getEnteredText(): String {
        return binding.customEditText.text.toString().trim()
    }


    private fun initializeView() {
        binding.customEditText.hint = mHintText
        binding.customEditText.setHintTextColor(
            ContextCompat.getColor(
                context,
                R.color.edit_text_hint_color
            )
        )
        //  binding.customEditText.addTextChangedListener(TextWatcherDelegator());
        binding.customEditText.onFocusChangeListener = focusChangeListener
        binding.customEditText.isSingleLine = true

        if (mSupportBlackTheme) {
            editTextBGLayout = R.drawable.themed_breeze_edit_text
            normalBG = R.color.themed_edit_text_bg
            editTextBGColour = resources.getColor(R.color.themed_edit_text_bg, null)
            binding.customEditText.setTextColor(
                ContextCompat.getColor(
                    context,
                    R.color.themed_edit_text_color
                )
            )
            binding.customEditText.setHintTextColor(
                ContextCompat.getColor(
                    context,
                    R.color.themed_text_grey
                )
            )
        } else {
            editTextBGLayout = R.drawable.breeze_edit_text_bg
            normalBG = R.color.edit_text_grey
        }



        binding.leftIcon.setImageResource(mLeftIconDrawable)
        setBGNormal()

        when {
            isTypePassword -> {
                binding.customEditText.inputType =
                    InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
                tooltip = BreezeToolTip(context, R.layout.layout_pwd_tooltip)
                binding.customEditText.transformationMethod = PasswordTransformationMethod()
                binding.rightIcon.setOnClickListener {
                    showPassword = !showPassword
//                    var eventName = Event.UNHIDE_PASSWORD
                    if (showPassword) {
                        binding.customEditText.transformationMethod = doNothingTransformation
                        binding.rightIcon.setImageResource(R.drawable.eye)
                    } else {
//                        eventName = Event.HIDE_PASSWORD
                        binding.customEditText.transformationMethod = PasswordTransformationMethod()
                        binding.rightIcon.setImageResource(R.drawable.eye_slash)
                    }
//                    if (analyticsScreenName != null)
//                        Analytics.logClickEvent(eventName, analyticsScreenName!!)
                }
                binding.rightIcon.visibility = View.VISIBLE

            }

            showCrossIcon -> {
                binding.rightIcon.setImageResource(R.drawable.cross)
                binding.rightIcon.setOnClickListener {
                    breezeEditTextListener?.onClearClick()
                    binding.customEditText.setText("")
                }
            }

            else -> {
                binding.rightIcon.visibility = View.GONE
            }
        }

        when {
            emailInputType -> {
                binding.customEditText.inputType = InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS
            }

            verificationEmailInputType -> {
                binding.customEditText.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 18f)
                binding.customEditText.inputType = InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS
            }

            phoneInputType -> {
                binding.customEditText.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 18f)
                val maxLength = 8
                binding.layoutCountryCode.visibility = View.VISIBLE
                binding.customEditText.inputType = InputType.TYPE_CLASS_NUMBER
                binding.customEditText.filters = arrayOf<InputFilter>(LengthFilter(maxLength))
            }

            searchInputType -> {
                binding.customEditText.setTextColor(context.resources.getColor(R.color.destination_black))
                binding.customEditText.setHintTextColor(context.resources.getColor(R.color.destination_black))
            }

            else -> {
                hideRightIcon()
            }
        }
        if (inputLimit != 0) {
            binding.customEditText.filters = arrayOf<InputFilter>(LengthFilter(inputLimit))
        }
    }


    private var doNothingTransformation: TransformationMethod = object : TransformationMethod {
        override fun getTransformation(p0: CharSequence?, p1: View?): CharSequence? {
            return p0
        }

        override fun onFocusChanged(p0: View?, p1: CharSequence?, p2: Boolean, p3: Int, p4: Rect?) {

        }
    }


    fun setBGNormal() {
        val drawable =
            ContextCompat.getDrawable(context, editTextBGLayout) as GradientDrawable
        drawable.setStroke(4, resources.getColor(normalBG))
        drawable.setColor(editTextBGColour)
        binding.edittextLayout.background = drawable
    }

    private fun setBGFocused() {
        val drawable =
            ContextCompat.getDrawable(context, editTextBGLayout) as GradientDrawable
        drawable.setStroke(4, resources.getColor(R.color.breeze_primary))
        binding.edittextLayout.background = drawable
    }

    fun setBGError() {
        binding.customEditText.clearFocus()
        val drawable =
            ContextCompat.getDrawable(context, editTextBGLayout) as GradientDrawable
        drawable.setStroke(4, resources.getColor(R.color.edit_text_red))
        binding.edittextLayout.background = drawable
    }

    fun setFocusChangedListener(listener: OnFocusChangeListener) {
        binding.customEditText.onFocusChangeListener = listener
    }

    fun showErrorView(errorMsg: String) {
        setBGError()
        binding.error.visibility = View.VISIBLE
        binding.error.text = errorMsg
    }

    fun showErrorView(errorMsg: SpannableString) {
        setBGError()
        binding.error.visibility = View.VISIBLE
        binding.error.text = errorMsg
        binding.error.movementMethod = LinkMovementMethod.getInstance()
        binding.error.highlightColor = context.resources.getColor(R.color.edit_text_red, null)
    }

    fun hideErrorView() {
        setBGNormal()
        binding.error.visibility = View.GONE
    }

    fun updateRightIcon(image: Int) {
        binding.rightIcon.visibility = View.VISIBLE

        val scale = resources.displayMetrics.density
        val dpAsPixels = (15 * scale + 0.5f)

        binding.rightIcon.setPadding(0, 0, dpAsPixels.toInt(), 0)
        binding.rightIcon.setImageResource(image)
    }

    fun hideRightIcon() {
        binding.rightIcon.visibility = View.GONE
    }

    fun getEditText(): EditText {
        return binding.customEditText
    }

    fun setEditTextListener(editTextListener: BreezeEditTextListener) {
        this.breezeEditTextListener = editTextListener
    }

    @JvmName("setScreenName1")
    fun setScreenName(screenName: String) {
        this.analyticsScreenName = screenName
    }

    fun addTextChangedListener(textWatcher: TextWatcher) {
        binding.customEditText.addTextChangedListener(textWatcher)
    }

    fun maximumLength() {
        val maxLength = 20
        binding.customEditText.filters = arrayOf<InputFilter>(LengthFilter(maxLength))


    }

    class TextWatcherDelegator : TextWatcher {
        override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            Timber.d(" beforeTextChanged :: $charSequence")
        }

        override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
        override fun afterTextChanged(editable: Editable) {
            Timber.d(" afterTextChanged :: $editable")
        }
    }

    interface BreezeEditTextListener {
        fun onBreezeEditTextFocusChange(isFocused: Boolean)
        fun onClearClick() {

        }
    }

    fun setRightIconVisibility(isVisible: Boolean) {
        binding.rightIcon.visibility = if (isVisible) View.VISIBLE else View.GONE
    }

    fun showTextLengthView() {
        binding.tvTextLength.visibility = View.VISIBLE
    }

    fun setTextForLength(text: String) {
        binding.tvTextLength.text = text
    }

}
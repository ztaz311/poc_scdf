package com.breeze.customization.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import com.breeze.customization.R
import com.breeze.customization.databinding.LayoutObuAmenitiesViewBinding
import com.breeze.model.extensions.dpToPx

class OBUAmenityToggleView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null
) : LinearLayout(context, attrs) {
    private val binding: LayoutObuAmenitiesViewBinding
    private var callback: ((Boolean, Boolean, Boolean) -> Unit)? = null
    private var callbackCarpark: ((Boolean) -> Unit)? = null
    private var callbackPetrol: ((Boolean) -> Unit)? = null
    private var callbackEV: ((Boolean) -> Unit)? = null
    private val defaultPadding = 5.dpToPx()
    var toggleType: AmenityToggleType = AmenityToggleType.NONE
        set(value) {
            field = value
            binding.btnParkingToggle.isSelected = value == AmenityToggleType.CARPARK
            binding.btnPetrolToggle.isSelected = value == AmenityToggleType.PETROL
            binding.btnEVToggle.isSelected = value == AmenityToggleType.EVCHARGER
        }

    private val isParkingShown: Boolean
        get() = toggleType == AmenityToggleType.CARPARK

    private val isPetrolShown: Boolean
        get() = toggleType == AmenityToggleType.PETROL
    private val isEVChargerShown: Boolean
        get() = toggleType == AmenityToggleType.EVCHARGER

    init {
        orientation = VERTICAL
        setPadding(defaultPadding, defaultPadding, defaultPadding, defaultPadding)
        setBackgroundResource(R.drawable.bg_custom_navigation_amenities_view)

        binding = LayoutObuAmenitiesViewBinding.inflate(LayoutInflater.from(context), this)

        with(binding) {
            btnParkingToggle.setOnClickListener {
                changeToggleType(AmenityToggleType.CARPARK)
                callbackCarpark?.invoke(isParkingShown)
            }
            btnPetrolToggle.setOnClickListener {
                changeToggleType(AmenityToggleType.PETROL)
                callbackPetrol?.invoke(isPetrolShown)
            }
            btnEVToggle.setOnClickListener {
                changeToggleType(AmenityToggleType.EVCHARGER)
                callbackEV?.invoke(isEVChargerShown)

            }
        }
    }

    private fun changeToggleType(type: AmenityToggleType) {
        toggleType = if (toggleType == type) {
            AmenityToggleType.NONE
        } else
            type
        callback?.invoke(isParkingShown, isPetrolShown, isEVChargerShown)
    }

    fun clearToggle() {
        changeToggleType(AmenityToggleType.NONE)
    }

    fun setAmenitiesViewCallback(callback: (Boolean, Boolean, Boolean) -> Unit) {
        this.callback = callback
    }

    fun setToggleCarparkClickedCallback(callback: (Boolean) -> Unit) {
        callbackCarpark = callback
    }

    fun setTogglePetrolClickedCallback(callback: (Boolean) -> Unit) {
        callbackPetrol = callback
    }

    fun setToggleEVChargerClickedCallback(callback: (Boolean) -> Unit) {
        callbackEV = callback
    }

    enum class AmenityToggleType(value: String) {
        NONE(""),
        CARPARK(""),
        PETROL(""),
        EVCHARGER(""),
    }
}
package com.breeze.customization.view

import android.content.Context
import android.util.AttributeSet
import android.widget.LinearLayout

class BreezeVehicleSelectionRadioGroup @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : LinearLayout(context, attrs, defStyle) {


    fun setAllButtonsToUnselectedState() {
        var container = this
        for (i in 0 until container.childCount) {
            val child = container.getChildAt(i)
            if (child is BreezeVehicleSelectionRadioButton) {
                val view: BreezeVehicleSelectionRadioButton = child
                view.setUnselectedState()
            }
        }
    }

    fun setOtherButtonsToUnselectedState(view: BreezeVehicleSelectionRadioButton) {
        for (i in 0 until childCount) {
            val child = getChildAt(i)
            if (child is BreezeVehicleSelectionRadioButton) {
                if (view == child) {
                    continue
                }
                child.setUnselectedState()
            }
        }
    }

}
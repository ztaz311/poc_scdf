package com.breeze.customization.view

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatTextView
import com.breeze.customization.R

class BreezeSimpleRadioButton @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : AppCompatTextView(context, attrs, defStyle) {

    var buttonSelected = false

    init {

        initializeView()

    }

    private fun initializeView() {
        setUnselectedState()
    }


    fun setUnselectedState() {
        buttonSelected = false
        setTextColor(getResources().getColor(R.color.silver_grey, null))
    }

    fun setSelectedState() {
        buttonSelected = true
        setTextColor(getResources().getColor(R.color.breeze_primary, null))
    }

}
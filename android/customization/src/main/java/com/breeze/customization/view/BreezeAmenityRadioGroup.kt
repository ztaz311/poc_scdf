package com.breeze.customization.view

import android.content.Context
import android.util.AttributeSet
import android.widget.LinearLayout

class BreezeAmenityRadioGroup @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : LinearLayout(context, attrs, defStyle) {


    fun setAllButtonsToUnselectedState() {
        var container = this
        for (i in 0 until container.childCount) {
            val child = container.getChildAt(i)
            if (child is BreezeAmenityRadioButton) {
                val view: BreezeAmenityRadioButton = child
                view.setUnselectedState(true)
            }
        }
    }

    fun setOtherButtonsToUnselectedState(view: BreezeAmenityRadioButton) {
        var container = this
        for (i in 0 until container.childCount) {
            val child = container.getChildAt(i)
            if (child is BreezeAmenityRadioButton) {
                if (view == child) {
                    continue
                }
                child.setUnselectedState()
            }
        }
    }

}
package com.breeze.customization.view

import android.content.Context
import android.graphics.drawable.GradientDrawable
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View

import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import com.breeze.customization.R
import com.breeze.customization.databinding.AlertRoutePlanViewBinding
import com.breeze.customization.extension.showViewSlideUp
import com.breeze.model.api.response.ValueConditionShowBroadcast
import com.breeze.model.extensions.dpToPx
import com.bumptech.glide.Glide

class AlertRoutePlanView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null
) : ConstraintLayout(context, attrs) {

    companion object {
        const val VIEW_ANIMATION_DURATION = 1000L
        var isShowing = false
    }

    private var mTheme: Theme = ThemeDefault
    private var iconUrl: String? = null
    private val binding: AlertRoutePlanViewBinding
    private var onViewClickedListener: ((Any) -> Unit)? = null
    private var otherData: Any? = null

    var notificationCriterias: ArrayList<ValueConditionShowBroadcast>? = null

    sealed class Theme(
        val icon: Int,
        val backgroundColor: Int,
        val titleTextColor: Int,
        val showArrowRight: Boolean = false
    )

    object ThemeDefault : Theme(
        icon = R.drawable.park_icon_broadcast,
        backgroundColor = R.color.carpark_reset_text,
        titleTextColor = R.color.white,
    )

    object ThemeNationalDayBroadcast : Theme(
        icon = R.drawable.exclamation_circle,
        backgroundColor = R.color.white,
        titleTextColor = R.color.color_alert_congestion,
    )

    object ThemeRoutePlanningNCSCarparkFull : Theme(
        icon = R.drawable.exclamation_circle,
        backgroundColor = R.color.white,
        titleTextColor = R.color.color_alert_congestion,
        showArrowRight = true
    )

    object ThemeRoutePlanningNCSCarparkLimited : Theme(
        icon = R.drawable.exclamation_circle,
        backgroundColor = R.color.white,
        titleTextColor = R.color.themed_blazed_orange,
        showArrowRight = true
    )

    init {
        binding = AlertRoutePlanViewBinding.inflate(LayoutInflater.from(context), this)
        setOnClickListener {
            otherData?.let {data->
                this.onViewClickedListener?.invoke(data)
            }
        }
    }

    fun setOnClickCallback(callback: (Any) -> Unit) {
        this.onViewClickedListener = callback
    }

    fun setData(
        theme: Theme,
        alertTitle: String?,
        alertString: String,
        pNotificationCriterias: ArrayList<ValueConditionShowBroadcast>? = null,
        otherData: Any? = null
    ) {
        this.otherData = otherData
        notificationCriterias = pNotificationCriterias
        mTheme = theme
        val shape = GradientDrawable()
        shape.shape = GradientDrawable.RECTANGLE
        shape.cornerRadii = floatArrayOf(
            18.0f.dpToPx(),
            18.0f.dpToPx(),
            18.0f.dpToPx(),
            18.0f.dpToPx(),
            0f,
            0f,
            0f,
            0f
        )
        shape.setColor(ContextCompat.getColor(context, mTheme.backgroundColor))
        val textColor = ContextCompat.getColor(
            context,
            mTheme.titleTextColor
        )
        background = shape
        binding.tvAlertCongestion.setTextColor(
            textColor
        )
        binding.tvAlertCongestion.text = alertString
        if (alertTitle.isNullOrBlank()) {
            binding.tvAlertTitle.isVisible = false
        } else {
            binding.tvAlertTitle.isVisible = true
            binding.tvAlertTitle.setTextColor(
                textColor
            )
            binding.tvAlertTitle.text = alertTitle
        }

        if (iconUrl != null && iconUrl!!.trim().isNotEmpty()) {
            Glide.with(context)
                .load(iconUrl)
                .placeholder(R.drawable.ic_breeze_round)
                .error(R.drawable.ic_breeze_round)
                .into(binding.imgAlert)
        } else if (theme.icon != -1) {
            binding.imgAlert.setImageResource(theme.icon)
            binding.imgAlert.setColorFilter(textColor)
        } else {
            binding.imgAlert.setImageResource(R.drawable.ic_breeze_round)
        }

        binding.imgArrowRight.isVisible = theme.showArrowRight
        binding.imgArrowRight.setColorFilter(textColor)
    }


    fun hideAnimation() {
//        runnable?.apply {
//            handler?.removeCallbacks(this)
//        }
        this.visibility = View.GONE
        //ViewAnimator.hideViewSlideDown(this, VIEW_ANIMATION_DURATION)
    }

    fun showAnimation() {
//        runnable?.apply {
//            handler?.removeCallbacks(this)
//        }
        //this.visibility = View.VISIBLE

        showViewSlideUp(VIEW_ANIMATION_DURATION)
//        runnable = Runnable() {
//            hideAnimation()
//        }
//        runnable?.let {
//            handler.postDelayed(
//                it, 15 * 1000
//            )
//        }
    }
}
package com.breeze.customization.view

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.breeze.customization.R
import com.breeze.model.extensions.dpToPx
import kotlin.properties.Delegates


class VoucherViewGroup @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : ConstraintLayout(context, attrs, defStyle) {


    private var voucherState = VoucherState.HIDDEN

    var voucherClickHandler: VoucherClickHandler = object : VoucherClickHandler {
        override fun onSelect() {
            //no op
        }

        override fun onDismiss() {
            //no op
        }
    }

    var mPreSelectedText by Delegates.notNull<String>()
    var mSelectedText by Delegates.notNull<String>()
    var mUnSelectedText by Delegates.notNull<String>()
    var mSelectedLeftIcon by Delegates.notNull<Int>()

    var mPreSelectedLeftIcon by Delegates.notNull<Int>()
    var mUnSelectedLeftIcon by Delegates.notNull<Int>()
    var mUnSelectedRightIcon by Delegates.notNull<Int>()
    var mSelectedBackground by Delegates.notNull<Int>()

    init {
        initializeAttributes(attrs)
        initClickListener()
    }

    fun updateViewState(voucherState: VoucherState) {
        if (voucherState == VoucherState.HIDDEN) {
            this.visibility = View.GONE
        } else {
            this.removeAllViews()
            setBackgroundResource(mSelectedBackground)
            val leftIconView = generateLeftIconView(voucherState)
            val textView = generateTextView(leftIconView, voucherState)
            this.addView(leftIconView)
            this.addView(textView)
        }
        this.voucherState = voucherState
    }

    private fun initializeAttributes(attrs: AttributeSet?) {
        attrs?.let {
            val typedArray = context.obtainStyledAttributes(
                it,
                R.styleable.VoucherViewGroup, 0, 0
            )

            mSelectedText = typedArray.getString(
                R.styleable.VoucherViewGroup_selected_text
            ) ?: ""

            mUnSelectedText = typedArray.getString(
                R.styleable.VoucherViewGroup_unselected_text
            ) ?: ""

            mPreSelectedText = typedArray.getString(
                R.styleable.VoucherViewGroup_preselected_text
            ) ?: ""

            mSelectedLeftIcon = typedArray.getResourceId(
                R.styleable.VoucherViewGroup_selected_left_icon,
                -1
            )

            mUnSelectedLeftIcon = typedArray.getResourceId(
                R.styleable.VoucherViewGroup_unselected_left_icon,
                -1
            )
            mPreSelectedLeftIcon = typedArray.getResourceId(
                R.styleable.VoucherViewGroup_preselected_left_icon,
                -1
            )

            mUnSelectedRightIcon = typedArray.getResourceId(
                R.styleable.VoucherViewGroup_unselected_right_icon,
                -1
            )

            mSelectedBackground = typedArray.getResourceId(
                R.styleable.VoucherViewGroup_selected_bg,
                -1
            )
            typedArray.recycle()
        }
    }

    private fun initClickListener() {
        setOnClickListener {
            if (voucherState.ordinal < VoucherState.HIDDEN.ordinal) {
                voucherState = VoucherState.values()[voucherState.ordinal + 1]
                updateViewState(voucherState)
                handleVoucherClick()
            }
        }
    }

    private fun handleVoucherClick() {

        when (voucherState) {
            VoucherState.SELECTED -> {
                voucherClickHandler.onSelect()
            }

            VoucherState.HIDDEN -> {
                voucherClickHandler.onDismiss()
            }

            else -> {}
        }
    }


    private fun generateLeftIconView(state: VoucherState): View {
        val leftIcon = ImageView(this.context)
        leftIcon.id = View.generateViewId()
        if (state == VoucherState.UNSELECTED) {
            val layoutParamsLeftIcon = LayoutParams(
                21.dpToPx(),
                14.dpToPx()
            )
            layoutParamsLeftIcon.topToTop = LayoutParams.PARENT_ID
            layoutParamsLeftIcon.startToStart = LayoutParams.PARENT_ID
            layoutParamsLeftIcon.bottomToBottom = LayoutParams.PARENT_ID
            leftIcon.layoutParams = layoutParamsLeftIcon
            leftIcon.setImageResource(mUnSelectedLeftIcon)
        } else if (state == VoucherState.PRESELECTED) {
            val layoutParamsLeftIcon = LayoutParams(
                21.dpToPx(),
                14.dpToPx()
            )
            layoutParamsLeftIcon.topToTop = LayoutParams.PARENT_ID
            layoutParamsLeftIcon.startToStart = LayoutParams.PARENT_ID
            layoutParamsLeftIcon.bottomToBottom = LayoutParams.PARENT_ID
            leftIcon.layoutParams = layoutParamsLeftIcon
            leftIcon.setImageResource(mPreSelectedLeftIcon)
        } else {
            val layoutParamsLeftIcon = LayoutParams(
                14.dpToPx(),
                14.dpToPx()
            )
            layoutParamsLeftIcon.topToTop = LayoutParams.PARENT_ID
            layoutParamsLeftIcon.startToStart = LayoutParams.PARENT_ID
            layoutParamsLeftIcon.bottomToBottom = LayoutParams.PARENT_ID
            leftIcon.layoutParams = layoutParamsLeftIcon
            leftIcon.setImageResource(mSelectedLeftIcon)
        }
        return leftIcon
    }

    private fun generateTextView(leftIcon: View, state: VoucherState): View {
        val textView = TextView(this.context)
        val layoutParams = LayoutParams(
            LayoutParams.WRAP_CONTENT,
            19.dpToPx()
        )
        layoutParams.topToTop = LayoutParams.PARENT_ID
        layoutParams.startToEnd = leftIcon.id
        layoutParams.bottomToBottom = LayoutParams.PARENT_ID
        textView.layoutParams = layoutParams
        textView.setTextColor(context.getColor(R.color.white))
        if (state == VoucherState.UNSELECTED) {
            textView.text = mUnSelectedText
            textView.setCompoundDrawablesWithIntrinsicBounds(
                null,
                null,
                resources.getDrawable(mUnSelectedRightIcon, null),
                null
            )
            textView.setPadding(7.dpToPx(), 0, 0, 0)
            textView.compoundDrawablePadding = 5.dpToPx()
        } else if (state == VoucherState.PRESELECTED) {
            textView.text = mPreSelectedText
            textView.setPadding(8.dpToPx(), 0, 0, 0)
        } else {
            textView.text = mSelectedText
            textView.setPadding(8.dpToPx(), 0, 0, 0)
        }
        return textView
    }

    enum class VoucherState {
        UNSELECTED,
        SELECTED,
        HIDDEN,
        PRESELECTED
    }


    interface VoucherClickHandler {

        /**
         * Invoked when voucher is in selected state
         */
        fun onSelect()

        /**
         * Invoked when voucher is dismissed
         */
        fun onDismiss()
    }

}
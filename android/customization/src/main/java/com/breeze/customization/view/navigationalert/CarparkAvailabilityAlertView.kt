package com.breeze.customization.view.navigationalert

import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import com.breeze.customization.R
import com.breeze.customization.databinding.LayoutCarparkAvailabilityAlertViewBinding
import com.breeze.model.api.response.amenities.BaseAmenity
import com.breeze.model.enums.AmenityBand
import com.breeze.model.extensions.dpToPx

class CarparkAvailabilityAlertView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null
) : CardView(context, attrs) {
    private val binding: LayoutCarparkAvailabilityAlertViewBinding
    private var dataToNavigate: BaseAmenity? = null
    private var availablePercentImageBand: String? = null

    var navigateClickedListener: ((BaseAmenity, String?) -> Unit)? = null
    var closeAlertClickedListener: (() -> Unit)? = null

    init {
        binding =
            LayoutCarparkAvailabilityAlertViewBinding.inflate(LayoutInflater.from(context), this)
        radius = 16.dpToPx().toFloat()
        binding.layoutButton.setOnClickListener {
            isVisible = false
            dataToNavigate?.let { carpark ->
                navigateClickedListener?.invoke(carpark, availablePercentImageBand)
            }
        }
        binding.imgClose.setOnClickListener {
            closeAlertClickedListener?.invoke()
        }
    }

    fun setData(name: String, type: String, data: BaseAmenity?) {
        dataToNavigate = data
        availablePercentImageBand = type
        with(binding) {
            tvName.text = name
            val cardBackgroundColor: Int
            val iconRes: Int
            val message: String
            val buttonBgRes: Int?
            val navigateIconRes: Int?
            when (AmenityBand.getAmenityBand(type)) {
                AmenityBand.TYPE0 -> {
                    cardBackgroundColor =
                        ContextCompat.getColor(context, R.color.themed_passion_pink)
                    iconRes = R.drawable.ic_obu_alert_message_carpark_update_red
                    buttonBgRes = R.drawable.bg_common_button_radius_white
                    navigateIconRes = R.drawable.ic_carpark_availability_full_navigate
                    message = "Carpark Full"
                }

                AmenityBand.TYPE1 -> {
                    cardBackgroundColor =
                        ContextCompat.getColor(context, R.color.themed_blazed_orange)
                    iconRes = R.drawable.ic_obu_alert_message_carpark_update_yellow
                    buttonBgRes = R.drawable.bg_common_button_radius_white
                    navigateIconRes = R.drawable.ic_carpark_availability_filling_up_fast_navigate
                    message = "Filling Up Fast"
                }

                AmenityBand.TYPE2 -> {
                    cardBackgroundColor =
                        ContextCompat.getColor(context, R.color.carpark_price_green)
                    iconRes = R.drawable.ic_obu_alert_message_carpark_update_green
                    buttonBgRes = null
                    navigateIconRes = null
                    message = "Available"
                }
            }
            setCardBackgroundColor(cardBackgroundColor)
            imgIcon.setBackgroundResource(iconRes)
            tvMessage.text = message

            if (buttonBgRes == null || data == null) {
                flBottom.isVisible = false
            } else {
                flBottom.isVisible = true
                layoutButton.setBackgroundResource(buttonBgRes)
                navigateIconRes?.let { imgNavigate.setImageResource(it) }
                btnNavigateToAlternative.setTextColor(cardBackgroundColor)
                btnNavigateToAlternative.text = "Park at ${data.name}"
            }
        }

    }
}
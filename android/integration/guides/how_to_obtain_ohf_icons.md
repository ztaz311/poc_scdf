How to obtain One Hundred Feet (OHF) icons
=========

## Preface

We provide icons, that can be used to display specific points (PARKING, ENTRANCE, and UNIT) returned from One Hundred Feet's API. This guide describes how you can obtain these icons for each response.

## Usage

1) Create `MapboxSearchKit` object:
```kotlin
val mapboxSearchKit = MapboxSearchKit(
    mapboxAccessToken = "<your-mapbox-access-token>"
)
```
Please note, an `access token` from your Mapbox account is required to create a `MapboxSearchKit` object. If you don't have one or you're not sure whether you have appropriate token, please, take a look at our [docs page](https://docs.mapbox.com/api/overview/#access-tokens-and-token-scopes).

2) To get an OHF response, please, use function `MapboxSearchKit.runOhfSearch()`. Because this function is synchronous, you need to make sure you call it on another thread / in another context:

```kotlin
// Here any appropriate CoroutineScope can be used.
// ViewModel.viewModelScope was used for simplicity.
val result: BeansCoreData? = viewModelScope.launch {
    mapboxSearchKit.runOhfSearch(
        // An address for which you want to get Beans.AI response
        address = "2255 Showers Dr, Mountain View, CA", 
        // (Optional) Unit number of the provided address (line 2),
        // used by Beans.AI to improve search quality
        unit = "352"
    ).value
}
```

3) Each `BeansCoreData` object contains information about the target coordinate (`navigateTo: BeansLocation`) and about [parking / entrance / unit] points (`markers: List<BeansMarker>`). To obtain a Drawable resource ID for the given `marker: BeansMarker`, please, use `BeansIconProvider.provideIconForMarkerType(): Int?` function:

```kotlin
val firstMarker = result?.markers?.firstOrNull()
@DrawableRes
val firstMarkerDrawableRes: Int? = firstMarker?.let {
    BeansIconProvider.provideIconForMarkerType(it.assetType)
}
```

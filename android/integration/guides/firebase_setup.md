Setting up Firebase
=========

1) If you would like to use this example, find and navigate to `firebase_keys/` directory. Here you'll find the Firebase configuration file:
  `firebase_keys/release/google-service.json`;

1.1) If you would like to implement the nav-data-recorder into your project, please reach out to @moritzzzzz or your technical account manager with the Android package name of your project. We will create a Firebase configuration file for your project.

2) Navigate to your `:app` module root directory and do the following:
  - Place `google-service.json` file in `${moduleDir}/src/release/` and `${moduleDir}/src/debug/` folder;

3) Setup your `build.gradle` files. You can find how to do it [here](https://firebase.google.com/docs/android/setup#add-config-file).

4) Add Firebase SDK to your app. Follow [this](https://firebase.google.com/docs/android/setup#add-sdks) link and choose tab "Analytics not enabled":

![firebase-sdk-analytics-not-enabled](./images/firebase-sdk-analytics-not-enabled.png)

5) Make sure the following dependencies are also included:
```groovy
dependencies {
    implementation 'org.jetbrains.kotlinx:kotlinx-coroutines-play-services:1.1.1'
    implementation platform('com.google.firebase:firebase-bom:26.3.0')
    implementation 'com.google.firebase:firebase-storage-ktx'
}
```

Support multiple Firebase projects
=========

If you already setup a Firebase project in your app, please, check out [Firebase docs regarding configuring and using multiple projects](https://firebase.google.com/docs/projects/multiprojects#use_multiple_projects_in_your_application).

In short, you should do the following:

1) Reach out to @moritzzzzz or your technical account manager with the Android package name of your project. We will create a Firebase configuration file for your project. Create `FirebaseOptions` for release build:

```kotlin
val MAPBOX_INTEGRATION_FIREBASE_KEY = "YOUR_FIREBASE_KEY"
val MAPBOX_INTEGRATION_FIREBASE_OPTIONS = FirebaseOptions.Builder()
    .setProjectId("navigation-trips-trace")
    .setApplicationId("YOUR_APPLICATION_ID")
    .setApiKey("YOUR_API_KEY")
    .setStorageBucket("navigation-trips-trace.appspot.com")
    .build()
```


2) Configure additional Firebase application instance with previously created `FirebaseOptions`:

```kotlin
Firebase.initialize(this, MAPBOX_INTEGRATION_FIREBASE_OPTIONS, MAPBOX_INTEGRATION_FIREBASE_KEY)
```

3) Create `FirebaseApp` instance for previously configured Firebase project:

```kotlin
private val integrationFirebase = Firebase.app(MAPBOX_INTEGRATION_FIREBASE_KEY)
```

4) Create `FirebaseAuth` and `FirebaseStorage` instances:

```koltin
val integrationAuth = Firebase.auth(integrationFirebase)
val integrationStorage = Firebase.storage(integrationFirebase)    
```

Please note, to be able to use this helper extension functions, you must add the following dependencies to your project:

```gradle
dependencies {
    // ...
    implementation 'com.google.firebase:firebase-auth-ktx'
    implementation 'com.google.firebase:firebase-storage-ktx'    
}
```

5) Use created `FirebaseAuth` and `FirebaseStorage` instances for creation of `FirebaseUploader`:

```kotlin
val firebaseUploader: FirebaseUploader by lazy {
    FirebaseUploader(integrationStorage, integrationAuth)
}
```

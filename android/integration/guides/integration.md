Integration
===========

1) Import the module `integration`:
 - Go to File > New > Import Module...
 - Select the source directory of the Module you want to import and click Finish.
 - Open Project Structure and open Module Settings for your project.
 - Open the Dependencies tab.
 - Click the (+) icon and select Module Dependency. Select the module and click Ok.


2) Open your build.gradle file and check that the module is now listed under dependencies.(implementation project(path: ':integration')


3) Before moving forward, you will need to gather two piece of sensitive information from your Mapbox account: 
    
    - A public access token: From your [account's tokens page](https://account.mapbox.com/access-tokens/), you can either copy your default public token or click the Create a token button to create a new public token. This key we will use later for `MapboxSearchKit` object creation;
    - A secret access token with the `Downloads:Read` scope.

Export your secret access token as an environment variable `SDK_REGISTRY_TOKEN`. Alternatively, you can provide those credentials as project properties.

You can create and manage access tokens on your [Account page](https://account.mapbox.com/). 

4) Setup top-level `build.gradle` with the following lines:
```groovy
buildscript {
    ext.kotlin_version = "1.5.0" // Kotlin >= 1.4.20 required, because
                                  // we use `kotlin-parcelize` plugin
    
    repositories {
        google()
        jcenter()
        // ...
    }
    
    dependencies {
        classpath "org.jetbrains.kotlin:kotlin-gradle-plugin:$kotlin_version"
        // ...
    }
}

def sdkRegistryToken = project.properties['SDK_REGISTRY_TOKEN'] ?: System.getenv('SDK_REGISTRY_TOKEN')
if (sdkRegistryToken == null) {
    throw new Exception("SDK Registry token is not specified.")
}

allprojects {
    repositories {
        google()
        jcenter()
        maven {
            url = uri("https://api.mapbox.com/downloads/v2/releases/maven")
            credentials {
                username = "mapbox"
                password = sdkRegistryToken
            }
            authentication {
                basic(BasicAuthentication)
            }
        }
    }
}
```

5) Attach `integration` module to your `app` module or other module, that will use our integration logic. To complete this step, you should add the following line to the target module `build.gradle` file:
```groovy
dependencies {
    // ...
    implementation project(':integration')
}
```
This stackoverflow post is helpful as well: https://stackoverflow.com/questions/41764338/importing-module-in-android-studio/41764421

6) Make sure Firebase is setup. See [this guide](./firebase_setup.md) for instructions to setup Firebase.

7) If you're using Proguard/R8, please, add the following lines to your `app` module's `proguard-rules.pro ` file:
```
-keep class com.example.nav_data_recorder_main_20.integration.geocoding.** { *; }
-keep class ai.beans.sdk.model.** { *; } # If you use Beans.AI integration
```

Change you application level build.gradle to implement

```
 buildTypes {
        release {
            minifyEnabled true
            proguardFiles getDefaultProguardFile('proguard-android-optimize.txt'), 'proguard-rules.pro'
            signingConfig signingConfigs.debug
        }
    }
```

8) You're good to go!

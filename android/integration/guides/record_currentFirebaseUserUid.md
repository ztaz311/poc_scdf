# Relating trace files to feedback

To relate a trace file that was stored on Mapbox Firebase storage to a specific feedback, the date and time of the feedback is insufficient. To uniquely identify the file, 
the Firebase user unique ID is required.

It can be retrieved by

```
val auth: FirebaseAuth
val currentFirebaseUserUid = auth.currentUser?.uid
```

Setting up Navigation Metrics recorder
=========

## Preface

This metrics recorder (`HistoryRecorder`) will help Mapbox to get a better understanding of the quality of nav-data-recorder-ga drivers' routing (and geocoding experience). This guide will help setup it up.

## Setup

1) Import the module `integration`;

2) Create `FirebaseRecorder` object:
```kotlin
val firebaseUploader = FirebaseUploader(Firebase.storage, Firebase.auth)
// or
val firebaseUploader = FirebaseUploader(integrationStorage, integrationAuth)
```
To setup Firebase in your project, please, check out [this guide](firebase_setup.md).

Please note, `FirebaseRecorder` uses `FirebaseAuth` instance to ensure there is an active user session, so we could access our storage in more reliable and safer way. We call this method each time report is scheduled to be uploaded to Firebase. If you want to ensure there is an active session beforehand, please, execute the following code when there is an established internet connection (internet connection is required for successful completion of this function - we basically log in anonymous user here):

```kotlin
try {
    firebaseUploader.ensureAuthorized()
} catch (e: Exception) {
    // Warning: we don't handle exception in this case!
}
```

In the `nav_data_recorder_main_20` example this part lives in `ServiceLocator.kt`.

3) Create `HistoryRecorder` object. To record geocoding results from `RoutablePointProvider.findRoutablePoint()` and attribute them to the subsequent route generated, you can specify an additional, optional `searchAnalyticsData` constructor parameter. You can omit sending search analytics data by setting this parameter to `null`:
```kotlin
val routablePointResult: RoutablePointResult = routablePointProvider.findRoutablePoint(/* ... */)

// ...

val historyRecorder = HistoryRecorder(
    navigation = mapboxNavigation,
    firebaseUploader = firebaseUploader,
    searchAnalyticsData = routablePointResult.searchAnalyticsData
)
```

This will allow us to understand when an OHF or Mapbox point are used, provide analysis about when each were used and the quality of the routing results. This parameter is required because `RoutablePointProvider.findRoutablePoint()` is run before a route is constructed and needs to be passed back to the `HistoryRecorder` to attribute the returned point to the output route.

3.1) Register TripSessionStateobserver

This will register the `TripSessionStateObserver`. The history recorder will attach `onTripSessionStarted` and stop collection of traces `onTripSessionStopped`
```
 mapboxNavigation!!.registerTripSessionStateObserver(
                            historyRecorder
                        )
```

4) Add `historyRecorderOptions` and `deviceProfile` to the `navigationOptions` before creating the MapboxNavigation object:

```
val navigationOptions = NavigationOptions.Builder(this)
            .accessToken(getString(R.string.mapbox_access_token))
            .historyRecorderOptions( 
                HistoryRecorderOptions.Builder()
                    .enabled(true)
                    .build()
            )
            // Use JSON format instead of protobuf
            .deviceProfile(
                DeviceProfile.Builder()
                .customConfig("{\"features\":{\"usePbf\": false}}")
                .build())
            .build()
```

This will register the history recorder, and tell it to record history when a navigation session is active. (Both free drive, or active guidance)



5) Change the storage path `firebasePath` in `com.mapbox.navtriprecorder.integration.Historyrecorder.kt` from `Mapbox-release` to `<Your_Company>-release` or `<Your_Company>-debug`, so that we can distinguish your data from other data.
```
        historyRecorder.saveHistory { filePath ->
            if (filePath != null) {
                // save to a file or upload to the cloud
                val history = readfile(filePath)
                val firebasePath = "Mapbox-release"
                firebaseUploader.report(firebasePath, startedAt, prepareReport(history!!))
            }
        }
```

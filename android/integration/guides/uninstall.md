# Uninstallation of the navigation data recorder

The un-installation process of the navigation data recorder is the reverse process of the installation. Therefore please keep track of the changes you make when installing the navigation data recorder.

## Removing Firebase

Reverse the steps outlined in https://github.com/mapbox/nav-data-recorder-ga/blob/main/integration/guides/firebase_setup.md 
Please note that these steps depended on your project configuration. If you were using other instances of Firebase, the steps are different than if the only instace of Firebase is the one used for this nav data recorder.

## Removing the module 'integration'
Reverse the steps outlined in https://github.com/mapbox/nav-data-recorder-ga/blob/main/integration/guides/integration.md
If you are still using Mapbox in your project after removing the nav data recorder, please do not reverse step 4.

## Removing nav trip recorder specific code
Reverse the steps outlined in https://github.com/mapbox/nav-data-recorder-ga/blob/main/integration/guides/nav_metrics_recorder_setup.md

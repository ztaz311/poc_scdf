How to get routable point for a given address
========

1) Attach our library source code to your project;

2) Create a `MapboxSearchKit` object:
```kotlin
val mapboxSearchKit = MapboxSearchKit(
    mapboxAccessToken = "<your-mapbox-access-token>"
)
```
Please note, an `access token` from your Mapbox account is required to create a `MapboxSearchKit` object. If you don't have one or you're not sure whether you have appropriate token, please, take a look at our [docs page](https://docs.mapbox.com/api/overview/#access-tokens-and-token-scopes) or reach out to your Mapbox technical account manager or Mapbox account manager. 

3) Create a `RoutablePointProvider` object:
```kotlin
val routablePointProvider = RoutablePointProvider(
    searchKit = mapboxSearchKit
)
```

4) To get a routable point result for the given address, please, use `RoutablePointProvider.findRoutablePoint()` function:
```kotlin
// Here any appropriate CoroutineScope can be used.
// ViewModel.viewModelScope was used for simplicity.
viewModelScope.launch {
    val routablePointResult: RoutablePointResult = 
        routablePointProvider.findRoutablePoint(
            // An address for which you want to find routable point
            address = "49 Showers Dr, Mountain View, CA 94040, United States",
            // Google rooftop point for the provided address
            rooftopPoint = Point(
                longitude = -122.1036894538177,
                latitude = 37.40464537319192
            ),
            // (Optional) Unit number of the provided address (line 2),
            // used by Beans.AI to improve search quality
            unit = null
        )
}
```

5) To access routable point result, use `RoutablePointResult.routablePoint: Point` property. You can use this point as the destination point for an upcoming route:
```kotlin
val mapboxNavigation: MapboxNavigation = // ...
val routesRequestCallback: RoutesRequestCallback? = // ...
val originPoint: Point = // ...

mapboxNavigation.requestRoutes(
    RouteOptions.builder().applyDefaultParams()
        // ...
        .coordinates(
            origin = originPoint,
            destination = routablePointResult.routablePoint.toMapboxPoint()
        )
        .build(),
    routesRequestCallback
)
```

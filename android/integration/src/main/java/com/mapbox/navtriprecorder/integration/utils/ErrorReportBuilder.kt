package com.mapbox.navtriprecorder.integration.utils


internal fun Throwable.prepareErrorReport(): String {
    val messages = mutableListOf<String?>()
    var throwable: Throwable? = this
    while (throwable != null) {
        messages.add(throwable.message)
        throwable = throwable.cause
    }
    return messages.joinToString(separator = "\nCaused by: ")
}

package com.mapbox.navtriprecorder.integration.geocoding;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;
import com.mapbox.geojson.BoundingBox;
import com.mapbox.geojson.Feature;
import com.mapbox.geojson.GeoJson;
import com.mapbox.geojson.Geometry;
import com.mapbox.geojson.GeometryAdapterFactory;
import com.mapbox.geojson.Point;
import com.mapbox.geojson.gson.BoundingBoxTypeAdapter;

import java.util.List;

/**
 * The Features key in the geocoding API response contains the majority of information you'll want
 * to use. It extends the {@link GeoJson} object in GeoJSON and adds several additional attribute
 * which further describe the geocoding result.
 * <p>
 * A Geocoding id is a String in the form {@code {type}.{id}} where {@code {type}} is the lowest
 * hierarchy feature in the  place_type field. The  {id} suffix of the feature id is unstable and
 * may change within versions.
 * <p>
 * Note: this class doesn't actually extend Feature due to the inherit rule in AutoValue (see link
 * below).
 *
 * @see <a href="https://github.com/mapbox/carmen/blob/master/carmen-geojson.md">Carmen Geojson information</a>
 * @see <a href="https://www.mapbox.com/api-documentation/search/#geocoding">Mapbox geocoder documentation</a>
 * @see <a href='geojson.org/geojson-spec.html#feature-objects'>Official GeoJson Feature Specifications</a>
 * @see <a href="https://github.com/google/auto/blob/master/value/userguide/howto.md#inherit">AutoValue inherit rule</a>
 */
public class CarmenFeature {

    /**
     * This describes the TYPE of GeoJson geometry this object is, thus this will always return
     * {@link Feature}.
     *
     * @return a String which describes the TYPE of geometry, for this object it will always return
     * {@code Feature}
     */
    @NonNull
    @SerializedName("type")
    public String type;
    /**
     * A {@link CarmenFeature} might have a member named {@code bbox} to include information on the
     * coordinate range for it's {@link Geometry}. The value of the bbox member MUST be a list of
     * size 2*n where n is the number of dimensions represented in the contained feature geometries,
     * with all axes of the most southwesterly point followed by all axes of the more northeasterly
     * point. The axes order of a bbox follows the axes order of geometries.
     *
     * @return a {@link BoundingBox} object containing the information
     */
    @Nullable
    public BoundingBox bbox;
    /**
     * A feature may have a commonly used identifier which is either a unique String or number.
     *
     * @return a String containing this features unique identification or null if one wasn't given
     * during creation.
     */
    @Nullable
    public String id;
    /**
     * The geometry which makes up this feature. A Geometry object represents points, curves, and
     * surfaces in coordinate space. One of the seven geometries provided inside this library can be
     * passed in through one of the static factory methods.
     *
     * @return a single defined {@link Geometry} which makes this feature spatially aware
     */
    @Nullable
    public Geometry geometry;
    /**
     * This contains the JSON object which holds the feature properties. The value of the properties
     * member is a {@link JsonObject} and might be empty if no properties are provided.
     *
     * @return a {@link JsonObject} which holds this features current properties
     */
    @Nullable
    public JsonObject properties;
    /**
     * A string representing the feature in the requested language, if specified.
     *
     * @return text representing the feature (e.g. "Austin")
     */
    @Nullable
    public String text;

    //
    // CarmenFeature specific attributes
    //
    /**
     * A string representing the feature in the requested language, if specified, and its full result
     * hierarchy.
     *
     * @return human-readable text representing the full result hierarchy (e.g. "Austin, Texas,
     * United States")
     */
    @Nullable
    @SerializedName("place_name")
    public String placeName;
    /**
     * A list of feature types describing the feature. Options are one of the following types defined
     * in the GeocodingTypeCriteria. Most features have only one type, but if the feature has
     * multiple types, (for example, Vatican City is a country, region, and place), all applicable
     * types will be provided in the list.
     *
     * @return a list containing the place type
     */
    @Nullable
    @SerializedName("place_type")
    public List<String> placeType;
    /**
     * A string of the house number for the returned {@code address} feature. Note that unlike the
     * address property for {@code poi} features, this property is outside the  properties object.
     *
     * @return while the string content isn't guaranteed, and might return null, in many cases, this
     * will be the house number
     */
    @Nullable
    public String address;
    /**
     * A list representing the hierarchy of encompassing parent features. This is where you can find
     * telephone, address, and other information pertaining to this feature.
     *
     * @return a list made up of {@link CarmenContext} which might contain additional information
     * about this specific feature
     */
    @Nullable
    public List<CarmenContext> context;
    /**
     * A numerical score from 0 (least relevant) to 0.99 (most relevant) measuring how well each
     * returned feature matches the query. You can use this property to remove results which don't
     * fully match the query.
     *
     * @return the relevant score between 0 and 1
     */
    @Nullable
    public Double relevance;
    /**
     * A string analogous to the {@link #text} field that more closely matches the query than
     * results in the specified language. For example, querying &quot;K&#246;ln, Germany&quot; with
     * language set to English might return a feature with the {@link #text} &quot;Cologne&quot;
     * and this would be &quot;K&#246;ln&quot;.
     *
     * @return a string containing the matching text
     */
    @Nullable
    @SerializedName("matching_text")
    public String matchingText;
    /**
     * A string analogous to the {@link #placeName} field that more closely matches the query than
     * results in the specified language. For example, querying "K&#246;ln, Germany" with language
     * set to English might return a feature with the {@link #placeName} "Cologne, Germany"
     * and this would return "K&#246;ln, North Rhine-Westphalia, Germany".
     *
     * @return a string containing the matching place name
     */
    @Nullable
    @SerializedName("matching_place_name")
    public String matchingPlaceName;
    /**
     * A string of the IETF language tag of the query's primary language.
     *
     * @return string containing the query's primary language
     * @see <a href="https://en.wikipedia.org/wiki/IETF_language_tag">IETF language tag Wikipedia page</a>
     */
    @Nullable
    public String language;
    /**
     * A list of {@link RoutablePoints}, that represents entries near the associated building.
     *
     * @return a list containing routable points
     */
    @Nullable
    @SerializedName("routable_points")
    public RoutablePoints routablePoints;
    // No public access thus, we lessen enforcement on mutability here.
    @Nullable
    @SerializedName("center")
    @SuppressWarnings("mutable")
    private double[] rawCenter;

    @NonNull
    public static CarmenFeature fromJson(@NonNull String json) {
        Gson gson = new GsonBuilder()
                .registerTypeAdapterFactory(GeometryAdapterFactory.create())
                .registerTypeAdapter(BoundingBox.class, new BoundingBoxTypeAdapter())
                .create();
        CarmenFeature feature = gson.fromJson(json, CarmenFeature.class);
        // Even thought properties are Nullable,
        // Feature object will be created with properties set to an empty object,
        if (feature.properties == null) {
            feature.properties = new JsonObject();
        }
        return feature;
    }

    /**
     * A {@link Point} object which represents the center point inside the {@link #bbox} if one is
     * provided.
     *
     * @return a GeoJson {@link Point} which defines the center location of this feature
     */
    @Nullable
    public Point center() {
        // Store locally since rawCenter() is mutable
        double[] center = rawCenter;
        if (center != null && center.length == 2) {
            return Point.fromLngLat(center[0], center[1]);
        }
        return null;
    }
}

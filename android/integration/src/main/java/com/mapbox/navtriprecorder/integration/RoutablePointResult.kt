package com.mapbox.navtriprecorder.integration

import android.os.Parcelable
import com.mapbox.navtriprecorder.integration.models.Point
import kotlinx.parcelize.Parcelize

@Parcelize
data class RoutablePointResult(
    val routablePoint: Point,
    val searchAnalyticsData: SearchAnalyticsData
) : Parcelable

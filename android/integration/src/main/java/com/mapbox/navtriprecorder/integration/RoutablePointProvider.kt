package com.mapbox.navtriprecorder.integration

import ai.beans.sdk.model.BeansCoreData
import ai.beans.sdk.model.BeansMarker
import com.mapbox.navtriprecorder.integration.geocoding.GeocodingResponse
import com.mapbox.navtriprecorder.integration.models.Point
import com.mapbox.navtriprecorder.integration.models.ResponseInfo
import com.mapbox.navtriprecorder.integration.models.Result
import com.mapbox.navtriprecorder.integration.utils.prepareErrorReport
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope

class RoutablePointProvider(
    val searchKit: MapboxSearchKit,
    private val requestDispatcher: CoroutineDispatcher = Dispatchers.IO
) {

    suspend fun findRoutablePoint(
        address: String, rooftopPoint: Point, unit: String? = null
    ): RoutablePointResult = coroutineScope {
        val ohfRequest = async(requestDispatcher) {
            searchKit.runOhfSearchInternal(address, unit)
        }
        val mapboxRequest = async(requestDispatcher) {
            searchKit.runMapboxSearchInternal(address, rooftopPoint)
        }

        val ohfResponse = ohfRequest.await()
        val mapboxResponse = mapboxRequest.await()

        val ohfRoutablePoint = ohfResponse.value?.first?.getRoutablePoint()
        val mapboxRoutablePoint = mapboxResponse.value?.first?.getRoutablePoint()
        val (routablePoint, provider) = ohfRoutablePoint?.let { it to SearchAnalyticsData.Provider.OHF }
            ?: mapboxRoutablePoint?.let { it to SearchAnalyticsData.Provider.MAPBOX }
            ?: rooftopPoint to SearchAnalyticsData.Provider.GOOGLE

        val searchAnalyticsData = prepareAnalyticsData(
            address,
            rooftopPoint,
            unit,
            ohfResponse,
            mapboxResponse,
            routablePoint,
            provider,
        )

        return@coroutineScope RoutablePointResult(
            routablePoint = routablePoint,
            searchAnalyticsData = searchAnalyticsData
        )
    }

    private fun prepareAnalyticsData(
        address: String, rooftopPoint: Point, unit: String?,
        ohfResponse: Result<Pair<BeansCoreData?, ResponseInfo>>,
        mapboxResponse: Result<Pair<GeocodingResponse?, ResponseInfo>>,
        routablePoint: Point,
        provider: SearchAnalyticsData.Provider,
    ): SearchAnalyticsData {

        fun Result<Pair<*, ResponseInfo>>.extractErrorStringForAnalytics(): String? {
            return if (value?.second?.isSuccessful == false) {
                value?.second?.responseString
            } else {
                asError()?.throwable?.prepareErrorReport()
            }
        }

        return SearchAnalyticsData(
            ohfRoutablePoint = ohfResponse.value?.first?.getRoutablePoint(),
            mapboxRoutablePoint = mapboxResponse.value?.first?.getRoutablePoint(),
            providedGooglePoint = rooftopPoint,
            routablePoint = routablePoint,
            searchProviderUsed = provider,
            searchAddress = address,
            searchAddressUnit = unit,
            ohfSearchRequest = searchKit.getBeansApiRequestUrl(address, unit).toString(),
            ohfSearchResponseCode = ohfResponse.value?.second?.httpResponseCode?.toString(),
            ohfSearchResponse = ohfResponse.value?.second?.responseString,
            ohfSearchRequestError = ohfResponse.extractErrorStringForAnalytics(),
            mapboxSearchRequest = searchKit.getMapboxSearchRequestUrl(
                address,
                rooftopPoint,
                encryptedToken = true
            ).toString(),
            mapboxSearchResponseCode = mapboxResponse.value?.second?.httpResponseCode?.toString(),
            mapboxSearchResponse = mapboxResponse.value?.second?.responseString,
            mapboxSearchRequestError = mapboxResponse.extractErrorStringForAnalytics(),
        )
    }

    private fun BeansCoreData.getRoutablePoint(): Point? {
        val beanMarker = markers.firstOrNull { marker ->
            marker.assetType == BeansMarker.AssetType.PARKING
                    && marker.location != null
                    && marker.location.latitude != null && marker.location.longitude != null
        }
        return if (beanMarker != null) {
            Point(
                latitude = beanMarker.location.latitude,
                longitude = beanMarker.location.longitude,
            )
        } else {
            null
        }
    }

    private fun GeocodingResponse.getRoutablePoint(): Point? {
        val mapboxPoint = features?.firstOrNull()?.routablePoints?.points?.firstOrNull()
        return if (mapboxPoint != null) {
            Point(
                latitude = mapboxPoint.latitude(),
                longitude = mapboxPoint.longitude(),
            )
        } else {
            null
        }
    }
}

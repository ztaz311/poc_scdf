package com.mapbox.navtriprecorder.integration.models

internal data class ResponseInfo(
    val requestUrl: String,
    val isSuccessful: Boolean,
    val httpResponseCode: Int,
    val responseString: String
)

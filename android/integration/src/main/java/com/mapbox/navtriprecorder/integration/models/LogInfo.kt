package com.mapbox.navtriprecorder.integration.models

import com.google.gson.annotations.SerializedName

internal data class LogInfo(
    @SerializedName("telem_id") val telemetryId: String,
    @SerializedName("query_address") val queryAddress: String?,
    @SerializedName("query_unit_number") val queryUnit: String?,
    @SerializedName("search_result") val searchResultJson: String?,
)

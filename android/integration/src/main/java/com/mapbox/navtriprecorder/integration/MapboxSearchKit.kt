package com.mapbox.navtriprecorder.integration

import ai.beans.sdk.model.BeansCoreData
import ai.beans.sdk.model.api.BeansAPIResponse
import android.util.Base64
import androidx.annotation.VisibleForTesting
import com.google.gson.GsonBuilder
import com.mapbox.geojson.BoundingBox
import com.mapbox.geojson.GeometryAdapterFactory
import com.mapbox.geojson.gson.BoundingBoxTypeAdapter
import com.mapbox.navtriprecorder.integration.geocoding.GeocodingResponse
import com.mapbox.navtriprecorder.integration.models.Point
import com.mapbox.navtriprecorder.integration.models.ResponseInfo
import com.mapbox.navtriprecorder.integration.models.Result
import com.mapbox.navtriprecorder.integration.models.wrapAsResult
import com.mapbox.navtriprecorder.integration.network.NetworkClient
import com.mapbox.navtriprecorder.integration.network.RealNetworkClient
import kotlinx.coroutines.*
import java.net.URL
import java.net.URLEncoder

class MapboxSearchKit(
    private val mapboxAccessToken: String,
    private val requestDispatcher: CoroutineDispatcher = Dispatchers.IO,
) {
    private val gson = GsonBuilder()
        .registerTypeAdapterFactory(GeometryAdapterFactory.create())
        .registerTypeAdapter(BoundingBox::class.java, BoundingBoxTypeAdapter())
        .create()

    @VisibleForTesting
    internal var networkClient: NetworkClient = RealNetworkClient()

    suspend fun runSearch(address: String, proximityPoint: Point, unit: String? = null): String {
        return coroutineScope {
            val mapboxGeoResponse = async(requestDispatcher) {
                wrapAsResult {
                    getMapboxSearchResponse(address, proximityPoint).responseString
                }
            }
            val ohfGeoResponse = async(requestDispatcher) {
                runOhfSearch(address, unit).map { it?.let { gson.toJson(it) } }
            }

            "{\"mapbox_response\":" + mapboxGeoResponse.await().requireValue() +
                    ",\"ohf_response\":" + ohfGeoResponse.await().requireValue() + "}"
        }
    }

    /**
     * Runs Mapbox Geocoding V5 search request.
     */
    suspend fun runMapboxSearch(
        address: String,
        proximityPoint: Point
    ): Result<GeocodingResponse?> {
        return withContext(requestDispatcher) {
            runMapboxSearchInternal(address, proximityPoint).toDataResult()
        }
    }

    internal fun runMapboxSearchInternal(
        address: String,
        proximityPoint: Point
    ): Result<Pair<GeocodingResponse?, ResponseInfo>> {
        return wrapAsResult {
            val responseInfo = getMapboxSearchResponse(address, proximityPoint)
            val geocodingResponse = if (responseInfo.isSuccessful) {
                gson.fromJson(responseInfo.responseString, GeocodingResponse::class.java)
            } else {
                null
            }
            geocodingResponse to responseInfo
        }
    }

    private fun getMapboxSearchResponse(
        address: String,
        proximityPoint: Point
    ): ResponseInfo {
        val url = getMapboxSearchRequestUrl(address, proximityPoint)

        return networkClient.createResponseInfo(url) {
            setRequestProperty("Accept", "application/json")
        }
    }

    internal fun getMapboxSearchRequestUrl(
        address: String, proximityPoint: Point,
        encryptedToken: Boolean = false
    ): URL {
        return URL(
            "https://api.mapbox.com/geocoding/v5/mapbox.places/"
                    + URLEncoder.encode(address, "UTF-8")
                    + ".json?access_token=" + (if (!encryptedToken) mapboxAccessToken else "*****")
                    + "&autocomplete=false"
                    + "&types=address"
                    + "&country=us"
                    + "&proximity=" + proximityPoint.longitude + "," + proximityPoint.latitude  /// proximity=-122.41226898310344%2C37.78063907095512
                    + "&limit=1"
                    + "&routing=true"
        )
    }

    /**
     * Runs Beans.ai (OHF) search request.
     */
    suspend fun runOhfSearch(address: String, unit: String?): Result<BeansCoreData?> {
        return withContext(requestDispatcher) {
            runOhfSearchInternal(address, unit).toDataResult()
        }
    }

    internal fun runOhfSearchInternal(
        address: String,
        unit: String?
    ): Result<Pair<BeansCoreData?, ResponseInfo>> {
        return wrapAsResult {
            val responseInfo = getBeansApiResponse(address, unit)
            val coreData = if (responseInfo.isSuccessful) {
                val apiResponse =
                    gson.fromJson(responseInfo.responseString, BeansAPIResponse::class.java)
                apiResponse.toCoreData()
            } else {
                null
            }
            coreData to responseInfo
        }
    }

    private fun getBeansApiResponse(address: String, unit: String?): ResponseInfo {
        val url = getBeansApiRequestUrl(address, unit)

        return networkClient.createResponseInfo(url) {
            setRequestProperty("Authorization", BEANS_AI_AUTH_TOKEN)
            setRequestProperty("Accept", "application/json")
        }
    }

    internal fun getBeansApiRequestUrl(address: String, unit: String?): URL {
        return URL(
            "https://api2.beans.ai/enterprise/v2/search/beans?"
                    + "address=" + URLEncoder.encode(address, "UTF-8")
                    + if (unit != null) {
                "&unit=" + URLEncoder.encode(unit, "UTF-8")
            } else ""
        )
    }

    private fun <T> Result<Pair<T, ResponseInfo>>.toDataResult(): Result<T> {
        return when (this) {
            is Result.Success -> {
                if (data.second.isSuccessful) {
                    Result.Success(data.first)
                } else {
                    val message = "HTTP error. " +
                            "Code: ${data.second.httpResponseCode}, " +
                            "Message: ${data.second.responseString}"
                    Result.Error(Exception(message))
                }
            }
            is Result.Error -> Result.Error(throwable)
        }
    }

    private companion object {
        const val BEANS_AI_AUTH_KEY_SECRET: String = "<YOUR_BEANS_AI_TOKEN>"
        val BEANS_AI_AUTH_TOKEN: String
            get() = "Basic " + Base64.encodeToString(
                BEANS_AI_AUTH_KEY_SECRET.toByteArray(),
                Base64.NO_WRAP
            )
    }
}

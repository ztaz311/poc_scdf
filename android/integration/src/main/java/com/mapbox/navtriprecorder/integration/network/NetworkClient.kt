package com.mapbox.navtriprecorder.integration.network

import com.mapbox.navtriprecorder.integration.models.ResponseInfo
import java.io.InputStream
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL
import java.nio.charset.StandardCharsets

internal interface NetworkClient {
    fun createResponseInfo(url: URL, setup: HttpURLConnection.() -> Unit = {}): ResponseInfo
}

internal class RealNetworkClient : NetworkClient {

    override fun createResponseInfo(url: URL, setup: HttpURLConnection.() -> Unit): ResponseInfo {
        val connection = url.openConnection() as HttpURLConnection
        connection.setup()

        val httpResponseCode = connection.responseCode
        val isSuccessful = isSuccessfulHttpStatusCode(httpResponseCode)
        val stream = if (isSuccessful) {
            connection.inputStream
        } else {
            connection.errorStream
        }

        val apiResponseStr = stream.readToString()
        return ResponseInfo(
            requestUrl = url.toString(),
            isSuccessful = isSuccessful,
            httpResponseCode = httpResponseCode,
            responseString = apiResponseStr
        )
    }

    private companion object {
        fun InputStream.readToString(): String {
            return InputStreamReader(this, StandardCharsets.UTF_8)
                .buffered()
                .use { reader -> reader.readText() }
        }

        fun isSuccessfulHttpStatusCode(code: Int) = code / 100 == 2
    }
}

package com.mapbox.navtriprecorder.integration.geocoding;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mapbox.geojson.BoundingBox;
import com.mapbox.geojson.FeatureCollection;
import com.mapbox.geojson.GeometryAdapterFactory;
import com.mapbox.geojson.gson.BoundingBoxTypeAdapter;

import java.util.List;

/**
 * This is the initial object which gets returned when the geocoding request receives a result.
 * Since each result is a {@link CarmenFeature}, the response simply returns a list of those
 * features.
 */
public class GeocodingResponse {

    private static final String TYPE = "FeatureCollection";
    /**
     * A geocoding response will always be an extension of a {@link FeatureCollection} containing
     * additional information.
     *
     * @return the type of GeoJSON this is
     */
    @NonNull
    public String type;
    /**
     * A list of space and punctuation-separated strings from the original query.
     *
     * @return a list containing the original query
     */
    @NonNull
    public List<String> query;
    /**
     * A list of the CarmenFeatures which contain the results and are ordered from most relevant to
     * least.
     *
     * @return a list of {@link CarmenFeature}s which each represent an individual result from the
     * query
     */
    @Nullable
    public List<CarmenFeature> features;
    /**
     * A string attributing the results of the Mapbox Geocoding API to Mapbox and links to Mapbox's
     * terms of service and data sources.
     *
     * @return information about Mapbox's terms of service and the data sources
     */
    @NonNull
    public String attribution;

    @NonNull
    public static GeocodingResponse fromJson(@NonNull String json) {
        Gson gson = new GsonBuilder()
                .registerTypeAdapterFactory(GeometryAdapterFactory.create())
                .registerTypeAdapter(BoundingBox.class, new BoundingBoxTypeAdapter())
                .create();
        return gson.fromJson(json, GeocodingResponse.class);
    }
}

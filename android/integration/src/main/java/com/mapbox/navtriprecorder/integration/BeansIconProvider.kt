package com.mapbox.navtriprecorder.integration

import ai.beans.sdk.model.BeansMarker
import androidx.annotation.DrawableRes

object BeansIconProvider {

    @DrawableRes
    fun provideIconForMarkerType(assetType: BeansMarker.AssetType): Int? {
        return when (assetType) {
            BeansMarker.AssetType.PARKING -> R.drawable.ic_parking
            BeansMarker.AssetType.ENTRANCE -> R.drawable.ic_entrance
            BeansMarker.AssetType.UNIT -> R.drawable.ic_appartment_unit
            // we don't provide assets for LOCKER and LOBBY types
            BeansMarker.AssetType.LOCKER,
            BeansMarker.AssetType.LOBBY -> null
        }
    }
}

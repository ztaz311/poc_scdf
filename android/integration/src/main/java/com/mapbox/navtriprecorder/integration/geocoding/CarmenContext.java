package com.mapbox.navtriprecorder.integration.geocoding;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;

/**
 * Array representing a hierarchy of parents. Each parent includes id, text keys along with
 * (if avaliable) a wikidata, short_code, and Maki key.
 *
 * @see <a href="https://github.com/mapbox/carmen/blob/master/carmen-geojson.md">Carmen Geojson information</a>
 * @see <a href="https://www.mapbox.com/api-documentation/search/#geocoding">Mapbox geocoder documentation</a>
 */
public class CarmenContext {

    /**
     * ID of the feature of the form {index}.{id} where index is the id/handle of the data-source
     * that contributed the result.
     *
     * @return string containing the ID
     */
    @Nullable
    public String id;
    /**
     * A string representing the feature.
     *
     * @return text representing the feature (e.g. "Austin")
     */
    @Nullable
    public String text;
    /**
     * The ISO 3166-1 country and ISO 3166-2 region code for the returned feature.
     *
     * @return a String containing the country or region code
     */
    @Nullable
    @SerializedName("short_code")
    public String shortCode;
    /**
     * The WikiData identifier for a country, region or place.
     *
     * @return a unique identifier WikiData uses to query and gather more information about this
     * specific feature
     */
    @Nullable
    public String wikidata;
    /**
     * provides the categories that define this features POI if applicable.
     *
     * @return comma-separated list of categories applicable to a poi
     */
    @Nullable
    public String category;
    /**
     * Suggested icon mapping from the most current version of the Maki project for a poi feature,
     * based on its category. Note that this doesn't actually return the image but rather the
     * identifier which can be used to download the correct image manually.
     *
     * @return string containing the recommended Maki icon
     */
    @Nullable
    public String maki;

    /**
     * Create a CarmenContext object from JSON.
     *
     * @param json string of JSON making up a carmen context
     * @return this class using the defined information in the provided JSON string
     */
    @SuppressWarnings("unused")
    public static CarmenContext fromJson(@NonNull String json) {
        Gson gson = new GsonBuilder()
                .create();
        return gson.fromJson(json, CarmenContext.class);
    }
}

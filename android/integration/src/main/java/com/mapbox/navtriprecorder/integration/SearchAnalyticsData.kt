package com.mapbox.navtriprecorder.integration

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.mapbox.navtriprecorder.integration.models.Point
import kotlinx.parcelize.Parcelize

@Parcelize
data class SearchAnalyticsData(
    @SerializedName("ohf_routable_point")
    private val ohfRoutablePoint: Point?,
    @SerializedName("mapbox_routable_point")
    private val mapboxRoutablePoint: Point?,
    @SerializedName("provided_google_point")
    private val providedGooglePoint: Point,
    @SerializedName("routable_point")
    private val routablePoint: Point,
    @SerializedName("search_provider_used")
    private val searchProviderUsed: Provider,
    @SerializedName("search_address")
    private val searchAddress: String,
    @SerializedName("search_address_unit")
    private val searchAddressUnit: String?,
    @SerializedName("ohf_search_request")
    private val ohfSearchRequest: String,
    @SerializedName("ohf_search_response_code")
    private val ohfSearchResponseCode: String?,
    @SerializedName("ohf_search_response")
    private val ohfSearchResponse: String?,
    @SerializedName("ohf_search_request_error")
    private val ohfSearchRequestError: String?,
    @SerializedName("mapbox_search_request")
    private val mapboxSearchRequest: String,
    @SerializedName("mapbox_search_response_code")
    private val mapboxSearchResponseCode: String?,
    @SerializedName("mapbox_search_response")
    private val mapboxSearchResponse: String?,
    @SerializedName("mapbox_search_request_error")
    private val mapboxSearchRequestError: String?,
) : Parcelable {

    enum class Provider {
        MAPBOX,
        OHF,
        GOOGLE,
    }
}

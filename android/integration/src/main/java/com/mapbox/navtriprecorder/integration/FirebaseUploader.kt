package com.mapbox.navtriprecorder.integration

import android.util.Log
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.storage.FirebaseStorage
//import com.mapbox.android.telemetry.TelemetryUtils
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.tasks.await
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.nio.charset.StandardCharsets
import java.text.SimpleDateFormat
import java.util.*
import java.util.zip.GZIPOutputStream

class FirebaseUploader(
    storage: FirebaseStorage,
    private val auth: FirebaseAuth,
    private val uploadDispatchers: CoroutineDispatcher = Dispatchers.Default
) {

    private val storageRef = storage.reference

    /**
     * Gzips and Uploads report to firebase
     * @throws IOException if upload fails
     */
    internal suspend fun report(path: String, timestamp: Date, data: String) {
//        GlobalScope.launch(uploadDispatchers) {
        writeFirebase(path, createFilename(timestamp), data)
//        }
    }

    /**
     * Gzips and Uploads report to firebase
     * @throws IOException if upload fails
     */
    private suspend fun writeFirebase(path: String, filename: String, history: String) {
        try {
            ensureAuthorized()
            val currentFirebaseUserUid = auth.currentUser?.uid ?: "empty"
            val telemetryId = "TelemetryUtils.retrieveVendorId()"
            val spaceRef = storageRef.child(
                path + "///" + currentFirebaseUserUid + "///" + telemetryId + "///" + filename + "json.gz"
            )
            spaceRef.putBytes(gzip(history)).await()
            Log.d("testing-log", "Firebase Uploader: " + filename)
        } catch (e: Exception) {
            if (BuildConfig.DEBUG) {
                Log.e("FirebaseUploader", "Failed to upload analytics", e)
            }
            //This should be caught by the caller
            if (e is IOException) throw e
        }
    }

    suspend fun ensureAuthorized() {
        val currentUser = auth.currentUser
        if (currentUser == null) {
            auth.signInAnonymously().await()
        }
    }


    private fun gzip(content: String): ByteArray {
        val bos = ByteArrayOutputStream()
        GZIPOutputStream(bos).bufferedWriter(StandardCharsets.UTF_8).use { it.write(content) }
        return bos.toByteArray()
    }

    private fun createFilename(startedAt: Date): String =
        "${UTC_FORMATTER.format(startedAt)}_${UTC_FORMATTER.format(Date())}_"

    private companion object {
        val UTC_FORMATTER = SimpleDateFormat("yyyy-MM-dd_HH:mm:ss", Locale.US)
            .also { it.timeZone = TimeZone.getTimeZone("UTC") }
    }
}

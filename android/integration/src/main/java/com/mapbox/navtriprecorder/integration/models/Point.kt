package com.mapbox.navtriprecorder.integration.models

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Point(
    val latitude: Double,
    val longitude: Double
) : Parcelable

public fun com.mapbox.geojson.Point.toSearchPoint(): Point {
    return Point(
        latitude = latitude(),
        longitude = longitude()
    )
}

public fun Point.toMapboxPoint(): com.mapbox.geojson.Point {
    return com.mapbox.geojson.Point.fromLngLat(longitude, latitude)
}

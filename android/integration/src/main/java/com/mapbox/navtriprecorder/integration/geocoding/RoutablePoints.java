package com.mapbox.navtriprecorder.integration.geocoding;

import com.google.gson.annotations.SerializedName;
import com.mapbox.geojson.Point;

import java.util.List;

/**
 * Represents entries to the building, associated with original search result.
 */
public class RoutablePoints {

    /**
     * List of entries coordinates.
     */
    @SerializedName("points")
    public List<Point> points;
}

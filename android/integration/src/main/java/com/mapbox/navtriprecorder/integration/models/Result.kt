package com.mapbox.navtriprecorder.integration.models

sealed class Result<out T> {
    data class Success<T>(val data: T) : Result<T>()
    data class Error(val throwable: Throwable) : Result<Nothing>()

    val value: T?
        get() = (this as? Success)?.data

    fun asSuccess(): Success<out T>? = this as? Success

    fun asError(): Error? = this as? Error

    fun requireValue(): T = when (this) {
        is Success -> data
        is Error -> throw throwable
    }

    inline fun <R> map(mapper: (T) -> R): Result<R> {
        return when (this) {
            is Success -> Success(mapper(this.data))
            is Error -> this
        }
    }
}

inline fun <T> wrapAsResult(block: () -> T): Result<T> {
    return try {
        Result.Success(block())
    } catch (e: Throwable) {
        Result.Error(e)
    }
}

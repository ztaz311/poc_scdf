package ai.beans.sdk.model.api;

public class BeansRoutePointData {
    private BeansStopPointData stopPointData;
    private BeansEntranceData entranceData;
    private Integer floor;

    public BeansStopPointData getStopPointData() {
        return stopPointData;
    }

    public BeansEntranceData getEntranceData() {
        return entranceData;
    }

    public Integer getFloor() {
        return floor;
    }
}
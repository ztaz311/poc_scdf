package ai.beans.sdk.model.api;

public class BeansDestination {
    private String address;
    private BeansLocation location;

    public String getAddress() {
        return address;
    }

    public BeansLocation getLocation() {
        return location;
    }
}

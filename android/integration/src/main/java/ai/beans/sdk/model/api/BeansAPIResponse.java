package ai.beans.sdk.model.api;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import ai.beans.sdk.model.BeansCoreData;
import ai.beans.sdk.model.BeansMarker;

public class BeansAPIResponse {
    private List<BeansRoute> routes;
    private String queryId;

    public List<BeansRoute> getRoutes() {
        return routes;
    }

    public String getQueryId() {
        return queryId;
    }

    /*
     * Converts Beans API response to a much simpler core data object that consists only of navigate
     * location and the markers, where each marker has an asset id and a location.
     *
     * Returns null if the result is not available.
     */
    @Nullable
    public BeansCoreData toCoreData() {
        List<BeansRoute> routes = getRoutes();
        if (routes == null || routes.isEmpty()) {
            return null;
        }

        BeansRoute route = routes.get(0);
        if (route.getNavigateTo() == null
                || route.getFragments() == null
                || route.getFragments().isEmpty()) {
            return null;
        }

        List<BeansMarker> markers = new ArrayList<>();
        BeansLocation navigateTo = route.getNavigateTo();

        List<BeansRoutePoint> routePoints = new ArrayList<>();
        for (int i = 0; i < route.getFragments().size(); i++) {
            BeansFragment fragment = route.getFragments().get(i);
            if (i == 0 && fragment.getOrigin() != null && !fragment.getOrigin().getType().equals("UNIT")) {
                // Consider origin, otherwise only look at destinations
                routePoints.add(fragment.getOrigin());
            }

            if (fragment.getDestination() == null) {
                continue;
            }

            routePoints.add(fragment.getDestination());
        }

        for (BeansRoutePoint routePoint : routePoints) {
            if (routePoint.getLocation() == null) {
                continue;
            }
            if (routePoint.getType().equals("PARKING") || routePoint.getType().equals("STOP_POINT")) {
                markers.add(new BeansMarker(BeansMarker.AssetType.PARKING, routePoint.getLocation()));
            }
            if (routePoint.getType().equals("ENTRANCE")
                    && routePoint.getData() != null
                    && routePoint.getData().getEntranceData() != null
                    && routePoint.getData().getEntranceData().getRealm() != null
                    && routePoint.getData().getEntranceData().getRealm().equals("BUILDING")) {
                markers.add(new BeansMarker(BeansMarker.AssetType.ENTRANCE, routePoint.getLocation()));
            }
            if (routePoint.getType().equals("UNIT")) {
                markers.add(new BeansMarker(BeansMarker.AssetType.UNIT, routePoint.getLocation()));
            }
            if (routePoint.getType().equals("LOBBY")) {
                markers.add(new BeansMarker(BeansMarker.AssetType.LOBBY, routePoint.getLocation()));
            }
            if (routePoint.getType().equals("LOCKER")) {
                markers.add(new BeansMarker(BeansMarker.AssetType.LOCKER, routePoint.getLocation()));
            }
            // Currently ignores elevators and stairs
        }

        return new BeansCoreData(navigateTo, markers);
    }
}

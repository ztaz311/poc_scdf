package ai.beans.sdk.model.api;

import java.util.List;

public class BeansRoute {
    private List<BeansFragment> fragments;
    private BeansLocation origin;
    private BeansDestination destination;
    private BeansLocation navigateTo;

    public List<BeansFragment> getFragments() {
        return fragments;
    }

    public BeansLocation getOrigin() {
        return origin;
    }

    public BeansDestination getDestination() {
        return destination;
    }

    public BeansLocation getNavigateTo() {
        return navigateTo;
    }
}

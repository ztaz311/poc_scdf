package ai.beans.sdk.model;

import androidx.annotation.NonNull;

import java.util.List;

import ai.beans.sdk.model.api.BeansLocation;

public class BeansCoreData {
    @NonNull
    private final BeansLocation navigateTo;
    @NonNull
    private final List<BeansMarker> markers;

    public BeansCoreData(@NonNull BeansLocation navigateTo,
                         @NonNull List<BeansMarker> markers) {
        this.navigateTo = navigateTo;
        this.markers = markers;
    }

    @NonNull
    public BeansLocation getNavigateTo() {
        return navigateTo;
    }

    @NonNull
    public List<BeansMarker> getMarkers() {
        return markers;
    }
}

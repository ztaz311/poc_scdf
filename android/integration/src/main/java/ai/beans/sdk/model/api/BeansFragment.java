package ai.beans.sdk.model.api;

public class BeansFragment {
    private BeansRoutePoint origin;
    private BeansRoutePoint destination;
    private String type;

    public BeansRoutePoint getOrigin() {
        return origin;
    }

    public BeansRoutePoint getDestination() {
        return destination;
    }

    public String getType() {
        return type;
    }
}

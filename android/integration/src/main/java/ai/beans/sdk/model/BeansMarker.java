package ai.beans.sdk.model;

import androidx.annotation.NonNull;

import org.jetbrains.annotations.NotNull;

import ai.beans.sdk.model.api.BeansLocation;

public class BeansMarker {
    @NonNull
    public final AssetType assetType;
    @NonNull
    public final BeansLocation location;

    public BeansMarker(@NotNull AssetType assetName,
                       @NotNull BeansLocation location) {
        this.assetType = assetName;
        this.location = location;
    }

    public enum AssetType {
        PARKING,
        ENTRANCE,
        UNIT,
        LOCKER,
        LOBBY;
    }
}

package ai.beans.sdk.model.api;

import java.util.List;

public class BeansEntranceData {
    private String realm;
    private List<String> modalities;

    public String getRealm() {
        return realm;
    }

    public List<String> getModalities() {
        return modalities;
    }
}
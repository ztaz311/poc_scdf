package ai.beans.sdk.model.api;

public class BeansRoutePoint {
    private BeansLocation location;
    private String type;
    private BeansRoutePointData data;

    public BeansLocation getLocation() {
        return location;
    }

    public String getType() {
        return type;
    }

    public BeansRoutePointData getData() {
        return data;
    }
}

package com.mapbox.navtriprecorder.integration

import androidx.test.ext.junit.runners.AndroidJUnit4
import com.mapbox.navtriprecorder.integration.models.Point
import com.mapbox.navtriprecorder.integration.utils.FakeNetworkClient
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
class RoutablePointProviderIntegrationTest : BaseIntegrationTest() {

    private lateinit var mapboxSearchKit: MapboxSearchKit
    private lateinit var routablePointProvider: RoutablePointProvider
    private lateinit var fakeNetworkClient: FakeNetworkClient

    private val testDispatcher = TestCoroutineDispatcher()

    @Before
    fun setUp() {
        fakeNetworkClient = FakeNetworkClient()

        mapboxSearchKit = MapboxSearchKit(
            mapboxAccessToken = "pk.test",
            requestDispatcher = testDispatcher
        ).apply {
            networkClient = fakeNetworkClient
        }

        routablePointProvider = RoutablePointProvider(
            searchKit = mapboxSearchKit,
            requestDispatcher =  testDispatcher
        )
    }

    @Test
    fun testSuccessfulResponses() {
        fakeNetworkClient.responses.add { url ->
            (200 to readFileFromAssets("geocoding_responses/success.json"))
                .takeIf { url.toString().contains("api.mapbox") }
        }
        fakeNetworkClient.responses.add { url ->
            (200 to readFileFromAssets("ohf_responses/success.json"))
                .takeIf { url.toString().contains("api2.beans.ai") }
        }

        val result = runBlocking {
            routablePointProvider.findRoutablePoint(
                "test_address",
                Point(
                    latitude = 37.423,
                    longitude = -122.085
                )
            )
        }

        assertEquals(
            RoutablePointResult(
                routablePoint = Point(
                    latitude = 41.87256861579449,
                    longitude = -87.6307038743054
                ),
                searchAnalyticsData = SearchAnalyticsData(
                    mapboxRoutablePoint = Point(
                        latitude = 37.423123,
                        longitude = -122.084739
                    ),
                    mapboxSearchRequest = "https://api.mapbox.com/geocoding/v5/mapbox.places/test_address.json?access_token=*****&autocomplete=false&types=address&country=us&proximity=-122.085,37.423&limit=1&routing=true",
                    mapboxSearchRequestError = null,
                    mapboxSearchResponse = readFileFromAssets("geocoding_responses/success.json"),
                    mapboxSearchResponseCode = "200",
                    ohfRoutablePoint = Point(
                        latitude = 41.87256861579449,
                        longitude = -87.6307038743054
                    ),
                    ohfSearchRequest = "https://api2.beans.ai/enterprise/v2/search/beans?address=test_address",
                    ohfSearchRequestError = null,
                    ohfSearchResponse = readFileFromAssets("ohf_responses/success.json"),
                    ohfSearchResponseCode = "200",
                    providedGooglePoint = Point(
                        latitude = 37.423,
                        longitude = -122.085
                    ),
                    routablePoint = Point(
                        latitude = 41.87256861579449,
                        longitude = -87.6307038743054
                    ),
                    searchAddress = "test_address",
                    searchAddressUnit = null,
                    searchProviderUsed = SearchAnalyticsData.Provider.OHF
                )
            ),
            result
        )
    }

    @Test
    fun testErrorResponses() {
        fakeNetworkClient.responses.add { url ->
            (401 to readFileFromAssets("geocoding_responses/error.json"))
                .takeIf { url.toString().contains("api.mapbox") }
        }
        fakeNetworkClient.responses.add { url ->
            (401 to readFileFromAssets("ohf_responses/error.json"))
                .takeIf { url.toString().contains("api2.beans.ai") }
        }

        val result = runBlocking {
            routablePointProvider.findRoutablePoint(
                "test_address",
                Point(
                    latitude = 37.423,
                    longitude = -122.085
                )
            )
        }

        assertEquals(
            RoutablePointResult(
                routablePoint = Point(
                    latitude = 37.423,
                    longitude = -122.085
                ),
                searchAnalyticsData = SearchAnalyticsData(
                    mapboxRoutablePoint = null,
                    mapboxSearchRequest = "https://api.mapbox.com/geocoding/v5/mapbox.places/test_address.json?access_token=*****&autocomplete=false&types=address&country=us&proximity=-122.085,37.423&limit=1&routing=true",
                    mapboxSearchRequestError = readFileFromAssets("geocoding_responses/error.json"),
                    mapboxSearchResponse = readFileFromAssets("geocoding_responses/error.json"),
                    mapboxSearchResponseCode = "401",
                    ohfRoutablePoint = null,
                    ohfSearchRequest = "https://api2.beans.ai/enterprise/v2/search/beans?address=test_address",
                    ohfSearchRequestError = readFileFromAssets("ohf_responses/error.json"),
                    ohfSearchResponse = readFileFromAssets("ohf_responses/error.json"),
                    ohfSearchResponseCode = "401",
                    providedGooglePoint = Point(
                        latitude = 37.423,
                        longitude = -122.085
                    ),
                    routablePoint = Point(
                        latitude = 37.423,
                        longitude = -122.085
                    ),
                    searchAddress = "test_address",
                    searchAddressUnit = null,
                    searchProviderUsed = SearchAnalyticsData.Provider.GOOGLE
                )
            ),
            result
        )
    }

    @Test
    fun testUnexpectedError() {
        fakeNetworkClient.responses.add { url ->
            (200 to readFileFromAssets("geocoding_responses/success.json"))
                .takeIf { url.toString().contains("api.mapbox") }
        }
        fakeNetworkClient.responses.add { url ->
            if (url.toString().contains("api2.beans.ai")) {
                throw RuntimeException("Unexpected error during Beans.AI call")
            } else {
                null
            }
        }

        val result = runBlocking {
            routablePointProvider.findRoutablePoint(
                "test_address",
                Point(
                    latitude = 37.423,
                    longitude = -122.085
                )
            )
        }

        assertEquals(
            RoutablePointResult(
                routablePoint = Point(
                    latitude = 37.423123,
                    longitude = -122.084739
                ),
                searchAnalyticsData = SearchAnalyticsData(
                    mapboxRoutablePoint = Point(
                        latitude = 37.423123,
                        longitude = -122.084739
                    ),
                    mapboxSearchRequest = "https://api.mapbox.com/geocoding/v5/mapbox.places/test_address.json?access_token=*****&autocomplete=false&types=address&country=us&proximity=-122.085,37.423&limit=1&routing=true",
                    mapboxSearchRequestError = null,
                    mapboxSearchResponse = readFileFromAssets("geocoding_responses/success.json"),
                    mapboxSearchResponseCode = "200",
                    ohfRoutablePoint = null,
                    ohfSearchRequest = "https://api2.beans.ai/enterprise/v2/search/beans?address=test_address",
                    ohfSearchRequestError = "Unexpected error during Beans.AI call",
                    ohfSearchResponse = null,
                    ohfSearchResponseCode = null,
                    providedGooglePoint = Point(
                        latitude = 37.423,
                        longitude = -122.085
                    ),
                    routablePoint = Point(
                        latitude = 37.423123,
                        longitude = -122.084739
                    ),
                    searchAddress = "test_address",
                    searchAddressUnit = null,
                    searchProviderUsed = SearchAnalyticsData.Provider.MAPBOX
                )
            ),
            result
        )
    }

    @After
    fun tearDown() {
    }
}

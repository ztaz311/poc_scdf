package com.mapbox.navtriprecorder.integration

import android.app.Application
import androidx.test.platform.app.InstrumentationRegistry

abstract class BaseIntegrationTest {

    protected val targetApplication: Application
        get() = InstrumentationRegistry.getInstrumentation().targetContext.applicationContext as Application

    protected fun readBytesFromAssets(fileName: String): ByteArray {
        return targetApplication.resources.assets.open(fileName).use {
            it.readBytes()
        }
    }

    protected fun readFileFromAssets(fileName: String): String = String(readBytesFromAssets(fileName))

}

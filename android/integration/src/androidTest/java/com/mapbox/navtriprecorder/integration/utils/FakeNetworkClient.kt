package com.mapbox.navtriprecorder.integration.utils

import com.mapbox.navtriprecorder.integration.models.ResponseInfo
import com.mapbox.navtriprecorder.integration.network.NetworkClient
import java.net.HttpURLConnection
import java.net.URL

internal class FakeNetworkClient(
    val responses: MutableList<(URL) -> Pair<Int, String>?> = mutableListOf()
) : NetworkClient {

    override fun createResponseInfo(url: URL, setup: HttpURLConnection.() -> Unit): ResponseInfo {
        val (httpCode, body) = responses
            .map { it(url) }
            .filterNotNull()
            .firstOrNull() ?: error("Hasn't found response for $url")

        return ResponseInfo(
            requestUrl = url.toString(),
            isSuccessful = isSuccessfulHttpStatusCode(httpCode),
            httpResponseCode = httpCode,
            responseString = body
        )
    }

    private companion object {
        fun isSuccessfulHttpStatusCode(code: Int) = code / 100 == 2
    }
}

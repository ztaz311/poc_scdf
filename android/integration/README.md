# Mapbox nav-data-recorder-ga Integration project

Table of content
================

1) [How to attach this integration to your project](guides/integration.md)
2) [Firebase Setup](guides/firebase_setup.md)
3) [(optional)How to obtain One Hundred Feet icons](guides/how_to_obtain_ohf_icons.md)
4) [How to use RoutablePointProvider](guides/how_to_use_routable_point_logic.md)
5) [Setup Navigation Metrics recorder](guides/nav_metrics_recorder_setup.md)
6) [Track currentFirebaseUserUid with feedback to identify recording](guides/record_currentFirebaseUserUid.md)
7) [Uninstall Navigation Metrics Recorder](guides/uninstall.md)

# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile
-keep class *.R

-keepclasseswithmembers class **.R$* {
    public static <fields>;
}
-keep class com.ncs.breeze.ui.dashboard.fragments.view.TripInfoFragment {*;}
-keep class androidx.viewpager2.adapter.FragmentStateAdapter {*;}

-keep class com.facebook.react.devsupport.** { *; }
-dontwarn com.facebook.react.devsupport.**
-keepattributes Exceptions,InnerClasses,Signature,RuntimeVisibleParameterAnnotations,RuntimeVisibleAnnotations


# Adding both com.ncs.breeze.common.model and com.ncs.breeze.common.model,
# Since package names and folder names are not consistent
# TODO: package names and folder names should be made consistent
# for com.ncs.breeze.common.model
-keep class com.breeze.model.CoordinateType {*;}
-keep class com.ncs.breeze.common.model.response.NavigationZoneData** {*;}
-keep class com.ncs.breeze.common.model.response.Feature** {*;}
-keep class com.ncs.breeze.common.model.response.Geometry** {*;}
-keep class com.ncs.breeze.common.model.response.Proprties** {*;}
-keepclassmembers class com.ncs.breeze.common.model.** {*;}

# for com.ncs.breeze.common.model
-keepclassmembers class com.ncs.breeze.common.model.** {*;}

# for triton
-keepclassmembers class com.ncs.breeze.common.triton.client.** {*;}


-keep class com.ncs.breeze.common.integration.geocoding.** { *; }

-keep class ai.beans.sdk.model.** { *; } # If you use Beans.AI integration
-keep class com.mapbox.turf.* {*;}
-keep class com.mapbox.auto.** {*;}
-keep class androidx.viewpager2.adapter.FragmentStateAdapter {*;}

-keep class com.ncs.breeze.BuildConfig { *; }

# Firestore
-keep class com.breeze.model.FirebaseDB { *; }

# Start for react-native-fast-image
-keep public class com.dylanvann.fastimage.* {*;}
-keep public class com.dylanvann.fastimage.** {*;}
-keep public class * implements com.bumptech.glide.module.GlideModule
-keep public class * extends com.bumptech.glide.module.AppGlideModule
-keep public enum com.bumptech.glide.load.ImageHeaderParser$** {
  **[] $VALUES;
  public *;
}
# End for react-native-fast-image

# Start for React Native Reanimated
-keep class com.swmansion.reanimated.** { *; }
-keep class com.facebook.react.turbomodule.** { *; }
# End for React Native Reanimated

# hermes
-keep class com.facebook.hermes.unicode.** { *; }
-keep class com.facebook.jni.** { *; }


# crashlytics
-keepattributes SourceFile,LineNumberTable        # Keep file names and line numbers.
-keep public class * extends java.lang.Exception  # Optional: Keep custom exceptions.

# inapp browser
-keepattributes *Annotation*
-keepclassmembers class ** {
  @org.greenrobot.eventbus.Subscribe <methods>;
}
-keep enum org.greenrobot.eventbus.ThreadMode { *; }


# Car
-keep class com.ncs.breeze.car.** {*;}

-keep class ai.beans.sdk.model.** { *; } # If you use Beans.AI integration
-keep class com.mapbox.turf.* {*;}
-keep class com.mapbox.maps.** {*;}
-keep class com.mapbox.navigation.* {*;}
-keep class com.mapbox.search.ui.* {*;}
-keep class com.mapbox.search.ui.utils.* {*;}
-keep class com.mapbox.search.ui.view.* {*;}
-keep class com.mapbox.**.** {*;}
-keep class com.mapbox.auto.** {*;}

-keep class com.breeze.model.** {*;}
-keepclassmembers class com.breeze.model.** {*;}

-dontwarn com.amazonaws.mobileconnectors.cognitoauth.**
-dontwarn com.amazonaws.mobile.auth.**
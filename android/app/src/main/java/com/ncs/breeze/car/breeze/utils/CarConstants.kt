package com.ncs.breeze.car.breeze.utils

import androidx.annotation.StringDef

object CarConstants {

    const val HOME_MARKER = "HOME_MARKER";

}

object ShortCutTypes {
    const val HOME = "HOME"
    const val WORK = "WORK"
    const val NORMAL = "NORMAL"
}

@Retention(AnnotationRetention.SOURCE)
@StringDef(ShortCutTypes.HOME, ShortCutTypes.WORK, ShortCutTypes.NORMAL)
annotation class ShortCutType
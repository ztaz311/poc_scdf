package com.ncs.breeze.common.helper.ehorizon

import com.mapbox.navigation.base.road.model.RoadComponent

interface BreezeEdgeMetaDataObserver {

    /**
     * invoked to update view for edge metadata related to speedLimit, edge name, tunnel
     */
    fun updateEdgeMetaData(speedLimit: Double?, names: List<RoadComponent>?, tunnel: Boolean)
}
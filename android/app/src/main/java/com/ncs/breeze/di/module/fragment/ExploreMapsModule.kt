package com.ncs.breeze.di.module.fragment

import com.ncs.breeze.ui.dashboard.fragments.exploremap.ExploreMapsFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class ExploreMapsModule {
    @ContributesAndroidInjector
    abstract fun contributeExploreMaps(): ExploreMapsFragment
}
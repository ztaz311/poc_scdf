package com.ncs.breeze.common.helper.ehorizon

import com.breeze.model.NavigationZone

interface BreezeNotificationObserver {

    /**
     * invoked to update notification view with the road object metadata, when onPositionUpdated is triggered
     */
    fun showNotificationView(
        zone: NavigationZone,
        distance: Int,
        mERPName: String?,
        chargeAmount: String?,
        roadObjectID: String,
        location: String?
    )

    /**
     * invoked to remove notification view with the road object passed metadata
     */
    fun removeNotificationView(roadObjectId: String);

}
package com.ncs.breeze.common.utils

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.view.View
import androidx.appcompat.app.AlertDialog
import com.ncs.breeze.R


object DialogFactory {
    fun createSimpleOkDialog(
        context: Context?,
        message: String?,
        listener: DialogInterface.OnClickListener?
    ): Dialog? {
        val alertDialog = AlertDialog.Builder(
            context!!
        )
            .setMessage(message)
            .setCancelable(false)
            .setPositiveButton("Got it", listener)

        return alertDialog.create()


    }

    fun createChoiceDialog(
        context: Context?,
        title: String?,
        message: String?,
        btnTextPositive: String?,
        btnTextNegative: String?,
        actionPositive: View.OnClickListener?,
        actionNegative: View.OnClickListener?,
        canDismiss: Boolean,
        dissmissListener: DialogInterface.OnDismissListener?
    ): Dialog {
        val alertDialog = AlertDialog.Builder(
            context!!
        )
            .setTitle(title)
            .setMessage(message)
            .setPositiveButton(btnTextPositive, object : DialogInterface.OnClickListener {
                override fun onClick(p0: DialogInterface?, p1: Int) {
                    if (actionPositive != null) {
                        actionPositive.onClick(null)
                    }
                    p0?.dismiss()
                }
            })
            .setNegativeButton(btnTextNegative, object : DialogInterface.OnClickListener {
                override fun onClick(p0: DialogInterface?, p1: Int) {
                    if (actionNegative != null) {
                        actionNegative.onClick(null)
                    }
                    p0?.dismiss()
                }
            })
            .setOnDismissListener(dissmissListener)
            .setCancelable(canDismiss)

        return alertDialog.create()
    }

    fun createSingleChoiceDialog(
        context: Context?,
        title: String?,
        message: String?,
        btnTextPositive: String?,
        actionPositive: View.OnClickListener?,
        canDismiss: Boolean,
        dissmissListener: DialogInterface.OnDismissListener?
    ): Dialog {
        val alertDialog = AlertDialog.Builder(
            context!!
        )
            .setTitle(title)
            .setMessage(message)
            .setPositiveButton(btnTextPositive, object : DialogInterface.OnClickListener {
                override fun onClick(p0: DialogInterface?, p1: Int) {
                    if (actionPositive != null) {
                        actionPositive.onClick(null)
                    }
                    p0?.dismiss()
                }
            })
            .setOnDismissListener(dissmissListener)
            .setCancelable(canDismiss)

        return alertDialog.create()
    }

    fun internetDialog(context: Context?, listener: DialogInterface.OnClickListener?): Dialog? {

        val alertDialog = AlertDialog.Builder(
            context!!
        ).setTitle(context.getString(R.string.no_internet_toast))
            .setMessage(context.getString(R.string.no_internet_desc))
            .setPositiveButton((android.R.string.ok), listener)
        return alertDialog.create()


    }
}
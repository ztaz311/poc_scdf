package com.ncs.breeze.ui.navigation

import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.content.res.Configuration
import android.graphics.Color
import android.graphics.drawable.AnimationDrawable
import android.graphics.drawable.ColorDrawable
import android.location.Location
import android.media.AudioManager
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.TextUtils
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.view.ViewTreeObserver
import android.view.WindowManager
import android.widget.ImageView
import android.widget.TextView
import androidx.activity.viewModels
import androidx.annotation.DrawableRes
import androidx.annotation.Keep
import androidx.car.app.connection.CarConnection
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.core.text.isDigitsOnly
import androidx.core.view.isVisible
import androidx.core.view.updateLayoutParams
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.whenResumed
import com.breeze.customization.view.BreezeButton
import com.breeze.customization.view.navigationalert.OBUAlertMessageViewWithActions
import com.breeze.model.*
import com.breeze.model.api.response.ValueConditionShowBroadcast
import com.breeze.model.api.response.amenities.Amenities
import com.breeze.model.api.response.amenities.BaseAmenity
import com.breeze.model.constants.AmenityType.CARPARK
import com.breeze.model.constants.AmenityType.EVCHARGER
import com.breeze.model.constants.AmenityType.PETROL
import com.breeze.model.constants.Constants
import com.breeze.model.constants.NavigationType
import com.breeze.model.constants.ShareDestinationType
import com.breeze.model.constants.TYPE_BROADCAST_MESSAGE
import com.breeze.model.enums.AmenityBand
import com.breeze.model.extensions.decompressToDirectionsResponse
import com.breeze.model.extensions.dpToPx
import com.breeze.model.extensions.round
import com.breeze.model.extensions.safeDouble
import com.breeze.model.extensions.toBitmap
import com.breeze.model.extensions.visibleAmount
import com.breeze.model.obu.OBUParkingAvailability
import com.breeze.model.obu.OBURoadEventData
import com.breeze.model.obu.OBUTravelTimeData
import com.facebook.react.bridge.Arguments
import com.facebook.react.bridge.WritableMap
import com.mapbox.android.gestures.MoveGestureDetector
import com.mapbox.android.gestures.StandardScaleGestureDetector
import com.mapbox.api.directions.v5.DirectionsCriteria
import com.mapbox.api.directions.v5.MapboxDirections
import com.mapbox.api.directions.v5.models.DirectionsResponse
import com.mapbox.api.directions.v5.models.RouteOptions
import com.mapbox.geojson.FeatureCollection
import com.mapbox.geojson.GeometryCollection
import com.mapbox.geojson.Point
import com.mapbox.maps.CameraOptions
import com.mapbox.maps.EdgeInsets
import com.mapbox.maps.MapView
import com.mapbox.maps.Style
import com.mapbox.maps.extension.style.expressions.dsl.generated.interpolate
import com.mapbox.maps.extension.style.expressions.generated.Expression
import com.mapbox.maps.extension.style.layers.addLayer
import com.mapbox.maps.extension.style.layers.generated.LineLayer
import com.mapbox.maps.extension.style.layers.generated.symbolLayer
import com.mapbox.maps.extension.style.layers.getLayer
import com.mapbox.maps.extension.style.layers.getLayerAs
import com.mapbox.maps.extension.style.layers.properties.generated.IconAnchor
import com.mapbox.maps.extension.style.layers.properties.generated.TextAnchor
import com.mapbox.maps.extension.style.sources.addSource
import com.mapbox.maps.extension.style.sources.generated.geoJsonSource
import com.mapbox.maps.plugin.LocationPuck2D
import com.mapbox.maps.plugin.animation.camera
import com.mapbox.maps.plugin.animation.easeTo
import com.mapbox.maps.plugin.gestures.OnMoveListener
import com.mapbox.maps.plugin.gestures.OnScaleListener
import com.mapbox.maps.plugin.gestures.gestures
import com.mapbox.maps.plugin.locationcomponent.LocationComponentConstants
import com.mapbox.maps.plugin.locationcomponent.LocationComponentPlugin
import com.mapbox.maps.plugin.locationcomponent.OnIndicatorPositionChangedListener
import com.mapbox.maps.plugin.locationcomponent.location
import com.mapbox.maps.plugin.logo.logo
import com.mapbox.navigation.base.ExperimentalPreviewMapboxNavigationAPI
import com.mapbox.navigation.base.TimeFormat
import com.mapbox.navigation.base.extensions.applyDefaultNavigationOptions
import com.mapbox.navigation.base.extensions.coordinates
import com.mapbox.navigation.base.formatter.DistanceFormatterOptions
import com.mapbox.navigation.base.formatter.UnitType
import com.mapbox.navigation.base.road.model.RoadComponent
import com.mapbox.navigation.base.route.NavigationRoute
import com.mapbox.navigation.base.route.NavigationRouterCallback
import com.mapbox.navigation.base.route.RouterFailure
import com.mapbox.navigation.base.route.RouterOrigin
import com.mapbox.navigation.base.trip.model.RouteLegProgress
import com.mapbox.navigation.base.trip.model.RouteProgress
import com.mapbox.navigation.base.utils.DecodeUtils.completeGeometryToLineString
import com.mapbox.navigation.core.directions.session.RoutesObserver
import com.mapbox.navigation.core.lifecycle.MapboxNavigationApp
import com.mapbox.navigation.core.trip.session.LocationMatcherResult
import com.mapbox.navigation.core.trip.session.LocationObserver
import com.mapbox.navigation.core.trip.session.RouteProgressObserver
import com.mapbox.navigation.core.trip.session.TripSessionState
import com.mapbox.navigation.ui.maneuver.api.MapboxManeuverApi
import com.mapbox.navigation.ui.maps.camera.NavigationCamera
import com.mapbox.navigation.ui.maps.camera.data.MapboxNavigationViewportDataSource
import com.mapbox.navigation.ui.maps.camera.lifecycle.NavigationBasicGesturesHandler
import com.mapbox.navigation.ui.maps.location.NavigationLocationProvider
import com.mapbox.navigation.ui.maps.route.arrow.api.MapboxRouteArrowApi
import com.mapbox.navigation.ui.maps.route.arrow.api.MapboxRouteArrowView
import com.mapbox.navigation.ui.maps.route.arrow.model.RouteArrowOptions
import com.mapbox.navigation.ui.maps.route.line.api.MapboxRouteLineApi
import com.mapbox.navigation.ui.maps.route.line.api.MapboxRouteLineView
import com.mapbox.navigation.ui.maps.route.line.model.MapboxRouteLineOptions
import com.mapbox.navigation.ui.maps.route.line.model.RouteLineResources
import com.mapbox.navigation.ui.speedlimit.api.MapboxSpeedInfoApi
import com.mapbox.navigation.ui.tripprogress.api.MapboxTripProgressApi
import com.mapbox.navigation.ui.tripprogress.model.*
import com.mapbox.navigation.ui.utils.internal.ifNonNull
import com.mapbox.navigation.utils.internal.toPoint
import com.mapbox.navtriprecorder.integration.RoutablePointResult
import com.mapbox.turf.TurfConstants
import com.mapbox.turf.TurfMeasurement
import com.ncs.breeze.App
import com.ncs.breeze.R
import com.ncs.breeze.car.breeze.ReplayRouteTripSession
import com.ncs.breeze.car.breeze.screen.navigation.CarNavigationScreenState
import com.ncs.breeze.car.breeze.screen.obulite.CarOBULiteManager
import com.ncs.breeze.car.breeze.utils.BreezeCarUtil
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.AnalyticsUtils
import com.ncs.breeze.common.analytics.Event
import com.ncs.breeze.common.analytics.Param
import com.ncs.breeze.common.analytics.Screen
import com.ncs.breeze.common.appSync.ClientFactory
import com.ncs.breeze.common.components.maplayer.WalkingRouteLineLayer.Companion.addDashingStyleToLine
import com.ncs.breeze.common.constant.MapIcon
import com.ncs.breeze.common.constant.OBUConnectionState
import com.ncs.breeze.common.constant.OBUConstants
import com.ncs.breeze.common.constant.RNScreen
import com.ncs.breeze.common.event.ToCarRx.postEventWithCacheLast
import com.ncs.breeze.common.extensions.android.getApp
import com.ncs.breeze.common.extensions.android.getBreezeUserPreference
import com.ncs.breeze.common.extensions.android.getRNFragmentNavigationParams
import com.ncs.breeze.common.extensions.android.getUserDataPreference
import com.ncs.breeze.common.extensions.mapbox.setBreezeDefaultOptions
import com.ncs.breeze.common.helper.ehorizon.BreezeEdgeMetaDataObserver
import com.ncs.breeze.common.helper.ehorizon.BreezeNotificationObserver
import com.ncs.breeze.common.helper.ehorizon.BreezeRoadObjectHandler
import com.ncs.breeze.common.helper.ehorizon.EHorizonMode
import com.ncs.breeze.common.helper.obu.OBUDataHelper
import com.ncs.breeze.common.model.rx.AppToNavigationEndEvent
import com.ncs.breeze.common.model.rx.AppToPhoneNavigationEndEvent
import com.ncs.breeze.common.model.rx.ToAppRxEvent
import com.ncs.breeze.common.model.rx.UpdateStateVoiceApp
import com.ncs.breeze.common.remote.ApiHelper
import com.ncs.breeze.common.rxbus.RxBus
import com.ncs.breeze.common.rxbus.RxEvent
import com.ncs.breeze.common.service.HorizonObserverFillterZoneUtils
import com.ncs.breeze.common.storage.BreezeUserPreference
import com.ncs.breeze.common.utils.*
import com.ncs.breeze.common.utils.RoutePlanningConsts.PROP_ERP_DISPLAY
import com.ncs.breeze.common.utils.distanceFormater.BreezeMapboxDistanceFormatter
import com.ncs.breeze.common.utils.mapStyle.BreezeNavigationMapStyle
import com.ncs.breeze.common.utils.walkathontracker.WalkathonEngine
import com.ncs.breeze.components.GlobalNotification
import com.ncs.breeze.components.GlobalToast
import com.ncs.breeze.components.map.profile.ProfileZoneLayer
import com.ncs.breeze.components.map.profile.ProfileZoneState
import com.ncs.breeze.components.navigationlog.HistoryRecorder
import com.ncs.breeze.customview.TBTAmenityToggleView
import com.ncs.breeze.databinding.ActivityTurnByTurnNavigationBinding
import com.ncs.breeze.reactnative.nativemodule.ReactNativeEventEmitter
import com.ncs.breeze.reactnative.nativemodule.ShareBusinessLogicEvent
import com.ncs.breeze.service.NavigationControllerService
import com.ncs.breeze.ui.base.BaseMapActivity
import com.ncs.breeze.ui.dashboard.fragments.obulite.OBUCheckSpeedHelper
import com.ncs.breeze.ui.dashboard.fragments.obulite.isStationary
import com.ncs.breeze.ui.rn.CustomReactFragment
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.*
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import sg.gov.lta.obu.sdk.core.enums.OBUCardStatus
import sg.gov.lta.obu.sdk.data.services.OBUSDK
import timber.log.Timber
import java.util.*
import javax.inject.Inject
import kotlin.math.roundToInt


@Keep
class TurnByTurnNavigationActivity :
    BaseMapActivity<ActivityTurnByTurnNavigationBinding, NavigationViewModel>() {

    companion object {

        private const val FOLLOW_MAX_ZOOM = 18.5
        private const val FOLLOW_MIN_ZOOM = 17.0
        private const val INITIAL_ZOOM = 13.0
        private const val INITIAL_PITCH = 45.0
        private const val SHW_END_NAVGTN_CNFRMTN_BFR_WLKNG = false

        private const val WHAT_RESET_AMENITY_MODE = 1199
        private const val WHAT_CHECK_DISTANCE_WAYPOINT = 1133
        private const val WHAT_CHECK_DISTANCE_TO_DESTINATION = 1166
    }

    private var carConnection: CarConnection? = null
    private var lastCarConnectionState = CarConnection.CONNECTION_TYPE_NOT_CONNECTED

    // job for initializing route

    private lateinit var routeInitJob: Job

    private val alertDialogHelper by lazy { TurnByTurnAlertDialogHelper(this) }


    private var timeArrival: String = ""
    private var mRoute: NavigationRoute? = null
    private var shouldRefreshLandingCarParks = false

    // to check if threshold for refreshing carparks has reached
    private var carParkRefreshThreshold: Boolean = false
    private val profileDetectionMutex = Mutex()


    private lateinit var breezeEdgeMetaDataObserver: BreezeEdgeMetaDataObserver
    private lateinit var breezeNotificationObserver: BreezeNotificationObserver

    private var displayedNavigationZone: NavigationZone? = null

    private lateinit var arrivedWalkathonDialog: Dialog
    private lateinit var ratingDialog: Dialog
    private var isRatingDialogDismissed: Boolean = false
    private var showWalkingPathDialogOnResume: Boolean = false
    private var showArrivedWalkathonDialogOnResume: Boolean = false

    @Inject
    lateinit var breezeFeatureDetectionUtil: BreezeFeatureDetectionUtil

    @Inject
    lateinit var breezeUserPreferenceUtil: BreezeUserPreferenceUtil

    //    private lateinit var mapboxNavigation: MapboxNavigation
    private lateinit var locationComponent: LocationComponentPlugin
    private lateinit var navigationCamera: NavigationCamera
    private lateinit var viewportDataSource: MapboxNavigationViewportDataSource
    private lateinit var tripProgressApi: MapboxTripProgressApi
    private lateinit var maneuverApi: MapboxManeuverApi

    /**
     * estimated time to arrive to destination in milliseconds
     * */
    private var currentEstimatedTimeToArrival = 0L
    private var maxDistance: Int = 0
    private var curTripId: String? = null

    @Inject
    lateinit var breezeERPUtil: BreezeERPRefreshUtil

    @Inject
    lateinit var breezeIncidentsUtil: BreezeTrafficIncidentsUtil

    @Inject
    lateinit var breezeCalendarUtil: BreezeCalendarUtil

    @Inject
    lateinit var breezeTripLoggingUtil: BreezeTripLoggingUtil

    @Inject
    lateinit var breezeStatisticsUtil: BreezeStatisticsUtil

    @Inject
    lateinit var breezeHistoryRecorderUtil: BreezeHistoryRecorderUtil

    @Inject
    lateinit var apiHelper: ApiHelper

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val viewModel: NavigationViewModel by viewModels {
        viewModelFactory
    }

    private lateinit var originalDestinationDetails: DestinationAddressDetails
    private lateinit var currentDestinationDetails: DestinationAddressDetails

    var mNotificationCriterias: ArrayList<ValueConditionShowBroadcast>? = null

    private var walkingRoute: NavigationRoute? = null
    var isWalking: Boolean = false
    private var selectedCarParkIconRes: Int = -1

    private lateinit var breezeRoadObjectHandler: BreezeRoadObjectHandler

    private var navAmenityLayerManager: TurnByTurnAmenityViewManager? = null

    var isEndingNavigationOnResume: Boolean = false

    private lateinit var locationObserver: LocationObserver
    private val speedLimitApi: MapboxSpeedInfoApi by lazy {
        MapboxSpeedInfoApi()
    }

    private lateinit var routablePointResult: RoutablePointResult

    private lateinit var currentDirectionsRoute: NavigationRoute
    private lateinit var currentRouteERPInfo: ERPRouteInfo

    private var isInProgressCheckBroadcastNoty = false
    private var currentDataCriteriasUse: ValueConditionShowBroadcast? = null

    private var routeLineAPI: MapboxRouteLineApi? = null
    private var routeLineView: MapboxRouteLineView? = null
    private var routeArrowView: MapboxRouteArrowView? = null
    private val routeArrowAPI: MapboxRouteArrowApi = MapboxRouteArrowApi()
    private var navigationLocationProvider = NavigationLocationProvider()

    private val overviewEdgeInsets: EdgeInsets by lazy {
        EdgeInsets(
            viewBinding.mapView.getMapboxMap().getSize().height.toDouble() * 1.0 / 4.0,
            40.0.dpToPx(),
            40.0.dpToPx(),
            40.0.dpToPx()
        )
    }

    private var visibleArea: EdgeInsets =
        EdgeInsets(
            40.0.dpToPx(),
            40.0.dpToPx(),
            40.0.dpToPx(),
            40.0.dpToPx()
        )
    private val landscapeOverviewEdgeInsets: EdgeInsets by lazy {
        EdgeInsets(
            20.0.dpToPx(),
            viewBinding.mapView.getMapboxMap().getSize().width.toDouble() / 1.75,
            20.0.dpToPx(),
            20.0.dpToPx()
        )
    }

    private lateinit var mapboxHistoryRecorder: HistoryRecorder
    private var autoRecenterMapHandler: Handler? = null

    var isCameraTrackingDismissed: Boolean = false
    var shareDriveAnnounced: Boolean = false

    var destinationProfileZone: ProfileZoneLayer.ProfileZoneType? = null
    var currentLocationZoneFirstTime: ProfileZoneLayer.ProfileZoneType? =
        ProfileZoneLayer.ProfileZoneType(null, null, ProfileZoneLayer.OUTER_ZONE)

    private val profileZoneState = ProfileZoneState()
    private var currentUserRawLocation: Location? = null
    private var currentLocationMatcherResult: LocationMatcherResult? = null

    private var removeAmenitiesHandler: Handler? = null

    private var shouldShowOBUConnectedMessage = false

    private var distanceRemainingInMetre: Float = -1f
    private var obuCheckSpeedHelper: OBUCheckSpeedHelper? = null
    private var isCurrentLocationHasCarPark = true
    private var isCarparkDisableByUser = false
    private var shouldResetAmenity = true

    private val speedLimitHandler = object : SpeedLimitHandler {
        override fun alertOnThreshold() {
            //VoiceInstructionsManager.getInstance()?.playVoiceInstructions(getString(R.string.voice_speed_limit_alert))
            BreezeAudioUtils.playOverSpeedAlert(this@TurnByTurnNavigationActivity)
        }
    }

    private val onMoveListener = object : OnMoveListener {
        override fun onMoveBegin(detector: MoveGestureDetector) {
            onCameraTrackingDismissed()
            if (viewBinding.toggleMapAmenity.displayMode != TBTAmenityToggleView.DisplayMode.NONE) {
                removeAmenitiesHandler?.removeMessages(WHAT_RESET_AMENITY_MODE)
                removeAmenitiesHandler?.sendEmptyMessageDelayed(WHAT_RESET_AMENITY_MODE, 10000)
            }
        }

        override fun onMove(detector: MoveGestureDetector): Boolean {
            return false
        }

        override fun onMoveEnd(detector: MoveGestureDetector) {}
    }

    private val mapMatcherResultObserver = object : LocationObserver {

        override fun onNewLocationMatcherResult(locationMatcherResult: LocationMatcherResult) {
            Timber.d("LocationObserver: Map matcher result observer called ${locationMatcherResult.enhancedLocation}")
//            publishUserLocationOBUData(locationMatcherResult)
            currentLocationMatcherResult = locationMatcherResult

            // get speed to check stationary
            val speedInfoValue = speedLimitApi.updatePostedAndCurrentSpeed(
                locationMatcherResult,
                DistanceFormatterOptions.Builder(this@TurnByTurnNavigationActivity)
                    .unitType(UnitType.METRIC)
                    .build()
            )
            obuCheckSpeedHelper?.changeSpeed(speedInfoValue.currentSpeed.toDouble())


            (navigationLocationProvider as? NavigationLocationProvider)?.let {
                val transitionOptions: (ValueAnimator.() -> Unit) =
                    if (locationMatcherResult.isTeleport) {
                        {
                            duration = 0
                        }
                    } else {
                        {
                            duration = 1000
                        }
                    }
                it.changePosition(
                    locationMatcherResult.enhancedLocation,
                    locationMatcherResult.keyPoints,
                    latLngTransitionOptions = transitionOptions,
                    bearingTransitionOptions = transitionOptions
                )

                viewportDataSource.onLocationChanged(locationMatcherResult.enhancedLocation)

                viewportDataSource.evaluate()
            }

            if (lifecycle.currentState.isAtLeast(Lifecycle.State.STARTED) && !OBUSDK.isConnectionActive()) {
                trackUserIdleOBUDisconnected(locationMatcherResult)
            }

            Timber.d("Map matcher result observer exited")
        }

        override fun onNewRawLocation(rawLocation: Location) {
            currentUserRawLocation = rawLocation
            /**
             * detect if user go to zone of tiong bahru
             * 1. if destination in zone first
             * 2. if current location in zone second
             */
            //Timber.w("Mock location ? [${rawLocation.isFromMockProvider}]")
            if (destinationProfileZone?.selectedProfile != null && currentLocationZoneFirstTime?.type == ProfileZoneLayer.NO_ZONE) {
                lifecycleScope.launch(Dispatchers.IO) {
                    profileDetectionMutex.withLock {
                        val profileDataPair =
                            ProfileZoneLayer.detectSelectedProfileLocationInZone(
                                destinationProfileZone!!.selectedProfile!!, rawLocation
                            )
                        when (profileDataPair.type) {
//                                ProfileZoneLayer.INNER_ZONE -> {
//                                    if(!profileZoneState.isInnnerTrigger){
//                                        profileZoneState.isInnnerTrigger=true
//                                        profileZoneState.isOuterTrigger=true
//                                        turnbyturnViewModel.getZoneAlertDetail(
//                                            profile = profileDataPair.selectedProfile!!,
//                                            navigationtype = NavigationType.NAVIGATION,
//                                            zoneid = profileDataPair.profileZoneId!!,
//                                            targetZoneId = null,
//                                            destinationCarpark = currentDestinationDetails.isCarPark,
//                                            carparkId = currentDestinationDetails.carParkID)
//                                    }
//                                }
                            ProfileZoneLayer.OUTER_ZONE -> {
                                if (!profileZoneState.isOuterTrigger) {
                                    profileZoneState.isOuterTrigger = true
                                    val innerZoneID =
                                        ProfileZoneLayer.getInnerZone(destinationProfileZone!!.selectedProfile!!)
                                    if (destinationProfileZone!!.type == ProfileZoneLayer.OUTER_ZONE) {
                                        val centerPoint = ProfileZoneLayer.getZoneCenter(
                                            destinationProfileZone!!.selectedProfile!!
                                        )
                                        centerPoint?.let {
                                            val radiusThreshold =
                                                ProfileZoneLayer.getZoneMinRadius(
                                                    destinationProfileZone!!.selectedProfile!!
                                                )
                                            if (::currentDirectionsRoute.isInitialized) {
                                                val foundPoint =
                                                    currentDirectionsRoute.directionsRoute.completeGeometryToLineString()
                                                        .coordinates().find {
                                                            return@find (TurfMeasurement.distance(
                                                                centerPoint,
                                                                it,
                                                                TurfConstants.UNIT_METERS
                                                            ) < radiusThreshold)
                                                        }
                                                if (foundPoint != null) {
                                                    viewModel.getZoneAlertDetail(
                                                        profile = profileDataPair.selectedProfile!!,
                                                        navigationtype = NavigationType.NAVIGATION,
                                                        zoneid = profileDataPair.profileZoneId!!,
                                                        targetZoneId = innerZoneID,
                                                        destinationCarpark = currentDestinationDetails.isDestinationCarPark(),
                                                    )
                                                }
                                            } else {
                                                profileZoneState.isOuterTrigger = false
                                            }
                                        }
                                    } else {
                                        viewModel.getZoneAlertDetail(
                                            profile = profileDataPair.selectedProfile!!,
                                            navigationtype = NavigationType.NAVIGATION,
                                            zoneid = profileDataPair.profileZoneId!!,
                                            targetZoneId = innerZoneID,
                                            destinationCarpark = currentDestinationDetails.isDestinationCarPark(),
                                        )
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private val routesObserver = RoutesObserver { result ->
        Timber.d("REROUTE - Routes observer called ${result.navigationRoutes.size}")
        if (result.navigationRoutes.isNotEmpty()) {
            showWalkingPathReReroutingMessage()
            lifecycleScope.launch(Dispatchers.Main) {
                navAmenityLayerManager?.currentRoute = result.navigationRoutes[0]
                routeLineAPI?.setNavigationRoutes(listOf(result.navigationRoutes[0])) { value ->
                    viewBinding.mapView.getMapboxMap().getStyle { style ->
                        routeLineView?.let { view ->
                            view.renderRouteDrawData(style, value)
                            handleRouteLineRefresh()
                        }
                    }
                }
            }
            viewportDataSource.onRouteChanged(result.navigationRoutes[0])
            currentDirectionsRoute = result.navigationRoutes[0]
            //Timber.d("--- REROUTE - ${currentDirectionsRoute.directionsRoute.geometry()} ---")

            computeRouteERPs()
            hideWalkingPathReReroutingMessage()
        } else {
            viewportDataSource.clearRouteData()
            updateCameraToIdle()
            clearRouteLine()
        }
        Timber.d("REROUTE - Routes observer exited")
    }

    private val onIndicatorPositionChangedListener = OnIndicatorPositionChangedListener { point ->
        routeLineAPI?.updateTraveledRouteLine(point)?.let { result ->
            viewBinding.mapView.getMapboxMap().getStyle { style ->
                routeLineView?.run {
                    renderRouteLineUpdate(style, result)
                    handleRouteLineRefresh()
                }
            }
        }
    }

    private val routeProgressObserver = RouteProgressObserver { routeProgress ->
        navAmenityLayerManager?.waypointsRemaining = routeProgress.remainingWaypoints
        distanceRemainingInMetre = routeProgress.distanceRemaining

        //Timber.d("--- PROGRESS - ${routeProgress.route.geometry()} ---")
        //Timber.d("--- PROGRESS % - ${routeProgress.fractionTraveled} ---")

        Timber.d("Route progress change observer called")
        viewportDataSource.onRouteProgressChanged(routeProgress)
        viewportDataSource.evaluate()

        routeArrowAPI.addUpcomingManeuverArrow(routeProgress).also {
            viewBinding.mapView.getMapboxMap().getStyle { style ->
                routeArrowView?.renderManeuverUpdate(style, it)
            }
        }
        if (::tripProgressApi.isInitialized) {
            Timber.d("Rendering of bottom panel information")
            render(routeProgress)
        }

        routeLineAPI?.updateWithRouteProgress(routeProgress) { result ->
            viewBinding.mapView.getMapboxMap().getStyle { style ->
                routeLineView?.renderRouteLineUpdate(style, result)
                handleRouteLineRefresh()
            }
        }

        // update top maneuver instructions

        val maneuvers = maneuverApi.getManeuvers(routeProgress)
        maneuvers.fold({ error ->
        }, {
//                Timber.d("Rendering of Top panel information ${routeProgress.distanceRemaining}")
            Timber.d("Rendering of Top panel information: ${routeProgress.distanceRemaining}")
            maneuvers.onValue { list ->
                viewBinding.customManeuverView.renderManeuvers(list)
            }
        })
        Timber.d("Route progress change observer exited")
    }

    private fun autoHideCarConnectedWarning() {
        if (CarNavigationScreenState.state.value?.isAtLeast(Lifecycle.State.STARTED) == true)
            (supportFragmentManager.findFragmentByTag(CarConnectedWarningFragment.TAG) as? CarConnectedWarningFragment)
                ?.showAutoHideWarningScreen()
    }

    private fun setupCustomManeuverView() {
        viewBinding.customManeuverView.setMapboxManeuversList(viewBinding.layoutManeuverList.upcomingManeuverRecycler) { _, _, _ ->
            autoHideCarConnectedWarning()
        }

        //Fix header
        viewBinding.customManeuverView.updatePrimaryManeuverTextAppearance(R.style.BreezeStylePrimaryManeuver)
        viewBinding.customManeuverView.updateSecondaryManeuverTextAppearance(R.style.BreezeStyleSecondaryManeuver)
        viewBinding.customManeuverView.updateSubManeuverTextAppearance(R.style.BreezeStyleSubManeuver)
        viewBinding.customManeuverView.updateStepDistanceTextAppearance(R.style.BreezeStyleStepDistance)
        viewBinding.customManeuverView.updateExitViewTextAppearance(R.style.BreezeStyleExitManeuver)
        viewBinding.customManeuverView.updateTurnIconStyle(R.style.BreezeMapboxStyleTurnIconManeuverNav)

        //List items
        viewBinding.customManeuverView.updateUpcomingManeuverStyles(
            stylePrimaryManeuver = R.style.BreezeStylePrimaryManeuver,
            styleSecondaryManeuver = R.style.BreezeStyleSecondaryManeuver,
            styleStepDistance = R.style.BreezeStyleStepDistance,
            styleExitManeuver = R.style.BreezeStyleExitManeuver,
            styleManeuverIcon = R.style.BreezeMapboxStyleTurnIconManeuverNav
        )
    }

    fun getDestinationLocation(): Point? {
        val lat = originalDestinationDetails.lat?.toDoubleOrNull()
        val long = originalDestinationDetails.long?.toDoubleOrNull()
        if (lat == null || long == null)
            return null
        return Point.fromLngLat(long, lat)
    }

    private fun showWalkingPathReReroutingMessage() {
        CoroutineScope(Dispatchers.Main).launch {
            viewBinding.txtRerouting.visibility = View.VISIBLE
        }
    }

    private fun hideWalkingPathReReroutingMessage() {
        CoroutineScope(Dispatchers.Main).launch {
            viewBinding.txtRerouting.visibility = View.GONE
        }
    }

    private fun setUpGestureListeners() {
        viewBinding.mapView.gestures.addOnMoveListener(onMoveListener)
        viewBinding.mapView.gestures.addOnFlingListener {
            Timber.d("On fling is called")
            Analytics.logMapInteractionEvents(Event.DRAG_MAP, getScreenName())
        }

        viewBinding.mapView.gestures.addOnScaleListener(object : OnScaleListener {
            override fun onScale(detector: StandardScaleGestureDetector) {}

            override fun onScaleBegin(detector: StandardScaleGestureDetector) {}

            override fun onScaleEnd(detector: StandardScaleGestureDetector) {
                Timber.d("On scale end is called")
                Analytics.logMapInteractionEvents(Event.PINCH_MAP, getScreenName())
            }
        })
    }

    private fun render(routeProgress: RouteProgress) {
        val tripProgress = tripProgressApi.getTripProgress(routeProgress)
        Timber.d("Rendering trip progress")

        val distanceFormatter = BreezeMapboxDistanceFormatter(
            DistanceFormatterOptions.Builder(
                applicationContext
            ).unitType(UnitType.METRIC).build()
        )
        val distance =
            distanceFormatter.formatDistance(tripProgress.distanceRemaining).toString().split(
                " "
            )
        viewBinding.layoutNavBottom.distanceRemainingText.text = distance[0]
        if (maxDistance < tripProgress.distanceRemaining.toInt()) {
            maxDistance = tripProgress.distanceRemaining.toInt()
        }
        //updating an inverse progress bar android:scaleX="-1", goes from 100 -> 0

        viewBinding.layoutNavBottom.navigationInverseProgressBar.progress =
            100 - (tripProgress.percentRouteTraveled * 100.0).toInt()
        viewBinding.layoutNavBottom.distanceUnit.text = distance[1]

        val timeToArrivalFormatter = EstimatedTimeToArrivalFormatter(
            this,
//            TimeFormat.TWENTY_FOUR_HOURS
            TimeFormat.TWELVE_HOURS
        )

        currentEstimatedTimeToArrival = tripProgress.estimatedTimeToArrival
        val time =
            timeToArrivalFormatter.format(tripProgress.estimatedTimeToArrival).toString().split(
                " "
            )
        timeArrival = time[0] + time[1]
        viewBinding.layoutNavBottom.estimatedTimeToArriveText.text = time[0]
        viewBinding.layoutNavBottom.timeToArrivalUnit.text = time[1]

        val timeRemaining =
            tripProgress.formatter.getTimeRemaining(tripProgress.totalTimeRemaining).toString()
                .split(
                    " "
                )
        if (timeRemaining[0].isDigitsOnly()) {
            viewBinding.layoutNavBottom.timeUnit.text = timeRemaining[1]
            viewBinding.layoutNavBottom.timeRemainingText.text = timeRemaining[0]
            viewBinding.arrivalNotification.bottomSheetHdr.text =
                "${timeRemaining[0]} ${timeRemaining[1]} walk"
            //BREEZE2-1845
//            ETAEngine.getInstance()?.updateArrivalTime(timeArrival)
//            ETAEngine.getInstance()?.updateCoordinates(
//                routeProgress.route.geometry() ?: ""
//            ) //ToDo - Send only remaining route line here

            //Here we can print the decoded value of geometry received and use them for debugging
            //val input = routeProgress.route.geometry() ?: ""
            //val points6 = PolylineUtils.decode(input, Constants.POLYLINE_PRECISION)
            //val points5 = PolylineUtils.decode(input, 5)
            //Timber.d("--- points6 : $points6 --")
            //Timber.d("--- points5 : $points5 --")

            //BREEZE2-1845
//            ETAEngine.getInstance()?.sendETAMessage(tripProgress.totalTimeRemaining)
        } else {
            viewBinding.layoutNavBottom.timeRemainingText.text = "0"
            viewBinding.layoutNavBottom.timeUnit.text = "min"
            updateArrivalIconAndMessage()
        }

        /**
         * checking show broadcast messsage
         */
        checkShowNotificationBroadcast(distance[0], distance[1], timeRemaining[0], timeRemaining[1])

        Timber.d("Rendering trip progress done")
    }

    private fun checkShowNotificationBroadcast(
        distance: String, distanceUnit: String, timeRemaining: String, timeUnit: String
    ) {
        if (currentDestinationDetails.isDestinationCarPark() && !isInProgressCheckBroadcastNoty) {
            var distanceMeter: Double? = null
            if (distance.toDoubleOrNull() != null) {
                if (distanceUnit.equals("km", true)) {
                    distanceMeter = distance.toDouble() * 1000
                } else if (distanceUnit.equals("m", true)) {
                    distanceMeter = distance.toDouble()
                }
            }
            var timeSecond: Int? = null
            if (timeRemaining.toFloatOrNull() != null) {
                if (timeUnit.equals("min", true)) {
                    timeSecond = timeRemaining.toInt() * 60
                } else if (timeUnit.equals("day", true)) {
                    timeSecond = timeRemaining.toInt() * 24 * 60 * 60
                }
            }

            if (distanceMeter != null || timeSecond != null) {
                mNotificationCriterias?.forEach {
                    currentDataCriteriasUse = it
                    if (it.distance != null && distanceMeter != null) {
                        if (distanceMeter <= it.distance!!) {
                            currentDestinationDetails.getSelectedCarParkID()?.let { carparkID ->
                                getCarparkBroadcastMessage(
                                    carparkID, it.duration, TYPE_BROADCAST_MESSAGE.NAVIGATION
                                )
                            }
                        }
                    } else if (it.time != null && timeSecond != null) {
                        if (timeSecond <= it.time!!) {
                            currentDestinationDetails.getSelectedCarParkID()?.let { carparkID ->
                                getCarparkBroadcastMessage(
                                    carparkID, it.duration, TYPE_BROADCAST_MESSAGE.NAVIGATION
                                )
                            }
                        }
                    }
                }
            }
        }
    }

    private fun getCarparkBroadcastMessage(
        idCarpark: String?,
        duration: Int = 15,
        typeBroadcastMessage: String = TYPE_BROADCAST_MESSAGE.NAVIGATION
    ) {
        idCarpark?.let {
            viewModel.mBroadcastMessage.observe(
                this
            ) { broadcastData ->
                /**
                 * remove item from list criterias
                 */
                if (typeBroadcastMessage == TYPE_BROADCAST_MESSAGE.NAVIGATION) {
                    mNotificationCriterias?.remove(currentDataCriteriasUse)
                }
                viewModel.mBroadcastMessage.removeObservers(this)
                broadcastData?.apply {
                    if (success && message != "") {
                        if (typeBroadcastMessage == TYPE_BROADCAST_MESSAGE.NAVIGATION) {
                            showCarParkBroadcastNotification(message, duration)
                            isInProgressCheckBroadcastNoty = false
                        } else if (typeBroadcastMessage == TYPE_BROADCAST_MESSAGE.PLANNING) {
                            mNotificationCriterias = notificationCriterias
                        }
                    }
                }
            }
            if (typeBroadcastMessage == TYPE_BROADCAST_MESSAGE.NAVIGATION) {
                isInProgressCheckBroadcastNoty = true
            }
            viewModel.getBroadcastMessage(idCarpark, typeBroadcastMessage)
        }
    }

    private fun showCarParkBroadcastNotification(message: String, duration: Int) {
        viewBinding.featureNotification.root.visibility = View.GONE
        viewBinding.notificationParkBroadcast.root.visibility = View.VISIBLE
        viewBinding.notificationParkBroadcast.tvTitleParkBroadcast.text =
            currentDestinationDetails.address1 ?: ""
        viewBinding.notificationParkBroadcast.tvDescriptionParkBroadcast.text = message
        lifecycleScope.launch(Dispatchers.Main) {
            delay((duration * 1000).toLong())
            viewBinding.notificationParkBroadcast.root.visibility = View.GONE
        }
    }


    private fun resetArrivalIconAndMessage() {
        viewBinding.arrivalNotification.bottomSheetHdr.setAutoSizeTextTypeWithDefaults(TextView.AUTO_SIZE_TEXT_TYPE_NONE)
        viewBinding.arrivalNotification.bottomSheetHdr.setTextSize(
            TypedValue.COMPLEX_UNIT_DIP, 32.0f
        )
        viewBinding.arrivalNotification.root.visibility = View.VISIBLE
        viewBinding.arrivalNotification.arrivedAddressButton.visibility = View.GONE
    }

    private fun updateArrivalIconAndMessage() {
        if (currentDestinationDetails.isDestinationCarPark() && !isWalking) {
            viewBinding.arrivalNotification.bottomSheetHdr.setAutoSizeTextTypeWithDefaults(TextView.AUTO_SIZE_TEXT_TYPE_UNIFORM)
            viewBinding.arrivalNotification.arrivedAddressButton.visibility = View.VISIBLE
            viewBinding.arrivalNotification.arrivedAddressButton.setImageResource(R.drawable.ic_parking_info_bright_purple)
            viewBinding.arrivalNotification.bottomSheetHdr.text =
                getString(R.string.we_have_reached_location, getDestinationName())
        } else {
            viewBinding.arrivalNotification.bottomSheetHdr.setAutoSizeTextTypeWithDefaults(TextView.AUTO_SIZE_TEXT_TYPE_NONE)
            viewBinding.arrivalNotification.bottomSheetHdr.setTextSize(
                TypedValue.COMPLEX_UNIT_DIP, 32.0f
            )
            viewBinding.arrivalNotification.arrivedAddressButton.visibility = View.VISIBLE
            viewBinding.arrivalNotification.arrivedAddressButton.setImageResource(R.drawable.saved_place)
            viewBinding.arrivalNotification.bottomSheetHdr.text =
                getString(R.string.we_have_arrived)
        }
    }

    private var isArrived: Boolean = false

    @ExperimentalPreviewMapboxNavigationAPI
    private fun showArrivedNotification(tripId: String?) {
        Analytics.logNotificationEvents(
            Event.NATURAL_END_DONE, Event.NOTIFICATION_OPEN, getScreenName()
        )
        viewBinding.etaToast.destroy()
        viewBinding.arrivalNotification.root.visibility = View.VISIBLE
        viewBinding.toggleMapAmenity.isVisible = false

        viewBinding.llTopButtons.updateLayoutParams<ConstraintLayout.LayoutParams> {
            startToStart = ConstraintLayout.LayoutParams.PARENT_ID
            topToBottom = viewBinding.arrivalNotification.root.id
        }
//        viewBinding.layoutSpeedLimit.root.updateLayoutParams<ConstraintLayout.LayoutParams> {
//            startToStart = ConstraintLayout.LayoutParams.PARENT_ID
//            topToBottom = viewBinding.arrivalNotification.root.id
//        }

        updateArrivalIconAndMessage()

        viewBinding.customManeuverView.visibility = View.GONE

        viewBinding.layoutNavBottom.recenterButton.visibility = View.GONE

        Analytics.logNotificationEvents(
            Event.NATURAL_END_DONE, Event.NOTIFICATION_CLOSE, getScreenName()
        )

        //hide carparks
        navAmenityLayerManager?.removeCarParks()
        MapboxNavigationApp.current()?.let {
            it.unregisterRoutesObserver(routesObserver)
            it.unregisterRouteProgressObserver(routeProgressObserver)
            if (::mapboxHistoryRecorder.isInitialized) {
                mapboxHistoryRecorder.stopRecording()
                it.unregisterTripSessionStateObserver(mapboxHistoryRecorder)
            }
            BreezeArrivalObserver.getInstance()?.unregisterNavigationArrivalObserver(
                navigationArrivalObserver, shouldStopTrip = false
            )
        }
        breezeRoadObjectHandler.voiceEnabled = false
        VoiceInstructionsManager.getInstance()?.unRegisterVoiceInstruction()
        breezeRoadObjectHandler.removeBreezeNotificationObserver(breezeNotificationObserver)
        viewBinding.mapView.getMapboxMap().getStyle { style ->
            breezeRoadObjectHandler.removeBreezeNotificationStyle(style)
            unregisterEhorizonNotificationObservers(style)
            viewBinding.tunnelToast.isVisible = false
        }
        viewportDataSource.clearRouteData()
        updateCameraToOverview()
        clearRouteLine()
        handleRouteLineRefresh()

        updateTripProgressSection()
        //BREEZE2-1845
//        viewBinding.layoutNavBottom.ETAViewTurnByTurn.visibility = View.GONE

        /**
         * check show carpark destination status
         */
        Timber.e("showArrivedNotification: " +
                "${currentDestinationDetails.showAvailabilityFB}, " +
                "${currentDestinationDetails.amenityType}, " +
                "${currentDestinationDetails.getSelectedCarParkID()}, " +
                "${currentDestinationDetails.userLocationLinkIdRef}, " +
                "${currentDestinationDetails.placeId}, " +
                "${currentDestinationDetails.amenityId}"
        )
        val showAvailabilityFB =
            currentDestinationDetails.showAvailabilityFB &&
                    currentDestinationDetails.amenityType != PETROL && currentDestinationDetails.amenityType != EVCHARGER &&
                    (!currentDestinationDetails.getSelectedCarParkID().isNullOrEmpty() ||
                            !currentDestinationDetails.userLocationLinkIdRef.isNullOrEmpty() ||
                            !currentDestinationDetails.placeId.isNullOrEmpty() ||
                            !currentDestinationDetails.amenityId.isNullOrEmpty())

        if (showAvailabilityFB && !isWalking) {
            openParkingAvailabilityScreen()
        } else {
            if (walkingRoute != null && !isWalking) {
                viewBinding.layoutNavBottom.layoutTravelInfo.visibility = View.GONE
                showWalkingPathDialog()
            }

            if (isWalking && currentDestinationDetails.trackNavigation) {
                showArrivedWalkathonDialog()
            }
        }
    }


    /**
     * when user click cancel bottom sheet update carpark status
     */
    fun cancelParkingAvailable() {
        if (walkingRoute != null && !isWalking) {
            viewBinding.layoutNavBottom.layoutTravelInfo.visibility = View.GONE
//            showWalkingPathDialog()
        } else {
            hideBottomSheetCarparkAvailability()
        }
    }

    /**
     * start walking path from react
     * + hide bottom sheet
     * + auto start walking
     */
    fun startWalkingPathFromReact() {
        shouldRefreshLandingCarParks = true
        hideBottomSheetCarparkAvailability()
        startWalkingNavigation()
    }

    private fun hideBottomSheetCarparkAvailability() {
        val currentFragment =
            supportFragmentManager.findFragmentById(R.id.layoutForCarparkAvailability)
        if (currentFragment != null) {
            if (currentFragment.isAdded) {
                supportFragmentManager.beginTransaction().remove(currentFragment).commit()
            }
        }
    }

    /**
     * send event to update carpark status
     */
    private fun sendEventUpdateCarParkStatus(carparkId: String, availablePercentImageBand: String) {
        App.instance?.let { app ->
            val shareBusinessLogicPackage = app.shareBusinessLogicPackage
            shareBusinessLogicPackage.getRequestsModule().flatMap {
                val rnData = Arguments.createMap().apply {
                    putString("availablePercentImageBand", availablePercentImageBand)
                    putString("carparkId", carparkId)
                }
                it.callRN(ShareBusinessLogicEvent.UPDATE_PARKING_AVAILABLE_STATUS.eventName, rnData)
            }.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe()
        }

    }

    /**
     * open carpark status screen
     */
    private fun openParkingAvailabilityScreen() {
        lifecycleScope.launch {
            lifecycle.whenResumed {
                val navigationParams = getRNFragmentNavigationParams(
                    sessionToken = myServiceInterceptor.getSessionToken(),
                    fragmentTag = RNScreen.PARKING_AVAILABLE
                ).apply {
                    putBoolean("isWalking", walkingRoute != null && !isWalking)
                    putString(Constants.RN_FROM_SCREEN, Screen.NAVIGATION)
                    putParcelable(
                        "data", Bundle().apply {
                            currentDestinationDetails.getSelectedCarParkID()?.takeIf { it.isNotEmpty() }?.let {
                                putString("carparkId", it)
                            }
                            currentDestinationDetails.placeId?.takeIf { it.isNotEmpty() }?.let {
                                putString("placeId", it)
                            }
                            currentDestinationDetails.userLocationLinkIdRef?.takeIf { it.isNotEmpty() && it.toIntOrNull() != null }
                                ?.let {
                                    putInt("userLocationLinkIdRef", it.toInt())
                                }
                            currentDestinationDetails.amenityId?.takeIf { it.isNotEmpty() }?.let {
                                putString("amenityId", it)
                            }
                            currentDestinationDetails.layerCode?.takeIf { it.isNotEmpty() }?.let {
                                putString("layerCode", it)
                            }
                        }
                    )
                }

                Timber.e("openParkingAvailabilityScreen: $navigationParams")
                val initialProperties = Bundle().apply {
                    putString(Constants.RN_TO_SCREEN, RNScreen.PARKING_AVAILABLE)
                    putBundle(Constants.RN_CONSTANTS.NAVIGATION_PARAMS, navigationParams)
                }
                val reactNativeFragment = CustomReactFragment.newInstance(
                    componentName = Constants.RN_CONSTANTS.COMPONENT_NAME,
                    launchOptions = initialProperties,
                    cleanOnDestroyed = false
                )
                supportFragmentManager.beginTransaction()
                    .add(
                        R.id.layoutForCarparkAvailability,
                        reactNativeFragment,
                        RNScreen.PARKING_AVAILABLE
                    )
                    .addToBackStack(RNScreen.PARKING_AVAILABLE).commit()
            }
        }
    }

    fun backToHomePageFromRN() {
        shouldRefreshLandingCarParks = true
        doEndNavigation()
    }

    private fun setActivityResult() {
        Timber.d("Activity result has been set")
        VoiceInstructionsManager.getInstance()?.cancelSpeech()
        val intent = Intent()
        intent.putExtra(Constants.NAVIGATION_END, true)
        intent.putExtra(
            Constants.NAVIGATION_END_REFRESH_LANDING_CAR_PARKS, shouldRefreshLandingCarParks
        )
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    private fun showArrivedWalkathonDialog() {
        if (baseContext == null) return

        if (!lifecycle.currentState.isAtLeast(Lifecycle.State.RESUMED)) {
            showArrivedWalkathonDialogOnResume = true
            return
        }

        showArrivedWalkathonDialogOnResume = false

        arrivedWalkathonDialog = Dialog(this@TurnByTurnNavigationActivity)

        arrivedWalkathonDialog.setCancelable(false)
        arrivedWalkathonDialog.setContentView(R.layout.dialog_arrived)
        arrivedWalkathonDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        arrivedWalkathonDialog.setCanceledOnTouchOutside(false)
        val buttonGo = arrivedWalkathonDialog.findViewById<BreezeButton>(R.id.btn_go)
        buttonGo.setOnClickListener {
            doEndNavigation()
        }
        val txtMessage = arrivedWalkathonDialog.findViewById<TextView>(R.id.message)
        txtMessage.text = "You have arrived at ${currentDestinationDetails.address1}"

        if (lifecycle.currentState.isAtLeast(Lifecycle.State.RESUMED)) {
            arrivedWalkathonDialog.show()
        }
    }

    //FIXME: app can crash when session is restored in turnbyturn
    // fixed crash issue with ETAEngine, but it will disable eta when this happens
    @ExperimentalPreviewMapboxNavigationAPI
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        volumeControlStream = AudioManager.STREAM_MUSIC
        viewBinding.root.keepScreenOn = true
        //RxBus.publish(RxEvent.CruiseMode(false))

        Timber.d("TurnByTurnNavigation onCreate is called")

        val intent = intent
        val args = intent.getBundleExtra(Constants.NAVIGATION_BUNDLE)
        if (args != null && args.containsKey(Constants.NAVIGATION_BUNDLE_DESTINATION_DETAILS)) {

            mNotificationCriterias = args.getParcelableArrayList(Constants.LIST_CRITERIAS)

            currentDestinationDetails =
                args.getSerializable(Constants.NAVIGATION_BUNDLE_DESTINATION_DETAILS) as DestinationAddressDetails

            originalDestinationDetails =
                args.getSerializable(Constants.NAVIGATION_BUNDLE_ORIGINAL_DESTINATION_DETAILS) as DestinationAddressDetails

            val compressedDirectionsResponse =
                args.getByteArray(Constants.NAVIGATION_BUNDLE_ROUTE_RESPONSE)!!

            val directionsRouteOptions =
                args.getSerializable(Constants.NAVIGATION_BUNDLE_ROUTE_OPTIONS) as RouteOptions

            selectedCarParkIconRes =
                args.getInt(Constants.NAVIGATION_BUNDLE_SELECTED_CARPARK_ICON_RES, -1)

            val selectedRouteIndex = args.getInt(Constants.NAVIGATION_BUNDLE_SELECTED_ROUTE_INDEX)

            val skipDriving = args.getBoolean(Constants.NAVIGATION_BUNDLE_SKIP_DRIVING, false)

            if (args.containsKey(Constants.NAVIGATION_BUNDLE_WALKING_ROUTE_DIRECTION)) {
                val directionsRoute =
                    args.getSerializable(Constants.NAVIGATION_BUNDLE_WALKING_ROUTE_DIRECTION) as DirectionsResponse
                val routeOptions =
                    args.getSerializable(Constants.NAVIGATION_BUNDLE_WALKING_ROUTE_ROUTE_OPTIONS) as RouteOptions
                val walkingRoutes = NavigationRoute.create(directionsRoute, routeOptions)
                if (walkingRoutes.isNotEmpty()) {
                    walkingRoute = walkingRoutes[0]
                }
            }

            populateArrivalView()
            setupCustomManeuverView()
            viewBinding.mapView.setBreezeDefaultOptions()
            viewBinding.mapView.setMaximumFps(30)

            viewBinding.layoutNavBottom.layoutTravelInfo.viewTreeObserver.addOnGlobalLayoutListener(
                object :
                    ViewTreeObserver.OnGlobalLayoutListener {
                    override fun onGlobalLayout() {
                        viewBinding.layoutNavBottom.layoutTravelInfo.viewTreeObserver.removeOnGlobalLayoutListener(
                            this
                        )
                        viewBinding.mapView.logo.marginBottom =
                            viewBinding.layoutNavBottom.layoutTravelInfo.height + 24.0f.dpToPx()
                    }
                })

            locationComponent = viewBinding.mapView.location.apply {
                this.locationPuck = LocationPuck2D(
                    bearingImage = ContextCompat.getDrawable(
                        this@TurnByTurnNavigationActivity, R.drawable.ic_navigaion_puck
                    ), scaleExpression = interpolate {
                        linear()
                        zoom()
                        stop {
                            literal(0.0)
                            literal(0.6)
                        }
                        stop {
                            literal(20.0)
                            literal(1.0)
                        }
                    }.toJson()
                ) //AAL
                pulsingEnabled = true
                pulsingColor = getColor(R.color.themed_nav_pulse_color)
                pulsingMaxRadius = 50.0F
                addOnIndicatorPositionChangedListener(onIndicatorPositionChangedListener)
                enabled = true
                setLocationProvider(navigationLocationProvider)
            }
            viewportDataSource = MapboxNavigationViewportDataSource(
                viewBinding.mapView.getMapboxMap()
            )
            viewportDataSource.followingPitchPropertyOverride(50.0)
            viewportDataSource.options.followingFrameOptions.maxZoom = FOLLOW_MAX_ZOOM
            viewportDataSource.options.followingFrameOptions.minZoom = FOLLOW_MIN_ZOOM
            navigationCamera = NavigationCamera(
                viewBinding.mapView.getMapboxMap(),
                viewBinding.mapView.camera,
                viewportDataSource
            )
            viewBinding.mapView.camera.addCameraAnimationsLifecycleListener(
                NavigationBasicGesturesHandler(navigationCamera)
            )

            maneuverApi = MapboxManeuverApi(
                BreezeMapboxDistanceFormatter(
                    DistanceFormatterOptions.Builder(this)
                        .locale(Locale(Constants.MAPBOX_DISTANCE_FORMATTER_LOCALE)).build()
                )
            )
            Timber.d("Nav - Before initialising navigation and map style")

            initNavigation()
            updateCameraLocation()
            initViews()

            initRouteDetails(
                compressedDirectionsResponse, directionsRouteOptions, selectedRouteIndex
            )
            init(args, skipDriving)

            if (currentDestinationDetails.destinationAddressType == DestinationAddressTypes.CALENDAR) {
                Timber.d("Chosen destination is Calendar item")
                compositeDisposable.add(RxBus.listen(RxEvent.NotifyCalendarEventDestinationChange::class.java)
                    .observeOn(AndroidSchedulers.mainThread()).subscribe {
                        alertDialogHelper.showRerouteConfirm(
                            it.destinationDestails.destinationName,
                            onProceed = {
                                // Setting currentDestinationDetails to new DestinationDetails and proceeding with reroute
                                currentDestinationDetails = it.destinationDestails
                                triggerReroute()
                                breezeCalendarUtil.rerouteToNewLocation(it.destinationDestails)
                            }
                        )
                    })
                breezeCalendarUtil.navigatingToCalendarEventDestination(currentDestinationDetails)
            }

            viewBinding.toggleVoice.setOnClickListener {
                VoiceInstructionsManager.getInstance()?.toggleMute()
            }

        }

        Analytics.logScreenView(getScreenName())

        compositeDisposable.add(RxBus.listen(RxEvent.SpeedChanged::class.java).subscribe {
            when (it.value) {
                SpeedLimitUtil.NORMAL_SPEED -> {
                    viewBinding.layoutSpeedLimit.ivSpeedLimit.setImageResource(R.drawable.img_bg_speedlimit_normal)
                }

                SpeedLimitUtil.OVER_SPEED -> {
                    viewBinding.layoutSpeedLimit.ivSpeedLimit.setImageResource(R.drawable.speed_limit_over_blinking)
                    val frameAnimation: AnimationDrawable =
                        viewBinding.layoutSpeedLimit.ivSpeedLimit.drawable as AnimationDrawable
                    frameAnimation.isOneShot = true //stops animation after running one time
                    frameAnimation.start()
                }
            }
        })

        /**
         * register play sound alert when user go to TBR
         */
        viewModel.mResponseOuterZoneAlertDetail.observe(this) {
            if (it.alertMessage != null && it.alertMessage!!.isNotEmpty()) {
                VoiceInstructionsManager.getInstance()?.playVoiceInstructions(it.alertMessage!!)
            }
//            if (it.isCongestion == true) {//BREEZE2-1545
//                renderDestinationNearbyCarParks(false)
//            }
        }

        viewModel.mResponseInnerZoneAlertDetail.observe(this) {
            if (it.alertMessage != null && it.alertMessage!!.isNotEmpty()) {
                VoiceInstructionsManager.getInstance()?.playVoiceInstructions(it.alertMessage!!)
            }
//            if (it.isCongestion == true) {//BREEZE2-1545
//                renderDestinationNearbyCarParks(false)
//            }
        }


        /**
         * start detect if destination user go to zone of profile
         */
        detectDestinationInProfileZone()

        /**
         * start detect if current user go to zone of profile
         */
        detectCurrentLocationInProfileZone()

        listenCarNavigationScreenLifecycle()
        addCarConnectedWarningFragment()
    }

    private fun showOBUAlertMessageGeneral(message: String, detailMessage: String) {
        with(viewBinding) {
            queueAlertView.addGeneralMessageView(message, detailMessage)
        }
    }

    override fun onStop() {
        super.onStop()
        navigationCamera.resetFrame()
        viewBinding.etaToast.stopToastETA()
    }

    private fun addCarConnectedWarningFragment() {
        supportFragmentManager.beginTransaction().replace(
            viewBinding.layoutManeuverList.fragmentCarConnectedWarningContainer.id,
            CarConnectedWarningFragment.newInstance(true),
            CarConnectedWarningFragment.TAG
        ).commitNow()
    }

    private fun listenCarNavigationScreenLifecycle() {
        CarNavigationScreenState.state.observe(this) { state ->
            if (state == Lifecycle.State.DESTROYED) {
                CarNavigationScreenState.canStartNavigation = false
                doEndNavigation()
                return@observe
            }
            Timber.d("listenCarNavigationScreenLifecycle state= $state-${CarNavigationScreenState.movedToAnotherScreen}")
            val isVisible =
                (state.isAtLeast(Lifecycle.State.STARTED) || CarNavigationScreenState.movedToAnotherScreen)

            if (isVisible) {
                Analytics.logPopupEvent(
                    Event.OBU_PROJECTED_TO_DHU,
                    Event.VALUE_POPUP_OPEN,
                    Screen.NAVIGATION
                )
            }
            if (!BuildConfig.DEBUG) {
                viewBinding.layoutManeuverList.upcomingManeuverRecycler.isVisible = isVisible
                viewBinding.layoutManeuverList.layoutManeuverListContainer.isVisible = isVisible
                viewBinding.layoutManeuverList.fragmentCarConnectedWarningContainer.isVisible =
                    isVisible
                viewBinding.customManeuverView.isVisible = !isVisible
            }

        }
    }

    private fun initRouteDetails(
        compressedDirectionsResponse: ByteArray,
        directionsRouteOptions: RouteOptions,
        selectedRouteIndex: Int
    ) {
        routeInitJob = lifecycleScope.launch(Dispatchers.Default) {
            val directionsResponse = compressedDirectionsResponse.decompressToDirectionsResponse()
            //NavigationRoute list returned cannot be empty as this is validated earlier
            mRoute = createNavigationRoute(
                directionsResponse, directionsRouteOptions, selectedRouteIndex
            )!!
            maxDistance = mRoute?.directionsRoute?.distance()?.toInt() ?: 0
        }
    }

    private fun initRouteObservers(args: Bundle) {
        //BREEZE2-1845
//        if (args.containsKey(Constants.NAVIGATION_ETA_DATA)) {
//            ETAEngine.getInstance()?.let {
//                it.startETA(mRoute?.directionsRoute)
//                viewBinding.layoutNavBottom.ETAViewTurnByTurn.updateUIOnlyForETA()
//            }
//        }
//
//
//        if (maxDistance < 50) {
//            viewBinding.layoutNavBottom.ETAViewTurnByTurn.visibility = View.GONE
//        } else {
//            if (!isWalking) {
//                viewBinding.layoutNavBottom.ETAViewTurnByTurn.visibility = View.VISIBLE
//            }
//        }

        initNavigationObservers(mRoute)
        //BREEZE2-1845
//        startLocationUpdates()
//        viewModel.etaStartProgress.observe(this@TurnByTurnNavigationActivity) {
//            if (true == it) {
//                viewBinding.layoutNavBottom.ETAViewTurnByTurn.visibility = View.GONE
//            } else {
//                if (!isWalking) {
//                    viewBinding.layoutNavBottom.ETAViewTurnByTurn.visibility = View.VISIBLE
//                }
//            }
//        }
//
//        viewModel.etaStartSuccessful.observe(this@TurnByTurnNavigationActivity) {
//            ETAEngine.getInstance()?.apply {
//                startETA(mRoute?.directionsRoute)
//                updateETAStatus(ETAStatus.INPROGRESS)
//                viewBinding.layoutNavBottom.ETAViewTurnByTurn.updateUIOnlyForETA()
//                announceShareDriveStarted()
//            }
//        }
//
//        compositeDisposable.add(RxBus.listen(RxEvent.ETARNDataCallback::class.java)
//            .observeOn(AndroidSchedulers.mainThread()).subscribe {
//                etaRNCAllback(it.data)
//            })

        compositeDisposable.add(RxBus.listen(RxEvent.CloseNotifyArrivalScreen::class.java)
            .observeOn(AndroidSchedulers.mainThread()).subscribe {
                closeEtaPopUp()
            })

        compositeDisposable.add(RxBus.listen(RxEvent.OBUCarParkSelectedEvent::class.java)
            .observeOn(AndroidSchedulers.mainThread()).subscribe { item ->
                viewBinding.navigationFrameContainer.isVisible = false
                closeCarparkListScreenRN()
                val amenity = BaseAmenity.fromRNData(item.carPark)
                AnalyticsUtils.sendSelectCarPark(amenity, Screen.NAVIGATION)
                if (!(currentDestinationDetails.isDestinationCarPark() && amenity.id == currentDestinationDetails.amenityId)) {
                    navAmenityLayerManager?.findRouteToCarPark(amenity)
                    showCustomToast(
                        R.drawable.tick_successful,
                        getString(R.string.toast_routing_to_carpark)
                    )
                    recenterMap()
                }
            })

        //BREEZE2-1845
//        /**
//         *check ETA button first time
//         */
//        handleETAFirstTime()
    }


    /**
     * detect if user go to zone of tiong bahru
     */
    private fun detectDestinationInProfileZone() {
        currentDestinationDetails.let {
            val locationDestination = Location("")
            if (it.lat != null && it.long != null) {
                locationDestination.latitude = it.lat!!.toDouble()
                locationDestination.longitude = it.long!!.toDouble()
                lifecycleScope.launch(Dispatchers.IO) {
                    val destinationInZoneData =
                        ProfileZoneLayer.detectAllProfileLocationInZone(locationDestination)
                    if (destinationInZoneData.type == ProfileZoneLayer.OUTER_ZONE) {
                        destinationProfileZone = destinationInZoneData
                    }
                }
            }

        }
    }


    /**
     * detect current user go to profile zone
     */
    private fun detectCurrentLocationInProfileZone() {
        LocationBreezeManager.getInstance().currentLocation.let {
            lifecycleScope.launch(Dispatchers.IO) {
                val currentLocationZoneData = ProfileZoneLayer.detectAllProfileLocationInZone(it)
                currentLocationZoneFirstTime = currentLocationZoneData
            }
        }
    }


    private fun createNavigationRoute(
        directionsResponse: DirectionsResponse,
        directionsRouteOptions: RouteOptions,
        selectedRouteIndex: Int?
    ) = NavigationRoute.create(directionsResponse, directionsRouteOptions)
        .find { navigationRoute -> navigationRoute.routeIndex == selectedRouteIndex }

    //BREEZE2-1845
//    private fun handleETALiveLocationToggleEvent() {
//        if (ETAEngine.getInstance()!!.isETARunning()) {
//            Analytics.logClickEvent(Event.ETA_RESUME, getScreenName())
//            showETAMessage(getString(R.string.eta_liveloc_started), false)
//        } else {
//            Analytics.logClickEvent(Event.ETA_PAUSE, getScreenName())
//            showETAMessage(getString(R.string.eta_liveloc_paused), true)
//        }
//        viewBinding.layoutNavBottom.ETAViewTurnByTurn.updateUIOnlyForETA()
//    }
//
//    private fun handleETAFirstTime() {
//        if (ETAEngine.getInstance()?.isETADisabled() == true) {
//            viewBinding.layoutNavBottom.ETAViewTurnByTurn.updateUIOnlyForETA()
//            viewBinding.layoutNavBottom.ETAViewTurnByTurn.ivEtaStatus?.setOnClickListener {
//                showShareLiveLocation()
//            }
//        }
//    }
//
    private fun showShareLiveLocation() {

        val name = when {
            !originalDestinationDetails.destinationName.isNullOrEmpty() -> {
                if (originalDestinationDetails.destinationName != Constants.TAGS.CURRENT_LOCATION_TAG) {
                    originalDestinationDetails.destinationName
                } else {
                    originalDestinationDetails.address1
                }
            }

            !originalDestinationDetails.address1.isNullOrEmpty() -> originalDestinationDetails.address1
            else -> ""
        }
        val navigationParams = getRNFragmentNavigationParams(
            sessionToken = myServiceInterceptor.getSessionToken(),
            fragmentTag = RNScreen.NOTIFY_ARRIVAL
        ).apply {
            putString(
                "etamessage",
                resources.getString(R.string.share_destination_message_template)
            )
            putString("username", breezeUserPreferenceUtil.retrieveUserName())
            putString("location", originalDestinationDetails.address1 ?: "")
            putString("eta", timeArrival)
            putBundle(
                "data", bundleOf(
                    "address1" to (originalDestinationDetails.address1 ?: ""),
                    "address2" to (originalDestinationDetails.address2 ?: ""),
                    "latitude" to (originalDestinationDetails.lat ?: ""),
                    "longitude" to (originalDestinationDetails.long ?: ""),
                    "amenityId" to (originalDestinationDetails.amenityId ?: ""),
                    "amenityType" to (originalDestinationDetails.amenityType ?: ""),
                    "name" to name,
                    "fullAddress" to (originalDestinationDetails.fullAddress ?: ""),
                    "arrivalTime" to (timeArrival),
                    "arrivalTimestamp" to (currentEstimatedTimeToArrival),
                    "type" to ShareDestinationType.NAVIGATION
                )
            )
            putString(Constants.RN_FROM_SCREEN, Screen.NAVIGATION)
        }

        Timber.e("showShareLiveLocation: $navigationParams")
        val initialProperties = Bundle().apply {
            putString(Constants.RN_TO_SCREEN, RNScreen.NOTIFY_ARRIVAL)
            putBundle(Constants.RN_CONSTANTS.NAVIGATION_PARAMS, navigationParams)
        }

        val reactNativeFragment = CustomReactFragment.newInstance(
            componentName = Constants.RN_CONSTANTS.COMPONENT_NAME,
            launchOptions = initialProperties,
            cleanOnDestroyed = false
        )

        supportFragmentManager.beginTransaction()
            .add(R.id.frBottomDetail, reactNativeFragment, RNScreen.NOTIFY_ARRIVAL)
            .commit()
    }

    private fun closeEtaPopUp() {
        supportFragmentManager.findFragmentByTag(RNScreen.NOTIFY_ARRIVAL)?.let {
            supportFragmentManager.beginTransaction().remove(it).commitNow()
        }
    }

//    private fun etaRNCAllback(data: HashMap<String, Any>) {
//        if (data.containsKey(Constants.TRIPETA.CONTACT_NAME) && data.containsKey(Constants.TRIPETA.CONTACT_NUMBER)) {
//            val etaRequest = ETARequest()
//            etaRequest.tripStartTime = Timestamp.now().seconds
//            etaRequest.type = Constants.TRIPETA.INIT
//            etaRequest.recipientName = data[Constants.TRIPETA.CONTACT_NAME].toString()
//            etaRequest.recipientNumber = data[Constants.TRIPETA.CONTACT_NUMBER].toString()
//            etaRequest.tripEtaFavouriteId =
//                (data[Constants.TRIPETA.TRIP_ETA_FAVOURITE_ID] as? Double)?.toInt()
//            etaRequest.message = resources.getString(
//                R.string.eta_message_template_to_send_messsage,
//                breezeUserPreferenceUtil.retrieveUserName(),
//                currentDestinationDetails.address1 ?: "",
//                timeArrival
//            )
//            etaRequest.destination = currentDestinationDetails.address1
//            etaRequest.arrivalTime = timeArrival
//            etaRequest.tripDestLat = currentDestinationDetails.lat?.toDouble()
//            etaRequest.tripDestLong = currentDestinationDetails.long?.toDouble()
//            etaRequest.tripEndTime =
//                etaRequest.tripStartTime?.plus(mRoute?.directionsRoute?.duration()?.toLong() ?: 0)
//            etaRequest.shareLiveLocation = Constants.TRIPETA.YES
//            etaRequest.voiceCallToRecipient = Constants.TRIPETA.NO
//            ETAEngine.updateETPRequest(etaRequest)
//            viewModel.startETA(etaRequest)
//        } else {
//            // Reset ETA
//            Analytics.logClickEvent(Event.RESET_MY_ETA, getScreenName())
//        }
//    }
//
//    private fun announceShareDriveStarted() {
//        if (!shareDriveAnnounced) {
//            shareDriveAnnounced = true
//            val message = getString(
//                R.string.eta_start_voice_message, ETAEngine.getInstance()?.getETARecipientName()
//            )
//            VoiceInstructionsManager.getInstance()?.playVoiceInstructions(message)
//        }
//    }

    private fun handleMuteToggleEvent() {
        if (VoiceInstructionsManager.getInstance()?.isUserMute == true) {
            Analytics.logClickEvent(Event.MUTE, getScreenName())
            viewBinding.toggleVoice.setImageResource(R.drawable.mute_on)
        } else {
            Analytics.logClickEvent(Event.UNMUTE, getScreenName())
            viewBinding.toggleVoice.setImageResource(R.drawable.mute_off)
        }
    }


    private fun triggerReroute() {
        Timber.d("Calendar event destination has changed")
        // Find routes
        if (getCurrentLocation() != null) {
            findRoute(
                Point.fromLngLat(getCurrentLocation()!!.longitude, getCurrentLocation()!!.latitude),
                Point.fromLngLat(
                    currentDestinationDetails.long!!.toDouble(),
                    currentDestinationDetails.lat!!.toDouble()
                )
            )
        }
    }

    private fun findRoute(originPoint: Point?, destination: Point?) {
        val profile: String = if (isWalking) {
            DirectionsCriteria.PROFILE_WALKING
        } else {
            DirectionsCriteria.PROFILE_DRIVING_TRAFFIC
        }

        val waypoint = navAmenityLayerManager?.amenityWayPoints?.map {
            Point.fromLngLat(
                it.long ?: 0.0, it.lat ?: 0.0
            )
        } ?: listOf()
        val routeOptions = RouteOptions.builder().applyDefaultNavigationOptions()
            .language(Constants.MAPBOX_ROUTE_OPTION_LOCALE)
            .coordinates(originPoint!!, waypoint, destination!!)
            .waypointIndicesList(
                listOf(
                    0, waypoint.size + 1
                )
            )
            .overview(DirectionsCriteria.OVERVIEW_FULL).profile(profile).annotationsList(
                arrayListOf(
                    DirectionsCriteria.ANNOTATION_DISTANCE, DirectionsCriteria.ANNOTATION_CONGESTION
                )
            ).alternatives(true).geometries(Constants.POLYLINE_GEOMETRY).voiceInstructions(true)
            .voiceUnits(DirectionsCriteria.METRIC).bannerInstructions(true) //this
            .steps(true).build()

        val client = MapboxDirections.builder().routeOptions(
            routeOptions
        ).accessToken(getString(R.string.mapbox_access_token)).build()

        client?.enqueueCall(object : retrofit2.Callback<DirectionsResponse> {

            override fun onResponse(
                call: retrofit2.Call<DirectionsResponse>,
                response: retrofit2.Response<DirectionsResponse>
            ) {
                if (response.body() == null) {
                    Timber.d("No routes found, make sure you set the right user and access token.")
                    return
                } else if (response.body()!!.routes().size < 1) {
                    Timber.d("No routes found")
                    return
                }

                if (response.body()!!.routes().isNotEmpty() && routeLineView != null) {
                    val currentRouteList = response.body()!!.routes()
                    Timber.d("Routes retrieved: " + currentRouteList.size)

                    //Populate Arrival View and add destination icon once routes found
                    populateArrivalView()
                    //Resetting maxdistance for ProgressBar to 0
                    maxDistance = 0

                    viewModel.sortedRouteObjects.observe(
                        this@TurnByTurnNavigationActivity,
                        Observer {
                            viewModel.sortedRouteObjects.removeObservers(this@TurnByTurnNavigationActivity)
                            val currentRouteObjects = it
                            val navigationRoute = createNavigationRoute(
                                response.body()!!,
                                routeOptions,
                                currentRouteObjects[0].route.routeIndex()?.toInt()
                            )!!
                            val iconToAdd = calculateDestinationIcon()
                            navAmenityLayerManager?.addDestinationIcon(
                                currentRouteObjects[0].route, iconToAdd
                            )
                            MapboxNavigationApp.current()
                                ?.setNavigationRoutes(listOf(navigationRoute))
                        })
                    viewModel.getSortedRoutes(currentRouteList, response.body()!!)
                }
            }

            override fun onFailure(call: retrofit2.Call<DirectionsResponse>, t: Throwable) {
                Timber.d("Error: " + t.message)
            }
        })
    }

    private fun updateTripProgressSection() {
        viewBinding.layoutNavBottom.btnNavigationEnd.run {
            layoutParams =
                layoutParams.apply { width = if (isArrived) MATCH_PARENT else WRAP_CONTENT }
        }
        viewBinding.layoutNavBottom.sectionTripProgress1.isVisible = !isArrived
        viewBinding.layoutNavBottom.sectionTripProgress2.isVisible = !isArrived
    }

    private fun showEndNavigationConfirmationDialog() {
        alertDialogHelper.showEndNavigationConfirmationDialog(getScreenName()) {
            viewBinding.mapView.location.apply {
                enabled = false
            }
            doEndNavigation()
        }
    }

    private fun initiateNavigationClose() {
        if (!isArrived) {
            Utils.isNavigationStarted = false
            Utils.phoneNavigationStartEventData = null
            postEventWithCacheLast(AppToNavigationEndEvent())
        }
        if (::breezeRoadObjectHandler.isInitialized) {
            viewBinding.mapView.getMapboxMap().getStyle { style ->
                breezeRoadObjectHandler.removeLastNotification(
                    style, breezeNotificationObserver
                )
            }

        }
        stopNavigation(isArrived)

        MapboxNavigationApp.current()?.let { mapboxNavigation ->
            mapboxNavigation.unregisterLocationObserver(locationObserver)
            mapboxNavigation.unregisterRoutesObserver(routesObserver)
            mapboxNavigation.unregisterRouteProgressObserver(routeProgressObserver)
            mapboxNavigation.unregisterLocationObserver(mapMatcherResultObserver)
            SpeedLimitUtil.removeLocationObserver(mapboxNavigation)
            /**
             * unregister voice instruction
             */
            VoiceInstructionsManager.getInstance()?.unRegisterVoiceInstruction()
            if (::mapboxHistoryRecorder.isInitialized) {
                mapboxNavigation.unregisterTripSessionStateObserver(mapboxHistoryRecorder)
            }
        }

        //ToDo - Cross check the logic here //ANKUR
        //BREEZE2-1845
//        ETAEngine.getInstance()?.getEtaLocationUpdateInstance(ETAMode.Navigation)?.let {
//            LocationBreezeManager.getInstance().removeLocationCallBack(it)
//        }

        /**
         * remove location update
         */
        if (isWalking) {
            WalkathonEngine.getInstance()?.stopCheckingLocationForWalkathon()
        }
        compositeDisposable.dispose()
        Timber.d("onDestroy() of Navigation before super")

    }

    private fun showWalkingPathDialog() {
        if (baseContext == null) return

        if (!lifecycle.currentState.isAtLeast(Lifecycle.State.RESUMED)) {
            showWalkingPathDialogOnResume = true
            return
        }

        showWalkingPathDialogOnResume = false

        lifecycleScope.launchWhenResumed {
            val navigationParams = getRNFragmentNavigationParams(
                sessionToken = myServiceInterceptor.getSessionToken(),
                fragmentTag = RNScreen.PARKING_AVAILABLE_FEEDBACK_COMPLETE
            )
            val initialProperties = Bundle().apply {
                putString(Constants.RN_TO_SCREEN, RNScreen.PARKING_AVAILABLE_FEEDBACK_COMPLETE)
                putBundle(Constants.RN_CONSTANTS.NAVIGATION_PARAMS, navigationParams)
            }
            val reactNativeFragment = CustomReactFragment.newInstance(
                componentName = Constants.RN_CONSTANTS.COMPONENT_NAME,
                launchOptions = initialProperties,
                cleanOnDestroyed = false
            )
            supportFragmentManager.beginTransaction()
                .add(
                    R.id.layoutForCarparkAvailability,
                    reactNativeFragment,
                    RNScreen.PARKING_AVAILABLE_FEEDBACK_COMPLETE
                )
                .addToBackStack(RNScreen.PARKING_AVAILABLE_FEEDBACK_COMPLETE).commit()
        }
    }


    @OptIn(ExperimentalPreviewMapboxNavigationAPI::class)
    fun startWalkingNavigation() {
        isArrived = false
        isWalking = true

        closeCarparkListScreenRN()
        viewBinding.layoutNavBottom.layoutShareAndCarpark.isVisible = false
        viewBinding.llTopButtons.isVisible = false
        //BREEZE2-1845
//        viewBinding.layoutNavBottom.ETAViewTurnByTurn.visibility = View.GONE
        BreezeEHorizonProvider.destroy()
        SpeedLimitUtil.setCurrentSpeedLimit(-1)
        viewBinding.layoutSpeedLimit.root.visibility = View.GONE
        viewBinding.toggleVoice.visibility = View.GONE
        VoiceInstructionsManager.getInstance()?.run {
            muteVoiceInstructions()
            isWalkingPath = true
            unRegisterVoiceInstruction()
        }
        BreezeArrivalObserver.getInstance()
            ?.registerNavigationArrivalObserver(navigationArrivalObserver)


        /**
         * start publish location realtime
         */
        WalkathonEngine.getInstance()?.startCheckingLocationForWalkathon()

        resetArrivalIconAndMessage()

        viewBinding.layoutNavBottom.layoutTravelInfo.visibility = View.VISIBLE
        viewBinding.layoutNavBottom.tvTurnStreetName.visibility = View.INVISIBLE

        updateTripProgressSection()

        viewBinding.customManeuverView.visibility = View.GONE
        viewBinding.mapView.location.apply {
            enabled = true
        }

        viewBinding.mapView.location.apply {
            this.locationPuck = LocationPuck2D(
                bearingImage = ContextCompat.getDrawable(
                    this@TurnByTurnNavigationActivity, R.drawable.cursor_with_layer
                    //R.drawable.cursor
                ), scaleExpression = interpolate {
                    linear()
                    zoom()
                    stop {
                        literal(0.0)
                        literal(0.6)
                    }
                    stop {
                        literal(20.0)
                        literal(1.0)
                    }
                }.toJson()
            ) //AAL
            pulsingEnabled = true
            pulsingColor = getColor(R.color.themed_nav_pulse_color)
            pulsingMaxRadius = 10.0F
            enabled = true
        }

        val newRoute = walkingRoute
        initNavigation()
        initNavigationObservers(newRoute!!)
        initRouteLine(newRoute)
        initRouteLineAPI(newRoute)
        startTrip()
        initWalkingRouteLine()
        handleRouteLineRefresh()
    }

    fun getDialogText(input: Float): String {
        when {
            input > 4 -> Analytics.logClickEvent(Event.POP_STAR_5, getScreenName())
            input > 3 -> Analytics.logClickEvent(Event.POP_STAR_4, getScreenName())
            input > 2 -> Analytics.logClickEvent(Event.POP_STAR_3, getScreenName())
            input > 1 -> Analytics.logClickEvent(Event.POP_STAR_2, getScreenName())
            else -> Analytics.logClickEvent(Event.POP_STAR_1, getScreenName())
        }
        return when {
            input > 4 -> resources.getString(R.string.rating_excellent)
            input > 3 -> resources.getString(R.string.rating_very_good)
            input > 2 -> resources.getString(R.string.rating_good)
            input > 1 -> resources.getString(R.string.rating_poor)
            else -> resources.getString(R.string.rating_very_poor)
        }
    }


    private fun showETAMessage(message: String, showPausedImage: Boolean = false) {
        viewBinding.etaToast.visibility = View.VISIBLE
        val icon = if (showPausedImage) {
            R.drawable.eta_toast_live_location_paused
        } else {
            R.drawable.eta_toast_live_location
        }
        viewBinding.etaToast.setData(message, icon)
        viewBinding.etaToast.show()
    }

    /**
     * show custom toast with icon and a message
     * @param duration duration in seconds for toast to be shown. Default 5s
     * */
    fun showCustomToast(@DrawableRes icon: Int, message: String, duration: Int = 5) {
        lifecycleScope.launch(Dispatchers.Main) {
            if (lifecycle.currentState.isAtLeast(Lifecycle.State.RESUMED)) {
                GlobalToast().setDescription(message)
                    .setIcon(icon)
                    .setDismissDurationSeconds(duration)
                    .show(this@TurnByTurnNavigationActivity)
            }
        }
    }

    override fun onDestroy() {
        Timber.d("onDestroy() of Navigation is called")
        VoiceInstructionsManager.getInstance()?.isWalkingPath = false
        navAmenityLayerManager?.destroy()
        //BREEZE2-1845
//        ETAEngine.getInstance()?.gc()
        WalkathonEngine.getInstance()?.stopCheckingLocationForWalkathon()
        super.onDestroy()
        removeAmenitiesHandler?.removeCallbacksAndMessages(null)
        removeAmenitiesHandler = null
        autoRecenterMapHandler?.removeCallbacksAndMessages(null)
        autoRecenterMapHandler = null
        lifecycleScope.cancel()
        obuCheckSpeedHelper?.destroy()
        obuCheckSpeedHelper = null
    }

    private fun init(args: Bundle, skipDriving: Boolean) {
        Timber.d("Calling Mapbox load style")
        viewBinding.mapView.getMapboxMap()
            .takeIf { ::breezeRoadObjectHandler.isInitialized }
            ?.getStyle {
                breezeRoadObjectHandler.removeBreezeNotificationStyle(it)
            }

        initStyle(args) { route ->
            Timber.d("After Mapbox style loaded: Initialisation of route line and views is triggered")

            initWalkingRouteLine()
            if (skipDriving && walkingRoute != null) {
                forceRerouteWalkingPath()
                startWalkingNavigation()
            } else {
                initRouteLine(route)
                initRouteLineAPI(route)

                // ERP Handling --> Computing route erp info and displaying ERP icons
                computeRouteERPs()
                erpRefreshListener()
                walkingPathRequestListener()
                registerEhorizonNotificationObservers(
                    viewBinding.mapView.getMapboxMap().getStyle()!!, route
                )
                Timber.d("Trip started in Navigation")
                startTrip()
//                if (!currentDestinationDetails.isDestinationCarPark()) {//BREEZE2-1545
//                    renderDestinationNearbyCarParks(false)
//                } else {
//                    if (currentDestinationDetails.availablePercentImageBand != AmenityBand.TYPE2.type) {
//                        renderDestinationNearbyCarParks(false)
//                    }
//                }

                removeAmenitiesHandler?.removeMessages(WHAT_CHECK_DISTANCE_TO_DESTINATION)
                removeAmenitiesHandler?.sendEmptyMessageDelayed(
                    WHAT_CHECK_DISTANCE_TO_DESTINATION,
                    2000
                )
            }

            listenOBUEvents()
        }

    }

    private fun findWalkingPath(startingPoint: Point, endingPoint: Point) {
        viewModel.walkingRoutesRetrieved.observe(this, Observer { route ->
            viewModel.walkingRoutesRetrieved.removeObservers(this)
            walkingRoute = route
            initWalkingRouteLine()
        })
        viewModel.findWalkingRoutes(
            startingPoint, endingPoint, originalDestinationDetails!!
        )
    }

    private fun forceRerouteWalkingPath() {
        if (walkingRoute != null) {
            val currentLocation = LocationBreezeManager.getInstance().currentLocation
            val destination = Point.fromLngLat(
                originalDestinationDetails.long!!.safeDouble(),
                originalDestinationDetails.lat!!.safeDouble()
            )

            //val routeOptionCoordinates = walkingRoute!!.routeOptions.coordinatesList()
            //if(currentLocation != null && routeOptionCoordinates.size>0) {

            if (currentLocation != null && destination != null) {
                findWalkingPath(
                    startingPoint = currentLocation.toPoint(), endingPoint = destination
                )
            }
        }
    }

    private fun initWalkingRouteLine() {
        walkingRoute?.let { route ->
            if (isWalking) {
                navAmenityLayerManager?.removeWalkingRouteLine()
            } else {
                navAmenityLayerManager?.addWalkingRouteLine(route)
            }
        }
    }

    private fun renderNearbyCarParks(announceWarning: Boolean) {
        val locationPoint = currentLocationMatcherResult?.enhancedLocation?.toPoint() ?: return
//        val locationPoint = Point.fromLngLat(// BREEZE2-1545
//            originalDestinationDetails.long!!.toDouble(),
//            originalDestinationDetails.lat!!.toDouble()
//        )
        CoroutineScope(Dispatchers.IO).launch {
            navAmenityLayerManager?.fetchAndHandleCarParks(
                locationPoint,
                currentDestinationDetails.destinationName,
                currentDestinationDetails.isCarPark,
                announceWarning
            )
        }
    }

    private fun listenOBUEvents() {
        Timber.d("OBU Events listener is initialized")
        compositeDisposable.add(
            RxBus.listen(RxEvent.OBURoadEvent::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    showRoadEventAlert(it.eventData)
                })
        compositeDisposable.add(RxBus.listen(RxEvent.OBULowCardBalanceEvent::class.java)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                showOBULowCardAlertMessage(it.balance / 100.0)
            }
        )
        compositeDisposable.add(
            RxBus.listen(RxEvent.OBUParkingAvailabilityEvent::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { event ->
                    AnalyticsUtils.sendRoadEventParkingAvailability(
                        Event.USER_POPUP,
                        Event.VALUE_POPUP_OPEN,
                        viewBinding.layoutNavBottom.tvTurnStreetName.text.toString(),
                        currentUserRawLocation?.latitude ?: 0.0,
                        currentUserRawLocation?.longitude ?: 0.0,
                        Screen.NAVIGATION
                    )
                    val showAlert =
                        BreezeUserPreference.getInstance(this).getParkingAvailabilityDisplay()
                    if (showAlert) {
                        closeCarparkListScreenRN()
                        var backgroundColor: Int = 0
                        var iconResourceId: Int = 0
                        val title = getString(R.string.obu_alert_message_carpark_update_title)
                        var detail: String = ""
                        val isAllAvailable =
                            event.data.count { it.color == "green" } == event.data.size
                        val isAllFull =
                            event.data.count { it.availableLots == "0" } == event.data.size
                        val isLimitedLotsAvailable =
                            event.data.count {
                                it.color == "red" || it.color == "yellow"
                            } >= 3
                                    || event.data.count {
                                it.color == "red" || it.color == "yellow"
                            } == event.data.size
                        val isFillingUpFastWithAvailableLots =
                            isLimitedLotsAvailable && event.data.any { it.color == "green" }
                        val isNearbyStillAvailable =
                            !isLimitedLotsAvailable && event.data.any { it.color == "green" }
                        when {
                            isAllAvailable -> {
                                detail =
                                    getString(R.string.obu_alert_message_carpark_update_content_nearby_all_available)
                                iconResourceId =
                                    R.drawable.ic_obu_alert_message_carpark_update_green
                                backgroundColor = Color.parseColor("#15B765")
                            }

                            isAllFull -> {
                                detail =
                                    getString(R.string.obu_alert_message_carpark_update_content_nearby_all_full)
                                iconResourceId = R.drawable.ic_obu_alert_message_carpark_update_red
                                backgroundColor = Color.parseColor("#E82370")
                            }

                            isNearbyStillAvailable -> {
                                detail =
                                    getString(R.string.obu_alert_message_carpark_update_content_nearby_available)
                                iconResourceId =
                                    R.drawable.ic_obu_alert_message_carpark_update_green
                                backgroundColor = Color.parseColor("#15B765")
                            }

                            else -> {
                                detail =
                                    getString(R.string.obu_alert_message_carpark_update_content_filling_up_fast)
                                iconResourceId =
                                    R.drawable.ic_obu_alert_message_carpark_update_yellow
                                backgroundColor = Color.parseColor("#F26415")
                            }
                        }
                        viewBinding.obuAlertMessageViewWithActions.setListener(object :
                            OBUAlertMessageViewWithActions.OBUAlertMessageViewListener {
                            override fun onReadMoreSelected() {
                                showOBUCarparkAvailability(event.data)
                            }
                        })
                        viewBinding.obuAlertMessageViewWithActions.setContent(
                            OBUAlertMessageViewWithActions.OBUAlertMessageType.CAR_PARK_UPDATE,
                            backgroundColor,
                            iconResourceId,
                            title,
                            detail
                        )
                        viewBinding.obuAlertMessageViewWithActions.show {
                            Analytics.logPopupEvent(
                                Event.AA_POPUP_NAVIGATION,
                                Event.VALUE_POPUP_CLOSE,
                                Screen.NAVIGATION
                            )
                        }
                    }
                })
        compositeDisposable.add(
            RxBus.listen(RxEvent.OBUTravelTimeEvent::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    val showAlert =
                        BreezeUserPreference.getInstance(this).getEstimatedTravelTimeDisplay()
                    if (showAlert) {
                        AnalyticsUtils.sendRoadEventTravelTime(
                            Event.USER_POPUP,
                            Event.VALUE_POPUP_OPEN,
                            viewBinding.layoutNavBottom.tvTurnStreetName.text.toString(),
                            currentUserRawLocation?.latitude ?: 0.0,
                            currentUserRawLocation?.longitude ?: 0.0,
                            Screen.NAVIGATION
                        )
                        closeCarparkListScreenRN()
                        val backgroundColor = Color.parseColor("#E82370")
                        val iconResourceId = R.drawable.ic_obu_alert_message_travel_time
                        val title = getString(R.string.obu_alert_message_travel_time_title)
                        viewBinding.obuAlertMessageViewWithActions.setListener(object :
                            OBUAlertMessageViewWithActions.OBUAlertMessageViewListener {
                            override fun onReadMoreSelected() {
                                showOBUTravelTime(it.data)
                            }
                        })
                        viewBinding.obuAlertMessageViewWithActions.setContent(
                            OBUAlertMessageViewWithActions.OBUAlertMessageType.TRAVEL_TIME,
                            backgroundColor,
                            iconResourceId,
                            title,
                            ""
                        )
                        viewBinding.obuAlertMessageViewWithActions.show {
                            Analytics.logPopupEvent(
                                Event.AA_POPUP_NAVIGATION,
                                Event.VALUE_POPUP_CLOSE,
                                Screen.NAVIGATION
                            )
                        }
                    }
                })

        compositeDisposable.add(
            RxBus.listen(RxEvent.CancelOBUTravelTimeEvent::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    viewBinding.navigationFrameContainer.isVisible = false
                }
        )

        compositeDisposable.add(
            RxBus.listen(RxEvent.OBUVelocityEvent::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    Timber.d("OBUVelocityEvent: speedLimit = ${it.speedLimit}, velocity = ${it.velocityMS}")
                    TripLoggingManager.getInstance()?.checkIdle(it.velocityMS * 3.6)
                }
        )

        compositeDisposable.add(
            RxBus.listen(RxEvent.OBUCardInfoEvent::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    Timber.d("OBUCardInfoEvent: ${it.data}")
                    updateCardBalanceViews(
                        OBUDataHelper.getOBUCardBalanceSGD() ?: 0.0,
                        OBUDataHelper.isShowBankCard()
                    )
                }
        )
    }

    private fun showRoadEventAlert(eventData: OBURoadEventData) {
        AnalyticsUtils.sendRoadEvent(
            eventData,
            viewBinding.layoutNavBottom.tvTurnStreetName.text.toString(),
            currentUserRawLocation?.latitude ?: 0.0,
            currentUserRawLocation?.longitude ?: 0.0,
            Screen.NAVIGATION,
            Event.OBU_DISPLAY_NOTIFICATION
        )

        var showAlert = true
        var backgroundColor: Int = 0
        var iconResourceId: Int = 0
        var title: String = ""
        var detail: String = ""
        var fee: String = ""
        var statusIndicator: Int = 0
        when (eventData.eventType) {
            OBURoadEventData.EVENT_TYPE_ERP_CHARGING -> {
                showAlert = false
                viewBinding.queueAlertView.addOBUAlertERPView(eventData)

            }

            OBURoadEventData.EVENT_TYPE_ERP_SUCCESS -> {
                showAlert = false
                delayCheckShowingOBULowCardAlertMessage()
                viewBinding.queueAlertView.addOBUAlertERPView(eventData)
            }

            OBURoadEventData.EVENT_TYPE_ERP_FAILURE -> {
                showAlert = false
                viewBinding.queueAlertView.addOBUAlertERPView(eventData)
            }

            OBURoadEventData.EVENT_TYPE_PARKING_FEE -> {
                backgroundColor = Color.parseColor("#782EB1")
                iconResourceId = R.drawable.ic_obu_alert_message_parking_success
                title = eventData.message ?: ""
                fee = eventData.chargeAmount ?: ""
            }

            OBURoadEventData.EVENT_TYPE_PARKING_SUCCESS -> {
                delayCheckShowingOBULowCardAlertMessage()
                backgroundColor = Color.parseColor("#782EB1")
                iconResourceId = R.drawable.ic_obu_alert_message_parking_success
                title = eventData.message ?: ""
                fee = eventData.chargeAmount ?: ""
                statusIndicator =
                    R.drawable.ic_obu_alert_message_status_indicator_success
            }

            OBURoadEventData.EVENT_TYPE_PARKING_FAILURE -> {
                backgroundColor = Color.parseColor("#E82370")
                iconResourceId = R.drawable.ic_obu_alert_message_parking_failure
                title = eventData.message ?: ""
                fee = eventData.chargeAmount ?: ""
                statusIndicator =
                    R.drawable.ic_obu_alert_message_status_indicator_failure
            }

            OBURoadEventData.EVENT_TYPE_SCHOOL_ZONE -> {
                showAlert = false
                if (getBreezeUserPreference().getSchoolZoneDisplay()) {
                    showOBUNotificationView(
                        NavigationZone.SCHOOL_ZONE.priority,
                        eventData.distance
                    )
                }
            }

            OBURoadEventData.EVENT_TYPE_TRAFFIC -> {
                showAlert = false
                showOBUNotificationView(
                    NavigationZone.ACCIDENT.priority,
                    eventData.distance
                )
            }

            OBURoadEventData.EVENT_TYPE_SILVER_ZONE -> {
                showAlert = false
                if (getBreezeUserPreference().getSilverZoneDisplay()) {
                    showOBUNotificationView(
                        NavigationZone.SILVER_ZONE.priority,
                        eventData.distance
                    )
                }
            }

            OBURoadEventData.EVENT_TYPE_EVENT,
            OBURoadEventData.EVENT_TYPE_ROAD_CLOSURE -> {
                showAlert = false
                showOBUNotificationView(
                    NavigationZone.ROAD_CLOSURE.priority,
                    eventData.message,
                    eventData.run {
                        detailTitle?.let { detail ->
                            "$detail $detailMessage"
                        } ?: detailMessage ?: ""
                    }
                )
            }

            OBURoadEventData.EVENT_TYPE_ROAD_DIVERSION -> {
                showAlert = false
                showOBUNotificationView(
                    NavigationZone.ROAD_DIVERSION.priority,
                    eventData.message,
                    eventData.detailMessage ?: ""
                )
            }

            OBURoadEventData.EVENT_TYPE_FLASH_FLOOD -> {
                showAlert = false
                showOBUNotificationView(
                    NavigationZone.FLASH_FLOOD.priority,
                    eventData.distance,
                    eventData.detailMessage ?: ""
                )
            }

            OBURoadEventData.EVENT_TYPE_HEAVY_TRAFFIC -> {
                showAlert = false
                showOBUNotificationView(
                    NavigationZone.HEAVY_TRAFFIC.priority,
                    eventData.distance,
                    eventData.detailMessage ?: ""
                )
            }

            OBURoadEventData.EVENT_TYPE_UNATTENDED_VEHICLE -> {
                showAlert = false
                showOBUNotificationView(
                    NavigationZone.UNATTENDED_VEHICLE.priority,
                    eventData.distance,
                    eventData.detailMessage ?: ""
                )
            }

            OBURoadEventData.EVENT_TYPE_MISCELLANEOUS -> {
                showAlert = false
                showOBUNotificationView(
                    NavigationZone.MISCELLANEOUS.priority,
                    eventData.distance,
                    eventData.detailMessage ?: ""
                )
            }

            OBURoadEventData.EVENT_TYPE_ROAD_BLOCK -> {
                showAlert = false
                showOBUNotificationView(
                    NavigationZone.ROAD_BLOCK.priority,
                    eventData.distance,
                    eventData.detailMessage ?: ""
                )
            }

            OBURoadEventData.EVENT_TYPE_OBSTACLE -> {
                showAlert = false
                showOBUNotificationView(
                    NavigationZone.OBSTACLE.priority,
                    eventData.distance,
                    eventData.detailMessage ?: ""
                )
            }

            OBURoadEventData.EVENT_TYPE_ROAD_WORK -> {
                showAlert = false
                showOBUNotificationView(
                    NavigationZone.ROAD_WORK.priority,
                    eventData.distance,
                    eventData.detailMessage ?: ""
                )
            }

            OBURoadEventData.EVENT_TYPE_VEHICLE_BREAKDOWN -> {
                showAlert = false
                showOBUNotificationView(
                    NavigationZone.VEHICLE_BREAKDOWN.priority,
                    eventData.distance,
                    eventData.detailMessage ?: ""
                )
            }

            OBURoadEventData.EVENT_TYPE_MAJOR_ACCIDENT -> {
                showAlert = false
                showOBUNotificationView(
                    NavigationZone.MAJOR_ACCIDENT.priority,
                    eventData.distance,
                    eventData.detailMessage ?: ""
                )
            }

            OBURoadEventData.EVENT_TYPE_SEASON_PARKING -> {
                showAlert = false
                showOBUNotificationView(
                    NavigationZone.SEASON_PARKING.priority,
                    eventData.distance,
                    eventData.detailMessage ?: ""
                )
            }

            OBURoadEventData.EVENT_TYPE_TREE_PRUNING -> {
                showAlert = false
                showOBUNotificationView(
                    NavigationZone.TREE_PRUNING.priority,
                    eventData.distance,
                    eventData.detailMessage ?: ""
                )
            }

            OBURoadEventData.EVENT_TYPE_YELLOW_DENGUE_ZONE -> {
                showAlert = false
                showOBUNotificationView(
                    NavigationZone.YELLOW_DENGUE_ZONE.priority,
                    eventData.distance,
                    eventData.detailMessage ?: ""
                )
            }

            OBURoadEventData.EVENT_TYPE_RED_DENGUE_ZONE -> {
                showAlert = false
                showOBUNotificationView(
                    NavigationZone.RED_DENGUE_ZONE.priority,
                    eventData.distance,
                    eventData.detailMessage ?: ""
                )
            }

            OBURoadEventData.EVENT_TYPE_SPEED_CAMERA -> {
                showAlert = false
                showOBUNotificationView(
                    NavigationZone.SPEED_CAMERA.priority,
                    eventData.distance
                )
            }

            OBURoadEventData.EVENT_TYPE_GENERAL_MESSAGE -> {
                showAlert = false
                showOBUAlertMessageGeneral(
                    eventData.message ?: "",
                    eventData.detailMessage ?: ""
                )
            }

            OBURoadEventData.EVENT_TYPE_BUS_LANE -> {
                showAlert = false
                if (getBreezeUserPreference().getBusLaneDisplay()) {
                    showOBUNotificationView(
                        NavigationZone.BUS_LANE.priority,
                        eventData.message ?: "",
                        eventData.detailMessage ?: "",
                        10000
                    )
                }
            }

            else -> {
                showAlert = false
            }
        }
        if (showAlert) {
            showOBUAlertMessage(
                backgroundColor,
                iconResourceId,
                title,
                detail,
                fee,
                statusIndicator
            )
        }
    }


    private fun delayCheckShowingOBULowCardAlertMessage() {
        lifecycleScope.launch(Dispatchers.IO) {
            // fixme: temporarily add 1 second to make sure low card alerts are shown
            delay(OBUConstants.OBU_LOW_CARD_ALERT_DELAY + 1000)
            OBUDataHelper.getOBUCardBalanceSGD()
                ?.takeIf { isActive && it < OBUConstants.OBU_LOW_CARD_THRESHOLD / 100.0 }
                ?.let {
                    showOBULowCardAlertMessage(it)
                }
        }
    }

    private fun showOBULowCardAlertMessage(balanceSGD: Double) {
        if (lifecycle.currentState.isAtLeast(Lifecycle.State.RESUMED)
            && !viewBinding.layoutManeuverList.fragmentCarConnectedWarningContainer.isVisible
        ) {
            lifecycleScope.launch(Dispatchers.Main) {
                showOBUAlertMessage(
                    Color.parseColor("#E82370"),
                    R.drawable.ic_navigation_obu_connection_error_explain,
                    "Low Card Balance",
                    "",
                    "$${balanceSGD.round(2)}",
                    R.drawable.ic_obu_alert_message_status_indicator_failure
                )
            }
        }
    }

    private fun showOBUCarparkAvailability(data: Array<OBUParkingAvailability>) {
        AnalyticsUtils.sendRoadEventParkingAvailability(
            Event.USER_CLICK,
            Event.POPUP_NAVIGATION_NOTIFICATION_CLICK_NOTIFICATION,
            viewBinding.layoutNavBottom.tvTurnStreetName.text.toString(),
            currentUserRawLocation?.latitude ?: 0.0,
            currentUserRawLocation?.longitude ?: 0.0,
            Screen.NAVIGATION
        )
        val navigationParams = getRNFragmentNavigationParams(
            sessionToken = myServiceInterceptor.getSessionToken(),
            fragmentTag = RNScreen.OBU_CARPARK_AVAILABILITY
        ).apply {
            putParcelableArray("carparks", data.map { it.toBundle() }.toTypedArray())
        }
        val initialProperties = Bundle().apply {
            putString(Constants.RN_TO_SCREEN, RNScreen.OBU_CARPARK_AVAILABILITY)
            putBundle(Constants.RN_CONSTANTS.NAVIGATION_PARAMS, navigationParams)
        }

        val reactNativeFragment = CustomReactFragment.newInstance(
            componentName = Constants.RN_CONSTANTS.COMPONENT_NAME,
            launchOptions = initialProperties,
            cleanOnDestroyed = false
        )

        supportFragmentManager
            .beginTransaction()
            .replace(
                R.id.navigation_frame_container,
                reactNativeFragment,
                RNScreen.OBU_CARPARK_AVAILABILITY
            )
            .addToBackStack(RNScreen.OBU_CARPARK_AVAILABILITY)
            .commit()
        viewBinding.navigationFrameContainer.isVisible = true
    }

    private fun showOBUTravelTime(data: Array<OBUTravelTimeData>) {
        AnalyticsUtils.sendRoadEventTravelTime(
            Event.USER_CLICK,
            Event.POPUP_NAVIGATION_NOTIFICATION_CLICK_NOTIFICATION,
            viewBinding.layoutNavBottom.tvTurnStreetName.text.toString(),
            currentUserRawLocation?.latitude ?: 0.0,
            currentUserRawLocation?.longitude ?: 0.0,
            Screen.NAVIGATION
        )
        val navigationParams = getRNFragmentNavigationParams(
            sessionToken = myServiceInterceptor.getSessionToken(),
            fragmentTag = RNScreen.OBU_TRAVEL_TIME
        ).apply {

            putParcelableArray("data", data.map { it.toBundle() }.toTypedArray())
        }
        val initialProperties = Bundle().apply {
            putString(Constants.RN_TO_SCREEN, RNScreen.OBU_TRAVEL_TIME)
            putBundle(Constants.RN_CONSTANTS.NAVIGATION_PARAMS, navigationParams)
        }

        val reactNativeFragment = CustomReactFragment.newInstance(
            componentName = Constants.RN_CONSTANTS.COMPONENT_NAME,
            launchOptions = initialProperties,
            cleanOnDestroyed = false
        )

        supportFragmentManager
            .beginTransaction()
            .replace(
                R.id.navigation_frame_container,
                reactNativeFragment,
                RNScreen.OBU_CARPARK_AVAILABILITY
            )
            .addToBackStack(RNScreen.OBU_TRAVEL_TIME)
            .commit()
        viewBinding.navigationFrameContainer.isVisible = true
    }

    private fun showOBUAlertMessage(
        backgroundColor: Int,
        iconResourceId: Int,
        title: String,
        detail: String,
        fee: String,
        statusIndicator: Int
    ) {
        closeCarparkListScreenRN()
        viewBinding.obuAlertMessageView.setContent(
            backgroundColor,
            iconResourceId,
            title,
            detail,
            fee,
            statusIndicator
        )
        viewBinding.obuAlertMessageView.show {
            Analytics.logPopupEvent(
                Event.AA_POPUP_NAVIGATION,
                Event.VALUE_POPUP_CLOSE,
                Screen.NAVIGATION
            )
        }
    }


    @SuppressLint("MissingPermission")
    fun startTrip() {
        if (!CarNavigationScreenState.canStartNavigation) return
        TripLoggingManager.setupTripLogging(
            this.applicationContext, breezeTripLoggingUtil, breezeStatisticsUtil
        )
        viewModel.routablePointResultEvent.observe(this, Observer {
            mapboxHistoryRecorder = HistoryRecorder(
//                navigation = MapboxNavigationApp.current(),
//                firebaseUploader = ServiceLocator.firebaseUploader,
//                searchAnalyticsData = it.searchAnalyticsData,
//                breezeHistoryListener = this
            ).apply {
                currentUserId =
                    userPreferenceUtil.getUserStoredData()?.cognitoID?.takeIf { it.isNotEmpty() }
                        ?: "unkown_user"
                navigationMode = if (isWalking) "Walking" else "NavigationMode"
            }

//            turnbyturnViewModel.sendSecondaryFirebaseID(userPreferenceUtil.retrieveUserName())

            MapboxNavigationApp.current()?.let { mapboxNavigation ->
                mapboxNavigation.registerTripSessionStateObserver(mapboxHistoryRecorder)
                if (mapboxNavigation.getTripSessionState() != TripSessionState.STARTED) {
                    ClientFactory.clearMutationCache()
                    mapboxNavigation.startTripSession()
                    startService(Intent(this, NavigationControllerService::class.java))
                }
            }


            TripLoggingManager.getInstance()?.startTripLogging(
                tripType = TripType.NAVIGATION,
                screenName = getScreenName(),
                destinationAddressDetails = currentDestinationDetails,
                isWalking = isWalking
            )
            curTripId = breezeTripLoggingUtil.getCurrentTripID()
        })

        if (!currentDestinationDetails.lat.isNullOrBlank() && !currentDestinationDetails.long.isNullOrBlank()) {
            viewModel.createSearchFeedback(
                currentDestinationDetails.address1 ?: "",
                currentDestinationDetails.lat.safeDouble(),
                currentDestinationDetails.long.safeDouble()
            )
        }
    }

    private fun getDestinationName(): String {
        return currentDestinationDetails.destinationName ?: currentDestinationDetails.address1
        ?: currentDestinationDetails.address2 ?: "your destination"
    }

    private fun initRouteLine(mRoute: NavigationRoute) {
        var routeLineBelowLayerID = LocationComponentConstants.LOCATION_INDICATOR_LAYER
        if (viewBinding.mapView.getMapboxMap().getStyle()
                ?.getLayer(Constants.SPEED_CAMERA) != null
        ) {
            routeLineBelowLayerID = Constants.SPEED_CAMERA
        }
        val iconToAdd = calculateDestinationIcon()
        navAmenityLayerManager?.addDestinationIcon(mRoute.directionsRoute, iconToAdd)

        val routeLineResources: RouteLineResources
        val mapStyle = BreezeNavigationMapStyle()
        if (isWalking) {
            routeLineResources = mapStyle.getWalkingRouteLineResources(resources)
        } else {
            routeLineResources = mapStyle.getRouteLineResources(resources)
        }
        val mapboxRouteLineOptions =
            MapboxRouteLineOptions.Builder(this).withRouteLineBelowLayerId(routeLineBelowLayerID)
                .withRouteLineResources(routeLineResources!!).withVanishingRouteLineEnabled(true)
                //.displaySoftGradientForTraffic(true)
                .build()
        routeLineAPI = MapboxRouteLineApi(mapboxRouteLineOptions)
        routeLineView = MapboxRouteLineView(mapboxRouteLineOptions)

        val routeArrowOptions = RouteArrowOptions.Builder(this).build()
        routeArrowView = MapboxRouteArrowView(routeArrowOptions)

    }

    private fun calculateDestinationIcon(): Int {
        if (walkingRoute != null && !isWalking) {
            if (selectedCarParkIconRes != -1) {
                return selectedCarParkIconRes
            }
            return R.drawable.parking_selected
        }
        return R.drawable.destination_puck
    }

    private fun initViews() {
        updateTripProgressSection()
        with(viewBinding.layoutNavBottom) {
            btnNavigationEnd.setOnClickListener { handleEndButtonClick() }
            btnConnectOBU.setOnClickListener {
                shouldShowOBUConnectedMessage = true
                getApp()?.obuConnectionHelper?.navigationConnectLastOBUDevice()
            }
            supportFragmentManager.beginTransaction()
                .replace(viewBinding.containerCarparkList.id, TBTCarParkListRNFragment())
                .commit()
            buttonShowCarParkList.setOnClickListener {
                showCarparkListScreenRN()
                Analytics.logClickEvent(
                    eventValue = ":show_destination_carpark_list",
                    Screen.NAVIGATION
                )
            }

            btnShareDestination.setOnClickListener {
                shareDestination()
            }
        }
        initOBUInfoView()
        initMapAmenityToggleView()
    }

    private fun shareDestination() {
        Analytics.logClickEvent(Event.SHARE_DESTINATION, Screen.NAVIGATION)
        showShareLiveLocation()
    }

    private fun showCarparkListScreenRN() {
        (supportFragmentManager.findFragmentById(viewBinding.containerCarparkList.id) as? TBTCarParkListRNFragment)
            ?.showCarParkListRNScreen(currentEstimatedTimeToArrival / 1000L)
    }

    private fun closeCarparkListScreenRN() {
        RxBus.publish(RxEvent.CloseCarparkListScreen())
    }

    private fun initMapAmenityToggleView() {
        viewBinding.toggleMapAmenity.setOnDisplayModeChanged(::onDisplayModeChanged)
        viewBinding.toggleMapAmenity.setOnParkingButtonStateChanged(::onParkingDisplayModeChanged)

        removeAmenitiesHandler = Handler(Looper.getMainLooper()) { msg ->
            when (msg.what) {
                WHAT_RESET_AMENITY_MODE -> {

                    if (viewBinding.toggleMapAmenity.displayMode != TBTAmenityToggleView.DisplayMode.NONE) {
                        val isStationary = obuCheckSpeedHelper.isStationary()
                                && viewBinding.toggleMapAmenity.displayMode == TBTAmenityToggleView.DisplayMode.PARKING
                        if (!isStationary) {
                            viewBinding.toggleMapAmenity.updateDisplayMode(TBTAmenityToggleView.DisplayMode.NONE)
                            onDisplayModeChanged(TBTAmenityToggleView.DisplayMode.NONE)
                        }
                    }
                }

                WHAT_CHECK_DISTANCE_WAYPOINT -> {
                    currentUserRawLocation?.let {
                        navAmenityLayerManager?.hasPassedWaypoints(
                            Point.fromLngLat(
                                it.longitude, it.latitude
                            )
                        )
                    }

                    removeAmenitiesHandler?.sendEmptyMessageDelayed(
                        WHAT_CHECK_DISTANCE_WAYPOINT,
                        5000
                    )
                }

                WHAT_CHECK_DISTANCE_TO_DESTINATION -> {
                    handleLogicDistanceToDestination()
                    handleLogicToGetAvailabilityAlertVoice()
                }
            }
            return@Handler false
        }
        removeAmenitiesHandler?.sendEmptyMessageDelayed(WHAT_CHECK_DISTANCE_WAYPOINT, 5000)
    }

    private var shouldCheckParkingAvailability = true
    private var shouldCheckAlternativeCarparkAt1KM = true
    private var shouldAddAlternativeCarparkToMap = true
    private  var shouldCheckAvailabilityAlertVoice = true
    private fun handleLogicToGetAvailabilityAlertVoice() {
        if (!shouldCheckAvailabilityAlertVoice || lastCarConnectionState != CarConnection.CONNECTION_TYPE_NOT_CONNECTED) return
        val distance = BreezeCarUtil.masterlists?.carparkListAlertDistance?.let { itemSetting ->
            if (itemSetting.isNotEmpty()) {
                itemSetting.first().value
            } else null
        } ?: Constants.DISTANCE_GET_AVAILABILITY_ALERT_VOICE

        if (distanceRemainingInMetre in 0f..distance.toFloat()) {
            getAvailabilityAlertVoice(distance)
        }
    }

    private fun getAvailabilityAlertVoice(distance: String) {
        shouldCheckAvailabilityAlertVoice = false
        val carparkId: String? = currentDestinationDetails.carParkID
        val userLocationLinkIdRef: String? = currentDestinationDetails.userLocationLinkIdRef
        val placeId: String? = currentDestinationDetails.placeId
        val amenityId: String? = currentDestinationDetails.amenityId
        val layerCode: String? = currentDestinationDetails.layerCode
        viewModel.getAvailabilityAlertVoice(
            distance = distance,
            carparkId = carparkId,
            userLocationLinkIdRef = userLocationLinkIdRef,
            placeId = placeId,
            amenityId = amenityId,
            layerCode = layerCode,
        ) { data ->
            Analytics.logEvent(
                Event.OBU_SYSTEM_EVENT,
                bundleOf(
                    Param.KEY_EVENT_VALUE to Event.BOTTOM_OBU_CARD_SHOWN,
                    Param.KEY_LATITUDE to (currentUserRawLocation?.latitude ?: 0.0),
                    Param.KEY_LONGITUDE to (currentUserRawLocation?.longitude ?: 0.0),
                    Param.KEY_MESSAGE to data.navVoiceAlert,
                    Param.KEY_SCREEN_NAME to getScreenName()
                )
            )
            data.navVoiceAlert.takeIf { it.isNotEmpty() }?.run {
                VoiceInstructionsManager.getInstance()?.playVoiceInstructions(this)
            }
        }
    }

    private fun handleLogicDistanceToDestination() {
        val masterLists = BreezeCarUtil.masterlists
        val carparkListDistance = masterLists?.carparkListDistance?.let { itemSetting ->
            if (itemSetting.isNotEmpty()) {
                itemSetting.first().value?.toFloatOrNull()
            } else null
        } ?: Constants.DISTANCE_SHOW_CAR_PARK_LIST
        val carparkAvailabilityDistance =
            masterLists?.carparkAvailabilityDistance?.let { itemSetting ->
                if (itemSetting.isNotEmpty()) {
                    itemSetting.first().value?.toFloatOrNull()
                } else null
            } ?: Constants.DISTANCE_SHOW_CAR_PARK_AVAILABILITY

        // logic when distance reach 1km to destination
        if (distanceRemainingInMetre in 0f..carparkListDistance) {
            if (!viewBinding.toggleMapAmenity.isParkingButtonShowed()) {
                viewBinding.toggleMapAmenity.enableParkingButton(true)
            }
            if (shouldCheckAlternativeCarparkAt1KM) {
                shouldCheckAlternativeCarparkAt1KM = false
                getCarparkAlternative(false, shouldAddAlternativeCarparkToMap)
            }
        } else {
            if (viewBinding.toggleMapAmenity.isParkingButtonShowed()) {
                viewBinding.toggleMapAmenity.enableParkingButton(false)
            }

            // logic when distance reach 2km to destination
            if (shouldCheckParkingAvailability && distanceRemainingInMetre in 0f..carparkAvailabilityDistance) {
                shouldCheckParkingAvailability = false
                getCarparkAlternative(isShowAlert = true)
            }
        }

        removeAmenitiesHandler?.sendEmptyMessageDelayed(WHAT_CHECK_DISTANCE_TO_DESTINATION, 2000)
    }

    private fun getCarparkAlternative(
        isShowAlert: Boolean,
        shouldAddToMap: Boolean = false
    ) {
        val name = currentDestinationDetails.address1 ?: ""
        val lat = currentDestinationDetails.lat?.toDouble() ?: 0.0
        val long = currentDestinationDetails.long?.toDouble() ?: 0.0
        viewModel.getCarparkAlternative(lat, long, name) { data ->
            //check id to know if have nearby NCS carpark or alternative carpark
            if (!data.name.isNullOrBlank() && data.shouldShowParkingAvailabilityAlert()) {
                val listAmenity = data.alternateCarparks
                val amenity: BaseAmenity? =
                    if (!listAmenity.isNullOrEmpty()) listAmenity[0] else null
                if (isShowAlert) {
                    // analytics
                    val popupName: String? = when (data.availablePercentImageBand) {
                        AmenityBand.TYPE0.type -> Event.CARPARK_FULL
                        AmenityBand.TYPE1.type -> Event.LIMITED_PARKING_SPACE
                        AmenityBand.TYPE2.type -> Event.PARKING_AVAILABLE
                        else -> null
                    }
                    popupName?.let {
                        Analytics.logPopupEvent(
                            it,
                            Event.VALUE_POPUP_OPEN,
                            getScreenName()
                        )
                    }
                    // analytics
                    val displayTime =
                        BreezeCarUtil.masterlists?.carparkAvailabilityAlertDisplayTime?.let { itemSetting ->
                            if (itemSetting.isNotEmpty()) {
                                itemSetting.first().value?.toLongOrNull()?.times(1000)
                            } else null
                        } ?: Constants.TIME_SHOW_CAR_PARK_AVAILABILITY
                    viewBinding.queueAlertView.addCarparkAvailabilityView(
                        data.name ?: "",
                        data.availablePercentImageBand ?: "",
                        amenity,
                        navigateCallback = { data, availablePercentImageBand ->
                            showCustomToast(
                                icon = R.drawable.tick_successful,
                                message = getString(R.string.toast_routing_to_carpark),
                                duration = 3
                            )
                            // analytics
                            val eventValue = when (availablePercentImageBand) {
                                AmenityBand.TYPE0.type -> Event.POPUP_CARPARK_FULL_REROUTE_WALK
                                AmenityBand.TYPE1.type -> Event.POPUP_LIMITED_PARKING_SPACE_REROUTE_WALK
                                else -> null
                            }
                            eventValue?.let {
                                Analytics.logClickEvent(
                                    it,
                                    getScreenName()
                                )
                            }

                            shouldAddAlternativeCarparkToMap = false
                            navAmenityLayerManager?.alternativeCarpark = null
                            navAmenityLayerManager?.findRouteToCarPark(data)
                        },
                        closeCallback = {
                            // analytics
                            val eventValue: String? = when (data.availablePercentImageBand) {
                                AmenityBand.TYPE0.type -> Event.POPUP_CARPARK_FULL_CLOSE
                                AmenityBand.TYPE1.type -> Event.POPUP_LIMITED_PARKING_SPACE_CLOSE
                                AmenityBand.TYPE2.type -> Event.POPUP_PARKING_AVAILABLE_CLOSE
                                else -> null
                            }
                            eventValue?.let {
                                Analytics.logClickEvent(
                                    it,
                                    getScreenName()
                                )
                            }
                            // analytics
                        },
                        delay = displayTime
                    )
                }

                if (shouldAddToMap && amenity != null
                    && (data.availablePercentImageBand == AmenityBand.TYPE0.type
                            || data.availablePercentImageBand == AmenityBand.TYPE1.type)
                ) {
                    navAmenityLayerManager?.addAlternativeCarpark(amenity)
                }
                if (isShowAlert && !viewBinding.layoutManeuverList.fragmentCarConnectedWarningContainer.isVisible) {
                    data.navVoiceAlert?.let { voice ->
                        VoiceInstructionsManager.getInstance()?.playVoiceInstructions(voice)
                    }
                }
            }
        }
    }

    private fun onDisplayModeChanged(mode: TBTAmenityToggleView.DisplayMode) {
        currentUserRawLocation?.let { userLocation ->
            viewBinding.toggleMapAmenity.isEnabled = false
            lifecycleScope.launch {
                removeAmenitiesHandler?.removeMessages(WHAT_RESET_AMENITY_MODE)

                when (mode) {
                    TBTAmenityToggleView.DisplayMode.NONE -> {
                        shouldResetAmenity = true
                        onCameraTrackingDismissed(shouldDelayRecenter = false)
                        withContext(Dispatchers.Main) {
                            navAmenityLayerManager?.removeCarParks()
                            if (navAmenityLayerManager?.alternativeCarpark != null) {
                                navAmenityLayerManager?.addAlternativeCarpark(navAmenityLayerManager?.alternativeCarpark!!)
                            }
                            listOf(PETROL, EVCHARGER).forEach { amenityType ->
                                navAmenityLayerManager?.renderNearbyAmenities(
                                    Amenities(
                                        type = amenityType,
                                        data = arrayListOf<BaseAmenity>().apply {
                                            navAmenityLayerManager?.amenityWayPoints?.let { amenityWayPoints ->
                                                addAll(amenityWayPoints.filter { baseAmenity -> baseAmenity.amenityType == amenityType })
                                            }
                                        }
                                    ))
                            }
                        }
                    }

                    TBTAmenityToggleView.DisplayMode.PARKING -> {
                        renderNearbyCarParks(false)
                    }

                    else -> {
                        navAmenityLayerManager?.fetchNearbyAmenities(userLocation) { amenities ->
                            if (isActive) {
                                launch(Dispatchers.Main) {
                                    renderUserNearByAmenities(amenities)
                                }
                            }
                        }
                    }
                }
            }.invokeOnCompletion {
                viewBinding.toggleMapAmenity.isEnabled = true
            }
        }
    }

    private fun onParkingDisplayModeChanged(isSelected: Boolean) {
        isCarparkDisableByUser = true

    }

    private suspend fun renderUserNearByAmenities(amenities: List<Amenities>) {
        val extractAmenityWaypoints: (amenityType: String) -> List<BaseAmenity> = { amenityType ->
            navAmenityLayerManager?.amenityWayPoints?.run {
                filter { baseAmenity -> baseAmenity.amenityType == amenityType }
            } ?: listOf()
        }

        var otherType: String? = null
        when (viewBinding.toggleMapAmenity.displayMode) {

            TBTAmenityToggleView.DisplayMode.EV -> {
                otherType = PETROL
                amenities.find { it.type == EVCHARGER }?.let { evData ->
                    if (evData.data.isEmpty()) {
                        showNoNearbyAmenitiesAlert(EVCHARGER)
                    }
                    val waypoints = extractAmenityWaypoints(EVCHARGER)
                    val dataList = if (waypoints.isEmpty()) evData.data else
                        evData.data.filter { amenity1 -> waypoints.find { amenity2 -> amenity1.id == amenity2.id } == null }
                    navAmenityLayerManager?.renderNearbyAmenities(
                        evData.apply {

                            data = ArrayList<BaseAmenity>().apply {
                                addAll(waypoints)
                                addAll(dataList)
                            }
                        }
                    )

                    currentUserRawLocation?.let {
                        val points = evData.data.map { item ->
                            Point.fromLngLat(
                                item.long ?: 0.0, item.lat ?: 0.0
                            )
                        }.toMutableList()

                        points.add(Point.fromLngLat(it.longitude, it.latitude))
                        zoomToPoints(points)

                    }
                } ?: showNoNearbyAmenitiesAlert(EVCHARGER)
            }

            TBTAmenityToggleView.DisplayMode.PETROL -> {
                otherType = EVCHARGER
                amenities.find { it.type == PETROL }?.let { petrolData ->
                    if (petrolData.data.isEmpty()) {
                        showNoNearbyAmenitiesAlert(PETROL)
                    }
                    val waypoints = extractAmenityWaypoints(PETROL)
                    val dataList = if (waypoints.isEmpty()) petrolData.data else
                        petrolData.data.filter { amenity1 -> waypoints.find { amenity2 -> amenity1.id == amenity2.id } == null }
                    navAmenityLayerManager?.renderNearbyAmenities(
                        petrolData.apply {
                            data = ArrayList<BaseAmenity>().apply {
                                addAll(waypoints)
                                addAll(dataList)
                            }
                        }
                    )
                    currentUserRawLocation?.let {
                        val points = petrolData.data.map { item ->
                            Point.fromLngLat(
                                item.long ?: 0.0, item.lat ?: 0.0
                            )
                        }.toMutableList()

                        points.add(Point.fromLngLat(it.longitude, it.latitude))
                        zoomToPoints(points)

                    }

                } ?: showNoNearbyAmenitiesAlert(PETROL)
            }

            else -> {}
        }
        navAmenityLayerManager?.removeCarParks()
        navAmenityLayerManager?.renderNearbyAmenities(
            Amenities(
                type = otherType,
                data = arrayListOf<BaseAmenity>().apply {
                    navAmenityLayerManager?.amenityWayPoints?.let { amenityWayPoints ->
                        addAll(amenityWayPoints.filter { baseAmenity -> baseAmenity.amenityType == otherType })
                    }
                }
            ))
    }

    private fun initOBUInfoView() {
        getApp()?.getUserDataPreference()?.getLastConnectedOBUDevice()?.let {
            Analytics.logEvent(
                Event.OBU_SYSTEM_EVENT,
                bundleOf(
                    Param.KEY_EVENT_VALUE to Event.BOTTOM_OBU_CARD_SHOWN,
                    Param.KEY_SCREEN_NAME to getScreenName()
                )
            )
            getApp()?.obuConnectionHelper?.obuConnectionState?.observe(this) { obuState ->
                when (obuState) {
                    OBUConnectionState.CONNECTED -> {
                        if (shouldShowOBUConnectedMessage) {
                            showCustomToast(
                                R.drawable.tick_successful,
                                getString(R.string.obu_is_connected)
                            )
                            shouldShowOBUConnectedMessage = false
                        }
                        showOBUConnectedData()
                    }

                    OBUConnectionState.PAIRING ->
                        showOBUDisconnectedData(true)

                    else -> showOBUDisconnectedData()
                }
            }
        } ?: run { hideAllOBUViews() }
    }

    private fun showOBUConnectedData() {
        with(viewBinding.layoutNavBottom) {
            imgCardStatus.isVisible = true
            tvCarBalance.isVisible = true
            tvCarBalanceValue.isVisible = true
            imgOBUStatus.isVisible = true
            tvOBUStatus.isVisible = true

            tvOBUDisconnected.isVisible = false
            btnConnectOBU.isVisible = false

            updateCardBalanceViews(
                OBUDataHelper.getOBUCardBalanceSGD() ?: 0.0,
                OBUDataHelper.isShowBankCard()
            )
        }
    }

    private fun updateCardBalanceViews(value: Double, showBankCard: Boolean) {
        with(viewBinding.layoutNavBottom) {
            if (showBankCard) {
                tvCarBalance.setText(R.string.bankcard)
                imgCardStatus.setImageResource(R.drawable.ic_obu_bank_card)
                tvCarBalanceValue.text = ""
            } else {
                tvCarBalance.setText(R.string.card_balance)
                imgCardStatus.setImageResource(R.drawable.ic_cepas)

                val shouldShowCardBalance = OBUDataHelper.getOBUCardStatus()?.let { status ->
                    status == OBUCardStatus.Detected || status == OBUCardStatus.InsufficientStoredValue
                } ?: false

                if (value < OBUConstants.OBU_LOW_CARD_THRESHOLD.div(100)
                    || !shouldShowCardBalance
                ) {
                    tvCarBalanceValue.setTextColor(
                        ContextCompat.getColor(
                            this@TurnByTurnNavigationActivity,
                            R.color.passion_pink
                        )
                    )
                } else {
                    tvCarBalanceValue.setTextColor(
                        ContextCompat.getColor(
                            this@TurnByTurnNavigationActivity,
                            R.color.carpark_price_green
                        )
                    )
                }
                if (!shouldShowCardBalance)
                    tvCarBalanceValue.text = "$-"
                else
                    tvCarBalanceValue.text = "$${value.round()}"
            }
        }
    }

    private fun showOBUDisconnectedData(isPairing: Boolean = false) {
        with(viewBinding.layoutNavBottom) {
            imgCardStatus.isVisible = false
            tvCarBalance.isVisible = false
            tvCarBalanceValue.isVisible = false
            imgOBUStatus.isVisible = false
            tvOBUStatus.isVisible = false

            tvOBUDisconnected.isVisible = true
            btnConnectOBU.isVisible = true
            if (isPairing) {
                btnConnectOBU.isEnabled = false
                btnConnectOBU.setText(R.string.pair_connecting)
            } else {
                btnConnectOBU.isEnabled = true
                btnConnectOBU.setText(R.string.pair_connect)
            }
        }
    }

    private fun hideAllOBUViews() {
        with(viewBinding.layoutNavBottom) {
            imgCardStatus.isVisible = false
            tvCarBalance.isVisible = false
            tvCarBalanceValue.isVisible = false
            imgOBUStatus.isVisible = false
            tvOBUStatus.isVisible = false

            tvOBUDisconnected.isVisible = false
            btnConnectOBU.isVisible = false
        }
    }

    private fun handleEndButtonClick() {
        if (isArrived) {
            if (isWalking)
                Analytics.logClickEvent(":end_trip", Screen.WALKING_GUIDE)
            else
                Analytics.logClickEvent(Event.END_NAVIGATION_ARRIVAL, Screen.NAVIGATION)
            viewBinding.mapView.location.apply {
                enabled = false
            }
            if (SHW_END_NAVGTN_CNFRMTN_BFR_WLKNG) {
                if (walkingRoute != null && !isWalking) {
                    showWalkingPathDialog()
                } else {
                    doEndNavigation()
                }
            } else {
                doEndNavigation()
            }
        } else {
            showEndNavigationConfirmationDialog()
        }
        //viewBinding.routeOverview.visibility = View.GONE
    }

    private fun initRouteLineAPI(mRoute: NavigationRoute) {
        lifecycleScope.launch(Dispatchers.Main) {
            routeLineAPI!!.setNavigationRoutes(listOf(mRoute)) { value ->
                window.decorView.post {
                    viewBinding.mapView.getMapboxMap().getStyle { style ->
                        routeLineView!!.renderRouteDrawData(style, value);
                        handleRouteLineRefresh()
                    }
                }
                MapboxNavigationApp.current()?.setNavigationRoutes(listOf(mRoute))
                checkForReroute(mRoute)
                currentDirectionsRoute = mRoute
                startNavigation()
            }
        }

        /*viewBinding.routeOverview.setOnClickListener {
            viewBinding.routeOverview.showTextAndExtend(2000L)
            updateCameraToOverview()
        }*/
    }

    private fun checkForReroute(mRoute: NavigationRoute) {
        val currentLocation = LocationBreezeManager.getInstance().currentLocation
        val routeOptionCoordinates = mRoute.routeOptions.coordinatesList()
        if (currentLocation != null && routeOptionCoordinates.size > 0) {
            if (currentLocation.latitude != routeOptionCoordinates[0].latitude() || currentLocation.longitude != routeOptionCoordinates[0].longitude()) {
                lifecycleScope.launch(Dispatchers.IO) {
                    val distanceMeters = TurfMeasurement.distance(
                        Point.fromLngLat(currentLocation.longitude, currentLocation.latitude),
                        routeOptionCoordinates[0],
                        TurfConstants.UNIT_METERS
                    )
                    //Re routes if distance is greater than 1000 meters from origin
                    if (distanceMeters > 1000) {
                        reRouteBasedOnCurrentLocation()
                    }
                }
            }
        }
    }

    fun resetToggleViewAmenityType() {
        viewBinding.toggleMapAmenity.updateDisplayMode(TBTAmenityToggleView.DisplayMode.NONE)
    }

    fun reRouteBasedOnCurrentLocation() {
        val currentLocation = LocationBreezeManager.getInstance().currentLocation
        if (currentLocation != null) {

            val profile: String = if (isWalking) {
                DirectionsCriteria.PROFILE_WALKING
            } else {
                DirectionsCriteria.PROFILE_DRIVING_TRAFFIC
            }

            val waypoint = navAmenityLayerManager?.amenityWayPoints?.map {
                Point.fromLngLat(
                    it.long ?: 0.0, it.lat ?: 0.0
                )
            } ?: listOf()

            val updateRouteOptions =
                RouteOptions.builder().applyDefaultNavigationOptions()
                    .language(Constants.MAPBOX_ROUTE_OPTION_LOCALE)
                    .coordinates(
                        Point.fromLngLat(currentLocation.longitude, currentLocation.latitude),
                        waypoint,
                        Point.fromLngLat(
                            currentDestinationDetails.long!!.toDouble(),
                            currentDestinationDetails.lat!!.toDouble()
                        )
                    )
                    .waypointIndicesList(
                        listOf(
                            0, waypoint.size + 1
                        )
                    )
                    .overview(DirectionsCriteria.OVERVIEW_FULL).profile(profile).annotationsList(
                        arrayListOf(
                            DirectionsCriteria.ANNOTATION_DISTANCE,
                            DirectionsCriteria.ANNOTATION_CONGESTION
                        )
                    ).alternatives(true).geometries(Constants.POLYLINE_GEOMETRY)
                    .voiceInstructions(true)
                    .voiceUnits(DirectionsCriteria.METRIC)
                    .bannerInstructions(true) //this
                    .steps(true)
                    .build()
            MapboxNavigationApp.current()?.requestRoutes(
                updateRouteOptions,
                object : NavigationRouterCallback {
                    override fun onCanceled(
                        routeOptions: RouteOptions, routerOrigin: RouterOrigin
                    ) {
                    }

                    override fun onFailure(
                        reasons: List<RouterFailure>, routeOptions: RouteOptions
                    ) {
                        Timber.e("NavigationRouterCallback: $reasons")
                    }

                    override fun onRoutesReady(
                        routes: List<NavigationRoute>, routerOrigin: RouterOrigin
                    ) {
                        MapboxNavigationApp.current()?.setNavigationRoutes(routes)
                    }
                })

        }
    }

    @OptIn(ExperimentalPreviewMapboxNavigationAPI::class)
    override fun onResume() {
        super.onResume()
        viewBinding.etaToast.resumeIfNeed()

        if (isEndingNavigationOnResume) {
            isEndingNavigationOnResume = false
            doEndNavigation()
        } else {
            setTurnByTurnActivityStatusBar()
        }

        if (showWalkingPathDialogOnResume) {
            showWalkingPathDialog()
        } else if (::ratingDialog.isInitialized) {
            if (!ratingDialog.isShowing && !isRatingDialogDismissed) {
                ratingDialog.show()
            }
        }

        if (showArrivedWalkathonDialogOnResume) {
            showArrivedWalkathonDialog()
        }
    }


    private fun doEndNavigation() {
        //showRatingDialog(curTripId)
        Utils.isNavigationStarted = false
        Utils.phoneNavigationStartEventData = null
        postEventWithCacheLast(AppToNavigationEndEvent())

        initiateNavigationClose()
        setActivityResult()
    }

    @OptIn(ExperimentalPreviewMapboxNavigationAPI::class)
    @SuppressLint("MissingPermission")
    private fun initNavigation() {
        carConnection = CarConnection(this)
        carConnection?.type?.observe(this) {
            lastCarConnectionState = it
        }

        locationObserver = object : LocationObserver {
            override fun onNewLocationMatcherResult(locationMatcherResult: LocationMatcherResult) {
            }

            override fun onNewRawLocation(rawLocation: Location) {
                //Timber.d("Raw location change called")
                //Timber.w("Mock location ? [${rawLocation.isFromMockProvider}]")
                val point = Point.fromLngLat(rawLocation.longitude, rawLocation.latitude)
                val cameraOptions =
                    CameraOptions.Builder().center(point).zoom(INITIAL_ZOOM).pitch(INITIAL_PITCH)
                        .build()
                viewBinding.mapView.getMapboxMap().setCamera(cameraOptions)
                MapboxNavigationApp.current()?.unregisterLocationObserver(this)
            }
        }
        tripProgressApi = MapboxTripProgressApi(getTripProgressFormatter())
    }

    private fun trackUserIdleOBUDisconnected(locationMatcherResult: LocationMatcherResult) {
        val speedLimitValue = speedLimitApi.updatePostedAndCurrentSpeed(
            locationMatcherResult,
            DistanceFormatterOptions.Builder(applicationContext)
                .unitType(UnitType.METRIC)
                .build()
        )

        TripLoggingManager.getInstance()
            ?.checkIdle(speedLimitValue.currentSpeed.toDouble())
    }

    private fun initNavigationObservers(pRoute: NavigationRoute?) {
        MapboxNavigationApp.current()?.let { mapboxNavigation ->
            mapboxNavigation.registerLocationObserver(locationObserver)
            mapboxNavigation.registerLocationObserver(mapMatcherResultObserver)
            SpeedLimitUtil.setupLocationObserver(mapboxNavigation, speedLimitHandler)
            mapboxNavigation.registerRoutesObserver(routesObserver)
            mapboxNavigation.registerRouteProgressObserver(routeProgressObserver)
            pRoute?.let { route -> initEhorizonObserver(route) }

            volumeControlStream = AudioManager.STREAM_MUSIC
            VoiceInstructionsManager.setupVoice(applicationContext)
            lifecycleScope.launch(Dispatchers.Main) {
                VoiceInstructionsManager.getInstance()
                    ?.registerVoiceInstruction() //It's crashing here. //ANKUR

                //BREEZE2-1845
//                if (ETAEngine.getInstance()?.isETAEnabled() == true) {
//                    announceShareDriveStarted()
//                }
            }
        }
        autoDrive(getApp()?.isAutoDriveSimulation == true)
    }

    /***
     * function for test only
     * make puck run follow the route line
     */
    private fun autoDrive(isAutoDriveEnabled: Boolean) {
        MapboxNavigationApp.unregisterObserver(ReplayRouteTripSession)
        if (isAutoDriveEnabled) {
            MapboxNavigationApp.registerObserver(ReplayRouteTripSession)
        }
    }

    private fun updateCameraLocation() {
        getCurrentLocation()?.let {
            (navigationLocationProvider as? NavigationLocationProvider)?.changePosition(it)
        }
        viewBinding.mapView.gestures.addOnMoveListener(onMoveListener)
    }

    @SuppressLint("MissingPermission")
    private fun initStyle(args: Bundle, after: (route: NavigationRoute) -> Unit) {
        viewBinding.mapView.getMapboxMap().loadStyleUri(
            getMapboxStyle(basicMapRequired = false)
        ) { style ->

            lifecycleScope.launch(Dispatchers.Main) {
                // BREEZES-4577: a rare condition where ondestroy is called, but style is not yet loaded
                if (this@TurnByTurnNavigationActivity.isDestroyed) {
                    return@launch
                }
                if (!routeInitJob.isCompleted) {
                    routeInitJob.join()
                }
                mRoute?.let { mRoute ->
                    initRouteObservers(args)

                    val dataHolder = TurnByTurnAmenityViewManager.MapViewDataHolder(
                        viewBinding.mapView,
                        getScreenName(),
                        calculateDestinationIcon(),
                        userPreferenceUtil.isDarkTheme()
                    )
                    navAmenityLayerManager = TurnByTurnAmenityViewManager(dataHolder,
                        apiHelper,
                        this@TurnByTurnNavigationActivity,
                        originalDestinationDetails,
                        mRoute,
                        handleDismissCamera = {
                            onCameraTrackingDismissed()
                        })
                    removeTrafficLayer(style)
                    Timber.d("Mapbox style is loaded")
                    setUpGestureListeners()

                    viewBinding.layoutNavBottom.recenterButton.setOnClickListener {
                        Analytics.logClickEvent(Event.RECENTER, getScreenName())
                        recenterMap()
                    }

                    MapIcon.CommonMapIcon.apply {
                        putAll(
                            hashMapOf(
                                Constants.FAVOURITES_DESTINATION_ICON to R.drawable.destination_puck,
                                //Constants.UNSAVED_DESTINATION_ICON to R.drawable.unsaved_destination_mark,
                                Constants.UNSAVED_DESTINATION_ICON to R.drawable.destination_puck,
                            )
                        )
                    }.forEach { (key, value) ->
                        value.toBitmap(this@TurnByTurnNavigationActivity)?.let { bmp ->
                            style.addImage(key, bmp, false)
                        }
                    }

                    getLatestIncidentData()
                    after(mRoute)
                }
            }

            if (obuCheckSpeedHelper == null) {
                obuCheckSpeedHelper = OBUCheckSpeedHelper()
                obuCheckSpeedHelper?.setStateChangeCallback(::onSpeedStateChanged)
            }
        }
    }

    private fun onSpeedStateChanged(
        speedState: OBUCheckSpeedHelper.SpeedState,
        stateChanged: Boolean
    ) {
        if (isArrived) return
        when (speedState) {
            OBUCheckSpeedHelper.SpeedState.RUNNING -> {
                isCarparkDisableByUser = false
                isCurrentLocationHasCarPark = true
                if (shouldResetAmenity) {
                    if (viewBinding.toggleMapAmenity.displayMode != TBTAmenityToggleView.DisplayMode.NONE) {
                        viewBinding.toggleMapAmenity.updateDisplayMode(TBTAmenityToggleView.DisplayMode.NONE)
                        onDisplayModeChanged(TBTAmenityToggleView.DisplayMode.NONE)
                    }
                } else {
                    if (removeAmenitiesHandler?.hasMessages(WHAT_RESET_AMENITY_MODE) != true) {
                        removeAmenitiesHandler?.sendEmptyMessageDelayed(
                            WHAT_RESET_AMENITY_MODE,
                            8000
                        )
                    }
                }
            }

            OBUCheckSpeedHelper.SpeedState.STATIONARY -> {
                val carparkListDistance =
                    BreezeCarUtil.masterlists?.carparkListDistance?.let { itemSetting ->
                        if (itemSetting.isNotEmpty()) {
                            itemSetting.first().value?.toFloat()
                        } else null
                    } ?: Constants.DISTANCE_SHOW_CAR_PARK_LIST
                if (distanceRemainingInMetre < 0f || distanceRemainingInMetre > carparkListDistance) return
                isCameraTrackingDismissed = false
                if (isCurrentLocationHasCarPark && !isCarparkDisableByUser) {
                    if (viewBinding.toggleMapAmenity.displayMode == TBTAmenityToggleView.DisplayMode.NONE) {
                        viewBinding.toggleMapAmenity.updateDisplayMode(TBTAmenityToggleView.DisplayMode.PARKING)
                        onDisplayModeChanged(TBTAmenityToggleView.DisplayMode.PARKING)
                    }
                }
            }

            else -> {}
        }
    }

    private fun removeTrafficLayer(it: Style) {
        it.removeStyleLayer(Constants.TRAFFIC_LAYER_ID)
    }

    private fun populateArrivalView() {
        val viewMyView = findViewById<View>(R.id.arrival_notification)
        val addressTitle = viewMyView.findViewById<View>(R.id.arrived_address_1) as TextView
        val addressDetails = viewMyView.findViewById<View>(R.id.arrived_address_2) as TextView
        val ivBookMark = viewMyView.findViewById<View>(R.id.arrived_bookmark) as ImageView
        val arrivedbt = viewMyView.findViewById<View>(R.id.arrived_address_button) as ImageView
        if (!currentDestinationDetails.destinationName.isNullOrBlank() && currentDestinationDetails.easyBreezyAddressID != -1) {
            addressTitle.text = currentDestinationDetails.destinationName
            addressDetails.text = currentDestinationDetails.address1

            ivBookMark.setImageResource(R.drawable.ic_bookmarked)
            arrivedbt.setImageResource(R.drawable.destination_puck)
        } else {
            addressTitle.text = currentDestinationDetails.address1
            addressDetails.text = currentDestinationDetails.address2

            ivBookMark.setImageResource(R.drawable.bookmark_add)
            arrivedbt.setImageResource(R.drawable.destination_puck)
        }
    }


    private fun computeRouteERPs() {
        // Compute ERPs in route
        Timber.d("Computing route ERPs")
        if (viewBinding.mapView.getMapboxMap()
                .getStyle()?.isStyleLoaded == true && ::currentDirectionsRoute.isInitialized
        ) {
            lifecycleScope.launch(Dispatchers.Default) {
                if (breezeERPUtil.getERPData() != null) {
                    Timber.d("Before getting ERPs in route - inside coroutine")
                    currentRouteERPInfo = breezeERPUtil.getERPInRoute(
                        breezeERPUtil.getERPData()!!, currentDirectionsRoute.directionsRoute, false
                    )
                    Timber.d("After getting ERPs in route - inside coroutine")
                    Timber.d("Nav - before displaying route ERPs")
                    withContext(Dispatchers.Main) {
                        displayRouteERP(currentRouteERPInfo.featureCollection)
                    }
                }
            }
        }
    }


    private fun erpRefreshListener() {
        Timber.d("ERP refresh listener is initialized")
        compositeDisposable.add(RxBus.listen(RxEvent.ERPRefresh::class.java)
            .observeOn(AndroidSchedulers.mainThread()).subscribe {
                Timber.d("ERP refresh listener is called")
                val mapboxMap = viewBinding.mapView.getMapboxMap()
                if (mapboxMap.getStyle() != null && ::currentDirectionsRoute.isInitialized && mapboxMap.isFullyLoaded()) {
                    // Update rates for all erps in the route
                    if (breezeERPUtil.getERPData() != null) {
                        Timber.d("Refreshing all erps with new rate")
                        lifecycleScope.launch(Dispatchers.Default) {
                            currentRouteERPInfo = breezeERPUtil.getERPInRoute(
                                breezeERPUtil.getERPData()!!,
                                currentDirectionsRoute.directionsRoute,
                                false
                            )
                            Timber.d("Display route ERP for refresh")
                            withContext(Dispatchers.Main) {
                                displayRouteERP(currentRouteERPInfo.featureCollection)
                            }
                        }
                    }
                }
            })
    }

    private fun walkingPathRequestListener() {
        Timber.d("ERP refresh listener is initialized")
        compositeDisposable.add(RxBus.listen(RxEvent.RequestToFindWalking::class.java)
            .observeOn(AndroidSchedulers.mainThread()).subscribe {
                //if(this@TurnByTurnNavigationActivity::mapboxMap.isInitialized) {
                findWalkingPath(
                    it.startingPoint, it.endingPoint
                )
                //}
            })
        compositeDisposable.add(RxBus.listen(RxEvent.RequestToUpdateCurrentDestination::class.java)
            .observeOn(AndroidSchedulers.mainThread()).subscribe {
                currentDestinationDetails = it.currentDestinationDetails
                if (currentDestinationDetails.isDestinationCarPark()) {
                    if (currentDestinationDetails.getSelectedCarParkID() != null) {
                        /**
                         * reCall API get carpark broadcast message for get list NotificationCriterias
                         */
                        getCarparkBroadcastMessage(
                            currentDestinationDetails.getSelectedCarParkID(),
                            typeBroadcastMessage = TYPE_BROADCAST_MESSAGE.PLANNING
                        )
                    }
                }
            })
    }


    private fun displayRouteERP(featureCollection: FeatureCollection) {
        viewBinding.mapView.getMapboxMap().getStyle { style ->
            Timber.d("Nav - displaying active ERPs")
            displayActiveERP(style, featureCollection)
        }
    }

    private fun getLatestIncidentData() {
        breezeIncidentsUtil.getLatestIncidentData().takeIf { it.isNotEmpty() }?.let {
            viewBinding.mapView.getMapboxMap().getStyle { style ->
                displayIncidentData(style, it)
            }
        }
        refreshIncidentData()
    }

    private fun refreshIncidentData() {
        compositeDisposable.add(RxBus.listen(RxEvent.IncidentDataRefresh::class.java)
            .observeOn(AndroidSchedulers.mainThread()).subscribe {
                viewBinding.mapView.getMapboxMap().getStyle { style ->
                    displayIncidentData(style, it.incidentData)
                }
            })
    }


    private fun updateCameraToOverview() {
        viewportDataSource.additionalPointsToFrameForOverview(listOf())
        if (this.resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            viewportDataSource.overviewPadding = landscapeOverviewEdgeInsets
        } else {
            viewportDataSource.overviewPadding = overviewEdgeInsets
        }
        viewportDataSource.evaluate()
        navigationCamera.requestNavigationCameraToOverview()
    }

    fun zoomToPoints(points: List<Point>) {
        if (points.isNotEmpty()) {
            shouldResetAmenity = false
            isCameraTrackingDismissed = false
            viewBinding.layoutNavBottom.recenterButton.isVisible = true
            if (viewBinding.toggleMapAmenity.displayMode == TBTAmenityToggleView.DisplayMode.PARKING) {
                isCurrentLocationHasCarPark = true
            }
            removeAmenitiesHandler?.sendEmptyMessageDelayed(
                WHAT_RESET_AMENITY_MODE,
                10000
            )
        }

        // points.size = 1 mean is only current location point, no need to zoom
        if (points.size > 1) {
            navigationCamera.requestNavigationCameraToIdle()
            val cameraOptions = viewBinding.mapView.getMapboxMap().cameraForGeometry(
                GeometryCollection.fromGeometries(points.toList()),
                visibleArea,
                bearing = viewBinding.mapView.getMapboxMap().cameraState.bearing,
                pitch = 0.0
            )
            viewBinding.mapView.getMapboxMap().easeTo(cameraOptions)
        }
    }

    private fun updateCameraToFollowing() {
        viewBinding.layoutNavBottom.layoutTravelInfo.viewTreeObserver.addOnGlobalLayoutListener(
            object :
                ViewTreeObserver.OnGlobalLayoutListener {
                override fun onGlobalLayout() {
                    viewBinding.layoutNavBottom.layoutTravelInfo.viewTreeObserver
                        .removeOnGlobalLayoutListener(this)

                    val topPadding = viewBinding.mapView.getMapboxMap()
                        .getSize().height.toDouble() / 5.0

                    val travelInfoLayoutHeight = viewBinding.layoutNavBottom.layoutTravelInfo.height
                    val roadNameLayoutHeight = viewBinding.layoutNavBottom.tvTurnStreetName.height
                    val obuInfoMaxHeight = 64.0.dpToPx()

                    val bottomPadding = travelInfoLayoutHeight +
                            roadNameLayoutHeight +
                            obuInfoMaxHeight +
                            70.0.dpToPx()
                    val followingEdgeInsets = EdgeInsets(
                        topPadding, 240.0, bottomPadding, 240.0
                    )

                    visibleArea = EdgeInsets(
                        topPadding,
                        240.0,
                        (travelInfoLayoutHeight +
                                roadNameLayoutHeight +
                                obuInfoMaxHeight + 20.dpToPx()),
                        240.0
                    )
                    viewportDataSource.additionalPointsToFrameForFollowing(listOf())
                    viewportDataSource.followingPadding = followingEdgeInsets
                    viewportDataSource.evaluate()
                    navigationCamera.requestNavigationCameraToFollowing { Timber.d("Following animation end") }
                }
            })
    }

    private fun updateCameraToIdle() {
        navigationCamera.requestNavigationCameraToIdle()
    }

    private fun startNavigation() {
        Timber.d("Starting navigation once route is drawn")
        if (isWalking) {
            updateCameraToOverview()
        } else {
            updateCameraToFollowing()
        }
        BreezeArrivalObserver.create(
            MapboxNavigationApp.current(), getScreenName(), currentDestinationDetails
        )
        BreezeArrivalObserver.getInstance()
            ?.registerNavigationArrivalObserver(navigationArrivalObserver)
        Timber.d("Nav - Initialising feature detection road objects")
        lifecycleScope.launch(Dispatchers.IO) {
            breezeFeatureDetectionUtil.initFeatureDetectionRoadObjects()
        }

    }

    @OptIn(ExperimentalPreviewMapboxNavigationAPI::class)
    private fun stopNavigation(isArrivedAtDestination: Boolean = false) {
        breezeFeatureDetectionUtil.stopFeatureDetection()
        viewBinding.layoutNavBottom.tvTurnStreetName.visibility = View.INVISIBLE
        viewBinding.layoutSpeedLimit.root.visibility = View.GONE

        //updateCameraToIdle()
        clearRouteLine()
        MapboxNavigationApp.current()
            ?.takeIf { !isArrivedAtDestination }
            ?.let {
                BreezeTripManagerUtil.stopTrip(
                    it, currentDestinationDetails, getScreenName(), isArrivedAtDestination
                )
                it.setNavigationRoutes(listOf())
            }
        updateCameraToOverview()


        //Clearing Calendar Util Navigation event details regardless of destination being a calendar item
        breezeCalendarUtil.navigationStopped()

        //BREEZE2-1845
//        ETAEngine.getInstance()?.stopETA(isArrivedAtDestination)
        //ToDo - Validate with the Author
//        ETAEngine.getInstance()?.getEtaLocationUpdateInstance(ETAMode.Navigation)?.let {
//            LocationBreezeManager.getInstance().removeLocationCallBack(it)
//        }
        BreezeArrivalObserver.getInstance()?.unregisterNavigationArrivalObserver(
            navigationArrivalObserver, shouldStopTrip = true
        )
        BreezeEHorizonProvider.destroy()

        SpeedLimitUtil.gc()
    }

    private fun clearRouteLine() {
        lifecycleScope.launch(Dispatchers.Main) {
            val mapboxMap = viewBinding.mapView.getMapboxMap()
            ifNonNull(routeLineAPI, routeLineView, mapboxMap.getStyle()) { api, view, style ->
                api.clearRouteLine { value ->
                    routeArrowView?.render(style, routeArrowAPI.clearArrows())
                    view.renderClearRouteLineValue(style, value)
                }
                //routeLineAPI.clearRouteLine()
            }
            routeLineAPI?.clearRouteLine() { value ->
                com.mapbox.navigation.utils.internal.ifNonNull(
                    routeLineView, mapboxMap.getStyle()
                ) { view, style ->
                    view.renderClearRouteLineValue(style, value)
                }
            }
            handleRouteLineRefresh()
        }

    }


    //TODO: this is a workaround to remove the default grey marker : https://github.com/mapbox-collab/ncs-collab/issues/365
    private fun handleRouteLineRefresh() {
        if (isWalking) {
            //makeAllLineLayersDashed()
            //makeLineDashed("mapbox-navigation-route-layer")
            //makeBackgroundDashed(TOP_LEVEL_ROUTE_LINE_LAYER_ID)
            makeLineDashed("mapbox-layerGroup-1-main") //RouteLayerConstants.LAYER_GROUP_1_MAIN
            makeLineDashed("mapbox-layerGroup-2-main") //RouteLayerConstants.LAYER_GROUP_2_MAIN
            makeLineDashed("mapbox-layerGroup-3-main") //RouteLayerConstants.LAYER_GROUP_3_MAIN
        }
        viewBinding.mapView.getMapboxMap().getStyle {
            it.removeStyleImage("destinationMarker")
        }
    }

    private fun makeLineDashed(layerId: String) {
        viewBinding.mapView.getMapboxMap().getStyle { style ->
            style.getLayerAs<LineLayer>(layerId)?.addDashingStyleToLine(baseContext)
        }
    }

    override fun getViewModelReference(): NavigationViewModel {
        return viewModel
    }

    private fun getTripProgressFormatter(): TripProgressUpdateFormatter {
        return TripProgressUpdateFormatter.Builder(this).distanceRemainingFormatter(
            DistanceRemainingFormatter(
                DistanceFormatterOptions.Builder(applicationContext).build()
            )
        ).timeRemainingFormatter(TimeRemainingFormatter(this))
            .percentRouteTraveledFormatter(PercentDistanceTraveledFormatter())
            .estimatedTimeToArrivalFormatter(
                EstimatedTimeToArrivalFormatter(
                    this, TimeFormat.TWELVE_HOURS
                )
            ).build()
    }

    private fun onCameraTrackingDismissed(shouldDelayRecenter: Boolean = true) {
        //Toast.makeText(this, "onCameraTrackingDismissed", Toast.LENGTH_SHORT).show()
        isCameraTrackingDismissed = true
        viewBinding.layoutNavBottom.recenterButton.visibility = View.VISIBLE

        if (autoRecenterMapHandler == null)
            autoRecenterMapHandler = Handler(Looper.getMainLooper())
        autoRecenterMapHandler?.removeCallbacksAndMessages(null)
        autoRecenterMapHandler?.postDelayed({
            if (isCameraTrackingDismissed) recenterMap()
        }, if (shouldDelayRecenter) Constants.AUTO_RECENTER_MAP_DURATION else 100)
    }

    private fun recenterMap() {
        resetTrackerMode()
        if (isWalking) {
            updateCameraToOverview()
        } else {
            updateCameraToFollowing()
        }
    }

    private fun resetTrackerMode() {
        isCameraTrackingDismissed = false
        viewBinding.layoutNavBottom.recenterButton.visibility = View.GONE
    }

    //BREEZE2-1845
//    @SuppressLint("MissingPermission")
//    fun startLocationUpdates() {
//        LocationBreezeManager.getInstance().startLocationUpdates()
//        ETAEngine.getInstance()?.getEtaLocationUpdateInstance(ETAMode.Navigation)?.let {
//            LocationBreezeManager.getInstance().registerLocationCallback(it)
//        }
//    }

    private fun showNotificationView(
        zone: NavigationZone,
        distance: Int,
        mERPName: String?,
        chargeAmount: String?,
        location: String?
    ) {
        if (isDestroyed) {
            return
        }

        closeCarparkListScreenRN()
        val currentZone = zone
        //notification.bringToFront()
        viewBinding.featureNotification.root.visibility = View.VISIBLE
        /**
         * hide notification park broadcast when system alert show
         */
        viewBinding.notificationParkBroadcast.root.visibility = View.GONE
        if (displayedNavigationZone == null) {
            Analytics.logNotificationEvents(
                Event.MAP_NOTIFICATION, Event.NOTIFICATION_OPEN, getScreenName()
            )
            displayedNavigationZone = zone
        }


        /**
         * filter zone & set data
         */
        HorizonObserverFillterZoneUtils.filterZone(currentZone, distance).let { data ->
            viewBinding.featureNotification.root.background =
                HorizonObserverFillterZoneUtils.getCruiseNotificationBG(
                    ZoneUIUtils.getZoneTypeBGColor(data.zonePriority), this
                )

            viewBinding.featureNotification.textDistance.text =
                if (zone.maxDistanceMtr <= 300) data.mDistance else location ?: data.mDistance
            if (currentZone == NavigationZone.ERP_ZONE) {

                //todo bangnv will change file type
                //data.mTextZoneType = mERPName
                viewBinding.featureNotification.textZoneType.text = mERPName
            } else {
                viewBinding.featureNotification.textZoneType.text =
                    getString(ZoneUIUtils.getZoneTypeString(data.zonePriority))
            }
            viewBinding.featureNotification.imgZone.setImageResource(
                ZoneUIUtils.getZoneTypeImage(
                    data.zonePriority
                )
            )
            viewBinding.featureNotification.textPrice.text =
                chargeAmount?.safeDouble()?.visibleAmount()
            viewBinding.featureNotification.textPrice.visibility = if (data.mIsShowAmount) {
                View.VISIBLE
            } else {
                View.GONE
            }
        }

        //Handling Voice


        viewBinding.featureNotification.root.visibility = View.VISIBLE
        lifecycleScope.launch(Dispatchers.Default) {
            delay(Constants.TRIP_NOTIFICATION_SHOW_DURATION)
            Timber.d("invisible this notification")
            runOnUiThread {
                viewBinding.featureNotification.root.visibility = View.GONE
            }
        }
    }

    private var lastDelayJob: Job? = null
    private fun showOBUNotificationView(
        navigationZonePriority: Int,
        distance: String?,
        message: String = "",
        delayInMillis: Long = Constants.TRIP_NOTIFICATION_SHOW_DURATION
    ) {
        if (isDestroyed) {
            return
        }
        closeCarparkListScreenRN()

        viewBinding.featureNotification.root.background =
            HorizonObserverFillterZoneUtils.getCruiseNotificationBG(
                ZoneUIUtils.getZoneTypeBGColor(navigationZonePriority), this
            )
        viewBinding.featureNotification.textDistance.text = distance
        viewBinding.featureNotification.textZoneType.text =
            message.ifBlank { getString(ZoneUIUtils.getZoneTypeString(navigationZonePriority)) }
        viewBinding.featureNotification.imgZone.setImageResource(
            ZoneUIUtils.getZoneTypeImage(
                navigationZonePriority
            )
        )
        viewBinding.featureNotification.textPrice.text = ""
        viewBinding.featureNotification.textPrice.visibility = View.GONE

        viewBinding.featureNotification.root.visibility = View.VISIBLE
        if (lastDelayJob != null) {
            lastDelayJob?.cancel()
        }
        lastDelayJob = lifecycleScope.launch(Dispatchers.Default) {
            delay(delayInMillis)
            runOnUiThread {
                Analytics.logPopupEvent(
                    Event.AA_POPUP_NAVIGATION,
                    Event.VALUE_POPUP_CLOSE,
                    Screen.NAVIGATION
                )
                viewBinding.featureNotification.root.visibility = View.GONE
            }
        }
    }

    fun setTurnByTurnActivityStatusBar() {
        window.apply {
            clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            var flags = window.decorView.systemUiVisibility // get current flag
            flags =
                flags and View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR.inv()// remove LIGHT_STATUS_BAR to flag
            //flags = flags or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            window.decorView.systemUiVisibility = flags
            //decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            statusBarColor = ContextCompat.getColor(
                this@TurnByTurnNavigationActivity, R.color.navigation_main_manuever
            )
        }
    }

    @OptIn(ExperimentalPreviewMapboxNavigationAPI::class)
    private val navigationArrivalObserver = object : NavigationArrivalObserver {
        override fun onFinalDestinationArrival(routeProgress: RouteProgress) {
            Timber.e("navigationArrivalObserver-onFinalDestinationArrival")
            Timber.e("navigationArrivalObserver-onFinalDestinationArrival: ${routeProgress.distanceTraveled}")
            /**
             * hide turn speed view when arrival
             */
            viewBinding.layoutSpeedLimit.root.visibility = View.GONE
            Analytics.logEvent(
                Event.POP_NATURAL_END_DONE, Event.POP_NATURAL_END_DONE, getScreenName()
            )


            val payload = bundleOf(
                Param.KEY_NAVIGATION_TYPE to if (isWalking) "WALKING" else "DRIVING"
            )
            if (isWalking) {
                WalkathonEngine.getInstance()?.stopCheckingLocationForWalkathon()
            }
            Analytics.logEvent(Event.DESTINATION_ARRIVED, payload)
            isArrived = true
            showArrivedNotification(curTripId)

        }

        override fun onNextRouteLegStart(routeProgress: RouteLegProgress) {
            viewBinding.arrivalNotification.root.visibility = View.GONE
            Timber.e("navigationArrivalObserver-onNextRouteLegStart")
            Timber.e("navigationArrivalObserver-onNextRouteLegStart: $routeProgress")
        }

        override fun onWaypointArrival(routeProgress: RouteProgress) {
            Timber.e("navigationArrivalObserver-onWaypointArrival")
            Timber.e("navigationArrivalObserver-onWaypointArrival: $routeProgress")
        }

    }


    private fun removeNotificationView(roadObjectId: String) {
        if (roadObjectId.isNotEmpty()) {
            Analytics.logNotificationEvents(
                Event.MAP_NOTIFICATION, Event.NOTIFICATION_CLOSE, getScreenName()
            )
            viewBinding.featureNotification.root.visibility = View.GONE
        }
    }

    private fun initEhorizonObserver(mRoute: NavigationRoute) {
        initRoadMetaDataObserver()
        initRoadNotificationObserver()
        val mapboxNavigation = MapboxNavigationApp.current()
        if (mapboxNavigation != null) {
            breezeRoadObjectHandler = BreezeEHorizonProvider.create(
                this@TurnByTurnNavigationActivity,
                breezeERPUtil,
                breezeIncidentsUtil,
                EHorizonMode.NAVIGATION,
                mRoute.directionsRoute
            )
        }
    }

    private fun registerEhorizonNotificationObservers(style: Style, mRoute: NavigationRoute) {
        unregisterEhorizonNotificationObservers(style)
        breezeRoadObjectHandler.addBreezeEdgeMetaDataObserver(breezeEdgeMetaDataObserver)
        breezeRoadObjectHandler.addBreezeNotificationObserver(breezeNotificationObserver)
        breezeRoadObjectHandler.addBreezeNotificationStyle(style)
    }

    private fun unregisterEhorizonNotificationObservers(style: Style) {
        breezeRoadObjectHandler.removeBreezeEdgeMetaDataObserver(breezeEdgeMetaDataObserver)
        breezeRoadObjectHandler.removeBreezeNotificationObserver(breezeNotificationObserver)
        breezeRoadObjectHandler.removeBreezeNotificationStyle(style)
    }


    private fun initRoadNotificationObserver() {
        breezeNotificationObserver = object : BreezeNotificationObserver {
            override fun showNotificationView(
                zone: NavigationZone,
                distance: Int,
                mERPName: String?,
                chargeAmount: String?,
                roadObjectID: String,
                location: String?
            ) {
                this@TurnByTurnNavigationActivity.showNotificationView(
                    zone, distance, mERPName, chargeAmount, location
                );
            }

            override fun removeNotificationView(roadObjectId: String) {
                this@TurnByTurnNavigationActivity.removeNotificationView(roadObjectId);
            }

        }
    }

    private fun initRoadMetaDataObserver() {
        breezeEdgeMetaDataObserver = object : BreezeEdgeMetaDataObserver {
            override fun updateEdgeMetaData(
                speedLimit: Double?, names: List<RoadComponent>?, tunnel: Boolean
            ) {
                if (isWalking) {
                    viewBinding.layoutSpeedLimit.root.visibility = View.GONE
                    SpeedLimitUtil.setCurrentSpeedLimit(-1)
                } else {
                    if (speedLimit != null) {
                        viewBinding.layoutSpeedLimit.root.visibility = View.VISIBLE
                        val roundSpeed = speedLimit.times(3.6).roundToInt()
                        viewBinding.layoutSpeedLimit.roadSpeed.text = roundSpeed.toString()
                        SpeedLimitUtil.setCurrentSpeedLimit(roundSpeed)
                    } else {
                        viewBinding.layoutSpeedLimit.root.visibility = View.GONE
                        SpeedLimitUtil.setCurrentSpeedLimit(-1)
                    }
                }

                if (!isWalking && !names.isNullOrEmpty()) {
                    val currentRoadName = names[0].text
                    if (TextUtils.isEmpty(currentRoadName)) {
                        viewBinding.layoutNavBottom.tvTurnStreetName.visibility = View.INVISIBLE
                    } else {
                        viewBinding.layoutNavBottom.tvTurnStreetName.visibility = View.VISIBLE
                        viewBinding.layoutNavBottom.tvTurnStreetName.text = currentRoadName
                    }
                } else {
                    viewBinding.layoutNavBottom.tvTurnStreetName.visibility = View.INVISIBLE
                }

                viewBinding.tunnelToast.isVisible = tunnel
            }
        }
    }


    fun getScreenName(): String {
        return Screen.NAVIGATION
    }

    override fun onBackPressed() {
        Analytics.logClickEvent(Event.BACK, getScreenName())
        if (!isArrived) {
            showEndNavigationConfirmationDialog()
        }
    }

//    override suspend fun generateNavigationLogData(filePath: String) {
//        Timber.d("filepath=${filePath}")
//        breezeHistoryRecorderUtil.saveHistoryData(
//            this@TurnByTurnNavigationActivity,
//            if (isWalking) "Walking" else "NavigationMode",
//            filePath
//        )
//    }

    override fun getFragmentContainer(): Int {
        return viewBinding.navigationFrameContainer.id
    }


    private fun displayActiveERP(style: Style, featureCollection: FeatureCollection) {

        //Removing all the source and layers relaated to ERP
        style.removeStyleLayer(Constants.ERP_LAYER)
        style.removeStyleSource(Constants.ERP_GEOJSONSOURCE)

        val erpFeatureCollection = featureCollection
        Timber.d("Nav - total number of ERPs displayed ${featureCollection.features()?.size}")

        val icon_size_exp = Expression.interpolate {
            linear()
            zoom()
            stop {
                literal(0.0)
                literal(0.0)
            }
            stop {
                literal(9.0)
                literal(0.0)
            }
            stop {
                literal(12.9)
                literal(0.0)
            }
            stop {
                literal(13.0)
                literal(0.8)
            }
            stop {
                literal(16.4)
                literal(1.0)
            }
        }

        val text_size_exp = Expression.interpolate {
            linear()
            zoom()
            stop {
                literal(0.0)
                literal(0.0)
            }
            stop {
                literal(9.0)
                literal(0.0)
            }
            stop {
                literal(12.9)
                literal(0.0)
            }
            stop {
                literal(13.0)
                literal(14.0)
            }
            stop {
                literal(16.4)
                literal(18.0)
            }
        }

        style.addSource(geoJsonSource(Constants.ERP_GEOJSONSOURCE) {
            featureCollection(erpFeatureCollection)
            //maxzoom(14)
        })

        style.addLayer(symbolLayer(Constants.ERP_LAYER, Constants.ERP_GEOJSONSOURCE) {
            iconImage(
                Constants.ERP_ICON
            )
            iconSize(icon_size_exp)
            iconAnchor(IconAnchor.BOTTOM)
            iconAllowOverlap(true)

            textFont(listOf("SF Pro Display Medium"))
            textField(Expression.get(PROP_ERP_DISPLAY))
            textColor(getColor(R.color.themed_erp_price_text))
            textAnchor(TextAnchor.CENTER)
            textOffset(listOf(0.0, -1.4))
            textSize(text_size_exp)
            iconIgnorePlacement(true)
        })
    }

    fun getMapViewInstance(): MapView? {
        return viewBinding.mapView
    }

    @OptIn(ExperimentalPreviewMapboxNavigationAPI::class)
    override fun onEventFromCar(event: ToAppRxEvent) {
        super.onEventFromCar(event)
        when (event) {
            is AppToPhoneNavigationEndEvent -> {
                if (lifecycle.currentState.isAtLeast(Lifecycle.State.RESUMED)) {
                    doEndNavigation()
                } else {
                    isEndingNavigationOnResume = true
                }
            }

            is UpdateStateVoiceApp -> {
                handleMuteToggleEvent()
            }

            //BREEZE2-1845
//            is UpdateETAPhone -> {
//                handleETALiveLocationToggleEvent()
//            }
//
//            is ETAMessageReceivedPhone -> {
//                showETAMessage(event.etaMessage)
//            }
//
//            is ETAStartedPhone -> {
//                viewBinding.layoutNavBottom.ETAViewTurnByTurn.updateUIOnlyForETA()
//                announceShareDriveStarted()
//            }

            else -> {}
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            Constants.REQUEST_CODES.CONTACTS_REQUEST_CODE_FROM_RN -> {
                if (resultCode == Activity.RESULT_OK && data != null) {
                    handleContacts(data)
                } else if (resultCode == Activity.RESULT_CANCELED) {
                    Analytics.logClickEvent(Event.CANCEL_ADD_CONTACT, getScreenName())
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun handleContacts(data: Intent) {

        val contactDetails = Utils.extractPhoneNumberFromContactIntent(data, this)
        if (contactDetails != null) {
            val params: WritableMap = Arguments.createMap()
            params.putString(Constants.TRIPETA.CONTACT_NAME, contactDetails.contactName)
            params.putString(
                Constants.TRIPETA.CONTACT_NUMBER, contactDetails.phoneNumberList[0]
            )
            ReactNativeEventEmitter.sendEvent(application = application, "SelectEvent", params)
        } else {
            alertDialogHelper.showIncorrectPhoneAlert()
        }
    }


    override fun inflateViewBinding() =
        ActivityTurnByTurnNavigationBinding.inflate(layoutInflater)

    fun showOBUConnectionError() {
        viewBinding.queueAlertView.addOBUConnectionErrorView()
    }

    fun showNoNearbyAmenitiesAlert(type: String) {

        val popupName: String
        val eventValue: String
        val name = when (type) {
            CARPARK -> {
                if (obuCheckSpeedHelper.isStationary()) {
                    isCurrentLocationHasCarPark = false
                }
                popupName = Event.NO_PARKING_VICINITY
                eventValue = Event.CLOSE_POPUP_PARKING_VICINITY
                "carpark"
            }

            PETROL -> {
                popupName = Event.NO_PETROL_VICINITY
                eventValue = Event.CLOSE_POPUP_NO_PETROL_VICINITY
                "petrol station"
            }

            EVCHARGER -> {
                popupName = Event.NO_EV_VICINITY
                eventValue = Event.CLOSE_POPUP_NO_EV_VICINITY

                "EV charger"
            }

            else -> return
        }


        if (popupName.isNotEmpty()) {
            Analytics.logPopupEvent(
                popupName,
                Event.POPUP_OPEN2,
                Screen.OBU_DISPLAY_MODE
            )
        }


        GlobalNotification().setIcon(R.drawable.ic_no_data_warning)
            .setTheme(GlobalNotification.ThemeTutorial)
            .setTitle(getString(R.string.no_available_data))
            .setDescription(getString(R.string.msg_no_available_data, name))
            .setActionText(null).setActionListener(null).setDismissDurationSeconds(10)
            .setSeeListener { isAutoDismiss, isDismissWithoutAction ->
                if (!isAutoDismiss && isDismissWithoutAction) {
                    if (eventValue.isNotEmpty()) {
                        Analytics.logClickEvent(
                            eventValue,
                            getScreenName()
                        )
                    }
                }
            }
            .show(this, 20)
    }
}
package com.ncs.breeze.ui.login.fragment.signUp.module

import com.ncs.breeze.ui.login.fragment.signUp.view.SignUpFragmentBreeze
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class SignUpFragmentBreezeModule {
    @ContributesAndroidInjector
    abstract fun contributeSignUpFragmentBreeze(): SignUpFragmentBreeze
}
package com.ncs.breeze.components

import android.content.Context
import android.content.res.Configuration
import android.content.res.Resources
import android.graphics.Typeface
import android.os.Build
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.RelativeSizeSpan
import android.text.style.StyleSpan
import com.mapbox.navigation.base.TimeFormat
import com.ncs.breeze.R
import java.util.*
import java.util.concurrent.TimeUnit

//Duplicate of Mapbox code
object BreezeTimeFormatter {

    private const val TIME_STRING_FORMAT = " %s "

    @JvmStatic
    fun formatTime(
        time: Calendar,
        routeDuration: Double,
        @TimeFormat.Type type: Int,
        isDeviceTwentyFourHourFormat: Boolean
    ): String {
        time.add(Calendar.SECOND, routeDuration.toInt())
        val chain = TimeFormattingChain()
        return chain.setup(isDeviceTwentyFourHourFormat).obtainTimeFormatted(type, time)
    }

    @JvmStatic
    fun formatTimeRemaining(
        context: Context,
        routeDuration: Double,
        locale: Locale?
    ): SpannableStringBuilder {
        var seconds = routeDuration.toLong()

        if (seconds < 0) {
            seconds = 0L
        }

        val days = TimeUnit.SECONDS.toDays(seconds)
        seconds -= TimeUnit.DAYS.toSeconds(days)
        val hoursAndMinutes = getHoursAndMinutes(seconds)
        val hours = hoursAndMinutes.first
        val minutes = hoursAndMinutes.second

        val textSpanItems = ArrayList<SpanItem>()
        val resources = context.resourcesWithLocale(locale)
        formatDays(resources, days, textSpanItems)
        formatHours(resources, hours, textSpanItems)
        formatMinutes(resources, minutes, textSpanItems)
        formatNoData(resources, days, hours, minutes, textSpanItems)
        return textSpanItems.combineSpan()
    }

    private fun formatDays(resources: Resources, days: Long, textSpanItems: MutableList<SpanItem>) {
        if (days != 0L) {
            val dayQuantityString =
                resources.getQuantityString(R.plurals.mapbox_number_of_days, days.toInt())
            val dayString = String.format(TIME_STRING_FORMAT, dayQuantityString)
            textSpanItems.add(TextSpanItem(StyleSpan(Typeface.BOLD), days.toString()))
            textSpanItems.add(TextSpanItem(RelativeSizeSpan(1f), dayString))
        }
    }

    private fun formatHours(
        resources: Resources,
        hours: Long,
        textSpanItems: MutableList<SpanItem>
    ) {
        if (hours != 0L) {
            val hourString =
                String.format(TIME_STRING_FORMAT, resources.getString(R.string.mapbox_unit_hr))
            textSpanItems.add(TextSpanItem(StyleSpan(Typeface.BOLD), hours.toString()))
            textSpanItems.add(TextSpanItem(RelativeSizeSpan(1f), hourString))
        }
    }

    private fun formatMinutes(
        resources: Resources,
        minutes: Long,
        textSpanItems: MutableList<SpanItem>
    ) {
        if (minutes != 0L) {
            val minuteString =
                String.format(TIME_STRING_FORMAT, resources.getString(R.string.mapbox_unit_min))
            textSpanItems.add(TextSpanItem(StyleSpan(Typeface.BOLD), minutes.toString()))
            textSpanItems.add(TextSpanItem(RelativeSizeSpan(1f), minuteString))
        }
    }

    private fun formatNoData(
        resources: Resources,
        days: Long,
        hours: Long,
        minutes: Long,
        textSpanItems: MutableList<SpanItem>
    ) {
        if (days == 0L && hours == 0L && minutes == 0L) {
            val minuteString =
                String.format(TIME_STRING_FORMAT, resources.getString(R.string.mapbox_unit_min))
            textSpanItems.add(TextSpanItem(StyleSpan(Typeface.BOLD), 1.toString()))
            textSpanItems.add(TextSpanItem(RelativeSizeSpan(1f), minuteString))
        }
    }

    private fun Context.resourcesWithLocale(locale: Locale?): Resources {
        val config = Configuration(this.resources.configuration).also {
            it.setLocale(locale)
        }
        return this.createConfigurationContext(config).resources
    }

    private fun getHoursAndMinutes(seconds: Long): Pair<Long, Long> {
        val initialHoursValue = TimeUnit.SECONDS.toHours(seconds)
        val leftOverSeconds = seconds - TimeUnit.HOURS.toSeconds(initialHoursValue)
        val minutes =
            TimeUnit.SECONDS.toMinutes(leftOverSeconds + TimeUnit.MINUTES.toSeconds(1) / 2)

        return if (minutes == 60L) Pair(initialHoursValue + 1, 0) else Pair(
            initialHoursValue,
            minutes
        )
    }

    internal interface SpanItem {
        val span: Any
    }

    internal fun List<SpanItem>.combineSpan(): SpannableStringBuilder {
        val builder = SpannableStringBuilder()
        for (item in this) {
            if (item is TextSpanItem) {
                builder.appendSupport(item.span, item.spanText)
            }
        }
        return builder
    }

    private fun SpannableStringBuilder.appendSupport(span: Any, spanText: String) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            this.append(spanText, span, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        } else {
            this.append(spanText)
        }
    }

    internal class TextSpanItem(override val span: Any, val spanText: String) : SpanItem

    internal class TimeFormattingChain {

        fun setup(isDeviceTwentyFourHourFormat: Boolean): TimeFormatResolver {
            val noneSpecified = NoneSpecifiedTimeFormat(isDeviceTwentyFourHourFormat)
            val twentyFourHours = TwentyFourHoursTimeFormat(noneSpecified)
            return TwelveHoursTimeFormat(twentyFourHours)
        }
    }

    internal interface TimeFormatResolver {
        fun obtainTimeFormatted(type: Int, time: Calendar): String
    }

    internal class NoneSpecifiedTimeFormat(
        private val isDeviceTwentyFourHourFormat: Boolean
    ) : TimeFormatResolver {

        override fun obtainTimeFormatted(type: Int, time: Calendar): String {
            return if (isDeviceTwentyFourHourFormat) {
                String.format(
                    Locale.getDefault(),
                    TwentyFourHoursTimeFormat.TWENTY_FOUR_HOURS_FORMAT,
                    time,
                    time
                )
            } else {
                String.format(
                    Locale.getDefault(),
                    TwelveHoursTimeFormat.TWELVE_HOURS_FORMAT,
                    time,
                    time,
                    time
                )
            }
        }
    }

    internal class TwentyFourHoursTimeFormat(
        private val chain: TimeFormatResolver
    ) : TimeFormatResolver {

        companion object {
            const val TWENTY_FOUR_HOURS_FORMAT = "%tk:%tM"
            private const val TWENTY_FOUR_HOURS_TYPE = 1
        }

        override fun obtainTimeFormatted(type: Int, time: Calendar): String {
            return if (type == TWENTY_FOUR_HOURS_TYPE) {
                String.format(Locale.getDefault(), TWENTY_FOUR_HOURS_FORMAT, time, time)
            } else {
                chain.obtainTimeFormatted(type, time)
            }
        }
    }


    internal class TwelveHoursTimeFormat(
        private val chain: TimeFormatResolver
    ) : TimeFormatResolver {

        companion object {
            const val TWELVE_HOURS_FORMAT = "%tl:%tM %tp"
            private const val TWELVE_HOURS_TYPE = 0
        }

        override fun obtainTimeFormatted(type: Int, time: Calendar): String {
            return if (type == TWELVE_HOURS_TYPE) {
                String.format(Locale.getDefault(), TWELVE_HOURS_FORMAT, time, time, time)
            } else {
                chain.obtainTimeFormatted(type, time)
            }
        }
    }
}
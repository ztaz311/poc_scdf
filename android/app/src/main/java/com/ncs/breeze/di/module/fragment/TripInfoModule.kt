package com.ncs.breeze.di.module.fragment

import com.ncs.breeze.ui.dashboard.fragments.view.TripInfoFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class TripInfoModule {
    @ContributesAndroidInjector
    abstract fun contributeTripInfoFragment(): TripInfoFragment
}
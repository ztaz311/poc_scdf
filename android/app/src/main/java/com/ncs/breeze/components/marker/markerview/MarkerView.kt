package com.ncs.breeze.components.marker.markerview

import android.graphics.PointF
import android.view.View
import androidx.annotation.NonNull
import com.google.android.gms.maps.model.LatLng
import com.mapbox.geojson.Point
import com.mapbox.maps.MapView
import com.mapbox.maps.MapboxMap
import com.mapbox.maps.ScreenCoordinate

/**
 * Create a MarkerView
 *
 * @param latLng latitude-longitude pair
 * @param view   an Android SDK View
 */
open class MarkerView(
    internal var latLng: LatLng,
    val view: View,
    val mapboxMap: MapboxMap
) {
    private var projection: ScreenCoordinate? = null
    private var onPositionUpdateListener: OnPositionUpdateListener? = null

    /**
     * Update the location of the MarkerView on the map.
     *
     *
     * Provided as a latitude-longitude pair.
     *
     *
     * @param latLng latitude-longitude pair
     */
    fun setLatLng(@NonNull latLng: LatLng, mapView: MapView) {
        this.latLng = latLng
        update()
    }

    /**
     * Set a callback to be invoked when position placement is calculated.
     *
     *
     * Can be used to offset a MarkerView on screen.
     *
     *
     * @param onPositionUpdateListener callback to be invoked when position placement is calculated
     */
    fun setOnPositionUpdateListener(onPositionUpdateListener: OnPositionUpdateListener?) {
        this.onPositionUpdateListener = onPositionUpdateListener
    }

    /**
     * Callback definition that is invoked when position placement of a MarkerView is calculated.
     */
    interface OnPositionUpdateListener {
        fun onUpdate(pointF: PointF?): PointF
    }

    open fun update() {
        val screenCoordinate =
            mapboxMap.pixelForCoordinate(Point.fromLngLat(latLng.longitude, latLng.latitude))
        view.x = screenCoordinate.x.toFloat() - 360
        view.y = screenCoordinate.y.toFloat() - 260
    }
}

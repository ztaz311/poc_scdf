package com.ncs.breeze.components.marker.markerview2

import android.content.Context
import android.location.Address
import android.location.Location
import android.view.LayoutInflater
import android.view.View
import androidx.annotation.UiThread
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import androidx.lifecycle.Lifecycle
import com.breeze.model.DestinationAddressDetails
import com.breeze.model.constants.Constants
import com.breeze.model.extensions.dpToPx
import com.breeze.model.extensions.rn.getIntOrNull
import com.breeze.model.extensions.rn.getMapOrNull
import com.facebook.react.bridge.Arguments
import com.facebook.react.bridge.ReadableMap
import com.mapbox.geojson.Point
import com.mapbox.maps.MapView
import com.ncs.breeze.R
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.extensions.android.generateName
import com.ncs.breeze.common.extensions.android.getApp
import com.ncs.breeze.common.extensions.android.getShareBusinessLogicPackage
import com.ncs.breeze.common.rxbus.RxBus
import com.ncs.breeze.common.rxbus.RxEvent
import com.ncs.breeze.common.utils.LocationBreezeManager
import com.ncs.breeze.common.utils.Utils
import com.ncs.breeze.databinding.MapTooltipDropPinBinding
import com.ncs.breeze.reactnative.nativemodule.ShareBusinessLogicEvent
import com.ncs.breeze.ui.dashboard.activity.DashboardActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class DropPinMarkerTooltip constructor(
    mapView: MapView,
    itemId: String? = null,
    private val parentScreen: Screen
) : MarkerView2(mapboxMap = mapView.getMapboxMap(), itemId = itemId) {

    private var viewBinding: MapTooltipDropPinBinding
    private val compositeDisposable = CompositeDisposable()
    private var currentPinAddress: PinAddress? = null
    var analyticsScreenName = ""
    var onCloseTooltip: () -> Unit = {}

    init {
        viewBinding = MapTooltipDropPinBinding.inflate(
            mapView.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater,
            null,
            false
        )
        mViewMarker = viewBinding.root
    }

    private fun listenRxEvents() {
        compositeDisposable.addAll(
            RxBus.listen(RxEvent.LandingDropPinSaved::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { event ->
                    onSavingPinResultFromRN(event.pinLocationData)
                })
    }

    private fun showRemoveBookmarkConfirm(pinAddress: PinAddress) {
        Analytics.logClickEvent(
            "[map]:tooltip_saved_unbookmark",
            analyticsScreenName
        )

        AlertDialog.Builder(viewBinding.root.context)
            .setMessage(R.string.desc_delete_saved_place_confirmation_2)
            .setNegativeButton(R.string.cancel_uppercase) { dialog, _ -> dialog.dismiss() }
            .setPositiveButton(R.string.confirm) { dialog, _ ->
                deleteBookmarkedAddress(pinAddress) { bookmarkId ->
                    if (bookmarkId != NO_BOOKMARK_ID) {
                        currentPinAddress = PinAddress(NO_BOOKMARK_ID, currentPinAddress!!.address)
                        updateBookmarkStatusIcon(false)
                    }
                }
                dialog.dismiss()
            }.show()

    }

    private fun onSavingPinResultFromRN(pinLocationData: ReadableMap) {
        val lat = pinLocationData.getString("lat")?.toDoubleOrNull() ?: -1
        val long = pinLocationData.getString("long")?.toDoubleOrNull() ?: -1
        val address1 = pinLocationData.getString("address1")
        val isCurrentPinLocation = currentPinAddress?.let { currentPin ->
            currentPin.address.latitude == lat
                    && currentPin.address.longitude == long
                    && currentPin.address.getAddressLine(0) == address1
        } ?: false
        if (isCurrentPinLocation) {
            currentPinAddress = PinAddress(
                address = currentPinAddress!!.address,
                bookmarkId = pinLocationData.getInt("bookmarkId")
            )
            updateBookmarkStatusIcon(true)
        }
    }

    fun showTooltip() {
        val latLng = mLatLng ?: return
        showMarker()
        viewBinding.tvName.text = ""
        viewBinding.tvDescription.text = "${latLng.latitude},${latLng.longitude}"
        CoroutineScope(Dispatchers.IO).launch {
            currentPinAddress = null
            withContext(Dispatchers.Main){
                viewBinding.root.isInvisible = true
            }
            val address = Utils.getAddressObject(Location("").apply {
                latitude = latLng.latitude
                longitude = latLng.longitude
            }, viewBinding.root.context)

            withContext(Dispatchers.Main) {
                if (address == null) {
                    hideMarker()
                    return@withContext
                }
                showAddressInfo(address)
                updateTooltipPosition()
                viewBinding.root.isVisible = true
            }
        }
    }

    @UiThread
    fun hideTooltip() {
        hideMarker()
        sentHideTooltipEventToRN()
    }

    fun isVisible() = viewBinding.root.isVisible

    private fun sentHideTooltipEventToRN() {
        viewBinding.root.context.getApp()?.shareBusinessLogicPackage?.getRequestsModule()
            ?.flatMap {
                it.callRN(
                    if (parentScreen == Screen.COLLECTION_DETAILS)
                        ShareBusinessLogicEvent.TOGGLE_DROP_PIN_COLLECTION_DETAILS.name
                    else ShareBusinessLogicEvent.TOGGLE_DROP_PIN_LANDING.name,
                    Arguments.fromBundle(bundleOf("isDropPin" to false))
                )
            }?.subscribeOn(Schedulers.io())?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe()
            ?.also { compositeDisposable.add(it) }
    }

    @UiThread
    private fun showAddressInfo(address: Address) {
        updatePinDroppedToRN(address) {
            val isBookmarked = it.bookmarkId != NO_BOOKMARK_ID
            currentPinAddress = it
            updateBookmarkStatusIcon(isBookmarked)
            viewBinding.imgFlagSaved.setOnClickListener {
                val pinAddress = currentPinAddress?.takeIf { current -> current.address == address }
                    ?: return@setOnClickListener

                if (pinAddress.bookmarkId != NO_BOOKMARK_ID) {
                    showRemoveBookmarkConfirm(pinAddress)
                } else {
                    bookmarkDropPinAddress(pinAddress.address)
                }
            }
        }

        viewBinding.tvName.text = address.generateName()
        viewBinding.tvDescription.text = address.getAddressLine(0)
        viewBinding.buttonNavigateHere.setOnClickListener { onNavigateHereClicked(it, address) }

        viewBinding.buttonShare.setOnClickListener {
            Analytics.logClickEvent(
                "[map]:tooltip_saved_share",
                analyticsScreenName
            )
            if (currentPinAddress == null || currentPinAddress?.address != address) return@setOnClickListener
            shareDropPinAddress(address)
        }

        viewBinding.buttonInvite.setOnClickListener {
            Analytics.logClickEvent(
                "[map]:tooltip_saved_invite",
                analyticsScreenName
            )
            if (currentPinAddress == null || currentPinAddress?.address != address) return@setOnClickListener
            inviteDropPinAddress(address)
        }

        viewBinding.buttonClose.setOnClickListener {
            Analytics.logClickEvent("[card_drop_pin]:close", analyticsScreenName)
            onCloseTooltip.invoke()
        }

        viewBinding.root.bringToFront()
        updateTooltipPosition()
    }

    private fun onNavigateHereClicked(view: View, address: Address) {
        (view.context as? DashboardActivity)?.takeIf {
            it.lifecycle.currentState.isAtLeast(
                Lifecycle.State.STARTED
            ) && address == currentPinAddress?.address
        }?.let {
            Analytics.logClickEvent("[map]:tooltip_saved_navigate_here", analyticsScreenName)
            showRouteToPin(it, address)
        }
    }

    @UiThread
    private fun updateBookmarkStatusIcon(isBookmarked: Boolean) {
        viewBinding.imgFlagSaved.setImageDrawable(
            ContextCompat.getDrawable(
                viewBinding.root.context,
                if (isBookmarked) R.drawable.ic_bookmarked else R.drawable.ic_nobookmark
            )
        )
    }

    private fun showRouteToPin(dashboardActivity: DashboardActivity, address: Address) {
        val destinationDetail = DestinationAddressDetails(
            lat = "${address.latitude}",
            long = "${address.longitude}",
            destinationName = address.generateName(),
            address1 = address.getAddressLine(0),
            isCarPark = false,
        )
        val currentLocation = LocationBreezeManager.getInstance().currentLocation
        val sourceAddressDetails = DestinationAddressDetails(
            null,
            address1 = null,
            null,
            currentLocation?.latitude?.toString(),
            currentLocation?.longitude?.toString(),
            null,
            Constants.TAGS.CURRENT_LOCATION_TAG,
            null,
            -1
        )
        dashboardActivity.showRouteAndResetToCenter(
            bundleOf(
                Constants.DESTINATION_ADDRESS_DETAILS to destinationDetail,
                Constants.SOURCE_ADDRESS_DETAILS to sourceAddressDetails
            )
        )
    }

    private fun shareDropPinAddress(address: Address) {
        Analytics.logClickEvent("[map]:tooltip_saved_share", analyticsScreenName)
        viewBinding.root.context.getShareBusinessLogicPackage()?.getRequestsModule()?.flatMap {
            it.callRN(
                ShareBusinessLogicEvent.SHARE_LOCATION_FROM_NATIVE.eventName,
                Arguments.fromBundle(
                    bundleOf(
                        "address1" to address.getAddressLine(0),
                        "latitude" to address.latitude.toString(),
                        "longitude" to address.longitude.toString(),
                        "name" to address.generateName(),
                    )
                )
            )
        }?.subscribeOn(Schedulers.io())?.observeOn(AndroidSchedulers.mainThread())?.subscribe()
    }

    private fun inviteDropPinAddress(address: Address) {
        Analytics.logClickEvent("[map]:tooltip_saved_share", analyticsScreenName)
        viewBinding.root.context.getShareBusinessLogicPackage()?.getRequestsModule()?.flatMap {
            it.callRN(
                ShareBusinessLogicEvent.LOCATION_INVITE_FROM_NATIVE.eventName,
                Arguments.fromBundle(
                    bundleOf(
                        "address1" to address.getAddressLine(0),
                        "latitude" to address.latitude.toString(),
                        "longitude" to address.longitude.toString(),
                        "name" to address.generateName(),
                    )
                )
            )
        }?.subscribeOn(Schedulers.io())?.observeOn(AndroidSchedulers.mainThread())?.subscribe()
    }

    private fun deleteBookmarkedAddress(
        pinAddress: PinAddress,
        onResult: (bookmarkId: Int) -> Unit
    ) {
        Analytics.logClickEvent("[map]:tooltip_saved_unbookmark", analyticsScreenName)
        viewBinding.root.context.getApp()?.shareBusinessLogicPackage?.getRequestsModule()
            ?.flatMap {
                val data = Arguments.fromBundle(
                    bundleOf(
                        "action" to "DELETE",
                        "bookmarkId" to pinAddress.bookmarkId
                    )
                )
                it.callRN(
                    if (parentScreen == Screen.COLLECTION_DETAILS)
                        ShareBusinessLogicEvent.TOGGLE_SAVE_LOCATION_COLLECTION_DETAILS.name
                    else ShareBusinessLogicEvent.TOGGLE_SAVE_DROP_PIN_LOCATION_LANDING.name,
                    data
                )
            }?.subscribeOn(Schedulers.io())?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe { result ->
                // Check if by the time result arrived,  user still in same pin
                val deletedBookmarkId =
                    result.getMapOrNull("data")?.getIntOrNull("bookmarkId") ?: NO_BOOKMARK_ID
                if (pinAddress.address == currentPinAddress?.address && deletedBookmarkId != NO_BOOKMARK_ID) {
                    onResult.invoke(deletedBookmarkId)
                }
            }
            ?.also { compositeDisposable.add(it) }
    }

    private fun bookmarkDropPinAddress(address: Address) {
        Analytics.logClickEvent("[map]:tooltip_saved_bookmark", analyticsScreenName)
        viewBinding.root.context.getApp()?.shareBusinessLogicPackage?.getRequestsModule()
            ?.flatMap {
                val data = Arguments.fromBundle(
                    bundleOf(
                        "action" to "ADD",
                        "bookmark" to bundleOf(
                            "lat" to address.latitude.toString(),
                            "long" to address.longitude.toString(),
                            "address1" to address.getAddressLine(0),
                            "name" to address.generateName()
                        )
                    )
                )
                it.callRN(
                    if (parentScreen == Screen.COLLECTION_DETAILS)
                        ShareBusinessLogicEvent.TOGGLE_SAVE_LOCATION_COLLECTION_DETAILS.name
                    else ShareBusinessLogicEvent.TOGGLE_SAVE_DROP_PIN_LOCATION_LANDING.name,
                    data
                )
            }?.subscribeOn(Schedulers.io())?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe()
            ?.also { compositeDisposable.add(it) }
    }

    private fun updatePinDroppedToRN(
        address: Address,
        onResult: (pinAddress: PinAddress) -> Unit
    ) {
        viewBinding.root.context.getApp()?.shareBusinessLogicPackage?.getRequestsModule()
            ?.flatMap {
                val payload = Arguments.fromBundle(
                    bundleOf(
                        "isDropPin" to true,
                        "data" to bundleOf(
                            "lat" to address.latitude.toString(),
                            "long" to address.longitude.toString(),
                            "address1" to address.getAddressLine(0),
                            "name" to address.generateName()
                        )
                    )
                )
                it.callRN(
                    if (parentScreen == Screen.COLLECTION_DETAILS)
                        ShareBusinessLogicEvent.TOGGLE_DROP_PIN_COLLECTION_DETAILS.name
                    else ShareBusinessLogicEvent.TOGGLE_DROP_PIN_LANDING.name,
                    payload
                )
            }?.subscribeOn(Schedulers.io())?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe { result ->
                val bookmarkId = result.getMapOrNull("data")?.getIntOrNull("bookmarkId") ?: NO_BOOKMARK_ID
                onResult.invoke(PinAddress(bookmarkId, address))
            }
            ?.also { compositeDisposable.add(it) }
    }

    override fun updateTooltipPosition() {
        val latLng = mLatLng ?: return
        val viewMarker = mViewMarker ?: return
        val screenCoordinate = mapboxMap.pixelForCoordinate(
            Point.fromLngLat(latLng.longitude, latLng.latitude)
        )
        viewMarker.measure(
            View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
            View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
        )
        viewMarker.x = screenCoordinate.x.toFloat() - (viewMarker.measuredWidth / 2)
        viewMarker.y = screenCoordinate.y.toFloat() - viewMarker.measuredHeight - 24.dpToPx()
    }

    override fun showMarker() {
        super.showMarker()
        viewBinding.layoutContent.isVisible = true
        viewBinding.layoutContent.isClickable = true
        listenRxEvents()
    }

    override fun hideMarker() {
        super.hideMarker()
        currentPinAddress = null
        compositeDisposable.clear()
        viewBinding.layoutContent.isClickable = false
        viewBinding.layoutContent.isVisible = false
        viewBinding.buttonShare.setOnClickListener(null)
        viewBinding.buttonNavigateHere.setOnClickListener(null)
        viewBinding.imgFlagSaved.setOnClickListener(null)

    }

    inner class PinAddress(
        val bookmarkId: Int,
        val address: Address
    )

    enum class Screen {
        LANDING,
        COLLECTION_DETAILS,
    }

    companion object {
        private const val NO_BOOKMARK_ID = -1
    }
}
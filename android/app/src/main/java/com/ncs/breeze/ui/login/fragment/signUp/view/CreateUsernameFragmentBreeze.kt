package com.ncs.breeze.ui.login.fragment.signUp.view

import android.app.Dialog
import android.os.Bundle
import android.text.Editable
import android.text.InputFilter
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.Event
import com.ncs.breeze.common.utils.BreezeUserPreferenceUtil
import com.breeze.model.constants.Constants
import com.ncs.breeze.common.utils.Utils.isValidUserNameWithBreezeName
import com.ncs.breeze.car.breeze.utils.BreezeCarUtil
import com.ncs.breeze.R
import com.breeze.customization.view.BreezeEditText
import com.ncs.breeze.databinding.FragmentCreateUsernameBreezeBinding
import com.ncs.breeze.helper.PermissionHelper
import com.ncs.breeze.ui.base.BaseFragment
import com.ncs.breeze.ui.login.activity.LoginActivity
import com.ncs.breeze.ui.login.fragment.signUp.viewmodel.CreateUserNameFragmentBreezeViewModel
import com.ncs.breeze.common.analytics.Screen
import javax.inject.Inject

class CreateUsernameFragmentBreeze :
    BaseFragment<FragmentCreateUsernameBreezeBinding, CreateUserNameFragmentBreezeViewModel>() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val viewModel: CreateUserNameFragmentBreezeViewModel by viewModels {
        viewModelFactory
    }

    @Inject
    lateinit var userPreferenceUtil: BreezeUserPreferenceUtil

    lateinit var dialog: Dialog
    var fromScreen: String? = ""


    companion object {

        val SCREEN_NAME = "CreateUsernameFragmentBreeze"

        val FROM_SCREEN = "FROM_SCREEN"

        @JvmStatic
        fun newInstance() =
            CreateUsernameFragmentBreeze().apply {
                arguments = Bundle().apply {
                }
            }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fromScreen = (arguments?.getString(TermsAndCondnFragment.FROM_SCREEN, "")) ?: ""
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        PermissionHelper.requestLocationPermission(view.context)
        initialize()
    }

    override fun onBackPressed() {}

    private fun initialize() {
        viewBinding.editUsername.getEditText()
            .setOnEditorActionListener(TextView.OnEditorActionListener { _, actionId, _ ->
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    if (!viewBinding.editUsername.getEnteredText().isNullOrBlank()) {
                        handleUpdateUserName()
                        return@OnEditorActionListener true
                    }
                }
                false
            })
        viewBinding.editUsername.getEditText().filters =
            arrayOf<InputFilter>(InputFilter.LengthFilter(Constants.MAX_USERNAME_LENGTH))
        viewBinding.editUsername.showTextLengthView()
        viewBinding.editUsername.setTextForLength(Constants.MAX_USERNAME_LENGTH.toString())
        viewBinding.editUsername.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}

            override fun afterTextChanged(s: Editable?) {
                if (s != null) {
                    viewBinding.editUsername.setTextForLength((Constants.MAX_USERNAME_LENGTH - s.length).toString())
                }
            }

        })

        viewBinding.editUsername.setEditTextListener(object :
            BreezeEditText.BreezeEditTextListener {
            override fun onBreezeEditTextFocusChange(isFocused: Boolean) {
                if (!isFocused && !viewBinding.editUsername.getEnteredText().isNullOrBlank()) {
                    if (!isValidUserNameWithBreezeName(
                            BreezeCarUtil.listNameBlackList,
                            viewBinding.editUsername.getEnteredText()
                        )
                    ) {
                        viewBinding.editUsername.showErrorView(resources.getString(R.string.error_account_user_name_contain_breeze))
                    } else if (!isValidUserName(viewBinding.editUsername.getEnteredText())) {
                        viewBinding.editUsername.showErrorView(resources.getString(R.string.error_account_name))
                    } else {
                        viewBinding.editUsername.hideErrorView()
                    }
                }
            }
        })

        viewBinding.editUsername.getEditText()
            .setOnClickListener(object : View.OnClickListener {
                private var isAnalyticsEventLogged = false
                override fun onClick(v: View?) {
                    if (!isAnalyticsEventLogged) {
                        when (fromScreen) {
                            TermsAndCondnFragment.FROM_GUEST -> {
                                Analytics.logEditEvent(
                                    Event.USRNME_INPUT,
                                    Screen.ONBOARDING_GUEST_SIGNUP
                                )
                            }
                            TermsAndCondnFragment.FROM_CREATE_ACCOUNT -> {
                                Analytics.logEditEvent(
                                    Event.USRNME_INPUT,
                                    Screen.ONBOARDING_CREATE_ACC_SIGNUP
                                )
                            }
                            TermsAndCondnFragment.FROM_SIGN_IN -> {
                                Analytics.logEditEvent(Event.USRNME_INPUT, Screen.ONBOARDING_SIGNIN)
                            }
                        }
                    }
                    isAnalyticsEventLogged = true
                }

            })

        viewBinding.editUsername.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun afterTextChanged(p0: Editable?) {
                viewBinding.editUsername.hideErrorView()
            }
        })

        viewBinding.editUsername.getEditText().requestFocus()

    }

    private fun handleUpdateUserName() {
        Analytics.logClickEvent(Event.USRNME_INPUT, Screen.CREATE_USER_NAME_ACTIVITY)
        val username = viewBinding.editUsername.getEnteredText()
        if (!isValidUserName(username)) {
            viewBinding.editUsername.showErrorView(resources.getString(R.string.error_account_name))
            return
        }

        if (!isValidUserNameWithBreezeName(
                BreezeCarUtil.listNameBlackList,
                viewBinding.editUsername.getEnteredText()
            )
        ) {
            viewBinding.editUsername.showErrorView(resources.getString(R.string.error_account_user_name_contain_breeze))
            return
        }
        userPreferenceUtil.saveUserName(username)
        (activity as? LoginActivity)?.hideKeyboard()

        /**
         * show dialog success login
         */
        (activity as? LoginActivity)?.showDialogSuccess(
            isExistUser = false,
            fromScreen ?: TermsAndCondnFragment.FROM_GUEST
        )
    }

    override fun inflateViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?,
        attachToContainer: Boolean
    )= FragmentCreateUsernameBreezeBinding.inflate(inflater, container, attachToContainer)

    override fun getViewModelReference(): CreateUserNameFragmentBreezeViewModel {
        return viewModel
    }

    override fun getScreenName(): String {
        return Screen.ONBOARDING_CREATE_ACCOUNT_STEP3
    }

    override fun onKeyboardVisibilityChanged(isOpen: Boolean) {
    }


    override fun onDestroy() {
        super.onDestroy()
    }

    override fun onPause() {
        super.onPause()
        if (::dialog.isInitialized && dialog.isShowing) {
            dialog.dismiss()
        }
    }
}
package com.ncs.breeze.ui.dashboard.fragments.obuconnect

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import com.google.android.flexbox.AlignItems
import com.google.android.flexbox.FlexDirection
import com.google.android.flexbox.FlexWrap
import com.google.android.flexbox.FlexboxLayoutManager
import com.google.android.flexbox.JustifyContent
import com.ncs.breeze.common.constant.OBUConnectionState
import com.ncs.breeze.common.extensions.android.getApp
import com.breeze.model.api.response.ObuVehicleDetails
import com.ncs.breeze.databinding.FragmentObuPairedListBinding
import com.ncs.breeze.ui.base.BaseFragment
import com.ncs.breeze.ui.dashboard.activity.DashboardActivity
import com.ncs.breeze.ui.dashboard.fragments.adapter.PairedVehicleItemAdapter
import com.ncs.breeze.ui.dashboard.fragments.viewmodel.OBUPairedListViewModel
import timber.log.Timber
import javax.inject.Inject

class OBUPairedListFragment : BaseFragment<FragmentObuPairedListBinding, OBUPairedListViewModel>() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val mViewModel: OBUPairedListViewModel by viewModels {
        viewModelFactory
    }


    private lateinit var pairedVehicleItemAdapter: PairedVehicleItemAdapter

    private var pairedVehicleData: ArrayList<ObuVehicleDetails>? = null
    private var pairedVehicleNumber: String? = null

    override fun inflateViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?,
        attachToContainer: Boolean
    ) = FragmentObuPairedListBinding.inflate(inflater, container, attachToContainer)

    override fun getViewModelReference() = mViewModel

    override fun getScreenName() = "OBUPairedListFragment"

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initializeView()

    }

    private fun initializeView() {

        viewBinding.backButton.setOnClickListener {
            onBackPressed()
        }

        viewBinding.tvObuPair.setOnClickListener {
            activity?.let {
                (it as DashboardActivity).openConnectOBUDeviceScreen()
            }
        }

        observeOBUConnectionStateChanged()
        initVehicleAdapter()
        handleDataSetResponse()
        loadObuDevices()

    }


    private fun observeOBUConnectionStateChanged() {
        activity?.getApp()?.obuConnectionHelper?.obuConnectionState?.observe(viewLifecycleOwner) { state ->
            Timber.d("Travel Log observeOBUConnectionStateChanged: $state")
            when (state) {
                OBUConnectionState.CONNECTED -> {
                    var reload = determineIfDatasetShouldBeReloaded()
                    if (reload) {
                        loadObuDevices()
                    } else {
                        handleShowLoadingProgress(false)
                        updateDataSet()
                    }
                }

                else -> {
                    // No Operation
                }
            }
        }
    }

    private fun determineIfDatasetShouldBeReloaded(): Boolean {
        var reload = false
        var obuConnectionHelper = activity?.getApp()?.obuConnectionHelper
        val obuDataList = obuConnectionHelper?.getUserConnectedOBUDevices()
        if (obuDataList != null) {
            pairedVehicleData?.let { pairedVehicleData ->
                if (obuDataList.size != pairedVehicleData.size) {
                    reload = true
                } else {
                    obuDataList.forEach { obuList ->
                        val foundElement =
                            pairedVehicleData.find { it.vehicleNumber == obuList.vehicleNumber }
                        if (foundElement == null) {
                            reload = true
                        }
                    }
                }
            }
        }
        return reload
    }

    private fun updateDataSet() {
        var obuConnectionHelper = activity?.getApp()?.obuConnectionHelper
        val obuDataList = obuConnectionHelper?.getUserConnectedOBUDevices()
        obuDataList?.let { obuList ->
            if (obuList.isNotEmpty()) {
                val latestVehicleNumber = obuList.last().vehicleNumber
                if (obuConnectionHelper?.hasActiveOBUConnection() == true) {
                    pairedVehicleNumber = latestVehicleNumber
                }
                pairedVehicleItemAdapter.dataList.forEachIndexed { index, obuVehicleDetails ->
                    if (obuVehicleDetails.vehicleNumber == pairedVehicleNumber) {
                        pairedVehicleItemAdapter.selectedIndex = index
                    }
                }
                pairedVehicleItemAdapter.notifyDataSetChanged()
            }
        }
    }

    private fun handleDataSetResponse() {
        mViewModel.vehicleDetailsResponseEvent.observe(viewLifecycleOwner) { obuVehicleResponse ->
            handleShowLoadingProgress(false)
            obuVehicleResponse?.let {
                pairedVehicleData = ArrayList(it.data)
                pairedVehicleData?.let { pairedVehicleDataList ->
                    if (pairedVehicleNumber != null) {
                        for (index in pairedVehicleDataList.indices) {
                            if (pairedVehicleDataList[index].vehicleNumber == pairedVehicleNumber) {
                                pairedVehicleItemAdapter.selectedIndex = index
                                break;
                            }
                        }
                    }
                    pairedVehicleItemAdapter.dataList = pairedVehicleDataList
                    pairedVehicleItemAdapter.notifyDataSetChanged()
                }

            }
        }
    }

    private fun initVehicleAdapter() {
        pairedVehicleItemAdapter = PairedVehicleItemAdapter(
            requireContext(),
            listOf(),
            null,
            pairedItemListener
        )
        initAmenityAdapterLayoutManager()
        viewBinding.vehicleSelector.adapter = pairedVehicleItemAdapter
    }

    private fun initAmenityAdapterLayoutManager() {
        activity?.let {
            val layoutManager = FlexboxLayoutManager(it)
            layoutManager.flexWrap = FlexWrap.WRAP
            layoutManager.flexDirection = FlexDirection.ROW
            layoutManager.justifyContent = JustifyContent.FLEX_START
            layoutManager.alignItems = AlignItems.FLEX_START
            viewBinding.vehicleSelector.overScrollMode = View.OVER_SCROLL_NEVER
            viewBinding.vehicleSelector.layoutManager = layoutManager

        }
    }


    private fun loadObuDevices() {
        var obuConnectionHelper = activity?.getApp()?.obuConnectionHelper
        obuConnectionHelper?.let { obuConnectionHelper ->
            val obuDataList = obuConnectionHelper.getUserConnectedOBUDevices()
            obuDataList?.let { obuList ->
                if (obuList.isNotEmpty()) {
                    val latestVehicleNumber = obuList.last().vehicleNumber
                    val vehicleNumbers = obuList.map {
                        return@map it.vehicleNumber
                    }
                    mViewModel.getObuVehicleList(vehicleNumbers)
                    handleShowLoadingProgress()
                    if (obuConnectionHelper.hasActiveOBUConnection()) {
                        pairedVehicleNumber = latestVehicleNumber
                    }
                }
            }
        }
    }


    private fun handleShowLoadingProgress(show: Boolean = true) {
        if (show) {
            viewBinding?.progressBarTravelLog?.visibility = View.VISIBLE
        } else {
            viewBinding?.progressBarTravelLog?.visibility = View.GONE
        }
    }

    private val pairedItemListener = object : PairedVehicleItemAdapter.PairedItemListener {
        override fun onItemClick(data: ObuVehicleDetails) {
            var obuConnectionHelper = activity?.getApp()?.obuConnectionHelper
            obuConnectionHelper?.let { obuConnectionHelper ->
                if (pairedVehicleNumber != null && pairedVehicleNumber == data.vehicleNumber) {
                    obuConnectionHelper.disconnectOBU()
                    pairedVehicleNumber = null;
                    pairedVehicleItemAdapter.selectedIndex = null;
                    pairedVehicleItemAdapter.notifyDataSetChanged()
                } else {
                    if (pairedVehicleNumber != null) {
                        obuConnectionHelper.disconnectOBU()
                        pairedVehicleItemAdapter.selectedIndex = null;
                        pairedVehicleNumber = null;
                    }
//                    obuConnectionHelper.startOBUConnection(data.vehicleNumber) //todo unused
                    handleShowLoadingProgress()
                }
            }
        }

        override fun onItemDelete(data: ObuVehicleDetails) {
            //
            var obuConnectionHelper = activity?.getApp()?.obuConnectionHelper
            obuConnectionHelper?.disconnectOBU()
            pairedVehicleItemAdapter.selectedIndex = null;
            pairedVehicleNumber = null;
            pairedVehicleData?.let { pairedVehicleDataList ->
                pairedVehicleDataList.remove(data)
                pairedVehicleItemAdapter.dataList = pairedVehicleDataList
            }
            pairedVehicleItemAdapter.notifyDataSetChanged()
        }
    }

    override fun onDestroyView() {
        activity?.getApp()?.obuConnectionHelper?.obuConnectionState?.removeObservers(
            viewLifecycleOwner
        )
        super.onDestroyView()
    }


}
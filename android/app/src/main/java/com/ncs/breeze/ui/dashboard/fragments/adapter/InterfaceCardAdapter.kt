package com.ncs.breeze.ui.dashboard.fragments.adapter

import androidx.cardview.widget.CardView

interface InterfaceCardAdapter {
    val baseElevation: Float
    fun getCardViewAt(position: Int): CardView?

    val count: Int

    companion object {
        const val MAX_ELEVATION_FACTOR = 8
    }
}
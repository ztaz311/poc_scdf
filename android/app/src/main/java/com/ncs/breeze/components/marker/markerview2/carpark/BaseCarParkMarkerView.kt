package com.ncs.breeze.components.marker.markerview2.carpark

import android.app.Activity
import android.content.res.Resources
import android.view.View
import com.mapbox.geojson.Point
import com.mapbox.maps.MapboxMap
import com.ncs.breeze.components.marker.markerview2.MarkerView2

open class BaseCarParkMarkerView(
    mapboxMap: MapboxMap,
    val activity: Activity,
    itemId: String? = null
) : MarkerView2(mapboxMap = mapboxMap, itemId) {

    protected var zoomLevel = 16.0
    protected val pixelDensity = Resources.getSystem().displayMetrics.density

    override fun updateTooltipPosition() {
        mLatLng?.let { location ->
            val screenCoordinate = mapboxMap.pixelForCoordinate(
                Point.fromLngLat(
                    location.longitude,
                    location.latitude
                )
            )
            mViewMarker?.let {
                it.measure(
                    View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                    View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
                );
                it.x = screenCoordinate.x.toFloat() - (it.measuredWidth / 2)
                it.y = screenCoordinate.y.toFloat() - it.measuredHeight
            }
        }
    }
}
package com.ncs.breeze.common.utils


import android.content.Context
import android.util.Log
import com.breeze.model.ERPFeatures
import com.google.gson.Gson
import com.mapbox.geojson.FeatureCollection
import com.mapbox.geojson.Point
import com.mapbox.navigation.base.trip.model.eh.MatchableGeometry
import com.mapbox.navigation.base.trip.model.eh.MatchablePoint
import com.mapbox.navigation.core.MapboxNavigation
import com.mapbox.navigation.core.lifecycle.MapboxNavigationApp
import com.mapbox.navigation.core.trip.session.eh.RoadObjectMatcherObserver
import com.ncs.breeze.common.extensions.android.getAppPreference
import com.breeze.model.NavigationZone
import com.breeze.model.enums.TrafficIncidentData
import com.breeze.model.api.response.NavigationZoneData
import com.ncs.breeze.common.rxbus.RxBus
import com.ncs.breeze.common.rxbus.RxEvent
import com.ncs.breeze.common.storage.AppPrefsKey
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import timber.log.Timber
import java.util.concurrent.ConcurrentHashMap
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class BreezeFeatureDetectionUtil @Inject constructor(
    val appContext: Context,
    breezeERPRefreshUtil: BreezeERPRefreshUtil,
    breezeTrafficIncidentsUtil: BreezeTrafficIncidentsUtil,
    breezeMiscRoadObjectUtil: BreezeMiscRoadObjectUtil,
) {
    var breezeERPRefreshUtil = breezeERPRefreshUtil
    var breezeIncidentsUtil = breezeTrafficIncidentsUtil
    var compositeDisposable: CompositeDisposable = CompositeDisposable()

    var currentIncidentObjects: ArrayList<String> = arrayListOf()
    var miscROObjects: ArrayList<String> = arrayListOf()

    var featureDetectionEnabled: Boolean = false;

    val breezeMiscRoadObjectUtil = breezeMiscRoadObjectUtil
    var roadObjectMatcherObserver: RoadObjectMatcherObserver? = null

    suspend fun initFeatureDetectionRoadObjects() {
        RoadObjectMatcherObserver { result ->
            result.value?.let {
                MapboxNavigationApp.current()?.roadObjectsStore?.addCustomRoadObject(it)
            }
        }.also {
            roadObjectMatcherObserver = it
            MapboxNavigationApp.current()?.roadObjectMatcher?.registerRoadObjectMatcherObserver(it)
        }

        synchronized(this) {
            if (!featureDetectionEnabled) {
                featureDetectionEnabled = true
                addingRoadObjects()
                listenForRoadObjectUpdates()
            }
        }
    }

    private fun listenForRoadObjectUpdates() {
        compositeDisposable.add(
            RxBus.listen(RxEvent.IncidentDataRefresh::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    removeAndAddIncidentRoadObjects(MapboxNavigationApp.current())
                })

        compositeDisposable.add(
            RxBus.listen(RxEvent.MiscRODataRefresh::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    removeAndAddMiscRoadObjects(MapboxNavigationApp.current())
                })


        compositeDisposable.add(
            RxBus.listen(RxEvent.ERPDataChange::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    addingRoadObjects()
                })

        compositeDisposable.add(
            RxBus.listen(RxEvent.SchoolSilverZoneDataChange::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    addingRoadObjects()
                })

    }

    fun stopFeatureDetection() {

        if (!compositeDisposable.isDisposed) {
            compositeDisposable.clear()
            //compositeDisposable.dispose()
        }
        featureDetectionEnabled = false
    }


    fun addingRoadObjects() {
        val mapboxNavigation = MapboxNavigationApp.current() ?: return
        mapboxNavigation.roadObjectsStore.removeAllCustomRoadObjects()
        mapboxNavigation.roadObjectMatcher.cancelAll()
        breezeERPRefreshUtil.getCurrentERPData()?.let {
            Timber.d("BreezeFeatureDetectionUtil - Road objects are going to be added")
            addERPObjects(mapboxNavigation, it)
        }
        addSchoolAndSilverZoneObjects(mapboxNavigation)
        addTrafficIncidentObjects(
            mapboxNavigation,
            breezeIncidentsUtil.getLatestIncidentData()
        )
        addMiscROObjects(
            mapboxNavigation,
            breezeMiscRoadObjectUtil.getLatestMiscROData()
        )
        addSeasonParkingZoneObjects(mapboxNavigation)
    }

    fun removeAndAddMiscRoadObjects(mapboxNavigation: MapboxNavigation?) {
        if (mapboxNavigation == null) return
        for (roadObjectID in miscROObjects) {
            mapboxNavigation.roadObjectsStore.removeCustomRoadObject(roadObjectID)
        }
        addMiscROObjects(mapboxNavigation, breezeMiscRoadObjectUtil.getLatestMiscROData())
    }

    private fun removeAndAddIncidentRoadObjects(mapboxNavigation: MapboxNavigation?) {
        if (mapboxNavigation == null) return
        for (roadObjectID in currentIncidentObjects) {
            mapboxNavigation.roadObjectsStore.removeCustomRoadObject(roadObjectID)
        }
        addTrafficIncidentObjects(mapboxNavigation, breezeIncidentsUtil.getLatestIncidentData())
    }

    private fun addSchoolAndSilverZoneObjects(mapboxNavigation: MapboxNavigation?) {
        if (mapboxNavigation == null) return
        val schoolZones = retrieveDataFromSharedPref(AppPrefsKey.SCHOOL_ZONE_RESPONSE)
        val silverZones = retrieveDataFromSharedPref(AppPrefsKey.SILVER_ZONE_RESPONSE)

        val schoolPointsList = ArrayList<ArrayList<Point>>()
        val silverPointsList = ArrayList<ArrayList<Point>>()

        val listOfMatchableGeometry: ArrayList<MatchableGeometry> = arrayListOf()

        schoolZones?.run {
            features?.forEach { feature ->
                val points = ArrayList<Point>()
                if (feature.geometry?.coordinates?.isNotEmpty() == true) {
                    feature.geometry?.coordinates?.get(0)?.forEach { coordinate ->
                        if (coordinate.isNotEmpty() && coordinate.size == 2) {
                            val x = coordinate[0]
                            val y = coordinate[1]
                            if (x is Double && y is Double)
                                points.add(Point.fromLngLat(x, y))
                        }
                    }
                }
                if (points.isNotEmpty() && points.size > 2) {
                    schoolPointsList.add(points)
                }
            }
        }

        // Adding School Zone
        for ((index, point) in schoolPointsList.withIndex()) {
            /*mapboxNavigation.roadObjectMatcher.matchPolygonObject(
                Companion.SCHOOL_ZONE + index,
                point
            )*/
            listOfMatchableGeometry.add(
                MatchableGeometry(RoadObjectUtil.generateSchoolZoneRoadObjectName(index), point)
            )

        }

        silverZones?.run {
            features?.forEach { feature ->
                val points = ArrayList<Point>()
                if (feature.geometry?.coordinates?.isNotEmpty() == true) {
                    feature.geometry?.coordinates?.get(0)?.forEach { coordinate ->
                        if (coordinate.isNotEmpty() && coordinate.size == 2) {
                            val x = coordinate[0]
                            val y = coordinate[1]
                            if (x is Double && y is Double)
                                points.add(Point.fromLngLat(x, y))
                        }
                    }
                }
                if (points.isNotEmpty() && points.size > 2) {
                    silverPointsList.add(points)
                }
            }
        }


        // Adding Silver Point
        for ((index, point) in silverPointsList.withIndex()) {
            /*mapboxNavigation.roadObjectMatcher.matchPolygonObject(
                Companion.SILVER_ZONE + index,
                point
            )*/
            listOfMatchableGeometry.add(
                MatchableGeometry(RoadObjectUtil.generateSilverZoneRoadObjectName(index), point)
            )
        }

        mapboxNavigation.roadObjectMatcher.matchPolygonObjects(
            listOfMatchableGeometry,
            false
        )

    }

    /**
     * this function add zone season parking to mapbox
     */
    fun addSeasonParkingZoneObjects(mapboxNavigation: MapboxNavigation) {
        if (BreezeMapDataHolder.isNotExistProfileAmenities()) return
        if (BreezeMapDataHolder.getFirstSeasonParkingProfile()?.SeasonParking?.isNullOrEmpty() != false) return
        val seasonParkingPointsList = ArrayList<Point>()
        BreezeMapDataHolder.getFirstSeasonParkingProfile()?.SeasonParking?.forEach {
            if (it.long != null && it.lat != null) {
                seasonParkingPointsList.add(
                    Point.fromLngLat(
                        it.long!!.toDouble(),
                        it.lat!!.toDouble()
                    )
                )
            }
        }
        val listOfMatchablePoints: ArrayList<MatchablePoint> = arrayListOf()
        seasonParkingPointsList.forEachIndexed() { position, data ->
            val objectName = RoadObjectUtil.generateSeasonParkingZoneRoadObjectName(position)
            listOfMatchablePoints.add(MatchablePoint(objectName, data))
        }
        mapboxNavigation.roadObjectMatcher.matchPointObjects(listOfMatchablePoints, false)
    }


    private fun retrieveDataFromSharedPref(key: AppPrefsKey): NavigationZoneData? {
        var data: NavigationZoneData? = null
        try {
            val responseString = appContext.getAppPreference()?.getString(key)
            data = Gson().fromJson(responseString, NavigationZoneData::class.java)
        } catch (e: Exception) {
            Timber.e(e, "Error while reading School/Silver zone data")
        }
        return data
    }


    /**
     * Adds the speed cameras to ehorizon
     */
    fun addMiscROObjects(
        mapboxNavigation: MapboxNavigation,
        incidentMap: ConcurrentHashMap<NavigationZone, FeatureCollection>,
    ) {
        var listOfMatchablePoints: ArrayList<MatchablePoint> = arrayListOf()
        miscROObjects.clear()
        incidentMap.forEach {
            val featureCollection = it.value
            for ((featureIndex, feature) in featureCollection.features()!!.withIndex()) {
                var objectName = RoadObjectUtil.generateSpeedCameraRoadObjectName(featureIndex)
                listOfMatchablePoints.add(MatchablePoint(objectName, feature.geometry() as Point))
                miscROObjects.add(objectName)
            }
        }
        mapboxNavigation.roadObjectMatcher.matchPointObjects(listOfMatchablePoints, false)
    }


    /**
     * Adds the traffic ehorizon objects
     * Generates a road object name based on the formula : (incident.type+feature_index)
     */
    fun addTrafficIncidentObjects(
        mapboxNavigation: MapboxNavigation,
        incidentMap: HashMap<String, FeatureCollection>,
    ) {
        var listOfMatchablePoints: ArrayList<MatchablePoint> = arrayListOf()
        currentIncidentObjects = arrayListOf()
        TrafficIncidentData.values().forEach {
            val featureCollection = incidentMap.get(it.type)
            if (featureCollection != null) {
                //FLASH_FLOOD/HEAVY_TRAFFIC/ROAD_CLOSURE/MAJOR_ACCIDENT does not use ehorizon
                if (it.zone.maxDistanceMtr == 3000) {
                    return@forEach
                }
                for ((featureIndex, feature) in featureCollection.features()!!.withIndex()) {
                    var objectName = RoadObjectUtil.generateRoadObjectName(it, featureIndex)
                    listOfMatchablePoints.add(
                        MatchablePoint(
                            objectName,
                            feature.geometry() as Point
                        )
                    )
                    currentIncidentObjects.add(objectName)
                }
            }
        }
        mapboxNavigation.roadObjectMatcher.matchPointObjects(listOfMatchablePoints, false)
    }

    fun addERPObjects(
        mapboxNavigation: MapboxNavigation,
        erpFeature: ERPFeatures
    ) {
        var listOfMatchableGeometry: ArrayList<MatchableGeometry> = arrayListOf()
        var mapOfERP = erpFeature.mapOfERP
        mapOfERP.forEach { erpItem ->
            var pointList = listOf(
                Point.fromLngLat(
                    erpItem.value.erp.startlong.toDouble(),
                    erpItem.value.erp.startlat.toDouble()
                ),
                Point.fromLngLat(
                    erpItem.value.erp.endlong.toDouble(),
                    erpItem.value.erp.endlat.toDouble()
                )
            )
            listOfMatchableGeometry.add(MatchableGeometry(erpItem.key, pointList))
            //mapboxNavigation.roadObjectMatcher.matchGantryObject(erpItem.key, pointList)
        }
        mapboxNavigation.roadObjectMatcher.matchGantryObjects(listOfMatchableGeometry, false)

        Timber.d("BreezeFeatureDetectionUtil - All ERP road object have been added")
    }
}



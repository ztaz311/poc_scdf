package com.ncs.breeze.ui.draggableview

import android.view.WindowManager

interface OverlayDraggableListener {
    fun onParamsChanged(updatedParams: WindowManager.LayoutParams)
}
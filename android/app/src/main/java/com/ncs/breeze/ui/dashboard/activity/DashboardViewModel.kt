package com.ncs.breeze.ui.dashboard.activity

import android.app.Application
import android.location.Location
import androidx.lifecycle.viewModelScope
import com.breeze.model.DestinationAddressDetails
import com.breeze.model.ERPFullRouteData
import com.breeze.model.SharedCollection
import com.breeze.model.api.ErrorData
import com.breeze.model.api.ErrorDataResponse
import com.breeze.model.api.request.AmenitieFilter
import com.breeze.model.api.request.AmenitiesRequest
import com.breeze.model.api.request.AmenityFilterTypeData
import com.breeze.model.api.request.AnalyticsRequest
import com.breeze.model.api.request.CopilotAPIRequest
import com.breeze.model.api.request.ETARequest
import com.breeze.model.api.response.EasyBreeziesResponse
import com.breeze.model.api.response.ExploreMapContentResponse
import com.breeze.model.api.response.SaveCollectionResponse
import com.breeze.model.api.response.SettingsItem
import com.breeze.model.api.response.SettingsResponse
import com.breeze.model.api.response.TripsPlannerResponse
import com.breeze.model.api.response.UpcomingTripDetailResponse
import com.breeze.model.api.response.UserDataResponse
import com.breeze.model.api.response.amenities.BaseAmenity
import com.breeze.model.api.response.amenities.ResponseAmenities
import com.breeze.model.api.response.tbr.ResponseCongestionDetail
import com.breeze.model.constants.AmenityType.CARPARK
import com.breeze.model.constants.AmenityType.EVCHARGER
import com.breeze.model.constants.AmenityType.PETROL
import com.breeze.model.constants.AmenityType.POI
import com.breeze.model.constants.Constants
import com.breeze.model.enums.CarParkViewOption
import com.google.common.cache.CacheBuilder
import com.google.gson.Gson
import com.google.gson.JsonElement
import com.mapbox.geojson.Point
import com.mapbox.navigation.base.route.NavigationRoute
import com.ncs.breeze.car.breeze.utils.BreezeCarUtil
import com.ncs.breeze.common.ServiceLocator
import com.ncs.breeze.common.constant.dashboard.LandingMode
import com.ncs.breeze.common.extensions.android.getApp
import com.ncs.breeze.common.extensions.android.getBreezeUserPreference
import com.ncs.breeze.common.extensions.android.getUserDataPreference
import com.ncs.breeze.common.model.ERPFullRouteDataMap
import com.ncs.breeze.common.model.RouteAdapterObjects
import com.ncs.breeze.common.remote.ApiHelper
import com.ncs.breeze.common.remote.ApiObserver
import com.ncs.breeze.common.storage.BreezeUserPreference
import com.ncs.breeze.common.utils.BreezeMapDataHolder
import com.ncs.breeze.common.utils.InitObjectUtilsController
import com.ncs.breeze.common.utils.Utils
import com.ncs.breeze.components.SingleLiveEvent
import com.ncs.breeze.components.navigationlog.HistoryFileUploader
import com.ncs.breeze.ui.base.BaseViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.HttpException
import timber.log.Timber
import javax.inject.Inject

class DashboardViewModel @Inject constructor(apiHelper: ApiHelper, application: Application) :
    BaseViewModel(
        application,
        apiHelper
    ) {

    val carParkAPIDisposable: CompositeDisposable = CompositeDisposable()
    private val amenityAPIDisposable: CompositeDisposable = CompositeDisposable()

    var easyBreeziesResponse: SingleLiveEvent<EasyBreeziesResponse> = SingleLiveEvent()
    var checkTripPlannerResponse: SingleLiveEvent<TripsPlannerResponse> = SingleLiveEvent()
    var upcomingTripDetails: SingleLiveEvent<UpcomingTripDetailResponse> = SingleLiveEvent()

    var etaAPISuccessful: SingleLiveEvent<ETARequest> = SingleLiveEvent()

    var furthestAmenity: Double = 0.0
    var furthestCarpark: Double = 0.0
    val erpRouteMap =
        CacheBuilder.newBuilder().maximumSize(20).build<String, ERPFullRouteDataMap?>()
    var mListCarpark: SingleLiveEvent<ArrayList<BaseAmenity>> = SingleLiveEvent()
    var mListAmenities: SingleLiveEvent<HashMap<String, List<BaseAmenity>>> = SingleLiveEvent()
    var mResponseCongestionDetail: SingleLiveEvent<ArrayList<ResponseCongestionDetail>?> =
        SingleLiveEvent()
    var walkingRoutesRetrieved: SingleLiveEvent<NavigationRoute?> = SingleLiveEvent()
    var exploreMapNew: SingleLiveEvent<ExploreMapContentResponse?> = SingleLiveEvent()
    var saveCollectionUsingTokenSuccess: SingleLiveEvent<SaveCollectionResponse> = SingleLiveEvent()
    var saveCollectionUsingTokenFailed: SingleLiveEvent<ErrorDataResponse> = SingleLiveEvent()

    /**
     * flag to check if user first open to show tooltip at nearest carpark
     * */
    var shouldFocusToNearestCarpark = false

    private val _currentLandingMode: MutableStateFlow<LandingMode> =
        MutableStateFlow(LandingMode.PARKING)
    val currentLandingModeFlow: StateFlow<LandingMode> = _currentLandingMode.asStateFlow()

    fun updateCurrentLandingMode(mode: LandingMode) {
        viewModelScope.launch(Dispatchers.IO) {
            _currentLandingMode.emit(mode)
        }
    }

    fun getAllAmenities(
        location: Location,
        poiList: ArrayList<String>?
    ) {
        val listAmenities: HashMap<String, List<BaseAmenity>> = HashMap()
        val amenityListPref =
            BreezeMapDataHolder.amenitiesPreference.filter { it.elementName == PETROL || it.elementName == EVCHARGER }
        viewModelScope.launch {
            amenityAPIDisposable.clear()
            val amenitiesRequest = AmenitiesRequest(
                lat = location.latitude,
                long = location.longitude,
                rad = getApp().getUserDataPreference()?.getEvPetrolRange()?.div(1000.0)
                    ?: Constants.DEFAULT_PETROL_EV_RADIUS,
                maxRad = getApp().getUserDataPreference()?.getEvPetrolMaxRange()?.div(1000.0)
                    ?: Constants.DEFAULT_PETROL_EV_MAX_RADIUS,
                resultCount = getApp().getUserDataPreference()?.getEvPetrolResultCount()
                    ?: Constants.DEFAULT_PETROL_EV_RESULT_COUNT
            )
            if (amenityListPref.isNotEmpty()) {
                amenitiesRequest.setAmenitiesPreferenceListType(amenityListPref)
            }
            if (poiList != null && poiList.size > 0) {
                amenitiesRequest.listAmenities.add(
                    AmenitieFilter(
                        POI,
                        AmenityFilterTypeData(poiList)
                    )
                )
            }
            apiHelper.getAmenities(amenitiesRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : ApiObserver<ResponseAmenities>(amenityAPIDisposable) {
                    override fun onSuccess(data: ResponseAmenities) {
                        data.apply {
                            furthestAmenity = 0.0
                            data.amenities.forEach { amenities ->
                                val typeAmenities: ArrayList<BaseAmenity> = ArrayList()
                                amenities.data.forEach {
                                    if (amenities.type != null) {
                                        it.amenityType = amenities.type!!
                                        it.iconUrl = amenities.iconUrl
                                        compareFurthestAmenity(it)
                                        typeAmenities.add(it)
                                    }
                                }
                                amenities.type?.let {
                                    listAmenities[it] = typeAmenities
                                }
                            }
                            mListAmenities.postValue(listAmenities)
                        }
                    }

                    override fun onError(e: ErrorData) {
                        mListAmenities.postValue(null)
                    }
                })
        }

    }

    /**
     * get zone congestion details
     */
    fun getZoneCongestionDetails(profileSelected: String) {
        viewModelScope.launch {
            apiHelper.getZoneCongestionDetails(profileSelected)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object :
                    ApiObserver<ArrayList<ResponseCongestionDetail>>(compositeDisposable) {
                    override fun onSuccess(data: ArrayList<ResponseCongestionDetail>) {
                        data.apply {
                            mResponseCongestionDetail.postValue(data)
                        }
                    }

                    override fun onError(e: ErrorData) {
                        mResponseCongestionDetail.postValue(null)
                    }
                })
        }
    }

    var userDataResponse: SingleLiveEvent<UserDataResponse> = SingleLiveEvent()

    fun getUserData() {
        viewModelScope.launch {
            apiHelper.getUserData()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : ApiObserver<UserDataResponse>(compositeDisposable) {
                    override fun onSuccess(data: UserDataResponse) {
                        userDataResponse.value = data
                    }

                    override fun onError(e: ErrorData) {
                    }
                })
        }
    }

    fun getExploreMap() {
        viewModelScope.launch {
            apiHelper.getExploreMapMaintenance()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : ApiObserver<ExploreMapContentResponse>(compositeDisposable) {
                    override fun onSuccess(data: ExploreMapContentResponse) {
                        data.apply {
                            exploreMapNew.postValue(data)
                        }
                    }

                    override fun onError(e: ErrorData) {
                        exploreMapNew.postValue(null)
                    }
                })
        }
    }


    fun recalculateFurthestAmenity(type: String, selected: Boolean) {
        if (selected) {
            mListAmenities.value?.get(type)?.forEach {
                compareFurthestAmenity(it)
            }
        } else {
            furthestAmenity = 0.0
            mListAmenities.value?.forEach { typeAmenities ->
                if (typeAmenities.key == type) {
                    return@forEach
                }
                if (BreezeMapDataHolder.amenitiesPreferenceHashmap[typeAmenities.key]?.isSelected == true) {
                    if (typeAmenities.value.isNotEmpty()) {
                        val item = typeAmenities.value.last()
                        if ((item.distance ?: 0) > furthestAmenity) {
                            furthestAmenity = item.distance!!.toDouble()
                        }
                    }
                }
            }
        }

    }


    private fun compareFurthestAmenity(it: BaseAmenity) {
        if (BreezeMapDataHolder.amenitiesPreferenceHashmap[it.amenityType]?.isSelected == true) {
            if ((it.distance ?: 0) > furthestAmenity) {
                furthestAmenity = it.distance!!.toDouble()
            }
        }
    }


    private fun compareFurthestCarPark(it: BaseAmenity) {
        if ((it.distance ?: 0) > furthestCarpark) {
            furthestCarpark = it.distance!!.toDouble()
        }
    }


    /**
     * get carpark
     */
    fun getCarParkDataShowInMap(
        currentLocation: Location,
        dest: String?,
        cpDistanceSetting: Double = Constants.PARKING_CHECK_RADIUS
    ) {
        getCarParkDataShowInMap(
            currentLocation = currentLocation,
            dest = dest,
            cpDistanceSetting = cpDistanceSetting,
            carParkAvailability = null
        )
    }

    fun getCarParkDataShowInMap(
        currentLocation: Location,
        dest: String?,
        carParkAvailability: String? = null,
        cpDistanceSetting: Double = Constants.PARKING_CHECK_RADIUS,
        routablePointCarParkId: String? = null
    ) {
        viewModelScope.launch {
            carParkAPIDisposable.clear()
            val carParkRequest = AmenitiesRequest(
                lat = currentLocation.latitude,
                long = currentLocation.longitude,
                rad = cpDistanceSetting,
                maxRad = Constants.PARKING_CHECK_RADIUS_MAX,
                carParkId = routablePointCarParkId,
                resultCount = getApp().getBreezeUserPreference()
                    .getCarParkCountSettingValue(BreezeCarUtil.masterlists),
            ).apply {
                dest?.let { destName = it }
                setListType(
                    arrayListOf(CARPARK),
                    carParkAvailability?.let {
                        getApp().getBreezeUserPreference()
                            .getCarParkAvailabilityValue(
                                BreezeCarUtil.masterlists,
                                carParkAvailability
                            )
                    }
                )
            }

            fetchCarParkAmenities(carParkRequest)
        }
    }

    private fun fetchCarParkAmenities(carparkRequest: AmenitiesRequest) {
        apiHelper.getAmenities(carparkRequest)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : ApiObserver<ResponseAmenities>(carParkAPIDisposable) {
                override fun onSuccess(data: ResponseAmenities) {
                    val listCarpark: ArrayList<BaseAmenity> = arrayListOf()

                    data.apply {
                        furthestCarpark = 0.0
                        data.amenities.forEach { amenities ->
                            amenities.data.forEach {
                                if (amenities.type != null) {
                                    it.amenityType = amenities.type!!
                                    it.iconUrl = amenities.iconUrl


                                    /**
                                     * new condition if carkparking distance <= 500 will show all
                                     * else show only 1 carpark
                                     */
                                    compareFurthestCarPark(it)
                                    listCarpark.add(it)
                                }
                            }
                        }
                    }
                    mListCarpark.postValue(listCarpark)
                }

                override fun onError(e: ErrorData) {
                    /**
                     * must check data null for return error
                     */
                    mListCarpark.postValue(null)
                }
            })
    }

    fun retriveEasyBreezies() {
        apiHelper.retrieveEasyBreezies(
            Utils.getAppVersion(getApplication()),
            Utils.getDeviceModel(),
            Constants.DEVICE_PLATFORM,
            Utils.getDeviceOS()
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : ApiObserver<EasyBreeziesResponse>(compositeDisposable) {
                override fun onSuccess(data: EasyBreeziesResponse) {
                    Timber.d("Easy Breezie api success")
                    if (!data.addresses.isNullOrEmpty()) {
                        easyBreeziesResponse.value = data
                    }
                }

                override fun onError(e: ErrorData) {
                    Timber.d("Easy Breezie api error $e")
                }
            })
    }

    fun checkTripUpcomingSummary() {
        apiHelper.checkTripUpcomingSummary(
            1, 10, "", "", ""
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : ApiObserver<TripsPlannerResponse>(compositeDisposable) {
                override fun onSuccess(data: TripsPlannerResponse) {
                    checkTripPlannerResponse.value = data
                }

                override fun onError(e: ErrorData) {
                    Timber.i("AAA ${e.message} ${e.throwable}")
                    Timber.d("checkTripUpcomingSummary api error")
                }
            })
    }


    fun cacheRouteData(
        key: String,
        eRPFullRouteData: ERPFullRouteData,
        routeList: List<RouteAdapterObjects>
    ) {
        erpRouteMap.put(key, ERPFullRouteDataMap(eRPFullRouteData, routeList))
    }

    fun sendSecondaryFirebaseID(username: String?) {
        if (HistoryFileUploader.ENABLE_COPILOT) {
            viewModelScope.launch {
                apiHelper.sendSecondaryFirebaseUserID(
                    CopilotAPIRequest(
                        ServiceLocator.getSecondaryFirebaseAuthUserId(),
                        username
                    )
                )
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(object : ApiObserver<JsonElement>(compositeDisposable) {
                        override fun onSuccess(data: JsonElement) {
                            Timber.d("Copilot API is successful: " + data.toString())
                        }

                        override fun onError(e: ErrorData) {
                            Timber.d("Copilot API failed: " + e.toString())
                        }

                    })
            }
        }
    }

    operator fun JSONArray.iterator(): Iterator<JSONObject> =
        (0 until length()).asSequence().map { get(it) as JSONObject }.iterator()

    fun getUpcomingTripDetails(tripPlannerId: Long) {
        apiHelper.getUpcomingTripDetails(tripPlannerId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : ApiObserver<UpcomingTripDetailResponse>(compositeDisposable) {
                override fun onSuccess(data: UpcomingTripDetailResponse) {
                    println("%%%% : upcomign details")
                    upcomingTripDetails.value = data
                }

                override fun onError(e: ErrorData) {
                    println("%%%% : upcomign details error" + e.toString())
                }
            })
    }

    fun saveUserSettings() {
        val settingsRequest = SettingsResponse(arrayListOf())
        settingsRequest.settings.add(SettingsItem(Constants.SETTINGS.ATTR_SHOW_DEMO, "No", "No"))

        apiHelper.setUserSettings(settingsRequest)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : ApiObserver<JsonElement>(
                compositeDisposable
            ) {
                override fun onSuccess(data: JsonElement) {
                    Timber.d("Retrieved user data is: " + data.toString())
                    BreezeCarUtil.settingsResponse?.settings?.filter { item -> item.attribute == Constants.SETTINGS.ATTR_SHOW_DEMO }
                        ?.takeIf { it.isNotEmpty() }
                        ?.let { showDemoItems ->
                            BreezeCarUtil.settingsResponse?.settings?.removeAll(showDemoItems.toSet())
                        }
                    BreezeCarUtil.settingsResponse?.settings?.addAll(settingsRequest.settings)
                }

                override fun onError(e: ErrorData) {
                    Timber.i("AAA ${e.message} ${e.throwable}")
                }
            })
    }


    /**
     * save state carpark option view
     */
    fun saveCarParkOptionUserSettings(carParkViewOption: CarParkViewOption) {
        val settingsRequest = SettingsResponse(arrayListOf())
        settingsRequest.settings.add(
            SettingsItem(
                Constants.SETTINGS.CARPARK_AVAILABILITY,
                carParkViewOption.mode,
                carParkViewOption.mode
            )
        )
        apiHelper.setUserSettings(settingsRequest)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : ApiObserver<JsonElement>(
                compositeDisposable
            ) {
                override fun onSuccess(data: JsonElement) {
                    Timber.d("Retrieved user data is: $data")
                }

                override fun onError(e: ErrorData) {
                    Timber.i("AAA ${e.message} ${e.throwable}")
                }
            })
    }


    fun findWalkingRoutes(
        originPoint: Point?,
        destination: Point?,
        currentDestinationDetails: DestinationAddressDetails
    ) {
        CoroutineScope(Dispatchers.IO).launch {
            InitObjectUtilsController.mRoutePlanningHelper?.let { routePlaningHelper ->
                val walkingNavigationRoute: NavigationRoute? =
                    routePlaningHelper.findWalkingRoute(
                        originPoint, destination,
                        currentDestinationDetails.address1,
                    )
                walkingRoutesRetrieved.postValue(walkingNavigationRoute)
            }
        }
    }

    /**
     * save user action analytics data
     */
    fun saveCustomAnalytics(eventName: String, eventValue: String, screenName: String) {
        val request = AnalyticsRequest()
        request.eventName = eventName
        request.eventValue = eventValue
        request.screenName = screenName
        request.eventTimestamp = System.currentTimeMillis()
        apiHelper.customAnalytics(request)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : ApiObserver<JsonElement>(
                compositeDisposable
            ) {
                override fun onSuccess(data: JsonElement) {
                    Timber.d("analytics api success")
                }

                override fun onError(e: ErrorData) {
                    Timber.i("AAA ${e.message} ${e.throwable}")
                }
            })
    }

    /**
     * to save collection based on token
     */
    fun saveCollection(token: String) {
        viewModelScope.launch {
            apiHelper.saveCollection(token)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : ApiObserver<SaveCollectionResponse>(compositeDisposable) {
                    override fun onSuccess(data: SaveCollectionResponse) {
                        Timber.d("save collection success: $data")
                        saveCollectionUsingTokenSuccess.postValue(data)
                    }

                    override fun onError(e: ErrorData) {
                        Timber.i("AAA ${e}")
                        (e.throwable as? HttpException)?.let { err ->
                            kotlin.runCatching {
                                val errResponse = Gson().fromJson(
                                    err.response()?.errorBody()?.string(),
                                    ErrorDataResponse::class.java
                                )
                                saveCollectionUsingTokenFailed.postValue(errResponse)
                            }
                        }
                    }
                })
        }
    }

    fun getSharedCollectionFromToken(token: String, consumer: Consumer<SharedCollection>) {
        apiHelper.getSharedCollection(token)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe (
                {
                    if (it.success) consumer.accept(it.data)
                },
                {
                    Timber.e("getSharedCollectionFromToken: $it")
                }
            ).also {
                compositeDisposable.add(it)
            }
    }

    override fun onCleared() {
        super.onCleared()
        carParkAPIDisposable.clear()
        amenityAPIDisposable.clear()
    }

}
package com.ncs.breeze.car.breeze.screen.navigation

import androidx.car.app.model.Distance
import androidx.car.app.navigation.model.NavigationTemplate
import androidx.car.app.navigation.model.RoutingInfo
import androidx.car.app.navigation.model.TravelEstimate
import com.mapbox.androidauto.internal.logAndroidAuto
import com.mapbox.bindgen.Expected
import com.mapbox.navigation.base.trip.model.RouteProgress
import com.mapbox.navigation.core.lifecycle.MapboxNavigationApp
import com.mapbox.navigation.core.trip.session.RouteProgressObserver
import com.mapbox.navigation.ui.maneuver.model.Maneuver
import com.mapbox.navigation.ui.maneuver.model.ManeuverError
import com.ncs.breeze.car.breeze.utils.NavigationNotificationUtils
import com.breeze.model.extensions.round

/**
 * Observe MapboxNavigation properties that create NavigationInfo.
 *
 * Attach the [start] [stop] functions to start observing navigation info.
 */
class CarNavigationInfoObserver(
    private val carNavigationCarContext: CarNavigationCarContext
) {
    private var onNavigationInfoChanged: (() -> Unit)? = null
    var navigationInfo: NavigationTemplate.NavigationInfo? = null
        private set(value) {
            if (field != value) {
                logAndroidAuto("CarNavigationInfoObserver navigationInfo changed")
                field = value
                field?.let {
                    val routingInfo = it as RoutingInfo
                    it.currentStep?.let { currentStep ->
                        if ((currentStep.road != null || currentStep.cue != null) && routingInfo.currentDistance != null) {
                            val displayUnit = routingInfo.currentDistance!!.displayUnit
                            val round: Int = if (displayUnit == Distance.UNIT_METERS) 0 else 2
                            val notificationTitle =
                                routingInfo.currentDistance!!.displayDistance.round(round) + " " + unitToString(
                                    displayUnit
                                )
                            NavigationNotificationUtils.showTurnByTurnNavigationNotification(
                                notificationTitle,
                                if (currentStep.road != null) currentStep.road.toString() else currentStep.cue.toString(),
                                currentStep.maneuver?.icon?.icon
                            )
                        }
                    }
                }
                onNavigationInfoChanged?.invoke()
            }
        }

    var travelEstimateInfo: TravelEstimate? = null
    private var expectedManeuvers: Expected<ManeuverError, List<Maneuver>>? = null
    private var routeProgress: RouteProgress? = null

    private val routeProgressObserver = RouteProgressObserver { routeProgress ->
        this.routeProgress = routeProgress
        expectedManeuvers = carNavigationCarContext.maneuverApi.getManeuvers(routeProgress)
        updateNavigationInfo()
    }

    private fun updateNavigationInfo() {
        this.navigationInfo = carNavigationCarContext.navigationInfoMapper
            .mapNavigationInfo(expectedManeuvers, routeProgress)
        //navigationInfo.mBackgroundColor = ? //ToDo - Ankur

        this.travelEstimateInfo = carNavigationCarContext.tripProgressMapper.from(routeProgress)
    }

    fun getArrivedNavigationInfo(
        destinationTitle: String,
        isDestinationCarPark: Boolean
    ): NavigationTemplate.NavigationInfo {
        return carNavigationCarContext.navigationInfoMapper.arrivedNavigationInfo(
            carContext = carNavigationCarContext.carContext,
            destinationTitle = destinationTitle,
            isDestinationCarPark = isDestinationCarPark
        )
    }

    fun start(onNavigationInfoChanged: () -> Unit) {
        this.onNavigationInfoChanged = onNavigationInfoChanged
        logAndroidAuto("CarRouteProgressObserver onStart")
        MapboxNavigationApp.current()
            ?.registerRouteProgressObserver(routeProgressObserver)
    }

    fun stop() {
        logAndroidAuto("CarRouteProgressObserver onStop")
        onNavigationInfoChanged = null

        MapboxNavigationApp.current()
            ?.unregisterRouteProgressObserver(routeProgressObserver)
    }


    fun unitToString(@Distance.Unit displayUnit: Int): String? {
        return when (displayUnit) {
            Distance.UNIT_FEET -> "feet"
            Distance.UNIT_KILOMETERS -> "km"
            Distance.UNIT_KILOMETERS_P1 -> "km"
            Distance.UNIT_METERS -> "m"
            Distance.UNIT_MILES -> "mi"
            Distance.UNIT_MILES_P1 -> "mi"
            Distance.UNIT_YARDS -> "yd"
            else -> "?"
        }
    }
}

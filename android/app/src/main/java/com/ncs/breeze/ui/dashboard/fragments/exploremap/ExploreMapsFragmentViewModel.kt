package com.ncs.breeze.ui.dashboard.fragments.exploremap

import android.app.Application
import android.location.Location
import androidx.lifecycle.viewModelScope
import com.google.gson.JsonElement
import com.mapbox.geojson.Point
import com.mapbox.navigation.base.route.NavigationRoute
import com.ncs.breeze.car.breeze.utils.BreezeCarUtil
import com.breeze.model.DestinationAddressDetails
import com.breeze.model.api.ErrorData
import com.breeze.model.api.request.AmenitiesRequest
import com.breeze.model.api.request.AnalyticsRequest
import com.breeze.model.api.response.ContentDetailsResponse
import com.breeze.model.api.response.ExploreMapContentResponse
import com.breeze.model.api.response.amenities.BaseAmenity
import com.breeze.model.constants.AmenityType.CARPARK
import com.breeze.model.api.response.amenities.ResponseAmenities
import com.breeze.model.api.response.tbr.ResponseCongestionDetail
import com.ncs.breeze.common.remote.ApiHelper
import com.ncs.breeze.common.remote.ApiObserver
import com.ncs.breeze.common.storage.BreezeUserPreference
import com.breeze.model.constants.Constants
import com.ncs.breeze.common.extensions.android.getApp
import com.ncs.breeze.common.utils.InitObjectUtilsController
import com.ncs.breeze.components.SingleLiveEvent
import com.ncs.breeze.ui.base.BaseFragmentViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.json.JSONArray
import org.json.JSONObject
import timber.log.Timber
import javax.inject.Inject

class ExploreMapsFragmentViewModel @Inject constructor(
    val apiHelper: ApiHelper,
    application: Application
) : BaseFragmentViewModel(
    application
) {

    val carParkAPIDisposable: CompositeDisposable = CompositeDisposable()
    private val amenityAPIDisposable: CompositeDisposable = CompositeDisposable()

    var mListCarpark: SingleLiveEvent<ArrayList<BaseAmenity>> = SingleLiveEvent()
    var isNotExistAnyCp: SingleLiveEvent<Boolean> = SingleLiveEvent()
    var mResponseCongestionDetail: SingleLiveEvent<ArrayList<ResponseCongestionDetail>?> =
        SingleLiveEvent()
    var walkingRoutesRetrieved: SingleLiveEvent<NavigationRoute?> = SingleLiveEvent()
    var contentDetailsResponse: SingleLiveEvent<ContentDetailsResponse?> = SingleLiveEvent()
    var exploreMapNew: SingleLiveEvent<ExploreMapContentResponse?> = SingleLiveEvent()
    var lastSelectedContentId: String? = null

    /**
     * get zone congestion details
     */
    fun getZoneCongestionDetails(profileSelected: String) {
        viewModelScope.launch {
            apiHelper.getZoneCongestionDetails(profileSelected)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object :
                    ApiObserver<ArrayList<ResponseCongestionDetail>>(compositeDisposable) {
                    override fun onSuccess(data: ArrayList<ResponseCongestionDetail>) {
                        data.apply {
                            mResponseCongestionDetail.postValue(data)
                        }
                    }

                    override fun onError(e: ErrorData) {
                        mResponseCongestionDetail.postValue(null)
                    }
                })
        }
    }

    /**
     * get carpark
     */
    fun getCarParkDataShowInMap(
        currentLocation: Location,
        dest: String?,
        radius: Double = Constants.PARKING_CHECK_RADIUS
    ) {
        val listCarpark: ArrayList<BaseAmenity> = arrayListOf()
        viewModelScope.launch {
            carParkAPIDisposable.clear()
            val carParkRequest = AmenitiesRequest(
                lat = currentLocation.latitude,
                long = currentLocation.longitude,
                rad = BreezeUserPreference.getInstance(getApp())
                    .getCarParkDistanceSettingValue(BreezeCarUtil.masterlists),
                maxRad = Constants.PARKING_CHECK_RADIUS_MAX,
                resultCount = BreezeUserPreference.getInstance(getApp())
                    .getCarParkCountSettingValue(BreezeCarUtil.masterlists),
            ).apply {
                dest?.let { destName = it }
            }
            carParkRequest.setListType(listOf(CARPARK))
            fetchCarParkAmenities(carParkRequest, listCarpark)
        }
    }

    /**
     * get carpark
     */
    fun getCarParkDataShowInMap(
        currentLocation: Location,
        dest: String?,
        carParkAvailability: String
    ) {
        val listCarpark: ArrayList<BaseAmenity> = arrayListOf()
        viewModelScope.launch {
            carParkAPIDisposable.clear()
            val carParkRequest = AmenitiesRequest(
                lat = currentLocation.latitude,
                long = currentLocation.longitude,
                rad = BreezeUserPreference.getInstance(getApp())
                    .getCarParkDistanceSettingValue(BreezeCarUtil.masterlists),
                maxRad = Constants.PARKING_CHECK_RADIUS_MAX,
                resultCount = BreezeUserPreference.getInstance(getApp())
                    .getCarParkCountSettingValue(BreezeCarUtil.masterlists),
            ).apply {
                dest?.let { destName = it }
            }
            carParkRequest.setListType(
                arrayListOf(CARPARK),
                BreezeUserPreference.getInstance(getApp())
                    .getCarParkAvailabilityValue(BreezeCarUtil.masterlists, carParkAvailability)
            )
            fetchCarParkAmenities(carParkRequest, listCarpark)
        }
    }

    private fun fetchCarParkAmenities(
        carparkRequest: AmenitiesRequest,
        listCarpark: ArrayList<BaseAmenity>
    ) {
        apiHelper.getAmenities(carparkRequest)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : ApiObserver<ResponseAmenities>(carParkAPIDisposable) {
                override fun onSuccess(data: ResponseAmenities) {
                    data.apply {

                        /**
                         * processing data before use
                         */
                        var isNotExistCarpark500M = true
                        data.amenities.forEach lit@{ amenities ->

                            amenities.data.forEach {
                                if (amenities.type != null) {
                                    it.amenityType = amenities.type!!
                                    it.iconUrl = amenities.iconUrl
                                    listCarpark.add(it)
                                }
                            }
                        }
                        if (data.amenities.isNotEmpty()) {
                            isNotExistCarpark500M = false
                        }
                        mListCarpark.postValue(listCarpark)
                        isNotExistAnyCp.postValue(isNotExistCarpark500M)
                    }


                }

                override fun onError(e: ErrorData) {
                    /**
                     * must check data null for return error
                     */
                    mListCarpark.postValue(listCarpark)
                }
            })
    }

    operator fun JSONArray.iterator(): Iterator<JSONObject> =
        (0 until length()).asSequence().map { get(it) as JSONObject }.iterator()


    fun findWalkingRoutes(
        originPoint: Point?,
        destination: Point?,
        currentDestinationDetails: DestinationAddressDetails
    ) {
        CoroutineScope(Dispatchers.IO).launch {
            InitObjectUtilsController.mRoutePlanningHelper?.let { routePlaningHelper ->
                val walkingNavigationRoute: NavigationRoute? =
                    routePlaningHelper.findWalkingRoute(
                        originPoint, destination,
                        currentDestinationDetails.address1,
                    )
                walkingRoutesRetrieved.postValue(walkingNavigationRoute)
            }
        }
    }

    /**
     * get selected content details
     */
    fun getSelectedContentDetails(contentId: String) {
        viewModelScope.launch {
            apiHelper.getExploreContentDetails(contentId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : ApiObserver<ContentDetailsResponse>(compositeDisposable) {
                    override fun onSuccess(data: ContentDetailsResponse) {
                        contentDetailsResponse.postValue(data)
                        lastSelectedContentId = contentId
                    }

                    override fun onError(e: ErrorData) {
                        Timber.i("AAA ${e.message} ${e.throwable}")
                    }
                })
        }
    }

    fun refreshContent() {
        lastSelectedContentId?.let {
            getSelectedContentDetails(it)
        }
    }

    override fun onCleared() {
        super.onCleared()
        carParkAPIDisposable.clear()
        amenityAPIDisposable.clear()
    }

    fun getExploreMap() {
        viewModelScope.launch {
            apiHelper.getExploreMapMaintenance()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : ApiObserver<ExploreMapContentResponse>(compositeDisposable) {
                    override fun onSuccess(data: ExploreMapContentResponse) {
                        data.apply {
                            exploreMapNew.postValue(data)
                        }
                    }

                    override fun onError(e: ErrorData) {
                        exploreMapNew.postValue(null)
                    }
                })
        }
    }

    /**
     * save user action analytics data
     */
    fun saveCustomAnalytics(
        eventName: String,
        eventValue: String,
        screenName: String,
        amenityId: String,
        layerCode: String,
        amenityType: String = "",
    ) {
        val request = AnalyticsRequest()
        request.eventName = eventName
        request.eventValue = eventValue
        request.screenName = screenName
        request.amenityId = amenityId
        request.layerCode = layerCode
        request.eventTimestamp = System.currentTimeMillis()
        request.amenityType = amenityType
        apiHelper.customAnalytics(request)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : ApiObserver<JsonElement>(
                compositeDisposable
            ) {
                override fun onSuccess(data: JsonElement) {
                    Timber.d("analytics api success")
                }

                override fun onError(e: ErrorData) {
                    Timber.i("AAA ${e.message} ${e.throwable}")
                }
            })
    }
}
package com.ncs.breeze.common.extensions.mapbox

import android.content.Context
import com.mapbox.navigation.base.formatter.DistanceFormatterOptions

fun DistanceFormatterOptions.defaultDistanceFormatterOptions(applicationContext: Context) =
    DistanceFormatterOptions.Builder(applicationContext).build()
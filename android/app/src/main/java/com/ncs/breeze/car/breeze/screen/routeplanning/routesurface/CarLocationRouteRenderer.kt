package com.ncs.breeze.car.breeze.screen.routeplanning.routesurface


import com.mapbox.maps.extension.androidauto.MapboxCarMapSurface
import com.mapbox.maps.extension.androidauto.MapboxCarMapObserver

import com.ncs.breeze.car.breeze.MainBreezeCarContext

import com.mapbox.maps.plugin.locationcomponent.LocationComponentPlugin


/**
 * Create a simple 3d location puck. This class is demonstrating how to
 * create a renderer. To Create a new location experience, try creating a new class.
 */
class CarLocationRouteRenderer(
    private val mainCarContext: MainBreezeCarContext
) : MapboxCarMapObserver {

    private var locationComponent: LocationComponentPlugin? = null


   override fun onAttached(mapboxCarMapSurface: MapboxCarMapSurface) {
//        locationComponent = mapboxCarMapSurface.mapSurface.location.apply {
//            //locationPuck = CarLocationPuck.navigationPuck2D(mainCarContext.carContext)
//            //enabled = true
//            //pulsingEnabled = true
//            mainCarContext.defaultLocationProvider=getLocationProvider()
//            //setLocationProvider(CarAppLocationObserver.navigationLocationProvider)
//            /**
//             * do something here
//             */
////            addOnIndicatorBearingChangedListener(onIndicatorBearingChangedListener)
//            locationComponent.apply {
//                pulsingColor = mainCarContext.carContext.getColor(R.color.themed_nav_pulse_color)
//                pulsingMaxRadius = 50.0F
//            }
//        }

    }

    override fun onDetached(mapboxCarMapSurface: MapboxCarMapSurface) {
//        logAndroidAuto("CarLocationRenderer carMapSurface detached")
//        locationComponent?.apply{
//            mainCarContext.defaultLocationProvider?.let{
//                setLocationProvider(it)
//            }
//        }
    }
}

package com.ncs.breeze.car.breeze.screen.obulite

import android.annotation.SuppressLint
import android.graphics.Rect
import android.location.Location
import com.breeze.model.api.ErrorData
import com.breeze.model.api.request.AmenitiesRequest
import com.breeze.model.api.response.amenities.BaseAmenity
import com.breeze.model.api.response.amenities.ResponseAmenities
import com.breeze.model.constants.AmenityType
import com.breeze.model.constants.Constants
import com.breeze.model.enums.CarParkViewOption
import com.breeze.model.extensions.dpToPx
import com.mapbox.maps.extension.androidauto.MapboxCarMapSurface
import com.mapbox.maps.extension.androidauto.MapboxCarMapObserver
import com.mapbox.androidauto.internal.logAndroidAuto
import com.mapbox.geojson.Point
import com.ncs.breeze.car.breeze.location.CarLocationPuck
import com.ncs.breeze.R
import com.mapbox.maps.EdgeInsets
import com.mapbox.maps.plugin.animation.camera
import com.mapbox.maps.plugin.locationcomponent.location
import com.mapbox.navigation.base.formatter.DistanceFormatterOptions
import com.mapbox.navigation.base.formatter.UnitType
import com.mapbox.navigation.core.lifecycle.MapboxNavigationApp
import com.mapbox.navigation.core.trip.session.LocationMatcherResult
import com.mapbox.navigation.core.trip.session.LocationObserver
import com.mapbox.navigation.ui.maps.camera.NavigationCamera
import com.mapbox.navigation.ui.maps.camera.data.MapboxNavigationViewportDataSource
import com.mapbox.navigation.ui.maps.camera.transition.NavigationCameraTransitionOptions
import com.mapbox.navigation.ui.maps.location.NavigationLocationProvider
import com.mapbox.navigation.ui.speedlimit.api.MapboxSpeedInfoApi
import com.ncs.breeze.car.breeze.model.CarParkLayerModel
import com.ncs.breeze.car.breeze.utils.BreezeCarUtil
import com.ncs.breeze.car.breeze.utils.BreezeMapBoxUtil
import com.ncs.breeze.common.remote.ApiObserver
import com.ncs.breeze.common.utils.Utils
import com.ncs.breeze.ui.dashboard.fragments.obulite.OBUCheckSpeedHelper
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber

class CarOBULiteLocationRenderer(
    private val obuLiteCarContext: OBULiteCarContext
) : MapboxCarMapObserver {
    var compositeDisposable: CompositeDisposable = CompositeDisposable()

    private var isInitialized = false
    private var viewportDataSource: MapboxNavigationViewportDataSource? = null
    private var navigationCamera: NavigationCamera? = null
    private var overviewEdgeInsets: EdgeInsets? = null
    private var followingEdgeInsets: EdgeInsets? = null

    private var currentLocation: Location? = null
    private val carParkFeatureDataList = ArrayList<CarParkLayerModel>();
    private var obuCheckSpeedHelper: OBUCheckSpeedHelper? = null
    private val speedLimitApi: MapboxSpeedInfoApi by lazy { MapboxSpeedInfoApi() }

    private var mapboxCarMapSurface: MapboxCarMapSurface? = null
    private var navigationLocationProvider = NavigationLocationProvider()

    private val locationObserver = object : LocationObserver {
        override fun onNewLocationMatcherResult(locationMatcherResult: LocationMatcherResult) {

            viewportDataSource?.onLocationChanged(locationMatcherResult.enhancedLocation)
            viewportDataSource?.evaluate()

            val speedInfoValue = speedLimitApi.updatePostedAndCurrentSpeed(
                locationMatcherResult,
                DistanceFormatterOptions.Builder(obuLiteCarContext.carContext)
                    .unitType(UnitType.METRIC)
                    .build()
            )
            obuCheckSpeedHelper?.changeSpeed(speedInfoValue.currentSpeed.toDouble())

            navigationLocationProvider.changePosition(
                locationMatcherResult.enhancedLocation,
                locationMatcherResult.keyPoints
            )

            if (!isInitialized) {
                isInitialized = true
                navigationCamera?.requestNavigationCameraToFollowing(NavigationCameraTransitionOptions.Builder()
                    .maxDuration(0)
                    .build())
            }
        }

        override fun onNewRawLocation(rawLocation: Location) {
            currentLocation = rawLocation
        }

    }

    @SuppressLint("MissingPermission")
    override fun onAttached(mapboxCarMapSurface: MapboxCarMapSurface) {
        this.mapboxCarMapSurface = mapboxCarMapSurface
        mapboxCarMapSurface.mapSurface.location.apply {
            locationPuck = CarLocationPuck.navigationCruise2D(obuLiteCarContext.carContext)
            enabled = true
            pulsingEnabled = true

            pulsingColor = obuLiteCarContext.carContext.getColor(
                R.color.themed_nav_puck_pulse_color
            )
            pulsingMaxRadius = 50.0F

            obuLiteCarContext.mainCarContext.defaultLocationProvider = getLocationProvider()
            setLocationProvider(navigationLocationProvider)
        }

        val mapboxMap = mapboxCarMapSurface.mapSurface.getMapboxMap()
        viewportDataSource = MapboxNavigationViewportDataSource(
            mapboxMap
        ).apply {
            overviewEdgeInsets?.let { overviewPadding = it }
            followingEdgeInsets?.let { followingPadding = it }
            overviewPitchPropertyOverride(0.0)
            options.followingFrameOptions.minZoom = 16.5
            options.followingFrameOptions.maxZoom = 16.5
            options.overviewFrameOptions.maxZoom = 18.0
        }

        navigationCamera = NavigationCamera(
            mapboxMap,
            mapboxCarMapSurface.mapSurface.camera,
            viewportDataSource!!
        )

        if (obuCheckSpeedHelper == null) {
            obuCheckSpeedHelper = OBUCheckSpeedHelper()
            obuCheckSpeedHelper?.setStateChangeCallback(::onSpeedStateChanged)
        }
        MapboxNavigationApp.current()?.registerLocationObserver(locationObserver)
    }

    override fun onVisibleAreaChanged(visibleArea: Rect, edgeInsets: EdgeInsets) {
        super.onVisibleAreaChanged(visibleArea, edgeInsets)
        logAndroidAuto("CarNavigationCamera visibleAreaChanged $visibleArea $edgeInsets")
        Timber.e("CarNavigationCamera visibleAreaChanged $visibleArea $edgeInsets")


        viewportDataSource?.overviewPadding = EdgeInsets(
            edgeInsets.top + 15.dpToPx(),
            edgeInsets.left + 10.dpToPx(),
            edgeInsets.bottom + 30.dpToPx(),
            edgeInsets.right + 40.dpToPx()
        ).also { overviewEdgeInsets = it }

        val visibleHeight = visibleArea.bottom - visibleArea.top
        val followingBottomPadding = visibleHeight * BOTTOM_FOLLOWING_PERCENTAGE
        viewportDataSource?.followingPadding = EdgeInsets(
            edgeInsets.top,
            edgeInsets.left,
            edgeInsets.bottom + followingBottomPadding,
            edgeInsets.right
        ).also { followingEdgeInsets = it }

        viewportDataSource?.evaluate()
    }


    override fun onDetached(mapboxCarMapSurface: MapboxCarMapSurface) {
        this.mapboxCarMapSurface = null
        isInitialized = false
        mapboxCarMapSurface.mapSurface.location.apply {
            obuLiteCarContext.mainCarContext.defaultLocationProvider?.let {
                setLocationProvider(it)
            }
        }

        mapboxCarMapSurface.mapSurface.getMapboxMap().getStyle()?.let {
            BreezeMapBoxUtil.removeCarkParkData(it, carParkFeatureDataList)
        }

        obuCheckSpeedHelper?.destroy()
        obuCheckSpeedHelper = null
        MapboxNavigationApp.current()?.unregisterLocationObserver(locationObserver)
    }

    private fun onSpeedStateChanged(
        speedState: OBUCheckSpeedHelper.SpeedState,
        stateChanged: Boolean
    ) {
        Timber.e("CarNavigationCamera onSpeedStateChanged: $speedState $stateChanged")
        when (speedState) {
            OBUCheckSpeedHelper.SpeedState.RUNNING -> {
                if (stateChanged) {
                    clearCarparks()
                }
            }

            OBUCheckSpeedHelper.SpeedState.STATIONARY -> {
                if (stateChanged) {
                    fetchCarparks()
                }
            }

            else -> {}
        }
    }


    private fun clearCarparks() {
        Timber.e("CarNavigationCamera clearCarparks")
        navigationCamera?.requestNavigationCameraToFollowing(
            NavigationCameraTransitionOptions.Builder().maxDuration(1000L).build()
        )
        mapboxCarMapSurface?.mapSurface?.getMapboxMap()?.getStyle()?.let { style ->
            BreezeMapBoxUtil.removeCarkParkData(
                style,
                carParkFeatureDataList
            )
        }
    }

    private fun fetchCarparks() {
        CoroutineScope(Dispatchers.IO).launch {
            currentLocation?.let { location ->
                var locationName: String? = Utils.getAddressFromLocation(
                    location,
                    obuLiteCarContext.carContext
                )
                withContext(Dispatchers.Main) {
                    if (locationName.isNullOrEmpty()) {
                        locationName = ""
                    }

                    getNearbyCarParks(
                        locationName!!,
                        location
                    )
                }
            }
        }

    }

    private fun getNearbyCarParks(
        destName: String,
        location: Location
    ) {
        val listCarPark: ArrayList<BaseAmenity> = arrayListOf()
        val carParkRequest = createCarParkRequest(location, destName)
        fetchAndUpdateCarParks(carParkRequest, listCarPark)
    }

    private fun fetchAndUpdateCarParks(
        carParkRequest: AmenitiesRequest,
        listCarPark: ArrayList<BaseAmenity>
    ) {
        BreezeCarUtil.apiHelper.getAmenities(carParkRequest)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : ApiObserver<ResponseAmenities>(compositeDisposable) {
                override fun onSuccess(data: ResponseAmenities) {
                    data.apply {
                        amenities.forEach { amenity ->
                            amenity.data.forEach {
                                if (amenity.type != null) {
                                    it.amenityType = amenity.type!!
                                    it.iconUrl = amenity.iconUrl
                                    listCarPark.add(it)
                                }
                            }
                        }

                        updateMapAndTemplateView(
                            listCarPark
                        )
                    }


                }

                override fun onError(e: ErrorData) {

                }
            })
    }

    private fun createCarParkRequest(
        currentLocation: Location,
        destName: String
    ): AmenitiesRequest {
        val carParkRequest = AmenitiesRequest(
            lat = currentLocation.latitude,
            long = currentLocation.longitude,
            rad = BreezeCarUtil.breezeUserPreferenceUtil.getBreezeUserPreference()
                .getCarParkDistanceSettingValue(BreezeCarUtil.masterlists),
            maxRad = Constants.PARKING_CHECK_RADIUS_MAX,
            resultCount = BreezeCarUtil.breezeUserPreferenceUtil.getBreezeUserPreference()
                .getCarParkCountSettingValue(BreezeCarUtil.masterlists),
            destName = destName,
        )
        carParkRequest.setListType(
            listOf(AmenityType.CARPARK),
            BreezeCarUtil.breezeUserPreferenceUtil.getBreezeUserPreference()
                .getCarParkAvailabilityValue(
                    BreezeCarUtil.masterlists,
                    CarParkViewOption.ALL_NEARBY.mode
                )
        )
        carParkRequest.setCarParkCategoryFilter()
        return carParkRequest
    }

    private fun updateMapAndTemplateView(
        listCarPark: ArrayList<BaseAmenity>
    ) {

        mapboxCarMapSurface?.mapSurface?.getMapboxMap()?.getStyle()?.let { style ->
            BreezeMapBoxUtil.displayCarkParkData(
                carParkFeatureDataList,
                style,
                listCarPark,
                obuLiteCarContext.carContext,
                false
            )
            viewportDataSource?.additionalPointsToFrameForOverview(
                listCarPark.filter { it.long != null && it.lat != null }
                    .map {
                        Point.fromLngLat(it.long ?: 0.0, it.lat ?: 0.0)
                    })
            viewportDataSource?.overviewBearingPropertyOverride(
                mapboxCarMapSurface?.mapSurface?.getMapboxMap()?.cameraState?.bearing
                    ?: currentLocation?.bearing?.toDouble()
            )
            viewportDataSource?.evaluate()
            navigationCamera?.requestNavigationCameraToOverview(
                NavigationCameraTransitionOptions.Builder().maxDuration(1000L).build(),
                NavigationCameraTransitionOptions.Builder().maxDuration(700L).build(),
            )
        }
    }

    private companion object {
        /**
         * While following the location puck, inset the bottom by 1/3 of the screen.
         */
        private const val BOTTOM_FOLLOWING_PERCENTAGE = 1.0 / 3.0
    }
}

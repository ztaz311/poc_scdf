package com.ncs.breeze.components.marker.markerview2.carpark

import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import com.breeze.model.api.response.AmenitiesPreference
import com.breeze.model.api.response.amenities.BaseAmenity
import com.breeze.model.constants.AmenityType
import com.breeze.model.constants.Constants
import com.facebook.react.bridge.Arguments
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.annotations.concurrent.UiThread
import com.mapbox.maps.MapboxMap
import com.mapbox.navigation.core.lifecycle.MapboxNavigationApp
import com.mapbox.navigation.core.trip.session.TripSessionState
import com.ncs.breeze.App
import com.ncs.breeze.R
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.extensions.android.getShareBusinessLogicPackage
import com.ncs.breeze.common.utils.BreezeMapDataHolder
import com.ncs.breeze.databinding.CarparkMarkerTooltipBinding
import com.ncs.breeze.reactnative.nativemodule.ShareBusinessLogicEvent
import com.ncs.breeze.ui.base.BaseActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


open class CarparkMarkerView(
    latLng: LatLng,
    mapboxMap: MapboxMap,
    activity: Activity,
    itemId: String? = null,
    showBookmarkIcon: Boolean,
    toolTipZoomRange: Double = Constants.DEFAULT_TOOLTIP_ZOOM_RADIUS
) : BaseCarParkMarkerView(mapboxMap, activity, itemId) {

    private var viewBinding: CarparkMarkerTooltipBinding
    private var objectConfigUiAmenities: AmenitiesPreference? = null
    protected var carParkToolTipManager: CarParkToolTipManager? = null
    var currentDataMarker: BaseAmenity? = null
    var currentState = 0

    init {
        zoomLevel = 16.0
        mLatLng = latLng
        viewBinding = CarparkMarkerTooltipBinding.inflate(
            activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater,
            null,
            false
        )
        viewBinding.imgBookmark.visibility = if (showBookmarkIcon) View.VISIBLE else View.GONE
        carParkToolTipManager =
            CarParkToolTipManager(viewBinding, mapboxMap, activity, toolTipZoomRange, true)
        mViewMarker = viewBinding.root
    }

    fun clickToolTip(
        item: BaseAmenity,
        shouldRecenter: Boolean,
        navigationButtonClick: (BaseAmenity, BaseActivity.EventSource) -> Unit,
        moreInfoButtonClick: (BaseAmenity) -> Unit,
        carParkIconClick: (Boolean) -> Unit,
        screenName: String,
        bookmarkIconClick: (BaseAmenity, BaseActivity.EventSource) -> Unit,
    ) {
        currentDataMarker = item
        objectConfigUiAmenities = BreezeMapDataHolder.amenitiesPreferenceHashmap[item.amenityType]

        viewBinding.imgBookmark.setImageDrawable(
            ContextCompat.getDrawable(
                viewBinding.root.context,
                if (item.isBookmarked) R.drawable.ic_bookmarked else R.drawable.ic_nobookmark
            )
        )

        item.additionalInfo?.let {
            viewBinding.tvAdditionalInfo.visibility = View.VISIBLE
            viewBinding.tvAdditionalInfo.text = it.text
            var color = Color.parseColor(it.style?.light?.color)
            if ((activity.application as? App)?.userPreferenceUtil?.isDarkTheme() == true) {
                color = Color.parseColor(it.style?.dark?.color)
            }
            viewBinding.tvAdditionalInfo.setTextColor(color)
        }

        /**
         * bookmark icon click
         */
        viewBinding.imgBookmark.setOnClickListener {
//            resetView()
            bookmarkIconClick.invoke(item, BaseActivity.EventSource.NONE)
        }

        viewBinding.buttonShare.isVisible =
            MapboxNavigationApp.current()?.getTripSessionState() != TripSessionState.STARTED
        viewBinding.buttonShare.setOnClickListener {
            Analytics.logClickEvent("[map]:tooltip_carpark_share", screenName)
            shareAmenity(item)
        }

        viewBinding.buttonShare.isVisible =
            MapboxNavigationApp.current()?.getTripSessionState() != TripSessionState.STARTED
        viewBinding.buttonInvite.setOnClickListener {
            Analytics.logClickEvent("[map]:tooltip_carpark_invite", screenName)
            inviteAmenity(item)
        }

        carParkToolTipManager?.clickToolTip(
            item,
            shouldRecenter,
            navigationButtonClick,
            moreInfoButtonClick,
            carParkIconClick,
            screenName
        )
        updateTooltipPosition()
    }

    private fun createShareLocationData(item: BaseAmenity) = Arguments.fromBundle(
        bundleOf(
            "address1" to if (item.amenityType == AmenityType.CARPARK) item.name else item.address,
            "latitude" to (item.lat?.toString() ?: ""),
            "longitude" to (item.long?.toString() ?: ""),
            "amenityId" to item.id,
            "amenityType" to item.amenityType,
            "name" to item.name,
        )
    )

    private fun shareAmenity(item: BaseAmenity) {
        viewBinding.root.context.getShareBusinessLogicPackage()?.getRequestsModule()?.flatMap {
            it.callRN(
                ShareBusinessLogicEvent.SHARE_LOCATION_FROM_NATIVE.eventName,
                createShareLocationData(item)
            )
        }?.subscribeOn(Schedulers.io())?.observeOn(AndroidSchedulers.mainThread())?.subscribe()
    }

    private fun inviteAmenity(item: BaseAmenity) {
        viewBinding.root.context.getShareBusinessLogicPackage()?.getRequestsModule()?.flatMap {
            it.callRN(
                ShareBusinessLogicEvent.LOCATION_INVITE_FROM_NATIVE.eventName,
                createShareLocationData(item)
            )
        }?.subscribeOn(Schedulers.io())?.observeOn(AndroidSchedulers.mainThread())?.subscribe()
    }

    @UiThread
    override fun updateBookMarkIconState(saved: Boolean, bookmarkId: Int) {
        currentDataMarker?.isBookmarked = saved
        viewBinding.imgBookmark.setImageDrawable(
            ContextCompat.getDrawable(
                viewBinding.root.context,
                if (saved) R.drawable.ic_bookmarked else R.drawable.ic_nobookmark
            )
        )
        currentDataMarker?.bookmarkId = if (saved) bookmarkId else null
    }

    override fun resetView() {
        carParkToolTipManager?.resetView()
    }

    override fun showMarker() {
        super.showMarker()
        viewBinding.root.visibility = View.VISIBLE

    }

    override fun hideMarker() {
        super.hideMarker()
        viewBinding.root.visibility = View.INVISIBLE

    }

    override fun hideAllToolTips() {
        super.hideAllToolTips()
        if (viewBinding.llToolTip.isShown) {
            viewBinding.llToolTip.visibility = View.INVISIBLE
        }
    }
}
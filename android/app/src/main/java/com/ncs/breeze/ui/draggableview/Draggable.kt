package com.ncs.breeze.ui.draggableview

object Draggable {

    @Deprecated(
        "Use DraggableView.Mode",
        ReplaceWith("DraggableView.Mode", "com.ncs.breeze.ui.draggableview.DraggableView.Mode")
    )
    enum class STICKY {
        NONE,
        AXIS_X,
        AXIS_Y,
        AXIS_XY
    }

    const val DRAG_TOLERANCE = 16
    const val DURATION_MILLIS = 250L
}
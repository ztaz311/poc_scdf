package com.ncs.breeze.ui.dashboard.fragments.viewmodel

import android.app.Application
import com.breeze.model.api.ErrorData
import com.breeze.model.api.request.ShortcutDetails
import com.google.gson.JsonElement
import com.ncs.breeze.common.remote.ApiHelper
import com.ncs.breeze.common.remote.ApiObserver
import com.ncs.breeze.components.SingleLiveEvent
import com.ncs.breeze.ui.base.BaseFragmentViewModel
import com.ncs.breeze.ui.dashboard.fragments.view.NewAddressFragment
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

class NewAddressViewModel @Inject constructor(apiHelper: ApiHelper, application: Application) :
    BaseFragmentViewModel(application) {
    var mAPIHelper: ApiHelper = apiHelper
    var createSuccessful: SingleLiveEvent<Boolean> = SingleLiveEvent()


    fun createShortcut(shortcutDetails: ShortcutDetails, name: String) {
        CoroutineScope(Dispatchers.IO).launch {
            val request = shortcutDetails.copy(
                name = when (shortcutDetails.code) {
                    NewAddressFragment.ShortcutKeys.HOME -> "Home"
                    NewAddressFragment.ShortcutKeys.WORK -> "Work"
                    else -> name
                }
            )
            mAPIHelper.createShortcut(request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : ApiObserver<JsonElement>(compositeDisposable) {
                    override fun onSuccess(data: JsonElement) {
                        Timber.d("shorcut API is successful: " + data.toString())
                        createSuccessful.value = true
                    }

                    override fun onError(e: ErrorData) {
                        Timber.d("shorcut API failed: " + e.toString())
                        createSuccessful.value = false
                    }
                })
        }
    }


    fun updateShortcut(shortcutDetails: ShortcutDetails, name: String, bookmarkId: Int) {
        CoroutineScope(Dispatchers.IO).launch {
            val request = shortcutDetails.copy(
                name = when (shortcutDetails.code) {
                    NewAddressFragment.ShortcutKeys.HOME -> "Home"
                    NewAddressFragment.ShortcutKeys.WORK -> "Work"
                    else -> name
                }
            )

            mAPIHelper.updateShortcut(bookmarkId, request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : ApiObserver<JsonElement>(compositeDisposable) {
                    override fun onSuccess(data: JsonElement) {
                        Timber.d("update shorcut API is successful: $data")
                        createSuccessful.value = true
                    }

                    override fun onError(e: ErrorData) {
                        Timber.d(" update shorcut API failed: $e")
                        createSuccessful.value = false
                    }
                })
        }
    }
}



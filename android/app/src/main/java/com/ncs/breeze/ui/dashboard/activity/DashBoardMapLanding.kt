package com.ncs.breeze.ui.dashboard.activity

import android.app.Activity
import android.location.Location
import android.os.Bundle
import android.util.ArraySet
import com.breeze.model.DestinationAddressDetails
import com.breeze.model.DestinationSearch
import com.breeze.model.api.response.AmenitiesPreference
import com.breeze.model.api.response.ERPResponseData
import com.breeze.model.api.response.amenities.BaseAmenity
import com.breeze.model.api.response.traffic.TrafficResponse
import com.breeze.model.constants.AmenityType.CARPARK
import com.breeze.model.constants.AmenityType.POI
import com.breeze.model.constants.Constants
import com.facebook.react.bridge.Arguments
import com.google.android.gms.maps.model.LatLng
import com.google.gson.JsonObject
import com.mapbox.geojson.Feature
import com.mapbox.geojson.Point
import com.mapbox.maps.CameraOptions
import com.mapbox.maps.MapView
import com.mapbox.maps.extension.observable.eventdata.CameraChangedEventData
import com.mapbox.maps.plugin.animation.easeTo
import com.mapbox.maps.plugin.delegates.listeners.OnCameraChangeListener
import com.mapbox.navigation.core.MapboxNavigation
import com.mapbox.navigation.core.trip.session.TripSessionState
import com.mapbox.navigation.ui.maps.camera.data.MapboxNavigationViewportDataSource
import com.mapbox.navigation.utils.internal.toPoint
import com.mapbox.turf.TurfConstants
import com.mapbox.turf.TurfMeasurement
import com.ncs.breeze.App
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.Event
import com.ncs.breeze.common.analytics.Screen
import com.ncs.breeze.common.constant.RNScreen
import com.ncs.breeze.common.constant.dashboard.LandingMode
import com.ncs.breeze.common.extensions.android.getApp
import com.ncs.breeze.common.utils.BreezeMapDataHolder
import com.ncs.breeze.common.utils.BreezeMapZoomUtil
import com.ncs.breeze.common.utils.ERPControllerData
import com.ncs.breeze.common.utils.ERPControllerData.centerPoint
import com.ncs.breeze.common.utils.LocationBreezeManager
import com.ncs.breeze.common.utils.Utils
import com.ncs.breeze.components.layermanager.MarkerLayerManager
import com.ncs.breeze.components.layermanager.impl.DropPinLayerManager
import com.ncs.breeze.components.layermanager.impl.TrafficMarkerLayerManager
import com.ncs.breeze.components.layermanager.impl.cluster.ClusteredMarkerLayerManager
import com.ncs.breeze.components.map.erp.MapERPManager
import com.ncs.breeze.components.map.profile.ClusteredMapProfile
import com.ncs.breeze.components.map.profile.ListenerCallbackLandingMap
import com.ncs.breeze.components.map.profile.MapProfile
import com.ncs.breeze.components.map.profile.MapProfileFactory
import com.ncs.breeze.components.map.profile.MapProfileOptions
import com.ncs.breeze.components.map.profile.MapProfileType
import com.ncs.breeze.components.map.profile.ProfileZoneLayer
import com.ncs.breeze.components.marker.markerview2.NotificationMarkerView
import com.ncs.breeze.components.marker.markerview2.NotificationMarkerViewManager
import com.ncs.breeze.components.marker.markerview2.TrafficMarkerView
import com.ncs.breeze.helper.DestinationIconManager
import com.ncs.breeze.helper.carpark.CarParkDestinationIconManager
import com.ncs.breeze.helper.marker.MarkerLayerIconHelper
import com.ncs.breeze.reactnative.nativemodule.ShareBusinessLogicEvent
import com.ncs.breeze.ui.dashboard.manager.DashboardMapStateManager
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber

class DashBoardMapLanding {
    lateinit var mapView: MapView
    lateinit var context: Activity
    lateinit var viewportDataSource: MapboxNavigationViewportDataSource

    //map profile instance
    lateinit var mapProfile: MapProfile
    lateinit var mapProfileOptions: MapProfileOptions

    var carparkLastLocation: Location? = null
    var amenitiesLastLocation: Location? = null

    var listErpNearByLocation = ArrayList<ERPResponseData.Erp>()

    var markerLayerClickHandlerEnabled = true

    var currentMapProfileType: MapProfileType = MapProfileType.CLUSTERED

    private var isMoveCameraWrapperAllCarpark = false

    private var listAmenities: HashMap<String, List<BaseAmenity>>? = null
    private var mListCarkPark: ArrayList<BaseAmenity>? = null
    private var showDestination: Boolean = false
    private var mlistTraffic: ArrayList<TrafficResponse>? = null
    private var mAmenityTypes: ArrayList<String>? = null

    var hasDestinationCarPark: Boolean = false
    var furthestERPDistance: Double? = null
    var furthestTrafficDistance: Double? = null

    /**
     * for carpark destination
     */
    var carParkDestinationIconManager: CarParkDestinationIconManager? = null

    /**
     * for erp
     */
    var erpMapViewManager: MapERPManager? = null

    /**
     * for Traffic
     */
    var trafficMarkerViewManager: TrafficMarkerLayerManager? = null


    var dropPinLayerManager: DropPinLayerManager? = null

    /**
     * for Global Notification Symbol layers
     */
    var notificationMarkerLayerManager: NotificationMarkerViewManager? = null


    var trafficDisabled = false

    var callBack: ListenerCallbackLandingMap? = null
    var nightModeEnabled: Boolean = false

    // to reset tooltip on camera change events
    private val resetToolTipCameraChangeListener = object : OnCameraChangeListener {
        override fun onCameraChanged(eventData: CameraChangedEventData) {
            if (mapView.getMapboxMap().cameraState.zoom < (Constants.CLUSTER_MAX_ZOOM + 1)) {
                mapView.getMapboxMap().removeOnCameraChangeListener(this)
                removeALlTooltip()
            }
        }
    }

    /** Triggered after the destination tooltip is successfully rendered */
    private val destinationClickHandler = DestinationIconManager.DestinationClickHandler {
        resetAllViews()
    }

//    private val dropPinLayerClickHandler = object : MarkerLayerManager.MarkerLayerClickHandler {
//        override fun handleOnclick(
//            type: String,
//            feature: Feature,
//            marker: MarkerLayerManager.MarkerViewType?
//        ) {}
//
//        override fun handleOnFreeMapClick(point: Point) {}
//    }

    private val trafficLayerClickHandler = object : MarkerLayerManager.MarkerLayerClickHandler {
        override fun handleOnclick(
            layerType: String,
            feature: Feature,
            currentMarker: MarkerLayerManager.MarkerViewType?
        ) {

            if (currentMarker != null && currentMarker.feature.id() == feature.id()) {
                trafficMarkerViewManager!!.removeMarkerTooltipView()
                return
            }
            mlistTraffic?.let { mlistTraffic ->
                val trafficResponse = mlistTraffic.find {
                    return@find it.id == feature.id()
                }
                trafficResponse?.let { trafficResponse ->
                    handleDisplayMarkerToolTip(trafficResponse, true)
                }
            }
        }

        override fun handleOnFreeMapClick(point: Point) {
            DashboardMapStateManager.resetCurrentLocationFocus(DashboardMapStateManager.TRAFFIC_INSTANCE)
        }
    }

    private val clusterMapMarkerLayerClickHandler =
        object : MarkerLayerManager.MarkerLayerClickHandler {
            override fun handleOnclick(
                layerType: String,
                feature: Feature,
                currentMarker: MarkerLayerManager.MarkerViewType?
            ) {
                if (!markerLayerClickHandlerEnabled) {
                    return
                }
                resetAllViews()
                //If the same marker was clicked then do nothing (resetAllViews would logically close and unselect it )
                if (currentMarker != null && currentMarker.feature.id() == feature.id()) {
                    DashboardMapStateManager.resetCurrentLocationFocus(DashboardMapStateManager.AMENITY_INSTANCE)
                    return
                }

                var layerType = layerType
                // For Clustered data, feature type is not the same as layer type, so we fetch the feature type from the feature selected
                val featureType =
                    feature.getStringProperty(ClusteredMarkerLayerManager.FEATURE_TYPE)
                if (featureType != null) {
                    layerType = featureType
                }
                val updateBottomSheet = false
                when (layerType) {
                    CARPARK -> {
                        mListCarkPark
                            ?.find {
                                it.id == feature.id()
                            }?.let { carPark ->
                                (context as? DashboardActivity)?.let {
                                    it.enableTrackingPan(false)
                                    showCarParkToolTip(carPark, layerType, !updateBottomSheet)
                                    carPark.id?.let { id ->
                                        sendEventParkingIconSelected(id, hasDestinationCarPark)
                                    }
                                    DashboardMapStateManager.setCurrentLocationFocus(
                                        DashboardMapStateManager.AMENITY_INSTANCE,
                                        carPark.lat!!,
                                        carPark.long!!,
                                        if (updateBottomSheet) Constants.DEFAULT_TOOLTIP_ZOOM_RADIUS else null
                                    )
                                    it.enableTrackingPan()
                                }
                            }
                    }

                    else -> {
                        if ((DashboardMapStateManager.selectedMapProfile != null)
                            && (BreezeMapDataHolder.profilePreferenceHashmap[DashboardMapStateManager.selectedMapProfile]?.amenities?.contains(
                                layerType
                            )!!)
                        ) {
                            listAmenities?.let {

                                val amenity = it[POI]?.find { ba ->
                                    return@find ba.id == feature.id()
                                }
                                amenity?.let { amenity ->
                                    handlePoiToolTip(
                                        amenity,
                                        layerType,
                                        layerType,
                                        !updateBottomSheet
                                    )
                                }
                            }
                        } else {
                            listAmenities?.let {

                                val amenity = it[layerType]?.find { ba ->
                                    return@find ba.id == feature.id()
                                }
                                amenity?.let { amenity ->
                                    handleAmenityToolTip(
                                        amenity,
                                        layerType,
                                        layerType,
                                        !updateBottomSheet
                                    )
                                }
                            }
                        }
                    }
                }
                //sets Bottom Sheet state to collapsed
                if (!isDropPinModeOn()) {
                    updateBottomSheetState(0, RNScreen.LANDING)
                }

                // only hide drop in tooltips if user click on other amenity tooltips
                dropPinLayerManager
                    ?.takeIf { layerType != DropPinLayerManager.DROP_PIN_MARKER }
                    ?.run {
                        hideTooltips()
                        removeMarkerIcons()
                    }
            }

            override fun handleOnFreeMapClick(point: Point) {
                DashboardMapStateManager.resetCurrentLocationFocus(DashboardMapStateManager.AMENITY_INSTANCE)
                if (isDropPinModeOn())
                    dropPinMarker(point)
            }
        }

    private val mapClusterLayerClickListener =
        object : ClusteredMarkerLayerManager.ClusterLayerClickHandler {
            override fun handleOnclick(point: Point, zoomLevel: Number) {
                callBack?.onDismissedCameraTracking()
                mapView.getMapboxMap().easeTo(
                    CameraOptions.Builder().center(point).zoom(zoomLevel.toDouble()).build()
                )
            }
        }

    fun dropPinMarker(location: Point) {
        dropPinLayerManager?.showMakerWithTooltip(location)
        (mapProfile as? ClusteredMapProfile)?.addExternalLayerData(
            "drop-pin${MarkerLayerManager.LAYER_POSTFIX}" to "drop-pin"
        )
    }

    fun isDropPinModeOn() =
        (context as? DashboardActivity)?.isDropPinEnabled() ?: false

    /**
     * NOTE:
     * destination icon manager map clicks has more precedence and should be initialized
     * before other amenities managers
     */
    fun setUp(
        pMapView: MapView,
        pMapboxNavigation: MapboxNavigation?,
        pContext: Activity,
        nightModeEnabled: Boolean,
        markerLayerClickHandlerEnabled: Boolean = true,
        isDisableErpClick: Boolean = false,
        pCallBack: ListenerCallbackLandingMap,
    ) {
        val screenName =
            if (pMapboxNavigation?.getTripSessionState() == TripSessionState.STARTED) Screen.HOME_CRUISE_MODE else Screen.HOME

        callBack = pCallBack
        context = pContext
        mapView = pMapView
        this.nightModeEnabled = nightModeEnabled
        viewportDataSource = MapboxNavigationViewportDataSource(
            mapView.getMapboxMap()
        )
        this.markerLayerClickHandlerEnabled = markerLayerClickHandlerEnabled
        //val destinationClickHanDestinationClickHandler
        carParkDestinationIconManager =
            CarParkDestinationIconManager(
                context,
                mapView,
                isMapClickEnabled = true
            )
        carParkDestinationIconManager?.analyticsScreenName = "[homepage]"
        carParkDestinationIconManager!!.destinationClickHandler = destinationClickHandler
        mapProfileOptions = MapProfileOptions.Builder(
            mapView,
            context,
            carParkDestinationIconManager!!,
            screenName
        ).setMarkerLayerClickHandler(clusterMapMarkerLayerClickHandler)
            .setClusterLayerClickHandler(mapClusterLayerClickListener)
            .setNightModeEnabled(nightModeEnabled)
            .setBaseMapCamera((context as DashboardActivity).dashBoardMapCamera)
            .setToolTipRadius(Constants.DEFAULT_TOOLTIP_ZOOM_RADIUS)
            .build()
        mapProfile = MapProfileFactory.generateMapProfile(
            currentMapProfileType,
            mapProfileOptions
        )
        (mapProfile as? ClusteredMapProfile)?.run {
            addExternalSourceData(DropPinLayerManager.DROP_PIN_SOURCE_KEY to DropPinLayerManager.DROP_PIN_MARKER)
            addExternalLayerData(DropPinLayerManager.DROP_PIN_LAYER_KEY to DropPinLayerManager.DROP_PIN_MARKER)
        }

        erpMapViewManager =
            MapERPManager(mapView, context, screenName, isDisableErpClick, pCallBack)
        trafficMarkerViewManager = TrafficMarkerLayerManager(mapView)
        trafficMarkerViewManager!!.markerLayerClickHandler = trafficLayerClickHandler

        dropPinLayerManager = DropPinLayerManager(mapView)

        notificationMarkerLayerManager = NotificationMarkerViewManager(mapView)
    }

    fun switchMapProfile(profile: MapProfileType, isCarparksShown: Boolean, reload: Boolean) {
        if (currentMapProfileType == profile) {
            return
        }
        currentMapProfileType = profile
        mapProfile.destroy()
        mapProfile = MapProfileFactory.generateMapProfile(
            profile,
            mapProfileOptions
        )
        CoroutineScope(Dispatchers.IO).launch {
            mAmenityTypes?.let {
                mapProfile.loadAmenityStyleResources(it)
            }
            withContext(Dispatchers.Main) {
                if (reload) {
                    mapProfile.handleAmenitiesData(
                        listAmenities ?: hashMapOf(),
                        showDestination,
                        DashboardMapStateManager.selectedMapProfile
                    ) {
                        false
                    }
                    mapProfile.handleCarPark(
                        mListCarkPark ?: listOf(),
                        isMoveCameraWrapperAllCarpark,
                        showDestination,
                        !canShowCarParkMarkers() || !isCarparksShown,
                        DashboardMapStateManager.selectedMapProfile
                    )
                }
            }
        }
    }

    fun sendEventCloseToolTips() {
        App.instance?.let { app ->
            val shareBusinessLogicPackage = app.shareBusinessLogicPackage
            shareBusinessLogicPackage.getRequestsModule().flatMap {
                it.callRN(ShareBusinessLogicEvent.SEND_CLOSE_TOOLTIP_TO_REACT.eventName, null)
            }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe()
        }
    }

    /**
     * Updates the bottom sheet state, whether in collapsed(0) , mid(1) or full(2)
     * @param stateIndex the state of bottom sheet
     */
    fun updateBottomSheetState(stateIndex: Int, screen: String, onEnd: () -> Unit = {}) {
        context.getApp()?.shareBusinessLogicPackage?.getRequestsModule()?.flatMap {
            val data = Arguments.createMap().apply {
                putInt(Constants.RN_CONSTANTS.BTM_SHEET_STATE_INDEX, stateIndex)
                putString(Constants.RN_TO_SCREEN, screen)
            }
            it.callRN(ShareBusinessLogicEvent.UPDATE_BOTTOM_SHEET_INDEX.eventName, data)
        }
            ?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe(
                {
                    onEnd.invoke()
                },
                {
                    Timber.e("Error occurred while updating bottom sheet state")
                })
    }


    fun startRoutePlanningFromMarker(ameniti: BaseAmenity, isCarPark: Boolean) {
        val currentLocation = LocationBreezeManager.getInstance().currentLocation ?: return


        CoroutineScope(Dispatchers.IO).launch {
            var destinationDetail: DestinationAddressDetails? = null
            val markerLocation = Location("")
            val sourceAddressDetails = DestinationAddressDetails(
                null,
                Utils.getAddressFromLocation(currentLocation, context),
                null,
                currentLocation.latitude.toString(),
                currentLocation.longitude.toString(),
                null,
                Constants.TAGS.CURRENT_LOCATION_TAG,
                null,
                -1,
            )
            if (ameniti.lat != null && ameniti.long != null) {
                markerLocation.latitude = ameniti.lat!!
                markerLocation.longitude = ameniti.long!!
                val nameLocation = ameniti.name
                val address = Utils.getAddressObject(markerLocation, context)
                destinationDetail = if (address != null) {
                    DestinationAddressDetails(
                        null,
                        nameLocation,
                        address.getAddressLine(0),
                        ameniti.lat!!.toString(),
                        ameniti.long!!.toString(),
                        null,
                        nameLocation,
                        null,
                        -1,
                        isCarPark
                    )
                } else {
                    DestinationAddressDetails(
                        null,
                        nameLocation,
                        null,
                        ameniti.lat!!.toString(),
                        ameniti.long!!.toString(),
                        null,
                        nameLocation,
                        null,
                        -1,
                        isCarPark
                    )
                }

                /**
                 * check if carpark set id will use in navigation get alert
                 */
                if (isCarPark) {
                    destinationDetail.carParkID = ameniti.id
                    destinationDetail.availablePercentImageBand =
                        ameniti.availablePercentImageBand
                    destinationDetail.showAvailabilityFB = ameniti.showAvailabilityFB
                    //destinationDetail.showAvailabilityFB = true
                    destinationDetail.hasAvailabilityCS = ameniti.hasAvailabilityCS
                    //destinationDetail.hasAvailabilityCS = true
                    destinationDetail.availabilityCSData = ameniti.availabilityCSData
//                    val daa = AvailabilityCSData(1676431757000, "Many available lots", "Updated by Breeze User, 10 mins ago", "#933DD8")
//                    destinationDetail.availabilityCSData = daa
                } else {
                    destinationDetail.amenityId = ameniti.id
                }

                /**
                 * apply amenities type to destination
                 */
                destinationDetail.amenityType = ameniti.amenityType

            }
            (context as? DashboardActivity)?.let {
                it.runOnUiThread {
                    if (destinationDetail != null) {
                        val args = Bundle()
                        args.putSerializable(
                            Constants.DESTINATION_ADDRESS_DETAILS,
                            destinationDetail
                        )
                        args.putSerializable(
                            Constants.SOURCE_ADDRESS_DETAILS,
                            sourceAddressDetails
                        )
                        it.showRouteAndResetToCenter(args)
                    }
                }
            }
        }
    }

    fun startWalkingPathFromMarker(ameniti: BaseAmenity, isCarPark: Boolean) {
        val currentLocation = LocationBreezeManager.getInstance().currentLocation ?: return

        CoroutineScope(Dispatchers.IO).launch {
            var destinationDetail: DestinationAddressDetails? = null
            val markerLocation = Location("")
            val sourceAddressDetails = DestinationAddressDetails(
                null,
                Utils.getAddressFromLocation(currentLocation, context),
                null,
                currentLocation.latitude.toString(),
                currentLocation.longitude.toString(),
                null,
                Constants.TAGS.CURRENT_LOCATION_TAG,
                null,
                -1,
            )
            if (ameniti.lat != null && ameniti.long != null) {
                markerLocation.latitude = ameniti.lat!!
                markerLocation.longitude = ameniti.long!!
                val nameLocation = ameniti.name
                val address = Utils.getAddressObject(markerLocation, context)
                destinationDetail = if (address != null) {
                    DestinationAddressDetails(
                        null,
                        nameLocation,
                        address.getAddressLine(0),
                        ameniti.lat!!.toString(),
                        ameniti.long!!.toString(),
                        null,
                        nameLocation,
                        null,
                        -1,
                        isCarPark
                    )
                } else {
                    DestinationAddressDetails(
                        null,
                        nameLocation,
                        null,
                        ameniti.lat!!.toString(),
                        ameniti.long!!.toString(),
                        null,
                        nameLocation,
                        null,
                        -1,
                        isCarPark
                    )
                }

                /**
                 * check if carpark set id will use in navigation get alert
                 */
                if (isCarPark) {
                    destinationDetail.carParkID = ameniti.id
                    destinationDetail.availablePercentImageBand =
                        ameniti.availablePercentImageBand
                } else {
                    destinationDetail.amenityId = ameniti.id
                }

                /**
                 * apply amenities type to destination
                 */
                destinationDetail.amenityType = ameniti.amenityType
            }
            (context as? DashboardActivity)?.let {
                it.runOnUiThread {
                    if (destinationDetail != null) {
                        val args = Bundle()
                        args.putSerializable(
                            Constants.DESTINATION_ADDRESS_DETAILS,
                            destinationDetail
                        )
                        args.putSerializable(
                            Constants.SOURCE_ADDRESS_DETAILS,
                            sourceAddressDetails
                        )
                        it.showWalkingRouteAndResetToCenter(args)
                    }
                }
            }
        }
    }

    suspend fun loadTrafficStyleImages() {
        trafficMarkerViewManager!!.addStyleImages(Constants.TRAFFIC_MARKER)
    }

    suspend fun loadDropPinStyleImages() {
        dropPinLayerManager?.addStyleImages(DropPinLayerManager.DROP_PIN_MARKER)
    }

    suspend fun loadAmenityStyleImages(amenityTypes: ArrayList<String>) {
        this.mAmenityTypes = amenityTypes
        mapProfile.loadAmenityStyleResources(amenityTypes)
    }

    fun displaySearchLocation(
        previousActiveLocationUse: Location?,
        hashMap: java.util.HashMap<String, Any>
    ) {

        DashboardMapStateManager.activeLocationUse?.let { activeLocationUse ->
            removeAmenitiesBasedOnLocation(previousActiveLocationUse, activeLocationUse)
            carParkDestinationIconManager?.run {
                addDestinationIconWithTooltip(
                    activeLocationUse.latitude,
                    activeLocationUse.longitude,
                    hashMap,
                    DestinationSearch.fromRNHashMapData(hashMap)
                )
                if (hashMap["availabilityCSData"] != null) // searched location has crowdsource data
                    updateBottomSheetState(0, RNScreen.LANDING)
                showDestinationTooltip()
            }
        }

    }


    /**
     * remove all marker for amenities & carpark, only if location has changed
     */
    private fun removeAmenitiesBasedOnLocation(
        previousActiveLocationUse: Location?,
        activeLocationUse: Location
    ) {

        if (previousActiveLocationUse == null) {
            mapProfile.clearALlMarkerAndTooltipAmenities()
        } else {
            if ((previousActiveLocationUse.latitude != activeLocationUse.latitude) ||
                (previousActiveLocationUse.longitude != activeLocationUse.longitude)
            ) {
                mapProfile.clearALlMarkerAndTooltipAmenities()
            }
        }
    }


    /**
     * handle amenities data
     */
    fun handleAmenitiesData(listAmenities: HashMap<String, List<BaseAmenity>>) {
        this.listAmenities = listAmenities
        val locationAtParkingClick = DashboardMapStateManager.activeLocationUse
        if (locationAtParkingClick != null) {
            mapProfile.handleAmenitiesData(
                listAmenities,
                showDestination,
                DashboardMapStateManager.selectedMapProfile
            ) { false }
            handleModeSelectionOnMap(false)
        }
    }

    private fun getSelectedSubItems(
        amenityPref: AmenitiesPreference,
        it: List<BaseAmenity>
    ): ArrayList<BaseAmenity> {
        val listEnabled = ArrayList<BaseAmenity>(it)
        amenityPref.subItems!!.forEach { subItem ->
            if (subItem.isSelected != true) {
                listEnabled.removeAll(it.filter { amenity ->
                    if (!amenity.provider.isNullOrBlank() && subItem.elementName?.lowercase() == amenity.provider!!.lowercase()) {
                        return@filter true
                    } else if (!amenity.name.isNullOrBlank() && subItem.elementName?.lowercase() == amenity.name!!.lowercase()) {
                        return@filter true
                    }
                    return@filter false
                })
            }
        }
        if (listEnabled.isEmpty()) {
            listEnabled.addAll(it)
        }
        return listEnabled
    }

    /**
     * handle show carpark
     */
    fun handleCarpark(
        fetchedCarparks: ArrayList<BaseAmenity>,
        isMoveCameraWrapperAllCarpark: Boolean = false,
        showDestination: Boolean,
    ) {
        this.isMoveCameraWrapperAllCarpark = isMoveCameraWrapperAllCarpark
        this.showDestination = showDestination
        if (DashboardMapStateManager.isInSearchLocationMode && DashboardMapStateManager.isInMapProfileSelectedMode) {
            val allCarParks = addSelectedCarParks(fetchedCarparks)
            this.mListCarkPark = ArrayList(allCarParks)
        } else {
            this.mListCarkPark = fetchedCarparks
        }
        val updatedCarParkList = this.mListCarkPark!!
        val locationAtParkingClick = DashboardMapStateManager.activeLocationUse
        if (locationAtParkingClick != null) {
            updateDestinationCarparkState(showDestination, updatedCarParkList)
            mapProfile.handleCarPark(
                updatedCarParkList,
                isMoveCameraWrapperAllCarpark,
                showDestination,
                !canShowCarParkMarkers(),
                DashboardMapStateManager.selectedMapProfile
            )
            if (isMoveCameraWrapperAllCarpark
                && DashboardMapStateManager.activeLocationUse != null
                && DashboardMapStateManager.currentModeSelected.canShowCarparks()
            ) {
                if (DashboardMapStateManager.isInMapProfileSelectedMode) {
                    zoomToProfileDefault()
                } else if (DashboardMapStateManager.isInSearchLocationMode) {
                    val destinationCarpark =
                        mListCarkPark?.find { carpark -> carpark.destinationCarPark == true }
                    if (destinationCarpark != null) {
                        showCarParkToolTip(destinationCarpark, "carpark", true)
                    } else {
                        if (DashboardMapStateManager.searchLocationAddress?.availabilityCSData != null) // searched location has crowdsource data
                            updateBottomSheetState(0, RNScreen.LANDING)
                        carParkDestinationIconManager?.showDestinationTooltip()
                    }
                } else {
                    zoomToDefault()
                }
            }

            handleModeSelectionOnMap(true)
        }
    }

    private fun addSelectedCarParks(mListCarkPark: ArrayList<BaseAmenity>): ArraySet<BaseAmenity> {
        val allCarParks = ArraySet<BaseAmenity>()
        BreezeMapDataHolder.profilePreferenceHashmap[DashboardMapStateManager.selectedMapProfile]?.let { profileData ->
            val carParks = profileData.defaultAmenities.find {
                it.type == CARPARK
            }
            carParks?.let { profileCarParks ->
                this.mListCarkPark?.filter {
                    profileCarParks.ids.contains(it.id)
                }?.let {
                    allCarParks.addAll(it)
                }
            }
        }
        allCarParks.addAll(mListCarkPark)
        return allCarParks
    }

    private fun updateDestinationCarparkState(
        showDestination: Boolean,
        mListCarkPark: ArrayList<BaseAmenity>
    ) {
        if (showDestination) {
            hasDestinationCarPark = false
            for (mCarkParkItem in mListCarkPark) {
                if (mCarkParkItem.destinationCarPark == true) {
                    hasDestinationCarPark = true
                    break
                }
            }

        }
    }

    fun canShowCarParkMarkers() =
        DashboardMapStateManager.currentModeSelected == LandingMode.PARKING || DashboardMapStateManager.currentModeSelected == LandingMode.OBU

    private fun handleModeSelectionOnMap(refreshCarParkToolTip: Boolean) {
        when (DashboardMapStateManager.currentModeSelected) {
            LandingMode.PARKING,
            LandingMode.OBU -> {
//                mapProfile.hideAmenities()
                if (refreshCarParkToolTip) {
                    this.mListCarkPark?.let {
                        mapHandleToParkingMode(it)
                    }
                }
            }

            LandingMode.ERP -> {
                mapHandleToERPMode()
            }

            LandingMode.TRAFFIC -> {
                mapHandleToTrafficMode()
            }

            else -> {}
        }
    }

    private fun mapHandleToTrafficMode() {
        hideAllAmenitiesAndCarParks()
    }

    private fun mapHandleToERPMode() {
        hideAllAmenitiesAndCarParks()
    }

    private fun mapHandleToParkingMode(carParks: ArrayList<BaseAmenity>) {
        (context as? DashboardActivity)?.onCameraTrackingDismissed()
        if (carParks.isEmpty()) return

        var carPark = carParks.find { it.destinationCarPark == true } ?: carParks[0]
        if ((context as? DashboardActivity)?.shouldFocusNearestCarpark() == true) {
            if (carPark.destinationCarPark != true)
                carPark = carParks.minByOrNull { it.distance ?: Int.MAX_VALUE } ?: carParks[0]
            showCarParkToolTip(carPark, "carpark", true)
        }
        if ((context as? DashboardActivity)?.shouldFocusNearestCarpark() == false) {
            DashboardMapStateManager.setCurrentLocationFocus(
                DashboardMapStateManager.AMENITY_INSTANCE,
                carPark.lat!!,
                carPark.long!!,
                null
            )
        }

        (context as? DashboardActivity)?.updateShouldFocusNearestCarpark(false)
        carPark.id?.let {
            sendEventParkingIconSelected(it, hasDestinationCarPark)
        }
    }


    fun sendEventParkingIconSelected(idCarpark: String, mHasDestinationCarPark: Boolean) {
        val locationFocusForReact =
            (context as? DashboardActivity)?.getLocationFocusedForReact()
        App.instance?.let { app ->
            val shareBusinessLogicPackage = app.shareBusinessLogicPackage
            shareBusinessLogicPackage.getRequestsModule().flatMap {
                val rnData = Arguments.createMap().apply {
                    putString("parkingID", idCarpark)
                    putBoolean("isDestinationHasCarPark", mHasDestinationCarPark)
                    locationFocusForReact?.let { loc ->
                        putString(Constants.RN_CONSTANTS.FOCUSED_LAT, loc.latitude.toString())
                        putString(Constants.RN_CONSTANTS.FOCUSED_LONG, loc.longitude.toString())
                    }
                }
                it.callRN(
                    ShareBusinessLogicEvent.PARKING_BOTTOM_SHEET_REFRESH.eventName,
                    rnData
                )
            }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe()
        }
    }

    fun refreshBookmarkIconTooltip(bookmarkId: Int?) {
        bookmarkId?.let {
            mapProfile.refreshBookmarkIconTooltip(true, it)
            if (carParkDestinationIconManager?.isDestinationTooltipOpen() == true) {
                carParkDestinationIconManager?.refreshBookmarkIconTooltip(true, it)
            }
        }
    }

    private fun showCarParkToolTip(
        amenity: BaseAmenity,
        layerType: String,
        shouldRecenter: Boolean
    ) {
        mapProfile.showCarParkToolTip(
            amenity,
            showDestination,
            true,
            callBack,
            layerType,
            shouldRecenter
        ) { amenity, isCarPark ->
            startRoutePlanningFromMarker(amenity, isCarPark)
        }
    }


    fun clearALlCarPark() {
        mapProfile.clearALlCarPark()
        DashboardMapStateManager.resetCurrentLocationFocus()
    }


    fun setCameraToolTipThreshold() {
        mapView.getMapboxMap().addOnCameraChangeListener(resetToolTipCameraChangeListener)
    }

    fun removeCameraToolTipThresholdListener() {
        mapView.getMapboxMap().removeOnCameraChangeListener(resetToolTipCameraChangeListener)
    }

    fun removeALlTooltip() {
        mapProfile.removeALlTooltip()
        DashboardMapStateManager.resetCurrentLocationFocus()
    }

    fun clearALlERP() {
        erpMapViewManager?.removeAllMarkers()
        DashboardMapStateManager.resetCurrentLocationFocus()
    }


    fun clearALlMarkerAndTooltipAmenities() {
        mapProfile.clearALlMarkerAndTooltipAmenities()
        DashboardMapStateManager.resetCurrentLocationFocus()
    }


    fun filterAllAmenitiesSelect(amenityType: String, isSelected: Boolean) {
        mapProfile.filterAllAmenitiesSelect(
            amenityType,
            isSelected,
            listAmenities?.get(amenityType) ?: listOf(),
            showDestination
        )
    }

    fun removeSearchLocationMarker() {
        carParkDestinationIconManager?.removeDestinationIcon()
    }

    fun removeSearchLocationToolTip() {
        carParkDestinationIconManager?.removeDestinationIconTooltip()
    }

    fun hideAllCarPark() {
        mapProfile.hideAllCarPark(mListCarkPark ?: listOf(), showDestination)
        DashboardMapStateManager.resetCurrentLocationFocus()
    }

    fun showDropPin() {
    }

    fun showAllCarPark() {
        mapProfile.showAllCarPark(
            mListCarkPark ?: listOf(),
            isMoveCameraWrapperAllCarpark,
            showDestination,
            false,
            DashboardMapStateManager.selectedMapProfile
        )
    }

    fun hideAllAmenitiesAndCarParks() {
        mapProfile.hideAllAmenitiesAndCarParks()
        DashboardMapStateManager.resetCurrentLocationFocus()
    }

    fun handleERP(
        timeToCheckERP: Long = System.currentTimeMillis(),
        zoomToERPDefault: Boolean = true
    ) {
        erpMapViewManager?.removeAllMarkers()
        val locationActive = DashboardMapStateManager.activeLocationUse
        locationActive?.let {
            listErpNearByLocation =
                ERPControllerData.filterERPNearByLocation(locationActive)
            if (listErpNearByLocation.size > 0) {
                erpMapViewManager?.handleERP(timeToCheckERP, listErpNearByLocation)
                furthestERPDistance = erpMapViewManager?.calculateFurthestErpDistance(
                    erpList = listErpNearByLocation,
                    targetPoint = locationActive.toPoint()
                )
                if (zoomToERPDefault) {
                    if (furthestERPDistance == 0.0) {
                        zoomToDefault()
                    } else {
                        BreezeMapZoomUtil.zoomViewToRange(
                            mapView.getMapboxMap(),
                            listErpNearByLocation[0].centerPoint(),
                            furthestERPDistance!!,
                            (context as? DashboardActivity)?.dashBoardMapCamera?.maxEdgeInsets!!,
                            animationEndCB = {}
                        )
                    }
                }
            } else {
                if (zoomToERPDefault) {
                    zoomToDefault()
                }
            }
            (context as? DashboardActivity)?.onCameraTrackingDismissed()
        }
    }


    private fun zoomToProfileDefault() {
        DashboardMapStateManager.selectedMapProfile?.let {
            var radius: Double? = ProfileZoneLayer.getZoneMaxRadius(it)
            BreezeMapZoomUtil.recenterMapToLocation(
                mapView.getMapboxMap(),
                lat = DashboardMapStateManager.activeLocationUse!!.latitude,
                lng = DashboardMapStateManager.activeLocationUse!!.longitude,
                edgeInset = (context as? DashboardActivity)?.dashBoardMapCamera?.maxEdgeInsets!!,
                zoomRadius = radius
            )
        }
    }

    private fun zoomToDefault() {
        val radius: Double? = (context as? DashboardActivity)?.determineZoomRadius()
        BreezeMapZoomUtil.recenterMapToLocation(
            mapView.getMapboxMap(),
            lat = DashboardMapStateManager.activeLocationUse!!.latitude,
            lng = DashboardMapStateManager.activeLocationUse!!.longitude,
            edgeInset = (context as? DashboardActivity)?.dashBoardMapCamera?.maxEdgeInsets!!,
            zoomRadius = radius
        )
    }

    fun TrafficResponse.toPoint(): Point {
        return Point.fromLngLat(
            this.long!!.toDouble(),
            this.lat!!.toDouble()
        )
    }

    /**
     * handle start
     */
    fun handleTraffic(listTraffic: ArrayList<TrafficResponse>?) {
        mlistTraffic = listTraffic
        trafficDisabled = false
        trafficMarkerViewManager!!.removeMarkerTooltipView(Constants.TRAFFIC_MARKER)
        if (listTraffic != null && listTraffic.size > 0) {
            val listPoint = ArrayList<Point>()
            listTraffic.forEach {
                listPoint.add(it.toPoint())
            }
            addTrafficIcon(Constants.TRAFFIC_MARKER, listTraffic)
            handleDisplayMarkerToolTip(listTraffic[0], false, false)
            if (listTraffic.size == 1) {
                BreezeMapZoomUtil.recenterToPoint(
                    mapView.getMapboxMap(),
                    listTraffic[0].toPoint(),
                    animationEndCB = {})
            } else {
                furthestTrafficDistance =
                    calculateFurthestTrafficDistance(listTraffic)
                BreezeMapZoomUtil.zoomViewToRange(
                    mapView.getMapboxMap(),
                    listTraffic[0].toPoint(),
                    furthestTrafficDistance!!,
                    (context as? DashboardActivity)?.dashBoardMapCamera?.maxEdgeInsets!!,
                    animationEndCB = {}
                )
            }
        }
    }


    private fun calculateFurthestTrafficDistance(
        listTraffic: ArrayList<TrafficResponse>
    ): Double {

        val trafficSize = listTraffic.size
        val furthestTrafficFromLocation = listTraffic[trafficSize - 1].distance!!.toDouble()
        var furthestDistance = TurfMeasurement.distance(
            Point.fromLngLat(listTraffic[0].long!!, listTraffic[0].lat!!),
            Point.fromLngLat(
                listTraffic[trafficSize - 1].long!!,
                listTraffic[trafficSize - 1].lat!!
            ),
            TurfConstants.UNIT_METRES
        )
        if ((furthestDistance > furthestTrafficFromLocation) || (trafficSize < 3)) {
            return furthestDistance
        }
        for (i in 1 until (trafficSize - 1)) {
            val distance = TurfMeasurement.distance(
                Point.fromLngLat(listTraffic[0].long!!, listTraffic[0].lat!!),
                Point.fromLngLat(
                    listTraffic[i].long!!,
                    listTraffic[i].lat!!
                ),
                TurfConstants.UNIT_METRES
            )
            if (distance > furthestDistance) {
                furthestDistance = distance
            }
        }
        return if (furthestDistance > furthestTrafficFromLocation) furthestDistance else furthestTrafficFromLocation
    }


    fun handleUpdateCenterOfMarker() {
        DashboardMapStateManager.currentLocationFocus?.let {
            BreezeMapZoomUtil.recenterMapToLocation(
                mapView.getMapboxMap(),
                it.latitude,
                it.longitude,
                (context as? DashboardActivity)?.dashBoardMapCamera?.overviewEdgeInsets!!,
                -1.0,
                DashboardMapStateManager.forceFocusZoom
            )

            //reset forceFocusZoom after first recenter
            DashboardMapStateManager.forceFocusZoom = null

            return@handleUpdateCenterOfMarker
        }
        (context as? DashboardActivity)?.let {

//            it.dashBoardMapCamera.maxEdgeInsets = EdgeInsets(
//                it.dashBoardMapCamera.maxEdgeInsets.top,
//                it.dashBoardMapCamera.maxEdgeInsets.left,
//                it.dashBoardMapCamera.overviewEdgeInsets.bottom,
//                it.dashBoardMapCamera.maxEdgeInsets.right
//            )

            BreezeMapZoomUtil.adjustExistingCameraPadding(
                mapView.getMapboxMap(),
                it.dashBoardMapCamera.maxEdgeInsets
            )
        }
    }


    fun addTrafficIcon(type: String, items: List<TrafficResponse>) {
        val featureList: ArrayList<Feature> = ArrayList()
        items.forEach {
            val properties = JsonObject()
            properties.addProperty(MarkerLayerManager.HIDDEN, false)
            MarkerLayerIconHelper.handleUnSelectedIcon(properties, nightModeEnabled)
            var feature =
                Feature.fromGeometry(it.toPoint(), properties, it.id)
            featureList.add(feature)
        }
        trafficMarkerViewManager!!.addOrReplaceMarkers(type, featureList)
    }

    fun addNotificationMarker(point: Point, iconResId: Int = -1, resIconUrl: String? = null) {
        val notificationMarker = NotificationMarkerView(
            LatLng(point.latitude(), point.longitude()),
            mapView.getMapboxMap(),
            activity = context
        )
        notificationMarker.renderMarkerToolTip(iconResId, resIconUrl)
        notificationMarker.showMarker()
        notificationMarkerLayerManager?.addMarker(notificationMarker)

        BreezeMapZoomUtil.recenterToPoint(
            mapView.getMapboxMap(),
            Point.fromLngLat(point.longitude(), point.latitude()),
            (context as? DashboardActivity)?.dashBoardMapCamera?.maxEdgeInsets!!,
            animationEndCB = {
            }
        )
    }


    fun clearAllMarkerNotification() {
        notificationMarkerLayerManager?.removeAllMarkers()
    }

    /**
     * resets all tool-tip views excluding destination tooltip
     */
    private fun resetAllViews() {
        erpMapViewManager!!.hideAllTooltips()
        trafficMarkerViewManager!!.removeMarkerTooltipView()
        mapProfile.removeALlTooltip()
        notificationMarkerLayerManager!!.removeAllMarkers()
    }

    private fun handleAmenityToolTip(
        amenity: BaseAmenity,
        type: String,
        layerType: String,
        shouldRecenter: Boolean
    ) {
        mapProfile.handleAmenityToolTip(
            amenity,
            type,
            callBack,
            layerType,
            shouldRecenter,
            true,
            startRoutePlanningFromMarker = { amenity, isCarPark ->
                startRoutePlanningFromMarker(amenity, isCarPark)
            }
        )
        DashboardMapStateManager.setCurrentLocationFocus(
            DashboardMapStateManager.AMENITY_INSTANCE, amenity.lat!!, amenity.long!!,
            if (shouldRecenter) null else Constants.POI_TOOLTIP_ZOOM_RADIUS
        )
    }

    private fun handlePoiToolTip(
        amenity: BaseAmenity,
        type: String,
        layerType: String,
        shouldRecenter: Boolean
    ) {
        mapProfile.handlePoiToolTip(
            amenity,
            type,
            callBack,
            layerType,
            shouldRecenter,
            { amenity, isCarPark ->
                startRoutePlanningFromMarker(amenity, isCarPark)
            },
            { amenity, isCarPark ->
                startWalkingPathFromMarker(amenity, isCarPark)
            })
        DashboardMapStateManager.setCurrentLocationFocus(
            DashboardMapStateManager.AMENITY_INSTANCE, amenity.lat!!, amenity.long!!,
            if (shouldRecenter) null else Constants.POI_TOOLTIP_ZOOM_RADIUS
        )
    }

    private fun handleDisplayMarkerToolTip(
        trafficResponse: TrafficResponse,
        recenter: Boolean,
        setCurrentFocus: Boolean = true
    ) {
        val trafficMarkerView = TrafficMarkerView(
            LatLng(trafficResponse.lat!!, trafficResponse.long!!),
            mapView.getMapboxMap(),
            context,
            trafficResponse.id
        )
        val properties = JsonObject()
        properties.addProperty(MarkerLayerManager.HIDDEN, false)
        MarkerLayerIconHelper.handleSelectedIcon(properties, nightModeEnabled)
        var feature = Feature.fromGeometry(
            trafficResponse.toPoint(), properties, trafficResponse.id
        )
        trafficMarkerViewManager!!.renderMarkerToolTipView(
            MarkerLayerManager.MarkerViewType(
                trafficMarkerView,
                Constants.TRAFFIC_MARKER,
                feature
            )
        )

        /**
         * set current locationfocus
         */
        if (setCurrentFocus) {
            DashboardMapStateManager.setCurrentLocationFocus(
                DashboardMapStateManager.TRAFFIC_INSTANCE,
                trafficResponse.lat!!,
                trafficResponse.long!!,
                null
            )
        }

        trafficMarkerView.handleClick(
            trafficResponse,
            recenter = recenter,
            heightBottomSheet = DashboardMapStateManager.landingBottomSheetHeight
        ) { res ->
            Analytics.logClickEvent(Event.MAP_TRAFFIC_ICON_CLICK, Screen.HOME_TRAFFIC_CHECKER)
            callBack?.onDismissedCameraTracking()
        }
    }

//    private fun handleDisplayMarkerToolTip(
//        trafficResponse: TrafficResponse,
//        recenter: Boolean,
//        setCurrentFocus: Boolean = true
//    ) {
//        val trafficMarkerView = TrafficMarkerView(
//            LatLng(trafficResponse.lat!!, trafficResponse.long!!),
//            mapView.getMapboxMap(),
//            context,
//            trafficResponse.id
//        )
//        val properties = JsonObject()
//        properties.addProperty(MarkerLayerManager.HIDDEN, false)
//        MarkerLayerIconHelper.handleSelectedIcon(properties, nightModeEnabled)
//        var feature = Feature.fromGeometry(
//            trafficResponse.toPoint(), properties, trafficResponse.id
//        )
//        trafficMarkerViewManager!!.renderMarkerToolTipView(
//            MarkerLayerManager.MarkerViewType(
//                trafficMarkerView,
//                Constants.TRAFFIC_MARKER,
//                feature
//            )
//        )
//
//        /**
//         * set current locationfocus
//         */
//        if (setCurrentFocus) {
//            DashboardMapStateManager.setCurrentLocationFocus(
//                DashboardMapStateManager.TRAFFIC_INSTANCE,
//                trafficResponse.lat!!,
//                trafficResponse.long!!,
//                null
//            )
//        }
//
//        trafficMarkerView.handleClick(
//            trafficResponse,
//            recenter = recenter,
//            heightBottomSheet = DashboardMapStateManager.landingBottomSheetHeight
//        ) { res ->
//            Analytics.logClickEvent(Event.MAP_TRAFFIC_ICON_CLICK, Screen.HOME_TRAFFIC_CHECKER)
//            callBack?.onDismissedCameraTracking()
//        }
//    }

    fun hideAllTraffic() {
        trafficMarkerViewManager?.removeMarkerTooltipView()
        trafficMarkerViewManager?.hideAllMarker()
    }

    fun clearAllTraffic() {
        trafficDisabled = true
        trafficMarkerViewManager?.removeMarkerTooltipView()
        trafficMarkerViewManager?.removeAllMarkers()
        DashboardMapStateManager.resetCurrentLocationFocus()
    }

    fun clearGlobalNotificationMarkers() {
        notificationMarkerLayerManager?.removeAllMarkers()
    }

    /**
     * handle when user selected to camera
     */
    fun handleSelectedCamera(cameraID: String) {
        val trafficResponse = mlistTraffic?.find {
            return@find it.id == cameraID
        }
        trafficResponse?.let { trafficResponse ->
            handleDisplayMarkerToolTip(trafficResponse, true)
        }
    }


    fun isAmenitiesEmpty(): Boolean {
        return mapProfile.isAmenitiesEmpty()
    }

    fun isCarParkEmpty(): Boolean {
        return mapProfile.isCarParkEmpty()
    }

}
package com.ncs.breeze.common.utils

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkInfo
import android.net.NetworkRequest
import androidx.annotation.RequiresPermission
import timber.log.Timber

class NetworkMonitor
@RequiresPermission(android.Manifest.permission.ACCESS_NETWORK_STATE)
constructor(private val application: Application) {

    fun startNetworkCallback() {
        val cm: ConnectivityManager =
            application.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val builder: NetworkRequest.Builder = NetworkRequest.Builder()

        cm.registerNetworkCallback(
            builder.build(),
            object : ConnectivityManager.NetworkCallback() {

                override fun onAvailable(network: Network) {
                    Timber.d("Internet - Network monitor: onAvailable")
                    //Variables.isNetworkConnected = true
                }

                override fun onLost(network: Network) {
                    Timber.d("Internet - Network monitor: onLost")
                    //Variables.isNetworkConnected = false
                }

                override fun onLosing(network: Network, maxMsToLive: Int) {
                    Timber.d("Internet - Network monitor: onLosing")
                    super.onLosing(network, maxMsToLive)
                }

                override fun onUnavailable() {
                    Timber.d("Internet - Network monitor: onUnavailable")
                    super.onUnavailable()
                }
            })
    }

    fun stopNetworkCallback() {
        Timber.d("Internet - Network monitor: stopNetworkCallback called")
        val cm: ConnectivityManager =
            application.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        cm.unregisterNetworkCallback(ConnectivityManager.NetworkCallback())
    }

    //Deprecated in API 29
    fun oldNetwork() {
        fun isNetworkAvailable(): Boolean {
            val connectivityManager = application.getSystemService(Context.CONNECTIVITY_SERVICE)
            return if (connectivityManager is ConnectivityManager) {
                val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo
                networkInfo?.isConnected ?: false
            } else false
        }
    }
}
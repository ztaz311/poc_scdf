package com.ncs.breeze.components

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import com.ncs.breeze.databinding.LayoutWarningProcessWithoutAccountBinding

class BottomSheetWarningWithoutAccount @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : ConstraintLayout(context, attrs, defStyle) {

    private var listener: ListenerWarningWithoutAccount? = null
    private val binding: LayoutWarningProcessWithoutAccountBinding

    init {
        binding = LayoutWarningProcessWithoutAccountBinding.inflate(
            LayoutInflater.from(context),
            this,
            true
        )
        binding.tvCloseWarningWithoutAccount.setOnClickListener {
            gone()
            listener?.close()
        }

        binding.tvProcess.setOnClickListener {
            gone()
            listener?.process()
        }

        binding.tvLater.setOnClickListener {
            gone()
            listener?.later()
        }

    }

    fun setListener(pListener: ListenerWarningWithoutAccount) {
        listener = pListener
    }

    fun gone() {
        this.visibility = View.GONE
    }

    fun show() {
        this.visibility = View.VISIBLE
    }


    interface ListenerWarningWithoutAccount {
        fun close()
        fun later()
        fun process()
    }
}
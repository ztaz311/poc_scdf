package com.ncs.breeze.ui.navigation

import android.app.Activity
import android.location.Location
import androidx.appcompat.app.AlertDialog
import androidx.collection.arraySetOf
import com.breeze.model.DestinationAddressDetails
import com.breeze.model.api.request.AmenitiesRequest
import com.breeze.model.api.response.amenities.Amenities
import com.breeze.model.api.response.amenities.BaseAmenity
import com.breeze.model.constants.AmenityType.CARPARK
import com.breeze.model.constants.AmenityType.EVCHARGER
import com.breeze.model.constants.AmenityType.PETROL
import com.breeze.model.constants.Constants
import com.breeze.model.enums.AmenityBand
import com.google.android.gms.maps.model.LatLng
import com.google.gson.JsonObject
import com.mapbox.api.directions.v5.models.DirectionsRoute
import com.mapbox.api.directions.v5.models.RouteOptions
import com.mapbox.geojson.Feature
import com.mapbox.geojson.LineString
import com.mapbox.geojson.Point
import com.mapbox.maps.MapView
import com.mapbox.navigation.base.extensions.coordinates
import com.mapbox.navigation.base.route.NavigationRoute
import com.mapbox.navigation.base.route.NavigationRouterCallback
import com.mapbox.navigation.base.route.RouterFailure
import com.mapbox.navigation.base.route.RouterOrigin
import com.mapbox.navigation.core.lifecycle.MapboxNavigationApp
import com.mapbox.turf.TurfConstants
import com.mapbox.turf.TurfMeasurement
import com.ncs.breeze.R
import com.ncs.breeze.car.breeze.utils.BreezeCarUtil
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.Event
import com.ncs.breeze.common.analytics.Screen
import com.ncs.breeze.common.components.maplayer.WalkingRouteLineLayer
import com.ncs.breeze.common.extensions.android.getUserDataPreference
import com.ncs.breeze.common.extensions.mapbox.updateDestinationAndLocationPuckLayer
import com.ncs.breeze.common.remote.ApiHelper
import com.ncs.breeze.common.rxbus.RxBus
import com.ncs.breeze.common.rxbus.RxEvent
import com.ncs.breeze.common.storage.BreezeUserPreference
import com.ncs.breeze.common.utils.BreezeMapDataHolder
import com.ncs.breeze.common.utils.LocationBreezeManager
import com.ncs.breeze.common.utils.ParkingIconUtils
import com.ncs.breeze.common.utils.Utils
import com.ncs.breeze.common.utils.VoiceInstructionsManager
import com.ncs.breeze.components.layermanager.MarkerLayerManager
import com.ncs.breeze.components.marker.markerview2.EVMarkerView
import com.ncs.breeze.components.marker.markerview2.PetrolMarkerView
import com.ncs.breeze.components.marker.markerview2.carpark.SimpleCarParkMarkerView
import com.ncs.breeze.helper.DestinationIconManager
import com.ncs.breeze.helper.marker.AmenityMarkerHelper
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.cancel
import kotlinx.coroutines.ensureActive
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.util.Collections

class TurnByTurnAmenityViewManager(
    private val mapViewHolder: MapViewDataHolder,
    val apiHelper: ApiHelper,
    val activity: Activity,
    private val originalDestinationDetails: DestinationAddressDetails,
    var currentRoute: NavigationRoute,
    val handleDismissCamera: (() -> Unit)? = null,
) {
    private val scope: CoroutineScope =
        CoroutineScope(SupervisorJob() + Dispatchers.Main.immediate)
    val amenityWayPoints = arraySetOf<BaseAmenity>()
    private var amenityMarkerManager: MarkerLayerManager
    private var amenityMarkerHelper: AmenityMarkerHelper
    private var destinationIconManager: DestinationIconManager
    private var walkingDestinationIconManager: DestinationIconManager? = null

    private val markerLayerClickHandler = initMarkerLayerClickHandler()
    private var listCarPark = ArrayList<BaseAmenity>()
    private var listNearbyAmenities = arrayListOf<BaseAmenity>()

    // waypoints including destination excluding source
    var waypointsRemaining: Int
    var alternativeCarpark: BaseAmenity? = null


    /**
     * NOTE:
     * destination icon manager map clicks has more precedence and should be initialized
     * before other amenities managers
     */
    init {
        if (mapViewHolder.mapView.getMapboxMap().getStyle() == null) {
            throw IllegalStateException("mapboxMap style is not initialized")
        }

        // removing source coordinate to calculate waypoint list size
        waypointsRemaining = currentRoute.routeOptions.coordinatesList().size - 1

        destinationIconManager =
            DestinationIconManager(
                activity = activity,
                mapView = mapViewHolder.mapView
            )
        amenityMarkerManager = MarkerLayerManager(
            mapViewHolder.mapView,
            nightMode = mapViewHolder.nightModeEnabled
        )
        amenityMarkerHelper =
            AmenityMarkerHelper(amenityMarkerManager, mapViewHolder.nightModeEnabled, true)
        amenityMarkerManager.markerLayerClickHandler = markerLayerClickHandler
        scope.launch(Dispatchers.Main) {
            amenityMarkerManager.addStyleImages(arrayListOf(CARPARK, PETROL, EVCHARGER))
        }
        VoiceInstructionsManager.setupVoice(activity.applicationContext)
    }

    fun addAlternativeCarpark(alternativeCarpark: BaseAmenity) {
        val carparks = ArrayList(listCarPark)
        carparks.add(alternativeCarpark)
        this.alternativeCarpark = alternativeCarpark
        scope.launch {
            withContext(Dispatchers.IO) {
                renderCarParks(carparks)
            }
        }
    }

    private suspend fun renderCarParks(carParks: List<BaseAmenity>) {
        try {
            withContext(Dispatchers.Main) {
                ensureActive()
                removeCarParks()
                listCarPark.addAll(carParks)
                amenityMarkerHelper.addAmenityMarkerView(
                    type = CARPARK,
                    items = listCarPark,
                    hidden = false,
                    showDestination = false
                )
                //clear others amenity
                removeNearByAmenity(PETROL)
                val petrolData = amenityWayPoints.filter { it.amenityType == PETROL }
                if (petrolData.isNotEmpty()) {
                    listNearbyAmenities.addAll(petrolData)
                    amenityMarkerHelper.addAmenityMarkerView(
                        type = PETROL,
                        items = petrolData,
                        hidden = false,
                        showDestination = false
                    )
                }
                removeNearByAmenity(EVCHARGER)
                val evData = amenityWayPoints.filter { it.amenityType == EVCHARGER }
                if (evData.isNotEmpty()) {
                    listNearbyAmenities.addAll(evData)
                    amenityMarkerHelper.addAmenityMarkerView(
                        type = EVCHARGER,
                        items = evData,
                        hidden = false,
                        showDestination = false
                    )
                }
                mapViewHolder.mapView.updateDestinationAndLocationPuckLayer()
            }
        } catch (e: CancellationException) {
            Timber.e(e, "job was cancelled")
        }
    }

    suspend fun renderNearbyAmenities(amenityData: Amenities) {
        amenityData.type
            ?.let { amenityType ->
                try {
                    withContext(Dispatchers.Main) {
                        ensureActive()
                        removeNearByAmenity(amenityType)
                        if (amenityData.data.isNotEmpty()) {
                            listNearbyAmenities.addAll(amenityData.data)
                            amenityMarkerHelper.addAmenityMarkerView(
                                type = amenityType,
                                items = amenityData.data,
                                hidden = false,
                                showDestination = false
                            )

                        }
                        mapViewHolder.mapView.updateDestinationAndLocationPuckLayer()
                    }
                } catch (e: CancellationException) {
                    Timber.e(e, "job was cancelled")
                }
            }
    }


    fun removeNearByAmenity(amenityType: String) {
        amenityMarkerManager.removeMarkers(amenityType)
        listNearbyAmenities.removeIf { it.amenityType == amenityType }
    }

    fun removeCarParks() {
        amenityMarkerManager.removeMarkers(CARPARK)
        listCarPark.clear()
    }

    suspend fun fetchAndHandleCarParks(
        locationPoint: Point,
        destinationName: String?,
        isDestinationCarPark: Boolean,
        announceWarning: Boolean = false
    ) {
        val resultCount = BreezeUserPreference.getInstance(activity)
            .getCarParkCountSettingValue(BreezeCarUtil.masterlists)
        val destCarParkRequest = createGetCarparkRequest(locationPoint)
        val carParkDest = kotlin.runCatching { apiHelper.getAmenitiesAsync(destCarParkRequest) }
        if (carParkDest.isSuccess) {
            val resp = carParkDest.getOrNull()
            resp?.amenities?.find {
                it.type == CARPARK
            }?.data?.let { data ->
                val filteredData = if (!isDestinationCarPark) {
                    data.take(resultCount)
                } else {
                    data.filter {
                        return@filter (it.name != destinationName)
                    }.take(resultCount)
                }
                if (filteredData.isEmpty()) {
                    showNoAmenitiesNearByDialog(CARPARK)
                }
                val list = alternativeCarpark?.let { alternative ->
                    val found =
                        filteredData.find { amenity -> amenity.id == alternative.id } != null
                    if (!found) {
                        filteredData.toMutableList().apply { add(alternative) }
                    } else null
                } ?: filteredData

                renderCarParks(list)
                if (isDestinationCarPark) {
                    handleAnnouncementWarning(announceWarning, destinationName, data)
                }
                val points = filteredData.map {
                    Point.fromLngLat(
                        it.long ?: 0.0,
                        it.lat ?: 0.0
                    )
                }.toMutableList()
                points.add(locationPoint)
                setCameraToPoints(points)
            } ?: showNoAmenitiesNearByDialog(CARPARK)
        }
    }

    private fun createGetCarparkRequest(locationPoint: Point) =
        AmenitiesRequest(
            lat = locationPoint.latitude(),
            long = locationPoint.longitude(),
            rad = BreezeUserPreference.getInstance(activity)
                .getCarParkDistanceSettingValue(BreezeCarUtil.masterlists),
            resultCount = BreezeUserPreference.getInstance(activity)
                .getCarParkCountSettingValue(BreezeCarUtil.masterlists),
            maxRad = Constants.PARKING_CHECK_RADIUS_MAX,
        ).apply {
            setListType(
                arrayListOf(CARPARK), BreezeUserPreference.getInstance(activity)
                    .getCarParkAvailabilityValue(BreezeCarUtil.masterlists, "")
            )
            setCarParkCategoryFilter()
        }

    /**
     * Fetch Petrol(s) and EV chargers from remote with default request (max 5 amenities in 1km radius)
     * @param currentLocation: User's current location
     * @param onResult: list of amenities, empty if it's failed
     * */
    suspend fun fetchNearbyAmenities(
        currentLocation: Location,
        onResult: (amenities: List<Amenities>) -> Unit
    ) {
        val amenitiesRequest = AmenitiesRequest(
            lat = currentLocation.latitude,
            long = currentLocation.longitude,
            rad = activity.getUserDataPreference()?.getEvPetrolRange()?.div(1000.0)
                ?: Constants.DEFAULT_PETROL_EV_RADIUS,
            maxRad = activity.getUserDataPreference()?.getEvPetrolMaxRange()?.div(1000.0)
                ?: Constants.DEFAULT_PETROL_EV_MAX_RADIUS,
            resultCount = activity.getUserDataPreference()?.getEvPetrolResultCount()
                ?: Constants.DEFAULT_PETROL_EV_RESULT_COUNT

        ).apply {
            setListType(listOf(PETROL, EVCHARGER))
            val amenityListPref =
                BreezeMapDataHolder.amenitiesPreference.filter { it.elementName == PETROL || it.elementName == EVCHARGER }
            if (amenityListPref.isNotEmpty()) {
                setAmenitiesPreferenceListType(amenityListPref)
            }
        }
        val apiResult = kotlin.runCatching { apiHelper.getAmenitiesAsync(amenitiesRequest) }
        if (apiResult.isSuccess) {
            val amenities = apiResult.getOrNull()?.amenities
                ?.filter { it.type == EVCHARGER || it.type == PETROL }
                ?: listOf()
            amenities.forEach { amenityData ->
                amenityData.data.forEach {
                    it.amenityType = amenityData.type ?: ""
                }
            }
            onResult.invoke(amenities)
        } else onResult.invoke(listOf())
    }

    /**
     * plays the warning announcement id the destination is a carpark and announceWarning is enabled
     */
    private suspend fun handleAnnouncementWarning(
        announceWarning: Boolean,
        destinationName: String?,
        data: List<BaseAmenity>
    ) {
        if (announceWarning) {
            destinationName?.let { name ->
                val existingCarPark = data.find { it.name == name }
                existingCarPark?.let { existingCarPark ->
                    val band = AmenityBand.getAmenityBand(existingCarPark.availablePercentImageBand)
                    if (band == AmenityBand.TYPE0 || band == AmenityBand.TYPE1) {
                        VoiceInstructionsManager.getInstance()?.playVoiceInstructions(
                            activity.getString(R.string.voice_carpark_warning)
                        )
                    }
                }
            }
        }
    }

    fun getScreenName(): String {
        return Screen.NAVIGATION
    }

    private fun initMarkerLayerClickHandler() =
        object : MarkerLayerManager.MarkerLayerClickHandler {
            override fun handleOnclick(
                layerType: String,
                feature: Feature,
                currentMarker: MarkerLayerManager.MarkerViewType?
            ) {
                amenityMarkerManager.removeMarkerTooltipView()
                if (currentMarker != null && currentMarker.feature.id() == feature.id()) {
                    return
                }
                when (layerType) {
                    CARPARK -> {
                        Analytics.logClickEvent(Event.TAP_CARPARK_CLICK, getScreenName())
                        val carpark = listCarPark.find { it.id == feature.id() }
                        carpark?.let { amenity ->
                            handleDismissCamera?.invoke()
                            showCarParkToolTip(amenity)
                        }
                    }

                    EVCHARGER -> {
                        listNearbyAmenities.find { it.id == feature.id() }
                            ?.let { amenity ->
                                handleDismissCamera?.invoke()
                                Analytics.logClickEvent(Event.TAP_EV_CLICK, getScreenName())
                                showEvChargerToolTip(amenity)
                            }
                    }

                    PETROL -> {
                        listNearbyAmenities.find { it.id == feature.id() }
                            ?.let { amenity ->
                                handleDismissCamera?.invoke()
                                Analytics.logClickEvent(Event.TAP_PETROL_CLICK, getScreenName())
                                showPetrolToolTip(amenity)
                            }
                    }
                }
            }

            override fun handleOnFreeMapClick(point: Point) {
                // no op
            }
        }

    private fun addAmenityFeature(amenity: BaseAmenity): Feature {
        val properties = JsonObject()
        amenityMarkerHelper?.handleSelectedAmenityProperties(properties, false, amenity)
        return Feature.fromGeometry(
            Point.fromLngLat(
                amenity.long!!,
                amenity.lat!!
            ), properties, amenity.id
        )
    }

    private fun showPetrolToolTip(amenity: BaseAmenity) {
        val amenitiesMarkerView = PetrolMarkerView(
            LatLng(amenity.lat!!, amenity.long!!),
            mapViewHolder.mapView.getMapboxMap(),
            activity,
            amenity.id,
            amenityWayPoints.size >= Constants.MAX_WAY_POINT_LIMIT
        )
        val amenityFeature = addAmenityFeature(amenity)
        amenityMarkerManager.renderMarkerToolTipView(
            MarkerLayerManager.MarkerViewType(
                amenitiesMarkerView,
                PETROL,
                amenityFeature
            )
        )
        val selected = amenityWayPoints.any { amenity.id == it.id }
        amenitiesMarkerView.handleMarkerClick(
            selected,
            amenity,
            addStopClick = { item, eventSource ->
                if (amenityWayPoints.size >= Constants.MAX_WAY_POINT_LIMIT) {
                    showExceededAlert()
                    return@handleMarkerClick false
                }
                amenityWayPoints.add(item)
                updateRouteWithWayPoints()
                amenityMarkerManager.removeMarkerTooltipView()
                Analytics.logClickEvent(Event.PETROL_ADD_STOP, getScreenName())
                return@handleMarkerClick true
            },
            removeStopClick = { item, eventSource ->
                item.id?.let { amenityId ->
                    amenityWayPoints.removeIf { it.id == amenityId }

                    scope.launch(Dispatchers.Main) {
                        renderNearbyAmenities(Amenities(
                            type = item.amenityType,
                            iconUrl = item.iconUrl,
                            data = listNearbyAmenities.filter { amenity -> amenity.amenityType == item.amenityType && amenity.id != item.id }
                        ))
                    }
                    updateRouteWithWayPoints()
                    amenityMarkerManager.removeMarkerTooltipView()
                    return@handleMarkerClick true
                }
                return@handleMarkerClick true
            },
            markerClick = {
            }
        )
    }

    private fun showEvChargerToolTip(amenity: BaseAmenity) {
        val amenitiesMarkerView = EVMarkerView(
            LatLng(amenity.lat!!, amenity.long!!),
            mapViewHolder.mapView.getMapboxMap(),
            activity,
            amenity.id,
            amenityWayPoints.size >= Constants.MAX_WAY_POINT_LIMIT
        )
        val amenityFeature = addAmenityFeature(amenity)
        amenityMarkerManager.renderMarkerToolTipView(
            MarkerLayerManager.MarkerViewType(
                amenitiesMarkerView,
                EVCHARGER,
                amenityFeature
            )
        )
        val selected = amenityWayPoints.find { amenity.id == it.id } != null
        amenitiesMarkerView.handleMarkerClick(
            selected,
            amenity,
            addStopClick = { item, _ ->
                if (amenityWayPoints.size >= Constants.MAX_WAY_POINT_LIMIT) {
                    showExceededAlert()
                    return@handleMarkerClick false
                }
                amenityWayPoints.add(item)
                updateRouteWithWayPoints()
                amenityMarkerManager.removeMarkerTooltipView()
                Analytics.logClickEvent(Event.EV_ADD_STOP, getScreenName())
                return@handleMarkerClick true
            },
            removeStopClick = { item, eventSource ->
                item.id?.let { amenityId ->
                    amenityWayPoints.removeIf { amenityId == it.id }
                    scope.launch(Dispatchers.Main) {
                        renderNearbyAmenities(Amenities(
                            type = item.amenityType,
                            iconUrl = item.iconUrl,
                            data = listNearbyAmenities.filter { amenity -> amenity.amenityType == item.amenityType && amenity.id != item.id }
                        ))
                    }
                    updateRouteWithWayPoints()
                    amenityMarkerManager.removeMarkerTooltipView()
                }
                return@handleMarkerClick true
            },
            markerClick = {
            }
        )
    }

    private fun showCarParkToolTip(amenity: BaseAmenity, shouldRecenter: Boolean = true) {
        val carParkMarkerView = SimpleCarParkMarkerView(
            LatLng(amenity.lat!!, amenity.long!!),
            mapViewHolder.mapView.getMapboxMap(),
            activity,
            amenity.id
        )
        val properties = JsonObject()
        amenityMarkerHelper.handleSelectedCarParkProperties(amenity, properties, false, false)
        val feature = Feature.fromGeometry(
            Point.fromLngLat(
                amenity.long!!,
                amenity.lat!!
            ), properties, amenity.id
        )

        amenityMarkerManager.renderMarkerToolTipView(
            MarkerLayerManager.MarkerViewType(
                carParkMarkerView,
                CARPARK,
                feature
            )
        )

        carParkMarkerView.clickToopTip(
            amenity,
            shouldRecenter,
            navigationButtonClick = { item, eventSource ->
                Analytics.logClickEvent(Event.CARPARK_NAVIGATE_HERE, getScreenName())
                if (item.long != null && item.lat != null) {
                    findRouteToCarPark(item)
                    (activity as? TurnByTurnNavigationActivity)?.run {
                        showCustomToast(
                            icon = R.drawable.tick_successful,
                            message = getString(R.string.toast_routing_to_carpark),
                            duration = 3
                        )
                    }
                }
            },
            moreInfoButtonClick = { item ->
                Analytics.logClickEvent(Event.PARKING_SEE_MORE, mapViewHolder.screenName)
            },
            carParkIconClick = { },
            mapViewHolder.screenName
        )
    }

    private fun updateRouteWithWayPoints() {
        (activity as? TurnByTurnNavigationActivity)?.reRouteBasedOnCurrentLocation()
    }

    private fun clearTBTAmenityToggleViewType() {
        (activity as? TurnByTurnNavigationActivity)?.resetToggleViewAmenityType()
    }

    private fun showNoAmenitiesNearByDialog(type: String) {
        (activity as? TurnByTurnNavigationActivity)?.showNoNearbyAmenitiesAlert(type)
    }

    private fun setCameraToPoints(points: List<Point>) {
        scope.launch(Dispatchers.Main) {
            (activity as? TurnByTurnNavigationActivity)?.zoomToPoints(points)
        }
    }

    fun findRouteToCarPark(carParkAmenity: BaseAmenity) {
        val currentLocation = LocationBreezeManager.getInstance().currentLocation ?: return
        val originPoint = Point.fromLngLat(currentLocation.longitude, currentLocation.latitude)
        val destinationPoint = Point.fromLngLat(carParkAmenity.long!!, carParkAmenity.lat!!)
        val originalDest = Point.fromLngLat(
            originalDestinationDetails.long!!.toDouble(),
            originalDestinationDetails.lat!!.toDouble()
        )
        clearTBTAmenityToggleViewType()
//        amenityWayPoints.clear()
        val updateRouteOptionsBuilder =
            currentRoute.routeOptions.toBuilder()
                .coordinates(
                    originPoint,
                    amenityWayPoints.map { Point.fromLngLat(it.long ?: 0.0, it.lat ?: 0.0) },
                    destinationPoint
                ).waypointIndicesList(listOf(0, amenityWayPoints.size + 1))
        carParkAmenity.name?.let {
            val destinationNameEscaped = Utils.escapeAmpersandWithSpace(it)
            val waypointNamesList = java.util.ArrayList(Collections.nCopies(1, ""))
            waypointNamesList.add(destinationNameEscaped)
            updateRouteOptionsBuilder.waypointNamesList(waypointNamesList)
        }

        requestRoutesToCarPark(
            updateRouteOptions = updateRouteOptionsBuilder.build(),
            originalDest = originalDest,
            carPark = carParkAmenity,
            wasPreviousDestinationCarPark = originalDestinationDetails.isDestinationCarPark()
        )
    }

    private fun requestRoutesToCarPark(
        updateRouteOptions: RouteOptions,
        originalDest: Point,
        carPark: BaseAmenity,
        wasPreviousDestinationCarPark: Boolean
    ) {
        MapboxNavigationApp.current()?.requestRoutes(
            updateRouteOptions,
            object : NavigationRouterCallback {
                override fun onCanceled(routeOptions: RouteOptions, routerOrigin: RouterOrigin) {}

                override fun onFailure(reasons: List<RouterFailure>, routeOptions: RouteOptions) {}

                override fun onRoutesReady(
                    routes: List<NavigationRoute>,
                    routerOrigin: RouterOrigin
                ) {
                    if (routes.isNotEmpty()) {
//                        amenityWayPoints.clear()
                        removeCarParks()
//                        clearTBTAmenityToggleViewType()
//                        removeNearByAmenity(PETROL)
//                        removeNearByAmenity(EVCHARGER)
                        MapboxNavigationApp.current()?.setNavigationRoutes(routes)
                        val route = routes[0]

                        val currentDestinationDetails = DestinationAddressDetails(
                            easyBreezyAddressID = null,
                            address1 = carPark.name,
                            address2 = null,
                            lat = carPark.lat.toString(),
                            long = carPark.long.toString(),
                            distance = null,
                            destinationName = carPark.name,
                            destinationAddressType = null,
                            ranking = -1,
                            isCarPark = true,
                            carParkID = carPark.id,
                            availablePercentImageBand = carPark.availablePercentImageBand,
                            showAvailabilityFB = carPark.showAvailabilityFB,
                            hasAvailabilityCS = carPark.hasAvailabilityCS,
                            availabilityCSData = carPark.availabilityCSData,
                            routableLocations = arrayListOf(),
                        )
                        requestToUpdateCurrentDestination(currentDestinationDetails)

                        if (wasPreviousDestinationCarPark) {
                            addDestinationIcon(
                                route.directionsRoute,
                                R.drawable.destination_puck
                            )
                        } else {
                            //val iconsToAdd = mapViewHolder.iconToAdd
                            val iconsToAdd = ParkingIconUtils.getCarParkIconForNavigation(
                                carPark,
                                isDestination = false
                            )
                            addDestinationIcon(route.directionsRoute, iconsToAdd)
                            if (route.routeOptions.coordinatesList().isNotEmpty()) {
                                requestToFindWalkingPath(
                                    route.routeOptions.coordinatesList().last(),
                                    originalDest
                                )
                            }
                        }
                    }
                }
            })
    }

    private fun requestToUpdateCurrentDestination(currentDestinationDetails: DestinationAddressDetails) {
        RxBus.publish(
            RxEvent.RequestToUpdateCurrentDestination(
                currentDestinationDetails = currentDestinationDetails
            )
        )
    }

    private fun requestToFindWalkingPath(startingPoint: Point, endingPoint: Point) {
        RxBus.publish(
            RxEvent.RequestToFindWalking(
                startingPoint = startingPoint,
                endingPoint = endingPoint
            )
        )
    }

    fun addDestinationIcon(
        route: DirectionsRoute,
        iconToAdd: Int
    ) {
        val geometry = LineString.fromPolyline(route.geometry()!!, Constants.POLYLINE_PRECISION)
        destinationIconManager.addDestinationIcon(
            geometry.coordinates().last()!!.latitude(),
            geometry.coordinates().last()!!.longitude(),
            iconToAdd
        )
    }

    fun addWalkingRouteLine(walkingRoute: NavigationRoute) {
        mapViewHolder.mapView.getMapboxMap().getStyle()?.let { style ->
            if (walkingDestinationIconManager == null) {
                walkingDestinationIconManager = DestinationIconManager(
                    activity = activity,
                    destinationSourceId = Constants.WALKING_DESTINATION_SOURCE,
                    destinationLayerId = Constants.WALKING_DESTINATION_LAYER,
                    destinationImageId = Constants.WALKING_DESTINATION_ICON_ID,
                    mapView = mapViewHolder.mapView
                )
            }
            walkingDestinationIconManager?.let {
                val geometry = LineString.fromPolyline(
                    walkingRoute.directionsRoute.geometry()!!,
                    Constants.POLYLINE_PRECISION
                )
                it.addDestinationIcon(
                    geometry.coordinates().last()!!.latitude(),
                    geometry.coordinates().last()!!.longitude()
                )
                WalkingRouteLineLayer(
                    style,
                    activity
                ).generateRouteLineLayer(geometry)
            }
        }
    }

    fun removeWalkingRouteLine() {
        mapViewHolder.mapView.getMapboxMap().getStyle { style ->
            WalkingRouteLineLayer(
                style,
                activity
            ).removeLineLayer()
            walkingDestinationIconManager?.removeDestinationIcon()
        }
    }

    private fun showExceededAlert() {
        showAlert("Maximum ${Constants.MAX_WAY_POINT_LIMIT} waypoints are allowed for now")
    }

    private fun showAlert(message: String) {
        val builder: AlertDialog.Builder = activity.let {
            AlertDialog.Builder(it)
        }

        builder.setMessage(message)
            .setPositiveButton(
                R.string.alert_close
            ) { dialog, _ ->
                dialog.dismiss()
            }

        val alertDialog: AlertDialog = builder.create()

        alertDialog.setOnShowListener {
            alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).isAllCaps = false
            alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).isAllCaps = false
        }

        alertDialog.show()
    }

    /***
     * Check and remove points with a distance
     * of less than 50m from the currentLocation
     * in #amenityWayPoints
     */
    fun hasPassedWaypoints(currentLocation: Point) {
        val removed = amenityWayPoints.removeIf {
            if (it.long != null && it.lat != null) {
                val distance = TurfMeasurement.distance(
                    currentLocation,
                    Point.fromLngLat(it.long!!, it.lat!!),
                    TurfConstants.UNIT_METERS
                )
                distance <= 50
            } else false
        }
        if (removed) {
            updateRouteWithWayPoints()
            amenityMarkerManager.removeMarkerTooltipView()
        }
    }

    fun destroy() {
        scope.cancel()
    }

    data class MapViewDataHolder(
        val mapView: MapView,
        val screenName: String,
        val iconToAdd: Int,
        val nightModeEnabled: Boolean
    )

}
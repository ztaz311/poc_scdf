package com.ncs.breeze.car.breeze.screen.obulite

import android.annotation.SuppressLint
import android.content.Intent
import androidx.car.app.CarToast
import androidx.car.app.ScreenManager
import androidx.car.app.model.Action
import androidx.car.app.model.ActionStrip
import androidx.car.app.model.CarColor
import androidx.car.app.model.CarIcon
import androidx.car.app.model.Template
import androidx.car.app.navigation.model.NavigationTemplate
import androidx.core.graphics.drawable.IconCompat
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.lifecycleScope
import com.breeze.model.TripType
import com.mapbox.androidauto.internal.logAndroidAuto
import com.mapbox.androidauto.map.compass.CarCompassRenderer
import com.mapbox.navigation.core.lifecycle.MapboxNavigationApp
import com.mapbox.navigation.core.trip.session.TripSessionState
import com.ncs.breeze.App
import com.ncs.breeze.R
import com.ncs.breeze.car.breeze.base.BaseNavigationScreenCar
import com.ncs.breeze.car.breeze.ehorizon.CarRoadObjectRenderer
import com.ncs.breeze.car.breeze.screen.navigation.NavigationScreen
import com.ncs.breeze.car.breeze.screen.navigation.OBUNotificationHelper
import com.ncs.breeze.car.breeze.style.CarFreeDriveStyle
import com.ncs.breeze.car.breeze.utils.BreezeCarUtil
import com.ncs.breeze.car.breeze.utils.LimitClick
import com.ncs.breeze.car.breeze.utils.setClickSafe
import com.ncs.breeze.car.breeze.widget.roadlabel.CarRoadLabelRenderer
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.Event
import com.ncs.breeze.common.analytics.Screen
import com.ncs.breeze.common.appSync.AppSyncHelper
import com.ncs.breeze.common.appSync.ClientFactory
import com.ncs.breeze.common.constant.OBUConnectionState
import com.ncs.breeze.common.constant.OBUConstants
import com.ncs.breeze.common.event.ToCarRx
import com.ncs.breeze.common.helper.ehorizon.EHorizonMode
import com.ncs.breeze.common.helper.obu.OBUDataHelper
import com.ncs.breeze.common.model.rx.CarERPRefresh
import com.ncs.breeze.common.model.rx.CarIncidentDataRefresh
import com.ncs.breeze.common.model.rx.ETAStartedCar
import com.ncs.breeze.common.model.rx.OBUDisplayLifecycleEvent
import com.ncs.breeze.common.model.rx.ToCarRxEvent
import com.ncs.breeze.common.model.rx.UpdateETACar
import com.ncs.breeze.common.model.rx.UpdateStateVoiceCar
import com.ncs.breeze.common.rxbus.RxBus
import com.ncs.breeze.common.rxbus.RxEvent
import com.ncs.breeze.common.utils.*
import com.ncs.breeze.common.utils.eta.ETAEngine
import com.ncs.breeze.service.CruiseControllerService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import java.lang.ref.WeakReference

/**
 * cruise mode screen
 */
class CarOBULiteScreen(
    private val obuLiteCarContext: OBULiteCarContext,
) :
    BaseNavigationScreenCar(obuLiteCarContext.mainCarContext) {

    private val speedLimitHandler = object : SpeedLimitHandler {
        override fun alertOnThreshold() {
            //VoiceInstructionsManager.getInstance()?.playVoiceInstructions(cruiseModeCarContext.mainCarContext.carContext.getString(R.string.voice_speed_limit_alert))
            BreezeAudioUtils.playOverSpeedAlert(mainCarContext.carContext)
        }
    }

    private var carDynamicStyle = CarFreeDriveStyle(mainCarContext)
    private val carLocationRenderer = CarOBULiteLocationRenderer(obuLiteCarContext)
    private val carSpeedLimitRenderer =
        SpeedLimitRenderer(obuLiteCarContext.mainCarContext.mapboxCarContext)
    private val carRoadLabelRenderer = CarRoadLabelRenderer()
    private val carCompassRenderer = CarCompassRenderer()

    private val carMapViewLayer = RoadLabelSurfaceLayer(
        obuLiteCarContext.carContext
    )
    private val carRoadObjectRenderer = CarRoadObjectRenderer(
        obuLiteCarContext.mainCarContext,
        EHorizonMode.CRUISE,
        null
    )
    private val incidentCompositeDisposable = CompositeDisposable()
    private val obuEventsCompositeDisposable = CompositeDisposable()
    private val obuNotificationHelper by lazy { OBUNotificationHelper(this) }
    private var obuLiteStrip: ActionStrip? = null
    private var obuLiteMapStrip: ActionStrip? = null

    private var isConnected = false
    private var currentOBUBalance: Double? = null

    override fun onGetTemplate(): Template {
        updateActionStrip()
        return NavigationTemplate.Builder()
            //.setBackgroundColor(CarColor.PRIMARY)
            .setBackgroundColor(
                CarColor.createCustom(
                    carContext.getColor(R.color.color_theme_bg),
                    carContext.getColor(R.color.color_theme_bg)
                )
            )
            .setActionStrip(obuLiteStrip!!)
            .setMapActionStrip(obuLiteMapStrip!!)
            .build()
    }

    override fun onReceiverEventFromApp(event: ToCarRxEvent?) {
        if (event is UpdateStateVoiceCar) {
            invalidate()
        }

        if (event is ETAStartedCar) {
            CarToast.makeText(
                carContext,
                R.string.car_share_confirmation_toast,
                CarToast.LENGTH_LONG
            )
            invalidate()
        }

        if (event is UpdateETACar) {
            handleETALiveLocationToggleEventCar(
                event.isETARunning,
                event.recipientName
            )
        }
    }

    private fun onUpdateVoiceState() {
        VoiceInstructionsManager.getInstance()?.toggleMute()
        if (true == VoiceInstructionsManager.getInstance()?.isUserMute) {
            Analytics.logClickEvent(Event.AA_MUTE, Screen.AA_OBU_LITE_DISPLAY)
        } else {
            Analytics.logClickEvent(Event.AA_UNMUTE, Screen.AA_OBU_LITE_DISPLAY)
        }
    }

    fun onToggleSharingGPS() {
        val wasETARunning = ETAEngine.getInstance()!!.isETARunning()
        ETAEngine.getInstance()!!.toggleLiveLocation()
        if (wasETARunning) {

            Analytics.logClickEvent(Event.AA_PAUSE_SHARING, Screen.AA_OBU_LITE_DISPLAY)
        } else {
            Analytics.logClickEvent(Event.AA_RESUME_SHARING, Screen.AA_OBU_LITE_DISPLAY)
        }
    }

    override fun getScreenName(): String {
        return Screen.AA_OBU_LITE_DISPLAY
    }

    override fun onCreateScreen() {
        super.onCreateScreen()
        Analytics.logEvent(Event.USER_CLICK, Event.OBU_CRUISE_START, Screen.ANDROID_AUTO_CRUISE)
        OBUStripStateManager.getInstance()?.setCarOpened()
        OBUStripStateManager.getInstance()?.obuTriggered()

        CarOBULiteManager.carContextRef = WeakReference(carContext)
        lifecycle.addObserver(CarOBULiteManager.carLifecycleObserver)
        lifecycle.addObserver(object : DefaultLifecycleObserver {
            override fun onCreate(owner: LifecycleOwner) {
                super.onCreate(owner)
                OBUStripStateManager.getInstance()?.let { obuStripStateManager ->
                    obuStripStateManager.obuStripCurrentState.observe(owner) {
                        if (it == OBUStripState.CLOSED || it == OBUStripState.NONE) {
                            ToCarRx.postEventWithCacheLast(OBUDisplayLifecycleEvent(Lifecycle.State.DESTROYED))
                        }
                    }
                }
            }

            override fun onStart(owner: LifecycleOwner) {
                super.onStart(owner)
                changeOBUConnectionState((carContext.applicationContext as? App)?.obuConnectionHelper?.obuConnectionState?.value == OBUConnectionState.CONNECTED)
            }

            override fun onResume(owner: LifecycleOwner) {
                super.onResume(owner)
                listenOBUEvents()
            }

            override fun onPause(owner: LifecycleOwner) {
                super.onPause(owner)
                clearOBUEventListener()
            }
        })
        startFeatureDetection()

        compositeDisposable.add(
            RxBus.listen(RxEvent.SpeedChanged::class.java)
                .subscribe {
                    when (it.value) {
                        SpeedLimitUtil.NORMAL_SPEED -> {
                            SpeedLimitUtil.setAARedBorder(false)
                        }

                        SpeedLimitUtil.OVER_SPEED -> {
                            SpeedLimitUtil.setAARedBorder(true)
                        }
                    }
                })
    }

    private fun startFeatureDetection() {
        CoroutineScope(Dispatchers.IO).launch {
            BreezeCarUtil.breezeFeatureDetectionUtil.initFeatureDetectionRoadObjects()
            BreezeCarUtil.breezeERPUtil.startERPHandler()
            BreezeCarUtil.breezeIncidentsUtil.startIncidentHandler()
            BreezeCarUtil.breezeMiscRoadObjectUtil.startMiscROHandler()
            BreezeCarUtil.breezeSchoolSilverZoneUpdateUtil.startSchoolSilverZoneHandler()
            incidentCompositeDisposable.clear()
            incidentCompositeDisposable.add(
                RxBus.listen(RxEvent.IncidentDataRefresh::class.java)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe {
                        ToCarRx.postEvent(CarIncidentDataRefresh(it.incidentData))
                    }
            )
            incidentCompositeDisposable.add(
                RxBus.listen(RxEvent.ERPRefresh::class.java)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe {
                        it.erpFeature?.let { erpFeature ->
                            ToCarRx.postEvent(CarERPRefresh(erpFeature))
                        }
                    }
            )
            AppSyncHelper.subscribeToAppSyncEvents(mainCarContext.carContext.applicationContext)
        }
    }

    override fun onDestroyScreen() {
        super.onDestroyScreen()
        stopDetectionAndDispose()
        compositeDisposable.dispose()
    }

    private fun stopDetectionAndDispose() {
        val topScreen = mainCarContext.carContext.getCarService(
            ScreenManager::class.java
        ).top
        CoroutineScope(Dispatchers.IO).launch {

            if (topScreen !is NavigationScreen
            ) {
                BreezeCarUtil.breezeFeatureDetectionUtil.stopFeatureDetection()
            }

            incidentCompositeDisposable.dispose()
        }
    }

    private fun handleETALiveLocationToggleEventCar(
        isETARunning: Boolean,
        recipientName: String
    ) {
        CarToast.makeText(
            carContext,
            if (isETARunning)
                carContext.getString(R.string.eta_notification_resumed, recipientName)
            else
                carContext.getString(R.string.eta_notification_paused, recipientName),
            CarToast.LENGTH_LONG
        ).show()
        invalidate()
    }

    @SuppressLint("MissingPermission")
    override fun onStartScreen() {
        super.onStartScreen()
        logAndroidAuto("CarNavigateScreen onStart")
        VoiceInstructionsManager.setupVoice(carContext.applicationContext)
        obuLiteCarContext.mapboxCarMap.registerObserver(carDynamicStyle)
        obuLiteCarContext.mapboxCarMap.registerObserver(carLocationRenderer)
        obuLiteCarContext.mapboxCarMap.registerObserver(carSpeedLimitRenderer)
        obuLiteCarContext.mapboxCarMap.registerObserver(carRoadLabelRenderer)
        obuLiteCarContext.mapboxCarMap.registerObserver(carCompassRenderer)
        obuLiteCarContext.mapboxCarMap.registerObserver(carMapViewLayer)
        obuLiteCarContext.mapboxCarMap.registerObserver(carRoadObjectRenderer)

        LocationBreezeManager.getInstance().startLocationUpdates()
        TripLoggingManager.setupTripLogging(
            carContext.applicationContext,
            BreezeCarUtil.breezeTripLoggingUtil,
            BreezeCarUtil.breezeStatisticsUtil
        )
        TripLoggingManager.getInstance()
            ?.startTripLogging(TripType.OBULITE, Screen.OBU_LITE_SCREEN, null)

        MapboxNavigationApp.current()?.let {
            SpeedLimitUtil.setupLocationObserver(it, speedLimitHandler)
            if (BreezeCarUtil.isActivityNotAvaiable && it.getTripSessionState() != TripSessionState.STARTED) {
                ClientFactory.clearMutationCache()
                it.startTripSession()
                startCruiseControllerService()
            }
        }
    }

    private fun startCruiseControllerService() {
        carContext.startService(Intent(carContext, CruiseControllerService::class.java))
    }

    override fun onStopScreen() {
        super.onStopScreen()
        logAndroidAuto("CarNavigateScreen onStop")
        obuLiteCarContext.mapboxCarMap.unregisterObserver(carDynamicStyle)
        obuLiteCarContext.mapboxCarMap.unregisterObserver(carLocationRenderer)
        obuLiteCarContext.mapboxCarMap.unregisterObserver(carSpeedLimitRenderer)
        obuLiteCarContext.mapboxCarMap.unregisterObserver(carCompassRenderer)
        obuLiteCarContext.mapboxCarMap.unregisterObserver(carRoadLabelRenderer)
        obuLiteCarContext.mapboxCarMap.unregisterObserver(carMapViewLayer)
        obuLiteCarContext.mapboxCarMap.unregisterObserver(carRoadObjectRenderer)

        SpeedLimitUtil.removeLocationObserver(MapboxNavigationApp.current())
    }


    private fun updateActionStrip() {
        obuLiteStrip = OBULiteActionStrip(mainCarContext).builder(
            this.isConnected,
            this.currentOBUBalance
        ).build()

        obuLiteMapStrip = ActionStrip.Builder()
            .addAction(
                createMuteAction()
            )
            .build()
    }

    @SuppressLint("UnsafeOptInUsageError")
    private fun createMuteAction(): Action {
        return Action.Builder()
            .setFlags(Action.FLAG_IS_PERSISTENT)
            .setClickSafe {
                LimitClick.handleSafe {
                    onUpdateVoiceState()
                }
            }
            .setIcon(
                CarIcon.Builder(
                    IconCompat.createWithResource(
                        carContext,
                        if (VoiceInstructionsManager.getInstance()?.isUserMute == true)
                            R.drawable.mapbox_car_ic_volume_off
                        else
                            R.drawable.mapbox_car_ic_volume_on
                    )
                ).build()
            )
            .build()
    }

    private fun listenOBUEvents() {
        obuEventsCompositeDisposable.add(
            RxBus.listen(RxEvent.OBURoadEvent::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    obuNotificationHelper.showRoadEventNotification(
                        it.eventData,
                        carRoadLabelRenderer.getCurrentRoadName(),
                        SpeedLimitUtil.currentLocation?.latitude ?: 0.0,
                        SpeedLimitUtil.currentLocation?.longitude ?: 0.0,
                        Screen.AA_OBU_LITE_SCREEN
                    )
                    if (it.eventData.shouldCheckLowCardBalance()) {
                        delayCheckShowingLowCardBalance()
                    }
                }
        )
        obuEventsCompositeDisposable.add(
            RxBus.listen(RxEvent.OBULowCardBalanceEvent::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    showLowCardBalanceNoti(it.balance / 100.0)
                }
        )
        obuEventsCompositeDisposable.add(
            RxBus.listen(RxEvent.OBUParkingAvailabilityEvent::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    obuNotificationHelper.showParkingAvailabilityNotification(
                        it.data,
                        Screen.AA_OBU_LITE_SCREEN,
                        carRoadLabelRenderer.getCurrentRoadName(),
                        SpeedLimitUtil.currentLocation
                    )
                }
        )
        obuEventsCompositeDisposable.add(
            RxBus.listen(RxEvent.OBUTravelTimeEvent::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    obuNotificationHelper.showTravelTimeEventNotification(
                        it.data,
                        Screen.AA_OBU_LITE_SCREEN,
                        carRoadLabelRenderer.getCurrentRoadName(),
                        SpeedLimitUtil.currentLocation
                    )
                }
        )
    }

    private fun delayCheckShowingLowCardBalance() {
        lifecycleScope.launch(Dispatchers.IO) {
            delay(OBUConstants.OBU_LOW_CARD_ALERT_DELAY)
            OBUDataHelper.getOBUCardBalanceSGD()
                ?.takeIf { isActive && it < OBUConstants.OBU_LOW_CARD_THRESHOLD / 100.0 }
                ?.let {
                    showLowCardBalanceNoti(it)
                }
        }
    }

    private fun showLowCardBalanceNoti(balanceSGD: Double) {
        if (lifecycle.currentState.isAtLeast(Lifecycle.State.RESUMED)) {
            lifecycleScope.launch(Dispatchers.Main) {
                obuNotificationHelper.showLowCardNotification(
                    cardBalanceSGD = balanceSGD,
                    screenName = Screen.AA_OBU_LITE_SCREEN
                )
            }
        }
    }

    private fun clearOBUEventListener() {
        obuEventsCompositeDisposable.clear()
    }

    override fun changeOBUConnectionState(connected: Boolean) {
        if (connected != this.isConnected) {
            this.isConnected = connected
            this.currentOBUBalance = OBUDataHelper.getOBUCardBalanceSGD()
            invalidate()
        }
    }

    override fun updateCardBalance(cardBalance: Double?) {
        if (cardBalance != currentOBUBalance) {
            currentOBUBalance = cardBalance
            invalidate()
        }
    }
}

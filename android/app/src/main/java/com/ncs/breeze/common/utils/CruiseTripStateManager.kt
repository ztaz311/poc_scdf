package com.ncs.breeze.common.utils

import android.content.Context
import com.breeze.model.TripType
import com.breeze.model.constants.Constants
import com.ncs.breeze.common.model.rx.AppToHomeTurnOnCruiseModeEvent
import com.ncs.breeze.common.model.rx.AppToPhoneTurnOnCruiseModeEvent
import com.mapbox.android.core.location.LocationEngineResult
import com.mapbox.navigation.core.lifecycle.MapboxNavigationApp
import com.mapbox.navigation.core.trip.session.TripSessionState
import com.ncs.breeze.common.analytics.Screen
import com.ncs.breeze.common.event.ToAppRx
import com.ncs.breeze.common.event.ToCarRx
import io.reactivex.disposables.CompositeDisposable
import timber.log.Timber
import java.lang.ref.WeakReference

class CruiseTripStateManager private constructor(context: Context) {

    private val contextRef = WeakReference(context)
    private var etaEnabled: Boolean = false
    private var shareLiveLocation: String? = null
    private var compositeDisposable: CompositeDisposable? = null
    private var isCruiseModeTriggered: Boolean = false
    private var cruiseSLCrossedTS: Long? = null;
    private var waitTS: Long = System.currentTimeMillis()

    private val callbackLocationBreezeManager =
        object : LocationBreezeManager.CallBackLocationBreezeManager {

            private var lastCruiseStopSpeedRecordedTime: Long = -1L
            override fun onSuccess(result: LocationEngineResult) {
                super.onSuccess(result)
                //wait for CRUISE_START_THRESHOLD_TIME before staring speed checks
                if ((System.currentTimeMillis() - waitTS) < CRUISE_START_THRESHOLD_TIME) {
                    cruiseSLCrossedTS = null
                    return
                }
                val mapboxNavigation = MapboxNavigationApp.current()
                var speedKmPH: Double = 0.0
                val currentLocation = result.lastLocation
                var totalActCruiseSpeedMs: Long = 0
                if (currentLocation != null && currentLocation.hasSpeed()) {
                    val speed = currentLocation.speed
                    speedKmPH = speed * 3.6
                }

                if (speedKmPH > Constants.CRUISE_MODE_TRIGGER_SPEED) {
                    if (contextRef.get() == null || !Utils.checkLocationPermission(contextRef.get()!!)) {
                        return
                    }
                    if (cruiseSLCrossedTS == null) {
                        cruiseSLCrossedTS = System.currentTimeMillis()
                    } else if (!isCruiseModeTriggered) {
                        totalActCruiseSpeedMs = System.currentTimeMillis() - cruiseSLCrossedTS!!
                    }

                    if (!isCruiseModeTriggered
                        && (mapboxNavigation != null && mapboxNavigation.getTripSessionState() != TripSessionState.STARTED)
                        && !isDisableCruiseMode
                        && (totalActCruiseSpeedMs >= CRUISE_TRIGGER_THRESHOLD_TIME)
                    ) {
                        Timber.i("Cruise mode triggered after waiting for : $totalActCruiseSpeedMs, and exceeding speed: $speedKmPH")
                        isCruiseModeTriggered = true
                        ToAppRx.postEvent(AppToPhoneTurnOnCruiseModeEvent())
                        ToCarRx.postEventWithCacheLast(
                            AppToHomeTurnOnCruiseModeEvent(
                                etaEnabled,
                                shareLiveLocation == Constants.TRIPETA.YES,
                            )
                        )
                    }
                } else {
                    cruiseSLCrossedTS = null
                }

                if (Utils.isNavigationStarted) {
                    return
                }

                if (mapboxNavigation?.getTripSessionState() == TripSessionState.STARTED) {
                    if (speedKmPH > Constants.CRUISE_MODE_TRIGGER_SPEED) {
                        TripLoggingManager.getInstance()
                            ?.startTripLogging(TripType.CRUISE, Screen.HOME_CRUISE_MODE, null)
                        lastCruiseStopSpeedRecordedTime = -1L
                    } else if (speedKmPH < Constants.CRUISE_MODE_STOP_SPEED) {
                        if (lastCruiseStopSpeedRecordedTime == -1L) {
                            lastCruiseStopSpeedRecordedTime = System.currentTimeMillis()
                        } else {
                            var currentTime = System.currentTimeMillis()
                            if ((currentTime - lastCruiseStopSpeedRecordedTime) > Constants.CRUISE_WAIT_TIME) {
                                lastCruiseStopSpeedRecordedTime = -1L
                                TripLoggingManager.getInstance()
                                    ?.stopTripLogging(null, Screen.HOME_CRUISE_MODE)
                            }
                        }
                    } else {
                        lastCruiseStopSpeedRecordedTime = -1L
                    }
                }
            }
        }


    companion object {

        private var isDisableCruiseMode = false
        private const val CRUISE_TRIGGER_THRESHOLD_TIME = 10000
        private const val CRUISE_START_THRESHOLD_TIME = 30000
        private const val CRUISE_RESUME_THRESHOLD_TIME = 25000
        private var mInstance: CruiseTripStateManager? = null
        private var isDestroyed = false;

        fun getInstance(): CruiseTripStateManager? {
            return mInstance
        }

        fun create(context: Context) {
            if (!isDestroyed && (mInstance == null)) {
                mInstance = CruiseTripStateManager(context).apply {
                    LocationBreezeManager.getInstance()
                        .registerLocationCallback(callbackLocationBreezeManager)
                }
            }
        }

        fun disableCruiseTrigger() {
            isDisableCruiseMode = true
        }

        fun enableCruiseTrigger() {
            isDisableCruiseMode = false
            isDestroyed = false
        }

        fun resetCruiseWaitTime() {
            mInstance?.waitTS = System.currentTimeMillis() - CRUISE_RESUME_THRESHOLD_TIME
        }

        fun destroy(manualDestroy: Boolean = false) {
            mInstance?.let {
                LocationBreezeManager.getInstance()
                    .removeLocationCallBack(it.callbackLocationBreezeManager)
                it.compositeDisposable?.dispose()
                mInstance = null
            }
            if (manualDestroy) {
                isDestroyed = true
            }
        }
    }
}
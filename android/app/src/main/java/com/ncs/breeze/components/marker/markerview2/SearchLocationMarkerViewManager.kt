package com.ncs.breeze.components.marker.markerview2

import com.mapbox.maps.MapView


/**
 * this class for manager all marker filter on dashboard activity
 */
class SearchLocationMarkerViewManager(
    private val mapView: MapView,
) : MarkerViewManager2(mapView) {


    fun removeAllMarkers() {
        markers.forEach {
            mapView.removeView(it.mViewMarker)
        }
        markers.clear()
    }


    override fun hideAllMarker() {
        for (markerView in markers) {
            markerView.hideMarker()
        }
    }

    override fun showAllMarker() {
        for (markerView in markers) {
            markerView.showMarker()
        }
    }

}
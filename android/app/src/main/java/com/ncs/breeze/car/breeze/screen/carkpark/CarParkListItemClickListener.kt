package com.ncs.breeze.car.breeze.screen.carkpark

import com.breeze.model.api.response.amenities.BaseAmenity

interface CarParkListItemClickListener {
    fun onItemClick(carParkItem: BaseAmenity)
}
package com.ncs.breeze.common.utils

import com.breeze.model.api.ErrorData
import com.ncs.breeze.common.remote.ApiHelper
import com.ncs.breeze.common.remote.ApiObserver
import com.breeze.model.enums.TrafficIncidentData
import com.ncs.breeze.common.rxbus.RxBus
import com.ncs.breeze.common.rxbus.RxEvent
import com.breeze.model.extensions.safeDouble
import com.google.gson.Gson
import com.google.gson.JsonElement
import com.google.gson.internal.Streams
import com.google.gson.stream.JsonReader
import com.mapbox.geojson.Feature
import com.mapbox.geojson.FeatureCollection
import com.mapbox.geojson.Point
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import timber.log.Timber
import java.io.StringReader
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class BreezeTrafficIncidentsUtil @Inject constructor(apiHelper: ApiHelper) {

    var apiHelper: ApiHelper = apiHelper
    var compositeDisposable: CompositeDisposable = CompositeDisposable()

    var latestIncidentFeatures: HashMap<String, FeatureCollection> = hashMapOf()
    var trafficIncidentsDetectionEnabled: Boolean = false;

    fun startIncidentHandler() {
        synchronized(this) {
            if (!trafficIncidentsDetectionEnabled) {
                trafficIncidentsDetectionEnabled = true
                subscribeToAppSyncRxEvent()
                retrieveIncidentData()
            }
        }
    }

    fun subscribeToAppSyncRxEvent() {

        compositeDisposable.add(
            RxBus.listen(RxEvent.IncidentAppSyncEvent::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError {
                    Timber.d("Observing on error")
                }
                .subscribe {
                    Timber.d("Incident event observed successfully")

                    try {
                        var jsonElement =
                            Streams.parse(JsonReader(StringReader(it.incidentDataUpdated)))
                        createFeaturesFromData(jsonElement)
                    } catch (e: Exception) {
                        Timber.e(e, "Error incorrect json")
                    }
                }
        )
    }

    private fun retrieveIncidentData() {
        apiHelper.getTrafficIncidents()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : ApiObserver<JsonElement>(compositeDisposable) {
                override fun onSuccess(data: JsonElement) {
                    createFeaturesFromData(data)
                }

                override fun onError(e: ErrorData) {
                    Timber.d("API failed: " + e.toString())
                }

            })
    }


    fun createFeaturesFromData(data: JsonElement) {
        Timber.d("Creating incident features from data")
        var json = Gson().toJson(data)
        var newJsonObject = JSONObject(json)
        var incidentFeatures: HashMap<String, FeatureCollection> = hashMapOf()

        val iter: Iterator<String> = newJsonObject.keys()
        while (iter.hasNext()) {
            var features: ArrayList<Feature> = arrayListOf()
            val key = iter.next()
            try {
                val value: Any = newJsonObject.get(key)
                var incidentType = TrafficIncidentData.getIncidentType(key)

                if (incidentType != null && value is JSONArray) {
                    var array = value as JSONArray
                    for (item in array) {
                        if (item is JSONObject) {
                            var obj = item as JSONObject
                            if (obj.has("Latitude") && obj.has("Longitude")) {
                                var feature = Feature.fromGeometry(
                                    Point.fromLngLat(
                                        obj.getString("Longitude").safeDouble(),
                                        obj.getString("Latitude").safeDouble()
                                    )
                                )
                                features.add(feature)
                            }
                        }
                    }
                    incidentFeatures.put(
                        incidentType.type,
                        FeatureCollection.fromFeatures(features)
                    )
                }
            } catch (e: JSONException) {
                // Something went wrong!
                Timber.e(e)
            }
        }
        latestIncidentFeatures = incidentFeatures
        Timber.d("Triggering incidents refresh")
        RxBus.publish(RxEvent.IncidentDataRefresh(latestIncidentFeatures))

    }

    fun getLatestIncidentData(): HashMap<String, FeatureCollection> {
        return latestIncidentFeatures
    }

    fun stopIncidentHandler() {
        Timber.d("Stop incident handler called")
        if (!compositeDisposable.isDisposed) {
            compositeDisposable.clear()
        }
        trafficIncidentsDetectionEnabled = false
    }

    operator fun JSONArray.iterator(): Iterator<JSONObject> =
        (0 until length()).asSequence().map { get(it) as JSONObject }.iterator()


}
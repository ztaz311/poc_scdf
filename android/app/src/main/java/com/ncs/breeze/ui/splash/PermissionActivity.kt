package com.ncs.breeze.ui.splash

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.lifecycle.ViewModelProvider
import com.breeze.model.constants.Constants
import com.ncs.breeze.common.utils.Utils
import com.ncs.breeze.databinding.ActivityPermissionBinding
import com.ncs.breeze.ui.base.BaseActivity
import javax.inject.Inject


class PermissionActivity : BaseActivity<ActivityPermissionBinding, PermissionViewModel>() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val permissionViewModel: PermissionViewModel by viewModels {
        viewModelFactory
    }

    private val startForResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
            if (Utils.checkLocationPermission(this)) {
                finish()
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewBinding.openPermission.setOnClickListener {
            val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
            val uri: Uri = Uri.fromParts("package", packageName, null)
            intent.data = uri
            startForResult.launch(intent)
        }
    }

    override fun inflateViewBinding() = ActivityPermissionBinding.inflate(layoutInflater)

    override fun getViewModelReference(): PermissionViewModel {
        return permissionViewModel
    }

    override fun getFragmentContainer(): Int? {
        return null
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        when (requestCode) {
            Constants.REQUEST_CODES.PERMISSION_ACTIVITY_REQUEST -> {
                if (Utils.checkLocationPermission(this)) {
                    finish()
                }
            }
        }

        super.onActivityResult(requestCode, resultCode, data)
    }
}
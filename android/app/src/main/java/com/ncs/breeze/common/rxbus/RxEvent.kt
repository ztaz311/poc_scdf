package com.ncs.breeze.common.rxbus

import android.os.Bundle
import com.breeze.model.DestinationAddressDetails
import com.breeze.model.ERPFeatures
import com.breeze.model.NavigationZone
import com.breeze.model.api.request.ChargingInfo
import com.breeze.model.api.request.OBUTransaction
import com.breeze.model.api.request.SaveOBUTripSummaryRequest
import com.breeze.model.api.request.TotalTravelCharge
import com.breeze.model.api.response.notification.GlobalNotificationData
import com.breeze.model.enums.AndroidScreen
import com.breeze.model.firebase.NavigationLogData
import com.breeze.model.obu.OBUCardStatusEventData
import com.breeze.model.obu.OBUParkingAvailability
import com.breeze.model.obu.OBURoadEventData
import com.breeze.model.obu.OBUTravelTimeData
import com.facebook.react.bridge.ReadableMap
import com.mapbox.geojson.FeatureCollection
import com.mapbox.geojson.Point
import java.util.concurrent.ConcurrentHashMap

class RxEvent {
    data class NoInternetConnection(var isConnected: Boolean)
    data class CruiseMode(var monitorCruiseMode: Boolean)
    data class ERPRefresh(var erpFeature: ERPFeatures?)
    data class IncidentDataRefresh(var incidentData: HashMap<String, FeatureCollection>)
    data class GlobalNotificationDataRefresh(var notificationData: GlobalNotificationData)
    class GlobalNotificationSkipped()
    data class UpdateCalendarEvent(var calendarUpdate: Boolean)
    data class NotifyCalendarEventDestinationChange(var destinationDestails: DestinationAddressDetails)
    data class IncidentAppSyncEvent(var incidentDataUpdated: String?)
    data class GlobalNotificationSyncEvent(var globalNotificationDataUpdated: String?)
    data class MiscROAppSyncEvent(var miscObjectData: String?)
    data class MiscRODataRefresh(var miscObjectData: ConcurrentHashMap<NavigationZone, FeatureCollection>)
    data class ERPAppSyncEvent(var erpDataUpdated: String?)
    data class SilverZoneAppSyncEvent(var silverZoneDataUpdated: String?)
    data class SchoolZoneAppSyncEvent(var schoolZoneUpdated: String?)
    data class ERPDataChange(var dataUpdated: Boolean)
    data class SchoolSilverZoneDataChange(var dataUpdated: Boolean)
    data class ETAMessage(var message: String)
    data class ETARNDataCallback(var data: HashMap<String, Any>)
    class CloseNotifyArrivalScreen()
    class VoucherAddedSyncEvent()
    data class AddDashboardFragment(
        var toScreen: AndroidScreen,
        var readableMap: HashMap<String, Any?>
    )

    data class OpenErpDetail(var erpID: Int)

    //data class ThemeChangeEvent(var isThemeChanged: Boolean)
    data class DateRangeSelected(var startDate: String, var endDate: String)
    data class PlannedTripItemsUpdated(var itemCount: Int)
    data class IsUpComingTripsFeatureUsed(var status: Boolean)
    data class IsTripHistoryFeatureUsed(var status: Boolean)
    class IsTripObuTransactionFeatureUsed(var isResultEmpty: Boolean)
    data class SpeedChanged(var value: Int)
    data class RequestToFindWalking(val startingPoint: Point, val endingPoint: Point)
    data class RequestToUpdateCurrentDestination(val currentDestinationDetails: DestinationAddressDetails)
    class LogoutRequested()
    class LogoutGoCreateScreen()
    data class EventOpenCalculateFee(var idCarPark: String)
    data class EventOpenCalculateFeeLandingPage(var idCarPark: String)
    data class EventOpenSeeMore(var idCarPark: String)

    //    obu
    data class OBUVelocityEvent(val velocityMS: Double, val speedLimit: Double? = null)
    data class OBUCardInfoEvent(val data: OBUCardStatusEventData)
    data class OBUParkingAvailabilityEvent(val data: Array<OBUParkingAvailability>)
    data class OBURoadEvent(val eventData: OBURoadEventData)
    data class OBUTravelTimeEvent(val data: Array<OBUTravelTimeData>)
    data class OBUCarParkSelectedEvent(val carPark: ReadableMap)
    data class OBUPaymentHistoriesEvent(val paymentHistories: List<OBUTransaction>)

    /**
     * @param balance: OBU card balance in cents
     * */
    data class OBULowCardBalanceEvent(val balance: Int)

    data class OBUTravelSummaryEvent(val totalTravelTime: Int, val totalTravelDistance: Int)
    data class OBUTotalTripChargedEvent(val totalTravelCharge: TotalTravelCharge)
    data class OBUTotalTripSummaryEvent(val totalTripSummary: SaveOBUTripSummaryRequest)
    class CancelOBUTravelTimeEvent()
    data class OBUChargeInfoEvent(val data: List<ChargingInfo>)

    data class OBUDisplayToggleMapEvent(val isShowingMap: Boolean)

    data class NavigationLogUploadEvent(val data: NavigationLogData)

    class CloseCarparkListScreen
    class OpenCarparkListScreen
    class NoCarparkAlert
    class CloseLandingDropPinScreen
    class LandingDropPinSaved(val pinLocationData: ReadableMap)
    class CollectionDetailsSearchLocation(val searchLocation: ReadableMap)
    class CollectionDetailsCloseShared
    class CollectionDetailsClearSearch

    class SharedCollectionDataRefresh(val locationMapData: ReadableMap)
    class DidSaveSharedLocation(val bookmarkId: Int)

    class RoutePlanningOpenInvite(val data: Bundle)
    class RoutePlanningCloseInvite
}
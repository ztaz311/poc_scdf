package com.ncs.breeze.ui.login.fragment.signUp.viewmodel

import android.app.Application
import com.amplifyframework.core.Amplify
import com.ncs.breeze.common.extensions.android.getApp
import com.ncs.breeze.common.utils.AWSUtils
import com.ncs.breeze.common.utils.Utils
import com.ncs.breeze.components.AmplifyErrorHandler
import com.ncs.breeze.components.SingleLiveEvent
import com.ncs.breeze.ui.base.BaseFragmentViewModel
import timber.log.Timber
import javax.inject.Inject


class VerifyOTPFragmentViewModel @Inject constructor(application: Application) :
    BaseFragmentViewModel(
        application
    ) {

    var confirmSignUpSuccessful: SingleLiveEvent<Boolean> = SingleLiveEvent();
    var confirmSignUpFailed: SingleLiveEvent<AmplifyErrorHandler.AuthObject> = SingleLiveEvent();

    var resendSuccessful: SingleLiveEvent<Boolean> = SingleLiveEvent()
    var resendFailed: SingleLiveEvent<AmplifyErrorHandler.AuthObject> = SingleLiveEvent()

    fun confirmSignUp(email: String, otpText: String) {
        AWSUtils.confirmSignUp(
            email,
            otpText,
            { result ->
                Utils.runOnMainThread {
                    if (result.isSignUpComplete) {
                        confirmSignUpSuccessful.value = true
                    } else {
                        // Show generic error message
                    }
                }
            },
            { error ->
                Timber.e("AuthQuickstart ${error.toString()}")
                Utils.runOnMainThread {
                    confirmSignUpFailed.value = AmplifyErrorHandler.getErrorMessage(
                        error,
                        getApp(),
                        AmplifyErrorHandler.AuthType.CONFIRM_SIGNUP
                    )
                }
            }
        )
    }

    fun resendOTP(email: String) {
        AWSUtils.resendSignUpCode(
            email,
            { result ->
                resendSuccessful.value = true
                Timber.i("AuthQuickstart ${result.toString()}")
            },
            { error ->
                Utils.runOnMainThread {
                    resendFailed.value = AmplifyErrorHandler.getErrorMessage(
                        error,
                        getApp(),
                        AmplifyErrorHandler.AuthType.SIGN_UP
                    )
                }
                Timber.e("AuthQuickstart ${error.toString()}")
            }
        )
    }
}
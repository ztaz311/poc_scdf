package com.ncs.breeze.car.breeze.screen.routeplanning

import android.location.Location
import androidx.car.app.OnScreenResultListener
import androidx.car.app.ScreenManager
import androidx.car.app.model.Action
import androidx.car.app.model.ActionStrip
import androidx.car.app.model.CarColor
import androidx.car.app.model.CarIcon
import androidx.core.graphics.drawable.IconCompat
import com.ncs.breeze.common.model.rx.DataForRoutePreview
import com.breeze.model.enums.ETAMode
import com.ncs.breeze.car.breeze.MainBreezeCarContext
import com.ncs.breeze.car.breeze.base.BaseScreenCar
import com.ncs.breeze.car.breeze.screen.carkpark.CarParkCarContext
import com.ncs.breeze.car.breeze.screen.carkpark.CarParkScreen
import com.ncs.breeze.car.breeze.screen.search.etaList.ETAListScreen
import com.ncs.breeze.car.breeze.utils.LimitClick
import com.ncs.breeze.car.breeze.utils.setClickSafe
import com.ncs.breeze.R
import com.mapbox.geojson.Point
import com.ncs.breeze.common.analytics.Screen
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class PreviewRouteActionStrip(
    private val mainCarContext: MainBreezeCarContext,
    private val screen: BaseScreenCar,
//    private val onScreenResultListener: OnScreenResultListener,
//    private val routeDetailHandle: RoutePlanningScreen.RouteDetailHandler,
    val data: DataForRoutePreview,
//    private val isShareDriveDisabled: Boolean
) {
    private val carContext = mainCarContext.carContext

    /**
     * Build the action strip
     */
    fun builder(
        isParkingEnabled: Boolean,
        destination: Point
    ): ActionStrip.Builder {

        val templateBuilder = ActionStrip.Builder()

        templateBuilder.addAction(buildCarPackAction(isParkingEnabled, destination))
//        templateBuilder.addAction(buildNotyArrivalAction())
        return templateBuilder
    }

//    private fun buildNotyArrivalAction(): Action {
//        val actionBuilder = Action.Builder()
//
//        if (isShareDriveDisabled) {
//            actionBuilder.setIcon(
//                CarIcon.Builder(
//                    IconCompat.createWithResource(
//                        carContext,
//                        R.drawable.ic_eta_start_no_bg,
//                    )
//                ).build()
//            )
//            actionBuilder.setClickSafe {
//                LimitClick.handleSafe {
//                    carContext
//                        .getCarService(ScreenManager::class.java)
//                        .push(
//                            ETAListScreen(
//                                mainCarContext,
//                                ETAMode.RoutePlanning,
//                                routeDetailHandle.getSelectedRoute(),
//                                onScreenResultListener
//                            )
//                        )
//                }
//            }
//        } else {
//            actionBuilder.setIcon(
//                CarIcon.Builder(
//                    IconCompat.createWithResource(
//                        carContext,
//                        R.drawable.ic_eta_paused_no_bg,
//                    )
//                ).build()
//            )
//            actionBuilder.setClickSafe {
//                LimitClick.handleSafe {
//                    onScreenResultListener.onScreenResult(null)
//                }
//            }
//        }
//        return actionBuilder.build()
//    }

    private fun buildCarPackAction(enabled: Boolean, destination: Point): Action {
        val carIconBuilder = CarIcon.Builder(
            IconCompat.createWithResource(
                carContext, R.drawable.ic_icon_car_pack
            )
        )
        if (!enabled) {
            carIconBuilder.setTint(
                CarColor.createCustom(
                    carContext.getColor(R.color.color_tint_parking),
                    carContext.getColor(R.color.color_tint_parking)
                )
            )
        }
        val location = Location("")
        location.latitude = destination.latitude()
        location.longitude = destination.longitude()
        return Action.Builder()
            .setIcon(carIconBuilder.build())
            .setClickSafe {
                LimitClick.handleSafe {
                    CoroutineScope(Dispatchers.Main).launch {
                        screen.screenManager.push(
                            CarParkScreen(
                                CarParkCarContext(mainCarContext),
                                null,
                                location,
                                true,
                                originalDestination = data.originalDestination,
                                fromScreen = Screen.ROUTE_PLANNING
                            )
                        )
                    }
                }
            }
            .build()
    }


}

package com.ncs.breeze.common.extensions.android

import android.app.Activity
import com.ncs.breeze.App

fun Activity.getApp() = this.application as? App
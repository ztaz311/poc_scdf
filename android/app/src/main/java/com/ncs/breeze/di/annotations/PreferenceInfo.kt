package com.kar.enes.app.di.annotations

import javax.inject.Qualifier

/**
 * Created by M.Enes on 5/9/2019
 */
@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class PreferenceInfo {
}
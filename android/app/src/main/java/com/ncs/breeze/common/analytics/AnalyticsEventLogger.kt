package com.ncs.breeze.common.analytics

import android.os.Bundle

/**
 * Created by Aiswarya on 16,June,2021
 */
interface AnalyticsEventLogger {

    fun logEvent(name: String, payload: Bundle)

    fun setUserId(cognitoUserName: String)

}
package com.ncs.breeze.components.map.profile

import com.mapbox.maps.EdgeInsets
import com.breeze.model.extensions.dpToPx

abstract class BaseMapCamera {

    /**
     * TODO: rename this variable as per actual usage
     * Used to store the Edge Insets adjusted by the bottom-sheet height (in all positions)
     * The padding does not regard tooltips (e.g carpark tooltip height)
     */
    var maxEdgeInsets: EdgeInsets = EdgeInsets(
        10.0.dpToPx(),
        20.0.dpToPx(),
        300.0.dpToPx(),
        20.0.dpToPx()
    )

    /**
     * TODO: rename this variable as per actual usage
     * Used to store the Edge Insets adjusted by the bottom-sheet height (in all positions)
     * The padding should regard adjustments due to tooltips
     */
    var overviewEdgeInsets: EdgeInsets = maxEdgeInsets

    /**
     * Used to store the Edge Insets adjusted by the bottom-sheet height (in all positions)
     * Should be used for any zone layers in content/explore view
     */
    var edgeCircleInsets: EdgeInsets = EdgeInsets(
        25.0.dpToPx(),
        20.0.dpToPx(),
        300.0.dpToPx(),
        20.0.dpToPx()
    )
}
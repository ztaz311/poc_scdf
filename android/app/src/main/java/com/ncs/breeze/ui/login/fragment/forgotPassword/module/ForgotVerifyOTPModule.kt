package com.ncs.breeze.ui.login.fragment.forgotPassword.module

import com.ncs.breeze.ui.login.fragment.forgotPassword.view.ForgotVerifyOTPFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class ForgotVerifyOTPModule {
    @ContributesAndroidInjector
    abstract fun contributeForgotVerifyOTPFragment(): ForgotVerifyOTPFragment
}
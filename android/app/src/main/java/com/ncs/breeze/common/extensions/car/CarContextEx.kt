package com.ncs.breeze.common.extensions.car

import android.graphics.Bitmap
import androidx.annotation.DrawableRes
import androidx.car.app.CarContext
import androidx.car.app.model.CarIcon
import androidx.core.graphics.drawable.IconCompat
import com.breeze.model.extensions.toBitmap
import com.ncs.breeze.R

fun CarContext.toBitmap(@DrawableRes res: Int): Bitmap? = res.toBitmap(this)

fun CarContext.createCarIcon(@DrawableRes res: Int) = CarIcon.Builder(
    IconCompat.createWithResource(this, res)
).build()
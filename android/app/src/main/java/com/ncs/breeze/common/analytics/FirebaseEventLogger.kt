package com.ncs.breeze.common.analytics

import android.os.Bundle
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase
import java.lang.Integer.min

/**
 * Created by Aiswarya on 16,June,2021
 */
class FirebaseEventLogger : AnalyticsEventLogger {

    override fun logEvent(name: String, payload: Bundle) {
        Firebase.analytics.logEvent(
            name.substring(0, min(name.length, MAX_EVENT_NAME_LENGTH)),
            payload
        )
    }

    override fun setUserId(cognitoUserName: String) {
        Firebase.analytics.setUserId(cognitoUserName)
    }

    companion object {
        const val MAX_EVENT_NAME_LENGTH = 40
    }
}
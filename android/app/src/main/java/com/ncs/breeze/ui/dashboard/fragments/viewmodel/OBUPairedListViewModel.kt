package com.ncs.breeze.ui.dashboard.fragments.viewmodel

import android.app.Application
import com.breeze.model.api.ErrorData
import com.breeze.model.api.request.ObuVehicleFilter
import com.breeze.model.api.response.ObuVehicleResponse
import com.ncs.breeze.common.remote.ApiHelper
import com.ncs.breeze.common.remote.ApiObserver
import com.ncs.breeze.components.SingleLiveEvent
import com.ncs.breeze.ui.base.BaseFragmentViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class OBUPairedListViewModel @Inject constructor(apiHelper: ApiHelper, application: Application) :
    BaseFragmentViewModel(
        application
    ) {

    val apiHelper: ApiHelper = apiHelper
    var vehicleDetailsResponseEvent: SingleLiveEvent<ObuVehicleResponse?> = SingleLiveEvent()

    fun getObuVehicleList(
        vehicleNumbers: List<String>,
    ) {
        val requestData = ObuVehicleFilter()
        requestData.vehicleNumbers = vehicleNumbers
        apiHelper.getFilterVehicleDetails(requestData)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : ApiObserver<ObuVehicleResponse>(compositeDisposable) {
                override fun onSuccess(data: ObuVehicleResponse) {
                    vehicleDetailsResponseEvent.postValue(data)
                }

                override fun onError(e: ErrorData) {
                    vehicleDetailsResponseEvent.postValue(null)
                    //
                }
            })
    }

}

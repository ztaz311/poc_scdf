package com.ncs.breeze.ui.dashboard.fragments.obuconnect

import android.os.Bundle
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.text.toSpannable
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import com.ncs.breeze.R
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.Event
import com.ncs.breeze.common.analytics.Screen
import com.ncs.breeze.common.extensions.android.getApp
import com.ncs.breeze.common.extensions.android.getOBUConnHelper
import com.ncs.breeze.databinding.FragmentObuConnectThirdStateBinding

class OBUConnectThirdStateFragment : Fragment() {

    private lateinit var viewBinding: FragmentObuConnectThirdStateBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewBinding = FragmentObuConnectThirdStateBinding.inflate(inflater, container, false)
        return viewBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewBinding.tvDescription.text = createDescriptionText()
        initClickListener()
    }

    fun toggleConnectingIndicatorVisibility(isShowing: Boolean) {
        viewBinding.progressPairing.isVisible = isShowing
        viewBinding.btCancelPair.isVisible = isShowing
    }

    private fun initClickListener() {
        viewBinding.btCancelPair.setOnClickListener {
            Analytics.logClickEvent(
                Event.OBU_PAIRING_PAIRING_CANCEL,
                getScreenName()
            )
            getOBUConnHelper()?.disconnectOBU()
            closeOBUConnectionScreen()
        }
    }

    private fun closeOBUConnectionScreen() {
        (parentFragment as? OBUConnectFragment)?.onBackPressed()
        activity?.getApp()?.shareBusinessLogicHelper?.backToHomepage()
    }

    private fun createDescriptionText(): Spannable {
        val highlight = SpannableStringBuilder("do not close Breeze App")
        highlight.setSpan(
            ForegroundColorSpan(
                ContextCompat.getColor(
                    requireContext(),
                    R.color.passion_pink
                )
            ), 0, highlight.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )

        return SpannableStringBuilder()
            .append("LTA will take 1-2 mins to verify and pair your OBU with Breeze.\n\nPlease ")
            .append(highlight)
            .append(". You will be notified once the pairing is complete.")
            .toSpannable()
    }

    private fun getScreenName() = Screen.OBU_PAIRING_SCREEN
}
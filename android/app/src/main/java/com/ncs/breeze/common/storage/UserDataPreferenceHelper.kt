package com.ncs.breeze.common.storage

import android.content.Context
import android.content.SharedPreferences
import androidx.core.content.edit
import aws.smithy.kotlin.runtime.util.push
import com.breeze.model.api.response.AmenitiesPreference
import com.breeze.model.constants.AmenityType
import com.breeze.model.constants.Constants
import com.breeze.model.obu.OBUDevice
import com.google.gson.Gson
import com.ncs.breeze.common.helper.obu.OBUConnectionHelper

class UserDataPreferenceHelper(context: Context, userId: String) {
    private val sharedPreference: SharedPreferences = context.getSharedPreferences(
        "${PREFERENCE_NAME_PREFIX}$userId", Context.MODE_PRIVATE
    )

    fun shouldShowOBUIntro() =
        sharedPreference.getBoolean(KEY_SHOW_OBU_INTRO, true)

    fun saveShowOBUIntroState(shouldShow: Boolean) {
        sharedPreference.edit()
            .putBoolean(KEY_SHOW_OBU_INTRO, shouldShow).commit()
    }

    fun saveConnectedOBUDevice(obuDevice: OBUDevice) {
        arrayListOf<OBUDevice>().apply {
            addAll(getConnectedOBUDevices())
            removeIf { currentDevice -> currentDevice == obuDevice }
            add(obuDevice)
        }.let {
            sharedPreference.edit().putString(KEY_STORED_OBU_DEVICES, Gson().toJson(it)).commit()
        }
    }

    fun saveConnectedOBUDevice(obuDevices: List<OBUDevice>) {
        sharedPreference.edit().putString(KEY_STORED_OBU_DEVICES, Gson().toJson(obuDevices))
            .commit()
    }

    fun getConnectedOBUDevices(): Array<OBUDevice> {
        val dataJson =
            sharedPreference.getString(KEY_STORED_OBU_DEVICES, "")
        kotlin.runCatching {
            return Gson().fromJson(dataJson, Array<OBUDevice>::class.java)
        }
        return arrayOf()
    }

    fun getLastConnectedOBUDevice(): OBUDevice? =
        getConnectedOBUDevices().takeIf { it.isNotEmpty() }?.last()

    fun isEnableOBUAutoConnect() = sharedPreference.getBoolean(KEY_OBU_AUTO_CONNECT, false)
    fun toggleOBUAutoConnect(isEnableAutoConnect: Boolean) {
        OBUConnectionHelper.isOBUAutoConnect = isEnableAutoConnect
        sharedPreference.edit().putBoolean(
            KEY_OBU_AUTO_CONNECT, isEnableAutoConnect
        ).commit()
    }

    fun getPetrolAmenityFilter(): List<String> {
        return sharedPreference.getString(KEY_PETROL_AMENITY_FILTER, null)?.split(",")?.filter { it.isNotEmpty() }
            ?: emptyList()
    }

    fun savePetrolAmenityFilter(providers: Array<String>, isCommit: Boolean = false) {
        sharedPreference.edit(isCommit) {
            putString(
                KEY_PETROL_AMENITY_FILTER,
                providers.takeIf { it.isNotEmpty() }?.joinToString(",") ?: ""
            )
        }
    }

    fun saveEvPetrolResultCount(count: Int, isCommit: Boolean = false) {
        sharedPreference.edit(isCommit) { putInt(KEY_EV_PETROL_RESULT_COUNT, count) }
    }

    fun getEvPetrolResultCount() =
        sharedPreference.getInt(KEY_EV_PETROL_RESULT_COUNT, Constants.DEFAULT_PETROL_EV_RESULT_COUNT)

    /**
     * @param range : radius to load nearby Ev/petrol in meters
     * */
    fun saveEvPetrolRange(range: Int, isCommit: Boolean = false) {
        sharedPreference.edit(isCommit) { putInt(KEY_EV_PETROL_RANGE, range) }
    }

    /**
     * radius from BE to load nearby Ev/petrol
     * measured in meters
     * */
    fun getEvPetrolRange() =
        sharedPreference.getInt(KEY_EV_PETROL_RANGE, (Constants.DEFAULT_PETROL_EV_RADIUS * 1000).toInt())

    /**
     * @param range : max radius to load nearby Ev/petrol in meters
     * */
    fun saveEvPetrolMaxRange(range: Int, isCommit: Boolean = false) {
        sharedPreference.edit(isCommit) { putInt(KEY_EV_PETROL_MAX_RANGE, range) }
    }

    /**
     * max radius from BE to load nearby Ev/petrol
     * measured in meters
     * */
    fun getEvPetrolMaxRange() =
        sharedPreference.getInt(KEY_EV_PETROL_MAX_RANGE, (Constants.DEFAULT_PETROL_EV_MAX_RADIUS * 1000).toInt())


    fun getEVAmenityFilter(): List<String> {
        return sharedPreference.getString(KEY_EV_AMENITY_FILTER, null)?.split(",")?.filter { it.isNotEmpty() }
            ?: emptyList()
    }

    fun saveEVAmenityFilter(plugTypes: Array<String>, isCommit: Boolean = false) {
        sharedPreference.edit(isCommit) {
            putString(
                KEY_EV_AMENITY_FILTER,
                plugTypes.takeIf { it.isNotEmpty() }?.joinToString(",") ?: ""
            )
        }
    }

    fun saveAmenityFilter(preference: List<AmenitiesPreference>, isCommit: Boolean = false) {
        preference.find { pref -> AmenityType.PETROL == pref.elementName }?.subItems
            ?.filter { it.isSelected == true && !it.elementName.isNullOrEmpty() }
            ?.mapNotNull { it.elementName }?.toTypedArray()
            ?.let { petrolProviders ->
                savePetrolAmenityFilter(petrolProviders, isCommit)
            }

        preference.find { pref -> AmenityType.EVCHARGER == pref.elementName }?.subItems
            ?.filter { it.isSelected == true && !it.elementName.isNullOrEmpty() }
            ?.mapNotNull { it.elementName }?.toTypedArray()
            ?.let { evChargerPlugTypes ->
                saveEVAmenityFilter(evChargerPlugTypes, isCommit)
            }
    }

    companion object {

        private const val PREFERENCE_NAME_PREFIX = "user_data_preference_"

        private const val KEY_SHOW_OBU_INTRO = "KEY_SHOW_OBU_INTRO"
        private const val KEY_STORED_OBU_DEVICES = "STORED_OBU_DEVICES"
        private const val KEY_OBU_AUTO_CONNECT = "KEY_OBU_AUTO_CONNECT"
        private const val KEY_PETROL_AMENITY_FILTER = "KEY_PETROL_AMENITY_FILTER"
        private const val KEY_EV_AMENITY_FILTER = "KEY_EV_AMENITY_FILTER"
        private const val KEY_EV_PETROL_RESULT_COUNT = "KEY_EV_PETROL_RESULT_COUNT"
        private const val KEY_EV_PETROL_RANGE = "KEY_EV_PETROL_RANGE"
        private const val KEY_EV_PETROL_MAX_RANGE = "KEY_EV_PETROL_MAX_RANGE"
    }
}
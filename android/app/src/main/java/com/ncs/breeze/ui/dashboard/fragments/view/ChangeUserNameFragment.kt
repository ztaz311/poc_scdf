package com.ncs.breeze.ui.dashboard.fragments.view

import android.os.Bundle
import android.text.Editable
import android.text.InputFilter
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.TextView.OnEditorActionListener
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import com.ncs.breeze.R
import com.ncs.breeze.car.breeze.utils.BreezeCarUtil
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.Event
import com.ncs.breeze.common.analytics.Screen
import com.ncs.breeze.common.utils.BreezeUserPreferenceUtil
import com.breeze.model.constants.Constants
import com.ncs.breeze.common.utils.Utils
import com.breeze.customization.view.BreezeEditText
import com.ncs.breeze.databinding.FragmentChangeUsernameBinding
import com.ncs.breeze.ui.base.BaseFragment
import com.ncs.breeze.ui.dashboard.activity.DashboardActivity
import com.ncs.breeze.ui.dashboard.fragments.viewmodel.ChangeUserNameViewModel
import javax.inject.Inject


class ChangeUserNameFragment :
    BaseFragment<FragmentChangeUsernameBinding, ChangeUserNameViewModel>() {

    @Inject
    lateinit var breezeUserPreferenceUtil: BreezeUserPreferenceUtil

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val viewModel: ChangeUserNameViewModel by viewModels {
        viewModelFactory
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initialize()
    }

    private fun initialize() {

        viewBinding.backButton.setOnClickListener {
            (activity as DashboardActivity).hideKeyboard()
            onBackPressed()
        }

        viewBinding.rlScrollView.setOnClickListener {
            hideKeyboard()
        }

        viewBinding.rlHeading.setOnClickListener {
            hideKeyboard()
        }

        viewBinding.editFullname.getEditText().filters =
            arrayOf<InputFilter>(InputFilter.LengthFilter(Constants.MAX_USERNAME_LENGTH))
        viewBinding.editFullname.getEditText().imeOptions = EditorInfo.IME_ACTION_DONE
        viewBinding.editFullname.setEditTextListener(object :
            BreezeEditText.BreezeEditTextListener {
            override fun onBreezeEditTextFocusChange(isFocused: Boolean) {
                if (!isFocused && !viewBinding.editFullname.getEnteredText().isNullOrBlank()) {
                    if (!Utils.isValidUserNameWithBreezeName(
                            BreezeCarUtil.listNameBlackList,
                            viewBinding.editFullname.getEnteredText()
                        )
                    ) {
                        viewBinding.editFullname.showErrorView(resources.getString(R.string.error_account_user_name_contain_breeze))
                    } else if (!isValidUserName(viewBinding.editFullname.getEnteredText())) {
                        viewBinding.editFullname.showErrorView(resources.getString(R.string.error_account_name))
                    } else {
                        viewBinding.editFullname.hideErrorView()
                    }
                    Analytics.logEditEvent(Event.USER_NAME_EDIT, getScreenName())
                }
            }
        })
        viewBinding.editFullname.showTextLengthView()
        viewBinding.editFullname.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                if (s != null) {
                    viewBinding.editFullname?.setTextForLength(
                        String.format(
                            "%d",
                            s.length
                        )
                    )
                }
                viewBinding.editFullname?.hideErrorView()
                Analytics.logEditEvent(Event.ACCOUNT_USERNAME_INPUT, getScreenName())
            }

        })
        viewBinding.editFullname.getEditText()
            .setText(breezeUserPreferenceUtil.retrieveUserName())

        viewBinding.editFullname.getEditText()
            .setOnEditorActionListener(OnEditorActionListener { v, actionId, event ->
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    // Call API
                    updateUserName()
                    return@OnEditorActionListener true
                }
                false
            })
        viewBinding.saveName.setBGThemed()
        viewBinding.saveName.setOnClickListener {
            updateUserName()
            Analytics.logClickEvent(Event.SAVE, getScreenName())
        }

        viewBinding.editFullname.getEditText().requestFocus()
        //(activity as DashboardActivity).showKeyboard(viewBinding.editFullname.getEditText())
    }

    private fun updateUserName() {
        if (viewBinding.editFullname.getEnteredText().isNotBlank()) {
            if (!Utils.isValidUserNameWithBreezeName(
                    BreezeCarUtil.listNameBlackList,
                    viewBinding.editFullname.getEnteredText()
                )
            ) {
                viewBinding.editFullname.showErrorView(resources.getString(R.string.error_account_user_name_contain_breeze))
            } else if (!isValidUserName(viewBinding.editFullname.getEnteredText())) {
                viewBinding.editFullname.showErrorView(resources.getString(R.string.error_account_name))
            } else {
                viewBinding.editFullname.hideErrorView()
                (activity as DashboardActivity).hideKeyboard()

                Analytics.logEvent(Event.UPDATE_NAME, Event.USER_NAME_SAVE_CLICK, getScreenName())
                breezeUserPreferenceUtil.saveUserName(viewBinding.editFullname.getEnteredText())
                (activity as DashboardActivity?)!!.refreshProfile()
                (activity as DashboardActivity).hideKeyboard()
                onBackPressed()
            }
        }
    }

    override fun inflateViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?,
        attachToContainer: Boolean
    ) = FragmentChangeUsernameBinding.inflate(inflater, container, attachToContainer)

    override fun getViewModelReference(): ChangeUserNameViewModel {
        return viewModel
    }

    fun hideKeyboard() {
        viewBinding.editFullname.clearFocus()
        (activity as DashboardActivity).hideKeyboard()
    }

    override fun getScreenName(): String {
        return Screen.SETTINGS_ACCOUNT_EDIT_USERNAME
    }
}
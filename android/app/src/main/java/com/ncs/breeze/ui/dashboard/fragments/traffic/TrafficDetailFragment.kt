package com.ncs.breeze.ui.dashboard.fragments.traffic

import android.content.res.Resources
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import com.google.android.gms.maps.model.LatLng
import com.google.gson.JsonObject
import com.mapbox.geojson.Feature
import com.mapbox.geojson.GeometryCollection
import com.mapbox.geojson.LineString
import com.mapbox.geojson.Point
import com.mapbox.maps.CameraOptions
import com.mapbox.maps.EdgeInsets
import com.mapbox.maps.MapView
import com.mapbox.maps.MapboxMap
import com.mapbox.maps.plugin.LocationPuck2D
import com.mapbox.maps.plugin.animation.camera
import com.mapbox.maps.plugin.animation.easeTo
import com.mapbox.maps.plugin.locationcomponent.LocationComponentPlugin
import com.mapbox.maps.plugin.locationcomponent.LocationProvider
import com.mapbox.maps.plugin.locationcomponent.location
import com.mapbox.navigation.ui.maps.camera.NavigationCamera
import com.mapbox.navigation.ui.maps.camera.data.MapboxNavigationViewportDataSource
import com.ncs.breeze.R
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.Event
import com.ncs.breeze.common.analytics.Screen
import com.ncs.breeze.common.constant.RNScreen
import com.ncs.breeze.common.event.ToAppRx
import com.ncs.breeze.common.extensions.android.getRNFragmentNavigationParams
import com.ncs.breeze.common.extensions.mapbox.setBreezeDefaultOptions
import com.breeze.model.api.response.traffic.TrafficResponse
import com.ncs.breeze.common.model.rx.ToAppRxEvent
import com.ncs.breeze.common.remote.MyServiceInterceptor
import com.ncs.breeze.common.utils.BreezeUserPreferenceUtil
import com.breeze.model.constants.Constants
import com.ncs.breeze.components.layermanager.MarkerLayerManager
import com.ncs.breeze.components.layermanager.impl.TrafficMarkerLayerManager
import com.ncs.breeze.components.marker.markerview2.SearchLocationMarkerViewManager
import com.ncs.breeze.components.marker.markerview2.TrafficMarkerView
import com.ncs.breeze.databinding.FragmentTrafficCameraDetailBinding
import com.ncs.breeze.helper.marker.MarkerLayerIconHelper
import com.ncs.breeze.ui.base.BaseActivity
import com.ncs.breeze.ui.base.BaseFragment
import com.ncs.breeze.ui.dashboard.activity.DashboardActivity
import com.ncs.breeze.ui.dashboard.manager.DashboardMapStateManager
import com.ncs.breeze.ui.rn.CustomReactFragment
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject


class TrafficDetailFragment :
    BaseFragment<FragmentTrafficCameraDetailBinding, TrafficFragmentViewModel>() {


    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var myServiceInterceptor: MyServiceInterceptor

    @Inject
    lateinit var breezeUserPreferenceUtil: BreezeUserPreferenceUtil

    private val mViewModel: TrafficFragmentViewModel by viewModels {
        viewModelFactory
    }

    var compositeDisposable: CompositeDisposable = CompositeDisposable()


    /**
     * for traffic
     */
    var trafficMarkerViewManager: TrafficMarkerLayerManager? = null
    private var mlistTraffic: ArrayList<TrafficResponse>? = null
    private var zoomCameraOptions: CameraOptions? = null


    /**
     *for showing current location
     */
    private lateinit var defaultLocationPuck: LocationPuck2D
    private lateinit var locationComponent: LocationComponentPlugin
    private var defaultLocationProvider: LocationProvider? = null

    private var codeExpressWay: String = ""

    private lateinit var mapboxMap: MapboxMap
    private lateinit var navigationCamera: NavigationCamera
    private lateinit var viewportDataSource: MapboxNavigationViewportDataSource

    private val pixelDensity = Resources.getSystem().displayMetrics.density

    var searchLocationMarkerViewManager: SearchLocationMarkerViewManager? = null

    companion object {
        internal fun newInstance() = TrafficDetailFragment()
        val TRAFFIC_CODE = "traffic_code"
    }

    private val carEventListener =
        ToAppRx.listenEvent(ToAppRxEvent::class.java)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
            }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        arguments?.let {
            codeExpressWay = arguments?.getString(TRAFFIC_CODE) ?: ""
        }

        (activity as DashboardActivity).hideKeyboard()
        findViews(view)
        initialize()

        searchLocationMarkerViewManager = SearchLocationMarkerViewManager(mapView)

        loadingView.visibility = View.VISIBLE

        showBottomSheetDetailInfor(codeExpressWay)
    }

    fun handleTraffic(listTraffic: ArrayList<TrafficResponse>?) {
        mlistTraffic = listTraffic
        mapboxMap.getStyle {
            trafficMarkerViewManager?.removeMarkerTooltipView(Constants.TRAFFIC_MARKER)
            loadingView.visibility = View.VISIBLE
            if (listTraffic != null && listTraffic.size > 0) {
                val listPoint = ArrayList<Point>()
                listTraffic.forEach {
                    listPoint.add(Point.fromLngLat(it.long!!, it.lat!!))
                }

                addTrafficIcon(Constants.TRAFFIC_MARKER, listTraffic)
                val geometry = LineString.fromLngLats(listPoint)
                val geometryCollection = GeometryCollection.fromGeometry(geometry)
                zoomCameraOptions = bestCameraOption(
                    geometryCollection,
                    1000.0
                )
                mapboxMap.easeTo(zoomCameraOptions!!)
                handleDisplayMarkerToolTip(listTraffic[0], false)
            }
            loadingView.visibility = View.GONE
        }
    }

    private fun addTrafficIcon(type: String, items: List<TrafficResponse>) {
        val featureList: ArrayList<Feature> = ArrayList()
        items.forEach {
            if (it.lat != null && it.long != null) {
                val properties = JsonObject()
                properties.addProperty(MarkerLayerManager.HIDDEN, false)
                MarkerLayerIconHelper.handleUnSelectedIcon(
                    properties,
                    breezeUserPreferenceUtil.isDarkTheme()
                )
                val feature =
                    Feature.fromGeometry(Point.fromLngLat(it.long!!, it.lat!!), properties, it.id)
                featureList.add(feature)
            }
        }
        trafficMarkerViewManager?.addOrReplaceMarkers(type, featureList)
    }


    /**
     * handle when user selected to camera
     */
    fun handleSelectedCamera(cameraID: String) {
        mlistTraffic?.find {
            return@find it.id == cameraID
        }?.let {
            handleDisplayMarkerToolTip(it, true)
        }
    }

    private fun handleDisplayMarkerToolTip(trafficResponse: TrafficResponse, recenter: Boolean) {
        if (trafficResponse.lat == null || trafficResponse.long == null) return

        val trafficMarkerView = TrafficMarkerView(
            LatLng(trafficResponse.lat!!, trafficResponse.long!!),
            mapboxMap,
            requireActivity(),
            trafficResponse.id
        )
        val properties = JsonObject()
        properties.addProperty(MarkerLayerManager.HIDDEN, false)
        MarkerLayerIconHelper.handleSelectedIcon(properties, breezeUserPreferenceUtil.isDarkTheme())
        val feature = Feature.fromGeometry(
            Point.fromLngLat(
                trafficResponse.long!!,
                trafficResponse.lat!!
            ), properties, trafficResponse.id
        )
        trafficMarkerViewManager?.renderMarkerToolTipView(
            MarkerLayerManager.MarkerViewType(
                trafficMarkerView,
                Constants.TRAFFIC_MARKER,
                feature
            )
        )
        trafficMarkerView.handleClick(
            trafficResponse,
            recenter,
            heightBottomSheet = DashboardMapStateManager.landingBottomSheetHeight
        ) { res ->
            Analytics.logClickEvent(Event.TAP_CARPARK_CLICK, getScreenName())
        }
    }


    private fun showBottomSheetDetailInfor(codeExpressWay: String) {
        val navigationParams = requireActivity().getRNFragmentNavigationParams(
            sessionToken = myServiceInterceptor.getSessionToken(),
            fragmentTag = RNScreen.EXPRESSWAY_DETAIL
        ).apply {
            putString("code_expressway", codeExpressWay)
        }
        val initialProperties = Bundle().apply {
            putString(Constants.RN_TO_SCREEN, RNScreen.EXPRESSWAY_DETAIL)
            putBundle(Constants.RN_CONSTANTS.NAVIGATION_PARAMS, navigationParams)
        }

        val reactNativeFragment = CustomReactFragment.newInstance(
            componentName = Constants.RN_CONSTANTS.COMPONENT_NAME,
            launchOptions = initialProperties,
            cleanOnDestroyed = false
        )

        //addFragment(reactNativeFragment, null, Constants.TAGS.TRAVEL_LOG_TAG)
        childFragmentManager
            .beginTransaction()
            .add(R.id.frBottomDetail, reactNativeFragment, "traffic_bottom_sheet")
            .addToBackStack("traffic_bottom_sheet")
            .commit()
    }

    lateinit var ll_heading: RelativeLayout
    lateinit var back_button: ImageView
    lateinit var header: TextView
    lateinit var select: TextView
    lateinit var mapView: MapView
    lateinit var loadingView: FrameLayout

    private fun findViews(view: View) {
        ll_heading = view.findViewById(R.id.ll_heading) as RelativeLayout
        back_button = view.findViewById(R.id.back_button) as ImageView
        header = view.findViewById(R.id.header) as TextView
        select = view.findViewById(R.id.select) as TextView
        mapView = view.findViewById(R.id.mapView) as MapView
        loadingView = view.findViewById(R.id.progressBar) as FrameLayout
    }

    fun initialize() {

        header.text = resources.getString(R.string.traffic_cameras)

        mapboxMap = mapView.getMapboxMap()
        viewportDataSource = MapboxNavigationViewportDataSource(
            mapView.getMapboxMap()
        )

        navigationCamera = NavigationCamera(
            mapView.getMapboxMap(),
            mapView.camera,
            viewportDataSource
        )
        mapView.setBreezeDefaultOptions()
        searchLocationMarkerViewManager = SearchLocationMarkerViewManager(mapView)

        initMapStyle {
            trafficMarkerViewManager = TrafficMarkerLayerManager(                mapView
            )
            trafficMarkerViewManager?.markerLayerClickHandler = trafficLayerClickHandler
            CoroutineScope(Dispatchers.Main).launch {
                trafficMarkerViewManager?.addStyleImages(Constants.TRAFFIC_MARKER)
            }
        }
        /**
         * init current location on map
         */
        initializeLocationComponent()

        back_button.setOnClickListener {
            onBackPressed()
        }

//        if (BreezeCarUtil.isInSearchLocationMode) {
//            LocationBreezeManager.getInstance().searchLocation?.apply {
//
//                val searchLocationMarker = LocationSearchMarkerView(
//                    LatLng(latitude, longitude),
//                    mapboxMap,
//                    requireActivity(),
//                    searchLocationMarkerViewManager!!
//                )
//                searchLocationMarkerViewManager?.addMarker(searchLocationMarker)
//            }
//
//        }
    }

    private fun initMapStyle(basicMapStyleReqd: Boolean = true, after: () -> Unit = {}) {
        (activity as? BaseActivity<*, *>)?.let {
            mapboxMap.loadStyleUri(
                it.getMapboxStyle(basicMapStyleReqd)
            ) { style ->
                if (isFragmentDestroyed()) {
                    return@loadStyleUri
                }
                after()
            }
        }
    }


    private fun initializeLocationComponent() {
        locationComponent = mapView.location.apply {
            enabled = true
            pulsingEnabled = true
        }
        defaultLocationPuck = LocationPuck2D(
            bearingImage = activity?.getDrawable(R.drawable.cursor)
        )
        locationComponent.locationPuck = defaultLocationPuck

        defaultLocationProvider = locationComponent.getLocationProvider()
    }

    override fun inflateViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?,
        attachToContainer: Boolean
    ) = FragmentTrafficCameraDetailBinding.inflate(inflater, container, attachToContainer)

    override fun getViewModelReference(): TrafficFragmentViewModel {
        return mViewModel
    }

    override fun getScreenName(): String {
        return Screen.FRAGMENT_CRUISE_MODE
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        compositeDisposable.remove(carEventListener)
        compositeDisposable.dispose()
    }

    private fun bestCameraOption(
        geometryCollection: GeometryCollection,
        routeMeasuredBottomHeight: Double
    ): CameraOptions? {
        val bearingList = arrayListOf(
            0.0,
            9.0,
            18.0,
            27.0,
            36.0,
            45.0,
            54.0,
            63.0,
            72.0,
            81.0,
            90.0
        )
        var bestOption: CameraOptions? = null
        var bestZoom = 0.0

        for (bearing in bearingList) {
            var option = mapboxMap.cameraForGeometry(
                geometryCollection,
                EdgeInsets(
                    100.0 * pixelDensity,
                    60.0 * pixelDensity,
                    routeMeasuredBottomHeight + (20 * pixelDensity),
                    60.0 * pixelDensity
                ),
                bearing = bearing,
                0.0
            )
            if (option.zoom != null && option.zoom!! >= bestZoom) {
                bestZoom = option.zoom!!
                bestOption = option
            }
        }

        return bestOption
    }

    private val trafficLayerClickHandler = object : MarkerLayerManager.MarkerLayerClickHandler {
        override fun handleOnclick(
            layerType: String,
            feature: Feature,
            currentMarker: MarkerLayerManager.MarkerViewType?
        ) {
            if (currentMarker != null && currentMarker.feature.id() == feature.id()) {
                trafficMarkerViewManager?.removeMarkerTooltipView()
                return
            }
            mlistTraffic?.let { mlistTraffic ->
                val trafficResponse = mlistTraffic.find {
                    return@find it.id == feature.id()
                }
                trafficResponse?.let { trafficResponse ->
                    handleDisplayMarkerToolTip(trafficResponse, true)
                }
            }
        }

        override fun handleOnFreeMapClick(point: Point) {
            // no op
        }
    }

}
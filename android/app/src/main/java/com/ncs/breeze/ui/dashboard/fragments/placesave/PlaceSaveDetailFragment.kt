package com.ncs.breeze.ui.dashboard.fragments.placesave

import android.content.Context
import android.location.Location
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.annotation.UiThread
import androidx.appcompat.app.AlertDialog
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.breeze.model.SearchLocation
import com.breeze.model.api.response.PlaceDetailsResponse
import com.breeze.model.api.response.amenities.BaseAmenity
import com.breeze.model.constants.Constants
import com.breeze.model.enums.BookmarkType
import com.breeze.model.enums.CollectionCode
import com.breeze.model.extensions.dpToPx
import com.breeze.model.extensions.rn.getBooleanOrNull
import com.breeze.model.extensions.rn.getStringOrNull
import com.breeze.model.extensions.safeDouble
import com.facebook.react.bridge.Arguments
import com.facebook.react.bridge.ReadableMap
import com.google.android.gms.maps.model.LatLng
import com.google.gson.JsonObject
import com.mapbox.android.gestures.MoveGestureDetector
import com.mapbox.android.gestures.RotateGestureDetector
import com.mapbox.android.gestures.StandardScaleGestureDetector
import com.mapbox.geojson.Feature
import com.mapbox.geojson.GeometryCollection
import com.mapbox.geojson.LineString
import com.mapbox.geojson.Point
import com.mapbox.maps.plugin.LocationPuck2D
import com.mapbox.maps.plugin.animation.camera
import com.mapbox.maps.plugin.gestures.OnMoveListener
import com.mapbox.maps.plugin.gestures.OnRotateListener
import com.mapbox.maps.plugin.gestures.OnScaleListener
import com.mapbox.maps.plugin.gestures.gestures
import com.mapbox.maps.plugin.locationcomponent.LocationComponentPlugin
import com.mapbox.maps.plugin.locationcomponent.LocationProvider
import com.mapbox.maps.plugin.locationcomponent.location
import com.mapbox.navigation.ui.maps.camera.NavigationCamera
import com.mapbox.navigation.ui.maps.camera.data.MapboxNavigationViewportDataSource
import com.mapbox.navigation.utils.internal.toPoint
import com.ncs.breeze.BuildConfig
import com.ncs.breeze.R
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.Event
import com.ncs.breeze.common.constant.RNScreen
import com.ncs.breeze.common.extensions.android.getApp
import com.ncs.breeze.common.extensions.android.getRNFragmentNavigationParams
import com.ncs.breeze.common.extensions.android.getShareBusinessLogicPackage
import com.ncs.breeze.common.extensions.mapbox.setBreezeDefaultOptions
import com.ncs.breeze.common.remote.MyServiceInterceptor
import com.ncs.breeze.common.rxbus.RxBus
import com.ncs.breeze.common.rxbus.RxEvent
import com.ncs.breeze.common.utils.BreezeMapZoomUtil
import com.ncs.breeze.common.utils.BreezeUserPreferenceUtil
import com.ncs.breeze.common.utils.LocationBreezeManager
import com.ncs.breeze.components.layermanager.MarkerLayerManager
import com.ncs.breeze.components.map.profile.ListenerCallbackLandingMap
import com.ncs.breeze.components.marker.markerview2.MarkerView2
import com.ncs.breeze.components.marker.markerview2.SavedPlaceMarkerTooltip
import com.ncs.breeze.components.marker.markerview2.SearchLocationMarkerViewManager
import com.ncs.breeze.databinding.FragmentPlaceSavedBinding
import com.ncs.breeze.helper.marker.MarkerLayerIconHelper
import com.ncs.breeze.reactnative.nativemodule.ShareBusinessLogicEvent
import com.ncs.breeze.ui.base.BaseActivity
import com.ncs.breeze.ui.base.BaseFragment
import com.ncs.breeze.ui.dashboard.AdjustableBottomSheet
import com.ncs.breeze.ui.dashboard.activity.DashboardActivity
import com.ncs.breeze.ui.dashboard.manager.PlaceSaveMapStateManager
import com.ncs.breeze.ui.rn.CustomReactFragment
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject


class PlaceSaveDetailFragment :
    BaseFragment<FragmentPlaceSavedBinding, PlaceSaveFragmentViewModel>(),
    AdjustableBottomSheet {


    @Inject
    lateinit var myServiceInterceptor: MyServiceInterceptor

    @Inject
    lateinit var userPreferenceUtil: BreezeUserPreferenceUtil

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel: PlaceSaveFragmentViewModel by viewModels { viewModelFactory }

    var compositeDisposable: CompositeDisposable = CompositeDisposable()

    private var mListPoi: ArrayList<PlaceDetailsResponse>? = null
    var markerTooltipView: MarkerView2? = null

    /**
     *for showing current location
     */
    private lateinit var defaultLocationPuck: LocationPuck2D
    private lateinit var locationComponent: LocationComponentPlugin
    private var defaultLocationProvider: LocationProvider? = null

    private lateinit var navigationCamera: NavigationCamera
    private lateinit var viewportDataSource: MapboxNavigationViewportDataSource

    /**
     * for ERP
     */
    var searchLocationMarkerViewManager: SearchLocationMarkerViewManager? = null

    var mapControllerPlaceSave: MapControllerPlaceSave? = null

    var locationFocus: Location? = null

    lateinit var placeMapMapCamera: PlaceMapMapCamera

    var isCameraTrackingDismissed: Boolean = false

    var toRNScreen: String = ""
    private var collectionId: Int? = null
    var collectionName: String = ""
    var collectionCode = CollectionCode.NORMAL
    var collectionDescription: String? = null
    var currentSearchedLocation: SearchLocation? = null

    override fun inflateViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?,
        attachToContainer: Boolean
    ) = FragmentPlaceSavedBinding.inflate(inflater, container, attachToContainer)

    override fun getViewModelReference() = viewModel

    override fun getScreenName() = when (collectionCode) {
        CollectionCode.SHORTCUTS -> "[shortcut_collection]"
        CollectionCode.SAVED_PLACES -> "[my_saved_places_collection]"
        else -> "[custom_saved_places_collection]"
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        locationFocus = LocationBreezeManager.getInstance().currentLocation
        arguments?.let { args ->
            collectionId = args.getInt(ARG_COLLECTION_ID) ?: 0
            collectionName = args.getString(ARG_COLLECTION_NAME) ?: ""
            toRNScreen = arguments?.getString(ARG_TO_RN_SCREEN) ?: ""
            val collectionCodeValue = args.getString(ARG_COLLECTION_CODE) ?: ""
            collectionDescription = args.getString(ARG_COLLECTION_DESCRIPTION)
            collectionCode = CollectionCode.values().find { it.name == collectionCodeValue }
                ?: CollectionCode.NORMAL
        }

        (activity as DashboardActivity).hideKeyboard()
        initialize()

        searchLocationMarkerViewManager =
            SearchLocationMarkerViewManager(viewBinding.mapView)

        isInPlaceSaveScreen = true
    }

    private fun listenRxEvents() {
        compositeDisposable.addAll(
            RxBus.listen(RxEvent.CollectionDetailsSearchLocation::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe { onSearchForLocation(it.searchLocation) },
            RxBus.listen(RxEvent.CollectionDetailsClearSearch::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe { onClearSearchLocation() },
        )
    }

    fun onClearSearchLocation() {
        currentSearchedLocation = null
        mapControllerPlaceSave?.resetAllViews()
        mListPoi?.let { listPoi ->
            listPoi.forEach { it.isSelected = false }
            handlePoi(listPoi, false)
        }
    }

    private fun toggleButtonRecenterVisibility(isVisible: Boolean) {
        if (!lifecycle.currentState.isAtLeast(Lifecycle.State.STARTED)) return
        activity?.runOnUiThread {
            viewBinding.buttonRecenter.isVisible = isVisible
        }
    }

    private fun onSearchForLocation(locationData: ReadableMap) {
        val searchedLocation = SearchLocation(
            address1 = locationData.getStringOrNull("address1"),
            token = locationData.getStringOrNull("token"),
            address2 = locationData.getStringOrNull("address2"),
            fullAddress = locationData.getStringOrNull("fullAddress"),
            placeId = locationData.getStringOrNull("placeId"),
            distance = locationData.getStringOrNull("distance"),
            source = locationData.getStringOrNull("source"),
            type = locationData.getStringOrNull("type"),
            lat = locationData.getStringOrNull("lat"),
            long = locationData.getStringOrNull("long"),
            showAvailabilityFB = locationData.getBooleanOrNull("showAvailabilityFB") ?: false
        )
        currentSearchedLocation = searchedLocation
        mapControllerPlaceSave?.currentPin = null
        mapControllerPlaceSave?.clearALlCarPark()
        mapControllerPlaceSave?.resetAllViews()
        mapControllerPlaceSave?.showSearchMarkerWithTooltip(searchedLocation)
        toggleButtonRecenterVisibility(false)
        placeMapMapCamera.recenterToSearchLocation(searchedLocation)
    }

    override fun onStart() {
        super.onStart()
        listenRxEvents()
    }

    override fun onStop() {
        super.onStop()
        compositeDisposable.clear()
    }

    fun initialize() {

        viewBinding.mapView.gestures.addOnMoveListener(onMoveListener)
        viewportDataSource = MapboxNavigationViewportDataSource(
            viewBinding.mapView.getMapboxMap()
        )
        placeMapMapCamera = PlaceMapMapCamera(viewBinding.mapView.getMapboxMap())
        navigationCamera = NavigationCamera(
            viewBinding.mapView.getMapboxMap(),
            viewBinding.mapView.camera,
            viewportDataSource
        )

        viewBinding.mapView.setBreezeDefaultOptions()
        searchLocationMarkerViewManager =
            SearchLocationMarkerViewManager(viewBinding.mapView)

        initMapStyle {

            initPlaceMapController()
            /**
             * init current location on map
             */
            intiCurrentLocationComponent()

            viewBinding.mapView.gestures.addOnFlingListener {
                Timber.d("On fling is called")
                Analytics.logMapInteractionEvents(Event.DRAG_MAP, getScreenName())
            }
            viewBinding.mapView.gestures.addOnScaleListener(object : OnScaleListener {
                override fun onScale(detector: StandardScaleGestureDetector) {}

                override fun onScaleBegin(detector: StandardScaleGestureDetector) {}

                override fun onScaleEnd(detector: StandardScaleGestureDetector) {
                    Timber.d("On scale end is called")
                    Analytics.logMapInteractionEvents(Event.PINCH_MAP, getScreenName())
                }
            })

            viewBinding.mapView.gestures.addOnRotateListener(object : OnRotateListener {
                override fun onRotate(detector: RotateGestureDetector) {}

                override fun onRotateBegin(detector: RotateGestureDetector) {}

                override fun onRotateEnd(detector: RotateGestureDetector) {
                    Timber.d("On rotate is called")
                    Analytics.logMapInteractionEvents(Event.ROTATE_MAP, getScreenName())
                }

            })
            lifecycleScope.launchWhenResumed {
                showBottomSheetDetailsScreenRN()
            }
        }

        viewBinding.buttonRecenter.setOnClickListener {
            recenterMap()

        }
    }

    private fun zoomToCurrentLocation() {
        placeMapMapCamera.recenterMap({
            arrayListOf<Point>().apply {
                LocationBreezeManager.getInstance().currentLocation?.toPoint()
                    ?.let { currentLocation -> add(currentLocation) }
                mapControllerPlaceSave?.currentPin?.let { pin -> add(pin) }
            }
        }) { toggleButtonRecenterVisibility(false) }
    }

    fun zoomToCarparks(carparks: List<BaseAmenity>) {
        val radius = mapControllerPlaceSave?.calculateCarParkZoomRadius(carparks) ?: return
        BreezeMapZoomUtil.recenterMapToLocation(
            viewBinding.mapView.getMapboxMap(),
            lat = PlaceSaveMapStateManager.activeLocationUse!!.latitude,
            lng = PlaceSaveMapStateManager.activeLocationUse!!.longitude,
            edgeInset = placeMapMapCamera.mapCameraPadding,
            zoomRadius = radius
        )
    }


    private fun initMapStyle(basicMapStyleReqd: Boolean = true, after: () -> Unit = {}) {
        (activity as? BaseActivity<*, *>)?.let {
            viewBinding.mapView.getMapboxMap().loadStyleUri(
                it.getMapboxStyle(basicMapStyleReqd)
            ) { style ->
                if (isFragmentDestroyed()) {
                    return@loadStyleUri
                }
                after()
            }
        }
    }

    private fun initPlaceMapController() {
        /**
         * setup dashboard activity
         */
        val mapListenerCallBack = object :
            ListenerCallbackLandingMap {
            override fun moreInfoButtonClick(ameniti: BaseAmenity) {
                showParkingInfoRN(ameniti.id!!)
            }

            override fun onDismissedCameraTracking() {
                toggleButtonRecenterVisibility(true)
            }

            override fun onMapOtherPointClicked(point: Point) {
                // no op required
            }

            override fun onERPInfoRequested(erpID: Int) {
            }

            override fun poiMoreInfoButtonClick(ameniti: BaseAmenity) {
            }
        }

        mapControllerPlaceSave = MapControllerPlaceSave().apply {
            analyticsScreenName = getScreenName()
        }

        mapControllerPlaceSave?.setUp(
            viewBinding.mapView,
            this,
            userPreferenceUtil.isDarkTheme(),
            pCallBack = mapListenerCallBack,
            collectionCode = collectionCode
        )
    }

    private fun intiCurrentLocationComponent() {
        locationComponent = viewBinding.mapView.location.apply {
            enabled = true
            pulsingEnabled = true
        }
        defaultLocationPuck = LocationPuck2D(
            bearingImage = activity?.getDrawable(R.drawable.cursor)
        )
        locationComponent.locationPuck = defaultLocationPuck

        defaultLocationProvider = locationComponent.getLocationProvider()
    }

    private fun showBottomSheetDetailsScreenRN() {
        val navigationParams = requireActivity().getRNFragmentNavigationParams(
            sessionToken = myServiceInterceptor.getSessionToken(),
            fragmentTag = toRNScreen
        ).apply {
            putInt("collection_id", collectionId!!)
            putString("collection_name", collectionName)
            putString("code", collectionCode.name)
            collectionDescription?.let { putString("collection_description", it) }
        }
        val initialProperties = Bundle().apply {
            putString(Constants.RN_TO_SCREEN, toRNScreen)
            putBundle(Constants.RN_CONSTANTS.NAVIGATION_PARAMS, navigationParams)
        }

        val reactNativeFragment = CustomReactFragment.newInstance(
            componentName = Constants.RN_CONSTANTS.COMPONENT_NAME,
            launchOptions = initialProperties,
            cleanOnDestroyed = false
        )

        childFragmentManager
            .beginTransaction()
            .add(R.id.frBottomDetail, reactNativeFragment, toRNScreen)
            .addToBackStack(RNScreen.COLLECTION_DETAILS)
            .commit()
    }


    fun handlePoi(list: ArrayList<PlaceDetailsResponse>?, isPreview: Boolean) {
        if (mapControllerPlaceSave == null || currentSearchedLocation != null) return

        mapControllerPlaceSave?.clearALlCarPark()
        mListPoi = list
        if ((mListPoi?.size ?: 0) > 0) {
            viewBinding.mapView.getMapboxMap().getStyle {
                mapControllerPlaceSave?.placeSaveLayerManager?.removeAllMarkers()
                mapControllerPlaceSave?.placeSaveLayerManager?.removeMarkerTooltipView()
                viewBinding.progressBar.visibility = View.VISIBLE
                if (validatePoiData(isPreview)) {
                    val listPoint = ArrayList<Point>()
                    mListPoi!!.forEach {
                        listPoint.add(
                            Point.fromLngLat(it.long!!.safeDouble(), it.lat!!.safeDouble())
                        )
                    }

                    addPoiIcon(mListPoi!!, isPreview)
                    val geometry = LineString.fromLngLats(listPoint)
                    val geometryCollection = GeometryCollection.fromGeometry(geometry)
                    placeMapMapCamera.savedPlacePoints = geometryCollection
                    toggleButtonRecenterVisibility(true)
                    if (isPreview) {
                        if ((mListPoi?.size ?: 0) > 0) {
                            placeMapMapCamera.recenterMap({
                                arrayListOf<Point>(
                                    Point.fromLngLat(
                                        (mListPoi?.get(0)?.long?.safeDouble()) ?: 0.0,
                                        (mListPoi?.get(0)?.lat?.safeDouble()) ?: 0.0
                                    )
                                )
                            }) { toggleButtonRecenterVisibility(false) }
                        }
                    } else {
                        placeMapMapCamera.recenterMap({
                            arrayListOf<Point>().apply {
                                mapControllerPlaceSave?.currentPin?.let { pin -> add(pin) }
                                LocationBreezeManager.getInstance().currentLocation?.toPoint()
                                    ?.let { currentLocation -> add(currentLocation) }
                            }
                        }) { toggleButtonRecenterVisibility(false) }
                    }
                    mListPoi?.find { it.isSelected == true }?.let {
                        mapControllerPlaceSave?.currentPin = null
                        handleDisplayMarkerToolTip(it, false, isPreview)
                    }
                }
                viewBinding.progressBar.visibility = View.GONE
            }
        } else {
            mapControllerPlaceSave?.placeSaveLayerManager?.removeAllMarkers()
            mapControllerPlaceSave?.placeSaveLayerManager?.removeMarkerTooltipView()
            viewBinding.progressBar.visibility = View.GONE
            zoomToCurrentLocation()
            toggleButtonRecenterVisibility(false)
        }
    }

    private fun validatePoiData(isPreview: Boolean) =
        (mListPoi != null && mListPoi!!.size > 0 && !(isPreview && mListPoi!!.size > 1))

    private fun addPoiIcon(items: List<PlaceDetailsResponse>, isPreview: Boolean) {
        val poiCodeMap = HashMap<String, ArrayList<Feature>>()
        items.forEach {
            if (it.lat != null && it.long != null) {
                val properties = JsonObject()
                properties.addProperty(MarkerLayerManager.HIDDEN, false)
                properties.addProperty(MarkerLayerManager.IMAGE_TYPE, "")
                MarkerLayerIconHelper.handleUnSelectedIcon(
                    properties,
                    userPreferenceUtil.isDarkTheme()
                )
                val feature = if (isPreview) {
                    Feature.fromGeometry(
                        Point.fromLngLat(
                            it.long!!.safeDouble(),
                            it.lat!!.safeDouble()
                        ), properties, MapControllerPlaceSave.PREVIEW_BOOKMARK_ID
                    )
                } else {
                    Feature.fromGeometry(
                        Point.fromLngLat(
                            it.long!!.safeDouble(),
                            it.lat!!.safeDouble()
                        ), properties, it.bookmarkId.toString()
                    )
                }

                if (!it.code.isNullOrEmpty()) {
                    if (poiCodeMap[it.code] == null) {
                        poiCodeMap[it.code!!] = ArrayList()
                    }
                    poiCodeMap[it.code!!]!!.add(feature)
                } else if (isPreview) {
                    poiCodeMap[BookmarkType.CUSTOM.name] = arrayListOf(feature)
                }
            }
        }
        poiCodeMap.forEach {
            mapControllerPlaceSave?.placeSaveLayerManager?.addOrReplaceMarkers(it.key, it.value)
        }
    }

    private fun handleDisplayMarkerToolTip(
        placeDetailsResponse: PlaceDetailsResponse,
        recenter: Boolean,
        isPreview: Boolean
    ) {
        if (isPreview || placeDetailsResponse.lat == null || placeDetailsResponse.long == null) return

        PlaceSaveMapStateManager.setCurrentLocationFocus(
            PlaceSaveMapStateManager.AMENITY_INSTANCE,
            placeDetailsResponse.lat!!.safeDouble(), placeDetailsResponse.long!!.safeDouble(),
            null
        )
        showCarParkWhenPOIFocus()

        val poiSaveMarker = SavedPlaceMarkerTooltip(
            LatLng(
                placeDetailsResponse.lat!!.safeDouble(),
                placeDetailsResponse.long!!.safeDouble()
            ),
            viewBinding.mapView.getMapboxMap(),
            requireActivity(),
            placeDetailsResponse.bookmarkId.toString()
        )
        markerTooltipView = poiSaveMarker
        val properties = JsonObject()
        properties.addProperty(MarkerLayerManager.HIDDEN, false)
        MarkerLayerIconHelper.handleSelectedIcon(properties, userPreferenceUtil.isDarkTheme())
        val feature = Feature.fromGeometry(
            Point.fromLngLat(
                placeDetailsResponse.long!!.safeDouble(),
                placeDetailsResponse.lat!!.safeDouble()
            ), properties, placeDetailsResponse.bookmarkId.toString()
        )
        mapControllerPlaceSave?.placeSaveLayerManager?.renderMarkerToolTipView(
            MarkerLayerManager.MarkerViewType(
                poiSaveMarker,
                placeDetailsResponse.code.toString(),
                feature
            )
        )
        poiSaveMarker.handleClick(
            placeDetailsResponse,
            recenter,
            heightBottomSheet = PlaceSaveMapStateManager.bottomSheetHeight,
            navigateHereButtonClick = { item, _ ->
                mapControllerPlaceSave?.startRoutePlanningFromPoiMarker(item)

                Analytics.logClickEvent(
                    "[map]:tooltip_saved_navigate_here",
                    getScreenName()
                )
                Analytics.logLocationDataEvent(
                    Event.NAVIGATE_HERE_SAVED,
                    getScreenName(),
                    item.name.toString(),
                    item.lat.toString(),
                    item.long.toString()
                )
            },
            bookmarkIconClick = { bookmarkId ->
                Analytics.logClickEvent(
                    "[map]:tooltip_saved_unbookmark",
                    getScreenName()
                )
                deleteBookmark(bookmarkId)
            },
            poiIconClick = { item, res ->
                Analytics.logClickEvent(Event.MAP_SAVED_ICON_CLICK, getScreenName())
                Analytics.logLocationDataEvent(
                    Event.MAP_SAVED_ICON_CLICK,
                    getScreenName(),
                    item.name.toString(),
                    item.lat.toString(),
                    item.long.toString()
                )
            }
        )
        toggleButtonRecenterVisibility(false)
    }

    private fun deleteBookmark(bookmarkId: Int) {
        viewBinding.root.context.getApp()?.shareBusinessLogicPackage?.getRequestsModule()
            ?.flatMap {
                val data = Arguments.fromBundle(
                    bundleOf(
                        "action" to "DELETE",
                        "bookmarkId" to bookmarkId
                    )
                )
                it.callRN(
                    ShareBusinessLogicEvent.TOGGLE_SAVE_LOCATION_COLLECTION_DETAILS.name,
                    data
                )
            }?.subscribeOn(Schedulers.io())?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe()?.also { compositeDisposable.add(it) }
    }

    private fun showDeleteSavedPlaceConfirmation(amenitiesID: String) {
        AlertDialog.Builder(viewBinding.root.context)
            .setMessage(R.string.desc_delete_saved_place_confirmation_2)
            .setNegativeButton(
                R.string.cancel_uppercase
            ) { dialog, _ ->
                dialog.dismiss()
            }
            .setPositiveButton(
                R.string.confirm
            ) { dialog, _ ->
                dialog.dismiss()
                viewModel.deselectingBookmarkTooltip(amenitiesID) {}
            }.show()
    }

    private val onMoveListener = object : OnMoveListener {
        override fun onMoveBegin(detector: MoveGestureDetector) {
            if (!lifecycle.currentState.isAtLeast(Lifecycle.State.STARTED)) return
            if (!viewBinding.buttonRecenter.isVisible) {
                toggleButtonRecenterVisibility(true)
            }
        }

        override fun onMove(detector: MoveGestureDetector): Boolean {
            return false
        }

        override fun onMoveEnd(detector: MoveGestureDetector) {}
    }

    private val carParkObserver = Observer<List<BaseAmenity>> {
        it?.let {
            mapControllerPlaceSave?.handleCarpark(
                it,
                isMoveCameraWrapperAllCarpark = true
            )
        }
    }

    private fun showCarParkWhenPOIFocus() {
        viewModel.mListCarpark.observe(viewLifecycleOwner, carParkObserver)
        if (mapControllerPlaceSave != null) {
            PlaceSaveMapStateManager.activeLocationUse =
                PlaceSaveMapStateManager.currentLocationFocus
        }
        PlaceSaveMapStateManager.currentLocationFocus?.let {
            viewModel.getCarParkDataShowInMap(it, null)
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        val callback: OnBackPressedCallback = object : OnBackPressedCallback(
            true // default to enabled
        ) {
            override fun handleOnBackPressed() {
                val currentFragment =
                    childFragmentManager.findFragmentById(R.id.frBottomDetailLayer2)
                if (currentFragment != null) {
                    if (currentFragment.isAdded) {
                        childFragmentManager.beginTransaction().remove(currentFragment).commit()
                    } else {
                        parentFragmentManager.popBackStack()
                    }
                } else {
                    parentFragmentManager.popBackStack()
                }
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(this, callback)
    }

    override fun onDestroyView() {
        isInPlaceSaveScreen = false
        super.onDestroyView()
        (activity as? DashboardActivity)?.openCollectionListOnCollectionDetailsClosed()
        compositeDisposable.dispose()
    }

    fun showRouteAndResetToCenter(args: Bundle) {
        (activity as? DashboardActivity)?.apply {
            showRouteAndResetToCenter(args)
        }
    }

    private fun sendEventPlaceSelected(id: Int, isSelected: Boolean) {
        context?.getShareBusinessLogicPackage()?.getRequestsModule()?.flatMap {
            val rnData = Arguments.createMap().apply {
                putInt("bookmark_id", id)
                putBoolean("is_selected", isSelected)
            }
            it.callRN(ShareBusinessLogicEvent.SELECT_SAVED_LOCATION.eventName, rnData)
        }?.subscribeOn(Schedulers.io())?.observeOn(AndroidSchedulers.mainThread())?.subscribe()
    }

    override fun adjustRecenterButton(height: Int, index: Int, fromScreen: String) {
        Timber.i("adjustRecenterButton")
        PlaceSaveMapStateManager.bottomSheetHeight = height
        PlaceSaveMapStateManager.bottomSheetState = index
        placeMapMapCamera.updateCameraPadding(bottom = height.dpToPx().toDouble())
        if (!viewLifecycleOwner.lifecycle.currentState.isAtLeast(Lifecycle.State.STARTED))
            return
        activity?.runOnUiThread {
            recenterMap()
            viewBinding.rlFreeDriveInfo.setPadding(0, 0, 0, height.dpToPx())
        }
    }

    @UiThread
    private fun recenterMap() {
        when {
            currentSearchedLocation != null -> {
                toggleButtonRecenterVisibility(false)
                placeMapMapCamera.recenterToSearchLocation(currentSearchedLocation!!)
            }

            PlaceSaveMapStateManager.currentLocationFocus != null -> {
                BreezeMapZoomUtil.recenterMapToLocation(
                    viewBinding.mapView.getMapboxMap(),
                    PlaceSaveMapStateManager.currentLocationFocus!!.latitude,
                    PlaceSaveMapStateManager.currentLocationFocus!!.longitude,
                    placeMapMapCamera.mapCameraPadding,
                    -1.0,
                    PlaceSaveMapStateManager.forceFocusZoom
                )
                //reset forceFocusZoom after first recenter
                PlaceSaveMapStateManager.forceFocusZoom = null
            }

            else -> {
                placeMapMapCamera.recenterMap({
                    arrayListOf<Point>().apply {
                        mapControllerPlaceSave?.currentPin?.let { pin -> add(pin) }
                        LocationBreezeManager.getInstance().currentLocation?.toPoint()
                            ?.let { currentLocation -> add(currentLocation) }
                    }
                }) {
                    toggleButtonRecenterVisibility(false)
                }
            }
        }
    }

    fun closeParkingCalculator() {
        activity?.runOnUiThread {
            val currentFragment = childFragmentManager.findFragmentById(R.id.frBottomDetailLayer2)
            if (currentFragment != null) {
                if (currentFragment.isAdded) {
                    childFragmentManager.beginTransaction().remove(currentFragment).commit()
                }
            }
        }
    }

    fun showParkingInfoRN(parkingId: String) {
        val fragmentTag = Constants.CARPARK_MOREINFO.TO_PARKING_MOREINFO_SCREEN
        val navigationParams = requireActivity().getRNFragmentNavigationParams(
            sessionToken = myServiceInterceptor.getSessionToken(),
            fragmentTag = fragmentTag
        )
        navigationParams.putString(Constants.RN_CONSTANTS.BASE_URL, BuildConfig.SERVER_HEADER)
        navigationParams.putString(Constants.CARPARK_MOREINFO.PARKING_ID, parkingId)
        navigationParams.putString(
            Constants.CARPARK_MOREINFO.AWS_ID_TOKEN,
            myServiceInterceptor.getSessionToken()!!
        )

        val initialProperties = bundleOf(
            Constants.RN_TO_SCREEN to fragmentTag,
            Constants.RN_CONSTANTS.NAVIGATION_PARAMS to navigationParams
        )
        val bottomPanelFragment = CustomReactFragment.newInstance(
            componentName = Constants.RN_CONSTANTS.COMPONENT_NAME,
            launchOptions = initialProperties,
            cleanOnDestroyed = false
        )

        childFragmentManager
            .beginTransaction()
            .add(R.id.frBottomDetailLayer2, bottomPanelFragment, fragmentTag)
            .addToBackStack(fragmentTag)
            .commit()
    }

    fun onClickMapMarkerIcon(featureId: String?) {
        mListPoi?.find { it.bookmarkId != null && "${it.bookmarkId}" == featureId }?.let {
            sendEventPlaceSelected(it.bookmarkId!!, !it.isSelected!!)
        }
    }


    companion object {
        const val ARG_COLLECTION_ID = "COLLECTION_ID"
        const val ARG_TO_RN_SCREEN = "TO_RN_SCREEN"
        const val ARG_COLLECTION_NAME = "COLLECTION_NAME"
        const val ARG_COLLECTION_CODE = "COLLECTION_CODE"
        const val ARG_COLLECTION_DESCRIPTION = "COLLECTION_DESCRIPTION"

        var isInPlaceSaveScreen = false
    }
}

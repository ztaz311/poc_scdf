package com.ncs.breeze.common.helper.ehorizon

enum class EHorizonMode {
    NAVIGATION,
    CRUISE
}
package com.ncs.breeze.service

import android.app.Service
import android.content.Intent
import com.mapbox.navigation.core.lifecycle.MapboxNavigationApp
import com.mapbox.navigation.core.trip.session.TripSessionState
import com.mapbox.navigation.core.trip.session.TripSessionStateObserver
import com.ncs.breeze.components.UserLocationForOBUEventsPublisher
import timber.log.Timber

class NavigationControllerService : Service() {
    private val userLocationForOBUEventsPublisher by lazy { UserLocationForOBUEventsPublisher(this) }

    private val tripSessionStateObserver by lazy {
        TripSessionStateObserver { tripSessionState ->
            if (tripSessionState == TripSessionState.STOPPED) {
                onTripSessionStopped()
            }
        }
    }

    private fun onTripSessionStopped() {
        stopSelf()
        userLocationForOBUEventsPublisher.stopPublishingUserLocation()
    }

    override fun onBind(p0: Intent?) = null

    override fun onCreate() {
        super.onCreate()
        Timber.d("onCreate: $this")
        MapboxNavigationApp.current()?.registerTripSessionStateObserver(tripSessionStateObserver)
        userLocationForOBUEventsPublisher.startPublishUserLocation()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Timber.d("onStartCommand: $this - flags= $flags - startId= $startId")
        return super.onStartCommand(intent, flags, startId)
    }

    override fun onDestroy() {
        super.onDestroy()
        Timber.d("onDestroy: $this")
        userLocationForOBUEventsPublisher.stopPublishingUserLocation()
        MapboxNavigationApp.current()?.registerTripSessionStateObserver(tripSessionStateObserver)
    }
}
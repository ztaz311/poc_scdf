package com.ncs.breeze.ui.base

import android.content.Intent
import android.content.res.Configuration
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.viewbinding.ViewBinding
import com.facebook.react.ReactFragment
import com.facebook.react.ReactInstanceManager
import com.facebook.react.modules.core.DefaultHardwareBackBtnHandler
import com.google.firebase.crashlytics.ktx.crashlytics
import com.google.firebase.ktx.Firebase
import com.mapbox.androidauto.internal.logAndroidAuto
import com.mapbox.geojson.FeatureCollection
import com.mapbox.maps.Style
import com.mapbox.maps.extension.style.expressions.generated.Expression
import com.mapbox.maps.extension.style.layers.addLayer
import com.mapbox.maps.extension.style.layers.addLayerAbove
import com.mapbox.maps.extension.style.layers.generated.symbolLayer
import com.mapbox.maps.extension.style.layers.getLayer
import com.mapbox.maps.extension.style.layers.properties.generated.IconAnchor
import com.mapbox.maps.extension.style.sources.addSource
import com.mapbox.maps.extension.style.sources.generated.geoJsonSource
import com.mapbox.navigation.core.lifecycle.MapboxNavigationApp
import com.mapbox.navigation.core.trip.session.TripSessionState
import com.ncs.breeze.App
import com.ncs.breeze.BuildConfig
import com.ncs.breeze.R
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.Event
import com.ncs.breeze.common.event.ToAppRx
import com.ncs.breeze.common.extensions.android.getApp
import com.ncs.breeze.common.extensions.android.getAppPreference
import com.ncs.breeze.common.extensions.android.getOBUConnHelper
import com.ncs.breeze.common.helper.obu.OBUAlertHelper
import com.ncs.breeze.common.model.rx.ToAppRxEvent
import com.ncs.breeze.common.remote.MyServiceInterceptor
import com.ncs.breeze.common.storage.AppPrefsKey
import com.ncs.breeze.common.storage.BreezeUserPreference
import com.ncs.breeze.common.utils.BreezeUserPreferenceUtil
import com.breeze.model.constants.Constants
import com.breeze.model.enums.TrafficIncidentData
import com.ncs.breeze.common.appSync.AppSyncHelper
import com.ncs.breeze.common.utils.AWSUtils
import com.ncs.breeze.common.utils.DialogFactory
import com.ncs.breeze.reactnative.nativemodule.ReactNativeEventEmitter
import com.ncs.breeze.ui.login.activity.LoginActivity
import com.ncs.breeze.ui.maintenance.GPSTurnedOffBottomSheet
import com.ncs.breeze.ui.rn.CustomReactFragment
import com.ncs.breeze.ui.splash.SplashActivity
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import sg.gov.lta.obu.sdk.core.types.OBUError
import timber.log.Timber
import javax.inject.Inject


abstract class BaseActivity<VB : ViewBinding, V : BaseViewModel> : AppCompatActivity(),
    HasAndroidInjector,
    DefaultHardwareBackBtnHandler {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Any>

    @Inject
    lateinit var userPreferenceUtil: BreezeUserPreferenceUtil

    @Inject
    lateinit var myServiceInterceptor: MyServiceInterceptor

    private var mViewModel: V? = null
    lateinit var viewBinding: VB

    private var mReactInstanceManager: ReactInstanceManager? = null

    val obuAlertHelper by lazy { OBUAlertHelper(this) }

    /**
     * Override for set view model
     *
     * @return view model instance
     */

    abstract fun getViewModelReference(): V

    protected abstract fun inflateViewBinding(): VB

    private val carEventListener =
        ToAppRx.listenEvent(ToAppRxEvent::class.java)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                logAndroidAuto("event in base screen ")
                onEventFromCar(it)
            }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //handleGlobalException()
        setStatusBar()

        this.mViewModel = if (mViewModel == null) getViewModelReference() else mViewModel
        viewBinding = inflateViewBinding()
        setContentView(viewBinding.root)

        if (BuildConfig.DEBUG) {
            val testToken = getAppPreference()?.getString(AppPrefsKey.PUSH_MESSAGING_TOKEN)
            Timber.d("FCMID ${testToken ?: "(null)"}")
        }

        compositeDisposable.add(carEventListener)

        mReactInstanceManager =
            (application as App).reactNativeHost.reactInstanceManager
    }

    private fun showOBUErrorMessage(error: OBUError) {
        obuAlertHelper.showErrorAlert(error)
    }

    //FIXME: catching the error will report the crash as non fatal in crash anamytics
    private fun handleGlobalException() {
        Thread.setDefaultUncaughtExceptionHandler { paramThread, paramThrowable -> //Catch your exception
            Firebase.crashlytics.recordException(paramThrowable)
            Timber.e("---- Found an unhandled exception ----")
            Timber.e(paramThrowable)
            System.exit(2)
        }
    }

    override fun onStart() {
        super.onStart()
        getOBUConnHelper()?.registerConnectionStatusListener(this)
    }

    override fun onStop() {
        super.onStop()
        getOBUConnHelper()?.unregisterConnectionStatusListener(this)
    }

    /*fun performDependencyInjection() {
        AndroidInjection.inject(this)
    }*/

    override fun androidInjector(): AndroidInjector<Any> = dispatchingAndroidInjector

    fun checkForUpdates() {
        validateAndCheckConfigForUpdates(object : UpdateEventHandler {
            override fun handleMaintenance(data: RemoteConfigParam) {
                val intent = Intent(this@BaseActivity, SplashActivity::class.java)
                intent.putExtra(
                    Constants.MAINTAINENCE_MODE_ENABLED,
                    true
                )
                intent.putExtra(
                    Constants.MAINTAINENCE_MODE_MESSAGE,
                    data.maintenanceMessage
                )
                replaceActivity(intent)
            }

            override fun handleSkip() {
            }

            override fun handleExit() {
                restartApp()
            }

            override fun handleAppUpToDate() {
                showAppUptoDateDialog()
            }
        })
    }

    fun restartApp() {
        val intent = Intent(this, SplashActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
    }

    fun openLoginScreen(mode: Int = -1) {
        val intent = Intent(this, LoginActivity::class.java).apply {
            this.putExtra(LoginActivity.KEY_MODE_LOGIN_TYPE, mode)
        }

        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        finish()
    }

    fun setStatusBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            var flags = window.decorView.systemUiVisibility // get current flag
            flags = flags or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR // add LIGHT_STATUS_BAR to flag
            window.decorView.systemUiVisibility = flags
            window.statusBarColor = ContextCompat.getColor(this, R.color.breeze_bg) // optional
        }
    }


    fun setLoginActivityStatusBar() {
        window.apply {
            clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            var flags = window.decorView.systemUiVisibility // get current flag
            flags =
                flags or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN// add LIGHT_STATUS_BAR to flag
            window.decorView.systemUiVisibility = flags
            //decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            statusBarColor = Color.TRANSPARENT
        }
    }

    fun View.setMarginTop(marginTop: Int) {
        val menuLayoutParams = this.layoutParams as ViewGroup.MarginLayoutParams
        menuLayoutParams.setMargins(0, marginTop, 0, 0)
        this.layoutParams = menuLayoutParams
    }

    fun hideKeyboard() {
        val view: View = findViewById(android.R.id.content)
        if (view != null) {
            val imm = this.getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    fun showKeyboard(view: View) {
        if (view != null) {
            val imm = this.getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)
        }
    }

    override fun onPostResume() {
        super.onPostResume()
    }

    override fun dispatchTouchEvent(
        ev: MotionEvent?
    ): Boolean {
//        InstabugTrackingDelegate
//            .notifyActivityGotTouchEvent(ev, this)
        return super
            .dispatchTouchEvent(ev)
    }

    fun getMapboxStyle(basicMapRequired: Boolean): String {
        return if (basicMapRequired) {
            getMapboxBasicStyleURL()
        } else {
            getMapboxStyleURL()
        }
    }

    private fun getMapboxStyleURL(): String {
        when (resources.configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK) {
            Configuration.UI_MODE_NIGHT_NO -> {
                //dark_mode.setText("Disabled")
                return BuildConfig.MAPBOX_STYLE_URL_LIGHT
            }

            Configuration.UI_MODE_NIGHT_YES -> {
                //dark_mode.setText("Enabled")
                return BuildConfig.MAPBOX_STYLE_URL_DARK
            }
        }
        return BuildConfig.MAPBOX_STYLE_URL_LIGHT
    }

    private fun getMapboxBasicStyleURL(): String {
        when (resources.configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK) {
            Configuration.UI_MODE_NIGHT_NO -> {
                //dark_mode.setText("Disabled")
                return BuildConfig.MAPBOX_BASIC_STYLE_URL_LIGHT
            }

            Configuration.UI_MODE_NIGHT_YES -> {
                //dark_mode.setText("Enabled")
                return BuildConfig.MAPBOX_BASIC_STYLE_URL_DARK
            }
        }
        return BuildConfig.MAPBOX_BASIC_STYLE_URL_DARK
    }


    fun displayIncidentData(style: Style, incidentMap: HashMap<String, FeatureCollection>) {

        //var userIncidentPreference = userPreferenceUtil.getIncidentSettings()

        // Display Accident Data
        TrafficIncidentData.values().forEach {

            val layerId = it.type + Constants.LAYER
            val sourceId = it.type + Constants.SOURCE
            val imgId = it.type + Constants.IMAGE

            style.removeStyleLayer(layerId)
            style.removeStyleSource(sourceId)

            if (incidentMap.get(it.type) != null) {
                addIncidentSymbolLayer(
                    style,
                    sourceId,
                    layerId,
                    imgId,
                    incidentMap.get(it.type)!!
                )
            }
        }
    }

    private fun addIncidentSymbolLayer(
        style: Style,
        sourceName: String,
        layerName: String,
        imgName: String,
        collection: FeatureCollection
    ) {
        var icon_size_exp = Expression.interpolate {
            linear()
            zoom()
            stop {
                literal(0.0)
                literal(0.0)
            }
            stop {
                literal(9.0)
                literal(0.0)
            }
            stop {
                literal(12.9)
                literal(0.0)
            }
            stop {
                literal(13.0)
                literal(0.8)
            }
            stop {
                literal(16.5)
                literal(1.0)
            }
        }

        style.addSource(
            geoJsonSource(sourceName) {
                featureCollection(collection)
            }
        )

        if (style.getLayer(Constants.ILLEGAL_PARKING_CAMERA) != null) {
            style.addLayerAbove(
                symbolLayer(layerName, sourceName) {
                    iconImage(
                        imgName
                    )
                    iconSize(icon_size_exp)
                    iconAnchor(IconAnchor.CENTER)
                    iconAllowOverlap(true)
                }, Constants.ILLEGAL_PARKING_CAMERA
            )
        } else {
            style.addLayer(
                symbolLayer(layerName, sourceName) {
                    iconImage(
                        imgName
                    )
                    iconSize(icon_size_exp)
                    iconAnchor(IconAnchor.CENTER)
                    iconAllowOverlap(true)
                }
            )
        }
    }

//    fun displayERP(style: Style, erpFeatures: ERPFeatures) {
//
//        //Removing all the source and layers relaated to ERP
//        style.removeStyleLayer(Constants.ERP_LAYER)
//        //style.removeStyleLayer(NO_RATES_ERP_LAYER)
//        style.removeStyleSource(Constants.ERP_GEOJSONSOURCE)
//        //style.removeStyleSource(NO_RATES_ERP_GEOJSONSOURCE)
//
//        var erpFeatureCollection = FeatureCollection.fromFeatures(erpFeatures.featureListERP)
//
//
//        var icon_size_exp = Expression.interpolate {
//            linear()
//            zoom()
//            stop {
//                literal(0.0)
//                literal(0.0)
//            }
//            stop {
//                literal(9.0)
//                literal(0.0)
//            }
//            stop {
//                literal(12.9)
//                literal(0.0)
//            }
//            stop {
//                literal(13.0)
//                literal(0.8)
//            }
//            stop {
//                literal(16.4)
//                literal(1.0)
//            }
//        }
//
//        var text_size_exp = Expression.interpolate {
//            linear()
//            zoom()
//            stop {
//                literal(0.0)
//                literal(0.0)
//            }
//            stop {
//                literal(9.0)
//                literal(0.0)
//            }
//            stop {
//                literal(11.0)
//                literal(0.0)
//            }
//            stop {
//                literal(13.0)
//                literal(14.0)
//            }
//            stop {
//                literal(16.4)
//                literal(18.0)
//            }
//        }
//
//        style.addSource(
//            geoJsonSource(Constants.ERP_GEOJSONSOURCE) {
//                featureCollection(erpFeatureCollection!!)
//                //maxzoom(14)
//            }
//        )
//
//        style.addLayer(
//            symbolLayer(Constants.ERP_LAYER, Constants.ERP_GEOJSONSOURCE) {
//                iconImage(
//                    Constants.ERP_ICON
//                )
//                iconSize(icon_size_exp)
//                iconAnchor(IconAnchor.BOTTOM)
//                iconAllowOverlap(true)
//
//                textField(Expression.get(Constants.ERP.ERP_RATE))
//                textColor(getColor(R.color.themed_erp_price_text))
//                textAnchor(TextAnchor.CENTER)
//                textOffset(listOf(0.0, -1.4))
//                textSize(text_size_exp)
//                iconIgnorePlacement(true)
//                //textAllowOverlap(true)
//            }
//        )
//
//
//        /*var noRatesERPFeatureCollection = FeatureCollection.fromFeatures(erpFeatures.featureListERPNoCharge)
//
//        style.addSource(
//            geoJsonSource(NO_RATES_ERP_GEOJSONSOURCE) {
//                featureCollection(noRatesERPFeatureCollection!!)
//            }
//        )
//
//        style.addLayerAbove(
//            symbolLayer(NO_RATES_ERP_LAYER, NO_RATES_ERP_GEOJSONSOURCE) {
//                iconImage(
//                    NO_RATES_ERP_ICON
//                )
//                iconSize(icon_size_exp)
//                iconAnchor(IconAnchor.CENTER)
//                iconAllowOverlap(true)
//            }, ILLEGAL_PARKING_CAMERA
//        )*/
//    }


    //Update Dialog


    interface UpdateEventHandler {
        fun handleMaintenance(data: RemoteConfigParam)
        fun handleSkip()
        fun handleExit(): Unit
        fun handleAppUpToDate()
    }

    protected fun checkConfig(
        updateEventHandler: UpdateEventHandler,
        showOptionalDialog: Boolean,
        isDirectlyFromNotification: Boolean
    ) {
        Timber.d("AppUpdate: -- checkConfig isDirectlyFromNotification = $isDirectlyFromNotification, showOptionalDialog = $showOptionalDialog --")
        RemoteConfigUtils.fetchConfigurations(this, object : RemoteConfigUtils.ConfigLoadListener {
            override fun onConfigLoadSuccess(data: RemoteConfigParam) {
                Timber.d("AppUpdate: -- onConfigLoadSuccess:: data = $data --")
                if (data.isMaintenance) {
                    Timber.d("AppUpdate: -- App under maintenance.. --")
                    updateEventHandler.handleMaintenance(data)
                } else if (MapboxNavigationApp.current()
                        ?.getTripSessionState() == TripSessionState.STARTED
                ) {
                    Timber.d("AppUpdate: -- Trip is running, skipping version check now.. --")
                    updateEventHandler.handleSkip()
                } else {
                    val currentVersion = com.ncs.breeze.BuildConfig.VERSION_CODE
                    Timber.d("AppUpdate: -- currentVersion( $currentVersion ) --")
                    Timber.d("AppUpdate: -- minBuildVersion( ${data.minBuildVersion} ) --")
                    Timber.d("AppUpdate: -- latestBuildVersion( ${data.latestBuildVersion} ) --")
                    if (currentVersion < data.minBuildVersion) {
                        showForceUpdateDialog(data.downloadLink, data.mandatoryRelogin)
                        {
                            updateEventHandler.handleExit()
                        }
                    } else if (currentVersion < data.latestBuildVersion) {
                        Timber.d(
                            "AppUpdate: -- shouldShowOptionsUpdateDialog( ${
                                shouldShowOptionsUpdateDialog(
                                    data
                                )
                            } ) --"
                        )
                        if (showOptionalDialog || shouldShowOptionsUpdateDialog(data)) {
                            //"We do not have force logout for optional updates" - Design team
                            showOptionalUpdateDialog(data.downloadLink, false)
                            {
                                updateEventHandler.handleSkip()
                            }
                        } else {
                            updateEventHandler.handleSkip()
                        }
                    } else {
                        Timber.d("AppUpdate: -- Versions seems okay, proceeding next step now --")
                        getAppPreference()?.saveInt(AppPrefsKey.OPTIONAL_VERSION_CHECK_ATTEMPS, 0)
                        if (isDirectlyFromNotification) {
                            updateEventHandler.handleAppUpToDate()
                        } else {
                            updateEventHandler.handleSkip()
                        }
                    }
                }
            }

            override fun onConfigLoadFailed() {
                Timber.d("AppUpdate: -- onConfigLoadFailed() --")
                updateEventHandler.handleSkip()
            }
        })
    }


    protected fun showOptionalUpdateDialog(
        downloadUrl: String,
        mandatoryRelogin: Boolean,
        handleSkip: () -> Unit
    ) {
        Timber.d("AppUpdate: -- showOptionalUpdateDialog() --")
        if (!lifecycle.currentState.isAtLeast(Lifecycle.State.STARTED)) {
            return
        }
        val remindButton = if (totalVersionCheckCount() == 0) {
            getString(R.string.update_dialog_later)
        } else {
            getString(R.string.update_dialog_dismiss)
        }

        DialogFactory.createChoiceDialog(
            this,
            getString(R.string.update_dialog_title),
            getString(R.string.update_dialog_message),
            getString(R.string.update_dialog_install_click),
            remindButton,
            {
                Analytics.logPopupEvent(Event.NORMAL_UPDATE, Event.INSTALL_NOW, "")
                if (mandatoryRelogin) {
                    AppSyncHelper.unSubscribeAll {
                        logoutApp {
                            handleSkip()
                            openUrl(downloadUrl)
                        }
                    }
                } else {
                    handleSkip()
                    openUrl(downloadUrl)
                }
            },
            {
                if (totalVersionCheckCount() == 0) {
                    Analytics.logPopupEvent(Event.NORMAL_UPDATE, Event.LATER, "")
                } else {
                    Analytics.logPopupEvent(Event.NORMAL_UPDATE, Event.DISMISS, "")
                }
                getAppPreference()?.saveLong(
                    AppPrefsKey.LAST_VERSION_CHECK,
                    System.currentTimeMillis()
                )
                getAppPreference()?.saveInt(
                    AppPrefsKey.OPTIONAL_VERSION_CHECK_ATTEMPS,
                    totalVersionCheckCount() + 1
                )
                handleSkip()
            },
            false,
            null
        ).show()
    }


    protected fun showAppUptoDateDialog() {
        if (lifecycle.currentState.isAtLeast(Lifecycle.State.STARTED)) {
            DialogFactory.createSingleChoiceDialog(
                this,
                getString(R.string.update_dialog_title_up_to_date),
                getString(R.string.update_dialog_message_up_to_date),
                getString(R.string.update_dialog_up_to_date_click),
                {
                    Analytics.logPopupEvent(Event.LATEST_VERSION_UPDATED, Event.DISMISS, "")
                },
                true,
                null
            ).show()
        }
    }

    protected fun showForceUpdateDialog(
        downloadUrl: String,
        mandatoryRelogin: Boolean,
        handleExit: () -> Unit
    ) {
        if (lifecycle.currentState.isAtLeast(Lifecycle.State.STARTED)) {
            DialogFactory.createSingleChoiceDialog(
                this,
                getString(R.string.update_dialog_title_force),
                getString(R.string.update_dialog_message_force),
                getString(R.string.update_dialog_update_click),
                {
                    Analytics.logPopupEvent(Event.MANDATORY_UPDATE, Event.UPDATE_NOW, "")
                    if (mandatoryRelogin) {
                        AppSyncHelper.unSubscribeAll {
                            logoutApp {
                                handleExit()
                                openUrl(downloadUrl)
                            }
                        }
                    } else {
                        handleExit()
                        openUrl(downloadUrl)
                    }
                },
                false,
                null
            ).show()
        }
    }

    fun logoutApp(callback: () -> Unit) {
        AWSUtils.signOut {
            handleLogoutSuccess {
                callback.invoke()
            }
        }
    }

    open fun handleLogoutSuccess(callback: () -> Unit) {
        getApp()?.destroyNavigationObservers()
        getAppPreference()?.saveBoolean(AppPrefsKey.CLEAR_FORCE_SESSION_NEXT_LAUNCH, false)
        BreezeUserPreference.getInstance(this)
            .clearAllSharedPreferences(BreezeUserPreference.DEFAULT_CLEAR_EXCLUSIONS)
        ReactNativeEventEmitter.sendEvent(application = application, "logout", null)
        getApp()?.obuConnectionHelper?.disconnectOBU()
        callback.invoke()
    }

    private fun openUrl(downloadUrl: String) {
        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(downloadUrl))
        startActivity(browserIntent)
    }

    private fun totalVersionCheckCount(): Int {
        return getAppPreference()?.getInt(AppPrefsKey.OPTIONAL_VERSION_CHECK_ATTEMPS) ?: 0
    }

    private fun lastVersionCheck(): Long {
        return getAppPreference()?.getLong(AppPrefsKey.LAST_VERSION_CHECK) ?: 0
    }

    protected fun shouldShowOptionsUpdateDialog(data: RemoteConfigParam): Boolean {
        val nextVersionCheckTime =
            lastVersionCheck() + (data.optionalUpdateCheckDurationHrs * 60 * 60 * 1000)
        return System.currentTimeMillis() > nextVersionCheckTime &&
                totalVersionCheckCount() < data.optionalUpdateCheckTrials
    }

    protected fun canOptionalUpdateRetry(currentVersion: Int, data: RemoteConfigParam): Boolean {
        return (currentVersion < data.latestBuildVersion)
                && totalVersionCheckCount() >= data.optionalUpdateCheckTrials
    }

    protected fun validateAndCheckConfigForUpdates(updateHandler: UpdateEventHandler) {

        if (RemoteConfigUtils.forceUpdate) {
            RemoteConfigUtils.forceUpdate = false
            checkConfig(updateHandler, true, RemoteConfigUtils.isDirectlyFromNotification)
            RemoteConfigUtils.isDirectlyFromNotification = false
            return
        }

        if (RemoteConfigUtils.updateCheckDurationHrs == null) {
            checkConfig(updateHandler, false, RemoteConfigUtils.isDirectlyFromNotification)
            RemoteConfigUtils.isDirectlyFromNotification = false
        } else {
            val nextVersionCheckTime =
                lastVersionCheck() + (RemoteConfigUtils.updateCheckDurationHrs!! * 60 * 60 * 1000)
            if (System.currentTimeMillis() > nextVersionCheckTime) {
                checkConfig(updateHandler, false, RemoteConfigUtils.isDirectlyFromNotification)
            }
            RemoteConfigUtils.isDirectlyFromNotification = false
        }
    }


    //End of Update Dialog


    open fun addFragment(fragment: Fragment, bundle: Bundle?, tag: String) {
        runOnUiThread {
            bundle?.let { fragment.arguments = it }
            getFragmentContainer()?.let {
                this.supportFragmentManager.beginTransaction()
                    .setCustomAnimations(R.anim.slide_right_in, -1, -1, R.anim.slide_right_out)
                    .add(it, fragment, tag)
                    .addToBackStack(tag)
                    .commitAllowingStateLoss()
            }
        }
    }

    open fun removeAllFragmentsFromBackStack() {
        for (i in 0 until this.supportFragmentManager.getBackStackEntryCount()) {
            this.supportFragmentManager.popBackStack()
        }
    }

    open fun addFragmentBottomUp(fragment: Fragment, bundle: Bundle?, tag: String) {
        fragment.arguments = bundle
        getFragmentContainer()?.let {
            this.supportFragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.slide_fragment_up, -1, -1, R.anim.slide_fragment_down)
                .add(it, fragment, tag)
                .addToBackStack(tag)
                .commitAllowingStateLoss()
        }
    }

    abstract fun getFragmentContainer(): Int?

    interface DeviceBackPressedListener {
        fun onDeviceBackPressed()
    }


    override fun invokeDefaultOnBackPressed() {
        super.onBackPressed()
        //supportFragmentManager.popBackStack()
    }

    override fun onBackPressed() {
        if (mReactInstanceManager != null) {
            if (getFragmentContainer() != null) {
                val fr = supportFragmentManager.findFragmentById(getFragmentContainer()!!)
                if (fr is ReactFragment || fr is CustomReactFragment) {
                    mReactInstanceManager!!.onBackPressed()
                } else {
                    super.onBackPressed()
                }
            } else {
                /*if (supportFragmentManager.backStackEntryCount == 0) {
                    this.finish()
                } else {
                    mReactInstanceManager!!.onBackPressed()
                    //super.onBackPressed()
                }*/
            }
        } else {
            super.onBackPressed()
        }

    }

    protected fun launchActivity(newIntent: Intent) {
        getIntent().extras?.let {
            removeCrashingParams(it)
            newIntent.putExtras(it)
        }
        startActivity(newIntent)
    }

    protected fun replaceActivity(newIntent: Intent) {
        launchActivity(newIntent)
        finish()
    }

    private fun removeCrashingParams(extras: Bundle) {
        if (extras.containsKey("profile")) {
            extras.remove("profile")
        }
    }

    open var compositeDisposable: CompositeDisposable = CompositeDisposable()

    /**
     * handle event from car
     */
    open fun onEventFromCar(event: ToAppRxEvent) {

    }

    fun showGPSTurnedOffBottomSheet(onDismiss: () -> Unit = {}) {
        val gpsTurnedOffBottomSheet =
            supportFragmentManager.findFragmentByTag(GPSTurnedOffBottomSheet.TAG) as? GPSTurnedOffBottomSheet
        if (gpsTurnedOffBottomSheet == null && lifecycle.currentState.isAtLeast(Lifecycle.State.STARTED)) {
            kotlin.runCatching {
                GPSTurnedOffBottomSheet().apply {
                    isCancelable = false
                    dialog?.setOnDismissListener { onDismiss.invoke() }
                }.show(supportFragmentManager, GPSTurnedOffBottomSheet.TAG)
            }.onFailure {
                Firebase.crashlytics.log("Breeze showGPSTurnedOffBottomSheet: ${it.message}")
            }
        }
    }

    fun closeGPSTurnedOffBottomSheet() {
        lifecycleScope.launchWhenResumed {
            (supportFragmentManager.findFragmentByTag(GPSTurnedOffBottomSheet.TAG) as? GPSTurnedOffBottomSheet)?.let { gpsTurnedOffDialog ->
                gpsTurnedOffDialog.dismissAllowingStateLoss()
                supportFragmentManager.beginTransaction().remove(gpsTurnedOffDialog).commitNow()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
//        /**
//         * post event for logout
//         */
//        RxToCarRxEvent.postEventToCar(AppEventLogout())

        compositeDisposable.remove(carEventListener)
        compositeDisposable.dispose()
    }


    enum class EventSource {
        CAR_APP,
        NONE
    }
}
package com.ncs.breeze.car.breeze.screen.obustatus

import androidx.car.app.CarContext
import androidx.car.app.model.Action
import androidx.car.app.model.CarColor
import androidx.car.app.model.CarIcon
import androidx.car.app.model.MessageTemplate
import androidx.core.graphics.drawable.IconCompat
import com.ncs.breeze.common.model.rx.AppToPhoneReportIssueEvent
import com.ncs.breeze.App
import com.ncs.breeze.R
import com.ncs.breeze.car.breeze.utils.LimitClick
import com.ncs.breeze.car.breeze.utils.setClickSafe
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.Event
import com.ncs.breeze.common.event.ToAppRx
import com.ncs.breeze.common.extensions.android.getUserDataPreference
import com.ncs.breeze.common.helper.obu.OBUDataHelper

class OBUConnectionStatusScreenTemplateFactory(val appContext: App) {

    fun createConnectedTemplate(carContext: CarContext): MessageTemplate {
        val obuDevice = appContext.getUserDataPreference()?.getLastConnectedOBUDevice()
        val cardBalance = OBUDataHelper.getOBUCardBalanceSGD()
        val message = appContext.getString(
            R.string.obu_connected_content,
            obuDevice?.getDisplayedVehicleName() ?: ""
        )

        return MessageTemplate.Builder(message)
            .setHeaderAction(Action.BACK)
            .setIcon(
                CarIcon.Builder(
                IconCompat.createWithResource(
                    carContext,
                    R.drawable.ic_aa_car
                )
            ).build())
            .setTitle(appContext.getString(R.string.obu_on_breeze))
            .build()
    }

    fun createNoDevicePairedTemplate(onBack: () -> Unit): MessageTemplate =
        MessageTemplate.Builder(appContext.getString(R.string.obu_not_paired_content))
            .setTitle(appContext.getString(R.string.obu_on_breeze))
            .setHeaderAction(Action.BACK)
            .addAction(
                Action.Builder()
                    .setTitle(appContext.getString(R.string.button_okay))
                    .setBackgroundColor(
                        CarColor.createCustom(
                            appContext.getColor(R.color.breeze_primary),
                            appContext.getColor(R.color.breeze_primary)
                        )
                    )
                    .setClickSafe {
                        LimitClick.handleSafe(onBack) }
                    .build()
            ).build()

    fun createRetryErrorTemplate(screenName: String): MessageTemplate =
        MessageTemplate.Builder(appContext.getString(R.string.obu_connection_error_content))
            .setHeaderAction(Action.BACK)
            .addAction(
                Action.Builder()
                    .setTitle(appContext.getString(R.string.obu_connection_error_report_issue))
                    .setBackgroundColor(
                        CarColor.createCustom(
                            appContext.getColor(R.color.breeze_primary),
                            appContext.getColor(R.color.breeze_primary)
                        )
                    )
                    .setClickSafe {
                        LimitClick.handleSafe {
                            Analytics.logClickEvent(
                                Event.AA_POPUP_REPORT_ISSUE_REPORT_ISSUE,
                                screenName
                            )
                            ToAppRx.postEvent(AppToPhoneReportIssueEvent())
                        }
                    }.build()
            )
            .setTitle(appContext.getString(R.string.obu_on_breeze))
            .build()

    fun createNotConnectTemplate(onRetry: () -> Unit) =
        MessageTemplate.Builder(appContext.getString(R.string.obu_not_connected_content))
            .setHeaderAction(Action.BACK)
            .addAction(
                Action.Builder()
                    .setTitle(appContext.getString(R.string.obu_not_connected_retry))
                    .setBackgroundColor(
                        CarColor.createCustom(
                            appContext.getColor(R.color.breeze_primary),
                            appContext.getColor(R.color.breeze_primary)
                        )
                    )
                    .setClickSafe {
                        LimitClick.handleSafe(onRetry)
                    }.build()
            )
            .setTitle(appContext.getString(R.string.obu_on_breeze))
            .build()

    fun createRetryingTemplate() = MessageTemplate.Builder("Retrying")
        .setHeaderAction(Action.BACK)
        .setLoading(true)
        .setTitle(appContext.getString(R.string.obu_not_connected_retry))
        .build()
}
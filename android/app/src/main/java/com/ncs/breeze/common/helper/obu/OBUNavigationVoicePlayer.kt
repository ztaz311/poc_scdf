package com.ncs.breeze.common.helper.obu

import android.media.AudioAttributes
import android.media.MediaPlayer
import com.mapbox.navigation.core.lifecycle.MapboxNavigationApp
import com.mapbox.navigation.core.trip.session.TripSessionState
import com.ncs.breeze.App
import com.breeze.model.obu.OBUParkingAvailability
import com.breeze.model.obu.OBUTravelTimeData
import com.ncs.breeze.common.utils.VoiceInstructionsManager
import java.lang.ref.WeakReference

class OBUNavigationVoicePlayer(app: App) {
    private val appRef = WeakReference(app)
    private var currentAlertPlayer: MediaPlayer? = null

    fun playVoiceInstruction(instructionsText: String){
        VoiceInstructionsManager.getInstance()?.playVoiceInstructions(instructionsText)
    }

    fun playAlertSound() {
        kotlin.runCatching {
            if (currentAlertPlayer?.isPlaying == true) return
            if (MapboxNavigationApp.current()
                    ?.getTripSessionState() != TripSessionState.STARTED
            ) return
            appRef.get()?.assets?.openFd("obu_common_alert.wav")?.let { assetFileDescriptor ->

                val audioAttributes = AudioAttributes.Builder()
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .build()
                MediaPlayer().apply {
                    isLooping = false
                    setAudioAttributes(audioAttributes)
                    setDataSource(
                        assetFileDescriptor.fileDescriptor,
                        assetFileDescriptor.startOffset,
                        assetFileDescriptor.length
                    )
                    setOnCompletionListener { mp ->
                        mp.seekTo(0)
                        mp.stop()
                        mp.release()
                        currentAlertPlayer = null
                    }
                    prepare()
                }.also { mp ->
                    currentAlertPlayer = mp
                }.start()

            }
        }
    }

    fun playTravelEstimatedTimeAnnouncement(data: Array<OBUTravelTimeData>) {
        if (MapboxNavigationApp.current()?.getTripSessionState() != TripSessionState.STARTED) return
        data.filter { it.color.contentEquals("red", true) }
            .takeIf { it.isNotEmpty() }
            ?.let { names ->
                val announcementMessage = StringBuilder("Heavy traffic ahead. Expect delays. ")
                val separator = ", "
                val regex = "(mins|min|minute|minutes)".toRegex()
                names.joinToString(separator) { item ->
                    announcementMessage.append(
                        "${
                            item.estTime.replace(
                                regex,
                                ""
                            )
                        } minutes to ${item.name}"
                    )
                }
                VoiceInstructionsManager.getInstance()
                    ?.playVoiceInstructions(announcementMessage.toString())
            }
    }

    fun playParkingAvailabilityAnnouncement(data: Array<OBUParkingAvailability>, isConnectedToCar:Boolean) {
        if (data.isEmpty() || MapboxNavigationApp.current()
                ?.getTripSessionState() != TripSessionState.STARTED
        ) return
        val isAllAvailable = data.count { it.color == "green" } == data.size
        val isAllFull = data.count { it.availableLots == "0" } == data.size
        val isLimitedLotsAvailable =
            data.count { it.color == "red" || it.color == "yellow" } >= 3 || data.count { it.color == "red" || it.color == "yellow" } == data.size
        val isFillingUpFastWithAvailableLots =
            isLimitedLotsAvailable && data.any { it.color == "green" }
        val isNearbyStillAvailable =
            !isLimitedLotsAvailable && data.any { it.color == "green" }
        when {
            isAllAvailable -> {
                //BREEZE2-1114
                if (isConnectedToCar)
                    "Parking Update - Nearby car parks are all still available."
                else
                    "Parking Update - Nearby car parks are all still available. You can select one to drive there now."
            }
            isAllFull -> "Parking Update - Nearby car parks are all full. Expect to wait or park further away."
            isFillingUpFastWithAvailableLots -> {
                val names = data.filter { it.color == "green" }
                    .map { it.name }
                    .reduce { accumulator, name ->
                        "$accumulator${if (accumulator.isEmpty()) "" else ", "}$name"
                    }
                "Parking Update - Lots are still available at $names"
            }

            isNearbyStillAvailable -> {
                "Parking Update - Nearby Car parks are still available."
            }

            isLimitedLotsAvailable -> {
                "Parking Update - Car parks nearby are filling up fast. Limited lots available"
            }

            else -> ""

        }.takeIf { it.isNotEmpty() }?.let {
            VoiceInstructionsManager.getInstance()?.playVoiceInstructions(it)
        }
    }
}
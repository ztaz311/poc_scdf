package com.ncs.breeze.components.marker.markerview2

import android.animation.Animator
import android.app.Activity
import android.content.res.Resources
import android.view.View
import android.widget.LinearLayout
import com.facebook.react.bridge.Arguments
import com.google.android.gms.maps.model.LatLng
import com.mapbox.geojson.Point
import com.mapbox.maps.CameraOptions
import com.mapbox.maps.EdgeInsets
import com.mapbox.maps.MapboxMap
import com.mapbox.maps.plugin.animation.MapAnimationOptions
import com.mapbox.maps.plugin.animation.easeTo
import com.ncs.breeze.App
import com.ncs.breeze.R
import com.ncs.breeze.common.analytics.Screen
import com.breeze.model.extensions.visibleAmount
import com.breeze.model.api.response.ERPResponseData
import com.ncs.breeze.common.rxbus.RxBus
import com.ncs.breeze.common.rxbus.RxEvent
import com.ncs.breeze.common.utils.ERPControllerData
import com.ncs.breeze.common.utils.ErpDataExcute
import com.ncs.breeze.common.utils.Utils
import com.breeze.model.extensions.safeDouble
import com.ncs.breeze.databinding.ErpMarkerWindowBinding
import com.ncs.breeze.reactnative.nativemodule.ShareBusinessLogicEvent
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

//TODO: too many optional params, can refactor to erp options class
class ERpMarkerView constructor(
    latLng: LatLng,
    mapboxMap: MapboxMap,
    val activity: Activity,
    showInfoIcon: Boolean = false,
    val isDisableClickErpLayout: Boolean = true,
    val showMinimalView: Boolean = false
) : MarkerView2(mapboxMap) {

    private val pixelDensity = Resources.getSystem().displayMetrics.density
    var erpMarkerClickListener: ERPMarkerClickListener? = null

    private var isPriceLayoutVisible = true

    var compositeDisposable: CompositeDisposable = CompositeDisposable()

    var currentDataMarker: ERPResponseData.Erp? = null

    var currentState = 0


    companion object {
        val DEFAULT = 0
        var SELECTED = 1
        var KEEP_STATE = 2
    }

    val viewBinding = ErpMarkerWindowBinding.inflate(activity.layoutInflater, null, false)

    init {
        mLatLng = latLng

        viewBinding.imgInfo.visibility = if (showInfoIcon) View.VISIBLE else View.GONE

        mViewMarker = viewBinding.root
    }


    /**
     * this class for set state
     */
    fun setStateMarker(state_marker: Int) {
        currentState = state_marker
        when (currentState) {
            DEFAULT -> {
                defaultState()
            }

            SELECTED -> {
                selectedState()
            }

            KEEP_STATE -> {
                keepFocusState()
            }
        }
    }


    /**
     * state keep focus (select & focus)
     *
     * hide Tooltips
     * Change icon to selected
     */
    fun keepFocusState() {
        mViewMarker?.let {
            viewBinding.llToolTip.visibility = View.INVISIBLE
            viewBinding.tvPrice.visibility = View.INVISIBLE
        }
    }


    /**
     * state selected (select & focus)
     *
     * show underline
     * show Tooltips
     * Change icon to selected
     */
    fun selectedState() {
        mViewMarker?.let {
            viewBinding.llToolTip.visibility = View.VISIBLE
        }
    }


    /**
     * state default
     *
     * Hide underline
     * Hide Tooltips
     * Change icon to no selected
     * if(ischeapest) => show
     *
     */
    fun defaultState() {
        mViewMarker?.let {
            viewBinding.llToolTip.visibility = View.INVISIBLE
        }
    }


    private fun sendEventERPSelected(erpId: Int) {
        (activity.applicationContext as? App)?.shareBusinessLogicPackage?.getRequestsModule()
            ?.flatMap {
                it.callRN(
                    ShareBusinessLogicEvent.SEND_EVENT_SELECTED_ERP.eventName,
                    Arguments.createMap().apply {
                        putInt("erpid", erpId)
                    })
            }?.subscribeOn(Schedulers.io())?.observeOn(AndroidSchedulers.mainThread())?.subscribe()
    }

    fun fillDataToMarkerView(
        item: ERPResponseData.Erp,
        eRPIconClick: (Boolean) -> Unit,
        heightBottomSheet: Int,
        timeToCheckERP: Long,
        forceShowErp: Boolean = false,
        screenName: String,
        zoom: Double? = 15.0,
        shouldSendEventERPSelected: Boolean = true,
        forceCheckNextDayERP: Boolean,
        isTimeAlreadyChanged: Boolean = false
    ) {
        currentDataMarker = item
        /**
         * for set name erp
         */
        viewBinding.tvNameERP?.text = item.name

        val curentDataWithTimeERP = ERPControllerData.getDataERp(
            item.zoneid,
            timeToCheckERP,
            forceShowErp,
            forceCheckNextDayERP
        )

        excuteErpData(curentDataWithTimeERP, isTimeAlreadyChanged)

        viewBinding.imgInfo?.setOnClickListener {
            erpMarkerClickListener?.onERPInfoSelected()
        }

        viewBinding.erpIconLayout?.setOnClickListener {
            handleErpIconClick(
                item,
                screenName,
                heightBottomSheet,
                zoom,
                eRPIconClick,
                shouldSendEventERPSelected
            )
        }

        if (showMinimalView) {
            viewBinding.erpIconLayout.visibility = View.GONE
            viewBinding.erpMinimalIconLayout.visibility = View.VISIBLE
        } else {
            viewBinding.erpIconLayout.visibility = View.VISIBLE
            viewBinding.erpMinimalIconLayout.visibility = View.GONE
        }

    }

    private fun handleErpIconClick(
        item: ERPResponseData.Erp,
        screenName: String,
        heightBottomSheet: Int,
        zoom: Double?,
        eRPIconClick: (Boolean) -> Unit,
        shouldSendEventERPSelected: Boolean
    ) {
        if (!isDisableClickErpLayout) {
            currentDataMarker?.erpid?.let { erpID ->
                RxBus.publish(RxEvent.OpenErpDetail(erpID))
            }
        } else {
            if (shouldSendEventERPSelected) {
                sendEventERPSelected(item.erpid)
            }

            val edgeInset: EdgeInsets = if (Screen.ROUTE_PLANNING == screenName) {
                EdgeInsets(
                    10.0 * pixelDensity,
                    10.0 * pixelDensity,
                    100.0 * pixelDensity,
                    10.0 * pixelDensity
                )
            } else {
                EdgeInsets(
                    10.0 * pixelDensity,
                    10.0 * pixelDensity,
                    heightBottomSheet.toDouble() * pixelDensity,
                    10.0 * pixelDensity
                )
            }

            val cameraOptionsBuilder = CameraOptions.Builder()
                .center(Point.fromLngLat(item.startlong.safeDouble(), item.startlat.safeDouble()))
                .padding(edgeInset)

            zoom?.let {
                cameraOptionsBuilder.zoom(it)
            }
            val cameraOptions = cameraOptionsBuilder.build()

            mapboxMap.easeTo(
                cameraOptions,
                MapAnimationOptions.mapAnimationOptions {
                    duration(400L)
                    animatorListener(object : Animator.AnimatorListener {
                        override fun onAnimationStart(animation: Animator) {}

                        override fun onAnimationEnd(animation: Animator) {
                            when (currentState) {
                                SELECTED -> {
                                    eRPIconClick.invoke(false)
                                    setStateMarker(DEFAULT)
                                }

                                DEFAULT -> {
                                    eRPIconClick.invoke(true)
                                    setStateMarker(SELECTED)
                                }

                                else -> {
                                    eRPIconClick.invoke(true)
                                    setStateMarker(SELECTED)
                                }
                            }
                            viewBinding.root?.bringToFront()
                        }

                        override fun onAnimationCancel(animation: Animator) {}

                        override fun onAnimationRepeat(animation: Animator) {}

                    })
                }
            )
        }
    }

    fun stopReloadData() {
        compositeDisposable.clear()
    }

    private fun excuteErpData(curentDataWithTimeERP: ErpDataExcute, isTimeChanged: Boolean) {
        if (curentDataWithTimeERP.erpStatus == ERPControllerData.ACTIVE) {
            viewBinding.root.visibility = View.VISIBLE
            /**
             * setup logic showing erp decresing
             */
            setDataErpDecresing(curentDataWithTimeERP)

            setDataErpAmount(curentDataWithTimeERP)
            if (isTimeChanged) {
                viewBinding.tvNowtext?.text =
                    Utils.formatTimeWith12HFromHH(curentDataWithTimeERP.currentStartTime) + " - " + Utils.formatTimeWith12HFromHH(
                        curentDataWithTimeERP.currentEndTime
                    )
            }
        } else {
            viewBinding.root.visibility = View.GONE
        }
    }

    private fun setDataErpAmount(curentDataWithTimeERP: ErpDataExcute) {

        if (curentDataWithTimeERP.currntErpAmount > 0) {
            viewBinding.ivERP?.setImageResource(R.drawable.erp_off)
            viewBinding.imgStatusErp?.setImageResource(R.drawable.img_carpark_price)
        } else {
            viewBinding.ivERP?.setImageResource(R.drawable.ic_erp_not_active)
            viewBinding.imgStatusErp?.setImageResource(R.drawable.ic_price_erp_not_active)
        }
        viewBinding.tvPrice?.text = curentDataWithTimeERP.currntErpAmount.toDouble().visibleAmount()
        viewBinding.tvNow?.text = curentDataWithTimeERP.currntErpAmount.toDouble().visibleAmount()

        val nextERPAmount = curentDataWithTimeERP.nextErpAmount.toDouble()
        if (
            valid(curentDataWithTimeERP.strRangeNextTime) ||
            nextERPAmount > 0
        ) {
            viewBinding.sectionNextERPInfo.visibility = View.VISIBLE
            viewBinding.tvNextHourtext?.text = curentDataWithTimeERP.strRangeNextTime
            viewBinding.tvNextHour?.text = nextERPAmount.visibleAmount()
        } else {
            viewBinding.sectionNextERPInfo.visibility = View.GONE
        }
    }

    private fun setDataErpDecresing(curentDataWithTimeERP: ErpDataExcute) {
        if (curentDataWithTimeERP.isInOperation && curentDataWithTimeERP.minimumThresholdSatisfied) {
            viewBinding.ImgErpDecreasing.visibility = View.VISIBLE
            if (curentDataWithTimeERP.currntErpAmount == 0.0f || curentDataWithTimeERP.isPriceIncrease) {
                viewBinding.ImgErpDecreasing?.setImageResource(R.drawable.ic_increasing)
            } else {
                viewBinding.ImgErpDecreasing?.setImageResource(R.drawable.ic_decreasing)
            }
        } else {
            viewBinding.ImgErpDecreasing.visibility = View.GONE
        }
    }

    private fun valid(string: String?): Boolean {
        return string != null && string.trim().isNotEmpty()
    }

    override fun hideAllToolTips() {
        super.hideAllToolTips()
        if (viewBinding.llToolTip?.isShown == true) {
            viewBinding.llToolTip.visibility = View.INVISIBLE
        }
    }

    override fun hideMarker() {
        super.hideMarker()
        viewBinding.root.visibility = View.INVISIBLE
    }

    fun showTooltip() {
        if (viewBinding.llToolTip?.isShown == false) {
            viewBinding.llToolTip.visibility = View.VISIBLE
        }
    }

    override fun updateTooltipPosition() {
        mLatLng?.let { location ->
            val screenCoordinate = mapboxMap.pixelForCoordinate(
                Point.fromLngLat(
                    location.longitude,
                    location.latitude
                )
            )
            mViewMarker?.let {
                it.measure(
                    View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                    View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
                );
                it.x = screenCoordinate.x.toFloat() - (it.measuredWidth / 2)
                it.y = screenCoordinate.y.toFloat() - it.measuredHeight
            }
        }
    }

    override fun resetView() {
        mViewMarker?.let {
            val toolTip = it.findViewById<View>(R.id.ll_tool_tip)
            if (toolTip.isShown) {
                toolTip.visibility = View.INVISIBLE
            }
        }
    }

    interface ERPMarkerClickListener {
        fun onERPInfoSelected()
    }

}
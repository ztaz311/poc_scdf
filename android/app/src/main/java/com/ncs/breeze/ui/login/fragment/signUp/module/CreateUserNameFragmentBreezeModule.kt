package com.ncs.breeze.ui.login.fragment.signUp.module

import com.ncs.breeze.ui.login.fragment.signUp.view.CreateUsernameFragmentBreeze
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class CreateUserNameFragmentBreezeModule {
    @ContributesAndroidInjector
    abstract fun contributeCreateUserNameFragmentBreezeModule(): CreateUsernameFragmentBreeze
}
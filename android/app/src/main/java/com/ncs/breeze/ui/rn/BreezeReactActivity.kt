package com.ncs.breeze.ui.rn

/**
 * Created by aiswarya on 14,July,2021
 */

import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.view.View
import android.view.WindowManager
import com.breeze.model.constants.Constants
import com.facebook.react.BuildConfig
import com.facebook.react.ReactActivity
import com.facebook.react.ReactRootView
import com.facebook.react.modules.core.DefaultHardwareBackBtnHandler


class BreezeReactActivity : ReactActivity(), DefaultHardwareBackBtnHandler {

    private val OVERLAY_PERMISSION_REQ_CODE = 22
    private var mReactRootView: ReactRootView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStatusBar()
        mReactRootView = ReactRootView(this)

        if (intent.extras != null) {

            val initialProperties = intent.extras!!

            mReactRootView!!.startReactApplication(
                reactInstanceManager,
                Constants.RN_CONSTANTS.COMPONENT_NAME,
                initialProperties
            )
            setContentView(mReactRootView)

            // Add this
            prepareOverlayPermission()
        }
    }

    override fun invokeDefaultOnBackPressed() {
        super.onBackPressed()
    }

    override fun onPause() {
        super.onPause()
        if (reactInstanceManager != null) {
            reactInstanceManager!!.onHostPause(this)
        }
    }

    override fun onResume() {
        super.onResume()
        if (reactInstanceManager != null) {
            reactInstanceManager!!.onHostResume(this, this)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (reactInstanceManager != null) {
            reactInstanceManager!!.onHostDestroy(this)
        }
        if (mReactRootView != null) {
            mReactRootView!!.unmountReactApplication()
        }
    }

    override fun onBackPressed() {
        if (reactInstanceManager != null) {
            reactInstanceManager!!.onBackPressed()
        } else {
            super.onBackPressed()
        }
    }

    // Add this
    private fun prepareOverlayPermission() {
        if (!(BuildConfig.DEBUG && Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)) return
        if (!Settings.canDrawOverlays(this)) {
            val intent = Intent(
                Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                Uri.parse("package:$packageName")
            )
            startActivityForResult(intent, OVERLAY_PERMISSION_REQ_CODE)
        }
    }

    private fun setStatusBar() {
        window.apply {
            clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            var flags = window.decorView.systemUiVisibility // get current flag
            flags =
                flags or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR //or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN// add LIGHT_STATUS_BAR to flag
            window.decorView.systemUiVisibility = flags
            //decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            decorView.systemUiVisibility =
                View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            statusBarColor = Color.TRANSPARENT
        }
    }
}

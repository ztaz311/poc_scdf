package com.ncs.breeze.ui.dashboard.fragments.viewmodel

import android.app.Application
import com.ncs.breeze.common.remote.ApiHelper
import com.ncs.breeze.ui.base.BaseFragmentViewModel
import javax.inject.Inject

class DebugViewModel @Inject constructor(apiHelper: ApiHelper, application: Application) :
    BaseFragmentViewModel(
        application
    ) {
    var mAPIHelper: ApiHelper = apiHelper
}
package com.ncs.breeze.common.utils.walkathontracker

class WalkathonRequest {
    var userId: String = ""
    var latitude: String = ""
    var longitude: String = ""
    var walkathonId: String = ""
    var currentTime: String = ""
    var data: String = ""
}
package com.ncs.breeze.car.breeze.screen.amenity

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import androidx.activity.OnBackPressedCallback
import androidx.car.app.CarToast
import androidx.car.app.ScreenManager
import androidx.car.app.model.Action
import androidx.car.app.model.CarIcon
import androidx.car.app.model.ItemList
import androidx.car.app.model.Row
import androidx.car.app.navigation.model.PlaceListNavigationTemplate
import androidx.core.graphics.drawable.IconCompat
import com.breeze.model.SearchLocation
import com.breeze.model.annotation.AmenityTypeVal
import com.breeze.model.api.ErrorData
import com.breeze.model.api.request.AmenitieFilter
import com.breeze.model.api.request.AmenitiesRequest
import com.breeze.model.api.request.AmenityFilterTypeData
import com.breeze.model.api.response.amenities.BaseAmenity
import com.breeze.model.api.response.amenities.ResponseAmenities
import com.breeze.model.constants.AmenityType
import com.breeze.model.constants.Constants
import com.mapbox.android.core.location.LocationEngineCallback
import com.mapbox.android.core.location.LocationEngineProvider
import com.mapbox.android.core.location.LocationEngineResult
import com.mapbox.geojson.Point
import com.mapbox.navigation.utils.internal.toPoint
import com.ncs.breeze.car.breeze.MainBreezeCarContext
import com.ncs.breeze.car.breeze.base.BaseScreenCar
import com.ncs.breeze.car.breeze.screen.obulite.CarOBULiteManager
import com.ncs.breeze.car.breeze.screen.routeplanning.RoutePlanningScreen
import com.ncs.breeze.car.breeze.screen.routeplanning.RoutePreviewCarContext
import com.ncs.breeze.car.breeze.utils.BreezeCarUtil
import com.ncs.breeze.car.breeze.utils.CarConstants
import com.ncs.breeze.car.breeze.utils.LimitClick
import com.ncs.breeze.car.breeze.utils.setClickSafe
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.Event
import com.ncs.breeze.common.analytics.Screen
import com.ncs.breeze.common.extensions.android.getUserDataPreference
import com.ncs.breeze.common.model.rx.DataForRoutePreview
import com.ncs.breeze.common.remote.ApiObserver
import com.ncs.breeze.common.utils.OBUStripStateManager
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

/**
 * Show Nearby Petrol(s) or EV Charger(s)
 * */
class AmenityScreen(
    private val mainBreezeCarContext: MainBreezeCarContext,
    @AmenityTypeVal private val amenityType: String,
    private val fromScreen: String? = null
) : BaseScreenCar(mainBreezeCarContext.carContext) {

    private var nearbyAmenities = arrayListOf<BaseAmenity>()
    private var isLoadingAmenities = true
    private val amenityLayerRenderer = AmenityLayerRenderer(amenityType)

    private fun toastNoAmenitiesNearby() {
        CarToast.makeText(
            carContext, when (amenityType) {
                AmenityType.PETROL -> "No petrol stations nearby"
                else -> "No EV charging points nearby"
            }, CarToast.LENGTH_LONG
        ).show()
    }

    override fun getScreenName() = "[androidauto_search]"

    override fun onCreateScreen() {
        super.onCreateScreen()
        carContext.onBackPressedDispatcher.addCallback({ lifecycle }, object :
            OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                Analytics.logClickEvent(
                    "[nearby_${if (amenityType == AmenityType.PETROL) "petrol" else "ev"}]:back",
                    "[androidauto_search]"
                )
                screenManager.pop()
            }
        })
    }

    @SuppressLint("MissingPermission")
    private fun loadNearbyAmenities() {
        LocationEngineProvider.getBestLocationEngine(carContext)
            .getLastLocation(object :
                LocationEngineCallback<LocationEngineResult> {
                override fun onSuccess(result: LocationEngineResult?) {
                    val locationPoint = result?.lastLocation?.toPoint()
                    if (locationPoint != null) {
                        fetchNearbyAmenities(locationPoint) {
                            reloadScreenTemplate()
                        }
                    } else {
                        reloadScreenTemplate()
                    }
                }

                override fun onFailure(e: Exception) {
                    Timber.e("on getting user location failure: $e")
                    reloadScreenTemplate()
                }
            })

    }

    private fun reloadScreenTemplate() {
        isLoadingAmenities = false
        if (nearbyAmenities.isNotEmpty()) {
            amenityLayerRenderer.refreshAmenityData(nearbyAmenities)
        } else {
            toastNoAmenitiesNearby()
        }
        invalidate()
    }


    private fun fetchNearbyAmenities(location: Point, onDone: () -> Unit) {
        val amenitiesRequest = AmenitiesRequest(
            lat = location.latitude(),
            long = location.longitude(),
            rad = carContext.getUserDataPreference()?.getEvPetrolRange()?.div(1000.0)
                ?: Constants.DEFAULT_PETROL_EV_RADIUS,
            maxRad = carContext.getUserDataPreference()?.getEvPetrolMaxRange()?.div(1000.0)
                ?: Constants.DEFAULT_PETROL_EV_MAX_RADIUS,
            resultCount = carContext.getUserDataPreference()?.getEvPetrolResultCount()
                ?: Constants.DEFAULT_PETROL_EV_RESULT_COUNT
        ).apply {
            setListType(listOf(amenityType))
            generateAmenitiesFilter()
                ?.takeUnless { it.isEmpty() }
                ?.let { filter -> listAmenities = filter }
        }
        BreezeCarUtil.apiHelper.getAmenities(amenitiesRequest)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : ApiObserver<ResponseAmenities>(compositeDisposable) {
                override fun onSuccess(data: ResponseAmenities) {
                    data.amenities.find { it.type == amenityType }?.let { amenity ->
                        nearbyAmenities.clear()
                        amenity.data.forEach {
                            it.amenityType = amenity.type!!
                            it.iconUrl = amenity.iconUrl
                            nearbyAmenities.add(it)
                        }
                        onDone.invoke()
                    }
                }

                override fun onError(e: ErrorData) {
                    Timber.d("AmenityLayerRenderer on fetch $amenityType error: $e")
                }
            })
    }

    private fun generateAmenitiesFilter(): ArrayList<AmenitieFilter>? {
        if (amenityType == AmenityType.PETROL) {
            return carContext.getUserDataPreference()?.getPetrolAmenityFilter()
                ?.takeIf { it.isNotEmpty() }
                ?.let { providerNames ->
                    arrayListOf(
                        AmenitieFilter(
                            type = AmenityType.PETROL,
                            filter = AmenityFilterTypeData(provider = ArrayList(providerNames))
                        )
                    )
                }
        }
        if (amenityType == AmenityType.EVCHARGER) {
            return carContext.getUserDataPreference()?.getEVAmenityFilter()
                ?.takeIf { it.isNotEmpty() }
                ?.let { plugTypes ->
                    arrayListOf(
                        AmenitieFilter(
                            type = AmenityType.EVCHARGER,
                            filter = AmenityFilterTypeData(plugTypes = ArrayList(plugTypes))
                        )
                    )
                }
        }
        return null
    }

    override fun onStartScreen() {
        super.onStartScreen()
        mainBreezeCarContext.mapboxCarContext.mapboxCarMap.registerObserver(amenityLayerRenderer)
        loadNearbyAmenities()
    }

    override fun onStopScreen() {
        super.onStopScreen()
        mainBreezeCarContext.mapboxCarContext.mapboxCarMap.unregisterObserver(amenityLayerRenderer)
    }

    override fun onGetTemplate(): PlaceListNavigationTemplate {
        val templateBuilder = PlaceListNavigationTemplate.Builder()
            .setTitle(getScreenTitle())
            .setHeaderAction(Action.BACK)
            .setLoading(isLoadingAmenities)
        if (!isLoadingAmenities) {
            templateBuilder.setItemList(buildItemList())
        }
        return templateBuilder.build()
    }

    private fun buildItemList(): ItemList {
        val listBuilder = ItemList.Builder()
        nearbyAmenities.forEachIndexed { index, amenity ->
            listBuilder.addItem(
                Row.Builder()
                    .setBrowsable(true)
                    .setTitle("${(amenity.distance ?: 0)}m · ${amenity.name}")
                    .addText(amenity.address ?: "")
                    .setImage(
                        createCarIcon(50, 50, (index + 1).toString())
                    )
                    .setClickSafe {
                        LimitClick.handleSafe {
                            Analytics.logClickEvent(
                                "[nearby_${if (amenityType == AmenityType.PETROL) "petrol" else "ev"}]:location_select",
                                "[androidauto_search]"
                            )
                            navigateToAmenity(amenity)
                        }
                    }
                    .build()
            )
        }
        return listBuilder.build()
    }

    private fun navigateToAmenity(amenity: BaseAmenity) {
        if (fromScreen == Screen.ANDROID_AUTO_CRUISE) {
            Analytics.logClickEvent(Event.AA_END, Screen.ANDROID_AUTO_CRUISE)
            OBUStripStateManager.getInstance()?.closeFromCar()
            CarOBULiteManager.closePhoneOBUScreen()
        }

        mainBreezeCarContext.carContext.getCarService(
            ScreenManager::class.java
        ).popTo(CarConstants.HOME_MARKER)
        val screenData = DataForRoutePreview(
            Point.fromLngLat(amenity.long!!, amenity.lat!!), null
        ).apply {
            destinationSearchLocation = SearchLocation(
                isCarPark = false,
                lat = amenity.lat.toString(),
                long = amenity.long.toString(),
                name = amenity.name,
                address1 = amenity.name,
                address2 = amenity.name,
                carparkId = amenity.id,
                showAvailabilityFB = amenity.showAvailabilityFB
            )
        }
        mainBreezeCarContext.carContext.getCarService(ScreenManager::class.java)
            .push(
                RoutePlanningScreen(RoutePreviewCarContext(mainBreezeCarContext), screenData)
            )
    }

    private fun createCarIcon(width: Int, height: Int, name: String): CarIcon {
        val bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        val paint = Paint(Paint.ANTI_ALIAS_FLAG)
        paint.color = Color.parseColor("#5da7ff")
        paint.textSize = 40f
        paint.isFakeBoldText = true
        paint.textScaleX = 1f
        canvas.drawText(name, 20f, 42f, paint)
        return CarIcon.Builder(IconCompat.createWithBitmap(bitmap)).build()
    }

    private fun getScreenTitle() = when (amenityType) {
        AmenityType.PETROL -> "Fuel"
        AmenityType.EVCHARGER -> "EV"
        else -> ""
    }
}

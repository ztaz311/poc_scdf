package com.ncs.breeze.car.breeze.screen.obulite

import com.ncs.breeze.car.breeze.MainBreezeCarContext

class OBULiteCarContext(
    val mainCarContext: MainBreezeCarContext
) {
    /** MapCarContext **/
    val carContext = mainCarContext.carContext
    val mapboxCarMap = mainCarContext.mapboxCarMap
}

package com.ncs.breeze.ui.dashboard.fragments.obuconnect

import android.annotation.SuppressLint
import android.bluetooth.BluetoothManager
import android.content.Context.BLUETOOTH_SERVICE
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.Event
import com.ncs.breeze.common.analytics.Param
import com.ncs.breeze.common.analytics.Screen
import com.ncs.breeze.common.constant.OBUConstants.OBU_PERMISSIONS
import com.ncs.breeze.common.extensions.android.getApp
import com.ncs.breeze.common.extensions.android.getOBUAlertHelper
import com.ncs.breeze.common.extensions.android.getOBUConnHelper
import com.ncs.breeze.common.helper.obu.callback.OBUSearchListenerWrapper
import com.breeze.model.obu.OBUInitialConnectionStep
import com.ncs.breeze.databinding.FragmentObuConnectFirstStateBinding
import com.ncs.breeze.ui.base.BaseActivity
import com.ncs.breeze.ui.dashboard.fragments.viewmodel.OBUConnectViewModel
import sg.gov.lta.obu.sdk.core.types.OBUError
import sg.gov.lta.obu.sdk.core.types.OBUInternalSearchError
import sg.gov.lta.obu.sdk.data.services.OBUSDK
import timber.log.Timber
import java.util.*

class OBUConnectFirstStateFragment : Fragment() {
    private lateinit var viewBinding: FragmentObuConnectFirstStateBinding
    private val obuConnectViewModel: OBUConnectViewModel by viewModels({ requireParentFragment() })

    private val obuPermissionLauncher =
        registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) { result ->
            if (result.containsValue(false)) {
                getOBUAlertHelper()?.showMissingPermissionAlert()
                Analytics.logPopupEvent(
                    Event.POPUP_OBU_CONNECTION_ERROR_OBU,
                    Event.VALUE_POPUP_OPEN,
                    getScreenName()
                )
            } else {
                Analytics.logClickEvent(
                    Event.OBU_PAIRING_POPUP_OBU_PERMISSION_OK,
                    getScreenName()
                )
                handleOnHavingPermissions()
            }
        }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewBinding = FragmentObuConnectFirstStateBinding.inflate(inflater, container, false)
        return viewBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewBinding.layoutLoading.root.isVisible = false
        obuConnectViewModel.obuSearchState.postValue(false)
        viewBinding.layoutFooter.btnConfirm.setOnClickListener {
            Analytics.logClickEvent(
                Event.START_ENGINE_NEXT,
                getScreenName()
            )
            if (hasOBURequiredPermissions())
                handleOnHavingPermissions()
            else {
                requestOBURequiredPermissions()
            }
        }
        observeOBUSearchState()
    }

    private fun observeOBUSearchState() {
        obuConnectViewModel.obuSearchState.observe(viewLifecycleOwner) { isSearching ->
            viewBinding.layoutLoading.root.isVisible = isSearching
        }
    }

    private fun handleOnHavingPermissions() {
        if (isBluetoothEnabled()) {
            getOBUConnHelper()?.initOBU {
                onSuccess = {
                    searchOBU()
                }
                onError = { error -> showErrorAlert(error) }
            }
        } else {
            getOBUAlertHelper()?.showBluetoothDisabledAlert()
            Analytics.logPopupEvent(
                Event.POPUP_OBU_PERMISSION_TURN_ON_BLUETOOTH,
                Event.VALUE_POPUP_OPEN,
                getScreenName()
            )
        }
    }


    private fun requestOBURequiredPermissions() {
        obuPermissionLauncher.launch(OBU_PERMISSIONS)

        Analytics.logPopupEvent(
            Event.POPUP_ALLOW_OBU_PERMISSION_ACCESS,
            Event.VALUE_POPUP_OPEN,
            getScreenName()
        )
    }

    private fun isBluetoothEnabled() =
        (activity?.getSystemService(BLUETOOTH_SERVICE) as? BluetoothManager)
            ?.adapter?.isEnabled == true

    @SuppressLint("MissingPermission")
    private fun searchOBU(retryCount: Int = 0) {
        obuConnectViewModel.obuSearchState.postValue(true)
        OBUSDK.startSearch(
            OBUSearchListenerWrapper(
                onPairing = {
                    obuConnectViewModel.obuSearchState.postValue(true)
                },

                onObuSelected = { obu ->

                    Analytics.logClickEvent(
                        Event.OBU_PAIRING_START_ENGINE_CONFIRM_OBU_SELECT_OBU,
                        getScreenName(),
                        obu.name
                    )
                    Analytics.logClickEvent(
                        Event.OBU_PAIRING_START_ENGINE_CONFIRM_OBU_SELECT_CONFIRM,
                        getScreenName(),
                        obu.name
                    )
                    obuConnectViewModel.obuSearchState.postValue(false)
                    if (activity?.getApp()?.obuConnectionHelper?.checkOBUIsAlreadyPaired(obu.name) == true) {
                        (activity as? BaseActivity<*, *>)?.obuAlertHelper?.showOBUAlreadyPaired()
                    } else {
                        obuConnectViewModel.obuInitialConnectionStep.postValue(
                            OBUInitialConnectionStep.VEHICLE_NUMBER
                        )
                        obuConnectViewModel.currentObuName = obu.name
                        obuConnectViewModel.currentObu = obu
                    }
                },
                onSearchFailure = { error ->
                    Analytics.logEvent(
                        Event.OBU_SYSTEM_EVENT,
                        bundleOf(
                            Param.KEY_EVENT_VALUE to Event.OBU_VERBOSE_ERROR,
                            Param.KEY_MESSAGE to "Code: ${error?.code}, message: ${error?.localizedMessage}",
                        )
                    )
                    // check if error mesage is `No devices found: null` to retry
                    if (!error?.message.isNullOrBlank()
                        && error?.message?.contains("No devices found") == true
                        && error.message?.length!! < 25
                        && retryCount < 1
                    ) {
                        searchOBU(1)
                    } else if (error is OBUInternalSearchError)
                        handleInternalSearchError(error, retryCount)
                    else if (error != null) {
                        showErrorAlert(error)
                    }
                    obuConnectViewModel.obuSearchState.postValue(false)
                },
            )
        )
    }

    private fun handleInternalSearchError(error: OBUInternalSearchError, retryCount: Int) {
        val errorMessage = error.message?.lowercase(Locale.getDefault())
        val isNoDeviceFound = errorMessage?.contains("no devices found") == true
        // if user cancel, error message will be "No devices found: null" + <chunk of a stacktrace>
        val isUserCancelled = isNoDeviceFound && (errorMessage?.length ?: 0) > 25
        Timber.d("searchOBU handleInternalSearchError: message = $errorMessage, isUserCancelled = $isUserCancelled, isNoDeviceFound = $isNoDeviceFound")
        if (isUserCancelled) {
            Analytics.logClickEvent(
                Event.OBU_PAIRING_START_ENGINE_CONFIRM_OBU_CLOSE,
                getScreenName()
            )
            return
        }
        if (isNoDeviceFound && retryCount < 1) {
            searchOBU(1)
            return
        }
        if (isNoDeviceFound)
            getOBUAlertHelper()?.showNoOBUFoundAlert()
    }

    private fun showErrorAlert(error: OBUError) = getOBUAlertHelper()?.showErrorAlert(error)

    private fun hasOBURequiredPermissions() = OBU_PERMISSIONS.all {
        ContextCompat.checkSelfPermission(
            requireContext(),
            it
        ) == PackageManager.PERMISSION_GRANTED
    }

    fun getScreenName() = Screen.OBU_PAIRING_SCREEN
}
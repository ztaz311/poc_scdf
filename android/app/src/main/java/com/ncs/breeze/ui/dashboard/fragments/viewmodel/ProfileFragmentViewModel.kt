package com.ncs.breeze.ui.dashboard.fragments.viewmodel

import android.app.Application
import androidx.lifecycle.viewModelScope
import com.breeze.model.api.ErrorData
import com.ncs.breeze.common.remote.ApiHelper
import com.ncs.breeze.common.remote.ApiObserver
import com.breeze.model.api.response.UserDataResponse
import com.ncs.breeze.components.SingleLiveEvent
import com.ncs.breeze.ui.base.BaseFragmentViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.launch
import javax.inject.Inject

class ProfileFragmentViewModel @Inject constructor(apiHelper: ApiHelper, application: Application) :
    BaseFragmentViewModel(
        application
    ) {

    var mAPIHelper: ApiHelper = apiHelper
    var userDataResponse: SingleLiveEvent<UserDataResponse> = SingleLiveEvent()

    /**
     * fetch UserData
     */
    fun getUserData() {
        viewModelScope.launch {
            mAPIHelper.getUserData()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : ApiObserver<UserDataResponse>(compositeDisposable) {
                    override fun onSuccess(data: UserDataResponse) {
                        userDataResponse.value = data
                    }

                    override fun onError(e: ErrorData) {

                    }
                })
        }
    }
}
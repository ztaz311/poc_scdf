package com.ncs.breeze.helper.animation

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.view.View

object ViewAnimator {


    fun hideViewSlideDown(view: View?, duration: Long, animationEndCB: () -> Unit = {}) {
        if (view == null) {
            return
        }
        view.animate()
            .translationY(view.height.toFloat())
            .setDuration(duration)
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    super.onAnimationEnd(animation)
                    view.visibility = View.GONE
                    animationEndCB.invoke()
                }
            })
    }

    fun showViewSlideUp(view: View?, duration: Long, animationEndCB: () -> Unit = {}) {
        if (view == null) {
            return
        }
        view.visibility = View.VISIBLE
        measureView(view)
        view.translationY = view.measuredHeight.toFloat()
        view.animate()
            .setDuration(duration)
            .translationY(0.0F)
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    super.onAnimationEnd(animation)
                    animationEndCB.invoke()
                }
            })
    }


    fun measureView(view: View) {
        view.measure(
            View.MeasureSpec.makeMeasureSpec(
                0,
                View.MeasureSpec.UNSPECIFIED
            ), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
        );
    }

}
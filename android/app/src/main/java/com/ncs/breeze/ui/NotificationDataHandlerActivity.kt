package com.ncs.breeze.ui

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.Event
import com.ncs.breeze.common.analytics.Screen
import com.ncs.breeze.common.extensions.android.getAppPreference
import com.ncs.breeze.common.storage.AppPrefsKey
import com.breeze.model.constants.Constants
import com.ncs.breeze.common.utils.Utils
import com.ncs.breeze.notification.FCMListenerService
import com.ncs.breeze.ui.splash.SplashActivity
import timber.log.Timber


class NotificationDataHandlerActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Timber.d("FCM - NotificationDataHandlerActivity::onCreate")
        handleNotification(intent)
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        Timber.d("FCM - NotificationDataHandlerActivity::onNewIntent")
        handleNotification(intent)
    }

    private fun handleNotification(intent: Intent?) {
        Timber.d("FCM - NotificationDataHandler:: handleNotification")
        if (intent != null) {
            val analyticsData =
                intent.getBundleExtra(FCMListenerService.ARG_NOTIFICATION_ANALYTICS_STATISTIC)
            val datePushed =
                analyticsData?.getString(Constants.PushNotification.KEY_DATE_PUSHED) ?: ""
            val message = analyticsData?.getString(Constants.PushNotification.KEY_MESSAGE) ?: ""
            val category = analyticsData?.getString(Constants.PushNotification.KEY_CATEGORY) ?: ""
            Analytics.logClickEvent(Event.NOTIFICATION_CLICK, Screen.OUTSIDE_APP)
            Analytics.logNotificationStatistics(
                screenName = Screen.OUTSIDE_APP,
                dateClicked = Utils.getCurrentDateFormatted("YYYY-MM-dd HH:mm"),
                datePushed = datePushed,
                message = message,
                category = category
            )

            intent.extras?.takeIf { it.containsKey(ARG_NOTIFICATION_CHANNEL) }
                ?.let { extras ->
                    val channel = extras.getString(ARG_NOTIFICATION_CHANNEL) ?: ""
                    val id = extras.getString(ARG_NOTIFICATION_ID) ?: ""
                    Timber.d("FCM - handleNotification[$channel][$id] --")
                    getAppPreference()?.saveString(
                        AppPrefsKey.PUSH_NOTIFICATION_CHANNEL, channel
                    )
                    getAppPreference()?.saveString(AppPrefsKey.PUSH_NOTIFICATION_ID, id)
                    getAppPreference()?.saveString(
                        AppPrefsKey.PUSH_NOTIFICATION_EXTRA,
                        extras.getString(ARG_NOTIFICATION_EXTRA) ?: ""
                    )
                    getAppPreference()?.saveString(
                        AppPrefsKey.KEY_CONTENT_CATEGORY_ID,
                        extras.getString(ARG_CONTENT_CATEGORY_ID) ?: ""
                    )
                }
            if (!isApplicationRunning()) {
                startSplashActivity()
            } else {
                notifyUpdateAppNotificationReceived()
            }
        }

        finish()
    }

    private fun notifyUpdateAppNotificationReceived() {
        //Not required
    }

    private fun startSplashActivity() {
        val intent = Intent(this, SplashActivity::class.java)
        //intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        //intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
    }

    private fun isApplicationRunning(): Boolean {
        return !isTaskRoot()
    }

    companion object {
        private const val ARG_NOTIFICATION_CHANNEL = "HANDLE_PUSH_NOTIFICATION_CHANNEL"
        private const val ARG_NOTIFICATION_ID = "HANDLE_PUSH_NOTIFICATION_ID"
        private const val ARG_NOTIFICATION_EXTRA = "HANDLE_PUSH_NOTIFICATION_EXTRA"
        private const val ARG_CONTENT_CATEGORY_ID = "KEY_CONTENT_CATEGORY_ID"
    }
}
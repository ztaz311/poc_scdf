package com.ncs.breeze.ui.login.fragment.signUp.viewmodel

import android.app.Activity
import android.app.Application
import androidx.lifecycle.viewModelScope
import com.amplifyframework.auth.AuthProvider
import com.amplifyframework.auth.cognito.exceptions.service.UserCancelledException
import com.amplifyframework.auth.options.AuthSignOutOptions
import com.amplifyframework.auth.result.AuthSignInResult
import com.amplifyframework.core.Amplify
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.Event
import com.breeze.model.api.ErrorData
import com.breeze.model.constants.LOGIN_TYPE
import com.breeze.model.constants.LOGIN_WITH_SOCIAL_STATE
import com.breeze.model.constants.STATE_FETCH_USER_ATTR
import com.ncs.breeze.common.remote.ApiHelper
import com.ncs.breeze.common.remote.ApiObserver
import com.ncs.breeze.common.remote.MyServiceInterceptor
import com.ncs.breeze.common.storage.BreezeUserPreference
import com.ncs.breeze.common.utils.*
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.ncs.breeze.common.extensions.android.getApp
import com.ncs.breeze.components.AmplifyErrorHandler
import com.ncs.breeze.components.SingleLiveEvent
import com.ncs.breeze.ui.base.BaseFragmentViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.*
import timber.log.Timber
import javax.inject.Inject
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException

class SignUpFragmentBreezeViewModel @Inject constructor(
    val apiHelper: ApiHelper,
    application: Application
) : BaseFragmentViewModel(
    application
) {

    var errorThrow: SingleLiveEvent<Boolean> = SingleLiveEvent()
    var isShowProgess: SingleLiveEvent<Boolean> = SingleLiveEvent()
    var shouldNotShowTAndC: SingleLiveEvent<Boolean> = SingleLiveEvent()
    var createAccountSuccessful: SingleLiveEvent<Boolean> = SingleLiveEvent();
    var createAccountFailed: SingleLiveEvent<AmplifyErrorHandler.AuthObject> = SingleLiveEvent();
    var toastMessage: SingleLiveEvent<String> = SingleLiveEvent();
    var reActionLogin: SingleLiveEvent<String> = SingleLiveEvent();

    var stateSignUpSocial = LOGIN_WITH_SOCIAL_STATE.NOT_START

    var mAPIHelper: ApiHelper = apiHelper

    @Inject
    lateinit var myServiceInterceptor: MyServiceInterceptor

    @Inject
    lateinit var breezeUserPreferenceUtil: BreezeUserPreferenceUtil

    private var isRetry = false
    private var typeLogin = LOGIN_TYPE.GOOGLE


    fun loginWithFaceBook(activity: Activity) {
        typeLogin = LOGIN_TYPE.FACEBOOK
        isRetry = false
        isShowProgess.postValue(false)
        viewModelScope.launch(Dispatchers.IO) {
            kotlin.runCatching {
                suspendCancellableCoroutine<AuthSignInResult> { cancelAble ->
                    logoutApp {
                        AWSUtils.signInWithSocialWebUI(
                            AuthProvider.facebook(),
                            activity,
                            { result ->
                                cancelAble.resume(result)
                            },
                            {
                                cancelAble.resumeWithException(it)
                            }
                        )
                    }
                }
            }.onSuccess { result ->
                if (result.isSignedIn) {
                    stateSignUpSocial = LOGIN_WITH_SOCIAL_STATE.SIGNIN_SUCCESS
                    /**
                     * fetch token
                     */
                    fetchToken()
                    Analytics.sendLoginEvent(Event.METHOD_FACEBOOK)
                } else {
                    isShowProgess.postValue(false)
                    errorThrow.postValue(false)

                }
            }
                .onFailure { e ->
                    /**
                     * handle on this case user cancel
                     */
                    isShowProgess.postValue(false)
                    if (e !is UserCancelledException) {
                        sendErrorEventToShow()
                    }
                    toastMessage.postValue("fail :" + e.message)
                }
        }
    }


    fun loginWithGoogle(activity: Activity) {
        typeLogin = LOGIN_TYPE.GOOGLE
        isRetry = false
        isShowProgess.postValue(true)

        viewModelScope.launch(Dispatchers.IO) {
            kotlin.runCatching {
                suspendCancellableCoroutine<AuthSignInResult> { cancelAble ->
                    logoutApp {
                        AWSUtils.signInWithSocialWebUI(
                            AuthProvider.google(),
                            activity,
                            { result ->
                                cancelAble.resume(result)
                            },
                            {
                                cancelAble.resumeWithException(it)
                            }
                        )
                    }
                }
            }.onSuccess { result ->
                Timber.d("loginWithGoogle-onSuccess: $result")
                if (result.isSignedIn) {
                    stateSignUpSocial = LOGIN_WITH_SOCIAL_STATE.SIGNIN_SUCCESS
                    /**
                     * fetch token
                     */
                    fetchToken()
                    Analytics.sendLoginEvent(Event.METHOD_GOOGLE)
                } else {
                    isShowProgess.postValue(false)
                    errorThrow.postValue(false)
                }
            }.onFailure { e ->
                Timber.d("loginWithGoogle-onFailure: $e")
                /**
                 * handle on this case user cancel
                 */
                isShowProgess.postValue(false)
                if (e !is UserCancelledException) {
                    sendErrorEventToShow()
                }
                toastMessage.postValue("fail : " + e.message)
            }
        }
    }

    private fun logoutApp(callback: () -> Unit) {
        AWSUtils.signOut(callback)
    }

    /**
     * fetch token
     */
    private suspend fun fetchToken() {
        var jobFetchToken: Job? = null
        var jobCountDown: Job? = null

        viewModelScope.launch(Dispatchers.IO) {
            var isHaveResponse = false
            jobFetchToken = launch {
                kotlin.runCatching {
                    suspendCancellableCoroutine<String?> {
                        AWSUtils.checkUserAuthSession(object : AWSUtils.AuthSessionCallback {
                            override fun onAuthSuccess(token: String?) {
                                it.resume(token)
                                isHaveResponse = true
                                jobCountDown?.cancel()
                            }

                            override fun onAuthFailed() {
                                it.resumeWithException(Exception())
                                isHaveResponse = true
                                jobCountDown?.cancel()
                            }
                        })
                    }
                }.onSuccess { token ->
                    if (token != null) {
                        stateSignUpSocial = LOGIN_WITH_SOCIAL_STATE.FETCH_TOKEN_SUCCESS

                        /**
                         * set token to interceptor
                         */
                        myServiceInterceptor.setSessionToken(token)

                        /**
                         * fetch user attr
                         */
                        fetchUserAttr()

                        /**
                         * end follow sign up fetch token
                         */
                    } else {
                        sendErrorEventToShow()
                    }
                }.onFailure {
                    isShowProgess.postValue(false)
                }
            }

            jobCountDown = launch {
                delay(3000)
                if (!isHaveResponse) {
                    /**
                     * send event time out
                     */
                    jobFetchToken?.cancel()
                    isRetry = true
                    /**
                     * recall again
                     */
                    if (isRetry) {
                        /**
                         * send event to fragment to recall login
                         */
                        reActionLogin.postValue("")

                    } else {
                        toastMessage.postValue("something error please try again !")
                        isShowProgess.postValue(false)
                    }
                }
            }
        }
    }


    /**
     * fetch user attr
     */
    private fun fetchUserAttr() {
        amplifyFetchUserAttributes(breezeUserPreferenceUtil) { state ->
            when (state) {
                STATE_FETCH_USER_ATTR.USER_USERNAME_AVAILABLE -> {
                    InitObjectUtilsController.currentObjectFlowLoginSignUp?.isUserNameAvailable =
                        true

                    stateSignUpSocial = LOGIN_WITH_SOCIAL_STATE.FETCH_USER_ATTR

                    /**
                     * fetch setting user
                     */
                    fetchSettingUser()

                }

                STATE_FETCH_USER_ATTR.USER_NO_USERNAME_ATTRIBUTE -> {
                    InitObjectUtilsController.currentObjectFlowLoginSignUp?.isUserNameAvailable =
                        false

                    stateSignUpSocial = LOGIN_WITH_SOCIAL_STATE.FETCH_USER_ATTR

                    /**
                     * fetch setting user
                     */
                    fetchSettingUser()
                }

                STATE_FETCH_USER_ATTR.FETCH_ATTR_USER_FAILS -> {
                    sendErrorEventToShow()
                }
            }
        }
    }

    /**
     * fetch setting user & all setting in system
     */
    private fun fetchSettingUser() {
        getAllSettingZipData(breezeUserPreferenceUtil, apiHelper) { isFetchSuccess ->
            if (isFetchSuccess) {
                /**
                 * send event with is user name available
                 */
                createAccountSuccessful.postValue(true)
            } else {
                sendErrorEventToShow()
            }
        }
    }


    /**
     * check term of condition user
     */
    fun checkTermOfCondition() {
        checkTermOfCondition(breezeUserPreferenceUtil, apiHelper) { isShouldShowTerm ->
            shouldNotShowTAndC.postValue(isShouldShowTerm)
        }
    }

    /**
     * send event to show error
     */
    fun sendErrorEventToShow() {
        Utils.runOnMainThread {
            isShowProgess.postValue(false)
            errorThrow.postValue(true)
        }
    }

    /**
     * reSignUp follow
     */
    fun restartSignUpFlow() {
        AWSUtils.init(getApp())
        viewModelScope.launch(Dispatchers.IO) {
            when (stateSignUpSocial) {
                LOGIN_WITH_SOCIAL_STATE.SIGNIN_SUCCESS -> {
                    fetchToken()
                }

                LOGIN_WITH_SOCIAL_STATE.FETCH_TOKEN_SUCCESS -> {
                    fetchUserAttr()
                }

                LOGIN_WITH_SOCIAL_STATE.FETCH_USER_ATTR -> {
                    fetchSettingUser()
                }
            }
        }
    }


    /**
     * recall Login
     */
    fun reCallLogin(activity: Activity) {
        when (typeLogin) {
            LOGIN_TYPE.GOOGLE -> {
                loginWithGoogle(activity)
            }

            LOGIN_TYPE.FACEBOOK -> {
                loginWithFaceBook(activity)
            }

            else -> {
                toastMessage.postValue("something error please try again !")
                isShowProgess.postValue(false)
            }
        }
    }


    /**
     * update term of condition
     */
    fun updateTncStatus() {
        val request = JsonObject()
        request.addProperty(
            "tncAcceptStatus",
            BreezeUserPreference.getInstance(getApp()).getTncStatus()
        )

        mAPIHelper.updateTncAcceptedStatus(request)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : ApiObserver<JsonElement>(compositeDisposable) {
                override fun onSuccess(data: JsonElement) {
                    Timber.d("Update T&C status API Successful")
                    Timber.d("Response from update T&C status API ${data}")
                    viewModelScope.launch(Dispatchers.Main) {
                        BreezeUserPreference.getInstance(getApp()).clearTncStatus()
                    }
                }

                override fun onError(e: ErrorData) {
                    Timber.d("API failed: " + e.toString())
                }
            })
    }

}
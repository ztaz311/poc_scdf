package com.ncs.breeze.components.map.profile

enum class MapProfileType {
    STANDARD,
    CLUSTERED
}
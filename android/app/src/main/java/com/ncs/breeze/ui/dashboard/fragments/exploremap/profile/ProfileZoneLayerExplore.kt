package com.ncs.breeze.ui.dashboard.fragments.exploremap.profile

import android.content.Context
import android.graphics.Color
import android.location.Location
import androidx.annotation.UiThread
import com.breeze.model.api.response.Zones
import com.breeze.model.api.response.tbr.ResponseCongestionDetail
import com.mapbox.geojson.Feature
import com.mapbox.geojson.LineString
import com.mapbox.geojson.Point
import com.mapbox.geojson.Polygon
import com.mapbox.maps.MapboxMap
import com.mapbox.maps.Style
import com.mapbox.maps.extension.style.layers.addLayer
import com.mapbox.maps.extension.style.layers.generated.FillLayer
import com.mapbox.maps.extension.style.layers.getLayerAs
import com.mapbox.maps.extension.style.layers.properties.generated.Visibility
import com.mapbox.maps.extension.style.sources.addSource
import com.mapbox.maps.extension.style.sources.generated.GeoJsonSource
import com.mapbox.maps.extension.style.sources.generated.geoJsonSource
import com.mapbox.maps.extension.style.sources.getSourceAs
import com.mapbox.turf.TurfConstants
import com.mapbox.turf.TurfMeasurement
import com.mapbox.turf.TurfMeta
import com.mapbox.turf.TurfTransformation
import com.ncs.breeze.ui.dashboard.manager.ExploreMapStateManager


open class ProfileZoneLayerExplore
    (
    protected val mapboxMap: MapboxMap,
    protected val context: Context,
    protected val style: Style,
) {

    companion object {
        const val CIRCLE_STEPS = 360
        const val LAYER_ID = "_layer_id"
        const val SOURCE_ID = "_source_id"

        const val IN_ZONE = "IN_ZONE"
        const val NO_ZONE = "NO_ZONE"

        const val INNER_ZONE = "INNER_ZONE"
        const val OUTER_ZONE = "OUTER_ZONE"

        fun detectSelectedProfileLocationInZone(userLocation: Location?): String {
            val typeZone = NO_ZONE

            ExploreMapStateManager.contentDetailsResponse?.zones?.let { profileZones ->
                if (profileZones.size == 0) {
                    return@let
                }
                val profilePoint = Point.fromLngLat(
                    profileZones.first().centerPointLong!!,
                    profileZones.first().centerPointLat!!
                )
                if (profilePoint == null || userLocation == null || profileZones.isNullOrEmpty()) return typeZone
                val distance = TurfMeasurement.distance(
                    profilePoint,
                    Point.fromLngLat(userLocation.longitude, userLocation.latitude),
                    TurfConstants.UNIT_METRES
                )
                return if (isDistanceInZone(distance)) {
                    IN_ZONE
                } else {
                    NO_ZONE
                }
            }
            return NO_ZONE
        }

        private fun isDistanceInZone(distance: Double): Boolean {
            ExploreMapStateManager.contentDetailsResponse?.zones?.forEach {
                if (distance < (it.radius ?: 0)) return true
            }
            return false
        }
    }

    var selectedProfilePoint: Point? = null
    var maxRadius: Double = 1000.0
    private var isDrawCircle = false
    private var selectedZoneList: ArrayList<Zones> = ArrayList()

    fun addSourceAndLayerID() {
        selectedZoneList.forEach {
            mapboxMap.getStyle { style ->
                style.addSource(
                    geoJsonSource("${it.zoneId}$SOURCE_ID") {
                    }
                )
                val fillLayerOuter = FillLayer(
                    "${it.zoneId}$LAYER_ID",
                    "${it.zoneId}$SOURCE_ID"
                )
                fillLayerOuter.fillOpacity(0.7)
                style.addLayer(fillLayerOuter)
            }
        }
    }


    fun updateColorOuterRing(pListColorResponse: ArrayList<ResponseCongestionDetail>) {
        pListColorResponse.forEachIndexed { _, data ->
            if (style.styleLayerExists("${data.name}$LAYER_ID")) {
                val fillLayer = style.getLayerAs<FillLayer>("${data.name}$LAYER_ID")
                fillLayer?.fillColor(Color.parseColor(data.color))
                fillLayer?.fillOpacity(data.opacity ?: 0.5)
            }
        }
    }

    fun updateZoneZoomDetails(profileZone: List<Zones>) {
        profileZone.forEachIndexed { _, data ->
            if (style.styleLayerExists("${data.zoneId}$LAYER_ID")) {
                val fillLayer = style.getLayerAs<FillLayer>("${data.zoneId}$LAYER_ID")
                fillLayer?.let { fillLayer ->
                    data.activeZoomLevel?.let {
                        fillLayer.minZoom(it.toDouble())
                    }
                    data.deactiveZoomLevel?.let {
                        fillLayer.maxZoom(it.toDouble())
                    }
                }
            }
        }
    }


    fun drawCircle() {
        if (selectedProfilePoint == null) {
            return
        }
        mapboxMap.getStyle { style ->
            isDrawCircle = true
            selectedZoneList.forEachIndexed() { position, zone ->
                if (position == 0) {
                    val innerCirclePolygon = getTurfPolygon(
                        (zone.radius)!!.toDouble(),
                        selectedProfilePoint!!
                    )
                    val innerCircleSource: GeoJsonSource =
                        style.getSourceAs("${zone.zoneId}$SOURCE_ID")!!

                    val feature = Feature.fromGeometry(
                        Polygon.fromOuterInner(
                            LineString.fromLngLats(TurfMeta.coordAll(innerCirclePolygon!!, false))
                        )
                    )
                    innerCircleSource.feature(feature)

                } else {

                    /**
                     * set radius max
                     */
                    maxRadius = (zone.radius ?: 0).toDouble()
                    val innerCirclePolygon = getTurfPolygon(
                        (selectedZoneList[position - 1].radius ?: 0).toDouble(),
                        selectedProfilePoint!!
                    )
                    val outerCirclePolygon = getTurfPolygon(
                        (zone.radius ?: 0).toDouble(),
                        selectedProfilePoint!!
                    )
                    val outerCircleSource: GeoJsonSource =
                        style.getSourceAs("${zone.zoneId}$SOURCE_ID")!!
                    outerCircleSource.feature(
                        Feature.fromGeometry(
                            Polygon.fromOuterInner(  // Create outer LineString
                                LineString.fromLngLats(
                                    TurfMeta.coordAll(
                                        outerCirclePolygon!!,
                                        false
                                    )
                                ),
                                LineString.fromLngLats(
                                    TurfMeta.coordAll(
                                        innerCirclePolygon!!,
                                        false
                                    )
                                )
                            )
                        )
                    )
                }
            }
        }
    }


    /**
     * remove all style & layer
     */
    fun remove() {
        isDrawCircle = false
        selectedZoneList.forEach {
            style.removeStyleLayer("${it.zoneId}$LAYER_ID")
            style.removeStyleSource("${it.zoneId}$SOURCE_ID")
        }
        selectedZoneList.clear()
    }

    fun hideCircleLayer() {
        mapboxMap.getStyle {
            selectedZoneList.forEach {
                val layer = style.getLayerAs<FillLayer>("${it.zoneId}$LAYER_ID")
                layer?.visibility(Visibility.NONE)
            }
        }
    }


    fun showCircleLayer() {
        mapboxMap.getStyle {
            selectedZoneList.forEach {
                val layer = style.getLayerAs<FillLayer>("${it.zoneId}$LAYER_ID")
                layer?.visibility(Visibility.VISIBLE)
            }
        }
    }


    /**
     * update config circle layer
     */
    fun updateConfigCircleLayer(pListZone: List<Zones>) {
        remove()
        selectedZoneList.addAll(pListZone)
        if (!selectedZoneList.isNullOrEmpty()) {
            val lat = selectedZoneList[0].centerPointLat ?: 1.2856954590697074
            val long = selectedZoneList[0].centerPointLong ?: 103.83289725755539
            selectedProfilePoint = Point.fromLngLat(long, lat)
        }
    }


    private fun getTurfPolygon(radius: Double, centerPoint: Point): Polygon? {
        return TurfTransformation.circle(
            centerPoint,
            radius,
            CIRCLE_STEPS,
            TurfConstants.UNIT_METERS
        )
    }

    @UiThread
    fun onDestroy() {
        isDrawCircle = false
    }


    data class ProfileZoneType(
        val selectedProfile: String?,
        val profileZoneId: String?,
        val type: String
    )
}
package com.ncs.breeze.ui.dashboard.fragments.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import com.ncs.breeze.BuildConfig
import com.ncs.breeze.common.analytics.Screen
import com.ncs.breeze.common.utils.BreezeUserPreferenceUtil
import com.ncs.breeze.databinding.FragmentAboutBinding
import com.ncs.breeze.ui.base.BaseFragment
import com.ncs.breeze.ui.dashboard.fragments.viewmodel.AboutViewModel
import javax.inject.Inject

class AboutFragment : BaseFragment<FragmentAboutBinding, AboutViewModel>() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val viewModel: AboutViewModel by viewModels {
        viewModelFactory
    }

    @Inject
    lateinit var breezeUserPreferenceUtil: BreezeUserPreferenceUtil

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initialize()
    }

    fun initialize() {
        viewBinding.backButton.setOnClickListener {
            onBackPressed()
        }
        //viewBinding.webViewAbout.loadUrl(resources.getString(R.string.about_link));

        viewBinding.webViewAbout.getSettings().cacheMode = WebSettings.LOAD_NO_CACHE;

        viewBinding.webViewAbout.settings.javaScriptEnabled = true
        viewBinding.webViewAbout.webViewClient = object : WebViewClient() {
            override fun onPageFinished(view: WebView?, url: String?) {
                if (breezeUserPreferenceUtil.isDarkTheme()) {
                    val code = """javascript:(function() { 
                        var node = document.createElement('style');
                        node.type = 'text/css';
                        node.innerHTML = 'body {
                            color: white;
                            background-color: #222638;
                        }';
                        document.head.appendChild(node);
                    })()""".trimIndent()
                    view?.loadUrl(code)
                }
            }
        }

        viewBinding.webViewAbout.loadUrl(BuildConfig.ABOUT_US_URL);
    }

    override fun inflateViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?,
        attachToContainer: Boolean
    ) = FragmentAboutBinding.inflate(inflater, container, attachToContainer)

    override fun getViewModelReference(): AboutViewModel {
        return viewModel
    }

    override fun getScreenName(): String {
        return Screen.SETTINGS_ABOUT
    }
}
package com.ncs.breeze.common.utils

import com.google.gson.Gson
import com.google.gson.internal.Streams
import com.google.gson.stream.JsonReader
import com.breeze.model.api.ErrorData
import com.breeze.model.api.response.notification.*
import com.ncs.breeze.common.remote.ApiHelper
import com.ncs.breeze.common.remote.ApiObserver
import com.ncs.breeze.common.rxbus.RxBus
import com.ncs.breeze.common.rxbus.RxEvent
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import org.json.JSONArray
import org.json.JSONObject
import timber.log.Timber
import java.io.StringReader
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class BreezeGlobalNotificationsUtil @Inject constructor(val apiHelper: ApiHelper) {

    var compositeDisposable: CompositeDisposable = CompositeDisposable()

    var globalNotificationHandlerEnabled: Boolean = false;
    var readNotificationIds: MutableList<Int> = ArrayList() //Value should be setting from RN
    var listItemSee: MutableList<SeenNotification> = ArrayList()

    fun startGlobalNotificationHelper() {
        synchronized(this) {
            if (!globalNotificationHandlerEnabled) {
                globalNotificationHandlerEnabled = true
                subscribeToAppSyncRxEvent()
            }
        }
    }

    fun stopGlobalNotificationHelper() {
        Timber.d("stopGlobalNotificationHelper called")
        if (!compositeDisposable.isDisposed) {
            compositeDisposable.clear()
        }
        globalNotificationHandlerEnabled = false
    }

    fun subscribeToAppSyncRxEvent() {
        compositeDisposable.add(
            RxBus.listen(RxEvent.GlobalNotificationSyncEvent::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError {
                    Timber.d("Global notification observing on error")
                }
                .subscribe {
                    Timber.d("Global notification event observed successfully")
                    try {
                        val jsonElement =
                            Streams.parse(JsonReader(StringReader(it.globalNotificationDataUpdated)))
                        val response =
                            Gson().fromJson(jsonElement, Array<NotificationItem>::class.java)
                        response.apply {
                            parseGlobalNotificationData(response.toList())
                        }
                    } catch (e: Exception) {
                        Timber.e(e, "Error incorrect json")
                    }
                }
        )
    }

    //Warning: This method is called like 1000 times
    //Without any one time check
    //So do no rely on this method if you are planning to integrate something like Tutorial
    //or Nudge after notification has been checked..
    fun getNotificationShowFirstTime() {
        apiHelper.getListNotificationJson(1, 50)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : ApiObserver<NotificationResponse>(compositeDisposable) {
                override fun onSuccess(data: NotificationResponse) {
                    parseGlobalNotificationData(data.listNotification)
                }

                override fun onError(e: ErrorData) {
                    Timber.d("API failed: " + e.toString())
                    RxBus.publish(RxEvent.GlobalNotificationSkipped())
                }
            })
    }

    private fun parseGlobalNotificationData(listNotifications: List<NotificationItem>) {
        val listFiltered = filterNotificationToShow(listNotifications)
        if (listFiltered.isNotEmpty()) {
            RxBus.publish(RxEvent.GlobalNotificationDataRefresh(
                GlobalNotificationData(
                    notificationItem = listFiltered[0],
                    seenNotifications = listFiltered.map { it.toSeenNotification() }.toTypedArray()
                )
            ))
        } else {
            RxBus.publish(RxEvent.GlobalNotificationSkipped())
        }
    }

    private fun NotificationItem.toSeenNotification(): SeenNotification {
        val seenNotification = SeenNotification()
        seenNotification.notificationId = this.notificationId
        seenNotification.startTime = this.startTime
        seenNotification.expireTime = this.expireTime
        return seenNotification
    }

    private fun filterNotificationToShow(listNotification: List<NotificationItem>): List<NotificationItem> {
        /*val listItemSee =
            BreezeUserPreference.getInstance(applicationContext)?.getLocalNotification()*/
        /**
         * filter list notification only use : ADMIN || TRFFIC_ALERTS
         */
        return listNotification
            .asSequence()
            .filter { item ->
                NotificationCategoryEnum.values().map { it.text }.contains(item.category)
            }
            .filterNot { item ->
                ((item.startTime ?: 0).toLong() * 1000) >= System.currentTimeMillis()
            }
            .filterNot { item ->
                System.currentTimeMillis() >= ((item.expireTime ?: 0).toLong() * 1000)
            }
            .filterNot { item ->
                readNotificationIds.contains(item.notificationId)
            }
            .filter { item -> item.isShowInAppNotification == true }
            .filterNot { item ->
                isNotificationSeen(item.notificationId ?: 0)
            }
            .toList()
    }

    private fun isNotificationSeen(itemId: Int): Boolean {
        listItemSee.toList().forEach { item ->
            if (item.notificationId == itemId) {
                return true
            }
        }
        return false
    }

    operator fun JSONArray.iterator(): Iterator<JSONObject> =
        (0 until length()).asSequence().map { get(it) as JSONObject }.iterator()

}
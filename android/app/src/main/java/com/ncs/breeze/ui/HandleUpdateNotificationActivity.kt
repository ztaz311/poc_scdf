package com.ncs.breeze.ui

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.breeze.model.constants.Constants
import com.ncs.breeze.ui.base.RemoteConfigUtils
import com.ncs.breeze.ui.splash.SplashActivity
import timber.log.Timber


class HandleUpdateNotificationActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        handleNotification(getIntent())
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        handleNotification(intent)
    }

    private fun handleNotification(intent: Intent?) {
        intent?.let {
            RemoteConfigUtils.updateCheckDurationHrs = null
            RemoteConfigUtils.isDirectlyFromNotification = true
            RemoteConfigUtils.forceUpdate = true

            val url = it.extras?.getString(Constants.SHARED_PREF_KEY.HANDLE_PUSH_NOTIFICATION_ID)
            if (url.isNullOrEmpty()) {
                openAppPlaystorePage()
            } else {
                openUrl(url)
            }
//            if (!isApplicationRunning()) {
//                startSplashActivity()
//            } else {
//                notifyUpdateAppNotificationReceived()
//            }
        }
        finish()
    }

    private fun openAppPlaystorePage() {
        val appPackageName = packageName
        try {
            startActivity(
                Intent(
                    Intent.ACTION_VIEW, Uri.parse("market://details?id=$appPackageName")
                )
            )
        } catch (e: ActivityNotFoundException) {
            startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("https://play.google.com/store/apps/details?id=$appPackageName")
                )
            )
        }
    }

    private fun openUrl(url: String) {
        try {
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))
        } catch (e: Exception) {
            Timber.e(e)
        }
    }

    private fun notifyUpdateAppNotificationReceived() {
        //Not required
    }

    private fun startSplashActivity() {
        val intent = Intent(this, SplashActivity::class.java)
        //intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        //intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
    }

    private fun isApplicationRunning(): Boolean {
        return !isTaskRoot()
    }
}
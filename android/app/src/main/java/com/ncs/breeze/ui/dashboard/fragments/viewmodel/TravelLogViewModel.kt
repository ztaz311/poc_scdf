package com.ncs.breeze.ui.dashboard.fragments.viewmodel

import android.app.Application
import androidx.lifecycle.viewModelScope
import androidx.paging.*
import com.breeze.model.api.ErrorData
import com.breeze.model.api.request.SendTripLogByMonthRequest
import com.breeze.model.api.request.SendTripLogRequest
import com.ncs.breeze.components.paged.TripSummaryPagingSource
import com.ncs.breeze.common.remote.ApiHelper
import com.ncs.breeze.common.remote.ApiObserver
import com.ncs.breeze.common.utils.TimeConst
import com.google.gson.JsonElement
import com.breeze.model.api.request.ObuVehicleFilter
import com.breeze.model.api.response.*
import com.ncs.breeze.components.paged.TripObuTransactionPagingSource
import com.ncs.breeze.components.SingleLiveEvent
import com.ncs.breeze.components.TravelHistoryAdapter
import com.breeze.model.extensions.formatDate
import com.ncs.breeze.helper.TripsPlannerPagingSource
import com.ncs.breeze.ui.base.BaseFragmentViewModel
import com.ncs.breeze.ui.dashboard.fragments.view.TransactionFilter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import timber.log.Timber
import java.util.*
import javax.inject.Inject

class TravelLogViewModel @Inject constructor(apiHelper: ApiHelper, application: Application) :
    BaseFragmentViewModel(
        application
    ) {
    var apiHelper: ApiHelper = apiHelper
    var month: String
    var year: String
    var lastTripPlanSearchQuery: String = ""
    var upcomingStartDate: Date? = null
    var upcomingEndDate: Date? = null
    var historyStartDate: Date? = null
    var historyEndDate: Date? = null
    var transactionStartDate: Date? = null
    var transactionEndDate: Date? = null
    var transactionDataType: TransactionFilter = TransactionFilter.ALL


    init {
        month = (Calendar.getInstance().get(Calendar.MONTH) + 1).toString()
        year = Calendar.getInstance().get(Calendar.YEAR).toString()
    }

    var isCurrentMonthSelected = false
    private var selectedList: ArrayList<Long> = arrayListOf()

    //    val trips: Flow<PagingData<TravelLogItem>> = getTripHistoryListStream()
    var upcomingTripDetails: SingleLiveEvent<UpcomingTripDetailResponse> = SingleLiveEvent()
    var deleteTripSuccessful: SingleLiveEvent<Pair<Boolean, Long>> = SingleLiveEvent()
    var deleteTripHistorySuccessful: SingleLiveEvent<Pair<Boolean, Long>> = SingleLiveEvent()
    var travelSummaryResult: SingleLiveEvent<TripDetailsResponse> = SingleLiveEvent()
    var sendEmailSuccessful: SingleLiveEvent<Boolean> = SingleLiveEvent()


    var vehicleDetailsResponseEvent: SingleLiveEvent<ObuVehicleResponse?> = SingleLiveEvent()

//    var tripsPlanner: Flow<PagingData<TripPlannerItem>> = getTripPlannerListStream()
//        .map { item -> item.map {
//            selectItem(isCurrentMonthSelected, it.tripId)
//            TravelLogItem.TripLogItem(isCurrentMonthSelected, it)
//        }
//        }

    fun isTransactionHasDateFilter() = transactionStartDate != null && transactionEndDate != null


    fun getTripSummary(tripId: Long) {
        apiHelper.getTripDetails(tripId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : ApiObserver<TripDetailsResponse>(compositeDisposable) {
                override fun onSuccess(data: TripDetailsResponse) {
                    travelSummaryResult.value = data
                }

                override fun onError(e: ErrorData) {
                    Timber.d("API failed: " + e.toString())
                }

            })
    }

    fun getUpcomingTripDetails(tripPlannerId: Long) {
        apiHelper.getUpcomingTripDetails(tripPlannerId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : ApiObserver<UpcomingTripDetailResponse>(compositeDisposable) {
                override fun onSuccess(data: UpcomingTripDetailResponse) {
                    println("%%%% : upcomign details")
                    upcomingTripDetails.value = data
                }

                override fun onError(e: ErrorData) {
                    println("%%%% : upcomign details error" + e.toString())
                }
            })
    }

    fun deleteTrips(tripId: Long) {
        apiHelper.deleteTrip(tripId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : ApiObserver<JsonElement>(compositeDisposable) {
                override fun onSuccess(data: JsonElement) {
                    deleteTripSuccessful.value = (true to tripId)
                }

                override fun onError(e: ErrorData) {
                    deleteTripSuccessful.value = (false to tripId)
                }

            })
    }

    fun deleteTripHistory(tripId: Long) {
        apiHelper.deleteTripHistory(tripId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : ApiObserver<JsonElement>(compositeDisposable) {
                override fun onSuccess(data: JsonElement) {
                    deleteTripHistorySuccessful.value = (true to tripId)
                }

                override fun onError(e: ErrorData) {
                    deleteTripHistorySuccessful.value = (false to tripId)
                }

            })
    }

    fun getTripHistoryListStream(
        startDate: String,
        endDate: String,
        searchText: String
    ): Flow<PagingData<TravelLogItem>> {
        return Pager(PagingConfig(10)) {
            TripSummaryPagingSource(apiHelper, startDate, endDate, searchText)
        }.flow.cachedIn(viewModelScope)
            .map { item ->
                item.map {
                    selectItem(isCurrentMonthSelected, it.tripId)
                    TravelLogItem.TripLogItem(isCurrentMonthSelected, it)
                }
            }
    }


    fun getObuVehicleList(
        vehicleNumbers: List<String>,
    ) {
        val requestData = ObuVehicleFilter()
        requestData.vehicleNumbers = vehicleNumbers
        apiHelper.getFilterVehicleDetails(requestData)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : ApiObserver<ObuVehicleResponse>(compositeDisposable) {
                override fun onSuccess(data: ObuVehicleResponse) {
                    vehicleDetailsResponseEvent.postValue(data)
                }

                override fun onError(e: ErrorData) {
                    vehicleDetailsResponseEvent.postValue(null)
                    //
                }
            })
    }

    fun getTripObuTransactionsListStream(
        obuName: String,
        startDate: String,
        endDate: String
    ): Flow<PagingData<TripObuTransactionItem>> {
        return Pager(PagingConfig(10)) {
            TripObuTransactionPagingSource(apiHelper, startDate, endDate, obuName, transactionDataType.value)
        }.flow
    }

    fun getTripPlannerListStream(
        startDate: String,
        endDate: String,
        searchText: String?
    ): Flow<PagingData<TripPlannerItem>> {
        val tripsPlanner: Flow<PagingData<TripPlannerItem>> = Pager(PagingConfig(10)) {
            TripsPlannerPagingSource(apiHelper, startDate, endDate, searchText)
        }.flow
        return tripsPlanner
    }

    fun sendTripLogEmail(request: SendTripLogRequest) {
        apiHelper.sendTripLogEmail(request)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : ApiObserver<JsonElement>(compositeDisposable) {
                override fun onSuccess(data: JsonElement) {
                    sendEmailSuccessful.value = true
                }

                override fun onError(e: ErrorData) {
                    sendEmailSuccessful.value = false
                }

            })
    }

    fun sendTripLogEmailForMonth(request: SendTripLogByMonthRequest) {
        apiHelper.sendTripLogEmailForMonth(request)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : ApiObserver<JsonElement>(compositeDisposable) {
                override fun onSuccess(data: JsonElement) {
                    sendEmailSuccessful.value = true
                }

                override fun onError(e: ErrorData) {
                    sendEmailSuccessful.value = false
                }

            })
    }

    fun selectRecordsOfMonth(adapter: TravelHistoryAdapter, isSelected: Boolean) {
        setCurrentMonthSelect(isSelected)
        var list = adapter.snapshot().items as ArrayList<TravelLogItem>
        list/*.filter {
            if (it is TravelLogItem.TripLogItem && it.tripLogItem.tripStartTime != null) {
                getMonthText(it.tripLogItem.tripStartTime!!).equals(monthName) && getYearText(it.tripLogItem.tripStartTime!!).equals(year)
            } else {
                if (it is TravelLogItem.MonthSeparatorItem) {
                    Timber.d(Constants.TAGS.SEARCH_TAG, "Month separator item: " + it.monthName + " " + it.isCheckboxSelected)
                }
                false
            }
        }*/.forEach {
            selectItem(isSelected, (it as TravelLogItem.TripLogItem).tripLogItem.tripId)
            (it as TravelLogItem.TripLogItem).isCheckboxSelected = isSelected
        }
        adapter.notifyDataSetChanged()
    }

    fun setCurrentMonthSelect(isSelected: Boolean) {
        isCurrentMonthSelected = isSelected
    }

    fun selectItem(isSelected: Boolean, tripId: Long) {
        if (selectedList.contains(tripId)) {
            if (!isSelected) {
                selectedList.remove(tripId)
            }
        } else {
            if (isSelected) {
                selectedList.add(tripId)
            }
        }
        Timber.d("Selected list is: " + selectedList.toString())
    }

    fun getSelectedList(): ArrayList<Long> {
        return selectedList
    }

    fun clearSelectedList() {
        selectedList = arrayListOf()
    }

    fun getUpcomingDateRangeStr() = if (upcomingEndDate != null && upcomingStartDate != null)
        "${upcomingStartDate?.formatDate(TimeConst.dMMMyy)} - ${
            upcomingEndDate?.formatDate(
                TimeConst.dMMMyy
            )
        }"
    else "All dates"

    fun getHistoryDateRangeStr() = if (historyEndDate != null && historyStartDate != null)
        "${historyStartDate?.formatDate(TimeConst.dMMMyy)} - ${historyEndDate?.formatDate(TimeConst.dMMMyy)}"
    else "All dates"

    fun getTransactionDateRangeStr() =
        if (transactionEndDate != null && transactionStartDate != null)
            "${transactionStartDate?.formatDate(TimeConst.dMMMyy)} - ${
                transactionEndDate?.formatDate(
                    TimeConst.dMMMyy
                )
            }"
        else "All dates"

}

sealed class TravelLogItem {
    data class TripLogItem(var isCheckboxSelected: Boolean, val tripLogItem: TripsItem) :
        TravelLogItem()
}
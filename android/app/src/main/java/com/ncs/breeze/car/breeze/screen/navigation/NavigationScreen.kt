package com.ncs.breeze.car.breeze.screen.navigation

import android.annotation.SuppressLint
import android.content.Intent
import androidx.car.app.CarToast
import androidx.car.app.ScreenManager
import androidx.car.app.model.*
import androidx.car.app.navigation.NavigationManager
import androidx.car.app.navigation.NavigationManagerCallback
import androidx.car.app.navigation.model.*
import androidx.core.graphics.drawable.IconCompat
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.breeze.model.BuildConfig
import com.breeze.model.ERPFeatures
import com.breeze.model.TripType
import com.breeze.model.api.response.eta.RecentETAContact
import com.breeze.model.enums.ETAMode
import com.breeze.model.extensions.round
import com.mapbox.android.core.permissions.PermissionsManager
import com.mapbox.androidauto.internal.logAndroidAuto
import com.mapbox.androidauto.map.compass.CarCompassRenderer
import com.mapbox.androidauto.navigation.speedlimit.CarSpeedLimitRenderer
import com.mapbox.geojson.FeatureCollection
import com.mapbox.navigation.base.ExperimentalPreviewMapboxNavigationAPI
import com.mapbox.navigation.base.trip.model.RouteLegProgress
import com.mapbox.navigation.base.trip.model.RouteProgress
import com.mapbox.navigation.core.lifecycle.MapboxNavigationApp
import com.mapbox.navigation.core.trip.session.TripSessionState
import com.ncs.breeze.App
import com.ncs.breeze.R
import com.ncs.breeze.car.breeze.ReplayRouteTripSession
import com.ncs.breeze.car.breeze.base.BaseNavigationScreenCar
import com.ncs.breeze.car.breeze.ehorizon.CarRoadObjectRenderer
import com.ncs.breeze.car.breeze.location.CarLocationRenderer
import com.ncs.breeze.car.breeze.model.ETARouteDetails
import com.ncs.breeze.car.breeze.screen.carkpark.CarParkCarContext
import com.ncs.breeze.car.breeze.screen.carkpark.CarParkScreen
import com.ncs.breeze.car.breeze.screen.obulite.CarOBULiteScreen
import com.ncs.breeze.car.breeze.screen.obustatus.OBUDeviceConnectionStatusScreen
import com.ncs.breeze.car.breeze.screen.search.etaList.ETAListScreen
import com.ncs.breeze.car.breeze.style.CarNavigationStyle
import com.ncs.breeze.car.breeze.utils.*
import com.ncs.breeze.car.breeze.widget.roadlabel.CarRoadLabelRenderer
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.Event
import com.ncs.breeze.common.analytics.Screen
import com.ncs.breeze.common.appSync.AppSyncHelper
import com.ncs.breeze.common.appSync.ClientFactory
import com.ncs.breeze.common.constant.OBUConnectionState
import com.ncs.breeze.common.constant.OBUConstants
import com.ncs.breeze.common.event.ToAppRx
import com.ncs.breeze.common.event.ToCarRx
import com.ncs.breeze.common.helper.ehorizon.EHorizonMode
import com.ncs.breeze.common.helper.obu.OBUDataHelper
import com.ncs.breeze.common.model.rx.AppToNavigationEndEvent
import com.ncs.breeze.common.model.rx.AppToPhoneNavigationEndEvent
import com.ncs.breeze.common.model.rx.CarIncidentDataRefresh
import com.ncs.breeze.common.model.rx.ETAMessageReceivedCar
import com.ncs.breeze.common.model.rx.ETAStartedCar
import com.ncs.breeze.common.model.rx.PhoneNavigationStartEventData
import com.ncs.breeze.common.model.rx.ToCarRxEvent
import com.ncs.breeze.common.model.rx.TriggerCarToastEvent
import com.ncs.breeze.common.model.rx.UpdateETACar
import com.ncs.breeze.common.model.rx.UpdateStateVoiceCar
import com.ncs.breeze.common.rxbus.RxBus
import com.ncs.breeze.common.rxbus.RxEvent
import com.ncs.breeze.common.utils.*
import com.ncs.breeze.common.utils.eta.ETAEngine
import com.ncs.breeze.service.NavigationControllerService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.*

/**
 * After a route has been selected. This view gives turn-by-turn instructions
 * for completing the route.
 */
class NavigationScreen(
    private val carNavigationCarContext: CarNavigationCarContext,
) : BaseNavigationScreenCar(carNavigationCarContext.mainCarContext), NavigationManagerCallback {
    private var isConnected = false
    private var currentOBUBalance: Double? = null
    private val lifecycleObserver = object : DefaultLifecycleObserver {
        override fun onCreate(owner: LifecycleOwner) {
            CarNavigationScreenState.state.postValue(owner.lifecycle.currentState)
            observeAutoDrive()
        }

        override fun onStart(owner: LifecycleOwner) {
            CarNavigationScreenState.state.postValue(owner.lifecycle.currentState)
            changeOBUConnectionState((carContext.applicationContext as? App)?.obuConnectionHelper?.obuConnectionState?.value == OBUConnectionState.CONNECTED)
        }

        override fun onResume(owner: LifecycleOwner) {
            super.onResume(owner)
            listenOBUEvent()
        }

        override fun onPause(owner: LifecycleOwner) {
            super.onPause(owner)
            clearOBUEventListener()
        }

        override fun onStop(owner: LifecycleOwner) {
            CarNavigationScreenState.movedToAnotherScreen =
                carContext.getCarService(ScreenManager::class.java).top !is NavigationScreen
            CarNavigationScreenState.state.postValue(owner.lifecycle.currentState)

        }

        override fun onDestroy(owner: LifecycleOwner) {
            CarNavigationScreenState.state.postValue(owner.lifecycle.currentState)
            CarNavigationScreenState.movedToAnotherScreen = false
        }
    }

    private fun listenOBUEvent() {
        compositeDisposable.add(
            RxBus.listen(RxEvent.OBURoadEvent::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    obuNotificationHelper.showRoadEventNotification(
                        it.eventData,
                        carRoadLabelRenderer.getCurrentRoadName(),
                        SpeedLimitUtil.currentLocation?.latitude ?: 0.0,
                        SpeedLimitUtil.currentLocation?.longitude ?: 0.0,
                        Screen.AA_DHU_NAVIGATION
                    )
                    if (it.eventData.shouldCheckLowCardBalance()) {
                        delayCheckShowingLowCardBalance()
                    }
                }
        )
        compositeDisposable.add(
            RxBus.listen(RxEvent.OBULowCardBalanceEvent::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    showLowCardBalanceNoti(it.balance / 100.0)
                }
        )
        compositeDisposable.add(
            RxBus.listen(RxEvent.OBUParkingAvailabilityEvent::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    obuNotificationHelper.showParkingAvailabilityNotification(
                        it.data,
                        Screen.AA_DHU_NAVIGATION,
                        carRoadLabelRenderer.getCurrentRoadName(),
                        SpeedLimitUtil.currentLocation
                    )
                }
        )
        compositeDisposable.add(
            RxBus.listen(RxEvent.OBUTravelTimeEvent::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    obuNotificationHelper.showTravelTimeEventNotification(
                        it.data,
                        Screen.AA_DHU_NAVIGATION,
                        carRoadLabelRenderer.getCurrentRoadName(),
                        SpeedLimitUtil.currentLocation
                    )
                }
        )
    }


    private fun delayCheckShowingLowCardBalance() {
        lifecycleScope.launch(Dispatchers.IO) {
            delay(OBUConstants.OBU_LOW_CARD_ALERT_DELAY)
            OBUDataHelper.getOBUCardBalanceSGD()
                ?.takeIf { isActive && it < OBUConstants.OBU_LOW_CARD_THRESHOLD / 100.0 }
                ?.let {
                    showLowCardBalanceNoti(it)
                }
        }
    }

    private fun showLowCardBalanceNoti(balanceSGD: Double) {
        if (lifecycle.currentState.isAtLeast(Lifecycle.State.RESUMED)) {
            lifecycleScope.launch(Dispatchers.Main) {
                obuNotificationHelper.showLowCardNotification(
                    cardBalanceSGD = balanceSGD,
                    screenName = Screen.AA_DHU_NAVIGATION
                )
            }
        }
    }

    private fun clearOBUEventListener() {
        compositeDisposable.clear()
    }

    private val obuNotificationHelper by lazy { OBUNotificationHelper(this) }

    init {
        lifecycle.addObserver(lifecycleObserver)
    }


    private val speedLimitHandler = object : SpeedLimitHandler {
        override fun alertOnThreshold() {
            //VoiceInstructionsManager.getInstance()?.playVoiceInstructions(carNavigationCarContext.mainCarContext.carContext.getString(R.string.voice_speed_limit_alert))
            BreezeAudioUtils.playOverSpeedAlert(carNavigationCarContext.carContext)
        }
    }

    private var carDynamicStyle =
        CarNavigationStyle(
            carNavigationCarContext,
            carNavigationCarContext.destinationAddressDetails
        )
    private val carRouteLine = CarNavigationRouteLine(carNavigationCarContext)
    private val carLocationRenderer = CarLocationRenderer(carNavigationCarContext.mainCarContext)
    private val carSpeedLimitRenderer = CarSpeedLimitRenderer(mainCarContext.mapboxCarContext)
    private val carRoadLabelRenderer = CarRoadLabelRenderer()
    private val carCompassRenderer = CarCompassRenderer()
    private val carNavigationCamera = CarNavigationCamera(
        mainCarContext,
        carNavigationCarContext,
        CarNavigationCamera.CameraMode.FOLLOWING
    )

    //EHorizonObserver
    private val carMapViewLayer = RoadLabelSurfaceLayer(carNavigationCarContext.carContext)

    private val breezeCarNavigationVoiceAction = BreezeCarNavigationVoiceAction(this)

    private val carRouteProgressObserver = CarNavigationInfoObserver(carNavigationCarContext)

    private val carRoadObjectRenderer = CarRoadObjectRenderer(
        carNavigationCarContext.mainCarContext,
        EHorizonMode.NAVIGATION,
        carNavigationCarContext.route
    )
    private val incidentCompositeDisposable = CompositeDisposable()
    private var mIsNavigating = false
    private var isArrived = false
    private var maxDistance: Int = 0

    override fun onGetTemplate(): Template {
        logAndroidAuto("CarNavigateScreen onGetTemplate")
        val builder = NavigationTemplate.Builder()
            .setBackgroundColor(
                CarColor.createCustom(
                    carContext.getColor(R.color.color_theme_bg),
                    carContext.getColor(R.color.color_theme_bg)
                )
            )
            .setActionStrip(buildActionStrip())

        if (!isArrived) {
            //Top left side, road name, maneuver, direction icon
            carRouteProgressObserver.navigationInfo?.let {
                builder.setNavigationInfo(it) //Ankur
            }
            //bottom left travel estimation progress with time and distance.
            carRouteProgressObserver.travelEstimateInfo?.let {
                builder.setDestinationTravelEstimate(it)
            }
        } else {
            builder.setNavigationInfo(
                carRouteProgressObserver.getArrivedNavigationInfo(
                    destinationTitle = getDestinationName(),
                    isDestinationCarPark = carNavigationCarContext.destinationAddressDetails.isDestinationCarPark()
                )
            )
        }
        updateTripInfo(
            carRouteProgressObserver.navigationInfo,
            carRouteProgressObserver.travelEstimateInfo
        )

        return builder.build()
    }

    private fun getDestinationName(): String {
        return if (!carNavigationCarContext.destinationAddressDetails.destinationName.isNullOrEmpty()) {
            carNavigationCarContext.destinationAddressDetails.destinationName!!
        } else {
            if (!carNavigationCarContext.destinationAddressDetails.address1.isNullOrEmpty()) {
                carNavigationCarContext.destinationAddressDetails.address1!!
            } else if (!carNavigationCarContext.destinationAddressDetails.address2.isNullOrEmpty()) {
                carNavigationCarContext.destinationAddressDetails.address2!!
            } else {
                "Destination"
            }
        }
    }

    private fun updateTripInfo(
        navigationInfo: NavigationTemplate.NavigationInfo?,
        travelEstimateInfo: TravelEstimate?
    ) {
        val tripBuilder = Trip.Builder()

        var destinationName: String? = null
        var destinationAddress: String? = null
        var destinationIcon: CarIcon? = null
        var currentRoadName: String? = null
        var currentStep: Step? = null
        var currentStepEstimation: TravelEstimate? = null
        var nextStep: Step? = null
        var nextStepEstimation: TravelEstimate? = null

        carNavigationCarContext.destinationAddressDetails.let { destination ->
            destinationName = destination.destinationName
            destinationAddress = (destination.address1 ?: "") + (destination.address2 ?: "")
        }

        //https://github.com/mapbox-collab/ncs-collab/issues/441
        //Need to sync with Mapbox team and update the logic below
        //-ANKUR

        navigationInfo?.let { navInfo ->
            val routingInfo = navInfo as RoutingInfo
            routingInfo.let { info ->
                info.nextStep?.let { it ->
                    nextStepEstimation = TravelEstimate.Builder(
                        Distance.create(1.0, Distance.UNIT_KILOMETERS),
                        DateTimeWithZone.create(
                            System.currentTimeMillis() + (60 * 1000),
                            TimeZone.getTimeZone("SG")
                        )
                    ).build()
                    nextStep = it
                    destinationIcon = it.lanesImage
                }
                info.currentStep?.let { it ->
                    currentStepEstimation = TravelEstimate.Builder(
                        Distance.create(1.0, Distance.UNIT_KILOMETERS),
                        DateTimeWithZone.create(
                            System.currentTimeMillis() + (60 * 1000),
                            TimeZone.getTimeZone("SG")
                        )
                    ).build()
                    currentRoadName = it.road.toString()
                    currentStep = it
                    destinationIcon = it.lanesImage
                }
            }
        }

        val destinationBuilder = Destination.Builder()
        destinationBuilder.setName(destinationName ?: "N/A")
        destinationBuilder.setAddress(destinationAddress ?: "N/A")
        destinationBuilder.setImage(
            destinationIcon ?: CarIcon.Builder(
                IconCompat.createWithResource(
                    carContext,
                    R.drawable.destination_puck
                )
            ).build()
        )

        if (travelEstimateInfo != null) {
            tripBuilder.addDestination(
                destinationBuilder.build(),
                travelEstimateInfo
            )
        }
        tripBuilder.setCurrentRoad(currentRoadName ?: "N/A")
        if (currentStep != null && currentStepEstimation != null) {
            tripBuilder.addStep(currentStep!!, currentStepEstimation!!)
        }
        if (nextStep != null && nextStepEstimation != null) {
            tripBuilder.addStep(nextStep!!, nextStepEstimation!!)
        }
        tripBuilder.setLoading(false)

        if (mIsNavigating) {
            kotlin.runCatching {
                getNavigationManager().updateTrip(tripBuilder.build())
            }
        }
    }

    @SuppressLint("UnsafeOptInUsageError")
    private fun buildActionStrip(): ActionStrip {
        val actionStripBuilder = ActionStrip.Builder()
        if (!isArrived) {
            actionStripBuilder.addAction(breezeCarNavigationVoiceAction.buildOnOffAction())
//            if (maxDistance > 50) {
//                actionStripBuilder.addAction(buildETAToggle())
//            }
        }
        actionStripBuilder.addAction(buildCarParkAction())
        actionStripBuilder.addAction(buildOBUAction())
        actionStripBuilder.addAction(
            Action.Builder()
                .setFlags(Action.FLAG_IS_PERSISTENT)
                .setIcon(
                    CarIcon.Builder(
                        IconCompat.createWithResource(
                            carContext,
                            R.drawable.ic_aa_close
                        )
                    ).build()
                )
                .setClickSafe {
                    LimitClick.handleSafe {
                        Analytics.logClickEvent(Event.AA_END, Screen.ANDROID_AUTO_NAVIGATION)
                        if (isArrived) {
                            closeNavigationAndExit()
                        } else {
                            handleNavigationStopped()
                        }
                    }
                }
                .build()
        )
        return actionStripBuilder.build()
    }


    @SuppressLint("UnsafeOptInUsageError")
    private fun buildOBUAction(): Action {
        val actionBuilder = Action.Builder()
            .setFlags(Action.FLAG_IS_PERSISTENT)
            .setTitle(
                if (isConnected)
                    "$${currentOBUBalance?.round(2) ?: "-"}"
                else "OBU"
            )
            .setClickSafe {
                mainCarContext.carContext.getCarService(ScreenManager::class.java).push(
                    OBUDeviceConnectionStatusScreen(
                        mainCarContext,
                        fromScreen = Screen.ANDROID_AUTO_NAVIGATION
                    )
                )
                if (!isConnected)
                    Analytics.logClickEvent(Event.AA_OBU_X, Screen.ANDROID_AUTO_NAVIGATION)

            }
        return actionBuilder.build()
    }

    @SuppressLint("UnsafeOptInUsageError")
    private fun buildCarParkAction(): Action {
        val carIconBuilder = CarIcon.Builder(
            IconCompat.createWithResource(
                carContext, R.drawable.ic_icon_car_pack
            )
        )

        return Action.Builder()
            .setFlags(Action.FLAG_IS_PERSISTENT)
            .setIcon(carIconBuilder.build())
            .setClickSafe {
                Analytics.logClickEvent(Event.NAVIGATION_CARPARKS, Screen.ANDROID_AUTO_NAVIGATION)
                screenManager.push(
                    CarParkScreen(
                        CarParkCarContext(mainCarContext),
                        originalDestination = null,
                        fromScreen = Screen.ANDROID_AUTO_NAVIGATION
                    )
                )
            }
            .build()
    }

    private fun buildETAToggle(): Action {
        val iconETA =
            when {
                ETAEngine.getInstance()!!.isETARunning() -> {
                    R.drawable.ic_eta_start_green
                }

                ETAEngine.getInstance()!!.isETAPause() -> {
                    R.drawable.ic_eta_paused_no_bg
                }

                else -> {
                    R.drawable.ic_eta_start_no_bg
                }
            }
        return Action.Builder()
            .setIcon(
                CarIcon.Builder(
                    IconCompat.createWithResource(
                        carContext, iconETA
                    )
                ).build()
            )
            .setClickSafe {
                LimitClick.handleSafe {
                    handleETAActionStripClick()
                }
            }
            .build()
    }

    private fun handleETAActionStripClick() {
        when {
            ETAEngine.getInstance()!!.isETARunning() || ETAEngine.getInstance()!!
                .isETAPause() -> {
                ETAEngine.getInstance()!!.toggleLiveLocation()
            }

            ETAEngine.getInstance()!!.isETADisabled() -> {
                carContext
                    .getCarService(ScreenManager::class.java)
                    .push(
                        ETAListScreen(
                            mainCarContext,
                            ETAMode.Navigation,
                            ETARouteDetails(
                                carNavigationCarContext.destinationAddressDetails,
                                carNavigationCarContext.route
                            )
                        ) { result ->
                            if (result != null) {
                                val contactItem = result as RecentETAContact
                                val etaRequest =
                                    ETAEngine.getInstance()!!.createNewETARequest(
                                        ETAMode.Navigation,
                                        contactItem,
                                        carNavigationCarContext.destinationAddressDetails,
                                        carContext.getString(
                                            R.string.car_share_message_navigation,
                                            BreezeCarUtil.breezeUserPreferenceUtil.retrieveUserName(),
                                            carNavigationCarContext.destinationAddressDetails.address1,
                                            Utils.formatTime((carNavigationCarContext.route.duration() * 1000).toLong() + System.currentTimeMillis())
                                        )
                                    )
                                ETAEngine.updateETPRequest(etaRequest)
                                ETAEngine.getInstance()?.let {
                                    it.startETA(carNavigationCarContext.route)
                                    LocationBreezeManager.getInstance()
                                        .registerLocationCallback(
                                            it.getEtaLocationUpdateInstance(ETAMode.Navigation)
                                        )
                                }
                            }
                        }
                    )
            }
        }
    }

    private fun handleNavigationArrived() {
        stopNavigation(isArrivedAtDestination = true)
        ToCarRx.postEvent(
            TriggerCarToastEvent.create(
                title = carNavigationCarContext.carContext.getString(R.string.we_have_arrived)
            )
        )
    }

    private fun handleNavigationStopped() {
        stopNavigation(isArrivedAtDestination = false)
    }

    @OptIn(ExperimentalPreviewMapboxNavigationAPI::class)
    private fun stopNavigation(isArrivedAtDestination: Boolean) {
        logAndroidAuto("CarNavigateScreen stopNavigation")
        mIsNavigating = false
        BreezeEHorizonProvider.destroy()
        ETAEngine.getInstance()!!.stopETA(isArrivedAtDestination)
        TripLoggingManager.getInstance()?.stopTripLogging(null, Screen.AA_NAVIGATION_SCREEN)
        VoiceInstructionsManager.getInstance()?.unRegisterVoiceInstruction()
        if (!isArrivedAtDestination) {
            VoiceInstructionsManager.getInstance()?.cancelSpeech()
            closeNavigationAndExit()
        } else {
            BreezeArrivalObserver.getInstance()?.unregisterNavigationArrivalObserver(
                navigationArrivalObserver,
                shouldStopTrip = false
            )
            invalidate()
        }
    }

    private fun closeNavigationAndExit() {
        Utils.isNavigationStarted = false
        Utils.phoneNavigationStartEventData = null

        MapboxNavigationApp.current()
            ?.setNavigationRoutes(emptyList())
        LocationBreezeManager.getInstance().removeLocationCallBack(
            ETAEngine.getInstance()!!.getEtaLocationUpdateInstance(ETAMode.Navigation)
        )
        if (MapboxNavigationApp.current()?.getTripSessionState() == TripSessionState.STARTED) {
            MapboxNavigationApp.current()?.stopTripSession()
            MapboxNavigationApp.current()?.resetTripSession {}
        }

        BreezeArrivalObserver.getInstance()?.unregisterNavigationArrivalObserver(
            navigationArrivalObserver,
            shouldStopTrip = true
        )
        SpeedLimitUtil.gc()
        exitNavigationScreen()
    }

    private fun exitNavigationScreen() {
        ToCarRx.postEventWithCacheLast(AppToNavigationEndEvent())
        ToAppRx.postEvent(AppToPhoneNavigationEndEvent())
    }

    override fun getScreenName(): String {
        return Screen.AA_NAVIGATION_SCREEN
    }

    @SuppressLint("MissingPermission")
    override fun onCreateScreen() {
        super.onCreateScreen()
        logAndroidAuto("CarNavigateScreen onCreate")

        Utils.isNavigationStarted = true
        startFeatureDetection()

        compositeDisposable.add(
            RxBus.listen(RxEvent.SpeedChanged::class.java)
                .subscribe {
                    when (it.value) {
                        SpeedLimitUtil.NORMAL_SPEED -> {
                            SpeedLimitUtil.setAARedBorder(false)
                        }

                        SpeedLimitUtil.OVER_SPEED -> {
                            SpeedLimitUtil.setAARedBorder(true)
                        }
                    }
                })

//        getNavigationManagaer().setNavigationManagerCallback(this)
        if (!mIsNavigating) {
            getNavigationManager().navigationStarted()
            mIsNavigating = true
        }

        MapboxNavigationApp.current()
            ?.takeIf { mapboxNavigation ->
                mapboxNavigation.getTripSessionState() != TripSessionState.STARTED
            }?.run {
                ClientFactory.clearMutationCache()
                startTripSession()
                carContext.startService(Intent(carContext, NavigationControllerService::class.java))
            }

        maxDistance = carNavigationCarContext.route.distance().toInt()

        if (carNavigationCarContext.etaRequest != null) {
            ETAEngine.getInstance()?.let {
                it.startETA(carNavigationCarContext.route)
                LocationBreezeManager.getInstance()
                    .registerLocationCallback(it.getEtaLocationUpdateInstance(ETAMode.Navigation))
            }

        }
    }

    private fun startFeatureDetection() {
        CoroutineScope(Dispatchers.IO).launch {

            BreezeCarUtil.breezeFeatureDetectionUtil.initFeatureDetectionRoadObjects()
            BreezeCarUtil.breezeERPUtil.startERPHandler()
            BreezeCarUtil.breezeIncidentsUtil.startIncidentHandler()
            BreezeCarUtil.breezeMiscRoadObjectUtil.startMiscROHandler()
            BreezeCarUtil.breezeSchoolSilverZoneUpdateUtil.startSchoolSilverZoneHandler()
            incidentCompositeDisposable.clear()
            incidentCompositeDisposable.add(
                RxBus.listen(RxEvent.IncidentDataRefresh::class.java)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe {
                        ToCarRx.postEvent(CarIncidentDataRefresh(it.incidentData))
                    }
            )
            incidentCompositeDisposable.add(
                RxBus.listen(RxEvent.ERPRefresh::class.java)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe {
                        it.erpFeature?.let {
                            handleERPRefreshEcent()
                        }
                    }
            )
            AppSyncHelper.subscribeToAppSyncEvents(mainCarContext.carContext.applicationContext)

        }
    }

    private fun handleERPRefreshEcent() {
        CoroutineScope(Dispatchers.Default).launch {
            //First get the latest values and prices of ERPs
            carDynamicStyle.currentRouteERPInfo =
                BreezeCarUtil.breezeERPUtil.getERPInRoute(
                    BreezeCarUtil.breezeERPUtil.getERPData()!!,
                    carNavigationCarContext.route,
                    false
                )
            //Now draw the ERPs on the route with relevant info
            carDynamicStyle.currentRouteERPInfo?.let {
                for (feature in it.featureCollection.features()!!) {
                    BreezeCarUtil.breezeERPUtil.getCurrentERPData()?.mapOfERP?.get(
                        feature.getStringProperty(
                            RoutePlanningConsts.PROP_ERP_ID
                        )
                    )?.let { rate ->
                        feature.addStringProperty(
                            RoutePlanningConsts.PROP_ERP_DISPLAY,
                            String.format("$%.2f", rate.charge)
                        )
                    }
                }
                withContext(Dispatchers.Main) {
                    updateNavigationERPLayer(carDynamicStyle.currentRouteERPInfo!!.featureCollection)
                }
            }
        }
    }

    override fun onDestroyScreen() {
        super.onDestroyScreen()
        logAndroidAuto("CarNavigateScreen onDestroy")
        stopFeatureDetectionAndDispose()

        getNavigationManager().navigationEnded()
//        getNavigationManagaer().clearNavigationManagerCallback()
        mIsNavigating = false

        carRouteProgressObserver.stop()
        NavigationNotificationUtils.cancelTurnByTurnNavigationNotification()
        compositeDisposable.dispose()
        lifecycle.removeObserver(lifecycleObserver)
    }

    private fun stopFeatureDetectionAndDispose() {
        val topScreen = mainCarContext.carContext.getCarService(
            ScreenManager::class.java
        ).top
        CoroutineScope(Dispatchers.IO).launch {

            if (topScreen !is CarOBULiteScreen) {
                BreezeCarUtil.breezeFeatureDetectionUtil.stopFeatureDetection()
            }
            incidentCompositeDisposable.dispose()
        }
    }

    override fun updateERPLayer(erpFeature: ERPFeatures?) {
        // NO OP
    }

    fun updateNavigationERPLayer(erpFeature: FeatureCollection) {
        mainCarContext.mapboxCarMap.carMapSurface?.mapSurface?.getMapboxMap()?.getStyle {
            BreezeMapBoxUtil.addNavigationERPLayerToStyle(
                it,
                erpFeature,
                mainCarContext.carContext
            )
        }
    }


    @SuppressLint("MissingPermission")
    override fun onStartScreen() {
        super.onStartScreen()
        logAndroidAuto("CarNavigateScreen onStart")
        changeOBUConnectionState((carContext.applicationContext as? App)?.obuConnectionHelper?.obuConnectionState?.value == OBUConnectionState.CONNECTED)
        val phoneData = PhoneNavigationStartEventData(
            carNavigationCarContext.originalDestination,
            carNavigationCarContext.destinationAddressDetails,
            carNavigationCarContext.route,
            carNavigationCarContext.compressedRouteOriginalResp,
            carNavigationCarContext.erpData,
            carNavigationCarContext.etaRequest
        )
        Utils.phoneNavigationStartEventData = phoneData

        VoiceInstructionsManager.setupVoice(carContext.applicationContext)
        VoiceInstructionsManager.getInstance()?.registerVoiceInstruction()
        carNavigationCarContext.mapboxCarMap.registerObserver(carDynamicStyle)
        carNavigationCarContext.mapboxCarMap.registerObserver(carLocationRenderer)
        carNavigationCarContext.mapboxCarMap.registerObserver(carSpeedLimitRenderer)
        carNavigationCarContext.mapboxCarMap.registerObserver(carCompassRenderer)
        carNavigationCarContext.mapboxCarMap.registerObserver(carRoadLabelRenderer)
        carNavigationCarContext.mapboxCarMap.registerObserver(carNavigationCamera)
        carNavigationCarContext.mapboxCarMap.registerObserver(carRouteLine)
        carNavigationCarContext.mapboxCarMap.registerObserver(carMapViewLayer)
        carNavigationCarContext.mapboxCarMap.registerObserver(carRoadObjectRenderer)
        LocationBreezeManager.getInstance().startLocationUpdates()
        TripLoggingManager.setupTripLogging(
            carContext.applicationContext,
            BreezeCarUtil.breezeTripLoggingUtil,
            BreezeCarUtil.breezeStatisticsUtil
        )
        TripLoggingManager.getInstance()
            ?.startTripLogging(
                TripType.NAVIGATION,
                Screen.AA_NAVIGATION_SCREEN,
                carNavigationCarContext.destinationAddressDetails
            )
        BreezeArrivalObserver.create(
            MapboxNavigationApp.current(),
            Screen.AA_NAVIGATION_SCREEN,
            carNavigationCarContext.destinationAddressDetails
        )
        BreezeArrivalObserver.getInstance()
            ?.registerNavigationArrivalObserver(navigationArrivalObserver)
        carRouteProgressObserver.start {
            invalidate()
        }

        SpeedLimitUtil.setupLocationObserver(MapboxNavigationApp.current(), speedLimitHandler)
    }

    override fun onStopScreen() {
        super.onStopScreen()
        logAndroidAuto("CarNavigateScreen onStop")
        carNavigationCarContext.mapboxCarMap.unregisterObserver(carDynamicStyle)
        carNavigationCarContext.mapboxCarMap.unregisterObserver(carLocationRenderer)
        carNavigationCarContext.mapboxCarMap.unregisterObserver(carSpeedLimitRenderer)
        carNavigationCarContext.mapboxCarMap.unregisterObserver(carCompassRenderer)
        carNavigationCarContext.mapboxCarMap.unregisterObserver(carRoadLabelRenderer)
        carNavigationCarContext.mapboxCarMap.unregisterObserver(carNavigationCamera)
        carNavigationCarContext.mapboxCarMap.unregisterObserver(carRouteLine)
        carNavigationCarContext.mapboxCarMap.unregisterObserver(carMapViewLayer)
        carNavigationCarContext.mapboxCarMap.unregisterObserver(carRoadObjectRenderer)
        //carRouteProgressObserver.stop()

        SpeedLimitUtil.removeLocationObserver(MapboxNavigationApp.current())
    }

    private fun getNavigationManager(): NavigationManager {
        return mainCarContext.carContext
            .getCarService(NavigationManager::class.java)
    }

    override fun onReceiverEventFromApp(event: ToCarRxEvent?) {
        when (event) {
            is UpdateStateVoiceCar -> {
                invalidate()
            }

            is UpdateETACar -> {
                handleETALiveLocationToggleEventCar(
                    event.isETARunning,
                    event.recipientName
                )
            }

            is ETAStartedCar -> {
                CarToast.makeText(
                    carContext,
                    mainCarContext.carContext.getString(R.string.car_share_confirmation_toast),
                    CarToast.LENGTH_LONG
                )
                invalidate()
            }

            is ETAMessageReceivedCar -> {
                handleETAStartEventCar(event.etaMessage)
            }

            else -> {}
        }

    }

    private fun handleETAStartEventCar(etaMessage: String) {
        CarToast.makeText(carContext, etaMessage, CarToast.LENGTH_LONG)
    }

    //ToDo - replace RxToCarRxEvent with direct method calls
    private fun handleETALiveLocationToggleEventCar(
        isETARunning: Boolean,
        recipientName: String
    ) {
        CarToast.makeText(
            carContext, if (isETARunning) {
                carContext.getString(R.string.eta_notification_resumed, recipientName)
            } else {
                carContext.getString(R.string.eta_notification_paused, recipientName)

            }, CarToast.LENGTH_LONG
        )

        invalidate()
    }

    private val navigationArrivalObserver = object : NavigationArrivalObserver {
        override fun onFinalDestinationArrival(routeProgress: RouteProgress) {
            isArrived = true
            handleNavigationArrived()
        }

        override fun onNextRouteLegStart(routeProgress: RouteLegProgress) {

        }

        override fun onWaypointArrival(routeProgress: RouteProgress) {
        }

    }


    private fun showTunnelToast() {
        //ToDo - check if this is required
    }

    init {
        logAndroidAuto("CarNavigateScreen constructor")
    }

//    override fun onStopNavigation() {
//        if (isArrived) {
//            closeNavigationAndExit()
//        } else {
//            handleNavigationStopped()
//        }
//    }

    // Enable auto drive. Open the app on the head unit and then execute the following from your
    // computer terminal.
    // adb shell dumpsys activity service com.ncs.breeze.car.BreezeCarAppService AUTO_DRIVE

    private fun observeAutoDrive() {
        if (BuildConfig.DEBUG) {
            refreshTripSession(App.instance?.isCarSessionAlive == true)
        } else {
            lifecycleScope.launch {
                lifecycle.repeatOnLifecycle(Lifecycle.State.STARTED) {
                    mainCarContext.mapboxCarContext.mapboxNavigationManager.autoDriveEnabledFlow
                        .collectLatest { isAutoDriveEnabled ->
                            refreshTripSession(isAutoDriveEnabled)
                            CarNavigationScreenState.carAutoDriveState.postValue(isAutoDriveEnabled)
                        }
                }
            }
        }
    }


    @SuppressLint("MissingPermission")
    private fun refreshTripSession(isAutoDriveEnabled: Boolean) {
        val mapboxNavigation = MapboxNavigationApp.current()
        if (!PermissionsManager.areLocationPermissionsGranted(carContext)) {
            mapboxNavigation?.stopTripSession()
            return
        }
        MapboxNavigationApp.unregisterObserver(ReplayRouteTripSession)
        if (isAutoDriveEnabled) {
            MapboxNavigationApp.registerObserver(ReplayRouteTripSession)
        } else {
            mapboxNavigation
                ?.takeIf { it.getTripSessionState() != TripSessionState.STARTED }
                ?.run {
                    ClientFactory.clearMutationCache()
                    startTripSession()
                }
        }
    }

    override fun changeOBUConnectionState(connected: Boolean) {
        if (connected != this.isConnected) {
            this.isConnected = connected
            this.currentOBUBalance = OBUDataHelper.getOBUCardBalanceSGD()
            invalidate()
        }
    }

    override fun updateCardBalance(cardBalance: Double?) {
        if (cardBalance != currentOBUBalance) {
            currentOBUBalance = cardBalance
            invalidate()
        }
    }
}

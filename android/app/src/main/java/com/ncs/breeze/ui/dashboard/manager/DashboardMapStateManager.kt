package com.ncs.breeze.ui.dashboard.manager

import android.location.Location
import com.ncs.breeze.common.constant.dashboard.LandingMode
import com.breeze.model.DestinationAddressDetails
import com.breeze.model.extensions.safeDouble
import com.ncs.breeze.ui.dashboard.LocationFocus

object DashboardMapStateManager : LocationFocus {

    const val AMENITY_INSTANCE = 0
    const val TRAFFIC_INSTANCE = 1

    // todo: this one should be replaced by flow variable in Dashboard's ViewModel
    var currentModeSelected = LandingMode.PARKING
    var selectedMapProfile: String? = null
    var isInMapProfileSelectedMode = false

    override var activeLocationUse: Location? = null

    var isInSearchLocationMode = false

    var landingBottomSheetHeight = 1000
    var landingBottomSheetState = 0

    override var currentLocationFocus: Location? = null
    override var forceFocusZoom: Double? = null
    override var currentFocusInstance: Int? = null

    var searchLocationAddress: DestinationAddressDetails? = null


    var selectedMapProfileLocation: Location? = null
    var selectedMapProfileLocationAddress: DestinationAddressDetails? = null
        set(value) {
            field = value
            value?.let { address ->
                selectedMapProfileLocation = Location("")
                selectedMapProfileLocation?.latitude = address.lat?.safeDouble() ?: 0.0
                selectedMapProfileLocation?.longitude = address.long?.safeDouble() ?: 0.0
            }
        }

    override fun setCurrentLocationFocus(
        type: Int,
        pLatitude: Double,
        pLong: Double,
        zoomRange: Double?
    ) {
        currentLocationFocus = Location("").apply {
            latitude = pLatitude
            longitude = pLong
        }
        forceFocusZoom = zoomRange
        currentFocusInstance = type
    }

    override fun resetCurrentLocationFocus(type: Int) {
        if (currentFocusInstance == type) {
            currentLocationFocus = null
            forceFocusZoom = null
            currentFocusInstance = null
        }

    }

    override fun resetCurrentLocationFocus() {
        currentLocationFocus = null
        forceFocusZoom = null
        currentFocusInstance = null
    }

    fun shouldShowMapProfileZones(): Boolean {
        if (currentModeSelected == LandingMode.PARKING || currentModeSelected == LandingMode.OBU) {
            return isInMapProfileSelectedMode
        }
        return false
    }

    fun reset() {
        currentModeSelected = LandingMode.PARKING
        selectedMapProfile = null
        isInMapProfileSelectedMode = false
        activeLocationUse = null
        isInSearchLocationMode = false
        landingBottomSheetHeight = 1000
        landingBottomSheetState = 0
        currentLocationFocus = null
        forceFocusZoom = null
        currentFocusInstance = null
        searchLocationAddress = null
        selectedMapProfileLocation = null
        selectedMapProfileLocationAddress = null
    }


}
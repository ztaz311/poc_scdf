package com.ncs.breeze.common.utils

import android.content.Context
import android.text.TextUtils
import android.util.Log
import com.amazonaws.mobile.auth.core.internal.util.ThreadUtils
import com.amplifyframework.core.Amplify
import com.breeze.model.constants.Constants
import com.instabug.library.Instabug
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class BreezeInstabugUtil @Inject constructor(
    var appContext: Context,
    var userPreferenceUtil: BreezeUserPreferenceUtil
) {

    fun handleInstabugInvocation(isShowInstabug: Boolean = true) {
        Log.d("testing-instabug", "Instabug boolean: $isShowInstabug")
        Log.d(
            "testing-instabug",
            "The user group is:  ${userPreferenceUtil.retrieveInstabugGroup()}"
        )

        var userGroup = userPreferenceUtil.retrieveInstabugGroup()

        if (userGroup.isNullOrEmpty()) {
            AWSUtils.fetchUserAttributes({ result ->
                ThreadUtils.runOnUiThread {
                    Log.d("testing-instabug", "Inside success block of fetch user attribute")
                    var retrievedUserGroup = ""
                    for (item in result) {
                        if (TextUtils.equals(
                                item.key.keyString,
                                Constants.AMPLIFY_CONSTANTS.INSTABUG_USER_GROUP_ATTRIBUTE
                            )
                        ) {
                            retrievedUserGroup = item.value
                            break
                        }
                    }

                    if (retrievedUserGroup.isNotEmpty()) {
                        userPreferenceUtil.saveInstabugGroup(retrievedUserGroup)
                        Instabug.setUserAttribute(
                            Constants.AMPLIFY_CONSTANTS.INSTABUG_USER_GROUP_ATTRIBUTE_UI,
                            retrievedUserGroup
                        )
                    }
                }
            }, { error ->
                ThreadUtils.runOnUiThread {
                    Log.d(
                        "testing-instabug",
                        "Unable to retrieve user attribute - inside error block"
                    )
                }
            })
        } else {
            Instabug.setUserAttribute(
                Constants.AMPLIFY_CONSTANTS.INSTABUG_USER_GROUP_ATTRIBUTE_UI,
                userGroup
            )
        }

    }
}
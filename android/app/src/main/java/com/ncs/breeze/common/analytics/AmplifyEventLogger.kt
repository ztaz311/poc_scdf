package com.ncs.breeze.common.analytics

import android.os.Bundle
import com.amplifyframework.analytics.AnalyticsEvent
import com.amplifyframework.core.Amplify

/**
 * Created by Aiswarya on 17,June,2021
 */
class AmplifyEventLogger() : AnalyticsEventLogger {
    override fun logEvent(name: String, payload: Bundle) {
        val analyticsBuilder = AnalyticsEvent.builder().name(name)
        payload.keySet().forEach { key ->
            when (val value = payload.get(key)) {
                is Int -> analyticsBuilder.addProperty(key, value)
                is Double -> analyticsBuilder.addProperty(key, value)
                is Float -> analyticsBuilder.addProperty(key, value.toDouble())
                is String -> analyticsBuilder.addProperty(key, value)
                is Boolean -> analyticsBuilder.addProperty(key, value)
            }
        }
        Amplify.Analytics.recordEvent(analyticsBuilder.build())
    }

    override fun setUserId(cognitoUserName: String) {
        Amplify.Analytics.identifyUser(cognitoUserName, null)
    }

}
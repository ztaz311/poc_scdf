package com.ncs.breeze.components

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.breeze.model.api.response.ERPDetails
import com.breeze.model.extensions.round
import com.breeze.model.constants.Constants
import com.breeze.model.extensions.safeDouble
import com.ncs.breeze.R
import com.breeze.customization.view.BreezeTripEditText

class TripERPAdapter(
    private var dataList: List<TripListObject>,
    var listener: ERPAmountChangeListener
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        lateinit var viewHolder: RecyclerView.ViewHolder
        when (viewType) {
            0 -> {
                val view: View =
                    layoutInflater.inflate(R.layout.item_trip_erp_origin, parent, false)
                viewHolder = OriginViewHolder(view)
            }

            1 -> {
                val view: View =
                    layoutInflater.inflate(R.layout.item_trip_erp_destination, parent, false)
                viewHolder = DestinationViewHolder(view)
            }

            2 -> {
                val view: View =
                    layoutInflater.inflate(R.layout.item_trip_erp_location, parent, false)
                viewHolder = ERPViewHolder(view)
            }
        }
        return viewHolder
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = dataList[position]

        when (item.viewType.type) {
            0 -> {
                if (holder is OriginViewHolder && item.addressDetails != null) {
                    holder.populateData(item.addressDetails!!)
                }
            }

            1 -> {
                if (holder is DestinationViewHolder && item.addressDetails != null) {
                    holder.populateData(item.addressDetails!!)
                }
            }

            2 -> {
                if (holder is ERPViewHolder && item.erpItem != null) {
                    holder.populateData(item.erpItem!!)
                }
            }
        }

    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    override fun getItemViewType(position: Int): Int {
        return dataList[position].viewType.type
    }


    inner class ERPViewHolder internal constructor(itemView: View) :
        RecyclerView.ViewHolder(itemView) {

        var locationTextView: TextView = itemView.findViewById(R.id.location)
        var editedTextView: TextView = itemView.findViewById(R.id.edited)
        var costEditText: BreezeTripEditText = itemView.findViewById(R.id.erp_cost)
        var erpCostError: TextView = itemView.findViewById(R.id.erp_error)

        init {
            itemView.setOnClickListener {
                hideKeyboard(itemView)
            }
        }

        fun populateData(erpObject: ERPAdapterObject) {
            if (erpObject.erpDetails.actualAmount != erpObject.erpDetails.originalAmount) {
                editedTextView.visibility = View.VISIBLE
            } else {
                editedTextView.visibility = View.GONE
            }

            locationTextView.text = erpObject.erpDetails.name
            costEditText.enableEditText(erpObject.erpDetails.isERPEditable != false)
            costEditText.getEditText().setText(erpObject.erpDetails.actualAmount.round())

            costEditText.getEditText().addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(
                    s: CharSequence?, start: Int, count: Int, after: Int
                ) {
                }

                override fun onTextChanged(
                    editedERP: CharSequence?,
                    start: Int,
                    before: Int,
                    count: Int
                ) {
                    try {
                        var editedAmount = 0.0

                        if (!editedERP.toString().isNullOrEmpty()) {
                            editedAmount = editedERP.toString().safeDouble()
                        }

                        if (erpObject.erpDetails.originalAmount != editedAmount) {
                            editedTextView.visibility = View.VISIBLE
                        } else {
                            editedTextView.visibility = View.GONE
                        }
                        erpObject.erpDetails.actualAmount = editedAmount
                        listener.erpAmountEdited()

                        if (editedAmount > Constants.ERP_MAX_THRESHOLD) {
                            // Show Error message
                            costEditText.setBGError()
                            erpCostError.visibility = View.VISIBLE
                        } else {
                            // Hide error message
                            costEditText.setBGNormal()
                            erpCostError.visibility = View.GONE
                        }
                    } catch (e: Exception) {
                        //Exception for number format exception
                    }
                }

                override fun afterTextChanged(s: Editable?) {}
            })

            costEditText.getEditText().onFocusChangeListener =
                View.OnFocusChangeListener { v, hasFocus ->
                    if (!hasFocus) {
                        if (!costEditText.getEditText().text.isNullOrEmpty()) {
                            costEditText.getEditText().setText(
                                costEditText.getEditText().text.toString().safeDouble().round()
                            )
                        } else {
                            costEditText.getEditText().setText("0.00")
                        }
                    }
                    listener.erpViewFocused(costEditText, hasFocus)
                }
        }
    }

    inner class OriginViewHolder internal constructor(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var address1: TextView = itemView.findViewById(R.id.address_1)
        var address2: TextView = itemView.findViewById(R.id.address_2)

        init {
            itemView.setOnClickListener {
                hideKeyboard(itemView)
            }
        }

        fun populateData(addressDetails: PlaceAddressDetails) {
            address1.text = addressDetails.address1 ?: ""
            address2.text = addressDetails.address2 ?: ""
        }
    }

    inner class DestinationViewHolder internal constructor(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var address1: TextView = itemView.findViewById(R.id.address_1)
        var address2: TextView = itemView.findViewById(R.id.address_2)

        init {
            itemView.setOnClickListener {
                hideKeyboard(itemView)
            }
        }

        fun populateData(addressDetails: PlaceAddressDetails) {
            address1.text = addressDetails.address1 ?: ""
            if (addressDetails.address2.isNullOrEmpty()) {
                address2.visibility = View.GONE
            } else {
                address2.visibility = View.VISIBLE
                address2.text = addressDetails.address2
            }
        }
    }

    enum class TripListViewType(var type: Int) {
        ORIGIN(0),
        DESTINATION(1),
        INDIVIDUAL_ERP(2)
    }

    data class ERPAdapterObject(var erpDetails: ERPDetails)
    data class PlaceAddressDetails(var address1: String?, var address2: String?)
    data class TripListObject(
        val viewType: TripListViewType,
        var addressDetails: PlaceAddressDetails? = null,
        var erpItem: ERPAdapterObject? = null
    )

    interface ERPAmountChangeListener {
        fun erpAmountEdited()
        fun erpViewFocused(focusedView: View, isFocused: Boolean)
    }

    fun hideKeyboard(view: View) {

        val imm =
            view.context.getSystemService(AppCompatActivity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)

    }
}
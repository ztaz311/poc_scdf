package com.ncs.breeze.common.helper.obu.impl

import aws.smithy.kotlin.runtime.util.length
import com.breeze.model.TripType
import com.breeze.model.api.request.OBUTransaction
import com.breeze.model.api.request.TotalTravelCharge
import com.breeze.model.extensions.round
import com.breeze.model.obu.OBUCardStatusEventData
import com.breeze.model.obu.OBUParkingAvailability
import com.breeze.model.obu.OBURoadEventData
import com.breeze.model.obu.OBUTravelTimeData
import com.ncs.breeze.common.model.rx.OBUCardBalanceChangedEvent
import com.ncs.breeze.common.constant.OBUConstants
import com.ncs.breeze.common.event.ToCarRx
import com.ncs.breeze.common.extensions.obu.toChargingInfo
import com.ncs.breeze.common.helper.obu.OBUConnectionHelper
import com.ncs.breeze.common.helper.obu.OBUDataHelper
import com.ncs.breeze.common.rxbus.RxBus
import com.ncs.breeze.common.rxbus.RxEvent
import com.ncs.breeze.common.utils.SpeedLimitUtil
import com.ncs.breeze.common.utils.TripLoggingManager
import sg.gov.lta.obu.sdk.core.enums.OBUBusinessFunction
import sg.gov.lta.obu.sdk.core.enums.OBUCardStatus
import sg.gov.lta.obu.sdk.core.enums.OBUChargingMessageType
import sg.gov.lta.obu.sdk.core.enums.OBUChargingPaymentMode
import sg.gov.lta.obu.sdk.core.enums.OBUPaymentMode
import sg.gov.lta.obu.sdk.core.models.OBUAcceleration
import sg.gov.lta.obu.sdk.core.models.OBUChargingInformation
import sg.gov.lta.obu.sdk.core.models.OBUPaymentHistory
import sg.gov.lta.obu.sdk.core.models.OBUTextWithStyle
import sg.gov.lta.obu.sdk.core.models.OBUTotalTripCharged
import sg.gov.lta.obu.sdk.core.models.OBUTrafficInfo
import sg.gov.lta.obu.sdk.core.models.OBUTravelSummary
import sg.gov.lta.obu.sdk.core.types.OBUError
import sg.gov.lta.obu.sdk.data.interfaces.OBUDataListener
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.Locale

open class OBUDataListenerImpl : OBUDataListener {
    override fun onChargingInformation(chargingInfo: List<OBUChargingInformation>) {
        Timber.d("OBUDataListenerImpl onChargingInformation: size= ${chargingInfo.size}, chargingInfo = $chargingInfo")
        if (OBUConnectionHelper.isOBUSimulationEnabled) return
        if (chargingInfo.isEmpty()) return

        when (chargingInfo[0].businessFunction) {
            OBUBusinessFunction.ERP
            -> handleERPChargingInformation(chargingInfo)

            OBUBusinessFunction.EEP,
            OBUBusinessFunction.EPS
            -> handleParkingChargingInformation(chargingInfo)

            else -> {}
        }
    }

    private fun handleParkingChargingInformation(chargingInfoList: List<OBUChargingInformation>) {
        chargingInfoList.find {
            arrayOf(
                OBUChargingMessageType.DeductionSuccessful,
                OBUChargingMessageType.DeductionFailure
            ).contains(it.chargingMessageType)
        }?.let { chargingResult ->
            val isDeductionSuccessful =
                chargingResult.chargingMessageType == OBUChargingMessageType.DeductionSuccessful
            val eventData = OBURoadEventData(
                eventType = if (isDeductionSuccessful) OBURoadEventData.EVENT_TYPE_PARKING_SUCCESS else OBURoadEventData.EVENT_TYPE_PARKING_FAILURE,
                message = if (isDeductionSuccessful) "Deduction Successful" else getCardStatusMessage(
                    chargingResult.cardStatus
                ),
                distance = "",
                detailTitle = /*it.roadName*/ null,
                detailMessage = getParkingDurationText(
                    chargingResult.startTime, chargingResult.endTime
                ),
                chargeAmount = generateChargeAmountString(chargingResult.chargingAmount),
                spokenText = null,
            )
            //  set data to send analytic
            if ((chargingResult.startTime?.length ?: 0) >= 12) {
                eventData.startTimeAnalytic =
                    StringBuilder(chargingResult.startTime!!).insert(8, "_").toString()
            }
            if ((chargingResult.endTime?.length ?: 0) >= 12) {
                eventData.endTimeAnalytic =
                    StringBuilder(chargingResult.endTime!!).insert(8, "_").toString()
            }
            RxBus.publish(RxEvent.OBURoadEvent(eventData))
        }

        chargingInfoList.map { it.toChargingInfo() }.also {
            RxBus.publish(RxEvent.OBUChargeInfoEvent(it))
        }
    }

    private fun handleERPChargingInformation(chargeInfoList: List<OBUChargingInformation>) {
        val handleChargingResult: (result: OBUChargingInformation) -> Unit = {
            val isDeductionSuccessful =
                it.chargingMessageType == OBUChargingMessageType.DeductionSuccessful
            val eventData = OBURoadEventData(
                eventType = if (isDeductionSuccessful) OBURoadEventData.EVENT_TYPE_ERP_SUCCESS else OBURoadEventData.EVENT_TYPE_ERP_FAILURE,
                message = if (isDeductionSuccessful) "Deduction Successful" else getCardStatusMessage(
                    it.cardStatus
                ),
                distance = "",
                detailTitle = null,
                detailMessage = it.roadName,
                chargeAmount = generateChargeAmountString(it.chargingAmount),
                spokenText = null
            )
            RxBus.publish(RxEvent.OBURoadEvent(eventData))
        }

        val handlePreChargeInformation = {
            chargeInfoList.find {
                arrayOf(
                    OBUChargingMessageType.AlertPoint,
                    OBUChargingMessageType.Charging
                ).contains(it.chargingMessageType)
            }?.let { info ->
                val eventData = OBURoadEventData(
                    eventType = OBURoadEventData.EVENT_TYPE_ERP_CHARGING,
                    message = null,
                    distance = "",
                    detailTitle = null,
                    detailMessage = info.roadName,
                    chargeAmount = generateChargeAmountString(info.chargingAmount),
                    spokenText = "ERP Ahead"
                )
                RxBus.publish(RxEvent.OBURoadEvent(eventData))
            }
        }

        chargeInfoList.find {
            arrayOf(
                OBUChargingMessageType.DeductionSuccessful,
                OBUChargingMessageType.DeductionFailure
            ).contains(it.chargingMessageType)
        }?.let {
            handleChargingResult.invoke(it)
        } ?: run { handlePreChargeInformation.invoke() }

        chargeInfoList.map { it.toChargingInfo() }.also {
            RxBus.publish(RxEvent.OBUChargeInfoEvent(it))
        }
    }

    override fun onVelocityInformation(velocity: Double) {
        Timber.d("OBUDataListenerImpl onVelocityInformation: $velocity")
        RxBus.publish(RxEvent.OBUVelocityEvent(velocityMS = velocity))
    }

    override fun onAccelerationInformation(acceleration: OBUAcceleration) {
        Timber.d("OBUDataListenerImpl onAccelerationInformation: acceleration = $acceleration")
    }

    override fun onCardInformation(
        status: OBUCardStatus?,
        paymentMode: OBUPaymentMode?,
        chargingPaymentMode: OBUChargingPaymentMode?,
        balance: Int?
    ) {
        Timber.d("OBUDataListenerImpl onCardInformation status = $status, paymentMode = $paymentMode, chargingPaymentMode = $chargingPaymentMode, balance = $balance")
        if (OBUConnectionHelper.isOBUSimulationEnabled) return
        val cardStatus = when (status) {
            OBUCardStatus.Detected -> OBUCardStatusEventData.EVENT_TYPE_CARD_VALID
            OBUCardStatus.InsufficientStoredValue -> OBUCardStatusEventData.EVENT_TYPE_CARD_INSUFFICIENT
            OBUCardStatus.ExpiredStored -> OBUCardStatusEventData.EVENT_TYPE_CARD_EXPIRED
            OBUCardStatus.NoCardForFrontendPayment -> OBUCardStatusEventData.EVENT_TYPE_NO_CARD
            else -> OBUCardStatusEventData.EVENT_TYPE_CARD_ERROR
        }
        RxBus.publish(
            RxEvent.OBUCardInfoEvent(
                OBUCardStatusEventData(
                    status = cardStatus,
                    balance = balance?.toDouble() ?: 0.0,
                )
            )
        )

        ToCarRx.postEvent(OBUCardBalanceChangedEvent)

        balance?.takeIf {
            it < OBUConstants.OBU_LOW_CARD_THRESHOLD
                    && !OBUDataHelper.isFirstOBUCardValidArrived
                    && status == OBUCardStatus.Detected
        }?.let {
            OBUDataHelper.isFirstOBUCardValidArrived = true
            RxBus.publish(RxEvent.OBULowCardBalanceEvent(it))
        }
    }

    override fun onTravelSummary(travelSummary: OBUTravelSummary) {
        Timber.d("OBUDataListenerImpl onTravelSummary: ${travelSummary.totalTravelTime} ${travelSummary.totalTravelDistance}")
        if (OBUConnectionHelper.isOBUSimulationEnabled) return
        RxBus.publish(
            RxEvent.OBUTravelSummaryEvent(
                travelSummary.totalTravelTime.toInt(), travelSummary.totalTravelDistance.toInt()
            )
        )
    }

    override fun onPaymentHistories(histories: List<OBUPaymentHistory>) {
        Timber.d("OBUDataListenerImpl onPaymentHistories $histories")
        if (OBUConnectionHelper.isOBUSimulationEnabled) return
        val obuTransactions = histories.map {
            OBUTransaction(
                it.sequentialNumber,
                it.businessFunction.name,
                it.paymentDate,
                it.paymentMode.name,
                it.chargeAmount
            )
        }
        RxBus.publish(RxEvent.OBUPaymentHistoriesEvent(obuTransactions))
    }

    override fun onTrafficInformation(trafficInfo: OBUTrafficInfo<out Any>) {
        Timber.d("OBUDataListenerImpl onTrafficInformation ${trafficInfo.icon} -${trafficInfo.dataList} - ${trafficInfo.templateId}")
        if (OBUConnectionHelper.isOBUSimulationEnabled) return
        (trafficInfo as? OBUTrafficInfo.OBUTrafficParking)?.let { obuTrafficParking ->
            if (TripLoggingManager.getInstance()?.currentTripLogType == TripType.OBULITE) {
                val data = obuTrafficParking.dataList.map { item ->
                    OBUParkingAvailability(
                        name = item.location.text,
                        availableLots = item.lots.text,
                        color = item.lots.color.name
                    )
                }.toTypedArray()
                RxBus.publish(RxEvent.OBUParkingAvailabilityEvent(data))
            }
        }
        (trafficInfo as? OBUTrafficInfo.OBUTravelTime)?.let { obuTravelTime ->
            val data = obuTravelTime.dataList.map { item ->
                OBUTravelTimeData(
                    name = item.location.text,
                    estTime = item.min.text,
                    color = item.location.color.name
                )
            }.toTypedArray()
            RxBus.publish(RxEvent.OBUTravelTimeEvent(data))
        }
        (trafficInfo as? OBUTrafficInfo.OBUTrafficText)?.let { info ->
            when (info.icon) {
                OBURoadEventData.ICON_TYPE_SCHOOL_ZONE -> {
                    val data = OBURoadEventData(
                        eventType = OBURoadEventData.EVENT_TYPE_SCHOOL_ZONE,
                        message = "School Zone",
                        distance = "",
                        detailTitle = "",
                        detailMessage = "",
                        chargeAmount = "",
                        spokenText = "School Zone, Drive Carefully"
                    )
                    RxBus.publish(RxEvent.OBURoadEvent(data))
                }

                OBURoadEventData.ICON_TYPE_SILVER_ZONE -> {
                    val data = OBURoadEventData(
                        eventType = OBURoadEventData.EVENT_TYPE_SILVER_ZONE,
                        message = "Silver Zone",
                        distance = "",
                        detailTitle = "",
                        detailMessage = "",
                        chargeAmount = "",
                        spokenText = "Silver Zone, Drive Carefully"
                    )
                    RxBus.publish(RxEvent.OBURoadEvent(data))
                }

                OBURoadEventData.ICON_TYPE_SPEED_CAMERA -> {
                    val data = OBURoadEventData(
                        eventType = OBURoadEventData.EVENT_TYPE_SPEED_CAMERA,
                        message = "Speed Camera",
                        distance = "",
                        detailTitle = "",
                        detailMessage = "",
                        chargeAmount = "",
                        spokenText = "speed camera nearby"
                    )
                    RxBus.publish(RxEvent.OBURoadEvent(data))
                }

                OBURoadEventData.ICON_TYPE_ACCIDENT -> {
                    val data = OBURoadEventData(
                        eventType = OBURoadEventData.EVENT_TYPE_TRAFFIC,
                        message = "Accident",
                        distance = "",
                        detailTitle = "",
                        detailMessage = "",
                        chargeAmount = "",
                        spokenText = SpeedLimitUtil.currentRoadNameAlphabet
                            .takeIf { it.isNotEmpty() }
                            ?.let { "Accident on $it" }
                    )
                    RxBus.publish(RxEvent.OBURoadEvent(data))
                }

                OBURoadEventData.ICON_TYPE_BUS_LANE -> {
                    val data = OBURoadEventData(
                        eventType = OBURoadEventData.EVENT_TYPE_BUS_LANE,
                        message = "Bus Lane",
                        distance = "",
                        detailTitle = "",
                        detailMessage = "In Operation",
                        chargeAmount = "",
                        spokenText = null
                    )
                    RxBus.publish(RxEvent.OBURoadEvent(data))
                }

                else -> {
                    val dataList = trafficInfo.dataList
                    val message =
                        (dataList.find { (it as? OBUTextWithStyle)?.text != null && (it as? OBUTextWithStyle)?.text?.isNotBlank() == true } as? OBUTextWithStyle)?.text
                    val detailMessage = StringBuilder()

                    var count = 0
                    for (index in dataList.indices) {
                        val d = dataList[index]
                        val text = (d as? OBUTextWithStyle)?.text ?: ""
                        if (text.isNotBlank()) {
                            if (detailMessage.isNotBlank()) {
                                detailMessage.append(", ")
                            }
                            detailMessage.append((d as? OBUTextWithStyle)?.text ?: "")
                            if (count == 2) {
                                break
                            }
                            count++
                        }
                    }
                    val data = OBURoadEventData(
                        eventType = OBURoadEventData.EVENT_TYPE_ROAD_DIVERSION,
                        message = message,
                        distance = "",
                        detailTitle = "",
                        detailMessage = detailMessage.toString(),
                        chargeAmount = "",
                        spokenText = detailMessage.toString()
                    )
                    RxBus.publish(RxEvent.OBURoadEvent(data))
                }
            }
        }
    }

    override fun onErpChargingAndTrafficInfo(
        trafficInfo: OBUTrafficInfo<out Any>?, chargingInfo: List<OBUChargingInformation>?
    ) {
        Timber.d("OBUDataListenerImpl onErpChargingAndTrafficInfo trafficInfo = {- ${trafficInfo?.icon} -${trafficInfo?.dataList} - ${trafficInfo?.templateId} -}, chargingInfo = $chargingInfo")
    }

    override fun onError(error: OBUError) {
        Timber.d("OBUDataListenerImpl onError: ${error.code} - ${error.message} - cause=${error.cause?.message}")
    }

    override fun onTotalTripCharged(totalCharged: OBUTotalTripCharged) {
        Timber.d("OBUDataListenerImpl-onTotalTripCharged $totalCharged")
        if (OBUConnectionHelper.isOBUSimulationEnabled) return
        RxBus.publish(
            RxEvent.OBUTotalTripChargedEvent(
                TotalTravelCharge(
                    erp = totalCharged.totalERP2Charged,
                    eep = totalCharged.totalEEPCharged,
                    rep = totalCharged.totalREPCharged,
                    opc = totalCharged.totalOPCCharged,
                    cpt = totalCharged.totalCPTCharged,
                )
            )
        )
    }

    private fun getCardStatusMessage(cardStatus: OBUCardStatus): String {
        return when (cardStatus) {
            OBUCardStatus.ExpiredStored -> "Card Expired"
            OBUCardStatus.InsufficientStoredValue -> "Insufficient Balance"
            OBUCardStatus.Invalid -> "Card Error"
            else -> "System Error"
        }
    }

    private fun getParkingDurationText(start: String?, end: String?): String {
        if (start == null || end == null) return ""
        val inputSdf = SimpleDateFormat(OBUConstants.OBU_TIME_FORMAT, Locale.getDefault())
        val outputSdf = SimpleDateFormat("hh:mma", Locale.getDefault())
        return try {
            val startDate = inputSdf.parse(start)
            val endDate = inputSdf.parse(end)

            val totalTimeInMilli = endDate?.time?.minus(startDate?.time ?: 0) ?: 0
            val startText = startDate?.let { outputSdf.format(it).lowercase() } ?: ""
            val endText = endDate?.let { outputSdf.format(it).lowercase() } ?: ""
            val durationText: String = run {
                val timeInMinutes = totalTimeInMilli / 60000
                val minutes = timeInMinutes % 60
                val hours = (timeInMinutes - minutes) / 60
                "$hours hr ${"%02d".format(minutes)} min"
            }
            "${startText}-$endText\n$durationText"
        } catch (e: Exception) {
            ""
        }
    }

    private fun generateChargeAmountString(amountInCent: Int) = "$${(amountInCent / 100.0).round()}"
}
package com.ncs.breeze.car.breeze.screen.navigation

import android.annotation.SuppressLint
import androidx.annotation.DrawableRes
import androidx.car.app.CarAppService
import androidx.car.app.Screen
import androidx.car.app.model.Action
import androidx.car.app.model.CarIcon
import androidx.core.graphics.drawable.IconCompat
import com.mapbox.navigation.core.lifecycle.MapboxNavigationApp
import com.mapbox.navigation.ui.voice.api.MapboxAudioGuidance
import com.mapbox.navigation.ui.voice.internal.impl.MapboxAudioGuidanceServices
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.Event
import com.ncs.breeze.common.utils.VoiceInstructionsManager
import com.ncs.breeze.car.breeze.utils.LimitClick
import com.ncs.breeze.car.breeze.utils.setClickSafe
import com.ncs.breeze.R
import com.ncs.breeze.common.analytics.Screen.AA_NAVIGATION_SCREEN

class BreezeCarNavigationVoiceAction(
    private val screen: Screen
) {

    init {
        MapboxNavigationApp
            .getObservers(MapboxAudioGuidance::class)
            .firstOrNull()?.mute()
    }

    fun buildOnOffAction(): Action {
        return if (VoiceInstructionsManager.getInstance()?.isUserMute == true) {
            buildIconAction(R.drawable.mapbox_car_ic_volume_off) {
                onUpdateVoiceState()
            }
        } else {
            buildIconAction(R.drawable.mapbox_car_ic_volume_on) {
                onUpdateVoiceState()
            }
        }
    }

    @SuppressLint("UnsafeOptInUsageError")
    private fun buildIconAction(@DrawableRes icon: Int, onClick: () -> Unit) = Action.Builder()
        .setFlags(Action.FLAG_IS_PERSISTENT)
        .setIcon(
            CarIcon.Builder(
                IconCompat.createWithResource(
                    screen.carContext, icon
                )
            ).build()
        )
        .setClickSafe {
            LimitClick.handleSafe {
                onClick()
            }
        }
        .build()

    private fun onUpdateVoiceState() {
        VoiceInstructionsManager.getInstance()?.toggleMute()
        if (true == VoiceInstructionsManager.getInstance()?.isUserMute) {
            Analytics.logClickEvent(Event.MUTE, AA_NAVIGATION_SCREEN)
        } else {
            Analytics.logClickEvent(Event.UNMUTE, AA_NAVIGATION_SCREEN)
        }
    }



}
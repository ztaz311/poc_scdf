package com.ncs.breeze.ui.dashboard.manager

import android.location.Location
import com.breeze.model.api.response.ContentDetailsResponse
import com.ncs.breeze.ui.dashboard.LocationFocus

object ExploreMapStateManager : LocationFocus {

    const val AMENITY_INSTANCE = 0

    var contentDetailsResponse: ContentDetailsResponse? = null
        set(value) {
            value?.details?.forEach { contentDetail ->
                contentDetail.data.forEach {
                    it.amenityType = contentDetail.tooltip_type.toString()
                }
            }
            field = value
        }
    var landingBottomSheetHeight = 1000
    var landingBottomSheetState = 0
    var currentSelectedPOILocation: Location? = null
    var isOnExploreMapScreen: Boolean = false

    override var activeLocationUse: Location? = null
    override var currentLocationFocus: Location? = null
    override var forceFocusZoom: Double? = null
    override var currentFocusInstance: Int? = null


    fun setCurrentSelectedPOILocation(pLatitude: Double, pLong: Double) {
        currentSelectedPOILocation = Location("").apply {
            latitude = pLatitude
            longitude = pLong
        }
    }

    override fun setCurrentLocationFocus(
        type: Int,
        pLatitude: Double,
        pLong: Double,
        zoomRange: Double?
    ) {
        currentLocationFocus = Location("").apply {
            latitude = pLatitude
            longitude = pLong
        }
        forceFocusZoom = zoomRange
        currentFocusInstance = type
    }

    override fun resetCurrentLocationFocus(type: Int) {
        if (currentFocusInstance == type) {
            currentLocationFocus = null
            forceFocusZoom = null
            currentFocusInstance = null
        }
    }

    override fun resetCurrentLocationFocus() {
        currentLocationFocus = null
        forceFocusZoom = null
        currentFocusInstance = null
    }

    fun reset() {
        contentDetailsResponse = null
        activeLocationUse = null
        landingBottomSheetHeight = 1000
        landingBottomSheetState = 0
        currentLocationFocus = null
        forceFocusZoom = null
        currentFocusInstance = null
    }
}
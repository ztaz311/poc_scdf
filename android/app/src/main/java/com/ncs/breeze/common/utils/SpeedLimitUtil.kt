package com.ncs.breeze.common.utils

import android.location.Location
import android.util.Log
import com.mapbox.navigation.base.road.model.RoadComponent
import com.mapbox.navigation.core.MapboxNavigation
import com.mapbox.navigation.core.trip.session.LocationMatcherResult
import com.mapbox.navigation.core.trip.session.LocationObserver
import com.ncs.breeze.common.extensions.mapbox.extractNonTamilAndChineseRoadName
import com.ncs.breeze.common.rxbus.RxBus
import com.ncs.breeze.common.rxbus.RxEvent
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.math.roundToInt

object SpeedLimitUtil {

    const val TAG: String = "SpeedLimitUtil"
    const val NORMAL_SPEED: Int = 1
    const val OVER_SPEED: Int = 2
    const val OVER_SPEED_FOR_MORE_THAN_1_MIN: Int = 3
    const val OVER_SPEED_FOR_MORE_THAN_10_MIN: Int = 4

    var currentLocation: Location? = null
    var speedLimit: Int = -1
    private var overSpeedingStartTime: Long? = 0
    private var audioAlertCount: Int = 0

    private var alreadyPublished1MinEvent: Boolean = false
    private var alreadyPublished10MinEvent: Boolean = false

    private var redAABorder: Boolean = false

    private var voiceAlertHandler: SpeedLimitHandler? = null

    var currentRoadNameFull = ""
    var currentRoadNameAlphabet = ""
//    private val speedLimitObserver = CopyOnWriteArraySet<SpeedLimitObserver>()

    private val locationObserver = object : LocationObserver {
        override fun onNewLocationMatcherResult(locationMatcherResult: LocationMatcherResult) {
            locationMatcherResult.speedLimit?.speedKmph?.let {
                setCurrentSpeedLimit(it)
            }
            updateLocation(locationMatcherResult.enhancedLocation)
            currentRoadNameFull = locationMatcherResult.road.components.joinToString("") { it.text }
            currentRoadNameAlphabet =
                locationMatcherResult.road.components.extractNonTamilAndChineseRoadName() ?: ""
//            processSpeedLimit(locationMatcherResult.speedLimit)
        }

        override fun onNewRawLocation(rawLocation: Location) {
            // no op
        }
    }

//    private fun processSpeedLimit(speedLimit: SpeedLimit?) {
//        speedLimitObserver.forEach {
//            it.processSpeedLimit(speedLimit)
//        }
//    }


    fun setupLocationObserver(
        mapboxNavigation: MapboxNavigation?,
        voiceAlertHandler: SpeedLimitHandler?
    ) {
        this.voiceAlertHandler = voiceAlertHandler
        mapboxNavigation?.registerLocationObserver(locationObserver)
    }

    fun removeLocationObserver(
        mapboxNavigation: MapboxNavigation?,
        resetSpeedLimitHandler: Boolean = false
    ) {
        currentRoadNameFull = ""
        currentRoadNameAlphabet = ""
        if (resetSpeedLimitHandler) {
            this.voiceAlertHandler = null
        }
        mapboxNavigation?.unregisterLocationObserver(locationObserver)
    }
//
//    fun registerSpeedLimitObserver(speedLimit: SpeedLimitObserver) {
//        speedLimitObserver.add(speedLimit)
//    }
//
//
//    fun unregisterSpeedLimitObserver(speedLimit: SpeedLimitObserver) {
//        speedLimitObserver.remove(speedLimit)
//    }

    fun setAARedBorder(flag: Boolean) {
        redAABorder = flag
    }

    fun isAABorderRed(): Boolean? {
        return redAABorder
    }

    fun setCurrentSpeedLimit(speedLimit: Int) {
        this.speedLimit = speedLimit
    }

    fun updateLocation(currentLocation: Location?) {
        this.currentLocation = currentLocation
        if ((speedLimit == -1) || currentLocation == null)
            return
        val currentSpeed = currentLocation.speed
        val currentSpeedKmPH = (currentSpeed * 3.6).roundToInt()
        Log.d(TAG, "speed limit is: $speedLimit and current speed is: $currentSpeedKmPH")

        if (currentSpeedKmPH > speedLimit) {//over speed
            if (overSpeedingStartTime == 0L) {//just started(first time detecting over speeding from a normal speed)
                overSpeedingStartTime = Date().time
                Log.d(TAG, "OVER_SPEED")
                RxBus.publish(RxEvent.SpeedChanged(OVER_SPEED))
            } else {//continues the over speeding

                /*
                //for testing purpose only
                val diff: Long = Date().time - overSpeedingStartTime!!
                if(TimeUnit.MILLISECONDS.toSeconds(diff) > 3) {
                    Log.i("Running", "send audio alert audioAlertCount: "+audioAlertCount)
                    if(!alreadyPublished1MinEvent && audioAlertCount < 2) {
                        Log.i("Running", "AUDIO")
                        audioAlertCount++
                        alreadyPublished1MinEvent = true
                        RxBus.publish(RxEvent.SpeedChanged(OVER_SPEED_FOR_MORE_THAN_1_MIN))
                    }
                }*/

                val durationInMinutes = getOverSpeedDurationInMinutes()
                if (durationInMinutes >= 10 && audioAlertCount < 2) {//over speeding continuously for more than 10 minutes
                    if (!alreadyPublished10MinEvent) {
                        audioAlertCount++
                        alreadyPublished10MinEvent = true
                        Log.d(TAG, "OVER_SPEED_FOR_MORE_THAN_10_MIN")
                        //RxBus.publish(RxEvent.SpeedChanged(OVER_SPEED_FOR_MORE_THAN_10_MIN))
                        this.voiceAlertHandler?.alertOnThreshold()
                    }
                } else if (durationInMinutes >= 1 && audioAlertCount < 2) {//over speeding continuously for more than 1 minute
                    if (!alreadyPublished1MinEvent) {
                        audioAlertCount++
                        alreadyPublished1MinEvent = true
                        Log.d(TAG, "OVER_SPEED_FOR_MORE_THAN_1_MIN")
                        //RxBus.publish(RxEvent.SpeedChanged(OVER_SPEED_FOR_MORE_THAN_1_MIN))
                        this.voiceAlertHandler?.alertOnThreshold()
                    }
                }
            }
        } else {//normal speed
            if (overSpeedingStartTime != 0L) {
                resetParams()
                RxBus.publish(RxEvent.SpeedChanged(NORMAL_SPEED))
            }
        }
    }

    private fun getOverSpeedDurationInMinutes(): Long {
        val diffInMinutes: Long
        val diffInMs: Long = Date().time - overSpeedingStartTime!!
        diffInMinutes = TimeUnit.MILLISECONDS.toMinutes(diffInMs)
        return diffInMinutes
    }

    private fun resetParams() {
        Log.d(TAG, "resetParams()")
        overSpeedingStartTime = 0
        alreadyPublished1MinEvent = false
        alreadyPublished10MinEvent = false
    }

    fun gc() {
        Log.d(TAG, "gc()")
        speedLimit = -1
        audioAlertCount = 0
        resetParams()
    }
}
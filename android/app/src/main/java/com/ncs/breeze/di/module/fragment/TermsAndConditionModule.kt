package com.ncs.breeze.di.module.fragment

import com.ncs.breeze.ui.dashboard.fragments.view.TermsAndConditionFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class TermsAndConditionModule {
    @ContributesAndroidInjector
    abstract fun contributeTermsAndConditionFragment(): TermsAndConditionFragment
}
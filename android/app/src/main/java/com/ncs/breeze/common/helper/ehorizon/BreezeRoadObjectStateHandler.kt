package com.ncs.breeze.common.helper.ehorizon

import com.mapbox.maps.Style

internal interface BreezeRoadObjectStateHandler {

    fun onTerminateNotification(style: Style?, roadObjectId: String)
}
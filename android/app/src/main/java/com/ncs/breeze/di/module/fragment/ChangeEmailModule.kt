package com.ncs.breeze.di.module.fragment

import com.ncs.breeze.ui.dashboard.fragments.view.ChangeEmailFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class ChangeEmailModule {
    @ContributesAndroidInjector
    abstract fun contributeChangeEmailFragment(): ChangeEmailFragment
}
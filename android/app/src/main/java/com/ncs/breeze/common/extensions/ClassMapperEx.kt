package com.ncs.breeze.common.extensions

import com.ncs.breeze.common.storage.TrafficIncidentPreference
import com.ncs.breeze.common.storage.UserPreference
import com.ncs.breeze.common.storage.UserStoredData
import com.breeze.model.common.UserPreference as UserPreferenceV2
import com.breeze.model.common.TrafficIncidentPreference as TrafficIncidentPreferenceV2
import com.breeze.model.common.UserStoredData as UserStoredDataV2


fun UserPreference.toV2Object(): UserPreferenceV2 {
    val map = HashMap(trafficIncidentMap.mapValues { it.value.toV2Object() })
    return UserPreferenceV2(
        userName,
        userEmail,
        amplifyProvider,
        preferredTheme,
        currentTheme,
        map,
        playNavigationVoice,
        routePreference,
        emailUpdatePopupCount,
        isUserEmailVerified,
        userPhoneNumber,
        instabugGroup,
        onBoardingState,
        userCreatedDate,
        userId,
        isGuestMode,
        emailGuestUser,
        customEmail,
        isCustomEmailVerified,
    )
}

fun TrafficIncidentPreference.toV2Object(): TrafficIncidentPreferenceV2 {
    return TrafficIncidentPreferenceV2(
        incidentName, showNotification, showIcon
    )
}

fun UserStoredData.toV2Object():UserStoredDataV2{
    return UserStoredDataV2(
        cognitoID, carplateNumber, iuNumber, userName, email
    )
}
package com.ncs.breeze.di

import com.ncs.breeze.di.module.NetworkModule
import com.ncs.breeze.common.utils.UtilModule
import com.ncs.breeze.App
import com.ncs.breeze.di.module.ActivityBuilderModule
import com.ncs.breeze.di.module.AppContextModule
import com.ncs.breeze.di.module.ViewModelModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        NetworkModule::class,
        ActivityBuilderModule::class,
        ViewModelModule::class,
        AppContextModule::class,
        UtilModule::class,
        AndroidListenableWorkerInjectionModule::class
    ]
)
interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(app: App): Builder

        fun build(): AppComponent
    }

    fun inject(app: App)
}
package com.ncs.breeze.ui.dashboard.activity

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.app.NotificationManager
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.pm.ResolveInfo
import android.content.res.Configuration
import android.graphics.Color
import android.graphics.Rect
import android.graphics.drawable.ColorDrawable
import android.location.Location
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.os.PersistableBundle
import android.text.TextUtils
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewTreeObserver
import android.view.ViewTreeObserver.OnGlobalLayoutListener
import android.view.Window
import android.view.WindowManager
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.animation.LinearInterpolator
import android.widget.TextView
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.annotation.DrawableRes
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.animation.doOnEnd
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.*
import androidx.lifecycle.Observer
import com.breeze.customization.view.AmenitiesOnMapStateView
import com.breeze.customization.view.BreezeButton
import com.breeze.customization.view.CarParkStateView
import com.breeze.customization.view.DispatchTouchLayout
import com.breeze.model.*
import com.breeze.model.api.ErrorDataResponse
import com.breeze.model.api.request.ETARequest
import com.breeze.model.api.request.ShortcutDetails
import com.breeze.model.api.response.*
import com.breeze.model.api.response.amenities.BaseAmenity
import com.breeze.model.api.response.notification.GlobalNotificationData
import com.breeze.model.api.response.notification.NotificationCategoryEnum
import com.breeze.model.api.response.notification.NotificationItem
import com.breeze.model.api.response.notification.SeenNotification
import com.breeze.model.api.response.tbr.ProfileAmenities
import com.breeze.model.api.response.traffic.TrafficResponse
import com.breeze.model.constants.AmenityType.EVCHARGER
import com.breeze.model.constants.AmenityType.PETROL
import com.breeze.model.constants.Constants
import com.breeze.model.enums.AndroidScreen
import com.breeze.model.enums.CarParkViewOption
import com.breeze.model.enums.ETAMode
import com.breeze.model.enums.UserTheme
import com.breeze.model.extensions.dpToPx
import com.breeze.model.extensions.round
import com.breeze.model.obu.OBUInitialConnectionStep
import com.breeze.model.obu.OBURoadEventData
import com.facebook.react.ReactFragment
import com.facebook.react.bridge.Arguments
import com.facebook.react.bridge.ReadableMap
import com.facebook.react.bridge.WritableMap
import com.facebook.react.modules.core.DefaultHardwareBackBtnHandler
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import com.instabug.bug.BugReporting
import com.instabug.library.invocation.InstabugInvocationEvent
import com.mapbox.android.core.location.LocationEngineResult
import com.mapbox.android.gestures.MoveGestureDetector
import com.mapbox.android.gestures.RotateGestureDetector
import com.mapbox.api.directions.v5.models.DirectionsRoute
import com.mapbox.geojson.Point
import com.mapbox.maps.CameraOptions
import com.mapbox.maps.EdgeInsets
import com.mapbox.maps.MapView
import com.mapbox.maps.plugin.LocationPuck2D
import com.mapbox.maps.plugin.animation.camera
import com.mapbox.maps.plugin.gestures.GesturesPlugin
import com.mapbox.maps.plugin.gestures.OnMoveListener
import com.mapbox.maps.plugin.gestures.OnRotateListener
import com.mapbox.maps.plugin.gestures.gestures
import com.mapbox.maps.plugin.locationcomponent.LocationComponentPlugin
import com.mapbox.maps.plugin.locationcomponent.LocationProvider
import com.mapbox.maps.plugin.locationcomponent.OnIndicatorPositionChangedListener
import com.mapbox.maps.plugin.locationcomponent.location
import com.mapbox.navigation.base.ExperimentalPreviewMapboxNavigationAPI
import com.mapbox.navigation.base.route.NavigationRoute
import com.mapbox.navigation.core.lifecycle.MapboxNavigationApp
import com.mapbox.navigation.core.trip.session.LocationMatcherResult
import com.mapbox.navigation.core.trip.session.TripSessionState
import com.mapbox.navigation.ui.maps.camera.NavigationCamera
import com.mapbox.navigation.ui.maps.camera.data.MapboxNavigationViewportDataSource
import com.mapbox.navigation.ui.maps.camera.lifecycle.NavigationScaleGestureHandler
import com.ncs.breeze.App
import com.ncs.breeze.BuildConfig
import com.ncs.breeze.R
import com.ncs.breeze.car.breeze.ReplayRouteTripSession
import com.ncs.breeze.car.breeze.screen.navigation.CarNavigationScreenState
import com.ncs.breeze.car.breeze.utils.BreezeCarUtil
import com.ncs.breeze.common.ETAStatus
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.Event
import com.ncs.breeze.common.analytics.Screen
import com.ncs.breeze.common.appSync.AppSyncHelper
import com.ncs.breeze.common.constant.OBUConnectionState
import com.ncs.breeze.common.constant.RNScreen
import com.ncs.breeze.common.constant.dashboard.LandingMode
import com.ncs.breeze.common.event.ToCarRx
import com.ncs.breeze.common.event.ToCarRx.postEventWithCacheLast
import com.ncs.breeze.common.extensions.android.*
import com.ncs.breeze.common.extensions.mapbox.compressToGzip
import com.ncs.breeze.common.extensions.mapbox.setBreezeDefaultOptions
import com.ncs.breeze.common.model.DepartureType
import com.ncs.breeze.common.model.RouteAdapterObjects
import com.ncs.breeze.common.model.RouteDepartureDetails
import com.ncs.breeze.common.model.SimpleRouteAdapterObjects
import com.ncs.breeze.common.model.rx.AppEventLogout
import com.ncs.breeze.common.model.rx.AppToCarParkEndEvent
import com.ncs.breeze.common.model.rx.AppToCarParkEvent
import com.ncs.breeze.common.model.rx.AppToHomeTurnOnCruiseModeEvent
import com.ncs.breeze.common.model.rx.AppToPhoneCarParkEndEvent
import com.ncs.breeze.common.model.rx.AppToPhoneCarParkEvent
import com.ncs.breeze.common.model.rx.AppToPhoneNavigationEndEvent
import com.ncs.breeze.common.model.rx.AppToPhoneNavigationStartEvent
import com.ncs.breeze.common.model.rx.AppToPhoneRoutePlanningStartEvent
import com.ncs.breeze.common.model.rx.ETAStartedPhone
import com.ncs.breeze.common.model.rx.SwitchThemeEvent
import com.ncs.breeze.common.model.rx.ToAppRxEvent
import com.ncs.breeze.common.model.rx.TriggerPhoneNotificaion
import com.ncs.breeze.common.model.rx.UpdateETAPhone
import com.ncs.breeze.common.rxbus.RxBus
import com.ncs.breeze.common.rxbus.RxEvent
import com.ncs.breeze.common.storage.AppPrefsKey
import com.ncs.breeze.common.storage.BreezeAppPreference
import com.ncs.breeze.common.storage.BreezeUserPreference
import com.ncs.breeze.common.utils.*
import com.ncs.breeze.common.utils.compression.GZIPCompressionUtil
import com.ncs.breeze.common.utils.eta.ETAEngine
import com.ncs.breeze.common.utils.notification.PushNotificationUtils
import com.ncs.breeze.components.*
import com.ncs.breeze.components.layermanager.impl.DropPinLayerManager
import com.ncs.breeze.components.map.profile.ListenerCallbackLandingMap
import com.ncs.breeze.components.map.profile.MapCongestionManager
import com.ncs.breeze.components.map.profile.MapProfileType
import com.ncs.breeze.databinding.ActivityDashboardBinding
import com.ncs.breeze.notification.FCMListenerService
import com.ncs.breeze.reactnative.nativemodule.ReactNativeEventEmitter
import com.ncs.breeze.reactnative.nativemodule.ShareBusinessLogicEvent
import com.ncs.breeze.ui.base.BaseMapActivity
import com.ncs.breeze.ui.dashboard.AdjustableBottomSheet
import com.ncs.breeze.ui.dashboard.fragments.erpdetail.ErpDetailFragment
import com.ncs.breeze.ui.dashboard.fragments.exploremap.ExploreMapsFragment
import com.ncs.breeze.ui.dashboard.fragments.obuconnect.OBUConnectFragment
import com.ncs.breeze.ui.dashboard.fragments.obuconnect.OBUPairedListFragment
import com.ncs.breeze.ui.dashboard.fragments.obulite.OBULiteFragment
import com.ncs.breeze.ui.dashboard.fragments.placesave.PlaceSaveDetailFragment
import com.ncs.breeze.ui.dashboard.fragments.sharedcollection.SharedCollectionFragment
import com.ncs.breeze.ui.dashboard.fragments.sharelocation.SharedLocationFragment
import com.ncs.breeze.ui.dashboard.fragments.traffic.TrafficDetailFragment
import com.ncs.breeze.ui.dashboard.fragments.view.*
import com.ncs.breeze.ui.dashboard.manager.DashboardMapStateManager
import com.ncs.breeze.ui.login.fragment.forgotPassword.view.ForgetPasswordEmailFragment
import com.ncs.breeze.ui.navigation.TurnByTurnNavigationActivity
import com.ncs.breeze.ui.rn.CustomReactFragment
import com.ncs.breeze.ui.walkathon.WalkingActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.map
import timber.log.Timber
import java.io.Serializable
import java.lang.ref.WeakReference
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject


class DashboardActivity : BaseMapActivity<ActivityDashboardBinding, DashboardViewModel>(),
    DefaultHardwareBackBtnHandler, AdjustableBottomSheet {

    companion object {
        private const val CAPTURED_PHOTO_PATH_KEY = "mCurrentPhotoPath"
        private const val CAPTURED_PHOTO_URI_KEY = "mCapturedImageURI"
        const val IS_DEEPLINK_EMAIL_VERIFICATION = "IS_DEEPLINK_EMAIL_VERIFICATION"
        const val DEEPLINK_EMAIL_OTP = "DEEPLINK_EMAIL_OTP"
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val viewModel: DashboardViewModel by viewModels { viewModelFactory }

    @Inject
    lateinit var breezeFeatureDetectionUtil: BreezeFeatureDetectionUtil

    @Inject
    lateinit var breezeERPUtil: BreezeERPRefreshUtil

    @Inject
    lateinit var breezeIncidentsUtil: BreezeTrafficIncidentsUtil

    @Inject
    lateinit var breezeMiscRoadObjectUtil: BreezeMiscRoadObjectUtil

    @Inject
    lateinit var breezeSchoolSilverZoneUpdateUtil: BreezeSchoolSilverZoneUpdateUtil

    @Inject
    lateinit var breezeTripLoggingUtil: BreezeTripLoggingUtil

    @Inject
    lateinit var breezeStatisticsUtil: BreezeStatisticsUtil

    @Inject
    lateinit var breezeHistoryRecorderUtil: BreezeHistoryRecorderUtil

    @Inject
    lateinit var breezeGlobalNotificationsUtil: BreezeGlobalNotificationsUtil

    @Inject
    lateinit var breezeUserPreferenceUtil: BreezeUserPreferenceUtil

    private var mRefreshRouteDestinationListener: RefreshRouteDestinationListener? = null
    private var mRefreshRouteNotificationListener: RefreshRouteNotificationListener? = null
    private var mRefreshRoutePlanningListener: RefreshRoutePlanningListener? = null
    private var mSettingsChangeListener: SettingsChangeListener? = null
    private var mProfileUserNameChangeListener: ProfileUserNameChangeListener? = null
    private var mTravelLogListener: TravelLogListener? = null
    private var erpListener: Disposable? = null
    private var erpOpenListener: Disposable? = null

    private var pendingGlobalNotifications: MutableList<GlobalNotificationData> = ArrayList()
    private var listNotificationGlobal: ArrayList<GlobalNotification?> =
        ArrayList<GlobalNotification?>()

    var isSharing = false

    //for erp route preview
    private var trackedERPDetailsId: String? = null
    private var trackedERPRouteId: Int? = null

    var isNudgeViewShowing = false

    lateinit var dialog: Dialog
    var isShowDialogUpdateEmail = false

    val MAX_EMAIL_ASK_COUNT = 1


    lateinit var dashBoardMapCamera: DashBoardMapCamera
    private var dashBoardMapLanding: DashBoardMapLanding? = null
    private var mapCongestionManager: MapCongestionManager? = null

    // --------------------- Startup Location related variables-----------------------

    // loads as soon as current location is discovered on start up
    var isCarLocationInit = true

    // load only if current location was null during startup
    var isAmenityLocationInit = true

    private var isAmenityStyleImagesLoaded = false
    private var isRNBottomSheetReady = false

    // --------------------- Free Drive Mode related variables -----------------------

    private lateinit var locationComponent: LocationComponentPlugin
    private var turnByTurnNavigationStarted = false

    private lateinit var navigationCamera: NavigationCamera
    private lateinit var viewportDataSource: MapboxNavigationViewportDataSource

    private var snapshotImageBase64: String? = null

    var isCameraTrackingDismissed: Boolean = false
    private var defaultLocationProvider: LocationProvider? = null

    private var mIsUserOnHome = true

    var isShowingCarparkList = false

    /**
     * for pantracking
     */
    var panTrackingManager: PanTrackingManager? = null

    private var currentUIMode = -1


    private var hideCustomToastHandler: Handler? = null

    //amenity and carpark observers

    //making this a class instance so that it doesn't need to be manually removed everytime (duplicate of the same instance are ignored in livedata)
    //this also prevents issue where race conditions could remove a listener when data is being returned
    private val amenityObserver = Observer<HashMap<String, List<BaseAmenity>>> {
        it?.let {
            dashBoardMapLanding?.handleAmenitiesData(it)
            runOnUiThread {
                selectAmenity(
                    arrayOf(EVCHARGER, PETROL),
                    arrayOf(
                        viewBinding.amenitiesOnMap.isEVChargerShown,
                        viewBinding.amenitiesOnMap.isPetrolShown
                    )
                )
            }
        }
    }

    //making this a class instance so that it doesn't need to be manually removed everytime (duplicate of the same instance are ignored in livedata)
    //this also prevents issue where race conditions could remove a listener when data is being returned
    private val carParkObserver = Observer<ArrayList<BaseAmenity>> { carParks ->
        carParks?.takeIf { it.isNotEmpty() }?.let {
            val finalCarParksToShow = ArrayList<BaseAmenity>()
            val cpDistanceSetting = BreezeUserPreference.getInstance(applicationContext)
                .getCarParkDistanceSettingValue(BreezeCarUtil.masterlists) * 1000
            var nearestCarParkTooFar = false
            carParks.first().distance?.let { distance ->
                if (distance > cpDistanceSetting) {
                    finalCarParksToShow.add(carParks.first())
                    nearestCarParkTooFar = true
                }
            }
            if (nearestCarParkTooFar) {
                Handler(mainLooper).postDelayed({
                    showCarParkVeryFarGN()
                }, 1000)
            } else {
                finalCarParksToShow.clear()
                finalCarParksToShow.addAll(carParks)
            }
            dashBoardMapLanding?.handleCarpark(
                finalCarParksToShow,
                isMoveCameraWrapperAllCarpark = panTrackingManager?.isInPanMode == false,
                DashboardMapStateManager.isInSearchLocationMode
            )

        }
    }


    private val walkingRouteObserver = WalkingRouteObserver()

    private val onMoveListener = object : OnMoveListener {
        override fun onMoveBegin(detector: MoveGestureDetector) {
            onCameraTrackingDismissed()
        }

        override fun onMove(detector: MoveGestureDetector): Boolean {
            return false
        }

        override fun onMoveEnd(detector: MoveGestureDetector) {}
    }

    private val onIndicatorPositionChangedListener =
        OnIndicatorPositionChangedListener { point ->
            if (!lifecycle.currentState.isAtLeast(Lifecycle.State.CREATED)) return@OnIndicatorPositionChangedListener
            if (!isTripRunning() && !DashboardMapStateManager.isInMapProfileSelectedMode && !DashboardMapStateManager.isInSearchLocationMode && !isCameraTrackingDismissed) {
                viewBinding.mapView.getMapboxMap()
                    .setCamera(CameraOptions.Builder().center(point).build())
                viewBinding.mapView.gestures.focalPoint =
                    viewBinding.mapView.getMapboxMap().pixelForCoordinate(point)
            }
        }

    // --------------------- Free Drive Mode related variables -----------------------

    private var navigationActivityResultLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            Timber.d("navigationActivityResultLauncher called in DashboardActivity")
            if (result.resultCode != Activity.RESULT_OK) return@registerForActivityResult
            result.data?.run {
                if (getBooleanExtra(Constants.NAVIGATION_END, false)) {
                    popFragmentsInBackStackExceptExploreMap()
                    if (getBooleanExtra(Constants.NAVIGATION_END_REFRESH_MAP, false)) {
                        (supportFragmentManager.findFragmentByTag(Constants.TAGS.EXPLORE_MAP) as? ExploreMapsFragment)
                            ?.refreshContent()
                    }
                    if (getBooleanExtra(
                            Constants.NAVIGATION_END_REFRESH_LANDING_CAR_PARKS,
                            false
                        )
                    ) {
                        getCarParkDataShowInMap()
                    }
                }

                val isSaveEasyBreezy = getBooleanExtra(Constants.SAVE_EASY_BREEZY, false)
                val destinationDetails = getSerializableExtra(Constants.DESTINATION_ADDRESS_DETAILS)
                if (isSaveEasyBreezy && destinationDetails != null) {
                    newAddressFragment(destinationDetails)
                }
            }
        }

    fun isTripRunning(): Boolean {
        return MapboxNavigationApp.current()?.getTripSessionState() == TripSessionState.STARTED
    }

    var lastUserInteraction: Long = 0
    val screenNavigationHelper by lazy {
        DashboardScreenNavigationHelper(WeakReference(this))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        currentUIMode = resources.configuration.uiMode
        Timber.d("FCM - DashboardActivity::onCreate")

        //toggleForceUpdate()

        BreezeUserPreference.getInstance(this).setAppLaunchCount()

        BugReporting.setInvocationEvents(InstabugInvocationEvent.SCREENSHOT)

        Timber.d("Logging to timber from dashboard")

        viewModel.logAppVersion()
        //dashboardViewModel.retrieveUserSettings()
        viewModel.registerDeviceDetails()

        AWSUtils.getCurrentUser()

        ETAEngine.init(viewModel.apiHelper, applicationContext)

        Timber.d("Email update handling")

        handleUpdateEmailFromDeepLink()

        /**
         * call api get all setting again
         * //todo bangnv for quick fix
         */
        getUserSetting()

        initViews()

        initAppSyncAndMapDataHandlers()

        registerRXbus()

        handleBackStackChangeListener()

        exploreMap()

        checkShowingOBUIntro()

        //showBottomPanel()
        //Enable only for cruisemode testing purpose.
        //startCruiseMode()
        hideCustomToastHandler = Handler(mainLooper)
        observeOBUConnectionStateChanged()

        OBUStripStateManager.getInstance()?.let { obuStripStateManager ->
            obuStripStateManager.obuStripCurrentState.observe(this@DashboardActivity) {
                if (it == OBUStripState.RUNNING && !obuStripStateManager.isPhoneOpened) {
                    openOBULiteDisplay()
                }
            }

        }
    }

    private fun checkShowingOBUIntro() {
        if (getUserDataPreference()?.shouldShowOBUIntro() == true) {
            getUserDataPreference()?.saveShowOBUIntroState(false)
            getShareBusinessLogicPackage()?.getRequestsModule()
                ?.flatMap {
                    it.callRN(ShareBusinessLogicEvent.OPEN_MODAL_OBU_INSTALL.eventName, null)
                }
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe()
        }
    }

    private var exploreContent = ArrayList<ExploreMaintenanceResponse>()

    private fun exploreMap() {
        viewModel.exploreMapNew.observe(this) { exploreContentData ->
            viewModel.exploreMapNew.removeObservers(this)
            exploreContentData?.item
                ?.takeIf { it.isNotEmpty() }
                ?.let {
                    this@DashboardActivity.exploreContent.clear()
                    this@DashboardActivity.exploreContent.addAll(it)
//                    refreshExploreMapButton()
                }
        }
        viewModel.getExploreMap()
    }

//    fun refreshExploreMapButton() {
//        val seenContentList = BreezeUserPreference.getInstance(this).getSeenContentIds()
//        val hasNewContent =
//            exploreContent.any { item -> !seenContentList.contains(item.contentId.toString()) }
//        if (!isNewUser()) {
//            viewBinding.btnExploreMapView.updateNotificationDot(hasNewContent)
//        }
//    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        if (newConfig.uiMode != currentUIMode) {
            currentUIMode = newConfig.uiMode
            startActivity(Intent(this, DashboardActivity::class.java))
            finish()
        }
    }

    private fun handleBackStackChangeListener() {
        this.supportFragmentManager.addOnBackStackChangedListener {
            if (this.supportFragmentManager.backStackEntryCount == 0) {
                setDashboardActivityStatusBarBasedOnTheme()
                (supportFragmentManager.findFragmentByTag(RNScreen.LANDING) as? CustomReactFragment)?.run {
                    reactDelegate?.onHostResume(this@DashboardActivity)
                }
                breezeGlobalNotificationsUtil.getNotificationShowFirstTime()
                exploreMap()
            } else {
                if (
                    this.supportFragmentManager.fragments.last() is RoutePlanningFragment ||
                    this.supportFragmentManager.fragments.last() is NewAddressFragment
                ) {
                    setDashboardActivityStatusBarBasedOnTheme()
                } else if (this.supportFragmentManager.fragments.filter { it is ForgetPasswordEmailFragment }
                        .isNotEmpty()) {
                    setDashboardForgotPasswordStatusBar()
                } else {
                    setDashboardActivityStatusBar()
                }
            }
        }
    }

    fun getLocationFocusedForReact(): Location? {
        //pan mode
        if (panTrackingManager != null && panTrackingManager?.isInPanMode == true && panTrackingManager?.currentLocationPan != null) {
            return panTrackingManager?.currentLocationPan
        }

        //search mode

        if (DashboardMapStateManager.isInSearchLocationMode) {
            DashboardMapStateManager.searchLocationAddress?.getRoutableLocation()?.let {
                return it
            }
        }
        return LocationBreezeManager.getInstance().currentLocation
    }


    private fun registerRXbus() {
        compositeDisposable.add(
            RxBus.listen(RxEvent.LogoutRequested::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    Toast.makeText(
                        this,
                        getString(R.string.error_occured),
                        Toast.LENGTH_SHORT
                    ).show()
                    Timber.d("LogoutRequested")
                    logoutApp {
                        postEventWithCacheLast(AppEventLogout())
                        restartApp()
                    }
                })


        compositeDisposable.add(
            RxBus.listen(RxEvent.LogoutGoCreateScreen::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    Toast.makeText(
                        this,
                        getString(R.string.error_occured),
                        Toast.LENGTH_SHORT
                    ).show()
                    Timber.d("LogoutGoCreateScreen")
                    logoutApp {
                        postEventWithCacheLast(AppEventLogout())
                        openLoginScreen()
                    }
                })


        compositeDisposable.add(
            RxBus.listen(RxEvent.GlobalNotificationDataRefresh::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    informNewNotificationArriveToRN()
                    if (!isTripRunning() && !BreezeUserPreference.getInstance(this)
                            .isInAppGuideTutorialPending() && !isShowDialogUpdateEmail
                    ) {
                        handleGlobalNotificationEvent(it.notificationData)
                    } else {
                        if (!hasPendingNotificationAlready(it.notificationData)) {
                            pendingGlobalNotifications.add(it.notificationData)
                        }
                    }
                })

        /**
         * send event voucher added to react
         */
        compositeDisposable.add(
            RxBus.listen(RxEvent.VoucherAddedSyncEvent::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    runOnUiThread {
                        callEventToAddNewVoucher()
                    }
                })


        compositeDisposable.add(
            RxBus.listen(RxEvent.GlobalNotificationSkipped::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    if (!isNudgeViewShowing) {
                        checkForPNudgeDisplay()
                    }
                })

        compositeDisposable.add(
            RxBus.listen(RxEvent.EventOpenCalculateFeeLandingPage::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    sendEventOpenCalculateFreeToReact(it.idCarPark)
                })

        compositeDisposable.add(
            RxBus.listen(RxEvent.AddDashboardFragment::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    when (it.toScreen) {
                        AndroidScreen.ERP_DETAILS_PLAN_TRIP -> {
                            CoroutineScope(Dispatchers.IO).launch {
                                addERPPlannedRoute()
                            }
                        }

                        AndroidScreen.EDIT_SHORTCUTS -> {
                            addEditShortcutFragment(it)
                        }

                        AndroidScreen.SETTINGS -> {
                            addMoreSettingsFragment()
                        }

                        else -> {}
                    }
                })
        compositeDisposable.add(
            RxBus.listen(RxEvent.CloseLandingDropPinScreen::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { event ->
                    showToast("Drop pin mode off", R.drawable.ic_pin_mode_off)
                    viewBinding.btnDropPinToggle.setDropPinMode(false)
                })
        showBottomPanel()
//        showInAppGuide()
    }


    private fun sendEventOpenCalculateFreeToReact(idCarPark: String) {
        val locationFocusForReact = getLocationFocusedForReact()
        getShareBusinessLogicPackage()?.getRequestsModule()?.flatMap {
            val rnData = Arguments.createMap().apply {
                putString("parkingID", idCarPark)
                locationFocusForReact?.let { loc ->
                    putString(Constants.RN_CONSTANTS.FOCUSED_LAT, loc.latitude.toString())
                    putString(Constants.RN_CONSTANTS.FOCUSED_LONG, loc.longitude.toString())
                }
            }

            it.callRN(ShareBusinessLogicEvent.OPEN_CALCULATE_FEE_CARPARK.eventName, rnData)
        }?.subscribeOn(Schedulers.io())?.observeOn(AndroidSchedulers.mainThread())?.subscribe()
    }

    private fun hasPendingNotificationAlready(notification: GlobalNotificationData) =
        pendingGlobalNotifications.find { item -> item.notificationItem.notificationId == notification.notificationItem.notificationId } != null

//    private fun showInAppGuide() {
//        manageInAppGuideHandler.postDelayed(Runnable {
//            manageInAppGuideHandler.postDelayed(runnableCode!!, delay.toLong())
//            if (getDifferenceInSeconds() >= 3) {
//                if (BreezeUserPreference.getInstance(this)!!.isInAppGuideTutorialPending()) {
//                    Timber.i("-show guide-")
//                    (applicationContext as? App)?.let { app ->
//                        val shareBusinessLogicPackage = app.shareBusinessLogicPackage
//                        shareBusinessLogicPackage.getRequestsModule().flatMap {

    //                            it.callRN(_root_ide_package_.com.ncs.breeze.reactnative.nativemodule.ShareBusinessLogicEvent.OPEN_ONBOARDING_TUTORIAL.eventName, null)
//                        }
//                            .subscribeOn(Schedulers.io())
//                            .observeOn(AndroidSchedulers.mainThread())
//                            .subscribe()
//                    }
//                    BreezeUserPreference.getInstance(this)!!.setInAppGuideTutorialPending(false)
//                } else {
//                    Timber.i("-stop handler-")
//                    if (manageInAppGuideHandler != null) {
//                        Timber.i("remove callbacks")
//                        manageInAppGuideHandler.removeCallbacksAndMessages(null)
//                    }
//                }
//            }
//        }.also { runnableCode = it }, delay.toLong())
//    }

    fun callEventToCheckVoucherCarAnimation() {
        if (!BreezeUserPreference.getInstance(this).isCarAnimationShown()) {
            if ("dev" == BuildConfig.FLAVOR) {
                Toast.makeText(
                    this@DashboardActivity,
                    "VOUCHER CAR ANIMATION TRIGGERED",
                    Toast.LENGTH_SHORT
                ).show()
            }
            // notify RN that they can run animation
            getApp()?.shareBusinessLogicPackage?.getRequestsModule()?.flatMap {

                it.callRN(ShareBusinessLogicEvent.CAN_SHOW_VOUCHER_ANIMATION.eventName, null)
            }?.subscribeOn(Schedulers.io())?.observeOn(AndroidSchedulers.mainThread())?.subscribe()

            BreezeUserPreference.getInstance(this).setCarAnimationShown()
        }
    }


    private fun callEventToAddNewVoucher() {
        getApp()?.shareBusinessLogicPackage?.getRequestsModule()?.flatMap {
            it.callRN(ShareBusinessLogicEvent.ON_NEW_VOUCHER_ADDED.eventName, null)
        }?.subscribeOn(Schedulers.io())?.observeOn(AndroidSchedulers.mainThread())?.subscribe()
    }

    fun updateInAppGuideUserPreference() {
        viewModel.saveUserSettings()
        lifecycleScope.launch(Dispatchers.Main) {
            executePendingGlobalNotifications { proceedForNextAction ->
                if (proceedForNextAction) {
                    checkForPNudgeDisplay()
                }
            }
            getCarParkDataShowInMap()
        }
    }

    /**
     * inform new notification arrive
     */
    private fun informNewNotificationArriveToRN() {
        getShareBusinessLogicPackage()?.getRequestsModule()?.flatMap {
            it.callRN(ShareBusinessLogicEvent.RECEIVE_NEW_NOTIFICATIONS.eventName, null)
        }?.subscribeOn(Schedulers.io())?.observeOn(AndroidSchedulers.mainThread())?.subscribe()
    }

    private fun checkUpcomingTrip() {
        viewModel.checkTripPlannerResponse.observe(this, Observer {
            viewModel.checkTripPlannerResponse.removeObservers(this)
            sendUpcomingTripEvent(it)
        })
        viewModel.checkTripUpcomingSummary()
    }

    private fun sendUpcomingTripEvent(response: TripsPlannerResponse) {
        val isExistTriplogToday = Utils.isExistTripLogInDay(response.trips)
        getShareBusinessLogicPackage()?.getRequestsModule()?.flatMap {
            it.callRN(
                ShareBusinessLogicEvent.FN_UPDATE_UPCOMING_TRIPS.eventName, Arguments.fromBundle(
                    bundleOf("hasUpcomingTrips" to isExistTriplogToday)
                )
            )
        }?.subscribeOn(Schedulers.io())?.subscribe()
    }

    private fun addEditShortcutFragment(event: RxEvent.AddDashboardFragment) {
        Timber.d("Printing the react native data: ${event.readableMap}")
        val args = Bundle()
        (event.readableMap["location"] as? HashMap<*, *>)?.let {
            args.putSerializable(
                Constants.DESTINATION_ADDRESS_DETAILS,
                ShortcutDetails.parseFromRNBookmarkData(it)
            )
        }

        addFragment(
            NewAddressFragment(),
            args, Constants.TAGS.ADD_NEW_BREEZE
        )
    }


    private suspend fun addERPPlannedRoute() {
        val id = trackedERPDetailsId
        val selectedRoute = trackedERPRouteId
        if (id == null || selectedRoute == null) {
            Timber.e("failed to plan trip, erp selected route does not exist")
            return
        }
        viewModel.erpRouteMap.getIfPresent(id)?.let { erpRouteMap ->
            val destinationAddress = erpRouteMap.erpFullRouteData.destination
            val sourceAddressDetails = erpRouteMap.erpFullRouteData.source
            val erpRouteGZip = GZIPCompressionUtil.compressToGzip(erpRouteMap.routeList.map {
                generateSimpleRouteObject(
                    it,
                    it.erpList?.filter { erp -> return@filter (erp.chargeAmount != null && erp.chargeAmount != 0.0F) }
                )
            })
            val args = Bundle()
            if (erpRouteMap.erpFullRouteData.timestamp > System.currentTimeMillis()) {
                args.putSerializable(Constants.ROUTE_PLANNING_MODE, RoutePlanningMode.PLANNED)
                args.putLong(
                    Constants.SELECTED_ERP_ROUTE_TS,
                    erpRouteMap.erpFullRouteData.timestamp
                )
                args.putByteArray(Constants.ERP_ROUTE_DETAILS_LIST, erpRouteGZip)
                args.putInt(Constants.SELECTED_ERP_ROUTE_INDEX, selectedRoute)
            } else {
                args.putSerializable(Constants.ROUTE_PLANNING_MODE, RoutePlanningMode.NORMAL)
                args.putLong(Constants.SELECTED_ERP_ROUTE_TS, -1L)
            }
            args.putSerializable(Constants.DESTINATION_ADDRESS_DETAILS, destinationAddress)
            args.putSerializable(Constants.SOURCE_ADDRESS_DETAILS, sourceAddressDetails)
            withContext(Dispatchers.Main) {
                showRouteAndResetToCenter(args)
            }
        }
    }

    suspend fun addERPRoutePreviewFragment(id: String, routeId: Int, isTimeChanged: Boolean) {
        viewModel.erpRouteMap.getIfPresent(id)?.let {
            val routeAdapterObjects = it.routeList[routeId]!!
            trackedERPDetailsId = id
            trackedERPRouteId = routeId
            val args = Bundle()
            args.putByteArray(
                Constants.SELECTED_ERP_ROUTE_DETAILS,
                GZIPCompressionUtil.compressToGzip(generateSimpleRouteObject(routeAdapterObjects))
            )
            args.putLong(
                Constants.SELECTED_ERP_ROUTE_TS,
                it.erpFullRouteData.timestamp!!
            )
            args.putBoolean(
                Constants.IS_TIME_CHANGED,
                isTimeChanged
            )

            withContext(Dispatchers.Main) {
                addFragment((ERPRoutePreviewFragment()), args, Constants.TAGS.ROUTE_PREVIEW_ERP)
            }
        }
    }

    private fun generateSimpleRouteObject(routeAdapterObjects: RouteAdapterObjects) =
        SimpleRouteAdapterObjects(
            routeAdapterObjects.route,
            routeAdapterObjects.arrivalTime,
            routeAdapterObjects.duration,
            routeAdapterObjects.distance,
            routeAdapterObjects.erpRate,
            routeAdapterObjects.erpList,
            routeAdapterObjects.originalResponse
        )

    private fun generateSimpleRouteObject(
        routeAdapterObjects: RouteAdapterObjects,
        erpList: List<ERPChargeInfo>?
    ) =
        SimpleRouteAdapterObjects(
            routeAdapterObjects.route,
            routeAdapterObjects.arrivalTime,
            routeAdapterObjects.duration,
            routeAdapterObjects.distance,
            routeAdapterObjects.erpRate,
            erpList,
            routeAdapterObjects.originalResponse
        )


    fun addTripLogsFragment(navigationParams: ReadableMap? = null) {
        Analytics.logClickEvent(Event.TRAVEL_LOG, getScreenName())

        addFragment(
            TravelLogFragment.createFragment(navigationParams),
            null, Constants.TAGS.TRAVEL_LOG_TAG
        )
    }

    override fun adjustRecenterButton(height: Int, index: Int, fromScreen: String) {
        if ((fromScreen == RNScreen.LANDING) || (fromScreen == RNScreen.CHOOSE_COLLECTION)) {
            runOnUiThread {
                DashboardMapStateManager.landingBottomSheetHeight = height
                DashboardMapStateManager.landingBottomSheetState = index
                dashBoardMapCamera.overviewEdgeInsets = EdgeInsets(
                    300.0.dpToPx(),
                    10.0.dpToPx(),
                    (height.toDouble() + 120).dpToPx(),
                    10.0.dpToPx(),
                )
                dashBoardMapCamera.maxEdgeInsets = EdgeInsets(
                    10.0.dpToPx(),
                    20.0.dpToPx(),
                    (height + 120.0).dpToPx(),
                    20.0.dpToPx()
                )
                dashBoardMapCamera.edgeCircleInsets = EdgeInsets(
                    30.0.dpToPx(), 60.0.dpToPx(),
                    (height.toDouble() + 130).dpToPx(),
                    60.0.dpToPx(),
                )
                callAfterInitialized {
                    dashBoardMapLanding?.handleUpdateCenterOfMarker()
                }

                viewBinding.rlFreeDriveInfo.setPadding(
                    0, 0, 0, height.dpToPx()
                )
            }
        }
    }

    private fun addMoreSettingsFragment() {
        Analytics.logClickEvent(Event.SETTINGS, getScreenName())
        addFragment(SettingsFragment(), null, SettingsFragment.TAG)
    }

    fun addTripLogsFragmentAndPop() {
        Analytics.logClickEvent(Event.TRAVEL_LOG, getScreenName())
        //callevent
        supportFragmentManager.popBackStack()
        addFragment(
            TravelLogFragment.newInstanceTabUpcoming(),
            null,
            Constants.TAGS.TRAVEL_LOG_TAG
        )
    }


    fun showDatePickerDialog(startDate: String?, endDate: String?, disableFutureDates: Boolean) {
        Analytics.logClickEvent(Event.TRAVEL_LOG, getScreenName())
        val navigationParams = getRNFragmentNavigationParams(
            sessionToken = myServiceInterceptor.getSessionToken(),
            fragmentTag = RNScreen.DATE_RANGE_PICKER
        ).apply {
            if (!startDate.isNullOrEmpty() && !endDate.isNullOrEmpty()) {
                putString("start_date", startDate)
                putString("end_date", endDate)
            }
            putBoolean("disable_future_dates", disableFutureDates)
        }
        val initialProperties = Bundle().apply {
            putString(Constants.RN_TO_SCREEN, RNScreen.DATE_RANGE_PICKER)
            putBundle(Constants.RN_CONSTANTS.NAVIGATION_PARAMS, navigationParams)
        }
        val reactNativeFragment = CustomReactFragment.newInstance(
            componentName = Constants.RN_CONSTANTS.COMPONENT_NAME,
            launchOptions = initialProperties,
            cleanOnDestroyed = false
        )
        addFragment(reactNativeFragment, null, RNScreen.DATE_RANGE_PICKER)
    }

    private fun handleUpdateEmailFromDeepLink() {
        if (intent.getBooleanExtra(IS_DEEPLINK_EMAIL_VERIFICATION, false)) {
            openEmailUpdateScreen()
        }
    }

    private fun openEmailUpdateScreen() {
        val bundle = Bundle()
        bundle.putBoolean(ProfileFragment.OPEN_EMAIL_FRAGMENT, true)
        bundle.putString(DEEPLINK_EMAIL_OTP, intent.getStringExtra(DEEPLINK_EMAIL_OTP))
        addFragment(ProfileFragment(), bundle, Constants.TAGS.SETTINGS_TAG)
    }

    private fun initAppSyncAndMapDataHandlers() {
        CoroutineScope(Dispatchers.IO).launch {
            breezeERPUtil.startERPHandler()
            breezeERPUtil.setListenerDataCallBack(callDataParserResponse = { erpResponse ->
                ERPControllerData.erpResponse = erpResponse
            }, callDataResponse = { dataErp ->
                /**
                 * send data to react
                 */
                invokeAfterReactCatalystInit {
                    sendErpEvent(dataErp)
                }
            })
            breezeIncidentsUtil.startIncidentHandler()
            breezeMiscRoadObjectUtil.startMiscROHandler()
            breezeSchoolSilverZoneUpdateUtil.startSchoolSilverZoneHandler()
            AppSyncHelper.subscribeToAppSyncEvents(applicationContext)
        }

    }

    private fun invokeAfterReactCatalystInit(callback: () -> Unit) {
        App.instance?.reactNativeHost?.reactInstanceManager?.let {
            it.addReactInstanceEventListener {
                callback.invoke()
            }
            it.currentReactContext?.let { currentReactContext ->
                if (currentReactContext.hasActiveCatalystInstance()) {
                    callback.invoke()
                }
            }
        }
    }

    private fun sendErpEvent(dataErp: String) {
        getShareBusinessLogicPackage()?.getRequestsModule()?.flatMap {
            val rnData = Arguments.createMap().apply {
                putString("ERP_DATA", dataErp)
            }
            it.callRN(ShareBusinessLogicEvent.SEND_ERP_DATA_TO_REACT.eventName, rnData)
        }?.subscribeOn(Schedulers.io())?.observeOn(AndroidSchedulers.mainThread())?.subscribe()
    }


    /**
     * send open inbox
     */
    private fun openInboxScreen(idNotification: Int = -1) {
        val currentFragment = supportFragmentManager.findFragmentById(R.id.frame_container)
        if (currentFragment != null && (currentFragment is ExploreMapsFragment
                    || currentFragment is RoutePlanningFragment
                    || currentFragment is TravelLogFragment
                    || currentFragment is SettingsFragment)
        ) {
            popAllFragmentsInBackStack()
        }

        getShareBusinessLogicPackage()?.getRequestsModule()?.flatMap {
            val rnData = Arguments.createMap().apply {
                putInt("NOTIFICATION_ID", idNotification)
            }
            it.callRN(ShareBusinessLogicEvent.NAVIGATE_TO_INBOX_SCREEN.eventName, rnData)
        }?.subscribeOn(Schedulers.io())?.observeOn(AndroidSchedulers.mainThread())?.subscribe()
    }

    fun pushScreenNameBottomTab(nameScreen: String) {
        removeExploreMap {
            getShareBusinessLogicPackage()?.getRequestsModule()?.flatMap {
                val rnData = Arguments.createMap().apply {
                    putString("screen_name", nameScreen)
                }
                it.callRN(ShareBusinessLogicEvent.SWITCH_BOTTOM_TAB.eventName, rnData)
            }?.subscribeOn(Schedulers.io())?.observeOn(AndroidSchedulers.mainThread())?.subscribe()
        }
    }

    private fun removeExploreMap(callback: () -> Unit) {
        val currentFragment = supportFragmentManager.findFragmentById(getFragmentContainer())
        if (currentFragment != null && currentFragment is ExploreMapsFragment) {
            Timber.w("BACKSTACK:: ----- [EXPORE MAP SHOULD BE DESTROYED] ----------")
            currentFragment.addDestroyCallback(callback)
            supportFragmentManager.popBackStack()
            //supportFragmentManager.popBackStackImmediate()
            //supportFragmentManager.beginTransaction().remove(currentFragment).commitNowAllowingStateLoss()
        } else {
            Timber.w("BACKSTACK:: ----- [BUG BUG BUG BUG] ----------")
            callback.invoke()
        }
    }


    /**
     * send open inbox
     */
    private fun readInboxEvent(idNotification: Int = -1) {
        getShareBusinessLogicPackage()?.getRequestsModule()?.flatMap {
            val rnData = Arguments.createMap().apply {
                putInt("NOTIFICATION_ID", idNotification)
            }
            it.callRN(ShareBusinessLogicEvent.READ_INBOX_EVENT.eventName, rnData)
        }?.subscribeOn(Schedulers.io())?.observeOn(AndroidSchedulers.mainThread())?.subscribe()
    }


    /**
     * send notice react that native open new screen
     */
    private fun openNotificationFromNativeEvent(categoryType: String = "") {
        getShareBusinessLogicPackage()?.getRequestsModule()?.flatMap {
            val rnData = Arguments.createMap().apply {
                putString("NOTIFICATION_CATEGORY", categoryType)
            }
            it.callRN(ShareBusinessLogicEvent.OPEN_NOTIFICATION_FROM_NATIVE.eventName, rnData)
        }?.subscribeOn(Schedulers.io())?.observeOn(AndroidSchedulers.mainThread())?.subscribe()
    }


    fun navigateToHomeTabAndReset() {
        callAfterInitialized {
            dashBoardMapLanding?.updateBottomSheetState(1, RNScreen.LANDING) {
                refreshHomeTab()
            }
        }
    }

    private fun refreshHomeTabTheme() {
        getShareBusinessLogicPackage()?.getRequestsModule()?.flatMap {
            it.callRN(ShareBusinessLogicEvent.SEND_THEME_CHANGE_EVENT.eventName, null)
        }?.subscribeOn(Schedulers.io())?.observeOn(AndroidSchedulers.mainThread())?.subscribe()
    }

    private fun refreshHomeTab() {
        getShareBusinessLogicPackage()?.getRequestsModule()?.flatMap {
            it.callRN(ShareBusinessLogicEvent.NAVIGATE_TO_HOME_TAB.eventName, null)
        }?.subscribeOn(Schedulers.io())?.observeOn(AndroidSchedulers.mainThread())?.subscribe()

    }


    /**
     * handle ERP
     */
    private fun handleERP(timeToCheckERP: Long) {
        dashBoardMapLanding?.handleERP(timeToCheckERP)
    }


    private fun initERPRefresh() {
        if (erpListener != null && (!erpListener!!.isDisposed)) {
            return
        }
        erpListener = RxBus.listen(RxEvent.ERPRefresh::class.java)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                Timber.d("Dashboard: Received rxevent for ERP refresh")
                dashBoardMapLanding?.handleERP(zoomToERPDefault = false)
            }
        compositeDisposable.add(erpListener!!)
    }


    /**
     * erp register erp open from marker view
     */
    private fun registerErpOpen() {
        erpOpenListener = RxBus.listen(RxEvent.OpenErpDetail::class.java)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                openERPDetail(it.erpID)
            }
        compositeDisposable.add(erpOpenListener!!)
    }

    /**
     * unregister erp open
     */
    private fun removeErpOpen() {
        erpOpenListener?.let {
            compositeDisposable.remove(it)
            erpOpenListener = null
        }
    }

    private fun removeERPRefresh() {
        erpListener?.let {
            compositeDisposable.remove(it)
            erpListener = null
        }
    }

    fun updateERPDetailTimeStamps(timestampToUse: Long) {
        removeERPRefresh()
        handleERP(timestampToUse)
    }

    private fun handleETAStartedEvent() {
        MapboxNavigationApp.current()
            ?.takeIf { mapboxNavigation -> mapboxNavigation.getTripSessionState() != TripSessionState.STARTED }
            ?.let { triggerAllCruiseMode() }
        if (ETAEngine.getInstance()!!.isLiveLocationSharingEnabled()) {
            ETAEngine.getInstance()!!.updateETAStatus(ETAStatus.INPROGRESS)
        }
    }

    private fun handleETALiveLocationToggleEvent() {
        if (ETAEngine.getInstance()!!.isETARunning()) {
            Analytics.logClickEvent(Event.ETA_RESUME, getScreenName())
        } else {
            Analytics.logClickEvent(Event.ETA_PAUSE, getScreenName())
        }
    }


    private fun triggerAllCruiseMode() {
        postEventWithCacheLast(
            AppToHomeTurnOnCruiseModeEvent(
                ETAEngine.getInstance()!!.isETAEnabled(),
                ETAEngine.getInstance()!!.isLiveLocationSharingEnabled(),
                true
            )
        )
    }

    @OptIn(ExperimentalPreviewMapboxNavigationAPI::class)
    @SuppressLint("MissingPermission")
    private fun initViews() {
        Timber.d("id_token : " + myServiceInterceptor.getSessionToken())
        initMap()
        viewBinding.mapView.setMaximumFps(30)

//        btn_feedback.setOnClickListener {
//            viewBinding.mapView.snapshot(onSnapShotReady)
//        }
//        btn_test.setOnClickListener {
//            //testNudge()
//            //showSavedAddressesNudge()
//            showPToolTipNudge()
//        }

        repeatCollectLatestFlowOnCreated(
            viewModel.currentLandingModeFlow.map { it == LandingMode.OBU || it == LandingMode.PARKING }) { isShowing ->
            viewBinding.amenitiesOnMap.isVisible = isShowing
            viewBinding.btnDropPinToggle.isVisible = isShowing
        }

        viewBinding.btnDropPinToggle.onToggleDropPin = { isOn, isFromUser ->
            if (isFromUser) {
                showToast(
                    "Drop pin mode ${if (isOn) "on" else "off"}",
                    if (isOn) R.drawable.ic_pin_selected else R.drawable.ic_pin_mode_off
                )
            }
            if (isOn) {
                showToast("Drop pin mode on", R.drawable.ic_pin_selected)
                viewBinding.amenitiesOnMap.setPetrolMode(true)
                viewBinding.amenitiesOnMap.setEVChargerMode(true)
                selectAmenity(arrayOf(EVCHARGER, PETROL), arrayOf(true, true))
                if (viewBinding.amenitiesOnMap.carParkViewOption != CarParkViewOption.ALL_NEARBY)
                    viewBinding.amenitiesOnMap.carParkViewOption = CarParkViewOption.ALL_NEARBY
                dashBoardMapLanding?.dropPinLayerManager?.initDropPinMarkerTooltip(viewBinding.mapView)
                dashBoardMapLanding?.dropPinLayerManager?.setAnalyticsScreenName("[homepage]")

            } else {
                viewBinding.amenitiesOnMap.setPetrolMode(false)
                viewBinding.amenitiesOnMap.setEVChargerMode(false)
                selectAmenity(arrayOf(EVCHARGER, PETROL), arrayOf(false, false))
                dashBoardMapLanding?.dropPinLayerManager?.removeMarkers(DropPinLayerManager.DROP_PIN_MARKER)
            }
            toggleLandingDropPinScreenRN(isOn)
            if (!isOn) {
                getShareBusinessLogicPackage()?.getRequestsModule()
                    ?.flatMap {
                        it.callRN(
                            ShareBusinessLogicEvent.TOGGLE_DROP_PIN_LANDING.name,
                            Arguments.fromBundle(bundleOf("isDropPin" to false))
                        )
                    }?.subscribeOn(Schedulers.io())?.observeOn(AndroidSchedulers.mainThread())
                    ?.subscribe()
                    ?.also { disposable -> compositeDisposable.add(disposable) }
            }

            if (isFromUser) {
                Analytics.logClickEvent(
                    eventValue = ":drop_pin_${if (isOn) "on" else "off"}",
                    screenName = "[homepage]",
                )
            }
        }

        viewBinding.btnRecenterMap.setOnClickListener {
            Analytics.logClickEvent(Event.RECENTER, getScreenName())
            MapboxNavigationApp.current()
                ?.takeIf { mapboxNavigation -> mapboxNavigation.getTripSessionState() != TripSessionState.STARTED }
                ?.let {
//                    setCameraToolTipThreshold()
//                    DashboardMapStateManager.resetCurrentLocationFocus()
                    dashBoardMapCamera.recenterMapBasedOnProfile(
                        // viewBinding.carParkViewOption.isCarParksShown(),
                        viewBinding.amenitiesOnMap.carParkViewOption.isShowingCarParks(),
                        getCurrentLocation = { getCurrentLocation() }) {
                        resetTrackerMode()
//                        dashBoardMapCamera.removeCameraToolTipThreshold()
                    }
                    val isInPanMode = panTrackingManager?.isInPanMode == true
                    panTrackingManager?.resetAll()
                    if (isInPanMode) {
                        getCarParkDataShowInMap()
                        getAmenitiesDataShowInMap()
                    }
                }
        }

        /**
         * new carpark option
         */
        viewBinding.carParkViewOption.setState(
            BreezeUserPreference.getInstance(this).getStateCarpParkOption()
        )
        viewBinding.carParkViewOption.setListener(object :
            CarParkStateView.ListenerFilterCarParkOption {
            override fun all() {

                /**
                 * update is carpark enable for PanTrackingManager
                 */
                panTrackingManager?.isCarParkEnable =
                    viewBinding.carParkViewOption.isCarParksShown()

                BreezeUserPreference.getInstance(applicationContext)
                    .setStateCarpParkOption(CarParkViewOption.ALL_NEARBY)

                /**
                 * send event to show button carpark list
                 */
                sendEventHideOrShowButtonCarparkList(isShow = true)


                if (panTrackingManager?.isInPanMode == true) {
                    handleGetCarParkWithNewLocationPan()
                } else {
                    /**
                     * call api to update setting carpark selected
                     */
                    viewModel.saveCarParkOptionUserSettings(CarParkViewOption.ALL_NEARBY)
                    Analytics.logClickEvent(Event.CAR_PARK_ALL_NEARBY, getScreenName())
                    toggleCarparks(EventSource.NONE)
                }
            }

            override fun available() {
                Analytics.logClickEvent(Event.CAR_PARK_AVAILABLE_ONLY, getScreenName())
                BreezeUserPreference.getInstance(applicationContext)
                    .setStateCarpParkOption(CarParkViewOption.AVAILABLE_ONLY)

                /**
                 * send event to show button carpark list
                 */
                sendEventHideOrShowButtonCarparkList(isShow = true)

                /**
                 * update is carpark enable for PanTrackingManager
                 */
                panTrackingManager?.isCarParkEnable =
                    viewBinding.carParkViewOption.isCarParksShown()


                if (panTrackingManager?.isInPanMode == true) {
                    handleGetCarParkWithNewLocationPan()
                } else {
                    /**
                     * call api to update setting carpark selected
                     */
                    viewModel.saveCarParkOptionUserSettings(CarParkViewOption.AVAILABLE_ONLY)
                    toggleCarparks(EventSource.NONE)

                }


            }

            override fun hide() {
                Analytics.logClickEvent(Event.CAR_PARK_HIDE, getScreenName())

                /**
                 * send event to hide button carpark list
                 */
                sendEventHideOrShowButtonCarparkList()

                BreezeUserPreference.getInstance(applicationContext)
                    .setStateCarpParkOption(CarParkViewOption.HIDE)

                /**
                 * update is carpark enable for PanTrackingManager
                 */
                panTrackingManager?.isCarParkEnable = false

                /**
                 * call api to update setting carpark selected
                 */
                viewModel.saveCarParkOptionUserSettings(CarParkViewOption.HIDE)
                postEventWithCacheLast(AppToCarParkEndEvent())

                // Hide carparks

                dashBoardMapLanding?.hideAllCarPark()

            }
        })
        viewBinding.carParkViewOption.setListenerTransition(object :
            CarParkStateView.ListenTransition {
            override fun collapse() {
                viewBinding.btnRecenterMap.isVisible = isCameraTrackingDismissed
            }

            override fun expand() {
                viewBinding.btnRecenterMap.isVisible = false
                callAfterInitialized {
                    dashBoardMapLanding?.updateBottomSheetState(0, RNScreen.LANDING)
                }
            }
        })

        viewBinding.dispatchTouchLayout.setDispatchListener(object :
            DispatchTouchLayout.OnDispatchListener {
            override fun onTouchDown(x: Float, y: Float) {
                if (!viewBinding.dispatchTouchLayout.isContainView(
                        viewBinding.amenitiesOnMap,
                        x.toInt(),
                        y.toInt()
                    )
                ) {
//                    viewBinding.carParkViewOption.collapse()
                    viewBinding.amenitiesOnMap.hideCarParkOptionsView()
                    //hidePToolTipNudge()
                }
            }
        })

        viewBinding.amenitiesOnMap.carParkViewOption = getBreezeUserPreference()
            .getStateCarpParkOption()

        viewBinding.amenitiesOnMap.onCarParkOptionsVisibilityChangedListener = { isShowing ->
            if (isShowing)
                dashBoardMapLanding?.updateBottomSheetState(0, RNScreen.LANDING)
        }

        viewBinding.amenitiesOnMap.setListener(object :
            AmenitiesOnMapStateView.AmenitiesOnMapListener {
            override fun onCarParkSelected(selectedOption: CarParkViewOption) {
                panTrackingManager?.isCarParkEnable = selectedOption.isShowingCarParks()
                BreezeUserPreference.getInstance(applicationContext)
                    .setStateCarpParkOption(selectedOption)

                sendEventHideOrShowButtonCarparkList(isShow = selectedOption.isShowingCarParks())

                if (selectedOption.isShowingCarParks()) {
                    if (panTrackingManager?.isInPanMode == true) {
                        handleGetCarParkWithNewLocationPan()
                    } else {
                        viewModel.saveCarParkOptionUserSettings(selectedOption)
                        Analytics.logClickEvent(
                            if (selectedOption == CarParkViewOption.ALL_NEARBY) Event.CAR_PARK_ALL_NEARBY else Event.CAR_PARK_AVAILABLE_ONLY,
                            getScreenName()
                        )
                        toggleCarParking(EventSource.NONE)
                        // toggleCarparks(EventSource.NONE)
//                        hidePToolTipNudge()
                    }
                } else {
                    Analytics.logClickEvent(Event.CAR_PARK_HIDE, getScreenName())
                    viewModel.saveCarParkOptionUserSettings(CarParkViewOption.HIDE)
                    postEventWithCacheLast(AppToCarParkEndEvent())

                    dashBoardMapLanding?.hideAllCarPark()

//                    hidePToolTipNudge()
                }
            }

            override fun onPetrolSelected(currentSelected: Boolean) {
                selectAmenity(arrayOf(PETROL, EVCHARGER), arrayOf(currentSelected, false))
            }

            override fun onEVChargerSelected(currentSelected: Boolean) {
                selectAmenity(arrayOf(EVCHARGER, PETROL), arrayOf(currentSelected, false))
            }

        })
//
//        viewBinding.btnExploreMapView.run {
//            isEnable = true
//            setOnClickListener {
//                Analytics.logClickEvent(Event.TAP_CUSTOM_MAP, getScreenName())
//                openExploreMapScreenFromDashboard(updateContentSeenState = true)
//            }
//        }

        /**
         * setup tracking pan -> get carpark around
         */
        setUpPanTrackingCarPark()

    }

    private fun toggleLandingDropPinScreenRN(isDropPinModeON: Boolean) {
        getShareBusinessLogicPackage()?.getRequestsModule()
            ?.flatMap {
                val data = bundleOf("isPinMode" to isDropPinModeON)
                it.callRN(
                    ShareBusinessLogicEvent.TOGGLE_LANDING_DROP_PIN_SCREEN.eventName,
                    Arguments.fromBundle(data)
                )
            }?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe()
    }

    private fun setUpPanTrackingCarPark() {
        /**
         * create instance PanTrackingManager
         */
        panTrackingManager = PanTrackingManager(viewBinding.mapView)

        panTrackingManager?.isCarParkEnable =
            viewBinding.amenitiesOnMap.carParkViewOption != CarParkViewOption.HIDE

        /**
         * should set enable carpark or not
         */
        val callBackUpdateCarParkWithNewLocation =
            object : PanTrackingManager.CallBackUpdateCarParkWithNewLocation {
                override fun panStart() {
                }

                override fun updateCarParkNewLocation() {
                    val locationCenterPan = panTrackingManager?.currentLocationPan
                    locationCenterPan?.let { lc ->
                        handleGetCarParkWithNewLocationPan()
                        getNearbyAmenitiesWitPannedLocation()
                    }
                }
            }
        panTrackingManager?.callBackUpdateCarParkWithNewLocation =
            callBackUpdateCarParkWithNewLocation
    }

    private fun getNearbyAmenitiesWitPannedLocation() {
        getLocationFocusedForReact()?.let {
            viewModel.getAllAmenities(it, null)
        }
    }

    private fun handleGetCarParkWithNewLocationPan() {
        val locationCenterPan = panTrackingManager?.currentLocationPan
        locationCenterPan?.let { lc ->
//            dashBoardMapLanding?.addNotificationMarker(
//                point = Point.fromLngLat(
//                    lc.longitude,
//                    lc.latitude
//                ),
//                //iconResId = resIconId
//                resIconUrl = "https://icons.iconarchive.com/icons/thehoth/seo/24/seo-panda-icon.png"
//            )
            if (DashboardMapStateManager.currentModeSelected == LandingMode.PARKING) {
                handleGetCarParkWithPanMap(lc, CarParkViewOption.ALL_NEARBY)
            } else if (BreezeUserPreference.getInstance(this@DashboardActivity)
                    .getStateCarpParkOption() != CarParkViewOption.HIDE
            ) {
                handleGetCarParkWithPanMap(
                    lc, BreezeUserPreference.getInstance(this@DashboardActivity)
                        .getStateCarpParkOption()
                )
            }
        }

    }

    private fun handleGetCarParkWithPanMap(location: Location, viewOption: CarParkViewOption) {
        viewModel.mListCarpark.observe(this@DashboardActivity, carParkObserver)
        val cpDistanceSetting = BreezeUserPreference.getInstance(applicationContext)
            .getCarParkDistanceSettingValue(BreezeCarUtil.masterlists)

        if (!DashboardMapStateManager.isInMapProfileSelectedMode) {
            viewModel.getCarParkDataShowInMap(
                currentLocation = location,
                dest = "",
                carParkAvailability = viewOption.mode,
                cpDistanceSetting = cpDistanceSetting
            )
        }
    }

//    private fun setCameraToolTipThreshold() {
//        if (DashboardMapStateManager.currentModeSelected == Constants.MODE_DASH_BOARD_SELLECTED.NO_MODE_SELLECT) {
//            dashBoardMapCamera.setCameraToolTipThreshold()
//        }
//    }

    fun openExploreMapScreenFromDashboard(
        data: String? = null,
        updateContentSeenState: Boolean = false
    ) {
        runOnUiThread {
            val args = bundleOf(
                ExploreMapsFragment.KEY_DATA to data,
                ExploreMapsFragment.KEY_UPDATE_CONTENT_SEEN_STATE to updateContentSeenState
            )

            addFragment(ExploreMapsFragment(), args, Constants.TAGS.EXPLORE_MAP)
        }
    }

    fun openSearch(fromScreen: String = Constants.RN_CONSTANTS.ROUTE_PLANNING) {
        //No need to sync the search screens as per confirmation from iOS carplay team
        //postEventToCar(OpenSearchScreenEvent)
        /*addFragment(SearchFragment.newInstance(), null, Constants.TAGS.SEARCH_TAG)*/
        Analytics.logEvent(Event.SEARCH, Event.SEARCH, getScreenName())

        val navigationParams = getRNFragmentNavigationParams(
            sessionToken = myServiceInterceptor.getSessionToken(),
            fragmentTag = RNScreen.SEARCH
        ).apply {
            putString(
                Constants.RN_FROM_SCREEN,
                fromScreen
            )
        }

        val initialProperties = bundleOf(
            Constants.RN_TO_SCREEN to RNScreen.SEARCH,
            Constants.RN_FROM_SCREEN to fromScreen,
            Constants.RN_CONSTANTS.NAVIGATION_PARAMS to navigationParams
        )
        val reactNativeFragment = ReactFragment.Builder()
            .setComponentName(Constants.RN_CONSTANTS.COMPONENT_NAME)
            .setLaunchOptions(initialProperties)
            .build()

        supportFragmentManager.beginTransaction()
            .add(
                getFragmentContainer(),
                reactNativeFragment,
                RNScreen.SEARCH
            )
            .addToBackStack(Constants.TAGS.SEARCH_TAG)
            .commit()
        supportFragmentManager.executePendingTransactions()
    }

    fun newFeedbackFragment() {
        snapshotImageBase64?.let {
            val args = Bundle()
            addFragmentBottomUp(FeedbackFragment(), args, Constants.TAGS.FEEDBACK_TAG)
        }
    }

    private fun toggleCarParksWithoutCar() {
        if (viewBinding.carParkViewOption.isCarParksShown()) {
            dashBoardMapLanding?.clearALlCarPark()
            getCarParkDataShowInMap()
        } else {
            dashBoardMapLanding?.hideAllCarPark()
            viewModel.shouldFocusToNearestCarpark = false
        }
    }

    private fun toggleCarParking(source: EventSource) {

        dashBoardMapLanding?.clearALlCarPark()

        getCarParkDataShowInMap()
        if (source == EventSource.NONE) {
            postEventWithCacheLast(AppToCarParkEvent(CarParkViewOption.ALL_NEARBY))
        }
    }

    private fun toggleCarparks(source: EventSource) {
        if (viewBinding.carParkViewOption.isCarParksShown()) {
            //remove all carpark
            dashBoardMapLanding?.clearALlCarPark()

            //get carpark in map
            //todo bangnv should filter with state of carpark option
            getCarParkDataShowInMap()
            if (source == EventSource.NONE) {
                postEventWithCacheLast(AppToCarParkEvent(viewBinding.carParkViewOption.currentState))
            }
        } else {
            dashBoardMapLanding?.hideAllCarPark()
            if (source == EventSource.NONE) {
                postEventWithCacheLast(AppToCarParkEndEvent())
            }
            handleProfileRecenter {}
        }
    }

    private fun getAmenitiesDataShowInMap() {
        val tempLocation = getLocationFocusedForReact()
        if (dashBoardMapLanding != null) {
            DashboardMapStateManager.activeLocationUse = tempLocation
            dashBoardMapLanding?.amenitiesLastLocation = tempLocation
        }

        if (tempLocation == null) {
            if (!DashboardMapStateManager.isInSearchLocationMode) {
                isAmenityLocationInit = false
            }
            return
        }

        viewModel.mListAmenities.observe(this, amenityObserver)
        if (!DashboardMapStateManager.isInMapProfileSelectedMode) {
            viewModel.getAllAmenities(tempLocation, null)
        } else {
            var amenities: ArrayList<String>? = null
            if (!DashboardMapStateManager.isInSearchLocationMode) {
                BreezeMapDataHolder.profilePreferenceHashmap[DashboardMapStateManager.selectedMapProfile]?.let {
                    amenities = it.amenities
                }
            }
            viewModel.getAllAmenities(tempLocation, amenities)
        }
    }


    /**
     * get config circle zone TBR
     */
    fun getZoneCongestionDetails(drawMapLayers: () -> Unit) {
        val profileSelected = DashboardMapStateManager.selectedMapProfile!!
        viewModel.mResponseCongestionDetail.observe(this, Observer {
            viewModel.mResponseCongestionDetail.removeObservers(this)
            it?.let {
                if (dashBoardMapLanding != null) {
                    mapCongestionManager?.reDrawCircle(
                        selectedMapProfile = profileSelected,
                        true,
                        dashBoardMapCamera.edgeCircleInsets
                    )
                    mapCongestionManager?.updateColorCircleLayer(it, profileSelected)
                    drawMapLayers.invoke()
                }
            }
        })
        viewModel.getZoneCongestionDetails(profileSelected)
    }

    private fun getCarParkDataShowInMap() {
        if (DashboardMapStateManager.currentModeSelected != LandingMode.PARKING && !viewBinding.amenitiesOnMap.carParkViewOption.isShowingCarParks())
            return
        val carParkOptions = when (DashboardMapStateManager.currentModeSelected) {
            LandingMode.PARKING -> CarParkViewOption.ALL_NEARBY
            LandingMode.OBU -> viewBinding.amenitiesOnMap.carParkViewOption
            else -> null
        } ?: return
        fetchFilteredCarParks(carParkOptions)
    }

    fun shouldFocusNearestCarpark() = viewModel.shouldFocusToNearestCarpark
    fun updateShouldFocusNearestCarpark(value: Boolean) {
        viewModel.shouldFocusToNearestCarpark = value
    }

    private fun fetchFilteredCarParks(viewOption: CarParkViewOption) {
        val tempLocation =
            dashBoardMapCamera.getCurrentCenterLocation { getCurrentLocation() }
        if (dashBoardMapLanding != null) {
            DashboardMapStateManager.activeLocationUse = tempLocation
            dashBoardMapLanding?.carparkLastLocation = tempLocation
        }
        if (tempLocation == null) {
            if (!DashboardMapStateManager.isInSearchLocationMode) {
                isCarLocationInit = false
            }
            return
        }

        viewModel.mListCarpark.observe(this, carParkObserver)

        var destinationName: String? = null
        if (DashboardMapStateManager.isInSearchLocationMode) {
            DashboardMapStateManager.searchLocationAddress?.let {
                destinationName =
                    if (it.destinationName.isNullOrEmpty()) it.address1 else it.destinationName
            }
        }

        val cpDistanceSetting = BreezeUserPreference.getInstance(applicationContext)
            .getCarParkDistanceSettingValue(BreezeCarUtil.masterlists)

        if (!DashboardMapStateManager.isInMapProfileSelectedMode) {
            val routablePointCarParkId = if (DashboardMapStateManager.isInSearchLocationMode)
                DashboardMapStateManager.searchLocationAddress?.getSelectedRoutablePoint()?.carparkId
                    ?: DashboardMapStateManager.searchLocationAddress?.carParkID
            else null
            viewModel.getCarParkDataShowInMap(
                currentLocation = tempLocation,
                dest = destinationName,
                carParkAvailability = viewOption.mode,
                cpDistanceSetting = cpDistanceSetting,
                routablePointCarParkId = routablePointCarParkId
            )
        } else {
            var profileRadius = cpDistanceSetting
            if (!DashboardMapStateManager.isInSearchLocationMode) {
                BreezeMapDataHolder.profilePreferenceHashmap[DashboardMapStateManager.selectedMapProfile]?.let {
                    if (it.zones.size > 0) {
                        it.zones.last().radius?.toDouble()?.let { radiusMeters ->
                            profileRadius = radiusMeters / 1000
                        }
                    }
                }
            }
            viewModel.getCarParkDataShowInMap(
                currentLocation = tempLocation,
                dest = destinationName,
                cpDistanceSetting = profileRadius,
            )
        }
    }


    private fun showCarParkVeryFarGN() {
        if (BreezeUserPreference.getInstance(this)
                .isInAppGuideTutorialPending() || isShowingCarparkList || panTrackingManager?.isInPanMode == true
        ) {
            return
        }
        if (dashBoardMapLanding?.canShowCarParkMarkers() == true) {
            GlobalNotification()
                .setIcon(R.drawable.ic_notification_no_carpark)
                .setTheme(GlobalNotification.ThemeTutorial)
                .setTitle(getString(R.string.nearby_carpark))
                .setDescription(getString(R.string.msg_nearby_carpark_found))
                .setActionText(null)
                .setActionListener(null)
                .setDismissDurationSeconds(10)
                .addToList(listNotificationGlobal)
                .show(this)
        }
    }

    private fun dismissGlobalNotification() {
        listNotificationGlobal.forEach {
            it?.dimissGlobalNotification()
        }
    }

    private fun showNoCarParkFoundGN() {
        if (BreezeUserPreference.getInstance(this).isInAppGuideTutorialPending()) {
            return
        }
        if (dashBoardMapLanding?.canShowCarParkMarkers() == true) {
            GlobalNotification()
                .setIcon(R.drawable.ic_notification_no_carpark)
                .setTheme(GlobalNotification.ThemeTutorial)
                .setTitle(getString(R.string.nearby_carpark_cruise_mode))
                .setDescription(getString(R.string.msg_nearby_carpark_found_cruisemode))
                .setActionText(null)
                .setActionListener(null)
                .setDismissDurationSeconds(10)
                .show(this)
        }
    }

    private fun canShowOBUChargeResultGN(): Boolean {
        if (BreezeUserPreference.getInstance(this).isInAppGuideTutorialPending())
            return false
        if (MapboxNavigationApp.current()?.getTripSessionState() == TripSessionState.STARTED) {
            val currentTopFragment =
                supportFragmentManager.findFragmentById(viewBinding.frameContainer.id)
            return currentTopFragment !is OBULiteFragment
        }
        return true
    }

    fun showOBUChargeResultGN(data: OBURoadEventData) {
        if (!canShowOBUChargeResultGN()) return
        val icon = when (data.eventType) {
            OBURoadEventData.EVENT_TYPE_PARKING_SUCCESS -> R.drawable.ic_notification_no_carpark
            OBURoadEventData.EVENT_TYPE_ERP_SUCCESS -> R.drawable.erp_off
            else -> R.drawable.warning
        }
        val theme = when (data.eventType) {
            OBURoadEventData.EVENT_TYPE_PARKING_SUCCESS -> GlobalNotification.ThemeParking
            OBURoadEventData.EVENT_TYPE_ERP_SUCCESS -> GlobalNotification.ThemeERP
            else -> GlobalNotification.ThemeWarningError
        }
        val title = when (data.eventType) {
            OBURoadEventData.EVENT_TYPE_PARKING_SUCCESS,
            OBURoadEventData.EVENT_TYPE_PARKING_FAILURE ->
                "Parking Fee - ${data.chargeAmount}"

            OBURoadEventData.EVENT_TYPE_ERP_FAILURE,
            OBURoadEventData.EVENT_TYPE_ERP_SUCCESS ->
                "ERP Fee - ${data.chargeAmount}"

            else -> ""
        }

        val description = when (data.eventType) {
            OBURoadEventData.EVENT_TYPE_PARKING_SUCCESS,
            OBURoadEventData.EVENT_TYPE_ERP_SUCCESS -> "Deduction successful"

            OBURoadEventData.EVENT_TYPE_ERP_FAILURE,
            OBURoadEventData.EVENT_TYPE_PARKING_FAILURE -> "Deduction failed"

            else -> ""
        }
        GlobalNotification()
            .setIcon(icon)
            .setTheme(theme)
            .setTitle(title)
            .setDescription(description)
            .setActionText(null)
            .setActionListener(null)
            .setDismissDurationSeconds(10)
            .show(this)
    }

    fun showLowCardGN(balanceSGD: Double) {
        if (BreezeUserPreference.getInstance(this).isInAppGuideTutorialPending()) {
            return
        }
        GlobalNotification()
            .setIcon(R.drawable.warning)
            .setTheme(GlobalNotification.ThemeWarningError)
            .setTitle("Low card balance - $${balanceSGD.round(2)}")
            .setDescription("Please top up your card")
            .setActionText(null)
            .setActionListener(null)
            .setDismissDurationSeconds(10)
            .show(this)
    }


    /**
     * callback click home button from react
     */
    fun handleClickHome() {
        dashBoardMapLanding?.clearAllMarkerNotification()
        if (!isTripRunning()) {
            checkForUpdates()
            checkForPNudgeDisplay()
        }
    }

    private fun handleGlobalNotificationEvent(
        globalNotificationData: GlobalNotificationData,
        customTitle: String? = null
    ) {
        if (globalNotificationData.notificationItem.isShowInAppNotification != true) return
        val iconUrl = globalNotificationData.getThemedImageURL(userPreferenceUtil.isDarkTheme())
        val notificationTitle = customTitle ?: globalNotificationData.generateTitle("")
        val notification = GlobalNotification()
            .setIconUrl(iconUrl ?: "")
            .setTitle(notificationTitle)
            .setDescription(globalNotificationData.notificationItem.message ?: "")
            .setDismissDurationSeconds(10)
            .setSeeListener { isAutoDismiss, isDismissWithoutAction ->
                /*to match with iOS, setting notification as seen as soon as it appears on the screen */
                if (isDismissWithoutAction) {
                    checkForPNudgeDisplay()
                }
            }
            .addToList(listNotificationGlobal)
        when {
            globalNotificationData.notificationItem.openScreen == NotificationItem.ScreenName.EXPLORE_MAP.nameStr -> {
                notification.setActionText(resources.getString(R.string.read_more))
                notification.setTheme(GlobalNotification.ThemeDefault)
                notification.setActionListener {
                    openExploreMapScreenFromDashboard(globalNotificationData.notificationItem.screenProperties?.toString())
                }
            }

            globalNotificationData.notificationItem.openScreen == NotificationItem.ScreenName.VOUCHER.nameStr -> {
                val voucherId =
                    globalNotificationData.notificationItem.screenProperties
                        ?.takeIf { it.isJsonObject }?.asJsonObject
                        ?.get("voucherId")?.asString ?: ""
                showCarParkVoucherNotification(
                    voucherId,
                    globalNotificationData.notificationItem.title ?: "",
                    globalNotificationData.notificationItem.message ?: ""
                )
                saveNotificationAsSeen(globalNotificationData)
                return
            }

            globalNotificationData.notificationItem.category == NotificationCategoryEnum.TUTORIAL.text ||
                    globalNotificationData.notificationItem.category == NotificationCategoryEnum.CUSTOM_INBOX.text -> {
                notification.setActionText(resources.getString(R.string.read_more))
                notification.setTheme(GlobalNotification.ThemeTutorial)
                notification.setActionListener {
                    Analytics.logClickEvent(
                        Event.GLOBAL_NOTIFICATION_WATCH_QUICK_START_VIDEO,
                        Screen.getScreenNameGlobalNotification(notificationTitle)
                    )
                    globalNotificationData.notificationItem.notificationId?.let { id ->
                        openInboxScreen(id)
                        readInboxEvent(id)
                    }
                }
            }

            globalNotificationData.notificationItem.category == NotificationCategoryEnum.CATEGORY_ADMIN.text -> {
                notification.setActionText(resources.getString(R.string.read_more))
                notification.setTheme(GlobalNotification.ThemeAdmin)
                notification.setActionListener {
                    Analytics.logClickEvent(
                        Event.GLOBAL_NOTIFICATION_READ_MORE,
                        Screen.getScreenNameGlobalNotification(notificationTitle)
                    )

                    globalNotificationData.notificationItem.notificationId?.let { id ->
                        openInboxScreen(id)
                        readInboxEvent(id)
                    }
                }
            }

            globalNotificationData.notificationItem.category == NotificationCategoryEnum.CATEGORY_TRAFFIC_ALERTS.text -> {
                notification.setActionText(resources.getString(R.string.see_on_map))
                notification.setTheme(GlobalNotification.ThemeWarningError)
                notification.setActionListener {
                    Analytics.logClickEvent(
                        Event.GLOBAL_NOTIFICATION_SEE_ON_MAP,
                        Screen.getScreenNameGlobalNotification(notificationTitle)
                    )

                    popAllFragmentsInBackStack()
                    if (globalNotificationData.notificationItem.longitude != null && globalNotificationData.notificationItem.latitude != null) {
                        val resIconUrl =
                            globalNotificationData.getThemedImageURL(userPreferenceUtil.isDarkTheme())
                        openNotificationFromNativeEvent(NotificationCategoryEnum.CATEGORY_TRAFFIC_ALERTS.text)

//                        dismissed camera before move to location ( avoid auto move to current location)
                        onCameraTrackingDismissed()

                        Handler(Looper.getMainLooper()).postDelayed({
                            dashBoardMapLanding?.addNotificationMarker(
                                point = Point.fromLngLat(
                                    globalNotificationData.notificationItem.longitude!!,
                                    globalNotificationData.notificationItem.latitude!!
                                ), resIconUrl = resIconUrl
                            )
                        }, 1000)

                        globalNotificationData.notificationItem.notificationId?.let { id ->
                            readInboxEvent(id)
                        }
                    }
                }
            }
        }


        if (globalNotificationData.isGroupNotification()) {
            notification.setActionText(resources.getString(R.string.read_more))
            notification.setActionListener {
                Analytics.logClickEvent(Event.GLOBAL_NOTIFICATION_READ_MORE, "")
                openInboxScreen()
                Handler(Looper.getMainLooper()).postDelayed({
                    popAllFragmentsInBackStack()
                }, 1000)

            }
        }
        notification.show(this)
        saveNotificationAsSeen(globalNotificationData)
    }

    private fun saveNotificationAsSeen(globalNotificationData: GlobalNotificationData) {
        globalNotificationData.seenNotifications.takeIf { it.isNotEmpty() }
            ?.let {
                CoroutineScope(Dispatchers.IO).launch {
                    breezeGlobalNotificationsUtil.listItemSee.addAll(it)
                    BreezeUserPreference.getInstance(applicationContext).saveNotification(
                        breezeGlobalNotificationsUtil.listItemSee
                    )
                }
            }
    }


    /**
     * fun call when react setting user preferences done
     * should be called after style is loaded( where dashBoardMapLanding is initialized)
     */
    fun initUserPreferenceSetting(navigationParams: ReadableMap?) {
        isRNBottomSheetReady = true
        navigationParams?.let {
            val dataStringAmeniti =
                it.getArray("amenities_preference").toString()

            val dataStringNotification =
                it.getArray("notification_preference").toString()

            val dataProfile = it.getArray("profiles").toString()

            val gson = Gson()

            if (!dataStringAmeniti.isNullOrEmpty()) {
                val myType = object : TypeToken<ArrayList<AmenitiesPreference>>() {}.type
                val newAmenityPref = gson.fromJson<ArrayList<AmenitiesPreference>>(
                    dataStringAmeniti,
                    myType
                )
                getUserDataPreference()?.saveAmenityFilter(newAmenityPref)
                val isAmenityDataChanged =
                    (newAmenityPref != BreezeMapDataHolder.amenitiesPreference)
                if (BreezeMapDataHolder.amenitiesPreference.isEmpty() ||
                    (shouldAmenitiesBeFetchedForActiveLoc()) ||
                    isAmenityDataChanged ||
                    BreezeMapDataHolder.shouldReLoadAmenities
                ) {
                    BreezeMapDataHolder.shouldReLoadAmenities = false
                    BreezeMapDataHolder.amenitiesPreference = newAmenityPref
                    val amenityTypes =
                        ArrayList(BreezeMapDataHolder.amenitiesPreferenceHashmap.keys)
                    if (!isAmenityStyleImagesLoaded) {
                        isAmenityStyleImagesLoaded = true
                        CoroutineScope(Dispatchers.IO).launch {
                            dashBoardMapLanding?.loadAmenityStyleImages(amenityTypes)
                            withContext(Dispatchers.Main) {
                                viewModel.shouldFocusToNearestCarpark = true
                                toggleCarParksWithoutCar()
                                getAmenitiesDataShowInMap()
                            }
                        }
                    } else if (isAmenityDataChanged) {
                        CoroutineScope(Dispatchers.IO).launch {
                            dashBoardMapLanding?.loadAmenityStyleImages(amenityTypes)
                            withContext(Dispatchers.Main) {
                                getAmenitiesDataShowInMap()
                            }
                        }
                    } else {
                        getAmenitiesDataShowInMap()
                    }
                }
            }

            /**
             * parser data profiles
             */
            if (!dataProfile.isNullOrEmpty()) {

                val myTypeProfile = object : TypeToken<ArrayList<ProfileAmenities>>() {}.type
                val newAmenityPref = gson.fromJson<ArrayList<ProfileAmenities>>(
                    dataProfile,
                    myTypeProfile
                )

                if (newAmenityPref != null) {
                    BreezeMapDataHolder.profilePreference = newAmenityPref
                }
            }


            /**
             * parser data notification
             */
            if (!dataStringNotification.isNullOrEmpty()) {
                val myType = object : TypeToken<ArrayList<NotificationPreference>>() {}.type
                BreezeMapDataHolder.notificationPreference =
                    gson.fromJson<ArrayList<NotificationPreference>>(
                        dataStringNotification,
                        myType
                    )
            }
        }
        handlePushNotificationOpenContent()
    }

    private fun shouldAmenitiesBeFetchedForActiveLoc() =
        ((DashboardMapStateManager.activeLocationUse == null) || (DashboardMapStateManager.activeLocationUse?.latitude != dashBoardMapLanding?.amenitiesLastLocation?.latitude ||
                DashboardMapStateManager.activeLocationUse?.longitude != dashBoardMapLanding?.amenitiesLastLocation?.longitude))


    fun selectAmenity(amenityTypes: Array<String>, isSelected: Array<Boolean>) {
        var shouldReturn = false
        BreezeMapDataHolder.profilePreferenceHashmap.forEach {
            amenityTypes.forEachIndexed { index, amenityType ->
                if (amenityType == it.value.elementId) {
                    manageAmenityProfile(amenityType, isSelected[index])
                    shouldReturn = true
                }
            }

        }

        if (shouldReturn) return

        val currentAmenities = BreezeMapDataHolder.amenitiesPreference
        currentAmenities.forEach {
            amenityTypes.forEachIndexed { index, amenityType ->
                if (it.elementName.equals(amenityType)) {
                    it.isSelected = isSelected[index]
                }
            }

        }
        /*
         * forcing invocation of amenitiesPreference setter to correctly update amenitiesPreferencehashmap and isAmenitiesSelected
         */
        BreezeMapDataHolder.amenitiesPreference = currentAmenities
        amenityTypes.forEachIndexed { index, amenityType ->
            viewModel.recalculateFurthestAmenity(amenityType, isSelected[index])
        }


        /**
         * update amenities selected  on map
         */
        if (dashBoardMapLanding != null) {
            amenityTypes.forEachIndexed { index, amenityType ->
                dashBoardMapLanding?.filterAllAmenitiesSelect(amenityType, isSelected[index])
            }
        }
//        setCameraToolTipThreshold()
        if (panTrackingManager?.isInPanMode == false) {
            handleProfileRecenter {
////            dashBoardMapCamera.removeCameraToolTipThreshold()
            }
        }
    }

    fun selectAmenity(amenityType: String, isSelected: Boolean) {
        BreezeMapDataHolder.profilePreferenceHashmap.forEach {
            if (amenityType == it.value.elementId) {
                manageAmenityProfile(amenityType, isSelected)
                return@selectAmenity
            }
        }

        val currentAmenities = BreezeMapDataHolder.amenitiesPreference
        currentAmenities.forEach {
            if (it.elementName.equals(amenityType)) {
                it.isSelected = isSelected
            }
        }
        /*
         * forcing invocation of amenitiesPreference setter to correctly update amenitiesPreferencehashmap and isAmenitiesSelected
         */
        BreezeMapDataHolder.amenitiesPreference = currentAmenities
        viewModel.recalculateFurthestAmenity(amenityType, isSelected)


        /**
         * update amenities selected  on map
         */
        dashBoardMapLanding?.filterAllAmenitiesSelect(amenityType, isSelected)
//        setCameraToolTipThreshold()
        handleProfileRecenter {
//            dashBoardMapCamera.removeCameraToolTipThreshold()
        }
    }

    //FIXME: refactor code based on requirements
    private fun manageAmenityProfile(amenityType: String, isSelected: Boolean) {
        if (isSelected) {
            showAmenityProfileInfo(amenityType)
        } else {
            hideAmenityProfileInfo()
        }
    }

    private fun showAmenityProfileInfo(amenityType: String) {
        DashboardMapStateManager.selectedMapProfile = amenityType
        val location = Location("")
        val zones =
            BreezeMapDataHolder.profilePreferenceHashmap[DashboardMapStateManager.selectedMapProfile]?.zones
        if (zones?.isNotEmpty() == true) {
            location.latitude = zones[0].centerPointLat!!
            location.longitude = zones[0].centerPointLong!!
        }

        var profileAddress = DestinationAddressDetails(
            null,
            null,
            null,
            location.latitude.toString(),
            location.longitude.toString(),
            null,
            null,
            null
        )
        profileAddress.apply {
            DashboardMapStateManager.isInMapProfileSelectedMode = true
            DashboardMapStateManager.resetCurrentLocationFocus()
            DashboardMapStateManager.selectedMapProfileLocationAddress = this
            DashboardMapStateManager.activeLocationUse =
                DashboardMapStateManager.selectedMapProfileLocation
        }
        /**
         * get zone color
         */
        getZoneCongestionDetails() {
            toggleCarParksWithoutCar()
            getAmenitiesDataShowInMap()
        }
    }

    private fun hideAmenityProfileInfo() {
        if (DashboardMapStateManager.selectedMapProfile == null) {
            DashboardMapStateManager.isInMapProfileSelectedMode = false
            return
        }
        DashboardMapStateManager.isInMapProfileSelectedMode = false
        DashboardMapStateManager.selectedMapProfile = null
        mapCongestionManager?.hideProfileZoneLayer()
        // dashBoardMapCamera.recenterMap(viewBinding.carParkViewOption.isCarParksShown()) { getCurrentLocation() }
        dashBoardMapCamera.recenterMap(viewBinding.amenitiesOnMap.carParkViewOption.isShowingCarParks()) { getCurrentLocation() }

        /**
         * set active location maplanding to current location
         */
        DashboardMapStateManager.activeLocationUse = getCurrentLocation()
        // if (viewBinding.carParkViewOption.isCarParksShown()) {
        if (viewBinding.amenitiesOnMap.carParkViewOption.isShowingCarParks()) {
            getCarParkDataShowInMap()
        }
        getAmenitiesDataShowInMap()
    }

    /**
     * this function call when user selected any mode or unselected mode
     */
    fun switchLandingModeFromRN(selectedModeValue: String) {
        if (!lifecycle.currentState.isAtLeast(Lifecycle.State.STARTED)) return


        LandingMode.values().find { it.mode == selectedModeValue }
            ?.let { mode ->
                if (mode != DashboardMapStateManager.currentModeSelected)
                    dashBoardMapLanding?.carParkDestinationIconManager?.removeDestinationIconTooltip()
                DashboardMapStateManager.currentModeSelected = mode
                viewModel.updateCurrentLandingMode(mode)
                panTrackingManager?.resetAll()
                runOnUiThread {
                    handleModeSelect(true)
                }
            }
    }

    private fun handleModeSelect(shouldReloadCarParks: Boolean) {

        if (DashboardMapStateManager.shouldShowMapProfileZones()) {
            mapCongestionManager?.showProfileZoneLayer(
                true,
                dashBoardMapCamera.edgeCircleInsets
            )
        } else {
            mapCongestionManager?.hideProfileZoneLayer()
        }

        when (DashboardMapStateManager.currentModeSelected) {
            LandingMode.PARKING -> handleParkingMode(shouldReloadCarParks)
            LandingMode.ERP -> handleERPMode()
            LandingMode.OBU -> handleOBUMode()
//            Constants.MODE_DASH_BOARD_SELLECTED.NO_MODE_SELLECT -> {
//                handleNoModeSelect(isModeChanged)
//            }
            else -> {}
        }
    }

    /**
     * + change search mode state
     * + remove search location marker
     * + set active location maplanding to current location
     * + move camera to  current location
     */
    private fun resetSearchState(): Location? {
        val activeLocationInstance = DashboardMapStateManager.activeLocationUse
        DashboardMapStateManager.isInSearchLocationMode = false
        dashBoardMapLanding?.removeSearchLocationMarker()
        DashboardMapStateManager.activeLocationUse = getCurrentLocation()
        resetTrackerMode()
        handleProfileRecenter {}
        DashboardMapStateManager.resetCurrentLocationFocus()
        return activeLocationInstance
    }

    private fun handleProfileRecenter(onFinishRecenter: () -> Unit) {
        val profileSelected = dashBoardMapCamera.recenterMapBasedOnProfile(
            // viewBinding.carParkViewOption.isCarParksShown(),
            viewBinding.amenitiesOnMap.carParkViewOption.isShowingCarParks(),
            getCurrentLocation = { getCurrentLocation() }) {
            onFinishRecenter.invoke()
        }
        if (profileSelected) {
            resetTrackerMode()
        }
    }

    /**
     * Opens ERP info in a new Fragment WITHOUT THE MAP
     */
    fun showERPInfoRN(erpId: Int, timestamp: Long) {
        //val screenTag = RNScreen.ERP_DETAIL
        val screenTag = RNScreen.ERP
        val navigationParams = getRNFragmentNavigationParams(
            sessionToken = myServiceInterceptor.getSessionToken(),
            fragmentTag = screenTag
        )
        navigationParams.putInt(Constants.RN_CONSTANTS.ERP_ID, erpId)
        navigationParams.putBoolean(Constants.RN_CONSTANTS.ERP_IS_SELECTED, true)
        navigationParams.putLong(Constants.RN_CONSTANTS.ERP_SELECTED_TS, timestamp)

        val initialProperties = bundleOf(
            Constants.RN_TO_SCREEN to screenTag,
            Constants.RN_CONSTANTS.NAVIGATION_PARAMS to navigationParams
        )
        val bottomPanelFragment = CustomReactFragment.newInstance(
            componentName = Constants.RN_CONSTANTS.COMPONENT_NAME,
            launchOptions = initialProperties,
            cleanOnDestroyed = false
        )
        addFragment(bottomPanelFragment, null, screenTag)
    }

    /**
     * handle search ERP
     */
    fun openERPDetail(erpID: Int) {
        val args = Bundle()
        args.putSerializable(ErpDetailFragment.ERP_ID, erpID)
        addFragment(
            ErpDetailFragment(),
            args,
            Constants.TAGS.ERP_DETAIL_TAG
        )
    }


    /**
     * handle search ERP
     */
    fun openTrafficDetail(expressWayCode: String) {
        val args = Bundle()
        args.putSerializable(TrafficDetailFragment.TRAFFIC_CODE, expressWayCode)
        addFragment(
            TrafficDetailFragment(),
            args,
            Constants.TAGS.TRAFFIC_DETAIL_TAG
        )
    }


    /**
     * handle event
     * NOTE: init user preference is also triggered by rn,
     * which will call amenity api, so skipping invocation here
     * + change search mode state
     * + remove search location marker
     * + set active location maplanding to current location
     * + move camera to  current location
     */
    fun handleClearSearch() {
        panTrackingManager?.resetAll()
        resetSearchState()
        resetAmenitiesAndRecenter()
        DashboardMapStateManager.resetCurrentLocationFocus()
    }

    /**
     * Check if active location and last amenity/carpark loction where data is loaded are differnet
     * If location is different reload the amenities and carpark data
     * Else if amenities or carpark data is empty , reload the amenities and carpark data
     * If Mode is ERP, reload erp for the current location
     * reset selection view and layouts
     */
    private fun resetAmenitiesAndRecenter() {
        if (dashBoardMapLanding == null) {
            return
        }
        dashBoardMapLanding?.carParkDestinationIconManager?.run {
            removeDestinationIconTooltip()
            removeDestinationIcon()
        }
        resetSelectedMode()
        fetchCarParkAmenitiesOnReset()
        fetchERPOnReset()
        resetCarParkMarkers()

    }

    private fun fetchCarParkAmenitiesOnReset() {
        if (shouldDataBeLoadedForActiveLoc()) {
            dashBoardMapLanding?.clearALlMarkerAndTooltipAmenities()
            toggleCarParksWithoutCar()
            getAmenitiesDataShowInMap()
        } else {
            if (dashBoardMapLanding?.isAmenitiesEmpty() == true) {
                getAmenitiesDataShowInMap()
            }
            if (dashBoardMapLanding?.isCarParkEmpty() == true) {
                toggleCarParksWithoutCar()
            }
        }
    }


    private fun fetchERPOnReset() {
        if (DashboardMapStateManager.currentModeSelected == LandingMode.ERP) {
            handleERP(System.currentTimeMillis())
        }
    }

    private fun resetCarParkMarkers() {
        if (DashboardMapStateManager.currentModeSelected == LandingMode.PARKING) {
            dashBoardMapLanding?.showAllCarPark()
        }
    }


    private fun shouldDataBeLoadedForActiveLoc() =
        ((dashBoardMapLanding?.amenitiesLastLocation?.latitude != DashboardMapStateManager.activeLocationUse?.latitude) || (dashBoardMapLanding?.amenitiesLastLocation?.longitude != DashboardMapStateManager.activeLocationUse?.longitude)
                || (dashBoardMapLanding?.carparkLastLocation?.latitude != DashboardMapStateManager.activeLocationUse?.latitude) || (dashBoardMapLanding?.carparkLastLocation?.longitude != DashboardMapStateManager.activeLocationUse?.longitude))

//    private fun handleNoModeSelect(isModeChanged: Boolean) {
//
//        handleShowOrHideMakerWithMode()
//        viewBinding.carParkViewOption.show()
//        // if (isModeChanged && viewBinding.carParkViewOption.isCarParksShown()) {
//        if (isModeChanged && viewBinding.amenitiesOnMap.isCarParkShown) {
//            getCarParkDataShowInMap()
//        }
//        handleProfileRecenter {}
//    }

    private fun resetSelectedMode() {

        handleShowOrHideMakerWithMode()
//
//        if (DashboardMapStateManager.currentModeSelected == Constants.MODE_DASH_BOARD_SELLECTED.NO_MODE_SELLECT) {
//            viewBinding.carParkViewOption.show()
//        }
//
        dashBoardMapCamera.recenterMap(viewBinding.amenitiesOnMap.carParkViewOption.isShowingCarParks()) { getCurrentLocation() }
    }

    fun showToastSaveAdress(type: String, message: String) {
        if (type == "01") {
            val resIconId = R.drawable.tick_successful
            GlobalToast().setDescription(message)
                .setIcon(resIconId)
                .setDismissDurationSeconds(5)
                .show(this)
        }
    }

    fun handleSaveAddressFromTooltip(data: ReadableMap) {
        //refresh bookmark icon in the tooltip
        data.let {
            val stringBookMarkData = it.getArray("bookmarks").toString()
            val gson = Gson()
            if (!stringBookMarkData.isNullOrEmpty()) {
                val myType = object : TypeToken<ArrayList<PlaceDetailsResponse>>() {}.type
                val listObjectBookMarks = gson.fromJson<ArrayList<PlaceDetailsResponse>>(
                    stringBookMarkData,
                    myType
                )
                if (listObjectBookMarks.size > 0) {
                    runOnUiThread {
                        dashBoardMapLanding?.refreshBookmarkIconTooltip(listObjectBookMarks[0].bookmarkId)
                    }
                }
            }
        }
    }

    fun onReceiveTrafficDataRN(data: ReadableMap) {
        callAfterInitialized {
            runOnUiThread {
                handleShowOrHideMakerWithMode()
                data.getArray("data").toString().takeIf { it.isNotEmpty() }?.let { trafficCameras ->
                    val gson = Gson()
                    val myType = object : TypeToken<ArrayList<TrafficResponse>>() {}.type
                    val listTraffic =
                        gson.fromJson<ArrayList<TrafficResponse>>(trafficCameras, myType)
                    val currentFragment =
                        supportFragmentManager.findFragmentById(getFragmentContainer())

                    if (currentFragment != null) {
                        if (currentFragment is TrafficDetailFragment) {
                            currentFragment.handleTraffic(listTraffic)
                        } else {
                            dashBoardMapLanding?.handleTraffic(listTraffic)
                        }
                    }
                }
            }
        }
    }


    /**
     * handle sellected camera
     */
    fun handleSelectedCamera(cameraID: String) {

        val currentFragment = supportFragmentManager.findFragmentById(getFragmentContainer())

        if (currentFragment != null) {
            if (currentFragment is TrafficDetailFragment) {
                currentFragment.handleSelectedCamera(cameraID)
            } else {
                dashBoardMapLanding?.handleSelectedCamera(cameraID)
            }
        }
    }

    fun handleNotificationClick(dataJson: String) {
        kotlin.runCatching {
            val myType = object : TypeToken<NotificationItem>() {}.type
            Gson().fromJson<NotificationItem>(dataJson, myType)
                ?.takeIf { notificationItem ->
                    notificationItem.category == NotificationCategoryEnum.CATEGORY_TRAFFIC_ALERTS.text
                            && notificationItem.longitude != null
                            && notificationItem.latitude != null
                }
                ?.let { notificationItem ->
                    /**
                     * dismissed camera before move to location ( avoid auto move to current location)
                     */
                    onCameraTrackingDismissed()
                    Handler(Looper.getMainLooper()).postDelayed({
                        dashBoardMapLanding?.addNotificationMarker(
                            point = Point.fromLngLat(
                                notificationItem.longitude!!,
                                notificationItem.latitude!!
                            ),
                            resIconUrl = notificationItem.getThemedIconUrl(userPreferenceUtil.isDarkTheme())
                        )
                    }, 1000)
                }
        }
    }


    /**
     * handle notification click
     */
    fun sendUnReadMessageList(data: String) {
        data.let {
            val gson = Gson()
            val myType = object : TypeToken<Array<Int>>() {}.type
            val listNotySeen = try {
                gson.fromJson<Array<Int>>(
                    it,
                    myType
                ).toMutableList()
            } catch (e: Exception) {
                ArrayList()
            }

            /**
             * get notification show first time open app
             */
            breezeGlobalNotificationsUtil.readNotificationIds.clear()
            breezeGlobalNotificationsUtil.readNotificationIds.addAll(listNotySeen)

            loadSeenNotificationList()
        }
    }

    fun handleSharingCollection(data: String) {
        isSharing = true
        data.let {
            val excludedComponents = ArrayList<ComponentName>()
            val i = Intent(Intent.ACTION_SEND)
            i.type = "text/plain"
            i.putExtra(Intent.EXTRA_TEXT, data)
            val resInfo: List<ResolveInfo> =
                packageManager.queryIntentActivities(i, 0)

            resInfo.forEach {
                val packageName = it.activityInfo.packageName
                val name = it.activityInfo.name
                if (
                    packageName.contains("com.facebook") ||
                    packageName.contains("com.google.android.gm") ||
                    packageName.contains("com.android.mms")
                ) {
                    excludedComponents.add(ComponentName(packageName, name))
                }
            }
            if (excludedComponents.size == resInfo.size) {
                Toast.makeText(this@DashboardActivity, "No apps to share !", Toast.LENGTH_SHORT)
                    .show()
            } else {
                val chooserIntent = Intent.createChooser(i, data)
                chooserIntent.putExtra(
                    Intent.EXTRA_EXCLUDE_COMPONENTS,
                    excludedComponents.toTypedArray()
                )
                startActivity(chooserIntent)
            }
        }
    }


    override fun handleLogoutSuccess(callback: () -> Unit) {
        breezeGlobalNotificationsUtil.listItemSee.clear()
        breezeGlobalNotificationsUtil.readNotificationIds.clear()
        super.handleLogoutSuccess(callback)
    }

    private fun loadSeenNotificationList() {
        CoroutineScope(Dispatchers.IO).launch {
            var seenNotifications: List<SeenNotification> = ArrayList<SeenNotification>()
            BreezeUserPreference.getInstance(this@DashboardActivity).getLocalNotification()
                .let {
                    seenNotifications = it
                }
            withContext(Dispatchers.Main) {
                breezeGlobalNotificationsUtil.listItemSee.clear()
                breezeGlobalNotificationsUtil.listItemSee.addAll(seenNotifications)
                breezeGlobalNotificationsUtil.getNotificationShowFirstTime()
            }
        }
    }

    fun setUserOnHome(userOnHome: Boolean) {
        mIsUserOnHome = userOnHome
    }

    fun toggleDropPinMode(isOn: Boolean) {
        runOnUiThread {
            viewBinding.btnDropPinToggle.setDropPinMode(isOn)
        }
    }

    private fun handleShowOrHideMakerWithMode() {
        if (dashBoardMapLanding == null) {
            return
        }
        viewBinding.btnDropPinToggle.setDropPinMode(false)
        when (DashboardMapStateManager.currentModeSelected) {
            LandingMode.PARKING,
            LandingMode.OBU -> {
                dashBoardMapLanding?.switchMapProfile(
                    MapProfileType.CLUSTERED,
                    viewBinding.amenitiesOnMap.carParkViewOption.isShowingCarParks(),
                    false
                )
                removeERPRefresh()
                dashBoardMapLanding?.hideAllAmenitiesAndCarParks()
                dashBoardMapLanding?.clearGlobalNotificationMarkers()
                dashBoardMapLanding?.clearALlERP()
                dashBoardMapLanding?.clearAllTraffic()
            }

            LandingMode.ERP -> {
                initERPRefresh()
                dashBoardMapLanding?.hideAllAmenitiesAndCarParks()
                dashBoardMapLanding?.clearGlobalNotificationMarkers()
                dashBoardMapLanding?.clearAllTraffic()
            }

            LandingMode.TRAFFIC -> {
                removeERPRefresh()
                dashBoardMapLanding?.hideAllAmenitiesAndCarParks()
                dashBoardMapLanding?.clearGlobalNotificationMarkers()
                dashBoardMapLanding?.hideAllCarPark()
                dashBoardMapLanding?.clearALlERP()
            }

            else -> {}
        }
    }


    /**
     * handle mode ERP
     */
    private fun handleERPMode() {
        /**
         * handle show or hide marker for mode
         */
        handleShowOrHideMakerWithMode()
        handleERP(System.currentTimeMillis())

        /**
         * try remove listen open erp first
         */
        removeErpOpen()

        /**
         * register erp click icon right now
         */
        registerErpOpen()
    }

    private fun handleParkingMode(shouldReloadCarParks: Boolean) {
        viewBinding.carParkViewOption.gone()
        handleShowOrHideMakerWithMode()
        selectAmenity(
            arrayOf(EVCHARGER, PETROL),
            arrayOf(
                viewBinding.amenitiesOnMap.isEVChargerShown,
                viewBinding.amenitiesOnMap.isPetrolShown
            )
        )
        if (shouldReloadCarParks) {
            fetchFilteredCarParks(CarParkViewOption.ALL_NEARBY)
        }
    }

    private fun handleOBUMode() {
        viewBinding.carParkViewOption.gone()
        handleShowOrHideMakerWithMode()
        selectAmenity(
            arrayOf(EVCHARGER, PETROL),
            arrayOf(
                viewBinding.amenitiesOnMap.isEVChargerShown,
                viewBinding.amenitiesOnMap.isPetrolShown
            )
        )
        viewBinding.amenitiesOnMap.carParkViewOption.takeIf { it.isShowingCarParks() }
            ?.let { option ->
                fetchFilteredCarParks(option)
            }
    }

    private fun newAddressFragment(
        destinationAddressDetails: Serializable?
    ) {
        val args = Bundle()
        args.putSerializable(Constants.DESTINATION_ADDRESS_DETAILS, destinationAddressDetails)
        addFragment(NewAddressFragment(), args, Constants.TAGS.ADD_NEW_BREEZE)
    }


    @OptIn(ExperimentalPreviewMapboxNavigationAPI::class)
    @SuppressLint("MissingPermission")
    private fun initMap() {

        dashBoardMapCamera = DashBoardMapCamera(viewBinding.mapView.getMapboxMap(), viewModel)

        initializeLocationComponent()

        viewportDataSource = MapboxNavigationViewportDataSource(
            viewBinding.mapView.getMapboxMap()
        )

        navigationCamera = NavigationCamera(
            viewBinding.mapView.getMapboxMap(),
            viewBinding.mapView.camera,
            viewportDataSource
        )

//        initNavigation()

        //TODO: handle duplicate location engines

        registerLocationUpdates()

        val viewTreeObserver: ViewTreeObserver = viewBinding.mapView.viewTreeObserver
        if (viewTreeObserver.isAlive) {
            viewTreeObserver.addOnGlobalLayoutListener(object : OnGlobalLayoutListener {
                override fun onGlobalLayout() {
                    viewBinding.mapView.viewTreeObserver.removeOnGlobalLayoutListener(this)
                    viewportDataSource.evaluate()
                }
            })
        }

        viewBinding.mapView.camera.addCameraAnimationsLifecycleListener(
            NavigationScaleGestureHandler(
                this,
                navigationCamera,
                viewBinding.mapView.getMapboxMap(),
                getGesturesPlugin(),
                locationComponent,
                {
                    viewportDataSource
                        .options
                        .followingFrameOptions
                        .zoomUpdatesAllowed = false
                }
            ).apply { initialize() }
        )

        viewBinding.mapView.gestures.addOnFlingListener {
            Timber.d("On fling is called")
            Analytics.logMapInteractionEvents(Event.DRAG_MAP, getScreenName())
        }

        viewBinding.mapView.gestures.addOnRotateListener(object : OnRotateListener {
            override fun onRotate(detector: RotateGestureDetector) {}

            override fun onRotateBegin(detector: RotateGestureDetector) {}

            override fun onRotateEnd(detector: RotateGestureDetector) {
                Timber.d("On rotate is called")
                Analytics.logMapInteractionEvents(Event.ROTATE_MAP, getScreenName())
            }

        })

        viewBinding.mapView.setBreezeDefaultOptions()

        initStyle(basicMapStyleReqd = true)
        TripLoggingManager.setupTripLogging(
            this.applicationContext,
            breezeTripLoggingUtil,
            breezeStatisticsUtil
        )
    }

    private fun initDashBoardMapLanding() {
        /**
         * setup dashboard activity
         */
        val mapListenerCallBack = object :
            ListenerCallbackLandingMap {
            override fun moreInfoButtonClick(ameniti: BaseAmenity) {
                showParkingInfoRN(ameniti.id!!)
            }

            override fun onDismissedCameraTracking() {
                onCameraTrackingDismissed()
            }

            override fun onMapOtherPointClicked(point: Point) {
                // no op required
            }

            override fun onERPInfoRequested(erpID: Int) {
                showERPInfoRN(erpId = erpID, timestamp = System.currentTimeMillis())
            }

            override fun poiMoreInfoButtonClick(ameniti: BaseAmenity) {
                showPoiAmenityInfoRN(ameniti.id!!)
            }
        }

        mapCongestionManager =
            MapCongestionManager(
                viewBinding.mapView.getMapboxMap(),
                this.applicationContext,
                viewBinding.mapView.getMapboxMap().getStyle()!!
            )
        dashBoardMapLanding = DashBoardMapLanding().apply {
            setUp(
                pMapView = viewBinding.mapView,
                pMapboxNavigation = MapboxNavigationApp.current(),
                pContext = this@DashboardActivity,
                nightModeEnabled = userPreferenceUtil.isDarkTheme(),
                isDisableErpClick = false,
                pCallBack = mapListenerCallBack
            )
            CoroutineScope(Dispatchers.Main).launch {
                loadTrafficStyleImages()
                loadDropPinStyleImages()
            }
        }.also {
            dashBoardMapCamera.setDashboardMapLanding(it)
        }
    }

    private fun resetTrackerMode() {
        isCameraTrackingDismissed = false
        viewBinding.btnRecenterMap.isVisible = false
        locationComponent.addOnIndicatorPositionChangedListener(
            onIndicatorPositionChangedListener
        )
        getGesturesPlugin().addOnMoveListener(onMoveListener)
    }

    fun isDropPinEnabled() =
        lifecycle.currentState.isAtLeast(Lifecycle.State.STARTED) && viewBinding.btnDropPinToggle.isOn

    fun handleShowingRouteToLocationFromRN(
        selectedIndex: Int,
        searchLocationData: ReadableMap
    ) {
        val routePlanningData = searchLocationData.toHashMap()
        routePlanningData["selectedIndex"] = selectedIndex
        runOnUiThread {
            closeOBULiteScreen()
            val fragment =
                supportFragmentManager.findFragmentByTag(Constants.TAGS.ROUTE_PLANNING)
            if (fragment != null && !fragment.isRemoving) {
                supportFragmentManager.setFragmentResult(
                    Constants.ROUTE_PLANNING_FRAGMENT_REQUEST_KEY,
                    bundleOf(Pair(Constants.SELECTED_ADDRESS_DETAILS, routePlanningData))
                )
                supportFragmentManager.popBackStack()
            } else {
                screenNavigationHelper.addRoutePlanningFragmentFromRN(routePlanningData)
            }
        }
    }

    fun closeOBULiteScreen() {
        val obuFragment = supportFragmentManager.findFragmentByTag(OBULiteFragment.TAG)
        if (obuFragment is OBULiteFragment) {
            kotlin.runCatching {
                obuFragment.closeOBULiteScreen()
            }

        }
    }

    fun handleSearchedLocationFromRN(location: ReadableMap) {
        if (!lifecycle.currentState.isAtLeast(Lifecycle.State.STARTED)) return
        runOnUiThread {
            val routePlanningFragment =
                supportFragmentManager.findFragmentByTag(Constants.TAGS.ROUTE_PLANNING)
            if (routePlanningFragment != null && !routePlanningFragment.isRemoving) {
                supportFragmentManager.setFragmentResult(
                    Constants.ROUTE_PLANNING_FRAGMENT_REQUEST_KEY,
                    bundleOf(Pair(Constants.SELECTED_ADDRESS_DETAILS, location.toHashMap()))
                )
                supportFragmentManager.popBackStack()
            } else {
                callAfterInitialized {
                    onSearchedLocation(location.toHashMap())
                }
            }
        }
    }

    private fun onSearchedLocation(locationMapData: HashMap<String, Any>) {
        DashboardMapStateManager.searchLocationAddress =
            DestinationAddressDetails.parseSearchLocationDataFromRN(locationMapData)
        DashboardMapStateManager.isInSearchLocationMode = true
        panTrackingManager?.resetAll()
        DashboardMapStateManager.resetCurrentLocationFocus()
        val previousActiveLocationUse = DashboardMapStateManager.activeLocationUse
        /**
         * remove current tracking the first
         */
        onCameraTrackingDismissed()
        DashboardMapStateManager.activeLocationUse =
            DashboardMapStateManager.searchLocationAddress?.getRoutableLocation()

        resetTrackerMode()

        dashBoardMapLanding?.displaySearchLocation(previousActiveLocationUse, locationMapData)

        // if (viewBinding.carParkViewOption.isCarParksShown()) {
        if (viewBinding.amenitiesOnMap.carParkViewOption.isShowingCarParks()) {
            getCarParkDataShowInMap()
        }

        handleModeSelect(!viewBinding.amenitiesOnMap.carParkViewOption.isShowingCarParks())
    }

    private fun initializeLocationComponent() {

        locationComponent = viewBinding.mapView.location.apply {
            addOnIndicatorPositionChangedListener(onIndicatorPositionChangedListener)
            enabled = true
            pulsingEnabled = true
            locationPuck = LocationPuck2D(
                bearingImage = ContextCompat.getDrawable(
                    this@DashboardActivity,
                    R.drawable.cursor
                )
            )
        }
        defaultLocationProvider = locationComponent.getLocationProvider()

    }

    private fun initStyle(basicMapStyleReqd: Boolean = false, after: () -> Unit = {}) {
        viewBinding.mapView.getMapboxMap().loadStyleUri(
            getMapboxStyle(basicMapStyleReqd)
        ) { style ->

            // BREEZES-4577: a rare condition where ondestroy is called, but style is not yet loaded
            if (this@DashboardActivity.isDestroyed) {
                return@loadStyleUri
            }
            style.removeStyleLayer("illegalparkingcamera")
            style.removeStyleLayer("speedcamera")
            style.removeStyleLayer("redlightcamera")

            setUpGestureListeners()
            after()
            initDashBoardMapLanding()
        }
    }

    private fun setUpGestureListeners() {
        getGesturesPlugin().addOnMoveListener(onMoveListener)
    }


    @SuppressLint("MissingPermission")
    fun registerLocationUpdates() {
        LocationBreezeManager.getInstance().startLocationUpdates()
        ETAEngine.getInstance()?.getEtaLocationUpdateInstance(ETAMode.Cruise)?.let {
            LocationBreezeManager.getInstance().registerLocationCallback(it)
        }
        LocationBreezeManager.getInstance().registerLocationCallback(locationObserverCallBack)
    }


    private val locationObserverCallBack =
        object : LocationBreezeManager.CallBackLocationBreezeManager {
            override fun onNewLocationMatcherResult(locationMatcherResult: LocationMatcherResult) {
                //no op
            }

            override fun onSuccess(result: LocationEngineResult) {
                runOnUiThread {
                    if (dashBoardMapLanding != null) {
                        if (!isCarLocationInit) {
                            isCarLocationInit = true
                            /**
                             * show carpark first time
                             */
                            toggleCarParksWithoutCar()
                        }
                        if (!isAmenityLocationInit) {
                            isAmenityLocationInit = true
                            getAmenitiesDataShowInMap()
                        }

                    }

                }
            }

            override fun onFailure(p0: java.lang.Exception) {
                //no op
            }

            override fun onNewRawLocation(rawLocation: Location) {
                //no op
            }
        }

    //FIXME: This is invoked from RN the first time maplanding is loaded and the first time search location is loaded
    // So it is called 2 extra times even without navigation from activity
    override fun onResume() {
        super.onResume()
        viewBinding.carParkViewOption.gone()
        Timber.d("AppUpdate: -- onResume --")
        checkUpcomingTrip()
        if (this.supportFragmentManager.backStackEntryCount == 0) {
            setDashboardActivityStatusBarBasedOnTheme()
        } else {
            setDashboardActivityStatusBar()
        }

        if (Utils.isNavigationStarted && Utils.phoneNavigationStartEventData != null) {
            Timber.d("AppUpdate: -- isNavigationStarted is TRUE. Skipping version check for now. --")
            turnByTurnNavigationStarted = true
            startNavigation(
                Utils.phoneNavigationStartEventData!!.originalDestinationAddressDetails,
                Utils.phoneNavigationStartEventData!!.destinationAddressDetails,
                Utils.phoneNavigationStartEventData!!.route,
                Utils.phoneNavigationStartEventData!!.compressedRouteOriginalResp,
                Utils.phoneNavigationStartEventData!!.etaRequest,
                Utils.phoneNavigationStartEventData!!.walkingRoute,
                notificationCriterias = Utils.phoneNavigationStartEventData!!.notificationCriterias
            )
        } else {
            turnByTurnNavigationStarted = false


            if (!isTripRunning()) {
                Timber.d("AppUpdate: -- App update version check triggered.. --")
                checkForUpdates()
                handlePushNotificationOpenContent()
                handleSharedLocationToken()
            } else {
                Timber.d("AppUpdate: -- isTripRunning is TRUE. Skipping version check for now.. --")
            }
            breezeGlobalNotificationsUtil.startGlobalNotificationHelper()
        }

        /**
         * call event to react to goback save collection page
         */
        if (isSharing) {
            dismissDialogSharing()
            isSharing = false
        }
    }

    private fun handleSharedLocationToken() {
        val token = getAppPreference()?.getString(AppPrefsKey.KEY_SHARED_LOCATION_DATA_TOKEN)
            ?.takeIf { it.isNotEmpty() } ?: return
        getAppPreference()?.clear(AppPrefsKey.KEY_SHARED_LOCATION_DATA_TOKEN)
        when {
            token.startsWith(BreezeAppPreference.PREFIX_COLLECTION) -> {
                handleSharedCollectionToken(token.substring(BreezeAppPreference.PREFIX_COLLECTION.length))
            }

            token.startsWith(BreezeAppPreference.PREFIX_LOCATION) -> {
                openSharedLocationScreenInternal(token.substring(BreezeAppPreference.PREFIX_LOCATION.length))
            }

            token.startsWith(BreezeAppPreference.PREFIX_LOCATION_DESTINATION) -> {
                openSharedLocationScreenInternal(token.substring(BreezeAppPreference.PREFIX_LOCATION_DESTINATION.length))
            }

            else -> {}
        }
    }


    override fun onPause() {
        breezeGlobalNotificationsUtil.stopGlobalNotificationHelper()
        super.onPause()
    }

    fun setDashboardActivityStatusBar() {
        window.apply {
            clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            var flags = window.decorView.systemUiVisibility // get current flag
            flags =
                flags or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN// add LIGHT_STATUS_BAR to flag
            window.decorView.systemUiVisibility = flags
            //decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            statusBarColor = Color.TRANSPARENT
        }
    }

    fun setDashboardActivityStatusBarBasedOnTheme() {
        when (resources.configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK) {
            Configuration.UI_MODE_NIGHT_NO -> {
                window.apply {
                    clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
                    addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                    var flags = window.decorView.systemUiVisibility // get current flag
                    flags =
                        flags or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN// add LIGHT_STATUS_BAR to flag
                    window.decorView.systemUiVisibility = flags
                    //decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    statusBarColor = Color.TRANSPARENT
                }
            }

            Configuration.UI_MODE_NIGHT_YES -> {
                window.apply {
                    clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
                    addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                    var flags = window.decorView.systemUiVisibility // get current flag
                    flags =
                        flags and View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR.inv() // remove light status bar
                    flags = flags or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    window.decorView.systemUiVisibility = flags
                    //decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    statusBarColor = Color.TRANSPARENT
                }
            }
        }
    }

    fun setDashboardForgotPasswordStatusBar() {
        window.apply {
            clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            var flags = window.decorView.systemUiVisibility // get current flag
            flags = flags or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR// add LIGHT_STATUS_BAR to flag
            flags = flags and View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN.inv()
            window.decorView.systemUiVisibility = flags
            //decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            statusBarColor = Color.TRANSPARENT
        }
    }

    /**
    // * todo bangnv set state if need
    // */
    private fun handlePushNotificationOpenContent() {
        if (getAppPreference() == null) return
        val channel = getAppPreference()!!.getString(AppPrefsKey.PUSH_NOTIFICATION_CHANNEL)
        val id = getAppPreference()!!.getString(AppPrefsKey.PUSH_NOTIFICATION_ID)
        val extra = getAppPreference()!!.getString(AppPrefsKey.PUSH_NOTIFICATION_EXTRA)
        Timber.d("-- FCM - handlePushNotificationOpenContent[$channel][$id][$extra] --")

        when (channel) {
            FCMListenerService.NOTIFICATION_CHANNEL_ID_UPCOMING_TRIPS -> {
                loadUpcomingTripDetailsForPlanning(id)
            }

            FCMListenerService.NOTIFICATION_CHANNEL_ID_CARPARK_VOUCHER -> {
                if (!isRNBottomSheetReady) {
                    return
                }
                if (extra != null && extra.toIntOrNull() != null) {
                    setPushNotificationAsRead(extra.toInt())
                }
                showCarParkVoucherRN(id ?: "")
            }

            FCMListenerService.NOTIFICATION_CHANNEL_ID_MISC -> {
                if (!isRNBottomSheetReady) {
                    return
                }
                val type = extra
                when (type) {
                    FCMListenerService.NOTIFICATION_TYPE_OPEN_INBOX -> {
                        var idNotification = -1
                        id?.let { it ->
                            try {
                                idNotification = id.toInt()
                                setPushNotificationAsRead(idNotification)
                            } catch (ignored: Exception) {
                            }
                        }
                        openInboxScreen(idNotification)
                    }

                    FCMListenerService.NOTIFICATION_TYPE_OPEN_EXPLORE_MAP -> {
                        val contentCategoryID =
                            getAppPreference()!!.getString(AppPrefsKey.KEY_CONTENT_CATEGORY_ID)
                        val jsonObj = JsonObject()
                        jsonObj.addProperty("contentID", id?.toInt())
                        jsonObj.addProperty("contentCategoryID", contentCategoryID?.toInt())
                        openExploreMapScreenFromDashboard(jsonObj.toString())
                    }
                }
            }

            else -> {
                //executePendingGlobalNotifications()
                loadSeenNotificationList()
                //checkForPNudgeDisplay()
            }
        }
        getAppPreference()?.saveString(AppPrefsKey.PUSH_NOTIFICATION_CHANNEL, "")
        getAppPreference()?.saveString(AppPrefsKey.PUSH_NOTIFICATION_ID, "")
        getAppPreference()?.saveString(AppPrefsKey.PUSH_NOTIFICATION_EXTRA, "")
    }

    private fun setPushNotificationAsRead(seenMessageId: Int) {
        val seenNotificationArray = arrayOf(
            SeenNotification().apply {
                notificationId = seenMessageId
                startTime = System.currentTimeMillis().toInt()
                expireTime = (System.currentTimeMillis() + 5000).toInt()
            }
        )
        val seenNotificationData = GlobalNotificationData(
            notificationItem = NotificationItem(notificationId = seenMessageId),
            seenNotifications = seenNotificationArray
        )
        saveNotificationAsSeen(seenNotificationData)
        breezeGlobalNotificationsUtil.readNotificationIds.add(seenMessageId)
        readInboxEvent(seenMessageId)
    }

    private fun openSharedLocationScreenInternal(token: String) {
        // add small delay for Landing bottom sheet RN screen to load
        Handler(mainLooper).postDelayed({
            addFragment(
                SharedLocationFragment.newInstance(token),
                null,
                RNScreen.SHARED_LOCATION
            )
        }, 300)

    }

    private fun handleSharedCollectionToken(token: String) {
        viewModel.getSharedCollectionFromToken(token) {
            Handler(mainLooper).postDelayed({
                if (it.isSaved()) {
                    addFragment(
                        fragment = PlaceSaveDetailFragment(),
                        bundle = bundleOf(
                            PlaceSaveDetailFragment.ARG_COLLECTION_ID to it.collectionId,
                            PlaceSaveDetailFragment.ARG_TO_RN_SCREEN to RNScreen.COLLECTION_DETAILS,
                            PlaceSaveDetailFragment.ARG_COLLECTION_NAME to it.name,
                            PlaceSaveDetailFragment.ARG_COLLECTION_CODE to it.code,
                            PlaceSaveDetailFragment.ARG_COLLECTION_DESCRIPTION to it.description
                        ),
                        tag = RNScreen.COLLECTION_DETAILS
                    )
                } else {
                    addFragment(
                        fragment = SharedCollectionFragment.newInstance(token),
                        bundle = null,
                        tag = RNScreen.SHARED_COLLECTION
                    )
                }
            }, 200)
        }
    }

    private fun showCollectionTokenErrorDialog(errorData: ErrorDataResponse) {
        AlertDialog.Builder(this)
            .setMessage(errorData.message)
            .setPositiveButton(
                android.R.string.ok
            ) { dialog, _ ->
                /**
                 * open collection bookmark
                 */
                openCollectionList()

                dialog?.dismiss()
            }
            .show()
    }

    /**
     * exceute pending notification after close tutorial
     */
    private fun executePendingGlobalNotifications(
        callback: (proceedForNextAction: Boolean) -> Unit = {}
    ) {
        if (BreezeUserPreference.getInstance(this).isInAppGuideTutorialPending()) {
            callback.invoke(false)
            return
        }
        runOnUiThread {
            if (pendingGlobalNotifications.size > 0) {
                if (pendingGlobalNotifications.size > 1) {
                    val lastNotification = pendingGlobalNotifications.last()
                    if (lastNotification.isGroupNotification()) {
                        handleGlobalNotificationEvent(lastNotification)
                    } else {
                        handleGlobalNotificationEvent(
                            globalNotificationData = lastNotification,
                            customTitle = "${lastNotification.notificationItem.title} and ${pendingGlobalNotifications.size - 1} other alerts"
                        )
                    }
                } else {
                    handleGlobalNotificationEvent(pendingGlobalNotifications[0])
                }
                pendingGlobalNotifications.clear()
                callback.invoke(false)
            } else {
                callback.invoke(true)
            }
        }
    }

    fun showUpcomingTripNotification(
        tripPlannerId: String,
        title: String,
        message: String
    ) {
        GlobalNotification()
            .setTitle(title)
            .setDescription(message)
            .setIcon(R.drawable.ic_vc_upcoming_trip_arrow_navigation)
            .setTheme(GlobalNotification.ThemeMiscellaneous)
            .setActionText(getString(R.string.action_start_trip))
            .setActionListener {
                Analytics.logClickEvent(
                    Event.GLOBAL_NOTIFICATION_START_TRIP,
                    Screen.getScreenNameGlobalNotification(title)
                )
                loadUpcomingTripDetailsForPlanning(tripPlannerId)
            }
            .addToList(listNotificationGlobal)
            .show(this)
    }

    fun showCarParkVoucherNotification(
        voucherId: String,
        title: String,
        message: String
    ) {
        GlobalNotification()
            .setTitle(title)
            .setDescription(message)
            .setIcon(R.drawable.voucher_tooltip)
            .setTheme(GlobalNotification.ThemeSuccess)
            .setActionText(getString(R.string.action_my_vouchers))
            .setActionListener {
                Analytics.logClickEvent(Event.GLOBAL_NOTIFICATION_MY_VOUCHERS, "")
                showCarParkVoucherRN(voucherId)
            }
            .addToList(listNotificationGlobal)
            .show(this)
    }

    private fun loadUpcomingTripDetailsForPlanning(id: String?) {
        try {
            val longId: Long = id!!.toLong()
            viewModel.upcomingTripDetails.observe(this, Observer {
                viewModel.upcomingTripDetails.removeObservers(this)
                openRoutePlanningScreen(it)
            })
            lifecycleScope.launch {
                viewModel.getUpcomingTripDetails(longId)
            }
        } catch (e: Exception) {
            Timber.e("Invalid trip id received. $e")
        }
    }

    private fun openRoutePlanningScreen(details: UpcomingTripDetailResponse) {
        //callevent
        supportFragmentManager.popBackStack()
//        addFragment(TravelLogFragment.newInstance(showBackKey = true), null, Constants.TAGS.TRAVEL_LOG_TAG)

        val address = DestinationAddressDetails(
            null, details.tripDestAddress1, details.tripDestAddress2,
            details.tripDestLat, details.tripDestLong,
            details.totalDistance.toString(), details.tripDestAddress1, null, null
        )
        val source = DestinationAddressDetails(
            null,
            details.tripStartAddress1,
            details.tripStartAddress2,
            details.tripStartLat,
            details.tripStartLong,
            details.totalDistance.toString(),
            details.tripStartAddress1,
            null,
            null
        )
        val args = Bundle()
        args.putSerializable(Constants.ROUTE_PLANNING_TRIP_DETAILS, details)
        args.putSerializable(Constants.ROUTE_PLANNING_MODE, RoutePlanningMode.PLANNED_RESUME)
        args.putSerializable(Constants.DESTINATION_ADDRESS_DETAILS, address)
        args.putSerializable(Constants.SOURCE_ADDRESS_DETAILS, source)
        addFragment(RoutePlanningFragment(), args, Constants.TAGS.ROUTE_PLANNING)
    }

    var currentPhotoPath: String? = null
    var capturedImageURI: Uri? = null

    override fun onSaveInstanceState(
        savedInstanceState: Bundle,
        outPersistentState: PersistableBundle
    ) {
        if (currentPhotoPath != null) {
            savedInstanceState.putString(
                CAPTURED_PHOTO_PATH_KEY,
                currentPhotoPath
            )
        }
        if (capturedImageURI != null) {
            savedInstanceState.putString(CAPTURED_PHOTO_URI_KEY, capturedImageURI.toString())
        }
        super.onSaveInstanceState(savedInstanceState, outPersistentState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        if (savedInstanceState.containsKey(CAPTURED_PHOTO_PATH_KEY)) {
            currentPhotoPath = savedInstanceState.getString(CAPTURED_PHOTO_PATH_KEY)
        }
        if (savedInstanceState.containsKey(CAPTURED_PHOTO_URI_KEY)) {
            capturedImageURI = Uri.parse(
                savedInstanceState.getString(
                    CAPTURED_PHOTO_URI_KEY
                )
            )
        }
        super.onRestoreInstanceState(savedInstanceState)
    }

    override fun inflateViewBinding() = ActivityDashboardBinding.inflate(layoutInflater)

    override fun getViewModelReference(): DashboardViewModel {
        return viewModel
    }

    @SuppressLint("MissingPermission")
    override fun onStart() {
        super.onStart()
        BreezeCarUtil.isActivityNotAvaiable = false
        navigationCamera.resetFrame()
        if (getApp()?.isAutoDriveSimulation == true) {
            MapboxNavigationApp.unregisterObserver(ReplayRouteTripSession)
        }
    }

    override fun onStop() {
        BreezeCarUtil.isActivityNotAvaiable = true
        super.onStop()
    }

    override fun onDestroy() {
        locationComponent.updateSettings { enabled = false }
        CoroutineScope(Dispatchers.IO).launch {
            BreezeUserPreference.getInstance(applicationContext).clearNotificationLocalExpired()
        }
        DashboardMapStateManager.resetCurrentLocationFocus()
        compositeDisposable.dispose()
        ETAEngine.getInstance()?.getEtaLocationUpdateInstance(ETAMode.Cruise)?.let {
            LocationBreezeManager.getInstance().removeLocationCallBack(it)
        }
        LocationBreezeManager.getInstance().removeLocationCallBack(locationObserverCallBack)
        DashboardMapStateManager.reset()
        refreshHomeTabTheme()
        super.onDestroy()

        removeOBUConnectionStateObserve()
    }

    override fun onBackPressed() {
        val f: Fragment? =
            this.supportFragmentManager.findFragmentById(viewBinding.frameContainer.id)
        if (f is DeviceBackPressedListener) {
            f.onDeviceBackPressed()
        }
        when {
            (f is ReactFragment || f is CustomReactFragment) -> {
                super.onBackPressed()
            }

            this.supportFragmentManager.backStackEntryCount == 0 -> {
                moveTaskToBack(true)
            }

            else -> {
                super.onBackPressed()
            }
        }
    }

    fun refreshRouteDestination(easyBreezyList: List<EasyBreezieAddress>) {
        mRefreshRouteDestinationListener?.updateRouteDestination(easyBreezyList)
    }

    fun refreshRoutePreferenceInSettings() {
        mSettingsChangeListener?.refreshRoutePreference()
    }

    fun refreshProfile() {
        mProfileUserNameChangeListener?.refreshProfile()
    }

    fun updateTravelLogList(itemId: Long, totalExpense: Double) {
        mTravelLogListener?.updateList(itemId, totalExpense)
    }

    fun setRefreshRouteDestinationListener(listener: RefreshRouteDestinationListener?) {
        mRefreshRouteDestinationListener = listener
    }

    fun setRoutePlanningListener(listener: RefreshRoutePlanningListener?) {
        mRefreshRoutePlanningListener = listener
    }

    fun setSettingsChangeListener(listener: SettingsChangeListener?) {
        mSettingsChangeListener = listener
    }

    fun setProfileUserNameChangeListener(listener: ProfileUserNameChangeListener?) {
        mProfileUserNameChangeListener = listener
    }

    fun setTravelLogListener(listener: TravelLogListener?) {
        mTravelLogListener = listener
    }

    interface RefreshFragmentListener {
        fun updateRefreshFragment(easyBreezyList: List<EasyBreezieAddress>)
    }

    interface RefreshRouteDestinationListener {
        fun updateRouteDestination(easyBreezyList: List<EasyBreezieAddress>)
    }

    interface RefreshRouteNotificationListener {
        fun updateRouteDestinationNotification()
    }

    interface RefreshRoutePlanningListener {
        fun showCarparkNavigation(carparkDetails: BaseAmenity)
        // BREEZE2-1845
//        fun disableETA(isDisableETA: Boolean)
    }

    interface SettingsChangeListener {
        fun refreshRoutePreference()
        fun refreshThemePreference()
    }

    interface ProfileUserNameChangeListener {
        fun refreshProfile()
    }

    interface TravelLogListener {
        fun updateList(itemId: Long, totalExpense: Double)
    }

    fun updateEasyBreezyAcrossScreens() {
        viewModel.easyBreeziesResponse.observe(this, Observer {
            viewModel.easyBreeziesResponse.removeObservers(this)
//            refreshRouteDestination(it.addresses as List<EasyBreezieAddress>)
        })
        viewModel.retriveEasyBreezies()
    }

    private fun showBottomPanel() {
        val navigationParams = getRNFragmentNavigationParams(
            sessionToken = myServiceInterceptor.getSessionToken(),
            fragmentTag = RNScreen.LANDING
        ).apply {
            putBoolean("isShowCarparkListButton", true)
        }
        val initialProperties = bundleOf(
            Constants.RN_TO_SCREEN to RNScreen.LANDING,
            Constants.RN_CONSTANTS.NAVIGATION_PARAMS to navigationParams
        )
        val bottomPanelFragment = CustomReactFragment.newInstance(
            Constants.RN_CONSTANTS.COMPONENT_NAME,
            initialProperties
        )
        supportFragmentManager
            .beginTransaction()
            .replace(
                getFragmentContainer(),
                bottomPanelFragment,
                RNScreen.LANDING
            )
            .commitNowAllowingStateLoss()
    }

    /**
     * open collection list
     */
    private fun openCollectionList() {
        /**
         * check some condition current fragment pop all screen to open collection screen
         */
        val currentFragment = supportFragmentManager.findFragmentById(R.id.frame_container)
        if (currentFragment != null && (currentFragment is ExploreMapsFragment
                    || currentFragment is RoutePlanningFragment
                    || currentFragment is TravelLogFragment
                    || currentFragment is SettingsFragment)
        ) {
            popAllFragmentsInBackStack()
        }
        getShareBusinessLogicPackage()?.getRequestsModule()?.flatMap {
            it.callRN(
                ShareBusinessLogicEvent.OPEN_SAVED_FROM_NATIVE.eventName,
                Arguments.createMap()
            )
        }?.subscribeOn(Schedulers.io())?.observeOn(AndroidSchedulers.mainThread())?.subscribe()
    }


    suspend fun searchERPRoutes(
        source: HashMap<String, Any>,
        destination: HashMap<String, Any>,
        searchTimestamp: Long
    ): ERPFullRouteData? {

        if (source["lat"] == null || source["long"] == null) {
            return null
        }
        return withContext(Dispatchers.IO) {
            val key = source.hashCode().toString() + "_" + destination.hashCode()
                .toString() + "_" + searchTimestamp
            if (viewModel.erpRouteMap.getIfPresent(key) != null) {
                return@withContext viewModel.erpRouteMap.getIfPresent(key)!!.erpFullRouteData
            }
            val sourceAddress =
                DestinationAddressDetails.parseSearchLocationDataFromRN(source).apply {
                    ranking = -1
                }
            val destinationAddress =
                DestinationAddressDetails.parseSearchLocationDataFromRN(destination).apply {
                    ranking = -1
                }
            InitObjectUtilsController.mRoutePlanningHelper?.let { routePlaningHelper ->
                val routeList = routePlaningHelper.findERPRoutes(
                    Point.fromLngLat(
                        sourceAddress.long!!.toDouble(),
                        sourceAddress.lat!!.toDouble()
                    ),
                    Point.fromLngLat(
                        destinationAddress.long!!.toDouble(),
                        destinationAddress.lat!!.toDouble()
                    ),
                    listOf(), RouteDepartureDetails(searchTimestamp, DepartureType.DEPARTURE)
                )
                val rRPRouteData: List<ERPRouteData> = routeList.mapIndexed { index, it ->
                    return@mapIndexed ERPRouteData(index, it.erpList ?: listOf())
                }

                val eRPFullRouteData =
                    ERPFullRouteData(
                        key,
                        rRPRouteData,
                        searchTimestamp,
                        sourceAddress,
                        destinationAddress
                    )
                viewModel.cacheRouteData(key, eRPFullRouteData, routeList)
                return@withContext eRPFullRouteData
            }
            return@withContext null
        }
    }

    fun showParkingInfoRN(parkingId: String) {
        val fragmentTag = Constants.CARPARK_MOREINFO.TO_PARKING_MOREINFO_SCREEN
        val navigationParams = getRNFragmentNavigationParams(
            sessionToken = myServiceInterceptor.getSessionToken(),
            fragmentTag = fragmentTag
        )
        navigationParams.putString(Constants.RN_CONSTANTS.BASE_URL, BuildConfig.SERVER_HEADER)
        navigationParams.putString(Constants.CARPARK_MOREINFO.PARKING_ID, parkingId)
        navigationParams.putString(
            Constants.CARPARK_MOREINFO.AWS_ID_TOKEN,
            myServiceInterceptor.getSessionToken()!!
        )

        val initialProperties = bundleOf(
            Constants.RN_TO_SCREEN to fragmentTag,
            Constants.RN_CONSTANTS.NAVIGATION_PARAMS to navigationParams
        )
        val bottomPanelFragment = CustomReactFragment.newInstance(
            componentName = Constants.RN_CONSTANTS.COMPONENT_NAME,
            launchOptions = initialProperties,
            cleanOnDestroyed = false
        )
        addFragment(
            bottomPanelFragment,
            null,
            fragmentTag
        )
    }

    private fun showCarParkVoucherRN(voucherId: String) {
        Timber.d("-- FCM: showCarParkVoucherRN [$voucherId] --")
        getShareBusinessLogicPackage()?.getRequestsModule()?.flatMap {
            it.callRN(
                ShareBusinessLogicEvent.OPEN_MYVOUCHER_FROM_NOTFICATION.eventName,
                Arguments.fromBundle(bundleOf("voucherId" to voucherId))
            )
        }?.subscribeOn(Schedulers.io())?.observeOn(AndroidSchedulers.mainThread())?.subscribe()
    }

    fun showPoiAmenityInfoRN(id: String) {
        val navigationParams = getRNFragmentNavigationParams(
            sessionToken = myServiceInterceptor.getSessionToken(),
            fragmentTag = Constants.POI_AMENITY_MOREINFO.TO_POI_MOREINFO_SCREEN
        )
        navigationParams.putString(Constants.RN_CONSTANTS.BASE_URL, BuildConfig.SERVER_HEADER)
        navigationParams.putString(Constants.POI_AMENITY_MOREINFO.POI_ID, id)
        navigationParams.putString(
            Constants.POI_AMENITY_MOREINFO.AWS_ID_TOKEN,
            myServiceInterceptor.getSessionToken()!!
        )

        val initialProperties = bundleOf(
            Constants.RN_TO_SCREEN to Constants.POI_AMENITY_MOREINFO.TO_POI_MOREINFO_SCREEN,
            Constants.RN_CONSTANTS.NAVIGATION_PARAMS to navigationParams
        )
        val bottomPanelFragment = CustomReactFragment.newInstance(
            componentName = Constants.RN_CONSTANTS.COMPONENT_NAME,
            launchOptions = initialProperties,
            cleanOnDestroyed = false
        )
        addFragment(
            bottomPanelFragment,
            null,
            Constants.POI_AMENITY_MOREINFO.TO_POI_MOREINFO_SCREEN
        )
    }

    fun onCameraTrackingDismissed() {
        isCameraTrackingDismissed = true
        viewBinding.btnRecenterMap.isVisible = true
        //viewBinding.mapView.gestures.removeOnMoveListener(onMoveListener)
        viewBinding.mapView.location.removeOnIndicatorPositionChangedListener(
            onIndicatorPositionChangedListener
        )
    }

    private fun getGesturesPlugin(): GesturesPlugin {
        return viewBinding.mapView.gestures
    }


    fun popAllFragmentsInBackStack() {
        if (!lifecycle.currentState.isAtLeast(Lifecycle.State.STARTED)) return
        val fm = this@DashboardActivity.supportFragmentManager
        val count = fm.backStackEntryCount
        for (i in 0 until count) {
            fm.popBackStackImmediate()
        }
    }

    private fun popFragmentsInBackStackExceptExploreMap() {
        val fm = this@DashboardActivity.supportFragmentManager
        val count = fm.backStackEntryCount
        for (i in count - 1 downTo 0) {
            val nameBackStack = fm.getBackStackEntryAt(i).name
            if (nameBackStack.equals(Constants.TAGS.EXPLORE_MAP)) {
                return
            } else {
                fm.popBackStackImmediate()
            }
        }
    }


    override fun getFragmentContainer(): Int {
        return R.id.frame_container
    }

    fun getScreenName() =
        if (MapboxNavigationApp.current()
                ?.getTripSessionState() == TripSessionState.STARTED
        ) Screen.HOME_CRUISE_MODE else
            Screen.HOME

    private fun isNewUser(): Boolean {
        val thresholdDays = BreezeUserPreference.getInstance(this)
            .getNewUserThresholdDaysSettings(BreezeCarUtil.masterlists)
        breezeUserPreferenceUtil.retrieveUserCreatedDate()?.let {
            val dateCreated = Date(it)
            val c1 = Calendar.getInstance()
            c1.time = dateCreated
            c1.add(Calendar.DATE, thresholdDays)
            val thresholdDate = c1.time
            val currentDate = Date()
            val c = Calendar.getInstance()
            c.time = currentDate
            if (currentDate.after(dateCreated) && currentDate.before(thresholdDate)) {
                return true
            }
        }
        return false
    }

    private fun checkForPNudgeDisplay() {
        if (
            !isShowDialogUpdateEmail &&
            !GlobalNotification.isShowing &&
            !BreezeUserPreference.getInstance(this).isInAppGuideTutorialPending() &&
            mIsUserOnHome
        ) {
//            callEventToCheckVoucherCarAnimation() //BREEZE2-898
        }
    }

    private fun showEmailUpdateDialog() {
        dialog = Dialog(this)
        dialog.setContentView(R.layout.dialog_email_update)

        if (dialog.window != null) {
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
        dialog.setCanceledOnTouchOutside(false)
        //dialog.setCancelable(true)

        val okButton = dialog.findViewById<BreezeButton>(R.id.button_okay)
        okButton.setOnClickListener {
            Analytics.logOpenPopupEvent(Event.POPUP_ADD_EMAIL_NOW, getScreenName())
            openEmailUpdateScreen()
            dialog.dismiss()
            isShowDialogUpdateEmail = false
        }

        dialog.setOnDismissListener {
            isShowDialogUpdateEmail = false
        }
        val dismissButton = dialog.findViewById<TextView>(R.id.button_dismiss)
        dismissButton.setOnClickListener {
            Analytics.logOpenPopupEvent(Event.POPUP_ADD_EMAIL_LATER, getScreenName())
            dialog.dismiss()
            isShowDialogUpdateEmail = false
        }

        dialog.show()

        isShowDialogUpdateEmail = true

        val displayRectangle = Rect()
        val window: Window = window
        window.decorView.getWindowVisibleDisplayFrame(displayRectangle)

        if (dialog.window != null) {
            val lp = WindowManager.LayoutParams()
            lp.copyFrom(dialog.window!!.attributes)
            lp.width = (displayRectangle.width() * 0.9f).toInt()
            dialog.window!!.attributes = lp
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        when (requestCode) {
            Constants.REQUEST_CODES.CONTACTS_REQUEST_CODE_FROM_RN -> {
                if (resultCode == Activity.RESULT_OK && data != null) {
                    handleContacts(data)
                } else if (resultCode == Activity.RESULT_CANCELED) {
                    Analytics.logClickEvent(Event.CANCEL_ADD_CONTACT, getScreenName())
                }
            }
        }
        data?.let {
            if (it.getBooleanExtra(Constants.NAVIGATION_END, false)) {
                DashboardMapStateManager.isInSearchLocationMode = false
                navigateToHomeTabAndReset()
            }
        }

        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun handleContacts(data: Intent) {

        val contactDetails = Utils.extractPhoneNumberFromContactIntent(data, this)

        if (contactDetails != null) {
            val params: WritableMap = Arguments.createMap()
            params.putString(Constants.TRIPETA.CONTACT_NAME, contactDetails.contactName)
            params.putString(
                Constants.TRIPETA.CONTACT_NUMBER,
                contactDetails.phoneNumberList[0]
            )
            ReactNativeEventEmitter.sendEvent(application = application, "SelectEvent", params)
        } else {
            showAlert(this)
        }
    }

    private fun showAlert(context: Context) {
        val builder: AlertDialog.Builder = context.let {
            AlertDialog.Builder(it)
        }

        builder.setMessage(R.string.incorrect_phone_number)
            .setPositiveButton(
                R.string.alert_close
            ) { dialog, _ ->
                dialog.dismiss()
            }

        val alertDialog: AlertDialog = builder.create()

        alertDialog.setOnShowListener {
            alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).isAllCaps = false
            alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).isAllCaps = false
        }

        alertDialog.show()
    }

    fun openRNFavouritesScreen(screenName: String) {
        val navigationParams = getRNFragmentNavigationParams(
            sessionToken = myServiceInterceptor.getSessionToken(),
            fragmentTag = screenName
        )
        navigationParams.putString(
            Constants.RN_CONSTANTS.OPEN_AS,
            Constants.RN_CONSTANTS.OPEN_AS_FRAGMENT
        )
        navigationParams.putString(Constants.RN_CONSTANTS.FRAGMENT_ID, screenName)
        navigationParams.putString(Constants.RN_CONSTANTS.BASE_URL, BuildConfig.SERVER_HEADER)
        navigationParams.putString(
            Constants.RN_CONSTANTS.ID_TOKEN,
            myServiceInterceptor.getSessionToken()
        )
        navigationParams.putString(Constants.RN_CONSTANTS.DEVICE_OS, "Android")
        navigationParams.putString(
            Constants.RN_CONSTANTS.DEVICE_OS_VERSION,
            Utils.getDeviceOS()
        )
        navigationParams.putString(Constants.RN_CONSTANTS.DEVICE_MODEL, Utils.getDeviceModel())
        navigationParams.putString(
            Constants.RN_CONSTANTS.APP_VERSION,
            Utils.getAppVersion(this)
        )

        val initialProperties = Bundle()
        initialProperties.putString(Constants.RN_TO_SCREEN, screenName)
        initialProperties.putBundle(Constants.RN_CONSTANTS.NAVIGATION_PARAMS, navigationParams)

        val reactNativeFragment: Fragment = ReactFragment.Builder()
            .setComponentName(Constants.RN_CONSTANTS.COMPONENT_NAME)
            .setLaunchOptions(initialProperties)
            .build()

        this.supportFragmentManager
            .beginTransaction()
            .add(getFragmentContainer(), reactNativeFragment, screenName)
            .addToBackStack(Constants.TAGS.FAVOURITES_ETA_TAG)
            .commit()
        this.supportFragmentManager.executePendingTransactions()
    }

    fun getMapViewInstance(): MapView? {
        return viewBinding.mapView
    }

    fun getSnapshotImage(): String? {
        return snapshotImageBase64
    }

    fun setSnapshotImage(pSnapShotImage: String?) {
        snapshotImageBase64 = pSnapShotImage
    }

//    private var onSnapShotReady = MapView.OnSnapshotReady { bitmap ->
//        runOnUiThread {
//            if (bitmap != null) {
//                snapshotImageBase64 = ViewUtils.encodeView(
//                    bitmap,
//                    BitmapEncodeOptions.Builder()
//                        .width(400).compressQuality(40).build()
//                )
//                Timber.d("Feedback Dashboard - Screenshot has been captured")
//
//            } else {
//                snapshotImageBase64 = null
//            }
//
//            Analytics.logClickEvent(Event.REPORT_ISSUE, getScreenName())
//            newFeedbackFragment()
//        }
//    }

    private fun startNavigation(
        originalDestinationAddressDetails: DestinationAddressDetails,
        destinationAddressDetails: DestinationAddressDetails,
        route: DirectionsRoute,
        compressedOriginalDirections: ByteArray,
        etaRequest: ETARequest?,
        walkingRoute: NavigationRoute?,
        selectedCarParkIconRes: Int = -1,
        notificationCriterias: ArrayList<ValueConditionShowBroadcast>? = null
    ) {
        val routeIndex = route.routeIndex()?.toInt() ?: return
        CarNavigationScreenState.canStartNavigation = true
        val intent = Intent(this@DashboardActivity, TurnByTurnNavigationActivity::class.java)
        val args = bundleOf(
            Constants.NAVIGATION_BUNDLE_ORIGINAL_DESTINATION_DETAILS to originalDestinationAddressDetails,
            Constants.NAVIGATION_BUNDLE_DESTINATION_DETAILS to destinationAddressDetails,
            Constants.NAVIGATION_BUNDLE_ROUTE_RESPONSE to compressedOriginalDirections,
            Constants.NAVIGATION_BUNDLE_ROUTE_OPTIONS to route.routeOptions(),
            Constants.NAVIGATION_BUNDLE_SELECTED_CARPARK_ICON_RES to selectedCarParkIconRes,
            Constants.NAVIGATION_BUNDLE_SELECTED_ROUTE_INDEX to routeIndex,
        )
        notificationCriterias?.let {
            args.putParcelableArrayList(Constants.LIST_CRITERIAS, it)
        }
        if (walkingRoute != null) {
            args.putSerializable(
                Constants.NAVIGATION_BUNDLE_WALKING_ROUTE_DIRECTION,
                walkingRoute.directionsResponse
            )
            args.putSerializable(
                Constants.NAVIGATION_BUNDLE_WALKING_ROUTE_ROUTE_OPTIONS,
                walkingRoute.routeOptions
            )
        }
        if (etaRequest != null) {
            args.putSerializable(Constants.NAVIGATION_ETA_DATA, etaRequest)
        }
        intent.putExtra(Constants.NAVIGATION_BUNDLE, args)
        navigationActivityResultLauncher.launch(intent)
    }

    private fun startPlaceSaved() {
        addFragment(
            PlaceSaveDetailFragment(),
            null,
            ""
        )
    }

    override fun onEventFromCar(event: ToAppRxEvent) {
        super.onEventFromCar(event)
        when (event) {
//            is AppToPhoneTurnOnCruiseModeEvent -> {
//                startCruiseMode()
//                postEventWithCacheLast(
//                    AppToHomeTurnOnCruiseModeEvent(
//                        ETAEngine.getInstance()!!.isETAEnabled(),
//                        ETAEngine.getInstance()!!.isLiveLocationSharingEnabled(),
//                        true
//                    )
//                )
//            }

//            is AppToPhoneTurnOnOBULiteEvent -> {
//                openOBULiteDisplay()
//            }

            is AppToPhoneNavigationStartEvent -> {
                Utils.isNavigationStarted = true
                Utils.phoneNavigationStartEventData = event.data

                ETAEngine.updateETPRequest(event.data.etaRequest)
                startNavigation(
                    originalDestinationAddressDetails = event.data.originalDestinationAddressDetails,
                    destinationAddressDetails = event.data.destinationAddressDetails,
                    route = event.data.route,
                    compressedOriginalDirections = event.data.compressedRouteOriginalResp,
                    etaRequest = event.data.etaRequest,
                    walkingRoute = event.data.walkingRoute,
                    selectedCarParkIconRes = event.data.selectedCarParkIconRes,
                    notificationCriterias = event.data.notificationCriterias,
                )
            }

            is AppToPhoneNavigationEndEvent -> {
                Utils.isNavigationStarted = false
                Utils.phoneNavigationStartEventData = null
            }

            is TriggerPhoneNotificaion -> {
                //showCarNotification()
            }

            is AppToPhoneCarParkEvent -> {
                // if (!viewBinding.carParkViewOption.isCarParksShown()) {
                if (!viewBinding.amenitiesOnMap.carParkViewOption.isShowingCarParks()) {
                    toggleCarparks(EventSource.CAR_APP)
                }
            }

            is AppToPhoneCarParkEndEvent -> {
                // if (viewBinding.carParkViewOption.isCarParksShown()) {
                if (viewBinding.amenitiesOnMap.carParkViewOption.isShowingCarParks()) {
                    toggleCarparks(EventSource.CAR_APP)
                }
            }

            is ETAStartedPhone -> {
                handleETAStartedEvent()
            }

            is UpdateETAPhone -> {
                handleETALiveLocationToggleEvent()
            }

            is AppToPhoneRoutePlanningStartEvent -> {
                startRoutePlanning(event.carPark)
            }

            else -> {}
        }
    }

    /**
     * start route plaining
     */
    fun startRoutePlanningFromParkingCalculator(
        carParkId: String?,
        lat: String?,
        long: String?,
        name: String?
    ) {
        CoroutineScope(Dispatchers.IO).launch {
            getCurrentLocation()?.let { location ->
                val destinationDetail = DestinationAddressDetails(
                    lat = lat,
                    long = long,
                    destinationName = name,
                    address1 = name,
                    isCarPark = true,
                    carParkID = carParkId
                )
                val sourceAddressDetails = DestinationAddressDetails(
                    null,
                    Utils.getAddressFromLocation(location, this@DashboardActivity),
                    null,
                    location.latitude.toString(),
                    location.longitude.toString(),
                    null,
                    Constants.TAGS.CURRENT_LOCATION_TAG,
                    null,
                    -1
                )
                withContext(Dispatchers.Main) {
                    val args = Bundle()
                    args.putSerializable(Constants.SOURCE_ADDRESS_DETAILS, sourceAddressDetails)
                    args.putSerializable(
                        Constants.DESTINATION_ADDRESS_DETAILS,
                        destinationDetail
                    )
                    showRouteAndResetToCenter(args)
                }
            }
        }
    }

    private fun startRoutePlanning(carPark: BaseAmenity) {
        CoroutineScope(Dispatchers.IO).launch {
            var destinationDetail: DestinationAddressDetails? = null
            val carparkLocation = Location("")
            carparkLocation.latitude = carPark.lat!!
            carparkLocation.longitude = carPark.long!!
            val address = Utils.getAddressObject(carparkLocation, this@DashboardActivity)
            if (address != null) {
                destinationDetail = DestinationAddressDetails(
                    null,
                    carPark.name,
                    address.getAddressLine(0),
                    carPark.lat.toString(),
                    carPark.long.toString(),
                    null,
                    carPark.name,
                    null,
                    -1,
                    true
                )
            } else {
                destinationDetail = DestinationAddressDetails(
                    null,
                    carPark.name,
                    null,
                    carPark.lat.toString(),
                    carPark.long.toString(),
                    null,
                    carPark.name,
                    null,
                    -1,
                    true
                )
            }

            val sourceAddressDetails = DestinationAddressDetails(
                null,
                Utils.getAddressFromLocation(getCurrentLocation()!!, this@DashboardActivity),
                null,
                getCurrentLocation()!!.latitude.toString(),
                getCurrentLocation()!!.longitude.toString(),
                null,
                Constants.TAGS.CURRENT_LOCATION_TAG,
                null,
                -1
            )

            runOnUiThread {
                val args = Bundle()
                args.putSerializable(Constants.DESTINATION_ADDRESS_DETAILS, destinationDetail)
                args.putSerializable(Constants.SOURCE_ADDRESS_DETAILS, sourceAddressDetails)
                showRouteAndResetToCenter(args)
            }
        }

    }

    fun showRouteAndResetToCenter(args: Bundle) {
        runOnUiThread {
            addFragment(
                RoutePlanningFragment(),
                args,
                Constants.TAGS.ROUTE_PLANNING
            )
            Handler(Looper.getMainLooper()).post {
                this@DashboardActivity.resetAmenitiesAndRecenter()
            }
        }

    }


    var isSearchingForWalkingRoute = false

    fun showWalkingRouteAndResetToCenter(args11: Bundle, isTampinesMode: Boolean = false) {
        synchronized(this) {
            if (isSearchingForWalkingRoute) {
                return
            }
            isSearchingForWalkingRoute = true

            var originDetails =
                args11.getSerializable(Constants.SOURCE_ADDRESS_DETAILS) as DestinationAddressDetails
            val originPoint = Point.fromLngLat(
                originDetails.long!!.toDouble(),
                originDetails.lat!!.toDouble()
            )

            var destinationDetails =
                args11.getSerializable(Constants.DESTINATION_ADDRESS_DETAILS) as DestinationAddressDetails
            val destinationPoint = Point.fromLngLat(
                destinationDetails.long!!.toDouble(),
                destinationDetails.lat!!.toDouble()
            )
            walkingRouteObserver.setDestinationData(destinationDetails, isTampinesMode)
            viewModel.walkingRoutesRetrieved.observe(this, walkingRouteObserver)

            /**
             * find route walking route then start to navigation screen
             */
            viewModel.findWalkingRoutes(
                originPoint,
                destinationPoint,
                destinationDetails
            )
        }
    }

    fun callAfterInitialized(callback: () -> Unit) {
        runOnUiThread {
            viewBinding.mapView.getMapboxMap().getStyle {
                callback.invoke()
            }
        }
    }

    override fun onUserInteraction() {
        super.onUserInteraction()
        lastUserInteraction = Date().time
    }

    private fun getDifferenceInSeconds(): Long {
        val diffInSec: Long
        val diffInMs: Long = Date().time - lastUserInteraction!!
        diffInSec = TimeUnit.MILLISECONDS.toSeconds(diffInMs)
        Timber.i("diffInSec: " + diffInSec)
        return diffInSec
    }

    fun updateUserPreferences(data: ReadableMap) {
        data.let {
            val dataSettings = it.getArray("settings").toString()
            val gson = Gson()
            if (!dataSettings.isNullOrEmpty()) {
                val myType = object : TypeToken<ArrayList<SettingsItem>>() {}.type
                val listSettings = gson.fromJson<ArrayList<SettingsItem>>(dataSettings, myType)
                BreezeCarUtil.settingsResponse?.settings?.apply {
                    clear()
                    addAll(listSettings)
                }
                var updateCarParks: Boolean = false
                for (item in listSettings) {
                    //update theme preference if changed
                    var currentThemePreference = userPreferenceUtil.getUserThemePreference()
                    if (TextUtils.equals(
                            item.attribute,
                            Constants.SETTINGS.ATTR_MAP_DISPLAY
                        ) && (currentThemePreference != item.attribute_code)
                    ) {
                        val themePreference = item.attribute_code
                        var userTheme: UserTheme = UserTheme.LIGHT
                        DashboardMapStateManager.reset()
                        refreshHomeTabTheme()
                        when (themePreference) {
                            UserTheme.LIGHT.type -> {
                                userTheme = UserTheme.LIGHT
                                Analytics.logClickEvent(Event.MAP_LIGHT_CLICK, getScreenName())
                                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
                            }

                            UserTheme.DARK.type -> {
                                userTheme = UserTheme.DARK
                                Analytics.logClickEvent(Event.MAP_DARK_CLICK, getScreenName())
                                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
                            }

                            UserTheme.SYSTEM_DEFAULT.type -> {
                                userTheme = UserTheme.SYSTEM_DEFAULT
                                Analytics.logClickEvent(Event.MAP_AUTO_CLICK, getScreenName())
                                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM)
                            }
                        }
                        userPreferenceUtil.saveUserThemePreference(userTheme)
                        ToCarRx.postEvent(SwitchThemeEvent)
//                        (application as? App)?.run{
//                            mReactNativeHost?.clear()
//                            mReactNativeHost = null
//                        }
                    }
                    if (TextUtils.equals(item.attribute, Constants.SETTINGS.CARPARK_COUNT)) {
                        BreezeUserPreference.getInstance(this)
                            .saveCarParkCount(item.attribute_code)
                        updateCarParks = true
                    }
                    if (TextUtils.equals(item.attribute, Constants.SETTINGS.CARPARK_DISTANCE)) {
                        BreezeUserPreference.getInstance(this)
                            .saveCarParkDistance(item.attribute_code)
                        updateCarParks = true
                    }
                    if (TextUtils.equals(
                            item.attribute,
                            Constants.SHARED_PREF_KEY.KEY_PARKING_AVAILABILITY_AUDIO
                        )
                    ) {
                        BreezeUserPreference.getInstance(this)
                            .saveParkingAvailabilityAudio(item.attribute_code)
                    }
                    if (TextUtils.equals(
                            item.attribute,
                            Constants.SHARED_PREF_KEY.KEY_PARKING_AVAILABILITY_DISPLAY
                        )
                    ) {
                        BreezeUserPreference.getInstance(this)
                            .saveParkingAvailabilityDisplay(item.attribute_code)
                    }
                    if (TextUtils.equals(
                            item.attribute,
                            Constants.SHARED_PREF_KEY.KEY_ESTIMATED_TRAVEL_TIME_AUDIO
                        )
                    ) {
                        BreezeUserPreference.getInstance(this)
                            .saveEstimatedTravelTimeAudio(item.attribute_code)
                    }
                    if (TextUtils.equals(
                            item.attribute,
                            Constants.SHARED_PREF_KEY.KEY_ESTIMATED_TRAVEL_TIME_DISPLAY
                        )
                    ) {
                        BreezeUserPreference.getInstance(this)
                            .saveEstimatedTravelTimeDisplay(item.attribute_code)
                    }

                    if (TextUtils.equals(
                            item.attribute,
                            Constants.SHARED_PREF_KEY.KEY_SCHOOL_ZONE_DISPLAY
                        )
                    ) {
                        BreezeUserPreference.getInstance(this)
                            .saveSchoolZoneDisplay(item.attribute_code)
                    }
                    if (TextUtils.equals(
                            item.attribute,
                            Constants.SHARED_PREF_KEY.KEY_SCHOOL_ZONE_AUDIO
                        )
                    ) {
                        BreezeUserPreference.getInstance(this)
                            .saveSchoolZoneAudio(item.attribute_code)
                    }

                    if (TextUtils.equals(
                            item.attribute,
                            Constants.SHARED_PREF_KEY.KEY_SILVER_ZONE_DISPLAY
                        )
                    ) {
                        BreezeUserPreference.getInstance(this)
                            .saveSilverZoneDisplay(item.attribute_code)
                    }
                    if (TextUtils.equals(
                            item.attribute,
                            Constants.SHARED_PREF_KEY.KEY_SILVER_ZONE_AUDIO
                        )
                    ) {
                        BreezeUserPreference.getInstance(this)
                            .saveSilverZoneAudio(item.attribute_code)
                    }

                    if (TextUtils.equals(
                            item.attribute,
                            Constants.SHARED_PREF_KEY.KEY_BUS_LANE_DISPLAY
                        )
                    ) {
                        BreezeUserPreference.getInstance(this)
                            .saveBusLaneDisplay(item.attribute_code)
                    }
                    if (TextUtils.equals(
                            item.attribute,
                            Constants.SHARED_PREF_KEY.KEY_BUS_LANE_AUDIO
                        )
                    ) {
                        BreezeUserPreference.getInstance(this)
                            .saveBusLaneAudio(item.attribute_code)
                    }

                }
                if (updateCarParks) {
                    toggleCarparks(EventSource.NONE)
                }
            }
        }
    }

    fun saveCustomAnalytics(eventName: String, eventValue: String, screenName: String) {
        viewModel.saveCustomAnalytics(eventName, eventValue, screenName)
    }

    fun getUserSetting() {
        viewModel.getAllSettingZipData()
    }

    fun sendEventHideOrShowButtonCarparkList(isShow: Boolean = false) {
        getShareBusinessLogicPackage()?.getRequestsModule()?.flatMap {
            val rnData = Arguments.createMap().apply {
                putBoolean("is_show", isShow)
            }

            it.callRN(
                ShareBusinessLogicEvent.SHOW_OR_HIDE_BUTTON_CARPARK_LIST.eventName,
                rnData
            )
        }?.subscribeOn(Schedulers.io())?.observeOn(AndroidSchedulers.mainThread())?.subscribe()
    }

    private fun dismissDialogSharing() {
        getShareBusinessLogicPackage()?.getRequestsModule()?.flatMap {
            it.callRN(ShareBusinessLogicEvent.DISMISS_SHARING_DIALOG.eventName, null)
        }?.subscribeOn(Schedulers.io())?.observeOn(AndroidSchedulers.mainThread())?.subscribe()
    }

    fun toggleCarparkList(selected: Boolean) {
        isShowingCarparkList = selected
        if (selected) {
            dismissGlobalNotification()
            animationMapViewFlip()
            Analytics.logClickEvent(Event.DISPLAY_CAR_PARK_LIST, getScreenName())
        } else {
            animationMapTurnBack()
            Analytics.logClickEvent(Event.DISPLAY_CAR_PARK_MAP, getScreenName())
        }
    }

    private fun animationMapTurnBack() {
        Handler(mainLooper).postDelayed({
            val oa1 = ObjectAnimator.ofFloat(viewBinding.mainLayout, "scaleX", 0f, 1f)
            oa1.interpolator = LinearInterpolator()
            oa1.start()

            val oa22 = ObjectAnimator.ofFloat(viewBinding.mapView, "scaleX", 0f, 1f)
            oa22.interpolator = LinearInterpolator()
            oa22.start()
        }, 100)
    }

    private fun animationMapViewFlip() {
        val animatorMainlayout =
            ObjectAnimator.ofFloat(viewBinding.mainLayout, "scaleX", 1f, 0f)
        val animatorMapView = ObjectAnimator.ofFloat(viewBinding.mapView, "scaleX", 1f, 0f)
        val animatorSet = AnimatorSet()
        animatorSet.playTogether(animatorMainlayout, animatorMapView)
        animatorSet.interpolator = LinearInterpolator()
        animatorSet.duration = 100
        animatorSet.doOnEnd {
            val oa2 = ObjectAnimator.ofFloat(viewBinding.mapView, "scaleX", 0f, 1f)
            oa2.interpolator = AccelerateDecelerateInterpolator()
            oa2.start()
            val oa22 = ObjectAnimator.ofFloat(viewBinding.mainLayout, "scaleX", 0f, 1f)
            oa22.interpolator = AccelerateDecelerateInterpolator()
            oa22.start()
        }
        animatorSet.start()
    }

    fun determineZoomRadius() =
        // dashBoardMapCamera.determineZoomRadius(viewBinding.carParkViewOption.isCarParksShown())
        dashBoardMapCamera.determineZoomRadius(viewBinding.amenitiesOnMap.carParkViewOption.isShowingCarParks())

    fun enableTrackingPan(isEnableTrackingPan: Boolean = true) {
        panTrackingManager?.togglePanTracking(isEnableTrackingPan)
    }

    fun getFocusedLocationForParkingCalculator(): Location? {
        return if (this.panTrackingManager != null && this.panTrackingManager?.isInPanMode == true) {
            this.panTrackingManager?.currentLocationPan
        } else if (DashboardMapStateManager.isInSearchLocationMode) {
            DashboardMapStateManager.searchLocationAddress?.getRoutableLocation()
        } else {
            LocationBreezeManager.getInstance().currentLocation
        }
    }

    fun getFocusedLocationForTrafficCamera(): Location? {
        return if (DashboardMapStateManager.isInSearchLocationMode) {
            DashboardMapStateManager.searchLocationAddress?.getRoutableLocation()
        } else {
            LocationBreezeManager.getInstance().currentLocation
        }
    }

    fun openConnectOBUDeviceScreen(step: OBUInitialConnectionStep = OBUInitialConnectionStep.INTRO) {
        (supportFragmentManager.findFragmentByTag(OBUConnectFragment.TAG) as? OBUConnectFragment)?.takeIf {
            it.isResumed
        }?.let {
            it.setStep(step)
            return
        }

        addFragment(
            OBUConnectFragment.newInstance(step),
            null,
            OBUConnectFragment.TAG
        )
    }

    fun openOBUPairedVehiclesScreen() {
        addFragment(OBUPairedListFragment(), null, AndroidScreen.CONNECT_OBU_DEVICE.nameStr)
    }

    fun openOBULiteDisplay() {
        Analytics.logEvent(Event.USER_CLICK, Event.OBU_CRUISE_START, Screen.OBU_DISPLAY_MODE)
        addFragment(OBULiteFragment(), null, OBULiteFragment.TAG)
    }

    private inner class WalkingRouteObserver : Observer<NavigationRoute?> {
        private var destinationDetails: DestinationAddressDetails? = null
        private var isTampinesMode: Boolean = false;

        fun setDestinationData(
            destinationDetails: DestinationAddressDetails,
            isTampinesMode: Boolean
        ) {
            this.destinationDetails = destinationDetails
            this.isTampinesMode = isTampinesMode
        }

        override fun onChanged(walkingRoute: NavigationRoute?) {
            if (walkingRoute != null) {
                CoroutineScope(Dispatchers.IO).launch {
                    val compressedRoute = walkingRoute.directionsResponse.compressToGzip()
                    withContext(Dispatchers.Main) {
                        CarNavigationScreenState.canStartNavigation = true
                        val intent = Intent(
                            this@DashboardActivity,
                            if (isTampinesMode) WalkingActivity::class.java else TurnByTurnNavigationActivity::class.java
                        )
                        val navigationData = bundleOf(
                            Constants.NAVIGATION_BUNDLE_DESTINATION_DETAILS to
                                    destinationDetails as Serializable,
                            Constants.NAVIGATION_BUNDLE_ORIGINAL_DESTINATION_DETAILS to
                                    destinationDetails as Serializable,
                            Constants.NAVIGATION_BUNDLE_ROUTE_RESPONSE to
                                    compressedRoute,
                            Constants.NAVIGATION_BUNDLE_ROUTE_OPTIONS to
                                    walkingRoute.routeOptions as Serializable,
                            Constants.NAVIGATION_BUNDLE_WALKING_ROUTE_DIRECTION to
                                    walkingRoute.directionsResponse as Serializable,
                            Constants.NAVIGATION_BUNDLE_WALKING_ROUTE_ROUTE_OPTIONS to
                                    walkingRoute.routeOptions as Serializable,
                            Constants.NAVIGATION_BUNDLE_SELECTED_ROUTE_INDEX to 0,
                            Constants.NAVIGATION_BUNDLE_SKIP_DRIVING to true,
                        )
                        intent.putExtra(Constants.NAVIGATION_BUNDLE, navigationData)
                        navigationActivityResultLauncher.launch(intent)
                        isSearchingForWalkingRoute = false
                    }
                }
            } else {
                isSearchingForWalkingRoute = false
            }
        }
    }

    fun showToast(content: String, @DrawableRes icon: Int? = null) {
        viewBinding.layoutCustomToast.imgEtaToast.setImageResource(
            icon ?: R.drawable.tick_successful
        )
        viewBinding.layoutCustomToast.etaToastText.text = content
        viewBinding.customToastContainer.visibility = VISIBLE
        hideCustomToastHandler?.removeCallbacksAndMessages(null)
        hideCustomToastHandler?.postDelayed({
            runOnUiThread {
                if (viewBinding.customToastContainer.isVisible) {
                    viewBinding.customToastContainer.visibility = GONE
                }
            }
        }, 2000)
    }

    private fun observeOBUConnectionStateChanged() {
        getApp()?.run {
            initOBU()
            obuConnectionHelper.obuConnectionState.observeForever(obuConnectionStateObserve)
        }

    }

    private fun removeOBUConnectionStateObserve() {
        getApp()?.obuConnectionHelper?.obuConnectionState?.removeObserver(
            obuConnectionStateObserve
        )
    }

    fun openCollectionListOnCollectionDetailsClosed() {
        if (!lifecycle.currentState.isAtLeast(Lifecycle.State.STARTED)) return
        runOnUiThread {
            supportFragmentManager.popBackStack()
            // put delay for landing to resume before navigating to collection list screen
            Handler(mainLooper).postDelayed({
                openCollectionList()
            }, 200)
        }
    }

    private val obuConnectionStateObserve = Observer<OBUConnectionState> { state ->
        Timber.d("DashboardActivity observeOBUConnectionStateChanged: $state")
        when (state) {
            OBUConnectionState.CONNECTED -> {
                if (getApp()?.appLifeCycleHandler?.isAppActive == true) {
                    (supportFragmentManager.findFragmentByTag(OBUConnectFragment.TAG) as? OBUConnectFragment)?.let {
                        supportFragmentManager.beginTransaction().remove(it).commitNow()
                        getApp()?.shareBusinessLogicHelper?.backToHomepage()
                    }
                    // Application is in foreground state, show a custom toast message
                    showToast(getString(R.string.obu_connected))
                } else {
                    // Application is in background state, show a local notification
                    val intent = Intent(this, DashboardActivity::class.java).apply {
                        flags = Intent.FLAG_ACTIVITY_REORDER_TO_FRONT
                    }

                    PushNotificationUtils.createBreezeNotification(
                        context = this,
                        notificationId = FCMListenerService.NOTIFICATION_ID_OBU_CONNECTED,
                        channelId = FCMListenerService.NOTIFICATION_CHANNEL_ID_OBU_CONNECTED,
                        channelName = FCMListenerService.NOTIFICATION_CHANNEL_NAME_OBU_CONNECTED,
                        title = getString(R.string.obu_successfully_paired_title),
                        description = getString(R.string.obu_successfully_paired_content),
                        intent = intent,
                        importanceLevel = NotificationManager.IMPORTANCE_HIGH
                    )
                }
            }

            else -> {}
        }
    }
}


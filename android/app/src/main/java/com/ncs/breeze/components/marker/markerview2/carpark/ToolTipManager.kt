package com.ncs.breeze.components.marker.markerview2.carpark

import com.breeze.model.api.response.amenities.BaseAmenity
import com.ncs.breeze.ui.base.BaseActivity

interface ToolTipManager {


    fun resetView()

    fun bringtoFont()

    fun sendEventParkingISelected(idCarpark: String)

    fun sendEventOpenCalculateFee(idCarpark: String)

    fun clickToolTip(
        item: BaseAmenity,
        shouldRecenter: Boolean,
        navigationButtonClick: (BaseAmenity, BaseActivity.EventSource) -> Unit,
        moreInfoButtonClick: (BaseAmenity) -> Unit,
        carParkIconClick: (Boolean) -> Unit,
        screenName: String
    )

    fun clickRouteToolTip(
        item: BaseAmenity,
        shouldRecenter: Boolean,
        navigationButtonClick: (BaseAmenity, BaseActivity.EventSource) -> Unit,
        moreInfoButtonClick: (BaseAmenity) -> Unit,
        carParkIconClick: (Boolean) -> Unit,
        calculateFee: (BaseAmenity) -> Unit,
        screenName: String
    )

}
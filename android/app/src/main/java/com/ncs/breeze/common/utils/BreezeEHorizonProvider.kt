package com.ncs.breeze.common.utils

import android.content.Context
import com.ncs.breeze.common.helper.ehorizon.BreezeEHorizonObserver
import com.ncs.breeze.common.helper.ehorizon.BreezeRoadObjectHandler
import com.ncs.breeze.common.helper.ehorizon.BreezeRoadObjectObserver
import com.ncs.breeze.common.helper.ehorizon.EHorizonMode
import com.mapbox.api.directions.v5.models.DirectionsRoute
import com.mapbox.navigation.base.ExperimentalPreviewMapboxNavigationAPI
import com.mapbox.navigation.core.MapboxNavigation
import com.mapbox.navigation.core.lifecycle.MapboxNavigationApp
import com.mapbox.navigation.core.trip.session.TripSessionState
import com.mapbox.navigation.core.trip.session.TripSessionStateObserver

/**
 * Handles Both mapbox ehorizon and custom roadObjectMatcher
 */
object BreezeEHorizonProvider {

    @Volatile
    private var roadObjectHandler: BreezeRoadObjectHandler? = null

    @Volatile
    private var breezeEHorizonObserver: BreezeEHorizonObserver? = null

    @Volatile
    private var breezeRoadObjectObserver: BreezeRoadObjectObserver? = null
    private var mapboxNavigation: MapboxNavigation? = null

    @OptIn(ExperimentalPreviewMapboxNavigationAPI::class)
    fun create(
        context: Context, breezeERPUtil: BreezeERPRefreshUtil,
        breezeIncidentsUtil: BreezeTrafficIncidentsUtil, mode: EHorizonMode, route: DirectionsRoute?
    ): BreezeRoadObjectHandler {
        val currentMapboxNavigation = MapboxNavigationApp.current()
        if (roadObjectHandler == null) {
            roadObjectHandler = BreezeRoadObjectHandler(context, mode)
            breezeEHorizonObserver =
                BreezeEHorizonObserver(roadObjectHandler!!, context, breezeERPUtil)
            breezeRoadObjectObserver = BreezeRoadObjectObserver(
                roadObjectHandler!!,
                breezeIncidentsUtil,
                mode,
                context,
                route
            )
            currentMapboxNavigation?.registerEHorizonObserver(breezeEHorizonObserver!!)
            currentMapboxNavigation?.registerTripSessionStateObserver(tripSessionStateObserver)
            this.mapboxNavigation = currentMapboxNavigation
        } else if (this.mapboxNavigation != currentMapboxNavigation) {
            this.mapboxNavigation?.let {
                it.unregisterEHorizonObserver(breezeEHorizonObserver!!)
                it.unregisterTripSessionStateObserver(tripSessionStateObserver)
            }
            this.mapboxNavigation = currentMapboxNavigation
            this.mapboxNavigation?.let {
                it.registerEHorizonObserver(breezeEHorizonObserver!!)
                it.registerTripSessionStateObserver(tripSessionStateObserver)
            }
        }
        return roadObjectHandler!!
    }


    @ExperimentalPreviewMapboxNavigationAPI
    fun destroy() {
        mapboxNavigation?.let { mapboxNavigation ->
            mapboxNavigation.unregisterTripSessionStateObserver(tripSessionStateObserver)
            breezeEHorizonObserver?.let {
                mapboxNavigation.unregisterEHorizonObserver(it)
            }
        }
        breezeRoadObjectObserver?.stopObserver()
        roadObjectHandler?.let {
            it.removeAllObservers()
        }
        breezeRoadObjectObserver = null
        roadObjectHandler = null
        breezeEHorizonObserver = null
    }


    private val tripSessionStateObserver =
        TripSessionStateObserver { tripSessionState ->
            when (tripSessionState) {
                TripSessionState.STARTED -> {
                    breezeRoadObjectObserver?.startObserver()
                }

                TripSessionState.STOPPED -> {
                    breezeRoadObjectObserver?.stopObserver()
                }
            }
        }

}


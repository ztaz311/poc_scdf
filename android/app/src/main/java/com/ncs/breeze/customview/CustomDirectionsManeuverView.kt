package com.ncs.breeze.customview


import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import androidx.annotation.StyleRes
import androidx.appcompat.view.ContextThemeWrapper
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.widget.TextViewCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView.VERTICAL
import com.mapbox.bindgen.Expected
import com.mapbox.navigation.ui.maneuver.api.MapboxManeuverApi
import com.mapbox.navigation.ui.maneuver.model.*
import com.ncs.breeze.R
import com.ncs.breeze.components.BreezeRoutePlanningManeuverAdapter
import com.ncs.breeze.databinding.BreezeRouteManueverLayoutBinding


/**
 * Default view to render a maneuver.
 *
 * @see MapboxManeuverApi
 */
class CustomDirectionsManeuverView : ConstraintLayout {

    /**
     * Default view to render a maneuver.
     *
     * @see MapboxManeuverApi
     */
    constructor(context: Context) : super(context)

    /**
     * Default view to render a maneuver.
     *
     * @see MapboxManeuverApi
     */
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        initAttributes()
    }

    /**
     * Default view to render a maneuver.
     *
     * @see MapboxManeuverApi
     */
    constructor(
        context: Context,
        attrs: AttributeSet?,
        defStyleAttr: Int
    ) : super(context, attrs, defStyleAttr) {
        initAttributes()
    }

    private val upcomingManeuverAdapter = BreezeRoutePlanningManeuverAdapter(context)
    private val binding = BreezeRouteManueverLayoutBinding.inflate(
        LayoutInflater.from(context),
        this,
        true
    )
    private val maneuverToRoadShield = mutableMapOf<String, RoadShield?>()
    private val currentlyRenderedManeuvers: MutableList<Maneuver> = mutableListOf()

    /**
     * The property enables/disables showing the list of upcoming maneuvers.
     * Note: The View doesn't maintain the state of this property. If the device undergoes
     * screen rotation changes make sure to set this property again at appropriate place.
     */
    var upcomingManeuverRenderingEnabled = true
        set(value) {
            field = value
        }

    /**
     * Initialize.
     */
    init {
        binding.routeUpcomingManeuverRecycler.apply {
            layoutManager = LinearLayoutManager(context, VERTICAL, false)
            adapter = upcomingManeuverAdapter
            visibility = View.VISIBLE
        }
    }

    /**
     * Invoke the method to render the maneuvers.
     *
     * The first maneuver on the list will be rendered as the current maneuver while the rest of the maneuvers will be rendered as upcoming maneuvers.
     *
     * You can control the number of maneuvers that will be rendered by manipulating the provided list,
     * for example, you can render the current maneuver and a list of up to 5 upcoming maneuvers like this:
     * ```
     * maneuverView.renderManeuvers(
     *     maneuvers.mapValue {
     *         it.take(6)
     *     }
     * )
     * ```
     *
     * If there were any shields provided via [renderManeuverShields], those shields will be attached to corresponding [Maneuver]s during rendering.
     *
     * This method ignores empty lists of maneuvers and will not clean up the view if an empty list is provided.
     *
     * @param maneuvers maneuvers to render
     * @see MapboxManeuverApi.getManeuvers
     */
    private fun renderManeuvers(maneuvers: Expected<ManeuverError, List<Maneuver>>) {
        maneuvers.onValue { list ->
            if (list.isNotEmpty()) {
                currentlyRenderedManeuvers.clear()
                currentlyRenderedManeuvers.addAll(list)
                renderManeuvers()
            }
        }
    }

    /**
     * Invoke the method to update rendered maneuvers with road shields.
     *
     * The provided shields are mapped to IDs of [PrimaryManeuver], [SecondaryManeuver], and [SubManeuver]
     * and if a maneuver has already been rendered via [renderManeuvers], the respective shields' text will be changed to the shield icon.
     *
     * Invoking this method also caches all of the available shields. Whenever [renderManeuvers] is invoked, the cached shields are reused for rendering.
     *
     * @param shieldMap the map of maneuver IDs to available shields
     */
    private fun renderManeuverShields(shieldMap: Map<String, RoadShield?>) {
        maneuverToRoadShield.putAll(shieldMap)
        renderManeuvers()
    }


    /**
     * Allows you to change the text appearance of step distance text in upcoming maneuver list.
     * @see [TextViewCompat.setTextAppearance]
     * @param style Int
     */
    fun updateUpcomingManeuverStepDistanceTextAppearance(@StyleRes style: Int) {
        upcomingManeuverAdapter.updateUpcomingManeuverStepDistanceTextAppearance(style)
    }


    private fun renderManeuvers() {
        if (currentlyRenderedManeuvers.isNotEmpty()) {
            upcomingManeuverAdapter.updateRoadShields(maneuverToRoadShield)
            drawUpcomingManeuvers(currentlyRenderedManeuvers)
        }
    }

    private fun initAttributes() {

        applyAttributes()

    }

    private fun applyAttributes() {

        upcomingManeuverAdapter.updateIconContextThemeWrapper(
            ContextThemeWrapper(
                context,
                R.style.BreezeMapboxStyleTurnIconManeuver
            )
        )

    }

    private fun drawUpcomingManeuvers(maneuvers: List<Maneuver>) {
        upcomingManeuverAdapter.addUpcomingManeuvers(maneuvers)
        upcomingManeuverAdapter.notifyDataSetChanged()
    }

}

package com.ncs.breeze.common.helper.obu

import android.os.Handler
import android.os.Looper
import com.ncs.breeze.App
import com.ncs.breeze.common.constant.OBUConstants
import com.breeze.model.obu.OBURoadEventData
import com.ncs.breeze.common.rxbus.RxBus
import com.ncs.breeze.common.rxbus.RxEvent
import sg.gov.lta.obu.sdk.conn.MockedConnectionHandler
import sg.gov.lta.obu.sdk.conn.mock.ERPEvent
import sg.gov.lta.obu.sdk.conn.mock.MockEvent
import sg.gov.lta.obu.sdk.conn.mock.TdcidEvent
import sg.gov.lta.obu.sdk.core.enums.OBUCardStatus
import sg.gov.lta.obu.sdk.core.enums.OBUPaymentMode
import timber.log.Timber

object OBUMockDataFactory {

    /**
     * Generate OBU mock event sequence for testing purpose
     * */
    fun generateMockSequence() = MockedConnectionHandler.Builder()
        .setCardBalance(OBUConstants.OBU_MOCK_CARD_BALANCE)
        .setFastestInterval(10000)
        .setCyclicMode(true)
//        .addMockSequence()
        .build()

    fun MockedConnectionHandler.Builder.addMockSequence() = setSequence(
        arrayListOf(
            MockEvent(
                arrayListOf(ERPEvent.ERP1BasedDeductionFailure()),
                TdcidEvent.Template1A()
            ),
        )
    )

    private var count: Int = 0
    fun startMockCardStatus() {
        Handler(Looper.getMainLooper()).postDelayed({
            val CARD_VALID = arrayOf(0, 99)
            val NO_CARD_HAS_BANK_CARD = arrayOf(7, 107)
            val NO_CARD_NO_BANK_CARD = 8

            var status: OBUCardStatus? = null
            var paymentMode: OBUPaymentMode? = null
            when {
                count in CARD_VALID -> {
                    status = OBUCardStatus.Detected
                    paymentMode = OBUPaymentMode.Frontend
                    Timber.e("startMockCardStatus: CARD_VALID $count")
                }

                count in NO_CARD_HAS_BANK_CARD -> {
                    status = OBUCardStatus.NoCardForFrontendPayment
                    paymentMode = OBUPaymentMode.Backend
                    Timber.e("startMockCardStatus: NO_CARD_HAS_BANK_CARD $count")
                }

                count % 10 == NO_CARD_NO_BANK_CARD -> {
                    status = OBUCardStatus.NoCardForFrontendPayment
                    paymentMode = OBUPaymentMode.Frontend
                    Timber.e("startMockCardStatus: NO_CARD_NO_BANK_CARD $count")
                }
            }
            status?.let {
                App.instance?.obuConnectionHelper?.handleCardInformation(
                    status,
                    paymentMode,
                    null,
                    0
                )
            }

            count++
            startMockCardStatus()
        }, 2000)
    }

    fun startMockERPCard() {
        Handler(Looper.getMainLooper()).postDelayed({
            val eventMessage = when (count % 4) {
                0 -> "Card Expired"
                1 -> "Insufficient Balance"
                2 -> "Card Error"
                else -> "System Error"
            }

            val roadData = OBURoadEventData(
                OBURoadEventData.EVENT_TYPE_ERP_FAILURE,
                eventMessage,
                null, null, null, "$999",
            )
            RxBus.publish(RxEvent.OBURoadEvent(roadData))
            count++
            startMockERPCard()
        }, 10000)
    }

    fun startMockERPAhead(){
        Handler(Looper.getMainLooper()).postDelayed({
            val roadData = OBURoadEventData(
                OBURoadEventData.EVENT_TYPE_ERP_CHARGING,
                "Eu Tong Sen St at Central (60)",
                "500m", null, "Eu Tong Sen St at Central (60)", "$999","ERP Ahead"
            )
            RxBus.publish(RxEvent.OBURoadEvent(roadData))
                                                    startMockERPAhead()
        },10000)
    }
}
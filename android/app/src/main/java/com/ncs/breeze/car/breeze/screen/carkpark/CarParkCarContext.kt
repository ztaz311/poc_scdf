package com.ncs.breeze.car.breeze.screen.carkpark

import com.ncs.breeze.car.breeze.MainBreezeCarContext


data class CarParkCarContext internal constructor(
    val mainCarContext: MainBreezeCarContext
) {
    /** MainCarContext **/
    val carContext = mainCarContext.carContext
    val mapboxCarMap = mainCarContext.mapboxCarMap
    val distanceFormatter = mainCarContext.distanceFormatter
}

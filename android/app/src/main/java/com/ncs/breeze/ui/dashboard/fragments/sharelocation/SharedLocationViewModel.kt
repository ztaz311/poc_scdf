package com.ncs.breeze.ui.dashboard.fragments.sharelocation

import android.app.Application
import androidx.annotation.DrawableRes
import com.breeze.model.SharedLocation
import com.breeze.model.constants.AmenityType
import com.breeze.model.enums.AmenityBand
import com.breeze.model.extensions.dpToPx
import com.mapbox.maps.EdgeInsets
import com.ncs.breeze.R
import com.ncs.breeze.common.remote.ApiHelper
import com.ncs.breeze.ui.base.BaseFragmentViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject

class SharedLocationViewModel @Inject constructor(
    private val apiHelper: ApiHelper,
    application: Application
) : BaseFragmentViewModel(application) {

    var sharedLocation: SharedLocation? = null
        set(value) {
            field = value
            updateMapCameraPadding()
        }
    var isSharedLocationSelected: Boolean = true

    var mapCameraPadding = EdgeInsets(
        200.0.dpToPx(), 60.0.dpToPx(), 200.0.dpToPx(), 60.0.dpToPx()
    )
        private set

    private fun updateMapCameraPadding() {
        val location = sharedLocation ?: return
        mapCameraPadding =
            EdgeInsets(
                200.0.dpToPx(),
                60.0.dpToPx(),
                (if (location.saved) 32.0 else 150.0).dpToPx(),
                60.0.dpToPx()
            )
    }

    fun fetchSharedLocation(token: String, onSuccess: Consumer<SharedLocation>) {
        apiHelper.getSharedLocation(token)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    if (it.success) onSuccess.accept(it.data)
                },
                {
                    Timber.e("fetchSharedLocation: $it")
                }
            ).also {
                compositeDisposable.add(it)
            }
    }

    @DrawableRes
    fun getSharedLocationAnnotationRes(): Int {
        val location = this.sharedLocation ?: return R.drawable.destination_puck
        if (isSharedLocationSelected) {
            when {
                location.amenityType == AmenityType.CARPARK && location.availablePercentImageBand == AmenityBand.TYPE0.name ->
                    return if (location.saved) R.drawable.ic_map_carpark_type0_bookmarked_selected else R.drawable.ic_parkingmarker_type0_nondestination_selected

                location.amenityType == AmenityType.CARPARK && location.availablePercentImageBand == AmenityBand.TYPE1.name ->
                    return if (location.saved) R.drawable.ic_map_carpark_type1_bookmarked_selected else R.drawable.ic_parkingmarker_type1_nondestination_selected

                location.amenityType == AmenityType.CARPARK && location.availablePercentImageBand == AmenityBand.TYPE2.name ->
                    return if (location.saved) R.drawable.ic_map_carpark_type2_bookmarked_selected else R.drawable.ic_parkingmarker_type2_nondestination_selected

                location.amenityType == AmenityType.EVCHARGER ->
                    return if (location.saved) R.drawable.ic_map_evcharger_bookmarked_selected else R.drawable.ic_map_evcharger_selected

                location.amenityType == AmenityType.PETROL ->
                    return if (location.saved) R.drawable.ic_map_petrol_bookmarked_selected else R.drawable.ic_map_petrol_selected
            }
        } else {
            when {
                location.amenityType == AmenityType.CARPARK && location.availablePercentImageBand == AmenityBand.TYPE0.name ->
                    return if (location.saved) R.drawable.ic_map_carpark_type0_bookmarked else R.drawable.ic_parkingmarker_type0_nondestination_notselected

                location.amenityType == AmenityType.CARPARK && location.availablePercentImageBand == AmenityBand.TYPE1.name ->
                    return if (location.saved) R.drawable.ic_map_carpark_type1_bookmarked else R.drawable.ic_parkingmarker_type1_nondestination_notselected

                location.amenityType == AmenityType.CARPARK && location.availablePercentImageBand == AmenityBand.TYPE2.name ->
                    return if (location.saved) R.drawable.ic_map_carpark_type2_bookmarked else R.drawable.ic_parkingmarker_type2_nondestination_notselected

                location.amenityType == AmenityType.EVCHARGER ->
                    return if (location.saved) R.drawable.ic_map_evcharger_bookmarked else R.drawable.ic_map_evcharger

                location.amenityType == AmenityType.PETROL ->
                    return if (location.saved) R.drawable.ic_map_petrol_bookmarked else R.drawable.ic_map_petrol
            }
        }
        return R.drawable.destination_puck
    }
}
package com.ncs.breeze.ui.dashboard.fragments.obulite

import android.app.Application
import android.location.Location
import androidx.lifecycle.viewModelScope
import com.breeze.model.api.ErrorData
import com.breeze.model.api.request.AmenitiesRequest
import com.breeze.model.api.request.CopilotAPIRequest
import com.breeze.model.api.request.ETARequest
import com.breeze.model.api.response.ETAResponse
import com.breeze.model.api.response.EasyBreeziesResponse
import com.breeze.model.api.response.amenities.BaseAmenity
import com.breeze.model.api.response.amenities.ResponseAmenities
import com.breeze.model.api.response.tbr.ResponseZoneAlertDetail
import com.breeze.model.constants.AmenityType.CARPARK
import com.breeze.model.constants.Constants
import com.breeze.model.enums.RoutePreference
import com.google.gson.JsonElement
import com.mapbox.geojson.Point
import com.ncs.breeze.car.breeze.utils.BreezeCarUtil
import com.ncs.breeze.car.breeze.utils.BreezeCarUtil.apiHelper
import com.ncs.breeze.common.ServiceLocator
import com.ncs.breeze.common.extensions.android.getApp
import com.ncs.breeze.common.extensions.android.getBreezeUserPreference
import com.ncs.breeze.common.extensions.android.getUserDataPreference
import com.ncs.breeze.common.helper.route.RoutePlanningHelper
import com.ncs.breeze.common.model.RouteAdapterObjects
import com.ncs.breeze.common.model.RouteDepartureDetails
import com.ncs.breeze.common.remote.ApiHelper
import com.ncs.breeze.common.remote.ApiObserver
import com.ncs.breeze.common.storage.BreezeUserPreference
import com.ncs.breeze.common.utils.BreezeMapDataHolder
import com.ncs.breeze.common.utils.InitObjectUtilsController
import com.ncs.breeze.components.SingleLiveEvent
import com.ncs.breeze.components.navigationlog.HistoryFileUploader
import com.ncs.breeze.ui.base.BaseFragmentViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import timber.log.Timber
import java.util.concurrent.ConcurrentHashMap
import javax.inject.Inject

class OBULiteFragmentViewModel @Inject constructor(
    apiHelper: ApiHelper,
    application: Application
) : BaseFragmentViewModel(
    application
) {

    var easyBreeziesResponse: SingleLiveEvent<EasyBreeziesResponse> = SingleLiveEvent()

    var routesRetrieved: SingleLiveEvent<ConcurrentHashMap<RoutePreference, List<RouteAdapterObjects>>> =
        SingleLiveEvent()

    private var mapOfRoutePreference: ConcurrentHashMap<RoutePreference, List<RouteAdapterObjects>> =
        ConcurrentHashMap<RoutePreference, List<RouteAdapterObjects>>()


    var mListAmenities: ArrayList<BaseAmenity> = arrayListOf()
    var amenityType: SingleLiveEvent<String?> = SingleLiveEvent()
    var mResponseZoneAlertDetail: SingleLiveEvent<ResponseZoneAlertDetail> = SingleLiveEvent()

    var isNotExistAnyAmenities: SingleLiveEvent<Boolean> = SingleLiveEvent()

    var etaStartSuccessful: SingleLiveEvent<ETARequest> =
        SingleLiveEvent()
    var etaStartProgress: SingleLiveEvent<Boolean> = SingleLiveEvent()

    /**
     * type: CARPARK, PETROL, EVCHARGER
     */
    fun getAmenitiesOnMap(currentLocation: Location, type: String) {
        val listAmenities: ArrayList<BaseAmenity> = arrayListOf()
        viewModelScope.launch {

            val amenitiesRequest = AmenitiesRequest(
                lat = currentLocation.latitude,
                long = currentLocation.longitude,
                maxRad = Constants.PARKING_CHECK_RADIUS_MAX
            )

            when (type) {
                CARPARK -> {
                    amenitiesRequest.setListType(listOf(type), getApp().getBreezeUserPreference()
                        .getCarParkAvailabilityValue(
                            BreezeCarUtil.masterlists,
                            ""
                        ))
                    amenitiesRequest.setCarParkCategoryFilter()
                    amenitiesRequest.rad = BreezeUserPreference.getInstance(getApp())
                        .getCarParkDistanceSettingValue(BreezeCarUtil.masterlists)
                    amenitiesRequest.resultCount =
                        BreezeUserPreference.getInstance(getApp())
                            .getCarParkCountSettingValue(BreezeCarUtil.masterlists)
                }

                else -> {
                    amenitiesRequest.setListType(listOf(type))
                    amenitiesRequest.rad = getApp().getUserDataPreference()?.getEvPetrolRange()?.div(1000.0)
                        ?: Constants.DEFAULT_PETROL_EV_RADIUS
                    amenitiesRequest.maxRad = getApp().getUserDataPreference()?.getEvPetrolMaxRange()?.div(1000.0)
                        ?: Constants.DEFAULT_PETROL_EV_MAX_RADIUS
                    amenitiesRequest.resultCount = getApp().getUserDataPreference()?.getEvPetrolResultCount()
                        ?: Constants.DEFAULT_PETROL_EV_RESULT_COUNT
                    val amenityListPref =
                        BreezeMapDataHolder.amenitiesPreference.filter { it.elementName == type }
                    if (amenityListPref.isNotEmpty()) {
                        amenitiesRequest.setAmenitiesPreferenceListType(amenityListPref)
                    }
                }
            }

            apiHelper.getAmenities(amenitiesRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : ApiObserver<ResponseAmenities>(compositeDisposable) {
                    override fun onSuccess(data: ResponseAmenities) {
                        data.apply {

                            /**
                             * processing data before use
                             */
                            var isNotExistAmenity = false
                            data.amenities.asSequence().forEach { amenities ->
                                amenities.data.asSequence().forEach {
                                    if (amenities.type != null) {
                                        it.amenityType = amenities.type!!
                                        it.iconUrl = amenities.iconUrl
                                        listAmenities.add(it)
                                    }
                                }
                            }

                            if (listAmenities.isEmpty()) {
                                isNotExistAmenity = true
                            }
                            mListAmenities = listAmenities
                            amenityType.postValue(type)
                            isNotExistAnyAmenities.value = isNotExistAmenity
                        }
                    }

                    override fun onError(e: ErrorData) {
                        /**
                         * must check data null for return error
                         */
                        mListAmenities = arrayListOf()
                    }
                })
        }

    }


    fun findRoutes(
        originPoint: Point,
        destination: Point,
        wayPoints: List<Point>,
        carParkName: String,
        departureDetails: RouteDepartureDetails?
    ) {
        Timber.d("Start api calls")
        viewModelScope.launch(Dispatchers.IO) {
            InitObjectUtilsController.mRoutePlanningHelper?.let { routePlaningHelper ->
                val curRoutePreference: HashMap<RoutePreference, List<RouteAdapterObjects>> =
                    routePlaningHelper.findRoutes(
                        originPoint, destination,
                        carParkName,
                        wayPoints,
                        RoutePlanningHelper.RouteAlgorithm.NORMAL,
                        departureDetails
                    );
                updateMapRoutePreference(curRoutePreference)
                routesRetrieved.postValue(mapOfRoutePreference);
            }
        }
    }

    /**
     * get alert when user cometo zone
     */
    fun getZoneAlertDetail(
        navigationtype: String = "CRUISE",
        profile: String,
        zoneid: String,
        targetZoneId: String?,
        destinationCarpark: Boolean = false,
        carparkId: String = "",
    ) {
        Timber.d("Start api calls")
        apiHelper.getZoneAlertDetails(
            navigationtype,
            profile,
            zoneid,
            targetZoneId,
            destinationCarpark,
            carparkId
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : ApiObserver<ResponseZoneAlertDetail>(compositeDisposable) {
                override fun onSuccess(data: ResponseZoneAlertDetail) {
                    data.apply {
                        mResponseZoneAlertDetail.postValue(data)
                    }
                }

                override fun onError(e: ErrorData) {

                }
            })

    }


    private fun updateMapRoutePreference(curRoutePreference: HashMap<RoutePreference, List<RouteAdapterObjects>>) {
        mapOfRoutePreference.clear();
        mapOfRoutePreference.putAll(curRoutePreference)
    }

    fun sendSecondaryFirebaseID(username: String?) {
        if (HistoryFileUploader.ENABLE_COPILOT) {
            viewModelScope.launch {
                apiHelper.sendSecondaryFirebaseUserID(
                    CopilotAPIRequest(
                        ServiceLocator.getSecondaryFirebaseAuthUserId(),
                        username
                    )
                )
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(object : ApiObserver<JsonElement>(compositeDisposable) {
                        override fun onSuccess(data: JsonElement) {
                            Timber.d("Copilot API is successful: $data")
                        }

                        override fun onError(e: ErrorData) {
                            Timber.d("Copilot API failed: $e")
                        }
                    })
            }
        }
    }

    /**
     * call api to noty user that is share live location
     */
    fun startETA(etaRequest: ETARequest) {
        Timber.d("ETA Request: " + etaRequest.toString())
        etaStartProgress.postValue(true)
        apiHelper.sendETAMessage(etaRequest)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : ApiObserver<ETAResponse>(compositeDisposable) {
                override fun onSuccess(data: ETAResponse) {
                    etaRequest.tripEtaUUID = data.tripEtaUUID
                    etaStartProgress.postValue(false)
                    etaStartSuccessful.value = etaRequest
                }

                override fun onError(e: ErrorData) {
                    Timber.d("API failed: " + e.toString())
                    etaStartProgress.postValue(false)
                }

            })
    }

}
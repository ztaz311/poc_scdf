package com.ncs.breeze.ui.login.activity

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Rect
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ImageView
import androidx.activity.viewModels
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.breeze.customization.view.BreezeEditText
import com.breeze.model.constants.Constants
import com.breeze.model.constants.INTENT_KEY
import com.breeze.model.constants.MODE_LOGIN_TYPE
import com.instabug.bug.BugReporting
import com.instabug.library.invocation.InstabugInvocationEvent
import com.ncs.breeze.App
import com.ncs.breeze.R
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.Event
import com.ncs.breeze.common.analytics.Screen
import com.ncs.breeze.common.event.ToCarRx
import com.ncs.breeze.common.extensions.android.getApp
import com.ncs.breeze.common.extensions.android.getRNFragmentNavigationParams
import com.ncs.breeze.common.model.rx.AppEventLogout
import com.ncs.breeze.common.model.rx.AppSessionReadyData
import com.ncs.breeze.common.model.rx.AppSessionReadyEvent
import com.ncs.breeze.common.storage.BreezeGuestPreference
import com.ncs.breeze.common.utils.InitObjectUtilsController
import com.ncs.breeze.common.utils.Utils
import com.ncs.breeze.common.utils.Variables
import com.ncs.breeze.databinding.ActivityLoginBinding
import com.ncs.breeze.reactnative.nativemodule.ShareBusinessLogicEvent
import com.ncs.breeze.ui.base.BaseActivity
import com.ncs.breeze.ui.dashboard.activity.DashboardActivity
import com.ncs.breeze.ui.login.fragment.guest.view.GuestModeSignUpFragment
import com.ncs.breeze.ui.login.fragment.signIn.view.SignInFragmentBreeze
import com.ncs.breeze.ui.login.fragment.signIn.view.VerifyMobileFragment
import com.ncs.breeze.ui.login.fragment.signUp.view.CreateUsernameFragmentBreeze
import com.ncs.breeze.ui.login.fragment.signUp.view.LoginPhoneNumberFragmentBreeze
import com.ncs.breeze.ui.login.fragment.signUp.view.SignUpFragmentBreeze
import com.ncs.breeze.ui.login.fragment.signUp.view.TermsAndCondnFragment
import com.ncs.breeze.ui.rn.CustomReactFragment
import com.ncs.breeze.ui.splash.PermissionActivity
import com.ncs.breeze.ui.utils.DialogUtilsBreeze
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import java.util.Calendar
import javax.inject.Inject


class LoginActivity : BaseActivity<ActivityLoginBinding, LoginViewModel>() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val loginViewModel: LoginViewModel by viewModels {
        viewModelFactory
    }

    companion object {
        const val KEY_MODE_LOGIN_TYPE = "KEY_MODE_LOGIN_TYPE"
    }

    private val MAX_CLICK_DURATION = 200
    private var startClickTime: Long = 0

    private var showUsernameScreen: Boolean = false

    override fun inflateViewBinding() = ActivityLoginBinding.inflate(layoutInflater, null, false)

    override fun getViewModelReference(): LoginViewModel {
        return loginViewModel
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.Theme_Breeze)
        super.onCreate(savedInstanceState)
        // fixme: destroy observers if user being logged out. Should have better method to control this
        getApp()?.destroyNavigationObservers()
        BugReporting.setInvocationEvents(InstabugInvocationEvent.SCREENSHOT)
        val modeLoginType = intent.getIntExtra(KEY_MODE_LOGIN_TYPE, -1)
        initializeUI()
        /**
         * depend on type of => go to screen login or create account
         */
        handleSignInOrSignUp(modeLoginType)

    }

    private fun handleSignInOrSignUp(modeLoginType: Int) {
        if (modeLoginType == MODE_LOGIN_TYPE.SIGN_IN) {
            requestSignInAccount()
        } else if (modeLoginType == MODE_LOGIN_TYPE.CREATE_ACCOUNT) {
            requestCreateAccount()
        }
    }

    override fun onResume() {
        super.onResume()
        setLoginActivityStatusBar()
        checkForUpdates()
    }

    private fun initializeUI() {
        openTutorialScreen()
        supportFragmentManager.addOnBackStackChangedListener {
            val f = supportFragmentManager.findFragmentById(R.id.fragment_container)
            val count = supportFragmentManager.fragments.size
        }
    }


    /**
     * open fragment create UserName screen
     */
    fun openCreateUserNameScreen(fromScreen: String) {
        val bundle = Bundle().apply {
            putString(TermsAndCondnFragment.FROM_SCREEN, fromScreen)
        }
        supportFragmentManager
            .beginTransaction()
            .replace(
                R.id.fragment_container_waring_process,
                CreateUsernameFragmentBreeze.newInstance().apply {
                    arguments = bundle
                },
                CreateUsernameFragmentBreeze.SCREEN_NAME
            )
            .addToBackStack(CreateUsernameFragmentBreeze.SCREEN_NAME)
            .commit()
    }


    /**
     * open tutorial screen
     */
    private fun openTutorialScreen() {
        val navigationParams = getRNFragmentNavigationParams(
            sessionToken = myServiceInterceptor.getSessionToken(),
            fragmentTag = Constants.RN_CONSTANTS.ONBOARDING_TUTORIAL_SCREEN
        )
        val initialProperties = bundleOf(
            Constants.RN_TO_SCREEN to Constants.RN_CONSTANTS.ONBOARDING_TUTORIAL_SCREEN,
            Constants.RN_CONSTANTS.NAVIGATION_PARAMS to navigationParams
        )
        val bottomPanelFragment = CustomReactFragment.newInstance(
            Constants.RN_CONSTANTS.COMPONENT_NAME,
            initialProperties
        )
        supportFragmentManager
            .beginTransaction()
            .replace(
                R.id.fragment_container_waring_process,
                bottomPanelFragment,
                Constants.RN_CONSTANTS.ONBOARDING_TUTORIAL_SCREEN
            )
            .commitNowAllowingStateLoss()
    }

    override fun onBackPressed() {

        val fragmentInWarninglayout: Fragment? =
            this.supportFragmentManager.findFragmentById(R.id.fragment_container_waring_process)
        if (fragmentInWarninglayout is CreateUsernameFragmentBreeze) return

        val f: Fragment? =
            this.supportFragmentManager.findFragmentById(viewBinding.fragmentContainer.id)

        if (f is DeviceBackPressedListener) {
            f.onDeviceBackPressed()
        }

        super.onBackPressed()
    }

    /**
     * open term & condition fragment
     */
    fun openTermAndConDnFragment(fromScreen: String) {
        val bundle = Bundle().apply {
            putString(TermsAndCondnFragment.FROM_SCREEN, fromScreen)
        }
        supportFragmentManager
            .beginTransaction()
            .replace(
                R.id.fragment_container_waring_process,
                TermsAndCondnFragment.newInstance().apply {
                    arguments = bundle
                },
                TermsAndCondnFragment.SCREEN_NAME
            )
            .addToBackStack(TermsAndCondnFragment.SCREEN_NAME)
            .commit()
    }


    fun declineTnc() {
        logoutApp {
            /**
             * post event for logout
             */
            ToCarRx.postEventWithCacheLast(AppEventLogout())
            restartApp()
        }
    }

    fun openDashboardActivity() {
        if (Constants.RESET_AMENITY_SELECTION_ON_LAUNCH) {
            loginViewModel.amenityResetResponse.observe(this) {
                loginViewModel.amenityResetResponse.removeObservers(this)
                replaceActivity(Intent(this, DashboardActivity::class.java))
            }
            resetAmenitySelections()
        } else {
            replaceActivity(Intent(this, DashboardActivity::class.java))
        }
    }

    /**
     * Resetting Amenity Selection (temp) as per Lee Fong's request
     */
    private fun resetAmenitySelections() {
        (applicationContext as? App)?.run {
            shareBusinessLogicPackage.getRequestsModule().flatMap {
                it.callRN(ShareBusinessLogicEvent.RESET_AMENITY_SELECTIONS.eventName, null)
            }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { result ->
                        loginViewModel.amenityResetResponse.postValue(true)
                    },
                    { error ->
                        loginViewModel.amenityResetResponse.postValue(false)
                    }
                )
        }
    }

    private fun openLocationPermissionActivity() {
        launchActivity(Intent(this, PermissionActivity::class.java))
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            Constants.REQUEST_CODES.REQUEST_CODE_REQUIRED_PERMISSION -> {

                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    openLocationPermissionActivity()
                }
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        if (ev != null) {
            if (ev.action == MotionEvent.ACTION_DOWN) {
                startClickTime = Calendar.getInstance().timeInMillis
            } else if (ev.action == MotionEvent.ACTION_UP) {
                val clickDuration = Calendar.getInstance().timeInMillis - startClickTime
                if (clickDuration < MAX_CLICK_DURATION) {
                    //click event has occurred
                    val v: View? = currentFocus
                    if (v is EditText) {
                        if (v.parent.parent.parent is BreezeEditText) {
                            val view = (v.parent as ViewGroup).getChildAt(2)
                            if (view is ImageView && view.id == R.id.right_icon) {
                                val outRect = Rect()
                                view.getGlobalVisibleRect(outRect)
                                if (!outRect.contains(ev.rawX.toInt(), ev.rawY.toInt())) {
                                    v.clearFocus()
                                    val imm: InputMethodManager =
                                        getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
                                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0)
                                }
                            }
                        }
                    }
                }
            }
        }
        return super.dispatchTouchEvent(ev)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Timber.d("onActivity result called")
        /**
         * should call like that because it will return callback to amplify fragment
         */
        val fragment =
            supportFragmentManager.findFragmentById(viewBinding.fragmentContainerWaringProcess.id)
        fragment?.onActivityResult(requestCode, resultCode, data)
    }

    override fun getFragmentContainer(): Int {
        return viewBinding.fragmentContainer.id
    }


    /**
     * show warning create guest account
     */
    fun showFragmentWarningCreateGuestAccount(isFromCreateAccount: Boolean = false) {
        if (Variables.isNetworkConnected) {
            val idGuestTemp = BreezeGuestPreference.getInstance(applicationContext)?.getGuestId()

            val bundle = Bundle()
            if (idGuestTemp.isNullOrEmpty()) {
                bundle.putBoolean(INTENT_KEY.AUTO_SIGN_IN_GEUST_MODE, false)
                bundle.putBoolean(
                    INTENT_KEY.IS_OPEN_GUEST_FROM_CREATE_ACCOUNT_SCREEN,
                    isFromCreateAccount
                )
            } else {
                bundle.putBoolean(INTENT_KEY.AUTO_SIGN_IN_GEUST_MODE, true)
                bundle.putBoolean(
                    INTENT_KEY.IS_OPEN_GUEST_FROM_CREATE_ACCOUNT_SCREEN,
                    isFromCreateAccount
                )
            }

            val fragmentSignUpGuest = GuestModeSignUpFragment.newInstance().apply {
                arguments = bundle
            }
            supportFragmentManager
                .beginTransaction()
                .addToBackStack(GuestModeSignUpFragment.SCREEN_NAME)
                .add(
                    R.id.fragment_container_waring_process,
                    fragmentSignUpGuest,
                    GuestModeSignUpFragment.SCREEN_NAME
                )
                .commit()

        } else {
            (application as? App)?.showNoInternetToast()
        }
    }


    /**
     * open fragment create account breeze screen
     */
    fun openCreateAccountBreezeScreen() {
        val bundle = Bundle().apply {
            putString("from", "login")
        }
        supportFragmentManager
            .beginTransaction()
            .replace(
                R.id.fragment_container_waring_process,
                SignUpFragmentBreeze.newInstance().apply {
                    arguments = bundle
                },
                SignUpFragmentBreeze.SCREEN_NAME
            )
            .addToBackStack(SignUpFragmentBreeze.SCREEN_NAME)
            .commit()
    }


    /**
     * open fragment signin account breeze screen
     */
    fun openSignInBreezeScreen() {
        val bundle = Bundle().apply {
            putString("from", "login")
        }
        supportFragmentManager
            .beginTransaction()
            .replace(
                R.id.fragment_container_waring_process,
                SignInFragmentBreeze.newInstance().apply {
                    arguments = bundle
                },
                SignInFragmentBreeze.SCREEN_NAME
            )
            .addToBackStack(SignInFragmentBreeze.SCREEN_NAME)
            .commit()
    }


    /**
     * open fragment signin account breeze screen
     */
    fun openLoginPhoneNumberBreezeScreen() {
        val bundle = Bundle().apply {
            putString("from", "login")
        }
        supportFragmentManager
            .beginTransaction()
            .replace(
                R.id.fragment_container_waring_process,
                LoginPhoneNumberFragmentBreeze.newInstance().apply {
                    arguments = bundle
                },
                LoginPhoneNumberFragmentBreeze.SCREEN_NAME
            )
            .addToBackStack(LoginPhoneNumberFragmentBreeze.SCREEN_NAME)
            .commit()
    }


    /**
     * open fragment signin account breeze screen
     */
    fun openVerifyOTPBreezeScreen(pBundle: Bundle) {
        supportFragmentManager
            .beginTransaction()
            .replace(
                R.id.fragment_container_waring_process,
                VerifyMobileFragment.newInstance().apply {
                    arguments = pBundle
                },
                VerifyMobileFragment.SCREEN_NAME
            )
            .addToBackStack(VerifyMobileFragment.SCREEN_NAME)
            .commit()
    }


    fun showDialogSuccess(isExistUser: Boolean, fromScreen: String) {
        ToCarRx.postEventWithCacheLast(AppSessionReadyEvent(AppSessionReadyData(true)))
        val messageShow =
            Utils.getFormattedSpan(
                resources.getString(if (isExistUser) R.string.logn_success_greeting else R.string.creation_success_greeting),
                String.format("%s", userPreferenceUtil.retrieveUserName()),
                resources.getString(R.string.creation_success_greeting_replace_text)
            )
        DialogUtilsBreeze.showSuccessDialog(
            this,
            messageShow,
            fromScreen = fromScreen,
            existUser = isExistUser
        ) {

            Analytics.logClickEvent(Event.SUCCESS_NEXT, Screen.ONBOARDING_GUEST_SIGNUP)
            when (fromScreen) {
                TermsAndCondnFragment.FROM_GUEST -> {
                    Analytics.logClickEvent(Event.SUCCESS_NEXT, Screen.ONBOARDING_GUEST_SIGNUP)
                }

                TermsAndCondnFragment.FROM_CREATE_ACCOUNT -> {
                    Analytics.logClickEvent(Event.SUCCESS_NEXT, Screen.ONBOARDING_CREATE_ACC_SIGNUP)
                }

                TermsAndCondnFragment.FROM_SIGN_IN -> {
                    Analytics.logClickEvent(Event.SUCCESS_NEXT, Screen.ONBOARDING_SIGNIN)
                }
            }

            getApp()?.shareBusinessLogicHelper?.updatePairedVehicleList()

            val intent = Intent(this, DashboardActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            }
            startActivity(intent)
        }
    }

    /**
     * when user click request
     */
    fun requestCreateAccount() {
        runOnUiThread {
            InitObjectUtilsController.currentObjectFlowLoginSignUp?.mode =
                MODE_LOGIN_TYPE.CREATE_ACCOUNT
            InitObjectUtilsController.currentObjectFlowLoginSignUp?.isUserNameAvailable = false
            /**
             * open term screen first
             */
            openTermAndConDnFragment(TermsAndCondnFragment.FROM_CREATE_ACCOUNT)
            Analytics.logClickEvent(
                Event.CREATE_ACCOUNT,
                Screen.ONBOARDING_CREATE_ACCOUNT
            )
        }
    }


    /**
     * when user click sign in account
     */
    fun requestSignInAccount() {
        runOnUiThread {
            /**
             * update mode create account
             */
            InitObjectUtilsController.currentObjectFlowLoginSignUp?.mode =
                MODE_LOGIN_TYPE.SIGN_IN
            InitObjectUtilsController.currentObjectFlowLoginSignUp?.isUserNameAvailable = false
            /**
             * open signIn Screen
             */
            openSignInBreezeScreen()
            Analytics.logClickEvent(Event.SIGNIN, Screen.ONBOARDING_SIGNIN)
        }
    }
}
package com.ncs.breeze.ui.base

import android.app.Application
import android.text.TextUtils
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import com.breeze.model.api.ErrorData
import com.breeze.model.api.response.SettingsResponse
import com.breeze.model.api.response.UserDataResponse
import com.breeze.model.constants.Constants
import com.breeze.model.constants.INSTABUG
import com.breeze.model.constants.STATE_FETCH_USER_ATTR
import com.breeze.model.enums.AmplifyIdentityProviderType
import com.breeze.model.enums.CarParkViewOption
import com.breeze.model.enums.RoutePreference
import com.breeze.model.enums.UserTheme
import com.google.firebase.crashlytics.ktx.crashlytics
import com.google.firebase.ktx.Firebase
import com.instabug.library.Instabug
import com.ncs.breeze.car.breeze.utils.BreezeCarUtil
import com.ncs.breeze.common.extensions.android.getApp
import com.ncs.breeze.common.remote.ApiHelper
import com.ncs.breeze.common.remote.ApiObserver
import com.ncs.breeze.common.storage.BreezeUserPreference
import com.ncs.breeze.common.utils.AWSUtils
import com.ncs.breeze.common.utils.BreezeUserPreferenceUtil
import com.ncs.breeze.components.SingleLiveEvent
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.launch
import org.json.JSONArray
import org.json.JSONObject
import timber.log.Timber

open class BaseFragmentViewModel(application: Application) : AndroidViewModel(application) {
    val loader: SingleLiveEvent<Boolean> = SingleLiveEvent()
    val error: SingleLiveEvent<ErrorData?> = SingleLiveEvent()
    var compositeDisposable: CompositeDisposable = CompositeDisposable()

    /**
     * fetch UserData
     */
    fun checkTermOfCondition(
        breezeUserPreferenceUtil: BreezeUserPreferenceUtil,
        apiHelper: ApiHelper,
        result: (Boolean) -> Unit
    ) {
        viewModelScope.launch {
            apiHelper.getUserData()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : ApiObserver<UserDataResponse>(compositeDisposable) {
                    override fun onSuccess(data: UserDataResponse) {
                        result(data.tncAcceptStatus ?: false)
                        breezeUserPreferenceUtil.saveUserCreatedDate(data.createdTimeStamp)
                    }

                    override fun onError(e: ErrorData) {
                        result(false)
                    }
                })
        }
    }

    fun amplifyFetchUserAttributes(
        breezeUserPreferenceUtil: BreezeUserPreferenceUtil,
        onResult: (String) -> Unit
    ) {
        AWSUtils.fetchUserAttributes({ result ->
            var userIdentityProviderType = AmplifyIdentityProviderType.AMPLIFY_DEFAULT.type

            Timber.d("amplifyFetchUserAttributes: $result")

            for (item in result) {
                if (TextUtils.equals(item.key.keyString, Constants.AMPLIFY_CONSTANTS.SUB)) {
                    Timber.d("Inside sub attribute ${item.value}")
                    BreezeUserPreference.getInstance(getApp()).saveUserId(item.value)
                    Firebase.crashlytics.setUserId(item.value)
                    Instabug.setUserAttribute(INSTABUG.KEY_USER_ID, item.value)
                    continue
                }

                if (TextUtils.equals(item.key.keyString, Constants.AMPLIFY_CONSTANTS.CUSTOM_ONBOARDING_STATE)) {
                    breezeUserPreferenceUtil.saveUserOnBoardingState(item.value)
                    continue
                }


                if (TextUtils.equals(item.key.keyString, Constants.AMPLIFY_CONSTANTS.EMAIL_VERIFIED)) {
                    Timber.d("Inside email verification attribute ${item.value}")
                    continue
                }

                if (TextUtils.equals(item.key.keyString, Constants.AMPLIFY_CONSTANTS.IDENTITIES)) {
                    try {
                        val jsonArray = JSONArray(item.value)
                        val jsonObj = jsonArray[0] as JSONObject
                        userIdentityProviderType = jsonObj.getString("providerType")
                    } catch (e: Exception) {
                        Timber.e(e, "Exception occurred while parsing JSON")
                    }
                }
            }
            breezeUserPreferenceUtil.saveAmplifyProviderType(userIdentityProviderType)

            onResult(
                if (breezeUserPreferenceUtil.retrieveUserName().isEmpty())
                    STATE_FETCH_USER_ATTR.USER_NO_USERNAME_ATTRIBUTE
                else
                    STATE_FETCH_USER_ATTR.USER_USERNAME_AVAILABLE
            )
        }, {
            Timber.e("amplifyFetchUserAttributes: $it")
            onResult(STATE_FETCH_USER_ATTR.FETCH_ATTR_USER_FAILS)
        })
    }


    /**
     * function to get all setting mapper & user setting
     */
    fun getAllSettingZipData(
        breezeUserPreferenceUtil: BreezeUserPreferenceUtil,
        apiHelper: ApiHelper,
        result: (Boolean) -> Unit,
    ) {
        val observableOne = apiHelper.getAllSettingSystem()
            .subscribeOn(Schedulers.io())?.observeOn(AndroidSchedulers.mainThread())
        val observableTwo = apiHelper.getUserSettings()
            .subscribeOn(Schedulers.io())?.observeOn(AndroidSchedulers.mainThread())
        val observableNameBlack = apiHelper.getNameBlackList()
            .subscribeOn(Schedulers.io())?.observeOn(AndroidSchedulers.mainThread())

        Observable.zip(
            observableOne,
            observableTwo,
            observableNameBlack
        ) { allSettingConfig, userSettingResponse, settingNameBlackList ->
            BreezeCarUtil.listNameBlackList = settingNameBlackList.data
            BreezeCarUtil.masterlists = allSettingConfig.masterlists
            Timber.e("BreezeCarUtil.masterlists: ${BreezeCarUtil.masterlists?.carparkListDistance} - ${BreezeCarUtil.masterlists?.carparkAvailabilityDistance}")
            handleUserSettingResponse(
                breezeUserPreferenceUtil,
                userSettingResponse
            )
            true
        }.subscribe(
            {
                result(true)
                //sendRNEvent()
            }, {
                result(false)
            }
        ).addTo(compositeDisposable)


    }


    private fun handleUserSettingResponse(
        breezeUserPreferenceUtil: BreezeUserPreferenceUtil,
        data: SettingsResponse,
    ) {
        BreezeCarUtil.settingsResponse = data

        for (item in data.settings) {
            if (TextUtils.equals(item.attribute, Constants.SETTINGS.ATTR_MAP_DISPLAY)) {
                val themePreference = item.attribute_code
                val userTheme: UserTheme = when (themePreference) {
                    UserTheme.LIGHT.type -> UserTheme.LIGHT
                    UserTheme.DARK.type -> UserTheme.DARK
                    else -> UserTheme.SYSTEM_DEFAULT
                }
                breezeUserPreferenceUtil.saveUserThemePreference(userTheme)

            } else if (TextUtils.equals(
                    item.attribute,
                    Constants.SETTINGS.ATTR_ROUTE_PREFERENCE
                )
            ) {
                val routePreference = item.attribute_code
                var userRoutePref: RoutePreference

                if (TextUtils.equals(
                        routePreference,
                        RoutePreference.CHEAPEST_ROUTE.type
                    )
                ) {
                    userRoutePref = RoutePreference.CHEAPEST_ROUTE
                } else {
                    userRoutePref = RoutePreference.FASTEST_ROUTE
                }
                breezeUserPreferenceUtil.saveUserRoutePreference(userRoutePref)
            } else if (TextUtils.equals(item.attribute, Constants.SETTINGS.ATTR_SHOW_DEMO)) {
                if (TextUtils.equals(item.attribute_code, "Yes")) {
                    BreezeUserPreference.getInstance(getApp())
                        .setInAppGuideTutorialPending(true)
                } else {
                    BreezeUserPreference.getInstance(getApp())
                        .setInAppGuideTutorialPending(false)
                }
            } else if (TextUtils.equals(item.attribute, Constants.SETTINGS.CARPARK_AVAILABILITY)) {
                CarParkViewOption.values().find { item.attribute_code == it.mode }?.let {
                    BreezeUserPreference.getInstance(getApp()).setStateCarpParkOption(it)
                }
            } else if (TextUtils.equals(item.attribute, Constants.SETTINGS.CARPARK_COUNT)) {
                BreezeUserPreference.getInstance(getApp())
                    .saveCarParkCount(item.attribute_code)
            } else if (TextUtils.equals(item.attribute, Constants.SETTINGS.CARPARK_DISTANCE)) {
                BreezeUserPreference.getInstance(getApp())
                    .saveCarParkDistance(item.attribute_code)
            } else if (TextUtils.equals(
                    item.attribute,
                    Constants.SHARED_PREF_KEY.KEY_PARKING_AVAILABILITY_AUDIO
                )
            ) {
                BreezeUserPreference.getInstance(getApp())
                    .saveParkingAvailabilityAudio(item.attribute_code)
            } else if (TextUtils.equals(
                    item.attribute,
                    Constants.SHARED_PREF_KEY.KEY_PARKING_AVAILABILITY_DISPLAY
                )
            ) {
                BreezeUserPreference.getInstance(getApp())
                    .saveParkingAvailabilityDisplay(item.attribute_code)
            } else if (TextUtils.equals(
                    item.attribute,
                    Constants.SHARED_PREF_KEY.KEY_ESTIMATED_TRAVEL_TIME_AUDIO
                )
            ) {
                BreezeUserPreference.getInstance(getApp())
                    .saveEstimatedTravelTimeAudio(item.attribute_code)
            } else if (TextUtils.equals(
                    item.attribute,
                    Constants.SHARED_PREF_KEY.KEY_ESTIMATED_TRAVEL_TIME_DISPLAY
                )
            ) {
                BreezeUserPreference.getInstance(getApp())
                    .saveEstimatedTravelTimeDisplay(item.attribute_code)
            } else if (TextUtils.equals(
                    item.attribute,
                    Constants.SHARED_PREF_KEY.KEY_SCHOOL_ZONE_DISPLAY
                )
            ) {

                BreezeUserPreference.getInstance(getApp())
                    .saveSchoolZoneDisplay(item.attribute_code)
            } else if (TextUtils.equals(
                    item.attribute,
                    Constants.SHARED_PREF_KEY.KEY_SCHOOL_ZONE_AUDIO
                )
            ) {
                BreezeUserPreference.getInstance(getApp())
                    .saveSchoolZoneAudio(item.attribute_code)
            } else if (TextUtils.equals(
                    item.attribute,
                    Constants.SHARED_PREF_KEY.KEY_SILVER_ZONE_DISPLAY
                )
            ) {
                BreezeUserPreference.getInstance(getApp())
                    .saveSilverZoneDisplay(item.attribute_code)
            } else if (TextUtils.equals(
                    item.attribute,
                    Constants.SHARED_PREF_KEY.KEY_SILVER_ZONE_AUDIO
                )
            ) {
                BreezeUserPreference.getInstance(getApp())
                    .saveSilverZoneAudio(item.attribute_code)
            } else if (TextUtils.equals(
                    item.attribute,
                    Constants.SHARED_PREF_KEY.KEY_BUS_LANE_DISPLAY
                )
            ) {
                BreezeUserPreference.getInstance(getApp())
                    .saveBusLaneDisplay(item.attribute_code)
            } else if (TextUtils.equals(
                    item.attribute,
                    Constants.SHARED_PREF_KEY.KEY_BUS_LANE_AUDIO
                )
            ) {
                BreezeUserPreference.getInstance(getApp())
                    .saveBusLaneAudio(item.attribute_code)
            }
        }
    }

//    private fun sendRNEvent() {
//        App.instance?.let { app ->
//            val shareBusinessLogicPackage = app.shareBusinessLogicPackage
//            shareBusinessLogicPackage.getRequestsModule().flatMap {
//                it.callRN(ShareBusinessLogicEvent.ON_FINISHED_INITIALIZING_USER_DATA.eventName, null)
//            }
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe()
//        }
//    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }
}
package com.ncs.breeze.common.utils

import android.content.Context
import android.media.AudioDeviceInfo
import android.media.AudioManager
import android.media.AudioManager.MODE_NORMAL
import android.media.MediaPlayer
import android.net.Uri
import android.os.Build
import java.io.IOException


/**
 * this class to manager play voice alert
 */
class SoundManager private constructor(private val context: Context) {


    companion object {
        @Volatile
        private var INSTANCE: SoundManager? = null

        fun getInstance(context: Context): SoundManager =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: SoundManager(context.applicationContext).also { INSTANCE = it }
            }
    }

    private val mAudioManager: AudioManager? = null
    private var mMediaPlayer: MediaPlayer? = null
    private var mAudioFocused = false

    private fun startBluetoothSco() {
        mAudioManager?.isBluetoothScoOn = true
        mAudioManager?.startBluetoothSco()
    }

    private fun stopBluetoothSco() {
        mAudioManager?.isBluetoothScoOn = false
        mAudioManager?.stopBluetoothSco()
    }

    private fun stopSound(callEnded: Boolean) {
        if (callEnded) {
            mAudioManager?.mode = MODE_NORMAL
            abandonAudioFocus()
        }
        if (mMediaPlayer != null) {
            if (mMediaPlayer?.isPlaying == true) {
                mMediaPlayer?.pause()
            }
            mMediaPlayer?.stop()
            mMediaPlayer?.reset()
            mMediaPlayer?.release()
            mMediaPlayer = null
        }
    }

    private fun requestAudioFocus(streamType: Int) {
        if (!mAudioFocused) {
            var result: Int = -1
            mAudioManager?.let {
                result = mAudioManager.requestAudioFocus(
                    null,
                    streamType,
                    AudioManager.AUDIOFOCUS_GAIN_TRANSIENT_EXCLUSIVE
                )
                if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
                    mAudioFocused = true
                }
            }
        }
    }

    private fun abandonAudioFocus() {
        if (mAudioManager != null) {
            if (mAudioFocused) {
                mAudioManager.abandonAudioFocus(null)
                mAudioFocused = false
            }
        }
    }


    fun playSound(soundUri: Uri, looping: Boolean = false, streamType: Int) {
        requestAudioFocus(AudioManager.STREAM_NOTIFICATION);
        when {
            isBluetoothOutput(context) -> {
                mAudioManager?.isSpeakerphoneOn = false;
                startBluetoothSco();
            }
            isWiredHeadsetOutput(context) -> {
                mAudioManager?.isSpeakerphoneOn = false;
            }
            else -> {
                mAudioManager?.isSpeakerphoneOn = true;
            }
        }
        playSound(soundUri, looping, streamType, null)
    }


    fun playSound(
        soundUri: Uri,
        looping: Boolean = false,
        streamType: Int,
        onCompletion: Runnable?
    ) {
        try {

            context.let {
                if (mMediaPlayer != null) {
                    if (mMediaPlayer?.isPlaying == true) {
                        mMediaPlayer?.pause()
                        mMediaPlayer?.stop()
                    }
                    mMediaPlayer?.reset()
                    mMediaPlayer?.setAudioStreamType(streamType)
                    mMediaPlayer?.setDataSource(it, soundUri)
                    mMediaPlayer?.isLooping = looping
                    mMediaPlayer?.prepare()
                    mMediaPlayer?.start()
                } else {
                    mMediaPlayer = MediaPlayer()
                    mMediaPlayer?.setAudioStreamType(streamType)
                    mMediaPlayer?.setDataSource(it, soundUri)
                    mMediaPlayer?.isLooping = looping
                    mMediaPlayer?.prepare()
                    mMediaPlayer?.start()
                }
                if (!looping && onCompletion != null) {
                    mMediaPlayer?.setOnCompletionListener { mp: MediaPlayer? -> onCompletion.run() }
                }
            }
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        } catch (e: IllegalStateException) {
            e.printStackTrace()
        } catch (e: SecurityException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }


    private fun isBluetoothOutput(context: Context?): Boolean {
        if (context == null) return false
        val am = context.getSystemService(Context.AUDIO_SERVICE) as AudioManager
        val audioDevices = am.getDevices(AudioManager.GET_DEVICES_ALL)
        for (adi in audioDevices) {
            if (adi.type == AudioDeviceInfo.TYPE_BLUETOOTH_SCO
                || adi.type == AudioDeviceInfo.TYPE_BLUETOOTH_A2DP
            ) {
                return true
            }
        }
        return false
    }

    private fun isWiredHeadsetOutput(context: Context?): Boolean {
        if (context == null) return false
        val am = context.getSystemService(Context.AUDIO_SERVICE) as AudioManager
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return am.isWiredHeadsetOn
        } else {
            val audioDevices = am.getDevices(AudioManager.GET_DEVICES_INPUTS)
            for (adi in audioDevices) {
                if (adi.type == AudioDeviceInfo.TYPE_WIRED_HEADPHONES || adi.type == AudioDeviceInfo.TYPE_WIRED_HEADSET || adi.type == AudioDeviceInfo.TYPE_USB_HEADSET || adi.type == AudioDeviceInfo.TYPE_USB_DEVICE) {
                    return true
                }
            }
        }
        return false
    }


}
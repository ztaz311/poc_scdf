package com.ncs.breeze.common.helper.obu

import androidx.collection.arraySetOf
import com.google.gson.JsonElement
import com.mapbox.navigation.core.lifecycle.MapboxNavigationApp
import com.mapbox.navigation.core.trip.session.TripSessionState
import com.ncs.breeze.App
import com.ncs.breeze.R
import com.ncs.breeze.common.constant.OBUConstants
import com.ncs.breeze.common.extensions.android.getUserDataPreference
import com.breeze.model.api.ErrorData
import com.breeze.model.obu.OBURoadEventData
import com.breeze.model.api.request.ChargingInfo
import com.breeze.model.api.request.OBUTransaction
import com.breeze.model.api.request.SaveOBUChargeInfoRequest
import com.breeze.model.api.request.SaveOBUTransactionsRequest
import com.breeze.model.api.request.SaveOBUTripSummaryRequest
import com.ncs.breeze.common.remote.ApiObserver
import com.ncs.breeze.common.rxbus.RxBus
import com.ncs.breeze.common.rxbus.RxEvent
import com.ncs.breeze.common.storage.BreezeUserPreference
import com.ncs.breeze.common.utils.LocationBreezeManager
import com.ncs.breeze.common.utils.VoiceInstructionsManager
import com.ncs.breeze.ui.dashboard.activity.DashboardActivity
import com.ncs.breeze.ui.dashboard.fragments.obulite.OBULiteFragment
import com.ncs.breeze.ui.navigation.TurnByTurnNavigationActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import timber.log.Timber
import java.lang.ref.WeakReference

class OBUEventGlobalHandler(app: App) {
    private val appRef = WeakReference(app)
    private val soundPlayer = OBUNavigationVoicePlayer(app)

    private fun canPlayVoiceInstructions(): Boolean {
        val currentActivity = appRef.get()?.getCurrentActivity()
        return currentActivity == null || (currentActivity is TurnByTurnNavigationActivity
                || (currentActivity is DashboardActivity
                && currentActivity.supportFragmentManager.findFragmentById(R.id.frame_container) is OBULiteFragment))
    }

    private val listeners = arraySetOf<Disposable>(
        RxBus.listen(RxEvent.OBURoadEvent::class.java)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { event ->
                Timber.d("OBUEventGlobalHandler OBURoadEvent : ${event.eventData}")

                playSoundRoadEventData(event.eventData)

                when (event.eventData.eventType) {
                    OBURoadEventData.EVENT_TYPE_ERP_SUCCESS,
                    OBURoadEventData.EVENT_TYPE_ERP_FAILURE,
                    OBURoadEventData.EVENT_TYPE_PARKING_FAILURE,
                    OBURoadEventData.EVENT_TYPE_PARKING_SUCCESS,
                    -> appRef.get()?.showOBUDeductionNotification(event.eventData)
                }

            },
        RxBus.listen(RxEvent.OBUParkingAvailabilityEvent::class.java)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                Timber.d("OBUEventGlobalHandler OBUParkingAvailabilityEvent: ${it.data}")
                if (!canPlayVoiceInstructions()) {
                    return@subscribe
                }

                val wantToHearAlert = appRef.get()?.applicationContext?.let { context ->
                    BreezeUserPreference.getInstance(context).getParkingAvailabilityAudio()
                }
                if ((VoiceInstructionsManager.getInstance() != null && !(VoiceInstructionsManager.getInstance()!!.isUserMute)) && wantToHearAlert != null && wantToHearAlert) {
                    soundPlayer.playAlertSound()
                    soundPlayer.playParkingAvailabilityAnnouncement(
                        it.data,
                        appRef.get()?.isConnectedToCar() == true
                    )
                }
            },
        RxBus.listen(RxEvent.OBUTravelTimeEvent::class.java)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                Timber.d("OBUEventGlobalHandler OBUTravelTimeEvent: ${it.data}")

                if (!canPlayVoiceInstructions()) {
                    return@subscribe
                }

                val wantToHearAlert = appRef.get()?.applicationContext?.let { context ->
                    BreezeUserPreference.getInstance(context).getEstimatedTravelTimeAudio()
                }
                if ((VoiceInstructionsManager.getInstance() != null && !(VoiceInstructionsManager.getInstance()!!.isUserMute)) && wantToHearAlert != null && wantToHearAlert) {
                    soundPlayer.playAlertSound()
                    soundPlayer.playTravelEstimatedTimeAnnouncement(it.data)
                }
            },
        RxBus.listen(RxEvent.OBUPaymentHistoriesEvent::class.java)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                Timber.d("OBUEventGlobalHandler OBUPaymentHistoriesEvent: ${it.paymentHistories}")
                saveOBUTransactions(it.paymentHistories)
            },
        RxBus.listen(RxEvent.OBUChargeInfoEvent::class.java)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                Timber.d("OBUEventGlobalHandler OBUChargeInfoEvent: ${it.data}")
                saveOBUChargingInfo(it.data)
            },
        RxBus.listen(RxEvent.OBUTravelSummaryEvent::class.java)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                Timber.d("OBUEventGlobalHandler OBUTravelSummaryEvent: ${it.totalTravelTime} - ${it.totalTravelDistance}")
                saveOBUTripSummary(
                    SaveOBUTripSummaryRequest(
                        totalTravelTime = it.totalTravelTime,
                        totalTravelDistance = it.totalTravelDistance
                    )
                )
            },
        RxBus.listen(RxEvent.OBUTotalTripChargedEvent::class.java)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                Timber.d("OBUEventGlobalHandler OBUTotalTripChargedEvent: ${it.totalTravelCharge}")
                saveOBUTripSummary(SaveOBUTripSummaryRequest(totalTravelCharge = it.totalTravelCharge))
            },
        RxBus.listen(RxEvent.OBUTotalTripSummaryEvent::class.java)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                Timber.d("OBUEventGlobalHandler OBUTotalTripSummaryEvent: ${it.totalTripSummary}")
                saveOBUTripSummary(it.totalTripSummary)
            },
        RxBus.listen(RxEvent.OBUCardInfoEvent::class.java)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                Timber.d("OBUCardInfoEvent: ${it.data}")
                appRef.get()?.shareBusinessLogicHelper?.updateOBUCashCardInfo(
                    it.data.status,
                    OBUDataHelper.getOBUCardBalanceSGD()
                )
                appRef.get()?.shareBusinessLogicHelper?.setNoCard(
                    OBUDataHelper.checkNoCardStatus()
                )
            }
    )

    private fun playSoundRoadEventData(data: OBURoadEventData) {

        val preference = appRef.get()?.let { context ->
            BreezeUserPreference.getInstance(context)
        }
        val isMuted = VoiceInstructionsManager.getInstance()?.isUserMute ?: false
        val canPlayVoiceInstruction = canPlayVoiceInstructions()
        var shouldPlaySound = !isMuted && canPlayVoiceInstructions()
        var shouldPlayAlertSound = false
        when (data.eventType) {
            OBURoadEventData.EVENT_TYPE_ERP_SUCCESS,
            OBURoadEventData.EVENT_TYPE_PARKING_SUCCESS,
            OBURoadEventData.EVENT_TYPE_ERP_FAILURE,
            OBURoadEventData.EVENT_TYPE_PARKING_FAILURE,
            -> {
                shouldPlayAlertSound = true
            }

            OBURoadEventData.EVENT_TYPE_PARKING_FEE,
            OBURoadEventData.EVENT_TYPE_ERP_CHARGING,
            -> {
                shouldPlayAlertSound = true
            }

            OBURoadEventData.EVENT_TYPE_SCHOOL_ZONE -> {
                shouldPlaySound = (preference?.getSchoolZoneAudio()
                    ?: true) && !isMuted && canPlayVoiceInstruction
                shouldPlayAlertSound = true

            }

            OBURoadEventData.EVENT_TYPE_SILVER_ZONE -> {
                shouldPlaySound = (preference?.getSilverZoneAudio()
                    ?: true) && !isMuted && canPlayVoiceInstruction
                shouldPlayAlertSound = true

            }

            OBURoadEventData.EVENT_TYPE_BUS_LANE -> {
                shouldPlaySound =
                    (preference?.getBusLaneAudio() ?: true) && !isMuted && canPlayVoiceInstruction
                shouldPlayAlertSound = true

            }
        }
        if (shouldPlaySound && shouldPlayAlertSound) {
            soundPlayer.playAlertSound()
        }
        if (shouldPlaySound) {
            data.spokenText?.takeIf {
                it.isNotEmpty() && MapboxNavigationApp.current()
                    ?.getTripSessionState() == TripSessionState.STARTED
            }?.let { text -> soundPlayer.playVoiceInstruction(text) }
        }

        if (shouldPlaySound && data.shouldCheckLowCardBalance()) {
            delayCheckPlayingLowCardVoiceAlert()
        }
    }

    private fun delayCheckPlayingLowCardVoiceAlert() {
        GlobalScope.launch(Dispatchers.IO) {
            delay(OBUConstants.OBU_LOW_CARD_ALERT_DELAY)
            OBUDataHelper.getOBUCardBalanceSGD()
                ?.takeIf {
                    isActive && it < OBUConstants.OBU_LOW_CARD_THRESHOLD / 100.0
                }?.run {
                    playLowCardBalanceVoiceAlert()
                }
        }
    }

    private fun playLowCardBalanceVoiceAlert() {
        if (MapboxNavigationApp.current()?.getTripSessionState() == TripSessionState.STARTED) {
            soundPlayer.playAlertSound()
            soundPlayer.playVoiceInstruction("Low card balance. Remember to top up cash card soon")
        }
    }

    init {
        listenOBUDataEvent()
    }

    private fun saveOBUTransactions(paymentHistories: List<OBUTransaction>) {
        appRef.get()?.run {
            val obuName =
                getUserDataPreference()?.getConnectedOBUDevices()?.takeIf { it.isNotEmpty() }
                    ?.last()?.obuName
            val tripGUID = breezeTripLoggingUtil.tripID
            apiHelper.saveOBUTransactions(
                SaveOBUTransactionsRequest(
                    obuName,
                    tripGUID,
                    paymentHistories
                )
            ).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : ApiObserver<JsonElement>(compositeDisposable) {
                    override fun onSuccess(data: JsonElement) {
                    }

                    override fun onError(e: ErrorData) {

                    }
                })
        }
    }

    private fun saveOBUChargingInfo(chargingInfo: List<ChargingInfo>) {
        appRef.get()?.run {
            val obuName =
                getUserDataPreference()?.getConnectedOBUDevices()?.takeIf { it.isNotEmpty() }
                    ?.last()?.obuName
            val tripGUID = breezeTripLoggingUtil.tripID
            val location = LocationBreezeManager.getInstance().currentLocation
            apiHelper.saveOBUChargeInfo(
                SaveOBUChargeInfoRequest(
                    tripGUID,
                    obuName,
                    lat = location?.latitude,
                    long = location?.longitude,
                    chargingInfo = chargingInfo
                )
            ).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : ApiObserver<JsonElement>(compositeDisposable) {
                    override fun onSuccess(data: JsonElement) {
                    }

                    override fun onError(e: ErrorData) {

                    }
                })
        }
    }

    private fun saveOBUTripSummary(request: SaveOBUTripSummaryRequest) {
        appRef.get()?.run {
            val obuName =
                getUserDataPreference()?.getConnectedOBUDevices()?.takeIf { it.isNotEmpty() }
                    ?.last()?.obuName
            val tripGUID = breezeTripLoggingUtil.tripID
            request.apply {
                vehicleNumber = obuName
                this.tripGUID = tripGUID
            }
            apiHelper.saveOBUTripSummary(
                request
            ).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : ApiObserver<JsonElement>(compositeDisposable) {
                    override fun onSuccess(data: JsonElement) {
                    }

                    override fun onError(e: ErrorData) {

                    }
                })
        }
    }

    private fun listenOBUDataEvent() {
        appRef.get()?.compositeDisposable?.run {
            listeners.forEach { listener -> add(listener) }
        }
    }
}

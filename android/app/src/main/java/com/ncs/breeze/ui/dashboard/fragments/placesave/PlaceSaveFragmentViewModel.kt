package com.ncs.breeze.ui.dashboard.fragments.placesave

import android.app.Application
import android.location.Location
import androidx.lifecycle.viewModelScope
import com.breeze.model.api.ErrorData
import com.breeze.model.api.request.AmenitiesRequest
import com.breeze.model.api.response.DeleteAllBookMarkItemResponse
import com.breeze.model.api.response.PlaceDetailsResponse
import com.breeze.model.api.response.amenities.BaseAmenity
import com.breeze.model.api.response.amenities.ResponseAmenities
import com.breeze.model.constants.AmenityType.CARPARK
import com.breeze.model.constants.Constants
import com.ncs.breeze.car.breeze.utils.BreezeCarUtil
import com.ncs.breeze.car.breeze.utils.BreezeCarUtil.apiHelper
import com.ncs.breeze.common.extensions.android.getApp
import com.ncs.breeze.common.remote.ApiHelper
import com.ncs.breeze.common.remote.ApiObserver
import com.ncs.breeze.common.storage.BreezeUserPreference
import com.ncs.breeze.components.SingleLiveEvent
import com.ncs.breeze.ui.base.BaseFragmentViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import org.json.JSONArray
import org.json.JSONObject
import timber.log.Timber
import javax.inject.Inject

class PlaceSaveFragmentViewModel @Inject constructor(
    apiHelper: ApiHelper,
    application: Application
) : BaseFragmentViewModel(
    application
) {

    var mListCarpark: SingleLiveEvent<List<BaseAmenity>> = SingleLiveEvent()
    private val _savePlaces = MutableStateFlow(listOf<PlaceDetailsResponse>())
    val savePlaces = _savePlaces.asStateFlow()

    fun resetSavePlacesData(newSavePlaces: List<PlaceDetailsResponse>) {
        viewModelScope.launch { _savePlaces.emit(newSavePlaces) }
    }

    fun deselectingBookmarkTooltip(
        amenitiesID: String,
        onSuccess: (data: DeleteAllBookMarkItemResponse) -> Unit
    ) {
        apiHelper.deselectingBookmarkTooltip(amenitiesID)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object :
                ApiObserver<DeleteAllBookMarkItemResponse>(compositeDisposable) {
                override fun onSuccess(data: DeleteAllBookMarkItemResponse) {
                    onSuccess.invoke(data)
                }

                override fun onError(e: ErrorData) {
                    Timber.i("deselectingBookmarkTooltip failed $e")
                }
            })
    }

    fun getCarParkDataShowInMap(
        locationSellected: Location,
        dest: String?,
    ) {
        viewModelScope.launch {
            val carParkRequest = AmenitiesRequest(
                lat = locationSellected.latitude,
                long = locationSellected.longitude,
                rad = BreezeUserPreference.getInstance(getApp())
                    .getCarParkDistanceSettingValue(BreezeCarUtil.masterlists),
                maxRad = Constants.PARKING_CHECK_RADIUS_MAX,
                resultCount = BreezeUserPreference.getInstance(getApp())
                    .getCarParkCountSettingValue(BreezeCarUtil.masterlists),
            ).apply {
                dest?.let { destName = it }
            }
            carParkRequest.setListType(listOf(CARPARK))
            apiHelper.getAmenities(carParkRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : ApiObserver<ResponseAmenities>(compositeDisposable) {
                    override fun onSuccess(data: ResponseAmenities) {
                        val carparks = data.amenities.find { it.type == CARPARK }?.let {
                            it.data.forEach { baseAmenity ->
                                baseAmenity.amenityType = CARPARK
                                baseAmenity.iconUrl = it.iconUrl
                            }
                            return@let it.data
                        } ?: arrayListOf()
                        mListCarpark.value = carparks
                    }

                    override fun onError(e: ErrorData) {
                        mListCarpark.value = arrayListOf()
                    }
                })
        }

    }

    operator fun JSONArray.iterator(): Iterator<JSONObject> =
        (0 until length()).asSequence().map { get(it) as JSONObject }.iterator()

}
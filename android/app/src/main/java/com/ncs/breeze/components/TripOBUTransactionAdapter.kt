package com.ncs.breeze.components

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.ncs.breeze.R
import com.breeze.model.extensions.visibleAmount
import com.breeze.model.OBUChargingMessageType
import com.breeze.model.ObuTransactionItemTypes
import com.breeze.model.obu.OBUCardStatusEventData
import com.breeze.model.api.response.TripObuTransactionItem
import com.ncs.breeze.databinding.RowTravelObuTransactionItemBinding
import com.ncs.breeze.ui.draggableview.setClickSafe
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date
import java.util.Locale
import java.util.TimeZone

class TripOBUTransactionAdapter(
    val listener: TripObuTransactionListener
) : PagingDataAdapter<TripObuTransactionItem, TripOBUTransactionAdapter.ViewHolder>(
    TRANSACTION_COMPARATOR
) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            RowTravelObuTransactionItemBinding.inflate(
                LayoutInflater.from(
                    parent.context
                ), parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        getItem(position)?.let { travelItem ->
            holder.bind(travelItem)
            holder.itemView.setClickSafe {
                listener.onItemClick(travelItem)
            }
        }
    }


    companion object {
        private val TRANSACTION_COMPARATOR =
            object : DiffUtil.ItemCallback<TripObuTransactionItem>() {
                override fun areItemsTheSame(
                    oldItem: TripObuTransactionItem,
                    newItem: TripObuTransactionItem
                ): Boolean =
                    (oldItem is TripObuTransactionItem && newItem is TripObuTransactionItem &&
                            oldItem.tripGUID == newItem.tripGUID)

                override fun areContentsTheSame(
                    oldItem: TripObuTransactionItem,
                    newItem: TripObuTransactionItem
                ): Boolean =
                    oldItem == newItem
            }
    }

    fun getAdapterItem(position: Int): TripObuTransactionItem {
        return getItem(position) as TripObuTransactionItem
    }

    override fun getItemViewType(position: Int): Int {
        return when (getItem(position)) {
            is TripObuTransactionItem -> R.layout.row_travel_log_item
            else -> throw UnsupportedOperationException("Unknown view")
        }
    }

    interface TripObuTransactionListener {
        fun onItemClick(item: TripObuTransactionItem)
    }

    inner class ViewHolder(private val binding: RowTravelObuTransactionItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        private lateinit var tripObuTransactionItem: TripObuTransactionItem

        fun bind(repo: TripObuTransactionItem?) {
            repo?.let {
                showTransactionData(it)
            }
        }

        private fun showTransactionData(travelLog: TripObuTransactionItem) {
            this.tripObuTransactionItem = travelLog
            binding.transactionName.text = tripObuTransactionItem.transactionName

            binding.travelDate.text = getDateText(tripObuTransactionItem.chargeTimeStamp)
            binding.transactionCost.text = travelLog.chargeAmount.visibleAmount()
            if (travelLog.type == ObuTransactionItemTypes.PARKING.type) {
                binding.llIcon.setImageResource(R.drawable.parking_transaction)
                val timeText = getTimeText(
                    tripObuTransactionItem.startTime ?: 0,
                    tripObuTransactionItem.endTime ?: 0
                )

                binding.travelTime.text =
                    timeText.ifBlank { getTimeText(tripObuTransactionItem.chargeTimeStamp) }
                binding.vSeparator.isInvisible = true
            } else {
                binding.llIcon.setImageResource(R.drawable.erp_transaction)
                binding.travelTime.text = getTimeText(tripObuTransactionItem.chargeTimeStamp)
                binding.vSeparator.isInvisible = false
            }

            if (travelLog.chargingMessageType == OBUChargingMessageType.DEDUCTION_FAILURE.type) {
                binding.llMessageType.isVisible = true

                binding.chargingMessageText.text = when (travelLog.cardStatus) {
                    OBUCardStatusEventData.EVENT_TYPE_CARD_INSUFFICIENT -> "Failed Deduction: Insufficient Balance"
                    OBUCardStatusEventData.EVENT_TYPE_CARD_EXPIRED -> "Failed Deduction: Card Expired"
                    OBUCardStatusEventData.EVENT_TYPE_CARD_ERROR -> "Failed Deduction: Card Error"
                    else -> "Failed Deduction: System Error"
                }
                binding.transactionCost.setTextColor(
                    ContextCompat.getColor(itemView.context, R.color.passion_pink)
                )
            } else {
                binding.llMessageType.isVisible = false
                binding.transactionCost.setTextColor(
                    ContextCompat.getColor(itemView.context, R.color.travel_log_place)
                )
            }
        }

        private fun getTimeText(time: Long): String {
            return try {
                val date = Date(time)
                val calendar = Calendar.getInstance()
                calendar.time = date
                calendar.timeZone = TimeZone.getDefault()

                val df = SimpleDateFormat("hh:mmaa", Locale.US)
                val formatTime = df.format(calendar.time)
                formatTime.lowercase()
            } catch (e: Exception) {
                ""
            }
        }

        private fun getTimeText(startTime: Long, endTime: Long): String {
            return try {
                if (startTime > 0 && endTime > 0) {
                    val startDate = Date(startTime)
                    val endDate = Date(endTime)
                    val outputSdf = SimpleDateFormat("hh:mm a", Locale.getDefault())
                    val startText = outputSdf.format(startDate).lowercase()
                    val endText = outputSdf.format(endDate).lowercase()

                    "$startText - $endText"
                } else {
                    ""
                }
            } catch (e: Exception) {
                ""
            }
        }

        private fun getDateText(time: Long): String {
            return try {
                val date = Date(time)
                val calendar = Calendar.getInstance()
                calendar.time = date
                calendar.timeZone = TimeZone.getDefault()
                val dayWeekText = SimpleDateFormat("EEE", Locale.US).format(date)

                val day = SimpleDateFormat("dd", Locale.US).format(date)
                val monthString = SimpleDateFormat("MMM", Locale.US).format(date)
                val yearString = SimpleDateFormat("YYYY", Locale.US).format(date)

                String.format("%s, %s %s, %s", dayWeekText, day, monthString, yearString)
            } catch (e: Exception) {
                ""
            }
        }
    }
}
package com.ncs.breeze.common.analytics

import android.os.Bundle
import android.text.TextUtils
import androidx.core.os.bundleOf
import com.facebook.react.bridge.ReadableMap
import com.google.firebase.analytics.FirebaseAnalytics
import com.ncs.breeze.common.analytics.data.Statistics
import timber.log.Timber
import java.lang.Integer.min
import java.time.Instant


/**
 * @author Aiswarya
 */
object Analytics {
    const val MAX_STRING_PARAM_LENGTH = 100
    private var analyticsTrackers = ArrayList<AnalyticsEventLogger>()

    private fun addAnalyticsTracker(analyticsEventLogger: AnalyticsEventLogger) {
        analyticsTrackers.add(analyticsEventLogger)
    }

    fun initialize() {
        //add or remove analytics here
        addAnalyticsTracker(FirebaseEventLogger())
//        addAnalyticsTracker(AmplifyEventLogger())

    }

    fun logScreenView(screenName: String) {
        val data = bundleOf()

        if (!TextUtils.isEmpty(screenName)) {
            data.putString(Param.KEY_SCREEN_NAME, screenName)
        }
        logEvent(FirebaseAnalytics.Event.SCREEN_VIEW, Bundle(data))
        logEvent(Event.PAGE_VIEW, Bundle(data))
    }


    fun logToggleEvent(eventValue: String, screenName: String) {
        val data = bundleOf()

        if (!TextUtils.isEmpty(eventValue)) {
            data.putString(Param.KEY_EVENT_VALUE, eventValue)
        }

        if (!TextUtils.isEmpty(screenName)) {
            data.putString(Param.KEY_SCREEN_NAME, screenName)
        }

        logEvent(Event.USER_TOGGLE, data)
    }


    fun logLocationDataEvent(
        eventValue: String, screenName: String,
        locationName: String, latitude: String, longitude: String
    ) {
        val data = bundleOf()

        if (!TextUtils.isEmpty(eventValue)) {
            data.putString(Param.KEY_EVENT_VALUE, eventValue)
        }

        if (!TextUtils.isEmpty(screenName)) {
            data.putString(Param.KEY_SCREEN_NAME, screenName)
        }

        if (!TextUtils.isEmpty(latitude)) {
            data.putString(Param.KEY_LATITUDE, latitude)
        }

        if (!TextUtils.isEmpty(longitude)) {
            data.putString(Param.KEY_LONGITUDE, longitude)
        }

        if (!TextUtils.isEmpty(locationName)) {
            data.putString(Param.KEY_LOCATION_NAME, locationName)
        }

        logEvent(Event.LOCATION_DATA, data)
    }

    fun logClickEvent(eventValue: String, screenName: String, extraData: Bundle) {
        val data = bundleOf()

        if (!TextUtils.isEmpty(eventValue)) {
            data.putString(Param.KEY_EVENT_VALUE, eventValue)
        }

        if (!TextUtils.isEmpty(screenName)) {
            data.putString(Param.KEY_SCREEN_NAME, screenName)
        }

        data.putAll(extraData)
        logEvent(Event.USER_CLICK, data)
    }

    fun logClickEvent(eventValue: String, screenName: String, message: String = "") {
        val data = bundleOf()
        if (!TextUtils.isEmpty(message)) {
            data.putString(Param.KEY_MESSAGE, message)
        }
        logClickEvent(eventValue, screenName, data)
    }

    fun logOpenPopupEvent(
        popupName: String,
        screenName: String,
        extraData: Bundle = bundleOf()
    ) {
        val data = bundleOf(
            Param.KEY_EVENT_VALUE to Event.VALUE_POPUP_OPEN
        )

        if (!TextUtils.isEmpty(popupName)) {
            data.putString(Param.KEY_POPUP_NAME, popupName)
        }

        if (!TextUtils.isEmpty(screenName)) {
            data.putString(Param.KEY_SCREEN_NAME, screenName)
        }
        data.putAll(extraData)
        logEvent(Event.USER_POPUP, data)
    }

    fun logClosePopupEvent(
        popupName: String,
        screenName: String,
        extraData: Bundle = bundleOf()
    ) {
        val data = bundleOf()
        data.putString(Param.KEY_EVENT_VALUE, Event.POPUP_CLOSE)

        if (!TextUtils.isEmpty(popupName)) {
            data.putString(Param.KEY_POPUP_NAME, popupName)
        }

        if (!TextUtils.isEmpty(screenName)) {
            data.putString(Param.KEY_SCREEN_NAME, screenName)
        }
        data.putAll(extraData)
        logEvent(Event.USER_POPUP, data)
    }

    fun logOriginAlertPopupEvent(popupName: String, popupValue: String, screenName: String) {
        val data = bundleOf(
            Param.KEY_EVENT_VALUE to popupValue
        )

        if (!TextUtils.isEmpty(popupName)) {
            data.putString(Param.KEY_POPUP_NAME, popupName)
        }

        if (!TextUtils.isEmpty(screenName)) {
            data.putString(Param.KEY_SCREEN_NAME, screenName)
        }
        logEvent(Event.USER_POPUP, data)
    }

    fun logPopupEvent(
        popupName: String,
        eventValue: String,
        screenName: String,
        message: String = ""
    ) {
        val data = bundleOf()
        if (!TextUtils.isEmpty(eventValue)) {
            data.putString(Param.KEY_EVENT_VALUE, eventValue)
        }

        if (!TextUtils.isEmpty(popupName)) {
            data.putString(Param.KEY_POPUP_NAME, popupName)
        }

        if (!TextUtils.isEmpty(screenName)) {
            data.putString(Param.KEY_SCREEN_NAME, screenName)
        }

        if (!TextUtils.isEmpty(message)) {
            data.putString(Param.KEY_MESSAGE, message)
        }
        logEvent(Event.USER_POPUP, data)
    }

    fun logMapInteractionEvents(eventValue: String, screenName: String) {
        val data = bundleOf()

        if (!TextUtils.isEmpty(eventValue)) {
            data.putString(Param.KEY_EVENT_VALUE, eventValue)
        }

        if (!TextUtils.isEmpty(screenName)) {
            data.putString(Param.KEY_SCREEN_NAME, screenName)
        }

        logEvent(Event.MAP_INTERACTION, data)
    }

    fun logNotificationEvents(popupName: String, eventValue: String, screenName: String) {
        val data = bundleOf()

        if (!TextUtils.isEmpty(eventValue)) {
            data.putString(Param.KEY_EVENT_VALUE, eventValue)
        }

        if (!TextUtils.isEmpty(popupName)) {
            data.putString(Param.KEY_POPUP_NAME, popupName)
        }

        if (!TextUtils.isEmpty(screenName)) {
            data.putString(Param.KEY_SCREEN_NAME, screenName)
        }

        logEvent(Event.USER_NOTIFICATION, data)
    }


    fun logEditEvent(eventValue: String, screenName: String) {
        val data = bundleOf()

        if (!TextUtils.isEmpty(eventValue)) {
            data.putString(Param.KEY_EVENT_VALUE, eventValue)
        }

        if (!TextUtils.isEmpty(screenName)) {
            data.putString(Param.KEY_SCREEN_NAME, screenName)
        }

        logEvent(Event.USER_EDIT, data)
    }

    fun logEvent(
        eventName: String,
        eventValue: String,
        screenName: String,
        extraData: Bundle = bundleOf()
    ) {
        val payload = bundleOf()
        if (!TextUtils.isEmpty(eventValue)) {
            payload.putString(Param.KEY_EVENT_VALUE, eventValue)
        }

        if (!TextUtils.isEmpty(screenName)) {
            payload.putString(Param.KEY_SCREEN_NAME, screenName)
        }
        payload.putAll(extraData)
        logEvent(eventName, payload)
    }

    fun logAnalyticsFromRN(
        eventName: String,
        eventValue: String,
        screenName: String,
        extraData: ReadableMap? = null
    ) {
        val payload = bundleOf()
        if (!TextUtils.isEmpty(eventValue)) {
            payload.putString(Param.KEY_EVENT_VALUE, eventValue)
        }

        if (!TextUtils.isEmpty(screenName)) {
            payload.putString(Param.KEY_SCREEN_NAME, screenName)
        }
        extraData?.toHashMap()?.entries?.forEach {
            when (val value = it.value) {
                is String -> payload.putString(it.key, value)
                is Int -> payload.putInt(it.key, value)
                is Float -> payload.putFloat(it.key, value)
                is Double -> payload.putDouble(it.key, value)
                is Boolean -> payload.putBoolean(it.key, value)
            }
        }
        logEvent(eventName, payload)
    }

    fun logEvent(name: String, payload: Bundle) {
        Timber.d("Analytics-eventName: $name, data: $payload")
        payload.getString(Param.KEY_MESSAGE)
            ?.takeIf { it.isNotEmpty() }
            ?.let { message ->
                payload.apply {
                    putString(
                        Param.KEY_MESSAGE,
                        message.substring(0, min(message.length, MAX_STRING_PARAM_LENGTH))
                    )
                }
            }

        for (tracker in analyticsTrackers) {
            tracker.logEvent(
                name,
                payload
            )
        }
    }


    fun sendLoginEvent(method: String) {
        val data = bundleOf()
        if (!TextUtils.isEmpty(method)) {
            data.putString(FirebaseAnalytics.Param.METHOD, method)
        }
        logEvent(FirebaseAnalytics.Event.LOGIN, bundleOf().apply {
            if (method.isNotEmpty())
                putString(FirebaseAnalytics.Param.METHOD, method)
        })
    }

    fun logStatisticsDataStart(statistics: Statistics, screenName: String) {
        val data = bundleOf(
            Param.KEY_BATTERY_LEVEL to statistics.batteryLevel.toString(),
            Param.KEY_DATA_RECEIVED to statistics.dataReceived.toString(),
            Param.KEY_DATA_SENT to statistics.dataSent.toString(),
            Param.KEY_EVENT_VALUE to Event.START,
            Param.KEY_EVENT_TIMESTAMP to Instant.ofEpochMilli(System.currentTimeMillis())
                .toString(),
        )
        if (!TextUtils.isEmpty(screenName)) {
            data.putString(Param.KEY_SCREEN_NAME, screenName)
        }
        logEvent(Event.STATISTIC_DATA, data)
    }

    fun logStatisticsDataEnd(statistics: Statistics, screenName: String) {
        val payload = bundleOf(
            Param.KEY_BATTERY_LEVEL to statistics.batteryLevel.toString(),
            Param.KEY_DATA_SENT to statistics.dataSent.toString(),
            Param.KEY_DATA_RECEIVED to statistics.dataReceived.toString(),
            Param.KEY_EVENT_VALUE to Event.END,
        )

        if (!TextUtils.isEmpty(screenName)) {
            payload.putString(Param.KEY_SCREEN_NAME, screenName)
        }

        logEvent(Event.STATISTIC_DATA, payload)
    }

    fun logNotificationStatistics(
        screenName: String,
        dateClicked: String,
        datePushed: String,
        message: String,
        category: String
    ) {
        val data = bundleOf()
        if (!TextUtils.isEmpty(screenName)) {
            data.putString(Param.KEY_SCREEN_NAME, screenName)
        }
        if (!TextUtils.isEmpty(dateClicked)) {
            data.putString(Param.KEY_DATE_CLICKED, dateClicked)
        }
        if (!TextUtils.isEmpty(datePushed)) {
            data.putString(Param.KEY_DATE_PUSHED, datePushed)
        }
        if (!TextUtils.isEmpty(message)) {
            data.putString(Param.KEY_MESSAGE, message)
        }
        if (!TextUtils.isEmpty(category)) {
            data.putString(Param.KEY_MAIN_CATEGORY, category)
        }
        logEvent(Event.STATISTIC, data)
    }

    fun setUserId(cognitoUserName: String) {
        for (tracker in analyticsTrackers) {
            tracker.setUserId(cognitoUserName)
        }
    }

    fun logExceptionEvent(message: String) {
        logEvent(
            Event.EXCEPTION_HANDLER,
            bundleOf(
                Param.KEY_MESSAGE to message.substring(
                    0,
                    min(message.length, MAX_STRING_PARAM_LENGTH)
                )
            )
        )
    }
}
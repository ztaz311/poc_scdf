package com.ncs.breeze.components

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.StyleRes
import androidx.annotation.UiThread
import androidx.appcompat.view.ContextThemeWrapper
import androidx.core.content.ContextCompat
import androidx.core.graphics.BlendModeColorFilterCompat
import androidx.core.graphics.BlendModeCompat
import androidx.recyclerview.widget.RecyclerView
import androidx.vectordrawable.graphics.drawable.VectorDrawableCompat
import com.mapbox.navigation.ui.maneuver.api.MapboxLaneIconsApi
import com.mapbox.navigation.ui.maneuver.databinding.MapboxItemLaneGuidanceLayoutBinding
import com.mapbox.navigation.ui.maneuver.model.LaneIndicator
import com.ncs.breeze.R

/**
 * Default recycler adapter to render lanes for the upcoming turn.
 * @property context Context
 * @property inflater (android.view.LayoutInflater..android.view.LayoutInflater?)
 * @property laneIndicatorList MutableList<LaneIndicator>
 * @constructor
 */
@UiThread
class BreezeLaneGuidanceAdapter(
    private val context: Context,
) : RecyclerView.Adapter<BreezeLaneGuidanceAdapter.BreezeLaneGuidanceViewHolder>() {

    private val inflater = LayoutInflater.from(context)
    private val laneIndicatorList = mutableListOf<LaneIndicator>()
    private val laneApi = MapboxLaneIconsApi()
    private var wrapper = ContextThemeWrapper(context, R.style.BreezeMapboxStyleTurnIconManeuver)

    /**
     * Binds the given View to the position.
     * @param parent ViewGroup
     * @param viewType Int
     * @return BreezeLaneGuidanceViewHolder
     */
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BreezeLaneGuidanceViewHolder {
        val binding = MapboxItemLaneGuidanceLayoutBinding.inflate(inflater, parent, false)
        return BreezeLaneGuidanceViewHolder(binding)
    }

    /**
     * Returns the total number of items in the data set held by the adapter.
     * @return Int
     */
    override fun getItemCount(): Int {
        return laneIndicatorList.size
    }

    /**
     * Invoked by RecyclerView to display the data at the specified position.
     * @param holder BreezeLaneGuidanceViewHolder
     * @param position Int
     */
    override fun onBindViewHolder(holder: BreezeLaneGuidanceViewHolder, position: Int) {
        val laneIndicator = laneIndicatorList[position]
        holder.bindLaneIndicator(laneIndicator)
    }

    /**
     * Invoke to add all the lanes to the recycler view.
     * @param laneIndicatorList List<LaneIndicator>
     */
    fun addLanes(laneIndicatorList: List<LaneIndicator>) {
        if (laneIndicatorList.isNotEmpty()) {
            this.laneIndicatorList.clear()
            this.laneIndicatorList.addAll(laneIndicatorList)
            notifyDataSetChanged()
        }
    }

    /**
     * Invoke to remove all the lanes from the recycler view.
     */
    fun removeLanes() {
        if (this.laneIndicatorList.isNotEmpty()) {
            this.laneIndicatorList.clear()
            notifyDataSetChanged()
        }
    }

    /**
     * Invoke to change how the turn icons would look in the lane guidance view.
     * @param style Int
     */
    @SuppressLint("NotifyDataSetChanged")
    fun updateStyle(@StyleRes style: Int) {
        this.wrapper = ContextThemeWrapper(context, style)
        notifyDataSetChanged()
    }

    /**
     * View Holder defined for the [RecyclerView.Adapter]
     * @property viewBinding
     * @constructor
     */
    inner class BreezeLaneGuidanceViewHolder(
        private val viewBinding: MapboxItemLaneGuidanceLayoutBinding
    ) : RecyclerView.ViewHolder(viewBinding.root) {

        /**
         * Invoke the method to bind the lane to the view.
         * @param laneIndicator LaneIndicator
         */
        @UiThread
        fun bindLaneIndicator(laneIndicator: LaneIndicator) {
            val laneIcon = laneApi.getTurnLane(laneIndicator)
            viewBinding.itemLaneGuidance.run {
                rotationY = if (laneIcon.shouldFlip) {
                    180f
                } else {
                    0f
                }
                val drawable = VectorDrawableCompat.create(
                    context.resources,
                    laneIcon.drawableResId,
                    wrapper.theme
                )?.also {
                    it.colorFilter = BlendModeColorFilterCompat.createBlendModeColorFilterCompat(
                        ContextCompat.getColor(
                            context,
                            if (laneIndicator.isActive)
                                R.color.white
                            else R.color.mapbox_turn_icon_shadow_color
                        ), BlendModeCompat.SRC_IN
                    )
                }
                setImageDrawable(drawable)
            }
        }
    }
}

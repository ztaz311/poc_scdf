package com.ncs.breeze.common.utils

import android.content.Context
import android.content.res.AssetManager
import android.media.AudioAttributes
import android.media.MediaPlayer
import android.util.Log
import java.io.IOException

object BreezeAudioUtils {

    fun playOverSpeedAlert(context: Context?, playCount: Int = 1) {
        context?.let { ctx ->
            var count: Int = 1
            val am: AssetManager
            var player: MediaPlayer? = null
            try {
                am = ctx.assets
                val afd = am.openFd("navigation_forward_selection.wav")
                player = MediaPlayer()
                player.setAudioAttributes(
                    AudioAttributes.Builder()
                        .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                        .build()
                )
                player.setDataSource(afd.fileDescriptor, afd.startOffset, afd.length)
                player.prepare()
                player.start()
                player.setOnCompletionListener(MediaPlayer.OnCompletionListener { mp ->
                    if (count < playCount) {
                        count++
                        player.seekTo(0)
                        player.start()
                    } else {
                        mp.release()
                    }
                })
                player.setLooping(false)
            } catch (e: IOException) {
                Log.e(
                    "AUDIO",
                    "IOException while playing speed limit exceeded sound: " + e.toString()
                )
                e.printStackTrace()
            }
        }
    }

}
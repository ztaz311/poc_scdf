package com.ncs.breeze.ui.dashboard.fragments.exploremap

import com.breeze.model.api.response.amenities.BaseAmenity

data class ContentWrapperDataPOI(
    val contentDetailKey: ContentDetailKey,
    val listAmenities: List<BaseAmenity>
)

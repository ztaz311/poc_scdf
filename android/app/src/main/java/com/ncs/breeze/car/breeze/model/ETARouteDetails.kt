package com.ncs.breeze.car.breeze.model

import com.breeze.model.DestinationAddressDetails
import com.mapbox.api.directions.v5.models.DirectionsRoute

data class ETARouteDetails(
    val searchLocation: DestinationAddressDetails,
    val route: DirectionsRoute
)

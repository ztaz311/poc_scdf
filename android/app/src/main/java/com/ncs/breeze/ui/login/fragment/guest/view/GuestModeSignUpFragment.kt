package com.ncs.breeze.ui.login.fragment.guest.view

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import com.breeze.model.constants.INTENT_KEY
import com.breeze.model.constants.MODE_LOGIN_TYPE
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.Event
import com.ncs.breeze.common.analytics.Screen
import com.ncs.breeze.common.storage.BreezeUserPreference
import com.ncs.breeze.common.utils.InitObjectUtilsController
import com.ncs.breeze.components.BottomSheetWarningWithoutAccount
import com.ncs.breeze.databinding.FragmentSignupGuestBinding
import com.ncs.breeze.ui.base.BaseFragment
import com.ncs.breeze.ui.login.activity.LoginActivity
import com.ncs.breeze.ui.login.fragment.guest.viewmodel.GuestModeSignUpViewModel
import com.ncs.breeze.ui.login.fragment.signUp.view.TermsAndCondnFragment
import com.ncs.breeze.ui.utils.DialogUtilsBreeze
import javax.inject.Inject

class GuestModeSignUpFragment :
    BaseFragment<FragmentSignupGuestBinding, GuestModeSignUpViewModel>() {

    var isAutoSignInGuestMode: Boolean = false

    var isOpenGuestWarningFromCreateAccount: Boolean = false


    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val viewModel: GuestModeSignUpViewModel by viewModels {
        viewModelFactory
    }

    companion object {

        val SCREEN_NAME = "GUEST_MODE_SIGNUP_FRAGMENT"

        @JvmStatic
        fun newInstance() =
            GuestModeSignUpFragment().apply {
                arguments = Bundle().apply {

                }
            }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            isAutoSignInGuestMode =
                if (requireArguments().containsKey(INTENT_KEY.AUTO_SIGN_IN_GEUST_MODE)) requireArguments().getBoolean(
                    INTENT_KEY.AUTO_SIGN_IN_GEUST_MODE,
                    false
                ) else false

            isOpenGuestWarningFromCreateAccount =
                if (requireArguments().containsKey(INTENT_KEY.IS_OPEN_GUEST_FROM_CREATE_ACCOUNT_SCREEN)) requireArguments().getBoolean(
                    INTENT_KEY.IS_OPEN_GUEST_FROM_CREATE_ACCOUNT_SCREEN,
                    false
                ) else false
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initialize()
    }

    private fun initialize() {
        viewBinding.ClRootSignUpGuest.setOnTouchListener { _, _ ->
            viewBinding.bottomSheetWarningProcess.gone()
            onBackPressed()
            true
        }
        viewBinding.bottomSheetWarningProcess.show()
        viewBinding.bottomSheetWarningProcess.setListener(object :
            BottomSheetWarningWithoutAccount.ListenerWarningWithoutAccount {
            override fun close() {
                onBackPressed()
                Analytics.logClickEvent(Event.GUEST_CONTINUE_CLOSE, Screen.ONBOARDING_GUEST_SIGNUP)
            }

            override fun later() {
                onBackPressed()
                Analytics.logClickEvent(Event.GUEST_CONTINUE_CANCEL, Screen.ONBOARDING_GUEST_SIGNUP)
            }

            override fun process() {
                /**
                 * set mode sign up to guest
                 */
                InitObjectUtilsController.currentObjectFlowLoginSignUp?.mode =
                    MODE_LOGIN_TYPE.GUEST_MODE_CREATE_ACCOUNT
                viewBinding.ClRootSignUpGuest.setBackgroundColor(Color.TRANSPARENT)
                startCreateGuestAccount()
                Analytics.logClickEvent(
                    Event.GUEST_CONTINUE_PROCEED,
                    Screen.ONBOARDING_GUEST_SIGNUP
                )
            }
        })

        viewModel.errorThrow.observe(viewLifecycleOwner) {
            activity?.let {
                DialogUtilsBreeze.showDialogGetUserFails(it, retryAction = {
                    viewModel.restartSignUpWithGuestMode()
                }, cancel = {
                    viewBinding.progressBar.visibility = View.GONE
                    onBackPressed()
                })
            }
        }

        /**
         * if auto sign in guest mode
         */
        if (isAutoSignInGuestMode) {
            viewBinding.bottomSheetWarningProcess.visibility = View.GONE
            viewBinding.progressBar.visibility = View.GONE
            viewBinding.ClRootSignUpGuest.setBackgroundColor(Color.TRANSPARENT)

            /**
             * call function to auto sign in
             */
            autoSignInGuestMode()
        }
    }


    private fun autoSignInGuestMode() {
        viewModel.shouldNotShowTAndC.observe(viewLifecycleOwner) { isShouldNotShowTerm ->
            if (isShouldNotShowTerm) {
                /**
                 * check if user not have username if not open create user name screen
                 */
                if (InitObjectUtilsController.currentObjectFlowLoginSignUp?.isUserNameAvailable == false) {
                    /**
                     * open create user name screen
                     */
                    (activity as? LoginActivity)?.openCreateUserNameScreen(TermsAndCondnFragment.FROM_GUEST)
                } else {
                    /**
                     * show dialog welcome
                     */
                    (activity as? LoginActivity)?.showDialogSuccess(
                        isExistUser = true,
                        TermsAndCondnFragment.FROM_GUEST
                    )
                }
            } else {
                /**
                 * show term of condition
                 */
                when (activity) {
                    is LoginActivity -> {
                        /**
                         * open create user name screen
                         */
                        (activity as? LoginActivity)?.openTermAndConDnFragment(TermsAndCondnFragment.FROM_GUEST)
                    }
                }

            }
        }

        viewBinding.progressBar.visibility = View.VISIBLE

        viewModel.signUpSuccess.observe(viewLifecycleOwner) {
            viewModel.signUpSuccess.removeObservers(viewLifecycleOwner)
            viewBinding.progressBar.visibility = View.GONE
            /**
             * check term of condition
             */
            viewModel.checkTermOfCondition()

        }
        /**
         * start process create guest account
         */
        viewModel.autoSignInGuestMode()


    }

    private fun startCreateGuestAccount() {
        viewModel.shouldNotShowTAndC.observe(viewLifecycleOwner) { isShouldNotShowTerm ->
            if (isShouldNotShowTerm) {
                /**
                 * check if user not have username if not open create user name screen
                 */
                if (InitObjectUtilsController.currentObjectFlowLoginSignUp?.isUserNameAvailable == false) {
                    /**
                     * open create user name screen
                     */
                    (activity as? LoginActivity)?.openCreateUserNameScreen(TermsAndCondnFragment.FROM_GUEST)
                } else {
                    /**
                     * show dialog welcome
                     */
                    (activity as? LoginActivity)?.showDialogSuccess(
                        isExistUser = false,
                        TermsAndCondnFragment.FROM_GUEST
                    )
                }
            } else {
                /**
                 * show term of condition
                 */
                (activity as? LoginActivity)?.openTermAndConDnFragment(TermsAndCondnFragment.FROM_GUEST)
            }
        }


        //show loading
        viewBinding.progressBar.visibility = View.VISIBLE

        viewModel.signUpSuccess.observe(viewLifecycleOwner) { isCreateGuestSuccess ->
            viewModel.signUpSuccess.removeObservers(viewLifecycleOwner)
            if (isCreateGuestSuccess) {

                /**
                 * if start guest from create account page => we already show
                 */
                if (isOpenGuestWarningFromCreateAccount) {
                    if (InitObjectUtilsController.currentObjectFlowLoginSignUp?.mode == MODE_LOGIN_TYPE.GUEST_MODE_CREATE_ACCOUNT
                        && InitObjectUtilsController.currentObjectFlowLoginSignUp?.isUserNameAvailable == false
                    ) {
                        /**
                         * open create user name screen
                         */
                        (activity as? LoginActivity)?.openCreateUserNameScreen(TermsAndCondnFragment.FROM_GUEST)

                        /**
                         * update term of condition right now
                         */
                        BreezeUserPreference.getInstance(requireContext()).saveTncStatus(true)
                        viewModel.updateTncStatus()
                    }
                } else {
                    /**
                     * check term of condition
                     */
                    viewModel.checkTermOfCondition()
                }
            }
        }
        /**
         * start process create guest account
         */
        viewModel.genIDGuest()
    }

    override fun inflateViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?,
        attachToContainer: Boolean
    ) = FragmentSignupGuestBinding.inflate(inflater, container, attachToContainer)

    override fun getViewModelReference(): GuestModeSignUpViewModel {
        return viewModel
    }

    override fun getScreenName(): String {
        return Screen.ONBOARDING_GUEST_SIGNUP
    }
}
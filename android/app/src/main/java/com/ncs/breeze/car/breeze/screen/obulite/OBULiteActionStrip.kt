package com.ncs.breeze.car.breeze.screen.obulite

import android.annotation.SuppressLint
import androidx.car.app.ScreenManager
import androidx.car.app.model.Action
import androidx.car.app.model.ActionStrip
import androidx.car.app.model.CarIcon
import androidx.core.graphics.drawable.IconCompat
import com.breeze.model.extensions.round
import com.ncs.breeze.R
import com.ncs.breeze.car.breeze.MainBreezeCarContext
import com.ncs.breeze.car.breeze.screen.carkpark.CarParkCarContext
import com.ncs.breeze.car.breeze.screen.carkpark.CarParkScreen
import com.ncs.breeze.car.breeze.screen.obustatus.OBUDeviceConnectionStatusScreen
import com.ncs.breeze.car.breeze.screen.search.recentsearch.RecentSearchScreen
import com.ncs.breeze.car.breeze.screen.search.searchoption.SearchOptionScreen
import com.ncs.breeze.car.breeze.utils.setClickSafe
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.Event
import com.ncs.breeze.common.analytics.Screen
import com.ncs.breeze.common.event.ToAppRx
import com.ncs.breeze.common.model.rx.AppToPhoneCarParkEvent
import com.ncs.breeze.common.utils.OBUStripStateManager
import java.lang.ref.WeakReference

class OBULiteActionStrip(
    mainCarContext: MainBreezeCarContext,
) {
    private val mainCarContextRef = WeakReference(mainCarContext)
    private var obuConnected = false
    private var cardBalance: Double? = null

    /**
     * Build the action strip
     */
    fun builder(connected: Boolean, cardBalance: Double?): ActionStrip.Builder {
        this.obuConnected = connected
        this.cardBalance = cardBalance
        val actionBuilder = ActionStrip.Builder()
        buildSearchAction()?.let {
            actionBuilder.addAction(it)
        }
        buildCarPackAction()?.let {
            actionBuilder.addAction(it)
        }
        actionBuilder.addAction(buildOBUAction())
        actionBuilder.addAction(buildCloseAction())
        return actionBuilder
    }

    @SuppressLint("UnsafeOptInUsageError")
    private fun buildOBUAction(): Action {
        val actionBuilder = Action.Builder()
            .setFlags(Action.FLAG_IS_PERSISTENT)
            .setTitle(
                if (obuConnected)
                    "$${cardBalance?.round(2) ?: "-"}"
                else "OBU"
            )
            .setClickSafe {
                mainCarContextRef.get()?.let { mainCarContext ->
                    mainCarContext.carContext.getCarService(ScreenManager::class.java).push(
                        OBUDeviceConnectionStatusScreen(
                            mainCarContext,
                            fromScreen = Screen.ANDROID_AUTO_CRUISE
                        )
                    )
                    if (!obuConnected)
                        Analytics.logClickEvent(Event.AA_OBU_X, Screen.ANDROID_AUTO_CRUISE)
                }
            }
        return actionBuilder.build()
    }

    @SuppressLint("UnsafeOptInUsageError")
    private fun buildCloseAction(): Action {
        val actionBuilder = Action.Builder()
            .setFlags(Action.FLAG_IS_PERSISTENT)
            .setClickSafe {
                Analytics.logClickEvent(Event.AA_END, Screen.ANDROID_AUTO_CRUISE)
                OBUStripStateManager.getInstance()?.closeFromCar()
                CarOBULiteManager.closePhoneOBUScreen()
            }
        mainCarContextRef.get()?.carContext?.let { carContext ->
            actionBuilder.setIcon(
                CarIcon.Builder(
                    IconCompat.createWithResource(
                        carContext,
                        R.drawable.ic_aa_close
                    )
                ).build()
            )
        }
        return actionBuilder.build()
    }

    @SuppressLint("UnsafeOptInUsageError")
    private fun buildSearchAction(): Action? {
        return mainCarContextRef.get()?.carContext?.let { carContext ->
            Action.Builder()
                .setFlags(Action.FLAG_IS_PERSISTENT)
                .setIcon(
                CarIcon.Builder(
                    IconCompat.createWithResource(
                        carContext, R.drawable.ic_search_black36dp
                    )
                ).build()
            ).setClickSafe {
                val screenManager = carContext.getCarService(ScreenManager::class.java)
                mainCarContextRef.get()?.let {
                    screenManager.push(SearchOptionScreen(it, Screen.ANDROID_AUTO_CRUISE))
                }
                /**
                 * post event open search in app
                 */
                //ToAppRx.postEventToApp(OpenSearchEvent(DataSearchEvent()))
            }.build()
        }
    }

    @SuppressLint("UnsafeOptInUsageError")
    private fun buildCarPackAction(): Action? {
        return mainCarContextRef.get()?.carContext?.let { carContext ->
            val carIconBuilder = CarIcon.Builder(
                IconCompat.createWithResource(
                    carContext, R.drawable.ic_icon_car_pack
                )
            )
            Action.Builder()
                .setFlags(Action.FLAG_IS_PERSISTENT)
                .setIcon(carIconBuilder.build())
                .setClickSafe {
                    val screenManager = carContext.getCarService(ScreenManager::class.java)
                    mainCarContextRef.get()?.let {
                        screenManager.push(
                            CarParkScreen(
                                CarParkCarContext(it),
                                originalDestination = null,
                                fromScreen = Screen.ANDROID_AUTO_CRUISE
                            )
                        )
                    }
                    ToAppRx.postEvent(AppToPhoneCarParkEvent())

//
//                Analytics.logClickEvent(Event.SHOW_CARPARKS, Screen.ANDROID_AUTO_HOMEPAGE)
//                Analytics.logClickEvent(Event.AA_DISPLAY_CARPARK, Screen.AA_HOMEPAGE_SCREEN)

                }
                .build()
        }
    }
}

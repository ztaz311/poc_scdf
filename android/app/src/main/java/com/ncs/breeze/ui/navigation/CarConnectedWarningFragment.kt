package com.ncs.breeze.ui.navigation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.Event
import com.ncs.breeze.common.analytics.Screen
import com.ncs.breeze.common.constant.OBUConstants
import com.ncs.breeze.databinding.FragmentCarConnectedWarningBinding
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.util.concurrent.CancellationException


class CarConnectedWarningFragment : Fragment() {
    private lateinit var viewBinding: FragmentCarConnectedWarningBinding

    private var autoHideJob: Job? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewBinding = FragmentCarConnectedWarningBinding.inflate(inflater, container, false)
        return viewBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initClickListeners()
    }

    private fun initViews() {
        viewBinding.root.isVisible = !(arguments?.getBoolean(ARG_DEFAULT_HIDE_SCREEN) ?: false)
        viewBinding.btnDismiss.isVisible = arguments?.getBoolean(ARG_DISMISSIBLE) ?: true
    }

    private fun initClickListeners() {
        viewBinding.btnDismiss.setOnClickListener {
            cancelAutoHideJob("User dismissed")
            viewBinding.root.isVisible = false
            Analytics.logClickEvent(
                Event.OBU_DHU_PROJECTED_OK,
                Screen.NAVIGATION)
        }
    }

    private fun cancelAutoHideJob(reason: String? = null) {
        if (autoHideJob?.isActive == true) {
            autoHideJob?.cancel(if (reason != null) CancellationException(reason) else null)
        }
        autoHideJob = null
    }

    fun showAutoHideWarningScreen() {
        cancelAutoHideJob("Trigger new autoHide")
        autoHideJob = lifecycleScope.launch {
            viewBinding.root.isVisible = true
            delay(OBUConstants.OBU_LOW_CARD_ALERT_DELAY)
            viewBinding.root.isVisible = false
        }
    }

    companion object {
        const val TAG = "CarConnectedWarningFragment"
        private const val ARG_DEFAULT_HIDE_SCREEN = "$TAG.ARG_DEFAULT_HIDE_SCREEN"
        private const val ARG_DISMISSIBLE = "$TAG.ARG_DISMISSIBLE"

        fun newInstance(isDefaultHideScreen: Boolean = false, isDismissible: Boolean = true) =
            CarConnectedWarningFragment().apply {
                arguments = bundleOf(
                    ARG_DEFAULT_HIDE_SCREEN to isDefaultHideScreen,
                    ARG_DISMISSIBLE to isDismissible,
                )
            }
    }
}
package com.ncs.breeze.components.marker.markerview2

import android.app.Activity
import android.content.Context
import android.content.res.Resources
import android.view.LayoutInflater
import android.view.View
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.breeze.model.api.response.amenities.BaseAmenity
import com.breeze.model.constants.AmenityType.EVCHARGER
import com.breeze.model.extensions.dpToPx
import com.ncs.breeze.common.utils.BreezeMapDataHolder
import com.ncs.breeze.common.utils.BreezeMapZoomUtil
import com.google.android.flexbox.*
import com.google.android.gms.maps.model.LatLng
import com.mapbox.geojson.Point
import com.mapbox.maps.EdgeInsets
import com.mapbox.maps.MapboxMap
import com.ncs.breeze.R
import com.ncs.breeze.databinding.MapTooltipEvBinding
import com.ncs.breeze.ui.base.BaseActivity
import com.ncs.breeze.ui.dashboard.fragments.adapter.AmenityItemToolTipAdapter

class EVMarkerView(
    latLng: LatLng,
    mapboxMap: MapboxMap,
    val activity: Activity,
    itemId: String? = null,
    val limitExceeded: Boolean
) : MarkerView2(mapboxMap = mapboxMap, itemId) {

    private var viewBinding =
        MapTooltipEvBinding.inflate(LayoutInflater.from(activity), null, false)

    init {
        mLatLng = latLng
        mViewMarker = viewBinding.root
    }

    private var currentAmenities: BaseAmenity? = null

    fun handleMarkerClick(
        isSelected: Boolean,
        item: BaseAmenity,
        addStopClick: (BaseAmenity, BaseActivity.EventSource) -> Boolean,
        removeStopClick: (BaseAmenity, BaseActivity.EventSource) -> Boolean,
        markerClick: () -> Unit,
    ) {
        currentAmenities = item
        viewBinding.tvProvider.text = item.provider

        addRecycleView(item)

        viewBinding.buttonToggleStop.setOnClickListener {
            resetView()
            if (isSelected) {
                if (removeStopClick.invoke(item, BaseActivity.EventSource.NONE)) {
                    viewBinding.buttonToggleStop.text = activity.getString(R.string.add_stop)
                    viewBinding.buttonToggleStop.setBackgroundResource(R.drawable.bg_corner_40)
                }
            } else {
                if (addStopClick.invoke(item, BaseActivity.EventSource.NONE)) {
                    viewBinding.buttonToggleStop.setBackgroundResource(R.drawable.amenity_navigate_remove_button)
                    viewBinding.buttonToggleStop.text = activity.getString(R.string.remove_stop)
                }
            }
        }
        if (isSelected) {
            viewBinding.buttonToggleStop.setBackgroundResource(R.drawable.amenity_navigate_remove_button)
            viewBinding.buttonToggleStop.text = activity.getString(R.string.remove_stop)
        } else {
            viewBinding.buttonToggleStop.text = activity.getString(R.string.add_stop)
            if (limitExceeded) {
                viewBinding.buttonToggleStop.setBackgroundResource(R.drawable.common_navigate_button_grey)
            } else {
                viewBinding.buttonToggleStop.setBackgroundResource(R.drawable.bg_corner_40)
            }
        }
        renderMarkerToolTip(markerClick, item)

    }

    private fun addRecycleView(item: BaseAmenity) {
        BreezeMapDataHolder.amenitiesPreferenceHashmap[EVCHARGER]?.subItems?.let { subItemList ->
            val plugDataList = HashSet<String>()
            item.plugTypes?.forEach { plugType ->
                val subItem =
                    subItemList.find { subItem ->
                        return@find subItem.elementName?.lowercase() == plugType.name?.lowercase()
                    }
                subItem?.let {
                    plugDataList.add(it.displayText ?: plugType.name!!)
                    return@forEach
                }
                plugDataList.add(plugType.name!!)
            }

            val amenityItemToolTipAdapter =
                AmenityItemToolTipAdapter(activity, plugDataList.toList())

            val layoutManager = FlexboxLayoutManager(activity)
            layoutManager.flexWrap = FlexWrap.WRAP
            layoutManager.flexDirection = FlexDirection.ROW
            layoutManager.justifyContent = JustifyContent.FLEX_START
            layoutManager.alignItems = AlignItems.FLEX_END
            viewBinding.listPlugTypes.layoutManager = layoutManager
            viewBinding.listPlugTypes.setHasFixedSize(true)
            viewBinding.listPlugTypes.adapter = amenityItemToolTipAdapter
        }
    }

    private fun renderMarkerToolTip(
        markerClick: () -> Unit,
        item: BaseAmenity
    ) {
        markerClick.invoke()
        BreezeMapZoomUtil.recenterToPoint(mapboxMap,
            Point.fromLngLat(item.long!!, item.lat!!),
            EdgeInsets(200.0.dpToPx(), 10.0.dpToPx(), 110.0.dpToPx(), 10.0.dpToPx()),
            animationEndCB = {
                viewBinding.root.bringToFront()
            }
        )
    }

    override fun showMarker() {
        super.showMarker()
        viewBinding.root.visibility = View.VISIBLE

    }

    override fun hideMarker() {
        super.hideMarker()
        viewBinding.root.visibility = View.INVISIBLE
    }

    override fun updateTooltipPosition() {
        mLatLng?.let { location ->
            val screenCoordinate = mapboxMap.pixelForCoordinate(
                Point.fromLngLat(
                    location.longitude,
                    location.latitude
                )
            )
            mViewMarker?.let {
                it.measure(
                    View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                    View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
                );
                it.x = screenCoordinate.x.toFloat() - (it.measuredWidth / 2)
                it.y = screenCoordinate.y.toFloat() - it.measuredHeight
            }
        }
    }
}
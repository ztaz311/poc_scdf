package com.ncs.breeze.components.navigationlog


import com.mapbox.navigation.core.MapboxNavigation
import com.mapbox.navigation.core.lifecycle.MapboxNavigationApp
import com.mapbox.navigation.core.trip.session.TripSessionState
import com.mapbox.navigation.core.trip.session.TripSessionStateObserver
import com.breeze.model.firebase.NavigationLogData
import com.ncs.breeze.common.rxbus.RxBus
import com.ncs.breeze.common.rxbus.RxEvent
import java.util.Date
import java.util.UUID


/**
 * Example showing how to record navigation history and safe it to a file.
 *
 * To access the file in Android Studio
 *  Go to View > Tools Windows > Device File Explorer > data > com.mapbox.navigation.examples > cache > history-cache
 */
class HistoryRecorder(
//    private val firebaseUploader: FirebaseUploader?,
//    private val searchAnalyticsData: SearchAnalyticsData?,
//    private val breezeHistoryListener: BreezeHistoryListener
) : TripSessionStateObserver {

    private var startedAt: Date? = null
    private var tripSessionUID: String = ""
    var currentUserId = ""
    var navigationMode = "Empty"

    /**
     * Attach the history recorder to your MapboxNavigation by using
     * [MapboxNavigation.registerTripSessionStateObserver]
     */
    override fun onSessionStateChanged(tripSessionState: TripSessionState) {
        when (tripSessionState) {
            TripSessionState.STARTED -> startRecording()
            TripSessionState.STOPPED -> stopRecording()
        }
    }

    /**
     * Manually start recording history. Make sure to do this after
     * [MapboxNavigation.startTripSession]
     */
    fun startRecording() {
        startedAt = Date()
        MapboxNavigationApp.current()?.historyRecorder?.startRecording()
        // Generate UID for trip session
        tripSessionUID = UUID.randomUUID().toString()

    }

    /**
     * Manually stop and save the history file.
     */
    fun stopRecording() {
        // Only record history that has been started.
        val startedAt = startedAt ?: return
        this.startedAt = null

        // retrieveHistory on the main thread before the navigator is reset.

        MapboxNavigationApp.current()?.historyRecorder?.stopRecording { filePath ->
            if (filePath != null) {
                RxBus.publish(
                    RxEvent.NavigationLogUploadEvent(
                        NavigationLogData(
                            filePath = filePath,
                            userId = currentUserId,
                            description = navigationMode
                        )
                    )
                )
            }
            tripSessionUID = "" // reset UID
        }

    }


//    /*
//    * Add custom search event to history trace file
//     */
//    fun addSearchEvent(searchData: SearchAnalyticsData?) {
//        if (mapboxNavigationRef.get()?.getTripSessionState() == TripSessionState.STARTED) {
//
//            // generate JSON string to be stored as custom object
//            val searchDataString = prepareSearchAnalyticsJSON(searchData)
//
//            // push search event into history file
//            mapboxNavigationRef.get()?.historyRecorder?.pushHistory(
//                "search_event",
//                searchDataString
//            )
//
//
//        } else {
//            Log.d("HistoryRecorder.kt", "addSearchEvent failed: TripSession not started")
//        }
//
//    }

//    /**
//     * prepare JSON String to later store as custom Event
//     */
//    private fun prepareSearchAnalyticsJSON(searchData: SearchAnalyticsData?): String {
//        return "{" +
//                "\"search_analytics_data\": ${searchData?.let { gson.toJson(searchData) } ?: "null"}" +
//                ", \"version\": \"v1.1\"" +
//                ", \"tripSessionUID\": \"$tripSessionUID\"" +
//                "}"
//    }


    interface BreezeHistoryListener {
        fun generateNavigationLogData(): NavigationLogData
    }
}
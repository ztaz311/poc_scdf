package com.ncs.breeze.common.utils

import android.util.ArraySet
import androidx.annotation.UiThread
import com.breeze.model.api.response.AmenitiesPreference
import com.breeze.model.api.response.NotificationPreference
import com.breeze.model.api.response.SubItems
import com.breeze.model.api.response.tbr.ProfileAmenities
import com.breeze.model.constants.AmenityType
import com.breeze.model.constants.AmenityType.CARPARK
import com.breeze.model.constants.AmenityType.POI

/**
 * Data holder for map amenities
 * Activity instances must ensure data is initialized during lifecycle events
 */
@UiThread
object BreezeMapDataHolder {

    var shouldReLoadAmenities = false
    var isAmenitiesSelected = false
    var notificationPreferenceHashmap = HashMap<String, NotificationPreference>()
    var amenitiesPreferenceHashmap = HashMap<String, AmenitiesPreference>()
    var profilePreferenceHashmap = HashMap<String, ProfileAmenities>()

    val listTypeUserChoose = ArraySet<String>()

    var notificationPreference: List<NotificationPreference> = arrayListOf()
        set(value) {
            notificationPreferenceHashmap.clear()
            value.forEach { notificationPreference ->
                notificationPreference.elementId?.let {
                    notificationPreferenceHashmap[it] = notificationPreference
                }
            }
            field = value
        }

    var amenitiesPreference: List<AmenitiesPreference> = arrayListOf()
        set(value) {
            isAmenitiesSelected = false
            amenitiesPreferenceHashmap.clear()
            value.forEach { amenitie ->
                /**
                 * get all ammenities
                 */
                amenitie.elementName?.let { elementName ->
                    if (elementName != CARPARK && elementName != POI) {
                        listTypeUserChoose.add(elementName)
                        if (amenitie.isSelected == true) {
                            isAmenitiesSelected = true
                        }
                    }
                }

                amenitie.elementName?.let {
                    amenitiesPreferenceHashmap[it] = amenitie
                }
            }
            field = value
        }

    var profilePreference: List<ProfileAmenities> = arrayListOf()
        set(value) {
            profilePreferenceHashmap.clear()
            value.forEach { profileAmenity ->
                if (profileAmenity.isActive == true && profileAmenity.elementId != null) {
                    profileAmenity.elementId?.let {
                        profilePreferenceHashmap[it] = profileAmenity
                    }
                    listProfileAmenities.add(profileAmenity)
                }
            }
            field = value
        }

    var listProfileAmenities = ArrayList<ProfileAmenities>()


    /**
     * check if exist TBR
     */
    fun isNotExistProfileAmenities(): Boolean {
        return profilePreferenceHashmap.isNullOrEmpty()
    }

    fun getFirstSeasonParkingProfile(): ProfileAmenities? {
        profilePreferenceHashmap.forEach {
            if (it.value.SeasonParking.isNotEmpty()) {
                return@getFirstSeasonParkingProfile it.value
            }
        }
        return null
    }

    fun getSelectedEVPlugTypes(): List<String> {
        return amenitiesPreference.find { AmenityType.EVCHARGER == it.elementName }?.subItems
            ?.filter { it.isSelected == true }
            ?.mapNotNull { it.elementName } ?: emptyList()
    }

    fun getSelectedPetrolProviders(): List<String> {
        return amenitiesPreference.find { AmenityType.PETROL == it.elementName }?.subItems
            ?.filter { it.isSelected == true }
            ?.mapNotNull { it.elementName } ?: emptyList()
    }


    fun updateAmenityPreference(
        elementName: String,
        subItemName: String,
        isSubItemSelected: Boolean
    ): SubItems? {
        amenitiesPreferenceHashmap[elementName]?.let { amenityMatched ->
            amenityMatched.subItems?.let { subItems ->
                val matchedSubItem = subItems.find {
                    it.elementName == subItemName
                }
                matchedSubItem?.let {
                    // this will update both amenitiesPreference and amenitiesPreferenceHashmap,
                    // as they reference the same object
                    it.isSelected = isSubItemSelected
                }

                var isParentSelected = isSubItemSelected
                if (!isParentSelected) {
                    subItems.find {
                        it.isSelected == true
                    }?.let {
                        isParentSelected = true
                    }
                }
                amenityMatched.isSelected = isParentSelected

                matchedSubItem?.let {
                    return@updateAmenityPreference it
                }
            }
        }
        return null
    }

}
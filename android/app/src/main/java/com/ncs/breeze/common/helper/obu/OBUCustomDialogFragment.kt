package com.ncs.breeze.common.helper.obu

import android.app.Dialog
import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.ncs.breeze.databinding.FragmentObuCustomDialogListDialogBinding
import com.ncs.breeze.databinding.FragmentObuCustomDialogListDialogItemBinding
import com.ncs.breeze.ui.base.BaseActivity
import kotlinx.parcelize.Parcelize


class OBUCustomDialogFragment : BottomSheetDialogFragment() {

    @Parcelize
    class Builder(
        var dialogType: String = "",
        var title: String = "",
        var contentSummary: String = "",
        var contentDescription: String = "",
        var contentDescriptionList: List<String> = listOf(),
        var actionOneButtonTitle: String = "",
        var actionTwoButtonTitle: String = "",
        var textButtonTitle: String = "",
        val callBackType: String? = null
    ) : Parcelable {
        fun build():OBUCustomDialogFragment{
            return OBUCustomDialogFragment().apply {
                arguments = Bundle().apply { putParcelable("data", this@Builder) }
            }
        }
    }

    enum class CallbackType(val value: String) {
        TypeSystemErrorDialog("TypeSystemErrorDialog"),
        TypePairingError("TypePairingError"),
        TypeOBUConnectionFailFirstTry("TypeOBUConnectionFailFirstTry"),
        TypeOBUConnectionLost("TypeOBUConnectionLost");
        companion object{
            fun fromValue(value: String?): CallbackType {
                return when (value) {
                    TypeSystemErrorDialog.value -> TypeSystemErrorDialog
                    TypePairingError.value -> TypePairingError
                    TypeOBUConnectionFailFirstTry.value -> TypeOBUConnectionFailFirstTry
                    else -> TypeOBUConnectionLost
                }
            }
        }
    }

    enum class OBUCustomDialogType(val value: String) {
        /**
         * No image, no content summary, no button one, no button two, no text button
         * */
        TYPE_1("TYPE_1"),

        /**
         * No content summary, no list description, no button three
         * */
        TYPE_2("TYPE_2"),

        /**
         * No content description, no button three
         * */
        TYPE_3("TYPE_3");
        companion object{
            fun fromValue(value: String?): OBUCustomDialogType {
                return when (value) {
                    TYPE_1.value -> TYPE_1
                    TYPE_2.value -> TYPE_2
                    else -> TYPE_3
                }
            }
        }
    }

    interface OBUCustomDialogListener {
        fun onActionOneButtonClicked() {}
        fun onActionTwoButtonClicked() {}
        fun onActionThreeButtonClicked() {}
        fun onTextButtonClicked() {}
    }

    private var _binding: FragmentObuCustomDialogListDialogBinding? = null
    private var builder: Builder? = null

    private var obuCustomDialogCallback: OBUCustomDialogListener? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, com.ncs.breeze.R.style.OBUCustomDialogTheme)
        builder = arguments?.getParcelable("data")
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = BottomSheetDialog(requireContext(), theme)
        with(dialog){
            behavior.state = BottomSheetBehavior.STATE_EXPANDED
            isCancelable = false
        }
        return dialog
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentObuCustomDialogListDialogBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        (activity as? BaseActivity<*, *>)?.let { act ->
            obuCustomDialogCallback =
                act.obuAlertHelper.getCallbackFromType(CallbackType.fromValue(builder?.callBackType))
        }
        val dialogType = OBUCustomDialogType.fromValue(builder?.dialogType)
        when(dialogType) {
            OBUCustomDialogType.TYPE_1 -> {
                binding.obuCustomDialogObuConnectionStateImageView.visibility = GONE
                binding.obuCustomDialogContentSummaryTextView.visibility = GONE
                binding.obuCustomDialogActionOneButton.visibility = GONE
                binding.obuCustomDialogActionTwoButton.visibility = GONE
                binding.obuCustomDialogButtonTextView.visibility = GONE
            }
            OBUCustomDialogType.TYPE_2 -> {
                binding.obuCustomDialogContentSummaryTextView.visibility = GONE
                binding.obuCustomDialogContentList.visibility = GONE
//                binding.obuCustomDialogActionThreeButton.visibility = GONE
            }
            OBUCustomDialogType.TYPE_3 -> {
                binding.obuCustomDialogContentDescriptionTextView.visibility = GONE
//                binding.obuCustomDialogActionThreeButton.visibility = GONE
            }
            else -> {
                // No Operation
            }
        }

        updateContent()

        processEvents()
    }

    private fun updateContent() {
        builder?.run {
            if (title.isNotEmpty())
                binding.obuCustomDialogTitleTextView.text = title
            if (contentSummary.isNotEmpty())
                binding.obuCustomDialogContentSummaryTextView.text = contentSummary
            if (contentDescription.isNotEmpty())
                binding.obuCustomDialogContentDescriptionTextView.text = contentDescription
            binding.obuCustomDialogContentList.adapter = ItemAdapter(contentDescriptionList)
            if (actionOneButtonTitle.isNotEmpty())
                binding.obuCustomDialogActionOneButton.text = actionOneButtonTitle
            if (actionTwoButtonTitle.isNotEmpty())
                binding.obuCustomDialogActionTwoButton.text = actionTwoButtonTitle
//        binding.obuCustomDialogActionThreeButton.text = actionThreeButtonTitle
            if (textButtonTitle.isNotEmpty())
                binding.obuCustomDialogButtonTextView.text = textButtonTitle
        }

    }

    private fun processEvents() {
        binding.obuCustomDialogCloseTextView.setOnClickListener {
            dismiss()
            obuCustomDialogCallback?.onActionThreeButtonClicked()
        }
        binding.obuCustomDialogActionOneButton.setOnClickListener {
            dismiss()
            obuCustomDialogCallback?.onActionOneButtonClicked()
        }
        binding.obuCustomDialogActionTwoButton.setOnClickListener {
            dismiss()
            obuCustomDialogCallback?.onActionTwoButtonClicked()
        }
//        binding.obuCustomDialogActionThreeButton.setOnClickListener {
//            dismiss()
//            obuCustomDialogListener.onActionThreeButtonClicked()
//        }
        binding.obuCustomDialogButtonTextView.setOnClickListener {
            dismiss()
            obuCustomDialogCallback?.onTextButtonClicked()
        }
    }

    private inner class ViewHolder internal constructor(binding: FragmentObuCustomDialogListDialogItemBinding) : RecyclerView.ViewHolder(binding.root) {
        internal val itemTextView: TextView = binding.listItemContentTextView
    }

    private inner class ItemAdapter internal constructor(private val itemList: List<String>?) : RecyclerView.Adapter<ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

            return ViewHolder(
                FragmentObuCustomDialogListDialogItemBinding.inflate(
                    LayoutInflater.from(
                        parent.context
                    ), parent, false
                )
            )
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.itemTextView.text = itemList?.get(position) ?: ""
        }

        override fun getItemCount(): Int {
            return itemList?.count() ?: 0
        }

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
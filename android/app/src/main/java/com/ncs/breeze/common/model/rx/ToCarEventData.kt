package com.ncs.breeze.common.model.rx

import androidx.car.app.CarToast
import androidx.lifecycle.Lifecycle
import com.breeze.model.DestinationAddressDetails
import com.breeze.model.SearchLocation
import com.breeze.model.api.request.ETARequest
import com.breeze.model.api.response.ERPResponseData
import com.breeze.model.api.response.FavouritesItem
import com.mapbox.api.directions.v5.models.DirectionsResponse
import com.mapbox.api.directions.v5.models.DirectionsRoute
import com.mapbox.navigation.base.route.NavigationRoute

sealed class ToCarEventData

data class AppSessionReadyData(val ready: Boolean) : ToCarEventData()

class DataTransferToLocationList : ToCarEventData() {
    var list = ArrayList<SearchLocation>()
}

data class CarRecentSearchData(
    var recentSearches: ArrayList<SearchLocation>,
    var homeLocation: SearchLocation?,
    var workLocation: SearchLocation?
) : ToCarEventData()

data class CarShortcutsListData(
    var normalShortcuts: ArrayList<SearchLocation>,
    var home: SearchLocation?,
    var work: SearchLocation?
) : ToCarEventData()

class DataTransferLatLngGoogle : ToCarEventData() {
    lateinit var data: SearchLocation
}

data class CarToastEventData(val title: String, val duration: Int = CarToast.LENGTH_LONG) :
    ToCarEventData()

@Deprecated("FavouritesItem is no longer used")
class DataTransferToETAList : ToCarEventData() {
    var list = ArrayList<FavouritesItem>()
}

class DataSearch : ToCarEventData() {
    var keySearch: String = ""
}

data class CarNavigationStartEventData(
    val originalDestination: DestinationAddressDetails,
    val destinationAddressDetails: DestinationAddressDetails,
    val route: DirectionsRoute,
    val compressedDirectionsResponse: ByteArray,
    val originalDestinationResponse: DirectionsResponse,
    val erpData: ERPResponseData.ERPResponse?,
    val etaRequest: ETARequest?,
    val walkingRoute: NavigationRoute? = null,
    val selectedCarParkIconRes: Int = -1,
    val isFromCruiseMode: Boolean = false,
) : ToCarEventData() {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as CarNavigationStartEventData

        if (originalDestination != other.originalDestination) return false
        if (destinationAddressDetails != other.destinationAddressDetails) return false
        if (route != other.route) return false
        if (!compressedDirectionsResponse.contentEquals(other.compressedDirectionsResponse)) return false
        if (originalDestinationResponse != other.originalDestinationResponse) return false
        if (erpData != other.erpData) return false
        if (etaRequest != other.etaRequest) return false

        return true
    }

    override fun hashCode(): Int {
        var result = originalDestination.hashCode()
        result = 31 * result + destinationAddressDetails.hashCode()
        result = 31 * result + route.hashCode()
        result = 31 * result + compressedDirectionsResponse.contentHashCode()
        result = 31 * result + originalDestinationResponse.hashCode()
        result = 31 * result + (erpData?.hashCode() ?: 0)
        result = 31 * result + (etaRequest?.hashCode() ?: 0)
        return result
    }
}

data class OBUDisplayLifecycleEvent(
    val state: Lifecycle.State
) : ToCarRxEvent()

enum class EhorizonLayerType {
    EHORIZON_SCHOOL_LAYER,
    EHORIZON_SILVER_LAYER,

}
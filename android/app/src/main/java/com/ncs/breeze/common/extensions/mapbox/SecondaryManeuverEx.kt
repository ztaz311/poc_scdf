package com.ncs.breeze.common.extensions.mapbox

import android.text.SpannableStringBuilder
import com.mapbox.navigation.ui.maneuver.model.SecondaryManeuver
import com.mapbox.navigation.ui.maneuver.model.TextComponentNode
import com.breeze.model.extensions.hasExcludedRoadNameCharacters

// name is array of text with format: [English Name][/][other language name][/][another language name]
fun SecondaryManeuver.extractEnglishName(): String? = (componentList.firstOrNull {
    (it.node as? TextComponentNode)?.let { textComponent ->
        textComponent.text.isNotEmpty() && textComponent.text != "/"
    } == true
}?.node as? TextComponentNode)?.text

fun SecondaryManeuver.extractNonTamilAndChineseName(): String? {
    val stringBuilder = SpannableStringBuilder()
    val separator = " / "
    componentList.forEach { component ->
        (component.node as? TextComponentNode)
            ?.takeIf {
                !it.text.hasExcludedRoadNameCharacters()
            }?.let {
                stringBuilder.append(it.text)
                stringBuilder.append(separator)
            }
    }
    if (stringBuilder.isNotEmpty()) {
        return stringBuilder.removeSuffix(separator).toString()
    }
    return null
}
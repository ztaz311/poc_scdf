package com.ncs.breeze.car.breeze.ehorizon

import com.mapbox.androidauto.internal.logAndroidAuto
import com.mapbox.api.directions.v5.models.DirectionsRoute
import com.mapbox.maps.extension.androidauto.MapboxCarMapObserver
import com.mapbox.maps.extension.androidauto.MapboxCarMapSurface
import com.mapbox.navigation.base.ExperimentalPreviewMapboxNavigationAPI
import com.ncs.breeze.R
import com.ncs.breeze.car.breeze.MainBreezeCarContext
import com.ncs.breeze.car.breeze.utils.BreezeCarUtil
import com.ncs.breeze.common.event.ToCarRx
import com.ncs.breeze.common.helper.ehorizon.BreezeEdgeMetaDataObserver
import com.ncs.breeze.common.helper.ehorizon.BreezeNotificationObserver
import com.ncs.breeze.common.helper.ehorizon.BreezeRoadObjectHandler
import com.ncs.breeze.common.helper.ehorizon.EHorizonMode
import com.breeze.model.NavigationZone
import com.mapbox.navigation.base.road.model.RoadComponent
import com.mapbox.navigation.ui.maps.internal.ui.RoadNameComponent
import com.ncs.breeze.common.model.rx.NavigationNotificationEventReset
import com.ncs.breeze.common.model.rx.NavigationNotificationEventShow
import com.ncs.breeze.common.model.rx.TriggerCarToastEvent
import com.ncs.breeze.common.service.HorizonObserverFillterZoneUtils
import com.ncs.breeze.common.utils.BreezeEHorizonProvider

class CarRoadObjectRenderer(
    private val mainCarContext: MainBreezeCarContext,
    private val mode: EHorizonMode,
    private val route: DirectionsRoute?,
) : MapboxCarMapObserver {

    var eHorizonHandler: BreezeRoadObjectHandler? = null;

    private lateinit var breezeEdgeMetaDataObserver: BreezeEdgeMetaDataObserver
    private lateinit var breezeNotificationObserver: BreezeNotificationObserver

    @OptIn(ExperimentalPreviewMapboxNavigationAPI::class)
    override fun onAttached(mapboxCarMapSurface: MapboxCarMapSurface) {
        logAndroidAuto("CarCruiseRoadObjectRenderer carMapSurface loaded")
        initRoadMetaDataObserver()
        initRoadNotificationObserver()
        eHorizonHandler = BreezeEHorizonProvider.create(
            mainCarContext.carContext,
            BreezeCarUtil.breezeERPUtil,
            BreezeCarUtil.breezeIncidentsUtil,
            mode,
            route
        )
        registerEhorizonNotificationObservers(mapboxCarMapSurface)
    }

    override fun onDetached(mapboxCarMapSurface: MapboxCarMapSurface) {
        unregisterEhorizonNotificationObservers(mapboxCarMapSurface)
        mapboxCarMapSurface.mapSurface.getMapboxMap().getStyle { style ->
            eHorizonHandler?.removeLastNotification(style, breezeNotificationObserver)
        }
    }

    private fun registerEhorizonNotificationObservers(mapboxCarMapSurface: MapboxCarMapSurface) {
        unregisterEhorizonNotificationObservers(mapboxCarMapSurface)
        eHorizonHandler?.let { eHorizonObserver ->
            eHorizonObserver.addBreezeEdgeMetaDataObserver(breezeEdgeMetaDataObserver)
            eHorizonObserver.addBreezeNotificationObserver(breezeNotificationObserver)
            mapboxCarMapSurface.mapSurface.getMapboxMap().getStyle { style ->
                eHorizonObserver.addBreezeNotificationStyle(style)
            }
        }
    }

    private fun unregisterEhorizonNotificationObservers(mapboxCarMapSurface: MapboxCarMapSurface) {
        eHorizonHandler?.let { eHorizonObserver ->
            eHorizonObserver.removeBreezeEdgeMetaDataObserver(breezeEdgeMetaDataObserver)
            eHorizonObserver.removeBreezeNotificationObserver(breezeNotificationObserver)
            mapboxCarMapSurface.mapSurface.getMapboxMap().getStyle { style ->
                eHorizonObserver.removeBreezeNotificationStyle(style)
            }
        }
    }


    private fun initRoadNotificationObserver() {
        if (this::breezeNotificationObserver.isInitialized) {
            return;
        }
        breezeNotificationObserver = object : BreezeNotificationObserver {
            override fun showNotificationView(
                zone: NavigationZone,
                distance: Int,
                mERPName: String?,
                chargeAmount: String?,
                roadObjectID: String,
                location: String?
            ) {
                val data = HorizonObserverFillterZoneUtils.filterZone(zone, distance)
                ToCarRx.postEvent(
                    NavigationNotificationEventShow(
                        data,
                        roadObjectID,
                        zone,
                        distance,
                        location
                    )
                )
            }

            override fun removeNotificationView(roadObjectId: String) {
                ToCarRx.postEvent(NavigationNotificationEventReset(roadObjectId))
            }

        }
    }

    private fun initRoadMetaDataObserver() {
        if (this::breezeEdgeMetaDataObserver.isInitialized) {
            return;
        }
        breezeEdgeMetaDataObserver = object : BreezeEdgeMetaDataObserver {
            override fun updateEdgeMetaData(
                speedLimit: Double?,
                names: List<RoadComponent>?,
                tunnel: Boolean
            ) {
                if (tunnel) {
                    ToCarRx.postEvent(
                        TriggerCarToastEvent.create(
                            title = mainCarContext.carContext.getString(R.string.tunnel)
                        )
                    )
                }
            }

        }
    }


}
package com.ncs.breeze.common.utils


import android.content.Context
import android.location.Location
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.data.Statistics
import com.breeze.model.ERPCost
import com.breeze.model.TripType
import com.breeze.model.DestinationAddressDetails
import com.breeze.model.constants.Constants
import com.mapbox.navigation.core.trip.session.LocationMatcherResult
import com.ncs.breeze.common.constant.OBUConnectionState
import com.ncs.breeze.common.extensions.android.getApp

import java.lang.ref.WeakReference


class TripLoggingManager private constructor(
    context: Context,
    val breezeTripLoggingUtil: BreezeTripLoggingUtil,
    val breezeStatisticsUtil: BreezeStatisticsUtil
) {
    private val contextRef = WeakReference(context)
    private var isTripLoggingEnabled = false
    private var currentOBUName: String? = null
    var currentTripLogType: TripType? = null
        private set

    fun logErpDetection(
        mERPID: String,
        mERPName: String,
        chargeAmount: String,
        time: Long
    ) {
        if (isTripLoggingEnabled) {
            val mErpCost = ERPCost(
                mERPID,
                mERPName,
                chargeAmount.toFloat(),
                time
            )
            breezeTripLoggingUtil.updateERPDetection(mErpCost)
        }
    }

    fun startTripLogging(
        tripType: TripType,
        screenName: String,
        destinationAddressDetails: DestinationAddressDetails?,
        isWalking: Boolean = false
    ) {
        synchronized(this) {
            if (!isTripLoggingEnabled) {
                isTripLoggingEnabled = true

                currentTripLogType = tripType

                LocationBreezeManager.getInstance()
                    .registerLocationCallback(callbackLocationBreezeManager)
                breezeTripLoggingUtil.startTripLogging(
                    LocationBreezeManager.getInstance().currentLocation,
                    tripType,
                    destinationAddressDetails,
                    isWalking
                )
                //log statistics data
                contextRef.get()?.let {
                    it.getApp()?.obuConnectionHelper?.let { obuHelper ->
                        currentOBUName = if (obuHelper.obuConnectionState.value == OBUConnectionState.CONNECTED) {
                            obuHelper.currentOBUDevice?.obuName
                        } else {
                            null
                        }

                        breezeTripLoggingUtil.changeOBUStatus(obuHelper.getCurrentOBUStatus())
                    }

                    val statistics = Statistics(
                        breezeStatisticsUtil.getBatteryPercentage(it),
                        breezeStatisticsUtil.getTotalDataSent(),
                        breezeStatisticsUtil.getTotalDataReceived()
                    )
                    Analytics.logStatisticsDataStart(statistics, screenName)
                }

            }
        }
    }


    fun stopTripLogging(
        currentDestinationDetails: DestinationAddressDetails?,
        screenName: String,
        isArrivedAtDestination: Boolean = false,
        bookmarkId: Int? = null,
        bookmarkName: String? = null,
        onFinished: (result: Boolean) -> Unit = {}
    ) {
        synchronized(this) {
            if (isTripLoggingEnabled) {
                isTripLoggingEnabled = false
                currentTripLogType = null
                LocationBreezeManager.getInstance()
                    .removeLocationCallBack(callbackLocationBreezeManager)
                callbackLocationBreezeManager.previousLocation = null
                if (isArrivedAtDestination) {
                    breezeTripLoggingUtil.stopTripLogging(
                        endLocation = LocationBreezeManager.getInstance().currentLocation,
                        endType = Constants.TYPE_END_NAVIGATION.NATURAL,
                        endAddress = currentDestinationDetails,
                        amenityId = currentDestinationDetails?.amenityId,
                        layerCode = currentDestinationDetails?.layerCode,
                        trackNavigation = currentDestinationDetails?.trackNavigation,
                        amenityType = currentDestinationDetails?.amenityType ?: "",
                        bookmarkId = bookmarkId,
                        bookmarkName = bookmarkName,
                        obuName = currentOBUName,
                        onFinished = onFinished
                    )
                } else {
                    breezeTripLoggingUtil.stopTripLogging(
                        endLocation = LocationBreezeManager.getInstance().currentLocation,
                        endType = Constants.TYPE_END_NAVIGATION.MANUAL,
                        endAddress = currentDestinationDetails,
                        amenityId = currentDestinationDetails?.amenityId,
                        layerCode = currentDestinationDetails?.layerCode,
                        trackNavigation = currentDestinationDetails?.trackNavigation,
                        amenityType = currentDestinationDetails?.amenityType ?: "",
                        bookmarkId = bookmarkId,
                        bookmarkName = bookmarkName,
                        obuName = currentOBUName,
                        onFinished = onFinished
                    )
                }
                //log statistics data
                contextRef.get()?.let {
                    val statistics = Statistics(
                        breezeStatisticsUtil.getBatteryPercentage(it),
                        breezeStatisticsUtil.getTotalDataSent(),
                        breezeStatisticsUtil.getTotalDataReceived()
                    )
                    Analytics.logStatisticsDataEnd(statistics, screenName)
                }

            } else {
                onFinished.invoke(false)
            }
        }
    }

    fun changeCurrentOBUName(obuName: String?) {
        this.currentOBUName = obuName
    }
    fun updateTripOBUStatus(status:String){
        breezeTripLoggingUtil.changeOBUStatus(status)
    }

    private val callbackLocationBreezeManager =
        object : LocationBreezeManager.CallBackLocationBreezeManager {
            var previousLocation: Location? = null

            override fun onNewLocationMatcherResult(locationMatcherResult: LocationMatcherResult) {
                if (isTripLoggingEnabled) {
                    val location = locationMatcherResult.enhancedLocation
                    var distanceTravelled = 0.0F
                    if (previousLocation == null) {
                        previousLocation = location
                        distanceTravelled = 0.0F
                    } else {

                        if (locationMatcherResult.isTeleport || locationMatcherResult.isOffRoad) {
                            distanceTravelled = 0.0F
                        } else {
                            previousLocation?.let {
                                distanceTravelled = location.distanceTo(it)
                            }
                            previousLocation = location
                        }


                    }
                    breezeTripLoggingUtil.updateDistance(
                        location,
                        distanceTravelled.toDouble()
                    )
                } else {
                    previousLocation = null
                }


            }
        }


    fun checkIdle(speedKmH: Double) {
        if (speedKmH < 1.0)
            breezeTripLoggingUtil.startCountIdleTime()
        else
            breezeTripLoggingUtil.stopCountIdleTime()
    }

    companion object {
        private var mInstance: TripLoggingManager? = null

        fun getInstance(): TripLoggingManager? {
            return mInstance
        }

        fun setupTripLogging(
            context: Context,
            breezeTripLoggingUtil: BreezeTripLoggingUtil,
            breezeStatisticsUtil: BreezeStatisticsUtil
        ) {
            if (mInstance == null) {
                mInstance = TripLoggingManager(context, breezeTripLoggingUtil, breezeStatisticsUtil)
            }
        }

        fun destroyInstance() {
            mInstance?.isTripLoggingEnabled = false
            mInstance = null;
        }
    }


}
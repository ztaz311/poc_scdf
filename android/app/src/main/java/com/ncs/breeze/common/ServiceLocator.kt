package com.ncs.breeze.common

import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.google.firebase.ktx.app
import com.google.firebase.storage.ktx.storage
import com.mapbox.navtriprecorder.integration.FirebaseUploader
import kotlinx.coroutines.tasks.await

object ServiceLocator {

    private const val MAPBOX_INTEGRATION_FIREBASE_KEY = "SECONDARY"

    val firebaseUploader: FirebaseUploader? by lazy {
        try {
            val integrationFirebase = Firebase.app(MAPBOX_INTEGRATION_FIREBASE_KEY)
            FirebaseUploader(
                Firebase.storage(integrationFirebase),
                Firebase.auth(integrationFirebase)
            )
        } catch (e: Exception) {
            null
        }
    }

    suspend fun getSecondaryFirebaseAuthUserId(): String? {
        val integrationFirebase = Firebase.app(MAPBOX_INTEGRATION_FIREBASE_KEY)
        var firebaseAuth = Firebase.auth(integrationFirebase)
        val currentUser = firebaseAuth.currentUser
        if (currentUser == null) {
            firebaseAuth.signInAnonymously().await()
        }
        return firebaseAuth.currentUser?.uid
    }
}
package com.ncs.breeze.di.module.fragment

import com.ncs.breeze.ui.dashboard.fragments.traffic.TrafficDetailFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class TrafficDetailFragmentModule {
    @ContributesAndroidInjector
    abstract fun contributeTrafficDetailFragment(): TrafficDetailFragment
}
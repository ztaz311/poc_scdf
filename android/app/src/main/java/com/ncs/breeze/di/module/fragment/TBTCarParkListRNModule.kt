package com.ncs.breeze.di.module.fragment

import com.ncs.breeze.ui.navigation.TBTCarParkListRNFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class TBTCarParkListRNModule {
    @ContributesAndroidInjector
    abstract fun contributeTBTCarParkListRNFragment(): TBTCarParkListRNFragment
}
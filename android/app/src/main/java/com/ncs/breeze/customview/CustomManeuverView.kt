package com.ncs.breeze.customview


import android.animation.ObjectAnimator
import android.content.Context
import android.content.res.TypedArray
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import androidx.annotation.StyleRes
import androidx.appcompat.view.ContextThemeWrapper
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.widget.TextViewCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager.HORIZONTAL
import androidx.recyclerview.widget.RecyclerView.VERTICAL
import com.mapbox.navigation.ui.maneuver.api.MapboxManeuverApi
import com.mapbox.navigation.ui.maneuver.databinding.MapboxMainManeuverLayoutBinding
import com.mapbox.navigation.ui.maneuver.databinding.MapboxSubManeuverLayoutBinding
import com.mapbox.navigation.ui.maneuver.model.*
import com.mapbox.navigation.ui.shield.model.RouteShield
import com.ncs.breeze.R
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.Event
import com.ncs.breeze.common.analytics.Screen
import com.ncs.breeze.common.extensions.android.setBackgroundColorResource
import com.ncs.breeze.common.extensions.mapbox.extractNonTamilAndChineseName
import com.ncs.breeze.components.BreezeLaneGuidanceAdapter
import com.ncs.breeze.components.BreezeNavigationManeuverAdapter
import com.ncs.breeze.components.MapboxManeuversList
import com.ncs.breeze.components.OnManeuverItemClick
import com.ncs.breeze.databinding.BreezeManueverLayoutBinding

/**
 * Default view to render a maneuver.
 *
 * @see MapboxManeuverApi
 */
class CustomManeuverView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null
) : ConstraintLayout(context, attrs) {

    val ENABLE_TOGGLE_BUTTON = true

    private val laneGuidanceAdapter = BreezeLaneGuidanceAdapter(context)
    private val upcomingManeuverAdapter = BreezeNavigationManeuverAdapter(context)
    private val binding = BreezeManueverLayoutBinding.inflate(
        LayoutInflater.from(context),
        this,
        true
    )
    private val mainLayoutBinding = MapboxMainManeuverLayoutBinding.bind(binding.root)
    private val subLayoutBinding = MapboxSubManeuverLayoutBinding.bind(binding.root)
    private val maneuverToRoadShield = mutableMapOf<String, RoadShield?>()
    private val currentlyRenderedManeuvers: MutableList<Maneuver> = mutableListOf()
    private lateinit var upcomingManeuverRecycler: MapboxManeuversList

    /**
     * The property enables/disables showing the list of upcoming maneuvers.
     * Note: The View doesn't maintain the state of this property. If the device undergoes
     * screen rotation changes make sure to set this property again at appropriate place.
     */
    var upcomingManeuverRenderingEnabled = true
        set(value) {
            if ((value != field) && !value && upcomingManeuverRecycler.visibility == VISIBLE) {
                updateUpcomingManeuversVisibility(GONE)
            }
            field = value
        }

    fun setMapboxManeuversList(
        maneuverRecycler: MapboxManeuversList,
        onItemClick: OnManeuverItemClick = { _, _, _ -> }
    ) {
        this.upcomingManeuverRecycler = maneuverRecycler
        upcomingManeuverRecycler.apply {
            setBackgroundColor(context.getColor(R.color.navigation_upcoming_maneuver))
            layoutManager = LinearLayoutManager(context, VERTICAL, false)
            upcomingManeuverAdapter.onItemClickListener = onItemClick
            adapter = upcomingManeuverAdapter
        }
    }

    /**
     * Initialize.
     */
    init {
        initAttributes(attrs)
        binding.laneGuidanceRecycler.apply {
            layoutManager = LinearLayoutManager(context, HORIZONTAL, false)
            adapter = laneGuidanceAdapter
        }

        binding.mainManeuverLayout.setOnClickListener {
            if (upcomingManeuverRenderingEnabled) {

                if (upcomingManeuverRecycler.visibility == GONE) {
                    Analytics.logClickEvent(Event.EXPAND_INSTRUCTIONS_CLICK, Screen.NAVIGATION)
                    updateUpcomingManeuversVisibility(VISIBLE)
                } else {
                    Analytics.logClickEvent(
                        Event.COLLAPSE_INSTRUCTIONS_CLICK,
                        Screen.NAVIGATION
                    )
                    updateUpcomingManeuversVisibility(GONE)
                    upcomingManeuverRecycler.smoothScrollToPosition(0)
                    updateSubManeuverViewVisibility(binding.subManeuverLayout.visibility)
                }
            }
        }

        binding.sectionToggle.setOnClickListener {
            binding.mainManeuverLayout.performClick()
        }
    }

    /**
     * Invoke the method to render the maneuvers.
     *
     * The first maneuver on the list will be rendered as the current maneuver while the rest of the maneuvers will be rendered as upcoming maneuvers.
     *
     * You can control the number of maneuvers that will be rendered by manipulating the provided list,
     * for example, you can render the current maneuver and a list of up to 5 upcoming maneuvers like this:
     * ```
     * maneuverView.renderManeuvers(
     *     maneuvers.mapValue {
     *         it.take(6)
     *     }
     * )
     * ```
     *
     * If there were any shields provided via [renderManeuverShields], those shields will be attached to corresponding [Maneuver]s during rendering.
     *
     * This method ignores empty lists of maneuvers and will not clean up the view if an empty list is provided.
     *
     * @param maneuvers maneuvers to render
     * @see MapboxManeuverApi.getManeuvers
     */
    fun renderManeuvers(maneuvers: List<Maneuver>) {
        if (maneuvers.isNotEmpty()) {
            currentlyRenderedManeuvers.clear()
            currentlyRenderedManeuvers.addAll(maneuvers)
            renderManeuvers()
        }
    }

    /**
     * Invoke the method to update rendered maneuvers with road shields.
     *
     * The provided shields are mapped to IDs of [PrimaryManeuver], [SecondaryManeuver], and [SubManeuver]
     * and if a maneuver has already been rendered via [renderManeuvers], the respective shields' text will be changed to the shield icon.
     *
     * Invoking this method also caches all of the available shields. Whenever [renderManeuvers] is invoked, the cached shields are reused for rendering.
     *
     * @param shieldMap the map of maneuver IDs to available shields
     */
    fun renderManeuverShields(shieldMap: Map<String, RoadShield?>) {
        maneuverToRoadShield.putAll(shieldMap)
        renderManeuvers()
    }

    /**
     * Invoke the method if there is a need to use other turn icon drawables than the default icons
     * supplied.
     * @param turnIconResources TurnIconResources
     */
    fun updateTurnIconResources(turnIconResources: TurnIconResources) {
        mainLayoutBinding.maneuverIcon.updateTurnIconResources(turnIconResources)
        subLayoutBinding.subManeuverIcon.updateTurnIconResources(turnIconResources)
    }

    /**
     * Allows you to change the style of turn icon in main and sub maneuver view.
     * @param style Int
     */
    fun updateTurnIconStyle(@StyleRes style: Int) {
        mainLayoutBinding.maneuverIcon.updateTurnIconStyle(
            ContextThemeWrapper(context, style)
        )
        mainLayoutBinding.stepDistance
        subLayoutBinding.subManeuverIcon.updateTurnIconStyle(
            ContextThemeWrapper(context, style)
        )
    }

    /**
     * Allows you to change the text appearance of primary maneuver text.
     * @see [TextViewCompat.setTextAppearance]
     * @param style Int
     */
    fun updatePrimaryManeuverTextAppearance(@StyleRes style: Int) {
        TextViewCompat.setTextAppearance(mainLayoutBinding.primaryManeuverText, style)
    }

    /**
     * Allows you to change the text appearance of primary maneuver text.
     * @see [TextViewCompat.setTextAppearance]
     * @param style Int
     */
    fun updateExitViewTextAppearance(@StyleRes style: Int) {
        val options = ManeuverPrimaryOptions.Builder()
            .exitOptions(
                ManeuverExitOptions.Builder()
                    .textAppearance(style)
                    .build()
            )
            .build()
        mainLayoutBinding.primaryManeuverText.updateOptions(options)
    }

    /**
     * Allows you to change the text appearance of secondary maneuver text.
     * @see [TextViewCompat.setTextAppearance]
     * @param style Int
     */
    fun updateSecondaryManeuverTextAppearance(@StyleRes style: Int) {
        TextViewCompat.setTextAppearance(mainLayoutBinding.secondaryManeuverText, style)
    }

    /**
     * Allows you to change the text appearance of sub maneuver text.
     * @see [TextViewCompat.setTextAppearance]
     * @param style Int
     */
    fun updateSubManeuverTextAppearance(@StyleRes style: Int) {
        TextViewCompat.setTextAppearance(subLayoutBinding.subManeuverText, style)
    }

    /**
     * Allows you to change the text appearance of step distance text.
     * @see [TextViewCompat.setTextAppearance]
     * @param style Int
     */
    fun updateStepDistanceTextAppearance(@StyleRes style: Int) {
        TextViewCompat.setTextAppearance(mainLayoutBinding.stepDistance, style)
    }

    /**
     * Allows you to change the text appearance of primary maneuver text in upcoming maneuver list.
     * @see [TextViewCompat.setTextAppearance]
     * @param style Int
     */
    fun updateUpcomingManeuverStyles(
        @StyleRes stylePrimaryManeuver: Int,
        @StyleRes styleSecondaryManeuver: Int,
        @StyleRes styleStepDistance: Int,
        @StyleRes styleExitManeuver: Int,
        @StyleRes styleManeuverIcon: Int
    ) {
        upcomingManeuverAdapter.updateUpcomingManeuverStyles(
            stylePrimaryManeuver = stylePrimaryManeuver,
            styleSecondaryManeuver = styleSecondaryManeuver,
            styleStepDistance = styleStepDistance,
            styleExitManeuver = styleExitManeuver,
            styleManeuverIcon = styleManeuverIcon
        )

    }

    /**
     * Allows you to change the style of [MapboxManeuverView].
     * @param style Int
     */
    fun updateStyle(@StyleRes style: Int) {
        val typedArray = context.obtainStyledAttributes(style, R.styleable.MapboxManeuverView)
        applyAttributes(typedArray)
        typedArray.recycle()
    }

    /**
     * Invoke the method to control the visibility of [MapboxPrimaryManeuver]
     * @param visibility Int
     */
    fun updatePrimaryManeuverTextVisibility(visibility: Int) {
        mainLayoutBinding.primaryManeuverText.visibility = visibility
    }

    /**
     * Invoke the method to control the visibility of [MapboxSecondaryManeuver]
     * @param visibility Int
     */
    fun updateSecondaryManeuverVisibility(visibility: Int) {
        when (visibility) {
            VISIBLE -> {
                showSecondaryManeuver()
            }

            INVISIBLE -> {
                hideSecondaryManeuver(INVISIBLE)
            }

            GONE -> {
                hideSecondaryManeuver(GONE)
            }
        }
    }

    /**
     * Invoke the method to control the visibility of [MapboxSubManeuver]
     * @param visibility Int
     */
    fun updateSubManeuverViewVisibility(visibility: Int) {
        binding.subManeuverLayout.visibility = visibility
    }

    /**
     * Invoke the method to control the visibility of upcoming instructions list
     * @param visibility Int
     */
    fun updateUpcomingManeuversVisibility(visibility: Int) {
        if (this::upcomingManeuverRecycler.isInitialized) {
            upcomingManeuverRecycler.visibility = visibility
        }
        if (visibility == View.VISIBLE)
            rotateToggleImage(180.0f)
        else
            rotateToggleImage(0.0f)
    }

    private fun rotateToggleImage(rotationValue: Float) {
        ObjectAnimator.ofFloat(
            binding.imgToggle,
            View.ROTATION,
            binding.imgToggle.rotation,
            rotationValue
        ).setDuration(300).start();
    }

    /**
     * Invoke the method to render primary instructions on top of [MapboxManeuverView]
     * @param primary PrimaryManeuver
     */
    @JvmOverloads
    fun renderPrimaryManeuver(primary: PrimaryManeuver, roadShield: RoadShield? = null) {
        mainLayoutBinding.primaryManeuverText.render(primary, roadShield)
        primary.extractNonTamilAndChineseName()
            ?.let { mainLayoutBinding.primaryManeuverText.text = it }
        mainLayoutBinding.maneuverIcon.renderPrimaryTurnIcon(primary)
    }

    /**
     * Invoke the method to render secondary instructions on top of [MapboxManeuverView]
     * @param secondary SecondaryManeuver?
     */
    @JvmOverloads
    fun renderSecondaryManeuver(secondary: SecondaryManeuver?, roadShield: RoadShield? = null) {
        mainLayoutBinding.secondaryManeuverText.renderManeuver(
            secondary,
            mutableSetOf<RouteShield>()
                .apply { roadShield?.toRouteShield()?.let { add(it) } })
        secondary?.extractNonTamilAndChineseName()
            ?.let { mainLayoutBinding.secondaryManeuverText.text = it }
    }

    /**
     * Invoke the method to render sub instructions on top of [MapboxManeuverView]
     * @param sub SubManeuver?
     */
    @JvmOverloads
    fun renderSubManeuver(sub: SubManeuver?, roadShield: RoadShield? = null) {
        subLayoutBinding.subManeuverText.render(sub, roadShield)
        subLayoutBinding.subManeuverIcon.renderSubTurnIcon(sub)
    }

    /**
     * Invoke the method to add lane information on top of [MapboxManeuverView]
     * @param lane Lane
     */
    fun renderAddLanes(lane: Lane) {
        laneGuidanceAdapter.addLanes(lane.allLanes)
    }

    /**
     * Invoke the method to remove lane information on top of [MapboxManeuverView]
     */
    fun renderRemoveLanes() {
        laneGuidanceAdapter.removeLanes()
    }

    /**
     * Invoke the method to render step distance remaining on top of [MapboxManeuverView]
     * @param stepDistance StepDistance
     */
    fun renderDistanceRemaining(stepDistance: StepDistance) {
        mainLayoutBinding.stepDistance.renderDistanceRemaining(stepDistance)
    }

    private fun renderManeuvers() {
        if (currentlyRenderedManeuvers.isNotEmpty()) {
            drawManeuver(currentlyRenderedManeuvers[0], maneuverToRoadShield)
            upcomingManeuverAdapter.updateRoadShields(maneuverToRoadShield)
            drawUpcomingManeuvers(currentlyRenderedManeuvers.drop(1))
            if (ENABLE_TOGGLE_BUTTON) {
                binding.sectionToggle.visibility = View.VISIBLE
            }
        }
    }

    private fun initAttributes(attrs: AttributeSet?) {
        val typedArray = context.obtainStyledAttributes(
            attrs,
            R.styleable.MapboxManeuverView,
            0,
            R.style.MapboxStyleManeuverView
        )
        applyAttributes(typedArray)
        typedArray.recycle()
    }

    private fun applyAttributes(typedArray: TypedArray) {

        binding.mainManeuverLayout.setBackgroundColorResource(R.color.navigation_main_manuever)
        binding.sectionToggle.setBackgroundColorResource(R.color.navigation_main_manuever)
        binding.subManeuverLayout.setBackgroundColorResource(R.color.navigation_upcoming_maneuver)

        /*binding.mainManeuverLayout.setBackgroundColor(
            typedArray.getColor(
                R.styleable.MapboxManeuverView_maneuverViewBackgroundColor,
                ContextCompat.getColor(
                    context,
                    R.color.mapbox_main_maneuver_background_color
                )
            )
        )
        binding.subManeuverLayout.setBackgroundColor(
            typedArray.getColor(
                R.styleable.MapboxManeuverView_subManeuverViewBackgroundColor,
                ContextCompat.getColor(
                    context,
                    R.color.mapbox_sub_maneuver_background_color
                )
            )
        )
        upcomingManeuverRecycler.setBackgroundColor(
            typedArray.getColor(
                R.styleable.MapboxManeuverView_upcomingManeuverViewBackgroundColor,
                ContextCompat.getColor(
                    context,
                    R.color.mapbox_upcoming_maneuver_background_color
                )
            )
        )*/
        laneGuidanceAdapter.updateStyle(
            typedArray.getResourceId(
                R.styleable.MapboxManeuverView_laneGuidanceManeuverIconStyle,
                R.style.CarLaneActiveTheme
            )
        )
        mainLayoutBinding.maneuverIcon.updateTurnIconStyle(
            ContextThemeWrapper(
                context,
                typedArray.getResourceId(
                    R.styleable.MapboxManeuverView_maneuverViewIconStyle,
                    R.style.MapboxStyleTurnIconManeuver
                )
            )
        )
        subLayoutBinding.subManeuverIcon.updateTurnIconStyle(
            ContextThemeWrapper(
                context,
                typedArray.getResourceId(
                    R.styleable.MapboxManeuverView_maneuverViewIconStyle,
                    R.style.MapboxStyleTurnIconManeuver
                )
            )
        )
    }

    private fun drawUpcomingManeuvers(maneuvers: List<Maneuver>) {
        upcomingManeuverAdapter.addUpcomingManeuvers(maneuvers)
    }

    private fun drawManeuver(maneuver: Maneuver, shieldMap: Map<String, RoadShield?>) {
        val primary = maneuver.primary
        val secondary = maneuver.secondary
        val sub = maneuver.sub
        val lane = maneuver.laneGuidance
        val stepDistance = maneuver.stepDistance
        val primaryId = primary.id
        val secondaryId = secondary?.id
        val subId = sub?.id
        if (secondary?.componentList != null) {
            updateSecondaryManeuverVisibility(VISIBLE)
            renderSecondaryManeuver(secondary, shieldMap[secondaryId])
        } else {
            updateSecondaryManeuverVisibility(GONE)
        }
        renderPrimaryManeuver(primary, shieldMap[primaryId])
        renderDistanceRemaining(stepDistance)
        if (sub?.componentList != null || lane != null) {
            updateSubManeuverViewVisibility(VISIBLE)
        } else {
            updateSubManeuverViewVisibility(GONE)
        }
        if (sub?.componentList != null) {
            renderSubManeuver(sub, shieldMap[subId])
        } else {
            renderSubManeuver(null)
        }
        when (lane != null) {
            true -> {
                renderAddLanes(lane)
            }

            else -> {
                renderRemoveLanes()
            }
        }
    }

    private fun hideSecondaryManeuver(visibility: Int) {
        mainLayoutBinding.secondaryManeuverText.visibility = visibility
        updateConstraintsToOnlyPrimary()
        mainLayoutBinding.primaryManeuverText.maxLines = 2
    }

    private fun showSecondaryManeuver() {
        mainLayoutBinding.secondaryManeuverText.visibility = VISIBLE
        updateConstraintsToHaveSecondary()
        mainLayoutBinding.primaryManeuverText.maxLines = 1
    }

    private fun updateConstraintsToOnlyPrimary() {
        val params = mainLayoutBinding.primaryManeuverText.layoutParams as LayoutParams
        params.topToTop = LayoutParams.PARENT_ID
        params.bottomToBottom = LayoutParams.PARENT_ID
        requestLayout()
    }

    private fun updateConstraintsToHaveSecondary() {
        val params = mainLayoutBinding.primaryManeuverText.layoutParams as LayoutParams
        params.topToTop = LayoutParams.UNSET
        params.bottomToBottom = LayoutParams.UNSET
        params.bottomToTop = mainLayoutBinding.secondaryManeuverText.id
        requestLayout()
    }
}

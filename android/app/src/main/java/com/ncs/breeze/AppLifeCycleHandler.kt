package com.ncs.breeze

import android.app.Activity
import android.app.Application
import android.content.ComponentCallbacks2
import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import android.content.res.Configuration
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.Event
import com.ncs.breeze.common.analytics.Screen
import com.ncs.breeze.common.rxbus.RxBus
import com.ncs.breeze.common.rxbus.RxEvent
import com.ncs.breeze.common.utils.Utils
import com.ncs.breeze.ui.login.activity.LoginActivity
import com.ncs.breeze.ui.splash.PermissionActivity
import com.ncs.breeze.ui.splash.SplashActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import timber.log.Timber
import java.lang.ref.WeakReference


class AppLifeCycleHandler(app: App) :
    Application.ActivityLifecycleCallbacks, ComponentCallbacks2 {

    private val appRef = WeakReference(app)

    /**
     * From Activity resume to being trim memory due to UI being hidden
     * */
    var isAppActive = false

    /**
     * true if there is activity is in resumed - pause
     * */
    var isAppInForeground = false
    private set

    private var noInternetDisposable: Disposable? = null
    private var noInternetPopupShowing: Boolean = false
    private var alertDialog: AlertDialog? = null
    private val activityMap: HashMap<String, ArrayList<ActivityForegroundState?>> = hashMapOf()

    var isAllActivitiesDestroyed = true
        private set

    override fun onConfigurationChanged(newConfig: Configuration) {}

    override fun onLowMemory() {}

    override fun onTrimMemory(level: Int) {
        if (level == ComponentCallbacks2.TRIM_MEMORY_UI_HIDDEN) {
            isAppActive = false
            sendAnalyticsAppVisibilityChanged(false)
        }
    }

    override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {
        Timber.d("onActivityCreated ${activity.localClassName}")
        appRef.get()?.run {
            setBoolActivityOnSaveInstanceState(false)
            setCurrentActivity(activity)
            var listActivity: ArrayList<ActivityForegroundState?>? =
                activityMap[activity.localClassName]
            if (listActivity == null) {
                listActivity = ArrayList()
                activityMap[activity.localClassName] = listActivity
            }

            listActivity.add(ActivityForegroundState(activity))
            isAllActivitiesDestroyed = false
        }
    }

    override fun onActivityStarted(activity: Activity) {
        Timber.d("onActivityStarted: $activity")
        var listActivity = activityMap[activity.localClassName]

        if (listActivity == null) {
            listActivity = ArrayList()
            activityMap[activity.localClassName] = listActivity
            listActivity.add(ActivityForegroundState(activity))
        } else if (listActivity.isEmpty()) {
            listActivity.add(ActivityForegroundState(activity))
        }

        val actState = listActivity.find { it?.activityRef?.get() == activity }
        actState?.isInForeground = true
        val lastIsAppInForeground = isAppInForeground
        isAppInForeground = activityMap.any { entry ->
            entry.value.any { activityForegroundState ->
                activityForegroundState?.isInForeground == true
            }
        }
        if (lastIsAppInForeground != isAppInForeground) {
            Timber.e("onActivityStarted: changed isAppInForeground to $isAppInForeground ")
            foregroundStateListener?.invoke(isAppInForeground)
        }
    }

    override fun onActivityResumed(activity: Activity) {
        Timber.d("onActivityResumed: $activity")
        appRef.get()?.run {
            setCurrentActivity(activity)
            setBoolActivityOnSaveInstanceState(false)
        }

        receiverInternetConnectionStatus()

        if (!isAppActive) {
            sendAnalyticsAppVisibilityChanged(true)
            if (activity !is SplashActivity && activity !is LoginActivity && activity !is PermissionActivity
                && !Utils.checkLocationPermission(activity)
            ) {
                goToSplashScreen(activity)
            }
            if (!isNetworkConnected(activity)) {
                Timber.d("Internet - App Life cycle handler: Network not connected")
                appRef.get()?.handleChangeInNetworkConnection(false)
            }
            if (!isGPSEnabled(activity) && activity !is SplashActivity) {
                appRef.get()?.handleChangeInGPSConnection(false)
            }
        }
        isAppActive = true
    }

    private fun receiverInternetConnectionStatus() {
        //if (noInternetDisposable != null) noInternetDisposable?.dispose()
        noInternetDisposable =
            RxBus.listen(RxEvent.NoInternetConnection::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    if (!it.isConnected) {
                        Timber.d("Internet - RxEvent: Showing no internet dialog")
                        Timber.d("An event has been observed")
                        if (!noInternetPopupShowing) {
                            showNoInternetDialog()
                        }
                    } else {
                        safeDismissInternetDialog()
                    }
                }
    }

    private fun goToSplashScreen(activity: Activity) {
        val intent = Intent(activity, SplashActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
        activity.startActivity(intent)
        activity.finish()
    }

    override fun onActivityPaused(activity: Activity) {
        Timber.d("onActivityPaused $activity")
    }

    override fun onActivityStopped(activity: Activity) {
        Timber.d("onActivityStopped $activity")
        val actState =
            activityMap[activity.localClassName]?.find { it?.activityRef?.get() == activity }
        actState?.isInForeground = false

        val lastIsAppInForeground = isAppInForeground
        isAppInForeground = !activityMap.all { entry ->
            entry.value.all { activityForegroundState ->
                activityForegroundState?.isInForeground != true
            }
        }

        if (lastIsAppInForeground != isAppInForeground) {
            Timber.e("onActivityStopped: changed isAppInForeground to $isAppInForeground ")
            foregroundStateListener?.invoke(isAppInForeground)
        }
    }

    override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle) {
        Timber.d("onActivitySaveInstanceState $activity")
        appRef.get()?.setBoolActivityOnSaveInstanceState(true)
    }

    override fun onActivityDestroyed(activity: Activity) {
        if (activity.isTaskRoot && noInternetDisposable != null) {
            noInternetDisposable!!.dispose()
            Timber.d("It is successfully disposed")
        }
        Timber.d("AppLifeCycleHandler: Activity destroyed $activity")
        appRef.get()?.run {
            if (getCurrentActivity() == activity) {
                setCurrentActivity(null)
            }
        }
    }

    private fun safeDismissInternetDialog() {
        alertDialog?.let {
            if (it.isShowing) {
                val dialogActivity = scanDialogForActivity(it.context)
                dialogActivity?.let { activity ->
                    if (!(activity.isFinishing || activity.isDestroyed)) {
                        it.dismiss()
                    }
                }
            }
            alertDialog = null
        }
    }


    private fun scanDialogForActivity(cont: Context?): Activity? {
        return when (cont) {
            null -> null
            is Activity -> cont
            is ContextWrapper -> scanDialogForActivity(
                cont.baseContext
            )

            else -> null
        }
    }

    private fun showNoInternetDialog() {
        safeDismissInternetDialog()
        if (appRef.get()?.getCurrentActivity() != null) {
            val builder: AlertDialog.Builder? = appRef.get()?.getCurrentActivity()?.let {
                AlertDialog.Builder(it)
            }

            builder!!.setTitle(R.string.no_internet_title)
                .setMessage(R.string.no_internet_desc)
                .setPositiveButton(
                    R.string.no_internet_button
                ) { dialog, which ->
                    noInternetPopupShowing = false
                    dialog.dismiss()
                }

            alertDialog = builder.create()
            alertDialog!!.show()
            noInternetPopupShowing = true
        }
    }

    private fun isNetworkConnected(activity: Activity): Boolean {
        Timber.d("Internet - AppLifecyclehandler isNetwork Connected check")
        val connectivityManager =
            activity.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkCapabilities = connectivityManager.activeNetwork ?: return false
        val actNw =
            connectivityManager.getNetworkCapabilities(networkCapabilities) ?: return false
        return actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) ||
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) ||
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)
    }

    override fun onActivityPostDestroyed(activity: Activity) {
        super.onActivityPostDestroyed(activity)
        Timber.d("AppLifeCycleHandler: Activity post destroyed ${activity}")

        val listActivity = activityMap[activity.localClassName]
        listActivity?.run {
            removeIf { it?.activityRef?.get() == activity }
            if (isEmpty()) {
                activityMap.remove(activity.localClassName)
            }
        }
        isAllActivitiesDestroyed = activityMap.isEmpty()
        if (isAllActivitiesDestroyed) {
            appRef.get()?.run {
                checkIsAppAndCarSessionIsDestroyed()
            }
        }
    }

    private fun isGPSEnabled(context: Context?): Boolean {
        return Utils.checkGPSEnabled(context)
    }

    private fun sendAnalyticsAppVisibilityChanged(isInForeground: Boolean) {
        Analytics.logEvent(
            Event.OBU_SYSTEM_EVENT,
            if (isInForeground) Event.OBU_SYSTEM_APP_FOREGROUND else Event.OBU_SYSTEM_APP_BACKGROUND,
            Screen.OBU_APP
        )
    }

    private var foregroundStateListener: ((Boolean) -> Unit)? = null
    fun addForegroundStateCallback(callback: (Boolean) -> Unit) {
        if(foregroundStateListener != callback) {
            foregroundStateListener = callback
            callback.invoke(isAppInForeground)
        }
    }

    private inner class ActivityForegroundState(
        activity: Activity,
        var isInForeground: Boolean = false
    ) {
        val activityRef = WeakReference(activity)
        override fun toString(): String {
            return "activity:${activityRef.get()}, isInForeground=$isInForeground"
        }
    }

}

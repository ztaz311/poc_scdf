package com.ncs.breeze.common.helper.ehorizon

import android.content.Context
import android.location.Location
import android.util.SparseBooleanArray
import androidx.core.util.set
import com.breeze.model.NavigationZone
import com.breeze.model.constants.Constants
import com.breeze.model.enums.TrafficIncidentData
import com.mapbox.android.core.location.LocationEngineResult
import com.mapbox.api.directions.v5.models.DirectionsRoute
import com.mapbox.geojson.LineString
import com.mapbox.geojson.Point
import com.mapbox.maps.Style
import com.mapbox.turf.TurfConstants
import com.mapbox.turf.TurfMeasurement
import com.mapbox.turf.TurfMisc
import com.ncs.breeze.common.extensions.android.getOBUConnHelper
import com.ncs.breeze.common.rxbus.RxBus
import com.ncs.breeze.common.rxbus.RxEvent
import com.ncs.breeze.common.utils.BreezeTrafficIncidentsUtil
import com.ncs.breeze.common.utils.LocationBreezeManager
import com.ncs.breeze.common.utils.RoadObjectUtil
import com.ncs.breeze.common.utils.Utils
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import kotlinx.coroutines.withContext
import java.lang.ref.WeakReference
import java.util.concurrent.ConcurrentHashMap

class BreezeRoadObjectObserver(
    val breezeRoadObjectHandler: BreezeRoadObjectHandler,
    val breezeIncidentsUtil: BreezeTrafficIncidentsUtil,
    val mode: EHorizonMode,
    context: Context,
    val route: DirectionsRoute? = null
) {

    private val contextRef = WeakReference(context)

    var disposable: Disposable? = null

    private val mutex = Mutex()
    private val roadObjectMap: ConcurrentHashMap<String, RoadObjectThreshold> = ConcurrentHashMap()
    private val passedObjects: ConcurrentHashMap<String, SparseBooleanArray> = ConcurrentHashMap()


    /**
     * handles location callbacks for custom BreezeRoadObjectObserver
     */
    private val callbackLocationBreezeManager =
        object : LocationBreezeManager.CallBackLocationBreezeManager {

            override fun onSuccess(result: LocationEngineResult) {
                super.onSuccess(result)
                val currentLocation = result.lastLocation ?: return
                if (mode == EHorizonMode.CRUISE) {
                    roadObjectMap.forEach { roadObjectThresh ->
                        val passedDistances = getPassedDistances(roadObjectThresh)
                        val distanceMtr = TurfMeasurement.distance(
                            roadObjectThresh.value.point,
                            Point.fromLngLat(currentLocation.longitude, currentLocation.latitude),
                            TurfConstants.UNIT_METRES
                        )
                        roadObjectThresh.value.zone.allDistanceThreshold.forEach {
                            if (passedDistances[it]) {
                                if (distanceMtr > (it + DISTANCE_BUFFER)) {
                                    passedDistances[it] = false
                                }
                                return@forEach
                            }
                            if (distanceMtr <= it) {
                                setDistancesPassed(passedDistances, it)
                                validateAndShowNotification(roadObjectThresh, distanceMtr)
                                return@onSuccess
                            }
                        }
                    }
                } else {
                    roadObjectMap.forEach { roadObjectThresh ->
                        val passedDistances = getPassedDistances(roadObjectThresh)
                        val distanceMtr = TurfMeasurement.distance(
                            roadObjectThresh.value.point,
                            Point.fromLngLat(currentLocation.longitude, currentLocation.latitude),
                            TurfConstants.UNIT_METRES
                        )
                        roadObjectThresh.value.zone.allDistanceThreshold.forEach {
                            if (passedDistances[it]) {
                                if (distanceMtr > (it + DISTANCE_BUFFER)) {
                                    passedDistances[it] = false
                                }
                                return@forEach
                            }
                            if (distanceMtr <= it) {
                                setDistancesPassed(passedDistances, it)
                                route?.let { route ->
                                    // check if the incident's position is near to the route that user is taking or not
                                    CoroutineScope(Dispatchers.IO).launch {
                                        val geometry = LineString.fromPolyline(
                                            route.geometry()!!,
                                            Constants.POLYLINE_PRECISION
                                        )
                                        val feature = TurfMisc.nearestPointOnLine(
                                            roadObjectThresh.value.point,
                                            geometry.coordinates()
                                        )
                                        val distanceToRoadMtr = TurfMeasurement.distance(
                                            roadObjectThresh.value.point,
                                            feature.geometry() as Point, TurfConstants.UNIT_METRES
                                        )
                                        if (distanceToRoadMtr < roadObjectThresh.value.zone.maxRouteRadius) {
                                            validateAndShowNotification(
                                                roadObjectThresh,
                                                distanceMtr
                                            )
                                        }
                                    }
                                    return@onSuccess
                                }
                                validateAndShowNotification(roadObjectThresh, distanceMtr)
                                return@onSuccess
                            }
                        }
                    }
                }
            }
        }

    private fun setDistancesPassed(passedDistances: SparseBooleanArray, distance: Int) {
        for (i in 0 until passedDistances.size()) {
            val distanceThreshold = passedDistances.keyAt(i)
            if (distanceThreshold >= distance) {
                passedDistances[distanceThreshold] = true
            }
        }
    }

    private fun getPassedDistances(roadObjectThresh: Map.Entry<String, RoadObjectThreshold>): SparseBooleanArray {
        var passedDistances = passedObjects[roadObjectThresh.key]
        if (passedDistances == null) {
            passedDistances = SparseBooleanArray()
            roadObjectThresh.value.zone.allDistanceThreshold.forEach {
                passedDistances[it] = false
            }
            passedObjects[roadObjectThresh.key] = passedDistances
        }
        return passedDistances
    }

    private fun validateAndShowNotification(
        roadObjectThreshold: Map.Entry<String, RoadObjectThreshold>,
        distanceMtr: Double
    ) {
        val incidentTypeStr =
            RoadObjectUtil.parseIncidentTypeFromRoadObject(roadObjectThreshold.key)
        TrafficIncidentData.getIncidentType(incidentTypeStr)?.let { trafficIncident ->
            CoroutineScope(Dispatchers.IO).launch {
                val location = Location("").apply {
                    latitude = roadObjectThreshold.value.point.latitude()
                    longitude = roadObjectThreshold.value.point.longitude()
                }
                contextRef.get()?.let { context ->
                    val locationAddress = Utils.getStreetNameFromLocation(location, context)
                    withContext(Dispatchers.Main) {
                        showNotification(
                            trafficIncident.zone,
                            distanceMtr.toInt(),
                            roadObjectThreshold.key,
                            locationAddress
                        )
                    }
                }
            }
        }
    }

    fun startObserver() {
        CoroutineScope(Dispatchers.Default).launch {
            removeAndAddIncidentRoadObjects(true)
            listenToIncidentChanges()
        }
        LocationBreezeManager.getInstance()?.registerLocationCallback(callbackLocationBreezeManager)
    }


    fun stopObserver() {
        LocationBreezeManager.getInstance()?.removeLocationCallBack(callbackLocationBreezeManager)
        disposable?.dispose()
        passedObjects.clear()
    }


    private fun listenToIncidentChanges() {
        disposable?.dispose()
        disposable = RxBus.listen(RxEvent.IncidentDataRefresh::class.java)
            .observeOn(Schedulers.io())
            .subscribe {
                CoroutineScope(Dispatchers.Default).launch {
                    removeAndAddIncidentRoadObjects()
                }
            }
    }

    /**
     *  clears all road objects and add the values returned from the api
     *  Adds road objects along with the distance threshold required for notifications
     */
    private suspend fun removeAndAddIncidentRoadObjects(initPassedRO: Boolean = false) {
        mutex.withLock {
            val incidentMap = breezeIncidentsUtil.getLatestIncidentData()
            roadObjectMap.clear()
            if (initPassedRO) {
                passedObjects.clear()
            }
            TrafficIncidentData.values().forEach { trafficIncident ->
                if (trafficIncident.zone.maxDistanceMtr != 3000) {
                    return@forEach
                }
                val featureCollection = incidentMap.get(trafficIncident.type)
                if (featureCollection != null) {
                    for ((featureIndex, feature) in featureCollection.features()!!.withIndex()) {
                        if (feature.geometry() !is Point) {
                            continue
                        }

                        val objectName =
                            RoadObjectUtil.generateRoadObjectName(trafficIncident, featureIndex)
                        roadObjectMap[objectName] =
                            RoadObjectThreshold(feature.geometry() as Point, trafficIncident.zone)
                    }
                }
            }
        }
    }

    private fun showNotification(
        zone: NavigationZone,
        distance: Int,
        id: String,
        location: String?
    ) {
        breezeRoadObjectHandler.apply {
            showNotificationView(zone, distance, null, null, roadObjectID, location)
            if (zone.maxDistanceMtr <= 300) playVoiceOnce(zone) else playCustomVoiceOnce(
                zone,
                location
            )
        }

    }

    /**
     * not required
     */
    private fun initBreezeRoadStateHandler() {
        breezeRoadObjectHandler.addBreezeRoadObjectStateHandler(object :
            BreezeRoadObjectStateHandler {
            override fun onTerminateNotification(
                style: Style?,
                roadObjectId: String
            ) {
                //no op
            }
        })
    }

    companion object {
        private const val DISTANCE_BUFFER = 20

    }

    data class RoadObjectThreshold(val point: Point, val zone: NavigationZone)

}
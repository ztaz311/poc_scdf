package com.ncs.breeze.common.utils

interface SpeedLimitHandler {
    fun alertOnThreshold()
}
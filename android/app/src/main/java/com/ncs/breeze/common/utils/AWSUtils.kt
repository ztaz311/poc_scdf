package com.ncs.breeze.common.utils

import android.app.Activity
import android.app.Application
import androidx.core.os.bundleOf
import com.amplifyframework.analytics.pinpoint.AWSPinpointAnalyticsPlugin
import com.amplifyframework.auth.AuthCodeDeliveryDetails
import com.amplifyframework.auth.AuthException
import com.amplifyframework.auth.AuthProvider
import com.amplifyframework.auth.AuthUserAttribute
import com.amplifyframework.auth.AuthUserAttributeKey
import com.amplifyframework.auth.cognito.AWSCognitoAuthPlugin
import com.amplifyframework.auth.cognito.AWSCognitoAuthSession
import com.amplifyframework.auth.cognito.options.AWSCognitoAuthSignInOptions
import com.amplifyframework.auth.cognito.result.AWSCognitoAuthSignOutResult
import com.amplifyframework.auth.exceptions.SessionExpiredException
import com.amplifyframework.auth.exceptions.NotAuthorizedException
import com.amplifyframework.auth.exceptions.SignedOutException
import com.amplifyframework.auth.options.AuthFetchSessionOptions
import com.amplifyframework.auth.options.AuthSignOutOptions
import com.amplifyframework.auth.options.AuthSignUpOptions
import com.amplifyframework.auth.result.AuthResetPasswordResult
import com.amplifyframework.auth.result.AuthSessionResult
import com.amplifyframework.auth.result.AuthSignInResult
import com.amplifyframework.auth.result.AuthSignUpResult
import com.amplifyframework.core.Amplify
import com.amplifyframework.core.AmplifyConfiguration
import com.breeze.model.constants.INSTABUG
import com.google.firebase.crashlytics.ktx.crashlytics
import com.google.firebase.ktx.Firebase
import com.instabug.library.Instabug
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.Event
import com.ncs.breeze.common.analytics.Param
import com.ncs.breeze.common.helper.network.NetworkConnection
import timber.log.Timber
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

object AWSUtils : NetworkConnection.ConnectivityReceiverListener {
    private var isInitializeSuccess = false

    fun init(application: Application) {
        try {
            // Add this line, to include the Auth plugin.
            Amplify.addPlugin(AWSCognitoAuthPlugin())
            Amplify.addPlugin(AWSPinpointAnalyticsPlugin())
            val config = AmplifyConfiguration.builder(application.applicationContext)
                .devMenuEnabled(false)
                .build()
            Amplify.configure(config, application.applicationContext)
            isInitializeSuccess = true
            Timber.d("Initialized Amplify")
        } catch (error: Exception) {
            Timber.e(error, "Could not initialize Amplify")
            Analytics.logEvent(
                Event.OBU_SYSTEM_EVENT,
                bundleOf(
                    Param.KEY_EVENT_VALUE to Event.OBU_VERBOSE_ERROR,
                    Param.KEY_MESSAGE to "Code: ${error.javaClass.name}, message: ${error.localizedMessage}",
                )
            )
        }
    }

    fun checkAmplify(application: Application){
        if(!isInitializeSuccess){
            init(application)
        }
    }

    fun checkUserAuthSession(callback: AuthSessionCallback) {
        if (!isInitializeSuccess) {
            Timber.e("AWSUtils still not initialize")
            callback.onAuthFailed()
            return
        }
        Amplify.Auth.fetchAuthSession(
            { result ->
                val session = result as AWSCognitoAuthSession
                when (session.identityIdResult.type) {
                    AuthSessionResult.Type.SUCCESS -> {
                        Timber.d("checkUserAuthSession: IdentityId = ${session.identityIdResult.value}")
                        callback.onAuthSuccess(session.userPoolTokensResult.value?.idToken)
                        Analytics.setUserId(session.userSubResult.value ?: "")
                        InitObjectUtilsController.cognitoUserID = session.userSubResult.value
                        Timber.d("checkUserAuthSession: Result = ${session.userSubResult.value}")
                    }

                    AuthSessionResult.Type.FAILURE -> {
                        Timber.d(
                            "checkUserAuthSession: FAILURE = ${session.identityIdResult.error?.message}",
                            session.identityIdResult.error
                        )
                        callback.onAuthFailed()
                    }
                }
            },
            { error ->
                Timber.d("checkUserAuthSession: error = ${error.message}", error)
                Analytics.logExceptionEvent("Error fetching idToken: ${error.message}")
                callback.onAuthFailed()
            }
        )
    }

    suspend fun fetchAuthSessionSync(): String? = suspendCoroutine { cont ->
        Amplify.Auth.takeIf { isInitializeSuccess }?.fetchAuthSession({ result ->
            val session = result as AWSCognitoAuthSession
            when (session.identityIdResult.type) {
                AuthSessionResult.Type.SUCCESS -> {
                    if (session.identityIdResult.value != null) {
                        Timber.d("fetchAuthSessionSync: SUCCESS = ${session.identityIdResult.value}, ${session.userSubResult.value}")
                        cont.resume(session.userPoolTokensResult.value?.idToken)
                        Analytics.setUserId(session.userSubResult.value ?: "")
                        InitObjectUtilsController.cognitoUserID = session.userSubResult.value
                    } else {
                        cont.resume(null)
                    }
                }

                AuthSessionResult.Type.FAILURE -> {
                    //Timber.e("AWSUtils", session.identityIdResult.error )
                    Timber.d(
                        "fetchAuthSessionSync: FAILURE = ${session.identityIdResult.error?.message}",
                        session.identityIdResult.error
                    )
                    Firebase.crashlytics.log("Error in fetching valid idToken 2: ${session.identityIdResult.error}")
                    cont.resume(null)
                }
            }
        }, { error ->
            Firebase.crashlytics.log("Error in fetching valid idToken 2: ${error.message}")
            Timber.d("fetchAuthSessionSync: error = ${error.message}", error)
            cont.resume(null)
        }) ?: run {
            Timber.e("AWSUtils still not initialize")
            cont.resume(null)
        }
    }

    suspend fun fetchAuthAccessTokenSync(): Pair<String?, FetchTokenError> =
        suspendCoroutine { cont ->

            Amplify.Auth.takeIf { isInitializeSuccess }?.fetchAuthSession(
                AuthFetchSessionOptions.builder().forceRefresh(true).build(),
                { result ->
                    val session = result as? AWSCognitoAuthSession

                    when (session?.identityIdResult?.type) {
                        AuthSessionResult.Type.SUCCESS -> {
                            Timber.d("fetchAuthAccessTokenSync: SUCCESS")
                            cont.resume(
                                session.userPoolTokensResult.value?.accessToken to FetchTokenError.NONE
                            )
                        }

                        AuthSessionResult.Type.FAILURE -> {
                            val error = session.identityIdResult.error
                            Timber.d(
                                "fetchAuthAccessTokenSync: FAILURE = isNetworkConnected:$isNetworkConnected, error:${error}"
                            )
                            val errorType = when {
                                !isNetworkConnected -> FetchTokenError.NO_INTERNET
                                error is NotAuthorizedException
                                        || error is SessionExpiredException
                                        || error is SignedOutException
                                -> FetchTokenError.SIGN_OUT

                                else -> FetchTokenError.OTHER
                            }
                            cont.resume(null to errorType)
                        }

                        else -> {
                            Timber.d(
                                "fetchAuthAccessTokenSync: session = null "
                            )
                            cont.resume(null to FetchTokenError.OTHER)
                        }
                    }

                },
                { error ->
                    Timber.d("fetchAuthAccessTokenSync: error = ${error.message}", error)
                    cont.resume(null to FetchTokenError.OTHER)
                }
            ) ?: run {
                Timber.e("AWSUtils still not initialize")
                cont.resume(null to FetchTokenError.OTHER)
            }
        }

    fun signUp(
        username: String,
        password: String,
        options: AuthSignUpOptions,
        success: (AuthSignUpResult) -> Unit,
        fail: (AuthException) -> Unit
    ) {
        Amplify.Auth.takeIf { isInitializeSuccess }
            ?.signUp(username, password, options, success, fail)
            ?: fail.invoke(AuthException("Could not initialize Amplify", ""))
    }

    fun signIn(
        username: String,
        password: String,
        options: AWSCognitoAuthSignInOptions,
        success: (AuthSignInResult) -> Unit,
        fail: (AuthException) -> Unit
    ) {
        Amplify.Auth.takeIf { isInitializeSuccess }
            ?.signIn(username, password, options, success, fail)
            ?: fail.invoke(AuthException("Could not initialize Amplify", ""))
    }

    fun confirmSignIn(
        challengeResponse: String,
        success: (AuthSignInResult) -> Unit,
        fail: (AuthException) -> Unit
    ) {
        Amplify.Auth.takeIf { isInitializeSuccess }
            ?.confirmSignIn(challengeResponse, success, fail)
            ?: fail.invoke(AuthException("Could not initialize Amplify", ""))
    }

    fun signInWithSocialWebUI(
        provider: AuthProvider,
        callingActivity: Activity,
        success: (AuthSignInResult) -> Unit,
        fail: (AuthException) -> Unit
    ) {
        Timber.d("signInWithSocialWebUI $provider, $isInitializeSuccess")
        Amplify.Auth.takeIf { isInitializeSuccess }
            ?.signInWithSocialWebUI(provider, callingActivity, success, fail)
            ?: fail.invoke(AuthException("Could not initialize Amplify", ""))
    }

    fun signOut(callback: () -> Unit) {
        if (!isInitializeSuccess) {
            Timber.e("Could not initialize Amplify: signOut")
            callback.invoke()
            return
        }
        val options = AuthSignOutOptions.builder()
            .globalSignOut(true)
            .build()
        Amplify.Auth.signOut(options) { signOutResult ->
            if (signOutResult is AWSCognitoAuthSignOutResult.FailedSignOut) {
                Timber.e("Amplify.Auth.signOut failed", signOutResult.exception)
            } else {
                Timber.e("Amplify.Auth.signOut signOutResult $signOutResult")
            }
            callback.invoke()
        }
    }

    fun getCurrentUser() {
        if (!isInitializeSuccess) {
            Timber.e("Could not initialize Amplify: getCurrentUser")
            return
        }
        Amplify.Auth.getCurrentUser({
            Firebase.crashlytics.setUserId(it.userId)
            Instabug.setUserAttribute(INSTABUG.KEY_USER_ID, it.userId)
        }, {})
    }

    fun fetchUserAttributes(
        onSuccess: (List<AuthUserAttribute>) -> Unit,
        onError: (AuthException) -> Unit
    ) {
        if (!isInitializeSuccess) {
            Timber.e("Could not initialize Amplify: fetchUserAttributes")
            return
        }
        Amplify.Auth.fetchUserAttributes(onSuccess, onError)
    }

    fun resendUserAttributeConfirmationCode(
        attribute: AuthUserAttributeKey,
        onSuccess: (AuthCodeDeliveryDetails) -> Unit,
        onError: (AuthException) -> Unit
    ) {
        if (!isInitializeSuccess) {
            Timber.e("Could not initialize Amplify: resendUserAttributeConfirmationCode")
            return
        }

        Amplify.Auth.resendUserAttributeConfirmationCode(attribute, onSuccess, onError)
    }

    fun confirmUserAttribute(
        email: AuthUserAttributeKey,
        confirmationCode: String,
        onSuccess: () -> Unit,
        onError: (AuthException) -> Unit
    ) {
        if (!isInitializeSuccess) {
            Timber.e("Could not initialize Amplify: confirmUserAttribute")
            return
        }
        Amplify.Auth.confirmUserAttribute(email, confirmationCode, onSuccess, onError)
    }

    fun updatePassword(
        oldPassword: String,
        newPassword: String,
        onSuccess: () -> Unit,
        onError: (AuthException) -> Unit
    ) {
        if (!isInitializeSuccess) {
            Timber.e("Could not initialize Amplify: updatePassword")
            return
        }
        Amplify.Auth.updatePassword(oldPassword, newPassword, onSuccess, onError)
    }

    fun resetPassword(
        email: String,
        onSuccess: (AuthResetPasswordResult) -> Unit,
        onError: (AuthException) -> Unit
    ) {
        if (!isInitializeSuccess) {
            Timber.e("Could not initialize Amplify: resetPassword")
            return
        }
        Amplify.Auth.resetPassword(email, onSuccess, onError)
    }

    fun confirmResetPassword(
        retrieveUserName: String,
        password: String,
        otpText: String,
        onSuccess: () -> Unit,
        onError: (AuthException) -> Unit
    ) {
        if (!isInitializeSuccess) {
            Timber.e("Could not initialize Amplify: confirmResetPassword")
            return
        }
        Amplify.Auth.confirmResetPassword(retrieveUserName, password, otpText, onSuccess, onError)
    }

    fun confirmSignUp(
        phoneNumber: String,
        otpText: String,
        onSuccess: (value: AuthSignUpResult) -> Unit,
        onError: (value: AuthException) -> Unit
    ) {
        if (!isInitializeSuccess) {
            Timber.e("Could not initialize Amplify: confirmSignUp")
            return
        }
        Amplify.Auth.confirmSignUp(phoneNumber, otpText, onSuccess, onError)
    }

    fun resendSignUpCode(
        phoneNumber: String,
        onSuccess: (AuthCodeDeliveryDetails) -> Unit,
        onError: (AuthException) -> Unit
    ) {
        if (!isInitializeSuccess) {
            Timber.e("Could not initialize Amplify: resendSignUpCode")
            return
        }
        Amplify.Auth.resendSignUpCode(phoneNumber, onSuccess, onError)
    }

    interface AuthSessionCallback {
        fun onAuthSuccess(token: String?)
        fun onAuthFailed()
    }

    private var isNetworkConnected = false
    override fun setCurrentNetworkState(isConnected: Boolean) {
        isNetworkConnected = isConnected
    }

    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        isNetworkConnected = isConnected
    }
}

enum class FetchTokenError {
    NONE,
    NO_INTERNET,
    SIGN_OUT,
    OTHER
}
package com.ncs.breeze.ui.dashboard.fragments.view

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.ncs.breeze.R
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.Event
import com.ncs.breeze.common.analytics.Screen
import com.ncs.breeze.databinding.FragmentTransactionFilterBinding

class TransactionFilterDialogFragment : BottomSheetDialogFragment() {

    private var binding: FragmentTransactionFilterBinding? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentTransactionFilterBinding.inflate(inflater)
        binding?.run {
            tvClose.setOnClickListener {
                Analytics.logClickEvent(
                    Event.TRANSACTION_FILTER_TRANSACTION_TYPE_CLOSE,
                    Screen.TRIP_LOG
                )
                dismiss() }
            tvAll.setOnClickListener { selectFilter(TransactionFilter.ALL) }
            tvErpOnly.setOnClickListener { selectFilter(TransactionFilter.ERP_ONLY) }
            tvParkingOnly.setOnClickListener { selectFilter(TransactionFilter.PARKING_ONLY) }
        }

        return binding?.root
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState) as BottomSheetDialog
        dialog.behavior.isDraggable = false
        dialog.setCanceledOnTouchOutside(false)
        dialog.setCancelable(false)
        return dialog
    }

    override fun getTheme(): Int {
        return R.style.TopRoundCornerBottomSheetDialog
    }

    private fun selectFilter(filter: TransactionFilter) {
        val eventValue = when (filter) {
            TransactionFilter.ERP_ONLY -> Event.TRANSACTION_FILTER_TRANSACTION_TYPE_ERP_ONLY
            TransactionFilter.PARKING_ONLY -> Event.TRANSACTION_FILTER_TRANSACTION_TYPE_PARKING_ONLY
            else -> Event.TRANSACTION_FILTER_TRANSACTION_TYPE_ALL
        }
        Analytics.logClickEvent(eventValue, Screen.TRIP_LOG)
        (parentFragment as? FilterSelectedCallback)?.onFilterSelected(filter)
        dismiss()
    }

    interface FilterSelectedCallback {
        fun onFilterSelected(filter: TransactionFilter)
    }
}

enum class TransactionFilter(val value: String?) {
    ALL(null),
    ERP_ONLY("erp"),
    PARKING_ONLY("parking");

    companion object {
        fun fromValue(value: String?): TransactionFilter {
            return when (value) {
                ERP_ONLY.value -> ERP_ONLY
                PARKING_ONLY.value -> PARKING_ONLY
                else -> ALL
            }
        }
    }
}
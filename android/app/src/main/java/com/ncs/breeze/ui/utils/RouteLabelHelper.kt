package com.ncs.breeze.ui.utils

import android.content.Context
import com.breeze.model.enums.RoutePreference
import com.breeze.model.extensions.toBitmap
import com.ncs.breeze.common.utils.RoutePlanningConsts
import com.google.gson.JsonObject
import com.mapbox.geojson.Feature
import com.mapbox.geojson.FeatureCollection
import com.mapbox.geojson.LineString
import com.mapbox.geojson.Point
import com.mapbox.maps.MapboxMap
import com.mapbox.maps.Style
import com.mapbox.maps.extension.style.expressions.generated.Expression
import com.mapbox.maps.extension.style.layers.addLayer
import com.mapbox.maps.extension.style.layers.generated.symbolLayer
import com.mapbox.maps.extension.style.layers.properties.generated.IconAnchor
import com.mapbox.maps.extension.style.sources.addSource
import com.mapbox.maps.extension.style.sources.generated.geoJsonSource
import com.mapbox.turf.TurfMeasurement
import com.ncs.breeze.R

class RouteLabelHelper(
    val context: Context,
    val mapboxMap: MapboxMap
) {

    companion object {
        val STYLE_IMAGE_MAP: HashMap<String, Int> = hashMapOf(
            RoutePlanningConsts.ROUTE_LABLE_IMG_FASTEST to R.drawable.img_label_rotue_fastest,
            RoutePlanningConsts.ROUTE_LABLE_IMG_CHEAPEST to R.drawable.img_label_rotue_cheapest,
            RoutePlanningConsts.ROUTE_LABLE_IMG_SHORTEST to R.drawable.img_label_rotue_shortest,
            RoutePlanningConsts.ROUTE_LABLE_IMG_FASTEST_CHEAPEST to R.drawable.img_label_rotue_fastest_cheapest,
            RoutePlanningConsts.ROUTE_LABLE_IMG_SHORTEST_CHEAPEST to R.drawable.img_label_rotue_shortestt_cheapest,
            RoutePlanningConsts.ROUTE_LABLE_IMG_FASTEST_SHORTEST to R.drawable.img_label_rotue_fastest_shortest,
            RoutePlanningConsts.ROUTE_LABLE_IMG_FASTEST_CHEAPEST_SHORTEST to R.drawable.img_label_rotue_fastest_shortestt_cheapest,
            RoutePlanningConsts.ROUTE_LABLE_IMG_FASTEST_INVERTED to R.drawable.img_label_rotue_fastest_i,
            RoutePlanningConsts.ROUTE_LABLE_IMG_CHEAPEST_INVERTED to R.drawable.img_label_rotue_cheapest_i,
            RoutePlanningConsts.ROUTE_LABLE_IMG_SHORTEST_INVERTED to R.drawable.img_label_rotue_shortest_i,
            RoutePlanningConsts.ROUTE_LABLE_IMG_FASTEST_CHEAPEST_INVERTED to R.drawable.img_label_rotue_fastest_cheapest_i,
            RoutePlanningConsts.ROUTE_LABLE_IMG_SHORTEST_CHEAPEST_INVERTED to R.drawable.img_label_rotue_shortestt_cheapest_i,
            RoutePlanningConsts.ROUTE_LABLE_IMG_FASTEST_SHORTEST_INVERTED to R.drawable.img_label_rotue_fastest_shortest_i,
            RoutePlanningConsts.ROUTE_LABLE_IMG_FASTEST_CHEAPEST_SHORTEST_INVERTED to R.drawable.img_label_rotue_fastest_shortestt_cheapest_i,
        )
    }

    private val lableLayers = ArrayList<String>()
    private val styleLayers = ArrayList<String>()

    fun initRouteLabelStyle(style: Style) {
        for ((key, value) in STYLE_IMAGE_MAP) {
            value.toBitmap(context)?.let {
                style.addImage(key, it, false)
            }
        }
    }

    fun showRouteLabel(
        geometry: LineString,
        routePreference: RoutePreference,
        isRootSelected: Boolean
    ) {
        val labelImage: String = getLabelImageFor(routePreference, isRootSelected) ?: return
        val position = getLabelPositionOnRoute(geometry, routePreference)
        val featureListForRouteLabel = ArrayList<Feature>()
        val feature = Feature.fromGeometry(
            position,
            JsonObject()
        )
//        feature.addStringProperty(
//            RoutePlanningConsts.ROUTE_LABEL_TEXT_STRING, routePreference.name
//        )
        featureListForRouteLabel.add(TurfMeasurement.center(feature))

        val mFeatureCollection = FeatureCollection.fromFeatures(featureListForRouteLabel)
        val icon_size_route_label_image = Expression.interpolate {
            linear()
            zoom()
            stop {
                literal(1.0)
                literal(0.0)
            }
            stop {
                literal(8.9)
                literal(0.0)
            }
            stop {
                literal(9.0)
                literal(1.0)
            }
            stop {
                literal(13.0)
                literal(1.0)
            }
        }

//        var icon_size_exp_route = Expression.interpolate {
//            linear()
//            zoom()
//            stop {
//                literal(5.0)
//                literal(0.0)
//            }
//            stop {
//                literal(13.0)
//                literal(1.1)
//            }
//        }
//
//        var text_size_exp_route = Expression.interpolate {
//            linear()
//            zoom()
//            stop {
//                literal(5.0)
//                literal(0.0)
//            }
//            stop {
//                literal(13.0)
//                literal(15.0)
//            }
//        }
//
//        var text_size_exp_erp = Expression.interpolate {
//            linear()
//            zoom()
//            stop {
//                literal(9.0)
//                literal(0.0)
//            }
//            stop {
//                literal(13.0)
//                literal(15.0)
//            }
//        }

        val layerName = RoutePlanningConsts.ROUTE_ERP_SOURCE + "_" + routePreference.name
        lableLayers.add(layerName)

        val styleName = RoutePlanningConsts.ROUTE_ERP_LAYER + "_" + routePreference.name
        styleLayers.add(styleName)

        mapboxMap.getStyle()!!.addSource(
            geoJsonSource(layerName) {
                featureCollection(mFeatureCollection)
            }
        )

        mapboxMap.getStyle()!!.addLayer(
            symbolLayer(styleName, layerName) {
                iconImage(
                    labelImage
                )
                iconSize(icon_size_route_label_image)
                iconAnchor(IconAnchor.BOTTOM)
                iconAllowOverlap(true)
                //textField(Expression.get(RoutePlanningConsts.ROUTE_LABEL_TEXT_STRING))
                //textColor(context.getColor(R.color.black))
                //textAnchor(TextAnchor.CENTER)
                //textOffset(listOf(0.55, -0.1))
                //textSize(text_size_exp_erp)
                //textAllowOverlap(true)
            }
        )
    }

    fun clearRoadLabels() {
        for (style in styleLayers) {
            mapboxMap.getStyle()!!.removeStyleLayer(style)
        }
        styleLayers.clear()

        for (layer in lableLayers) {
            mapboxMap.getStyle()!!.removeStyleSource(layer)
        }
        lableLayers.clear()
    }

    private fun getLabelPositionOnRoute(
        geometry: LineString,
        routePreference: RoutePreference
    ): Point? {
        var routeBreaker: Float = 0.5f
        when (routePreference) {
            RoutePreference.FASTEST_CHEAPEST_SHORTEST_ROUTE -> {
                routeBreaker = 0.5f
            }
            RoutePreference.FASTEST_SHORTEST_ROUTE -> {
                routeBreaker = 0.5f
            }
            RoutePreference.SHORTEST_CHEAPEST_ROUTE -> {
                routeBreaker = 0.8f
            }
            RoutePreference.FASTEST_CHEAPEST_ROUTE -> {
                routeBreaker = 0.3f
            }
            RoutePreference.SHORTEST_ROUTE -> {
                routeBreaker = 0.5f
            }
            RoutePreference.CHEAPEST_ROUTE -> {
                routeBreaker = 0.8f
            }
            RoutePreference.FASTEST_ROUTE -> {
                routeBreaker = 0.3f
            }
            else -> {}
        }
        return geometry.coordinates()
            .get((geometry.coordinates().size * routeBreaker).toInt())
    }

    private fun getLabelImageFor(
        routePreference: RoutePreference,
        isRouteSelected: Boolean
    ): String? {
        if (isRouteSelected) {
            when (routePreference) {
                RoutePreference.FASTEST_CHEAPEST_SHORTEST_ROUTE -> {
                    return RoutePlanningConsts.ROUTE_LABLE_IMG_FASTEST_CHEAPEST_SHORTEST
                }
                RoutePreference.FASTEST_SHORTEST_ROUTE -> {
                    return RoutePlanningConsts.ROUTE_LABLE_IMG_FASTEST_SHORTEST
                }
                RoutePreference.SHORTEST_CHEAPEST_ROUTE -> {
                    return RoutePlanningConsts.ROUTE_LABLE_IMG_SHORTEST_CHEAPEST
                }
                RoutePreference.FASTEST_CHEAPEST_ROUTE -> {
                    return RoutePlanningConsts.ROUTE_LABLE_IMG_FASTEST_CHEAPEST
                }
                RoutePreference.SHORTEST_ROUTE -> {
                    return RoutePlanningConsts.ROUTE_LABLE_IMG_SHORTEST
                }
                RoutePreference.CHEAPEST_ROUTE -> {
                    return RoutePlanningConsts.ROUTE_LABLE_IMG_CHEAPEST
                }
                RoutePreference.FASTEST_ROUTE -> {
                    return RoutePlanningConsts.ROUTE_LABLE_IMG_FASTEST
                }
                else -> null
            }
        } else {
            when (routePreference) {
                RoutePreference.FASTEST_CHEAPEST_SHORTEST_ROUTE -> {
                    return RoutePlanningConsts.ROUTE_LABLE_IMG_FASTEST_CHEAPEST_SHORTEST_INVERTED
                }
                RoutePreference.FASTEST_SHORTEST_ROUTE -> {
                    return RoutePlanningConsts.ROUTE_LABLE_IMG_FASTEST_SHORTEST_INVERTED
                }
                RoutePreference.SHORTEST_CHEAPEST_ROUTE -> {
                    return RoutePlanningConsts.ROUTE_LABLE_IMG_SHORTEST_CHEAPEST_INVERTED
                }
                RoutePreference.FASTEST_CHEAPEST_ROUTE -> {
                    return RoutePlanningConsts.ROUTE_LABLE_IMG_FASTEST_CHEAPEST_INVERTED
                }
                RoutePreference.SHORTEST_ROUTE -> {
                    return RoutePlanningConsts.ROUTE_LABLE_IMG_SHORTEST_INVERTED
                }
                RoutePreference.CHEAPEST_ROUTE -> {
                    return RoutePlanningConsts.ROUTE_LABLE_IMG_CHEAPEST_INVERTED
                }
                RoutePreference.FASTEST_ROUTE -> {
                    return RoutePlanningConsts.ROUTE_LABLE_IMG_FASTEST_INVERTED
                }
                else -> null
            }
        }
        return null
    }

}
package com.ncs.breeze.common.event

import com.ncs.breeze.common.model.rx.ToAppRxEvent
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

object ToAppRx {

    private val publisher = PublishSubject.create<ToAppRxEvent>()

    fun postEvent(event: ToAppRxEvent) {
        publisher.onNext(event)
    }

    fun listenEvent(eventType: Class<ToAppRxEvent>): Observable<ToAppRxEvent> =
        publisher.ofType(eventType)
}










package com.ncs.breeze.di.module.fragment

import com.ncs.breeze.ui.dashboard.fragments.view.FAQFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class FAQModule {
    @ContributesAndroidInjector
    abstract fun contributeFAQFragment(): FAQFragment
}
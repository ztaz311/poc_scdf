package com.ncs.breeze.ui.rn

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import com.facebook.react.ReactApplication
import com.facebook.react.modules.core.PermissionAwareActivity
import com.facebook.react.modules.core.PermissionListener
import com.ncs.breeze.App

open class CustomReactFragment : Fragment(),
    PermissionAwareActivity {
    var reactDelegate: CustomReactDelegate? = null
    private var mPermissionListener: PermissionListener? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val mainComponentName = arguments?.getString(ARG_COMPONENT_NAME)
        var launchOptions = arguments?.getBundle(ARG_LAUNCH_OPTIONS)

        reactDelegate =
            (this.requireActivity().application as? ReactApplication)?.reactNativeHost?.let {
                CustomReactDelegate(
                    activity,
                    App.instance?.reactNativeHost,
                    mainComponentName,
                    launchOptions
                ).apply { loadApp() }
            }
        return reactDelegate?.reactRootView
    }

    override fun onResume() {
        super.onResume()
        reactDelegate?.onHostResume()
    }

    override fun onPause() {
        super.onPause()
        reactDelegate?.onHostPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        reactDelegate?.onHostDestroy(arguments?.getBoolean(ARG_SHOULD_CLEAN) == true)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        reactDelegate?.onActivityResult(requestCode, resultCode, data, false)
    }
//
//    fun onBackPressed(): Boolean {
//        return reactDelegate?.onBackPressed()?:true
//    }
//
//    fun onKeyUp(keyCode: Int, event: KeyEvent?): Boolean {
//        return reactDelegate?.shouldShowDevMenuOrReload(keyCode, event)?:true
//    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (mPermissionListener != null && mPermissionListener!!.onRequestPermissionsResult(
                requestCode,
                permissions,
                grantResults
            )
        ) {
            mPermissionListener = null
        }
    }


    override fun checkPermission(permission: String, pid: Int, uid: Int): Int {
        return this.requireActivity().checkPermission(permission, pid, uid)
    }

    override fun checkSelfPermission(permission: String): Int {
        return this.requireActivity().checkSelfPermission(permission)
    }

    override fun requestPermissions(
        permissions: Array<String>,
        requestCode: Int,
        listener: PermissionListener
    ) {
        mPermissionListener = listener
        this.requestPermissions(permissions, requestCode)
    }

    companion object {
        protected const val ARG_COMPONENT_NAME = "arg_component_name"
        protected const val ARG_LAUNCH_OPTIONS = "arg_launch_options"
        protected const val ARG_SHOULD_CLEAN = "ARG_SHOULD_CLEAN"
        fun newInstance(
            componentName: String?,
            launchOptions: Bundle?,
            cleanOnDestroyed: Boolean = true
        ) =
            CustomReactFragment().apply {
                arguments = bundleOf(
                    ARG_COMPONENT_NAME to componentName,
                    ARG_LAUNCH_OPTIONS to launchOptions,
                    ARG_SHOULD_CLEAN to cleanOnDestroyed
                )
            }
    }
}
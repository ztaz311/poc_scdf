package com.ncs.breeze.di.module.fragment


class RoutePlanningModule(s: String, s1: String) {
    private var mTextResource = s
    private var mTitleResource = s1

    fun getText(): String {
        return mTextResource
    }

    fun getTitle(): String {
        return mTitleResource
    }

}
package com.ncs.breeze.ui.dashboard.fragments.view

import android.app.AlertDialog
import android.os.Bundle
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.FrameLayout
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import com.ncs.breeze.R
import com.ncs.breeze.common.extensions.android.getApp
import com.ncs.breeze.common.extensions.android.getAppPreference
import com.ncs.breeze.common.extensions.android.getBreezeUserPreference
import com.ncs.breeze.common.extensions.android.getOBUConnHelper
import com.breeze.model.extensions.dpToPx
import com.breeze.model.enums.DebugMode
import com.ncs.breeze.common.storage.AppPrefsKey
import com.ncs.breeze.common.utils.BreezeFileLoggingUtil
import com.ncs.breeze.common.utils.InitObjectUtilsController
import com.ncs.breeze.databinding.FragmentDebugBinding
import com.ncs.breeze.ui.base.BaseFragment
import com.ncs.breeze.ui.dashboard.activity.DashboardActivity
import com.ncs.breeze.ui.dashboard.fragments.viewmodel.DebugViewModel
import javax.inject.Inject

class DebugFragment : BaseFragment<FragmentDebugBinding, DebugViewModel>() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val viewModel: DebugViewModel by viewModels { viewModelFactory }

    @Inject
    lateinit var breezeFileLoggingUtil: BreezeFileLoggingUtil

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initialize()
    }

    fun initialize() {

        with(viewBinding) {
            tvUserID.text = "${InitObjectUtilsController.cognitoUserID}"
            btnUpload.setBGThemed()
            btnUpload.setOnClickListener {
                showDialogToUpload()
            }

            btnBack.setOnClickListener {
                onBackPressed()
            }
            switchOBUSimulation.isChecked =
                requireContext().getBreezeUserPreference().isEnableOBUSimulation()
            switchOBUSimulation.setOnCheckedChangeListener { buttonView, isChecked ->
                buttonView.context.getBreezeUserPreference().toggleOBUSimulation(isChecked)
                if (isChecked) {
                    enableOBUSimulation()
                } else {
                    disableOBUSimulation()
                }
            }

            switchAutoDrive.isChecked =
                requireContext().getBreezeUserPreference().isEnableAutoDriveSimulation()
            switchAutoDrive.setOnCheckedChangeListener { buttonView, isChecked ->
                getApp()?.isAutoDriveSimulation = isChecked
                buttonView.context.getBreezeUserPreference().toggleAutoDriveSimulation(isChecked)
            }
            btnDisableDebug.setOnClickListener {
                disableDebugMode()
            }
        }
    }

    private fun enableOBUSimulation() {
        getOBUConnHelper()?.enableOBUSimulation()
    }

    private fun disableOBUSimulation() {
        getOBUConnHelper()?.disableOBUSimulation {
            context?.getApp()?.initOBU()
        }
    }

    override fun inflateViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?,
        attachToContainer: Boolean
    ) = FragmentDebugBinding.inflate(inflater, container, attachToContainer)
    
    override fun getViewModelReference(): DebugViewModel {
        return viewModel
    }

    override fun getScreenName(): String {
        return "Debug_Screen"
    }


    private fun showDialogToUpload() {

        // Set up the input
        val input = EditText(requireContext()).apply {
            // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
            hint = "Description"
            inputType = InputType.TYPE_CLASS_TEXT
        }
        FrameLayout.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        ).apply {
            leftMargin = 20.dpToPx()
            rightMargin = 20.dpToPx()
        }.let {
            input.layoutParams = it
        }
        val container = FrameLayout(requireContext())
        container.addView(input)

        AlertDialog.Builder(context).setMessage(R.string.feedback_message)
            .setTitle(R.string.feedback_title)
            .setView(container)
            .setPositiveButton(
                R.string.feedback_submit
            ) { dialog, _ ->
                breezeFileLoggingUtil.enableLog(input.text.toString())
                dialog.dismiss()
            }
            .setNegativeButton(
                R.string.feedback_cancel
            ) { dialog, _ ->
                dialog.dismiss()
            }.show()
    }

    private fun disableDebugMode() {
        context?.getAppPreference()?.saveInt(AppPrefsKey.DEBUG_MODE, DebugMode.OFF.value)
        ((activity as? DashboardActivity)?.supportFragmentManager?.findFragmentByTag(
            SettingsFragment.TAG
        ) as? SettingsFragment)?.toggleDebugMode(DebugMode.OFF)
        breezeFileLoggingUtil.turnOffLog()
        disableOBUSimulation()
        onBackPressed()
    }

    companion object {
        const val TAG = "DebugFragment"
    }
}
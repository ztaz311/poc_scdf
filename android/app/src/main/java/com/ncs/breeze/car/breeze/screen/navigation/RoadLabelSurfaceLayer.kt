package com.ncs.breeze.car.breeze.screen.navigation

import android.graphics.Color
import androidx.car.app.CarContext
import com.mapbox.androidauto.internal.extensions.styleFlow

import com.mapbox.car.breeze.model.RoadLabelOptions
import com.mapbox.maps.Style
import com.mapbox.maps.extension.androidauto.MapboxCarMapObserver
import com.mapbox.maps.extension.androidauto.MapboxCarMapSurface
import com.mapbox.maps.plugin.locationcomponent.LocationComponentConstants
import com.mapbox.navigation.base.ExperimentalPreviewMapboxNavigationAPI
import com.mapbox.navigation.base.road.model.RoadComponent
import com.mapbox.navigation.core.lifecycle.MapboxNavigationApp
import com.ncs.breeze.car.breeze.utils.BreezeUserPreferencesUtil
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch


//todo bangnv RoadLabelSurfaceLayer check this class
class RoadLabelSurfaceLayer(
    val carContext: CarContext,
) : MapboxCarMapObserver {

    private val roadLabelRenderer = RoadLabelRenderer()
    private var mStyle: Style? = null

    override fun onAttached(mapboxCarMapSurface: MapboxCarMapSurface) {
        super.onAttached(mapboxCarMapSurface)
        CoroutineScope(Dispatchers.Main).launch {
            mapboxCarMapSurface.styleFlow().collectLatest { style ->
                mStyle = style
            }
        }
    }

    @OptIn(ExperimentalPreviewMapboxNavigationAPI::class)
    override fun onDetached(mapboxCarMapSurface: MapboxCarMapSurface) {
        mStyle?.removeStyleLayer(CAR_NAVIGATION_VIEW_LAYER_ID)
        MapboxNavigationApp.current()?.unregisterEHorizonObserver(
            roadNameObserver
        )
        super.onDetached(mapboxCarMapSurface)

    }

    private val roadNameObserver =
        object : RoadNameObserver({ MapboxNavigationApp.current() }) {
            override fun onRoadUpdate(currentRoadName: RoadComponent?) {
                roadLabelRenderer.render(
                    currentRoadName?.text,
                    roadLabelOptions()
                )
                //carTextLayerHost.offerBitmap(bitmap)
            }
        }

    private fun roadLabelOptions(): RoadLabelOptions =
        if (BreezeUserPreferencesUtil.isDarkTheme(carContext)) {
            DARK_OPTIONS
        } else {
            LIGHT_OPTIONS
        }

    private companion object {
        private const val CAR_NAVIGATION_VIEW_LAYER_ID = "car_road_label_layer_id"
        private const val BELOW_LAYER = LocationComponentConstants.LOCATION_INDICATOR_LAYER

        private val DARK_OPTIONS = RoadLabelOptions.Builder()
            .shadowColor(null)
            .roundedLabelColor(Color.BLACK)
            .textColor(Color.WHITE)
            .build()

        private val LIGHT_OPTIONS = RoadLabelOptions.Builder()
            .roundedLabelColor(Color.parseColor("#1E2230"))
            .textColor(Color.WHITE)
            .build()
    }
}

@file:JvmName("Event")

package com.ncs.breeze.common.analytics

/**
 * @author Aiswarya
 */
object Event {

    //common_event
    const val BACK = "back"
    const val NEXT = "next"
    const val CLOSE = "close"
    const val DONE = "done"
    const val POPUP_OPEN = "popup_open"
    const val POPUP_CLOSE = "popup_close"
    const val USER_CLICK = "user_click"
    const val USER_EDIT = "user_edit"
    const val USER_POPUP = "user_popup"
    const val USER_TOGGLE = "user_toggle"
    const val USER_SELECTION = "user_selection"
    const val USER_NOTIFICATION = "user_notification"
    const val NOTIFICATION_OPEN = "notification_open"
    const val NOTIFICATION_CLOSE = "notification_close"
    const val NOTIFICATION_CLICK = "notification_click"
    const val SAVE = "save"
    const val YES = "yes"
    const val LOCATION_DATA = "location_data"
    const val POI_LAYER = "poi_layer"
    const val EXCEPTION_HANDLER = "exception_handler"

    const val PAGE_VIEW = "page_view"
    const val DESTINATION_ARRIVED = "destination_arrived"

    // onboarding signin main
    const val CREATE_ACCOUNT = "create_acc"
    const val SIGNIN = "signin"
    const val SIGNIN_FACEBOOK = "signin_facebook"
    const val SIGNIN_GOOGLE = "signin_google"
    const val METHOD_FACEBOOK = "facebook "
    const val METHOD_GOOGLE = "google"
    const val PHOTO_MEDIA_PERMISSION = "photomedia_permission"
    const val LOCATION_PERMISSION = "location_permission"
    const val MOBILE_INPUT = "mobile_num_input"
    const val WELCOME_SCREEN_POPUP = "welcome"

    //onboarding_tnc
    const val TNC_DECLINE = "decline"
    const val TNC_ACCEPT = "accept"

    //onboarding_mobile_signup
    const val SIGNUP_SIGNIN = "signin" //Uncoded function

    //onboarding_sms_verify
    const val SMS_RESEND = "resend"
    const val SMS_OTP_INPUT = "otp_input"

    //onboarding_username
    const val USRNME_INPUT = "username_input"
    const val ONBOARDING_SIGNUP_SUCCESS = "signup_success" //VerifyOTP (UNUSED)

    //onboarding_signin
    const val SIGNIN_SIGNUP = "signup" //Uncoded function
    const val SIGNIN_LOGIN = "login"


    //onboarding_create_account_step1
    const val NAME = "name"
    const val EMAIL = "email"
    const val PHONE = "phone"
    const val PASSWORD = "password"
    const val TERMS_OF_SERVICE = "terms_of_service"
    const val PRIVACY_POLICY = "privacy_policy"
    const val AGREED_RADIO = "agreed_terms_and_policy"
    const val HIDE_PASSWORD = "hide_password"
    const val UNHIDE_PASSWORD = "unhide_password"

    // onboarding_create_account_step2
    const val OTP = "otp"  //ForgotVerifyOTP and forget Password (UNUSED)
    const val RESEND = "resend"//ForgotVerifyOTP and forget Password (UNUSED)

    //onboarding_signin //old
    const val FORGOT_PASSWORD = "forgot password"//old

    //onboarding_signin_forgot_password_step2//old
    const val NEW_PASSWORD = "new_password";//old
    const val ONBOARDING_FORGOT_PASSWORD_SUCCESS = "onboarding_forgot_password_success"//old

    //permission
    const val GALLERY_PERMISSION = "gallery_permission"
    const val GALLERY_PERMISSION_ALLOW = "gallery_permission_allow"
    const val GALLERY_PERMISSION_DENY = "gallery__permission_deny"
    const val CONTACT_PERMISSION_ALLOW = "calendar_permission_allow"
    const val CONTACT_PERMISSION_DENY = "calendar_permission_deny"
    const val CAMERA_PERMISSION = "camera_permission"
    const val CAMERA_PERMISSION_ALLOW = "camera_permission_allow"
    const val CAMERA_PERMISSION_DENY = "camera_permission_deny"
    const val PHOTO_MEDIA_ALLOW = "photomedia_permission_allow"
    const val PHOTO_MEDIA_DENY = "photomedia_permission_deny"
    const val LOCATION_PERMISSION_ALLOW = "location_permission_allow"
    const val LOCATION_PERMISSION_DENY = "location_permission_deny"

    //home
    const val CALENDAR_PERMISSION_ALLOW = "calendar_permission_allow"
    const val CALENDAR_PERMISSION_DENY = "calendar_permission_deny"
    const val POPUP_CAR_PARK_NUDGE = "carpark_nudge"
    const val POPUP_SAVED_ADDRESSES_NUDGE = "saved_addresses_nudge"
    const val POPUP_EXPLORE_MAP_NUDGE = "explore_nudge"
    const val POPUP_ADD_EMAIL_NOW = "popup_add_email_now"
    const val POPUP_ADD_EMAIL_LATER = "popup_add_email_later"
    const val SEARCH = "search"
    const val PARKING = "parking"
    const val RECENTER = "recenter"
    const val REPORT_ISSUE = "report_issue"
    const val ADD_FAV_DESTINATION = "add_fav_destination"
    const val FAV_DESTINATION_SELECT = "fav_destination_select"
    const val VIEW_ALL_FAV = "view_all_favourites"
    const val RECENT_DESINATION = "recent_destination"
    const val ADD_ARRIVAL = "add_arrival"
    const val FAV_ARRIVAL = "fav_arrival"
    const val FAVORITE_SELECT = "favourite_select"  //Not in the list
    const val VIEW_ALL_ARRIVAL = "view_all_arrival"
    const val HOME_TAB = "home_tab"
    const val NOTIFY_ARRIVAL_TAB = "notify_arrival_tab"
    const val MORE_TAB = "more_tab"
    const val TRAVEL_LOG = "travel_log"
    const val FAVOURITES = "favourites"
    const val SETTINGS = "settings"
    const val PINCH_MAP = "pinch_map"
    const val DRAG_MAP = "drag_map"
    const val ROTATE_MAP = "rotate_map"
    const val SWIPE_UP = "swipe_up"
    const val SWIPE_DOWN = "swipe_down"
    const val PARKING_ICON_ON = "parking_icon_on"
    const val PARKING_ICON_OFF = "parking_icon_off"
    const val NAVIGATE_HERE_PARKING = "navigate_here_parking"
    const val CARPARK_NAVIGATE_HERE = "[map]:tooltip_carpark_navigate_here"
    const val EV_ADD_STOP = "[map]:tooltip_ev_add_stop"
    const val PETROL_ADD_STOP = "[map]:tooltip_petrol_add_stop"
    const val CAR_PARK_ALL_NEARBY = "carpark_all_nearby"
    const val CAR_PARK_AVAILABLE_ONLY = "carpark_available_only"
    const val CAR_PARK_HIDE = "carpark_hide"
    const val TAP_CUSTOM_MAP = "tap_custom_map"
    const val DELETE_SAVE_CONFIRM = "delete_save_confirm"
    const val DELETE_SAVE_CANCEL = "delete_save_cancel"
    const val CAR_PARK_TOOLTIP_SAVE_ON = "carpark_tooltip_save_on"
    const val CAR_PARK_TOOLTIP_SAVE_OFF = "carpark_tooktip_save_off"
    const val AMENITIES_TOOLTIP_SAVE_ON = "amenities_tooltip_save_on"
    const val AMENITIES_TOOLTIP_SAVE_OFF = "amenities_tooltip_save_off"
    const val POPUP_UNSAVE_COLLECTION = "unsave_collection"
    const val DISPLAY_CAR_PARK_LIST = "display_carpark_list"
    const val DISPLAY_CAR_PARK_MAP = "display_carpark_map"
    const val TAMPANIES_GREEN_PROGRESS_TRACKING_OPEN = "tampines_green_progress_tracking_open"

    const val HOME_CALENDAR_PERMISSION = "calendar_permission"

    //unused HOME
    const val MENU = "menu"
    const val FAVORITE = "favorite"
    const val SELECT_RECOMMENDED_DESTINATION = "select_recommended_destination"
    const val MAP_INTERACTION = "map_interaction"
    const val SWIPE = "swipe"
    const val SWIPE_CARD = "swipe_card"
    const val SEARCH_ADDRESS_INPUT = "search_address_input"
    const val SEARCH_ADDRESS_CLEAR = "search_address_clear"
    const val FAVORITE_MORE = "favourite_more"
    const val RECENT_SEARCH_SELECT = "recent_search_select"
    const val RECENT_SEARCH_CLEAR = "recent_search_clear"
    const val RECENT_SEARCH_SINGLE_CLEAR = "recent_search_single_clear"
    const val MAP_NOTIFICATION = "map_notification"
    const val MUTE = "mute"
    const val UNMUTE = "unmute"
    const val MAP_TRAFFIC_ICON_CLICK = "map_traffic_icon_click"

    //statistics
    const val STATISTIC = "statistics"
    const val STATISTIC_DATA = "statistics_data"
    const val START = "start"
    const val END = "end"


    // Favourites
    const val FAV_DELETE_CONFIRMATION_POPUP = "favourite_delete_confirmation"
    const val FAV_SELECT_FAVOURITE = "select_favourite"
    const val FAV_ADD_NEW = "add_new"
    const val FAV_3DOT_BUTTON = "android_3dot_button"
    const val FAV_EDIT = "android_edit"
    const val FAV_DELETE = "android_delete"
    const val FAV_CANCEL = "android_cancel"

    // Add New Favourites Search
    const val ADD_FAV_SELECT_ADDRESS = "select_address"

    // Favourites Edit / New Favourite
    const val FAVOURITE_NAME_INPUT = "favourite_name_input"
    const val FAVOURITE_NAME_CLEAR = "favourite_name_clear"
    const val FAVOURITE_NAME_SAVE = "save"

    // Settings
    const val SETTINGS_ACCOUNT_CLICK = "account"
    const val SETTINGS_MAP_DISPLAY_CLICK = "map_display"
    const val SETTINGS_PRIVACY_POLICY_CLICK = "privacy_policy"
    const val SETTINGS_TNC_CLICK = "terms_condition"
    const val SETTINGS_FAQ_CLICK = "faq"
    const val SETTINGS_FEEDBACK_CLICK = "feedback"
    const val SETTINGS_ABOUT_CLICK = "about"
    const val SETTINGS_PREFERENCES_CLICK = "preferences"

    // Settings - Account
    const val ACCOUNT_NAME_CLICK = "edit_name"
    const val ACCOUNT_EMAIL_CLICK = "edit_email"
    const val ACCOUNT_CREATE_ACCOUNT_CLICK = "create_acc"
    const val ACCOUNT_SIGNIN_CLICK = "signin"
    const val LOGOUT_CLICK = "logout"
    const val ACCOUNT_CAR_DETAILS_CLICK = "car_details_edit"

    // Settings - Account Edit Username
    const val ACCOUNT_USERNAME_INPUT = "username_input"

    // Settings - Account Edit Email
    const val ACCOUNT_EMAIL_INPUT = "email_input"
    const val ACCOUNT_RESEND_LINK = "resend_link"

    //Route PLanning
    const val CARPARK_NEARBY = "carpark_nearby"
    const val RESET_CARPARK = "reset_carpark"
    const val LETS_GO = "lets_go"
    const val EDIT_FAVOURITE = "edit_favourite"
    const val REMOVE_FAVOURITE = "add_remove_favourite"
    const val TAP_ROUTE = "tap_route"
    const val SHARE_MY_ETA = "share_my_eta"
    const val RESET_MY_ETA = "reset_my_eta"
    const val FASTEST_ROUTE = "fastest_route"
    const val SHORTEST_ROUTE = "shortest_route"
    const val CHEAPEST_ROUE = "cheapest_route"

    //Route PLanning - Carpark
    const val TAP_CARPARK_CLICK = "[map]:tap_carpark"
    const val TAP_PETROL_CLICK = "[map]:tap_petrol"
    const val TAP_EV_CLICK = "[map]:tap_ev"
    const val TAP_VOUCHER_CARPARK_CLICK = "tap_voucher_carpark"
    const val TAP_ERP_CLICK = "tap_erp"
    const val CLOSE_BUBBLE_CLICK = "close_bubble" //CLOSE BUBBLE BUTTON NOT FOUND
    const val MORE_INFO_CLICK = "more_info"
    const val NAVIGATE_HERE = "navigate_here"

    //Route PLanning - Carpark Information
    const val CARPARK_INFO_CLOSE = "close" //CLOSE BUTTON NOT FOUND

    //Search Contact
    const val SEARCH_CONTACT = "search_contact"
    const val RECENT_CONTACT_SEARCH = "recent_contact_select"
    const val CONTACT_SELECT = "contact_select"
    const val SEARCH_CANCEL = "cancel"
    const val CONTACT_PERMISSION = "contact_permission"

    // Change User Name
    const val UPDATE_NAME = "update_name"
    const val USER_NAME_SAVE_CLICK = "save"
    const val USER_NAME_EDIT = "name_input"

    // Change Password
    const val UPDATE_PASSWORD = "update_password"
    const val UPDATE_PASSWORD_DONE = "done"
    const val OLD_PASSWORD_EDIT = "old_password_input"
    const val NEW_PASSWORD_EDIT = "new_password_input"
    const val FORGET_PASSWORD_CLICK = "forget_password"

    // Change Email
    const val UPDATE_EMAIL = "update_email"
    const val EMAIL_SAVE_CLICK = "save"
    const val EMAIL_EDIT = "email_input"

    // Map Display
    const val MAP_AUTO_CLICK = "auto"
    const val MAP_LIGHT_CLICK = "light"
    const val MAP_DARK_CLICK = "dark"

    // Route Preference
    const val ROUTE_PREF_FASTEST_CLICK = "fastest"
    const val ROUTE_PREF_CHEAPEST_CLICK = "cheapest"
    const val ROUTE_PREF_SHORTEST_CLICK = "shortest"

    // Route Planning
    const val CARPARK_NEARBY_CLICK = "carpark_nearby"
    const val RESET_CARPARK_CLICK = "reset_carpark"
    const val LETS_GO_CLICK = "lets_go"
    const val TRIP_BACK_TO_HOME = "back_to_home"
    const val TRIP_LOG_CLICK = "trip_log"
    const val EDIT_FAVOURITE_CLICK = "edit_favourite"
    const val ADD_REMOVE_FAVOURITE_CLICK = "add_remove_favourite"
    const val TAP_ROUTE_CLICK = "tap_route"
    const val SEARCH_ORIGIN_CLICK = "search_origin"
    const val SEARCH_DESTINATION_CLICK = "search_destination"
    const val SWAP_ROUTE_CLICK = "origin_swap"
    const val DIRECTION_CLICK = "direction"
    const val NOTIFY_ARRIVAL_CLICK = "notify_arrival"
    const val LATER = "later"
    const val SAVE_LATER = "savelater"
    const val DATETIME_CANCEL = "datetime_cancel"
    const val DATETIME_NEXT = "datetime_next"
    const val PARKING_MORE_INFO = "parking_moreinfo"
    const val PARKING_SEE_MORE = "parking_tooltip_see_more"
    const val PARKING_CALCULATE_FEE = "parking_calculatefee"
    const val PARKING_NAVIGATE_HERE = "parking_navigatehere"
    const val ADD_STOP_PETROL = "add_stop_petrol"
    const val REMOVE_STOP_PETROL = "remove_stop_petrol"
    const val ADD_STOP_EV = "add_stop_ev"
    const val REMOVE_STOP_EV = "remove_stop_ev"
    const val PETROL_MAP_ICON = "petrol_map_icon"
    const val POPUP_ORIGIN_ALERT = "origin_alert"
    const val PARKING_ON = "parking_on"
    const val PARKING_OFF = "parking_off"
    const val PETROL_ON = "petrol_on"
    const val PETROL_OFF = "petrol_off"
    const val EV_ON = "ev_on"
    const val EV_OFF = "ev_off"
    const val LETS_GO_ORIGIN = "let's_go_origin"
    const val LETS_GO_DESTINATION = "let's_go_destination"
    const val LETS_GO_ADDED_STOP = "let's_go_added_stop"
    const val LATER_ORIGIN = "later_route_origin"
    const val LATER_DESTINATION = "later_destination"
    const val LATER_ADDED_STOP = "later_added_stop"
    const val SAVE_LATER_ORIGIN = "savelater_origin"
    const val SAVE_LATER_DESTINATION = "savelater_destination"
    const val SAVE_LATER_ADDED_STOP = "savelater_added_stop"
    const val GOT_IT_UNABLE_TO_SAVE = "got_it_unable_to_save"
    const val POPUP_UNABLE_TO_SAVE = "unable_to_save"
    const val POPUP_TRIP_UPDATED = "trip_been_updated"
    const val CLOSE_POPUP_TRIP_UPDATED = "close_trip_updated_popup"
    const val TRIP_BEEN_SAVED = "trip_been_saved"
    const val LETS_GO_TRIP_SAVED = "lets_go_trip_saved_popup"
    const val CHECK_IN_TRIP_LOG_TRIP_SAVED = "check_in_triplog_trip_saved_popup"
    const val VIEW_NEARBY_VOUCHER_CAR_PARKS = "view_nearby_voucher_carparks"
    const val BACK_TO_ROUTE_OPTIONS = "back_route_options"

    // Navigation
    const val EXPAND_INSTRUCTIONS_CLICK = "expand_instructions"
    const val COLLAPSE_INSTRUCTIONS_CLICK = "collapse_instructions"
    const val ETA_RESUME = "eta_resume"
    const val ETA_PAUSE = "eta_pause"
    const val NAVIGATION_NATURAL_END = "natural_end"
    const val USER_NAVIGATION_END = "user_end"
    const val END_NAVIGATION = "end_navigation"
    const val END_WALKING = "end_walking"
    const val END_NAVIGATION_ARRIVAL = "arrival_end_navigation"
    const val POP_USER_END_NO = "pop_user_end_no"
    const val POP_USER_END_YES = "pop_user_end_yes"
    const val NATURAL_END_DONE = "natural_end_done"
    const val POP_NATURAL_END_DONE = "pop_natural_end"
    const val POP_RATING_CLOSE = "pop_rating_close"
    const val POP_RATING_SUBMIT = "pop_rating_submit"
    const val POP_INSTA_BUG = "pop_instabug"
    const val POP_STAR_1 = "pop_star_1"
    const val POP_STAR_2 = "pop_star_2"
    const val POP_STAR_3 = "pop_star_3"
    const val POP_STAR_4 = "pop_star_4"
    const val POP_STAR_5 = "pop_star_5"
    const val USE_WALKING_GUIDE = "use_walking_guide"
    const val END_NO_WALKING_GUIDE = "end_no_walking_guide"

    // car park availability verification dialog
    const val VERIFY_AVAIL_YES = "verify_avail_yes"
    const val VERIFY_AVAIL_NO = "verify_avail_no"


    // Travel Log Screen
    const val TRAVEL_LOG_ENABLE_SELECT = "enable_select"
    const val TRAVEL_LOG_CANCEL_SELECT = "cancel_select"
    const val TRAVEL_LOG_SEND_EMAIL = "send_email"
    const val TRAVEL_LOG_BACK_TO_TOP = "back_to_top"
    const val TRAVEL_LOG_MONTH_YEAR_SELECT = "month_select_year_select"
    const val TRAVEL_LOG_SELECT_ALL = "select_all"
    const val EMAIL_SUMMARY_POPUP = "email_travel_summary_success_ok"
    const val TRAVEL_LOG_INDIVIDUAL_TRIP_SELECT = "individual_trip_select"
    const val TRAVEL_LOG_MONTH_SELECT = "month_select_travel_log"
    const val TRAVEL_LOG_VIEW_SINGLE_TRIP = "view_single_trip"
    const val TRIP_LOG_UPCOMING = "upcoming"
    const val TRIP_LOG_HISTORY = "history"
    const val TRIP_LOG_TRANSACTION = ":transactions_tab"


    // Trip Summary Screen
    const val EDIT_ERP_PRICE = "edit_erp_price"
    const val EDIT_PARKING_PRICE = "edit_parking_price"
    const val ADD_PARKING_IMG_CLICK = "add_parking_img"
    const val EDIT_PARKING_IMG = "edit_image"
    const val CANCEL = "cancel"
    const val SAVE_BUTTON_CLICK = "save"
    const val POP_ADD_GALLERY = "pop_add_gallery"
    const val POP_ADD_CAMERA = "pop_add_camera"
    const val POP_ADD_CANCEL = "pop_add_cancel"
    const val RETAKE_PHOTO = "retake_photo"
    const val RESELECT_FROM_GALLERY = "reselect_from_gallery"
    const val DELETE_RECEIPT = "delete_receipt"
    const val PREVIEW_PARKING_RECEIPT_POPUP = "preview_parking_receipt" //NOT in the list

    // Trip Summary Screen - POPUP NAMES
    const val ADD_PARKING_RECEIPT = "add_parking_receipt"
    const val EDIT_PARKING_RECEIPT = "edit_parking_receipt"
    const val DELETE_PARKING_RECEIPT_POPUP = "delete_parking_receipt_confirmation"

    //Cruise Mode
    const val RATING = "rating" //POPUPNAME
    const val START_CRUISE = "start_cruise"
    const val END_CRUISE = "end_cruise"

    //Feedback Screenroute
    const val ROUTE_CLICK = "route"
    const val VOICE_CLICK = "voice"
    const val PARKING_CLICK = "parking"
    const val ERP_CLICK = "erp"
    const val INTERFACE_CLICK = "interface"
    const val TRAFFIC_CLICK = "traffic"
    const val SUBMIT_CLICK = "submit"
    const val INSTABUG_CLICK = "instabug"
    const val DESCRIPTION_CLICK = "description"

    //ETA - Arrival Fav - RN Screen
    const val EDIT = "edit"
    const val DELETE = "delete"
    const val POP_CONFIRMATION_REMOVE = "popup_confirmation_remove"
    const val POP_CONFIRMATION_CANCEL = "popup_confirmation_cancel"
    const val ENTRY_SWIPE_RIGHT = "entry_swipe_right"
    const val ENTRY_SWIPE_LEFT = "entry_swipe_left"
    const val DELETE_CONFIRMATION = "popup_contacts_open"
    const val POPUP_CONTACTS_OPEN = "popup_contacts_open"
    const val POPUP_CONTACTS_CLOSE = "popup_contacts_close"
    const val POPUP_DELETE_CONFIRM_OPEN = "popup_delete_confirm_open"
    const val POPUP_DELETE_CONFIRM_CLOSE = "popup_delete_confirm_open"

    //ETA - Arrival Message - RN Screen
    const val ETA_CONTACT_SELECT = "popup_confirmation_cancel"
    const val ETA_DESTINATON_SELECT = "popup_confirmation_cancel"
    const val ETA_ARRIVAL_MESSAGE_INPUT = "popup_confirmation_cancel"
    const val ETA_SHARE_LOCATION = "popup_confirmation_cancel"
    const val ETA_AUTO_CALL = "popup_confirmation_cancel"
    const val CANCEL_ADD_CONTACT = "cancel_add_contact"

    const val TRIP_SAVE_SCUCESS = "trip_save_scucess" //VerifyOTP (UNUSED)

    //ETA - Contacts Search - RN Screen

    //ETA - Destination Search - RN Screen

    //software update
    const val MANDATORY_UPDATE = "mandotory_update"
    const val NORMAL_UPDATE = "normal_update"
    const val LATEST_VERSION_UPDATED = "lastest_version_updated"
    const val UPDATE_NOW = "update_now"
    const val INSTALL_NOW = "install_now"
    const val DISMISS = "dismiss"

    //Global Notification
    const val POPUP_GLOBAL_NOTIFICATION = "global_notification"
    const val GLOBAL_NOTIFICATION_CLOSE = "close_global_noti"
    const val GLOBAL_NOTIFICATION_START_TRIP = "start_trip_global_noti"
    const val GLOBAL_NOTIFICATION_MY_VOUCHERS = "my_vouchers_noti"
    const val GLOBAL_NOTIFICATION_READ_MORE = "read_more_global_noti"
    const val GLOBAL_NOTIFICATION_SEE_ON_MAP = "see_on_map_global_noti"
    const val GLOBAL_NOTIFICATION_WATCH_QUICK_START_VIDEO = "watch_quick_start_video"

    const val NUDGE_NEXT = "nudge_next"
    const val NUDGE_CLOSE = "nudge_close"
    const val NUDGE_DONE = "nudge_done"

    //Saved Collections
    const val MAP_SAVED_ICON_CLICK = "map_saved_icon_click"
    const val NAVIGATE_HERE_SAVED = "navigate_here_saved"
    const val EDIT_SHORTCUT_BACK = "edit_shortcut_back"
    const val EDIT_SHORTCUT_NAME_CLEAR = "edit_shortcut_name_clear"
    const val ADD_HOME_BACK = "add_home_back"
    const val ADD_WORK_BACK = "add_work_back"
    const val ADD_HOME_SAVE = "add_home_save"
    const val ADD_WORK_SAVE = "add_work_save"

    //Android Auto
    const val AA_CARCONNECT = "carconnect"
    const val AA_SEARCH = "search"
    const val AA_OBU = "obu"
    const val AA_CRUISE = "cruise"
    const val AA_NOTIFY = "notify"
    const val AA_MAPBOX_ZOOM_IN = "mapbox_zoomin"
    const val AA_MAPBOX_ZOOM_OUT = "mapbox_zoomout"
    const val AA_MAPBOX_PAN_UP = "mapbox_pan_up"
    const val AA_MAPBOX_PAN_DOWN = "mapbox_pan_down"
    const val AA_MAPBOX_PAN_LEFT = "mapbox_pan_left"
    const val AA_MAPBOX_PAN_RIGHT = "mapbox_pan_right"
    const val AA_MAPBOX_PAN_CLOSE = "mapbox_pan_close"
    const val AA_MAPBOX_RECENTER = "mapbox_recener"
    const val AA_DISPLAY_CARPARK = "display_carpark"
    const val AA_HIDE_CARPARK = "hide_carpark"
    const val AA_BACK = "back"
    const val AA_NEW_SEARCH = "new_search"
    const val AA_FAVOURITE = "favourite"
    const val AA_RECENT_SEARCH = "recent_search"
    const val AA_SCROLL_UP = "scroll_up"
    const val AA_SCROLL_DOWN = "scroll_down"
    const val AA_SELECT_DESTINATION = "select_destination"
    const val AA_CANCEL = "cancel"
    const val AA_VOICE_INPUT = "voice_input"
    const val AA_SEARCH_INPUT = "search_input"
    const val AA_PARKING = "parking"
    const val AA_OTHER_ROUTE = "other_route"
    const val AA_LETS_GO = "lets_go"
    const val AA_CONFIRM = "confirm"
    const val AA_ROUTE_SELECTION_RADIO_BUTTON = "route_selection_radio_button"
    const val AA_MUTE = "mute"
    const val AA_UNMUTE = "unmute"
    const val AA_END = ":end"
    const val AA_NOTIFICATION_CLOSE = "notification_close"
    const val AA_PAUSE_SHARING = "pause_sharing"
    const val AA_RESUME_SHARING = "resume_sharing"
    const val AA_END_CRUISE = "end_cruise"
    const val AA_SELECT_NOTIFY_LIST = "select_notify_list"
    const val AA_CHOOSE_FROM_NOTIFY_LIST = "choose_from_notify_list"
    const val AA_SETUP_NEW_ARRIVAL_MESSAGE = "setup_new_arrival_message"
    const val AA_DONE = "done"


    //Guest Mode onboarding_create_acc_signup
    const val TNC_BACK = "tnc_back"
    const val TNC_DECLINE_GUEST = "tnc_decline"
    const val TNC_AGREE = "tnc_agree"
    const val CONTINUE_WO_ACC = "continue_wo_acc"
    const val CREATE_ACC_BACK = "create_acc_back"
    const val CREATE_ACC_FACEBOOK = "create_acc_facebook"
    const val CREATE_ACCOUNT_GOOGLE = "create_account_google"
    const val CREATE_ACCOUNT_APPLE = "create_account_apple"
    const val USER_NAME_BACK = "user_name_back"
    const val SUCCESS_NEXT = "success_next"


    //screen_name: onboarding_create_acc_signup
    const val GUEST_CONTINUE_CLOSE = ":guest_mode_warning_close"
    const val GUEST_CONTINUE_CANCEL = ":guest_mode_warning_cancel"
    const val GUEST_CONTINUE_PROCEED = ":guest_mode_warning_proceed"


    //onboarding_signin
    const val SIGNIN_BACK = "signin_back"
    const val SIGNIN_MOBILE = "signin_mobile"
    const val SIGNIN_MOBILE_BACK = "signin_mobile_back"
    const val SIGNIN_SUCCESS_NEXT = "signin_success_next"


    //obu
    const val OBU_SYSTEM_EVENT = "system_event"

    const val OBU_CRUISE_START = ":cruise_start"
    const val OBU_CLOSE = ":close"
    const val OBU_MUTE_ON = ":mute_on"
    const val OBU_MUTE_OFF = ":mute_off"

    const val OBU_INFO_VIEW_P_BUTTON_STATE_OFF = ":P_button_off"
    const val OBU_INFO_VIEW_P_BUTTON_STATE_ON = ":P_button_on"
    const val OBU_DISPLAY_CAR_PARK_NAVIGATE_HERE = "[map]:tooltip_carpark_navigate_here"

    const val OBU_CAR_PARK_SELECT = "[parking_guidance_system]:carpark_select"
    const val OBU_CAR_PARK_CLOSE = "[parking_guidance_system]:close"

    const val VALUE_POPUP_CLOSE = ":popup_close"
    const val OBU_MANUAL_POP_UP_CLICK = "[pop_up_navigation_notification]:click_notification"
    const val OBU_MANUAL_POP_UP_CLOSE = "[pop_up_navigation_notification]:close"

    const val OBU_READ_MORE = "[popup_travel_time]:read_more"
    const val OBU_DHU_PROJECTED_OK = "[popup_projected_to_dhu]:ok"

    const val OBU_NAVIGATION_NOTIFICATION_CLOSE = "[pop_up_navigation_notification]:close"
    const val OBU_NAVIGATION_NOTIFICATION_CLICK =
        "[pop_up_navigation_notification]:click_notification"

    const val OBU_TRANSACTIONS_TAB = ":transactions_tab"
    const val OBU_TRANSACTIONS_TAB_PAIR = "[transactions]:start_to_pair"

    const val OBU_SYSTEM_CONNECTING = "obu_connecting"
    const val OBU_SYSTEM_CONNECTION_FAILED = "obu_connect_failed"
    const val OBU_SYSTEM_DISCONNECTED = "obu_disconnect"
    const val OBU_SYSTEM_CONNECTED = "obu_connected"
    const val OBU_SYSTEM_CAR_DISCONNECTED = "cardisconnect"
    const val OBU_SYSTEM_CAR_CONNECT = "carconnect"
    const val OBU_SYSTEM_APP_START = "app_start"
    const val OBU_SYSTEM_APP_TERMINATED = "app_terminated"
    const val OBU_SYSTEM_APP_FOREGROUND = "app_foreground"
    const val OBU_SYSTEM_APP_BACKGROUND = "app_background"

    // OBU connection status Screen AA
    const val AA_POPUP_OBU_CONNECTED = "obu_connected"
    const val AA_POPUP_OBU_NOT_CONNECTED = "obu_not_connected"
    const val AA_POPUP_OBU_CONNECTION_ERROR = "obu_connection_error"
    const val AA_POPUP_OBU_NOT_PAIRED = "obu_not_paired"
    const val AA_POPUP_NAVIGATION = "navigation_notification"
    const val AA_POPUP_CRUISE = "cruise_notification"
    const val AA_POPUP_OPEN = "[dhu]:popup_open"
    const val AA_POPUP_CLOSE = "[dhu]:popup_close"

    //screen_name: [obu_pairing]
    const val OBU_PAIRING_BACK = ":back"
    const val OBU_PAIRING_ENGINE_BACK = "[start_engine]:back"
    const val OBU_PAIRING_OBU_ALREADY_PAIRED_OK = "[start_engine]:obu_already_paired_ok"
    const val VALUE_POPUP_OPEN = ":popup_open"
    const val OBU_PAIRING_POPUP_BLUETOOTH_SETTINGS =
        "[popup_obu_permission_turn_on_bluetooth]:settings"
    const val OBU_PAIRING_POPUP_BLUETOOTH_OK = "[popup_obu_permission_turn_on_bluetooth]:ok"
    const val OBU_PAIRING_POPUP_OBU_INTERNET_SETTINGS =
        "[popup_obu_permission_turn_on_internet]:settings"
    const val OBU_PAIRING_POPUP_OBU_INTERNET_OK = "[popup_obu_permission_turn_on_internet]:ok"
    const val OBU_PAIRING_POPUP_OBU_PERMISSION_SETTINGS =
        "[popup_allow_obu_access]:settings"
    const val OBU_PAIRING_POPUP_OBU_PERMISSION_OK = "[popup_allow_obu_access]:ok"
    const val OBU_PAIRING_POPUP_OBU_NOT_FOUND_OK = "[popup_obu_not_found]:ok"
    const val OBU_PAIRING_POPUP_OBU_PAIRING_FAILED_OK = "[popup_obu_pairing_failed]:ok"
    const val OBU_PAIRING_START_ENGINE_CONFIRM_OBU_CLOSE = "[start_engine],[confirm_obu]:close"
    const val OBU_PAIRING_START_ENGINE_CONFIRM_OBU_SELECT_OBU =
        "[start_engine],[confirm_obu]:select_obu"
    const val OBU_PAIRING_START_ENGINE_CONFIRM_OBU_SELECT_CONFIRM =
        "[start_engine],[confirm_obu]:select_confirm"
    const val OBU_PAIRING_NUMBER_PLATE_BACK = "[number_plate]:back"
    const val OBU_PAIRING_NUMBER_PLATE_INPUT = "[number_plate]:name_number_input"
    const val OBU_PAIRING_NUMBER_PLATE_NEXT = "[number_plate]:next"
    const val OBU_PAIRING_PAIRING_BACK = "[pairing]:back"
    const val OBU_PAIRING_PAIRING_CANCEL = "[pairing]:cancel_pairing"

    const val POPUP_OBU_PERMISSION_TURN_ON_BLUETOOTH = "popup_obu_permission_turn_on_bluetooth"
    const val POPUP_OBU_PERMISSION_TURN_ON_INTERNET = "popup_obu_permission_turn_on_internet"
    const val POPUP_ALLOW_OBU_PERMISSION_ACCESS = "popup_allow_obu_access"
    const val POPUP_OBU_NOT_FOUND = "popup_obu_not_found"
    const val POPUP_OBU_PAIRING_FAILED = "popup_obu_pairing_failed"
    const val POPUP_OBU_MAC_NOT_MATCH = "popup_obu_mac_not_match"

    //screen_name:  dialog
    const val POPUP_OBU_SUCCESSFUL_PAIRED = "popup_obu_successful_paired"
    const val POPUP_OBU_CONNECTION_ERROR_BLUETOOTH = "popup_obu_connection_error_bluetooth"
    const val POPUP_OBU_CONNECTION_ERROR_INTERNET = "popup_obu_connection_error_internet"
    const val POPUP_OBU_CONNECTION_ERROR_OBU_PERMISSION =
        "popup_obu_connection_error_obu_permission"
    const val POPUP_OBU_CONNECTION_ERROR_OBU = "popup_obu_connection_error_obu"

    const val POPUP_POPUP_OBU_CONNECTION_ERROR_BLUETOOTH_CLOSE =
        "[popup_obu_connection_error_bluetooth]:close"
    const val POPUP_POPUP_OBU_CONNECTION_ERROR_INTERNET_CLOSE =
        "[popup_obu_connection_error_internet]:close"
    const val POPUP_PAIRING_ERROR_CLOSE = "[pairing_error]:close"
    const val POPUP_PAIRING_ERROR_RETRY = "[pairing_error]:retry"
    const val POPUP_PAIRING_ERROR_SKIP = "[pairing_error]:skip"
    const val POPUP_PAIRING_ERROR_REPORT_ISSUE = "[pairing_error]:report_issue"
    const val POPUP_SYSTEM_ERROR_CLOSE = "[system_error]:close"
    const val POPUP_SYSTEM_ERROR_RETRY = "[system_error]:retry"
    const val POPUP_SYSTEM_ERROR_SKIP = "[system_error]:skip"
    const val POPUP_SYSTEM_ERROR_REPORT_ISSUE = "[system_error]:report_issue"
    const val POPUP_OBU_NOT_DETECTED_CLOSE = "[obu_not_detected]:close"
    const val POPUP_OBU_NOT_DETECTED_RETRY = "[obu_not_detected]:retry"
    const val POPUP_OBU_NOT_DETECTED_SKIP = "[obu_not_detected]:skip"
    const val POPUP_OBU_NOT_DETECTED_REPORT_ISSUE = "[obu_not_detected]:report_issue"
    const val POPUP_OBU_CONNECTION_LOST_CLOSE = "[obu_connection_lost]:close"
    const val POPUP_OBU_CONNECTION_LOST_RETRY = "[obu_connection_lost]:retry"
    const val POPUP_OBU_CONNECTION_LOST_SKIP = "[obu_connection_lost]:skip"
    const val POPUP_OBU_CONNECTION_LOST_REPORT_ISSUE = "[obu_connection_lost]:report_issue"


    const val OBU_PROJECTED_TO_DHU = "projected_to_dhu"
    const val OBU_DISPLAY_NOTIFICATION = "obu_display_notification"
    const val POPUP_NAVIGATION_NOTIFICATION_CLICK_NOTIFICATION =
        "[popup_navigation_notification]:click_notification"
    const val OBU_VERBOSE_ERROR = "obu_verbose_error"
    const val OBU_SDK_INIT_SUCCESS = "sdk_init_success"
    const val OBU_SDK_INIT_FAILURE = "sdk_init_failure"
    const val OBU_CARD_STATUS = "obu_card_status"
    const val OBU_CARD_ERROR = "obu_card_error"
    const val POPUP_OBU_CARD_ERROR_GOT_IT = "[popup_obu_card_error]:got_it"
    const val SUMMARY_TAB_TOTAL_PARKING_FEE_READ_MORE_BACK =
        "[summary_tab],[total_parking_fee_read_more]:back"
    const val SUMMARY_TAB_TOTAL_ERP_READ_MORE_BACK = "[summary_tab],[total_erp_read_more]:back"
    const val SUMMARY_TAB_DRIVING_TIME_READ_MORE_BACK =
        "[summary_tab],[driving_time_read_more]:back"

    const val SEARCH_COLON = ":search"

    // Trip log transaction
    const val TRANSACTION_FILTER_DATE = "[transactions]:filter_date"
    const val TRANSACTION_FILTER_TRANSACTION_TYPE = "[transactions]:filter_transaction_type"
    const val TRANSACTION_FILTER_TRANSACTION_TYPE_CLOSE =
        "[transactions],[filter_transaction_type]:close"
    const val TRANSACTION_FILTER_TRANSACTION_TYPE_ALL =
        "[transactions],[filter_transaction_type]:all"
    const val TRANSACTION_FILTER_TRANSACTION_TYPE_ERP_ONLY =
        "[transactions],[filter_transaction_type]:erp_only"
    const val TRANSACTION_FILTER_TRANSACTION_TYPE_PARKING_ONLY =
        "[transactions],[filter_transaction_type]:parking_only"

    const val OBU_DISPLAY_TOGGLE_EV_OFF = ":ev_button_off"
    const val OBU_DISPLAY_TOGGLE_EV_ON = ":ev_button_on"
    const val OBU_DISPLAY_TOGGLE_PETROL_OFF = ":petrol_button_off"
    const val OBU_DISPLAY_TOGGLE_PETROL_ON = ":petrol_button_on"
    const val OBU_DISPLAY_CLICK_MAP_PETROL = "[map]:tap_petrol"
    const val OBU_DISPLAY_CLICK_MAP_PETROL_NAVIGATE = "[map]:tooltip_petrol_navigate_here"
    const val OBU_DISPLAY_CLICK_MAP_EV = "[map]:tap_ev"
    const val OBU_DISPLAY_CLICK_MAP_EV_NAVIGATE = "[map]:tooltip_ev_navigate_here"
    const val OBU_DISPLAY_CLICK_MAP_CARPARK_BOOKMARK = "[map]:tooltip_carpark_bookmark"
    const val OBU_DISPLAY_CLICK_MAP_EV_BOOKMARK = "[map]:tooltip_ev_bookmark"
    const val OBU_DISPLAY_CLICK_MAP_PETROL_BOOKMARK = "[map]:tooltip_petrol_bookmark"
    const val OBU_DISPLAY_CLICK_RECENTER_MAP = "[map]:recentre_map"
    const val OBU_DISPLAY_CLICK_SHARE_DRIVE = ":share_drive"
    const val ROUTE_PLANNING_CLICK_CANCEL_SHARE_DRIVE = ":cancel_share_drive"
    const val ROUTE_PLANNING_CLICK_CANCEL_SHARE_DRIVE_CANCEL = ":cancel_share_drive_cancel"
    const val ROUTE_PLANNING_CLICK_CANCEL_SHARE_DRIVE_CONFIRM = ":cancel_share_drive_confirm"

    //screen_name: [obu_display_mode]
    const val POPUP_OPEN2 = ":popup_open"
    const val NO_PARKING_VICINITY = "no_parking_vicinity"
    const val CLOSE_POPUP_PARKING_VICINITY = "[popup_no_parking_vicinity]:close"
    const val NO_PETROL_VICINITY = "no_petrol_vicinity"
    const val CLOSE_POPUP_NO_PETROL_VICINITY = "[popup_no_petrol_vicinity]:close"
    const val NO_EV_VICINITY = "no_ev_vicinity"
    const val CLOSE_POPUP_NO_EV_VICINITY = "[popup_no_ev_vicinity]:close"


    const val BOTTOM_OBU_CARD_SHOWN = ":bottom_obu_card_shown"
    const val START_ENGINE_NEXT = "[start_engine]:next"
    const val SHOW_CARPARKS = ":show_carparks"
    const val AA_CARPARKS_BACK = "[carparks]:back"
    const val AA_CARPARKS_UP = "[carparks]:up"
    const val AA_CARPARKS_DOWN = "[carparks]:down"
    const val AA_CARPARKS_NAVIGATE_HERE = "[carparks]:select_carpark_navigate_here"
    const val AA_SHOW_BALANCE = ":obu_show_balance"
    const val AA_SHOW_BALANCE_BACK = "[obu_show_balance]:back"
    const val AA_OBU_X = ":obu_x"
    const val AA_OBU_NOT_PAIRED_OK = "[obu_not_paired]:ok"
    const val AA_OBU_NOT_PAIRED_BACK = "[obu_not_paired]:back"
    const val AA_CONNECTION_ERROR = "connection_error"
    const val AA_POPUP_REPORT_ISSUE_REPORT_ISSUE = "[popup_report_issue]:report_issue"
    const val AA_POPUP_REPORT_ISSUE_BACK = "[popup_report_issue]:back"
    const val AA_OBU_NOT_CONNECTED_RETRY = "[obu_not_connected]:retry"
    const val AA_OBU_NOT_CONNECTED_BACK = "[obu_not_connected]:back"

    const val LIMITED_PARKING_SPACE = "limited_parking_space"
    const val POPUP_LIMITED_PARKING_SPACE_REROUTE_WALK = "[popup_limited_parking_space]:reroute_walk"
    const val CARPARK_FULL = "carpark_full"
    const val POPUP_CARPARK_FULL_REROUTE_WALK = "[popup_carpark_full]:reroute_walk"
    const val PARKING_AVAILABLE = "parking_available"

    const val MAP_TOOLTIP_SAVED_NAVIGATE_HERE = "[map]:tooltip_saved_navigate_here"
    const val MAP_TOOLTIP_SAVED_BOOKMARK = "[map]:tooltip_saved_bookmark"
    const val MAP_TOOLTIP_SAVED_UNBOOKMARK = "[map]:tooltip_saved_unbookmark"
    const val MAP_TOOLTIP_SAVED_SHARE = "[map]:tooltip_saved_share"
    const val NAVIGATION_CARPARKS = ":carparks"
    const val NEARBY_CARPARKS_LOCATION_SELECT = "[nearby_carparks]:location_select"
    const val NEARBY_CARPARKS_BACK = "[nearby_carparks]:back"
    const val SHARE_DESTINATION = ":share_destination"

    const val POPUP_CARPARK_FULL_CLOSE = "[popup_carpark_full]:close"
    const val POPUP_LIMITED_PARKING_SPACE_CLOSE = "[popup_limited_parking_space]:close"
    const val POPUP_PARKING_AVAILABLE_CLOSE = "[popup_parking_available]:close"

}
package com.ncs.breeze.common.remote;

import com.breeze.model.TripSummary
import com.breeze.model.TripWalkingSummary
import com.breeze.model.api.request.*
import com.breeze.model.api.response.*
import com.breeze.model.api.response.amenities.CarParkDetailResponse
import com.breeze.model.api.response.amenities.CarparkAvailabilityResponse
import com.breeze.model.api.response.amenities.ResponseAmenities
import com.breeze.model.api.response.eta.RecentETAContact
import com.breeze.model.api.response.guest.GuestGenResponse
import com.breeze.model.api.response.notification.NotificationResponse
import com.breeze.model.api.response.tbr.ResponseCongestionDetail
import com.breeze.model.api.response.tbr.ResponseZoneAlertDetail
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import io.reactivex.Observable
import retrofit2.http.*

//FIXME: currently all apis are being observed on ui thread, which is not required
interface ApiHelper {

    @POST("user/appversion")
    fun logAppVersion(
        @Body request: LogAppVersionRequest?
    ): Observable<String>

    @GET("address/addressrecommendations")
    fun retrieveAddressRecommendation(
        @Query("lat") latitude: String?,
        @Query("long") longitude: String?,
        @Query("resultcount") resultCount: Int
    )
            : Observable<AddressRecommendationResponse>

    @GET("address/searchaddresses")
    fun retrieveSearchAddresses(
        @Query("appversion") appVersion: String,
        @Query("devicemodel") devicemodel: String,
        @Query("deviceos") deviceOS: String,
        @Query("deviceosversion") deviceOSVersion: String,
        @Query("lat") latitude: String?,
        @Query("long") longitude: String?,
        @Query("searchstring") searchString: String
    )
            : Observable<SearchQueryResponse>

    @GET("address/details")
    fun getDetails(
        @Query("placeId") placeId: String,
        @Query("token") token: String
    ): Observable<PlaceDetailsResponse>


    @GET("address")
    fun getAddress(
        @Query("searchstring") searchstring: String,
        @Query("lat") latitude: String,
        @Query("long") longitude: String,
        @Query("token") token: String
    ): Observable<SearchQueryResponse>

    /*@GET("/api/savedaddresses")
    fun retrieveSavedAddresses(
        @Query("appversion") appVersion: String, @Query("devicemodel") devicemodel: String,
        @Query("deviceos") deviceOS: String, @Query("deviceosversion") deviceOSVersion: String
    )
            : Observable<SavedAddressResponse>


    @GET("/api/userdata")
    fun getUserData(): Observable<JsonElement>*/

    /*retrieve saved easybreeze*/
    @GET("address/easybreezies")
    fun retrieveEasyBreezies(
        @Query("appversion") appVersion: String, @Query("devicemodel") devicemodel: String,
        @Query("deviceos") deviceOS: String, @Query("deviceosversion") deviceOSVersion: String
    ): Observable<EasyBreeziesResponse>

    @POST("address/selectaddress")
    fun selectAddress(@Body request: SelectAddressRequest): Observable<JsonElement>

    @POST("user/v3/bookmarks")
    fun createShortcut(@Body request: ShortcutDetails): Observable<JsonElement>

    @PUT("user/v3/bookmarks/{id}")
    fun updateShortcut(
        @Path("id") id: Int,
        @Body request: ShortcutDetails
    ): Observable<JsonElement>


    @GET("maintenance/erprates")
    fun getErpRates(
        /* @Query("vehicletype") vehicleType: String*/
    ): Observable<ERPResponseData.ERPResponse>

    @GET("maintenance/masterlist?module=USER_PREFERENCE")
    fun getAllSettingSystem(
        /* @Query("vehicletype") vehicleType: String*/
    ): Observable<AllSettingResponse>

    @GET("dataset/trafficincidentv2.json")
    fun getTrafficIncidents(): Observable<JsonElement>

    @GET("carpark/getcarparksv2")
    fun getNearbyCarparks(
        @Query("latitude") latitude: String,
        @Query("longitude") longitude: String,
        @Query("radius") radius: Double,
        @Query("arrivalTime") arrivalTime: Long,
        @Query("destName") destName: String,
    ): Observable<JsonElement>

    @GET("dataset/schoolzone.json")
    fun getSchoolZone(): Observable<JsonElement>

    @GET("dataset/speedlightcamera.json")
    fun getSpeedLightCameras(): Observable<JsonElement>

    @GET("dataset/silverzone.json")
    fun getSilverZone(): Observable<JsonElement>

    @GET("user/usersetting")
    fun getUserSettings(): Observable<SettingsResponse>

    @GET("maintenance/contentlayer/v2")
    fun getExploreMapMaintenance(): Observable<ExploreMapContentResponse>

    @POST("user/registerdevice")
    fun registerDevice(@Body request: RegisterDeviceRequest): Observable<JsonElement>

    @PUT("user/usersetting")
    fun setUserSettings(@Body request: SettingsResponse): Observable<JsonElement>

    @POST("user/v4/obu/vehicle/filter")
    fun getFilterVehicleDetails(@Body request: ObuVehicleFilter): Observable<ObuVehicleResponse>

    @POST("trip")
    fun setTripSummary(@Body request: TripSummary): Observable<JsonElement>

    @POST("trip/walking")
    fun setTripWalkingSummary(@Body request: TripWalkingSummary): Observable<JsonElement>

    @GET("trip")
    suspend fun getTripSummary(
        @Query("page") page: Int,
        @Query("pagesize") pageSize: Int,
        @Query("startdate") startDate: String,
        @Query("enddate") endDate: String,
        @Query("searchkey") searchkey: String?

    ): TripSummaryResponse

    @GET("trip/planner")
    suspend fun getTripUpcomingSummary(
        @Query("page") page: Int,
        @Query("pagesize") pageSize: Int,
        @Query("startdate") startDate: String,
        @Query("enddate") endDate: String,
        @Query("searchkey") searchkey: String?
    ): TripsPlannerResponse

    @GET("trip/v4/obu/transactions")
    suspend fun getTripObuTransactions(
        @Query("page") page: Int,
        @Query("pagesize") pageSize: Int,
        @Query("startdate") startDate: String,
        @Query("enddate") endDate: String,
        @Query("vehicleNumber") vehicleNumber: String,
        @Query("filter") filter: String?
    ): TripsOBUTransactionResponse

    @GET("trip/planner")
    fun checkTripUpcomingSummary(
        @Query("page") page: Int,
        @Query("pagesize") pageSize: Int,
        @Query("startdate") startDate: String,
        @Query("enddate") endDate: String,
        @Query("searchkey") searchkey: String?
    ): Observable<TripsPlannerResponse>

    @GET("trip/{tripid}")
    fun getTripDetails(@Path("tripid") page: Long): Observable<TripDetailsResponse>

    @GET("trip/planner/{tripPlannerId}")
    fun getUpcomingTripDetails(@Path("tripPlannerId") tripPlannerId: Long): Observable<UpcomingTripDetailResponse>

    @DELETE("trip/planner/{tripid}")
    fun deleteTrip(@Path("tripid") tripId: Long): Observable<JsonElement>

    @DELETE("trip/{tripid}")
    fun deleteTripHistory(@Path("tripid") tripId: Long): Observable<JsonElement>

    @PUT("trip/{tripId}")
    fun updateTripHistory(
        @Path("tripId") page: Long,
        @Body request: TripUpdateRequest
    ): Observable<JsonElement>

    @GET("trip/document/{tripId}")
    fun retrieveFile(@Path("tripId") page: Long): Observable<RetrieveFileResponse>

    @POST("trip/document/upload")
    fun uploadParkingFile(@Body request: TripFileRequest): Observable<UploadFileResponse>

    @POST("trip/document/delete")
    fun deleteFile(@Body request: TripIdRequest): Observable<DeleteFileResponse>


    @POST("trip/sendemail")
    fun sendTripLogEmail(@Body request: SendTripLogRequest): Observable<JsonElement>

    @POST("trip/sendemail")
    fun sendTripLogEmailForMonth(@Body request: SendTripLogByMonthRequest): Observable<JsonElement>

    @POST("trip/copilot")
    fun sendSecondaryFirebaseUserID(@Body request: CopilotAPIRequest): Observable<JsonElement>

    @POST("trip/eta")
    fun sendETAMessage(@Body request: ETARequest): Observable<ETAResponse>

    @POST("trip/eta/status")
    fun updateETATripStatus(@Body request: ETAStatusRequest): Observable<JsonElement>

//    @GET("trip/eta/favourite")
//    suspend fun getAllETA(): ETAFavouritesResponse

    @GET("trip/eta/recent")
    suspend fun getRecentETAContacts(): List<RecentETAContact>


    @POST("feedback/reportissue")
    fun sendFeedBack(@Body request: FeedbackRequest): Observable<JsonElement>

    @POST("feedback/triprating")
    fun sendTripRating(@Body request: JsonObject): Observable<JsonElement>


    @POST("amenitiesv2")
    fun getAmenities(@Body request: AmenitiesRequest): Observable<ResponseAmenities>

    @POST("amenitiesv2")
    suspend fun getAmenitiesAsync(@Body request: AmenitiesRequest): ResponseAmenities

    @POST("trip/planner")
    suspend fun createTripPlan(@Body request: TripPlannerRequest): TripPlannerCreateResponse

    @PUT("trip/planner/{tripPlannerId}")
    suspend fun updateTripPlan(
        @Path("tripPlannerId") tripPlannerId: Long,
        @Body request: TripPlannerRequest
    ): TripPlannerCreateResponse

    @GET("user/messagesv3")
    fun getListNotificationJson(
        @Query("page") page: Int,
        @Query("pagesize") pageSize: Int,
    ): Observable<NotificationResponse>


    @GET("carpark/zonecongestiondetails")
    fun getZoneCongestionDetails(@Query("profile") typeProfile: String): Observable<ArrayList<ResponseCongestionDetail>>

    @GET("carpark/zonecongestiondetails")
    suspend fun getZoneCongestionDetailsAsync(@Query("profile") typeProfile: String): ArrayList<ResponseCongestionDetail>

    @GET("carpark/zonealertdetails")
    fun getZoneAlertDetails(
        @Query("navigationtype") navigationtype: String,
        @Query("profile") profile: String,
        @Query("zoneid") zoneid: String,
        @Query("targetzoneid") targetZoneId: String?,
        @Query("destinationCarpark") destinationCarpark: Boolean?,
        @Query("carparkId") carparkId: String?,
    ): Observable<ResponseZoneAlertDetail>

    @POST("user/tnc")
    fun updateTncAcceptedStatus(@Body request: JsonObject): Observable<JsonElement>

    @GET("user/userdata")
    fun getUserData(): Observable<UserDataResponse>

    @GET("maintenance/contentlayer/details/{contentId}")
    fun getExploreContentDetails(@Path("contentId") contentId: String): Observable<ContentDetailsResponse>

    @POST("user/analytics")
    fun customAnalytics(@Body request: AnalyticsRequest): Observable<JsonElement>

    @GET("user/v3/collection/token/{token}")
    fun saveCollection(@Path("token") token: String): Observable<SaveCollectionResponse>

    @GET("user/location/token/{token}")
    fun getSharedLocation(@Path("token") token: String): Observable<SharedLocationResponse>

    @GET("user/v3/collection/token/{token}")
    fun getSharedCollection(@Path("token") token: String): Observable<SharedCollectionResponse>

    @DELETE("user/v3/bookmarks/amenity/{amenityId}")
    fun deselectingBookmarkTooltip(@Path("amenityId") amenitiesID: String): Observable<DeleteAllBookMarkItemResponse>

    @DELETE("user/v3/bookmarks/address")
    fun deselectingBookmarkDestinationTooltip(
        @Query("address1") address1: String,
        @Query("address2") address2: String
    ): Observable<DeleteAllBookMarkItemResponse>

    @GET("amenities/{id}/broadcast")
    fun getBroadcastMessage(
        @Path("id") idCarpark: String,
        @Query("mode") mode: String,
    ): Observable<BroadcastMessageResponse>

    @GET("carpark/{id}")
    fun getCarParkDetail(@Path("id") idCarpark: String): Observable<CarParkDetailResponse>

    @POST("destination/alternatives")
    fun getCarParkAlternative(@Body request: GetParkingAlternativeRequest): Observable<CarparkAvailabilityResponse>

    @GET("user/guests/generate")
    fun guestGenerate(): Observable<GuestGenResponse>

    @GET("user/name/blacklists")
    fun getNameBlackList(): Observable<SettingNameBlacklist>

    @POST("trip/v4/obu/payment/batch")
    fun saveOBUTransactions(@Body request: SaveOBUTransactionsRequest): Observable<JsonElement>

    @POST("trip/v4/obu/chargeinfo/batch")
    fun saveOBUChargeInfo(@Body request: SaveOBUChargeInfoRequest): Observable<JsonElement>

    @POST("trip/v4/obu/summary")
    fun saveOBUTripSummary(@Body request: SaveOBUTripSummaryRequest): Observable<JsonElement>

    @POST("user/v2/broadcast/travelzone/filter")
    fun broadcastTravelZoneFilter(@Body request: BroadcastTravelZoneFilterRequest): Observable<BroadcastTravelZoneFilterResponse>

    @GET("carpark/availability/alert")
    fun getAvailabilityAlert(
        @Query("distance") distance: String,
        @Query("carparkId") carparkId: String? = null,
        @Query("userLocationLinkIdRef") userLocationLinkIdRef: String? = null,
        @Query("placeId") placeId: String? = null,
        @Query("amenityId") amenityId: String? = null,
        @Query("layerCode") layerCode: String? = null,
    ): Observable<AvailabilityAlertResponse>
}
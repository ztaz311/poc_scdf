package com.ncs.breeze.car.breeze.screen.carkpark

import android.annotation.SuppressLint
import android.graphics.Rect
import android.location.Location
import com.breeze.model.enums.CarParkViewOption
import com.mapbox.android.core.location.LocationEngineCallback
import com.mapbox.android.core.location.LocationEngineProvider
import com.mapbox.android.core.location.LocationEngineResult
import com.mapbox.androidauto.internal.logAndroidAuto
import com.mapbox.geojson.Point
import com.mapbox.maps.CameraOptions
import com.mapbox.maps.EdgeInsets
import com.mapbox.maps.Style
import com.mapbox.maps.extension.androidauto.MapboxCarMapObserver
import com.mapbox.maps.extension.androidauto.MapboxCarMapSurface
import com.mapbox.maps.plugin.animation.camera
import com.mapbox.maps.plugin.locationcomponent.LocationComponentPlugin
import com.mapbox.maps.plugin.locationcomponent.location
import com.mapbox.navigation.ui.maps.camera.NavigationCamera
import com.mapbox.navigation.ui.maps.camera.data.MapboxNavigationViewportDataSource
import com.mapbox.navigation.ui.maps.camera.transition.NavigationCameraTransitionOptions
import com.ncs.breeze.car.breeze.location.CarLocationPuck
import com.ncs.breeze.car.breeze.model.CarParkLayerModel
import com.ncs.breeze.car.breeze.utils.BreezeCarUtil
import com.ncs.breeze.car.breeze.utils.BreezeMapBoxUtil
import com.breeze.model.api.ErrorData
import com.breeze.model.api.request.AmenitiesRequest
import com.breeze.model.api.response.amenities.BaseAmenity
import com.breeze.model.constants.AmenityType.CARPARK
import com.breeze.model.api.response.amenities.ResponseAmenities
import com.ncs.breeze.common.remote.ApiObserver
import com.breeze.model.constants.Constants
import com.ncs.breeze.common.utils.Utils
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class CarParkLayerRenderer(
    private val carParkCarContext: CarParkCarContext,
    private val carParkScreen: CarParkScreen,
    val location: Location? = null,
    var typeCarParkOption: CarParkViewOption = CarParkViewOption.ALL_NEARBY
) : MapboxCarMapObserver {

    private var viewportDataSource: MapboxNavigationViewportDataSource? = null
    private var navigationCamera: NavigationCamera? = null
    private var overviewEdgeInsets: EdgeInsets? = null
    private var followingEdgeInsets: EdgeInsets? = null

    private val carParkFeatureDataList = ArrayList<CarParkLayerModel>();
    var compositeDisposable: CompositeDisposable = CompositeDisposable()
    private var locationComponent: LocationComponentPlugin? = null

    var currentLocation: Location? = null

    private var mapboxCarMapSurface: MapboxCarMapSurface? = null

    lateinit var style: Style

    @SuppressLint("MissingPermission")
    override fun onAttached(mapboxCarMapSurface: MapboxCarMapSurface) {
        this.mapboxCarMapSurface = mapboxCarMapSurface

        mapboxCarMapSurface.mapSurface.getMapboxMap().getStyle()?.let {
            style = it
            locationComponent = mapboxCarMapSurface.mapSurface.location.apply {
                locationPuck =
                    CarLocationPuck.homePuck2D(carParkCarContext.mainCarContext.carContext)
                enabled = true
                pulsingEnabled = false
            }
            viewportDataSource = MapboxNavigationViewportDataSource(
                mapboxCarMapSurface.mapSurface.getMapboxMap()
            ).apply {
                overviewPitchPropertyOverride(0.0)
                overviewEdgeInsets?.let { padding ->
                    overviewPadding = padding
                }
                followingEdgeInsets?.let { padding ->
                    followingPadding = padding
                }
            }

            navigationCamera = NavigationCamera(
                mapboxCarMapSurface.mapSurface.getMapboxMap(),
                mapboxCarMapSurface.mapSurface.camera,
                viewportDataSource!!
            )

            if (location != null) {
                renderCarParks(location, it)
                return
            }
            val locationEngine =
                LocationEngineProvider.getBestLocationEngine(carParkCarContext.mainCarContext.carContext);

            locationEngine.getLastLocation(object : LocationEngineCallback<LocationEngineResult> {
                override fun onSuccess(result: LocationEngineResult?) {
                    if (result != null && result.lastLocation != null) {
                        currentLocation = result.lastLocation

                        renderCarParks(
                            currentLocation!!,
                            it,
                        )

                    }
                }

                override fun onFailure(p0: java.lang.Exception) {
                }
            })

        }
    }


    fun renderCarParks() {
        CoroutineScope(Dispatchers.IO).launch {
            if (currentLocation != null) {
                var currentLocationName: String? = Utils.getAddressFromLocation(
                    currentLocation!!,
                    carParkCarContext.mainCarContext.carContext
                )
                withContext(Dispatchers.Main) {
                    if (currentLocationName.isNullOrEmpty()) {
                        currentLocationName = ""
                    }
                    val point: Point =
                        Point.fromLngLat(currentLocation!!.longitude, currentLocation!!.latitude);
                    val cameraOptions = CameraOptions.Builder()
                        .center(point)
                        .zoom(15.0)
                        .pitch(0.0)
                        .bearing(0.0)
                        .build()
                    mapboxCarMapSurface?.mapSurface?.getMapboxMap()
                        ?.setCamera(cameraOptions)
                    // Change view
                    // Zoom to view

                    getNearbyCarParks(
                        style,
                        currentLocation!!.latitude,
                        currentLocation!!.longitude,
                        currentLocationName!!,
                        currentLocation!!
                    )
                }
            }
        }
    }


    fun renderCarParks(
        currentLocation: Location,
        it: Style,
    ) {
        CoroutineScope(Dispatchers.IO).launch {
            var currentLocationName: String? = Utils.getAddressFromLocation(
                currentLocation,
                carParkCarContext.mainCarContext.carContext
            )
            withContext(Dispatchers.Main) {
                if (currentLocationName.isNullOrEmpty()) {
                    currentLocationName = ""
                }

                getNearbyCarParks(
                    it,
                    currentLocation.latitude,
                    currentLocation.longitude,
                    currentLocationName!!,
                    currentLocation
                )
            }
        }
    }


    override fun onVisibleAreaChanged(visibleArea: Rect, edgeInsets: EdgeInsets) {
        super.onVisibleAreaChanged(visibleArea, edgeInsets)
        logAndroidAuto("CarNavigationCamera visibleAreaChanged $visibleArea $edgeInsets")

        viewportDataSource?.overviewPadding = EdgeInsets(
            edgeInsets.top + OVERVIEW_PADDING,
            edgeInsets.left + OVERVIEW_PADDING,
            edgeInsets.bottom + OVERVIEW_PADDING,
            edgeInsets.right + OVERVIEW_PADDING
        ).also { overviewEdgeInsets = it }

        val visibleHeight = visibleArea.bottom - visibleArea.top
        val followingBottomPadding = visibleHeight * BOTTOM_FOLLOWING_PERCENTAGE
        viewportDataSource?.followingPadding = EdgeInsets(
            edgeInsets.top,
            edgeInsets.left,
            edgeInsets.bottom + followingBottomPadding,
            edgeInsets.right
        ).also { followingEdgeInsets = it }

        viewportDataSource?.evaluate()
    }


    override fun onDetached(mapboxCarMapSurface: MapboxCarMapSurface) {
        viewportDataSource = null
        navigationCamera = null
        mapboxCarMapSurface.mapSurface.getMapboxMap().getStyle()?.let {
            BreezeMapBoxUtil.removeCarkParkData(it, carParkFeatureDataList);
        }
        compositeDisposable.dispose()
    }

    //TODO handle errors
    private fun getNearbyCarParks(
        style: Style,
        latitude: Double?,
        longitude: Double?,
        destName: String,
        currentLocation: Location
    ) {

        val listCarPark: ArrayList<BaseAmenity> = arrayListOf()
        val carParkRequest = createCarParkRequest(currentLocation, destName)
        fetchAndUpdateCarParks(
            carParkRequest,
            listCarPark,
            longitude,
            latitude,
            style,
            currentLocation
        )
    }

    private fun fetchAndUpdateCarParks(
        carParkRequest: AmenitiesRequest,
        listCarPark: ArrayList<BaseAmenity>,
        longitude: Double?,
        latitude: Double?,
        style: Style,
        currentLocation: Location
    ) {
        BreezeCarUtil.apiHelper.getAmenities(carParkRequest)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : ApiObserver<ResponseAmenities>(compositeDisposable) {
                override fun onSuccess(data: ResponseAmenities) {
                    data.apply {
                        data.amenities.forEach lit@{ amenities ->
                            amenities.data.forEach {
                                if (amenities.type != null) {
                                    it.amenityType = amenities.type!!
                                    it.iconUrl = amenities.iconUrl
                                    listCarPark.add(it)
                                }
                            }
                        }

                        val (sortedValueItem, curLocPoint) = updateMapAndTemplateView(
                            listCarPark,
                            longitude,
                            latitude,
                            style
                        )
                        updateMapCamera(sortedValueItem, curLocPoint, currentLocation)
                    }


                }

                override fun onError(e: ErrorData) {

                }
            })
    }

    private fun createCarParkRequest(
        currentLocation: Location,
        destName: String
    ): AmenitiesRequest {
        val carParkRequest = AmenitiesRequest(
            lat = currentLocation.latitude,
            long = currentLocation.longitude,
            rad = BreezeCarUtil.breezeUserPreferenceUtil.getBreezeUserPreference()
                .getCarParkDistanceSettingValue(BreezeCarUtil.masterlists),
            maxRad = Constants.PARKING_CHECK_RADIUS_MAX,
            resultCount = BreezeCarUtil.breezeUserPreferenceUtil.getBreezeUserPreference()
                .getCarParkCountSettingValue(BreezeCarUtil.masterlists),
            destName = destName,
        )

        carParkRequest.setListType(
            listOf(CARPARK),
            BreezeCarUtil.breezeUserPreferenceUtil.getBreezeUserPreference()
                .getCarParkAvailabilityValue(BreezeCarUtil.masterlists, typeCarParkOption.mode)
        )
        return carParkRequest
    }

    private fun updateMapCamera(
        sortedCarparkItem: List<BaseAmenity>,
        curLocPoint: Point,
        currentLocation: Location
    ) {
        val points = sortedCarparkItem.map {
            Point.fromLngLat(it.long!!, it.lat!!)
        }
        val listWithMarker = ArrayList(points);
        listWithMarker.add(curLocPoint)

        val instantTransition = NavigationCameraTransitionOptions.Builder()
            .maxDuration(0)
            .build()
        viewportDataSource?.onLocationChanged(currentLocation)
        viewportDataSource?.additionalPointsToFrameForOverview(listWithMarker)
        //viewportDataSource.options.overviewFrameOptions.maxZoom=15.0;
        viewportDataSource?.evaluate()
        navigationCamera
            ?.requestNavigationCameraToOverview(
                stateTransitionOptions = instantTransition
            )

    }

    private fun updateMapAndTemplateView(
        listCarPark: ArrayList<BaseAmenity>,
        longitude: Double?,
        latitude: Double?,
        style: Style
    ): Pair<List<BaseAmenity>, Point> {
        val sortedValueItem =
            listCarPark.sortedWith(
                compareBy<BaseAmenity> {
                    it.isSeasonParking()
                }.thenByDescending {
                    it.isCheapest
                }.thenBy {
                    it.destinationCarPark
                }
            ).take(6)

        val curLocPoint = Point.fromLngLat(longitude!!, latitude!!)
        BreezeMapBoxUtil.displayCarkParkData(
            carParkFeatureDataList,
            style,
            sortedValueItem,
            carParkCarContext.mainCarContext.carContext
        )
        carParkScreen.loadPlaceRecords(carParkFeatureDataList, curLocPoint);
        return Pair(sortedValueItem, curLocPoint)
    }


    /**
     * update type new carpark & update filter carpark
     */
    fun updateFilterCarPark(typeCarPark: CarParkViewOption) {
        if (typeCarPark != CarParkViewOption.HIDE) {
            typeCarParkOption = typeCarPark
            renderCarParks()
        }
    }

    private companion object {
        /**
         * While following the location puck, inset the bottom by 1/3 of the screen.
         */
        private const val BOTTOM_FOLLOWING_PERCENTAGE = 1.0 / 3.0

        /**
         * While overviewing a route, add padding to thew viewport.
         */
        private const val OVERVIEW_PADDING = 30
    }


}
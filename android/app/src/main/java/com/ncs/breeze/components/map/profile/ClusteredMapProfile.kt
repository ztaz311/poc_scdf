package com.ncs.breeze.components.map.profile

import androidx.appcompat.app.AlertDialog
import androidx.collection.ArraySet
import androidx.core.os.bundleOf
import com.breeze.model.api.ErrorData
import com.breeze.model.api.response.AmenitiesPreference
import com.breeze.model.api.response.ContentDetails
import com.breeze.model.api.response.DeleteAllBookMarkItemResponse
import com.breeze.model.api.response.amenities.BaseAmenity
import com.breeze.model.constants.AmenityType.CARPARK
import com.breeze.model.constants.AmenityType.POI
import com.breeze.model.constants.Constants
import com.facebook.react.bridge.Arguments
import com.google.android.gms.maps.model.LatLng
import com.google.gson.JsonObject
import com.mapbox.geojson.Feature
import com.mapbox.geojson.Point
import com.ncs.breeze.App
import com.ncs.breeze.R
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.Event
import com.ncs.breeze.common.analytics.Screen
import com.ncs.breeze.common.extensions.android.getShareBusinessLogicPackage
import com.ncs.breeze.common.extensions.mapbox.updateDestinationAndLocationPuckLayer
import com.ncs.breeze.common.remote.ApiObserver
import com.ncs.breeze.common.utils.BreezeMapDataHolder
import com.ncs.breeze.common.utils.BreezeMapZoomUtil
import com.ncs.breeze.components.layermanager.MarkerLayerManager
import com.ncs.breeze.components.layermanager.impl.cluster.ClusteredMarkerLayerManager
import com.ncs.breeze.components.marker.markerview2.AmenitiesMarkerView
import com.ncs.breeze.components.marker.markerview2.ExplorePoiMarkerView
import com.ncs.breeze.components.marker.markerview2.MarkerView2
import com.ncs.breeze.components.marker.markerview2.PoiMarkerView
import com.ncs.breeze.components.marker.markerview2.carpark.CarparkMarkerView
import com.ncs.breeze.helper.marker.ClusteredAmenityMarkerHelper
import com.ncs.breeze.reactnative.nativemodule.ShareBusinessLogicEvent
import com.ncs.breeze.ui.dashboard.fragments.exploremap.ContentDetailKey
import com.ncs.breeze.ui.dashboard.fragments.exploremap.ContentWrapperDataPOI
import com.ncs.breeze.ui.dashboard.manager.DashboardMapStateManager
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import timber.log.Timber

/**
 * Map profile that can display amenities such as petrol, ev charger, etc.
 * In certain zoom levels these amenities will be grouped into clusters that show as as dot in map
 * */
class ClusteredMapProfile(
    private val mapProfileOptions: MapProfileOptions
) : MapProfile {

    var markerTooltipView: MarkerView2? = null
    var isDestroyed = false
    var compositeDisposable: CompositeDisposable = CompositeDisposable()

    /**
     * for manager all marker of amenities
     */
    private val clusteredAmenitiesMarkerViewManager: ClusteredMarkerLayerManager =
        ClusteredMarkerLayerManager(
            mapProfileOptions.mapView,
            nightMode = mapProfileOptions.nightModeEnabled
        )
    private val clusteredAmenityMarkerHelper: ClusteredAmenityMarkerHelper =
        ClusteredAmenityMarkerHelper(
            clusteredAmenitiesMarkerViewManager,
            mapProfileOptions.nightModeEnabled,
            true
        )

    init {
        clusteredAmenitiesMarkerViewManager.markerLayerClickHandler =
            mapProfileOptions.markerLayerClickHandler
        clusteredAmenitiesMarkerViewManager.clusterLayerClickHandler =
            mapProfileOptions.clusterLayerClickHandler
        CoroutineScope(Dispatchers.Main).launch {
            clusteredAmenitiesMarkerViewManager.addStyleImages(CARPARK)
        }
    }

    fun addExternalLayerData(vararg data: Pair<String, String>) {
        data.forEach {
            clusteredAmenitiesMarkerViewManager.externalLayerIds[it.first] = it.second
        }
    }

    fun addExternalSourceData(vararg data: Pair<String, String>) {
        data.forEach {
            clusteredAmenitiesMarkerViewManager.externalSourceIds[it.first] = it.second
        }
    }

    override suspend fun loadAmenityStyleResources(amenityTypes: ArrayList<String>) {
        clusteredAmenitiesMarkerViewManager.addStyleImages(amenityTypes)
        clusteredAmenitiesMarkerViewManager.loadAmenityClusteredColorMap(amenityTypes)
        clusteredAmenitiesMarkerViewManager.loadCarParkClusterId()
    }

    override suspend fun loadImageStyleResources(listContentDetail: ArrayList<ContentDetails>) {
        clusteredAmenitiesMarkerViewManager.addStyleImagesWithContentDetail(listContentDetail)
    }


    override fun handleAmenitiesData(
        allAmenities: HashMap<String, List<BaseAmenity>>,
        showDestination: Boolean,
        selectedProfile: String?,
        shouldShowAmenity: (AmenitiesPreference) -> Boolean,
    ) {
        val poiAmenities = HashMap<String, List<BaseAmenity>>()
        filterClusteredPoiAmenities(selectedProfile, allAmenities, poiAmenities)
        val supportedAmenities = determineSupportedAmenities(allAmenities, poiAmenities)
        clusteredAmenitiesMarkerViewManager.removeAllMarkersExcludingType(CARPARK)
        supportedAmenities.forEach {
            clusteredAmenitiesMarkerViewManager.removeMarkerTooltipView(it.key)
            if (poiAmenities[it.key] != null) {
                clusteredAmenitiesMarkerViewManager.nonClusteredTypes.add(it.key)
                clusteredAmenityMarkerHelper.addAmenityMarkerView(
                    it.key,
                    it.value,
                    false,
                    showDestination
                )
            } else {
                val preferenceAmenities = BreezeMapDataHolder.amenitiesPreferenceHashmap[it.key]
                preferenceAmenities?.let { amenityPref ->
                    if (amenityPref.subItems != null) {
                        val listEnabled = getSelectedSubItems(amenityPref, it.value)
                        val isEnable = shouldShowAmenity(amenityPref)
                        if (isEnable) {
                            amenityPref.clusterId?.let { clusterId ->
                                clusteredAmenityMarkerHelper.appendAmenityMarkerView(
                                    clusterId.toString(),
                                    it.key,
                                    listEnabled,
                                    !isEnable,
                                    showDestination
                                )
                            }
                        }
                    } else {
                        val isEnable = shouldShowAmenity(amenityPref)
                        if (isEnable) {

                            amenityPref.clusterId?.let { clusterId ->
                                clusteredAmenityMarkerHelper.appendAmenityMarkerView(
                                    clusterId.toString(),
                                    it.key,
                                    it.value,
                                    !isEnable,
                                    showDestination
                                )
                            }
                        }
                    }
                }
            }
        }
        if (allAmenities.size > 0) {
            mapProfileOptions.mapView.updateDestinationAndLocationPuckLayer()
        }
    }

    override fun handleContentDetailData(
        listAmenities: HashMap<ContentDetailKey, List<BaseAmenity>>,
        showDestination: Boolean,
        selectedProfile: String?,
        shouldShowAmenity: (AmenitiesPreference) -> Boolean
    ) {
        val supportedAmenities = HashMap(listAmenities)
        val listContentWrapperForSort = ArrayList<ContentWrapperDataPOI>()
        supportedAmenities.forEach {
            listContentWrapperForSort.add(ContentWrapperDataPOI(it.key, it.value))
        }
        listContentWrapperForSort.sortBy { it.contentDetailKey.tooltip_type }
        listContentWrapperForSort.forEach {
            clusteredAmenitiesMarkerViewManager.removeMarkerTooltipView(it.contentDetailKey.code)
            val isEnable = true
            clusteredAmenityMarkerHelper.addAmenityMarkerView(
                it.contentDetailKey.code,
                it.listAmenities,
                !isEnable,
                showDestination
            )
        }
        if (supportedAmenities.size > 0) {
            mapProfileOptions.mapView.updateDestinationAndLocationPuckLayer()
        }
    }

    private fun determineSupportedAmenities(
        allAmenities: HashMap<String, List<BaseAmenity>>,
        poiAmenities: HashMap<String, List<BaseAmenity>>
    ): HashMap<String, List<BaseAmenity>> {
        val supportedAmenities = HashMap(allAmenities)
        supportedAmenities.remove(POI)
        supportedAmenities.remove(CARPARK)
        supportedAmenities.putAll(poiAmenities)
        return supportedAmenities
    }

    override fun handleCarPark(
        carparks: List<BaseAmenity>,
        isMoveCameraWrapperAllCarpark: Boolean,
        showDestination: Boolean,
        isMarkerHidden: Boolean,
        selectedProfile: String?
    ) {
        clusteredAmenitiesMarkerViewManager.removeMarkerTooltipView(CARPARK)
        clusteredAmenitiesMarkerViewManager.removeMarkers(CARPARK)
        clusteredAmenitiesMarkerViewManager.carParkGroupId?.let {
            clusteredAmenitiesMarkerViewManager.removeMarkers(it, CARPARK)
        }
        mapProfileOptions.carParkDestinationIconManager.handleDestinationCarParkIcon(
            carparks,
            showDestination
        )
        clusteredAmenitiesMarkerViewManager.nonClusteredTypes.add(CARPARK)

        val clusteredCarParks = ArraySet<BaseAmenity>()
        val nonClusteredCarParks = ArraySet<BaseAmenity>()
        nonClusteredCarParks.addAll(carparks)
        clusteredAmenityMarkerHelper.addAmenityMarkerView(
            CARPARK,
            nonClusteredCarParks.toList(),
            isMarkerHidden,
            showDestination
        )
        clusteredAmenitiesMarkerViewManager.carParkGroupId?.let {
            clusteredAmenityMarkerHelper.appendAmenityMarkerView(
                it,
                CARPARK,
                clusteredCarParks.toList(),
                isMarkerHidden,
                showDestination
            )
        }
        if (carparks.isNotEmpty()) {
            mapProfileOptions.mapView.updateDestinationAndLocationPuckLayer()
        }
    }

    /**
     * can be used to filter when carpark clustering is enabled
     * @param selectedProfile the profile selected
     * @param mListCarPark the list of carpark amenities
     * @param clusteredCarParks the set of Clustered Carparks (should generally be empty)
     * @param nonClusteredCarParks the set of Non-clustered Carparks (should generally be empty)
     * @param showDestination whether a destination exists and is shown
     */
    private fun filterClusteredCarParks(
        selectedProfile: String?,
        mListCarPark: List<BaseAmenity>,
        clusteredCarParks: ArraySet<BaseAmenity>,
        nonClusteredCarParks: ArraySet<BaseAmenity>,
        showDestination: Boolean,
    ) {
        val profileCarParks =
            BreezeMapDataHolder.profilePreferenceHashmap[selectedProfile]?.defaultAmenities?.find { it.type == CARPARK }
        clusteredCarParks.addAll(mListCarPark)
        for (carPark in mListCarPark) {
            if (showDestination && carPark.destinationCarPark == true) {
                clusteredCarParks.remove(carPark)
                nonClusteredCarParks.add(carPark)
                continue;
            }
            if (profileCarParks == null) {
                clusteredCarParks.remove(carPark)
                nonClusteredCarParks.add(carPark)
            } else {
                profileCarParks.ids.forEach { profileCPId ->
                    if (carPark.id == profileCPId) {
                        clusteredCarParks.remove(carPark)
                        nonClusteredCarParks.add(carPark)
                    }
                }
            }
        }
    }


    private fun filterClusteredPoiAmenities(
        selectedProfile: String?,
        allAmenities: HashMap<String, List<BaseAmenity>>,
        poiAmenities: HashMap<String, List<BaseAmenity>>,
    ) {
        val poiData = allAmenities[POI] ?: return
        val profilePoiAmenities =
            BreezeMapDataHolder.profilePreferenceHashmap[selectedProfile]?.amenities

        profilePoiAmenities?.let { profilePoiAmenities ->
            profilePoiAmenities.forEach { poi ->
                val selectedPIList = poiData.filter { it.provider == poi }
                if (selectedPIList.isNotEmpty()) {
                    poiAmenities[poi] = selectedPIList
                }
            }
        }
    }

    override fun clearALlCarPark() {
        clusteredAmenitiesMarkerViewManager.removeMarkers(CARPARK)
        clusteredAmenitiesMarkerViewManager.carParkGroupId?.let {
            clusteredAmenitiesMarkerViewManager.removeMarkers(it, CARPARK)
        }
        mapProfileOptions.carParkDestinationIconManager.showDestinationIcon()
    }

    override fun clearALlMarkerAndTooltipAmenities() {
        mapProfileOptions.carParkDestinationIconManager.removeDestinationIconTooltip()
        clusteredAmenitiesMarkerViewManager.removeMarkerTooltipView()
        clusteredAmenitiesMarkerViewManager.removeAllMarkers()
    }

    override fun removeALlTooltip() {
        clusteredAmenitiesMarkerViewManager.removeMarkerTooltipView()
    }

    override fun filterAllAmenitiesSelect(
        amenityType: String,
        isSelected: Boolean,
        amenityItems: List<BaseAmenity>,
        showDestination: Boolean
    ) {
        if (isSelected) {
            if (clusteredAmenitiesMarkerViewManager.nonClusteredTypes.contains(amenityType)) {
                clusteredAmenityMarkerHelper.addAmenityMarkerView(
                    amenityType,
                    amenityItems,
                    false,
                    showDestination
                )
                mapProfileOptions.mapView.updateDestinationAndLocationPuckLayer()
            } else {
                BreezeMapDataHolder.amenitiesPreferenceHashmap[amenityType]?.let { pref ->
                    pref.clusterId?.let {
                        clusteredAmenityMarkerHelper.appendAmenityMarkerView(
                            it.toString(),
                            amenityType,
                            amenityItems,
                            false,
                            showDestination
                        )
                        mapProfileOptions.mapView.updateDestinationAndLocationPuckLayer()
                    }
                }
            }
        } else {
            if (clusteredAmenitiesMarkerViewManager.nonClusteredTypes.contains(amenityType)) {
                clusteredAmenitiesMarkerViewManager.removeMarkerTooltipView(amenityType)
                clusteredAmenitiesMarkerViewManager.removeMarkers(amenityType)
            } else {
                BreezeMapDataHolder.amenitiesPreferenceHashmap[amenityType]?.let { pref ->
                    pref.clusterId?.let {
                        clusteredAmenitiesMarkerViewManager.removeMarkerTooltipView(it.toString())
                        clusteredAmenityMarkerHelper.removeAmenityMarkerFeatures(
                            it.toString(),
                            amenityType,
                            amenityItems,
                            false,
                            showDestination
                        )
                    }
                }
            }
        }
    }

    override fun showCarParkToolTip(
        amenity: BaseAmenity,
        showDestination: Boolean,
        showBookmark: Boolean,
        toolTipAction: ListenerCallbackLandingMap?,
        layerType: String,
        shouldRecenter: Boolean,
        startRoutePlanningFromMarker: (item: BaseAmenity, isCarPark: Boolean) -> Unit,

        ) {
        val carParkMarkerView = CarparkMarkerView(
            LatLng(amenity.lat!!, amenity.long!!),
            mapProfileOptions.mapView.getMapboxMap(),
            mapProfileOptions.activity,
            amenity.id,
            showBookmark,
            Constants.POI_TOOLTIP_ZOOM_RADIUS
        )
        markerTooltipView = carParkMarkerView
        val properties = JsonObject()
        clusteredAmenityMarkerHelper.handleSelectedCarParkProperties(
            CARPARK,
            amenity,
            properties,
            showDestination,
            false
        )
        val feature = Feature.fromGeometry(
            Point.fromLngLat(
                amenity.long!!,
                amenity.lat!!
            ), properties, amenity.id
        )

        clusteredAmenitiesMarkerViewManager.renderMarkerToolTipView(
            MarkerLayerManager.MarkerViewType(
                carParkMarkerView,
                layerType,
                feature
            )
        )

        carParkMarkerView.clickToolTip(
            amenity,
            shouldRecenter,
            screenName = mapProfileOptions.screenName,
            navigationButtonClick = { item, eventSource ->
                Analytics.logClickEvent(Event.NAVIGATE_HERE_PARKING, mapProfileOptions.screenName)
                Analytics.logLocationDataEvent(
                    Event.NAVIGATE_HERE_PARKING,
                    mapProfileOptions.screenName,
                    item.name.toString(),
                    item.lat.toString(),
                    item.long.toString()
                )
                startRoutePlanningFromMarker(item, true)
            },
            moreInfoButtonClick = { item ->
                Analytics.logClickEvent(Event.PARKING_SEE_MORE, mapProfileOptions.screenName)
                toolTipAction?.moreInfoButtonClick(item)
            },
            carParkIconClick = { isEnable ->
                toolTipAction?.onDismissedCameraTracking()
            },
            bookmarkIconClick = { amenity, _ ->
                if (amenity.isBookmarked) {
                    amenity.id?.let { idAmenities ->
                        Analytics.logClickEvent(
                            Event.CAR_PARK_TOOLTIP_SAVE_OFF,
                            mapProfileOptions.screenName
                        )
                        showDeleteSavedPlaceConfirmation(idAmenities)
                    }
                } else {
                    Analytics.logClickEvent(
                        Event.CAR_PARK_TOOLTIP_SAVE_ON,
                        mapProfileOptions.screenName
                    )
                    bookmarkAmenityRN(amenity)
                }
            })

        if (shouldRecenter) {
            mapProfileOptions?.mapCamera?.let {
                BreezeMapZoomUtil.recenterMapToLocation(
                    mapProfileOptions.mapView.getMapboxMap(),
                    lat = amenity.lat!!.toDouble(),
                    lng = amenity.long!!.toDouble(),
                    edgeInset = it.overviewEdgeInsets,
                    zoomRadius = Constants.POI_TOOLTIP_ZOOM_RADIUS
                )
            }
        }
    }

    override fun handleAmenityToolTip(
        amenity: BaseAmenity,
        featureType: String,
        toolTipAction: ListenerCallbackLandingMap?,
        layerType: String,
        shouldRecenter: Boolean,
        showBookmark: Boolean,
        enableWalking: Boolean,
        enableZoneDrive: Boolean,
        startRoutePlanningFromMarker: (item: BaseAmenity, isCarPark: Boolean) -> Unit,
        startWalkingPathFromMarker: ((item: BaseAmenity, isCarPark: Boolean) -> Unit)?
    ) {

        val amenitiesMarkerView = AmenitiesMarkerView(
            LatLng(amenity.lat!!, amenity.long!!),
            mapProfileOptions.mapView.getMapboxMap(),
            mapProfileOptions.activity,
            amenity.id,
            amenity.amenityType,
            showBookmark,
            Constants.POI_TOOLTIP_ZOOM_RADIUS,
            enableWalking,
            enableZoneDrive
        )
        amenitiesMarkerView.analyticsScreenName = mapProfileOptions.screenName
        markerTooltipView = amenitiesMarkerView
        val properties = JsonObject()
        clusteredAmenityMarkerHelper.handleSelectedAmenityProperties(
            amenity.amenityType,
            properties,
            false,
            amenity
        )
        val amenityFeature = Feature.fromGeometry(
            Point.fromLngLat(
                amenity.long!!,
                amenity.lat!!
            ), properties, amenity.id
        )
        clusteredAmenitiesMarkerViewManager.renderMarkerToolTipView(
            MarkerLayerManager.MarkerViewType(
                amenitiesMarkerView,
                layerType,
                amenityFeature
            )
        )

        amenitiesMarkerView.handleClickMarker(
            amenity,
            shouldRecenter,
            navigationButtonClick = { item, _ ->
                startRoutePlanningFromMarker(item, false)

                Analytics.logClickEvent(
                    "navigate_here_amenities_" + featureType,
                    mapProfileOptions.screenName
                )
                Analytics.logLocationDataEvent(
                    "navigate_here_amenities_" + featureType,
                    mapProfileOptions.screenName,
                    item.name.toString(),
                    item.lat.toString(),
                    item.long.toString()
                )
            },
            amenitiesIconClick = { item, _ ->
                Analytics.logClickEvent(
                    "map_" + featureType + "_icon_click",
                    mapProfileOptions.screenName
                )
                Analytics.logLocationDataEvent(
                    "map_" + featureType + "_icon_click",
                    mapProfileOptions.screenName,
                    item.name.toString(),
                    item.lat.toString(),
                    item.long.toString()
                )
                toolTipAction?.onDismissedCameraTracking()
            },
            bookmarkIconClick = { item, _ ->
                if (item.isBookmarked) {
                    item.id?.let { idAmenities ->
                        Analytics.logClickEvent(
                            Event.AMENITIES_TOOLTIP_SAVE_OFF,
                            mapProfileOptions.screenName
                        )
                        showDeleteSavedPlaceConfirmation(idAmenities)
                    }
                } else {
                    Analytics.logClickEvent(
                        Event.AMENITIES_TOOLTIP_SAVE_ON,
                        mapProfileOptions.screenName
                    )
                    bookmarkAmenityRN(item)
                }
            },
            walkingIconClick = { item, _ -> }
        )
    }

    override fun handlePoiToolTip(
        amenity: BaseAmenity,
        featureType: String,
        toolTipAction: ListenerCallbackLandingMap?,
        layerType: String,
        shouldRecenter: Boolean,
        startRoutePlanningFromMarker: (item: BaseAmenity, isCarPark: Boolean) -> Unit,
        startWalkingPathFromMarker: (item: BaseAmenity, isCarPark: Boolean) -> Unit
    ) {

        val amenitiesMarkerView = PoiMarkerView(
            LatLng(amenity.lat!!, amenity.long!!),
            mapProfileOptions.mapView.getMapboxMap(),
            mapProfileOptions.activity,
            amenity.id,
            featureType,
            Constants.POI_TOOLTIP_ZOOM_RADIUS
        )
        val properties = JsonObject()
        clusteredAmenityMarkerHelper.handleSelectedAmenityProperties(
            featureType,
            properties,
            false,
            amenity
        )
        val amenityFeature = Feature.fromGeometry(
            Point.fromLngLat(
                amenity.long!!,
                amenity.lat!!
            ), properties, amenity.id
        )
        clusteredAmenitiesMarkerViewManager.renderMarkerToolTipView(
            MarkerLayerManager.MarkerViewType(
                amenitiesMarkerView,
                layerType,
                amenityFeature
            )
        )

        amenitiesMarkerView.handleClickMarker(
            amenity,
            shouldRecenter,
            walkHereButtonClick = { item, _ ->
                startWalkingPathFromMarker(item, false)

                Analytics.logClickEvent(
                    "walk_here_amenities_" + item.amenityType,
                    mapProfileOptions.screenName
                )
                Analytics.logLocationDataEvent(
                    "walk_here_amenities_" + item.amenityType,
                    mapProfileOptions.screenName,
                    item.name.toString(),
                    item.lat.toString(),
                    item.long.toString()
                )
            },
            driveHereButtonClick = { item, _ ->
                startRoutePlanningFromMarker(item, false)

                Analytics.logClickEvent(
                    "navigate_here_amenities_" + item.amenityType,
                    mapProfileOptions.screenName
                )
                Analytics.logLocationDataEvent(
                    "navigate_here_amenities_" + item.amenityType,
                    mapProfileOptions.screenName,
                    item.name.toString(),
                    item.lat.toString(),
                    item.long.toString()
                )
            },
            amenitiesIconClick = { item, _ ->
                Analytics.logClickEvent(
                    "map_" + item.amenityType + "_icon_click",
                    mapProfileOptions.screenName
                )
                Analytics.logLocationDataEvent(
                    "map_" + item.amenityType + "_icon_click",
                    mapProfileOptions.screenName,
                    item.name.toString(),
                    item.lat.toString(),
                    item.long.toString()
                )
                toolTipAction?.onDismissedCameraTracking()
            })
    }

    override fun handlePoiToolTipMapExplore(
        amenity: BaseAmenity,
        featureType: String,
        toolTipAction: ListenerCallbackLandingMap?,
        layerType: String,
        shouldRecenter: Boolean,
        enableWalking: Boolean,
        enableZoneDrive: Boolean,
        startRoutePlanningFromMarker: (item: BaseAmenity, isCarPark: Boolean) -> Unit,
        startWalkingPathFromMarker: (item: BaseAmenity, isCarPark: Boolean) -> Unit
    ) {
        val explorePoiMarkerView = ExplorePoiMarkerView(
            latLng = LatLng(amenity.lat!!, amenity.long!!),
            mapboxMap = mapProfileOptions.mapView.getMapboxMap(),
            activity = mapProfileOptions.activity,
            itemId = amenity.id,
            itemType = featureType,
            enableWalking = enableWalking,
            enableZoneDrive = enableZoneDrive
        )
        val properties = JsonObject()
        clusteredAmenityMarkerHelper.handleSelectedAmenityProperties(properties, false, amenity)
        val amenityFeature = Feature.fromGeometry(
            Point.fromLngLat(
                amenity.long!!,
                amenity.lat!!
            ), properties, amenity.id
        )
        clusteredAmenitiesMarkerViewManager.renderMarkerToolTipView(
            MarkerLayerManager.MarkerViewType(
                explorePoiMarkerView,
                layerType,
                amenityFeature
            )
        )

        explorePoiMarkerView.handleClickMarker(
            amenity,
            shouldRecenter,
            walkHereButtonClick = { item, _ ->
                startWalkingPathFromMarker(item, false)

                Analytics.logClickEvent(
                    "walk_here_amenities_" + item.name,
                    mapProfileOptions.screenName
                )
                Analytics.logLocationDataEvent(
                    "walk_here_amenities_" + item.name,
                    mapProfileOptions.screenName,
                    item.name.toString(),
                    item.lat.toString(),
                    item.long.toString()
                )
            },
            driveHereButtonClick = { item, _ ->
                startRoutePlanningFromMarker(item, false)

                Analytics.logClickEvent(
                    "navigate_here_amenities_" + item.name,
                    mapProfileOptions.screenName
                )
                Analytics.logLocationDataEvent(
                    "navigate_here_amenities_" + item.name,
                    mapProfileOptions.screenName,
                    item.name.toString(),
                    item.lat.toString(),
                    item.long.toString()
                )
            },
            amenitiesIconClick = { item, _ ->
                Analytics.logClickEvent(
                    "map_" + item.name + "_icon_click",
                    mapProfileOptions.screenName
                )
                Analytics.logLocationDataEvent(
                    "map_" + item.name + "_icon_click",
                    mapProfileOptions.screenName,
                    item.name.toString(),
                    item.lat.toString(),
                    item.long.toString()
                )
                toolTipAction?.onDismissedCameraTracking()
            })

    }

    private fun showDeleteSavedPlaceConfirmation(amenitiesID: String) {
        Analytics.logOpenPopupEvent(Event.POPUP_UNSAVE_COLLECTION, mapProfileOptions.screenName)
        val builder: AlertDialog.Builder = mapProfileOptions.activity.let {
            AlertDialog.Builder(it)
        }
        val message: String =
            mapProfileOptions.activity.getString(R.string.desc_delete_saved_place_confirmation_2)

        builder
            .setMessage(message)
            .setNegativeButton(
                R.string.cancel_uppercase
            ) { dialog, _ ->
                Analytics.logClickEvent(Event.DELETE_SAVE_CANCEL, mapProfileOptions.screenName)
                Analytics.logClosePopupEvent(
                    Event.POPUP_UNSAVE_COLLECTION,
                    mapProfileOptions.screenName
                )
                dialog.dismiss()
            }
            .setPositiveButton(
                R.string.confirm
            ) { dialog, _ ->
                amenitiesID?.let {
                    Analytics.logClickEvent(Event.DELETE_SAVE_CONFIRM, mapProfileOptions.screenName)
                    Analytics.logClosePopupEvent(
                        Event.POPUP_UNSAVE_COLLECTION,
                        mapProfileOptions.screenName
                    )
                    deselectingBookmarkTooltip(amenitiesID)
                }
                dialog.dismiss()

            }
        val alertDialog: AlertDialog = builder.create()
        alertDialog.show()
    }

    /**
     * call api to deselecting bookmark tooltip
     */
    private fun deselectingBookmarkTooltip(amenitiesID: String) {
        App.instance?.let { app ->
            app.apiHelper.deselectingBookmarkTooltip(amenitiesID)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object :
                    ApiObserver<DeleteAllBookMarkItemResponse>(compositeDisposable) {
                    override fun onSuccess(data: DeleteAllBookMarkItemResponse) {
                        refreshBookmarkIconTooltip(false)
                    }

                    override fun onError(e: ErrorData) {
                        Timber.i("AAA ${e}")
                    }
                })
        }
    }

    private fun bookmarkAmenityRN(amenity: BaseAmenity) {
        App.instance?.getShareBusinessLogicPackage()?.getRequestsModule()?.flatMap {
            val bundleData = bundleOf(
                "lat" to amenity.lat,
                "long" to amenity.long,
                "name" to amenity.name,
                "address1" to (if (amenity.amenityType == CARPARK) amenity.name else amenity.address),
                "address2" to "",
                "amenityType" to amenity.amenityType,
                "amenityId" to amenity.id.orEmpty(),
                "is_selected" to (amenity.isBookmarked),
                "isDestination" to (mapProfileOptions.screenName == Screen.HOME && DashboardMapStateManager.isInSearchLocationMode && amenity.destinationCarPark == true),
                "showAvailabilityFB" to amenity.showAvailabilityFB,
            )
            Timber.i("call RN event [UPDATE_SAVED_LOCATION] $bundleData")
            it.callRN(ShareBusinessLogicEvent.UPDATE_SAVED_LOCATION.eventName, Arguments.fromBundle(bundleData))
        }?.subscribeOn(Schedulers.io())?.observeOn(AndroidSchedulers.mainThread())?.subscribe()
    }

    override fun hideAllCarPark(amenityItems: List<BaseAmenity>, showDestination: Boolean) {
        clearALlCarPark()
    }

    override fun hideAmenities() {
        clusteredAmenitiesMarkerViewManager.removeAllMarkersExcludingType(CARPARK)
    }

    override fun showAllCarPark(
        mListCarPark: List<BaseAmenity>?,
        isMoveCameraWrapperAllCarpark: Boolean,
        showDestination: Boolean,
        isMarkerHidden: Boolean,
        selectedProfile: String?
    ) {
        handleCarPark(
            mListCarPark ?: listOf(),
            isMoveCameraWrapperAllCarpark,
            showDestination,
            isMarkerHidden,
            selectedProfile
        )
        mapProfileOptions.carParkDestinationIconManager?.handleDestinationCarParkIcon(
            mListCarPark ?: listOf(),
            showDestination
        )
    }

    override fun hideAllAmenitiesAndCarParks() {
        clusteredAmenitiesMarkerViewManager.removeAllMarkers()
        mapProfileOptions.carParkDestinationIconManager?.showDestinationIcon()
    }

    override fun showAllAmenities(
        mListCarPark: List<BaseAmenity>?,
        listAmenities: HashMap<String, List<BaseAmenity>>,
        showDestination: Boolean,
        selectedProfile: String?,
        shouldShowAmenity: (AmenitiesPreference) -> Boolean,
    ) {
        listAmenities?.let {
            handleAmenitiesData(it, showDestination, selectedProfile, shouldShowAmenity)
        }
        mapProfileOptions.carParkDestinationIconManager?.handleDestinationCarParkIcon(
            mListCarPark ?: listOf(),
            showDestination
        )
    }

    override fun isAmenitiesEmpty(): Boolean {
        return !(clusteredAmenitiesMarkerViewManager.hasFeatureLayerItemsExceptType(CARPARK))
    }

    override fun isCarParkEmpty(): Boolean {
        return !(clusteredAmenitiesMarkerViewManager.hasFeatureLayerItems(CARPARK))
    }

    override fun destroy() {
        clusteredAmenitiesMarkerViewManager.onDestroy()
        compositeDisposable.clear()
        isDestroyed = true
    }

    private fun getSelectedSubItems(
        amenityPref: AmenitiesPreference,
        it: List<BaseAmenity>
    ): ArrayList<BaseAmenity> {
        val listEnabled = ArrayList<BaseAmenity>(it)
        amenityPref.subItems!!.forEach { subItem ->
            if (subItem.isSelected != true) {
                listEnabled.removeAll(it.filter { amenity ->
                    if (!amenity.provider.isNullOrBlank() && subItem.elementName?.lowercase() == amenity.provider!!.lowercase()) {
                        return@filter true
                    } else if (!amenity.name.isNullOrBlank() && subItem.elementName?.lowercase() == amenity.name!!.lowercase()) {
                        return@filter true
                    }
                    return@filter false
                })
            }
        }
        if (listEnabled.isEmpty()) {
            listEnabled.addAll(it)
        }
        return listEnabled
    }

    override fun refreshBookmarkIconTooltip(saved: Boolean, bookmarkId: Int) {
        if (markerTooltipView != null) {
            markerTooltipView?.updateBookMarkIconState(saved, bookmarkId)
        }
    }
}
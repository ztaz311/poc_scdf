package com.ncs.breeze.ui.dashboard.fragments.traffic

import android.app.Application
import android.location.Location
import androidx.lifecycle.viewModelScope
import com.google.gson.JsonElement
import com.ncs.breeze.car.breeze.utils.BreezeCarUtil.apiHelper
import com.ncs.breeze.common.ServiceLocator
import com.breeze.model.api.ErrorData
import com.breeze.model.api.request.AmenitiesRequest
import com.breeze.model.api.request.CopilotAPIRequest
import com.breeze.model.api.response.EasyBreeziesResponse
import com.breeze.model.api.response.amenities.BaseAmenity
import com.breeze.model.constants.AmenityType.CARPARK
import com.breeze.model.api.response.amenities.ResponseAmenities
import com.ncs.breeze.common.remote.ApiHelper
import com.ncs.breeze.common.remote.ApiObserver
import com.breeze.model.constants.Constants
import com.ncs.breeze.common.utils.Utils
import com.ncs.breeze.components.SingleLiveEvent
import com.ncs.breeze.components.navigationlog.HistoryFileUploader
import com.ncs.breeze.ui.base.BaseFragmentViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.launch
import org.json.JSONArray
import org.json.JSONObject
import timber.log.Timber
import javax.inject.Inject

class TrafficFragmentViewModel @Inject constructor(apiHelper: ApiHelper, application: Application) :
    BaseFragmentViewModel(
        application
    ) {

    var easyBreeziesResponse: SingleLiveEvent<EasyBreeziesResponse> = SingleLiveEvent()
    private val context = getApplication<Application>().applicationContext


    var mListCarpark: SingleLiveEvent<ArrayList<BaseAmenity>> = SingleLiveEvent()

    /**
     * get carpark
     */
    fun getCarParkDataShowInMap(currentLocation: Location) {

        val listCarpark: ArrayList<BaseAmenity> = arrayListOf()

        viewModelScope.launch {
            val amenitiesRequest = AmenitiesRequest(
                lat = currentLocation.latitude,
                long = currentLocation.longitude,
            )
            amenitiesRequest.setListType(listOf(CARPARK))
            apiHelper.getAmenities(amenitiesRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : ApiObserver<ResponseAmenities>(compositeDisposable) {
                    override fun onSuccess(data: ResponseAmenities) {
                        data.apply {
                            data.amenities.forEach { amenities ->

                                amenities.data.forEach {
                                    if (amenities.type != null) {
                                        it.amenityType = amenities.type!!
                                        it.iconUrl = amenities.iconUrl

                                        listCarpark.add(it)
                                    }
                                }
                            }
                            mListCarpark.value = listCarpark
                        }
                    }

                    override fun onError(e: ErrorData) {
                        /**
                         * must check data null for return error
                         */
                        mListCarpark.value = listCarpark
                    }
                })
        }

    }


    fun retriveEasyBreezies() {
        apiHelper.retrieveEasyBreezies(
            Utils.getAppVersion(getApplication()),
            Utils.getDeviceModel(),
            Constants.DEVICE_PLATFORM,
            Utils.getDeviceOS()
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : ApiObserver<EasyBreeziesResponse>(compositeDisposable) {
                override fun onSuccess(data: EasyBreeziesResponse) {
                    Timber.d("Easy Breezie api success")
                    if (!data.addresses.isNullOrEmpty()) {

                        easyBreeziesResponse.value = data
                    }
                }

                override fun onError(e: ErrorData) {
                    Timber.i("AAA ${e.message} ${e.throwable}")
                    Timber.d("Easy Breezie api error")
                }
            })
    }

    fun sendSecondaryFirebaseID(username: String?) {
        if (HistoryFileUploader.ENABLE_COPILOT) {
            viewModelScope.launch {
                apiHelper.sendSecondaryFirebaseUserID(
                    CopilotAPIRequest(
                        ServiceLocator.getSecondaryFirebaseAuthUserId(),
                        username
                    )
                )
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(object : ApiObserver<JsonElement>(compositeDisposable) {
                        override fun onSuccess(data: JsonElement) {
                            Timber.d("Copilot API is successful: " + data.toString())
                        }

                        override fun onError(e: ErrorData) {
                            Timber.d("Copilot API failed: " + e.toString())
                        }
                    })
            }
        }
    }

    operator fun JSONArray.iterator(): Iterator<JSONObject> =
        (0 until length()).asSequence().map { get(it) as JSONObject }.iterator()

}
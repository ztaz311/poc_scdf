package com.ncs.breeze.ui.dashboard.fragments.adapter


import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.breeze.model.constants.AmenityType.EVCHARGER
import com.ncs.breeze.common.utils.BreezeMapDataHolder
import com.bumptech.glide.Glide
import com.ncs.breeze.R


class AmenityItemToolTipAdapter(val context: Context, var dataList: List<String>) :
    RecyclerView.Adapter<AmenityItemToolTipAdapter.ViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder {

        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.item_amenity_tooltip, viewGroup, false)
        return ViewHolder(view)

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.textView.text = dataList[position]
        val subItem =
            BreezeMapDataHolder.amenitiesPreferenceHashmap[EVCHARGER]?.subItems?.find {
                if (it.displayText == dataList[position]) {
                    return@find true
                }
                return@find false
            }
        subItem?.let {
            Glide.with(context).load(it.buttonActiveImageUrl).into(holder.imgView)
        }
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val textView: TextView
        val imgView: ImageView

        init {
            textView = view.findViewById(R.id.txt_item_amenity)
            imgView = view.findViewById(R.id.img_item_amenity)
        }
    }
}
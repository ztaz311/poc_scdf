package com.ncs.breeze.ui.dashboard.fragments.exploremap

import android.location.Location
import com.ncs.breeze.common.utils.BreezeMapZoomUtil
import com.breeze.model.constants.Constants
import com.mapbox.geojson.Point
import com.mapbox.maps.EdgeInsets
import com.mapbox.maps.MapboxMap
import com.mapbox.turf.TurfConstants
import com.mapbox.turf.TurfMeasurement
import com.breeze.model.extensions.dpToPx
import com.ncs.breeze.components.map.profile.BaseMapCamera

class ExploreMapCamera(val mapboxMap: MapboxMap) : BaseMapCamera() {


    // for selected content map layer type
    private var contentLayerType: String? = null
    private var furthestContentPoint: Point? = null
    private var contentZoneRadius: Double? = null

    init {
        maxEdgeInsets =
            EdgeInsets(
                10.0.dpToPx(),
                20.0.dpToPx(),
                300.0.dpToPx(),
                20.0.dpToPx()
            )
        overviewEdgeInsets =
            maxEdgeInsets
        edgeCircleInsets =
            EdgeInsets(
                25.0.dpToPx(),
                20.0.dpToPx(),
                300.0.dpToPx(),
                20.0.dpToPx()
            )
    }

    private fun determineZoomRadius(userLocation: Point): Double? {
        furthestContentPoint?.let {
            return@determineZoomRadius when (contentLayerType) {
                Constants.CONTENT_LAYER.CONTENT_TYPE_ZONE -> {
                    TurfMeasurement.distance(
                        userLocation,
                        it,
                        TurfConstants.UNIT_METERS
                    ) + (contentZoneRadius ?: 0.0)
                }
                Constants.CONTENT_LAYER.CONTENT_TYPE_GLOBAL_POI -> {
                    TurfMeasurement.distance(userLocation, it, TurfConstants.UNIT_METERS)
                }
                else -> {
                    null
                }
            }
        }
        return null
    }

    internal fun recenterMap(getCurrentLocation: () -> Location?, animationEndCB: () -> Unit) {
        val location = getCurrentLocation()
        if (location != null) {
            var radius =
                determineZoomRadius(Point.fromLngLat(location.longitude, location.latitude))
            radius?.let {
                if (it < Constants.POI_TOOLTIP_ZOOM_RADIUS) {
                    radius = Constants.POI_TOOLTIP_ZOOM_RADIUS
                }
            }
            BreezeMapZoomUtil.recenterMapToLocation(
                mapboxMap,
                lat = location.latitude,
                lng = location.longitude,
                maxEdgeInsets,
                zoomRadius = radius,
                animationEndCB = animationEndCB
            )
        }
    }


    fun setContentDetails(
        contentLayerType: String,
        furthestContentPoint: Point?,
        contentZoneRadius: Double?
    ) {
        this.contentLayerType = contentLayerType
        this.furthestContentPoint = furthestContentPoint
        this.contentZoneRadius = contentZoneRadius
    }
}
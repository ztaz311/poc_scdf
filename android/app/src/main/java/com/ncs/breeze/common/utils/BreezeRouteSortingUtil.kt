package com.ncs.breeze.common.utils

import android.text.TextUtils
import com.breeze.model.enums.RoutePreference
import com.ncs.breeze.common.model.RouteAdapterObjects
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class BreezeRouteSortingUtil @Inject constructor(userPreferenceUtil: BreezeUserPreferenceUtil) {

    var userPreferenceUtil = userPreferenceUtil

    fun getSortedRoutes(routeObject: List<RouteAdapterObjects>)
            : List<RouteAdapterObjects> {
        var userRoutePreference = userPreferenceUtil.getRoutePreference()

        if (TextUtils.equals(userRoutePreference, RoutePreference.FASTEST_ROUTE.type)) {
            return sortBasedOnFastestRoute(routeObject)
        } else {
            return sortBasedOnCheapestRoute(routeObject)
        }
    }

    // Fastest First followed by Cheapest Route
    fun sortBasedOnFastestRoute(routeObject: List<RouteAdapterObjects>):
            List<RouteAdapterObjects> {
        return routeObject.sortedWith(compareBy({ it.duration }, { it.erpRate }, { it.distance }))
    }

    // Sorting based on shortest route
    fun sortBasedOnShortestRoute(routeObject: List<RouteAdapterObjects>):
            List<RouteAdapterObjects> {
        return routeObject.sortedWith(compareBy { it.distance })
    }

    // Cheapest First followed by Fastest Route
    fun sortBasedOnCheapestRoute(routeObject: List<RouteAdapterObjects>):
            List<RouteAdapterObjects> {
        return routeObject.sortedWith(compareBy({ it.erpRate }, { it.duration }))
    }
}
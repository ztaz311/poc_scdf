package com.ncs.breeze.common.model

import com.breeze.model.ERPChargeInfo
import com.breeze.model.ERPFullRouteData
import com.mapbox.api.directions.v5.models.DirectionsResponse
import com.mapbox.api.directions.v5.models.DirectionsRoute
import com.mapbox.geojson.FeatureCollection
import com.breeze.model.enums.RoutePreference
import java.io.Serializable

data class RouteAdapterObjects(
    override var route: DirectionsRoute,
    override var arrivalTime: String,
    override var duration: Int,
    override var distance: Float,
    override var erpRate: Float?,
    var erpFeatureCollection: FeatureCollection?,
    override var erpList: List<ERPChargeInfo>?,
    override var originalResponse: DirectionsResponse,
) : Serializable, SimpleRouteAdapterObjects(
    route,
    arrivalTime,
    duration,
    distance,
    erpRate,
    erpList,
    originalResponse
)

open class SimpleRouteAdapterObjects(
    open var route: DirectionsRoute,
    open var arrivalTime: String,
    open var duration: Int,
    open var distance: Float,
    open var erpRate: Float?,
    open var erpList: List<ERPChargeInfo>?,
    open var originalResponse: DirectionsResponse,
) : Serializable {}


data class RouteAdapterTypeObjects(
    val type: RoutePreference,
    val routeData: SimpleRouteAdapterObjects
)

data class RouteDepartureDetails(val timestamp: Long, val type: DepartureType)

data class ERPFullRouteDataMap(
    val erpFullRouteData: ERPFullRouteData,
    val routeList: List<RouteAdapterObjects>
)

enum class DepartureType {
    DEPARTURE, ARRIVAL
}

fun SimpleRouteAdapterObjects.toRouteAdapterObjects(): RouteAdapterObjects {
    return RouteAdapterObjects(
        route,
        arrivalTime,
        duration,
        distance,
        erpRate,
        null,
        erpList,
        originalResponse
    )
}
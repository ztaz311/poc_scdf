package com.ncs.breeze.components.marker.markerview2

import android.app.Activity
import android.content.Context
import android.content.res.Resources
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import com.ncs.breeze.common.utils.BreezeMapZoomUtil
import com.breeze.model.constants.Constants
import com.bumptech.glide.Glide
import com.google.android.gms.maps.model.LatLng
import com.mapbox.geojson.Point
import com.mapbox.maps.EdgeInsets
import com.mapbox.maps.MapboxMap
import com.ncs.breeze.R

class NotificationMarkerView constructor(
    latLng: LatLng,
    mapboxMap: MapboxMap,
    val activity: Activity,
    itemId: String? = null
) : MarkerView2(mapboxMap = mapboxMap, itemId) {

    private var annotationView: View? = null
    private var imgIcon: ImageView? = null
    private val pixelDensity = Resources.getSystem().displayMetrics.density

    init {
        mLatLng = latLng
        val inflater = activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        annotationView = inflater.inflate(R.layout.notification_marker_view, null)
        annotationView?.apply {
            imgIcon = findViewById(R.id.imgIcon)
        }
        mViewMarker = annotationView
    }

    fun renderMarkerToolTip(
        iconResId: Int = -1,
        resIconUrl: String? = null
    ) {
        if (resIconUrl != null && resIconUrl.trim().length > 0) {
            Glide.with(activity)
                .load(resIconUrl)
                .placeholder(R.drawable.ic_breeze_round)
                .error(R.drawable.ic_breeze_round)
                .into(imgIcon!!)
        } else if (iconResId != -1) {
            imgIcon!!.setImageResource(iconResId)
        } else {
            imgIcon!!.setImageResource((R.drawable.ic_breeze_round))
        }
        BreezeMapZoomUtil.zoomViewToRange(
            mapboxMap = mapboxMap,
            point = Point.fromLngLat(mLatLng!!.longitude, mLatLng!!.latitude),
            radius = Constants.DEFAULT_TOOLTIP_ZOOM_RADIUS,
            padding = EdgeInsets(
                10.0 * pixelDensity,
                10.0 * pixelDensity,
                200.0 * pixelDensity,
                10.0 * pixelDensity
            ),
            animationEndCB = {
                annotationView?.bringToFront()
            }
        )
    }

    override fun showMarker() {
        super.showMarker()
        annotationView?.visibility = View.VISIBLE
    }

    override fun hideMarker() {
        super.hideMarker()
        annotationView?.visibility = View.INVISIBLE
    }

    override fun updateTooltipPosition() {
        mLatLng?.let { location ->
            val screenCoordinate = mapboxMap.pixelForCoordinate(
                Point.fromLngLat(
                    location.longitude,
                    location.latitude
                )
            )
            mViewMarker?.let {
                it.measure(
                    View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                    View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
                );
                it.x = screenCoordinate.x.toFloat() - (it.measuredWidth / 2)
                it.y = screenCoordinate.y.toFloat() - it.measuredHeight
            }
        }
    }
}
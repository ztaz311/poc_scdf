package com.ncs.breeze.components.marker.markerview2

import androidx.annotation.UiThread
import com.mapbox.maps.MapView


open class MarkerViewManager2
/**
 * Create a MarkerViewManager.
 *
 * @param mapView   the MapView used to synchronise views on
 * @param mapboxMap the MapboxMap to synchronise views with
 */(private val mapView: MapView) {
    protected val markers: MutableList<MarkerView2> = ArrayList()
    protected var initialised = false

    init {
        mapView.getMapboxMap().addOnCameraChangeListener { update() }
    }

    /**
     * Destroys the MarkerViewManager.
     *
     *
     * Should be called before MapView#onDestroy
     *
     */
    @UiThread
    fun onDestroy() {
        markers.clear()

        initialised = false
    }

    /**
     * Add a MarkerView to the map using MarkerView and LatLng.
     *
     * @param markerView the markerView to synchronise on the map
     */
    @UiThread
    fun addMarker(markerView: MarkerView2) {
        if (markers.contains(markerView)) {
            return
        }
        if (!initialised) {
            initialised = true

        }

        removeMarker(markerView)
        mapView.addView(markerView.mViewMarker)
        markers.add(markerView)
        markerView.updateTooltipPosition()
    }

    /**
     * Remove an existing markerView from the map.
     *
     * @param markerView the markerView to be removed from the map
     */
    @UiThread
    fun removeMarker(markerView: MarkerView2) {
        if (!markers.contains(markerView)) {
            return
        }
        mapView.removeView(markerView.mViewMarker)
        markers.remove(markerView)
        markerView.updateTooltipPosition()
    }

    @UiThread
    fun findAndRemoveMarkerByItemId(itemId: String) {
        var existingMarker: MarkerView2? = null
        for (item: MarkerView2 in markers) {
            if (item.itemId == itemId) {
                existingMarker = item
                break
            }
        }
        existingMarker?.let {
            mapView.removeView(it.mViewMarker)
            markers.remove(it)
        }
    }


    open fun hideAllMarker() {}


    open fun showAllMarker() {}


    private fun update() {
        for (marker in markers) {
            marker.updateTooltipPosition()
        }
    }

}
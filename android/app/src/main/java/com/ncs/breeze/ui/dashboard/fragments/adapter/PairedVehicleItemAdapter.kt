package com.ncs.breeze.ui.dashboard.fragments.adapter


import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.ncs.breeze.R
import com.breeze.model.api.response.ObuVehicleDetails


class PairedVehicleItemAdapter(
    val context: Context,
    var dataList: List<ObuVehicleDetails>,
    var selectedIndex: Int? = null,
    val listener: PairedItemListener
) : RecyclerView.Adapter<PairedVehicleItemAdapter.ViewHolder>() {

    val scale = context.resources.displayMetrics.density


    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder {

        val view =
            LayoutInflater.from(viewGroup.context).inflate(R.layout.item_paired_vehicle, viewGroup, false)
        return ViewHolder(view)

    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val vehicleIndex = position+1
        holder.tvVehicleIndex.text = "Vehicle #${vehicleIndex}"
        holder.tvVehicleNumber.text = dataList[position].vehicleNumber
        val drawableResource = ObuVehicleTypes.getVehicleByType(dataList[position].vehicleType)?.resourceId
            ?: R.drawable.obu_light_vehicle
        holder.ivVehicleType.setImageDrawable(context.resources.getDrawable(drawableResource,null))

        if(selectedIndex!=null && selectedIndex == position){
            holder.tvVehicleNumber.setCompoundDrawablesWithIntrinsicBounds(null,null,context.resources.getDrawable(R.drawable.obu_paired_tick,null),null)
            holder.tvPairedText.text = context.resources.getString(R.string.pair_disconnect)
            holder.tvPairedText.setTextColor(context.resources.getColor(R.color.red_dark,null))
            holder.tvPairedText.background = context.resources.getDrawable(R.drawable.themed_red_border_bg_selector,null)
        }else{
            holder.tvPairedText.text = context.resources.getString(R.string.pair_connect)
            holder.tvPairedText.setTextColor(context.resources.getColor(R.color.themed_breeze_primary,null))
            holder.tvPairedText.background = context.resources.getDrawable(R.drawable.themed_purple_border_bg_selector,null)
        }
        holder.tvPairedText.setOnClickListener {
            listener.onItemClick(dataList[position])
        }
        holder.ivVehicleDelete.setOnClickListener {
            listener.onItemDelete(dataList[position])
        }

    }


    override fun getItemCount(): Int {
        return dataList.size
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tvVehicleIndex: TextView
        val tvVehicleNumber: TextView

        val ivVehicleType: ImageView
        val tvPairedText: TextView

        val ivVehicleDelete: ImageView

        init {
            tvVehicleIndex = view.findViewById(R.id.tv_vehicle_title)
            tvVehicleNumber = view.findViewById(R.id.tv_vehicle_name)
            tvPairedText = view.findViewById(R.id.tv_obu_pair)
            ivVehicleType = view.findViewById(R.id.img_vehicle)
            ivVehicleDelete = view.findViewById(R.id.img_obu_delete)
        }
    }

    interface PairedItemListener {
        fun onItemClick(data: ObuVehicleDetails)
        fun onItemDelete(data: ObuVehicleDetails)
    }
}

enum class ObuVehicleTypes(val type: String, val resourceId: Int) {
    CAR("CAR", R.drawable.obu_light_vehicle),
    MOTORCYCLE("MOTORCYCLE", R.drawable.obu_motorcycle),
    HEAVY_VEHICLE("HEAVY_VEHICLE", R.drawable.obu_heavy_vehicle_alt);

    companion object {
        private val OBU_VEHICLE_MAP: Map<String, ObuVehicleTypes> =
            ObuVehicleTypes.values().associate {
                return@associate Pair(it.type, it)
            }

        fun getVehicleByType(requiredType: String): ObuVehicleTypes? {
            return OBU_VEHICLE_MAP[requiredType]
        }

    }
}
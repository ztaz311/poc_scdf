package com.ncs.breeze.common.utils

object TimeConst {
    val yyyyMMdd = "yyyy-MM-dd"
    val dMMMyy = "d MMM yy"
}
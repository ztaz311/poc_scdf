package com.ncs.breeze.ui.base

import android.graphics.Rect
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.Event
import com.breeze.model.constants.Constants
import com.ncs.breeze.di.Injectable
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber

abstract class BaseFragment<T : ViewBinding, V : BaseFragmentViewModel> : Fragment(),
    Injectable,
    BaseActivity.DeviceBackPressedListener {

    protected lateinit var viewBinding: T
    private var mViewModel: V? = null
    var isKeyboardOpened = false
    lateinit var globalLayoutListener: ViewTreeObserver.OnGlobalLayoutListener
    private var isDestroyed = false

    protected var isFinishing: Boolean = false
    protected var isPaused: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Analytics.logScreenView(getScreenName())
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        viewBinding = inflateViewBinding(inflater, container, false)
        setListenerToRootView()
        return viewBinding.root
    }

    /**
     * Inflate ViewBinding for this fragment
     * */
    abstract fun inflateViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?,
        attachToContainer: Boolean
    ): T

    /**
     * Override for set view model
     *
     * @return view model instance
     */
    abstract fun getViewModelReference(): V

    open fun onBackPressed() {
        if (isPaused) {
            //When application is minimised, we can not pop the fragment, otherwise
            //it will throw "IllegalStateException - Can not perform this action after onSaveInstanceState"
            isFinishing = true
            return
        }
        try {
            Analytics.logClickEvent(Event.OBU_PAIRING_BACK, getScreenName())
            requireActivity().supportFragmentManager.popBackStackImmediate()
            //Runtime.getRuntime().gc()
        } catch (e: Exception) {
            Timber.e("On back pressed error $e ")
        }
    }

    override fun onPause() {
        isPaused = true
        super.onPause()
    }

    override fun onResume() {
        super.onResume()
        isPaused = false
        if (isFinishing) {
            isFinishing = false
            CoroutineScope(Dispatchers.IO).launch {
                delay(500)
                withContext(Dispatchers.Main) {
                    onBackPressed()
                }
            }
        }
    }

    fun setListenerToRootView() {
        val activityRootView =
            requireActivity().window.decorView.findViewById<View>(android.R.id.content)

        globalLayoutListener = ViewTreeObserver.OnGlobalLayoutListener {
            val r = Rect()
            activityRootView.getWindowVisibleDisplayFrame(r)
            val screenHeight = activityRootView.rootView.height

            // r.bottom is the position above soft keypad or device button.
            // if keypad is shown, the r.bottom is smaller than that before.
            val keypadHeight = screenHeight - r.bottom
            if (keypadHeight > screenHeight * 0.15) { // 0.15 ratio is perhaps enough to determine keypad height.
                // keyboard is opened
                if (!isKeyboardOpened) {
                    isKeyboardOpened = true
                    onKeyboardVisibilityChanged(true)
                }
            } else {
                // keyboard is closed
                if (isKeyboardOpened) {
                    isKeyboardOpened = false
                    onKeyboardVisibilityChanged(false)
                }
            }
        }

        activityRootView.viewTreeObserver.addOnGlobalLayoutListener(globalLayoutListener)


    }

    protected fun isFragmentDestroyed(): Boolean {
        return (isDestroyed || this.activity == null || this.view == null)
    }

    override fun onDestroyView() {
        isDestroyed = true
        super.onDestroyView()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (::globalLayoutListener.isInitialized) {
            val activityRootView =
                requireActivity().window.decorView.findViewById<View>(android.R.id.content)
            activityRootView.viewTreeObserver.removeOnGlobalLayoutListener(globalLayoutListener)
        }

    }

    open fun onKeyboardVisibilityChanged(isOpen: Boolean) {
        // To be overridden in required fragments
    }

    abstract fun getScreenName(): String

    override fun onDeviceBackPressed() {
        Analytics.logClickEvent(Event.BACK, getScreenName())
    }

    protected fun isValidUserName(userName: String): Boolean {
        return userName.matches(Regex(Constants.REGEX.NAME_REGEX))
    }

}
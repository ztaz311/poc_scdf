package com.ncs.breeze.components.marker.markerview2

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import androidx.annotation.UiThread
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.lifecycle.Lifecycle
import com.breeze.model.DestinationAddressDetails
import com.breeze.model.SearchLocation
import com.breeze.model.constants.Constants
import com.breeze.model.enums.AmenityBand
import com.breeze.model.extensions.dpToPx
import com.facebook.react.bridge.Arguments
import com.google.android.gms.maps.model.LatLng
import com.mapbox.geojson.Point
import com.mapbox.maps.MapView
import com.ncs.breeze.R
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.extensions.android.getApp
import com.ncs.breeze.common.extensions.android.getShareBusinessLogicPackage
import com.ncs.breeze.common.rxbus.RxBus
import com.ncs.breeze.common.rxbus.RxEvent
import com.ncs.breeze.common.utils.LocationBreezeManager
import com.ncs.breeze.databinding.MapTooltipSeachLocationPlaceSaveBinding
import com.ncs.breeze.reactnative.nativemodule.ShareBusinessLogicEvent
import com.ncs.breeze.ui.dashboard.activity.DashboardActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class SearchLocationPlaceSaveTooltip constructor(
    mapView: MapView,
    itemId: String? = null,
) : MarkerView2(mapboxMap = mapView.getMapboxMap(), itemId = itemId) {

    var analyticsScreenName: String = ""
    private var viewBinding: MapTooltipSeachLocationPlaceSaveBinding

    init {
        viewBinding =
            MapTooltipSeachLocationPlaceSaveBinding.inflate(
                LayoutInflater.from(mapView.context), null, false
            )
        mViewMarker = viewBinding.root
    }

    override fun updateTooltipPosition() {
        val latLng = mLatLng ?: return
        val viewMarker = mViewMarker ?: return
        val screenCoordinate = mapboxMap.pixelForCoordinate(
            Point.fromLngLat(latLng.longitude, latLng.latitude)
        )
        viewMarker.measure(
            View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
            View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
        )
        viewMarker.x = screenCoordinate.x.toFloat() - (viewMarker.measuredWidth / 2)
        viewMarker.y = screenCoordinate.y.toFloat() - viewMarker.measuredHeight - 36.dpToPx()
    }


    fun showTooltip(searchLocation: SearchLocation) {
        val latitude = searchLocation.lat?.toDoubleOrNull() ?: return
        val longitude = searchLocation.long?.toDoubleOrNull() ?: return
        mLatLng = LatLng(latitude, longitude)
        displayCrowdsourceInfo(searchLocation)
        showMarker()
        updateTooltipPosition()
        viewBinding.tvName.text = searchLocation.name?.takeIf { it.isNotEmpty() } ?: searchLocation.address1
        viewBinding.tvDescription.text =
            searchLocation.fullAddress?.takeIf { it.isNotEmpty() } ?: searchLocation.address2
        viewBinding.buttonNavigateHere.setOnClickListener {
            Analytics.logClickEvent(
                "[map]:tooltip_saved_navigate_here",
                analyticsScreenName
            )
            onNavigateHereClicked(it, searchLocation)
        }
        viewBinding.buttonShare.setOnClickListener {
            Analytics.logClickEvent(
                "[map]:tooltip_saved_share",
                analyticsScreenName
            )
            shareLocation(searchLocation)
        }
        viewBinding.buttonInvite.setOnClickListener {
            Analytics.logClickEvent(
                "[map]:tooltip_saved_invite",
                analyticsScreenName
            )
            RxBus.publish(RxEvent.CollectionDetailsClearSearch())
            inviteLocation(searchLocation)
        }
        viewBinding.imgFlagSaved.setOnClickListener {
            Analytics.logClickEvent(
                "[map]:tooltip_saved_bookmark",
                analyticsScreenName
            )
            bookmarkLocation(searchLocation)
        }
    }

    private fun displayCrowdsourceInfo(searchLocation: SearchLocation) {
        val availabilityCSData = searchLocation.availabilityCSData

        viewBinding.layoutCarparkStatus.root.isVisible = availabilityCSData != null
        if (availabilityCSData == null) return
        val title: String = availabilityCSData.title ?: ""
//            Calculate last update time
        val timeUpdate =
            "${availabilityCSData.primaryDesc ?: viewBinding.root.context.getString(com.breeze.customization.R.string.update_carpark_stt_time)} ${availabilityCSData.generateDisplayedLastUpdate()}"
        val drawableHeaderCpStatus =
            AppCompatResources.getDrawable(viewBinding.root.context, R.drawable.bg_header_carpark_status)
        drawableHeaderCpStatus?.setTint(Color.parseColor(availabilityCSData.themeColor ?: "#F26415"))
        viewBinding.layoutCarparkStatus.root.background = drawableHeaderCpStatus
        viewBinding.layoutCarparkStatus.icCarparkStatus.setImageResource(
            when (searchLocation.availablePercentImageBand) {
                AmenityBand.TYPE0.type -> R.drawable.ic_cp_stt_full
                AmenityBand.TYPE2.type -> R.drawable.ic_cp_stt_available
                else -> R.drawable.ic_cp_stt_few
            }
        )
        viewBinding.layoutCarparkStatus.tvStatusCarpark.text = title
        viewBinding.layoutCarparkStatus.tvTimeUpdateCarparkStatus.text = timeUpdate
    }

    private fun bookmarkLocation(searchLocation: SearchLocation) {
        viewBinding.root.context.getApp()?.shareBusinessLogicPackage?.getRequestsModule()
            ?.flatMap {
                val data = Arguments.fromBundle(
                    bundleOf(
                        "action" to "ADD",
                        "bookmark" to bundleOf(
                            "lat" to searchLocation.lat,
                            "long" to searchLocation.long,
                            "address1" to searchLocation.address1,
                            "name" to searchLocation.name,
                            "address2" to searchLocation.address2,
                            "placeId" to searchLocation.placeId,
                        )
                    )
                )
                it.callRN(ShareBusinessLogicEvent.TOGGLE_SAVE_LOCATION_COLLECTION_DETAILS.name, data)
            }?.subscribeOn(Schedulers.io())?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe()
    }

    private fun createShareLocationData(item: SearchLocation) = Arguments.fromBundle(
        bundleOf(
            "address1" to item.address1,
            "address2" to item.address2,
            "latitude" to item.lat,
            "longitude" to item.long,
            "name" to (item.name?.takeIf { it.isNotEmpty() } ?: item.address1),
            "fullAddress" to item.fullAddress,
            "placeId" to item.placeId,
        )
    )

    private fun shareLocation(item: SearchLocation) {
        viewBinding.root.context.getShareBusinessLogicPackage()?.getRequestsModule()?.flatMap {
            it.callRN(
                ShareBusinessLogicEvent.SHARE_LOCATION_FROM_NATIVE.eventName,
                createShareLocationData(item)
            )
        }?.subscribeOn(Schedulers.io())?.observeOn(AndroidSchedulers.mainThread())?.subscribe()
    }

    private fun inviteLocation(item: SearchLocation) {
        viewBinding.root.context.getShareBusinessLogicPackage()?.getRequestsModule()?.flatMap {
            it.callRN(
                ShareBusinessLogicEvent.LOCATION_INVITE_FROM_NATIVE.eventName,
                createShareLocationData(item)
            )
        }?.subscribeOn(Schedulers.io())?.observeOn(AndroidSchedulers.mainThread())?.subscribe()
    }

    private fun onNavigateHereClicked(view: View, searchLocation: SearchLocation) {
        (view.context as? DashboardActivity)?.takeIf {
            it.lifecycle.currentState.isAtLeast(
                Lifecycle.State.STARTED
            )
        }?.let { dashboardActivity ->
            val currentLocation = LocationBreezeManager.getInstance().currentLocation
            val sourceAddressDetails = DestinationAddressDetails(
                null,
                address1 = null,
                null,
                currentLocation?.latitude?.toString(),
                currentLocation?.longitude?.toString(),
                null,
                Constants.TAGS.CURRENT_LOCATION_TAG,
                null,
                -1
            )
            dashboardActivity.showRouteAndResetToCenter(
                bundleOf(
                    Constants.DESTINATION_ADDRESS_DETAILS to searchLocation.convertToDestinationAddressDetails(),
                    Constants.SOURCE_ADDRESS_DETAILS to sourceAddressDetails
                )
            )
        }
    }


    @UiThread
    fun hideTooltip() {
        hideMarker()
    }
}
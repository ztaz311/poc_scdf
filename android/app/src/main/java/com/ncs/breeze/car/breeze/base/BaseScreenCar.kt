package com.ncs.breeze.car.breeze.base

import androidx.annotation.CallSuper
import androidx.car.app.CarContext
import androidx.car.app.Screen
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.LifecycleOwner
import com.mapbox.androidauto.internal.logAndroidAuto
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.model.rx.ToCarEventData
import com.ncs.breeze.common.model.rx.ToCarRxEvent
import com.ncs.breeze.common.event.ToCarRx
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable


abstract class BaseScreenCar(carContext: CarContext) : Screen(carContext) {

    var compositeDisposable: CompositeDisposable = CompositeDisposable()

    init {
        lifecycle.addObserver(object : DefaultLifecycleObserver {
            override fun onCreate(owner: LifecycleOwner) {
                onCreateScreen()
            }

            override fun onDestroy(owner: LifecycleOwner) {
                onDestroyScreen()
                /**
                 * clear all
                 */
                compositeDisposable.dispose()
            }

            override fun onStart(owner: LifecycleOwner) {
                onStartScreen()
                logAndroidAuto("onStart base screen")
            }

            override fun onResume(owner: LifecycleOwner) {
                /**
                 * register event from car
                 */
                compositeDisposable.add(
                    ToCarRx.listenEvent(ToCarRxEvent::class.java)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe {
                            onReceiverEventFromApp(it)
                        })

                compositeDisposable.add(
                    ToCarRx.listenEventWithLastCache(ToCarRxEvent::class.java)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe {
                            onReceiverEventFromApp(it)
                        })
                onResumeScreen()

            }

            override fun onPause(owner: LifecycleOwner) {
                onPauseScreen()
                compositeDisposable.clear()
            }

            override fun onStop(owner: LifecycleOwner) {
                onStopScreen()
            }
        })


    }

    /**
     * all function just support easy for implement life cycle of screen
     */
    @CallSuper
    open fun onCreateScreen() {
        Analytics.logScreenView(getScreenName())
    }

    open fun onResumeScreen() {}

    open fun onPauseScreen() {}

    open fun onStartScreen() {}

    open fun onStopScreen() {}

    open fun onDestroyScreen() {}

    abstract fun getScreenName(): String

    /**
     *  this function call when new event need for update screen from app
     */
    open fun onScreenUpdateEvent(data: ToCarEventData?) {

    }


    open fun onReceiverEventFromApp(event: ToCarRxEvent?) {

    }

}
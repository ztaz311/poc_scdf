package com.ncs.breeze.common.car

import android.annotation.SuppressLint
import android.content.Context
import com.mapbox.navigation.core.MapboxNavigation
import com.mapbox.navigation.core.directions.session.RoutesObserver
import com.mapbox.navigation.core.directions.session.RoutesUpdatedResult
import com.mapbox.navigation.core.replay.MapboxReplayer

class ReplayRoutesObserver(
    val mapboxReplayer: MapboxReplayer,
    val context: Context,
    val mapboxNavigation: MapboxNavigation
) : RoutesObserver {
    @SuppressLint("MissingPermission")
    override fun onRoutesChanged(result: RoutesUpdatedResult) {
        val routes = result.routes
        if (routes.isEmpty()) {
            mapboxReplayer.clearEvents()
            mapboxNavigation.resetTripSession()
            mapboxReplayer.pushRealLocation(context, 0.0)
            mapboxReplayer.play()
        }
    }
}

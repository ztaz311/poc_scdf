package com.ncs.breeze.common.helper.route

import android.text.format.DateFormat
import android.util.Log
import com.breeze.model.enums.RoutePreference
import com.ncs.breeze.common.model.DepartureType
import com.ncs.breeze.common.model.RouteAdapterObjects
import com.ncs.breeze.common.model.RouteDepartureDetails
import com.ncs.breeze.common.utils.BreezeERPRefreshUtil
import com.ncs.breeze.common.utils.BreezeRouteSortingUtil
import com.breeze.model.constants.Constants
import com.breeze.model.constants.Constants.MAPBOX_ROUTE_OPTION_LOCALE
import com.ncs.breeze.common.utils.Utils
import com.mapbox.api.directions.v5.DirectionsCriteria
import com.mapbox.api.directions.v5.MapboxDirections
import com.mapbox.api.directions.v5.models.DirectionsResponse
import com.mapbox.api.directions.v5.models.DirectionsRoute
import com.mapbox.api.directions.v5.models.RouteOptions
import com.mapbox.geojson.Point
import com.mapbox.navigation.base.extensions.applyDefaultNavigationOptions
import com.mapbox.navigation.base.extensions.coordinates
import com.mapbox.navigation.base.internal.utils.isSameRoute
import com.mapbox.navigation.base.route.NavigationRoute
import com.mapbox.navigation.base.route.RouterOrigin
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.withContext
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber
import java.util.*
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

class RoutePlanningHelper(
    val breezeRouteSortingUtil: BreezeRouteSortingUtil,
    val breezeERPUtil: BreezeERPRefreshUtil,
    private val mapboxToken: String
) {

    suspend fun findRoutes(
        originPoint: Point,
        destination: Point,
        destinationName: String?,
        wayPoints: List<Point>,
        routingAlgorithm: RouteAlgorithm,
        routeDepartureDetails: RouteDepartureDetails? = null,
    ): HashMap<RoutePreference, List<RouteAdapterObjects>> {
        Timber.d("Start api calls")
        //TODO: Error Handling is missing
        return withContext(Dispatchers.IO) {
            val mapOfRouteResponseTypes = hashMapOf<RouteType, List<DirectionsRouteResponse>>()
            val dirTrafficTollsRes = async {
                directionsAPIWithDrivingTrafficAndTolls(
                    originPoint,
                    destination,
                    destinationName,
                    mapOfRouteResponseTypes,
                    wayPoints,
                    routeDepartureDetails
                )
            }
            val dirTrafficNoTollsRes = async {
                directionsAPIWithDrivingTrafficAndNoTolls(
                    originPoint,
                    destination,
                    destinationName,
                    mapOfRouteResponseTypes,
                    wayPoints,
                    routeDepartureDetails
                )
            }

            val successAll =
                awaitAll(dirTrafficTollsRes, dirTrafficNoTollsRes).all { it }

            if (successAll) {
                return@withContext createMapOfDifferentRoutePref(
                    mapOfRouteResponseTypes,
                    routingAlgorithm,
                    false,
                    routeDepartureDetails
                )
            }
            return@withContext hashMapOf<RoutePreference, List<RouteAdapterObjects>>()
        }
    }

    suspend fun findWalkingRoute(
        originPoint: Point?,
        destination: Point?,
        destinationName: String?
    ) = directionsAPIWithWalking(
        originPoint,
        destination,
        destinationName
    )

    suspend fun findERPRoutes(
        originPoint: Point,
        destination: Point,
        wayPoints: List<Point>,
        routeDepartureDetails: RouteDepartureDetails? = null,
    ): List<RouteAdapterObjects> {

        Timber.d("Start api calls")
        //TODO: Error Handling is missing
        return withContext(Dispatchers.IO) {
            val mapOfRouteTypes = hashMapOf<RouteType, List<DirectionsRouteResponse>>()
            val dirTrafficTollsRes = async {
                directionsAPIWithDrivingTrafficAndTolls(
                    originPoint,
                    destination,
                    null,
                    mapOfRouteTypes,
                    wayPoints,
                    routeDepartureDetails
                )
            }
            val success = dirTrafficTollsRes.await()
            if (success) {
                return@withContext fetchAllERPRoutes(
                    mapOfRouteTypes,
                    true,
                    routeDepartureDetails,
                    3
                )
            }
            return@withContext listOf<RouteAdapterObjects>()
        }
    }


    private suspend fun directionsAPIWithDrivingTrafficAndTolls(
        originPoint: Point,
        destination: Point,
        destinationName: String?,
        mapOfRouteTypes: HashMap<RouteType, List<DirectionsRouteResponse>>,
        wayPoints: List<Point>,
        routeDepartureDetails: RouteDepartureDetails?
    ): Boolean {
        val client = getRouteOptions(
            isDrivingTraffic = true,
            isAvoidTolls = false,
            originPoint = originPoint,
            destination = destination,
            destinationName = destinationName,
            wayPoints = wayPoints,
            routeDepartureDetails = routeDepartureDetails
        )

        return suspendCoroutine { cont ->
            client.enqueueCall(object : Callback<DirectionsResponse> {
                override fun onResponse(
                    call: Call<DirectionsResponse>,
                    response: Response<DirectionsResponse>
                ) {
                    if (response.body()?.routes()?.isNotEmpty() == true) {
                        val routeList = response.body()!!.routes()
                        mapOfRouteTypes[RouteType.WITH_DRIVING_TRAFFIC_AND_TOLLS] =
                            routeList.map { DirectionsRouteResponse(response.body()!!, it) }
                        Timber.d("current route list size is: ${routeList.size}")
                    }
                    cont.resume(true)
                }

                override fun onFailure(call: Call<DirectionsResponse>, t: Throwable) {
                    cont.resume(false)
                }
            })
        }
    }

    private suspend fun directionsAPIWithDrivingTrafficAndNoTolls(
        originPoint: Point,
        destination: Point,
        destinationName: String?,
        mapOfRouteTypes: HashMap<RouteType, List<DirectionsRouteResponse>>,
        wayPoints: List<Point>,
        routeDepartureDetails: RouteDepartureDetails?
    ): Boolean {
        val client = getRouteOptions(
            isDrivingTraffic = true,
            isAvoidTolls = true,
            originPoint = originPoint,
            destination = destination,
            destinationName = destinationName,
            wayPoints = wayPoints,
            routeDepartureDetails = routeDepartureDetails
        )
        return suspendCoroutine { cont ->
            client.enqueueCall(object : Callback<DirectionsResponse> {
                override fun onResponse(
                    call: Call<DirectionsResponse>,
                    response: Response<DirectionsResponse>
                ) {
                    if (response.body()?.routes()?.isNotEmpty() == true) {
                        val routeList = response.body()!!.routes()
                        mapOfRouteTypes[RouteType.WITH_DRIVING_TRAFFIC_AND_NO_TOLLS] =
                            routeList.map { DirectionsRouteResponse(response.body()!!, it) }
                        Timber.d("current route list size is: ${routeList.size}")
                    }
                    cont.resume(true)
                }

                override fun onFailure(call: Call<DirectionsResponse>, t: Throwable) {
                    cont.resume(false)
                }
            })
        }
    }

    suspend fun directionsAPIWithProfileDriving(
        originPoint: Point,
        destination: Point,
        destinationName: String?,
        mapOfRouteTypes: HashMap<RouteType, List<DirectionsRouteResponse>>,
        wayPoints: List<Point>,
        routeDepartureDetails: RouteDepartureDetails?
    ): Boolean {
        val client = getRouteOptions(
            isDrivingTraffic = false,
            isAvoidTolls = false,
            originPoint = originPoint,
            destination = destination,
            destinationName = destinationName,
            wayPoints = wayPoints,
            routeDepartureDetails = routeDepartureDetails
        )
        return suspendCoroutine { cont ->
            client.enqueueCall(object : Callback<DirectionsResponse> {
                override fun onResponse(
                    call: Call<DirectionsResponse>,
                    response: Response<DirectionsResponse>
                ) {
                    if (response.body()?.routes()?.isNotEmpty() == true) {
                        val routeList = response.body()!!.routes()
                        mapOfRouteTypes[RouteType.WITH_PROFILE_DRIVING] =
                            routeList.map { DirectionsRouteResponse(response.body()!!, it) }
                        Timber.d("current route list size is: ${routeList.size}")
                    }
                    Timber.d("Last API call")
                    cont.resume(true)
                }

                override fun onFailure(call: Call<DirectionsResponse>, t: Throwable) {
                    cont.resume(false)
                }
            })
        }
    }

    private suspend fun directionsAPIWithWalking(
        originPoint: Point?,
        destination: Point?,
        destinationName: String?,
    ): NavigationRoute? {
        val routeRequestObject = getWalkingRouteOptions(originPoint, destination, destinationName)
        return suspendCoroutine { cont ->
            routeRequestObject.first.enqueueCall(object : Callback<DirectionsResponse> {
                override fun onResponse(
                    call: Call<DirectionsResponse>,
                    response: Response<DirectionsResponse>
                ) {
                    if (response.body() != null) {
                        val walkingRoutes = NavigationRoute.create(
                            response.body()!!,
                            routeRequestObject.second,
                            RouterOrigin.Custom()
                        )
                        cont.resume(if (walkingRoutes.isEmpty()) null else walkingRoutes[0])
                    } else {
                        cont.resume(null)
                    }
                }

                override fun onFailure(call: Call<DirectionsResponse>, t: Throwable) {
                    cont.resume(null)
                }
            })
        }
    }

    private suspend fun createMapOfDifferentRoutePref(
        mapOfRouteTypes: HashMap<RouteType, List<DirectionsRouteResponse>>,
        routingAlgorithm: RouteAlgorithm,
        allowZeroCharge: Boolean,
        routeDepartureDetails: RouteDepartureDetails?
    ): HashMap<RoutePreference, List<RouteAdapterObjects>> {

        Timber.d("Creating map")
        when (routingAlgorithm) {
            RouteAlgorithm.NORMAL -> return fetchDefaultRoutePreference(
                mapOfRouteTypes,
                routeDepartureDetails,
                allowZeroCharge,
                3
            )
            RouteAlgorithm.SINGLE_ROUTE_PER_TYPE -> return fetchSingleTypeRoutePreference(
                mapOfRouteTypes, allowZeroCharge, routeDepartureDetails, 3
            )
            RouteAlgorithm.MERGED_SINGLE_ROUTE_PER_TYPE -> return fetchMergedTypeRoutePreference(
                mapOfRouteTypes, allowZeroCharge, routeDepartureDetails, 3
            )
        }
    }

    /**
     * @param  sortedRoutes  list of RouteAdapterObjects which are sorted by fastest then cheapest then shortest using breezeRouteSortingUtil.sortBasedOnFastestRoute
     * @return   List<RoutePreference>  list of route preferences matching the order of routes
     */
    fun calculateMergedRoutePreference(
        sortedRoutes: List<RouteAdapterObjects>
    ): List<RoutePreference> {
        val routePrefList = ArrayList<RoutePreference>()
        if (sortedRoutes.isEmpty()) {
            return listOf()
        }
        val mapOfRoutePreference = HashMap<RoutePreference, List<RouteAdapterObjects>>()
        val completeList = mutableListOf<RouteAdapterObjects>()
        val alternateRoutes = getAllAlternateRoutes(listOf(sortedRoutes))
        determineRoutePreferenceMap(
            sortedRoutes,
            completeList,
            alternateRoutes,
            mapOfRoutePreference
        )
        correlateRouteResults(sortedRoutes, mapOfRoutePreference, routePrefList)
        return routePrefList
    }

    private fun determineRoutePreferenceMap(
        routes: List<RouteAdapterObjects>,
        completeList: MutableList<RouteAdapterObjects>,
        alternateRoutes: MutableList<RouteAdapterObjects>,
        mapOfRoutePreference: HashMap<RoutePreference, List<RouteAdapterObjects>>
    ) {
        val routePreference = RoutePreferenceTypeData(null, null, null, null, true, false)
        routePreference.apply {
            routes.forEach {
                if (fastestRoute == null) {
                    fastestRoute = it
                } else {
                    if (fastestRoute!!.duration > it.duration) {
                        fastestRoute = it
                    }
                }
            }
            shortestRoute = fastestRoute

            routes.forEach {
                if (shortestRoute!!.distance > it.distance) {
                    shortestRoute = it
                }
            }

            cheapestRoute = if (shortestRoute != fastestRoute
                && ((shortestRoute!!.erpRate ?: 0.0F) < (fastestRoute!!.erpRate ?: 0.0F))
            ) shortestRoute else fastestRoute



            routes.forEach {
                if ((cheapestRoute!!.erpRate ?: 0.0F) > (it.erpRate ?: 0.0F)) {
                    cheapestRoute = it
                }
            }

            updateCompleteAndAlternateList(completeList, fastestRoute!!, alternateRoutes)
            isERPRateSame(commonERPPrice, fastestRoute!!, isAllERPRateSame).let {
                commonERPPrice = it.first
                isAllERPRateSame = it.second
            }

            if (shortestRoute!!.route != fastestRoute!!.route) {
                updateCompleteAndAlternateList(completeList, shortestRoute!!, alternateRoutes)
                isERPRateSame(commonERPPrice, shortestRoute!!, isAllERPRateSame).let {
                    commonERPPrice = it.first
                    isAllERPRateSame = it.second
                    shortestDetected = true
                }
            }

            if (!(cheapestRoute!!.route == fastestRoute!!.route || cheapestRoute!!.route ==
                        shortestRoute!!.route
                        )
            ) {
                updateCompleteAndAlternateList(completeList, cheapestRoute!!, alternateRoutes)
                isERPRateSame(commonERPPrice, cheapestRoute!!, isAllERPRateSame).let {
                    commonERPPrice = it.first
                    isAllERPRateSame = it.second
                }
            }
            isAllERPRateSame = checkISErpSameForAltRoutes(
                completeList, alternateRoutes,
                commonERPPrice, isAllERPRateSame
            )

            determineMixedTypeGrouping(
                completeList, alternateRoutes, mapOfRoutePreference,
                shortestDetected, true, isAllERPRateSame
            )
        }
    }

    private fun correlateRouteResults(
        routes: List<RouteAdapterObjects>,
        mapOfRoutePreference: HashMap<RoutePreference, List<RouteAdapterObjects>>,
        routePrefList: ArrayList<RoutePreference>
    ) {
        for (route in routes) {
            var routeDetected = false
            for (key in mapOfRoutePreference.keys) {
                for (detectedRoutes in mapOfRoutePreference[key]!!) {
                    if (detectedRoutes.route == route.route) {
                        routePrefList.add(key)
                        routeDetected = true
                        break
                    }
                }
                if (routeDetected) {
                    break
                }
            }
            if (!routeDetected) {
                routePrefList.add(RoutePreference.ALTERNATE_ROUTE)
            }
        }
    }


    private suspend fun fetchAllERPRoutes(
        mapOfRouteTypes: HashMap<RouteType, List<DirectionsRouteResponse>>,
        allowZeroCharge: Boolean,
        routeDepartureDetails: RouteDepartureDetails?,
        limit: Int
    ): List<RouteAdapterObjects> {

        if (mapOfRouteTypes.containsKey(RouteType.WITH_DRIVING_TRAFFIC_AND_TOLLS)) {
            val fastestRoutes = mapOfRouteTypes[RouteType.WITH_DRIVING_TRAFFIC_AND_TOLLS]
            val routeObjects = populateERPValues(
                createRouteObjects(fastestRoutes!!, routeDepartureDetails),
                allowZeroCharge,
                routeDepartureDetails
            )
            val sortedRoutes = breezeRouteSortingUtil.sortBasedOnFastestRoute(routeObjects)
            return sortedRoutes.take(limit)
        }
        return listOf()
    }


    private suspend fun fetchDefaultRoutePreference(
        mapOfRouteTypes: HashMap<RouteType, List<DirectionsRouteResponse>>,
        routeDepartureDetails: RouteDepartureDetails?,
        allowZeroCharge: Boolean,
        limit: Int
    ): HashMap<RoutePreference, List<RouteAdapterObjects>> {

        val mapOfRoutePreference = hashMapOf<RoutePreference, List<RouteAdapterObjects>>()
        if (mapOfRouteTypes.containsKey(RouteType.WITH_DRIVING_TRAFFIC_AND_TOLLS)) {
            val fastestRoutes = mapOfRouteTypes[RouteType.WITH_DRIVING_TRAFFIC_AND_TOLLS]
            val routeObjects = populateERPValues(
                createRouteObjects(fastestRoutes!!, routeDepartureDetails),
                allowZeroCharge,
                routeDepartureDetails
            )
            val sortedRoutes = breezeRouteSortingUtil.sortBasedOnFastestRoute(routeObjects)

            mapOfRoutePreference[RoutePreference.FASTEST_ROUTE] = sortedRoutes.take(limit)
            mapOfRoutePreference[RoutePreference.SHORTEST_ROUTE] =
                breezeRouteSortingUtil.sortBasedOnShortestRoute(routeObjects).take(limit)

        }


        if (mapOfRouteTypes.containsKey(RouteType.WITH_DRIVING_TRAFFIC_AND_NO_TOLLS)) {
            val cheapestRoutes = mapOfRouteTypes[RouteType.WITH_DRIVING_TRAFFIC_AND_NO_TOLLS]!!
            // Convert all cheapest routes to Route Adapter objects
            val cheapestList = createRouteObjects(cheapestRoutes, routeDepartureDetails)
            Timber.d("No of cheapest route $cheapestRoutes")
            val fastestList = mutableListOf<RouteAdapterObjects>()
            val completeList = mutableListOf<RouteAdapterObjects>()
            val shortestRoutes = ArrayList(
                mapOfRoutePreference[RoutePreference.SHORTEST_ROUTE] ?: mutableListOf()
            )

            // Taking the map of route preference and removing all routes with tolls
            val fastestRoutes = mapOfRoutePreference[RoutePreference.FASTEST_ROUTE]
            if (fastestRoutes != null) {
                for (item in fastestRoutes) {
                    if (item.erpRate == null || item.erpRate == 0F) {
                        fastestList.add(item)
                    }
                }
            }

            completeList.addAll(fastestList)

            for (cheapRoute in cheapestList) {
                var shortestRouteExists = false
                var routeExists = false
                cheapRoute.erpRate = 0F
                for (fastRoute in fastestList) {
                    if (fastRoute.route.isSameRoute(cheapRoute.route)) {
                        routeExists = true
                        break
                    }
                }
                for (shortRoute in shortestRoutes) {
                    if (shortRoute.route.isSameRoute(cheapRoute.route)) {
                        shortestRouteExists = true
                        break
                    }
                }
                if (!shortestRouteExists) {
                    shortestRoutes.add(cheapRoute)
                }

                if (!routeExists) {
                    completeList.add(cheapRoute)
                }
            }
            mapOfRoutePreference[RoutePreference.SHORTEST_ROUTE] =
                breezeRouteSortingUtil.sortBasedOnShortestRoute(shortestRoutes).take(limit)
            mapOfRoutePreference[RoutePreference.CHEAPEST_ROUTE] = completeList.take(limit)

        }
        return mapOfRoutePreference
    }


    private suspend fun fetchSingleTypeRoutePreference(
        mapOfRouteTypes: HashMap<RouteType, List<DirectionsRouteResponse>>,
        allowZeroCharge: Boolean,
        routeDepartureDetails: RouteDepartureDetails?,
        limit: Int
    ): HashMap<RoutePreference, List<RouteAdapterObjects>> {

        val mapOfRoutePreference: HashMap<RoutePreference, List<RouteAdapterObjects>> =
            fetchDefaultRoutePreference(
                mapOfRouteTypes,
                routeDepartureDetails,
                allowZeroCharge,
                limit
            )
        mapOfRoutePreference[RoutePreference.CHEAPEST_ROUTE]?.let {
            mapOfRoutePreference[RoutePreference.CHEAPEST_ROUTE] = it.take(1)
        }
        fetchSingleRouteTypePreference(mapOfRoutePreference)

        return mapOfRoutePreference
    }

    private suspend fun fetchMergedTypeRoutePreference(
        mapOfRouteTypes: HashMap<RouteType, List<DirectionsRouteResponse>>,
        allowZeroCharge: Boolean,
        routeDepartureDetails: RouteDepartureDetails?,
        limit: Int
    ): HashMap<RoutePreference, List<RouteAdapterObjects>> {

        val mapOfRoutePreference: HashMap<RoutePreference, List<RouteAdapterObjects>> =
            fetchDefaultRoutePreference(
                mapOfRouteTypes,
                routeDepartureDetails,
                allowZeroCharge,
                limit
            )
        fetchMergedRouteTypePreference(mapOfRoutePreference)
        return mapOfRoutePreference
    }

    private fun fetchSingleRouteTypePreference(mapOfRoutePreference: HashMap<RoutePreference, List<RouteAdapterObjects>>) {
        val completeList = mutableListOf<RouteAdapterObjects>()
        mapOfRoutePreference[RoutePreference.CHEAPEST_ROUTE]?.let {
            completeList.addAll(it)
        }

        mapOfRoutePreference.remove(RoutePreference.FASTEST_ROUTE)?.let { fastestRoutes ->
            for (fastRoute in fastestRoutes) {
                var distinctRoute = true
                for (route in completeList) {
                    if (fastRoute.route.isSameRoute(route.route)) {
                        distinctRoute = false
                        break
                    }
                }
                if (distinctRoute) {
                    mapOfRoutePreference[RoutePreference.FASTEST_ROUTE] =
                        mutableListOf(fastRoute)
                    completeList.add(fastRoute)
                    break
                }
            }
        }


        mapOfRoutePreference.remove(RoutePreference.SHORTEST_ROUTE)?.let { shortestRoutes ->
            for (shortRoute in shortestRoutes) {
                var distinctRoute = true
                for (route in completeList) {
                    if (shortRoute.route.isSameRoute(route.route)) {
                        distinctRoute = false
                        break
                    }
                }
                if (distinctRoute) {
                    mapOfRoutePreference[RoutePreference.SHORTEST_ROUTE] =
                        mutableListOf(shortRoute)
                    completeList.add(shortRoute)
                    break
                }
            }
        }
    }

    /**
     * 1)Calculates upto 3 distinct routes from fastest/cheapest/shortest
     *       - If any two routes are the same for the corresponding type (e.g if fastest and shortest route are the same),
     *         they counted as a single route
     *       - For the chosen routes, the erp rates are compared to check if any erp rate is different
     * 2)The complete list is ordered and will contain these distinct routes, skipping any common route
     * 3)The Fastest/Shortest/Cheapest labels are calculated based on distinct routes detected
     * (e.g) if complete list is of size 2, and if cheapestDetected and fastestDetected,
     *     then we decide if its shortest_cheapest or shortest_fastest by comparing the total distance of cheapest and fastest route
     * 4)If there are no distinct erp rates, then we dont show cheapest route label
     *
     * @param  mapOfRoutePreference a map containing the fastest/shortest/cheapest routes determined by fetchDefaultRoutePreference algorithm
     * @return
     *
     */
    private fun fetchMergedRouteTypePreference(mapOfRoutePreference: HashMap<RoutePreference, List<RouteAdapterObjects>>) {

        val fastestRoutes = mapOfRoutePreference.remove(RoutePreference.FASTEST_ROUTE)
        val shortestRoutes = mapOfRoutePreference.remove(RoutePreference.SHORTEST_ROUTE)
        val cheapestRoutes = mapOfRoutePreference.remove(RoutePreference.CHEAPEST_ROUTE)
        val completeList = mutableListOf<RouteAdapterObjects>()
        var commonERPPrice: Float? = null
        var isAllERPRateSame = true
        val alternateRoutes =
            getAllAlternateRoutes(listOf(fastestRoutes, shortestRoutes, cheapestRoutes))

        var shortestDetected = false
        var fastestDetected = false

        fastestRoutes?.let {
            if (it.isNotEmpty()) {
                fastestDetected = true
                commonERPPrice = it[0].erpRate
                updateCompleteAndAlternateList(completeList, it[0], alternateRoutes)
            }
        }

        if (shortestRoutes != null) {
            for (shortRoute in shortestRoutes) {
                var distinctRoute = true
                for (route in completeList) {
                    if (shortRoute.route.isSameRoute(route.route)) {
                        distinctRoute = false
                        break
                    }
                    if (shortRoute.route.distance() >= route.route.distance()) {
                        distinctRoute = false
                        break
                    }
                }
                if (distinctRoute) {
                    shortestDetected = true
                    val erpPriceResultPair =
                        isERPRateSame(commonERPPrice, shortRoute, isAllERPRateSame)
                    commonERPPrice = erpPriceResultPair.first
                    isAllERPRateSame = erpPriceResultPair.second
                    updateCompleteAndAlternateList(completeList, shortRoute, alternateRoutes)
                    break
                }
            }
        }

        if (cheapestRoutes != null) {
            for (cheapRoute in cheapestRoutes) {
                var distinctRoute = true
                for (route in completeList) {
                    if (cheapRoute.route.isSameRoute(route.route)) {
                        distinctRoute = false
                        break
                    }
                    if ((cheapRoute.erpRate ?: 0.0F) >= (route.erpRate ?: 0.0F)) {
                        distinctRoute = false
                        break
                    }
                }
                if (distinctRoute) {
                    val erpPriceResultPair =
                        isERPRateSame(commonERPPrice, cheapRoute, isAllERPRateSame)
                    commonERPPrice = erpPriceResultPair.first
                    isAllERPRateSame = erpPriceResultPair.second
                    updateCompleteAndAlternateList(completeList, cheapRoute, alternateRoutes)
                    break
                }
            }
        }


        isAllERPRateSame = checkISErpSameForAltRoutes(
            completeList,
            alternateRoutes,
            commonERPPrice,
            isAllERPRateSame
        )


        determineMixedTypeGrouping(
            completeList,
            alternateRoutes,
            mapOfRoutePreference,
            shortestDetected,
            fastestDetected,
            isAllERPRateSame
        )
    }

    private fun checkISErpSameForAltRoutes(
        completeList: MutableList<RouteAdapterObjects>,
        alternateRoutes: MutableList<RouteAdapterObjects>,
        commonERPPrice: Float?,
        isAllERPRateSame: Boolean
    ): Boolean {
        var commonERPPrice1 = commonERPPrice
        var isAllERPRateSame1 = isAllERPRateSame
        val count = (3 - completeList.size)
        if (count > 0) {
            for (altRouteInd in 0 until count) {
                if (altRouteInd > (alternateRoutes.size - 1)) {
                    break
                }
                val erpPriceResultPair =
                    isERPRateSame(commonERPPrice1, alternateRoutes[altRouteInd], isAllERPRateSame1)
                commonERPPrice1 = erpPriceResultPair.first
                isAllERPRateSame1 = erpPriceResultPair.second
            }
        }
        return isAllERPRateSame1
    }

    private fun isERPRateSame(
        commonERPPrice: Float?,
        fastRoute: RouteAdapterObjects,
        isAllERPRateSame: Boolean
    ): Pair<Float?, Boolean> {
        var commonERPPrice1 = commonERPPrice
        var isAllERPRateSame1 = isAllERPRateSame
        if (commonERPPrice1 == null) {
            commonERPPrice1 = fastRoute.erpRate
        } else {
            isAllERPRateSame1 =
                checkIfERPPriceIsCommonPrice(commonERPPrice1, fastRoute, isAllERPRateSame1)
        }
        return Pair(commonERPPrice1, isAllERPRateSame1)
    }

    private fun checkIfERPPriceIsCommonPrice(
        commonERPPrice: Float?,
        route: RouteAdapterObjects,
        isAllERPRateSame: Boolean
    ): Boolean {
        var isAllERPRateSame1 = isAllERPRateSame
        if (commonERPPrice != route.erpRate) {
            isAllERPRateSame1 = false
        }
        return isAllERPRateSame1
    }

    private fun getAllAlternateRoutes(routes: List<List<RouteAdapterObjects>?>): MutableList<RouteAdapterObjects> {
        val alternateRoute = HashSet<DirectionsRoute>()
        val alternateRouteAdapterObjects = mutableListOf<RouteAdapterObjects>()
        routes.forEach { routeAdapterObjects ->
            routeAdapterObjects?.forEach {
                val result = alternateRoute.find { alternate ->
                    alternate.isSameRoute(it.route)
                }
                if (result == null) {
                    alternateRoute.add(it.route)
                    alternateRouteAdapterObjects.add(it)
                }
            }
        }
        return alternateRouteAdapterObjects
    }


    private fun updateCompleteAndAlternateList(
        completeList: MutableList<RouteAdapterObjects>,
        routeAdapterObject: RouteAdapterObjects,
        alternateRoutes: MutableList<RouteAdapterObjects>
    ) {
        completeList.add(routeAdapterObject)
        alternateRoutes.removeIf { atlRouteObj ->
            atlRouteObj.route.geometry() == routeAdapterObject.route.geometry()
        }
    }

    /**
     * This functions clubs items from the completeList to generate the route label tagging
     * The function makes assumptions about the indexes of the completeList
     * 1) index [0] is the FASTEST_ROUTE, if  fastestDetected is true else it is the SHORTEST_ROUTE
     * 2) index [1] is the SHORTEST_ROUTE, if  shortestDetected is true else it is the CHEAPEST_ROUTE
     * complete list having size 1 would indicate only 1 best route was found, which is fastest and shortest and cheapest
     * complete list having size 3 would indicate the 0 -> fastest, 1-> shortest, 2-> cheapest
     *
     */
    private fun determineMixedTypeGrouping(
        completeList: MutableList<RouteAdapterObjects>,
        unCategorizedRoutes: MutableList<RouteAdapterObjects>,
        mapOfRoutePreference: HashMap<RoutePreference, List<RouteAdapterObjects>>,
        shortestDetected: Boolean,
        fastestDetected: Boolean,
        isAllERPRateSame: Boolean
    ) {
        when (completeList.size) {
            3 -> {
                mapOfRoutePreference[RoutePreference.FASTEST_ROUTE] =
                    mutableListOf(completeList[0])
                mapOfRoutePreference[RoutePreference.SHORTEST_ROUTE] =
                    mutableListOf(completeList[1])
                mapOfRoutePreference[RoutePreference.CHEAPEST_ROUTE] =
                    mutableListOf(completeList[2])
            }
            2 -> {
                val alternateRoutes = ArrayList<RouteAdapterObjects>()
                if (!shortestDetected) {
                    if (completeList[0].distance <= completeList[1].distance) {

                        if (!isAllERPRateSame) {
                            mapOfRoutePreference[RoutePreference.CHEAPEST_ROUTE] =
                                mutableListOf(completeList[1])
                        } else {
                            alternateRoutes.add(completeList[1])
                        }
                        mapOfRoutePreference[RoutePreference.FASTEST_SHORTEST_ROUTE] =
                            mutableListOf(completeList[0])
                    } else {
                        if (isAllERPRateSame) {
                            mapOfRoutePreference[RoutePreference.SHORTEST_ROUTE] =
                                mutableListOf(completeList[1])
                        } else {
                            mapOfRoutePreference[RoutePreference.SHORTEST_CHEAPEST_ROUTE] =
                                mutableListOf(completeList[1])
                        }
                        mapOfRoutePreference[RoutePreference.FASTEST_ROUTE] =
                            mutableListOf(completeList[0])
                    }
                } else if (!fastestDetected) {
                    if (completeList[0].duration <= completeList[1].duration) {

                        if (!isAllERPRateSame) {
                            mapOfRoutePreference[RoutePreference.CHEAPEST_ROUTE] =
                                mutableListOf(completeList[1])
                        } else {
                            alternateRoutes.add(completeList[1])
                        }

                        mapOfRoutePreference[RoutePreference.FASTEST_SHORTEST_ROUTE] =
                            mutableListOf(completeList[0])
                    } else {

                        if (isAllERPRateSame) {
                            mapOfRoutePreference[RoutePreference.FASTEST_ROUTE] =
                                mutableListOf(completeList[1])
                        } else {
                            mapOfRoutePreference[RoutePreference.FASTEST_CHEAPEST_ROUTE] =
                                mutableListOf(completeList[1])
                        }

                        mapOfRoutePreference[RoutePreference.SHORTEST_ROUTE] =
                            mutableListOf(completeList[0])
                    }
                } else {
                    if ((completeList[0].erpRate == null) || (completeList[1].erpRate == null) || (completeList[0].erpRate!! > completeList[1].erpRate!!)) {
                        mapOfRoutePreference[RoutePreference.FASTEST_ROUTE] =
                            mutableListOf(completeList[0])
                        if (isAllERPRateSame) {
                            mapOfRoutePreference[RoutePreference.SHORTEST_ROUTE] =
                                mutableListOf(completeList[1])
                        } else {
                            mapOfRoutePreference[RoutePreference.SHORTEST_CHEAPEST_ROUTE] =
                                mutableListOf(completeList[1])
                        }
                    } else {
                        if (isAllERPRateSame) {
                            mapOfRoutePreference[RoutePreference.FASTEST_ROUTE] =
                                mutableListOf(completeList[0])
                        } else {
                            mapOfRoutePreference[RoutePreference.FASTEST_CHEAPEST_ROUTE] =
                                mutableListOf(completeList[0])
                        }

                        mapOfRoutePreference[RoutePreference.SHORTEST_ROUTE] =
                            mutableListOf(completeList[1])
                    }
                }
                alternateRoutes.addAll(unCategorizedRoutes.take(1))
                mapOfRoutePreference[RoutePreference.ALTERNATE_ROUTE] = alternateRoutes
            }
            1 -> {
                mapOfRoutePreference[RoutePreference.FASTEST_SHORTEST_ROUTE] =
                    mutableListOf(completeList[0])
                mapOfRoutePreference[RoutePreference.ALTERNATE_ROUTE] = unCategorizedRoutes.take(2)
            }
        }
    }


    fun getRouteOptions(
        isDrivingTraffic: Boolean,
        isAvoidTolls: Boolean,
        originPoint: Point,
        destination: Point,
        destinationName: String?,
        wayPoints: List<Point>,
        routeDepartureDetails: RouteDepartureDetails?
    ): MapboxDirections {

//        val wayPointList = ArrayList<Point>()
//        wayPointList.add(Point.fromLngLat(103.90280,1.41040))
        val routeOptionsBuilder = RouteOptions.builder()
            .applyDefaultNavigationOptions()
            .language(MAPBOX_ROUTE_OPTION_LOCALE)
            .coordinates(originPoint, wayPoints, destination)
            .waypointIndicesList(listOf(0, wayPoints.size + 1))
            .overview(DirectionsCriteria.OVERVIEW_FULL)
            .annotationsList(
                arrayListOf(
                    DirectionsCriteria.ANNOTATION_DISTANCE,
                    DirectionsCriteria.ANNOTATION_CONGESTION,
                    DirectionsCriteria.ANNOTATION_MAXSPEED
                )
            )
            .geometries(Constants.POLYLINE_GEOMETRY)
            .alternatives(true)
            .voiceInstructions(true)
            .voiceUnits(DirectionsCriteria.METRIC)
            .bannerInstructions(true) //this
            .steps(true)

        destinationName?.let {
            val destinationNameEscaped = Utils.escapeAmpersandWithSpace(it)
            Log.d("RoutePlanningHelper", "destinationNameEscaped is$destinationNameEscaped")
            val waypointNamesList = ArrayList(Collections.nCopies(1, ""))
            waypointNamesList.add(destinationNameEscaped)
            routeOptionsBuilder.waypointNamesList(waypointNamesList)
        }

        if (isDrivingTraffic) {
            routeOptionsBuilder.profile(DirectionsCriteria.PROFILE_DRIVING_TRAFFIC)
        } else {
            routeOptionsBuilder.profile(DirectionsCriteria.PROFILE_DRIVING)
        }

        if (isAvoidTolls) {
            routeOptionsBuilder.exclude(DirectionsCriteria.EXCLUDE_TOLL)
        }

        routeDepartureDetails?.let {
            if (it.type == DepartureType.DEPARTURE) {
                routeOptionsBuilder.departAt(
                    DateFormat.format(
                        "yyyy-MM-dd'T'HH:mm",
                        Date(it.timestamp)
                    ).toString()
                )
            } else {
                routeOptionsBuilder.arriveBy(
                    DateFormat.format(
                        "yyyy-MM-dd'T'HH:mm",
                        Date(it.timestamp)
                    ).toString()
                )
            }
        }

        return MapboxDirections.builder()
            .routeOptions(routeOptionsBuilder.build())
            .accessToken(mapboxToken)
            .build()
    }

    private fun getWalkingRouteOptions(
        originPoint: Point?,
        destination: Point?,
        destinationName: String?,
    ): Pair<MapboxDirections, RouteOptions> {
        val routeOptionsBuilder = RouteOptions.builder()
            .applyDefaultNavigationOptions()
            .language(MAPBOX_ROUTE_OPTION_LOCALE)
            .coordinates(originPoint!!, null, destination!!)
            .overview(DirectionsCriteria.OVERVIEW_FULL)
            .annotationsList(
                arrayListOf(
                    DirectionsCriteria.ANNOTATION_DISTANCE,
                    DirectionsCriteria.ANNOTATION_CONGESTION,
                    DirectionsCriteria.ANNOTATION_MAXSPEED
                )
            )
            .alternatives(true)
            .geometries(Constants.POLYLINE_GEOMETRY)
            .voiceInstructions(true)
            .voiceUnits(DirectionsCriteria.METRIC)
            .bannerInstructions(true) //this
            .steps(true)

        destinationName?.let {
            val destinationNameEscaped = Utils.escapeAmpersandWithSpace(it)
            Log.d("RoutePlanningHelper", "destinationNameEscaped is$destinationNameEscaped")
            val waypointNamesList = ArrayList(Collections.nCopies(1, ""))
            waypointNamesList.add(destinationNameEscaped)
            routeOptionsBuilder.waypointNamesList(waypointNamesList)
        }
        routeOptionsBuilder.profile(DirectionsCriteria.PROFILE_WALKING)
        val routeOptions = routeOptionsBuilder.build()

        val client = MapboxDirections.builder()
            .routeOptions(routeOptions)
            .accessToken(mapboxToken)
            .build()

        return Pair(client, routeOptions)
    }

    private suspend fun populateERPValues(
        routeData: List<RouteAdapterObjects>,
        allowZeroCharge: Boolean,
        routeDepartureDetails: RouteDepartureDetails?
    ): List<RouteAdapterObjects> {
        val erpInfo = breezeERPUtil.getERPData()?.let {
            breezeERPUtil.getERPsIntersectingWithRouteList(
                it, routeData, allowZeroCharge, routeDepartureDetails
            )
        }

        if (erpInfo != null) {
            for ((index, routeObject) in routeData.withIndex()) {
                routeObject.erpRate = erpInfo[index]?.erpRate
                routeObject.erpFeatureCollection = erpInfo[index]?.featureCollection
                routeObject.erpList = erpInfo[index]?.erpList
            }
        }

        //var currentRouteObjects = breezeRouteSortingUtil.getSortedRoutes(routeData)
        return routeData
    }


    private fun createRouteObjects(
        currentRouteList: List<DirectionsRouteResponse>,
        routeDepartureDetails: RouteDepartureDetails?
    )
            : MutableList<RouteAdapterObjects> {
        val routeObjectList: MutableList<RouteAdapterObjects> = arrayListOf()
        for (route in currentRouteList) {
            val minutes = Utils.convertDurationToMinutes(route.directionsRoute.duration().toFloat())
            val time = Utils.splitToComponentTimes(minutes)
            val arrivalTime: String = if (routeDepartureDetails != null) {
                if (routeDepartureDetails.type == DepartureType.DEPARTURE) {
                    Utils.addTimeToTimeStamp(
                        time!![0],
                        time!![1],
                        routeDepartureDetails.timestamp
                    )
                } else {
                    Utils.formatTime(routeDepartureDetails.timestamp)
                }
            } else {
                Utils.addTimeToCurrentTime(time!![0], time!![1])
            }

            //val distance = Utils.convertMeterToKilometer(route.distance().toFloat())
            val distanceInMeters = route.directionsRoute.distance().toFloat()
            routeObjectList.add(
                RouteAdapterObjects(
                    route.directionsRoute,
                    arrivalTime,
                    minutes,
                    distanceInMeters,
                    null,
                    null,
                    null,
                    route.originalResponse
                )
            )
        }
        return routeObjectList
    }


    enum class RouteType {
        WITH_DRIVING_TRAFFIC_AND_TOLLS,
        WITH_DRIVING_TRAFFIC_AND_NO_TOLLS,
        WITH_PROFILE_DRIVING
    }

    enum class RouteAlgorithm {
        NORMAL,
        SINGLE_ROUTE_PER_TYPE,
        MERGED_SINGLE_ROUTE_PER_TYPE,
    }

    data class RoutePreferenceTypeData(
        var fastestRoute: RouteAdapterObjects?, var shortestRoute: RouteAdapterObjects?,
        var cheapestRoute: RouteAdapterObjects?, var commonERPPrice: Float?,
        var isAllERPRateSame: Boolean, var shortestDetected: Boolean
    )

    data class DirectionsRouteResponse(
        val originalResponse: DirectionsResponse,
        val directionsRoute: DirectionsRoute
    )
}
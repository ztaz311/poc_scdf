package com.ncs.breeze.ui.dashboard.fragments.viewmodel

import android.app.Application
import androidx.lifecycle.viewModelScope
import com.breeze.model.enums.DebugMode
import com.ncs.breeze.common.remote.ApiHelper
import com.ncs.breeze.ui.base.BaseFragmentViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

class SettingsViewModel @Inject constructor(apiHelper: ApiHelper, application: Application) :
    BaseFragmentViewModel(
        application
    ) {
    var mAPIHelper: ApiHelper = apiHelper
    private val _debugModeFlow = MutableStateFlow(DebugMode.OFF.value)
    val debugModeFlow = _debugModeFlow.asStateFlow()

    fun updateDebugMode(mode: DebugMode) {
        viewModelScope.launch(Dispatchers.IO) {
            _debugModeFlow.emit(mode.value)
        }
    }
}
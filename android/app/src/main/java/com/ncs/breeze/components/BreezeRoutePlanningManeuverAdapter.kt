package com.ncs.breeze.components

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.StyleRes
import androidx.appcompat.view.ContextThemeWrapper
import androidx.constraintlayout.widget.Guideline
import androidx.core.widget.TextViewCompat
import androidx.recyclerview.widget.RecyclerView
import com.mapbox.navigation.ui.maneuver.model.*
import com.mapbox.navigation.ui.maneuver.view.MapboxPrimaryManeuver
import com.mapbox.navigation.ui.maneuver.view.MapboxStepDistance
import com.mapbox.navigation.ui.maneuver.view.MapboxTurnIconManeuver
import com.mapbox.navigation.utils.internal.ifNonNull
import com.ncs.breeze.R
import com.ncs.breeze.common.extensions.mapbox.extractNonTamilAndChineseName


class BreezeRoutePlanningManeuverAdapter(
    private val context: Context
) : RecyclerView.Adapter<BreezeRoutePlanningManeuverAdapter.BreezeUpcomingManeuverViewHolder>() {

    @StyleRes
    private var stepDistanceAppearance: Int? = null

    @StyleRes
    private var primaryManeuverAppearance: Int? = null

    @StyleRes
    private var secondaryManeuverAppearance: Int? = null

    private var contextTheme: ContextThemeWrapper? = null

    private val inflater = LayoutInflater.from(context)
    private val upcomingManeuverList = mutableListOf<Maneuver>()
    private val maneuverShieldMap = mutableMapOf<String, RoadShield?>()

    /**
     * Binds the given View to the position.
     * @param parent ViewGroup
     * @param viewType Int
     * @return MapboxLaneGuidanceViewHolder
     */
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BreezeUpcomingManeuverViewHolder {
        val root: View =
            inflater.inflate(R.layout.breeze_item_upcoming_maneuvers_layout, parent, false)
        return BreezeUpcomingManeuverViewHolder(root)
    }

    /**
     * Returns the total number of items in the data set held by the adapter.
     * @return Int
     */
    override fun getItemCount(): Int {
        return upcomingManeuverList.size
    }


    /**
     * Given a [Map] of id to [RoadShield], the function maintains a map of id to [RoadShield] that
     * is used to render the shield on the view
     * @param shieldMap Map<String, RoadShield?>
     */
    fun updateRoadShields(shieldMap: Map<String, RoadShield?>) {
        maneuverShieldMap.putAll(shieldMap)
    }

    /**
     * Allows you to change the text appearance of step distance text in upcoming maneuver list.
     * @see [TextViewCompat.setTextAppearance]
     * @param style Int
     */
    fun updateUpcomingManeuverStepDistanceTextAppearance(@StyleRes style: Int) {
        stepDistanceAppearance = style
    }

    /**
     * Allows you to change the text appearance of primary maneuver text in upcoming maneuver list.
     * @see [TextViewCompat.setTextAppearance]
     * @param style Int
     */
    fun updateUpcomingPrimaryManeuverTextAppearance(@StyleRes style: Int) {
        primaryManeuverAppearance = style
    }

    /**
     * Allows you to change the text appearance of secondary maneuver text in upcoming maneuver list.
     * @see [TextViewCompat.setTextAppearance]
     * @param style Int
     */
    fun updateIconContextThemeWrapper(wrapper: ContextThemeWrapper) {
        contextTheme = wrapper
    }


    fun updateUpcomingSecondaryManeuverTextAppearance(@StyleRes style: Int) {
        secondaryManeuverAppearance = style
    }


    /**
     * Invoke to add all upcoming maneuvers to the recycler view.
     * @param upcomingManeuvers List<Maneuver>
     */
    fun addUpcomingManeuvers(upcomingManeuvers: List<Maneuver>) {
        upcomingManeuverList.clear()
        upcomingManeuverList.addAll(upcomingManeuvers)

    }

    /**
     * View Holder defined for the [RecyclerView.Adapter]
     * @property viewBinding
     * @constructor
     */
    inner class BreezeUpcomingManeuverViewHolder(
        val view: View
    ) : RecyclerView.ViewHolder(view) {

        val mainManeuverGuideline: Guideline
        val maneuverIcon: MapboxTurnIconManeuver
        val primaryManeuverText: MapboxPrimaryManeuver
        val stepDistance: MapboxStepDistance

        init {

            mainManeuverGuideline = view.findViewById<View>(R.id.mainManeuverGuideline) as Guideline
            maneuverIcon = view.findViewById<View>(R.id.maneuverIcon) as MapboxTurnIconManeuver
            primaryManeuverText =
                view.findViewById<View>(R.id.primaryManeuverText) as MapboxPrimaryManeuver
            stepDistance = view.findViewById<View>(R.id.stepDistance) as MapboxStepDistance
        }

        /**
         * Invoke the method to bind the maneuver to the view.
         * @param maneuver Maneuver
         */
        fun bindUpcomingManeuver(maneuver: Maneuver) {
            val primary = maneuver.primary
            val stepDistance = maneuver.stepDistance
            val primaryId = primary.id
            updateIconContextTheme()
            drawPrimaryManeuver(primary, maneuverShieldMap[primaryId])
            drawTotalStepDistance(stepDistance)


        }

        private fun updateIconContextTheme() {
            contextTheme?.let {
                maneuverIcon.updateTurnIconStyle(it)
            }
        }

        private fun updateUpcomingPrimaryManeuverTextAppearance() {
            ifNonNull(primaryManeuverAppearance) { appearance ->
                TextViewCompat.setTextAppearance(primaryManeuverText, appearance)
            }
        }

        private fun updateStepDistanceTextAppearance() {
            ifNonNull(stepDistanceAppearance) { appearance ->
                TextViewCompat.setTextAppearance(stepDistance, appearance)
            }
        }


        private fun drawPrimaryManeuver(primary: PrimaryManeuver, roadShield: RoadShield? = null) {
            primaryManeuverText.render(primary, roadShield)
            primary.extractNonTamilAndChineseName()?.let { primaryManeuverText.text = it }
            maneuverIcon.renderPrimaryTurnIcon(primary)
        }

        private fun drawTotalStepDistance(stepDistance: StepDistance) {
            this.stepDistance.renderTotalStepDistance(stepDistance)
        }

    }

    override fun onBindViewHolder(holder: BreezeUpcomingManeuverViewHolder, position: Int) {
        val maneuver = upcomingManeuverList[position]
        holder.bindUpcomingManeuver(maneuver)
    }

}

package com.ncs.breeze.car.breeze.base

import com.breeze.model.ERPFeatures
import com.ncs.breeze.common.model.rx.EhorizonLayerType
import com.ncs.breeze.common.utils.BreezeERPRefreshUtil
import com.ncs.breeze.car.breeze.MainBreezeCarContext
import com.ncs.breeze.car.breeze.utils.BreezeMapBoxUtil
import com.mapbox.geojson.Feature
import com.mapbox.geojson.FeatureCollection

abstract class BaseNavigationScreenCar(var mainCarContext: MainBreezeCarContext) :
    BaseScreenCar(mainCarContext.carContext) {

    open fun updateERPLayer(erpFeature: ERPFeatures?) {
        if (erpFeature != null) {
            mainCarContext.mapboxCarMap.carMapSurface?.mapSurface?.getMapboxMap()?.getStyle()
                ?.let {
                    BreezeMapBoxUtil.addERPLayerToStyle(it, erpFeature, mainCarContext.carContext);

                }
        }
    }

    fun updateIncidentLayer(incidentData: HashMap<String, FeatureCollection>) {
        mainCarContext.mapboxCarMap.carMapSurface?.mapSurface?.getMapboxMap()?.getStyle()?.let {
            BreezeMapBoxUtil.displayIncidentData(it, incidentData);
        }
    }


    @Deprecated("No longer in use", ReplaceWith("BreezeRoadObjectObserver"))
    fun updateEhorizonLayer(type: EhorizonLayerType, featureList: ArrayList<Feature>) {
        mainCarContext.mapboxCarMap.carMapSurface?.mapSurface?.getMapboxMap()?.getStyle()
            ?.let {
                when (type) {
                    EhorizonLayerType.EHORIZON_SCHOOL_LAYER -> BreezeMapBoxUtil.addSchoolZoneLayerToStyle(
                        it,
                        featureList
                    );
                    EhorizonLayerType.EHORIZON_SILVER_LAYER -> BreezeMapBoxUtil.addSilverZoneLayerToStyle(
                        it,
                        featureList
                    );
                }
            }
    }


    @Deprecated("No longer in use", ReplaceWith("BreezeRoadObjectObserver"))
    fun removeEhorizonLayer() {
        mainCarContext.mapboxCarMap.carMapSurface?.mapSurface?.getMapboxMap()?.getStyle()
            ?.let {
                BreezeMapBoxUtil.removeEhorizonLayers(it);
            }
    }

    open fun updateCardBalance(cardBalance:Double?){}
    open fun changeOBUConnectionState(connected: Boolean) {}

}
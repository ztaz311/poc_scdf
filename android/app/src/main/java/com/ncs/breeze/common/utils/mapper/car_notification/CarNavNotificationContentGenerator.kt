package com.ncs.breeze.common.utils.mapper.car_notification

import androidx.annotation.VisibleForTesting
import com.ncs.breeze.common.constant.OBUConstants
import com.breeze.model.extensions.parseOrNull
import com.breeze.model.obu.OBURoadEventData
import com.breeze.model.api.request.ChargingInfo
import sg.gov.lta.obu.sdk.core.enums.OBUChargingMessageType
import java.text.SimpleDateFormat
import java.util.*

object CarNavNotificationContentGenerator {
    private fun generateTitleFromOBURoadEvent(data: OBURoadEventData): String? =
        when (data.eventType) {
            OBURoadEventData.EVENT_TYPE_TRAFFIC,
            OBURoadEventData.EVENT_TYPE_SCHOOL_ZONE,
            OBURoadEventData.EVENT_TYPE_SILVER_ZONE,
            OBURoadEventData.EVENT_TYPE_PARKING_FEE,
            OBURoadEventData.EVENT_TYPE_PARKING_SUCCESS,
            OBURoadEventData.EVENT_TYPE_PARKING_FAILURE,
            OBURoadEventData.EVENT_TYPE_GENERAL_MESSAGE,
            OBURoadEventData.EVENT_TYPE_ROAD_CLOSURE,
            OBURoadEventData.EVENT_TYPE_EVENT,
            OBURoadEventData.EVENT_TYPE_ROAD_DIVERSION,
            OBURoadEventData.EVENT_TYPE_FLASH_FLOOD,
            OBURoadEventData.EVENT_TYPE_HEAVY_TRAFFIC,
            OBURoadEventData.EVENT_TYPE_UNATTENDED_VEHICLE,
            OBURoadEventData.EVENT_TYPE_MISCELLANEOUS,
            OBURoadEventData.EVENT_TYPE_ROAD_BLOCK,
            OBURoadEventData.EVENT_TYPE_OBSTACLE,
            OBURoadEventData.EVENT_TYPE_ROAD_WORK,
            OBURoadEventData.EVENT_TYPE_VEHICLE_BREAKDOWN,
            OBURoadEventData.EVENT_TYPE_MAJOR_ACCIDENT,
            OBURoadEventData.EVENT_TYPE_SEASON_PARKING,
            OBURoadEventData.EVENT_TYPE_TREE_PRUNING,
            OBURoadEventData.EVENT_TYPE_YELLOW_DENGUE_ZONE,
            OBURoadEventData.EVENT_TYPE_RED_DENGUE_ZONE,
            OBURoadEventData.EVENT_TYPE_SPEED_CAMERA,
            OBURoadEventData.EVENT_TYPE_BUS_LANE,
            -> data.message
            OBURoadEventData.EVENT_TYPE_ERP_CHARGING,
            OBURoadEventData.EVENT_TYPE_ERP_SUCCESS,
            OBURoadEventData.EVENT_TYPE_ERP_FAILURE -> data.detailMessage
            else -> null
        }

    fun generateTitleFromOBURoadEvent(
        data: OBURoadEventData, defaultText: String
    ): String = generateTitleFromOBURoadEvent(data) ?: defaultText

    private fun generateContentFromOBURoadEvent(data: OBURoadEventData): String? =
        when (data.eventType) {
            OBURoadEventData.EVENT_TYPE_TRAFFIC,
            OBURoadEventData.EVENT_TYPE_SCHOOL_ZONE,
            OBURoadEventData.EVENT_TYPE_SILVER_ZONE,
            OBURoadEventData.EVENT_TYPE_ERP_CHARGING,
            OBURoadEventData.EVENT_TYPE_PARKING_FEE,
            OBURoadEventData.EVENT_TYPE_ROAD_CLOSURE,
            OBURoadEventData.EVENT_TYPE_EVENT,
            OBURoadEventData.EVENT_TYPE_ROAD_DIVERSION,
            OBURoadEventData.EVENT_TYPE_GENERAL_MESSAGE,
            OBURoadEventData.EVENT_TYPE_FLASH_FLOOD,
            OBURoadEventData.EVENT_TYPE_HEAVY_TRAFFIC,
            OBURoadEventData.EVENT_TYPE_UNATTENDED_VEHICLE,
            OBURoadEventData.EVENT_TYPE_MISCELLANEOUS,
            OBURoadEventData.EVENT_TYPE_ROAD_BLOCK,
            OBURoadEventData.EVENT_TYPE_OBSTACLE,
            OBURoadEventData.EVENT_TYPE_ROAD_WORK,
            OBURoadEventData.EVENT_TYPE_VEHICLE_BREAKDOWN,
            OBURoadEventData.EVENT_TYPE_MAJOR_ACCIDENT,
            OBURoadEventData.EVENT_TYPE_SEASON_PARKING,
            OBURoadEventData.EVENT_TYPE_TREE_PRUNING,
            OBURoadEventData.EVENT_TYPE_YELLOW_DENGUE_ZONE,
            OBURoadEventData.EVENT_TYPE_RED_DENGUE_ZONE,
            OBURoadEventData.EVENT_TYPE_SPEED_CAMERA -> data.distance
            OBURoadEventData.EVENT_TYPE_ERP_SUCCESS,
            OBURoadEventData.EVENT_TYPE_ERP_FAILURE-> data.message
            OBURoadEventData.EVENT_TYPE_BUS_LANE-> data.detailMessage
            OBURoadEventData.EVENT_TYPE_PARKING_SUCCESS -> {
                data.chargingInfo?.find { it.chargingMessageType == OBUChargingMessageType.DeductionSuccessful.name }
                    ?.let { generateCarParkChargeResultMessage(it) }
            }
            OBURoadEventData.EVENT_TYPE_PARKING_FAILURE -> {
                data.chargingInfo?.find { it.chargingMessageType == OBUChargingMessageType.DeductionFailure.name }
                    ?.let { generateCarParkChargeResultMessage(it) }
            }
            else -> null
        }

    fun generateContentFromOBURoadEvent(data: OBURoadEventData, defaultText: String): String =
        generateContentFromOBURoadEvent(data) ?: defaultText

    @VisibleForTesting
    private fun generateCarParkChargeResultMessage(chargingInfo: ChargingInfo): String {
        val totalCharge = chargingInfo.chargingAmount?.div(100.0) ?: return ""
        val totalChargeFormatted = "%.2f".format(totalCharge)
        val timeFormat = SimpleDateFormat(OBUConstants.OBU_TIME_FORMAT, Locale.getDefault())
        val startDate =
            timeFormat.parseOrNull(chargingInfo.startTime ?: "")
                ?: return "$${totalChargeFormatted}"
        val endDate =
            timeFormat.parseOrNull(chargingInfo.endTime ?: "") ?: return "$${totalChargeFormatted}"
        val durationHours = (endDate.time - startDate.time).toDouble() / 3600000.0
        return "$${totalChargeFormatted} | ${"%.1f".format(durationHours)} hr${if (durationHours == 1.0) "" else "s"}"
    }
}
package com.ncs.breeze.ui.login.fragment.signIn.module

import com.ncs.breeze.ui.login.fragment.signIn.view.VerifyMobileFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class VerifyMobileModule {
    @ContributesAndroidInjector
    abstract fun contributeVerifyMobileFragment(): VerifyMobileFragment
}
package com.ncs.breeze.common.extensions.mapbox

import android.text.SpannableStringBuilder
import com.mapbox.navigation.base.road.model.RoadComponent
import com.breeze.model.extensions.hasExcludedRoadNameCharacters

fun List<RoadComponent>.extractNonTamilAndChineseRoadName(): String? {
    val stringBuilder = SpannableStringBuilder()
    val separator = " / "
    forEach { textComponentNode ->
        textComponentNode.text.takeIf { text -> !text.hasExcludedRoadNameCharacters() }?.let {
            stringBuilder.append(it)
            stringBuilder.append(separator)
        }
    }
    return stringBuilder.takeIf { it.isNotEmpty() }
        ?.removeSuffix(separator)?.toString()
}
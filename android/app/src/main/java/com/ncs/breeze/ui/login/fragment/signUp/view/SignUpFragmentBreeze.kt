package com.ncs.breeze.ui.login.fragment.signUp.view

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.text.SpannableString
import android.text.Spanned
import android.text.TextPaint
import android.text.TextUtils
import android.text.style.ClickableSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import com.breeze.model.constants.MODE_LOGIN_TYPE
import com.ncs.breeze.R
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.Event
import com.ncs.breeze.common.analytics.Screen
import com.ncs.breeze.common.storage.BreezeUserPreference
import com.ncs.breeze.common.utils.BreezeUserPreferenceUtil
import com.ncs.breeze.common.utils.DialogFactory
import com.ncs.breeze.common.utils.InitObjectUtilsController
import com.ncs.breeze.common.utils.Utils
import com.ncs.breeze.common.utils.Variables
import com.ncs.breeze.databinding.FragmentSignUpFragmentBreezeBinding
import com.ncs.breeze.helper.PermissionHelper
import com.ncs.breeze.ui.base.BaseFragment
import com.ncs.breeze.ui.login.activity.LoginActivity
import com.ncs.breeze.ui.login.fragment.signUp.viewmodel.SignUpFragmentBreezeViewModel
import com.ncs.breeze.ui.utils.DialogUtilsBreeze
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import timber.log.Timber
import javax.inject.Inject

class SignUpFragmentBreeze :
    BaseFragment<FragmentSignUpFragmentBreezeBinding, SignUpFragmentBreezeViewModel>() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val viewModel: SignUpFragmentBreezeViewModel by viewModels {
        viewModelFactory
    }
    lateinit var dialog: Dialog
    val scope: CoroutineScope = CoroutineScope(SupervisorJob() + Dispatchers.Main.immediate)

    @Inject
    lateinit var userPreferenceUtil: BreezeUserPreferenceUtil

    companion object {
        val SCREEN_NAME = "SignUpFragmentBreeze"

        @JvmStatic
        fun newInstance() =
            SignUpFragmentBreeze().apply {
                arguments = Bundle().apply {}
            }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        PermissionHelper.requestLocationPermission(view.context)
        observeSocialLogin()
        loginWithoutAccount()
        handleLoginWithSocial()
    }

    /**
     * handle login fb & google
     */
    private fun handleLoginWithSocial() {
        viewBinding.backButton.setOnClickListener {
            Analytics.logClickEvent(Event.BACK, Screen.ONBOARDING_CREATE_ACC_SIGNUP)
            (activity as LoginActivity).hideKeyboard()
            onBackPressed()
        }

        if (Utils.isAnyCustomTabsPackageInstalled(requireContext())) {
//            viewBinding.facebookButton.visibility = View.VISIBLE
            viewBinding.googleButton.visibility = View.VISIBLE
            loginWithFacebook()
            loginWithGoogle()
        } else {
//            viewBinding.facebookButton.visibility = View.GONE
            viewBinding.googleButton.visibility = View.GONE
        }
    }

    /**
     * login with google
     */
    private fun loginWithGoogle() {
        viewBinding.googleButton.setOnClickListener {
            if (Variables.isNetworkConnected) {
                Analytics.logClickEvent(
                    Event.CREATE_ACCOUNT_GOOGLE,
                    Screen.ONBOARDING_CREATE_ACC_SIGNUP
                )
                viewBinding.createProgressDialog.visibility = View.VISIBLE
                activity?.let {
                    viewModel.loginWithGoogle(it)
                }
            } else {
                DialogFactory.internetDialog(
                    context,
                ) { _, _ -> }?.show()
            }
        }
    }

    /**
     * login with facebook
     */
    private fun loginWithFacebook() {
        viewBinding.facebookButton.setOnClickListener {
            if (Variables.isNetworkConnected) {
                Analytics.logClickEvent(
                    Event.CREATE_ACC_FACEBOOK,
                    Screen.ONBOARDING_CREATE_ACC_SIGNUP
                )
                viewBinding.createProgressDialog.visibility = View.VISIBLE
                activity?.let {
                    viewModel.loginWithFaceBook(it)
                }
            } else {
                DialogFactory.internetDialog(
                    context,
                ) { _, _ -> }?.show()
            }
        }
    }


    /**
     * sign in without account
     */
    private fun loginWithoutAccount() {
        viewBinding.btnContinueWithoutAccount.setOnClickListener {

            /**
             * show bottom sheet process warning without account
             */
            (activity as LoginActivity).showFragmentWarningCreateGuestAccount(isFromCreateAccount = true)
        }

    }

    private fun observeSocialLogin() {
        viewModel.reActionLogin.observe(viewLifecycleOwner) { _ ->
            activity?.let {
                viewModel.reCallLogin(it)
            }
        }

        viewModel.createAccountSuccessful.observe(viewLifecycleOwner) { isLoginSuccess ->
            viewModel.createAccountSuccessful.removeObservers(viewLifecycleOwner)
            if (isLoginSuccess) {

                /**
                 * for sign up screen mean user already view term => no need show
                 */
                BreezeUserPreference.getInstance(requireContext()).saveTncStatus(true)
                viewModel.updateTncStatus()

                userPreferenceUtil.saveEmailGuestUser("")
                userPreferenceUtil.saveIsGuestMode(false)

                /**
                 * check if user not have username if not open create user name screen
                 */
                if (InitObjectUtilsController.currentObjectFlowLoginSignUp?.mode == MODE_LOGIN_TYPE.CREATE_ACCOUNT
                    && InitObjectUtilsController.currentObjectFlowLoginSignUp?.isUserNameAvailable == false
                ) {
                    /**
                     * open create user name screen
                     */
                    (activity as? LoginActivity)?.openCreateUserNameScreen(TermsAndCondnFragment.FROM_CREATE_ACCOUNT)
                } else {
                    (activity as? LoginActivity)?.showDialogSuccess(
                        isExistUser = true,
                        TermsAndCondnFragment.FROM_CREATE_ACCOUNT
                    )
                }
            }
        }

        viewModel.isShowProgess.observe(viewLifecycleOwner) { isShowProgress ->
            hideProgressBar(isShowProgress)
        }

        viewModel.errorThrow.observe(viewLifecycleOwner) {
            activity?.let {
                DialogUtilsBreeze.showDialogGetUserFails(it, retryAction = {
                    viewModel.restartSignUpFlow()
                }, cancel = {
                    viewBinding.progressBar.visibility = View.GONE
                    onBackPressed()
                })
            }
        }

        viewModel.createAccountFailed.observe(viewLifecycleOwner) {
            viewModel.createAccountFailed.removeObservers(viewLifecycleOwner)
            if (TextUtils.equals(it.errorMessage, "Username_Exists_Exception")) {
                val clickableSpan = object : ClickableSpan() {
                    override fun onClick(widget: View) {
                        Timber.d("Launch fragment")
                        Analytics.logClickEvent(Event.SIGNIN, getScreenName())
                    }

                    override fun updateDrawState(ds: TextPaint) {
                        super.updateDrawState(ds)
                        ds.isUnderlineText = true
                        ds.color = requireContext().resources.getColor(
                            R.color.edit_text_red,
                            null
                        )
                    }
                }
                val spannableString = SpannableString(getString(R.string.phone_number_in_use))
                spannableString.setSpan(
                    clickableSpan,
                    getString(R.string.phone_number_in_use).length - 8,
                    getString(R.string.phone_number_in_use).length,
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
                )
            }
        }
    }


    private fun hideProgressBar(isShowProgress: Boolean = true) {
        Utils.runOnMainThread {
            if (context != null) {
                if (isShowProgress) {
                    viewBinding.createProgressDialog.visibility = View.VISIBLE
                } else {
                    viewBinding.createProgressDialog.visibility = View.GONE
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        // In fragment class callback
//        if (requestCode == AWSCognitoAuthPlugin.WEB_UI_SIGN_IN_ACTIVITY_CODE) {
//            Amplify.Auth.handleWebUISignInResponse(data)
//        }
    }


    override fun getScreenName(): String {
        return Screen.ONBOARDING_CREATE_ACCOUNT_STEP1
    }

    override fun inflateViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?,
        attachToContainer: Boolean
    ) = FragmentSignUpFragmentBreezeBinding.inflate(inflater, container, attachToContainer)

    override fun getViewModelReference(): SignUpFragmentBreezeViewModel {
        return viewModel
    }

}
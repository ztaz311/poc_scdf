package com.ncs.breeze.ui.base

import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.ktx.Firebase
import com.google.firebase.remoteconfig.ktx.remoteConfig
import com.google.firebase.remoteconfig.ktx.remoteConfigSettings
import com.ncs.breeze.BuildConfig
import com.ncs.breeze.R

object RemoteConfigUtils {

    var updateCheckDurationHrs: Double? = null;
    var isDirectlyFromNotification: Boolean = false
    var forceUpdate: Boolean = false;

    fun init() {
        val configSettings = remoteConfigSettings {
            if (BuildConfig.FLAVOR.contains("prod")) {
                minimumFetchIntervalInSeconds = 300 //5 minutes
            } else {
                minimumFetchIntervalInSeconds = 0
            }
        }
        Firebase.remoteConfig.setConfigSettingsAsync(configSettings)
        Firebase.remoteConfig.setDefaultsAsync(R.xml.remote_config_defaults)
    }

    fun updateRemoteConfigValues() {
        Firebase.remoteConfig.fetch()
    }

    fun checkRemoteConfigValues(listener: ConfigLoadListener) {
        with(Firebase.remoteConfig) {
            //ToDo - do not pass hardcoded values in "key" here, access these values from 'res' instead
            val params = RemoteConfigParam(
                isMaintenance = getBoolean("is_maintenance_v2"),
                maintenanceMessage = getString("maintenance_message_v2"),
                optionalUpdateCheckDurationHrs = getDouble("optional_update_check_duration_hrs"),
                optionalUpdateCheckTrials = getLong("optional_update_check_trials").toInt(),
                minBuildVersion = getDouble("min_build_version"),
                latestBuildVersion = getDouble("latest_build_version"),
                downloadLink = getString("download_link"),
                mandatoryRelogin = getBoolean("mandatory_relogin")
            )
            updateCheckDurationHrs = params.optionalUpdateCheckDurationHrs
            listener.onConfigLoadSuccess(params)
        }
    }

    fun fetchConfigurations(activity: AppCompatActivity, listener: ConfigLoadListener) {
        Firebase.remoteConfig.fetchAndActivate()
            .addOnCompleteListener(activity) { task ->
                if (task.isSuccessful) {
                    checkRemoteConfigValues(listener)
                } else {
                    listener.onConfigLoadFailed()
                }
            }
    }

    interface ConfigLoadListener {
        fun onConfigLoadSuccess(data: RemoteConfigParam)
        fun onConfigLoadFailed()
    }
}

data class RemoteConfigParam(
    val isMaintenance: Boolean,
    val maintenanceMessage: String,
    val optionalUpdateCheckDurationHrs: Double,
    val optionalUpdateCheckTrials: Int,
    val minBuildVersion: Double,
    val latestBuildVersion: Double,
    val downloadLink: String,
    val mandatoryRelogin: Boolean,
)
package com.ncs.breeze.di.module.fragment

import com.ncs.breeze.ui.dashboard.fragments.sharedcollection.SharedCollectionFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class SharedCollectionModule {
    @ContributesAndroidInjector
    abstract fun contributeSharedCollection(): SharedCollectionFragment
}
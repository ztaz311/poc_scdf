package com.ncs.breeze.ui.login.fragment.signUp.view

import android.Manifest
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.text.Editable
import android.text.Spanned
import android.text.TextUtils
import android.text.TextWatcher
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.core.text.set
import androidx.core.text.toSpannable
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import com.ncs.breeze.R
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.Event
import com.ncs.breeze.common.analytics.Screen
import com.breeze.model.constants.Constants
import com.ncs.breeze.common.utils.DialogFactory
import com.ncs.breeze.common.utils.Utils
import com.ncs.breeze.common.utils.Variables
import com.breeze.customization.view.BreezeEditText
import com.ncs.breeze.databinding.FragmentSignInMobileBreezeBinding
import com.ncs.breeze.ui.base.BaseFragment
import com.ncs.breeze.ui.login.activity.LoginActivity
import com.ncs.breeze.ui.login.fragment.signIn.view.VerifyMobileFragment
import com.ncs.breeze.ui.login.fragment.signUp.viewmodel.LoginPhoneNumberFragmentBreezeViewModel
import com.ncs.breeze.ui.splash.PermissionActivity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.Locale
import javax.inject.Inject


class LoginPhoneNumberFragmentBreeze :
    BaseFragment<FragmentSignInMobileBreezeBinding, LoginPhoneNumberFragmentBreezeViewModel>() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val viewModel: LoginPhoneNumberFragmentBreezeViewModel by viewModels {
        viewModelFactory
    }

    lateinit var dialog: Dialog
    var isErrorViewShown: Boolean = false
    var currentFocusedView: View? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        permissionCheckBeforeSignUp()
        initViews()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            Constants.REQUEST_CODES.REQUEST_PERMISSION_SIGN_IN_MAIN -> {
                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Analytics.logClosePopupEvent(Event.PHOTO_MEDIA_ALLOW, getScreenName())
                    Analytics.logClosePopupEvent(Event.LOCATION_PERMISSION_ALLOW, getScreenName())
                    val intent = Intent(activity, PermissionActivity::class.java)
                    intent.putExtra(Constants.PERMISSION_FROM_SIGNIN, true)
                    startActivity(intent)
                } else {
                    Analytics.logClosePopupEvent(Event.PHOTO_MEDIA_DENY, getScreenName())
                    Analytics.logClosePopupEvent(Event.LOCATION_PERMISSION_DENY, getScreenName())
                }
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    private fun initViews() {
        setBottomText()

        viewBinding.editPhonenumber.getEditText()
            .setOnEditorActionListener(TextView.OnEditorActionListener { v, actionId, event ->
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    performCreateAccountClick()
                    return@OnEditorActionListener true
                }
                false
            })

        viewBinding.editPhonenumber.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                setNormalBg()
            }

            override fun afterTextChanged(s: Editable?) {

            }
        })

        viewBinding.editPhonenumber.setOnClickListener(object : View.OnClickListener {
            private var isAnalyticsEventLogged = false
            override fun onClick(v: View?) {
                if (!isAnalyticsEventLogged) {
                    Analytics.logClickEvent(Event.MOBILE_INPUT, getScreenName())
                }
                isAnalyticsEventLogged = true
            }
        })

        viewBinding.editPhonenumber.setEditTextListener(object :
            BreezeEditText.BreezeEditTextListener {
            override fun onBreezeEditTextFocusChange(isFocused: Boolean) {
                if (isFocused) {
                    editTextFocused()
                    currentFocusedView = viewBinding.editPhonenumber
                } else {
                    if (!viewBinding.editPhonenumber.getEnteredText().isNullOrBlank()) {
                        Analytics.logEditEvent(Event.PHONE, getScreenName())
                        if (!viewBinding.editPhonenumber.getEnteredText()
                                .matches(Regex(Constants.REGEX.PHONE_REGEX))
                        ) {
                            viewBinding.editPhonenumber.showErrorView(resources.getString(R.string.error_account_phone_nmber))
                        } else {
                            viewBinding.editPhonenumber.hideErrorView()
                        }
                    }
                }
            }
        })
        (activity as LoginActivity).showKeyboard(viewBinding.editPhonenumber.getEditText())
        clickListeners()
    }

    private fun performCreateAccountClick() {
        if (validate()) {
            Analytics.logClickEvent(Event.NEXT, getScreenName())
            viewBinding.editPhonenumber.clearFocus()
            if (Variables.isNetworkConnected) {
                startSignInWithPhoneNumber()
            } else {
                DialogFactory.internetDialog(
                    context,
                ) { _, i -> }!!.show()
            }
        }
    }

    private fun setNormalBg() {
        if (viewBinding.editPhonenumber.getEnteredText().length > 7
        ) {
            Analytics.logClickEvent(Event.AGREED_RADIO, getScreenName())
        }
    }

    private fun permissionCheckBeforeSignUp() {
        if (!Utils.checkLocationPermission(requireContext())) {
            Analytics.logOpenPopupEvent(Event.PHOTO_MEDIA_PERMISSION, getScreenName())
            Analytics.logOpenPopupEvent(Event.LOCATION_PERMISSION, getScreenName())
            requestPermissions(
                arrayOf(
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                ), Constants.REQUEST_CODES.REQUEST_PERMISSION_SIGN_IN_MAIN
            )
        }
    }


    /**
     * start signin with phone number
     */
    private fun startSignInWithPhoneNumber() {
        /**
         * handle when login phone number success should open verify screen
         */
        viewModel.signInSuccessful.observe(viewLifecycleOwner) {
            viewBinding.createProgressDialog.visibility = View.GONE
            viewModel.signInSuccessful.removeObservers(viewLifecycleOwner)
            if (it) {
                val bundle = Bundle()
                bundle.putString(
                    Constants.KEYS.KEY_PHONENUMBER,
                    Utils.appendCountryCode(viewBinding.editPhonenumber.getEnteredText())
                )
                bundle.putString(
                    Constants.KEYS.KEY_PASSWORD,
                    Constants.AMPLIFY_CONSTANTS.AMPLIFY_PASSWORD
                )
                bundle.putBoolean(Constants.KEYS.KEY_USER_NOT_CONFIRMED, false)
                bundle.putBoolean(VerifyMobileFragment.IS_USER_FROM_LOGIN, false)
                (activity as? LoginActivity)?.openVerifyOTPBreezeScreen(bundle)
            }
        }


        viewModel.signInSkip.observe(viewLifecycleOwner) {
            viewModel.signInSkip.removeObservers(viewLifecycleOwner)
            CoroutineScope(Dispatchers.Main).launch {
                viewBinding.createProgressDialog.visibility = View.GONE

                /**
                 * should call fetch user attr
                 */
                (activity as? LoginActivity)?.showDialogSuccess(
                    isExistUser = true,
                    fromScreen = TermsAndCondnFragment.FROM_SIGN_IN
                )
            }
        }

        viewModel.signInFailed.observe(viewLifecycleOwner) {
            viewBinding.createProgressDialog.visibility = View.GONE
            isErrorViewShown = true

            viewBinding.editPhonenumber.setBGError()
            viewBinding.editPhonenumber.showErrorView(it.errorMessage)
            viewModel.signInFailed.removeObservers(viewLifecycleOwner)
        }

        viewModel.userNotConfirmed.observe(viewLifecycleOwner) {
            viewBinding.createProgressDialog.visibility = View.GONE
            viewModel.userNotConfirmed.removeObservers(viewLifecycleOwner)
            val bundle = Bundle()
            bundle.putString(
                Constants.KEYS.KEY_PHONENUMBER,
                Utils.appendCountryCode(viewBinding.editPhonenumber.getEnteredText())
            )
            bundle.putString(
                Constants.KEYS.KEY_PASSWORD,
                Constants.AMPLIFY_CONSTANTS.AMPLIFY_PASSWORD
            )
            bundle.putBoolean(Constants.KEYS.KEY_USER_NOT_CONFIRMED, false)
            bundle.putBoolean(VerifyMobileFragment.IS_USER_FROM_LOGIN, false)
            (activity as? LoginActivity)?.openVerifyOTPBreezeScreen(bundle)
        }

        viewBinding.createProgressDialog.visibility = View.VISIBLE
        (activity as LoginActivity).hideKeyboard()

        /**
         * call signIn
         */
//        viewModel.signInMobilePhone(
//            Utils.appendCountryCode(
//                viewBinding.editPhonenumber.getEnteredText().lowercase(Locale.getDefault())
//            ),
//            Constants.AMPLIFY_CONSTANTS.AMPLIFY_PASSWORD
//        )

        viewModel.signInMobilePhone(
            Utils.appendCountryCode(
                viewBinding.editPhonenumber.getEnteredText().lowercase(Locale.getDefault())
            ),
            Constants.AMPLIFY_CONSTANTS.AMPLIFY_PASSWORD
        )
    }

    companion object {
        const val SCREEN_NAME = "LoginPhoneNumberFragmentBreeze"

        @JvmStatic
        fun newInstance() =
            LoginPhoneNumberFragmentBreeze().apply {
                arguments = Bundle().apply {}
            }
    }

    override fun inflateViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?,
        attachToContainer: Boolean
    ) = FragmentSignInMobileBreezeBinding.inflate(inflater, container, attachToContainer)

    override fun getViewModelReference(): LoginPhoneNumberFragmentBreezeViewModel {
        return viewModel
    }


    /**
     * verify phone number singapore
     */
    private fun validate(): Boolean {
        var isValid = true
        if (TextUtils.isEmpty(viewBinding.editPhonenumber.getEnteredText())) {
            isValid = false
            viewBinding.editPhonenumber.setBGError()
        } else {
            if (!viewBinding.editPhonenumber.getEnteredText()
                    .matches(Regex(Constants.REGEX.PHONE_REGEX))
            ) {
                isValid = false
                viewBinding.editPhonenumber.showErrorView(resources.getString(R.string.error_account_phone_nmber))
            }
        }
        if (!isValid) isErrorViewShown = true
        return isValid
    }

    private fun setBottomText() {
        val bottomText = resources.getString(R.string.bottom_text_create_account).toSpannable()
        bottomText[bottomText.length - 8 until bottomText.length] = object : ClickableSpan() {
            //set clickable span
            override fun onClick(view: View) {
                (activity as LoginActivity).removeAllFragmentsFromBackStack()
                Analytics.logClickEvent(Event.SIGNIN, getScreenName())
            }
        }
        bottomText.setSpan(
            ForegroundColorSpan(resources.getColor(R.color.text_black)),
            bottomText.length - 8,
            bottomText.length,
            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
        )
    }

    private fun clickListeners() {
        viewBinding.backButton.setOnClickListener {
            (activity as LoginActivity).hideKeyboard()
            onBackPressed()
        }
    }

    private fun hideProgressBar() {
        Utils.runOnMainThread {
            if (context != null) {
                viewBinding.createProgressDialog.visibility = View.GONE
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        // In fragment class callback
//        if (requestCode == AWSCognitoAuthPlugin.WEB_UI_SIGN_IN_ACTIVITY_CODE) {
//            Amplify.Auth.handleWebUISignInResponse(data)
//        }
    }

    fun editTextFocused() {
        if (isErrorViewShown) {
            viewBinding.editPhonenumber.hideErrorView()
            isErrorViewShown = false
        }
    }

    override fun getScreenName(): String {
        return Screen.ONBOARDING_SIGNIN
    }

}
package com.ncs.breeze.ui.dashboard.activity

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.breeze.model.constants.BreezeDynamicLink
import com.ncs.breeze.R
import com.ncs.breeze.common.extensions.android.getAppPreference
import com.ncs.breeze.common.storage.AppPrefsKey
import com.ncs.breeze.common.storage.BreezeAppPreference
import com.ncs.breeze.ui.splash.SplashActivity
import timber.log.Timber

class DeeplinkEntryActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Timber.d("On create is successfully called")
        handleNotification()
    }

    private fun handleNotification() {
        val uriParams: Uri? = intent.data
        if (uriParams != null) {
            when (uriParams.host) {
                getString(R.string.host_dynamic_link) -> {
                    handleSharedLocationData(uriParams)
                }
            }
        }
        finish()
    }

    private fun handleSharedLocationData(uriParams: Uri) {
        val pathSegments = uriParams.pathSegments
        val shareType = if(pathSegments.size > 0) pathSegments[0] else return


        val token = uriParams.getQueryParameter(BreezeDynamicLink.PARAM_TOKEN)?.takeIf {
            it.isNotEmpty()
        } ?: return

        val prefix = when (shareType) {
            "destination" -> BreezeAppPreference.PREFIX_LOCATION_DESTINATION
            "location" -> BreezeAppPreference.PREFIX_LOCATION
            else -> BreezeAppPreference.PREFIX_COLLECTION
        }
        getAppPreference()?.saveString(
            AppPrefsKey.KEY_SHARED_LOCATION_DATA_TOKEN,
            "$prefix$token"
        )
        Intent(this, SplashActivity::class.java)
            .apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            }
            .let {
                startActivity(it)
                finish()
            }
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        handleNotification()
    }

    private fun isApplicationRunning(): Boolean {
        return !isTaskRoot()
    }

    private fun startSplashActivity() {
        val intent = Intent(this, SplashActivity::class.java)
        startActivity(intent)
        finish()
    }

    companion object {
        private const val URL_SHARED_LOCATION_PATH = "/location/dev-index.html"
    }
}
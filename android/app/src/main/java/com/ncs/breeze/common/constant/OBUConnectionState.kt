package com.ncs.breeze.common.constant

import androidx.annotation.StringDef

@Retention(AnnotationRetention.SOURCE)
@StringDef("IDLE", "PAIRING", "CONNECTED")
annotation class OBUConnState

enum class OBUConnectionState(@OBUConnState val stateStr: String) {
    IDLE("IDLE"),
    PAIRING("PAIRING"),
    CONNECTED("CONNECTED"),
}
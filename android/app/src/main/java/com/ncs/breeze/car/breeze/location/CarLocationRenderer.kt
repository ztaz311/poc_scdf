package com.ncs.breeze.car.breeze.location

import com.mapbox.maps.extension.androidauto.MapboxCarMapSurface
import com.mapbox.maps.extension.androidauto.MapboxCarMapObserver
import com.mapbox.androidauto.internal.logAndroidAuto
import com.mapbox.androidauto.location.CarLocationProvider
import com.ncs.breeze.car.breeze.MainBreezeCarContext
import com.mapbox.maps.plugin.locationcomponent.LocationComponentPlugin
import com.mapbox.maps.plugin.locationcomponent.LocationProvider
import com.mapbox.maps.plugin.locationcomponent.location

/**
 * Create a simple 3d location puck. This class is demonstrating how to
 * create a renderer. To Create a new location experience, try creating a new class.
 */
class CarLocationRenderer(
    private val mainCarContext: MainBreezeCarContext
) : MapboxCarMapObserver {


   override fun onAttached(mapboxCarMapSurface: MapboxCarMapSurface) {
        logAndroidAuto("CarLocationRenderer carMapSurface loaded")
        mapboxCarMapSurface.mapSurface.location.apply {
            locationPuck = CarLocationPuck.navigationCruise2D(mainCarContext.carContext)
            enabled = true
            pulsingEnabled = true
//            setLocationProvider(MapboxCarApp.carAppServices.location().navigationLocationProvider)
        }

    }

    override fun onDetached(mapboxCarMapSurface: MapboxCarMapSurface) {
        logAndroidAuto("CarLocationRenderer carMapSurface detached")
    }
}

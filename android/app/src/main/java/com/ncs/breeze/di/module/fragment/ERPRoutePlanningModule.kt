package com.ncs.breeze.di.module.fragment

import com.ncs.breeze.ui.dashboard.fragments.view.ERPRoutePreviewFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class ERPRoutePlanningModule {
    @ContributesAndroidInjector
    abstract fun contributeERPRoutePlanningFragment(): ERPRoutePreviewFragment
}
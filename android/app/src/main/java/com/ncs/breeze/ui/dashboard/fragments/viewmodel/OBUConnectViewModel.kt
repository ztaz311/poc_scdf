package com.ncs.breeze.ui.dashboard.fragments.viewmodel

import android.app.Application
import com.breeze.model.obu.OBUInitialConnectionStep
import com.ncs.breeze.components.SingleLiveEvent
import com.ncs.breeze.ui.base.BaseFragmentViewModel
import sg.gov.lta.obu.sdk.conn.OBU
import javax.inject.Inject

class OBUConnectViewModel @Inject constructor(application: Application) :
    BaseFragmentViewModel(
        application
    ) {
    val obuInitialConnectionStep: SingleLiveEvent<OBUInitialConnectionStep> = SingleLiveEvent()
    val obuSearchState: SingleLiveEvent<Boolean> = SingleLiveEvent()
    var currentObuName: String? = null
    var currentObu: OBU? = null
}

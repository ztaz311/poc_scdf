package com.ncs.breeze.components.marker.markerview2

import android.app.Activity
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.view.isVisible
import com.breeze.model.api.response.amenities.BaseAmenity
import com.breeze.model.constants.Constants
import com.breeze.model.enums.AmenityBand
import com.breeze.model.extensions.dpToPx
import com.google.android.gms.maps.model.LatLng
import com.mapbox.geojson.Point
import com.mapbox.maps.EdgeInsets
import com.mapbox.maps.MapboxMap
import com.ncs.breeze.R
import com.ncs.breeze.car.breeze.utils.LimitClick
import com.ncs.breeze.common.utils.BreezeMapZoomUtil
import com.ncs.breeze.common.utils.LocationBreezeManager
import com.ncs.breeze.databinding.MapTooltipPoiExploreBinding
import com.ncs.breeze.ui.base.BaseActivity
import com.ncs.breeze.ui.dashboard.fragments.exploremap.profile.ProfileZoneLayerExplore

class ExplorePoiMarkerView constructor(
    latLng: LatLng,
    mapboxMap: MapboxMap,
    val activity: Activity,
    itemId: String? = null,
    itemType: String? = null,
    enableWalking: Boolean,
    enableZoneDrive: Boolean,
    private val toolTipZoomRange: Double = Constants.POI_TOOLTIP_ZOOM_RADIUS
) : MarkerView2(mapboxMap = mapboxMap, itemId) {
    private var currentAmenity: BaseAmenity? = null
    private val viewBinding = MapTooltipPoiExploreBinding.inflate(LayoutInflater.from(activity), null, false)

    init {
        mLatLng = latLng
        val isInNoZoneProfile =
            ProfileZoneLayerExplore.detectSelectedProfileLocationInZone(LocationBreezeManager.getInstance().currentLocation) == ProfileZoneLayerExplore.NO_ZONE

        viewBinding.walkHereButton.isVisible = enableWalking && !isInNoZoneProfile
        viewBinding.driveHereButton.isVisible = isInNoZoneProfile || enableZoneDrive
        mViewMarker = viewBinding.root
    }

    fun handleClickMarker(
        item: BaseAmenity,
        shouldRecenter: Boolean,
        walkHereButtonClick: (BaseAmenity, BaseActivity.EventSource) -> Unit,
        driveHereButtonClick: (BaseAmenity, BaseActivity.EventSource) -> Unit,
        amenitiesIconClick: (BaseAmenity, Boolean) -> Unit
    ) {
        currentAmenity = item
        viewBinding.textviewName.text = item.name ?: item.provider
        viewBinding.textviewAddress.text = item.address
        viewBinding.walkHereButton.setOnClickListener {
            LimitClick.handleSafe {
                resetView()
                walkHereButtonClick.invoke(item, BaseActivity.EventSource.NONE)
            }
        }
        displayCrowdsourceData(item)

        viewBinding.driveHereButton.setOnClickListener {
            LimitClick.handleSafe {
                resetView()
                driveHereButtonClick.invoke(item, BaseActivity.EventSource.NONE)
            }
        }

        amenitiesIconClick.invoke(item, true)
        renderMarkerToolTip(item, shouldRecenter)

    }

    private fun displayCrowdsourceData(amenity: BaseAmenity) {
        val availabilityCSData = amenity.availabilityCSData
        viewBinding.layoutCarparkStatus.root.isVisible = availabilityCSData != null
        if (availabilityCSData == null) return
        val drawableHeaderCpStatus =
            AppCompatResources.getDrawable(activity, R.drawable.bg_header_carpark_status)
        drawableHeaderCpStatus?.setTint(
            Color.parseColor(availabilityCSData.themeColor ?: "#F26415")
        )
        viewBinding.layoutCarparkStatus.root.background = drawableHeaderCpStatus
        viewBinding.layoutCarparkStatus.icCarparkStatus.setImageResource(
            when (amenity.availablePercentImageBand) {
                AmenityBand.TYPE0.type -> R.drawable.ic_cp_stt_full
                AmenityBand.TYPE2.type -> R.drawable.ic_cp_stt_available
                else -> R.drawable.ic_cp_stt_few
            }
        )
        viewBinding.layoutCarparkStatus.tvStatusCarpark.text = availabilityCSData.title ?: ""
        viewBinding.layoutCarparkStatus.tvTimeUpdateCarparkStatus.text =
            "${availabilityCSData?.primaryDesc ?: activity.getString(com.breeze.customization.R.string.update_carpark_stt_time)} ${availabilityCSData?.generateDisplayedLastUpdate()}"
    }

    private fun renderMarkerToolTip(item: BaseAmenity, shouldRecenter: Boolean) {
        updateTooltipPosition()
        if (!shouldRecenter) {
            viewBinding.root.bringToFront()
            return
        }

        BreezeMapZoomUtil.zoomViewToRange(mapboxMap,
            Point.fromLngLat(item.long!!, item.lat!!),
            toolTipZoomRange,
            EdgeInsets(10.0.dpToPx(), 10.0.dpToPx(), 200.0.dpToPx(), 10.0.dpToPx()),
            animationEndCB = {
                viewBinding.root.bringToFront()
            }
        )
    }

    override fun showMarker() {
        super.showMarker()
        viewBinding.root.visibility = View.VISIBLE
    }

    override fun hideMarker() {
        super.hideMarker()
        viewBinding.root.visibility = View.INVISIBLE
    }

    override fun updateTooltipPosition() {
        val mainView = mViewMarker ?: return
        val screenCoordinate = mLatLng?.let {
            mapboxMap.pixelForCoordinate(Point.fromLngLat(it.longitude, it.latitude))
        } ?: return
        mainView.measure(
            View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
            View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
        )
        mainView.x = screenCoordinate.x.toFloat() - (mainView.measuredWidth / 2)
        mainView.y = screenCoordinate.y.toFloat() - mainView.measuredHeight - 20.dpToPx()
    }
}
package com.ncs.breeze.car.breeze.screen.search.shortcuts

import androidx.car.app.model.Action
import androidx.car.app.model.CarIcon
import androidx.car.app.model.ItemList
import androidx.car.app.model.ListTemplate
import androidx.car.app.model.Row
import androidx.car.app.model.Template
import androidx.core.graphics.drawable.IconCompat
import com.mapbox.androidauto.internal.logAndroidAuto
import com.mapbox.geojson.Point
import com.ncs.breeze.R
import com.ncs.breeze.car.breeze.MainBreezeCarContext
import com.ncs.breeze.car.breeze.base.BaseScreenCar
import com.ncs.breeze.car.breeze.utils.CarConstants
import com.ncs.breeze.car.breeze.utils.LimitClick
import com.ncs.breeze.car.breeze.utils.ShortCutType
import com.ncs.breeze.car.breeze.utils.ShortCutTypes
import com.ncs.breeze.car.breeze.utils.setClickSafe
import com.ncs.breeze.common.analytics.Screen
import com.ncs.breeze.common.model.rx.AppToRoutePlanningStartEvent
import com.ncs.breeze.common.model.rx.AppToShortcutListScreenEvent
import com.ncs.breeze.common.model.rx.ToCarRxEvent
import com.ncs.breeze.common.model.rx.DataForRoutePreview
import com.ncs.breeze.common.model.rx.GetShortcutListFromCar
import com.ncs.breeze.common.event.ToAppRx.postEvent
import com.ncs.breeze.common.event.ToCarRx
import com.breeze.model.SearchLocation
import com.breeze.model.extensions.safeDouble
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ShortcutListScreen(val mainCarContext: MainBreezeCarContext) :
    BaseScreenCar(mainCarContext.carContext) {
    var isLoading = true
    private var normaShortcuts = arrayListOf<SearchLocation>()
    private var home: SearchLocation? = null
    private var work: SearchLocation? = null

    override fun onResumeScreen() {
        super.onResumeScreen()
        logAndroidAuto("onResumeScreen ShortcutListScreen ")
        postEvent(GetShortcutListFromCar())
    }

    override fun onPauseScreen() {
        super.onPauseScreen()
        logAndroidAuto("onPauseScreen ShortcutListScreen ")
    }

    override fun getScreenName(): String {
        return Screen.AA_SHORTCUT_LIST_SCREEN
    }

    override fun onGetTemplate(): Template {
        return buildTemplate()
    }

    override fun onReceiverEventFromApp(event: ToCarRxEvent?) {
        super.onReceiverEventFromApp(event)
        when (event) {
            is AppToShortcutListScreenEvent -> {
                home = event.data.home
                work = event.data.work
                normaShortcuts.clear()
                normaShortcuts.addAll(event.data.normalShortcuts)
                isLoading = false
                invalidate()
            }
            else -> {}
        }
    }

    private fun buildTemplate(): Template {
        val templateBuilder = ListTemplate.Builder()
            .setLoading(isLoading)
            .setTitle(carContext.resources.getString(R.string.shortcuts))
            .setHeaderAction(Action.BACK)
        if (!isLoading) {
            templateBuilder.setSingleList(buildList())
        }
        return templateBuilder.build()
    }

    private fun buildList(): ItemList {
        val listBuilder = ItemList.Builder()
        home?.let {
            listBuilder.addItem(createItemBuilder(it, ShortCutTypes.HOME).build())
        }
        work?.let {
            listBuilder.addItem(createItemBuilder(it, ShortCutTypes.WORK).build())
        }
        normaShortcuts.forEach { data ->
            listBuilder.addItem(createItemBuilder(data, ShortCutTypes.NORMAL).build())
        }

        if (normaShortcuts.isEmpty() && home == null && work == null)
            listBuilder.setNoItemsMessage(carContext.getString(R.string.shortcut_list_no_data))
        return listBuilder.build()
    }

    private fun createItemBuilder(location: SearchLocation, @ShortCutType type: String) =
        Row.Builder()
            .setTitle(getShortcutTitle(location, type))
            .addText(location.address1!!)
            .setImage(getShortcutIcon(type), Row.IMAGE_TYPE_LARGE)
            .setClickSafe {
                LimitClick.handleSafe {
                    onSelectLocation(location)
                }
            }

    private fun onSelectLocation(location: SearchLocation) {
        val originalDestination = location.convertToDestinationAddressDetails()
        screenManager.popTo(CarConstants.HOME_MARKER)
        CoroutineScope(Dispatchers.Main).launch {
            ToCarRx.postEventWithCacheLast(
                AppToRoutePlanningStartEvent(
                    DataForRoutePreview(
                        selectedDestination = Point.fromLngLat(
                            location.long!!.safeDouble(),
                            location.lat!!.safeDouble(),
                        ),
                        originalDestination = originalDestination,
                        destinationSearchLocation = location
                    )
                )
            )
        }
    }

    private fun getShortcutTitle(location: SearchLocation, @ShortCutType type: String) =
        when (type) {
            ShortCutTypes.HOME -> "Home"
            ShortCutTypes.WORK -> "Work"
            else -> location.name!!
        }

    private fun getShortcutIcon(@ShortCutType type: String) = CarIcon.Builder(
        IconCompat.createWithResource(
            carContext,
            when (type) {
                ShortCutTypes.HOME -> R.drawable.ic_car_item_home
                ShortCutTypes.WORK -> R.drawable.ic_car_item_work
                else -> R.drawable.ic_shortcut_normal_car
            }
        )
    ).build()
}
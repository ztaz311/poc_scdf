package com.ncs.breeze.ui.dashboard.fragments.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.Event
import com.ncs.breeze.common.analytics.Screen
import com.breeze.model.enums.RoutePreference
import com.ncs.breeze.common.utils.BreezeUserPreferenceUtil
import com.ncs.breeze.databinding.FragmentRoutePrefBinding
import com.ncs.breeze.ui.base.BaseFragment
import com.ncs.breeze.ui.dashboard.activity.DashboardActivity
import com.ncs.breeze.ui.dashboard.fragments.viewmodel.RoutePrefFragmentViewModel
import javax.inject.Inject

class RoutePreferenceFragment :
    BaseFragment<FragmentRoutePrefBinding, RoutePrefFragmentViewModel>() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val viewModel: RoutePrefFragmentViewModel by viewModels {
        viewModelFactory
    }

    @Inject
    lateinit var userPreferenceUtil: BreezeUserPreferenceUtil

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initialize()
    }

    private fun initialize() {
        viewBinding.backButton.setOnClickListener {
            onBackPressed()
        }

        // Show Route Preference
        var routePreference = userPreferenceUtil.getRoutePreference()
        if (routePreference == RoutePreference.CHEAPEST_ROUTE.type) {
            viewBinding.imgCheapestChecked.visibility = View.VISIBLE
        } else {
            viewBinding.imgFastestChecked.visibility = View.VISIBLE
        }

        viewBinding.rlFastest.setOnClickListener {
            Analytics.logClickEvent(Event.ROUTE_PREF_FASTEST_CLICK, getScreenName())
            setUserSettings(RoutePreference.FASTEST_ROUTE)

            viewBinding.imgFastestChecked.visibility = View.VISIBLE
            viewBinding.imgCheapestChecked.visibility = View.GONE
        }

        viewBinding.rlCheapest.setOnClickListener {
            Analytics.logClickEvent(Event.ROUTE_PREF_CHEAPEST_CLICK, getScreenName())
            setUserSettings(RoutePreference.CHEAPEST_ROUTE)

            viewBinding.imgFastestChecked.visibility = View.GONE
            viewBinding.imgCheapestChecked.visibility = View.VISIBLE
        }

    }

    override fun inflateViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?,
        attachToContainer: Boolean
    ) = FragmentRoutePrefBinding.inflate(inflater, container, attachToContainer)

    override fun getViewModelReference(): RoutePrefFragmentViewModel {
        return viewModel
    }

    fun setUserSettings(routePref: RoutePreference) {
        userPreferenceUtil.saveUserRoutePreference(routePref)
        viewModel.saveUserSettings(userPreferenceUtil.getUserThemePreference(), routePref.type)
        (requireActivity() as DashboardActivity).refreshRoutePreferenceInSettings()
    }

    override fun getScreenName(): String {
        return Screen.SETTINGS_ROUTE_PREFERENCE
    }

}
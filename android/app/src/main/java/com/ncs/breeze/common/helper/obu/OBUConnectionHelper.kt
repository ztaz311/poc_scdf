package com.ncs.breeze.common.helper.obu

import android.content.pm.PackageManager
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.ncs.breeze.App
import com.ncs.breeze.BuildConfig
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.Event
import com.ncs.breeze.common.analytics.Param
import com.ncs.breeze.common.analytics.Screen
import com.ncs.breeze.common.constant.OBUConnectionState
import com.ncs.breeze.common.constant.OBUConstants
import com.ncs.breeze.common.event.ToCarRx
import com.ncs.breeze.common.extensions.android.getUserDataPreference
import com.ncs.breeze.common.helper.obu.callback.OBUConnectionStatusListenerKt
import com.ncs.breeze.common.helper.obu.callback.OBUInitListenerInitializer
import com.ncs.breeze.common.helper.obu.callback.OBUInitListenerKt
import com.ncs.breeze.common.helper.obu.impl.OBUDataListenerImpl
import com.breeze.model.obu.OBUDevice
import com.breeze.model.api.request.OBUTransaction
import com.breeze.model.constants.TripOBUStatus
import com.ncs.breeze.common.model.rx.OBUStateChangedEvent
import com.ncs.breeze.common.model.rx.TriggerCarToastEvent
import com.ncs.breeze.common.rxbus.RxBus
import com.ncs.breeze.common.rxbus.RxEvent
import com.ncs.breeze.common.utils.TripLoggingManager
import com.ncs.breeze.ui.base.BaseActivity
import com.ncs.breeze.ui.navigation.TurnByTurnNavigationActivity
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import sg.gov.lta.obu.sdk.conn.OBU
import sg.gov.lta.obu.sdk.core.enums.OBUCardStatus
import sg.gov.lta.obu.sdk.core.enums.OBUChargingPaymentMode
import sg.gov.lta.obu.sdk.core.enums.OBUPaymentMode
import sg.gov.lta.obu.sdk.data.interfaces.OBUConnectionStatusListener
import sg.gov.lta.obu.sdk.data.services.OBUSDK
import timber.log.Timber
import java.lang.ref.WeakReference


class OBUConnectionHelper(app: App) {
    private val appRef = WeakReference(app)
    private val obuConnectionStatusObservers = mutableSetOf<WeakReference<BaseActivity<*, *>>>()
    val obuConnectionState = MutableLiveData(OBUConnectionState.IDLE)
    var currentOBUDevice: OBUDevice? = null
    private var lastOBUDevice: OBUDevice? = null
    private var lastOBU: OBU? = null
    private var currentOBU: OBU? = null
    private var updateRNConnectionStateObserver: Observer<OBUConnectionState>? = null
    private var activeOBUConnectionListener: OBUConnectionStatusListenerKt? = null
    private var isManualDisconnect = false
    fun startSyncOBUConnectionToRN() {
        if (updateRNConnectionStateObserver != null)
            return
        Observer<OBUConnectionState> { state ->

            val device = when (state) {
                OBUConnectionState.CONNECTED -> {
                    appRef.get()?.getUserDataPreference()
                        ?.getConnectedOBUDevices()
                        ?.takeIf { it.isNotEmpty() }
                        ?.last()
                        ?.also { d ->
                            TripLoggingManager.getInstance()?.run {
                                changeCurrentOBUName(d.obuName)
                                updateTripOBUStatus(TripOBUStatus.CONNECTED)
                            }
                        }

                }

                OBUConnectionState.PAIRING -> {
                    currentOBUDevice
                }

                else -> null
            }

            appRef.get()?.shareBusinessLogicHelper?.updateOBUConnectionState(state, device)
        }.let {
            obuConnectionState.observeForever(it)
            updateRNConnectionStateObserver = it
        }
    }

    fun registerConnectionStatusListener(activity: BaseActivity<*, *>) {
        Timber.e("obu - registerConnectionStatusListener $activity")
        obuConnectionStatusObservers.add(WeakReference(activity))
    }

    fun unregisterConnectionStatusListener(activity: BaseActivity<*, *>) {
        Timber.e("obu - unregisterConnectionStatusListener $activity")
        obuConnectionStatusObservers.removeIf { it.get() == activity }
    }

    fun initOBU(listenerInitializer: OBUInitListenerInitializer) {
        val initListener = OBUInitListenerKt(appRef.get())
        listenerInitializer.invoke(initListener)
        if (OBUSDK.isInitialized) {
            initListener.onSuccess()
        } else {
            appRef.get()?.let { app ->
                obuConnectionState.postValue(OBUConnectionState.IDLE)
                OBUSDK.init(app, BuildConfig.OBU_SDK_KEY, initListener)
            }
        }
    }

    private fun initMockManager() {
        if (OBUSDK.isConnectionActive()) return
        OBUSDK.enableMockData(OBUMockDataFactory.generateMockSequence())
    }

    fun startOBUConnection(
        vehicleNumber: String,
        obu: OBU,
        isRetrying: Boolean = false,
        connectionListener: OBUConnectionStatusListener? = null
    ) {
        Analytics.logEvent(Event.OBU_SYSTEM_EVENT, Event.OBU_SYSTEM_CONNECTING, Screen.OBU_APP)
        Timber.d("startOBUConnection: currentOBU ${currentOBU?.name}, isConnectionActive = ${OBUSDK.isConnectionActive()} , isOBUAutoConnect = $isOBUAutoConnect")
        if (OBUSDK.isConnectionActive()) {
            OBUSDK.disconnect()
            GlobalScope.launch {
                delay(1000)
                startOBUConnection(
                    vehicleNumber,
                    obu,
                    isRetrying,
                    connectionListener
                )
            }
            return
        }
        currentOBU = obu
        currentOBUDevice = OBUDevice(obu.name, vehicleNumber)
        obuConnectionState.postValue(OBUConnectionState.PAIRING)
        Timber.d("startOBUConnection: activeOBUConnectionListener = $activeOBUConnectionListener")
        activeOBUConnectionListener = OBUConnectionStatusListenerKt(
            context = appRef.get(),
            vehicleNumber = vehicleNumber,
            onOBUConnected = { device ->
                Analytics.logEvent(
                    Event.OBU_SYSTEM_EVENT,
                    Event.OBU_SYSTEM_CONNECTED,
                    Screen.OBU_APP
                )
                OBUDataHelper.isFirstOBUCardValidArrived = false
                saveConnectedOBUDevice(vehicleNumber, obu.name)
                // Send payment histories once time when connect with OBU successfully
                sendPaymentHistories()
                startOBUDataListener()
                obuConnectionState.postValue(OBUConnectionState.CONNECTED)
                ToCarRx.postEvent(TriggerCarToastEvent.create("OBU Connected"))
                ToCarRx.postEvent(OBUStateChangedEvent(true))
                connectionListener?.onOBUConnected(device)
                isManualDisconnect = false
                lastCardStatus = null
                lastPaymentMode = null
            },
            onOBUConnectionFailure = { error ->
                Analytics.logEvent(
                    Event.OBU_SYSTEM_EVENT,
                    Event.OBU_SYSTEM_CONNECTION_FAILED,
                    Screen.OBU_APP
                )
                Analytics.logEvent(
                    Event.OBU_SYSTEM_EVENT,
                    bundleOf(
                        Param.KEY_EVENT_VALUE to Event.OBU_VERBOSE_ERROR,
                        Param.KEY_MESSAGE to "Code: ${error.code}, message: ${error.localizedMessage}",
                    )
                )
                obuConnectionStatusObservers.forEach {
                    val activity = it.get()
                    if (activity is TurnByTurnNavigationActivity) {
                        activity.showOBUConnectionError()
                    } else if (activity?.lifecycle?.currentState?.isAtLeast(Lifecycle.State.STARTED) == true)
                        activity.obuAlertHelper.showErrorAlert(error, isRetrying)
                }
                obuConnectionState.postValue(OBUConnectionState.IDLE)
                ToCarRx.postEvent(TriggerCarToastEvent.create("OBU not Connected"))
                activeOBUConnectionListener = null
//                currentOBUDevice = null
                connectionListener?.onOBUConnectionFailure(error)
                isManualDisconnect = false
                ToCarRx.postEvent(OBUStateChangedEvent(false))
            },
            onOBUDisconnected = { device, error ->
                Analytics.logEvent(
                    Event.OBU_SYSTEM_EVENT,
                    Event.OBU_SYSTEM_DISCONNECTED,
                    Screen.OBU_APP
                )
                Analytics.logEvent(
                    Event.OBU_SYSTEM_EVENT,
                    bundleOf(
                        Param.KEY_EVENT_VALUE to Event.OBU_VERBOSE_ERROR,
                        Param.KEY_MESSAGE to "Code: ${error?.code}, message: ${error?.localizedMessage}",
                    )
                )
                ToCarRx.postEvent(OBUStateChangedEvent(false))
                obuConnectionState.postValue(OBUConnectionState.IDLE)
                if (error != null && !isManualDisconnect) {
                    obuConnectionStatusObservers.forEach {

                        val activity = it.get()
                        if (activity is TurnByTurnNavigationActivity) {
                            activity.showOBUConnectionError()
                        } else if (activity?.lifecycle?.currentState?.isAtLeast(Lifecycle.State.STARTED) == true)
                            activity.obuAlertHelper.showErrorAlert(error)
                    }
                }
                TripLoggingManager.getInstance()?.run {
                    updateTripOBUStatus(TripOBUStatus.NOT_CONNECTED)
                }
                activeOBUConnectionListener = null
                lastOBUDevice = currentOBUDevice
                currentOBUDevice = null
                lastOBU = currentOBU
                currentOBU = null

                connectionListener?.onOBUDisconnected(device, error)
                isManualDisconnect = false
            }
        ).also { listener ->
            OBUSDK.connect(obu, listener)
        }
    }

    fun autoConnectLastOBUDevice() {
        Timber.e("autoConnectLastOBUDevice: $isOBUAutoConnect")
        if (!isOBUAutoConnect) {
            ToCarRx.postEventWithCacheLast(TriggerCarToastEvent.create("OBU not Connected"))
            return
        }
        if (obuConnectionState.value == OBUConnectionState.PAIRING || !OBUSDK.isInitialized)
            return
        if (OBUSDK.isConnectionActive()) {
            obuConnectionState.postValue(OBUConnectionState.CONNECTED)
            return
        }
        val lastConnectedDevice = appRef.get()?.getUserDataPreference()?.getConnectedOBUDevices()
            ?.takeIf {
                it.isNotEmpty()
            }?.last()
        Timber.d("autoConnectLastOBUDevice: lastDevice = $lastConnectedDevice")
        lastConnectedDevice?.let { device ->
            val obuPairedDevices = OBUSDK.getPairedDevices()
            val obu = if (isOBUSimulationEnabled) {
                OBU(null, device.obuName)
            } else obuPairedDevices?.find { it.name == device.obuName }
            if (obu != null)
                startOBUConnection(device.vehicleNumber, obu)
        }
    }

    fun navigationConnectLastOBUDevice() {
        val lastConnectedDevice = appRef.get()?.getUserDataPreference()?.getConnectedOBUDevices()
            ?.takeIf {
                it.isNotEmpty()
            }?.last()
        Timber.d("autoConnectLastOBUDevice: lastDevice = $lastConnectedDevice")
        lastConnectedDevice?.let { device ->
            val obuPairedDevices = OBUSDK.getPairedDevices()
            val obu = if (isOBUSimulationEnabled) {
                OBU(null, device.obuName)
            } else obuPairedDevices?.find { it.name == device.obuName }
            if (obu != null)
                startOBUConnection(device.vehicleNumber, obu)
        }
    }

    fun retryConnect() {
        Timber.d("retryConnect OBU: current currentOBUDevice = $currentOBUDevice - $lastOBUDevice")
        currentOBUDevice?.run {
            startOBUConnection(vehicleNumber, currentOBU!!, true)
        } ?: lastOBUDevice?.run { startOBUConnection(vehicleNumber, lastOBU!!, true) }

    }

    fun getUserConnectedOBUDevices(): Array<OBUDevice>? {
        return appRef.get()?.getUserDataPreference()?.getConnectedOBUDevices()
    }

    fun removePairedOBUDevice(vehicleNumber: String, obuName: String) {
        val userDataPref = appRef.get()?.getUserDataPreference()
        userDataPref?.getConnectedOBUDevices()
            ?.takeIf { it.isNotEmpty() }?.let { listPaired ->
                if (obuConnectionState.value == OBUConnectionState.CONNECTED) {
                    val obuDevice = listPaired.last()
                    if (obuDevice.obuName == obuName) {
                        OBUSDK.disconnect()
                    }
                }

                val devices =
                    listPaired.filterNot { it.obuName == obuName }
                userDataPref.saveConnectedOBUDevice(devices)
                appRef.get()?.shareBusinessLogicHelper?.updatePairedVehicleList(devices.toTypedArray())
            }
    }

    private fun saveConnectedOBUDevice(vehicleNumber: String, obuName: String) {
        val currentUserId =
            appRef.get()?.userPreferenceUtil?.getBreezeUserPreference()?.getUserId()
        val device = OBUDevice(
            obuName = obuName,
            vehicleNumber = vehicleNumber,
            userId = currentUserId
        )
        appRef.get()?.getUserDataPreference()?.saveConnectedOBUDevice(device)
    }

    private var lastCardStatus: OBUCardStatus? = null
    private var lastPaymentMode: OBUPaymentMode? = null
    private fun startOBUDataListener() {
        OBUSDK.setDataListener(object : OBUDataListenerImpl() {
            override fun onCardInformation(
                status: OBUCardStatus?,
                paymentMode: OBUPaymentMode?,
                chargingPaymentMode: OBUChargingPaymentMode?,
                balance: Int?
            ) {
                super.onCardInformation(status, paymentMode, chargingPaymentMode, balance)
                handleCardInformation(status, paymentMode, chargingPaymentMode, balance)
            }
        })
    }

    internal fun handleCardInformation(
        status: OBUCardStatus?,
        paymentMode: OBUPaymentMode?,
        chargingPaymentMode: OBUChargingPaymentMode?,
        balance: Int?
    ) {
        Timber.d("OBUConnectionHelper - onCardInformation")
        if (lastCardStatus != status || lastPaymentMode != paymentMode) {
            status?.let {
                Analytics.logEvent(
                    Event.OBU_SYSTEM_EVENT,
                    Event.OBU_CARD_STATUS,
                    Screen.OBU_APP,
                    bundleOf(
                        Param.KEY_MESSAGE to it.name
                    )
                )
            }

            if (lastCardStatus == null || lastCardStatus != status || lastPaymentMode != paymentMode) {
                if (status !in arrayOf(
                        OBUCardStatus.Detected,
                        OBUCardStatus.Invalid,
                        OBUCardStatus.InsufficientStoredValue,
                        OBUCardStatus.ExpiredStored
                    )
                    /*&& paymentMode != OBUPaymentMode.Backend*///todo remove logic check bank card to show no card error
                ) {
                    obuConnectionStatusObservers.forEach {
                        val activity = it.get()
                        activity?.obuAlertHelper?.showNoCardError()
                    }
                }
            }
            lastCardStatus = status
            lastPaymentMode = paymentMode
        }
    }

    fun hasActiveOBUConnection() =
        OBUSDK.isConnectionActive()

    fun disconnectOBU() {
        Timber.d("disconnectOBU: connectionState = ${obuConnectionState.value}")
        isManualDisconnect = true
        if (obuConnectionState.value == OBUConnectionState.PAIRING) {
            // Still connecting, add flag to disconnect after successful connection
            activeOBUConnectionListener?.isUserCanceled = true
        } else {
            OBUSDK.disconnect()
        }
        obuConnectionState.postValue(OBUConnectionState.IDLE)
        Timber.d("disconnectOBU: end of function haveActiveConnection = ${OBUSDK.isConnectionActive()}")
    }

    fun sendPaymentHistories() {
        OBUSDK.getPaymentHistories()?.map {
            OBUTransaction(
                it.sequentialNumber,
                it.businessFunction.name,
                it.paymentDate,
                it.paymentMode.name,
                it.chargeAmount
            )
        }?.let {
            RxBus.publish(RxEvent.OBUPaymentHistoriesEvent(it))
        }
    }

    fun toggleOBUConnectionRN(isConnecting: Boolean, vehicleNumber: String, obuName: String) {
        if (hasActiveOBUConnection())
            disconnectOBU()
        Timber.d("toggleOBUConnectionRN: haveActiveConnection = ${OBUSDK.isConnectionActive()}, isConnecting = $isConnecting, vehicleNumber = $vehicleNumber, obuName = $obuName")
        if (!isConnecting) {
            val obuPairedDevices = OBUSDK.getPairedDevices()
            val obu = if (isOBUSimulationEnabled) {
                OBU(null, obuName)
            } else obuPairedDevices?.find { it.name == obuName }
            Timber.d("toggleOBUConnectionRN: connecting to obu = ${obu?.name}")
            if (obu != null)
                startOBUConnection(vehicleNumber, obu)
        }
    }

    fun enableOBUSimulation() {
        initMockManager()
    }

    fun disableOBUSimulation(onDataCleared: () -> Unit) {
        OBUSDK.disconnect()

        appRef.get()?.run {
            shareBusinessLogicHelper.updatePairedVehicleList(arrayOf())
            getUserDataPreference()?.saveConnectedOBUDevice(arrayListOf())
            obuConnectionState.postValue(OBUConnectionState.IDLE)
        }
        onDataCleared.invoke()
    }

    fun checkOBUIsAlreadyPaired(obuName: String): Boolean {
        return appRef.get()?.getUserDataPreference()?.getConnectedOBUDevices()
            ?.find { it.obuName == obuName } != null
    }

    fun enableAutoConnect(enabled: Boolean) {
        isOBUAutoConnect = enabled
    }

    fun getCurrentOBUStatus(): String {
        if (!hasPairDevice())
            return TripOBUStatus.NOT_PAIRED

        return if (hasActiveOBUConnection()) TripOBUStatus.CONNECTED else TripOBUStatus.NOT_CONNECTED
    }

    private fun hasPairDevice(): Boolean {
        return appRef.get()?.let { app ->
            if (isOBUSimulationEnabled) {
                !appRef.get()
                    ?.getUserDataPreference()
                    ?.getConnectedOBUDevices()
                    .isNullOrEmpty()
            } else if (OBUConstants.OBU_PERMISSIONS.all {
                    ContextCompat.checkSelfPermission(
                        app,
                        it
                    ) == PackageManager.PERMISSION_GRANTED
                }
            ) {
                !OBUSDK.getPairedDevices().isNullOrEmpty()
            } else
                false
        } ?: false

    }

    companion object {
        var isOBUSimulationEnabled: Boolean = false
        var isOBUAutoConnect: Boolean = false
    }
}

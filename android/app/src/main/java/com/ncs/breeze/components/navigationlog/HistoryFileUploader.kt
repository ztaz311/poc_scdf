package com.ncs.breeze.components.navigationlog

import android.util.Log
import com.google.firebase.storage.BuildConfig
import com.mapbox.navtriprecorder.integration.FirebaseUploader
import com.mapbox.navtriprecorder.integration.SearchAnalyticsData
import java.util.*

object HistoryFileUploader {

    const val ENABLE_COPILOT = false

    suspend fun uploadHistoryFile(
        absFilePath: String,
        startTimeStamp: Date,
        breezeHistoryListener: HistoryRecorder.BreezeHistoryListener,
        firebaseUploader: FirebaseUploader?,
        searchAnalyticsData: SearchAnalyticsData?,
        tripSessionUID: String?
    ) {
        try {
//            if (ENABLE_COPILOT) {
//                //FIXME: this can cause out of memory issues if files are too large
//                handleCoPilotUpload(
//                    absFilePath,
//                    tripSessionUID,
//                    searchAnalyticsData,
//                    firebaseUploader,
//                    startTimeStamp
//                )
//            }

//            val logData = breezeHistoryListener.generateNavigationLogData(absFilePath)
//            RxBus.publish(RxEvent.NavigationLogUploadEvent(
//                NavigationLogData(
//                    filePath = absFilePath,
//                    userId = cur
//                )
//            ))
//            val file = File(absFilePath)
//            file.delete()
        } catch (e: Exception) {
            if (BuildConfig.DEBUG) {
                Log.e("HistoryRecorder", "Failed to upload analytics", e)
            }
        }
    }
//
//    @Deprecated("This can cause out of memory issues if files are too large")
//    private suspend fun handleCoPilotUpload(
//        absFilePath: String,
//        tripSessionUID: String?,
//        searchAnalyticsData: SearchAnalyticsData?,
//        firebaseUploader: FirebaseUploader?,
//        startTimeStamp: Date
//    ): Boolean {
//        val history = readFile(absFilePath) ?: return true
//        val firebasePath = "ncs"
//        val preparedHistory = if (tripSessionUID != null) {
//            prepareReport(history, searchAnalyticsData, tripSessionUID)
//        } else {
//            prepareReport(history, searchAnalyticsData)
//        }
//        firebaseUploader?.report(firebasePath, startTimeStamp, preparedHistory)
//        return false
//    }


//    suspend fun uploadHistoryFiles(
//        absDirPath: String?,
//        startTimeStamp: Date,
//        breezeHistoryListener: HistoryRecorder.BreezeHistoryListener,
//        firebaseUploader: FirebaseUploader?,
//        searchAnalyticsData: SearchAnalyticsData?,
//        tripSessionUID: String?
//    ) {
//        Log.d("HistoryRecorder", "uploadHistoryFiles invoked")
//        if (absDirPath == null) {
//            return
//        }
//        val directory = File(absDirPath)
//        val files: Array<File>? = directory.listFiles()
//        files?.let {
//            Log.d("HistoryRecorder", "uploadHistoryFiles uploading: " + it.size + ", it")
//            for (i in it.indices) {
//                uploadHistoryFile(
//                    it[i].absolutePath,
//                    startTimeStamp,
//                    breezeHistoryListener,
//                    firebaseUploader,
//                    searchAnalyticsData,
//                    tripSessionUID
//                )
//            }
//        }
//        Log.d("HistoryRecorder", "uploadHistoryFiles Completed")
//    }
//
//    private fun readFile(filename: String?): String? {
//        var credentials: String?
//        credentials = null
//        try {
//            val fis: FileInputStream =
//                FileInputStream(filename)
//            val bis = BufferedInputStream(fis)
//            val b = StringBuffer()
//            try {
//                while (bis.available() !== 0) {
//                    val c = bis.read().toChar()
//                    b.append(c)
//                }
//                credentials = b.toString()
//            } catch (e: IOException) {
//                e.printStackTrace()
//            }
//        } catch (e: FileNotFoundException) {
//            e.printStackTrace()
//        }
//        return credentials
//    }
//
//
//    /*
//    * prepare JSON String to later store as custom Event
//     */
//    private fun prepareReport(
//        history: String,
//        searchAnalyticsData: SearchAnalyticsData?,
//        tripSessionUID: String = UUID.randomUUID().toString()
//    ): String {
//        if (searchAnalyticsData !== null) {
//            val gson: Gson = GsonBuilder().serializeNulls().create()
//            val historyValue = if (history == "{}") {
//                "null"
//            } else {
//                history
//            }
//            return "{" +
//                    "\"navigation_metrics\": $historyValue" +
//                    ", \"search_analytics_data\": ${gson.toJson(searchAnalyticsData)}" +
//                    ", \"version\": \"v1.1\"" +
//                    ", \"tripSessionUID\": \"$tripSessionUID\"" +
//                    "}"
//        } else {
//
//            val historyValue = if (history == "{}") {
//                "null"
//            } else {
//                history
//            }
//            return "{" +
//                    "\"navigation_metrics\": $historyValue" +
//                    ", \"search_analytics_data\": null" +
//                    ", \"version\": \"v1.1\"" +
//                    ", \"tripSessionUID\": \"$tripSessionUID\"" +
//                    "}"
//        }
//
//
//    }
//

}
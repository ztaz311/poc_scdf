package com.ncs.breeze.components.marker.markerview2


import android.app.Activity
import android.content.Context
import android.content.res.Resources
import android.view.LayoutInflater
import android.view.View
import com.breeze.model.constants.Constants
import com.google.android.gms.maps.model.LatLng
import com.mapbox.geojson.Point
import com.mapbox.maps.MapboxMap
import com.ncs.breeze.R

class UserLocationToastMarker constructor(
    latLng: LatLng,
    mapboxMap: MapboxMap,
    val activity: Activity,
    itemId: String? = null,
    private val toolTipZoomRange: Double = Constants.POI_TOOLTIP_ZOOM_RADIUS
) : MarkerView2(mapboxMap = mapboxMap, itemId) {

    private var annotationView: View? = null
    private val pixelDensity = Resources.getSystem().displayMetrics.density

    init {
        mLatLng = latLng
        val inflater = activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        annotationView = inflater.inflate(R.layout.here_tootip, null)
        mViewMarker = annotationView
    }

    fun handleClickMarker() {
        renderMarkerToolTip()
    }

    private fun renderMarkerToolTip() {
        annotationView?.bringToFront()
        updateTooltipPosition()
    }

    override fun showMarker() {
        super.showMarker()
        annotationView?.visibility = View.VISIBLE
    }

    override fun hideMarker() {
        super.hideMarker()
        annotationView?.visibility = View.INVISIBLE
    }

    override fun updateTooltipPosition() {
        mLatLng?.let { location ->
            val screenCoordinate = mapboxMap.pixelForCoordinate(
                Point.fromLngLat(
                    location.longitude,
                    location.latitude
                )
            )
            mViewMarker?.let {
                it.measure(
                    View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                    View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
                );
                it.x = screenCoordinate.x.toFloat() - (it.measuredWidth / 2)
                it.y = screenCoordinate.y.toFloat() - it.measuredHeight
            }
        }
    }
}
package com.ncs.breeze.helper.marker

import com.breeze.model.extensions.visibleAmount
import com.breeze.model.api.response.amenities.BaseAmenity
import com.breeze.model.constants.AmenityType.CARPARK
import com.google.gson.JsonObject
import com.mapbox.geojson.Feature
import com.mapbox.geojson.Point
import com.ncs.breeze.components.layermanager.MarkerLayerManager

/**
 * Uses Delegation Pattern to delegate functionality of instances of MarkerLayerManager
 */
open class AmenityMarkerHelper(
    protected open val amenityMarkerManager: MarkerLayerManager,
    val nightModeEnabled: Boolean,
    val voucherEnabled: Boolean
) {

    /**
     * removes all existing features in the layer of given type
     * and then adds the list of amenities to the layer
     * (if layer doesn't exist, it is created)
     *
     * @param type type of amenity layer
     * @param items list of amenities to be added/replaced
     * @param hidden whether all amenities should be hidden once rendered
     * @param showDestination if amenity type is 'carpark', whether destination icon should be replaced
     */
    open fun addAmenityMarkerView(
        type: String,
        items: List<BaseAmenity>,
        hidden: Boolean,
        showDestination: Boolean
    ) {
        val featureList: ArrayList<Feature> = ArrayList()
        items.forEach {
            addFeature(type, it, featureList, showDestination, hidden)
        }
        amenityMarkerManager.addOrReplaceMarkers(type, featureList)
    }

    /**
     * Adds the default properties to the amenity feature as if the amenity was selected
     *
     * @param properties properties of a feature representing this amenity
     * @param hidden whether the amenity added is hidden
     * @param item an individual amenity
     */
    open fun handleSelectedAmenityProperties(
        properties: JsonObject,
        hidden: Boolean = false,
        item: BaseAmenity
    ) {
        properties.addProperty(MarkerLayerManager.HIDDEN, hidden)
        properties.addProperty(
            MarkerLayerManager.IMAGE_TYPE,
            if (item.imageType != null) "-${item.imageType}" else ""
        )
        MarkerLayerIconHelper.handleSelectedIcon(properties, nightModeEnabled)
    }


    /**
     * Adds the additional metadata properties to the car park amenity feature
     * @param properties properties of a feature representing this amenity
     * @param amenity an individual amenity
     * @param showPrice whether the price tooltip layer should be shown
     */
    open fun handleCarParkMetaData(
        properties: JsonObject,
        amenity: BaseAmenity,
        showPrice: Boolean
    ) {
        properties.addProperty(MarkerLayerManager.CHEAPEST_KEY, amenity.isCheapest ?: false)
        amenity.currentHrRate?.oneHrRate?.let { rate ->
            properties.addProperty(MarkerLayerManager.CARPARK_PRICE_SHOW, showPrice)
            if (!amenity.isSeasonParking()) {
                var rateAmount = rate.visibleAmount()
                if (rate == 0.0) {
                    rateAmount = "Free"
                    properties.addProperty(MarkerLayerManager.CARPARK_PRICE, rateAmount)
                } else if (rate == -2.0) {
                    properties.addProperty(MarkerLayerManager.CARPARK_PRICE_SHOW, false)
                } else {
                    properties.addProperty(MarkerLayerManager.CARPARK_PRICE, rateAmount)
                }
            }
        }
    }

    /**
     * Adds the default properties to the car park amenity feature as if the amenity was selected
     * @param amenity an individual amenity
     * @param properties properties of a feature representing this amenity
     * @param showDestination if amenity type is 'carpark', whether destination icon should be replaced
     * @param hidden whether the amenity added is hidden
     */
    fun handleSelectedCarParkProperties(
        amenity: BaseAmenity,
        properties: JsonObject,
        showDestination: Boolean,
        hidden: Boolean = false
    ) {
        properties.addProperty(MarkerLayerManager.HIDDEN, hidden)
        properties.addProperty(
            MarkerLayerManager.IMAGE_TYPE,
            if (amenity.imageType != null) "-${amenity.imageType}" else ""
        )
        amenity.distance?.let { distance ->
            properties.addProperty(MarkerLayerManager.DISTANCE, distance)
        }
        setSelectedCarParkIconProperties(amenity, properties, showDestination)
        handleCarParkMetaData(properties, amenity, false)
    }

    /**
     * Sets the icon property to the car park amenity feature as if the amenity was selected
     * @param it an individual amenity
     * @param properties properties of a feature representing this amenity
     * @param showDestination if amenity type is 'carpark', whether destination icon should be replaced
     */
    protected open fun setSelectedCarParkIconProperties(
        it: BaseAmenity,
        properties: JsonObject,
        showDestination: Boolean
    ) {
        MarkerLayerIconHelper.handleShowCarParkSelectedIcon(
            it,
            properties,
            nightModeEnabled,
            showDestination,
            voucherEnabled
        )
    }


    protected fun handleCarParkProperties(
        it: BaseAmenity,
        properties: JsonObject,
        showDestination: Boolean,
        hidden: Boolean = false
    ) {
        properties.addProperty(MarkerLayerManager.HIDDEN, hidden)
        it.distance?.let { distance ->
            properties.addProperty(MarkerLayerManager.DISTANCE, distance)
        }
        setUnselectedCarParkIconProperties(it, properties, showDestination)
        handleCarParkMetaData(properties, it, !it.isSeasonParking())
    }

    /**
     * Sets the icon property to the car park amenity feature as if the amenity was unselected
     * @param it an individual amenity
     * @param properties properties of a feature representing this amenity
     * @param showDestination if amenity type is 'carpark', whether destination icon should be replaced
     */
    protected open fun setUnselectedCarParkIconProperties(
        it: BaseAmenity,
        properties: JsonObject,
        showDestination: Boolean
    ) {
        MarkerLayerIconHelper.handleCarParkUnSelectedIcon(
            it,
            properties,
            nightModeEnabled,
            showDestination,
            voucherEnabled
        )
    }

    /**
     * hides all the amenities of given type
     * @param type  type of amenity layer
     */
    protected open fun handleHideAmenity(type: String) {
        amenityMarkerManager.hideAllMarker(type)
    }

    /**
     * Adds the default properties to the amenity feature as if the amenity was unselected
     * @param properties properties of a feature representing this amenity
     * @param hidden whether the amenity added is hidden
     * @param item an individual amenity
     */
    protected open fun handleAmenityProperties(
        properties: JsonObject,
        hidden: Boolean = false,
        item: BaseAmenity
    ) {
        properties.addProperty(MarkerLayerManager.HIDDEN, hidden)
        properties.addProperty(
            MarkerLayerManager.IMAGE_TYPE,
            if (item.imageType != null) "-${item.imageType}" else ""
        )
        MarkerLayerIconHelper.handleUnSelectedIcon(properties, nightModeEnabled)
    }

    /**
     * Creates a amenity feature based on a given amenity and adds the default unselected properties
     * @param type type of amenity layer
     * @param it an individual amenity
     * @param featureList empty list passed by reference to store the amenity features
     * @param showDestination if amenity type is 'carpark', whether destination icon should be replaced
     * @param hidden whether the amenities added is hidden
     */
    protected open fun addFeature(
        type: String,
        it: BaseAmenity,
        featureList: ArrayList<Feature>,
        showDestination: Boolean,
        hidden: Boolean
    ) {
        val properties = JsonObject()
        when (type) {
            CARPARK -> {
                handleCarParkProperties(it, properties, showDestination, hidden)
            }
            else -> {
                handleAmenityProperties(properties, hidden, it)
            }
        }
        var feature =
            Feature.fromGeometry(Point.fromLngLat(it.long!!, it.lat!!), properties, it.id)
        featureList.add(feature)
    }

}
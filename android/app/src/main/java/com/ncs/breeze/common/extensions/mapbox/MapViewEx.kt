package com.ncs.breeze.common.extensions.mapbox

import com.ncs.breeze.common.analytics.Analytics
import com.breeze.model.constants.AmenityType.CARPARK
import com.breeze.model.constants.Constants
import com.mapbox.maps.LayerPosition
import com.mapbox.maps.MapView
import com.mapbox.maps.Style
import com.mapbox.maps.extension.style.layers.generated.SymbolLayer
import com.mapbox.maps.extension.style.layers.getLayerAs
import com.mapbox.maps.extension.style.layers.properties.generated.Visibility
import com.mapbox.maps.plugin.locationcomponent.location
import com.ncs.breeze.components.layermanager.MarkerLayerManager

fun MapView.updateDestinationAndLocationPuckLayer(destinationLayer: String = Constants.DESTINATION_LAYER) {
    kotlin.runCatching {
        val style = getMapboxMap().getStyle()
        style?.let {
            moveDestinationLayer(it, destinationLayer)
            updateLocationPuckLayer(it)
        }
    }.onFailure {
        Analytics.logExceptionEvent("Puck update error: ${it.message}")
    }
}

/**
 * if layerAbove is null, the puck layer is added to the top of the stack naturally
 * Note: adding layerAbove set to a custom layer will cause issues if the layer is removed in the future and layerAbove is not reset
 */
private fun MapView.updateLocationPuckLayer(style: Style) {
    location.apply {
        updateSettings {
            layerAbove = null
        }
    }
}

private fun moveDestinationLayer(style: Style, destinationLayer: String) {
    val layer = style.getLayerAs<SymbolLayer>(destinationLayer)
    if (layer != null) {
        style.moveStyleLayer(layer.layerId, LayerPosition(null, null, null))
        // If destination layer exists but is not visible, then a carpark destination is shown instead
        // move the carpark destination to top
        if (layer.visibility == Visibility.NONE) {
            val carParkLayer =
                style.getLayerAs<SymbolLayer>(CARPARK + MarkerLayerManager.LAYER_POSTFIX)
            if (carParkLayer != null) {
                style.moveStyleLayer(carParkLayer.layerId, LayerPosition(null, null, null))
            }
        }
    }
}
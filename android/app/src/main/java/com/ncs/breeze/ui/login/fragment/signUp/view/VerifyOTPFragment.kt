package com.ncs.breeze.ui.login.fragment.signUp.view

import android.app.Dialog
import android.graphics.Color
import android.graphics.Rect
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.WindowManager
import android.widget.TextView
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import com.ncs.breeze.R
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.Event
import com.ncs.breeze.common.analytics.Screen
import com.breeze.model.constants.Constants
import com.ncs.breeze.common.utils.DialogFactory
import com.ncs.breeze.common.utils.Utils
import com.ncs.breeze.common.utils.Variables
import com.breeze.customization.view.BreezeButton
import com.breeze.customization.view.otpview.OTPListener
import com.ncs.breeze.databinding.FragmentVerifyOtpBinding
import com.ncs.breeze.ui.base.BaseFragment
import com.ncs.breeze.ui.login.fragment.signUp.viewmodel.VerifyOTPFragmentViewModel
import timber.log.Timber
import javax.inject.Inject


class VerifyOTPFragment : BaseFragment<FragmentVerifyOtpBinding, VerifyOTPFragmentViewModel>() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val viewModel: VerifyOTPFragmentViewModel by viewModels {
        viewModelFactory
    }

    var email: String? = null
    var name: String? = null
    var password: String? = null
    var userNotConfirmed: Boolean = false

    lateinit var dialog: Dialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            email = requireArguments().getString(Constants.KEYS.KEY_EMAIL)
            name = requireArguments().getString(Constants.KEYS.KEY_NAME)
            password = requireArguments().getString(Constants.KEYS.KEY_PASSWORD)
            userNotConfirmed =
                requireArguments().getBoolean(Constants.KEYS.KEY_USER_NOT_CONFIRMED, false)
        }
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        initialize()
    }

    fun initialize() {
        viewBinding.description.text = email?.let {
            Utils.getFormattedSpan(
                resources.getString(R.string.check_otp),
                it,
                resources.getString(R.string.check_otp_replace_text)
            )
        }
        clickListeners()

        if (userNotConfirmed) {
            resendOTP()
        }
    }

    private fun clickListeners() {

        viewBinding.otpView.otpListener = object : OTPListener {
            override fun onInteractionListener(otp: String) {

            }

            override fun onOTPComplete(otp: String) {
                Analytics.logEditEvent(Event.OTP, getScreenName())

                if (otp.length == 6) {
                    viewBinding.progressBar.visibility = View.VISIBLE
                    if (Variables.isNetworkConnected) {
                        confirmSignUp(otp)
                    } else {
                        DialogFactory.internetDialog(
                            context,
                        ) { _, _ -> }!!.show()
                    }
                }
            }

        }
        viewBinding.backButton.setOnClickListener {
            onBackPressed()
        }

        viewBinding.txtResendVotp.setOnClickListener {
            Analytics.logClickEvent(Event.RESEND, getScreenName())
            resendOTP()
        }
    }

    companion object {
        @JvmStatic
        fun newInstance() =
            VerifyOTPFragment().apply {
                arguments = Bundle().apply {
                }
            }
    }

    override fun inflateViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?,
        attachToContainer: Boolean
    ) = FragmentVerifyOtpBinding.inflate(inflater, container, attachToContainer)

    override fun getViewModelReference(): VerifyOTPFragmentViewModel {
        return viewModel
    }


    private fun confirmSignUp(otpText: String?) {
        viewModel.confirmSignUpSuccessful.observe(viewLifecycleOwner) {
            viewModel.confirmSignUpSuccessful.removeObservers(viewLifecycleOwner)
            viewBinding.progressBar.visibility = View.GONE
            showSuccessDialog()
        }
        viewModel.confirmSignUpFailed.observe(viewLifecycleOwner) {
            viewModel.confirmSignUpFailed.removeObservers(viewLifecycleOwner)
            viewBinding.progressBar.visibility = View.GONE
            Timber.d("Signup Error occurred : " + it.errorMessage)
            viewBinding.otpView.setOTP("")
            viewBinding.otpView.showError()
            viewBinding.error.visibility = View.VISIBLE
            viewBinding.error.text = it.errorMessage
        }
        viewModel.confirmSignUp(email.toString(), otpText!!)
    }

    private fun resendOTP() {
        viewModel.resendSuccessful.observe(viewLifecycleOwner) {
            viewModel.resendSuccessful.removeObservers(viewLifecycleOwner)
            viewBinding.otpView.setOTP("")
        }
        viewModel.resendFailed.observe(
            viewLifecycleOwner
        ) {
            viewModel.resendFailed.removeObservers(viewLifecycleOwner)
            viewBinding.otpView.showError()
            viewBinding.error.visibility = View.VISIBLE
            viewBinding.error.text = it.errorMessage
        }
        viewModel.resendOTP(email!!)
    }


    private fun showSuccessDialog() {


        dialog = activity?.let {
            Dialog(it)
        }!!

        dialog.setContentView(R.layout.dialog_login_successful)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCanceledOnTouchOutside(false)
        dialog.setCancelable(false)

        val greeting = dialog.findViewById<TextView>(R.id.greeting)
        greeting.text = name?.let {
            Utils.getFormattedSpan(
                resources.getString(R.string.logn_success_greeting),
                String.format("%s%s", it, "!"),
                resources.getString(R.string.creation_success_greeting_replace_text)
            )
        }

        val button = dialog.findViewById<BreezeButton>(R.id.button_okay)
        button.setOnClickListener {
            Analytics.logClosePopupEvent(Event.ONBOARDING_SIGNUP_SUCCESS, getScreenName())
        }

        dialog.show()

        val displayRectangle = Rect()
        val window: Window = requireActivity().window
        window.decorView.getWindowVisibleDisplayFrame(displayRectangle)

        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window!!.attributes)
        lp.width = (displayRectangle.width() * 0.9f).toInt()
        dialog.window!!.attributes = lp

        Analytics.logOpenPopupEvent(Event.ONBOARDING_SIGNUP_SUCCESS, getScreenName())
    }


    override fun getScreenName(): String {
        return Screen.ONBOARDING_CREATE_ACCOUNT_STEP2
    }
}


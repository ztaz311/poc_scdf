package com.ncs.breeze.components.marker.markerview2

import com.mapbox.maps.MapView


/**
 * this class for manager all marker filter on dashboard activity
 */
class NotificationMarkerViewManager(
    private val mapView: MapView,
) : MarkerViewManager2(mapView) {

    fun removeAllMarkers() {
        for (markerView in markers) {
            if (markerView is NotificationMarkerView) {
                if (!markers.contains(markerView)) {
                    return
                }
                mapView.removeView(markerView.mViewMarker)
            }
        }
        markers.clear()
    }

    /**
     * hide all tooltips
     */
    fun hideAllTooltips() {
        for (markerView in markers) {
            markerView.hideAllToolTips()
        }
    }

    /**
     * hide all tooltips
     */
    override fun hideAllMarker() {
        for (markerView in markers) {
            markerView.hideMarker()
        }
    }

    /**
     * hide all tooltips
     */
    override fun showAllMarker() {
        for (markerView in markers) {
            markerView.showMarker()
        }
    }
}
package com.ncs.breeze.common.utils

import com.breeze.model.DestinationAddressDetails
import com.mapbox.navigation.core.MapboxNavigation

object BreezeTripManagerUtil {

    fun stopTrip(
        mapboxNavigation: MapboxNavigation,
        currentDestinationDetails: DestinationAddressDetails,
        screen: String,
        isArrivedAtDestination: Boolean = false,
        onFinished: (result: Boolean) -> Unit = {}
    ) {
        if (!mapboxNavigation.isDestroyed) {
            mapboxNavigation.stopTripSession()
            mapboxNavigation.resetTripSession{}
        }
        TripLoggingManager.getInstance()
            ?.stopTripLogging(currentDestinationDetails, screen, isArrivedAtDestination, onFinished =  onFinished)
    }

}
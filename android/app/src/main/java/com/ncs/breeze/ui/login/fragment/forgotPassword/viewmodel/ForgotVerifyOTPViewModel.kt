package com.ncs.breeze.ui.login.fragment.forgotPassword.viewmodel

import android.app.Application
import com.amplifyframework.core.Amplify
import com.ncs.breeze.common.utils.BreezeUserPreferenceUtil
import com.facebook.react.bridge.UiThreadUtil.runOnUiThread
import com.ncs.breeze.common.extensions.android.getApp
import com.ncs.breeze.common.utils.AWSUtils
import com.ncs.breeze.components.AmplifyErrorHandler
import com.ncs.breeze.components.SingleLiveEvent
import com.ncs.breeze.ui.base.BaseFragmentViewModel
import timber.log.Timber
import javax.inject.Inject

class ForgotVerifyOTPViewModel @Inject constructor(application: Application) :
    BaseFragmentViewModel(
        application
    ) {

    var confirmPasswordSuccessful: SingleLiveEvent<Boolean> = SingleLiveEvent()
    var confirmPasswordFailed: SingleLiveEvent<AmplifyErrorHandler.AuthObject> = SingleLiveEvent()

    var resendSuccessful: SingleLiveEvent<Boolean> = SingleLiveEvent()
    var resendFailed: SingleLiveEvent<AmplifyErrorHandler.AuthObject> = SingleLiveEvent()

    @Inject
    lateinit var breezeUserPreferenceUtil: BreezeUserPreferenceUtil


    fun confirmResetPassword(password: String, otpText: String) {
        AWSUtils.confirmResetPassword(
            breezeUserPreferenceUtil.retrieveUserName(),
            password,
            otpText,
            {
                runOnUiThread{
                    confirmPasswordSuccessful.value = true
                }
            },
            { error ->
                Timber.e("AuthQuickstart ${error.toString()}")
                runOnUiThread{
                    confirmPasswordFailed.value = AmplifyErrorHandler.getErrorMessage(
                        error,
                        getApp(),
                        AmplifyErrorHandler.AuthType.FORGOT_PASSWORD
                    )
                }
            }
        )
    }

    fun resendOTP(email: String) {
        AWSUtils.resetPassword(
            email,
            { result ->
                resendSuccessful.value = true
                Timber.i("AuthQuickstart ${result.toString()}")
            },
            { error ->
                runOnUiThread {
                    resendFailed.value = AmplifyErrorHandler.getErrorMessage(
                        error,
                        getApp(),
                        AmplifyErrorHandler.AuthType.FORGOT_PASSWORD
                    )
                }
                Timber.e("AuthQuickstart ${error.toString()}")
            }
        )
    }

}
package com.ncs.breeze.car.breeze.screen.obulite

import com.mapbox.navigation.base.road.model.Road
import com.mapbox.navigation.base.road.model.RoadComponent
import com.mapbox.navigation.base.trip.model.eh.EHorizonPosition
import com.mapbox.navigation.base.trip.model.roadobject.RoadObjectEnterExitInfo
import com.mapbox.navigation.base.trip.model.roadobject.RoadObjectPassInfo
import com.mapbox.navigation.base.trip.model.roadobject.distanceinfo.RoadObjectDistanceInfo
import com.mapbox.navigation.core.lifecycle.MapboxNavigationApp
import com.mapbox.navigation.core.trip.session.eh.EHorizonObserver

abstract class RoadNameObserver : EHorizonObserver {

    var currentRoadName: RoadComponent? = null

    abstract fun onRoadUpdate(currentRoadName: RoadComponent?)

    override fun onPositionUpdated(
        position: EHorizonPosition,
        distances: List<RoadObjectDistanceInfo>
    ) {
        val edgeId = position.eHorizonGraphPosition.edgeId
        val edgeMetadata = MapboxNavigationApp.current()?.graphAccessor?.getEdgeMetadata(edgeId)
        val currentRoadName = edgeMetadata?.names?.firstOrNull()
        if (this.currentRoadName != currentRoadName) {
            this.currentRoadName = currentRoadName
            onRoadUpdate(currentRoadName)
        }
    }

    override fun onRoadObjectAdded(roadObjectId: String) {
        // Do nothing
    }

    override fun onRoadObjectEnter(objectEnterExitInfo: RoadObjectEnterExitInfo) {
        // Do nothing
    }

    override fun onRoadObjectExit(objectEnterExitInfo: RoadObjectEnterExitInfo) {
        // Do nothing
    }

    override fun onRoadObjectPassed(objectPassInfo: RoadObjectPassInfo) {
        // Do nothing
    }

    override fun onRoadObjectRemoved(roadObjectId: String) {
        // Do nothing
    }

    override fun onRoadObjectUpdated(roadObjectId: String) {
        // Do nothing
    }
}

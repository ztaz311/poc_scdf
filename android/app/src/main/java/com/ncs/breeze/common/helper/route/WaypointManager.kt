package com.ncs.breeze.common.helper.route

import com.breeze.model.annotation.AmenityTypeVal
import com.breeze.model.api.response.BreezeWaypointCoordinates
import com.breeze.model.api.response.amenities.BaseAmenity
import com.mapbox.geojson.Point
import java.util.concurrent.ConcurrentHashMap


class WaypointManager {

    /**
     *  map of waypoints
     *  key is the id of the waypoint amenity/feature
     */
    var wayPoints = ConcurrentHashMap<String, BreezeWaypoint>()

    fun getWaypoint(wayPointFeatureId: String): Point? {
        return wayPoints[wayPointFeatureId]?.point
    }

    fun addWayPoint(key: String, waypoint: Point, item: BaseAmenity) {
        wayPoints[key] = BreezeWaypoint(item.id, item.amenityType, waypoint)
    }

    fun addWayPoint(breezeWaypointCoordinates: BreezeWaypointCoordinates) {
        breezeWaypointCoordinates.amenityId?.let {
            wayPoints[it] = BreezeWaypoint(
                it,
                breezeWaypointCoordinates.type,
                Point.fromLngLat(
                    breezeWaypointCoordinates.coordinates[1],
                    breezeWaypointCoordinates.coordinates[0]
                )
            )
        }

    }

    fun removeWayPoint(wayPointFeatureId: String) {
        wayPoints.remove(wayPointFeatureId)
    }

    fun clear() {
        wayPoints.clear()
    }

    fun getAllWayPointsList(): List<Point> {
        return wayPoints.values.map { return@map it.point }
    }

    fun getBreezeWaypointCoordinates(): ArrayList<BreezeWaypointCoordinates> {
        val data = ArrayList<BreezeWaypointCoordinates>()
        wayPoints.values.forEach {
            val coordinateList =
                BreezeWaypointCoordinates(it.amenityId, it.type, it.point.coordinates().reversed())
            data.add(coordinateList)
        }
        return data
    }

}


data class BreezeWaypoint(val amenityId: String?, @AmenityTypeVal val type: String, val point: Point)


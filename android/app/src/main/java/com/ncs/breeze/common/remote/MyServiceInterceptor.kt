package com.ncs.breeze.common.remote

import android.content.Context
import android.net.ConnectivityManager
import android.util.Log
import com.auth0.android.jwt.JWT
import com.ncs.breeze.common.rxbus.RxBus
import com.ncs.breeze.common.rxbus.RxEvent
import com.ncs.breeze.common.utils.AWSUtils
import com.ncs.breeze.common.utils.Variables
import com.facebook.react.bridge.Promise
import kotlinx.coroutines.runBlocking
import okhttp3.Interceptor
import okhttp3.Response
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Singleton

@Singleton
class MyServiceInterceptor(val context: Context? = null) : Interceptor {

    private var sessionToken: String? = null

    fun setSessionToken(sessionToken: String?) {
        this.sessionToken = sessionToken
    }

    fun getSessionToken(): String? {
        return this.sessionToken
    }

    fun getValidSessionToken(promise: Promise) {
        if (sessionToken == null || !isTokenValid(sessionToken!!)) {
            AWSUtils.checkUserAuthSession(object : AWSUtils.AuthSessionCallback {
                override fun onAuthSuccess(token: String?) {
                    setSessionToken(token)
                    promise.resolve(sessionToken)
                }

                override fun onAuthFailed() {
                    promise.reject(Exception("Error while fetching session!"))
                }
            })
        } else {
            promise.resolve(sessionToken)
        }
    }

    /**
     * Interceptor class for setting of the headers for every request
     */
    override fun intercept(chain: Interceptor.Chain): Response {
        if (!Variables.isNetworkConnected) {
            Timber.d("Internet - MyServiceInterceptor: Network is not available - Throwing offline exception")
            RxBus.publish(RxEvent.NoInternetConnection(false))
            throw com.ncs.breeze.common.remote.OfflineException()
        }
        RxBus.publish(RxEvent.NoInternetConnection(true))

        val request = chain.request()
        val requestBuilder = request.newBuilder()
            .addHeader("x-breeze-platform", "android")
            .addHeader("content-type", "application/json")
            .addHeader("cache-control", "no-cache")

        if (sessionToken == null || !isTokenValid(sessionToken!!)) {
            Timber.w("401 Exception observed. Trying to fetch new token now.. 1")
            var newToken: String? = null
            runBlocking {
                newToken = AWSUtils.fetchAuthSessionSync()
            }
            if (newToken.isNullOrEmpty()) {
                Timber.w("Unable to fetch new token. 1")
                RxBus.publish(RxEvent.LogoutRequested())
            } else {
                Timber.w("Fetch new token success. 1")
                setSessionToken(newToken)
            }
        }
        sessionToken?.takeIf { it.isNotEmpty() }
            ?.let {
                requestBuilder.addHeader("Authorization", "Bearer $it")
            }

        val response = chain.proceed(requestBuilder.build())
        if (response.code == 401) {
            Timber.w("401 Exception observed. Trying to fetch new token now.. 2")
            // close previous response
            response.close()
            var newToken: String?
            runBlocking {
                newToken = AWSUtils.fetchAuthSessionSync()
            }
            if (isTokenValid(newToken)) {
                Timber.w("Fetch new token success. 2")
                setSessionToken(newToken)
                // create a new request and modify it accordingly using the new token
                val newRequest = requestBuilder
                    .removeHeader("Authorization")
                    .addHeader("Authorization", "Bearer $sessionToken")
                    .build()
                // retry the request
                return chain.proceed(newRequest)
            } else {
                Timber.w("Request Logout when refresh Token was expired")
                RxBus.publish(RxEvent.LogoutRequested())
            }
        }
        return response
    }

    fun isTokenValid(pJwtString: String?): Boolean {
        if (pJwtString.isNullOrEmpty()) return false
        val jwt = JWT(pJwtString)
        val expiresAt = jwt.expiresAt
        val currentTime = Calendar.getInstance().time
        if (expiresAt != null) {
            if ((expiresAt.time - currentTime.time) <= 0) {
                return false
            }
        }
        return true

    }

}
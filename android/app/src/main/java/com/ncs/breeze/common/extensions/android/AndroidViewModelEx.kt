package com.ncs.breeze.common.extensions.android

import androidx.lifecycle.AndroidViewModel
import com.ncs.breeze.App

fun AndroidViewModel.getApp() = this.getApplication<App>()
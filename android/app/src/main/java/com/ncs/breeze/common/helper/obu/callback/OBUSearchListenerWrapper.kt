package com.ncs.breeze.common.helper.obu.callback

import com.ncs.breeze.common.helper.obu.OBUConnectionHelper
import sg.gov.lta.obu.sdk.conn.OBU
import sg.gov.lta.obu.sdk.conn.OBUSearchListener
import sg.gov.lta.obu.sdk.core.types.OBUError
import timber.log.Timber

class OBUSearchListenerWrapper(
    private val onPairing: () -> Unit = {},
    private val onObuSelected: (OBU) -> Unit = {},
    private val onSearchFailure: (error: OBUError?) -> Unit = {},
) : OBUSearchListener {

    override fun onObuSelected(obu: OBU) {
        super.onObuSelected(obu)
        Timber.d("OBUSearchListenerWrapper: onObuSelected ${obu.name}")
        onObuSelected.invoke(obu)
    }

    override fun onPairing() {
        super.onPairing()
        Timber.d("OBUSearchListenerWrapper: onPairing ")
        onPairing.invoke()
    }

    override fun onSearchFailure(error: OBUError?) {
        super.onSearchFailure(error)
        Timber.d(
            error,
            "OBUSearchListenerWrapper onSearchFailure: ${error?.code} - ${error?.message}, isMock = ${isSimulationEnabled()}"
        )
        onSearchFailure.invoke(error)
//        if (isSimulationEnabled() && error is OBUInternalSearchError) {
//            onObuSelected.invoke(OBUMockData.obtainOBURandom())
//        }
    }

    private fun isSimulationEnabled() =
        OBUConnectionHelper.isOBUSimulationEnabled
}
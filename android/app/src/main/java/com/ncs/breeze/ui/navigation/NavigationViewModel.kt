package com.ncs.breeze.ui.navigation

import android.app.Application
import androidx.lifecycle.viewModelScope
import com.ncs.breeze.common.ServiceLocator
import com.breeze.model.api.ErrorData
import com.breeze.model.api.request.CopilotAPIRequest
import com.breeze.model.api.response.ETAResponse
import com.ncs.breeze.common.remote.ApiHelper
import com.ncs.breeze.common.remote.ApiObserver
import com.breeze.model.DestinationAddressDetails
import com.breeze.model.ERPRouteInfo
import com.ncs.breeze.common.model.RouteAdapterObjects
import com.breeze.model.api.request.ETARequest
import com.breeze.model.api.request.GetParkingAlternativeRequest
import com.breeze.model.api.response.AvailabilityAlertData
import com.breeze.model.api.response.AvailabilityAlertResponse
import com.breeze.model.api.response.BroadcastMessageResponse
import com.breeze.model.api.response.ERPResponseData
import com.breeze.model.api.response.amenities.CarparkAvailabilityData
import com.breeze.model.api.response.amenities.CarparkAvailabilityResponse
import com.breeze.model.api.response.tbr.ResponseZoneAlertDetail
import com.breeze.model.constants.TYPE_BROADCAST_MESSAGE
import com.ncs.breeze.common.utils.*
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.mapbox.api.directions.v5.models.DirectionsResponse
import com.mapbox.api.directions.v5.models.DirectionsRoute
import com.ncs.breeze.car.breeze.utils.BreezeCarUtil
import com.mapbox.geojson.Point
import com.mapbox.navigation.base.route.NavigationRoute
import com.mapbox.navtriprecorder.integration.MapboxSearchKit
import com.mapbox.navtriprecorder.integration.RoutablePointProvider
import com.mapbox.navtriprecorder.integration.RoutablePointResult
import com.ncs.breeze.components.navigationlog.HistoryFileUploader
import com.ncs.breeze.components.SingleLiveEvent
import com.ncs.breeze.ui.base.BaseViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import javax.inject.Inject

class NavigationViewModel @Inject constructor(application: Application, apiHelper: ApiHelper) :
    BaseViewModel(application, apiHelper) {

    @Inject
    lateinit var breezeERPUtil: BreezeERPRefreshUtil

    @Inject
    lateinit var breezeRouteSortingUtil: BreezeRouteSortingUtil

    var mBroadcastMessage: SingleLiveEvent<BroadcastMessageResponse> =
        SingleLiveEvent()


    var walkingRoutesRetrieved: SingleLiveEvent<NavigationRoute?> = SingleLiveEvent()

    var etaAPISuccessful: SingleLiveEvent<ETARequest> =
        SingleLiveEvent()

    //var erpRouteInfo: SingleLiveEvent<Map<Int, ERPRouteInfo>> = SingleLiveEvent()
    var sortedRouteObjects: SingleLiveEvent<List<RouteAdapterObjects>> = SingleLiveEvent()
    var routablePointResultEvent: SingleLiveEvent<RoutablePointResult> = SingleLiveEvent()
    var ratingSuccessful: SingleLiveEvent<Boolean> = SingleLiveEvent()
    var mResponseOuterZoneAlertDetail: SingleLiveEvent<ResponseZoneAlertDetail> = SingleLiveEvent()
    var mResponseInnerZoneAlertDetail: SingleLiveEvent<ResponseZoneAlertDetail> = SingleLiveEvent()
    var etaStartSuccessful: SingleLiveEvent<ETARequest> = SingleLiveEvent()
    var etaStartProgress: SingleLiveEvent<Boolean> = SingleLiveEvent()

    suspend fun checkERPs(
        erpData: ERPResponseData.ERPResponse,
        routeData: List<RouteAdapterObjects>
    )
            : HashMap<Int, ERPRouteInfo> {
        return breezeERPUtil.getERPsIntersectingWithRouteList(erpData, routeData, false)
    }

    fun getCarparkAlternative(
        lat: Double,
        long: Double,
        destName: String,
        onSuccess: (CarparkAvailabilityData) -> Unit
    ) {
        val request = GetParkingAlternativeRequest(lat, long, destName)
        BreezeCarUtil.apiHelper.getCarParkAlternative(request)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : ApiObserver<CarparkAvailabilityResponse>(compositeDisposable) {
                override fun onSuccess(data: CarparkAvailabilityResponse) {
                    if (data.success) {
                        data.data?.let(onSuccess)
                    }
                }

                override fun onError(e: ErrorData) {

                }

            })
    }

    fun getAvailabilityAlertVoice(
        distance: String,
        carparkId: String? = null,
        userLocationLinkIdRef: String? = null,
        placeId: String? = null,
        amenityId: String? = null,
        layerCode: String? = null,
        onSuccess: (AvailabilityAlertData) -> Unit
    ) {
        BreezeCarUtil.apiHelper.getAvailabilityAlert(
            distance,
            carparkId,
            userLocationLinkIdRef,
            placeId,
            amenityId,
            layerCode
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                object : ApiObserver<AvailabilityAlertResponse>(compositeDisposable) {
                    override fun onSuccess(data: AvailabilityAlertResponse) {
                        if (data.success) {
                            data.data?.let(onSuccess)
                        }
                    }

                    override fun onError(e: ErrorData) {

                    }

                }
            )
    }

    /**
     * alert when user enter zone
     */
    //todo must update when get type from preferencess
    fun getZoneAlertDetail(
        navigationtype: String = "CRUISE",
        profile: String,
        zoneid: String,
        targetZoneId: String?,
        destinationCarpark: Boolean = false,
        carparkId: String? = null,
    ) {
        BreezeCarUtil.apiHelper.getZoneAlertDetails(
            navigationtype,
            profile,
            zoneid,
            targetZoneId,
            destinationCarpark,
            carparkId
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : ApiObserver<ResponseZoneAlertDetail>(compositeDisposable) {
                override fun onSuccess(data: ResponseZoneAlertDetail) {
                    data.apply {
                        if (destinationCarpark && carparkId != null) {
                            mResponseInnerZoneAlertDetail.postValue(data)
                        } else {
                            mResponseOuterZoneAlertDetail.postValue(data)
                        }
                    }
                }

                override fun onError(e: ErrorData) {

                }
            })

    }

    /**
     * get broadcast message
     */
    fun getBroadcastMessage(
        idCarpark: String,
        typeBroadcastMessage: String = TYPE_BROADCAST_MESSAGE.NAVIGATION
    ) {
        CoroutineScope(Dispatchers.IO).launch {
            BreezeCarUtil.apiHelper.getBroadcastMessage(idCarpark, typeBroadcastMessage)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : ApiObserver<BroadcastMessageResponse>(compositeDisposable) {
                    override fun onSuccess(data: BroadcastMessageResponse) {
                        data.apply {
                            mBroadcastMessage.postValue(data)
                        }
                    }

                    override fun onError(e: ErrorData) {
                    }
                })
        }
    }


    fun getSortedRoutes(
        currentRouteList: List<DirectionsRoute>,
        originalResponse: DirectionsResponse
    ) {
        viewModelScope.launch {
            withContext(Dispatchers.Default) {
                //Create and Sort route objects
                var currentRouteObjects = createRouteObjects(currentRouteList, originalResponse)

                breezeERPUtil.getERPData()?.let {
                    var map = checkERPs(it, currentRouteObjects)
                    for ((index, routeObject) in currentRouteObjects.withIndex()) {
                        var data = routeObject
                        data.erpRate = map[index]?.erpRate
                        data.erpFeatureCollection = map[index]?.featureCollection
                    }

                    var sortedObjects = breezeRouteSortingUtil.getSortedRoutes(currentRouteObjects)
                    sortedRouteObjects.postValue(sortedObjects)
                }
            }
        }
    }

    private fun createRouteObjects(
        currentRouteList: List<DirectionsRoute>,
        originalResponse: DirectionsResponse
    )
            : MutableList<RouteAdapterObjects> {
        var routeObjectList: MutableList<RouteAdapterObjects> = arrayListOf()
        for (route in currentRouteList) {
            val minutes = Utils.convertDurationToMinutes(route.duration().toFloat())
            val time = Utils.splitToComponentTimes(minutes)
            val arrivalTime = Utils.addTimeToCurrentTime(time!![0], time!![1])
            //val distance = Utils.convertMeterToKilometer(route.distance().toFloat())
            val distanceInMeters = route.distance().toFloat()
            routeObjectList.add(
                RouteAdapterObjects(
                    route,
                    arrivalTime,
                    minutes,
                    distanceInMeters,
                    null,
                    null,
                    null,
                    originalResponse
                )
            )
        }
        return routeObjectList
    }

    fun createSearchFeedback(address: String, latitude: Double, longitude: Double) {
        val mapboxSearchKit = MapboxSearchKit(
            mapboxAccessToken = ""
        )
        val routablePointProvider = RoutablePointProvider(
            searchKit = mapboxSearchKit
        )

        viewModelScope.launch {
            var routablePointResult: RoutablePointResult =
                routablePointProvider.findRoutablePoint(
                    // An address for which you want to find routable point
                    address = address,
                    // Google rooftop point for the provided address
                    rooftopPoint = com.mapbox.navtriprecorder.integration.models.Point(
                        longitude = longitude,
                        latitude = latitude
                    ),
                    // (Optional) Unit number of the provided address (line 2),
                    // used by Beans.AI to improve search quality
                    unit = null
                )
            routablePointResultEvent.value = routablePointResult
        }

    }

    fun sendSecondaryFirebaseID(username: String?) {
        if (HistoryFileUploader.ENABLE_COPILOT) {
            viewModelScope.launch {
                apiHelper.sendSecondaryFirebaseUserID(
                    CopilotAPIRequest(
                        ServiceLocator.getSecondaryFirebaseAuthUserId(),
                        username
                    )
                )
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(object : ApiObserver<JsonElement>(compositeDisposable) {
                        override fun onSuccess(data: JsonElement) {
                            Timber.d("Copilot API is successful: " + data.toString())
                        }

                        override fun onError(e: ErrorData) {
                            Timber.d("Copilot API failed: " + e.toString())
                        }
                    })
            }
        }
    }


    fun sendTripRating(rating: Int, tripGUID: String) {

        var request = JsonObject()
        request.addProperty("tripGUID", tripGUID)
        request.addProperty("rating", rating)

        apiHelper.sendTripRating(request)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : ApiObserver<JsonElement>(compositeDisposable) {
                override fun onSuccess(data: JsonElement) {
                    Timber.d("Update ETA status API Successful")
                    Timber.d("Response from trip rating ${data}")
                    ratingSuccessful.value = true
                }

                override fun onError(e: ErrorData) {
                    Timber.d("API failed: " + e.toString())
                    ratingSuccessful.value = false
                }

            })
    }

    fun findWalkingRoutes(
        originPoint: Point?,
        destination: Point?,
        currentDestinationDetails: DestinationAddressDetails
    ) {
        CoroutineScope(Dispatchers.IO).launch {
            InitObjectUtilsController.mRoutePlanningHelper?.let { routePlaningHelper ->
                val walkingNavigationRoute: NavigationRoute? =
                    routePlaningHelper.findWalkingRoute(
                        originPoint, destination,
                        currentDestinationDetails.address1,
                    )
                walkingRoutesRetrieved.postValue(walkingNavigationRoute)
            }
        }
    }

    fun startETA(etaRequest: ETARequest) {
        Timber.d("ETA Request: " + etaRequest.toString())
        etaStartProgress.postValue(true)
        BreezeCarUtil.apiHelper.sendETAMessage(etaRequest)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : ApiObserver<ETAResponse>(compositeDisposable) {
                override fun onSuccess(data: ETAResponse) {
                    etaRequest.tripEtaUUID = data.tripEtaUUID
                    etaStartProgress.postValue(false)
                    etaStartSuccessful.postValue(etaRequest)
                }

                override fun onError(e: ErrorData) {
                    etaStartProgress.postValue(false)
                    Timber.d("Start ETA API failed: " + e.toString())
                }
            })
    }

}
package com.ncs.breeze.car.breeze.style

import androidx.car.app.ScreenManager
import com.mapbox.maps.extension.androidauto.MapboxCarMapObserver
import com.mapbox.maps.extension.androidauto.MapboxCarMapSurface
import com.ncs.breeze.car.breeze.MainBreezeCarContext
import com.ncs.breeze.car.breeze.screen.homescreen.HomeCarScreen
import com.ncs.breeze.car.breeze.screen.navigation.NavigationScreen
import com.ncs.breeze.car.breeze.screen.obulite.CarOBULiteScreen
import com.ncs.breeze.car.breeze.utils.BreezeCarUtil
import com.ncs.breeze.car.breeze.utils.BreezeMapBoxUtil
import com.ncs.breeze.common.constant.MapIcon
import com.breeze.model.extensions.toBitmap
import com.breeze.model.constants.Constants


class CarFreeDriveStyle(
    private val mainCarContext: MainBreezeCarContext
) : MapboxCarMapObserver {


    override fun onAttached(mapboxCarMapSurface: MapboxCarMapSurface) {
        super.onAttached(mapboxCarMapSurface)
        mapboxCarMapSurface.mapSurface.getMapboxMap().getStyle {
            MapIcon.CommonMapIcon.forEach { (key, value) ->
                value.toBitmap(mainCarContext.carContext)?.let { bitmap ->
                    it.addImage(key, bitmap, false)
                }
            }

            it.removeStyleLayer(Constants.TRAFFIC_LAYER_ID)
            val incidentData = BreezeCarUtil.breezeIncidentsUtil.getLatestIncidentData()
            if (incidentData.isNotEmpty()) {
                BreezeMapBoxUtil.displayIncidentData(it, incidentData);
            }

            if (BreezeCarUtil.breezeERPUtil.getCurrentERPData() != null) {
                BreezeMapBoxUtil.addERPLayerToStyle(
                    it,
                    BreezeCarUtil.breezeERPUtil.getCurrentERPData()!!,
                    mainCarContext.carContext
                );
            }

        }
    }

    override fun onDetached(mapboxCarMapSurface: MapboxCarMapSurface) {
        mapboxCarMapSurface?.mapSurface?.getMapboxMap()?.getStyle()?.let {
            val topScreen = mainCarContext.carContext.getCarService(
                ScreenManager::class.java
            ).top
            if ((topScreen !is HomeCarScreen) &&
                (topScreen !is NavigationScreen) &&
                (topScreen !is CarOBULiteScreen)
            ) {
                BreezeMapBoxUtil.removeDynamicLayers(it);
            }

        }
    }


}
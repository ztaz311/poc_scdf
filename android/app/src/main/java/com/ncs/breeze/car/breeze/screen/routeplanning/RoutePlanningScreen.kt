package com.ncs.breeze.car.breeze.screen.routeplanning

import android.annotation.SuppressLint
import android.location.Location
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.util.Log
import androidx.car.app.CarToast
import androidx.car.app.model.*
import androidx.car.app.navigation.model.RoutePreviewNavigationTemplate
import androidx.lifecycle.lifecycleScope
import com.breeze.model.*
import com.breeze.model.api.request.ETARequest
import com.breeze.model.api.response.ERPResponseData
import com.breeze.model.enums.RoutePreference
import com.mapbox.android.core.location.LocationEngineResult
import com.mapbox.api.directions.v5.models.DirectionsResponse
import com.mapbox.api.directions.v5.models.DirectionsRoute
import com.mapbox.geojson.Point
import com.mapbox.navigation.base.route.NavigationRoute
import com.mapbox.navigation.base.route.RouterOrigin
import com.mapbox.navigation.core.lifecycle.MapboxNavigationApp
import com.mapbox.navigation.utils.internal.toPoint
import com.ncs.breeze.R
import com.ncs.breeze.car.breeze.base.BaseScreenCar
import com.ncs.breeze.car.breeze.model.ETARouteDetails
import com.ncs.breeze.car.breeze.screen.routeplanning.routesurface.CarRouteCamera
import com.ncs.breeze.car.breeze.screen.routeplanning.routesurface.CarRouteLine
import com.ncs.breeze.car.breeze.utils.BreezeCarUtil
import com.ncs.breeze.car.breeze.utils.CarConstants
import com.ncs.breeze.car.breeze.utils.LimitClick
import com.ncs.breeze.car.breeze.utils.extension.appendCarSpan
import com.ncs.breeze.car.breeze.utils.setClickSafe
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.Event
import com.ncs.breeze.common.analytics.Screen
import com.ncs.breeze.common.event.ToAppRx.postEvent
import com.ncs.breeze.common.event.ToCarRx.postEventWithCacheLast
import com.ncs.breeze.common.extensions.mapbox.compressToGzip
import com.ncs.breeze.common.helper.route.RoutePlanningHelper
import com.ncs.breeze.common.model.RouteAdapterObjects
import com.ncs.breeze.common.model.RouteAdapterTypeObjects
import com.ncs.breeze.common.model.SimpleRouteAdapterObjects
import com.ncs.breeze.common.model.rx.AppToNavigationStartEvent
import com.ncs.breeze.common.model.rx.AppToPhoneNavigationStartEvent
import com.ncs.breeze.common.model.rx.AppToRoutePlanningStartEvent
import com.ncs.breeze.common.model.rx.CarNavigationStartEventData
import com.ncs.breeze.common.model.rx.DataForRoutePreview
import com.ncs.breeze.common.model.rx.PhoneNavigationStartEventData
import com.ncs.breeze.common.model.rx.StopOBULiteDisplay
import com.ncs.breeze.common.model.toRouteAdapterObjects
import com.ncs.breeze.common.rxbus.RxBus
import com.ncs.breeze.common.rxbus.RxEvent
import com.ncs.breeze.common.utils.*
import com.ncs.breeze.common.utils.route.RouteSelectorUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.lang.Float.max


class RoutePlanningScreen(
    private val routePreviewCarContext: RoutePreviewCarContext,
    val routePreviewData: DataForRoutePreview,
) : BaseScreenCar(routePreviewCarContext.carContext) {

    private val SEPARATOR_DOT = " · "
    private val incidentCompositeDisposable = CompositeDisposable()
//    val SHOW_RANDOM_ICON = false

    private var isLoadingData = true
    private val locationCallback = object : LocationBreezeManager.CallBackLocationBreezeManager {
        override fun onFailure(exception: Exception) {
            handleFetchLocationFailure()
        }

        override fun onSuccess(result: LocationEngineResult) {
            LocationBreezeManager.getInstance().removeLocationCallBack(this)
            result.lastLocation?.let { currentLocation ->
                validateAddresses(
                    originPoint = currentLocation.toPoint(),
                    destination = routePreviewData.selectedDestination
                ) {
                    loadRouteData(
                        originPoint = currentLocation.toPoint(),
                        destination = routePreviewData.selectedDestination
                    )
                }
            }

        }
    }

    private val carRouteLine = CarRouteLine(routePreviewCarContext.mainCarContext, lifecycle)
    private val carNavigationCamera = CarRouteCamera(
        routePreviewCarContext, CarRouteCamera.CameraMode.OVERVIEW
    )

    //BREEZE2-1845
//    private var etaRequest: ETARequest? = null

    private var directionsRoutes: ArrayList<RouteAdapterObjects> = arrayListOf()
    private var listRoute: ArrayList<NavigationRoute> = arrayListOf()

    private var selectedIndex = 0


    override fun onCreateScreen() {
        super.onCreateScreen()
        OBUStripStateManager.getInstance()?.disable()
        if (LocationBreezeManager.getInstance().currentLocation != null) {
            validateAddresses(
                originPoint = LocationBreezeManager.getInstance().currentLocation?.toPoint(),
                destination = routePreviewData.selectedDestination
            ) {
                loadRouteData(
                    LocationBreezeManager.getInstance().currentLocation!!.toPoint(),
                    routePreviewData.selectedDestination
                )
            }
        } else {
            LocationBreezeManager.getInstance().startLocationUpdates()
            LocationBreezeManager.getInstance().registerLocationCallback(locationCallback)
        }

        incidentCompositeDisposable.clear()
        incidentCompositeDisposable.add(RxBus.listen(RxEvent.ERPRefresh::class.java)
            .observeOn(AndroidSchedulers.mainThread()).subscribe {
                if (!isLoadingData) {
                    checkERPRateChanged {
                        relaunchScreen()
                    }
                }
            })
        postEvent(StopOBULiteDisplay())
    }

    private fun checkERPRateChanged(onChanged: () -> Unit) {
        lifecycleScope.launch(Dispatchers.IO) {
            BreezeCarUtil.breezeERPUtil.getERPData()?.let { allERPData ->
                val updatedERPData = BreezeCarUtil.breezeERPUtil.getCurrentERPData()
                directionsRoutes.forEach { routeObject ->
                    val erpInRoute = BreezeCarUtil.breezeERPUtil.getERPInRoute(
                        allERPData, routeObject.route, false
                    )
                    val newRate = erpInRoute.featureCollection.features()?.sumOf { feature ->
                        val erpName =
                            feature.getStringProperty(RoutePlanningConsts.PROP_ERP_ID)
                        return@sumOf updatedERPData?.mapOfERP?.get(erpName)?.charge?.toDouble()
                            ?: 0.0
                    }?.toFloat() ?: 0f
                    if (newRate != routeObject.erpRate) {
                        onChanged.invoke()
                        return@launch
                    }
                }
            }
        }
    }

    private fun relaunchScreen() {
        lifecycleScope.launch(Dispatchers.Main) {
            screenManager.popTo(CarConstants.HOME_MARKER)
            postEventWithCacheLast(
                AppToRoutePlanningStartEvent(
                    routePreviewData
                )
            )
        }
    }

    override fun onGetTemplate() = buildRoutePreviewTemplate()

    @SuppressLint("MissingPermission")
    override fun onStartScreen() {
        super.onStartScreen()
        routePreviewCarContext.mapboxCarMap.registerObserver(carNavigationCamera)
        routePreviewCarContext.mapboxCarMap.registerObserver(carRouteLine)
    }


    override fun onStopScreen() {
        super.onStopScreen()
        routePreviewCarContext.mapboxCarMap.unregisterObserver(carNavigationCamera)
        routePreviewCarContext.mapboxCarMap.unregisterObserver(carRouteLine)
    }


    override fun onDestroyScreen() {
        super.onDestroyScreen()
        CoroutineScope(Dispatchers.IO).launch {
            incidentCompositeDisposable.dispose()
        }
    }

    private fun onFinishedLoadingRoutes() {
        LocationBreezeManager.getInstance().currentLocation?.let {
            listRoute.clear()
            directionsRoutes.forEach { routeAdapterObject ->
                routeAdapterObject.route.routeOptions()?.let { routeOptions ->
                    val navigationRoutes = NavigationRoute.create(
                        routeAdapterObject.originalResponse, routeOptions, RouterOrigin.Custom()
                    ).filter { navigationRoute ->
                        navigationRoute.routeIndex == routeAdapterObject.route.routeIndex()?.toInt()
                    }
                    listRoute.addAll(navigationRoutes)
                }
            }
            MapboxNavigationApp.current()?.setNavigationRoutes(listRoute)
            isLoadingData = false
            invalidate()
        }
    }

    private fun validateAddresses(
        originPoint: Point?, destination: Point?, onValidAddress: () -> Unit = {}
    ) {
        if (originPoint == null || destination == null) {
            showUnsupportedAddressDialog()
            return
        }

        lifecycleScope.launch(Dispatchers.IO) {
            //Assuming that 'originPoint' will never be null at this point.
            val originLocation = Location("")
            originLocation.latitude = originPoint.latitude()
            originLocation.longitude = originPoint.longitude()
            val destinationLocation = Location("")
            destinationLocation.latitude = destination.latitude()
            destinationLocation.longitude = destination.longitude()

            val isAddressSupported = Utils.areAddressesSupported(
                context = carContext, origin = originLocation, destination = destinationLocation
            )

            withContext(Dispatchers.Main) {
                if (isAddressSupported) {
                    onValidAddress.invoke()
                } else {
                    showUnsupportedAddressDialog()
                }
            }
        }

    }

    private fun startNavigationWithThisRoute() {
        Analytics.logClickEvent(Event.NAVIGATE_HERE, Screen.AA_ROUTE_PREVIEW_SCREEN)
        val currentRoute = listRoute[selectedIndex]
        val originalResponse = directionsRoutes[selectedIndex].originalResponse
        val destinationAddressDetails = generateDestinationAddress()
        Analytics.logLocationDataEvent(
            "lets_go_origin",
            getScreenName(),
            routePreviewData.originalDestination?.destinationName
                ?: routePreviewData.originalDestination?.address1 ?: "",
            routePreviewData.originalDestination?.lat ?: "",
            routePreviewData.originalDestination?.long ?: ""
        )
        Analytics.logLocationDataEvent(
            "lets_go_destination",
            getScreenName(),
            destinationAddressDetails.destinationName
                ?: destinationAddressDetails.address1 ?: "",
            destinationAddressDetails.lat ?: "",
            destinationAddressDetails.long ?: ""
        )

        //BREEZE2-1845
//        // Start ETA
//        if (etaRequest != null) {
//            BreezeCarUtil.apiHelper.sendETAMessage(etaRequest!!).subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(object : ApiObserver<ETAResponse>(compositeDisposable) {
//                    override fun onSuccess(data: ETAResponse) {
//                        etaRequest!!.tripEtaUUID = data.tripEtaUUID
//                        triggerNavigationStartEvent(
//                            this@RoutePlanningScreen.routePreviewData.originalDestination
//                                ?: destinationAddressDetails,
//                            destinationAddressDetails = destinationAddressDetails,
//                            route = currentRoute.directionsRoute,
//                            originalResponse = originalResponse,
//                            erpData = null,
//                            etaRequest = etaRequest
//                        )
//                    }
//
//                    override fun onError(e: ErrorData) {
//                        println("RoutePlanningScreen sendETAMessage onError ${e.message}")
//                    }
//                })
//
//            return
//        }

        triggerNavigationStartEvent(
            routePreviewData.originalDestination ?: destinationAddressDetails,
            destinationAddressDetails = destinationAddressDetails,
            route = currentRoute.directionsRoute,
            originalResponse = originalResponse,
            erpData = null,
            etaRequest = null/*etaRequest*/ //BREEZE2-1845
        )
    }

    private fun generateDestinationAddress(): DestinationAddressDetails {
        return DestinationAddressDetails(
            routePreviewData.destinationSearchLocation?.addressid,
            routePreviewData.destinationSearchLocation?.address1,
            routePreviewData.destinationSearchLocation?.address2,
            routePreviewData.destinationSearchLocation?.lat,
            routePreviewData.destinationSearchLocation?.long,
            null,
            routePreviewData.destinationSearchLocation?.name,
            DestinationAddressTypes.EASY_BREEZIES,
            isCarPark = routePreviewData.destinationSearchLocation?.isCarPark ?: false,
            fullAddress = routePreviewData.destinationSearchLocation?.fullAddress,
            carParkID = routePreviewData.destinationSearchLocation?.carparkId,
            placeId = routePreviewData.destinationSearchLocation?.placeId,
        )
    }

    private fun triggerNavigationStartEvent(
        originalDestination: DestinationAddressDetails,
        destinationAddressDetails: DestinationAddressDetails,
        route: DirectionsRoute,
        originalResponse: DirectionsResponse,
        erpData: ERPResponseData.ERPResponse?,
        etaRequest: ETARequest?
    ) {
        CoroutineScope(Dispatchers.Main).launch {
            try {
                val compressedRoute = originalResponse.compressToGzip()
                val phoneData = PhoneNavigationStartEventData(
                    originalDestination,
                    destinationAddressDetails,
                    route,
                    compressedRoute,
                    erpData,
                    etaRequest
                )
                postEvent(AppToPhoneNavigationStartEvent(phoneData))
                val carData = CarNavigationStartEventData(
                    originalDestination,
                    destinationAddressDetails,
                    route,
                    compressedRoute,
                    originalResponse,
                    erpData,
                    etaRequest
                )
                postEventWithCacheLast(AppToNavigationStartEvent(carData))
            } catch (e: Exception) {
                Log.e("RoutePlanningScreen", e.stackTraceToString())
            }
        }
    }

    override fun getScreenName(): String {
        return Screen.AA_ROUTE_PREVIEW_SCREEN
    }

    private fun showUnsupportedAddressDialog() {
        CarToast.makeText(
            carContext,
            carContext.getString(R.string.dialog_msg_country_not_supported),
            CarToast.LENGTH_LONG
        ).show()
        finish()
    }

    private fun handleFetchLocationFailure() {
        CarToast.makeText(
            carContext,
            carContext.getString(R.string.error_get_current_location),
            CarToast.LENGTH_LONG
        ).show()
        finish()
    }

    private fun loadRouteData(
        originPoint: Point,
        destination: Point,
    ) {

        InitObjectUtilsController.mRoutePlanningHelper?.let { routePlanningHelper ->
            lifecycleScope.launch(Dispatchers.Main) {
                val curRoutePreference: HashMap<RoutePreference, List<RouteAdapterObjects>> =
                    routePlanningHelper.findRoutes(
                        originPoint,
                        destination,
                        routePreviewData.destinationSearchLocation!!.address1!!,
                        listOf(),
                        RoutePlanningHelper.RouteAlgorithm.MERGED_SINGLE_ROUTE_PER_TYPE
                    )
                val allRoutes = getRouteTypeDataList(HashMap(curRoutePreference))
                directionsRoutes.clear()
                if (allRoutes.size > 0) {
                    allRoutes.take(3).forEach {
                        directionsRoutes.add(it.routeData.toRouteAdapterObjects())
                    }
                }
                onFinishedLoadingRoutes()
            }
        }
    }

    /**
     * converts the route map to a ordered selectable list
     */
    private fun getRouteTypeDataList(routesMap: HashMap<RoutePreference, List<SimpleRouteAdapterObjects>>): ArrayList<RouteAdapterTypeObjects> {
        val allRoutes: ArrayList<RouteAdapterTypeObjects> = ArrayList()
        val alternates = routesMap.remove(RoutePreference.ALTERNATE_ROUTE)
        RouteSelectorUtil.handleRoutePreference(
            routesMap, allRoutes, RoutePreference.FASTEST_ROUTE.type
        )
        routesMap.forEach { map ->
            map.value.forEach {
                allRoutes.add(RouteAdapterTypeObjects(map.key, it))
            }
        }
        alternates?.let { alternates ->
            alternates.forEach {
                allRoutes.add(RouteAdapterTypeObjects(RoutePreference.ALTERNATE_ROUTE, it))
            }
        }
        return allRoutes
    }


    @SuppressLint("MissingPermission")
    private fun buildRoutePreviewTemplate(): Template {
        val title = routePreviewData.destinationSearchLocation?.address1
            ?: carContext.getString(R.string.route_preview)

        val templateBuilder = RoutePreviewNavigationTemplate.Builder().setLoading(isLoadingData)
            .setHeaderAction(Action.BACK).setTitle(title)
        if (!isLoadingData) {
            val routesLength = directionsRoutes.size
            templateBuilder.setItemList(buildRouteList()).setNavigateAction(buildNavigateAction())
            if (routesLength > 0) {
                templateBuilder.setActionStrip(buildActionStrip())
            }
        }
        return templateBuilder.build()
    }

    private fun buildNavigateAction() =
        Action.Builder().setTitle(carContext.getString(R.string.let_go)).setClickSafe {
            LimitClick.handleSafe {
                Analytics.logClickEvent(
                    Event.LETS_GO_CLICK, Screen.AA_ROUTE_PREVIEW_SCREEN
                )
                startNavigationWithThisRoute()
            }
        }.build()

    private fun buildActionStrip(): ActionStrip {
//        val destinationAddress = generateDestinationAddress()
        return PreviewRouteActionStrip(routePreviewCarContext.mainCarContext, this,routePreviewData
        ).builder(true, routePreviewData.selectedDestination).build()
        //BREEZE2-1845
//        return PreviewRouteActionStrip(routePreviewCarContext.mainCarContext, this, { result ->
//            etaRequest = (result as? RecentETAContact)?.let { contact ->
//                ETAEngine.getInstance()?.createNewETARequest(
//                    ETAMode.Navigation, contact, destinationAddress, carContext.getString(
//                        R.string.car_share_message_route_planning,
//                        BreezeCarUtil.breezeUserPreferenceUtil.retrieveUserName(),
//                        destinationAddress.address1,
//                        Utils.formatTime((listRoute[selectedIndex].directionsRoute.duration() * 1000).toLong() + System.currentTimeMillis())
//                    )
//                )
//            }
//            ETAEngine.updateETPRequest(etaRequest)
//
//            updateRoutesOnMapWithSelection()
//            invalidate()
//        }, {
//            ETARouteDetails(
//                destinationAddress, listRoute[selectedIndex].directionsRoute
//            )
//        }, routePreviewData, etaRequest == null
//        ).builder(true, routePreviewData.selectedDestination).build()
    }

    private fun buildRouteList(): ItemList {
        val listBuilder = ItemList.Builder()
        val routesLength = directionsRoutes.size

        if (routesLength == 0) {
            listBuilder.setNoItemsMessage(carContext.getString(R.string.dialog_msg_no_route_found))
        } else {
            for (i in 0 until routesLength) {
                val routeAdapterObject = directionsRoutes[i]
                val subTitle = routePreviewData.destinationSearchLocation?.address1
                    ?: carContext.getString(R.string.route_preview)

                val firstRoute = SpannableStringBuilder("")

//            //Following icon span is not in design : BREEZES-4920
//            if (SHOW_RANDOM_ICON) {
//                firstRoute.appendCarSpan(
//                    CarIconSpan.create(
//                        CarIcon.Builder(
//                            IconCompat.createWithResource(
//                                carContext, R.drawable.mapbox_car_ic_volume_on
//                            )
//                        ).build()
//                    ),
//                    Spanned.SPAN_COMPOSING
//                )
//            }

                firstRoute.append(subTitle)

                if (routeAdapterObject.duration > 0) {
                    firstRoute.append(SEPARATOR_DOT)
                    firstRoute.appendCarSpan(
                        DurationSpan.create(routeAdapterObject.duration * 60L)
                    )
                }

//            val route: DirectionsRoute = routeAdapterObject.route
//
//            val arrivalTime = routeAdapterObject.arrivalTime
//            val parts: List<String> = arrivalTime.split(" ")
//            val arrival = parts[0]
//            val second = parts[1]
//
//            //var time = String.format("%d", routeAdapterObject.duration)


                val chargeAmountStr = routeAdapterObject.erpRate?.let { rate ->
                    carContext.getString(R.string.route_erp_format, max(rate, 0.0f))
                } ?: ""

                //Displaying distance
                val distance = if (routeAdapterObject.distance < 1000) {
                    Distance.create(
                        routeAdapterObject.distance.toDouble(), Distance.UNIT_METERS
                    )
                } else {
                    Distance.create(
                        Utils.convertMeterToKilometer(routeAdapterObject.distance),
                        Distance.UNIT_KILOMETERS
                    )
                }
                val distanceSpan = DistanceSpan.create(distance)
                val amountSpan = SpannableString(chargeAmountStr).apply {
                    setSpan(
                        ForegroundCarColorSpan.create(CarColor.RED),
                        0,
                        chargeAmountStr.length,
                        Spanned.SPAN_INCLUSIVE_EXCLUSIVE
                    )
                }

                val distancePriceText = SpannableStringBuilder("")
                distancePriceText.appendCarSpan(distanceSpan)
                distancePriceText.append(SEPARATOR_DOT)
                distancePriceText.append(amountSpan)

                listBuilder.addItem(
                    Row.Builder().setTitle(firstRoute).addText(distancePriceText).build()
                )
            }
            /**
             * event click list preview route
             */
            if (listRoute.isNotEmpty()) {
                listBuilder.setOnSelectedListener { index ->
                    selectedIndex = index
                    updateRoutesOnMapWithSelection()
                }
                listBuilder.setSelectedIndex(selectedIndex)
            }
        }

        return listBuilder.build()
    }

    private fun updateRoutesOnMapWithSelection() {
        if (listRoute.isNotEmpty()) {
            if (selectedIndex > 0 && selectedIndex < listRoute.size) {
                val newRouteOrder = listRoute.toMutableList()
                val swap = newRouteOrder[0]
                newRouteOrder[0] = newRouteOrder[selectedIndex]
                newRouteOrder[selectedIndex] = swap
                MapboxNavigationApp.current()?.setNavigationRoutes(newRouteOrder)
            } else {
                MapboxNavigationApp.current()?.setNavigationRoutes(listRoute)
            }
        }
    }

    fun interface RouteDetailHandler {

        /**
         * returns the selected ETARouteDetails
         * Utils.addTimeToTimeStamp(time!![0], time!![1],routeDepartureDetails.timestamp)
         */
        fun getSelectedRoute(): ETARouteDetails
    }

}

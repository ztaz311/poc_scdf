package com.ncs.breeze.customview

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import com.breeze.model.constants.Constants
import com.ncs.breeze.R
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import java.util.concurrent.TimeUnit
import kotlin.properties.Delegates


class CustomToastBreeze @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : RelativeLayout(context, attrs, defStyle) {

    var mContext: Context? = null
    var imgIcon: ImageView? = null
    var tvDes: TextView? = null
    var mIcon by Delegates.notNull<Int>()
    var description by Delegates.notNull<Int>()
    var compositeDisposable: CompositeDisposable = CompositeDisposable()
    var isStopLifeCycle = false

    init {
        LayoutInflater.from(context).inflate(R.layout.layout_custom_toast, this, true)
        mContext = context
        initializeAttributes(attrs)
        initializeView()
    }


    private fun initializeAttributes(attrs: AttributeSet?) {
        attrs?.let {
            val typedArray = context.obtainStyledAttributes(
                it,
                R.styleable.custom_toast, 0, 0
            )
            mIcon = typedArray.getResourceId(R.styleable.custom_toast_icon, 0)
            description = typedArray.getResourceId(R.styleable.custom_toast_description, 0)
            typedArray.recycle()
        }
    }

    fun setData(str: String, iconID: Int = -1) {
        tvDes?.text = str
        if (iconID != -1) {
            imgIcon?.setImageResource(iconID)
        }
    }


    private fun initializeView() {
        imgIcon = findViewById(R.id.img_eta_toast)
        tvDes = findViewById(R.id.eta_toast_text)
        imgIcon?.setImageResource(mIcon)
        tvDes?.setText(description)
    }

    fun show() {
        Observable.interval(1000, TimeUnit.MILLISECONDS)
            .take((Constants.CRUISE_MODE_TOAST_DURATION / 1000).toLong()).observeOn(
                AndroidSchedulers.mainThread()
            ).subscribeBy(
                onNext = {
                    if (!isStopLifeCycle) {
                        this.visibility = View.VISIBLE
                    }
                },
                onComplete = {
                    if (!isStopLifeCycle) {
                        this.visibility = View.GONE
                    }
                }
            ).addTo(compositeDisposable)
    }

    fun destroy() {
        compositeDisposable.dispose()
        this.visibility = View.GONE
    }


    fun stopToastETA() {
        isStopLifeCycle = true
        this.visibility = View.GONE
    }

    fun resumeIfNeed() {
        isStopLifeCycle = false
    }


}
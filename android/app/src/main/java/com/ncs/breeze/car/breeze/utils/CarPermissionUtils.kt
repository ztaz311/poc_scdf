package com.ncs.breeze.car.breeze.utils

import android.Manifest
import android.content.pm.PackageManager
import androidx.car.app.CarContext
import androidx.core.app.ActivityCompat

object CarPermissionUtils {

    fun hasEnoughPermissions(carContext: CarContext): Boolean {
        return isPermissionGranted(carContext, Manifest.permission.ACCESS_FINE_LOCATION) &&
                isPermissionGranted(carContext, Manifest.permission.ACCESS_COARSE_LOCATION)
    }

    private fun isPermissionGranted(carContext: CarContext, permission: String): Boolean =
        ActivityCompat.checkSelfPermission(
            carContext.applicationContext,
            permission
        ) == PackageManager.PERMISSION_GRANTED
}
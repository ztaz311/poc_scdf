package com.ncs.breeze.ui.dashboard.fragments.sharelocation

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.annotation.UiThread
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.ViewModelProvider
import com.breeze.model.DestinationAddressDetails
import com.breeze.model.SharedLocation
import com.breeze.model.constants.Constants
import com.breeze.model.enums.AmenityBand
import com.breeze.model.extensions.dpToPx
import com.breeze.model.extensions.rn.getIntOrNull
import com.breeze.model.extensions.rn.getMapOrNull
import com.breeze.model.extensions.toBitmap
import com.facebook.react.bridge.Arguments
import com.mapbox.android.gestures.MoveGestureDetector
import com.mapbox.geojson.Geometry
import com.mapbox.geojson.GeometryCollection
import com.mapbox.maps.CameraOptions
import com.mapbox.maps.ViewAnnotationAnchor
import com.mapbox.maps.plugin.LocationPuck2D
import com.mapbox.maps.plugin.animation.MapAnimationOptions
import com.mapbox.maps.plugin.animation.easeTo
import com.mapbox.maps.plugin.annotation.annotations
import com.mapbox.maps.plugin.annotation.generated.OnPointAnnotationClickListener
import com.mapbox.maps.plugin.annotation.generated.PointAnnotationManager
import com.mapbox.maps.plugin.annotation.generated.PointAnnotationOptions
import com.mapbox.maps.plugin.annotation.generated.createPointAnnotationManager
import com.mapbox.maps.plugin.gestures.OnMoveListener
import com.mapbox.maps.plugin.gestures.gestures
import com.mapbox.maps.plugin.locationcomponent.location
import com.mapbox.maps.viewannotation.viewAnnotationOptions
import com.mapbox.navigation.utils.internal.toPoint
import com.ncs.breeze.R
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.Event
import com.ncs.breeze.common.analytics.Screen
import com.ncs.breeze.common.constant.RNScreen
import com.ncs.breeze.common.extensions.android.getRNFragmentNavigationParams
import com.ncs.breeze.common.extensions.android.getShareBusinessLogicPackage
import com.ncs.breeze.common.extensions.mapbox.setBreezeDefaultOptions
import com.ncs.breeze.common.remote.MyServiceInterceptor
import com.ncs.breeze.common.rxbus.RxBus
import com.ncs.breeze.common.rxbus.RxEvent
import com.ncs.breeze.common.utils.LocationBreezeManager
import com.ncs.breeze.databinding.FragmentSharedLocationBinding
import com.ncs.breeze.databinding.MapTooltipSharedLocationBinding
import com.ncs.breeze.di.Injectable
import com.ncs.breeze.reactnative.nativemodule.ShareBusinessLogicEvent
import com.ncs.breeze.ui.base.BaseActivity
import com.ncs.breeze.ui.dashboard.activity.DashboardActivity
import com.ncs.breeze.ui.rn.CustomReactFragment
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class SharedLocationFragment : Fragment(), Injectable {
    private var sharedLocationToken: String? = null

    @Inject
    lateinit var myServiceInterceptor: MyServiceInterceptor

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel: SharedLocationViewModel by viewModels { viewModelFactory }

    private val compositeDisposable = CompositeDisposable()

    private var annotationManager: PointAnnotationManager? = null

    private lateinit var viewBinding: FragmentSharedLocationBinding

    private var tooltipViewBinding: MapTooltipSharedLocationBinding? = null


    private val onMoveListener = object : OnMoveListener {
        override fun onMoveBegin(detector: MoveGestureDetector) {
            if (!viewBinding.buttonRecenter.isVisible)
                viewBinding.buttonRecenter.isVisible = true
        }

        override fun onMove(detector: MoveGestureDetector): Boolean {
            return false
        }

        override fun onMoveEnd(detector: MoveGestureDetector) {}
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sharedLocationToken = arguments?.getString(ARG_SHARED_LOCATION_TOKEN)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewBinding = FragmentSharedLocationBinding.inflate(inflater, container, false)
        return viewBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewBinding.buttonRecenter.setOnClickListener {
            recenterMap()
            unselectSharedLocation()
            it.isVisible = false
        }

        annotationManager = viewBinding.mapView.annotations.createPointAnnotationManager().apply {
            addClickListener(OnPointAnnotationClickListener { point ->
                if (point.point == viewModel.sharedLocation?.getLocation()) {
                    tooltipViewBinding?.root?.isVisible = true
                    viewModel.isSharedLocationSelected = true
                    zoomToSharedLocation(true)
                    refreshSharedLocationAnnotation()
                }
                true
            })
        }
        initMapStyle {
            with(viewBinding.mapView) {
                gestures.addOnMoveListener(onMoveListener)
                setBreezeDefaultOptions()
                location.enabled = true
                location.pulsingEnabled = true
                location.locationPuck = LocationPuck2D(
                    ContextCompat.getDrawable(context, R.drawable.cursor)
                )
            }
            sharedLocationToken?.let { token ->
                viewModel.fetchSharedLocation(token) { sharedLocation ->
                    viewModel.sharedLocation = sharedLocation
                    viewModel.isSharedLocationSelected = true
                    showRNScreen(sharedLocation)
                    showSharedLocationAnnotation()
                    updateButtonRecenterPosition()
                }
            }
        }
    }

    private fun updateButtonRecenterPosition() {
        val sharedLocation = viewModel.sharedLocation ?: return
        val layoutParams =
            (viewBinding.buttonRecenter.layoutParams as? FrameLayout.LayoutParams) ?: return
        layoutParams.setMargins(
            layoutParams.leftMargin,
            layoutParams.topMargin,
            layoutParams.rightMargin,
            if (sharedLocation.saved) 32.dpToPx() else 120.dpToPx()
        )
        viewBinding.buttonRecenter.layoutParams = layoutParams
    }


    private fun unselectSharedLocation() {
        viewModel.isSharedLocationSelected = false
        tooltipViewBinding?.root?.isInvisible = true
        refreshSharedLocationAnnotation()
    }

    private fun recenterMap() {
        LocationBreezeManager.getInstance().currentLocation?.toPoint()?.let { currentLocation ->
            val points = arrayListOf<Geometry>(currentLocation)
            viewModel.sharedLocation?.getLocation()?.let { location -> points.add(location) }
            val cameraOptions = viewBinding.mapView.getMapboxMap().cameraForGeometry(
                GeometryCollection.fromGeometries(points),
                viewModel.mapCameraPadding,
                bearing = 0.0,
                pitch = 0.0
            )
            val zoom = (cameraOptions.zoom
                ?: Constants.MAX_ZOOM_LEVEL).coerceAtMost(Constants.MAX_ZOOM_LEVEL)
                .coerceAtLeast(10.0)
            viewBinding.mapView.getMapboxMap()
                .easeTo(cameraOptions.toBuilder().zoom(zoom).build(),
                    MapAnimationOptions.mapAnimationOptions {
                        duration(400L)
                    })
        }
    }

    private fun zoomToSharedLocation(hasAnimation: Boolean) {
        val sharedLocationPoint = viewModel.sharedLocation?.getLocation() ?: return
        val cameraOptions = CameraOptions.Builder()
            .padding(viewModel.mapCameraPadding)
            .center(sharedLocationPoint)
            .bearing(0.0)
            .pitch(0.0)
            .zoom(Constants.MAX_ZOOM_LEVEL)
            .build()

        if (hasAnimation)
            viewBinding.mapView.getMapboxMap()
                .easeTo(cameraOptions,
                    MapAnimationOptions.mapAnimationOptions {
                        duration(400L)
                    })
        else viewBinding.mapView.getMapboxMap().setCamera(cameraOptions)
        viewBinding.buttonRecenter.isVisible = true
        tooltipViewBinding?.root?.isVisible = true
    }

    @UiThread
    private fun showSharedLocationAnnotation() {
        zoomToSharedLocation(false)
        refreshSharedLocationAnnotation()
        refreshSharedLocationTooltip()
    }

    private fun refreshSharedLocationTooltip() {
        val sharedLocation = viewModel.sharedLocation ?: return
        val locationPoint = sharedLocation.getLocation() ?: return
        if (tooltipViewBinding == null) {
            val tooltipView = viewBinding.mapView.viewAnnotationManager.addViewAnnotation(
                R.layout.map_tooltip_shared_location,
                viewAnnotationOptions {
                    geometry(locationPoint)
                    anchor(ViewAnnotationAnchor.BOTTOM)

                })
            tooltipViewBinding = MapTooltipSharedLocationBinding.bind(tooltipView)
        }
        bindTooltip(sharedLocation)
    }

    private fun bindTooltip(sharedLocation: SharedLocation) {
        tooltipViewBinding?.tvName?.text =
            sharedLocation.name?.takeIf { it.isNotBlank() } ?: sharedLocation.address1
        val description =
            if (!sharedLocation.fullAddress.isNullOrBlank()) sharedLocation.fullAddress
            else if (!sharedLocation.address2.isNullOrBlank()) sharedLocation.address2
            else if (!sharedLocation.address1.isNullOrBlank()) sharedLocation.address1
            else {
                "${sharedLocation.lat},${sharedLocation.long}"
            }
        displayCrowdsourceInfo(sharedLocation)
        tooltipViewBinding?.tvDescription?.text = description
        tooltipViewBinding?.buttonNavigateHere?.setOnClickListener {
            Analytics.logClickEvent(
                Event.MAP_TOOLTIP_SAVED_NAVIGATE_HERE,
                Screen.SHARED_COLLECTION_RECIPIENT
            )
            onNavigateHereClicked(sharedLocation)
        }
        tooltipViewBinding?.buttonShare?.setOnClickListener {
            Analytics.logClickEvent(
                "[map]:tooltip_saved_share",
                Screen.SHARED_COLLECTION_RECIPIENT
            )
            shareLocation(sharedLocation)
        }
        tooltipViewBinding?.buttonInvite?.setOnClickListener {
            Analytics.logClickEvent(
                "[map]:tooltip_saved_invite",
                Screen.SHARED_COLLECTION_RECIPIENT
            )
            inviteLocation(sharedLocation)
        }

        tooltipViewBinding?.imgFlagSaved?.run {
            setImageDrawable(
                ContextCompat.getDrawable(
                    this.context,
                    if (sharedLocation.saved) R.drawable.ic_bookmarked else R.drawable.ic_nobookmark
                )
            )
            setOnClickListener {
                onTooltipBookmarkIndicatorClicked(sharedLocation)
            }
        }
    }

    private fun displayCrowdsourceInfo(sharedLocation: SharedLocation) {
        val availabilityCSData = sharedLocation.availabilityCSData
        tooltipViewBinding?.layoutCarparkStatus?.root?.isVisible = availabilityCSData != null
        if (availabilityCSData != null) {
//            Calculate last update time
            val timeUpdate =
                "${availabilityCSData.primaryDesc ?: viewBinding.root.context.getString(com.breeze.customization.R.string.update_carpark_stt_time)} ${availabilityCSData.generateDisplayedLastUpdate()}"
            val drawableHeaderCpStatus =
                AppCompatResources.getDrawable(viewBinding.root.context, R.drawable.bg_header_carpark_status)
            drawableHeaderCpStatus?.setTint(Color.parseColor(availabilityCSData.themeColor ?: "#F26415"))
            tooltipViewBinding?.layoutCarparkStatus?.root?.background = drawableHeaderCpStatus
            tooltipViewBinding?.layoutCarparkStatus?.icCarparkStatus?.setImageResource(
                when (sharedLocation.availablePercentImageBand) {
                    AmenityBand.TYPE0.type -> R.drawable.ic_cp_stt_full
                    AmenityBand.TYPE2.type -> R.drawable.ic_cp_stt_available
                    else -> R.drawable.ic_cp_stt_few
                }
            )
            tooltipViewBinding?.layoutCarparkStatus?.tvStatusCarpark?.text = availabilityCSData.title ?: ""
            tooltipViewBinding?.layoutCarparkStatus?.tvTimeUpdateCarparkStatus?.text = timeUpdate
        }
    }

    private fun onTooltipBookmarkIndicatorClicked(sharedLocation: SharedLocation) {
        if (sharedLocation.saved) {
            sharedLocation.bookmarkId?.let { deletedBookmarkId ->
                Analytics.logClickEvent(
                    Event.MAP_TOOLTIP_SAVED_UNBOOKMARK,
                    Screen.SHARED_COLLECTION_RECIPIENT
                )
                deleteBookmark(deletedBookmarkId) {
                    viewModel.sharedLocation?.run {
                        bookmarkId = null
                        saved = false
                    }
                    updateButtonRecenterPosition()
                    refreshSharedLocationAnnotation()
                    refreshSharedLocationTooltip()
                }
            }
        } else bookmarkSharedLocation(sharedLocation)
    }

    override fun onStart() {
        super.onStart()
        listenRxEvents()
    }

    private fun listenRxEvents() {
        compositeDisposable.add(RxBus.listen(RxEvent.DidSaveSharedLocation::class.java)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                viewModel.sharedLocation?.bookmarkId = it.bookmarkId
                viewModel.sharedLocation?.saved = true
                activity?.runOnUiThread {
                    bindTooltip(viewModel.sharedLocation!!)
                }
            })
    }

    override fun onStop() {
        super.onStop()
        compositeDisposable.dispose()
    }

    private fun refreshSharedLocationAnnotation() {
        val locationPoint = viewModel.sharedLocation?.getLocation() ?: return
        annotationManager?.deleteAll()
        annotationManager?.create(
            PointAnnotationOptions()
                .withPoint(locationPoint)
                .withIconImage(
                    viewModel.getSharedLocationAnnotationRes().toBitmap(requireContext())!!
                )
        )
    }

    private fun deleteBookmark(bookmarkId: Int, onDeleted: (Int) -> Unit) {
        viewBinding.root.context?.getShareBusinessLogicPackage()?.getRequestsModule()
            ?.flatMap {
                val data = Arguments.fromBundle(
                    bundleOf(
                        "action" to "DELETE",
                        "bookmarkId" to bookmarkId
                    )
                )
                it.callRN(
                    ShareBusinessLogicEvent.TOGGLE_SAVE_LOCATION_SHARED_COLLECTION.name, data
                )
            }?.subscribeOn(Schedulers.io())?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe { result ->
                // Check if by the time result arrived,  user still in same pin
                result.getMapOrNull("data")?.getIntOrNull("bookmarkId")?.takeIf { it == bookmarkId }
                    ?.let { deletedBookmarkId -> onDeleted.invoke(deletedBookmarkId) }

            }?.also { compositeDisposable.add(it) }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        compositeDisposable.dispose()
    }

    private fun bookmarkSharedLocation(sharedLocation: SharedLocation) {
        Analytics.logClickEvent(
            Event.MAP_TOOLTIP_SAVED_BOOKMARK,
            Screen.SHARED_COLLECTION_RECIPIENT
        )
        viewBinding.root.context?.getShareBusinessLogicPackage()?.getRequestsModule()
            ?.flatMap {
                val data = Arguments.fromBundle(
                    bundleOf(
                        "action" to "ADD",
                        "bookmark" to bundleOf(
                            "lat" to sharedLocation.lat,
                            "long" to sharedLocation.long,
                            "address1" to sharedLocation.address1,
                            "name" to sharedLocation.name,
                            "address2" to sharedLocation.address2,
                            "amenityType" to sharedLocation.amenityType,
                            "amenityId" to sharedLocation.amenityId,
                            "placeId" to sharedLocation.placeId,
                            "isDestination" to sharedLocation.isDestination,
                            "code" to sharedLocation.code,
                            "userLocationLinkId" to sharedLocation.userLocationLinkId,
                            "fullAddress" to (sharedLocation.fullAddress),
                            "placeId" to (sharedLocation.placeId),
                            "userLocationLinkIdRef" to (sharedLocation.userLocationLinkIdRef),
                            "showAvailabilityFB" to (sharedLocation.showAvailabilityFB),
                        )
                    )
                )
                it.callRN(
                    ShareBusinessLogicEvent.TOGGLE_SAVE_LOCATION_SHARED_COLLECTION.name,
                    data
                )
            }?.subscribeOn(Schedulers.io())?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe()
    }

    private fun createShareLocationData(sharedLocation: SharedLocation) = Arguments.fromBundle(
        bundleOf(
            "address1" to sharedLocation.address1,
            "latitude" to sharedLocation.lat,
            "longitude" to sharedLocation.long,
            "amenityId" to sharedLocation.amenityId,
            "amenityType" to sharedLocation.amenityType,
            "name" to sharedLocation.name,
            "code" to sharedLocation.code,
            "isDestination" to sharedLocation.isDestination,
            "description" to (sharedLocation.description ?: ""),
            "fullAddress" to (sharedLocation.fullAddress),
            "bookmarkId" to sharedLocation.bookmarkId,
            "placeId" to sharedLocation.placeId,
            "userLocationLinkIdRef" to sharedLocation.userLocationLinkIdRef
        )
    )

    private fun shareLocation(sharedLocation: SharedLocation) {
        viewBinding.root.context.getShareBusinessLogicPackage()?.getRequestsModule()?.flatMap {
            it.callRN(
                ShareBusinessLogicEvent.SHARE_LOCATION_FROM_NATIVE.eventName,
                createShareLocationData(sharedLocation)
            )
        }?.subscribeOn(Schedulers.io())?.observeOn(AndroidSchedulers.mainThread())?.subscribe()
    }

    private fun inviteLocation(sharedLocation: SharedLocation) {
        viewBinding.root.context.getShareBusinessLogicPackage()?.getRequestsModule()?.flatMap {
            it.callRN(
                ShareBusinessLogicEvent.LOCATION_INVITE_FROM_NATIVE.eventName,
                createShareLocationData(sharedLocation)
            )
        }?.subscribeOn(Schedulers.io())?.observeOn(AndroidSchedulers.mainThread())?.subscribe()
    }

    private fun onNavigateHereClicked(sharedLocation: SharedLocation) {
        (activity as? DashboardActivity)?.takeIf {
            it.lifecycle.currentState.isAtLeast(
                Lifecycle.State.STARTED
            )
        }?.let { dashboardActivity ->
            val destinationDetail = sharedLocation.toDestinationDetails()
            val currentLocation = LocationBreezeManager.getInstance().currentLocation
            val sourceAddressDetails = DestinationAddressDetails(
                null,
                address1 = null,
                null,
                currentLocation?.latitude?.toString(),
                currentLocation?.longitude?.toString(),
                null,
                Constants.TAGS.CURRENT_LOCATION_TAG,
                null,
                -1
            )
            dashboardActivity.showRouteAndResetToCenter(
                bundleOf(
                    Constants.DESTINATION_ADDRESS_DETAILS to destinationDetail,
                    Constants.SOURCE_ADDRESS_DETAILS to sourceAddressDetails
                )
            )
        }
    }


    private fun initMapStyle(onFinished: () -> Unit = {}) {
        (activity as? BaseActivity<*, *>)?.let { baseActivity ->
            viewBinding.mapView.getMapboxMap().loadStyleUri(baseActivity.getMapboxStyle(true)) {
                onFinished.invoke()
            }
        }
    }

    private fun showRNScreen(sharedLocation: SharedLocation) {
        val navigationParams = requireActivity().getRNFragmentNavigationParams(
            sessionToken = myServiceInterceptor.getSessionToken(),
            fragmentTag = RNScreen.SHARED_LOCATION
        ).apply {
            putBundle("sharedLocation", sharedLocation.toBundle())
        }
        val initialProperties = Bundle().apply {
            putString(Constants.RN_TO_SCREEN, RNScreen.SHARED_LOCATION)
            putBundle(Constants.RN_CONSTANTS.NAVIGATION_PARAMS, navigationParams)
        }

        val reactNativeFragment = CustomReactFragment.newInstance(
            componentName = Constants.RN_CONSTANTS.COMPONENT_NAME,
            launchOptions = initialProperties,
            cleanOnDestroyed = false
        )

        childFragmentManager
            .beginTransaction()
            .replace(
                viewBinding.rnScreenContainer.id,
                reactNativeFragment,
                RNScreen.SHARED_LOCATION
            )
            .commit()
    }


    companion object {
        private const val ARG_SHARED_LOCATION_TOKEN = "SHARED_LOCATION_TOKEN"

        fun newInstance(token: String) = SharedLocationFragment().apply {
            arguments = bundleOf(
                ARG_SHARED_LOCATION_TOKEN to token
            )
        }
    }

}
package com.ncs.breeze.common.storage

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson

class BreezeGuestPreference private constructor(private val mContext: Context) {

    val sharedPreference: SharedPreferences
    private val SHARE_PREFERENCE_NAME = "Breeze_Guest_Preference"
    private val mGson: Gson

    companion object {
        private var breezeUserPreference: BreezeGuestPreference? = null
        fun getInstance(context: Context): BreezeGuestPreference? {
            if (null == breezeUserPreference) {
                breezeUserPreference = BreezeGuestPreference(context)
            }
            return breezeUserPreference
        }

        private const val KEY_ID_GUEST = "KEY_ID_GUEST"
    }

    init {
        sharedPreference =
            mContext.getSharedPreferences(SHARE_PREFERENCE_NAME, Context.MODE_PRIVATE)
        mGson = Gson()
    }

    fun getString(key: String): String? {
        return sharedPreference.getString(key, "")
    }

    fun saveString(key: String, value: String, commitTransaction: Boolean = false) {
        val editor = sharedPreference.edit()
        editor.putString(key, value)
        if (commitTransaction) {
            editor.commit()
        } else {
            editor.apply()
        }
    }

    /**
     * save guest id
     */
    fun saveGuestID(idGuest: String) {
        saveString(KEY_ID_GUEST, idGuest, true)
    }

    /**
     * get id guest
     */
    fun getGuestId(): String? {
        return sharedPreference.getString(KEY_ID_GUEST, "")
    }


    fun saveBoolean(key: String, value: Boolean) {
        val editor = sharedPreference.edit()
        editor.putBoolean(key, value)
        editor.apply()
    }

    fun getBoolean(key: String): Boolean {
        return sharedPreference.getBoolean(key, false)
    }

    fun saveLong(key: String, value: Long) {
        val editor = sharedPreference.edit()
        editor.putLong(key, value)
        editor.apply()
    }

    fun getLong(key: String): Long {
        return sharedPreference.getLong(key, 0)
    }

    fun saveInt(key: String, value: Int) {
        val editor = sharedPreference.edit()
        editor.putInt(key, value)
        editor.apply()
    }

    fun getInt(key: String): Int {
        return sharedPreference.getInt(key, 0)
    }
}
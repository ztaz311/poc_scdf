package com.ncs.breeze.car.breeze.screen.search.etaList

import androidx.car.app.OnScreenResultListener
import androidx.car.app.model.*
import androidx.core.graphics.drawable.IconCompat
import androidx.lifecycle.lifecycleScope
import com.mapbox.androidauto.internal.logAndroidAuto
import com.breeze.model.api.response.eta.RecentETAContact
import com.breeze.model.enums.ETAMode
import com.ncs.breeze.car.breeze.MainBreezeCarContext
import com.ncs.breeze.car.breeze.base.BaseScreenCar
import com.ncs.breeze.car.breeze.component.ContactInitialsRenderer
import com.ncs.breeze.car.breeze.model.ETARouteDetails
import com.ncs.breeze.car.breeze.screen.confirmation.share.ShareConfirmationScreen
import com.ncs.breeze.car.breeze.utils.BreezeCarUtil
import com.ncs.breeze.car.breeze.utils.LimitClick
import com.ncs.breeze.car.breeze.utils.setClickSafe
import com.ncs.breeze.R
import com.ncs.breeze.common.analytics.Screen
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ETAListScreen(
    val mainBreezeCarContext: MainBreezeCarContext,
    private val etaMode: ETAMode,
    private val etaRouteDetails: ETARouteDetails?,
    private val onScreenResultListener: OnScreenResultListener,
) :
    BaseScreenCar(mainBreezeCarContext.carContext) {
    private val listData = arrayListOf<RecentETAContact>()
    var isLoadingData = true

    override fun onCreateScreen() {
        super.onCreateScreen()
        getEtaListFavorite()

    }

    override fun getScreenName(): String {
        return Screen.AA_NOTIFY_ARRIVAL_LIST_SCREEN
    }


    override fun onGetTemplate(): Template {
        return buildTemplate()
    }


    private fun buildTemplate(): Template {
        val listTemplateBuilder = ListTemplate.Builder()
            .setLoading(isLoadingData)
            .setHeaderAction(Action.BACK)
            .setTitle(carContext.getString(R.string.select_from_recent_contacts))
        if (!isLoadingData) {
            listTemplateBuilder.setSingleList(generateItemList())
            if (listData.isEmpty())
                return buildEmptyListTemplate()
        }
        return listTemplateBuilder.build()
    }

    private fun buildEmptyListTemplate() =
        MessageTemplate.Builder(carContext.getString(R.string.empty_list_noty_arrival))
            .setTitle("Share Drive")
            .addAction(
                Action.Builder()
                    .setBackgroundColor(CarColor.BLUE)
                    .setTitle("OK")
                    .setOnClickListener {
                        finish()
                    }.build()
            ).build()


    private fun generateItemList(): ItemList {
        val listBuilder = ItemList.Builder()
        listData.forEach { favEta ->
            if (favEta.recipientName == null) {
                return@forEach
            }
            val row = Row.Builder()
                .setTitle(favEta.recipientName!!)
                .setClickSafe {
                    LimitClick.handleSafe {
                        finish()
                        CoroutineScope(Dispatchers.Main).launch {
                            screenManager.push(
                                ShareConfirmationScreen(
                                    mainBreezeCarContext,
                                    etaMode,
                                    favEta,
                                    etaRouteDetails,
                                    onScreenResultListener
                                )
                            )
                        }
                    }
                }
            ContactInitialsRenderer.render(favEta.recipientName!!)?.let {
                row.setImage(
                    CarIcon.Builder(
                        IconCompat.createWithBitmap(it)
                    ).build()
                )
            }
            listBuilder.addItem(row.build())
        }

        return listBuilder.build()

    }

    private fun getEtaListFavorite() {
        lifecycleScope.launch(Dispatchers.IO) {
            listData.addAll(BreezeCarUtil.apiHelper.getRecentETAContacts())
            isLoadingData = false
            invalidate()
        }
    }

    override fun onStartScreen() {
        super.onStartScreen()
        logAndroidAuto("CarNavigateScreen onStart")
    }

    override fun onStopScreen() {
        super.onStopScreen()
        logAndroidAuto("CarNavigateScreen onStop")
    }
}
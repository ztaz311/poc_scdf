package com.ncs.breeze.ui.dashboard.fragments.obuconnect

import android.content.Context
import android.os.Bundle
import android.text.InputFilter
import android.text.InputType
import android.text.Spanned
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat.getSystemService
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.Event
import com.ncs.breeze.common.analytics.Screen
import com.ncs.breeze.common.extensions.android.getOBUConnHelper
import com.ncs.breeze.common.extensions.android.getUserDataPreference
import com.breeze.model.obu.OBUDevice
import com.breeze.model.obu.OBUInitialConnectionStep
import com.ncs.breeze.databinding.FragmentObuConnectSecondStateBinding
import com.ncs.breeze.ui.dashboard.fragments.viewmodel.OBUConnectSecondStateViewModel
import com.ncs.breeze.ui.dashboard.fragments.viewmodel.OBUConnectViewModel


class OBUConnectSecondStateFragment : Fragment() {
    private lateinit var viewBinding: FragmentObuConnectSecondStateBinding
    private val parentViewModel: OBUConnectViewModel by viewModels({ requireParentFragment() })
    private val viewModel: OBUConnectSecondStateViewModel by viewModels({ this })

    override fun onAttach(context: Context) {
        super.onAttach(context)
        context.getUserDataPreference()?.getConnectedOBUDevices()
            ?.map { device ->
                device.vehicleNumber.takeUnless { it.isBlank() || it.isEmpty() } ?: device.obuName
            }
            ?.takeIf { it.isNotEmpty() }
            ?.let { viewModel.existingVehicleNames.addAll(it) }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewBinding = FragmentObuConnectSecondStateBinding.inflate(inflater, container, false)
        return viewBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewBinding.layoutFooter.btnConfirm.setOnClickListener {
            Analytics.logClickEvent(Event.OBU_PAIRING_NUMBER_PLATE_NEXT, getScreenName())
            getSystemService(it.context, InputMethodManager::class.java)
                ?.hideSoftInputFromWindow(view.windowToken, 0)
            val vehicleName = viewBinding.editVehicleName.getEnteredText().trim()
            val obuDevice =
                OBUDevice(obuName = parentViewModel.currentObu!!.name, vehicleNumber = vehicleName)
            if (viewModel.isVehicleNameExisted(obuDevice)) {
                showDuplicatedVehicleNameDialog()
            } else {
                parentViewModel.obuInitialConnectionStep.postValue(OBUInitialConnectionStep.OUTRO)
                connectOBU(vehicleName)
            }
        }

        viewBinding.editVehicleName.getEditText().run {
            inputType = InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS
            setOnFocusChangeListener { _, hasFocus ->
                if (!hasFocus) {
                    getSystemService(context, InputMethodManager::class.java)
                        ?.hideSoftInputFromWindow(view.windowToken, 0)

                    Analytics.logEditEvent(Event.OBU_PAIRING_NUMBER_PLATE_INPUT, getScreenName())
                }
            }

            filters = arrayOf(
                InputFilter.LengthFilter(8),
                object : InputFilter {
                    override fun filter(
                        source: CharSequence?,
                        start: Int,
                        end: Int,
                        dest: Spanned?,
                        dstart: Int,
                        dend: Int
                    ): CharSequence {
                        return if (viewModel.isValidVehicleName(source)) {
                            source ?: ""
                        } else ""
                    }
                }
            )
        }

        viewBinding.editVehicleName.getEditText().run {
            imeOptions = EditorInfo.IME_ACTION_NEXT
            setOnEditorActionListener { v, actionId, event ->
                if (actionId == EditorInfo.IME_ACTION_NEXT && viewModel.isValidVehicleName(text)) {
                    viewBinding.layoutFooter.btnConfirm.performClick()
                    return@setOnEditorActionListener true
                }
                return@setOnEditorActionListener false
            }
        }

        activity?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)
    }

    private fun getScreenName() = Screen.OBU_PAIRING_SCREEN

    private fun showDuplicatedVehicleNameDialog() {
        if (context == null) return
        AlertDialog.Builder(requireContext())
            .setMessage("You have used that name before. Please pick another.")
            .show()
    }

    private fun connectOBU(vehicleName: String) {
        parentViewModel.currentObu?.let { obu ->
            activity?.getOBUConnHelper()?.startOBUConnection(
                vehicleNumber = vehicleName,
                obu = obu
            )
        }
    }
}
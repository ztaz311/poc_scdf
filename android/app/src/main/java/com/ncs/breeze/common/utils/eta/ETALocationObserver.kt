package com.ncs.breeze.common.utils.eta

import com.breeze.model.enums.ETAMode
import com.ncs.breeze.common.utils.LocationBreezeManager
import com.mapbox.android.core.location.LocationEngineResult


class ETALocationObserver(val mode: ETAMode, val etaEngine: ETAEngine) :
    LocationBreezeManager.CallBackLocationBreezeManager {
    override fun onSuccess(result: LocationEngineResult) {
        if (mode == ETAMode.Cruise) {
            etaEngine.updateLocationForCruise(result.lastLocation)
        } else {
            etaEngine.updateLocation(result.lastLocation)
        }

    }
}
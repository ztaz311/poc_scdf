package com.ncs.breeze.ui.dashboard.fragments.viewmodel

import android.app.Application
import com.amplifyframework.auth.exceptions.NotAuthorizedException
import com.amplifyframework.core.Amplify
import com.ncs.breeze.common.remote.ApiHelper
import com.ncs.breeze.common.utils.AWSUtils
import com.ncs.breeze.common.utils.Utils
import com.ncs.breeze.components.SingleLiveEvent
import com.ncs.breeze.ui.base.BaseFragmentViewModel
import timber.log.Timber
import javax.inject.Inject

class ChangePasswordViewModel @Inject constructor(apiHelper: ApiHelper, application: Application) :
    BaseFragmentViewModel(
        application
    ) {
    var mAPIHelper: ApiHelper = apiHelper
    var changePasswordSuccessful: SingleLiveEvent<Boolean> = SingleLiveEvent()
    var changePasswordUnsuccessful: SingleLiveEvent<Boolean> = SingleLiveEvent()

    fun changePassword(oldPassword: String, newPassword: String) {
        AWSUtils.updatePassword(oldPassword, newPassword,
            {
                Utils.runOnMainThread {
                    changePasswordSuccessful.value = true
                    Timber.i("Updated password successfully")
                }
            },
            {
                Utils.runOnMainThread {
                    if (it.cause is NotAuthorizedException) {
                        changePasswordUnsuccessful.value = true
                    }
                    Timber.e("Password update failed  $it")
                }
            }
        )
    }
}
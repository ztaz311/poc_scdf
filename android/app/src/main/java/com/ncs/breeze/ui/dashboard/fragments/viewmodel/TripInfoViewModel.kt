package com.ncs.breeze.ui.dashboard.fragments.viewmodel

import android.app.Application
import com.google.gson.JsonElement
import com.breeze.model.api.ErrorData
import com.breeze.model.api.request.TripFileRequest
import com.breeze.model.api.request.TripIdRequest
import com.breeze.model.api.request.TripUpdateRequest
import com.breeze.model.api.response.DeleteFileResponse
import com.breeze.model.api.response.RetrieveFileResponse
import com.breeze.model.api.response.UploadFileResponse
import com.ncs.breeze.common.remote.ApiHelper
import com.ncs.breeze.common.remote.ApiObserver
import com.ncs.breeze.components.SingleLiveEvent
import com.ncs.breeze.ui.base.BaseFragmentViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject

class TripInfoViewModel @Inject constructor(apiHelper: ApiHelper, application: Application) :
    BaseFragmentViewModel(application) {
    var apiHelper: ApiHelper = apiHelper
    var updateAPISuccessful: SingleLiveEvent<Boolean> = SingleLiveEvent()

    var uploadReceiptAPISuccessful: SingleLiveEvent<UploadFileResponse> = SingleLiveEvent()
    var uploadReceiptAPIFailure: SingleLiveEvent<Boolean> = SingleLiveEvent()

    var retrieveAPISuccessful: SingleLiveEvent<RetrieveFileResponse> = SingleLiveEvent()
    var retrieveAPIFailure: SingleLiveEvent<Boolean> = SingleLiveEvent()

    var deleteAPISuccessful: SingleLiveEvent<DeleteFileResponse> = SingleLiveEvent()
    var deleteAPIFailure: SingleLiveEvent<Boolean> = SingleLiveEvent()


    fun updateTripDetails(tripId: Long, tripUpdateRequest: TripUpdateRequest) {
        apiHelper.updateTripHistory(tripId, tripUpdateRequest)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : ApiObserver<JsonElement>(compositeDisposable) {
                override fun onSuccess(data: JsonElement) {
                    updateAPISuccessful.value = true
                }

                override fun onError(e: ErrorData) {
                    updateAPISuccessful.value = false
                }

            })
    }


    fun retrieveParkingReceipt(tripId: Long) {
        apiHelper.retrieveFile(tripId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : ApiObserver<RetrieveFileResponse>(compositeDisposable) {
                override fun onSuccess(data: RetrieveFileResponse) {
                    Timber.d("API Successful: " + data.toString())
                    retrieveAPISuccessful.value = data
                }

                override fun onError(e: ErrorData) {
                    retrieveAPIFailure.value = false
                    Timber.d("API failed: " + e.toString())
                }

            })
    }


    fun uploadParkingReceipt(mTripFileRequest: TripFileRequest) {
        apiHelper.uploadParkingFile(mTripFileRequest)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : ApiObserver<UploadFileResponse>(compositeDisposable) {
                override fun onSuccess(data: UploadFileResponse) {
                    Timber.d("API Successful: " + data.toString())
                    uploadReceiptAPISuccessful.value = data
                }

                override fun onError(e: ErrorData) {
                    uploadReceiptAPIFailure.value = true
                    Timber.d("API failed: " + e.toString())
                }

            })
    }

    fun deleteparkingReceipt(tripId: TripIdRequest) {
        apiHelper.deleteFile(tripId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : ApiObserver<DeleteFileResponse>(compositeDisposable) {
                override fun onSuccess(data: DeleteFileResponse) {
                    deleteAPISuccessful.value = data

                    Timber.d("API Successful: " + data.toString())
                }

                override fun onError(e: ErrorData) {
                    deleteAPIFailure.value = false
                    Timber.d("API failed: " + e.toString())
                }

            })
    }
}
package com.ncs.breeze.car.breeze.screen.homescreen

import android.annotation.SuppressLint
import androidx.car.app.ScreenManager
import androidx.car.app.model.Action
import androidx.car.app.model.Action.FLAG_IS_PERSISTENT
import androidx.car.app.model.ActionStrip
import com.breeze.model.extensions.round
import com.ncs.breeze.R
import com.ncs.breeze.car.breeze.MainBreezeCarContext
import com.ncs.breeze.car.breeze.screen.carkpark.CarParkCarContext
import com.ncs.breeze.car.breeze.screen.carkpark.CarParkScreen
import com.ncs.breeze.car.breeze.screen.obustatus.OBUDeviceConnectionStatusScreen
import com.ncs.breeze.car.breeze.screen.search.searchoption.SearchOptionScreen
import com.ncs.breeze.car.breeze.utils.LimitClick
import com.ncs.breeze.car.breeze.utils.setClickSafe
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.Event
import com.ncs.breeze.common.analytics.Screen
import com.ncs.breeze.common.event.ToAppRx.postEvent
import com.ncs.breeze.common.extensions.car.createCarIcon
import com.ncs.breeze.common.model.rx.AppToPhoneCarParkEvent

class HomeActionStrip(
    private val mainCarContext: MainBreezeCarContext
) {
    private val carContext = mainCarContext.carContext
    private var obuConnected = false
    private var cardBalance: Double? = null

    /**
     * Build the action strip
     */
    fun builder(
        isOBUConnected: Boolean,
        cardBalance: Double?
    ): ActionStrip.Builder {
        obuConnected = isOBUConnected
        this.cardBalance = cardBalance
        return ActionStrip.Builder()
            .addAction(buildSearchAction())
            .addAction(buildCarPackAction())
            .addAction(buildOBUAction())
//            .addAction(buildCruiseAction())
    }


    /**
     * build search
     */
    @SuppressLint("UnsafeOptInUsageError")
    private fun buildSearchAction() = Action.Builder()
        .setFlags(FLAG_IS_PERSISTENT)
        .setIcon(carContext.createCarIcon(R.drawable.ic_search_black36dp))
        .setClickSafe {
            val screenManager = carContext.getCarService(ScreenManager::class.java)
            (screenManager.top as? HomeCarScreen)?.run {
                screenManager.push(SearchOptionScreen(mainCarContext))
            }
            Analytics.logClickEvent(":search", "[androidauto_homepage]")
        }
        .build()

    @SuppressLint("UnsafeOptInUsageError")
    private fun buildCarPackAction(): Action {
        return Action.Builder()
            .setFlags(FLAG_IS_PERSISTENT)
            .setIcon(carContext.createCarIcon(R.drawable.ic_icon_car_pack))
            .setClickSafe {
                LimitClick.handleSafe {
                    Analytics.logClickEvent(":carparks", "[androidauto_homepage]")
                    val screenManager = carContext.getCarService(ScreenManager::class.java)
                    if (screenManager.top is HomeCarScreen) {
                        screenManager.push(
                            CarParkScreen(
                                CarParkCarContext(mainCarContext),
                                originalDestination = null,
                                fromScreen = Screen.ANDROID_AUTO_HOMEPAGE
                            )
                        )
                        postEvent(AppToPhoneCarParkEvent())
                    }
                }
            }
            .build()
    }

    @SuppressLint("UnsafeOptInUsageError")
    private fun buildOBUAction() = Action.Builder()
        .setFlags(FLAG_IS_PERSISTENT)
        /*
            Besides Action.APP_ICON and Action.BACK, NavigationTemplate requires at least 1 and up to 4 Actions in its ActionStrip.
            Of the 4 allowed Actions, only one can contain a title as set via Action.Builder.setTitle.
            Otherwise, only Actions with icons are allowed.
        */
//        .setIcon(
//            CarIcon.Builder(
//                IconCompat.createWithResource(
//                    carContext,
//                    R.drawable.ic_android_auto_action_strip_obu
//                )
//            ).build()
//        )
        .setTitle(
            if (obuConnected)
                "$${cardBalance?.round(2) ?: "-"}"
            else "OBU"
        )
        .setClickSafe {
            LimitClick.handleSafe {
                carContext.getCarService(ScreenManager::class.java).push(
                    OBUDeviceConnectionStatusScreen(
                        mainCarContext,
                        fromScreen = Screen.ANDROID_AUTO_HOMEPAGE
                    )
                )
                if (!obuConnected)
                    Analytics.logClickEvent(Event.AA_OBU_X, Screen.ANDROID_AUTO_HOMEPAGE)
            }
        }
        .build()
}

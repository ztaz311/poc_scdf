package com.ncs.breeze.ui.login.fragment.signUp.module

import com.ncs.breeze.ui.login.fragment.signUp.view.LoginPhoneNumberFragmentBreeze
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class LoginPhoneNumberFragmentBreezeModule {
    @ContributesAndroidInjector
    abstract fun contributeLoginPhoneNumberFragmentBreeze(): LoginPhoneNumberFragmentBreeze
}
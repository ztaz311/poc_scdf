package com.ncs.breeze.common.extensions.android

import android.widget.TextView
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat

fun TextView.setTextColorResource(@ColorRes colorRes: Int) =
    setTextColor(ContextCompat.getColor(context, colorRes))
package com.ncs.breeze.car.breeze.screen.obulite

import androidx.car.app.CarContext
import androidx.car.app.ScreenManager
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import com.mapbox.navigation.base.ExperimentalPreviewMapboxNavigationAPI
import com.mapbox.navigation.core.lifecycle.MapboxNavigationApp
import com.mapbox.navigation.core.trip.session.TripSessionState
import com.ncs.breeze.common.analytics.Screen
import com.ncs.breeze.common.event.ToAppRx
import com.ncs.breeze.common.model.rx.StopOBULiteDisplay
import com.ncs.breeze.common.utils.BreezeEHorizonProvider
import com.ncs.breeze.common.utils.LocationBreezeManager
import com.ncs.breeze.common.utils.SpeedLimitUtil
import com.ncs.breeze.common.utils.TripLoggingManager
import com.ncs.breeze.common.utils.eta.ETAEngine
import com.breeze.model.enums.ETAMode
import com.ncs.breeze.common.utils.OBUStripStateManager
import java.lang.ref.WeakReference

object CarOBULiteManager {
    var movedToAnotherScreen: Boolean = false
    var carContextRef: WeakReference<CarContext>? = null
    val carScreenState = MutableLiveData(Lifecycle.State.INITIALIZED)
    val carLifecycleObserver = object : DefaultLifecycleObserver {
        override fun onStart(owner: LifecycleOwner) {
            super.onStart(owner)
            carScreenState.postValue(owner.lifecycle.currentState)
        }

        override fun onStop(owner: LifecycleOwner) {
            super.onStop(owner)
            movedToAnotherScreen = carContextRef?.get()?.let {
                it.getCarService(ScreenManager::class.java).top !is CarOBULiteScreen
            } ?: false
            carScreenState.postValue(owner.lifecycle.currentState)
        }
    }

    @OptIn(ExperimentalPreviewMapboxNavigationAPI::class)
    fun closePhoneOBUScreen() {
        carContextRef = null
        movedToAnotherScreen = false
//        ToAppRx.postEvent(StopOBULiteDisplay())
        TripLoggingManager.getInstance()?.stopTripLogging(null, Screen.OBU_LITE_SCREEN)
        BreezeEHorizonProvider.destroy()
        ETAEngine.getInstance()!!.stopETA(false)
        LocationBreezeManager.getInstance().removeLocationCallBack(
            ETAEngine.getInstance()!!.getEtaLocationUpdateInstance(ETAMode.Cruise)
        )
        SpeedLimitUtil.gc()
        if (MapboxNavigationApp.current()
                ?.getTripSessionState() == TripSessionState.STARTED
        ) {
            MapboxNavigationApp.current()?.stopTripSession()
            MapboxNavigationApp.current()?.resetTripSession {}
        }
    }
}
package com.ncs.breeze.ui.dashboard.fragments.exploremap

import android.app.Activity
import android.location.Location
import com.google.android.gms.maps.model.LatLng
import com.mapbox.android.gestures.MoveGestureDetector
import com.mapbox.android.gestures.StandardScaleGestureDetector
import com.mapbox.maps.MapView
import com.mapbox.maps.MapboxMap
import com.mapbox.maps.extension.observable.eventdata.CameraChangedEventData
import com.mapbox.maps.plugin.delegates.listeners.OnCameraChangeListener
import com.mapbox.maps.plugin.gestures.*
import com.ncs.breeze.components.marker.markerview2.UserLocationToastMarker

class ExploreUserLocationToolTip(private val mapboxMap: MapboxMap, private val mapView: MapView) {

    // you are here tool tip
    var userLocationToolTip: UserLocationToastMarker? = null
    private var showUserLocationToolTip = true

    // to reset tooltip on camera change events
    private val resetYouAreHereTTOnScaleListener = object : OnScaleListener {
        override fun onScale(detector: StandardScaleGestureDetector) {
            // no op
        }

        override fun onScaleBegin(detector: StandardScaleGestureDetector) {
            removeUserLocationToolTip()
        }

        override fun onScaleEnd(detector: StandardScaleGestureDetector) {
            // no op
        }
    }

    private val resetYouAreHereTTOnMoveListener = object : OnMoveListener {
        override fun onMove(detector: MoveGestureDetector): Boolean {
            return false
        }

        override fun onMoveBegin(detector: MoveGestureDetector) {
            removeUserLocationToolTip()
        }

        override fun onMoveEnd(detector: MoveGestureDetector) {
            // no op
        }
    }

    private val resetYouAreHereTTCameraChangeListener = object : OnCameraChangeListener {
        override fun onCameraChanged(eventData: CameraChangedEventData) {
            userLocationToolTip?.let {
                it.updateTooltipPosition()
            }
        }
    }

    fun removeUserLocationToolTip() {
        mapboxMap.removeOnScaleListener(resetYouAreHereTTOnScaleListener)
        mapboxMap.removeOnMoveListener(resetYouAreHereTTOnMoveListener)
        mapboxMap.removeOnCameraChangeListener(resetYouAreHereTTCameraChangeListener)
        userLocationToolTip?.let {
            mapView.removeView(it.mViewMarker)
            userLocationToolTip = null
        }
    }

    fun validateAndAddToolTip(currentLocation: Location, activity: Activity) {
        if (!showUserLocationToolTip) return
        showUserLocationToolTip = false
        userLocationToolTip = createUserLocationToolTip(currentLocation, activity)
        mapboxMap.addOnScaleListener(resetYouAreHereTTOnScaleListener)
        mapboxMap.addOnMoveListener(resetYouAreHereTTOnMoveListener)
        mapboxMap.addOnCameraChangeListener(resetYouAreHereTTCameraChangeListener)
    }


    fun resetToolTipState() {
        showUserLocationToolTip = true
    }

    private fun createUserLocationToolTip(
        currentLocation: Location,
        activity: Activity
    ): UserLocationToastMarker {
        val userLocationMarker = UserLocationToastMarker(
            LatLng(currentLocation.latitude, currentLocation.longitude), mapboxMap,
            activity
        )
        mapView.addView(userLocationMarker.mViewMarker)
        userLocationMarker.handleClickMarker()
        userLocationMarker.updateTooltipPosition()
        return userLocationMarker
    }
}
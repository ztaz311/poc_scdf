package com.ncs.breeze.ui.rn

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.KeyEvent
import com.facebook.react.ReactInstanceManager
import com.facebook.react.ReactNativeHost
import com.facebook.react.devsupport.DoubleTapReloadRecognizer
import com.facebook.react.modules.core.DefaultHardwareBackBtnHandler


open class CustomReactDelegate(
    private var mActivity: Activity?,
    private val reactNativeHost: ReactNativeHost?,
    private var mMainComponentName: String?,
    private var mLaunchOptions: Bundle?
) {
    private var mReactRootView: CustomReactRootView? = null
    private val mDoubleTapReloadRecognizer: DoubleTapReloadRecognizer = DoubleTapReloadRecognizer()
    private val backBtnHandler = DefaultHardwareBackBtnHandler {
        //leon: dummy back button handler
    }

    fun onHostResume(activity: Activity? = null) {
        activity?.let { mActivity = it }
        if (reactNativeHost?.hasInstance() == true) {
            reactNativeHost.reactInstanceManager.onHostResume(
                mActivity,
                backBtnHandler
            )
        }
    }

    fun onHostPause() {
        runCatching {
            reactNativeHost?.reactInstanceManager?.onHostPause(mActivity)
        }
    }

    fun onHostDestroy(shouldClean: Boolean = true) {
        mReactRootView?.unmountReactApplication()
        mReactRootView = null
        if (shouldClean)
            reactNativeHost?.reactInstanceManager?.onHostDestroy(mActivity)
    }

    fun onBackPressed(): Boolean {
        reactNativeHost?.reactInstanceManager?.run {
            onBackPressed()
            return true
        }
        return false
    }

    fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?,
        shouldForwardToReactInstance: Boolean
    ) {
        if (reactNativeHost?.hasInstance() == true && shouldForwardToReactInstance) {
            reactNativeHost.reactInstanceManager.onActivityResult(
                mActivity,
                requestCode,
                resultCode,
                data
            )
        }
    }

    @JvmOverloads
    fun loadApp(appKey: String? = mMainComponentName) {
        check(mReactRootView == null) { "Cannot loadApp while app is already running." }
        mReactRootView = createRootView().apply {
            startReactApplication(
                reactNativeHost?.reactInstanceManager, appKey,
                mLaunchOptions
            )
        }
    }

    val reactRootView: CustomReactRootView?
        get() = mReactRootView

    private fun createRootView(): CustomReactRootView {
        return CustomReactRootView(mActivity, null)
    }

    fun shouldShowDevMenuOrReload(keyCode: Int, event: KeyEvent?): Boolean {
        if (reactNativeHost?.hasInstance() == true && reactNativeHost.useDeveloperSupport) {
            if (keyCode == 82) {
                reactNativeHost.reactInstanceManager.showDevOptionsDialog()
                return true
            }
            val didDoubleTapR =
                (mDoubleTapReloadRecognizer as? DoubleTapReloadRecognizer)?.didDoubleTapR(
                    keyCode,
                    mActivity?.currentFocus
                )
            if (didDoubleTapR == true) {
                reactNativeHost.reactInstanceManager.devSupportManager.handleReloadJS()
                return true
            }
        }
        return false
    }

    val reactInstanceManager: ReactInstanceManager?
        get() = reactNativeHost?.reactInstanceManager

}

package com.ncs.breeze.components.map.profile

import android.app.Activity
import com.mapbox.maps.MapView
import com.ncs.breeze.components.layermanager.MarkerLayerManager
import com.ncs.breeze.components.layermanager.impl.cluster.ClusteredMarkerLayerManager
import com.ncs.breeze.helper.carpark.CarParkDestinationIconManager

class MapProfileOptions private constructor(builder: Builder) {
    val mapView: MapView = builder.mapView
    val activity: Activity = builder.activity
    val screenName: String = builder.screenName
    val carParkDestinationIconManager: CarParkDestinationIconManager =
        builder.carParkDestinationIconManager
    val markerLayerClickHandler: MarkerLayerManager.MarkerLayerClickHandler? =
        builder.markerLayerClickHandler
    val clusterLayerClickHandler: ClusteredMarkerLayerManager.ClusterLayerClickHandler? =
        builder.clusterLayerClickHandler
    val nightModeEnabled: Boolean = builder.nightModeEnabled
    val mapCamera: BaseMapCamera? = builder.mapCamera
    val toolTipRadius: Double? = builder.toolTipRadius

    class Builder(
        val mapView: MapView,
        val activity: Activity,
        val carParkDestinationIconManager: CarParkDestinationIconManager,
        val screenName: String,
    ) {
        var markerLayerClickHandler: MarkerLayerManager.MarkerLayerClickHandler? = null
            private set

        var clusterLayerClickHandler: ClusteredMarkerLayerManager.ClusterLayerClickHandler? = null
            private set

        var nightModeEnabled: Boolean = false
            private set

        var mapCamera: BaseMapCamera? = null
            private set

        var toolTipRadius: Double? = null
            private set

        fun setMarkerLayerClickHandler(markerLayerClickHandler: MarkerLayerManager.MarkerLayerClickHandler): Builder =
            apply { this@Builder.markerLayerClickHandler = markerLayerClickHandler }

        fun setClusterLayerClickHandler(clusterLayerClickHandler: ClusteredMarkerLayerManager.ClusterLayerClickHandler): Builder =
            apply { this@Builder.clusterLayerClickHandler = clusterLayerClickHandler }

        fun setNightModeEnabled(nightModeEnabled: Boolean): Builder =
            apply { this@Builder.nightModeEnabled = nightModeEnabled }

        fun setBaseMapCamera(mapCamera: BaseMapCamera): Builder =
            apply { this@Builder.mapCamera = mapCamera }

        fun setToolTipRadius(toolTipRadius: Double): Builder =
            apply { this@Builder.toolTipRadius = toolTipRadius }


        fun build() = MapProfileOptions(this)
    }
}
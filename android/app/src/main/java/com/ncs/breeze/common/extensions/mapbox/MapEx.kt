package com.ncs.breeze.common.extensions.mapbox

import com.mapbox.api.directions.v5.models.DirectionsResponse
import com.mapbox.maps.MapView
import com.mapbox.maps.plugin.compass.compass
import com.mapbox.maps.plugin.gestures.gestures
import com.mapbox.maps.plugin.scalebar.scalebar
import com.mapbox.navigation.base.ExperimentalPreviewMapboxNavigationAPI
import com.mapbox.navigation.core.MapboxNavigation
import com.mapbox.navigation.core.directions.session.RoutesObserver
import com.mapbox.navigation.core.replay.route.ReplayProgressObserver
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.io.ObjectOutputStream
import java.util.zip.GZIPOutputStream


fun MapView.setBreezeDefaultOptions() {
    this.scalebar?.enabled = false
    this.compass?.enabled = false
    this.gestures?.pitchEnabled = false
}

@OptIn(ExperimentalPreviewMapboxNavigationAPI::class)
fun MapboxNavigation.withDebugSimulatorEnabled(
    replayEnabled: Boolean,
    replayObserver: RoutesObserver,
    replayProgressObserver: ReplayProgressObserver
) = apply {
    if (replayEnabled) {
        registerRouteProgressObserver(replayProgressObserver)
        registerRoutesObserver(replayObserver)
    }
}


@OptIn(ExperimentalPreviewMapboxNavigationAPI::class)
fun MapboxNavigation.disableDebugSimulator(
    replayEnabled: Boolean,
    replayObserver: RoutesObserver,
    replayProgressObserver: ReplayProgressObserver
) = apply {
    if (replayEnabled) {
        unregisterRouteProgressObserver(replayProgressObserver)
        unregisterRoutesObserver(replayObserver)
    }
}


@Throws(IOException::class)
suspend fun DirectionsResponse.compressToGzip(): ByteArray {
    val inputByteArrayStream = ByteArrayOutputStream()
    val outPutByteArrayStream = ByteArrayOutputStream()
    val gzip = GZIPOutputStream(outPutByteArrayStream)
    val time = System.currentTimeMillis()
    try {
        var out: ObjectOutputStream? = null;
        out = ObjectOutputStream(inputByteArrayStream);
        out.writeObject(this);
        out.flush();
        val byteArray = inputByteArrayStream.toByteArray();
        gzip.write(byteArray)
        gzip.close();
        val compressed: ByteArray = outPutByteArrayStream.toByteArray()
        return compressed
    } finally {
        inputByteArrayStream.close()
        outPutByteArrayStream.close()
        gzip.close()
    }
}
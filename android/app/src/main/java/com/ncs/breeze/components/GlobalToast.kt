package com.ncs.breeze.components

import android.app.Activity
import android.view.Gravity
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.ncs.breeze.R
import timber.log.Timber

class GlobalToast {

    companion object {
        const val TAG = "GlobalToast"
        const val UNSET_RES_VALUE = -1
        const val DIALOG_AUTO_DISMISS_DURATION_SECONDS: Int = 10
    }

    private var icon: Int = UNSET_RES_VALUE
    private lateinit var description: String
    private var dismissDuration: Int = DIALOG_AUTO_DISMISS_DURATION_SECONDS

    fun setIcon(icon: Int): GlobalToast {
        this.icon = icon
        return this
    }

    fun setDescription(description: String): GlobalToast {
        this.description = description
        return this
    }

    fun setDismissDurationSeconds(dismissDuration: Int): GlobalToast {
        this.dismissDuration = dismissDuration
        return this
    }

    fun show(activity: Activity?) {
        if (activity == null) {
            Timber.e("Requested activity is NULL!!!")
            return
        }
        val view = activity.findViewById<View>(R.id.mainLayout)
        if (view == null) {
            Timber.e("Main layout not defined in requested activity!!!")
            return
        }
        val snackbar = Snackbar.make(view, "", dismissDuration * 1000)
        snackbar.view.setBackgroundColor(android.graphics.Color.TRANSPARENT)
        val snackbarLayout = snackbar.view as Snackbar.SnackbarLayout
        snackbarLayout.setPadding(0, 0, 0, 0)
        val rootView = initViews(activity)
        snackbarLayout.addView(rootView, 0)

        snackbar.addCallback(object : BaseTransientBottomBar.BaseCallback<Snackbar>() {
            override fun onDismissed(transientBottomBar: Snackbar?, event: Int) {
                super.onDismissed(transientBottomBar, event)
                Timber.d("onDismissed snackbar")
            }

            override fun onShown(transientBottomBar: Snackbar?) {
                super.onShown(transientBottomBar)
                Timber.d("onShown snackbar")
            }
        })

        val params = snackbar.view.layoutParams as FrameLayout.LayoutParams
        params.gravity = Gravity.TOP
        snackbar.view.layoutParams = params
        snackbar.show()
    }

    private fun initViews(activity: Activity): View {
        val rootView = activity.layoutInflater.inflate(R.layout.layout_global_toast, null, false);
        val imgIcon = rootView?.findViewById<ImageView>(R.id.imgIconToast)
        val txtMessage = rootView?.findViewById<TextView>(R.id.txtMessageToast)

        txtMessage?.text = description
        if (icon != GlobalNotification.UNSET_RES_VALUE) {
            imgIcon?.setImageResource(icon)
        } else {
            imgIcon?.setImageResource(R.drawable.ic_breeze_round)
        }

        return rootView
    }
}
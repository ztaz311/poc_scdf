package com.ncs.breeze.components.marker.markerview2.carpark

import android.app.Activity
import android.content.res.Resources
import android.graphics.Color
import android.view.View
import android.view.ViewGroup.MarginLayoutParams
import android.widget.TextView
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.core.view.marginBottom
import com.facebook.react.bridge.Arguments
import com.mapbox.geojson.Point
import com.mapbox.maps.EdgeInsets
import com.mapbox.maps.MapboxMap
import com.ncs.breeze.App
import com.ncs.breeze.R
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.Event
import com.ncs.breeze.common.analytics.Param
import com.breeze.model.extensions.visibleAmount
import com.breeze.model.api.response.amenities.BaseAmenity
import com.ncs.breeze.common.utils.BreezeMapZoomUtil
import com.breeze.model.constants.Constants
import com.breeze.model.enums.AmenityBand
import com.breeze.model.extensions.dpToPx
import com.ncs.breeze.databinding.SimpleCarparkMarkerTooltipBinding
import com.ncs.breeze.reactnative.nativemodule.ShareBusinessLogicEvent
import com.ncs.breeze.ui.base.BaseActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class SimpleCarParkToolTipManager(
    val viewBinding: SimpleCarparkMarkerTooltipBinding,
    val mapboxMap: MapboxMap,
    val activity: Activity,
    private val voucherEnabled: Boolean
) : ToolTipManager {

    private val pixelDensity = Resources.getSystem().displayMetrics.density
    var currentDataMarker: BaseAmenity? = null
    private var fromLandingPage: Boolean = false

    override fun clickToolTip(
        item: BaseAmenity,
        shouldRecenter: Boolean,
        navigationButtonClick: (BaseAmenity, BaseActivity.EventSource) -> Unit,
        moreInfoButtonClick: (BaseAmenity) -> Unit,
        carParkIconClick: (Boolean) -> Unit,
        screenName: String
    ) {
        fromLandingPage = true

        val carParkInfo = bundleOf(
            Param.KEY_LOCATION_NAME to "location_${item.name}",
            Param.KEY_LATITUDE to "${item.lat}",
            Param.KEY_LONGITUDE to "${item.long}",
        )
        if (item.hasVouchers) {
            Analytics.logClickEvent(Event.TAP_VOUCHER_CARPARK_CLICK, screenName, carParkInfo)
        } else {
            Analytics.logClickEvent(Event.TAP_CARPARK_CLICK, screenName, carParkInfo)
        }

//        /**
//         * btn calculate fee
//         */
//        btn_calculate_fee?.visibility =
//            if (BreezeCarUtil.currentModeSelected == Constants.MODE_DASH_BOARD_SELLECTED.PARKING_MODE
//                || item.parkingType.equals(Constants.PARKING_TYPE_SEASON_PARKING)
//                || item.parkingType.equals(Constants.PARKING_TYPE_CUSTOMER_PARKING)) {
//                View.GONE
//            } else {
//                btn_calculate_fee?.setOnClickListener{
//                    currentDataMarker?.id?.let {
//                        Analytics.logClickEvent(Event.PARKING_CALCULATE_FEE, Screen.HOME)
//                        sendEventOpenCalculateFee(it)
//                    }
//                }
//                View.VISIBLE
//            }

        manageViews(item, shouldRecenter, carParkIconClick, navigationButtonClick)
    }

    override fun clickRouteToolTip(
        item: BaseAmenity,
        shouldRecenter: Boolean,
        navigationButtonClick: (BaseAmenity, BaseActivity.EventSource) -> Unit,
        moreInfoButtonClick: (BaseAmenity) -> Unit,
        carParkIconClick: (Boolean) -> Unit,
        calculateFee: (BaseAmenity) -> Unit,
        screenName: String
    ) {
        fromLandingPage = false

        /**
         * check if in carpark mode => invisible btn calculate fee
         */
////        btn_calculate_fee?.visibility = View.VISIBLE
//        btn_calculate_fee?.visibility =
//            if (item.parkingType.equals(Constants.PARKING_TYPE_SEASON_PARKING)
//                || item.parkingType.equals(Constants.PARKING_TYPE_CUSTOMER_PARKING)) {
//                View.GONE
//            } else {
//                btn_calculate_fee?.setOnClickListener{
//                    calculateFee.invoke(item)
//                }
//                View.VISIBLE
//            }

        manageViews(item, shouldRecenter, carParkIconClick, navigationButtonClick)
    }

    private fun manageViews(
        item: BaseAmenity,
        shouldRecenter: Boolean,
        carParkIconClick: (Boolean) -> Unit,
        navigationButtonClick: (BaseAmenity, BaseActivity.EventSource) -> Unit
    ) {
        currentDataMarker = item

        /**
         * for set name carpark
         */
        viewBinding.tvNameCarpark.text = item.name

//        /**
//         * for available lot
//         */
//        if(item.parkingType.equals(Constants.PARKING_TYPE_SEASON_PARKING)) {
//            llAvailableLots?.visibility = View.GONE
//        } else {
//            if (item.availablelot.toString() == "-1") {
//                lot_available?.text = activity.getString(R.string.nearby_carpark_no_info)
//            } else {
//                lot_available?.text = item.availablelot.toString()
//            }
//        }

        /**
         * current and Next HR Rate
         */
        if (item.parkingType.equals(Constants.PARKING_TYPE_SEASON_PARKING) || item.parkingType.equals(
                Constants.PARKING_TYPE_CUSTOMER_PARKING
            )
        ) {
//            llCurrentHourPrice?.visibility = View.GONE
//            llNextHourPrice?.visibility = View.GONE
            viewBinding.tvSpecialsTitle.visibility = View.GONE
        } else {
            /**
             * current HR Rate
             */
            if (item.currentHrRate?.oneHrRate != null) {
                if (item.currentHrRate?.oneHrRate == -2.0) {
                    viewBinding.tvNow.text = activity.getString(R.string.carpark_season)
                    viewBinding.tvNow.let { setTextColor(it, R.color.themed_passion_pink) }
                } else if (item.currentHrRate?.oneHrRate == 0.0) {
                    viewBinding.tvNow.text = activity.getString(R.string.carpark_free)
                    viewBinding.tvNow.let { setTextColor(it, R.color.themed_parakeet_green) }
                } else {
                    val rate = item.currentHrRate!!.oneHrRate!!.visibleAmount()
                    viewBinding.tvNow.text = rate
                    viewBinding.tvNow.let { setTextColor(it, R.color.tv_color_tooltip_title) }
                }
            } else {
                viewBinding.tvNow.text = activity.getString(R.string.nearby_carpark_no_info)
                viewBinding.tvNow.let { setTextColor(it, R.color.tv_color_tooltip_title) }
            }

            /**
             * isCheapest
             */
            if (item.isCheapest == true) {
                if (item.currentHrRate?.oneHrRate == -2.0) {//if season
                    viewBinding.tvSpecialsTitle.visibility = View.GONE
                } else {
                    viewBinding.tvSpecialsTitle.visibility = View.VISIBLE
                }
            } else {
                viewBinding.tvSpecialsTitle.visibility = View.GONE
            }
        }

        /**
         * navigation button click
         */
        viewBinding.confirmButton.setOnClickListener {
            resetView()
            navigationButtonClick.invoke(item, BaseActivity.EventSource.NONE);
        }

        /*
        * seasonal carpark message
        * */
//        if(item.parkingType.equals(Constants.PARKING_TYPE_SEASON_PARKING)) {
//            tvParkingTypeMessage?.visibility = View.VISIBLE
//            tvParkingTypeMessage?.text = activity.getString(R.string.carpark_season_parking)
//        } else if (item.parkingType.equals(Constants.PARKING_TYPE_CUSTOMER_PARKING)) {
//            tvParkingTypeMessage?.visibility = View.VISIBLE
//            tvParkingTypeMessage?.text = activity.getString(R.string.carpark_customers_only)
//        } else {
//            tvParkingTypeMessage?.visibility = View.GONE
//        }

        item.availablePercentImageBand?.let {
            handleShowCarParkStatus(item)
        }

        carParkOnClick(item, shouldRecenter, carParkIconClick)
    }

    private fun handleShowCarParkStatus(amenity: BaseAmenity) {

        if (amenity.hasAvailabilityCS) {
            viewBinding.layoutCarparkStatus.isVisible = true
            viewBinding.view.isVisible = true

            amenity.apply {
                val title: String = availabilityCSData?.title ?: ""
                var colorBoxCarParkStatus: Int = R.color.bg_carpark_status_few_lot
                var iconCarParkStatus: Int = R.drawable.ic_cp_stt_few
                when (availablePercentImageBand) {
                    AmenityBand.TYPE0.type -> {
                        iconCarParkStatus = R.drawable.ic_cp_stt_full
                    }

                    AmenityBand.TYPE1.type -> {
                        iconCarParkStatus = R.drawable.ic_cp_stt_few
                    }

                    AmenityBand.TYPE2.type -> {
                        iconCarParkStatus = R.drawable.ic_cp_stt_available
                    }
                }
                /**
                 * calculate time
                 */
                val timeUpdate =
                    "${availabilityCSData?.primaryDesc ?: activity.getString(com.breeze.customization.R.string.update_carpark_stt_time)} ${availabilityCSData?.generateDisplayedLastUpdate()}"
                val drawableHeaderCpStatus =
                    AppCompatResources.getDrawable(
                        activity,
                        R.drawable.bg_header_carpark_status
                    )
                drawableHeaderCpStatus?.setTint(
                    Color.parseColor(availabilityCSData?.themeColor ?: "#F26415")
                )
                viewBinding.layoutCarparkStatus.background = drawableHeaderCpStatus
                viewBinding.icCarparkStatus.setImageResource(iconCarParkStatus)
                viewBinding.tvStatusCarpark.text = title
                viewBinding.tvTimeUpdateCarparkStatus.text = timeUpdate
            }

        } else {
            viewBinding.layoutCarparkStatus.isVisible = false
            viewBinding.view.isVisible = false
        }
    }

    private fun carParkOnClick(
        item: BaseAmenity,
        shouldRecenter: Boolean,
        carParkIconClick: (Boolean) -> Unit
    ) {
        carParkIconClick.invoke(false)

        if (!shouldRecenter) {
            bringtoFont()
            return
        }

        BreezeMapZoomUtil.zoomViewToRange(mapboxMap, Point.fromLngLat(item.long!!, item.lat!!),
            Constants.DEFAULT_TOOLTIP_ZOOM_RADIUS, EdgeInsets(
                200.0 * pixelDensity,
                10.0 * pixelDensity,
                10.0 * pixelDensity,
                10.0 * pixelDensity
            ), animationEndCB = {
                if (!viewBinding.llToolTip.isShown) {
                    viewBinding.llToolTip.visibility = View.VISIBLE
                }
                bringtoFont()
            }
        )
        bringtoFont()
    }

    private fun setTextColor(tv: TextView, color: Int) {
        tv?.setTextColor(ContextCompat.getColor(activity, color))
    }

    override fun resetView() {
        viewBinding.llToolTip.visibility = View.INVISIBLE
    }

    override fun bringtoFont() {
        viewBinding.root.bringToFront()
    }

    override fun sendEventParkingISelected(idCarpark: String) {
        App.instance?.let { app ->
            val shareBusinessLogicPackage = app.shareBusinessLogicPackage
            shareBusinessLogicPackage.getRequestsModule().flatMap {
                val rnData = Arguments.createMap().apply {
                    putString("parkingID", idCarpark)
                }
                it.callRN(ShareBusinessLogicEvent.PARKING_OPEN_INFOR_CARPARK.eventName, rnData)
            }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe()
        }
    }

    override fun sendEventOpenCalculateFee(idCarpark: String) {
        App.instance?.let { app ->
            val shareBusinessLogicPackage = app.shareBusinessLogicPackage
            shareBusinessLogicPackage.getRequestsModule().flatMap {
                val rnData = Arguments.createMap().apply {
                    putString("parkingID", idCarpark)
                }
                it.callRN(ShareBusinessLogicEvent.OPEN_CALCULATE_FEE_CARPARK.eventName, rnData)
            }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe()
        }
    }
}
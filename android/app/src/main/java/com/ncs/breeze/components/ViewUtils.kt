package com.ncs.breeze.components

import android.content.Context
import android.content.res.Configuration
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.util.Base64
import android.view.View
import androidx.coordinatorlayout.widget.CoordinatorLayout
import java.io.ByteArrayOutputStream


object ViewUtils {
    fun captureView(view: View): Bitmap {
        val rootView = view.rootView
        rootView.isDrawingCacheEnabled = true
        val bitmap = Bitmap.createBitmap(rootView.drawingCache)
        rootView.isDrawingCacheEnabled = false
        return bitmap
    }

    @JvmOverloads
    fun encodeView(
        capture: Bitmap,
        options: BitmapEncodeOptions = BitmapEncodeOptions.Builder().build()
    ): String {
        // Resize up to original width while keeping the aspect ratio
        val width = Math.min(capture.width, options.width)
        val height = Math.round(width.toFloat() * capture.height / capture.width)
        val scaled = Bitmap.createScaledBitmap(capture, width, height,  /*filter=*/true)

        // Convert to JPEG at a quality between 20% ~ 100%
        val stream = ByteArrayOutputStream()
        scaled.compress(Bitmap.CompressFormat.JPEG, options.compressQuality, stream)

        // Convert to base64 encoded string
        val data = stream.toByteArray()
        return Base64.encodeToString(data, Base64.DEFAULT)
    }

    fun decodeScreenshot(screenshotBase64Format: String?): Bitmap? {
        return try {
            val bytes = Base64.decode(screenshotBase64Format, Base64.DEFAULT)
            BitmapFactory.decodeByteArray(bytes, 0, bytes.size)
        } catch (exception: Exception) {
            null
        }
    }

    fun loadBitmapFromView(view: View): Bitmap? {
        if (view.measuredHeight <= 0) {
            view.measure(
                CoordinatorLayout.LayoutParams.WRAP_CONTENT,
                CoordinatorLayout.LayoutParams.WRAP_CONTENT
            )
            val bitmap = Bitmap.createBitmap(
                view.measuredWidth,
                view.measuredHeight,
                Bitmap.Config.ARGB_8888
            )
            val canvas = Canvas(bitmap)
            view.layout(view.left, view.top, view.right, view.bottom)
            view.draw(canvas)
            return bitmap
        }
        return null
    }

    fun isLandscape(context: Context): Boolean {
        return context.resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE
    }
}
package com.ncs.breeze.common.utils.mapStyle

import android.content.res.Resources
import com.ncs.breeze.R
import com.mapbox.maps.extension.style.expressions.generated.Expression
import com.mapbox.navigation.ui.maps.route.line.model.RouteLineColorResources
import com.mapbox.navigation.ui.maps.route.line.model.RouteLineResources
import com.mapbox.navigation.ui.maps.route.line.model.RouteLineScaleValue

class BreezeNavigationMapStyle {

    fun getRouteLineResources(
        resources: Resources,
        destinationIcon: Int? = null
    ): RouteLineResources {
        val builder = RouteLineResources.Builder()
            .routeLineColorResources(navigationRouteLineColorResources(resources))
            .routeLineScaleExpression(routeLineScaleExpression)
            .routeCasingLineScaleExpression(routeCasingLineScaleExpression)
            .routeTrafficLineScaleExpression(routeTrafficLineScaleExpression)
            .alternativeRouteLineScaleExpression(alternativeRouteLineScaleExpression)
            .alternativeRouteCasingLineScaleExpression(alternativeRouteCasingLineScaleExpression)
            .alternativeRouteTrafficLineScaleExpression(alternativeRouteTrafficLineScaleExpression)
        if (destinationIcon != null) {
            builder.destinationWaypointIcon(destinationIcon)
        }
        return builder.build()
    }

    fun getWalkingRouteLineResources(
        resources: Resources,
        destinationIcon: Int? = null
    ): RouteLineResources {
        val builder = RouteLineResources.Builder()
            .routeLineColorResources(navigationWalkingRouteLineColorResources(resources))
        if (destinationIcon != null) {
            builder.destinationWaypointIcon(destinationIcon)
        }
        return builder.build()
    }

    private fun navigationWalkingRouteLineColorResources(resources: Resources): RouteLineColorResources {
        return RouteLineColorResources.Builder()
            .routeSevereCongestionColor(clr(resources, R.color.transparent))
            .routeHeavyCongestionColor(clr(resources, R.color.transparent))
            .routeModerateCongestionColor(clr(resources, R.color.transparent))
            .routeDefaultColor(clr(resources, R.color.themed_walking_routeline_color))
            .routeLowCongestionColor(clr(resources, R.color.transparent))
            .routeCasingColor(clr(resources, R.color.transparent))
            .routeUnknownCongestionColor(clr(resources, R.color.transparent))
            .alternativeRouteSevereCongestionColor(clr(resources, R.color.transparent))
            .alternativeRouteHeavyCongestionColor(clr(resources, R.color.transparent))
            .alternativeRouteModerateCongestionColor(clr(resources, R.color.transparent))
            .alternativeRouteDefaultColor(clr(resources, R.color.transparent))
            .alternativeRouteLowCongestionColor(clr(resources, R.color.transparent))
            .alternativeRouteCasingColor(clr(resources, R.color.transparent))
            .alternativeRouteUnknownCongestionColor(clr(resources, R.color.transparent))
            .build()
    }

    private val routeThicknessScale = 1.5f

    private var routeLineScaleExpression: Expression =
        buildScalingExpression(
            listOf(
                RouteLineScaleValue(4f, 3f, routeThicknessScale),
                RouteLineScaleValue(10f, 4f, routeThicknessScale),
                RouteLineScaleValue(13f, 6f, routeThicknessScale),
                RouteLineScaleValue(16f, 10f, routeThicknessScale),
                RouteLineScaleValue(19f, 14f, routeThicknessScale),
                RouteLineScaleValue(22f, 18f, routeThicknessScale)
            )
        )
    private var routeCasingLineScaleExpression: Expression =
        buildScalingExpression(
            listOf(
                RouteLineScaleValue(10f, 7f, routeThicknessScale),
                RouteLineScaleValue(14f, 10.5f, routeThicknessScale),
                RouteLineScaleValue(16.5f, 15.5f, routeThicknessScale),
                RouteLineScaleValue(19f, 24f, routeThicknessScale),
                RouteLineScaleValue(22f, 29f, routeThicknessScale)
            )
        )
    private var routeTrafficLineScaleExpression: Expression =
        buildScalingExpression(
            listOf(
                RouteLineScaleValue(4f, 3f, routeThicknessScale),
                RouteLineScaleValue(10f, 4f, routeThicknessScale),
                RouteLineScaleValue(13f, 6f, routeThicknessScale),
                RouteLineScaleValue(16f, 10f, routeThicknessScale),
                RouteLineScaleValue(19f, 14f, routeThicknessScale),
                RouteLineScaleValue(22f, 18f, routeThicknessScale)
            )
        )
    private var alternativeRouteLineScaleExpression: Expression =
        buildScalingExpression(
            listOf(
                RouteLineScaleValue(4f, 3f, routeThicknessScale),
                RouteLineScaleValue(10f, 4f, routeThicknessScale),
                RouteLineScaleValue(13f, 6f, routeThicknessScale),
                RouteLineScaleValue(16f, 10f, routeThicknessScale),
                RouteLineScaleValue(19f, 14f, routeThicknessScale),
                RouteLineScaleValue(22f, 18f, routeThicknessScale)
            )
        )
    private var alternativeRouteCasingLineScaleExpression: Expression =
        buildScalingExpression(
            listOf(
                RouteLineScaleValue(10f, 7f, routeThicknessScale),
                RouteLineScaleValue(14f, 10.5f, routeThicknessScale),
                RouteLineScaleValue(16.5f, 15.5f, routeThicknessScale),
                RouteLineScaleValue(19f, 24f, routeThicknessScale),
                RouteLineScaleValue(22f, 29f, routeThicknessScale)
            )
        )
    private var alternativeRouteTrafficLineScaleExpression: Expression =
        buildScalingExpression(
            listOf(
                RouteLineScaleValue(4f, 3f, routeThicknessScale),
                RouteLineScaleValue(10f, 4f, routeThicknessScale),
                RouteLineScaleValue(13f, 6f, routeThicknessScale),
                RouteLineScaleValue(16f, 10f, routeThicknessScale),
                RouteLineScaleValue(19f, 14f, routeThicknessScale),
                RouteLineScaleValue(22f, 18f, routeThicknessScale)
            )
        )

    private fun navigationRouteLineColorResources(resources: Resources): RouteLineColorResources {
        return RouteLineColorResources.Builder()
            .routeSevereCongestionColor(clr(resources, R.color.themed_traffic_severe_color))
            .routeHeavyCongestionColor(clr(resources, R.color.themed_traffic_heavy_color))
            .routeModerateCongestionColor(clr(resources, R.color.themed_traffic_moderate_color))
            .routeDefaultColor(clr(resources, R.color.themed_traffic_default_color))
            .routeLowCongestionColor(clr(resources, R.color.themed_traffic_low_color))
            .routeCasingColor(clr(resources, R.color.transparent))
            .routeUnknownCongestionColor(clr(resources, R.color.themed_traffic_unknown_color))
            .alternativeRouteSevereCongestionColor(
                clr(
                    resources,
                    R.color.themed_alt_traffic_severe_color
                )
            )
            .alternativeRouteHeavyCongestionColor(
                clr(
                    resources,
                    R.color.themed_alt_traffic_heavy_color
                )
            )
            .alternativeRouteModerateCongestionColor(
                clr(
                    resources,
                    R.color.themed_alt_traffic_moderate_color
                )
            )
            .alternativeRouteDefaultColor(clr(resources, R.color.themed_alt_traffic_default_color))
            .alternativeRouteLowCongestionColor(
                clr(
                    resources,
                    R.color.themed_alt_traffic_low_color
                )
            )
            .alternativeRouteCasingColor(clr(resources, R.color.themed_alt_traffic_low_color))
            .alternativeRouteUnknownCongestionColor(
                clr(
                    resources,
                    R.color.themed_alt_traffic_unknown_color
                )
            )
            .build()
    }

    private fun clr(resources: Resources, colorInt: Int): Int {
        return resources.getColor(
            colorInt,
            null
        )
    }

    private fun buildScalingExpression(scalingValues: List<RouteLineScaleValue>): Expression {
        val expressionBuilder = Expression.ExpressionBuilder("interpolate")
        expressionBuilder.addArgument(Expression.exponential { literal(1.5) })
        expressionBuilder.zoom()
        scalingValues.forEach { routeLineScaleValue ->
            expressionBuilder.stop {
                this.literal(routeLineScaleValue.scaleStop.toDouble())
                product {
                    literal(routeLineScaleValue.scaleMultiplier.toDouble())
                    literal(routeLineScaleValue.scale.toDouble())
                }
            }
        }
        return expressionBuilder.build()
    }
}
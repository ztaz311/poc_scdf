package com.ncs.breeze.ui.login.fragment.guest.viewmodel

import android.app.Application
import androidx.lifecycle.viewModelScope
import com.amplifyframework.auth.AuthUserAttribute
import com.amplifyframework.auth.AuthUserAttributeKey
import com.amplifyframework.auth.cognito.exceptions.service.UsernameExistsException
import com.amplifyframework.auth.cognito.options.AWSCognitoAuthSignInOptions
import com.amplifyframework.auth.cognito.options.AuthFlowType
import com.amplifyframework.auth.exceptions.SessionExpiredException
import com.amplifyframework.auth.options.AuthSignOutOptions
import com.amplifyframework.auth.options.AuthSignUpOptions
import com.amplifyframework.core.Amplify
import com.breeze.model.api.ErrorData
import com.breeze.model.api.response.guest.GuestGenResponse
import com.breeze.model.constants.Constants
import com.breeze.model.constants.GUEST_SIGN_UP_STATE
import com.breeze.model.constants.STATE_FETCH_USER_ATTR
import com.ncs.breeze.common.remote.ApiHelper
import com.ncs.breeze.common.remote.ApiObserver
import com.ncs.breeze.common.remote.MyServiceInterceptor
import com.ncs.breeze.common.storage.BreezeGuestPreference
import com.ncs.breeze.common.storage.BreezeUserPreference
import com.ncs.breeze.common.utils.*
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.ncs.breeze.BuildConfig
import com.ncs.breeze.car.breeze.utils.BreezeCarUtil
import com.ncs.breeze.common.extensions.android.getApp
import com.ncs.breeze.components.SingleLiveEvent
import com.ncs.breeze.ui.base.BaseFragmentViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

class GuestModeSignUpViewModel @Inject constructor(
    val apiHelper: ApiHelper,
    application: Application
) : BaseFragmentViewModel(
    application
) {
    @Inject
    lateinit var myServiceInterceptor: MyServiceInterceptor

    @Inject
    lateinit var breezeUserPreferenceUtil: BreezeUserPreferenceUtil
    var shouldNotShowTAndC: SingleLiveEvent<Boolean> = SingleLiveEvent()
    var signUpSuccess: SingleLiveEvent<Boolean> = SingleLiveEvent()
    var errorThrow: SingleLiveEvent<Boolean> = SingleLiveEvent()
    var stateSignUpGuestAccount = GUEST_SIGN_UP_STATE.GET_ID_GUEST_NOT_SUCCESS
    var mAPIHelper: ApiHelper = apiHelper
    var idGuestUser = ""


    /**
     * get guest ID
     */
    fun genIDGuest() {
        val idGuestTemp = BreezeGuestPreference.getInstance(getApp())?.getGuestId()
        if (!idGuestTemp.isNullOrEmpty()) {
            idGuestUser = idGuestTemp
            stateSignUpGuestAccount = GUEST_SIGN_UP_STATE.GET_ID_GUEST_SUCCESS
            /**
             * start sign in guest user
             */
            startSignUpGuestUser()
        } else {
            CoroutineScope(Dispatchers.IO).launch {
                BreezeCarUtil.apiHelper.guestGenerate()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(object : ApiObserver<GuestGenResponse>(compositeDisposable) {
                        override fun onSuccess(data: GuestGenResponse) {
                            data.apply {
                                /**
                                 * set state get ID success & save id to share
                                 */
                                stateSignUpGuestAccount =
                                    GUEST_SIGN_UP_STATE.GET_ID_GUEST_SUCCESS
                                idGuestUser = id ?: ""

                                startSignUpGuestUser()
                            }
                        }

                        override fun onError(e: ErrorData) {
                            errorThrow.postValue(true)
                        }
                    })
            }
        }
    }

    /**
     * auto sign in guest mode
     */
    fun autoSignInGuestMode() {
        val emailGuestLogin = BreezeGuestPreference.getInstance(getApp())?.getGuestId()
        if (!emailGuestLogin.isNullOrEmpty()) {
            idGuestUser = emailGuestLogin
            breezeUserPreferenceUtil.saveIsGuestMode(true)
            breezeUserPreferenceUtil.saveEmailGuestUser(idGuestUser)

            stateSignUpGuestAccount = GUEST_SIGN_UP_STATE.GET_ID_GUEST_SUCCESS
            /**
             * start sign up
             */
            startSignInGuestUser()
        }
    }

    /**
     * check term of condition user
     */
    fun checkTermOfCondition() {
        checkTermOfCondition(breezeUserPreferenceUtil, apiHelper) { isShouldShowTerm ->
            shouldNotShowTAndC.postValue(isShouldShowTerm)
        }
    }


    /**
     * start sign up guest mode
     */
    private fun startSignUpGuestUser() {
        val pass = Constants.AMPLIFY_CONSTANTS.AMPLIFY_PASSWORD
        val attrs = mapOf(
            AuthUserAttributeKey.custom("custom:guest_login") to "true",
            AuthUserAttributeKey.custom("custom:signup_device_os") to "ANDROID",
            AuthUserAttributeKey.custom("custom:signup_app_version") to BuildConfig.VERSION_CODE.toString())

        val options = AuthSignUpOptions.builder()
            .userAttributes(attrs.map { AuthUserAttribute(it.key, it.value) })
            .build()
        AWSUtils.signUp(idGuestUser,
            pass, options,
            {
                if (it.isSignUpComplete) {
                    /**
                     * save ID guest mode in share that is not clear
                     */
                    BreezeGuestPreference.getInstance(getApp())
                        ?.saveGuestID(idGuestUser)
                    breezeUserPreferenceUtil.saveEmailGuestUser(idGuestUser)
                    breezeUserPreferenceUtil.saveIsGuestMode(true)

                    /**
                     * set new state
                     */
                    stateSignUpGuestAccount = GUEST_SIGN_UP_STATE.SIGNUP_SUCCESS
                    /**
                     * start sign in
                     */
                    startSignInGuestUser()

                } else {
                    errorThrow.postValue(true)
                }
            },
            {
                if (it is UsernameExistsException || it is SessionExpiredException) {
                    startSignInGuestUser()
                } else {
                    idGuestUser = ""
                    stateSignUpGuestAccount = GUEST_SIGN_UP_STATE.GET_ID_GUEST_NOT_SUCCESS

                    errorThrow.postValue(true)
                }
            }
        )
    }

    /**
     * start sign in guest user
     */
    private fun startSignInGuestUser() {
        logoutApp {
            val options = AWSCognitoAuthSignInOptions.builder()
                .authFlowType(AuthFlowType.CUSTOM_AUTH_WITH_SRP)
                .build()
            val pass = Constants.AMPLIFY_CONSTANTS.AMPLIFY_PASSWORD
            AWSUtils.signIn(
                idGuestUser,
                pass,
                options,
                { result ->

                    if (!result.isSignedIn && result.nextStep.signInStep.name == Constants.AMPLIFY_CONSTANTS.SIGN_IN_WITH_CUSTOM_CHALLENGE) {
                        stateSignUpGuestAccount = GUEST_SIGN_UP_STATE.NEED_CONFIRM_SIGNIN
                        startConfirmSignInGuestUser()
                    } else if (result.isSignedIn && result.nextStep.signInStep.name == Constants.AMPLIFY_CONSTANTS.DONE) {
                        stateSignUpGuestAccount = GUEST_SIGN_UP_STATE.SIGNIN_SUCCESS
                        fetchToken()
                    } else {
                        errorThrow.postValue(true)
                    }
                },
                {
                    errorThrow.postValue(true)
                }
            )
        }
    }

    /**
     * start confirm signin guest user
     */
    private fun startConfirmSignInGuestUser() {
        AWSUtils.confirmSignIn(
            "123123",
            { result ->
                if (result.isSignedIn) {
                    /**
                     * set new state
                     */
                    stateSignUpGuestAccount = GUEST_SIGN_UP_STATE.CONFIRM_SIGNIN

                    /**
                     * fetch token when confirm sign in success
                     */
                    fetchToken()
                } else {
                    errorThrow.postValue(true)
                }
            },
            {
                errorThrow.postValue(true)
            }
        )
    }

    /**
     * fetch token
     */
    private fun fetchToken() {
        AWSUtils.checkUserAuthSession(object : AWSUtils.AuthSessionCallback {
            override fun onAuthSuccess(token: String?) {
                if (token != null) {

                    stateSignUpGuestAccount = GUEST_SIGN_UP_STATE.FETCH_TOKEN_SUCCESS

                    /**
                     * set token to interceptor
                     */
                    myServiceInterceptor.setSessionToken(token)


                    /**
                     * fetch user attr
                     */
                    fetchUserAttr()

                    /**
                     * end follow sign up fetch token
                     */
                } else {
                    errorThrow.postValue(true)
                }
            }

            override fun onAuthFailed() {
                errorThrow.postValue(true)
            }
        })
    }


    /**
     * fetch user attr
     */
    private fun fetchUserAttr() {
        amplifyFetchUserAttributes(breezeUserPreferenceUtil) { state ->

            stateSignUpGuestAccount = GUEST_SIGN_UP_STATE.FETCH_USER_ATTR

            when (state) {
                STATE_FETCH_USER_ATTR.USER_USERNAME_AVAILABLE -> {
                    InitObjectUtilsController.currentObjectFlowLoginSignUp?.isUserNameAvailable =
                        true
                    /**
                     * fetch setting user
                     */
                    fetchSettingUser()
                }

                STATE_FETCH_USER_ATTR.USER_NO_USERNAME_ATTRIBUTE -> {
                    InitObjectUtilsController.currentObjectFlowLoginSignUp?.isUserNameAvailable =
                        false

                    /**
                     * fetch setting user
                     */
                    fetchSettingUser()
                }

                STATE_FETCH_USER_ATTR.FETCH_ATTR_USER_FAILS -> {
                    errorThrow.postValue(true)
                }
            }
        }
    }

    /**
     * fetch setting user & all setting in system
     */
    private fun fetchSettingUser() {
        getAllSettingZipData(breezeUserPreferenceUtil, apiHelper) { isFetchSuccess ->
            if (isFetchSuccess) {
                stateSignUpGuestAccount = GUEST_SIGN_UP_STATE.FETCH_USER_SETTING_AND_ALL_SETTING
                /**
                 * send event with is user name available
                 */
                signUpSuccess.postValue(true)
            } else {
                errorThrow.postValue(true)
            }
        }
    }


    /**
     * reSignUp follow with guest mode
     */
    fun restartSignUpWithGuestMode() {
        AWSUtils.init(getApp())
        when (stateSignUpGuestAccount) {
            GUEST_SIGN_UP_STATE.GET_ID_GUEST_NOT_SUCCESS -> {
                genIDGuest()
            }
            GUEST_SIGN_UP_STATE.GET_ID_GUEST_SUCCESS -> {
                startSignUpGuestUser()
            }

            GUEST_SIGN_UP_STATE.SIGNUP_SUCCESS -> {
                startSignInGuestUser()
            }

            GUEST_SIGN_UP_STATE.NEED_CONFIRM_SIGNIN -> {
                startConfirmSignInGuestUser()
            }

            GUEST_SIGN_UP_STATE.CONFIRM_SIGNIN -> {
                fetchToken()
            }

            GUEST_SIGN_UP_STATE.FETCH_TOKEN_SUCCESS -> {
                fetchUserAttr()
            }

            GUEST_SIGN_UP_STATE.FETCH_USER_ATTR -> {
                fetchSettingUser()
            }
        }
    }


    /**
     * update term of condition
     */
    fun updateTncStatus() {
        val request = JsonObject()
        request.addProperty(
            "tncAcceptStatus",
            BreezeUserPreference.getInstance(getApp()).getTncStatus()
        )

        mAPIHelper.updateTncAcceptedStatus(request)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : ApiObserver<JsonElement>(compositeDisposable) {
                override fun onSuccess(data: JsonElement) {
                    Timber.d("Update T&C status API Successful")
                    Timber.d("Response from update T&C status API ${data}")
                    viewModelScope.launch(Dispatchers.Main) {
                        BreezeUserPreference.getInstance(getApp()).clearTncStatus()
                    }
                }

                override fun onError(e: ErrorData) {
                    Timber.d("API failed: " + e.toString())
                }
            })
    }

    private fun logoutApp(callback: () -> Unit) {
        AWSUtils.signOut(callback)
    }


}
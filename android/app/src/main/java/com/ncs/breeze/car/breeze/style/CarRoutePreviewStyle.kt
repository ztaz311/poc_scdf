package com.ncs.breeze.car.breeze.style

import android.graphics.BitmapFactory
import com.breeze.model.constants.Constants
import com.ncs.breeze.common.utils.RoutePlanningConsts
import com.mapbox.maps.extension.androidauto.MapboxCarMapSurface
import com.mapbox.maps.extension.androidauto.MapboxCarMapObserver
import com.ncs.breeze.car.breeze.MainBreezeCarContext
import com.ncs.breeze.R

class CarRoutePreviewStyle(
    private val mainCarContext: MainBreezeCarContext
) : MapboxCarMapObserver {


   override fun onAttached(mapboxCarMapSurface: MapboxCarMapSurface) {
        mapboxCarMapSurface.mapSurface.getMapboxMap().getStyle()?.let {
            val resources = mainCarContext.carContext.resources;
            it.addImage(
                RoutePlanningConsts.SELECTED_ERP_RATE_IMG, BitmapFactory.decodeResource(
                    resources,
                    R.drawable.route_info_blue_down
                ), false
            )

            it.addImage(
                RoutePlanningConsts.UNSELECTED_ERP_RATE_IMG, BitmapFactory.decodeResource(
                    resources,
                    R.drawable.route_info_white_down
                ), false
            )

            it.addImage(
                RoutePlanningConsts.ROUTE_ERP_IMG, BitmapFactory.decodeResource(
                    resources,
                    R.drawable.erp_small
                ), false
            )
            it.removeStyleLayer(Constants.TRAFFIC_LAYER_ID)
        }
    }

}
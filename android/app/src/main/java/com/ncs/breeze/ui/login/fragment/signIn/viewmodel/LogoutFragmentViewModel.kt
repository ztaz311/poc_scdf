package com.ncs.breeze.ui.login.fragment.signIn.viewmodel

import android.app.Application
import com.ncs.breeze.ui.base.BaseFragmentViewModel
import javax.inject.Inject

class LogoutFragmentViewModel @Inject constructor(application: Application) : BaseFragmentViewModel(
    application
) {
}
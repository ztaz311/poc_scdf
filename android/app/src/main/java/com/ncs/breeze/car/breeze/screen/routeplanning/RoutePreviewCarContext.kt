package com.ncs.breeze.car.breeze.screen.routeplanning

import com.ncs.breeze.car.breeze.MainBreezeCarContext


data class RoutePreviewCarContext internal constructor(
    val mainCarContext: MainBreezeCarContext
) {
    /** MainCarContext **/
    val carContext = mainCarContext.carContext
    val mapboxCarMap = mainCarContext.mapboxCarMap
    val distanceFormatter = mainCarContext.distanceFormatter
}

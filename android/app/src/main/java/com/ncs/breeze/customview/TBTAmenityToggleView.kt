package com.ncs.breeze.customview

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.core.view.isVisible
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.Event
import com.ncs.breeze.common.analytics.Screen
import com.breeze.model.extensions.dpToPx
import com.ncs.breeze.databinding.LayoutTbtAmenityToggleViewBinding

class TBTAmenityToggleView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null
) : LinearLayout(context, attrs) {
    private val binding: LayoutTbtAmenityToggleViewBinding

    private var _onDisplayModeChanged: (displayMode: DisplayMode) -> Unit = {}
    private var _onParkingDisplayModeChanged: ((isSelected: Boolean) -> Unit)? = null
    var displayMode = DisplayMode.NONE
        private set(value) {
            field = value
            binding.btnParkingToggle.isSelected = value == DisplayMode.PARKING
            binding.btnEVToggle.isSelected = value == DisplayMode.EV
            binding.btnPetrolToggle.isSelected = value == DisplayMode.PETROL
        }

    fun getScreenName(): String {
        return Screen.NAVIGATION
    }

    init {
        orientation = VERTICAL
        setPadding(4.dpToPx(), 4.dpToPx(), 4.dpToPx(), 4.dpToPx())
//        setBackgroundResource(R.drawable.bg_custom_navigation_amenities_view)
        binding = LayoutTbtAmenityToggleViewBinding.inflate(LayoutInflater.from(context), this)

        with(binding) {
            btnParkingToggle.setOnClickListener {
                displayMode =
                    if (displayMode == DisplayMode.PARKING) {
//                        Analytics.logToggleEvent(Event.OBU_DISPLAY_TOGGLE_PETROL_OFF, getScreenName())
                        DisplayMode.NONE
                    } else {
//                        Analytics.logToggleEvent(Event.OBU_DISPLAY_TOGGLE_PETROL_ON, getScreenName())
                        DisplayMode.PARKING
                    }
                _onDisplayModeChanged.invoke(displayMode)
                _onParkingDisplayModeChanged?.invoke(it.isSelected)
            }
            btnPetrolToggle.setOnClickListener {
                displayMode =
                    if (displayMode == DisplayMode.PETROL) {
                        Analytics.logToggleEvent(
                            Event.OBU_DISPLAY_TOGGLE_PETROL_OFF,
                            getScreenName()
                        )
                        DisplayMode.NONE
                    } else {
                        Analytics.logToggleEvent(
                            Event.OBU_DISPLAY_TOGGLE_PETROL_ON,
                            getScreenName()
                        )
                        DisplayMode.PETROL
                    }
                _onDisplayModeChanged.invoke(displayMode)
            }
            btnEVToggle.setOnClickListener {
                displayMode =
                    if (displayMode == DisplayMode.EV) {
                        Analytics.logToggleEvent(Event.OBU_DISPLAY_TOGGLE_EV_OFF, getScreenName())
                        DisplayMode.NONE
                    } else {
                        Analytics.logToggleEvent(Event.OBU_DISPLAY_TOGGLE_EV_ON, getScreenName())
                        DisplayMode.EV
                    }
                _onDisplayModeChanged.invoke(displayMode)
            }
        }
    }

    fun enableParkingButton(enable: Boolean) {
        binding.btnParkingToggle.isVisible = enable
    }

    fun isParkingButtonShowed(): Boolean = binding.btnParkingToggle.isVisible

    fun updateDisplayMode(displayMode: DisplayMode) {
        if (displayMode == DisplayMode.PARKING && !binding.btnParkingToggle.isVisible)
            return
        this.displayMode = displayMode
    }

    fun setOnDisplayModeChanged(listener: (displayMode: DisplayMode) -> Unit) {
        _onDisplayModeChanged = listener
    }

    fun setOnParkingButtonStateChanged(listener: (isSelected: Boolean) -> Unit){
        _onParkingDisplayModeChanged = listener
    }

    enum class DisplayMode {
        NONE,
        PARKING,
        EV,
        PETROL
    }
}
package com.ncs.breeze.common.utils

import android.content.Context
import android.location.Location
import android.os.Handler
import android.os.HandlerThread
import android.os.SystemClock
import com.breeze.model.DestinationAddressDetails
import com.breeze.model.ERPCost
import com.breeze.model.TripLog
import com.breeze.model.TripStatus
import com.breeze.model.TripSummary
import com.breeze.model.TripType
import com.breeze.model.TripWalkingSummary
import com.breeze.model.api.ErrorData
import com.breeze.model.constants.Constants
import com.breeze.model.constants.TripOBUStatus
import com.google.firebase.Timestamp
import com.google.gson.Gson
import com.google.gson.JsonElement
import com.ncs.breeze.R
import com.ncs.breeze.common.remote.ApiHelper
import com.ncs.breeze.common.remote.ApiObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import timber.log.Timber
import java.lang.ref.WeakReference
import java.util.UUID
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class BreezeTripLoggingUtil @Inject constructor(appContext: Context, val apiHelper: ApiHelper) {

    companion object {
        const val BACKGROUND_THREAD_NAME = "TripLoggingHandlerThread"
        const val TRIP_END_DESC = "Trip End"
        const val TRIP_LOG_DELAY: Long = 120000
        const val ERP_DUPLICATE_GRACE_TIME: Int = (60)
    }
    private val contextRef = WeakReference(appContext)
    var compositeDisposable: CompositeDisposable = CompositeDisposable()

//    var kinesisFirehoseRecorder = KinesisFirehoseRecorder(
//        appContext.cacheDir,
//        Regions.AP_SOUTHEAST_1,
//        AWSMobileClient.getInstance()
//    )
    lateinit var handlerThread: HandlerThread

    // Trip Summary variables
    var tripID: String? = null
    var totalListOfERPDetected: ArrayList<ERPCost> = arrayListOf()
    var totalDistanceTravelled: Double = 0.0
    var totalERPCost: Float = 0.0F
    var startTimeOfTrip: Long? = null
    var endTimeOfTrip: Long? = null
    var destinationAddress: DestinationAddressDetails? = null
    var startTripLocation: Location? = null
    var endTripLocation: Location? = null

    // Trip Log variables
    var listOfERPDetected: ArrayList<ERPCost> = arrayListOf()
    var distanceTravelled: Double = 0.0
    var currentLocation: Location? = null

    var currentTripType: TripType? = null
    var isWalking: Boolean = false

    private var idleTimeInMillis = 0L
    private var idleStartTime = 0L
    private var isIdle = false
    private var tripOBUStatus: String? = null

    private val runnableCode = Runnable { // Do something here on the main thread
        sendTripLogData(TripStatus.NAVIGATION)
    }

    lateinit var backgroundHandler: Handler

    fun changeOBUStatus(status: String) {
        if (tripOBUStatus == null || tripOBUStatus == TripOBUStatus.NOT_PAIRED) {
            tripOBUStatus = status
        } else {
            tripOBUStatus = when (tripOBUStatus) {
                TripOBUStatus.NOT_CONNECTED -> {
                    if (status == TripOBUStatus.CONNECTED) {
                        TripOBUStatus.PARTIALLY_CONNECTED
                    } else {
                        status
                    }
                }

                TripOBUStatus.CONNECTED -> {
                    if (status == TripOBUStatus.NOT_CONNECTED) {
                        TripOBUStatus.PARTIALLY_CONNECTED
                    } else {
                        status
                    }
                }

                else -> {
                    TripOBUStatus.PARTIALLY_CONNECTED
                }
            }
        }
    }

    fun startCountIdleTime() {
        if (!isIdle) {
            isIdle = true
            idleStartTime = SystemClock.elapsedRealtime()
        }
    }

    fun stopCountIdleTime() {
        if (!isIdle) return
        isIdle = false
        val idle = SystemClock.elapsedRealtime() - idleStartTime
        idleTimeInMillis += idle
    }

    fun startTripLogging(
        startLocation: Location?,
        tripType: TripType,
        mdestinationAddress: DestinationAddressDetails?,
        isWalking: Boolean
    ) {
        tripOBUStatus = null
        idleTimeInMillis = 0L
        //Timber.d("Trip Type is: " + tripType.type)
        handlerThread = HandlerThread(BACKGROUND_THREAD_NAME)
        handlerThread.start()
        backgroundHandler = Handler(handlerThread.looper)

        clearTripSummaryData()
        startTimeOfTrip = Timestamp.now().seconds
        tripID = generateTripID()
        this.isWalking = isWalking
        startTripLocation = startLocation
        currentLocation = startLocation
        currentTripType = tripType
        destinationAddress = mdestinationAddress
        sendTripLogData(TripStatus.START)
    }

    fun stopTripLogging(
        endLocation: Location?,
        endType: String,
        endAddress: DestinationAddressDetails? = null,
        amenityId: String? = null,
        layerCode: String? = null,
        trackNavigation: Boolean? = false,
        amenityType: String? = null,
        bookmarkId: Int? = null,
        bookmarkName: String? = null,
        obuName: String? = null,
        onFinished: (result: Boolean) -> Unit = {}
    ) {
        backgroundHandler.removeCallbacksAndMessages(null)
        endTimeOfTrip = Timestamp.now().seconds
        endTripLocation = endLocation
        sendTripLogData(TripStatus.END)

        sendCurrentTripSummary(
            endAddress,
            isWalking,
            endType,
            amenityId,
            layerCode,
            trackNavigation,
            amenityType,
            bookmarkId,
            bookmarkName,
            obuName,
            onFinished
        )

        clearTripSummaryData()
        handlerThread.quitSafely()
    }

    private fun sendCurrentTripSummary(
        endAddress: DestinationAddressDetails?,
        isWalking: Boolean,
        endType: String,
        amenityId: String? = null,
        layerCode: String? = null,
        trackNavigation: Boolean? = false,
        amenityType: String?,
        bookmarkId: Int? = null,
        bookmarkName: String? = null,
        obuName: String? = null,
        onFinished: (result: Boolean) -> Unit = {}
    ) {
        //storing local instances before launching thread since these values will be cleared immediately
        val tripID = this.tripID;
        val startTimeOfTrip = this.startTimeOfTrip;
        val endTimeOfTrip = this.endTimeOfTrip;
        val startTripLocation = this.startTripLocation;
        val endTripLocation = this.endTripLocation;
        val totalERPCost = this.totalERPCost;
        val totalDistanceTravelled = this.totalDistanceTravelled;
        val totalListOfERPDetected = this.totalListOfERPDetected;
        val tripType = currentTripType!!.type

        CoroutineScope(Dispatchers.IO).launch {
            sendTripSummary(
                type = tripType,
                destinationAddress = endAddress,
                tripID = tripID,
                startTimeOfTrip = startTimeOfTrip,
                endTimeOfTrip = endTimeOfTrip,
                startTripLocation = startTripLocation,
                endTripLocation = endTripLocation,
                totalERPCost = totalERPCost,
                totalDistanceTravelled = totalDistanceTravelled,
                totalListOfERPDetected = totalListOfERPDetected,
                isWalking = isWalking,
                endType = endType,
                amenityId = amenityId,
                layerCode = layerCode,
                trackNavigation = trackNavigation,
                amenityType,
                bookmarkId = bookmarkId,
                bookmarkName = bookmarkName,
                obuName = obuName,
                onFinished = onFinished
            )
        }
    }

    private fun sendKinesisData(jsonData: String) {
//        CoroutineScope(Dispatchers.IO).launch {
//            try {
//                //Timber.d("Kinesis data sent ${jsonData}")
//                kinesisFirehoseRecorder.saveRecord(
//                    jsonData, BuildConfig.AMPLIFY_FIREHOSE_STREAMNAME
//                )
//                kinesisFirehoseRecorder.submitAllRecords()
//            } catch (e: Exception) {
//                Timber.e(e, "Exception when saving the kinesis data")
//            }
//        }
    }

    fun updateDistance(location: Location?, distanceUpdate: Double) {
        if (location != null) {
            currentLocation = location
        }
        distanceTravelled += distanceUpdate
        totalDistanceTravelled += distanceUpdate
    }

    fun updateERPDetection(detectedERPObj: ERPCost) {
        Timber.d("Adding detected EPR: ${detectedERPObj}")

        if (!listOfERPDetected.any { it.erpId == detectedERPObj.erpId }) {
            addERP(detectedERPObj)
        } else {
            var prevERPList = listOfERPDetected.filter { it.erpId == detectedERPObj.erpId }
            var isDuplicate = false
            for (erp in prevERPList) {
                if (Math.abs(erp.time - detectedERPObj.time) < ERP_DUPLICATE_GRACE_TIME) {
                    isDuplicate = true
                }
            }

            if (!isDuplicate) {
                addERP(detectedERPObj)
            }
        }
    }

    private fun addERP(detectedERPObj: ERPCost) {
        listOfERPDetected.add(detectedERPObj)
        totalListOfERPDetected.add(detectedERPObj)
        totalERPCost += detectedERPObj.originalAmount
    }

    suspend fun sendTripSummary(
        type: String,
        destinationAddress: DestinationAddressDetails? = null,
        tripID: String?,
        startTimeOfTrip: Long?,
        endTimeOfTrip: Long?,
        startTripLocation: Location?,
        endTripLocation: Location?,
        totalERPCost: Float,
        totalDistanceTravelled: Double,
        totalListOfERPDetected: ArrayList<ERPCost>,
        isWalking: Boolean,
        endType: String,
        amenityId: String? = null,
        layerCode: String? = null,
        trackNavigation: Boolean? = false,
        amenityType: String?,
        bookmarkId: Int? = null,
        bookmarkName: String? = null,
        obuName: String? = null,
        onFinished: (result: Boolean) -> Unit = {}
    ) {

        Timber.d("TripLog-sendTripSummary")
        if (
            tripID != null &&
            startTimeOfTrip != null &&
            endTimeOfTrip != null &&
            startTripLocation != null &&
            endTripLocation != null
        ) {
            val appContext = contextRef.get()
            val startAddress = appContext?.let { Utils.getAddressObject(startTripLocation, it) }

            var startAddress1 = appContext?.getString(R.string.default_location) ?: ""
            var startAddress2 = ""
            if (startAddress != null) {
                if (startAddress.maxAddressLineIndex != -1) {
                    startAddress1 = startAddress.getAddressLine(0)
                    var address2 = startAddress.getAddressLine(1)
                    if (address2 != null) {
                        startAddress2 = address2
                    }
                }
            }
            //-----------------------------------------------------------------------------------
            var planned_dest_address1 = ""
            var planned_dest_address2 = ""
            val planned_dest_long = destinationAddress?.long
            val planned_dest_lat = destinationAddress?.lat
            if (destinationAddress != null) {
                // have no address planed - for example location carpark || explore map
                if (destinationAddress.address1 == null) {
                    val locationPlaned = Location("")
                    locationPlaned.latitude = destinationAddress.lat?.toDouble() ?: 0.0
                    locationPlaned.longitude = destinationAddress.long?.toDouble() ?: 0.0
                    val addressLocationPlaned = appContext?.let { Utils.getAddressObject(locationPlaned, it) }
                    if (addressLocationPlaned != null) {
                        if (addressLocationPlaned.maxAddressLineIndex != -1) {
                            planned_dest_address1 = addressLocationPlaned.getAddressLine(0)
                            val address2 = addressLocationPlaned.getAddressLine(1)
                            if (address2 != null) {
                                planned_dest_address2 = address2
                            }
                        }
                    }
                } else {
                    planned_dest_address1 = destinationAddress.address1 ?: ""
                    planned_dest_address2 = destinationAddress.address2 ?: ""
                }
            }

            //---------------------------------------------------------------------------------------
            var destAddress1 = appContext?.getString(R.string.default_location) ?: ""
            var destAddress2 = ""
            /**
             * in case stop navigation manual
             */
            if (endType == Constants.TYPE_END_NAVIGATION.MANUAL) {
                endTripLocation.apply {
                    /**
                     * get location end trip
                     */
                    val endLocationAddress = appContext?.let { Utils.getAddressObject(this, it) }
                    if (endLocationAddress != null) {
                        if (endLocationAddress.maxAddressLineIndex != -1) {
                            destAddress1 = endLocationAddress.getAddressLine(0)
                            val address2 = endLocationAddress.getAddressLine(1)
                            if (address2 != null) {
                                destAddress2 = address2
                            }
                        }
                    }
                }
            } else {
                destAddress1 = planned_dest_address1
                destAddress2 = planned_dest_address2
            }


            //---------------------------------------------------------------------------------

            val carParId = destinationAddress?.carParkID
            val isCarPark = (carParId != null && destinationAddress.isDestinationCarPark())
            if (isIdle) {
                stopCountIdleTime()
            }
            val totalIdleTimeInMinutes: Long =
                if (idleTimeInMillis in 1..60000) 1 else idleTimeInMillis / 60000
            if (!isWalking) {
                val tripSummary = TripSummary(
                    type = type,
                    tripGUID = tripID,
                    tripStartTime = startTimeOfTrip,
                    tripStartAddress1 = startAddress1,
                    tripStartAddress2 = startAddress2,
                    tripStartLat = startTripLocation.latitude,
                    tripStartLong = startTripLocation.longitude,
                    tripEndTime = endTimeOfTrip,
                    tripDestAddress1 = destAddress1,
                    tripDestAddress2 = destAddress2,
                    tripDestLat = endTripLocation.latitude,
                    tripDestLong = endTripLocation.longitude,
                    totalExpense = totalERPCost,
                    totalDistance = totalDistanceTravelled / 1000.0,
                    erp = totalListOfERPDetected,
                    isDestinationCarpark = isCarPark,
                    navigationEndType = endType,
                    carParkId = carParId,
                    amenityId = amenityId,
                    amenityType = amenityType,
                    plannedDestAddress1 = planned_dest_address1,
                    plannedDestAddress2 = planned_dest_address2,
                    plannedDestLat = planned_dest_lat?.toDouble() ?: 0.0,
                    plannedDestLong = planned_dest_long?.toDouble() ?: 0.0,
                    layerCode = layerCode,
                    idleTime = totalIdleTimeInMinutes,
                    vehicleNumber = obuName,// use obuName instead of vehicle number
                    bookmarkId = bookmarkId,
                    bookmarkName = bookmarkName,
                    obuStatus = tripOBUStatus ?: TripOBUStatus.NOT_PAIRED

                )
                Timber.d("Printing Trip summary API: " + tripSummary.toString())
                triggerTripSummaryAPI(tripSummary, onFinished)
            } else {
                val tripWalkingSummary = TripWalkingSummary(
                    type = type,
                    tripGUID = tripID,
                    tripStartTime = startTimeOfTrip,
                    tripStartAddress1 = startAddress1,
                    tripStartAddress2 = startAddress2,
                    tripStartLat = startTripLocation.latitude,
                    tripStartLong = startTripLocation.longitude,
                    tripEndTime = endTimeOfTrip,
                    tripDestAddress1 = destAddress1,
                    tripDestAddress2 = destAddress2,
                    tripDestLat = endTripLocation.latitude,
                    tripDestLong = endTripLocation.longitude,
                    totalDistance = totalDistanceTravelled / 1000.0,
                    isDestinationCarpark = isCarPark,
                    navigationEndType = endType,
                    carParkId = carParId,
                    amenityId = amenityId,
                    layerCode = layerCode,
                    plannedDestAddress1 = planned_dest_address1,
                    plannedDestAddress2 = planned_dest_address2,
                    plannedDestLat = planned_dest_lat?.toDouble() ?: 0.0,
                    plannedDestLong = planned_dest_long?.toDouble() ?: 0.0,
                )
                Timber.d("Printing Trip Walking summary API: " + tripWalkingSummary.toString())
                triggerTripWalkingSummaryAPI(tripWalkingSummary, onFinished)
            }
        } else {
            Timber.d("Trip summary Condition failed")
        }
    }

    fun getCurrentTripID(): String? {
        return tripID
    }

    private fun triggerTripSummaryAPI(
        tripSummary: TripSummary,
        onFinished: (result: Boolean) -> Unit = {}
    ) {
        apiHelper.setTripSummary(tripSummary)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : ApiObserver<JsonElement>(compositeDisposable) {
                override fun onSuccess(data: JsonElement) {
                    Timber.d("")
                    onFinished.invoke(true)
                }

                override fun onError(e: ErrorData) {
                    Timber.d("API failed: " + e.toString())
                    onFinished.invoke(false)
                }

            })
    }

    private fun triggerTripWalkingSummaryAPI(
        tripWalkingSummary: TripWalkingSummary,
        onFinished: (result: Boolean) -> Unit
    ) {
        apiHelper.setTripWalkingSummary(tripWalkingSummary)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : ApiObserver<JsonElement>(compositeDisposable) {
                override fun onSuccess(data: JsonElement) {
                    Timber.d("")
                    onFinished.invoke(true)
                }

                override fun onError(e: ErrorData) {
                    Timber.d("API failed: " + e.toString())
                    onFinished.invoke(false)
                }

            })
    }

    private fun sendTripLogData(status: TripStatus = TripStatus.NAVIGATION) {
        if (tripID != null && currentLocation != null) {
            var tripLog = TripLog(
                tripID!!,
                Timestamp.now().seconds,
                status.type,
                currentLocation!!.latitude,
                currentLocation!!.longitude,
                distanceTravelled / 1000.0,
                listOfERPDetected
            )
            sendKinesisData(Gson().toJson(tripLog))
        }
        clearTripLogData()
        backgroundHandler.postDelayed(runnableCode, TRIP_LOG_DELAY)
    }

    private fun clearTripLogData() {
        listOfERPDetected = arrayListOf()
        distanceTravelled = 0.0
    }

    private fun clearTripSummaryData() {
        // Trip Summary
        tripID = null
        totalListOfERPDetected = arrayListOf()
        totalDistanceTravelled = 0.0
        totalERPCost = 0F
        startTimeOfTrip = null
        endTimeOfTrip = null

        // Trip log
        listOfERPDetected = arrayListOf()
        distanceTravelled = 0.0
    }

    private fun generateTripID() = UUID.randomUUID().toString()

}
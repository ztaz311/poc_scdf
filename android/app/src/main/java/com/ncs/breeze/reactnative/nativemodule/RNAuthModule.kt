package com.ncs.breeze.reactnative.nativemodule

import com.facebook.react.bridge.Promise
import com.facebook.react.bridge.ReactApplicationContext
import com.facebook.react.bridge.ReactContextBaseJavaModule
import com.facebook.react.bridge.ReactMethod
import com.ncs.breeze.BuildConfig
import com.ncs.breeze.common.remote.MyServiceInterceptor
import timber.log.Timber


class RNAuthModule internal constructor(
    val context: ReactApplicationContext?,
    val myServiceInterceptor: MyServiceInterceptor
) :
    ReactContextBaseJavaModule(context) {

    override fun getName() = "RNAuth"

    @ReactMethod
    fun refreshIdToken(promise: Promise) {
        // Implement your logic here
        Timber.d("Refresh ID token called")
        myServiceInterceptor.getValidSessionToken(promise)
    }

    override fun getConstants(): MutableMap<String, Any> = hashMapOf(
        "API_BASE_URL" to BuildConfig.SERVER_HEADER
    )
}

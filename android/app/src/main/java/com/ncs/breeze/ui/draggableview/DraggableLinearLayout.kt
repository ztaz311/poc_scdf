package com.ncs.breeze.ui.draggableview

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import android.widget.LinearLayout

class DraggableLinearLayout : LinearLayout {
    private var initialTouchX = 0f
    private var initialTouchY = 0f

    constructor(context: Context?) : super(context) {}
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {}
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
    }

    constructor(
        context: Context?,
        attrs: AttributeSet?,
        defStyleAttr: Int,
        defStyleRes: Int
    ) : super(context, attrs, defStyleAttr, defStyleRes) {
    }

    override fun onInterceptTouchEvent(event: MotionEvent): Boolean {
        when (event.actionMasked) {
            MotionEvent.ACTION_DOWN -> {
                initialTouchX = event.x
                initialTouchY = event.y
                return false
            }
            MotionEvent.ACTION_UP -> {
                val xDiff = (event.x - initialTouchX).toInt()
                val yDiff = (event.y - initialTouchY).toInt()



                return if (xDiff < 5 && yDiff < 5) {
                    super.onInterceptTouchEvent(event)
                } else false
            }
            MotionEvent.ACTION_MOVE -> {
                val moveX = (event.x - initialTouchX + x).toInt()
                val moveY = (event.y - initialTouchY + y).toInt()
                x = moveX.toFloat()
                y = moveY.toFloat()

                invalidate()
            }
        }
        return false
    }
}
package com.ncs.breeze.components.marker.markerview

import androidx.annotation.UiThread
import com.mapbox.maps.MapView
import com.mapbox.maps.MapboxMap
import com.mapbox.maps.extension.observable.eventdata.CameraChangedEventData
import com.mapbox.maps.plugin.delegates.listeners.OnCameraChangeListener

/**
 * Create a MarkerViewManager.
 *
 * @param mapView   the MapView used to synchronise views on
 * @param mapboxMap the MapboxMap to synchronise views with
 */
open class MarkerViewManager(private val mapView: MapView, private val mapboxMap: MapboxMap) {
    private val markers: MutableList<MarkerView> = ArrayList()
    private var initialised = false

    init {
        mapboxMap.addOnCameraChangeListener(object : OnCameraChangeListener {
            override fun onCameraChanged(eventData: CameraChangedEventData) {
                update()
            }
        })
    }

    /**
     * Destroys the MarkerViewManager.
     * Should be called before MapView#onDestroy
     */
    @UiThread
    fun onDestroy() {
        markers.clear()

        initialised = false
    }

    /**
     * Add a MarkerView to the map using MarkerView and LatLng.
     *
     * @param markerView the markerView to synchronise on the map
     */
    @UiThread
    fun addMarker(markerView: MarkerView) {
        if (markers.contains(markerView)) {
            return
        }
        if (!initialised) {
            initialised = true

        }

        removeMarker(markerView)
        mapView.addView(markerView.view)
        markers.add(markerView)
        markerView.update()
    }

    /**
     * Remove an existing markerView from the map.
     *
     * @param markerView the markerView to be removed from the map
     */
    @UiThread
    fun removeMarker(markerView: MarkerView) {
        if (!markers.contains(markerView)) {
            return
        }
        mapView.removeView(markerView.view)
        markers.remove(markerView)
    }

    private fun update() {
        for (marker in markers) {
            marker.update()
        }
    }

}
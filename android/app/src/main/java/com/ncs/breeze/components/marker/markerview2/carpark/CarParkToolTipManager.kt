package com.ncs.breeze.components.marker.markerview2.carpark

import android.app.Activity
import android.graphics.Color
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.widget.TextView
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import com.breeze.model.api.response.amenities.BaseAmenity
import com.breeze.model.constants.Constants
import com.breeze.model.enums.AmenityBand
import com.breeze.model.extensions.dpToPx
import com.breeze.model.extensions.roundToAmount
import com.breeze.model.extensions.visibleAmount
import com.facebook.react.bridge.Arguments
import com.mapbox.geojson.Point
import com.mapbox.maps.EdgeInsets
import com.mapbox.maps.MapboxMap
import com.ncs.breeze.App
import com.ncs.breeze.R
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.Event
import com.ncs.breeze.common.rxbus.RxBus
import com.ncs.breeze.common.rxbus.RxEvent
import com.ncs.breeze.common.utils.BreezeMapZoomUtil
import com.ncs.breeze.databinding.CarparkMarkerTooltipBinding
import com.ncs.breeze.reactnative.nativemodule.ShareBusinessLogicEvent
import com.ncs.breeze.ui.base.BaseActivity
import com.ncs.breeze.ui.dashboard.fragments.placesave.PlaceSaveDetailFragment
import com.ncs.breeze.ui.dashboard.manager.ExploreMapStateManager
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class CarParkToolTipManager(
    private val viewBinding: CarparkMarkerTooltipBinding,
    val mapboxMap: MapboxMap,
    val activity: Activity,
    private val toolTipZoomRange: Double = Constants.DEFAULT_TOOLTIP_ZOOM_RADIUS,
    private val voucherEnabled: Boolean
) : ToolTipManager {

    var currentDataMarker: BaseAmenity? = null
    private var fromLandingPage: Boolean = false

    override fun clickToolTip(
        item: BaseAmenity,
        shouldRecenter: Boolean,
        navigationButtonClick: (BaseAmenity, BaseActivity.EventSource) -> Unit,
        moreInfoButtonClick: (BaseAmenity) -> Unit,
        carParkIconClick: (Boolean) -> Unit,
        screenName: String
    ) {
        fromLandingPage = true

//        /**
//         * btn calculate fee
//         */
//        with(viewBinding.btnCalculateFee) {
//            setOnClickListener {
//                currentDataMarker?.id?.let {
//                    Analytics.logClickEvent(Event.PARKING_CALCULATE_FEE, screenName)
//                    sendEventOpenCalculateFee(it)
//                }
//            }
//            val isInParkingTab = (context as? DashboardActivity)?.let { dashboardActivity ->
//                ViewModelProvider(dashboardActivity).get<DashboardViewModel>().currentLandingModeFlow.value == LandingMode.PARKING
//            } ?: false
//            isVisible = isInParkingTab
//                    && item.parkingType != Constants.PARKING_TYPE_SEASON_PARKING
//                    && item.parkingType != Constants.PARKING_TYPE_CUSTOMER_PARKING
//        }

        viewBinding.seeMore.setOnClickListener {
            if (ExploreMapStateManager.isOnExploreMapScreen) {
                RxBus.publish(RxEvent.EventOpenSeeMore(currentDataMarker?.id!!))
            } else {
                moreInfoButtonClick.invoke(item)

                /**
                 * send event to react open carpark detail
                 */
                currentDataMarker?.id?.let {
                    sendEventParkingISelected(it)
                }
            }

        }

        manageViews(item, shouldRecenter, carParkIconClick, navigationButtonClick, screenName)
    }

    override fun clickRouteToolTip(
        item: BaseAmenity,
        shouldRecenter: Boolean,
        navigationButtonClick: (BaseAmenity, BaseActivity.EventSource) -> Unit,
        moreInfoButtonClick: (BaseAmenity) -> Unit,
        carParkIconClick: (Boolean) -> Unit,
        calculateFee: (BaseAmenity) -> Unit,
        screenName: String
    ) {
        fromLandingPage = false

//        /**
//         * check if in carpark mode => invisible btn calculate fee
//         */
////        btn_calculate_fee?.visibility = View.VISIBLE
//        viewBinding.btnCalculateFee.visibility =
//            if (item.parkingType.equals(Constants.PARKING_TYPE_SEASON_PARKING)
//                || item.parkingType.equals(Constants.PARKING_TYPE_CUSTOMER_PARKING)
//            ) {
//                View.GONE
//            } else {
//                viewBinding.btnCalculateFee.setOnClickListener {
//                    calculateFee.invoke(item)
//                }
//                View.VISIBLE
//            }

        viewBinding.seeMore.setOnClickListener {
            moreInfoButtonClick.invoke(item)
        }

        manageViews(item, shouldRecenter, carParkIconClick, navigationButtonClick, screenName)
    }

    private fun manageViews(
        item: BaseAmenity,
        shouldRecenter: Boolean,
        carParkIconClick: (Boolean) -> Unit,
        navigationButtonClick: (BaseAmenity, BaseActivity.EventSource) -> Unit,
        screenName: String
    ) {

        if (item.hasVouchers) {
            Analytics.logClickEvent(Event.TAP_VOUCHER_CARPARK_CLICK, screenName)
        } else {
            Analytics.logClickEvent(Event.TAP_CARPARK_CLICK, screenName)
            Analytics.logLocationDataEvent(
                Event.TAP_CARPARK_CLICK,
                screenName,
                item.name.toString(),
                item.lat.toString(),
                item.long.toString()
            )
        }

        currentDataMarker = item
        viewBinding.tvNameCarpark.text = item.name

        /**
         * for available lot
         */
        if (item.parkingType.equals(Constants.PARKING_TYPE_SEASON_PARKING)) {
            viewBinding.llAvailableLots.visibility = View.GONE
        } else {
            if (item.availablelot.toString() == "-1") {
//                lot_available?.text = activity.getString(R.string.nearby_carpark_no_info)
                viewBinding.llAvailableLots.visibility = View.GONE
            } else {
                viewBinding.lotAvailable.text = item.availablelot.toString()
            }
        }

        /**
         * for carpark status
         */
        item.availablePercentImageBand?.let {
            handleShowCarParkStatus(item)
        }


        /**
         * current and Next HR Rate
         */
        if (item.parkingType.equals(Constants.PARKING_TYPE_SEASON_PARKING) || item.parkingType.equals(
                Constants.PARKING_TYPE_CUSTOMER_PARKING
            )
        ) {
            viewBinding.llCurrentHourPrice.visibility = View.GONE
            viewBinding.llNextHourPrice.visibility = View.GONE
            viewBinding.tvSpecialsTitle.visibility = View.GONE
        } else {
            /**
             * current HR Rate
             */


            if (item.currentHrRate?.oneHrRate != null) {
                when (item.currentHrRate?.oneHrRate) {
                    -2.0 -> {
                        viewBinding.tvNow.text = activity.getString(R.string.carpark_season)
                        setTextColor(
                            viewBinding.tvNow,
                            R.color.themed_passion_pink
                        )
                    }

                    0.0 -> {
                        viewBinding.tvNow.text = activity.getString(R.string.carpark_free)
                        setTextColor(
                            viewBinding.tvNow,
                            R.color.themed_parakeet_green
                        )
                    }

                    else -> {
                        val rate = item.currentHrRate!!.oneHrRate!!.visibleAmount()
                        viewBinding.tvNow.text = rate
                        setTextColor(
                            viewBinding.tvNow,
                            R.color.tv_color_tooltip_title
                        )
                    }
                }
            } else {
                viewBinding.tvNow.text =
                    activity.getString(R.string.nearby_carpark_no_info)
                setTextColor(viewBinding.tvNow, R.color.tv_color_tooltip_title)
            }

            /**
             * next Hr Rate
             */
            if (item.nextHrRate?.oneHrRate != null) {
                when (item.nextHrRate?.oneHrRate) {
                    -2.0 -> {
                        viewBinding.tvNextHour.text =
                            activity.getString(R.string.carpark_season)
                        setTextColor(
                            viewBinding.tvNextHour,
                            R.color.themed_passion_pink
                        )
                    }

                    0.0 -> {
                        viewBinding.tvNextHour.text =
                            activity.getString(R.string.carpark_free)
                        setTextColor(
                            viewBinding.tvNextHour,
                            R.color.themed_parakeet_green
                        )
                    }

                    else -> {
                        val rate = item.nextHrRate!!.oneHrRate!!.visibleAmount()
                        viewBinding.tvNextHour.text = rate
                        setTextColor(
                            viewBinding.tvNextHour,
                            R.color.tv_color_tooltip_title
                        )
                    }
                }
            } else {
                viewBinding.tvNextHour.text =
                    activity.getString(R.string.nearby_carpark_no_info)
                setTextColor(
                    viewBinding.tvNextHour,
                    R.color.tv_color_tooltip_title
                )
            }

            /**
             * isCheapest
             */
            if (voucherEnabled && item.hasVouchers) {
                viewBinding.tvSpecialsTitle.apply {
                    setCompoundDrawablesWithIntrinsicBounds(
                        context.getDrawable(R.drawable.voucher_tooltip),
                        null,
                        null,
                        null
                    )
                    compoundDrawablePadding = 2.dpToPx()
                    text = context.getString(
                        R.string.route_voucher_tooltip,
                        item.voucherAmount.roundToAmount()
                    )
                    setPadding(0, 0, 0, 0)
                    setTextSize(TypedValue.COMPLEX_UNIT_DIP, 14.0F)
                    background = null
                    gravity = Gravity.CENTER_VERTICAL
                    visibility = View.VISIBLE
                }
            } else {
                if (item.isCheapest == true) {
                    if (item.currentHrRate?.oneHrRate == -2.0) {//if season
                        viewBinding.tvSpecialsTitle.visibility = View.GONE
                    } else {
                        viewBinding.tvSpecialsTitle.visibility = View.VISIBLE
                    }
                } else {
                    viewBinding.tvSpecialsTitle.visibility = View.GONE
                }
            }
        }

        /**
         * navigation button click
         */
        viewBinding.confirmButton.setOnClickListener {
            resetView()
            navigationButtonClick.invoke(item, BaseActivity.EventSource.NONE)
        }

        /*
        * seasonal carpark message
        * */
        if (item.parkingType.equals(Constants.PARKING_TYPE_SEASON_PARKING)) {
            viewBinding.tvParkingTypeMessage.visibility = View.VISIBLE
            viewBinding.tvParkingTypeMessage.text =
                activity.getString(R.string.carpark_season_parking)
        } else if (item.parkingType.equals(Constants.PARKING_TYPE_CUSTOMER_PARKING)) {
            viewBinding.tvParkingTypeMessage.visibility = View.VISIBLE
            viewBinding.tvParkingTypeMessage.text =
                activity.getString(R.string.carpark_customers_only)
        } else {
            viewBinding.tvParkingTypeMessage.visibility = View.GONE
        }

        carParkOnClick(item, shouldRecenter, carParkIconClick)
    }


    /**
     * handle color carpark status
     */
    private fun handleShowCarParkStatus(carparkAmenity: BaseAmenity) {
        val hasAvailabilityCS = carparkAmenity.availabilityCSData != null
        viewBinding.layoutCarparkStatus.root.isVisible = carparkAmenity.availabilityCSData != null
        if (hasAvailabilityCS) {
//            Calculate last update time
            val drawableHeaderCpStatus =
                AppCompatResources.getDrawable(
                    activity,
                    R.drawable.bg_header_carpark_status
                )
            drawableHeaderCpStatus?.setTint(
                Color.parseColor(carparkAmenity.availabilityCSData?.themeColor ?: "#F26415")
            )
            viewBinding.layoutCarparkStatus.root.background = drawableHeaderCpStatus
            viewBinding.layoutCarparkStatus.icCarparkStatus.setImageResource(
                when (carparkAmenity.availablePercentImageBand) {
                    AmenityBand.TYPE0.type -> R.drawable.ic_cp_stt_full
                    AmenityBand.TYPE2.type -> R.drawable.ic_cp_stt_available
                    else -> R.drawable.ic_cp_stt_few
                }
            )
            viewBinding.layoutCarparkStatus.tvStatusCarpark.text = carparkAmenity.availabilityCSData?.title ?: ""
            viewBinding.layoutCarparkStatus.tvTimeUpdateCarparkStatus.text =
                "${carparkAmenity.availabilityCSData?.primaryDesc ?: activity.getString(com.breeze.customization.R.string.update_carpark_stt_time)} ${carparkAmenity.availabilityCSData?.generateDisplayedLastUpdate()}"
        }
    }

    private fun carParkOnClick(
        item: BaseAmenity,
        shouldRecenter: Boolean,
        carParkIconClick: (Boolean) -> Unit
    ) {
        carParkIconClick.invoke(false)

//        if (fromLandingPage) {
//            with(viewBinding.btnCalculateFee) {
//                val isInParkingTab = (context as? DashboardActivity)?.let { dashboardActivity ->
//                    ViewModelProvider(dashboardActivity).get<DashboardViewModel>().currentLandingModeFlow.value == LandingMode.PARKING
//                } ?: false
//                isVisible =
//                    !isInParkingTab && item.parkingType != Constants.PARKING_TYPE_SEASON_PARKING
//                            && item.parkingType != Constants.PARKING_TYPE_CUSTOMER_PARKING
//            }
//
//        }

        if (!shouldRecenter) {
            bringtoFont()
            return
        }

        BreezeMapZoomUtil.zoomViewToRange(mapboxMap, Point.fromLngLat(item.long!!, item.lat!!),
            toolTipZoomRange, EdgeInsets(
                200.0.dpToPx(),
                10.0.dpToPx(),
                10.0.dpToPx(),
                10.0.dpToPx()
            ), animationEndCB = {
                if (!viewBinding.llToolTip.isShown) {
                    viewBinding.llToolTip.visibility = View.VISIBLE
                }
                bringtoFont()
            }
        )
        bringtoFont()
    }

    private fun setTextColor(tv: TextView, color: Int) {
        tv.setTextColor(ContextCompat.getColor(activity, color))
    }

    override fun resetView() {
        viewBinding.llToolTip.visibility = View.INVISIBLE
    }

    override fun bringtoFont() {
        viewBinding.root.bringToFront()
    }

    override fun sendEventParkingISelected(idCarpark: String) {
        App.instance?.let { app ->
            val shareBusinessLogicPackage = app.shareBusinessLogicPackage
            shareBusinessLogicPackage.getRequestsModule().flatMap {
                val rnData = Arguments.createMap().apply {
                    putString("parkingID", idCarpark)
                }
                it.callRN(ShareBusinessLogicEvent.PARKING_OPEN_INFOR_CARPARK.eventName, rnData)
            }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe()
        }
    }

    override fun sendEventOpenCalculateFee(idCarpark: String) {
        /**
         * send rx bus to explore screen
         */
        if (ExploreMapStateManager.isOnExploreMapScreen || PlaceSaveDetailFragment.isInPlaceSaveScreen) {
            RxBus.publish(
                RxEvent.EventOpenCalculateFee(idCarpark)
            )
        } else {
            RxBus.publish(
                RxEvent.EventOpenCalculateFeeLandingPage(idCarpark)
            )
        }
    }
}
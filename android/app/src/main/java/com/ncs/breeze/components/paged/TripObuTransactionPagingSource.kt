package com.ncs.breeze.components.paged

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.breeze.model.api.response.TripObuTransactionItem
import com.ncs.breeze.common.remote.ApiHelper
import com.ncs.breeze.common.rxbus.RxBus
import com.ncs.breeze.common.rxbus.RxEvent

class TripObuTransactionPagingSource(
    private val apiHelper: ApiHelper,
    var startDate: String,
    var endDate: String,
    var obuName: String,
    var filter: String?
) : PagingSource<Int, TripObuTransactionItem>() {

    companion object {
        const val STARTING_PAGE_INDEX: Int = 1
        const val PAGE_SIZE = 10
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, TripObuTransactionItem> {

        return try {
            val nextPage = params.key ?: STARTING_PAGE_INDEX
            val tripsObuTransactionResponse =
                apiHelper.getTripObuTransactions(
                    nextPage,
                    PAGE_SIZE,
                    startDate,
                    endDate,
                    obuName,
                    filter
                )
            RxBus.publish(RxEvent.IsTripObuTransactionFeatureUsed((nextPage==1) && (tripsObuTransactionResponse.totalItems?:0 == 0)))
            LoadResult.Page(
                data = tripsObuTransactionResponse.transactions,
                prevKey = if (nextPage == 1) null else nextPage - 1,
                nextKey = if (nextPage < tripsObuTransactionResponse.totalPages!!)
                    tripsObuTransactionResponse.currentPage?.plus(1) else null
            )
        } catch (e: Exception) {
            LoadResult.Error(e)
        }
    }

    // The refresh key is used for subsequent refresh calls to PagingSource.load after the initial load
    override fun getRefreshKey(state: PagingState<Int, TripObuTransactionItem>): Int? {
        // We need to get the previous key (or next key if previous is null) of the page
        // that was closest to the most recently accessed index.
        // Anchor position is the most recently accessed index
        return state.anchorPosition?.let { anchorPosition ->
            state.closestPageToPosition(anchorPosition)?.prevKey?.plus(1)
                ?: state.closestPageToPosition(anchorPosition)?.nextKey?.minus(1)
        }
    }
}

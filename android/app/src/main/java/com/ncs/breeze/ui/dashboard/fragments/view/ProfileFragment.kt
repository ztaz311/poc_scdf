package com.ncs.breeze.ui.dashboard.fragments.view

import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import com.ncs.breeze.R
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.Event
import com.ncs.breeze.common.analytics.Screen
import com.ncs.breeze.common.appSync.AppSyncHelper
import com.ncs.breeze.common.event.ToCarRx
import com.ncs.breeze.common.extensions.android.getRNFragmentNavigationParams
import com.ncs.breeze.common.model.rx.AppEventLogout
import com.breeze.model.enums.AmplifyIdentityProviderType
import com.ncs.breeze.common.utils.BreezeUserPreferenceUtil
import com.breeze.model.constants.Constants
import com.breeze.model.constants.MODE_LOGIN_TYPE
import com.ncs.breeze.common.utils.DialogFactory

import com.ncs.breeze.common.utils.Variables
import com.ncs.breeze.databinding.FragmentProfileBinding
import com.ncs.breeze.ui.base.BaseFragment
import com.ncs.breeze.ui.dashboard.activity.DashboardActivity
import com.ncs.breeze.ui.dashboard.fragments.viewmodel.ProfileFragmentViewModel
import com.ncs.breeze.ui.rn.CustomReactFragment
import timber.log.Timber
import javax.inject.Inject

class ProfileFragment : BaseFragment<FragmentProfileBinding, ProfileFragmentViewModel>(),
    DashboardActivity.ProfileUserNameChangeListener {

    companion object {
        const val OPEN_EMAIL_FRAGMENT = "OPEN_EMAIL_FRAGMENT"
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val viewModel: ProfileFragmentViewModel by viewModels {
        viewModelFactory
    }

    @Inject
    lateinit var breezeUserPreferenceUtil: BreezeUserPreferenceUtil

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initClickActions()

        if (arguments?.getBoolean(OPEN_EMAIL_FRAGMENT, false) == true) {
            val otp = arguments?.getString(DashboardActivity.DEEPLINK_EMAIL_OTP)
            val bundle = Bundle()
            bundle.putString(DashboardActivity.DEEPLINK_EMAIL_OTP, otp)
            bundle.putBoolean(ChangeEmailFragment.IS_EMAIL_VERIFICATION, true)
            (requireActivity() as DashboardActivity).addFragment(
                ChangeEmailFragment(),
                bundle,
                Constants.TAGS.SETTINGS_TAG
            )
        }
    }

    fun onCarDetailsChanged() {
        kotlin.runCatching {
            activity?.runOnUiThread {
                initViews()
            }
        }
    }

    private fun initViews() {
        val isGuestMode = breezeUserPreferenceUtil.isGuestMode()
        val providerType = breezeUserPreferenceUtil.retrieveAmplifyIdentityProvider()
        val phoneNumber = breezeUserPreferenceUtil.retrieveUserPhoneNumber()
        val isSocialLogin =
            providerType == AmplifyIdentityProviderType.GOOGLE.type || providerType == AmplifyIdentityProviderType.FACEBOOK.type
        viewBinding.tvPhoneNumber.text = phoneNumber
        viewBinding.layoutPhoneNumber.visibility =
            if (isSocialLogin || isGuestMode) View.GONE else View.VISIBLE
        viewBinding.tvWarningCreateAccount.visibility =
            if (isGuestMode) View.VISIBLE else View.GONE

        viewBinding.layoutProvider.visibility =
            if (providerType == AmplifyIdentityProviderType.GOOGLE.type || providerType == AmplifyIdentityProviderType.FACEBOOK.type)
                View.VISIBLE else View.GONE
        viewBinding.imgProvider.setImageResource(
            when (providerType) {
                AmplifyIdentityProviderType.FACEBOOK.type -> R.drawable.fb_logo
                else -> R.drawable.google_logo
            }
        )
        viewBinding.tvProviderName.text = providerType

        viewBinding.tvUserName.text = breezeUserPreferenceUtil.retrieveUserName()

        viewBinding.viewCarDetails.fillData(
            iuNumber = breezeUserPreferenceUtil.retrieveUserCarIU(),
            carplateNumber = breezeUserPreferenceUtil.retrieveUserCarPlate()
        )

        viewBinding.logout.visibility = if (isGuestMode) View.GONE else View.VISIBLE
        viewBinding.layoutSignIn.visibility = if (isGuestMode) View.VISIBLE else View.GONE
    }

    private fun initClickActions() {
        viewBinding.viewCarDetails.setOnClickListener {
            showCarDetailsScreenRN()
        }
        viewBinding.layoutUsername.setOnClickListener {
            Analytics.logClickEvent(Event.ACCOUNT_NAME_CLICK, getScreenName())
            (requireActivity() as? DashboardActivity)?.addFragment(
                ChangeUserNameFragment(),
                null,
                Constants.TAGS.SETTINGS_TAG
            )
        }

        viewBinding.logout.setOnClickListener {
            Analytics.logClickEvent(Event.LOGOUT_CLICK, getScreenName())
            if (Variables.isNetworkConnected) {
                showDialogConfirmLogout()
            } else {
                DialogFactory.internetDialog(
                    activity,
                ) { _, _ -> }!!.show()
            }
        }

        viewBinding.tvCreateAccount.setOnClickListener {
            Analytics.logClickEvent(Event.ACCOUNT_CREATE_ACCOUNT_CLICK, getScreenName())
            if (Variables.isNetworkConnected) {
                activity?.let {
                    /**
                     * destroy app sync
                     */
                    activity?.applicationContext?.let {
                        AppSyncHelper.unSubscribeAll {
                            Handler(it.mainLooper).postDelayed({
                                (activity as? DashboardActivity)?.logoutApp {
                                    handleLogoutCompleted(MODE_LOGIN_TYPE.CREATE_ACCOUNT)
                                }
                            }, 1000)
                        }
                    }
                }
            } else {
                DialogFactory.internetDialog(
                    activity,
                ) { _, _ -> }!!.show()
            }

        }
        viewBinding.tvSignIn.setOnClickListener {
            Analytics.logClickEvent(Event.ACCOUNT_SIGNIN_CLICK, getScreenName())
            if (Variables.isNetworkConnected) {
                activity?.let {
                    /**
                     * destroy app sync
                     */
                    activity?.applicationContext?.let {
                        AppSyncHelper.unSubscribeAll {
                            Handler(it.mainLooper).postDelayed({
                                (activity as? DashboardActivity)?.logoutApp {
                                    handleLogoutCompleted(MODE_LOGIN_TYPE.SIGN_IN)
                                }
                            }, 1000)

                        }
                    }
                }
            } else {
                DialogFactory.internetDialog(
                    activity,
                ) { _, _ -> }!!.show()
            }
        }

        viewBinding.backButton.setOnClickListener {
            onBackPressed()
        }
    }

    private fun showDialogConfirmLogout() {
        activity?.let { it ->
            val builder: AlertDialog.Builder = AlertDialog.Builder(it)
            builder
                .setMessage(it.getString(R.string.confirm_logout))
                .setPositiveButton(
                    R.string.sign_out
                ) { dialog, _ ->
                    dialog.dismiss()
                    activity?.let {
                        /**
                         * destroy app sync
                         */
                        activity?.applicationContext?.let {
                            AppSyncHelper.unSubscribeAll {
                                Handler(it.mainLooper).postDelayed({
                                    (activity as? DashboardActivity)?.logoutApp {
                                        handleLogoutCompleted()
                                    }
                                }, 1000)

                            }
                        }
                    }
                }
                .setNegativeButton(R.string.cancel) { dialog, _ ->
                    dialog.dismiss()
                }
                .setCancelable(true)
            val alertDialog: AlertDialog? = builder?.create()

            alertDialog?.setOnShowListener {
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE)
                    .setTextColor(resources.getColor(R.color.themed_passion_pink))
            }

            alertDialog?.show()
        }
    }

    private fun handleLogoutCompleted(mode: Int = -1) {
        Timber.i("Signed out globally")

        /**
         * post event for logout
         */
        ToCarRx.postEventWithCacheLast(AppEventLogout())
        (activity as? DashboardActivity)?.openLoginScreen(mode)
    }

    private fun showCarDetailsScreenRN() {
        Analytics.logClickEvent(Event.ACCOUNT_CAR_DETAILS_CLICK, getScreenName())
        val navigationParams = requireActivity().getRNFragmentNavigationParams(
            sessionToken = (activity as? DashboardActivity)?.myServiceInterceptor?.getSessionToken(),
            fragmentTag = Constants.RN_CONSTANTS.CAR_DETAILS_SCREEN
        )
        val initialProperties = Bundle().apply {
            putString(Constants.RN_TO_SCREEN, Constants.RN_CONSTANTS.CAR_DETAILS_SCREEN)
            putBundle(Constants.RN_CONSTANTS.NAVIGATION_PARAMS, navigationParams)
        }
        val reactNativeFragment = CustomReactFragment.newInstance(
            componentName = Constants.RN_CONSTANTS.COMPONENT_NAME,
            launchOptions = initialProperties,
            cleanOnDestroyed = false
        )
        (requireActivity() as DashboardActivity).addFragment(
            reactNativeFragment,
            null,
            Constants.RN_CONSTANTS.CAR_DETAILS_SCREEN
        )
    }

    override fun inflateViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?,
        attachToContainer: Boolean
    ) = FragmentProfileBinding.inflate(inflater, container, attachToContainer)

    override fun getViewModelReference(): ProfileFragmentViewModel {
        return viewModel
    }

    override fun onResume() {
        super.onResume()
        (activity as? DashboardActivity?)?.setProfileUserNameChangeListener(this)
    }

    override fun onStop() {
        super.onStop()
        (activity as? DashboardActivity?)?.setProfileUserNameChangeListener(null)
    }

    override fun onPause() {
        super.onPause()
        (activity as? DashboardActivity?)?.setProfileUserNameChangeListener(null)
    }

    override fun refreshProfile() {
        initViews()
    }

    override fun getScreenName(): String {
        return if (::breezeUserPreferenceUtil.isInitialized && breezeUserPreferenceUtil.isGuestMode())
            Screen.SETTINGS_ACCOUNT_GUEST
        else
            Screen.SETTINGS_ACCOUNT
    }

}
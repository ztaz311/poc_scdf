package com.ncs.breeze.common.utils

import android.animation.Animator
import android.content.res.Resources
import android.location.Location
import android.util.DisplayMetrics
import com.mapbox.geojson.Point
import com.mapbox.geojson.Polygon
import com.mapbox.maps.CameraOptions
import com.mapbox.maps.EdgeInsets
import com.mapbox.maps.MapboxMap
import com.mapbox.maps.plugin.animation.Cancelable
import com.mapbox.maps.plugin.animation.MapAnimationOptions
import com.mapbox.maps.plugin.animation.easeTo
import com.mapbox.turf.TurfConstants
import com.mapbox.turf.TurfTransformation
import com.breeze.model.api.response.amenities.BaseAmenity
import com.breeze.model.constants.Constants
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.concurrent.ConcurrentHashMap
import kotlin.math.ln

object BreezeMapZoomUtil {

    private const val DISTANCE_OFFSET = 1.4
    private const val CIRCLE_STEPS = 4
    private val cancellableMap = ConcurrentHashMap<Int, Cancelable>()

    fun recenterMapToLocation(
        mapboxMap: MapboxMap,
        lat: Double,
        lng: Double,
        edgeInset: EdgeInsets,
        zoomRadius: Double,
        pitch: Double = 0.0,
        animationEndCB: () -> Unit = {}
    ) {
        val cameraOptionBuilder = CameraOptions.Builder()
            .center(Point.fromLngLat(lng, lat))
            .padding(edgeInset)

        cameraOptionBuilder.zoom(
            getZoomForMetersWide(
                mapboxMap,
                Point.fromLngLat(lng, lat),
                zoomRadius
            )
        )

        cameraOptionBuilder.pitch(pitch)
        cancellableMap.put(
            mapboxMap.hashCode(), mapboxMap.easeTo(
                cameraOptionBuilder.build(),
                generateMapAnimationOptions(mapboxMap, animationEndCB)
            )
        )
    }

    fun recenterMapToLocationOverviewNavigation(
        mapboxMap: MapboxMap,
        edgeInset: EdgeInsets,
        points: List<Point>,
        animationEndCB: () -> Unit = {}
    ) {
        cancellableMap[mapboxMap.hashCode()] = mapboxMap.easeTo(
            mapboxMap.cameraForGeometry(
                Polygon.fromLngLats(listOf(points)),
                padding = edgeInset,
                pitch = 0.0
            ),
            generateMapAnimationOptions(mapboxMap, animationEndCB)
        )
    }

    fun recenterMapToLocation(
        mapboxMap: MapboxMap,
        lat: Double,
        lng: Double,
        edgeInset: EdgeInsets,
        zoomLevel: Double = Constants.MAX_ZOOM_LEVEL,
        zoomRadius: Double? = null,
        animationEndCB: () -> Unit = {}
    ) {

        if (zoomRadius != null) {
            zoomViewToRange(
                mapboxMap, Point.fromLngLat(lng, lat),
                zoomRadius, edgeInset,
                animationEndCB
            )
        } else {
            val cameraOptionBuilder = CameraOptions.Builder()
                .center(Point.fromLngLat(lng, lat))
                .padding(edgeInset)
            if (zoomLevel > 0) {
                cameraOptionBuilder.zoom(zoomLevel)
            }
            cameraOptionBuilder.pitch(0.0)
            cancellableMap.put(
                mapboxMap.hashCode(), mapboxMap.easeTo(
                    cameraOptionBuilder.build(),
                    generateMapAnimationOptions(mapboxMap, animationEndCB)
                )
            )
        }
    }

    fun recenterMapToLocation(
        mapboxMap: MapboxMap,
        cameraOptions: CameraOptions,
        animationEndCB: () -> Unit = {}
    ) {
        cancellableMap.put(
            mapboxMap.hashCode(), mapboxMap.easeTo(
                cameraOptions,
                generateMapAnimationOptions(mapboxMap, animationEndCB)
            )
        )
    }


    fun recenterMapToOuterCircle(
        mapboxMap: MapboxMap,
        point: Point,
        radius: Double,
        edgeInset: EdgeInsets,
    ) {
        val cameraOptions =
            determineCameraOptionsForCircularRadius(mapboxMap, point, radius, edgeInset)
        cancellableMap.put(
            mapboxMap.hashCode(), mapboxMap.easeTo(
                cameraOptions,
                generateMapAnimationOptions(mapboxMap)
            )
        )
    }

    fun zoomCameraDependOnCircleIncludeCurrentLocation(
        mapboxMap: MapboxMap,
        point: Point,
        radius: Double,
        edgeInset: EdgeInsets,
        currentLocation: Location
    ) {
        val cameraOptions =
            determineCameraOptionsForCircularRadiusIncludeCurrentLocation(
                mapboxMap,
                point,
                radius,
                edgeInset,
                currentLocation
            )
        cancellableMap.put(
            mapboxMap.hashCode(), mapboxMap.easeTo(
                cameraOptions,
                generateMapAnimationOptions(mapboxMap)
            )
        )
    }

    fun recenterToPoint(
        mapboxMap: MapboxMap,
        point: Point,
        padding: EdgeInsets,
        animationEndCB: () -> Unit
    ) {
        val option: CameraOptions = CameraOptions.Builder()
            .center(point)
            .padding(padding)
            .build()
        cancellableMap.put(
            mapboxMap.hashCode(), mapboxMap.easeTo(
                option,
                generateMapAnimationOptions(mapboxMap, animationEndCB)
            )
        )
    }

    fun recenterToPoint(mapboxMap: MapboxMap, point: Point, animationEndCB: () -> Unit) {
        val option: CameraOptions = CameraOptions.Builder()
            .center(point)
            .build()
        cancellableMap.put(
            mapboxMap.hashCode(), mapboxMap.easeTo(
                option,
                generateMapAnimationOptions(mapboxMap, animationEndCB)
            )
        )
    }

    fun adjustExistingCameraPadding(mapboxMap: MapboxMap, padding: EdgeInsets) {
        val cancellable = cancellableMap[mapboxMap.hashCode()]
        cancellable?.let {
            CoroutineScope(Dispatchers.Default).launch {
                delay(200)
                withContext(Dispatchers.Main) {
                    adjustExistingCameraPadding(mapboxMap, padding)
                }
            }
            return@adjustExistingCameraPadding
        }
        updatePadding(padding, mapboxMap)
    }

    private fun updatePadding(
        padding: EdgeInsets,
        mapboxMap: MapboxMap
    ) {

        val option: CameraOptions = CameraOptions.Builder()
            .padding(padding)
            .center(mapboxMap.cameraState.center)
            .build()
        mapboxMap.easeTo(
            option,
            generateMapAnimationOptions()
        )
    }


    fun zoomViewToRange(
        mapboxMap: MapboxMap,
        point: Point,
        radius: Double,
        padding: EdgeInsets,
        animationEndCB: () -> Unit
    ) {
        val option: CameraOptions =
            determineCameraOptionsForHorizontalDistance(mapboxMap, point, radius, padding)
        cancellableMap.put(
            mapboxMap.hashCode(), mapboxMap.easeTo(
                option,
                generateMapAnimationOptions(mapboxMap, animationEndCB)
            )
        )
    }

    private fun generateMapAnimationOptions(
        mapboxMap: MapboxMap,
        animationEndCB: () -> Unit
    ) = MapAnimationOptions.mapAnimationOptions {
        duration(400L)
        animatorListener(object : Animator.AnimatorListener {
            var isCancelled = false
            override fun onAnimationStart(animation: Animator) {}

            override fun onAnimationEnd(animation: Animator) {
                cancellableMap.remove(mapboxMap.hashCode())
                if (!isCancelled) {
                    animationEndCB.invoke()
                }
            }

            override fun onAnimationCancel(animation: Animator) {
                cancellableMap.remove(mapboxMap.hashCode())
                isCancelled = true
            }

            override fun onAnimationRepeat(animation: Animator) {}
        })
    }

    private fun generateMapAnimationOptions(mapboxMap: MapboxMap) =
        MapAnimationOptions.mapAnimationOptions {
            duration(400L)
            animatorListener(object : Animator.AnimatorListener {
                override fun onAnimationStart(animation: Animator) {}

                override fun onAnimationEnd(animation: Animator) {
                    cancellableMap.remove(mapboxMap.hashCode())
                }

                override fun onAnimationCancel(animation: Animator) {
                    cancellableMap.remove(mapboxMap.hashCode())
                }

                override fun onAnimationRepeat(animation: Animator) {}

            })
        }

    private fun generateMapAnimationOptions() =
        MapAnimationOptions.mapAnimationOptions {
            duration(400L)
            animatorListener(object : Animator.AnimatorListener {
                override fun onAnimationStart(animation: Animator) {}

                override fun onAnimationEnd(animation: Animator) {}

                override fun onAnimationCancel(animation: Animator) {}

                override fun onAnimationRepeat(animation: Animator) {}

            })
        }

    /**
     * Alternate logic to determine zoom
     * Performance is lower
     */
    private fun determineCameraOptionsForCircularRadius(
        mapboxMap: MapboxMap,
        point: Point,
        radius: Double,
        padding: EdgeInsets
    ): CameraOptions {
        return mapboxMap.cameraForCoordinates(
            TurfTransformation.circle(point, radius, CIRCLE_STEPS, TurfConstants.UNIT_METERS)
                .coordinates()[0],
            padding,
            null,
            null
        )
    }

    private fun determineCameraOptionsForCircularRadiusIncludeCurrentLocation(
        mapboxMap: MapboxMap,
        point: Point,
        radius: Double,
        padding: EdgeInsets,
        currentLocation: Location
    ): CameraOptions {
        val currentLocationPoint =
            Point.fromLngLat(currentLocation.longitude, currentLocation.latitude)
        val listPoint =
            TurfTransformation.circle(point, radius, CIRCLE_STEPS, TurfConstants.UNIT_METERS)
                .coordinates()[0]
        listPoint.add(currentLocationPoint)
        return mapboxMap.cameraForCoordinates(
            listPoint,
            padding,
            null,
            null
        )
    }


    fun handleZoomWrapAllPOI(
        mapboxMap: MapboxMap,
        listContentDetail: List<BaseAmenity>,
        currentLocation: Location?,
        padding: EdgeInsets
    ) {
        val listCodinate = ArrayList<Point>()
        listContentDetail.forEach {
            listCodinate.add(Point.fromLngLat(it.long!!, it.lat!!))
        }
        currentLocation?.let {
            listCodinate.add(Point.fromLngLat(it.longitude, it.latitude))
        }
        var cameraPosition = mapboxMap.cameraForCoordinates(listCodinate, padding)
        mapboxMap.setCamera(cameraPosition)
    }


    /**
     * Camera options with zoom level calculated dynamically
     */
    private fun determineCameraOptionsForHorizontalDistance(
        mapboxMap: MapboxMap,
        point: Point,
        radius: Double,
        padding: EdgeInsets
    ): CameraOptions {
        val zoom = getZoomForMetersWide(mapboxMap, point, radius)
        return CameraOptions.Builder()
            .center(point)
            .zoom(zoom)
            .padding(padding)
            .build()
    }


    /**
     * Calculates zoom level based on radius and screen width
     */
    private fun getZoomForMetersWide(
        mapboxMap: MapboxMap,
        point: Point,
        desiredMeters: Double
    ): Double {
        // scaling the radius to avoid edges being out of view
        val scaledRadius = DISTANCE_OFFSET * desiredMeters
        val displayMetrics: DisplayMetrics = Resources.getSystem().displayMetrics
        val size = mapboxMap.getSize()
        var width: Float = if (size.width > size.height) {
            // handling landscape orientation
            size.height * (size.height / size.width) / displayMetrics.density
        } else {
            size.width / displayMetrics.density
        }
        // https://docs.mapbox.com/help/glossary/zoom-level/
        // mapbox tile pixel is 512
        // metersParallelToEquatorAtLat for zoom 0 *512 will return the length of line
        val metersParallelToEquatorAtLatPerPixel =
            mapboxMap.getMetersPerPixelAtLatitude(point.latitude(), 0.0)
        // since metersParallelToEquatorAtLat is divided by factor of 2 per zoom level
        // taking logbase2 of metersParallelToEquatorAtLat divided by desired distance . (converting logbase2 to ln)
        val arg: Double =
            metersParallelToEquatorAtLatPerPixel * width / (scaledRadius * 2.0)
        return ln(arg) / ln(2.0)
    }

}
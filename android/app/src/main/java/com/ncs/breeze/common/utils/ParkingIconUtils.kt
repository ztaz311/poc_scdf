package com.ncs.breeze.common.utils

import com.ncs.breeze.R
import com.breeze.model.enums.AmenityBand
import com.breeze.model.api.response.amenities.BaseAmenity
import com.ncs.breeze.common.utils.ParkingIconUtils.ParkingMarkerConstants.MARKER_DESTINATION
import com.ncs.breeze.common.utils.ParkingIconUtils.ParkingMarkerConstants.MARKER_NONDESTINATION
import com.ncs.breeze.common.utils.ParkingIconUtils.ParkingMarkerConstants.MARKER_NORATE
import com.ncs.breeze.common.utils.ParkingIconUtils.ParkingMarkerConstants.MARKER_NOTSELECTED
import com.ncs.breeze.common.utils.ParkingIconUtils.ParkingMarkerConstants.MARKER_SEASON
import com.ncs.breeze.common.utils.ParkingIconUtils.ParkingMarkerConstants.MARKER_SELECTED
import com.ncs.breeze.common.utils.ParkingIconUtils.ParkingMarkerConstants.MARKER_TYPE0
import com.ncs.breeze.common.utils.ParkingIconUtils.ParkingMarkerConstants.MARKER_TYPE1
import com.ncs.breeze.common.utils.ParkingIconUtils.ParkingMarkerConstants.MARKER_TYPE2
import com.ncs.breeze.common.utils.ParkingIconUtils.ParkingMarkerConstants.PARKINGMARKER
import com.ncs.breeze.common.utils.ParkingIconUtils.ParkingMarkerConstants.VOUCHER

object ParkingIconUtils {

    enum class ImageIconType(val type: String) {
        UNSELECTED("unselected"),
        SELECTED("selected"),
        SELECTED_NIGHT("selected-night"),
        UNSELECTED_NIGHT("unselected-night"),
    }

    object ParkingMarkerConstants {
        val PARKINGMARKER = "PARKINGMARKER"
        val MARKER_SEASON = "SEASON"
        val MARKER_TYPE0 = "TYPE0"
        val MARKER_TYPE1 = "TYPE1"
        val MARKER_TYPE2 = "TYPE2"
        val MARKER_NORATE = "NORATE"
        val MARKER_SELECTED = "SELECTED"
        val MARKER_NOTSELECTED = "NOTSELECTED"
        val MARKER_DESTINATION = "DESTINATION"
        val MARKER_NONDESTINATION = "NONDESTINATION"
        val VOUCHER = "VOUCHER"
    }

    enum class ParkingMarkerImageIconType(val type: String) {
        PARKINGMARKER_SEASON_DESTINATION_SELECTED("${PARKINGMARKER}_${MARKER_SEASON}_${MARKER_DESTINATION}_${MARKER_SELECTED}"),
        PARKINGMARKER_SEASON_DESTINATION_NOTSELECTED("${PARKINGMARKER}_${MARKER_SEASON}_${MARKER_DESTINATION}_${MARKER_NOTSELECTED}"),
        PARKINGMARKER_SEASON_NONDESTINATION_SELECTED("${PARKINGMARKER}_${MARKER_SEASON}_${MARKER_NONDESTINATION}_${MARKER_SELECTED}"),
        PARKINGMARKER_SEASON_NONDESTINATION_NOTSELECTED("${PARKINGMARKER}_${MARKER_SEASON}_${MARKER_NONDESTINATION}_${MARKER_NOTSELECTED}"),
        PARKINGMARKER_TYPE0_DESTINATION_SELECTED("${PARKINGMARKER}_${MARKER_TYPE0}_${MARKER_DESTINATION}_${MARKER_SELECTED}"),
        PARKINGMARKER_TYPE0_DESTINATION_NOTSELECTED("${PARKINGMARKER}_${MARKER_TYPE0}_${MARKER_DESTINATION}_${MARKER_NOTSELECTED}"),
        PARKINGMARKER_TYPE0_NONDESTINATION_SELECTED("${PARKINGMARKER}_${MARKER_TYPE0}_${MARKER_NONDESTINATION}_${MARKER_SELECTED}"),
        PARKINGMARKER_TYPE0_NONDESTINATION_NOTSELECTED("${PARKINGMARKER}_${MARKER_TYPE0}_${MARKER_NONDESTINATION}_${MARKER_NOTSELECTED}"),
        PARKINGMARKER_TYPE1_DESTINATION_SELECTED("${PARKINGMARKER}_${MARKER_TYPE1}_${MARKER_DESTINATION}_${MARKER_SELECTED}"),
        PARKINGMARKER_TYPE1_DESTINATION_NOTSELECTED("${PARKINGMARKER}_${MARKER_TYPE1}_${MARKER_DESTINATION}_${MARKER_NOTSELECTED}"),
        PARKINGMARKER_TYPE1_NONDESTINATION_SELECTED("${PARKINGMARKER}_${MARKER_TYPE1}_${MARKER_NONDESTINATION}_${MARKER_SELECTED}"),
        PARKINGMARKER_TYPE1_NONDESTINATION_NOTSELECTED("${PARKINGMARKER}_${MARKER_TYPE1}_${MARKER_NONDESTINATION}_${MARKER_NOTSELECTED}"),
        PARKINGMARKER_TYPE2_DESTINATION_SELECTED("${PARKINGMARKER}_${MARKER_TYPE2}_${MARKER_DESTINATION}_${MARKER_SELECTED}"),
        PARKINGMARKER_TYPE2_DESTINATION_NOTSELECTED("${PARKINGMARKER}_${MARKER_TYPE2}_${MARKER_DESTINATION}_${MARKER_NOTSELECTED}"),
        PARKINGMARKER_TYPE2_NONDESTINATION_SELECTED("${PARKINGMARKER}_${MARKER_TYPE2}_${MARKER_NONDESTINATION}_${MARKER_SELECTED}"),
        PARKINGMARKER_TYPE2_NONDESTINATION_NOTSELECTED("${PARKINGMARKER}_${MARKER_TYPE2}_${MARKER_NONDESTINATION}_${MARKER_NOTSELECTED}"),
        PARKINGMARKER_NORATE_DESTINATION_SELECTED("${PARKINGMARKER}_${MARKER_NORATE}_${MARKER_DESTINATION}_${MARKER_SELECTED}"),
        PARKINGMARKER_NORATE_DESTINATION_NOTSELECTED("${PARKINGMARKER}_${MARKER_NORATE}_${MARKER_DESTINATION}_${MARKER_NOTSELECTED}"),
        PARKINGMARKER_NORATE_NONDESTINATION_SELECTED("${PARKINGMARKER}_${MARKER_NORATE}_${MARKER_NONDESTINATION}_${MARKER_SELECTED}"),
        PARKINGMARKER_NORATE_NONDESTINATION_NOTSELECTED("${PARKINGMARKER}_${MARKER_NORATE}_${MARKER_NONDESTINATION}_${MARKER_NOTSELECTED}"),


        PARKINGMARKER_VOUCHER_SEASON_DESTINATION_SELECTED("${PARKINGMARKER}_${VOUCHER}_${MARKER_SEASON}_${MARKER_DESTINATION}_${MARKER_SELECTED}"),
        PARKINGMARKER_VOUCHER_SEASON_DESTINATION_NOTSELECTED("${PARKINGMARKER}_${VOUCHER}_${MARKER_SEASON}_${MARKER_DESTINATION}_${MARKER_NOTSELECTED}"),
        PARKINGMARKER_VOUCHER_SEASON_NONDESTINATION_SELECTED("${PARKINGMARKER}_${VOUCHER}_${MARKER_SEASON}_${MARKER_NONDESTINATION}_${MARKER_SELECTED}"),
        PARKINGMARKER_VOUCHER_SEASON_NONDESTINATION_NOTSELECTED("${PARKINGMARKER}_${VOUCHER}_${MARKER_SEASON}_${MARKER_NONDESTINATION}_${MARKER_NOTSELECTED}"),
        PARKINGMARKER_VOUCHER_TYPE0_DESTINATION_SELECTED("${PARKINGMARKER}_${VOUCHER}_${MARKER_TYPE0}_${MARKER_DESTINATION}_${MARKER_SELECTED}"),
        PARKINGMARKER_VOUCHER_TYPE0_DESTINATION_NOTSELECTED("${PARKINGMARKER}_${VOUCHER}_${MARKER_TYPE0}_${MARKER_DESTINATION}_${MARKER_NOTSELECTED}"),
        PARKINGMARKER_VOUCHER_TYPE0_NONDESTINATION_SELECTED("${PARKINGMARKER}_${VOUCHER}_${MARKER_TYPE0}_${MARKER_NONDESTINATION}_${MARKER_SELECTED}"),
        PARKINGMARKER_VOUCHER_TYPE0_NONDESTINATION_NOTSELECTED("${PARKINGMARKER}_${VOUCHER}_${MARKER_TYPE0}_${MARKER_NONDESTINATION}_${MARKER_NOTSELECTED}"),
        PARKINGMARKER_VOUCHER_TYPE1_DESTINATION_SELECTED("${PARKINGMARKER}_${VOUCHER}_${MARKER_TYPE1}_${MARKER_DESTINATION}_${MARKER_SELECTED}"),
        PARKINGMARKER_VOUCHER_TYPE1_DESTINATION_NOTSELECTED("${PARKINGMARKER}_${VOUCHER}_${MARKER_TYPE1}_${MARKER_DESTINATION}_${MARKER_NOTSELECTED}"),
        PARKINGMARKER_VOUCHER_TYPE1_NONDESTINATION_SELECTED("${PARKINGMARKER}_${VOUCHER}_${MARKER_TYPE1}_${MARKER_NONDESTINATION}_${MARKER_SELECTED}"),
        PARKINGMARKER_VOUCHER_TYPE1_NONDESTINATION_NOTSELECTED("${PARKINGMARKER}_${VOUCHER}_${MARKER_TYPE1}_${MARKER_NONDESTINATION}_${MARKER_NOTSELECTED}"),
        PARKINGMARKER_VOUCHER_TYPE2_DESTINATION_SELECTED("${PARKINGMARKER}_${VOUCHER}_${MARKER_TYPE2}_${MARKER_DESTINATION}_${MARKER_SELECTED}"),
        PARKINGMARKER_VOUCHER_TYPE2_DESTINATION_NOTSELECTED("${PARKINGMARKER}_${VOUCHER}_${MARKER_TYPE2}_${MARKER_DESTINATION}_${MARKER_NOTSELECTED}"),
        PARKINGMARKER_VOUCHER_TYPE2_NONDESTINATION_SELECTED("${PARKINGMARKER}_${VOUCHER}_${MARKER_TYPE2}_${MARKER_NONDESTINATION}_${MARKER_SELECTED}"),
        PARKINGMARKER_VOUCHER_TYPE2_NONDESTINATION_NOTSELECTED("${PARKINGMARKER}_${VOUCHER}_${MARKER_TYPE2}_${MARKER_NONDESTINATION}_${MARKER_NOTSELECTED}"),
        PARKINGMARKER_VOUCHER_NORATE_DESTINATION_SELECTED("${PARKINGMARKER}_${VOUCHER}_${MARKER_NORATE}_${MARKER_DESTINATION}_${MARKER_SELECTED}"),
        PARKINGMARKER_VOUCHER_NORATE_DESTINATION_NOTSELECTED("${PARKINGMARKER}_${VOUCHER}_${MARKER_NORATE}_${MARKER_DESTINATION}_${MARKER_NOTSELECTED}"),
        PARKINGMARKER_VOUCHER_NORATE_NONDESTINATION_SELECTED("${PARKINGMARKER}_${VOUCHER}_${MARKER_NORATE}_${MARKER_NONDESTINATION}_${MARKER_SELECTED}"),
        PARKINGMARKER_VOUCHER_NORATE_NONDESTINATION_NOTSELECTED("${PARKINGMARKER}_${VOUCHER}_${MARKER_NORATE}_${MARKER_NONDESTINATION}_${MARKER_NOTSELECTED}"),


    }

    fun getParkingMarkerTypeString(
        it: BaseAmenity,
    ): String {
        if (it.isSeasonParking()) {
            return MARKER_SEASON
        }
        if (it.hasRatesInfo ?: false == false && it.availablePercent == -1.0) {
            return MARKER_NORATE
        }
        when (it.availablePercentImageBand) {
            AmenityBand.TYPE0.type -> return MARKER_TYPE0
            AmenityBand.TYPE1.type -> return MARKER_TYPE1
            else -> return MARKER_TYPE2
        }
    }

    fun getCarParkIconForNavigation(
        it: BaseAmenity,
        isDestination: Boolean
    ): Int {
        if (isDestination) {
            if (it.isSeasonParking()) {
                if (it.hasVouchers) {
                    return R.drawable.ic_parkingmarker_voucher_norate_destination
                }
                return R.drawable.ic_parkingmarker_season_destination_selected
            }
            if (it.hasRatesInfo != true && it.availablePercent == -1.0) {
                if (it.hasVouchers) {
                    return R.drawable.ic_parkingmarker_voucher_norate_destination
                }
                return R.drawable.ic_parkingmarker_norate_destination_selected
            }
            when (it.availablePercentImageBand) {
                AmenityBand.TYPE0.type -> {
                    if (it.hasVouchers) {
                        return R.drawable.ic_parkingmarker_voucher_typezero_destination
                    }
                    return R.drawable.ic_parkingmarker_typezero_destination_selected
                }
                AmenityBand.TYPE1.type -> {
                    if (it.hasVouchers) {
                        return R.drawable.ic_parkingmarker_voucher_typeone_destination
                    }
                    return R.drawable.ic_parkingmarker_typeone_destination_selected
                }
                AmenityBand.TYPE2.type -> {
                    if (it.hasVouchers) {
                        return R.drawable.ic_parkingmarker_voucher_typetwo_destination
                    }
                    return R.drawable.ic_parkingmarker_typetwo_destination_selected
                }
            }
        } else {
            if (it.isSeasonParking()) {
                if (it.hasVouchers) {
                    return R.drawable.ic_parkingmarker_voucher_norate_nondestination_selected
                }
                return R.drawable.ic_parkingmarker_season_nondestination_selected
            }
            if (it.hasRatesInfo ?: false == false && it.availablePercent == -1.0) {
                if (it.hasVouchers) {
                    return R.drawable.ic_parkingmarker_voucher_norate_nondestination_selected
                }
                return R.drawable.ic_parkingmarker_norate_nondestination_selected
            }
            when (it.availablePercentImageBand) {
                AmenityBand.TYPE0.type -> {
                    if (it.hasVouchers) {
                        return R.drawable.ic_parkingmarker_voucher_typezero_nondestination_selected
                    }
                    return R.drawable.ic_parkingmarker_type0_nondestination_selected
                }
                AmenityBand.TYPE1.type -> {
                    if (it.hasVouchers) {
                        return R.drawable.ic_parkingmarker_voucher_typeone_nondestination_selected
                    }
                    return R.drawable.ic_parkingmarker_type1_nondestination_selected
                }
                AmenityBand.TYPE2.type -> {
                    if (it.hasVouchers) {
                        return R.drawable.ic_parkingmarker_voucher_typetwo_nondestination_selected
                    }
                    return R.drawable.ic_parkingmarker_type2_nondestination_selected
                }
            }
        }
        if (it.hasVouchers) {
            return R.drawable.ic_parkingmarker_voucher_norate_destination
        }
        return R.drawable.destination_puck
    }

}
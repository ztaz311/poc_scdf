package com.ncs.breeze.di.module.fragment

import com.ncs.breeze.ui.dashboard.fragments.view.ChangePasswordFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class ChangePasswordModule {
    @ContributesAndroidInjector
    abstract fun contributeChangePasswordFragment(): ChangePasswordFragment
}
package com.ncs.breeze.components.marker.markerview2.carpark

import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import com.breeze.model.api.response.AmenitiesPreference
import com.breeze.model.api.response.amenities.BaseAmenity
import com.breeze.model.constants.AmenityType
import com.facebook.react.bridge.Arguments
import com.google.android.gms.maps.model.LatLng
import com.mapbox.maps.MapboxMap
import com.mapbox.navigation.core.lifecycle.MapboxNavigationApp
import com.mapbox.navigation.core.trip.session.TripSessionState
import com.ncs.breeze.common.extensions.android.getShareBusinessLogicPackage
import com.ncs.breeze.common.utils.BreezeMapDataHolder
import com.ncs.breeze.databinding.SimpleCarparkMarkerTooltipBinding
import com.ncs.breeze.reactnative.nativemodule.ShareBusinessLogicEvent
import com.ncs.breeze.ui.base.BaseActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


open class SimpleCarParkMarkerView constructor(
    latLng: LatLng,
    mapboxMap: MapboxMap,
    activity: Activity,
    itemId: String? = null
) : BaseCarParkMarkerView(mapboxMap, activity, itemId) {
    private var viewBinding: SimpleCarparkMarkerTooltipBinding

    private var objectConfigUiAmenities: AmenitiesPreference? = null
    protected var carParkToolTipManager: SimpleCarParkToolTipManager? = null
    protected var ll_tool_tip: View? = null
    var currentDataMarker: BaseAmenity? = null
    var currentState = 0

    init {
        zoomLevel = 16.0
        mLatLng = latLng

        val inflater = activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        viewBinding = SimpleCarparkMarkerTooltipBinding.inflate(inflater)
        ll_tool_tip = viewBinding.llToolTip
        carParkToolTipManager =
            SimpleCarParkToolTipManager(viewBinding, mapboxMap, activity, true)
        mViewMarker = viewBinding.root
    }

    fun clickToopTip(
        item: BaseAmenity,
        shouldRecenter: Boolean,
        navigationButtonClick: (BaseAmenity, BaseActivity.EventSource) -> Unit,
        moreInfoButtonClick: (BaseAmenity) -> Unit,
        carParkIconClick: (Boolean) -> Unit,
        screenName: String
    ) {
        currentDataMarker = item
        objectConfigUiAmenities = BreezeMapDataHolder.amenitiesPreferenceHashmap[item.amenityType]

        viewBinding.buttonShare.isVisible =
            MapboxNavigationApp.current()?.getTripSessionState() != TripSessionState.STARTED
        viewBinding.buttonShare.setOnClickListener {
            shareAmenity(item)
        }
        viewBinding.buttonInvite.isVisible =
            MapboxNavigationApp.current()?.getTripSessionState() != TripSessionState.STARTED
        viewBinding.buttonInvite.setOnClickListener {
            inviteAmenity(item)
        }

        carParkToolTipManager?.clickToolTip(
            item,
            shouldRecenter,
            navigationButtonClick,
            moreInfoButtonClick,
            carParkIconClick,
            screenName
        )
        updateTooltipPosition()
    }

    private fun createShareLocationData(item: BaseAmenity) = Arguments.fromBundle(
        bundleOf(
            "address1" to if (item.amenityType == AmenityType.CARPARK) item.name else item.address,
            "latitude" to (item.lat?.toString() ?: ""),
            "longitude" to (item.long?.toString() ?: ""),
            "amenityId" to item.id,
            "amenityType" to item.amenityType,
            "name" to item.name,
        )
    )

    private fun shareAmenity(item: BaseAmenity) {
        viewBinding.root.context.getShareBusinessLogicPackage()?.getRequestsModule()?.flatMap {
            it.callRN(
                ShareBusinessLogicEvent.SHARE_LOCATION_FROM_NATIVE.eventName,
                createShareLocationData(item)
            )
        }?.subscribeOn(Schedulers.io())?.observeOn(AndroidSchedulers.mainThread())?.subscribe()
    }

    private fun inviteAmenity(item: BaseAmenity) {
        viewBinding.root.context.getShareBusinessLogicPackage()?.getRequestsModule()?.flatMap {
            it.callRN(
                ShareBusinessLogicEvent.LOCATION_INVITE_FROM_NATIVE.eventName,
                createShareLocationData(item)
            )
        }?.subscribeOn(Schedulers.io())?.observeOn(AndroidSchedulers.mainThread())?.subscribe()
    }

    override fun resetView() {
        carParkToolTipManager?.resetView()
    }

    override fun showMarker() {
        super.showMarker()
        viewBinding.root.visibility = View.VISIBLE

    }

    override fun hideMarker() {
        super.hideMarker()
        viewBinding.root.visibility = View.INVISIBLE
    }


    override fun hideAllToolTips() {
        super.hideAllToolTips()
        if (ll_tool_tip?.isShown == true) {
            ll_tool_tip?.visibility = View.INVISIBLE
        }
    }
}
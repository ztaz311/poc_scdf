package com.ncs.breeze.customview

import android.content.Context
import android.os.Handler
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.ImageView
import android.widget.RelativeLayout
import androidx.constraintlayout.widget.Group
import com.ncs.breeze.common.utils.eta.ETAEngine
import com.ncs.breeze.R


/**
 * for eta custom view
 */
class ETACustomView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : RelativeLayout(context, attrs, defStyle) {

    var mContext: Context? = null
    var imgAnimation2: ImageView? = null
    var btnTempRe: ImageView? = null
    var imgAnimation1: ImageView? = null
    var ivEtaStatus: ImageView? = null
    var rlETA: Group? = null

    private var handlerAnimation = Handler()

    private var runnable = object : Runnable {
        override fun run() {

            imgAnimation1?.apply {
                animate().scaleX(1.9f).scaleY(1.9f).alpha(0f).setDuration(1000)
                    .withEndAction {
                        scaleX = 1f
                        scaleY = 1f
                        alpha = 1f
                    }
            }

            imgAnimation2?.apply {
                animate().scaleX(1.9f).scaleY(1.9f).alpha(0f).setDuration(700)
                    .withEndAction {
                        scaleX = 1f
                        scaleY = 1f
                        alpha = 1f
                    }
            }
            handlerAnimation.postDelayed(this, 1500)
        }
    }

    init {
        LayoutInflater.from(context).inflate(R.layout.eta_custom_view, this, true)
        mContext = context
        initializeView()
    }

    private fun initializeView() {
        imgAnimation2 = findViewById(R.id.imgAnimation2)
        btnTempRe = findViewById(R.id.btn_temp_re)
        imgAnimation1 = findViewById(R.id.imgAnimation1)
        ivEtaStatus = findViewById(R.id.iv_eta_status)
        rlETA = findViewById(R.id.rlETA)
        clipChildren = false
    }

    fun updateUIOnlyForETA() {
        if (ETAEngine.getInstance()!!.isETARunning()) {
            ivEtaStatus?.setImageResource(R.drawable.eta_button_play)
            runnable.run()
            ivEtaStatus?.setOnClickListener {
                ETAEngine.getInstance()!!.toggleLiveLocation()
            }
        } else if (ETAEngine.getInstance()!!.isETAPause()) {
            ivEtaStatus?.setImageResource(R.drawable.eta_button_pause)
            handlerAnimation.removeCallbacks(runnable)
            ivEtaStatus?.setOnClickListener {
                ETAEngine.getInstance()!!.toggleLiveLocation()
            }
        } else if (ETAEngine.getInstance()!!.isETADisabled()) {
            ivEtaStatus?.setImageResource(R.drawable.eta_button_not_enable)
        }
    }

    fun setVisibleETA() {
        ivEtaStatus?.setOnClickListener {
            ETAEngine.getInstance()!!.toggleLiveLocation()
        }
    }


}
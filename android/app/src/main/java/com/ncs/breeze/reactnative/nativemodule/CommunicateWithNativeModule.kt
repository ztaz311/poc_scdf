package com.ncs.breeze.reactnative.nativemodule

import android.app.Activity
import android.content.Intent
import com.breeze.model.api.response.AllSettingResponse
import com.breeze.model.api.response.PlaceDetailsResponse
import com.breeze.model.constants.Constants
import com.breeze.model.enums.AndroidScreen
import com.facebook.react.bridge.Arguments
import com.facebook.react.bridge.Callback
import com.facebook.react.bridge.Promise
import com.facebook.react.bridge.ReactApplicationContext
import com.facebook.react.bridge.ReactContextBaseJavaModule
import com.facebook.react.bridge.ReactMethod
import com.facebook.react.bridge.ReadableMap
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.instabug.library.Instabug
import com.ncs.breeze.App
import com.ncs.breeze.R
import com.ncs.breeze.car.breeze.utils.BreezeCarUtil
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.Event
import com.ncs.breeze.common.constant.RNScreen
import com.ncs.breeze.common.constant.dashboard.LandingMode
import com.ncs.breeze.common.extensions.android.getApp
import com.ncs.breeze.common.extensions.android.getUserDataPreference
import com.ncs.breeze.common.rxbus.RxBus
import com.ncs.breeze.common.rxbus.RxEvent
import com.ncs.breeze.common.utils.BreezeUserPreferenceUtil
import com.ncs.breeze.common.utils.LocationBreezeManager
import com.ncs.breeze.common.utils.MapUtil
import com.ncs.breeze.common.utils.OBUStripStateManager
import com.ncs.breeze.ui.base.BaseActivity
import com.ncs.breeze.ui.dashboard.AdjustableBottomSheet
import com.ncs.breeze.ui.dashboard.activity.DashboardActivity
import com.ncs.breeze.ui.dashboard.fragments.exploremap.ExploreMapsFragment
import com.ncs.breeze.ui.dashboard.fragments.obulite.OBULiteFragment
import com.ncs.breeze.ui.dashboard.fragments.placesave.PlaceSaveDetailFragment
import com.ncs.breeze.ui.dashboard.fragments.view.ProfileFragment
import com.ncs.breeze.ui.dashboard.fragments.view.RoutePlanningFragment
import com.ncs.breeze.ui.dashboard.manager.DashboardMapStateManager
import com.ncs.breeze.ui.login.activity.LoginActivity
import com.ncs.breeze.ui.navigation.TurnByTurnNavigationActivity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber

class CommunicateWithNativeModule internal constructor(
    val context: ReactApplicationContext,
    val breezeUserPreferenceUtil: BreezeUserPreferenceUtil
) :
    ReactContextBaseJavaModule(context) {

    val currentTopActivity: Activity?
        get() {
            return (context.applicationContext as App).getCurrentActivity() ?: currentActivity
        }


    @ReactMethod
    fun closeOBULiteScreen() {
        Timber.d("closeOBULiteScreen")
        (currentTopActivity as? DashboardActivity)?.run {
            closeOBULiteScreen()
        }
    }

    @ReactMethod
    fun dismissZoneDetail() {
        (currentTopActivity as? DashboardActivity)?.run {
            val currentFragment = supportFragmentManager.findFragmentById(R.id.frame_container)
            if (currentFragment != null && currentFragment is ExploreMapsFragment) {
                currentFragment.backTBRAmmenitiesDetail()
            }
        }
    }

    @ReactMethod
    fun backToNative(data: ReadableMap?) {
        Timber.d("Event triggered [backToNative]")
        if (data == null) {
            Timber.d("Current activity finished")
            currentTopActivity!!.finish()
            return
        }
        // Update logic here to fit your need

        lateinit var openAs: String;
        data.getString("openAs")?.let {
            openAs = it;
        }
        // Activity
        if (openAs == null || openAs == "Activity") {
            Timber.d("Current activity finished")
            currentTopActivity!!.finish()
            return
        }
        // Fragment => handle your logic here
        data.getString("fragmentId")?.let { fragmentTag ->
            (currentTopActivity as? BaseActivity<*, *>)?.run {
                Timber.d("Closeing  fragment from RN: $fragmentTag")
                runOnUiThread {
                    supportFragmentManager.findFragmentByTag(fragmentTag)?.let { fragment ->
                        supportFragmentManager.popBackStackImmediate()
                    }
                    if (fragmentTag == Constants.CARPARK_MOREINFO.TO_PARKING_MOREINFO_SCREEN) {
                        val currentFragment =
                            supportFragmentManager.findFragmentById(R.id.frame_container)
                        if (currentFragment != null) {
                            if (currentFragment is ExploreMapsFragment) {
                                currentFragment.closeParkingCalculator()
                            } else if (currentFragment is PlaceSaveDetailFragment) {
                                currentFragment.closeParkingCalculator()
                            }
                        }
                    }
                    if (fragmentTag == RNScreen.PARKING_CALCULATOR) {
                        runOnUiThread {
                            val currentFragment =
                                supportFragmentManager.findFragmentById(R.id.frame_container)
                            if (currentFragment != null && currentFragment is ExploreMapsFragment) {
                                currentFragment.closeParkingCalculator()
                            } else if (currentFragment != null && currentFragment is PlaceSaveDetailFragment) {
                                currentFragment.closeParkingCalculator()
                            } else {
                                (supportFragmentManager.findFragmentByTag(Constants.TAGS.ROUTE_PLANNING) as? RoutePlanningFragment)?.closeParkingCalculator()
                            }
                        }
                    }
                }

            }
        }
    }


    @ReactMethod
    fun closeProgressTrackingScreen() {
        (currentTopActivity as? BaseActivity<*, *>)?.run {
            runOnUiThread {
                val currentFragment = supportFragmentManager.findFragmentById(R.id.frame_container)
                if (currentFragment != null && currentFragment is ExploreMapsFragment) {
                    currentFragment.closeProgressTrackingScreen()
                }
            }
        }
    }

    @ReactMethod
    fun startWalkingPath() {
        (currentTopActivity as? TurnByTurnNavigationActivity)?.apply {
            runOnUiThread {
                (currentTopActivity as? TurnByTurnNavigationActivity)?.startWalkingPathFromReact()
            }
        }
    }


    @ReactMethod
    fun cancelParkingAvailable() {
        (currentTopActivity as? TurnByTurnNavigationActivity)?.apply {
            runOnUiThread {
                (currentTopActivity as? TurnByTurnNavigationActivity)?.cancelParkingAvailable()
            }
        }
    }

    /**
     * Handle backing to Landing screen after Parking availability update
     */
    @ReactMethod
    fun backToHomePage() {
        (currentTopActivity as? TurnByTurnNavigationActivity)?.apply {
            runOnUiThread {
                (currentTopActivity as? TurnByTurnNavigationActivity)?.backToHomePageFromRN()
            }
        }
    }

    @ReactMethod
    fun appearInstabug() {
        (currentTopActivity as? DashboardActivity)?.run {
            Analytics.logClickEvent(Event.SETTINGS_FEEDBACK_CLICK, getScreenName())
            Instabug.show()
        }
    }

    @ReactMethod
    fun closeCollectionDetail() {
        Timber.d("closeCollectionDetail: ")
        (currentTopActivity as? DashboardActivity)?.run {
            runOnUiThread {
                supportFragmentManager.popBackStack()
            }
        }
    }

    /**
     * This function is called when we want to open route planning from React Native
     * */
    @ReactMethod
    fun showRouteToLocation(
        selectedIndex: Int,
        location: ReadableMap?
    ) {
        Timber.d("Event triggered [searchLocationResult($selectedIndex)]")
        location?.let {
            Timber.d("Index: $selectedIndex location details: $location")
            (currentTopActivity as? DashboardActivity)?.handleShowingRouteToLocationFromRN(
                selectedIndex,
                it
            )
        }
    }

    /**
     * This function is call when user select a location when searching in React Native
     * */
    @ReactMethod
    fun handleSearchLocationResult(location: ReadableMap?) {
        Timber.d("Event triggered [handleSearchLocationResult] $location")
        location?.let {
            (currentTopActivity as? DashboardActivity)?.handleSearchedLocationFromRN(it)
        }
    }

    @ReactMethod
    fun sendTrafficCameraData(data: ReadableMap?) {
        Timber.d("Event triggered [sendTrafficCameraData]")
        (currentTopActivity as? DashboardActivity)
            ?.takeIf {
                DashboardMapStateManager.currentModeSelected == LandingMode.TRAFFIC && data != null
            }?.run {
                onReceiveTrafficDataRN(data!!)
            }
    }


    @ReactMethod
    fun handleResetHome() {
        Timber.d("Event triggered [handleResetHome]")
        (currentTopActivity as? DashboardActivity)?.apply {
            callAfterInitialized {
                runOnUiThread {
                    handleClickHome()
                }
            }
        }
    }


    /**
     * cameraID: string
     */
    @ReactMethod
    fun selectCamera(cameraID: String?) {
        Timber.d("Event triggered [selectCamera($cameraID)]")
        if (cameraID != null) {
            (currentTopActivity as? DashboardActivity)?.apply {
                callAfterInitialized {
                    runOnUiThread {
                        handleSelectedCamera(cameraID)
                    }

                }
            }
        }
    }


    /**
     * data: string
     */
    @ReactMethod
    fun handleClickTrafficInbox(dataJson: String?) {
        Timber.d("Event triggered [handleClickTrafficInbox]")
        if (dataJson != null) {
            (currentTopActivity as? DashboardActivity)?.apply {
                callAfterInitialized {
                    runOnUiThread {
                        handleNotificationClick(dataJson)
                    }
                }
            }
        }
    }

    @ReactMethod
    fun sendSeenMessageList(data: String) {
        Timber.d("Event triggered [sendSeenMessageList]")
        if (data != null) {
            (currentTopActivity as? DashboardActivity)?.apply {
                callAfterInitialized {
                    runOnUiThread {
                        sendUnReadMessageList(data)
                    }
                }
            }
        }
    }

    @ReactMethod
    fun handleSharingMapCollection(data: String) {
        if (data != null) {
            (currentTopActivity as? DashboardActivity)?.apply {
                callAfterInitialized {
                    runOnUiThread {
                        handleSharingCollection(data)
                    }
                }
            }
        }
    }


    /**
     * this function to get location for react in dashboard landing
     */
    @ReactMethod
    fun getLandingFocusedLocation(mode: String, callback: Callback) {
        (currentTopActivity as? DashboardActivity)?.run {
            callback.invoke(Arguments.createMap().apply {
                val locationFocus = when (mode) {
                    LandingMode.PARKING.mode -> getFocusedLocationForParkingCalculator()
                    LandingMode.OBU.mode -> getFocusedLocationForParkingCalculator()
                    LandingMode.TRAFFIC.mode -> getFocusedLocationForTrafficCamera()
                    else -> LocationBreezeManager.getInstance().currentLocation
                }
                putString("lat", "${locationFocus?.latitude}")
                putString("long", "${locationFocus?.longitude}")
            })
        }
    }


    @ReactMethod
    fun getUserPreferenceSettings(callback: Callback) {
        Timber.d("Event triggered getUserPreferenceSettings")
        if (BreezeCarUtil.masterlists != null && BreezeCarUtil.settingsResponse != null) {
            val dataMapList =
                Gson().toJson(AllSettingResponse(masterlists = BreezeCarUtil.masterlists))
            val dataUserSetting = Gson().toJson(BreezeCarUtil.settingsResponse)

            callback.invoke(Arguments.createMap().apply {
                putString("settings_mapper", dataMapList)
                putString("settings_user", dataUserSetting)
            })
        } else {
            (currentTopActivity as? DashboardActivity)?.apply {
                callAfterInitialized {
                    runOnUiThread {
                        getUserSetting()
                    }
                }
            }
        }
    }

    @ReactMethod
    fun getUserLocation(callback: Callback) {
        Timber.d("Event triggered [getUserLocation]")
        LocationBreezeManager.getInstance().getUserCurrentLocation(callback)
    }

    @ReactMethod
    fun openERPDetail(
        erpID: Int
    ) {
        Timber.d("Event triggered [openERPDetail($erpID)]")
        if (erpID != null) {
            (currentTopActivity as? DashboardActivity)?.apply {
                callAfterInitialized {
                    runOnUiThread {
                        openERPDetail(erpID)
                    }
                }
            }
        }
    }


    @ReactMethod
    fun handleChangeERPSelectedTime(timestampToUse: Double) {
        Timber.d("Event triggered [handleChangeERPSelectedTime($timestampToUse)]")
        if (timestampToUse != null) {
            (currentTopActivity as? DashboardActivity)?.apply {
                callAfterInitialized {
                    runOnUiThread {
                        updateERPDetailTimeStamps(timestampToUse.toLong())
                    }
                }
            }
        }
    }


    @ReactMethod
    fun openTrafficDetail(expressWayCode: String) {
        Timber.d("Event triggered [openTrafficDetail]")
        if (expressWayCode != null) {
            (currentTopActivity as? DashboardActivity)?.apply {
                callAfterInitialized {
                    openTrafficDetail(expressWayCode)
                }
            }
        }
    }


    @ReactMethod
    fun initUserPreferenceSetting(navigationParams: ReadableMap?) {
        Timber.d("Event triggered [initUserPreferenceSetting]")
        currentTopActivity?.runOnUiThread {
            (currentTopActivity as? DashboardActivity)?.apply {
                callAfterInitialized {
                    initUserPreferenceSetting(navigationParams)
                }
            }
        }
    }

    @ReactMethod
    fun getTrafficNearBy(objectTraffic: ReadableMap?) {
        Timber.d("Event triggered [getTrafficNearBy]")
        currentTopActivity?.runOnUiThread {

        }
    }


    @ReactMethod
    fun selectAmenity(element_name: String, isSelected: Boolean) {
        Timber.d("Event triggered [selectAmenity($element_name,$isSelected)]")
        currentTopActivity?.runOnUiThread {
            (currentTopActivity as? DashboardActivity)?.apply {
                callAfterInitialized {
                    selectAmenity(element_name, isSelected)
                }
            }
        }
    }

    @ReactMethod
    fun openCarkParkList(isSelected: Boolean) {
        currentTopActivity?.runOnUiThread {
            (currentTopActivity as? DashboardActivity)?.apply {
                callAfterInitialized {
                    toggleCarparkList(isSelected)
                }
            }
        }
    }


    @ReactMethod
    fun switchMode(selectedMode: String) {
        Timber.d("Event triggered [switchMode($selectedMode)]")
        (currentTopActivity as? DashboardActivity)?.run {
            callAfterInitialized {
                switchLandingModeFromRN(selectedMode)
            }
        }
    }


    @ReactMethod
    fun handleClearSearch(promise: Promise) {
        Timber.d("Event triggered [handleClearSearch]")

        (currentTopActivity as? DashboardActivity)?.apply {
            callAfterInitialized {
                runOnUiThread {
                    handleClearSearch()
                    promise.resolve(null)
                }
            }
        }
    }

    @ReactMethod
    fun closeMoreScreen() {
        Timber.d("Event triggered [closeMoreScreen]")
        //Nothing to do for Android it seems. Need to check further
    }

    @ReactMethod
    fun sendEventERPNavigateToRoutePlaning() {
        Timber.d("Event triggered [sendEventERPNavigateToRoutePlaning]")
    }


    @ReactMethod
    fun sendMessageToNative(rnDetails: ReadableMap?, screenName: String) {
        Timber.d("Event triggered [sendMessageToNative($screenName)]")
        if (screenName == RNScreen.ETA_FAVOURITE_SELECT) {
            return
        }
        when (screenName) {
//            RNScreen.NOTIFY_UPDATE -> {
//                Timber.d("ETANotifyUpdate called")
//            }

//            RNScreen.FAVOURITE_DELETE -> {
//                Timber.d("FavouriteDelete called")
//            }

            else -> {
                if (rnDetails?.toHashMap() != null) {
                    val map = rnDetails.toHashMap()
                    RxBus.publish(RxEvent.ETARNDataCallback(map))
                }
                Timber.d("Callback received - send message to native")
            }
        }
    }

    @ReactMethod
    fun getAnalyticEventsFromRN(
        eventName: String,
        eventValue: String,
        screenName: String,
        saveToLocal: Boolean = false,
        extraData: ReadableMap? = null
    ) {
        Timber.d(
            "Event triggered [getAnalyticEventsFromRN($eventName, $eventValue, $screenName, $saveToLocal, $extraData)]"
        )
        Analytics.logAnalyticsFromRN(eventName, eventValue, screenName, extraData)

        if (saveToLocal) {
            (currentTopActivity as? DashboardActivity)?.run {
                saveCustomAnalytics(eventName, eventValue, screenName)
            }
        }
    }

    @ReactMethod
    fun getAnalyticEventsFromRNForScreenView(screenName: String) {
        Timber.d("Event triggered [getAnalyticEventsFromRNForScreenView($screenName)]")
        Analytics.logScreenView(screenName)
    }

    @ReactMethod
    fun getAnalyticLogOpenPopUpEvent(screenName: String, popupName: String) {
        Timber.d("Event triggered [getAnalyticLogOpenPopUpEvent($screenName, $popupName)]")
        Analytics.logOpenPopupEvent(popupName, screenName)
    }

    @ReactMethod
    fun getAnalyticLogClosePopUpEvent(screenName: String, popupName: String) {
        Timber.d("Event triggered [getAnalyticLogClosePopUpEvent($screenName, $popupName)]")
        Analytics.logClosePopupEvent(popupName, screenName)
    }

    @ReactMethod
    fun getAnalyticLogPopUpEvent(screenName: String, popupName: String, eventValue: String) {
        Timber.d("Event triggered [getAnalyticLogPopUpEvent($screenName, $popupName, $eventValue)]")
        Analytics.logPopupEvent(popupName, eventValue, screenName)
    }


    @ReactMethod
    fun closeNotifyArrivalScreen() {
        Timber.d("Event triggered [closeNotifyArrivalScreen]")
        RxBus.publish(RxEvent.CloseNotifyArrivalScreen())
    }


    /*
   * Open native screen
   */
    @ReactMethod
    fun openNativeScreen(screenName: String, navigationParams: ReadableMap?) {
        Timber.d("Event triggered [openNativeScreen($screenName)]")
        val screen = AndroidScreen.values().find { it.nameStr == screenName } ?: return

        when (screen) {
            AndroidScreen.LANDING -> {
                currentTopActivity?.run {
                    val intent = Intent(this, DashboardActivity::class.java).apply {
                        flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    }
                    startActivity(intent)
                }
            }

            AndroidScreen.TRIP_LOG -> {
                (currentTopActivity as? DashboardActivity)
                    ?.screenNavigationHelper
                    ?.openTripLogFromRN(navigationParams)
            }

            AndroidScreen.ROUTE_PLANNING -> {
                (currentTopActivity as? DashboardActivity)?.screenNavigationHelper?.openRoutePlanningFromRN(
                    navigationParams
                )
            }

            AndroidScreen.COLLECTION_DETAILS -> {
                (currentTopActivity as? DashboardActivity)?.screenNavigationHelper?.openCollectionDetailsFromRN(
                    navigationParams
                )
            }

            AndroidScreen.CREATE_ACCOUNT -> {
                (currentTopActivity as? LoginActivity)?.requestCreateAccount()
            }

            AndroidScreen.SIGN_IN -> {
                (currentTopActivity as? LoginActivity)?.requestSignInAccount()
            }

            AndroidScreen.EXPLORE_MAP -> {
                (currentTopActivity as? DashboardActivity)?.openExploreMapScreenFromDashboard(
                    navigationParams?.getString(ExploreMapsFragment.DATA)
                )
            }

            AndroidScreen.CONNECT_OBU_DEVICE -> {
                (currentTopActivity as? DashboardActivity)?.openConnectOBUDeviceScreen()
            }

            AndroidScreen.OBU_PAIRED_DEVICES -> {
                (currentTopActivity as? DashboardActivity)?.openOBUPairedVehiclesScreen()
            }

            AndroidScreen.OBU_LITE_DISPLAY -> {
//                (currentTopActivity as? DashboardActivity)?.openOBULiteDisplay()
                OBUStripStateManager.getInstance()?.openOBUFromRN()
            }

            else -> {
                RxBus.publish(
                    RxEvent.AddDashboardFragment(screen, navigationParams?.toHashMap() ?: HashMap())
                )
            }
        }
    }

    @ReactMethod
    fun startDashboardActivity() {
        Timber.d("Event triggered [startDashboardActivity]")
    }

    @ReactMethod
    fun getCalendarDates(startDate: String, endDate: String) {
        Timber.d("Event triggered [getCalendarDates($startDate, $endDate)]")
        RxBus.publish(RxEvent.DateRangeSelected(startDate, endDate))
    }

    @ReactMethod
    fun finishActivity() {
        Timber.d("Event triggered [finishActivity]")
        currentTopActivity?.finish()
    }

    @ReactMethod
    fun closeTripLogScreen() {
        Timber.d("Event triggered [closeTripLogScreen]")
//        (currentTopActivity as? DashboardActivity)?.run {
//            callAfterInitialized {
//                runOnUiThread {
//                    closeTripLogFragment()
//                }
//            }
//        }
    }

    //For ERP Screen
    @ReactMethod
    fun searchERPRoutes(
        source: ReadableMap,
        destination: ReadableMap,
        searchTimestamp: Double,
        callback: Callback
    ) {
        Timber.d("Event triggered [searchERPRoutes ($searchTimestamp)]")

        (currentTopActivity as? DashboardActivity)?.run {
            callAfterInitialized {
                CoroutineScope(Dispatchers.IO).launch {
                    searchERPRoutes(
                        source.toHashMap(),
                        destination.toHashMap(),
                        searchTimestamp.toLong()
                    )?.let {
                        val searchErpResultMap = MapUtil.toWritableMap(it)
                        withContext(Dispatchers.Main) {
                            callback.invoke(searchErpResultMap)
                        }
                    }
                }
            }
        }
    }

    @ReactMethod
    fun selectERPRoute(id: String, routeId: Int, erpTimeChanged: Boolean) {
        Timber.d("Event triggered selectERPRoute [$id][$routeId] ")
        (currentTopActivity as? DashboardActivity)?.run {
            CoroutineScope(Dispatchers.IO).launch {
                addERPRoutePreviewFragment(id, routeId, erpTimeChanged)
            }
        }
    }


    @ReactMethod
    fun disableCruise(pIsDisableCruiseMode: Boolean) {
        Timber.d("Event triggered disableCruise [$pIsDisableCruiseMode] ")
//        CoroutineScope(Dispatchers.IO).launch {// change request from Shiva
//            (currentTopActivity as? DashboardActivity)?.run {
//                //Timber.d("bangnv - pIsEnableCruiseMode ", pIsEnableCruiseMode.toString())
//                setUserOnHome(!pIsDisableCruiseMode)
//                if (pIsDisableCruiseMode) {
//                    OBUStripStateManager.getInstance()?.disable()
//                } else {
//                    OBUStripStateManager.getInstance()?.enable()
//                }
//            }
//        }
    }

    @ReactMethod
    fun adjustRecenter(height: Int, index: Int, fromScreen: String) {
        Timber.d("Event triggered adjustRecenter [$height] [$index] [$fromScreen]")
        CoroutineScope(Dispatchers.IO).launch {
            (currentTopActivity as? DashboardActivity)?.run {
                val currentFragment = supportFragmentManager.findFragmentById(R.id.frame_container)
                if (currentFragment != null && currentFragment.isVisible && currentFragment is AdjustableBottomSheet) {
                    currentFragment.adjustRecenterButton(height, index, fromScreen)
                } else {
                    adjustRecenterButton(height, index, fromScreen)
                }
            }
        }
    }

    @ReactMethod
    fun adjustRecenterMapExplore(height: Int, index: Int, fromScreen: String) {
        Timber.d("Event triggered adjustRecenter [$height] [$index] [$fromScreen]")
        CoroutineScope(Dispatchers.IO).launch {
            (currentTopActivity as? DashboardActivity)?.run {
                (supportFragmentManager.findFragmentById(R.id.frame_container) as? ExploreMapsFragment)?.run {
                    adjustRecenterButton(height, index, fromScreen)
                }
            }
        }
    }


    @ReactMethod
    fun selectContent(content_id: Int) {
        Timber.d("Event triggered selectContent [$content_id]")
        CoroutineScope(Dispatchers.IO).launch {
            (currentTopActivity as? DashboardActivity)?.run {
                (supportFragmentManager.findFragmentById(R.id.frame_container) as? ExploreMapsFragment)
                    ?.run {
                        getSelectedContentDetails(content_id)
                    }
            }
        }
    }

    @ReactMethod
    fun showToastfromRN(type: String, message: String) {
        CoroutineScope(Dispatchers.Main).launch {
            (currentTopActivity as? DashboardActivity)?.run {
                showToastSaveAdress(type, message)
            }
            (currentTopActivity as? TurnByTurnNavigationActivity)?.showCustomToast(
                R.drawable.tick_successful,
                message
            )
        }
    }

    @ReactMethod
    fun didSaveSharedLocationToCollection(bookmarkId: Int) {
        Timber.d("Event triggered [ didSaveSharedLocationToCollection( $bookmarkId )")
        RxBus.publish(RxEvent.DidSaveSharedLocation(bookmarkId))
    }

    @ReactMethod
    fun sendCollectionData(fromScreen: String, data: ReadableMap) {
        Timber.d("Event triggered [ sendCollectionData( $fromScreen) ] - data = $data")
        when (fromScreen) {
            RNScreen.CHOOSE_COLLECTION -> {
                (currentTopActivity as? DashboardActivity)?.handleSaveAddressFromTooltip(data)
            }

            RNScreen.SHARED_COLLECTION -> RxBus.publish(RxEvent.SharedCollectionDataRefresh(data))
            RNScreen.COLLECTION_DETAILS,
            RNScreen.SHORTCUT_FULL_LIST,
            RNScreen.COLLECTION_BOOKMARK_EDITOR -> {
                val myType = object : TypeToken<ArrayList<PlaceDetailsResponse>>() {}.type
                val bookmarks = Gson().fromJson<ArrayList<PlaceDetailsResponse>>(
                    data.getArray("bookmarks").toString(), myType
                )
                (currentTopActivity as? DashboardActivity)?.run {
                    runOnUiThread {
                        (supportFragmentManager.findFragmentById(R.id.frame_container) as? PlaceSaveDetailFragment)
                            ?.handlePoi(
                                bookmarks, fromScreen == RNScreen.COLLECTION_BOOKMARK_EDITOR
                            )
                    }
                }
            }
        }
    }

    @ReactMethod
    fun switchToLandingTab(screenName: String) {
        Timber.d("switchToLandingTab [$screenName]")
        CoroutineScope(Dispatchers.Main).launch {
            (currentTopActivity as? DashboardActivity)?.run {
                pushScreenNameBottomTab(screenName)
            }
        }
    }


    @ReactMethod
    fun sendLocationDataAnalytics(
        eventValue: String, screenName: String,
        locationName: String, latitude: String, longitude: String
    ) {
        Timber.d("Event triggered [sendLocationDataAnalytics]")
        Analytics.logLocationDataEvent(eventValue, screenName, locationName, latitude, longitude)
    }

    @ReactMethod
    fun closeOnboardingTutorial() {
        Timber.d("Event triggered [closeOnboardingTutorial]")
        (currentTopActivity as? DashboardActivity)?.run {
            setUserOnHome(true)
            updateInAppGuideUserPreference()
        }
    }

    @ReactMethod
    fun continueWithoutAccount() {
        CoroutineScope(Dispatchers.Main).launch {
            (currentTopActivity as? LoginActivity)?.run {
                showFragmentWarningCreateGuestAccount()
            }
        }
    }


    @ReactMethod
    fun updateUserPreferenceSettings(data: ReadableMap) {
        Timber.d("Event triggered updateUserPreferenceSettings: ${data}")

        (currentTopActivity as? DashboardActivity)?.apply {
            callAfterInitialized {
                runOnUiThread {
                    updateUserPreferences(data)
                }
            }
        }
    }

    @ReactMethod
    fun goBackExploreMap(screenName: String = "") {
        Timber.d("Event triggered goBackExploreMap [$screenName]")
        (currentTopActivity as? DashboardActivity)?.run {
            val currentFragment = supportFragmentManager.findFragmentById(R.id.frame_container)
            if (currentFragment != null && currentFragment is ExploreMapsFragment) {
                Analytics.logClickEvent(Event.BACK, currentFragment.getScreenName())
                supportFragmentManager.popBackStack()
            }
        }
    }

    @ReactMethod
    fun shareCarDetailsToNative(carplateNumber: String, iuNumber: String) {
        breezeUserPreferenceUtil.saveUserCarDetails(iuNumber, carplateNumber)
        ((currentTopActivity as? DashboardActivity)?.supportFragmentManager?.findFragmentByTag(
            Constants.TAGS.ACCOUNT_TAG
        ) as? ProfileFragment)?.onCarDetailsChanged()
    }

    @ReactMethod
    fun getLocalUserData(promise: Promise) {
        promise.resolve(breezeUserPreferenceUtil.getUserStoredData()?.toRNMap())
    }

    @ReactMethod
    fun toggleDisplayTopObuButtons(isShow: Boolean) {
        (((currentTopActivity as? DashboardActivity)?.supportFragmentManager
            ?.findFragmentByTag(OBULiteFragment.TAG) as? OBULiteFragment)
            ?.toggleControllersLayoutVisibility(isShow))
    }

    @ReactMethod
    fun selectCarparkFromCarparkList(carpark: ReadableMap) {
        RxBus.publish(RxEvent.OBUCarParkSelectedEvent(carpark))
    }

    @ReactMethod
    fun removeOBU(vehicleNumber: String, obuName: String) {
        Timber.d("removeOBU: vehicleNumber = $vehicleNumber, obuName = $obuName")
        currentTopActivity?.getApp()?.obuConnectionHelper
            ?.removePairedOBUDevice(vehicleNumber, obuName)
    }

    @ReactMethod
    fun toggleConnectOBU(isConnecting: Boolean, vehicleNumber: String, obuName: String) {
        Timber.d("toggleConnectOBU: vehicleNumber = $vehicleNumber, obuName = $obuName, isConnecting = $isConnecting")
        currentTopActivity?.getApp()?.obuConnectionHelper?.toggleOBUConnectionRN(
            isConnecting,
            vehicleNumber,
            obuName
        )
    }

    @ReactMethod
    fun cancelOBUCarparkAvailability() {
        RxBus.publish(RxEvent.CancelOBUTravelTimeEvent())
    }

    @ReactMethod
    fun cancelOBUTravelTime() {
        RxBus.publish(RxEvent.CancelOBUTravelTimeEvent())
    }

    /**
     * To handle click event to back to Landing screen in First OBU connection introduction
     * */
    @ReactMethod
    fun popToMapLanding() {
        (currentTopActivity as? DashboardActivity)?.run {
            runOnUiThread {
                popAllFragmentsInBackStack()
            }
        }
    }

    @ReactMethod
    fun logNative(tag: String, message: String) {
        when (tag) {
            "info" -> Timber.i("ReactNative - $message")
            "warning" -> Timber.w("ReactNative - $message")
            "error" -> Timber.e("ReactNative - $message")
            else -> Timber.d("ReactNative - $message")
        }
    }

    @ReactMethod
    fun toggleOBUAutoConnect(isEnabled: Boolean) {
        currentTopActivity?.getApp()?.run {
            getUserDataPreference()?.toggleOBUAutoConnect(isEnabled)
            obuConnectionHelper.enableAutoConnect(isEnabled)
        }
    }

    @ReactMethod
    fun cancelPairing(obuName: String) {
        currentTopActivity?.getApp()?.obuConnectionHelper
            ?.disconnectOBU()
    }

    @ReactMethod
    fun toggleMapViewOBU(isShowingMap: Boolean) {
        Timber.d("Event triggered toggleMapViewOBU: isShowingMap = $isShowingMap")
        RxBus.publish(RxEvent.OBUDisplayToggleMapEvent(isShowingMap))
    }

    @ReactMethod
    fun closeCarparkListScreen() {
        Timber.d("closeCarparkListScreen")
        RxBus.publish(RxEvent.CloseCarparkListScreen())
    }

    @ReactMethod
    fun openCarparkListScreen() {
        Timber.d("openCarparkListScreen")
        RxBus.publish(RxEvent.OpenCarparkListScreen())
    }

    @ReactMethod
    fun showNoCarparkAlert() {
        Timber.d("showNoCarparkAlert")
        RxBus.publish(RxEvent.NoCarparkAlert())
    }

    @ReactMethod
    fun closeLandingDropPinScreen() {
        Timber.d("closeLandingDropPinScreen")
        RxBus.publish(RxEvent.CloseLandingDropPinScreen())
    }

    @ReactMethod
    fun onSavedDropPinLanding(pinLocationData: ReadableMap) {
        Timber.d("onSavedDropPinLanding: $pinLocationData")
        RxBus.publish(RxEvent.LandingDropPinSaved(pinLocationData))
    }

    @ReactMethod
    fun onCollectionDetailSearch(searchLocation: ReadableMap) {
        Timber.d("onCollectionDetailSearch: $searchLocation")
        RxBus.publish(RxEvent.CollectionDetailsSearchLocation(searchLocation))
    }

    @ReactMethod
    fun clearSearchCollectionDetails() {
        Timber.d("clearSearchCollectionDetails")
        RxBus.publish(RxEvent.CollectionDetailsClearSearch())
    }

    @ReactMethod
    fun closeInviteScreen() {
        Timber.d("closeInviteScreen")
        RxBus.publish(RxEvent.RoutePlanningCloseInvite())
    }

    override fun getName(): String {
        return "CommunicateWithNative"
    }
}


package com.ncs.breeze.helper

import androidx.core.os.bundleOf
import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.facebook.react.bridge.Arguments
import com.ncs.breeze.App
import com.breeze.model.api.response.TripPlannerItem
import com.ncs.breeze.common.remote.ApiHelper
import com.ncs.breeze.common.rxbus.RxBus
import com.ncs.breeze.common.rxbus.RxEvent
import com.ncs.breeze.common.utils.Utils
import com.ncs.breeze.reactnative.nativemodule.ShareBusinessLogicEvent
import io.reactivex.schedulers.Schedulers

class TripsPlannerPagingSource(
    private val apiHelper: ApiHelper,
    var startDate: String = "",
    var endDate: String = "",
    var searchText: String? = ""
) : PagingSource<Int, TripPlannerItem>() {

    companion object {
        const val STARTING_PAGE_INDEX: Int = 1
        const val PAGE_SIZE = 10
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, TripPlannerItem> {

        return try {
            val nextPage = params.key ?: STARTING_PAGE_INDEX
            val tripSummaryResponse = apiHelper.getTripUpcomingSummary(
                nextPage,
                PAGE_SIZE,
                startDate,
                endDate,
                searchText
            )
            RxBus.publish(RxEvent.PlannedTripItemsUpdated(tripSummaryResponse.trips.size))
            RxBus.publish(RxEvent.IsUpComingTripsFeatureUsed(
                (tripSummaryResponse.totalItems ?: 0) != 0
            ))
            checkUpcomingTripEvent(tripSummaryResponse.trips)
            LoadResult.Page(
                data = tripSummaryResponse.trips,
                prevKey = if (nextPage == 1) null else nextPage - 1,
                nextKey = if (nextPage < tripSummaryResponse.totalPages!!)
                    tripSummaryResponse.currentPage?.plus(1) else null
            )
        } catch (e: Exception) {
            LoadResult.Error(e)
        }
    }


    private fun checkUpcomingTripEvent(trips: List<TripPlannerItem>) {

        val isExistTriplogToday = Utils.isExistTripLogInDay(trips)
        App.instance!!.run {
            compositeDisposable.add(
                shareBusinessLogicPackage.getRequestsModule().flatMap {
                    Arguments.fromBundle(
                        bundleOf("hasUpcomingTrips" to isExistTriplogToday)
                    ).let { args ->
                        it.callRN(ShareBusinessLogicEvent.FN_UPDATE_UPCOMING_TRIPS.eventName, args)
                    }
                }
                    .subscribeOn(Schedulers.io())
                    .subscribe()
            )
        }
    }


    // The refresh key is used for subsequent refresh calls to PagingSource.load after the initial load
    override fun getRefreshKey(state: PagingState<Int, TripPlannerItem>): Int? {
        // We need to get the previous key (or next key if previous is null) of the page
        // that was closest to the most recently accessed index.
        // Anchor position is the most recently accessed index
        return state.anchorPosition?.let { anchorPosition ->
            state.closestPageToPosition(anchorPosition)?.prevKey?.plus(1)
                ?: state.closestPageToPosition(anchorPosition)?.nextKey?.minus(1)
        }
    }
}
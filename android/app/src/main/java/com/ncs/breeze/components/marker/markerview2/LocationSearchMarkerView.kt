package com.ncs.breeze.components.marker.markerview2

import android.animation.Animator
import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import com.google.android.gms.maps.model.LatLng
import com.mapbox.geojson.Point
import com.mapbox.maps.CameraOptions
import com.mapbox.maps.MapboxMap
import com.mapbox.maps.plugin.animation.MapAnimationOptions
import com.mapbox.maps.plugin.animation.easeTo
import com.ncs.breeze.R


class LocationSearchMarkerView constructor(
    val latLng: LatLng,
    mapboxMap: MapboxMap,
    val activity: Activity,
    val markerViewManager: MarkerViewManager2
) : MarkerView2(mapboxMap = mapboxMap) {

    private var annotationView: View?

    private var img_destination: ImageView? = null

    init {

        mLatLng = latLng

        val inflater = activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

        annotationView = inflater.inflate(R.layout.search_location_marker_view, null)
        annotationView?.apply {
            img_destination = findViewById(R.id.img_destination)
        }
        mViewMarker = annotationView

        img_destination?.setOnClickListener {


            val cameraOptions = CameraOptions.Builder()
                .center(Point.fromLngLat(latLng.longitude, latLng.latitude))
                .zoom(16.0)
                .build()

            mapboxMap.easeTo(
                cameraOptions,
                MapAnimationOptions.mapAnimationOptions {
                    duration(400L)
                    animatorListener(object : Animator.AnimatorListener {
                        override fun onAnimationStart(animation: Animator) {}

                        override fun onAnimationEnd(animation: Animator) {
                            annotationView?.bringToFront()
                        }

                        override fun onAnimationCancel(animation: Animator) {}

                        override fun onAnimationRepeat(animation: Animator) {}

                    })
                }
            )

        }
    }


    override fun updateTooltipPosition() {
        mLatLng?.let { location ->
            val screenCoordinate = mapboxMap.pixelForCoordinate(
                Point.fromLngLat(
                    location.longitude,
                    location.latitude
                )
            )
            mViewMarker?.let {
                it.measure(
                    View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                    View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
                );
                it.x = screenCoordinate.x.toFloat() - (it.measuredWidth / 2)
                it.y = screenCoordinate.y.toFloat() - it.measuredHeight
            }
        }


    }


}
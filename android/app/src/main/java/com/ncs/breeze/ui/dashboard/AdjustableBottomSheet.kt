package com.ncs.breeze.ui.dashboard

interface AdjustableBottomSheet {
    fun adjustRecenterButton(height: Int, index: Int, fromScreen: String)
}
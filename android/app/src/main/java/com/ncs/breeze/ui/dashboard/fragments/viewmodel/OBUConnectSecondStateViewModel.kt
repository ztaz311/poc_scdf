package com.ncs.breeze.ui.dashboard.fragments.viewmodel

import androidx.lifecycle.ViewModel
import com.breeze.model.obu.OBUDevice
import com.breeze.model.constants.Constants

class OBUConnectSecondStateViewModel : ViewModel() {
    var existingVehicleNames: ArrayList<String> = arrayListOf()

    fun isValidVehicleName(vehicleName: CharSequence?) =
        vehicleName.isNullOrBlank() || vehicleName.matches(Regex(Constants.REGEX.VEHICLE_NAME))

    fun isVehicleNameExisted(obuDevice: OBUDevice) = existingVehicleNames.find { name ->
        name == (obuDevice.vehicleNumber.takeUnless { it.isBlank() }
            ?: obuDevice.obuName)
    } != null

}
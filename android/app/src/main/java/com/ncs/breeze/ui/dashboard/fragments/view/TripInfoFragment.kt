package com.ncs.breeze.ui.dashboard.fragments.view

import android.Manifest
import android.Manifest.permission.CAMERA
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.text.Editable
import android.text.TextWatcher
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.ImageView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.FileProvider
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import com.ncs.breeze.BuildConfig
import com.ncs.breeze.R
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.Event
import com.ncs.breeze.common.analytics.Screen
import com.breeze.model.extensions.round
import com.breeze.model.api.request.ErpItem
import com.breeze.model.api.request.TripFileRequest
import com.breeze.model.api.request.TripIdRequest
import com.breeze.model.api.request.TripUpdateRequest
import com.breeze.model.api.response.TripDetailsResponse
import com.breeze.model.constants.Constants
import com.ncs.breeze.common.utils.Utils
import com.ncs.breeze.components.TripERPAdapter
import com.ncs.breeze.databinding.FragmentTripInfoBinding
import com.ncs.breeze.ui.base.BaseFragment
import com.ncs.breeze.ui.dashboard.activity.DashboardActivity
import com.ncs.breeze.ui.dashboard.fragments.viewmodel.TripInfoViewModel
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import kotlin.math.roundToInt


open class TripInfoFragment : BaseFragment<FragmentTripInfoBinding, TripInfoViewModel>(),
    TripERPAdapter.ERPAmountChangeListener {

    private var originalValue: Double = 0.00

    companion object {
        const val TAG = "TripInfoFragment"
    }


    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val mViewModel: TripInfoViewModel by viewModels {
        viewModelFactory
    }

    private var tripDetails: TripDetailsResponse? = null
    private var currentFocusedView: View? = null
    private var compositeDisposable: CompositeDisposable = CompositeDisposable()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activity?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
        arguments?.let {
            tripDetails =
                arguments?.getSerializable(Constants.TRIP_SUMMARY_DETAILS) as? TripDetailsResponse
        }
        initialize()

    }

    private fun initialize() {

        viewBinding.backButton.setOnClickListener {
            (requireActivity() as DashboardActivity).hideKeyboard()
            onBackPressed()
        }
        viewBinding.parkingExpense.getEditText().isEnabled =
            tripDetails?.isParkingEditable ?: true
        viewBinding.tvParkingExpenseExplain.isVisible = !tripDetails?.remark.isNullOrBlank()
        viewBinding.tvParkingExpenseExplain.text = tripDetails?.remark


        viewBinding.saveButton.setOnClickListener {
            Analytics.logClickEvent(Event.SAVE_BUTTON_CLICK, getScreenName())
            triggerUpdateTrip()
        }
        disableSaveButton()
        viewBinding.layout.setOnClickListener {
            hideKeyboard()
        }
        viewBinding.rlScrollView.setOnClickListener {
            hideKeyboard()
        }

        if (tripDetails != null) {
            viewBinding.saveButton.isVisible =
                tripDetails?.run { isParkingEditable != false || tripDetails?.erp?.any { erp -> erp.isERPEditable == true } != false } ?: true

            if (tripDetails!!.tripStartTime != null && tripDetails!!.tripEndTime != null) {
                viewBinding.travelTime.text =
                    getTravelledTimeString(
                        tripDetails!!.tripStartTime!!,
                        tripDetails!!.tripEndTime!!
                    )
                viewBinding.date.text = getDateText(tripDetails!!.tripStartTime!!)
                viewBinding.travelMin.text = String.format(
                    "%d min",
                    getTimeDiff(tripDetails!!.tripStartTime!!, tripDetails!!.tripEndTime!!)
                )
            }

            if (tripDetails!!.totalDistance != null) {
                viewBinding.totalDistance.text = tripDetails!!.totalDistance!!.round()
            } else {
                viewBinding.totalDistance.text = "0.00"
            }

            if (tripDetails!!.parkingExpense != null) {
                viewBinding.parkingExpense.getEditText()
                    .setText(tripDetails!!.parkingExpense!!.round())
                originalValue = tripDetails!!.parkingExpense!!.toDouble()
            } else {
                viewBinding.parkingExpense.getEditText().setText("0.00")
            }

            viewBinding.parkingExpense.getEditText()
                .addTextChangedListener(object : TextWatcher {
                    override fun beforeTextChanged(
                        s: CharSequence?,
                        start: Int,
                        count: Int,
                        after: Int
                    ) {
                    }

                    override fun onTextChanged(
                        editedExpense: CharSequence?,
                        start: Int,
                        before: Int,
                        count: Int
                    ) {
                        try {
                            if (!(originalValue == editedExpense.toString()
                                    .toDouble() || editedExpense.toString().toDouble() < 0)
                            ) {
                                enableSaveButton()
                            }
                            val editedAmount = editedExpense.toString().toDouble()
                            tripDetails!!.parkingExpense = editedAmount
                            updateExpenses()
                        } catch (e: Exception) {
                            Timber.e(e)
                        }
                    }

                    override fun afterTextChanged(s: Editable?) {}
                })
            viewBinding.parkingExpense.getEditText().onFocusChangeListener =
                View.OnFocusChangeListener { v, hasFocus ->
                    if (!hasFocus) {
                        Analytics.logEditEvent(Event.EDIT_PARKING_PRICE, getScreenName())
                        if (viewBinding.parkingExpense.getEditText().text.isNullOrEmpty()) {
                            viewBinding.parkingExpense.getEditText().setText("0.00")
                        } else {
                            viewBinding.parkingExpense.getEditText().setText(
                                viewBinding.parkingExpense.getEditText().text.toString()
                                    .toDouble().round()
                            )
                        }
                    } else {
                        setFocusedView(viewBinding.parkingExpense)
                    }
                }

            updateExpenses()
            initAdapter()
        }

    }

    private fun calculateImageSize(imParkingReceipt: ImageView, bitmap: Bitmap) {
        var widthImage = bitmap.width
        var heightImage = bitmap.height
        val heightMaxImage = resources.displayMetrics.heightPixels * 0.7
        val widthMaxImage = resources.displayMetrics.widthPixels * 0.9
        val ratio = heightImage / widthImage.toFloat()

        if (widthImage != widthMaxImage.toInt()) {
            widthImage = widthMaxImage.toInt()
            heightImage = (widthMaxImage * ratio).toInt()
        }

        if (heightImage > heightMaxImage) {
            heightImage = heightMaxImage.toInt()
            widthImage = (heightImage / ratio).toInt()
        }

        val layoutParams = imParkingReceipt.layoutParams
        layoutParams.width = widthImage
        layoutParams.height = heightImage
        imParkingReceipt.layoutParams = layoutParams
    }

    private fun enableSaveButton() {
        viewBinding.saveButton.isEnabled = true
        viewBinding.saveButton.setTextColor(requireContext().getColor(R.color.breeze_primary))
    }

    private fun disableSaveButton() {
        viewBinding.saveButton.isEnabled = false
        viewBinding.saveButton.setTextColor(requireContext().getColor(R.color.themed_light_grey))
    }

    private fun updateExpenses() {
        val erpExpense = calculateERPExpense()
        var totalExpense = erpExpense
        if (tripDetails!!.parkingExpense != null) {
            totalExpense += tripDetails!!.parkingExpense!!
        }
        tripDetails!!.totalExpense = totalExpense
        viewBinding.erpExpense.text = erpExpense.round()
        viewBinding.totalExpense.text = totalExpense.round()
    }

    fun openGallery() {
        try {
            if (ActivityCompat.checkSelfPermission(
                    activity as DashboardActivity,
                    Manifest.permission.READ_EXTERNAL_STORAGE
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                Analytics.logOpenPopupEvent(Event.GALLERY_PERMISSION, getScreenName())
                requestPermissions(
                    arrayOf(
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                    ), Constants.REQUEST_CODES.TRIP_INFO_PERMISSION_GALLERY
                )
            } else {
                val galleryIntent =
                    Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                startActivityForResult(
                    galleryIntent,
                    Constants.REQUEST_CODES.TRIP_INFO_SELECT_PHOTO
                )
            }
        } catch (e: java.lang.Exception) {
            Timber.e(e)
        }
    }

    private fun initAdapter() {
        viewBinding.erpRecyclerView.adapter =
            TripERPAdapter(createTripAdapterList(),  this)
    }

    private fun calculateERPExpense(): Double {
        var erpExpense = 0.0
        for (erp in tripDetails!!.erp) {
            erpExpense += erp.actualAmount
        }
        return erpExpense
    }

    private fun openCamera() {
        try {
            if (ActivityCompat.checkSelfPermission(
                    activity as DashboardActivity,
                    CAMERA
                ) != PackageManager.PERMISSION_GRANTED
            ) {

                Analytics.logOpenPopupEvent(Event.CAMERA_PERMISSION, getScreenName())
                requestPermissions(
                    arrayOf(
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        CAMERA
                    ), Constants.REQUEST_CODES.TRIP_INFO_PERMISSION_CAMERA
                )
            } else {
                dispatchTakePictureIntent()
            }
        } catch (e: Exception) {
            Timber.e(e)
        }
    }

    private fun dispatchTakePictureIntent() {

        // Check if there is a camera.
        val context: Context? = activity
        val packageManager = context!!.packageManager
        if (!packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA_ANY)) {
            Toast.makeText(activity, getString(R.string.no_camera), Toast.LENGTH_SHORT)
                .show()
            return
        }

        // Camera exists? Then proceed...
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)

        // Ensure that there's a camera activity to handle the intent
        val activity = activity as DashboardActivity?
        if (takePictureIntent.resolveActivity(activity!!.getPackageManager()) != null) {
            // Create the File where the photo should go.
            // If you don't do this, you may get a crash in some devices.
            var photoFile: File? = null
            try {
                photoFile = createImageFile()
            } catch (ex: IOException) {
                Timber.e(ex)
                val toast = Toast.makeText(
                    activity as DashboardActivity?,
                    getString(R.string.problem_saving_photo),
                    Toast.LENGTH_SHORT
                )
                toast.show()
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                val fileUri = FileProvider.getUriForFile(
                    context,
                    BuildConfig.APPLICATION_ID + ".provider",
                    photoFile
                )
                activity.capturedImageURI = fileUri
                activity.currentPhotoPath = fileUri.path
                val photoURI = FileProvider.getUriForFile(
                    activity as DashboardActivity, BuildConfig.APPLICATION_ID + ".provider",
                    photoFile
                )
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                startActivityForResult(
                    takePictureIntent,
                    Constants.REQUEST_CODES.TRIP_INFO_TAKE_PHOTO
                )
            }
        }
    }


    @Throws(IOException::class)
    protected fun createImageFile(): File? {
        // Create an image file name
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(Date())
        val imageFileName = "JPEG_" + timeStamp + "_"
        val storageDir =
            (activity as DashboardActivity).getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        val image = File.createTempFile(
            imageFileName,  /* prefix */
            ".jpg",  /* suffix */
            storageDir /* directory */
        )
        // Save a file: path for use with ACTION_VIEW intents
        val activity = activity as DashboardActivity?
        activity!!.currentPhotoPath = (image.absolutePath)
        return image
    }

    override fun inflateViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?,
        attachToContainer: Boolean
    ) = FragmentTripInfoBinding.inflate(inflater, container, attachToContainer)

    override fun getViewModelReference(): TripInfoViewModel {
        return mViewModel
    }

    @Suppress("DEPRECATION")
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            Constants.REQUEST_CODES.TRIP_INFO_PERMISSION_GALLERY ->                 // If request is cancelled, the result arrays are empty.
                if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Analytics.logClosePopupEvent(Event.GALLERY_PERMISSION_ALLOW, getScreenName())
                    val galleryIntent =
                        Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                    startActivityForResult(
                        galleryIntent,
                        Constants.REQUEST_CODES.TRIP_INFO_SELECT_PHOTO
                    )
                } else {
                    //do something like displaying a message that he didn`t allow the app to access gallery and you wont be able to let him select from gallery
                }

            Constants.REQUEST_CODES.TRIP_INFO_PERMISSION_CAMERA -> if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                dispatchTakePictureIntent()
            } else {
                Analytics.logClosePopupEvent(Event.GALLERY_PERMISSION_DENY, getScreenName())
                //do something like displaying a message that he didn`t allow the app to access gallery and you wont be able to let him select from gallery
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

    }

    //Convert time to local format
    private fun getTravelledTimeString(tripStartTime: Long, tripEndTime: Long): String {
        return String.format("%s - %s", getTimeText(tripStartTime), getTimeText(tripEndTime))
    }

    private fun getTimeText(time: Long): String {
        return try {
            val date = Utils.CURRENT_DATE()
            date.time = time * 1000L
            val calendar = Utils.CURRENT_CALENDER()
            calendar.time = date
            calendar.timeZone = TimeZone.getDefault()

            val df = SimpleDateFormat("hh:mmaa", Locale.US)
            val formatTime = df.format(calendar.time)
            formatTime.lowercase()
        } catch (e: Exception) {
            Timber.e(e)
            ""
        }
    }


    private fun getDateText(time: Long): String {
        return try {
            val date = Utils.CURRENT_DATE()
            date.time = time * 1000L
            val calendar = Utils.CURRENT_CALENDER()
            calendar.time = date
            calendar.timeZone = TimeZone.getDefault()
            val dayWeekText = SimpleDateFormat("EEEE", Locale.US).format(date)

            val dateStr = SimpleDateFormat("dd MMM yyyy", Locale.US).format(date)

            String.format("%s, %s", dayWeekText, dateStr)
        } catch (e: Exception) {
            Timber.e(e)
            ""
        }
    }


    private fun getTimeDiff(tripStartTime: Long, tripEndTime: Long): Int {
        val totalSeconds = tripEndTime - tripStartTime
        return (totalSeconds / 60.0).roundToInt()
    }

    private fun createTripAdapterList(): ArrayList<TripERPAdapter.TripListObject> {
        val adapterList = arrayListOf<TripERPAdapter.TripListObject>()

        val originItem = TripERPAdapter.TripListObject(
            TripERPAdapter.TripListViewType.ORIGIN,
            addressDetails = TripERPAdapter.PlaceAddressDetails(
                tripDetails!!.tripStartAddress1,
                tripDetails!!.tripStartAddress2
            )
        )

        adapterList.add(originItem)

        val reversedERPList = tripDetails!!.erp.asReversed()
        for (erp in reversedERPList) {
            val erpItem = TripERPAdapter.TripListObject(
                TripERPAdapter.TripListViewType.INDIVIDUAL_ERP,
                erpItem = TripERPAdapter.ERPAdapterObject(erp)
            )
            adapterList.add(erpItem)
        }

        val destinationItem = TripERPAdapter.TripListObject(
            TripERPAdapter.TripListViewType.DESTINATION,
            addressDetails = TripERPAdapter.PlaceAddressDetails(
                tripDetails!!.tripDestAddress1,
                tripDetails!!.tripDestAddress2
            )
        )

        adapterList.add(destinationItem)

        return adapterList
    }

    override fun erpAmountEdited() {
        updateExpenses()
        enableSaveButton()
    }

    override fun erpViewFocused(focusedView: View, isFocused: Boolean) {
        if (isFocused) {
            setFocusedView(focusedView)
        } else {
            Analytics.logEditEvent(Event.EDIT_ERP_PRICE, getScreenName())
        }
    }

    fun hideKeyboard() {
        (activity as DashboardActivity).hideKeyboard()
    }

    override fun onKeyboardVisibilityChanged(isOpen: Boolean) {
        if (isOpen && currentFocusedView != null) {
            scrollToView(currentFocusedView!!)
        }
    }


    private fun scrollToView(currentFocusedView: View) {
        viewBinding.scrollView.post {
            viewBinding.scrollView.scrollTo(
                0,
                getPositionInParent(currentFocusedView)
            )
        }
    }

    /** Returns array of views [x, y] position relative to parent  */
    private fun getPositionInParent(view: View): Int {
        val relativePosition = intArrayOf(view.left, view.top)
        var currentParent = view.parent as ViewGroup
        while (currentParent !== viewBinding.scrollView) {
            relativePosition[0] += currentParent.left
            relativePosition[1] += currentParent.top
            currentParent = currentParent.parent as ViewGroup
        }
        return relativePosition[1]
    }

    private fun setFocusedView(focusedView: View?) {
        currentFocusedView = focusedView
    }

    private fun triggerUpdateTrip() {
        if (tripDetails == null) return

        // Boolean added to check validity before allowing upload
        var isValid = true

        val updateRequest = TripUpdateRequest()
        updateRequest.parkingExpense = tripDetails!!.parkingExpense
        updateRequest.totalExpense = tripDetails!!.totalExpense
        for (erpItem in tripDetails!!.erp) {
            if (erpItem.actualAmount > Constants.ERP_MAX_THRESHOLD) {
                isValid = false
            }
            val updateERP = ErpItem()
            updateERP.actualAmount = erpItem.actualAmount
            updateERP.erpId = erpItem.erpId
            updateERP.tripErpId = erpItem.tripErpId
            updateRequest.erp.add(updateERP)
        }

        if (isValid) {
            mViewModel.updateAPISuccessful.observe(viewLifecycleOwner) {
                mViewModel.updateAPISuccessful.removeObservers(this)
                if (it) {
                    viewBinding.warningToast.root.visibility = View.GONE
                    if (tripDetails != null) {
                        (requireActivity() as DashboardActivity).updateTravelLogList(
                            tripDetails!!.tripId,
                            tripDetails!!.totalExpense!!
                        )
                    }
                    (requireActivity() as DashboardActivity).hideKeyboard()
                    requireActivity().supportFragmentManager.popBackStackImmediate()
                } else {
                    viewBinding.warningToast.warningText.text =
                        getString(R.string.failed_to_save_trip_details)
                    Observable.interval(1, TimeUnit.SECONDS)
                        .take(5).observeOn(
                            AndroidSchedulers.mainThread()
                        ).subscribeBy(
                            onNext = {
                                viewBinding.warningToast.root.visibility = View.VISIBLE
                            },
                            onComplete = {//hide view after 5 seconds
                                viewBinding.warningToast.root.visibility = View.GONE
                            }
                        ).addTo(compositeDisposable)
                }
            }
            mViewModel.updateTripDetails(tripDetails!!.tripId, updateRequest)
        }
    }

    private fun convertBitmapToBase64(bitmap: Bitmap): String {
        return compressImage(bitmap)
    }

    private fun compressImage(image: Bitmap): String {
        val baos = ByteArrayOutputStream()
        image.compress(
            Bitmap.CompressFormat.JPEG,
            100,
            baos
        ) //Compression quality, here 100 means no compression, the storage of compressed data to baos
        var options = 90
        Timber.d("The size is: " + baos.toByteArray().size)
        while (baos.toByteArray().size / 1024 > 2 * 1024) {  //Loop if compressed picture is greater than 400kb, than to compression
            baos.reset() //Reset baos is empty baos
            image.compress(
                Bitmap.CompressFormat.JPEG,
                options,
                baos
            ) //The compression options%, storing the compressed data to the baos
            options -= 10 //Every time reduced by 10
            Timber.d("The size is: " + baos.toByteArray().size)
        }
        return Base64.encodeToString(baos.toByteArray(), Base64.DEFAULT)

    }


    private fun triggerUploadFile(encodedImage: String) {
        mViewModel.uploadReceiptAPIFailure.observe(viewLifecycleOwner) {
            if (it) {
                viewBinding.warningToast.warningText.text =
                    getString(R.string.failed_to_add_receipt)
                Observable.interval(1, TimeUnit.SECONDS)
                    .take(5).observeOn(
                        AndroidSchedulers.mainThread()
                    ).subscribeBy(
                        onNext = {
                            viewBinding.warningToast.root.visibility = View.VISIBLE
                        },
                        onComplete = {//hide view after 5 seconds
                            viewBinding.warningToast.root.visibility = View.GONE
                        }
                    ).addTo(compositeDisposable)
            } else {
                viewBinding.warningToast.root.visibility = View.GONE
            }
        }
        mViewModel.uploadReceiptAPISuccessful.observe(viewLifecycleOwner) {
            triggerUpdateTrip()
        }
        val mTripFileRequest = TripFileRequest()
        mTripFileRequest.tripId = tripDetails!!.tripId
        mTripFileRequest.fileType = "image/jpeg"
        mTripFileRequest.attachmentFile = encodedImage
        mViewModel.uploadParkingReceipt(mTripFileRequest)

    }

    private fun triggerDeletefile() {
        val mTripid = TripIdRequest()
        mTripid.tripId = tripDetails!!.tripId
        mViewModel.deleteparkingReceipt(mTripid)

    }

    override fun getScreenName(): String {
        return Screen.TRAVEL_LOG_TRIP_SUMMARY
    }

    override fun onDestroyView() {
        super.onDestroyView()
        compositeDisposable.dispose()
    }
}

package com.ncs.breeze.ui.dashboard.fragments.view

import android.location.Location
import android.os.Bundle
import android.text.Editable
import android.text.InputFilter
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import com.instabug.library.Instabug
import com.mapbox.maps.MapView
import com.mapbox.navigation.core.lifecycle.MapboxNavigationApp
import com.mapbox.navigation.core.telemetry.events.FeedbackEvent
import com.ncs.breeze.R
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.Event
import com.ncs.breeze.common.analytics.Screen
import com.breeze.model.api.request.FeedbackRequest
import com.breeze.model.api.request.TypeItem
import com.breeze.model.constants.Constants
import com.ncs.breeze.common.utils.LocationBreezeManager
import com.ncs.breeze.databinding.FragmentFeedbackBinding
import com.ncs.breeze.ui.base.BaseActivity
import com.ncs.breeze.ui.base.BaseFragment
import com.ncs.breeze.ui.dashboard.activity.DashboardActivity
import com.ncs.breeze.ui.dashboard.fragments.viewmodel.FeedbackViewModel
import timber.log.Timber
import javax.inject.Inject


class FeedbackFragment : BaseFragment<FragmentFeedbackBinding, FeedbackViewModel>() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private var routeIssue: Boolean = false
    private var voiceIssue: Boolean = false
    private var parkingIssue: Boolean = false
    private var erpIssue: Boolean = false
    private var uiIssue: Boolean = false
    private var alertIssue: Boolean = false
    private var description: String = ""
    var currentFocusedView: View? = null

    var mapViewInstance: MapView? = null
    var encodedScreenshot: String? = null

    override fun onKeyboardVisibilityChanged(isOpen: Boolean) {
        super.onKeyboardVisibilityChanged(isOpen)
        if (isOpen && currentFocusedView != null) {
            scrollToView(currentFocusedView!!)
        }
    }

    private fun scrollToView(currentFocusedView: View) {
        viewBinding.scrollView.post {
            viewBinding.scrollView.scrollTo(
                0,
                getPositionInParent(currentFocusedView)
            )
        }
    }

    private val viewModel: FeedbackViewModel by viewModels {
        viewModelFactory
    }

    companion object {

        const val ROUTE_ISSUE = "RouteIssue"
        const val VOICE_ISSUE = "VoiceIssue"
        const val PARKING_ISSUE = "ParkingIssue"
        const val ERP_ISSUE = "ERPIssue"
        const val UI_ISSUE = "UI Issue"
        const val ALERT_ISSUE = "TrafficIssue"

    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initialize()
    }

    private fun initialize() {

        viewBinding.btnSubmit.setBGGrey()
        viewBinding.ivRouteImg.setOnClickListener {
            Analytics.logClickEvent(Event.ROUTE_CLICK, getScreenName())
            routeIssue = !routeIssue
            updateUI()
        }
        viewBinding.ivVoiceImg.setOnClickListener {
            Analytics.logClickEvent(Event.VOICE_CLICK, getScreenName())
            voiceIssue = !voiceIssue
            updateUI()
        }
        viewBinding.ivErpImg.setOnClickListener {
            Analytics.logClickEvent(Event.ERP_CLICK, getScreenName())
            erpIssue = !erpIssue
            updateUI()
        }
        viewBinding.ivParkingImg.setOnClickListener {
            Analytics.logClickEvent(Event.PARKING_CLICK, getScreenName())
            parkingIssue = !parkingIssue
            updateUI()
        }
        viewBinding.ivUiImg.setOnClickListener {
            Analytics.logClickEvent(Event.INTERFACE_CLICK, getScreenName())
            uiIssue = !uiIssue
            updateUI()
        }
        viewBinding.ivAlertImg.setOnClickListener {
            Analytics.logClickEvent(Event.TRAFFIC_CLICK, getScreenName())
            alertIssue = !alertIssue
            updateUI()
        }
        viewBinding.btnClose.setOnClickListener {
            Analytics.logClickEvent(Event.CLOSE, getScreenName())
            (activity as BaseActivity<*, *>).hideKeyboard()
            onBackPressed()
        }
        viewBinding.etInput.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                setFocusedView(v)
            }
        }
        viewBinding.etInput.filters =
            arrayOf<InputFilter>(InputFilter.LengthFilter(Constants.FEEDBACK_DESCRIPTION_LENGTH))
        viewBinding.etInput.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

                // TODO Auto-generated method stub
            }

            override fun afterTextChanged(s: Editable) {
                // TODO Auto-generated method stub
                if (s.length <= Constants.FEEDBACK_DESCRIPTION_LENGTH) {
                    description = s.toString()
                } else {
                    description = s.substring(0, Constants.FEEDBACK_DESCRIPTION_LENGTH)
                }
                Analytics.logClickEvent(Event.DESCRIPTION_CLICK, getScreenName())
            }

        })

        viewBinding.btnReportOthers.setOnClickListener {
            Analytics.logClickEvent(Event.INSTABUG_CLICK, getScreenName())
            Instabug.show()
        }
    }

    private fun updateUI() {
        if (routeIssue) viewBinding.ivRouteImg.setImageResource(R.drawable.feedback_route_selected) else viewBinding.ivRouteImg.setImageResource(
            R.drawable.feedback_route_unselected
        )
        if (voiceIssue) viewBinding.ivVoiceImg.setImageResource(R.drawable.feedback_voice_selected) else viewBinding.ivVoiceImg.setImageResource(
            R.drawable.feedback_voice_unselected
        )
        if (parkingIssue) viewBinding.ivParkingImg.setImageResource(R.drawable.feedback_parking_selected) else viewBinding.ivParkingImg.setImageResource(
            R.drawable.feedback_parking_unselected
        )
        if (erpIssue) viewBinding.ivErpImg.setImageResource(R.drawable.feedback_erp_selected) else viewBinding.ivErpImg.setImageResource(
            R.drawable.feedback_erp_unselected
        )
        if (uiIssue) viewBinding.ivUiImg.setImageResource(R.drawable.feedback_ui_selected) else viewBinding.ivUiImg.setImageResource(
            R.drawable.feedback_ui_unselected
        )
        if (alertIssue) viewBinding.ivAlertImg.setImageResource(R.drawable.feedback_alert_selected) else viewBinding.ivAlertImg.setImageResource(
            R.drawable.feedback_alert_unselected
        )

        if (routeIssue || voiceIssue || parkingIssue || erpIssue || uiIssue || alertIssue) {
            viewBinding.btnSubmit.setBGNormal()
            submitListener()
        } else {
            viewBinding.btnSubmit.setBGGrey()
            viewBinding.btnSubmit.setOnClickListener(null)
        }

    }

    override fun inflateViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?,
        attachToContainer: Boolean
    ) = FragmentFeedbackBinding.inflate(inflater, container, attachToContainer)

    override fun getViewModelReference(): FeedbackViewModel {
        return viewModel
    }

    fun hideKeyboard() {
        (activity as BaseActivity<*, *>).hideKeyboard()
    }

    override fun getScreenName(): String {
        return Screen.REPORT_ISSUE
    }

    fun getFeedbackRequest(location: Location): FeedbackRequest {

        var request = FeedbackRequest()

        if (routeIssue) {
            request.type.add(
                TypeItem(
                    ROUTE_ISSUE,
                    location.latitude.toString(),
                    location.longitude.toString()
                )
            )
        }

        if (voiceIssue) {
            request.type.add(
                TypeItem(
                    VOICE_ISSUE,
                    location.latitude.toString(),
                    location.longitude.toString()
                )
            )
        }

        if (parkingIssue) {
            request.type.add(
                TypeItem(
                    PARKING_ISSUE,
                    location.latitude.toString(),
                    location.longitude.toString()
                )
            )
        }

        if (uiIssue) {
            request.type.add(
                TypeItem(
                    UI_ISSUE,
                    location.latitude.toString(),
                    location.longitude.toString()
                )
            )
        }

        if (erpIssue) {
            request.type.add(
                TypeItem(
                    ERP_ISSUE,
                    location.latitude.toString(),
                    location.longitude.toString()
                )
            )
        }

        if (alertIssue) {
            request.type.add(
                TypeItem(
                    ALERT_ISSUE,
                    location.latitude.toString(),
                    location.longitude.toString()
                )
            )
        }

        request.issuedescription = description
        return request
    }

    fun submitListener() {
        viewBinding.btnSubmit.setOnClickListener {
            Analytics.logClickEvent(Event.SUBMIT_CLICK, getScreenName())

            var currentLocation: Location? = null

            if (requireActivity() is DashboardActivity) {
                currentLocation = LocationBreezeManager.getInstance().currentLocation
                mapViewInstance = (requireActivity() as DashboardActivity).getMapViewInstance()

                encodedScreenshot = (requireActivity() as DashboardActivity).getSnapshotImage()
            }

            if (currentLocation != null) {
                var request = getFeedbackRequest(currentLocation)

                viewModel.sendFeedbackSuccessful.observe(viewLifecycleOwner) {
                    viewModel.sendFeedbackSuccessful.removeObservers(this)
                    // Show success screen

                    if (routeIssue || voiceIssue) {
                        // Send Mapbox feedback
                        //Timber.d(Constants.TAGS.SEARCH_TAG, "Printing encoded image: ${encodedScreenshot}")
                        //Timber.d("Feedback - The encoded image is: $encodedScreenshot")

                        if (routeIssue) {
                            postUserFeedback(
                                request.issuedescription,
                                encodedImage = encodedScreenshot!!,
                                FeedbackEvent.ROUTING_ERROR
                            )
                        }

                        if (voiceIssue) {
                            postUserFeedback(
                                request.issuedescription,
                                encodedImage = encodedScreenshot!!,
                                FeedbackEvent.INCORRECT_AUDIO_GUIDANCE
                            )
                        }
                    }

                    viewBinding.feedbackView.visibility = View.GONE
                    viewBinding.successView.visibility = View.VISIBLE
                }
                viewModel.sendFeedback(request)
            }

        }
    }

    /** Returns array of views [x, y] position relative to parent  */
    private fun getPositionInParent(view: View): Int {
        val relativePosition = intArrayOf(view.left, view.top)
        var currentParent = view.parent as ViewGroup
        while (currentParent !== viewBinding.scrollView) {
            relativePosition[0] += currentParent.left
            relativePosition[1] += currentParent.top
            currentParent = currentParent.parent as ViewGroup
        }
        return relativePosition[1]
    }

    fun setFocusedView(focusedView: View?) {
        currentFocusedView = focusedView
    }

    fun postUserFeedback(
        description: String,
        encodedImage: String,
        @FeedbackEvent.Type feedbackType: String
    ) {
        Timber.d("Feedback - Posting feedback $feedbackType")
        MapboxNavigationApp.current()?.postUserFeedback(
            feedbackType,
            description,
            FeedbackEvent.REROUTE,
            encodedImage,
            null
        )
    }
}
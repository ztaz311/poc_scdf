package com.ncs.breeze.car.breeze.screen.needlogin

import android.content.Intent
import android.graphics.Typeface
import android.text.Spannable
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.text.style.StyleSpan
import android.util.Log
import androidx.car.app.ScreenManager
import androidx.car.app.model.Action
import androidx.car.app.model.CarIcon
import androidx.car.app.model.MessageTemplate
import androidx.car.app.model.Template
import androidx.core.graphics.drawable.IconCompat
import com.mapbox.navigation.core.lifecycle.MapboxNavigationApp
import com.mapbox.navigation.core.trip.session.TripSessionState
import com.ncs.breeze.R
import com.ncs.breeze.car.breeze.MainBreezeCarContext
import com.ncs.breeze.car.breeze.base.BaseScreenCar
import com.ncs.breeze.car.breeze.utils.BreezeUserPreferencesUtil
import com.ncs.breeze.car.breeze.utils.LimitClick
import com.ncs.breeze.car.breeze.utils.setClickSafe
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.Event
import com.ncs.breeze.common.analytics.Screen
import com.ncs.breeze.common.model.rx.AppSessionReadyData
import com.ncs.breeze.common.model.rx.AppSessionReadyEvent
import com.ncs.breeze.common.model.rx.ToCarEventData
import com.ncs.breeze.common.event.ToCarRx
import com.ncs.breeze.common.utils.AWSUtils
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class NeedsLoginScreen(
    val mainCarContext: MainBreezeCarContext,
    val intent: Intent? = null
) : BaseScreenCar(mainCarContext.carContext) {

    var loginCheckCompleted = false

    override fun onGetTemplate(): Template {
        val message: CharSequence
        if (loginCheckCompleted) {
            message = SpannableStringBuilder()
            val noticePrefix = carContext.getString(R.string.need_login_precaution)
            val noticeMessage = carContext.getString(R.string.need_login)
            val noticeMessageSpannable = SpannableString(noticeMessage)
            val boldSpan = StyleSpan(Typeface.BOLD)
            noticeMessageSpannable.setSpan(
                boldSpan,
                0,
                noticeMessage.length,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
            )
            message.append(noticePrefix)
            message.append(" ")
            message.append(noticeMessageSpannable)

        } else {
            message = "Welcome to Breeze car app!"
        }
        val builder = MessageTemplate.Builder(message)
        builder.setIcon(
            CarIcon.Builder(
                IconCompat.createWithResource(
                    carContext,
                    R.drawable.ic_app_icon
                )
            ).build()
        )
        if (loginCheckCompleted) {
            builder.addAction(
                Action.Builder()
                    .setTitle(carContext.getString(R.string.action_retry))
                    .setClickSafe {
                        LimitClick.handleSafe {
                            Analytics.logClickEvent(Event.AA_CARCONNECT, Screen.AA_SIGNIN_SCREEN)
                            checkUserAuth()
                        }
                    }.build()
            )
        }
        builder.setTitle(carContext.getString(R.string.app_name))
        return builder.build()
    }

    override fun getScreenName(): String {
        return Screen.AA_SIGNIN_SCREEN
    }

    override fun onStartScreen() {
        super.onStartScreen()
        checkUserAuth()
        triggerAutoLoginCheckTimer()
    }

    private fun triggerAutoLoginCheckTimer() {
        CoroutineScope(Dispatchers.IO).launch {
            delay(3000)
            loginCheckCompleted = true
            withContext(Dispatchers.Main) {
                invalidate()
            }
        }
    }

    private fun checkUserAuth() {
        AWSUtils.checkUserAuthSession(object : AWSUtils.AuthSessionCallback {
            override fun onAuthSuccess(token: String?) {
                CoroutineScope(Dispatchers.Main).launch {
                    mainCarContext.phoneAppLoggedIn = true
                    try {
                        val screenManager: ScreenManager =
                            carContext.getCarService(ScreenManager::class.java)
                        MapboxNavigationApp.current()
                            ?.takeIf { mapboxNavigation ->
                                screenManager.top is NeedsLoginScreen
                                        && mapboxNavigation.getTripSessionState() != TripSessionState.STARTED
                            }?.let {
                                var hasUserName = false
                                kotlin.runCatching {
                                    hasUserName =
                                        BreezeUserPreferencesUtil.breezeUserPreferenceUtil.retrieveUserName()
                                            .isNotEmpty()
                                }
                                if (hasUserName) {
                                    ToCarRx.postEventWithCacheLast(
                                        AppSessionReadyEvent(
                                            AppSessionReadyData(true),
                                            intent
                                        )
                                    )
                                }
                            }
                    } catch (e: Exception) {
                        Log.e("NeedsLogin", "Error while opening home screen in Android auto")
                    }
                }
            }

            override fun onAuthFailed() {
                CoroutineScope(Dispatchers.Main).launch {
                    loginCheckCompleted = true
                    invalidate()
                }
            }
        })
    }

    override fun onScreenUpdateEvent(data: ToCarEventData?) {
        super.onScreenUpdateEvent(data)
        if (data is AppSessionReadyData) {
            mainCarContext.phoneAppLoggedIn = true
            var screenManager: ScreenManager =
                carContext.getCarService(ScreenManager::class.java)
            try {
                if (screenManager.top is NeedsLoginScreen) {
                    ToCarRx.postEventWithCacheLast(
                        AppSessionReadyEvent(
                            data,
                            intent
                        )
                    )
                }
            } catch (e: Exception) {
                Log.e("NeedsLogin", "Error while opening home screen in Android auto")
            }
        } else {
            loginCheckCompleted = true
            invalidate()
        }
    }
}

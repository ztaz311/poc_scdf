package com.ncs.breeze.ui.walkathon

import android.animation.Animator
import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.*
import android.content.res.Configuration
import android.content.res.Resources
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.location.Location
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.Html
import android.util.TypedValue
import android.view.View
import android.view.ViewTreeObserver
import android.view.WindowManager
import android.view.animation.DecelerateInterpolator
import android.widget.ImageView
import android.widget.TextView
import androidx.activity.viewModels
import androidx.annotation.Keep
import androidx.appcompat.app.AlertDialog
import androidx.core.animation.addListener
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.core.text.isDigitsOnly
import androidx.core.view.ViewCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.facebook.react.bridge.Arguments
import com.facebook.react.bridge.WritableMap
import com.mapbox.android.gestures.MoveGestureDetector
import com.mapbox.android.gestures.StandardScaleGestureDetector
import com.mapbox.api.directions.v5.DirectionsCriteria
import com.mapbox.api.directions.v5.MapboxDirections
import com.mapbox.api.directions.v5.models.DirectionsResponse
import com.mapbox.api.directions.v5.models.RouteOptions
import com.mapbox.geojson.Point
import com.mapbox.maps.*
import com.mapbox.maps.extension.style.expressions.dsl.generated.interpolate
import com.mapbox.maps.extension.style.layers.generated.LineLayer
import com.mapbox.maps.extension.style.layers.getLayer
import com.mapbox.maps.extension.style.layers.getLayerAs
import com.mapbox.maps.plugin.LocationPuck2D
import com.mapbox.maps.plugin.animation.MapAnimationOptions
import com.mapbox.maps.plugin.animation.camera
import com.mapbox.maps.plugin.animation.easeTo
import com.mapbox.maps.plugin.compass.compass
import com.mapbox.maps.plugin.gestures.GesturesPlugin
import com.mapbox.maps.plugin.gestures.OnMoveListener
import com.mapbox.maps.plugin.gestures.OnScaleListener
import com.mapbox.maps.plugin.gestures.gestures
import com.mapbox.maps.plugin.locationcomponent.LocationComponentConstants
import com.mapbox.maps.plugin.locationcomponent.LocationComponentPlugin
import com.mapbox.maps.plugin.locationcomponent.OnIndicatorPositionChangedListener
import com.mapbox.maps.plugin.locationcomponent.location
import com.mapbox.maps.plugin.logo.logo
import com.mapbox.navigation.base.ExperimentalPreviewMapboxNavigationAPI
import com.mapbox.navigation.base.TimeFormat
import com.mapbox.navigation.base.extensions.applyDefaultNavigationOptions
import com.mapbox.navigation.base.extensions.coordinates
import com.mapbox.navigation.base.options.NavigationOptions
import com.mapbox.navigation.base.route.NavigationRoute
import com.mapbox.navigation.base.route.NavigationRouterCallback
import com.mapbox.navigation.base.route.RouterFailure
import com.mapbox.navigation.base.route.RouterOrigin
import com.mapbox.navigation.base.trip.model.RouteLegProgress
import com.mapbox.navigation.base.trip.model.RouteProgress
import com.mapbox.navigation.core.directions.session.RoutesObserver
import com.mapbox.navigation.core.lifecycle.MapboxNavigationApp
import com.mapbox.navigation.core.trip.session.LocationMatcherResult
import com.mapbox.navigation.core.trip.session.LocationObserver
import com.mapbox.navigation.core.trip.session.RouteProgressObserver
import com.mapbox.navigation.core.trip.session.TripSessionState
import com.mapbox.navigation.ui.maps.camera.NavigationCamera
import com.mapbox.navigation.ui.maps.camera.data.MapboxNavigationViewportDataSource
import com.mapbox.navigation.ui.maps.camera.lifecycle.NavigationBasicGesturesHandler
import com.mapbox.navigation.ui.maps.location.NavigationLocationProvider
import com.mapbox.navigation.ui.maps.route.arrow.api.MapboxRouteArrowApi
import com.mapbox.navigation.ui.maps.route.arrow.api.MapboxRouteArrowView
import com.mapbox.navigation.ui.maps.route.arrow.model.RouteArrowOptions
import com.mapbox.navigation.ui.maps.route.line.api.MapboxRouteLineApi
import com.mapbox.navigation.ui.maps.route.line.api.MapboxRouteLineView
import com.mapbox.navigation.ui.maps.route.line.model.MapboxRouteLineOptions
import com.mapbox.navigation.ui.maps.route.line.model.RouteLineResources
import com.mapbox.navigation.ui.tripprogress.api.MapboxTripProgressApi
import com.mapbox.navigation.ui.tripprogress.model.*
import com.mapbox.navigation.ui.utils.internal.ifNonNull
import com.mapbox.navigation.utils.internal.toPoint
import com.mapbox.turf.TurfConstants
import com.mapbox.turf.TurfMeasurement
import com.ncs.breeze.BuildConfig
import com.ncs.breeze.R
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.Event
import com.ncs.breeze.common.analytics.Param
import com.ncs.breeze.common.analytics.Screen
import com.ncs.breeze.common.appSync.ClientFactory
import com.ncs.breeze.common.components.maplayer.WalkingRouteLineLayer.Companion.addDashingStyleToLine
import com.ncs.breeze.common.constant.MapIcon
import com.ncs.breeze.common.event.ToCarRx.postEventWithCacheLast
import com.breeze.model.extensions.decompressToDirectionsResponse
import com.ncs.breeze.common.extensions.mapbox.setBreezeDefaultOptions
import com.breeze.model.extensions.toBitmap
import com.breeze.model.extensions.visibleAmount
import com.breeze.model.DestinationAddressDetails
import com.breeze.model.DestinationAddressTypes
import com.breeze.model.NavigationZone
import com.breeze.model.TripType
import com.breeze.model.enums.AmenityBand
import com.ncs.breeze.common.model.rx.AppToNavigationEndEvent
import com.ncs.breeze.common.remote.ApiHelper
import com.ncs.breeze.common.rxbus.RxBus
import com.ncs.breeze.common.rxbus.RxEvent
import com.ncs.breeze.common.service.HorizonObserverFillterZoneUtils
import com.ncs.breeze.common.utils.*
import com.breeze.model.extensions.safeDouble
import com.ncs.breeze.common.utils.eta.ETAEngine
import com.breeze.model.enums.ETAMode
import com.ncs.breeze.common.utils.mapStyle.BreezeNavigationMapStyle
import com.ncs.breeze.common.utils.walkathontracker.WalkathonEngine
import com.ncs.breeze.components.navigationlog.HistoryRecorder
import com.breeze.customization.view.BreezeButton
import com.breeze.model.constants.Constants
import com.breeze.model.extensions.dpToPx
import com.ncs.breeze.databinding.ActivityWalkingBinding
import com.ncs.breeze.reactnative.nativemodule.ReactNativeEventEmitter
import com.ncs.breeze.ui.base.BaseMapActivity
import com.ncs.breeze.ui.dashboard.fragments.view.FeedbackFragment
import com.ncs.breeze.ui.navigation.NavigationViewModel
import com.ncs.breeze.ui.navigation.TurnByTurnAmenityViewManager
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.coroutines.*
import timber.log.Timber
import javax.inject.Inject


@Keep
class WalkingActivity :
    BaseMapActivity<ActivityWalkingBinding, NavigationViewModel>() {

    companion object {

        private const val FOLLOW_MAX_ZOOM = 16.5
        private const val FOLLOW_MIN_ZOOM = 16.5
        private const val INITIAL_ZOOM = 16.5
        private const val INITIAL_PITCH = 0.0
    }


    private var handlePostDelayHideArrivalEstimate: Handler? = null
    private lateinit var routeInitJob: Job

    private var mRoute: NavigationRoute? = null

    /**
     * this variable for check if activity is become stop or resume
     */
    private var isOnForeground: Boolean = false

    private var displayedNavigationZone: NavigationZone? = null

    private var arrivedWalkathonDialog: Dialog? = null
    private var stopTripConfirmationDialog: Dialog? = null
    private lateinit var ratingDialog: Dialog
    private var isRatingDialogDismissed: Boolean = false
    private var showWalkingPathDialogOnResume: Boolean = false
    private var showArrivedWalkathonDialogOnResume: Boolean = false

    @Inject
    lateinit var breezeFeatureDetectionUtil: BreezeFeatureDetectionUtil

    @Inject
    lateinit var breezeUserPreferenceUtil: BreezeUserPreferenceUtil


    private lateinit var mapboxMap: MapboxMap
    private lateinit var locationComponent: LocationComponentPlugin
    private lateinit var navigationCamera: NavigationCamera
    private lateinit var viewportDataSource: MapboxNavigationViewportDataSource
    private lateinit var tripProgressApi: MapboxTripProgressApi
    private lateinit var walkingPathDialog: Dialog


    private var maxDistance: Int = 0
    private var curTripId: String? = null

    @Inject
    lateinit var breezeCalendarUtil: BreezeCalendarUtil

    @Inject
    lateinit var breezeTripLoggingUtil: BreezeTripLoggingUtil

    @Inject
    lateinit var breezeStatisticsUtil: BreezeStatisticsUtil

    @Inject
    lateinit var breezeHistoryRecorderUtil: BreezeHistoryRecorderUtil

    @Inject
    lateinit var apiHelper: ApiHelper

    private var timeArrival: String = ""

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val turnbyturnViewModel: NavigationViewModel by viewModels {
        viewModelFactory
    }
    private lateinit var originalDestinationDetails: DestinationAddressDetails
    private lateinit var currentDestinationDetails: DestinationAddressDetails
    private var walkingRoute: NavigationRoute? = null
    private var isWalking: Boolean = false
    private var selectedCarParkIconRes: Int = -1

    private var navAmenityLayerManager: TurnByTurnAmenityViewManager? = null

    var isEndingNavigationOnResume: Boolean = false

    private lateinit var locationObserver: LocationObserver

    private lateinit var currentDirectionsRoute: NavigationRoute

    var isSetTimeArriver = true

    private var routeLineAPI: MapboxRouteLineApi? = null
    private var routeLineView: MapboxRouteLineView? = null
    private var routeArrowView: MapboxRouteArrowView? = null
    private val routeArrowAPI: MapboxRouteArrowApi = MapboxRouteArrowApi()
    private val navigationLocationProvider = NavigationLocationProvider()
    private val pixelDensity = Resources.getSystem().displayMetrics.density
    private val overviewEdgeInsets: EdgeInsets by lazy {
        EdgeInsets(
            mapboxMap.getSize().height.toDouble() * 1.0 / 4.0,
            40.0 * pixelDensity,
            40.0 * pixelDensity,
            40.0 * pixelDensity
        )
    }
    private val landscapeOverviewEdgeInsets: EdgeInsets by lazy {
        EdgeInsets(
            20.0 * pixelDensity,
            mapboxMap.getSize().width.toDouble() / 1.75,
            20.0 * pixelDensity,
            20.0 * pixelDensity
        )
    }

    private lateinit var mapboxHistoryRecorder: HistoryRecorder
    lateinit var autoRecenterMapHandler: Handler

    var isCameraTrackingDismissed: Boolean = false

    private val onMoveListener = object : OnMoveListener {
        override fun onMoveBegin(detector: MoveGestureDetector) {
            onCameraTrackingDismissed()
        }

        override fun onMove(detector: MoveGestureDetector): Boolean {
            return false
        }

        override fun onMoveEnd(detector: MoveGestureDetector) {}
    }

    override fun inflateViewBinding() = ActivityWalkingBinding.inflate(layoutInflater)

    private val mapMatcherResultObserver = object : LocationObserver {

        override fun onNewLocationMatcherResult(locationMatcherResult: LocationMatcherResult) {
            Timber.d("LocationObserver: Map matcher result observer called" + locationMatcherResult.enhancedLocation.toString())
            handleRowLocation(
                rawLocation = locationMatcherResult.enhancedLocation,
                isTeleport = false
            )
        }

        override fun onNewRawLocation(rawLocation: Location) {
            Timber.d(" -- onNewRawLocation --")
//            handleRowLocation(rawLocation, false)
        }

        fun handleRowLocation(rawLocation: Location, isTeleport: Boolean) {
            val transitionOptions: (ValueAnimator.() -> Unit)? =
                if (isTeleport) {
                    {
                        duration = 0
                    }
                } else {
                    {
                        duration = 1000
                    }
                }
            navigationLocationProvider.changePosition(
                rawLocation,
                emptyList(),
                latLngTransitionOptions = transitionOptions,
                bearingTransitionOptions = transitionOptions
            )
            viewportDataSource.onLocationChanged(rawLocation)
            viewportDataSource.evaluate()

            Timber.d("Map matcher result observer exited")
        }
    }


    val scope: CoroutineScope = CoroutineScope(SupervisorJob() + Dispatchers.Main.immediate);

    private val routesObserver = RoutesObserver { result ->
        Timber.d("REROUTE - Routes observer called ${result.navigationRoutes.size}")
        if (result.navigationRoutes.isNotEmpty()) {
            showWalkingPathReReroutingMessage()
            scope.launch(Dispatchers.Main) {
                navAmenityLayerManager?.currentRoute = result.navigationRoutes[0]
                routeLineAPI?.setNavigationRoutes(listOf(result.navigationRoutes[0])) { value ->
                    ifNonNull(routeLineView, mapboxMap.getStyle()) { view, style ->
                        view.renderRouteDrawData(style, value)
                        handleRouteLineRefresh()
                    }
                }
            }
            currentDirectionsRoute = result.navigationRoutes[0]

            hideWalkingPathReReroutingMessage()
        } else {
            viewportDataSource.clearRouteData()
            updateCameraToIdle()
            clearRouteLine()
        }
        Timber.d("REROUTE - Routes observer exited")
    }

    private val onIndicatorPositionChangedListener = OnIndicatorPositionChangedListener { point ->
        val result = routeLineAPI?.updateTraveledRouteLine(point)
        mapboxMap.getStyle()?.apply {
            // Render the result to update the map.
            if (result != null) {
                routeLineView?.renderRouteLineUpdate(this, result!!)
                handleRouteLineRefresh()
            }
        }
    }

    private val routeProgressObserver = RouteProgressObserver { routeProgress ->
        navAmenityLayerManager?.waypointsRemaining = routeProgress.remainingWaypoints

        Timber.d("Route progress change observer called")
        routeArrowAPI.addUpcomingManeuverArrow(routeProgress).apply {
            ifNonNull(routeArrowView, mapboxMap.getStyle()) { view, style ->
                view.renderManeuverUpdate(style, this)
            }
        }
        if (::tripProgressApi.isInitialized) {
            Timber.d("Rendering of bottom panel information")
            render(routeProgress)
        }

        routeLineAPI?.updateWithRouteProgress(routeProgress) { result ->
            mapboxMap.getStyle()?.apply {
                routeLineView?.renderRouteLineUpdate(this, result)
                handleRouteLineRefresh()
            }
        }
        Timber.d("Route progress change observer exited")
    }

    private fun showWalkingPathReReroutingMessage() {
        CoroutineScope(Dispatchers.Main).launch {
            viewBinding.txtRerouting.visibility = View.VISIBLE
        }
    }

    private fun hideWalkingPathReReroutingMessage() {
        CoroutineScope(Dispatchers.Main).launch {
            viewBinding.txtRerouting.visibility = View.GONE
        }
    }

    private fun setUpGestureListeners() {
        getGesturesPlugin().addOnMoveListener(onMoveListener)
    }

    private fun getGesturesPlugin(): GesturesPlugin {
        return viewBinding.mapViewNav.gestures
    }

    private fun render(routeProgress: RouteProgress) {
        val tripProgress = tripProgressApi.getTripProgress(routeProgress)
        Timber.d("Rendering trip progress")
        if (maxDistance < tripProgress.distanceRemaining.toInt()) {
            maxDistance = tripProgress.distanceRemaining.toInt()
        }
        val timeRemaining =
            tripProgress.formatter.getTimeRemaining(tripProgress.totalTimeRemaining).toString()
                .split(
                    " "
                )

        if (isSetTimeArriver) {
            val timeToArrivalFormatter = EstimatedTimeToArrivalFormatter(
                this,
                TimeFormat.TWELVE_HOURS
            )
            val time =
                timeToArrivalFormatter.format(tripProgress.estimatedTimeToArrival).toString().split(
                    " "
                )
            timeArrival = time[0] + time[1]
            viewBinding.estimatedTimeToArriveText.text = time[0]
            viewBinding.timeToArrivalUnit.text = time[1]
        }

        if (!timeRemaining[0].isDigitsOnly()) {
            updateArrivalIconAndMessage()
        }

        Timber.d("Rendering trip progress done")
    }

    private fun updateArrivalIconAndMessage() {
        viewBinding.arrivalNotification.bottomSheetHdr.setAutoSizeTextTypeWithDefaults(TextView.AUTO_SIZE_TEXT_TYPE_NONE)
        viewBinding.arrivalNotification.bottomSheetHdr.setTextSize(
            TypedValue.COMPLEX_UNIT_DIP,
            32.0f
        )
        viewBinding.arrivalNotification.bottomSheetHdr.text = getString(R.string.we_have_arrived)
        viewBinding.arrivalNotification.arrivedAddressButton.visibility = View.VISIBLE
        viewBinding.arrivalNotification.arrivedAddressButton.setImageResource(R.drawable.saved_place)

    }

    private var isArrived: Boolean = false

    @ExperimentalPreviewMapboxNavigationAPI
    private fun showArrivedNotification(tripId: String?) {
        Analytics.logNotificationEvents(
            Event.NATURAL_END_DONE,
            Event.NOTIFICATION_OPEN,
            getScreenName()
        )
        viewBinding.arrivalNotification.root.visibility = View.VISIBLE

        updateArrivalIconAndMessage()

        viewBinding.recenterButton.visibility = View.GONE

        Analytics.logNotificationEvents(
            Event.NATURAL_END_DONE,
            Event.NOTIFICATION_CLOSE,
            getScreenName()
        )

        //hide carparks
        navAmenityLayerManager?.removeCarParks()

        MapboxNavigationApp.current()?.let { mapboxNavigation ->
            mapboxNavigation.setRerouteController(null)
            mapboxNavigation.unregisterRoutesObserver(routesObserver)
            mapboxNavigation.unregisterRouteProgressObserver(routeProgressObserver)
            if (::mapboxHistoryRecorder.isInitialized) {
                mapboxHistoryRecorder.stopRecording()
                mapboxNavigation.unregisterTripSessionStateObserver(mapboxHistoryRecorder)
            }
            BreezeArrivalObserver.getInstance()?.unregisterNavigationArrivalObserver(
                navigationArrivalObserver,
                shouldStopTrip = false
            )
        }

        viewportDataSource.clearRouteData()
        updateCameraToFollowing()
        clearRouteLine()
        handleRouteLineRefresh()

        if (walkingRoute != null && !isWalking) {
            viewBinding.rlDestination.visibility = View.GONE
            showWalkingPathDialog()
        }

        if (isWalking && currentDestinationDetails.trackNavigation) {
            initiateNavigationClose { isApiSuccess ->
                if (isApiSuccess) {
                    runOnUiThread {
                        showArrivedWalkathonDialog()
                    }
                }
            }
        }
    }

    private fun setActivityResult() {
        Timber.d("Activity result has been set")
        Intent().apply {
            putExtras(
                bundleOf(
                    Constants.NAVIGATION_END to true,
                    // This activity is for Tempanies walking, so we all way refresh ExploreMap when going back
                    Constants.NAVIGATION_END_REFRESH_MAP to true,
                )
            )
        }.let {
            setResult(Activity.RESULT_OK, it)
        }
        finish()
    }

    private fun showArrivedWalkathonDialog() {
        if (baseContext == null) return

        if (!isOnForeground) {
            showArrivedWalkathonDialogOnResume = true
            return
        }

        showArrivedWalkathonDialogOnResume = false

        Dialog(this@WalkingActivity).apply {
            setCancelable(false)
            setContentView(R.layout.dialog_arrived)
            window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            setCanceledOnTouchOutside(false)
            findViewById<BreezeButton>(R.id.btn_go).setOnClickListener {
                setActivityResult()
            }
            findViewById<TextView>(R.id.message).text =
                "You have arrived at ${currentDestinationDetails.address1}"
        }.run {
            if (isOnForeground) {
                if (stopTripConfirmationDialog?.isShowing == true) {
                    stopTripConfirmationDialog?.dismiss()
                }
                show()
            }
        }
    }

    //FIXME: app can crash when session is restored in turnbyturn
    // fixed crash issue with ETAEngine, but it will disable eta when this happens
    @ExperimentalPreviewMapboxNavigationAPI
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewBinding.root.keepScreenOn = true
        Timber.d("TurnByTurnNavigation onCreate is called")
        val intent = intent
        val args = intent.getBundleExtra(Constants.NAVIGATION_BUNDLE)

        if (args != null && args.containsKey(Constants.NAVIGATION_BUNDLE_DESTINATION_DETAILS)) {
            currentDestinationDetails =
                args.getSerializable(Constants.NAVIGATION_BUNDLE_DESTINATION_DETAILS) as DestinationAddressDetails

            originalDestinationDetails =
                args.getSerializable(Constants.NAVIGATION_BUNDLE_ORIGINAL_DESTINATION_DETAILS) as DestinationAddressDetails

            val compressedDirectionsResponse =
                args.getByteArray(Constants.NAVIGATION_BUNDLE_ROUTE_RESPONSE)!!

            val directionsRouteOptions =
                args.getSerializable(Constants.NAVIGATION_BUNDLE_ROUTE_OPTIONS) as RouteOptions

            selectedCarParkIconRes =
                args.getInt(Constants.NAVIGATION_BUNDLE_SELECTED_CARPARK_ICON_RES, -1)

            val selectedRouteIndex =
                args.getInt(Constants.NAVIGATION_BUNDLE_SELECTED_ROUTE_INDEX)

            val skipDriving =
                args.getBoolean(Constants.NAVIGATION_BUNDLE_SKIP_DRIVING, false)

            if (args.containsKey(Constants.NAVIGATION_BUNDLE_WALKING_ROUTE_DIRECTION)) {
                val directionsRoute =
                    args.getSerializable(Constants.NAVIGATION_BUNDLE_WALKING_ROUTE_DIRECTION) as DirectionsResponse
                val routeOptions =
                    args.getSerializable(Constants.NAVIGATION_BUNDLE_WALKING_ROUTE_ROUTE_OPTIONS) as RouteOptions
                val walkingRoutes = NavigationRoute.create(directionsRoute, routeOptions)
                if (!walkingRoutes.isEmpty()) {
                    walkingRoute = walkingRoutes[0]
                }
            }

            mapboxMap = viewBinding.mapViewNav.getMapboxMap()

            populateArrivalView()

            viewBinding.mapViewNav.setBreezeDefaultOptions()

            viewBinding.rlDestination.viewTreeObserver.addOnGlobalLayoutListener(object :
                ViewTreeObserver.OnGlobalLayoutListener {
                override fun onGlobalLayout() {
                    viewBinding.rlDestination.viewTreeObserver.removeOnGlobalLayoutListener(this)
                    viewBinding.mapViewNav.logo.marginBottom =
                        viewBinding.rlDestination.height  + 24.0f.dpToPx()
                }
            })

            locationComponent = viewBinding.mapViewNav.location.apply {
                this.locationPuck = LocationPuck2D(
                    bearingImage = ContextCompat.getDrawable(
                        this@WalkingActivity,
                        R.drawable.ic_navigaion_puck
                    ), scaleExpression = interpolate {
                        linear()
                        zoom()
                        stop {
                            literal(0.0)
                            literal(0.6)
                        }
                        stop {
                            literal(20.0)
                            literal(1.0)
                        }
                    }.toJson()
                ) //AAL
                pulsingEnabled = true
                pulsingColor = getColor(R.color.themed_nav_pulse_color)
                pulsingMaxRadius = 50.0F
                setLocationProvider(navigationLocationProvider)
                addOnIndicatorPositionChangedListener(onIndicatorPositionChangedListener)
                enabled = true
            }
            viewportDataSource = MapboxNavigationViewportDataSource(
                viewBinding.mapViewNav.getMapboxMap()
            )
            //viewportDataSource.followingPitchPropertyOverride(50.0)
            viewportDataSource.followingPitchPropertyOverride(0.0)
            viewportDataSource.options.followingFrameOptions.maxZoom = FOLLOW_MAX_ZOOM
            viewportDataSource.options.followingFrameOptions.minZoom = FOLLOW_MIN_ZOOM
            navigationCamera = NavigationCamera(
                viewBinding.mapViewNav.getMapboxMap(),
                viewBinding.mapViewNav.camera,
                viewportDataSource
            )
            viewBinding.mapViewNav.camera.addCameraAnimationsLifecycleListener(
                NavigationBasicGesturesHandler(navigationCamera)
            )

            Timber.d("Nav - Before initialising navigation and map style")

            initNavigation()
            updateCameraToCurrentLocation(
                zoom = INITIAL_ZOOM,
                isTeleport = true
            )
            initViews()

            initRouteDetails(
                compressedDirectionsResponse,
                directionsRouteOptions,
                selectedRouteIndex
            )
            init(args, skipDriving)

            if (currentDestinationDetails.destinationAddressType == DestinationAddressTypes.CALENDAR) {
                Timber.d("Chosen destination is Calendar item")
                compositeDisposable.add(
                    RxBus.listen(RxEvent.NotifyCalendarEventDestinationChange::class.java)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe {

                            Timber.d("changed item observed")
                            val builder: AlertDialog.Builder? = this.let {
                                AlertDialog.Builder(it)
                            }

                            val message = Html.fromHtml(
                                getString(R.string.reroute_message) + "<b>" + it.destinationDestails.destinationName + "</b>",
                                Html.FROM_HTML_MODE_LEGACY
                            )

                            builder!!.setMessage(message)
                                .setPositiveButton(
                                    R.string.proceed_reroute,
                                    DialogInterface.OnClickListener { dialog, which ->

                                        // Setting currentDestinationDetails to new DestinationDetails and proceeding with reroute
                                        currentDestinationDetails = it.destinationDestails
                                        triggerReroute()
                                        breezeCalendarUtil.rerouteToNewLocation(it.destinationDestails)

                                        dialog.dismiss()
                                    })
                                .setNegativeButton(
                                    R.string.cancel_reroute,
                                    DialogInterface.OnClickListener { dialog, which ->
                                        dialog.dismiss()
                                    })

                            val alertDialog: AlertDialog = builder.create()

                            alertDialog.setOnShowListener(DialogInterface.OnShowListener {
                                alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE)
                                    .setTextColor(resources.getColor(R.color.alert_negative_red))
                                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE)
                                    .setTextColor(resources.getColor(R.color.alert_positive_blue))
                                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).isAllCaps = false
                                alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).isAllCaps = false
                            })

                            alertDialog.show()

                        }
                )
                breezeCalendarUtil.navigatingToCalendarEventDestination(currentDestinationDetails)
            }

        }
        Analytics.logScreenView(getScreenName())

        hanle1MinHideEstimateTime()
    }

    private fun hanle1MinHideEstimateTime() {
        viewBinding.sectionTripProgress2.visibility = View.VISIBLE
        handlePostDelayHideArrivalEstimate = Handler()
        handlePostDelayHideArrivalEstimate?.postDelayed({
            if (!isDestroyed) {
                isSetTimeArriver = false
                viewBinding.sectionTripProgress2.visibility = View.GONE
                animateViewToMatchParent(viewBinding.btnNavigationEnd, {})
            }
        }, 60000)
    }


    private fun animateViewToMatchParent(target: View, handler: () -> Unit): Int {
        val fromValueWidth = target.measuredWidth
        val toValueWidth = (target.parent as View).width
        if (isOnForeground) {
            val animator = ValueAnimator.ofInt(fromValueWidth, toValueWidth)
            animator.duration = 250
            animator.interpolator = DecelerateInterpolator()
            animator.addUpdateListener { animation ->
                target.layoutParams.width = animation.animatedValue as Int
                target.requestLayout()
            }
            animator.addListener(
                onEnd = { animator: Animator -> handler.invoke() },
                onStart = {},
                onCancel = {},
                onRepeat = {}
            )
            animator.start()
        } else {
            target.layoutParams.width = toValueWidth
        }
        return fromValueWidth
    }


    private fun initRouteDetails(
        compressedDirectionsResponse: ByteArray,
        directionsRouteOptions: RouteOptions,
        selectedRouteIndex: Int
    ) {
        routeInitJob = scope.launch(Dispatchers.Default) {
            val directionsResponse = compressedDirectionsResponse.decompressToDirectionsResponse()
            //NavigationRoute list returned cannot be empty as this is validated earlier
            mRoute = createNavigationRoute(
                directionsResponse,
                directionsRouteOptions,
                selectedRouteIndex
            )!!
            maxDistance = mRoute?.directionsRoute?.distance()?.toInt() ?: 0
        }
    }

    private fun initRouteObservers(args: Bundle) {

        initNavigationObservers(mRoute)
        startLocationUpdates()

        compositeDisposable.add(
            RxBus.listen(RxEvent.CloseNotifyArrivalScreen::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                })
    }


    private fun createNavigationRoute(
        directionsResponse: DirectionsResponse,
        directionsRouteOptions: RouteOptions,
        selectedRouteIndex: Int?
    ) =
        NavigationRoute.create(directionsResponse, directionsRouteOptions)
            .find { navigationRoute -> navigationRoute.routeIndex == selectedRouteIndex }

    private fun newFeedbackFragment() {
        val args = Bundle()
        addFragmentBottomUp(FeedbackFragment(), args, Constants.TAGS.FEEDBACK_TAG)
    }

    private fun triggerReroute() {
        Timber.d("Calendar event destination has changed")
        // Find routes
        if (getCurrentLocation() != null) {
            findRoute(
                Point.fromLngLat(getCurrentLocation()!!.longitude, getCurrentLocation()!!.latitude),
                Point.fromLngLat(
                    currentDestinationDetails.long!!.toDouble(),
                    currentDestinationDetails.lat!!.toDouble()
                )
            )
        }
    }

    private fun findRoute(originPoint: Point?, destination: Point?) {
        val profile: String = DirectionsCriteria.PROFILE_WALKING
        val routeOptions = RouteOptions.builder()
            .applyDefaultNavigationOptions()
            .language(Constants.MAPBOX_ROUTE_OPTION_LOCALE)
            .coordinates(originPoint!!, null, destination!!)
            .overview(DirectionsCriteria.OVERVIEW_FULL)
            .profile(profile)
            .annotationsList(
                arrayListOf(
                    DirectionsCriteria.ANNOTATION_DISTANCE,
                    DirectionsCriteria.ANNOTATION_CONGESTION
                )
            )
            .alternatives(true)
            .geometries(Constants.POLYLINE_GEOMETRY)
            .voiceInstructions(true)
            .voiceUnits(DirectionsCriteria.METRIC)
            .bannerInstructions(true) //this
            .steps(true)
            .build()

        val client = MapboxDirections.builder()
            .routeOptions(
                routeOptions
            )
            .accessToken(getString(R.string.mapbox_access_token))
            .build()

        client?.enqueueCall(object : retrofit2.Callback<DirectionsResponse> {

            override fun onResponse(
                call: retrofit2.Call<DirectionsResponse>,
                response: retrofit2.Response<DirectionsResponse>
            ) {
                if (response.body() == null) {
                    Timber.d("No routes found, make sure you set the right user and access token.")
                    return
                } else if (response.body()!!.routes().size < 1) {
                    Timber.d("No routes found")
                    return
                }

                if (response.body()!!.routes().isNotEmpty() && routeLineView != null) {
                    val currentRouteList = response.body()!!.routes()
                    Timber.d("Routes retrieved: " + currentRouteList.size)

                    //Populate Arrival View and add destination icon once routes found
                    populateArrivalView()
                    //Resetting maxdistance for ProgressBar to 0
                    maxDistance = 0

                    turnbyturnViewModel.sortedRouteObjects.observe(
                        this@WalkingActivity,
                        Observer {
                            turnbyturnViewModel.sortedRouteObjects.removeObservers(this@WalkingActivity)
                            val currentRouteObjects = it
                            val navigationRoute = createNavigationRoute(
                                response.body()!!,
                                routeOptions,
                                currentRouteObjects[0].route.routeIndex()?.toInt()
                            )!!
                            val iconToAdd = calculateDestinationIcon()
                            navAmenityLayerManager?.addDestinationIcon(
                                currentRouteObjects[0].route,
                                iconToAdd
                            )
                            MapboxNavigationApp.current()?.run {
                                setNavigationRoutes(listOf(navigationRoute))
                                setRerouteController(null)
                            }
                        })
                    turnbyturnViewModel.getSortedRoutes(currentRouteList, response.body()!!)
                }
            }

            override fun onFailure(call: retrofit2.Call<DirectionsResponse>, t: Throwable) {
                Timber.d("Error: " + t.message)
            }
        })
    }

    private fun showEndNavigationConfirmationDialog() {
        Analytics.logClickEvent(Event.END_WALKING, getScreenName())
        val endNavigation: (dialog: DialogInterface) -> Unit = { dialog ->
            viewBinding.mapViewNav.location.apply {
                enabled = false
            }
            Analytics.logClosePopupEvent(Event.POP_USER_END_YES, getScreenName())
            dialog.dismiss()
            initiateNavigationClose {
                setActivityResult()
            }
        }
        AlertDialog.Builder(this@WalkingActivity)
            .setMessage(R.string.end_navigation)
            .setPositiveButton(R.string.end) { dialog, _ -> endNavigation(dialog) }
            .setNegativeButton(R.string.search_alert_cancel) { dialog, _ ->
                Analytics.logClosePopupEvent(Event.POP_USER_END_NO, getScreenName())
                dialog.dismiss()
            }.create().apply {
                setCanceledOnTouchOutside(false)
            }.also {
                stopTripConfirmationDialog = it
            }.show()
    }

    private fun initiateNavigationClose(onFinished: (result: Boolean) -> Unit = {}) {
        Utils.isNavigationStarted = false
        Utils.phoneNavigationStartEventData = null
        if (!isArrived) {
            postEventWithCacheLast(AppToNavigationEndEvent())
        }
        stopNavigation(isArrived, onFinished)

        MapboxNavigationApp.current()?.run {
            unregisterLocationObserver(locationObserver)
            unregisterRoutesObserver(routesObserver)
            unregisterRouteProgressObserver(routeProgressObserver)
            unregisterLocationObserver(mapMatcherResultObserver)
            SpeedLimitUtil.removeLocationObserver(this)
            unregisterTripSessionStateObserver(mapboxHistoryRecorder)
        }

        //ToDo - Cross check the logic here //ANKUR
        RxBus.publish(RxEvent.CruiseMode(true))
        ETAEngine.getInstance()?.getEtaLocationUpdateInstance(ETAMode.Navigation)?.let {
            LocationBreezeManager.getInstance().removeLocationCallBack(it)
        }

        /**
         * remove location update
         */
        if (isWalking) {
            WalkathonEngine.getInstance()?.stopCheckingLocationForWalkathon()
        }
        compositeDisposable.dispose()
        Timber.d("onDestroy() of Navigation before super")

    }

    private fun showWalkingPathDialog() {
        if (baseContext == null)
            return

        if (!isOnForeground) {
            showWalkingPathDialogOnResume = true
            return
        }

        showWalkingPathDialogOnResume = false

        Dialog(this@WalkingActivity).apply {
            setCancelable(false)
            setContentView(R.layout.dialog_walking_path_guide)
            setOnCancelListener() {
                dismiss()
                setActivityResult()
            }
            window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            setCanceledOnTouchOutside(false)
            findViewById<BreezeButton>(R.id.button_walking_guide).setOnClickListener {
                Analytics.logClickEvent(Event.USE_WALKING_GUIDE, getScreenName())
                dismiss()
                startWalkingNavigation()
            }
            findViewById<TextView>(R.id.textview_end_trip).setOnClickListener {
                Analytics.logClickEvent(Event.END_NO_WALKING_GUIDE, getScreenName())
                dismiss()
                initiateNavigationClose {
                    setActivityResult()
                }
            }
        }.run {
            if (isOnForeground) {
                show()
            }
        }
    }

    @OptIn(ExperimentalPreviewMapboxNavigationAPI::class)
    fun startWalkingNavigation() {
        isArrived = false
        isWalking = true
        BreezeEHorizonProvider.destroy()
        BreezeArrivalObserver.getInstance()
            ?.registerNavigationArrivalObserver(navigationArrivalObserver)
        /**
         * start publish location realtime
         */
        WalkathonEngine.getInstance()?.startCheckingLocationForWalkathon()

        viewBinding.rlDestination.visibility = View.VISIBLE

        viewBinding.mapViewNav.location.apply {
            enabled = true
        }

        viewBinding.mapViewNav.location.apply {
            this.locationPuck = LocationPuck2D(
                bearingImage = ContextCompat.getDrawable(
                    this@WalkingActivity,
                    R.drawable.cursor_with_layer
                    //R.drawable.cursor
                ), scaleExpression = interpolate {
                    linear()
                    zoom()
                    stop {
                        literal(0.0)
                        literal(0.6)
                    }
                    stop {
                        literal(20.0)
                        literal(1.0)
                    }
                }.toJson()
            ) //AAL
            pulsingEnabled = true
            pulsingColor = getColor(R.color.themed_nav_pulse_color)
            pulsingMaxRadius = 10.0F
            enabled = true
        }

        val newRoute = walkingRoute
        initNavigation()
        initNavigationObservers(newRoute!!)
        initRouteLine(newRoute)
        initRouteLineAPI(newRoute)
        startTrip()
        initWalkingRouteLine()
        handleRouteLineRefresh()

        //ToDo - Need to check why the zoom is not working here
        getCurrentLocation()?.let {
            val cameraOptions = CameraOptions.Builder()
                .center(it.toPoint())
                .zoom(FOLLOW_MAX_ZOOM)
                //.pitch(INITIAL_PITCH)
                .build()
            mapboxMap.setCamera(cameraOptions)
        }
    }

    override fun onStop() {
        super.onStop()
        navigationCamera.resetFrame()
        isOnForeground = false
    }

    override fun onDestroy() {
        Timber.d("onDestroy() of Navigation is called")
        handlePostDelayHideArrivalEstimate?.removeCallbacksAndMessages(null)
        navAmenityLayerManager?.destroy()
        ETAEngine.getInstance()?.gc()
        WalkathonEngine.getInstance()?.stopCheckingLocationForWalkathon()
        super.onDestroy()
        scope.cancel()
    }

    private fun init(args: Bundle, skipDriving: Boolean) {
        Timber.d("Calling Mapbox load style")

        initStyle(args) { route ->
            Timber.d("After Mapbox style loaded: Initialisation of route line and views is triggered")

            initWalkingRouteLine()
            if (skipDriving && walkingRoute != null) {
                forceRerouteWalkingPath()
                startWalkingNavigation()
            } else {
                initRouteLine(route)
                initRouteLineAPI(route)

                walkingPathRequestListener()
                Timber.d("Trip started in Navigation")
                startTrip()
                if (!currentDestinationDetails.isDestinationCarPark()) {
                    renderCarParks(false)
                } else {
                    if (currentDestinationDetails.availablePercentImageBand != AmenityBand.TYPE2.type) {
                        renderCarParks(false)
                    }
                }
            }
        }

    }

    private fun findWalkingPath(startingPoint: Point, endingPoint: Point) {
        turnbyturnViewModel.walkingRoutesRetrieved.observe(this) { route ->
            turnbyturnViewModel.walkingRoutesRetrieved.removeObservers(this)
            walkingRoute = route
            initWalkingRouteLine()
        }
        turnbyturnViewModel.findWalkingRoutes(
            startingPoint,
            endingPoint,
            originalDestinationDetails
        )
    }

    private fun forceRerouteWalkingPath() {
        if (walkingRoute != null) {
            val currentLocation = LocationBreezeManager.getInstance().currentLocation
            val destination = Point.fromLngLat(
                originalDestinationDetails.long!!.safeDouble(),
                originalDestinationDetails.lat!!.safeDouble()
            )

            //val routeOptionCoordinates = walkingRoute!!.routeOptions.coordinatesList()
            //if(currentLocation != null && routeOptionCoordinates.size>0) {

            if (currentLocation != null && destination != null) {
                findWalkingPath(
                    startingPoint = currentLocation.toPoint(),
                    endingPoint = destination
                )
            }
        }
    }

    private fun initWalkingRouteLine() {
        walkingRoute?.let { route ->
            if (isWalking) {
                navAmenityLayerManager?.removeWalkingRouteLine()
            } else {
                navAmenityLayerManager?.addWalkingRouteLine(route)
            }
        }
    }

    private fun renderCarParks(announceWarning: Boolean) {
        val originalDest = Point.fromLngLat(
            originalDestinationDetails.long!!.toDouble(),
            originalDestinationDetails.lat!!.toDouble()
        )
        CoroutineScope(Dispatchers.IO).launch {
            navAmenityLayerManager?.fetchAndHandleCarParks(
                originalDest,
                currentDestinationDetails.destinationName,
                currentDestinationDetails.isDestinationCarPark(),
                announceWarning
            )
        }
    }

    @SuppressLint("MissingPermission")
    fun startTrip() {
        TripLoggingManager.setupTripLogging(
            this.applicationContext,
            breezeTripLoggingUtil,
            breezeStatisticsUtil
        )
        turnbyturnViewModel.routablePointResultEvent.observe(this) {
            mapboxHistoryRecorder = HistoryRecorder(
//                firebaseUploader = ServiceLocator.firebaseUploader,
//                searchAnalyticsData = it.searchAnalyticsData,
//                breezeHistoryListener = this
            ).apply {
                navigationMode = "Walking"
                currentUserId = userPreferenceUtil.getUserStoredData()?.cognitoID ?: "unknown_user"
            }

//            turnbyturnViewModel.sendSecondaryFirebaseID(userPreferenceUtil.retrieveUserName())

            MapboxNavigationApp.current()?.let { mapboxNavigation ->
                mapboxNavigation.registerTripSessionStateObserver(mapboxHistoryRecorder)
                if (mapboxNavigation.getTripSessionState() != TripSessionState.STARTED) {
                    ClientFactory.clearMutationCache()
                    mapboxNavigation.startTripSession()
                }
            }


            TripLoggingManager.getInstance()?.startTripLogging(
                tripType = TripType.NAVIGATION,
                screenName = getScreenName(),
                destinationAddressDetails = currentDestinationDetails,
                isWalking = isWalking
            )
            curTripId = breezeTripLoggingUtil.getCurrentTripID()
        }

        turnbyturnViewModel.createSearchFeedback(
            currentDestinationDetails.address1!!,
            currentDestinationDetails.lat!!.safeDouble(),
            currentDestinationDetails.long!!.safeDouble()
        )
    }

    private fun initRouteLine(mRoute: NavigationRoute) {
        var routeLineBelowLayerID = LocationComponentConstants.LOCATION_INDICATOR_LAYER
        if (mapboxMap.getStyle()?.getLayer(Constants.SPEED_CAMERA) != null) {
            routeLineBelowLayerID = Constants.SPEED_CAMERA
        }
        val iconToAdd = calculateDestinationIcon()
        navAmenityLayerManager?.addDestinationIcon(mRoute.directionsRoute, iconToAdd)

        val routeLineResources: RouteLineResources
        val mapStyle = BreezeNavigationMapStyle()
        if (isWalking) {
            routeLineResources = mapStyle.getWalkingRouteLineResources(resources)
        } else {
            routeLineResources = mapStyle.getRouteLineResources(resources)
        }
        val mapboxRouteLineOptions = MapboxRouteLineOptions.Builder(this)
            .withRouteLineBelowLayerId(routeLineBelowLayerID)
            .withRouteLineResources(routeLineResources)
            .withVanishingRouteLineEnabled(false)
            //.displaySoftGradientForTraffic(true)
            .build()
        routeLineAPI = MapboxRouteLineApi(mapboxRouteLineOptions)
        routeLineView = MapboxRouteLineView(mapboxRouteLineOptions)

        val routeArrowOptions = RouteArrowOptions.Builder(this).build()
        routeArrowView = MapboxRouteArrowView(routeArrowOptions)

    }

    private fun calculateDestinationIcon(): Int {
        if (walkingRoute != null && !isWalking) {
            if (selectedCarParkIconRes != -1) {
                return selectedCarParkIconRes
            }
            return R.drawable.parking_selected
        }
        return R.drawable.destination_puck
    }

    private fun initViews() {
        ViewCompat.setElevation(viewBinding.rlDestination, 30.0f)
        viewBinding.btnNavigationEnd.setOnClickListener { handleEndButtonClick() }
    }

    private fun handleEndButtonClick() {
        if (isArrived) {
            Analytics.logClickEvent(Event.END_NAVIGATION_ARRIVAL, getScreenName())
            viewBinding.mapViewNav.location.apply {
                enabled = false
            }
            initiateNavigationClose {
                setActivityResult()
            }
        } else {
            showEndNavigationConfirmationDialog()
        }
    }

    private fun initRouteLineAPI(mRoute: NavigationRoute) {
        scope.launch(Dispatchers.Main) {
            routeLineAPI!!.setNavigationRoutes(listOf(mRoute)) { value ->
                window.decorView.post {
                    routeLineView!!.renderRouteDrawData(mapboxMap.getStyle()!!, value)
                    handleRouteLineRefresh()
                }
                MapboxNavigationApp.current()?.setNavigationRoutes(listOf(mRoute))
                checkForReroute(mRoute)
                currentDirectionsRoute = mRoute
                startNavigation()
            }
        }
    }

    private fun checkForReroute(mRoute: NavigationRoute) {
        val currentLocation = LocationBreezeManager.getInstance().currentLocation
        val routeOptionCoordinates = mRoute.routeOptions.coordinatesList()
        if (currentLocation != null && routeOptionCoordinates.size > 0) {
            if (currentLocation.latitude != routeOptionCoordinates[0].latitude()
                || currentLocation.longitude != routeOptionCoordinates[0].longitude()
            ) {
                scope.launch(Dispatchers.IO) {
                    val distanceMeters = TurfMeasurement.distance(
                        Point.fromLngLat(currentLocation.longitude, currentLocation.latitude),
                        routeOptionCoordinates[0], TurfConstants.UNIT_METERS
                    )
                    //Re routes if distance is greater than 1000 meters from origin
                    if (distanceMeters > 1000) {
                        reRouteBasedOnCurrentLocation(mRoute)
                    }
                }
            }
        }
    }

    private fun reRouteBasedOnCurrentLocation(mRoute: NavigationRoute) {
        val currentLocation = LocationBreezeManager.getInstance().currentLocation
        val currentRouteOption = mRoute.routeOptions
        if (currentLocation != null) {
            val coordinates = currentRouteOption.coordinatesList()
            if (coordinates.size > 0) {
                coordinates[0] =
                    Point.fromLngLat(currentLocation.longitude, currentLocation.latitude)
                val updateRouteOptions =
                    currentRouteOption.toBuilder().coordinatesList(coordinates).build()
                MapboxNavigationApp.current()?.requestRoutes(
                    updateRouteOptions,
                    object : NavigationRouterCallback {
                        override fun onCanceled(
                            routeOptions: RouteOptions,
                            routerOrigin: RouterOrigin
                        ) {
                        }

                        override fun onFailure(
                            reasons: List<RouterFailure>,
                            routeOptions: RouteOptions
                        ) {
                        }

                        override fun onRoutesReady(
                            routes: List<NavigationRoute>,
                            routerOrigin: RouterOrigin
                        ) {
                            MapboxNavigationApp.current()?.setNavigationRoutes(routes)
                        }
                    })
            }
        }
    }

    @OptIn(ExperimentalPreviewMapboxNavigationAPI::class)
    override fun onResume() {
        super.onResume()
        isOnForeground = true

        if (isEndingNavigationOnResume) {
            isEndingNavigationOnResume = false
            initiateNavigationClose {
                setActivityResult()
            }
        } else {
            setTurnByTurnActivityStatusBar()
        }

        if (showWalkingPathDialogOnResume) {
            showWalkingPathDialog()
        } else if (::ratingDialog.isInitialized) {
            if (!ratingDialog.isShowing && !isRatingDialogDismissed) {
                ratingDialog.show()
            }
        }

        if (showArrivedWalkathonDialogOnResume) {
            showArrivedWalkathonDialog()
        }
    }

//    private fun doEndNavigation() {
//        initiateNavigationClose()
//        setActivityResult()
//    }

    @SuppressLint("MissingPermission")
    private fun initNavigation() {
        locationObserver = object : LocationObserver {
            override fun onNewLocationMatcherResult(locationMatcherResult: LocationMatcherResult) {}
            override fun onNewRawLocation(rawLocation: Location) {
                //Timber.d("Raw location change called")
                //Timber.w("Mock location ? [${rawLocation.isFromMockProvider}]")
                val point = Point.fromLngLat(rawLocation.longitude, rawLocation.latitude)
                val cameraOptions = CameraOptions.Builder()
                    .center(point)
                    .zoom(INITIAL_ZOOM)
                    .pitch(INITIAL_PITCH)
                    .build()
                mapboxMap.setCamera(cameraOptions)
                //navigationLocationProvider.changePosition(rawLocation)

            }
        }.also {
            MapboxNavigationApp.current()?.unregisterLocationObserver(it)
            MapboxNavigationApp.current()?.setRerouteController(null)
        }
        tripProgressApi = MapboxTripProgressApi(getTripProgressFormatter())

        /**
         * handle showcompass with walkathon
         */
        handleShowCompassView()

    }


    private fun handleShowCompassView() {
        viewBinding.mapViewNav.compass.enabled = true
        viewBinding.mapViewNav.compass.visibility = true
        viewBinding.mapViewNav.compass.updateSettings {
            marginBottom = 30.toFloat()
            marginLeft = 30.toFloat()
            marginRight = 30.toFloat()
            marginTop = 30.toFloat()
        }
        viewBinding.mapViewNav.compass.fadeWhenFacingNorth = false
    }

    private fun initNavigationObservers(pRoute: NavigationRoute?) {
        MapboxNavigationApp.current()?.run {
            registerLocationObserver(locationObserver)
            registerLocationObserver(mapMatcherResultObserver)
            registerRoutesObserver(routesObserver)
            registerRouteProgressObserver(routeProgressObserver)
        }
    }

    private fun updateCameraToCurrentLocation(isTeleport: Boolean = false, zoom: Double = 0.0) {
        getCurrentLocation()?.let {
            updateCameraLocation(
                location = it,
                isTeleport = isTeleport,
                bearing = 0.0f,
                zoom = zoom
            )
        }
    }

    //var lastResultTime : Long = 0

    private fun updateCameraLocation(
        location: Location,
        isTeleport: Boolean = false,
        bearing: Float = 0.0f,
        zoom: Double = 0.0
    ) {
        val point = Point.fromLngLat(location.longitude, location.latitude)
        val cob = CameraOptions.Builder()
            .center(point)
            .bearing(bearing.toDouble())
            .pitch(INITIAL_PITCH)

        if (zoom > 0.0) {
            cob.zoom(zoom)
        }

        val cameraOptions = cob.build()
        if (isTeleport) {
            mapboxMap.setCamera(cameraOptions)
            navigationLocationProvider.changePosition(location)
        } else {
            //val animationGap = Math.min((System.currentTimeMillis()-lastResultTime), 1000)
            val animationGap = 1000L
            val animationOptions = MapAnimationOptions.Builder().duration(animationGap).build()
            mapboxMap.easeTo(cameraOptions, animationOptions = animationOptions)
            //lastResultTime = System.currentTimeMillis()

            val transitionOptions: (ValueAnimator.() -> Unit)? = {
                duration = animationGap
            }
            navigationLocationProvider.changePosition(
                location,
                emptyList(),
                latLngTransitionOptions = transitionOptions,
                bearingTransitionOptions = transitionOptions
            )
        }
    }

    /**
     * config map style walkathon
     */
    private fun getMapStyleWalkathon(): String {
        when (resources.configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK) {
            Configuration.UI_MODE_NIGHT_NO -> {
                //dark_mode.setText("Disabled")
                return BuildConfig.MAPBOX_WALKATHON_STYLE_URL_LIGHT
            }

            Configuration.UI_MODE_NIGHT_YES -> {
                //dark_mode.setText("Enabled")
                return BuildConfig.MAPBOX_WALKATHON_STYLE_URL_DARK
            }
        }
        return BuildConfig.MAPBOX_WALKATHON_STYLE_URL_DARK
    }

    @SuppressLint("MissingPermission")
    private fun initStyle(args: Bundle, after: (route: NavigationRoute) -> Unit) {
        mapboxMap.loadStyleUri(
            getMapStyleWalkathon()
        ) {

            scope.launch(Dispatchers.Main) {
                // BREEZES-4577: a rare condition where ondestroy is called, but style is not yet loaded
                if (this@WalkingActivity.isDestroyed) {
                    return@launch
                }
                if (!routeInitJob.isCompleted) {
                    routeInitJob.join()
                }
                mRoute?.let { mRoute ->


                    initRouteObservers(args)

                    val dataHolder = TurnByTurnAmenityViewManager.MapViewDataHolder(
                        viewBinding.mapViewNav, getScreenName(),
                        calculateDestinationIcon(), userPreferenceUtil.isDarkTheme()
                    )
                    navAmenityLayerManager = TurnByTurnAmenityViewManager(
                        dataHolder, apiHelper, this@WalkingActivity, originalDestinationDetails,
                        mRoute
                    ) { onCameraTrackingDismissed() }
                    removeTrafficLayer(it)
                    Timber.d("Mapbox style is loaded")
                    setUpGestureListeners()

                    viewBinding.recenterButton.setOnClickListener {
                        recenterMap()
                    }

                    MapIcon.CommonMapIcon.apply {
                        putAll(
                            hashMapOf(
                                Constants.FAVOURITES_DESTINATION_ICON to R.drawable.destination_puck,
                                //Constants.UNSAVED_DESTINATION_ICON to R.drawable.unsaved_destination_puck,
                                Constants.UNSAVED_DESTINATION_ICON to R.drawable.destination_puck,
                            )
                        )
                    }.forEach { (key, value) ->
                        value.toBitmap(this@WalkingActivity)?.let { bmp ->
                            it.addImage(key, bmp, false)
                        }
                    }

                    after(mRoute)

                    viewBinding.mapViewNav.gestures.addOnFlingListener {
                        Timber.d("On fling is called")
                        Analytics.logMapInteractionEvents(Event.DRAG_MAP, getScreenName())
                    }

                    viewBinding.mapViewNav.gestures.addOnScaleListener(object : OnScaleListener {
                        override fun onScale(detector: StandardScaleGestureDetector) {}

                        override fun onScaleBegin(detector: StandardScaleGestureDetector) {}

                        override fun onScaleEnd(detector: StandardScaleGestureDetector) {
                            Timber.d("On scale end is called")
                            Analytics.logMapInteractionEvents(Event.PINCH_MAP, getScreenName())
                        }
                    })
                }
            }
        }
    }

    private fun removeTrafficLayer(it: Style) {
        it.removeStyleLayer(Constants.TRAFFIC_LAYER_ID)
    }

    private fun populateArrivalView() {
        val viewMyView = findViewById<View>(R.id.arrival_notification)
        viewMyView.visibility = View.GONE
        val addressTitle = viewMyView.findViewById<View>(R.id.arrived_address_1) as TextView
        val addressDetails =
            viewMyView.findViewById<View>(R.id.arrived_address_2) as TextView
        val ivBookMark = viewMyView.findViewById<View>(R.id.arrived_bookmark) as ImageView
        val arrivedbt =
            viewMyView.findViewById<View>(R.id.arrived_address_button) as ImageView
        if (!currentDestinationDetails.destinationName.isNullOrBlank() && currentDestinationDetails.easyBreezyAddressID != -1) {
            addressTitle.text = currentDestinationDetails.destinationName
            addressDetails.text = currentDestinationDetails.address1

            ivBookMark.setImageResource(R.drawable.ic_bookmarked)
            arrivedbt.setImageResource(R.drawable.destination_puck)
        } else {
            addressTitle.text = currentDestinationDetails.address1
            addressDetails.text = currentDestinationDetails.address2

            ivBookMark.setImageResource(R.drawable.bookmark_add)
            arrivedbt.setImageResource(R.drawable.destination_puck)
        }
    }

    private fun walkingPathRequestListener() {
        Timber.d("ERP refresh listener is initialized")
        compositeDisposable.add(
            RxBus.listen(RxEvent.RequestToFindWalking::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    findWalkingPath(
                        it.startingPoint,
                        it.endingPoint
                    )
                }
        )
        compositeDisposable.add(
            RxBus.listen(RxEvent.RequestToUpdateCurrentDestination::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    currentDestinationDetails = it.currentDestinationDetails
                }
        )
    }

    @Deprecated(
        level = DeprecationLevel.WARNING,
        message = "We are not using overview camera now as per new design"
    )
    private fun updateCameraToOverview() {
        if (this.resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            viewportDataSource.overviewPadding = landscapeOverviewEdgeInsets
        } else {
            viewportDataSource.overviewPadding = overviewEdgeInsets
        }
        viewportDataSource.evaluate()
        navigationCamera.requestNavigationCameraToOverview()
    }

    private fun updateCameraToFollowing() {
        val topPadding = if (::mapboxMap.isInitialized) {
            mapboxMap.getSize().height.toDouble() * 1.0 / 2.0
        } else {
            0.0
        }
        val followingEdgeInsets = EdgeInsets(
            topPadding,
            0.0,
            topPadding,
            0.0
        )
        viewportDataSource.followingPadding = followingEdgeInsets
        viewportDataSource.evaluate()
        navigationCamera.requestNavigationCameraToFollowing()
    }

    private fun updateCameraToIdle() {
        navigationCamera.requestNavigationCameraToIdle()
    }

    private fun startNavigation() {
        Timber.d("Starting navigation once route is drawn")
        if (isWalking) {
            updateCameraToFollowing()
        }
        BreezeArrivalObserver.create(
            MapboxNavigationApp.current(),
            getScreenName(),
            currentDestinationDetails
        )
        BreezeArrivalObserver.getInstance()
            ?.registerNavigationArrivalObserver(navigationArrivalObserver)
        Timber.d("Nav - Initialising feature detection road objects")
        scope.launch(Dispatchers.IO) {
            breezeFeatureDetectionUtil.initFeatureDetectionRoadObjects()
        }
    }

    @OptIn(ExperimentalPreviewMapboxNavigationAPI::class)
    private fun stopNavigation(
        isArrivedAtDestination: Boolean = false,
        onFinished: (result: Boolean) -> Unit = {}
    ) {
        breezeFeatureDetectionUtil.stopFeatureDetection()

        //updateCameraToIdle()
        clearRouteLine()
        val mapboxNavigation = MapboxNavigationApp.current()
        if (!isArrivedAtDestination && mapboxNavigation != null) {
            BreezeTripManagerUtil.stopTrip(
                mapboxNavigation,
                currentDestinationDetails,
                getScreenName(),
                isArrivedAtDestination,
                onFinished
            )
            mapboxNavigation.setNavigationRoutes(listOf())
        }
        updateCameraToFollowing()


        //Clearing Calendar Util Navigation event details regardless of destination being a calendar item
        breezeCalendarUtil.navigationStopped()

        BreezeArrivalObserver.getInstance()?.unregisterNavigationArrivalObserver(
            navigationArrivalObserver,
            shouldStopTrip = true
        )
        BreezeEHorizonProvider.destroy()

        SpeedLimitUtil.gc()
        if (isArrivedAtDestination) {
            onFinished.invoke(true)
        }
    }

    private fun clearRouteLine() {
        scope.launch(Dispatchers.Main) {

            ifNonNull(routeLineAPI, routeLineView, mapboxMap.getStyle()) { api, view, style ->
                api.clearRouteLine { value ->
                    routeArrowView?.render(style, routeArrowAPI.clearArrows())
                    view.renderClearRouteLineValue(style, value)
                }
                //routeLineAPI.clearRouteLine()
            }
            routeLineAPI?.clearRouteLine() { value ->
                com.mapbox.navigation.utils.internal.ifNonNull(
                    routeLineView,
                    mapboxMap.getStyle()
                ) { view, style ->
                    view.renderClearRouteLineValue(style, value)
                }
            }
            handleRouteLineRefresh()
        }

    }


    //TODO: this is a workaround to remove the default grey marker : https://github.com/mapbox-collab/ncs-collab/issues/365
    private fun handleRouteLineRefresh() {
        if (isWalking) {
            makeLineDashed("mapbox-layerGroup-1-main") //RouteLayerConstants.LAYER_GROUP_1_MAIN
            makeLineDashed("mapbox-layerGroup-2-main") //RouteLayerConstants.LAYER_GROUP_2_MAIN
            makeLineDashed("mapbox-layerGroup-3-main") //RouteLayerConstants.LAYER_GROUP_3_MAIN
        }
        mapboxMap.getStyle()?.removeStyleImage("destinationMarker")
    }

    private fun makeLineDashed(layerId: String) {
        val layer = mapboxMap.getStyle()?.getLayerAs<LineLayer>(layerId)
        layer?.apply {
            //Timber.d("\"${layerId}\"")
            addDashingStyleToLine(baseContext)
        }
    }

    override fun getViewModelReference(): NavigationViewModel {
        return turnbyturnViewModel
    }

    private fun getTripProgressFormatter(): TripProgressUpdateFormatter {
        return TripProgressUpdateFormatter.Builder(this)
            .distanceRemainingFormatter(
                DistanceRemainingFormatter(
                    NavigationOptions.Builder(this.applicationContext)
                        .build().distanceFormatterOptions
                )
            )
            .timeRemainingFormatter(TimeRemainingFormatter(this))
            .percentRouteTraveledFormatter(PercentDistanceTraveledFormatter())
            .estimatedTimeToArrivalFormatter(
                EstimatedTimeToArrivalFormatter(
                    this,
                    TimeFormat.TWELVE_HOURS
                )
            ).build()
    }

    private fun onCameraTrackingDismissed() {
        //Toast.makeText(this, "onCameraTrackingDismissed", Toast.LENGTH_SHORT).show()
        isCameraTrackingDismissed = true
        viewBinding.recenterButton.visibility = View.VISIBLE

        viewBinding.mapViewNav.gestures.removeOnMoveListener(onMoveListener)

        autoRecenterMapHandler = Handler(Looper.getMainLooper())
        autoRecenterMapHandler.postDelayed({
            if (isCameraTrackingDismissed) {
                recenterMap()
            }
        }, Constants.AUTO_RECENTER_MAP_DURATION)
    }

    private fun recenterMap() {
        resetTrackerMode()
        if (isWalking) {
            updateCameraToFollowing()
        }
        Analytics.logClickEvent(Event.RECENTER, getScreenName())
    }

    private fun resetTrackerMode() {
        isCameraTrackingDismissed = false
        viewBinding.recenterButton.visibility = View.GONE

        getGesturesPlugin().addOnMoveListener(onMoveListener)
    }

    @SuppressLint("MissingPermission")
    fun startLocationUpdates() {
        LocationBreezeManager.getInstance().startLocationUpdates()
        ETAEngine.getInstance()?.getEtaLocationUpdateInstance(ETAMode.Navigation)?.let {
            LocationBreezeManager.getInstance().registerLocationCallback(it)
        }
    }

    private fun showNotificationView(
        zone: NavigationZone,
        distance: Int,
        mERPName: String?,
        chargeAmount: String?,
        location: String?
    ) {
        if (isDestroyed) {
            return
        }

        val currentZone = zone
        viewBinding.featureNotification.root.visibility = View.VISIBLE

        if (displayedNavigationZone == null) {
            Analytics.logNotificationEvents(
                Event.MAP_NOTIFICATION,
                Event.NOTIFICATION_OPEN,
                getScreenName()
            )
            displayedNavigationZone = zone
        }


        /**
         * filter zone & set data
         */
        HorizonObserverFillterZoneUtils.filterZone(currentZone, distance).let { data ->
            viewBinding.featureNotification.root.background =
                HorizonObserverFillterZoneUtils.getCruiseNotificationBG(
                    ZoneUIUtils.getZoneTypeBGColor(data.zonePriority),
                    this
                )

            viewBinding.featureNotification.textDistance.text =
                if (zone.maxDistanceMtr <= 300) data.mDistance else location ?: data.mDistance
            if (currentZone == NavigationZone.ERP_ZONE) {

                //todo bangnv will change file type
                //data.mTextZoneType = mERPName
                viewBinding.featureNotification.textZoneType.text = mERPName
            } else {
                viewBinding.featureNotification.textZoneType.text =
                    getString(ZoneUIUtils.getZoneTypeString(data.zonePriority))
            }
            viewBinding.featureNotification.imgZone.setImageResource(
                ZoneUIUtils.getZoneTypeImage(
                    data.zonePriority
                )
            )
            viewBinding.featureNotification.textPrice.text =
                chargeAmount?.safeDouble()?.visibleAmount()
            viewBinding.featureNotification.textPrice.visibility = if (data.mIsShowAmount) {
                View.VISIBLE
            } else {
                View.GONE
            }
        }

        //Handling Voice


        viewBinding.featureNotification.root.visibility = View.VISIBLE
        scope.launch(Dispatchers.Default) {
            delay(Constants.TRIP_NOTIFICATION_SHOW_DURATION)
            Timber.d("invisible this notification")
            runOnUiThread {
                viewBinding.featureNotification.root.visibility = View.GONE
            }
        }
    }

    private fun setTurnByTurnActivityStatusBar() {
        window.apply {
            clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            var flags = window.decorView.systemUiVisibility // get current flag
            flags =
                flags and View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR.inv()// remove LIGHT_STATUS_BAR to flag
            //flags = flags or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            window.decorView.systemUiVisibility = flags
            //decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            statusBarColor =
                ContextCompat.getColor(this@WalkingActivity, R.color.navigation_main_manuever)
        }
    }

    @OptIn(ExperimentalPreviewMapboxNavigationAPI::class)
    private val navigationArrivalObserver = object : NavigationArrivalObserver {
        override fun onFinalDestinationArrival(routeProgress: RouteProgress) {

            if (isWalking) {
                WalkathonEngine.getInstance()?.stopCheckingLocationForWalkathon()
            }
            val payload = bundleOf(
                Param.KEY_NAVIGATION_TYPE to if (isWalking) "WALKING" else "DRIVING"
            )
            Analytics.logEvent(Event.DESTINATION_ARRIVED, payload)

            isArrived = true

            showArrivedNotification(curTripId)

            Analytics.logEvent(
                Event.POP_NATURAL_END_DONE,
                Event.POP_NATURAL_END_DONE,
                getScreenName()
            )
        }

        override fun onNextRouteLegStart(routeProgress: RouteLegProgress) {
            viewBinding.arrivalNotification.root.visibility = View.GONE
        }

        override fun onWaypointArrival(routeProgress: RouteProgress) {
        }

    }

    private fun removeNotificationView(roadObjectId: String) {
        if (roadObjectId.isNotEmpty()) {
            Analytics.logNotificationEvents(
                Event.MAP_NOTIFICATION,
                Event.NOTIFICATION_CLOSE,
                getScreenName()
            )
            viewBinding.featureNotification.root.visibility = View.GONE
        }
    }

    fun getScreenName(): String {
        return Screen.NAVIGATION
    }

    override fun onBackPressed() {
        Analytics.logClickEvent(Event.BACK, getScreenName())
        if (!isArrived) {
            showEndNavigationConfirmationDialog()
        }
    }
//
//    override suspend fun generateNavigationLogData(filePath: String) {
//        Timber.d("filepath=${filePath}")
//        breezeHistoryRecorderUtil.saveHistoryData(
//            this@WalkingActivity,
//            "Walking", filePath
//        )
//    }

    override fun getFragmentContainer(): Int {
        return viewBinding.navigationFrameContainer.id
    }

    fun getMapViewInstance(): MapView? {
        return viewBinding.mapViewNav
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            Constants.REQUEST_CODES.CONTACTS_REQUEST_CODE_FROM_RN -> {
                if (resultCode == Activity.RESULT_OK && data != null) {
                    handleContacts(data)
                } else if (resultCode == Activity.RESULT_CANCELED) {
                    Analytics.logClickEvent(Event.CANCEL_ADD_CONTACT, getScreenName())
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun handleContacts(data: Intent) {

        val contactDetails = Utils.extractPhoneNumberFromContactIntent(data, this)
        if (contactDetails != null) {
            val params: WritableMap = Arguments.createMap()
            params.putString(Constants.TRIPETA.CONTACT_NAME, contactDetails.contactName)
            params.putString(
                Constants.TRIPETA.CONTACT_NUMBER,
                contactDetails.phoneNumberList[0]
            )
            ReactNativeEventEmitter.sendEvent(application = application, "SelectEvent", params)
        } else {
            showAlert(this)
        }
    }

    private fun showAlert(context: Context) {
        val builder: AlertDialog.Builder = context.let {
            AlertDialog.Builder(it)
        }

        builder.setMessage(R.string.incorrect_phone_number)
            .setPositiveButton(
                R.string.alert_close
            ) { dialog, _ ->
                dialog.dismiss()
            }

        val alertDialog: AlertDialog = builder.create()

        alertDialog.setOnShowListener {
            alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).isAllCaps = false
            alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).isAllCaps = false
        }

        alertDialog.show()
    }
}
package com.ncs.breeze.car.breeze.screen.navigation

import androidx.car.app.model.CarColor
import androidx.car.app.navigation.model.TravelEstimate
import com.mapbox.navigation.base.trip.model.RouteProgress
import com.mapbox.navigation.base.trip.model.RouteProgressState
import java.time.LocalDateTime
import java.time.ZonedDateTime
import java.util.*
import java.util.concurrent.TimeUnit

class CarNavigationEtaMapper(private val carDistanceFormatter: CarDistanceFormatter) {

    fun from(routeProgress: RouteProgress?): TravelEstimate? {
        return if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            routeProgress?.run {
                val eta = if (routeProgress.currentState == RouteProgressState.COMPLETE) {
                    Calendar.getInstance()
                } else {
                    Calendar.getInstance().also {
                        it.add(Calendar.SECOND, routeProgress.durationRemaining.toInt())
                    }
                }
                val localDateTime = LocalDateTime.ofInstant(
                    eta.toInstant(),
                    eta.timeZone.toZoneId()
                )

                val distance =
                    carDistanceFormatter.carDistance(routeProgress.distanceRemaining.toDouble())
                val zonedDateTime = ZonedDateTime.of(localDateTime, eta.timeZone.toZoneId())
                TravelEstimate.Builder(distance, zonedDateTime)
                    .setRemainingTimeSeconds(formatTimeRemaining(routeProgress.durationRemaining.toLong()))
                    .setRemainingTimeColor(CarColor.GREEN)
                    .build()
            }
        } else {
            null
        }
    }

    /**
     * TODO: This is a suggested fix
     * Refactor after long term solution is added https://github.com/mapbox/mapbox-navigation-android/issues/5065
     *
     * Logic: The TimeRemainingFormatter formatter used
     * in TurnByTurnNavigationActivity for the phone app
     * rounds up the time in minutes and hours portion when formatting remaining time
     *
     * The value shown in car app TravelEstimate (see above)
     * is rounded down. This causes time remaining disparity
     *
     * The function below will normalize the value
     */
    fun formatTimeRemaining(
        routeDuration: Long,
    ): Long {
        var seconds = routeDuration

        if (seconds < 0) {
            seconds = 0L
        }

        val days = TimeUnit.SECONDS.toDays(seconds)
        seconds -= TimeUnit.DAYS.toSeconds(days)
        val hoursAndMinutes = getHoursAndMinutes(seconds)
        val hours = hoursAndMinutes.first
        val minutes = hoursAndMinutes.second

        return TimeUnit.MINUTES.toSeconds(minutes) + TimeUnit.HOURS.toSeconds(hours) + TimeUnit.DAYS.toSeconds(
            days
        )
    }

    private fun getHoursAndMinutes(seconds: Long): Pair<Long, Long> {
        val initialHoursValue = TimeUnit.SECONDS.toHours(seconds)
        val leftOverSeconds = seconds - TimeUnit.HOURS.toSeconds(initialHoursValue)
        val minutes =
            TimeUnit.SECONDS.toMinutes(leftOverSeconds + TimeUnit.MINUTES.toSeconds(1) / 2)

        return if (minutes == 60L) Pair(initialHoursValue + 1, 0) else Pair(
            initialHoursValue,
            minutes
        )
    }

}

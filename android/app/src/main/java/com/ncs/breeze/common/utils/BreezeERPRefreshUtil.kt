package com.ncs.breeze.common.utils

import android.content.Context
import android.os.Handler
import android.os.Looper
import android.text.TextUtils
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.mapbox.api.directions.v5.models.DirectionsRoute
import com.mapbox.geojson.Feature
import com.mapbox.geojson.FeatureCollection
import com.mapbox.geojson.LineString
import com.mapbox.geojson.Point
import com.mapbox.turf.TurfMeasurement
import com.mapbox.turf.models.LineIntersectsResult
import com.ncs.breeze.common.extensions.android.getAppPreference
import com.ncs.breeze.common.model.DepartureType
import com.breeze.model.ERPChargeInfo
import com.breeze.model.ERPFeatures
import com.breeze.model.ERPRouteInfo
import com.ncs.breeze.common.model.RouteAdapterObjects
import com.ncs.breeze.common.model.RouteDepartureDetails
import com.breeze.model.api.ErrorData
import com.breeze.model.api.response.ERPResponseData
import com.breeze.model.api.response.checkCorrectDayType
import com.breeze.model.constants.Constants
import com.ncs.breeze.common.remote.ApiHelper
import com.ncs.breeze.common.remote.ApiObserver
import com.ncs.breeze.common.rxbus.RxBus
import com.ncs.breeze.common.rxbus.RxEvent
import com.ncs.breeze.common.storage.AppPrefsKey
import com.ncs.breeze.common.utils.RoutePlanningConsts.PROP_ERP_DISPLAY
import com.ncs.breeze.common.utils.RoutePlanningConsts.PROP_ERP_ID
import com.breeze.model.extensions.safeDouble
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Locale
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class BreezeERPRefreshUtil @Inject constructor(apiHelper: ApiHelper, applicationContext: Context) {

    var apiHelper: ApiHelper = apiHelper
    var appContext = applicationContext
    var compositeDisposable: CompositeDisposable = CompositeDisposable()

    lateinit var erpData: ERPResponseData.ERPResponse
    lateinit var listOfUniqueTimes: List<String>
    var map: HashMap<String, ERPFeatures> = HashMap()
    lateinit var lastEmittedData: ERPFeatures
    var currentListItem = -1;

    var handler: Handler = Handler(Looper.getMainLooper())
    var erpDetectionEnabled: Boolean = false;

    val simpleHHmmTimeFormat = SimpleDateFormat(ERP_TIME_FORMAT, Locale.US)

    private val runnableCode = Runnable { // Do something here on the main thread
        erpUpdates()
    }

    var callDataResponse: ((String) -> Unit)? = null
    var callDataParserResponse: ((ERPResponseData.ERPResponse) -> Unit)? = null

    fun setListenerDataCallBack(
        callDataResponse: ((String) -> Unit)? = null,
        callDataParserResponse: ((ERPResponseData.ERPResponse) -> Unit)? = null
    ) {
        this.callDataResponse = callDataResponse
        this.callDataParserResponse = callDataParserResponse
    }

    fun startERPHandler() {
        Timber.d("ERP - Starting ERP handler")
        synchronized(this) {
            if (!erpDetectionEnabled) {
                erpDetectionEnabled = true
                subscribeToAppSyncRxEvent()
                triggerAPICall()
            }
        }
    }

    private fun subscribeToAppSyncRxEvent() {
        compositeDisposable.add(
            RxBus.listen(RxEvent.ERPAppSyncEvent::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError {
                    Timber.d("Observing on error")
                }
                .subscribe(
                    {
                        Timber.d("ERP event observed successfully")

                        try {
                            if (it.erpDataUpdated != null) {
                                saveERPDataInSharedPref(it.erpDataUpdated!!)
                                val data = Gson().fromJson(
                                    it.erpDataUpdated,
                                    ERPResponseData.ERPResponse::class.java
                                )
                                processERPData(data)
                            }
                        } catch (e: Exception) {
                            Timber.e(e, "Error incorrect json")
                        }
                    },
                    {
                        Timber.d("Observing on error")
                    }
                )
        )
    }


    private fun triggerAPICall() {
        apiHelper.getErpRates()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : ApiObserver<ERPResponseData.ERPResponse>(compositeDisposable) {
                override fun onSuccess(data: ERPResponseData.ERPResponse) {
                    Timber.d("Fetching ERPs")
                    val erpResponseJSON = Gson().toJson(data)
                    saveERPDataInSharedPref(erpResponseJSON)
                    processERPData(data)
                }

                override fun onError(e: ErrorData) {
                    Timber.d("ERP Call Failed")
                    val localERPData = retrieveERPDataFromSharedPref()
                    if (localERPData != null) {
                        processERPData(localERPData)
                    } else {
                        Timber.d("ERP Call Failed")
                        RxBus.publish(RxEvent.ERPRefresh(null))
                    }
                }
            })
    }

    fun saveERPDataInSharedPref(erpResponse: String) {
        appContext.getAppPreference()?.saveString(AppPrefsKey.ERP_RESPONSE, erpResponse)
        callDataResponse?.invoke(erpResponse)
    }

    fun retrieveERPDataFromSharedPref(): ERPResponseData.ERPResponse? {
        var erpData: ERPResponseData.ERPResponse? = null
        try {
            val erpResponse = appContext.getAppPreference()?.getString(AppPrefsKey.ERP_RESPONSE)
            erpData = Gson().fromJson(erpResponse, ERPResponseData.ERPResponse::class.java)
        } catch (e: Exception) {
            Timber.e(e, "Error while reading ERP data from shared preference")
        }
        return erpData
    }

    /**
     * Processes ERP response:
     * 1.) Stores the response data in this singleton object instance in a field called erpData
     * 2.) Calculates a list of time where erp can update on each day (regardless of day of week) by calling function calculateUniqueTime
     *     a) This is used to detect any erp changes throughout the day to update all listeners to update erp rate display
     * 3.) Calculates the ERP price list for each timeslot for the day and caches it for the day
     * FIXME: cache doesnt reset on day change
     */
    private fun processERPData(data: ERPResponseData.ERPResponse) {
        CoroutineScope(Dispatchers.Default).launch {
            handler.removeCallbacksAndMessages(null)
            map = hashMapOf()
            erpData = data

            calculateUniqueTime()
            calculateERPList()
            Timber.d("----->   Map  items: " + map.size)
            startHandler()
            Timber.d("Publishing ERP data change")
            RxBus.publish(RxEvent.ERPDataChange(true))
            withContext(Dispatchers.Main) {
                callDataParserResponse?.invoke(data)
            }
        }
    }

    fun startHandler() {
        handler.post(runnableCode)
    }

    fun getCurrentERPData(): ERPFeatures? {
        if (currentListItem != -1) {
            return map[listOfUniqueTimes[currentListItem]]
        }
        return null
    }

    fun getERPData(): ERPResponseData.ERPResponse? {
        if (::erpData.isInitialized) {
            return erpData
        }
        return null
    }


    private suspend fun emitERPData() {
        Timber.d("ERP emitted")
        var listItem: Int = -1
        for ((index, startTime) in listOfUniqueTimes.withIndex()) {
            if (index != listOfUniqueTimes.lastIndex && checkIsTimeInBetween(
                    startTime,
                    listOfUniqueTimes[index + 1],
                    simpleHHmmTimeFormat.format(Calendar.getInstance().time)
                )
            ) {
                listItem = index
                //Timber.d( "Checking time between " + startTime + " " + listOfUniqueTimes[index + 1])
                break;
            } else if (index == listOfUniqueTimes.lastIndex) {
                listItem = index
                break;
            }
        }
        //Timber.d( "List item: " + listItem + " time: " + listOfUniqueTimes[listItem])

        if (listItem != -1) {
            currentListItem = listItem
            Timber.d("RxEvent for ERP Refresh")
            RxBus.publish(RxEvent.ERPRefresh(map[listOfUniqueTimes[listItem]]))
            var erpFeatures = map[listOfUniqueTimes[listItem]]
        } else {
            Timber.d("Wanted to trigger ERP Refresh  but listItem  = -1")
        }
    }

    fun erpUpdates() {
        CoroutineScope(Dispatchers.Default).launch {
            emitERPData()
            val listItem = currentListItem
            var nextItemIsSameDay = true
            if (listItem != -1) {
                val nextListItemIndex = if (listItem == listOfUniqueTimes.lastIndex) {
                    nextItemIsSameDay = false
                    0
                } else {
                    listItem + 1
                }
                //Timber.d( "Current list item: " + listOfUniqueTimes[listItem])
                //Timber.d( "Next list item: " + listOfUniqueTimes[nextListItemIndex])

                val nextTimeValue = listOfUniqueTimes[nextListItemIndex]
                val nextTimeInterval = findIntervalToNextUpdate(nextTimeValue, nextItemIsSameDay)

                Timber.d("Next ERP Refresh value [$nextTimeValue] interval [$nextTimeInterval] ")

                handler.removeCallbacksAndMessages(null)
                handler.postDelayed(runnableCode, nextTimeInterval)
            }
        }
    }

    private fun findIntervalToNextUpdate(
        nextUpdateTimeStr: String,
        isNextUpdateSameDay: Boolean
    ): Long {
        val cal: Calendar = Utils.CURRENT_CALENDER()
        val sdf_1 = SimpleDateFormat("HH:mm:ss", Locale.US)
        val currentTimeStr = sdf_1.format(cal.time)
        val currentTime = sdf_1.parse(currentTimeStr)

        var nextUpdateTime = simpleHHmmTimeFormat.parse(nextUpdateTimeStr)

        if (nextUpdateTime != null && !isNextUpdateSameDay) {
            val nextUpdateCal = Utils.CURRENT_CALENDER()
            nextUpdateCal.time = nextUpdateTime
            nextUpdateCal.add(Calendar.DAY_OF_WEEK, 1)
            nextUpdateTime = nextUpdateCal.time
        }

        if (nextUpdateTime == null || currentTime == null) {
            Timber.d("BreezeERPRefreshUtil - One of the date is NULL to check different. nextUpdateTime: [$nextUpdateTime] currentTime: [$currentTime]")
            return Integer.MAX_VALUE.toLong()
        }

        val difference = nextUpdateTime.time - currentTime.time
        //Timber.d( "Difference in milliseconds is: " + difference)

        Timber.d("BreezeERPRefreshUtil - Printing interval to next update ${difference}")
        return difference
    }

    private suspend fun calculateERPList() {
        for (time in listOfUniqueTimes) {
            map[time] = calculateERPs(time)
        }
    }


    private suspend fun checkIsTimeInBetween(
        startTime: String,
        endTime: String,
        timeToCheck: String
    ): Boolean {
        val timeToCheckNumber = timeToCheck.replace(":", "").toIntOrNull()
        val startTimeNumber = startTime.replace(":", "").toIntOrNull()
        val endTimeNumber = endTime.replace(":", "").toIntOrNull()
        return timeToCheckNumber != null && startTimeNumber != null && endTimeNumber != null &&
                (timeToCheckNumber in startTimeNumber until endTimeNumber) // ignore endtime because it's start time of next period
    }


    fun calculateUniqueTime() {
        val list: ArrayList<String> = arrayListOf()
        if (erpData != null && erpData.erprates != null) {
            for (zone in erpData.erprates) {
                for (rates in zone.rates) {
                    list.add(rates.starttime)
                }
            }
            erpData.publicholiday.forEach {
                list.add(it.startTime)
                list.add(it.endTime)
            }
        }

        val newList = list.distinct()
        /*Timber.d( "Number of item: " + list.size)
        Timber.d( "Number of distinct item: " + newList.size)
        Timber.d( "All item: " + newList.toString())
        Timber.d( "Sorted item: " + newList.sorted().toString())*/
        listOfUniqueTimes = newList.sorted()
        Timber.d("ERP - list of unique times: ${listOfUniqueTimes}")
    }

    private suspend fun getChargeAmountInTimeInterval(
        erpRates: ArrayList<ERPResponseData.Rate>,
        calendar: Calendar
    ): Float {
        val publicHolidayItem = checkIfDateIsPublicHoliday(
            erpData.publicholiday,
            getCurrentDateInStringFormat(calendar),
            calendar
        )

        return publicHolidayItem?.chargeAmount
            ?: (erpRates
                .find {
                    checkIsTimeInBetween(
                        it.starttime,
                        it.endtime,
                        simpleHHmmTimeFormat.format(calendar.timeInMillis)
                    )
                }
                ?.takeIf { it.checkCorrectDayType(calendar) }?.chargeamount
                ?: 0f)
    }

    private suspend fun checkIfDateIsPublicHoliday(
        publicHolidaysList: ArrayList<ERPResponseData.PublicholidayItem>,
        dateStr: String, timeToCheck: Calendar
    ): ERPResponseData.PublicholidayItem? {
        var matchingPublicHoliday: ERPResponseData.PublicholidayItem? = null
        if (!publicHolidaysList.isNullOrEmpty()) {
            matchingPublicHoliday = publicHolidaysList.firstOrNull() {
                TextUtils.equals(it.date, dateStr) && checkIsTimeInBetween(
                    it.startTime,
                    it.endTime,
                    simpleHHmmTimeFormat.format(timeToCheck.timeInMillis)
                )
            }
        }
        return matchingPublicHoliday
    }

    //for current day
    private suspend fun calculateERPs(startTime: String): ERPFeatures {
        val featureListERPNoCharge = arrayListOf<Feature>()
        val featureListERP = arrayListOf<Feature>()
        val mapOfERP: HashMap<String, ERPFeatures.ERPDetails> = hashMapOf()
        val calendarTemp = Calendar.getInstance()
        calendarTemp.time = simpleHHmmTimeFormat.parse(startTime)
        calendarTemp.set(Calendar.DAY_OF_WEEK, Calendar.getInstance().get(Calendar.DAY_OF_WEEK))
        erpData.erps.forEach { erp ->
            val feature = TurfMeasurement.center(
                Feature.fromGeometry(
                    LineString.fromLngLats(
                        listOf(
                            Point.fromLngLat(
                                erp.startlong.safeDouble(),
                                erp.startlat.safeDouble()
                            ),
                            Point.fromLngLat(
                                erp.endlong.safeDouble(),
                                erp.endlat.safeDouble()
                            )
                        )
                    ), JsonObject()
                )
            )
            erpData.erprates.find { erpRate -> erpRate.zoneid == erp.zoneid }?.let { erpRate ->
                val chargeAmount = getChargeAmountInTimeInterval(
                    erpRate.rates,
                    calendarTemp
                )
                mapOfERP["${Constants.ERP_ZONE_NAME_PREFIX}${erp.erpid}"] =
                    ERPFeatures.ERPDetails(erp, chargeAmount)

                if (chargeAmount > 0) {
                    feature.addStringProperty(
                        Constants.ERP.ERP_RATE, "\$%.2f".format(chargeAmount)
                    )
                    featureListERP.add(feature)
                } else {
                    // Calculating feature that is at center of feature using TurfMeasurement
                    featureListERPNoCharge.add(feature)
                }
            }
        }
        return ERPFeatures(featureListERPNoCharge, featureListERP, mapOfERP)
    }

    /**
     * Util function to pass in RouteObjects and find the current
     * ERP charges of the ERPs which intersect with the route
     *
     * Currently used in RoutePlanningFragment
     * and TurnByTurnNavigationActivity(Used in reroute scenario as ERP rates are required for sorting)
     *
     */
    suspend fun getERPsIntersectingWithRouteList(
        erpData: ERPResponseData.ERPResponse,
        routeData: List<RouteAdapterObjects>,
        allowZeroCharge: Boolean,
        routeDepartureDetails: RouteDepartureDetails? = null
    ): HashMap<Int, ERPRouteInfo> {
        val erpInfo = HashMap<Int, ERPRouteInfo>()
        for ((indexRoute, data) in routeData.withIndex()) {
            val calendarDeparture = calculateDepartureTime(
                routeDepartureDetails,
                data
            )
            erpInfo[indexRoute] =
                getERPsIntersectingWithRoute(
                    erpData,
                    data.route,
                    calendarDeparture,
                    allowZeroCharge
                )
        }
        return erpInfo
    }


    private fun calculateDepartureTime(
        routeDepartureDetails: RouteDepartureDetails?, data: RouteAdapterObjects
    ): Calendar {
        val calendarTemp = Calendar.getInstance()
        if (routeDepartureDetails != null) {
            if (routeDepartureDetails.type == DepartureType.DEPARTURE) {
                calendarTemp.timeInMillis = routeDepartureDetails.timestamp
            } else {
                calendarTemp.timeInMillis =
                    (routeDepartureDetails.timestamp - 1000 * data.route.duration()).toLong()
            }
        }
        return calendarTemp
    }

    suspend fun getERPInRoute(
        erpData: ERPResponseData.ERPResponse,
        routeData: DirectionsRoute,
        allowZeroCharge: Boolean
    ): ERPRouteInfo {
        val cal: Calendar = Utils.CURRENT_CALENDER()
        return getERPsIntersectingWithRoute(erpData, routeData, cal, allowZeroCharge)
    }

    private suspend fun getERPsIntersectingWithRoute(
        erpData: ERPResponseData.ERPResponse,
        routeData: DirectionsRoute,
        calendar: Calendar,
        allowZeroCharge: Boolean
    ): ERPRouteInfo {
        val geometry = LineString.fromPolyline(routeData.geometry()!!, Constants.POLYLINE_PRECISION)
        var routeERPAmount = 0.0F
        val featureListERP = arrayListOf<Feature>()
        val erpList = arrayListOf<ERPChargeInfo>()

        for ((index, point1) in geometry.coordinates().withIndex()) {

            if ((index + 1) >= geometry.coordinates().size) {
                break
            } else {
                val point2 = geometry.coordinates().get(index + 1)

                for (erp in erpData.erps) {

                    val intersect = lineIntersects(
                        point1.latitude(),
                        point1.longitude(),
                        point2.latitude(),
                        point2.longitude(),
                        erp.startlat.toDouble(),
                        erp.startlong.toDouble(),
                        erp.endlat.toDouble(),
                        erp.endlong.toDouble()
                    )
                    if (intersect != null) {
                        if (intersect.onLine1()) {

                            val erpRateObject = erpData.erprates.find {
                                it.zoneid == erp.zoneid
                            }

                            if (erpRateObject != null) {
                                val chargeAmount =
                                    getChargeAmountInTimeInterval(erpRateObject.rates, calendar)

                                if (chargeAmount > 0 || allowZeroCharge) {
                                    val geometry = LineString.fromLngLats(
                                        listOf(
                                            Point.fromLngLat(
                                                erp.startlong.toDouble(),
                                                erp.startlat.toDouble()
                                            ),
                                            Point.fromLngLat(
                                                erp.endlong.toDouble(),
                                                erp.endlat.toDouble()
                                            )
                                        )
                                    )
                                    val lineFeature = Feature.fromGeometry(
                                        geometry, JsonObject()
                                    )
                                    val feature = TurfMeasurement.center(lineFeature)
                                    feature.addStringProperty(
                                        PROP_ERP_DISPLAY,
                                        String.format("$%.2f", chargeAmount)
                                    )
                                    feature.addStringProperty(
                                        PROP_ERP_ID,
                                        "${Constants.ERP_ZONE_NAME_PREFIX}${erp.erpid}"
                                    )
                                    erpList.add(ERPChargeInfo(erp.erpid, chargeAmount))
                                    featureListERP.add(feature)

                                    routeERPAmount += chargeAmount
                                }
                            }
                        }
                    }
                }
            }
        }
        return ERPRouteInfo(erpList, FeatureCollection.fromFeatures(featureListERP), routeERPAmount)
    }

    private fun lineIntersects(
        line1StartX: Double, line1StartY: Double,
        line1EndX: Double, line1EndY: Double,
        line2StartX: Double, line2StartY: Double,
        line2EndX: Double, line2EndY: Double
    ): LineIntersectsResult? {
        // If the lines intersect, the result contains the x and y of the intersection
        // (treating the lines as infinite) and booleans for whether line segment 1 or line
        // segment 2 contain the point
        var result = LineIntersectsResult.builder()
            .onLine1(false)
            .onLine2(false)
            .build()
        val denominator = ((line2EndY - line2StartY) * (line1EndX - line1StartX)
                - (line2EndX - line2StartX) * (line1EndY - line1StartY))
        if (denominator == 0.0) {
            return if (result.horizontalIntersection() != null && result.verticalIntersection() != null) {
                result
            } else {
                null
            }
        }
        var varA = line1StartY - line2StartY
        var varB = line1StartX - line2StartX
        val numerator1 = (line2EndX - line2StartX) * varA - (line2EndY - line2StartY) * varB
        val numerator2 = (line1EndX - line1StartX) * varA - (line1EndY - line1StartY) * varB
        varA = numerator1 / denominator
        varB = numerator2 / denominator

        // if we cast these lines infinitely in both directions, they intersect here:
        result = result.toBuilder().horizontalIntersection(
            line1StartX
                    + varA * (line1EndX - line1StartX)
        ).build()
        result = result.toBuilder().verticalIntersection(
            (line1StartY
                    + (varA * (line1EndY - line1StartY)))
        ).build()

        // if line1 is a segment and line2 is infinite, they intersect if:
        if (varA > 0 && varA < 1) {
            result = result.toBuilder().onLine1(true).build()
        }
        // if line2 is a segment and line1 is infinite, they intersect if:
        if (varB > 0 && varB < 1) {
            result = result.toBuilder().onLine2(true).build()
        }
        // if line1 and line2 are segments, they intersect if both of the above are true
        return if (result.onLine1() && result.onLine2()) {
            result
        } else {
            null
        }
    }




    fun stopERPHandler() {
        Timber.d("ERP - Stopping ERP handler")
        handler.removeCallbacksAndMessages(null)
        if (!compositeDisposable.isDisposed) {
            compositeDisposable.clear()
        }
        erpDetectionEnabled = false
        callDataResponse = null
    }

    private fun getCurrentDateInStringFormat(calendar: Calendar): String {
        val formatter = SimpleDateFormat(
            "yyyy-MM-dd"
        )
        return formatter.format(calendar.time)
    }

    companion object {
        val ERP_TIME_FORMAT = "HH:mm"
    }
}
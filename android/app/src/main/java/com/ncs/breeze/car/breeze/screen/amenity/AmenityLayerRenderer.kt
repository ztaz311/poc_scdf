package com.ncs.breeze.car.breeze.screen.amenity

import android.annotation.SuppressLint
import android.graphics.Rect
import com.breeze.model.annotation.AmenityTypeVal
import com.breeze.model.api.ErrorData
import com.breeze.model.api.request.AmenitiesRequest
import com.breeze.model.api.response.amenities.BaseAmenity
import com.breeze.model.api.response.amenities.ResponseAmenities
import com.breeze.model.constants.AmenityType.EVCHARGER
import com.breeze.model.constants.AmenityType.PETROL
import com.breeze.model.extensions.dpToPx
import com.breeze.model.extensions.toBitmap
import com.mapbox.android.core.location.LocationEngineCallback
import com.mapbox.android.core.location.LocationEngineProvider
import com.mapbox.android.core.location.LocationEngineResult
import com.mapbox.androidauto.internal.logAndroidAuto
import com.mapbox.geojson.Feature
import com.mapbox.geojson.FeatureCollection
import com.mapbox.geojson.GeometryCollection
import com.mapbox.geojson.Point
import com.mapbox.maps.EdgeInsets
import com.mapbox.maps.Style
import com.mapbox.maps.extension.androidauto.MapboxCarMapObserver
import com.mapbox.maps.extension.androidauto.MapboxCarMapSurface
import com.mapbox.maps.extension.style.expressions.generated.Expression
import com.mapbox.maps.extension.style.layers.addLayer
import com.mapbox.maps.extension.style.layers.generated.symbolLayer
import com.mapbox.maps.extension.style.layers.properties.generated.IconAnchor
import com.mapbox.maps.extension.style.layers.properties.generated.IconTextFit
import com.mapbox.maps.extension.style.sources.addSource
import com.mapbox.maps.extension.style.sources.generated.geoJsonSource
import com.mapbox.maps.plugin.animation.easeTo
import com.mapbox.maps.plugin.locationcomponent.LocationComponentPlugin
import com.mapbox.maps.plugin.locationcomponent.location
import com.mapbox.navigation.utils.internal.toPoint
import com.ncs.breeze.R
import com.ncs.breeze.car.breeze.location.CarLocationPuck
import com.ncs.breeze.car.breeze.utils.BreezeCarUtil
import com.ncs.breeze.common.remote.ApiObserver
import com.ncs.breeze.common.utils.BreezeMapDataHolder
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

class AmenityLayerRenderer(
    @AmenityTypeVal private val amenityType: String
) : MapboxCarMapObserver {
    var currentLocation: Point? = null

    private var visibleArea: EdgeInsets =
        EdgeInsets(30.0.dpToPx(), 30.0.dpToPx(), 30.0.dpToPx(), 30.0.dpToPx())
    private var locationComponent: LocationComponentPlugin? = null
    private var mapboxCarMapSurface: MapboxCarMapSurface? = null
    val nearbyAmenities = arrayListOf<BaseAmenity>()

    @SuppressLint("MissingPermission")
    override fun onAttached(mapboxCarMapSurface: MapboxCarMapSurface) {
        this.mapboxCarMapSurface = mapboxCarMapSurface
        mapboxCarMapSurface.mapSurface.getMapboxMap().getStyle { style ->
            loadAmenityMapImage(style)
            locationComponent = mapboxCarMapSurface.mapSurface.location.apply {
                locationPuck = CarLocationPuck.homePuck2D(mapboxCarMapSurface.carContext)
                enabled = true
                pulsingEnabled = false
            }
        }
    }

    fun refreshAmenityData(amenities: List<BaseAmenity>) {
        this.nearbyAmenities.clear()
        this.nearbyAmenities.addAll(amenities)
        removeItemInMap()
        displayAmenitiesOnMap()
        updateMapCamera()
    }

    private fun loadAmenityMapImage(style: Style) {
        val carContext = mapboxCarMapSurface?.carContext ?: return
        when (amenityType) {
            PETROL -> R.drawable.ic_map_petrol
            EVCHARGER -> R.drawable.ic_map_evcharger
            else -> -1
        }.takeIf { it > -1 }?.let { imageRes ->
            imageRes.toBitmap(carContext)?.let { bitmap ->
                style.addImage(amenityType, bitmap)
            }
        }
        style.addImage(
            IMAGE_ICON_INDEX_BACKGROUND,
            R.drawable.bg_car_map_icon_index.toBitmap(carContext)!!
        )
    }

    override fun onVisibleAreaChanged(visibleArea: Rect, edgeInsets: EdgeInsets) {
        super.onVisibleAreaChanged(visibleArea, edgeInsets)
        logAndroidAuto("CarNavigationCamera visibleAreaChanged $visibleArea $edgeInsets")
        this.visibleArea = EdgeInsets(
            edgeInsets.top + 30.0.dpToPx(),
            edgeInsets.left + 30.0.dpToPx(),
            edgeInsets.bottom + 30.0.dpToPx(),
            edgeInsets.right + 30.0.dpToPx(),
        )
        updateMapCamera()
    }


    override fun onDetached(mapboxCarMapSurface: MapboxCarMapSurface) {
        removeItemInMap()
        this.mapboxCarMapSurface = null
    }

    private fun updateMapCamera() {
        if (mapboxCarMapSurface == null) return
        val points = arrayListOf<Point>()
        currentLocation?.let { points.add(it) }
        nearbyAmenities.mapNotNull {
            if (it.lat != null && it.long != null)
                Point.fromLngLat(it.long!!, it.lat!!) else null
        }.let { points.addAll(it) }
        mapboxCarMapSurface?.mapSurface?.getMapboxMap()?.let { mapboxMap ->
            val cameraOptions = mapboxMap.cameraForGeometry(
                GeometryCollection.fromGeometries(points.toList()),
                visibleArea,
                bearing = 0.0,
                pitch = 0.0
            )
            mapboxMap.easeTo(cameraOptions)
        }
    }

    private fun displayAmenitiesOnMap() {
        if (nearbyAmenities.isEmpty()) return
        var iconSizeExpression = Expression.interpolate {
            linear()
            zoom()
            stop {
                literal(0.0)
                literal(0.0)
            }
            stop {
                literal(9.0)
                literal(0.0)
            }
            stop {
                literal(12.9)
                literal(0.0)
            }
            stop {
                literal(13.0)
                literal(0.8)
            }
            stop {
                literal(16.5)
                literal(1.0)
            }
        }

        val features = nearbyAmenities.mapNotNull {
            if (it.lat != null && it.long != null) Feature.fromGeometry(
                Point.fromLngLat(
                    it.long!!,
                    it.lat!!
                )
            ) else null
        }
        mapboxCarMapSurface?.mapSurface?.getMapboxMap()?.getStyle { style ->
            // icon sources and layer
            style.addSource(
                geoJsonSource("$amenityType$SOURCE_POSTFIX_ICON") {
                    featureCollection(FeatureCollection.fromFeatures(features))
                }
            )
            style.addLayer(
                symbolLayer("$amenityType$LAYER_POSTFIX_ICON", "$amenityType$SOURCE_POSTFIX_ICON") {
                    iconImage(amenityType)
                    iconSize(iconSizeExpression)
                    iconAnchor(IconAnchor.CENTER)
                    iconAllowOverlap(true)
                    iconIgnorePlacement(true)
                }
            )

            // index for each icon 's source and layer
            style.addSource(
                geoJsonSource("$amenityType$SOURCE_POSTFIX_INDEX") {
                    featureCollection(FeatureCollection.fromFeatures(features.mapIndexed { index, feature ->
                        feature.addStringProperty(KEY_PROPERTY_INDEX, "${index + 1}")
                        return@mapIndexed feature
                    }))
                }
            )
            style.addLayer(
                symbolLayer(
                    "$amenityType$LAYER_POSTFIX_INDEX",
                    "$amenityType$SOURCE_POSTFIX_INDEX"
                ) {
                    textField(Expression.get(KEY_PROPERTY_INDEX))
                    textAllowOverlap(true)
                    textIgnorePlacement(true)
                    textOffset(listOf(1.0, 0.0))
                    textColor(mapboxCarMapSurface!!.carContext.getColor(R.color.themed_black))
                    iconImage(IMAGE_ICON_INDEX_BACKGROUND)
                    iconSize(iconSizeExpression)
                    iconAnchor(IconAnchor.CENTER)
                    iconAllowOverlap(true)
                    iconIgnorePlacement(true)
                    iconTextFit(IconTextFit.BOTH)
                    //iconOffset(listOf(0.0, -2.3))
                    iconTextFitPadding(listOf(1.0, 7.5, 1.0, 5.5)) //Top, Right, Bottom, Left

                }
            )
        }

    }

    private fun removeItemInMap() {
        mapboxCarMapSurface?.mapSurface?.getMapboxMap()?.getStyle { style ->
            style.removeStyleLayer("$amenityType$LAYER_POSTFIX_ICON")
            style.removeStyleLayer("$amenityType$LAYER_POSTFIX_INDEX")

            style.removeStyleSource("$amenityType$SOURCE_POSTFIX_ICON")
            style.removeStyleSource("$amenityType$SOURCE_POSTFIX_INDEX")

        }
    }

    companion object {
        private const val SOURCE_POSTFIX_ICON = "-SOURCE"
        private const val SOURCE_POSTFIX_INDEX = "-INDEX-SOURCE"
        private const val LAYER_POSTFIX_ICON = "-LAYER"
        private const val LAYER_POSTFIX_INDEX = "-INDEX-LAYER"
        private const val KEY_PROPERTY_INDEX = "INDEX"
        private const val IMAGE_ICON_INDEX_BACKGROUND = "IMAGE_ICON_INDEX_BACKGROUND"
    }
}
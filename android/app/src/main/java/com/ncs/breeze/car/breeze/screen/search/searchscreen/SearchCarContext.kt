package com.ncs.breeze.car.breeze.screen.search.searchscreen

import com.ncs.breeze.car.breeze.MainBreezeCarContext

/**
 * Contains the dependencies for the search feature.
 */
class SearchCarContext(
    val mainCarContext: MainBreezeCarContext
) {
    /** MainCarContext **/
    val carContext = mainCarContext.carContext
//    val distanceFormatter = mainCarContext.distanceFormatter
//
//    /** SearchCarContext **/
//    val carSearchEngine = CarSearchEngine(
//        MapboxSearchSdk.createSearchEngine(),
//        CarAppLocationObserver.navigationLocationProvider
//    )
//    val carRouteRequest = CarRouteRequest(
//        mainCarContext.mapboxNavigation,
//        CarAppLocationObserver.navigationLocationProvider
//    )
}

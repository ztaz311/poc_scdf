package com.ncs.breeze.ui.login.fragment.guest.module

import com.ncs.breeze.ui.login.fragment.guest.view.GuestModeSignUpFragment
import com.ncs.breeze.ui.login.fragment.signUp.view.TermsAndCondnFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class GuestModeFragmentModule {
    @ContributesAndroidInjector
    abstract fun contributeGuestModeSignUpfragment(): GuestModeSignUpFragment
}
package com.ncs.breeze.helper.marker

import com.breeze.model.annotation.AmenityTypeVal
import com.breeze.model.api.response.amenities.BaseAmenity
import com.breeze.model.constants.AmenityType.CARPARK
import com.google.gson.JsonObject
import com.mapbox.geojson.Feature
import com.mapbox.geojson.Point
import com.ncs.breeze.components.layermanager.impl.cluster.ClusteredMarkerLayerManager


open class ClusteredAmenityMarkerHelper(
    override val amenityMarkerManager: ClusteredMarkerLayerManager,
    nightModeEnabled: Boolean, voucherEnabled: Boolean
) :
    AmenityMarkerHelper(amenityMarkerManager, nightModeEnabled, voucherEnabled) {


    fun appendAmenityMarkerView(
        type: String,
        featureType: String,
        items: List<BaseAmenity>,
        hidden: Boolean,
        showDestination: Boolean
    ) {
        val featureList: ArrayList<Feature> = ArrayList()
        items.forEach {
            addFeature(featureType, it, featureList, showDestination, hidden)
        }
        amenityMarkerManager.appendMarkers(type, featureList)
    }

    fun removeAmenityMarkerFeatures(
        type: String,
        featureType: String,
        items: List<BaseAmenity>,
        hidden: Boolean,
        showDestination: Boolean
    ) {
        val featureList: ArrayList<Feature> = ArrayList()
        items.forEach {
            addFeature(featureType, it, featureList, showDestination, hidden)
        }
        amenityMarkerManager.removeMarkerFeatures(type, featureList)
    }

    override fun addFeature(
        featureType: String,
        it: BaseAmenity,
        featureList: ArrayList<Feature>,
        showDestination: Boolean,
        hidden: Boolean
    ) {
        val properties = JsonObject()
        when (featureType) {
            CARPARK -> {
                handleCarParkProperties(featureType, it, properties, showDestination, hidden)
            }
            else -> {
                handleAmenityProperties(featureType, properties, hidden, it)
            }
        }
        var feature =
            Feature.fromGeometry(Point.fromLngLat(it.long!!, it.lat!!), properties, it.id)
        featureList.add(feature)
    }

    fun handleAmenityProperties(
        type: String,
        properties: JsonObject,
        hidden: Boolean,
        item: BaseAmenity
    ) {
        super.handleAmenityProperties(properties, hidden, item)
        addClusterProperties(properties, type)
    }

    fun handleCarParkProperties(
        type: String,
        it: BaseAmenity,
        properties: JsonObject,
        showDestination: Boolean,
        hidden: Boolean,
    ) {
        super.handleCarParkProperties(it, properties, showDestination, hidden)
        addClusterProperties(properties, type)
    }

    fun handleSelectedCarParkProperties(
        type: String,
        it: BaseAmenity,
        properties: JsonObject,
        showDestination: Boolean,
        hidden: Boolean = false
    ) {
        super.handleSelectedCarParkProperties(it, properties, showDestination, hidden)
        addClusterProperties(properties, type)
    }

    fun handleSelectedAmenityProperties(
        @AmenityTypeVal type: String,
        properties: JsonObject,
        hidden: Boolean,
        item: BaseAmenity
    ) {
        super.handleSelectedAmenityProperties(properties, hidden, item)
        addClusterProperties(properties, type)
    }

    private fun addClusterProperties(
        properties: JsonObject,
        type: String
    ) {
        properties.addProperty(ClusteredMarkerLayerManager.FEATURE_TYPE, type)
    }

}
package com.ncs.breeze.common.helper.obu

import com.ncs.breeze.common.constant.OBUConstants
import sg.gov.lta.obu.sdk.core.enums.OBUCardStatus
import sg.gov.lta.obu.sdk.core.enums.OBUPaymentMode
import sg.gov.lta.obu.sdk.data.services.OBUSDK

object OBUDataHelper {
    /***
     * only for simulation
     */
    var cardBalance: Double? = OBUConstants.OBU_MOCK_CARD_BALANCE.toDouble()
    var cardStatus: OBUCardStatus? = OBUCardStatus.Detected

    /**
     * To track if user have received card balance from obu yet
     * */
    var isFirstOBUCardValidArrived = false

    fun getOBUCardBalanceSGD(): Double? {
        return if (OBUConnectionHelper.isOBUSimulationEnabled) {
            cardBalance?.div(100.0)
        } else {
            OBUSDK.getCardBalance()?.div(100.0)
        }
    }

    fun getOBUCardBalanceInCent(): Double? {
        return if (OBUConnectionHelper.isOBUSimulationEnabled) {
            cardBalance
        } else {
            OBUSDK.getCardBalance()?.toDouble()
        }
    }

    fun getOBUPaymentMode(): String = OBUSDK.getPaymentMode()?.name ?: ""

    fun isShowBankCard(): Boolean {
        return OBUSDK.getPaymentMode() == OBUPaymentMode.Backend && checkNoCardStatus()
    }

    fun checkNoCardStatus() = false/*OBUSDK.getCardStatus() != null && OBUSDK.getCardStatus() !in arrayOf(
        OBUCardStatus.Detected,
        OBUCardStatus.Invalid,
        OBUCardStatus.InsufficientStoredValue,
        OBUCardStatus.ExpiredStored
    )*/ //todo remove logic check no card for show bankcard

    fun getOBUCardStatus() =
        if (OBUConnectionHelper.isOBUSimulationEnabled) cardStatus else OBUSDK.getCardStatus()
}
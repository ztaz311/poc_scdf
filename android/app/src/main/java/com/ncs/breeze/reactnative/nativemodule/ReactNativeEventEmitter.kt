package com.ncs.breeze.reactnative.nativemodule


import android.app.Application
import com.facebook.react.bridge.WritableMap
import com.facebook.react.modules.core.DeviceEventManagerModule.RCTDeviceEventEmitter
import com.ncs.breeze.App


class ReactNativeEventEmitter {
    companion object {
        fun sendEvent(
            application: Application?,
            eventName: String,
            params: WritableMap?
        ) {
            (application as? App)?.reactNativeHost?.reactInstanceManager?.currentReactContext
                ?.getJSModule(RCTDeviceEventEmitter::class.java)?.emit(eventName, params)
        }
    }
}

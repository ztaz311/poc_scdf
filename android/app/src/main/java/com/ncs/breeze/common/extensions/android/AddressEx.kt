package com.ncs.breeze.common.extensions.android

import android.location.Address

fun Address.generateName() =
    "${if (featureName.isNullOrEmpty()) "" else featureName}${if (thoroughfare.isNullOrEmpty()) "" else " $thoroughfare"}"
package com.ncs.breeze.components.map.profile

import com.breeze.model.api.response.AmenitiesPreference
import com.breeze.model.api.response.ContentDetails
import com.breeze.model.api.response.amenities.BaseAmenity
import com.ncs.breeze.ui.dashboard.fragments.exploremap.ContentDetailKey

interface MapProfile {

    /**
     * loads the style images for the amenities
     * @param amenityTypes the list of amenity types
     */
    suspend fun loadAmenityStyleResources(amenityTypes: ArrayList<String>)

    suspend fun loadImageStyleResources(listContentDetail: ArrayList<ContentDetails>)

    /**
     * loads amenities into the map layer.
     * Also handles showing/hiding and replacing amenities
     * Also handles poi amenities (where type should be poi subitem type)
     * Note: carpark amenity are special and should be handled by [MapProfile.handleCarPark]
     *
     * @param listAmenities map of amenities
     * @param showDestination whether destination icon should be replaced, only used for carparks
     * @param selectedProfile the profile selected, should be null if no profile is selected (only works for clustered mode)
     * @param shouldShowAmenity function callback returns true if amenity is selected
     */
    fun handleAmenitiesData(
        listAmenities: HashMap<String, List<BaseAmenity>>,
        showDestination: Boolean,
        selectedProfile: String?,
        shouldShowAmenity: (AmenitiesPreference) -> Boolean,
    )

    fun handleContentDetailData(
        listAmenities: HashMap<ContentDetailKey, List<BaseAmenity>>,
        showDestination: Boolean,
        selectedProfile: String?,
        shouldShowAmenity: (AmenitiesPreference) -> Boolean,
    )


    /**
     * loads carpark data into map, adds special attributes to the map layer specific for carparks
     * @param mListCarkPark list of amenities
     * @param isMoveCameraWrapperAllCarpark
     * @param showDestination whether to replace destination with the carpark destination
     * @param isMarkerHidden to hide the carparks
     * @param selectedProfile the profile selected, should be null if no profile is selected (only works for clustered mode)
     */
    fun handleCarPark(
        mListCarkPark: List<BaseAmenity>,
        isMoveCameraWrapperAllCarpark: Boolean = false,
        showDestination: Boolean,
        isMarkerHidden: Boolean,
        selectedProfile: String?
    )


    /**
     * clears all carparks from the map layer (removes it completely without hiding)
     * resets any destination carpark marker to the original
     */
    fun clearALlCarPark()

    /**
     * clears all amenities and carparks from the map layer (removes it completely without hiding)
     * removes destination tooltip
     * Note: Does not reset the destination icon
     */
    fun clearALlMarkerAndTooltipAmenities()


    /**
     * removes all tooltips related to amenities and carparks
     */
    fun removeALlTooltip()

    /**
     * to chang the bookmark icon selection
     */
    fun refreshBookmarkIconTooltip(saved: Boolean, bookmarkId: Int = -1)

    /**
     * filters the selected amenities based on type,
     * note: should not include carpark
     * @param element_name name of the type of amenity
     * @param isSelected whether the amenity should be shown or hidden
     * @param amenityItems the current amenity list of the selected type that needs to be filtered
     * @param showDestination whether to update the destination based on carparks
     */
    fun filterAllAmenitiesSelect(
        amenityType: String,
        isSelected: Boolean,
        amenityItems: List<BaseAmenity>,
        showDestination: Boolean
    )

    /**
     * shows the tooltip associated with the carpark  selected
     * @param amenity the carpark amenity
     * @param showDestination whether the destination carpark is to be shown
     * @param toolTipAction instance of [ListenerCallbackLandingMap] to handle click events affect the map view
     * @param startRoutePlanningFromMarker callback invoked when navigating
     * @param layerType type of layer where amenity was clicked from
     */
    fun showCarParkToolTip(
        amenity: BaseAmenity,
        showDestination: Boolean,
        showBookmark: Boolean,
        toolTipAction: ListenerCallbackLandingMap?,
        layerType: String,
        shouldRecenter: Boolean,
        startRoutePlanningFromMarker: (item: BaseAmenity, isCarPark: Boolean) -> Unit,

        )

    /**
     * shows the tooltip associated with the amenity selected
     * @param amenity the amenity selected
     * @param type the type of the amenity
     * @param toolTipAction instance of [ListenerCallbackLandingMap] to handle click events affect the map view
     * @param startRoutePlanningFromMarker callback invoked when navigating
     * @param layerType type of layer where amenity was clicked from
     */
    fun handleAmenityToolTip(
        amenity: BaseAmenity,
        type: String,
        toolTipAction: ListenerCallbackLandingMap?,
        layerType: String,
        shouldRecenter: Boolean,
        showBookmark: Boolean,
        enableWalking: Boolean = false,
        enableZoneDrive: Boolean = true,
        startRoutePlanningFromMarker: (item: BaseAmenity, isCarPark: Boolean) -> Unit,
        startWalkingPathFromMarker: ((item: BaseAmenity, isCarPark: Boolean) -> Unit)? = null
    )

    /**
     * shows the tooltip associated with the profile amenity selected
     * @param amenity the amenity selected
     * @param type the type of the amenity
     * @param toolTipAction instance of [ListenerCallbackLandingMap] to handle click events affect the map view
     * @param startRoutePlanningFromMarker callback invoked when navigating
     * @param startWalkingPathFromMarker callback invoked when navigating
     * @param layerType type of layer where amenity was clicked from
     */
    fun handlePoiToolTip(
        amenity: BaseAmenity,
        type: String,
        toolTipAction: ListenerCallbackLandingMap?,
        layerType: String,
        shouldRecenter: Boolean,
        startRoutePlanningFromMarker: (item: BaseAmenity, isCarPark: Boolean) -> Unit,
        startWalkingPathFromMarker: (item: BaseAmenity, isCarPark: Boolean) -> Unit
    )

    /**
     * shows the tooltip associated with the profile amenity selected
     * @param amenity the amenity selected
     * @param type the type of the amenity
     * @param toolTipAction instance of [ListenerCallbackLandingMap] to handle click events affect the map view
     * @param startRoutePlanningFromMarker callback invoked when navigating
     * @param startWalkingPathFromMarker callback invoked when navigating
     * @param layerType type of layer where amenity was clicked from
     */
    fun handlePoiToolTipMapExplore(
        amenity: BaseAmenity,
        type: String,
        toolTipAction: ListenerCallbackLandingMap?,
        layerType: String,
        shouldRecenter: Boolean,
        enableWalking: Boolean,
        enableZoneDrive: Boolean,
        startRoutePlanningFromMarker: (item: BaseAmenity, isCarPark: Boolean) -> Unit,
        startWalkingPathFromMarker: (item: BaseAmenity, isCarPark: Boolean) -> Unit
    )


    /**
     * hides all the carpark amenities
     * removes all tooltips shown
     * resets any destination carpark marker to the original destination
     * @param amenityItems all amenity items to be hidden
     * @param showDestination
     */
    fun hideAllCarPark(amenityItems: List<BaseAmenity>, showDestination: Boolean)

    /**
     * hides all amenities excluding car parks
     */
    fun hideAmenities()

    /**
     * shows all the carpark amenities
     * resets and shows destination carpark marker
     * @param mListCarPark list of amenities
     * @param isMoveCameraWrapperAllCarpark
     * @param showDestination whether to replace destination with the carpark destination
     * @param isMarkerHidden to hide the carparks
     * @param selectedProfile the profile selected, should be null if no profile is selected (only works for clustered mode)
     */
    fun showAllCarPark(
        mListCarPark: List<BaseAmenity>?,
        isMoveCameraWrapperAllCarpark: Boolean,
        showDestination: Boolean,
        isMarkerHidden: Boolean,
        selectedProfile: String?
    )

    /**
     * hides all carpark and amenities
     * resets any destination carpark marker to the original
     */
    fun hideAllAmenitiesAndCarParks()


    /**
     * shows all selected amenities (based on [filterAllAmenitiesSelect])
     * resets destination carpark marker to the correct state
     * @param mListCarPark
     * @param amenityItems all amenity items to be shown
     * @param showDestination
     * @param selectedProfile the profile selected, should be null if no profile is selected (only works for clustered mode)
     * @param shouldShowAmenity function callback returns true if amenity is selected
     */
    fun showAllAmenities(
        mListCarPark: List<BaseAmenity>?,
        listAmenities: HashMap<String, List<BaseAmenity>>,
        showDestination: Boolean,
        selectedProfile: String?,
        shouldShowAmenity: (AmenitiesPreference) -> Boolean,
    )


    /**
     * return true if the layer has no amenities added excluding car parks
     */
    fun isAmenitiesEmpty(): Boolean

    /**
     * return true if the layer has no car parks added
     */
    fun isCarParkEmpty(): Boolean


    /**
     * clears and destroys all map associated resources
     */
    fun destroy()

}
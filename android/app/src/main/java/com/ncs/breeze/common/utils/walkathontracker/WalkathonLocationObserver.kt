package com.ncs.breeze.common.utils.walkathontracker

import com.ncs.breeze.common.utils.LocationBreezeManager
import com.mapbox.android.core.location.LocationEngineResult


class WalkathonLocationObserver(val walkathonEngine: WalkathonEngine) :
    LocationBreezeManager.CallBackLocationBreezeManager {
    override fun onSuccess(result: LocationEngineResult) {
        walkathonEngine.updateLocation(result.lastLocation)
    }
}
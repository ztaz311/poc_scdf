package com.ncs.breeze.common.extensions.android

import androidx.fragment.app.Fragment
import com.ncs.breeze.ui.base.BaseActivity

fun Fragment.getApp() = context?.getApp()
fun Fragment.getOBUConnHelper() = getApp()?.obuConnectionHelper
fun Fragment.getOBUAlertHelper() = (activity as? BaseActivity<*,*>)?.obuAlertHelper

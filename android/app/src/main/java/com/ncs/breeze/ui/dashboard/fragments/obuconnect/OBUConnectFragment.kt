package com.ncs.breeze.ui.dashboard.fragments.obuconnect

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import com.ncs.breeze.R
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.Event
import com.ncs.breeze.common.analytics.Screen
import com.ncs.breeze.common.constant.OBUConnectionState
import com.ncs.breeze.common.extensions.android.getOBUConnHelper
import com.breeze.model.obu.OBUInitialConnectionStep
import com.ncs.breeze.databinding.FragmentObuConnectBinding
import com.ncs.breeze.ui.base.BaseFragment
import com.ncs.breeze.ui.dashboard.fragments.viewmodel.OBUConnectViewModel
import javax.inject.Inject

class OBUConnectFragment : BaseFragment<FragmentObuConnectBinding, OBUConnectViewModel>() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val mViewModel: OBUConnectViewModel by viewModels {
        viewModelFactory
    }

    override fun inflateViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?,
        attachToContainer: Boolean
    ) = FragmentObuConnectBinding.inflate(inflater, container, attachToContainer)

    override fun getViewModelReference() = mViewModel

    override fun getScreenName() = Screen.OBU_PAIRING_SCREEN

    private val obuConnectionStateChangeObserver: (state: OBUConnectionState) -> Unit = { state ->
        toggleThirdStateLoadingVisibility(state == OBUConnectionState.PAIRING)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initStateObserver()
        mViewModel.obuInitialConnectionStep.postValue(OBUInitialConnectionStep.INTRO)
        viewBinding.backButton.setOnClickListener {
            getOBUConnHelper()?.disconnectOBU()
            onBackPressed()
        }
        getOBUConnHelper()?.obuConnectionState?.observe(
            viewLifecycleOwner,
            obuConnectionStateChangeObserver
        )
        initConnectionStep()
    }

    override fun onBackPressed() {
        sendLogAnalytic()
        super.onBackPressed()
    }

    override fun onDeviceBackPressed() {
        getOBUConnHelper()?.disconnectOBU()
        sendLogAnalytic()
        super.onDeviceBackPressed()
    }

    private fun sendLogAnalytic() {
        when (childFragmentManager.findFragmentById(viewBinding.fragmentContainer.id)) {
            is OBUConnectFirstStateFragment -> {
                Analytics.logClickEvent(Event.OBU_PAIRING_BACK, getScreenName())
            }

            is OBUConnectSecondStateFragment -> {
                Analytics.logClickEvent(Event.OBU_PAIRING_NUMBER_PLATE_BACK, getScreenName())
            }

            is OBUConnectThirdStateFragment -> {
                Analytics.logClickEvent(Event.OBU_PAIRING_PAIRING_BACK, getScreenName())
            }
        }
    }

    private fun initConnectionStep() {
        arguments?.get(ARG_CONNECTION_STEP)?.let { initialStep ->
            mViewModel.obuInitialConnectionStep.postValue(
                OBUInitialConnectionStep.values()
                    .find { step -> step == initialStep }
                    ?: OBUInitialConnectionStep.INTRO
            )
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        getOBUConnHelper()?.obuConnectionState?.removeObserver(
            obuConnectionStateChangeObserver
        )
    }

    private fun toggleThirdStateLoadingVisibility(isVisible: Boolean) {
        (childFragmentManager.findFragmentById(viewBinding.fragmentContainer.id) as? OBUConnectThirdStateFragment)
            ?.toggleConnectingIndicatorVisibility(isVisible)
    }

    private fun initStateObserver() {
        val activeColor = ContextCompat.getColor(requireContext(), R.color.breeze_primary)
        val activeTintColor =
            ContextCompat.getColorStateList(requireContext(), R.color.breeze_primary)
        val inactiveColor = ContextCompat.getColor(requireContext(), R.color.obu_unselected)
        val inactiveTintColor =
            ContextCompat.getColorStateList(requireContext(), R.color.obu_unselected)

        val inactiveTextColor = ContextCompat.getColor(requireContext(), R.color.obu_unselected)
        val activeTextColor = ContextCompat.getColor(requireContext(), R.color.text_black)
        mViewModel.obuInitialConnectionStep.observe(viewLifecycleOwner) { stateIndex ->
            viewBinding.layoutState.imgSecondState.imageTintList =
                if (stateIndex >= OBUInitialConnectionStep.VEHICLE_NUMBER) activeTintColor else inactiveTintColor
            (if (stateIndex >= OBUInitialConnectionStep.VEHICLE_NUMBER) activeColor else inactiveColor).let { color ->
                viewBinding.layoutState.vFirstStateDivider.setBackgroundColor(color)

            }
            (if (stateIndex >= OBUInitialConnectionStep.VEHICLE_NUMBER) activeTextColor else inactiveTextColor).let { color ->
                viewBinding.layoutState.tvSecondState.setTextColor(color)
            }

            viewBinding.layoutState.imgThirdState.imageTintList =
                if (stateIndex >= OBUInitialConnectionStep.OUTRO) activeTintColor else inactiveTintColor
            (if (stateIndex >= OBUInitialConnectionStep.OUTRO) activeColor else inactiveColor).let { color ->
                viewBinding.layoutState.vSecondStateDivider.setBackgroundColor(color)
            }
            (if (stateIndex >= OBUInitialConnectionStep.OUTRO) activeTextColor else inactiveTextColor).let { color ->
                viewBinding.layoutState.tvThirdSate.setTextColor(color)
            }

            when (stateIndex) {
                OBUInitialConnectionStep.INTRO -> getString(R.string.obu_connect_first_state_title)
                OBUInitialConnectionStep.VEHICLE_NUMBER -> getString(R.string.obu_connect_second_state_title)
                OBUInitialConnectionStep.OUTRO -> getString(R.string.obu_connect_third_state_title)
                else -> null
            }?.let {
                viewBinding.tvTitle.text = it
            }

            when (stateIndex) {
                OBUInitialConnectionStep.INTRO -> OBUConnectFirstStateFragment()
                OBUInitialConnectionStep.VEHICLE_NUMBER -> OBUConnectSecondStateFragment()
                OBUInitialConnectionStep.OUTRO -> OBUConnectThirdStateFragment()
                else -> null
            }?.let { fragment ->
                childFragmentManager.beginTransaction()
                    .replace(viewBinding.fragmentContainer.id, fragment)
                    .commitNow()
            }
        }
    }

    fun setStep(step: OBUInitialConnectionStep) {
        mViewModel.obuInitialConnectionStep.postValue(step)
    }


    companion object {
        const val TAG = "OBUConnectFragment"

        private const val ARG_CONNECTION_STEP = "ARG_CONNECTION_STEP"
        fun newInstance(step: OBUInitialConnectionStep = OBUInitialConnectionStep.INTRO) =
            OBUConnectFragment().apply {
                arguments = bundleOf(
                    ARG_CONNECTION_STEP to step
                )
            }
    }
}
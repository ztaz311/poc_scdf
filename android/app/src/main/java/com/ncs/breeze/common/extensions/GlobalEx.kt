package com.ncs.breeze.common.extensions

import com.ncs.breeze.BuildConfig
fun isDev() = BuildConfig.FLAVOR == "dev"
fun isQA() = BuildConfig.FLAVOR == "qa"
fun isDemo() = BuildConfig.FLAVOR == "demo"
fun isProd() = BuildConfig.FLAVOR == "production"

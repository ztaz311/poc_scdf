package com.ncs.breeze.ui.dashboard.fragments.exploremap

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.Resources
import android.location.Location
import android.os.Bundle
import android.os.SystemClock
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.activity.OnBackPressedCallback
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.mapbox.android.gestures.MoveGestureDetector
import com.mapbox.android.gestures.RotateGestureDetector
import com.mapbox.android.gestures.StandardScaleGestureDetector
import com.mapbox.geojson.Point
import com.mapbox.maps.CameraOptions
import com.mapbox.maps.EdgeInsets
import com.mapbox.maps.MapView
import com.mapbox.maps.MapboxMap
import com.mapbox.maps.plugin.LocationPuck2D
import com.mapbox.maps.plugin.animation.camera
import com.mapbox.maps.plugin.gestures.OnMoveListener
import com.mapbox.maps.plugin.gestures.OnRotateListener
import com.mapbox.maps.plugin.gestures.OnScaleListener
import com.mapbox.maps.plugin.gestures.gestures
import com.mapbox.maps.plugin.locationcomponent.LocationComponentPlugin
import com.mapbox.maps.plugin.locationcomponent.LocationProvider
import com.mapbox.maps.plugin.locationcomponent.OnIndicatorPositionChangedListener
import com.mapbox.maps.plugin.locationcomponent.location
import com.mapbox.navigation.ui.maps.camera.NavigationCamera
import com.mapbox.navigation.ui.maps.camera.data.MapboxNavigationViewportDataSource
import com.mapbox.navigation.ui.maps.camera.lifecycle.NavigationScaleGestureHandler
import com.mapbox.turf.TurfConstants
import com.mapbox.turf.TurfMeasurement
import com.ncs.breeze.BuildConfig
import com.ncs.breeze.R
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.Event
import com.ncs.breeze.common.analytics.Screen
import com.breeze.model.enums.CarParkViewOption
import com.ncs.breeze.common.constant.RNScreen
import com.ncs.breeze.common.event.ToAppRx
import com.ncs.breeze.common.extensions.android.getRNFragmentNavigationParams
import com.ncs.breeze.common.extensions.mapbox.setBreezeDefaultOptions
import com.breeze.model.DestinationAddressDetails
import com.breeze.model.api.response.ContentDetailsResponse
import com.breeze.model.api.response.ExploreMaintenanceResponse
import com.breeze.model.api.response.amenities.BaseAmenity
import com.breeze.model.constants.Constants
import com.ncs.breeze.common.model.rx.ToAppRxEvent
import com.ncs.breeze.common.remote.MyServiceInterceptor
import com.ncs.breeze.common.rxbus.RxBus
import com.ncs.breeze.common.rxbus.RxEvent
import com.ncs.breeze.common.storage.BreezeUserPreference
import com.ncs.breeze.common.utils.*
import com.ncs.breeze.components.GlobalNotification
import com.ncs.breeze.components.map.profile.ListenerCallbackLandingMap
import com.ncs.breeze.components.map.profile.MapProfileType
import com.ncs.breeze.components.marker.markerview2.SearchLocationMarkerViewManager
import com.ncs.breeze.databinding.FragmentExploreMapsBinding
import com.ncs.breeze.ui.base.BaseActivity
import com.ncs.breeze.ui.base.BaseFragment
import com.ncs.breeze.ui.dashboard.AdjustableBottomSheet
import com.ncs.breeze.ui.dashboard.activity.DashboardActivity
import com.ncs.breeze.ui.dashboard.fragments.exploremap.profile.ExploreMapCongestionManager
import com.ncs.breeze.ui.dashboard.manager.ExploreMapStateManager
import com.ncs.breeze.ui.rn.CustomReactFragment
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import javax.inject.Inject


class ExploreMapsFragment :
    BaseFragment<FragmentExploreMapsBinding, ExploreMapsFragmentViewModel>(),
    AdjustableBottomSheet {


    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var myServiceInterceptor: MyServiceInterceptor

    @Inject
    lateinit var breezeUserPreferenceUtil: BreezeUserPreferenceUtil

    @Inject
    lateinit var userPreferenceUtil: BreezeUserPreferenceUtil

    lateinit var disposableOpenCalculateFee: Disposable
    lateinit var disposaableEventOpenSeeMore: Disposable

    private val mViewModel: ExploreMapsFragmentViewModel by viewModels {
        viewModelFactory
    }

    var compositeDisposable: CompositeDisposable = CompositeDisposable()

    var dataFromRN: String? = null

    var distroyCallback: (() -> Unit)? = null

    lateinit var mapView: MapView
    private var progressBar: FrameLayout? = null
    private var orange_container: FrameLayout? = null
    private var rl_free_drive_info: LinearLayout? = null
    private var camera_tracker: ImageView? = null
    private var imgTrackedInProgress: ImageView? = null
    private var frBottomDetail: FrameLayout? = null
    var annotationView: View? = null

    /**
     *for showing current location
     */
    private lateinit var defaultLocationPuck: LocationPuck2D
    private lateinit var locationComponent: LocationComponentPlugin
    private var defaultLocationProvider: LocationProvider? = null

    private lateinit var mapboxMap: MapboxMap
    private lateinit var navigationCamera: NavigationCamera
    private lateinit var viewportDataSource: MapboxNavigationViewportDataSource

    private val pixelDensity = Resources.getSystem().displayMetrics.density
    lateinit var exploreMapCamera: ExploreMapCamera

    var isCameraTrackingDismissed: Boolean = false

    private lateinit var exploreMapCongestionManager: ExploreMapCongestionManager
    lateinit var exploreMapController: ExploreMapController

    /**
     * for ERP
     */
    var searchLocationMarkerViewManager: SearchLocationMarkerViewManager? = null

    private val onIndicatorPositionChangedListener =
        OnIndicatorPositionChangedListener { point ->
            if (!isCameraTrackingDismissed) {
                mapView.getMapboxMap().setCamera(CameraOptions.Builder().center(point).build())
                mapView.gestures.focalPoint =
                    mapView.getMapboxMap().pixelForCoordinate(point)
            }
        }


    private val contentSelectedObserver = Observer<ContentDetailsResponse?> {
        it?.let {
            exploreMapController.clearALlMarkerAndTooltipAmenities()
            exploreMapController.resetAllViews()
            exploreMapController.exploreUserLocationToolTip.resetToolTipState()
            exploreMapCongestionManager.hideProfileZoneLayer()
            ExploreMapStateManager.contentDetailsResponse = it
            handleSelectedContentData(it)
            resetTrackerMode()
        }
    }

    /**
     * handle selected content Data
     */
    private fun handleSelectedContentData(it: ContentDetailsResponse) {
        when (it.type) {
            Constants.CONTENT_LAYER.CONTENT_TYPE_ZONE -> {
                handleZoneType()
            }

            Constants.CONTENT_LAYER.CONTENT_TYPE_GLOBAL_POI -> {
                handleGlobalPoiType()
            }

            else -> {
                handleGlobalPoiType()
            }
        }

        /**
         * handle button tracked progress
         */
        handleButtonTrackedProgress()
    }

    var upComingLastClickTime: Long = 0
    private fun handleButtonTrackedProgress() {
        ExploreMapStateManager.contentDetailsResponse?.let {
            if (it.track_navigation) {
                val linkImageButton = if (breezeUserPreferenceUtil.isDarkTheme()) {
                    it.stats_icon_dark
                } else {
                    it.stats_icon_light
                }
                activity?.let {
                    Glide.with(it)
                        .load(linkImageButton)
                        .into(imgTrackedInProgress!!)
                }
                imgTrackedInProgress?.visibility = View.VISIBLE
                imgTrackedInProgress?.setOnClickListener {
                    if (SystemClock.elapsedRealtime() - upComingLastClickTime < 1000) return@setOnClickListener
                    Analytics.logClickEvent(
                        Event.TAMPANIES_GREEN_PROGRESS_TRACKING_OPEN,
                        getScreenName()
                    )
                    showBottomSheetProgressTracked()
                    upComingLastClickTime = SystemClock.elapsedRealtime()
                }
            } else {
                imgTrackedInProgress?.visibility = View.GONE
            }
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        val callback: OnBackPressedCallback = object : OnBackPressedCallback(
            true // default to enabled
        ) {
            override fun handleOnBackPressed() {
                val currentFragment =
                    childFragmentManager.findFragmentById(R.id.frBottomDetailLayer2)
                if (currentFragment != null) {
                    if (currentFragment.isAdded) {
                        childFragmentManager.beginTransaction().remove(currentFragment).commit()
                        exploreMapController.updateBottomSheetState(0, RNScreen.EXPLORE)
                        exploreMapController.removeALlTooltip()
                        hideCarParkWhenUnSelectPOI()
                    } else {
                        parentFragmentManager.popBackStack()
                    }
                } else {
                    parentFragmentManager.popBackStack()
                }
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(this, callback)

    }

    private fun handleGlobalPoiType() {
        loadStyleImage {
            LocationBreezeManager.getInstance().currentLocation?.let {
                if (ExploreMapStateManager.contentDetailsResponse?.details?.isNotEmpty() == true) {
                    exploreMapCamera.setContentDetails(
                        Constants.CONTENT_LAYER.CONTENT_TYPE_ZONE,
                        calculateFurthestContentPoint(Point.fromLngLat(it.longitude, it.latitude)),
                        null
                    )
//                    onCameraTrackingDismissed()
                }
            }
            handleContentPOI()
        }
    }

    private fun calculateFurthestContentPoint(currentLocation: Point): Point {
        var furthestDistance = 0.0
        var furthestPoint: Point = currentLocation
        ExploreMapStateManager.contentDetailsResponse?.let { content ->
            content.details.forEach { details ->
                details.data.forEach {
                    if (it.long != null && it.lat != null) {
                        val currentPoint = Point.fromLngLat(it.long!!, it.lat!!)
                        val currentDistance = TurfMeasurement.distance(
                            currentPoint,
                            currentLocation,
                            TurfConstants.UNIT_METERS
                        )
                        if (currentDistance > furthestDistance) {
                            furthestDistance = currentDistance
                            furthestPoint = currentPoint
                        }
                    }
                }
            }
        }
        return furthestPoint
    }


    private fun handleZoneType() {
        /**
         * load style image for zone
         */

        loadStyleImage {
            if (isFragmentDestroyed()) {
                return@loadStyleImage
            }
            val location = Location("")
            val zones =
                ExploreMapStateManager.contentDetailsResponse?.zones
            if (zones?.isNotEmpty() == true) {
                val radius = zones.last().radius ?: 1000
                location.latitude = zones[0].centerPointLat!!
                location.longitude = zones[0].centerPointLong!!
                exploreMapCamera.setContentDetails(
                    Constants.CONTENT_LAYER.CONTENT_TYPE_ZONE,
                    Point.fromLngLat(location.longitude, location.latitude),
                    radius.toDouble()
                )
            } else {
                exploreMapCamera.setContentDetails(
                    Constants.CONTENT_LAYER.CONTENT_TYPE_ZONE,
                    null,
                    null
                )
            }

//            onCameraTrackingDismissed()

            val profileAddress = DestinationAddressDetails(
                null,
                null,
                null,
                location.latitude.toString(),
                location.longitude.toString(),
                null,
                null,
                null
            )
            profileAddress.apply {
                ExploreMapStateManager.resetCurrentLocationFocus()
            }

            /**
             * get zone color
             */
            ExploreMapStateManager.contentDetailsResponse?.layerCode?.let {
                getZoneCongestionDetails(it) {
                    /**
                     * handle poi
                     */
                    handleContentPOI()
                }
            }
        }
    }


    /**
     * handle poi with content ID
     */
    private fun handleContentPOI() {
        val mapContentDetail = HashMap<ContentDetailKey, List<BaseAmenity>>()
        ExploreMapStateManager.contentDetailsResponse?.details?.forEach {
            if (it.code != null && it.data != null && it.tooltip_type != null) {
                val contentDetailKey = ContentDetailKey(it.code!!, it.tooltip_type!!)
                mapContentDetail[contentDetailKey] = it.data
            }
        }
        exploreMapController.handleAmenitiesData(mapContentDetail)
    }

    private fun initExploreMapController() {
        /**
         * setup dashboard activity
         */
        val mapListenerCallBack = object :
            ListenerCallbackLandingMap {
            override fun moreInfoButtonClick(ameniti: BaseAmenity) {
            }

            override fun onDismissedCameraTracking() {
                onCameraTrackingDismissed()
            }

            override fun onMapOtherPointClicked(point: Point) {
                // no op required
            }

            override fun onERPInfoRequested(erpID: Int) {
            }

            override fun poiMoreInfoButtonClick(ameniti: BaseAmenity) {
                //showPoiAmenityInfoRN(ameniti.id!!)
            }
        }

        activity?.let {
            exploreMapCongestionManager =
                ExploreMapCongestionManager(
                    mapboxMap,
                    it.applicationContext,
                    mapboxMap.getStyle()!!
                )
            exploreMapController = ExploreMapController()
            exploreMapController.setUp(
                mapView,
                this,
                userPreferenceUtil.isDarkTheme(),
                pCallBack = mapListenerCallBack
            )
            exploreMapController.setListenerShowTooltip(object :
                ExploreMapController.ListenerShowToolTip {
                override fun show() {
                    showCarParkWhenSelectedPOI()
                }

                override fun hide() {
                    hideCarParkWhenUnSelectPOI()
                }

                override fun hideUpdateBottomSheet() {
                    //no op
                }

            })
            exploreMapController.switchMapProfile(MapProfileType.STANDARD, false, false)
        }
    }


    private fun loadStyleImage(onFinish: () -> Unit) {
        CoroutineScope(Dispatchers.IO).launch {
            ExploreMapStateManager.contentDetailsResponse?.details?.let {
                exploreMapController.loadAmenityStyleImages(it)
            }
            withContext(Dispatchers.Main) {
                onFinish.invoke()
            }
        }
    }


    fun resetTrackerMode() {
        camera_tracker?.visibility = View.GONE
    }

    fun onCameraTrackingDismissed() {
        callAfterInitialized {
            camera_tracker?.visibility = View.VISIBLE
        }
    }

    @SuppressLint("MissingPermission")
    private fun initMap() {

        mapboxMap = mapView.getMapboxMap()

        exploreMapCamera = ExploreMapCamera(mapboxMap)
        viewportDataSource = MapboxNavigationViewportDataSource(
            mapView.getMapboxMap()
        )
        searchLocationMarkerViewManager = SearchLocationMarkerViewManager(mapView)
        navigationCamera = NavigationCamera(
            mapView.getMapboxMap(),
            mapView.camera,
            viewportDataSource
        )
        mapView.setBreezeDefaultOptions()

        initStyle(basicMapStyleReqd = true)

        initializeLocationComponent()
        registerLocationUpdates()

        val viewTreeObserver: ViewTreeObserver = mapView.viewTreeObserver
        if (viewTreeObserver.isAlive) {
            viewTreeObserver.addOnGlobalLayoutListener(object :
                ViewTreeObserver.OnGlobalLayoutListener {
                override fun onGlobalLayout() {
                    mapView.viewTreeObserver.removeOnGlobalLayoutListener(this)
                    viewportDataSource.evaluate()
                }
            })
        }

        activity?.let {
            mapView.camera.addCameraAnimationsLifecycleListener(
                NavigationScaleGestureHandler(
                    it,
                    navigationCamera,
                    mapboxMap,
                    mapView.gestures,
                    locationComponent,
                    {
                        viewportDataSource
                            .options
                            .followingFrameOptions
                            .zoomUpdatesAllowed = false
                    }
                ).apply { initialize() }
            )
        }

        mapView.gestures.addOnFlingListener {
            Timber.d("On fling is called")
            Analytics.logMapInteractionEvents(Event.DRAG_MAP, getScreenName())
        }

        mapView.gestures.addOnScaleListener(object : OnScaleListener {
            override fun onScale(detector: StandardScaleGestureDetector) {}

            override fun onScaleBegin(detector: StandardScaleGestureDetector) {}

            override fun onScaleEnd(detector: StandardScaleGestureDetector) {
                Timber.d("On scale end is called")
                Analytics.logMapInteractionEvents(Event.PINCH_MAP, getScreenName())
            }
        })

        mapView.gestures.addOnRotateListener(object : OnRotateListener {
            override fun onRotate(detector: RotateGestureDetector) {}

            override fun onRotateBegin(detector: RotateGestureDetector) {}

            override fun onRotateEnd(detector: RotateGestureDetector) {
                Timber.d("On rotate is called")
                Analytics.logMapInteractionEvents(Event.ROTATE_MAP, getScreenName())
            }

        })
    }

    @SuppressLint("MissingPermission")
    fun registerLocationUpdates() {
        LocationBreezeManager.getInstance().startLocationUpdates()
        //LocationBreezeManager.getInstance().registerLocationCallback(locationObserverCallBack)
    }

    private val carEventListener =
        ToAppRx.listenEvent(ToAppRxEvent::class.java)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
            }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ExploreMapStateManager.isOnExploreMapScreen = true
        (activity as DashboardActivity).hideKeyboard()
        findViews(view)
        initMap()
        initialize()
        OBUStripStateManager.getInstance()?.disable()
    }


    private fun showBottomSheetDetailInfo() {
        if (this.isVisible) {
            val navigationParams = requireActivity().getRNFragmentNavigationParams(
                sessionToken = myServiceInterceptor.getSessionToken(),
                fragmentTag = RNScreen.EXPLORE
            ).apply {
                putString(DATA, dataFromRN)
            }
            val initialProperties = Bundle().apply {
                putString(Constants.RN_TO_SCREEN, RNScreen.EXPLORE)
                putBundle(Constants.RN_CONSTANTS.NAVIGATION_PARAMS, navigationParams)
            }

            val reactNativeFragment = CustomReactFragment.newInstance(
                componentName = Constants.RN_CONSTANTS.COMPONENT_NAME,
                launchOptions = initialProperties,
                cleanOnDestroyed = false
            )

            //addFragment(reactNativeFragment, null, Constants.TAGS.TRAVEL_LOG_TAG)
            childFragmentManager
                .beginTransaction()
                .add(R.id.frBottomDetail, reactNativeFragment, "explore_bottom_sheet")
                .addToBackStack("explore_bottom_sheet")
                .commit()
        }
    }


    private fun showBottomSheetProgressTracked() {
        if (ExploreMapStateManager.contentDetailsResponse?.track_navigation == true) {
            val message_category =
                ExploreMapStateManager.contentDetailsResponse?.message_category ?: ""
            val message_type = ExploreMapStateManager.contentDetailsResponse?.message_type ?: ""
            val isDark = if (breezeUserPreferenceUtil.isDarkTheme()) {
                "dark"
            } else {
                "light"
            }
            val message_title = ExploreMapStateManager.contentDetailsResponse?.message_title ?: ""
            val linkConfig = BuildConfig.SERVER_HEADER +
                    String.format(
                        getString(R.string.format_string_progress_link),
                        message_category, message_type, isDark
                    )
            val navigationParams = requireActivity().getRNFragmentNavigationParams(
                sessionToken = myServiceInterceptor.getSessionToken(),
                fragmentTag = RNScreen.WEBVIEWPANEL
            ).apply {
                putString(Constants.RN_CONSTANTS.WEBVIEWURL, linkConfig)
                putString(Constants.RN_CONSTANTS.TITLE, message_title)
            }
            val initialProperties = Bundle().apply {
                putString(Constants.RN_TO_SCREEN, RNScreen.WEBVIEWPANEL)
                putBundle(Constants.RN_CONSTANTS.NAVIGATION_PARAMS, navigationParams)
            }
            val reactNativeFragment = CustomReactFragment.newInstance(
                componentName = Constants.RN_CONSTANTS.COMPONENT_NAME,
                launchOptions = initialProperties,
                cleanOnDestroyed = false
            )
            //addFragment(reactNativeFragment, null, Constants.TAGS.TRAVEL_LOG_TAG)
            childFragmentManager
                .beginTransaction()
                .add(R.id.frBottomDetailLayer2, reactNativeFragment, RNScreen.WEBVIEWPANEL)
                .addToBackStack(RNScreen.WEBVIEWPANEL)
                .commit()
        }

    }


    private fun findViews(view: View) {
        mapView = view.findViewById(R.id.mapView) as MapView
        progressBar = view.findViewById(R.id.progressBar) as FrameLayout?
        orange_container = view.findViewById(R.id.orange_container) as FrameLayout?
        rl_free_drive_info = view.findViewById(R.id.rl_free_drive_info) as LinearLayout?
        camera_tracker = view.findViewById(R.id.camera_tracker) as ImageView?
        imgTrackedInProgress = view.findViewById(R.id.imgTrackedInProgress) as ImageView?
        frBottomDetail = view.findViewById(R.id.frBottomDetail) as FrameLayout?
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            dataFromRN = it.getString(KEY_DATA)
        }
    }

    private fun updateContentSeenState(contentData: List<ExploreMaintenanceResponse>) {
        contentData.forEach { item ->
            item.contentId?.let { id ->
                BreezeUserPreference.getInstance(requireContext())
                    .addSeenContentId(id.toString())
            }
        }
    }

    fun initialize() {
        //Marking all content_ids as seen in user preferences, to update red dot button on dashboard
        mViewModel.exploreMapNew.observe(viewLifecycleOwner) { contentResponse ->
            mViewModel.exploreMapNew.removeObservers(viewLifecycleOwner)
            val shouldUpdateContentState = arguments?.getBoolean(
                KEY_UPDATE_CONTENT_SEEN_STATE,
                false
            ) == true
            contentResponse?.item?.takeIf {
                shouldUpdateContentState && it.isNotEmpty()
            }?.let {
                updateContentSeenState(it)
//                (activity as DashboardActivity).refreshExploreMapButton()
            }
        }
        mViewModel.getExploreMap()

        /**
         * check & handle button track progress walkathon
         */

        camera_tracker?.setOnClickListener {
            exploreMapController.clearALlCarPark()
            exploreMapController.resetAllViews()
            if (ExploreMapStateManager.contentDetailsResponse?.type == Constants.CONTENT_LAYER.CONTENT_TYPE_ZONE) {
                backTBRAmmenitiesDetail(false)
            }

            exploreMapController.resetPOIListZoomLevel()
            Analytics.logClickEvent(Event.RECENTER, getScreenName())
        }


        /**
         * handle event calculate free
         */
        disposableOpenCalculateFee = RxBus.listen(RxEvent.EventOpenCalculateFee::class.java)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                showCalculateParking(it.idCarPark)
            }
        compositeDisposable.add(disposableOpenCalculateFee)

        /**
         * handle event see more
         */
        disposaableEventOpenSeeMore = RxBus.listen(RxEvent.EventOpenSeeMore::class.java)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                showMore(it.idCarPark)
            }
        compositeDisposable.add(disposaableEventOpenSeeMore)

        /**
         * event handle carpark
         */
        activity?.let {
            mViewModel.mListCarpark.observe(viewLifecycleOwner, carParkObserver)
        }


        /**
         * event handle ammenities
         */
        mViewModel.contentDetailsResponse.observe(viewLifecycleOwner, contentSelectedObserver)


    }

    private fun showMore(idCarPark: String) {
        val fragmentTag = Constants.CARPARK_MOREINFO.TO_PARKING_MOREINFO_SCREEN
        val navigationParams = requireActivity().getRNFragmentNavigationParams(
            sessionToken = myServiceInterceptor.getSessionToken(),
            fragmentTag = fragmentTag
        )
        navigationParams.putString(Constants.RN_CONSTANTS.BASE_URL, BuildConfig.SERVER_HEADER)
        navigationParams.putString(Constants.CARPARK_MOREINFO.PARKING_ID, idCarPark)
        navigationParams.putString(
            Constants.CARPARK_MOREINFO.AWS_ID_TOKEN,
            myServiceInterceptor.getSessionToken()!!
        )

        val initialProperties = bundleOf(
            Constants.RN_TO_SCREEN to fragmentTag,
            Constants.RN_CONSTANTS.NAVIGATION_PARAMS to navigationParams
        )
        val bottomPanelFragment = CustomReactFragment.newInstance(
            componentName = Constants.RN_CONSTANTS.COMPONENT_NAME,
            launchOptions = initialProperties,
            cleanOnDestroyed = false
        )

        childFragmentManager
            .beginTransaction()
            .add(R.id.frBottomDetailLayer2, bottomPanelFragment, fragmentTag)
            .addToBackStack(fragmentTag)
            .commit()
    }


    private fun showCalculateParking(parkingID: String?) {
        val navigationParams = requireActivity().getRNFragmentNavigationParams(
            sessionToken = (activity as? DashboardActivity)?.myServiceInterceptor?.getSessionToken(),
            fragmentTag = RNScreen.PARKING_CALCULATOR
        ).apply {
            putString("parkingID", parkingID)
            putLong("start_timestamp", System.currentTimeMillis())
            ExploreMapStateManager.currentSelectedPOILocation?.let { loc ->
                putString(Constants.RN_CONSTANTS.FOCUSED_LAT, loc.latitude.toString())
                putString(Constants.RN_CONSTANTS.FOCUSED_LONG, loc.longitude.toString())
            }
        }
        val initialProperties = Bundle().apply {
            putString(Constants.RN_TO_SCREEN, RNScreen.PARKING_CALCULATOR)
            putBundle(Constants.RN_CONSTANTS.NAVIGATION_PARAMS, navigationParams)
        }

        val reactNativeFragment = CustomReactFragment.newInstance(
            componentName = Constants.RN_CONSTANTS.COMPONENT_NAME,
            launchOptions = initialProperties,
            cleanOnDestroyed = false
        )

        childFragmentManager
            .beginTransaction()
            .add(R.id.frBottomDetailLayer2, reactNativeFragment, RNScreen.PARKING_CALCULATOR)
            .addToBackStack(RNScreen.PARKING_CALCULATOR)
            .commit()
    }


    fun backTBRAmmenitiesDetail(updateBottomSheet: Boolean = true) {
        activity?.runOnUiThread {
            val wasFragmentRemoved = removeAllFragmentInFrameLayer2()
            if (updateBottomSheet || wasFragmentRemoved) {
                exploreMapController.updateBottomSheetState(1, RNScreen.EXPLORE)
            }
            exploreMapController.removeALlTooltip()
            hideCarParkWhenUnSelectPOI()
        }
    }


    fun removeAllFragmentInFrameLayer2(): Boolean {
        val currentFragment = childFragmentManager.findFragmentById(R.id.frBottomDetailLayer2)
        if (currentFragment != null) {
            childFragmentManager.beginTransaction().remove(currentFragment).commit()
            return true
        }
        return false
    }


    private fun showCarParkWhenSelectedPOI() {
        fetchFilteredCarParks()
    }


    private fun hideCarParkWhenUnSelectPOI() {
        exploreMapController.clearALlCarPark()
    }


    /**
     * get config circle zone TBR
     */
    private fun getZoneCongestionDetails(layerCode: String, drawMapLayers: () -> Unit) {
        //val profileSelected = ExploreMapStateManager.selectedMapProfile!!
        mViewModel.mResponseCongestionDetail.observe(viewLifecycleOwner, Observer {
            mViewModel.mResponseCongestionDetail.removeObservers(this)
            it?.let {
                /**
                 * draw circle layer first
                 */
                if (this::exploreMapController.isInitialized) {
                    exploreMapCongestionManager.reDrawCircle(
                        true,
                        exploreMapCamera.edgeCircleInsets
                    )
                    exploreMapCongestionManager.updateColorCircleLayer(it, layerCode)
                    drawMapLayers.invoke()
                }
            }
        })
        mViewModel.getZoneCongestionDetails(layerCode)
    }


    private fun fetchFilteredCarParks() {
        val carParkAvailability =
            BreezeUserPreference.getInstance(requireActivity()).getStateCarpParkOption()

        if (carParkAvailability == CarParkViewOption.HIDE) return
        var tempLocation = ExploreMapStateManager.currentLocationFocus
        if (this::exploreMapController.isInitialized) {
            ExploreMapStateManager.activeLocationUse = tempLocation
        }
        /**
         * register & handle notification have no car park in 500M
         */
        handleShowNotyNoCarpark()
        val destinationName: String? = null
        tempLocation?.let {
            mViewModel.getCarParkDataShowInMap(
                it,
                destinationName,
                carParkAvailability.mode
            )
        }
    }

    private fun handleShowNotyNoCarpark() {
        activity?.let {
            mViewModel.isNotExistAnyCp.observe(it) { isNotExistAnyCpIn500 ->
                if (this::exploreMapController.isInitialized && isNotExistAnyCpIn500) {
                    GlobalNotification()
                        .setIcon(R.drawable.ic_notification_no_carpark)
                        .setTheme(GlobalNotification.ThemeTutorial)
                        .setTitle(getString(R.string.nearby_carpark))
                        .setDescription(getString(R.string.msg_nearby_carpark_found))
                        .setActionText(null)
                        .setActionListener(null)
                        .setDismissDurationSeconds(10)
                        .show(activity)
                }
            }
        }
    }

    private val carParkObserver = Observer<ArrayList<BaseAmenity>> {
        it?.let {
            if (this::exploreMapController.isInitialized) {
                exploreMapController.handleCarkPark(
                    it,
                    isMoveCameraWrapperAllCarpark = true,
                    false
                )
            }
        }
    }


    private fun initStyle(basicMapStyleReqd: Boolean = false, after: () -> Unit = {}) {
        (activity as? BaseActivity<*, *>)?.let {
            mapboxMap.loadStyleUri(
                it.getMapboxStyle(basicMapStyleReqd)
            ) { style ->
                if (isFragmentDestroyed()) {
                    return@loadStyleUri
                }
                /**
                 * remove layer
                 * illegalparkingcamera
                 * speedcamera
                 * redlightcamera
                 */
                style.removeStyleLayer("illegalparkingcamera")
                style.removeStyleLayer("speedcamera")
                style.removeStyleLayer("redlightcamera")

                after()
                mapView.gestures.addOnMoveListener(object : OnMoveListener {
                    override fun onMoveBegin(detector: MoveGestureDetector) {
                        camera_tracker?.visibility = View.VISIBLE
                    }

                    override fun onMove(detector: MoveGestureDetector): Boolean {
                        return false
                    }

                    override fun onMoveEnd(detector: MoveGestureDetector) {}
                })
                initExploreMapController()
                showBottomSheetDetailInfo()
            }
        }
    }


    private fun initializeLocationComponent() {
        locationComponent = mapView.location.apply {
            enabled = true
            pulsingEnabled = true
        }
        defaultLocationPuck = LocationPuck2D(
            bearingImage = activity?.getDrawable(R.drawable.cursor)
        )
        locationComponent.locationPuck = defaultLocationPuck
        defaultLocationProvider = locationComponent.getLocationProvider()
    }

    override fun inflateViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?,
        attachToContainer: Boolean
    ) = FragmentExploreMapsBinding.inflate(inflater, container, attachToContainer)

    override fun getViewModelReference(): ExploreMapsFragmentViewModel {
        return mViewModel
    }

    override fun getScreenName(): String {
        return Screen.FRAGMENT_EXPLORE
    }

    override fun onDestroyView() {
        super.onDestroyView()
        OBUStripStateManager.getInstance()?.enable()
        ExploreMapStateManager.isOnExploreMapScreen = false
        if (::disposableOpenCalculateFee.isInitialized) {
            compositeDisposable.remove(disposableOpenCalculateFee)
        }
        if (::disposaableEventOpenSeeMore.isInitialized) {
            compositeDisposable.remove(disposaableEventOpenSeeMore)
        }
        compositeDisposable.remove(carEventListener)
        compositeDisposable.dispose()

        distroyCallback?.let {
            it.invoke()
        }
    }

//    override suspend fun generateNavigationLogData(filePath: String) {}

    override fun adjustRecenterButton(height: Int, index: Int, fromScreen: String) {
        activity?.runOnUiThread {
            ExploreMapStateManager.landingBottomSheetHeight = height
            ExploreMapStateManager.landingBottomSheetState = index
            exploreMapCamera.overviewEdgeInsets = EdgeInsets(
                50.0 * pixelDensity,
                10.0 * pixelDensity,
                (height.toDouble() + 20) * pixelDensity,
                10.0 * pixelDensity,
            )
            exploreMapCamera.edgeCircleInsets = EdgeInsets(
                30.0 * pixelDensity, 60.0 * pixelDensity,
                (height.toDouble() + 130) * pixelDensity,
                60.0 * pixelDensity,
            )
            callAfterInitialized {
                exploreMapController.handleUpdateCenterOfMarker()
            }

            rl_free_drive_info?.setPadding(
                0, 0, 0,
                com.mapbox.android.gestures.Utils.dpToPx(height.toFloat())
                    .toInt()
            )

        }
    }

    fun addDestroyCallback(callback: () -> Unit) {
        distroyCallback = callback
    }

    fun callAfterInitialized(callback: () -> Unit) {
        activity?.runOnUiThread {
            mapboxMap.getStyle {
                callback.invoke()
            }
        }
    }


    /**
     * get selected content details from API
     */
    fun getSelectedContentDetails(content_id: Int) {
        activity?.let { activity ->
            mViewModel.getSelectedContentDetails(content_id.toString())
        }
    }

    fun closeParkingCalculator() {
        activity?.runOnUiThread {
            val currentFragment = childFragmentManager.findFragmentById(R.id.frBottomDetailLayer2)
            if (currentFragment != null) {
                if (currentFragment.isAdded) {
                    childFragmentManager.beginTransaction().remove(currentFragment).commit()
                    exploreMapController.updateBottomSheetState(0, RNScreen.EXPLORE)
                    exploreMapController.removeALlTooltip()
                }
            }
        }
    }

    fun saveCustomAnalytics(amenityId: String, layerCode: String, amenityType: String) {
        mViewModel.saveCustomAnalytics(
            Event.USER_CLICK,
            Event.POI_LAYER,
            getScreenName(),
            amenityId,
            layerCode,
            amenityType
        )
    }

    fun closeProgressTrackingScreen() {
        activity?.runOnUiThread {
            val currentFragment = childFragmentManager.findFragmentById(R.id.frBottomDetailLayer2)
            if (currentFragment != null) {
                if (currentFragment.isAdded) {
                    childFragmentManager.beginTransaction().remove(currentFragment).commit()
                }
            }
        }
    }

    fun refreshContent() {
        mViewModel.refreshContent()
    }

    companion object {
        const val DATA = "data"
        const val KEY_DATA = "ExploreMap.Data"
        const val KEY_UPDATE_CONTENT_SEEN_STATE = "ExploreMap.ContentSeenState"
    }
}
package com.ncs.breeze.common.appSync

import com.amazonaws.mobileconnectors.appsync.sigv4.CognitoUserPoolsAuthProvider
import com.ncs.breeze.common.rxbus.RxBus
import com.ncs.breeze.common.rxbus.RxEvent
import com.ncs.breeze.common.utils.AWSUtils
import com.ncs.breeze.common.utils.FetchTokenError
import kotlinx.coroutines.*
import timber.log.Timber

class BreezeCognitoUserPoolsAuthProvider() :
    CognitoUserPoolsAuthProvider {

    private var fetchTokenResult: Pair<String?, FetchTokenError>? = null

    /**
     * Fetches token from the Cognito User Pools client for the current user.
     */
    private fun fetchToken() {
        runBlocking {
            fetchTokenResult = AWSUtils.fetchAuthAccessTokenSync()

            Timber.e("fetchTokenResult: $fetchTokenResult")
        }
    }

    override fun getLatestAuthToken(): String {
        fetchToken()
        GlobalScope.launch(Dispatchers.Main) {
            delay(3000)
            if (
                fetchTokenResult?.second == FetchTokenError.SIGN_OUT
                && fetchTokenResult?.first.isNullOrEmpty()
            ) {
                Timber.d("Can not fetch token: Logout")
                RxBus.publish(RxEvent.LogoutGoCreateScreen())
            }
        }
        return fetchTokenResult?.first ?: ""
    }
}

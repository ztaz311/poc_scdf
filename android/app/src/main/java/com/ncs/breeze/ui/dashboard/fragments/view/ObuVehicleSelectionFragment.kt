package com.ncs.breeze.ui.dashboard.fragments.view

import android.app.Dialog
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.core.os.bundleOf
import androidx.fragment.app.setFragmentResult
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.ncs.breeze.R
import com.ncs.breeze.common.extensions.android.getApp
import com.breeze.model.api.response.ObuVehicleDetails
import com.breeze.model.constants.Constants
import com.breeze.customization.view.BreezeVehicleSelectionRadioButton
import com.breeze.customization.view.BreezeVehicleSelectionRadioGroup
import com.ncs.breeze.ui.dashboard.fragments.adapter.ObuVehicleTypes


open class ObuVehicleSelectionFragment : BottomSheetDialogFragment() {


    private var vehicleNumberDetails: ArrayList<ObuVehicleDetails>? = null
    private var selectedVehicleNumber: String? = null

    private lateinit var close_btn: TextView
    private lateinit var cancel_btn: TextView
    private lateinit var done_btn: TextView
    private lateinit var vehicle_radio_group: BreezeVehicleSelectionRadioGroup


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let { bundle ->
            extractData(bundle)
        }
    }

    private fun extractData(bundle: Bundle) {
        vehicleNumberDetails =
            bundle.getSerializable(Constants.VEHICLE_NUMBERS) as ArrayList<ObuVehicleDetails>?
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    private fun selectItem(
        vehicleNumber: String,
        vehicleRadioButton: BreezeVehicleSelectionRadioButton
    ) {
        selectedVehicleNumber = vehicleNumber
        vehicleRadioButton.setSelectedState()
        vehicle_radio_group.setOtherButtonsToUnselectedState(vehicleRadioButton)
    }

    private fun confirmSelection(vehicleNumber: String) {
        setFragmentResult(
            Constants.VEHICLE_SELECTION_FRAGMENT_REQUEST_KEY,
            bundleOf(
                Constants.VEHICLE_NUMBER to vehicleNumber
            )
        )
    }


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState) as BottomSheetDialog

        val contentView = View.inflate(context, R.layout.fragment_obu_vehicle_selection, null)
        contentView?.let { view ->
            close_btn = view.findViewById(R.id.close_btn)
            cancel_btn = view.findViewById(R.id.tv_cancel)
            done_btn = view.findViewById(R.id.tv_done)
            vehicle_radio_group =
                view.findViewById(R.id.vehicle_radio_group)
            close_btn.setOnClickListener {
                dismiss()
            }
            cancel_btn.setOnClickListener {
                dismiss()
            }
            done_btn.setOnClickListener {
                selectedVehicleNumber?.let {
                    confirmSelection(it)
                }
                dismiss()
            }

            activity?.let { act ->
                vehicleNumberDetails?.let { vehicleNumberData ->
                    val obuDataList = getApp()?.obuConnectionHelper?.getUserConnectedOBUDevices()
                    vehicleNumberData.reversed().forEachIndexed { index, vehicleData ->
                        val vehicleRadioButton = BreezeVehicleSelectionRadioButton(act)
                        vehicleRadioButton.mSelectedLeftIcon =
                            ObuVehicleTypes.getVehicleByType(vehicleData.vehicleType)?.resourceId
                                ?: R.drawable.obu_light_vehicle
                        vehicleRadioButton.updateImageDrawable()
                        vehicleRadioButton.mTextView.text =
                            obuDataList?.find { it.obuName == vehicleData.vehicleNumber }
                                ?.getDisplayedVehicleName() ?: vehicleData.vehicleNumber
                        vehicleRadioButton.setOnClickListener {
                            selectItem(vehicleData.vehicleNumber, vehicleRadioButton)
                        }
                        vehicle_radio_group.addView(vehicleRadioButton)
                        if (index == 0) {
                            selectItem(vehicleData.vehicleNumber, vehicleRadioButton)
                        }
                    }
                }
            }
        }
        dialog.setContentView(contentView)
        dialog.setCanceledOnTouchOutside(false)
        dialog.setCancelable(false)
        dialog.apply {
            setOnShowListener {
                val bottomSheet = findViewById<View?>(com.google.android.material.R.id.design_bottom_sheet)
                if (bottomSheet != null) {
                    bottomSheet.setBackgroundResource(android.R.color.transparent)
                    val behavior: BottomSheetBehavior<*> = BottomSheetBehavior.from(bottomSheet)
                    behavior.isDraggable = false
                }
            }
        }
        return dialog
    }

}

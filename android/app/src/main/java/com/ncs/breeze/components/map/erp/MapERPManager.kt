package com.ncs.breeze.components.map.erp


import android.app.Activity
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.Event
import com.breeze.model.api.response.ERPResponseData
import com.breeze.model.extensions.dpToPx
import com.ncs.breeze.common.utils.BreezeMapZoomUtil
import com.ncs.breeze.common.utils.ERPControllerData
import com.ncs.breeze.common.utils.ERPControllerData.centerPoint
import com.google.android.gms.maps.model.LatLng
import com.mapbox.geojson.Point
import com.mapbox.maps.EdgeInsets
import com.mapbox.maps.MapView
import com.mapbox.maps.plugin.gestures.gestures
import com.mapbox.turf.TurfConstants
import com.mapbox.turf.TurfMeasurement
import com.ncs.breeze.components.map.profile.ListenerCallbackLandingMap
import com.ncs.breeze.components.marker.markerview2.ERPMarkerViewManager
import com.ncs.breeze.components.marker.markerview2.ERpMarkerView
import com.ncs.breeze.ui.dashboard.manager.DashboardMapStateManager

class MapERPManager(
    val mapView: MapView,
    val activity: Activity,
    val screenName: String,
    private val isDisableClickIconErp: Boolean,
    var callBack: ListenerCallbackLandingMap
) {


    /**
     * for erp
     */
    var erpMarkerViewManager: ERPMarkerViewManager = ERPMarkerViewManager(mapView)


    init {
        mapView.gestures.addOnMapClickListener {
            erpMarkerViewManager.hideAllTooltips()
            callBack.onMapOtherPointClicked(it)
            false
        }
    }


    fun hideAllTooltips() {
        erpMarkerViewManager.hideAllTooltips()
    }

    fun removeAllMarkers() {
        erpMarkerViewManager.removeAllMarkers()
    }


    fun handleERP(
        timeToCheckERP: Long,
        listErpNearByLocation: ArrayList<ERPResponseData.Erp>,
    ) {
        erpMarkerViewManager?.removeAllMarkers()
        ERPControllerData.erpResponse?.apply {
            listErpNearByLocation.forEach {
                erpMarkerViewManager?.addMarker(
                    getERPMarker(
                        it,
                        timeToCheckERP = timeToCheckERP,
                        screenName = screenName,
                        forceShowErp = true,
                        showInfoIcon = true,
                        zoom = 15.0,
                        showMinimalView = false,
                        forceCheckNextDayERP = true,
                    )
                )
            }
        }
    }

    private fun getERPMarker(
        item: ERPResponseData.Erp,
        showInfoIcon: Boolean = false,
        timeToCheckERP: Long,
        forceShowErp: Boolean = false,
        screenName: String,
        zoom: Double?,
        showMinimalView: Boolean,
        forceCheckNextDayERP: Boolean,
        pIsTimeAlreadyChanged: Boolean = false
    ): ERpMarkerView {
        val erpViewMarker = ERpMarkerView(
            mapboxMap = mapView.getMapboxMap(),
            latLng = LatLng(item.startlat.toDouble(), item.startlong.toDouble()),
            activity = activity,
            showInfoIcon = showInfoIcon,
            isDisableClickErpLayout = this.isDisableClickIconErp,
            showMinimalView = showMinimalView
        )
        if (showInfoIcon) {
            erpViewMarker.erpMarkerClickListener = object : ERpMarkerView.ERPMarkerClickListener {
                override fun onERPInfoSelected() {
                    callBack?.onERPInfoRequested(item.erpid)
                }
            }
        }
        erpViewMarker.fillDataToMarkerView(
            item = item,
            eRPIconClick = {
                Analytics.logClickEvent(Event.TAP_ERP_CLICK, screenName)
                callBack?.onDismissedCameraTracking()
                erpMarkerViewManager?.swithAllToDefaultState()

            },
            heightBottomSheet = DashboardMapStateManager.landingBottomSheetHeight,
            timeToCheckERP = timeToCheckERP,
            forceShowErp = forceShowErp,
            screenName = screenName,
            zoom = zoom,
            forceCheckNextDayERP = forceCheckNextDayERP,
            isTimeAlreadyChanged = pIsTimeAlreadyChanged
        )

        return erpViewMarker
    }

    fun calculateFurthestErpDistance(
        erpList: ArrayList<ERPResponseData.Erp>,
        targetPoint: Point,
    ): Double {

        val size = erpList.size
        val centerPoint = erpList[0].centerPoint()
        val lastPoint = erpList[size - 1].centerPoint()
        var furthestDistance = 0.0
        for (i in 1 until (size)) {
            val distance = TurfMeasurement.distance(
                centerPoint,
                erpList[i].centerPoint(),
                TurfConstants.UNIT_METRES
            )
            if (distance > furthestDistance) {
                furthestDistance = distance
            }
        }

        val distance = TurfMeasurement.distance(
            lastPoint,
            targetPoint,
            TurfConstants.UNIT_METRES
        )
        if (distance > furthestDistance) {
            furthestDistance = distance
        }

        return furthestDistance
    }


    //TODO: too many optional params, can refactor to erp options class
    fun handleSearchERP(
        erpID: Int,
        expandAutomatically: Boolean = true,
        zoomTo: Boolean = expandAutomatically,
        showInfoIcon: Boolean = false,
        timeToCheckERP: Long,
        forceShowErp: Boolean = false,
        screenName: String,
        zoomValue: Double?,
        showMinimalView: Boolean,
        forceCheckNextDayERP: Boolean,
        isTimeAlreadyChanged: Boolean = false
    ) {

        erpMarkerViewManager?.hideAllTooltips()

        /**
         * filter erp in list
         */
        val currentErpSearch = ERPControllerData.getERPByID(erpID)

        currentErpSearch?.let { erp ->
            val markerSearchErp = getERPMarker(
                erp,
                showInfoIcon,
                timeToCheckERP,
                forceShowErp,
                screenName,
                zoomValue,
                showMinimalView,
                forceCheckNextDayERP,
                pIsTimeAlreadyChanged = isTimeAlreadyChanged
            )
            /**
             * show tooltip
             */
            markerSearchErp.showMarker()
            if (expandAutomatically) {
                markerSearchErp.setStateMarker(ERpMarkerView.SELECTED)
            }

            erpMarkerViewManager?.addMarker(markerSearchErp)

            if (zoomTo) {
                BreezeMapZoomUtil.recenterMapToLocation(
                    mapView.getMapboxMap(),
                    lat = erp.startlat.toDouble(),
                    lng = erp.startlong.toDouble(),
                    edgeInset = EdgeInsets(
                        10.0.dpToPx(),
                        20.0.dpToPx(),
                        160.0.dpToPx(),
                        20.0.dpToPx()
                    ),
                    zoomLevel = 14.0
                )
            }
        }
    }

    //TODO: too many optional params, can refactor to erp options class
    fun handleSearchNearbyERP(
        erpID: Int,
        listErpNearByLocation: ArrayList<ERPResponseData.Erp>
    ) {
        erpMarkerViewManager?.hideAllTooltips()
        listErpNearByLocation.forEach {
            if (it.erpid == erpID) {
                handleShowErpSearch(it)
                return
            }
        }
    }


    private fun handleShowErpSearch(erpIDSearch: ERPResponseData.Erp) {
        erpMarkerViewManager?.showERPByID(erpIDSearch.erpid)
        /**
         * move camera to
         */
        callBack?.onDismissedCameraTracking()

        BreezeMapZoomUtil.recenterMapToLocation(
            mapView.getMapboxMap(),
            lat = erpIDSearch.startlat.toDouble(),
            lng = erpIDSearch.startlong.toDouble(),
            edgeInset = EdgeInsets(
                10.0.dpToPx(),
                20.0.dpToPx(),
                180.0.dpToPx(),
                20.0.dpToPx()
            ),
            zoomLevel = 14.0
        )
    }


}
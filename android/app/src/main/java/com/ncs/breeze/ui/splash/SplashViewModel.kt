package com.ncs.breeze.ui.splash

import android.app.Application
import androidx.lifecycle.viewModelScope
import com.breeze.model.api.ErrorData
import com.breeze.model.api.response.UserDataResponse
import com.ncs.breeze.common.remote.ApiHelper
import com.ncs.breeze.common.remote.ApiObserver
import com.ncs.breeze.common.utils.AWSUtils
import com.ncs.breeze.components.SingleLiveEvent
import com.ncs.breeze.ui.base.BaseViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.*
import javax.inject.Inject
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException

class SplashViewModel @Inject constructor(application: Application, apiHelper: ApiHelper) :
    BaseViewModel(application, apiHelper) {
    var tncAccepted: SingleLiveEvent<Boolean> = SingleLiveEvent()
    var amenityResetResponse: SingleLiveEvent<Boolean> = SingleLiveEvent()

    fun getUserData(onSuccess: (data: UserDataResponse) -> Unit) {
        viewModelScope.launch {
            apiHelper.getUserData()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : ApiObserver<UserDataResponse>(compositeDisposable) {
                    override fun onSuccess(data: UserDataResponse) {
                        tncAccepted.value = data.tncAcceptStatus ?: false
                        breezeUserPreferenceUtil.saveUserCreatedDate(data.createdTimeStamp)
                        onSuccess.invoke(data)
                    }

                    override fun onError(e: ErrorData) {
                        tncAccepted.postValue(false)
                    }
                })
        }
    }


    fun fetchToken() {
        var jobFetchToken: Job? = null
        var jobCountDown: Job? = null
        viewModelScope.launch(Dispatchers.IO) {
            var isHaveResponse = false
            jobFetchToken  = launch {
                kotlin.runCatching {
                    suspendCancellableCoroutine {
                        AWSUtils.checkUserAuthSession(object : AWSUtils.AuthSessionCallback {
                            override fun onAuthSuccess(token: String?) {
                                it.resume(token)
                                isHaveResponse = true
                                jobCountDown?.cancel()
                            }

                            override fun onAuthFailed() {
                                it.resumeWithException(Exception())
                                isHaveResponse = true
                                jobCountDown?.cancel()
                            }
                        })
                    }
                }.onSuccess { token ->
                    if (myServiceInterceptor.isTokenValid(token)) {
                        myServiceInterceptor.setSessionToken(token)
                        amplifyFetchUserAttributes()
                    } else {
                        userLoggedIn.postValue(USER_STATUS.USER_LOGGED_OUT)
                    }
                }.onFailure {
                    userLoggedIn.postValue(USER_STATUS.USER_LOGGED_OUT)
                }
            }

            jobCountDown = launch {
                delay(4000)
                if (!isHaveResponse) {
                    jobFetchToken?.cancel()
                    userLoggedIn.postValue(USER_STATUS.USER_LOGGED_OUT)
                }
            }

        }
    }

}
package com.ncs.breeze.common.utils

import android.location.Location

object LocationUtils {
    fun generateLocationInstance(lat: Double = 0.0, long: Double = 0.0) = Location("").apply {
        latitude = lat
        longitude = long
    }
}
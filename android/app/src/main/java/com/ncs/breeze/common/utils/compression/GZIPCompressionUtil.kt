package com.ncs.breeze.common.utils.compression

import java.io.*
import java.util.zip.GZIPInputStream
import java.util.zip.GZIPOutputStream

object GZIPCompressionUtil {

    @Throws(IOException::class)
    suspend fun compressToGzip(any: Any): ByteArray {
        val inputByteArrayStream = ByteArrayOutputStream()
        val outPutByteArrayStream = ByteArrayOutputStream()
        val gzip = GZIPOutputStream(outPutByteArrayStream)
        try {
            var out: ObjectOutputStream? = null;
            out = ObjectOutputStream(inputByteArrayStream);
            out.writeObject(any);
            out.flush();
            val byteArray = inputByteArrayStream.toByteArray();
            gzip.write(byteArray)
            gzip.close();
            val compressed: ByteArray = outPutByteArrayStream.toByteArray()
            return compressed
        } finally {
            inputByteArrayStream.close()
            outPutByteArrayStream.close()
            gzip.close()
        }
    }


    @Throws(IOException::class)
    suspend fun decompressFromGzip(byteArray: ByteArray): Any {
        val time = System.currentTimeMillis()
        val gzipInputStream = ByteArrayInputStream(byteArray)
        var gis: GZIPInputStream? = null
        var objInputStream: ByteArrayInputStream? = null
        try {
            gis = GZIPInputStream(gzipInputStream)
            objInputStream = ByteArrayInputStream(gis.readBytes())
            val objectInput = ObjectInputStream(objInputStream)
            return objectInput.readObject()
        } finally {
            gzipInputStream.close()
            gis?.close()
            objInputStream?.close()
        }
    }

}
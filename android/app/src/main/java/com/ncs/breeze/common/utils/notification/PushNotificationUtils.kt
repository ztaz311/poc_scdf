package com.ncs.breeze.common.utils.notification

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import com.ncs.breeze.R
import com.breeze.model.extensions.round
import com.breeze.model.obu.OBURoadEventData
import com.ncs.breeze.notification.FCMListenerService
import com.ncs.breeze.ui.dashboard.activity.DashboardActivity

object PushNotificationUtils {

    const val NOTIFICATION_FLAGS: Int =
        PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE

    fun createBreezeNotification(
        context: Context,
        notificationId: Int,
        channelId: String,
        channelName: String,
        title: String,
        description: String,
        intent: Intent? = null,
        importanceLevel: Int = NotificationManager.IMPORTANCE_DEFAULT
    ) {
        val notificationManager =
            context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        setupNotificationChannel(
            notificationManager,
            channelId,
            channelName,
            importanceLevel
        )

        val builder: NotificationCompat.Builder =
            NotificationCompat.Builder(context, channelId)
        builder.setContentTitle(title)
        builder.setContentText(description)
        builder.setSmallIcon(R.drawable.ic_stat_notification_small)
        builder.setAutoCancel(true)
        builder.setChannelId(channelId)
        builder.setColor(ContextCompat.getColor(context, R.color.themed_breeze_primary))
        builder.setStyle(NotificationCompat.BigTextStyle().bigText(description))

        //val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        //builder.setSound(defaultSoundUri)

//        if (message!!.length > 50) {
//            val trimmedMessage = message.subSequence(0, 50).toString() + ".."
//            notificationBuilder.setContentText(trimmedMessage)
//            notificationBuilder.setStyle(NotificationCompat.BigTextStyle().bigText(message))
//        } else {
//            notificationBuilder.setContentText(message)
//        }

        if (intent != null) {
            val contentIntent = PendingIntent.getActivity(
                context,
                0,
                intent,
                NOTIFICATION_FLAGS
            )
            builder.setContentIntent(contentIntent)
        }

        return notificationManager.notify(notificationId, builder.build())
    }

    fun createOBUChargeResultNotification(context: Context, data: OBURoadEventData){
        val title = when (data.eventType) {
            OBURoadEventData.EVENT_TYPE_PARKING_SUCCESS,
            OBURoadEventData.EVENT_TYPE_PARKING_FAILURE ->
                "Parking Fee - ${data.chargeAmount}"

            OBURoadEventData.EVENT_TYPE_ERP_FAILURE,
            OBURoadEventData.EVENT_TYPE_ERP_SUCCESS ->
                "ERP Fee - ${data.chargeAmount}"

            else -> ""
        }
        val message = when (data.eventType) {
            OBURoadEventData.EVENT_TYPE_PARKING_SUCCESS,
            OBURoadEventData.EVENT_TYPE_ERP_SUCCESS -> "Deduction successful"

            OBURoadEventData.EVENT_TYPE_ERP_FAILURE,
            OBURoadEventData.EVENT_TYPE_PARKING_FAILURE -> "Deduction failed"

            else -> ""
        }

        val intent = Intent(context, DashboardActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_REORDER_TO_FRONT
        }
        createBreezeNotification(
            context = context,
            notificationId = FCMListenerService.UPDATE_NOTIFICATION_ID,
            channelId = FCMListenerService.NOTIFICATION_CHANNEL_ID_GENERAL,
            channelName = FCMListenerService.NOTIFICATION_CHANNEL_NAME_GENERAL,
            title = title,
            description = message,
            intent = intent,
            importanceLevel = NotificationManager.IMPORTANCE_HIGH
        )
    }

    fun createLowCardNotification(context: Context, balanceSGD: Double){
        val intent = Intent(context, DashboardActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_REORDER_TO_FRONT
        }
        createBreezeNotification(
            context = context,
            notificationId = FCMListenerService.UPDATE_NOTIFICATION_ID,
            channelId = FCMListenerService.NOTIFICATION_CHANNEL_ID_GENERAL,
            channelName = FCMListenerService.NOTIFICATION_CHANNEL_NAME_GENERAL,
            title = "Low card balance - $${balanceSGD.round(2)}",
            description = "Please top up your card",
            intent = intent,
            importanceLevel = NotificationManager.IMPORTANCE_HIGH
        )
    }

    fun setupNotificationChannel(
        notificationManager: NotificationManager,
        channelId: String,
        channelName: String,
        importanceLevel: Int
    ) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val navChannel = NotificationChannel(
                channelId,
                channelName,
                importanceLevel
            )

            //navChannel.setShowBadge(true)
            ////channel.canShowBadge()
            ////channel.enableLights(true)
            ////channel.lightColor = Color.BLUE
            //navChannel.enableVibration(true)
            //navChannel.vibrationPattern = longArrayOf(100, 200, 300, 400, 500)

            //navChannel.description = FCMListenerService.CHANNEL_DESCRIPTION

            notificationManager.createNotificationChannel(navChannel)
        }
    }
}
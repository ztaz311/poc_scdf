package com.ncs.breeze.ui.dashboard.fragments.view

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.InputFilter
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import androidx.core.view.isVisible
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import com.breeze.customization.view.BreezeEditText
import com.breeze.model.api.request.ShortcutDetails
import com.breeze.model.constants.Constants
import com.breeze.model.extensions.dpToPx
import com.breeze.model.extensions.safeDouble
import com.google.android.gms.maps.model.LatLng
import com.mapbox.android.gestures.StandardScaleGestureDetector
import com.mapbox.geojson.Feature
import com.mapbox.geojson.Point
import com.mapbox.maps.CameraOptions
import com.mapbox.maps.EdgeInsets
import com.mapbox.maps.MapboxMap
import com.mapbox.maps.RenderedQueryOptions
import com.mapbox.maps.ScreenBox
import com.mapbox.maps.ScreenCoordinate
import com.mapbox.maps.extension.style.expressions.dsl.generated.literal
import com.mapbox.maps.extension.style.style
import com.mapbox.maps.plugin.animation.CameraAnimatorOptions
import com.mapbox.maps.plugin.animation.camera
import com.mapbox.maps.plugin.gestures.OnMapClickListener
import com.mapbox.maps.plugin.gestures.OnScaleListener
import com.mapbox.maps.plugin.gestures.addOnMapClickListener
import com.mapbox.maps.plugin.gestures.gestures
import com.mapbox.maps.plugin.scalebar.scalebar
import com.ncs.breeze.R
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.Event
import com.ncs.breeze.common.analytics.Screen
import com.ncs.breeze.common.extensions.android.getApp
import com.ncs.breeze.common.utils.BreezeUserPreferenceUtil
import com.ncs.breeze.components.marker.markerview.MarkerView
import com.ncs.breeze.components.marker.markerview.MarkerViewManager
import com.ncs.breeze.databinding.FragmentNewAddressBinding
import com.ncs.breeze.databinding.MarkerWindowBinding
import com.ncs.breeze.reactnative.nativemodule.ReactNativeEventEmitter
import com.ncs.breeze.ui.base.BaseFragment
import com.ncs.breeze.ui.dashboard.activity.DashboardActivity
import com.ncs.breeze.ui.dashboard.fragments.view.NewAddressFragment.ShortcutKeys.HOME
import com.ncs.breeze.ui.dashboard.fragments.view.NewAddressFragment.ShortcutKeys.WORK
import com.ncs.breeze.ui.dashboard.fragments.viewmodel.NewAddressViewModel
import timber.log.Timber
import javax.inject.Inject


class NewAddressFragment : BaseFragment<FragmentNewAddressBinding, NewAddressViewModel>(),
    OnMapClickListener {

    private var markerViewManager: MarkerViewManager? = null
    var mapboxMap: MapboxMap? = null
    private var locationTitle: String? = null
    private var locationDetail: String? = null
    private var locationLat: String? = null
    private var locationLng: String? = null
    private var nameText: String? = null
    private var addressID: Int? = null
    private var code: String? = null
    private var collectionId: Int? = null
    private var bookmarkId: Int? = null
    private var shortcutName: String? = null

    private var destinationDetails: ShortcutDetails? = null
    private var isEditFavourite = false

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var breezeUserPreferenceUtil: BreezeUserPreferenceUtil

    private val viewModel: NewAddressViewModel by viewModels {
        viewModelFactory
    }

    object ShortcutKeys {
        const val HOME = "HOME"
        const val WORK = "WORK"
        const val NORMAL = "NORMAL"
        const val SHORTCUT_NAME = "shortcutName"

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initializeAddressDetails()
        init()
        editTextWatcher()
        clickListener()
        observeShortcutChangeResult()

        viewBinding.tvHeader.text = getString(
            if (code?.equals(HOME, true)!!) {
                R.string.home
            } else if (code?.equals(WORK, true)!!) {
                R.string.work
            } else {
                if (bookmarkId != null) {
                    R.string.edit_shortcut
                } else {
                    R.string.new_shortcut
                }
            }
        )

        if (isHomeOrWork()) {
            viewBinding.txtDestination.visibility = View.GONE
            viewBinding.editTextNewAddress.visibility = View.GONE
            viewBinding.btnDone.visibility = View.VISIBLE
        } else {
            viewBinding.btnDone.visibility = View.GONE
            viewBinding.txtDestination.visibility = View.VISIBLE
            viewBinding.editTextNewAddress.visibility = View.VISIBLE
            viewBinding.editTextNewAddress.getEditText().requestFocus()

            if (!nameText.isNullOrEmpty()) {
                setDefaultText(nameText!!)
            } else if (!locationTitle.isNullOrEmpty()) {
                setDefaultText(locationTitle!!)
            }

            setDefaultText(
                if (nameText.isNullOrEmpty()) (locationTitle ?: "") else (nameText ?: "")
            )
            showKeyBoard()
        }
        viewBinding.newaddressMapView.getMapboxMap().addOnMapClickListener(this)
    }

    private fun setDefaultText(value: String) {
        if (value.length > MAX_TITLE_CHAR_LIMIT) {
            setTextMaxLength(1000)
        } else {
            setTextMaxLength(MAX_TITLE_CHAR_LIMIT)
        }
        viewBinding.editTextNewAddress.getEditText().setText(value)
    }

    private fun setTextMaxLength(length: Int) {
        viewBinding.editTextNewAddress.getEditText().filters =
            arrayOf(InputFilter.LengthFilter(length))
    }

    private fun isHomeOrWork(): Boolean {
        return HOME.equals(code, true) || WORK.equals(code, true)
    }

    override fun onKeyboardVisibilityChanged(isOpen: Boolean) {
        viewBinding.btnDone.isVisible = !isOpen
    }

    private fun showKeyBoard() {
        val imm: InputMethodManager? =
            requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
        imm?.showSoftInput(
            viewBinding.editTextNewAddress.getEditText(),
            InputMethodManager.SHOW_IMPLICIT
        )
    }

    private fun initializeAddressDetails() {
        if (arguments?.containsKey(Constants.DESTINATION_ADDRESS_DETAILS) == true && arguments?.getSerializable(
                Constants.DESTINATION_ADDRESS_DETAILS
            ) != null
        ) {
            isEditFavourite = arguments?.getBoolean(Constants.FAVOURITE_EDIT, false) as Boolean
            destinationDetails =
                arguments?.getSerializable(Constants.DESTINATION_ADDRESS_DETAILS) as ShortcutDetails

            if (destinationDetails != null) {
                locationTitle = destinationDetails!!.address1
                locationDetail = destinationDetails!!.address2
                locationLat = destinationDetails!!.lat
                locationLng = destinationDetails!!.long

                nameText = destinationDetails!!.name
                code = destinationDetails?.code
                shortcutName = destinationDetails?.shortcutName
                collectionId = destinationDetails?.collectionId
                bookmarkId = destinationDetails?.bookmarkId
            }
        }
    }

    private fun clickListener() {
        viewBinding.backButtonNewAddress.setOnClickListener {
            (activity as DashboardActivity).hideKeyboard()

            var eventBack: String = Event.BACK
            if (bookmarkId != null) {//edit
                eventBack = Event.EDIT_SHORTCUT_BACK
            } else {//add
                if (code?.equals(HOME, true)!!) {
                    eventBack = Event.ADD_HOME_BACK
                } else if (code?.equals(WORK, true)!!) {
                    eventBack = Event.ADD_WORK_BACK
                }
            }
            if (eventBack != Event.BACK)
                Analytics.logClickEvent(eventBack, getScreenName())

            onBackPressed()
        }

        viewBinding.btnDone.setOnClickListener {
            saveShortcut()
        }

        viewBinding.editTextNewAddress.getEditText()
            .setOnEditorActionListener { v, actionId, event ->
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    if (isValidInput()) {
                        saveShortcut()
                        return@setOnEditorActionListener false
                    }
                    return@setOnEditorActionListener true
                }
                return@setOnEditorActionListener false
            }
    }

    private fun isValidInput(): Boolean {
        return viewBinding.editTextNewAddress.getEditText().text.isNotEmpty()
    }

    private fun saveShortcut() {
        var eventSave: String = Event.FAVOURITE_NAME_SAVE
        if (code?.equals(HOME, true)!!) {
            eventSave = Event.ADD_HOME_SAVE
        } else if (code?.equals(WORK, true)!!) {
            eventSave = Event.ADD_WORK_SAVE
        }
        Analytics.logClickEvent(eventSave, getScreenName())
        val shortcutData = destinationDetails ?: return

        if (bookmarkId != null) {
            viewModel.updateShortcut(shortcutData, viewBinding.editTextNewAddress.getEnteredText(), bookmarkId!!)
        } else {
            viewModel.createShortcut(shortcutData, viewBinding.editTextNewAddress.getEnteredText())
        }
    }

    private fun editTextWatcher() {
        viewBinding.editTextNewAddress.setFocusChangedListener { v, hasFocus ->
            if (!hasFocus) {
                Analytics.logEditEvent(Event.FAVOURITE_NAME_INPUT, getScreenName())
            }
            viewBinding.editTextNewAddress.setBGNormal()
        }

        viewBinding.editTextNewAddress.breezeEditTextListener =
            object : BreezeEditText.BreezeEditTextListener {
                override fun onBreezeEditTextFocusChange(isFocused: Boolean) {

                }

                override fun onClearClick() {
                    Analytics.logClickEvent(Event.EDIT_SHORTCUT_NAME_CLEAR, getScreenName())
                }

            }

        viewBinding.editTextNewAddress.getEditText().setImeOptions(EditorInfo.IME_ACTION_DONE)

        viewBinding.editTextNewAddress.addTextChangedListener(object : TextWatcher {
            var wasBigString = false

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                // displayText(s.length)
                if (s.isNotEmpty()) {
                    viewBinding.editTextNewAddress.setRightIconVisibility(true)
                } else {
                    viewBinding.editTextNewAddress.setRightIconVisibility(false)
                }
                if (wasBigString && s.length < MAX_TITLE_CHAR_LIMIT) {
                    setTextMaxLength(MAX_TITLE_CHAR_LIMIT)
                }
                viewBinding.btnDone.isEnabled = isValidInput()
                if (isValidInput()) {
                    viewBinding.btnDone.setBGNormal()
                } else {
                    viewBinding.btnDone.setBGGrey()
                }
            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
                wasBigString = s.length > (MAX_TITLE_CHAR_LIMIT - 1)
            }

            override fun afterTextChanged(s: Editable) {}
        })
    }

    private fun init() {
        val mLat = locationLat!!.safeDouble()
        val mLng = locationLng!!.safeDouble()
        val symbolLayerIconFeatureList: MutableList<Feature> = ArrayList()
        symbolLayerIconFeatureList.add(
            Feature.fromGeometry(Point.fromLngLat(mLng, mLat))
        )

        viewBinding.newaddressMapView.getMapboxMap().loadStyle(

            styleExtension = style(
                (requireActivity() as DashboardActivity).getMapboxStyle(
                    basicMapRequired = true
                )
            ) {
                mapboxMap = viewBinding.newaddressMapView.getMapboxMap()
                val marker = MarkerView(LatLng(mLat, mLng), inflateMarkerView(), mapboxMap!!)
                markerViewManager =
                    MarkerViewManager(viewBinding.newaddressMapView, mapboxMap!!)
                markerViewManager?.addMarker(marker)

                viewBinding.newaddressMapView.camera.apply {
                    val zoom = createZoomAnimator(CameraAnimatorOptions.cameraAnimatorOptions(15.0)) {
                        viewBinding.newaddressMapView.camera.flyTo(
                            CameraOptions.Builder()
                                .center(Point.fromLngLat(mLng, mLat))
                                .padding(EdgeInsets(10.0.dpToPx(), 60.0.dpToPx(), 20.0.dpToPx(), 10.0.dpToPx()))
                                .bearing(0.0)
                                .zoom(14.0)
                                .pitch(0.0)
                                .build()
                        )
                    }
                    playAnimatorsSequentially(zoom)
                }
                viewBinding.newaddressMapView.scalebar.enabled = false

                viewBinding.newaddressMapView.gestures.addOnFlingListener {
                    Timber.d("On fling is called")
                    Analytics.logMapInteractionEvents(Event.DRAG_MAP, getScreenName())
                }

                viewBinding.newaddressMapView.gestures.addOnScaleListener(object :
                    OnScaleListener {
                    override fun onScale(detector: StandardScaleGestureDetector) {}

                    override fun onScaleBegin(detector: StandardScaleGestureDetector) {}

                    override fun onScaleEnd(detector: StandardScaleGestureDetector) {
                        Timber.d("On scale end is called")
                        Analytics.logMapInteractionEvents(Event.PINCH_MAP, getScreenName())
                    }
                })

            }
        )

        viewBinding.editTextNewAddress.setOnKeyListener(object : View.OnKeyListener {
            override fun onKey(v: View?, keyCode: Int, event: KeyEvent): Boolean {
                if (event.action == KeyEvent.ACTION_DOWN) {
                    when (keyCode) {
                        KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                            return true
                        }

                        else -> {
                        }
                    }
                }
                return false
            }
        })


    }

    private fun inflateMarkerView(): View {
        val markerViewBinding = MarkerWindowBinding.inflate(layoutInflater, null, false)
        markerViewBinding.ivSavedplace.setOnClickListener {
            if (markerViewBinding.llToolTip.isShown) {
                markerViewBinding.llToolTip.visibility = View.INVISIBLE
                markerViewBinding.tooltipImgUp.visibility = View.INVISIBLE
            } else {
                markerViewBinding.llToolTip.visibility = View.VISIBLE
                markerViewBinding.tooltipImgUp.visibility = View.VISIBLE
            }
        }


        markerViewBinding.textviewAddress.text = locationTitle
        markerViewBinding.textviewAddressdetail.text = locationDetail
        return markerViewBinding.root
    }


    private fun observeShortcutChangeResult() {
        viewModel.createSuccessful.observe(viewLifecycleOwner) {
            viewModel.createSuccessful.removeObservers(this)
            if (viewModel.createSuccessful.value != true) return@observe
            ReactNativeEventEmitter.sendEvent(getApp(), "favouriteListChange", null)
            (activity as? DashboardActivity)?.run {
                updateEasyBreezyAcrossScreens()
                hideKeyboard()
                supportFragmentManager.popBackStack(
                    Constants.TAGS.ADD_NEW_BREEZE,
                    FragmentManager.POP_BACK_STACK_INCLUSIVE
                )
            }
        }
    }

    override fun inflateViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?,
        attachToContainer: Boolean
    ) = FragmentNewAddressBinding.inflate(inflater, container, attachToContainer)

    override fun getViewModelReference() = viewModel

    companion object {
        const val MAX_TITLE_CHAR_LIMIT = 20
    }

    override fun onMapClick(point: Point): Boolean {
        val clicked = viewBinding.newaddressMapView.getMapboxMap().pixelForCoordinate(point)
        viewBinding.newaddressMapView.getMapboxMap().queryRenderedFeatures(
            ScreenBox(
                ScreenCoordinate(clicked.x - 5, clicked.y - 5),
                ScreenCoordinate(clicked.x + 5, clicked.y + 5)
            ),
            RenderedQueryOptions(listOf("earthquakeCircle", "earthquakeText"), literal(true))
        ) { expected ->

        }
        return true
    }

    override fun getScreenName(): String {
        return Screen.SAVED_COLLECTION_SHORTCUT
    }
}



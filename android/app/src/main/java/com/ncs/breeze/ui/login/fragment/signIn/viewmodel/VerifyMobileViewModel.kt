package com.ncs.breeze.ui.login.fragment.signIn.viewmodel

import android.app.Application
import com.amplifyframework.auth.cognito.exceptions.service.*
import com.amplifyframework.auth.cognito.options.AWSCognitoAuthSignInOptions
import com.amplifyframework.auth.cognito.options.AuthFlowType
import com.amplifyframework.auth.exceptions.NotAuthorizedException
import com.amplifyframework.core.Amplify
import com.breeze.model.constants.Constants
import com.breeze.model.constants.PHONE_NUMBER_LOGIN_STATE
import com.breeze.model.constants.STATE_FETCH_USER_ATTR
import com.ncs.breeze.common.remote.ApiHelper
import com.ncs.breeze.common.remote.MyServiceInterceptor
import com.ncs.breeze.common.utils.*
import com.ncs.breeze.R
import com.ncs.breeze.common.extensions.android.getApp
import com.ncs.breeze.components.AmplifyErrorHandler
import com.ncs.breeze.components.SingleLiveEvent
import com.ncs.breeze.ui.base.BaseFragmentViewModel
import timber.log.Timber
import javax.inject.Inject

class VerifyMobileViewModel @Inject constructor(
    val apiHelper: ApiHelper,
    application: Application
) : BaseFragmentViewModel(
    application
) {

    companion object {
        const val DEFAULT_OTP_TRIES = 3
    }

    var mAPIHelper: ApiHelper = apiHelper
    var resendSuccessful: SingleLiveEvent<Boolean> = SingleLiveEvent()
    var resendFailed: SingleLiveEvent<AmplifyErrorHandler.AuthObject> = SingleLiveEvent()

    var confirmSignUpFailed: SingleLiveEvent<AmplifyErrorHandler.AuthObject> = SingleLiveEvent()

    var confirmSignInSuccessful: SingleLiveEvent<Boolean> = SingleLiveEvent()
    var confirmSignInFailed: SingleLiveEvent<AmplifyErrorHandler.AuthObject> = SingleLiveEvent()
    var closeOTPScreen: SingleLiveEvent<Boolean> = SingleLiveEvent()
    var clearOTP : SingleLiveEvent<Boolean> = SingleLiveEvent()

    var stateLoginWithPhoneNumber = PHONE_NUMBER_LOGIN_STATE.NOT_START

    /**
     * handle error
     */
    var errorThrow: SingleLiveEvent<Boolean> = SingleLiveEvent()

    var otpAttemptsLeft: Int = DEFAULT_OTP_TRIES

    @Inject
    lateinit var myServiceInterceptor: MyServiceInterceptor

    @Inject
    lateinit var breezeUserPreferenceUtil: BreezeUserPreferenceUtil

    var mPhoneNumber : String = ""
    var mOtpText : String = ""



    /**
     * confirm sign up phone number
     */
    fun confirmSignUp(phoneNumber: String, otpText: String) {
        mPhoneNumber = phoneNumber
        mOtpText = otpText

        AWSUtils.confirmSignUp(
            phoneNumber,
            otpText,
            { result ->
                Utils.runOnMainThread {
                    if (result.isSignUpComplete) {
                        /***
                         * state confirm sign in success
                         */
                        stateLoginWithPhoneNumber = PHONE_NUMBER_LOGIN_STATE.CONFIRM_SIGN_UP_SUCCESS
                        signIn(phoneNumber, Constants.AMPLIFY_CONSTANTS.AMPLIFY_PASSWORD)
                    } else {
                        sendErrorEventToShow()
                    }
                }
            },
            { error ->

                Utils.runOnMainThread {
                    sendErrorEventToShow()

                    when (error.cause) {
                        is CodeMismatchException -> {
                            // Incorrect OTP
                            Timber.d("No of otp attempts left $otpAttemptsLeft")
                            if (otpAttemptsLeft > 0) {
                                confirmSignUpFailed.value = AmplifyErrorHandler.AuthObject(
                                    String.format(
                                        getApp().getString(
                                            R.string.verify_mobile_incorrect_otp
                                        ), otpAttemptsLeft
                                    ), AmplifyErrorHandler.FieldType.DEFAULT
                                )
                            } else {
                                closeOTPScreen.value = true
                                confirmSignUpFailed.value = AmplifyErrorHandler.AuthObject(
                                    getApp().getString(
                                        R.string.verify_mobile_attempts_exceeded
                                    ), AmplifyErrorHandler.FieldType.DEFAULT
                                )
                            }
                            otpAttemptsLeft -= 1
                        }
                        is LimitExceededException -> {
                            closeOTPScreen.value = true
                            confirmSignUpFailed.value = AmplifyErrorHandler.AuthObject(
                                getApp().getString(
                                    R.string.verify_mobile_attempts_exceeded
                                ), AmplifyErrorHandler.FieldType.DEFAULT
                            )
                        }
                        else -> {
                            confirmSignUpFailed.value = AmplifyErrorHandler.getErrorMessage(
                                error,
                                getApp(),
                                AmplifyErrorHandler.AuthType.CONFIRM_SIGNUP
                            )
                        }
                    }
                }
            }
        )
    }


    /**
     * sign in phone number
     */
    private fun signIn(phoneNumber: String, confirmationCode: String) {
        AWSUtils.signIn(
            phoneNumber,
            Constants.AMPLIFY_CONSTANTS.AMPLIFY_PASSWORD,
            AWSCognitoAuthSignInOptions.builder()
                .authFlowType(AuthFlowType.CUSTOM_AUTH_WITH_SRP)
                .build(),
            { result ->
                stateLoginWithPhoneNumber = PHONE_NUMBER_LOGIN_STATE.SIGNIN_SUCCESS
                Utils.runOnMainThread {
                    if (result.nextStep.signInStep.name == Constants.AMPLIFY_CONSTANTS.SIGN_IN_WITH_CUSTOM_CHALLENGE) {
                        confirmSignIn(confirmationCode, false)
                    }
                }
            },
            { error ->
                sendErrorEventToShow()
            }
        )
    }


    /**
     * confirm sign in
     */
    fun confirmSignIn(confirmationCode: String, isSignIn: Boolean = false) {
        AWSUtils.confirmSignIn(
            confirmationCode,
            { result ->
                Utils.runOnMainThread {
                    if (result.isSignedIn) {
                        /**
                         * set state to sign up
                         */
                        stateLoginWithPhoneNumber = PHONE_NUMBER_LOGIN_STATE.CONFIRM_SIGN_IN

                        if (isSignIn) {
                            /**
                             * after confirm sign in => fetchToken
                             */
                            amplifyFetchAuthSession()
                        } else {
                            sendErrorEventToShow()
                        }
                    } else {
                        sendErrorEventToShow()

                        if (isSignIn && result.nextStep.signInStep.name == Constants.AMPLIFY_CONSTANTS.SIGN_IN_WITH_CUSTOM_CHALLENGE) {
                            val nextStep = result.nextStep
                            val map = nextStep.additionalInfo
                            if (map != null) {

                                if (map.containsKey(Constants.AMPLIFY_CONSTANTS.ERROR_CODE) && map[Constants.AMPLIFY_CONSTANTS.ERROR_CODE] == Constants.AMPLIFY_CONSTANTS.ERROR_INVALID_OTP) {
                                    if (map.containsKey(Constants.AMPLIFY_CONSTANTS.ERROR_REMAINING_ATTEMPTS) && map[Constants.AMPLIFY_CONSTANTS.ERROR_REMAINING_ATTEMPTS] != "0") {
                                        confirmSignInFailed.value =
                                            AmplifyErrorHandler.AuthObject(
                                                String.format(
                                                    getApp().getString(
                                                        R.string.verify_mobile_incorrect_otp
                                                    ),
                                                    map[Constants.AMPLIFY_CONSTANTS.ERROR_REMAINING_ATTEMPTS]
                                                ), AmplifyErrorHandler.FieldType.DEFAULT
                                            )
                                    }
                                } else {
                                    confirmSignInFailed.value = AmplifyErrorHandler.AuthObject(
                                        getApp().getString(R.string.verify_mobile_default),
                                        AmplifyErrorHandler.FieldType.DEFAULT
                                    )
                                }
                            }
                        }
                    }
                }
            },
            { error ->
                errorThrow.postValue(true)

                Utils.runOnMainThread {
                    if (isSignIn) {
                        if (error.cause is NotAuthorizedException) {
                            closeOTPScreen.value = true
                            confirmSignInFailed.value = AmplifyErrorHandler.AuthObject(
                                getApp().getString(
                                    R.string.verify_mobile_attempts_exceeded
                                ), AmplifyErrorHandler.FieldType.DEFAULT
                            )
                        } else {
                            confirmSignInFailed.value = AmplifyErrorHandler.getErrorMessage(
                                error,
                                getApp(),
                                AmplifyErrorHandler.AuthType.SIGN_IN
                            )
                        }
                    } else {
                        confirmSignUpFailed.value = AmplifyErrorHandler.getErrorMessage(
                            error,
                            getApp(),
                            AmplifyErrorHandler.AuthType.SIGN_UP
                        )
                    }

                    Timber.d("custom sign in error scenario ${error.message}")
                }
            }
        )
    }

    fun resetAttempts() {
        otpAttemptsLeft = DEFAULT_OTP_TRIES
    }

//    fun updateUserAttribute() {
//        Amplify.Auth.updateUserAttribute(
//            AuthUserAttribute(
//                AuthUserAttributeKey.custom(Constants.AMPLIFY_CONSTANTS.CUSTOM_FIRST_TIME_LOGIN),
//                "false"
//            ),
//            {
//                ThreadUtils.runOnUiThread {
//                    Timber.d("Update user attribute successful")
//                    confirmSignUpSuccessful.value = true
//                }
//            },
//            {
//                ThreadUtils.runOnUiThread {
//                    Timber.e("Failed to update user attribute.$it")
//                    //saveUserNameSuccessful.value = false
//                    confirmSignUpFailed.value = AmplifyErrorHandler.getErrorMessage(
//                        it,
//                        getApp(),
//                        AmplifyErrorHandler.AuthType.CONFIRM_SIGNUP,
//                    )
//                }
//            }
//        )
//    }

    fun resendSignUpOTP(phoneNumber: String) {
        AWSUtils.resendSignUpCode(
            phoneNumber,
            { result ->
                resendSuccessful.value = true
                Timber.i("AuthQuickstart $result")
            },
            { error ->
                Utils.runOnMainThread {
                    resendFailed.value = AmplifyErrorHandler.getErrorMessage(
                        error,
                        getApp(),
                        AmplifyErrorHandler.AuthType.SIGN_UP,
                    )
                }
                Timber.e("AuthQuickstart $error")
            }
        )
    }


    fun resendSignInOTP(phoneNumber: String) {
        AWSUtils.signIn(
            phoneNumber,
            Constants.AMPLIFY_CONSTANTS.AMPLIFY_PASSWORD,
            AWSCognitoAuthSignInOptions.builder()
                .authFlowType(AuthFlowType.CUSTOM_AUTH_WITH_SRP)
                .build(),
            {
                Utils.runOnMainThread {
                    // Triggering OTP again
                    Timber.d("OTP triggered successfully")
                }
            },
            {
                Utils.runOnMainThread {
                    Timber.d("OTP trigger failed")
                }
            }
        )
    }

    /**
     * fetch token
     */
    private fun amplifyFetchAuthSession() {
        AWSUtils.checkUserAuthSession(object : AWSUtils.AuthSessionCallback {
            override fun onAuthSuccess(token: String?) {
                myServiceInterceptor.setSessionToken(token)
                //updateTncStatus()

                /**
                 * set state to fetch token success
                 */
                stateLoginWithPhoneNumber = PHONE_NUMBER_LOGIN_STATE.FETCH_TOKEN_SUCCESS

                /**
                 * after fetch token success => fetch user attr => to get data -> is user name available
                 */
                fetchUserAttr()


            }

            override fun onAuthFailed() {
                sendErrorEventToShow()
            }
        })
    }

    /**
     * fetch user attr
     */
    private fun fetchUserAttr() {
        amplifyFetchUserAttributes(breezeUserPreferenceUtil) { state ->
            /**
             * set state to fetch ATTR success
             */
            stateLoginWithPhoneNumber = PHONE_NUMBER_LOGIN_STATE.FETCH_USER_ATTR

            when (state) {
                STATE_FETCH_USER_ATTR.USER_USERNAME_AVAILABLE -> {
                    InitObjectUtilsController.currentObjectFlowLoginSignUp?.isUserNameAvailable =
                        true
                    /**
                     * fetch setting user
                     */
                    fetchSettingUser()
                }
                STATE_FETCH_USER_ATTR.USER_NO_USERNAME_ATTRIBUTE -> {
                    InitObjectUtilsController.currentObjectFlowLoginSignUp?.isUserNameAvailable =
                        false

                    /**
                     * fetch setting user
                     */
                    fetchSettingUser()
                }
                STATE_FETCH_USER_ATTR.FETCH_ATTR_USER_FAILS -> {
                    sendErrorEventToShow()
                }
            }
        }
    }


    /**
     * fetch setting user & all setting in system
     */
    private fun fetchSettingUser() {
        getAllSettingZipData(breezeUserPreferenceUtil, apiHelper) { isFetchSuccess ->
            if (isFetchSuccess) {
                /**
                 * send event with is user name available
                 */
                confirmSignInSuccessful.value = true
            } else {
                sendErrorEventToShow()
            }
        }
    }

    /**
     * send event to show error
     */
    fun sendErrorEventToShow() {
        Utils.runOnMainThread {
            //isShowProgess.postValue(false)
            errorThrow.postValue(true)
        }
    }

    fun restartVerifyOTP() {
        AWSUtils.init(getApp())
        when (stateLoginWithPhoneNumber) {
            PHONE_NUMBER_LOGIN_STATE.CONFIRM_SIGN_IN -> {
                amplifyFetchAuthSession()
            }
            PHONE_NUMBER_LOGIN_STATE.FETCH_USER_ATTR -> {
                /**
                 * fetch setting user
                 */
                fetchSettingUser()
            }
            PHONE_NUMBER_LOGIN_STATE.SIGNIN_SUCCESS -> {
                confirmSignIn(Constants.AMPLIFY_CONSTANTS.AMPLIFY_PASSWORD, false)
            }
            PHONE_NUMBER_LOGIN_STATE.CONFIRM_SIGN_UP_SUCCESS -> {
                signIn(mPhoneNumber, Constants.AMPLIFY_CONSTANTS.AMPLIFY_PASSWORD)
            }
            PHONE_NUMBER_LOGIN_STATE.FETCH_TOKEN_SUCCESS -> {
                fetchSettingUser()
            }
            else -> {
            }
        }


    }
}
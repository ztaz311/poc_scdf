package com.ncs.breeze.di.module.fragment

import com.ncs.breeze.ui.dashboard.fragments.view.DebugFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class DebugModule {
    @ContributesAndroidInjector
    abstract fun contributeDebugFragment(): DebugFragment
}
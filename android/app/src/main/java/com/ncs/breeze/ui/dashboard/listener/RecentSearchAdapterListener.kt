package com.ncs.breeze.ui.dashboard.listener

interface RecentSearchAdapterListener {
    fun onRemoveClick(index: Int)
    fun onItemClick(index: Int)
}
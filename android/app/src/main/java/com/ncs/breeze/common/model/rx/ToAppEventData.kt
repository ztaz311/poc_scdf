package com.ncs.breeze.common.model.rx

import com.breeze.model.DestinationAddressDetails
import com.breeze.model.api.request.ETARequest
import com.breeze.model.api.response.ERPResponseData
import com.breeze.model.api.response.ValueConditionShowBroadcast
import com.mapbox.api.directions.v5.models.DirectionsRoute
import com.mapbox.navigation.base.route.NavigationRoute


sealed class ToAppEventData

data class PhoneNavigationStartEventData(
    val originalDestinationAddressDetails: DestinationAddressDetails,
    val destinationAddressDetails: DestinationAddressDetails,
    val route: DirectionsRoute,
    val compressedRouteOriginalResp: ByteArray,
    val erpData: ERPResponseData.ERPResponse?,
    val etaRequest: ETARequest?,
    val walkingRoute: NavigationRoute? = null,
    val selectedCarParkIconRes: Int = -1,
    var notificationCriterias: ArrayList<ValueConditionShowBroadcast>? = null,
) : ToAppEventData() {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as PhoneNavigationStartEventData

        if (originalDestinationAddressDetails != other.originalDestinationAddressDetails) return false
        if (destinationAddressDetails != other.destinationAddressDetails) return false
        if (route != other.route) return false
        if (!compressedRouteOriginalResp.contentEquals(other.compressedRouteOriginalResp)) return false
        if (erpData != other.erpData) return false
        if (etaRequest != other.etaRequest) return false

        return true
    }

    override fun hashCode(): Int {
        var result = originalDestinationAddressDetails.hashCode()
        result = 31 * result + destinationAddressDetails.hashCode()
        result = 31 * result + route.hashCode()
        result = 31 * result + compressedRouteOriginalResp.contentHashCode()
        result = 31 * result + (erpData?.hashCode() ?: 0)
        result = 31 * result + (etaRequest?.hashCode() ?: 0)
        return result
    }
}
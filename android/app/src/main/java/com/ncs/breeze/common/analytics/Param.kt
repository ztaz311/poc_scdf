package com.ncs.breeze.common.analytics

/**
 * @author Aiswarya
 */
object Param {
    const val KEY_EVENT_VALUE = "event_value"
    const val KEY_EVENT_TIMESTAMP = "event_ts"
    const val KEY_SCREEN_NAME = "screen_name"
    const val KEY_POPUP_NAME = "popup_name"
    const val KEY_BATTERY_LEVEL = "battery_level"
    const val KEY_DATA_SENT = "data_sent"
    const val KEY_DATA_RECEIVED = "data_received"
    const val KEY_LATITUDE = "latitude"
    const val KEY_LONGITUDE = "longitude"
    const val KEY_LOCATION_NAME = "location_name"
    const val KEY_NAVIGATION_TYPE = "navigation_type"
    const val KEY_DATE_PUSHED = "date_pushed"
    const val KEY_DATE_CLICKED = "date_clicked"
    const val KEY_MESSAGE = "message"
    const val KEY_MAIN_CATEGORY = "main_category"

}
package com.ncs.breeze.common.utils

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.breeze.model.constants.Constants
import com.mapbox.android.core.location.LocationEngineResult
import com.mapbox.navigation.core.lifecycle.MapboxNavigationApp
import com.mapbox.navigation.core.trip.session.TripSessionState

class OBUStripStateManager {
    private val currentState = MutableLiveData(OBUStripState.NONE)
    val obuStripCurrentState:LiveData<OBUStripState> = currentState
    var isPhoneOpened = false
        private set
    var isCarOpened = false
        private set


    private var isDisabled = false
    private var isDestroyed = false
    private var isAlreadyTriggered = false
    private var timeWhenCruiseClosed: Long? = null
    private var timeWhenExceeded: Long? = null

    private val callbackLocationBreezeManager =
        object : LocationBreezeManager.CallBackLocationBreezeManager {
            override fun onSuccess(result: LocationEngineResult) {
                if (isDisabled || Utils.isNavigationStarted || isDestroyed) return
                if (isAlreadyTriggered) {
                    if (timeWhenCruiseClosed == null || (System.currentTimeMillis() - timeWhenCruiseClosed!!) < CRUISE_TRIGGER_RESET_THRESHOLD_TIME) {
                        return
                    } else {
                        resetState()
                    }
                }
                val location = result.lastLocation
                var speedKmPH = 0.0
                location?.let { l ->
                    val speed = l.speed
                    speedKmPH = speed * 3.6
                }

                if (speedKmPH > Constants.CRUISE_MODE_TRIGGER_SPEED) {
                    var totalTimeExceeded: Long = 0
                    if (timeWhenExceeded == null) {
                        timeWhenExceeded = System.currentTimeMillis()
                    } else {
                        totalTimeExceeded = System.currentTimeMillis() - timeWhenExceeded!!
                    }
                    val mapboxNavigation = MapboxNavigationApp.current()
                    if (mapboxNavigation != null
                        && mapboxNavigation.getTripSessionState() != TripSessionState.STARTED
                        && totalTimeExceeded >= CRUISE_TRIGGER_THRESHOLD_TIME
                    ) {
                        if (!isDisabled) {
                            obuTriggered()
                            currentState.postValue(OBUStripState.RUNNING)
                        }
                    }
                } else {
                    timeWhenExceeded = null
                }

            }
        }

    fun resetState(){
        currentState.postValue(OBUStripState.NONE)
        isPhoneOpened = false
        isCarOpened = false
        isAlreadyTriggered = false
        timeWhenCruiseClosed = null
        timeWhenExceeded = null
    }

    fun obuTriggered() {
        isAlreadyTriggered = true
        timeWhenExceeded = null
    }

    fun onEndCruise() {
        timeWhenCruiseClosed = System.currentTimeMillis()
    }

    fun init() {
        LocationBreezeManager.getInstance()
            .registerLocationCallback(callbackLocationBreezeManager)
    }

    fun disable(){
        isDisabled = true
        timeWhenExceeded = null
    }

    fun enable(){
        isDisabled = false
    }

    fun setPhoneOpened(){
        isPhoneOpened = true
    }
    fun setCarOpened(){
        isCarOpened = true
    }

    fun openOBUFromRN(){
        isPhoneOpened = false
        isCarOpened = false
        currentState.postValue(OBUStripState.RUNNING)
    }

    fun closeFromPhone(){
        isPhoneOpened = false
        currentState.postValue(OBUStripState.CLOSED)
    }

    fun closeFromCar() {
        isCarOpened = false
        currentState.postValue(OBUStripState.CLOSED)
    }

    fun destroy() {
        resetState()
        LocationBreezeManager.getInstance()
            .removeLocationCallBack(callbackLocationBreezeManager)
        isDestroyed = true
    }

    companion object {
        private const val CRUISE_TRIGGER_THRESHOLD_TIME = 10000L
        private const val CRUISE_TRIGGER_RESET_THRESHOLD_TIME = 60 * 60 * 1000L//1 hour
        private var instance: OBUStripStateManager? = null
        fun getInstance(): OBUStripStateManager? {
            if (instance == null) {
                instance = OBUStripStateManager()
                instance?.init()
            }
            return instance
        }

        fun destroyInstance(){
            instance?.destroy()
            instance = null
        }
    }
}

enum class OBUStripState{
    NONE,
    RUNNING,
    CLOSED
}
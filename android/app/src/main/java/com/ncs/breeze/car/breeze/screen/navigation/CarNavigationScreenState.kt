package com.ncs.breeze.car.breeze.screen.navigation

import androidx.lifecycle.Lifecycle
import com.ncs.breeze.components.SingleLiveEvent

object CarNavigationScreenState {
    var movedToAnotherScreen: Boolean = false
    val state = SingleLiveEvent<Lifecycle.State>()
    var canStartNavigation = true
    val carAutoDriveState = SingleLiveEvent<Boolean>()
}

package com.ncs.breeze.reactnative.nativemodule

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.provider.ContactsContract
import android.widget.Toast
import androidx.annotation.Nullable
import androidx.appcompat.app.AlertDialog
import com.breeze.model.constants.Constants
import com.ncs.breeze.common.utils.Utils
import com.facebook.react.bridge.*
import com.facebook.react.modules.core.DeviceEventManagerModule
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import com.ncs.breeze.R
import timber.log.Timber


class NativeContactActivityStarterModule(context: ReactApplicationContext) :
    ReactContextBaseJavaModule(context) {

    var reactContext: ReactApplicationContext = context

    init {
        context.addActivityEventListener(ActivityEventListener())
    }

    override fun getName(): String {
        return "ContactActivityStarter";
    }

    @ReactMethod
    fun contactStarter() {
        Timber.d("contact starter event called")
        val contextActivity = currentActivity;
        if (contextActivity != null) {
            launchContactsIntent()
        }
    }

    private fun launchContactsIntent() {
        Dexter.withContext(currentActivity)
            .withPermission(Manifest.permission.READ_CONTACTS)
            .withListener(object : PermissionListener {
                override fun onPermissionGranted(response: PermissionGrantedResponse) {
                    val intent =
                        Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI)
                    intent.type = ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE
                    if (currentActivity?.packageManager != null) {
                        val activityInfo = intent.resolveActivityInfo(currentActivity?.packageManager!!, intent.flags)
                        if (activityInfo?.exported == true) {
                            try {
                                currentActivity?.startActivityForResult(
                                    intent,
                                    Constants.REQUEST_CODES.CONTACTS_REQUEST_CODE_FROM_RN
                                )
                            } catch (e: Exception) {
                                currentActivity?.let {
                                    Toast.makeText(
                                        it,
                                        "No application that can handle this intent",
                                        Toast.LENGTH_SHORT
                                    ).show()
                                }
                            }
                        } else {
                            currentActivity?.let {
                                Toast.makeText(
                                    it,
                                    "No application that can handle this intent",
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        }
                    }
                }

                override fun onPermissionDenied(response: PermissionDeniedResponse) {
                    if (response.isPermanentlyDenied) {
                        /**
                         * show dialog open setting
                         */
                        showSettingsDialog()
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    permission: PermissionRequest,
                    token: PermissionToken
                ) {
                    token.continuePermissionRequest()
                }
            })
            .withErrorListener {}.onSameThread().check()
    }

    private fun sendEvent(
        reactContext: ReactContext,
        eventName: String,
        @Nullable params: WritableMap
    ) {
        reactContext
            .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter::class.java)
            .emit(eventName, params)
    }

    /**
     * show dialog open setting permission
     */
    private fun showSettingsDialog() {
        currentActivity?.let {
            val builder = AlertDialog.Builder(it)
            builder.setTitle("Need Permissions")
            builder.setMessage(R.string.message_permission_open_setting)
            builder.setPositiveButton(
                "GOTO SETTINGS"
            ) { dialog, _ ->
                dialog.cancel()
                Utils.openSettings(it)
            }
            builder.setNegativeButton(
                "Cancel"
            ) { dialog, which -> dialog.cancel() }
            builder.show()
        }
    }


    inner class ActivityEventListener : BaseActivityEventListener() {
        override fun onActivityResult(
            activity: Activity?,
            requestCode: Int,
            resultCode: Int,
            data: Intent?
        ) {
            Timber.d("Activity result called in NativeContactActivityStarterModule")
            if (requestCode == Constants.REQUEST_CODES.CONTACTS_REQUEST_CODE_FROM_RN && resultCode == Activity.RESULT_OK && data != null) {
                handleContacts(data)
            }
        }
    }

    private fun handleContacts(data: Intent) {

        val contactDetails = Utils.extractPhoneNumberFromContactIntent(data, reactContext)

        if (contactDetails != null) {
            val params: WritableMap = Arguments.createMap()
            params.putString(Constants.TRIPETA.CONTACT_NAME, contactDetails.contactName)
            params.putString(Constants.TRIPETA.CONTACT_NUMBER, contactDetails.phoneNumberList[0])
            sendEvent(reactContext, "SelectEvent", params);
        } else {
            currentActivity?.let { showAlert(it) }
        }
    }

    private fun showAlert(context: Context) {
        val builder: AlertDialog.Builder? = context.let {
            AlertDialog.Builder(it)
        }

        builder!!.setMessage(R.string.incorrect_phone_number)
            .setPositiveButton(
                R.string.alert_close,
                DialogInterface.OnClickListener { dialog, which ->
                    dialog.dismiss()
                })

        val alertDialog: AlertDialog? = builder.create()

        alertDialog?.setOnShowListener {
            alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).isAllCaps = false
            alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).isAllCaps = false
        }

        alertDialog?.show()
    }

}
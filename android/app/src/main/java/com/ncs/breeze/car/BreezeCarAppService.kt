package com.ncs.breeze.car

import android.content.IntentFilter
import androidx.car.app.CarAppService
import androidx.car.app.Session
import androidx.car.app.validation.HostValidator
import com.ncs.breeze.car.breeze.MainCarBreezeSession
import com.ncs.breeze.car.breeze.receivers.BreezeCarNotificationReceiver

class BreezeCarAppService : CarAppService() {
    var receiver: BreezeCarNotificationReceiver? = null
    override fun createHostValidator(): HostValidator {
        return HostValidator.ALLOW_ALL_HOSTS_VALIDATOR
        // TODO limit hosts for production
        //    https://github.com/mapbox/mapbox-navigation-android-examples/issues/27
//        return HostValidator.Builder(this)
//                .addAllowedHosts(R.array.android_auto_allow_list)
//                .build()
    }

    override fun onCreateSession(): Session {
        receiver = BreezeCarNotificationReceiver()
        val filter = IntentFilter(BreezeCarNotificationReceiver.ACTION_OPEN_APP)
        registerReceiver(receiver, filter)
        return MainCarBreezeSession()
    }

    override fun onDestroy() {
        super.onDestroy()
        receiver?.let {
            unregisterReceiver(it)
        }
    }
}

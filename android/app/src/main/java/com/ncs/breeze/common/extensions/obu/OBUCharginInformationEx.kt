package com.ncs.breeze.common.extensions.obu

import com.breeze.model.api.request.ChargingInfo
import com.breeze.model.obu.OBUCardStatusEventData
import sg.gov.lta.obu.sdk.core.enums.OBUCardStatus
import sg.gov.lta.obu.sdk.core.models.OBUChargingInformation

fun OBUChargingInformation.toChargingInfo() = ChargingInfo(
    businessFunction = businessFunction.name,
    roadName = roadName,
    chargingAmount = chargingAmount,
    chargingType = chargingType.name,
    chargingMessageType = chargingMessageType.name,
    content1 = content1,
    content2 = content2,
    content3 = content3,
    content4 = content4,
    content5 = null,
    startTime = startTime,
    endTime = endTime,
    cardStatus = when (cardStatus) {
        OBUCardStatus.Detected -> OBUCardStatusEventData.EVENT_TYPE_CARD_VALID
        OBUCardStatus.InsufficientStoredValue -> OBUCardStatusEventData.EVENT_TYPE_CARD_INSUFFICIENT
        OBUCardStatus.ExpiredStored -> OBUCardStatusEventData.EVENT_TYPE_CARD_EXPIRED
        else -> OBUCardStatusEventData.EVENT_TYPE_CARD_ERROR
    }
)
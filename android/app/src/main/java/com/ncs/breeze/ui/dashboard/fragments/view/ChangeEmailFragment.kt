package com.ncs.breeze.ui.dashboard.fragments.view

import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.ncs.breeze.R
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.Event
import com.ncs.breeze.common.analytics.Screen
import com.ncs.breeze.common.utils.BreezeUserPreferenceUtil
import com.breeze.model.constants.Constants
import com.ncs.breeze.databinding.FragmentChangeEmailBinding
import com.ncs.breeze.ui.base.BaseFragment
import com.ncs.breeze.ui.dashboard.activity.DashboardActivity
import com.ncs.breeze.ui.dashboard.fragments.viewmodel.ChangeEmailViewModel
import javax.inject.Inject


class ChangeEmailFragment : BaseFragment<FragmentChangeEmailBinding, ChangeEmailViewModel>() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val viewModel: ChangeEmailViewModel by viewModels {
        viewModelFactory
    }

    companion object {
        const val IS_EMAIL_VERIFICATION = "IS_EMAIL_VERIFICATION"
        const val IS_FROM_TRAVEL_LOG = "IS_FROM_TRAVEL_LOG"
    }

    @Inject
    lateinit var breezeUserPreferenceUtil: BreezeUserPreferenceUtil

    var isFromTravelLog: Boolean = false
    var lastUpdatedEmail: String? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initialize()
    }

    private fun initialize() {

        val userEmail = breezeUserPreferenceUtil.retrieveUserEmail()

        viewBinding.backButton.setOnClickListener {
            backButtonBehaviour()
        }

        viewBinding.btnClose.setOnClickListener {
            backButtonBehaviour()
        }

        viewBinding.editEmail.getEditText().imeOptions = EditorInfo.IME_ACTION_DONE

        viewBinding.editEmail.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                viewBinding.editEmail.setBGNormal()

                if (s.isNotEmpty()) {
                    viewBinding.editEmail.setRightIconVisibility(true)
                    viewBinding.editEmail.updateRightIcon(R.drawable.cross)
                } else {
                    viewBinding.editEmail.setRightIconVisibility(false)
                }

                if (TextUtils.equals(s.toString(), userEmail)) {
                    viewBinding.editEmail.getEditText().setOnEditorActionListener(null)
                    viewBinding.saveEmail.setOnClickListener(null)
                    viewBinding.saveEmail.setBGGrey()
                } else {
                    setUpdateEmailListeners()
                    viewBinding.saveEmail.setBGNormal()
                    Analytics.logClickEvent(Event.ACCOUNT_EMAIL_INPUT, getScreenName())
                }
            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            }

            override fun afterTextChanged(s: Editable) {}
        })
        viewBinding.tvResponse.visibility = View.GONE
        viewBinding.btnResendEmail.visibility = View.GONE

        viewBinding.btnResendEmail.setOnClickListener {
            Analytics.logClickEvent(Event.ACCOUNT_RESEND_LINK, getScreenName())

            if (lastUpdatedEmail != null && TextUtils.equals(
                    lastUpdatedEmail,
                    viewBinding.editEmail.getEnteredText()
                )
            ) {
                viewModel.triggeredConfirmationCode.observe(viewLifecycleOwner) {
                    viewModel.triggeredConfirmationCode.removeObservers(this)
                    if (it) {
                        breezeUserPreferenceUtil.saveUserEmailVerifiedStatus(false)
                        emailUnverifiedView()
                    }
                }
                viewModel.retriggerEmail()
            } else {
                updateEmail()
            }
        }

        isFromTravelLog = arguments?.getBoolean(IS_FROM_TRAVEL_LOG, false) == true

        if (isFromTravelLog) {
            viewBinding.llHeadingMain.visibility = View.GONE
            viewBinding.rlCurvedHeaderBg.visibility = View.VISIBLE
        }


        if (arguments?.getBoolean(IS_EMAIL_VERIFICATION, false) == true &&
            !userEmail.isNullOrEmpty()
        ) {

            viewBinding.editEmail.getEditText().setText(userEmail)
            viewBinding.saveEmail.setBGGrey()

            val otp = arguments?.getString(DashboardActivity.DEEPLINK_EMAIL_OTP)
            if (!otp.isNullOrEmpty()) {

                viewModel.confirmEmailSuccessful.observe(viewLifecycleOwner, Observer {
                    viewModel.confirmEmailSuccessful.removeObservers(this)
                    if (it) {
                        // Enable verified OTP image
                        emailVerifiedView()
                        breezeUserPreferenceUtil.saveUserEmailVerifiedStatus(true)
                        backButtonBehaviour()
                    } else {
                        // Display error
                        // emailUnverifiedView()
                    }
                })
                viewModel.verifyEmailConfirmationCode(otp)
            }
        } else if (!userEmail.isNullOrEmpty()) {
            viewBinding.editEmail.getEditText().setText(userEmail)
            viewBinding.saveEmail.setBGGrey()

            lastUpdatedEmail = userEmail

            viewBinding.tvResponse.text = ""
            viewBinding.tvResponse.visibility = View.GONE
            viewBinding.btnResendEmail.visibility = View.GONE

        } else {
            if (isFromTravelLog) {
                viewBinding.tvResponse.text =
                    getString(R.string.missing_email_travel_log_error)
            } else {
                viewBinding.exclamation.visibility = View.VISIBLE
                viewBinding.tvResponse.text = getString(R.string.missing_email_error)
            }
            viewBinding.tvResponse.visibility = View.VISIBLE
            viewBinding.btnResendEmail.visibility = View.GONE

            setUpdateEmailListeners()

        }
    }

    private fun backButtonBehaviour() {
        if (activity != null) {
            (activity as DashboardActivity)?.refreshProfile()
            (activity as DashboardActivity).hideKeyboard()
            onBackPressed()
        }
    }

    private fun setUpdateEmailListeners() {
        viewBinding.editEmail.getEditText()
            .setOnEditorActionListener(TextView.OnEditorActionListener { v, actionId, event ->
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    viewBinding.saveEmail.performClick()
                    return@OnEditorActionListener true
                }
                false
            })

        viewBinding.saveEmail.setBGThemed()
        viewBinding.saveEmail.setOnClickListener {
            Analytics.logClickEvent(Event.SAVE, getScreenName())
            updateEmail()
        }
    }

    private fun emailUnverifiedView() {
        viewBinding.tvResponse.visibility = View.VISIBLE
        viewBinding.btnResendEmail.visibility = View.VISIBLE
        viewBinding.btnResendEmail.text = getString(R.string.button_resend_link)
        viewBinding.editEmail.updateRightIcon(R.drawable.exclaimation_warning)
        viewBinding.tvResponse.text = resources.getString(R.string.verification_email_sent)
    }

    private fun emailVerifiedView() {
        viewBinding.tvResponse.visibility = View.VISIBLE
        viewBinding.btnResendEmail.visibility = View.GONE
        viewBinding.editEmail.hideErrorView()
        viewBinding.editEmail.updateRightIcon(R.drawable.tick_successful)
        viewBinding.tvResponse.text = resources.getString(R.string.verification_email_verified)
    }

    private fun updateEmail() {
        if (!viewBinding.editEmail.getEnteredText().isNullOrBlank()) {
            if (!viewBinding.editEmail.getEnteredText()
                    .matches(Regex(Constants.REGEX.EMAIL_REGEX))
            ) {
                viewBinding.editEmail.showErrorView(resources.getString(R.string.error_account_email_invalid))
            } else {
                viewBinding.editEmail.hideErrorView()
                viewBinding.exclamation.visibility = View.GONE
                viewBinding.tvResponse.visibility = View.GONE
                viewBinding.btnResendEmail.visibility = View.GONE
                (activity as DashboardActivity).hideKeyboard()
                breezeUserPreferenceUtil.saveUserEmail(viewBinding.editEmail.getEnteredText())
                emailVerifiedView()
                backButtonBehaviour()
            }
        }
    }

    override fun inflateViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?,
        attachToContainer: Boolean
    ) = FragmentChangeEmailBinding.inflate(inflater, container, attachToContainer)

    override fun getViewModelReference(): ChangeEmailViewModel {
        return viewModel
    }

    fun hideKeyboard() {
        viewBinding.editEmail.clearFocus()
        (activity as DashboardActivity).hideKeyboard()
    }

    override fun getScreenName(): String {
        return Screen.SETTINGS_ACCOUNT_EDIT_EMAIL
    }
}
package com.ncs.breeze.car.breeze.component

import android.graphics.*

/**
 * Draws an image based on the initial
 */
object ContactInitialsRenderer {

    private const val CIRCLE_COLOR = "#5DA7FF"
    private const val TEXT_COLOR = "#FFFFFF"

    private const val TEXT_SIZE = 40.0f
    private const val WIDTH = 73
    private const val HEIGHT = 73
    private const val X_POS = WIDTH.toFloat() / 2
    private const val Y_POS = HEIGHT.toFloat() / 2
    private const val radius: Float = (73.0F / 2.0F)

    /**
     * Render the [input] to a [Bitmap] circle
     */
    fun render(
        input: String
    ): Bitmap? {
        if (input.isBlank()) return null
        val initial = getInitial(input)
        val bitmapOutput = Bitmap.createBitmap(WIDTH, HEIGHT, Bitmap.Config.ARGB_8888)
        bitmapOutput.eraseColor(Color.TRANSPARENT)
        val canvas = drawCircle(bitmapOutput)
        drawText(initial, canvas)
        return bitmapOutput
    }

    private fun drawText(initial: String, canvas: Canvas) {
        val textPaint = Paint()
        textPaint.color = Color.parseColor(TEXT_COLOR)
        textPaint.textSize = TEXT_SIZE

        val bounds = Rect()
        textPaint.getTextBounds(initial, 0, 1, bounds)

        canvas.drawText(
            initial,
            X_POS - (bounds.width().toFloat() / 2),
            Y_POS + (bounds.height().toFloat() / 2),
            textPaint
        )
    }

    private fun drawCircle(bitmapOutput: Bitmap): Canvas {
        val circlePaint = Paint()
        circlePaint.color = Color.parseColor(CIRCLE_COLOR)
        val canvas = Canvas(bitmapOutput)
        canvas.drawCircle(
            X_POS, Y_POS, radius, circlePaint
        )
        return canvas
    }

    private fun getInitial(input: String): String {
        return if (input.length > 1) {
            input.trim().substring(0, 1).uppercase()
        } else {
            input.uppercase()
        }
    }
}

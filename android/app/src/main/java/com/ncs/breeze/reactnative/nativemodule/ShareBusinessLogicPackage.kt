package com.ncs.breeze.reactnative.nativemodule

import com.facebook.react.ReactPackage
import com.facebook.react.bridge.NativeModule
import com.facebook.react.bridge.ReactApplicationContext
import com.facebook.react.uimanager.ViewManager
import io.reactivex.Single
import io.reactivex.subjects.BehaviorSubject

class ShareBusinessLogicPackage : ReactPackage {
    private val requestsModuleSubject = BehaviorSubject.create<ShareBusinessLogic>()

    override fun createViewManagers(reactContext: ReactApplicationContext): List<ViewManager<*, *>> {
        return emptyList()
    }

    override fun createNativeModules(reactContext: ReactApplicationContext): List<NativeModule> {
        val requestsModule = ShareBusinessLogic(reactContext)
        requestsModuleSubject.onNext(requestsModule)
        return listOf(requestsModule)
    }

    fun getRequestsModule(): Single<ShareBusinessLogic> = requestsModuleSubject.firstOrError()
}

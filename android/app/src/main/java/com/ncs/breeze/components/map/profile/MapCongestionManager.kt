package com.ncs.breeze.components.map.profile

import android.content.Context
import com.breeze.model.api.response.tbr.ResponseCongestionDetail
import com.ncs.breeze.common.utils.BreezeMapDataHolder
import com.ncs.breeze.common.utils.BreezeMapZoomUtil
import com.ncs.breeze.common.utils.LocationBreezeManager
import com.mapbox.maps.EdgeInsets
import com.mapbox.maps.MapboxMap
import com.mapbox.maps.Style

class MapCongestionManager(val mapboxMap: MapboxMap, val context: Context, val style: Style) {

    private val profileZoneLayer = ProfileZoneLayer(mapboxMap, context, style)

    fun reDrawCircle(
        selectedMapProfile: String?,
        shouldMoveCamera: Boolean,
        edgeInset: EdgeInsets? = null
    ) {
        profileZoneLayer?.let { profileZone ->
            BreezeMapDataHolder.profilePreferenceHashmap[selectedMapProfile]?.zones?.let {
                profileZone.remove()
                profileZone.updateConfigCircleLayer(it)
                profileZone.addSourceAndLayerID()
                profileZone.drawCircle()
                if (shouldMoveCamera && edgeInset != null) {
                    BreezeMapZoomUtil.recenterMapToOuterCircle(
                        mapboxMap,
                        profileZoneLayer.selectedProfilePoint!!,
                        profileZoneLayer.maxRadius,
                        edgeInset = edgeInset,
                    )
                }
            }
        }
    }


    fun zoomCameraDependOnCurrentLocation(
        selectedMapProfile: String?,
        edgeInset: EdgeInsets? = null
    ) {
        profileZoneLayer.let { profileZone ->
            BreezeMapDataHolder.profilePreferenceHashmap[selectedMapProfile]?.zones?.let {
                if (edgeInset != null) {
                    if (LocationBreezeManager.getInstance().currentLocation != null) {
                        BreezeMapZoomUtil.zoomCameraDependOnCircleIncludeCurrentLocation(
                            mapboxMap,
                            profileZoneLayer.selectedProfilePoint!!,
                            profileZoneLayer.maxRadius,
                            edgeInset = edgeInset,
                            LocationBreezeManager.getInstance().currentLocation!!
                        )
                    } else {
                        BreezeMapZoomUtil.recenterMapToOuterCircle(
                            mapboxMap,
                            profileZoneLayer.selectedProfilePoint!!,
                            profileZoneLayer.maxRadius,
                            edgeInset = edgeInset,
                        )
                    }
                }
            }
        }
    }


    /**
     * update color circle layer
     */
    fun updateColorCircleLayer(
        pListColorResponse: java.util.ArrayList<ResponseCongestionDetail>,
        selectedMapProfile: String?
    ) {
        profileZoneLayer?.let { profileZone ->
            profileZone.updateColorOuterRing(pListColorResponse)
            BreezeMapDataHolder.profilePreferenceHashmap[selectedMapProfile]?.zones?.let {
                profileZone.updateZoneZoomDetails(it)
            }
        }
    }


    /**
     * show tiong bahru zone
     */
    fun showProfileZoneLayer(shouldMoveCamera: Boolean, edgeInset: EdgeInsets? = null) {
        profileZoneLayer?.let { profileZone ->
            profileZone.showCircleLayer()
            if (shouldMoveCamera && edgeInset != null) {
                BreezeMapZoomUtil.recenterMapToOuterCircle(
                    mapboxMap,
                    profileZoneLayer.selectedProfilePoint!!,
                    profileZoneLayer.maxRadius,
                    edgeInset = edgeInset,
                )
            }
        }
    }

    fun hideProfileZoneLayer() {
        profileZoneLayer?.hideCircleLayer()
    }


}
package com.ncs.breeze.common.helper.network

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import com.ncs.breeze.common.extensions.android.isConnected
import com.ncs.breeze.common.utils.Variables


class NetworkConnection(private val context: Context) {
    private var connectivityManager: ConnectivityManager =
        context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

    private val listener: MutableList<ConnectivityReceiverListener>
    init {
        listener = arrayListOf()
        connectivityManager.registerDefaultNetworkCallback(getConnectivityManagerCallback())
    }
    fun addListener(pListener: ConnectivityReceiverListener) {
        pListener.setCurrentNetworkState(context.isConnected)
        listener.add(pListener)
    }

    private fun getConnectivityManagerCallback(): ConnectivityManager.NetworkCallback {
        return object : ConnectivityManager.NetworkCallback() {
            override fun onCapabilitiesChanged(
                network: Network,
                networkCapabilities: NetworkCapabilities
            ) {
                networkCapabilities.let { capabilities ->
                    if (capabilities.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET) && capabilities.hasCapability(
                            NetworkCapabilities.NET_CAPABILITY_VALIDATED
                        )
                    ) {
                        Variables.isNetworkConnected = true
                        listener.forEach { callback ->
                            callback.onNetworkConnectionChanged(true)
                        }

                    }
                }
            }

            override fun onLost(network: Network) {
                Variables.isNetworkConnected = false
                listener.forEach { callback ->
                    callback.onNetworkConnectionChanged(false)
                }
            }
        }
    }

    interface ConnectivityReceiverListener {
        fun setCurrentNetworkState(isConnected: Boolean){}
        fun onNetworkConnectionChanged(isConnected: Boolean)
    }

}

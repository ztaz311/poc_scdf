package com.ncs.breeze.common.utils.walkathontracker

import android.content.Context
import android.location.Location
import android.util.Log
import com.ncs.breeze.common.appSync.AppSyncHelper
import com.ncs.breeze.common.utils.InitObjectUtilsController
import com.ncs.breeze.common.utils.LocationBreezeManager

class WalkathonEngine {

    private var context: Context? = null // FixME - use weak reference here
    var currentLocation: Location? = null
    var walkathonRequest: WalkathonRequest? = null
    private val walkathonLocationObserver = WalkathonLocationObserver(this)

    fun updateLocation(lastLocation: Location?) {
        if (lastLocation != null && walkathonRequest != null) {
            Log.d("bangnv_walkathon", "updateLocation Walkathon")
            currentLocation = lastLocation
            walkathonRequest?.latitude = currentLocation?.latitude.toString()
            walkathonRequest?.longitude = currentLocation?.longitude.toString()
            walkathonRequest?.currentTime = System.currentTimeMillis().toString()
            publishLiveLocation()
        }
    }


    fun publishLiveLocation() {
        if (walkathonRequest != null && currentLocation != null) {
            Log.d("bangnv_walkathon", "publishLiveLocation Walkathon")
            AppSyncHelper.publishLiveLocationForWalkathonData(
                context = context!!,
                userId = walkathonRequest!!.userId,
                walkathonId = walkathonRequest!!.walkathonId,
                latitude = currentLocation!!.latitude.toString(),
                longitude = currentLocation!!.longitude.toString(),
                currentTime = walkathonRequest!!.currentTime,
                data = ""
            )
        }
    }

    /**
     * get walkathon location update instance
     */
    fun getWalkathonLocationUpdateInstance(): WalkathonLocationObserver {
        return walkathonLocationObserver
    }


    fun createNewWalkathonRequest(
        pWalkathonId: String,
        pLatitude: String,
        pLongitude: String
    ): WalkathonRequest {
        return WalkathonRequest().apply {
            userId = InitObjectUtilsController.cognitoUserID.toString()
            walkathonId = pWalkathonId
            latitude = pLatitude
            longitude = pLongitude
            currentTime = System.currentTimeMillis().toString()
        }
    }

    fun isShouldEnableTrackingLocation(): Boolean {
        return walkathonRequest != null
    }

    fun startCheckingLocationForWalkathon() {
        if (isShouldEnableTrackingLocation()) {
            Log.d("bangnv_walkathon", "startCheckingLocationForWalkathon")
            LocationBreezeManager.getInstance().startLocationUpdates()
            getInstance()?.getWalkathonLocationUpdateInstance()?.let {
                LocationBreezeManager.getInstance().registerLocationCallback(it)
            }
        }

    }

    fun stopCheckingLocationForWalkathon() {
        Log.d("bangnv_walkathon", "stopCheckingLocationForWalkathon")
        walkathonRequest = null
        try {
            LocationBreezeManager.getInstance().removeLocationCallBack(walkathonLocationObserver)
        } catch (e: Exception) {
        }
    }

    companion object {
        var mInstance: WalkathonEngine? = null

        fun init(context: Context) {
            if (mInstance == null) {
                mInstance = WalkathonEngine()
            }
            mInstance!!.context = context
        }

        fun updateWalkathonRequest(request: WalkathonRequest?) {
            Log.d("bangnv_walkathon", "updateWalkathonRequest")
            mInstance?.walkathonRequest = request
        }

        fun getInstance(): WalkathonEngine? {
            return mInstance
        }
    }

}
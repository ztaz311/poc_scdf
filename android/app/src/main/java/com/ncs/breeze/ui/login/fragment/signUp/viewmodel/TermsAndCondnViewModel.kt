package com.ncs.breeze.ui.login.fragment.signUp.viewmodel

import android.app.Application
import com.breeze.model.api.ErrorData
import com.ncs.breeze.common.remote.ApiHelper
import com.ncs.breeze.common.remote.ApiObserver
import com.ncs.breeze.common.storage.BreezeUserPreference
import com.facebook.react.bridge.UiThreadUtil.runOnUiThread
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.ncs.breeze.common.extensions.android.getApp
import com.ncs.breeze.ui.base.BaseFragmentViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject

class TermsAndCondnViewModel @Inject constructor(
    val apiHelper: ApiHelper,
    application: Application
) : BaseFragmentViewModel(
    application
) {

    var mAPIHelper: ApiHelper = apiHelper

    fun updateTncStatus() {
        val request = JsonObject()
        request.addProperty(
            "tncAcceptStatus",
            BreezeUserPreference.getInstance(getApp()).getTncStatus()
        )

        mAPIHelper.updateTncAcceptedStatus(request)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : ApiObserver<JsonElement>(compositeDisposable) {
                override fun onSuccess(data: JsonElement) {
                    Timber.d("Update T&C status API Successful")
                    Timber.d("Response from update T&C status API ${data}")
                    runOnUiThread {
                        BreezeUserPreference.getInstance(getApp()).clearTncStatus()
                    }
                }

                override fun onError(e: ErrorData) {
                    Timber.d("API failed: " + e.toString())
                }
            })
    }
}
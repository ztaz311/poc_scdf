package com.ncs.breeze.car.breeze.screen.confirmation.share

import androidx.car.app.OnScreenResultListener
import androidx.car.app.model.Action
import androidx.car.app.model.MessageTemplate
import androidx.car.app.model.Template
import com.breeze.model.api.response.eta.RecentETAContact
import com.ncs.breeze.common.utils.Utils
import com.breeze.model.enums.ETAMode
import com.ncs.breeze.car.breeze.MainBreezeCarContext
import com.ncs.breeze.car.breeze.base.BaseScreenCar
import com.ncs.breeze.car.breeze.model.ETARouteDetails
import com.ncs.breeze.car.breeze.utils.LimitClick
import com.ncs.breeze.car.breeze.utils.setClickSafe
import com.ncs.breeze.R
import com.ncs.breeze.common.analytics.Screen

/**
 * Supports confirmation messages for
 * Routeplanning
 * Navigation
 * Cruisemode
 */
class ShareConfirmationScreen(
    val mainCarContext: MainBreezeCarContext,
    private val etaMode: ETAMode,
    private val contactItem: RecentETAContact?,
    private val etaRouteDetails: ETARouteDetails?,
    private val onScreenResultListener: OnScreenResultListener,
) : BaseScreenCar(mainCarContext.carContext) {

    override fun onGetTemplate(): Template {

        contactItem?.let {
            return showContactSelectionConfirmation(it)
        }
        return showNoSelectionConfirmation()
    }

    private fun showContactSelectionConfirmation(mContactItem: RecentETAContact): MessageTemplate {
        val message = if (etaMode == ETAMode.Cruise || etaMode == ETAMode.Navigation) {
            carContext.getString(
                R.string.car_share_confirmation_body,
                Utils.capitalize(mContactItem.recipientName ?: "")
            )
        } else {
            carContext.getString(
                R.string.car_share_long_confirmation_body,
                Utils.capitalize(mContactItem.recipientName ?: "")
            )
        }

        val builder = MessageTemplate.Builder(
            message
        )
        builder.setTitle(carContext.getString(R.string.car_share_confirmation_title))
        builder.addAction(
            Action.Builder()
                .setTitle(carContext.getString(R.string.car_share_confirmation_cancel))
                .setClickSafe {
                    LimitClick.handleSafe {
                        onScreenResultListener.onScreenResult(null)
                        this.finish()
                    }
                }.build()
        )
        builder.addAction(
            Action.Builder()
                .setTitle(carContext.getString(R.string.car_share_confirmation_share))
                .setClickSafe {
                    LimitClick.handleSafe {
                        onScreenResultListener.onScreenResult(contactItem)
                        this.finish()
                    }
                }.build()
        )
        return builder.build()
    }

    private fun showNoSelectionConfirmation(): MessageTemplate {
        val builder = MessageTemplate.Builder(
            carContext.getString(
                R.string.car_share_confirmation_body_failure
            )
        )
        builder.setTitle(carContext.getString(R.string.car_share_confirmation_title))
        builder.addAction(
            Action.Builder()
                .setTitle(carContext.getString(R.string.car_share_confirmation_ok))
                .setClickSafe {
                    LimitClick.handleSafe {
                        onScreenResultListener.onScreenResult(null)
                        this.finish()
                    }
                }.build()
        )
        return builder.build()
    }

    override fun getScreenName(): String {
        return Screen.AA_SHARE_CONFIRMATION
    }
}

package com.ncs.breeze.ui.dashboard

import android.location.Location

interface LocationFocus {
    var activeLocationUse: Location?
    var currentLocationFocus: Location?
    var forceFocusZoom: Double?
    var currentFocusInstance: Int?


    fun setCurrentLocationFocus(type: Int, pLatitude: Double, pLong: Double, zoomRange: Double?)

    fun resetCurrentLocationFocus(type: Int)

    fun resetCurrentLocationFocus()
}
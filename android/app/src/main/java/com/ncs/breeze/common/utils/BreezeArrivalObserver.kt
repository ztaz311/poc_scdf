package com.ncs.breeze.common.utils


import com.breeze.model.DestinationAddressDetails
import com.mapbox.navigation.base.trip.model.RouteLegProgress
import com.mapbox.navigation.base.trip.model.RouteProgress
import com.mapbox.navigation.core.MapboxNavigation
import com.mapbox.navigation.core.arrival.ArrivalObserver
import com.mapbox.navigation.core.lifecycle.MapboxNavigationApp
import timber.log.Timber
import java.util.concurrent.CopyOnWriteArraySet

class BreezeArrivalObserver private constructor(
    private val screen: String,
    private val currentDestinationDetails: DestinationAddressDetails
) {

    private val navigationArrivalObserverSet = CopyOnWriteArraySet<NavigationArrivalObserver>()


    val arrivalObserver = object : ArrivalObserver {
        override fun onFinalDestinationArrival(routeProgress: RouteProgress) {
            Timber.d("arrival observer called")
            //play voice
            VoiceInstructionsManager.getInstance()?.playVoiceInstructions(ARRIVAL_MESSAGE)
            navigationArrivalObserverSet.forEach {
                it.onFinalDestinationArrival(routeProgress)
            }
        }

        override fun onNextRouteLegStart(routeLegProgress: RouteLegProgress) {
            navigationArrivalObserverSet.forEach {
                it.onNextRouteLegStart(routeLegProgress)
            }
//            arrival_notification.visibility = View.GONE
        }

        override fun onWaypointArrival(routeProgress: RouteProgress) {
            navigationArrivalObserverSet.forEach {
                it.onWaypointArrival(routeProgress)
            }
        }

    }

    private fun stopArrivalObserver() {
        Utils.isNavigationStarted = false
        Utils.phoneNavigationStartEventData = null
        MapboxNavigationApp.current()?.let { mapboxNavigation ->
            BreezeTripManagerUtil.stopTrip(
                mapboxNavigation!!,
                currentDestinationDetails,
                screen,
                true
            )
            mapboxNavigation!!.setNavigationRoutes(listOf())
        }

        destroy()
    }

    fun registerNavigationArrivalObserver(navigationArrivalObserver: NavigationArrivalObserver) {
        navigationArrivalObserverSet.add(navigationArrivalObserver)
    }

    fun unregisterNavigationArrivalObserver(
        navigationArrivalObserver: NavigationArrivalObserver,
        shouldStopTrip: Boolean = true
    ) {
        if (shouldStopTrip) {
            stopArrivalObserver()
        }
        navigationArrivalObserverSet.remove(navigationArrivalObserver)
    }

    private fun clearObservers() {
        navigationArrivalObserverSet.clear()
    }

    companion object {
        private const val ARRIVAL_MESSAGE = "You have arrived"
        private var mInstance: BreezeArrivalObserver? = null


        fun getInstance(): BreezeArrivalObserver? {
            return mInstance
        }

        fun create(
            mapboxNavigation: MapboxNavigation?,
            screen: String,
            currentDestinationDetails: DestinationAddressDetails
        ) {
            if (mInstance == null) {
                mInstance = BreezeArrivalObserver(screen, currentDestinationDetails)
                mapboxNavigation?.registerArrivalObserver(mInstance!!.arrivalObserver)
            }
        }

        fun destroy() {
            mInstance?.let {
                MapboxNavigationApp.current()?.unregisterArrivalObserver(it.arrivalObserver)
                it.clearObservers()
            }
            mInstance = null
        }
    }

}


interface NavigationArrivalObserver {
    fun onFinalDestinationArrival(routeProgress: RouteProgress)
    fun onNextRouteLegStart(routeProgress: RouteLegProgress)
    fun onWaypointArrival(routeProgress: RouteProgress)
}
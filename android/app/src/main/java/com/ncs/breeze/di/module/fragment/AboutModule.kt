package com.ncs.breeze.di.module.fragment

import com.ncs.breeze.ui.dashboard.fragments.view.AboutFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class AboutModule {
    @ContributesAndroidInjector
    abstract fun contributeAboutFragment(): AboutFragment
}
package com.ncs.breeze.components

import android.app.Service
import android.location.Location
import android.os.Handler
import com.mapbox.navigation.core.lifecycle.MapboxNavigationApp
import com.mapbox.navigation.core.trip.session.LocationMatcherResult
import com.mapbox.navigation.core.trip.session.LocationObserver
import com.ncs.breeze.common.appSync.AppSyncHelper
import com.ncs.breeze.common.extensions.android.getOBUConnHelper
import java.lang.ref.WeakReference

class UserLocationForOBUEventsPublisher(navigationControllerService: Service) {
    private val serviceRef = WeakReference(navigationControllerService)
    private var lastUserEnhancedLocation: LocationMatcherResult? = null
    private val locationObserver by lazy {
        object : LocationObserver {
            override fun onNewLocationMatcherResult(locationMatcherResult: LocationMatcherResult) {
                lastUserEnhancedLocation = locationMatcherResult
            }

            override fun onNewRawLocation(rawLocation: Location) {}
        }
    }
    private var userLocationPublishingForOBUEventsHandler: Handler? = null

     fun startPublishUserLocation() {
        val service = serviceRef.get() ?: return
        MapboxNavigationApp.current()?.registerLocationObserver(locationObserver)
        userLocationPublishingForOBUEventsHandler = Handler(service.mainLooper) { message ->
            when (message.what) {
                WHAT_PUBLISH_LAST_USER_LOCATION -> {
                    publishLastUserLocationForOBUEvents()
                    userLocationPublishingForOBUEventsHandler?.sendEmptyMessageDelayed(
                        WHAT_PUBLISH_LAST_USER_LOCATION, 1000L
                    )
                }
            }
            false
        }.also {
            it.sendEmptyMessage(WHAT_PUBLISH_LAST_USER_LOCATION)
        }
    }

     fun stopPublishingUserLocation() {
        lastUserEnhancedLocation = null
        userLocationPublishingForOBUEventsHandler?.removeCallbacksAndMessages(null)
        userLocationPublishingForOBUEventsHandler = null
    }

    private fun publishLastUserLocationForOBUEvents() {
        val service = serviceRef.get() ?: return
        val enhancedLocation = lastUserEnhancedLocation ?: return
        AppSyncHelper.publishLiveLocationOBU(service, enhancedLocation)
    }

    companion object {
        private const val WHAT_PUBLISH_LAST_USER_LOCATION = 5001
    }

}
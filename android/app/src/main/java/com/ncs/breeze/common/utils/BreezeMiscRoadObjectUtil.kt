package com.ncs.breeze.common.utils

import com.breeze.model.api.ErrorData
import com.ncs.breeze.common.remote.ApiHelper
import com.ncs.breeze.common.remote.ApiObserver
import com.breeze.model.NavigationZone
import com.ncs.breeze.common.rxbus.RxBus
import com.ncs.breeze.common.rxbus.RxEvent
import com.breeze.model.extensions.safeDouble
import com.google.gson.Gson
import com.google.gson.JsonElement
import com.google.gson.internal.Streams
import com.google.gson.stream.JsonReader
import com.mapbox.geojson.Feature
import com.mapbox.geojson.FeatureCollection
import com.mapbox.geojson.Point
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import timber.log.Timber
import java.io.StringReader
import java.util.concurrent.ConcurrentHashMap
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class BreezeMiscRoadObjectUtil @Inject constructor(apiHelper: ApiHelper) {

    var apiHelper: ApiHelper = apiHelper
    var compositeDisposable: CompositeDisposable = CompositeDisposable()

    var miscFeatures: ConcurrentHashMap<NavigationZone, FeatureCollection> = ConcurrentHashMap()
    var miscDetectionEnabled: Boolean = false;

    fun startMiscROHandler() {
        synchronized(this) {
            if (!miscDetectionEnabled) {
                miscDetectionEnabled = true
                subscribeToAppSyncRxEvent()
                retrieveSpeedLightCameraData()
            }
        }
    }

    fun subscribeToAppSyncRxEvent() {
        compositeDisposable.add(
            RxBus.listen(RxEvent.MiscROAppSyncEvent::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError {
                    Timber.d("Observing on error")
                }
                .subscribe {
                    Timber.d("Misc RO event observed successfully")

                    try {
                        var jsonElement =
                            Streams.parse(JsonReader(StringReader(it.miscObjectData)))
                        createFeaturesFromSpeedLightData(jsonElement)
                    } catch (e: Exception) {
                        Timber.e(e, "Error incorrect json")
                    }
                }
        )
    }

    private fun retrieveSpeedLightCameraData() {
        apiHelper.getSpeedLightCameras()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : ApiObserver<JsonElement>(compositeDisposable) {
                override fun onSuccess(data: JsonElement) {
                    createFeaturesFromSpeedLightData(data)
                }

                override fun onError(e: ErrorData) {
                    Timber.d("API failed: " + e.toString())
                }

            })
    }


    fun createFeaturesFromSpeedLightData(speedCameraElements: JsonElement) {
        Timber.d("Creating mis ro from data")
        var json = Gson().toJson(speedCameraElements)
        var speedCameras = JSONArray(json)
        createFeaturesFromSpeedLightData(speedCameras)
    }

    private fun createFeaturesFromSpeedLightData(speedCameras: JSONArray) {
        miscFeatures.clear()
        val speedCamerasIterator: Iterator<JSONObject> = speedCameras.iterator()
        val speedCameraFeatures: ArrayList<Feature> = arrayListOf()
        while (speedCamerasIterator.hasNext()) {
            try {
                val speedCamera: Any = speedCamerasIterator.next()
                if (speedCamera is JSONObject) {
                    if (speedCamera.has("location_latitude") && speedCamera.has("location_longitude") && speedCamera.has(
                            "_id"
                        )
                    ) {
                        var feature = Feature.fromGeometry(
                            Point.fromLngLat(
                                speedCamera.getString("location_longitude").safeDouble(),
                                speedCamera.getString("location_latitude").safeDouble()
                            ),
                            null,
                            speedCamera.getInt("_id").toString()
                        )
                        try {
                            feature.addStringProperty("location", speedCamera.getString("location"))
                            feature.addStringProperty(
                                "type_of_speed_camera",
                                speedCamera.getString("type_of_speed_camera")
                            )
                        } catch (e: JSONException) {
                            //no op
                            Timber.e(e)
                            Timber.e("missing feature metadata for speed cameras")
                        }
                        speedCameraFeatures.add(feature)
                    }
                }
            } catch (e: JSONException) {
                // Something went wrong!
                Timber.e(e)
            }
        }
        miscFeatures[NavigationZone.SPEED_CAMERA] =
            FeatureCollection.fromFeatures(speedCameraFeatures)

        Timber.d("Triggering misc ro refresh")
        RxBus.publish(RxEvent.MiscRODataRefresh(miscFeatures))

    }


    fun getLatestMiscROData(): ConcurrentHashMap<NavigationZone, FeatureCollection> {
        return miscFeatures
    }

    fun stopMiscROHandler() {
        Timber.d("Stop misc ro handler called")
        if (!compositeDisposable.isDisposed) {
            compositeDisposable.clear()
        }
        miscDetectionEnabled = false
    }

    operator fun JSONArray.iterator(): Iterator<JSONObject> =
        (0 until length()).asSequence().map { get(it) as JSONObject }.iterator()


}
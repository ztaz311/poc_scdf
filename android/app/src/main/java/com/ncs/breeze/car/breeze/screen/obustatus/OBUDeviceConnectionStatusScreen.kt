package com.ncs.breeze.car.breeze.screen.obustatus

import android.annotation.SuppressLint
import androidx.activity.OnBackPressedCallback
import androidx.car.app.model.MessageTemplate
import com.ncs.breeze.car.breeze.MainBreezeCarContext
import com.ncs.breeze.car.breeze.base.BaseScreenCar
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.Event
import com.ncs.breeze.common.analytics.Screen
import com.ncs.breeze.common.constant.OBUConnectionState
import com.ncs.breeze.common.extensions.android.getApp
import com.ncs.breeze.common.extensions.android.getOBUConnHelper
import com.ncs.breeze.common.extensions.android.getUserDataPreference
import sg.gov.lta.obu.sdk.conn.OBU
import sg.gov.lta.obu.sdk.core.types.OBUError
import sg.gov.lta.obu.sdk.data.interfaces.OBUConnectionStatusListener
import sg.gov.lta.obu.sdk.data.services.OBUSDK

class OBUDeviceConnectionStatusScreen(
    private val mainBreezeCarContext: MainBreezeCarContext,
    private val isRetryConnection: Boolean = false,
    private val fromScreen: String
) : BaseScreenCar(mainBreezeCarContext.carContext) {
    private val templateFactory by lazy { OBUConnectionStatusScreenTemplateFactory(carContext.getApp()!!) }

    private var isRetrying = isRetryConnection

    private val callbackNotPairedState = object : OnBackPressedCallback(
        false // default to enabled
    ) {
        override fun handleOnBackPressed() {
            Analytics.logClickEvent(
                Event.AA_OBU_NOT_PAIRED_BACK,
                fromScreen
            )
            screenManager.pop()
        }
    }

    private val callbackConnectedState = object : OnBackPressedCallback(
        false // default to enabled
    ) {
        override fun handleOnBackPressed() {
            Analytics.logClickEvent(
                Event.AA_SHOW_BALANCE_BACK,
                fromScreen
            )
            screenManager.pop()
        }
    }

    private val callbackRetryError = object : OnBackPressedCallback(
        false // default to enabled
    ) {
        override fun handleOnBackPressed() {
            Analytics.logClickEvent(
                Event.AA_POPUP_REPORT_ISSUE_BACK,
                fromScreen
            )
            screenManager.pop()
        }
    }
    private val callbackOBUNotConnect = object : OnBackPressedCallback(
        false // default to enabled
    ) {
        override fun handleOnBackPressed() {
            Analytics.logClickEvent(
                Event.AA_OBU_NOT_CONNECTED_BACK,
                fromScreen
            )
            screenManager.pop()
        }
    }


    @SuppressLint("MissingPermission")
    override fun onCreateScreen() {
        super.onCreateScreen()
        carContext.onBackPressedDispatcher.addCallback({ lifecycle }, callbackNotPairedState)
        carContext.onBackPressedDispatcher.addCallback({ lifecycle }, callbackConnectedState)
        carContext.onBackPressedDispatcher.addCallback({ lifecycle }, callbackRetryError)
        carContext.onBackPressedDispatcher.addCallback({ lifecycle }, callbackOBUNotConnect)
        if (isRetrying) {
            val lastDevice = carContext.getUserDataPreference()?.getLastConnectedOBUDevice()
            val obu = OBUSDK.getPairedDevices()
                ?.find { obu -> obu.name == lastDevice?.obuName } ?: OBU(
                null, lastDevice!!.obuName
            )
            carContext.getOBUConnHelper()?.startOBUConnection(
                lastDevice!!.vehicleNumber,
                obu,
                false,
                object : OBUConnectionStatusListener {
                    override fun onOBUConnected(device: OBU) {
                        super.onOBUConnected(device)
                        isRetrying = false
                        invalidate()
                    }

                    override fun onOBUConnectionFailure(error: OBUError) {
                        super.onOBUConnectionFailure(error)
                        isRetrying = false
                        invalidate()
                    }
                })
        }
    }


    override fun getScreenName() = Screen.AA_OBU_CONNECTION_STATUS_SCREEN

    override fun onGetTemplate(): MessageTemplate =
        when {
            !haveLastConnectedOBUDevice() -> {
                Analytics.logPopupEvent(
                    Event.AA_POPUP_OBU_NOT_PAIRED,
                    Event.VALUE_POPUP_OPEN,
                    fromScreen
                )

                callbackNotPairedState.isEnabled = true
                callbackConnectedState.isEnabled = false
                callbackRetryError.isEnabled = false
                callbackOBUNotConnect.isEnabled = false
                templateFactory.createNoDevicePairedTemplate {
                    Analytics.logClickEvent(
                        Event.AA_OBU_NOT_PAIRED_OK,
                        fromScreen
                    )
                    screenManager.pop()
                }
            }

            isRetrying -> templateFactory.createRetryingTemplate()
            isOBUConnected() -> {
                Analytics.logClickEvent(
                    Event.AA_SHOW_BALANCE,
                    fromScreen
                )
                Analytics.logPopupEvent(
                    Event.AA_POPUP_OBU_CONNECTED,
                    Event.AA_POPUP_OPEN,
                    fromScreen
                )
                callbackNotPairedState.isEnabled = false
                callbackConnectedState.isEnabled = true
                callbackRetryError.isEnabled = false
                callbackOBUNotConnect.isEnabled = false
                templateFactory.createConnectedTemplate(carContext)
            }

            isRetryConnection && !isRetrying -> {
                Analytics.logPopupEvent(
                    Event.AA_CONNECTION_ERROR,
                    Event.VALUE_POPUP_OPEN,
                    fromScreen
                )
                callbackNotPairedState.isEnabled = false
                callbackConnectedState.isEnabled = false
                callbackRetryError.isEnabled = true
                callbackOBUNotConnect.isEnabled = false
                templateFactory.createRetryErrorTemplate(fromScreen)
            }

            else -> {
                Analytics.logPopupEvent(
                    Event.AA_POPUP_OBU_NOT_CONNECTED,
                    Event.VALUE_POPUP_OPEN,
                    fromScreen
                )
                callbackNotPairedState.isEnabled = false
                callbackConnectedState.isEnabled = false
                callbackRetryError.isEnabled = false
                callbackOBUNotConnect.isEnabled = true
                templateFactory.createNotConnectTemplate(onRetry = {
                    Analytics.logClickEvent(
                        Event.AA_OBU_NOT_CONNECTED_RETRY,
                        fromScreen
                    )
                    retryConnectingLastOBU()
                })
            }
        }


    private fun retryConnectingLastOBU() {
        // Keep this screen on top of screen stack, wait for next OBU connection status
        // Post event to Breeze App on Phone
        screenManager.pop()
        val isHavingSavedDevice =
            carContext.getUserDataPreference()?.getLastConnectedOBUDevice() != null
        screenManager.push(
            OBUDeviceConnectionStatusScreen(
                mainBreezeCarContext,
                isHavingSavedDevice,
                fromScreen
            )
        )
    }

    private fun isOBUConnected() =
        carContext.getOBUConnHelper()?.obuConnectionState?.value == OBUConnectionState.CONNECTED

    private fun haveLastConnectedOBUDevice() =
        (carContext.getUserDataPreference()?.getConnectedOBUDevices()?.size ?: 0) > 0
}
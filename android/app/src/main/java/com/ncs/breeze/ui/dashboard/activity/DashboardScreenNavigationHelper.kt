package com.ncs.breeze.ui.dashboard.activity

import androidx.core.os.bundleOf
import com.breeze.model.DestinationAddressDetails
import com.breeze.model.constants.Constants
import com.breeze.model.enums.AndroidScreen
import com.breeze.model.extensions.rn.getStringOrNull
import com.facebook.react.bridge.ReadableMap
import com.ncs.breeze.common.constant.RNScreen
import com.ncs.breeze.common.utils.LocationBreezeManager
import com.ncs.breeze.common.utils.Utils
import com.ncs.breeze.ui.dashboard.fragments.placesave.PlaceSaveDetailFragment
import com.ncs.breeze.ui.dashboard.fragments.view.RoutePlanningFragment
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import timber.log.Timber
import java.lang.ref.WeakReference

class DashboardScreenNavigationHelper(private val dashboardActivityRef: WeakReference<DashboardActivity>) {
    fun openRoutePlanningFromRN(navigationParams: ReadableMap?) {
        dashboardActivityRef.get()?.run {
            runOnUiThread {
                supportFragmentManager.findFragmentByTag(RNScreen.PARKING_CALCULATOR)?.let {
                    supportFragmentManager.executePendingTransactions()
                    supportFragmentManager
                        .beginTransaction()
                        .remove(it)
                        .commitNow()
                }

                val routePlaningFragment =
                    supportFragmentManager.findFragmentByTag(Constants.TAGS.ROUTE_PLANNING) as? RoutePlanningFragment
                if (routePlaningFragment != null && !routePlaningFragment.isRemoving) {
                    runOnUiThread {
                        routePlaningFragment.navigateFromParkingCalculator(
                            lat = navigationParams?.getDouble("lat")?.toString(),
                            long = navigationParams?.getDouble("long")?.toString(),
                            name = navigationParams?.getString("name"),
                            carParkId = navigationParams?.getString("parkingID")
                        )
                    }
                } else {
                    callAfterInitialized {
                        runOnUiThread {
                            navigationParams?.getString("parkingID")?.let {
                                startRoutePlanningFromParkingCalculator(
                                    lat = navigationParams.getDouble("lat")
                                        .toString(),
                                    long = navigationParams.getDouble("long").toString(),
                                    name = navigationParams.getString("name"),
                                    carParkId = navigationParams.getString("parkingID")
                                )
                            }
                        }
                    }
                }
            }
        }

    }

    fun openCollectionDetailsFromRN(navigationParams: ReadableMap?) {
        dashboardActivityRef.get()?.run {
            toggleDropPinMode(false)
            runOnUiThread {
                addFragment(
                    fragment = PlaceSaveDetailFragment(),
                    bundle = navigationParams?.let {
                        bundleOf(
                            PlaceSaveDetailFragment.ARG_COLLECTION_ID to it.getInt("collection_id"),
                            PlaceSaveDetailFragment.ARG_TO_RN_SCREEN to it.getString("to_rn_screen"),
                            PlaceSaveDetailFragment.ARG_COLLECTION_NAME to it.getString("collection_name"),
                            PlaceSaveDetailFragment.ARG_COLLECTION_CODE to it.getString("code"),
                            PlaceSaveDetailFragment.ARG_COLLECTION_DESCRIPTION to it.getStringOrNull(
                                "collection_description"
                            )
                        )
                    },
                    tag = AndroidScreen.COLLECTION_DETAILS.nameStr
                )
            }
        }
    }

    fun openTripLogFromRN(navigationParams: ReadableMap?) {
        dashboardActivityRef.get()?.run {
            callAfterInitialized {
                runOnUiThread {
                    addTripLogsFragment(navigationParams)
                }
            }
        }
    }

    fun addRoutePlanningFragmentFromRN(destinationData: HashMap<String, Any>) {
        val currentLocation = LocationBreezeManager.getInstance().currentLocation ?: return
        val destinationAddress =
            DestinationAddressDetails.parseSearchLocationDataFromRN(destinationData)
        CoroutineScope(Dispatchers.IO).launch {
            Timber.d("Printing the react native data: ${destinationData}")
            val sourceAddressDetails = DestinationAddressDetails(
                easyBreezyAddressID = null,
                address1 = null,
                address2 = null,
                lat = currentLocation.latitude.toString(),
                long = currentLocation.longitude.toString(),
                distance = null,
                destinationName = Constants.TAGS.CURRENT_LOCATION_TAG,
                destinationAddressType = null,
                ranking = -1
            )

            dashboardActivityRef.get()?.let { dashboardActivity ->
                val sourceAddress1 =
                    Utils.getAddressFromLocation(currentLocation, dashboardActivity)
                sourceAddressDetails.address1 = sourceAddress1
            }

            // Because the previous block is async, reference to DashboardActivity can be lost while it was running
            dashboardActivityRef.get()?.run {
                runOnUiThread {
                    showRouteAndResetToCenter(
                        bundleOf(
                            Constants.DESTINATION_ADDRESS_DETAILS to destinationAddress,
                            Constants.SOURCE_ADDRESS_DETAILS to sourceAddressDetails
                        )
                    )
                }
            }
        }
    }
}
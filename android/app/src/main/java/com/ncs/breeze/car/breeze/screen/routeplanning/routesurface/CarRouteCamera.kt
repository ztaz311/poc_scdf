package com.ncs.breeze.car.breeze.screen.routeplanning.routesurface

import android.graphics.Rect
import com.ncs.breeze.common.utils.LocationBreezeManager
import com.mapbox.maps.extension.androidauto.MapboxCarMapSurface
import com.mapbox.maps.extension.androidauto.MapboxCarMapObserver
import com.mapbox.androidauto.internal.logAndroidAuto
import com.ncs.breeze.car.breeze.screen.routeplanning.RoutePreviewCarContext
import com.mapbox.maps.CameraOptions
import com.mapbox.maps.EdgeInsets
import com.mapbox.maps.plugin.animation.camera
import com.mapbox.maps.plugin.locationcomponent.LocationComponentPlugin
import com.mapbox.navigation.core.MapboxNavigation
import com.mapbox.navigation.core.directions.session.RoutesObserver
import com.mapbox.navigation.core.lifecycle.MapboxNavigationApp
import com.mapbox.navigation.core.trip.session.RouteProgressObserver
import com.mapbox.navigation.ui.maps.camera.NavigationCamera
import com.mapbox.navigation.ui.maps.camera.data.MapboxNavigationViewportDataSource
import com.mapbox.navigation.ui.maps.camera.transition.NavigationCameraTransitionOptions

private const val DEFAULT_INITIAL_ZOOM = 15.0

/**
 * Integrates the Android Auto [MapboxCarMapSurface] with the [NavigationCamera].
 */
class CarRouteCamera(
    val routePreviewCarContext: RoutePreviewCarContext,
    val cameraMode: CameraMode,
    private val initialCameraOptions: CameraOptions? = CameraOptions.Builder()
        .zoom(DEFAULT_INITIAL_ZOOM)
        .build()
) : MapboxCarMapObserver {
    private var mapboxCarMapSurface: MapboxCarMapSurface? = null
    private lateinit var navigationCamera: NavigationCamera
    private lateinit var viewportDataSource: MapboxNavigationViewportDataSource

    private var isLocationInitialized = false


    private var locationComponent: LocationComponentPlugin? = null


    override fun onAttached(mapboxCarMapSurface: MapboxCarMapSurface) {
        super.onAttached(mapboxCarMapSurface)
        this.mapboxCarMapSurface = mapboxCarMapSurface
        logAndroidAuto("CarNavigationCamera loaded $mapboxCarMapSurface")

        val mapboxMap = mapboxCarMapSurface.mapSurface.getMapboxMap()
        initialCameraOptions?.let { mapboxMap.setCamera(it) }
        viewportDataSource = MapboxNavigationViewportDataSource(
            mapboxCarMapSurface.mapSurface.getMapboxMap()
        )
        navigationCamera = NavigationCamera(
            mapboxMap,
            mapboxCarMapSurface.mapSurface.camera,
            viewportDataSource
        )
        LocationBreezeManager.getInstance()?.currentLocation?.let {
            viewportDataSource.onLocationChanged(it)
            //viewportDataSource.options.overviewFrameOptions.maxZoom= 14.0
            viewportDataSource.evaluate()
            if (!isLocationInitialized) {
                isLocationInitialized = true
                val instantTransition = NavigationCameraTransitionOptions.Builder()
                    .maxDuration(0)
                    .build()
                when (cameraMode) {
                    CameraMode.IDLE -> navigationCamera.requestNavigationCameraToIdle()
                    CameraMode.FOLLOWING -> navigationCamera.requestNavigationCameraToFollowing(
                        stateTransitionOptions = instantTransition
                    )

                    CameraMode.OVERVIEW -> navigationCamera
                        .requestNavigationCameraToOverview(
                            stateTransitionOptions = instantTransition
                        )
                }
            }


            /**
             * draw icon current user
             */

//            locationComponent = mapboxCarMapSurface.mapSurface.location.apply {
//                locationPuck = CarLocationPuck.RouteIconCurrentUserPuck2D(routePreviewCarContext.carContext)
//                enabled = true
//                pulsingEnabled = true
//                routePreviewCarContext.defaultLocationProvider = getLocationProvider()
//                /**
//                 * do something here
//                 */
//                locationComponent.apply {
//                    pulsingColor = routePreviewCarContext.carContext.getColor(R.color.themed_nav_pulse_color)
//                    pulsingMaxRadius = 50.0F
//                }
//            }


        }
        MapboxNavigationApp.current()?.let { mapboxNavigation ->
            mapboxNavigation.registerRoutesObserver(routeObserver)
            mapboxNavigation.registerRouteProgressObserver(routeProgressObserver)
        }
    }

    override fun onVisibleAreaChanged(visibleArea: Rect, edgeInsets: EdgeInsets) {
        super.onVisibleAreaChanged(visibleArea, edgeInsets)
        logAndroidAuto("CarNavigationCamera visibleAreaChanged $visibleArea $edgeInsets")

        viewportDataSource.overviewPadding = EdgeInsets(
            edgeInsets.top + OVERVIEW_PADDING,
            edgeInsets.left + OVERVIEW_PADDING,
            edgeInsets.bottom + OVERVIEW_PADDING,
            edgeInsets.right + OVERVIEW_PADDING
        )

        val visibleHeight = visibleArea.bottom - visibleArea.top
        val followingBottomPadding = visibleHeight * BOTTOM_FOLLOWING_PERCENTAGE
        viewportDataSource.followingPadding = EdgeInsets(
            edgeInsets.top,
            edgeInsets.left,
            edgeInsets.bottom + followingBottomPadding,
            edgeInsets.right
        )

        viewportDataSource.evaluate()
    }

    override fun onDetached(mapboxCarMapSurface: MapboxCarMapSurface) {
        super.onDetached(mapboxCarMapSurface)
        logAndroidAuto("CarNavigationCamera detached $mapboxCarMapSurface")
        MapboxNavigationApp.current()?.let { mapboxNavigation ->
            mapboxNavigation.unregisterRoutesObserver(routeObserver)
            mapboxNavigation.unregisterRouteProgressObserver(routeProgressObserver)
        }
        this.mapboxCarMapSurface = null
        isLocationInitialized = false
    }


    private val routeObserver = RoutesObserver { result ->
        if (result.navigationRoutes.isEmpty()) {
            viewportDataSource.clearRouteData()
        } else {
            viewportDataSource.onRouteChanged(result.navigationRoutes.first())
        }
        viewportDataSource.evaluate()
    }

    private val routeProgressObserver = RouteProgressObserver { routeProgress ->
        viewportDataSource.onRouteProgressChanged(routeProgress)
        viewportDataSource.evaluate()
    }

    enum class CameraMode {
        IDLE,
        FOLLOWING,
        OVERVIEW
    }

    private companion object {
        /**
         * While following the location puck, inset the bottom by 1/3 of the screen.
         */
        private const val BOTTOM_FOLLOWING_PERCENTAGE = 1.0 / 3.0

        /**
         * While overviewing a route, add padding to thew viewport.
         */
        private const val OVERVIEW_PADDING = 30
    }
}

package com.ncs.breeze.ui.dashboard.fragments.placesave

import android.location.Location
import android.os.Bundle
import com.breeze.model.DestinationAddressDetails
import com.breeze.model.SearchLocation
import com.breeze.model.api.response.PlaceDetailsResponse
import com.breeze.model.api.response.amenities.BaseAmenity
import com.breeze.model.constants.AmenityType.CARPARK
import com.breeze.model.constants.Constants
import com.breeze.model.enums.BookmarkType
import com.breeze.model.enums.CollectionCode
import com.breeze.model.extensions.safeDouble
import com.facebook.react.bridge.Arguments
import com.mapbox.geojson.Feature
import com.mapbox.geojson.Point
import com.mapbox.maps.CameraOptions
import com.mapbox.maps.MapView
import com.mapbox.maps.MapboxMap
import com.mapbox.maps.plugin.animation.easeTo
import com.mapbox.navigation.ui.maps.camera.data.MapboxNavigationViewportDataSource
import com.mapbox.turf.TurfConstants
import com.mapbox.turf.TurfMeasurement
import com.ncs.breeze.App
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.utils.LocationBreezeManager
import com.ncs.breeze.common.utils.Utils
import com.ncs.breeze.components.layermanager.MarkerLayerManager
import com.ncs.breeze.components.layermanager.impl.PlaceSaveLayerManager
import com.ncs.breeze.components.layermanager.impl.cluster.ClusteredMarkerLayerManager
import com.ncs.breeze.components.map.profile.ClusteredMapProfile
import com.ncs.breeze.components.map.profile.ListenerCallbackLandingMap
import com.ncs.breeze.components.map.profile.MapProfileOptions
import com.ncs.breeze.components.marker.markerview2.NotificationMarkerViewManager
import com.ncs.breeze.helper.carpark.CarParkDestinationIconManager
import com.ncs.breeze.reactnative.nativemodule.ShareBusinessLogicEvent
import com.ncs.breeze.ui.dashboard.manager.PlaceSaveMapStateManager
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.lang.ref.WeakReference

class MapControllerPlaceSave {

    var currentPin: Point? = null
    lateinit var mapboxMap: MapboxMap
    lateinit var mapView: MapView
    var placeSaveDetailFragmentRef = WeakReference<PlaceSaveDetailFragment>(null)

    lateinit var viewportDataSource: MapboxNavigationViewportDataSource

    //map profile instance
    lateinit var mapProfile: ClusteredMapProfile

    private var isMoveCameraWrapperAllCarpark = false

    private var mListCarpark = arrayListOf<BaseAmenity>()


    var hasDestinationCarPark: Boolean = false

    var carParkDestinationIconManager: CarParkDestinationIconManager? = null

    /**
     * for Global Notification Symbol layers
     */
    var notificationMarkerLayerManager: NotificationMarkerViewManager? = null

    lateinit var placeSaveLayerManager: PlaceSaveLayerManager

    var callBack: ListenerCallbackLandingMap? = null
    var nightModeEnabled: Boolean = false

    var selectedBookMarkParkingPoint: Point? = null
    var analyticsScreenName = ""

    private val clusterMapClickHandler = object : MarkerLayerManager.MarkerLayerClickHandler {
        override fun handleOnclick(
            layerType: String,
            feature: Feature,
            currentMarker: MarkerLayerManager.MarkerViewType?
        ) {
            // For Clustered data, feature type is not the same as layer type, so we fetch the feature type from the feature selected
            when (feature.getStringProperty(ClusteredMarkerLayerManager.FEATURE_TYPE)
                ?: layerType) {
                CARPARK -> {
                    mListCarpark.find { it.id == feature.id() }?.let {
                        showCarParkToolTip(it, CARPARK, true)
                        placeSaveLayerManager.removeAllMarkers()
                    }
                }

                BookmarkType.CUSTOM.name,
                BookmarkType.HOME.name,
                BookmarkType.WORK.name,
                BookmarkType.NORMAL.name -> {
                    //don't trigger tooltip for preview bookmarks
                    if (feature.id() == PREVIEW_BOOKMARK_ID) {
                        return
                    }
                    placeSaveLayerManager.removeDropPinMarkerAndTooltip()
                    placeSaveLayerManager.removeSearchedLocationMarker()
                    placeSaveLayerManager.removeSearchedLocationTooltip()
                    placeSaveDetailFragmentRef.get()?.onClickMapMarkerIcon(feature.id())
                }
            }
        }

        override fun handleOnFreeMapClick(point: Point) {
            currentPin = point
            if (placeSaveDetailFragmentRef.get()?.currentSearchedLocation != null) {
                placeSaveDetailFragmentRef.get()?.onClearSearchLocation()
            }
            PlaceSaveMapStateManager.resetCurrentLocationFocus()
            clearALlCarPark()
            mapProfile.removeALlTooltip()
            placeSaveLayerManager.removeFocusedBookMarkToolTip()
            placeSaveLayerManager.showDropPinMarkerWithTooltip(point)
            Analytics.logClickEvent(
                eventValue = "[map]:tap_map/drop_pin",
                screenName = analyticsScreenName
            )
        }
    }

    private val mapClusterLayerClickListener =
        object : ClusteredMarkerLayerManager.ClusterLayerClickHandler {
            override fun handleOnclick(point: Point, zoomLevel: Number) {
                callBack?.onDismissedCameraTracking()
                mapboxMap.easeTo(
                    CameraOptions.Builder().center(point).zoom(zoomLevel.toDouble()).build()
                )
            }
        }

    /**
     * NOTE:
     * destination icon manager map clicks has more precedence and should be initialized
     * before other amenities managers
     */
    fun setUp(
        pMapView: MapView,
        fragmentPlaceSaved: PlaceSaveDetailFragment,
        nightModeEnabled: Boolean,
        pCallBack: ListenerCallbackLandingMap,
        collectionCode: CollectionCode
    ) {
        callBack = pCallBack
        placeSaveDetailFragmentRef = WeakReference(fragmentPlaceSaved)
        mapView = pMapView
        this.nightModeEnabled = nightModeEnabled
        mapboxMap = pMapView.getMapboxMap()
        viewportDataSource = MapboxNavigationViewportDataSource(
            mapView.getMapboxMap()
        )
        carParkDestinationIconManager =
            CarParkDestinationIconManager(
                fragmentPlaceSaved.requireActivity(),
                mapView
            )

        mapProfile = ClusteredMapProfile(
            MapProfileOptions.Builder(
                mapView,
                fragmentPlaceSaved.requireActivity(),
                carParkDestinationIconManager!!,
                fragmentPlaceSaved.getScreenName()
            ).setMarkerLayerClickHandler(clusterMapClickHandler)
                .setClusterLayerClickHandler(mapClusterLayerClickListener)
                .setNightModeEnabled(nightModeEnabled)
                .setBaseMapCamera(fragmentPlaceSaved.placeMapMapCamera)
                .build()
        )
        mapProfile.addExternalLayerData(
            "${PlaceSaveLayerManager.SEARCHED_LOCATION}${MarkerLayerManager.LAYER_POSTFIX}" to PlaceSaveLayerManager.SEARCHED_LOCATION,
            "${PlaceSaveLayerManager.DROP_PIN}${MarkerLayerManager.LAYER_POSTFIX}" to PlaceSaveLayerManager.DROP_PIN,
            "${BookmarkType.CUSTOM.name}${MarkerLayerManager.LAYER_POSTFIX}" to BookmarkType.CUSTOM.name,
            "${BookmarkType.HOME.name}${MarkerLayerManager.LAYER_POSTFIX}" to BookmarkType.HOME.name,
            "${BookmarkType.WORK.name}${MarkerLayerManager.LAYER_POSTFIX}" to BookmarkType.WORK.name,
            "${BookmarkType.NORMAL.name}${MarkerLayerManager.LAYER_POSTFIX}" to BookmarkType.NORMAL.name,
        )
        mapProfile.addExternalSourceData(
            "${PlaceSaveLayerManager.SEARCHED_LOCATION}${MarkerLayerManager.SOURCE_POSTFIX}" to PlaceSaveLayerManager.SEARCHED_LOCATION,
            "${PlaceSaveLayerManager.DROP_PIN}${MarkerLayerManager.SOURCE_POSTFIX}" to PlaceSaveLayerManager.DROP_PIN,
            "${BookmarkType.CUSTOM.name}${MarkerLayerManager.SOURCE_POSTFIX}" to BookmarkType.CUSTOM.name,
            "${BookmarkType.HOME.name}${MarkerLayerManager.SOURCE_POSTFIX}" to BookmarkType.HOME.name,
            "${BookmarkType.WORK.name}${MarkerLayerManager.SOURCE_POSTFIX}" to BookmarkType.WORK.name,
            "${BookmarkType.NORMAL.name}${MarkerLayerManager.SOURCE_POSTFIX}" to BookmarkType.NORMAL.name,
        )
        notificationMarkerLayerManager = NotificationMarkerViewManager(mapView)
        placeSaveLayerManager = PlaceSaveLayerManager(mapView, collectionCode)
    }

    /**
     * Updates the bottom sheet state, whether in collapsed(0) , mid(1) or full(2)
     * @param stateIndex the state of bottom sheet
     */
    fun updateBottomSheetState(stateIndex: Int, screen: String, onEnd: () -> Unit = {}) {
        App.instance?.let { app ->
            val shareBusinessLogicPackage = app.shareBusinessLogicPackage
            shareBusinessLogicPackage.getRequestsModule().flatMap {
                val data = Arguments.createMap().apply {
                    putInt(Constants.RN_CONSTANTS.BTM_SHEET_STATE_INDEX, stateIndex)
                    putString(Constants.RN_TO_SCREEN, screen)
                }
                it.callRN(ShareBusinessLogicEvent.UPDATE_BOTTOM_SHEET_INDEX.eventName, data)
            }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    onEnd.invoke()
                },
                    {
                        Timber.e("Error occurred while updating bottom sheet state")
                    })
        }
    }


    fun startRoutePlanningFromMarker(ameniti: BaseAmenity, isCarPark: Boolean) {
        val currentLocation = LocationBreezeManager.getInstance().currentLocation ?: return


        CoroutineScope(Dispatchers.IO).launch {
            var destinationDetail: DestinationAddressDetails? = null
            val markerLocation = Location("")
            val sourceAddressDetails = DestinationAddressDetails(
                null,
                Utils.getAddressFromLocation(
                    currentLocation,
                    mapView.context
                ),
                null,
                currentLocation.latitude.toString(),
                currentLocation.longitude.toString(),
                null,
                Constants.TAGS.CURRENT_LOCATION_TAG,
                null,
                -1,
            )
            if (ameniti.lat != null && ameniti.long != null) {
                markerLocation.latitude = ameniti.lat!!
                markerLocation.longitude = ameniti.long!!
                val nameLocation = ameniti.name
                val address =
                    Utils.getAddressObject(
                        markerLocation,
                        mapView.context
                    )
                destinationDetail = if (address != null) {
                    DestinationAddressDetails(
                        null,
                        nameLocation,
                        address.getAddressLine(0),
                        ameniti.lat!!.toString(),
                        ameniti.long!!.toString(),
                        null,
                        nameLocation,
                        null,
                        -1,
                        isCarPark
                    )
                } else {
                    DestinationAddressDetails(
                        null,
                        nameLocation,
                        null,
                        ameniti.lat!!.toString(),
                        ameniti.long!!.toString(),
                        null,
                        nameLocation,
                        null,
                        -1,
                        isCarPark
                    )
                }

                /**
                 * check if carpark set id will use in navigation get alert
                 */
                if (isCarPark) {
                    destinationDetail.carParkID = ameniti.id
                    destinationDetail.availablePercentImageBand = ameniti.availablePercentImageBand
                    destinationDetail.showAvailabilityFB = ameniti.showAvailabilityFB
                    destinationDetail.hasAvailabilityCS = ameniti.hasAvailabilityCS
                    destinationDetail.availabilityCSData = ameniti.availabilityCSData
                }
            }

            withContext(Dispatchers.Main) {
                if (destinationDetail != null) {
                    val args = Bundle()
                    args.putSerializable(
                        Constants.DESTINATION_ADDRESS_DETAILS,
                        destinationDetail
                    )
                    args.putSerializable(Constants.SOURCE_ADDRESS_DETAILS, sourceAddressDetails)
                    placeSaveDetailFragmentRef.get()?.showRouteAndResetToCenter(args)
                }
            }
        }
    }

    fun startRoutePlanningFromPoiMarker(savedPlace: PlaceDetailsResponse) {
        val currentLocation = LocationBreezeManager.getInstance().currentLocation ?: return


        CoroutineScope(Dispatchers.IO).launch {
            val sourceAddressDetails = DestinationAddressDetails(
                easyBreezyAddressID = null,
                address1 = Utils.getAddressFromLocation(currentLocation, mapView.context),
                address2 = null,
                lat = currentLocation.latitude.toString(),
                long = currentLocation.longitude.toString(),
                distance = null,
                destinationName = Constants.TAGS.CURRENT_LOCATION_TAG,
                destinationAddressType = null,
                ranking = -1,
            )
            var destinationDetail: DestinationAddressDetails? = null

            if (savedPlace.lat != null && savedPlace.long != null) {
                val markerLocation = Location("").apply {
                    latitude = savedPlace.lat!!.safeDouble()
                    longitude = savedPlace.long!!.safeDouble()
                }
                val address = Utils.getAddressObject(
                    markerLocation,
                    mapView.context
                )

                destinationDetail = DestinationAddressDetails(
                    easyBreezyAddressID = null,
                    address1 = savedPlace.name,
                    address2 = address?.getAddressLine(0),
                    lat = savedPlace.lat!!.toString(),
                    long = savedPlace.long!!.toString(),
                    distance = null,
                    destinationName = savedPlace.name,
                    destinationAddressType = null,
                    ranking = -1,
                    isCarPark = (savedPlace.amenityType == CARPARK && !savedPlace.amenityId.isNullOrEmpty()),
                    fullAddress = savedPlace.fullAddress,
                    amenityId = savedPlace.amenityId,
                    placeId = savedPlace.placeId,
                    userLocationLinkIdRef = savedPlace.userLocationLinkIdRef,
                    showAvailabilityFB = savedPlace.showAvailabilityFB
                )
                if (destinationDetail.isCarPark) {
                    destinationDetail.carParkID = savedPlace.amenityId
                }
            }

            withContext(Dispatchers.Main) {
                if (destinationDetail != null) {
                    val args = Bundle()
                    args.putSerializable(
                        Constants.DESTINATION_ADDRESS_DETAILS,
                        destinationDetail
                    )
                    args.putSerializable(Constants.SOURCE_ADDRESS_DETAILS, sourceAddressDetails)
                    placeSaveDetailFragmentRef.get()?.showRouteAndResetToCenter(args)
                }
            }
        }
    }

    fun handleCarpark(
        fetchedCarparks: List<BaseAmenity>,
        isMoveCameraWrapperAllCarpark: Boolean = false,
    ) {
        this.isMoveCameraWrapperAllCarpark = isMoveCameraWrapperAllCarpark
        mListCarpark.clear()
        mListCarpark.addAll(fetchedCarparks)
        mapProfile.handleCarPark(
            fetchedCarparks,
            isMoveCameraWrapperAllCarpark,
            false,
            false,
            null
        )
        placeSaveDetailFragmentRef.get()?.zoomToCarparks(fetchedCarparks)
    }

    fun calculateCarParkZoomRadius(carparks: List<BaseAmenity>): Double? {
        if (carparks.isEmpty()) return null
        val selectedLocation = PlaceSaveMapStateManager.activeLocationUse ?: return null
        val currentPoint = Point.fromLngLat(selectedLocation.longitude, selectedLocation.latitude)
        var radius: Double? = null
        carparks.forEach {
            val currentDistance = TurfMeasurement.distance(
                currentPoint, Point.fromLngLat(it.long!!, it.lat!!),
                TurfConstants.UNIT_METERS
            )
            if (currentDistance > (radius ?: 0.0)) {
                radius = currentDistance
            }
        }
        return radius
    }

    private fun showCarParkToolTip(
        amenity: BaseAmenity,
        layerType: String,
        shouldRecenter: Boolean
    ) {
        mapProfile.showCarParkToolTip(
            amenity,
            false,
            false,
            callBack,
            layerType,
            shouldRecenter
        ) { amenity, isCarPark ->
            startRoutePlanningFromMarker(amenity, isCarPark)
        }

        val forceFocusZoom = if (shouldRecenter) {
            null
        } else {
            Constants.POI_TOOLTIP_ZOOM_RADIUS
        }

        PlaceSaveMapStateManager.setCurrentLocationFocus(
            PlaceSaveMapStateManager.AMENITY_INSTANCE,
            amenity.lat!!,
            amenity.long!!,
            forceFocusZoom
        )
        selectedBookMarkParkingPoint = Point.fromLngLat(amenity.long!!, amenity.lat!!)
    }


    fun clearALlCarPark() {
        mapProfile.clearALlCarPark()
        PlaceSaveMapStateManager.resetCurrentLocationFocus()
    }

    fun resetAllViews() {
        mapProfile.removeALlTooltip()
        notificationMarkerLayerManager!!.removeAllMarkers()
        placeSaveLayerManager.removeAllMarkers()
        placeSaveLayerManager.removeSearchedLocationTooltip()
        placeSaveLayerManager.removeSearchedLocationMarker()
        selectedBookMarkParkingPoint = null
    }

    fun showSearchMarkerWithTooltip(searchedLocation: SearchLocation) {
        clearALlCarPark()
        placeSaveLayerManager.showSearchLocationMarkerWithTooltip(searchedLocation)
    }

    companion object {
        const val PREVIEW_BOOKMARK_ID = "-1"
    }

}
package com.ncs.breeze.di.module.fragment

import com.ncs.breeze.ui.dashboard.fragments.placesave.PlaceSaveDetailFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class PlaceSaveModule {
    @ContributesAndroidInjector
    abstract fun contributePlaceSaved(): PlaceSaveDetailFragment
}
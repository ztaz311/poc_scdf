package com.ncs.breeze.car.breeze.screen.travel_time

import androidx.activity.OnBackPressedCallback
import androidx.car.app.model.Action
import androidx.car.app.model.CarIcon
import androidx.car.app.model.ItemList
import androidx.car.app.model.ListTemplate
import androidx.car.app.model.Row
import androidx.car.app.model.Row.IMAGE_TYPE_LARGE
import androidx.car.app.model.Template
import androidx.core.graphics.drawable.IconCompat
import androidx.lifecycle.lifecycleScope
import com.ncs.breeze.R
import com.ncs.breeze.car.breeze.MainBreezeCarContext
import com.ncs.breeze.car.breeze.base.BaseScreenCar
import com.ncs.breeze.car.breeze.utils.LimitClick
import com.ncs.breeze.car.breeze.utils.setClickSafe
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.Screen
import com.ncs.breeze.common.model.rx.ToCarEventData
import com.breeze.model.obu.OBUTravelTimeData
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class CarTravelTimeScreen(
    val mainCarContext: MainBreezeCarContext,
    private val congestionAlertModel: Array<OBUTravelTimeData>?
) : BaseScreenCar(mainCarContext.carContext) {

    lateinit var roadStatusIndicatorRed: CarIcon
    lateinit var roadStatusIndicatorYellow: CarIcon
    lateinit var roadStatusIndicatorGreen: CarIcon

    private val backPressedCallback = object : OnBackPressedCallback(
        true // default to enabled
    ) {
        override fun handleOnBackPressed() {
            Analytics.logClickEvent("[dhu],[travel_time]:back", Screen.AA_DHU_NAVIGATION)
            screenManager.pop()
        }
    }

    override fun onGetTemplate(): Template {
        val listTemplateBuilder = ListTemplate.Builder()
            .setLoading(false)
            .setHeaderAction(Action.BACK)
            .setTitle(carContext.getString(R.string.travel_time_alert_estimate_time_to))
        listTemplateBuilder.setSingleList(generateItemList())
        return listTemplateBuilder.build()
    }

    override fun onCreateScreen() {
        super.onCreateScreen()
        carContext.onBackPressedDispatcher.addCallback({ lifecycle }, backPressedCallback)
        roadStatusIndicatorRed = CarIcon.Builder(
            IconCompat.createWithResource(
                carContext,
                R.drawable.ic_road_status_red
            )
        ).build()
        roadStatusIndicatorYellow = CarIcon.Builder(
            IconCompat.createWithResource(
                carContext,
                R.drawable.ic_road_status_yellow
            )
        ).build()
        roadStatusIndicatorGreen = CarIcon.Builder(
            IconCompat.createWithResource(
                carContext,
                R.drawable.ic_road_status_green
            )
        ).build()
        startAutoGoBackCountdown()
    }

    private fun startAutoGoBackCountdown() {
        lifecycleScope.launch {
            delay(15000)
            screenManager.pop()
        }
    }
    override fun getScreenName(): String {
        return Screen.AA_CONGESTION_ALERT_SCREEN
    }

    override fun onScreenUpdateEvent(data: ToCarEventData?) {
        super.onScreenUpdateEvent(data)
        invalidate()
    }

    private fun generateItemList(): ItemList {
        val listBuilder = ItemList.Builder()

        if (congestionAlertModel.isNullOrEmpty()) {
            listBuilder.setNoItemsMessage(carContext.getString(R.string.empty_list))
        } else {
            congestionAlertModel.forEach { item ->
                val roadStatusIcon: CarIcon = when(item.color) {
                    "red" -> {
                        roadStatusIndicatorRed
                    }
                    "yellow" -> {
                        roadStatusIndicatorYellow
                    }
                    else -> {
                        roadStatusIndicatorGreen
                    }
                }
                val estTime = if (item.estTime.contains("min")) item.estTime else
                    carContext.getString(R.string.travel_time_alert_minute_unit, item.estTime)
                val row = Row.Builder()
                    .setTitle(item.name)
                    .setBrowsable(false)
                    .setImage(roadStatusIcon, IMAGE_TYPE_LARGE)
                    .addText(estTime)
                    .setClickSafe {
                        LimitClick.handleSafe {
                            finish()
                        }
                    }
                listBuilder.addItem(row.build())
            }
        }

        return listBuilder.build()
    }

}

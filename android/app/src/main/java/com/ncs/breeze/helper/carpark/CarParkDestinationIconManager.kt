package com.ncs.breeze.helper.carpark

import android.app.Activity
import com.ncs.breeze.R
import com.ncs.breeze.common.analytics.Analytics
import com.breeze.model.extensions.toBitmap
import com.breeze.model.SelectedAmenity
import com.breeze.model.api.response.amenities.BaseAmenity
import com.breeze.model.constants.Constants
import com.mapbox.geojson.LineString
import com.mapbox.geojson.Point
import com.mapbox.maps.LayerPosition
import com.mapbox.maps.MapView
import com.mapbox.maps.extension.style.layers.addLayerBelow
import com.mapbox.maps.extension.style.layers.generated.LineLayer
import com.mapbox.maps.extension.style.layers.generated.SymbolLayer
import com.mapbox.maps.extension.style.layers.generated.symbolLayer
import com.mapbox.maps.extension.style.layers.getLayerAs
import com.mapbox.maps.extension.style.layers.properties.generated.LineCap
import com.mapbox.maps.extension.style.layers.properties.generated.LineJoin
import com.mapbox.maps.extension.style.layers.properties.generated.Visibility
import com.mapbox.maps.extension.style.sources.addSource
import com.mapbox.maps.extension.style.sources.generated.geoJsonSource
import com.ncs.breeze.helper.DestinationIconManager
import timber.log.Timber

class CarParkDestinationIconManager(
    activity: Activity,
    mapView: MapView,
    isMapClickEnabled: Boolean = false
) :
    DestinationIconManager(
        activity = activity,
        mapView = mapView,
        isMapClickEnabled = isMapClickEnabled
    ) {


    override fun showDestinationIcon() {
        super.showDestinationIcon()
        removeDestinationConnector()
        mapView.getMapboxMap().getStyle()?.removeStyleLayer("$CUSTOM$destinationLayerId")
    }

    override fun removeDestinationIcon() {
        mapView.getMapboxMap().getStyle()?.removeStyleLayer("$CUSTOM$destinationLayerId")
        removeDestinationConnector()
        super.removeDestinationIcon()
    }

    fun isDestinationTooltipOpen(): Boolean? {
        return isDestinationMarkerViewDisplayed
    }

    override fun refreshBookmarkIconTooltip(saved: Boolean, bookmarkId: Int) {
        super.refreshBookmarkIconTooltip(saved, bookmarkId)
    }

    fun handleDestinationCarParkIcon(
        carParkList: List<BaseAmenity>,
        showAlternateDestination: Boolean
    ) {
        val destinationAmenity = carParkList.find {
            it.destinationCarPark == true
        }
        if (destinationAmenity == null || !showAlternateDestination) {
            showDestinationIcon()
        } else {
            hideDestinationIcon()
        }
    }

    fun handleSelectedDestinationCarParkIcon(
        carParkList: List<SelectedAmenity>,
        showAlternateDestination: Boolean
    ) {
        val destinationAmenity = carParkList?.find {
            it.selected && it.baseAmenity.destinationCarPark == true
        }
        if (destinationAmenity == null || !showAlternateDestination) {
            showDestinationIcon()
        } else {
            hideDestinationIcon()
        }
    }


    fun replaceSelectedDestinationCarParkIcon(
        carParkList: List<SelectedAmenity>,
        res: Int, shouldDrawLineConnector: Boolean
    ) {
        val destinationAmenity = carParkList?.find {
            it.selected && it.baseAmenity.destinationCarPark == true
        }
        if (destinationAmenity == null) {
            showDestinationIcon()
        } else {
            hideDestinationIcon()
            showCustomDestinationIcon(res)
            mapView.getMapboxMap().getStyle()
                ?.takeIf { it.getLayerAs<SymbolLayer>(destinationLayerId) != null }
                ?.let { style ->
                    style.moveStyleLayer(
                        destinationLayerId,
                        LayerPosition(null, Constants.COUNTRY_LABEL, null)
                    )
                    validateAndDrawLineConnector(shouldDrawLineConnector, destinationAmenity)
                }
        }
    }

    private fun validateAndDrawLineConnector(
        shouldDrawLineConnector: Boolean,
        destinationAmenity: SelectedAmenity
    ) {
        if (shouldDrawLineConnector) {
            if (selectedDestinationPoint != null
                && destinationAmenity.baseAmenity.long != null &&
                destinationAmenity.baseAmenity.lat != null
            ) {
                selectedDestinationPoint?.let {
                    val destinationAmenityPoint = Point.fromLngLat(
                        destinationAmenity.baseAmenity.long!!,
                        destinationAmenity.baseAmenity.lat!!
                    )
                    drawDestinationConnector(listOf(it, destinationAmenityPoint))
                }
            }
        }
    }

    private fun drawDestinationConnector(
        points: List<Point>,
    ) {
        kotlin.runCatching {
            removeDestinationConnector()
            val geometry = LineString.fromLngLats(points)
            mapView.getMapboxMap().getStyle()?.addSource(
                geoJsonSource(DESTINATION_CONNECTOR_SOURCE_ID) {
                    geometry(geometry)
                }
            )
            val lineLayer =
                LineLayer(DESTINATION_CONNECTOR_LAYER_ID, DESTINATION_CONNECTOR_SOURCE_ID)
            val density = activity.resources.displayMetrics.density
            lineLayer.apply {
                lineDasharray(listOf(1.0, 1.6))
                lineWidth(density * 1.0)
                lineCap(LineCap.SQUARE)
                lineJoin(LineJoin.ROUND)
                lineColor(activity.getColor(R.color.themed_breeze_primary))
            }
            mapView.getMapboxMap().getStyle()?.addLayerBelow(
                lineLayer, Constants.COUNTRY_LABEL
            )

        }.onFailure {
            Timber.e(it, "Error occurred drawing line layer")
            Analytics.logExceptionEvent("Error draw destination connector: ${it.message}")
        }
    }

    private fun removeDestinationConnector(
    ) {
        mapView.getMapboxMap().getStyle()?.let { style ->
            style.removeStyleLayer(DESTINATION_CONNECTOR_LAYER_ID)
            style.removeStyleSource(DESTINATION_CONNECTOR_SOURCE_ID)
        }
    }

    private fun showCustomDestinationIcon(res: Int) {
        mapView.getMapboxMap().getStyle()?.let { style ->
            var iconToAdd = "$CUSTOM$destinationImageId"
            res.toBitmap(activity)?.let {
                style.addImage(
                    iconToAdd,
                    it,
                    false
                )
            }
            style.removeStyleLayer("$CUSTOM$destinationLayerId")
            style.addLayerBelow(
                symbolLayer("$CUSTOM$destinationLayerId", destinationSourceId) {
                    iconImage(
                        iconToAdd
                    )
                    iconAllowOverlap(true)
                    iconIgnorePlacement(true)
                    iconSize(1.0)
                    visibility(Visibility.VISIBLE)
                }, Constants.COUNTRY_LABEL
            )
        }
    }


    companion object {
        const val DESTINATION_CONNECTOR_SOURCE_ID = "breeze-destination-connector-source"
        const val DESTINATION_CONNECTOR_LAYER_ID = "breeze-destination-connector-layer"
        private const val CUSTOM = "custom"
    }

}
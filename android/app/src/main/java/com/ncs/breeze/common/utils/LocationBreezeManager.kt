package com.ncs.breeze.common.utils

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.os.HandlerThread
import androidx.core.app.ActivityCompat
import com.breeze.model.constants.Constants
import com.facebook.react.bridge.Arguments
import com.facebook.react.bridge.Callback
import com.mapbox.android.core.location.*
import com.mapbox.navigation.core.lifecycle.MapboxNavigationApp
import com.mapbox.navigation.core.trip.session.LocationMatcherResult
import com.mapbox.navigation.core.trip.session.LocationObserver
import com.mapbox.navigation.core.trip.session.TripSessionState
import com.mapbox.navigation.core.trip.session.TripSessionStateObserver
import com.ncs.breeze.App
import com.ncs.breeze.common.storage.BreezeUserPreference
import timber.log.Timber
import java.util.concurrent.CopyOnWriteArraySet

class LocationBreezeManager private constructor() {

    private val locationEngineCallback = MyLocationEngineCallback()
    private var locationEngine: LocationEngine? = null
    var currentLocation: Location? = null

    private var currentSessionState: TripSessionState = TripSessionState.STOPPED
    private val isTripStarted: Boolean
        get() = currentSessionState == TripSessionState.STARTED


    lateinit var locationHandlerThread: HandlerThread

    interface CallBackLocationBreezeManager {
        fun onNewLocationMatcherResult(locationMatcherResult: LocationMatcherResult) {}
        fun onSuccess(result: LocationEngineResult) {}
        fun onFailure(exception: Exception) {}
        fun onNewRawLocation(rawLocation: Location) {}
    }

    private val callbacks = CopyOnWriteArraySet<CallBackLocationBreezeManager>()

    /**
     * setup voice instruction
     */
    fun setUp(context: Context) {
        locationHandlerThread = HandlerThread(LOCATION_ENGINE_THREAD_NAME)
        locationHandlerThread.start()
        locationEngine = LocationEngineProvider.getBestLocationEngine(context)
        currentLocation =
            BreezeUserPreference.getInstance(context.applicationContext).getLastLocation()
    }

    private val locationObserver = object : LocationObserver {
        override fun onNewLocationMatcherResult(locationMatcherResult: LocationMatcherResult) {
            currentLocation = locationMatcherResult.enhancedLocation

            callbacks.forEach { callback ->
                callback.onNewLocationMatcherResult(locationMatcherResult)
                callback.onSuccess(LocationEngineResult.create(currentLocation))
            }
        }

        override fun onNewRawLocation(rawLocation: Location) {
//            Timber.d("LocationBreezeManager: rawLocation=[${rawLocation}]")

        }
    }
    private val tripSessionStateObserver =
        TripSessionStateObserver { sessionState ->
            currentSessionState = sessionState
            when (sessionState) {
                TripSessionState.STARTED -> {
                    Timber.d("LocationBreezeManager - TripSessionState.STARTED")
                    MapboxNavigationApp.current()?.registerLocationObserver(locationObserver)
                    unregisterLocationEngineRequest()
                }

                TripSessionState.STOPPED -> {
                    Timber.d("LocationBreezeManager - TripSessionState.STOPPED")
                    MapboxNavigationApp.current()?.unregisterLocationObserver(locationObserver)
                    registerLocationEngineRequest()
                }
            }
        }

    fun startLocationUpdates() {
        MapboxNavigationApp.current()?.run {
            registerTripSessionStateObserver(tripSessionStateObserver)
        }
    }


    private fun registerLocationEngineRequest() {
        App.instance?.let {
            if (ActivityCompat.checkSelfPermission(
                    it,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(
                    it,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                return
            }
            val request =
                LocationEngineRequest.Builder(Constants.LOCATION.DEFAULT_INTERVAL_IN_MILLISECONDS)
                    .setFastestInterval(Constants.LOCATION.DEFAULT_FASTEST_INTERVAL_IN_MILLISECONDS)
                    .setPriority(LocationEngineRequest.PRIORITY_BALANCED_POWER_ACCURACY)
                    .build()
            locationEngine?.requestLocationUpdates(
                request,
                locationEngineCallback,
                locationHandlerThread.looper
            )
            locationEngine?.getLastLocation(locationEngineCallback)
        }
    }

    private fun unregisterLocationEngineRequest() {
        locationEngine?.removeLocationUpdates(locationEngineCallback)
    }

    fun registerLocationCallback(observer: CallBackLocationBreezeManager) {
        callbacks.add(observer)
    }


    fun removeLocationCallBack(observer: CallBackLocationBreezeManager) {
        try {
            callbacks.remove(observer)
        } catch (e: Exception) {
        }
    }

    fun removeAllLocationCallback() {
        callbacks.clear()
    }

    fun getUserCurrentLocation(callback: Callback) {
        currentLocation?.let { loc ->
            callback.invoke(Arguments.createMap().apply {
                putString("lat", "${loc.latitude}")
                putString("long", "${loc.longitude}")
            })
        }
    }

    fun removeLocationUpdates() {
        try {
            locationEngine?.removeLocationUpdates(locationEngineCallback)
        } catch (e: Exception) {
        }
    }

    private object Holder {
        val INSTANCE = LocationBreezeManager()
    }

    companion object {
        const val LOCATION_ENGINE_THREAD_NAME = "LocationEngineHandlerThread"

        @JvmStatic
        fun getInstance(): LocationBreezeManager {
            return Holder.INSTANCE
        }
    }


    inner class MyLocationEngineCallback : LocationEngineCallback<LocationEngineResult> {
        override fun onSuccess(p0: LocationEngineResult?) {
            if (p0?.lastLocation != null) {
                currentLocation = p0.lastLocation
            }
            p0?.let {
                callbacks.forEach { callback ->
                    callback.onSuccess(it)
                }
            }

        }

        override fun onFailure(p0: Exception) {
            callbacks.forEach {
                it.onFailure(p0)
            }
        }

    }

}

fun createNewLocationEngineRequest(): LocationEngineRequest = LocationEngineRequest
    .Builder(3000L)
    .setFastestInterval(1000)
    .setPriority(LocationEngineRequest.PRIORITY_HIGH_ACCURACY)
    .build()
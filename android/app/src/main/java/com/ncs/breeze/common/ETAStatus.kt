package com.ncs.breeze.common

import com.breeze.model.constants.Constants


enum class ETAStatus(val type: String) {
    INPROGRESS(Constants.TRIPETA.LIVE_LOCATION_INPROGRESS),
    PAUSE(Constants.TRIPETA.LIVE_LOCATION_PAUSE),
    COMPLETE(Constants.TRIPETA.LIVE_LOCATION_COMPLETE),
    CANCEL(Constants.TRIPETA.LIVE_LOCATION_CANCEL)
}
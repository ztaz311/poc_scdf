package com.ncs.breeze

import android.app.Activity
import android.app.Application
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.location.LocationManager
import android.view.WindowManager
import android.webkit.CookieManager
import androidx.appcompat.app.AppCompatActivity
import androidx.car.app.connection.CarConnection
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.lifecycle.LifecycleObserver
import androidx.multidex.MultiDex
import androidx.work.ListenableWorker
import com.breeze.model.ObjectFlowLoginAndSignUp
import com.breeze.model.SearchLocation
import com.breeze.model.enums.UserTheme
import com.breeze.model.firebase.NavigationLogData
import com.breeze.model.obu.OBURoadEventData
import com.facebook.react.PackageList
import com.facebook.react.ReactApplication
import com.facebook.react.ReactNativeHost
import com.facebook.react.ReactPackage
import com.facebook.react.bridge.Arguments
import com.facebook.react.bridge.JSIModulePackage
import com.facebook.soloader.SoLoader
import com.instabug.bug.BugReporting
import com.instabug.bug.invocation.Option
import com.instabug.library.Instabug
import com.instabug.library.InstabugCustomTextPlaceHolder
import com.instabug.library.invocation.InstabugInvocationEvent
import com.instabug.library.ui.onboarding.WelcomeMessage
import com.mapbox.navigation.base.ExperimentalPreviewMapboxNavigationAPI
import com.mapbox.navigation.base.options.DeviceProfile
import com.mapbox.navigation.base.options.EHorizonOptions
import com.mapbox.navigation.base.options.HistoryRecorderOptions
import com.mapbox.navigation.base.options.NavigationOptions
import com.mapbox.navigation.core.MapboxNavigation
import com.mapbox.navigation.core.lifecycle.MapboxNavigationApp
import com.mapbox.navigation.core.lifecycle.MapboxNavigationObserver
import com.mapbox.navigation.core.trip.session.TripSessionState
import com.ncs.breeze.car.breeze.utils.BreezeCarUtil
import com.ncs.breeze.car.breeze.utils.BreezeUserPreferencesUtil
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.Event
import com.ncs.breeze.common.analytics.Param
import com.ncs.breeze.common.analytics.Screen
import com.ncs.breeze.common.appSync.AppSyncHelper
import com.ncs.breeze.common.constant.OBUConstants
import com.ncs.breeze.common.event.ToAppRx
import com.ncs.breeze.common.event.ToCarRx
import com.ncs.breeze.common.extensions.android.getBreezeUserPreference
import com.ncs.breeze.common.extensions.android.getUserDataPreference
import com.ncs.breeze.common.helper.network.NetworkConnection
import com.ncs.breeze.common.helper.obu.OBUConnectionHelper
import com.ncs.breeze.common.helper.obu.OBUDataHelper
import com.ncs.breeze.common.helper.obu.OBUEventGlobalHandler
import com.ncs.breeze.common.helper.route.RoutePlanningHelper
import com.ncs.breeze.common.model.rx.AddToRecentList
import com.ncs.breeze.common.model.rx.AppToPhoneReportIssueEvent
import com.ncs.breeze.common.model.rx.AppToRecentSearchScreenEvent
import com.ncs.breeze.common.model.rx.AppToSearchLocationEvent
import com.ncs.breeze.common.model.rx.AppToSearchLocationLatLngGoogleEvent
import com.ncs.breeze.common.model.rx.AppToShortcutListScreenEvent
import com.ncs.breeze.common.model.rx.CarGetListRecentSearch
import com.ncs.breeze.common.model.rx.CarRecentSearchData
import com.ncs.breeze.common.model.rx.CarShortcutsListData
import com.ncs.breeze.common.model.rx.DataTransferLatLngGoogle
import com.ncs.breeze.common.model.rx.DataTransferToLocationList
import com.ncs.breeze.common.model.rx.GetLatLngGoogleIfNeed
import com.ncs.breeze.common.model.rx.GetSearchLocationResult
import com.ncs.breeze.common.model.rx.GetShortcutListFromCar
import com.ncs.breeze.common.model.rx.SaveOnboardingState
import com.ncs.breeze.common.model.rx.ToAppRxEvent
import com.ncs.breeze.common.receivers.LocationEnabledReceiver
import com.ncs.breeze.common.remote.ApiHelper
import com.ncs.breeze.common.remote.MyServiceInterceptor
import com.ncs.breeze.common.rxbus.RxBus
import com.ncs.breeze.common.rxbus.RxEvent
import com.ncs.breeze.common.utils.AWSUtils
import com.ncs.breeze.common.utils.ArrayUtil
import com.ncs.breeze.common.utils.BreezeEHorizonProvider
import com.ncs.breeze.common.utils.BreezeERPRefreshUtil
import com.ncs.breeze.common.utils.BreezeFeatureDetectionUtil
import com.ncs.breeze.common.utils.BreezeFileLoggingUtil
import com.ncs.breeze.common.utils.BreezeInstabugUtil
import com.ncs.breeze.common.utils.BreezeMiscRoadObjectUtil
import com.ncs.breeze.common.utils.BreezeRouteSortingUtil
import com.ncs.breeze.common.utils.BreezeSchoolSilverZoneUpdateUtil
import com.ncs.breeze.common.utils.BreezeStatisticsUtil
import com.ncs.breeze.common.utils.BreezeTrafficIncidentsUtil
import com.ncs.breeze.common.utils.BreezeTripLoggingUtil
import com.ncs.breeze.common.utils.BreezeUserPreferenceUtil
import com.ncs.breeze.common.utils.InitObjectUtilsController
import com.ncs.breeze.common.utils.InitObjectUtilsController.mRoutePlanningHelper
import com.ncs.breeze.common.utils.LocationBreezeManager
import com.ncs.breeze.common.utils.MapUtil
import com.ncs.breeze.common.utils.NetworkMonitor
import com.ncs.breeze.common.utils.OBUStripStateManager
import com.ncs.breeze.common.utils.Utils
import com.ncs.breeze.common.utils.VoiceInstructionsManager
import com.ncs.breeze.common.utils.createNewLocationEngineRequest
import com.ncs.breeze.common.utils.eta.ETAEngine
import com.ncs.breeze.common.utils.notification.PushNotificationUtils
import com.ncs.breeze.components.GlobalToast
import com.ncs.breeze.components.NavigationLogUploaderScheduler
import com.ncs.breeze.di.AppInjector
import com.ncs.breeze.di.HasListenableWorkerInjector
import com.ncs.breeze.reactnative.nativemodule.CommunicateWithNativePackage
import com.ncs.breeze.reactnative.nativemodule.RNAuthPackage
import com.ncs.breeze.reactnative.nativemodule.ShareBusinessLogicEvent
import com.ncs.breeze.reactnative.nativemodule.ShareBusinessLogicHelper
import com.ncs.breeze.reactnative.nativemodule.ShareBusinessLogicPackage
import com.ncs.breeze.ui.base.BaseActivity
import com.ncs.breeze.ui.base.RemoteConfigUtils
import com.ncs.breeze.ui.dashboard.activity.DashboardActivity
import com.ncs.breeze.ui.navigation.TurnByTurnNavigationActivity
import com.swmansion.reanimated.ReanimatedJSIModulePackage
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.lang.ref.WeakReference
import javax.inject.Inject


class App : Application(), HasAndroidInjector, HasListenableWorkerInjector, LifecycleObserver,
    NetworkConnection.ConnectivityReceiverListener,
    LocationEnabledReceiver.LocationReceiverListener, ReactApplication {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Any>

    @Inject
    lateinit var workerInjector: DispatchingAndroidInjector<ListenableWorker>

    // We add this public property so we can access it throughout the application.
    val shareBusinessLogicPackage = ShareBusinessLogicPackage()

    private var currentTopActivityRef: WeakReference<Activity> = WeakReference(null)
    private var isOnSaveInstanceStateCalled: Boolean = false

    @Inject
    lateinit var userPreferenceUtil: BreezeUserPreferenceUtil

    @Inject
    lateinit var myServiceInterceptor: MyServiceInterceptor

    @Inject
    lateinit var breezeFileLoggingUtil: BreezeFileLoggingUtil


    @Inject
    lateinit var breezeRouteSortingUtil: BreezeRouteSortingUtil


    @Inject
    lateinit var breezeERPUtil: BreezeERPRefreshUtil

    @Inject
    lateinit var apiHelper: ApiHelper


    @Inject
    lateinit var breezeIncidentsUtil: BreezeTrafficIncidentsUtil

    @Inject
    lateinit var breezeMiscRoadObjectUtil: BreezeMiscRoadObjectUtil

    @Inject
    lateinit var breezeInstabugUtil: BreezeInstabugUtil

    @Inject
    lateinit var breezeTripLoggingUtil: BreezeTripLoggingUtil

    @Inject
    lateinit var breezeStatisticsUtil: BreezeStatisticsUtil

    @Inject
    lateinit var breezeFeatureDetectionUtil: BreezeFeatureDetectionUtil

    @Inject
    lateinit var breezeSchoolSilverZoneUpdateUtil: BreezeSchoolSilverZoneUpdateUtil

    private var carConnection: CarConnection? = null
    private var currentCarConnectionState: Int = CarConnection.CONNECTION_TYPE_NOT_CONNECTED

    fun getCurrentActivity() = currentTopActivityRef.get()
    fun setCurrentActivity(activity: Activity?) {
        currentTopActivityRef = WeakReference(activity)
    }

    fun setBoolActivityOnSaveInstanceState(isSaveInstanceStateCalled: Boolean) {
        this.isOnSaveInstanceStateCalled = isSaveInstanceStateCalled
    }

    var compositeDisposable: CompositeDisposable = CompositeDisposable()

    val shareBusinessLogicHelper by lazy { ShareBusinessLogicHelper(this) }
    val obuConnectionHelper by lazy {
        OBUConnectionHelper.isOBUSimulationEnabled =
            getBreezeUserPreference().isEnableOBUSimulation()
        OBUConnectionHelper(this)
    }
    private lateinit var obuEventGlobalHandler: OBUEventGlobalHandler

    val appLifeCycleHandler by lazy { AppLifeCycleHandler(this) }
    lateinit var navigationLogUploaderScheduler: NavigationLogUploaderScheduler

    var isCarSessionAlive = false
        set(value) {
            if (!field && value) {
                onForegroundStateChanged(true)
            }
            field = value
            checkIsAppAndCarSessionIsDestroyed()
        }

    var isAutoDriveSimulation = false

    fun checkIsAppAndCarSessionIsDestroyed() {
        Timber.e("checkIsAppAndCarSessionIsDestroyed: $isCarSessionAlive ${appLifeCycleHandler.isAllActivitiesDestroyed}")
        if (!isCarSessionAlive && appLifeCycleHandler.isAllActivitiesDestroyed) {
            Analytics.logEvent(
                Event.OBU_SYSTEM_EVENT, Event.OBU_SYSTEM_APP_TERMINATED, Screen.OBU_APP
            )
            obuConnectionHelper.disconnectOBU()
            OBUStripStateManager.destroyInstance()
        }
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        setNotAllowCookies()
        carConnection = CarConnection(this)
        carConnection?.type?.observeForever {
            currentCarConnectionState = it
        }
        navigationLogUploaderScheduler = NavigationLogUploaderScheduler(this)
        initLogger()
        listenRxEvents()
        initMapboxNavigation()
        AppInjector.init(this)
        RemoteConfigUtils.init()

        InitObjectUtilsController.currentObjectFlowLoginSignUp = ObjectFlowLoginAndSignUp()

//        if (com.ncs.breeze.BuildConfig.DEBUG)
//            StrictMode.setVmPolicy(
//                StrictMode.VmPolicy.Builder()
//                    //.detectUnsafeIntentLaunch()
//                    .detectActivityLeaks()
//                    .penaltyLog()
//                    .penaltyDeath()
//                    .build()
//            )

        mRoutePlanningHelper = RoutePlanningHelper(
            breezeRouteSortingUtil, breezeERPUtil, getString(R.string.mapbox_access_token)
        )
        BreezeCarUtil.initBreezeUtils(
            breezeIncidentsUtil,
            breezeERPUtil,
            apiHelper,
            breezeTripLoggingUtil,
            breezeStatisticsUtil,
            breezeFeatureDetectionUtil,
            breezeSchoolSilverZoneUpdateUtil,
            breezeMiscRoadObjectUtil,
            userPreferenceUtil
        )
        BreezeUserPreferencesUtil.initUtil(breezeUserPreferenceUtil = userPreferenceUtil)

        NetworkMonitor(this).startNetworkCallback()

//        ProcessLifecycleOwner.get().lifecycle.addObserver(this)

        registerLifecycleHandler(appLifeCycleHandler)
        initSecondaryFirebase()
        setupDebugLog()
        initReactNative()
        setupAppTheme()

        AWSUtils.init(this)
        registerNetworkConnectionListener()
        registerLocationListener()
        Analytics.initialize()
        setupInstabug()

        initOBU(false)
        obuEventGlobalHandler = OBUEventGlobalHandler(this)
        Analytics.logEvent(Event.OBU_SYSTEM_EVENT, Event.OBU_SYSTEM_APP_START, Screen.OBU_APP)

    }

    private fun setNotAllowCookies() {
        val cookieManager: CookieManager = CookieManager.getInstance()
        cookieManager.removeAllCookies {
            Timber.d("cookieManager.removeAllCookies: $it")
        }
        cookieManager.setAcceptCookie(false)
    }

    private fun listenRxEvents() {
        compositeDisposable.add(carEventListener)
        compositeDisposable.add(RxBus.listen(RxEvent.NavigationLogUploadEvent::class.java)
            .observeOn(Schedulers.io()).subscribeOn(Schedulers.io()).subscribe {
                uploadNavigationLog(it.data.apply {
                    userId =
                        userPreferenceUtil.getUserStoredData()?.cognitoID?.takeIf { id -> id.isNotEmpty() }
                            ?: "unknown_user"
                })
            })
    }

    private fun uploadNavigationLog(data: NavigationLogData) {
        navigationLogUploaderScheduler.scheduleNavigationLogUpload(data)
    }

    fun isConnectedToCar(): Boolean {
        return currentCarConnectionState == CarConnection.CONNECTION_TYPE_PROJECTION
    }

    private fun initLogger() {
        if (BuildConfig.DEBUG) {
            Timber.plant(object : Timber.DebugTree() {
                override fun createStackElementTag(element: StackTraceElement): String? {
                    return "(${element.fileName}:${element.lineNumber})#${element.methodName}"
                }
            })
        }
    }

    fun setupDebugLog() {
        breezeFileLoggingUtil.enableLog("LogUpload_OnLaunch")
//        breezeFileLoggingUtil.plantTimberDebugLog()
    }

    /**
     * Initialize Mapbox's Firestore to upload navigation logs to
     * For debugging purposes
     * (Not use for now)
     * */
    private fun initSecondaryFirebase() {
//        Firebase.initialize(
//            this,
//            FirebaseOptions.Builder()
//                .setProjectId("navigation-trips-trace")
//                .setApplicationId(BuildConfig.MAPBOX_FIREBASE_APPLICATION_ID)
//                .setApiKey(BuildConfig.MAPBOX_FIREBASE_INTEGRATION_API_KEY)
//                .setStorageBucket("navigation-trips-trace.appspot.com")
//                .build(),
//            ServiceLocator.MAPBOX_INTEGRATION_FIREBASE_KEY
//        )
    }

    private var obuInitAnalyticSent = false
    fun initOBU(startListenForegroundState: Boolean = true) {
        obuConnectionHelper.initOBU {
            onSuccess = {
                if (!obuInitAnalyticSent) {
                    obuInitAnalyticSent = true
                    Analytics.logEvent(
                        Event.OBU_SYSTEM_EVENT,
                        bundleOf(Param.KEY_EVENT_VALUE to Event.OBU_SDK_INIT_SUCCESS)
                    )
                }
                if (getBreezeUserPreference().isEnableOBUSimulation()) {
                    obuConnectionHelper.enableOBUSimulation()
                }
                val isAutoConnect = getUserDataPreference()?.isEnableOBUAutoConnect() ?: false
                obuConnectionHelper.enableAutoConnect(isAutoConnect)
                shareBusinessLogicHelper.initOBUAutoConnect(isAutoConnect)

                obuConnectionHelper.startSyncOBUConnectionToRN()
                if (startListenForegroundState) appLifeCycleHandler.addForegroundStateCallback(::onForegroundStateChanged)
            }
        }
    }

    private fun initMapboxNavigation() {
        val navigationOptions = NavigationOptions.Builder(applicationContext)
            //.locationEngine(locationEngine!!)
            .accessToken(getString(R.string.mapbox_access_token)).eHorizonOptions(
                EHorizonOptions.Builder().length(1500.0).build()
            )
            // Enable history
            .historyRecorderOptions(
                HistoryRecorderOptions.Builder().build()
            )
            // Use JSON format
            .deviceProfile(
                DeviceProfile.Builder().customConfig("{\"features\":{\"usePbf\": true}}").build()
            ).locationEngineRequest(
                createNewLocationEngineRequest()
            ).build()

        MapboxNavigationApp.setup(navigationOptions).attachAllActivities(this)
            .registerObserver(object : MapboxNavigationObserver {
                override fun onAttached(mapboxNavigation: MapboxNavigation) {
                    Timber.d("onAttached MapboxNavigation: $mapboxNavigation")
                }

                override fun onDetached(mapboxNavigation: MapboxNavigation) {
                    Timber.d("onDetached: MapboxNavigation $mapboxNavigation")
                    destroyNavigationObservers()
                }

            })

        LocationBreezeManager.getInstance().setUp(this)
        isAutoDriveSimulation = getBreezeUserPreference().isEnableAutoDriveSimulation()
    }

    private fun initReactNative() {
        SoLoader.init(this,  /* native exopackage */false)
        // This will initialize React Native so it can be used without any React Native components
        reactNativeHost.reactInstanceManager.createReactContextInBackground()
    }

    private fun registerLocationListener() {
        LocationEnabledReceiver.locationReceiverListener = this
        val filter = IntentFilter().apply {
            addAction(LocationManager.PROVIDERS_CHANGED_ACTION)
            addAction(Intent.ACTION_PROVIDER_CHANGED)
        }
        registerReceiver(
            LocationEnabledReceiver(), filter
        )
    }

    private var networkConnection: NetworkConnection? = null
    private fun registerNetworkConnectionListener() {
        networkConnection = NetworkConnection(this)
        networkConnection?.addListener(this)
        networkConnection?.addListener(AWSUtils)
    }

    private fun setupInstabug() {
        Instabug.Builder(this, BuildConfig.INSTABUG_TOKEN).setInvocationEvents(
                InstabugInvocationEvent.NONE
            ).build()
        Instabug.setPrimaryColor(ContextCompat.getColor(applicationContext, R.color.breeze_primary))
        Instabug.setWelcomeMessageState(WelcomeMessage.State.DISABLED)
        BugReporting.setScreenshotByMediaProjectionEnabled(true)
        BugReporting.setOptions(Option.EMAIL_FIELD_OPTIONAL)

        Timber.d("Registering invocation callback")
        BugReporting.setOnInvokeCallback {
            Timber.d("Screenshot detected")
            breezeInstabugUtil.handleInstabugInvocation(isShowInstabug = false)
        }

        InstabugCustomTextPlaceHolder().apply {
            setPlaceHoldersMap(
                hashMapOf(
                    InstabugCustomTextPlaceHolder.Key.INVOCATION_HEADER to getString(R.string.instabug_header),
                    InstabugCustomTextPlaceHolder.Key.REPORT_BUG to getString(R.string.instabug_report_bug),
                    InstabugCustomTextPlaceHolder.Key.REPORT_BUG_DESCRIPTION to getString(R.string.instabug_bug_desc),
                    InstabugCustomTextPlaceHolder.Key.REPORT_FEEDBACK to getString(R.string.instabug_feedback),
                    InstabugCustomTextPlaceHolder.Key.REPORT_FEEDBACK_DESCRIPTION to getString(R.string.instabug_feedback_desc),
                    InstabugCustomTextPlaceHolder.Key.REPORT_QUESTION to getString(R.string.instabug_question),
                    InstabugCustomTextPlaceHolder.Key.REPORT_QUESTION_DESCRIPTION to getString(R.string.instabug_question_desc),
                    InstabugCustomTextPlaceHolder.Key.COMMENT_FIELD_HINT_FOR_BUG_REPORT to getString(
                        R.string.instabug_comment
                    ),
                    InstabugCustomTextPlaceHolder.Key.COMMENT_FIELD_HINT_FOR_FEEDBACK to getString(R.string.instabug_comment),
                    InstabugCustomTextPlaceHolder.Key.COMMENT_FIELD_HINT_FOR_QUESTION to getString(R.string.instabug_comment),
                )
            )
        }.let {
            Instabug.setCustomTextPlaceHolders(it)
        }
    }

    private fun setupAppTheme() {
        UserTheme.setDefaultNightMode(UserTheme.LIGHT)
//        UserTheme.setDefaultNightMode(UserTheme.values().find { it.type == userPreferenceUtil.getUserThemePreference() }?: UserTheme.SYSTEM_DEFAULT)
    }

    @OptIn(ExperimentalPreviewMapboxNavigationAPI::class)
    fun destroyNavigationObservers() {
        AppSyncHelper.unsubscribeToAppSyncEvents() {}
        breezeERPUtil.stopERPHandler()
        breezeIncidentsUtil.stopIncidentHandler()
        breezeMiscRoadObjectUtil.stopMiscROHandler()
        breezeSchoolSilverZoneUpdateUtil.stopSchoolSilverZoneHandler()
        BreezeEHorizonProvider.destroy()
        VoiceInstructionsManager.destroyInstance()
    }


    private fun registerLifecycleHandler(lifeCycleHandler: AppLifeCycleHandler) {
        registerActivityLifecycleCallbacks(lifeCycleHandler)
        registerComponentCallbacks(lifeCycleHandler)
    }

    private fun onForegroundStateChanged(isAppInForeground: Boolean) {

        Timber.e("onForegroundStateChanged: $isAppInForeground")
        if (isAppInForeground) {
            val isAutoConnect = getUserDataPreference()?.isEnableOBUAutoConnect() ?: false
            if (isAutoConnect) {
                AWSUtils.checkUserAuthSession(object : AWSUtils.AuthSessionCallback {
                    override fun onAuthSuccess(token: String?) {
                        obuConnectionHelper.autoConnectLastOBUDevice()
                    }

                    override fun onAuthFailed() {
                        Timber.d("initOBU-checkUserAuthSession: Failed")
                    }
                })
            }
        }
    }

    override fun androidInjector() = dispatchingAndroidInjector

    override fun workerInjector(): AndroidInjector<ListenableWorker> = workerInjector


    companion object {
        var instance: App? = null
    }

    override fun onTerminate() {
        super.onTerminate()
        NetworkMonitor(this).stopNetworkCallback()
    }

    override fun onTrimMemory(level: Int) {

        when (level) {
            TRIM_MEMORY_COMPLETE -> {
                Timber.d("onTrimMemory() called - Level: TRIM_MEMORY_COMPLETE")
            }

            TRIM_MEMORY_MODERATE -> {
                Timber.d("onTrimMemory() called - Level: TRIM_MEMORY_MODERATE")
            }

            TRIM_MEMORY_BACKGROUND -> {
                Timber.d("onTrimMemory() called - Level: TRIM_MEMORY_BACKGROUND")
            }

            TRIM_MEMORY_UI_HIDDEN -> {
                Timber.d("onTrimMemory() called - Level: TRIM_MEMORY_UI_HIDDEN")
            }

            TRIM_MEMORY_RUNNING_CRITICAL -> {
                Timber.d("onTrimMemory() called - Level: TRIM_MEMORY_RUNNING_CRITICAL")
            }

            TRIM_MEMORY_RUNNING_LOW -> {
                Timber.d("onTrimMemory() called - Level: TRIM_MEMORY_RUNNING_LOW")
            }

            TRIM_MEMORY_RUNNING_MODERATE -> {
                Timber.d("onTrimMemory() called - Level: TRIM_MEMORY_RUNNING_MODERATE")
            }
        }

        super.onTrimMemory(level)
    }

    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        Timber.d("Internet - OnNetworkConnectionChanged ${isConnected}")
        Utils.runOnMainThread {
            handleChangeInNetworkConnection(isConnected)
        }
//        if (isConnected) {
//            if ((mCurrentActivity is DashboardActivity) || (mCurrentActivity is TurnByTurnNavigationActivity)) {
//                AppSyncHelper.subscribeToAppSyncEvents(applicationContext)
//            }
//        } else {
//            AppSyncHelper.unSubscribeAll(this){
//            }
//        }
    }

    fun handleChangeInNetworkConnection(isConnected: Boolean) {
        if (appLifeCycleHandler.isAppActive) {
            //Show toast if not connected
            if (!isConnected) {
                Timber.d("Internet - App in foreground and internet is not connected")
                showNoInternetToast()
            }

            if (currentTopActivityRef.get() != null) {
                if (isConnected) {
                    changeStatusBarColour(R.color.transparent)
                } else {
                    changeStatusBarColour(R.color.white)
                }
            }
        }
    }

    fun showNoInternetToast() {
        Timber.d("Internet - Showing no internet toast")
        CoroutineScope(Dispatchers.IO).launch {
            withContext(Dispatchers.Main) {
                val resIconId = R.drawable.no_internet
                GlobalToast().setDescription(resources.getString(R.string.no_internet_toast))
                    .setIcon(resIconId).setDismissDurationSeconds(5)
                    .show(currentTopActivityRef.get())
            }
        }
    }

    override fun onGPSLocation(isEnabled: Boolean) {
        Timber.d("onGPSLocation function called")
        handleChangeInGPSConnection(isEnabled)
    }

    fun handleChangeInGPSConnection(isEnabled: Boolean) {
        if (!(currentTopActivityRef.get() != null && (currentTopActivityRef.get() is AppCompatActivity) && appLifeCycleHandler.isAppActive)) return
        if (isEnabled) {
            Timber.d("handleChangeInGPSConnection  GPS Enabled")
            handleGPSEnabled()
        } else {
            handleGPSDisable()
        }
    }

    private fun handleGPSDisable() {
        Timber.d("handleChangeInGPSConnection  GPS Disabled")
        changeStatusBarColour(R.color.white)
        (currentTopActivityRef.get() as? BaseActivity<*, *>)?.showGPSTurnedOffBottomSheet()
    }

    private fun handleGPSEnabled() {
        changeStatusBarColour(R.color.transparent)
        (currentTopActivityRef.get() as? BaseActivity<*, *>)?.closeGPSTurnedOffBottomSheet()
    }

    // FIXME: react catalyst may not be initialized, this could cause crashes if events are invoked too early
    // FIXME: reactNativeHost?.reactInstanceManager addReactInstanceEventListener should be invoked before calling events
    private val carEventListener =
        ToAppRx.listenEvent(ToAppRxEvent::class.java).observeOn(AndroidSchedulers.mainThread())
            .subscribe { event ->
                when (event) {
                    is CarGetListRecentSearch -> {
                        val shareBusinessLogicPackage = shareBusinessLogicPackage
                        compositeDisposable.add(shareBusinessLogicPackage.getRequestsModule()
                            .flatMap {
                                // Then we can perform our action.
                                it.callRN(
                                    ShareBusinessLogicEvent.FN_GET_RECENT_LIST.eventName, null
                                )
                            }.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                            .subscribe({ result ->
                                val dataFromRN = result.getMap("data")
                                val recentSearches = dataFromRN?.getArray("recent_search")?.let {
                                    ArrayUtil.toArrayObject(
                                        it, SearchLocation::class.java
                                    )
                                } ?: arrayListOf()
                                val homeLocation = dataFromRN?.getMap("home")?.let {
                                    MapUtil.toObject(it, SearchLocation::class.java)
                                }
                                val workLocation = dataFromRN?.getMap("work")?.let {
                                    MapUtil.toObject(it, SearchLocation::class.java)
                                }
                                ToCarRx.postEvent(
                                    AppToRecentSearchScreenEvent(
                                        CarRecentSearchData(
                                            recentSearches = recentSearches,
                                            homeLocation = homeLocation,
                                            workLocation = workLocation,
                                        )
                                    )
                                )
                            }, { error ->
                                println("error $error")
                            }))
                    }

                    is AddToRecentList -> {
                        val shareBusinessLogicPackage = shareBusinessLogicPackage
                        compositeDisposable.add(shareBusinessLogicPackage.getRequestsModule()
                            .flatMap {
                                it.callRN(
                                    ShareBusinessLogicEvent.FN_SAVE_RECENT_LIST.eventName,
                                    event.item.toRNWritableMap()
                                )
                            }.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                            .subscribe({ result -> Timber.d("Save recent list: $result") },
                                { error -> Timber.d("Push AddToRecentList to RN error $error") }

                            ))
                    }

                    is GetShortcutListFromCar -> {
                        val shareBusinessLogicPackage = shareBusinessLogicPackage
                        compositeDisposable.add(shareBusinessLogicPackage.getRequestsModule()
                            .flatMap {
                                // Then we can perform our action.
                                it.callRN(
                                    ShareBusinessLogicEvent.GET_SHORTCUT_LIST.eventName, null
                                )
                            }.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                            .subscribe({ result ->

                                val dataFromRN = result.getMap("data")
                                val normalLocations = dataFromRN?.getArray("shortcutList")?.let {
                                    ArrayUtil.toArrayObject(
                                        it, SearchLocation::class.java
                                    )
                                } ?: arrayListOf()
                                val homeLocation = dataFromRN?.getArray("home")?.let {
                                    ArrayUtil.toArrayObject(it, SearchLocation::class.java)
                                }
                                val workLocation = dataFromRN?.getArray("work")?.let {
                                    ArrayUtil.toArrayObject(it, SearchLocation::class.java)
                                }
                                ToCarRx.postEvent(
                                    AppToShortcutListScreenEvent(
                                        CarShortcutsListData(
                                            normalShortcuts = normalLocations,
                                            home = if (!homeLocation.isNullOrEmpty()) homeLocation[0] else null,
                                            work = if (!workLocation.isNullOrEmpty()) workLocation[0] else null,
                                        )
                                    )
                                )
                            }, { error -> println("error $error") }))
                    }

                    is GetSearchLocationResult -> {
                        val shareBusinessLogicPackage = shareBusinessLogicPackage
                        compositeDisposable.add(shareBusinessLogicPackage.getRequestsModule()
                            .flatMap {
                                val params = Arguments.createMap()
                                params.putString("searchTerm", event.searchTerm)
                                ETAEngine.getInstance()?.currentLocation?.let {
                                    params.putString("lat", it.latitude.toString())
                                    params.putString("lng", it.longitude.toString())
                                }
                                it.callRN(
                                    ShareBusinessLogicEvent.FN_SEARCH_LOCATION.eventName, params
                                )
                            }.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                            .subscribe({ result ->
                                val dataTransfer = DataTransferToLocationList()
                                dataTransfer.list = ArrayUtil.toArrayObject(
                                    result.getArray("data")!!, SearchLocation::class.java
                                )
                                ToCarRx.postEvent(
                                    AppToSearchLocationEvent(
                                        dataTransfer
                                    )
                                )
                            }, { error ->
                                Timber.d(
                                    "SEARCHRESULT : Unable to load the search result [$error]"
                                )
                            }))
                    }

                    is GetLatLngGoogleIfNeed -> {
                        val shareBusinessLogicPackage = shareBusinessLogicPackage
                        compositeDisposable.add(shareBusinessLogicPackage.getRequestsModule()
                            .flatMap {
                                it.callRN(
                                    ShareBusinessLogicEvent.FN_GET_GOOGLE_LAT_LNG_IF_NEED.eventName,
                                    MapUtil.toWritableMap(event.searchLocation as Object),
                                )
                            }.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                            .subscribe({ result ->
                                val dataTransfer = DataTransferLatLngGoogle()
                                dataTransfer.data = MapUtil.toObject(
                                    result.getMap("data")!!,
                                    SearchLocation::class.java,
                                )
                                ToCarRx.postEvent(
                                    AppToSearchLocationLatLngGoogleEvent(dataTransfer)
                                )
                            }, { error -> println("error $error") }))
                    }

                    is SaveOnboardingState -> {
                        val shareBusinessLogicPackage = shareBusinessLogicPackage
                        compositeDisposable.add(shareBusinessLogicPackage.getRequestsModule()
                            .flatMap {
                                val rnData = Arguments.createMap().apply {
                                    putString("userOnboardingState", event.state)
                                }
                                it.callRN(
                                    ShareBusinessLogicEvent.FN_SAVE_ONBOARDING_STATE.eventName,
                                    rnData
                                )

                            }.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                            .subscribe({ result -> Timber.d("Save recent list: $result") },
                                { error -> Timber.d("Push AddToRecentList to RN error $error") }

                            ))
                    }

                    is AppToPhoneReportIssueEvent -> {
                        BugReporting.show(BugReporting.ReportType.BUG)
                    }

                    else -> {}
                }
            }


    private fun changeStatusBarColour(color: Int) {
        currentTopActivityRef.get()?.let {
            if (it !is TurnByTurnNavigationActivity) {
                it.window?.run {
                    addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                    clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
                    statusBarColor = ContextCompat.getColor(this@App, color)
                }
            }
        }
    }

    // React Native Code

    private var mReactNativeHost: ReactNativeHost? = null

    override fun getReactNativeHost(): ReactNativeHost {
        if (mReactNativeHost == null) {
            mReactNativeHost = object : ReactNativeHost(this) {
                override fun getUseDeveloperSupport(): Boolean {
                    return BuildConfig.DEBUG
                }

                override fun getPackages(): List<ReactPackage> = PackageList(this).packages.apply {
                    add(CommunicateWithNativePackage(userPreferenceUtil))
                    add(RNAuthPackage(myServiceInterceptor))
                    add(shareBusinessLogicPackage)
                }

                override fun getJSMainModuleName(): String {
                    return "index"
                }

                override fun getJSIModulePackage(): JSIModulePackage? {
                    return ReanimatedJSIModulePackage() // <- add
                }
            }
        }
        return mReactNativeHost!!
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    fun showOBUDeductionNotification(eventData: OBURoadEventData) {
        var shouldCheckShowingLowCardAlert = false
        if (appLifeCycleHandler.isAppInForeground) {
            (currentTopActivityRef.get() as? DashboardActivity)?.run {
                showOBUChargeResultGN(eventData)
                shouldCheckShowingLowCardAlert = eventData.shouldCheckLowCardBalance()
            }
        } else {
            PushNotificationUtils.createOBUChargeResultNotification(this, eventData)
            shouldCheckShowingLowCardAlert = eventData.shouldCheckLowCardBalance()
        }

        if (shouldCheckShowingLowCardAlert) {
            delayCheckShowingLowCardAlert()
        }
    }

    private fun delayCheckShowingLowCardAlert() {
        CoroutineScope(Dispatchers.IO).launch {
            delay(OBUConstants.OBU_LOW_CARD_ALERT_DELAY)
            OBUDataHelper.getOBUCardBalanceSGD()?.takeIf { balanceSGD ->
                    isActive && balanceSGD < OBUConstants.OBU_LOW_CARD_THRESHOLD / 100.0
                }?.let {
                    showLowCardBalanceAlertNonNavigation(it)
                }
        }
    }

    suspend fun showLowCardBalanceAlertNonNavigation(balanceSGD: Double) {
        if (appLifeCycleHandler.isAppInForeground) {
            withContext(Dispatchers.Main) {
                (currentTopActivityRef.get() as? DashboardActivity)?.takeIf {
                        MapboxNavigationApp.current()
                            ?.getTripSessionState() != TripSessionState.STARTED
                    }?.showLowCardGN(balanceSGD)
            }
        } else {
            PushNotificationUtils.createLowCardNotification(this@App, balanceSGD)
        }
    }
}

package com.ncs.breeze.common.utils

import com.breeze.model.enums.TrafficIncidentData
import com.breeze.model.constants.Constants

object RoadObjectUtil {


    /**
     * Generates a road object name based on the formula : (SPEED_CAMERA+roadObjIdentifier)
     * @return returns a unique Road Object Name
     */
    fun generateSpeedCameraRoadObjectName(roadObjIdentifier: Int): String {
        return Constants.SPEED_CAMERA_RO + roadObjIdentifier.toString()
    }

    /**
     * Generates a road object name based on the formula : (SCHOOL_ZONE+roadObjIdentifier)
     * @return returns a unique Road Object Name
     */
    fun generateSchoolZoneRoadObjectName(roadObjIdentifier: Int): String {
        return Constants.SCHOOL_ZONE_RO + roadObjIdentifier.toString()
    }

    /**
     * Generates a road object name based on the formula : (SEASON_PARKING_RO+roadObjIdentifier)
     * @return returns a unique Road Object Name
     */
    fun generateSeasonParkingZoneRoadObjectName(roadObjIdentifier: Int): String {
        return Constants.SEASON_PARKING_RO + roadObjIdentifier.toString()
    }


    /**
     * Generates a road object name based on the formula : (SILVER_ZONE+roadObjIdentifier)
     * @return returns a unique Road Object Name
     */
    fun generateSilverZoneRoadObjectName(roadObjIdentifier: Int): String {
        return Constants.SILVER_ZONE_RO + roadObjIdentifier.toString()
    }


    /**
     * Generates a road object name based on the formula : (incident.type+roadObjIdentifier)
     * @return returns a unique Road Object Name
     */
    fun generateRoadObjectName(incident: TrafficIncidentData, roadObjIdentifier: Int): String {
        return incident.type + roadObjIdentifier.toString()
    }

    /**
     * parses the incident type based on the formula : (incident.type+roadObjIdentifier)
     * @return returns the incident type by parsing the full roadobject name
     */
    fun parseIncidentTypeFromRoadObject(roadObject: String): String {
        for (index in (roadObject.length - 1) downTo 0) {
            if (!roadObject[index].isDigit()) {
                return roadObject.substring(0, index + 1)
            }
        }
        return roadObject
    }

}
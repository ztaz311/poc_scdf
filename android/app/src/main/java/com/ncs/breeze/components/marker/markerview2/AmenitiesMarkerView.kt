package com.ncs.breeze.components.marker.markerview2

import android.app.Activity
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import com.breeze.model.api.response.amenities.BaseAmenity
import com.breeze.model.constants.AmenityType
import com.breeze.model.constants.AmenityType.CARPARK
import com.breeze.model.constants.AmenityType.PASAR_MALAM
import com.breeze.model.constants.Constants
import com.breeze.model.enums.AmenityBand
import com.breeze.model.extensions.dpToPx
import com.facebook.react.bridge.Arguments
import com.google.android.gms.maps.model.LatLng
import com.mapbox.geojson.Point
import com.mapbox.maps.EdgeInsets
import com.mapbox.maps.MapboxMap
import com.mapbox.navigation.core.lifecycle.MapboxNavigationApp
import com.mapbox.navigation.core.trip.session.TripSessionState
import com.ncs.breeze.App
import com.ncs.breeze.R
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.extensions.android.getShareBusinessLogicPackage
import com.ncs.breeze.common.utils.BreezeMapZoomUtil
import com.ncs.breeze.common.utils.LocationBreezeManager
import com.ncs.breeze.databinding.AmenitiesMarkerTooltipBinding
import com.ncs.breeze.reactnative.nativemodule.ShareBusinessLogicEvent
import com.ncs.breeze.ui.base.BaseActivity
import com.ncs.breeze.ui.dashboard.fragments.exploremap.profile.ProfileZoneLayerExplore
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

class AmenitiesMarkerView(
    latLng: LatLng,
    mapboxMap: MapboxMap,
    val activity: Activity,
    itemId: String? = null,
    itemType: String? = null,
    showBookmarkIcon: Boolean,
    private val toolTipZoomRange: Double = Constants.DEFAULT_TOOLTIP_ZOOM_RADIUS,
    enableWalking: Boolean = false,
    enableZoneDrive: Boolean = true
) : MarkerView2(mapboxMap = mapboxMap, itemId) {

    var analyticsScreenName: String = ""
    private val viewBinding = AmenitiesMarkerTooltipBinding.inflate(LayoutInflater.from(activity), null, false)

    init {
        mLatLng = latLng
        viewBinding.pasarMalamInfo.isVisible = itemType?.lowercase(Locale.getDefault()) == PASAR_MALAM
        viewBinding.imgBookmark.isVisible = showBookmarkIcon
        val currentZone =
            ProfileZoneLayerExplore.detectSelectedProfileLocationInZone(LocationBreezeManager.getInstance().currentLocation)
        viewBinding.buttonWalkHere.isVisible = currentZone != ProfileZoneLayerExplore.NO_ZONE && enableWalking
        viewBinding.buttonNavigate.isVisible = currentZone == ProfileZoneLayerExplore.NO_ZONE || enableZoneDrive
        mViewMarker = viewBinding.root
    }

    private var currentAmenities: BaseAmenity? = null

    fun handleClickMarker(
        item: BaseAmenity,
        shouldRecenter: Boolean,
        navigationButtonClick: (BaseAmenity, BaseActivity.EventSource) -> Unit,
        walkingIconClick: (BaseAmenity, BaseActivity.EventSource) -> Unit,
        amenitiesIconClick: (BaseAmenity, Boolean) -> Unit,
        bookmarkIconClick: (BaseAmenity, BaseActivity.EventSource) -> Unit,
    ) {

        currentAmenities = item
        viewBinding.textviewAddress.text = item.name ?: item.provider
        viewBinding.tvNameStress.text = item.address
        displayCrowdsourceData(item)
        handleItemType(item)
        viewBinding.imgBookmark.setImageDrawable(
            ContextCompat.getDrawable(
                activity,
                if (item.isBookmarked) R.drawable.ic_bookmarked else R.drawable.ic_nobookmark
            )
        )


        item.startDate?.let {
            viewBinding.pasarMalamStartDate.text = SimpleDateFormat("dd MMM YY").format(Date(it * 1000L))
        }
        item.endDate?.let {
            viewBinding.pasarMalamEndDate.text =
                SimpleDateFormat("dd MMM YY").format(Date(it * 1000L))// *1000 to convert sec to millisec
        }

        item.additionalInfo?.let {
            viewBinding.tvAdditionalInfo.visibility = View.VISIBLE
            viewBinding.tvAdditionalInfo.text = it.text
            var color = Color.parseColor(it.style?.light?.color)
            if ((activity.application as? App)?.userPreferenceUtil?.isDarkTheme() == true) {
                color = Color.parseColor(it.style?.dark?.color)
            }
            viewBinding.tvAdditionalInfo.setTextColor(color)
        }

        viewBinding.buttonNavigate.setOnClickListener {
            resetView()
            navigationButtonClick.invoke(item, BaseActivity.EventSource.NONE);
        }

        viewBinding.buttonWalkHere.setOnClickListener {
            resetView()
            walkingIconClick.invoke(item, BaseActivity.EventSource.NONE)
        }

        viewBinding.imgBookmark.setOnClickListener {
            resetView()
            bookmarkIconClick.invoke(item, BaseActivity.EventSource.NONE)
        }

        viewBinding.buttonShare.isVisible =
            MapboxNavigationApp.current()?.getTripSessionState() != TripSessionState.STARTED
                    && item.amenityType != AmenityType.GENERAL

        viewBinding.buttonShare.setOnClickListener {
            Analytics.logClickEvent(
                "[map]:tooltip_${item.amenityType}_share",
                analyticsScreenName
            )
            shareAmenity(item)
        }

        viewBinding.buttonInvite.isVisible =
            MapboxNavigationApp.current()?.getTripSessionState() != TripSessionState.STARTED
                    && item.amenityType != AmenityType.GENERAL
        viewBinding.buttonInvite.setOnClickListener {
            Analytics.logClickEvent(
                "[map]:tooltip_${item.amenityType}_invite",
                analyticsScreenName
            )
            inviteLocation(item)
        }

        amenitiesIconClick.invoke(item, true)
        renderMarkerToolTip(item, shouldRecenter)
        updateTooltipPosition()
    }

    private fun displayCrowdsourceData(amenity: BaseAmenity) {
        val availabilityCSData = amenity.availabilityCSData
        viewBinding.layoutCarparkStatus.root.isVisible = availabilityCSData != null
        if (availabilityCSData == null) return
        val drawableHeaderCpStatus =
            AppCompatResources.getDrawable(activity, R.drawable.bg_header_carpark_status)
        drawableHeaderCpStatus?.setTint(
            Color.parseColor(availabilityCSData.themeColor ?: "#F26415")
        )
        viewBinding.layoutCarparkStatus.root.background = drawableHeaderCpStatus
        viewBinding.layoutCarparkStatus.icCarparkStatus.setImageResource(
            when (amenity.availablePercentImageBand) {
                AmenityBand.TYPE0.type -> R.drawable.ic_cp_stt_full
                AmenityBand.TYPE2.type -> R.drawable.ic_cp_stt_available
                else -> R.drawable.ic_cp_stt_few
            }
        )
        viewBinding.layoutCarparkStatus.tvStatusCarpark.text = availabilityCSData.title ?: ""
        viewBinding.layoutCarparkStatus.tvTimeUpdateCarparkStatus.text =
            "${availabilityCSData?.primaryDesc ?: activity.getString(com.breeze.customization.R.string.update_carpark_stt_time)} ${availabilityCSData?.generateDisplayedLastUpdate()}"
    }

    private fun createShareLocationData(item: BaseAmenity) =
        Arguments.fromBundle(
            bundleOf(
                "address1" to if (item.amenityType == CARPARK) item.name else item.address,
                "latitude" to (item.lat?.toString() ?: ""),
                "longitude" to (item.long?.toString() ?: ""),
                "amenityId" to item.id,
                "amenityType" to item.amenityType,
                "name" to item.name,
            )
        )

    private fun shareAmenity(item: BaseAmenity) {
        viewBinding.root.context.getShareBusinessLogicPackage()?.getRequestsModule()?.flatMap {
            it.callRN(
                ShareBusinessLogicEvent.SHARE_LOCATION_FROM_NATIVE.eventName,
                createShareLocationData(item)
            )
        }?.subscribeOn(Schedulers.io())?.observeOn(AndroidSchedulers.mainThread())?.subscribe()
    }

    private fun inviteLocation(item: BaseAmenity) {
        viewBinding.root.context.getShareBusinessLogicPackage()?.getRequestsModule()?.flatMap {
            it.callRN(
                ShareBusinessLogicEvent.LOCATION_INVITE_FROM_NATIVE.eventName,
                createShareLocationData(item)
            )
        }?.subscribeOn(Schedulers.io())?.observeOn(AndroidSchedulers.mainThread())?.subscribe()
    }


    private fun handleItemType(item: BaseAmenity) {
        if (item.imageType == Constants.AMENITY.ANE) {
            viewBinding.logo.setBackgroundResource(R.drawable.bg_ae_clinic_logo)
            viewBinding.logo.setTextColor(activity.getColor(R.color.passion_pink))
            viewBinding.logo.setText(R.string.logo_ane)
            viewBinding.logo.visibility = View.VISIBLE
        }
    }


    private fun renderMarkerToolTip(
        item: BaseAmenity,
        shouldRecenter: Boolean
    ) {
        updateTooltipPosition()
        if (!shouldRecenter) {
            viewBinding.root.bringToFront()
            return
        }

        BreezeMapZoomUtil.zoomViewToRange(mapboxMap,
            Point.fromLngLat(item.long!!, item.lat!!),
            toolTipZoomRange,
            EdgeInsets(10.0.dpToPx(), 10.0.dpToPx(), 200.0.dpToPx(), 10.0.dpToPx()),
            animationEndCB = {
                viewBinding.root.bringToFront()
            }
        )
    }


    override fun showMarker() {
        super.showMarker()
        viewBinding.root.visibility = View.VISIBLE

    }

    override fun hideMarker() {
        super.hideMarker()
        viewBinding.root.visibility = View.INVISIBLE

    }


    override fun updateTooltipPosition() {
        val locationPoint = mLatLng?.let { Point.fromLngLat(it.longitude, it.latitude) } ?: return
        val mainView = mViewMarker?:return
        val screenCoordinate = mapboxMap.pixelForCoordinate(locationPoint)
        mainView.measure(
            View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
            View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
        )
        mainView.x = screenCoordinate.x.toFloat() - mainView.measuredWidth / 2f
        mainView.y = screenCoordinate.y.toFloat() - mainView.measuredHeight - 24.dpToPx()
    }

    override fun updateBookMarkIconState(saved: Boolean, bookmarkId: Int) {
        currentAmenities?.isBookmarked = saved
        viewBinding.imgBookmark.setImageDrawable(
            ContextCompat.getDrawable(
                activity,
                if (saved) R.drawable.ic_bookmarked else R.drawable.ic_nobookmark
            )
        )
        currentAmenities?.bookmarkId = if (saved) bookmarkId else null

    }
}
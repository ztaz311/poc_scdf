package com.ncs.breeze.common.helper.obu.callback

import androidx.core.os.bundleOf
import com.ncs.breeze.App
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.Event
import com.ncs.breeze.common.analytics.Param
import com.ncs.breeze.ui.dashboard.activity.DashboardActivity
import sg.gov.lta.obu.sdk.core.types.OBUError
import sg.gov.lta.obu.sdk.data.interfaces.OBUInitListener
import timber.log.Timber
import java.lang.ref.WeakReference

open class OBUInitListenerKt(
    val app: App?,
    var onError: (error: OBUError) -> Unit = {},
    var onSuccess: () -> Unit = {},
) : OBUInitListener {
    private val appRef = WeakReference<App?>(app)
    override fun onError(error: OBUError) {
        Timber.d(error, "OBUSDK initialization failed: ${error.code} - ${error.message}")
        val message = "OBUSDK Init Error Code: ${error.code}, message: ${error.localizedMessage}"
        Analytics.logEvent(
            Event.OBU_SYSTEM_EVENT,
            bundleOf(
                Param.KEY_EVENT_VALUE to Event.OBU_SDK_INIT_FAILURE,
                Param.KEY_MESSAGE to message,

                )
        )
        Analytics.logEvent(
            Event.OBU_SYSTEM_EVENT,
            bundleOf(
                Param.KEY_EVENT_VALUE to Event.OBU_VERBOSE_ERROR,
                Param.KEY_MESSAGE to "Code: ${error.code}, message: ${error.localizedMessage}",
            )
        )
        onError.invoke(error)
    }

    override fun onSuccess() {
        Timber.d("OBUSDK initialization success")
        appRef.get()?.run {
            // todo need verify sending PairedVehicleList many time
            Timber.d("OBUSDK initialization updatePairedVehicleList")
            shareBusinessLogicHelper.updatePairedVehicleList()
        }
        onSuccess.invoke()
    }
}

typealias OBUInitListenerInitializer = OBUInitListenerKt.() -> Unit
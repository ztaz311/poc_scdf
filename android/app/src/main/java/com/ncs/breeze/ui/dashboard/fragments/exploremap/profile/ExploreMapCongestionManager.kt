package com.ncs.breeze.ui.dashboard.fragments.exploremap.profile

import android.content.Context
import com.breeze.model.api.response.tbr.ResponseCongestionDetail
import com.ncs.breeze.common.utils.BreezeMapDataHolder
import com.ncs.breeze.common.utils.BreezeMapZoomUtil
import com.ncs.breeze.common.utils.LocationBreezeManager
import com.mapbox.maps.EdgeInsets
import com.mapbox.maps.MapboxMap
import com.mapbox.maps.Style
import com.ncs.breeze.ui.dashboard.manager.ExploreMapStateManager

class ExploreMapCongestionManager(
    val mapboxMap: MapboxMap,
    val context: Context,
    val style: Style
) {

    private val profileZoneLayerExplore = ProfileZoneLayerExplore(mapboxMap, context, style)

    fun reDrawCircle(shouldMoveCamera: Boolean, edgeInset: EdgeInsets? = null) {
        profileZoneLayerExplore?.let { profileZone ->
            ExploreMapStateManager.contentDetailsResponse?.zones?.let { zonesList ->
                profileZone.remove()
                profileZone.updateConfigCircleLayer(zonesList)
                profileZone.addSourceAndLayerID()
                profileZone.drawCircle()
                if (shouldMoveCamera && edgeInset != null && mapboxMap != null) {
                    profileZone.selectedProfilePoint?.let {
                        BreezeMapZoomUtil.recenterMapToOuterCircle(
                            mapboxMap,
                            it,
                            profileZone.maxRadius,
                            edgeInset = edgeInset,
                        )
                    }
                }
            }
        }
    }


    fun zoomCameraDependOnCurrentLocation(
        selectedMapProfile: String?,
        edgeInset: EdgeInsets? = null
    ) {
        profileZoneLayerExplore.let { profileZone ->
            BreezeMapDataHolder.profilePreferenceHashmap[selectedMapProfile]?.zones?.let {
                if (edgeInset != null) {
                    profileZone.selectedProfilePoint?.let {
                        if (LocationBreezeManager.getInstance().currentLocation != null) {
                            BreezeMapZoomUtil.zoomCameraDependOnCircleIncludeCurrentLocation(
                                mapboxMap,
                                it,
                                profileZoneLayerExplore.maxRadius,
                                edgeInset = edgeInset,
                                LocationBreezeManager.getInstance().currentLocation!!
                            )
                        } else {
                            BreezeMapZoomUtil.recenterMapToOuterCircle(
                                mapboxMap,
                                it,
                                profileZoneLayerExplore.maxRadius,
                                edgeInset = edgeInset,
                            )
                        }
                    }
                }
            }
        }
    }


    /**
     * update color circle layer
     */
    fun updateColorCircleLayer(
        pListColorResponse: java.util.ArrayList<ResponseCongestionDetail>,
        selectedMapProfile: String?
    ) {
        profileZoneLayerExplore.let { profileZone ->
            profileZone.updateColorOuterRing(pListColorResponse)
            ExploreMapStateManager.contentDetailsResponse?.zones?.let {
                profileZone.updateZoneZoomDetails(it)
            }
        }
    }


    /**
     * show tiong bahru zone
     */
    fun showProfileZoneLayer(shouldMoveCamera: Boolean, edgeInset: EdgeInsets? = null) {
        profileZoneLayerExplore?.let { profileZone ->
            profileZone.showCircleLayer()
            if (shouldMoveCamera && edgeInset != null) {
                profileZone.selectedProfilePoint?.let {
                    BreezeMapZoomUtil.recenterMapToOuterCircle(
                        mapboxMap,
                        it,
                        profileZoneLayerExplore.maxRadius,
                        edgeInset = edgeInset,
                    )
                }
            }
        }
    }

    fun hideProfileZoneLayer() {
        profileZoneLayerExplore?.hideCircleLayer()
    }


}
package com.ncs.breeze.di.module

import android.app.Application
import android.content.Context
import com.ncs.breeze.App
import dagger.Module
import dagger.Provides

@Module
class AppContextModule {

    @Provides
    internal fun provideContext(app: App): Context {
        return app.applicationContext
    }

    @Provides
    internal fun provideApplication(app: App): Application {
        return app
    }

}
package com.ncs.breeze.components

import android.app.Activity
import android.view.Gravity
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.Event
import com.bumptech.glide.Glide
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.ncs.breeze.R
import com.ncs.breeze.common.analytics.Screen
import timber.log.Timber

class GlobalNotification {

    companion object {
        const val UNSET_RES_VALUE = -1
        const val DIALOG_AUTO_DISMISS_DURATION_SECONDS: Int = 10
        var isShowing = false
    }

    private var snackbar: Snackbar? = null
    private var isAutoDismiss: Boolean = true
    private var isDismissWithoutAction: Boolean = true
    private var analyticsScreenName: String = ""
    private var analyticsPopupName: String = Event.POPUP_GLOBAL_NOTIFICATION
    private var analyticsEventValue: String = Event.GLOBAL_NOTIFICATION_CLOSE

    sealed class Theme(
        val icon: Int,
        val background: Int,
        val titleTextColor: Int,
        val descTextColor: Int,
        val actionTextColor: Int,
    )

    fun addToList(list: ArrayList<GlobalNotification?>): GlobalNotification {
        list.clear()
        list.add(this@GlobalNotification)
        return this
    }

    object ThemeDefault : Theme(
        icon = UNSET_RES_VALUE,
        background = R.drawable.bg_carparking_notification,
        titleTextColor = R.color.dark_navy_blue,
        descTextColor = R.color.dark_navy_blue,
        actionTextColor = R.color.themed_cruise_mode_toast_text
    )

    object ThemeAdmin : Theme(
        icon = UNSET_RES_VALUE,
        background = R.drawable.bg_admin_notification,
        titleTextColor = R.color.themed_dark_font_color,
        descTextColor = R.color.themed_dark_font_color,
        actionTextColor = R.color.color_admin_text_action
    )

    object ThemeTutorial : Theme(
        icon = UNSET_RES_VALUE,
        background = R.drawable.bg_tutorial_notification,
        titleTextColor = R.color.themed_dark_font_color,
        descTextColor = R.color.themed_dark_font_color,
        actionTextColor = R.color.themed_breeze_primary
    )


    object ThemeSuccess : Theme(
        icon = R.drawable.tick_successful,
        background = R.drawable.bg_success_toast_notification,
        titleTextColor = R.color.themed_dark_font_color,
        descTextColor = R.color.themed_dark_font_color,
        actionTextColor = R.color.themed_parakeet_green
    )

    object ThemeWarningError : Theme(
        icon = R.drawable.cross_red,
        background = R.drawable.bg_warning_toast_notification,
        titleTextColor = R.color.themed_dark_font_color,
        descTextColor = R.color.themed_dark_font_color,
        actionTextColor = R.color.themed_passion_pink
    )

    object ThemeMiscellaneous : Theme(
        icon = R.drawable.ic_breeze_round,
        background = R.drawable.bg_miscellaneous_toast_notification,
        titleTextColor = R.color.themed_dark_font_color,
        descTextColor = R.color.themed_dark_font_color,
        actionTextColor = R.color.themed_breeze_primary
    )

    object ThemeParking : Theme(
        icon = UNSET_RES_VALUE,
        background = R.drawable.bg_miscellaneous_toast_notification,
        titleTextColor = R.color.themed_dark_font_color,
        descTextColor = R.color.themed_dark_font_color,
        actionTextColor = R.color.themed_breeze_primary
    )
    object ThemeERP : Theme(
        icon = UNSET_RES_VALUE,
        background = R.drawable.bg_erp_toast_notification,
        titleTextColor = R.color.themed_dark_font_color,
        descTextColor = R.color.themed_dark_font_color,
        actionTextColor = R.color.themed_ocean_blue
    )

    private var icon: Int = UNSET_RES_VALUE
    private var iconUrl: String? = null
    private var theme: Theme = ThemeDefault
    private lateinit var title: String
    private lateinit var description: String
    private var dismissDuration: Int = DIALOG_AUTO_DISMISS_DURATION_SECONDS
    private var actionText: String? = null
    private var actionListener: View.OnClickListener? = null
    private var mActionSeeListener: ((Boolean, Boolean) -> Unit)? = null

    fun setIcon(icon: Int): GlobalNotification {
        this.icon = icon
        return this
    }

    fun setIconUrl(iconUrl: String): GlobalNotification {
        this.iconUrl = iconUrl
        return this
    }

    fun setTheme(theme: Theme): GlobalNotification {
        this.theme = theme
        return this
    }

    fun setTitle(title: String): GlobalNotification {
        this.title = title
        return this
    }

    fun setDescription(description: String): GlobalNotification {
        this.description = description
        return this
    }

    fun setActionText(actionText: String?): GlobalNotification {
        this.actionText = actionText
        return this
    }

    fun setActionListener(actionListener: View.OnClickListener?): GlobalNotification {
        this.actionListener = actionListener
        return this
    }

    fun setDismissDurationSeconds(dismissDuration: Int): GlobalNotification {
        this.dismissDuration = dismissDuration
        return this
    }

    /**
     * condition for user seen notification
     * 1. not click x button for close notification
     * 2. no click action see on or view more
     */
    fun setSeeListener(callback: (Boolean, Boolean) -> Unit): GlobalNotification {
        mActionSeeListener = callback
        return this
    }

    fun setAnalyticsData(
        screenName: String,
        popupName: String,
        eventValue: String
    ): GlobalNotification {
        this.analyticsScreenName = screenName
        this.analyticsPopupName = popupName
        this.analyticsEventValue = eventValue
        return this
    }

    fun show(activity: Activity?, paddingTop: Int = 0) {
        if (activity == null || activity.isDestroyed) {
            Timber.e("Requested activity is NULL!!!")
            return
        }
        val view = activity.findViewById<View>(R.id.mainLayout)
        if (view == null) {
            Timber.e("Main layout not defined in requested activity!!!")
            return
        }
        snackbar = Snackbar.make(view, "", dismissDuration * 1000)
        snackbar?.view?.setBackgroundColor(android.graphics.Color.TRANSPARENT)
        val snackbarLayout = snackbar?.view as Snackbar.SnackbarLayout
        snackbarLayout.setPadding(0, paddingTop, 0, 0)
        snackbar?.let { snb ->
            val rootView = initViews(snb, activity)
            snackbarLayout.addView(rootView, 0)

            snb.addCallback(object : BaseTransientBottomBar.BaseCallback<Snackbar>() {
                override fun onDismissed(transientBottomBar: Snackbar?, event: Int) {
                    super.onDismissed(transientBottomBar, event)
                    isShowing = false
                    mActionSeeListener?.invoke(isAutoDismiss, isDismissWithoutAction)
                    Timber.d("onDismissed snackbar")
                    Analytics.logClosePopupEvent(
                        analyticsPopupName,
                        Screen.getScreenNameGlobalNotification(title ?: "")
                    )
                }

                override fun onShown(transientBottomBar: Snackbar?) {
                    super.onShown(transientBottomBar)
                    Timber.d("onShown snackbar")
                    Analytics.logOpenPopupEvent(
                        Event.POPUP_OPEN,
                        Screen.getScreenNameGlobalNotification(title ?: "")
                    )
                }
            })

            val params = snb.view.layoutParams as FrameLayout.LayoutParams
            params.gravity = Gravity.TOP
            snb.view.layoutParams = params
            snb.show()
            isShowing = true
        }
    }

    fun dimissGlobalNotification() {
        snackbar?.dismiss()
    }

    private fun initViews(snackbar: Snackbar, activity: Activity): View {
        val rootView =
            activity.layoutInflater.inflate(R.layout.layout_global_notification, null, false);
        val containerView = rootView?.findViewById<View>(R.id.containerView)
        val imgIcon = rootView?.findViewById<ImageView>(R.id.imgIcon)
        val imgCloseDialog = rootView?.findViewById<View>(R.id.imgCloseDialog)
        val txtTitle = rootView?.findViewById<TextView>(R.id.txtTitle)
        val txtDescription = rootView?.findViewById<TextView>(R.id.txtDescription)
        val txtAction = rootView?.findViewById<TextView>(R.id.txtAction)

        containerView?.setBackgroundResource(theme.background)
        txtTitle?.setTextColorResource(theme.titleTextColor)
        txtDescription?.setTextColorResource(theme.descTextColor)
        txtAction?.setTextColorResource(theme.actionTextColor)

        if (iconUrl != null && iconUrl!!.trim().isNotEmpty()) {
            Glide.with(activity)
                .load(iconUrl)
                .placeholder(R.drawable.ic_breeze_round_corner)
                .error(R.drawable.ic_breeze_round_corner)
                .into(imgIcon!!)
        } else if (icon != UNSET_RES_VALUE) {
            imgIcon?.setImageResource(icon)
        } else if (theme.icon != UNSET_RES_VALUE) {
            imgIcon?.setImageResource(theme.icon)
        } else {
            imgIcon?.setImageResource(R.drawable.ic_breeze_round_corner)
        }

        txtTitle?.text = title
        txtDescription?.text = description
        imgCloseDialog?.setOnClickListener {

            /**
             * user close notification -> user seen
             */
            isAutoDismiss = false
            isDismissWithoutAction = true
            Analytics.logClickEvent(
                analyticsEventValue,
                Screen.getScreenNameGlobalNotification(title)
            )
            Analytics.logClosePopupEvent(
                analyticsPopupName,
                Screen.getScreenNameGlobalNotification(title)
            )
            snackbar.dismiss()
        }
        if (actionText != null) {
            txtAction?.setText(actionText)
            txtAction?.setOnClickListener {
                /**
                 * user close notification -> user seen
                 */
                isAutoDismiss = false
                isDismissWithoutAction = false

                actionListener?.onClick(txtAction)
                snackbar.dismiss()
            }
            txtAction?.visibility = View.VISIBLE
        } else {
            txtAction?.visibility = View.GONE
        }
        return rootView
    }

    private fun TextView.setTextColorResource(colorRes: Int) {
        setTextColor(ContextCompat.getColor(context, colorRes))
    }
}
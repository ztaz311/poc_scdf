package com.ncs.breeze.ui.login.fragment.forgotPassword.module

import com.ncs.breeze.ui.login.fragment.forgotPassword.view.ForgotNewPasswordFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Suppress("unused")
@Module
abstract class ForgotNewPasswordModule {
    @ContributesAndroidInjector
    abstract fun contributeForgotNewPasswordFragment(): ForgotNewPasswordFragment
}
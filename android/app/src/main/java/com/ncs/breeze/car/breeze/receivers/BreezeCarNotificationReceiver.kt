package com.ncs.breeze.car.breeze.receivers

import android.content.BroadcastReceiver
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.car.app.CarContext
import com.ncs.breeze.car.BreezeCarAppService
import com.breeze.model.constants.Constants

class BreezeCarNotificationReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        val intentAction = intent.action
        if (ACTION_OPEN_APP == intentAction) {
            CarContext.startCarApp(
                intent,
                Intent(Intent.ACTION_VIEW)
                    .setComponent(ComponentName(context, BreezeCarAppService::class.java))
                    .setData(Uri.fromParts(URI_SCHEME, URI_HOST, intentAction))
                    .putExtra(Constants.CONGESTION_ALERT_EXTRA_DATA, intent.extras?.getBundle(Constants.CONGESTION_ALERT_EXTRA_DATA))
                    .putExtra(Constants.PARKING_AVAILABILITY_EXTRA_DATA, intent.extras?.getBundle(Constants.PARKING_AVAILABILITY_EXTRA_DATA))
            )
        }
    }

    companion object {
        const val ACTION_OPEN_APP = "ACTION_OPEN_APP"
        const val URI_SCHEME = "https"
        const val URI_HOST = "breeze.com"
    }

}
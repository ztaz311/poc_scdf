package com.ncs.breeze.helper

import android.Manifest.permission.ACCESS_COARSE_LOCATION
import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.Manifest.permission.POST_NOTIFICATIONS
import android.content.Context
import android.content.Intent
import android.os.Build
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.ncs.breeze.ui.splash.PermissionActivity

object PermissionHelper {
    fun requestLocationPermission(
        context: Context,
        onAllPermissionGranted: () -> Unit = {},
    ) {
        val permissionList = mutableListOf(ACCESS_COARSE_LOCATION, ACCESS_FINE_LOCATION)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            permissionList.add(POST_NOTIFICATIONS)
        }

        Dexter.withContext(context)
            .withPermissions(permissionList)
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                    report?.let {
                        if (it.areAllPermissionsGranted()) {
                            onAllPermissionGranted.invoke()
                        } else {
                            it.deniedPermissionResponses.find { deniedResponse ->
                                deniedResponse.permissionName == ACCESS_COARSE_LOCATION || deniedResponse.permissionName == ACCESS_FINE_LOCATION
                            }?.run {
                                context.startActivity(
                                    Intent(
                                        context, PermissionActivity::class.java
                                    )
                                )
                            }

                        }
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: MutableList<PermissionRequest>?,
                    token: PermissionToken?
                ) {
                    token?.continuePermissionRequest()
                }
            })
            .check()
    }
}
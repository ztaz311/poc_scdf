package com.ncs.breeze.ui.dashboard.activity

import android.location.Location
import com.breeze.model.constants.Constants
import com.breeze.model.extensions.dpToPx
import com.mapbox.maps.EdgeInsets
import com.mapbox.maps.MapboxMap
import com.ncs.breeze.common.constant.dashboard.LandingMode
import com.ncs.breeze.common.utils.BreezeMapDataHolder
import com.ncs.breeze.common.utils.BreezeMapZoomUtil
import com.ncs.breeze.components.map.profile.BaseMapCamera
import com.ncs.breeze.components.map.profile.ProfileZoneLayer
import com.ncs.breeze.ui.dashboard.manager.DashboardMapStateManager
import kotlin.math.max

class DashBoardMapCamera(
    val mapboxMap: MapboxMap,
    private val dashboardViewModel: DashboardViewModel
) : BaseMapCamera() {

    private var dashBoardMapLanding: DashBoardMapLanding? = null

    init {
        maxEdgeInsets = EdgeInsets(10.0.dpToPx(), 20.0.dpToPx(), 300.0.dpToPx(), 20.0.dpToPx())
        overviewEdgeInsets = maxEdgeInsets
        edgeCircleInsets = EdgeInsets(25.0.dpToPx(), 20.0.dpToPx(), 300.0.dpToPx(), 20.0.dpToPx())
    }

    fun setDashboardMapLanding(dashBoardMapLanding: DashBoardMapLanding) {
        this.dashBoardMapLanding = dashBoardMapLanding
    }

    fun determineZoomRadius(isCarparksShown: Boolean): Double? {
        dashBoardMapLanding?.let { dashBoardMapLanding ->
            return if (dashBoardMapLanding.canShowCarParkMarkers()) {
                determineCarParkZoomRadius(isCarparksShown)
            } else if (DashboardMapStateManager.currentModeSelected == LandingMode.ERP) {
                if (dashBoardMapLanding.furthestERPDistance == 0.0) null else dashBoardMapLanding.furthestERPDistance
            } else if (DashboardMapStateManager.currentModeSelected == LandingMode.TRAFFIC) {
                if (dashBoardMapLanding.furthestTrafficDistance == 0.0) null else dashBoardMapLanding.furthestTrafficDistance
            } else {
                null
            }
        }
        return null
    }


    /**
     * @return true if the map re-centered to the selected profile
     */
    internal fun recenterMapBasedOnProfile(
        isCarParksShown: Boolean,
        getCurrentLocation: () -> Location?,
        animationEndCB: () -> Unit
    ): Boolean {
        val currentSelectedProfile = DashboardMapStateManager.selectedMapProfile
        val selectedProfile =
            if (DashboardMapStateManager.isInMapProfileSelectedMode) currentSelectedProfile else null
        return recenterMap(selectedProfile, isCarParksShown, getCurrentLocation, animationEndCB)
    }

    /**
     * @return true if the map re-centered to the selected profile
     */
    internal fun recenterMapBasedOnProfile(
        isCarParksShown: Boolean,
        getCurrentLocation: () -> Location?
    ): Boolean {
        return recenterMapBasedOnProfile(isCarParksShown, getCurrentLocation, animationEndCB = {})
    }


    /**
     * @return true if the map re-centered to the selected profile
     */
    internal fun recenterMap(
        selectedProfile: String?,
        isCarParksShown: Boolean,
        getCurrentLocation: () -> Location?,
        animationEndCB: () -> Unit
    ): Boolean {
        val location = getCurrentCenterLocation(getCurrentLocation)
        var reCenteredToProfile = false
        if (location != null) {
            val radius =
                if (selectedProfile == null || DashboardMapStateManager.isInSearchLocationMode) {
                    determineZoomRadius(isCarParksShown)
                } else {
                    reCenteredToProfile = true
                    ProfileZoneLayer.getZoneMaxRadius(selectedProfile)
                }
            BreezeMapZoomUtil.recenterMapToLocation(
                mapboxMap,
                lat = location.latitude,
                lng = location.longitude,
                maxEdgeInsets,
                zoomRadius = radius,
                animationEndCB = animationEndCB
            )
        }
        return reCenteredToProfile
    }


    internal fun recenterMap(isCarParksShown: Boolean, getCurrentLocation: () -> Location?) {
        val location = getCurrentCenterLocation(getCurrentLocation)
        if (location != null) {
            val radius = determineZoomRadius(isCarParksShown)
            BreezeMapZoomUtil.recenterMapToLocation(
                mapboxMap,
                lat = location.latitude,
                lng = location.longitude,
                maxEdgeInsets,
                zoomRadius = radius
            )
        }
    }

    internal fun recenterMap(
        isCarParksShown: Boolean,
        getCurrentLocation: () -> Location?,
        animationEndCB: () -> Unit
    ) {
        val location = getCurrentCenterLocation(getCurrentLocation)
        if (location != null) {
            val radius = determineZoomRadius(isCarParksShown)
            BreezeMapZoomUtil.recenterMapToLocation(
                mapboxMap,
                lat = location.latitude,
                lng = location.longitude,
                maxEdgeInsets,
                zoomRadius = radius,
                animationEndCB = animationEndCB
            )
        }
    }

    fun getCurrentCenterLocation(getCurrentLocation: () -> Location?): Location? {

        return if (DashboardMapStateManager.isInSearchLocationMode) {
            DashboardMapStateManager.searchLocationAddress?.getRoutableLocation()
        } else {
            if (DashboardMapStateManager.isInMapProfileSelectedMode && DashboardMapStateManager.selectedMapProfile != null) {
                val zones =
                    BreezeMapDataHolder.profilePreferenceHashmap[DashboardMapStateManager.selectedMapProfile]?.zones
                if (zones?.isNotEmpty() == true) {
                    val location = Location("")
                    location.latitude = zones[0].centerPointLat!!
                    location.longitude = zones[0].centerPointLong!!
                    return location
                }
                return getCurrentLocation()
            }
            getCurrentLocation()
        }
    }

    private fun determineCarParkZoomRadius(isCarParksShown: Boolean): Double? {
        val furthestCarParkDistance =
            if (isCarParksShown) dashboardViewModel.furthestCarpark else 0.0
        val furthestAmenity = max(furthestCarParkDistance, dashboardViewModel.furthestAmenity)
        if (furthestAmenity > 0.0) return furthestAmenity
        return if ((dashboardViewModel.mListCarpark.value?.size ?: 0) > 0
        ) Constants.CARPARK_ZOOM_RADIUS else null
    }

    fun setCameraToolTipThreshold() {
        dashBoardMapLanding?.setCameraToolTipThreshold()
    }

    fun removeCameraToolTipThreshold() {
        dashBoardMapLanding?.removeCameraToolTipThresholdListener()
    }

}
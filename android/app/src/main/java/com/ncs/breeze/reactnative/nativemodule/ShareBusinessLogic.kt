package com.ncs.breeze.reactnative.nativemodule

import com.facebook.react.bridge.Arguments
import com.facebook.react.bridge.ReactApplicationContext
import com.facebook.react.bridge.ReactContextBaseJavaModule
import com.facebook.react.bridge.ReactMethod
import com.facebook.react.bridge.ReadableMap
import com.facebook.react.bridge.WritableMap
import com.facebook.react.modules.core.DeviceEventManagerModule
import io.reactivex.Single
import io.reactivex.SingleEmitter
import timber.log.Timber
import java.util.UUID

class ShareBusinessLogic(context: ReactApplicationContext) : ReactContextBaseJavaModule(context) {
    override fun getName(): String {
        return "ShareBusinessLogic"
    }

    // We need to expose the constants for our events.
    override fun hasConstants(): Boolean = true


    /** The requests currently waiting on data from `react-native`. */
    private val pendingRequests = mutableMapOf<String, SingleEmitter<ReadableMap>>()

    // This will be called by your `Activity` or `Service` to execute some JavaScript code.
    fun callRN(functionName: String, data: ReadableMap?): Single<ReadableMap> {
        Timber.d("SHARE_BUSINESS_LOGIC $functionName: $data")
        return Single.create {
            val map = getMapWithRequestId(it)
            map.putString(EVENT_KEY_FUNCTION_NAME, functionName)
            map.putMap(EVENT_KEY_DATA, data)
            sendEvent(ShareBusinessLogicEvent.REQUEST_EVENT_NAME.eventName, map)
        }
    }

    // This method will be called by React Native once the result is ready.
    @ReactMethod
    fun finishedRequest(requestId: String, result: ReadableMap) {
        synchronized(pendingRequests) {
            pendingRequests[requestId]?.onSuccess(result)
            pendingRequests.remove(requestId)
        }
    }

    // This method will be called by React Native if there was an error performing the action.
    @ReactMethod
    fun failedRequest(requestId: String, errorMessage: String) {
        synchronized(pendingRequests) {
            pendingRequests[requestId]?.onError(RuntimeException("Error thrown in JS: $errorMessage"))
            pendingRequests.remove(requestId)
        }
    }

    // This handles keeping track of running requests.
    private fun getMapWithRequestId(emitter: SingleEmitter<ReadableMap>): WritableMap {
        // We store the emitter so we can call it when we get our results back from `react-native`.
        val requestId = UUID.randomUUID().toString()
        synchronized(pendingRequests) {
            pendingRequests[requestId] = emitter
        }
        val map = Arguments.createMap()
        map.putString(EVENT_KEY_REQUEST_ID, requestId)
        return map
    }

    // This sends our request down to React Native to be handled.
    private fun sendEvent(
        eventName: String,
        params: WritableMap
    ) {
        if (reactApplicationContext.hasActiveCatalystInstance()) {
            reactApplicationContext
                .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter::class.java)
                .emit(eventName, params)
        }
    }

    override fun getConstants(): MutableMap<String, Any> = mutableMapOf<String, Any>().apply {
        ShareBusinessLogicEvent.values().forEach { event -> put(event.eventName, event.eventName) }
    }

    companion object {
        // The keys we use to send data to React Native.
        private const val EVENT_KEY_REQUEST_ID = "requestId"
        private const val EVENT_KEY_DATA = "data"
        private const val EVENT_KEY_FUNCTION_NAME = "functionName"
    }


}
package com.ncs.breeze.components

import androidx.work.Constraints
import androidx.work.Data
import androidx.work.NetworkType
import androidx.work.OneTimeWorkRequest
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkManager
import com.mapbox.navigation.core.lifecycle.MapboxNavigationApp
import com.ncs.breeze.App
import com.breeze.model.firebase.NavigationLogData
import com.ncs.breeze.common.worker.WorkerNavigationLogDBUpdate
import com.ncs.breeze.common.worker.WorkerNavigationLogFileUpload
import timber.log.Timber
import java.io.File
import java.lang.ref.WeakReference

class NavigationLogUploaderScheduler(app: App) {
    private val appRef = WeakReference(app)

    fun scheduleNavigationLogUpload(data: NavigationLogData) {
        val file = File(data.filePath)
        if (!file.exists()) {
            Timber.d("Navigation log is not exists: ${data.filePath}")
            return
        }
        val lockFile = File("${data.filePath}.lock")
        if (lockFile.exists()) {
            Timber.d("Navigation log has been scheduled to upload: ${data.filePath}")
            return
        }
        lockFile.createNewFile()
        appRef.get()?.let { app ->
            val uploadWorkRequest = OneTimeWorkRequestBuilder<WorkerNavigationLogFileUpload>()
                .setInputData(
                    Data.Builder()
                        .putString(WorkerNavigationLogFileUpload.KEY_FILE_PATH, data.filePath)
                        .putString(WorkerNavigationLogFileUpload.KEY_USER_ID, data.userId)
                        .putString(
                            WorkerNavigationLogFileUpload.KEY_DESCRIPTION,
                            data.description
                        )
                        .build()
                ).setConstraints(
                    Constraints.Builder().setRequiredNetworkType(NetworkType.CONNECTED).build()
                )

                .build()
            WorkManager.getInstance(app)
                .beginWith(uploadWorkRequest)
                .then(OneTimeWorkRequest.Companion.from(WorkerNavigationLogDBUpdate::class.java))
                .enqueue()
        }
    }

    fun uploadPendingHistoryData() {

        val historyDirPath = MapboxNavigationApp.current()?.historyRecorder?.fileDirectory()
            ?.takeIf { it.isNotEmpty() } ?: return

        val historyDir = File(historyDirPath).takeIf { it.exists() && it.isDirectory } ?: return
        if((historyDir.listFiles { file -> file.name.endsWith(".lock") }?.count() ?: 0) > 3){
            return
        }
        val userId =
            appRef.get()?.userPreferenceUtil?.getUserStoredData()?.cognitoID?.takeIf { it.isNotEmpty() }
                ?: "unknown_user"
        historyDir.listFiles()
            ?.filter {
                !File("${it.absolutePath}.lock").exists()
            }?.map { file ->
                NavigationLogData(
                    filePath = file.absolutePath,
                    userId = userId,
                    description = "PendingData"
                )
            }
            ?.take(5)
            ?.forEach {
                scheduleNavigationLogUpload(it)
            }
    }
}
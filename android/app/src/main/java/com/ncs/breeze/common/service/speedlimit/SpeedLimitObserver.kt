package com.ncs.breeze.common.service.speedlimit

import com.mapbox.navigation.base.speed.model.SpeedLimit

interface SpeedLimitObserver {
    fun processSpeedLimit(speedLimit: SpeedLimit?)
}
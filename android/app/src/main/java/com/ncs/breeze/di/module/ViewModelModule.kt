package com.ncs.breeze.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.kar.enes.app.di.annotations.ViewModelKey
import com.ncs.breeze.ui.base.ViewModelFactory
import com.ncs.breeze.ui.dashboard.activity.DashboardViewModel
import com.ncs.breeze.ui.dashboard.fragments.erpdetail.ErpFragmentViewModel
import com.ncs.breeze.ui.dashboard.fragments.exploremap.ExploreMapsFragmentViewModel
import com.ncs.breeze.ui.dashboard.fragments.obulite.OBULiteFragmentViewModel
import com.ncs.breeze.ui.dashboard.fragments.obulite.OBULiteRNViewModel
import com.ncs.breeze.ui.dashboard.fragments.placesave.PlaceSaveFragmentViewModel
import com.ncs.breeze.ui.dashboard.fragments.sharedcollection.SharedCollectionViewModel
import com.ncs.breeze.ui.dashboard.fragments.sharelocation.SharedLocationViewModel
import com.ncs.breeze.ui.dashboard.fragments.traffic.TrafficFragmentViewModel
import com.ncs.breeze.ui.dashboard.fragments.viewmodel.AboutViewModel
import com.ncs.breeze.ui.dashboard.fragments.viewmodel.ChangeEmailViewModel
import com.ncs.breeze.ui.dashboard.fragments.viewmodel.ChangePasswordViewModel
import com.ncs.breeze.ui.dashboard.fragments.viewmodel.ChangeUserNameViewModel
import com.ncs.breeze.ui.dashboard.fragments.viewmodel.DebugViewModel
import com.ncs.breeze.ui.dashboard.fragments.viewmodel.ERPRoutePreviewViewModel
import com.ncs.breeze.ui.dashboard.fragments.viewmodel.FAQViewModel
import com.ncs.breeze.ui.dashboard.fragments.viewmodel.FeedbackViewModel
import com.ncs.breeze.ui.dashboard.fragments.viewmodel.MapDisplayPrefViewModel
import com.ncs.breeze.ui.dashboard.fragments.viewmodel.NewAddressViewModel
import com.ncs.breeze.ui.dashboard.fragments.viewmodel.OBUConnectViewModel
import com.ncs.breeze.ui.dashboard.fragments.viewmodel.OBUPairedListViewModel
import com.ncs.breeze.ui.dashboard.fragments.viewmodel.PrivacyPolicyViewModel
import com.ncs.breeze.ui.dashboard.fragments.viewmodel.ProfileFragmentViewModel
import com.ncs.breeze.ui.dashboard.fragments.viewmodel.RoutePlanningViewModel
import com.ncs.breeze.ui.dashboard.fragments.viewmodel.RoutePrefFragmentViewModel
import com.ncs.breeze.ui.dashboard.fragments.viewmodel.SettingsViewModel
import com.ncs.breeze.ui.dashboard.fragments.viewmodel.TermsAndConditionViewModel
import com.ncs.breeze.ui.dashboard.fragments.viewmodel.TravelLogViewModel
import com.ncs.breeze.ui.dashboard.fragments.viewmodel.TripInfoViewModel
import com.ncs.breeze.ui.login.activity.LoginViewModel
import com.ncs.breeze.ui.login.fragment.forgotPassword.viewmodel.ForgetPasswordEmailFragmentViewModel
import com.ncs.breeze.ui.login.fragment.forgotPassword.viewmodel.ForgotNewPasswordFragmentViewModel
import com.ncs.breeze.ui.login.fragment.forgotPassword.viewmodel.ForgotVerifyOTPViewModel
import com.ncs.breeze.ui.login.fragment.guest.viewmodel.GuestModeSignUpViewModel
import com.ncs.breeze.ui.login.fragment.signIn.viewmodel.LogoutFragmentViewModel
import com.ncs.breeze.ui.login.fragment.signIn.viewmodel.SignInFragmentBreezeViewModel
import com.ncs.breeze.ui.login.fragment.signIn.viewmodel.VerifyMobileViewModel
import com.ncs.breeze.ui.login.fragment.signUp.viewmodel.CreateUserNameFragmentBreezeViewModel
import com.ncs.breeze.ui.login.fragment.signUp.viewmodel.LoginPhoneNumberFragmentBreezeViewModel
import com.ncs.breeze.ui.login.fragment.signUp.viewmodel.SignUpFragmentBreezeViewModel
import com.ncs.breeze.ui.login.fragment.signUp.viewmodel.TermsAndCondnViewModel
import com.ncs.breeze.ui.login.fragment.signUp.viewmodel.VerifyOTPFragmentViewModel
import com.ncs.breeze.ui.navigation.NavigationViewModel
import com.ncs.breeze.ui.splash.PermissionViewModel
import com.ncs.breeze.ui.splash.SplashViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import javax.inject.Singleton

@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(LoginViewModel::class)
    abstract fun bindLoginViewModel(loginViewModel: LoginViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(NavigationViewModel::class)
    abstract fun bindNavigationViewModel(navigationViewModel: NavigationViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(LogoutFragmentViewModel::class)
    abstract fun bindLogoutFragmentViewModel(logoutFragmentViewModel: LogoutFragmentViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(VerifyOTPFragmentViewModel::class)
    abstract fun bindVerifyOTPFragmentViewModel(verifyOTPFragmentViewModel: VerifyOTPFragmentViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ForgetPasswordEmailFragmentViewModel::class)
    abstract fun bindForgetPasswordFragmentViewModel(forgetPasswordEmailFragmentViewModel: ForgetPasswordEmailFragmentViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ForgotNewPasswordFragmentViewModel::class)
    abstract fun bindForgotNewPasswordFragmentViewModel(forgotNewPasswordFragmentViewModel: ForgotNewPasswordFragmentViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ForgotVerifyOTPViewModel::class)
    abstract fun bindForgotVerifyOTPFragmentViewModel(forgotVerifyOTPViewModel: ForgotVerifyOTPViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DashboardViewModel::class)
    abstract fun bindDashboardViewModel(dashboardViewModel: DashboardViewModel): ViewModel


    @Binds
    @IntoMap
    @ViewModelKey(NewAddressViewModel::class)
    abstract fun bindNewAddressViewModel(newAddressViewModel: NewAddressViewModel): ViewModel


    @Binds
    @IntoMap
    @ViewModelKey(SplashViewModel::class)
    abstract fun bindSplashViewModel(splashViewModel: SplashViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PermissionViewModel::class)
    abstract fun bindPermissionViewModel(permissionViewModel: PermissionViewModel): ViewModel


    @Binds
    @IntoMap
    @ViewModelKey(ProfileFragmentViewModel::class)
    abstract fun bindAccountFragmentViewModel(profileFragmentViewModel: ProfileFragmentViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(RoutePlanningViewModel::class)
    abstract fun bindRoutePlanningViewModel(routePlanningViewModel: RoutePlanningViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ERPRoutePreviewViewModel::class)
    abstract fun bindERPRoutePreviewViewModel(erpRoutePreviewViewModel: ERPRoutePreviewViewModel): ViewModel


    @Binds
    @IntoMap
    @ViewModelKey(SettingsViewModel::class)
    abstract fun bindSettingsViewModel(settingsViewModel: SettingsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MapDisplayPrefViewModel::class)
    abstract fun bindMapDisplayPrefViewModel(mapDisplayPrefViewModel: MapDisplayPrefViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(RoutePrefFragmentViewModel::class)
    abstract fun bindRoutePrefFragmentViewModel(routePrefFragmentViewModel: RoutePrefFragmentViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ChangeUserNameViewModel::class)
    abstract fun bindChangeUserNameFragmentViewModel(changeUserNameViewModel: ChangeUserNameViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ChangeEmailViewModel::class)
    abstract fun bindChangeEmailViewModel(changeEmailViewModel: ChangeEmailViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ChangePasswordViewModel::class)
    abstract fun bindChangePasswordViewModel(changePasswordViewModel: ChangePasswordViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PrivacyPolicyViewModel::class)
    abstract fun bindPrivacyPolicyViewModel(privacyPolicyViewModel: PrivacyPolicyViewModel): ViewModel


    @Binds
    @IntoMap
    @ViewModelKey(TermsAndConditionViewModel::class)
    abstract fun bindTermsAndConditionViewModel(termsAndConditionViewModel: TermsAndConditionViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AboutViewModel::class)
    abstract fun bindAboutViewModel(aboutViewModel: AboutViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TravelLogViewModel::class)
    abstract fun bindTravelLogViewModel(travelLogViewModel: TravelLogViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TripInfoViewModel::class)
    abstract fun bindTripInfoViewModel(tripInfoViewModel: TripInfoViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TermsAndCondnViewModel::class)
    abstract fun bindTnCViewModel(termsAndCondnViewModel: TermsAndCondnViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(FAQViewModel::class)
    abstract fun bindFAQViewModel(faqViewModel: FAQViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(VerifyMobileViewModel::class)
    abstract fun bindVerifyMobileViewModel(verifyMobileViewModel: VerifyMobileViewModel): ViewModel


    @Binds
    @IntoMap
    @ViewModelKey(FeedbackViewModel::class)
    abstract fun bindFeedbackViewModel(feedbackViewModel: FeedbackViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DebugViewModel::class)
    abstract fun bindDebugViewModel(debugViewModel: DebugViewModel): ViewModel

    @Binds
    @Singleton
    abstract fun bindViewModelFactory(viewModelFactory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(ErpFragmentViewModel::class)
    abstract fun bindErpDetailFragmentViewModel(erpDetailViewModel: ErpFragmentViewModel): ViewModel


    @Binds
    @IntoMap
    @ViewModelKey(TrafficFragmentViewModel::class)
    abstract fun bindTrafficDetailFragmentViewModel(trafficFragmentViewModel: TrafficFragmentViewModel): ViewModel


    @Binds
    @IntoMap
    @ViewModelKey(PlaceSaveFragmentViewModel::class)
    abstract fun bindPlaceSavedFragmentViewModel(place: PlaceSaveFragmentViewModel): ViewModel


    @Binds
    @IntoMap
    @ViewModelKey(ExploreMapsFragmentViewModel::class)
    abstract fun bindExplorMapsFragmentViewModel(place: ExploreMapsFragmentViewModel): ViewModel


    @Binds
    @IntoMap
    @ViewModelKey(GuestModeSignUpViewModel::class)
    abstract fun bindGuestModeSignUpViewModel(guestModeSignUpViewModel: GuestModeSignUpViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SignInFragmentBreezeViewModel::class)
    abstract fun bindSignInFragmentBreezeViewModel(createUsernameViewModel: SignInFragmentBreezeViewModel): ViewModel


    @Binds
    @IntoMap
    @ViewModelKey(SignUpFragmentBreezeViewModel::class)
    abstract fun bindSignUpFragmentBreezeViewModel(viewModel: SignUpFragmentBreezeViewModel): ViewModel


    @Binds
    @IntoMap
    @ViewModelKey(LoginPhoneNumberFragmentBreezeViewModel::class)
    abstract fun bindLoginPhoneNumberFragmentBreezeViewModel(viewModel: LoginPhoneNumberFragmentBreezeViewModel): ViewModel


    @Binds
    @IntoMap
    @ViewModelKey(CreateUserNameFragmentBreezeViewModel::class)
    abstract fun bindLoginCreateUserNameFragmentBreezeViewModel(viewModel: CreateUserNameFragmentBreezeViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(OBULiteFragmentViewModel::class)
    abstract fun bindOBULiteFragmentViewModel(obuLiteFragmentViewModel: OBULiteFragmentViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(OBUConnectViewModel::class)
    abstract fun bindOBUConnectViewModel(obuConnectViewModel: OBUConnectViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(OBUPairedListViewModel::class)
    abstract fun bindOBUPairedListViewModel(obuPairedListViewModel: OBUPairedListViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(OBULiteRNViewModel::class)
    abstract fun bindOBULiteRNViewModel(obuLiteRNViewModel: OBULiteRNViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SharedLocationViewModel::class)
    abstract fun bindSharedLocationViewModel(viewModel: SharedLocationViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SharedCollectionViewModel::class)
    abstract fun bindSharedCollectionViewModel(viewModel: SharedCollectionViewModel): ViewModel
}
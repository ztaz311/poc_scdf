package com.ncs.breeze.ui.dashboard.fragments.sharedcollection

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.breeze.model.constants.Constants
import com.breeze.model.extensions.dpToPx
import com.facebook.react.bridge.Arguments
import com.facebook.react.bridge.ReadableMap
import com.mapbox.android.gestures.MoveGestureDetector
import com.mapbox.geojson.Point
import com.mapbox.maps.plugin.LocationPuck2D
import com.mapbox.maps.plugin.gestures.OnMoveListener
import com.mapbox.maps.plugin.gestures.gestures
import com.mapbox.maps.plugin.locationcomponent.location
import com.ncs.breeze.R
import com.ncs.breeze.common.constant.RNScreen
import com.ncs.breeze.common.extensions.android.getRNFragmentNavigationParams
import com.ncs.breeze.common.extensions.android.getShareBusinessLogicPackage
import com.ncs.breeze.common.extensions.mapbox.setBreezeDefaultOptions
import com.ncs.breeze.common.remote.MyServiceInterceptor
import com.ncs.breeze.common.rxbus.RxBus
import com.ncs.breeze.common.rxbus.RxEvent
import com.ncs.breeze.common.utils.BreezeUserPreferenceUtil
import com.ncs.breeze.components.layermanager.impl.SharedCollectionLayerManager
import com.ncs.breeze.databinding.FragmentSharedCollectionBinding
import com.ncs.breeze.di.Injectable
import com.ncs.breeze.reactnative.nativemodule.ShareBusinessLogicEvent
import com.ncs.breeze.ui.base.BaseActivity
import com.ncs.breeze.ui.dashboard.AdjustableBottomSheet
import com.ncs.breeze.ui.dashboard.activity.DashboardActivity
import com.ncs.breeze.ui.rn.CustomReactFragment
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject


class SharedCollectionFragment : Fragment(),
    Injectable, AdjustableBottomSheet {

    companion object {
        private const val ARG_COLLECTION_TOKEN = "ARG_COLLECTION_TOKEN"

        fun newInstance(token: String) = SharedCollectionFragment().apply {
            arguments = bundleOf(ARG_COLLECTION_TOKEN to token)
        }
    }

    @Inject
    lateinit var myServiceInterceptor: MyServiceInterceptor

    @Inject
    lateinit var userPreferenceUtil: BreezeUserPreferenceUtil

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel: SharedCollectionViewModel by viewModels { viewModelFactory }

    var compositeDisposable: CompositeDisposable = CompositeDisposable()

    private lateinit var mapCameraHelper: SharedCollectionMapCameraHelper

    private var mapLayerManager: SharedCollectionLayerManager? = null

    private lateinit var viewBinding: FragmentSharedCollectionBinding

    private val onMoveListener = object : OnMoveListener {
        override fun onMoveBegin(detector: MoveGestureDetector) {
            if (!viewBinding.buttonRecenter.isVisible)
                viewBinding.buttonRecenter.isVisible = true
        }

        override fun onMove(detector: MoveGestureDetector): Boolean {
            return false
        }

        override fun onMoveEnd(detector: MoveGestureDetector) {}
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        (context as? DashboardActivity)?.hideKeyboard()
        val callback: OnBackPressedCallback = object : OnBackPressedCallback(
            true // default to enabled
        ) {
            override fun handleOnBackPressed() {
                val currentFragment =
                    childFragmentManager.findFragmentById(R.id.frBottomDetailLayer2)
                if (currentFragment != null) {
                    if (currentFragment.isAdded) {
                        childFragmentManager.beginTransaction().remove(currentFragment).commit()
                    } else {
                        parentFragmentManager.popBackStack()
                    }
                } else {
                    parentFragmentManager.popBackStack()
                }
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(this, callback)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewBinding = FragmentSharedCollectionBinding.inflate(inflater, container, false)
        return viewBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mapCameraHelper = SharedCollectionMapCameraHelper(viewBinding.mapView.getMapboxMap())
        viewBinding.mapView.setBreezeDefaultOptions()
        viewBinding.buttonRecenter.setOnClickListener {
            mapCameraHelper.recenterMap {
                viewBinding.buttonRecenter.isVisible = false
            }
        }

        initMapStyle {
            mapLayerManager = SharedCollectionLayerManager(viewBinding.mapView).apply {
                onToggleIconSelect = { index ->
                    sendSelectedItemToRN(index)
                    viewModel.sharedCollection?.let {
                        viewModel.currentSelectedIndex = index
                    }
                    this@SharedCollectionFragment.updateMapMarkersAndCamera()
                }
            }
            with(viewBinding.mapView) {
                gestures.addOnMoveListener(onMoveListener)
                setBreezeDefaultOptions()
                location.enabled = true
                location.pulsingEnabled = true
                location.locationPuck = LocationPuck2D(
                    ContextCompat.getDrawable(context, R.drawable.cursor)
                )
                getMapboxMap().setCamera(mapCameraHelper.cameraOptions)
            }
            listenRxEvents()
            showRNScreen()
        }

    }

    override fun onDestroyView() {
        super.onDestroyView()
        compositeDisposable.dispose()
    }

    private fun listenRxEvents() {
        compositeDisposable.addAll(
            RxBus.listen(RxEvent.SharedCollectionDataRefresh::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe { onRefreshData(it.locationMapData) },
        )
    }

    private fun onRefreshData(locationMapData: ReadableMap) {
        viewModel.parseRNData(locationMapData)
        updateMapMarkersAndCamera()
    }

    private fun updateMapMarkersAndCamera() {
        val sharedCollection = viewModel.sharedCollection?: return
        mapLayerManager?.refreshMapMarkers(sharedCollection, viewModel.currentSelectedIndex)

        val points = arrayListOf<Point>()
        var selectedPoint: Point? = null
        sharedCollection.bookmarks?.forEachIndexed { index, sharedLocation ->
            val lat = sharedLocation.lat.toDoubleOrNull() ?: return@forEachIndexed
            val long = sharedLocation.long.toDoubleOrNull() ?: return@forEachIndexed
            val point = Point.fromLngLat(long, lat)
            if (index == viewModel.currentSelectedIndex) {
                selectedPoint = point
            }
            points.add(point)
        }
        points.takeIf { it.isNotEmpty() }?.let {
            mapCameraHelper.calculateCenterLocation(it)
            mapCameraHelper.updateCurrentFocusedLocation(selectedPoint)
            mapCameraHelper.recenterMap { }
        }
    }


    private fun initMapStyle(onFinished: () -> Unit = {}) {
        (activity as? BaseActivity<*, *>)?.let { baseActivity ->
            viewBinding.mapView.getMapboxMap().loadStyleUri(baseActivity.getMapboxStyle(true)) {
                onFinished.invoke()
            }
        }
    }

    private fun showRNScreen() {
        val token = requireArguments().getString(ARG_COLLECTION_TOKEN)!!
        val navigationParams = requireActivity().getRNFragmentNavigationParams(
            sessionToken = myServiceInterceptor.getSessionToken(),
            fragmentTag = RNScreen.SHARED_COLLECTION
        ).apply {
            putString("collectionToken", token)
        }
        val initialProperties = Bundle().apply {
            putString(Constants.RN_TO_SCREEN, RNScreen.SHARED_COLLECTION)
            putBundle(Constants.RN_CONSTANTS.NAVIGATION_PARAMS, navigationParams)
        }

        val reactNativeFragment = CustomReactFragment.newInstance(
            componentName = Constants.RN_CONSTANTS.COMPONENT_NAME,
            launchOptions = initialProperties,
            cleanOnDestroyed = false
        )

        childFragmentManager
            .beginTransaction()
            .add(viewBinding.containerRNScreen.id, reactNativeFragment, RNScreen.SHARED_COLLECTION)
            .addToBackStack(RNScreen.SHARED_COLLECTION)
            .commit()
    }

    fun showRouteAndResetToCenter(args: Bundle) {
        (activity as? DashboardActivity)?.apply {
            showRouteAndResetToCenter(args)
        }
    }

    private fun sendSelectedItemToRN(index: Int) {
        context?.getShareBusinessLogicPackage()?.getRequestsModule()?.flatMap {
            val rnData = Arguments.fromBundle(bundleOf("selectedIndex" to index))
            it.callRN(ShareBusinessLogicEvent.SELECTED_INDEX_SHARE_COLLECTION.name, rnData)
        }?.subscribeOn(Schedulers.io())?.observeOn(AndroidSchedulers.mainThread())?.subscribe()
    }

    override fun adjustRecenterButton(height: Int, index: Int, fromScreen: String) {
        Timber.i("adjustRecenterButton")
        lifecycleScope.launch(Dispatchers.Main) {
            mapCameraHelper.updateCameraPadding(bottom = (height + 40.0).dpToPx())
            viewBinding.layoutRecenterContainer.setPadding(0, 0, 0, height.dpToPx())
        }
    }
}

package com.ncs.breeze.components


import android.content.Context
import com.amplifyframework.auth.AuthException
import com.amplifyframework.auth.cognito.exceptions.service.*
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.R


object AmplifyErrorHandler {

    fun getErrorMessage(exception: AuthException, context: Context, type: AuthType): AuthObject {
        Analytics.logExceptionEvent("Observed a login error for: [$type]")

        if (type == AuthType.SIGN_IN) {
            when (exception) {
                is UserNotFoundException -> return AuthObject(
                    context.getString(R.string.error_login_user_not_found),
                    FieldType.DEFAULT
                )
                is UserNotConfirmedException -> return AuthObject(
                    context.getString(R.string.error_login),
                    FieldType.DEFAULT
                )
                is UsernameExistsException -> return AuthObject(
                    context.getString(R.string.error_login),
                    FieldType.DEFAULT
                )
                is AliasExistsException -> return AuthObject(
                    context.getString(R.string.error_login),
                    FieldType.DEFAULT
                )
                is InvalidPasswordException -> return AuthObject(
                    context.getString(R.string.error_login),
                    FieldType.DEFAULT
                )
                is InvalidParameterException -> return AuthObject(
                    context.getString(R.string.error_login),
                    FieldType.DEFAULT
                )
                is CodeExpiredException -> return AuthObject(
                    context.getString(R.string.error_login),
                    FieldType.PASSWORD
                )
                is CodeMismatchException -> return AuthObject(
                    context.getString(R.string.error_otp_expired),
                    FieldType.DEFAULT
                )
                is CodeDeliveryFailureException -> return AuthObject(
                    context.getString(
                        R.string.error_login
                    ), FieldType.DEFAULT
                )
                is LimitExceededException -> return AuthObject(
                    context.getString(R.string.error_otp_retry_attempts_exceeded),
                    FieldType.DEFAULT
                )
                is ResourceNotFoundException -> return AuthObject(
                    context.getString(R.string.error_login),
                    FieldType.DEFAULT
                )
                is FailedAttemptsLimitExceededException -> return AuthObject(
                    context.getString(
                        R.string.error_otp_retry_attempts_exceeded
                    ), FieldType.DEFAULT
                )
                is PasswordResetRequiredException -> return AuthObject(
                    context.getString(
                        R.string.error_login
                    ), FieldType.DEFAULT
                )

                else -> return AuthObject(
                    context.getString(R.string.error_login),
                    FieldType.DEFAULT
                )
            }
        } else if (type == AuthType.SIGN_UP || type == AuthType.CONFIRM_SIGNUP) {
            when (exception) {
                is UserNotFoundException -> return AuthObject(
                    context.getString(R.string.error_create_account_generic),
                    FieldType.DEFAULT
                )
                is UserNotConfirmedException -> return AuthObject(
                    context.getString(R.string.error_create_account_generic),
                    FieldType.DEFAULT
                )
                is UsernameExistsException -> return AuthObject(
                    context.getString(R.string.phone_number_in_use),
                    FieldType.DEFAULT
                )
                is AliasExistsException -> return AuthObject(
                    context.getString(R.string.error_create_account_generic),
                    FieldType.DEFAULT
                )
                is InvalidPasswordException -> return AuthObject(
                    context.getString(R.string.error_password_incorrect_format),
                    FieldType.DEFAULT
                )
                is InvalidParameterException -> return AuthObject(
                    context.getString(R.string.error_create_account_generic),
                    FieldType.DEFAULT
                )
                is CodeExpiredException -> return AuthObject(
                    context.getString(R.string.error_otp_expired),
                    FieldType.DEFAULT
                )
                is CodeMismatchException -> return AuthObject(
                    context.getString(R.string.error_incorrect_otp),
                    FieldType.DEFAULT
                )
                is CodeDeliveryFailureException -> return AuthObject(
                    context.getString(
                        R.string.error_create_account_generic
                    ), FieldType.DEFAULT
                )
                is LimitExceededException -> return AuthObject(
                    context.getString(R.string.error_otp_retry_attempts_exceeded),
                    FieldType.DEFAULT
                )
                is ResourceNotFoundException -> return AuthObject(
                    context.getString(R.string.error_create_account_generic),
                    FieldType.DEFAULT
                )
                is FailedAttemptsLimitExceededException -> return AuthObject(
                    context.getString(
                        R.string.error_otp_retry_attempts_exceeded
                    ), FieldType.DEFAULT
                )
                is PasswordResetRequiredException -> return AuthObject(
                    context.getString(
                        R.string.error_create_account_generic
                    ), FieldType.DEFAULT
                )

                else -> {
                    if (exception.cause is LimitExceededException) {
                        return AuthObject(
                            context.getString(R.string.error_otp_resend_attempts_exceeded),
                            FieldType.DEFAULT
                        )
                    } else {
                        return AuthObject(
                            context.getString(R.string.error_create_account_generic),
                            FieldType.PASSWORD
                        )
                    }
                }
            }
        } else {
            //Forgot Password
            when (exception) {
                is UserNotFoundException -> return AuthObject(
                    context.getString(R.string.error_email_no_match),
                    FieldType.DEFAULT
                )
                is UserNotConfirmedException -> return AuthObject(
                    exception.recoverySuggestion,
                    FieldType.DEFAULT
                )
                is UsernameExistsException -> return AuthObject(
                    context.getString(R.string.error_email_exists),
                    FieldType.DEFAULT
                )
                is AliasExistsException -> return AuthObject(
                    context.getString(R.string.error_forget_pwd_generic),
                    FieldType.DEFAULT
                )
                is InvalidPasswordException -> return AuthObject(
                    context.getString(R.string.error_password_incorrect_format),
                    FieldType.DEFAULT
                )
                is InvalidParameterException -> return AuthObject(
                    context.getString(R.string.error_forget_pwd_generic),
                    FieldType.DEFAULT
                )
                is CodeExpiredException -> return AuthObject(
                    exception.recoverySuggestion,
                    FieldType.DEFAULT
                )
                is CodeMismatchException -> return AuthObject(
                    context.getString(R.string.error_incorrect_otp),
                    FieldType.DEFAULT
                )
                is CodeDeliveryFailureException -> return AuthObject(
                    exception.recoverySuggestion,
                    FieldType.DEFAULT
                )
                is LimitExceededException -> return AuthObject(
                    context.getString(R.string.error_otp_retry_attempts_exceeded),
                    FieldType.DEFAULT
                )
                is ResourceNotFoundException -> return AuthObject(
                    context.getString(R.string.error_forget_pwd_generic),
                    FieldType.DEFAULT
                )
                is FailedAttemptsLimitExceededException -> return AuthObject(
                    context.getString(
                        R.string.error_otp_retry_attempts_exceeded
                    ), FieldType.DEFAULT
                )
                is PasswordResetRequiredException -> return AuthObject(
                    context.getString(
                        R.string.error_forget_pwd_generic
                    ), FieldType.DEFAULT
                )

                else -> {
                    if (exception.cause is LimitExceededException) {
                        return AuthObject(
                            context.getString(R.string.error_otp_resend_attempts_exceeded),
                            FieldType.DEFAULT
                        )
                    } else if (exception.cause is CodeMismatchException) {
                        return AuthObject(
                            context.getString(R.string.incorrect_otp_there),
                            FieldType.DEFAULT
                        )
                    } else {
                        return AuthObject(
                            context.getString(R.string.error_forget_pwd_generic),
                            FieldType.DEFAULT
                        )
                    }
                }
            }
        }
    }

    enum class AuthType {
        SIGN_IN, SIGN_UP, CONFIRM_SIGNUP, FORGOT_PASSWORD
    }

    enum class FieldType {
        USERNAME, PASSWORD, EMAIL, DEFAULT
    }

    class AuthObject {
        var errorMessage: String
        var fieldType: FieldType

        constructor(errorMessage: String, fieldType: FieldType) {
            this.errorMessage = errorMessage
            this.fieldType = fieldType
        }
    }

}


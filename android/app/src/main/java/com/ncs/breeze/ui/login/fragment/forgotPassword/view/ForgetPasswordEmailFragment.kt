package com.ncs.breeze.ui.login.fragment.forgotPassword.view

import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.ncs.breeze.R
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.Event
import com.ncs.breeze.common.analytics.Screen
import com.breeze.model.constants.Constants
import com.ncs.breeze.common.utils.DialogFactory
import com.ncs.breeze.common.utils.Variables
import com.breeze.customization.view.BreezeEditText
import com.ncs.breeze.databinding.FragmentForgetPasswordEmailBinding
import com.ncs.breeze.ui.base.BaseActivity
import com.ncs.breeze.ui.base.BaseFragment
import com.ncs.breeze.ui.login.fragment.forgotPassword.viewmodel.ForgetPasswordEmailFragmentViewModel
import javax.inject.Inject

class ForgetPasswordEmailFragment :
    BaseFragment<FragmentForgetPasswordEmailBinding, ForgetPasswordEmailFragmentViewModel>() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val viewModel: ForgetPasswordEmailFragmentViewModel by viewModels {
        viewModelFactory
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        initView()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }

    companion object {
        @JvmStatic
        fun newInstance() =
            ForgetPasswordEmailFragment().apply {
                arguments = Bundle().apply {

                }
            }
    }

    override fun inflateViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?,
        attachToContainer: Boolean
    ) = FragmentForgetPasswordEmailBinding.inflate(inflater, container, attachToContainer)

    override fun getViewModelReference(): ForgetPasswordEmailFragmentViewModel {
        return viewModel
    }

    private fun initView() {

        viewBinding.editEmail.getEditText()
            .setOnEditorActionListener(TextView.OnEditorActionListener { v, actionId, event ->
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    if (checkValidEmail()) {
                        if (Variables.isNetworkConnected) {
                            forgetPassword(viewBinding.editEmail.getEnteredText()!!)
                        } else {
                            DialogFactory.internetDialog(
                                context,
                            ) { dialogInterface, i -> }!!.show()
                        }
                        return@OnEditorActionListener true
                    } else {
                        return@OnEditorActionListener false
                    }
                }
                false
            })
        viewBinding.editEmail.setEditTextListener(object : BreezeEditText.BreezeEditTextListener {
            override fun onBreezeEditTextFocusChange(isFocused: Boolean) {
                if (!isFocused) {
                    Analytics.logEditEvent(Event.EMAIL, getScreenName())
                }
            }
        })

        clickListeners()
        viewBinding.btForgetemailNext.setBGGrey()
        viewBinding.editEmail.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                setNormalBg()
            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            }

            override fun afterTextChanged(s: Editable) {
//                AnalyticsTracker.logEvent(Event.ENTER_EMAIL, AnalyticsUtil.actionEditField(Event.EDIT_FORGOT_PASSWORD_EMAIL), Screen.FORGOT_PASSWORD_STEP1)
            }
        })
    }

    private fun setNormalBg() {
        if (viewBinding.editEmail.getEnteredText()!!.length > 1
        ) {
            viewBinding.btForgetemailNext.setBGNormal()
        } else {
            viewBinding.btForgetemailNext.setBGGrey()

        }
    }

    private fun forgetPassword(username: String) {
        viewModel.resetPasswordSuccessful.observe(viewLifecycleOwner, Observer {
            viewModel.resetPasswordSuccessful.removeObservers(viewLifecycleOwner)
            val bundle = Bundle()
            bundle.putString(Constants.KEYS.KEY_EMAIL, viewBinding.editEmail.getEnteredText())
            (activity as BaseActivity<*, *>).addFragment(
                ForgotNewPasswordFragment.newInstance(),
                bundle,
                Constants.TAGS.FORGET_PASSWORD_TAG
            )
        })
        viewModel.resetPasswordFailed.observe(viewLifecycleOwner, Observer {
            viewModel.resetPasswordFailed.removeObservers(viewLifecycleOwner)
            viewBinding.editEmail.showErrorView(it.errorMessage)
        })
        viewModel.resetPassword(username.toLowerCase())
    }

    fun checkValidEmail(): Boolean {
        var isValid = true
        if (TextUtils.isEmpty(viewBinding.editEmail.getEnteredText())) {
            isValid = false
            viewBinding.editEmail.setBGError()
        } else {
            if (viewBinding.editEmail.getEnteredText()
                    ?.matches(Regex(Constants.REGEX.EMAIL_REGEX)) == false
            ) {
                isValid = false
                viewBinding.editEmail.showErrorView(resources.getString(R.string.error_account_email_invalid))
            }
        }

        return isValid
    }

    private fun clickListeners() {
        viewBinding.backButton.setOnClickListener {
            onBackPressed()
        }
        viewBinding.btForgetemailNext.setOnClickListener {
            Analytics.logClickEvent(Event.NEXT, getScreenName())
            if (checkValidEmail()) {
                if (Variables.isNetworkConnected) {
                    forgetPassword(viewBinding.editEmail.getEnteredText()!!)
                } else {
                    DialogFactory.internetDialog(
                        context,
                    ) { dialogInterface, i -> }!!.show()
                }
            }
        }
    }

    override fun onKeyboardVisibilityChanged(isOpen: Boolean) {
        if (isOpen) {
            viewBinding.btForgetemailNext.visibility = View.GONE
        } else {
            viewBinding.btForgetemailNext.visibility = View.VISIBLE
        }
    }

    override fun getScreenName(): String {
        return Screen.FORGOT_PASSWORD_STEP1
    }
}
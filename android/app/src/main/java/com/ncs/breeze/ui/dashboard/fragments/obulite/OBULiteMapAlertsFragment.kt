package com.ncs.breeze.ui.dashboard.fragments.obulite

import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import com.breeze.model.NavigationZone
import com.breeze.model.extensions.round
import com.breeze.model.extensions.visibleAmount
import com.breeze.model.obu.OBUParkingAvailability
import com.breeze.model.obu.OBURoadEventData
import com.breeze.model.obu.OBUTravelTimeData
import com.facebook.react.bridge.Arguments
import com.ncs.breeze.R
import com.ncs.breeze.common.constant.OBUConstants
import com.ncs.breeze.common.extensions.android.getApp
import com.ncs.breeze.common.extensions.android.getBreezeUserPreference
import com.ncs.breeze.common.helper.obu.OBUDataHelper
import com.ncs.breeze.common.rxbus.RxBus
import com.ncs.breeze.common.rxbus.RxEvent
import com.ncs.breeze.common.service.HorizonObserverFillterZoneUtils
import com.ncs.breeze.common.storage.BreezeUserPreference
import com.ncs.breeze.common.utils.ZoneUIUtils
import com.ncs.breeze.databinding.FragmentObuLiteMapViewAlertsBinding
import com.ncs.breeze.reactnative.nativemodule.ShareBusinessLogicEvent
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import timber.log.Timber

class OBULiteMapAlertsFragment : Fragment() {
    private lateinit var viewBinding: FragmentObuLiteMapViewAlertsBinding
    private val compositeDisposable = CompositeDisposable()
    private var lastDelayJob: Job? = null
    private var isDestroyed = false

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewBinding = FragmentObuLiteMapViewAlertsBinding.inflate(inflater, container, false)
        return viewBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onStart() {
        super.onStart()
        listenOBUEvents()
    }

    override fun onStop() {
        super.onStop()
        compositeDisposable.clear()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        isDestroyed = true
    }

    private fun listenOBUEvents() {
        compositeDisposable.add(
            RxBus.listen(RxEvent.OBURoadEvent::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    handleOBURoadEvent(it.eventData)
                })
        compositeDisposable.add(
            RxBus.listen(RxEvent.OBULowCardBalanceEvent::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    showOBULowCardAlertMessage(it.balance / 100.0)
                })
        compositeDisposable.add(
            RxBus.listen(RxEvent.OBUParkingAvailabilityEvent::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { event ->
                    if (context?.getBreezeUserPreference()?.getParkingAvailabilityDisplay() != true
                        || event.data.isEmpty()
                    )
                        return@subscribe
                    showOBUCarParkAvailabilityScreenRN(event.data)
                })
        compositeDisposable.add(
            RxBus.listen(RxEvent.OBUTravelTimeEvent::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { event ->
                    if (context?.getBreezeUserPreference()?.getEstimatedTravelTimeDisplay() != true
                        || event.data.isEmpty()
                    )
                        return@subscribe
                    showOBUTravelTimeScreenRN(event.data)
                })
    }

    private fun showOBUTravelTimeScreenRN(eventData: Array<OBUTravelTimeData>) {
        val travelDisplay = BreezeUserPreference.getInstance(requireContext().applicationContext)
            .getEstimatedTravelTimeDisplay()
        if (!travelDisplay) return
        RxBus.publish(RxEvent.CloseCarparkListScreen())
        activity?.getApp()?.let { app ->
            compositeDisposable.add(app.shareBusinessLogicPackage.getRequestsModule().flatMap {
                it.callRN(
                    ShareBusinessLogicEvent.OPEN_OBU_TRAVEL_TIME.eventName,
                    Arguments.createMap().apply {
                        putArray(
                            "data",
                            Arguments.fromArray(
                                eventData.map { item -> item.toBundle() }.toTypedArray()
                            )
                        )
                    }
                )
            }.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe())
        }
    }

    private fun showOBUCarParkAvailabilityScreenRN(carParkData: Array<OBUParkingAvailability>) {
        RxBus.publish(RxEvent.CloseCarparkListScreen())
        getApp()?.shareBusinessLogicPackage?.getRequestsModule()?.flatMap {
            // Then we can perform our action.
            it.callRN(
                ShareBusinessLogicEvent.OPEN_OBU_CAR_PARK_AVAILABILITY.eventName,
                Arguments.createMap().apply {
                    putArray(
                        "carparks", Arguments.fromArray(
                            carParkData.map { item -> item.toBundle() }.toTypedArray()
                        )
                    )
                }
            )
        }?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe(
                { result ->
                    Timber.d("OBULiteMapAlertsFragment showOBUCarParkAvailability: $result")
                },
                { error -> Timber.d("OBULiteMapAlertsFragment showOBUCarParkAvailability failed: $error") }
            )?.also { compositeDisposable.add(it) }
    }

    private fun handleOBURoadEvent(eventData: OBURoadEventData) {
        RxBus.publish(RxEvent.CloseCarparkListScreen())
        var showAlert = true
        var backgroundColor = 0
        var iconResourceId = 0
        var title = ""
        var detail = ""
        var fee = ""
        var statusIndicator = 0
        when (eventData.eventType) {
            OBURoadEventData.EVENT_TYPE_ERP_CHARGING -> {
                showAlert = false
                viewBinding.queueAlertView.addOBUAlertERPView(eventData)
            }

            OBURoadEventData.EVENT_TYPE_ERP_SUCCESS -> {
                showAlert = false
                viewBinding.queueAlertView.addOBUAlertERPView(eventData)
                delayCheckShowingOBULowCardAlertMessage()
            }

            OBURoadEventData.EVENT_TYPE_ERP_FAILURE -> {
                showAlert = false
                viewBinding.queueAlertView.addOBUAlertERPView(eventData)
            }

            OBURoadEventData.EVENT_TYPE_PARKING_FEE -> {
                backgroundColor = Color.parseColor("#782EB1")
                iconResourceId = R.drawable.ic_obu_alert_message_parking_success
                title = eventData.message ?: ""
                fee = eventData.chargeAmount ?: ""
            }

            OBURoadEventData.EVENT_TYPE_PARKING_SUCCESS -> {
                backgroundColor = Color.parseColor("#782EB1")
                iconResourceId = R.drawable.ic_obu_alert_message_parking_success
                title = eventData.message ?: ""
                fee = eventData.chargeAmount ?: ""
                statusIndicator =
                    R.drawable.ic_obu_alert_message_status_indicator_success
                delayCheckShowingOBULowCardAlertMessage()
            }

            OBURoadEventData.EVENT_TYPE_PARKING_FAILURE -> {
                backgroundColor = Color.parseColor("#E82370")
                iconResourceId = R.drawable.ic_obu_alert_message_parking_failure
                title = eventData.message ?: ""
                fee = eventData.chargeAmount ?: ""
                statusIndicator =
                    R.drawable.ic_obu_alert_message_status_indicator_failure
            }

            OBURoadEventData.EVENT_TYPE_SCHOOL_ZONE -> {
                showAlert = false
                if (activity?.getBreezeUserPreference()?.getSchoolZoneDisplay() == true) {
                    showOBUNotificationView(
                        NavigationZone.SCHOOL_ZONE.priority,
                        eventData.distance
                    )
                }
            }

            OBURoadEventData.EVENT_TYPE_TRAFFIC -> {
                showAlert = false
                showOBUNotificationView(
                    NavigationZone.ACCIDENT.priority,
                    eventData.distance
                )
            }

            OBURoadEventData.EVENT_TYPE_SILVER_ZONE -> {
                showAlert = false
                if (activity?.getBreezeUserPreference()?.getSilverZoneDisplay() == true) {
                    showOBUNotificationView(
                        NavigationZone.SILVER_ZONE.priority,
                        eventData.distance
                    )
                }
            }

            OBURoadEventData.EVENT_TYPE_FLASH_FLOOD -> {
                showAlert = false
                showOBUNotificationView(
                    NavigationZone.FLASH_FLOOD.priority,
                    eventData.distance,
                    eventData.detailMessage ?: ""
                )
            }

            OBURoadEventData.EVENT_TYPE_HEAVY_TRAFFIC -> {
                showAlert = false
                showOBUNotificationView(
                    NavigationZone.HEAVY_TRAFFIC.priority,
                    eventData.distance,
                    eventData.detailMessage ?: ""
                )
            }

            OBURoadEventData.EVENT_TYPE_UNATTENDED_VEHICLE -> {
                showAlert = false
                showOBUNotificationView(
                    NavigationZone.UNATTENDED_VEHICLE.priority,
                    eventData.distance,
                    eventData.detailMessage ?: ""
                )
            }

            OBURoadEventData.EVENT_TYPE_MISCELLANEOUS -> {
                showAlert = false
                showOBUNotificationView(
                    NavigationZone.MISCELLANEOUS.priority,
                    eventData.distance,
                    eventData.detailMessage ?: ""
                )
            }

            OBURoadEventData.EVENT_TYPE_ROAD_BLOCK -> {
                showAlert = false
                showOBUNotificationView(
                    NavigationZone.ROAD_BLOCK.priority,
                    eventData.distance,
                    eventData.detailMessage ?: ""
                )
            }

            OBURoadEventData.EVENT_TYPE_OBSTACLE -> {
                showAlert = false
                showOBUNotificationView(
                    NavigationZone.OBSTACLE.priority,
                    eventData.distance,
                    eventData.detailMessage ?: ""
                )
            }

            OBURoadEventData.EVENT_TYPE_ROAD_WORK -> {
                showAlert = false
                showOBUNotificationView(
                    NavigationZone.ROAD_WORK.priority,
                    eventData.distance,
                    eventData.detailMessage ?: ""
                )
            }

            OBURoadEventData.EVENT_TYPE_VEHICLE_BREAKDOWN -> {
                showAlert = false
                showOBUNotificationView(
                    NavigationZone.VEHICLE_BREAKDOWN.priority,
                    eventData.distance,
                    eventData.detailMessage ?: ""
                )
            }

            OBURoadEventData.EVENT_TYPE_MAJOR_ACCIDENT -> {
                showAlert = false
                showOBUNotificationView(
                    NavigationZone.MAJOR_ACCIDENT.priority,
                    eventData.distance,
                    eventData.detailMessage ?: ""
                )
            }

            OBURoadEventData.EVENT_TYPE_SEASON_PARKING -> {
                showAlert = false
                showOBUNotificationView(
                    NavigationZone.SEASON_PARKING.priority,
                    eventData.distance,
                    eventData.detailMessage ?: ""
                )
            }

            OBURoadEventData.EVENT_TYPE_TREE_PRUNING -> {
                showAlert = false
                showOBUNotificationView(
                    NavigationZone.TREE_PRUNING.priority,
                    eventData.distance,
                    eventData.detailMessage ?: ""
                )
            }

            OBURoadEventData.EVENT_TYPE_YELLOW_DENGUE_ZONE -> {
                showAlert = false
                showOBUNotificationView(
                    NavigationZone.YELLOW_DENGUE_ZONE.priority,
                    eventData.distance,
                    eventData.detailMessage ?: ""
                )
            }

            OBURoadEventData.EVENT_TYPE_RED_DENGUE_ZONE -> {
                showAlert = false
                showOBUNotificationView(
                    NavigationZone.RED_DENGUE_ZONE.priority,
                    eventData.distance,
                    eventData.detailMessage ?: ""
                )
            }

            OBURoadEventData.EVENT_TYPE_EVENT,
            OBURoadEventData.EVENT_TYPE_ROAD_CLOSURE -> {
                showAlert = false
                showOBUNotificationView(
                    NavigationZone.ROAD_CLOSURE.priority,
                    eventData.distance,
                    eventData.detailMessage ?: ""
                )
            }

            OBURoadEventData.EVENT_TYPE_ROAD_DIVERSION -> {
                showAlert = false
                showOBUNotificationView(
                    NavigationZone.ROAD_DIVERSION.priority,
                    eventData.message,
                    eventData.detailMessage ?: ""
                )
            }

            OBURoadEventData.EVENT_TYPE_SPEED_CAMERA -> {
                showAlert = false
                showOBUNotificationView(
                    NavigationZone.SPEED_CAMERA.priority,
                    eventData.distance
                )
            }

            OBURoadEventData.EVENT_TYPE_GENERAL_MESSAGE -> {
                showAlert = false
                showOBUAlertMessageGeneral(
                    eventData.message ?: "",
                    eventData.detailMessage ?: ""
                )
            }

            OBURoadEventData.EVENT_TYPE_BUS_LANE -> {
                showAlert = false
                if (activity?.getBreezeUserPreference()?.getBusLaneDisplay() == true) {
                    showOBUNotificationView(
                        NavigationZone.BUS_LANE.priority,
                        eventData.message ?: "",
                        eventData.detailMessage ?: "",
                        10000
                    )
                }
            }

            else -> {
                showAlert = false
            }
        }
        if (showAlert) {
            showOBUAlertMessage(
                backgroundColor,
                iconResourceId,
                title,
                detail,
                fee,
                statusIndicator
            )
        }
    }

    private fun delayCheckShowingOBULowCardAlertMessage() {
        lifecycleScope.launch(Dispatchers.IO) {
            // fixme: temporarily add 1 second to make sure low card alerts are shown
            delay(OBUConstants.OBU_LOW_CARD_ALERT_DELAY + 1000)
            OBUDataHelper.getOBUCardBalanceSGD()
                ?.takeIf { isActive && it < OBUConstants.OBU_LOW_CARD_THRESHOLD / 100.0 }
                ?.let { showOBULowCardAlertMessage(it) }
        }
    }

    private fun showOBULowCardAlertMessage(balanceSGD: Double) {
        RxBus.publish(RxEvent.CloseCarparkListScreen())
        if (lifecycle.currentState.isAtLeast(Lifecycle.State.RESUMED)) {
            lifecycleScope.launch(Dispatchers.Main) {
                showOBUAlertMessage(
                    Color.parseColor("#E82370"),
                    R.drawable.ic_navigation_obu_connection_error_explain,
                    "Low Card Balance",
                    "",
                    "$${balanceSGD.round(2)}",
                    R.drawable.ic_obu_alert_message_status_indicator_failure
                )
            }
        }
    }

    private fun showOBUAlertMessageGeneral(
        message: String,
        detailMessage: String,
        delayInMillis: Long = 5000
    ) {
        with(viewBinding) {
            if (obuAlertMessageGeneral.isVisible) return
            tvOBUGeneralMessage.text = message
            tvOBUGeneralDetailMessage.text = detailMessage
            obuAlertMessageGeneral.isVisible = true
            Handler(android.os.Looper.getMainLooper()).postDelayed(
                { obuAlertMessageGeneral.isVisible = false }, delayInMillis
            )
        }
    }

    private fun showOBUAlertMessage(
        backgroundColor: Int,
        iconResourceId: Int,
        title: String,
        detail: String,
        fee: String,
        statusIndicator: Int
    ) {
        viewBinding.obuAlertMessageView.setContent(
            backgroundColor,
            iconResourceId,
            title,
            detail,
            fee,
            statusIndicator
        )
        viewBinding.obuAlertMessageView.show()
    }

    private fun showOBUNotificationView(
        navigationZonePriority: Int,
        distance: String?,
        message: String = "",
        delayInMillis: Long = 3000
    ) {

        viewBinding.notificationCruiseMode.background =
            HorizonObserverFillterZoneUtils.getCruiseNotificationBG(
                ZoneUIUtils.getZoneTypeBGColor(navigationZonePriority), requireContext()
            )
        viewBinding.textDistance.text = distance
        viewBinding.textZoneType.text =
            message.ifBlank { getString(ZoneUIUtils.getZoneTypeString(navigationZonePriority)) }
        viewBinding.imgZone.setImageResource(ZoneUIUtils.getZoneTypeImage(navigationZonePriority))
        viewBinding.textPrice.text = ""
        viewBinding.textPrice.visibility = View.GONE

        startShowNotification(delayInMillis)
    }

    private fun startShowNotification(delayInMillis: Long = 3000) {
        viewBinding.notificationCruiseMode.visibility = View.VISIBLE

        if (lastDelayJob != null) {
            lastDelayJob?.cancel()
        }
        lastDelayJob = lifecycleScope.launch(Dispatchers.Main) {
            delay(delayInMillis)
            viewBinding.notificationCruiseMode.isVisible = false
        }
    }

    companion object {
        const val TAG = "OBULiteMapAlertsFragment"
    }

    fun showNotificationViewCruise(
        zone: NavigationZone,
        distance: Int,
        mERPName: String?,
        chargeAmount: String?,
        location: String?
    ) {
        if (isFragmentDestroyed()) return

        filterZone(zone, distance, zone, location, mERPName, chargeAmount)


        /**
         * show cruise mode notification
         */
        startShowNotification()
    }

    private fun filterZone(
        currentZone: NavigationZone,
        distance: Int,
        zone: NavigationZone,
        location: String?,
        mERPName: String?,
        chargeAmount: String?
    ) {
        HorizonObserverFillterZoneUtils.filterZone(currentZone, distance).let { data ->
            activity?.let { act ->
                viewBinding.notificationCruiseMode.background =
                    HorizonObserverFillterZoneUtils.getCruiseNotificationBG(
                        ZoneUIUtils.getZoneTypeBGColor(
                            data.zonePriority
                        ), act
                    )
                viewBinding.textDistance.text =
                    if (zone.maxDistanceMtr <= 300) data.mDistance else location ?: data.mDistance
                if (currentZone == NavigationZone.ERP_ZONE) {
                    viewBinding.textZoneType.text = mERPName
                } else {
                    viewBinding.textZoneType.text =
                        getString(ZoneUIUtils.getZoneTypeString(currentZone.priority))
                }
                viewBinding.imgZone.setImageResource(ZoneUIUtils.getZoneTypeImage(data.zonePriority))
                viewBinding.textPrice.text = chargeAmount?.toDouble()?.visibleAmount()
                viewBinding.textPrice.visibility = if (data.mIsShowAmount) {
                    View.VISIBLE
                } else {
                    View.GONE
                }
            }
        }
    }

    fun isFragmentDestroyed(): Boolean {
        return (isDestroyed || this.activity == null || this.view == null)
    }
}
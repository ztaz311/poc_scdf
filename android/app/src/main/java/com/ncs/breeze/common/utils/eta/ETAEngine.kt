package com.ncs.breeze.common.utils.eta

import android.content.Context
import android.location.Location
import com.google.gson.JsonElement
import com.mapbox.api.directions.v5.models.DirectionsRoute
import com.ncs.breeze.common.ETAStatus
import com.ncs.breeze.common.appSync.AppSyncHelper
import com.ncs.breeze.common.model.rx.ETAMessageReceivedCar
import com.ncs.breeze.common.model.rx.ETAMessageReceivedPhone
import com.ncs.breeze.common.model.rx.ETAStartedCar
import com.ncs.breeze.common.model.rx.ETAStartedPhone
import com.ncs.breeze.common.event.ToAppRx
import com.ncs.breeze.common.event.ToCarRx
import com.ncs.breeze.common.model.rx.UpdateETACar
import com.ncs.breeze.common.model.rx.UpdateETAPhone
import com.breeze.model.DestinationAddressDetails
import com.breeze.model.api.ErrorData
import com.breeze.model.api.request.ETARequest
import com.breeze.model.api.request.ETAStatusRequest
import com.breeze.model.api.response.ETAResponse
import com.breeze.model.api.response.eta.RecentETAContact
import com.ncs.breeze.common.remote.ApiHelper
import com.ncs.breeze.common.remote.ApiObserver
import com.ncs.breeze.common.rxbus.RxBus
import com.ncs.breeze.common.rxbus.RxEvent
import com.breeze.model.constants.Constants
import com.breeze.model.enums.ETAMode
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

class ETAEngine() {

    private var etaRequest: ETARequest? = null
    private var triggerETAMessage_5Min: Boolean = false
    private var triggerETAMessage_15Min: Boolean = false
    private var etaStatus: ETAStatus? = null
    private var context: Context? = null // FixME - use weak reference here
    lateinit var apiHelper: ApiHelper

    var currentLocation: Location? = null

    //Singleton location observer instance to be used by both android auto and car app
    private val cruiseLocationUpdateInstance = ETALocationObserver(ETAMode.Cruise, this)
    private val navigationLocationUpdateInstance = ETALocationObserver(ETAMode.Navigation, this)

    var compositeDisposable: CompositeDisposable = CompositeDisposable()

    fun sendETAMessage(remainingTimeInSeconds: Double) {
        if (isETAEnabled()) {
            if (remainingTimeInSeconds < Constants.TRIPETA.MESSAGE_INTERVAL_5) {
                if (triggerETAMessage_5Min) {
                    triggerETAMessage_5Min = false
                    if (isETARunning()) {
                        sendETAMessage(
                            ETARequest(
                                tripEtaUUID = etaRequest!!.tripEtaUUID,
                                type = Constants.TRIPETA.REMINDER_1
                            )
                        )
                    }
                }
            } else if (remainingTimeInSeconds < Constants.TRIPETA.MESSAGE_INTERVAL_15) {
                if (triggerETAMessage_15Min) {
                    triggerETAMessage_15Min = false
                }
            }
        }
    }

    fun createNewETARequest(
        etaMode: ETAMode,
        favETAItem: RecentETAContact,
        navDestination: DestinationAddressDetails? = null,
        etaMessage: String
    ): ETARequest {
        return ETARequest().apply {
            type = Constants.TRIPETA.INIT
            tripEtaFavouriteId = favETAItem.tripEtaFavouriteId
            shareLiveLocation = Constants.TRIPETA.YES
            voiceCallToRecipient = Constants.TRIPETA.NO
            tripStartTime = System.currentTimeMillis() / 1000
            tripEndTime = null
            recipientName = favETAItem.recipientName
            recipientNumber = favETAItem.recipientNumber
            message = etaMessage

            // Extra data in case Navigation mode
            if (etaMode == ETAMode.Navigation || etaMode == ETAMode.RoutePlanning) {
                if (navDestination != null) {
                    destination = navDestination!!.address1
                    tripDestLat = navDestination!!.lat!!.toDouble()
                    tripDestLong = navDestination!!.long!!.toDouble()
                }
            }
        }
    }


    fun getEtaLocationUpdateInstance(mode: ETAMode): ETALocationObserver {
        return if (mode == ETAMode.Cruise) {
            cruiseLocationUpdateInstance
        } else {
            navigationLocationUpdateInstance
        }
    }


    private fun sendETAMessage(request: ETARequest) {
        apiHelper.sendETAMessage(request)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : ApiObserver<ETAResponse>(compositeDisposable) {
                override fun onSuccess(data: ETAResponse) {
                    Timber.d("ETA API Successful")
                    if (etaRequest!!.tripEtaUUID == null) {
                        etaRequest!!.tripEtaUUID = data.tripEtaUUID
                        AppSyncHelper.subscribeToETAMessage(etaRequest!!.tripEtaUUID!!, context!!)
                        ToAppRx.postEvent(ETAStartedPhone())
                        ToCarRx.postEvent(ETAStartedCar(etaRequest?.recipientName!!))
                    }
                }

                override fun onError(e: ErrorData) {
                    Timber.d("API failed: " + e.toString())
                }
            })
    }

    fun isLiveLocationSharingEnabled(): Boolean {
        return isETAEnabled() && etaRequest!!.shareLiveLocation.equals(Constants.TRIPETA.YES)
    }

    fun isETARunning(): Boolean {
        return isETAEnabled() && ETAStatus.INPROGRESS == etaStatus
    }

    fun isETAPause(): Boolean {
        return isETAEnabled() && ETAStatus.PAUSE == etaStatus
    }

    fun isETADisabled(): Boolean {
        return !isETAEnabled()
    }


    fun toggleLiveLocation() {
        if (isETAEnabled()) {
            val isETARunning = if (etaStatus == ETAStatus.INPROGRESS) {
                updateETAStatus(ETAStatus.PAUSE)
                false
            } else {
                updateETAStatus(ETAStatus.INPROGRESS)
                true
            }
            ToAppRx.postEvent(UpdateETAPhone())
            ToCarRx.postEvent(
                UpdateETACar(
                    isETARunning,
                    etaRequest!!.recipientName!!
                )
            )
        }
    }

    fun updateLocation(location: Location?) {
        if (location != null) {
            currentLocation = location
            if (!isETAPause()) {
                publishLiveLocation()
            }
        }
    }

    fun updateLocationForCruise(location: Location?) {
        if (location != null) {
            currentLocation = location
            if (etaStatus == ETAStatus.INPROGRESS) {
                publishLiveLocation()
            }
            if (isETAEnabled() && currentLocation != null && etaStatus != null) {
                if (etaRequest != null && etaRequest?.tripDestLat != null && etaRequest?.tripDestLong != null) {
                    var destinationLocation = Location("")
                    destinationLocation.latitude = etaRequest?.tripDestLat!!
                    destinationLocation.longitude = etaRequest?.tripDestLong!!
                    if (currentLocation!!.distanceTo(destinationLocation) <= 200 && triggerETAMessage_5Min) {
                        // Send 1 minute message
                        triggerETAMessage_5Min = false
                        var request = ETARequest()
                        request.tripEtaUUID = etaRequest!!.tripEtaUUID
                        request.type = Constants.TRIPETA.REMINDER_1
                        sendETAMessage(request)
                    }
                }
            }
        }
    }

    fun updateETAStatus(status: ETAStatus) {
        etaStatus = status
        publishLiveLocation()
        triggerStatusChangeAPI()
    }

    fun publishLiveLocation() {
        if (isETAEnabled() && currentLocation != null && etaStatus != null) {
            AppSyncHelper.publishLiveLocationData(
                context = context!!,
                tripId = etaRequest!!.tripEtaUUID!!,
                latitude = currentLocation!!.latitude,
                longitude = currentLocation!!.longitude,
                status = etaStatus!!.type,
                arrivalTime = etaRequest!!.arrivalTime,
                coordinates = etaRequest!!.coordinates
            )
        }
    }

    private fun triggerStatusChangeAPI() {
        if (isETAEnabled() && currentLocation != null && etaStatus != null) {
            var request = ETAStatusRequest(etaRequest!!.tripEtaUUID!!, etaStatus!!.type)
            apiHelper.updateETATripStatus(request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : ApiObserver<JsonElement>(compositeDisposable) {
                    override fun onSuccess(data: JsonElement) {
                        Timber.d("Update ETA status API Successful")
                    }

                    override fun onError(e: ErrorData) {
                        Timber.d("API failed: " + e.toString())
                    }
                })
        }
    }

    fun isETAEnabled(): Boolean {
        return etaRequest != null && !etaRequest!!.tripEtaUUID.isNullOrBlank()
    }

    fun getETARecipientName(): String? {
        return etaRequest!!.recipientName
    }


    /**
     * to update arrival time everyTime
     */
    fun updateArrivalTime(arrivalTime: String = "") {
        etaRequest?.arrivalTime = arrivalTime
    }

    /**
     * to update coordinates everyTime
     */
    fun updateCoordinates(coordinates: String = "") {
        etaRequest?.coordinates = coordinates
    }

    fun startETA(route: DirectionsRoute?) {

        if (route != null) {
            if (isETAEnabled()) {
                triggerETAMessage_15Min =
                    route.duration() >= Constants.TRIPETA.MESSAGE_INTERVAL_15
                triggerETAMessage_5Min =
                    route.duration() >= Constants.TRIPETA.MESSAGE_INTERVAL_5
            }
        } else {
            triggerETAMessage_5Min = true
        }

        compositeDisposable.add(
            RxBus.listen(RxEvent.ETAMessage::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    ToAppRx.postEvent(ETAMessageReceivedPhone(it.message))
                    ToCarRx.postEvent(ETAMessageReceivedCar(it.message))
                }
        )

        etaRequest?.let {
            if (it.shareLiveLocation.equals(Constants.TRIPETA.YES)) {
                updateETAStatus(ETAStatus.INPROGRESS)
            }
            if (it.tripEtaUUID != null) {
                AppSyncHelper.subscribeToETAMessage(it.tripEtaUUID!!, context!!)
            } else {
                sendETAMessage(it)
            }
        }
    }

    fun stopETA(isArrivedAtDestination: Boolean) {
        if (isETAEnabled()) {
            AppSyncHelper.unsubscribeToETAMessage()
            if (isArrivedAtDestination) {
                updateETAStatus(ETAStatus.COMPLETE)
            } else {
                updateETAStatus(ETAStatus.CANCEL)
            }
            etaRequest = null
            etaStatus = null
        }
    }


    fun gc() {
        compositeDisposable.clear()
        etaRequest = null
        etaStatus = null
    }

    companion object {
        var mInstance: ETAEngine? = null

        fun init(apiHelper: ApiHelper, context: Context) {
            if (mInstance == null) {
                mInstance = ETAEngine()
            }
            mInstance!!.apiHelper = apiHelper
            mInstance!!.context = context
            mInstance!!.updateETAStatus(ETAStatus.CANCEL)
        }

        // FIXME: fixed a potential crash issue with ETAEngine,
        //  but it will disable eta when this happens
        //  clean fix should handle saveInstanceState and restoreInstanceState
        fun updateETPRequest(request: ETARequest?) {
            mInstance?.etaRequest = request
        }

        fun getInstance(): ETAEngine? {
            return mInstance
        }

//        fun destroy() {
//            if (mInstance != null) {
//                mInstance!!.destroy()
//                mInstance = null
//            }
//        }
    }


}

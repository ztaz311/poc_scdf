package com.ncs.breeze.common.remote;

import com.breeze.model.constants.Constants;

import java.io.IOException;

public class OfflineException extends IOException {
    @Override
    public String getMessage() {
        return Constants.No_Internet_Connection;
        // You can send any message whatever you want from here.
    }
}

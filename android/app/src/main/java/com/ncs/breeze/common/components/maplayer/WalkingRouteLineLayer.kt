package com.ncs.breeze.common.components.maplayer

import android.annotation.SuppressLint
import android.content.Context
import com.mapbox.geojson.Feature
import com.mapbox.geojson.FeatureCollection
import com.mapbox.geojson.LineString
import com.mapbox.maps.Style
import com.mapbox.maps.extension.style.layers.addLayer
import com.mapbox.maps.extension.style.layers.addLayerAbove
import com.mapbox.maps.extension.style.layers.generated.LineLayer
import com.mapbox.maps.extension.style.layers.properties.generated.LineCap
import com.mapbox.maps.extension.style.layers.properties.generated.LineJoin
import com.mapbox.maps.extension.style.sources.addSource
import com.mapbox.maps.extension.style.sources.generated.geoJsonSource


class WalkingRouteLineLayer(
    private val style: Style,
    private val context: Context,
) {
    fun generateRouteLineLayer(geometry: LineString) {
        removeLineLayer()
        addStyleSource(FeatureCollection.fromFeature(Feature.fromGeometry(geometry)))
        addLayer()
    }

    fun removeLineLayer() {
        style.removeStyleLayer(LAYER_ID)
        style.removeStyleSource(SOURCE_ID)
    }

    private fun addStyleSource(
        featureCollection: FeatureCollection
    ) {
        style.addSource(
            geoJsonSource(SOURCE_ID) {
                featureCollection(featureCollection)
            }
        )
    }


    @SuppressLint("ResourceType")
    private fun addLayer() {
        //style.removeStyleLayer(LAYER_ID)
        val lineLayer = LineLayer(LAYER_ID, SOURCE_ID)
        lineLayer.addDashingStyleToLine(context)
        if (style.styleLayers.isNullOrEmpty()) {
            style.addLayer(
                lineLayer
            )
        } else {
            style.addLayerAbove(
                lineLayer, style.styleLayers.last().id
            )
        }
    }

    companion object {
        const val SOURCE_ID = "breeze-walking-route-source"
        const val LAYER_ID = "breeze-walking-route-layer"
        private const val WALKING_PATH_DASH_COLOR = "#5DA7FF"

        fun LineLayer.addDashingStyleToLine(context: Context) {
            val density = context.resources.displayMetrics.density
            lineDasharray(listOf(1.0, 1.6))
            lineWidth(density * 2.0)
            lineCap(LineCap.SQUARE)
            lineJoin(LineJoin.ROUND)
            lineColor(WALKING_PATH_DASH_COLOR)
        }
    }

}
package com.ncs.breeze.common.utils

import android.content.Context
import android.content.Context.BATTERY_SERVICE
import android.net.TrafficStats
import android.os.BatteryManager
import android.util.Log
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by Aiswarya on 23,June,2021
 */
@Singleton
class BreezeStatisticsUtil @Inject constructor(appContext: Context) {

    companion object {
        const val TAG = "BreezeStatisticsUtil"
    }

    fun getBatteryPercentage(context: Context): Int {
        val bm = context.getSystemService(BATTERY_SERVICE) as BatteryManager
        Log.d(
            TAG,
            "getBatteryPercentage=" + (bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY))
        )
        return bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY)

    }

    fun getTotalDataReceived(): Long {
        return TrafficStats.getTotalRxBytes()
    }

    fun getTotalDataSent(): Long {
        return TrafficStats.getTotalTxBytes()
    }

}
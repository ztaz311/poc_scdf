package com.ncs.breeze.components.marker.markerview2

import com.mapbox.maps.MapView

class ERPMarkerViewManager(private val mapView: MapView) :
    MarkerViewManager2(mapView) {

    fun removeAllMarkers() {
        for (markerView in markers) {
            if (markerView is ERpMarkerView) {
                if (!markers.contains(markerView)) {
                    return
                }
                markerView.stopReloadData()
                mapView.removeView(markerView.mViewMarker)
            }
        }
        markers.clear()
    }

    fun hideAllTooltips() {
        for (markerView in markers) {
            markerView.hideAllToolTips()
        }
    }

    override fun hideAllMarker() {
        for (markerView in markers) {
            markerView.hideMarker()
        }
    }


    /**
     * hide all tooltips
     */
    override fun showAllMarker() {
        for (markerView in markers) {
            markerView.showMarker()
        }
    }

    fun showERPByID(erpid: Int) {
        for (markerView in markers) {
            if (markerView is ERpMarkerView) {
                val currentDataMaker = markerView.currentDataMarker
                if (currentDataMaker?.erpid == erpid) {
                    markerView.showTooltip()
                } else {
                    markerView.hideAllToolTips()
                }
            }
        }
    }

    /**
     * set all amenities to default state
     */
    fun swithAllToDefaultState() {
        for (markerView in markers) {
            if (markerView is ERpMarkerView && (markerView.currentState == ERpMarkerView.SELECTED || markerView.currentState == ERpMarkerView.KEEP_STATE)) {
                markerView.setStateMarker(ERpMarkerView.DEFAULT)
            }
        }
    }

}
package com.ncs.breeze.ui.dashboard.activity

import android.location.Location
import android.os.Handler
import android.os.Looper
import android.util.Log
import com.mapbox.android.gestures.MoveGestureDetector
import com.mapbox.geojson.Point
import com.mapbox.maps.MapView
import com.mapbox.maps.plugin.gestures.OnMoveListener
import com.mapbox.maps.plugin.gestures.gestures
import com.mapbox.turf.TurfConstants
import com.mapbox.turf.TurfMeasurement
import java.lang.Math.abs
import java.lang.ref.WeakReference

class PanTrackingManager(mapView: MapView) {
    private val mapViewRef = WeakReference(mapView)
    private val MIN_MOVE_DELTA = 20
    private val panHander = Handler(Looper.getMainLooper())
    private var oldPoint: Point? = null
    private var newPoint: Point? = null
    var isInPanMode = false
        set(value) {
            if (!value) {
                oldPoint = null
                newPoint = null
                panHander.removeCallbacks(runnable)
            }
            field = value
        }
    var isEnable = true
    var callBackUpdateCarParkWithNewLocation: CallBackUpdateCarParkWithNewLocation? = null
    var currentLocationPan: Location? = null

    var isCarParkEnable = false


    val onMoveListener = object : OnMoveListener {
        //last point coordinates
        var lastX = 0F
        var lastY = 0F

        override fun onMoveBegin(detector: MoveGestureDetector) {
            //remember start coordinates of user's finger
            lastX = detector.focalPoint.x
            lastY = detector.focalPoint.y
        }

        override fun onMove(detector: MoveGestureDetector): Boolean {
            return false
        }

        override fun onMoveEnd(detector: MoveGestureDetector) {
            val deltaX = detector.focalPoint.x - lastX
            val deltaY = detector.focalPoint.y - lastY
            if (abs(deltaX) > MIN_MOVE_DELTA || abs(deltaY) > MIN_MOVE_DELTA) {
                callBackUpdateCarParkWithNewLocation?.panStart()
                startCountDownToTrackNewLocation()
            }
        }
    }

    init {
        mapView.gestures.addOnMoveListener(onMoveListener)
    }

    private var runnable = Runnable {
        val mapPoint = mapViewRef.get()?.getMapboxMap()?.cameraState?.center
        if (mapPoint != null) {
            oldPoint = newPoint
            newPoint = mapPoint
            if (oldPoint == null) {
                startGetCarParkWithPan()
            } else {
                val distance = TurfMeasurement.distance(
                    oldPoint!!,
                    newPoint!!,
                    TurfConstants.UNIT_METRES
                )

                if (distance > 500) {
                    startGetCarParkWithPan()
                }

            }
        }
    }

    fun togglePanTracking(isEnabled: Boolean) {
        this.isEnable = isEnabled
    }


    private fun startGetCarParkWithPan() {
        newPoint?.let {
            isInPanMode = true
            currentLocationPan = Location("")
            currentLocationPan?.latitude = it.latitude()
            currentLocationPan?.longitude = it.longitude()
            Log.d("PanTrackingManager", "updateCarParkNewLocation")
            currentLocationPan?.let { lc ->
                callBackUpdateCarParkWithNewLocation?.updateCarParkNewLocation()
            }
        }
    }

    fun startCountDownToTrackNewLocation() {
        if (isCarParkEnable && isEnable) {
            Log.d("PanTrackingManager", "startCountDownToGetCarpark")
            panHander.removeCallbacks(runnable)
            /**
             * create handle to delay after 1s to get new carpark
             */
            panHander.postDelayed(runnable, 1000)
        }
    }

    fun resetAll() {
        isInPanMode = false
        currentLocationPan = null
    }


    interface CallBackUpdateCarParkWithNewLocation {

        /**
         * this funcation call when user start pan
         */
        fun panStart()

        /**
         * invoked to update view for edge metadata related to speedLimit, edge name, tunnel
         */
        fun updateCarParkNewLocation()
    }

}
package com.ncs.breeze.components.marker.markerview2

import android.app.Activity
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.os.bundleOf
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import com.breeze.model.api.response.PlaceDetailsResponse
import com.breeze.model.constants.Constants
import com.breeze.model.enums.AmenityBand
import com.breeze.model.extensions.dpToPx
import com.facebook.react.bridge.Arguments
import com.google.android.gms.maps.model.LatLng
import com.mapbox.geojson.Point
import com.mapbox.maps.EdgeInsets
import com.mapbox.maps.MapboxMap
import com.ncs.breeze.R
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.extensions.android.getShareBusinessLogicPackage
import com.ncs.breeze.common.utils.BreezeMapZoomUtil
import com.ncs.breeze.databinding.MapTooltipSavedPlaceBinding
import com.ncs.breeze.reactnative.nativemodule.ShareBusinessLogicEvent
import com.ncs.breeze.ui.base.BaseActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class SavedPlaceMarkerTooltip(
    latLng: LatLng,
    mapboxMap: MapboxMap,
    val activity: Activity,
    itemId: String?,
    private val toolTipZoomRange: Double = Constants.POI_TOOLTIP_ZOOM_RADIUS
) : MarkerView2(mapboxMap = mapboxMap, itemId) {
    var currentState = 0
    var currentPlaceDetailsResponse: PlaceDetailsResponse? = null
    val viewBinding: MapTooltipSavedPlaceBinding
    var analyticsScreenName = ""

    init {
        mLatLng = latLng

        viewBinding = MapTooltipSavedPlaceBinding.inflate(LayoutInflater.from(activity), null, false)
        mViewMarker = viewBinding.root
    }

    fun handleClick(
        item: PlaceDetailsResponse,
        recenter: Boolean = true,
        heightBottomSheet: Int,
        navigateHereButtonClick: (PlaceDetailsResponse, BaseActivity.EventSource) -> Unit,
        bookmarkIconClick: (bookmarkId: Int) -> Unit,
        poiIconClick: (PlaceDetailsResponse, Boolean) -> Unit
    ) {

        currentPlaceDetailsResponse = item
        viewBinding.tvNamePOISave.text = item.name
        viewBinding.tvAddress.text =
            when {
                !item.fullAddress.isNullOrEmpty() -> item.fullAddress
                !item.address2.isNullOrEmpty() -> item.address2
                else -> item.address1
            }
        displayCrowdsourceInfo(item)

        viewBinding.navigateHereButton.setOnClickListener {
            resetView()
            navigateHereButtonClick.invoke(item, BaseActivity.EventSource.NONE)
        }

        viewBinding.buttonShare.setOnClickListener {
            Analytics.logClickEvent("[map]:tooltip_saved_share", analyticsScreenName)
            shareLocation(item)
        }

        viewBinding.buttonInvite.setOnClickListener {
            Analytics.logClickEvent("[map]:tooltip_saved_invite", analyticsScreenName)
            inviteLocation(item)
        }

        viewBinding.imgFlagSaved.setOnClickListener {
            item.bookmarkId?.let { bookmarkId -> bookmarkIconClick.invoke(bookmarkId) }
        }

        poiIconClick.invoke(item, true)

        if (recenter) {
            BreezeMapZoomUtil.zoomViewToRange(
                mapboxMap, Point.fromLngLat(item.long!!.toDouble(), item.lat!!.toDouble()),
                toolTipZoomRange, EdgeInsets(
                    10.0.dpToPx(),
                    10.0.dpToPx(),
                    heightBottomSheet.toDouble().dpToPx(),
                    10.0.dpToPx()
                ), animationEndCB = {
                    viewBinding.root.bringToFront()
                }
            )
        } else {
            updateTooltipPosition()
        }
    }

    private fun displayCrowdsourceInfo(item: PlaceDetailsResponse) {
        val availabilityCSData = item.availabilityCSData

        viewBinding.layoutCarparkStatus.root.isVisible = availabilityCSData != null
        if (availabilityCSData == null) return
        val title: String = availabilityCSData.title ?: ""
//            Calculate last update time
        val timeUpdate =
            "${availabilityCSData.primaryDesc ?: viewBinding.root.context.getString(com.breeze.customization.R.string.update_carpark_stt_time)} ${availabilityCSData.generateDisplayedLastUpdate()}"
        val drawableHeaderCpStatus =
            AppCompatResources.getDrawable(viewBinding.root.context, R.drawable.bg_header_carpark_status)
        drawableHeaderCpStatus?.setTint(Color.parseColor(availabilityCSData.themeColor ?: "#F26415"))
        viewBinding.layoutCarparkStatus.root.background = drawableHeaderCpStatus
        viewBinding.layoutCarparkStatus.icCarparkStatus.setImageResource(
            when (item.availablePercentImageBand) {
                AmenityBand.TYPE0.type -> R.drawable.ic_cp_stt_full
                AmenityBand.TYPE2.type -> R.drawable.ic_cp_stt_available
                else -> R.drawable.ic_cp_stt_few
            }
        )
        viewBinding.layoutCarparkStatus.tvStatusCarpark.text = title
        viewBinding.layoutCarparkStatus.tvTimeUpdateCarparkStatus.text = timeUpdate
    }

    private fun createShareLocationData(item: PlaceDetailsResponse) = Arguments.fromBundle(
        bundleOf(
            "address1" to item.address1,
            "address2" to item.address2,
            "latitude" to item.lat,
            "longitude" to item.long,
            "code" to item.code,
            "isDestination" to item.isDestination,
            "amenityId" to item.amenityId,
            "amenityType" to item.amenityType,
            "description" to (item.description ?: ""),
            "name" to item.name,
            "bookmarkId" to item.bookmarkId,
            "placeId" to item.placeId,
            "userLocationLinkIdRef" to item.userLocationLinkIdRef
        )
    )

    private fun shareLocation(item: PlaceDetailsResponse) {
        viewBinding.root.context?.getShareBusinessLogicPackage()?.getRequestsModule()?.flatMap {
            it.callRN(
                ShareBusinessLogicEvent.SHARE_LOCATION_FROM_NATIVE.eventName,
                createShareLocationData(item)
            )
        }?.subscribeOn(Schedulers.io())?.observeOn(AndroidSchedulers.mainThread())?.subscribe()
    }

    private fun inviteLocation(item: PlaceDetailsResponse) {
        viewBinding.root.context?.getShareBusinessLogicPackage()?.getRequestsModule()?.flatMap {
            it.callRN(
                ShareBusinessLogicEvent.LOCATION_INVITE_FROM_NATIVE.eventName,
                createShareLocationData(item)
            )
        }?.subscribeOn(Schedulers.io())?.observeOn(AndroidSchedulers.mainThread())?.subscribe()
    }

    override fun updateTooltipPosition() {
        mLatLng?.let { location ->
            val screenCoordinate = mapboxMap.pixelForCoordinate(
                Point.fromLngLat(
                    location.longitude,
                    location.latitude
                )
            )
            mViewMarker?.let {
                it.measure(
                    View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                    View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
                )
                it.x = screenCoordinate.x.toFloat() - (it.measuredWidth / 2)
                it.y = screenCoordinate.y.toFloat() - it.measuredHeight
            }
        }
    }

    override fun showMarker() {
        super.showMarker()
        viewBinding.root.visibility = View.VISIBLE
    }

    override fun hideMarker() {
        super.hideMarker()
        viewBinding.root.visibility = View.INVISIBLE
    }

    override fun hideAllToolTips() {
        super.hideAllToolTips()
        if (viewBinding.llToolTip.isShown) {
            viewBinding.llToolTip.isInvisible = true
        }
    }

}

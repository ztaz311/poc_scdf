package com.ncs.breeze.di.module.fragment

import com.ncs.breeze.ui.dashboard.fragments.view.NewAddressFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class NewAddressModule {
    @ContributesAndroidInjector
    abstract fun contributeNewAddressFragment(): NewAddressFragment
}
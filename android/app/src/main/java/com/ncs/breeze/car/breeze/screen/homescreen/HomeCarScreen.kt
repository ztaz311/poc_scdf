package com.ncs.breeze.car.breeze.screen.homescreen

import androidx.car.app.model.ActionStrip
import androidx.car.app.model.CarColor
import androidx.car.app.model.Template
import androidx.car.app.navigation.model.NavigationTemplate
import com.ncs.breeze.App
import com.ncs.breeze.R
import com.ncs.breeze.car.breeze.MainBreezeCarContext
import com.ncs.breeze.car.breeze.base.BaseNavigationScreenCar
import com.ncs.breeze.car.breeze.style.CarFreeDriveStyle
import com.ncs.breeze.car.breeze.utils.BreezeCarUtil
import com.ncs.breeze.car.breeze.utils.CarConstants
import com.ncs.breeze.common.analytics.Screen
import com.ncs.breeze.common.appSync.AppSyncHelper
import com.ncs.breeze.common.constant.OBUConnectionState
import com.ncs.breeze.common.event.ToCarRx
import com.ncs.breeze.common.helper.obu.OBUDataHelper
import com.ncs.breeze.common.model.rx.CarERPRefresh
import com.ncs.breeze.common.model.rx.CarIncidentDataRefresh
import com.ncs.breeze.common.rxbus.RxBus
import com.ncs.breeze.common.rxbus.RxEvent
import com.ncs.breeze.common.utils.LocationBreezeManager
import com.ncs.breeze.common.utils.OBUStripStateManager
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class HomeCarScreen(
    mainCarContext: MainBreezeCarContext
) : BaseNavigationScreenCar(mainCarContext) {
    private var homeActionStrip: ActionStrip? = null
    private var carDynamicStyle = CarFreeDriveStyle(mainCarContext)
    private val carLocationRenderer = CarFreeDriveHomeLocationRenderer(mainCarContext)
    private val incidentCompositeDisposable = CompositeDisposable()
    private var isConnected = false
    private var currentOBUBalance: Double? = null

    override fun onGetTemplate(): Template {
        updateActionStrip()
        return NavigationTemplate.Builder()
            //.setBackgroundColor(CarColor.PRIMARY)
            .setBackgroundColor(
                CarColor.createCustom(
                    carContext.getColor(R.color.color_theme_bg),
                    carContext.getColor(R.color.color_theme_bg)
                )
            )
            .setActionStrip(homeActionStrip!!)
            .build()
    }

    override fun getScreenName(): String {
        return Screen.AA_HOMEPAGE_SCREEN
    }

    override fun onCreateScreen() {
        super.onCreateScreen()
        startFeatureDetection()
    }

    private fun startFeatureDetection() {
        CoroutineScope(Dispatchers.IO).launch {
            BreezeCarUtil.breezeERPUtil.startERPHandler()
            BreezeCarUtil.breezeIncidentsUtil.startIncidentHandler()
            BreezeCarUtil.breezeMiscRoadObjectUtil.startMiscROHandler()
            BreezeCarUtil.breezeSchoolSilverZoneUpdateUtil.startSchoolSilverZoneHandler()
            incidentCompositeDisposable.clear()
            incidentCompositeDisposable.add(
                RxBus.listen(RxEvent.IncidentDataRefresh::class.java)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe {
                        ToCarRx.postEvent(CarIncidentDataRefresh(it.incidentData))
                    }
            )
            incidentCompositeDisposable.add(
                RxBus.listen(RxEvent.ERPRefresh::class.java)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe {
                        it.erpFeature?.let { erpFeature ->
                            ToCarRx.postEvent(CarERPRefresh(erpFeature))
                        }
                    }
            )
            AppSyncHelper.subscribeToAppSyncEvents(mainCarContext.carContext.applicationContext)
        }
    }

    override fun onDestroyScreen() {
        super.onDestroyScreen()
        stopHandlersAndDispose()
    }

    private fun stopHandlersAndDispose() {
        CoroutineScope(Dispatchers.IO).launch {
            incidentCompositeDisposable.dispose()
        }
    }


    override fun onStartScreen() {
        super.onStartScreen()
        this.marker = CarConstants.HOME_MARKER
        changeOBUConnectionState((carContext.applicationContext as? App)?.obuConnectionHelper?.obuConnectionState?.value == OBUConnectionState.CONNECTED)
        LocationBreezeManager.getInstance().startLocationUpdates()
        OBUStripStateManager.getInstance()?.enable()
        mainCarContext.mapboxCarMap.registerObserver(carDynamicStyle)
        mainCarContext.mapboxCarMap.registerObserver(carLocationRenderer)
    }

    override fun onStopScreen() {
        super.onStopScreen()
        mainCarContext.mapboxCarMap.unregisterObserver(carDynamicStyle)
        mainCarContext.mapboxCarMap.unregisterObserver(carLocationRenderer)
    }

    private fun updateActionStrip() {
        homeActionStrip =
            HomeActionStrip(mainCarContext)
                .builder(isConnected, currentOBUBalance)
                .build()
    }

    override fun changeOBUConnectionState(connected: Boolean) {
        if (connected != this.isConnected) {
            this.isConnected = connected
            this.currentOBUBalance = OBUDataHelper.getOBUCardBalanceSGD()
            invalidate()
        }
    }

    override fun updateCardBalance(cardBalance: Double?) {
        if (cardBalance != currentOBUBalance) {
            currentOBUBalance = cardBalance
            invalidate()
        }
    }
}
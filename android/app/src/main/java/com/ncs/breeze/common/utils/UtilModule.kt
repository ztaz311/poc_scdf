package com.ncs.breeze.common.utils

import android.content.Context
import com.ncs.breeze.common.remote.ApiHelper
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class UtilModule {

    @Provides
    @Singleton
    internal fun provideBreezeERPRefreshUtil(
        apiHelper: ApiHelper,
        context: Context
    ): BreezeERPRefreshUtil {
        return BreezeERPRefreshUtil(apiHelper, context)
    }

    @Provides
    @Singleton
    internal fun provideBreezeTraffficIncidentsUtil(
        apiHelper: ApiHelper
    ): BreezeTrafficIncidentsUtil {
        return BreezeTrafficIncidentsUtil(apiHelper)
    }

    @Provides
    @Singleton
    internal fun provideBreezeMiscRoadObjectUtil(
        apiHelper: ApiHelper
    ): BreezeMiscRoadObjectUtil {
        return BreezeMiscRoadObjectUtil(apiHelper)
    }


    @Provides
    @Singleton
    internal fun provideBreezeUserPreferenceUtil(
        context: Context
    ): BreezeUserPreferenceUtil {
        return BreezeUserPreferenceUtil(context)
    }

    @Provides
    @Singleton
    internal fun provideBreezeRouteSortingUtil(
        userPreferenceUtil: BreezeUserPreferenceUtil
    ): BreezeRouteSortingUtil {
        return BreezeRouteSortingUtil(userPreferenceUtil)
    }

    @Provides
    @Singleton
    internal fun provideBreezeInstabugUtil(
        context: Context,
        userPreferenceUtil: BreezeUserPreferenceUtil
    ): BreezeInstabugUtil {
        return BreezeInstabugUtil(
            context,
            userPreferenceUtil
        )
    }

    @Provides
    @Singleton
    internal fun provideBreezeCalendarUtil(
        context: Context
    ): BreezeCalendarUtil {
        return BreezeCalendarUtil(context)
    }

    @Provides
    @Singleton
    internal fun provideHistoryRecorderUtil(
        userPreferenceUtil: BreezeUserPreferenceUtil,
        context: Context
    ): BreezeHistoryRecorderUtil {
        return BreezeHistoryRecorderUtil(userPreferenceUtil, context)
    }

    @Provides
    @Singleton
    internal fun provideBreezeFeatureDetectionUtil(
        context: Context,
        breezeERPUtil: BreezeERPRefreshUtil,
        breezeTrafficIncidentsUtil: BreezeTrafficIncidentsUtil,
        breezeMiscRoadObjectUtil: BreezeMiscRoadObjectUtil,
    ): BreezeFeatureDetectionUtil {
        return BreezeFeatureDetectionUtil(
            context,
            breezeERPUtil,
            breezeTrafficIncidentsUtil,
            breezeMiscRoadObjectUtil,
        )
    }

    @Provides
    @Singleton
    internal fun provideBreezeLocationEngineUtil(
        context: Context,
        apiHelper: ApiHelper
    ): BreezeTripLoggingUtil {
        return BreezeTripLoggingUtil(context, apiHelper)
    }

    @Provides
    @Singleton
    internal fun provideBreezeSchoolSilverZoneUpdateUtil(
        context: Context,
        apiHelper: ApiHelper
    ): BreezeSchoolSilverZoneUpdateUtil {
        return BreezeSchoolSilverZoneUpdateUtil(context, apiHelper)
    }

    @Provides
    @Singleton
    internal fun provideBreezeStatisticsUtil(
        context: Context
    ): BreezeStatisticsUtil {
        return BreezeStatisticsUtil(context)
    }


    @Provides
    @Singleton
    internal fun provideBreezeFileLoggingUtil(
        userPreferenceUtil: BreezeUserPreferenceUtil,
        context: Context
    ): BreezeFileLoggingUtil {
        return BreezeFileLoggingUtil(userPreferenceUtil, context)
    }
}
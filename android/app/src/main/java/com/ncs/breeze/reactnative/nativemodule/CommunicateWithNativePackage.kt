package com.ncs.breeze.reactnative.nativemodule

import com.ncs.breeze.common.utils.BreezeUserPreferenceUtil
import com.facebook.react.ReactPackage
import com.facebook.react.bridge.NativeModule
import com.facebook.react.bridge.ReactApplicationContext
import com.facebook.react.uimanager.ViewManager

class CommunicateWithNativePackage(val breezeUserPreferenceUtil: BreezeUserPreferenceUtil) : ReactPackage {
    override fun createViewManagers(reactContext: ReactApplicationContext): List<ViewManager<*, *>> {
        return emptyList()
    }

    override fun createNativeModules(reactContext: ReactApplicationContext): List<NativeModule> {
        val modules: MutableList<NativeModule> = ArrayList()
        modules.add(CommunicateWithNativeModule(reactContext, breezeUserPreferenceUtil))
        modules.add(NativeContactActivityStarterModule(reactContext))
        return modules
    }
}
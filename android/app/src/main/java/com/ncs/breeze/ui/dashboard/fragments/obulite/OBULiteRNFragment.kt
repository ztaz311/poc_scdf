package com.ncs.breeze.ui.dashboard.fragments.obulite

import android.location.Location
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager.OnBackStackChangedListener
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import com.facebook.react.bridge.Arguments
import com.facebook.react.bridge.ReadableMap
import com.mapbox.navigation.base.formatter.DistanceFormatterOptions
import com.mapbox.navigation.base.formatter.UnitType
import com.mapbox.navigation.core.lifecycle.MapboxNavigationApp
import com.mapbox.navigation.core.trip.session.LocationMatcherResult
import com.mapbox.navigation.core.trip.session.LocationObserver
import com.mapbox.navigation.ui.speedlimit.api.MapboxSpeedInfoApi
import com.ncs.breeze.R
import com.ncs.breeze.common.analytics.AnalyticsUtils
import com.ncs.breeze.common.analytics.Screen
import com.ncs.breeze.common.appSync.AppSyncHelper
import com.ncs.breeze.common.constant.OBUConstants
import com.ncs.breeze.common.constant.RNScreen
import com.ncs.breeze.common.event.ToCarRx
import com.ncs.breeze.common.extensions.android.getApp
import com.ncs.breeze.common.extensions.android.getBreezeUserPreference
import com.ncs.breeze.common.extensions.android.getOBUConnHelper
import com.breeze.model.extensions.round
import com.ncs.breeze.common.helper.obu.OBUConnectionHelper
import com.ncs.breeze.common.helper.obu.OBUDataHelper
import com.breeze.model.obu.OBUCardStatusEventData
import com.breeze.model.obu.OBUParkingAvailability
import com.breeze.model.obu.OBURoadEventData
import com.breeze.model.obu.OBUTravelTimeData
import com.breeze.model.api.response.amenities.BaseAmenity
import com.ncs.breeze.common.model.rx.OBUDisplayLifecycleEvent
import com.ncs.breeze.common.rxbus.RxBus
import com.ncs.breeze.common.rxbus.RxEvent
import com.ncs.breeze.common.storage.BreezeUserPreference
import com.breeze.model.constants.Constants
import com.ncs.breeze.databinding.FragmentObuLiteRnContainerBinding
import com.ncs.breeze.reactnative.nativemodule.ShareBusinessLogicEvent
import com.ncs.breeze.ui.dashboard.activity.DashboardActivity
import com.ncs.breeze.ui.rn.CustomReactFragment
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import sg.gov.lta.obu.sdk.data.services.OBUSDK
import timber.log.Timber
import kotlin.math.roundToInt

class OBULiteRNFragment : Fragment() {
    private lateinit var viewBinding: FragmentObuLiteRnContainerBinding

    private val compositeDisposable = CompositeDisposable()
    private var currentLocationMatcherResult: LocationMatcherResult? = null

    private var lastOBUVelocityKmH = -1

    private val speedLimitApi: MapboxSpeedInfoApi by lazy { MapboxSpeedInfoApi() }

    private var isInMapViewMode = false

    private var lastBackStackEntryName = ""

    private val activityBackStackChangeListener = OnBackStackChangedListener {
        activity?.supportFragmentManager
            ?.takeIf { lifecycle.currentState.isAtLeast(Lifecycle.State.STARTED) }
            ?.run {
                if (backStackEntryCount > 0) {
                    val currentBackStackEntryName =
                        getBackStackEntryAt(backStackEntryCount - 1).name
                    if (currentBackStackEntryName == OBULiteFragment.TAG && lastBackStackEntryName == Constants.TAGS.SEARCH_TAG) {
                        (childFragmentManager.findFragmentById(viewBinding.rootContainer.id) as? CustomReactFragment)?.reactDelegate?.onHostResume()
                    }
                }
                lastBackStackEntryName = getBackStackEntryAt(backStackEntryCount - 1).name ?: ""
            }
    }

    private val locationObserver = object : LocationObserver {
        override fun onNewLocationMatcherResult(locationMatcherResult: LocationMatcherResult) {
            currentLocationMatcherResult = locationMatcherResult
            if (lifecycle.currentState.isAtLeast(Lifecycle.State.STARTED)) {
                if (OBUConnectionHelper.isOBUSimulationEnabled || !OBUSDK.isConnectionActive()) {
                    val speedInfoValue = speedLimitApi.updatePostedAndCurrentSpeed(
                        locationMatcherResult,
                        DistanceFormatterOptions.Builder(requireContext())
                            .unitType(UnitType.METRIC)
                            .build()
                    )

                    (parentFragment as? OBULiteFragment)?.checkTripIdle(speedInfoValue.currentSpeed.toDouble())
                }
//                updateSpeedToRN(//BREEZE2-1011
//                    speedKmH = speedInfoValue.currentSpeed.toDouble(),
//                    speedLimitKmH = (speedInfoValue.postedSpeed ?: 0).toDouble()
//                )
            }
        }

        override fun onNewRawLocation(rawLocation: Location) {}
    }

    override fun onStart() {
        super.onStart()
        activity?.supportFragmentManager?.addOnBackStackChangedListener(
            activityBackStackChangeListener
        )
    }

    override fun onStop() {
        super.onStop()
        activity?.supportFragmentManager?.removeOnBackStackChangedListener(
            activityBackStackChangeListener
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewBinding = FragmentObuLiteRnContainerBinding.inflate(inflater, container, false)
        return viewBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        addRNScreen()
//        listenMapViewModeChanged()//BREEZE2-1011
        listenOBUDataEvent()
        MapboxNavigationApp.current()?.registerLocationObserver(locationObserver)
//        ToCarRx.postEventWithCacheLast(OBUDisplayLifecycleEvent(Lifecycle.State.CREATED))
        initCardInfoToRN()
    }

//    private fun listenMapViewModeChanged() {//BREEZE2-1011
//        (parentFragment as? OBULiteFragment)?.let { obuLiteFragment ->
//            lifecycleScope.launch(Dispatchers.IO) {
//                repeatOnLifecycle(Lifecycle.State.STARTED) {
//                    ViewModelProvider(obuLiteFragment)[OBULiteFragmentViewModel::class.java].isMapViewModeFlow.collectLatest {
//                        isInMapViewMode = it
//                    }
//                }
//            }
//        }
//    }

    private fun initCardInfoToRN() {
        updateCardInfoToRN(
            OBUCardStatusEventData.EVENT_TYPE_CARD_VALID,
            OBUDataHelper.getOBUCardBalanceSGD()
        )
    }


    override fun onDestroyView() {
        super.onDestroyView()
        compositeDisposable.clear()
        MapboxNavigationApp.current()?.unregisterLocationObserver(locationObserver)
    }

    private fun listenOBUDataEvent() {
//        compositeDisposable.add(//BREEZE2-1011
//            RxBus.listen(RxEvent.OBURoadEvent::class.java)
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe { event ->
//                    Timber.d("OBURoadEvent : ${event.eventData}")
//                    if (shouldSendAnalyticEvent() || event.eventData.eventType in arrayOf(
//                            OBURoadEventData.EVENT_TYPE_ERP_CHARGING,
//                            OBURoadEventData.EVENT_TYPE_ERP_SUCCESS,
//                            OBURoadEventData.EVENT_TYPE_ERP_FAILURE,
//                            OBURoadEventData.EVENT_TYPE_PARKING_FEE,
//                            OBURoadEventData.EVENT_TYPE_PARKING_SUCCESS,
//                            OBURoadEventData.EVENT_TYPE_PARKING_FAILURE,
//                        )
//                    ) {
//                        AnalyticsUtils.sendRoadEvent(
//                            event.eventData,
//                            (parentFragment as? OBULiteFragment)?.currentRoadName ?: "",
//                            currentLocation?.latitude ?: 0.0,
//                            currentLocation?.longitude ?: 0.0,
//                            Screen.OBU_DISPLAY_MODE,
//                            Event.OBU_DISPLAY_NOTIFICATION
//                        )
//                    }
//                    if (shouldSendAnalyticEvent()) {
//                        handleRoadEvent(event.eventData)
//                    }
//                }
//        )
//        compositeDisposable.add(
//            RxBus.listen(RxEvent.OBUParkingAvailabilityEvent::class.java)
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe {
//                    Timber.d("OBUParkingAvailabilityEvent: ${it.data}")
//                    if (shouldSendAnalyticEvent()) {
//                        AnalyticsUtils.sendRoadEventParkingAvailability(
//                            Event.USER_POPUP,
//                            Event.VALUE_POPUP_OPEN,
//                            (parentFragment as? OBULiteFragment)?.currentRoadName ?: "",
//                            currentLocation?.latitude ?: 0.0,
//                            currentLocation?.longitude ?: 0.0,
//                            Screen.OBU_DISPLAY_MODE
//                        )
//                        if (!isInMapViewMode)
//                            openParkingAvailabilityRN(it.data)
//                    }
//                }
//        )
//        compositeDisposable.add(
//            RxBus.listen(RxEvent.OBUTravelTimeEvent::class.java)
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe {
//                    Timber.d("OBUTravelTimeEvent: ${it.data}")
//                    if (shouldSendAnalyticEvent()) {
//                        AnalyticsUtils.sendRoadEventTravelTime(
//                            Event.USER_POPUP,
//                            Event.VALUE_POPUP_OPEN,
//                            (parentFragment as? OBULiteFragment)?.currentRoadName ?: "",
//                            currentLocation?.latitude ?: 0.0,
//                            currentLocation?.longitude ?: 0.0,
//                            Screen.OBU_DISPLAY_MODE
//                        )
//                        if (!isInMapViewMode)
//                            openTravelTimeScreenRN(it.data)
//                    }
//                }
//        )
        compositeDisposable.add(
            RxBus.listen(RxEvent.OBUVelocityEvent::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    Timber.d("OBUVelocityEvent: speedLimit = ${it.speedLimit}, velocity = ${it.velocityMS}")
                    if (!OBUConnectionHelper.isOBUSimulationEnabled) {
                        val speedKmH = it.velocityMS * 3.6
                        (parentFragment as? OBULiteFragment)?.checkTripIdle(speedKmH)
                    }
//                    updateSpeedToRN(it.velocityMS * 3.6, SpeedLimitUtil.speedLimit.toDouble())//BREEZE2-1011
                }
        )
        compositeDisposable.add(
            RxBus.listen(RxEvent.OBUCarParkSelectedEvent::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    Timber.d("OBUCarParkSelectedEvent: ${it.carPark}")
                    navigateToCarParkRN(it.carPark)
                }
        )
    }


    private fun navigateToCarParkRN(carPark: ReadableMap) {
        val amenity = BaseAmenity.fromRNData(carPark)
        AnalyticsUtils.sendSelectCarPark(amenity, Screen.OBU_DISPLAY_MODE)
        (parentFragment as? OBULiteFragment)?.navigateToCarPark(amenity)
    }

    private fun handleRoadEvent(eventData: OBURoadEventData) {
        if (isInMapViewMode) return
        val shouldShowNotification = when (eventData.eventType) {
            OBURoadEventData.EVENT_TYPE_SCHOOL_ZONE -> context?.getBreezeUserPreference()
                ?.getSchoolZoneDisplay() ?: true

            OBURoadEventData.EVENT_TYPE_SILVER_ZONE -> context?.getBreezeUserPreference()
                ?.getSilverZoneDisplay() ?: true

            OBURoadEventData.EVENT_TYPE_BUS_LANE -> context?.getBreezeUserPreference()
                ?.getBusLaneDisplay() ?: true

            else -> true
        }
        if(shouldShowNotification)
            updateRoadEventToRN(eventData)

        if (arrayOf(
                OBURoadEventData.EVENT_TYPE_TRAFFIC,
                OBURoadEventData.EVENT_TYPE_ERP_CHARGING,
                OBURoadEventData.EVENT_TYPE_SPEED_CAMERA,
            ).contains(eventData.eventType)
            && viewBinding.root.isVisible
        ) {
            lifecycleScope.launch {
                delay(3000)
                if (!isActive || isInMapViewMode) return@launch
                launch(Dispatchers.Main) {
                    viewBinding.root.isInvisible = true
                }
                delay(15000)
                launch(Dispatchers.Main) {
                    viewBinding.root.isVisible = true
                }
            }
        }

        if (eventData.shouldCheckLowCardBalance()) {
            lifecycleScope.launch(Dispatchers.IO) {
                delay(OBUConstants.OBU_LOW_CARD_ALERT_DELAY)
                val currentCardBalanceSGD = OBUDataHelper.getOBUCardBalanceSGD()
                    ?.takeIf { balanceSGD ->
                        isActive
                                && lifecycle.currentState.isAtLeast(Lifecycle.State.RESUMED)
                                && balanceSGD < OBUConstants.OBU_LOW_CARD_THRESHOLD / 100.0
                                && (parentFragment as? OBULiteFragment)?.isProjectedInAA() != true
                    }
                    ?: return@launch
                updateRoadEventToRN(
                    OBURoadEventData(
                        chargeAmount = "$${currentCardBalanceSGD.round(2)}",
                        message = "Low Card Balance",
                        detailMessage = null,
                        detailTitle = null,
                        distance = null,
                        eventType = OBURoadEventData.EVENT_TYPE_LOW_CARD
                    )
                )
            }
        }
    }

    private fun addRNScreen() {
        val initialProperties = bundleOf(
            Constants.RN_TO_SCREEN to RNScreen.OBU_LITE_DISPLAY
        )

        val reactNativeFragment = CustomReactFragment.newInstance(
            componentName = Constants.RN_CONSTANTS.COMPONENT_NAME,
            launchOptions = initialProperties,
            cleanOnDestroyed = false
        )

        childFragmentManager
            .beginTransaction()
            .add(viewBinding.rootContainer.id, reactNativeFragment, RNScreen.OBU_LITE_DISPLAY)
            .addToBackStack(RNScreen.OBU_LITE_DISPLAY)
            .commit()
    }

    private fun updateSpeedToRN(speedKmH: Double, speedLimitKmH: Double) {
        if (lastOBUVelocityKmH == speedKmH.roundToInt() || isInMapViewMode) return

        activity?.getApp()?.let { app ->
            lastOBUVelocityKmH = speedKmH.roundToInt()
            compositeDisposable.add(
                app.shareBusinessLogicPackage.getRequestsModule().flatMap {
                    val payload = Arguments.fromBundle(
                        bundleOf(
                            "speed" to lastOBUVelocityKmH,
                            "limitSpeed" to speedLimitKmH.roundToInt(),
                        )
                    )
                    it.callRN(ShareBusinessLogicEvent.UPDATE_OBU_LITE_SPEED.eventName, payload)
                }.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe()
            )

        }
    }

    private fun updateRoadEventToRN(eventData: OBURoadEventData) {
        activity?.getApp()?.let { app ->
            compositeDisposable.add(
                app.shareBusinessLogicPackage.getRequestsModule().flatMap {
                    it.callRN(
                        ShareBusinessLogicEvent.UPDATE_OBU_EVENT_NOTI.eventName,
                        Arguments.fromBundle(eventData.toBundle())
                    )
                }.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe()
            )
        }
    }

    private fun updateCardInfoToRN(cardStatus: String, cardBalance: Double?) {
        activity?.getApp()?.shareBusinessLogicHelper?.updateOBUCashCardInfo(cardStatus, cardBalance)
    }

    private fun openParkingAvailabilityRN(data: Array<OBUParkingAvailability>) {
        val parkingDisplay = BreezeUserPreference.getInstance(requireContext().applicationContext)
            .getParkingAvailabilityDisplay()
        if (!parkingDisplay) return
        activity?.getApp()?.run {
            (parentFragment as? OBULiteFragment)?.toggleControllersLayoutVisibility(false)
            compositeDisposable.add(
                shareBusinessLogicPackage.getRequestsModule().flatMap {
                    it.callRN(
                        ShareBusinessLogicEvent.OPEN_OBU_CAR_PARK_AVAILABILITY.eventName,
                        Arguments.createMap().apply {
                            putArray(
                                "carparks",
                                Arguments.fromArray(data.map { item -> item.toBundle() }
                                    .toTypedArray())
                            )
                        }
                    )
                }.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe()
            )
        }
    }

    private fun openTravelTimeScreenRN(data: Array<OBUTravelTimeData>) {
        val travelDisplay = BreezeUserPreference.getInstance(requireContext().applicationContext)
            .getEstimatedTravelTimeDisplay()
        if (!travelDisplay) return
        activity?.getApp()?.let { app ->
            compositeDisposable.add(app.shareBusinessLogicPackage.getRequestsModule().flatMap {
                it.callRN(
                    ShareBusinessLogicEvent.OPEN_OBU_TRAVEL_TIME.eventName,
                    Arguments.createMap().apply {
                        putArray(
                            "data",
                            Arguments.fromArray(data.map { item -> item.toBundle() }.toTypedArray())
                        )
                    }
                )
            }.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe())
        }
    }

    private fun shouldSendAnalyticEvent(): Boolean {
        return (activity as? DashboardActivity)?.let { activity ->
            val fragment = activity.supportFragmentManager.findFragmentById(R.id.frame_container)
            fragment is OBULiteFragment
        } ?: false
    }

}

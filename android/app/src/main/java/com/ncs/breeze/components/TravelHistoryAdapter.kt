package com.ncs.breeze.components

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.ncs.breeze.R
import com.breeze.model.api.response.TripsItem
import com.ncs.breeze.common.utils.Utils
import com.ncs.breeze.databinding.RowTravelLogItemBinding
import com.ncs.breeze.ui.dashboard.fragments.viewmodel.TravelLogItem
import com.ncs.breeze.ui.draggableview.setClickSafe
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Locale
import java.util.TimeZone

class TravelHistoryAdapter(
    val listener: TripItemListener
) : PagingDataAdapter<TravelLogItem, TravelHistoryAdapter.ViewHolder>(REPO_COMPARATOR) {

    var showCheckBoxIcon: Boolean = false

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            RowTravelLogItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: TravelHistoryAdapter.ViewHolder, position: Int) {
        (getItem(position) as? TravelLogItem.TripLogItem)
            ?.takeIf { it.tripLogItem != null }
            ?.let { travelItem ->
                holder.bind(travelItem)
                holder.itemView.setClickSafe {
                    if (!showCheckBoxIcon) {
                        listener.onItemClick(travelItem.tripLogItem)
                    }
                }
            }

    }

    companion object {
        private val REPO_COMPARATOR = object : DiffUtil.ItemCallback<TravelLogItem>() {
            override fun areItemsTheSame(oldItem: TravelLogItem, newItem: TravelLogItem): Boolean =
                (oldItem is TravelLogItem.TripLogItem && newItem is TravelLogItem.TripLogItem &&
                        oldItem.tripLogItem.tripId == newItem.tripLogItem.tripId)

            override fun areContentsTheSame(
                oldItem: TravelLogItem,
                newItem: TravelLogItem
            ): Boolean =
                oldItem == newItem
        }
    }

    fun getAdapterItem(position: Int): TravelLogItem.TripLogItem {
        return getItem(position) as TravelLogItem.TripLogItem
    }

    override fun getItemViewType(position: Int): Int {
        return when (getItem(position)) {
            is TravelLogItem.TripLogItem -> R.layout.row_travel_log_item
            null -> throw UnsupportedOperationException("Unknown view")
        }
    }

    interface TripItemListener {
        fun onItemClick(item: TripsItem)
        fun onItemSelected(isSelected: Boolean, tripId: Long)
    }

    inner class ViewHolder(private val viewBinding: RowTravelLogItemBinding) :
        RecyclerView.ViewHolder(viewBinding.root) {
//        private val originName: TextView = view.findViewById(R.id.origin_name)
//        private val destinationName: TextView = view.findViewById(R.id.destination_name)
//        private val totalDistance: TextView = view.findViewById(R.id.total_distance)
//        private val travelTime: TextView = view.findViewById(R.id.travel_time)
//        private val travelDate: TextView = view.findViewById(R.id.travel_date)
//        private val travelCost: TextView = view.findViewById(R.id.travel_cost)
//        private val selectCheckbox: CheckBox = view.findViewById(R.id.select_checkbox)

        lateinit var tripItem: TripsItem

        fun bind(repo: TravelLogItem.TripLogItem?) {
            repo?.let {
                showRepoData(it)
            }
        }

        private fun showRepoData(travelLog: TravelLogItem.TripLogItem) {
            this.tripItem = travelLog.tripLogItem

            viewBinding.originName.text = tripItem.tripStartAddress1
            viewBinding.destinationName.text = tripItem.tripDestAddress1

            viewBinding.totalDistance.text =
                tripItem.totalDistance?.let { getDistanceText(it.toFloat()) } ?: ""

            if (tripItem.tripStartTime != null && tripItem.tripEndTime != null) {
                viewBinding.travelTime.text =
                    getTravelledTimeString(tripItem.tripStartTime!!, tripItem.tripEndTime!!)
            } else {
                viewBinding.travelTime.text = ""
            }

            viewBinding.travelDate.text = tripItem.tripStartTime?.let { getDateText(it) }

            viewBinding.selectCheckbox.isVisible = showCheckBoxIcon

            viewBinding.selectCheckbox.setOnCheckedChangeListener { buttonView, isChecked ->
                /*if(!isChecked)
                    selectAll = false*/
                travelLog.isCheckboxSelected = isChecked
                listener.onItemSelected(
                    isChecked,
                    travelLog.tripLogItem.tripId
                )
            }
            viewBinding.selectCheckbox.isChecked = travelLog.isCheckboxSelected
            /*if(selectAll){
                checkBox.isChecked = true
            }*/

        }

        private fun getDistanceText(distance: Float): String {
            return distance.roundTo().toString() + "km"
        }

        private fun Float.roundTo(): Float {
            return "%.${2}f".format(Locale.ENGLISH, this).toFloat()
        }

        //Convert time to local format
        private fun getTravelledTimeString(tripStartTime: Long, tripEndTime: Long): String {
            return String.format("%s - %s", getTimeText(tripStartTime), getTimeText(tripEndTime))
        }

        fun getTimeText(time: Long): String {
            return try {
                var date = Utils.CURRENT_DATE()
                date.time = time * 1000L
                var calendar = Calendar.getInstance()
                calendar.time = date
                calendar.timeZone = TimeZone.getDefault()

                val df = SimpleDateFormat("hh:mmaa", Locale.US)
                val formatTime = df.format(calendar.time)
                formatTime.toLowerCase()
            } catch (e: Exception) {
                ""
            }
        }

        fun getDateText(time: Long): String {
            return try {
                var date = Utils.CURRENT_DATE()
                date.time = time * 1000L
                var calendar = Calendar.getInstance()
                calendar.time = date
                calendar.timeZone = TimeZone.getDefault()
                val dayWeekText = SimpleDateFormat("EEE", Locale.US).format(date)

                val day = SimpleDateFormat("dd", Locale.US).format(date)
                val monthString = SimpleDateFormat("MMM", Locale.US).format(date)
                val yearString = SimpleDateFormat("YYYY", Locale.US).format(date)

                String.format("%s, %s %s, %s", dayWeekText, day, monthString, yearString)
            } catch (e: Exception) {
                ""
            }
        }
    }

    fun setCheckBoxVisibility(isVisible: Boolean) {
        showCheckBoxIcon = isVisible
        this.notifyDataSetChanged()
    }


}

package com.ncs.breeze.common.helper.ehorizon

import android.content.Context
import com.breeze.model.NavigationZone
import com.mapbox.maps.Style
import com.mapbox.navigation.base.road.model.RoadComponent
import com.ncs.breeze.R
import com.ncs.breeze.common.utils.TripLoggingManager
import com.ncs.breeze.common.utils.VoiceInstructionsManager
import java.lang.ref.WeakReference
import java.util.concurrent.CopyOnWriteArraySet

class BreezeRoadObjectHandler(
    context: Context,
    val mode: EHorizonMode,
) {
    var voiceEnabled = true
    var roadObjectID: String = "";
    private val contextRef = WeakReference(context)

    private val breezeEdgeMetaDataObserverList = CopyOnWriteArraySet<BreezeEdgeMetaDataObserver>()
    private val breezeNotificationObserverList = CopyOnWriteArraySet<BreezeNotificationObserver>()
    private val breezeNotificationStyleList = CopyOnWriteArraySet<Style>()
    private val breezeRoadObjectStateHandlerList =
        CopyOnWriteArraySet<BreezeRoadObjectStateHandler>()

    private val voicePlayBackStateMap by lazy {
        NavigationZone.values().associate { zone ->
            val playbackState = when (zone) {
                //ERP
                NavigationZone.ERP_ZONE -> PlayBackState(
                    true,
                    context.getString(R.string.voice_erp),
                    context.getString(R.string.voice_erp_base)
                )
                //Incidents
                NavigationZone.ACCIDENT -> PlayBackState(
                    true,
                    context.getString(R.string.voice_accident),
                    context.getString(R.string.voice_accident_base)
                )

                NavigationZone.VEHICLE_BREAKDOWN -> PlayBackState(
                    true,
                    context.getString(R.string.voice_vehiclebreakdown),
                    context.getString(R.string.voice_vehiclebreakdown_base)
                )

                NavigationZone.ROAD_WORK -> PlayBackState(
                    true,
                    context.getString(R.string.voice_roadwork),
                    context.getString(R.string.voice_roadwork_base)
                )

                NavigationZone.OBSTACLE -> PlayBackState(
                    true,
                    context.getString(R.string.voice_obstacle),
                    context.getString(R.string.voice_obstacle_base)
                )

                NavigationZone.ROAD_BLOCK -> PlayBackState(
                    true,
                    context.getString(R.string.voice_roadblock),
                    context.getString(R.string.voice_roadblock_base)
                )

                NavigationZone.MISCELLANEOUS -> PlayBackState(
                    true,
                    context.getString(R.string.voice_miscellaneous),
                    context.getString(R.string.voice_miscellaneous_base)
                )

                NavigationZone.UNATTENDED_VEHICLE -> PlayBackState(
                    true,
                    context.getString(R.string.voice_unattended),
                    context.getString(R.string.voice_unattended_base)
                )

                NavigationZone.SPEED_CAMERA -> PlayBackState(
                    true,
                    context.getString(R.string.voice_speed_camera),
                    context.getString(R.string.voice_speed_camera_base)
                )

                NavigationZone.TREE_PRUNING -> PlayBackState(
                    true,
                    context.getString(R.string.voice_tree_pruning),
                    context.getString(R.string.voice_tree_pruning_base)
                )

                NavigationZone.ROAD_CLOSURE -> PlayBackState(
                    true,
                    context.getString(R.string.voice_road_closure),
                    context.getString(R.string.voice_road_closure_base)
                )

                NavigationZone.HEAVY_TRAFFIC -> PlayBackState(
                    true,
                    context.getString(R.string.voice_heavy_traffic),
                    context.getString(R.string.voice_heavy_traffic_base)
                )

                NavigationZone.FLASH_FLOOD -> PlayBackState(
                    true,
                    context.getString(R.string.voice_flash_flood),
                    context.getString(R.string.voice_flash_flood_base)
                )

                NavigationZone.MAJOR_ACCIDENT -> PlayBackState(
                    true,
                    context.getString(R.string.voice_major_accident),
                    context.getString(R.string.voice_major_accident_base)
                )

                NavigationZone.SEASON_PARKING -> PlayBackState(
                    true,
                    context.getString(R.string.voice_season_parking_base),
                    context.getString(R.string.voice_season_parking_base)
                )

                //Silver Zone / School Zone
                NavigationZone.SILVER_ZONE -> {
                    if (mode == EHorizonMode.NAVIGATION) {
                        PlayBackState(
                            true,
                            context.getString(R.string.voice_approaching_silverzone),
                            context.getString(R.string.voice_silverzone_base)
                        )
                    } else {
                        PlayBackState(
                            true,
                            context.getString(R.string.voice_silverzone),
                            context.getString(R.string.voice_silverzone_base)
                        )
                    }
                }

                NavigationZone.SCHOOL_ZONE -> {
                    if (mode == EHorizonMode.NAVIGATION) {
                        PlayBackState(
                            true,
                            context.getString(R.string.voice_approaching_schoolzone),
                            context.getString(R.string.voice_schoolzone_base)
                        )
                    } else {
                        PlayBackState(
                            true,
                            context.getString(R.string.voice_schoolzone),
                            context.getString(R.string.voice_schoolzone_base)
                        )
                    }
                }

                else -> PlayBackState(
                    false,
                    context.getString(R.string.voice_miscellaneous),
                    context.getString(R.string.voice_miscellaneous_base)
                )
            }
            return@associate Pair(zone, playbackState)
        }
    }

    internal fun removeAllNotificationViews(roadObjectId: String) {
        breezeNotificationObserverList.forEach {
            it.removeNotificationView(roadObjectId)
        }
    }

    internal fun updateStyleList(handler: (style: Style) -> Unit = {}) {
        breezeNotificationStyleList.forEach {
            handler.invoke(it)
        }
    }


    internal fun updateEdgeMetaData(speedLimit: Double?, names: List<RoadComponent>?, tunnel: Boolean) {
        breezeEdgeMetaDataObserverList.forEach {
            it.updateEdgeMetaData(speedLimit, names, tunnel)
        }
    }


    fun showNotificationView(
        zone: NavigationZone,
        distance: Int,
        mERPName: String?,
        chargeAmount: String?,
        roadObjectID: String,
        location: String?
    ) {
        this.roadObjectID = roadObjectID
        breezeNotificationObserverList.forEach {
            it.showNotificationView(zone, distance, mERPName, chargeAmount, roadObjectID, location)
        }
    }

    fun handleTripLogging(mERPID: String, mERPName: String, chargeAmount: String, time: Long) {
        TripLoggingManager.getInstance()?.logErpDetection(mERPID, mERPName, chargeAmount, time)
    }

    internal fun playVoiceOnce(zone: NavigationZone) {
        if (!voiceEnabled) return
        voicePlayBackStateMap[zone]?.let { playBackState ->
            VoiceInstructionsManager.getInstance()
                ?.playVoiceInstructions(playBackState.voiceInstruction)
        }
    }

    internal fun playCustomVoiceOnce(zone: NavigationZone, location: String?) {
        if (!voiceEnabled || location == null) return
        voicePlayBackStateMap[zone]?.let { playBackState ->
            contextRef.get()?.getString(
                R.string.voice_incident_custom,
                playBackState.voiceInstructionBase,
                location
            )?.let { instruction ->
                VoiceInstructionsManager.getInstance()?.playVoiceInstructions(instruction)
            }
        }
    }


    fun stopVoice() {
        VoiceInstructionsManager.getInstance()?.cancelSpeech()
    }


    fun addBreezeEdgeMetaDataObserver(breezeEdgeMetaDataObserver: BreezeEdgeMetaDataObserver) {
        breezeEdgeMetaDataObserverList.add(breezeEdgeMetaDataObserver)
    }

    fun removeBreezeEdgeMetaDataObserver(breezeEdgeMetaDataObserver: BreezeEdgeMetaDataObserver) {
        breezeEdgeMetaDataObserverList.remove(breezeEdgeMetaDataObserver)
    }

    fun addBreezeNotificationObserver(breezeNotificationObserver: BreezeNotificationObserver) {
        breezeNotificationObserverList.add(breezeNotificationObserver)
    }

    fun removeBreezeNotificationObserver(breezeNotificationObserver: BreezeNotificationObserver) {
        breezeNotificationObserverList.remove(breezeNotificationObserver)
    }

    fun addBreezeNotificationStyle(style: Style) {
        breezeNotificationStyleList.add(style)
    }

    fun removeBreezeNotificationStyle(style: Style) {
        breezeNotificationStyleList.remove(style)
    }


    internal fun addBreezeRoadObjectStateHandler(breezeRoadObjectStateHandler: BreezeRoadObjectStateHandler) {
        breezeRoadObjectStateHandlerList.add(breezeRoadObjectStateHandler)
    }

    internal fun removeBreezeRoadObjectStateHandler(breezeRoadObjectStateHandler: BreezeRoadObjectStateHandler) {
        breezeRoadObjectStateHandlerList.add(breezeRoadObjectStateHandler)
    }


    fun removeAllObservers() {
        breezeEdgeMetaDataObserverList.clear()
        breezeNotificationObserverList.clear()
        breezeNotificationStyleList.clear()
        breezeRoadObjectStateHandlerList.clear()
    }

    fun removeLastNotification(
        style: Style?,
        breezeNotificationObserver: BreezeNotificationObserver
    ) {
        roadObjectID.let { rObj ->
            breezeRoadObjectStateHandlerList.forEach {
                it.onTerminateNotification(style, rObj)
            }
            breezeNotificationObserver.removeNotificationView(rObj)
        }
    }

    companion object {
        internal const val SCHOOL_GEOJSONSOURCE = "SCHOOL_GEOJSONSOURCE"
        internal const val SCHOOL_ICON = "SCHOOL_ICON"
        internal const val SCHOOL_LAYER = "SCHOOL_LAYER"

        internal const val SILVER_GEOJSONSOURCE = "SILVER_GEOJSONSOURCE"
        internal const val SILVER_ICON = "SILVER_ICON"
        internal const val SILVER_LAYER = "SILVER_LAYER"

        internal const val SCHOOL_LAYER_DEBUG = "SCHOOL_LAYER_DEBUG"
        internal const val SILVER_LAYER_DEBUG = "SILVER_LAYER_DEBUG"
        internal const val SCHOOL_GEOJSONSOURCE_DEBUG = "SCHOOL_GEOJSONSOURCE_DEBUG"
        internal const val SILVER_GEOJSONSOURCE_DEBUG = "SILVER_GEOJSONSOURCE_DEBUG"
    }

}

data class PlayBackState(
    var isPlayBackEnabled: Boolean,
    val voiceInstruction: String,
    val voiceInstructionBase: String
)

data class RoadObjectNotificationData(
    var zone: NavigationZone? = null,
    var distance: Int? = null,
    var roadObjectId: String? = null
)



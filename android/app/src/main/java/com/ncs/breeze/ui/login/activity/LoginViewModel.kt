package com.ncs.breeze.ui.login.activity

import android.app.Application
import androidx.lifecycle.viewModelScope
import com.breeze.model.api.ErrorData
import com.ncs.breeze.common.remote.ApiHelper
import com.ncs.breeze.common.remote.ApiObserver
import com.breeze.model.api.response.UserDataResponse
import com.ncs.breeze.components.SingleLiveEvent
import com.ncs.breeze.ui.base.BaseViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.launch
import javax.inject.Inject

class LoginViewModel @Inject constructor(application: Application, apiHelper: ApiHelper) :
    BaseViewModel(application, apiHelper) {

    var tncAccepted: SingleLiveEvent<Boolean> = SingleLiveEvent()
    var amenityResetResponse: SingleLiveEvent<Boolean> = SingleLiveEvent()

    /**
     * fetch UserData
     */
    fun getUserData() {
        viewModelScope.launch {
            apiHelper.getUserData()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : ApiObserver<UserDataResponse>(compositeDisposable) {
                    override fun onSuccess(data: UserDataResponse) {
                        tncAccepted.value = data.tncAcceptStatus!!
                        breezeUserPreferenceUtil.saveUserCreatedDate(data.createdTimeStamp)
                    }

                    override fun onError(e: ErrorData) {

                    }
                })
        }
    }
}
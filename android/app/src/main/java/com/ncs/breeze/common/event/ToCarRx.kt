package com.ncs.breeze.common.event

import com.ncs.breeze.common.model.rx.ToCarRxEvent
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject


object ToCarRx {

    private val publishSubject = PublishSubject.create<ToCarRxEvent>()
    private val behaviorSubject = BehaviorSubject.create<ToCarRxEvent>()

    fun postEvent(event: ToCarRxEvent) {
        publishSubject.onNext(event)
    }

    fun postEventWithCacheLast(event: ToCarRxEvent) {
        behaviorSubject.onNext(event)
    }

    fun listenEvent(eventType: Class<ToCarRxEvent>): Observable<ToCarRxEvent> =
        publishSubject.ofType(eventType)

    fun listenEventWithLastCache(eventType: Class<ToCarRxEvent>): Observable<ToCarRxEvent> =
        behaviorSubject.ofType(eventType)
}


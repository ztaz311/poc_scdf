package com.ncs.breeze.ui.dashboard.fragments.viewmodel

import android.app.Application
import com.breeze.model.api.ErrorData
import com.breeze.model.api.request.FeedbackRequest
import com.ncs.breeze.common.remote.ApiHelper
import com.ncs.breeze.common.remote.ApiObserver
import com.ncs.breeze.common.utils.BreezeUserPreferenceUtil
import com.google.gson.JsonElement
import com.ncs.breeze.components.SingleLiveEvent
import com.ncs.breeze.ui.base.BaseFragmentViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject

class FeedbackViewModel @Inject constructor(apiHelper: ApiHelper, application: Application) :
    BaseFragmentViewModel(
        application
    ) {
    var mAPIHelper: ApiHelper = apiHelper

    @Inject
    lateinit var breezeUserPreferenceUtil: BreezeUserPreferenceUtil

    var sendFeedbackSuccessful: SingleLiveEvent<Boolean> = SingleLiveEvent()

    fun sendFeedback(request: FeedbackRequest) {
        //Send Feedback

        mAPIHelper.sendFeedBack(request)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : ApiObserver<JsonElement>(compositeDisposable) {
                override fun onSuccess(data: JsonElement) {
                    sendFeedbackSuccessful.value = true
                }

                override fun onError(e: ErrorData) {
                    Timber.d("API failed: " + e.toString())
                    sendFeedbackSuccessful.value = false
                }

            })
    }


}
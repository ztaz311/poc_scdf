package com.ncs.breeze.components

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.ncs.breeze.common.model.DepartureType
import com.ncs.breeze.common.model.RouteDepartureDetails
import com.breeze.model.RoutePlanningMode
import com.ncs.breeze.common.model.SimpleRouteAdapterObjects
import com.ncs.breeze.common.utils.RouteDisplayUtils
import com.ncs.breeze.common.utils.Utils
import com.mapbox.api.directions.v5.models.DirectionsRoute
import com.ncs.breeze.R
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.roundToInt


class RouteViewPagerAdapter internal constructor(
    context: Context,
    routeDepartureDetails: RouteDepartureDetails,
    routeplanningMode: RoutePlanningMode,
    data: List<SimpleRouteAdapterObjects>,
    recyclerView: RecyclerView,
) : RecyclerView.Adapter<RouteViewPagerAdapter.ViewHolder>() {

    private var mData: List<SimpleRouteAdapterObjects> = data
    private val mInflater: LayoutInflater = LayoutInflater.from(context)
    private val mRecyclerView: RecyclerView = recyclerView
    private var mContext: Context = context
    private val mRouteplanningMode: RoutePlanningMode = routeplanningMode
    private val mRouteDepartureDetails: RouteDepartureDetails = routeDepartureDetails

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = mInflater.inflate(R.layout.adapter_card_pager, parent, false)
        val width: Int = mRecyclerView.width
        val params = view.layoutParams
        if (mData.size > 1) {
            params.width = (width * 0.92).toInt()
        } else {
            params.width = (width * 0.95).toInt()
        }
        view.layoutParams = params
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val routeData = mData[position]
        val arrivalTime = routeData.arrivalTime
        holder.tvArrivalTime.text = RouteDisplayUtils.formatArrivalTimeWithTime12H(arrivalTime)
        holder.tvDuration.text = String.format("%d", routeData.duration)


        //Displaying distance
        if (routeData.distance < 1000) {
            // Distance is lesser than 1 km
            holder.tvDistance.text = routeData.distance.roundToInt().toString()
            holder.tvDistanceUnit.text = mContext.getString(R.string.route_card_metres)
        } else {
            //holder.tvDistance.text =  String.format("%.2f", Utils.convertMeterToKilometer(routeData.distance))
            holder.tvDistance.text =
                Utils.roundOffDecimal(Utils.convertMeterToKilometer(routeData.distance)).toString()
            holder.tvDistanceUnit.text = mContext.getString(R.string.route_card_km)
        }

        if (routeData.erpRate != null) {
            if (routeData.erpRate!! > 0.0) {
                var chargeAmountStr = "%.2f".format(routeData.erpRate)
                chargeAmountStr = "$$chargeAmountStr"

                holder.tvERPRate.text = chargeAmountStr
            } else {
                holder.tvERPRate.text = mContext.getString(R.string.route_no_erp_rate)
            }
        } else {
            holder.tvERPRate.text = ""
        }

        if (mRouteplanningMode != RoutePlanningMode.NORMAL) {
            if (mRouteDepartureDetails.type == DepartureType.DEPARTURE) {
                holder.tvArrivalText.text = mContext.getString(R.string.depart_at_card_text)
                holder.tvDurationText.text = mContext.getString(R.string.duration_card_text)
            } else {
                updateDepartCardDateText(routeData, holder)
                holder.tvArrivalText.text = mContext.getString(R.string.arrive_by_card_text)
                holder.tvDurationText.text = mContext.getString(R.string.duration_card_text)
            }
        } else {
            holder.tvArrivalText.text = mContext.getString(R.string.route_card_arrive)
            holder.tvDurationText.text = mContext.getString(R.string.route_card_duration)
        }

    }


    override fun getItemCount(): Int {
        return mData.size
    }

    private fun updateDepartCardDateText(routeData: SimpleRouteAdapterObjects, holder: ViewHolder) {
        val departDate = Date(mRouteDepartureDetails.timestamp - routeData.duration * 60 * 1000)
        val dateFormat = SimpleDateFormat("a")
        val dateFormatHHMM = SimpleDateFormat("hh:mmaa")
        holder.tvArrivalTime.text = dateFormatHHMM.format(departDate)
    }


    fun updateERPRates(data: List<SimpleRouteAdapterObjects>) {
        mData = data
        notifyDataSetChanged()
    }

    inner class ViewHolder internal constructor(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var tvArrivalTime: TextView = itemView.findViewById(R.id.tv_arrival_time)
        var tvDuration: TextView = itemView.findViewById(R.id.tv_time_left)
        var tvDistance: TextView = itemView.findViewById(R.id.tv_distance_left)
        var tvDistanceUnit: TextView = itemView.findViewById(R.id.tv_distance_unit)
        var tvERPRate: TextView = itemView.findViewById(R.id.tv_erp)
        var tvDurationText: TextView
        var tvArrivalText: TextView

        init {
            tvERPRate = itemView.findViewById(R.id.tv_erp)
            tvDurationText = itemView.findViewById(R.id.tv_duration_text)
            tvArrivalText = itemView.findViewById(R.id.tv_arrival_text)
        }
    }

    interface RouteViewPagerListener {
        fun onConfirmRouteClick(mData: DirectionsRoute)
    }
}
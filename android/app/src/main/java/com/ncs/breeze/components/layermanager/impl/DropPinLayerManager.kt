package com.ncs.breeze.components.layermanager.impl

import androidx.annotation.UiThread
import com.breeze.model.extensions.toBitmap
import com.google.android.gms.maps.model.LatLng
import com.google.gson.JsonObject
import com.mapbox.geojson.Feature
import com.mapbox.geojson.Point
import com.mapbox.maps.MapView
import com.mapbox.maps.extension.style.expressions.dsl.generated.eq
import com.mapbox.maps.extension.style.expressions.generated.Expression
import com.mapbox.maps.extension.style.layers.addLayer
import com.mapbox.maps.extension.style.layers.addLayerAbove
import com.mapbox.maps.extension.style.layers.generated.symbolLayer
import com.mapbox.maps.extension.style.layers.properties.generated.IconAnchor
import com.ncs.breeze.R
import com.ncs.breeze.common.utils.ParkingIconUtils
import com.ncs.breeze.components.layermanager.MarkerLayerManager
import com.ncs.breeze.components.marker.markerview2.DropPinMarkerTooltip
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class DropPinLayerManager(mapView: MapView) : MarkerLayerManager(mapView) {

    override suspend fun addStyleImages(type: String) {
        val STYLE_IMAGES: HashMap<String, Int> = hashMapOf(
            "$type-${ParkingIconUtils.ImageIconType.SELECTED_NIGHT.type}$ICON_POSTFIX" to R.drawable.ic_map_pin,
            "$type-${ParkingIconUtils.ImageIconType.UNSELECTED_NIGHT.type}$ICON_POSTFIX" to R.drawable.ic_map_pin,
            "$type-${ParkingIconUtils.ImageIconType.SELECTED.type}$ICON_POSTFIX" to R.drawable.ic_map_pin,
            "$type-${ParkingIconUtils.ImageIconType.UNSELECTED.type}$ICON_POSTFIX" to R.drawable.ic_map_pin,
        )

        CoroutineScope(Dispatchers.Main).launch {
            for ((key, value) in STYLE_IMAGES) {
                mapViewRef.get()?.context
                    ?.let { context -> value.toBitmap(context) }
                    ?.let {
                        mapViewRef.get()?.getMapboxMap()?.getStyle()?.addImage(key, it, false)
                    }
            }
        }
    }

    override fun shouldClearMarkersOnMapFreeClick() = false

    fun setAnalyticsScreenName(name: String) {
        (marker?.markerView2 as? DropPinMarkerTooltip)?.analyticsScreenName = name
    }

    fun initDropPinMarkerTooltip(mapView: MapView) {
        val tooltip =
            DropPinMarkerTooltip(mapView, DROP_PIN_MARKER, DropPinMarkerTooltip.Screen.LANDING)
                .apply {
                    onCloseTooltip = {
                        (marker?.markerView2 as? DropPinMarkerTooltip)?.hideTooltip()
                        removeMarkers(DROP_PIN_MARKER)
                        removeMarkerTooltipView(DROP_PIN_MARKER)
                    }
                }
        marker = MarkerViewType(
            markerView2 = tooltip,
            DROP_PIN_MARKER,
            Feature.fromGeometry(Point.fromLngLat(0.0, 0.0))
        )
        mapView.addView(tooltip.mViewMarker)
        tooltip.hideMarker()
    }

    override fun addStyleLayer(
        type: String,
        sourceId: String,
        iconSizeExp: Expression
    ) {
        symbolLayer(type + LAYER_POSTFIX, sourceId) {
            addIconImageExpression(this, type)
            iconSize(iconSizeExp)
            iconAnchor(IconAnchor.BOTTOM)
            iconAllowOverlap(true)
            iconIgnorePlacement(true)
            textAllowOverlap(true)
            textIgnorePlacement(true)
            filter(eq {
                get {
                    literal(HIDDEN)
                }
                literal(false)
            })
        }.let { symbolLayer ->
            if (mapViewRef.get()?.getMapboxMap()?.getStyle()?.styleLayers.isNullOrEmpty()) {
                mapViewRef.get()?.getMapboxMap()?.getStyle()?.addLayer(symbolLayer)
            } else {
                mapViewRef.get()?.getMapboxMap()?.getStyle()?.addLayerAbove(
                    symbolLayer,
                    mapViewRef.get()?.getMapboxMap()?.getStyle()?.styleLayers?.last()?.id
                )
            }
        }
    }

    //
//    @UiThread
//    override fun renderMarkerToolTipView(marker: MarkerViewType) {
//        removeMarkerTooltipView()
//        this.marker = marker
//        mapViewRef.get()?.addView(marker.markerView2.mViewMarker)
//        marker.markerView2.update()
//        addOrReplaceMarker(marker.type, marker.feature)
//    }
    @UiThread
    fun showMakerWithTooltip(location: Point) {
        val properties = JsonObject()
        properties.addProperty(HIDDEN, false)
        val feature = Feature.fromGeometry(location, properties, DROP_PIN_MARKER)
        marker?.markerView2?.hideMarker()
        this.marker?.markerView2?.mLatLng = LatLng(location.latitude(), location.longitude())
//        this.marker?.markerView2?.update()
        if (marker?.markerView2 !is DropPinMarkerTooltip) {
            mapViewRef.get()?.let { mapview ->
                initDropPinMarkerTooltip(mapview)
                (marker?.markerView2 as? DropPinMarkerTooltip)?.mLatLng = LatLng(location.latitude(), location.longitude())
            }
        }
        addOrReplaceMarker(DROP_PIN_MARKER, feature)
        (marker?.markerView2 as? DropPinMarkerTooltip)?.showTooltip()
    }

    @UiThread
    fun hideTooltips() {
        (marker?.markerView2 as? DropPinMarkerTooltip)?.hideTooltip()
    }

    fun removeMarkerIcons() {
        markerFeatures[DROP_PIN_MARKER] ?: return
        removeSourceAndLayer(DROP_PIN_MARKER)
        markerFeatures.remove(DROP_PIN_MARKER)
    }

    companion object {
        const val DROP_PIN_MARKER = "drop-pin"
        const val DROP_PIN_LAYER_KEY = "$DROP_PIN_MARKER$LAYER_POSTFIX"
        const val DROP_PIN_SOURCE_KEY = "$DROP_PIN_MARKER$SOURCE_POSTFIX"
    }
}
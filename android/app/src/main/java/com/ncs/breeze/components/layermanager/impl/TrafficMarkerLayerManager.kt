package com.ncs.breeze.components.layermanager.impl

import com.breeze.model.extensions.toBitmap
import com.mapbox.maps.MapView
import com.ncs.breeze.R
import com.ncs.breeze.common.utils.ParkingIconUtils.ImageIconType
import com.ncs.breeze.components.layermanager.MarkerLayerManager
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class TrafficMarkerLayerManager(
    mapView: MapView
) : MarkerLayerManager(mapView) {

    override suspend fun addStyleImages(type: String) {
        val STYLE_IMAGES: HashMap<String, Int> = hashMapOf(
            "$type-${ImageIconType.SELECTED_NIGHT.type}$ICON_POSTFIX" to R.drawable.ic_traffic_camera_selelcted_dark,
            "$type-${ImageIconType.UNSELECTED_NIGHT.type}$ICON_POSTFIX" to R.drawable.ic_traffic_camera_unselelcted_dark,
            "$type-${ImageIconType.SELECTED.type}$ICON_POSTFIX" to R.drawable.ic_traffic_camera_selelcted_light,
            "$type-${ImageIconType.UNSELECTED.type}$ICON_POSTFIX" to R.drawable.ic_traffic_camera_unselelcted_light,
        )

        CoroutineScope(Dispatchers.Main).launch {
            for ((key, value) in STYLE_IMAGES) {
                mapViewRef.get()?.context
                    ?.let { context -> value.toBitmap(context) }
                    ?.let {
                    mapViewRef.get()?.getMapboxMap()?.getStyle()?.addImage(key, it, false)
                }
            }
        }
    }

}
package com.ncs.breeze.ui.dashboard.fragments.obulite

import android.os.Handler
import android.os.Looper

class OBUCheckSpeedHelper {
    var currentState: SpeedState = SpeedState.UNDEFINED
    private var lastState = SpeedState.UNDEFINED
    private var currentSpeed = -1.0
    private var handler: Handler? = null
    private var callback: ((SpeedState, Boolean) -> Unit)? = null

    init {
        handler = Handler(Looper.getMainLooper()) {
            checkSpeed()
            handler?.sendEmptyMessageDelayed(111, 3000)
            false
        }
        handler?.sendEmptyMessageDelayed(111, 3000)
    }


    private fun checkSpeed() {
        if (currentSpeed > 0.0) {
            currentState = SpeedState.RUNNING
        } else if (currentSpeed == 0.0) {
            currentState = SpeedState.STATIONARY
        }

        callback?.invoke(currentState, lastState != currentState)
    }

    fun changeSpeed(speed: Double) {
        lastState = currentState
        currentSpeed = speed
    }

    fun setStateChangeCallback(callback: (SpeedState, Boolean) -> Unit) {
        this.callback = callback
    }

    fun destroy() {
        handler?.removeCallbacksAndMessages(null)
        handler = null
    }

    enum class SpeedState {
        UNDEFINED,
        RUNNING,
        STATIONARY
    }
}

fun OBUCheckSpeedHelper?.isStationary(): Boolean =
    this?.currentState == OBUCheckSpeedHelper.SpeedState.STATIONARY
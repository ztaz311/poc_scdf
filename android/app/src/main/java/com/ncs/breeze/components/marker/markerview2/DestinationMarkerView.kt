package com.ncs.breeze.components.marker.markerview2

import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import com.breeze.model.DestinationAddressDetails
import com.breeze.model.DestinationSearch
import com.breeze.model.api.response.amenities.AvailabilityCSData
import com.breeze.model.constants.Constants
import com.breeze.model.enums.AmenityBand
import com.breeze.model.extensions.dpToPx
import com.breeze.model.extensions.safeDouble
import com.facebook.react.bridge.Arguments
import com.google.android.gms.maps.model.LatLng
import com.mapbox.geojson.Point
import com.mapbox.maps.EdgeInsets
import com.mapbox.maps.MapboxMap
import com.ncs.breeze.R
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.extensions.android.getShareBusinessLogicPackage
import com.ncs.breeze.common.utils.BreezeMapZoomUtil
import com.ncs.breeze.common.utils.LocationBreezeManager
import com.ncs.breeze.databinding.MapTooltipDestinationBinding
import com.ncs.breeze.reactnative.nativemodule.ShareBusinessLogicEvent
import com.ncs.breeze.ui.base.BaseActivity
import com.ncs.breeze.ui.dashboard.activity.DashboardActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class DestinationMarkerView(
    latLng: LatLng,
    mapboxMap: MapboxMap,
    val activity: Activity,
    itemId: String? = null
) : MarkerView2(mapboxMap = mapboxMap, itemId) {

    private var viewBinding: MapTooltipDestinationBinding

    private var hashMapDestinationInfo: HashMap<String, Any>? = null
    var destinationInfo: DestinationSearch? = null
    var analyticsScreenName = ""

    init {
        mLatLng = latLng
        viewBinding = MapTooltipDestinationBinding.inflate(
            activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater,
            null,
            false
        )
        mViewMarker = viewBinding.root
    }

    fun handleClick(
        hashMap: HashMap<String, Any>,
        item: DestinationSearch,
        bookmarkIconClick: (BaseActivity.EventSource) -> Unit,
        removeBookmarkIconClick: (BaseActivity.EventSource) -> Unit
    ) {
        destinationInfo = item
        hashMapDestinationInfo = hashMap
        val routablePointText =
            if (item.routablePoint?.name.isNullOrEmpty()) "" else " - ${item.routablePoint?.name}"
        viewBinding.tvName.text = "${item.address1}${routablePointText}"
        viewBinding.tvDescription.text = item.fullAddress.ifEmpty {
            item.address2
        }
        viewBinding.layoutCarparkStatus.icCarparkStatus

        viewBinding.imgFlagSaved.setImageDrawable(
            ContextCompat.getDrawable(
                viewBinding.root.context,
                if (item.isBookmarked == true) R.drawable.ic_bookmarked else R.drawable.ic_nobookmark
            )
        )
        viewBinding.imgFlagSaved.setOnClickListener {
            resetView()
            if (item.isBookmarked == true) {
                removeBookmarkIconClick.invoke(BaseActivity.EventSource.NONE)
            } else {
                Analytics.logClickEvent("[map]:tooltip_destination_bookmark", analyticsScreenName)
                bookmarkIconClick.invoke(BaseActivity.EventSource.NONE)
            }
        }

        viewBinding.buttonShare.setOnClickListener {
            Analytics.logClickEvent("[map]:tooltip_destination_share", analyticsScreenName)
            shareLocation(item)
        }

        viewBinding.buttonNavigateHere.setOnClickListener {
            navigateToLocation(hashMap)
        }

        viewBinding.buttonInvite.setOnClickListener {
            inviteLocation(item)
        }
        renderMarkerToolTip()
        displayCarparkStatus(hashMap)
    }

    private fun displayCarparkStatus(data: HashMap<String, Any>) {
        val availabilityCSData =
            (data["availabilityCSData"] as? HashMap<String, Any>)?.let { AvailabilityCSData.fromRNHashMapData(it) }
        val hasAvailabilityCS = data["availabilityCSData"] != null
        viewBinding.layoutCarparkStatus.root.isVisible = hasAvailabilityCS
        if (availabilityCSData == null) return
        val title: String = availabilityCSData.title ?: ""
//            Calculate last update time
        val timeUpdate =
            "${availabilityCSData.primaryDesc ?: activity.getString(com.breeze.customization.R.string.update_carpark_stt_time)} ${availabilityCSData.generateDisplayedLastUpdate()}"
        val drawableHeaderCpStatus =
            AppCompatResources.getDrawable(activity, R.drawable.bg_header_carpark_status)
        drawableHeaderCpStatus?.setTint(Color.parseColor(availabilityCSData.themeColor ?: "#F26415"))
        viewBinding.layoutCarparkStatus.root.background = drawableHeaderCpStatus
        viewBinding.layoutCarparkStatus.icCarparkStatus.setImageResource(
            when (data["availablePercentImageBand"] as? String) {
                AmenityBand.TYPE0.type -> R.drawable.ic_cp_stt_full
                AmenityBand.TYPE2.type -> R.drawable.ic_cp_stt_available
                else -> R.drawable.ic_cp_stt_few
            }
        )
        viewBinding.layoutCarparkStatus.tvStatusCarpark.text = title
        viewBinding.layoutCarparkStatus.tvTimeUpdateCarparkStatus.text = timeUpdate
    }

    private fun navigateToLocation(destinationData: HashMap<String, Any>) {
        val currentLocation = LocationBreezeManager.getInstance().currentLocation ?: return
        val destinationAddress =
            DestinationAddressDetails.parseSearchLocationDataFromRN(destinationData)
        val sourceAddressDetails = DestinationAddressDetails(
            easyBreezyAddressID = null,
            address1 = null,
            address2 = null,
            lat = currentLocation.latitude.toString(),
            long = currentLocation.longitude.toString(),
            distance = null,
            destinationName = Constants.TAGS.CURRENT_LOCATION_TAG,
            destinationAddressType = null,
            ranking = -1
        )
        (activity as? DashboardActivity)?.run {
            runOnUiThread {
                showRouteAndResetToCenter(
                    bundleOf(
                        Constants.DESTINATION_ADDRESS_DETAILS to destinationAddress,
                        Constants.SOURCE_ADDRESS_DETAILS to sourceAddressDetails
                    )
                )
            }
        }
    }

    private fun shareLocation(item: DestinationSearch) {
        viewBinding.root.context.getShareBusinessLogicPackage()?.getRequestsModule()?.flatMap {
            it.callRN(
                ShareBusinessLogicEvent.SHARE_LOCATION_FROM_NATIVE.eventName,
                createShareLocationData(item)
            )
        }?.subscribeOn(Schedulers.io())?.observeOn(AndroidSchedulers.mainThread())?.subscribe()
    }

    private fun inviteLocation(item: DestinationSearch) {
        viewBinding.root.context.getShareBusinessLogicPackage()?.getRequestsModule()?.flatMap {
            it.callRN(
                ShareBusinessLogicEvent.LOCATION_INVITE_FROM_NATIVE.eventName,
                createShareLocationData(item)
            )
        }?.subscribeOn(Schedulers.io())?.observeOn(AndroidSchedulers.mainThread())?.subscribe()
    }

    private fun createShareLocationData(item: DestinationSearch) = Arguments.fromBundle(
        bundleOf(
            "address1" to item.address1,
            "address2" to item.address2,
            "latitude" to (item.routablePoint?.lat?.toString() ?: item.latitude ?: ""),
            "longitude" to (item.routablePoint?.long?.toString() ?: item.longitude
            ?: ""),
            "name" to item.address1,
            "fullAddress" to item.fullAddress,
            "placeId" to item.placeId
        ).apply {
            hashMapDestinationInfo?.get("userLocationLinkIdRef")
                ?.let { putString("userLocationLinkIdRef", it.toString()) }
        }
    )


    private fun renderMarkerToolTip(shouldRecenter: Boolean? = true) {
        if (!shouldRecenter!!) {
            viewBinding.root.bringToFront()
            updateTooltipPosition()
            return
        }

        val long: String =
            if (hashMapDestinationInfo?.get("long") != null) hashMapDestinationInfo?.get("long")
                .toString() else ""
        val lat: String =
            if (hashMapDestinationInfo?.get("lat") != null) hashMapDestinationInfo?.get("lat")
                .toString() else ""
        BreezeMapZoomUtil.zoomViewToRange(mapboxMap,
            Point.fromLngLat(long.safeDouble(), lat.safeDouble()),
            Constants.DEFAULT_TOOLTIP_ZOOM_RADIUS,
            EdgeInsets(10.0.dpToPx(), 10.0.dpToPx(), 200.0.dpToPx(), 10.0.dpToPx()),
            animationEndCB = {
                viewBinding.root.bringToFront()
            }
        )
    }

    override fun updateTooltipPosition() {
        mLatLng?.let { location ->
            val screenCoordinate = mapboxMap.pixelForCoordinate(
                Point.fromLngLat(location.longitude, location.latitude)
            )
            mViewMarker?.let {
                it.measure(
                    View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                    View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
                );
                it.x = screenCoordinate.x.toFloat() - (it.measuredWidth / 2)
                it.y = screenCoordinate.y.toFloat() - it.measuredHeight
            }
        }
    }

    override fun showMarker() {
        super.showMarker()
        viewBinding.root.visibility = View.VISIBLE
    }

    override fun hideMarker() {
        super.hideMarker()
        viewBinding.root.visibility = View.INVISIBLE
    }

    override fun updateBookMarkIconState(saved: Boolean, bookmarkId: Int) {
        destinationInfo?.isBookmarked = saved
        viewBinding.imgFlagSaved.setImageDrawable(
            ContextCompat.getDrawable(
                viewBinding.root.context,
                if (saved) R.drawable.ic_bookmarked else R.drawable.ic_nobookmark
            )
        )
        destinationInfo?.bookmarkId = if (saved) bookmarkId.toString() else null

    }
}
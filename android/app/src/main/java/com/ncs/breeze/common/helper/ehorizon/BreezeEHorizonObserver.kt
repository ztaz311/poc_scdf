package com.ncs.breeze.common.helper.ehorizon

import android.content.Context
import android.os.SystemClock
import android.util.Log
import com.breeze.model.CoordinateType
import com.breeze.model.NavigationZone
import com.breeze.model.constants.Constants
import com.breeze.model.enums.TrafficIncidentData
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.mapbox.geojson.Feature
import com.mapbox.geojson.FeatureCollection
import com.mapbox.geojson.LineString
import com.mapbox.geojson.Point
import com.mapbox.maps.Style
import com.mapbox.maps.extension.style.expressions.generated.Expression
import com.mapbox.maps.extension.style.layers.addLayer
import com.mapbox.maps.extension.style.layers.generated.lineLayer
import com.mapbox.maps.extension.style.layers.generated.symbolLayer
import com.mapbox.maps.extension.style.layers.properties.generated.IconAnchor
import com.mapbox.maps.extension.style.layers.properties.generated.LineCap
import com.mapbox.maps.extension.style.layers.properties.generated.LineJoin
import com.mapbox.maps.extension.style.sources.addSource
import com.mapbox.maps.extension.style.sources.generated.geoJsonSource
import com.mapbox.navigation.base.road.model.RoadComponent
import com.mapbox.navigation.base.trip.model.eh.EHorizonPosition
import com.mapbox.navigation.base.trip.model.roadobject.RoadObjectEnterExitInfo
import com.mapbox.navigation.base.trip.model.roadobject.RoadObjectPassInfo
import com.mapbox.navigation.base.trip.model.roadobject.UpcomingRoadObject
import com.mapbox.navigation.base.trip.model.roadobject.distanceinfo.PolygonDistanceInfo
import com.mapbox.navigation.base.trip.model.roadobject.distanceinfo.RoadObjectDistanceInfo
import com.mapbox.navigation.core.lifecycle.MapboxNavigationApp
import com.mapbox.navigation.core.trip.session.TripSessionState
import com.mapbox.navigation.core.trip.session.eh.EHorizonObserver
import com.mapbox.turf.TurfMeasurement
import com.ncs.breeze.BuildConfig
import com.ncs.breeze.common.extensions.android.getOBUConnHelper
import com.ncs.breeze.common.utils.BreezeERPRefreshUtil
import com.ncs.breeze.common.utils.RoadObjectUtil
import timber.log.Timber
import java.lang.ref.WeakReference
import java.util.Calendar

class BreezeEHorizonObserver(
    val breezeRoadObjectHandler: BreezeRoadObjectHandler,
    context: Context,
    val breezeERPUtil: BreezeERPRefreshUtil
) : EHorizonObserver {
    private val TAG = "BreezeEHorizonObserver"
    private val contextRef = WeakReference(context)

    internal var chargeAmount = "0.00"
    internal var mERPName = "ERP"
    internal var mERPID = ""
    internal var onRoadObjectPassedERP: Boolean = true
    internal var onRoadObjectPassedSilver: Boolean = true
    internal var onRoadObjectPassedSchool: Boolean = true
    var notificationZoneID: String = ""
    var displaySchoolLayer: Boolean = true
    var displaySilverLayer: Boolean = true

    var erpDetected: Boolean = false
    var lastNotificationShownDistance = 301
    val shownNotiAccidentObjectIds = hashSetOf<String>()
    private val notificationShowTimeMap = hashMapOf<String, Long>()

    init {
        initBreezeRoadStateHandler()
    }


    override fun onPositionUpdated(
        position: EHorizonPosition,
        distances: List<RoadObjectDistanceInfo>
    ) {
        val currentEdgeMetaData =
            MapboxNavigationApp.current()?.graphAccessor?.getEdgeMetadata(position.eHorizonGraphPosition.edgeId)
        if (currentEdgeMetaData != null && MapboxNavigationApp.current()
                ?.getTripSessionState() == TripSessionState.STARTED
        ) {
            breezeRoadObjectHandler.updateEdgeMetaData(
                currentEdgeMetaData.speedLimit,
                currentEdgeMetaData.names,
                currentEdgeMetaData.tunnel
            )
        }

        MapboxNavigationApp.current()?.roadObjectsStore?.getUpcomingRoadObjects(distances)
            ?.let { upcomingObjects ->
                val rObjectData = RoadObjectNotificationData()
                //FLASH_FLOOD/HEAVY_TRAFFIC/ROAD_CLOSURE/MAJOR_ACCIDENT does not use ehorizon
                processRoadObjects(upcomingObjects, rObjectData)

                if (rObjectData.zone != null) {
                    Timber.d("Ehorizon Notification $rObjectData")
                    if (rObjectData.zone == NavigationZone.ACCIDENT) {
                        showAccidentNotification(
                            rObjectData.distance!!,
                            rObjectData.roadObjectId!!,
                            currentEdgeMetaData?.names
                        )
                    } else {
                        showNotification(
                            rObjectData.zone!!,
                            rObjectData.distance!!,
                            rObjectData.roadObjectId!!,
                            currentEdgeMetaData?.names
                        )
                    }
                }
            }
    }


    override fun onRoadObjectAdded(roadObjectId: String) {
    }

    override fun onRoadObjectEnter(objectEnterExitInfo: RoadObjectEnterExitInfo) {

        Log.d("testing1", "OnRoadObjectEnter: " + objectEnterExitInfo.toString())
    }

    override fun onRoadObjectExit(objectEnterExitInfo: RoadObjectEnterExitInfo) {
        Timber.d("Road object exited: ${objectEnterExitInfo.roadObjectId}")
        if (objectEnterExitInfo.roadObjectId.contains("SCHOOL_ZONE", true)) {
            handlePassedSchoolZone(objectEnterExitInfo)

        } else if (objectEnterExitInfo.roadObjectId.contains("SILVER_ZONE", true)) {
            handlePassedSilverZone(objectEnterExitInfo)

        }

    }


    override fun onRoadObjectPassed(objectPassInfo: RoadObjectPassInfo) {
        Timber.d("Road object passed: ${objectPassInfo.roadObjectId}")
        if (objectPassInfo.roadObjectId.contains(Constants.ERP_ZONE_NAME_PREFIX, true)) {
            Timber.d("ERP Road object passed: ${objectPassInfo.roadObjectId}")
            if (onRoadObjectPassedERP) {
                Timber.d("Logging erp")
                //TODO trip
                //handleTripLogging(mERPID,mERPName,chargeAmount,System.currentTimeMillis() / 1000);
                removeNotification(objectPassInfo.roadObjectId)
                onRoadObjectPassedERP = false;
            }

            if (erpDetected) {
                Timber.d("Logging erp")
                breezeRoadObjectHandler.handleTripLogging(
                    mERPID,
                    mERPName,
                    chargeAmount,
                    System.currentTimeMillis() / 1000
                )
                erpDetected = false
            }
        }
    }

    override fun onRoadObjectRemoved(roadObjectId: String) {
        //  Log.d("testing", "OnRoadObjectRemoved: " + roadObjectId)
    }

    override fun onRoadObjectUpdated(roadObjectId: String) {
        //     Log.d("testing", "OnRoadObjectUpdated: " + roadObjectId)
    }


    private fun showAccidentNotification(
        distance: Int,
        id: String,
        names: List<RoadComponent>?
    ) {
        shownNotiAccidentObjectIds.add(id)
        breezeRoadObjectHandler.showNotificationView(
            NavigationZone.ACCIDENT,
            distance,
            mERPName,
            chargeAmount,
            breezeRoadObjectHandler.roadObjectID,
            if (!names.isNullOrEmpty())
                names[0].text
            else
                null
        )
        breezeRoadObjectHandler.playVoiceOnce(NavigationZone.ACCIDENT)
    }

    private fun showNotification(
        zone: NavigationZone,
        distance: Int,
        id: String,
        names: List<RoadComponent>?
    ) {

        val lastRID = notificationZoneID
        notificationZoneID = id

        Timber.d("EHorizonObserver Zone: $zone, distance: $distance, id: $id, names: $names")
        Timber.d("EHorizonObserver lastNotificationShownDistance: $lastNotificationShownDistance")

        when (zone) {
            NavigationZone.SCHOOL_ZONE -> {
                onRoadObjectPassedSchool = true
            }

            NavigationZone.SILVER_ZONE -> {
                onRoadObjectPassedSilver = true
            }

            NavigationZone.ERP_ZONE -> {
                onRoadObjectPassedERP = true
            }

            else -> {}
        }
        if (lastRID != id) {
            lastNotificationShownDistance = distance
            val currTime = SystemClock.elapsedRealtime()
            // check time in case go in and go out an area within short time
            if (currTime - (notificationShowTimeMap[id] ?: 0) > 10000) {
                notificationShowTimeMap[id] = SystemClock.elapsedRealtime()
                breezeRoadObjectHandler.showNotificationView(
                    zone,
                    distance,
                    mERPName,
                    chargeAmount,
                    breezeRoadObjectHandler.roadObjectID,
                    if (!names.isNullOrEmpty())
                        names[0].text
                    else
                        null
                )
                breezeRoadObjectHandler.playVoiceOnce(zone)
            }

        } else {
            if ((distance in 1..300 && lastNotificationShownDistance !in 1..300)) {
                lastNotificationShownDistance = distance
                val currTime = SystemClock.elapsedRealtime()
                // check time in case go in and go out an area within short time
                if (currTime - (notificationShowTimeMap[id] ?: 0) > 10000) {
                    notificationShowTimeMap[id] = SystemClock.elapsedRealtime()

                    breezeRoadObjectHandler.showNotificationView(
                        zone,
                        distance,
                        mERPName,
                        chargeAmount,
                        breezeRoadObjectHandler.roadObjectID,
                        if (!names.isNullOrEmpty())
                            names[0].text
                        else
                            null
                    )
                    breezeRoadObjectHandler.playVoiceOnce(zone)
                }
            } else {
                Timber.d("EHorizonObserver Skipping this notification to show and play voice because last notification was shown in less than 300 meter OR the current notification distance is more than 300 meter")
            }

        }
    }

    private fun updateNotificationData(
        zone: NavigationZone,
        distance: Int,
        id: String,
        rObjectData: RoadObjectNotificationData
    ) {
        if ((rObjectData.zone == null) || (rObjectData.zone!!.priority > zone.priority)) {
            rObjectData.zone = zone;
            rObjectData.distance = distance;
            rObjectData.roadObjectId = id;
        }
    }


    private fun handlePassedSilverZone(objectEnterExitInfo: RoadObjectEnterExitInfo) {
        if (onRoadObjectPassedSilver) {
            removeNotification(objectEnterExitInfo.roadObjectId, true)
            onRoadObjectPassedSilver = false
            lastNotificationShownDistance = 301
        }
    }

    private fun handlePassedSchoolZone(objectEnterExitInfo: RoadObjectEnterExitInfo) {
        if (onRoadObjectPassedSchool) {
            removeNotification(objectEnterExitInfo.roadObjectId, true)
            onRoadObjectPassedSchool = false;
            lastNotificationShownDistance = 301
        }
    }


    private fun processRoadObjects(
        upcomingObjects: List<UpcomingRoadObject>,
        rObjectData: RoadObjectNotificationData
    ) {
        upcomingObjects
            .filter { it.distanceToStart != null }
            .forEach {
                Timber.d(
                    "$TAG-processRoadObjects id = ${it.roadObject.id}, distanceToStart = ${it.distanceToStart}, type=  ${it.distanceInfo}, location= ${it.roadObject.location.shape}"
                )
                val shape = it.roadObject.location.shape.toJson()
                val distanceToStart = it.distanceToStart!!.toInt()
                val objectId = it.roadObject.id
                when {
                    objectId.contains(Constants.ERP_ZONE_NAME_PREFIX) -> {
                        if (distanceToStart < 300
                            && contextRef.get()?.getOBUConnHelper()?.hasActiveOBUConnection() != true
                        )
                            processERPRoadObject(objectId, distanceToStart, it, rObjectData)
                    }

                    objectId.contains(Constants.SCHOOL_ZONE_RO) -> {
                        if (distanceToStart < 300 &&
                            Calendar.getInstance().get(Calendar.HOUR_OF_DAY) < 18
                            && contextRef.get()?.getOBUConnHelper()?.hasActiveOBUConnection() != true
                        ) {// not process School zone after 6PM
                            processSchoolZoneRoadObject(
                                objectId,
                                distanceToStart,
                                it,
                                rObjectData,
                                shape
                            )
                        }
                    }

                    objectId.contains(Constants.SILVER_ZONE_RO) -> {
                        if (distanceToStart < 300
                            && contextRef.get()?.getOBUConnHelper()?.hasActiveOBUConnection() != true
                        )
                            processSilverZoneRoadObject(
                                objectId,
                                distanceToStart,
                                it,
                                rObjectData,
                                shape
                            )
                    }

                    objectId.contains(Constants.SPEED_CAMERA_RO) -> {
                        if (distanceToStart < 300
                            && contextRef.get()?.getOBUConnHelper()?.hasActiveOBUConnection() != true
                        )
                            checkAndUpdateNotification(
                                objectId,
                                distanceToStart, it,
                                rObjectData,
                                NavigationZone.SPEED_CAMERA
                            )
                    }

                    (objectId.contains(Constants.SEASON_PARKING_RO) && distanceToStart < SEASON_PARKING_DISTANCE_THRESHOLD) -> {
                        checkAndUpdateNotification(
                            it,
                            rObjectData,
                            NavigationZone.SEASON_PARKING
                        )
                    }

                    else -> {
                        TrafficIncidentData.getIncidentType(
                            RoadObjectUtil.parseIncidentTypeFromRoadObject(objectId)
                        )
                            ?.takeIf { incident ->
                                (incident.zone == NavigationZone.ACCIDENT
                                        && distanceToStart < 1000
                                        && !shownNotiAccidentObjectIds.contains(objectId)
                                        )
                                        || (incident.zone !== NavigationZone.ACCIDENT && distanceToStart < 300)
                            }
                            ?.let { incident ->
                                checkAndUpdateNotification(
                                    objectId,
                                    distanceToStart,
                                    it,
                                    rObjectData,
                                    incident.zone
                                )
                            }
                    }
                }
            }
    }


    private fun processSilverZoneRoadObject(
        objectString: String,
        distanceToStart: Int,
        it: UpcomingRoadObject,
        rObjectData: RoadObjectNotificationData,
        shape: String?
    ) {
        if (objectString.equals(notificationZoneID) && notificationZoneID != null && notificationZoneID.length > 1) {
            updateNotificationData(
                NavigationZone.SILVER_ZONE,
                distanceToStart, it.roadObject.id,
                rObjectData
            )
        } else {
            val featureListForSilver = arrayListOf<Feature>()
            val featureListForSilverDebug = arrayListOf<Feature>()
            val gsonObj = Gson().fromJson(shape, CoordinateType::class.java)
            if (gsonObj != null && gsonObj.coordinates != null) {
                val silverZonePoints = ArrayList<Point>()
                gsonObj.coordinates.forEach {
                    silverZonePoints.add(
                        Point.fromLngLat(
                            it.get(0),
                            it.get(1)
                        )
                    )
                }

                //DEBUG LINE
                if (BuildConfig.DEBUG) {
                    val feature = Feature.fromGeometry(
                        LineString.fromLngLats(silverZonePoints),
                        JsonObject()
                    )
                    featureListForSilverDebug.add(feature)
                }
                ///////////

                if (it.distanceInfo is PolygonDistanceInfo) {
                    val distanceInfo: PolygonDistanceInfo = it.distanceInfo as PolygonDistanceInfo
                    if (!distanceInfo.entrances.isNullOrEmpty()) {
                        val intersactionPoint = distanceInfo.entrances.get(0).position.coordinate
                        val feature = Feature.fromGeometry(
                            LineString.fromLngLats(arrayListOf(intersactionPoint)),
                            JsonObject()
                        )
                        featureListForSilver.add(TurfMeasurement.center(feature))
                    }
                }
                //If the entry point is not defined in the response then
                //use the center point of polygon to put symbol layer
                if (featureListForSilver.isEmpty()) {
                    val feature = Feature.fromGeometry(
                        LineString.fromLngLats(silverZonePoints),
                        JsonObject()
                    )
                    featureListForSilver.add(TurfMeasurement.center(feature))
                }
            }
            if (displaySilverLayer) {
                displaySilverIconLayer(featureListForSilver, featureListForSilverDebug)
                displaySilverLayer = false;
            }
            updateNotificationData(
                NavigationZone.SILVER_ZONE,
                distanceToStart, it.roadObject.id,
                rObjectData
            )
        }
    }

    private fun processSchoolZoneRoadObject(
        objectString: String,
        distanceToStart: Int,
        it: UpcomingRoadObject,
        rObjectData: RoadObjectNotificationData,
        shape: String?
    ) {
        if (objectString.equals(notificationZoneID) && notificationZoneID.length > 1) {
            updateNotificationData(
                NavigationZone.SCHOOL_ZONE,
                distanceToStart, it.roadObject.id,
                rObjectData
            )
        } else {
            val featureListForSchool = arrayListOf<Feature>()
            val featureListForSchoolDebug = arrayListOf<Feature>()
            val gsonObj = Gson().fromJson(shape, CoordinateType::class.java)
            if (gsonObj != null) {
                val schoolZonePoints = ArrayList<Point>()
                gsonObj.coordinates.forEach {
                    schoolZonePoints.add(
                        Point.fromLngLat(
                            it[0],
                            it[1]
                        )
                    )
                }

                //DEBUG LINE
                if (BuildConfig.DEBUG) {
                    val feature = Feature.fromGeometry(
                        LineString.fromLngLats(schoolZonePoints),
                        JsonObject()
                    )
                    featureListForSchoolDebug.add(feature)
                }
                ///////////

                if (it.distanceInfo is PolygonDistanceInfo) {
                    val distanceInfo: PolygonDistanceInfo = it.distanceInfo as PolygonDistanceInfo
                    if (!distanceInfo.entrances.isNullOrEmpty()) {
                        val intersactionPoint = distanceInfo.entrances.get(0).position.coordinate
                        val feature = Feature.fromGeometry(
                            LineString.fromLngLats(arrayListOf(intersactionPoint)),
                            JsonObject()
                        )
                        featureListForSchool.add(TurfMeasurement.center(feature))
                    }
                }
                //If the entry point is not defined in the response then
                //use the center point of polygon to put symbol layer
                if (featureListForSchool.isEmpty()) {
                    val feature = Feature.fromGeometry(
                        LineString.fromLngLats(schoolZonePoints),
                        JsonObject()
                    )
                    featureListForSchool.add(TurfMeasurement.center(feature))
                }
            }
            if (displaySchoolLayer) {
                displaySchoolIcon(featureListForSchool, featureListForSchoolDebug)
                displaySchoolLayer = false;
            }
            updateNotificationData(
                NavigationZone.SCHOOL_ZONE,
                distanceToStart,
                it.roadObject.id,
                rObjectData
            )
        }
    }

    private fun processERPRoadObject(
        objectString: String,
        distanceToStart: Int,
        it: UpcomingRoadObject,
        rObjectData: RoadObjectNotificationData
    ) {
        erpDetected = true
        if (breezeERPUtil.getCurrentERPData() != null) {
            var erpMap = breezeERPUtil.getCurrentERPData()!!.mapOfERP
            if (erpMap.containsKey(objectString)) {
                var erpDetails = erpMap[objectString]
                if (erpDetails != null) {

                    mERPName = erpDetails.erp.name
                    chargeAmount = erpDetails.charge.toString()
                    mERPID = erpDetails.erp.erpid.toString()

                    if (erpDetails.charge != 0F) {
                        //view
                        updateNotificationData(
                            NavigationZone.ERP_ZONE,
                            distanceToStart, it.roadObject.id,
                            rObjectData
                        )
                    }
                }
            }
        }
    }

    private fun checkAndUpdateNotification(
        objectString: String,
        distanceToStart: Int,
        it: UpcomingRoadObject,
        rObjectData: RoadObjectNotificationData,
        navigationZoneType: NavigationZone
    ) {
        if (objectString == notificationZoneID && notificationZoneID.length > 1) {
            if (distanceToStart <= DEFAULT_DISTANCE_THRESHOLD) {
                removeNotification(it.roadObject.id)
            } else {
                updateNotificationData(
                    navigationZoneType,
                    distanceToStart, it.roadObject.id,
                    rObjectData
                )
            }
        } else {
            if (distanceToStart <= DEFAULT_DISTANCE_THRESHOLD) {
                removeNotification(it.roadObject.id)
            } else {
                updateNotificationData(
                    navigationZoneType,
                    distanceToStart, it.roadObject.id,
                    rObjectData
                )
            }
        }
    }

    private fun checkAndUpdateNotification(
        it: UpcomingRoadObject,
        rObjectData: RoadObjectNotificationData,
        navigationZoneType: NavigationZone
    ) {
        updateNotificationData(
            navigationZoneType,
            DEFAULT_DISTANCE_THRESHOLD, it.roadObject.id,
            rObjectData
        )
    }


    private fun removeNotification(roadObjectId: String, removeNotificationId: Boolean = false) {
        if (roadObjectId.isNotEmpty()) {
            breezeRoadObjectHandler.updateStyleList {
                if (roadObjectId.contains(Constants.SCHOOL_ZONE_RO, true)) {
                    it.removeStyleLayer(BreezeRoadObjectHandler.SCHOOL_LAYER)
                    it.removeStyleSource(BreezeRoadObjectHandler.SCHOOL_GEOJSONSOURCE)
                    if (BuildConfig.DEBUG) {
                        it.removeStyleLayer(BreezeRoadObjectHandler.SCHOOL_LAYER_DEBUG)
                        it.removeStyleSource(BreezeRoadObjectHandler.SCHOOL_GEOJSONSOURCE_DEBUG)
                    }
                } else if (roadObjectId.contains(Constants.SILVER_ZONE_RO, true)) {
                    it.removeStyleLayer(BreezeRoadObjectHandler.SILVER_LAYER)
                    it.removeStyleSource(BreezeRoadObjectHandler.SILVER_GEOJSONSOURCE)
                    if (BuildConfig.DEBUG) {
                        it.removeStyleLayer(BreezeRoadObjectHandler.SILVER_LAYER_DEBUG)
                        it.removeStyleSource(BreezeRoadObjectHandler.SILVER_GEOJSONSOURCE_DEBUG)
                    }
                }
            }
            if (roadObjectId.contains(Constants.SCHOOL_ZONE_RO, true)) {
                displaySchoolLayer = true
            } else if (roadObjectId.contains(Constants.SILVER_ZONE_RO, true)) {
                displaySilverLayer = true
            }
            if ((roadObjectId == notificationZoneID) && (removeNotificationId)) {
                notificationZoneID = ""
            }
        }
    }

    internal fun displaySchoolIcon(
        featureListForSchool: ArrayList<Feature>,
        featureListForSchoolDebug: ArrayList<Feature>
    ) {
        breezeRoadObjectHandler.updateStyleList {
            displaySchoolZone(it, featureListForSchool, featureListForSchoolDebug)
        }
    }

    private fun displaySchoolZone(
        style: Style,
        featureListForSchool: ArrayList<Feature>,
        featureListForSchoolDebug: ArrayList<Feature>
    ) {
        if (style != null) {
            Timber.d("Display of school zone")
            style!!.removeStyleLayer(BreezeRoadObjectHandler.SCHOOL_LAYER)
            style!!.removeStyleSource(BreezeRoadObjectHandler.SCHOOL_GEOJSONSOURCE)
            if (BuildConfig.DEBUG) {
                style.removeStyleLayer(BreezeRoadObjectHandler.SCHOOL_LAYER_DEBUG)
                style.removeStyleSource(BreezeRoadObjectHandler.SCHOOL_GEOJSONSOURCE_DEBUG)
            }

            val icon_size_exp = Expression.interpolate {
                linear()
                zoom()
                stop {
                    literal(0.0)
                    literal(0.0)
                }
                stop {
                    literal(9.0)
                    literal(0.0)
                }
                stop {
                    literal(12.9)
                    literal(0.0)
                }
                stop {
                    literal(13.0)
                    literal(0.8)
                }
            }


            val mFeatureCollection = FeatureCollection.fromFeatures(featureListForSchool)
            style.addSource(
                geoJsonSource(BreezeRoadObjectHandler.SCHOOL_GEOJSONSOURCE) {
                    featureCollection(mFeatureCollection!!)
                    //maxzoom(14)
                }
            )
            style.addLayer(
                symbolLayer(
                    BreezeRoadObjectHandler.SCHOOL_LAYER,
                    BreezeRoadObjectHandler.SCHOOL_GEOJSONSOURCE
                ) {
                    iconImage(
                        BreezeRoadObjectHandler.SCHOOL_ICON
                    )
                    iconSize(icon_size_exp)
                    iconAnchor(IconAnchor.CENTER)
                    iconAllowOverlap(true)


                }
            )

            if (BuildConfig.DEBUG) {
                val mFeatureCollectionDebug =
                    FeatureCollection.fromFeatures(featureListForSchoolDebug)
                style.addSource(
                    geoJsonSource(BreezeRoadObjectHandler.SCHOOL_GEOJSONSOURCE_DEBUG) {
                        featureCollection(mFeatureCollectionDebug)
                        //maxzoom(14)
                    }
                )
                style.addLayer(
                    lineLayer(
                        BreezeRoadObjectHandler.SCHOOL_LAYER_DEBUG,
                        BreezeRoadObjectHandler.SCHOOL_GEOJSONSOURCE_DEBUG
                    ) {
                        lineCap(LineCap.ROUND)
                        lineJoin(LineJoin.ROUND)
                        lineOpacity(0.7)
                        lineWidth(8.0)
                        lineColor("#FF5800")
                    }
                )
            }


        } else {
            Timber.d("Trying to display school zone when style is null")
        }
    }

    internal fun displaySilverIconLayer(
        featureListForSilver: ArrayList<Feature>,
        featureListForSilverDebug: ArrayList<Feature>
    ) {
        breezeRoadObjectHandler.updateStyleList {
            displaySilverIconLayer(it, featureListForSilver, featureListForSilverDebug)
        }
    }

    // SILVER_LAYER
    private fun displaySilverIconLayer(
        style: Style,
        featureListForSilver: ArrayList<Feature>,
        featureListForSilverDebug: ArrayList<Feature>
    ) {

        if (style != null) {
            Timber.d("Display of silver zone")
            style!!.removeStyleLayer(BreezeRoadObjectHandler.SILVER_LAYER)
            style!!.removeStyleSource(BreezeRoadObjectHandler.SILVER_GEOJSONSOURCE)
            if (BuildConfig.DEBUG) {
                style!!.removeStyleLayer(BreezeRoadObjectHandler.SILVER_LAYER_DEBUG)
                style.removeStyleSource(BreezeRoadObjectHandler.SILVER_GEOJSONSOURCE_DEBUG)
            }
            var icon_size_exp = Expression.interpolate {
                linear()
                zoom()
                stop {
                    literal(0.0)
                    literal(0.0)
                }
                stop {
                    literal(9.0)
                    literal(0.0)
                }
                stop {
                    literal(12.9)
                    literal(0.0)
                }
                stop {
                    literal(13.0)
                    literal(0.8)
                }
            }


            val mFeatureCollection = FeatureCollection.fromFeatures(featureListForSilver)
            style.addSource(
                geoJsonSource(BreezeRoadObjectHandler.SILVER_GEOJSONSOURCE) {
                    featureCollection(mFeatureCollection!!)
                    //maxzoom(14)
                }
            )
            style.addLayer(
                symbolLayer(
                    BreezeRoadObjectHandler.SILVER_LAYER,
                    BreezeRoadObjectHandler.SILVER_GEOJSONSOURCE
                ) {
                    iconImage(
                        BreezeRoadObjectHandler.SILVER_ICON
                    )
                    iconSize(icon_size_exp)
                    iconAnchor(IconAnchor.CENTER)
                    iconAllowOverlap(false)


                }
            )

            if (BuildConfig.DEBUG) {
                val mFeatureCollectionDebug =
                    FeatureCollection.fromFeatures(featureListForSilverDebug)
                style.addSource(
                    geoJsonSource(BreezeRoadObjectHandler.SILVER_GEOJSONSOURCE_DEBUG) {
                        featureCollection(mFeatureCollectionDebug)
                        //maxzoom(14)
                    }
                )
                style.addLayer(
                    lineLayer(
                        BreezeRoadObjectHandler.SILVER_LAYER_DEBUG,
                        BreezeRoadObjectHandler.SILVER_GEOJSONSOURCE_DEBUG
                    ) {
                        lineCap(LineCap.ROUND)
                        lineJoin(LineJoin.ROUND)
                        lineOpacity(0.7)
                        lineWidth(8.0)
                        lineColor("#FF5800")
                    }
                )
            }

        } else {
            Timber.d("Trying to display silver zone when style is null")
        }

    }

    private fun initBreezeRoadStateHandler() {
        breezeRoadObjectHandler.addBreezeRoadObjectStateHandler(object :
            BreezeRoadObjectStateHandler {
            override fun onTerminateNotification(
                style: Style?,
                roadObjectId: String
            ) {
                if (roadObjectId.isNotEmpty()) {
                    if (roadObjectId.contains("SCHOOL_ZONE", true)) {
                        displaySchoolLayer = true
                        style?.let { style ->
                            style.removeStyleLayer(BreezeRoadObjectHandler.SCHOOL_LAYER)
                            style.removeStyleSource(BreezeRoadObjectHandler.SCHOOL_GEOJSONSOURCE)
                            if (BuildConfig.DEBUG) {
                                style.removeStyleLayer(BreezeRoadObjectHandler.SCHOOL_LAYER_DEBUG)
                                style.removeStyleSource(BreezeRoadObjectHandler.SCHOOL_GEOJSONSOURCE_DEBUG)
                            }
                        }
                    } else if (roadObjectId.contains("SILVER_ZONE", true)) {
                        displaySilverLayer = true
                        style?.let { style ->
                            style.removeStyleLayer(BreezeRoadObjectHandler.SILVER_LAYER)
                            style.removeStyleSource(BreezeRoadObjectHandler.SILVER_GEOJSONSOURCE)
                            if (BuildConfig.DEBUG) {
                                style.removeStyleLayer(BreezeRoadObjectHandler.SILVER_LAYER_DEBUG)
                                style.removeStyleSource(BreezeRoadObjectHandler.SILVER_GEOJSONSOURCE_DEBUG)
                            }
                        }
                    }
                }
            }
        })
    }


    companion object {
        private const val DEFAULT_DISTANCE_THRESHOLD = 15
        private const val SEASON_PARKING_DISTANCE_THRESHOLD = 16
    }


}
package com.ncs.breeze.ui.login.fragment.forgotPassword.module

import com.ncs.breeze.ui.login.fragment.forgotPassword.view.ForgetPasswordEmailFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class ForgetPasswordEmailModule {
    @ContributesAndroidInjector
    abstract fun contributeForgetPasswordEmailFragment(): ForgetPasswordEmailFragment
}
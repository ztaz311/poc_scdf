package com.ncs.breeze.components.marker.markerview2.carpark

import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import androidx.core.os.bundleOf
import com.breeze.model.api.response.AmenitiesPreference
import com.breeze.model.api.response.amenities.BaseAmenity
import com.breeze.model.constants.AmenityType
import com.facebook.react.bridge.Arguments
import com.google.android.gms.maps.model.LatLng
import com.mapbox.maps.MapboxMap
import com.ncs.breeze.common.extensions.android.getShareBusinessLogicPackage
import com.ncs.breeze.common.rxbus.RxBus
import com.ncs.breeze.common.rxbus.RxEvent
import com.ncs.breeze.common.utils.BreezeMapDataHolder
import com.ncs.breeze.databinding.CarparkMarkerTooltipBinding
import com.ncs.breeze.reactnative.nativemodule.ShareBusinessLogicEvent
import com.ncs.breeze.ui.base.BaseActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


open class RouteCarParkMarkerView constructor(
    latLng: LatLng,
    mapboxMap: MapboxMap,
    activity: Activity,
    itemId: String? = null
) : BaseCarParkMarkerView(mapboxMap, activity, itemId) {

    protected var viewBinding: CarparkMarkerTooltipBinding
    private var objectConfigUiAmenities: AmenitiesPreference? = null
    protected var carParkToolTipManager: CarParkToolTipManager? = null
    var currentDataMarker: BaseAmenity? = null
    var currentState = 0


    init {
        zoomLevel = 16.0
        mLatLng = latLng

        viewBinding = CarparkMarkerTooltipBinding.inflate(
            activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater,
            null,
            false
        )
        carParkToolTipManager =
            CarParkToolTipManager(viewBinding, mapboxMap, activity, voucherEnabled = true)
        mViewMarker = viewBinding.root
    }

    fun clickToopTip(
        item: BaseAmenity,
        shouldRecenter: Boolean,
        navigationButtonClick: (BaseAmenity, BaseActivity.EventSource) -> Unit,
        moreInfoButtonClick: (BaseAmenity) -> Unit,
        carParkIconClick: (Boolean) -> Unit,
        calculateFee: (BaseAmenity) -> Unit,
        screenName: String
    ) {
        currentDataMarker = item
        objectConfigUiAmenities = BreezeMapDataHolder.amenitiesPreferenceHashmap[item.amenityType]
        viewBinding.buttonShare.setOnClickListener {
            shareAmenity(item)
        }
        viewBinding.buttonInvite.setOnClickListener {
            inviteAmenity(item)
        }
        carParkToolTipManager?.clickRouteToolTip(
            item,
            shouldRecenter,
            navigationButtonClick,
            moreInfoButtonClick,
            carParkIconClick,
            calculateFee,
            screenName
        )
        updateTooltipPosition()
    }

    private fun createShareDataBundle(item: BaseAmenity) =
        bundleOf(
            "address1" to if (item.amenityType == AmenityType.CARPARK) item.name else item.address,
            "latitude" to (item.lat?.toString() ?: ""),
            "longitude" to (item.long?.toString() ?: ""),
            "amenityId" to item.id,
            "amenityType" to item.amenityType,
            "name" to item.name,
        )


    private fun shareAmenity(item: BaseAmenity) {
        viewBinding.root.context.getShareBusinessLogicPackage()?.getRequestsModule()?.flatMap {
            it.callRN(
                ShareBusinessLogicEvent.SHARE_LOCATION_FROM_NATIVE.eventName,
                Arguments.fromBundle(createShareDataBundle(item))
            )
        }?.subscribeOn(Schedulers.io())?.observeOn(AndroidSchedulers.mainThread())?.subscribe()
    }

    private fun inviteAmenity(item: BaseAmenity) {
        RxBus.publish(RxEvent.RoutePlanningOpenInvite(createShareDataBundle(item)))
    }

    override fun resetView() {
        carParkToolTipManager?.resetView()
    }

    override fun showMarker() {
        super.showMarker()
        viewBinding.root.visibility = View.VISIBLE

    }

    override fun hideMarker() {
        super.hideMarker()
        viewBinding.root.visibility = View.INVISIBLE

    }


    override fun hideAllToolTips() {
        super.hideAllToolTips()
        if (viewBinding.llToolTip.isShown) {
            viewBinding.llToolTip.visibility = View.INVISIBLE
        }
    }
}
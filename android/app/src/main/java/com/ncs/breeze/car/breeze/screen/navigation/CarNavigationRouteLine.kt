package com.ncs.breeze.car.breeze.screen.navigation

import com.mapbox.androidauto.internal.extensions.getStyle
import com.mapbox.androidauto.internal.logAndroidAuto
import com.mapbox.maps.Style
import com.mapbox.maps.extension.androidauto.MapboxCarMapObserver
import com.mapbox.maps.extension.androidauto.MapboxCarMapSurface
import com.mapbox.maps.extension.style.layers.getLayer
import com.mapbox.maps.plugin.locationcomponent.LocationComponentConstants
import com.mapbox.maps.plugin.locationcomponent.OnIndicatorPositionChangedListener
import com.mapbox.maps.plugin.locationcomponent.location
import com.mapbox.navigation.base.route.NavigationRoute
import com.mapbox.navigation.core.directions.session.RoutesObserver
import com.mapbox.navigation.core.directions.session.RoutesUpdatedResult
import com.mapbox.navigation.core.lifecycle.MapboxNavigationApp
import com.mapbox.navigation.core.trip.session.RouteProgressObserver
import com.mapbox.navigation.ui.maps.route.RouteLayerConstants
import com.mapbox.navigation.ui.maps.route.arrow.api.MapboxRouteArrowApi
import com.mapbox.navigation.ui.maps.route.arrow.api.MapboxRouteArrowView
import com.mapbox.navigation.ui.maps.route.arrow.model.RouteArrowOptions
import com.mapbox.navigation.ui.maps.route.line.api.MapboxRouteLineApi
import com.mapbox.navigation.ui.maps.route.line.api.MapboxRouteLineView
import com.mapbox.navigation.ui.maps.route.line.model.MapboxRouteLineOptions
import com.ncs.breeze.R
import com.ncs.breeze.car.breeze.utils.BreezeCarUtil
import com.ncs.breeze.car.breeze.utils.BreezeMapBoxUtil
import com.ncs.breeze.common.extensions.car.toBitmap
import com.breeze.model.constants.Constants
import com.ncs.breeze.common.utils.mapStyle.BreezeNavigationMapStyle
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber


class CarNavigationRouteLine(
    private val carNavigationCarContext: CarNavigationCarContext,
) : MapboxCarMapObserver {

    private val resources = carNavigationCarContext.carContext.resources

    private val navigationMapStyle = BreezeNavigationMapStyle()

//    private val options: MapboxRouteLineOptions by lazy {
//        var routeLineBelowLayerID = LocationComponentConstants.LOCATION_INDICATOR_LAYER
//        carNavigationCarContext.mapboxCarMap.carMapSurface?.getStyle()?.let {
//            if (it.getLayer(Constants.SPEED_CAMERA) != null) {
//                routeLineBelowLayerID = Constants.SPEED_CAMERA
//            }
//        }
//        MapboxRouteLineOptions.Builder(carNavigationCarContext.carContext)
//            .withRouteLineBelowLayerId(routeLineBelowLayerID)
//            .withRouteLineResources(
//                navigationMapStyle.getRouteLineResources(
//                    resources,
//                    R.drawable.destination_puck
//                )
//            )
//            .withVanishingRouteLineEnabled(true)
//            .build()
//    }

    private var routeLineView: MapboxRouteLineView? = null
    private var routeLineApi: MapboxRouteLineApi? = null

    private val routeArrowApi by lazy { MapboxRouteArrowApi() }
    private val routeArrowView: MapboxRouteArrowView by lazy {
        MapboxRouteArrowView(
            RouteArrowOptions.Builder(carNavigationCarContext.carContext)
                .withAboveLayerId(RouteLayerConstants.TOP_LEVEL_ROUTE_LINE_LAYER_ID)
                .build()
        )
    }

    private val onPositionChangedListener = OnIndicatorPositionChangedListener { point ->
        carNavigationCarContext.mapboxCarMap.carMapSurface?.mapSurface?.getMapboxMap()
            ?.getStyle { style ->
                routeLineApi?.updateTraveledRouteLine(point)?.let { result ->
                    routeLineView?.renderRouteLineUpdate(style, result)
                }
            }
    }

    private val routesObserver = RoutesObserver { result ->
        logAndroidAuto("CarRouteLine onRoutesChanged ${result.navigationRoutes.size}")
        carNavigationCarContext.mapboxCarMap.carMapSurface?.let { carMapSurface ->
            if (result.navigationRoutes.isNotEmpty()) {
                routeLineApi?.setNavigationRoutes(listOf(result.navigationRoutes[0])) { value ->
                    carMapSurface.getStyle()?.let { style ->
                        routeLineView?.renderRouteDrawData(style, value)
                    }
                }
                if (BreezeCarUtil.breezeERPUtil.getERPData() != null) {
                    reloadErpWhenReroute(result, carMapSurface)
                }
            } else {
                routeLineApi?.clearRouteLine { value ->
                    carMapSurface.getStyle()?.let { style ->
                        routeLineView?.renderClearRouteLineValue(style, value)
                    }
                }
                val clearArrowValue = routeArrowApi.clearArrows()
                carMapSurface.getStyle()?.let { style ->
                    routeArrowView.render(style, clearArrowValue)
                }
            }
        }

    }

    private fun reloadErpWhenReroute(
        result: RoutesUpdatedResult,
        carMapSurface: MapboxCarMapSurface
    ) {
        CoroutineScope(Dispatchers.Default).launch {
            val currentDirectionsRoute = result.routes[0]
            val currentRouteERPInfo = BreezeCarUtil.breezeERPUtil.getERPInRoute(
                BreezeCarUtil.breezeERPUtil.getERPData()!!,
                currentDirectionsRoute,
                false
            )
            withContext(Dispatchers.Main) {
                carMapSurface.getStyle()?.let { style ->
                    BreezeMapBoxUtil.addNavigationERPLayerToStyle(
                        style,
                        currentRouteERPInfo.featureCollection,
                        carNavigationCarContext.carContext
                    )
                }
            }
        }
    }

    val routeProgressObserver = RouteProgressObserver { routeProgress ->
        Timber.d("RouteProgressObserver: carMapSurface = ${carNavigationCarContext.mapboxCarMap.carMapSurface}, routeLineApi = ${routeLineApi}, ")
        carNavigationCarContext.mapboxCarMap.carMapSurface?.let { carMapSurface ->
            routeLineApi?.updateWithRouteProgress(routeProgress) { result ->
                carMapSurface.getStyle()?.let { style ->
                    routeLineView?.renderRouteLineUpdate(style, result)
                }
            }
            routeArrowApi.addUpcomingManeuverArrow(routeProgress).also { arrowUpdate ->
                carMapSurface.getStyle()?.let { style ->
                    routeArrowView.renderManeuverUpdate(style, arrowUpdate)
                }
            }
        }
    }

    override fun onAttached(mapboxCarMapSurface: MapboxCarMapSurface) {
        logAndroidAuto("CarNavigationRouteLine carMapSurface loaded $mapboxCarMapSurface")
        mapboxCarMapSurface.mapSurface.getMapboxMap().getStyle { style ->
            initRouteLine(style)

            updateOriginAndDestinationIcon(mapboxCarMapSurface)
            mapboxCarMapSurface.mapSurface.location.addOnIndicatorPositionChangedListener(
                onPositionChangedListener
            )
            MapboxNavigationApp.current()?.run {
                registerRouteProgressObserver(routeProgressObserver)
                registerRoutesObserver(routesObserver)
                historyRecorder.startRecording()
            }
            carNavigationCarContext.route.routeOptions()?.let { routeOptions ->
                val navigationRoute = NavigationRoute.create(
                    carNavigationCarContext.originalDirectionsResponse,
                    routeOptions
                ).filter {
                    it.routeIndex == carNavigationCarContext.route.routeIndex()?.toInt()
                }
                MapboxNavigationApp.current()?.setNavigationRoutes(navigationRoute)
            }
        }
    }

    private fun initRouteLine(style: Style) {
        var routeLineBelowLayerID = LocationComponentConstants.LOCATION_INDICATOR_LAYER
        if (style.getLayer(Constants.SPEED_CAMERA) != null) {
            routeLineBelowLayerID = Constants.SPEED_CAMERA
        }

        MapboxRouteLineOptions.Builder(carNavigationCarContext.carContext)
            .withRouteLineBelowLayerId(routeLineBelowLayerID)
            .withRouteLineResources(
                navigationMapStyle.getRouteLineResources(
                    resources,
                    R.drawable.destination_puck
                )
            )
            .withVanishingRouteLineEnabled(true)
            .build()
            .also {
                routeLineApi = MapboxRouteLineApi(it)
                routeLineView = MapboxRouteLineView(it).apply { initializeLayers(style) }
            }
    }

    /**
     * required workaround due to mapbox bug https://github.com/mapbox-collab/ncs-collab/issues/365
     * TODO: update logic when mapbox includes feature to update destination icon on style from routeline
     */
    private fun updateOriginAndDestinationIcon(mapboxCarMapSurface: MapboxCarMapSurface) {
        mapboxCarMapSurface.mapSurface.getMapboxMap().getStyle { style ->
            carNavigationCarContext.carContext.toBitmap(R.drawable.mapbox_ic_route_origin)
                ?.let { bitmap ->
                    style.removeStyleImage("originMarker")
                    style.addImage("originMarker", bitmap)
                }
            carNavigationCarContext.carContext.toBitmap(R.drawable.destination_puck)
                ?.let { bitmap ->
                    style.removeStyleImage("destinationMarker")
                    style.addImage("destinationMarker", bitmap, false)
                }
        }
    }

    override fun onDetached(mapboxCarMapSurface: MapboxCarMapSurface) {
        logAndroidAuto("CarRouteLine carMapSurface detached $mapboxCarMapSurface")
        mapboxCarMapSurface.mapSurface.location
            .removeOnIndicatorPositionChangedListener(onPositionChangedListener)
        MapboxNavigationApp.current()?.run {
            unregisterRouteProgressObserver(routeProgressObserver)
            unregisterRoutesObserver(routesObserver)

        }

        mapboxCarMapSurface.getStyle()?.let { style ->
            routeLineApi?.clearRouteLine { value ->
                routeLineView?.renderClearRouteLineValue(style, value)
            }
        }

        MapboxNavigationApp.current()?.historyRecorder?.stopRecording {
            logAndroidAuto("CarRouteLine saved history $it")
        }
    }
}

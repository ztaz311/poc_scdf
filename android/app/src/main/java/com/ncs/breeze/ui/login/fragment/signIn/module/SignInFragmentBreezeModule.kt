package com.ncs.breeze.ui.login.fragment.signIn.module

import com.ncs.breeze.ui.login.fragment.signIn.view.SignInFragmentBreeze
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class SignInFragmentBreezeModule {
    @ContributesAndroidInjector
    abstract fun contributeSignInFragmentBreeze(): SignInFragmentBreeze
}
package com.ncs.breeze.common.utils

import android.util.Log
import timber.log.Timber
import kotlin.properties.Delegates

object Variables {
    var isNetworkConnected: Boolean by Delegates.observable(true) { property, oldValue, newValue ->
        Timber.d("Internet - Variable: isNetworkConnected ${newValue}")
        Log.i("Network connectivity", "$newValue")
    }
}
package com.ncs.breeze.di.module.fragment

import com.ncs.breeze.ui.dashboard.fragments.view.RoutePlanningFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class RoutePanningModuleCard {
    @ContributesAndroidInjector
    abstract fun contributeRoutePlanningFragment(): RoutePlanningFragment
}
package com.ncs.breeze.car.breeze

import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import androidx.car.app.Screen
import androidx.car.app.Session
import androidx.car.app.connection.CarConnection
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import com.mapbox.android.core.permissions.PermissionsManager
import com.mapbox.androidauto.MapboxCarContext
import com.mapbox.androidauto.internal.logAndroidAuto
import com.mapbox.androidauto.map.MapboxCarMapLoader
import com.mapbox.androidauto.navigation.speedlimit.SpeedLimitOptions
import com.mapbox.androidauto.notification.MapboxCarNotificationOptions
import com.mapbox.maps.ContextMode
import com.mapbox.maps.MapInitOptions
import com.mapbox.maps.MapOptions
import com.mapbox.maps.extension.androidauto.MapboxCarMap
import com.mapbox.maps.extension.style.style
import com.mapbox.navigation.base.options.NavigationOptions
import com.mapbox.navigation.base.speed.model.SpeedLimitSign
import com.mapbox.navigation.core.lifecycle.MapboxNavigationApp
import com.ncs.breeze.BuildConfig
import com.ncs.breeze.R
import com.ncs.breeze.car.BreezeCarAppService
import com.ncs.breeze.car.breeze.screen.needlogin.NeedsLoginScreen
import com.ncs.breeze.car.breeze.screen.permissions.NeedsLocationPermissionsScreen
import com.ncs.breeze.car.breeze.utils.BreezeCarUtil
import com.ncs.breeze.car.breeze.utils.CarPermissionUtils
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.Event
import com.ncs.breeze.common.model.rx.ToCarRxEvent
import com.ncs.breeze.common.model.rx.DataSearch
import com.ncs.breeze.common.model.rx.NavigationCongestionAlertEventShow
import com.ncs.breeze.common.model.rx.NavigationParkingAvailabilityAlertEventShow
import com.ncs.breeze.common.model.rx.OpenSearchScreenEvent
import com.ncs.breeze.common.event.ToCarRx
import com.breeze.model.obu.OBUParkingAvailability
import com.breeze.model.obu.OBUTravelTimeData
import com.ncs.breeze.common.storage.BreezeUserPreference
import com.breeze.model.constants.Constants
import com.ncs.breeze.App
import com.ncs.breeze.common.model.rx.OBUDisplayLifecycleEvent
import com.ncs.breeze.common.utils.LocationBreezeManager
import com.ncs.breeze.common.utils.OBUStripState
import com.ncs.breeze.common.utils.OBUStripStateManager
import com.ncs.breeze.common.utils.createNewLocationEngineRequest
import com.ncs.breeze.common.utils.eta.ETAEngine
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import sg.gov.lta.obu.sdk.data.services.OBUSDK
import timber.log.Timber

class MainCarBreezeSession : Session() {
    private var breezeCarContext: MainBreezeCarContext? = null
    private var carScreenManager: BreezeScreenManager? = null

    private val mapboxCarMapLoader = MapboxCarMapLoader().apply {
        setDarkStyleOverride(style(BuildConfig.MAPBOX_STYLE_URL_DARK) {})
        setLightStyleOverride(style(BuildConfig.MAPBOX_STYLE_URL_LIGHT) {})
    }
    val mapboxCarMap = MapboxCarMap().registerObserver(mapboxCarMapLoader)
    private val mapboxCarContext = MapboxCarContext(lifecycle, mapboxCarMap)

    init {
        MapboxNavigationApp.attach(lifecycleOwner = this)
        mapboxCarContext.customize {
            // You need to tell the car notification which app to open when a
            // user taps it.
            notificationOptions = MapboxCarNotificationOptions.Builder()
                .startAppService(BreezeCarAppService::class.java)
                .build()
            speedLimitOptions = SpeedLimitOptions.Builder()
                .forcedSignFormat(SpeedLimitSign.VIENNA)
                .build()
        }

        // Decide how you want the car and app to interact. In this example, the car and app
        // are kept in sync where they essentially mirror each other.
//        CarAppSyncComponent.getInstance().setCarSession(this)

        lifecycle.addObserver(object : DefaultLifecycleObserver {
            lateinit var compositeDisposable: CompositeDisposable
            override fun onCreate(owner: LifecycleOwner) {
                checkLocationPermissions()
                if (!MapboxNavigationApp.isSetup()) {
                    MapboxNavigationApp.setup(
                        NavigationOptions.Builder(carContext)
                            .accessToken(carContext.getString(R.string.mapbox_access_token))
                            .locationEngineRequest(
                                createNewLocationEngineRequest()
                            )
                            .build()
                    )
                }

                // Once a CarContext is available, pass it to the MapboxCarMap.
                mapboxCarMap.setup(
                    carContext,
                    MapInitOptions(context = carContext).apply {
                        mapOptions = MapOptions.Builder()
                            .contextMode(ContextMode.SHARED)
                            .build()
                    })
                compositeDisposable = CompositeDisposable()
                CarConnection(carContext).type.observe(
                    this@MainCarBreezeSession,
                    ::onConnectionStateUpdated
                )

                OBUStripStateManager.getInstance()?.let {obuStripStateManager ->
                    obuStripStateManager.obuStripCurrentState.observe(owner){
                        if(it == OBUStripState.RUNNING && !obuStripStateManager.isCarOpened){
                            ToCarRx.postEventWithCacheLast(OBUDisplayLifecycleEvent(Lifecycle.State.STARTED))
                        }
                    }

                }

                (carContext.applicationContext as? App)?.run {
                    initOBU()
                    isCarSessionAlive = true
                }

            }

            override fun onResume(owner: LifecycleOwner) {
                super.onResume(owner)
                compositeDisposable.add(
                    ToCarRx.listenEvent(ToCarRxEvent::class.java)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe {
                            logAndroidAuto("event in session ")
                            carScreenManager?.handleEvent(it)
                        })

                compositeDisposable.add(
                    ToCarRx.listenEventWithLastCache(ToCarRxEvent::class.java)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe {
                            logAndroidAuto("event in session ")
                            carScreenManager?.handleEvent(it)
                        })
            }

            override fun onPause(owner: LifecycleOwner) {
                super.onPause(owner)
                logAndroidAuto("MainCarSession onPause")
                compositeDisposable.clear()
                LocationBreezeManager.getInstance().currentLocation?.let {
                    BreezeUserPreference.getInstance(carContext.applicationContext)
                        .saveLastLocation(it)
                }
            }

            override fun onDestroy(owner: LifecycleOwner) {
                super.onDestroy(owner)
                logAndroidAuto("MainCarSession onDestroy")
                compositeDisposable.dispose()
                mapboxCarMap.clearObservers()
                (carContext.applicationContext as? App)?.isCarSessionAlive = false
            }
        })


    }

    override fun onCreateScreen(intent: Intent): Screen {
        logAndroidAuto("MainCarBreezeSession onCreateScreen")

        ETAEngine.init(BreezeCarUtil.apiHelper, carContext.applicationContext)
        breezeCarContext = MainBreezeCarContext(carContext, mapboxCarMap, mapboxCarContext)
        carScreenManager = BreezeScreenManager(breezeCarContext!!)
        return if (CarPermissionUtils.hasEnoughPermissions(carContext)) {
            NeedsLoginScreen(breezeCarContext!!, intent)
        } else {
            NeedsLocationPermissionsScreen(carContext)
        }
    }

    private fun onConnectionStateUpdated(connectionState: Int) {
        logAndroidAuto("MainCarBreezeSession onConnectionStateUpdated")
        when (connectionState) {
            CarConnection.CONNECTION_TYPE_NOT_CONNECTED ->
                Analytics.logEvent(
                    Event.OBU_SYSTEM_EVENT,
                    Event.OBU_SYSTEM_CAR_DISCONNECTED,
                    com.ncs.breeze.common.analytics.Screen.OBU_APP
                )

            CarConnection.CONNECTION_TYPE_PROJECTION ->
                Analytics.logEvent(
                    Event.OBU_SYSTEM_EVENT,
                    Event.OBU_SYSTEM_CAR_CONNECT,
                    com.ncs.breeze.common.analytics.Screen.OBU_APP
                )

            else -> "Unknown car connection type"
        }
    }


    // Location permissions are required for this example. Check the state and replace the current
// screen if there is not one already set.
    private fun checkLocationPermissions() {
        PermissionsManager.areLocationPermissionsGranted(carContext).also { isGranted ->
            if (!isGranted) {
                NeedsLocationPermissionsScreen(carContext)
            }
        }
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)

        logAndroidAuto("onNewIntent $intent")
        if (intent.action.equals("androidx.car.app.action.NAVIGATE")) {
            handleVoiceCmd(intent)
        }

//        if (PermissionsManager.areLocationPermissionsGranted(carContext)) {
//            GeoDeeplinkNavigateAction(mapboxCarContext).onNewIntent(intent)
//        }

        intent.extras?.getBundle(Constants.CONGESTION_ALERT_EXTRA_DATA)?.let {
            val congestionAlertExtraData =
                it.getSerializable(Constants.CONGESTION_ALERT_LIST) as? Array<OBUTravelTimeData>
            if (congestionAlertExtraData != null) {
                senAnalyticClick(it)
                ToCarRx.postEventWithCacheLast(
                    NavigationCongestionAlertEventShow(
                        congestionAlertExtraData
                    )
                )
            }
        }

        intent.extras?.getBundle(Constants.PARKING_AVAILABILITY_EXTRA_DATA)?.let {
            val parkingAvailabilityExtraData =
                it.getSerializable(Constants.PARKING_AVAILABILITY_LIST) as Array<OBUParkingAvailability>?

            if (parkingAvailabilityExtraData != null) {
                senAnalyticClick(it)
                ToCarRx.postEventWithCacheLast(
                    NavigationParkingAvailabilityAlertEventShow(
                        parkingAvailabilityExtraData
                    )
                )
            }
        }

    }

    private fun senAnalyticClick(it: Bundle) {
        val screenName = it.getString(Constants.CONGESTION_ALERT_SCREEN) ?: ""
        val title = it.getString(Constants.CONGESTION_ALERT_TITLE) ?: ""
        when (screenName) {
            com.ncs.breeze.common.analytics.Screen.AA_DHU_NAVIGATION -> {
                Analytics.logClickEvent(
                    "[dhu],[popup_navigation_notification]:click_notification",
                    screenName,
                    title
                )
            }

            com.ncs.breeze.common.analytics.Screen.AA_DHU_CRUISE -> {
                Analytics.logClickEvent(
                    "[dhu],[popup_cruise_notification]:click_notification",
                    screenName,
                    title
                )
            }
        }
    }


    private fun handleVoiceCmd(intent: Intent) {
        if (intent.action.equals("androidx.car.app.action.NAVIGATE")) {
            if (breezeCarContext?.phoneAppLoggedIn == true) {
                carScreenManager?.parseSearchResult(intent)?.let {
                    ToCarRx.postEventWithCacheLast(OpenSearchScreenEvent(DataSearch().apply {
                        keySearch = it
                    }))
                }
            }
        }
    }


    override fun onCarConfigurationChanged(newConfiguration: Configuration) {
        logAndroidAuto("onCarConfigurationChanged isCarDarkMode = ${carContext.isDarkMode}")
        mapboxCarMapLoader.onCarConfigurationChanged(carContext)
    }

//    private val mapStyleUri: String
//        get() = MapboxAndroidAuto.options.run {
//            if (carContext.isDarkMode) {
//                mapNightStyle ?: mapDayStyle
//            } else {
//                mapDayStyle
//            }
//        }
}

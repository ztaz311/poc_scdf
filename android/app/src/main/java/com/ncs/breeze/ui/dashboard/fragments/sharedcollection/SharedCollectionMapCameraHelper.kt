package com.ncs.breeze.ui.dashboard.fragments.sharedcollection

import com.breeze.model.constants.Constants
import com.breeze.model.extensions.dpToPx
import com.mapbox.geojson.Geometry
import com.mapbox.geojson.GeometryCollection
import com.mapbox.geojson.Point
import com.mapbox.maps.CameraOptions
import com.mapbox.maps.EdgeInsets
import com.mapbox.maps.MapboxMap
import com.ncs.breeze.common.utils.BreezeMapZoomUtil
import com.ncs.breeze.common.utils.LocationBreezeManager

class SharedCollectionMapCameraHelper(val mapboxMap: MapboxMap) {
    private var mapCameraPadding =
        EdgeInsets(200.0.dpToPx(), 60.0.dpToPx(), 490.0.dpToPx(), 60.0.dpToPx())

    var cameraOptions: CameraOptions = CameraOptions.Builder()
        .padding(mapCameraPadding)
        .center(LocationBreezeManager.getInstance().currentLocation?.let {
            Point.fromLngLat(it.longitude, it.latitude)
        })
        .bearing(0.0)
        .pitch(0.0)
        .build()
        private set

    private var collectionCenterLocation: Point? = null

    var currentFocusedLocation: Point? = null
        private set

    fun updateCurrentFocusedLocation(newLocation: Point?) {
        currentFocusedLocation = newLocation
    }

    /**
     * update current mapCameraPadding
     * set -1 or negative value to keep current
     * */
    fun updateCameraPadding(
        top: Double = -1.0,
        left: Double = -1.0,
        bottom: Double = -1.0,
        right: Double = -1.0
    ) {
        mapCameraPadding = EdgeInsets(
            if (top < 0) mapCameraPadding.top else top,
            if (left < 0) mapCameraPadding.left else left,
            if (bottom < 0) mapCameraPadding.bottom else bottom,
            if (right < 0) mapCameraPadding.right else right,
        )
        cameraOptions = cameraOptions.toBuilder().padding(mapCameraPadding).build()
    }

    fun calculateCenterLocation(points: List<Geometry>) {
        var newCameraOptions = mapboxMap.cameraForGeometry(
            geometry = GeometryCollection.fromGeometries(points),
            padding = mapCameraPadding,
        )
        newCameraOptions.zoom?.takeIf { it > Constants.MAX_ZOOM_LEVEL }?.run {
            newCameraOptions = newCameraOptions.toBuilder().zoom(Constants.MAX_ZOOM_LEVEL).build()
        }
        cameraOptions = newCameraOptions
        collectionCenterLocation = newCameraOptions.center
    }

    fun recenterMap(onAnimationEnd: () -> Unit) {
        var newCameraOptions = cameraOptions
        if(currentFocusedLocation!= null){
            newCameraOptions = newCameraOptions.toBuilder().center(currentFocusedLocation).build()
        }
        BreezeMapZoomUtil.recenterMapToLocation(
            mapboxMap,
            newCameraOptions,
            animationEndCB = onAnimationEnd
        )
    }
}
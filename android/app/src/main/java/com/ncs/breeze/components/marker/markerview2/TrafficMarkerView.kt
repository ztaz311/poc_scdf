package com.ncs.breeze.components.marker.markerview2

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import com.breeze.model.api.response.traffic.TrafficResponse
import com.breeze.model.extensions.dpToPx
import com.facebook.react.bridge.Arguments
import com.google.android.gms.maps.model.LatLng
import com.mapbox.geojson.Point
import com.mapbox.maps.CameraOptions
import com.mapbox.maps.EdgeInsets
import com.mapbox.maps.MapboxMap
import com.mapbox.maps.plugin.animation.easeTo
import com.ncs.breeze.App
import com.ncs.breeze.databinding.TrafficMarkerWindowBinding
import com.ncs.breeze.reactnative.nativemodule.ShareBusinessLogicEvent
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class TrafficMarkerView(
    latLng: LatLng,
    mapboxMap: MapboxMap,
    val activity: Activity,
    itemId: String?
) : MarkerView2(mapboxMap = mapboxMap, itemId) {

    var currentState = 0

    var currentTrafficResponse: TrafficResponse? = null
    private val viewBinding: TrafficMarkerWindowBinding

    init {
        mLatLng = latLng
        viewBinding = TrafficMarkerWindowBinding.inflate(LayoutInflater.from(activity), null, false)
        mViewMarker = viewBinding.root
    }


    fun handleClick(
        item: TrafficResponse,
        recenter: Boolean = true,
        heightBottomSheet: Int,
        trafficIconClick: (Boolean) -> Unit
    ) {

        currentTrafficResponse = item
        viewBinding.tvTrafficName.text = item.name
        trafficIconClick.invoke(true)
        item.id?.let { idCamera ->
            sendEventTrafficSelected(idCamera)
        }

        if (recenter) {
            val cameraOptions = CameraOptions.Builder()
                .center(Point.fromLngLat(item.long!!.toDouble(), item.lat!!.toDouble()))
                .padding(
                    EdgeInsets(
                        10.0.dpToPx(),
                        10.0.dpToPx(),
                        heightBottomSheet.toDouble().dpToPx(),
                        10.0.dpToPx()
                    )
                )
                .build()
            mapboxMap.easeTo(cameraOptions)
        } else {
            updateTooltipPosition()
        }
    }


    override fun updateTooltipPosition() {
        val location = mLatLng ?: return
        val viewMarker = mViewMarker ?: return

        val screenCoordinate = mapboxMap.pixelForCoordinate(
            Point.fromLngLat(
                location.longitude,
                location.latitude
            )
        )
        viewMarker.measure(
            View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
            View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
        )
        viewMarker.x = screenCoordinate.x.toFloat() - viewMarker.measuredWidth / 2.0f
        viewMarker.y = screenCoordinate.y.toFloat() - viewMarker.measuredHeight
    }

    /**
     * send event traffic select
     */
    fun sendEventTrafficSelected(idTraffic: String) {
        App.instance?.let { app ->
            val shareBusinessLogicPackage = app.shareBusinessLogicPackage
            shareBusinessLogicPackage.getRequestsModule().flatMap {
                val rnData = Arguments.createMap().apply {
                    putString("cameraID", idTraffic)
                }
                it.callRN(ShareBusinessLogicEvent.SELECT_TRAFFIC_CAMERA.eventName, rnData)
            }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe()
        }
    }

    override fun showMarker() {
        super.showMarker()
        viewBinding.root.visibility = View.VISIBLE

    }

    override fun hideMarker() {
        super.hideMarker()
        viewBinding.root.visibility = View.INVISIBLE
    }

    override fun hideAllToolTips() {
        super.hideAllToolTips()
        if (viewBinding.llToolTip.isShown) {
            viewBinding.llToolTip.visibility = View.INVISIBLE
        }
    }
}
package com.ncs.breeze.di.module.fragment

import com.ncs.breeze.ui.dashboard.fragments.erpdetail.ErpDetailFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class ErpDetailFragmentModule {
    @ContributesAndroidInjector
    abstract fun contributeErpDetailFragment(): ErpDetailFragment
}
package com.ncs.breeze.car.breeze.location

import android.content.Context
import androidx.car.app.CarContext
import androidx.core.content.ContextCompat
import com.ncs.breeze.car.breeze.utils.BreezeUserPreferencesUtil
import com.ncs.breeze.R
import com.mapbox.maps.extension.style.expressions.dsl.generated.interpolate
import com.mapbox.maps.plugin.LocationPuck2D

object CarLocationPuck {

    fun navigationPuck2D(context: Context) = LocationPuck2D(
        bearingImage = ContextCompat.getDrawable(context, R.drawable.mapbox_navigation_puck_icon)
    )

    fun homePuck2D(context: Context) = LocationPuck2D(
        bearingImage = ContextCompat.getDrawable(context, R.drawable.cursor)
    )

    fun RouteIconCurrentUserPuck2D(context: Context) = LocationPuck2D(
        bearingImage = ContextCompat.getDrawable(context, R.drawable.cursor_with_layer)
    )


//    fun DesPuck2D(context: Context) = LocationPuck2D(
//        bearingImage = ContextCompat.getDrawable(context, R.drawable.destination_mark)
//    )
//
//
//    fun parkLocationPuck2D(context: Context) = LocationPuck2D(
//        bearingImage = ContextCompat.getDrawable(context, R.drawable.destination_mark)
//    )

    fun navigationCruise2D(context: CarContext) = LocationPuck2D(
        bearingImage = ContextCompat.getDrawable(
            context,
            R.drawable.ic_aa_puck
        ),
        scaleExpression = interpolate {
            linear()
            zoom()
            stop {
                literal(0.0)
                literal(0.6)
            }
            stop {
                literal(20.0)
                literal(1.0)
            }
        }.toJson()

    )

    // Example 2d compass puck
//    fun puck2D(carContext: CarContext) = LocationPuck2D(
//        topImage = carContext.getDrawable(R.drawable.mapbox_user_icon),
//        bearingImage = carContext.getDrawable(R.drawable.mapbox_user_bearing_icon),
//        shadowImage = carContext.getDrawable(R.drawable.mapbox_user_stroke_icon),
//    )

    // TODO Add a 3d model as an example, to the `assets` directory
//    private const val MODEL_SCALE = 0.2f
//    val puck3D: LocationPuck = LocationPuck3D(
//        modelUri = "asset://race_car_model.gltf",
//        modelScale = listOf(MODEL_SCALE, MODEL_SCALE, MODEL_SCALE)
//    )
}

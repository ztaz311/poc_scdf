package com.ncs.breeze.ui.login.fragment.forgotPassword.view

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import com.ncs.breeze.R
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.Event
import com.ncs.breeze.common.analytics.Screen
import com.breeze.model.constants.Constants
import com.breeze.customization.view.BreezeEditText
import com.ncs.breeze.databinding.FragmentForgotNewPasswordBinding
import com.ncs.breeze.ui.base.BaseActivity
import com.ncs.breeze.ui.base.BaseFragment
import com.ncs.breeze.ui.login.fragment.forgotPassword.viewmodel.ForgotNewPasswordFragmentViewModel
import javax.inject.Inject


class ForgotNewPasswordFragment :
    BaseFragment<FragmentForgotNewPasswordBinding, ForgotNewPasswordFragmentViewModel>()/*,OTPListener*/ {

    private var imm: InputMethodManager? = null

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val viewModel: ForgotNewPasswordFragmentViewModel by viewModels {
        viewModelFactory
    }

    var email: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            email = requireArguments().getString(Constants.KEYS.KEY_EMAIL)
        }

    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        initView()

        viewBinding.rlFragmentForget.setOnTouchListener { v, event ->
            when (event.action) {
                MotionEvent.ACTION_MOVE -> {
                    imm =
                        (activity as BaseActivity<*, *>).getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
                    imm!!.toggleSoftInput(
                        InputMethodManager.SHOW_FORCED,
                        InputMethodManager.HIDE_IMPLICIT_ONLY
                    )

                }

                MotionEvent.ACTION_UP -> {
                    imm =
                        (activity as BaseActivity<*, *>).getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
                    imm!!.toggleSoftInput(
                        InputMethodManager.SHOW_FORCED,
                        InputMethodManager.HIDE_IMPLICIT_ONLY
                    )

                }

                MotionEvent.ACTION_DOWN -> {
                    viewBinding.editNewPassword.requestFocus()
                    imm =
                        (activity as BaseActivity<*, *>).getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
                    imm!!.toggleSoftInput(
                        InputMethodManager.SHOW_FORCED,
                        InputMethodManager.HIDE_IMPLICIT_ONLY
                    )

                }
            }
            true
        }


    }

    private fun initView() {
        viewBinding.editNewPassword.getEditText()
            .setOnEditorActionListener(TextView.OnEditorActionListener { v, actionId, event ->
                if (actionId == EditorInfo.IME_ACTION_NEXT || actionId == EditorInfo.IME_ACTION_DONE) {
                    if (checkValidPassword()) {
                        val bundle = Bundle()
                        bundle.putString(
                            Constants.KEYS.KEY_PASSWORD,
                            viewBinding.editNewPassword.getEnteredText()
                        )
                        bundle.putString(Constants.KEYS.KEY_EMAIL, email)
                        (activity as BaseActivity<*, *>).addFragment(
                            ForgotVerifyOTPFragment.newInstance(),
                            bundle,
                            Constants.TAGS.FORGET_PASSWORD_TAG
                        )
                        return@OnEditorActionListener true
                    } else {
                        return@OnEditorActionListener false
                    }
                }
                false
            })
        viewBinding.editNewPassword.requestFocus()
        clickListeners()
        viewBinding.btForgetNewNext.setBGGrey()
        viewBinding.editNewPassword.analyticsScreenName = getScreenName()
        viewBinding.editNewPassword.setEditTextListener(object :
            BreezeEditText.BreezeEditTextListener {
            override fun onBreezeEditTextFocusChange(isFocused: Boolean) {
                if (!isFocused) {
                    Analytics.logEditEvent(Event.NEW_PASSWORD, getScreenName())
                }
            }
        })
        viewBinding.editNewPassword.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                setNormalBg()
            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            }

            override fun afterTextChanged(s: Editable) {
            }
        })
    }

    private fun setNormalBg() {
        if (viewBinding.editNewPassword.getEnteredText()!!.length > 1
        ) {
            viewBinding.btForgetNewNext.setBGNormal()
        } else {
            viewBinding.btForgetNewNext.setBGGrey()

        }
    }

    companion object {
        @JvmStatic
        fun newInstance() =
            ForgotNewPasswordFragment().apply {
                arguments = Bundle().apply {

                }
            }
    }

    override fun inflateViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?,
        attachToContainer: Boolean
    ) = FragmentForgotNewPasswordBinding.inflate(inflater, container, attachToContainer)

    override fun getViewModelReference(): ForgotNewPasswordFragmentViewModel {
        return viewModel
    }

    fun checkValidPassword(): Boolean {
        var isValid = true
        if (TextUtils.isEmpty(viewBinding.editNewPassword.getEnteredText())) {
            isValid = false
            viewBinding.editNewPassword.setBGError()
        } else {
            if (viewBinding.editNewPassword.getEnteredText()
                    ?.matches(Regex(Constants.REGEX.PASSWORD_REGEX)) == false
            ) {
                isValid = false
                viewBinding.editNewPassword.showErrorView(resources.getString(R.string.error_password_incorrect_format))
            }
        }
        return isValid
    }

    override fun onStop() {
        super.onStop()
        if (imm != null) {
            imm!!.toggleSoftInputFromWindow(
                viewBinding.rlFragmentForget.getApplicationWindowToken(),
                InputMethodManager.SHOW_FORCED, 0
            )
        }
    }

    override fun onDestroy() {
        super.onDestroy()


    }

    private fun clickListeners() {
        viewBinding.backButton.setOnClickListener {
            onBackPressed()
        }
        viewBinding.btForgetNewNext.setOnClickListener {
            Analytics.logClickEvent(Event.NEXT, getScreenName())
            if (checkValidPassword()) {
                val bundle = Bundle()
                bundle.putString(
                    Constants.KEYS.KEY_PASSWORD,
                    viewBinding.editNewPassword.getEnteredText()
                )
                bundle.putString(Constants.KEYS.KEY_EMAIL, email)
                (activity as BaseActivity<*, *>).addFragment(
                    ForgotVerifyOTPFragment.newInstance(),
                    bundle,
                    Constants.TAGS.FORGET_PASSWORD_TAG
                )
            }
        }
    }

    override fun onKeyboardVisibilityChanged(isOpen: Boolean) {
        if (isOpen) {
            viewBinding.btForgetNewNext.visibility = View.GONE
        } else {
            viewBinding.btForgetNewNext.visibility = View.VISIBLE
        }
    }

    override fun getScreenName(): String {
        return Screen.FORGOT_PASSWORD_STEP2
    }
}
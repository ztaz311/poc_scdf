package com.ncs.breeze.common.utils

import android.content.Context
import android.content.pm.PackageInfo
import android.net.Uri
import android.provider.SyncStateContract
import android.util.Log
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.ktx.storage
import com.ncs.breeze.BuildConfig
import com.ncs.breeze.R
import com.ncs.breeze.common.ServiceLocator
import com.ncs.breeze.common.extensions.android.getAppPreference
import com.breeze.model.FirebaseDB
import com.breeze.model.constants.Constants
import com.ncs.breeze.common.storage.AppPrefsKey
import timber.log.Timber
import java.io.*
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class BreezeHistoryRecorderUtil @Inject constructor(
    userPreferenceUtil: BreezeUserPreferenceUtil,
    appContext: Context
) {

    var userPreferenceUtil = userPreferenceUtil
    var appContext = appContext

    companion object {
        val utcFormatter = SimpleDateFormat("yyyy-MM-dd_HH:mm:ss", Locale.US)
            .also { it.timeZone = TimeZone.getDefault() }
    }

    // Create a storage reference from our app
    var storageRef = Firebase.storage.reference

    // Create a child reference
    // imagesRef now points to "images"
    var folderRef: StorageReference = storageRef.child(Constants.CLOUD_STORAGE_FOLDER)

    val db = Firebase.firestore


    suspend fun saveHistoryData(context: Context, description: String, filePath: String?) {
        Log.d("testing-log", "printing file path: ${filePath}")
//        CoroutineScope(Dispatchers.IO).launch {

        val userId = (userPreferenceUtil.getUserStoredData()?.cognitoID
            ?: "").ifEmpty { appContext.getString(R.string.vehicle_type_private_car) }
        val secondaryFirebaseID = ServiceLocator.getSecondaryFirebaseAuthUserId()
        var fcmId = ""
        if (BuildConfig.FLAVOR != "production") {
            fcmId = appContext.getAppPreference()?.getString(AppPrefsKey.PUSH_MESSAGING_TOKEN) ?: ""
        }

        val file = File(filePath)
        val fileName = createFirebaseFileName(description) + ".pbf.gz"
        val uri = Uri.fromFile(file)
        if (uri != null) {
            Log.d("testing-log", "File is not null")

            //FIXME: GZIP ENCODING not done
            val fileRef = folderRef.child("${userId}/${fileName}")
            fileRef.putFile(uri)
                .continueWithTask { uploadTask ->
                    if (!uploadTask.isSuccessful) {
                        uploadTask.exception?.let {
                            Log.d("testing-log", "Upload failed")
                            throw it
                        }
                    }
                    fileRef.downloadUrl
                }
                .addOnCompleteListener { urlTask ->
                    if (urlTask.isSuccessful) {
                        val downloadUri = urlTask.result
                        Log.d("testing-log", "Download URI is: " + downloadUri.toString())

                        val pInfo: PackageInfo =
                            context.getPackageManager().getPackageInfo(context.packageName, 0)
                        val version = pInfo.versionName
                        val buildNumber = pInfo.versionCode

                        val dbItem = FirebaseDB(
                            description = description,
                            fileDownloadURL = downloadUri.toString(),
                            deviceOS = "ANDROID",
                            deviceOSVersion = Utils.getDeviceOS(),
                            deviceModel = Utils.getDeviceModel(),
                            deviceManufacturer = Utils.getDeviceManufacturer(),
                            appVersion = String.format("V%s(%s)", version, buildNumber),
                            time = utcFormatter.format(Date()),
                            secondaryFirebaseID = secondaryFirebaseID,
                            fcmId = fcmId,
                        )
                        db.collection(userId).document(fileName).set(dbItem)
                            .addOnSuccessListener { documentReference ->
                                file.delete()
                                Log.d("testing-log", "DocumentSnapshot written")
                            }
                            .addOnFailureListener { e ->
                                //ToDo - Ankur - check if we can delete "fileRef.delete()" here
                                file.delete()
                                Log.w("testing-log", "Error adding document", e)
                            }
                    } else {
                        // Handle failures
                        // ...
                        //ToDo - Ankur - check if we can delete "fileRef.delete()" here
                        file.delete()
                    }
                }
        }
    }

    //ToDo - Review and delet this method
    @Deprecated("Pass Directly File reference to firebase sdk to upload. Do not pass input stream.")
    private fun readFile(fileName: String?, context: Context, description: String): File? {
        val stringBuilder = StringBuilder()
        var line: String?
        var br: BufferedReader? = null
        try {
            br = BufferedReader(FileReader(File(fileName)))
            while (br.readLine().also { line = it } != null) {
                Log.d("testing-abc", "printing line: " + line)
                stringBuilder.append(line)
            }

            val cacheDirectory = context.getFilesDir()
            val historyDirectory = File(cacheDirectory, "history-cache")
                .also { it.mkdirs() }
            val file =
                File.createTempFile(
                    createFirebaseFileName(description),
                    ".pbf.gz",
                    historyDirectory
                )
            file.writeText(stringBuilder.toString())

            return file


        } catch (e: FileNotFoundException) {
            Timber.e(e, "Error")
        } catch (e: IOException) {
            Timber.e(e, "Error")
        } finally {
            br?.close()
        }
        return null
    }

    private fun createFirebaseFileName(description: String): String {
        return "${description}_${utcFormatter.format(Date())}"
    }
}
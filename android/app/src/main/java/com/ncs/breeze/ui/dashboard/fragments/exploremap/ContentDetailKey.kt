package com.ncs.breeze.ui.dashboard.fragments.exploremap

data class ContentDetailKey(
    val code: String,
    val tooltip_type: String
)

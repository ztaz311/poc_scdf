package com.ncs.breeze.ui.utils

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.Rect
import android.graphics.drawable.ColorDrawable
import android.text.SpannableStringBuilder
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import com.ncs.breeze.R
import com.breeze.customization.view.BreezeButton
import com.ncs.breeze.ui.login.fragment.signUp.view.TermsAndCondnFragment

object DialogUtilsBreeze {

    fun showDialogGetUserFails(context: Context, retryAction: () -> Unit, cancel: () -> Unit) {
        val builder: AlertDialog.Builder = context.let {
            AlertDialog.Builder(it)
        }
        builder.let {
            val message: String = context.getString(R.string.message_load_user_setting_fails)
            it.setTitle("")
                .setMessage(message)
                .setPositiveButton(
                    R.string.action_retry
                ) { dialog, _ ->
                    retryAction.invoke()
                    dialog.dismiss()
                }
                .setNegativeButton("Cancel"){
                        dialog, _ ->
                    cancel.invoke()
                    dialog.dismiss()
                }
            val alertDialog: AlertDialog = it.create()
            alertDialog.setOnShowListener {
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).isAllCaps = false
                alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).isAllCaps = false
            }
            alertDialog.setCanceledOnTouchOutside(false)
            alertDialog.show()
        }
    }


    fun showSuccessDialog(
        activity: Activity,
        messageShow: SpannableStringBuilder,
        fromScreen: String,
        existUser: Boolean = false,
        onClickDone: () -> Unit,
    ) {
        val dialog = activity.let {
            Dialog(it)
        }
        dialog.setContentView(R.layout.dialog_create_account_successful)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCanceledOnTouchOutside(false)
        dialog.setCancelable(false)

        val greeting = dialog.findViewById<TextView>(R.id.greeting)
        greeting.text = messageShow

        val successMessage = dialog.findViewById<TextView>(R.id.success_message)
        if (existUser || fromScreen == TermsAndCondnFragment.FROM_GUEST) {
            successMessage.visibility = View.GONE
        } else {
            successMessage.visibility = View.VISIBLE
        }

        val button = dialog.findViewById<BreezeButton>(R.id.button_okay)
        button.setOnClickListener {
            dialog.dismiss()
            onClickDone()
        }

        button.updateText(if (existUser) R.string.button_okay else R.string.lets_go)

        dialog.create()
        dialog.show()

        val displayRectangle = Rect()
        val window: Window = activity.window
        window.decorView.getWindowVisibleDisplayFrame(displayRectangle)

        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window!!.attributes)
        lp.width = (displayRectangle.width() * 0.9f).toInt()
        dialog.window!!.attributes = lp

    }
}
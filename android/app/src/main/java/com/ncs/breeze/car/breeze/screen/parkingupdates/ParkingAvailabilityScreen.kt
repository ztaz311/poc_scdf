package com.ncs.breeze.car.breeze.screen.parkingupdates

import androidx.activity.OnBackPressedCallback
import androidx.car.app.model.*
import androidx.car.app.model.Row.IMAGE_TYPE_LARGE
import androidx.core.graphics.drawable.IconCompat
import androidx.lifecycle.lifecycleScope
import com.ncs.breeze.R
import com.ncs.breeze.car.breeze.MainBreezeCarContext
import com.ncs.breeze.car.breeze.base.BaseScreenCar
import com.ncs.breeze.car.breeze.utils.LimitClick
import com.ncs.breeze.car.breeze.utils.setClickSafe
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.Screen
import com.breeze.model.obu.OBUParkingAvailability
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class ParkingAvailabilityScreen(
    val mainCarContext: MainBreezeCarContext,
    private val parkingAvailability: Array<OBUParkingAvailability>?
) : BaseScreenCar(mainCarContext.carContext) {

    lateinit var parkingTypeAvailableIcon: CarIcon
    lateinit var parkingTypeFillingUpSoonIcon: CarIcon
    lateinit var parkingTypeFullIcon: CarIcon

    override fun onGetTemplate(): Template {
        val listTemplateBuilder = ListTemplate.Builder()
            .setLoading(false)
            .setHeaderAction(Action.BACK)
            .setTitle(carContext.getString(R.string.parking_updates))
        listTemplateBuilder.setSingleList(generateItemList())
        return listTemplateBuilder.build()
    }

    private val callback = object : OnBackPressedCallback(
        true // default to enabled
    ) {
        override fun handleOnBackPressed() {
            Analytics.logClickEvent("[dhu],[parking_availability]:back", Screen.AA_DHU_NAVIGATION)
            screenManager.pop()
        }
    }

    override fun onCreateScreen() {
        super.onCreateScreen()
        mainCarContext.carContext.onBackPressedDispatcher.addCallback( { lifecycle }, callback)
        parkingTypeAvailableIcon = CarIcon.Builder(
            IconCompat.createWithResource(
                carContext,
                R.drawable.ic_android_auto_parking_slots_nearby_all_available
            )
        ).build()
        parkingTypeFillingUpSoonIcon = CarIcon.Builder(
            IconCompat.createWithResource(
                carContext,
                R.drawable.ic_android_auto_parking_slots_filling_up_soon
            )
        ).build()
        parkingTypeFullIcon = CarIcon.Builder(
            IconCompat.createWithResource(
                carContext,
                R.drawable.ic_android_auto_parking_slots_nearby_all_full
            )
        ).build()
        startAutoGoBackCountdown()
    }

    private fun startAutoGoBackCountdown() {
        lifecycleScope.launch {
            delay(15000)
            screenManager.pop()
        }
    }

    override fun getScreenName(): String {
        return Screen.AA_CONGESTION_ALERT_SCREEN
    }

    private fun generateItemList(): ItemList {
        val listBuilder = ItemList.Builder()

        if (parkingAvailability.isNullOrEmpty()) {
            listBuilder.setNoItemsMessage(carContext.getString(R.string.empty_list))
        } else {
            parkingAvailability.forEach { item ->
                val (parkingIcon, displayedText) = parseItemType(item)
                val row = Row.Builder()
                    .setTitle(item.name)
                    .setBrowsable(false)
                    .setImage(parkingIcon, IMAGE_TYPE_LARGE)
                    .addText(displayedText)
                    .setClickSafe {
                        LimitClick.handleSafe {
                            Analytics.logClickEvent("[dhu],[parking_availability]:select_carpark", Screen.AA_DHU_NAVIGATION)
                            finish()
                        }
                    }
                listBuilder.addItem(row.build())
            }
        }

        return listBuilder.build()
    }

    private fun parseItemType(obuParkingAvailability: OBUParkingAvailability): Pair<CarIcon, String> {
        var displayedText = ""
        var parkingIcon: CarIcon
        val availLots = obuParkingAvailability.availableLots.toIntOrNull() ?: 0
        if (obuParkingAvailability.color.isNullOrEmpty()) {
            if (availLots >= 80) {
                parkingIcon = parkingTypeAvailableIcon
                displayedText =
                    carContext.getString(R.string.parking_updates_available_slots, availLots)
            } else if (availLots in (1..79)) {
                parkingIcon = parkingTypeFillingUpSoonIcon
                displayedText =
                    carContext.getString(R.string.parking_updates_available_slots, availLots)
            } else {
                parkingIcon = parkingTypeFullIcon
                displayedText = carContext.getString(R.string.parking_updates_full)
            }
        } else {
            if (obuParkingAvailability.color.contentEquals("red", true)) {
                parkingIcon = parkingTypeFullIcon
                displayedText = carContext.getString(R.string.parking_updates_full)
            } else if (obuParkingAvailability.color.contentEquals("yellow", true)) {
                parkingIcon = parkingTypeFillingUpSoonIcon
                displayedText = carContext.getString(
                    R.string.parking_updates_available_slots,
                    availLots
                )
            } else {
                parkingIcon = parkingTypeAvailableIcon
                displayedText = carContext.getString(
                    R.string.parking_updates_available_slots,
                    availLots
                )
            }
        }

        return Pair(parkingIcon, displayedText)
    }
}

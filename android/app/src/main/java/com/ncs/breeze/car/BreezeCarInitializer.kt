package com.ncs.breeze.car

import android.content.Context
import androidx.lifecycle.Lifecycle
import com.mapbox.androidauto.MapboxCarOptions
import com.mapbox.maps.MapInitOptions
import com.mapbox.maps.Style
//import com.ncs.breeze.BuildConfig
//
//class BreezeCarInitializer : MapboxCarInitializer {
//
//    override fun create(lifecycle: Lifecycle, context: Context): MapboxCarOptions {
//        val mapInitOptions = MapInitOptions(context)
//        return MapboxCarOptions.Builder(mapInitOptions)
//            .mapDayStyle(BuildConfig.MAPBOX_STYLE_URL_LIGHT)
//            .mapNightStyle(BuildConfig.MAPBOX_STYLE_URL_DARK)
//            .replayEnabled(ENABLE_REPLAY)
//            .build()
//    }
//
//    companion object {
//        const val ENABLE_REPLAY = true
//        const val DAY_STYLE = Style.TRAFFIC_DAY
//        const val NIGHT_STYLE = Style.TRAFFIC_NIGHT
//    }
//}

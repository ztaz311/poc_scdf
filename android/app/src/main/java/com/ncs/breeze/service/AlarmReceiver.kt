package com.ncs.breeze.service

import android.app.NotificationManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.breeze.model.constants.Constants
import com.ncs.breeze.common.utils.notification.PushNotificationUtils
import com.ncs.breeze.notification.FCMListenerService
import com.ncs.breeze.ui.HandleUpdateNotificationActivity

class AlarmReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        val title = intent.getStringExtra(PARAM_NOTIFICATION_TITLE)
        val description = intent.getStringExtra(PARAM_NOTIFICATION_DESC)

        sendUpdateNotification(
            context, title ?: "TEST", description ?: "TEST"
        )
    }

    companion object {
        var PARAM_NOTIFICATION_TITLE = "PARAM_NOTIFICATION_TITLE"
        var PARAM_NOTIFICATION_DESC = "PARAM_NOTIFICATION_DESC"

        fun sendUpdateNotification(context: Context, title: String, description: String) {
            val channelId = FCMListenerService.NOTIFICATION_CHANNEL_ID_UPDATE_APP
            val intent = Intent(context, HandleUpdateNotificationActivity::class.java)
            intent.putExtra(
                Constants.SHARED_PREF_KEY.HANDLE_PUSH_NOTIFICATION_CHANNEL,
                channelId
            )
            intent.putExtra(Constants.SHARED_PREF_KEY.HANDLE_PUSH_NOTIFICATION_ID, "")
            PushNotificationUtils.createBreezeNotification(
                context = context,
                notificationId = FCMListenerService.UPDATE_NOTIFICATION_ID,
                channelId = channelId,
                channelName = FCMListenerService.NOTIFICATION_CHANNEL_NAME_UPDATE_APP,
                title = title,
                description = description,
                intent = intent,
                importanceLevel = NotificationManager.IMPORTANCE_HIGH
            )
        }
    }
}
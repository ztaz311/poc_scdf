package com.ncs.breeze.helper.marker

import com.ncs.breeze.components.layermanager.MarkerLayerManager


open class DashBoardAmenityMarkerHelper(
    amenityMarkerManager: MarkerLayerManager,
    nightModeEnabled: Boolean, voucherEnabled: Boolean
) :
    AmenityMarkerHelper(amenityMarkerManager, nightModeEnabled, voucherEnabled) {

}
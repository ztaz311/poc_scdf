package com.ncs.breeze.components.layermanager.impl.cluster

import android.graphics.Color
import androidx.annotation.UiThread
import androidx.collection.ArraySet
import androidx.collection.arraySetOf
import com.breeze.model.constants.AmenityType.CARPARK
import com.breeze.model.constants.Constants
import com.breeze.model.extensions.toBitmap
import com.mapbox.android.gestures.StandardScaleGestureDetector
import com.mapbox.geojson.Feature
import com.mapbox.geojson.FeatureCollection
import com.mapbox.geojson.Point
import com.mapbox.maps.MapView
import com.mapbox.maps.RenderedQueryGeometry
import com.mapbox.maps.RenderedQueryOptions
import com.mapbox.maps.ScreenBox
import com.mapbox.maps.ScreenCoordinate
import com.mapbox.maps.Style
import com.mapbox.maps.extension.style.expressions.dsl.generated.all
import com.mapbox.maps.extension.style.expressions.dsl.generated.literal
import com.mapbox.maps.extension.style.expressions.dsl.generated.switchCase
import com.mapbox.maps.extension.style.expressions.generated.Expression
import com.mapbox.maps.extension.style.layers.addLayerBelow
import com.mapbox.maps.extension.style.layers.generated.CircleLayer
import com.mapbox.maps.extension.style.layers.generated.SymbolLayerDsl
import com.mapbox.maps.extension.style.sources.addSource
import com.mapbox.maps.extension.style.sources.generated.GeoJsonSource
import com.mapbox.maps.extension.style.sources.generated.geoJsonSource
import com.mapbox.maps.extension.style.sources.getSourceAs
import com.mapbox.maps.plugin.gestures.OnMapClickListener
import com.mapbox.maps.plugin.gestures.OnScaleListener
import com.mapbox.maps.plugin.gestures.addOnMapClickListener
import com.mapbox.maps.plugin.gestures.addOnScaleListener
import com.mapbox.maps.plugin.gestures.removeOnMapClickListener
import com.mapbox.maps.plugin.gestures.removeOnScaleListener
import com.ncs.breeze.R
import com.ncs.breeze.common.utils.BreezeMapDataHolder
import com.ncs.breeze.common.utils.ParkingIconUtils
import com.ncs.breeze.components.layermanager.MarkerLayerManager
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import timber.log.Timber
import java.util.TreeMap
import kotlin.collections.set


/**
 * type (layer type) -> represents either an amenity or amenity group
 * featureType -> represents the amenity feature type, can be the same as type if the layer type is also an amenity
 */
class ClusteredMarkerLayerManager(
    mapView: MapView,
    nightMode: Boolean = false,
) :
    MarkerLayerManager(mapView, nightMode) {

    val nonClusteredTypes: ArraySet<String> = arraySetOf(DROP_PIN_TYPE)

    var carParkGroupId: String? = null
        private set
    private val clusterColorMap: TreeMap<String, Int> = TreeMap<String, Int>()
    private val clusterLayerIds: HashMap<String, String> = HashMap()

    var clusterLayerClickHandler: ClusterLayerClickHandler? = null

    private val mapCircleLayerClickListener =
        OnMapClickListener { point ->
            val mapboxMap = mapViewRef.get()?.getMapboxMap() ?: return@OnMapClickListener false
            val clicked = mapboxMap.pixelForCoordinate(point)
            mapboxMap.queryRenderedFeatures(
                RenderedQueryGeometry(
                    ScreenBox(
                        ScreenCoordinate(clicked.x - 15, clicked.y - 15),
                        ScreenCoordinate(clicked.x + 15, clicked.y + 15)
                    )
                ),
                RenderedQueryOptions(clusterLayerIds.keys.toList(), literal(true))
            ) { expected ->
                if (expected.isValue) {
                    if (expected.value!!.size > 0) {
                        val feature = expected.value!![0].feature
                        mapboxMap.getGeoJsonClusterExpansionZoom(
                            expected.value!![0].source,
                            feature
                        ) { zoomExpected ->
                            if (zoomExpected.isValue) {
                                val zoomLevel = zoomExpected.value!!.value?.contents
                                if (zoomLevel is Number) {
                                    clusterLayerClickHandler?.handleOnclick(point, zoomLevel)
                                }
                                Timber.d("zoomExpected.value!!.value" + zoomExpected.value.toString())
                            }
                        }
                    }
                    Timber.d("" + expected.value.toString())
                }
            }
            return@OnMapClickListener false
        }

    private val onCameraZoomChangedListener = object : OnScaleListener {
        override fun onScale(detector: StandardScaleGestureDetector) {
            // no op
        }

        override fun onScaleBegin(detector: StandardScaleGestureDetector) {
            // no op
        }

        override fun onScaleEnd(detector: StandardScaleGestureDetector) {
            // only run if block if zoom value is not null and < 15
            if ((mapViewRef.get()?.getMapboxMap()?.cameraState?.zoom ?: 16.0) < 15) {
                removeMarkerTooltipView()
            }
        }

    }

    init {
        mapView.getMapboxMap().addOnMapClickListener(mapCircleLayerClickListener)
        mapView.getMapboxMap().addOnScaleListener(onCameraZoomChangedListener)
    }

    /**
     * loads the color for clustered groups
     * should be called after BreezeMapDataHolder is initialized
     */
    fun loadAmenityClusteredColorMap(types: List<String>) {
        types.forEach { type ->
            BreezeMapDataHolder.amenitiesPreferenceHashmap[type]?.let { pref ->
                if (pref.clusterId != null && pref.clusterColor != null) {
                    clusterColorMap[pref.clusterId.toString()] = Color.parseColor(pref.clusterColor)
                }
            }
        }
    }

    /**
     * loads the group id for car parks
     * should be called after BreezeMapDataHolder is initialized
     */
    fun loadCarParkClusterId() {
        BreezeMapDataHolder.amenitiesPreferenceHashmap[CARPARK]?.let { pref ->
            carParkGroupId = pref.clusterId.toString()
        }
    }

    override fun updateSourceGeoJson(
        type: String,
        featureCollection: FeatureCollection
    ) {
        val sourceId = type + SOURCE_POSTFIX
        val style = mapViewRef.get()?.getMapboxMap()?.getStyle()
        val source = style?.getSourceAs<GeoJsonSource>(sourceId)
        val sourceClustered = style?.getSourceAs<GeoJsonSource>("$sourceId$CLUSTERED")
        source?.featureCollection(featureCollection)
        sourceClustered?.featureCollection(featureCollection)
    }


    override fun updateSourceCollection(
        style: Style,
        sourceId: String,
        featureCollection: FeatureCollection
    ) {
        val source = style.getSourceAs<GeoJsonSource>(sourceId)
        val sourceClustered = style.getSourceAs<GeoJsonSource>("$sourceId$CLUSTERED")
        source?.featureCollection(featureCollection)
        sourceClustered?.featureCollection(featureCollection)
    }

    /**
     * appends markers without replacing previous features added
     */
    @UiThread
    fun appendMarkers(type: String, feature: List<Feature>) {
        var featureCollection = FeatureCollection.fromFeatures(ArrayList(feature))
        val sourceId = type + SOURCE_POSTFIX
        val style = mapViewRef.get()?.getMapboxMap()?.getStyle()
        val source = style?.getSourceAs<GeoJsonSource>(sourceId)
        if (source == null) {
            val iconSizeExp = addStyleIconSizeExpression()
            addStyleSource(type, sourceId, featureCollection)
            addStyleLayerAndReArrange(type, sourceId, iconSizeExp)
        } else {
            val featureCollectionData = markerFeatures[type]
            if (featureCollectionData?.features() != null) {
                val updatedFeatures = ArrayList(featureCollection.features())
                updatedFeatures.addAll(featureCollectionData.features()!!)
                featureCollection = FeatureCollection.fromFeatures(updatedFeatures)
            }
            updateSourceCollection(style, sourceId, featureCollection)
        }
        markerFeatures[type] = featureCollection
    }

    override fun addStyleSource(
        type: String,
        sourceId: String,
        featureCollection: FeatureCollection
    ) {
        mapViewRef.get()?.getMapboxMap()?.getStyle {
            it.addSource(
                geoJsonSource(sourceId) {
                    featureCollection(featureCollection)
                    cluster(false)
                }
            )
            it.addSource(
                geoJsonSource("$sourceId$CLUSTERED") {
                    featureCollection(featureCollection)
                    cluster(true)
                    clusterRadius(CLUSTER_RADIUS)
                    clusterMaxZoom(Constants.CLUSTER_MAX_ZOOM)
                }
            )
        }

        sourceIds["$sourceId$CLUSTERED"] = type
        sourceIds[sourceId] = type
    }

    override fun addIconImageExpression(
        symbolLayerDsl: SymbolLayerDsl,
        type: String
    ) {
        symbolLayerDsl.iconImage(
            switchCase {
                for (parkingType in ParkingIconUtils.ParkingMarkerImageIconType.values()) {
                    eq {
                        get {
                            literal(ICON_KEY)
                        }
                        literal(parkingType.type)
                    }
                    concat { get { literal(FEATURE_TYPE) }; literal("-${parkingType.type}$ICON_POSTFIX") }
                }
                //all unselected night
                eq {
                    get {
                        literal(ICON_KEY)
                    }
                    literal(ParkingIconUtils.ImageIconType.UNSELECTED_NIGHT.type)
                }
                concat { get { literal(FEATURE_TYPE) }; get { literal(IMAGE_TYPE) }; literal("-${ParkingIconUtils.ImageIconType.UNSELECTED_NIGHT.type}$ICON_POSTFIX") }
                //all selected night
                eq {
                    get {
                        literal(ICON_KEY)
                    }
                    literal(ParkingIconUtils.ImageIconType.SELECTED_NIGHT.type)
                }
                concat { get { literal(FEATURE_TYPE) }; get { literal(IMAGE_TYPE) }; literal("-${ParkingIconUtils.ImageIconType.SELECTED_NIGHT.type}$ICON_POSTFIX") }
                //all unselected
                eq {
                    get {
                        literal(ICON_KEY)
                    }
                    literal(ParkingIconUtils.ImageIconType.UNSELECTED.type)
                }
                concat { get { literal(FEATURE_TYPE) }; get { literal(IMAGE_TYPE) }; literal("-${ParkingIconUtils.ImageIconType.UNSELECTED.type}$ICON_POSTFIX") }

                //default
                concat { get { literal(FEATURE_TYPE) }; get { literal(IMAGE_TYPE) };literal("-${ParkingIconUtils.ImageIconType.SELECTED.type}$ICON_POSTFIX") }
            }
        )
    }


    override fun addStyleLayer(
        type: String,
        sourceId: String,
        iconSizeExp: Expression
    ) {
        if (nonClusteredTypes.contains(type)) {
            super.addStyleLayer(type, sourceId, iconSizeExp)
            return
        }
        super.addStyleLayer(type, sourceId, iconSizeExp)
        // adds a hidden tooltip to non carparks and visible tooltip for carparks within the group
        carParkGroupId?.let {
            if (type == it) {
                addCarParkToolTipLayer(type, sourceId)
            }
        }
    }

    override fun addBaseAmenityLayer(
        type: String,
        sourceId: String,
        icon_size_exp: Expression
    ) {
        if (nonClusteredTypes.contains(type)) {
            super.addBaseAmenityLayer(type, sourceId, icon_size_exp)
            return
        }
        addClusteredLayer(type, sourceId, icon_size_exp)
    }

    override fun addCarParkToolTipLayer(
        type: String,
        sourceId: String
    ) {
        if (nonClusteredTypes.contains(type)) {
            super.addCarParkToolTipLayer(type, sourceId)
            return
        }
        super.addCarParkToolTipLayer(type, "$sourceId$CLUSTERED")
    }

    override fun toolTipCarParkExpression() = Expression.all {
        eq {
            get {
                literal(FEATURE_TYPE)
            }
            literal(CARPARK)
        }
        eq {
            get {
                literal(HIDDEN)
            }
            literal(false)
        }
        eq {
            get {
                literal(CARPARK_PRICE_SHOW)
            }
            literal(true)
        }
    }


    private fun addClusteredLayer(
        type: String,
        sourceId: String,
        icon_size_exp: Expression
    ) {
        super.addBaseAmenityLayer(type, "$sourceId$CLUSTERED", icon_size_exp)

        val layerBelow = getLowestAmenityLayer()

        val circleLayer = CircleLayer("$type$LAYER_POSTFIX$FILTERED_CLUSTER", "$sourceId$CLUSTERED")
        circleLayer.circleRadius(5.0)
        circleLayer.circleColor(clusterColorMap[type] ?: Color.BLACK)
        circleLayer.filter(
            all {
                has(POINT_COUNT)
                gte {
                    literal(POINT_COUNT)
                    literal(1)
                }
            }
        )
        mapViewRef.get()?.getMapboxMap()?.getStyle()?.addLayerBelow(circleLayer, layerBelow)
        clusterLayerIds.put("$type$LAYER_POSTFIX$FILTERED_CLUSTER", type)
    }

    private fun getLowestAmenityLayer() = if (layerIds.keys.size > 0) {
        layerIds.keys.first()
    } else {
        if (mapViewRef.get()?.getMapboxMap()?.getStyle()?.styleLayers.isNullOrEmpty()) {
            null
        } else {
            mapViewRef.get()?.getMapboxMap()?.getStyle()?.styleLayers?.last()?.id
        }
    }

    /**
     * removes marker features
     * type can be either a group of amenity or an amenity type
     */
    @UiThread
    open fun removeMarkerFeatures(type: String, features: List<Feature>) {
        val sourceId = type + SOURCE_POSTFIX
        val source =
            mapViewRef.get()?.getMapboxMap()?.getStyle()?.getSourceAs<GeoJsonSource>(sourceId)
        val sourceClustered =
            mapViewRef.get()?.getMapboxMap()?.getStyle()
                ?.getSourceAs<GeoJsonSource>("$sourceId$CLUSTERED")
        if (source != null) {
            val featureCollectionData = markerFeatures[type]
            var featureCollection = FeatureCollection.fromFeatures(listOf())
            if (featureCollectionData?.features() != null) {
                val updatedFeatures = ArrayList(featureCollectionData.features())
                updatedFeatures.removeIf { updatedFeature ->
                    val exists = features.find {
                        return@find ((it.id() != null) && (it.id() == updatedFeature.id()))
                    }
                    return@removeIf (exists != null)
                }
                featureCollection = FeatureCollection.fromFeatures(updatedFeatures)
            }
            source.featureCollection(featureCollection)
            sourceClustered?.featureCollection(featureCollection)
            markerFeatures[type] = featureCollection
        }
    }


    override fun removeAllMarkersExcludingType(exclude: String) {
        val keys = markerFeatures.keys()
        for (type in keys) {
            val features = ArrayList(markerFeatures[type]!!.features())
            if (type == exclude) {
                continue
            }
            val excludedFeatures = ArrayList<Feature>()
            val includedFeatures = ArrayList<Feature>()
            features.forEach {
                // handles null and excluded property
                if (it.getStringProperty(FEATURE_TYPE) != exclude) {
                    excludedFeatures.add(it)
                } else {
                    includedFeatures.add(it)
                }
            }
            if (excludedFeatures.size == features.size) {
                removeMarkerTooltipView(type)
                removeSourceAndLayer(type)
                markerFeatures.remove(type)
                nonClusteredTypes.remove(type)
            } else {
                excludedFeatures.forEach { feature ->
                    feature.getStringProperty(FEATURE_TYPE)?.let { featureType ->
                        removeMarkerTooltipView(type, featureType)
                    }
                }
                removeMarkerFeatures(type, excludedFeatures)
                markerFeatures[type] = FeatureCollection.fromFeatures(includedFeatures)
            }

        }
    }

    @UiThread
    fun removeMarkers(type: String, featureType: String) {
        val markerFeatureCollection = markerFeatures[type] ?: return
        val includedFeatures = ArrayList<Feature>()
        markerFeatureCollection.features()!!.forEach {
            // handles null and excluded property
            if (it.getStringProperty(FEATURE_TYPE) == featureType) {
                includedFeatures.add(it)
            }
        }
        if (includedFeatures.size == markerFeatureCollection.features()!!.size) {
            removeMarkerTooltipView(type)
            removeSourceAndLayer(type)
            markerFeatures.remove(type)
        } else {
            removeMarkerTooltipView(type, featureType)
            removeMarkerFeatures(type, includedFeatures)
        }
    }

    override fun removeSourceAndLayer(type: String) {
        val sourceId = type + SOURCE_POSTFIX
        val style = mapViewRef.get()?.getMapboxMap()?.getStyle()?.takeIf {
            it.getSourceAs<GeoJsonSource>(sourceId) != null
        }?.let {
            it.removeStyleLayer(type + LAYER_POSTFIX)
            it.removeStyleLayer("$type$LAYER_POSTFIX$CLUSTERED")
            it.removeStyleLayer("$type$LAYER_POSTFIX$FILTERED_CLUSTER")
            it.removeStyleLayer(type + PRICE_BAR + LAYER_POSTFIX)
            it.removeStyleLayer(type + SEASON_BAR + LAYER_POSTFIX)
            it.removeStyleSource(sourceId)
            it.removeStyleSource("$sourceId$CLUSTERED")
            sourceIds.remove(sourceId)
            sourceIds.remove("$sourceId$CLUSTERED")
            layerIds.remove(type + LAYER_POSTFIX)
            clusterLayerIds.remove("$type$LAYER_POSTFIX$CLUSTERED")
        }
    }


    override fun handleCarParkReset(it: MarkerViewType) {
        if (it.type != CARPARK && it.feature.getStringProperty(FEATURE_TYPE) != CARPARK) {
            return
        }
        if (it.feature.getStringProperty(CARPARK_PRICE) != null) {
            it.feature.addBooleanProperty(CARPARK_PRICE_SHOW, true)
        }
    }

    @UiThread
    override fun removeMarkers(type: String) {
        super.removeMarkers(type)
        nonClusteredTypes.remove(type)
    }

    @UiThread
    override fun removeAllMarkers() {
        super.removeAllMarkers()
        clusterLayerIds.clear()
        nonClusteredTypes.clear()
    }


    @UiThread
    override fun onDestroy() {
        super.onDestroy()
        mapViewRef.get()?.getMapboxMap()?.removeOnMapClickListener(mapCircleLayerClickListener)
        mapViewRef.get()?.getMapboxMap()?.removeOnScaleListener(onCameraZoomChangedListener)
    }

    /**
     * this fun to remove all tooltip
     */
    private fun removeMarkerTooltipView(type: String, featureType: String) {
        marker?.let {
            if (it.type == type && it.feature.getStringProperty(FEATURE_TYPE) == featureType) {
                mapViewRef.get()?.removeView(it.markerView2.mViewMarker)
                handleCarParkReset(it)
                deselectImage(it)
                addOrReplaceMarker(it.type, it.feature)
                marker = null
            }
        }
    }

    companion object {
        const val FEATURE_TYPE = "featureType"
        private const val POINT_COUNT = "point_count"
        private const val CLUSTERED = "-clustered"
        private const val FILTERED_CLUSTER = "-cluster-0"
        private const val CLUSTER_RADIUS = 50L

        const val DROP_PIN_TYPE = "drop-pin"
    }

    interface ClusterLayerClickHandler {
        fun handleOnclick(point: Point, zoomLevel: Number)
    }

}

package com.ncs.breeze.components

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.StyleRes
import androidx.appcompat.view.ContextThemeWrapper
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.widget.TextViewCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.mapbox.navigation.ui.maneuver.databinding.MapboxItemUpcomingManeuversLayoutBinding
import com.mapbox.navigation.ui.maneuver.databinding.MapboxMainManeuverLayoutBinding
import com.mapbox.navigation.ui.maneuver.model.*
import com.mapbox.navigation.ui.maneuver.view.*
import com.mapbox.navigation.ui.shield.model.RouteShield
import com.mapbox.navigation.utils.internal.ifNonNull
import com.ncs.breeze.common.extensions.mapbox.extractNonTamilAndChineseName


class BreezeNavigationManeuverAdapter(
    private val context: Context
) : RecyclerView.Adapter<BreezeNavigationManeuverAdapter.MapboxUpcomingManeuverViewHolder>() {

    @StyleRes
    private var stepDistanceAppearance: Int? = null

    @StyleRes
    private var styleManeuverIcon: Int? = null

    @StyleRes
    private var primaryManeuverAppearance: Int? = null

    @StyleRes
    private var secondaryManeuverAppearance: Int? = null

    @StyleRes
    private var exitManeuverAppearance: Int? = null
    private val inflater = LayoutInflater.from(context)
    private val upcomingManeuverList = mutableListOf<Maneuver>()
    private val routeShields = mutableSetOf<RouteShield>()
    var onItemClickListener : (maneuver: Maneuver, position: Int, rootView: View) -> Unit ={_,_,_->}

    /**
     * Binds the given View to the position.
     * @param parent ViewGroup
     * @param viewType Int
     * @return MapboxLaneGuidanceViewHolder
     */
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MapboxUpcomingManeuverViewHolder {
        val binding = MapboxItemUpcomingManeuversLayoutBinding.inflate(inflater, parent, false)
        val mainLayoutBinding = MapboxMainManeuverLayoutBinding.bind(binding.root)
        return MapboxUpcomingManeuverViewHolder(mainLayoutBinding)
    }

    /**
     * Returns the total number of items in the data set held by the adapter.
     * @return Int
     */
    override fun getItemCount(): Int {
        return upcomingManeuverList.size
    }

    /**
     * Invoked by RecyclerView to display the data at the specified position.
     * @param holder MapboxLaneGuidanceViewHolder
     * @param position Int
     */
    override fun onBindViewHolder(holder: MapboxUpcomingManeuverViewHolder, position: Int) {
        val maneuver = upcomingManeuverList[position]
        holder.viewBinding.root.setOnClickListener {
            onItemClickListener.invoke(maneuver, position, it)
        }
        holder.bindUpcomingManeuver(maneuver)
    }

    /**
     * Given a [Map] of id to [RoadShield], the function maintains a map of id to [RoadShield] that
     * is used to render the shield on the view
     * @param shieldMap Map<String, RoadShield?>
     */
    @Deprecated(
        message = "The method may or may not render multiple shields for a given instruction",
        replaceWith = ReplaceWith("updateShields(shields)")
    )
    fun updateRoadShields(shieldMap: Map<String, RoadShield?>) {
        shieldMap.forEach { entry ->
            ifNonNull(entry.value) { value ->
                routeShields.add(value.toRouteShield())
            }
        }
    }

    /**
     * Given a set of [RoadShield], the function maintains a set of [RoadShield] that is used to
     * render the shield on the view
     * @param shields Set<RoadShield>
     */
    fun updateShields(shields: Set<RouteShield>) {
        routeShields.addAll(shields)
    }

    /**
     * Allows you to change the text appearance of all maneuver text in upcoming maneuver list.
     * @see [TextViewCompat.setTextAppearance]
     * @param style Int
     */
    fun updateUpcomingManeuverStyles(
        @StyleRes stylePrimaryManeuver: Int,
        @StyleRes styleSecondaryManeuver: Int,
        @StyleRes styleStepDistance: Int,
        @StyleRes styleExitManeuver: Int,
        @StyleRes styleManeuverIcon: Int,
    ) {
        this.primaryManeuverAppearance = stylePrimaryManeuver
        this.secondaryManeuverAppearance = styleSecondaryManeuver
        this.exitManeuverAppearance = styleExitManeuver
        this.stepDistanceAppearance = styleStepDistance
        this.styleManeuverIcon = styleManeuverIcon
        notifyDataSetChanged()
    }

    /**
     * Invoke to add all upcoming maneuvers to the recycler view.
     * @param upcomingManeuvers List<Maneuver>
     */
    fun addUpcomingManeuvers(upcomingManeuvers: List<Maneuver>) {
        val diffCallback = BreezeManeuverDiffCallback(upcomingManeuverList, upcomingManeuvers)
        val diffResult = DiffUtil.calculateDiff(diffCallback)
        upcomingManeuverList.clear()
        upcomingManeuverList.addAll(upcomingManeuvers)
        diffResult.dispatchUpdatesTo(this)
    }

    /**
     * View Holder defined for the [RecyclerView.Adapter]
     * @property viewBinding
     * @constructor
     */
    inner class MapboxUpcomingManeuverViewHolder(
        val viewBinding: MapboxMainManeuverLayoutBinding
    ) : RecyclerView.ViewHolder(viewBinding.root) {

        /**
         * Invoke the method to bind the maneuver to the view.
         * @param maneuver Maneuver
         */
        fun bindUpcomingManeuver(maneuver: Maneuver) {
            updateStepDistanceTextAppearance()
            updateUpcomingPrimaryManeuverTextAppearance()
            updateUpcomingSecondaryManeuverTextAppearance()
            updateStyleManeuverIcons()

            val primary = maneuver.primary
            val secondary = maneuver.secondary
            val stepDistance = maneuver.stepDistance
            drawPrimaryManeuver(primary, routeShields)
            drawSecondaryManeuver(secondary, routeShields)
            drawTotalStepDistance(stepDistance)

        }

        private fun updateUpcomingPrimaryManeuverTextAppearance() {
            ifNonNull(primaryManeuverAppearance) { appearance ->
                TextViewCompat.setTextAppearance(viewBinding.primaryManeuverText, appearance)
            }
            ifNonNull(exitManeuverAppearance) { appearance ->
                viewBinding.primaryManeuverText.updateOptions(
                    ManeuverPrimaryOptions.Builder()
                        .exitOptions(
                            ManeuverExitOptions.Builder()
                                .textAppearance(appearance)
                                .build()
                        )
                        .build()
                )
            }
        }

        private fun updateUpcomingSecondaryManeuverTextAppearance() {
            ifNonNull(secondaryManeuverAppearance) { appearance ->
                TextViewCompat.setTextAppearance(viewBinding.secondaryManeuverText, appearance)
            }
        }

        private fun updateStepDistanceTextAppearance() {
            ifNonNull(stepDistanceAppearance) { appearance ->
                TextViewCompat.setTextAppearance(viewBinding.stepDistance, appearance)
            }
        }

        private fun updateStyleManeuverIcons() {
            styleManeuverIcon?.let {
                viewBinding.maneuverIcon.updateTurnIconStyle(
                    ContextThemeWrapper(context, it)
                )
            }
        }

        private fun drawPrimaryManeuver(primary: PrimaryManeuver, routeShields: Set<RouteShield>?) {
            viewBinding.primaryManeuverText.renderManeuver(primary, routeShields)
            primary.extractNonTamilAndChineseName()
                ?.let { viewBinding.primaryManeuverText.text = it }
            viewBinding.maneuverIcon.renderPrimaryTurnIcon(primary)
        }

        private fun drawTotalStepDistance(stepDistance: StepDistance) {
            viewBinding.stepDistance.renderTotalStepDistance(stepDistance)
        }

        private fun drawSecondaryManeuver(
            secondary: SecondaryManeuver?,
            routeShields: Set<RouteShield>?
        ) {
            if (secondary != null) {
                viewBinding.secondaryManeuverText.visibility = View.VISIBLE
                viewBinding.secondaryManeuverText.renderManeuver(secondary, routeShields)
                secondary.extractNonTamilAndChineseName()
                    ?.let { viewBinding.secondaryManeuverText.text = it }
                updateConstraintsToHaveSecondary()
                viewBinding.primaryManeuverText.maxLines = 1
            } else {
                updateConstraintsToOnlyPrimary()
                viewBinding.secondaryManeuverText.visibility = View.GONE
                viewBinding.primaryManeuverText.maxLines = 2
            }
        }

        private fun updateConstraintsToOnlyPrimary() {
            val params = viewBinding.primaryManeuverText.layoutParams
                    as ConstraintLayout.LayoutParams
            params.topToTop = ConstraintLayout.LayoutParams.PARENT_ID
            params.bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID
            viewBinding.root.requestLayout()
        }

        private fun updateConstraintsToHaveSecondary() {
            val params = viewBinding.primaryManeuverText.layoutParams
                    as ConstraintLayout.LayoutParams
            params.topToTop = ConstraintLayout.LayoutParams.UNSET
            params.bottomToBottom = ConstraintLayout.LayoutParams.UNSET
            params.bottomToTop = viewBinding.secondaryManeuverText.id
            viewBinding.root.requestLayout()
        }
    }

}

internal class BreezeManeuverDiffCallback(
    private val oldList: List<Maneuver>,
    private val newList: List<Maneuver>
) : DiffUtil.Callback() {

    /**
     * Returns the size of old list.
     * @return Int
     */
    override fun getOldListSize(): Int = oldList.size

    /**
     * Returns the size of new list.
     * @return Int
     */
    override fun getNewListSize(): Int = newList.size

    /**
     * Called by the DiffUtil to decide whether two object represent the same Item.
     * @param oldItemPosition Int
     * @param newItemPosition Int
     * @return Boolean
     */
    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].primary.text === newList[newItemPosition].primary.text
    }

    /**
     * Called by the DiffUtil when it wants to check whether two items have the same data.
     * @param oldItemPosition Int
     * @param newItemPosition Int
     * @return Boolean
     */
    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition] == newList[newItemPosition]
    }
}
typealias OnManeuverItemClick = (maneuver:Maneuver, position: Int, view: View) -> Unit


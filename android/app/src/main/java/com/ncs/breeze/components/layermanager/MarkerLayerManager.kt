package com.ncs.breeze.components.layermanager

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import androidx.annotation.UiThread
import com.breeze.model.api.response.AmenitiesPreference
import com.breeze.model.api.response.ContentDetails
import com.breeze.model.api.response.ImageTypes
import com.breeze.model.api.response.SubItems
import com.breeze.model.constants.AmenityType.CARPARK
import com.breeze.model.constants.AmenityType.POI
import com.breeze.model.constants.Constants
import com.breeze.model.extensions.toBitmap
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.mapbox.geojson.Feature
import com.mapbox.geojson.FeatureCollection
import com.mapbox.geojson.Point
import com.mapbox.maps.*
import com.mapbox.maps.extension.style.expressions.dsl.generated.eq
import com.mapbox.maps.extension.style.expressions.dsl.generated.get
import com.mapbox.maps.extension.style.expressions.dsl.generated.literal
import com.mapbox.maps.extension.style.expressions.dsl.generated.switchCase
import com.mapbox.maps.extension.style.expressions.generated.Expression
import com.mapbox.maps.extension.style.layers.addLayer
import com.mapbox.maps.extension.style.layers.addLayerAbove
import com.mapbox.maps.extension.style.layers.generated.SymbolLayerDsl
import com.mapbox.maps.extension.style.layers.generated.symbolLayer
import com.mapbox.maps.extension.style.layers.properties.generated.IconAnchor
import com.mapbox.maps.extension.style.layers.properties.generated.IconTextFit
import com.mapbox.maps.extension.style.sources.addSource
import com.mapbox.maps.extension.style.sources.generated.GeoJsonSource
import com.mapbox.maps.extension.style.sources.generated.geoJsonSource
import com.mapbox.maps.extension.style.sources.getSourceAs
import com.mapbox.maps.plugin.delegates.listeners.OnCameraChangeListener
import com.mapbox.maps.plugin.gestures.OnMapClickListener
import com.mapbox.maps.plugin.gestures.addOnMapClickListener
import com.mapbox.maps.plugin.gestures.removeOnMapClickListener
import com.ncs.breeze.R
import com.ncs.breeze.common.utils.BreezeMapDataHolder
import com.ncs.breeze.common.utils.ParkingIconUtils.ImageIconType
import com.ncs.breeze.common.utils.ParkingIconUtils.ParkingMarkerConstants
import com.ncs.breeze.common.utils.ParkingIconUtils.ParkingMarkerImageIconType
import com.ncs.breeze.components.marker.markerview2.MarkerView2
import kotlinx.coroutines.*
import java.lang.ref.WeakReference
import java.util.concurrent.ConcurrentHashMap
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine


open class MarkerLayerManager(
    mapView: MapView,
    private val nightMode: Boolean = false
) {
    var markerLayerClickHandler: MarkerLayerClickHandler? = null
    protected val mapViewRef = WeakReference(mapView)

    protected var marker: MarkerViewType? = null

    val markerFeatures: ConcurrentHashMap<String, FeatureCollection> = ConcurrentHashMap()
    protected val layerIds: HashMap<String, String> = HashMap()
    /**
     * Save layer ids from other Layer Manager to detect user clicking map
     * */
    val externalLayerIds: HashMap<String, String> = hashMapOf()

    protected val sourceIds: HashMap<String, String> = HashMap()

    /**
     * Save external source ids from other Layer Manager to detect user clicking map
     * */
    val externalSourceIds: HashMap<String, String> = hashMapOf()

    protected var destroyed = false

    private val mapClickListener =
        OnMapClickListener { point ->
            val clicked = mapView.getMapboxMap().pixelForCoordinate(point)
            mapView.getMapboxMap().queryRenderedFeatures(
                RenderedQueryGeometry(
                    ScreenBox(
                        ScreenCoordinate(clicked.x - 15, clicked.y - 15),
                        ScreenCoordinate(clicked.x + 15, clicked.y + 15)
                    )
                ),
                RenderedQueryOptions(
                    layerIds.keys.toMutableList().apply {
                        addAll(externalLayerIds.keys.toList())
                    },
                    literal(true)
                )
            ) { expected ->
                if ((expected.value?.size ?: 0) > 0 ) {
                    (sourceIds[expected.value!![0].source] ?: externalSourceIds[expected.value!![0].source])
                        ?.let { sourceId ->
                        markerLayerClickHandler?.handleOnclick(
                            sourceId,
                            expected.value!![0].feature,
                            marker
                        )
                        return@queryRenderedFeatures
                    }
                }
                if(shouldClearMarkersOnMapFreeClick())
                    removeMarkerTooltipView()
                markerLayerClickHandler?.handleOnFreeMapClick(point)
            }
            return@OnMapClickListener false
        }

    private val onCameraChangedListener = OnCameraChangeListener {
        marker?.markerView2?.updateTooltipPosition()
    }

    init {
        mapView.getMapboxMap().addOnCameraChangeListener(onCameraChangedListener)
        mapView.getMapboxMap().addOnMapClickListener(mapClickListener)
    }

    protected open fun shouldClearMarkersOnMapFreeClick() = true

    @UiThread
    fun renderMarkerToolTipView(marker: MarkerViewType) {
        removeMarkerTooltipView()
        this.marker = marker
        mapViewRef.get()?.addView(marker.markerView2.mViewMarker)
        marker.markerView2.updateTooltipPosition()
        addOrReplaceMarker(marker.type, marker.feature)
    }


    /**
     * this fun to remove all tooltip
     */
    @UiThread
    fun removeMarkerTooltipView() {
        marker?.let {
            mapViewRef.get()?.removeView(it.markerView2.mViewMarker)
            handleCarParkReset(it)
            deselectImage(it)
            addOrReplaceMarker(it.type, it.feature)
            marker = null
        }
    }

    /**
     * this fun to remove all tooltip
     */
    @UiThread
    open fun removeMarkerTooltipView(type: String) {
        marker?.let {
            if (it.type == type) {
                mapViewRef.get()?.removeView(it.markerView2.mViewMarker)
                handleCarParkReset(it)
                deselectImage(it)
                addOrReplaceMarker(it.type, it.feature)
                marker = null
            }
        }
    }


    /**
     * this fun to hide all tooltip
     */
    @UiThread
    fun hideMarkerTooltipView(type: String) {
        marker?.let {
            if (it.type == type) {
                it.markerView2.hideMarker()
            }
        }
    }

    protected open fun deselectImage(it: MarkerViewType) {
        val imageType = it.feature.getStringProperty(ICON_KEY)
        if (imageType != null) {
            if (imageType.contains(ParkingMarkerConstants.PARKINGMARKER) && imageType.contains("_${ParkingMarkerConstants.MARKER_SELECTED}")) {
                val newKey = imageType.replace(
                    "_${ParkingMarkerConstants.MARKER_SELECTED}",
                    "_${ParkingMarkerConstants.MARKER_NOTSELECTED}"
                )
                it.feature.addStringProperty(ICON_KEY, newKey)
            } else {
                when (imageType) {
                    ImageIconType.SELECTED.type -> {
                        it.feature.addStringProperty(ICON_KEY, ImageIconType.UNSELECTED.type)
                    }

                    ImageIconType.SELECTED_NIGHT.type -> {
                        it.feature.addStringProperty(ICON_KEY, ImageIconType.UNSELECTED_NIGHT.type)
                    }
                }
            }
        }
    }


    internal open fun handleCarParkReset(it: MarkerViewType) {
        if (it.type != CARPARK) {
            return
        }
        if (it.feature.getStringProperty(CARPARK_PRICE) != null) {
            it.feature.addBooleanProperty(CARPARK_PRICE_SHOW, true)
        }
    }

    /**
     * Destroys the MarkerViewManager.
     *
     */
    @UiThread
    open fun onDestroy() {
        removeAllMarkers()
        layerIds.clear()
        sourceIds.clear()
        destroyed = true
        mapViewRef.get()?.getMapboxMap()?.run {
            removeOnMapClickListener(mapClickListener)
            removeOnCameraChangeListener(onCameraChangedListener)
        }
    }

    //performing a shallow clone of features to prevent concurrency issues
    @UiThread
    fun addOrReplaceMarker(type: String, markerFeature: Feature) {
        var featureCollection = markerFeatures[type]
        if (featureCollection == null) {
            featureCollection = FeatureCollection.fromFeature(markerFeature)
        } else {
            featureCollection =
                FeatureCollection.fromFeatures(ArrayList(featureCollection.features()!!))
            featureCollection.features()!!.removeIf {
                return@removeIf (it.id() == markerFeature.id())
            }
            featureCollection.features()!!.add(markerFeature)
        }
        val sourceId = "$type$SOURCE_POSTFIX"
        mapViewRef.get()?.getMapboxMap()?.getStyle { mapStyle ->
            val source = mapStyle.getSourceAs<GeoJsonSource>(sourceId)
            if (source == null) {
                val iconSizeExp = addStyleIconSizeExpression()
                addStyleSource(type, sourceId, featureCollection!!)
                addStyleLayerAndReArrange(type, sourceId, iconSizeExp)
            } else {
                updateSourceCollection(mapStyle, sourceId, featureCollection)
            }
            markerFeatures[type] = featureCollection!!
        }
    }

    //performing a shallow clone of features to prevent concurrency issues
    @UiThread
    fun addOrReplaceMarkers(type: String, feature: List<Feature>) {
        val featureCollection = FeatureCollection.fromFeatures(ArrayList(feature))
        val sourceId = type + SOURCE_POSTFIX
        val mapStyle = mapViewRef.get()?.getMapboxMap()?.getStyle()
        val source = mapStyle?.getSourceAs<GeoJsonSource>(sourceId)
        if (source == null) {
            val iconSizeExp = addStyleIconSizeExpression()
            addStyleSource(type, sourceId, featureCollection)
            addStyleLayerAndReArrange(type, sourceId, iconSizeExp)
        } else {
            updateSourceCollection(mapStyle, sourceId, featureCollection)
        }
        markerFeatures[type] = featureCollection
    }

    protected open fun updateSourceCollection(
        style: Style,
        sourceId: String,
        featureCollection: FeatureCollection
    ) {
        style.getSourceAs<GeoJsonSource>(sourceId)?.featureCollection(featureCollection)
    }

    /**
     * add style source
     */
    protected open fun addStyleSource(
        type: String,
        sourceId: String,
        featureCollection: FeatureCollection
    ) {
        mapViewRef.get()?.getMapboxMap()?.getStyle()?.addSource(
            geoJsonSource(sourceId) {
                featureCollection(featureCollection)
            }
        )
        sourceIds[sourceId] = type
    }


    /**
     * add style layer
     */
    protected fun addStyleLayerAndReArrange(
        type: String,
        sourceId: String,
        iconSizeExp: Expression
    ) {
        addStyleLayer(type, sourceId, iconSizeExp)
        layerIds[type + LAYER_POSTFIX] = type
    }

    protected open fun addStyleLayer(
        type: String,
        sourceId: String,
        iconSizeExp: Expression
    ) {
        if (type == CARPARK) {
            addBaseAmenityLayer(type, sourceId, iconSizeExp)
            addCarParkToolTipLayer(type, sourceId)
        } else {
            addBaseAmenityLayer(type, sourceId, iconSizeExp)
        }
    }


    @SuppressLint("ResourceType")
    protected open fun addCarParkToolTipLayer(
        type: String,
        sourceId: String
    ) {
        val iconSizeExp = Expression.interpolate {
            linear()
            zoom()
            stop {
                literal(0.0)
                literal(0.0)
            }
            stop {
                literal(9.0)
                literal(0.0)
            }
            stop {
                literal(14.9)
                literal(0.0)
            }
            stop {
                literal(15.0)
                literal(1.0)
            }
            stop {
                literal(16.5)
                literal(1.0)
            }
        }

        val textSizeExp = Expression.interpolate {
            linear()
            zoom()
            stop {
                literal(0.0)
                literal(0.0)
            }
            stop {
                literal(9.0)
                literal(0.0)
            }
            stop {
                literal(14.9)
                literal(0.0)
            }
            stop {
                literal(15.0)
                literal(14.0)
            }
            stop {
                literal(16.5)
                literal(14.0)
            }
        }
        if (mapViewRef.get()?.getMapboxMap()?.getStyle()?.styleLayers.isNullOrEmpty()) {
            mapViewRef.get()?.getMapboxMap()?.getStyle()?.addLayer(
                configCarParkToolTipSymbolLayer(type, sourceId, textSizeExp, iconSizeExp)
            )
        } else {
            mapViewRef.get()?.getMapboxMap()?.getStyle()?.addLayerAbove(
                configCarParkToolTipSymbolLayer(type, sourceId, textSizeExp, iconSizeExp),
                mapViewRef.get()?.getMapboxMap()?.getStyle()?.styleLayers?.last()?.id
            )
        }
    }

    private fun configCarParkToolTipSymbolLayer(
        type: String,
        sourceId: String,
        textSizeExp: Expression,
        iconSizeExp: Expression
    ) = symbolLayer(type + PRICE_BAR + LAYER_POSTFIX, sourceId) {
        textField(Expression.get(CARPARK_PRICE))
        textOffset(listOf(0.0, -2.5))
        textColor(
            switchCase {
                eq {
                    get {
                        literal(CHEAPEST_KEY)
                    }
                    literal(true)
                }
                literal(WHITE_COLOR_STR)
                if (nightMode) {
                    literal(WHITE_COLOR_STR)
                } else {
                    literal(BLACK_COLOR_STR)
                }
            }
        )
        iconImage(
            switchCase {
                eq {
                    get {
                        literal(CHEAPEST_KEY)
                    }
                    literal(true)
                }
                literal(Constants.CARPARK_PRICE_GREEN_IMAGE)
                literal(Constants.CARPARK_PRICE_IMAGE)
            }
        )
        textFont(listOf(Constants.MAPBOXSTUDIOFONTFAMILY.MEDIUM))
        textSize(textSizeExp)
        iconSize(iconSizeExp)
        textIgnorePlacement(true)
        textAllowOverlap(true)
        iconAllowOverlap(true)
        iconIgnorePlacement(true)
        iconAnchor(IconAnchor.CENTER)
        iconTextFit(IconTextFit.BOTH)
        iconOffset(listOf(0.0, -2.0))
        iconTextFitPadding(listOf(1.0, 6.0, 4.5, 5.0))
        symbolSortKey(get(DISTANCE))
        filter(
            toolTipCarParkExpression()
        )
    }

    protected open fun toolTipCarParkExpression() = Expression.all {
        eq {
            get {
                literal(HIDDEN)
            }
            literal(false)
        }
        eq {
            get {
                literal(CARPARK_PRICE_SHOW)
            }
            literal(true)
        }
    }

    protected open fun addBaseAmenityLayer(
        type: String,
        sourceId: String,
        icon_size_exp: Expression
    ) {
        if (mapViewRef.get()?.getMapboxMap()?.getStyle()?.styleLayers.isNullOrEmpty()) {
            mapViewRef.get()?.getMapboxMap()?.getStyle()?.addLayer(
                configBaseSymbolLayer(type, sourceId, icon_size_exp)
            )
        } else {
            mapViewRef.get()?.getMapboxMap()?.getStyle()?.addLayerAbove(
                configBaseSymbolLayer(type, sourceId, icon_size_exp),
                mapViewRef.get()?.getMapboxMap()?.getStyle()?.styleLayers?.last()?.id
            )
        }
    }

    private fun configBaseSymbolLayer(
        type: String,
        sourceId: String,
        icon_size_exp: Expression
    ) = symbolLayer(type + LAYER_POSTFIX, sourceId) {
        addIconImageExpression(this, type)
        iconSize(icon_size_exp)
        iconAnchor(IconAnchor.CENTER)
        iconAllowOverlap(true)
        iconIgnorePlacement(true)
        textAllowOverlap(true)
        textIgnorePlacement(true)
        filter(eq {
            get {
                literal(HIDDEN)
            }
            literal(false)
        })
    }

    /**
     * hide all marker
     * not performing a shallow clone of features since its not required for property updates
     */
    @UiThread
    open fun hideAllMarker() {
        marker?.markerView2?.hideMarker()
        markerFeatures.forEach { map ->
            hideAllMarker(map.key)
        }
    }


    /**
     * hide all marker excluding type
     */
    @UiThread
    open fun hideAllMarkerExcludingType(type: String) {
        marker?.let {
            if (it.type != type) {
                it.markerView2.hideMarker()
            }
        }
        markerFeatures.forEach { map ->
            if (map.key == type) {
                return@forEach
            }
            hideAllMarker(map.key)
        }
    }

    /**
     * hide all marker
     * not performing a shallow clone of features since its not required for property updates
     */
    @UiThread
    open fun hideAllMarker(type: String) {
        val featureCollection = markerFeatures[type] ?: return
        hideMarkerTooltipView(type)
        featureCollection.features()!!.forEach {
            it.addBooleanProperty(HIDDEN, true)
        }
        updateSourceGeoJson(type, featureCollection)
    }

    /**
     * show all markers
     */
    @UiThread
    open fun showAllMarker() {
        markerFeatures.forEach { map ->
            showAllMarker(map.key)
        }
    }


    /**
     * show all markers
     * not performing a shallow clone of features since its not required for property updates
     */
    @UiThread
    open fun showAllMarker(type: String) {
        val featureCollection = markerFeatures[type] ?: return
        featureCollection.features()!!.forEach {
            it.addBooleanProperty(HIDDEN, false)
        }
        updateSourceGeoJson(type, featureCollection)
    }

    /**
     * hide marker
     * not performing a shallow clone of features since its not required for property updates
     */
    @UiThread
    open fun hideMarker(type: String, id: String) {
        marker?.let {
            if (it.markerView2.itemId == id) {
                it.markerView2.hideMarker()
            }
        }
        val featureCollection = markerFeatures[type] ?: return
        for (feature in featureCollection.features()!!) {
            if (feature.id() == id) {
                feature.addBooleanProperty(HIDDEN, true)
                break
            }
        }
        updateSourceGeoJson(type, featureCollection)
    }

    /**
     * show marker
     * not performing a shallow clone of features since its not required for property updates
     */
    @UiThread
    open fun showMarker(type: String, id: String) {
        val featureCollection = markerFeatures[type] ?: return
        for (feature in featureCollection.features()!!) {
            if (feature.id() == id) {
                feature.addBooleanProperty(HIDDEN, false)
                break
            }
        }
        updateSourceGeoJson(type, featureCollection)
    }

    /**
     * show markers
     * not performing a shallow clone of features since its not required for property updates
     */
    @UiThread
    open fun showMarkers(type: String, ids: List<String>) {
        val featureCollection = markerFeatures[type] ?: return
        for (feature in featureCollection.features()!!) {
            for (id in ids) {
                if (feature.id() == id) {
                    feature.addBooleanProperty(HIDDEN, false)
                    break
                }
            }
        }
        updateSourceGeoJson(type, featureCollection)
    }


    /**
     * Change icon load expression
     */
    protected open fun addIconImageExpression(
        symbolLayerDsl: SymbolLayerDsl,
        type: String
    ) {
        symbolLayerDsl.iconImage(
            switchCase {
                for (parkingType in ParkingMarkerImageIconType.values()) {
                    eq {
                        get {
                            literal(ICON_KEY)
                        }
                        literal(parkingType.type)
                    }
                    literal("$type-${parkingType.type}$ICON_POSTFIX")
                }
                //all unselected night
                eq {
                    get {
                        literal(ICON_KEY)
                    }
                    literal(ImageIconType.UNSELECTED_NIGHT.type)
                }
                concat { literal("$type"); get { literal(IMAGE_TYPE) }; literal("-${ImageIconType.UNSELECTED_NIGHT.type}$ICON_POSTFIX") }
                //all selected night
                eq {
                    get {
                        literal(ICON_KEY)
                    }
                    literal(ImageIconType.SELECTED_NIGHT.type)
                }
                concat { literal("$type"); get { literal(IMAGE_TYPE) }; literal("-${ImageIconType.SELECTED_NIGHT.type}$ICON_POSTFIX") }
                //all unselected
                eq {
                    get {
                        literal(ICON_KEY)
                    }
                    literal(ImageIconType.UNSELECTED.type)
                }
                concat { literal("$type"); get { literal(IMAGE_TYPE) }; literal("-${ImageIconType.UNSELECTED.type}$ICON_POSTFIX") }

                //default
                concat { literal("$type"); get { literal(IMAGE_TYPE) }; literal("-${ImageIconType.SELECTED.type}$ICON_POSTFIX") }
            }
        )
    }

    /**
     * Change icon size expression
     */
    protected open fun addStyleIconSizeExpression(): Expression = Expression.interpolate {
        linear()
        zoom()
        stop {
            literal(0.0)
            literal(0.8)
        }
        stop {
            literal(9.0)
            literal(1.0)
        }
        stop {
            literal(12.9)
            literal(1.0)
        }
        stop {
            literal(13.0)
            literal(1.0)
        }
        stop {
            literal(16.5)
            literal(1.0)
        }
    }


    // performing a shallow clone of features to prevent concurrency issues
    @UiThread
    open fun removeMarkers(type: String) {
        removeMarkerTooltipView(type)
        markerFeatures[type] ?: return
        removeSourceAndLayer(type)
        markerFeatures.remove(type)
    }

    protected open fun removeSourceAndLayer(type: String) {
        val sourceId = type + SOURCE_POSTFIX
        mapViewRef.get()?.getMapboxMap()?.getStyle { style ->
            style.getSourceAs<GeoJsonSource>(sourceId)?.let {
                style.removeStyleLayer(type + LAYER_POSTFIX)
                if (type == CARPARK) {
                    style.removeStyleLayer(type + PRICE_BAR + LAYER_POSTFIX)
                    style.removeStyleLayer(type + SEASON_BAR + LAYER_POSTFIX)
                }
                style.removeStyleSource(sourceId)
                sourceIds.remove(sourceId)
                layerIds.remove(type + LAYER_POSTFIX)
            }
        }
    }

    // performing a shallow clone of features to prevent concurrency issues
    @UiThread
    open fun removeAllMarkers() {
        markerFeatures.forEach {
            val type = it.key
            removeMarkerTooltipView(type)
            removeSourceAndLayer(type)
        }
        markerFeatures.clear()
    }

    // performing a shallow clone of features to prevent concurrency issues
    @UiThread
    open fun removeAllMarkersExcludingType(exclude: String) {
        val keys = markerFeatures.keys()
        for (type in keys) {
            if (type == exclude) {
                continue
            }
            removeMarkerTooltipView(type)
            removeSourceAndLayer(type)
            markerFeatures.remove(type)
        }
    }

    protected open fun updateSourceGeoJson(
        type: String,
        featureCollection: FeatureCollection
    ) {
        val sourceId = type + SOURCE_POSTFIX
        mapViewRef.get()?.getMapboxMap()?.getStyle()?.getSourceAs<GeoJsonSource>(sourceId)
            ?.featureCollection(featureCollection)

    }

    @UiThread
    open suspend fun addStyleImages(type: String) {

        if (type == CARPARK) {
            addParkingStyleImages(type)
        } else {
            addAmenityStyleImages(type)
            addAmenityPOIStyleImages()
        }
    }

    @UiThread
    open suspend fun addStyleImages(allTypes: ArrayList<String>) {
        val types = ArrayList(allTypes)
        if (types.contains(CARPARK)) {
            addParkingStyleImages(CARPARK)
        }
        types.remove(CARPARK)
        addAmenityStyleImages(types)
        addAmenityPOIStyleImages()
    }

    @UiThread
    open suspend fun addStyleImagesWithContentDetail(listContentDetails: ArrayList<ContentDetails>) {
        addParkingStyleImages(CARPARK)
        addImageStyles(listContentDetails)
    }

    private suspend fun addImageStyles(listContentDetails: ArrayList<ContentDetails>) {

        val results = ArrayList<Pair<String, Bitmap>?>()
        listContentDetails.forEach { content ->
            content?.let {
                val loadedImageList = loadImageListWithContentDetail(content)
                results.addAll(
                    awaitAll(
                        *loadedImageList.toTypedArray()
                    )
                )
            }
        }
        CoroutineScope(Dispatchers.Main).launch {
            results.forEach { pair ->
                pair?.let { it ->
                    mapViewRef.get()?.getMapboxMap()?.getStyle()?.addImage(
                        it.first,
                        it.second,
                        false
                    )
                }
            }
        }


    }

    private suspend fun loadImageListWithContentDetail(
        amenityPrefSubItem: ContentDetails
    ): ArrayList<Deferred<Pair<String, Bitmap>?>> {
        val loadedImageList = ArrayList<Deferred<Pair<String, Bitmap>?>>()
        amenityPrefSubItem.map_dark_selected_url?.let {
            loadedImageList.add(
                loadStyleImageAsync(
                    it,
                    "${amenityPrefSubItem.name}-${ImageIconType.SELECTED_NIGHT.type}$ICON_POSTFIX"
                )
            )
        }
        amenityPrefSubItem.map_dark_unselected_url?.let {
            loadedImageList.add(
                loadStyleImageAsync(
                    it,
                    "${amenityPrefSubItem.name}-${ImageIconType.UNSELECTED_NIGHT.type}$ICON_POSTFIX"
                )
            )
        }
        amenityPrefSubItem.map_light_selected_url?.let {
            loadedImageList.add(
                loadStyleImageAsync(
                    it,
                    "${amenityPrefSubItem.name}-${ImageIconType.SELECTED.type}$ICON_POSTFIX"
                )
            )
        }
        amenityPrefSubItem.map_light_unselected_url?.let {
            loadedImageList.add(
                loadStyleImageAsync(
                    it,
                    "${amenityPrefSubItem.name}-${ImageIconType.UNSELECTED.type}$ICON_POSTFIX"
                )
            )
        }
        return loadedImageList
    }


    fun hasFeatureLayerItems(type: String): Boolean {
        return markerFeatures[type] != null
    }

    fun hasFeatureLayerItemsExceptType(exclusionType: String): Boolean {
        val baseCount = if (markerFeatures[exclusionType] == null) 0 else 1
        return (markerFeatures.size > baseCount)
    }

    private suspend fun addAmenityStyleImages(types: List<String>) {
        var results = ArrayList<Pair<String, Bitmap>?>()
        types.forEach { type ->
            BreezeMapDataHolder.amenitiesPreferenceHashmap[type]?.let { amenitiesPreference ->
                val loadedImageList = loadImageList(amenitiesPreference, type)
                results.addAll(
                    awaitAll(
                        *loadedImageList.toTypedArray()
                    )
                )
            }
        }

        CoroutineScope(Dispatchers.Main).launch {
            results.forEach { pair ->
                pair?.let { it ->
                    mapViewRef.get()?.getMapboxMap()?.getStyle()?.addImage(
                        it.first,
                        it.second,
                        false
                    )
                }
            }
        }


    }

    private suspend fun addAmenityStyleImages(type: String) {

        BreezeMapDataHolder.amenitiesPreferenceHashmap[type]?.let { amenitiesPreference ->
            val loadedImageList = loadImageList(amenitiesPreference, type)
            val result = awaitAll(
                *loadedImageList.toTypedArray()
            )
            CoroutineScope(Dispatchers.Main).launch {
                result.forEach { pair ->
                    pair?.let { it ->
                        mapViewRef.get()?.getMapboxMap()?.getStyle()?.addImage(
                            it.first,
                            it.second,
                            false
                        )
                    }
                }
            }
        }
    }


    private suspend fun addAmenityPOIStyleImages() {
        var results = ArrayList<Pair<String, Bitmap>?>()
        BreezeMapDataHolder.amenitiesPreferenceHashmap[POI]?.let { amenitiesPreference ->
            amenitiesPreference.subItems?.forEach { amenityPrefSubItem ->
                amenityPrefSubItem.elementName?.let {
                    val loadedImageList = loadImageList(amenityPrefSubItem, it)
                    results.addAll(
                        awaitAll(
                            *loadedImageList.toTypedArray()
                        )
                    )
                }
            }
        }


        CoroutineScope(Dispatchers.Main).launch {
            results.forEach { pair ->
                pair?.let { it ->
                    mapViewRef.get()?.getMapboxMap()?.getStyle()?.addImage(
                        it.first,
                        it.second,
                        false
                    )
                }
            }
        }


    }

    private suspend fun loadImageList(
        amenityPrefSubItem: SubItems,
        type: String
    ): ArrayList<Deferred<Pair<String, Bitmap>?>> {
        val loadedImageList = ArrayList<Deferred<Pair<String, Bitmap>?>>()
        amenityPrefSubItem.map_icon_dark_selected_url?.let {
            loadedImageList.add(
                loadStyleImageAsync(
                    it,
                    "$type-${ImageIconType.SELECTED_NIGHT.type}$ICON_POSTFIX"
                )
            )
        }
        amenityPrefSubItem.map_icon_dark_unselected_url?.let {
            loadedImageList.add(
                loadStyleImageAsync(
                    it,
                    "$type-${ImageIconType.UNSELECTED_NIGHT.type}$ICON_POSTFIX"
                )
            )
        }
        amenityPrefSubItem.map_icon_light_selected_url?.let {
            loadedImageList.add(
                loadStyleImageAsync(
                    it,
                    "$type-${ImageIconType.SELECTED.type}$ICON_POSTFIX"
                )
            )
        }
        amenityPrefSubItem.map_icon_light_unselected_url?.let {
            loadedImageList.add(
                loadStyleImageAsync(
                    it,
                    "$type-${ImageIconType.UNSELECTED.type}$ICON_POSTFIX"
                )
            )
        }
        return loadedImageList
    }


    private suspend fun loadImageList(
        amenitiesPreference: AmenitiesPreference,
        type: String
    ): ArrayList<Deferred<Pair<String, Bitmap>?>> {
        val loadedImageList = ArrayList<Deferred<Pair<String, Bitmap>?>>()
        if (amenitiesPreference.imageTypes != null) {
            amenitiesPreference.imageTypes!!.forEach { imageType ->
                loadImageList(imageType, loadedImageList, type)
            }
        }
        amenitiesPreference.map_icon_dark_selected_url?.let {
            loadedImageList.add(
                loadStyleImageAsync(
                    it,
                    "$type-${ImageIconType.SELECTED_NIGHT.type}$ICON_POSTFIX"
                )
            )
        }
        amenitiesPreference.map_icon_dark_unselected_url?.let {
            loadedImageList.add(
                loadStyleImageAsync(
                    it,
                    "$type-${ImageIconType.UNSELECTED_NIGHT.type}$ICON_POSTFIX"
                )
            )
        }
        amenitiesPreference.map_icon_light_selected_url?.let {
            loadedImageList.add(
                loadStyleImageAsync(
                    it,
                    "$type-${ImageIconType.SELECTED.type}$ICON_POSTFIX"
                )
            )
        }
        amenitiesPreference.map_icon_light_unselected_url?.let {
            loadedImageList.add(
                loadStyleImageAsync(
                    it,
                    "$type-${ImageIconType.UNSELECTED.type}$ICON_POSTFIX"
                )
            )
        }
        return loadedImageList
    }

    private suspend fun loadImageList(
        imageType: ImageTypes,
        loadedImageList: ArrayList<Deferred<Pair<String, Bitmap>?>>,
        type: String
    ) {
        imageType.map_icon_dark_selected_url?.let {
            loadedImageList.add(
                loadStyleImageAsync(
                    it,
                    "$type-${imageType.imageType}-${ImageIconType.SELECTED_NIGHT.type}$ICON_POSTFIX"
                )
            )
        }
        imageType.map_icon_dark_unselected_url?.let {
            loadedImageList.add(
                loadStyleImageAsync(
                    it,
                    "$type-${imageType.imageType}-${ImageIconType.UNSELECTED_NIGHT.type}$ICON_POSTFIX"
                )
            )
        }
        imageType.map_icon_light_selected_url?.let {
            loadedImageList.add(
                loadStyleImageAsync(
                    it,
                    "$type-${imageType.imageType}-${ImageIconType.SELECTED.type}$ICON_POSTFIX"
                )
            )
        }
        imageType.map_icon_light_unselected_url?.let {
            loadedImageList.add(
                loadStyleImageAsync(
                    it,
                    "$type-${imageType.imageType}-${ImageIconType.UNSELECTED.type}$ICON_POSTFIX"
                )
            )
        }
    }

    /**
     * From java doc of Target
     * The typical lifecycle is onLoadStarted -> onResourceReady or onLoadFailed -> onLoadCleared.
     * However, there are no guarantees. onLoadStarted may not be called if the resource is in memory or
     * if the load will fail because of a null model object. onLoadCleared similarly may never be called
     * if the target is never cleared. See the docs for the individual methods for details.
     */
    private suspend fun loadStyleImageAsync(
        url: String?,
        imageId: String
    ): Deferred<Pair<String, Bitmap>?> {
        return CoroutineScope(Dispatchers.IO).async {
            suspendCoroutine<Pair<String, Bitmap>?> { cont ->
                if (url.isNullOrBlank()) {
                    cont.resume(null);
                    return@suspendCoroutine
                }
                mapViewRef.get()?.context?.let { context ->
                    Glide.with(context).asBitmap().load(url)
                        .into(object : CustomTarget<Bitmap>() {
                            var isResumed = false
                            override fun onResourceReady(
                                resource: Bitmap,
                                transition: Transition<in Bitmap>?
                            ) {
                                if (isResumed) return
                                isResumed = true
                                cont.resume(Pair(imageId, resource));
                            }

                            override fun onLoadCleared(placeholder: Drawable?) {
                                if (isResumed) return
                                isResumed = true
                                cont.resume(null);
                            }
                        })
                }
            }
        }
    }

    private fun addParkingStyleImages(type: String) {
        CoroutineScope(Dispatchers.Main).launch {
            mapViewRef.get()?.getMapboxMap()?.getStyle { style ->
                generateStyleImagesMap(type).forEach { key, value ->
                    mapViewRef.get()?.context?.let { context -> value.toBitmap(context) }
                        ?.let { bitmap -> style.addImage(key, bitmap, false) }
                }
            }
        }
    }

    private fun generateStyleImagesMap(type: String): HashMap<String, Int> {
        return hashMapOf(
            Constants.CARPARK_PRICE_IMAGE to R.drawable.ic_bg_parking_price,
            Constants.CARPARK_PRICE_GREEN_IMAGE to R.drawable.ic_bg_parking_price_cheapest,
            Constants.CARPARK_SEASON_BAR_IMAGE to R.drawable.img_parking_underline,

            //Keeping this icon as fail safe case because this icon is in DEFAULT block
            "$type-${ImageIconType.SELECTED.type}$ICON_POSTFIX" to R.drawable.parking_selected,

            //Finalised Colored Parking icons
            "$type-${ParkingMarkerImageIconType.PARKINGMARKER_SEASON_DESTINATION_SELECTED.type}$ICON_POSTFIX" to R.drawable.ic_parkingmarker_season_destination_selected,
            "$type-${ParkingMarkerImageIconType.PARKINGMARKER_SEASON_DESTINATION_NOTSELECTED.type}$ICON_POSTFIX" to R.drawable.ic_parkingmarker_season_destination_notselected,
            "$type-${ParkingMarkerImageIconType.PARKINGMARKER_SEASON_NONDESTINATION_SELECTED.type}$ICON_POSTFIX" to R.drawable.ic_parkingmarker_season_nondestination_selected,
            "$type-${ParkingMarkerImageIconType.PARKINGMARKER_SEASON_NONDESTINATION_NOTSELECTED.type}$ICON_POSTFIX" to R.drawable.ic_parkingmarker_season_nondestination_notselected,
            "$type-${ParkingMarkerImageIconType.PARKINGMARKER_TYPE0_DESTINATION_SELECTED.type}$ICON_POSTFIX" to R.drawable.ic_parkingmarker_typezero_destination_selected,
            "$type-${ParkingMarkerImageIconType.PARKINGMARKER_TYPE0_DESTINATION_NOTSELECTED.type}$ICON_POSTFIX" to R.drawable.ic_parkingmarker_typezero_destination_notselected,
            "$type-${ParkingMarkerImageIconType.PARKINGMARKER_TYPE0_NONDESTINATION_SELECTED.type}$ICON_POSTFIX" to R.drawable.ic_parkingmarker_type0_nondestination_selected,
            "$type-${ParkingMarkerImageIconType.PARKINGMARKER_TYPE0_NONDESTINATION_NOTSELECTED.type}$ICON_POSTFIX" to R.drawable.ic_parkingmarker_type0_nondestination_notselected,
            "$type-${ParkingMarkerImageIconType.PARKINGMARKER_TYPE1_DESTINATION_SELECTED.type}$ICON_POSTFIX" to R.drawable.ic_parkingmarker_typeone_destination_selected,
            "$type-${ParkingMarkerImageIconType.PARKINGMARKER_TYPE1_DESTINATION_NOTSELECTED.type}$ICON_POSTFIX" to R.drawable.ic_parkingmarker_typeone_destination_notselected,
            "$type-${ParkingMarkerImageIconType.PARKINGMARKER_TYPE1_NONDESTINATION_SELECTED.type}$ICON_POSTFIX" to R.drawable.ic_parkingmarker_type1_nondestination_selected,
            "$type-${ParkingMarkerImageIconType.PARKINGMARKER_TYPE1_NONDESTINATION_NOTSELECTED.type}$ICON_POSTFIX" to R.drawable.ic_parkingmarker_type1_nondestination_notselected,
            "$type-${ParkingMarkerImageIconType.PARKINGMARKER_TYPE2_DESTINATION_SELECTED.type}$ICON_POSTFIX" to R.drawable.ic_parkingmarker_typetwo_destination_selected,
            "$type-${ParkingMarkerImageIconType.PARKINGMARKER_TYPE2_DESTINATION_NOTSELECTED.type}$ICON_POSTFIX" to R.drawable.ic_parkingmarker_typetwo_destination_notselected,
            "$type-${ParkingMarkerImageIconType.PARKINGMARKER_TYPE2_NONDESTINATION_SELECTED.type}$ICON_POSTFIX" to R.drawable.ic_parkingmarker_type2_nondestination_selected,
            "$type-${ParkingMarkerImageIconType.PARKINGMARKER_TYPE2_NONDESTINATION_NOTSELECTED.type}$ICON_POSTFIX" to R.drawable.ic_parkingmarker_type2_nondestination_notselected,
            "$type-${ParkingMarkerImageIconType.PARKINGMARKER_NORATE_DESTINATION_SELECTED.type}$ICON_POSTFIX" to R.drawable.ic_parkingmarker_norate_destination_selected,
            "$type-${ParkingMarkerImageIconType.PARKINGMARKER_NORATE_DESTINATION_NOTSELECTED.type}$ICON_POSTFIX" to R.drawable.ic_parkingmarker_norate_destination_notselected,
            "$type-${ParkingMarkerImageIconType.PARKINGMARKER_NORATE_NONDESTINATION_SELECTED.type}$ICON_POSTFIX" to R.drawable.ic_parkingmarker_norate_nondestination_selected,
            "$type-${ParkingMarkerImageIconType.PARKINGMARKER_NORATE_NONDESTINATION_NOTSELECTED.type}$ICON_POSTFIX" to R.drawable.ic_parkingmarker_norate_nondestination_notselected,

            "$type-${ParkingMarkerImageIconType.PARKINGMARKER_VOUCHER_SEASON_DESTINATION_SELECTED.type}$ICON_POSTFIX" to R.drawable.ic_parkingmarker_voucher_norate_destination,
            "$type-${ParkingMarkerImageIconType.PARKINGMARKER_VOUCHER_SEASON_DESTINATION_NOTSELECTED.type}$ICON_POSTFIX" to R.drawable.ic_parkingmarker_voucher_norate_destination_notselected,
            "$type-${ParkingMarkerImageIconType.PARKINGMARKER_VOUCHER_SEASON_NONDESTINATION_SELECTED.type}$ICON_POSTFIX" to R.drawable.ic_parkingmarker_voucher_norate_nondestination_selected,
            "$type-${ParkingMarkerImageIconType.PARKINGMARKER_VOUCHER_SEASON_NONDESTINATION_NOTSELECTED.type}$ICON_POSTFIX" to R.drawable.ic_parkingmarker_voucher_norate_nondestination,
            "$type-${ParkingMarkerImageIconType.PARKINGMARKER_VOUCHER_TYPE0_DESTINATION_SELECTED.type}$ICON_POSTFIX" to R.drawable.ic_parkingmarker_voucher_typezero_destination,
            "$type-${ParkingMarkerImageIconType.PARKINGMARKER_VOUCHER_TYPE0_DESTINATION_NOTSELECTED.type}$ICON_POSTFIX" to R.drawable.ic_parkingmarker_voucher_typezero_destination_nonselected,
            "$type-${ParkingMarkerImageIconType.PARKINGMARKER_VOUCHER_TYPE0_NONDESTINATION_SELECTED.type}$ICON_POSTFIX" to R.drawable.ic_parkingmarker_voucher_typezero_nondestination_selected,
            "$type-${ParkingMarkerImageIconType.PARKINGMARKER_VOUCHER_TYPE0_NONDESTINATION_NOTSELECTED.type}$ICON_POSTFIX" to R.drawable.ic_parkingmarker_voucher_typezero_nondestination_nonselected,
            "$type-${ParkingMarkerImageIconType.PARKINGMARKER_VOUCHER_TYPE1_DESTINATION_SELECTED.type}$ICON_POSTFIX" to R.drawable.ic_parkingmarker_voucher_typeone_destination,
            "$type-${ParkingMarkerImageIconType.PARKINGMARKER_VOUCHER_TYPE1_DESTINATION_NOTSELECTED.type}$ICON_POSTFIX" to R.drawable.ic_parkingmarker_voucher_typeone_destination_notselected,
            "$type-${ParkingMarkerImageIconType.PARKINGMARKER_VOUCHER_TYPE1_NONDESTINATION_SELECTED.type}$ICON_POSTFIX" to R.drawable.ic_parkingmarker_voucher_typeone_nondestination_selected,
            "$type-${ParkingMarkerImageIconType.PARKINGMARKER_VOUCHER_TYPE1_NONDESTINATION_NOTSELECTED.type}$ICON_POSTFIX" to R.drawable.ic_parkingmarker_voucher_typeone_nondestination_notselected,
            "$type-${ParkingMarkerImageIconType.PARKINGMARKER_VOUCHER_TYPE2_DESTINATION_SELECTED.type}$ICON_POSTFIX" to R.drawable.ic_parkingmarker_voucher_typetwo_destination,
            "$type-${ParkingMarkerImageIconType.PARKINGMARKER_VOUCHER_TYPE2_DESTINATION_NOTSELECTED.type}$ICON_POSTFIX" to R.drawable.ic_parkingmarker_voucher_typetwo_destination_notselected,
            "$type-${ParkingMarkerImageIconType.PARKINGMARKER_VOUCHER_TYPE2_NONDESTINATION_SELECTED.type}$ICON_POSTFIX" to R.drawable.ic_parkingmarker_voucher_typetwo_nondestination_selected,
            "$type-${ParkingMarkerImageIconType.PARKINGMARKER_VOUCHER_TYPE2_NONDESTINATION_NOTSELECTED.type}$ICON_POSTFIX" to R.drawable.ic_parkingmarker_voucher_typetwo_nondestination_notselected,
            "$type-${ParkingMarkerImageIconType.PARKINGMARKER_VOUCHER_NORATE_DESTINATION_SELECTED.type}$ICON_POSTFIX" to R.drawable.ic_parkingmarker_voucher_norate_destination,
            "$type-${ParkingMarkerImageIconType.PARKINGMARKER_VOUCHER_NORATE_DESTINATION_NOTSELECTED.type}$ICON_POSTFIX" to R.drawable.ic_parkingmarker_voucher_norate_destination_notselected,
            "$type-${ParkingMarkerImageIconType.PARKINGMARKER_VOUCHER_NORATE_NONDESTINATION_SELECTED.type}$ICON_POSTFIX" to R.drawable.ic_parkingmarker_voucher_norate_nondestination_selected,
            "$type-${ParkingMarkerImageIconType.PARKINGMARKER_VOUCHER_NORATE_NONDESTINATION_NOTSELECTED.type}$ICON_POSTFIX" to R.drawable.ic_parkingmarker_voucher_norate_nondestination,


            )
    }

    companion object {

        const val DISTANCE = "distance"
        const val HIDDEN = "hidden"
        const val IMAGE_TYPE = "image-type"
        const val AMENITY = "amenity"
        const val SOURCE_POSTFIX = "-breeze-marker-source"
        const val LAYER_POSTFIX = "-breeze-marker-layer"
        const val ICON_POSTFIX = "-breeze-marker-icon"
        const val ICON_KEY = "icon-type"
        const val FEATURE_ID = "id"
        const val CARPARK_PRICE = "curPrice"
        const val CHEAPEST_KEY = "cheapest-key"
        const val IS_SEASON_KEY = "is-season-key"
        const val CARPARK_PRICE_SHOW = "curPriceShow"
        const val PRICE_BAR = "-price-bar"
        const val SEASON_BAR = "-season-bar"
        private const val BLACK_COLOR_STR = "#000000"
        private const val WHITE_COLOR_STR = "#ffffff"

    }

    data class MarkerViewType(val markerView2: MarkerView2, val type: String, val feature: Feature)

    interface MarkerLayerClickHandler {
        /**
         * Handle user click to maker
         * @param layerType layer type
         * @param feature a cloned feature containing id of original clicked feature but contains dynamic properties and lat/long which may have different values to the original feature
         * @currentMarker the marker containing the previously selected feature if any
         */
        fun handleOnclick(layerType: String, feature: Feature, currentMarker: MarkerViewType?)

        /**
         * Handle user click to map
         * @param point lat-long where user click
         */
        fun handleOnFreeMapClick(point: Point)

    }
//
//    interface CustomLayerHandler {
//        suspend fun addStyleImages(style: Style, type: String)
//        fun addLayer(
//            style: Style,
//            type: String,
//            sourceId: String,
//            icon_size_exp: Expression,
//            baseLayerId: String
//        )
//
//        fun removeLayer(style: Style, type: String)
//        fun onDestroy(style: Style)
//    }
}
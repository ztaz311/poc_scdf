package com.ncs.breeze.car.breeze.screen.carkpark

import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.location.Location
import android.text.SpannableString
import android.text.Spanned
import androidx.car.app.model.*
import androidx.core.graphics.drawable.IconCompat
import com.breeze.model.extensions.visibleAmount
import com.breeze.model.constants.Constants
import com.breeze.model.enums.AmenityBand
import com.ncs.breeze.car.breeze.model.CarParkLayerModel
import com.ncs.breeze.car.breeze.utils.setClickSafe
import com.ncs.breeze.R
import com.ncs.breeze.common.utils.ParkingIconUtils

class CarParkMapper(private val carParkCarContext: CarParkCarContext) {

    val SEPARATOR_DOT = " · "


    /**
     * create custom span
     */
    fun createImage(width: Int, height: Int, name: String): Bitmap {
        val bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        val paint = Paint(Paint.ANTI_ALIAS_FLAG)
        paint.color = Color.parseColor("#5da7ff")
        paint.textSize = 40f
        paint.isFakeBoldText = true
        paint.textScaleX = 1f
        canvas.drawText(name, 20f, 42f, paint)
        return bitmap
    }


    fun mapToItemList(
        carParkList: List<CarParkLayerModel>,
        itemClickListener: CarParkListItemClickListener?,
    ): ItemList {
        val listBuilder = ItemList.Builder()
        carParkList.forEachIndexed { index, carParkItem ->
            val carPark = carParkItem.carkPark;
            val title = SpannableString(" " + SEPARATOR_DOT + carPark.name)
            title.setSpan(
                DistanceSpan.create(
                    Distance.create(
                        (carPark.distance ?: 0).toDouble(),
                        Distance.UNIT_METERS
                    )
                ),
                0,
                1,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            )

            val description: SpannableString
            var canClick = true

            if (carPark.parkingType.equals(Constants.PARKING_TYPE_PARTIAL_SEASON_PARKING)) {
                canClick = false
            }

            if (carPark.parkingType.equals(Constants.PARKING_TYPE_SEASON_PARKING)) {
                description =
                    SpannableString(carParkCarContext.carContext.getString(R.string.carpark_season_parking))
                canClick = false
            } else if (carPark.parkingType.equals(Constants.PARKING_TYPE_CUSTOMER_PARKING) && carPark.availablelot == 0) {
                description =
                    SpannableString(carParkCarContext.carContext.getString(R.string.carpark_customers_only))
                canClick = false
            } else if (carPark.currentHrRate?.oneHrRate != null && carPark.nextHrRate?.oneHrRate != null) {
                val rate: String
                if (carPark.currentHrRate!!.oneHrRate == 0.0) {
                    rate = carParkCarContext.carContext.getString(R.string.carpark_free)
                } else if (carPark.currentHrRate!!.oneHrRate == -2.0) {
                    rate = carParkCarContext.carContext.getString(R.string.carpark_season)
                } else {
                    rate = carPark.currentHrRate!!.oneHrRate!!.visibleAmount()
                }
                val nextrate: String
                if (carPark.nextHrRate!!.oneHrRate == 0.0) {
                    nextrate = carParkCarContext.carContext.getString(R.string.carpark_free)
                } else if (carPark.currentHrRate!!.oneHrRate == -2.0) {
                    nextrate = carParkCarContext.carContext.getString(R.string.carpark_season)
                } else {
                    nextrate = carPark.nextHrRate!!.oneHrRate!!.visibleAmount()
                }
                if (carPark.availablelot == -1) {
                    description =
                        SpannableString("No lots info $rate(1st hr) $nextrate(next half)")
                } else {
                    val (amenityBand, colorSpan) = getTextAndColorFromAmenityBand(carPark.availablePercentImageBand)
                    val text1 =
                        carPark.availablelot.toString() + " lots" + SEPARATOR_DOT
                    val text2 = " $rate(1st hr)$SEPARATOR_DOT$nextrate(next half)"
                    description = SpannableString(text1 + amenityBand + text2)
                    description.setSpan(
                        colorSpan,
                        text1.length,
                        text1.length + amenityBand.length,
                        Spanned.SPAN_INCLUSIVE_EXCLUSIVE
                    )
                }
            } else {
                if (carPark.availablelot == -1) {
                    description = SpannableString("No Info")
                } else {
                    val (amenityBand, colorSpan) = getTextAndColorFromAmenityBand(carPark.availablePercentImageBand)
                    val text1 = carPark.availablelot.toString() + " lots" + SEPARATOR_DOT
                    description = SpannableString(text1 + amenityBand)
                    description.setSpan(
                        colorSpan,
                        text1.length,
                        text1.length + amenityBand.length,
                        Spanned.SPAN_INCLUSIVE_EXCLUSIVE
                    )
                }
            }

            val isBrowsable = false

            listBuilder.addItem(
                Row.Builder()
                    .setTitle(title)
                    .addText(description)
                    .setImage(
                        CarIcon.Builder(
                            IconCompat.createWithBitmap(
                                createImage(50, 50, (index + 1).toString())
                            )
                        ).build()
                    )
                    .setMetadata(
                        Metadata.Builder()
                            .setPlace(
                                Place.Builder(
                                    CarLocation.create(
                                        Location("").also {
                                            it.latitude = carPark.lat!!
                                            it.longitude = carPark.long!!
                                        }
                                    )
                                ).build()
                            )
                            .build()
                    )
                    .setClickSafe {
                        if (canClick) {
                            itemClickListener?.onItemClick(carPark)
                        }
                    }
                    .setBrowsable(isBrowsable)
                    .build()
            )
        }

        return listBuilder.build()
    }

    private fun getTextAndColorFromAmenityBand(amenityBand: String?): Pair<String, CarSpan> {
        return when (amenityBand) {
            AmenityBand.TYPE1.type -> "Limited" to ForegroundCarColorSpan.create(CarColor.YELLOW)
            AmenityBand.TYPE2.type -> "Available" to ForegroundCarColorSpan.create(CarColor.GREEN)
            else -> "Carpark Full" to ForegroundCarColorSpan.create(CarColor.RED)
        }
    }

//    private fun getCarIcon(): CarIcon {
//        // fixme this is using a hardcoded icon and is taking for granted it won't be null
//        return CarIcon.Builder(
//            IconCompat.createWithBitmap(bitmap(R.drawable.new_car_park_icon))
//        ).build()
//
//    }
//
//    private fun bitmap(drawableId: Int): Bitmap {
//        val drawable = VectorDrawableCompat.create(
//            carParkCarContext.carContext.resources,
//            drawableId,
//            ContextThemeWrapper(carParkCarContext.carContext, R.style.CarAppTheme).theme
//        )
//        return bitmap(drawable!!)
//    }
//
//    private fun bitmap(vectorDrawable: VectorDrawableCompat): Bitmap {
//        val px = (CAR_ICON_DIMEN * carParkCarContext.carContext.resources.displayMetrics.density).toInt()
//        val bitmap: Bitmap = Bitmap.createBitmap(px, px, Bitmap.Config.ARGB_8888)
//        val canvas = Canvas(bitmap)
//        vectorDrawable.setBounds(0, 0, canvas.width, canvas.height)
//        vectorDrawable.draw(canvas)
//        return bitmap
//    }

    private companion object {

        private const val CAR_ICON_DIMEN = 64
    }
}
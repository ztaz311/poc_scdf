package com.ncs.breeze.common.utils

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.Matrix
import android.graphics.Typeface
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.location.LocationManager
import android.net.Uri
import android.os.Build
import android.os.Handler
import android.os.Looper
import android.provider.CalendarContract
import android.provider.ContactsContract
import android.provider.MediaStore
import android.provider.Settings
import android.text.Spannable
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.text.format.DateUtils
import android.text.style.StyleSpan
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.browser.customtabs.CustomTabsService
import androidx.core.content.ContextCompat
import com.breeze.model.api.response.AddressesItem
import com.ncs.breeze.common.model.rx.PhoneNavigationStartEventData
import com.breeze.model.DestinationAddressTypes
import com.breeze.model.api.response.TripPlannerItem
import com.breeze.model.constants.Constants
import com.breeze.model.extensions.safeDouble
import com.ncs.breeze.common.receivers.LocationEnabledReceiver
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.crashlytics.ktx.crashlytics
import com.google.firebase.ktx.Firebase
import com.mapbox.geojson.Point
import com.mapbox.geojson.Polygon
import com.mapbox.turf.TurfConstants
import com.mapbox.turf.TurfJoins
import com.mapbox.turf.TurfMeasurement
import timber.log.Timber
import java.io.IOException
import java.math.RoundingMode
import java.text.*
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.ZoneId
import java.util.*
import kotlin.math.roundToInt


object Utils {

    var isNavigationStarted = false
    var phoneNavigationStartEventData: PhoneNavigationStartEventData? = null

    //private var testDate: Calendar? = null

    suspend fun readCalendarEvent(context: Context): ArrayList<BreezeCalendarUtil.CalendarEvent> {

        var listOfCalendarEvents = arrayListOf<BreezeCalendarUtil.CalendarEvent>()
        // Projection array. Creating indices for this array instead of doing
        // dynamic lookups improves performance.
        val EVENT_PROJECTION: Array<String> = arrayOf(
            CalendarContract.Events._ID, // 0
            CalendarContract.Events.TITLE, // 1
            CalendarContract.Events.DTSTART, // 2
            CalendarContract.Events.DTEND, // 3
            CalendarContract.Events.EVENT_LOCATION // 4
        )

        // The indices for the projection array above.
        val PROJECTION_ID_INDEX: Int = 0
        val PROJECTION_TITLE_INDEX: Int = 1
        val PROJECTION_DTSTART_INDEX: Int = 2
        val PROJECTION_DTEND_INDEX: Int = 3
        val PROJECTION_EVENT_LOCATION_INDEX: Int = 4

        val selection: String = "((${CalendarContract.Events.DTSTART} >= ?) AND (" +
                "${CalendarContract.Events.DTEND} <= ?)" + " AND ( " + CalendarContract.Events.DELETED + "  != 1) " + "     )";

        /*val selection: String = "(${CalendarContract.Events.DTSTART} >= ?)"*/

        val selectionArgs: Array<String> = arrayOf(
            "${Calendar.getInstance().timeInMillis}",
            getEndOfDayMilliSeconds().toString()
        )

        /*val selectionArgs: Array<String> = arrayOf(
            getDateInMilliSeconds().toString()
        )*/

        val sortOrder = "${CalendarContract.Events.DTSTART} ASC"

        val cursor: Cursor? = context.contentResolver
            .query(
                Uri.parse(CalendarContract.Events.CONTENT_URI.toString()),
                EVENT_PROJECTION,
                selection,
                selectionArgs,
                sortOrder
            )

        try {
            if (cursor!!.count > 0) {
                while (cursor.moveToNext()) {

                    val eventID: Long = cursor.getLong(PROJECTION_ID_INDEX)
                    val title: String = cursor.getString(PROJECTION_TITLE_INDEX)
                    val dtStart: String = cursor.getString(PROJECTION_DTSTART_INDEX)
                    val dtEnd: String = cursor.getString(PROJECTION_DTEND_INDEX)

                    if (cursor.getType(PROJECTION_EVENT_LOCATION_INDEX) != Cursor.FIELD_TYPE_NULL) {
                        val eventLocation: String =
                            cursor.getString(PROJECTION_EVENT_LOCATION_INDEX)
                        val latLng = getLocationFromAddress(eventLocation, context)
                        val eventItem = AddressesItem()
                        eventItem.address1 = title
                        eventItem.address2 = eventLocation
                        eventItem.destinationAddressType = DestinationAddressTypes.CALENDAR
                        eventItem.name = eventLocation
                        // Calendar item will always be the first item by default
                        eventItem.ranking = 0
                        if (latLng != null) {
                            eventItem.lat = latLng.latitude.toString()
                            eventItem.long = latLng.longitude.toString()
                            var calendarEvent = BreezeCalendarUtil.CalendarEvent(eventID, eventItem)
                            listOfCalendarEvents.add(calendarEvent)
                        }
                    }
                }

            }
        } catch (e: Exception) {
            Timber.e(e, "Exception while reading Calendar Events")
        }

        Log.d("testing-abc", "Printing list of calendar events ${listOfCalendarEvents}")
        return listOfCalendarEvents
    }


    /**
     * check is exist any triplog
     */
    fun isExistTripLogInDay(trips: List<TripPlannerItem>): Boolean {
        var isExistTripLogInDay: Boolean = false
        trips.forEach {
            if (DateUtils.isToday((it.tripEstStartTime?.toLong() ?: 0) * 1000)) {
                isExistTripLogInDay = true
                return isExistTripLogInDay
            }
        }
        return isExistTripLogInDay
    }


    fun isValidUserNameWithBreezeName(
        listNameBlackList: ArrayList<String>?,
        enteredText: String
    ): Boolean {
        listNameBlackList?.forEach {
            if (enteredText.toLowerCase() == it.lowercase()) {
                return false
            }
        }
        return true
    }


    /**
     * Input time string in HH:mm (24 hour period)
     * @return returns date in epoch timestamp (current date is taken as reference), for invalid input returns the current timestamp
     */
    fun convertHHMMToTimeStamp(dateString: String): Long {
        val timeFields = dateString.split(":")
        if (timeFields.size != 2) {
            return Calendar.getInstance().timeInMillis
        }
        return try {
            val calendar = Calendar.getInstance()
            calendar.set(Calendar.HOUR_OF_DAY, timeFields[0].toInt())
            calendar.set(Calendar.MINUTE, timeFields[1].toInt())
            calendar.timeInMillis
        } catch (e: NumberFormatException) {
            Timber.e(e, e.message)
            Calendar.getInstance().timeInMillis
        }

    }

    suspend fun getLocationFromAddress(strAddress: String, context: Context): LatLng? {

        var coder = Geocoder(context, Locale.getDefault())
        var address: List<Address>

        try {
            address = coder.getFromLocationName(strAddress, 5)?.toList() ?: emptyList()
            if (address.isEmpty()) {
                Log.e("GeoCoder", "Unable to get lat-lng for address [$strAddress]")
                return null;
            }
            var location = address[0]
            return LatLng(location.latitude, location.longitude);
        } catch (exception: Exception) {
            Timber.e(exception)
            return null
        }
    }

    suspend fun getAddressFromLocation(location: Location, context: Context): String? {
        try {
            val address = getAddressObject(location, context)
            if (address != null) {
                return address.getAddressLine(0)
            }
        } catch (
            e: Exception
        ) {
            return null
        }
        return null
    }

    fun calculateBoundingPoints(
        carparkRadius: Double,
        centreLat: Double,
        centreLong: Double
    ): ArrayList<Point> {
        var bearingList = arrayListOf(
            0.0,
            45.0,
            90.0,
            135.0,
            180.0,
            -90.0,
            -45.0,
            -135.0
        )

        var destinationPoint = Point.fromLngLat(centreLong, centreLat)
        var distance = carparkRadius + 50

        var listOfPoints = arrayListOf<Point>()

        for (bearing in bearingList) {
            var point = TurfMeasurement.destination(
                destinationPoint,
                distance,
                bearing,
                TurfConstants.UNIT_METRES
            )
            listOfPoints.add(point)
        }
        return listOfPoints
    }

    suspend fun getAddressObject(location: Location, context: Context): Address? {
        var geocoder = Geocoder(context, Locale.getDefault())
        try {
            val addresses: List<Address> = geocoder.getFromLocation(
                location.latitude,
                location.longitude,
                1
            )?.toList() ?: listOf()
            return if (addresses.isNotEmpty()) {
                /*for (address in addresses) {
                            if (!TextUtils.isEmpty(address.thoroughfare)) {
                                return address.thoroughfare
                            }
                        }*/
                addresses[0]
            } else {
                null
            }
        } catch (e: IOException) {
            Timber.e(e)
        }
        return null
    }

    suspend fun areAddressesSupported(
        context: Context,
        origin: Location,
        destination: Location,
    ): Boolean {
        return isAddressSupported(context, origin) &&
                isAddressSupported(context, destination)
    }

    suspend fun isAddressSupported(
        context: Context,
        location: Location,
    ): Boolean {
        val address = getAddressObject(location, context)
        address?.let {
            if (
                it.countryCode.equals("SG", ignoreCase = true) ||
                it.countryName.equals("singapore", ignoreCase = true) ||
                TurfJoins.inside(
                    Point.fromLngLat(location.longitude, location.latitude),
                    Polygon.fromLngLats(listOf(Constants.SINGAPORE_REGION_COORDS))
                )
            ) {
                return true
            } else {
                val logMessage =
                    "Country code [${it.countryCode}] and name [${it.countryName}] found for location [$location]"
                Log.e("Geocoder", logMessage)
                Firebase.crashlytics.log(logMessage)
                return false
            }
        }
        return true
    }

    suspend fun getStreetNameFromLocation(location: Location, context: Context): String? {
        var geocoder = Geocoder(context, Locale.getDefault())
        try {
            val addresses: List<Address> = geocoder.getFromLocation(
                location.latitude,
                location.longitude,
                1
            ) ?: emptyList()
            return if (addresses.isNotEmpty()) {
                val returnedAddress = addresses[0]
                /*for (address in addresses) {
                            if (!TextUtils.isEmpty(address.thoroughfare)) {
                                return address.thoroughfare
                            }
                        }*/
                returnedAddress.thoroughfare
            } else {
                null
            }
        } catch (e: IOException) {
            Timber.e(e)
        }
        return null
    }

    fun getCurrentDateFormatted(outputFormat: String): String {
        val formatter = SimpleDateFormat(outputFormat)
        val calendar: Calendar = CURRENT_CALENDER()
        return formatter.format(calendar.time)
    }

    fun CURRENT_DATE(): Date {
//        if (testDate != null) {
//            return testDate!!.time
//        }
        return Date()
    }

    fun CURRENT_CALENDER(): Calendar {
//        if (testDate != null) {
//            return testDate!!
//        }
        return Calendar.getInstance()
    }

    fun TEST_DATE_TIME(newTestTime: Calendar) {
        //testDate = newTestTime
    }

    fun getDate(milliSeconds: Long): String? {
        val formatter = SimpleDateFormat(
            "dd/MM/yyyy hh:mm:ss a"
        )
        val calendar: Calendar = Calendar.getInstance()
        calendar.setTimeInMillis(milliSeconds)
        return formatter.format(calendar.getTime())
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun atEndOfDay(date: Date): Date? {
        val localDateTime: LocalDateTime = dateToLocalDateTime(date)
        val endOfDay: LocalDateTime = localDateTime.with(LocalTime.MAX)
        return localDateTimeToDate(endOfDay)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun dateToLocalDateTime(date: Date): LocalDateTime {
        return LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault())
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun localDateTimeToDate(localDateTime: LocalDateTime): Date? {
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant())
    }

    private fun getEndOfDayMilliSeconds(): Long {
        val calendar = Calendar.getInstance()
        calendar[Calendar.HOUR_OF_DAY] = 23
        calendar[Calendar.MINUTE] = 59
        calendar[Calendar.SECOND] = 59
        calendar[Calendar.MILLISECOND] = 999
        return calendar.timeInMillis
    }

    fun getDeviceID(context: Context): String? {
        return Settings.Secure.getString(context.contentResolver, Settings.Secure.ANDROID_ID)
    }

    fun getDeviceDetails(context: Context): String? {
        val platform = "Android"
        val manufacturer = Build.MANUFACTURER
        val model = Build.MODEL
        //String OS = Build.VERSION_CODES.class.getFields()[android.os.Build.VERSION.SDK_INT].getName();
        val OS = Build.VERSION.RELEASE
        val version = String.format("Version-%s", getAppVersion(context))
        return String.format("%s %s %s %s %s", platform, manufacturer, model, OS, version)
    }

    fun getDeviceModel(): String {
        return Build.MODEL
    }


    fun getDeviceManufacturer(): String {
        return Build.MANUFACTURER
    }

    fun getDeviceOS(): String {
        return Build.VERSION.RELEASE
    }

    fun getAppVersion(context: Context): String {
        var result = ""
        try {
            result = context.packageManager
                .getPackageInfo(context.packageName, 0).versionName
            result = result.replace("[a-zA-Z]|-".toRegex(), "")
        } catch (e: PackageManager.NameNotFoundException) {
            Timber.e(e, e.message)
        }
        return result
    }


    fun calculation(lat1: Double, lon1: Double, lat2: Double, lon2: Double): Double {
        val theta = lon1 - lon2
        var dist = (Math.sin(deg2rad(lat1))
                * Math.sin(deg2rad(lat2))
                + (Math.cos(deg2rad(lat1))
                * Math.cos(deg2rad(lat2))
                * Math.cos(deg2rad(theta))))
        dist = Math.acos(dist)
        dist = rad2deg(dist)
        dist = dist * 60 * 1.1515
        return dist
    }

    fun deg2rad(deg: Double): Double {
        return deg * Math.PI / 180.0
    }

    fun rad2deg(rad: Double): Double {
        return rad * 180.0 / Math.PI
    }

    fun calculateNoOfColumns(
        context: Context,
        columnWidthDp: Float
    ): Int { // For example columnWidthdp=180
        val displayMetrics = context.resources.displayMetrics
        val screenWidthDp = displayMetrics.widthPixels / displayMetrics.density
        return (screenWidthDp / columnWidthDp + 0.5).toInt()
    }

    fun checkLocationPermission(context: Context): Boolean {
        val fineLocationPermission = ContextCompat.checkSelfPermission(
            context,
            Manifest.permission.ACCESS_FINE_LOCATION
        )

        val coarseLocationPermission = ContextCompat.checkSelfPermission(
            context,
            Manifest.permission.ACCESS_COARSE_LOCATION
        )

        return !(fineLocationPermission != PackageManager.PERMISSION_GRANTED || coarseLocationPermission != PackageManager.PERMISSION_GRANTED)
    }

    fun checkCalendarPermission(context: Context): Boolean {
        val readCalendarPermission = ContextCompat.checkSelfPermission(
            context,
            Manifest.permission.READ_CALENDAR
        )
        return readCalendarPermission == PackageManager.PERMISSION_GRANTED
    }

    fun checkContactsPermission(context: Context): Boolean {
        val readCalendarPermission = ContextCompat.checkSelfPermission(
            context,
            Manifest.permission.READ_CONTACTS
        )
        return readCalendarPermission == PackageManager.PERMISSION_GRANTED
    }

    fun checkGPSEnabled(context: Context?): Boolean {
        val lm = context!!.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        var gps_enabled = false
        var network_enabled = false

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER)
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
        } catch (ex: Exception) {
            Timber.e(ex)
        }

        var isEnabled = gps_enabled || network_enabled
        LocationEnabledReceiver.lastDetectedStatus = isEnabled

        return isEnabled
    }

    fun roundOffDecimal(number: Double): Double {
        val DcmFrmSmb = DecimalFormatSymbols(Locale.US)
        val df = DecimalFormat("#.#", DcmFrmSmb)
        df.roundingMode = RoundingMode.HALF_EVEN
        return df.format(number).safeDouble()
    }

    fun convertMeterToKilometer(meter: Float): Double {
        return (meter * 0.001)
    }

    fun convertDurationToMinutes(seconds: Float): Int {
        return (seconds / 60.0).roundToInt()
    }

    fun convertTimeToMinsAndSec(seconds: Int): String {
        var min = seconds / 60
        var sec = seconds % 60
        if (min == 0) {
            return String.format("%dsec", sec)
        } else {
            return String.format("%dm %dsec", min, sec)
        }
    }


    fun splitToComponentTimes(totalMinutes: Int): IntArray? {
        val hours = totalMinutes / 60;
        val minutes = totalMinutes % 60
        return intArrayOf(Integer.parseInt(hours.toString().split(".")[0]), minutes)
    }

    fun addTimeToCurrentTime(additionalHour: Int, additionalMinute: Int): String {
        val additionalSeconds = 0
        val sdf = SimpleDateFormat("yyyy:MM:dd:HH:mm", Locale.getDefault())
        val currentDateandTime = sdf.format(CURRENT_DATE())

        var date: Date? = null
        try {
            date = sdf.parse(currentDateandTime)
        } catch (e: ParseException) {
            Timber.e(e)
        }
        val calendar = Calendar.getInstance()
        calendar.time = date
        calendar.add(Calendar.HOUR, additionalHour)
        calendar.add(Calendar.MINUTE, additionalMinute)
        calendar.add(Calendar.SECOND, additionalSeconds)


        val df = SimpleDateFormat("hh:mm a")
        val formatTime = df.format(calendar.time)
        return formatTime
    }

    fun addTimeToTimeStamp(
        additionalHour: Int,
        additionalMinute: Int,
        startTimestamp: Long
    ): String {
        val df = SimpleDateFormat("hh:mm a")
        val formatTime =
            df.format(startTimestamp + additionalHour * 3600000 + additionalMinute * 60000)
        return formatTime
    }

    fun formatTime(startTimestamp: Long): String {
        val df = SimpleDateFormat("hh:mmaa", Locale.US)
        val formatTime = df.format(startTimestamp)
        return formatTime
    }

    fun formatTimeWith12HFromHH(time: String): String {
        val inFormat: DateFormat = SimpleDateFormat("HH:mm")
        val outFormat: DateFormat = SimpleDateFormat("hh:mmaa")
        var date: Date? = null
        try {
            date = inFormat.parse(time)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        if (date != null) {
            return outFormat.format(date).toLowerCase()
        }
        return time
    }

    fun addTimeToCurrentTimeInSeconds(additionalMinutes: Int): Long {
        val calendar = Calendar.getInstance()
        calendar.add(Calendar.MINUTE, additionalMinutes)
        return calendar.timeInMillis / 1000L
    }


    fun getOrientation(context: Context, photoUri: Uri?): Int {
        val cursor = context.contentResolver.query(
            photoUri!!,
            arrayOf(MediaStore.Images.ImageColumns.ORIENTATION),
            null,
            null,
            null
        )
        if (cursor == null || cursor.count != 1) {
            return 90 //Assuming it was taken portrait
        }
        cursor.moveToFirst()
        return cursor.getInt(0)
    }

    fun rotateImage(source: Bitmap, angle: Float): Bitmap? {
        val matrix = Matrix()
        matrix.postRotate(angle)
        return Bitmap.createBitmap(
            source, 0, 0, source.width, source.height,
            matrix, true
        )
    }

    fun escapeAmpersandWithSpace(input: String): String {
        return input.replace(" & ", "&").replace("&", " & ")
    }

    // navigating user to app settings
    fun openSettings(context: Context) {
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        intent.data = Uri.parse("package:" + context.packageName)
        context.startActivity(intent)
    }


    fun extractPhoneNumberFromContactIntent(data: Intent, context: Context): ContactDetails? {

        var contactDetails: ContactDetails? = null
        var phoneNumberList = arrayListOf<String>()
        var name: String? = null

        val contactData = data.data
        if (contactData != null) {
            context.contentResolver?.query(contactData, null, null, null, null).use { c ->
                if (c != null && c.moveToFirst()) {
                    name =
                        c.getString(c.getColumnIndexOrThrow(ContactsContract.Contacts.DISPLAY_NAME))
                    val contactId =
                        c.getString(c.getColumnIndexOrThrow(ContactsContract.Contacts._ID))

                    val columnIndex = c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER)
                    if ((columnIndex > -1) && (c.getString(columnIndex)
                            .toInt() > 0)
                    ) {
                        // Query phone here. Covered next
                        val phones = context.contentResolver?.query(
                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            null,
                            ContactsContract.CommonDataKinds.Phone._ID + " = " + contactId,
                            null,
                            null
                        )

                        while (phones != null && phones.moveToNext()) {
                            val phonesColumnIndex =
                                phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)
                            if (phonesColumnIndex > -1) {
                                var phoneNumber =
                                    phones.getString(phonesColumnIndex)

                                val re = Regex("[^0-9]")
                                phoneNumber = re.replace(phoneNumber, "")
                                phoneNumberList.add(phoneNumber)
                            }
                        }
                        phones?.close()
                    }
                }
            }
        }

        if (!name.isNullOrBlank() && phoneNumberList.size > 0) {
            // Check phone numbers
            var uniquePhoneList = phoneNumberList.distinct()
            var validatedList = arrayListOf<String>()
            Log.d("testing-abc", "phone number list size: " + phoneNumberList.size)
            Log.d("testing-abc", "unique list size: " + uniquePhoneList.size)
            uniquePhoneList.forEach {
                var phonenumber = it
                if (it.startsWith("65")) {
                    phonenumber = it.substring(2, it.length)
                }

                if (phonenumber.length == 8 && /*(phonenumber.startsWith("8") || phonenumber.startsWith("9"))*/ phonenumber.matches(
                        Regex(Constants.REGEX.PHONE_REGEX)
                    )
                ) {
                    validatedList.add(phonenumber)
                }
            }

            if (validatedList.size > 0) {
                Log.d("testing-xyz", "New contact has been chosen")
                contactDetails =
                    ContactDetails(contactName = name!!, phoneNumberList = validatedList)
            }
        }

        return contactDetails
    }

    fun appendCountryCode(phoneNumber: String): String {
        return String.format("%s%s", Constants.SINGAPORE_COUNTRY_CODE, phoneNumber)
    }

    fun getFormattedSpan(
        displayText: String,
        replaceText: String,
        toBeReplaced: String
    ): SpannableStringBuilder {
        val str = SpannableStringBuilder(displayText)

        val replaceSpan = SpannableString(replaceText)
        replaceSpan.setSpan(
            StyleSpan(Typeface.BOLD),
            0,
            replaceText.length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )

        val startSpan: Int = str.indexOf(toBeReplaced)
        val endSpan: Int = startSpan + toBeReplaced.length
        str.replace(startSpan, endSpan, replaceSpan)
        return str
    }


    fun capitalize(strToCap: String): String {
        return strToCap.split(' ')
            .joinToString(separator = " ") { value ->
                value.lowercase().replaceFirstChar { it.uppercaseChar() }
            }
    }


    fun adjustDPToPixels(dp: Int, scale: Float) = ((dp * scale) + 0.5F).toInt()

    fun isAnyCustomTabsPackageInstalled(context: Context): Boolean {
        val pm: PackageManager = context.packageManager
        // Get default VIEW intent handler.
        val activityIntent = Intent()
            .setAction(Intent.ACTION_VIEW)
            .addCategory(Intent.CATEGORY_BROWSABLE)
            .setData(Uri.fromParts("http", "", null))
        // Get all apps that can handle VIEW intents.
        val resolvedActivityList =
            pm.queryIntentActivities(activityIntent, PackageManager.MATCH_ALL)
        for (info in resolvedActivityList) {
            if (
                info.activityInfo.packageName.startsWith("com.android.chrome") ||
                info.activityInfo.packageName.startsWith("com.chrome")
            ) {
                val serviceIntent = Intent()
                serviceIntent.action = CustomTabsService.ACTION_CUSTOM_TABS_CONNECTION
                serviceIntent.setPackage(info.activityInfo.packageName)
                // Check if this package also resolves the Custom Tabs service.
                if (pm.resolveService(serviceIntent, 0) != null) {
                    return true
                }
            }
        }
        return false
    }

    fun runOnMainThread(code: () -> Unit) {
        val mainHandler = Handler(Looper.getMainLooper())
        mainHandler.post { code() }
    }

    data class ContactDetails(var contactName: String, var phoneNumberList: List<String>)
}




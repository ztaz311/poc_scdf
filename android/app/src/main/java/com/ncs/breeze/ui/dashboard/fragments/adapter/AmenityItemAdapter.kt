package com.ncs.breeze.ui.dashboard.fragments.adapter


import android.content.Context
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.ncs.breeze.common.analytics.Analytics
import com.breeze.model.constants.AmenityType.EVCHARGER
import com.ncs.breeze.common.utils.BreezeMapDataHolder
import com.ncs.breeze.common.utils.Utils
import com.bumptech.glide.Glide
import com.ncs.breeze.R
import com.ncs.breeze.common.analytics.Screen


class AmenityItemAdapter(
    val context: Context,
    var dataList: List<AmenityCount>,
    val listener: AmenityItemListener
) : RecyclerView.Adapter<AmenityItemAdapter.ViewHolder>() {

    val scale = context.resources.displayMetrics.density

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder {

        val view =
            LayoutInflater.from(viewGroup.context).inflate(R.layout.item_amenity, viewGroup, false)
        return ViewHolder(view)

    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.textView.text = dataList[position].name
        setSelectionViewBackground(position, holder)
        setSelectionViewPaddingAndTextSize(position, holder)
        holder.itemAmenity.setOnClickListener {
            handleItemClick(position, holder)
        }
    }

    private fun handleItemClick(
        position: Int,
        holder: ViewHolder
    ) {
        dataList[position].selected = !dataList[position].selected
        listener.onItemClick(dataList[position].type, dataList[position])
        setSelectionViewBackground(position, holder)

        if (dataList[position].selected) {
            Analytics.logToggleEvent(
                dataList[position].name.lowercase() + "_on",
                Screen.ROUTE_PLANNING
            )
        } else {
            Analytics.logToggleEvent(
                dataList[position].name.lowercase() + "_off",
                Screen.ROUTE_PLANNING
            )
        }
    }

    private fun setSelectionViewBackground(
        position: Int,
        holder: ViewHolder
    ) {
        if (dataList[position].selected) {
            holder.textView.setBackgroundResource(R.drawable.themed_route_amenity_selected_bg)
            holder.textView.setTextColor(context.getResources().getColor(R.color.white, null))
            if (dataList[position].type == EVCHARGER) {
                val subItem =
                    BreezeMapDataHolder.amenitiesPreferenceHashmap[EVCHARGER]?.subItems?.find {
                        if (it.displayText == dataList[position].name) {
                            return@find true
                        }
                        return@find false
                    }
                subItem?.let {
                    Glide.with(context).load(it.buttonActiveImageUrl).into(holder.imgView)
                    holder.imgView.visibility = View.VISIBLE
                }
            } else {
                holder.imgView.visibility = View.GONE
            }
        } else {
            holder.textView.setBackgroundResource(R.drawable.themed_route_amenity_unselected_bg)
            holder.textView.setTextColor(
                context.getResources().getColor(R.color.themed_text_black, null)
            )
            if (dataList[position].type == EVCHARGER) {
                val subItem =
                    BreezeMapDataHolder.amenitiesPreferenceHashmap[EVCHARGER]?.subItems?.find {
                        if (it.displayText == dataList[position].name) {
                            return@find true
                        }
                        return@find false
                    }
                subItem?.let {
                    Glide.with(context).load(it.buttonInactiveImageUrl).into(holder.imgView)
                    holder.imgView.visibility = View.VISIBLE
                }
            } else {
                holder.imgView.visibility = View.GONE
            }
        }
    }

    private fun setSelectionViewPaddingAndTextSize(
        position: Int,
        holder: ViewHolder
    ) {
        if (dataList[position].type == EVCHARGER) {
            holder.textView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 14.0F)
            holder.textView.setPadding(
                Utils.adjustDPToPixels(18, scale),
                Utils.adjustDPToPixels(5, scale),
                Utils.adjustDPToPixels(18, scale),
                Utils.adjustDPToPixels(5, scale)
            )
            if (position == (dataList.size - 1)) {//no need padding end for last item
                holder.itemAmenity.setPadding(
                    0, Utils.adjustDPToPixels(12, scale), 0, Utils.adjustDPToPixels(12, scale)
                )
            } else {
                holder.itemAmenity.setPadding(
                    0,
                    Utils.adjustDPToPixels(12, scale),
                    Utils.adjustDPToPixels(14, scale),
                    Utils.adjustDPToPixels(12, scale)
                )
            }
        } else {
            holder.textView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 16.0F)
            holder.textView.setPadding(
                Utils.adjustDPToPixels(18, scale),
                Utils.adjustDPToPixels(5, scale),
                Utils.adjustDPToPixels(18, scale),
                Utils.adjustDPToPixels(5, scale)
            )
            holder.itemAmenity.setPadding(
                0,
                Utils.adjustDPToPixels(12, scale),
                Utils.adjustDPToPixels(18, scale),
                Utils.adjustDPToPixels(12, scale)
            )
        }
    }


    override fun getItemCount(): Int {
        return dataList.size
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val textView: TextView
        val imgView: ImageView
        val itemAmenity: RelativeLayout

        init {
            itemAmenity = view.findViewById(R.id.item_amenity)
            textView = view.findViewById(R.id.txt_item_amenity)
            imgView = view.findViewById(R.id.img_item_amenity)
        }
    }

    override fun getItemId(position: Int): Long {
        return super.getItemId(position)
    }

    interface AmenityItemListener {
        fun onItemClick(type: String, data: AmenityCount)
    }

    /**
     * Constructs type AmenityCount for petrol and ev, amneity subitems
     * @param name is the display_text asssociated with the subitem
     * @param itemType is the element_name asssociated with the subitem
     * @param count is the total count of the subitem
     * @param type is the amneity type (element_name of main item), e.g petrol/evcharger
     * @param selected selection state
     */
    data class AmenityCount(
        val name: String,
        val itemType: String,
        var count: Int,
        val type: String,
        var selected: Boolean
    )
}
package com.ncs.breeze.ui.draggableview

import android.view.View

interface DraggableListener {

    fun onPositionChanged(view: View)

}
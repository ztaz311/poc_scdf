package com.ncs.breeze.common.utils

import android.content.Context
import android.database.ContentObserver
import android.os.Handler
import android.os.Looper
import android.os.Parcelable
import android.provider.CalendarContract
import android.util.Log
import com.breeze.model.api.response.AddressesItem
import com.breeze.model.DestinationAddressDetails
import com.ncs.breeze.common.rxbus.RxBus
import com.ncs.breeze.common.rxbus.RxEvent
import kotlinx.android.parcel.Parcelize
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.Serializable
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class BreezeCalendarUtil @Inject constructor(appContext: Context) {

    var appContext = appContext
    var eventsList: ArrayList<CalendarEvent> = arrayListOf()
    lateinit var calendarContentObserver: CalendarObserver

    var isNavigatingToCalendarEventDestination: Boolean = false
    var navigationEvent: CalendarEvent? = null
    var navigationAddressDetails: DestinationAddressDetails? = null

    /*
    * Taking reference of the last clicked event in case there are multiple events and the current event shown in
    * Address Recommendations is a different event
    */
    var lastClickedCalendarEvent: CalendarEvent? = null

    fun startCalendarObserver() {
        calendarContentObserver = CalendarObserver(Handler(Looper.getMainLooper()), appContext)
        appContext.contentResolver.registerContentObserver(
            CalendarContract.Events.CONTENT_URI, false, calendarContentObserver
        )
    }

    inner class CalendarObserver(handler: Handler?, context: Context) : ContentObserver(handler) {
        var context = context
        var lastTimeofCall = 0L
        var lastTimeofUpdate = 0L
        var threshold_time: Long = 10000
        override fun deliverSelfNotifications(): Boolean {
            return true
        }

        override fun onChange(selfChange: Boolean) {
            super.onChange(selfChange)

            // Keeping a threshold as OnChanged is called multiple times
            lastTimeofCall = System.currentTimeMillis();

            if (lastTimeofCall - lastTimeofUpdate > threshold_time) {
                Log.d("testing-abc", "On Change triggered")
                CoroutineScope(Dispatchers.IO).launch {
                    var calendarEvents = readCalendarEvents()
                    withContext(Dispatchers.Main) {
                        RxBus.publish(RxEvent.UpdateCalendarEvent(true))
                        if (calendarEvents.isNotEmpty() && isNavigatingToCalendarEventDestination) {
                            // Update scenario, need to check if navigation impacted
                            for (event in calendarEvents) {
                                if (navigationEvent != null && navigationEvent!!.eventId == event.eventId) {

                                    /*
                                    * Checking both navigationEvent & lastClickedCalendarEvent to avoid prompting user
                                    * more than once as onChanged is called multiple times when there is a change in event
                                    */
                                    if ((navigationEvent!!.eventItem.lat != event.eventItem.lat ||
                                                navigationEvent!!.eventItem.long != event.eventItem.long) &&
                                        (lastClickedCalendarEvent!!.eventItem.lat != event.eventItem.lat ||
                                                lastClickedCalendarEvent!!.eventItem.long != event.eventItem.long)
                                    ) {
                                        var addressItem = event.eventItem
                                        var destinationDetails = DestinationAddressDetails(
                                            addressItem.addressId,
                                            addressItem.address1,
                                            addressItem.address2,
                                            addressItem.lat,
                                            addressItem.long,
                                            null,
                                            addressItem.name,
                                            addressItem.destinationAddressType,
                                            navigationEvent!!.eventItem.ranking!!
                                        )
                                        RxBus.publish(
                                            RxEvent.NotifyCalendarEventDestinationChange(
                                                destinationDetails
                                            )
                                        )
                                    }
                                    // Replacing the lastClickedCalendarEvent with the latest details
                                    lastClickedCalendarEvent = event
                                    break
                                }
                            }
                        }
                        lastTimeofUpdate = System.currentTimeMillis();
                    }
                }
            }
        }
    }

    fun clickedCalendarEvent() {
        lastClickedCalendarEvent = eventsList[0]
    }

    fun navigatingToCalendarEventDestination(destinationItem: DestinationAddressDetails) {
        //Check to confirm if navigating to same event as latest calendar event
        if (lastClickedCalendarEvent != null) {
            Log.d("testing-abc", "Navigating to calendar event set")
            isNavigatingToCalendarEventDestination = true
            navigationEvent = lastClickedCalendarEvent
            navigationAddressDetails = destinationItem
        }
    }

    fun rerouteToNewLocation(destinationDetails: DestinationAddressDetails) {
        navigationAddressDetails = destinationDetails
        navigationEvent = lastClickedCalendarEvent
    }

    fun navigationStopped() {
        isNavigatingToCalendarEventDestination = false
        navigationEvent = null
        navigationAddressDetails = null
        lastClickedCalendarEvent = null
    }

    fun unregisterCalendarObserver() {
        if (::calendarContentObserver.isInitialized) {
            appContext.contentResolver.unregisterContentObserver(calendarContentObserver)
        }
    }

    suspend fun readCalendarEvents(): ArrayList<CalendarEvent> {
        eventsList = Utils.readCalendarEvent(appContext)
        return eventsList
    }

    @Parcelize
    data class CalendarEvent(
        var eventId: Long,
        var eventItem: AddressesItem
    ) : Parcelable, Serializable
}
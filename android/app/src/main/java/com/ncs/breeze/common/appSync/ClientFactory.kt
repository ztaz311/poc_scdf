package com.ncs.breeze.common.appSync

import android.content.Context
import com.amazonaws.mobile.config.AWSConfiguration
import com.amazonaws.mobileconnectors.appsync.AWSAppSyncClient
import com.amazonaws.mobileconnectors.appsync.ClearCacheOptions
import timber.log.Timber

object ClientFactory {
    @Volatile
    private var clientMap: HashMap<String,AWSAppSyncClient> = HashMap()

    /**
     * All subscriptions/queries and OBU_REAL/ OBU_SIMULATED mutations use the default instance
     */
    private const val DEFAULT_CLIENT_INSTANCE = "DEFAULT";

    /**
     *  Due to Appsync not handling mutations cleanly in a single instance when we trigger multiple mutations per second while passing live location ,
     *  we have using multiton design pattern to have multiple isolated instance of the appsync client for each type of appsync mutation.
     *  Currently supported are [OBU_SIMULATED,OBU_REAL,ETA_LIVE_LOCATION,WALKATHON_LIVE_LOCATION]
     *
     *  This, along with cancelling mutations before enqueuing a new one, would resolve the following issues:
     *  1. Appsync client has some race-conditions causing crashing when the mutation rate is high and there are failures/or timeouts see -> {@link https://github.com/awslabs/aws-mobile-appsync-sdk-android/issues/153}
     *  2. Appsync client has a very high timeout, causing extremely high cumulating delays when triggering the mutations, but lowering the timeout causing crashes as seen above
     *
     *
     */
    @Synchronized
    fun getInstance(context: Context, clientInstanceKey: String = DEFAULT_CLIENT_INSTANCE): AWSAppSyncClient? {
        var client = clientMap[clientInstanceKey]
        if (client == null) {
            try {
                val awsConfig = AWSConfiguration(context)
                client = AWSAppSyncClient.builder()
                    .context(context)
                    .awsConfiguration(awsConfig)
                    .cognitoUserPoolsAuthProvider(BreezeCognitoUserPoolsAuthProvider())
                    .clientDatabasePrefix(clientInstanceKey)
                    .useClientDatabasePrefix(true)
                    .build()
                clientMap[clientInstanceKey] = client
            } catch (e: Exception) {
                Timber.e(e, "Run time exception observed")
            }
        }
        return client
    }


    @Synchronized
    fun destroyClientObject(clientInstanceKey: String = DEFAULT_CLIENT_INSTANCE) {
        clientMap.remove(clientInstanceKey)
    }

    fun clearMutationCache() {
        clientMap.values.forEach { client ->
            runCatching {
                client.clearCaches(
                    ClearCacheOptions.builder().clearMutations().build()
                )
            }.onFailure {
                Timber.e(it, "ClientFactory-clearMutationCache")
            }
        }
    }
}
package com.ncs.breeze.ui.dashboard.fragments.erpdetail

import android.content.res.Resources
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import com.facebook.react.bridge.Arguments
import com.google.android.gms.maps.model.LatLng
import com.mapbox.geojson.Point
import com.mapbox.maps.CameraOptions
import com.mapbox.maps.EdgeInsets
import com.mapbox.maps.MapView
import com.mapbox.maps.MapboxMap
import com.mapbox.maps.plugin.LocationPuck2D
import com.mapbox.maps.plugin.animation.camera
import com.mapbox.maps.plugin.animation.easeTo
import com.mapbox.maps.plugin.locationcomponent.LocationComponentPlugin
import com.mapbox.maps.plugin.locationcomponent.LocationProvider
import com.mapbox.maps.plugin.locationcomponent.location
import com.mapbox.navigation.ui.maps.camera.NavigationCamera
import com.mapbox.navigation.ui.maps.camera.data.MapboxNavigationViewportDataSource
import com.ncs.breeze.App
import com.ncs.breeze.R
import com.ncs.breeze.common.analytics.Screen
import com.ncs.breeze.common.constant.RNScreen
import com.ncs.breeze.common.event.ToAppRx
import com.ncs.breeze.common.extensions.android.getRNFragmentNavigationParams
import com.ncs.breeze.common.extensions.mapbox.setBreezeDefaultOptions
import com.breeze.model.api.response.ERPResponseData
import com.ncs.breeze.common.model.rx.ToAppRxEvent
import com.ncs.breeze.common.remote.MyServiceInterceptor
import com.ncs.breeze.common.rxbus.RxBus
import com.ncs.breeze.common.rxbus.RxEvent
import com.breeze.model.constants.Constants
import com.ncs.breeze.common.utils.ERPControllerData
import com.breeze.model.extensions.safeDouble
import com.ncs.breeze.components.marker.markerview2.ERPMarkerViewManager
import com.ncs.breeze.components.marker.markerview2.ERpMarkerView
import com.ncs.breeze.components.marker.markerview2.SearchLocationMarkerViewManager
import com.ncs.breeze.databinding.FragmentErpDetailBinding
import com.ncs.breeze.reactnative.nativemodule.ShareBusinessLogicEvent
import com.ncs.breeze.ui.base.BaseActivity
import com.ncs.breeze.ui.base.BaseFragment
import com.ncs.breeze.ui.dashboard.manager.DashboardMapStateManager
import com.ncs.breeze.ui.rn.CustomReactFragment
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject


class ErpDetailFragment : BaseFragment<FragmentErpDetailBinding, ErpFragmentViewModel>() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var myServiceInterceptor: MyServiceInterceptor

    private val mViewModel: ErpFragmentViewModel by viewModels {
        viewModelFactory
    }

    var compositeDisposable: CompositeDisposable = CompositeDisposable()


    /**
     *for showing current location
     */
    private lateinit var defaultLocationPuck: LocationPuck2D
    private lateinit var locationComponent: LocationComponentPlugin
    private var defaultLocationProvider: LocationProvider? = null
    private var markerSearchErp: ERpMarkerView? = null

    /**
     * for ERP
     */
    var erpMarkerViewManager: ERPMarkerViewManager? = null

    private var idERP: Int = -1

    private lateinit var mapboxMap: MapboxMap
    private lateinit var navigationCamera: NavigationCamera
    private lateinit var viewportDataSource: MapboxNavigationViewportDataSource

    private val pixelDensity = Resources.getSystem().displayMetrics.density


    /**
     * for ERP
     */
    var searchLocationMarkerViewManager: SearchLocationMarkerViewManager? = null

    companion object {
        internal fun newInstance() = ErpDetailFragment()
        val ERP_ID = "erp_id"
    }

    var currentErpSearch: ERPResponseData.Erp? = null

    private val carEventListener =
        ToAppRx.listenEvent(ToAppRxEvent::class.java)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
            }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        arguments?.let {
            idERP = arguments?.getInt(ERP_ID) ?: -1
        }

        findViews(view)

        initialize()

        loadingView.visibility = View.VISIBLE

        //filter & get data ERP

        /**
         * filter erp in list
         */
        currentErpSearch = ERPControllerData.getERPByID(idERP)

        currentErpSearch?.let { erp ->
            markerSearchErp = getERPMarker(erp)
            markerSearchErp!!.showMarker()
            markerSearchErp!!.showTooltip()

            erpMarkerViewManager?.addMarker(markerSearchErp!!)
            val cameraOptions = CameraOptions.Builder()
                .center(
                    Point.fromLngLat(
                        erp.startlong.safeDouble(),
                        erp.startlat.safeDouble()
                    )
                )
                .padding(
                    EdgeInsets(
                        10.0 * pixelDensity,
                        10.0 * pixelDensity,
                        160.0 * pixelDensity,
                        10.0 * pixelDensity
                    )
                )
                .zoom(15.0)
                .build()
            mapView.getMapboxMap().easeTo(cameraOptions)
        }

        showBottomSheetDetailInfo(idERP)

        loadingView.visibility = View.GONE
    }


    private fun showBottomSheetDetailInfo(idERP: Int) {
        val navigationParams = requireActivity().getRNFragmentNavigationParams(
            sessionToken = myServiceInterceptor.getSessionToken(),
            fragmentTag = RNScreen.ERP_DETAIL
        ).apply {
            putInt(Constants.RN_CONSTANTS.ERP_ID, idERP)
        }
        val initialProperties = Bundle().apply {
            putString(Constants.RN_TO_SCREEN, RNScreen.ERP_DETAIL)
            putBundle(Constants.RN_CONSTANTS.NAVIGATION_PARAMS, navigationParams)
        }
        val reactNativeFragment = CustomReactFragment.newInstance(
            componentName = Constants.RN_CONSTANTS.COMPONENT_NAME,
            launchOptions = initialProperties,
            cleanOnDestroyed = false
        )
        sendEventERPSelected(idERP)
        childFragmentManager
            .beginTransaction()
            .replace(R.id.frBottomDetail, reactNativeFragment, "erp_bottom_sheet")
            .commit()
    }

    lateinit var ll_heading: RelativeLayout
    lateinit var back_button: ImageView
    lateinit var header: TextView
    lateinit var select: TextView
    lateinit var mapView: MapView
    lateinit var loadingView: FrameLayout

    private fun findViews(view: View) {
        ll_heading = view.findViewById(R.id.ll_heading) as RelativeLayout
        back_button = view.findViewById(R.id.back_button) as ImageView
        header = view.findViewById(R.id.header) as TextView
        select = view.findViewById(R.id.select) as TextView
        mapView = view.findViewById(R.id.mapView) as MapView
        loadingView = view.findViewById(R.id.progressBar) as FrameLayout
    }

    fun initialize() {

        header.text = resources.getString(R.string.erp_detail)

        mapboxMap = mapView.getMapboxMap()

        erpMarkerViewManager = ERPMarkerViewManager(mapView)
        searchLocationMarkerViewManager = SearchLocationMarkerViewManager(mapView)

        viewportDataSource = MapboxNavigationViewportDataSource(
            mapView.getMapboxMap()
        )

        navigationCamera = NavigationCamera(
            mapView.getMapboxMap(),
            mapView.camera,
            viewportDataSource
        )

        mapView.setBreezeDefaultOptions()

        /**
         * load stype of map
         */
        initStyle()
        /**
         * init current location on map
         */
        initializeLocationComponent()

        back_button.setOnClickListener {
            onBackPressed()
        }

        erpRefreshListener()
    }

    //EDGEINSET
    private fun erpRefreshListener() {
        Timber.d("ErpDetailFragment ERP refresh listener is initialized")
        compositeDisposable.add(
            RxBus.listen(RxEvent.ERPRefresh::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    Timber.d("ErpDetailFragment ERP refresh listener is called")
                    if (context != null && !isDetached && !isPaused && !isFinishing) {
                        if (idERP != -1) {
                            showBottomSheetDetailInfo(idERP)
                            currentErpSearch?.let { erpValue ->
                                markerSearchErp?.fillDataToMarkerView(
                                    item = erpValue,
                                    eRPIconClick = {},
                                    timeToCheckERP = System.currentTimeMillis(),
                                    heightBottomSheet = DashboardMapStateManager.landingBottomSheetHeight,
                                    screenName = getScreenName(),
                                    forceShowErp = true,
                                    shouldSendEventERPSelected = false,
                                    forceCheckNextDayERP = false
                                )
                            }
                        }
                    }
                }
        )
    }

    private fun sendEventERPSelected(erpId: Int) {
        (activity?.applicationContext as? App)?.shareBusinessLogicPackage?.getRequestsModule()
            ?.flatMap {
                it.callRN(
                    ShareBusinessLogicEvent.SEND_EVENT_SELECTED_ERP.eventName,
                    Arguments.createMap().apply {
                        putInt("erpid", erpId)
                    })
            }?.subscribeOn(Schedulers.io())?.observeOn(AndroidSchedulers.mainThread())?.subscribe()
    }

    private fun initStyle(basicMapStyleReqd: Boolean = true, after: () -> Unit = {}) {
        (activity as? BaseActivity<*, *>)?.apply {
            mapboxMap.loadStyleUri(
                getMapboxStyle(basicMapStyleReqd)
            ) { style ->
                if (isFragmentDestroyed()) {
                    return@loadStyleUri
                }
                after()
            }
        }
    }


    private fun initializeLocationComponent() {
        locationComponent = mapView.location.apply {
            enabled = true
            pulsingEnabled = true
        }
        defaultLocationPuck = LocationPuck2D(
            bearingImage = activity?.getDrawable(R.drawable.cursor)
        )
        locationComponent.locationPuck = defaultLocationPuck

        defaultLocationProvider = locationComponent.getLocationProvider()
    }

    override fun inflateViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?,
        attachToContainer: Boolean
    ) = FragmentErpDetailBinding.inflate(inflater, container, attachToContainer)

    override fun getViewModelReference(): ErpFragmentViewModel {
        return mViewModel
    }

    override fun getScreenName(): String {
        return Screen.FRAGMENT_CRUISE_MODE
    }

    override fun onDestroyView() {
        super.onDestroyView()
        compositeDisposable.remove(carEventListener)
        compositeDisposable.dispose()
    }

//    override suspend fun generateNavigationLogData(filePath: String) {}

    private fun getERPMarker(item: ERPResponseData.Erp): ERpMarkerView {
        return ERpMarkerView(
            LatLng(item.startlat.safeDouble(), item.startlong.safeDouble()),
            mapboxMap,
            activity = requireActivity(),
        ).also {
            it.fillDataToMarkerView(
                item = item,
                eRPIconClick = {},
                timeToCheckERP = System.currentTimeMillis(),
                screenName = getScreenName(),
                forceShowErp = true,
                heightBottomSheet = DashboardMapStateManager.landingBottomSheetHeight,
                shouldSendEventERPSelected = false,
                forceCheckNextDayERP = false
            )
        }
    }
}
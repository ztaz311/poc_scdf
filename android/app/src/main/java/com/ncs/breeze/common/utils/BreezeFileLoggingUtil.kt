package com.ncs.breeze.common.utils

import android.content.Context
import android.content.pm.PackageInfo
import android.os.Environment
import android.util.Log
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.ktx.storage
import com.ncs.breeze.BuildConfig
import com.ncs.breeze.R
import com.ncs.breeze.common.extensions.android.getAppPreference
import com.breeze.model.FirebaseDB
import com.breeze.model.constants.Constants
import com.breeze.model.enums.DebugMode
import com.ncs.breeze.common.storage.AppPrefsKey
import fr.bipi.treessence.file.FileLoggerTree
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import timber.log.Timber
import timber.log.Timber.DebugTree
import java.io.File
import java.io.FileInputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale
import java.util.TimeZone
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class BreezeFileLoggingUtil @Inject constructor(
    var userPreferenceUtil: BreezeUserPreferenceUtil,
    var appContext: Context
) {
    var logcatProcess1: Process? = null
    var logcatProcess2: Process? = null

    companion object {
        val utcFormatter = SimpleDateFormat("yyyy-MM-dd_HH:mm:ss", Locale.US)
            .also { it.timeZone = TimeZone.getDefault() }

        const val DIRECTORY_NAME = "Breeze_logs"
    }

    // Create a storage reference from our app
    private var storageRef = Firebase.storage.reference

    // Create a child reference
    // imagesRef now points to "images"
    private var folderRef: StorageReference = storageRef.child(Constants.CLOUD_STORAGE_FOLDER)

    private fun isExternalStorageWritable(): Boolean {
        val state: String = Environment.getExternalStorageState()
        return Environment.MEDIA_MOUNTED.equals(state)
    }

    fun turnOffLog() {
        Timber.uprootAll()
        logcatProcess1?.destroy()
        logcatProcess2?.destroy()
    }

    fun enableLog(description: String) {
        if (!DebugMode.isDebugOn(
                appContext.getAppPreference()?.getInt(AppPrefsKey.DEBUG_MODE, if (BuildConfig.DEBUG) 1 else 0)
            )
        ) return

        Timber.d("Manage logs function called")
        if (isExternalStorageWritable()) {
            val fileDirectory = File(appContext.filesDir, DIRECTORY_NAME).absolutePath
            val logDirectory = File(fileDirectory)
            if (!logDirectory.exists()) {
                logDirectory.mkdirs()
            }

            if (logDirectory.exists()) {
                val files: Array<File>? = logDirectory.listFiles()

                if (files != null) {
                    Timber.d("Size: ${files.size}")
                    for (i in files.indices) {
                        Timber.d("FileName: ${files[i].name}")
                        if (!files[i].name.contains(".lck")) {
                            saveLogToFirebase(appContext, description, files[i])
                        } else {
                            files[i].delete()
                        }
                    }
                }

                /**
                 * Below code helps to capture all the logs that is usually printed in Logcat
                 * Useful when trying to debug a crash that was not logged in Firebase Crashlytics (Usually Mapbox crashes)
                 */

                val timeStampString = BreezeHistoryRecorderUtil.utcFormatter.format(Date())

                if (BuildConfig.LOGCAT_SYNC) {
                    val logFile = File(logDirectory, "Logcat_$timeStampString.txt")
                    // clear the previous logcat and then write the new one to the file
                    try {
                        //FixMe - Filter the log only for BuildConfig.APPLICATION_ID here.r
                        logcatProcess1 = Runtime.getRuntime().exec("logcat -c")
                        logcatProcess2 = Runtime.getRuntime().exec("logcat -f $logFile")
                    } catch (e: IOException) {
                        logcatProcess1 = null
                        logcatProcess2 = null
                    }
                }

                if (BuildConfig.TIMBER_LOG_SYNC) {
                    Timber.uprootAll()
                    val fileName = "Timber_logs_$timeStampString.log"
                    val t: Timber.Tree = FileLoggerTree.Builder()
                        .withFileName(fileName)
                        .withDirName(logDirectory.toString())
                        .withSizeLimit(FileLoggerTree.Builder.SIZE_LIMIT * 4)
                        .withFileLimit(8)
                        .withMinPriority(Log.DEBUG)
                        .appendToFile(true)
                        .build()
                    Timber.plant(t)
                    Timber.d("----- Timber log start -----")
                }
            }
        }
    }

    fun plantTimberDebugLog() {
        if (BuildConfig.DEBUG) {
            Timber.plant(DebugTree())
        }
    }

    private fun saveLogToFirebase(context: Context, description: String, file: File) {
        CoroutineScope(Dispatchers.IO).launch {

            var userId = (userPreferenceUtil.getUserStoredData()?.cognitoID ?: "").ifEmpty {
                appContext.getString(R.string.vehicle_type_private_car)
            }
            var fcmId = ""
            if (BuildConfig.FLAVOR != "production") {
                fcmId = context.getAppPreference()
                    ?.getString(AppPrefsKey.PUSH_MESSAGING_TOKEN) ?: ""
            }

            val file: File? = file
            if (file != null) {
                Timber.d("File is not null")
                val stream = FileInputStream(file)

                val fileRef = folderRef.child("${userId}/${file.name}")
                var uploadTask = fileRef.putStream(stream)
                val urlTask = uploadTask.continueWithTask { task ->
                    if (!task.isSuccessful) {
                        task.exception?.let {
                            Timber.d("Upload failed")
                            throw it
                        }
                    }
                    fileRef.downloadUrl
                }.addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        val downloadUri = task.result
                        Timber.d("Download URI is: $downloadUri")

                        val pInfo: PackageInfo =
                            context.packageManager.getPackageInfo(context.packageName, 0)
                        val version = pInfo.versionName
                        val buildNumber = pInfo.versionCode


                        val dbItem = FirebaseDB(
                            description = description,
                            fileDownloadURL = downloadUri.toString(),
                            deviceOS = "ANDROID",
                            deviceOSVersion = Utils.getDeviceOS(),
                            deviceModel = Utils.getDeviceModel(),
                            deviceManufacturer = Utils.getDeviceManufacturer(),
                            appVersion = String.format("V%s(%s)", version, buildNumber),
                            time = BreezeHistoryRecorderUtil.utcFormatter.format(Date()),
                            secondaryFirebaseID = "",
                            fcmId = fcmId,
                        )

                        Firebase.firestore.collection(userId).document(file.name).set(dbItem)
                            .addOnSuccessListener { _ ->
                                Timber.d("DocumentSnapshot written")
                            }
                            .addOnFailureListener { e ->
                                Log.w("testing-log", "Error adding document", e)
                            }
                        file.delete()


                    } else {
                        // Handle failures
                        // ...
                        file.delete()
                    }
                }
            }
        }
    }


}
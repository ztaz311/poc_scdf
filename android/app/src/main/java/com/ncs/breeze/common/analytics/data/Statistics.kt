package com.ncs.breeze.common.analytics.data

/**
 * Created by mobiledev on 28,June,2021
 */
data class Statistics(var batteryLevel: Int, val dataSent: Long, val dataReceived: Long)
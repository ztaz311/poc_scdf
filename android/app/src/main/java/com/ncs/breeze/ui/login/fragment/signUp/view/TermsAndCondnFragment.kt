package com.ncs.breeze.ui.login.fragment.signUp.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebSettings
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import com.ncs.breeze.BuildConfig
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.Event
import com.ncs.breeze.common.analytics.Screen
import com.ncs.breeze.common.storage.BreezeUserPreference
import com.breeze.model.constants.Constants
import com.ncs.breeze.common.utils.InitObjectUtilsController
import com.ncs.breeze.databinding.FragmentTncBinding
import com.ncs.breeze.ui.base.BaseFragment
import com.ncs.breeze.ui.login.activity.LoginActivity
import com.ncs.breeze.ui.login.fragment.signUp.viewmodel.TermsAndCondnViewModel
import com.ncs.breeze.ui.splash.SplashActivity
import javax.inject.Inject

class TermsAndCondnFragment : BaseFragment<FragmentTncBinding, TermsAndCondnViewModel>() {

    var isFromSignIn: Boolean = false
    var fromScreen: String? = ""

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val viewModel: TermsAndCondnViewModel by viewModels {
        viewModelFactory
    }


    companion object {
        val SCREEN_NAME = "TermsAndCondnFragment"

        val FROM_SCREEN = "FROM_SCREEN"

        val FROM_SIGN_IN = "FROM_SIGN_IN"
        val FROM_CREATE_ACCOUNT = "FROM_CREATE_ACCOUNT"

        val FROM_GUEST = "FROM_GUEST"

        @JvmStatic
        fun newInstance() =
            TermsAndCondnFragment().apply {
                arguments = Bundle().apply {

                }
            }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            isFromSignIn =
                if (requireArguments().containsKey(Constants.IS_FROM_SIGN_IN)) requireArguments().getBoolean(
                    Constants.IS_FROM_SIGN_IN,
                    false
                ) else false
            fromScreen =
                if (requireArguments().containsKey(FROM_SCREEN)) requireArguments().getString(
                    FROM_SCREEN,
                    ""
                ) else ""
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initialize()
    }

    private fun initialize() {
        viewBinding.webViewTnc.loadUrl(BuildConfig.TERMS_AND_CONDITIONS_URL)
        viewBinding.webViewTnc.getSettings().cacheMode = WebSettings.LOAD_NO_CACHE;

        viewBinding.backButton.setOnClickListener {
            when (fromScreen) {
                FROM_GUEST -> {
                    Analytics.logClickEvent(Event.TNC_BACK, Screen.ONBOARDING_GUEST_SIGNUP)
                }

                FROM_CREATE_ACCOUNT -> {
                    Analytics.logClickEvent(Event.TNC_BACK, Screen.ONBOARDING_CREATE_ACC_SIGNUP)
                }

                else -> {
                }
            }

            if (isFromSignIn) {
                (activity as LoginActivity).hideKeyboard()
            } else if (fromScreen.equals("splash")) {
                (activity as SplashActivity).declineTnc()
                onBackPressed()
            } else {
                (activity as LoginActivity).hideKeyboard()
                onBackPressed()
            }
        }
        viewBinding.btnDisagree.setOnClickListener {
            (activity as? LoginActivity)?.declineTnc()
            if (fromScreen == FROM_GUEST) {
                Analytics.logClickEvent(Event.TNC_DECLINE_GUEST, Screen.ONBOARDING_GUEST_SIGNUP)
            } else if (fromScreen == FROM_CREATE_ACCOUNT) {
                Analytics.logClickEvent(Event.TNC_BACK, Screen.ONBOARDING_CREATE_ACC_SIGNUP)
            }

        }
        viewBinding.btnAgree.setOnClickListener {
            when (fromScreen) {
                FROM_GUEST -> {
                    Analytics.logClickEvent(Event.TNC_AGREE, Screen.ONBOARDING_GUEST_SIGNUP)
                }

                FROM_CREATE_ACCOUNT -> {
                    Analytics.logClickEvent(Event.TNC_BACK, Screen.ONBOARDING_CREATE_ACC_SIGNUP)
                }

                else -> {
                    Analytics.logClickEvent(Event.TNC_ACCEPT, getScreenName())
                }
            }

            BreezeUserPreference.getInstance(requireContext()).saveTncStatus(true)
            /**
             * update term of condition to backend
             */
            viewModel.updateTncStatus()

            when (fromScreen) {
                FROM_SIGN_IN, FROM_GUEST -> {
                    if (InitObjectUtilsController.currentObjectFlowLoginSignUp?.isUserNameAvailable == false) {
                        (activity as? LoginActivity)?.openCreateUserNameScreen(FROM_GUEST)
                    } else {
                        /**
                         * show dialog success login
                         */
                        (activity as? LoginActivity)?.showDialogSuccess(
                            isExistUser = false,
                            fromScreen ?: TermsAndCondnFragment.FROM_GUEST
                        )
                    }
                }

                FROM_CREATE_ACCOUNT -> {
                    (activity as? LoginActivity)?.openCreateAccountBreezeScreen()
                }
            }
        }
    }

    override fun inflateViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?,
        attachToContainer: Boolean
    ) = FragmentTncBinding.inflate(inflater, container, attachToContainer)

    override fun getViewModelReference(): TermsAndCondnViewModel {
        return viewModel
    }

    override fun getScreenName(): String {
        return Screen.ONBOARDING_TNC
    }
}
package com.ncs.breeze.ui.dashboard.fragments.obulite

import android.app.Activity
import android.content.res.Resources
import android.location.Location
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.gson.JsonObject
import com.mapbox.geojson.Feature
import com.mapbox.geojson.Point
import com.mapbox.maps.CameraOptions
import com.mapbox.maps.EdgeInsets
import com.mapbox.maps.MapView
import com.mapbox.maps.MapboxMap
import com.mapbox.maps.plugin.gestures.gestures
import com.mapbox.navigation.core.lifecycle.MapboxNavigationApp
import com.mapbox.navigation.core.trip.session.TripSessionState
import com.mapbox.navigation.ui.maps.camera.data.MapboxNavigationViewportDataSource
import com.mapbox.turf.TurfConstants
import com.mapbox.turf.TurfMeasurement
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.Event
import com.ncs.breeze.common.analytics.Screen
import com.ncs.breeze.common.extensions.mapbox.updateDestinationAndLocationPuckLayer
import com.breeze.model.api.response.amenities.BaseAmenity
import com.breeze.model.constants.AmenityType.CARPARK
import com.breeze.model.constants.AmenityType.EVCHARGER
import com.breeze.model.constants.AmenityType.PETROL
import com.ncs.breeze.common.utils.BreezeMapZoomUtil
import com.breeze.model.constants.Constants
import com.ncs.breeze.components.layermanager.MarkerLayerManager
import com.ncs.breeze.components.layermanager.impl.cluster.ClusteredMarkerLayerManager
import com.ncs.breeze.components.marker.markerview2.AmenitiesMarkerView
import com.ncs.breeze.components.marker.markerview2.MarkerView2
import com.ncs.breeze.components.marker.markerview2.carpark.SimpleCarParkMarkerView
import com.ncs.breeze.helper.carpark.CarParkDestinationIconManager
import com.ncs.breeze.helper.marker.DashBoardAmenityMarkerHelper
import com.ncs.breeze.ui.dashboard.activity.DashboardActivity
import com.ncs.breeze.ui.dashboard.manager.DashboardMapStateManager
import io.reactivex.disposables.CompositeDisposable
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import timber.log.Timber

class OBULiteHelper {

    lateinit var mapboxMap: MapboxMap
    lateinit var mapView: MapView
    lateinit var context: Activity
    lateinit var viewportDataSource: MapboxNavigationViewportDataSource
    private val pixelDensity = Resources.getSystem().displayMetrics.density

    private val overviewEdgeInsets: EdgeInsets by lazy {
        EdgeInsets(
            10.0 * pixelDensity,
            10.0 * pixelDensity,
            8.0 * pixelDensity,
            10.0 * pixelDensity
        )
    }

    var activeLocationUse: Location? = null
    var isShowingCarParkMarkers = false
    var markerLayerClickHandlerEnabled = true
    var markerLayerClickFromCruise = false

    private var listCarkPark: ArrayList<BaseAmenity>? = null
    private var listPetrol: ArrayList<BaseAmenity>? = null
    private var listEVCharger: ArrayList<BaseAmenity>? = null
    private var currentAmenityType: String? = null
    private var showDestination: Boolean = false
    var hasDestinationCarPark: Boolean = false

    var compositeDisposable: CompositeDisposable = CompositeDisposable()
    var markerTooltipView: MarkerView2? = null


    /**
     * for carpark destination
     */
    var carParkDestinationIconManager: CarParkDestinationIconManager? = null

    /**
     * for manager all marker of amenities
     */
    var amenitiesMarkerViewManager: MarkerLayerManager? = null
    var amenityMarkerViewHelper: DashBoardAmenityMarkerHelper? = null

    var handleNavigationToCarPark: ((BaseAmenity) -> Unit)? = null
    var handleDismissCamera: (() -> Unit)? = null
    var nightModeEnabled: Boolean = false

    /**
     * NOTE:
     * destination icon manager map clicks has more precedence and should be initialized
     * before other amenities managers if required
     */
    fun setUp(
        pMapView: MapView,
        pContext: Activity,
        nightModeEnabled: Boolean,
        markerLayerClickHandlerEnabled: Boolean = true,
        markerLayerClickFromCruise: Boolean = false,
        pHandleNavigationToCarPark: ((BaseAmenity) -> Unit)? = null,
        pHandleDismissCamera: (() -> Unit)? = null
    ) {
        this.handleDismissCamera = pHandleDismissCamera
        this.handleNavigationToCarPark = pHandleNavigationToCarPark
        context = pContext
        mapView = pMapView
        this.nightModeEnabled = nightModeEnabled
        mapboxMap = pMapView.getMapboxMap()
        viewportDataSource = MapboxNavigationViewportDataSource(
            mapView.getMapboxMap()
        )

        mapView.gestures.addOnMapClickListener {
            /**
             * reset current location search here
             */
            DashboardMapStateManager.resetCurrentLocationFocus()

            false
        }

        carParkDestinationIconManager =
            CarParkDestinationIconManager(context, mapView)

        /**
         * AmenitiesMarkerViewManager  for control all marker in home
         */
        amenitiesMarkerViewManager =
            MarkerLayerManager(mapView, nightMode = nightModeEnabled)
        //TODO: this can be more optimized ( by setting markerLayerClickHandler as null based on condition)
        this.markerLayerClickHandlerEnabled = markerLayerClickHandlerEnabled
        this.markerLayerClickFromCruise = markerLayerClickFromCruise

        amenityMarkerViewHelper =
            DashBoardAmenityMarkerHelper(amenitiesMarkerViewManager!!, nightModeEnabled, true)
        amenitiesMarkerViewManager!!.markerLayerClickHandler = markerLayerClickHandler
        CoroutineScope(Dispatchers.Main).launch {
            amenitiesMarkerViewManager!!.addStyleImages(arrayListOf(CARPARK, PETROL, EVCHARGER))
        }
    }

    fun checkLocationAmenities(location: Location, actionIfNoAmenitiesInView: () -> Unit) {
        val currentPoint = Point.fromLngLat(location.longitude, location.latitude)
        val options = CameraOptions.Builder()
            .zoom(mapboxMap.cameraState.zoom)
            .center(mapboxMap.cameraState.center)
            .build()
        val shouldCheckAmenity =
            mapboxMap.coordinateBoundsForCamera(options).contains(currentPoint, false)
        Timber.e("checkLocationAmenities: $shouldCheckAmenity")
        if (shouldCheckAmenity) {
            val listAmenities: ArrayList<BaseAmenity>? = when (currentAmenityType) {
                CARPARK -> listCarkPark
                PETROL -> listPetrol
                EVCHARGER -> listEVCharger
                else -> return
            }
            val itemInView = listAmenities?.find {
                if (it.long != null && it.lat != null) {
                    val p = Point.fromLngLat(it.long!!, it.lat!!)
                    mapboxMap.coordinateBoundsForCamera(options).contains(p, false)
                } else {
                    false
                }
            }
            if (itemInView == null) actionIfNoAmenitiesInView()
            Timber.e("checkLocationAmenities: itemInView:$itemInView")

        }
    }

    /**
     * handle show carpark
     */
    fun handleCarkPark(
        mListCarkPark: ArrayList<BaseAmenity>,
        isMoveCameraWrapperAll: Boolean = false,
        showDestination: Boolean,
    ) {
        this.currentAmenityType = CARPARK
        this.showDestination = showDestination
        this.listCarkPark = mListCarkPark
        amenitiesMarkerViewManager!!.removeMarkerTooltipView(CARPARK)
        val locationAtParkingClick = activeLocationUse
        if (locationAtParkingClick != null) {
            updateDestinationCarparkState(showDestination, mListCarkPark)
            carParkDestinationIconManager!!.handleDestinationCarParkIcon(
                mListCarkPark,
                showDestination
            )
            amenityMarkerViewHelper!!.addAmenityMarkerView(
                CARPARK,
                mListCarkPark,
                false,
                showDestination
            )
            mapView.updateDestinationAndLocationPuckLayer()
            if (isMoveCameraWrapperAll) {
                //move camera to location search or current location
                activeLocationUse?.let {
                    zoomToAmenities(Point.fromLngLat(it.longitude, it.latitude), mListCarkPark)
                }
            }

            handleModeSelectionOnMap()
        }
    }

    fun handlePetrol(
        listPetrol: ArrayList<BaseAmenity>,
        isMoveCameraWrapperAll: Boolean = false,
        showDestination: Boolean
    ) {
        this.currentAmenityType = PETROL
        this.showDestination = showDestination
        this.listPetrol = listPetrol
        amenitiesMarkerViewManager!!.removeMarkerTooltipView(PETROL)
        Timber.e("getPetrolDataShowInMap-handlePetrol: $activeLocationUse")
        val locationAtParkingClick = activeLocationUse
        if (locationAtParkingClick != null) {
            updateDestinationCarparkState(showDestination, listPetrol)
            carParkDestinationIconManager!!.handleDestinationCarParkIcon(
                listPetrol,
                showDestination
            )
            amenityMarkerViewHelper!!.addAmenityMarkerView(
                PETROL,
                listPetrol,
                false,
                showDestination
            )
            mapView.updateDestinationAndLocationPuckLayer()
            if (isMoveCameraWrapperAll) {
                //move camera to location search or current location
                activeLocationUse?.let {
                    zoomToAmenities(Point.fromLngLat(it.longitude, it.latitude), listPetrol)
                }
            }

        }
    }

    fun handleEVCharger(
        listEV: ArrayList<BaseAmenity>,
        isMoveCameraWrapperAll: Boolean = false,
        showDestination: Boolean
    ) {
        this.currentAmenityType = EVCHARGER
        this.showDestination = showDestination
        this.listEVCharger = listEV
        amenitiesMarkerViewManager!!.removeMarkerTooltipView(EVCHARGER)

        val locationAtParkingClick = activeLocationUse
        if (locationAtParkingClick != null) {
            updateDestinationCarparkState(showDestination, listEV)
            carParkDestinationIconManager!!.handleDestinationCarParkIcon(
                listEV,
                showDestination
            )
            amenityMarkerViewHelper!!.addAmenityMarkerView(
                EVCHARGER,
                listEV,
                false,
                showDestination
            )
            mapView.updateDestinationAndLocationPuckLayer()
            if (isMoveCameraWrapperAll) {
                //move camera to location search or current location
                activeLocationUse?.let {
                    zoomToAmenities(Point.fromLngLat(it.longitude, it.latitude), listEV)
                }
            }
        }
    }

    private fun updateDestinationCarparkState(
        showDestination: Boolean,
        mListCarkPark: ArrayList<BaseAmenity>
    ) {
        if (showDestination) {
            hasDestinationCarPark = false
            for (mCarkParkItem in mListCarkPark) {
                if (mCarkParkItem.destinationCarPark == true) {
                    hasDestinationCarPark = true
                    break
                }
            }

        }
    }

    private fun handleModeSelectionOnMap() {
        if (isShowingCarParkMarkers) {
            hideAmenities()
            this.listCarkPark?.let {
                mapHandleToParkingMode(it)
            }
        }
    }

    fun mapHandleToParkingMode(mListCarkPark: ArrayList<BaseAmenity>) {
        (context as? DashboardActivity)?.onCameraTrackingDismissed()
        showClosestCarParktoolTip(mListCarkPark)
    }

    private fun showClosestCarParktoolTip(carParkAmenities: ArrayList<BaseAmenity>) {
        if (carParkAmenities.size > 0) {
            for (carParkAmenity in carParkAmenities) {
                if (carParkAmenity.destinationCarPark == true) {
                    showCarParkToolTipInCruiseMode(carParkAmenity)
                    return
                }
            }
        }
    }


    private fun showCarParkToolTipInCruiseMode(amenity: BaseAmenity) {

        val carParkMarkerView = SimpleCarParkMarkerView(
            LatLng(amenity.lat!!, amenity.long!!),
            mapboxMap,
            context,
            amenity.id
        )
        markerTooltipView = carParkMarkerView
        val properties = JsonObject()
        amenityMarkerViewHelper!!.handleSelectedCarParkProperties(
            amenity,
            properties,
            showDestination,
            false
        )
        val feature = Feature.fromGeometry(
            Point.fromLngLat(
                amenity.long!!,
                amenity.lat!!
            ), properties, amenity.id
        )

        amenitiesMarkerViewManager!!.renderMarkerToolTipView(
            MarkerLayerManager.MarkerViewType(
                carParkMarkerView,
                CARPARK,
                feature
            )
        )

        carParkMarkerView.clickToopTip(
            amenity,
            true,
            navigationButtonClick = { item, eventSource ->
                Analytics.logClickEvent(Event.NAVIGATE_HERE_PARKING, getScreenName())
//                val message = parseAnalyticsMessage(amenity)
//                Analytics.logEvent(Event.USER_CLICK,Event.OBU_CAR_PARK_SELECT,Screen.OBU_DISPLAY_MODE,message,amenity.name.toString(),amenity.long.toString(),amenity.lat.toString())

                if (item.long != null && item.lat != null) {
                    handleNavigationToCarPark?.invoke(item)
                }
            },
            moreInfoButtonClick = { item ->
                Analytics.logClickEvent(Event.PARKING_SEE_MORE, getScreenName())
            },
            carParkIconClick = {},
            getScreenName()
        )

//        /**
//         * move camera
//         */
        BreezeMapZoomUtil.recenterMapToLocation(
            mapboxMap,
            lat = amenity.lat!!.toDouble(),
            lng = amenity.long!!.toDouble(),
            edgeInset = overviewEdgeInsets,
            zoomRadius = Constants.DEFAULT_TOOLTIP_ZOOM_RADIUS
        )

    }

    private fun handleAmenityToolTip(amenity: BaseAmenity, type: String) {

        val amenitiesMarkerView = AmenitiesMarkerView(
            LatLng(amenity.lat!!, amenity.long!!),
            mapboxMap,
            context,
            amenity.id,
            amenity.amenityType,
            false,
            Constants.POI_TOOLTIP_ZOOM_RADIUS
        )
        markerTooltipView = amenitiesMarkerView
        val properties = JsonObject()
        amenityMarkerViewHelper!!.handleSelectedAmenityProperties(
            properties,
            false,
            amenity
        )
        properties.addProperty(ClusteredMarkerLayerManager.FEATURE_TYPE, amenity.amenityType)

        val feature = Feature.fromGeometry(
            Point.fromLngLat(
                amenity.long!!,
                amenity.lat!!
            ), properties, amenity.id
        )

        amenitiesMarkerViewManager!!.renderMarkerToolTipView(
            MarkerLayerManager.MarkerViewType(
                amenitiesMarkerView,
                type,
                feature
            )
        )

        amenitiesMarkerView.handleClickMarker(amenity,
            true, navigationButtonClick = { item, eventSource ->
                Timber.e("navigationButtonClick $item")
                if (item.amenityType == PETROL) {
                    Analytics.logClickEvent(
                        Event.OBU_DISPLAY_CLICK_MAP_PETROL_NAVIGATE,
                        getScreenName()
                    )
                } else {
                    Analytics.logClickEvent(
                        Event.OBU_DISPLAY_CLICK_MAP_EV_NAVIGATE,
                        getScreenName()
                    )
                }

                if (item.long != null && item.lat != null) {
                    handleNavigationToCarPark?.invoke(item)
                }
            },
            walkingIconClick = { _, _ -> },
            amenitiesIconClick = { _, _ -> },
            bookmarkIconClick = { _, _ -> })

        BreezeMapZoomUtil.recenterMapToLocation(
            mapboxMap,
            lat = amenity.lat!!.toDouble(),
            lng = amenity.long!!.toDouble(),
            edgeInset = overviewEdgeInsets,
            zoomRadius = Constants.DEFAULT_TOOLTIP_ZOOM_RADIUS
        )
    }

    private fun parseAnalyticsMessage(amenity: BaseAmenity): String {
        var message = Constants.OBU_ANALYTICS_MESSAGE.NORMAL
        if (amenity.isCheapest == true) {
            message = Constants.OBU_ANALYTICS_MESSAGE.CHEAPEST
        } else if (listCarkPark?.isNotEmpty() == true && listCarkPark!![0].id == amenity.id) {
            message = Constants.OBU_ANALYTICS_MESSAGE.NEAREST
        }
        return message
    }


    fun clearALlCarPark() {
        amenitiesMarkerViewManager?.removeMarkers(CARPARK)
        carParkDestinationIconManager?.showDestinationIcon()
        DashboardMapStateManager.resetCurrentLocationFocus()
    }

    fun clearAmenities(
        isClearParking: Boolean = true,
        isClearPetrol: Boolean = true,
        isClearEV: Boolean = true
    ) {
        if (isClearParking) {
            amenitiesMarkerViewManager?.removeMarkers(CARPARK)
            listCarkPark = null
        }
        if (isClearPetrol) {
            amenitiesMarkerViewManager?.removeMarkers(PETROL)
            listPetrol = null
        }
        if (isClearEV) {
            amenitiesMarkerViewManager?.removeMarkers(EVCHARGER)
            listEVCharger = null
        }
        carParkDestinationIconManager?.showDestinationIcon()
        DashboardMapStateManager.resetCurrentLocationFocus()
    }


    fun removeALlTooltip() {
        amenitiesMarkerViewManager?.removeMarkerTooltipView()
        DashboardMapStateManager.resetCurrentLocationFocus()
    }


    fun hideAllToolTipCarPark() {
        amenitiesMarkerViewManager?.removeMarkerTooltipView()
        DashboardMapStateManager.resetCurrentLocationFocus()
    }


    private fun hideAmenities() {
        amenitiesMarkerViewManager?.let {
            it.hideAllMarkerExcludingType(CARPARK)
        }
    }

    fun getScreenName(): String =
        if (MapboxNavigationApp.current()
                ?.getTripSessionState() == TripSessionState.STARTED
        ) Screen.HOME_CRUISE_MODE else Screen.HOME


    private fun zoomToDefault() {
        val radius: Double? = 600.0
        BreezeMapZoomUtil.recenterMapToLocation(
            mapboxMap,
            lat = activeLocationUse!!.latitude,
            lng = activeLocationUse!!.longitude,
            edgeInset = EdgeInsets(0.0,0.0,100.0,0.0),
            zoomRadius = radius
        )
    }


    private fun resetAllViews() {
        amenitiesMarkerViewManager!!.removeMarkerTooltipView()
    }

    val markerLayerClickHandler = object : MarkerLayerManager.MarkerLayerClickHandler {
        override fun handleOnclick(
            layerType: String,
            feature: Feature,
            currentMarker: MarkerLayerManager.MarkerViewType?
        ) {
            if (!markerLayerClickHandlerEnabled) {
                return
            }
            resetAllViews()
            if (currentMarker != null && currentMarker.feature.id() == feature.id()) {
                return
            }
            when (layerType) {
                CARPARK -> {

                    listCarkPark?.let {
                        val amenity = it.find { ba ->
                            return@find ba.id == feature.id()
                        }
                        amenity?.let { amenity ->
                            handleDismissCamera?.invoke()
                            showCarParkToolTipInCruiseMode(amenity)
                        }
                    }
                }

                else -> {
                    if (layerType == PETROL) {
                        Analytics.logClickEvent(Event.OBU_DISPLAY_CLICK_MAP_PETROL, getScreenName())
                        listPetrol
                    } else {
                        Analytics.logClickEvent(Event.OBU_DISPLAY_CLICK_MAP_EV, getScreenName())
                        listEVCharger
                    }?.let {

                        val amenity = it.find { ba -> ba.id == feature.id() }
                        amenity?.let {
                            handleAmenityToolTip(amenity, layerType)
                        }
                    }
                }

            }
        }

        override fun handleOnFreeMapClick(point: Point) {
            // no op
        }
    }

    private fun zoomToAmenities(currentLocation: Point, amenities: List<BaseAmenity>) {
        if (amenities.isEmpty()) {
            return
        }
        var farthestDistance = 200.0
        var farthestCarpark: BaseAmenity? = amenities[0]
        amenities.forEach { amenity ->
            if (amenity.long != null && amenity.lat != null) {
                val distance = TurfMeasurement.distance(
                    currentLocation,
                    Point.fromLngLat(amenity.long!!, amenity.lat!!),
                    TurfConstants.UNIT_METERS
                )
                if (distance > farthestDistance) {
                    farthestDistance = distance
                    farthestCarpark = amenity
                }
            }
        }

        val center = LatLngBounds.Builder()
            .include(LatLng(currentLocation.latitude(), currentLocation.longitude()))
            .include(LatLng(farthestCarpark?.lat!!, farthestCarpark?.long!!)).build().center

        BreezeMapZoomUtil.recenterMapToLocation(
            mapboxMap,
            lat = center.latitude,
            lng = center.longitude,
            edgeInset = EdgeInsets(0.0, 0.0, 0.0, 0.0),
            zoomRadius = (farthestDistance / 2) + 100
        )
    }


}
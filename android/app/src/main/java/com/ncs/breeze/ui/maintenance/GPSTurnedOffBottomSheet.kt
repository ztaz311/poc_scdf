package com.ncs.breeze.ui.maintenance

import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.ActivityResult
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import com.ncs.breeze.common.utils.Utils
import com.ncs.breeze.R
import com.ncs.breeze.components.CustomBottomSheetFragment
import com.ncs.breeze.databinding.LayoutGpsTurnOffBinding
import timber.log.Timber

class GPSTurnedOffBottomSheet : CustomBottomSheetFragment() {

    companion object {
        const val TAG = "GPS_TURNED_OFF"
    }

    private lateinit var viewBinding: LayoutGpsTurnOffBinding

    private var openLocationSettings:ActivityResultLauncher<Intent>? = null


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        openLocationSettings =
            registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
                context?.let {
                    if (Utils.checkGPSEnabled(it)) {
                        dismiss()
                    }
                }
            }
        viewBinding = LayoutGpsTurnOffBinding.inflate(inflater, container, false)
        return viewBinding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewBinding.buttonTurnOn.setOnClickListener {
            AlertDialog.Builder(it.context, android.R.style.Theme_Material_Dialog_NoActionBar)
                .setMessage(R.string.gps_popup_msg)
                .setPositiveButton(
                    R.string.gps_popup_positive_btn_text
                ) { dialog, which ->
                    if (lifecycle.currentState.isAtLeast(Lifecycle.State.RESUMED)) {
                        openLocationSettings?.launch(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
                    }

                }.show()
        }
    }

    override fun onResume() {
        super.onResume()
        Timber.d("Fragment on resume called")
        context?.let {
            if (Utils.checkGPSEnabled(it))
                dismiss()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        openLocationSettings = null
    }
}
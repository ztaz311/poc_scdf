package com.ncs.breeze.components.layermanager.impl

import android.content.Context
import androidx.annotation.UiThread
import com.ncs.breeze.common.helper.route.WaypointManager
import com.mapbox.maps.MapView
import com.mapbox.maps.MapboxMap
import com.mapbox.maps.Style
import com.ncs.breeze.components.layermanager.MarkerLayerManager


/**
 * this class for manager all marker filter on route planning
 */
open class WaypointAmenitiesMarkerLayerManager(
    mapView: MapView,
    val waypointManager: WaypointManager,
    nightMode: Boolean = false
) : MarkerLayerManager(mapView, nightMode = nightMode) {

    @UiThread
    fun removeNonWaypointMarkers(type: String) {
        val featureCollection = markerFeatures[type] ?: return
        featureCollection.features()!!.removeIf { feature ->
            feature.id()?.let {
                if (waypointManager.getWaypoint(it) != null) {
                    return@removeIf false
                }
            }
            return@removeIf true
        }
        updateSourceGeoJson(type, featureCollection)
    }


    /**
     * hide all tooltips
     */
    @UiThread
    fun hideAllTooltips() {
        marker?.markerView2?.hideAllToolTips()
    }


    /**
     * hide all tooltips
     */
    @UiThread
    fun hideNonWaypointMarkers() {
        marker?.markerView2?.hideMarker()
        markerFeatures.forEach { map ->
            hideNonWaypointMarkers(map.key)
        }
    }

    /**
     * hide all tooltips
     */
    @UiThread
    fun hideNonWaypointMarkers(type: String) {
        val featureCollection = markerFeatures[type] ?: return
        featureCollection.features()!!.forEach { feature ->
            feature.id()?.let {
                if (waypointManager.getWaypoint(it) != null) {
                    return@forEach
                }
            }
            feature.addBooleanProperty(HIDDEN, true)
        }
        updateSourceGeoJson(type, featureCollection)
    }

    override fun deselectImage(markerViewType: MarkerViewType) {
        markerViewType.feature.id()?.let { id ->
            if (waypointManager.getWaypoint(id) != null) {
                return
            }
        }
        super.deselectImage(markerViewType)
    }


}
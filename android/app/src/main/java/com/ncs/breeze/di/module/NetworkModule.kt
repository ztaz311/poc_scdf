package com.ncs.breeze.di.module


import android.content.Context
import com.ncs.breeze.common.remote.ApiHelper
import com.ncs.breeze.common.remote.MyServiceInterceptor
import com.breeze.model.constants.Constants
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.ncs.breeze.BuildConfig
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.io.File
import java.util.concurrent.TimeUnit
import javax.inject.Singleton


/**
 * Module which provides all required dependencies about network
 */
@Module
// Safe here as we are dealing with a Dagger 2 module
@Suppress("unused")
object NetworkModule {

    //TODO: revise connection and read timeouts
    @Provides
    @Singleton
    internal fun provideHttpClient(
        logger: HttpLoggingInterceptor,
        cache: Cache,
        myServiceInterceptor: MyServiceInterceptor
    ): OkHttpClient = OkHttpClient().newBuilder()
        .retryOnConnectionFailure(true)
        .cache(cache)
        .connectTimeout(60, TimeUnit.SECONDS)
        .readTimeout(60, TimeUnit.SECONDS)
        .writeTimeout(15, TimeUnit.SECONDS)
        .addInterceptor(logger)
        .addInterceptor(myServiceInterceptor)
        .build()


    @Provides
    @Singleton
    internal fun provideInterceptor(): HttpLoggingInterceptor =
        HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY }

    @Provides
    @Singleton
    internal fun provideCache(file: File): Cache {
        return Cache(file, (10 * 1024 * 1024).toLong())
    }

    @Provides
    @Singleton
    internal fun provideCacheFile(context: Context): File {
        return context.filesDir
    }

    @Provides
    @Singleton
    internal fun provideMyServiceInterceptor(
        context: Context,
    ): MyServiceInterceptor {
        return MyServiceInterceptor(context)
    }

    @Provides
    @Singleton
    internal fun provideGson(): Gson {
        val gsonBuilder = GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss")
        gsonBuilder.setLenient()
        return gsonBuilder.create()
    }

    @Provides
    @Singleton
    internal fun provideGsonClient(gson: Gson): GsonConverterFactory {

        return GsonConverterFactory.create(gson)
    }

    @Provides
    @Singleton
    internal fun provideRxAdapter(): RxJava2CallAdapterFactory {
        return RxJava2CallAdapterFactory.create()
    }

    @Provides
    internal fun provideApiService(
        client: OkHttpClient,
        gson: GsonConverterFactory,
        rxAdapter: RxJava2CallAdapterFactory
    ): ApiHelper {
        val retrofit = Retrofit.Builder()
            .client(client)
            .addConverterFactory(ScalarsConverterFactory.create())

            .baseUrl(BuildConfig.SERVER_HEADER)

            .addConverterFactory(gson)

            .addCallAdapterFactory(rxAdapter)
            .build()

        return retrofit.create(ApiHelper::class.java)
    }
}
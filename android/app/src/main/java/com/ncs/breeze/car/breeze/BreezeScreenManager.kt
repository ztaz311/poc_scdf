package com.ncs.breeze.car.breeze

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Handler
import android.os.Looper
import androidx.car.app.CarToast
import androidx.car.app.Screen
import androidx.car.app.ScreenManager
import androidx.lifecycle.Lifecycle
import com.breeze.model.enums.CarParkViewOption
import com.mapbox.androidauto.internal.logAndroidAuto
import com.ncs.breeze.App
import com.ncs.breeze.car.breeze.base.BaseNavigationScreenCar
import com.ncs.breeze.car.breeze.base.BaseScreenCar
import com.ncs.breeze.car.breeze.screen.amenity.AmenityScreen
import com.ncs.breeze.car.breeze.screen.carkpark.CarParkCarContext
import com.ncs.breeze.car.breeze.screen.carkpark.CarParkScreen
import com.ncs.breeze.car.breeze.screen.homescreen.HomeCarScreen
import com.ncs.breeze.car.breeze.screen.navigation.CarNavigationCarContext
import com.ncs.breeze.car.breeze.screen.navigation.NavigationScreen
import com.ncs.breeze.car.breeze.screen.needlogin.NeedsLoginScreen
import com.ncs.breeze.car.breeze.screen.obulite.CarOBULiteScreen
import com.ncs.breeze.car.breeze.screen.obulite.OBULiteCarContext
import com.ncs.breeze.car.breeze.screen.parkingupdates.ParkingAvailabilityScreen
import com.ncs.breeze.car.breeze.screen.routeplanning.RoutePlanningScreen
import com.ncs.breeze.car.breeze.screen.routeplanning.RoutePreviewCarContext
import com.ncs.breeze.car.breeze.screen.search.searchscreen.SearchScreen
import com.ncs.breeze.car.breeze.screen.travel_time.CarTravelTimeScreen
import com.ncs.breeze.car.breeze.utils.CarConstants
import com.ncs.breeze.car.breeze.utils.NavigationNotificationUtils
import com.ncs.breeze.common.event.ToCarRx
import com.ncs.breeze.common.helper.obu.OBUDataHelper
import com.ncs.breeze.common.model.rx.AppEventLogout
import com.ncs.breeze.common.model.rx.AppEventPermissionsGranted
import com.ncs.breeze.common.model.rx.AppOnPauseEvent
import com.ncs.breeze.common.model.rx.AppOnResumeEvent
import com.ncs.breeze.common.model.rx.AppSessionReadyEvent
import com.ncs.breeze.common.model.rx.AppToCarParkEndEvent
import com.ncs.breeze.common.model.rx.AppToCarParkEvent
import com.ncs.breeze.common.model.rx.AppToHomePageEvent
import com.ncs.breeze.common.model.rx.AppToHomeTurnOffCruiseModeEvent
import com.ncs.breeze.common.model.rx.AppToHomeTurnOnCruiseModeEvent
import com.ncs.breeze.common.model.rx.AppToNavigationEndEvent
import com.ncs.breeze.common.model.rx.AppToNavigationStartEvent
import com.ncs.breeze.common.model.rx.AppToRoutePlanningEndEvent
import com.ncs.breeze.common.model.rx.AppToRoutePlanningStartEvent
import com.ncs.breeze.common.model.rx.CarERPRefresh
import com.ncs.breeze.common.model.rx.CarIncidentDataRefresh
import com.ncs.breeze.common.model.rx.CarOpenAmenityScreen
import com.ncs.breeze.common.model.rx.CarToastEventData
import com.ncs.breeze.common.model.rx.IgnoredEvent
import com.ncs.breeze.common.model.rx.NavigationCongestionAlertEventShow
import com.ncs.breeze.common.model.rx.NavigationNotificationEventReset
import com.ncs.breeze.common.model.rx.NavigationNotificationEventShow
import com.ncs.breeze.common.model.rx.NavigationParkingAvailabilityAlertEventShow
import com.ncs.breeze.common.model.rx.OBUCardBalanceChangedEvent
import com.ncs.breeze.common.model.rx.OBUDisplayLifecycleEvent
import com.ncs.breeze.common.model.rx.OBUStateChangedEvent
import com.ncs.breeze.common.model.rx.OpenSearchScreenEvent
import com.ncs.breeze.common.model.rx.ToCarRxEvent
import com.ncs.breeze.common.model.rx.TriggerCarToastEvent
import com.ncs.breeze.common.utils.eta.ETAEngine
import com.ncs.breeze.common.analytics.Screen as AnalyticScreen


/**
 * this class for manager variable and switch screen
 */
class BreezeScreenManager(val mainCarContext: MainBreezeCarContext) {
    var screenManager: ScreenManager =
        mainCarContext.carContext.getCarService(ScreenManager::class.java)

    init {
        NavigationNotificationUtils.setUpNotification(mainCarContext)
    }

    fun handleEvent(event: ToCarRxEvent?) {
        when (event) {
            is OpenSearchScreenEvent -> {
                logAndroidAuto("OpenSearchScreenEvent")
                val top: Screen = screenManager.top
                if (top is SearchScreen) {
                    (top as? SearchScreen)?.handleSearchWithKey(event.data.keySearch)
                } else if (top !is NavigationScreen) {
                    screenManager.push(SearchScreen(mainCarContext, event.data.keySearch))
                }
            }

            is AppToHomePageEvent -> {
                val screenOntop = screenManager.top
                screenManager.push(HomeCarScreen(mainCarContext))
                screenManager.remove(screenOntop)
            }

            is AppSessionReadyEvent -> {
                logAndroidAuto("AppSessionReadyEvent")
                val screenOntop = screenManager.top
                mainCarContext.phoneAppLoggedIn = true
                if (screenOntop !is CarOBULiteScreen && screenOntop !is NavigationScreen) {
                    screenManager.push(HomeCarScreen(mainCarContext))
                    event.intent?.let { intent ->
                        openSearch(intent)?.let {
                            screenManager.push(it)
                        }
                    }
                    screenManager.remove(screenOntop)
                }
            }

            is AppOnPauseEvent -> {
                //ToDo - Handle mobile app pause event here in car app
            }

            is AppOnResumeEvent -> {
                //ToDo - Handle mobile app resume event here in car app
            }

            is AppToHomeTurnOffCruiseModeEvent -> {
                if (screenManager.top !is RoutePlanningScreen) {
                    screenManager.popTo(CarConstants.HOME_MARKER)
                    if (screenManager.top !is HomeCarScreen) {
                        screenManager.push(HomeCarScreen(mainCarContext))
                    }
                }
            }

            is AppToRoutePlanningStartEvent -> {
                screenManager.popToRoot()
                if (event.data != null) {
                    screenManager.push(
                        RoutePlanningScreen(
                            RoutePreviewCarContext(mainCarContext),
                            event.data
                        )
                    )
                } else {
                    //todo show something alert to user
                }

            }

            is AppToRoutePlanningEndEvent -> {
                screenManager.popTo(CarConstants.HOME_MARKER)
                if (screenManager.top !is HomeCarScreen) {
                    screenManager.push(HomeCarScreen(mainCarContext))
                }
            }

            is AppToNavigationStartEvent -> {
                //screenManager.popToRoot()
                ETAEngine.updateETPRequest(event.data.etaRequest)
                //fixme: should have better mechanism to synchonize state between car and app
                Handler(Looper.getMainLooper()).postDelayed({
                    screenManager.popTo(CarConstants.HOME_MARKER)
                    screenManager.push(
                        NavigationScreen(
                            CarNavigationCarContext(
                                mainCarContext,
                                event.data.originalDestination,
                                event.data.destinationAddressDetails,
                                event.data.route,
                                event.data.compressedDirectionsResponse,
                                event.data.originalDestinationResponse,
                                event.data.erpData,
                                event.data.etaRequest
                            )
                        )
                    )
                }, if (event.data.isFromCruiseMode) 1000 else 0)
            }

            is AppToNavigationEndEvent -> {
                NavigationNotificationUtils.cancelTurnByTurnNavigationNotification()
                screenManager.popTo(CarConstants.HOME_MARKER)
                if (screenManager.top !is HomeCarScreen) {
                    screenManager.push(HomeCarScreen(mainCarContext))
                }
                //screenManager.popToRoot() //ToDo - Ankur
            }

            is NavigationNotificationEventShow -> {
//                if (System.currentTimeMillis() - lastShow < 4000) return
                NavigationNotificationUtils.showNavigationNotification(
                    event.data,
                    event.roadObjectId,
                    event.zone,
                    event.distanceRaw,
                    event.location
                )

            }

            is NavigationCongestionAlertEventShow -> {
                val congestionAlertScreen = CarTravelTimeScreen(mainCarContext, event.data)
                screenManager.push(congestionAlertScreen)
                ToCarRx.postEventWithCacheLast(IgnoredEvent())
            }

            is NavigationParkingAvailabilityAlertEventShow -> {
                val parkingUpdatesScreen = ParkingAvailabilityScreen(mainCarContext, event.data)
                screenManager.push(parkingUpdatesScreen)
                ToCarRx.postEventWithCacheLast(IgnoredEvent())
            }

            is NavigationNotificationEventReset -> {
                NavigationNotificationUtils.resetNavigationNotification(event.roadObjectId)
            }

            is CarERPRefresh -> {
                if (screenManager.top is BaseNavigationScreenCar) {
                    (screenManager.top as BaseNavigationScreenCar).updateERPLayer(event.erpFeature);
                }
            }

            is CarIncidentDataRefresh -> {
                if (screenManager.top is BaseNavigationScreenCar) {
                    (screenManager.top as BaseNavigationScreenCar).updateIncidentLayer(event.incidentData);
                }
            }

            is AppToCarParkEvent -> {

                // If item list in the PlaceListNavigationTemplate changes (aka 4 car parks vs 6), it will not refresh the component,
                // instead it adds it as a new one increasing the stack
                // "From Docs: Refreshes only when the template title has not changed, and the number of rows and the title (not counting spans) of each row between the previous,
                // and new ItemLists have not changed."
                // So we need to pop and push again
                if (event.typeCarPark != CarParkViewOption.HIDE) {
                    if (screenManager.top !is HomeCarScreen) {
                        screenManager.popTo(CarConstants.HOME_MARKER)
                    }
                    Handler(Looper.getMainLooper()).post {
                        screenManager.push(
                            CarParkScreen(
                                CarParkCarContext(mainCarContext),
                                originalDestination = null,
                                fromScreen = AnalyticScreen.ANDROID_AUTO_HOMEPAGE
                            )
                        )
                    }
                } else {
                    (screenManager.top as? CarParkScreen)?.finish()
                }
            }

            is AppToCarParkEndEvent -> {
                screenManager.popTo(CarConstants.HOME_MARKER)
                if (screenManager.top !is HomeCarScreen) {
                    screenManager.push(HomeCarScreen(mainCarContext))
                }
            }

            is AppEventLogout, is AppEventPermissionsGranted -> {
                screenManager.popToRoot()
                val screenOntop = screenManager.top
                mainCarContext.phoneAppLoggedIn = false
                screenManager.push(NeedsLoginScreen(mainCarContext))
                screenManager.remove(screenOntop)
            }

            is TriggerCarToastEvent -> {
                showCarToast(event.data)
            }

            is OBUStateChangedEvent -> {

                val top = screenManager.top
                if (top is BaseNavigationScreenCar) {
                    top.changeOBUConnectionState(event.isConnected)
                }
            }

            is OBUCardBalanceChangedEvent -> {
                val top = screenManager.top
                if (top is BaseNavigationScreenCar) {
                    top.updateCardBalance(OBUDataHelper.getOBUCardBalanceSGD())
                }
            }

            is OBUDisplayLifecycleEvent -> {
                toggleOBULiteScreen(event.state)
            }

//            is SwitchThemeEvent -> {
//                mainCarContext.mapboxCarMap.
//                mainCarContext.mapboxCarMap.updateMapStyle(
//                    if (BreezeUserPreferencesUtil.isDarkTheme(mainCarContext.carContext))
//                        MapboxCarApp.options.mapNightStyle ?: MapboxCarApp.options.mapDayStyle
//                    else
//                        MapboxCarApp.options.mapDayStyle
//                )
//            }
            is CarOpenAmenityScreen -> {
                screenManager.push(AmenityScreen(mainCarContext, event.type, event.fromScreen))
            }

            else -> {}
        }
    }

    private fun showCarToast(data: CarToastEventData) {
        CarToast.makeText(mainCarContext.carContext, data.title, data.duration).show()
    }

    private fun toggleOBULiteScreen(appScreenState: Lifecycle.State) {
        if (appScreenState.isAtLeast(Lifecycle.State.CREATED)) {
            if (screenManager.top !is CarOBULiteScreen) {
                screenManager.popTo(CarConstants.HOME_MARKER)
                screenManager.push(CarOBULiteScreen(OBULiteCarContext(mainCarContext)))
            }
        } else {
            if (screenManager.top is CarOBULiteScreen) {
                screenManager.popTo(CarConstants.HOME_MARKER)
                if (screenManager.top !is HomeCarScreen) {
                    screenManager.push(HomeCarScreen(mainCarContext))
                }
            }
        }
    }

    private fun createNotificationChannel(channelId: String): String {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = "Breeze Car"
            val descriptionText = "Breeze Car"
            val importance = NotificationManager.IMPORTANCE_HIGH
            val channel = NotificationChannel(channelId, name, importance).apply {
                description = descriptionText
            }
            // Register the channel with the system
            val notificationManager: NotificationManager =
                mainCarContext.carContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
        return channelId
    }

    fun parseSearchResult(intent: Intent): String? {
        return intent.data?.toString()?.let { uriString ->
            var indexString = uriString.indexOf("?q=")
            if (indexString > 0) {
                uriString.substring(indexString + 3)
            } else {
                indexString = uriString.indexOf("geo:")
                if (indexString > 0) {
                    uriString.substring(indexString)
                } else null
            }
        }
    }


    private fun openSearch(intent: Intent): BaseScreenCar? {
        val query = parseSearchResult(intent)
        if (query != null) {
            return SearchScreen(mainCarContext!!, query)
        }
        return null
    }

}
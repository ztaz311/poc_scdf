package com.ncs.breeze.car.breeze.screen.navigation

import android.annotation.SuppressLint
import android.graphics.Rect
import android.location.Location
import com.mapbox.androidauto.internal.logAndroidAuto
import com.mapbox.geojson.Point
import com.mapbox.maps.EdgeInsets
import com.mapbox.maps.extension.androidauto.MapboxCarMapObserver
import com.mapbox.maps.extension.androidauto.MapboxCarMapSurface
import com.mapbox.maps.plugin.locationcomponent.LocationComponentPlugin
import com.mapbox.navigation.ui.maps.camera.data.MapboxNavigationViewportDataSource
import com.mapbox.navigation.ui.maps.camera.transition.NavigationCameraTransitionOptions
import com.ncs.breeze.car.breeze.model.CarParkLayerModel
import com.ncs.breeze.car.breeze.screen.carkpark.CarParkCarContext
import com.ncs.breeze.car.breeze.utils.BreezeCarUtil
import com.ncs.breeze.car.breeze.utils.BreezeMapBoxUtil
import com.breeze.model.enums.CarParkViewOption
import com.breeze.model.api.ErrorData
import com.breeze.model.api.request.AmenitiesRequest
import com.breeze.model.api.response.amenities.BaseAmenity
import com.breeze.model.constants.AmenityType.CARPARK
import com.breeze.model.api.response.amenities.ResponseAmenities
import com.ncs.breeze.common.remote.ApiObserver
import com.breeze.model.constants.Constants
import com.ncs.breeze.common.utils.Utils
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class NavigationCarParkLayerRenderer(
    private val carParkCarContext: CarParkCarContext,
    val destinationLocation: Location,
    var typeCarParkOption:CarParkViewOption = CarParkViewOption.ALL_NEARBY
) : MapboxCarMapObserver {

    private var viewportDataSource: MapboxNavigationViewportDataSource? = null
//    private lateinit var navigationCamera: NavigationCamera

    private val carParkFeatureDataList = ArrayList<CarParkLayerModel>();
    var compositeDisposable: CompositeDisposable = CompositeDisposable()
    private var locationComponent: LocationComponentPlugin? = null

//    var currentLocation: Location? = null

    lateinit var mMapboxCarMapSurface: MapboxCarMapSurface

    @SuppressLint("MissingPermission")
    override fun onAttached(mapboxCarMapSurface: MapboxCarMapSurface) {
        mMapboxCarMapSurface = mapboxCarMapSurface
        mapboxCarMapSurface.mapSurface.getMapboxMap().getStyle()?.let {
            viewportDataSource = MapboxNavigationViewportDataSource(
                mapboxCarMapSurface.mapSurface.getMapboxMap()
            )
            renderCarParks()
        }
    }


    private fun renderCarParks() {
        CoroutineScope(Dispatchers.IO).launch {
            var destinationLocationName: String? = Utils.getAddressFromLocation(
                destinationLocation,
                carParkCarContext.mainCarContext.carContext
            )
            withContext(Dispatchers.Main) {
                if (destinationLocationName.isNullOrEmpty()) {
                    destinationLocationName = ""
                }

                getNearbyCarParks(
                    destinationLocation.latitude,
                    destinationLocation.longitude,
                    destinationLocationName!!,
                    destinationLocation
                )
            }
        }
    }


//    fun renderCarParks(
//        currentLocation: Location,
//        mapboxCarMapSurface: MapboxCarMapSurface,
//        it: Style,
//    ) {
//        CoroutineScope(Dispatchers.IO).launch {
//            var currentLocationName: String? = Utils.getAddressFromLocation(
//                currentLocation!!,
//                carParkCarContext.mainCarContext.carContext
//            )
//            withContext(Dispatchers.Main) {
//                if (currentLocationName.isNullOrEmpty()) {
//                    currentLocationName = ""
//                }
//                val point: Point =
//                    Point.fromLngLat(currentLocation.longitude, currentLocation.latitude);
//                val cameraOptions = CameraOptions.Builder()
//                    .center(point)
//                    .zoom(15.0)
//                    .pitch(0.0)
//                    .bearing(0.0)
//                    .build()
//                mapboxCarMapSurface?.mapSurface?.getMapboxMap()
//                    ?.setCamera(cameraOptions)
//                // Change view
//                // Zoom to view
//
//                getNearbyCarParks(
//                    it,
//                    currentLocation.latitude,
//                    currentLocation.longitude,
//                    currentLocationName!!,
//                    currentLocation
//                )
//            }
//        }
//    }

    override fun onVisibleAreaChanged(visibleArea: Rect, edgeInsets: EdgeInsets) {
        super.onVisibleAreaChanged(visibleArea, edgeInsets)
        logAndroidAuto("CarNavigationCamera visibleAreaChanged $visibleArea $edgeInsets")

        viewportDataSource?.overviewPadding = EdgeInsets(
            edgeInsets.top + OVERVIEW_PADDING,
            edgeInsets.left + OVERVIEW_PADDING,
            edgeInsets.bottom + OVERVIEW_PADDING,
            edgeInsets.right + OVERVIEW_PADDING
        )

        val visibleHeight = visibleArea.bottom - visibleArea.top
        val followingBottomPadding = visibleHeight * BOTTOM_FOLLOWING_PERCENTAGE
        viewportDataSource?.followingPadding = EdgeInsets(
            edgeInsets.top,
            edgeInsets.left,
            edgeInsets.bottom + followingBottomPadding,
            edgeInsets.right
        )

        viewportDataSource?.evaluate()
    }


    override fun onDetached(mapboxCarMapSurface: MapboxCarMapSurface) {
        mapboxCarMapSurface.mapSurface?.getMapboxMap()?.getStyle()?.let {
            BreezeMapBoxUtil.removeCarkParkData(it, carParkFeatureDataList);
        }
        compositeDisposable.dispose()
    }

    //TODO handle errors
    private fun getNearbyCarParks(
        latitude: Double?,
        longitude: Double?,
        destName: String,
        location: Location
    ) {
        val listCarPark: ArrayList<BaseAmenity> = arrayListOf()
        val carParkRequest = createCarParkRequest(location, destName)
        fetchAndUpdateCarParks(carParkRequest, listCarPark, longitude, latitude)
    }

    private fun fetchAndUpdateCarParks(
        carParkRequest: AmenitiesRequest,
        listCarPark: ArrayList<BaseAmenity>,
        longitude: Double?,
        latitude: Double?,
    ) {
        BreezeCarUtil.apiHelper.getAmenities(carParkRequest)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : ApiObserver<ResponseAmenities>(compositeDisposable) {
                override fun onSuccess(data: ResponseAmenities) {
                    data.apply {
                        amenities.forEach { amenity ->
                            amenity.data.forEach {
                                if (amenity.type != null) {
                                    it.amenityType = amenity.type!!
                                    it.iconUrl = amenity.iconUrl
                                    listCarPark.add(it)
                                }
                            }
                        }

                        val (sortedValueItem, curLocPoint) = updateMapAndTemplateView(
                            listCarPark,
                            longitude,
                            latitude
                        )
//                        updateMapCamera(sortedValueItem, curLocPoint, currentLocation)
                    }


                }

                override fun onError(e: ErrorData) {

                }
            })
    }

    private fun createCarParkRequest(
        currentLocation: Location,
        destName: String
    ): AmenitiesRequest {
        val carParkRequest = AmenitiesRequest(
            lat = currentLocation.latitude,
            long = currentLocation.longitude,
            rad = BreezeCarUtil.breezeUserPreferenceUtil.getBreezeUserPreference()
                .getCarParkDistanceSettingValue(BreezeCarUtil.masterlists),
            maxRad = Constants.PARKING_CHECK_RADIUS_MAX,
            resultCount = BreezeCarUtil.breezeUserPreferenceUtil.getBreezeUserPreference()
                .getCarParkCountSettingValue(BreezeCarUtil.masterlists),
            destName = destName,
        )
        carParkRequest.setListType(
            listOf(CARPARK),
            BreezeCarUtil.breezeUserPreferenceUtil.getBreezeUserPreference()
                .getCarParkAvailabilityValue(BreezeCarUtil.masterlists, typeCarParkOption.mode)
        )
        carParkRequest.setCarParkCategoryFilter()
        return carParkRequest
    }

    private fun updateMapCamera(
        sortedCarparkItem: List<BaseAmenity>,
        curLocPoint: Point,
        currentLocation: Location
    ) {
        val points = sortedCarparkItem.map {
            Point.fromLngLat(it.long!!, it.lat!!)
        }
        val listWithMarker = ArrayList(points);
        listWithMarker.add(curLocPoint)

        val instantTransition = NavigationCameraTransitionOptions.Builder()
            .maxDuration(0)
            .build()
        viewportDataSource?.run {
            onLocationChanged(currentLocation)
            additionalPointsToFrameForOverview(listWithMarker)
            //viewportDataSource.options.overviewFrameOptions.maxZoom=15.0;
            evaluate()
//        navigationCamera
//            .requestNavigationCameraToOverview(
//                stateTransitionOptions = instantTransition
//            )
        }
    }

    private fun updateMapAndTemplateView(
        listCarPark: ArrayList<BaseAmenity>,
        longitude: Double?,
        latitude: Double?,
    ): Pair<List<BaseAmenity>, Point> {
        val sortedValueItem =
            listCarPark.sortedWith(
                compareBy<BaseAmenity> {
                    it.isSeasonParking()
                }.thenByDescending {
                    it.isCheapest
                }.thenBy {
                    it.destinationCarPark
                }
            ).take(6)

        val curLocPoint = Point.fromLngLat(longitude!!, latitude!!)
        mMapboxCarMapSurface.mapSurface.getMapboxMap().getStyle()?.let { style ->
            BreezeMapBoxUtil.displayCarkParkData(
                carParkFeatureDataList,
                style,
                sortedValueItem,
                carParkCarContext.mainCarContext.carContext,
                false
            )
        }
        return Pair(sortedValueItem, curLocPoint)
    }


    private companion object {
        /**
         * While following the location puck, inset the bottom by 1/3 of the screen.
         */
        private const val BOTTOM_FOLLOWING_PERCENTAGE = 1.0 / 3.0

        /**
         * While overviewing a route, add padding to thew viewport.
         */
        private const val OVERVIEW_PADDING = 30
    }


}
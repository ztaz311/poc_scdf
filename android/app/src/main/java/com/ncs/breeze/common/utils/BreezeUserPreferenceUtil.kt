package com.ncs.breeze.common.utils

import android.content.Context
import com.breeze.model.enums.TrafficIncidentData
import com.breeze.model.enums.RoutePreference
import com.breeze.model.common.TrafficIncidentPreference as TrafficIncidentPreferenceV2
import com.breeze.model.common.UserPreference as UserPreferenceV2
import com.breeze.model.common.UserStoredData as UserStoredDataV2
import com.breeze.model.enums.UserTheme
import com.ncs.breeze.common.storage.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class BreezeUserPreferenceUtil @Inject constructor(private val appContext: Context) {

    fun saveUserPreference(userPreference: UserPreferenceV2) {
        this.getBreezeUserPreference().saveUserPreference(userPreference)
    }

    fun getBreezeUserPreference(): BreezeUserPreference {
        return BreezeUserPreference.getInstance(appContext)
    }

    private fun retrieveUserPreference(): UserPreferenceV2 {
        var userPreference = this.getBreezeUserPreference().retrieveUserPreference()

        if (userPreference == null) {
            userPreference = initUserPreference()
        }
        return userPreference
    }

    fun saveUserStoredData(data: UserStoredDataV2) {
        getBreezeUserPreference().saveUserStoredData(data)
    }

    fun getUserStoredData(): UserStoredDataV2? {
        return getBreezeUserPreference().retrieveUserStoredDataV2()
    }


    //Initialize preference with default settings
    fun initUserPreference(): UserPreferenceV2 {
        val preferredTheme = UserTheme.SYSTEM_DEFAULT.type
        val map: HashMap<String, TrafficIncidentPreferenceV2> = hashMapOf()

        for (incidentType in TrafficIncidentData.values()) {
            map.put(incidentType.type, TrafficIncidentPreferenceV2(incidentType.type, true, true))
        }
        val routePreference = RoutePreference.FASTEST_ROUTE.type

        return UserPreferenceV2(
            "",
            "",
            "",
            preferredTheme,
            UserTheme.LIGHT.type,
            map,
            playNavigationVoice = true,
            routePreference
        )
    }

//    fun isDarkTheme() =
//        getUserThemePreference() == UserTheme.DARK.type
//                || (getUserThemePreference() == UserTheme.SYSTEM_DEFAULT.type && appContext.resources.configuration.uiMode and
//                Configuration.UI_MODE_NIGHT_MASK == Configuration.UI_MODE_NIGHT_YES)

    fun isDarkTheme() = false

    fun getUserThemePreference(): String {
        val userPreference = retrieveUserPreference()
        return userPreference.preferredTheme
    }

    fun getIncidentSettings(): HashMap<String, TrafficIncidentPreferenceV2> {
        val userPreference = retrieveUserPreference()
        return userPreference.trafficIncidentMap
    }


    fun saveUserThemePreference(chosenTheme: UserTheme) {
        val userPreference = retrieveUserPreference()
        userPreference.preferredTheme = chosenTheme.type
        saveUserPreference(userPreference)
    }

    fun getRoutePreference(): String {
        val userPreference = retrieveUserPreference()
        var routePreference = userPreference.routePreference
        if (routePreference == null) {
            routePreference = RoutePreference.FASTEST_ROUTE.type
            saveUserRoutePreference(RoutePreference.FASTEST_ROUTE)
        }
        return routePreference
    }

    fun saveUserRoutePreference(chosenRoutePreference: RoutePreference) {
        val userPreference = retrieveUserPreference()
        userPreference.routePreference = chosenRoutePreference.type
        saveUserPreference(userPreference)
    }


    fun saveUserOnBoardingState(onBoardingState: String) {
        val userPreference = retrieveUserPreference()
        userPreference.onBoardingState = onBoardingState
        saveUserPreference(userPreference)
    }

    fun saveUserName(userName: String) {
        getBreezeUserPreference().retrieveUserStoredDataV2()?.apply {
            this.userName = userName
        }?.let { saveUserStoredData(it) }
    }

    fun retrieveUserName() =  getUserStoredData()?.userName ?: ""

    fun saveUserEmail(userEmail: String) {
        getBreezeUserPreference().retrieveUserStoredDataV2()?.apply {
            email = userEmail
        }?.let { saveUserStoredData(it) }
    }

    fun retrieveUserEmail() = getUserStoredData()?.email ?: ""

    fun retrieveUserCarIU() = getUserStoredData()?.iuNumber ?: ""

    fun retrieveUserCarPlate() = getUserStoredData()?.carplateNumber ?: ""

    fun saveUserCarDetails(iuNumber: String, carplateNumber: String) {
        getUserStoredData()?.apply {
            this.iuNumber = iuNumber
            this.carplateNumber = carplateNumber
        }?.let {
            saveUserStoredData(it)
        }
    }

    fun saveUserEmailVerifiedStatus(isVerified: Boolean) {
        val userPreference = retrieveUserPreference()
        userPreference.isUserEmailVerified = isVerified
        saveUserPreference(userPreference)
    }

    fun retrieveUserEmailVerifiedStatus(): Boolean {
        val userPreference = retrieveUserPreference()
        return userPreference.isUserEmailVerified
    }

    fun retrieveUserPhoneNumber(): String {
        val userPreference = retrieveUserPreference()
        return userPreference.userPhoneNumber
    }

    fun saveUserPhoneNumber(phoneNumber: String) {
        val userPreference = retrieveUserPreference()
        userPreference.userPhoneNumber = phoneNumber
        saveUserPreference(userPreference)
    }

    fun saveAmplifyProviderType(provider: String) {
        val userPreference = retrieveUserPreference()
        userPreference.amplifyProvider = provider
        saveUserPreference(userPreference)
    }

    fun retrieveAmplifyIdentityProvider(): String {
        val userPreference = retrieveUserPreference()
        return userPreference.amplifyProvider
    }

    fun saveInstabugGroup(groupName: String) {
        val userPreference = retrieveUserPreference()
        userPreference.instabugGroup = groupName
        saveUserPreference(userPreference)
    }

    fun retrieveInstabugGroup(): String? {
        val userPreference = retrieveUserPreference()
        return userPreference.instabugGroup
    }

    fun saveUserCreatedDate(dateTimeStamp: Long) {
        val userPreference = retrieveUserPreference()
        userPreference.userCreatedDate = dateTimeStamp
        saveUserPreference(userPreference)
    }

    fun retrieveUserCreatedDate(): Long? {
        val userPreference = retrieveUserPreference()
        return userPreference.userCreatedDate
    }

    /**
     * for guest mode
     */
    fun saveEmailGuestUser(emailLoginGuestMode: String) {
        val userPreference = retrieveUserPreference()
        userPreference.emailGuestUser = emailLoginGuestMode
        saveUserPreference(userPreference)
    }

    fun saveIsGuestMode(isGuestMode: Boolean = false) {
        val userPreference = retrieveUserPreference()
        userPreference.isGuestMode = isGuestMode
        saveUserPreference(userPreference)
    }

    fun retrieveEmailGuestUser(): String {
        val userPreference = retrieveUserPreference()
        return userPreference.emailGuestUser
    }

    fun isGuestMode(): Boolean {
        val userPreference = retrieveUserPreference()
        return userPreference.isGuestMode
    }
}
package com.ncs.breeze.reactnative.nativemodule

import androidx.core.os.bundleOf
import com.facebook.react.bridge.Arguments
import com.ncs.breeze.App
import com.ncs.breeze.common.constant.OBUConnectionState
import com.ncs.breeze.common.extensions.android.getUserDataPreference
import com.breeze.model.extensions.round
import com.ncs.breeze.common.helper.obu.OBUDataHelper
import com.breeze.model.obu.OBUDevice
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import java.lang.ref.WeakReference

class ShareBusinessLogicHelper(app: App) {
    private val appRef = WeakReference(app)

    fun setNoCard(isNoCard: Boolean) {
        appRef.get()?.shareBusinessLogicPackage?.getRequestsModule()?.flatMap {
            it.callRN(
                ShareBusinessLogicEvent.SET_NO_CARD.eventName,
                Arguments.fromBundle(
                    bundleOf(
                        "data" to isNoCard,
                    )
                )
            )
        }?.subscribeOn(Schedulers.io())?.observeOn(AndroidSchedulers.mainThread())?.subscribe()
    }

    fun updateOBUCashCardInfo(cardStatus: String, cardBalance: Double?) {
        appRef.get()?.shareBusinessLogicPackage?.getRequestsModule()?.flatMap {
            it.callRN(
                ShareBusinessLogicEvent.UPDATE_OBU_CASH_CARD_INFO.eventName,
                Arguments.fromBundle(
                    bundleOf(
                        "status" to cardStatus,
                        "paymentMode" to OBUDataHelper.getOBUPaymentMode(),
                        "balance" to cardBalance?.round(),
                    )
                )
            )
        }?.subscribeOn(Schedulers.io())?.observeOn(AndroidSchedulers.mainThread())?.subscribe()
    }

    fun updateOBUConnectionState(state: OBUConnectionState, obuDevice: OBUDevice? = null) {
        appRef.get()?.shareBusinessLogicPackage?.getRequestsModule()?.flatMap {
            val payload = Arguments.createMap().apply {
                putString("status", state.stateStr)
                obuDevice?.takeIf { state == OBUConnectionState.CONNECTED || state == OBUConnectionState.PAIRING }
                    ?.let { device ->
                        OBUDataHelper.getOBUCardBalanceSGD()?.let { cardBalance ->
                            putString("cardBalance", cardBalance.round())
                        }
                        putString("paymentMode", OBUDataHelper.getOBUPaymentMode())
                        putString("obuName", device.obuName)
                        putString("vehicleNumber", device.vehicleNumber)
                    }
            }
            Timber.d("updateOBUConnectionState $state, device = $payload")
            it.callRN(ShareBusinessLogicEvent.CHANGE_STATUS_PAIR.eventName, payload)
        }?.subscribeOn(Schedulers.io())?.observeOn(AndroidSchedulers.mainThread())?.subscribe()
    }

    fun updatePairedVehicleList(pairedDevices: Array<OBUDevice>) {
        appRef.get()?.shareBusinessLogicPackage?.getRequestsModule()
            ?.flatMap { shareBusinessLogic ->
                val payload = Arguments.createMap().apply {
                    putArray(
                        "data",
                        Arguments.fromArray(
                            pairedDevices.map
                            { item -> item.toBundle() }.toTypedArray()
                        )
                    )
                }
                shareBusinessLogic.callRN(
                    ShareBusinessLogicEvent.UPDATE_PAIRED_VEHICLE_LIST.eventName, payload
                )
            }?.subscribeOn(Schedulers.io())?.observeOn(AndroidSchedulers.mainThread())?.subscribe()
    }

    fun updatePairedVehicleList() {
        appRef.get()?.let { app ->
            val connectedDevices =
                app.getUserDataPreference()?.getConnectedOBUDevices() ?: arrayOf()
            updatePairedVehicleList(connectedDevices)
        }
    }

    fun initOBUAutoConnect(isEnabled: Boolean) {
        appRef.get()?.shareBusinessLogicPackage?.getRequestsModule()
            ?.flatMap { shareBusinessLogic ->
                val payload = Arguments.createMap().apply {
                    putBoolean(
                        "isEnable",
                        isEnabled
                    )
                }

                shareBusinessLogic.callRN(
                    ShareBusinessLogicEvent.UPDATE_VALUE_AUTO_CONNECT_OBU.eventName,
                    payload
                )
            }?.subscribeOn(Schedulers.io())?.observeOn(AndroidSchedulers.mainThread())?.subscribe()
    }

    fun backToHomepage(mode: String? = null) {
        appRef.get()?.shareBusinessLogicPackage?.getRequestsModule()
            ?.flatMap { shareBusinessLogic ->
                val data = mode?.let {
                    Arguments.createMap().apply {
                        putString("mode", it)
                    }
                }
                shareBusinessLogic.callRN(
                    ShareBusinessLogicEvent.BACK_TO_HOME_PAGE.eventName, data
                )
            }?.subscribeOn(Schedulers.io())?.observeOn(AndroidSchedulers.mainThread())?.subscribe()
    }

    fun toggleButtonCarparkList(disable: Boolean) {
        appRef.get()?.shareBusinessLogicPackage?.getRequestsModule()
            ?.flatMap { shareBusinessLogic ->
                val data = Arguments.createMap().apply {
                    putBoolean("disable", disable)
                }
                shareBusinessLogic.callRN(
                    ShareBusinessLogicEvent.DISABLE_TOGGLE_BUTTON_CARPARK_LIST.eventName, data
                )
            }?.subscribeOn(Schedulers.io())?.observeOn(AndroidSchedulers.mainThread())?.subscribe()
    }
}

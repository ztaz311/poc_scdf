package com.ncs.breeze.ui.rn

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent

import com.swmansion.gesturehandler.react.RNGestureHandlerEnabledRootView

class CustomReactRootView @JvmOverloads constructor(context: Context?, attrs: AttributeSet?) :
    RNGestureHandlerEnabledRootView(context, attrs) {
    init {
        setIsFabric(false)
    }
    override fun onTouchEvent(ev: MotionEvent): Boolean {
        super.onTouchEvent(ev)
        return false
    }
}
package com.ncs.breeze.car.breeze.screen.settings

import com.ncs.breeze.car.breeze.MainBreezeCarContext

/**
 * Contains the dependencies for the settings screen.
 */
class SettingsCarContext(
    val mainCarContext: MainBreezeCarContext
) {
    val carContext = mainCarContext.carContext
    val carSettingsStorage = mainCarContext.carSettingsStorage
}

package com.ncs.breeze.notification

import android.app.AlarmManager
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import androidx.core.os.bundleOf
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.instabug.chat.Replies
import com.ncs.breeze.App
import com.ncs.breeze.R
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.Screen
import com.ncs.breeze.common.extensions.android.getAppPreference
import com.ncs.breeze.common.remote.ApiHelper
import com.ncs.breeze.common.storage.AppPrefsKey
import com.ncs.breeze.common.storage.BreezeUserPreference.Companion.getInstance
import com.breeze.model.constants.Constants
import com.ncs.breeze.common.utils.Utils
import com.ncs.breeze.common.utils.notification.PushNotificationUtils
import com.ncs.breeze.service.AlarmReceiver
import com.ncs.breeze.ui.NotificationDataHandlerActivity
import com.ncs.breeze.ui.dashboard.activity.DashboardActivity
import com.ncs.breeze.ui.splash.SplashActivity
import timber.log.Timber
import java.util.*
import javax.inject.Inject

class FCMListenerService : FirebaseMessagingService() {
    @JvmField
    @Inject
    var mAPIHelper: ApiHelper? = null

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)

        val title = remoteMessage.data["title"]
        val message = remoteMessage.data["message"]
        val pushType = remoteMessage.data["PUSH_TYPE"]

        Timber.d("$TAG onMessageReceived called")
        Timber.d("$TAG message: $message")
        Timber.d("$TAG remoteMessage.getData(): ${remoteMessage.data}")

        sendAnalytics(remoteMessage)

        if (pushType != null && pushType == "background") {
            captureSilentPush(remoteMessage.data)
            return
        }

        //Check first if notification related to Instabug or not
        if (Replies.isInstabugNotification(remoteMessage.data)) {
            //Shown notification related to Instabug
            Replies.showNotification(remoteMessage.data)
        } else {
            if (remoteMessage.data.containsKey("type")) {
                if (remoteMessage.data["type"].equals(Constants.NOTIFICATION_TRIP_EMAIL)) {
                    sendTripEmailNotification(remoteMessage)
                } else if (remoteMessage.data["type"].equals(Constants.NOTIFICATION_APP_UPDATE)) {
                    sendUpdateNotification(remoteMessage)
                } else if (remoteMessage.data["type"].equals(Constants.NOTIFICATION_UPCOMING_TRIP)) {
                    sendUpcomingTripNotification(remoteMessage)
                } else if (remoteMessage.data["type"].equals(Constants.NOTIFICATION_CARPARK_VOUCHER)) {
                    sendCarParkVoucherNotification(remoteMessage)
                } else if (remoteMessage.data["type"].equals(Constants.NOTIFICATION_INBOX)) {
                    sendOpenActionNotification(remoteMessage)
                } else if (remoteMessage.data["type"].equals(Constants.NOTIFICATION_EXPLORE_MAP)) {
                    sendOpenExploreMapNotification(remoteMessage)
                } else {
                    sendNotification(title = title, message = message ?: "")
                }
            } else {
                sendNotification(title = title, message = message ?: "")
            }
        }
    }

    private fun captureSilentPush(data: Map<String, String>) {
        val refreshKey = data["REFRESH_KEY"]
        if (refreshKey != null) {
            getInstance(this).saveString(Constants.SHARED_PREF_KEY.PUSH_REFRESH, refreshKey, false)
        }
    }

    override fun onNewToken(newToken: String) {
        super.onNewToken(newToken)
        Timber.d("new token generated: [$newToken]")
        getAppPreference()?.saveString(AppPrefsKey.PUSH_MESSAGING_TOKEN, newToken)
    }

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param message FCM message received.
     */
    private fun sendNotification(title: String?, message: String) {
        val intent = Intent(this, SplashActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP) //Woa  woa, be careful, coz this will kill
        // the running app and start the splash activity again

        val notificationId = 0
        PushNotificationUtils.createBreezeNotification(
            context = this,
            notificationId = notificationId,
            channelId = NOTIFICATION_CHANNEL_ID_GENERAL,
            channelName = NOTIFICATION_CHANNEL_NAME_GENERAL,
            title = title ?: getString(R.string.app_name),
            description = message,
            intent = intent
        )
    }

    private fun createNotificationIntent(remoteMessage: RemoteMessage) =
        Intent(this, NotificationDataHandlerActivity::class.java).apply {
            Analytics.logScreenView(Screen.OUTSIDE_APP)
            val analyticsType: String =
                if (remoteMessage.data.containsKey("type")) remoteMessage.data["type"].toString() else ""
            val analyticsMessage: String =
                if (remoteMessage.data.containsKey("body")) remoteMessage.data["body"].toString() else ""

            putExtra(
                Constants.SHARED_PREF_KEY.HANDLE_PUSH_NOTIFICATION_ANALYTICS_STATISTIC, bundleOf(
                    Constants.PushNotification.KEY_DATE_PUSHED to Utils.getCurrentDateFormatted("YYYY-MM-dd HH:mm"),
                    Constants.PushNotification.KEY_MESSAGE to analyticsMessage,
                    Constants.PushNotification.KEY_CATEGORY to analyticsType,
                )
            )
        }

    private fun sendTripEmailNotification(remoteMessage: RemoteMessage) {
        val title = remoteMessage.data["title"] ?: ""
        val body = remoteMessage.data["body"] ?: ""
        val notificationId = (Date().time / 1000L % Int.MAX_VALUE).toInt()
        PushNotificationUtils.createBreezeNotification(
            context = this,
            notificationId = notificationId,
            channelId = NOTIFICATION_CHANNEL_ID_GENERAL,
            channelName = NOTIFICATION_CHANNEL_NAME_GENERAL,
            title = title,
            description = body,
        )
    }

    private fun sendUpdateNotification(remoteMessage: RemoteMessage) {
        val channelId = NOTIFICATION_CHANNEL_ID_UPDATE_APP
        val title = remoteMessage.data["title"] ?: ""
        val message = remoteMessage.data["body"] ?: ""
        val url = remoteMessage.data["link_url"]
        val intent = createNotificationIntent(remoteMessage).apply {

        }
        intent.putExtra(Constants.SHARED_PREF_KEY.HANDLE_PUSH_NOTIFICATION_CHANNEL, channelId)
        intent.putExtra(Constants.SHARED_PREF_KEY.HANDLE_PUSH_NOTIFICATION_ID, url ?: "")
        PushNotificationUtils.createBreezeNotification(
            context = this,
            notificationId = UPDATE_NOTIFICATION_ID,
            channelId = channelId,
            channelName = NOTIFICATION_CHANNEL_NAME_UPDATE_APP,
            title = title,
            description = message,
            intent = intent,
            importanceLevel = NotificationManager.IMPORTANCE_HIGH
        )
    }

    private fun sendUpcomingTripNotification(
        remoteMessage: RemoteMessage
    ) {
        val tripPlannerId = remoteMessage.data["tripPlannerId"] ?: ""
        val title = remoteMessage.data["title"] ?: ""
        val message = remoteMessage.data["body"] ?: ""
        val dashboardActivity = getActivityThatCanHandleNotifications()
        if (dashboardActivity != null) {
            dashboardActivity.showUpcomingTripNotification(
                tripPlannerId = tripPlannerId,
                title = title,
                message = message
            )
        } else {
            val channelId = NOTIFICATION_CHANNEL_ID_UPCOMING_TRIPS
            val intent = createNotificationIntent(remoteMessage).apply {
                putExtra(Constants.SHARED_PREF_KEY.HANDLE_PUSH_NOTIFICATION_CHANNEL, channelId)
                putExtra(Constants.SHARED_PREF_KEY.HANDLE_PUSH_NOTIFICATION_ID, tripPlannerId)
            }
            //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)

            val notificationId = tripPlannerId.safeInt()
            PushNotificationUtils.createBreezeNotification(
                context = this,
                notificationId = notificationId,
                channelId = channelId,
                channelName = NOTIFICATION_CHANNEL_NAME_UPCOMING_TRIPS,
                title = title,
                description = message,
                intent = intent,
                importanceLevel = NotificationManager.IMPORTANCE_HIGH
            )
        }
    }

    private fun sendCarParkVoucherNotification(
        remoteMessage: RemoteMessage
    ) {
        val voucherId = remoteMessage.data["voucherId"] ?: ""
        val title = remoteMessage.data["title"] ?: ""
        val message = remoteMessage.data["body"] ?: ""
        val messageId = remoteMessage.data["messageId"] ?: ""
        val canShowAsGlobalNotification = false
        val dashboardActivity = getActivityThatCanHandleNotifications()
        if (canShowAsGlobalNotification && dashboardActivity != null) {
            dashboardActivity.showCarParkVoucherNotification(
                voucherId = voucherId,
                title = title,
                message = message
            )
        } else {
            val channelId = NOTIFICATION_CHANNEL_ID_CARPARK_VOUCHER
            val intent = createNotificationIntent(remoteMessage).apply {
                putExtra(Constants.SHARED_PREF_KEY.HANDLE_PUSH_NOTIFICATION_CHANNEL, channelId)
                putExtra(Constants.SHARED_PREF_KEY.HANDLE_PUSH_NOTIFICATION_ID, voucherId)
                putExtra(Constants.SHARED_PREF_KEY.HANDLE_PUSH_NOTIFICATION_EXTRA, messageId)
                addFlags(
                    Intent.FLAG_ACTIVITY_CLEAR_TASK or
                            Intent.FLAG_ACTIVITY_CLEAR_TOP or
                            Intent.FLAG_ACTIVITY_NEW_TASK
                )
            }
            //val notificationId = tripPlannerId.safeInt()
            val notificationId = 1001
            PushNotificationUtils.createBreezeNotification(
                context = this,
                notificationId = notificationId,
                channelId = channelId,
                channelName = NOTIFICATION_CHANNEL_NAME_CARPARK_VOUCHERS,
                title = title,
                description = message,
                intent = intent,
                importanceLevel = NotificationManager.IMPORTANCE_HIGH
            )
        }
    }

    private fun sendOpenActionNotification(remoteMessage: RemoteMessage) {
        val messageId = remoteMessage.data["messageId"] ?: "-1"
        val title = remoteMessage.data["title"] ?: ""
        val message = remoteMessage.data["body"] ?: ""
        val type = NOTIFICATION_TYPE_OPEN_INBOX

        val channelId = NOTIFICATION_CHANNEL_ID_MISC
        val intent = createNotificationIntent(remoteMessage).apply {
            putExtra(Constants.SHARED_PREF_KEY.HANDLE_PUSH_NOTIFICATION_CHANNEL, channelId)
            putExtra(Constants.SHARED_PREF_KEY.HANDLE_PUSH_NOTIFICATION_ID, messageId)
            putExtra(Constants.SHARED_PREF_KEY.HANDLE_PUSH_NOTIFICATION_EXTRA, type)
            addFlags(
                Intent.FLAG_ACTIVITY_CLEAR_TASK or
                        Intent.FLAG_ACTIVITY_CLEAR_TOP or
                        Intent.FLAG_ACTIVITY_NEW_TASK
            )
        }
        //val notificationId = tripPlannerId.safeInt()
        val notificationId = 1001
        PushNotificationUtils.createBreezeNotification(
            context = this,
            notificationId = notificationId,
            channelId = channelId,
            channelName = NOTIFICATION_CHANNEL_NAME_MISC,
            title = title,
            description = message,
            intent = intent,
            importanceLevel = NotificationManager.IMPORTANCE_HIGH
        )
    }

    private fun sendOpenExploreMapNotification(remoteMessage: RemoteMessage) {
        val contentID = remoteMessage.data["contentID"] ?: "-1"
        val contentCategoryID = remoteMessage.data["contentCategoryID"] ?: "-1"
        val title = remoteMessage.data["title"] ?: ""
        val message = remoteMessage.data["body"] ?: ""
        val type = NOTIFICATION_TYPE_OPEN_EXPLORE_MAP
        val channelId = NOTIFICATION_CHANNEL_ID_MISC
        val intent = createNotificationIntent(remoteMessage).apply {
            putExtra(Constants.SHARED_PREF_KEY.HANDLE_PUSH_NOTIFICATION_CHANNEL, channelId)
            putExtra(Constants.SHARED_PREF_KEY.HANDLE_PUSH_NOTIFICATION_ID, contentID)
            putExtra(Constants.SHARED_PREF_KEY.KEY_CONTENT_CATEGORY_ID, contentCategoryID)
            putExtra(Constants.SHARED_PREF_KEY.HANDLE_PUSH_NOTIFICATION_EXTRA, type)
            addFlags(
                Intent.FLAG_ACTIVITY_CLEAR_TASK or
                        Intent.FLAG_ACTIVITY_CLEAR_TOP or
                        Intent.FLAG_ACTIVITY_NEW_TASK
            )
        }
        //val notificationId = tripPlannerId.safeInt()
        val notificationId = 1001
        PushNotificationUtils.createBreezeNotification(
            context = this,
            notificationId = notificationId,
            channelId = channelId,
            channelName = NOTIFICATION_CHANNEL_NAME_MISC,
            title = title,
            description = message,
            intent = intent,
            importanceLevel = NotificationManager.IMPORTANCE_HIGH
        )
    }

    private fun getActivityThatCanHandleNotifications(): DashboardActivity? {
        val currentActivity = (application as? App)?.takeIf { it.appLifeCycleHandler.isAppActive }
            ?.getCurrentActivity() as? DashboardActivity
        return currentActivity?.takeUnless { it.isTripRunning() }
    }

    private fun sendAnalytics(remoteMessage: RemoteMessage) {
        Analytics.logScreenView(Screen.OUTSIDE_APP)
        val analyticsType: String =
            if (remoteMessage.data.containsKey("type")) remoteMessage.data["type"].toString() else ""
        val analyticsMessage: String =
            if (remoteMessage.data.containsKey("title")) remoteMessage.data["title"].toString() else ""
        Analytics.logNotificationStatistics(
            screenName = Screen.OUTSIDE_APP,
            dateClicked = "",
            datePushed = Utils.getCurrentDateFormatted("YYYY-MM-dd HH:mm"),
            message = analyticsMessage,
            category = analyticsType
        )
    }

    companion object {
        const val TAG = "FCMListenerService"
        const val NOTIFICATION_CHANNEL_ID_GENERAL = "BREEZE_ID"
        const val NOTIFICATION_CHANNEL_ID_UPDATE_APP = "100"
        const val NOTIFICATION_CHANNEL_ID_UPCOMING_TRIPS = "101"
        const val NOTIFICATION_CHANNEL_ID_CARPARK_VOUCHER = "102"
        const val NOTIFICATION_CHANNEL_ID_MISC = "103"

        const val ARG_NOTIFICATION_ANALYTICS_STATISTIC = "HANDLE_PUSH_NOTIFICATION_ANALYTICS_STATISTIC"

        const val UPDATE_NOTIFICATION_ID = 20;
        const val NOTIFICATION_ID_OBU_CONNECTED = 1
        const val NOTIFICATION_CHANNEL_ID_OBU_CONNECTED = "104"
        const val NOTIFICATION_CHANNEL_NAME_OBU_CONNECTED = "OBU Connected"

        //Warning: This names are VISIBLE to users in App setting -> 'Notifications' section
        const val NOTIFICATION_CHANNEL_NAME_GENERAL = "General"
        const val NOTIFICATION_CHANNEL_NAME_UPDATE_APP = "Version updates"
        const val NOTIFICATION_CHANNEL_NAME_UPCOMING_TRIPS = "Upcoming Trips"
        const val NOTIFICATION_CHANNEL_NAME_CARPARK_VOUCHERS = "Parking Vouchers"
        const val NOTIFICATION_CHANNEL_NAME_MISC = "Features and improvements"

        const val NOTIFICATION_TYPE_OPEN_INBOX = "NOTIFICATION_TYPE_OPEN_INBOX"
        const val NOTIFICATION_TYPE_OPEN_EXPLORE_MAP = "NOTIFICATION_TYPE_OPEN_EXPLORE_MAP"

        @Deprecated("This method was created for testing purpose only. Need to test fully before using in live app.")
        fun scheduleUpdateNotification(
            context: Context,
            title: String,
            message: String,
            delayMilis: Long = 0
        ) {
            if (delayMilis > 0) {
                val notificationIntent = Intent(context, AlarmReceiver::class.java)
                notificationIntent.putExtra(AlarmReceiver.PARAM_NOTIFICATION_TITLE, title)
                notificationIntent.putExtra(AlarmReceiver.PARAM_NOTIFICATION_DESC, message)

                val pendingIntent = PendingIntent.getBroadcast(
                    context,
                    UPDATE_NOTIFICATION_ID,
                    notificationIntent,
                    PushNotificationUtils.NOTIFICATION_FLAGS
                )

                val futureInMillis = System.currentTimeMillis() + delayMilis
                val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
                alarmManager[AlarmManager.RTC_WAKEUP, futureInMillis] = pendingIntent
            } else {
                AlarmReceiver.sendUpdateNotification(context, title, message)
            }
        }
    }
}

private fun String.safeInt(): Int {
    return try {
        this.toInt()
    } catch (e: Exception) {
        0
    }
}

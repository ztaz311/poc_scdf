package com.ncs.breeze.ui.dashboard.fragments.view

import android.content.Intent
import android.net.MailTo
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import com.ncs.breeze.BuildConfig
import com.ncs.breeze.common.analytics.Screen
import com.ncs.breeze.common.utils.BreezeUserPreferenceUtil
import com.ncs.breeze.databinding.FragmentTermsandconditionBinding
import com.ncs.breeze.ui.base.BaseFragment
import com.ncs.breeze.ui.dashboard.activity.DashboardActivity
import com.ncs.breeze.ui.dashboard.fragments.viewmodel.TermsAndConditionViewModel
import javax.inject.Inject

class TermsAndConditionFragment :
    BaseFragment<FragmentTermsandconditionBinding, TermsAndConditionViewModel>() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val viewModel: TermsAndConditionViewModel by viewModels {
        viewModelFactory
    }

    @Inject
    lateinit var breezeUserPreferenceUtil: BreezeUserPreferenceUtil

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initialize()
    }

    fun initialize() {
        viewBinding.backButton.setOnClickListener {
            onBackPressed()
        }

        viewBinding.webViewTnc.getSettings().cacheMode = WebSettings.LOAD_NO_CACHE;

        viewBinding.webViewTnc.settings.javaScriptEnabled = true
        viewBinding.webViewTnc.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                if (url != null) {
                    if (url.contains("mailto:")) {
                        val mailTo: MailTo = MailTo.parse(url)
                        val addresses = Array<String>(1) { mailTo.getTo() }
                        val mailIntent: Intent = composeEmail(addresses, "")
                        (activity as DashboardActivity).startActivity(mailIntent)
                        view!!.reload()
                        return true
                    } else {
                        view?.loadUrl(url)
                    }
                    return true
                }
                return true
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                if (breezeUserPreferenceUtil.isDarkTheme()) {
                    val code = """javascript:(function() { 
                        var node = document.createElement('style');
                        node.type = 'text/css';
                        node.innerHTML = 'body {
                            color: white;
                            background-color: #222638;
                            padding-right: 24px;
                            padding-left: 24px;
                        }';
                        
                        
                        document.head.appendChild(node);
                        
                        
                      
                        var links = document.getElementsByTagName("a");
                        for(var i=0;i<links.length;i++)
                        {
                            if(links[i].href)
                            {
                                links[i].style.color = '#B155F8';  
                            }
                        }
                       document.getElementsByTagName('span')[0].style.webkitTextFillColor= 'white';

                        
                    })()""".trimIndent()
                    view?.loadUrl(code)
                } else {
                    val code = """javascript:(function() { 
                        var node = document.createElement('style');
                        node.type = 'text/css';
                        node.innerHTML = 'body {
                            padding-right: 24px;
                            padding-left: 24px;
                        }';
                        
                        document.head.appendChild(node);
                        
                    })()""".trimIndent()
                    view?.loadUrl(code)
                }
            }
        }
        viewBinding.webViewTnc.loadUrl(BuildConfig.TERMS_AND_CONDITIONS_URL)
        //viewBinding.webViewTnc.loadUrl(resources.getString(R.string.terms_and_condition_link));
    }

    override fun inflateViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?,
        attachToContainer: Boolean
    ) = FragmentTermsandconditionBinding.inflate(inflater, container, attachToContainer)

    override fun getViewModelReference(): TermsAndConditionViewModel {
        return viewModel
    }

    override fun getScreenName(): String {
        return Screen.SETTINGS_TERMS
    }

    // Now you need the sendEmail method to open the devices mail client and set the relevant fields
    private fun composeEmail(addresses: Array<String>, subject: String): Intent {
        val intent = Intent(Intent.ACTION_SENDTO).apply {
            data = Uri.parse("mailto:") // only email apps should handle this
            putExtra(Intent.EXTRA_EMAIL, addresses)
            putExtra(Intent.EXTRA_SUBJECT, subject)
        }
        return intent
    }
}
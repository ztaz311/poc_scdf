package com.ncs.breeze.components

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.Event
import com.breeze.model.api.response.FavouritesItem
import com.ncs.breeze.R
import com.ncs.breeze.common.analytics.Screen

@Deprecated("Obsolete view Adapter", level = DeprecationLevel.WARNING)
class FavouritesETAAdapter(
    var context: Context,
    var dataList: List<FavouritesItem>,
    var listener: FavouritesETAListener
) : RecyclerView.Adapter<FavouritesETAAdapter.RecyclerItemViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerItemViewHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_fav_eta_contact, parent, false)
        return RecyclerItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerItemViewHolder, position: Int) {
        // Get Item data and display
        var data = dataList[position]
        holder.nameAbbr.text = data.recipientName[0].toString()
        holder.contactName.text = data.recipientName
        holder.contactAddress.text = data.destination
        holder.mainLayout.setOnClickListener {
            Analytics.logClickEvent(Event.FAV_ARRIVAL, getScreenName())
            listener.onItemClick(data)
        }
    }


    override fun getItemCount(): Int {
        return dataList.size
    }

    inner class RecyclerItemViewHolder(parent: View) : RecyclerView.ViewHolder(parent) {
        val nameAbbr: TextView
        var contactName: TextView
        var contactAddress: TextView
        var mainLayout: RelativeLayout

        init {
            nameAbbr = parent.findViewById<TextView>(R.id.name_abbr) as TextView
            contactAddress = parent.findViewById<TextView>(R.id.contact_address) as TextView
            contactName = parent.findViewById<TextView>(R.id.contact_name) as TextView
            mainLayout = parent.findViewById<View>(R.id.rl_main) as RelativeLayout
        }
    }

    interface FavouritesETAListener {
        fun onItemClick(item: FavouritesItem)
    }

    fun getScreenName(): String {
        return Screen.FAVOURITES_ETA
    }
}
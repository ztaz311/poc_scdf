package com.ncs.breeze.common.extensions.android

import android.app.UiModeManager
import android.content.Context
import android.content.Context.UI_MODE_SERVICE
import android.content.res.Configuration
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import androidx.core.os.bundleOf
import com.ncs.breeze.App
import com.ncs.breeze.BuildConfig
import com.ncs.breeze.common.storage.BreezeAppPreference
import com.ncs.breeze.common.storage.BreezeUserPreference
import com.ncs.breeze.common.storage.UserDataPreferenceHelper
import com.breeze.model.constants.Constants
import com.ncs.breeze.common.utils.Utils

fun Context.getDefaultRNNavigationParams(sessionToken: String?) = bundleOf(
    Constants.RN_CONSTANTS.ID_TOKEN to sessionToken,
    Constants.RN_CONSTANTS.BASE_URL to BuildConfig.SERVER_HEADER,
    Constants.RN_CONSTANTS.DEVICE_OS to "Android",
    Constants.RN_CONSTANTS.DEVICE_OS_VERSION to Utils.getDeviceOS(),
    Constants.RN_CONSTANTS.DEVICE_MODEL to Utils.getDeviceModel(),
    Constants.RN_CONSTANTS.APP_VERSION to Utils.getAppVersion(this),
    Constants.RN_DARK_MODE to isDarkThemeOn()
)

fun Context.getRNFragmentNavigationParams(sessionToken: String?, fragmentTag: String?) =
    this.getDefaultRNNavigationParams(sessionToken).apply {
        putString(Constants.RN_CONSTANTS.OPEN_AS, Constants.RN_CONSTANTS.OPEN_AS_FRAGMENT)
        putString(Constants.RN_CONSTANTS.FRAGMENT_ID, fragmentTag)
    }

fun Context.isDarkThemeOn() = resources.configuration.uiMode and
        Configuration.UI_MODE_NIGHT_MASK == Configuration.UI_MODE_NIGHT_YES

val Context.isConnected: Boolean
    get() {
        val connectivityManager =
            getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkCapabilities = connectivityManager.activeNetwork ?: return false
        val actNw =
            connectivityManager.getNetworkCapabilities(networkCapabilities) ?: return false
        return actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) ||
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) ||
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)
    }

fun Context.getSystemUIMode() =
    (this.getSystemService(UI_MODE_SERVICE) as? UiModeManager)?.nightMode

fun Context.getBreezeUserPreference() = BreezeUserPreference.getInstance(this)

fun Context.getUserDataPreference() =
    getBreezeUserPreference().getUserId()?.let { id ->
        UserDataPreferenceHelper(this, id)
    }

fun Context.getAppPreference() = BreezeAppPreference.getInstance(this)

fun Context.getApp() = this.applicationContext as? App

fun Context.getShareBusinessLogicPackage() = getApp()?.shareBusinessLogicPackage

//OBU
fun Context.getOBUConnHelper() = getApp()?.obuConnectionHelper

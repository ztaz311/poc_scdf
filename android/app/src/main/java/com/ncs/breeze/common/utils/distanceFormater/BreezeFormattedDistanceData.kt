package com.ncs.breeze.common.utils.distanceFormater

import com.mapbox.navigation.base.formatter.UnitType
import com.mapbox.navigation.core.formatter.FormattedDistanceData

class BreezeFormattedDistanceData constructor(
    val distance: Double,
    val distanceAsString: String,
    val distanceSuffix: String,
    val unitType: UnitType
) {

    /**
     * Indicates whether some other object is "equal to" this one.
     */
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as FormattedDistanceData

        if (distance != other.distance) return false
        if (distanceAsString != other.distanceAsString) return false
        if (distanceSuffix != other.distanceSuffix) return false
        if (unitType != other.unitType) return false

        return true
    }

    /**
     * Returns a hash code value for the object.
     */
    override fun hashCode(): Int {
        var result = distance.hashCode()
        result = 31 * result + distanceAsString.hashCode()
        result = 31 * result + distanceSuffix.hashCode()
        result = 31 * result + unitType.hashCode()
        return result
    }

    /**
     * The toString implementation
     */
    override fun toString(): String {
        return "FormattedDistanceData(" +
                "distance=$distance, " +
                "distanceAsString=$distanceAsString, " +
                "distanceSuffix=$distanceSuffix, " +
                "unitType=$unitType" +
                ")"
    }
}

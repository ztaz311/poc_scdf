package com.ncs.breeze.di.module.fragment

import com.ncs.breeze.ui.dashboard.fragments.view.ChangeUserNameFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class ChangeUserNameModule {
    @ContributesAndroidInjector
    abstract fun contributeChangeUserNameFragment(): ChangeUserNameFragment
}
package com.ncs.breeze.common.utils.mapper.car_notification

import android.content.Context
import androidx.annotation.DrawableRes
import androidx.core.graphics.drawable.IconCompat
import com.ncs.breeze.R
import com.breeze.model.obu.OBURoadEventData

object CarNavNotificationIconMapper {
    fun getOBURoadEventIcon(
        context: Context,
        data: OBURoadEventData,
        @DrawableRes defaultIcon: Int? = null
    ): IconCompat? =
        when (data.eventType) {
            OBURoadEventData.EVENT_TYPE_TRAFFIC -> R.drawable.ic_android_auto_road_incident_notification
            OBURoadEventData.EVENT_TYPE_SCHOOL_ZONE -> R.drawable.school_zone
            OBURoadEventData.EVENT_TYPE_SILVER_ZONE -> R.drawable.silver_zone
            OBURoadEventData.EVENT_TYPE_ERP_CHARGING -> R.drawable.ic_android_auto_road_erp_blue_notification
            OBURoadEventData.EVENT_TYPE_ERP_SUCCESS -> R.drawable.ic_android_auto_road_erp_blue_notification
            OBURoadEventData.EVENT_TYPE_ERP_FAILURE -> R.drawable.ic_android_auto_road_erp_red_notification
            OBURoadEventData.EVENT_TYPE_PARKING_FEE -> R.drawable.ic_obu_alert_message_parking_success
            OBURoadEventData.EVENT_TYPE_PARKING_SUCCESS -> R.drawable.ic_obu_alert_message_parking_success
            OBURoadEventData.EVENT_TYPE_PARKING_FAILURE -> R.drawable.ic_obu_alert_message_parking_failure
            OBURoadEventData.EVENT_TYPE_EVENT,
            OBURoadEventData.EVENT_TYPE_ROAD_CLOSURE
            -> R.drawable.road_closure
            OBURoadEventData.EVENT_TYPE_ROAD_DIVERSION -> R.drawable.ic_road_diversion
            OBURoadEventData.EVENT_TYPE_GENERAL_MESSAGE -> R.drawable.ic_road_diversion
            OBURoadEventData.EVENT_TYPE_SPEED_CAMERA -> R.drawable.speed_camera
            OBURoadEventData.EVENT_TYPE_FLASH_FLOOD -> R.drawable.flash_flood_xl
            OBURoadEventData.EVENT_TYPE_UNATTENDED_VEHICLE -> R.drawable.unattended_vehicle_icon_xl
            OBURoadEventData.EVENT_TYPE_MISCELLANEOUS -> R.drawable.miscellaneous_xl
            OBURoadEventData.EVENT_TYPE_ROAD_BLOCK -> R.drawable.road_block_xl
            OBURoadEventData.EVENT_TYPE_OBSTACLE -> R.drawable.obstacle_xl
            OBURoadEventData.EVENT_TYPE_ROAD_WORK -> R.drawable.road_work_xl
            OBURoadEventData.EVENT_TYPE_VEHICLE_BREAKDOWN -> R.drawable.vehicle_breakdown_xl
            OBURoadEventData.EVENT_TYPE_MAJOR_ACCIDENT -> R.drawable.major_accident_xl
            OBURoadEventData.EVENT_TYPE_SEASON_PARKING -> R.drawable.season_parking_icon
            OBURoadEventData.EVENT_TYPE_TREE_PRUNING -> R.drawable.tree_pruning_xl
            OBURoadEventData.EVENT_TYPE_YELLOW_DENGUE_ZONE -> R.drawable.exclaimation_warning
            OBURoadEventData.EVENT_TYPE_RED_DENGUE_ZONE -> R.drawable.warning
            OBURoadEventData.EVENT_TYPE_BUS_LANE -> R.drawable.bus_lane_aa_xl
            else -> defaultIcon
        }?.let {
            IconCompat.createWithResource(context, it)
        }
}
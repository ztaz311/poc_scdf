package com.ncs.breeze.car.breeze.model

import com.breeze.model.api.response.amenities.BaseAmenity
import com.mapbox.geojson.Feature

data class CarParkLayerModel(
    var imageName: String,
    var sourceName: String,
    var layerName: String,
    var features: ArrayList<Feature>,
    var carkPark: BaseAmenity
) {}

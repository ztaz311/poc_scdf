package com.ncs.breeze.common.helper.obu.callback

import android.content.Context
import sg.gov.lta.obu.sdk.conn.OBU
import sg.gov.lta.obu.sdk.core.types.OBUError
import sg.gov.lta.obu.sdk.data.interfaces.OBUConnectionStatusListener
import sg.gov.lta.obu.sdk.data.services.OBUSDK
import timber.log.Timber
import java.lang.ref.WeakReference

class OBUConnectionStatusListenerKt(
    context: Context?,
    val vehicleNumber: String,
    val onOBUConnected: (device: OBU) -> Unit = {},
    val onOBUConnectionFailure: (error: OBUError) -> Unit = {},
    val onOBUDisconnected: (device: OBU?, error: OBUError?) -> Unit = { _, _ -> },
) : OBUConnectionStatusListener {
    var isUserCanceled = false
    override fun onOBUConnected(device: OBU) {
        super.onOBUConnected(device)
        Timber.d("OBUConnectionStatusListener onOBUConnected: name=${device.name}, vehicleNumber=$vehicleNumber, isUserCanceled=${isUserCanceled}")
        if (isUserCanceled) {
            OBUSDK.disconnect()
        } else {
            onOBUConnected.invoke(device)
        }
    }

    override fun onOBUConnectionFailure(error: OBUError) {
        super.onOBUConnectionFailure(error)
        Timber.d(
            error,
            "OBUConnectionStatusListener onOBUConnectionFailure: $vehicleNumber - ${error.code} - ${error.message}"
        )
        onOBUConnectionFailure.invoke(error)
    }

    override fun onOBUDisconnected(device: OBU?, error: OBUError?) {
        super.onOBUDisconnected(device, error)
        Timber.d(
            error,
            "OBUConnectionStatusListener onOBUDisconnected: obu=${device?.name}, $vehicleNumber, code=${error?.code}, message=${error?.message}"
        )
        if (!isUserCanceled)
            onOBUDisconnected.invoke(device, error)
    }

}
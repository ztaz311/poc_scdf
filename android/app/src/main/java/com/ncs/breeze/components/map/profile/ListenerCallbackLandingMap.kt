package com.ncs.breeze.components.map.profile

import com.breeze.model.api.response.amenities.BaseAmenity
import com.mapbox.geojson.Point

interface ListenerCallbackLandingMap {
    fun moreInfoButtonClick(ameniti: BaseAmenity)
    fun onDismissedCameraTracking()
    fun onMapOtherPointClicked(point: Point)
    fun onERPInfoRequested(erpID: Int)
    fun poiMoreInfoButtonClick(ameniti: BaseAmenity)
}
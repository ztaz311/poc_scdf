package com.ncs.breeze.car.breeze.screen.navigation

import com.mapbox.androidauto.navigation.lanes.CarLanesImageRenderer
import com.mapbox.androidauto.navigation.maneuver.CarManeuverIconOptions
import com.mapbox.androidauto.navigation.maneuver.CarManeuverIconRenderer
import com.mapbox.androidauto.navigation.maneuver.CarManeuverMapper
import com.breeze.model.DestinationAddressDetails
import com.breeze.model.api.request.ETARequest
import com.breeze.model.api.response.ERPResponseData
import com.breeze.model.constants.Constants
import com.mapbox.api.directions.v5.models.DirectionsResponse
import com.mapbox.api.directions.v5.models.DirectionsRoute
import com.ncs.breeze.car.breeze.MainBreezeCarContext
import com.mapbox.navigation.base.formatter.Rounding
import com.mapbox.navigation.base.formatter.UnitType
import com.mapbox.navigation.ui.maneuver.api.MapboxManeuverApi
import java.util.*

class CarNavigationCarContext(
    val mainCarContext: MainBreezeCarContext,
    val originalDestination: DestinationAddressDetails,
    val destinationAddressDetails: DestinationAddressDetails,
    val route: DirectionsRoute,
    val compressedRouteOriginalResp: ByteArray,
    val originalDirectionsResponse: DirectionsResponse,
    val erpData: ERPResponseData.ERPResponse?,
    val etaRequest: ETARequest?
) {
    /** MapCarContext **/
    val carContext = mainCarContext.carContext
    val mapboxCarMap = mainCarContext.mapboxCarMap
    val distanceFormatter = mainCarContext.distanceFormatter

    /** NavigationCarContext **/
    val carDistanceFormatter = CarDistanceFormatter(
        UnitType.METRIC,
        Rounding.INCREMENT_FIFTY,
        Locale(Constants.MAPBOX_DISTANCE_FORMATTER_LOCALE)
    )
    val carLaneImageGenerator = CarLanesImageRenderer(carContext)
    val navigationInfoMapper = CarNavigationInfoMapper(
        CarManeuverMapper,
        CarManeuverIconRenderer(CarManeuverIconOptions.Builder(carContext).build()),
        carLaneImageGenerator,
        carDistanceFormatter
    )
    val maneuverApi: MapboxManeuverApi by lazy {
        MapboxManeuverApi(distanceFormatter)
    }
    val tripProgressMapper = CarNavigationEtaMapper(carDistanceFormatter)
}

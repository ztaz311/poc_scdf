package com.ncs.breeze.helper.marker

import com.breeze.model.api.response.amenities.BaseAmenity
import com.ncs.breeze.common.utils.ParkingIconUtils
import com.ncs.breeze.common.utils.ParkingIconUtils.ImageIconType
import com.ncs.breeze.common.utils.ParkingIconUtils.ParkingMarkerConstants
import com.google.gson.JsonObject
import com.ncs.breeze.components.layermanager.MarkerLayerManager

object MarkerLayerIconHelper {


    fun handleShowCarParkSelectedIcon(
        it: BaseAmenity,
        properties: JsonObject,
        nightModeEnabled: Boolean,
        showDestination: Boolean,
        voucherEnabled: Boolean,
    ) {
        //Ignore 'nightModeEnabled' variable as it will automatically load
        //from 'drawable-night' folder in Android
        addParkingIconProperty(
            it = it,
            showDestination = showDestination,
            selected = true,
            voucherEnabled,
            properties = properties
        )
    }

    fun handleCarParkUnSelectedIcon(
        it: BaseAmenity,
        properties: JsonObject,
        nightModeEnabled: Boolean,
        showDestination: Boolean,
        voucherEnabled: Boolean,
    ) {
        //Ignore 'nightModeEnabled' variable as it will automatically load
        //from 'drawable-night' folder in Android
        addParkingIconProperty(
            it = it,
            showDestination = showDestination,
            selected = false,
            voucherEnabled,
            properties = properties
        )
    }

    private fun addParkingIconProperty(
        it: BaseAmenity,
        showDestination: Boolean,
        selected: Boolean,
        voucherEnabled: Boolean,
        properties: JsonObject
    ) {
        val DESTINATIONTYPE = if (showDestination && it.destinationCarPark ?: false) {
            ParkingMarkerConstants.MARKER_DESTINATION
        } else {
            ParkingMarkerConstants.MARKER_NONDESTINATION
        }
        val SELECTEDTYPE = if (selected) {
            ParkingMarkerConstants.MARKER_SELECTED
        } else {
            ParkingMarkerConstants.MARKER_NOTSELECTED
        }
        val MARKERTYPE = ParkingIconUtils.getParkingMarkerTypeString(it)
        val VOUCHERTYPE =
            if (voucherEnabled && it.hasVouchers) ParkingMarkerConstants.VOUCHER + "_" else ""

        properties.addProperty(
            MarkerLayerManager.ICON_KEY,
            ParkingMarkerConstants.PARKINGMARKER + "_" + VOUCHERTYPE + MARKERTYPE + "_" + DESTINATIONTYPE + "_" + SELECTEDTYPE
        )
    }

    private fun isParkingAvail(it: BaseAmenity) =
        ((it.availablelot == null || it.availablelot == -1) && it.hasRatesInfo == true) || (it.availablelot ?: 0 > 0)

    fun handleUnSelectedIcon(
        properties: JsonObject, nightModeEnabled: Boolean
    ) {

        if (nightModeEnabled) {
            properties.addProperty(
                MarkerLayerManager.ICON_KEY,
                ImageIconType.UNSELECTED_NIGHT.type
            )
        } else {
            properties.addProperty(
                MarkerLayerManager.ICON_KEY,
                ImageIconType.UNSELECTED.type
            )
        }

    }

    fun handleSelectedIcon(
        properties: JsonObject, nightModeEnabled: Boolean
    ) {

        if (nightModeEnabled) {
            properties.addProperty(
                MarkerLayerManager.ICON_KEY,
                ImageIconType.SELECTED_NIGHT.type
            )
        } else {
            properties.addProperty(
                MarkerLayerManager.ICON_KEY,
                ImageIconType.SELECTED.type
            )
        }
    }

}
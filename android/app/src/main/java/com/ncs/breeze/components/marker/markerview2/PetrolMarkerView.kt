package com.ncs.breeze.components.marker.markerview2

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import androidx.core.os.bundleOf
import com.breeze.model.api.response.amenities.BaseAmenity
import com.breeze.model.constants.AmenityType
import com.breeze.model.extensions.dpToPx
import com.facebook.react.bridge.Arguments
import com.google.android.gms.maps.model.LatLng
import com.mapbox.geojson.Point
import com.mapbox.maps.EdgeInsets
import com.mapbox.maps.MapboxMap
import com.ncs.breeze.R
import com.ncs.breeze.common.utils.BreezeMapZoomUtil
import com.ncs.breeze.databinding.MapTooltipPetrolBinding
import com.ncs.breeze.ui.base.BaseActivity


class PetrolMarkerView constructor(
    latLng: LatLng,
    mapboxMap: MapboxMap,
    val activity: Activity,
    itemId: String? = null,
    val limitExceeded: Boolean
) : MarkerView2(mapboxMap = mapboxMap, itemId) {

    private val viewBinding =
        MapTooltipPetrolBinding.inflate(LayoutInflater.from(activity), null, false)

    init {
        mLatLng = latLng

        mViewMarker = viewBinding.root
    }

    var currentAmenities: BaseAmenity? = null

    fun handleMarkerClick(
        isSelected: Boolean,
        item: BaseAmenity,
        addStopClick: (BaseAmenity, BaseActivity.EventSource) -> Boolean,
        removeStopClick: (BaseAmenity, BaseActivity.EventSource) -> Boolean,
        markerClick: () -> Unit,
    ) {
        currentAmenities = item
        viewBinding.tvProvider.text = item.provider
        viewBinding.tvAddress.text = item.address
        viewBinding.buttonToggleStop.setOnClickListener {
            resetView()
            if (isSelected) {
                if (removeStopClick.invoke(item, BaseActivity.EventSource.NONE)) {
                    viewBinding.buttonToggleStop.text = activity.getString(R.string.add_stop)
                    viewBinding.buttonToggleStop.setBackgroundResource(R.drawable.bg_corner_40)
                }
            } else {
                if (addStopClick.invoke(item, BaseActivity.EventSource.NONE)) {
                    viewBinding.buttonToggleStop.setBackgroundResource(R.drawable.amenity_navigate_remove_button)
                    viewBinding.buttonToggleStop.text = activity.getString(R.string.remove_stop)
                }
            }
        }

        if (isSelected) {
            viewBinding.buttonToggleStop.setBackgroundResource(R.drawable.amenity_navigate_remove_button)
            viewBinding.buttonToggleStop.text = activity.getString(R.string.remove_stop)
        } else {
            viewBinding.buttonToggleStop.text = activity.getString(R.string.add_stop)

            if (limitExceeded) {
                viewBinding.buttonToggleStop.setBackgroundResource(R.drawable.common_navigate_button_grey)
            } else {
                viewBinding.buttonToggleStop.setBackgroundResource(R.drawable.bg_corner_40)
            }
        }
        renderMarkerToolTip(markerClick, item)
    }

    private fun createShareLocationData(item: BaseAmenity) = Arguments.fromBundle(
        bundleOf(
            "address1" to if (item.amenityType == AmenityType.CARPARK) item.name else item.address,
            "latitude" to (item.lat?.toString() ?: ""),
            "longitude" to (item.long?.toString() ?: ""),
            "amenityId" to item.id,
            "amenityType" to item.amenityType,
            "name" to item.name,
        )
    )

    private fun renderMarkerToolTip(
        markerClick: () -> Unit,
        item: BaseAmenity
    ) {
        markerClick.invoke()
        BreezeMapZoomUtil.recenterToPoint(mapboxMap,
            Point.fromLngLat(item.long!!, item.lat!!),
            EdgeInsets(200.0.dpToPx(), 10.0.dpToPx(), 110.0.dpToPx(), 10.0.dpToPx()),
            animationEndCB = {
                viewBinding.root.bringToFront()
            }
        )
    }

    override fun showMarker() {
        super.showMarker()
        viewBinding.root.visibility = View.VISIBLE
    }

    override fun hideMarker() {
        super.hideMarker()
        viewBinding.root.visibility = View.INVISIBLE

    }


    override fun updateTooltipPosition() {
        mLatLng?.let { location ->
            val screenCoordinate = mapboxMap.pixelForCoordinate(
                Point.fromLngLat(
                    location.longitude,
                    location.latitude
                )
            )
            mViewMarker?.let {
                it.measure(
                    View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                    View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
                );
                it.x = screenCoordinate.x.toFloat() - (it.measuredWidth / 2)
                it.y = screenCoordinate.y.toFloat() - it.measuredHeight
            }
        }
    }

}
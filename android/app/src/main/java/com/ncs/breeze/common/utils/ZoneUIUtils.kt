package com.ncs.breeze.common.utils

import com.breeze.model.NavigationZone
import com.ncs.breeze.R

object ZoneUIUtils {

    fun getZoneTypeString(zonePriority: Int, template: String? = null): Int {
        when (zonePriority) {
            NavigationZone.ERP_ZONE.priority ->
                return R.string.erp

            NavigationZone.SCHOOL_ZONE.priority ->
                return R.string.school_zone

            NavigationZone.SILVER_ZONE.priority ->
                return R.string.silver_zone

            NavigationZone.ACCIDENT.priority ->
                return R.string.accident_zone

            NavigationZone.VEHICLE_BREAKDOWN.priority ->
                return R.string.vehicle_bk_zone

            NavigationZone.ROAD_WORK.priority ->
                return R.string.roadwork_zone

            NavigationZone.OBSTACLE.priority ->
                return R.string.obstacle_zone

            NavigationZone.ROAD_BLOCK.priority ->
                return R.string.roadblock_zone

            NavigationZone.MISCELLANEOUS.priority ->
                return R.string.miscellaneous_zone

            NavigationZone.UNATTENDED_VEHICLE.priority ->
                return R.string.unattended_zone

            NavigationZone.YELLOW_DENGUE_ZONE.priority ->
                return R.string.yellow_dengue_zone

            NavigationZone.RED_DENGUE_ZONE.priority ->
                return R.string.red_dengue_zone

            NavigationZone.FLASH_FLOOD.priority ->
                return R.string.flash_flood

            NavigationZone.ROAD_CLOSURE.priority ->
                return R.string.road_closure

            NavigationZone.MAJOR_ACCIDENT.priority ->
                return R.string.major_accident

            NavigationZone.HEAVY_TRAFFIC.priority ->
                return R.string.heavy_traffic

            NavigationZone.SPEED_CAMERA.priority ->
                return R.string.speed_camera

            NavigationZone.TREE_PRUNING.priority ->
                return R.string.tree_pruning

            NavigationZone.SEASON_PARKING.priority ->
                return R.string.season_parking_zone

            NavigationZone.ROAD_DIVERSION.priority ->
                return R.string.road_diversion

            NavigationZone.BUS_LANE.priority ->
                return R.string.road_diversion
        }
        return R.string.miscellaneous_zone
    }

    fun getZoneTypeBGColor(zonePriority: Int) =
        when (zonePriority) {
            NavigationZone.ERP_ZONE.priority -> R.color.navigation_erp_bg
            NavigationZone.SCHOOL_ZONE.priority,
            NavigationZone.SILVER_ZONE.priority,
            NavigationZone.BUS_LANE.priority -> R.color.navigation_zone_bg

            NavigationZone.ACCIDENT.priority,
            NavigationZone.VEHICLE_BREAKDOWN.priority,
            NavigationZone.ROAD_WORK.priority,
            NavigationZone.OBSTACLE.priority,
            NavigationZone.ROAD_BLOCK.priority,
            NavigationZone.MISCELLANEOUS.priority,
            NavigationZone.UNATTENDED_VEHICLE.priority,
            NavigationZone.YELLOW_DENGUE_ZONE.priority,
            NavigationZone.RED_DENGUE_ZONE.priority -> R.color.navigation_alert_bg

            NavigationZone.FLASH_FLOOD.priority,
            NavigationZone.ROAD_CLOSURE.priority,
            NavigationZone.MAJOR_ACCIDENT.priority,
            NavigationZone.SPEED_CAMERA.priority,
            NavigationZone.TREE_PRUNING.priority,
            NavigationZone.SEASON_PARKING.priority,
            NavigationZone.ROAD_DIVERSION.priority -> R.color.navigation_incidents_alt_bg

            else -> R.color.navigation_alert_bg
        }

    fun getZoneTypeImage(zonePriority: Int, fromAA: Boolean = false): Int {
        when (zonePriority) {
            NavigationZone.ERP_ZONE.priority ->
                return R.drawable.erp_icon

            NavigationZone.SCHOOL_ZONE.priority ->
                return R.drawable.school_zone_xl

            NavigationZone.SILVER_ZONE.priority ->
                return R.drawable.silver_zone_xl

            NavigationZone.ACCIDENT.priority ->
                return R.drawable.accident_xl

            NavigationZone.VEHICLE_BREAKDOWN.priority ->
                return R.drawable.vehicle_breakdown_xl

            NavigationZone.ROAD_WORK.priority ->
                return R.drawable.road_work_xl

            NavigationZone.OBSTACLE.priority ->
                return R.drawable.obstacle_xl

            NavigationZone.ROAD_BLOCK.priority ->
                return R.drawable.road_block_xl

            NavigationZone.MISCELLANEOUS.priority ->
                return R.drawable.miscellaneous_xl

            NavigationZone.UNATTENDED_VEHICLE.priority ->
                return R.drawable.unattended_vehicle_icon_xl

            NavigationZone.YELLOW_DENGUE_ZONE.priority ->
                return R.drawable.exclaimation_warning

            NavigationZone.RED_DENGUE_ZONE.priority ->
                return R.drawable.warning

            NavigationZone.HEAVY_TRAFFIC.priority ->
                return R.drawable.ic_heavy_traffic_alert

            NavigationZone.FLASH_FLOOD.priority ->
                return R.drawable.flash_flood_xl

            NavigationZone.ROAD_CLOSURE.priority ->
                return R.drawable.road_closure_xl

            NavigationZone.MAJOR_ACCIDENT.priority ->
                return R.drawable.major_accident_xl

            NavigationZone.SPEED_CAMERA.priority ->
                return R.drawable.speed_camera_xl

            NavigationZone.TREE_PRUNING.priority ->
                return R.drawable.tree_pruning_xl

            NavigationZone.SEASON_PARKING.priority ->
                return R.drawable.season_parking_icon

            NavigationZone.ROAD_DIVERSION.priority ->
                return R.drawable.ic_road_diversion

            NavigationZone.BUS_LANE.priority ->
                return if (fromAA) R.drawable.bus_lane_aa_xl
                else R.drawable.bus_lane_xl
        }
        return R.drawable.miscellaneous_xl
    }
}
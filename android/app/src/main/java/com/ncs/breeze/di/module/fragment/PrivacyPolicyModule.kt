package com.ncs.breeze.di.module.fragment

import com.ncs.breeze.ui.dashboard.fragments.view.PrivacyPolicyFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class PrivacyPolicyModule {
    @ContributesAndroidInjector
    abstract fun contributePrivacyPolicyFragment(): PrivacyPolicyFragment
}
package com.ncs.breeze.ui.navigation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import com.ncs.breeze.common.analytics.Screen
import com.ncs.breeze.common.constant.RNScreen
import com.ncs.breeze.common.extensions.android.getRNFragmentNavigationParams
import com.breeze.model.constants.AmenityType.CARPARK
import com.ncs.breeze.common.remote.MyServiceInterceptor
import com.ncs.breeze.common.rxbus.RxBus
import com.ncs.breeze.common.rxbus.RxEvent
import com.breeze.model.constants.Constants
import com.ncs.breeze.databinding.FragmentTbtCarParkListRnBinding
import com.ncs.breeze.di.Injectable
import com.ncs.breeze.ui.rn.CustomReactFragment
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class TBTCarParkListRNFragment : Fragment(), Injectable {

    @Inject
    lateinit var myServiceInterceptor: MyServiceInterceptor

    lateinit var binding: FragmentTbtCarParkListRnBinding

    private val compositeDisposable = CompositeDisposable()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentTbtCarParkListRnBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onStart() {
        super.onStart()
        listenRXEvents()
    }

    override fun onStop() {
        super.onStop()
        compositeDisposable.clear()
    }

    private fun listenRXEvents() {
        compositeDisposable.addAll(
            RxBus.listen(RxEvent.CloseCarparkListScreen::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    if (lifecycle.currentState.isAtLeast(Lifecycle.State.STARTED))
                        closeCarparkListScreen()
                },
            RxBus.listen(RxEvent.NoCarparkAlert::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    if (lifecycle.currentState.isAtLeast(Lifecycle.State.STARTED)) {
                        closeCarparkListScreen()
                        (activity as? TurnByTurnNavigationActivity)
                            ?.showNoNearbyAmenitiesAlert(CARPARK)
                    }
                },
        )
    }

    private fun closeCarparkListScreen() {
        childFragmentManager.findFragmentById(binding.rnScreenContainer.id)?.let {
            childFragmentManager.beginTransaction().remove(it).commitNow()
        }
    }

    /**
     * @param arrivalTime: unix estimated time to reach to destination in seconds
     * */
    fun showCarParkListRNScreen(arrivalTime: Long) {
        childFragmentManager.findFragmentById(binding.rnScreenContainer.id)
            ?.takeIf { lifecycle.currentState.isAtLeast(Lifecycle.State.STARTED) }
            ?.let {
                childFragmentManager.beginTransaction().remove(it).commitNow()
            }
        if (!lifecycle.currentState.isAtLeast(Lifecycle.State.RESUMED)) return
        val destination =
            (activity as? TurnByTurnNavigationActivity)?.getDestinationLocation() ?: return
        val navigationParams = requireActivity().getRNFragmentNavigationParams(
            sessionToken = myServiceInterceptor.getSessionToken(),
            fragmentTag = RNScreen.CARPARK_LIST
        ).apply {
            putLong("arrival_time", arrivalTime)
            putBundle(
                "location", bundleOf(
                    "lat" to "${destination.latitude()}",
                    "long" to "${destination.longitude()}",
                )
            )
            putString(Constants.RN_FROM_SCREEN, Screen.NAVIGATION)
        }
        val initialProperties = bundleOf(
            Constants.RN_TO_SCREEN to RNScreen.CARPARK_LIST,
            Constants.RN_CONSTANTS.NAVIGATION_PARAMS to navigationParams
        )

        val reactNativeFragment = CustomReactFragment.newInstance(
            componentName = Constants.RN_CONSTANTS.COMPONENT_NAME,
            launchOptions = initialProperties,
            cleanOnDestroyed = false
        )

        //addFragment(reactNativeFragment, null, Constants.TAGS.TRAVEL_LOG_TAG)
        childFragmentManager
            .beginTransaction()
            .replace(binding.rnScreenContainer.id, reactNativeFragment, RNScreen.CARPARK_LIST)
            .commit()
    }
}

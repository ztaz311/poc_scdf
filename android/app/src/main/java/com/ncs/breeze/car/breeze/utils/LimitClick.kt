package com.ncs.breeze.car.breeze.utils

import android.os.SystemClock

object LimitClick {

    private var lastTimeClicked: Long = 0
    private var interval: Int = 500

    fun handleSafe(onSafeClick: () -> Unit) {
        if (SystemClock.elapsedRealtime() - lastTimeClicked < interval) {
            return
        }
        onSafeClick()
        lastTimeClicked = SystemClock.elapsedRealtime()
    }
}
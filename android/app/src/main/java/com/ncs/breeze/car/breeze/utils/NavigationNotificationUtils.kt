package com.ncs.breeze.car.breeze.utils

import android.annotation.SuppressLint
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.ComponentName
import android.content.Context.NOTIFICATION_SERVICE
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.car.app.CarContext
import androidx.car.app.notification.CarAppExtender
import androidx.car.app.notification.CarPendingIntent
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.graphics.drawable.IconCompat
import com.ncs.breeze.car.BreezeCarAppService
import com.ncs.breeze.car.breeze.MainBreezeCarContext
import com.ncs.breeze.car.breeze.receivers.BreezeCarNotificationReceiver
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.Event
import com.ncs.breeze.common.analytics.Screen
import com.breeze.model.DataNotificationCruiseMode
import com.breeze.model.NavigationZone
import com.breeze.model.obu.OBUParkingAvailability
import com.breeze.model.obu.OBUTravelTimeData
import com.breeze.model.constants.Constants
import com.ncs.breeze.common.utils.ZoneUIUtils
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

object NavigationNotificationUtils {

    private const val DEFAULT_TITLE = "Ahead"
    private const val NOTIFICATION_ID = 3
    private const val NAVIGATION_TBT_NOTIFICATION_ID = 4
    private const val NOTIFICATION_CONGESTION_NOTIFICATION_ID = 5
    private const val NOTIFICATION_PARKING_AVAILABILITY_NOTIFICATION_ID = 6
    private const val NOTIFICATION_ROAD_EVENT_NOTIFICATION_ID = 7
    private const val NOTIFICATION_ID_TEST = 99
    private const val NOTIFICATION_CHANNEL_ID = "CRUISE_NOTIFICATION_CHANNEL_ID"
    private val NOTIFICATION_CHANNEL_NAME: CharSequence = "Breeze Car Notification"

    private const val NAVIGATION_TBT_NOTIFICATION_CHANNEL_ID = "NAVIGATION_TURN_BY_TURN_ID"
    private val NAVIGATION_TBT_NOTIFICATION_CHANNEL_NAME: CharSequence =
        "Breeze Car TurnbyTurn Notification"

    private const val NAVIGATION_CONGESTION_NOTIFICATION_CHANNEL_ID = "NAVIGATION_CONGESTION_ID"
    private const val NAVIGATION_CONGESTION_NOTIFICATION_CHANNEL_NAME =
        "Breeze Car Congestion Notification"
    private const val NAVIGATION_PARKING_AVAILABILITY_NOTIFICATION_CHANNEL_ID =
        "NAVIGATION_PARKING_AVAILABILITY_ID"
    private const val NAVIGATION_PARKING_AVAILABILITY_NOTIFICATION_CHANNEL_NAME =
        "Breeze Car Parking Availability Notification"
    private const val NAVIGATION_ROAD_EVENT_NOTIFICATION_CHANNEL_ID = "NAVIGATION_ROAD_EVENT_ID"
    private const val NAVIGATION_ROAD_EVENT_NOTIFICATION_CHANNEL_NAME =
        "Breeze Car Road Event Notification"

    lateinit var mainBreezeCarContext: MainBreezeCarContext
    private var nRoadObjectId: String? = null;
    private var nRoadObjectZone: NavigationZone? = null;
    private var nRoadObjectSpeedState: RoadObjectSpeedState = RoadObjectSpeedState.SPEED_LIMIT_NONE;
    fun setUpNotification(pCarContext: MainBreezeCarContext) {
        mainBreezeCarContext = pCarContext
        initNotifications(mainBreezeCarContext.carContext)
    }


    fun resetNavigationNotification(roadObjectId: String) {
        if (nRoadObjectId == roadObjectId) {
            updateNotificationStateObjects(null, null, RoadObjectSpeedState.SPEED_LIMIT_NONE);
        }
        CoroutineScope(Dispatchers.Default).launch {
            NotificationManagerCompat.from(mainBreezeCarContext.carContext)
                .cancel(NOTIFICATION_ID)
        }
    }


    fun showNavigationNotification(
        dataShowNotification: DataNotificationCruiseMode, roadObjectId: String,
        zone: NavigationZone, distanceRaw: Int, location: String?
    ) {

        Log.d("bangnv", "showNotificationCruiseNotification - $distanceRaw")
        val fullScreenIntent = Intent()
        val fullScreenPendingIntent = PendingIntent.getActivity(
            mainBreezeCarContext.carContext, 0,
            fullScreenIntent,
            PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_MUTABLE
        )

        val content = if (zone.maxDistanceMtr <= 300) dataShowNotification.mDistance else location
            ?: dataShowNotification.mDistance
        val title = mainBreezeCarContext.carContext.resources.getString(
            ZoneUIUtils.getZoneTypeString(dataShowNotification.zonePriority)
        )
        val notificationBuilder =
            NotificationCompat.Builder(
                mainBreezeCarContext.carContext,
                NOTIFICATION_CHANNEL_ID
            )
                .setContentTitle(title)
                .setContentText(content)
                .setSmallIcon(ZoneUIUtils.getZoneTypeImage(dataShowNotification.zonePriority, true))
                .setColor(
                    mainBreezeCarContext.carContext.resources.getColor(
                        ZoneUIUtils.getZoneTypeBGColor(dataShowNotification.zonePriority)
                    )
                )
                .setCategory(NotificationCompat.CATEGORY_NAVIGATION)
                .extend(
                    CarAppExtender.Builder()
                        .setSmallIcon(ZoneUIUtils.getZoneTypeImage(dataShowNotification.zonePriority, true))
                        .setContentText(content)
                        .setContentIntent(fullScreenPendingIntent)
                        .setContentTitle(title)
                        .setImportance(NotificationManagerCompat.IMPORTANCE_HIGH)
                        .build()
                )


        CoroutineScope(Dispatchers.Default).launch {
            notifyNavigation(
                roadObjectId,
                zone,
                notificationBuilder,
                RoadObjectSpeedState.SPEED_LIMIT_NONE
            )
        }
    }


    fun showTurnByTurnNavigationNotification(title: String, content: String, icon: IconCompat?) {
        val fullScreenPendingIntent = PendingIntent.getBroadcast(
            mainBreezeCarContext.carContext,
            BreezeCarNotificationReceiver.ACTION_OPEN_APP.hashCode(),
            Intent(BreezeCarNotificationReceiver.ACTION_OPEN_APP)
                .setComponent(
                    ComponentName(
                        mainBreezeCarContext.carContext,
                        BreezeCarNotificationReceiver::class.java
                    )
                ),
            PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_MUTABLE
        )


        val carAppExtenderBuilder = CarAppExtender.Builder()
            .setContentText(content)
            .setContentIntent(fullScreenPendingIntent)
            .setImportance(NotificationManagerCompat.IMPORTANCE_HIGH)

        val notificationBuilder =
            NotificationCompat.Builder(
                mainBreezeCarContext.carContext,
                NAVIGATION_TBT_NOTIFICATION_CHANNEL_ID
            )
                .setContentTitle(title)
                .setContentText(content)
                .setCategory(NotificationCompat.CATEGORY_NAVIGATION)
                .setOnlyAlertOnce(true)
                .setOngoing(true)
        if (icon !== null) {
            notificationBuilder.setSmallIcon(icon)
        }
        notificationBuilder.extend(carAppExtenderBuilder.build())
        NotificationManagerCompat.from(mainBreezeCarContext.carContext)
            .notify(NAVIGATION_TBT_NOTIFICATION_ID, notificationBuilder.build())
    }

    fun showRoadEventNotification(
        title: String,
        content: String,
        icon: IconCompat,
        screenName: String
    ) {
        val fullScreenPendingIntent = CarPendingIntent.getCarApp(
            mainBreezeCarContext.carContext,
            0,
            Intent().setComponent(
                ComponentName(
                    mainBreezeCarContext.carContext,
                    BreezeCarAppService::class.java
                )
            ),
            PendingIntent.FLAG_UPDATE_CURRENT
        )

        val notificationBuilder = makeNotificationBuilder(
            title,
            content,
            icon,
            fullScreenPendingIntent,
            NAVIGATION_ROAD_EVENT_NOTIFICATION_CHANNEL_ID
        )
        CoroutineScope(Dispatchers.Default).launch {
            triggerAutoDismissNotification(
                notificationBuilder,
                NOTIFICATION_ROAD_EVENT_NOTIFICATION_ID,
                5000,
                screenName
            )
        }
    }

    fun showCongestionAlert(
        title: String,
        content: String,
        icon: IconCompat,
        data: Array<OBUTravelTimeData>,
        screenName: String
    ) {
        val extraData = Bundle()
        extraData.putSerializable(Constants.CONGESTION_ALERT_LIST, data)
        extraData.putString(Constants.CONGESTION_ALERT_TITLE, title)
        extraData.putString(Constants.CONGESTION_ALERT_SCREEN, screenName)
        val fullScreenPendingIntent = CarPendingIntent.getCarApp(
            mainBreezeCarContext.carContext,
            0,
            Intent().setComponent(
                ComponentName(
                    mainBreezeCarContext.carContext,
                    BreezeCarAppService::class.java
                )
            ).putExtra(Constants.CONGESTION_ALERT_EXTRA_DATA, extraData),
            PendingIntent.FLAG_UPDATE_CURRENT
        )

        val notificationBuilder = makeNotificationBuilder(
            title,
            content,
            icon,
            fullScreenPendingIntent,
            NAVIGATION_CONGESTION_NOTIFICATION_CHANNEL_ID
        )
        CoroutineScope(Dispatchers.Default).launch {
            triggerAutoDismissNotification(
                notificationBuilder,
                NOTIFICATION_CONGESTION_NOTIFICATION_ID,
                5000,
                screenName
            )
        }
    }

    fun showParkingAvailabilityAlert(
        title: String,
        content: String,
        icon: IconCompat,
        data: Array<OBUParkingAvailability>,
        screenName: String
    ) {
        val extraData = Bundle()
        extraData.putSerializable(Constants.PARKING_AVAILABILITY_LIST, data)
        extraData.putString(Constants.PARKING_AVAILABILITY_TITLE, title)
        extraData.putString(Constants.PARKING_AVAILABILITY_SCREEN, screenName)
        val fullScreenPendingIntent = CarPendingIntent.getCarApp(
            mainBreezeCarContext.carContext,
            0,
            Intent().setComponent(
                ComponentName(
                    mainBreezeCarContext.carContext,
                    BreezeCarAppService::class.java
                )
            ).putExtra(Constants.PARKING_AVAILABILITY_EXTRA_DATA, extraData),
            PendingIntent.FLAG_UPDATE_CURRENT
        )

        val notificationBuilder = makeNotificationBuilder(
            title,
            content,
            icon,
            fullScreenPendingIntent,
            NAVIGATION_PARKING_AVAILABILITY_NOTIFICATION_CHANNEL_ID
        )
        CoroutineScope(Dispatchers.Default).launch {
            triggerAutoDismissNotification(
                notificationBuilder,
                NOTIFICATION_PARKING_AVAILABILITY_NOTIFICATION_ID,
                5000,
                screenName
            )
        }
    }

    private fun makeNotificationBuilder(
        title: String,
        content: String,
        icon: IconCompat,
        fullScreenPendingIntent: PendingIntent,
        channelId: String
    ): NotificationCompat.Builder {
        return NotificationCompat.Builder(
            mainBreezeCarContext.carContext,
            channelId
        )
            .setContentTitle(title)
            .setContentText(content)
            .setSmallIcon(icon)
            .setCategory(NotificationCompat.CATEGORY_NAVIGATION)
            .extend(
                CarAppExtender.Builder()
                    .setSmallIcon(icon.resId)
                    .setContentText(content)
                    .setContentIntent(fullScreenPendingIntent)
                    .setContentTitle(title)
                    .setImportance(NotificationManagerCompat.IMPORTANCE_HIGH)
                    .build()
            )
    }

    private suspend fun triggerAutoDismissNotification(
        notificationBuilder: NotificationCompat.Builder,
        notificationId: Int,
        delayDuration: Long,
        screenName: String
    ) {
        NotificationManagerCompat.from(mainBreezeCarContext.carContext).cancel(notificationId)
        NotificationManagerCompat.from(mainBreezeCarContext.carContext)
            .notify(notificationId, notificationBuilder.build())
        delay(delayDuration)
        NotificationManagerCompat.from(mainBreezeCarContext.carContext).cancel(notificationId)
        when (screenName) {
            Screen.AA_DHU_NAVIGATION -> {
                Analytics.logPopupEvent(
                    Event.AA_POPUP_NAVIGATION,
                    Event.AA_POPUP_CLOSE,
                    screenName
                )
                Analytics.logPopupEvent(
                    Event.AA_POPUP_NAVIGATION,
                    "[dhu],[popup_navigation_notification]:close",
                    screenName
                )
            }

            Screen.AA_DHU_CRUISE -> {
                Analytics.logPopupEvent(
                    Event.AA_POPUP_CRUISE,
                    Event.AA_POPUP_CLOSE,
                    screenName
                )

                Analytics.logPopupEvent(
                    Event.AA_POPUP_CRUISE,
                    "[dhu],[popup_cruise_notification]:close",
                    screenName
                )
            }
        }
    }

    fun cancelTurnByTurnNavigationNotification() {
        NotificationManagerCompat.from(mainBreezeCarContext.carContext)
            .cancel(NAVIGATION_TBT_NOTIFICATION_ID)
    }

    private suspend fun notifyNavigation(
        roadObjectId: String,
        zone: NavigationZone,
        notificationBuilder: NotificationCompat.Builder,
        speedState: RoadObjectSpeedState
    ) {
        triggerShortNotification(roadObjectId, zone, speedState, notificationBuilder, true)
    }

    private suspend fun triggerShortNotification(
        roadObjectId: String,
        zone: NavigationZone,
        speedState: RoadObjectSpeedState,
        notificationBuilder: NotificationCompat.Builder,
        cancelPrevious: Boolean
    ) {
        updateNotificationStateObjects(roadObjectId, zone, speedState);
        if (cancelPrevious) {
            NotificationManagerCompat.from(mainBreezeCarContext.carContext)
                .cancel(NOTIFICATION_ID)
        }
        NotificationManagerCompat.from(mainBreezeCarContext.carContext)
            .notify(NOTIFICATION_ID, notificationBuilder.build())
        delay(4000)
        NotificationManagerCompat.from(mainBreezeCarContext.carContext)
            .cancel(NOTIFICATION_ID)
    }

    private fun updateNotificationStateObjects(
        roadObjectId: String?,
        zone: NavigationZone?,
        speedState: RoadObjectSpeedState
    ) {
        nRoadObjectId = roadObjectId
        nRoadObjectZone = zone
        nRoadObjectSpeedState = speedState
    }

    @SuppressLint("UnsafeNewApiCall")
    private fun initNotifications(context: CarContext) {
        createNotificationChannel(
            context,
            NOTIFICATION_CHANNEL_ID,
            NOTIFICATION_CHANNEL_NAME
        )
        createNotificationChannel(
            context,
            NAVIGATION_TBT_NOTIFICATION_CHANNEL_ID,
            NAVIGATION_TBT_NOTIFICATION_CHANNEL_NAME
        )
        createNotificationChannel(
            context,
            NAVIGATION_CONGESTION_NOTIFICATION_CHANNEL_ID,
            NAVIGATION_CONGESTION_NOTIFICATION_CHANNEL_NAME
        )
        createNotificationChannel(
            context,
            NAVIGATION_PARKING_AVAILABILITY_NOTIFICATION_CHANNEL_ID,
            NAVIGATION_PARKING_AVAILABILITY_NOTIFICATION_CHANNEL_NAME
        )
        createNotificationChannel(
            context,
            NAVIGATION_ROAD_EVENT_NOTIFICATION_CHANNEL_ID,
            NAVIGATION_ROAD_EVENT_NOTIFICATION_CHANNEL_NAME
        )
    }

    private fun createNotificationChannel(
        context: CarContext,
        channelId: String,
        channelName: CharSequence
    ) {
        val navChannel = NotificationChannel(
            channelId,
            channelName,
            NotificationManager.IMPORTANCE_LOW
        )
        navChannel.setSound(null, null) // Disable sound for this channel
        val notificationManager: NotificationManager =
            context.getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.createNotificationChannel(navChannel)
    }

    enum class RoadObjectSpeedState {
        SPEED_LIMIT_NONE,
        SPEED_LIMIT_300,
        SPEED_LIMIT_150,
    }

}


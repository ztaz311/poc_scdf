package com.ncs.breeze.ui.base

import android.app.Application
import android.text.TextUtils
import androidx.lifecycle.AndroidViewModel
import com.breeze.model.api.ErrorData
import com.breeze.model.api.request.LogAppVersionRequest
import com.breeze.model.api.request.RegisterDeviceRequest
import com.breeze.model.api.response.SettingsResponse
import com.breeze.model.constants.Constants
import com.breeze.model.constants.INSTABUG
import com.breeze.model.enums.AmplifyIdentityProviderType
import com.breeze.model.enums.CarParkViewOption
import com.breeze.model.enums.RoutePreference
import com.breeze.model.enums.UserTheme
import com.google.firebase.crashlytics.ktx.crashlytics
import com.google.firebase.ktx.Firebase
import com.google.gson.JsonElement
import com.instabug.library.Instabug
import com.ncs.breeze.App
import com.ncs.breeze.BuildConfig
import com.ncs.breeze.car.breeze.utils.BreezeCarUtil
import com.ncs.breeze.common.extensions.android.getApp
import com.ncs.breeze.common.extensions.android.getAppPreference
import com.ncs.breeze.common.extensions.android.getUserDataPreference
import com.ncs.breeze.common.remote.ApiHelper
import com.ncs.breeze.common.remote.ApiObserver
import com.ncs.breeze.common.remote.MyServiceInterceptor
import com.ncs.breeze.common.storage.AppPrefsKey
import com.ncs.breeze.common.storage.BreezeUserPreference
import com.ncs.breeze.common.utils.AWSUtils
import com.ncs.breeze.common.utils.BreezeUserPreferenceUtil
import com.ncs.breeze.common.utils.Utils
import com.ncs.breeze.components.SingleLiveEvent
import com.ncs.breeze.reactnative.nativemodule.ShareBusinessLogicEvent
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import org.json.JSONArray
import org.json.JSONObject
import timber.log.Timber
import javax.inject.Inject


open class BaseViewModel(application: Application, apiHelper: ApiHelper) : AndroidViewModel(application) {
    var apiHelper = apiHelper
    val loader: SingleLiveEvent<Boolean> = SingleLiveEvent()
    val error: SingleLiveEvent<ErrorData?> = SingleLiveEvent()
    var compositeDisposable: CompositeDisposable = CompositeDisposable()

    var hasGotAllUserSettingData: SingleLiveEvent<Boolean> = SingleLiveEvent()
    var userLoggedIn: SingleLiveEvent<USER_STATUS> = SingleLiveEvent()

    @Inject
    lateinit var breezeUserPreferenceUtil: BreezeUserPreferenceUtil

    @Inject
    lateinit var myServiceInterceptor: MyServiceInterceptor

    fun displayLoader(isLoading: Boolean) {
        loader.value = isLoading
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

    fun amplifyFetchAuthSession() {
        AWSUtils.checkUserAuthSession(object : AWSUtils.AuthSessionCallback {
            override fun onAuthSuccess(token: String?) {
                myServiceInterceptor.setSessionToken(token)
                amplifyFetchUserAttributes()
            }

            override fun onAuthFailed() {
                userLoggedIn.postValue(USER_STATUS.USER_LOGGED_OUT)
            }
        })
    }

    fun amplifyFetchUserAttributes() {
        AWSUtils.fetchUserAttributes({ result ->
            var userIdentityProviderType = AmplifyIdentityProviderType.AMPLIFY_DEFAULT.type

            Timber.d("user details: ${result}")
            for (item in result) {

                if (TextUtils.equals(item.key.keyString, Constants.AMPLIFY_CONSTANTS.SUB)) {
                    Timber.d("Inside sub attribute ${item.value}")
                    BreezeUserPreference.getInstance(getApp()).saveUserId(item.value)
                    Firebase.crashlytics.setUserId(item.value)
                    Instabug.setUserAttribute(INSTABUG.KEY_USER_ID, item.value)
                    continue
                }

                if (TextUtils.equals(
                        item.key.keyString,
                        Constants.AMPLIFY_CONSTANTS.CUSTOM_ONBOARDING_STATE
                    )
                ) {
                    breezeUserPreferenceUtil.saveUserOnBoardingState(item.value)
                    continue
                }

                if (TextUtils.equals(
                        item.key.keyString,
                        Constants.AMPLIFY_CONSTANTS.IDENTITIES
                    )
                ) {
                    try {
                        val jsonArray = JSONArray(item.value)
                        val jsonObj = jsonArray[0] as JSONObject
                        userIdentityProviderType = jsonObj.getString("providerType")
                    } catch (e: Exception) {
                        Timber.e(e, "Exception occurred while parsing JSON")
                    }
                }

            }


            /**
             * Below code checks first if the custom email attribute is available for user and stores that in shared
             * preference to show in UI. This attribute is only available for users who login with phone number.
             * Trip log email will be sent to the custom email.
             *
             * For social login, the user will only have primary email attribute and that will be stored in the
             * shared preference.
             */

            breezeUserPreferenceUtil.saveAmplifyProviderType(userIdentityProviderType)
            Utils.runOnMainThread {
                val userStatus =
                    if (breezeUserPreferenceUtil.retrieveUserName().isEmpty())
                        USER_STATUS.USER_NO_USERNAME_ATTRIBUTE
                    else USER_STATUS.USER_LOGGED_IN

                userLoggedIn.value = userStatus
            }
        }, {
            Utils.runOnMainThread {
                userLoggedIn.value = USER_STATUS.USER_LOGGED_OUT
            }

        })
    }

    fun logAppVersion() {
        apiHelper.logAppVersion(
            LogAppVersionRequest(
                Constants.DEVICE_OS, BuildConfig.VERSION_CODE.toString()
            )
        )
            .subscribeOn(Schedulers.io())
            .subscribe(object : ApiObserver<String>(compositeDisposable) {
                override fun onSuccess(data: String) {
                    //Do not remove
                }

                override fun onError(e: ErrorData) {
                    //Do not remove
                }
            })
    }

    fun registerDeviceDetails() {
        val pushToken = getApp().getAppPreference()!!
            .getString(AppPrefsKey.PUSH_MESSAGING_TOKEN)
        apiHelper.registerDevice(
            RegisterDeviceRequest(
                Constants.DEVICE_OS,
                Utils.getDeviceID(getApplication()),
                pushToken
            )
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : ApiObserver<JsonElement>(compositeDisposable) {
                override fun onSuccess(data: JsonElement) {
                    Timber.i("-- registerdevice  call success and device FCM token registered successfully --")
                }

                override fun onError(e: ErrorData) {
                    Timber.e("-- registerdevice call failed ${e.message} - ${e.throwable} --")
                }
            })
    }

    private fun handleUserSettingResponse(data: SettingsResponse) {
        BreezeCarUtil.settingsResponse = data
        for (item in data.settings) {
            if (TextUtils.equals(item.attribute, Constants.SETTINGS.ATTR_MAP_DISPLAY)) {
                val themePreference = item.attribute_code
                val userTheme: UserTheme =
                    UserTheme.values().find { it.type == themePreference } ?: UserTheme.SYSTEM_DEFAULT
                breezeUserPreferenceUtil.saveUserThemePreference(userTheme)
                UserTheme.setDefaultNightMode(UserTheme.LIGHT)

            } else if (TextUtils.equals(item.attribute, Constants.SETTINGS.ATTR_ROUTE_PREFERENCE)) {
                val userRoutePref = if (TextUtils.equals(item.attribute_code, RoutePreference.CHEAPEST_ROUTE.type)) {
                    RoutePreference.CHEAPEST_ROUTE
                } else {
                    RoutePreference.FASTEST_ROUTE
                }
                breezeUserPreferenceUtil.saveUserRoutePreference(userRoutePref)
            } else if (TextUtils.equals(item.attribute, Constants.SETTINGS.ATTR_SHOW_DEMO)) {
                if (TextUtils.equals(item.attribute_code, "Yes")) {
                    BreezeUserPreference.getInstance(getApp()).setInAppGuideTutorialPending(true)
                } else {
                    BreezeUserPreference.getInstance(getApp()).setInAppGuideTutorialPending(false)
                }
            } else if (TextUtils.equals(item.attribute, Constants.SETTINGS.CARPARK_AVAILABILITY)) {
                CarParkViewOption.values().find { item.attribute_code == it.mode }?.let {
                    BreezeUserPreference.getInstance(getApp()).setStateCarpParkOption(it)
                }
            } else if (TextUtils.equals(item.attribute, Constants.SETTINGS.CARPARK_COUNT)) {
                BreezeUserPreference.getInstance(getApp())
                    .saveCarParkCount(item.attribute_code)
            } else if (TextUtils.equals(item.attribute, Constants.SETTINGS.CARPARK_DISTANCE)) {
                BreezeUserPreference.getInstance(getApp())
                    .saveCarParkDistance(item.attribute_code)
            }
        }
    }


    /**
     * funtion to get all setting mapper & user setting
     */
    fun getAllSettingZipData() {
        val observableOne = apiHelper.getAllSettingSystem()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
        val observableTwo = apiHelper.getUserSettings()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
        val observableNameBlack = apiHelper.getNameBlackList()
            .subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())

        Observable.zip(
            observableOne,
            observableTwo,
            observableNameBlack
        ) { allsettingConfig, usserSettingResponse, settingNameBlackList ->
            BreezeCarUtil.masterlists = allsettingConfig.masterlists
            BreezeCarUtil.listNameBlackList = settingNameBlackList.data
            getApp().getUserDataPreference()?.run {
                saveEvPetrolMaxRange(
                    allsettingConfig.masterlists?.MaxEVPetrolRange?.getOrNull(0)?.value?.toIntOrNull()
                        ?: (Constants.DEFAULT_PETROL_EV_MAX_RADIUS * 1000).toInt()
                )
                saveEvPetrolRange(
                    allsettingConfig.masterlists?.EVPetrolRange?.getOrNull(0)?.value?.toIntOrNull()
                        ?: (Constants.DEFAULT_PETROL_EV_RADIUS * 1000).toInt()
                )
                saveEvPetrolResultCount(
                    allsettingConfig.masterlists?.EVPetrolCount?.getOrNull(0)?.value?.toIntOrNull()
                        ?: Constants.DEFAULT_PETROL_EV_RESULT_COUNT
                )
            }

            handleUserSettingResponse(usserSettingResponse)
            true
        }.subscribe(
            {
                hasGotAllUserSettingData.value = true
                sendRNEvent()
            }, {
                hasGotAllUserSettingData.value = false
            }
        ).addTo(compositeDisposable)
    }

    private fun sendRNEvent() {
        App.instance?.let { app ->
            val shareBusinessLogicPackage = app.shareBusinessLogicPackage
            shareBusinessLogicPackage.getRequestsModule().flatMap {
                it.callRN(ShareBusinessLogicEvent.ON_FINISHED_INITIALIZING_USER_DATA.eventName, null)
            }.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe()
        }
    }


    enum class USER_STATUS {
        USER_LOGGED_OUT,
        USER_LOGGED_IN,
        USER_NO_USERNAME_ATTRIBUTE
    }

}
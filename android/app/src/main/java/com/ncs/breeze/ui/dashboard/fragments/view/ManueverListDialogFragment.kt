package com.ncs.breeze.ui.dashboard.fragments.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.view.ContextThemeWrapper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mapbox.bindgen.Expected
import com.mapbox.navigation.ui.maneuver.model.Maneuver
import com.mapbox.navigation.ui.maneuver.model.ManeuverError
import com.mapbox.navigation.ui.maneuver.model.RoadShield
import com.ncs.breeze.R
import com.ncs.breeze.components.BreezeRoutePlanningManeuverAdapter
import com.ncs.breeze.components.CustomBottomSheetFragment

class ManueverListDialogFragment() : CustomBottomSheetFragment() {

    protected val maneuverToRoadShield = mutableMapOf<String, RoadShield?>()
    protected val currentlyRenderedManeuvers: MutableList<Maneuver> = mutableListOf()

    private lateinit var upcomingManeuverAdapter: BreezeRoutePlanningManeuverAdapter
    private lateinit var routeUpcomingManeuverRecycler: RecyclerView
    private lateinit var routeManeuverClose: View

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(
            R.layout.breeze_route_manuever_layout, container,
            false
        )
        routeUpcomingManeuverRecycler = rootView.findViewById(R.id.route_upcoming_maneuver_recycler)
        routeManeuverClose = rootView.findViewById(R.id.route_maneuver_close)
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        upcomingManeuverAdapter = BreezeRoutePlanningManeuverAdapter(requireContext())
        routeUpcomingManeuverRecycler.apply {
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            adapter = upcomingManeuverAdapter
            visibility = View.VISIBLE
        }
        routeManeuverClose.setOnClickListener {
            dismiss()
        }
        init()
        renderManeuvers()
    }

    private fun renderManeuvers() {
        if (currentlyRenderedManeuvers.isNotEmpty()) {
            upcomingManeuverAdapter.updateRoadShields(maneuverToRoadShield)
            drawUpcomingManeuvers(currentlyRenderedManeuvers)
        }
    }

    private fun drawUpcomingManeuvers(maneuvers: List<Maneuver>) {
        upcomingManeuverAdapter.addUpcomingManeuvers(maneuvers)
        upcomingManeuverAdapter.notifyDataSetChanged()
    }

    private fun init() {
        upcomingManeuverAdapter.updateIconContextThemeWrapper(
            ContextThemeWrapper(
                context,
                R.style.BreezeMapboxStyleTurnIconManeuver
            )
        )
    }

    class Builder {
        var fragment: ManueverListDialogFragment

        constructor() {
            fragment = ManueverListDialogFragment()
        }

        fun renderManeuvers(maneuvers: Expected<ManeuverError, List<Maneuver>>): Builder {
            maneuvers.onValue { list ->
                if (list.isNotEmpty()) {
                    fragment.currentlyRenderedManeuvers.clear()
                    fragment.currentlyRenderedManeuvers.addAll(list)
                }
            }
            return this
        }

        fun renderManeuverShields(shieldMap: Map<String, RoadShield?>): Builder {
            fragment.maneuverToRoadShield.putAll(shieldMap)
            return this
        }

        fun build(): ManueverListDialogFragment {
            return fragment
        }
    }
}
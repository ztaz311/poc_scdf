package com.ncs.breeze.di.module.fragment

import com.ncs.breeze.ui.dashboard.fragments.view.SettingsFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class SettingsFragmentModule {
    @ContributesAndroidInjector
    abstract fun contributeSettingsFragment(): SettingsFragment
}
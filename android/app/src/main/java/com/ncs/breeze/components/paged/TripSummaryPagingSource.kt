package com.ncs.breeze.components.paged

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.breeze.model.api.response.TripsItem
import com.ncs.breeze.common.remote.ApiHelper
import com.ncs.breeze.common.rxbus.RxBus
import com.ncs.breeze.common.rxbus.RxEvent

class TripSummaryPagingSource(
    private val apiHelper: ApiHelper,
    var startDate: String,
    var endDate: String,
    var searchKey: String
) : PagingSource<Int, TripsItem>() {

    companion object {
        const val STARTING_PAGE_INDEX: Int = 1
        const val PAGE_SIZE = 10
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, TripsItem> {

        return try {
            val nextPage = params.key ?: STARTING_PAGE_INDEX
            val tripSummaryResponse =
                apiHelper.getTripSummary(nextPage, PAGE_SIZE, startDate, endDate, searchKey)
            RxBus.publish(RxEvent.IsTripHistoryFeatureUsed(tripSummaryResponse.isFeatureUsed!!))
            LoadResult.Page(
                data = tripSummaryResponse.trips,
                prevKey = if (nextPage == 1) null else nextPage - 1,
                nextKey = if (nextPage < tripSummaryResponse.totalPages!!)
                    tripSummaryResponse.currentPage?.plus(1) else null
            )
        } catch (e: Exception) {
            LoadResult.Error(e)
        }
    }

    // The refresh key is used for subsequent refresh calls to PagingSource.load after the initial load
    override fun getRefreshKey(state: PagingState<Int, TripsItem>): Int? {
        // We need to get the previous key (or next key if previous is null) of the page
        // that was closest to the most recently accessed index.
        // Anchor position is the most recently accessed index
        return state.anchorPosition?.let { anchorPosition ->
            state.closestPageToPosition(anchorPosition)?.prevKey?.plus(1)
                ?: state.closestPageToPosition(anchorPosition)?.nextKey?.minus(1)
        }
    }
}

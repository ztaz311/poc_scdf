package com.ncs.breeze.components.map.profile

class ProfileZoneState {
    var isOuterTrigger = false
    var isInnnerTrigger = false

    fun resetTriggerDetectZone() {
        isOuterTrigger = false
        isInnnerTrigger = false
    }
}
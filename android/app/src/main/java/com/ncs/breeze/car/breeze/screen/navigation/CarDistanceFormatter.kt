package com.ncs.breeze.car.breeze.screen.navigation

import androidx.car.app.model.Distance
import com.breeze.model.extensions.safeDouble
import com.mapbox.navigation.base.formatter.Rounding
import com.mapbox.navigation.base.formatter.UnitType
import java.text.NumberFormat
import java.util.*
import kotlin.math.floor
import kotlin.math.roundToInt

class CarDistanceFormatter(
    private val unitType: UnitType,
    @Rounding.Increment private val roundingIncrement: Int,
    private val locale: Locale
) {

    fun carDistance(distanceMeters: Double): Distance = when (unitType) {
        UnitType.IMPERIAL -> carDistanceImperial(distanceMeters)
        UnitType.METRIC -> carDistanceMetric(distanceMeters)
    }

    private fun carDistanceImperial(distanceMeters: Double): Distance {
        return when (distanceMeters) {
            !in 0.0..Double.MAX_VALUE -> {
                Distance.create(formatDistanceAndSuffixForSmallUnit(0.0), Distance.UNIT_FEET)
            }
            in 0.0..smallDistanceMeters -> {
                Distance.create(
                    formatDistanceAndSuffixForSmallUnit(distanceMeters.metersToFeet()),
                    Distance.UNIT_FEET
                )
            }
            in smallDistanceMeters..mediumDistanceMeters -> {
                Distance.create(
                    formatDistanceAndSuffixForLargeUnit(
                        distanceMeters.metersToMiles(),
                        1
                    ), Distance.UNIT_MILES_P1
                )
            }
            else -> {
                Distance.create(
                    formatDistanceAndSuffixForLargeUnit(
                        distanceMeters.metersToMiles(),
                        0
                    ), Distance.UNIT_MILES
                )
            }
        }
    }

    private fun carDistanceMetric(distanceMeters: Double): Distance {
        return when (distanceMeters) {
            !in 0.0..Double.MAX_VALUE -> {
                Distance.create(formatDistanceAndSuffixForSmallUnit(0.0), Distance.UNIT_METERS)
            }
            in 0.0..smallDistanceMeters -> {
                Distance.create(
                    formatDistanceAndSuffixForSmallUnit(distanceMeters),
                    Distance.UNIT_METERS
                )
            }
            in smallDistanceMeters..mediumDistanceMeters -> {
                Distance.create(
                    formatDistanceAndSuffixForLargeUnit(
                        distanceMeters.metersToKilometers(),
                        1
                    ), Distance.UNIT_KILOMETERS_P1
                )
            }
            else -> {
                Distance.create(
                    formatDistanceAndSuffixForLargeUnit(
                        distanceMeters.metersToKilometers(),
                        0
                    ), Distance.UNIT_KILOMETERS
                )
            }
        }
    }

    /**
     * TODO: This is a suggested fix
     * Refactor after long term solution is added https://github.com/mapbox/mapbox-navigation-android/issues/5065
     */
    private fun formatDistanceAndSuffixForSmallUnit(distance: Double): Double {
        val roundedValue = if (roundingIncrement > 0) {
            val roundedDistance = distance.roundToInt()
            if (roundedDistance < roundingIncrement) {
                roundingIncrement
            } else {
                roundedDistance / roundingIncrement * roundingIncrement
            }
        } else {
            distance.roundToInt()
        }

        return roundedValue.toDouble()
    }

    /**
     * TODO: This is a suggested fix
     * Refactor after long term solution is added https://github.com/mapbox/mapbox-navigation-android/issues/5065
     */
    private fun formatDistanceAndSuffixForLargeUnit(
        distance: Double,
        maxFractionDigits: Int
    ): Double {

        val roundedValue = NumberFormat.getNumberInstance(locale).also {
            it.maximumFractionDigits = maxFractionDigits
        }.format(distance)
        return roundedValue.safeDistanceDouble()
    }

    private fun String.safeDistanceDouble(): Double {
        return this.replace(",", "")
            .replace("#", "")
            .safeDouble()
    }

    private fun Double.metersToFeet() = floor(this * FEET_PER_METER)
    private fun Double.metersToMiles() = this * MILES_PER_METER
    private fun Double.metersToKilometers() = this * KILOMETERS_PER_METER

    internal companion object {
        internal const val smallDistanceMeters = 1000.0
        internal const val mediumDistanceMeters = 10000.0

        private const val FEET_PER_METER = 3.28084
        private const val MILES_PER_METER = 0.000621371
        private const val KILOMETERS_PER_METER = 0.001
    }
}

package com.ncs.breeze.car.breeze.screen.navigation

import android.annotation.SuppressLint
import android.util.Log
import androidx.car.app.CarToast
import androidx.car.app.model.Action
import androidx.car.app.model.Pane
import androidx.car.app.model.PaneTemplate
import androidx.car.app.model.Template
import androidx.lifecycle.lifecycleScope
import com.breeze.model.DestinationAddressDetails
import com.breeze.model.DestinationAddressTypes
import com.ncs.breeze.common.model.RouteAdapterObjects
import com.ncs.breeze.common.model.RouteAdapterTypeObjects
import com.breeze.model.SearchLocation
import com.ncs.breeze.common.model.SimpleRouteAdapterObjects
import com.mapbox.android.core.location.LocationEngineResult
import com.mapbox.api.directions.v5.models.DirectionsResponse
import com.mapbox.api.directions.v5.models.DirectionsRoute
import com.mapbox.geojson.Point
import com.mapbox.navigation.base.route.NavigationRoute
import com.mapbox.navigation.base.route.RouterOrigin
import com.ncs.breeze.R
import com.ncs.breeze.car.breeze.MainBreezeCarContext
import com.ncs.breeze.car.breeze.base.BaseScreenCar
import com.ncs.breeze.common.analytics.Screen
import com.ncs.breeze.common.event.ToAppRx
import com.ncs.breeze.common.event.ToCarRx
import com.ncs.breeze.common.extensions.mapbox.compressToGzip
import com.ncs.breeze.common.helper.route.RoutePlanningHelper
import com.breeze.model.*
import com.breeze.model.api.request.ETARequest
import com.breeze.model.api.response.ERPResponseData
import com.ncs.breeze.common.model.rx.AppToNavigationStartEvent
import com.ncs.breeze.common.model.rx.AppToPhoneNavigationReRouteEvent
import com.ncs.breeze.common.model.rx.CarNavigationStartEventData
import com.ncs.breeze.common.model.rx.DataForRoutePreview
import com.ncs.breeze.common.model.rx.PhoneNavigationStartEventData
import com.ncs.breeze.common.model.toRouteAdapterObjects
import com.breeze.model.enums.RoutePreference
import com.ncs.breeze.common.utils.InitObjectUtilsController.mRoutePlanningHelper
import com.ncs.breeze.common.utils.LocationBreezeManager
import com.ncs.breeze.common.utils.route.RouteSelectorUtil
import kotlinx.coroutines.*


class NavigationReRouteLoadingScreen(
    val breezeCarContext: MainBreezeCarContext,
    val data: DataForRoutePreview,
) : BaseScreenCar(breezeCarContext.carContext) {

    private val locationCallback = object : LocationBreezeManager.CallBackLocationBreezeManager {
        override fun onFailure(p0: Exception) {
            super.onFailure(p0)
            handleFetchLocationFailure()
        }

        override fun onSuccess(p0: LocationEngineResult) {
            super.onSuccess(p0)
            if (!isHandleWhenLocationAvaiable) {
                isHandleWhenLocationAvaiable = true
                handleWhenLocationAvailable()
                LocationBreezeManager.getInstance().removeLocationCallBack(this)
            }
        }

    }

    var mOriginPoint: Point? = null
    lateinit var mDestination: Point

    var mDestinationSearchLocation: SearchLocation? = null

    var isHandleWhenLocationAvaiable = false

    override fun onGetTemplate(): Template {
        return buildLoadingTemplate()
    }

    override fun getScreenName(): String {
        return Screen.AA_ROUTE_PLAN_SCREEN_LOADING
    }

    override fun onCreateScreen() {
        super.onCreateScreen()
        mDestination = data.selectedDestination
        mDestinationSearchLocation = data.destinationSearchLocation

        findOriginLocation()
    }

    override fun onResumeScreen() {
        super.onResumeScreen()
        isHandleWhenLocationAvaiable = false
    }

    private fun handleWhenLocationAvailable() {
        LocationBreezeManager.getInstance().currentLocation?.let { currentLocation ->
            triggerRoutePlanning(
                originPoint = Point.fromLngLat(currentLocation.longitude, currentLocation.latitude),
                destination = mDestination
            )
        }
    }

    @SuppressLint("MissingPermission")
    private fun findOriginLocation() {
        //LocationBreezeManager.getInstance()?.currentLocation = null
        if (LocationBreezeManager.getInstance().currentLocation != null) {
            handleWhenLocationAvailable()
        } else {
            LocationBreezeManager.getInstance().startLocationUpdates()
            LocationBreezeManager.getInstance().registerLocationCallback(locationCallback)
        }
    }

    private fun triggerRoutePlanning(
        originPoint: Point,
        destination: Point
    ) {
        lifecycleScope.launch(Dispatchers.IO) {
            withContext(Dispatchers.Main) {
                GlobalScope.launch(Dispatchers.Main) {
                    mRoutePlanningHelper?.findRoutes(
                        originPoint, destination,
                        mDestinationSearchLocation?.address1 ?: "",
                        listOf(),
                        RoutePlanningHelper.RouteAlgorithm.MERGED_SINGLE_ROUTE_PER_TYPE
                    )?.let { routePreference ->
                        val directionsRoutes = arrayListOf<RouteAdapterObjects>()
                        val allRoutes = getRouteTypeDataList(HashMap(routePreference))
                        if (allRoutes.size > 0) {
                            allRoutes.take(3).forEach {
                                directionsRoutes.add(it.routeData.toRouteAdapterObjects())
                            }
                        }
                        if (directionsRoutes.size > 0) {
                            data.currentRouteObjects = directionsRoutes
                            val navigationRoutes = NavigationRoute.create(
                                directionsRoutes[0].originalResponse,
                                directionsRoutes[0].route.routeOptions()!!,
                                RouterOrigin.Custom()
                            )

                            CoroutineScope(Dispatchers.Main).launch {
                                finish()
                                triggerNavigationStartEvent(
                                    originalDestination = data.originalDestination
                                        ?: generateDestinationAddress(data.destinationSearchLocation),
                                    destinationAddressDetails = generateDestinationAddress(data.destinationSearchLocation),
                                    route = navigationRoutes[0].directionsRoute,
                                    originalResponse = directionsRoutes[0].originalResponse,
                                    erpData = null,
                                    etaRequest = null,
                                )
//                                RxToCarRxEvent.postViewEventToCar(
//                                    AppToRoutePlanningStartEvent(
//                                        data
//                                    )
//                                )
                            }
                        }
                    }
                }
            }
        }
    }

    private fun generateDestinationAddress(searchLocation: SearchLocation?): DestinationAddressDetails {
        return DestinationAddressDetails(
            searchLocation?.addressid,
            searchLocation?.address1,
            searchLocation?.address2,
            searchLocation?.lat,
            searchLocation?.long,
            null,
            searchLocation?.name,
            DestinationAddressTypes.EASY_BREEZIES,
            isCarPark = searchLocation?.isCarPark ?: false,
            fullAddress = searchLocation?.fullAddress
        )
    }

    private fun triggerNavigationStartEvent(
        originalDestination: DestinationAddressDetails,
        destinationAddressDetails: DestinationAddressDetails,
        route: DirectionsRoute,
        originalResponse: DirectionsResponse,
        erpData: ERPResponseData.ERPResponse?,
        etaRequest: ETARequest?
    ) {
        CoroutineScope(Dispatchers.Main).launch {
            try {
                val compressedRoute = originalResponse.compressToGzip()
                val phoneData = PhoneNavigationStartEventData(
                    originalDestination,
                    destinationAddressDetails,
                    route,
                    compressedRoute,
                    erpData,
                    etaRequest
                )
                ToAppRx.postEvent(AppToPhoneNavigationReRouteEvent(phoneData))
                val carData = CarNavigationStartEventData(
                    originalDestination,
                    destinationAddressDetails,
                    route,
                    compressedRoute,
                    originalResponse,
                    erpData,
                    etaRequest
                )
                ToCarRx.postEventWithCacheLast(AppToNavigationStartEvent(carData))
            } catch (e: Exception) {
                Log.e("RoutePlanningScreen", e.stackTraceToString())
            }
        }
    }

    private fun showUnsupportedAddressDialog() {
        CarToast.makeText(
            carContext,
            carContext.getString(R.string.dialog_msg_country_not_supported),
            CarToast.LENGTH_LONG
        ).show()
        finish()
    }

    private fun handleFetchLocationFailure() {
        CarToast.makeText(
            carContext,
            carContext.getString(R.string.error_get_current_location),
            CarToast.LENGTH_LONG
        ).show()
        finish()
    }

    /**
     * converts the route map to a ordered selectable list
     */
    private fun getRouteTypeDataList(routesMap: HashMap<RoutePreference, List<SimpleRouteAdapterObjects>>): ArrayList<RouteAdapterTypeObjects> {
        val allRoutes: ArrayList<RouteAdapterTypeObjects> = ArrayList()
        val alternates = routesMap.remove(RoutePreference.ALTERNATE_ROUTE)
        RouteSelectorUtil.handleRoutePreference(
            routesMap,
            allRoutes,
            RoutePreference.FASTEST_ROUTE.type
        )
        routesMap.forEach { map ->
            map.value.forEach {
                allRoutes.add(RouteAdapterTypeObjects(map.key, it))
            }
        }
        alternates?.forEach {
            allRoutes.add(RouteAdapterTypeObjects(RoutePreference.ALTERNATE_ROUTE, it))
        }
        return allRoutes
    }


    /**
     * build loading template
     */
    private fun buildLoadingTemplate(): Template {
        val paneBuilder = Pane.Builder()
        paneBuilder.setLoading(true)
        return PaneTemplate.Builder(paneBuilder.build())
            .setTitle("Loading Route")
            .setHeaderAction(Action.BACK)
            .build()
    }
}

package com.ncs.breeze.ui.login.fragment.signUp.module

import com.ncs.breeze.ui.login.fragment.signUp.view.TermsAndCondnFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class TermsAndCondnModule {
    @ContributesAndroidInjector
    abstract fun contributeTnCFragment(): TermsAndCondnFragment
}
package com.ncs.breeze.ui.login.fragment.forgotPassword.view

import android.app.Dialog
import android.graphics.Color
import android.graphics.Rect
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.WindowManager
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.ncs.breeze.R
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.Event
import com.ncs.breeze.common.analytics.Screen
import com.breeze.model.constants.Constants
import com.ncs.breeze.common.utils.DialogFactory
import com.ncs.breeze.common.utils.Utils
import com.ncs.breeze.common.utils.Variables
import com.breeze.customization.view.BreezeButton
import com.breeze.customization.view.otpview.OTPListener
import com.ncs.breeze.databinding.FragmentForgotVerifyOtpBinding
import com.ncs.breeze.ui.base.BaseActivity
import com.ncs.breeze.ui.base.BaseFragment
import com.ncs.breeze.ui.login.fragment.forgotPassword.viewmodel.ForgotVerifyOTPViewModel
import javax.inject.Inject

class ForgotVerifyOTPFragment :
    BaseFragment<FragmentForgotVerifyOtpBinding, ForgotVerifyOTPViewModel>() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val viewModel: ForgotVerifyOTPViewModel by viewModels {
        viewModelFactory
    }

    var email: String? = null
    var password: String? = null

    lateinit var dialog: Dialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            email = requireArguments().getString(Constants.KEYS.KEY_EMAIL)
            password = requireArguments().getString(Constants.KEYS.KEY_PASSWORD)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewBinding.otpView.otpListener = object : OTPListener {
            override fun onInteractionListener(otp: String) {
            }

            override fun onOTPComplete(otp: String) {
                Analytics.logEditEvent(Event.OTP, getScreenName())
                if (otp.length == 6) {
                    viewBinding.progressBar.visibility = View.VISIBLE
                    password?.let {
                        if (Variables.isNetworkConnected) {
                            confirmResetPassword(password!!, otp)
                        } else {
                            DialogFactory.internetDialog(
                                context,
                            ) { _, _ -> }!!.show()
                        }
                    }
                }
            }
        }

        viewBinding.otpView.requestFocusOTP()
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        initialize()
    }

    private fun initialize() {

        viewBinding.description.setText(email?.let {
            Utils.getFormattedSpan(
                resources.getString(R.string.check_otp), it, resources.getString(
                    R.string.check_otp_replace_text
                )
            )
        })
        clickListeners()
    }

    private fun clickListeners() {
        viewBinding.backButton.setOnClickListener {
            onBackPressed()
        }

        viewBinding.txtResendOtp.setOnClickListener() {
            otpResend()
            Analytics.logClickEvent(Event.RESEND, getScreenName())

        }
    }

    companion object {

        @JvmStatic
        fun newInstance() =
            ForgotVerifyOTPFragment().apply {
                arguments = Bundle().apply {

                }
            }
    }

    override fun inflateViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?,
        attachToContainer: Boolean
    ) = FragmentForgotVerifyOtpBinding.inflate(inflater, container, attachToContainer)

    override fun getViewModelReference(): ForgotVerifyOTPViewModel {
        return viewModel
    }

    private fun otpResend() {
        viewModel.resendSuccessful.observe(viewLifecycleOwner, Observer {
            viewModel.resendSuccessful.removeObservers(viewLifecycleOwner)
            viewBinding.otpView.setOTP("")
        })
        viewModel.resendFailed.observe(viewLifecycleOwner, Observer {
            viewModel.resendFailed.removeObservers(viewLifecycleOwner)
            viewBinding.otpView.setOTP("")
            viewBinding.error.visibility = View.VISIBLE
            viewBinding.error.text = it.errorMessage
        })
        viewModel.resendOTP(email!!)
    }

    private fun confirmResetPassword(password: String, otpText: String) {
        viewModel.confirmPasswordSuccessful.observe(viewLifecycleOwner, Observer {
            viewModel.confirmPasswordSuccessful.removeObservers(viewLifecycleOwner)
            viewBinding.progressBar.visibility = View.GONE
            (activity as BaseActivity<*, *>).hideKeyboard()
            showSuccessDialog()
        })
        viewModel.confirmPasswordFailed.observe(viewLifecycleOwner, Observer {
            viewModel.confirmPasswordFailed.removeObservers(viewLifecycleOwner)
            viewBinding.progressBar.visibility = View.GONE
            viewBinding.otpView.setOTP("")
            viewBinding.otpView.showError()
            viewBinding.error.visibility = View.VISIBLE
            viewBinding.error.text = it.errorMessage
        })
        viewModel.confirmResetPassword(password, otpText)
    }

    private fun showSuccessDialog() {

        dialog = activity?.let {
            Dialog(it)
        }!!

        dialog.setContentView(R.layout.dialog_create_account_successful)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCanceledOnTouchOutside(false)
        //dialog.setCancelable(true)

        val imgView = dialog.findViewById<ImageView>(R.id.popup_image)
        imgView.setImageResource(R.drawable.password_changed_successfully)

        val greeting = dialog.findViewById<TextView>(R.id.greeting)
        greeting.setText(R.string.good_job)

        val successMsg = dialog.findViewById<TextView>(R.id.success_message)
        successMsg.setText(R.string.pwd_change_successful)

        val button = dialog.findViewById<BreezeButton>(R.id.button_okay)
        button.setOnClickListener {
            dialog.dismiss()
            (activity as BaseActivity<*, *>).hideKeyboard()
            requireActivity().supportFragmentManager.popBackStackImmediate(
                Constants.TAGS.FORGET_PASSWORD_TAG,
                FragmentManager.POP_BACK_STACK_INCLUSIVE
            )
            Analytics.logClosePopupEvent(Event.ONBOARDING_FORGOT_PASSWORD_SUCCESS, getScreenName())

        }

        dialog.show()

        val displayRectangle = Rect()
        val window: Window = requireActivity().window
        window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle)

        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window!!.getAttributes())
        lp.width = (displayRectangle.width() * 0.9f).toInt()
        dialog.window!!.setAttributes(lp)

        Analytics.logOpenPopupEvent(Event.ONBOARDING_FORGOT_PASSWORD_SUCCESS, getScreenName())
    }

    override fun getScreenName(): String {
        return Screen.FORGOT_PASSWORD_STEP3
    }
}
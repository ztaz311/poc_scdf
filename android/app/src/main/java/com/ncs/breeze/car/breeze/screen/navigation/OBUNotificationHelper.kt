package com.ncs.breeze.car.breeze.screen.navigation

import android.location.Location
import androidx.core.graphics.drawable.IconCompat
import com.ncs.breeze.R
import com.ncs.breeze.car.breeze.base.BaseNavigationScreenCar
import com.ncs.breeze.car.breeze.utils.NavigationNotificationUtils
import com.ncs.breeze.common.analytics.AnalyticsUtils
import com.ncs.breeze.common.analytics.Event
import com.ncs.breeze.common.analytics.Screen
import com.breeze.model.extensions.round
import com.breeze.model.obu.OBUParkingAvailability
import com.breeze.model.obu.OBURoadEventData
import com.breeze.model.obu.OBUTravelTimeData
import com.ncs.breeze.common.storage.BreezeUserPreference
import com.ncs.breeze.common.utils.mapper.car_notification.CarNavNotificationContentGenerator
import com.ncs.breeze.common.utils.mapper.car_notification.CarNavNotificationIconMapper
import java.lang.ref.WeakReference

class OBUNotificationHelper(carScreen: BaseNavigationScreenCar) {
    private val screenRef = WeakReference(carScreen)

    private fun getAnalyticsScreenName() = when (screenRef.get()) {
        is NavigationScreen -> Screen.AA_DHU_NAVIGATION
        else -> Screen.AA_DHU_CRUISE
    }

    private fun getAnalyticsPopupName() = when (screenRef.get()) {
        is NavigationScreen -> Event.AA_POPUP_NAVIGATION
        else -> Event.AA_POPUP_CRUISE
    }

    fun showParkingAvailabilityNotification(
        data: Array<OBUParkingAvailability>,
        screenName: String,
        currentRoadName: String,
        currentLocation: Location?
    ) {
        if (screenRef.get() == null || data.isEmpty()) return

        val context = screenRef.get()!!.carContext
        val wantToSeeAlert = BreezeUserPreference.getInstance(context.applicationContext)
            .getParkingAvailabilityDisplay()
        if (!wantToSeeAlert) return

        val isAllAvailable = data.count { it.color == "green" } == data.size
        val isAllFull = data.count { it.availableLots == "0" } == data.size
        var title = ""
        var icon: IconCompat? = null

        when {
            isAllAvailable -> {
                title = context.getString(R.string.parking_availability_alert_title_available)
                icon = IconCompat.createWithResource(
                    context,
                    R.drawable.ic_android_auto_parking_slots_nearby_all_available
                )
            }

            isAllFull -> {
                title = context.getString(R.string.parking_availability_alert_title_full)
                icon = IconCompat.createWithResource(
                    context,
                    R.drawable.ic_android_auto_parking_slots_nearby_all_full
                )
            }

            else -> {
                title = context.getString(R.string.parking_availability_alert_title_filling_up_soon)
                icon = IconCompat.createWithResource(
                    context,
                    R.drawable.ic_android_auto_parking_slots_filling_up_soon
                )
            }
        }
        if (title.isNotEmpty()) {
            AnalyticsUtils.sendRoadEventParkingAvailability(
                Event.USER_POPUP,
                Event.AA_POPUP_OPEN,
                currentRoadName,
                currentLocation?.latitude ?: 0.0,
                currentLocation?.longitude ?: 0.0,
                screenName,
                getAnalyticsPopupName()
            )
            val content = context.getString(R.string.parking_availability_alert_content)
            NavigationNotificationUtils.showParkingAvailabilityAlert(
                title,
                content,
                icon,
                data,
                screenName
            )
        }
    }

    fun showRoadEventNotification(
        eventData: OBURoadEventData,
        roadName: String,
        lat: Double,
        long: Double,
        screenName: String,
    ) {
        if (screenRef.get() == null) return

        val context = screenRef.get()!!.carContext
        val title = CarNavNotificationContentGenerator.generateTitleFromOBURoadEvent(eventData, "")
        val content =
            CarNavNotificationContentGenerator.generateContentFromOBURoadEvent(eventData, "")
        val icon: IconCompat? = CarNavNotificationIconMapper.getOBURoadEventIcon(context, eventData)
        val shouldShowNotification = when (eventData.eventType) {
            OBURoadEventData.EVENT_TYPE_SCHOOL_ZONE -> BreezeUserPreference.getInstance(context.applicationContext)
                .getSchoolZoneDisplay()

            OBURoadEventData.EVENT_TYPE_SILVER_ZONE -> BreezeUserPreference.getInstance(context.applicationContext)
                .getSilverZoneDisplay()

            OBURoadEventData.EVENT_TYPE_BUS_LANE -> BreezeUserPreference.getInstance(context.applicationContext)
                .getBusLaneDisplay()

            else -> true
        }

        if (icon != null) {
            AnalyticsUtils.sendRoadEvent(
                eventData,
                roadName,
                lat,
                long,
                screenName,
                getAnalyticsPopupName(),
                Event.AA_POPUP_OPEN
            )
            if(shouldShowNotification)
                NavigationNotificationUtils.showRoadEventNotification(title, content, icon, screenName)
        }
    }

    fun showTravelTimeEventNotification(
        data: Array<OBUTravelTimeData>,
        screenName: String,
        currentRoadName: String,
        currentLocation: Location?
    ) {
        if (screenRef.get() == null || data.isNullOrEmpty()) return

        val context = screenRef.get()!!.carContext
        val wantToSeeAlert = BreezeUserPreference.getInstance(context.applicationContext)
            .getEstimatedTravelTimeDisplay()
        if (!wantToSeeAlert) return

        // val isAllYellow = data.count { it.color == "yellow" } == data.size
        val title = context.getString(R.string.congestion_alert_title)
        val content = context.getString(R.string.congestion_alert_content)
        var icon = IconCompat.createWithResource(context, R.drawable.ic_road_status_red)
        /*
        if (isAllYellow) {
            icon = IconCompat.createWithResource(context, R.drawable.ic_road_status_yellow)
        }
        */
        AnalyticsUtils.sendRoadEventTravelTime(
            Event.USER_POPUP,
            Event.AA_POPUP_OPEN,
            currentRoadName,
            currentLocation?.latitude ?: 0.0,
            currentLocation?.longitude ?: 0.0,
            screenName,
            getAnalyticsPopupName()
        )
        NavigationNotificationUtils.showCongestionAlert(title, content, icon, data, screenName)
    }

    fun showLowCardNotification(cardBalanceSGD: Double, screenName: String) {
        if (screenRef.get() == null) return

        val context = screenRef.get()!!.carContext
        NavigationNotificationUtils.showRoadEventNotification(
            "Low Card Balance",
            "$${cardBalanceSGD.round(2)}",
            IconCompat.createWithResource(context, R.drawable.warning),
            screenName
        )
    }
}
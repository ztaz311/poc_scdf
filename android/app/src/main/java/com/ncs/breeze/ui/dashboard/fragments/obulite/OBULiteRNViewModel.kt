package com.ncs.breeze.ui.dashboard.fragments.obulite

import android.app.Application
import androidx.lifecycle.viewModelScope
import com.ncs.breeze.ui.base.BaseFragmentViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

class OBULiteRNViewModel @Inject constructor(application: Application) :
    BaseFragmentViewModel(
        application
    ) {
    private val _isMapViewMode = MutableStateFlow(false)
    val isMapViewMode = _isMapViewMode.asStateFlow()

    fun updateViewMode(isMapViewMode: Boolean) {
        viewModelScope.launch(Dispatchers.IO) {
            _isMapViewMode.emit(isMapViewMode)
        }
    }
}
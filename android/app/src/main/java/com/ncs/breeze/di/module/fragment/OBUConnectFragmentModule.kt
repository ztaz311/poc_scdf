package com.ncs.breeze.di.module.fragment

import com.ncs.breeze.ui.dashboard.fragments.obuconnect.OBUConnectFragment
import com.ncs.breeze.ui.dashboard.fragments.obulite.OBULiteFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class OBUConnectFragmentModule {
    @ContributesAndroidInjector
    abstract fun contributeOBUConnectFragment(): OBUConnectFragment
}
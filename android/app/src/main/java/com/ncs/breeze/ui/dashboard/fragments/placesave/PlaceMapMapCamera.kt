package com.ncs.breeze.ui.dashboard.fragments.placesave

import com.breeze.model.SearchLocation
import com.breeze.model.constants.Constants
import com.breeze.model.extensions.dpToPx
import com.mapbox.geojson.Geometry
import com.mapbox.geojson.GeometryCollection
import com.mapbox.geojson.Point
import com.mapbox.maps.CameraOptions
import com.mapbox.maps.EdgeInsets
import com.mapbox.maps.MapboxMap
import com.ncs.breeze.common.utils.BreezeMapZoomUtil
import com.ncs.breeze.components.map.profile.BaseMapCamera

class PlaceMapMapCamera(
    val mapboxMap: MapboxMap
) : BaseMapCamera() {

    var savedPlacePoints: GeometryCollection? = null
    var mapCameraPadding =
        EdgeInsets(250.0.dpToPx(), 60.0.dpToPx(), 490.0.dpToPx(), 60.0.dpToPx())
        private set

    /**
     * update current mapCameraPadding
     * set -1 or negative value to keep current
     * */
    fun updateCameraPadding(
        top: Double = -1.0,
        left: Double = -1.0,
        bottom: Double = -1.0,
        right: Double = -1.0,
    ) {
        mapCameraPadding = EdgeInsets(
            if (top < 0) mapCameraPadding.top else top,
            if (left < 0) mapCameraPadding.left else left,
            if (bottom < 0) mapCameraPadding.bottom else bottom,
            if (right < 0) mapCameraPadding.right else right,
        )
    }

    private fun bestCameraOption(locations: List<Point>): CameraOptions {
        val allPoints = arrayListOf<Geometry>().apply {
            addAll(locations)
        }
        savedPlacePoints?.let { allPoints.addAll(it.geometries()) }
        val cameraOptions = mapboxMap.cameraForGeometry(
            GeometryCollection.fromGeometries(allPoints),
            mapCameraPadding,
            bearing = 0.0,
            pitch = 0.0
        )
        val zoom = (cameraOptions.zoom ?: Constants.MAX_ZOOM_LEVEL).coerceAtMost(Constants.MAX_ZOOM_LEVEL).coerceAtLeast(10.0)
        return cameraOptions.toBuilder().zoom(zoom).build()
    }


    internal fun recenterMap(getCurrentLocations: () -> List<Point>, animationEndCB: () -> Unit) {
        getCurrentLocations().takeIf { it.isNotEmpty() }?.let { locations ->
            BreezeMapZoomUtil.recenterMapToLocation(
                mapboxMap,
                bestCameraOption(locations),
                animationEndCB = animationEndCB
            )
        }

    }

    fun recenterToSearchLocation(searchLocation: SearchLocation) {
        val latitude = searchLocation.lat?.toDoubleOrNull() ?: return
        val longitude = searchLocation.long?.toDoubleOrNull() ?: return
        BreezeMapZoomUtil.recenterMapToLocation(
            mapboxMap,
            CameraOptions.Builder()
                .zoom(Constants.MAX_ZOOM_LEVEL)
                .center(Point.fromLngLat(longitude, latitude))
                .padding(mapCameraPadding)
                .bearing(0.0)
                .pitch(0.0)
                .build()
        )
    }
}
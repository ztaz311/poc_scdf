package com.ncs.breeze.ui.dashboard.fragments.view

import android.app.Dialog
import android.graphics.Color
import android.graphics.Rect
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.WindowManager
import android.view.inputmethod.EditorInfo
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import com.ncs.breeze.R
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.Event
import com.ncs.breeze.common.analytics.Screen
import com.breeze.model.constants.Constants
import com.breeze.customization.view.BreezeButton
import com.breeze.customization.view.BreezeEditText
import com.ncs.breeze.databinding.FragmentChangePasswordBinding
import com.ncs.breeze.ui.base.BaseActivity
import com.ncs.breeze.ui.base.BaseFragment
import com.ncs.breeze.ui.dashboard.activity.DashboardActivity
import com.ncs.breeze.ui.dashboard.fragments.viewmodel.ChangePasswordViewModel
import com.ncs.breeze.ui.login.fragment.forgotPassword.view.ForgetPasswordEmailFragment
import javax.inject.Inject

@Deprecated(level = DeprecationLevel.WARNING, message = "Too old")
class ChangePasswordFragment :
    BaseFragment<FragmentChangePasswordBinding, ChangePasswordViewModel>() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val viewModel: ChangePasswordViewModel by viewModels {
        viewModelFactory
    }

    lateinit var dialog: Dialog

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initialize()
    }

    fun initialize() {
        viewBinding.backButton.setOnClickListener {
            (activity as DashboardActivity).hideKeyboard()
            onBackPressed()
        }

        viewBinding.rlScrollView.setOnClickListener {
            hideKeyboard()
        }

        viewBinding.rlHeading.setOnClickListener {
            hideKeyboard()
        }


        viewBinding.forgotPassword.setOnClickListener {
            Analytics.logClickEvent(Event.FORGET_PASSWORD_CLICK, getScreenName())
            (activity as DashboardActivity).addFragment(
                ForgetPasswordEmailFragment.newInstance(),
                null,
                Constants.TAGS.FORGET_PASSWORD_TAG
            )
        }

        viewBinding.editOldPwd.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                viewBinding.editOldPwd.setBGNormal()
            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            }

            override fun afterTextChanged(s: Editable) {
            }
        })
        viewBinding.editOldPwd.setEditTextListener(object :
            BreezeEditText.BreezeEditTextListener {
            override fun onBreezeEditTextFocusChange(isFocused: Boolean) {
                if (!isFocused) {
                    Analytics.logEditEvent(Event.OLD_PASSWORD_EDIT, getScreenName())
                }
            }

            override fun onClearClick() {
            }
        })
        viewBinding.editOldPwd.setScreenName(getScreenName())

        viewBinding.editNewPwd.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                viewBinding.editNewPwd.setBGNormal()
            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            }

            override fun afterTextChanged(s: Editable) {
            }
        })
        viewBinding.editNewPwd.setEditTextListener(object :
            BreezeEditText.BreezeEditTextListener {
            override fun onBreezeEditTextFocusChange(isFocused: Boolean) {
                if (!isFocused) {
                    Analytics.logEditEvent(Event.NEW_PASSWORD_EDIT, getScreenName())
                }
            }

        })
        viewBinding.editNewPwd.setScreenName(getScreenName())

        viewBinding.editOldPwd.getEditText().imeOptions = EditorInfo.IME_ACTION_NEXT
        viewBinding.editOldPwd.getEditText()
            .setOnEditorActionListener(TextView.OnEditorActionListener { _, actionId, _ ->
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    viewBinding.editNewPwd.getEditText().requestFocus()
                    return@OnEditorActionListener true
                }
                false
            })
        viewBinding.editNewPwd.getEditText().imeOptions = EditorInfo.IME_ACTION_DONE

        viewBinding.editNewPwd.getEditText()
            .setOnEditorActionListener(TextView.OnEditorActionListener { v, actionId, event ->
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    // Call API
                    changePassword()
                    return@OnEditorActionListener true
                }
                false
            })
        viewBinding.savePassword.setOnClickListener {
            changePassword()
        }
    }

    fun changePassword() {
        hideKeyboard()
        if (checkValidPassword()) {
            viewModel.changePasswordSuccessful.observe(viewLifecycleOwner) {
                viewModel.changePasswordSuccessful.removeObservers(this)
                if (it) {
                    showSuccessDialog()
                } else {
                    viewBinding.editOldPwd.showErrorView(getString(R.string.change_pwd_same_error))
                }
            }
            viewModel.changePasswordUnsuccessful.observe(viewLifecycleOwner) {
                viewModel.changePasswordUnsuccessful.removeObservers(this)

                viewBinding.editOldPwd.showErrorView(getString(R.string.change_pwd_incorrect_error))
            }

            Analytics.logEvent(Event.UPDATE_PASSWORD, Event.UPDATE_PASSWORD_DONE, getScreenName())
            viewModel.changePassword(
                viewBinding.editOldPwd.getEnteredText(),
                viewBinding.editNewPwd.getEnteredText()!!
            )
        }
    }

    private fun checkValidPassword(): Boolean {
        var isValid = true

        if (TextUtils.isEmpty(viewBinding.editOldPwd.getEnteredText())) {
            isValid = false
            viewBinding.editOldPwd.setBGError()
        }

        if (TextUtils.isEmpty(viewBinding.editNewPwd.getEnteredText())) {
            isValid = false
            viewBinding.editNewPwd.setBGError()
        } else if (TextUtils.equals(
                viewBinding.editNewPwd.getEnteredText(),
                viewBinding.editOldPwd.getEnteredText()
            )
        ) {
            isValid = false
            viewBinding.editNewPwd.showErrorView(getString(R.string.change_pwd_same_error))
        } else {
            if (!viewBinding.editNewPwd.getEnteredText()
                    .matches(Regex(Constants.REGEX.PASSWORD_REGEX))
            ) {
                isValid = false
                viewBinding.editNewPwd.showErrorView(resources.getString(R.string.error_password_incorrect_format))
            }
        }
        return isValid
    }

    override fun inflateViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?,
        attachToContainer: Boolean
    ) = FragmentChangePasswordBinding.inflate(inflater, container, attachToContainer)


    override fun getViewModelReference(): ChangePasswordViewModel {
        return viewModel
    }

    fun hideKeyboard() {
        viewBinding.editOldPwd.clearFocus()
        viewBinding.editNewPwd.clearFocus()
        (activity as DashboardActivity).hideKeyboard()
    }

    private fun showSuccessDialog() {

        dialog = activity?.let {
            Dialog(it)
        }!!

        dialog.setContentView(R.layout.dialog_create_account_successful)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCanceledOnTouchOutside(false)
        //dialog.setCancelable(true)

        val imgView = dialog.findViewById<ImageView>(R.id.popup_image)
        imgView.setImageResource(R.drawable.password_changed_successfully)

        val greeting = dialog.findViewById<TextView>(R.id.greeting)
        greeting.setText(R.string.good_job)

        val successMsg = dialog.findViewById<TextView>(R.id.success_message)
        successMsg.setText(R.string.pwd_change_successful)

        val button = dialog.findViewById<BreezeButton>(R.id.button_okay)
        button.setOnClickListener {
            dialog.dismiss()
            (activity as BaseActivity<*, *>).hideKeyboard()
            onBackPressed()
        }

        dialog.show()

        val displayRectangle = Rect()
        val window: Window = requireActivity().window
        window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle)

        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window!!.getAttributes())
        lp.width = (displayRectangle.width() * 0.9f).toInt()
        dialog.window!!.setAttributes(lp)
    }

    override fun getScreenName(): String {
        return Screen.SETTINGS_ACCOUNT_RESET_PASSWORD
    }
}
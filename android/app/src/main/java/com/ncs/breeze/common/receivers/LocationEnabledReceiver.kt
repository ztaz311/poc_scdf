package com.ncs.breeze.common.receivers

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.location.LocationManager
import timber.log.Timber


class LocationEnabledReceiver : BroadcastReceiver() {


    override fun onReceive(context: Context?, intent: Intent?) {
        if (locationReceiverListener != null) {
            var currentStatus = isEnabledOrNot(context, intent)
            if (lastDetectedStatus == null || lastDetectedStatus != currentStatus) {
                lastDetectedStatus = currentStatus
                Timber.d("Change in GPS is observed")
                locationReceiverListener!!.onGPSLocation(isEnabledOrNot(context, intent))
            }
        }
    }

    interface LocationReceiverListener {
        fun onGPSLocation(isEnabled: Boolean)
    }

    companion object {
        var locationReceiverListener: LocationReceiverListener? = null
        var lastDetectedStatus: Boolean? = null
    }


    private fun isEnabledOrNot(context: Context?, intent: Intent?): Boolean {
        val lm = context!!.getSystemService(Context.LOCATION_SERVICE) as LocationManager?
        var enabled = false
        if (LocationManager.PROVIDERS_CHANGED_ACTION.equals(intent!!.getAction())) {

            val lm = context!!.getSystemService(Context.LOCATION_SERVICE) as LocationManager?
            val isGpsEnabled = lm!!.isProviderEnabled(LocationManager.GPS_PROVIDER);
            val isNetworkEnabled = lm!!.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            enabled = isGpsEnabled || isNetworkEnabled
        }
        return enabled
    }

}
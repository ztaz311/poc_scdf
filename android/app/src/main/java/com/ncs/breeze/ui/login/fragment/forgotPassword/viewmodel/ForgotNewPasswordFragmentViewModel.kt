package com.ncs.breeze.ui.login.fragment.forgotPassword.viewmodel

import android.app.Application
import com.ncs.breeze.ui.base.BaseFragmentViewModel
import javax.inject.Inject

class ForgotNewPasswordFragmentViewModel @Inject constructor(application: Application) :
    BaseFragmentViewModel(
        application
    ) {
}
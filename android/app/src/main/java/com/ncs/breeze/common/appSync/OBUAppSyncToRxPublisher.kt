package com.ncs.breeze.common.appSync

import com.breeze.model.TripType
import com.breeze.model.obu.OBUCardStatusDataAppSync
import com.breeze.model.obu.OBUCardStatusEventData
import com.breeze.model.obu.OBUParkingAvailabilitiesAppSync
import com.breeze.model.obu.OBUParkingAvailability
import com.breeze.model.obu.OBURoadEventData
import com.breeze.model.obu.OBUTravelTimeData
import com.breeze.model.obu.OBUTravelTimeDataAppSync
import com.google.gson.Gson
import com.ncs.breeze.App
import com.ncs.breeze.common.constant.OBUConstants
import com.ncs.breeze.common.helper.obu.OBUDataHelper
import com.ncs.breeze.common.rxbus.RxBus
import com.ncs.breeze.common.rxbus.RxEvent
import com.ncs.breeze.common.utils.TripLoggingManager

object OBUAppSyncToRxPublisher {
    fun handleAppSyncEvent(eventName: String?, dataJson: String?) {
        kotlin.runCatching {
            when (eventName) {
                OBURoadEventData.EVENT_TYPE_ERP_CHARGING,
                OBURoadEventData.EVENT_TYPE_ERP_SUCCESS,
                OBURoadEventData.EVENT_TYPE_ERP_FAILURE,
                OBURoadEventData.EVENT_TYPE_PARKING_FEE,
                OBURoadEventData.EVENT_TYPE_PARKING_SUCCESS,
                OBURoadEventData.EVENT_TYPE_PARKING_FAILURE,

                OBURoadEventData.EVENT_TYPE_SCHOOL_ZONE,
                OBURoadEventData.EVENT_TYPE_SPEED_CAMERA,
                OBURoadEventData.EVENT_TYPE_SILVER_ZONE,

                OBURoadEventData.EVENT_TYPE_EVENT,
                OBURoadEventData.EVENT_TYPE_ROAD_DIVERSION,
                OBURoadEventData.EVENT_TYPE_GENERAL_MESSAGE,
                OBURoadEventData.EVENT_TYPE_SEASON_PARKING,
                OBURoadEventData.EVENT_TYPE_BUS_LANE
                -> {
                    handleOBURoadEvent(eventName, dataJson)
                }

                OBUParkingAvailability.EVENT_TYPE_PARKING_AVAILABILITY -> {
                    if (TripLoggingManager.getInstance()?.currentTripLogType == TripType.OBULITE) {

                        val data =
                            Gson().fromJson(
                                dataJson,
                                OBUParkingAvailabilitiesAppSync::class.java
                            )
                        RxBus.publish(RxEvent.OBUParkingAvailabilityEvent(data.carparks))
                    }
                }

                OBUTravelTimeData.EVENT_TYPE_TRAVEL_TIME -> {
                    val data = Gson().fromJson(dataJson, OBUTravelTimeDataAppSync::class.java)
                    RxBus.publish(RxEvent.OBUTravelTimeEvent(data.travelTime))
                }

                OBUCardStatusEventData.EVENT_TYPE_CARD_ERROR,
                OBUCardStatusEventData.EVENT_TYPE_CARD_EXPIRED,
                OBUCardStatusEventData.EVENT_TYPE_CARD_INSUFFICIENT,
                OBUCardStatusEventData.EVENT_TYPE_NO_CARD -> {
                    val data = Gson().fromJson(dataJson, OBUCardStatusDataAppSync::class.java)
                    RxBus.publish(RxEvent.OBUCardInfoEvent(data.toOBUCardStatusEventData()))
                    val roadData = OBURoadEventData(
                        OBURoadEventData.EVENT_TYPE_ERP_FAILURE,
                        getMessageFromCardEventType(eventName),
                        null, null, null, data.chargeAmount,
                    )
                    RxBus.publish(RxEvent.OBURoadEvent(roadData))
                }

                OBUCardStatusEventData.EVENT_TYPE_CARD_VALID -> {
                    val data = Gson().fromJson(dataJson, OBUCardStatusDataAppSync::class.java)
                    OBUDataHelper.cardBalance = data.balance
                    if (!OBUDataHelper.isFirstOBUCardValidArrived
                        && data.balance < (OBUConstants.OBU_LOW_CARD_THRESHOLD / 100.0)
                    ) {
                        OBUDataHelper.isFirstOBUCardValidArrived = true
                        RxBus.publish(RxEvent.OBULowCardBalanceEvent((data.balance * 100).toInt()))
                    }
                    RxBus.publish(RxEvent.OBUCardInfoEvent(data.toOBUCardStatusEventData()))
                }

                else -> {}
            }
        }
    }

    private fun handleOBURoadEvent(eventName: String, dataJson: String?) {
        // Not handling upcoming ERP events if OBU is not connected since it's already handled by Ehorizon
        if (App.instance?.obuConnectionHelper?.hasActiveOBUConnection() == false
            && eventName == OBURoadEventData.EVENT_TYPE_ERP_CHARGING
        ) return
        val data = Gson().fromJson(dataJson, OBURoadEventData::class.java)
        RxBus.publish(RxEvent.OBURoadEvent(data))
        data.chargingInfo?.let {
            RxBus.publish(RxEvent.OBUChargeInfoEvent(it))
        }
        data.tripSummary?.let { RxBus.publish(RxEvent.OBUTotalTripSummaryEvent(it)) }
        data.payments?.let { RxBus.publish(RxEvent.OBUPaymentHistoriesEvent(it)) }
    }

    private fun getMessageFromCardEventType(cardEventType: String): String {
        return when (cardEventType) {
            OBUCardStatusEventData.EVENT_TYPE_CARD_EXPIRED -> "Card Expired"
            OBUCardStatusEventData.EVENT_TYPE_CARD_INSUFFICIENT -> "Insufficient Balance"
            OBUCardStatusEventData.EVENT_TYPE_CARD_ERROR -> "Card Error"
            else -> "System Error"
        }
    }

}
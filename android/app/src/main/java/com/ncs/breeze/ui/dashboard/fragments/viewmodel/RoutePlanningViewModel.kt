package com.ncs.breeze.ui.dashboard.fragments.viewmodel

import android.app.Application
import android.location.Location
import android.util.Log
import androidx.lifecycle.viewModelScope
import com.breeze.model.AmenityAddressType
import com.breeze.model.DestinationAddressDetails
import com.breeze.model.DestinationAddressTypes
import com.breeze.model.SelectedAmenity
import com.breeze.model.api.ErrorData
import com.breeze.model.api.request.AmenitiesRequest
import com.breeze.model.api.request.BroadcastTravelZoneDestination
import com.breeze.model.api.request.BroadcastTravelZoneFilterRequest
import com.breeze.model.api.request.BroadcastTravelZoneRoute
import com.breeze.model.api.request.ETARequest
import com.breeze.model.api.request.SelectAddressRequest
import com.breeze.model.api.request.TripPlannerRequest
import com.breeze.model.api.response.BroadcastMessageResponse
import com.breeze.model.api.response.BroadcastTravelZoneFilterResponse
import com.breeze.model.api.response.ETAResponse
import com.breeze.model.api.response.SettingsItem
import com.breeze.model.api.response.SettingsResponse
import com.breeze.model.api.response.amenities.Amenities
import com.breeze.model.api.response.amenities.BaseAmenity
import com.breeze.model.api.response.amenities.CarParkDetailResponse
import com.breeze.model.api.response.amenities.PlugTypes
import com.breeze.model.api.response.amenities.ResponseAmenities
import com.breeze.model.api.response.tbr.ResponseCongestionDetail
import com.breeze.model.constants.AmenityType.CARPARK
import com.breeze.model.constants.AmenityType.EVCHARGER
import com.breeze.model.constants.AmenityType.PETROL
import com.breeze.model.constants.Constants
import com.breeze.model.constants.TYPE_BROADCAST_MESSAGE
import com.breeze.model.enums.RoutePreference
import com.google.gson.JsonElement
import com.mapbox.api.directions.v5.models.DirectionsResponse
import com.mapbox.geojson.LineString
import com.mapbox.geojson.Point
import com.mapbox.navigation.base.route.NavigationRoute
import com.ncs.breeze.car.breeze.utils.BreezeCarUtil
import com.ncs.breeze.common.extensions.android.getApp
import com.ncs.breeze.common.extensions.android.getUserDataPreference
import com.ncs.breeze.common.extensions.mapbox.compressToGzip
import com.ncs.breeze.common.helper.route.RoutePlanningHelper
import com.ncs.breeze.common.model.RouteAdapterObjects
import com.ncs.breeze.common.model.RouteDepartureDetails
import com.ncs.breeze.common.model.SimpleRouteAdapterObjects
import com.ncs.breeze.common.remote.ApiHelper
import com.ncs.breeze.common.remote.ApiObserver
import com.ncs.breeze.common.storage.BreezeUserPreference
import com.ncs.breeze.common.utils.BreezeERPRefreshUtil
import com.ncs.breeze.common.utils.BreezeMapDataHolder
import com.ncs.breeze.common.utils.BreezeRouteSortingUtil
import com.ncs.breeze.common.utils.InitObjectUtilsController
import com.ncs.breeze.common.utils.Utils
import com.ncs.breeze.components.SingleLiveEvent
import com.ncs.breeze.components.map.profile.ProfileZoneLayer
import com.ncs.breeze.ui.base.BaseFragmentViewModel
import com.ncs.breeze.ui.dashboard.fragments.adapter.AmenityItemAdapter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import timber.log.Timber
import java.io.IOException
import java.util.concurrent.ConcurrentHashMap
import javax.inject.Inject

class RoutePlanningViewModel @Inject constructor(
    val apiHelper: ApiHelper,
    application: Application
) :
    BaseFragmentViewModel(application) {
    var mAPIHelper: ApiHelper = apiHelper
    var etaAPISuccessful: SingleLiveEvent<ETARequest> =
        SingleLiveEvent()

    var isNotExistAnyCp: SingleLiveEvent<Boolean> = SingleLiveEvent()

    var plannedTripChanged = false

    val amenityDataMap = ConcurrentHashMap<String, AmenityData>()

    var carParkProfileDetails: SingleLiveEvent<CarParkProfileDetails> = SingleLiveEvent()

    var mListCarPark: SingleLiveEvent<AmenityData> = SingleLiveEvent()
    var mListAmenities: SingleLiveEvent<ConcurrentHashMap<String, AmenityData>> = SingleLiveEvent()
    private var mapOfRoutePreference: ConcurrentHashMap<RoutePreference, List<RouteAdapterObjects>> =
        ConcurrentHashMap<RoutePreference, List<RouteAdapterObjects>>()
    var compressedOriginalRouteData = ConcurrentHashMap<DirectionsResponse, ByteArray>()
    var routesRetrieved: SingleLiveEvent<ConcurrentHashMap<RoutePreference, List<RouteAdapterObjects>>> =
        SingleLiveEvent()
    var walkingRoutesRetrieved: SingleLiveEvent<NavigationRoute?> = SingleLiveEvent()

    var planSaved: SingleLiveEvent<Boolean> = SingleLiveEvent()

    var carParkDetail: SingleLiveEvent<CarParkDetailResponse> = SingleLiveEvent()

    var mBroadcastMessage: SingleLiveEvent<BroadcastMessageResponse> =
        SingleLiveEvent()

    private val EV_AMENITY_LIST = mapOf(
        "Type-2" to "Type 2",
        "Combo-2" to "CCS/SAE",
        "CHAdeMo" to "chademo"
    )

    private val PETROL_AMENITY_LIST = mapOf(
        "SPC" to "SPC",
        "Caltex" to "Caltex",
        "ESSO" to "Esso",
        "Sinopec" to "sinopec",
        "Shell" to "Shell",
    )

    // synchronizes response for fetching amenities
    private val amenitiesMutex = Mutex()

    @Inject
    lateinit var breezeRouteSortingUtil: BreezeRouteSortingUtil

    @Inject
    lateinit var breezeERPUtil: BreezeERPRefreshUtil


    fun selectAddressAPI(
        destinationAddressDetails: DestinationAddressDetails,
        currentLocation: Location
    ) {
        CoroutineScope(Dispatchers.IO).launch {
            var request = SelectAddressRequest()
            request.startAddress1 =
                Utils.getAddressFromLocation(currentLocation!!, getApp())
            request.startAddress2 = ""
            request.startLatitude = currentLocation.latitude.toString()
            request.startLongtitude = currentLocation.longitude.toString()
            request.destAddress1 = destinationAddressDetails.address1
            request.destAddress2 = destinationAddressDetails.address2
            request.destLatitude = destinationAddressDetails.lat
            request.destLongtitude = destinationAddressDetails.long
            request.source = destinationAddressDetails.destinationAddressType?.type
            if (destinationAddressDetails.destinationAddressType == DestinationAddressTypes.EASY_BREEZIES) {
                request.addressid = destinationAddressDetails.easyBreezyAddressID.toString()
            }
            request.ranking = destinationAddressDetails.ranking
            request.eventtime = Utils.CURRENT_DATE()

            mAPIHelper.selectAddress(request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : ApiObserver<JsonElement>(compositeDisposable) {
                    override fun onSuccess(data: JsonElement) {
                        Timber.d("API is successful: " + data.toString())
                    }

                    override fun onError(e: ErrorData) {
                        Timber.d("API failed: " + e.toString())
                    }

                })
        }
    }

    fun getBroadcastMessage(idCarpark: String) {
        CoroutineScope(Dispatchers.IO).launch {
            BreezeCarUtil.apiHelper.getBroadcastMessage(idCarpark, TYPE_BROADCAST_MESSAGE.PLANNING)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : ApiObserver<BroadcastMessageResponse>(compositeDisposable) {
                    override fun onSuccess(data: BroadcastMessageResponse) {
                        data.apply {
                            mBroadcastMessage.postValue(data)
                        }
                    }

                    override fun onError(e: ErrorData) {

                    }
                })
        }
    }

    /**
     * get carpark detail
     */
    fun getCarparkDetail(idCarpark: String) {
        CoroutineScope(Dispatchers.IO).launch {
            BreezeCarUtil.apiHelper.getCarParkDetail(idCarpark)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : ApiObserver<CarParkDetailResponse>(compositeDisposable) {
                    override fun onSuccess(data: CarParkDetailResponse) {
                        data.apply {
                            carParkDetail.postValue(data)
                        }
                    }

                    override fun onError(e: ErrorData) {}
                })
        }
    }

    fun getCarparkDetailWithCallback(idCarpark: String, callback: (BaseAmenity?) -> Unit) {
        CoroutineScope(Dispatchers.IO).launch {
            BreezeCarUtil.apiHelper.getCarParkDetail(idCarpark)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : ApiObserver<CarParkDetailResponse>(compositeDisposable) {
                    override fun onSuccess(data: CarParkDetailResponse) {
                        callback.invoke(data.data)

                    }

                    override fun onError(e: ErrorData) {}
                })
        }
    }

    fun getBroadcastFilter(
        allRoutes: ArrayList<SimpleRouteAdapterObjects>,
        departTimeInMillis: Long,
        arrivalTimeInMillis: Long,
        destinationLat: Double,
        destinationLng: Double,
        apiObserver: ApiObserver<BroadcastTravelZoneFilterResponse>
    ) {
        CoroutineScope(Dispatchers.IO).launch {
            val data = allRoutes.map {
                it.route.geometry()?.let { geometry ->
                    LineString.fromPolyline(
                        geometry,
                        Constants.POLYLINE_PRECISION
                    ).coordinates().map { c ->
                        listOf(c.latitude(), c.longitude())
                    }.let { d -> BroadcastTravelZoneRoute(d.toList()) }
                }
            }
            mAPIHelper.broadcastTravelZoneFilter(
                BroadcastTravelZoneFilterRequest(
                    data,
                    arrivalTimeInMillis,
                    departTimeInMillis,
                    BroadcastTravelZoneDestination(destinationLat, destinationLng)
                )
            )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(apiObserver)
        }
    }

    /**
     * get all amenities
     */
    fun fetchAmenities(originPoint: Point, destination: Point, timestamp: Long) {
        viewModelScope.launch(Dispatchers.IO) {
            val rad = getApp().getUserDataPreference()?.getEvPetrolRange()?.div(1000.0)
                ?: Constants.DEFAULT_PETROL_EV_RADIUS
            val maxRad = getApp().getUserDataPreference()?.getEvPetrolMaxRange()?.div(1000.0)
                ?: Constants.DEFAULT_PETROL_EV_MAX_RADIUS
            val resultCount = getApp().getUserDataPreference()?.getEvPetrolResultCount()
                ?: Constants.DEFAULT_PETROL_EV_RESULT_COUNT
            val requestOrigin = AmenitiesRequest(
                lat = originPoint.latitude(),
                long = originPoint.longitude(),
                rad = rad,
                maxRad = maxRad,
                resultCount = resultCount,
                arrivalTime = timestamp,
            )

            val destRequest = AmenitiesRequest(
                lat = destination.latitude(),
                long = destination.longitude(),
                rad = rad,
                maxRad = maxRad,
                resultCount = resultCount,
                arrivalTime = timestamp,
            )

            listOf(PETROL, EVCHARGER).let {
                requestOrigin.setListType(it)
                destRequest.setListType(it)
            }

            val originAmenitiesAsync = async {
                kotlin.runCatching { mAPIHelper.getAmenitiesAsync(requestOrigin) }
            }
            val destinationAmenitiesAsync =
                async { kotlin.runCatching { mAPIHelper.getAmenitiesAsync(destRequest) } }


            val responseOrigin = originAmenitiesAsync.await()
            val responseDestination = destinationAmenitiesAsync.await()

            amenitiesMutex.withLock {
                if (responseOrigin.isSuccess && responseDestination.isSuccess) {
                    resetAmenityMap()
                    responseOrigin.getOrNull()?.apply {
                        amenities.forEach { amenities ->

                            amenities.data.forEach {
                                if (amenities.type != null) {
                                    addAmenityToMap(
                                        it,
                                        amenities,
                                        AmenityAddressType.SOURCE,
                                        AMENITY_COUNT
                                    )
                                }
                            }
                            amenityDataMap[amenities.type]?.let {
                                if (!isAnyProviderSelected(it)) {
                                    selectMinimumSourceAmenities(it, AMENITY_COUNT)
                                }
                            }

                        }
                    }

                    responseDestination.getOrNull()?.apply {
                        amenities.forEach { amenities ->
                            amenities.data.forEach {
                                if (amenities.type != null) {
                                    addAmenityToMap(
                                        it,
                                        amenities,
                                        AmenityAddressType.DESTINATION,
                                        AMENITY_COUNT
                                    )
                                }
                            }

                            amenityDataMap[amenities.type]?.let {
                                if (!isAnyProviderSelected(it)) {
                                    selectMinimumDestinationAmenities(it, AMENITY_COUNT)
                                }
                            }
                        }
                    }

                }
                mListAmenities.postValue(amenityDataMap)
            }
        }
    }

    private fun resetAmenityMap() {
        amenityDataMap.remove(PETROL)
        amenityDataMap.remove(EVCHARGER)
        initAmenityDataForType(null, PETROL)
        initAmenityDataForType(null, EVCHARGER)
    }

    /**
     * fetch carparks
     */
    fun fetchCarParks(
        destination: Point,
        timestamp: Long,
        dest: String?,
        carParkAvailability: String
    ) {
        viewModelScope.launch(Dispatchers.IO) {
            val responseCarParkDest =
                fetchCarParkData(destination, timestamp, dest, carParkAvailability)
            postCarParkData(responseCarParkDest)
        }
    }

    /**
     * fetch carparks
     */
    fun validateCarParkCongestionAndVouchers(
        profile: String?,
        typeZone: ProfileZoneLayer.ProfileZoneType?,
        destination: Point,
        timestamp: Long,
        refreshVouchers: Boolean,
        dest: String?,
        carParkAvailability: String
    ) {
        viewModelScope.launch(Dispatchers.IO) {

            val responseCarParkDeferred =
                async { fetchCarParkData(destination, timestamp, dest, carParkAvailability) }
            var congestionResults: ArrayList<ResponseCongestionDetail>? = null
            profile?.let {
                val congestionResultsDeferred =
                    async { apiHelper.getZoneCongestionDetailsAsync(profile) }
                congestionResults = congestionResultsDeferred.await()
            }
            val responseCarParkDest = responseCarParkDeferred.await().getOrNull()
            setCarParkCongestionDataAndDetectVouchers(
                profile,
                typeZone,
                refreshVouchers,
                congestionResults,
                responseCarParkDest
            )
        }
    }


    private suspend fun CoroutineScope.fetchCarParkData(
        destination: Point,
        timestamp: Long,
        dest: String?,
        carParkAvailability: String
    ): Result<ResponseAmenities> {
        val destCarParkRequest = AmenitiesRequest(
            lat = destination.latitude(),
            long = destination.longitude(),
            rad = BreezeUserPreference.getInstance(getApp())
                .getCarParkDistanceSettingValue(BreezeCarUtil.masterlists),
            resultCount = BreezeUserPreference.getInstance(getApp())
                .getCarParkCountSettingValue(BreezeCarUtil.masterlists),
            maxRad = RADIUS_CARPARK_MAX,
        )
        destCarParkRequest.setListType(
            listType = listOf(CARPARK),
            availableBand = BreezeUserPreference.getInstance(getApp())
                .getCarParkAvailabilityValue(
                    masterList = BreezeCarUtil.masterlists,
                    carParkAvailabilityCode = carParkAvailability
                )
        )
        destCarParkRequest.arrivalTime = timestamp
        destCarParkRequest.isVoucher = true
        dest?.let {
            destCarParkRequest.destName = it
        }
        val carParkDest =
            async { kotlin.runCatching { mAPIHelper.getAmenitiesAsync(destCarParkRequest) } }
        val responseCarParkDest = carParkDest.await()
        return responseCarParkDest
    }

    private suspend fun postCarParkData(responseCarParkDest: Result<ResponseAmenities>) {
        amenitiesMutex.withLock {
            if (responseCarParkDest.isSuccess) {
                amenityDataMap.remove(CARPARK)
                responseCarParkDest.getOrNull()?.apply {
                    handleCarParkResponse(this, true)
                }
            }
            mListCarPark.postValue(amenityDataMap[CARPARK])
        }
    }

    private suspend fun setCarParkCongestionDataAndDetectVouchers(
        profile: String?,
        typeZone: ProfileZoneLayer.ProfileZoneType?,
        refreshVouchers: Boolean,
        congestionDetails: ArrayList<ResponseCongestionDetail>?,
        responseCarParkDest: ResponseAmenities?
    ) {
        amenitiesMutex.withLock {
            if (responseCarParkDest != null) {
                amenityDataMap.remove(CARPARK)
                val voucherPresent = handleCarParkResponse(responseCarParkDest, false)
                carParkProfileDetails.postValue(
                    CarParkProfileDetails(
                        profile,
                        typeZone,
                        congestionDetails,
                        if (refreshVouchers) voucherPresent else false
                    )
                )
            } else {
                carParkProfileDetails.postValue(
                    CarParkProfileDetails(
                        profile,
                        typeZone,
                        congestionDetails,
                        false
                    )
                )
            }
        }
    }

    private fun handleCarParkResponse(
        response: ResponseAmenities,
        showCPWarning: Boolean
    ): Boolean {
        var isNotExistCarparkInRD = true
        var voucherAmenityDetected = false

        response.amenities.forEach { amenities ->
            for (it in amenities.data) {
                if (it.hasVouchers) {
                    voucherAmenityDetected = true
                }
                if (amenities.type != null) {
                    isNotExistCarparkInRD = false
                    addAmenityToMap(
                        it,
                        amenities,
                        AmenityAddressType.DESTINATION,
                        CARPARK_COUNT
                    )
                }
            }
            amenityDataMap[amenities.type]?.let {
                selectMinimumDestinationAmenities(it, CARPARK_COUNT)
            }
        }
        /**
         * post value to check show or not show notification have no parking on map
         */
        if (showCPWarning) {
            isNotExistAnyCp.postValue(isNotExistCarparkInRD)
        }
        return voucherAmenityDetected
    }

    fun createTripPlan(request: TripPlannerRequest) {
        viewModelScope.launch(Dispatchers.IO) {
            val tripLogResponse = kotlin.runCatching { mAPIHelper.createTripPlan(request) }

            if (tripLogResponse.isSuccess) {
                val tripData = tripLogResponse.getOrNull()
                if (tripData != null && tripData.tripPlannerId != null) {
                    planSaved.postValue(true)
                    return@launch
                }
            }
            planSaved.postValue(false)
        }
    }

    fun updateTripPlan(tripPlannerId: Long, request: TripPlannerRequest) {
        viewModelScope.launch(Dispatchers.IO) {
            val tripLogResponse =
                kotlin.runCatching { mAPIHelper.updateTripPlan(tripPlannerId, request) }

            if (tripLogResponse.isSuccess) {
                val tripData = tripLogResponse.getOrNull()
                if (tripData?.tripPlannerId != null) {
                    planSaved.postValue(true)
                    return@launch
                }
            }
            planSaved.postValue(false)
        }
    }


    fun recalculateEvChargerAmenities() {
        amenityDataMap[EVCHARGER]?.let {

            it.sourceCount = 0
            it.destinationCount = 0
            it.amenities.forEach { selectedAmenity ->
                selectedAmenity.selected = false
                if (it.sourceCount >= AMENITY_COUNT && it.destinationCount > AMENITY_COUNT) {
                    return@forEach
                }
                if (selectedAmenity.baseAmenity.plugTypes != null) {
                    for (plugType: PlugTypes in selectedAmenity.baseAmenity.plugTypes!!) {
                        if (plugType.name == null) {
                            continue
                        }
                        val amenityItem = it.distinctAmenities!![plugType.name]
                        if (amenityItem != null && amenityItem.selected) {
                            if ((selectedAmenity.amenityAddressType == AmenityAddressType.DESTINATION) && it.destinationCount < AMENITY_COUNT) {
                                selectedAmenity.selected = true
                                it.destinationCount++
                                break;
                            }
                            if ((selectedAmenity.amenityAddressType == AmenityAddressType.SOURCE) && it.sourceCount < AMENITY_COUNT) {
                                selectedAmenity.selected = true
                                it.sourceCount++
                                break;
                            }
                        }
                    }
                }
            }
            val isAnySelected = isAnyProviderSelected(it)
            if (!isAnySelected) {
                if (it.sourceCount < AMENITY_COUNT) {
                    selectMinimumSourceAmenities(it, AMENITY_COUNT)
                }
                if (it.destinationCount < AMENITY_COUNT) {
                    selectMinimumDestinationAmenities(it, AMENITY_COUNT)
                }
            }

        }
    }

    fun recalculatePetrolAmenities() {
        amenityDataMap[PETROL]?.let {
            it.sourceCount = 0
            it.destinationCount = 0
            it.amenities.forEach { selectedAmenity ->
                selectedAmenity.selected = false
                if (it.sourceCount >= AMENITY_COUNT && it.destinationCount > AMENITY_COUNT) {
                    return@forEach
                }
                val amenityItem = it.distinctAmenities!![selectedAmenity.baseAmenity.provider]
                if (amenityItem != null && amenityItem.selected) {
                    if ((selectedAmenity.amenityAddressType == AmenityAddressType.DESTINATION) && it.destinationCount < AMENITY_COUNT) {
                        selectedAmenity.selected = true
                        it.destinationCount++
                    }
                    if ((selectedAmenity.amenityAddressType == AmenityAddressType.SOURCE) && it.sourceCount < AMENITY_COUNT) {
                        selectedAmenity.selected = true
                        it.sourceCount++
                    }
                }
            }
            val isAnySelected = isAnyProviderSelected(it)
            if (!isAnySelected) {
                if (it.sourceCount < AMENITY_COUNT) {
                    selectMinimumSourceAmenities(it, AMENITY_COUNT)
                }
                if (it.destinationCount < AMENITY_COUNT) {
                    selectMinimumDestinationAmenities(it, AMENITY_COUNT)
                }
            }

        }
    }

    private fun isAnyProviderSelected(it: AmenityData): Boolean {
        var isAnySelected = false
        it.distinctAmenities?.let { distinctAmenities ->
            for (distinctAmenity in distinctAmenities) {
                if (distinctAmenity.value.selected) {
                    isAnySelected = true
                    break
                }
            }
        }
        return isAnySelected
    }


    private fun selectMinimumDestinationAmenities(it: AmenityData, amenityCount: Int) {
        for (amenity: SelectedAmenity in it.amenities) {
            if (amenity.selected) {
                continue
            }
            if (amenity.amenityAddressType == AmenityAddressType.DESTINATION) {
                if (it.destinationCount >= amenityCount) {
                    break
                }
                amenity.selected = true
                it.destinationCount++
            }
        }
    }

    private fun selectMinimumSourceAmenities(it: AmenityData, amenityCount: Int) {

        for (amenity: SelectedAmenity in it.amenities) {
            if (amenity.selected) {
                continue
            }
            if (amenity.amenityAddressType == AmenityAddressType.SOURCE) {
                if (it.sourceCount >= amenityCount) {
                    break
                }
                amenity.selected = true
                it.sourceCount++
            }
        }
    }

    private fun addAmenityToMap(
        it: BaseAmenity,
        amenities: Amenities,
        amenityAddressType: AmenityAddressType,
        amenityCount: Int
    ) {
        it.amenityType = amenities.type!!
        it.iconUrl = amenities.iconUrl
        addAmenityData(it, amenityAddressType, amenityCount)
    }


    /**
     * Creates a map that stores
     * 1) List of selected amenities (taking top 6 based on user preference) to show on map
     * 2) Distinct amenities based on brand (for petrol) or plug types (ev) or name (others except carpark)
     *
     * Distinct amenities also stores count and display name
     */
    private fun addAmenityData(
        it: BaseAmenity,
        amenityAddressType: AmenityAddressType,
        amenityCount: Int
    ) {
        var amenityData = amenityDataMap.get(it.amenityType)
        var amenitySubItemIdentifiers = ArrayList<AmenityIdentifier>()
        amenityData = initAmenityDataForType(amenityData, it.amenityType)
        val duplicate = addAmenityTypeIdentifiersAndSelection(
            amenityData,
            it,
            amenitySubItemIdentifiers,
            amenityAddressType,
            amenityCount
        )
        amenityData.distinctAmenities?.let { distinctAmenities ->
            addToAmenityMap(amenitySubItemIdentifiers, it, distinctAmenities, duplicate)
        }
    }

    private fun initAmenityDataForType(
        amenityData: AmenityData?,
        type: String
    ): AmenityData {
        var amenityData1 = amenityData
        if (amenityData1 == null) {
            if (type == CARPARK) {
                amenityData1 = AmenityData(ArrayList<SelectedAmenity>(), null, 0, 0)
            } else if (type == EVCHARGER) {
                amenityData1 = AmenityData(
                    ArrayList<SelectedAmenity>(),
                    LinkedHashMap<String, AmenityItemAdapter.AmenityCount>(),
                    0, 0
                )
                addDefaultEVChargers(amenityData1, type)
            } else if (type == PETROL) {
                amenityData1 = AmenityData(
                    ArrayList<SelectedAmenity>(),
                    LinkedHashMap<String, AmenityItemAdapter.AmenityCount>(),
                    0, 0
                )
                addDefaultPetrolStations(amenityData1, type)
            } else {
                amenityData1 = AmenityData(
                    ArrayList<SelectedAmenity>(),
                    LinkedHashMap<String, AmenityItemAdapter.AmenityCount>(),
                    0, 0
                )
            }
            amenityDataMap.put(type, amenityData1)
        }
        return amenityData1
    }

    private fun addDefaultPetrolStations(
        amenityData1: AmenityData?,
        type: String
    ) {
        PETROL_AMENITY_LIST.forEach { map ->
            val subItem =
                BreezeMapDataHolder.amenitiesPreferenceHashmap[PETROL]?.subItems?.find { subItem ->
                    if (subItem.elementName?.lowercase() == map.value?.lowercase()) {
                        return@find true
                    }
                    return@find false
                }
            amenityData1!!.distinctAmenities!!.put(
                map.value,
                AmenityItemAdapter.AmenityCount(
                    map.key,
                    map.value,
                    0,
                    type,
                    subItem?.isSelected ?: false
                )
            )
        }
    }

    private fun addDefaultEVChargers(
        amenityData1: AmenityData?,
        type: String
    ) {
        EV_AMENITY_LIST.forEach { map ->
            val subItem =
                BreezeMapDataHolder.amenitiesPreferenceHashmap[EVCHARGER]?.subItems?.find { subItem ->
                    if (subItem.elementName?.lowercase() == map.value?.lowercase()) {
                        return@find true
                    }

                    return@find false
                }
            amenityData1!!.distinctAmenities!!.put(
                map.value,
                AmenityItemAdapter.AmenityCount(
                    map.key,
                    map.value,
                    0,
                    type,
                    subItem?.isSelected ?: false
                )
            )
        }
    }

    /**
     * @return returns whether amenity is duplicate
     */
    private fun addAmenityTypeIdentifiersAndSelection(
        amenityData: AmenityData,
        it: BaseAmenity,
        identifiers: ArrayList<AmenityIdentifier>,
        amenityAddressType: AmenityAddressType,
        amenityCount: Int
    ): Boolean {

        val isDuplicate = checkIfDuplicateId(amenityData, it)
        when (it.amenityType) {
            PETROL -> {
                val subItem =
                    BreezeMapDataHolder.amenitiesPreferenceHashmap[PETROL]?.subItems?.find { subItem ->
                        if (subItem.elementName?.lowercase() == it.provider?.lowercase()) {
                            return@find true
                        }
                        return@find false
                    }
                it.provider?.let { provider ->
                    subItem?.displayText?.let { displayText ->
                        identifiers.add(
                            AmenityIdentifier(
                                displayText, provider,
                                subItem.isSelected ?: false
                            )
                        )
                    }
                }
                val isSelected =
                    selectAmenityBasedOnMaxCount(
                        subItem?.isSelected,
                        amenityAddressType,
                        amenityData,
                        amenityCount
                    )
                amenityData.amenities.add(SelectedAmenity(it, isSelected, amenityAddressType))
            }

            EVCHARGER -> {
                matchEVPlugType(it, identifiers)
                val matchedSubItem =
                    BreezeMapDataHolder.amenitiesPreferenceHashmap[EVCHARGER]?.subItems?.find { subItem ->
                        if (it.plugTypes != null) {
                            for (plugType: PlugTypes in it.plugTypes!!) {
                                if (subItem.elementName?.lowercase() == plugType.name?.lowercase()) {
                                    if (subItem.isSelected == true) {
                                        return@find true
                                    }
                                }
                            }
                        }
                        return@find false
                    }
                val isSelected =
                    selectAmenityBasedOnMaxCount(
                        matchedSubItem?.isSelected,
                        amenityAddressType,
                        amenityData,
                        amenityCount
                    )
                amenityData.amenities.add(SelectedAmenity(it, isSelected, amenityAddressType))
            }

            else -> {
                val item =
                    BreezeMapDataHolder.amenitiesPreferenceHashmap[it.amenityType]
                it.name?.let { name ->
                    identifiers.add(AmenityIdentifier(name, name, item?.isSelected ?: false))
                }
                val isSelected =
                    selectAmenityBasedOnMaxCount(
                        item?.isSelected,
                        amenityAddressType,
                        amenityData,
                        amenityCount
                    )
                amenityData.amenities.add(SelectedAmenity(it, isSelected, amenityAddressType))
            }
        }
        return (isDuplicate > -1)
    }

    private fun checkIfDuplicateId(
        amenityData: AmenityData,
        it: BaseAmenity
    ): Int {
        return amenityData.amenities.binarySearch { amenity ->
            if (amenity.baseAmenity.id == it.id) {
                return@binarySearch 0
            }
            if (amenity.baseAmenity.distance ?: 0 > it.distance ?: 0) {
                return@binarySearch 1
            }
            return@binarySearch -1
        }
    }

    private fun selectAmenityBasedOnMaxCount(
        selected: Boolean?,
        amenityAddressType: AmenityAddressType,
        amenityData: AmenityData,
        amenityCount: Int
    ): Boolean {
        var isSelected = selected ?: false
        if (isSelected) {
            if (amenityAddressType == AmenityAddressType.SOURCE) {
                if (amenityData.sourceCount >= amenityCount) {
                    isSelected = false
                } else {
                    amenityData.sourceCount++
                }
            } else {
                if (amenityData.destinationCount >= amenityCount) {
                    isSelected = false
                } else {
                    amenityData.destinationCount++
                }
            }
        }
        return isSelected
    }

    //FIXME: There can be multiple plug types
    private fun matchEVPlugType(
        it: BaseAmenity,
        identifiers: ArrayList<AmenityIdentifier>
    ) {
        BreezeMapDataHolder.amenitiesPreferenceHashmap[EVCHARGER]?.subItems?.let { subItems ->
            it.plugTypes?.forEach { plugType ->
                plugType.name?.let { name ->
                    val subItem = subItems.find {
                        if (it.elementName?.lowercase() == name.lowercase()) {
                            return@find true
                        }
                        return@find false
                    }
                    subItem?.displayText?.let { displayText ->
                        val existingIdentifier = identifiers.find { it.identifier == displayText }
                        if (existingIdentifier == null) {
                            identifiers.add(
                                AmenityIdentifier(
                                    displayText,
                                    name,
                                    subItem.isSelected ?: false
                                )
                            )
                        }
                    }
                }
            }
        }
    }

    private fun addToAmenityMap(
        amenityIdentifiers: List<AmenityIdentifier>?, it: BaseAmenity,
        amenityMap: HashMap<String, AmenityItemAdapter.AmenityCount>, duplicate: Boolean
    ) {
        amenityIdentifiers?.let { amenityIdentifiers ->
            amenityIdentifiers.forEach { amenityIdentifier ->
                if (amenityMap[amenityIdentifier.itemType] == null) {
                    amenityMap.put(
                        amenityIdentifier.itemType, AmenityItemAdapter.AmenityCount(
                            amenityIdentifier.identifier, amenityIdentifier.itemType,
                            1, it.amenityType, amenityIdentifier.selected
                        )
                    )
                } else {
                    if (!duplicate) {
                        amenityMap[amenityIdentifier.itemType]!!.count++
                    }
                }
            }
        }
    }


    fun sendETAMessage(etaRequest: ETARequest) {
        Timber.d("ETA Request: " + etaRequest.toString())
        mAPIHelper.sendETAMessage(etaRequest)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : ApiObserver<ETAResponse>(compositeDisposable) {
                override fun onSuccess(data: ETAResponse) {
                    etaRequest.tripEtaUUID = data.tripEtaUUID
                    etaAPISuccessful.value = etaRequest
                }

                override fun onError(e: ErrorData) {
                    Timber.d("API failed: " + e.toString())
                }

            })
    }

    fun saveUserSettings(mapDisplayPreference: String, routePreference: String) {

        val settingsRequest = SettingsResponse(arrayListOf())
        settingsRequest.settings.add(
            SettingsItem(
                Constants.SETTINGS.ATTR_MAP_DISPLAY,
                mapDisplayPreference,
                mapDisplayPreference
            )
        )
        settingsRequest.settings.add(
            SettingsItem(
                Constants.SETTINGS.ATTR_ROUTE_PREFERENCE,
                routePreference,
                routePreference
            )
        )

        mAPIHelper.setUserSettings(settingsRequest)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : ApiObserver<JsonElement>(
                compositeDisposable
            ) {
                override fun onSuccess(data: JsonElement) {
                    Timber.d("Retrieved user data is: " + data.toString())

                }

                override fun onError(e: ErrorData) {
                    Timber.i("AAA ${e.message} ${e.throwable}")
                }
            })

    }

    fun findRoutes(
        originPoint: Point,
        destination: Point,
        wayPoints: List<Point>,
        currentDestinationDetails: DestinationAddressDetails,
        departureDetails: RouteDepartureDetails?
    ) {
        /*viewModelScope.launch(Dispatchers.IO) {
            directionsAPIWithDrivingTrafficAndTolls(originPoint, destination)
        }*/
        Timber.d("Start api calls")

        CoroutineScope(Dispatchers.IO).launch {
            try {
                InitObjectUtilsController.mRoutePlanningHelper?.let { routePlaningHelper ->
                    val curRoutePreference: HashMap<RoutePreference, List<RouteAdapterObjects>> =
                        routePlaningHelper.findRoutes(
                            originPoint, destination,
                            currentDestinationDetails.address1,
                            wayPoints,
                            RoutePlanningHelper.RouteAlgorithm.MERGED_SINGLE_ROUTE_PER_TYPE,
                            departureDetails
                        );
                    updateMapRoutePreference(curRoutePreference)
                    routesRetrieved.postValue(mapOfRoutePreference);
                }
            } catch (e: IOException) {
                Timber.e(e, "Failed to fetch route details")
            }
        }
    }

    fun findWalkingRoutes(
        originPoint: Point?,
        destination: Point?,
        currentDestinationDetails: DestinationAddressDetails
    ) {
        CoroutineScope(Dispatchers.IO).launch {
            InitObjectUtilsController.mRoutePlanningHelper?.let { routePlaningHelper ->
                val walkingNavigationRoute: NavigationRoute? =
                    routePlaningHelper.findWalkingRoute(
                        originPoint, destination,
                        currentDestinationDetails.address1,
                    )
                walkingRoutesRetrieved.postValue(walkingNavigationRoute)
            }
        }
    }

    private suspend fun updateMapRoutePreference(curRoutePreference: HashMap<RoutePreference, List<RouteAdapterObjects>>) {
        mapOfRoutePreference.clear();
        compressedOriginalRouteData.clear()
        mapOfRoutePreference.putAll(curRoutePreference)
        curRoutePreference.map { routeAdapterObj ->
            routeAdapterObj.value.forEach {
                if (compressedOriginalRouteData[it.originalResponse] == null) {
                    compressedOriginalRouteData[it.originalResponse] =
                        it.originalResponse.compressToGzip()
                }
            }
        }
    }

    companion object {

        private const val CARPARK_COUNT = 10
        private const val AMENITY_COUNT = 3
        private const val RADIUS = 3.0
        private const val RADIUS_CARPARK_MAX = 3.0

        //Very high number so practically everything is returned
        private const val AMENITY_RESULT_COUNT = 1000
    }
}

data class CarParkProfileDetails(
    val profile: String?,
    val zoneType: ProfileZoneLayer.ProfileZoneType?,
    val congestionDetails: ArrayList<ResponseCongestionDetail>?,
    val voucherDetected: Boolean
)

data class AmenityIdentifier(val identifier: String, val itemType: String, val selected: Boolean)
data class AmenityData(
    val amenities: ArrayList<SelectedAmenity>,
    val distinctAmenities: LinkedHashMap<String, AmenityItemAdapter.AmenityCount>?,
    var sourceCount: Int,
    var destinationCount: Int
)
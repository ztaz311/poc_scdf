package com.ncs.breeze.components.map.profile

import androidx.appcompat.app.AlertDialog
import androidx.core.os.bundleOf
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.Event
import com.breeze.model.api.ErrorData
import com.ncs.breeze.common.remote.ApiObserver
import com.breeze.model.DestinationAddressDetails
import com.breeze.model.api.response.AmenitiesPreference
import com.breeze.model.api.response.ContentDetails
import com.breeze.model.api.response.DeleteAllBookMarkItemResponse
import com.breeze.model.api.response.amenities.BaseAmenity
import com.breeze.model.constants.AmenityType.CARPARK
import com.breeze.model.constants.AmenityType.POI
import com.ncs.breeze.common.utils.BreezeMapDataHolder
import com.ncs.breeze.common.utils.BreezeMapZoomUtil
import com.breeze.model.constants.Constants
import com.facebook.react.bridge.Arguments
import com.google.android.gms.maps.model.LatLng
import com.google.gson.JsonObject
import com.mapbox.geojson.Feature
import com.mapbox.geojson.Point
import com.ncs.breeze.App
import com.ncs.breeze.R
import com.ncs.breeze.components.layermanager.MarkerLayerManager
import com.ncs.breeze.components.marker.markerview2.AmenitiesMarkerView
import com.ncs.breeze.components.marker.markerview2.ExplorePoiMarkerView
import com.ncs.breeze.components.marker.markerview2.MarkerView2
import com.ncs.breeze.components.marker.markerview2.PoiMarkerView
import com.ncs.breeze.components.marker.markerview2.carpark.CarparkMarkerView
import com.ncs.breeze.common.extensions.mapbox.updateDestinationAndLocationPuckLayer
import com.ncs.breeze.helper.marker.DashBoardAmenityMarkerHelper
import com.ncs.breeze.ui.dashboard.fragments.exploremap.ContentDetailKey
import com.ncs.breeze.ui.dashboard.fragments.exploremap.ContentWrapperDataPOI
import com.ncs.breeze.ui.dashboard.manager.DashboardMapStateManager
import com.ncs.breeze.ui.dashboard.manager.ExploreMapStateManager
import com.ncs.breeze.common.analytics.Screen
import com.ncs.breeze.reactnative.nativemodule.ShareBusinessLogicEvent
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import timber.log.Timber

/**
 * Normal map profile that it's amenities can not be clustering when user zoom out
 * */
class StandardMapProfile(
    private val mapProfileOptions: MapProfileOptions
) : MapProfile {

    var markerTooltipView: MarkerView2? = null
    var isDestroyed = false
    var compositeDisposable: CompositeDisposable = CompositeDisposable()

    /**
     * for manager all marker of amenities
     */
    private val amenitiesMarkerViewManager: MarkerLayerManager = MarkerLayerManager(
        mapView = mapProfileOptions.mapView,
        nightMode = mapProfileOptions.nightModeEnabled
    )
    private val amenityMarkerViewHelper: DashBoardAmenityMarkerHelper =
        DashBoardAmenityMarkerHelper(
            amenitiesMarkerViewManager,
            mapProfileOptions.nightModeEnabled,
            true
        )

    init {
        amenitiesMarkerViewManager.markerLayerClickHandler =
            mapProfileOptions.markerLayerClickHandler
        CoroutineScope(Dispatchers.Main).launch {
            amenitiesMarkerViewManager.addStyleImages(CARPARK)
        }
    }

    override suspend fun loadAmenityStyleResources(amenityTypes: ArrayList<String>) {
        amenitiesMarkerViewManager.addStyleImages(amenityTypes)
    }

    override suspend fun loadImageStyleResources(listContentDetail: ArrayList<ContentDetails>) {
        amenitiesMarkerViewManager.addStyleImagesWithContentDetail(listContentDetail)
    }

    override fun handleAmenitiesData(
        allAmenities: HashMap<String, List<BaseAmenity>>,
        showDestination: Boolean,
        selectedProfile: String?,
        shouldShowAmenity: (AmenitiesPreference) -> Boolean,
    ) {
        val supportedAmenities = HashMap(allAmenities)
        supportedAmenities.remove(POI)
        supportedAmenities.remove(CARPARK)
        supportedAmenities.forEach {
            amenitiesMarkerViewManager.removeMarkerTooltipView(it.key)
            val preferenceAmenities = BreezeMapDataHolder.amenitiesPreferenceHashmap[it.key]
            preferenceAmenities?.let { amenityPref ->
                if (amenityPref.subItems != null) {
                    val listEnabled = getSelectedSubItems(amenityPref, it.value)
                    val isEnable = shouldShowAmenity(amenityPref)
                    amenityMarkerViewHelper.addAmenityMarkerView(
                        it.key,
                        listEnabled,
                        !isEnable,
                        showDestination
                    )
                } else {
                    val isEnable = shouldShowAmenity(amenityPref)
                    amenityMarkerViewHelper.addAmenityMarkerView(
                        it.key,
                        it.value,
                        !isEnable,
                        showDestination
                    )
                }
            }
        }
        if (supportedAmenities.size > 0) {
            mapProfileOptions.mapView.updateDestinationAndLocationPuckLayer()
        }
    }


    override fun handleContentDetailData(
        listAmenities: HashMap<ContentDetailKey, List<BaseAmenity>>,
        showDestination: Boolean,
        selectedProfile: String?,
        shouldShowAmenity: (AmenitiesPreference) -> Boolean
    ) {

        val supportedAmenities = HashMap(listAmenities)
        val listContentWrapperForSort = ArrayList<ContentWrapperDataPOI>()
        supportedAmenities.forEach {
            listContentWrapperForSort.add(ContentWrapperDataPOI(it.key, it.value))
        }
        listContentWrapperForSort.sortBy { it.contentDetailKey.tooltip_type }
        listContentWrapperForSort.forEach {
            amenitiesMarkerViewManager.removeMarkerTooltipView(it.contentDetailKey.code)
            val isEnable = true
            amenityMarkerViewHelper.addAmenityMarkerView(
                it.contentDetailKey.code,
                it.listAmenities,
                !isEnable,
                showDestination
            )
        }

        if (listContentWrapperForSort.size > 0) {
            mapProfileOptions.mapView.updateDestinationAndLocationPuckLayer()
        }
    }

    override fun handleCarPark(
        mListCarkPark: List<BaseAmenity>,
        isMoveCameraWrapperAllCarpark: Boolean,
        showDestination: Boolean,
        isMarkerHidden: Boolean,
        selectedProfile: String?
    ) {
        amenitiesMarkerViewManager.removeMarkerTooltipView(CARPARK)
        mapProfileOptions.carParkDestinationIconManager.handleDestinationCarParkIcon(
            mListCarkPark,
            showDestination
        )
        amenityMarkerViewHelper.addAmenityMarkerView(
            CARPARK,
            mListCarkPark,
            isMarkerHidden,
            showDestination
        )
        if (mListCarkPark.isNotEmpty()) {
            mapProfileOptions.mapView.updateDestinationAndLocationPuckLayer()
        }
    }

    override fun clearALlCarPark() {
        amenitiesMarkerViewManager.removeMarkers(CARPARK)
        mapProfileOptions.carParkDestinationIconManager.showDestinationIcon()
    }

    override fun clearALlMarkerAndTooltipAmenities() {
        mapProfileOptions.carParkDestinationIconManager.removeDestinationIconTooltip()
        amenitiesMarkerViewManager.removeMarkerTooltipView()
        amenitiesMarkerViewManager.removeAllMarkers()
    }

    override fun removeALlTooltip() {
        amenitiesMarkerViewManager.removeMarkerTooltipView()
    }

    override fun filterAllAmenitiesSelect(
        amenityType: String,
        isSelected: Boolean,
        amenityItems: List<BaseAmenity>,
        showDestination: Boolean
    ) {
        if (isSelected) {
            amenitiesMarkerViewManager.showAllMarker(amenityType)
        } else {
            amenitiesMarkerViewManager.hideAllMarker(amenityType)
        }
    }

    override fun showCarParkToolTip(
        amenity: BaseAmenity,
        showDestination: Boolean,
        showBookmark: Boolean,
        toolTipAction: ListenerCallbackLandingMap?,
        layerType: String,
        shouldRecenter: Boolean,
        startRoutePlanningFromMarker: (item: BaseAmenity, isCarPark: Boolean) -> Unit,
    ) {

        val carParkMarkerView = CarparkMarkerView(
            LatLng(amenity.lat!!, amenity.long!!),
            mapProfileOptions.mapView.getMapboxMap(),
            mapProfileOptions.activity,
            amenity.id,
            showBookmark
        )
        markerTooltipView = carParkMarkerView
        val properties = JsonObject()
        amenityMarkerViewHelper.handleSelectedCarParkProperties(
            amenity,
            properties,
            showDestination,
            false
        )
        val feature = Feature.fromGeometry(
            Point.fromLngLat(
                amenity.long!!,
                amenity.lat!!
            ), properties, amenity.id
        )

        amenitiesMarkerViewManager.renderMarkerToolTipView(
            MarkerLayerManager.MarkerViewType(
                carParkMarkerView,
                CARPARK,
                feature
            )
        )

        carParkMarkerView.clickToolTip(
            amenity,
            shouldRecenter,
            navigationButtonClick = { item, eventSource ->
                Analytics.logClickEvent(Event.NAVIGATE_HERE_PARKING, mapProfileOptions.screenName)
                Analytics.logLocationDataEvent(
                    Event.NAVIGATE_HERE_PARKING,
                    mapProfileOptions.screenName,
                    item.name.toString(),
                    item.lat.toString(),
                    item.long.toString()
                )
                startRoutePlanningFromMarker(item, true)
            },
            moreInfoButtonClick = { item ->
                Analytics.logClickEvent(Event.PARKING_SEE_MORE, mapProfileOptions.screenName)
                toolTipAction?.moreInfoButtonClick(item)
            },
            carParkIconClick = { isEnable ->
                toolTipAction?.onDismissedCameraTracking()
            },
            mapProfileOptions.screenName,
            bookmarkIconClick = { item, _ ->
                if (item.isBookmarked) {
                    item.id?.let { idAmenities ->
                        Analytics.logClickEvent(
                            Event.CAR_PARK_TOOLTIP_SAVE_OFF,
                            mapProfileOptions.screenName
                        )
                        showDeleteSavedPlaceConfirmation(idAmenities)
                    }
                } else {
                    Analytics.logClickEvent(
                        Event.CAR_PARK_TOOLTIP_SAVE_ON,
                        mapProfileOptions.screenName
                    )
                    sendEventToBookmarkItem(item)
                }
            })
        if (shouldRecenter) {
            mapProfileOptions.mapCamera?.let {
                BreezeMapZoomUtil.recenterMapToLocation(
                    mapProfileOptions.mapView.getMapboxMap(),
                    lat = amenity.lat!!.toDouble(),
                    lng = amenity.long!!.toDouble(),
                    edgeInset = it.overviewEdgeInsets,
                    zoomRadius = mapProfileOptions.toolTipRadius
                        ?: Constants.POI_TOOLTIP_ZOOM_RADIUS
                )
            }
        }
    }

    override fun handleAmenityToolTip(
        amenity: BaseAmenity,
        type: String,
        toolTipAction: ListenerCallbackLandingMap?,
        layerType: String,
        shouldRecenter: Boolean,
        showBookmark: Boolean,
        enableWalking: Boolean,
        enableZoneDrive: Boolean,
        startRoutePlanningFromMarker: (item: BaseAmenity, isCarPark: Boolean) -> Unit,
        startWalkingPathFromMarker: ((item: BaseAmenity, isCarPark: Boolean) -> Unit)?
    ) {
        val amenitiesMarkerView = AmenitiesMarkerView(
            LatLng(amenity.lat!!, amenity.long!!),
            mapProfileOptions.mapView.getMapboxMap(),
            mapProfileOptions.activity,
            amenity.id,
            amenity.amenityType,
            showBookmark,
            Constants.POI_TOOLTIP_ZOOM_RADIUS,
            enableWalking = enableWalking,
            enableZoneDrive = enableZoneDrive,
        )
        markerTooltipView = amenitiesMarkerView
        val properties = JsonObject()
        amenityMarkerViewHelper.handleSelectedAmenityProperties(properties, false, amenity)
        val amenityFeature = Feature.fromGeometry(
            Point.fromLngLat(
                amenity.long!!,
                amenity.lat!!
            ), properties, amenity.id
        )
        amenitiesMarkerViewManager.renderMarkerToolTipView(
            MarkerLayerManager.MarkerViewType(
                amenitiesMarkerView,
                type,
                amenityFeature
            )
        )

        amenitiesMarkerView.handleClickMarker(
            amenity,
            shouldRecenter,
            navigationButtonClick = { item, _ ->
                startRoutePlanningFromMarker(item, false)

                Analytics.logClickEvent(
                    "navigate_here_amenities_$type",
                    mapProfileOptions.screenName
                )
                Analytics.logLocationDataEvent(
                    "navigate_here_amenities_$type",
                    mapProfileOptions.screenName,
                    item.name.toString(),
                    item.lat.toString(),
                    item.long.toString()
                )
            },
            amenitiesIconClick = { item, _ ->
                Analytics.logClickEvent(
                    "map_" + type + "_icon_click",
                    mapProfileOptions.screenName
                )
                Analytics.logLocationDataEvent(
                    "map_" + type + "_icon_click",
                    mapProfileOptions.screenName,
                    item.name.toString(),
                    item.lat.toString(),
                    item.long.toString()
                )
                toolTipAction?.onDismissedCameraTracking()
            },
            bookmarkIconClick = { item, _ ->
                if (item.isBookmarked) {
                    item.id?.let { idAmenities ->
                        Analytics.logClickEvent(
                            Event.AMENITIES_TOOLTIP_SAVE_OFF,
                            mapProfileOptions.screenName
                        )
                        showDeleteSavedPlaceConfirmation(idAmenities)
                    }
                } else {
                    Analytics.logClickEvent(
                        Event.AMENITIES_TOOLTIP_SAVE_ON,
                        mapProfileOptions.screenName
                    )
                    sendEventToBookmarkItem(item)
                }
            },
            walkingIconClick = { item, _ ->
                startWalkingPathFromMarker?.let { it(item, false) }

                Analytics.logClickEvent(
                    "walk_here_amenities_" + item.amenityType,
                    mapProfileOptions.screenName
                )
                Analytics.logLocationDataEvent(
                    "walk_here_amenities_" + item.amenityType,
                    mapProfileOptions.screenName,
                    item.name.toString(),
                    item.lat.toString(),
                    item.long.toString()
                )
            }
        )
    }

    override fun handlePoiToolTip(
        amenity: BaseAmenity,
        featureType: String,
        toolTipAction: ListenerCallbackLandingMap?,
        layerType: String,
        shouldRecenter: Boolean,
        startRoutePlanningFromMarker: (item: BaseAmenity, isCarPark: Boolean) -> Unit,
        startWalkingPathFromMarker: (item: BaseAmenity, isCarPark: Boolean) -> Unit
    ) {

        val amenitiesMarkerView = PoiMarkerView(
            LatLng(amenity.lat!!, amenity.long!!),
            mapProfileOptions.mapView.getMapboxMap(),
            mapProfileOptions.activity,
            amenity.id,
            featureType
        )
        val properties = JsonObject()
        amenityMarkerViewHelper.handleSelectedAmenityProperties(properties, false, amenity)
        val amenityFeature = Feature.fromGeometry(
            Point.fromLngLat(
                amenity.long!!,
                amenity.lat!!
            ), properties, amenity.id
        )
        amenitiesMarkerViewManager.renderMarkerToolTipView(
            MarkerLayerManager.MarkerViewType(
                amenitiesMarkerView,
                layerType,
                amenityFeature
            )
        )

        amenitiesMarkerView.handleClickMarker(
            amenity,
            shouldRecenter,
            walkHereButtonClick = { item, _ ->
                startWalkingPathFromMarker(item, false)

                Analytics.logClickEvent(
                    "walk_here_amenities_" + item.amenityType,
                    mapProfileOptions.screenName
                )
                Analytics.logLocationDataEvent(
                    "walk_here_amenities_" + item.amenityType,
                    mapProfileOptions.screenName,
                    item.name.toString(),
                    item.lat.toString(),
                    item.long.toString()
                )
            },
            driveHereButtonClick = { item, _ ->
                startRoutePlanningFromMarker(item, false)

                Analytics.logClickEvent(
                    "navigate_here_amenities_" + item.amenityType,
                    mapProfileOptions.screenName
                )
                Analytics.logLocationDataEvent(
                    "navigate_here_amenities_" + item.amenityType,
                    mapProfileOptions.screenName,
                    item.name.toString(),
                    item.lat.toString(),
                    item.long.toString()
                )
            },
            amenitiesIconClick = { item, _ ->
                Analytics.logClickEvent(
                    "map_" + item.amenityType + "_icon_click",
                    mapProfileOptions.screenName
                )
                Analytics.logLocationDataEvent(
                    "map_" + item.amenityType + "_icon_click",
                    mapProfileOptions.screenName,
                    item.name.toString(),
                    item.lat.toString(),
                    item.long.toString()
                )
                toolTipAction?.onDismissedCameraTracking()
            })
    }

    override fun handlePoiToolTipMapExplore(
        amenity: BaseAmenity,
        featureType: String,
        toolTipAction: ListenerCallbackLandingMap?,
        layerType: String,
        shouldRecenter: Boolean,
        enableWalking: Boolean,
        enableZoneDrive: Boolean,
        startRoutePlanningFromMarker: (item: BaseAmenity, isCarPark: Boolean) -> Unit,
        startWalkingPathFromMarker: (item: BaseAmenity, isCarPark: Boolean) -> Unit
    ) {
        val explorePoiMarkerView = ExplorePoiMarkerView(
            latLng = LatLng(amenity.lat!!, amenity.long!!),
            mapboxMap = mapProfileOptions.mapView.getMapboxMap(),
            activity = mapProfileOptions.activity,
            itemId = amenity.id,
            itemType = featureType,
            enableWalking = enableWalking,
            enableZoneDrive = enableZoneDrive
        )
        val properties = JsonObject()
        amenityMarkerViewHelper.handleSelectedAmenityProperties(properties, false, amenity)
        val amenityFeature = Feature.fromGeometry(
            Point.fromLngLat(
                amenity.long!!,
                amenity.lat!!
            ), properties, amenity.id
        )
        amenitiesMarkerViewManager.renderMarkerToolTipView(
            MarkerLayerManager.MarkerViewType(
                explorePoiMarkerView,
                layerType,
                amenityFeature
            )
        )

        explorePoiMarkerView.handleClickMarker(
            amenity,
            shouldRecenter,
            walkHereButtonClick = { item, _ ->
                startWalkingPathFromMarker(item, false)

                Analytics.logClickEvent(
                    "walk_here_amenities_" + item.name,
                    mapProfileOptions.screenName
                )
                Analytics.logLocationDataEvent(
                    "walk_here_amenities_" + item.name,
                    mapProfileOptions.screenName,
                    item.name.toString(),
                    item.lat.toString(),
                    item.long.toString()
                )
            },
            driveHereButtonClick = { item, _ ->
                startRoutePlanningFromMarker(item, false)

                Analytics.logClickEvent(
                    "navigate_here_amenities_" + item.name,
                    mapProfileOptions.screenName
                )
                Analytics.logLocationDataEvent(
                    "navigate_here_amenities_" + item.name,
                    mapProfileOptions.screenName,
                    item.name.toString(),
                    item.lat.toString(),
                    item.long.toString()
                )
            },
            amenitiesIconClick = { item, _ ->
                Analytics.logClickEvent(
                    "map_" + ExploreMapStateManager.contentDetailsResponse!!.name + "_icon_click",
                    mapProfileOptions.screenName
                )
                Analytics.logLocationDataEvent(
                    "map_" + item.name + "_icon_click",
                    mapProfileOptions.screenName,
                    item.name.toString(),
                    item.lat.toString(),
                    item.long.toString()
                )
                toolTipAction?.onDismissedCameraTracking()
            })
    }

    private fun showDeleteSavedPlaceConfirmation(amenitiesID: String) {
        Analytics.logOpenPopupEvent(Event.POPUP_UNSAVE_COLLECTION, mapProfileOptions.screenName)
        val builder: AlertDialog.Builder = mapProfileOptions.activity.let {
            AlertDialog.Builder(it)
        }
        val message: String =
            mapProfileOptions.activity.getString(R.string.desc_delete_saved_place_confirmation_2)
        builder
            .setMessage(message)
            .setNegativeButton(
                R.string.cancel_uppercase
            ) { dialog, _ ->
                Analytics.logClickEvent(Event.DELETE_SAVE_CANCEL, mapProfileOptions.screenName)
                Analytics.logClosePopupEvent(
                    Event.POPUP_UNSAVE_COLLECTION,
                    mapProfileOptions.screenName
                )
                dialog.dismiss()
            }
            .setPositiveButton(
                R.string.confirm
            ) { dialog, _ ->
                Analytics.logClickEvent(Event.DELETE_SAVE_CONFIRM, mapProfileOptions.screenName)
                Analytics.logClosePopupEvent(
                    Event.POPUP_UNSAVE_COLLECTION,
                    mapProfileOptions.screenName
                )
                dialog.dismiss()
                deselectingBookmarkTooltip(amenitiesID)
            }
        val alertDialog: AlertDialog = builder.create()
        alertDialog.show()
    }

    /**
     * call api to deselecting bookmark tooltip
     */
    private fun deselectingBookmarkTooltip(amenitiesID: String) {
        App.instance?.let { app ->
            app.apiHelper.deselectingBookmarkTooltip(amenitiesID)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object :
                    ApiObserver<DeleteAllBookMarkItemResponse>(compositeDisposable) {
                    override fun onSuccess(data: DeleteAllBookMarkItemResponse) {
                        refreshBookmarkIconTooltip(false)
                    }

                    override fun onError(e: ErrorData) {
                        Timber.i("AAA ${e}")

                    }
                })
        }
    }

    /**
     * send event to save the item
     */
    private fun sendEventToBookmarkItem(amenity: BaseAmenity?) {
        App.instance?.let { app ->
            var searchedLocation: DestinationAddressDetails? = null
            if (mapProfileOptions.screenName == Screen.HOME && DashboardMapStateManager.isInSearchLocationMode && amenity?.destinationCarPark == true) {
                searchedLocation = DashboardMapStateManager.searchLocationAddress
            }
            val rnData = if (searchedLocation != null) {
                Arguments.fromBundle(
                    bundleOf(
                        "lat" to searchedLocation.lat?.toDoubleOrNull(),
                        "long" to searchedLocation.long?.toDoubleOrNull(),
                        "name" to searchedLocation.address1,
                        "address1" to searchedLocation.address1,
                        "address2" to searchedLocation.address2,
                        "amenityType" to "",
                        "amenityId" to "",
                        "is_selected" to (amenity?.isBookmarked == true),
                        "isDestination" to true,
                        "showAvailabilityFB" to searchedLocation.showAvailabilityFB,
                        "placeId" to searchedLocation.placeId,
                        "userLocationLinkIdRef" to searchedLocation.userLocationLinkIdRef,
                    )
                )
            } else if (amenity != null) {
                Arguments.fromBundle(
                    bundleOf(
                        "lat" to amenity.lat!!,
                        "long" to amenity.long!!,
                        "name" to amenity.name,
                        "address1" to if (amenity.amenityType == CARPARK) amenity.name else amenity.address,
                        "address2" to "",
                        "amenityType" to amenity.amenityType,
                        "amenityId" to amenity.id,
                        "is_selected" to amenity.isBookmarked,
                        "showAvailabilityFB" to amenity.showAvailabilityFB,
                    )
                )
            } else Arguments.createMap()

            app.shareBusinessLogicPackage.getRequestsModule().flatMap {
                Timber.i("call RN event [UPDATE_SAVED_LOCATION] $rnData")
                it.callRN(ShareBusinessLogicEvent.UPDATE_SAVED_LOCATION.eventName, rnData)
            }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe()
        }
    }

    override fun hideAllCarPark(amenityItems: List<BaseAmenity>, showDestination: Boolean) {
        amenitiesMarkerViewManager.removeMarkerTooltipView()
        amenitiesMarkerViewManager.hideAllMarker(CARPARK)
        mapProfileOptions.carParkDestinationIconManager?.showDestinationIcon()
    }

    override fun hideAmenities() {
        amenitiesMarkerViewManager.hideAllMarkerExcludingType(CARPARK)
    }

    override fun showAllCarPark(
        mListCarPark: List<BaseAmenity>?,
        isMoveCameraWrapperAllCarpark: Boolean,
        showDestination: Boolean,
        isMarkerHidden: Boolean,
        selectedProfile: String?
    ) {
        amenitiesMarkerViewManager.showAllMarker(CARPARK)
        mapProfileOptions.carParkDestinationIconManager.handleDestinationCarParkIcon(
            mListCarPark ?: listOf(),
            showDestination
        )
    }

    override fun hideAllAmenitiesAndCarParks() {
        amenitiesMarkerViewManager.let {
            it.removeMarkerTooltipView()
            it.hideAllMarker()
        }
        mapProfileOptions.carParkDestinationIconManager?.showDestinationIcon()
    }

    override fun showAllAmenities(
        mListCarPark: List<BaseAmenity>?,
        originalAmenitiesList: HashMap<String, List<BaseAmenity>>,
        showDestination: Boolean,
        selectedProfile: String?,
        shouldShowAmenity: (AmenitiesPreference) -> Boolean,
    ) {
        showSelectedAmenities()
        mapProfileOptions.carParkDestinationIconManager?.handleDestinationCarParkIcon(
            mListCarPark ?: listOf(),
            showDestination
        )
    }

    override fun isAmenitiesEmpty(): Boolean {
        return !(amenitiesMarkerViewManager?.hasFeatureLayerItemsExceptType(CARPARK) ?: false)
    }

    override fun isCarParkEmpty(): Boolean {
        return !(amenitiesMarkerViewManager?.hasFeatureLayerItems(CARPARK) ?: false)
    }

    override fun destroy() {
        amenitiesMarkerViewManager.onDestroy()
        isDestroyed = true
    }


    private fun showSelectedAmenities() {
        BreezeMapDataHolder.amenitiesPreferenceHashmap.forEach {
            if (it.value.isSelected == true) {
                amenitiesMarkerViewManager.showAllMarker(it.key)
            }
        }
    }


    private fun getSelectedSubItems(
        amenityPref: AmenitiesPreference,
        it: List<BaseAmenity>
    ): ArrayList<BaseAmenity> {
        val listEnabled = ArrayList<BaseAmenity>(it)
        amenityPref.subItems!!.forEach { subItem ->
            if (subItem.isSelected != true) {
                listEnabled.removeAll(it.filter { amenity ->
                    if (!amenity.provider.isNullOrBlank() && subItem.elementName?.lowercase() == amenity.provider!!.lowercase()) {
                        return@filter true
                    } else if (!amenity.name.isNullOrBlank() && subItem.elementName?.lowercase() == amenity.name!!.lowercase()) {
                        return@filter true
                    }
                    return@filter false
                }.toSet())
            }
        }
        if (listEnabled.isEmpty()) {
            listEnabled.addAll(it)
        }
        return listEnabled
    }

    override fun refreshBookmarkIconTooltip(saved: Boolean, bookmarkId: Int) {
        if (markerTooltipView != null) {
            markerTooltipView?.updateBookMarkIconState(saved, bookmarkId)
        }
    }
}
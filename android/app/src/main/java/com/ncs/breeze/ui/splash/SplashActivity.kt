package com.ncs.breeze.ui.splash

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.ViewModelProvider
import com.breeze.model.constants.Constants
import com.breeze.model.constants.INTENT_KEY
import com.breeze.model.enums.DebugMode
import com.mapbox.navigation.core.lifecycle.MapboxNavigationApp
import com.mapbox.navigation.core.trip.session.TripSessionState
import com.ncs.breeze.App
import com.ncs.breeze.BuildConfig
import com.ncs.breeze.R
import com.ncs.breeze.common.event.ToCarRx
import com.ncs.breeze.common.extensions.android.getApp
import com.ncs.breeze.common.extensions.android.getAppPreference
import com.ncs.breeze.common.extensions.android.isConnected
import com.ncs.breeze.common.model.rx.AppEventLogout
import com.ncs.breeze.common.model.rx.AppSessionReadyData
import com.ncs.breeze.common.model.rx.AppSessionReadyEvent
import com.ncs.breeze.common.storage.AppPrefsKey
import com.ncs.breeze.common.utils.BreezeFileLoggingUtil
import com.ncs.breeze.common.utils.BreezeHistoryRecorderUtil
import com.ncs.breeze.common.utils.BreezeUserPreferenceUtil
import com.ncs.breeze.common.utils.Utils
import com.ncs.breeze.databinding.ActivitySplashBinding
import com.ncs.breeze.reactnative.nativemodule.ShareBusinessLogicEvent
import com.ncs.breeze.ui.base.BaseActivity
import com.ncs.breeze.ui.base.BaseViewModel
import com.ncs.breeze.ui.base.RemoteConfigParam
import com.ncs.breeze.ui.base.RemoteConfigUtils
import com.ncs.breeze.ui.dashboard.activity.DashboardActivity
import com.ncs.breeze.ui.login.activity.LoginActivity
import com.ncs.breeze.ui.login.fragment.guest.view.GuestModeSignUpFragment
import com.ncs.breeze.ui.utils.DialogUtilsBreeze
import com.ncs.breeze.ui.utils.RootUtils
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject


class SplashActivity : BaseActivity<ActivitySplashBinding, SplashViewModel>() {

    @Inject
    lateinit var breezeHistoryRecorderUtil: BreezeHistoryRecorderUtil


    @Inject
    lateinit var breezeUserPreferenceUtil: BreezeUserPreferenceUtil

    @Inject
    lateinit var breezeFileLoggingUtil: BreezeFileLoggingUtil

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val splashViewModel: SplashViewModel by viewModels {
        viewModelFactory
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Timber.d("Splash onCreate() is called")
        if (intent.extras?.getBoolean(Constants.MAINTAINENCE_MODE_ENABLED) == true) {
            val maintenanceMessage = intent.extras?.getString(Constants.MAINTAINENCE_MODE_MESSAGE)
            showAppIsInMaintenanceMessage(maintenanceMessage!!)
        }
        if (!isTaskRoot
            && intent.hasCategory(Intent.CATEGORY_LAUNCHER)
            && intent.action != null
            && intent.action.equals(Intent.ACTION_MAIN)
        ) {
            finish()
            return
        }
    }

    private fun showAppIsInMaintenanceMessage(maintenanceMessage: String) {
        viewBinding.txtMessage.visibility = View.VISIBLE
        viewBinding.txtMessage.text = maintenanceMessage
    }

    override fun onResume() {
        super.onResume()
        RemoteConfigUtils.updateCheckDurationHrs = null
        if (!BuildConfig.DEBUG && RootUtils.isDeviceRooted) {
            Toast.makeText(baseContext, "Cannot run app on rooted device.", Toast.LENGTH_SHORT)
                .show()
            finish()
        }
        if (intent.extras?.getBoolean(Constants.MAINTAINENCE_MODE_ENABLED) == true)
            return
        handleAutoLogin()
    }

    private fun checkForUpdatesAndNavigate(handleSkip: () -> Unit) {
        MapboxNavigationApp.current()
            ?.takeIf { mapboxNavigation ->
                (!Utils.isNavigationStarted || Utils.phoneNavigationStartEventData == null)
                        && mapboxNavigation.getTripSessionState() != TripSessionState.STARTED
            }?.let {
                validateAndCheckConfigForUpdates(object : UpdateEventHandler {
                    override fun handleMaintenance(data: RemoteConfigParam) {
                        showAppIsInMaintenanceMessage(data.maintenanceMessage)
                    }

                    override fun handleSkip() {
                        handleSkip()
                    }

                    override fun handleExit() {
                        finish()
                    }

                    override fun handleAppUpToDate() {
                        showAppUptoDateDialog()
                    }
                })
                return
            }
        handleSkip()
    }


    /**
     * check session & exe autologin
     */
    fun handleAutoLogin() {
        if (this.isConnected) {
            splashViewModel.userLoggedIn.observe(this) { userStatus ->
                splashViewModel.userLoggedIn.removeObservers(this)
                when (userStatus) {
                    BaseViewModel.USER_STATUS.USER_LOGGED_IN -> {
                        if (!Utils.checkLocationPermission(this)) {
                            replaceActivity(Intent(this, PermissionActivity::class.java))
                        } else if (!Utils.checkGPSEnabled(this)) {
                            showGPSTurnedOffBottomSheet {
                                uploadPendingHistoryData()
                                startHandleNextActionAfterAutoLogin()
                            }
                        } else {
                            uploadPendingHistoryData()
                            startHandleNextActionAfterAutoLogin()
                        }
                    }

                    BaseViewModel.USER_STATUS.USER_NO_USERNAME_ATTRIBUTE -> {
                        checkForUpdatesAndNavigate {
                            val intent = Intent(this, LoginActivity::class.java)
                            replaceActivity(intent)
                        }
                    }

                    else -> {
                        checkForUpdatesAndNavigate {
                            /**
                             * check if guest mode => do other thing
                             */
                            openLoginActivity()
                        }
                    }
                }
            }
            splashViewModel.fetchToken()
        } else {
            showNoInternetDialog(SplashActivity@ this)
        }
    }

    /**
     * open login activity
     */
    fun openLoginActivity() {
        replaceActivity(Intent(this, LoginActivity::class.java))
    }

    /**
     * after auto login sucess need to call api get setting & term ( api term to get some information)
     */
    private fun startHandleNextActionAfterAutoLogin() {
        if (getAppPreference()?.getBoolean(AppPrefsKey.CLEAR_FORCE_SESSION_NEXT_LAUNCH) == true) {
            logoutApp {}
            replaceActivity(Intent(this, LoginActivity::class.java))
            return
        }

        /**
         * get all setting config & user setting
         */
        getAllSettingInSystem()
    }


    /**
     * show warning creat guest account
     */
    private fun showFragmentWarningCreateGuestAccount() {
        /**
         *  we only need logic in side guest logic
         */
        val bundle = Bundle().apply {
            putBoolean(INTENT_KEY.AUTO_SIGN_IN_GEUST_MODE, true)
        }
        val fragmentSignUpGuest = GuestModeSignUpFragment.newInstance()
        fragmentSignUpGuest.arguments = bundle
        supportFragmentManager
            .beginTransaction()
            .addToBackStack(GuestModeSignUpFragment.SCREEN_NAME)
            .replace(
                viewBinding.frameContainer.id,
                fragmentSignUpGuest,
                GuestModeSignUpFragment.SCREEN_NAME
            )
            .commit()
    }

    private fun uploadPendingHistoryData() {
        getApp()?.navigationLogUploaderScheduler?.uploadPendingHistoryData()
    }

    private fun showNoInternetDialog(activity: Activity) {
        val builder: AlertDialog.Builder = activity.let {
            AlertDialog.Builder(it)
        }
        builder.setTitle(R.string.no_internet_title)
            .setMessage(R.string.no_internet_desc)
            .setPositiveButton(
                R.string.no_internet_button
            ) { dialog, _ ->
                this.finishAffinity();
                dialog.dismiss()
            }

        val alertDialog: AlertDialog = builder.create()
        alertDialog.show()
        Timber.d("Popup shown in activity" + activity.localClassName)
    }


    /**
     * get all setting & user setting
     */
    private fun getAllSettingInSystem() {
        splashViewModel.hasGotAllUserSettingData.observe(this) { isAllSettingFetched ->
            if (isAllSettingFetched) {
                checkAcceptedTNC {
                    openDashBoardScreen()
                }
            } else {
                DialogUtilsBreeze.showDialogGetUserFails(SplashActivity@ this, retryAction = {
                    getAllSettingInSystem()
                }, cancel = {
                    finish()
                })
            }
        }
        splashViewModel.getAllSettingZipData()
    }


    /**
     * In API getUserData have some information => should call save data to local
     * + no need check term on this screen
     */
    private fun checkAcceptedTNC(onTNCAccepted: () -> Unit) {
        splashViewModel.tncAccepted.observe(this) {
            splashViewModel.tncAccepted.removeObservers(this)
            onTNCAccepted.invoke()
        }
        splashViewModel.getUserData { userData ->
            val debugMode = getAppPreference()?.getInt(AppPrefsKey.DEBUG_MODE, if (BuildConfig.DEBUG) 1 else 0)
                .let { savedValue ->
                    DebugMode.values().find { it.value == savedValue }
                } ?: DebugMode.OFF

            if (userData.isLogEnabled) {
                if (debugMode == DebugMode.OFF)
                    getAppPreference()?.saveInt(
                        AppPrefsKey.DEBUG_MODE,
                        DebugMode.BE_ENABLED.value,
                        true
                    )
                getApp()?.setupDebugLog()
            } else {
                if (!BuildConfig.DEBUG && debugMode == DebugMode.BE_ENABLED)
                    breezeFileLoggingUtil.turnOffLog()
            }
        }
    }

    fun declineTnc() {
        logoutApp {
            /**
             * post event for logout
             */
            ToCarRx.postEventWithCacheLast(AppEventLogout())
            restartApp()
        }
    }


    /**
     * Resetting Amenity Selection (temp) as per Lee Fong's request
     */
    private fun resetAmenitySelections() {
        (applicationContext as? App)?.run {
            shareBusinessLogicPackage.getRequestsModule().flatMap {
                it.callRN(ShareBusinessLogicEvent.RESET_AMENITY_SELECTIONS.eventName, null)
            }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { result ->
                        splashViewModel.amenityResetResponse.postValue(true)
                    },
                    { error ->
                        splashViewModel.amenityResetResponse.postValue(false)
                    }
                )
        }
    }

    override fun inflateViewBinding() = ActivitySplashBinding.inflate(layoutInflater)

    override fun getViewModelReference(): SplashViewModel {
        return splashViewModel
    }

    override fun getFragmentContainer(): Int {
        return R.id.frame_container
    }

    override fun onDestroy() {
        super.onDestroy()
        Timber.d("Splash onDestroy() is called")
    }


    /**
     * open dash board screen
     */
    private fun openDashBoardScreen() {
        if (Constants.RESET_AMENITY_SELECTION_ON_LAUNCH) {
            splashViewModel.amenityResetResponse.observe(this) {
                splashViewModel.amenityResetResponse.removeObservers(this)
                replaceActivity(Intent(this, DashboardActivity::class.java))
            }
            resetAmenitySelections()
        } else {
            replaceActivity(Intent(this, DashboardActivity::class.java))
        }

        /**
         * start event to open home android auto
         */
        ToCarRx.postEventWithCacheLast(AppSessionReadyEvent(AppSessionReadyData(true)))
    }

}
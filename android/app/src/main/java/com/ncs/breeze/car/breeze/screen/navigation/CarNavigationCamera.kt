package com.ncs.breeze.car.breeze.screen.navigation

import android.graphics.Rect
import android.location.Location
import androidx.core.os.bundleOf
import com.breeze.model.DestinationAddressDetails
import com.breeze.model.api.ErrorData
import com.breeze.model.api.request.AmenitiesRequest
import com.breeze.model.api.response.AvailabilityAlertResponse
import com.breeze.model.api.response.amenities.BaseAmenity
import com.breeze.model.api.response.amenities.ResponseAmenities
import com.breeze.model.constants.AmenityType
import com.breeze.model.constants.Constants
import com.breeze.model.enums.CarParkViewOption
import com.breeze.model.extensions.dpToPx
import com.mapbox.androidauto.internal.logAndroidAuto
import com.mapbox.geojson.GeometryCollection
import com.mapbox.geojson.Point
import com.mapbox.maps.EdgeInsets
import com.mapbox.maps.extension.androidauto.MapboxCarMapObserver
import com.mapbox.maps.extension.androidauto.MapboxCarMapSurface
import com.mapbox.maps.plugin.animation.camera
import com.mapbox.maps.plugin.animation.easeTo
import com.mapbox.maps.plugin.locationcomponent.location
import com.mapbox.navigation.base.formatter.DistanceFormatterOptions
import com.mapbox.navigation.base.formatter.UnitType
import com.mapbox.navigation.core.directions.session.RoutesObserver
import com.mapbox.navigation.core.lifecycle.MapboxNavigationApp
import com.mapbox.navigation.core.trip.session.LocationMatcherResult
import com.mapbox.navigation.core.trip.session.LocationObserver
import com.mapbox.navigation.core.trip.session.RouteProgressObserver
import com.mapbox.navigation.ui.maps.camera.NavigationCamera
import com.mapbox.navigation.ui.maps.camera.data.MapboxNavigationViewportDataSource
import com.mapbox.navigation.ui.maps.camera.state.NavigationCameraState
import com.mapbox.navigation.ui.maps.camera.transition.NavigationCameraTransitionOptions
import com.mapbox.navigation.ui.maps.location.NavigationLocationProvider
import com.mapbox.navigation.ui.speedlimit.api.MapboxSpeedInfoApi
import com.ncs.breeze.car.breeze.MainBreezeCarContext
import com.ncs.breeze.car.breeze.model.CarParkLayerModel
import com.ncs.breeze.car.breeze.utils.BreezeCarUtil
import com.ncs.breeze.car.breeze.utils.BreezeMapBoxUtil
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.Event
import com.ncs.breeze.common.analytics.Param
import com.ncs.breeze.common.analytics.Screen
import com.ncs.breeze.common.remote.ApiObserver
import com.ncs.breeze.common.utils.Utils
import com.ncs.breeze.common.utils.VoiceInstructionsManager
import com.ncs.breeze.ui.dashboard.fragments.obulite.OBUCheckSpeedHelper
import com.ncs.breeze.ui.dashboard.fragments.obulite.isStationary
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber


/**
 * Integrates the Android Auto [MapboxCarMapSurface] with the [NavigationCamera].
 */
class CarNavigationCamera(
    private val mainCarContext: MainBreezeCarContext,
    val navigationContext: CarNavigationCarContext,
    val cameraMode: CameraMode
) : MapboxCarMapObserver {
    private var distanceRemainingInMetre: Float = -1f
    private var shouldCheckAvailabilityAlertVoice = true

    var compositeDisposable: CompositeDisposable = CompositeDisposable()

    private var mapboxCarMapSurface: MapboxCarMapSurface? = null
    private var navigationCamera: NavigationCamera? = null
    private var viewportDataSource: MapboxNavigationViewportDataSource? = null
    private var overviewEdgeInsets: EdgeInsets? = null
    private var followingEdgeInsets: EdgeInsets? = null
    private var visibleArea: EdgeInsets =
        EdgeInsets(30.0.dpToPx(), 30.0.dpToPx(), 30.0.dpToPx(), 30.0.dpToPx())

    private var currentLocation: Location? = null
    private val carParkFeatureDataList = ArrayList<CarParkLayerModel>();
    private var obuCheckSpeedHelper: OBUCheckSpeedHelper? = null
    private val speedLimitApi: MapboxSpeedInfoApi by lazy { MapboxSpeedInfoApi() }

    private var isLocationInitialized = false
    private var currentCarparkList: List<BaseAmenity>? = null

    private var navigationLocationProvider = NavigationLocationProvider()

    override fun onAttached(mapboxCarMapSurface: MapboxCarMapSurface) {
        super.onAttached(mapboxCarMapSurface)
        this.mapboxCarMapSurface = mapboxCarMapSurface
        logAndroidAuto("CarNavigationCamera loaded $mapboxCarMapSurface")

        val mapboxMap = mapboxCarMapSurface.mapSurface.getMapboxMap()
//        initialCameraOptions?.let { mapboxMap.setCamera(it) }
        viewportDataSource = MapboxNavigationViewportDataSource(
            mapboxCarMapSurface.mapSurface.getMapboxMap()
        ).apply {
            options.followingFrameOptions.minZoom = 16.5
            options.followingFrameOptions.maxZoom = 16.5

            overviewEdgeInsets?.let { overviewPadding = it }
            followingEdgeInsets?.let { followingPadding = it }
            overviewPitchPropertyOverride(0.0)
            followingPitchPropertyOverride(50.0)
        }

        navigationCamera = NavigationCamera(
            mapboxMap,
            mapboxCarMapSurface.mapSurface.camera,
            viewportDataSource!!
        )

        mapboxCarMapSurface.mapSurface.location.apply {
            mainCarContext.defaultLocationProvider = getLocationProvider()
            setLocationProvider(navigationLocationProvider)
        }

        if (obuCheckSpeedHelper == null) {
            obuCheckSpeedHelper = OBUCheckSpeedHelper()
            obuCheckSpeedHelper?.setStateChangeCallback(::onSpeedStateChanged)
        }

        MapboxNavigationApp.current()?.let { mapboxNavigation ->
            mapboxNavigation.registerLocationObserver(locationObserver)
            mapboxNavigation.registerRoutesObserver(routeObserver)
            mapboxNavigation.registerRouteProgressObserver(routeProgressObserver)
        }
    }

    override fun onVisibleAreaChanged(visibleArea: Rect, edgeInsets: EdgeInsets) {
        super.onVisibleAreaChanged(visibleArea, edgeInsets)
        logAndroidAuto("CarNavigationCamera visibleAreaChanged $visibleArea $edgeInsets")

        viewportDataSource?.overviewPadding = EdgeInsets(
            edgeInsets.top,
            edgeInsets.left,
            edgeInsets.bottom,
            edgeInsets.right
        ).also { overviewEdgeInsets = it }

        val visibleHeight = visibleArea.bottom - visibleArea.top
        val followingBottomPadding = visibleHeight * BOTTOM_FOLLOWING_PERCENTAGE
        viewportDataSource?.followingPadding = EdgeInsets(
            edgeInsets.top,
            edgeInsets.left,
            edgeInsets.bottom + followingBottomPadding,
            edgeInsets.right
        ).also { followingEdgeInsets = it }

        this.visibleArea = EdgeInsets(
            edgeInsets.top + 15.dpToPx(),
            edgeInsets.left + 10.dpToPx(),
            edgeInsets.bottom + 30.dpToPx(),
            edgeInsets.right + 40.dpToPx()
        ).also {
            if (obuCheckSpeedHelper.isStationary()) {
                currentCarparkList?.let { carparkList ->
                    updateCamera(carparkList)
                }
            }
        }
        viewportDataSource?.evaluate()
    }

    override fun onDetached(mapboxCarMapSurface: MapboxCarMapSurface) {
        super.onDetached(mapboxCarMapSurface)
        logAndroidAuto("CarNavigationCamera detached $mapboxCarMapSurface")
        MapboxNavigationApp.current()?.let { mapboxNavigation ->
            mapboxNavigation.unregisterRoutesObserver(routeObserver)
            mapboxNavigation.unregisterLocationObserver(locationObserver)
            mapboxNavigation.unregisterRouteProgressObserver(routeProgressObserver)
        }
        this.mapboxCarMapSurface = null
        isLocationInitialized = false

        mapboxCarMapSurface.mapSurface.getMapboxMap().getStyle()?.let {
            BreezeMapBoxUtil.removeCarkParkData(it, carParkFeatureDataList)
        }

        mapboxCarMapSurface.mapSurface.location.apply {
            mainCarContext.defaultLocationProvider?.let {
                setLocationProvider(it)
            }
        }

        obuCheckSpeedHelper?.destroy()
        obuCheckSpeedHelper = null
    }

    private val locationObserver = object : LocationObserver {
        override fun onNewRawLocation(rawLocation: Location) {
            currentLocation = rawLocation
            // not handled
        }

        override fun onNewLocationMatcherResult(locationMatcherResult: LocationMatcherResult) {
            // Initialize the camera at the current location. The next location will
            // transition into the following or overview mode.
            viewportDataSource?.onLocationChanged(locationMatcherResult.enhancedLocation)
            viewportDataSource?.evaluate()
            if (!isLocationInitialized) {
                isLocationInitialized = true
                val instantTransition = NavigationCameraTransitionOptions.Builder()
                    .maxDuration(0)
                    .build()
                when (cameraMode) {
                    CameraMode.IDLE -> navigationCamera?.requestNavigationCameraToIdle()
                    CameraMode.FOLLOWING -> navigationCamera?.requestNavigationCameraToFollowing(
                        stateTransitionOptions = instantTransition
                    )

                    CameraMode.OVERVIEW -> navigationCamera
                        ?.requestNavigationCameraToOverview(
                            stateTransitionOptions = instantTransition
                        )
                }
            }

            val speedInfoValue = speedLimitApi.updatePostedAndCurrentSpeed(
                locationMatcherResult,
                DistanceFormatterOptions.Builder(navigationContext.carContext)
                    .unitType(UnitType.METRIC)
                    .build()
            )
            obuCheckSpeedHelper?.changeSpeed(speedInfoValue.currentSpeed.toDouble())

            navigationLocationProvider.changePosition(
                locationMatcherResult.enhancedLocation,
                locationMatcherResult.keyPoints
            )
        }
    }

    private val routeObserver = RoutesObserver { result ->
        if (result.navigationRoutes.isEmpty()) {
            viewportDataSource?.clearRouteData()
        } else {
            viewportDataSource?.onRouteChanged(result.navigationRoutes.first())
        }
        viewportDataSource?.evaluate()
    }

    private val routeProgressObserver = RouteProgressObserver { routeProgress ->
        distanceRemainingInMetre = routeProgress.distanceRemaining
        if (shouldCheckAvailabilityAlertVoice) {
            val distance = BreezeCarUtil.masterlists?.carparkListAlertDistance?.let { itemSetting ->
                if (itemSetting.isNotEmpty()) {
                    itemSetting.first().value
                } else null
            } ?: Constants.DISTANCE_GET_AVAILABILITY_ALERT_VOICE

            if (distanceRemainingInMetre in 0f..distance.toFloat()) {
                getAvailabilityAlertVoice(
                    distance,
                    navigationContext.destinationAddressDetails
                )
            }
        }
        viewportDataSource?.onRouteProgressChanged(routeProgress)
        viewportDataSource?.evaluate()
    }

    /**
    START check stationary and get nearby carpark
     */
    private fun onSpeedStateChanged(
        speedState: OBUCheckSpeedHelper.SpeedState,
        stateChanged: Boolean
    ) {
        when (speedState) {
            OBUCheckSpeedHelper.SpeedState.RUNNING -> {
                if (stateChanged) {
                    clearCarparks()
                }
            }

            OBUCheckSpeedHelper.SpeedState.STATIONARY -> {
                if (stateChanged) {
                    fetchCarparks()
                }
            }

            else -> {}
        }
    }


    private fun clearCarparks() {
        when (cameraMode) {
            CameraMode.FOLLOWING -> {
                if (navigationCamera?.state != NavigationCameraState.FOLLOWING) {
                    navigationCamera?.requestNavigationCameraToFollowing(
                        NavigationCameraTransitionOptions.Builder().maxDuration(1000L).build()
                    )
                }
            }

            CameraMode.OVERVIEW -> {
                navigationCamera?.requestNavigationCameraToOverview(
                    NavigationCameraTransitionOptions.Builder().maxDuration(1000L).build()
                )
            }

            CameraMode.IDLE -> {
                navigationCamera?.requestNavigationCameraToIdle()
            }
        }

        if (carParkFeatureDataList.isNotEmpty()) {
            mapboxCarMapSurface?.mapSurface?.getMapboxMap()?.getStyle()?.let { style ->
                BreezeMapBoxUtil.removeCarkParkData(
                    style,
                    carParkFeatureDataList
                )
            }
        }
    }

    private fun fetchCarparks() {
        CoroutineScope(Dispatchers.IO).launch {
            currentLocation?.let { location ->
                var locationName: String? = Utils.getAddressFromLocation(
                    location,
                    navigationContext.carContext
                )
                withContext(Dispatchers.Main) {
                    if (locationName.isNullOrEmpty()) {
                        locationName = ""
                    }

                    getNearbyCarParks(
                        locationName!!,
                        location
                    )
                }
            }
        }

    }

    private fun getNearbyCarParks(
        destName: String,
        location: Location
    ) {
        val listCarPark: ArrayList<BaseAmenity> = arrayListOf()
        val carParkRequest = createCarParkRequest(location, destName)
        fetchAndUpdateCarParks(carParkRequest, listCarPark)
    }

    private fun fetchAndUpdateCarParks(
        carParkRequest: AmenitiesRequest,
        listCarPark: ArrayList<BaseAmenity>
    ) {
        BreezeCarUtil.apiHelper.getAmenities(carParkRequest)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : ApiObserver<ResponseAmenities>(compositeDisposable) {
                override fun onSuccess(data: ResponseAmenities) {
                    data.apply {
                        amenities.forEach { amenity ->
                            amenity.data.forEach {
                                if (amenity.type != null) {
                                    it.amenityType = amenity.type!!
                                    it.iconUrl = amenity.iconUrl
                                    listCarPark.add(it)
                                }
                            }
                        }

                        updateMapAndTemplateView(
                            listCarPark
                        )
                        updateCamera(listCarPark)
                        currentCarparkList = listCarPark
                    }


                }

                override fun onError(e: ErrorData) {

                }
            })
    }

    private fun createCarParkRequest(
        currentLocation: Location,
        destName: String
    ): AmenitiesRequest {
        val carParkRequest = AmenitiesRequest(
            lat = currentLocation.latitude,
            long = currentLocation.longitude,
            rad = BreezeCarUtil.breezeUserPreferenceUtil.getBreezeUserPreference()
                .getCarParkDistanceSettingValue(BreezeCarUtil.masterlists),
            maxRad = Constants.PARKING_CHECK_RADIUS_MAX,
            resultCount = BreezeCarUtil.breezeUserPreferenceUtil.getBreezeUserPreference()
                .getCarParkCountSettingValue(BreezeCarUtil.masterlists),
            destName = destName,
        )
        carParkRequest.setListType(
            listOf(AmenityType.CARPARK),
            BreezeCarUtil.breezeUserPreferenceUtil.getBreezeUserPreference()
                .getCarParkAvailabilityValue(
                    BreezeCarUtil.masterlists,
                    CarParkViewOption.ALL_NEARBY.mode
                )
        )
        carParkRequest.setCarParkCategoryFilter()
        return carParkRequest
    }

    private fun updateMapAndTemplateView(
        listCarPark: ArrayList<BaseAmenity>
    ) {
        mapboxCarMapSurface?.mapSurface?.getMapboxMap()?.getStyle()?.let { style ->
            BreezeMapBoxUtil.displayCarkParkData(
                carParkFeatureDataList,
                style,
                listCarPark,
                navigationContext.carContext,
                false
            )
        }
    }

    private fun updateCamera(listCarPark: List<BaseAmenity>) {
        navigationCamera?.requestNavigationCameraToIdle()
        mapboxCarMapSurface?.mapSurface?.getMapboxMap()?.let { mapboxMap ->
            val points = listCarPark.filter { it.long != null && it.lat != null }
                .map {
                    Point.fromLngLat(it.long ?: 0.0, it.lat ?: 0.0)
                }.toMutableList()
            currentLocation?.let {
                points.add(Point.fromLngLat(it.longitude, it.latitude))
            }

            val cameraOptions = mapboxMap.cameraForGeometry(
                GeometryCollection.fromGeometries(points.toList()),
                visibleArea,
                bearing = mapboxMap.cameraState.bearing,
                pitch = 0.0
            )
            mapboxMap.easeTo(cameraOptions)
        }
    }

    private fun getAvailabilityAlertVoice(
        distance: String,
        currentDestinationDetails: DestinationAddressDetails
    ) {
        shouldCheckAvailabilityAlertVoice = false
        val carparkId: String? = currentDestinationDetails.carParkID
        val userLocationLinkIdRef: String? = currentDestinationDetails.userLocationLinkIdRef
        val placeId: String? = currentDestinationDetails.placeId
        val amenityId: String? = currentDestinationDetails.amenityId
        val layerCode: String? = currentDestinationDetails.layerCode
        BreezeCarUtil.apiHelper.getAvailabilityAlert(
            distance = distance,
            carparkId = carparkId,
            userLocationLinkIdRef = userLocationLinkIdRef,
            placeId = placeId,
            amenityId = amenityId ?: carparkId,
            layerCode = layerCode,
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : ApiObserver<AvailabilityAlertResponse>(compositeDisposable) {
                override fun onSuccess(data: AvailabilityAlertResponse) {
                    if (data.success) {
                        Analytics.logEvent(
                            Event.OBU_SYSTEM_EVENT,
                            bundleOf(
                                Param.KEY_EVENT_VALUE to Event.BOTTOM_OBU_CARD_SHOWN,
                                Param.KEY_LATITUDE to (currentLocation?.latitude ?: 0.0),
                                Param.KEY_LONGITUDE to (currentLocation?.longitude ?: 0.0),
                                Param.KEY_MESSAGE to data.data?.navVoiceAlert,
                                Param.KEY_SCREEN_NAME to Screen.ANDROID_AUTO_NAVIGATION
                            )
                        )
                        data.data?.navVoiceAlert?.takeIf { it.isNotEmpty() }?.run {
                            VoiceInstructionsManager.getInstance()?.playVoiceInstructions(this)
                        }
                    }
                }

                override fun onError(e: ErrorData) {

                }
            }
            )
    }

    /**
    END check stationary and get nearby carpark
     */

    enum class CameraMode {
        IDLE,
        FOLLOWING,
        OVERVIEW
    }

    private companion object {
        /**
         * While following the location puck, inset the bottom by 1/3 of the screen.
         */
        private const val BOTTOM_FOLLOWING_PERCENTAGE = 1.0 / 3.0
    }
}

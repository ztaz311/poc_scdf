package com.ncs.breeze.di.module.fragment

import com.ncs.breeze.ui.dashboard.fragments.sharelocation.SharedLocationFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class  SharedLocationModule {
    @ContributesAndroidInjector
    abstract fun contributeSharedLocation(): SharedLocationFragment
}
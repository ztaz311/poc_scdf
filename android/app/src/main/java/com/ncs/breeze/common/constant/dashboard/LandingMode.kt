package com.ncs.breeze.common.constant.dashboard

enum class LandingMode(val mode: String) {
    PARKING("parking_mode"),
    ERP("erp_mode"),
    TRAFFIC("traffic_mode"),
    NO_MODE_SELLECT("no_mode_sellect"),
    OBU("obu_mode");

    fun canShowCarparks() = this == OBU || this == PARKING
}
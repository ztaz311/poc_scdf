package com.ncs.breeze.common.storage

import android.content.Context
import android.content.SharedPreferences

class BreezeAppPreference private constructor(context: Context) {

    private val sharedPreferences: SharedPreferences =
        context.getSharedPreferences(SHARE_PREFERENCE_NAME, Context.MODE_PRIVATE)

    fun saveString(key: AppPrefsKey, value: String, commitTransaction: Boolean = false) {
        val editor = sharedPreferences.edit().putString(key.value, value)
        if (commitTransaction) editor.commit() else editor.apply()
    }

    fun getString(key: AppPrefsKey, defaultValue: String = ""): String? {
        return sharedPreferences.getString(key.value, defaultValue)
    }

    fun saveBoolean(key: AppPrefsKey, value: Boolean, commitTransaction: Boolean = false) {
        val editor = sharedPreferences.edit().putBoolean(key.value, value)
        if (commitTransaction) editor.commit() else editor.apply()
    }

    fun getBoolean(key: AppPrefsKey, defaultValue: Boolean = false): Boolean {
        return sharedPreferences.getBoolean(key.value, defaultValue)
    }

    fun saveLong(key: AppPrefsKey, value: Long, commitTransaction: Boolean = false) {
        val editor = sharedPreferences.edit().putLong(key.value, value)
        if (commitTransaction) editor.commit() else editor.apply()
    }

    fun getLong(key: AppPrefsKey, defaultValue: Long = 0): Long {
        return sharedPreferences.getLong(key.value, defaultValue)
    }

    fun saveInt(key: AppPrefsKey, value: Int, commitTransaction: Boolean = false) {
        val editor = sharedPreferences.edit().putInt(key.value, value)
        if (commitTransaction) editor.commit() else editor.apply()
    }

    fun getInt(key: AppPrefsKey, defaultValue: Int = 0): Int {
        return sharedPreferences.getInt(key.value, defaultValue)
    }

    fun clear(key: AppPrefsKey, commitTransaction: Boolean = false) {
        val editor = sharedPreferences.edit().remove(key.value)
        if (commitTransaction) editor.commit() else editor.apply()
    }

    companion object {
        private const val SHARE_PREFERENCE_NAME = "Breeze_App_Preference"

        const val PREFIX_COLLECTION = "collection-"
        const val PREFIX_LOCATION = "location-"
        const val PREFIX_LOCATION_DESTINATION = "destination-"

        private var breezeUserPreference: BreezeAppPreference? = null
        fun getInstance(context: Context): BreezeAppPreference? {
            if (null == breezeUserPreference) {
                breezeUserPreference = BreezeAppPreference(context)
            }
            return breezeUserPreference
        }
    }

}
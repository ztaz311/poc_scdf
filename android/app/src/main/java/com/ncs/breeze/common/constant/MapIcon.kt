package com.ncs.breeze.common.constant

import com.ncs.breeze.R
import com.breeze.model.enums.TrafficIncidentData
import com.breeze.model.constants.Constants
import com.ncs.breeze.common.utils.ParkingIconUtils

object MapIcon {
    val RoadIncident = hashMapOf(
        (TrafficIncidentData.ACCIDENT.type + Constants.IMAGE) to R.drawable.accident,
        (TrafficIncidentData.OBSTACLE.type + Constants.IMAGE) to R.drawable.obstacle,
        (TrafficIncidentData.ROAD_WORK.type + Constants.IMAGE) to R.drawable.road_work,
        (TrafficIncidentData.VEHICLE_BREAKDOWN.type + Constants.IMAGE) to R.drawable.vehicle_breakdown,
        (TrafficIncidentData.ROAD_BLOCK.type + Constants.IMAGE) to R.drawable.road_block,
        (TrafficIncidentData.MISCELLANEOUS.type + Constants.IMAGE) to R.drawable.miscellaneous,
        (TrafficIncidentData.UNATTENDED_VEHICLE.type + Constants.IMAGE) to R.drawable.unattended_vehicle,
        (TrafficIncidentData.FLASH_FLOOD.type + Constants.IMAGE) to R.drawable.flash_flood,
        (TrafficIncidentData.MAJOR_ACCIDENT.type + Constants.IMAGE) to R.drawable.major_accident,
        (TrafficIncidentData.HEAVY_TRAFFIC.type + Constants.IMAGE) to R.drawable.miscellaneous,
        (TrafficIncidentData.ROAD_CLOSURE.type + Constants.IMAGE) to R.drawable.road_closure,
    )

    val WarningZone = hashMapOf(
        Constants.SCHOOL_ICON to R.drawable.school_zone,
        Constants.SILVER_ICON to R.drawable.silver_zone,
    )

    val ERP = hashMapOf(
        Constants.NO_RATES_ERP_ICON to R.drawable.erp_off,
        Constants.ERP_ICON to R.drawable.erp_with_price,
    )

    val CommonMapIcon = hashMapOf<String, Int>().apply {
        putAll(RoadIncident)
        putAll(WarningZone)
        putAll(ERP)

    }

    val NavigationMapIcon = CommonMapIcon.apply {
        putAll(hashMapOf(
            //car park
            ParkingIconUtils.ImageIconType.SELECTED.type to R.drawable.new_car_park_icon,
            //Finalised Colored Parking icons
            ParkingIconUtils.ParkingMarkerImageIconType.PARKINGMARKER_SEASON_DESTINATION_SELECTED.type to R.drawable.ic_parkingmarker_season_destination_selected,
            ParkingIconUtils.ParkingMarkerImageIconType.PARKINGMARKER_SEASON_DESTINATION_NOTSELECTED.type to R.drawable.ic_parkingmarker_season_destination_notselected,
            ParkingIconUtils.ParkingMarkerImageIconType.PARKINGMARKER_SEASON_NONDESTINATION_SELECTED.type to R.drawable.ic_parkingmarker_season_nondestination_selected,
            ParkingIconUtils.ParkingMarkerImageIconType.PARKINGMARKER_SEASON_NONDESTINATION_NOTSELECTED.type to R.drawable.ic_parkingmarker_season_nondestination_notselected,
            ParkingIconUtils.ParkingMarkerImageIconType.PARKINGMARKER_TYPE0_DESTINATION_SELECTED.type to R.drawable.ic_parkingmarker_typezero_destination_selected,
            ParkingIconUtils.ParkingMarkerImageIconType.PARKINGMARKER_TYPE0_DESTINATION_NOTSELECTED.type to R.drawable.ic_parkingmarker_typezero_destination_notselected,
            ParkingIconUtils.ParkingMarkerImageIconType.PARKINGMARKER_TYPE0_NONDESTINATION_SELECTED.type to R.drawable.ic_parkingmarker_type0_nondestination_selected,
            ParkingIconUtils.ParkingMarkerImageIconType.PARKINGMARKER_TYPE0_NONDESTINATION_NOTSELECTED.type to R.drawable.ic_parkingmarker_type0_nondestination_notselected,
            ParkingIconUtils.ParkingMarkerImageIconType.PARKINGMARKER_TYPE1_DESTINATION_SELECTED.type to R.drawable.ic_parkingmarker_typeone_destination_selected,
            ParkingIconUtils.ParkingMarkerImageIconType.PARKINGMARKER_TYPE1_DESTINATION_NOTSELECTED.type to R.drawable.ic_parkingmarker_typeone_destination_notselected,
            ParkingIconUtils.ParkingMarkerImageIconType.PARKINGMARKER_TYPE1_NONDESTINATION_SELECTED.type to R.drawable.ic_parkingmarker_type1_nondestination_selected,
            ParkingIconUtils.ParkingMarkerImageIconType.PARKINGMARKER_TYPE1_NONDESTINATION_NOTSELECTED.type to R.drawable.ic_parkingmarker_type1_nondestination_notselected,
            ParkingIconUtils.ParkingMarkerImageIconType.PARKINGMARKER_TYPE2_DESTINATION_SELECTED.type to R.drawable.ic_parkingmarker_typetwo_destination_selected,
            ParkingIconUtils.ParkingMarkerImageIconType.PARKINGMARKER_TYPE2_DESTINATION_NOTSELECTED.type to R.drawable.ic_parkingmarker_typetwo_destination_notselected,
            ParkingIconUtils.ParkingMarkerImageIconType.PARKINGMARKER_TYPE2_NONDESTINATION_SELECTED.type to R.drawable.ic_parkingmarker_type2_nondestination_selected,
            ParkingIconUtils.ParkingMarkerImageIconType.PARKINGMARKER_TYPE2_NONDESTINATION_NOTSELECTED.type to R.drawable.ic_parkingmarker_type2_nondestination_notselected,
            ParkingIconUtils.ParkingMarkerImageIconType.PARKINGMARKER_NORATE_DESTINATION_SELECTED.type to R.drawable.ic_parkingmarker_norate_destination_selected,
            ParkingIconUtils.ParkingMarkerImageIconType.PARKINGMARKER_NORATE_DESTINATION_NOTSELECTED.type to R.drawable.ic_parkingmarker_norate_destination_notselected,
            ParkingIconUtils.ParkingMarkerImageIconType.PARKINGMARKER_NORATE_NONDESTINATION_SELECTED.type to R.drawable.ic_parkingmarker_norate_nondestination_selected,
            ParkingIconUtils.ParkingMarkerImageIconType.PARKINGMARKER_NORATE_NONDESTINATION_NOTSELECTED.type to R.drawable.ic_parkingmarker_norate_nondestination_notselected,
            Constants.CARPARK_FEATURE_NEW_CARPARK_IMAGE to R.drawable.new_car_park_icon,
            Constants.CARPARK_INDEX_IMAGE to R.drawable.bg_car_map_icon_index,
            Constants.CARPARK_PRICE_IMAGE to R.drawable.ic_bg_parking_price_aa,
            Constants.CARPARK_PRICE_GREEN_IMAGE to R.drawable.ic_bg_parking_price_cheapest_aa
        ))
    }

}
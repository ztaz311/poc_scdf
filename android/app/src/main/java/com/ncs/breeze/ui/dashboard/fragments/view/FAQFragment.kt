package com.ncs.breeze.ui.dashboard.fragments.view

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import com.ncs.breeze.BuildConfig
import com.ncs.breeze.common.analytics.Screen
import com.ncs.breeze.databinding.FragmentFaqBinding
import com.ncs.breeze.ui.base.BaseFragment
import com.ncs.breeze.ui.dashboard.fragments.viewmodel.FAQViewModel
import javax.inject.Inject

class FAQFragment : BaseFragment<FragmentFaqBinding, FAQViewModel>() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val viewModel: FAQViewModel by viewModels {
        viewModelFactory
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initialize()
    }

    fun initialize() {
        viewBinding.backButton.setOnClickListener {
            onBackPressed()
        }

        viewBinding.webViewFaq.settings.run {
            cacheMode = WebSettings.LOAD_NO_CACHE
            javaScriptEnabled = true
        }

        viewBinding.webViewFaq.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                if (url.startsWith("mailto:") || url.startsWith("tel:")) {
                    val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
                    startActivity(intent)
                } else {
                    view.loadUrl(url)
                }
                return true
            }
        }

        viewBinding.webViewFaq.loadUrl(BuildConfig.FAQ_URL)
    }

    override fun inflateViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?,
        attachToContainer: Boolean
    ) = FragmentFaqBinding.inflate(inflater, container, attachToContainer)


    override fun getViewModelReference(): FAQViewModel {
        return viewModel
    }

    override fun getScreenName(): String {
        return Screen.SETTINGS_TERMS
    }
}
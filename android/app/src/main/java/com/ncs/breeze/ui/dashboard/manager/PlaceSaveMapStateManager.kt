package com.ncs.breeze.ui.dashboard.manager

import android.location.Location
import com.ncs.breeze.ui.dashboard.LocationFocus

object PlaceSaveMapStateManager : LocationFocus {

    const val AMENITY_INSTANCE = 0

    override var activeLocationUse: Location? = null
    override var currentLocationFocus: Location? = null
    override var forceFocusZoom: Double? = null
    override var currentFocusInstance: Int? = null
    var bottomSheetHeight = 1000
    var bottomSheetState = 0

    override fun setCurrentLocationFocus(
        type: Int,
        pLatitude: Double,
        pLong: Double,
        zoomRange: Double?
    ) {
        currentLocationFocus = Location("").apply {
            latitude = pLatitude
            longitude = pLong
        }
        forceFocusZoom = zoomRange
        currentFocusInstance = type
    }

    override fun resetCurrentLocationFocus(type: Int) {
        resetCurrentLocationFocus()
    }

    override fun resetCurrentLocationFocus() {
        currentLocationFocus = null
        forceFocusZoom = null
        currentFocusInstance = null
    }

    fun reset() {
        activeLocationUse = null
        bottomSheetHeight = 1000
        bottomSheetState = 0
        currentLocationFocus = null
    }
}
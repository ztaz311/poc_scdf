package com.ncs.breeze.components

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.ncs.breeze.R
import com.breeze.model.extensions.visibleAmount
import com.breeze.model.api.response.TripPlannerItem
import com.ncs.breeze.databinding.RowTravelLogItemBinding
import com.ncs.breeze.ui.draggableview.setClickSafe
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date
import java.util.Locale
import java.util.TimeZone

class UpcomingTripsAdapter(
    val listener: TripPlannerListener
) : PagingDataAdapter<TripPlannerItem, UpcomingTripsAdapter.ViewHolder>(REPO_COMPARATOR) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            RowTravelLogItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: UpcomingTripsAdapter.ViewHolder, position: Int) {
        getItem(position)?.let { travelItem ->
            holder.bind(travelItem)
            holder.itemView.setClickSafe {
                listener.onItemClick(travelItem)
            }
        }
    }


    companion object {
        private val REPO_COMPARATOR = object : DiffUtil.ItemCallback<TripPlannerItem>() {
            override fun areItemsTheSame(
                oldItem: TripPlannerItem,
                newItem: TripPlannerItem
            ): Boolean =
                (oldItem is TripPlannerItem && newItem is TripPlannerItem &&
                        oldItem.tripPlannerId == newItem.tripPlannerId)

            override fun areContentsTheSame(
                oldItem: TripPlannerItem,
                newItem: TripPlannerItem
            ): Boolean =
                oldItem == newItem
        }
    }

    fun getAdapterItem(position: Int): TripPlannerItem {
        return getItem(position) as TripPlannerItem
    }

    override fun getItemViewType(position: Int): Int {
        return when (getItem(position)) {
            is TripPlannerItem -> R.layout.row_travel_log_item
            else -> throw UnsupportedOperationException("Unknown view")
        }
    }

    interface TripPlannerListener {
        fun onItemClick(item: TripPlannerItem)
    }

    inner class ViewHolder(private val binding: RowTravelLogItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        private lateinit var tripItem: TripPlannerItem

        fun bind(repo: TripPlannerItem?) {
            repo?.let {
                showRepoData(it)
            }
        }

        private fun showRepoData(travelLog: TripPlannerItem) {
            this.tripItem = travelLog

            binding.originName.text = tripItem.tripStartAddress1
            binding.destinationName.text = tripItem.tripDestAddress1
            binding.totalDistance.text =
                tripItem.totalDistance?.let { getDistanceText(it.toFloat() / 1000) } ?: ""

            if (tripItem.tripEstStartTime != null && tripItem.tripEstArrivalTime != null) {
                binding.travelTime.text = getTravelledTimeString(
                    tripItem.tripEstStartTime!!,
                    tripItem.tripEstArrivalTime!!
                )
            } else {
                binding.travelTime.text = ""
            }

            binding.travelCost.text = tripItem.erpExpense?.visibleAmount() ?: "0.00"
            binding.travelDate.text = tripItem.tripEstStartTime?.let { getDateText(it) }
        }

        private fun getDistanceText(distance: Float): String {
            return distance.roundTo().toString() + "km"
        }

        private fun Float.roundTo(): Float {
            return "%.${2}f".format(Locale.ENGLISH, this).toFloat()
        }

        //Convert time to local format
        private fun getTravelledTimeString(tripStartTime: Long, tripEndTime: Long) =
            "${getTimeText(tripStartTime)} - ${getTimeText(tripEndTime)}"

        private fun getTimeText(time: Long): String {
            return try {
                val date = Date()
                date.time = time * 1000L
                val calendar = Calendar.getInstance()
                calendar.time = date
                calendar.timeZone = TimeZone.getDefault()

                val df = SimpleDateFormat("hh:mmaa", Locale.US)
                val formatTime = df.format(calendar.time)
                formatTime.toLowerCase()
            } catch (e: Exception) {
                ""
            }
        }

        private fun getDateText(time: Long): String {
            return try {
                val date = Date()
                date.time = time * 1000L
                val calendar = Calendar.getInstance()
                calendar.time = date
                calendar.timeZone = TimeZone.getDefault()
                val dayWeekText = SimpleDateFormat("EEE", Locale.US).format(date)

                val day = SimpleDateFormat("dd", Locale.US).format(date)
                val monthString = SimpleDateFormat("MMM", Locale.US).format(date)
                val yearString = SimpleDateFormat("YYYY", Locale.US).format(date)

                String.format("%s, %s %s, %s", dayWeekText, day, monthString, yearString)
            } catch (e: Exception) {
                ""
            }
        }


    }

}
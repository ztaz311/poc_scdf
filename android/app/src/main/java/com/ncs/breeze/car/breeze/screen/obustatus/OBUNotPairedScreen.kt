package com.ncs.breeze.car.breeze.screen.obustatus

import android.content.Intent
import androidx.car.app.model.Action
import androidx.car.app.model.CarColor
import androidx.car.app.model.MessageTemplate
import androidx.car.app.model.Template
import com.ncs.breeze.R
import com.ncs.breeze.car.breeze.MainBreezeCarContext
import com.ncs.breeze.car.breeze.base.BaseScreenCar
import com.ncs.breeze.car.breeze.utils.LimitClick
import com.ncs.breeze.car.breeze.utils.setClickSafe
import com.ncs.breeze.common.analytics.Screen


class OBUNotPairedScreen(val mainCarContext: MainBreezeCarContext, val intent: Intent? = null) :
    BaseScreenCar(mainCarContext.carContext) {

    override fun getScreenName(): String {
        return Screen.AA_OBU_NOT_PAIRED_SCREEN
    }

    override fun onGetTemplate(): Template {
        val message = carContext.getString(R.string.obu_not_paired_content)
        val builder = MessageTemplate.Builder(message)
        builder.setHeaderAction(Action.BACK)
        builder.addAction(
            Action.Builder()
                .setTitle(carContext.getString(R.string.button_okay))
                .setBackgroundColor(
                    CarColor.createCustom(
                        carContext.getColor(R.color.breeze_primary),
                        carContext.getColor(R.color.breeze_primary)
                    )
                )
                .setClickSafe {
                    LimitClick.handleSafe {
                        this.finish()
                    }
                }.build()
        )

            .setTitle(carContext.getString(R.string.obu_on_breeze))
        return builder.build()
    }

}
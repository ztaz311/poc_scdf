package com.ncs.breeze.ui.dashboard.fragments.exploremap

import android.content.Context
import android.content.res.Resources
import android.location.Location
import android.os.Bundle
import androidx.core.os.bundleOf
import com.breeze.model.DestinationAddressDetails
import com.breeze.model.api.response.ContentDetails
import com.breeze.model.api.response.amenities.BaseAmenity
import com.breeze.model.constants.AmenityType
import com.breeze.model.constants.AmenityType.CARPARK
import com.breeze.model.constants.AmenityType.POI
import com.breeze.model.constants.Constants
import com.breeze.model.extensions.dpToPx
import com.facebook.react.bridge.Arguments
import com.mapbox.geojson.Feature
import com.mapbox.geojson.Point
import com.mapbox.maps.CameraOptions
import com.mapbox.maps.EdgeInsets
import com.mapbox.maps.MapView
import com.mapbox.maps.MapboxMap
import com.mapbox.maps.plugin.animation.easeTo
import com.mapbox.navigation.ui.maps.camera.data.MapboxNavigationViewportDataSource
import com.mapbox.turf.TurfConstants
import com.mapbox.turf.TurfMeasurement
import com.ncs.breeze.App
import com.ncs.breeze.R
import com.ncs.breeze.common.analytics.Screen
import com.ncs.breeze.common.constant.RNScreen
import com.ncs.breeze.common.extensions.android.getRNFragmentNavigationParams
import com.ncs.breeze.common.utils.BreezeMapZoomUtil
import com.ncs.breeze.common.utils.LocationBreezeManager
import com.ncs.breeze.common.utils.Utils
import com.ncs.breeze.common.utils.walkathontracker.WalkathonEngine
import com.ncs.breeze.components.layermanager.MarkerLayerManager
import com.ncs.breeze.components.layermanager.impl.cluster.ClusteredMarkerLayerManager
import com.ncs.breeze.components.map.profile.ListenerCallbackLandingMap
import com.ncs.breeze.components.map.profile.MapProfile
import com.ncs.breeze.components.map.profile.MapProfileFactory
import com.ncs.breeze.components.map.profile.MapProfileOptions
import com.ncs.breeze.components.map.profile.MapProfileType
import com.ncs.breeze.components.marker.markerview2.NotificationMarkerViewManager
import com.ncs.breeze.helper.carpark.CarParkDestinationIconManager
import com.ncs.breeze.reactnative.nativemodule.ShareBusinessLogicEvent
import com.ncs.breeze.ui.dashboard.activity.DashboardActivity
import com.ncs.breeze.ui.dashboard.manager.DashboardMapStateManager
import com.ncs.breeze.ui.dashboard.manager.ExploreMapStateManager
import com.ncs.breeze.ui.rn.CustomReactFragment
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber

class ExploreMapController {

    lateinit var mapboxMap: MapboxMap
    lateinit var mapView: MapView
    lateinit var exploreFragment: ExploreMapsFragment
    lateinit var viewportDataSource: MapboxNavigationViewportDataSource
    lateinit var exploreUserLocationToolTip: ExploreUserLocationToolTip

    //map profile instance
    lateinit var mapProfile: MapProfile
    lateinit var mapProfileOptions: MapProfileOptions

    var markerLayerClickHandlerEnabled = true

    var currentMapProfileType: MapProfileType = MapProfileType.STANDARD

    lateinit var mContext: Context

    private var isMoveCameraWrapperAllCarpark = false

    private var listAmenities: HashMap<String, List<BaseAmenity>>? = null
    private var mListCarkPark: ArrayList<BaseAmenity>? = null
    private var showDestination: Boolean = false
    private var listContentDetail: ArrayList<ContentDetails>? = null

    var hasDestinationCarPark: Boolean = false

    /**
     * for carpark destination
     */
    var carParkDestinationIconManager: CarParkDestinationIconManager? = null

    /**
     * for Global Notification Symbol layers
     */
    var notificationMarkerLayerManager: NotificationMarkerViewManager? = null

    var callBack: ListenerCallbackLandingMap? = null
    var nightModeEnabled: Boolean = false

    var listenerShowToolTip: ListenerShowToolTip? = null

    fun setListenerShowTooltip(pListenerShowToolTip: ListenerShowToolTip) {
        listenerShowToolTip = pListenerShowToolTip
    }

    /**
     * NOTE:
     * destination icon manager map clicks has more precedence and should be initialized
     * before other amenities managers
     */
    fun setUp(
        pMapView: MapView,
        pExploreFragment: ExploreMapsFragment,
        nightModeEnabled: Boolean,
        markerLayerClickHandlerEnabled: Boolean = true,
        pCallBack: ListenerCallbackLandingMap,
    ) {
        callBack = pCallBack
        exploreFragment = pExploreFragment
        pExploreFragment.activity?.let {
            mContext = it.applicationContext
        }

        mapView = pMapView
        this.nightModeEnabled = nightModeEnabled
        mapboxMap = pMapView.getMapboxMap()
        viewportDataSource = MapboxNavigationViewportDataSource(
            mapView.getMapboxMap()
        )


        this.markerLayerClickHandlerEnabled = markerLayerClickHandlerEnabled
        carParkDestinationIconManager =
            CarParkDestinationIconManager(
                exploreFragment.requireActivity(),
                mapView
            )

        mapProfileOptions = MapProfileOptions.Builder(
            mapView,
            exploreFragment.requireActivity(),
            carParkDestinationIconManager!!,
            getScreenName()
        ).setMarkerLayerClickHandler(markerLayerClickHandler)
            .setClusterLayerClickHandler(mapClusterLayerClickListener)
            .setNightModeEnabled(nightModeEnabled)
            .setBaseMapCamera(exploreFragment.exploreMapCamera)
            .build()
        mapProfile = MapProfileFactory.generateMapProfile(
            currentMapProfileType,
            mapProfileOptions
        )
        notificationMarkerLayerManager = NotificationMarkerViewManager(mapView)
        exploreUserLocationToolTip = ExploreUserLocationToolTip(mapboxMap, mapView)
    }


    fun switchMapProfile(profile: MapProfileType, isCarparksShown: Boolean, reload: Boolean) {
        if (currentMapProfileType == profile) {
            return
        }
        currentMapProfileType = profile
        mapProfile.destroy()
        mapProfile = MapProfileFactory.generateMapProfile(
            profile,
            mapProfileOptions
        )
        CoroutineScope(Dispatchers.IO).launch {

            //todo bangnv
//            mAmenityTypes?.let {
//                mapProfile.loadAmenityStyleResources(it)
//            }
            withContext(Dispatchers.Main) {
                if (reload) {
                    mapProfile.handleAmenitiesData(
                        listAmenities ?: hashMapOf(),
                        showDestination,
                        null
                    ) {
                        true
                    }
                    mapProfile.handleCarPark(
                        mListCarkPark ?: listOf(),
                        isMoveCameraWrapperAllCarpark,
                        showDestination,
                        !isCarparksShown,
                        null
                    )
                }
            }
        }
    }


    /**
     * Updates the bottom sheet state, whether in collapsed(0) , mid(1) or full(2)
     * @param stateIndex the state of bottom sheet
     */
    fun updateBottomSheetState(stateIndex: Int, screen: String, onEnd: () -> Unit = {}) {
        App.instance?.let { app ->
            val shareBusinessLogicPackage = app.shareBusinessLogicPackage
            shareBusinessLogicPackage.getRequestsModule().flatMap {
                val data = Arguments.createMap().apply {
                    putInt(Constants.RN_CONSTANTS.BTM_SHEET_STATE_INDEX, stateIndex)
                    putString(Constants.RN_TO_SCREEN, screen)
                }
                it.callRN(ShareBusinessLogicEvent.UPDATE_BOTTOM_SHEET_INDEX.eventName, data)
            }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    onEnd.invoke()
                },
                    {
                        Timber.e("Error occurred while updating bottom sheet state")
                    })
        }
    }

    fun showYouAreHereTooltip() {
        if (!exploreFragment.isAdded || exploreFragment.context == null) {
            return
        }
        LocationBreezeManager.getInstance().currentLocation?.let { currentLocation ->
            exploreUserLocationToolTip.validateAndAddToolTip(
                currentLocation,
                exploreFragment.requireActivity()
            )
        }
    }


    private fun startRoutePlanningFromMarker(amenity: BaseAmenity, isCarPark: Boolean) {
        val currentLocation = LocationBreezeManager.getInstance().currentLocation ?: return
        CoroutineScope(Dispatchers.IO).launch {
            val sourceAddressDetails = DestinationAddressDetails(
                easyBreezyAddressID = null,
                address1 = Utils.getAddressFromLocation(currentLocation, mContext),
                address2 = null,
                lat = currentLocation.latitude.toString(),
                long = currentLocation.longitude.toString(),
                distance = null,
                destinationName = Constants.TAGS.CURRENT_LOCATION_TAG,
                destinationAddressType = null,
                ranking = -1,
            )
            val amenityLat = amenity.lat ?: return@launch
            val amenityLong = amenity.long ?: return@launch
            val markerLocation = Location("").apply {
                latitude = amenityLat
                longitude = amenityLong
            }
            val address = Utils.getAddressObject(markerLocation, mContext)
            val destinationDetail = DestinationAddressDetails(
                easyBreezyAddressID = null,
                address1 = amenity.name,
                address2 = address?.getAddressLine(0),
                lat = amenityLat.toString(),
                long = amenityLong.toString(),
                distance = null,
                destinationName = amenity.name,
                destinationAddressType = null,
                ranking = -1,
                isCarPark = isCarPark
            ).apply {
                carParkID = amenity.id
                if (isCarPark || amenity.amenityType == AmenityType.GENERAL || amenity.amenityType.equals(AmenityType.POI, true)) {
                    availablePercentImageBand = amenity.availablePercentImageBand
                    showAvailabilityFB = amenity.showAvailabilityFB
                    hasAvailabilityCS = amenity.hasAvailabilityCS
                    availabilityCSData = amenity.availabilityCSData
                }
                ExploreMapStateManager.contentDetailsResponse?.let { contentDetailsResponse ->
                    layerCode = contentDetailsResponse.layerCode
                    amenityId = amenity.id
                }
            }
            (exploreFragment.activity as? DashboardActivity)?.showRouteAndResetToCenter(
                bundleOf(
                    Constants.SOURCE_ADDRESS_DETAILS to sourceAddressDetails,
                    Constants.DESTINATION_ADDRESS_DETAILS to destinationDetail,

                    )
            )
        }
    }


    suspend fun loadAmenityStyleImages(pListContentDetail: List<ContentDetails>) {
        this.listContentDetail = pListContentDetail as ArrayList<ContentDetails>
        listContentDetail?.let {
            mapProfile.loadImageStyleResources(it)
        }
    }

    /**
     * handle amenities data
     */
    fun handleAmenitiesData(
        pListAmenities: HashMap<ContentDetailKey, List<BaseAmenity>>,
    ) {
        val supportedAmenities = HashMap(pListAmenities)
        this.listAmenities = HashMap()
        supportedAmenities.forEach {
            listAmenities?.put(it.key.code, it.value)
        }

        mapProfile.handleContentDetailData(
            pListAmenities!!,
            showDestination,
            null
        ) {
            true
        }
        if (ExploreMapStateManager.contentDetailsResponse?.type == Constants.CONTENT_LAYER.CONTENT_TYPE_ZONE) {
            zoomToZoneTypeDefault()
        } else {
            LocationBreezeManager.getInstance().currentLocation?.let {
                exploreFragment.exploreMapCamera.recenterMap({ it }) {
                    showYouAreHereTooltip()
                    exploreFragment.resetTrackerMode()
                }
            }
        }
    }

    fun resetPOIListZoomLevel() {
        if (ExploreMapStateManager.contentDetailsResponse?.type == Constants.CONTENT_LAYER.CONTENT_TYPE_ZONE) {
            zoomToZoneTypeDefault {
                exploreFragment.resetTrackerMode()
            }
        } else {
            LocationBreezeManager.getInstance().currentLocation?.let {
                exploreFragment.exploreMapCamera.recenterMap({ it }) {
                    showYouAreHereTooltip()
                    exploreFragment.resetTrackerMode()
                }
            }
        }
    }

    private fun zoomToZoneTypeDefault(onAnimationEnded: () -> Unit = {}) {
        ExploreMapStateManager.contentDetailsResponse?.let {
            if (it.zones.isNotEmpty()) {
                val radius: Double? = it.zones.last().radius?.toDouble()
                BreezeMapZoomUtil.recenterMapToLocation(
                    mapboxMap,
                    lat = it.zones[0].centerPointLat!!,
                    lng = it.zones[0].centerPointLong!!,
                    edgeInset = exploreFragment.exploreMapCamera.maxEdgeInsets,
                    zoomRadius = radius,
                    animationEndCB = onAnimationEnded
                )
            }
        }
    }

    /**
     * handle show carpark
     */
    fun handleCarkPark(
        fetchedCarkParks: ArrayList<BaseAmenity>,
        isMoveCameraWrapperAllCarpark: Boolean = false,
        showDestination: Boolean,
    ) {
        this.isMoveCameraWrapperAllCarpark = isMoveCameraWrapperAllCarpark
        this.showDestination = showDestination
        this.mListCarkPark = fetchedCarkParks
        val updatedCarParkList = this.mListCarkPark!!
        updateDestinationCarparkState(showDestination, updatedCarParkList)
        mapProfile.handleCarPark(
            updatedCarParkList,
            isMoveCameraWrapperAllCarpark,
            showDestination,
            false,
            null
        )

        /**
         * handle zoom carpark
         */
        val radius = calculateCarParkZoomRadius(updatedCarParkList)
        zoomForCarPark(radius)
    }

    private fun calculateCarParkZoomRadius(carkParks: ArrayList<BaseAmenity>): Double? {
        if (carkParks.isEmpty()) return null
        val selectedLocation = ExploreMapStateManager.activeLocationUse ?: return null
        val currentPoint = Point.fromLngLat(selectedLocation.longitude, selectedLocation.latitude)
        var radius: Double? = null
        carkParks.forEach {
            val currentDistance = TurfMeasurement.distance(
                currentPoint,
                Point.fromLngLat(it.long!!, it.lat!!),
                TurfConstants.UNIT_METERS
            )
            if (currentDistance > radius ?: 0.0) {
                radius = currentDistance
            }
        }
        return radius
    }

    private fun updateDestinationCarparkState(
        showDestination: Boolean,
        mListCarkPark: ArrayList<BaseAmenity>
    ) {
        if (showDestination) {
            hasDestinationCarPark = false
            for (mCarkParkItem in mListCarkPark) {
                if (mCarkParkItem.destinationCarPark == true) {
                    hasDestinationCarPark = true
                    break
                }
            }

        }
    }

    private fun showCarParkToolTip(
        amenity: BaseAmenity,
        layerType: String,
        shouldRecenter: Boolean
    ) {
        mapProfile.showCarParkToolTip(
            amenity,
            showDestination,
            false,
            callBack,
            layerType,
            shouldRecenter
        ) { amenity, isCarPark ->
            startRoutePlanningFromMarker(amenity, isCarPark)
        }

        val forceFocusZoom = if (shouldRecenter) {
            null
        } else {
            Constants.POI_TOOLTIP_ZOOM_RADIUS
        }

        ExploreMapStateManager.setCurrentLocationFocus(
            ExploreMapStateManager.AMENITY_INSTANCE,
            amenity.lat!!,
            amenity.long!!,
            forceFocusZoom
        )
    }


    fun clearALlCarPark() {
        mapProfile.clearALlCarPark()
        ExploreMapStateManager.resetCurrentLocationFocus()
    }

    fun removeALlTooltip() {
        mapProfile.removeALlTooltip()
        DashboardMapStateManager.resetCurrentLocationFocus()
    }

    fun clearALlMarkerAndTooltipAmenities() {
        mapProfile.clearALlMarkerAndTooltipAmenities()
        ExploreMapStateManager.resetCurrentLocationFocus()

    }


    fun getScreenName(): String {
        return Screen.FRAGMENT_EXPLORE
    }


    private fun zoomForCarPark(radius: Double?) {
        BreezeMapZoomUtil.recenterMapToLocation(
            mapboxMap,
            lat = ExploreMapStateManager.activeLocationUse!!.latitude,
            lng = ExploreMapStateManager.activeLocationUse!!.longitude,
            edgeInset = exploreFragment.exploreMapCamera.overviewEdgeInsets,
            zoomRadius = radius
        )
    }

    fun handleUpdateCenterOfMarker() {
        ExploreMapStateManager.currentLocationFocus?.let {
            BreezeMapZoomUtil.recenterMapToLocation(
                mapboxMap,
                it.latitude,
                it.longitude,
                EdgeInsets(
                    exploreFragment.exploreMapCamera.overviewEdgeInsets.top,
                    exploreFragment.exploreMapCamera.overviewEdgeInsets.left,
                    exploreFragment.exploreMapCamera.overviewEdgeInsets.bottom - 185.dpToPx(),
                    exploreFragment.exploreMapCamera.overviewEdgeInsets.right
                ),
                -1.0,
                ExploreMapStateManager.forceFocusZoom
            )

            //reset forceFocusZoom after first recenter
            ExploreMapStateManager.forceFocusZoom = null

            return@handleUpdateCenterOfMarker
        }
        exploreFragment.let {

            it.exploreMapCamera.maxEdgeInsets = EdgeInsets(
                it.exploreMapCamera.maxEdgeInsets.top,
                it.exploreMapCamera.maxEdgeInsets.left,
                it.exploreMapCamera.overviewEdgeInsets.bottom,
                it.exploreMapCamera.maxEdgeInsets.right
            )

            BreezeMapZoomUtil.adjustExistingCameraPadding(
                mapboxMap,
                it.exploreMapCamera.maxEdgeInsets
            )
        }
    }

    fun resetAllViews() {
        mapProfile.removeALlTooltip()
        notificationMarkerLayerManager!!.removeAllMarkers()
        exploreUserLocationToolTip.removeUserLocationToolTip()
    }

    private fun resetAllViews(layerType: String) {
        if (layerType == CARPARK && (ExploreMapStateManager.contentDetailsResponse?.type == Constants.CONTENT_LAYER.CONTENT_TYPE_ZONE)) {
            updateBottomSheetState(0, RNScreen.ZONE_AMENITY_DETAILS)
        }
        mapProfile.removeALlTooltip()
        notificationMarkerLayerManager!!.removeAllMarkers()
        exploreUserLocationToolTip.removeUserLocationToolTip()
    }

    private val markerLayerClickHandler = object : MarkerLayerManager.MarkerLayerClickHandler {
        override fun handleOnclick(
            layerType: String,
            feature: Feature,
            marker: MarkerLayerManager.MarkerViewType?
        ) {
            if (!markerLayerClickHandlerEnabled) {
                return
            }
            resetAllViews(layerType)
            //If the same marker was clicked then do nothing (resetAllViews would logically close and unselect it )
            if (marker != null && marker.feature.id() == feature.id()) {
                ExploreMapStateManager.resetCurrentLocationFocus(ExploreMapStateManager.AMENITY_INSTANCE)
                return
            }

            var type = layerType
            // For Clustered data, feature type is not the same as layer type, so we fetch the feature type from the feature selected
            val featureType = feature.getStringProperty(ClusteredMarkerLayerManager.FEATURE_TYPE)
            if (featureType != null) {
                type = featureType
            }
            var updateBottomSheet = false
            when (type) {
                CARPARK -> {
                    updateBottomSheet = shouldUpdateBottomSheet()
                    mListCarkPark?.let { cAmenity ->
                        val amenity = cAmenity.find { ba ->
                            return@find ba.id == feature.id()
                        }
                        amenity?.let { amenity ->
                            showCarParkToolTip(amenity, layerType, !updateBottomSheet)
                        }
                    }
                }

                else -> {
                    listAmenities?.let {
                        val amenity = it[type]?.find { ba ->
                            return@find ba.id == feature.id()
                        }
                        amenity?.let { amenity ->
                            if (amenity.amenityType.equals(POI, ignoreCase = true)) {
                                val enableWalking =
                                    ExploreMapStateManager.contentDetailsResponse?.enableWalking ?: false &&
                                            ExploreMapStateManager.contentDetailsResponse?.type == Constants.CONTENT_LAYER.CONTENT_TYPE_ZONE
                                handlePoiToolTip(
                                    amenity = amenity,
                                    type = type,
                                    layerType = layerType,
                                    shouldRecenter = false,
                                    enableWalking = enableWalking,
                                    enableZoneDrive = ExploreMapStateManager.contentDetailsResponse?.enableZoneDrive
                                        ?: true
                                )
                            } else {
                                updateBottomSheet = shouldUpdateBottomSheet()
                                if (
                                    ExploreMapStateManager.contentDetailsResponse?.type ==
                                    Constants.CONTENT_LAYER.CONTENT_TYPE_ZONE
                                ) {
                                    handlePoiToolTip(
                                        amenity = amenity,
                                        type = type,
                                        layerType = layerType,
                                        shouldRecenter = false,
                                        enableWalking = ExploreMapStateManager.contentDetailsResponse?.enableWalking
                                            ?: false,
                                        enableZoneDrive = ExploreMapStateManager.contentDetailsResponse?.enableZoneDrive
                                            ?: true
                                    )
                                } else {
                                    handleAmenityToolTip(
                                        amenity,
                                        type,
                                        layerType,
                                        !updateBottomSheet,
                                        enableWalking = ExploreMapStateManager.contentDetailsResponse?.enableWalking
                                            ?: false,
                                        enableZoneDrive = ExploreMapStateManager.contentDetailsResponse?.enableZoneDrive
                                            ?: true
                                    )
                                }
                            }
                            listenerShowToolTip?.show()
                        }
                    }
                }
            }
            //sets Bottom Sheet state to collapsed
            if (updateBottomSheet) {
                updateBottomSheetState(0, RNScreen.EXPLORE)
            }
        }

        override fun handleOnFreeMapClick(point: Point) {
            listenerShowToolTip?.hideUpdateBottomSheet()
            ExploreMapStateManager.resetCurrentLocationFocus(ExploreMapStateManager.AMENITY_INSTANCE)
        }
    }

    private fun shouldUpdateBottomSheet() =
        (ExploreMapStateManager.landingBottomSheetState != 0)

    private val mapClusterLayerClickListener =
        object : ClusteredMarkerLayerManager.ClusterLayerClickHandler {
            override fun handleOnclick(point: Point, zoomLevel: Number) {
                callBack?.onDismissedCameraTracking()
                mapboxMap.easeTo(
                    CameraOptions.Builder().center(point).zoom(zoomLevel.toDouble()).build()
                )
            }
        }

    private fun handlePoiToolTip(
        amenity: BaseAmenity,
        type: String,
        layerType: String,
        shouldRecenter: Boolean,
        enableWalking: Boolean,
        enableZoneDrive: Boolean,
    ) {
        mapProfile.handlePoiToolTipMapExplore(
            amenity = amenity,
            type = type,
            toolTipAction = callBack,
            layerType = layerType,
            shouldRecenter = shouldRecenter,
            startRoutePlanningFromMarker = { amenity, isCarPark ->
                startRoutePlanningFromMarker(amenity, isCarPark)
            },
            startWalkingPathFromMarker = { amenity, isCarPark ->
                startWalkingPathFromMarker(amenity, isCarPark)
            },
            enableWalking = enableWalking,
            enableZoneDrive = enableZoneDrive
        )

        ExploreMapStateManager.setCurrentSelectedPOILocation(amenity.lat!!, amenity.long!!)

        ExploreMapStateManager.setCurrentLocationFocus(
            DashboardMapStateManager.AMENITY_INSTANCE, amenity.lat!!, amenity.long!!,
            null
        )
        updateBottomSheetState(0, RNScreen.EXPLORE)
        if (amenity.amenityType.equals(POI, ignoreCase = true)) {
            showBottomSheetPOIInfo(amenity.id!!)
        }

        zoomToPoi(amenity)

        //call api to update analytics data
        if (ExploreMapStateManager.contentDetailsResponse!!.track_navigation) {
            amenity.id?.let {
                exploreFragment.saveCustomAnalytics(
                    it,
                    ExploreMapStateManager.contentDetailsResponse!!.layerCode!!,
                    amenity.amenityType
                )
            }
        }
    }

    private fun zoomToPoi(amenity: BaseAmenity) {
        BreezeMapZoomUtil.zoomViewToRange(mapboxMap,
            Point.fromLngLat(amenity.long!!, amenity.lat!!),
            Constants.POI_TOOLTIP_ZOOM_RADIUS,
            exploreFragment.exploreMapCamera.overviewEdgeInsets,
            animationEndCB = {
            }
        )
    }

    private fun showBottomSheetPOIInfo(poiId: String) {
        exploreFragment.removeAllFragmentInFrameLayer2()
        val navigationParams = exploreFragment.requireActivity().getRNFragmentNavigationParams(
            sessionToken = exploreFragment.myServiceInterceptor.getSessionToken(),
            fragmentTag = Constants.POI_AMENITY_MOREINFO.TO_POI_MOREINFO_SCREEN
        ).apply {
            putString(Constants.POI_AMENITY_MOREINFO.POI_ID, poiId)
        }
        val initialProperties = Bundle().apply {
            putString(Constants.RN_TO_SCREEN, Constants.POI_AMENITY_MOREINFO.TO_POI_MOREINFO_SCREEN)
            putBundle(Constants.RN_CONSTANTS.NAVIGATION_PARAMS, navigationParams)
        }

        val reactNativeFragment = CustomReactFragment.newInstance(
            componentName = Constants.RN_CONSTANTS.COMPONENT_NAME,
            launchOptions = initialProperties,
            cleanOnDestroyed = false
        )

        exploreFragment.childFragmentManager
            .beginTransaction()
            .replace(
                R.id.frBottomDetailLayer2,
                reactNativeFragment,
                Constants.POI_AMENITY_MOREINFO.TO_POI_MOREINFO_SCREEN
            )
            .commit()
    }

    private fun startWalkingPathFromMarker(ameniti: BaseAmenity, isCarPark: Boolean) {
        val currentLocation = LocationBreezeManager.getInstance().currentLocation ?: return
        CoroutineScope(Dispatchers.IO).launch {
            var destinationDetail: DestinationAddressDetails? = null
            val markerLocation = Location("")
            val sourceAddressDetails = DestinationAddressDetails(
                null,
                Utils.getAddressFromLocation(currentLocation, mContext),
                null,
                currentLocation.latitude.toString(),
                currentLocation.longitude.toString(),
                null,
                Constants.TAGS.CURRENT_LOCATION_TAG,
                null,
                -1,
            )
            if (ameniti.lat != null && ameniti.long != null) {
                markerLocation.latitude = ameniti.lat!!
                markerLocation.longitude = ameniti.long!!
                val nameLocation = ameniti.name
                val address = Utils.getAddressObject(markerLocation, mContext)
                destinationDetail = if (address != null) {
                    DestinationAddressDetails(
                        null,
                        nameLocation,
                        address.getAddressLine(0),
                        ameniti.lat!!.toString(),
                        ameniti.long!!.toString(),
                        null,
                        nameLocation,
                        null,
                        -1,
                        isCarPark
                    )
                } else {
                    DestinationAddressDetails(
                        null,
                        nameLocation,
                        null,
                        ameniti.lat!!.toString(),
                        ameniti.long!!.toString(),
                        null,
                        nameLocation,
                        null,
                        -1,
                        isCarPark
                    )
                }
                destinationDetail.amenityId = ameniti.id
                destinationDetail.layerCode =
                    ExploreMapStateManager.contentDetailsResponse!!.layerCode
                if (ExploreMapStateManager.contentDetailsResponse!!.track_navigation) {
                    destinationDetail.trackNavigation = true
                }

                /**
                 * check if carpark set id will use in navigation get alert
                 */
                if (isCarPark) {
                    destinationDetail.carParkID = ameniti.id
                    destinationDetail.availablePercentImageBand = ameniti.availablePercentImageBand
                }
            }

            /**
             * create object to push live location tracked walkathon
             * should check is tracked location.....
             */

            ExploreMapStateManager.contentDetailsResponse?.let { contentDetail ->

                if (contentDetail.track_navigation) {
                    WalkathonEngine.init(context = mContext)
                    WalkathonEngine.updateWalkathonRequest(
                        WalkathonEngine.getInstance()?.createNewWalkathonRequest(
                            contentDetail.content_id.toString(),
                            LocationBreezeManager.getInstance().currentLocation?.latitude.toString(),
                            LocationBreezeManager.getInstance().currentLocation?.longitude.toString()
                        )
                    )
                }

                (exploreFragment.activity as? DashboardActivity)?.let {
                    it.runOnUiThread {
                        if (destinationDetail != null) {
                            val args = Bundle()
                            args.putSerializable(
                                Constants.DESTINATION_ADDRESS_DETAILS,
                                destinationDetail
                            )
                            args.putSerializable(
                                Constants.SOURCE_ADDRESS_DETAILS,
                                sourceAddressDetails
                            )
                            it.showWalkingRouteAndResetToCenter(
                                args,
                                isTampinesMode = contentDetail.track_navigation
                            )
                        }
                    }
                }
            }
        }
    }


    private fun handleAmenityToolTip(
        amenity: BaseAmenity,
        type: String,
        layerType: String,
        shouldRecenter: Boolean,
        enableWalking: Boolean,
        enableZoneDrive: Boolean,
    ) {
        mapProfile.handleAmenityToolTip(
            amenity,
            type,
            callBack,
            layerType,
            shouldRecenter,
            false,
            enableWalking = enableWalking,
            enableZoneDrive = enableZoneDrive,
            startRoutePlanningFromMarker = { amenity, isCarPark ->
                startRoutePlanningFromMarker(amenity, isCarPark)
            },
            startWalkingPathFromMarker = { amenity, isCarPark ->
                startWalkingPathFromMarker(amenity, isCarPark)
            }
        )

        ExploreMapStateManager.setCurrentSelectedPOILocation(amenity.lat!!, amenity.long!!)

        ExploreMapStateManager.setCurrentLocationFocus(
            ExploreMapStateManager.AMENITY_INSTANCE, amenity.lat!!, amenity.long!!,
            if (shouldRecenter) null else Constants.POI_TOOLTIP_ZOOM_RADIUS
        )
    }


    interface ListenerShowToolTip {
        fun show()
        fun hide()
        fun hideUpdateBottomSheet()
    }

}
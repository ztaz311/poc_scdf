package com.ncs.breeze.car.breeze.screen.search.recentsearch

import androidx.car.app.ScreenManager
import androidx.car.app.model.Action
import androidx.car.app.model.ItemList
import androidx.car.app.model.ListTemplate
import androidx.car.app.model.Row
import androidx.car.app.model.Template
import com.breeze.model.SearchLocation
import com.breeze.model.extensions.safeDouble
import com.mapbox.androidauto.internal.logAndroidAuto
import com.mapbox.geojson.Point
import com.ncs.breeze.R
import com.ncs.breeze.car.breeze.MainBreezeCarContext
import com.ncs.breeze.car.breeze.base.BaseScreenCar
import com.ncs.breeze.car.breeze.utils.CarConstants
import com.ncs.breeze.car.breeze.utils.LimitClick
import com.ncs.breeze.car.breeze.utils.setClickSafe
import com.ncs.breeze.common.analytics.Screen
import com.ncs.breeze.common.event.ToAppRx.postEvent
import com.ncs.breeze.common.event.ToCarRx
import com.ncs.breeze.common.extensions.car.createCarIcon
import com.ncs.breeze.common.model.rx.AppToRecentSearchScreenEvent
import com.ncs.breeze.common.model.rx.AppToRoutePlanningStartEvent
import com.ncs.breeze.common.model.rx.CarGetListRecentSearch
import com.ncs.breeze.common.model.rx.DataForRoutePreview
import com.ncs.breeze.common.model.rx.ToCarRxEvent
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class RecentSearchScreen(val mainCarContext: MainBreezeCarContext) :
    BaseScreenCar(mainCarContext.carContext) {
    private var recentSearches: ArrayList<SearchLocation> = ArrayList()
    private var isLoading = true

    override fun onResumeScreen() {
        super.onResumeScreen()
        logAndroidAuto("onResumeScreen RecentSearchScreen ")
        postEvent(CarGetListRecentSearch())
    }


    override fun onPauseScreen() {
        super.onPauseScreen()
        logAndroidAuto("onPauseScreen RecentSearchScreen ")
    }

    override fun getScreenName(): String {
        return Screen.AA_RECENT_SEARCH_SCREEN
    }

    override fun onGetTemplate(): Template {
        return buildTemplate()
    }

    override fun onReceiverEventFromApp(event: ToCarRxEvent?) {
        super.onReceiverEventFromApp(event)
        when (event) {
            is AppToRecentSearchScreenEvent -> {
                logAndroidAuto("OpenSearchScreenEvent")
                recentSearches.clear()
                recentSearches.addAll(event.data.recentSearches)
                isLoading = false
                invalidate()
            }

            else -> {}
        }
    }

    private fun buildTemplate(): Template {

        val templateBuilder =
            ListTemplate.Builder().setTitle(carContext.getString(R.string.recent_search))
                .setHeaderAction(Action.BACK)
                .setLoading(isLoading)

        if (!isLoading) {
            val listBuilder = ItemList.Builder()
            recentSearches.forEach { data ->
                listBuilder.addItem(createRecentSearchItemBuilder(data).build())
            }
            templateBuilder.setSingleList(listBuilder.build())
        }
        return templateBuilder.build()
    }

    private fun createRecentSearchItemBuilder(data: SearchLocation) = Row.Builder()
        .setTitle(data.address1!!)
        .addText(data.address2!!)
        .setImage(carContext.createCarIcon(R.drawable.ic_car_item_recent_search))
        .setClickSafe {
            LimitClick.handleSafe {
                onSelectLocation(data)
            }
        }

    private fun onSelectLocation(data: SearchLocation) {
        val originalDestination = data.convertToDestinationAddressDetails()
        val screenManager: ScreenManager =
            mainCarContext.carContext.getCarService(ScreenManager::class.java)
        screenManager.popTo(CarConstants.HOME_MARKER)
        CoroutineScope(Dispatchers.Main).launch {
            ToCarRx.postEventWithCacheLast(
                AppToRoutePlanningStartEvent(
                    DataForRoutePreview(
                        selectedDestination = Point.fromLngLat(
                            data.long!!.safeDouble(),
                            data.lat!!.safeDouble(),
                        ),
                        originalDestination = originalDestination,
                        destinationSearchLocation = data
                    )
                )
            )
        }
    }
}
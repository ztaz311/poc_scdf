package com.ncs.breeze.ui.base

import android.location.Location
import androidx.viewbinding.ViewBinding
import com.ncs.breeze.common.storage.BreezeUserPreference
import com.ncs.breeze.common.utils.LocationBreezeManager

abstract class BaseMapActivity<T : ViewBinding, V : BaseViewModel> : BaseActivity<T, V>() {
    fun getCurrentLocation(): Location? {
        return LocationBreezeManager.getInstance().currentLocation
    }

    override fun onPause() {
        LocationBreezeManager.getInstance().currentLocation?.let {
            BreezeUserPreference.getInstance(applicationContext).saveLastLocation(it)
        }
        super.onPause()
    }

}
package com.ncs.breeze.car.breeze.utils

import android.bluetooth.BluetoothManager
import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.graphics.drawable.GradientDrawable
import android.graphics.drawable.ShapeDrawable
import android.os.SystemClock
import androidx.annotation.ColorInt

import androidx.car.app.model.Action
import androidx.car.app.model.GridItem
import androidx.car.app.model.OnClickListener
import androidx.car.app.model.Row
import java.util.Calendar

fun Action.Builder.setClickSafe(listener: OnClickListener?): Action.Builder {

    setOnClickListener(
        object : OnClickListener {

            var lastClickTime: Long = 0

            override fun onClick() {
                if (SystemClock.elapsedRealtime() - lastClickTime < 1000) {
                    return
                }
                listener?.onClick()
                lastClickTime = SystemClock.elapsedRealtime()
            }

        }
    )

    return this
}

fun GridItem.Builder.setClickSafe(listener: OnClickListener?): GridItem.Builder {

    setOnClickListener(
        object : OnClickListener {

            var lastClickTime: Long = 0

            override fun onClick() {
                if (SystemClock.elapsedRealtime() - lastClickTime < 1000) {
                    return
                }
                listener?.onClick()
                lastClickTime = SystemClock.elapsedRealtime()
            }

        }
    )

    return this
}


fun Row.Builder.setClickSafe(listener: OnClickListener?): Row.Builder {

    setOnClickListener(
        object : OnClickListener {

            var lastClickTime: Long = 0

            override fun onClick() {
                if (SystemClock.elapsedRealtime() - lastClickTime < 1000) {
                    return
                }
                listener?.onClick()
                lastClickTime = SystemClock.elapsedRealtime()
            }

        }
    )

    return this
}

fun Drawable.setDrawableColor(@ColorInt colorInt: Int) {
    when (this) {
        is GradientDrawable -> setColor(colorInt)
        is ShapeDrawable -> paint.color = colorInt
        is ColorDrawable -> color = colorInt
    }
}

fun Context.isBluetoothEnabled():Boolean{
    return (getSystemService(Context.BLUETOOTH_SERVICE) as? BluetoothManager)
    ?.adapter?.isEnabled == true
}

fun isAfter6PMOrWeekend():Boolean{
    val calendar = Calendar.getInstance()
    return calendar.get(Calendar.HOUR_OF_DAY) >= 18 || calendar.get(Calendar.DAY_OF_WEEK) !in 2..6
}
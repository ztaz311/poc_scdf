package com.ncs.breeze.di.module.fragment

import com.ncs.breeze.ui.dashboard.fragments.view.TravelLogFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class TravelLogModule {
    @ContributesAndroidInjector
    abstract fun contributeTravelLogFragment(): TravelLogFragment
}
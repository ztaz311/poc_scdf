package com.ncs.breeze.car.breeze.screen.search.searchoption

import androidx.activity.OnBackPressedCallback
import androidx.car.app.ScreenManager
import androidx.car.app.model.Action
import androidx.car.app.model.ActionStrip
import androidx.car.app.model.GridItem
import androidx.car.app.model.GridTemplate
import androidx.car.app.model.ItemList
import com.breeze.model.SearchLocation
import com.breeze.model.annotation.AmenityTypeVal
import com.breeze.model.constants.AmenityType.EVCHARGER
import com.breeze.model.constants.AmenityType.PETROL
import com.breeze.model.extensions.safeDouble
import com.mapbox.androidauto.internal.logAndroidAuto
import com.mapbox.geojson.Point
import com.ncs.breeze.R
import com.ncs.breeze.car.breeze.MainBreezeCarContext
import com.ncs.breeze.car.breeze.base.BaseScreenCar
import com.ncs.breeze.car.breeze.screen.obulite.CarOBULiteManager
import com.ncs.breeze.car.breeze.screen.search.recentsearch.RecentSearchScreen
import com.ncs.breeze.car.breeze.screen.search.searchscreen.SearchScreen
import com.ncs.breeze.car.breeze.utils.CarConstants
import com.ncs.breeze.car.breeze.utils.LimitClick
import com.ncs.breeze.car.breeze.utils.setClickSafe
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.Event
import com.ncs.breeze.common.analytics.Screen
import com.ncs.breeze.common.event.ToAppRx
import com.ncs.breeze.common.event.ToCarRx
import com.ncs.breeze.common.extensions.car.createCarIcon
import com.ncs.breeze.common.model.rx.AppToRoutePlanningStartEvent
import com.ncs.breeze.common.model.rx.AppToShortcutListScreenEvent
import com.ncs.breeze.common.model.rx.CarOpenAmenityScreen
import com.ncs.breeze.common.model.rx.DataForRoutePreview
import com.ncs.breeze.common.model.rx.GetShortcutListFromCar
import com.ncs.breeze.common.model.rx.ToCarRxEvent
import com.ncs.breeze.common.utils.OBUStripStateManager

class SearchOptionScreen(
    val mainCarContext: MainBreezeCarContext, private val fromScreen: String? = null
) :
    BaseScreenCar(mainCarContext.carContext) {

    private var isLoadingShortcuts = true
    private var homeLocation: SearchLocation? = null
    private var workLocation: SearchLocation? = null

    override fun getScreenName() = "[androidauto_search]"

    override fun onResumeScreen() {
        super.onResumeScreen()
        ToAppRx.postEvent(GetShortcutListFromCar())
    }

    override fun onCreateScreen() {
        super.onCreateScreen()
        carContext.onBackPressedDispatcher.addCallback({ lifecycle }, object :
            OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                Analytics.logClickEvent(":back", "[androidauto_search]")
                screenManager.pop()
            }
        })
    }

    override fun onReceiverEventFromApp(event: ToCarRxEvent?) {
        super.onReceiverEventFromApp(event)
        when (event) {
            is AppToShortcutListScreenEvent -> {
                logAndroidAuto("OpenSearchScreenEvent")
                homeLocation = event.data.home
                workLocation = event.data.work
                isLoadingShortcuts = false
                invalidate()
            }

            else -> {}
        }
    }


    override fun onGetTemplate(): GridTemplate {
        val templateBuilder = GridTemplate.Builder()
            .setLoading(isLoadingShortcuts)
            .setTitle(carContext.getString(R.string.search_title))
            .setHeaderAction(Action.BACK)
            .setActionStrip(
                ActionStrip.Builder()
                    .addAction(buildTextSearchAction())
                    .build()
            )
        if (!isLoadingShortcuts)
            templateBuilder.setSingleList(buildGridItemList())

        return templateBuilder.build()
    }

    private fun buildGridItemList(): ItemList {
        val gridItemListBuilder = ItemList.Builder()
        gridItemListBuilder.addItem(
            GridItem.Builder()
                .setImage(carContext.createCarIcon(R.drawable.ic_car_item_recent_search))
                .setTitle(carContext.getString(R.string.recent_searches))
                .setClickSafe {
                    LimitClick.handleSafe {
                        Analytics.logClickEvent(":recent_searches", getScreenName())
                        carContext.getCarService(ScreenManager::class.java)
                            .push(RecentSearchScreen(mainCarContext))
                        finish()
                    }
                }
                .build())

        if (homeLocation != null)
            gridItemListBuilder.addItem(
                GridItem.Builder()
                    .setImage(carContext.createCarIcon(R.drawable.ic_car_item_home))
                    .setTitle(carContext.getString(R.string.home))
                    .setClickSafe {
                        LimitClick.handleSafe {
                            Analytics.logClickEvent(":home", getScreenName())
                            onSelectLocation(homeLocation!!)
                        }
                    }

                    .build()
            )

        if (workLocation != null)
            gridItemListBuilder.addItem(
                GridItem.Builder()
                    .setImage(carContext.createCarIcon(R.drawable.ic_car_item_work))
                    .setClickSafe {
                        LimitClick.handleSafe {
                            Analytics.logClickEvent(":work", getScreenName())
                            onSelectLocation(workLocation!!)
                        }
                    }
                    .setTitle(carContext.getString(R.string.work))
                    .build()
            )

        gridItemListBuilder.addItem(
            GridItem.Builder()
                .setImage(carContext.createCarIcon(R.drawable.ic_car_item_petrol))
                .setTitle(carContext.getString(R.string.petrol))
                .setClickSafe {
                    LimitClick.handleSafe {
                        Analytics.logClickEvent(":petrol", getScreenName())
                        openAmenityScreen(PETROL)
                    }
                }
                .build())

        gridItemListBuilder.addItem(
            GridItem.Builder()
                .setImage(carContext.createCarIcon(R.drawable.ic_car_item_ev))
                .setTitle(carContext.getString(R.string.ev))
                .setClickSafe {
                    LimitClick.handleSafe {
                        Analytics.logClickEvent(":ev", getScreenName())
                        openAmenityScreen(EVCHARGER)
                    }
                }
                .build())
        return gridItemListBuilder.build()
    }

    private fun buildTextSearchAction() = Action.Builder()
        .setIcon(carContext.createCarIcon(R.drawable.ic_car_item_new_search))
        .setClickSafe {
            LimitClick.handleSafe {
                Analytics.logClickEvent(":keyboard", getScreenName())
                carContext
                    .getCarService(ScreenManager::class.java)
                    .push(SearchScreen(mainCarContext))
            }
        }.build()

    private fun onSelectLocation(data: SearchLocation) {
        val originalDestination = data.convertToDestinationAddressDetails()
        val screenManager: ScreenManager =
            mainCarContext.carContext.getCarService(ScreenManager::class.java)
        screenManager.popTo(CarConstants.HOME_MARKER)
        if (fromScreen == Screen.ANDROID_AUTO_CRUISE) {
            Analytics.logClickEvent(Event.AA_END, Screen.ANDROID_AUTO_CRUISE)
            OBUStripStateManager.getInstance()?.closeFromCar()
            CarOBULiteManager.closePhoneOBUScreen()
        }
        ToCarRx.postEventWithCacheLast(
            AppToRoutePlanningStartEvent(
                DataForRoutePreview(
                    selectedDestination = Point.fromLngLat(
                        data.long!!.safeDouble(),
                        data.lat!!.safeDouble(),
                    ),
                    originalDestination = originalDestination,
                    destinationSearchLocation = data
                )
            )
        )
    }

    private fun openAmenityScreen(@AmenityTypeVal type: String) {
        if (fromScreen == null)
            screenManager.popTo(CarConstants.HOME_MARKER)
        ToCarRx.postEvent(CarOpenAmenityScreen(type, fromScreen))
    }

}
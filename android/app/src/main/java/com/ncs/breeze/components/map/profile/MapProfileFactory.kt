package com.ncs.breeze.components.map.profile

object MapProfileFactory {

    fun generateMapProfile(type: MapProfileType, options: MapProfileOptions): MapProfile {
        return if (type == MapProfileType.STANDARD) {
            StandardMapProfile(options)
        } else {
            ClusteredMapProfile(options)
        }
    }

}
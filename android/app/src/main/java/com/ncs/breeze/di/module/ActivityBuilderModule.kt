package com.ncs.breeze.di.module

import com.ncs.breeze.di.module.fragment.AboutModule
import com.ncs.breeze.di.module.fragment.ChangeEmailModule
import com.ncs.breeze.di.module.fragment.ChangePasswordModule
import com.ncs.breeze.di.module.fragment.ChangeUserNameModule
import com.ncs.breeze.di.module.fragment.DebugModule
import com.ncs.breeze.di.module.fragment.ERPRoutePlanningModule
import com.ncs.breeze.di.module.fragment.ErpDetailFragmentModule
import com.ncs.breeze.di.module.fragment.ExploreMapsModule
import com.ncs.breeze.di.module.fragment.FAQModule
import com.ncs.breeze.di.module.fragment.FeedbackModule
import com.ncs.breeze.di.module.fragment.NewAddressModule
import com.ncs.breeze.di.module.fragment.OBUConnectFragmentModule
import com.ncs.breeze.di.module.fragment.OBULiteFragmentModule
import com.ncs.breeze.di.module.fragment.OBUPairedListFragmentModule
import com.ncs.breeze.di.module.fragment.PlaceSaveModule
import com.ncs.breeze.di.module.fragment.PrivacyPolicyModule
import com.ncs.breeze.di.module.fragment.ProfileFragmentModule
import com.ncs.breeze.di.module.fragment.RoutePanningModuleCard
import com.ncs.breeze.di.module.fragment.RoutePreferenceModule
import com.ncs.breeze.di.module.fragment.SettingsFragmentModule
import com.ncs.breeze.di.module.fragment.SharedCollectionModule
import com.ncs.breeze.di.module.fragment.SharedLocationModule
import com.ncs.breeze.di.module.fragment.TBTCarParkListRNModule
import com.ncs.breeze.di.module.fragment.TermsAndConditionModule
import com.ncs.breeze.di.module.fragment.TrafficDetailFragmentModule
import com.ncs.breeze.di.module.fragment.TravelLogModule
import com.ncs.breeze.di.module.fragment.TripInfoModule
import com.ncs.breeze.ui.dashboard.activity.DashboardActivity
import com.ncs.breeze.ui.dashboard.activity.DeeplinkEntryActivity
import com.ncs.breeze.ui.login.activity.LoginActivity
import com.ncs.breeze.ui.login.fragment.forgotPassword.module.ForgetPasswordEmailModule
import com.ncs.breeze.ui.login.fragment.forgotPassword.module.ForgotNewPasswordModule
import com.ncs.breeze.ui.login.fragment.forgotPassword.module.ForgotVerifyOTPModule
import com.ncs.breeze.ui.login.fragment.guest.module.GuestModeFragmentModule
import com.ncs.breeze.ui.login.fragment.signIn.module.SignInFragmentBreezeModule
import com.ncs.breeze.ui.login.fragment.signIn.module.VerifyMobileModule
import com.ncs.breeze.ui.login.fragment.signUp.module.CreateUserNameFragmentBreezeModule
import com.ncs.breeze.ui.login.fragment.signUp.module.LoginPhoneNumberFragmentBreezeModule
import com.ncs.breeze.ui.login.fragment.signUp.module.SignUpFragmentBreezeModule
import com.ncs.breeze.ui.login.fragment.signUp.module.TermsAndCondnModule
import com.ncs.breeze.ui.login.fragment.signUp.module.VerifyOTPFragmentModule
import com.ncs.breeze.ui.navigation.TurnByTurnNavigationActivity
import com.ncs.breeze.ui.splash.PermissionActivity
import com.ncs.breeze.ui.splash.SplashActivity
import com.ncs.breeze.ui.walkathon.WalkingActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Module
abstract class ActivityBuilderModule {

    @ContributesAndroidInjector(
        modules = [VerifyOTPFragmentModule::class,
            ForgetPasswordEmailModule::class, ForgotNewPasswordModule::class, ForgotVerifyOTPModule::class,
            TermsAndCondnModule::class, VerifyMobileModule::class,
            GuestModeFragmentModule::class, SignInFragmentBreezeModule::class, LoginPhoneNumberFragmentBreezeModule::class, SignUpFragmentBreezeModule::class,
            CreateUserNameFragmentBreezeModule::class]
    )
    internal abstract fun bindLoginActivity(): LoginActivity

    @ContributesAndroidInjector(
        modules = [NewAddressModule::class, RoutePanningModuleCard::class, ProfileFragmentModule::class, SettingsFragmentModule::class, RoutePreferenceModule::class, ChangeEmailModule::class,
            ChangeUserNameModule::class, ChangePasswordModule::class, ForgetPasswordEmailModule::class, ForgotNewPasswordModule::class,
            ForgotVerifyOTPModule::class, PrivacyPolicyModule::class, TravelLogModule::class, FeedbackModule::class, FAQModule::class, TermsAndConditionModule::class,
            AboutModule::class, TripInfoModule::class,
            DebugModule::class, ErpDetailFragmentModule::class, TrafficDetailFragmentModule::class,
            ERPRoutePlanningModule::class, PlaceSaveModule::class, ExploreMapsModule::class, OBULiteFragmentModule::class, OBUConnectFragmentModule::class,
            OBUPairedListFragmentModule::class, SharedCollectionModule::class, SharedLocationModule::class
        ]
    )
    internal abstract fun bindDashboardActivity(): DashboardActivity

    @ContributesAndroidInjector(modules = [TermsAndCondnModule::class, GuestModeFragmentModule::class, CreateUserNameFragmentBreezeModule::class])
    internal abstract fun bindSplashActivity(): SplashActivity

    @ContributesAndroidInjector(modules = [FeedbackModule::class, TBTCarParkListRNModule::class])
    internal abstract fun bindTurnByTurnActivity(): TurnByTurnNavigationActivity

    @ContributesAndroidInjector(modules = [FeedbackModule::class])
    internal abstract fun bindWalkingActivity(): WalkingActivity

    @ContributesAndroidInjector
    internal abstract fun bindPermissionActivity(): PermissionActivity

    @ContributesAndroidInjector
    internal abstract fun bindDeeplinkActivity(): DeeplinkEntryActivity
}
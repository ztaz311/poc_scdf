package com.ncs.breeze.car.breeze.utils

import com.breeze.model.api.response.Masterlists
import com.breeze.model.api.response.SettingsResponse
import com.ncs.breeze.common.remote.ApiHelper
import com.ncs.breeze.common.utils.*

/**
 * Loads singleton instances of util objects
 */
object BreezeCarUtil {

    var isActivityNotAvaiable = true

    lateinit var breezeIncidentsUtil: BreezeTrafficIncidentsUtil;
    lateinit var breezeERPUtil: BreezeERPRefreshUtil;
    lateinit var apiHelper: ApiHelper
    lateinit var breezeTripLoggingUtil: BreezeTripLoggingUtil
    lateinit var breezeStatisticsUtil: BreezeStatisticsUtil
    lateinit var breezeFeatureDetectionUtil: BreezeFeatureDetectionUtil
    lateinit var breezeSchoolSilverZoneUpdateUtil: BreezeSchoolSilverZoneUpdateUtil
    lateinit var breezeMiscRoadObjectUtil: BreezeMiscRoadObjectUtil
    lateinit var breezeUserPreferenceUtil: BreezeUserPreferenceUtil
    var masterlists: Masterlists? = null
    var listNameBlackList: ArrayList<String>? = null
    var settingsResponse: SettingsResponse? = null

    fun initBreezeUtils(
        breezeIncidentsUtil: BreezeTrafficIncidentsUtil,
        breezeERPUtil: BreezeERPRefreshUtil,
        apiHelper: ApiHelper,
        breezeTripLoggingUtil: BreezeTripLoggingUtil,
        breezeStatisticsUtil: BreezeStatisticsUtil,
        breezeFeatureDetectionUtil: BreezeFeatureDetectionUtil,
        breezeSchoolSilverZoneUpdateUtil: BreezeSchoolSilverZoneUpdateUtil,
        breezeMiscRoadObjectUtil: BreezeMiscRoadObjectUtil,
        breezeUserPreferenceUtil: BreezeUserPreferenceUtil
    ) {
        this.breezeIncidentsUtil = breezeIncidentsUtil;
        this.breezeERPUtil = breezeERPUtil;
        this.apiHelper = apiHelper;
        this.breezeTripLoggingUtil = breezeTripLoggingUtil
        this.breezeStatisticsUtil = breezeStatisticsUtil
        this.breezeFeatureDetectionUtil = breezeFeatureDetectionUtil
        this.breezeSchoolSilverZoneUpdateUtil = breezeSchoolSilverZoneUpdateUtil
        this.breezeMiscRoadObjectUtil = breezeMiscRoadObjectUtil
        this.breezeUserPreferenceUtil = breezeUserPreferenceUtil
    }

}
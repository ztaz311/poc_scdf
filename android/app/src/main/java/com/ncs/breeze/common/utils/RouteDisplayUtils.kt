package com.ncs.breeze.common.utils

import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

object RouteDisplayUtils {

    @Deprecated(
        message = "To match with new design requirements, please use methid 'formatArrivalTimeWithTime12H()'",
        level = DeprecationLevel.ERROR
    )
    fun formatArrivalTime(time: String): String {
        val inFormat: DateFormat = SimpleDateFormat("hh:mm a")
        val outFormat: DateFormat = SimpleDateFormat("HH:mm")
        var date: Date? = null
        try {
            date = inFormat.parse(time)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        if (date != null) {
            return outFormat.format(date)
        }
        return time
    }

    fun formatArrivalTimeWithTime12H(time: String): String {
        val inFormat: DateFormat = SimpleDateFormat("hh:mm a")
        val outFormat: DateFormat = SimpleDateFormat("hh:mmaa")
        var date: Date? = null
        try {
            date = inFormat.parse(time)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        if (date != null) {
            return outFormat.format(date).toLowerCase()
        }
        return time
    }
}
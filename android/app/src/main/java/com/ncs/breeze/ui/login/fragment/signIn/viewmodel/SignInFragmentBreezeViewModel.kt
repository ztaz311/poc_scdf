package com.ncs.breeze.ui.login.fragment.signIn.viewmodel

import android.app.Application
import com.amplifyframework.auth.cognito.exceptions.service.UserNotConfirmedException
import com.amplifyframework.auth.cognito.options.AWSCognitoAuthSignInOptions
import com.amplifyframework.auth.cognito.options.AuthFlowType
import com.amplifyframework.core.Amplify
import com.ncs.breeze.common.remote.ApiHelper
import com.breeze.model.constants.Constants
import com.ncs.breeze.common.utils.Utils
import com.google.firebase.crashlytics.ktx.crashlytics
import com.google.firebase.ktx.Firebase
import com.ncs.breeze.R
import com.ncs.breeze.common.extensions.android.getApp
import com.ncs.breeze.common.utils.AWSUtils
import com.ncs.breeze.components.AmplifyErrorHandler
import com.ncs.breeze.components.SingleLiveEvent
import com.ncs.breeze.ui.base.BaseFragmentViewModel
import timber.log.Timber
import javax.inject.Inject

class SignInFragmentBreezeViewModel @Inject constructor(
    val apiHelper: ApiHelper,
    application: Application
) : BaseFragmentViewModel(
    application
) {

    var mAPIHelper: ApiHelper = apiHelper
    var signInSuccessful: SingleLiveEvent<Boolean> = SingleLiveEvent()
    var signInSkip: SingleLiveEvent<Boolean> = SingleLiveEvent()
    var signInFailed: SingleLiveEvent<AmplifyErrorHandler.AuthObject> = SingleLiveEvent()
    var userNotConfirmed: SingleLiveEvent<Boolean> = SingleLiveEvent()

    fun login(username: String, password: String) {
        AWSUtils.signIn(
            username,
            password,
            AWSCognitoAuthSignInOptions.builder()
                .authFlowType(AuthFlowType.CUSTOM_AUTH_WITH_SRP)
                .build(),
            { result ->
                Utils.runOnMainThread {
                    if (result.nextStep.signInStep.name == Constants.AMPLIFY_CONSTANTS.SIGN_IN_WITH_CUSTOM_CHALLENGE) {
                        Timber.d("custom sign in successful")
                        signInSuccessful.postValue(true)
                    } else {
                        if (result.isSignedIn) {
                            Firebase.crashlytics.log("Custom sign in skipped, next action: [${result.nextStep.signInStep.name}]")
                            Timber.d("custom sign in skipped")
                            signInSkip.postValue(true)
                        } else {
                            Timber.d("custom sign in failed")
                            signInFailed.value = AmplifyErrorHandler.AuthObject(
                                getApp().getString(R.string.error_login),
                                AmplifyErrorHandler.FieldType.PASSWORD
                            )
                        }
                    }
                }
            },
            { error ->
                Utils.runOnMainThread {
                    if (error is UserNotConfirmedException) {
                        userNotConfirmed.value = true
                    } else {
                        signInFailed.value = AmplifyErrorHandler.getErrorMessage(
                            error,
                            getApp(),
                            AmplifyErrorHandler.AuthType.SIGN_IN
                        )
                    }
                }

            }
        )
    }

}
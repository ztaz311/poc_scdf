package com.ncs.breeze.ui.navigation

import androidx.car.app.connection.CarConnection

class CarConnectionStateHelper {
    var previousState = -1
    var currentState = CarConnection.CONNECTION_TYPE_NOT_CONNECTED
        set(value) {
            previousState = field
            field = value
        }

    fun isCarConnected() = currentState == CarConnection.CONNECTION_TYPE_PROJECTION
    fun isCarDisconnecting() =
        currentState != CarConnection.CONNECTION_TYPE_PROJECTION && previousState == CarConnection.CONNECTION_TYPE_PROJECTION
}
package com.ncs.breeze.di.module.fragment

import com.ncs.breeze.ui.dashboard.fragments.obuconnect.OBUPairedListFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class OBUPairedListFragmentModule {
    @ContributesAndroidInjector
    abstract fun contributeOBUConnectFragment(): OBUPairedListFragment
}
package com.ncs.breeze.car.breeze.screen.permissions

import androidx.car.app.CarContext
import androidx.car.app.model.Action
import androidx.car.app.model.MessageTemplate
import androidx.car.app.model.ParkedOnlyOnClickListener
import androidx.car.app.model.Template
import com.ncs.breeze.common.model.rx.AppEventPermissionsGranted
import com.ncs.breeze.common.event.ToCarRx
import com.ncs.breeze.car.breeze.base.BaseScreenCar
import com.ncs.breeze.car.breeze.utils.CarPermissionUtils
import com.ncs.breeze.R
import com.ncs.breeze.common.analytics.Screen.AA_NEED_PERMISSION_SCREEN

class NeedsLocationPermissionsScreen(
    carContext: CarContext
) : BaseScreenCar(carContext) {

    override fun getScreenName(): String {
        return AA_NEED_PERMISSION_SCREEN
    }

    override fun onGetTemplate(): Template {
        return MessageTemplate.Builder(
            carContext.getString(R.string.car_message_location_permissions)
        ).setTitle(
            carContext.getString(R.string.car_message_location_permissions_title)
        ).addAction(
            Action.Builder()
                .setTitle(carContext.getString(R.string.car_label_ok))
                .setOnClickListener(
                    ParkedOnlyOnClickListener.create {
                        if (CarPermissionUtils.hasEnoughPermissions(carContext)) {
                            ToCarRx.postEvent(AppEventPermissionsGranted())
                        } else {
                            carContext.finishCarApp()
                        }
                    }
                )
                .build()
        ).build()
    }
}

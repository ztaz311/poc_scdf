package com.ncs.breeze.car.breeze.screen.navigation

import android.content.Context
import androidx.car.app.model.CarIcon
import androidx.car.app.navigation.model.MessageInfo
import androidx.car.app.navigation.model.NavigationTemplate
import androidx.car.app.navigation.model.RoutingInfo
import androidx.car.app.navigation.model.Step
import androidx.core.graphics.drawable.IconCompat
import com.mapbox.androidauto.navigation.lanes.CarLanesImageRenderer
import com.mapbox.androidauto.navigation.lanes.useMapboxLaneGuidance
import com.mapbox.androidauto.navigation.maneuver.CarManeuverIconRenderer
import com.mapbox.androidauto.navigation.maneuver.CarManeuverMapper
import com.mapbox.bindgen.Expected
import com.mapbox.navigation.base.trip.model.RouteProgress
import com.mapbox.navigation.ui.maneuver.model.Maneuver
import com.mapbox.navigation.ui.maneuver.model.ManeuverError
import com.ncs.breeze.R
import com.ncs.breeze.common.extensions.mapbox.extractNonTamilAndChineseName

/**
 * The car library provides an [NavigationTemplate.NavigationInfo] interface to show
 * in a similar way we show [Maneuver]s. This class takes our maneuvers and maps them to the
 * provided [RoutingInfo] for now.
 */
class CarNavigationInfoMapper(
    private val carManeuverMapper: CarManeuverMapper,
    private val carManeuverIconRenderer: CarManeuverIconRenderer,
    private val carLanesImageGenerator: CarLanesImageRenderer,
    private val carDistanceFormatter: CarDistanceFormatter
) {

    fun mapNavigationInfo(
        expectedManeuvers: Expected<ManeuverError, List<Maneuver>>?,
        routeProgress: RouteProgress?
    ): NavigationTemplate.NavigationInfo? {
        val currentStepProgress = routeProgress?.currentLegProgress?.currentStepProgress
        val distanceRemaining = currentStepProgress?.distanceRemaining ?: return null
        val maneuver = expectedManeuvers?.value?.firstOrNull()
        val primaryManeuver = maneuver?.primary
        return if (primaryManeuver != null) {
            val carManeuver = carManeuverMapper
                .from(primaryManeuver.type, primaryManeuver.modifier, primaryManeuver.degrees)
            carManeuverIconRenderer.renderManeuverIcon(primaryManeuver)?.let {
                carManeuver.setIcon(it)
            }
            val step = Step.Builder(
                primaryManeuver.extractNonTamilAndChineseName() ?: primaryManeuver.text
            )
                .setManeuver(carManeuver.build())
                .useMapboxLaneGuidance(carLanesImageGenerator, maneuver.laneGuidance)
                .build()

            val stepDistance = carDistanceFormatter.carDistance(distanceRemaining.toDouble())
            RoutingInfo.Builder()
                //.setBackgroundColor() //? //ToDo - Ankur
                .setCurrentStep(step, stepDistance)
                .withOptionalNextStep(expectedManeuvers.value)
                .build()
        } else {
            null
        }
    }

    fun arrivedNavigationInfo(
        carContext: Context,
        destinationTitle: String,
        isDestinationCarPark: Boolean
    ): NavigationTemplate.NavigationInfo {
        val arrivalMessage = carContext.getString(R.string.we_have_arrived)
        val arrivalIconDrawable = if (isDestinationCarPark)
            R.drawable.ic_parking_info_bright_purple
        else
            R.drawable.saved_place
        val arrivalIcon = CarIcon.Builder(
            IconCompat.createWithResource(
                carContext,
                arrivalIconDrawable
            )
        ).build()
        return MessageInfo.Builder(destinationTitle)
            .setText(arrivalMessage)
            .setImage(arrivalIcon)
            .build()
    }

    private fun RoutingInfo.Builder.withOptionalNextStep(maneuvers: List<Maneuver>?) = apply {
        maneuvers?.getOrNull(1)?.primary?.let { nextPrimaryManeuver ->
            val nextCarManeuver = carManeuverMapper.from(
                nextPrimaryManeuver.type,
                nextPrimaryManeuver.modifier,
                nextPrimaryManeuver.degrees
            )
            val nextStep =
                Step.Builder(
                    nextPrimaryManeuver.extractNonTamilAndChineseName() ?: nextPrimaryManeuver.text
                )
                    .setManeuver(nextCarManeuver.build())
                    .build()
            setNextStep(nextStep)
        }
    }
}

package com.ncs.breeze.ui.navigation

import android.text.Html
import androidx.appcompat.app.AlertDialog
import com.ncs.breeze.R
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.Event
import com.ncs.breeze.common.extensions.android.setTextColorResource
import timber.log.Timber
import java.lang.ref.WeakReference

class TurnByTurnAlertDialogHelper(activity: TurnByTurnNavigationActivity) {
    private val activityRef = WeakReference(activity)

    fun showIncorrectPhoneAlert() {
        activityRef.get()?.let { activity ->
            val builder = AlertDialog.Builder(activity)
                .setMessage(R.string.incorrect_phone_number).setPositiveButton(
                    R.string.alert_close
                ) { dialog, _ ->
                    dialog.dismiss()
                }

            val alertDialog: AlertDialog = builder.create()

            alertDialog.setOnShowListener {
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).isAllCaps = false
                alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).isAllCaps = false
            }

            alertDialog.show()
        }
    }

    fun showRerouteConfirm(destinationName: String?, onProceed: () -> Unit) {
        activityRef.get()?.let { activity ->
            Timber.d("changed item observed")
            val builder = AlertDialog.Builder(activity)
            val message = Html.fromHtml(
                activity.getString(R.string.reroute_message) + "<b>" + destinationName + "</b>",
                Html.FROM_HTML_MODE_LEGACY
            )

            builder.setMessage(message)
                .setPositiveButton(R.string.proceed_reroute) { dialog, _ ->
                    onProceed.invoke()
                    dialog.dismiss()
                }.setNegativeButton(R.string.cancel_reroute) { dialog, which ->
                    dialog.dismiss()
                }

            val alertDialog: AlertDialog = builder.create()

            alertDialog.setOnShowListener {
                alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE)
                    .setTextColorResource(R.color.alert_negative_red)
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE)
                    .setTextColorResource(R.color.alert_positive_blue)
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).isAllCaps = false
                alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).isAllCaps = false
            }

            alertDialog.show()
        }
    }

    fun showEndNavigationConfirmationDialog(
        screenName: String,
        onConfirmed: () -> Unit
    ) {
        activityRef.get()?.let { activity ->
            if (activity.isWalking) {
                Analytics.logClickEvent(Event.END_WALKING, screenName)
            } else {
                Analytics.logClickEvent(Event.END_NAVIGATION, screenName)
            }

            val alertDialog = AlertDialog.Builder(activity)
                .setMessage(R.string.end_navigation)
                .setPositiveButton(R.string.end) { dialog, _ ->
                    onConfirmed.invoke()
                    dialog.dismiss()
                    Analytics.logClosePopupEvent(Event.POP_USER_END_YES, screenName)
                }
            alertDialog.setNegativeButton(R.string.search_alert_cancel) { dialog, _ ->
                Analytics.logClosePopupEvent(Event.POP_USER_END_NO, screenName)
                dialog.dismiss()
            }
            val alert = alertDialog.create()
            alert.setCanceledOnTouchOutside(false)
            alert.show()
        }
    }

}
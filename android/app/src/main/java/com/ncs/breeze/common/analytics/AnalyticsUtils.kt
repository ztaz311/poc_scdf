package com.ncs.breeze.common.analytics

import androidx.core.os.bundleOf
import com.ncs.breeze.common.helper.obu.OBUConnectionHelper
import com.breeze.model.obu.OBURoadEventData
import com.breeze.model.api.response.amenities.BaseAmenity

object AnalyticsUtils {
    fun sendSelectCarPark(amenity: BaseAmenity, screenName: String) {
        if (OBUConnectionHelper.isOBUSimulationEnabled) return
        Analytics.logEvent(
            Event.USER_CLICK,
            bundleOf(
                Param.KEY_SCREEN_NAME to screenName,
                Param.KEY_EVENT_VALUE to Event.OBU_CAR_PARK_SELECT,
                Param.KEY_MESSAGE to if (amenity.isCheapest == true) "select_cheapest" else "select_normal",
                Param.KEY_LOCATION_NAME to "location_${amenity.name ?: ""}",
                Param.KEY_LONGITUDE to ("${amenity.long ?: 0.0}"),
                Param.KEY_LATITUDE to ("${amenity.lat ?: 0.0}"),

                )
        )

    }

    fun sendRoadEvent(
        roadEvent: OBURoadEventData,
        locationName: String,
        lat: Double,
        long: Double,
        screenName: String,
        popupName: String,
        popupValue: String = Event.VALUE_POPUP_OPEN
    ) {
        if (OBUConnectionHelper.isOBUSimulationEnabled) return

        val title = getMessageText(roadEvent)
        val message = StringBuilder("message_$title")
        if (!roadEvent.distance.isNullOrEmpty()) {
            message.append(", distance_${roadEvent.distance}")
        }
        if (!roadEvent.chargeAmount.isNullOrEmpty()) {
            message.append(", price_${roadEvent.chargeAmount}")
        }
        if (!roadEvent.startTimeAnalytic.isNullOrEmpty() && !roadEvent.endTimeAnalytic.isNullOrEmpty()) {
            message.append(", parking_start_${roadEvent.startTimeAnalytic}")
            message.append(", parking_end_${roadEvent.endTimeAnalytic}")
        }
        Analytics.logEvent(
            Event.USER_POPUP,
            bundleOf(
                Param.KEY_SCREEN_NAME to screenName,
                Param.KEY_EVENT_VALUE to popupValue,
                Param.KEY_POPUP_NAME to popupName,
                Param.KEY_MESSAGE to message.toString(),
                Param.KEY_LOCATION_NAME to "location_$locationName",
                Param.KEY_LONGITUDE to ("$long"),
                Param.KEY_LATITUDE to ("$lat"),
            )
        )
    }

    fun sendRoadEventParkingAvailability(
        eventName: String,
        eventValue: String,
        locationName: String,
        lat: Double,
        long: Double,
        screenName: String,
        popupName: String = Event.OBU_DISPLAY_NOTIFICATION
    ) {
        if (OBUConnectionHelper.isOBUSimulationEnabled) return

        Analytics.logEvent(
            eventName,
            bundleOf(
                Param.KEY_SCREEN_NAME to screenName,
                Param.KEY_EVENT_VALUE to eventValue,
                Param.KEY_POPUP_NAME to popupName,
                Param.KEY_MESSAGE to "message_parking_availability",
                Param.KEY_LOCATION_NAME to "location_$locationName",
                Param.KEY_LONGITUDE to ("$long"),
                Param.KEY_LATITUDE to ("$lat"),
            )
        )
    }

    fun sendRoadEventTravelTime(
        eventName: String,
        eventValue: String,
        locationName: String,
        lat: Double,
        long: Double,
        screenName: String,
        popupName: String = Event.OBU_DISPLAY_NOTIFICATION
    ) {
        if (OBUConnectionHelper.isOBUSimulationEnabled) return

        Analytics.logEvent(
            eventName,
            bundleOf(
                Param.KEY_SCREEN_NAME to screenName,
                Param.KEY_EVENT_VALUE to eventValue,
                Param.KEY_POPUP_NAME to popupName,
                Param.KEY_MESSAGE to "message_traveltime",
                Param.KEY_LOCATION_NAME to "location_$locationName",
                Param.KEY_LONGITUDE to ("$long"),
                Param.KEY_LATITUDE to ("$lat"),
            )
        )
    }

    private fun getMessageText(data: OBURoadEventData): String {
        return data.eventType
    }
}
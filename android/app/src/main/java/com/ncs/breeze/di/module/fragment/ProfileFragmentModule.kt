package com.ncs.breeze.di.module.fragment

import com.ncs.breeze.ui.dashboard.fragments.view.ProfileFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class ProfileFragmentModule {
    @ContributesAndroidInjector
    abstract fun contributeAccountFragment(): ProfileFragment
}
package com.ncs.breeze.di.module.fragment

import com.ncs.breeze.ui.dashboard.fragments.view.FeedbackFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class FeedbackModule {
    @ContributesAndroidInjector
    abstract fun contributeFeedbackFragment(): FeedbackFragment
}
package com.ncs.breeze.ui.dashboard.fragments.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import com.facebook.react.bridge.Arguments
import com.google.gson.JsonObject
import com.mapbox.android.core.permissions.PermissionsManager
import com.mapbox.geojson.Feature
import com.mapbox.geojson.LineString
import com.mapbox.geojson.Point
import com.mapbox.maps.MapboxMap
import com.mapbox.maps.plugin.LocationPuck2D
import com.mapbox.maps.plugin.animation.camera
import com.mapbox.maps.plugin.locationcomponent.LocationComponentPlugin
import com.mapbox.maps.plugin.locationcomponent.LocationProvider
import com.mapbox.maps.plugin.locationcomponent.location
import com.mapbox.navigation.base.route.NavigationRoute
import com.mapbox.navigation.ui.maps.camera.NavigationCamera
import com.mapbox.navigation.ui.maps.camera.data.MapboxNavigationViewportDataSource
import com.mapbox.navigation.ui.utils.internal.ifNonNull
import com.mapbox.turf.TurfMeasurement
import com.mapbox.turf.TurfMisc
import com.ncs.breeze.App
import com.ncs.breeze.R
import com.ncs.breeze.common.analytics.Screen
import com.ncs.breeze.common.constant.RNScreen
import com.ncs.breeze.common.extensions.android.getRNFragmentNavigationParams
import com.ncs.breeze.common.extensions.mapbox.setBreezeDefaultOptions
import com.breeze.model.extensions.toBitmap
import com.ncs.breeze.common.model.RouteAdapterTypeObjects
import com.ncs.breeze.common.model.SimpleRouteAdapterObjects
import com.breeze.model.api.response.amenities.BaseAmenity
import com.ncs.breeze.common.remote.MyServiceInterceptor
import com.breeze.model.enums.RoutePreference
import com.ncs.breeze.common.utils.BreezeUserPreferenceUtil
import com.breeze.model.constants.Constants
import com.ncs.breeze.common.utils.RoutePlanningConsts.PROP_ERP_DISPLAY
import com.ncs.breeze.common.utils.RoutePlanningConsts.ROUTE_ERP_IMG
import com.ncs.breeze.common.utils.RoutePlanningConsts.SELECTED_ERP_RATE_IMG
import com.ncs.breeze.common.utils.RoutePlanningConsts.UNSELECTED_ERP_RATE_IMG
import com.ncs.breeze.common.utils.compression.GZIPCompressionUtil
import com.ncs.breeze.components.map.erp.MapERPManager
import com.ncs.breeze.components.map.profile.ListenerCallbackLandingMap
import com.ncs.breeze.databinding.FragmentErpRoutePreviewBinding
import com.ncs.breeze.helper.animation.ViewAnimator
import com.ncs.breeze.reactnative.nativemodule.ShareBusinessLogicEvent
import com.ncs.breeze.ui.dashboard.activity.DashboardActivity
import com.ncs.breeze.ui.dashboard.fragments.viewmodel.ERPRoutePreviewViewModel
import com.ncs.breeze.ui.rn.CustomReactFragment
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject


class ERPRoutePreviewFragment :
    BaseRoutePlanningFragment<FragmentErpRoutePreviewBinding, ERPRoutePreviewViewModel>() {

    private var selectedRoute: SimpleRouteAdapterObjects? = null
    private var selectedTimeStamp: Long? = null
    private var selectedTimeChanged: Boolean = false

    /**
     *for showing current location
     */
    private lateinit var defaultLocationPuck: LocationPuck2D
    private lateinit var locationComponent: LocationComponentPlugin
    private var defaultLocationProvider: LocationProvider? = null


    @Inject
    lateinit var breezeUserPreferenceUtil: BreezeUserPreferenceUtil

    @Inject
    lateinit var myServiceInterceptor: MyServiceInterceptor

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val viewModel: ERPRoutePreviewViewModel by viewModels {
        viewModelFactory
    }

    private lateinit var mapboxMap: MapboxMap
    private lateinit var viewportDataSource: MapboxNavigationViewportDataSource
    private lateinit var navigationCamera: NavigationCamera
    private lateinit var mapERPManager: MapERPManager

    var compositeDisposable: CompositeDisposable = CompositeDisposable()

    override fun inflateViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?,
        attachToContainer: Boolean
    ) = FragmentErpRoutePreviewBinding.inflate(inflater, container, attachToContainer)

    override fun getViewModelReference(): ERPRoutePreviewViewModel {
        return viewModel
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            CoroutineScope(Dispatchers.IO).launch {
                parseRouteDetails()
            }
        }
    }

    private suspend fun parseRouteDetails() {
        val routeDetails = (arguments?.getByteArray(Constants.SELECTED_ERP_ROUTE_DETAILS))!!
        selectedRoute =
            GZIPCompressionUtil.decompressFromGzip(routeDetails) as SimpleRouteAdapterObjects
        selectedTimeStamp =
            arguments?.getLong(Constants.SELECTED_ERP_ROUTE_TS)!!

        selectedTimeChanged =
            arguments?.getBoolean(Constants.IS_TIME_CHANGED) ?: false

        isDataLoaded.postValue(true)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mapboxMap = viewBinding.mapViewRoutePlan.getMapboxMap()

        init()

        viewportDataSource = MapboxNavigationViewportDataSource(
            mapboxMap
        )
        navigationCamera = NavigationCamera(
            viewBinding.mapViewRoutePlan.getMapboxMap(),
            viewBinding.mapViewRoutePlan.camera,
            viewportDataSource,

            )
        viewBinding.mapViewRoutePlan.setBreezeDefaultOptions()
        if (PermissionsManager.areLocationPermissionsGranted(activity)) {
            initRouteLine()
            initStyle()
        }
        initializeLocationComponent()
    }

    private fun initializeLocationComponent() {
        locationComponent = viewBinding.mapViewRoutePlan.location.apply {
            enabled = true
            pulsingEnabled = true
        }
        defaultLocationPuck = LocationPuck2D(
            bearingImage = activity?.getDrawable(R.drawable.cursor)
        )
        locationComponent.locationPuck = defaultLocationPuck

        defaultLocationProvider = locationComponent.getLocationProvider()
    }


    private fun initMarkerViews() {
        val mapListenerCallBack = object :
            ListenerCallbackLandingMap {
            override fun moreInfoButtonClick(ameniti: BaseAmenity) {
            }

            override fun onDismissedCameraTracking() {
            }

            override fun onMapOtherPointClicked(point: Point) {
            }

            override fun onERPInfoRequested(erpID: Int) {
                (requireActivity() as DashboardActivity).showERPInfoRN(
                    erpId = erpID,
                    timestamp = selectedTimeStamp ?: System.currentTimeMillis()
                )
            }

            override fun poiMoreInfoButtonClick(ameniti: BaseAmenity) {
                //
            }
        }
        mapERPManager = MapERPManager(
            viewBinding.mapViewRoutePlan, requireActivity(), getScreenName(),
            true, mapListenerCallBack
        )
    }


    override fun clearMarkerViews() {
        mapERPManager.removeAllMarkers()
    }


    private fun init() {

        (activity as DashboardActivity).hideKeyboard()
        initClickListeners()

    }


    private fun initClickListeners() {
        viewBinding.backButton.setOnClickListener {
            onBackPressed()
        }
    }


    private fun initStyle() {
        mapboxMap.loadStyleUri(
            (requireActivity() as DashboardActivity).getMapboxStyle(basicMapRequired = true)
        ) { style ->

            if (isFragmentDestroyed()) {
                return@loadStyleUri
            }
            onInitialized {
                showBottomSheetDetails()
                initMarkerViews()
                val STYLE_IMAGES: HashMap<String, Int> = hashMapOf(
                    SELECTED_ERP_RATE_IMG to R.drawable.route_info_blue_down,
                    UNSELECTED_ERP_RATE_IMG to R.drawable.route_info_white_down,
                    ROUTE_ERP_IMG to R.drawable.erp_small,
                    Constants.UNSAVED_DESTINATION_ICON to R.drawable.destination_puck,
                    Constants.FAVOURITES_DESTINATION_ICON to R.drawable.destination_puck,
                )
                for ((key, value) in STYLE_IMAGES) {
                    value.toBitmap(requireContext())?.let {
                        style.addImage(key, it, false)
                    }
                }
                selectedRoute?.let { selectedRoute ->
                    processRoutes(
                        listOf(
                            RouteAdapterTypeObjects(
                                RoutePreference.FASTEST_ROUTE,
                                selectedRoute
                            )
                        )
                    )
                }
            }
        }
    }

    override fun clearRoutes() {
        navigationCamera.requestNavigationCameraToIdle()
        viewportDataSource.clearRouteData()
    }

    override fun processRoutes(routeObjects: List<RouteAdapterTypeObjects>?) {
        if (routeObjects?.isNotEmpty() == true && routeLineView != null) {
            val routeLines: MutableList<NavigationRoute> = ArrayList()
            for (routeObj in routeObjects) {
                val navigationRoute = createNavigationRoute(routeObj)
                navigationRoute?.let {
                    routeLines.add(navigationRoute)
                }
            }
            routeLineAPI?.setNavigationRoutes(routeLines) { value ->
                ifNonNull(routeLineView, mapboxMap.getStyle()) { view, style ->
                    displayERPIcons()
                    view.renderRouteDrawData(style, value)
                }
            }
            ViewAnimator.measureView(viewBinding.llHeading)
            resetCameraToRoutes(
                routeLines,
                mapboxMap,
                370.0 * pixelDensity,
                30.0 * pixelDensity,
                30.0 * pixelDensity,
                30.0 * pixelDensity
            )

        } else {
            navigationCamera.requestNavigationCameraToIdle()
            viewportDataSource.clearRouteData()
        }
    }


    override fun displayERPIcons() {
        clearMarkerViews()
        val data = selectedRoute!!
        val geometry =
            LineString.fromPolyline(data.route.geometry()!!, Constants.POLYLINE_PRECISION)
        var feature = Feature.fromGeometry(
            geometry, JsonObject()
        )

        feature = TurfMeasurement.center(feature)
        var nearestPoint = TurfMisc.nearestPointOnLine(
            Point.fromLngLat(
                (feature.geometry() as Point).longitude(),
                (feature.geometry() as Point).latitude()
            ), geometry.coordinates()
        )
        feature = nearestPoint
        feature.addStringProperty(
            PROP_ERP_DISPLAY, String.format(
                "%dmin | $%.2f",
                data.duration.toInt(),
                data.erpRate
            )
        )

        if (data.erpList != null) {
            for ((index, erpChargeInfo) in data.erpList!!.reversed().withIndex()) {
                mapERPManager.handleSearchERP(
                    erpChargeInfo.erpId,
                    index == data.erpList!!.size - 1,
                    showInfoIcon = false,
                    timeToCheckERP = selectedTimeStamp ?: System.currentTimeMillis(),
                    screenName = getScreenName(),
                    forceShowErp = true,
                    zoomTo = false,
                    zoomValue = null,
                    showMinimalView = false,
                    forceCheckNextDayERP = false,
                    isTimeAlreadyChanged = selectedTimeChanged
                )
            }
        }
    }


    override fun onStart() {
        super.onStart()
        viewBinding.mapViewRoutePlan.onStart()
    }

    override fun onStop() {
        viewBinding.mapViewRoutePlan.onStop()
        super.onStop()
    }


    override fun onDestroy() {
        if (!compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
        super.onDestroy()
    }


    private fun showBottomSheetDetails() {
        val defaultSelectedErpID = selectedRoute?.erpList?.getOrNull(0)?.erpId

        val navigationParams = requireActivity().getRNFragmentNavigationParams(
            sessionToken = myServiceInterceptor.getSessionToken(),
            fragmentTag = RNScreen.ERP_DETAIL
        ).apply {
            defaultSelectedErpID?.let { id -> putInt(Constants.ERP_ID, id) }
            putLong(Constants.RN_CONSTANTS.ERP_SELECTED_TS, selectedTimeStamp ?: -1)
            putBoolean(Constants.RN_CONSTANTS.SHOW_PLAN_TRIP, true)
        }
        val initialProperties = Bundle().apply {
            putString(Constants.RN_TO_SCREEN, RNScreen.ERP_DETAIL)
            putBundle(Constants.RN_CONSTANTS.NAVIGATION_PARAMS, navigationParams)
        }

        val reactNativeFragment = CustomReactFragment.newInstance(
            componentName = Constants.RN_CONSTANTS.COMPONENT_NAME,
            launchOptions = initialProperties,
            cleanOnDestroyed = false
        )
        childFragmentManager
            .beginTransaction()
            .add(R.id.frBottomDetail, reactNativeFragment, Constants.TAGS.ERP_DETAIL_TAG)
            .addToBackStack(Constants.TAGS.ERP_DETAIL_TAG)
            .commit()
        defaultSelectedErpID?.let {
            sendEventERPSelected(it)
        }
    }

    private fun sendEventERPSelected(erpId: Int) {
        (activity?.applicationContext as? App)?.shareBusinessLogicPackage?.getRequestsModule()
            ?.flatMap {
                it.callRN(
                    ShareBusinessLogicEvent.SEND_EVENT_SELECTED_ERP.eventName,
                    Arguments.createMap().apply {
                        putInt("erpid", erpId)
                    })
            }?.subscribeOn(Schedulers.io())?.observeOn(AndroidSchedulers.mainThread())?.subscribe()
    }

    override fun getScreenName(): String {
        return Screen.ROUTE_PLANNING_ERP
    }

}



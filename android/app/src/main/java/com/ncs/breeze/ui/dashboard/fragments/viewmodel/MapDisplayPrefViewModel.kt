package com.ncs.breeze.ui.dashboard.fragments.viewmodel

import android.app.Application
import com.breeze.model.api.ErrorData
import com.breeze.model.api.response.SettingsItem
import com.breeze.model.api.response.SettingsResponse
import com.ncs.breeze.common.remote.ApiHelper
import com.ncs.breeze.common.remote.ApiObserver
import com.breeze.model.constants.Constants
import com.google.gson.JsonElement
import com.ncs.breeze.ui.base.BaseFragmentViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject

class MapDisplayPrefViewModel @Inject constructor(apiHelper: ApiHelper, application: Application) :
    BaseFragmentViewModel(
        application
    ) {
    var mAPIHelper: ApiHelper = apiHelper

    fun saveUserSettings(mapDisplayPreference: String, routePreference: String) {

        var settingsRequest = SettingsResponse(arrayListOf())
        settingsRequest.settings.add(
            SettingsItem(
                Constants.SETTINGS.ATTR_MAP_DISPLAY,
                mapDisplayPreference,
                mapDisplayPreference
            )
        )
        settingsRequest.settings.add(
            SettingsItem(
                Constants.SETTINGS.ATTR_ROUTE_PREFERENCE,
                routePreference,
                mapDisplayPreference
            )
        )

        mAPIHelper.setUserSettings(settingsRequest)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : ApiObserver<JsonElement>(
                compositeDisposable
            ) {
                override fun onSuccess(data: JsonElement) {
                    Timber.d("Retrieved user data is: " + data.toString())

                }

                override fun onError(e: ErrorData) {
                    Timber.i("AAA ${e.message} ${e.throwable}")
                }
            })

    }
}
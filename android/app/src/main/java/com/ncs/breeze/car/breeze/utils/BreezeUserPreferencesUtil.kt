package com.ncs.breeze.car.breeze.utils

import androidx.car.app.CarContext
import com.ncs.breeze.common.utils.BreezeUserPreferenceUtil

object BreezeUserPreferencesUtil {
    lateinit var breezeUserPreferenceUtil: BreezeUserPreferenceUtil
    fun initUtil(breezeUserPreferenceUtil: BreezeUserPreferenceUtil) {
        this.breezeUserPreferenceUtil = breezeUserPreferenceUtil
    }

    fun isDarkTheme(carContext: CarContext) = carContext.isDarkMode
}
package com.ncs.breeze.ui.dashboard.fragments.view

import android.annotation.SuppressLint
import android.app.Dialog
import android.graphics.Color
import android.graphics.Rect
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.os.SystemClock
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.WindowManager
import android.widget.CompoundButton
import android.widget.TextView
import androidx.annotation.IntDef
import androidx.appcompat.widget.SearchView
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.fragment.app.setFragmentResultListener
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.paging.PagingData
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import com.facebook.react.bridge.Arguments
import com.facebook.react.bridge.ReadableMap
import com.google.android.material.tabs.TabLayout
import com.ncs.breeze.App
import com.ncs.breeze.R
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.Event
import com.ncs.breeze.common.analytics.Screen
import com.ncs.breeze.common.constant.OBUConnectionState
import com.ncs.breeze.common.constant.dashboard.LandingMode
import com.ncs.breeze.common.extensions.android.getApp
import com.breeze.model.extensions.changeStringDateFormat
import com.breeze.model.extensions.formatDate
import com.breeze.model.extensions.toDate
import com.breeze.model.DestinationAddressDetails
import com.breeze.model.RoutePlanningMode
import com.breeze.model.obu.OBUDevice
import com.breeze.model.api.request.SendTripLogByMonthRequest
import com.breeze.model.api.request.SendTripLogRequest
import com.breeze.model.api.response.*
import com.ncs.breeze.common.rxbus.RxBus
import com.ncs.breeze.common.rxbus.RxEvent
import com.breeze.model.enums.AmplifyIdentityProviderType
import com.ncs.breeze.common.utils.BreezeUserPreferenceUtil
import com.breeze.model.constants.Constants
import com.ncs.breeze.common.utils.TimeConst
import com.breeze.customization.view.BreezeButton
import com.ncs.breeze.components.TravelHistoryAdapter
import com.ncs.breeze.components.TripOBUTransactionAdapter
import com.ncs.breeze.components.UpcomingTripsAdapter
import com.ncs.breeze.databinding.FragmentTravelLogBinding
import com.ncs.breeze.reactnative.nativemodule.ShareBusinessLogicEvent
import com.ncs.breeze.ui.base.BaseFragment
import com.ncs.breeze.ui.dashboard.activity.DashboardActivity
import com.ncs.breeze.ui.dashboard.fragments.viewmodel.TravelLogViewModel
import com.ncs.breeze.ui.draggableview.setClickSafe
import com.ncs.breeze.ui.utils.SwipeHelper
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

class TravelLogFragment : BaseFragment<FragmentTravelLogBinding, TravelLogViewModel>(),
    TravelHistoryAdapter.TripItemListener,
    UpcomingTripsAdapter.TripPlannerListener,
    TripOBUTransactionAdapter.TripObuTransactionListener,
    DashboardActivity.TravelLogListener,
    TransactionFilterDialogFragment.FilterSelectedCallback {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val mViewModel: TravelLogViewModel by viewModels {
        viewModelFactory
    }

    private lateinit var travelHistoryAdapter: TravelHistoryAdapter
    private lateinit var upcomingTripsAdapter: UpcomingTripsAdapter
    private lateinit var tripOBUTransactionAdapter: TripOBUTransactionAdapter

    private var lastClickedItem: TripsItem? = null
    private var isSelectViewEnabled: Boolean = false

    @TabPos
    private var tabPosition: Int = TAB_POSITION_HISTORY
    lateinit var dialog: Dialog
    private var isUpComingTripsFeatureUsed: Boolean = true
    private var isTripHistoryFeatureUsed: Boolean = true
    private var isObuTransactionsEmpty: Boolean = true

    private var selectedOBU: OBUDevice? = null
    private var pairedVehicleData: List<ObuVehicleDetails>? = null

    lateinit var monthSelectCheckedChangeListener: CompoundButton.OnCheckedChangeListener

    var compositeDisposable: CompositeDisposable = CompositeDisposable()

    var historyItemTouchHelper: ItemTouchHelper? = null

    private var canShowTransactionInfoExplain = false

    private var currentAnalyticsEventValue: String = ""

    @Inject
    lateinit var breezeUserPreferenceUtil: BreezeUserPreferenceUtil


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        tabPosition = arguments?.getInt(PARAM_TAB_POSITION) ?: TAB_POSITION_HISTORY
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initialize()
        setTravelLogFragmentResultListener()
        setVehicleDetailsFragmentResultListener()
    }

    fun initialize() {

        historyItemTouchHelper =
            ItemTouchHelper(object : SwipeHelper(viewBinding.tripRecyclerview) {
                override fun instantiateUnderlayButton(position: Int): List<UnderlayButton> {
                    return listOf(historyDeleteButton(position))
                }

            })

        travelHistoryAdapter = TravelHistoryAdapter(this)
        upcomingTripsAdapter = UpcomingTripsAdapter(this)
        tripOBUTransactionAdapter = TripOBUTransactionAdapter(this)

        tripOBUTransactionAdapter.addLoadStateListener {combinedLoadStates ->

            if (combinedLoadStates.source.refresh is LoadState.NotLoading ) {
                canShowTransactionInfoExplain = tripOBUTransactionAdapter.itemCount != 0
                if (tabPosition == TAB_POSITION_TRANSACTION)
                    viewBinding.llTransactionInfo.isVisible = canShowTransactionInfoExplain
            }
        }

        // show the loading state for  first load
//        TODO("Show/hide Progressbar")
        travelHistoryAdapter.addLoadStateListener { loadState ->

            if (loadState.source.refresh is LoadState.NotLoading && loadState.append.endOfPaginationReached && travelHistoryAdapter.itemCount < 1) {
                showEmptyView(true)
            } else {
                showEmptyView(false)
                if (loadState.refresh is LoadState.Loading) {
//                // Show ProgressBar
//                progressBar.visibility = View.VISIBLE
                } else {
                    // Hide ProgressBar
//                progressBar.visibility = View.GONE
                    // Handle for error
                    val errorState = when {
                        loadState.append is LoadState.Error -> loadState.append as LoadState.Error
                        loadState.prepend is LoadState.Error -> loadState.prepend as LoadState.Error
                        loadState.refresh is LoadState.Error -> {
                            loadState.refresh as LoadState.Error
                        }

                        else -> null
                    }
                    errorState?.let {
//                    TODO("Error Handling")
                    }
                }
            }
        }

        viewBinding.backButton.setOnClickListener {
            onBackPressed()
        }

        viewBinding.searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                Analytics.logClickEvent(Event.SEARCH, getScreenName())
                if (viewBinding.tripPlannerRecyclerview.visibility == View.VISIBLE) {
                    mViewModel.lastTripPlanSearchQuery = query ?: ""
                    refreshTripPlannerData(
                        searchText = query,
                        startDate = mViewModel.upcomingStartDate?.formatDate(TimeConst.yyyyMMdd),
                        endDate = mViewModel.upcomingEndDate?.formatDate(TimeConst.yyyyMMdd)
                    )
                } else if (viewBinding.tripRecyclerview.visibility == View.VISIBLE) {
                    refreshTripHistoryData(
                        searchText = query,
                        startDate = mViewModel.historyStartDate?.formatDate(TimeConst.yyyyMMdd),
                        endDate = mViewModel.historyEndDate?.formatDate(TimeConst.yyyyMMdd)
                    )
                }
                viewBinding.searchView.clearFocus()
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {

                if (newText.isNullOrEmpty()) {
                    if (tabPosition == TAB_POSITION_UPCOMING) {
                        mViewModel.lastTripPlanSearchQuery = ""
                        refreshTripPlannerData(
                            searchText = "",
                            startDate = "",
                            endDate = ""
                        )
                    } else if (tabPosition == TAB_POSITION_HISTORY) {
                        refreshTripHistoryData(
                            searchText = "",
                            startDate = "",
                            endDate = ""
                        )
                    }
                }
                return true
            }
        })

        viewBinding.tvDateRange.run {
            text = mViewModel.getUpcomingDateRangeStr()
            setOnClickListener() {
                when (tabPosition) {
                    TAB_POSITION_UPCOMING -> {
                        showDateRangeSelector(
                            mViewModel.upcomingStartDate?.formatDate(TimeConst.yyyyMMdd),
                            mViewModel.upcomingEndDate?.formatDate(TimeConst.yyyyMMdd)
                        )
                    }

                    TAB_POSITION_HISTORY -> {
                        showDateRangeSelector(
                            mViewModel.historyStartDate?.formatDate(TimeConst.yyyyMMdd),
                            mViewModel.historyEndDate?.formatDate(TimeConst.yyyyMMdd)
                        )
                    }

                    TAB_POSITION_TRANSACTION -> {
                        showDateRangeSelector(
                            mViewModel.transactionStartDate?.formatDate(TimeConst.yyyyMMdd),
                            mViewModel.transactionEndDate?.formatDate(TimeConst.yyyyMMdd)
                        )
                    }
                }
            }
        }

        compositeDisposable.add(
            RxBus.listen(RxEvent.DateRangeSelected::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    handleDateRangeSelected(it.startDate, it.endDate)
                })

        compositeDisposable.add(
            RxBus.listen(RxEvent.IsUpComingTripsFeatureUsed::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    isUpComingTripsFeatureUsed = it.status
                    manageFeatureText()
                })
        compositeDisposable.add(
            RxBus.listen(RxEvent.IsTripHistoryFeatureUsed::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    isTripHistoryFeatureUsed = it.status
                    manageFeatureText()
                })

        compositeDisposable.add(
            RxBus.listen(RxEvent.IsTripObuTransactionFeatureUsed::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    isObuTransactionsEmpty = it.isResultEmpty
                    manageFeatureText()
                })

        monthSelectCheckedChangeListener = object : CompoundButton.OnCheckedChangeListener {
            override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
                mViewModel.selectRecordsOfMonth(travelHistoryAdapter, isChecked)
                Analytics.logClickEvent(Event.TRAVEL_LOG_MONTH_SELECT, getScreenName())
            }
        }
        viewBinding.selectCheckbox.setOnCheckedChangeListener(monthSelectCheckedChangeListener)

        viewBinding.select.setOnClickListener {
            Analytics.logClickEvent(Event.TRAVEL_LOG_SELECT_ALL, getScreenName())
            viewBinding.selectCheckbox.visibility = View.VISIBLE
            viewToggle()
        }
        viewBinding.tvFilter.setClickSafe {
            Analytics.logClickEvent(Event.TRANSACTION_FILTER_TRANSACTION_TYPE, getScreenName())
            openFilterDialog()
        }

        initTab()

        initTripHistoryAdapter()
        initUpcomingTripAdapter()
        initTripObuTransactionListenersAndAdapter()

        mViewModel.deleteTripSuccessful.observe(viewLifecycleOwner) {
            if (it.first) {
                clearAndFetchUpcomingTrips()
            }
        }

        mViewModel.deleteTripHistorySuccessful.observe(viewLifecycleOwner) {
            if (it.first) {
                clearAndFetchTripHistory()
            }
        }

        viewBinding.emailButton.setBGThemed()
        viewBinding.emailButton.setOnClickListener {
            Analytics.logClickEvent(Event.TRAVEL_LOG_SEND_EMAIL, getScreenName())
            if (mViewModel.getSelectedList().isEmpty()) {
                return@setOnClickListener
            }

            val identityProvider = breezeUserPreferenceUtil.retrieveAmplifyIdentityProvider()

            if ((identityProvider != AmplifyIdentityProviderType.AMPLIFY_DEFAULT.type) ||
                (breezeUserPreferenceUtil.retrieveUserEmail().isNotEmpty() &&
                        breezeUserPreferenceUtil.retrieveUserEmailVerifiedStatus())
            ) {

                mViewModel.sendEmailSuccessful.observe(viewLifecycleOwner) {
                    if (it) {
                        showSuccessDialog(mViewModel.getSelectedList().size)
                        resetSelectedList()
                    }
                }
                if (viewBinding.selectCheckbox.isChecked) { // if month is selected call trip email api by month
                    val request = SendTripLogByMonthRequest()
                    request.month = mViewModel.month
                    request.year = mViewModel.year
                    mViewModel.sendTripLogEmailForMonth(request)
                } else { //trip email api by selected trips
                    val request = SendTripLogRequest()
                    request.tripIds = mViewModel.getSelectedList()
                    mViewModel.sendTripLogEmail(request)
                }
            } else {
                // Launch Email fragment
                val bundle = Bundle()
                bundle.putBoolean(ChangeEmailFragment.IS_FROM_TRAVEL_LOG, true)
                (requireActivity() as DashboardActivity).addFragmentBottomUp(
                    ChangeEmailFragment(),
                    bundle,
                    Constants.TAGS.SETTINGS_TAG
                )
            }
        }

        viewLifecycleOwner.lifecycleScope.launch {
            travelHistoryAdapter.loadStateFlow.collectLatest { loadStates ->
                if (loadStates.refresh is LoadState.NotLoading && travelHistoryAdapter.itemCount > 0) {
                    viewBinding.select.isEnabled = true
                    activity?.let {
                        viewBinding.select.setTextColor(
                            ContextCompat.getColor(
                                it,
                                R.color.breeze_primary
                            )
                        )
                    }
                } else {
                    viewBinding.select.isEnabled = false
                    activity?.let {
                        viewBinding.select.setTextColor(
                            ContextCompat.getColor(
                                it,
                                R.color.color_disable_text
                            )
                        )
                    }
                }
            }
        }
    }

    private fun openRoutePlanningScreen(details: UpcomingTripDetailResponse) {
        val address = DestinationAddressDetails(
            null,
            details.tripDestAddress1,
            details.tripDestAddress2,
            details.tripDestLat,
            details.tripDestLong,
            details.totalDistance.toString(),
            details.tripDestAddress1,
            null,
            null,
            isCarPark = details.isDestinationCarpark,
            carParkID = details.carParkId
        )
        val source = DestinationAddressDetails(
            null,
            details.tripStartAddress1,
            details.tripStartAddress2,
            details.tripStartLat,
            details.tripStartLong,
            details.totalDistance.toString(),
            details.tripStartAddress1,
            null,
            null
        )
        val args = Bundle()
        args.putSerializable(Constants.ROUTE_PLANNING_TRIP_DETAILS, details)
        args.putSerializable(Constants.ROUTE_PLANNING_MODE, RoutePlanningMode.PLANNED_RESUME)
        args.putSerializable(Constants.DESTINATION_ADDRESS_DETAILS, address)
        args.putSerializable(Constants.SOURCE_ADDRESS_DETAILS, source)
        (activity as DashboardActivity).addFragment(
            RoutePlanningFragment(),
            args,
            Constants.TAGS.ROUTE_PLANNING
        )
    }

    private fun setVehicleDetailsFragmentResultListener() {
        childFragmentManager.setFragmentResultListener(
            Constants.VEHICLE_SELECTION_FRAGMENT_REQUEST_KEY,
            this
        ) { _, bundle ->
            if (view == null) {
                return@setFragmentResultListener
            }
            val obuDataList = activity?.getApp()?.obuConnectionHelper?.getUserConnectedOBUDevices()
            val lastSelectedVehicle = selectedOBU
            val selectedOBUVehicleNumber =
                bundle.getString(Constants.VEHICLE_NUMBER)
            selectedOBU =
                obuDataList?.find { it.obuName == selectedOBUVehicleNumber || selectedOBUVehicleNumber == it.vehicleNumber }
            selectedOBU?.let {
                viewBinding.bSelection.text = it.getDisplayedVehicleName()
                if (lastSelectedVehicle?.obuName == it.obuName) {
                    return@setFragmentResultListener
                }

                if (mViewModel.transactionStartDate != null && mViewModel.transactionEndDate != null)
                    refreshTripObuTransactionData(
                        it.obuName,
                        startDate = mViewModel.transactionStartDate?.formatDate(TimeConst.yyyyMMdd),
                        endDate = mViewModel.transactionEndDate?.formatDate(TimeConst.yyyyMMdd)
                    ) else {
                    refreshTripObuTransactionData(it.obuName)
                }
            } ?: run {
                canShowTransactionInfoExplain =  false
                if (tabPosition == TAB_POSITION_TRANSACTION)
                    viewBinding.llTransactionInfo.isVisible = canShowTransactionInfoExplain
            }

        }
    }


    private fun initTab() {
        with(viewBinding) {
            tabLayout.addOnTabSelectedListener(object :
                TabLayout.OnTabSelectedListener {
                override fun onTabSelected(tab: TabLayout.Tab) {
                    tabPosition = tab.position
                    when (tabPosition) {
                        TAB_POSITION_HISTORY -> {
                            Analytics.logToggleEvent(Event.TRIP_LOG_HISTORY, Screen.TRIP_LOG)
                            if (isSelectViewEnabled) {
                                selectCheckbox.visibility = View.VISIBLE
                                emailView.visibility = View.VISIBLE
                            }
                            //                    select.visibility = View.VISIBLE
                            divider.visibility = View.VISIBLE
                            divider2.visibility = View.GONE
                            tripRecyclerview.visibility = View.VISIBLE
                            tripObuTransactionRecyclerview.visibility = View.GONE
                            tripPlannerRecyclerview.visibility = View.GONE
                            llObuPairedSelection.visibility = View.GONE
                            llTransactionInfo.isVisible = false
                            tvFilter.isVisible = false
                            tvDateRange.text = mViewModel.getHistoryDateRangeStr()
                        }

                        TAB_POSITION_UPCOMING -> {
                            Analytics.logToggleEvent(Event.TRIP_LOG_UPCOMING, Screen.TRIP_LOG)
                            //                    select.visibility = View.GONE
                            llObuPairedSelection.visibility = View.GONE
                            selectCheckbox.visibility = View.GONE
                            tripRecyclerview.visibility = View.GONE
                            tripOBUTransactionAdapter
                            emailView.visibility = View.GONE
                            divider.visibility = View.VISIBLE
                            divider2.visibility = View.GONE
                            tripPlannerRecyclerview.visibility = View.VISIBLE
                            tripObuTransactionRecyclerview.visibility = View.GONE
                            //back_to_top_view.visibility = View.GONE
                            tvFilter.isVisible = false
                            llTransactionInfo.isVisible = false
                            tvDateRange.text = mViewModel.getUpcomingDateRangeStr()
                        }

                        else -> {
                            Analytics.logClickEvent(Event.TRIP_LOG_TRANSACTION, Screen.OBU_TRIP_LOG)
                            search.visibility = View.GONE
                            llObuPairedSelection.visibility = View.VISIBLE
                            emailView.visibility = View.GONE
                            selectCheckbox.visibility = View.GONE
                            divider.visibility = View.GONE
                            divider2.visibility = View.VISIBLE
                            //                    select.visibility = View.VISIBLE
                            tripRecyclerview.visibility = View.GONE
                            tripPlannerRecyclerview.visibility = View.GONE
                            tripObuTransactionRecyclerview.visibility = View.VISIBLE
                            tvFilter.isVisible = true
                            llTransactionInfo.isVisible = canShowTransactionInfoExplain
                            tvDateRange.text = mViewModel.getTransactionDateRangeStr()
                        }
                    }

                    manageFeatureText()

                    /**
                     * always expand appbar
                     */
                    Handler(Looper.getMainLooper()).postDelayed({
                        appbarLayout.isActivated = true
                        appbarLayout.setExpanded(true, true);
                    }, 200)

                }

                override fun onTabUnselected(tab: TabLayout.Tab) {}
                override fun onTabReselected(tab: TabLayout.Tab) {}
            })
            val startDate: String? = arguments?.getString(PARAM_START_DATE)
            val endDate: String? = arguments?.getString(PARAM_END_DATE)

            mViewModel.transactionDataType =
                TransactionFilter.fromValue(arguments?.getString(PARAM_TRANSACTION_DATA_TYPE))
            viewBinding.tvFilter.text = getTransactionFilterText(mViewModel.transactionDataType)

            when (tabPosition) {
                TAB_POSITION_UPCOMING -> {
                    mViewModel.upcomingStartDate = startDate?.toDate(TimeConst.yyyyMMdd)
                    mViewModel.upcomingEndDate = endDate?.toDate(TimeConst.yyyyMMdd)
                }

                TAB_POSITION_HISTORY -> {
                    mViewModel.historyStartDate = startDate?.toDate(TimeConst.yyyyMMdd)
                    mViewModel.historyEndDate = endDate?.toDate(TimeConst.yyyyMMdd)
                    currentAnalyticsEventValue = Event.SUMMARY_TAB_DRIVING_TIME_READ_MORE_BACK
                }

                TAB_POSITION_TRANSACTION -> {
                    mViewModel.transactionStartDate = startDate?.toDate(TimeConst.yyyyMMdd)
                    mViewModel.transactionEndDate = endDate?.toDate(TimeConst.yyyyMMdd)
                    currentAnalyticsEventValue = when (mViewModel.transactionDataType) {
                        TransactionFilter.ERP_ONLY -> Event.SUMMARY_TAB_TOTAL_ERP_READ_MORE_BACK
                        TransactionFilter.PARKING_ONLY -> Event.SUMMARY_TAB_TOTAL_PARKING_FEE_READ_MORE_BACK
                        else -> ""
                    }
                }
            }
            tabLayout.selectTab(viewBinding.tabLayout.getTabAt(tabPosition))
        }
    }

    private fun viewToggle() {
        if (isSelectViewEnabled) {
            Analytics.logClickEvent(Event.TRAVEL_LOG_CANCEL_SELECT, getScreenName())
            isSelectViewEnabled = false
            viewBinding.select.text = getString(R.string.travel_log_select)
            viewBinding.selectCheckbox.visibility = View.GONE
            viewBinding.selectCheckbox.isChecked = false
//            if (back_to_top_view.visibility == View.VISIBLE) {
//                back_to_top_view.visibility = View.VISIBLE
//            } else {
//                back_to_top_view.visibility = View.GONE
//            }
            viewBinding.emailView.visibility = View.GONE
            toggleSelectCheckBoxVisibility()

        } else {
            Analytics.logClickEvent(Event.TRAVEL_LOG_ENABLE_SELECT, getScreenName())
            isSelectViewEnabled = true
            viewBinding.select.text = getString(R.string.travel_log_cancel)
            viewBinding.selectCheckbox.visibility = View.VISIBLE
            viewBinding.selectCheckbox.isChecked = false
            //back_to_top_view.visibility = View.GONE
            viewBinding.emailView.visibility = View.VISIBLE
            toggleSelectCheckBoxVisibility()
        }
    }

    private fun resetSelectedList() {
        mViewModel.selectRecordsOfMonth(travelHistoryAdapter, false)
        mViewModel.clearSelectedList()
    }

    private fun toggleSelectCheckBoxVisibility() {
        travelHistoryAdapter.setCheckBoxVisibility(isSelectViewEnabled)
        //adapter.notifyDataSetChanged()
        //tripRecyclerview.adapter = adapter
        resetSelectedList()

        //disable swipe delete option when Select mode is ON
        if (isSelectViewEnabled) {
            historyItemTouchHelper?.attachToRecyclerView(null)
        } else {
            historyItemTouchHelper?.attachToRecyclerView(viewBinding.tripRecyclerview)
        }
    }

    private fun deleteButton(position: Int): SwipeHelper.UnderlayButton {
        return SwipeHelper.UnderlayButton(
            requireContext(),
            "Delete",
            14.0f,
            object : SwipeHelper.UnderlayButtonClickListener {
                override fun onClick() {
                    val tripID = upcomingTripsAdapter.getAdapterItem(position).tripPlannerId
                    mViewModel.deleteTrips(tripID)
                    Analytics.logClickEvent(Event.DELETE, getScreenName())
                }
            })
    }

    private fun clearAndFetchUpcomingTrips() {
        lifecycleScope.launch {
            upcomingTripsAdapter.submitData(PagingData.empty())
        }
        refreshTripPlannerData()
    }

    private fun historyDeleteButton(position: Int): SwipeHelper.UnderlayButton {
        return SwipeHelper.UnderlayButton(
            requireContext(),
            "Delete",
            14.0f,
            object : SwipeHelper.UnderlayButtonClickListener {
                override fun onClick() {
                    val tripID = travelHistoryAdapter.getAdapterItem(position).tripLogItem.tripId
                    mViewModel.deleteTripHistory(tripID)
                    Analytics.logClickEvent(Event.DELETE, getScreenName())
                }
            })
    }

    private fun clearAndFetchTripHistory() {
        lifecycleScope.launch {
            travelHistoryAdapter.submitData(PagingData.empty())
        }
        refreshTripHistoryData()
    }

    private fun clearAndFetchTripObuTransaction() {
        lifecycleScope.launch {
            travelHistoryAdapter.submitData(PagingData.empty())
        }
        refreshTripHistoryData()
    }

    private fun initTripHistoryAdapter() {
        historyItemTouchHelper?.attachToRecyclerView(viewBinding.tripRecyclerview)

        val decoration = DividerItemDecoration(requireContext(), DividerItemDecoration.VERTICAL)
        viewBinding.tripRecyclerview.addItemDecoration(decoration)
        viewBinding.tripRecyclerview.adapter = travelHistoryAdapter
        viewBinding.tripRecyclerview.apply {
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
        }

        refreshTripHistoryData(
            startDate = mViewModel.historyStartDate?.formatDate(TimeConst.yyyyMMdd),
            endDate = mViewModel.historyEndDate?.formatDate(TimeConst.yyyyMMdd)
        )
    }


    private fun initTripObuTransactionListenersAndAdapter() {

        val decoration = DividerItemDecoration(requireContext(), DividerItemDecoration.VERTICAL)
        viewBinding.tripObuTransactionRecyclerview.addItemDecoration(decoration)
        viewBinding.tripObuTransactionRecyclerview.adapter = tripOBUTransactionAdapter
        viewBinding.tripObuTransactionRecyclerview.apply {
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
        }
        initObuTransactionDataAndListeners()
    }

    private fun initObuTransactionDataAndListeners() {
        loadObuDevices()

        mViewModel.vehicleDetailsResponseEvent.observe(viewLifecycleOwner) { obuVehicleResponse ->
            obuVehicleResponse?.let {
                pairedVehicleData = it.data
            }
        }

        viewBinding.tvObuPair.setOnClickListener {
            Analytics.logClickEvent(Event.OBU_TRANSACTIONS_TAB_PAIR, Screen.OBU_TRIP_LOG)
//            activity?.let {
//                (it as DashboardActivity).openConnectOBUDeviceScreen()
//            }

            activity?.getApp()?.shareBusinessLogicHelper?.backToHomepage(LandingMode.OBU.mode)
        }

        viewBinding.bSelection.setOnClickListener {
            onSelectVehicleMenu()
        }

        observeOBUConnectionStateChanged()
    }

    private fun loadObuDevices() {
        val obuDataList = activity?.getApp()?.obuConnectionHelper?.getUserConnectedOBUDevices()
        obuDataList?.let { obuList ->
            if (obuList.isNotEmpty()) {
                val selectedFromRN = arguments?.getString(PARAM_TRANSACTION_VEHICLE_NUMBER)
                selectedOBU = selectedFromRN?.let {
                    obuList.find { obu ->
                        obu.vehicleNumber == it
                    }
                } ?: obuList.last()

                viewBinding.bSelection.text = selectedOBU?.getDisplayedVehicleName()

                selectedOBU?.let {
                    refreshTripObuTransactionData(
                        it.obuName,
                        startDate = mViewModel.transactionStartDate?.formatDate(TimeConst.yyyyMMdd),
                        endDate = mViewModel.transactionEndDate?.formatDate(TimeConst.yyyyMMdd)
                    )
                }

                val obuNames = obuList.map {
                    return@map it.obuName
                }
                mViewModel.getObuVehicleList(obuNames)

            }
        }
    }


    private fun observeOBUConnectionStateChanged() {
        activity?.getApp()?.obuConnectionHelper?.obuConnectionState?.observe(viewLifecycleOwner) { state ->
            Timber.d("Travel Log observeOBUConnectionStateChanged: $state")
            when (state) {
                OBUConnectionState.CONNECTED -> {
                    loadObuDevices()
                    manageFeatureText()
                }

                else -> {
                    // No Operation
                }
            }
        }
    }

    private fun initUpcomingTripAdapter() {

        with(viewBinding) {
            val decoration = DividerItemDecoration(requireContext(), DividerItemDecoration.VERTICAL)
            tripPlannerRecyclerview.addItemDecoration(decoration)
            tripPlannerRecyclerview.adapter = upcomingTripsAdapter

            val itemTouchHelper = ItemTouchHelper(object : SwipeHelper(tripPlannerRecyclerview) {
                override fun instantiateUnderlayButton(position: Int): List<UnderlayButton> {
                    return listOf(deleteButton(position))
                }

            })

            itemTouchHelper.attachToRecyclerView(tripPlannerRecyclerview)

            tripPlannerRecyclerview.apply {
                layoutManager = LinearLayoutManager(context)
                setHasFixedSize(true)
            }

            refreshTripPlannerData(
                startDate = mViewModel.upcomingStartDate?.formatDate(TimeConst.yyyyMMdd),
                endDate = mViewModel.upcomingEndDate?.formatDate(TimeConst.yyyyMMdd)
            )
        }
    }

    private fun manageFeatureText() {
        with(viewBinding) {
            if (tabPosition == TAB_POSITION_HISTORY) {
                emptyView.text = resources.getString(R.string.travel_log_trip_history_empty)
                emptyView.visibility = (if (isTripHistoryFeatureUsed) View.GONE else View.VISIBLE)
                rlObuPair.visibility = View.GONE
            } else if (tabPosition == TAB_POSITION_UPCOMING) {
                emptyView.text = resources.getString(R.string.travel_log_upcoming_trips_empty)
                emptyView.visibility =
                    (if (isUpComingTripsFeatureUsed) View.GONE else View.VISIBLE)
                rlObuPair.visibility = View.GONE
            } else {
                if (selectedOBU == null) {
                    emptyView.text = resources.getString(R.string.travel_log_obu_no_devices)
                    rlObuPair.visibility = View.VISIBLE
                    emptyView.visibility = View.VISIBLE
                    llObuPairedSelection.visibility = View.GONE
                    divider.visibility = View.VISIBLE
                    divider2.visibility = View.GONE
                } else {
                    if (isObuTransactionsEmpty) {
                        emptyView.text = if (mViewModel.transactionDataType != TransactionFilter.ALL
                            || mViewModel.isTransactionHasDateFilter()
                        ) {
                            resources.getString(R.string.no_transactions_found)
                        } else {
                            resources.getString(R.string.travel_log_obu_no_transactions)
                        }
                        emptyView.visibility = View.VISIBLE
                    } else {
                        emptyView.visibility = View.GONE
                    }
                    rlObuPair.visibility = View.GONE
                    llObuPairedSelection.visibility = View.VISIBLE
                    divider.visibility = View.GONE
                    divider2.visibility = View.VISIBLE

                }
            }
        }
    }

    private fun refreshTripHistoryData(
        startDate: String? = null,
        endDate: String? = null,
        searchText: String? = null
    ) {
        handleShowLoadingProgress()
        lifecycleScope.launch {
            travelHistoryAdapter.submitData(PagingData.empty())
            mViewModel.getTripHistoryListStream(startDate ?: "", endDate ?: "", searchText ?: "")
                .catch {
                    Timber.e("Error loading trip history data")
                    handleShowLoadingProgress(false)
                }
                .collectLatest {
//                    if (it != null) {
                    handleShowLoadingProgress(false)
                    travelHistoryAdapter.submitData(it)
//                    } else
//                        select.visibility = View.GONE
                }
        }
    }


    private fun refreshTripObuTransactionData(
        obuName: String,
        startDate: String? = null,
        endDate: String? = null
    ) {
        handleShowLoadingProgress()
        lifecycleScope.launch {
            tripOBUTransactionAdapter.submitData(PagingData.empty())
            mViewModel.getTripObuTransactionsListStream(
                obuName,
                startDate ?: "",
                endDate ?: ""
            )
                .catch {
                    Timber.e("Error loading trip history data")
                    handleShowLoadingProgress(false)
                }
                .collectLatest {
                    handleShowLoadingProgress(false)
                    tripOBUTransactionAdapter.submitData(it)
                }
        }
    }

    private fun handleShowLoadingProgress(isShow: Boolean = true) {
        if (isShow) {
            viewBinding.progressBarTravelLog.visibility = View.VISIBLE
        } else {
            viewBinding.progressBarTravelLog.visibility = View.GONE
        }

    }


    private fun setTravelLogFragmentResultListener() {
        setFragmentResultListener(Constants.TRAVEL_LOG_FRAGMENT_REQUEST_KEY) { requestKey, bundle ->
            refreshTripPlannerData(
                searchText = mViewModel.lastTripPlanSearchQuery,
                startDate = mViewModel.upcomingStartDate?.formatDate(TimeConst.yyyyMMdd),
                endDate = mViewModel.upcomingEndDate?.formatDate(TimeConst.yyyyMMdd)
            )
        }
    }


    private fun refreshTripPlannerData(
        startDate: String? = null,
        endDate: String? = null,
        searchText: String? = null
    ) {
        handleShowLoadingProgress()
        lifecycleScope.launch {
            upcomingTripsAdapter.submitData(PagingData.empty())
            mViewModel.getTripPlannerListStream(startDate ?: "", endDate ?: "", searchText ?: "")
                .catch {
                    Timber.e("Error loading trip planner data")
                    handleShowLoadingProgress(false)
                }
                .collect { data ->
                    handleShowLoadingProgress(false)
                    upcomingTripsAdapter.submitData(data)
                }
        }
    }


    private fun onSelectVehicleMenu() {
        if (SystemClock.elapsedRealtime() - lastClickTime < 1000) {
            return
        }
        pairedVehicleData?.let {
            val bundle = Bundle()
            bundle.putSerializable(Constants.VEHICLE_NUMBERS, ArrayList(pairedVehicleData!!))
            val obuVehicleSelectionFragment = ObuVehicleSelectionFragment()
            obuVehicleSelectionFragment.arguments = bundle
            obuVehicleSelectionFragment.show(
                childFragmentManager,
                Constants.OBU_SELECTION_FRAGMENT_NAME
            )
        }

    }

    private fun showEmptyView(isListEmpty: Boolean) {
//        if (isListEmpty) {
//            back_to_top_button.visibility = View.GONE
//        } else {
//            back_to_top_button.visibility = View.VISIBLE
//        }
    }


    override fun inflateViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?,
        attachToContainer: Boolean
    ) = FragmentTravelLogBinding.inflate(inflater, container, attachToContainer)

    override fun getViewModelReference(): TravelLogViewModel {
        return mViewModel
    }

    var lastClickTime: Long = 0
    override fun onItemClick(item: TripsItem) {
        if (SystemClock.elapsedRealtime() - lastClickTime < 1000) {
            return
        }
        Analytics.logClickEvent(Event.TRAVEL_LOG_VIEW_SINGLE_TRIP, getScreenName())
        mViewModel.travelSummaryResult.observe(this) {
            mViewModel.travelSummaryResult.removeObservers(this)
            lastClickedItem = item
            val bundle = Bundle()
            bundle.putSerializable(Constants.TRIP_SUMMARY_DETAILS, it)
            (requireActivity() as DashboardActivity).addFragmentBottomUp(
                TripInfoFragment(),
                bundle,
                Constants.TAGS.TRAVEL_LOG_TAG
            )
        }
        mViewModel.getTripSummary(item.tripId)
        lastClickTime = SystemClock.elapsedRealtime()
    }

    override fun onItemSelected(isSelected: Boolean, tripId: Long) {
        mViewModel.selectItem(isSelected, tripId)
        with(viewBinding) {
            if (!isSelected && selectCheckbox.isChecked) {
                selectCheckbox.setOnCheckedChangeListener(null)
                selectCheckbox.isChecked = false
                mViewModel.setCurrentMonthSelect(false)
                selectCheckbox.setOnCheckedChangeListener(monthSelectCheckedChangeListener)
            }
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun updateList(itemId: Long, totalExpense: Double) {
        if (lastClickedItem != null && lastClickedItem!!.tripId == itemId) {
            Analytics.logClickEvent(Event.TRAVEL_LOG_INDIVIDUAL_TRIP_SELECT, getScreenName())
            lastClickedItem!!.totalExpense = totalExpense
        }
        travelHistoryAdapter.notifyDataSetChanged()
    }

    override fun onResume() {
        super.onResume()
        refreshTripPlannerData(
            searchText = "",
            startDate = mViewModel.upcomingStartDate?.formatDate(TimeConst.yyyyMMdd),
            endDate = mViewModel.upcomingEndDate?.formatDate(TimeConst.yyyyMMdd)
        )
        (activity as DashboardActivity?)!!.setTravelLogListener(this)
//        if (!isHistoryTab)
//            select.visibility = View.GONE
    }

    override fun onStop() {
        super.onStop()
        (activity as DashboardActivity?)!!.setTravelLogListener(null)
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.dispose()
        (activity as DashboardActivity?)!!.setTravelLogListener(null)
    }

    override fun onDestroyView() {
        activity?.getApp()?.obuConnectionHelper?.obuConnectionState?.removeObservers(
            viewLifecycleOwner
        )
        super.onDestroyView()
    }


    private fun showSuccessDialog(noOfTrips: Int) {

        dialog = activity?.let {
            Dialog(it)
        }!!

        dialog.setContentView(R.layout.dialog_email_sent_successfully)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCanceledOnTouchOutside(false)

        val greeting = dialog.findViewById<TextView>(R.id.greeting)
        val title = dialog.findViewById<TextView>(R.id.title)

        greeting.text = String.format(
            "%d %s",
            noOfTrips,
            resources.getQuantityString(R.plurals.email_sent, noOfTrips)
        )
        title.text = resources.getQuantityString(R.plurals.travel_summary_sent, noOfTrips)

        val button = dialog.findViewById<BreezeButton>(R.id.button_okay)
        button.setOnClickListener {
            Analytics.logClosePopupEvent(Event.EMAIL_SUMMARY_POPUP, getScreenName())
//            select.performClick()
            dialog.dismiss()
        }



        Analytics.logOpenPopupEvent(Event.EMAIL_SUMMARY_POPUP, getScreenName())
        dialog.show()

        val displayRectangle = Rect()
        val window: Window = requireActivity().window
        window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle)

        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window!!.getAttributes())
        lp.width = (displayRectangle.width() * 0.9f).toInt()
        dialog.window!!.setAttributes(lp)
    }

    override fun getScreenName(): String {
        return Screen.TRIP_LOG
    }

    private fun showDateRangeSelector(startDate: String?, endDate: String?) {
        viewBinding.searchView.clearFocus()
        Analytics.logClickEvent(Event.TRAVEL_LOG_MONTH_YEAR_SELECT, getScreenName())
        if (tabPosition == TAB_POSITION_TRANSACTION) {
            Analytics.logClickEvent(Event.TRANSACTION_FILTER_DATE, getScreenName())
        }
        (activity as? DashboardActivity)?.showDatePickerDialog(startDate, endDate, tabPosition != TAB_POSITION_UPCOMING)
    }

    private fun handleDateRangeSelected(startDate: String, endDate: String) {
        //mViewModel.setCurrentMonthSelect(false)
        when (tabPosition) {
            TAB_POSITION_UPCOMING -> {
                // upcoming tab is focused
                mViewModel.upcomingStartDate = startDate.toDate(TimeConst.yyyyMMdd)
                mViewModel.upcomingEndDate = endDate.toDate(TimeConst.yyyyMMdd)
                viewBinding.tvDateRange.text = mViewModel.getUpcomingDateRangeStr()
                if (startDate.isNotEmpty() && endDate.isNotEmpty())
                    refreshTripPlannerData(
                        searchText = "",
                        startDate = mViewModel.upcomingStartDate?.formatDate(TimeConst.yyyyMMdd),
                        endDate = mViewModel.upcomingEndDate?.formatDate(TimeConst.yyyyMMdd)
                    )
                else
                    refreshTripPlannerData(
                        searchText = "",
                        startDate = "",
                        endDate = ""
                    )
            }

            TAB_POSITION_HISTORY -> {
                mViewModel.historyStartDate = startDate.toDate(TimeConst.yyyyMMdd)
                mViewModel.historyEndDate = endDate.toDate(TimeConst.yyyyMMdd)
                viewBinding.tvDateRange.text = mViewModel.getHistoryDateRangeStr()
                if (startDate.isNotEmpty() && endDate.isNotEmpty())
                    refreshTripHistoryData(
                        searchText = "",
                        startDate = mViewModel.historyStartDate?.formatDate(TimeConst.yyyyMMdd),
                        endDate = mViewModel.historyEndDate?.formatDate(TimeConst.yyyyMMdd)
                    )
                else
                    refreshTripHistoryData(
                        searchText = "",
                        startDate = "",
                        endDate = ""
                    )
            }

            TAB_POSITION_TRANSACTION -> {
                mViewModel.transactionStartDate = startDate.toDate(TimeConst.yyyyMMdd)
                mViewModel.transactionEndDate = endDate.toDate(TimeConst.yyyyMMdd)
                viewBinding.tvDateRange.text = mViewModel.getTransactionDateRangeStr()
                selectedOBU?.let {
                    if (startDate.isNotEmpty() && endDate.isNotEmpty())
                        refreshTripObuTransactionData(
                            it.obuName,
                            startDate = mViewModel.transactionStartDate?.formatDate(TimeConst.yyyyMMdd),
                            endDate = mViewModel.transactionEndDate?.formatDate(TimeConst.yyyyMMdd)
                        )
                    else
                        refreshTripObuTransactionData(
                            it.obuName,
                            startDate = "",
                            endDate = ""
                        )
                }
            }
        }

    }

    //temp - Ankur
    //Call this Method when a upcoming trip is created or deleted
    protected fun onUpcomingTripListUpdated(upcomingTripCount: Int) {
        App.instance?.run {
            compositeDisposable.add(
                shareBusinessLogicPackage.getRequestsModule()
                    .flatMap {
                        val rnData = Arguments.createMap().apply {
                            putBoolean("hasUpcomingTrips", upcomingTripCount > 0)
                        }
                        it.callRN(
                            ShareBusinessLogicEvent.FN_UPDATE_UPCOMING_TRIPS.eventName,
                            rnData
                        )
                    }
                    .subscribeOn(Schedulers.io())
                    .subscribe()
            )
        }
    }

    private var upComingLastClickTime: Long = 0
    override fun onItemClick(item: TripPlannerItem) {
        if (SystemClock.elapsedRealtime() - upComingLastClickTime < 1000) {
            return
        }
        mViewModel.upcomingTripDetails.observe(this, Observer {
            mViewModel.upcomingTripDetails.removeObservers(this)
            openRoutePlanningScreen(it)
        })
        lifecycleScope.launch {
            mViewModel.getUpcomingTripDetails(item.tripPlannerId)
        }
        upComingLastClickTime = SystemClock.elapsedRealtime()

    }

    override fun onItemClick(item: TripObuTransactionItem) {
        // nothing to do
    }

    override fun onBackPressed() {
        super.onBackPressed()
        if (currentAnalyticsEventValue.isNotEmpty()) {
            Analytics.logClickEvent(Screen.DRIVE_TREND, currentAnalyticsEventValue)
        }
    }

    override fun onFilterSelected(filter: TransactionFilter) {
        mViewModel.transactionDataType = filter
        viewBinding.tvFilter.text = getTransactionFilterText(filter)
        selectedOBU?.let {
            viewBinding.bSelection.text = it.getDisplayedVehicleName()

            if (mViewModel.transactionStartDate != null && mViewModel.transactionEndDate != null)
                refreshTripObuTransactionData(
                    it.obuName,
                    startDate = mViewModel.transactionStartDate?.formatDate(TimeConst.yyyyMMdd),
                    endDate = mViewModel.transactionEndDate?.formatDate(TimeConst.yyyyMMdd)
                ) else {
                refreshTripObuTransactionData(it.obuName)
            }
        }
    }

    private fun getTransactionFilterText(filter: TransactionFilter) = getString(
        when (filter) {
            TransactionFilter.ERP_ONLY -> R.string.erp_only
            TransactionFilter.PARKING_ONLY -> R.string.parking_only
            else -> R.string.all
        }
    )

    private fun openFilterDialog() {
        TransactionFilterDialogFragment().show(childFragmentManager, null)
    }

    companion object {
        private const val TAB_POSITION_HISTORY = 0
        private const val TAB_POSITION_TRANSACTION = 1
        private const val TAB_POSITION_UPCOMING = 2

        private const val PARAM_TAB_POSITION = "PARAM_TAB_POSITION"
        private const val PARAM_START_DATE = "PARAM_START_DATE"
        private const val PARAM_END_DATE = "PARAM_END_DATE"
        private const val PARAM_TRANSACTION_DATA_TYPE = "PARAM_TRANSACTION_DATA_TYPE"
        private const val PARAM_TRANSACTION_VEHICLE_NUMBER = "PARAM_TRANSACTION_VEHICLE_NUMBER"

        internal fun createFragment(navigationParams: ReadableMap? = null): TravelLogFragment {
            val tripLogTab: String? = navigationParams?.getString("tripLogTab")
            val selectedWeek: String? = navigationParams?.getString("selectedWeek")
            val vehicleNumber: String? = navigationParams?.getString("vehicleNumber")
            val filter: String? = navigationParams?.getString("filter")
            var startDate: String? = null
            var endDate: String? = null
            selectedWeek?.let {
                val arr = it.split("-")
                if (arr.size >= 2) {
                    startDate = arr[0].changeStringDateFormat("yyyyMMdd", TimeConst.yyyyMMdd)
                    endDate = arr[1].changeStringDateFormat("yyyyMMdd", TimeConst.yyyyMMdd)
                }
                if (startDate == null || endDate == null) {
                    startDate = null
                    endDate = null
                }
            }
            return when (tripLogTab) {
                "history" -> newInstanceTabHistory(startDate, endDate)
                "transactions" -> newInstanceTabTransaction(
                    startDate,
                    endDate,
                    filter,
                    vehicleNumber
                )

                else -> newInstance()
            }

        }

        private fun newInstance(): TravelLogFragment {
            return TravelLogFragment()
        }

        fun newInstanceTabUpcoming(): TravelLogFragment {
            return TravelLogFragment().apply {
                arguments = Bundle().apply {
                    putInt(PARAM_TAB_POSITION, TAB_POSITION_UPCOMING)
                }
            }
        }

        /***
         *  Time format yyyy-MM-dd
         */
        private fun newInstanceTabHistory(
            startDate: String? = null,
            endDate: String? = null
        ): TravelLogFragment {
            return TravelLogFragment().apply {
                arguments = Bundle().apply {
                    putInt(PARAM_TAB_POSITION, TAB_POSITION_HISTORY)
                    putString(PARAM_START_DATE, startDate)
                    putString(PARAM_END_DATE, endDate)
                }

            }
        }


        /***
         *  Time format yyyy-MM-dd
         */
        private fun newInstanceTabTransaction(
            startDate: String? = null,
            endDate: String? = null,
            filter: String? = null,
            vehicleNumber: String? = null
        ): TravelLogFragment {
            return TravelLogFragment().apply {
                arguments = Bundle().apply {
                    putInt(PARAM_TAB_POSITION, TAB_POSITION_TRANSACTION)
                    putString(PARAM_START_DATE, startDate)
                    putString(PARAM_END_DATE, endDate)
                    putString(PARAM_TRANSACTION_VEHICLE_NUMBER, vehicleNumber)
                    putString(PARAM_TRANSACTION_DATA_TYPE, filter)
                }

            }
        }
    }

    @Retention(AnnotationRetention.SOURCE)
    @IntDef(TAB_POSITION_UPCOMING, TAB_POSITION_TRANSACTION, TAB_POSITION_HISTORY)
    private annotation class TabPos
}
package com.ncs.breeze.common.appSync

/**
 *  Pattern for value must match the following regex :
 *  "^[_a-zA-Z0-9]+$";
 *
 *  The name is used as the cache prefix used by appsync and if the pattern does not match as above it will throw an error
 *
 *  OBU_SIMULATED/OBU_REAL both use the default connection to avoid extra connections (and extra aws pricing charges), since they don't get used together
 *  Warning: dont use both OBU_SIMULATED and OBU_REAL together
 *
 */
object MutationClientTypes {

    /**
     * Used for simulated obu for mock location data
     */
    public const val OBU_SIMULATED = "DEFAULT"

    /**
     * used for real obu connected via bluetooth
     */
    public const val OBU_REAL = "DEFAULT"

    /**
     *  used to publish user's live location for ETA
     */
    public const val ETA_LIVE_LOCATION = "ETA_LIVE_LOCATION"

    /**
     *  used for users live location for walkathons
     */
    public const val WALKATHON_LIVE_LOCATION = "WALKATHON_LIVE_LOCATION"

}
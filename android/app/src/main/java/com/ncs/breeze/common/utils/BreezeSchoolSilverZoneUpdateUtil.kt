package com.ncs.breeze.common.utils

import android.content.Context
import com.google.gson.Gson
import com.google.gson.JsonElement
import com.ncs.breeze.common.extensions.android.getAppPreference
import com.breeze.model.api.ErrorData
import com.ncs.breeze.common.remote.ApiHelper
import com.ncs.breeze.common.remote.ApiObserver
import com.ncs.breeze.common.rxbus.RxBus
import com.ncs.breeze.common.rxbus.RxEvent
import com.ncs.breeze.common.storage.AppPrefsKey
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class BreezeSchoolSilverZoneUpdateUtil @Inject constructor(
    appContext: Context,
    apiHelper: ApiHelper,
) {

    var apiHelper: ApiHelper = apiHelper
    var appContext = appContext
    var compositeDisposable: CompositeDisposable = CompositeDisposable()
    var schoolSilverZoneEnabled: Boolean = false;

    fun startSchoolSilverZoneHandler() {
        compositeDisposable = CompositeDisposable()
        synchronized(this) {
            if (!schoolSilverZoneEnabled) {
                schoolSilverZoneEnabled = true
                subscribeToSchoolZoneRxEvent()
                subscribeToSilverZoneRxEvent()
                fetchSchoolZoneData()
                fetchSilverZoneData()
            }
        }
    }

    private fun fetchSchoolZoneData() {
        apiHelper.getSchoolZone()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : ApiObserver<JsonElement>(compositeDisposable) {
                override fun onSuccess(data: JsonElement) {
                    appContext.getAppPreference()?.saveString(
                        AppPrefsKey.SCHOOL_ZONE_RESPONSE,
                        Gson().toJson(data)
                    )
                    RxBus.publish(RxEvent.SchoolSilverZoneDataChange(true))
                }

                override fun onError(e: ErrorData) {
                    Timber.d("API failed: " + e.toString())
                }

            })
    }

    private fun fetchSilverZoneData() {
        apiHelper.getSilverZone()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : ApiObserver<JsonElement>(compositeDisposable) {
                override fun onSuccess(data: JsonElement) {
                    appContext.getAppPreference()?.saveString(
                        AppPrefsKey.SILVER_ZONE_RESPONSE,
                        Gson().toJson(data)
                    )
                    RxBus.publish(RxEvent.SchoolSilverZoneDataChange(true))
                }

                override fun onError(e: ErrorData) {
                    Timber.d("API failed: " + e.toString())
                }

            })
    }

    private fun subscribeToSchoolZoneRxEvent() {

        compositeDisposable.add(
            RxBus.listen(RxEvent.SchoolZoneAppSyncEvent::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    Timber.d("School zone event observed successfully")
                    appContext.getAppPreference()?.saveString(
                        AppPrefsKey.SCHOOL_ZONE_RESPONSE,
                        Gson().toJson(it.schoolZoneUpdated)
                    )
                    RxBus.publish(RxEvent.SchoolSilverZoneDataChange(true))
                }
        )
    }

    private fun subscribeToSilverZoneRxEvent() {

        compositeDisposable.add(
            RxBus.listen(RxEvent.SilverZoneAppSyncEvent::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    Timber.d("Silver zone event observed successfully")
                    appContext.getAppPreference()?.saveString(
                        AppPrefsKey.SILVER_ZONE_RESPONSE,
                        Gson().toJson(it.silverZoneDataUpdated)
                    )
                    RxBus.publish(RxEvent.SchoolSilverZoneDataChange(true))
                }
        )
    }

    fun stopSchoolSilverZoneHandler() {
        if (!compositeDisposable.isDisposed) {
            compositeDisposable.clear()
        }
        schoolSilverZoneEnabled = false
    }
}
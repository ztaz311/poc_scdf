package com.ncs.breeze.ui.dashboard.fragments.view

import android.animation.Animator
import android.content.res.Resources
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.viewbinding.ViewBinding
import com.mapbox.geojson.Geometry
import com.mapbox.geojson.GeometryCollection
import com.mapbox.geojson.LineString
import com.mapbox.maps.CameraOptions
import com.mapbox.maps.EdgeInsets
import com.mapbox.maps.MapboxMap
import com.mapbox.maps.extension.style.expressions.generated.Expression
import com.mapbox.maps.plugin.animation.MapAnimationOptions.Companion.mapAnimationOptions
import com.mapbox.maps.plugin.animation.easeTo
import com.mapbox.navigation.base.route.NavigationRoute
import com.mapbox.navigation.ui.maps.route.arrow.api.MapboxRouteArrowApi
import com.mapbox.navigation.ui.maps.route.arrow.api.MapboxRouteArrowView
import com.mapbox.navigation.ui.maps.route.arrow.model.RouteArrowOptions
import com.mapbox.navigation.ui.maps.route.line.api.MapboxRouteLineApi
import com.mapbox.navigation.ui.maps.route.line.api.MapboxRouteLineView
import com.mapbox.navigation.ui.maps.route.line.model.MapboxRouteLineOptions
import com.mapbox.navigation.ui.maps.route.line.model.RouteLineResources
import com.mapbox.navigation.ui.maps.route.line.model.RouteLineScaleValue
import com.ncs.breeze.R
import com.breeze.model.DestinationAddressDetails
import com.ncs.breeze.common.model.RouteAdapterTypeObjects
import com.breeze.model.constants.Constants
import com.mapbox.geojson.Point
import com.ncs.breeze.common.utils.mapStyle.BreezeRoutePlanningMapStyle
import com.ncs.breeze.components.SingleLiveEvent
import com.ncs.breeze.helper.animation.ViewAnimator
import com.ncs.breeze.ui.base.BaseFragment
import com.ncs.breeze.ui.base.BaseFragmentViewModel
import com.ncs.breeze.ui.dashboard.activity.DashboardActivity
import java.util.concurrent.CopyOnWriteArraySet


abstract class BaseRoutePlanningFragment<T : ViewBinding, V : BaseFragmentViewModel> :
    BaseFragment<T, V>() {


    private var dataLoadedObserver: Observer<Boolean> =
        Observer<Boolean> { t -> finishedLoading(t) }
    private val initializedListeners = CopyOnWriteArraySet<OnLoaded>()
    private lateinit var routeLineResources: RouteLineResources

    protected var isDataLoaded: SingleLiveEvent<Boolean> = SingleLiveEvent<Boolean>()

    /**
     * original destination, should remain the same even when a nearby car park is selected
     * */
    protected var originalDestinationDetails: DestinationAddressDetails? = null

    /**
     * the destination the user is travelling to, can be same as originalDestinationDetails in certain scenarios
     * */
    protected var currentDestinationDetails: DestinationAddressDetails? = null
    protected var sourceAddressDetails: DestinationAddressDetails? = null

    protected var routeLineAPI: MapboxRouteLineApi? = null
    protected val routeArrowAPI: MapboxRouteArrowApi = MapboxRouteArrowApi()
    protected var routeLineView: MapboxRouteLineView? = null
    private var routeArrowView: MapboxRouteArrowView? = null
    protected var zoomCameraOptions: CameraOptions? = null


    protected var maxZoom: Double = 15.0
    protected var destinationDrawable: Int? = null
    protected val pixelDensity = Resources.getSystem().displayMetrics.density

    protected var routeObjects: List<NavigationRoute>? = null
    protected var anotherPoints:List<Point> = listOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        isDataLoaded.observe(this, dataLoadedObserver)
    }


    protected abstract fun processRoutes(routeObjects: List<RouteAdapterTypeObjects>?)
    protected abstract fun clearRoutes()
    protected abstract fun displayERPIcons()
    protected abstract fun clearMarkerViews()


    /**
     * triggers after fragment data is loaded and parsed from bundle
     *
     */
    protected fun onInitialized(loadedListener: OnLoaded) {
        if (isDataLoaded.value == true) {
            loadedListener.onBundleLoaded()
        } else {
            initializedListeners.add(loadedListener)
        }
    }

    /**
     * triggers after fragment data is loaded and parsed from bundle
     *
     */
    private fun finishedLoading(initialized: Boolean?) {
        if (initialized == true) {
            isDataLoaded.removeObservers(viewLifecycleOwner)
            initializedListeners.forEach {
                it.onBundleLoaded()
            }
            initializedListeners.clear()
        }
    }

    protected fun initRouteLine() {
        showDestinationDrawable()
        updateRouteLineViews()
    }


    private fun updateRouteLineViews() {
        (activity as? DashboardActivity)?.let { dashboardActivity ->
            val routeLineResourcesBuilder =
                BreezeRoutePlanningMapStyle().generateRouteLineResourceBuilder(dashboardActivity)
            destinationDrawable?.let {
                routeLineResourcesBuilder.destinationWaypointIcon(it)
            }
            val routeLineOptions = MapboxRouteLineOptions.Builder(dashboardActivity)
                .withRouteLineResources(routeLineResourcesBuilder.build())
                .withRouteLineBelowLayerId(Constants.COUNTRY_LABEL)
                .build()
            routeLineAPI = MapboxRouteLineApi(routeLineOptions)
            routeLineView = MapboxRouteLineView(routeLineOptions)
            routeArrowView = MapboxRouteArrowView(
                RouteArrowOptions.Builder(dashboardActivity)
                    .build()
            )
        }

    }

    protected open fun showDestinationDrawable() {
        destinationDrawable = R.drawable.destination_puck
    }

    protected fun createNavigationRoute(routeObj: RouteAdapterTypeObjects): NavigationRoute? {
        return if (routeObj.routeData.route.routeOptions() == null) {
            null
        } else {
            NavigationRoute.create(
                routeObj.routeData.originalResponse,
                routeObj.routeData.route.routeOptions()!!
            ).find { it.routeIndex == routeObj.routeData.route.routeIndex()?.toInt() }
        }
    }

    protected fun resetCameraToRoutes(
        routeObjects: List<NavigationRoute>, mapboxMap: MapboxMap,
        bottomView: View, topView: View, rightView: View?, leftView: View?
    ) {
        this.routeObjects = routeObjects
        val geometryList = arrayListOf<Geometry>()
        var leftHeight = 0.0
        var rightHeight = 0.0

        for (routeObj in routeObjects) {
            val geometry = LineString.fromPolyline(
                routeObj.directionsRoute.geometry()!!,
                Constants.POLYLINE_PRECISION
            )

            geometryList.add(geometry)
        }
        anotherPoints.forEach {
            geometryList.add(it)
        }

        val geometryCollection = GeometryCollection.fromGeometries(geometryList)
//        ViewAnimator.measureView(bottomView)
//        ViewAnimator.measureView(topView)

        rightView?.let {
//            ViewAnimator.measureView(it)
            rightHeight = it.height.toDouble()
        }

        leftView?.let {
//            ViewAnimator.measureView(it)
            leftHeight = it.height.toDouble()
        }

        zoomCameraOptions = bestCameraOption(
            geometryCollection,
            topView.height.toDouble(),
            bottomView.height.toDouble(),
            leftHeight,
            rightHeight,
            mapboxMap
        )
        resetCameraZoom(mapboxMap)
    }


    protected fun resetCameraToRoutes(
        routeObjects: List<NavigationRoute>, mapboxMap: MapboxMap,
        bottomHeight: Double, topHeight: Double, rightWidth: Double, leftWidth: Double
    ) {
        val geometryList = arrayListOf<Geometry>()

        for (routeObj in routeObjects) {
            val geometry = LineString.fromPolyline(
                routeObj.directionsRoute.geometry()!!,
                Constants.POLYLINE_PRECISION
            )
            geometryList.add(geometry)
        }

        val geometryCollection = GeometryCollection.fromGeometries(geometryList)
        zoomCameraOptions = bestCameraOption(
            geometryCollection,
            topHeight,
            bottomHeight,
            leftWidth,
            rightWidth,
            mapboxMap
        )
        resetCameraZoom(mapboxMap)
    }

    protected fun resetCameraZoom(mapboxMap: MapboxMap) {
        zoomCameraOptions?.let {
            mapboxMap.easeTo(
                it,
                mapAnimationOptions {
                    duration(1000L)
                    animatorListener(object : Animator.AnimatorListener {
                        override fun onAnimationStart(animation: Animator) {}

                        override fun onAnimationEnd(animation: Animator) {}

                        override fun onAnimationCancel(animation: Animator) {}

                        override fun onAnimationRepeat(animation: Animator) {}

                    })
                }
            )
        }
    }

    private fun bestCameraOption(
        geometryCollection: GeometryCollection,
        routeMeasuredTopHeight: Double,
        routeMeasuredBottomHeight: Double,
        routeMeasuredLeftHeight: Double,
        routeMeasuredRightHeight: Double,
        mapboxMap: MapboxMap
    ): CameraOptions? {
        val bearingList = arrayListOf(
            0.0,
            9.0,
            18.0,
            27.0,
            36.0,
            45.0,
            54.0,
            63.0,
            72.0,
            81.0,
            90.0
        )
        var bestOption: CameraOptions? = null
        var bestZoom = 0.0

        for (bearing in bearingList) {
            val option = mapboxMap.cameraForGeometry(
                geometryCollection,
                EdgeInsets(
                    routeMeasuredTopHeight + (70.0 * pixelDensity),
                    routeMeasuredLeftHeight + (40.0 * pixelDensity),
                    routeMeasuredBottomHeight+ (20.0 * pixelDensity),
                    routeMeasuredRightHeight+ (20.0 * pixelDensity)
                ),
                bearing = bearing,
                0.0
            )
            if (option.zoom != null && option.zoom!! >= bestZoom) {
                bestZoom = option.zoom!!
                bestOption = option
            }
        }

        return bestOption
    }


    private fun buildScalingExpression(scalingValues: List<RouteLineScaleValue>): Expression {
        val expressionBuilder = Expression.ExpressionBuilder("interpolate")
        expressionBuilder.addArgument(Expression.exponential { literal(1.5) })
        expressionBuilder.zoom()
        scalingValues.forEach { routeLineScaleValue ->
            expressionBuilder.stop {
                this.literal(routeLineScaleValue.scaleStop.toDouble())
                product {
                    literal(routeLineScaleValue.scaleMultiplier.toDouble())
                    literal(routeLineScaleValue.scale.toDouble())
                }
            }
        }
        return expressionBuilder.build()
    }

    fun getCarParkDestinationLocation() = currentDestinationDetails

    fun interface OnLoaded {
        /**
         * Invoked when a style has finished loading.
         */
        fun onBundleLoaded()
    }

}



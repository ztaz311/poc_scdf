package com.ncs.breeze.common.constant

import android.Manifest
import android.os.Build
import com.ncs.breeze.BuildConfig

object OBUConstants {
    private const val OBU_CUSTOM_PERMISSION = "${BuildConfig.APPLICATION_ID}.permission.OBU_DATA_ACCESS"
    const val OBU_TIME_FORMAT = "yyyyMMddHHmmss"

    val OBU_PERMISSIONS = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
        arrayOf(
            Manifest.permission.BLUETOOTH_SCAN,
            Manifest.permission.BLUETOOTH_CONNECT,
            OBU_CUSTOM_PERMISSION
        )
    } else {
        arrayOf(OBU_CUSTOM_PERMISSION)
    }

    /**
     * OBU card balance in cent
     * */
    const val OBU_LOW_CARD_THRESHOLD  = 500

    /**
    * Duration in milliseconds from ERP charged successful to starting to show low card balance alert if conditions are matched
    * */
    const val OBU_LOW_CARD_ALERT_DELAY = 5000L

    /**
     * Mocked OBU's card balance in cent
     * */
    const val OBU_MOCK_CARD_BALANCE = 10000
}
package com.ncs.breeze.ui.dashboard.fragments.viewmodel

import android.app.Application
import com.amplifyframework.auth.AuthUserAttributeKey
import com.amplifyframework.core.Amplify
import com.ncs.breeze.common.remote.ApiHelper
import com.ncs.breeze.common.utils.BreezeUserPreferenceUtil
import com.facebook.react.bridge.UiThreadUtil.runOnUiThread
import com.ncs.breeze.common.utils.AWSUtils
import com.ncs.breeze.components.SingleLiveEvent
import com.ncs.breeze.ui.base.BaseFragmentViewModel
import javax.inject.Inject

class ChangeEmailViewModel @Inject constructor(apiHelper: ApiHelper, application: Application) :
    BaseFragmentViewModel(
        application
    ) {
    var mAPIHelper: ApiHelper = apiHelper

    @Inject
    lateinit var breezeUserPreferenceUtil: BreezeUserPreferenceUtil

    var triggeredConfirmationCode: SingleLiveEvent<Boolean> = SingleLiveEvent()
    var confirmEmailSuccessful: SingleLiveEvent<Boolean> = SingleLiveEvent()

    fun verifyEmailConfirmationCode(confirmationCode: String) {
        AWSUtils.confirmUserAttribute(
            AuthUserAttributeKey.email(),
            confirmationCode,
            {
                runOnUiThread {
                    confirmEmailSuccessful.value = true
                }
            },
            {
                runOnUiThread {
                    confirmEmailSuccessful.value = true
                }
            })
    }

    fun retriggerEmail() {
        AWSUtils.resendUserAttributeConfirmationCode(
            AuthUserAttributeKey.email(),
            {
                triggeredConfirmationCode.value = true
            }
        ) {
            triggeredConfirmationCode.value = false
        }
    }
}
package com.ncs.breeze.common.utils

import android.location.Location
import com.ncs.breeze.common.helper.route.RoutePlanningHelper
import com.breeze.model.ObjectFlowLoginAndSignUp

object InitObjectUtilsController {

    var mRoutePlanningHelper: RoutePlanningHelper? = null
    var currentLocation: Location? = null
    var cognitoUserID: String? = null
    var currentObjectFlowLoginSignUp: ObjectFlowLoginAndSignUp? = null
}
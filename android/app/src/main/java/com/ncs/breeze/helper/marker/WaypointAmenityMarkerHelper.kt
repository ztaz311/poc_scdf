package com.ncs.breeze.helper.marker

import com.breeze.model.api.response.amenities.BaseAmenity
import com.google.gson.JsonObject
import com.ncs.breeze.components.layermanager.MarkerLayerManager
import com.ncs.breeze.components.layermanager.impl.WaypointAmenitiesMarkerLayerManager


open class WaypointAmenityMarkerHelper(
    override val amenityMarkerManager: WaypointAmenitiesMarkerLayerManager,
    nightModeEnabled: Boolean, voucherEnabled: Boolean
) :
    AmenityMarkerHelper(amenityMarkerManager, nightModeEnabled, voucherEnabled) {


    fun addWaypointAmenityMarkerView(
        type: String, items: List<BaseAmenity>, waypoints: List<BaseAmenity>,
        hidden: Boolean, showDestination: Boolean
    ) {
        addAmenityMarkerView(type, items, hidden, showDestination)
        if (hidden) {
            showWayPoints(waypoints, type)
        }

    }

    private fun showWayPoints(
        waypoints: List<BaseAmenity>,
        type: String
    ) {
        val waypointIds = ArrayList<String>()
        waypoints.forEach {
            if (it.id == null) {
                return@forEach
            }
            waypointIds.add(it.id!!)
        }
        amenityMarkerManager.showMarkers(type, waypointIds)
    }


    override fun handleHideAmenity(type: String) {
        amenityMarkerManager.hideNonWaypointMarkers(type)
    }

    override fun handleAmenityProperties(
        properties: JsonObject,
        hidden: Boolean,
        item: BaseAmenity
    ) {
        properties.addProperty(MarkerLayerManager.HIDDEN, hidden)
        //can get way point
        val wayPoint = amenityMarkerManager.waypointManager.wayPoints.get(item.id)
        if (wayPoint != null) {
            MarkerLayerIconHelper.handleSelectedIcon(properties, nightModeEnabled)
        } else {
            MarkerLayerIconHelper.handleUnSelectedIcon(properties, nightModeEnabled)
        }
    }

}
package com.ncs.breeze.common.worker

import android.content.Context
import android.net.Uri
import androidx.work.CoroutineWorker
import androidx.work.Data
import androidx.work.WorkerParameters
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import com.breeze.model.constants.Constants
import kotlinx.coroutines.tasks.await
import timber.log.Timber
import java.io.File
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date
import java.util.Locale
import java.util.TimeZone

class WorkerNavigationLogFileUpload(
    appContext: Context,
    workerParams: WorkerParameters
) : CoroutineWorker(appContext, workerParams) {

    private val utcFormatter = SimpleDateFormat("yyyy-MM-dd_HH:mm:ss", Locale.US)
        .also { it.timeZone = TimeZone.getDefault() }

    override suspend fun doWork(): Result {

        val filePath = inputData.getString(KEY_FILE_PATH)
            ?.takeIf { it.isNotEmpty() } ?: return Result.failure()
        val userId = inputData.getString(KEY_USER_ID)
            ?.takeIf { it.isNotEmpty() } ?: return Result.failure()
        val description = inputData.getString(KEY_DESCRIPTION)
            ?.takeIf { it.isNotEmpty() } ?: return Result.failure()
//        val secondaryFirebaseID = inputData.getString(KEY_SECONDARY_FIREBASE_ID)
//            ?.takeIf { it.isNotEmpty() } ?: return Result.failure()

        Timber.d("Start uploading Navigation Log: userId=$userId, file=$filePath")


        val file = File(filePath)
        val uri = Uri.fromFile(file) ?: return Result.failure()
        val lastModified = kotlin.runCatching {
            file.lastModified()
                .takeIf { it > 0L }
                ?.let { Date(it) }
        }.getOrNull() ?: Calendar.getInstance().time
        val fileName = "${description}_${utcFormatter.format(lastModified)}.pbf.gz"
        //FIXME: GZIP ENCODING not done
        val fileRef = Firebase.storage.reference
            .child(Constants.CLOUD_STORAGE_FOLDER)
            .child("${userId}/${fileName}")
        val uploadResult = fileRef.putFile(uri).await()
        return if (!uploadResult.task.isSuccessful) {
            Result.retry()
        } else {
            File("$filePath.lock").takeIf { it.exists() }?.delete()
            file.delete()
            val downloadUrl = fileRef.downloadUrl.await()
            Timber.d("Download URI for $filePath is: $downloadUrl")
            val outputData = Data.Builder()
                .putString(WorkerNavigationLogDBUpdate.KEY_DOWNLOAD_URL, downloadUrl.toString())
                .putString(KEY_DESCRIPTION, description)
                .putString(KEY_USER_ID, userId)
                .putString(KEY_SAVED_FILE_NAME, fileName)
                .build()
            Result.success(outputData)
        }
    }

    companion object {
        const val KEY_FILE_PATH = "KEY_FILE_PATH"
        const val KEY_USER_ID = "KEY_USER_ID"
        const val KEY_DESCRIPTION = "KEY_DESCRIPTION"
        const val KEY_SECONDARY_FIREBASE_ID = "KEY_SECONDARY_FIREBASE_ID"
        const val KEY_SAVED_FILE_NAME = "KEY_SAVED_FILE_NAME"
    }
}
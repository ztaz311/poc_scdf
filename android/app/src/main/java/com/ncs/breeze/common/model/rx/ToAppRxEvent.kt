package com.ncs.breeze.common.model.rx

import com.breeze.model.SearchLocation
import com.breeze.model.api.response.amenities.BaseAmenity


//ToDo - Ankur - This class should be inside the Shared module
sealed class ToAppRxEvent


class AppToPhoneTurnOffCruiseModeEvent(val data: ToAppEventData? = null) : ToAppRxEvent()
class AppToPhoneTurnOnCruiseModeEvent(val data: ToAppEventData? = null) : ToAppRxEvent()
class AppToPhoneTurnOnOBULiteEvent(val data: ToAppEventData? = null) : ToAppRxEvent()
class AppToPhoneNavigationStartEvent(val data: PhoneNavigationStartEventData) : ToAppRxEvent()
class AppToPhoneNavigationReRouteEvent(val data: PhoneNavigationStartEventData) : ToAppRxEvent()
class AppToPhoneNavigationEndEvent(val data: ToAppEventData? = null) : ToAppRxEvent()

class AppToPhoneRoutePlanningStartEvent(val carPark: BaseAmenity) : ToAppRxEvent()
class AppToPhoneCarParkEvent(val data: ToAppEventData? = null) : ToAppRxEvent()
class AppToPhoneCarParkEndEvent(val data: ToAppEventData? = null) : ToAppRxEvent()

class CarGetListRecentSearch : ToAppRxEvent()
class GetShortcutListFromCar : ToAppRxEvent()
class GetSearchLocationResult(val searchTerm: String) : ToAppRxEvent()
class AddToRecentList(val item: SearchLocation) : ToAppRxEvent()
class GetLatLngGoogleIfNeed(val searchLocation: SearchLocation) : ToAppRxEvent()
class TriggerPhoneNotificaion() : ToAppRxEvent()

class ETAMessageReceivedPhone(val etaMessage: String) : ToAppRxEvent()
class ETAStartedPhone() : ToAppRxEvent()
class UpdateETAPhone() : ToAppRxEvent()
class UpdateStateVoiceApp(val isVoiceMuted: Boolean) : ToAppRxEvent()
class SaveOnboardingState(val state: String) : ToAppRxEvent()

class AppToPhoneReportIssueEvent() : ToAppRxEvent()

class StopOBULiteDisplay : ToAppRxEvent()

package com.ncs.breeze.car.breeze.screen.homescreen


import android.graphics.Rect
import com.mapbox.androidauto.internal.logAndroidAuto
import com.mapbox.maps.CameraOptions
import com.mapbox.maps.EdgeInsets
import com.mapbox.maps.extension.androidauto.MapboxCarMapObserver
import com.mapbox.maps.extension.androidauto.MapboxCarMapSurface
import com.mapbox.maps.plugin.locationcomponent.LocationComponentPlugin
import com.mapbox.maps.plugin.locationcomponent.OnIndicatorPositionChangedListener
import com.mapbox.maps.plugin.locationcomponent.location
import com.mapbox.navigation.core.lifecycle.MapboxNavigationApp
import com.mapbox.navigation.core.trip.session.TripSessionState
import com.ncs.breeze.R
import com.ncs.breeze.car.breeze.MainBreezeCarContext
import com.ncs.breeze.car.breeze.location.CarLocationPuck
import timber.log.Timber

/**
 * Create a simple 3d location puck. This class is demonstrating how to
 * create a renderer. To Create a new location experience, try creating a new class.
 */
class CarFreeDriveHomeLocationRenderer(
    private val mainCarContext: MainBreezeCarContext
) : MapboxCarMapObserver {

    private var locationComponent: LocationComponentPlugin? = null
    private var visibleArea = EdgeInsets(
        0.0,
        0.0,
        0.0,
        0.0
    )

    private val onIndicatorPositionChangedListener =
        OnIndicatorPositionChangedListener { point ->
//            Timber.e("onIndicatorPositionChangedListener: ${MapboxNavigationApp.current()?.getTripSessionState()}")
            val cameraOptions = CameraOptions.Builder()
                .center(point)
                .zoom(16.5)
                .pitch(0.0)
                .bearing(0.0)
                .padding(visibleArea)
                .build()
            mainCarContext.mapboxCarMap.carMapSurface?.mapSurface?.getMapboxMap()
                ?.setCamera(cameraOptions)
        }

    override fun onAttached(mapboxCarMapSurface: MapboxCarMapSurface) {
        logAndroidAuto("CarFreeDriveLocationRenderer carMapSurface loaded")
        locationComponent = mapboxCarMapSurface.mapSurface.location.apply {
            locationPuck = CarLocationPuck.homePuck2D(mainCarContext.carContext)
            enabled = true
            pulsingEnabled = true
            mainCarContext.defaultLocationProvider?.let {
                logAndroidAuto("CarFreeDriveLocationRenderer carMapSurface defaultLocationProvider not null")
                setLocationProvider(it)
            }
            pulsingColor = mainCarContext.carContext.getColor(
                R.color.themed_nav_puck_pulse_color
            )
            pulsingMaxRadius = 40.0F

            addOnIndicatorPositionChangedListener(onIndicatorPositionChangedListener)
        }
    }

    override fun onDetached(mapboxCarMapSurface: MapboxCarMapSurface) {
        logAndroidAuto("CarFreeDriveLocationRenderer carMapSurface detached")
        locationComponent?.removeOnIndicatorPositionChangedListener(
            onIndicatorPositionChangedListener
        )
    }

    override fun onVisibleAreaChanged(visibleArea: Rect, edgeInsets: EdgeInsets) {
        super.onVisibleAreaChanged(visibleArea, edgeInsets)
        this.visibleArea = edgeInsets
    }
}

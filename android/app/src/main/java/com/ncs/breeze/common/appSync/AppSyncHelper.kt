package com.ncs.breeze.common.appSync

import android.content.Context
import com.amazonaws.amplify.generated.graphql.MessageInboxSubscription
import com.amazonaws.amplify.generated.graphql.NotifyUserSubscription
import com.amazonaws.amplify.generated.graphql.RefreshDataSetSubscription
import com.amazonaws.amplify.generated.graphql.RefreshMockEventSubscription
import com.amazonaws.amplify.generated.graphql.RefreshOBUEventSubscription
import com.amazonaws.amplify.generated.graphql.UpdateDeviceUserLocationMutation
import com.amazonaws.amplify.generated.graphql.UpdateOBUDeviceUserLocationMutation
import com.amazonaws.amplify.generated.graphql.UpdateUserTripLocationMutation
import com.amazonaws.amplify.generated.graphql.UpdateWalkathonLocationMutation
import com.amazonaws.mobileconnectors.appsync.AWSAppSyncClient
import com.amazonaws.mobileconnectors.appsync.AppSyncSubscriptionCall
import com.apollographql.apollo.GraphQLCall
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.exception.ApolloException
import com.apollographql.apollo.internal.util.Cancelable
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.mapbox.navigation.core.trip.session.LocationMatcherResult
import com.ncs.breeze.App
import com.ncs.breeze.common.constant.OBUConnectionState
import com.ncs.breeze.common.extensions.android.getApp
import com.ncs.breeze.common.helper.obu.OBUConnectionHelper
import com.ncs.breeze.common.rxbus.RxBus
import com.ncs.breeze.common.rxbus.RxEvent
import com.ncs.breeze.common.utils.InitObjectUtilsController
import com.ncs.breeze.common.utils.SpeedLimitUtil
import com.ncs.breeze.common.utils.TripLoggingManager
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import timber.log.Timber


object AppSyncHelper {
    const val SPEED_LIGHT_CAMERA = "SPEEDLIGHT_CAMERA"
    const val TRAFFIC_INCIDENTS = "TRAFFIC_INCIDENTS_V2"
    const val GLOBAL_NOTIFICATION_DATA = "INBOX_GLOBAL_NOTIFICATION"
    const val ON_NEW_VOUCHER_ADDED = "ON_NEW_VOUCHER_ADDED"
    const val SCHOOL_ZONE = "SCHOOL_ZONE"
    const val SILVER_ZONE = "SILVER_ZONE"
    const val ERP = "ERP_PASSENGER_CARS"
    var appSyncStart: Boolean = false
    private var lastLiveLocationMutation: Cancelable? = null
    private var lastOBUMockLocationMutation: Cancelable? = null
    private var lastOBUBreezeDataLocationMutation: Cancelable? = null

    private var speedLightSubscription: AppSyncSubscriptionCall<RefreshDataSetSubscription.Data>? =
        null
    private var incidentSubscription: AppSyncSubscriptionCall<RefreshDataSetSubscription.Data>? =
        null
    private var globalNotificationSubscription: AppSyncSubscriptionCall<RefreshDataSetSubscription.Data>? =
        null
    private var erpSubscription: AppSyncSubscriptionCall<RefreshDataSetSubscription.Data>? = null
    private var silverZoneSubscription: AppSyncSubscriptionCall<RefreshDataSetSubscription.Data>? =
        null
    private var schoolZoneSubscription: AppSyncSubscriptionCall<RefreshDataSetSubscription.Data>? =
        null
    private var etaMessageSubscription: AppSyncSubscriptionCall<MessageInboxSubscription.Data>? =
        null
    private var voucherAddedMessageSubscription: AppSyncSubscriptionCall<NotifyUserSubscription.Data>? =
        null
    private var obuMockEventSubscription: AppSyncSubscriptionCall<RefreshMockEventSubscription.Data>? =
        null
    private var obuDataEventSubscription: AppSyncSubscriptionCall<RefreshOBUEventSubscription.Data>? =
        null

    fun subscribeToAppSyncEvents(applicationContext: Context) {
        synchronized(this) {
            if (!appSyncStart) {
                appSyncStart = true
                CoroutineScope(Dispatchers.IO).launch {
                    // Get the client instance
                    val awsAppSyncClient: AWSAppSyncClient? =
                        ClientFactory.getInstance(applicationContext)
                    if (awsAppSyncClient != null) {
                        subscribeSpeedLightEvent(awsAppSyncClient)
                        subscribeIncidentEvent(awsAppSyncClient)
                        subscribeGlobalNotificationEvent(awsAppSyncClient)
                        subscribeERPEvent(awsAppSyncClient)
                        subscribeSchoolZoneEvent(awsAppSyncClient)
                        subscribeSilverZoneEvent(awsAppSyncClient)
                        subscribeVoucherAddedEvent(awsAppSyncClient)
                        subscribeOBUMockEvent(awsAppSyncClient)
                        subscribeOBUDataEvent(awsAppSyncClient)
                    }
                }
            }
        }
    }

    private fun subscribeOBUDataEvent(awsAppSyncClient: AWSAppSyncClient) {
        InitObjectUtilsController.cognitoUserID?.let { cognitoUserID ->
            obuDataEventSubscription = awsAppSyncClient.subscribe(
                RefreshOBUEventSubscription.builder().userId(cognitoUserID).build()
            ).also {
                it.execute(obuDataEventCallback)
            }
        }
    }

    private fun subscribeOBUMockEvent(awsAppSyncClient: AWSAppSyncClient) {
        InitObjectUtilsController.cognitoUserID?.let { cognitoUserID ->
            obuMockEventSubscription = awsAppSyncClient.subscribe(
                RefreshMockEventSubscription.builder().userId(cognitoUserID).build()
            ).also {
                it.execute(obuMockEventCallback)
            }
        }
    }

    fun unSubscribeAll(callbackWhenDone: () -> Unit) {
        unsubscribeToAppSyncEvents(callbackWhenDone)
//        val cliear = ClearCacheOptions.builder()
//            .clearQueries()Î
//            //.clearMutations()
//            .clearSubscriptions()
//            .build()
//        ClientFactory.getInstance(applicationContext)?.clearCaches(cliear)
//        callbackWhenDone.invoke()
    }

    private fun subscribeVoucherAddedEvent(awsAppSyncClient: AWSAppSyncClient) {
        InitObjectUtilsController.cognitoUserID?.let { id ->
            voucherAddedMessageSubscription = awsAppSyncClient.subscribe(
                NotifyUserSubscription.builder().cognitoUserId(id)
                    .build()
            ).also {
                it.execute(voucherAddedCallback)
            }
        }
    }

    private fun subscribeSpeedLightEvent(appSyncClient: AWSAppSyncClient) {
        speedLightSubscription = appSyncClient.subscribe(
            RefreshDataSetSubscription.builder().notificationType(SPEED_LIGHT_CAMERA)
                .build()
        ).also {
            it.execute(speedLightCallback)
        }
    }

    private fun subscribeIncidentEvent(appSyncClient: AWSAppSyncClient) {
        incidentSubscription = appSyncClient.subscribe(
            RefreshDataSetSubscription.builder().notificationType(TRAFFIC_INCIDENTS)
                .build()
        ).also {
            it.execute(incidentsCallback)
        }
    }

    private fun subscribeGlobalNotificationEvent(appSyncClient: AWSAppSyncClient) {
        globalNotificationSubscription = appSyncClient.subscribe(
            RefreshDataSetSubscription.builder().notificationType(GLOBAL_NOTIFICATION_DATA)
                .build()
        ).also {
            it.execute(globalNotificationCallback)
        }
    }

    private fun subscribeERPEvent(appSyncClient: AWSAppSyncClient) {
        erpSubscription = appSyncClient.subscribe(
            RefreshDataSetSubscription.builder().notificationType(ERP)
                .build()
        ).also {
            it.execute(erpCallback)
        }
    }

    private fun subscribeSilverZoneEvent(appSyncClient: AWSAppSyncClient) {
        silverZoneSubscription = appSyncClient.subscribe(
            RefreshDataSetSubscription.builder().notificationType(SILVER_ZONE)
                .build()
        ).also {
            it.execute(silverZoneCallback)
        }
    }

    private fun subscribeSchoolZoneEvent(appSyncClient: AWSAppSyncClient) {
        schoolZoneSubscription = appSyncClient.subscribe(
            RefreshDataSetSubscription.builder().notificationType(SCHOOL_ZONE)
                .build()
        ).also {
            it.execute(schoolZoneCallback)
        }
    }

    fun subscribeToETAMessage(tripId: String, context: Context) {

        CoroutineScope(Dispatchers.IO).launch {
            val awsAppSyncClient: AWSAppSyncClient? = ClientFactory.getInstance(context)
            if (awsAppSyncClient != null) {
                etaMessageSubscription = awsAppSyncClient.subscribe(
                    MessageInboxSubscription.builder().tripId(tripId)
                        .build()
                ).also {
                    it.execute(etaMessageCallback)
                }
            }
        }
    }

    fun unsubscribeToETAMessage() {
        CoroutineScope(Dispatchers.IO).launch {
            etaMessageSubscription?.takeIf { !it.isCanceled }?.cancel()
        }
    }

    fun publishLiveLocationData(
        context: Context,
        tripId: String,
        latitude: Double,
        longitude: Double,
        status: String,
        arrivalTime: String = "",
        coordinates: String = "",
    ) {
        CoroutineScope(Dispatchers.IO).launch {
            Timber.d("Update trip location with status: $status")
            val updateTripLocation = UpdateUserTripLocationMutation.builder()
                .tripId(tripId)
                .latitude(latitude.toString())
                .longitude(longitude.toString())
                .status(status)
                .arrivalTime(arrivalTime)
                .coordinates(coordinates)
                .build()
            val awsAppSyncClient: AWSAppSyncClient? = ClientFactory.getInstance(context, MutationClientTypes.ETA_LIVE_LOCATION)
            awsAppSyncClient?.mutate(updateTripLocation)?.let {mutation->
                lastLiveLocationMutation?.cancel()
                lastLiveLocationMutation = mutation
                mutation.enqueue(updateTripLocationCallback)
            }
        }
    }

    fun publishLiveLocationForWalkathonData(
        context: Context,
        userId: String,
        walkathonId: String,
        latitude: String,
        longitude: String,
        currentTime: String,
        data: String = ""
    ) {
        CoroutineScope(Dispatchers.IO).launch {
            val updateTripLocation = UpdateWalkathonLocationMutation.builder()
                .userId(userId)
                .walkathonId(walkathonId)
                .latitude(latitude)
                .longitude(longitude)
                .currentTime(currentTime)
                .data(data)
                .build()
            val awsAppSyncClient: AWSAppSyncClient? = ClientFactory.getInstance(context, MutationClientTypes.WALKATHON_LIVE_LOCATION)
            awsAppSyncClient?.mutate(updateTripLocation)?.enqueue(publishLocationHackathonCallback)
        }
    }

    fun publishLiveLocationOBU(
        context: Context,
        locationMatcherResult: LocationMatcherResult
    ) {
        Timber.d("publishLiveLocationOBU: isTeleport=${locationMatcherResult.isTeleport}, location=${locationMatcherResult.enhancedLocation}, hasUserId=${InitObjectUtilsController.cognitoUserID != null}, isOBUSimulation=${OBUConnectionHelper.isOBUSimulationEnabled}")
        if (locationMatcherResult.isTeleport) return
        val userId = InitObjectUtilsController.cognitoUserID ?: return
        val latitude = "${locationMatcherResult.enhancedLocation.latitude}"
        val longitude = "${locationMatcherResult.enhancedLocation.longitude}"
        val bearing = "${locationMatcherResult.enhancedLocation.bearing}"

        val data = JsonObject().apply {
            addProperty("tripGUID", context.getApp()?.breezeTripLoggingUtil?.tripID)
            addProperty(
                "roadName",
                SpeedLimitUtil.currentRoadNameFull
            )
        }
        if (OBUConnectionHelper.isOBUSimulationEnabled)
            UpdateDeviceUserLocationMutation.builder()
                .userId(userId)
                .latitude(latitude)
                .longitude(longitude)
                .bearing(bearing)
                .data(Gson().toJson(data))
                .build().let { it ->
                    ClientFactory.getInstance(context, MutationClientTypes.OBU_SIMULATED)?.mutate(it)?.let { mutation ->
                        lastOBUMockLocationMutation?.cancel()
                        lastOBUMockLocationMutation = mutation
                        mutation.enqueue(null)
                    }

                }
        else
            UpdateOBUDeviceUserLocationMutation.builder()
                .userId(userId)
                .latitude(latitude)
                .longitude(longitude)
                .bearing(bearing)
                .data(Gson().toJson(data))
                .build().let {
                    ClientFactory.getInstance(context, MutationClientTypes.OBU_REAL)?.mutate(it)?.let { mutation ->
                        lastOBUBreezeDataLocationMutation?.cancel()
                        lastOBUBreezeDataLocationMutation = mutation
                        mutation.enqueue(null)
                    }
                }
    }

    private var updateTripLocationCallback =
        object : GraphQLCall.Callback<UpdateUserTripLocationMutation.Data>() {
            override fun onResponse(response: Response<UpdateUserTripLocationMutation.Data>) {
                Timber.d("Update trip location was successful")
            }

            override fun onFailure(e: ApolloException) {
                Timber.d("Update trip location was failed")
            }
        }

    private var updateOBULocationCallback =
        object : GraphQLCall.Callback<UpdateOBUDeviceUserLocationMutation.Data>() {
            override fun onResponse(response: Response<UpdateOBUDeviceUserLocationMutation.Data>) {
                Timber.d("Update trip location was successful")
            }

            override fun onFailure(e: ApolloException) {
                Timber.d("Update trip location was failed")
            }
        }


    private var publishLocationHackathonCallback =
        object : GraphQLCall.Callback<UpdateWalkathonLocationMutation.Data>() {
            override fun onResponse(response: Response<UpdateWalkathonLocationMutation.Data>) {}
            override fun onFailure(e: ApolloException) {}
        }

    private var speedLightCallback =
        object : AppSyncSubscriptionCall.Callback<RefreshDataSetSubscription.Data> {
            override fun onResponse(response: Response<RefreshDataSetSubscription.Data>) {
                Timber.d("On Response successfully called")
                RxBus.publish(
                    RxEvent.MiscROAppSyncEvent(response.data()?.refreshDataSet()?.data())
                )
            }

            override fun onFailure(e: ApolloException) {
                Timber.d("On SpeedLight Sync Failure successfully called")
            }

            override fun onCompleted() {}
        }

    private var incidentsCallback =
        object : AppSyncSubscriptionCall.Callback<RefreshDataSetSubscription.Data> {
            override fun onResponse(response: Response<RefreshDataSetSubscription.Data>) {
                Timber.d("On Response successfully called")
                RxBus.publish(
                    RxEvent.IncidentAppSyncEvent(response.data()?.refreshDataSet()?.data())
                )
            }

            override fun onFailure(e: ApolloException) {
                Timber.d("On Incident Sync Failure successfully called")
            }

            override fun onCompleted() {}
        }

    private var globalNotificationCallback =
        object : AppSyncSubscriptionCall.Callback<RefreshDataSetSubscription.Data> {
            override fun onResponse(response: Response<RefreshDataSetSubscription.Data>) {
                Timber.d("globalNotificationCallback::On Response successfully called")
                RxBus.publish(
                    RxEvent.GlobalNotificationSyncEvent(
                        response.data()?.refreshDataSet()?.data()
                    )
                )
            }

            override fun onFailure(e: ApolloException) {
                Timber.d("On Global Notification Sync Failure successfully called")
            }

            override fun onCompleted() {}

        }

    private var voucherAddedCallback =
        object : AppSyncSubscriptionCall.Callback<NotifyUserSubscription.Data> {
            override fun onResponse(response: Response<NotifyUserSubscription.Data>) {
                Timber.d("globalNotificationCallback:voucherAddedCallback :On Response successfully called")
                val typeNotification = response.data()?.notifyUser()?.notificationType()
                if (typeNotification == ON_NEW_VOUCHER_ADDED) {
                    RxBus.publish(RxEvent.VoucherAddedSyncEvent())
                } else if (typeNotification == GLOBAL_NOTIFICATION_DATA) {
                    RxBus.publish(
                        RxEvent.GlobalNotificationSyncEvent(
                            response.data()?.notifyUser()?.data()
                        )
                    )
                }
            }

            override fun onFailure(e: ApolloException) {
                Timber.d("On Global Notification Sync Failure successfully called")
            }

            override fun onCompleted() {}

        }

    private var erpCallback =
        object : AppSyncSubscriptionCall.Callback<RefreshDataSetSubscription.Data> {
            override fun onResponse(response: Response<RefreshDataSetSubscription.Data>) {
                Timber.d("ERP On Response successfully called")
                RxBus.publish(
                    RxEvent.ERPAppSyncEvent(response.data()?.refreshDataSet()?.data())
                )
            }

            override fun onFailure(e: ApolloException) {
                Timber.d("On ERP Sync Failure successfully called")
            }

            override fun onCompleted() {
            }

        }

    private var schoolZoneCallback =
        object : AppSyncSubscriptionCall.Callback<RefreshDataSetSubscription.Data> {
            override fun onResponse(response: Response<RefreshDataSetSubscription.Data>) {
                Timber.d("School Zone On Response successfully called")
                RxBus.publish(
                    RxEvent.SchoolZoneAppSyncEvent(response.data()?.refreshDataSet()?.data())
                )
            }

            override fun onFailure(e: ApolloException) {
                Timber.d("On SchoolZone Sync Failure successfully called: $e")
            }

            override fun onCompleted() {}
        }

    private var silverZoneCallback =
        object : AppSyncSubscriptionCall.Callback<RefreshDataSetSubscription.Data> {
            override fun onResponse(response: Response<RefreshDataSetSubscription.Data>) {
                Timber.d("Silver Zone On Response successfully called")
                RxBus.publish(
                    RxEvent.SilverZoneAppSyncEvent(response.data()?.refreshDataSet()?.data())
                )
            }

            override fun onFailure(e: ApolloException) {
                Timber.d("On SilverZone Sync Failure successfully called")
            }

            override fun onCompleted() {}
        }

    private var etaMessageCallback =
        object : AppSyncSubscriptionCall.Callback<MessageInboxSubscription.Data> {
            override fun onResponse(response: Response<MessageInboxSubscription.Data>) {
                Timber.d("ETA Message: ${response.data()?.messageInbox()?.message()}")
                RxBus.publish(
                    RxEvent.ETAMessage(response.data()?.messageInbox()?.message().toString())
                )
            }

            override fun onFailure(e: ApolloException) {
                Timber.d("On ETAMessage Sync Failure successfully called")
            }

            override fun onCompleted() {}
        }

    private val obuMockEventCallback =
        object : AppSyncSubscriptionCall.Callback<RefreshMockEventSubscription.Data> {
            override fun onResponse(response: Response<RefreshMockEventSubscription.Data>) {
                val dataJson = response.data()?.refreshMockEvent()?.data()
                Timber.d("OBUAppSyncMockDataHandler: onResponse $dataJson")
                OBUAppSyncToRxPublisher.handleAppSyncEvent(
                    eventName = response.data()?.refreshMockEvent()?.eventType(),
                    dataJson = dataJson
                )
            }

            override fun onFailure(e: ApolloException) {}
            override fun onCompleted() {}
        }

    private val obuDataEventCallback =
        object : AppSyncSubscriptionCall.Callback<RefreshOBUEventSubscription.Data> {
            override fun onResponse(response: Response<RefreshOBUEventSubscription.Data>) {
                val dataJson = response.data()?.refreshOBUEvent()?.data()
                Timber.d("OBUAppSyncDataHandler: onResponse $dataJson")
                OBUAppSyncToRxPublisher.handleAppSyncEvent(
                    eventName = response.data()?.refreshOBUEvent()?.eventType(),
                    dataJson = dataJson
                )
            }

            override fun onFailure(e: ApolloException) {}
            override fun onCompleted() {}
        }


    fun unsubscribeToAppSyncEvents(callbackWhenDone: () -> Unit) {
        appSyncStart = false
        CoroutineScope(Dispatchers.IO).launch {
            kotlin.runCatching {
                speedLightSubscription?.takeIf { !it.isCanceled }?.cancel()
                speedLightSubscription = null
                incidentSubscription?.takeIf { !it.isCanceled }?.cancel()
                incidentSubscription = null
                globalNotificationSubscription?.takeIf { !it.isCanceled }?.cancel()
                globalNotificationSubscription = null
                erpSubscription?.takeIf { !it.isCanceled }?.cancel()
                erpSubscription = null
                silverZoneSubscription?.takeIf { !it.isCanceled }?.cancel()
                silverZoneSubscription = null
                schoolZoneSubscription?.takeIf { !it.isCanceled }?.cancel()
                schoolZoneSubscription = null
                etaMessageSubscription?.takeIf { !it.isCanceled }?.cancel()
                etaMessageSubscription = null
                voucherAddedMessageSubscription?.takeIf { !it.isCanceled }?.cancel()
                voucherAddedMessageSubscription = null
                obuMockEventSubscription?.takeIf { !it.isCanceled }?.cancel()
                obuMockEventSubscription = null
                obuDataEventSubscription?.takeIf { !it.isCanceled }?.cancel()
                obuDataEventSubscription = null

            }.onFailure { e ->
                Timber.e(e, "Exception when trying to unsubscribe from App sync events")
            }
            CoroutineScope(Dispatchers.Main).launch {
                callbackWhenDone.invoke()
            }
        }
    }
}
package com.ncs.breeze.ui.dashboard.fragments.view

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.Rect
import android.graphics.drawable.ColorDrawable
import android.location.Location
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.provider.ContactsContract
import android.text.TextUtils
import android.text.format.DateFormat
import android.text.format.DateUtils
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.WindowManager
import android.widget.TextView
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.core.app.NotificationManagerCompat
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.setFragmentResultListener
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.PagerSnapHelper
import com.breeze.customization.view.AlertRoutePlanView
import com.breeze.customization.view.BreezeAmenityRadioButton
import com.breeze.customization.view.BreezeButton
import com.breeze.customization.view.CarParkStateView
import com.breeze.customization.view.DispatchTouchLayout
import com.breeze.customization.view.VoucherViewGroup
import com.breeze.model.*
import com.breeze.model.api.ErrorData
import com.breeze.model.api.request.ETARequest
import com.breeze.model.api.request.TripPlannerRequest
import com.breeze.model.api.response.BroadcastData
import com.breeze.model.api.response.BroadcastTravelZoneFilterResponse
import com.breeze.model.api.response.ERPResponseData
import com.breeze.model.api.response.EasyBreezieAddress
import com.breeze.model.api.response.UpcomingTripDetailResponse
import com.breeze.model.api.response.ValueConditionShowBroadcast
import com.breeze.model.api.response.amenities.BaseAmenity
import com.breeze.model.api.response.tbr.ResponseCongestionDetail
import com.breeze.model.constants.AmenityType.CARPARK
import com.breeze.model.constants.AmenityType.EVCHARGER
import com.breeze.model.constants.AmenityType.PETROL
import com.breeze.model.constants.Constants
import com.breeze.model.constants.ShareDestinationType
import com.breeze.model.enums.CarParkViewOption
import com.breeze.model.enums.RoutePreference
import com.breeze.model.extensions.highlightWords
import com.breeze.model.extensions.toBitmap
import com.facebook.react.bridge.Arguments
import com.google.android.flexbox.*
import com.google.android.gms.maps.model.LatLng
import com.google.gson.JsonObject
import com.mapbox.android.core.permissions.PermissionsManager
import com.mapbox.android.gestures.StandardScaleGestureDetector
import com.mapbox.api.directions.v5.models.DirectionsResponse
import com.mapbox.api.directions.v5.models.DirectionsRoute
import com.mapbox.bindgen.Expected
import com.mapbox.geojson.Feature
import com.mapbox.geojson.LineString
import com.mapbox.geojson.Point
import com.mapbox.maps.*
import com.mapbox.maps.extension.style.layers.properties.generated.Visibility
import com.mapbox.maps.plugin.animation.CameraAnimatorOptions
import com.mapbox.maps.plugin.animation.camera
import com.mapbox.maps.plugin.gestures.OnScaleListener
import com.mapbox.maps.plugin.gestures.gestures
import com.mapbox.navigation.base.formatter.DistanceFormatterOptions
import com.mapbox.navigation.base.route.NavigationRoute
import com.mapbox.navigation.core.formatter.MapboxDistanceFormatter
import com.mapbox.navigation.ui.base.util.MapboxNavigationConsumer
import com.mapbox.navigation.ui.maneuver.api.MapboxManeuverApi
import com.mapbox.navigation.ui.maps.camera.NavigationCamera
import com.mapbox.navigation.ui.maps.camera.data.MapboxNavigationViewportDataSource
import com.mapbox.navigation.ui.maps.route.line.api.MapboxRouteLineApi
import com.mapbox.navigation.ui.maps.route.line.model.ClosestRouteValue
import com.mapbox.navigation.ui.maps.route.line.model.RouteLineError
import com.mapbox.navigation.ui.maps.route.line.model.RouteNotFound
import com.mapbox.navigation.ui.maps.route.line.model.RouteSetValue
import com.mapbox.navigation.ui.utils.internal.ifNonNull
import com.mapbox.turf.TurfMeasurement
import com.mapbox.turf.TurfMisc
import com.ncs.breeze.App
import com.ncs.breeze.BuildConfig
import com.ncs.breeze.R
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.Event
import com.ncs.breeze.common.analytics.Screen
import com.ncs.breeze.common.components.maplayer.WalkingRouteLineLayer
import com.ncs.breeze.common.constant.RNScreen
import com.ncs.breeze.common.event.ToAppRx.postEvent
import com.ncs.breeze.common.event.ToCarRx.postEventWithCacheLast
import com.ncs.breeze.common.extensions.android.getRNFragmentNavigationParams
import com.ncs.breeze.common.extensions.android.getShareBusinessLogicPackage
import com.ncs.breeze.common.extensions.android.getUserDataPreference
import com.ncs.breeze.common.extensions.android.isContainView
import com.ncs.breeze.common.extensions.mapbox.compressToGzip
import com.ncs.breeze.common.extensions.mapbox.setBreezeDefaultOptions
import com.ncs.breeze.common.extensions.mapbox.updateDestinationAndLocationPuckLayer
import com.ncs.breeze.common.helper.route.WaypointManager
import com.ncs.breeze.common.model.DepartureType
import com.ncs.breeze.common.model.RouteAdapterTypeObjects
import com.ncs.breeze.common.model.RouteDepartureDetails
import com.ncs.breeze.common.model.SimpleRouteAdapterObjects
import com.ncs.breeze.common.model.rx.AppToNavigationStartEvent
import com.ncs.breeze.common.model.rx.AppToPhoneNavigationStartEvent
import com.ncs.breeze.common.model.rx.AppToRoutePlanningEndEvent
import com.ncs.breeze.common.model.rx.CarNavigationStartEventData
import com.ncs.breeze.common.model.rx.PhoneNavigationStartEventData
import com.ncs.breeze.common.model.toRouteAdapterObjects
import com.ncs.breeze.common.remote.ApiObserver
import com.ncs.breeze.common.remote.MyServiceInterceptor
import com.ncs.breeze.common.rxbus.RxBus
import com.ncs.breeze.common.rxbus.RxEvent
import com.ncs.breeze.common.utils.*
import com.ncs.breeze.common.utils.RoutePlanningConsts.PROP_ERP_DISPLAY
import com.ncs.breeze.common.utils.RoutePlanningConsts.ROUTE_ERP_IMG
import com.ncs.breeze.common.utils.RoutePlanningConsts.SELECTED_ERP_RATE_IMG
import com.ncs.breeze.common.utils.RoutePlanningConsts.UNSELECTED_ERP_RATE_IMG
import com.ncs.breeze.common.utils.compression.GZIPCompressionUtil
import com.ncs.breeze.common.utils.notification.PushNotificationUtils
import com.ncs.breeze.common.utils.route.RouteSelectorUtil
import com.ncs.breeze.components.*
import com.ncs.breeze.components.layermanager.MarkerLayerManager
import com.ncs.breeze.components.layermanager.impl.WaypointAmenitiesMarkerLayerManager
import com.ncs.breeze.components.map.erp.MapERPManager
import com.ncs.breeze.components.map.profile.ListenerCallbackLandingMap
import com.ncs.breeze.components.map.profile.MapCongestionManager
import com.ncs.breeze.components.map.profile.ProfileZoneLayer
import com.ncs.breeze.components.marker.markerview2.EVMarkerView
import com.ncs.breeze.components.marker.markerview2.PetrolMarkerView
import com.ncs.breeze.components.marker.markerview2.carpark.RouteCarParkMarkerView
import com.ncs.breeze.databinding.FragmentRoutePlanningBinding
import com.ncs.breeze.helper.DestinationIconManager
import com.ncs.breeze.helper.animation.ViewAnimator
import com.ncs.breeze.helper.carpark.CarParkDestinationIconManager
import com.ncs.breeze.helper.marker.WaypointAmenityMarkerHelper
import com.ncs.breeze.notification.FCMListenerService
import com.ncs.breeze.reactnative.nativemodule.ShareBusinessLogicEvent
import com.ncs.breeze.ui.dashboard.activity.DashboardActivity
import com.ncs.breeze.ui.dashboard.activity.PanTrackingManager
import com.ncs.breeze.ui.dashboard.fragments.adapter.AmenityItemAdapter
import com.ncs.breeze.ui.dashboard.fragments.viewmodel.AmenityData
import com.ncs.breeze.ui.dashboard.fragments.viewmodel.CarParkProfileDetails
import com.ncs.breeze.ui.dashboard.fragments.viewmodel.RoutePlanningViewModel
import com.ncs.breeze.ui.rn.CustomReactFragment
import com.ncs.breeze.ui.utils.RouteLabelHelper
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.*
import timber.log.Timber
import java.util.*
import java.util.concurrent.ConcurrentHashMap
import javax.inject.Inject


class RoutePlanningFragment :
    BaseRoutePlanningFragment<FragmentRoutePlanningBinding, RoutePlanningViewModel>(),
    DashboardActivity.RefreshRouteDestinationListener,
    DashboardActivity.RefreshRoutePlanningListener {

    private var shouldShowNCSBroadcast = true
    private var isShouldRecenter: Boolean = false
    private var isDestinationInZoneTBR: Boolean = false
    private var viewLoadComplete = false
    private var lastCarParkToggleState: CarParkViewOption? = null
    private var currentRouteObjects: ArrayList<SimpleRouteAdapterObjects> = arrayListOf()
    private var currentRouteTypes: ArrayList<RoutePreference> = arrayListOf()
    private var walkingRoute: NavigationRoute? = null
    private lateinit var amenityItemAdapter: AmenityItemAdapter

    private val routeClickPadding = com.mapbox.android.gestures.Utils.dpToPx(30F)
    lateinit var mAdapter: RouteViewPagerAdapter
    lateinit var handler: Handler

    private var CURRENT_SELECTED_ROUTE: Int = 0

    private var isSourceLocation = false

    private var routeplanningMode = RoutePlanningMode.NORMAL

    @Inject
    lateinit var breezeERPUtil: BreezeERPRefreshUtil

    @Inject
    lateinit var breezeUserPreferenceUtil: BreezeUserPreferenceUtil

    @Inject
    lateinit var myServiceInterceptor: MyServiceInterceptor

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val viewModel: RoutePlanningViewModel by viewModels {
        viewModelFactory
    }

    private lateinit var mapboxMap: MapboxMap
    private lateinit var viewportDataSource: MapboxNavigationViewportDataSource
    private lateinit var navigationCamera: NavigationCamera
    private lateinit var maneuverApi: MapboxManeuverApi

    /**
     * for pantracking
     */
    var panTrackingManager: PanTrackingManager? = null


    var currentLocationZoneFirstTime: ProfileZoneLayer.ProfileZoneType? =
        ProfileZoneLayer.ProfileZoneType(null, null, ProfileZoneLayer.OUTER_ZONE)

    lateinit var snapHelper: PagerSnapHelper

    private var showingCarparkDetails: Boolean = false

    // BREEZE2-1845
//    private var etaEnabled: Boolean = false
//    private var etaRequest: ETARequest? = null

    lateinit var routeLabelHelper: RouteLabelHelper

    // Disposables
    var compositeDisposable: CompositeDisposable = CompositeDisposable()
    private var erpRefreshDisposable: Disposable? = null

    private var nightModeEnabled: Boolean = false

    lateinit var mapCongestionManager: MapCongestionManager
    lateinit var mapERPManager: MapERPManager

    /**
     * for planned later
     * will only be null when in normal mode
     */
    private var plannedSelectedDate: Date? = null
    private var isDepartureDate: Boolean = true

    private var plannedTripDetails: UpcomingTripDetailResponse? = null
    private var selectedCarParkIconRes: Int = -1
    private var selectedCarParkDestinationIconRes: Int = -1

    /**
     * for carpark broadcast
     */
    private var mCurrentNotificationCriterias: ArrayList<ValueConditionShowBroadcast>? = null
    private var mCurrentMessageBroadcastMessageShouldShow: String = ""

    /**
     * for carpark in routePlaning
     */
    private val waypointManager = WaypointManager()

    private var amenityMarkerManager: WaypointAmenitiesMarkerLayerManager? = null
    private var amenityMarkerHelper: WaypointAmenityMarkerHelper? = null

    /**
     * Dashboard amenities selection state
     */

    private var isAmenitySelectionUpdated = false

    /**
     * for carpark destination
     */
    private lateinit var carParkDestinationIconManager: CarParkDestinationIconManager
    private lateinit var walkingDestinationIconManager: DestinationIconManager

    var isZoneCongestion = false

    var contactsResultLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK && result.data != null) {
                processContactsResult(result.data)
            } else if (result.resultCode == Activity.RESULT_CANCELED) {
                Analytics.logClickEvent(Event.SEARCH_CANCEL, Screen.SEARCH_CONTACT)
            }
        }


    private fun launchContactsIntent() {
        Analytics.logScreenView(getScreenName())
        val intent = Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI)
        intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE)
        contactsResultLauncher.launch(intent)
    }


    private val amenityObserver = Observer<ConcurrentHashMap<String, AmenityData>> {
        it?.let {
            handleAmenities(it)
        }
    }

    private val carParkAmenityObserver = CarParkObserver()
    private val panCarParkAmenityObserver = PanCarParkObserver()


    private val voucherClickHandler = object : VoucherViewGroup.VoucherClickHandler {

        override fun onSelect() {
            Analytics.logClickEvent(Event.VIEW_NEARBY_VOUCHER_CAR_PARKS, getScreenName())
            viewModel.amenityDataMap[CARPARK]?.let {
                carParkAmenityObserver.renderUpdatedData(it, true)
            }
        }

        override fun onDismiss() {
            Analytics.logClickEvent(Event.BACK_TO_ROUTE_OPTIONS, getScreenName())
            if (!viewBinding.carParkViewOption.isCarParksShown()) {
                hideCarParks()
            }
        }
    }


    /**
     * Updates the destination icon
     * Shows the correct congestion information
     */
    private val voucherDetectedEvent = Observer<CarParkProfileDetails> { carParkProfileDetails ->

        val amenities = viewModel.amenityDataMap[CARPARK]?.amenities?.let { ArrayList(it) }
        val selectedAmenity =
            amenities?.find { it.baseAmenity.id == currentDestinationDetails?.carParkID }
        val isDestinationVoucherCarPark =
            currentDestinationDetails?.isDestinationCarPark() == true && selectedAmenity != null && selectedAmenity.baseAmenity.hasVouchers
        if (isDestinationVoucherCarPark) {
            updateCarParkDestinationForVoucher(selectedAmenity!!)
        }
        if (carParkProfileDetails.congestionDetails != null) {
            mapCongestionManager.reDrawCircle(carParkProfileDetails.profile, false)
            mapCongestionManager.updateColorCircleLayer(
                carParkProfileDetails.congestionDetails,
                carParkProfileDetails.profile
            )
            if (isZoneCongestion(
                    carParkProfileDetails.zoneType!!.profileZoneId!!,
                    carParkProfileDetails.congestionDetails
                )
            ) {
                isZoneCongestion = true
                viewBinding.voucherLayout.visibility = View.GONE
                handleVoucherAndCongestionConflict(
                    carParkProfileDetails.voucherDetected,
                    isDestinationVoucherCarPark
                )
            } else {
                isZoneCongestion = false
                if (carParkProfileDetails.voucherDetected) {
                    showVoucherView(isDestinationVoucherCarPark)
                } else {
                    ViewAnimator.hideViewSlideDown(
                        viewBinding.layoutAlertCongestion,
                        VIEW_ANIMATION_DURATION
                    ) {
                        if (isDestinationVoucherCarPark) {
                            viewBinding.voucherLayout.updateViewState(VoucherViewGroup.VoucherState.PRESELECTED)
                            ViewAnimator.showViewSlideUp(
                                viewBinding.voucherLayout,
                                VIEW_ANIMATION_DURATION
                            )
                        }
                    }
                }
            }
        } else if (carParkProfileDetails.voucherDetected) {
            showVoucherView(isDestinationVoucherCarPark)
        } else if (isDestinationVoucherCarPark) {
            ViewAnimator.hideViewSlideDown(
                viewBinding.layoutAlertCongestion,
                VIEW_ANIMATION_DURATION
            ) {
                viewBinding.voucherLayout.updateViewState(VoucherViewGroup.VoucherState.PRESELECTED)
                ViewAnimator.showViewSlideUp(
                    viewBinding.voucherLayout,
                    VIEW_ANIMATION_DURATION
                )
            }
        }
    }

    private fun handleVoucherAndCongestionConflict(
        voucherDetected: Boolean,
        isDestinationCarPark: Boolean
    ) {
        if (currentLocationZoneFirstTime!!.type == ProfileZoneLayer.NO_ZONE) {
            ViewAnimator.showViewSlideUp(
                viewBinding.layoutAlertCongestion,
                VIEW_ANIMATION_DURATION
            ) {
                lifecycleScope.launch {
                    try {
                        ensureActive()
                        delay(VIEW_ANIMATION_SWITCH_DELAY)
                        swapCongestionVoucherView(voucherDetected, isDestinationCarPark)
                    } catch (e: CancellationException) {
                    }
                }
            }
        } else {
            checkAndShowVoucherBanner(voucherDetected, isDestinationCarPark)
        }
    }

    private fun showVoucherView(isDestinationCarPark: Boolean) {
        if (viewBinding.layoutAlertCongestion.visibility == View.VISIBLE) {
            swapCongestionVoucherView(true, isDestinationCarPark)
        } else {
            viewBinding.voucherLayout.updateViewState(VoucherViewGroup.VoucherState.UNSELECTED)
            ViewAnimator.showViewSlideUp(viewBinding.voucherLayout, VIEW_ANIMATION_DURATION)
        }
    }

    private fun swapCongestionVoucherView(voucherDetected: Boolean, isDestinationCarPark: Boolean) {
        if (!voucherDetected && !isDestinationCarPark) {
            return
        }
        ViewAnimator.hideViewSlideDown(
            viewBinding.layoutAlertCongestion,
            VIEW_ANIMATION_DURATION
        ) {
            checkAndShowVoucherBanner(voucherDetected, isDestinationCarPark)
        }
    }

    private fun checkAndShowVoucherBanner(voucherDetected: Boolean, isDestinationCarPark: Boolean) {
        if (!voucherDetected && !isDestinationCarPark) {
            return
        }
        if (voucherDetected) {
            viewBinding.voucherLayout.updateViewState(VoucherViewGroup.VoucherState.UNSELECTED)
            ViewAnimator.showViewSlideUp(viewBinding.voucherLayout, VIEW_ANIMATION_DURATION)
        } else if (isDestinationCarPark) {
            viewBinding.voucherLayout.updateViewState(VoucherViewGroup.VoucherState.PRESELECTED)
            ViewAnimator.showViewSlideUp(viewBinding.voucherLayout, VIEW_ANIMATION_DURATION)
        }
    }

    //Removing destination icon from 'routeLineResourceBuilder' and adding via
    //CarParkDestinationIconManager.addDestinationIcon()
    override fun showDestinationDrawable() {
        destinationDrawable = null
    }

    override fun inflateViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?,
        attachToContainer: Boolean
    ) = FragmentRoutePlanningBinding.inflate(inflater, container, attachToContainer)

    override fun getViewModelReference(): RoutePlanningViewModel {
        return viewModel
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let { bundle ->
            extractRouteData(bundle)
        }
        setFragmentFragmentResultListener()
    }

    /**
     * Extracts the following Bundle information
     * 1. The Destination Address Details [Constants.DESTINATION_ADDRESS_DETAILS]
     * 2. The Source Address Details [Constants.SOURCE_ADDRESS_DETAILS]
     * 3. (Optional) The Route Planning Mode, defaults to normal [Constants.ROUTE_PLANNING_MODE]
     * 4. (Optional) The Plan Trip Details, if not present starts normal route planning [Constants.ROUTE_PLANNING_TRIP_DETAILS]
     * 5. (Optional) The ERP Route List, if not present starts normal route planning [Constants.ERP_ROUTE_DETAILS_LIST]
     * 6. (Optional) The erp route timestamp, defaults to current time [Constants.SELECTED_ERP_ROUTE_TS]
     * 7. (Optional) The Selected ERP ROUTE index, defaults to 0 [Constants.SELECTED_ERP_ROUTE_INDEX]
     */
    private fun extractRouteData(bundle: Bundle) {
        extractRouteInformation(bundle)
        extractPlannedTripData(bundle)
        lifecycleScope.launch(Dispatchers.IO) {
            extractERPRouteData(bundle)
            isDataLoaded.postValue(true)
        }
    }

    /**
     *  Optional Trip Plan data, if trip is selected from travel log
     */
    private fun extractPlannedTripData(bundle: Bundle) {
        (bundle.getSerializable(Constants.ROUTE_PLANNING_MODE) as RoutePlanningMode?)?.let {
            routeplanningMode = it
        }
        (bundle.getSerializable(Constants.ROUTE_PLANNING_TRIP_DETAILS) as UpcomingTripDetailResponse?)?.let {
            plannedTripDetails = it
            if (plannedTripDetails?.tripPlanBy == DEPART_AT) {
                isDepartureDate = true
                plannedSelectedDate = Date(plannedTripDetails!!.tripEstStartTime!! * 1000)
            } else {
                isDepartureDate = false
                plannedSelectedDate = Date(plannedTripDetails!!.tripEstArrivalTime!! * 1000)
            }
            plannedTripDetails!!.routeStops.forEach { waypoint ->
                waypointManager.addWayPoint(waypoint)
            }
        }
    }

    /**
     *  Optional Route Selected from erp route preview
     *  These routes will be shown to the user
     */
    private suspend fun extractERPRouteData(bundle: Bundle) {
        (bundle.getByteArray(Constants.ERP_ROUTE_DETAILS_LIST))?.let { byteArray ->
            val it: List<SimpleRouteAdapterObjects> =
                GZIPCompressionUtil.decompressFromGzip(byteArray) as List<SimpleRouteAdapterObjects>
            currentRouteObjects.clear()
            currentRouteTypes.clear()
            calculateRoutePreference(it)?.let { rp ->
                currentRouteTypes.addAll(rp)
                currentRouteObjects.addAll(it)
            }
        }

        (bundle.getLong(Constants.SELECTED_ERP_ROUTE_TS, -1) as Long?)?.let {
            if (it != -1L) {
                isDepartureDate = true
                plannedSelectedDate = Date(it)
            }
        }
        (bundle.getInt(Constants.SELECTED_ERP_ROUTE_INDEX, -1) as Int?)?.let {
            if (it != -1) {
                CURRENT_SELECTED_ROUTE = it
            }
        }
    }

    /**
     *  These fields are mandatory and are required to plan a route
     */
    private fun extractRouteInformation(bundle: Bundle) {
        originalDestinationDetails =
            bundle.getSerializable(Constants.DESTINATION_ADDRESS_DETAILS)!! as DestinationAddressDetails

        (bundle.getSerializable(Constants.SOURCE_ADDRESS_DETAILS) as DestinationAddressDetails?)?.let {
            sourceAddressDetails = it
        }
    }


    private fun calculateRoutePreference(routes: List<SimpleRouteAdapterObjects>): List<RoutePreference>? {
        InitObjectUtilsController.mRoutePlanningHelper?.let { routePlaningHelper ->
            return@calculateRoutePreference routePlaningHelper.calculateMergedRoutePreference(
                routes.map { it.toRouteAdapterObjects() }
            );
        }
        return null
    }

    //FIXME: replace locationDetails["address1"].toString() to locationDetails["address1"] as String?
    private fun setFragmentFragmentResultListener() {
        setFragmentResultListener(Constants.ROUTE_PLANNING_FRAGMENT_REQUEST_KEY) { _, bundle ->
            //Handling multiple clicks during load, so can ignore click event completely
            if (view == null || !viewLoadComplete) {
                return@setFragmentResultListener
            }
            val locationDetails =
                bundle.getSerializable(Constants.SELECTED_ADDRESS_DETAILS) as HashMap<String, Any>
            (activity as DashboardActivity).hideKeyboard()
            if (checkIsSameLocation(locationDetails, isSourceLocation)) {
                showDialog2OriginAndDestinationSame()
                return@setFragmentResultListener
            }

            if (routeplanningMode == RoutePlanningMode.PLANNED) {
                routeplanningMode = RoutePlanningMode.NORMAL
            }

            DestinationAddressDetails.parseSearchLocationDataFromRN(locationDetails).let {
                if (isSourceLocation) {
                    sourceAddressDetails = it
                } else {
                    currentDestinationDetails = it
                    originalDestinationDetails = currentDestinationDetails
                }
            }
            /**
             * if location is same dont do anything
             */
            showOriginAndDestinationDetails()
            routeInitiate()
        }
    }


    private fun checkIsSameLocation(
        locationDetails: HashMap<String, Any>,
        isSourceLocation: Boolean
    ): Boolean {
        val locationName =
            if (locationDetails.get("name") != null) locationDetails["name"] else locationDetails.get(
                "address1"
            )
        if (isSourceLocation) {
            val destinationStr =
                if (currentDestinationDetails?.destinationName.isNullOrEmpty()) currentDestinationDetails?.address1 else currentDestinationDetails!!.destinationName
            if (locationName == destinationStr) {
                return true
            }
        } else {
            val currentLocationStr =
                if (sourceAddressDetails?.destinationName.isNullOrEmpty()) sourceAddressDetails?.address1 else sourceAddressDetails!!.destinationName
            if (locationName == currentLocationStr) {
                return true
            }
        }
        return false
    }

    fun navigateFromParkingCalculator(
        carParkId: String?,
        lat: String?,
        long: String?,
        name: String?
    ) {
        closeParkingCalculator()
        currentDestinationDetails = DestinationAddressDetails(
            lat = lat,
            long = long,
            destinationName = name,
            address1 = name,
            isCarPark = true,
            carParkID = carParkId
        )
        showOriginAndDestinationDetails()
        routeInitiate(false, true)
    }

    private fun triggerSelectAddressAPI() {
        val currentLoc = LocationBreezeManager.getInstance().currentLocation
        if (currentLoc != null) {
            viewModel.selectAddressAPI(
                currentDestinationDetails!!,
                currentLoc
            )
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        OBUStripStateManager.getInstance()?.disable()
        nightModeEnabled = breezeUserPreferenceUtil.isDarkTheme()
        mapboxMap = viewBinding.mapViewRoutePlan.getMapboxMap()

        currentDestinationDetails = originalDestinationDetails

        routeLabelHelper = RouteLabelHelper(requireContext(), mapboxMap)


        init()

        viewportDataSource = MapboxNavigationViewportDataSource(
            mapboxMap
        )
        navigationCamera = NavigationCamera(
            viewBinding.mapViewRoutePlan.getMapboxMap(),
            viewBinding.mapViewRoutePlan.camera,
            viewportDataSource,

            )
        viewBinding.mapViewRoutePlan.setBreezeDefaultOptions()

        if (PermissionsManager.areLocationPermissionsGranted(activity)) {
            initRouteLine()
            initStyle()
        }
// BREEZE2-1845
//        compositeDisposable.add(
//            RxBus.listen(RxEvent.ETARNDataCallback::class.java)
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe {
//                    etaRNCAllback(it.data)
//                })
//
//        compositeDisposable.add(
//            RxBus.listen(RxEvent.CloseNotifyArrivalScreen::class.java)
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe {
//                    closeEtaPopUp()
//                })
        listeRxEvents()
    }

    private fun listeRxEvents() {
        compositeDisposable.addAll(
            RxBus.listen(RxEvent.RoutePlanningOpenInvite::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    openInviteDialogRN(it.data)
                },
            RxBus.listen(RxEvent.RoutePlanningCloseInvite::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    closeInviteDialogRN()
                },

            )
    }

    private fun closeInviteDialogRN() {
        activity?.runOnUiThread {
            (childFragmentManager.findFragmentByTag(RNScreen.LOCATION_INVITE) as? CustomReactFragment)
                ?.takeIf { it.lifecycle.currentState.isAtLeast(Lifecycle.State.STARTED) }
                ?.let {
                    childFragmentManager.beginTransaction().remove(it).commit()
                }
        }
    }

    private fun openInviteDialogRN(data: Bundle) {
        val navigationParams = requireActivity().getRNFragmentNavigationParams(
            sessionToken = myServiceInterceptor.getSessionToken(),
            fragmentTag = RNScreen.LOCATION_INVITE
        ).apply {
            putString("fromScreen", "RoutePlanning")
            putBundle("data", data)
        }
        val initialProperties = Bundle().apply {
            putString(Constants.RN_TO_SCREEN, RNScreen.LOCATION_INVITE)
            putBundle(Constants.RN_CONSTANTS.NAVIGATION_PARAMS, navigationParams)
        }
        val reactNativeFragment = CustomReactFragment.newInstance(
            componentName = Constants.RN_CONSTANTS.COMPONENT_NAME,
            launchOptions = initialProperties,
            cleanOnDestroyed = false
        )
        childFragmentManager.beginTransaction()
            .replace(viewBinding.rnFragmentContainer.id, reactNativeFragment, RNScreen.LOCATION_INVITE)
            .addToBackStack(RNScreen.LOCATION_INVITE).commit()
    }


    private fun setUpPanTrackingCarPark() {
        if (activity != null) {

            /**
             * create instance PanTrackingManager
             */
            panTrackingManager = PanTrackingManager(viewBinding.mapViewRoutePlan)
            panTrackingManager?.isCarParkEnable =
                viewBinding.carParkViewOption.isCarParksShown()

            /**
             * should set enable carpark or not
             */
            val callBackUpdateCarParkWithNewLocation =
                object : PanTrackingManager.CallBackUpdateCarParkWithNewLocation {

                    override fun panStart() {
                        /**
                         * show camera when user start pan
                         */
                        viewBinding.cameraTracker.visibility = View.VISIBLE
                        isShouldRecenter = true
                    }

                    override fun updateCarParkNewLocation() {
                        val locationCenterPan = panTrackingManager?.currentLocationPan
                        locationCenterPan?.let { lc ->
                            handleGetCarParkWithPanMap()
                        }
                    }
                }
            panTrackingManager?.callBackUpdateCarParkWithNewLocation =
                callBackUpdateCarParkWithNewLocation
        }
    }


    private fun handleGetCarParkWithPanMap() {
        val locationCenterPan = panTrackingManager?.currentLocationPan ?: return
        if (viewBinding.carParkViewOption.currentState == CarParkViewOption.HIDE) return
        if (originalDestinationDetails == null) {
            enableAndDisplayCarparkToggle()
            return
        }
        val locationPan = Point.fromLngLat(
            locationCenterPan.longitude,
            locationCenterPan.latitude
        )

        if (locationPan != null && view != null) {
            viewModel.mListCarPark.observe(viewLifecycleOwner, panCarParkAmenityObserver)
            /**
             * show notification if not found any carpark in 500
             */
            viewModel.isNotExistAnyCp.observe(viewLifecycleOwner) { isNotExistAnyCpIn500 ->
                if (isNotExistAnyCpIn500 && viewBinding.carParkViewOption.isCarParksShown()) {
                    GlobalNotification()
                        .setIcon(R.drawable.ic_notification_no_carpark)
                        .setTheme(GlobalNotification.ThemeTutorial)
                        .setTitle(getString(R.string.nearby_carpark))
                        .setDescription(getString(R.string.msg_nearby_carpark_found))
                        .setActionText(null)
                        .setActionListener(null)
                        .setDismissDurationSeconds(10)
                        .show(activity)
                }
            }
            viewModel.fetchCarParks(
                locationPan,
                System.currentTimeMillis() / 1000,
                "",
                viewBinding.carParkViewOption.currentState.mode
            )

        }
    }


    /**
     * detect current user go to profile zone
     */
    private fun detectCurrentLocationInProfileZone() {
        LocationBreezeManager.getInstance().currentLocation.let {
            lifecycleScope.launch(Dispatchers.IO) {
                val currentLocationZoneData =
                    ProfileZoneLayer.detectAllProfileLocationInZone(it)
                currentLocationZoneFirstTime = currentLocationZoneData
            }
        }
    }

    private fun initMarkerViews() {
        val mapListenerCallBack = object :
            ListenerCallbackLandingMap {
            override fun moreInfoButtonClick(ameniti: BaseAmenity) {}

            override fun onDismissedCameraTracking() {}

            override fun onMapOtherPointClicked(point: Point) {
                val clicked = mapboxMap.pixelForCoordinate(point)
                mapboxMap.queryRenderedFeatures(
                    ScreenBox(
                        ScreenCoordinate(clicked.x - 5, clicked.y - 5),
                        ScreenCoordinate(clicked.x + 5, clicked.y + 5)
                    ),
                    //RenderedQueryOptions(listOf("earthquakeCircle", "earthquakeText"), literal(true))
                    RenderedQueryOptions(null, null)
                ) {
                    handleMapFeatureClick(point)
                }
            }

            override fun onERPInfoRequested(erpID: Int) {
                var erpTimeStamp = System.currentTimeMillis()
                if (routeplanningMode != RoutePlanningMode.NORMAL) {
                    val routeData = currentRouteObjects[CURRENT_SELECTED_ROUTE]
                    erpTimeStamp = getPlannedDepartureTime(routeData)
                }
                (requireActivity() as DashboardActivity).showERPInfoRN(
                    erpId = erpID,
                    timestamp = erpTimeStamp
                )
            }

            override fun poiMoreInfoButtonClick(ameniti: BaseAmenity) {
                //
            }
        }
        mapCongestionManager = MapCongestionManager(
            mapboxMap,
            requireActivity().applicationContext,
            mapboxMap.getStyle()!!
        )
        mapERPManager = MapERPManager(
            viewBinding.mapViewRoutePlan, requireActivity(), getScreenName(),
            true, mapListenerCallBack
        )
    }

    override fun clearMarkerViews() {
        mapERPManager.removeAllMarkers()
        routeLabelHelper.clearRoadLabels()
    }


    @SuppressLint("ClickableViewAccessibility")
    private fun cardAdapterInitialize() {
        mAdapter = RouteViewPagerAdapter(
            requireContext(),
            RouteDepartureDetails(
                plannedSelectedDate?.time ?: System.currentTimeMillis(),
                if (isDepartureDate) DepartureType.DEPARTURE else DepartureType.ARRIVAL
            ),
            routeplanningMode,
            currentRouteObjects,
            viewBinding.rvRouteCards
        )
        viewBinding.rvRouteCards.adapter = mAdapter
        viewBinding.rvRouteCards.onFlingListener = null;
        snapHelper = PagerSnapHelper()
        snapHelper.attachToRecyclerView(viewBinding.rvRouteCards)

        // Note: Prevents all touch listeners, to prevent only scroll listener can override layoutmanager onscroll instead
        viewBinding.rvRouteCards.setOnTouchListener { _: View, _: MotionEvent ->
            return@setOnTouchListener true;
        }
    }

    private fun showCarPark() {
        if (viewBinding.carParkViewOption.isCarParksShown()) {
            Analytics.logToggleEvent(Event.PARKING_ON, getScreenName())
            resetAllViews()
            viewBinding.progressBar.visibility = View.VISIBLE
            fetchCarPark()
            if (viewBinding.voucherLayout.visibility != View.GONE) {
                viewBinding.voucherLayout.updateViewState(VoucherViewGroup.VoucherState.HIDDEN)
            }
        } else {
            Analytics.logToggleEvent(Event.PARKING_OFF, getScreenName())
            hideCarParks()
        }
    }


    /**
     * hide carkpark
     */
    private fun hideCarParks() {
        resetAllViews()
        amenityMarkerManager?.hideNonWaypointMarkers(CARPARK)
        carParkDestinationIconManager.showDestinationIcon()
        resetCameraZoom(mapboxMap)
    }


    private fun fetchCarPark() {

        if (viewBinding.carParkViewOption.currentState == CarParkViewOption.HIDE) return

        if (originalDestinationDetails == null) {
            enableAndDisplayCarparkToggle()
            return
        }

        val destinationPoint =
            originalDestinationDetails?.let {
                if (it.long != null && it.lat != null) {
                    Point.fromLngLat(
                        it.long!!.toDouble(),
                        it.lat!!.toDouble()
                    )
                } else null
            }

        if (destinationPoint != null) {
            viewModel.mListCarPark.observe(viewLifecycleOwner, carParkAmenityObserver)

            /**
             * show notification if not found any carpark in 500
             */
            viewModel.isNotExistAnyCp.observe(viewLifecycleOwner) { isNotExistAnyCpIn500 ->
                if (isNotExistAnyCpIn500 && viewBinding.carParkViewOption.isCarParksShown()) {
                    GlobalNotification()
                        .setIcon(R.drawable.ic_notification_no_carpark)
                        .setTheme(GlobalNotification.ThemeTutorial)
                        .setTitle(getString(R.string.nearby_carpark))
                        .setDescription(getString(R.string.msg_nearby_carpark_found))
                        .setActionText(null)
                        .setActionListener(null)
                        .setDismissDurationSeconds(10)
                        .show(activity)
                }
            }

            val destinationName =
                if (originalDestinationDetails?.destinationName.isNullOrEmpty()) originalDestinationDetails?.address1 else originalDestinationDetails?.destinationName

            if (routeplanningMode != RoutePlanningMode.NORMAL) {
                plannedSelectedDate?.let { date ->
                    viewModel.fetchCarParks(
                        destinationPoint,
                        date.time / 1000,
                        destinationName,
                        viewBinding.carParkViewOption.currentState.mode
                    )
                }
            } else {
                viewModel.fetchCarParks(
                    destinationPoint,
                    System.currentTimeMillis() / 1000,
                    destinationName,
                    viewBinding.carParkViewOption.currentState.mode
                )
            }

        }
    }

    private fun enableAndDisplayCarparkToggle() {
        viewBinding.progressBar.visibility = View.GONE
        amenityMarkerManager?.showAllMarker(CARPARK)
    }

    /**
     * adjust zoom to show all carpark around destination
     */
    private fun adjustZoomToShowAllCarParks(carParkAmenities: ArrayList<SelectedAmenity>) {

//        ViewAnimator.measureView(viewBinding.bottomView)
//        ViewAnimator.measureView(viewBinding.routeLlHeading)
//        ViewAnimator.measureView(viewBinding.rlAmenitySelection)
//        ViewAnimator.measureView(viewBinding.carParkViewOption)

        if (carParkAmenities.size > 0 && panTrackingManager?.isInPanMode == false && zoomCameraOptions != null) {
            val listPoint = arrayListOf<Point>()
            carParkAmenities.forEach {
                listPoint.add(
                    Point.fromLngLat(
                        it.baseAmenity.long ?: 0.0,
                        it.baseAmenity.lat ?: 0.0,
                    )
                )
            }
            anotherPoints = listPoint
            routeObjects?.let {
                resetCameraToRoutes(
                    it,
                    mapboxMap,
                    viewBinding.bottomView,
                    viewBinding.routeLlHeading,
                    viewBinding.carParkViewOption,
                    null
                )
            }

        }
    }

    private fun showCarParkToolTip(amenity: SelectedAmenity) {
        val amenitiesMarkerView = RouteCarParkMarkerView(
            LatLng(amenity.baseAmenity.lat!!, amenity.baseAmenity.long!!),
            mapboxMap,
            requireActivity(),
            amenity.baseAmenity.id
        )
        val properties = JsonObject()
        amenityMarkerHelper?.handleSelectedCarParkProperties(
            amenity.baseAmenity,
            properties,
            true,
            false
        )
        val feature = Feature.fromGeometry(
            Point.fromLngLat(
                amenity.baseAmenity.long!!,
                amenity.baseAmenity.lat!!
            ), properties, amenity.baseAmenity.id
        )
        amenityMarkerManager?.renderMarkerToolTipView(
            MarkerLayerManager.MarkerViewType(
                amenitiesMarkerView,
                CARPARK,
                feature
            )
        )

        /**
         * call api to check show broadcast Messasge
         */
        getCarparkBroadcastMessage(amenity.baseAmenity.id)


        amenitiesMarkerView.clickToopTip(
            amenity.baseAmenity,
            true,
            navigationButtonClick = { item, _ ->
                Analytics.logClickEvent(Event.PARKING_NAVIGATE_HERE, getScreenName())
                val wasCurrentDestinationCarPark: Boolean =
                    currentDestinationDetails?.isDestinationCarPark() ?: false
                initCarParkDestinationDetails(item = item, updateOriginalDest = false)
                showOriginAndDestinationDetails()
                if (CALCULATE_WALKING_PATH_IN_ROUTEPLANNING) {
                    selectedCarParkDestinationIconRes =
                        ParkingIconUtils.getCarParkIconForNavigation(item, isDestination = true)
                    if (wasCurrentDestinationCarPark) {
                        selectedCarParkIconRes = -1
                        routeInitiate(
                            clearWaypoints = false,
                            refreshAmenities = false,
                            showWalkingRoute = false
                        )
                    } else {
                        selectedCarParkIconRes = ParkingIconUtils.getCarParkIconForNavigation(
                            item,
                            isDestination = false
                        )
                        routeInitiate(
                            clearWaypoints = false,
                            refreshAmenities = false,
                            showWalkingRoute = true
                        )
                    }
                } else {
                    selectedCarParkDestinationIconRes = -1
                    selectedCarParkIconRes = -1
                    routeInitiate(
                        clearWaypoints = false,
                        refreshAmenities = false,
                        showWalkingRoute = false
                    )
                }
            },
            moreInfoButtonClick = { item ->
                Analytics.logClickEvent(Event.PARKING_MORE_INFO, getScreenName())
                (requireActivity() as DashboardActivity).showParkingInfoRN(
                    parkingId = item.id!!
                )
            },
            carParkIconClick = {},
            calculateFee = {
                Analytics.logClickEvent(Event.PARKING_CALCULATE_FEE, getScreenName())
                showCalculateParking(it.id)
            },
            getScreenName()
        )
    }

    /**
     * check show broadcast message first time
     */
    private fun checkCarParkBroadcast() {
        val selectedRoutablePoint = currentDestinationDetails?.getSelectedRoutablePoint()
        val canShowBroadcast = if (selectedRoutablePoint == null) {
            !currentDestinationDetails?.carParkID.isNullOrEmpty()
        } else {
            !selectedRoutablePoint.carparkId.isNullOrEmpty()
        }
        if (canShowBroadcast) {
            getCarparkBroadcastMessage(currentDestinationDetails?.getSelectedCarParkID())
            getCarParkDetail(currentDestinationDetails?.getSelectedCarParkID())
        }
    }

    /**
     * Api get message broadcast Message
     */
    private fun getCarparkBroadcastMessage(idCarpark: String?) {
        idCarpark?.let {
            viewModel.mBroadcastMessage.observe(
                viewLifecycleOwner
            ) { broadcastData ->
                viewModel.mBroadcastMessage.removeObservers(viewLifecycleOwner)

                broadcastData?.apply {
                    if (success && message != "") {
                        mCurrentNotificationCriterias = this.notificationCriterias
                        mCurrentMessageBroadcastMessageShouldShow = this.message
                    }
                }
            }
            viewModel.getBroadcastMessage(idCarpark)
        }
    }


    /**
     * get carpark detail
     */
    private fun getCarParkDetail(idCarpark: String?) {
        if (currentDestinationDetails?.isDestinationCarPark() == true) {
            idCarpark?.let {
                viewModel.carParkDetail.observe(
                    viewLifecycleOwner
                ) { carparkDetailResponse ->
                    viewModel.carParkDetail.removeObservers(viewLifecycleOwner)
                    carparkDetailResponse?.let {
                        if (carparkDetailResponse.success && carparkDetailResponse.data != null) {
                            carparkDetailResponse.data?.apply {
                                currentDestinationDetails?.availablePercentImageBand =
                                    availablePercentImageBand
                                currentDestinationDetails?.showAvailabilityFB = showAvailabilityFB
                                currentDestinationDetails?.hasAvailabilityCS = hasAvailabilityCS
                                currentDestinationDetails?.availabilityCSData = availabilityCSData
                            }
                        }
                    }
                }
                viewModel.getCarparkDetail(idCarpark)
            }
        }
    }

    private fun getCarparkDetailAndReroute(id: String) {
        viewModel.getCarparkDetailWithCallback(id) { baseAmenity ->
            baseAmenity?.let {
                viewBinding.tvAlertBannerRoutePlanView.hideAnimation()

                initCarParkDestinationDetails(item = it, updateOriginalDest = false)
                showOriginAndDestinationDetails()
                selectedCarParkDestinationIconRes = -1
                selectedCarParkIconRes = -1
                routeInitiate(
                    clearWaypoints = false,
                    refreshVouchers = false,
                    refreshAmenities = false,
                    showWalkingRoute = true
                )
            }

        }
    }


    private fun showCalculateParking(parkingID: String?) {

        val locationFocus = getLocationFocusForReact()
        val navigationParams = requireActivity().getRNFragmentNavigationParams(
            sessionToken = (activity as? DashboardActivity)?.myServiceInterceptor?.getSessionToken(),
            fragmentTag = RNScreen.PARKING_CALCULATOR
        ).apply {
            putString("parkingID", parkingID)
            if (routeplanningMode != RoutePlanningMode.NORMAL) {
                putLong("start_timestamp", plannedSelectedDate?.time ?: System.currentTimeMillis())
            } else {
                putLong("start_timestamp", System.currentTimeMillis())
            }
            locationFocus.let { loc ->
                putString(Constants.RN_CONSTANTS.FOCUSED_LAT, loc.latitude.toString())
                putString(Constants.RN_CONSTANTS.FOCUSED_LONG, loc.longitude.toString())
            }
        }
        val initialProperties = Bundle().apply {
            putString(Constants.RN_TO_SCREEN, RNScreen.PARKING_CALCULATOR)
            putBundle(Constants.RN_CONSTANTS.NAVIGATION_PARAMS, navigationParams)
        }


        val reactNativeFragment = CustomReactFragment.newInstance(
            componentName = Constants.RN_CONSTANTS.COMPONENT_NAME,
            launchOptions = initialProperties,
            cleanOnDestroyed = false
        )

        childFragmentManager
            .beginTransaction()
            .add(R.id.rnFragmentContainer, reactNativeFragment, RNScreen.PARKING_CALCULATOR)
            .addToBackStack(RNScreen.PARKING_CALCULATOR)
            .commit()
    }


    /**
     * get location for react
     */
    private fun getLocationFocusForReact(): Location {
        val location = Location("")

        if (panTrackingManager?.isInPanMode == true) {
            /**
             * in pan mode
             */
            location.latitude = panTrackingManager?.currentLocationPan?.latitude ?: 0.0
            location.longitude = panTrackingManager?.currentLocationPan?.longitude ?: 0.0
        } else {
            /**
             * not in pan mode => return location destination
             */
            location.latitude = currentDestinationDetails?.lat?.toDouble() ?: 0.0
            location.longitude = currentDestinationDetails?.long?.toDouble() ?: 0.0
        }
        return location
    }

// BREEZE2-1845
//    private fun showShareLiveLocation() {
//
//        val navigationParams = requireActivity().getRNFragmentNavigationParams(
//            sessionToken = (activity as? DashboardActivity)?.myServiceInterceptor?.getSessionToken(),
//            fragmentTag = RNScreen.NOTIFY_ARRIVAL
//        ).apply {
//            val arrivalTime = currentRouteObjects[CURRENT_SELECTED_ROUTE].arrivalTime
//            putString("etamessage", resources.getString(R.string.eta_message_template))
//            putString("username", breezeUserPreferenceUtil.retrieveUserName())
//            putString("location", currentDestinationDetails?.address1 ?: "")
//            putString("eta", RouteDisplayUtils.formatArrivalTimeWithTime12H(arrivalTime))
//            putString(Constants.RN_FROM_SCREEN, Screen.ROUTE_PLANNING_MODE)
//        }
//        val initialProperties = Bundle().apply {
//            putString(Constants.RN_TO_SCREEN, RNScreen.NOTIFY_ARRIVAL)
//            putBundle(Constants.RN_CONSTANTS.NAVIGATION_PARAMS, navigationParams)
//        }
//
//        val reactNativeFragment = CustomReactFragment.newInstance(
//            componentName = Constants.RN_CONSTANTS.COMPONENT_NAME,
//            launchOptions = initialProperties,
//            cleanOnDestroyed = false
//        )
//
//        childFragmentManager
//            .beginTransaction()
//            .add(R.id.rnFragmentContainer, reactNativeFragment, RNScreen.NOTIFY_ARRIVAL)
//            .commit()
//    }


    fun closeParkingCalculator() {
        childFragmentManager.findFragmentByTag(RNScreen.PARKING_CALCULATOR)?.let {
            childFragmentManager.beginTransaction().remove(it).commitNow()
        }
    }


    // BREEZE2-1845
//    fun closeEtaPopUp() {
//        childFragmentManager.findFragmentByTag(RNScreen.NOTIFY_ARRIVAL)?.let {
//            childFragmentManager.beginTransaction().remove(it).commitNow()
//        }
//    }


    private fun fetchAmenities(originPoint: Point?, destPoint: Point?) {
        if (originPoint != null && destPoint != null) {
            viewModel.mListAmenities.observe(viewLifecycleOwner, amenityObserver)
            if (routeplanningMode != RoutePlanningMode.NORMAL) {
                viewModel.fetchAmenities(originPoint, destPoint, plannedSelectedDate!!.time / 1000)
            } else {
                viewModel.fetchAmenities(originPoint, destPoint, System.currentTimeMillis() / 1000)
            }
        }
    }

    private fun resetAllViews() {
        amenityMarkerManager?.removeMarkerTooltipView()
    }


    private fun initCarParkDestinationDetails(
        item: BaseAmenity,
        updateOriginalDest: Boolean = true
    ) {
        currentDestinationDetails = DestinationAddressDetails(
            null,
            item.name,
            null,
            item.lat.toString(),
            item.long.toString(),
            null,
            item.name,
            null,
            -1,
            true,
            carParkID = item.id,
            availablePercentImageBand = item.availablePercentImageBand,
            showAvailabilityFB = item.showAvailabilityFB
        )
        if (updateOriginalDest) {
            originalDestinationDetails = currentDestinationDetails
        }
    }

    /**
     * handle carparking in routeplainning
     */
    private fun handleAmenities(amenitiesDataMap: ConcurrentHashMap<String, AmenityData>) {
        lifecycleScope.launch(Dispatchers.IO) {
            amenitiesDataMap.forEach {
                try {
                    ensureActive()
                    amenityMarkerManager?.addStyleImages(it.key)
                    withContext(Dispatchers.Main) {
                        handleAmenityTypeMapLayerCreation(it.key, it.value.amenities, true)
                    }
                } catch (e: CancellationException) {
                }
            }
        }
    }

    private fun clearMarkersByType(type: String) {
        amenityMarkerManager?.removeNonWaypointMarkers(type)
    }

    private fun handleAmenityTypeMapLayerCreation(
        type: String,
        amenities: ArrayList<SelectedAmenity>,
        hide: Boolean
    ) {
        val amenitiesSelected: ArrayList<BaseAmenity> = ArrayList()
        val wayPointsSelected: ArrayList<BaseAmenity> = ArrayList()
        clearMarkersByType(type)
        amenities.forEach {
            val id = it.baseAmenity.id
            if (id != null && waypointManager.wayPoints[id] != null) {
                amenitiesSelected.add(it.baseAmenity)
                wayPointsSelected.add(it.baseAmenity)
            } else if (it.selected) {
                amenitiesSelected.add(it.baseAmenity)
            }
        }
        amenityMarkerHelper?.addWaypointAmenityMarkerView(
            type,
            amenitiesSelected,
            wayPointsSelected,
            hide,
            true
        )
        if (amenities.size > 0) {
            viewBinding.mapViewRoutePlan.updateDestinationAndLocationPuckLayer()
        }
    }


    // BREEZE2-1845
//    fun openEditETAScreen(screenName: String) {
//        val navigationParams = requireActivity().getRNFragmentNavigationParams(
//            sessionToken = myServiceInterceptor.getSessionToken(),
//            fragmentTag = screenName
//        )
//        navigationParams.putString(
//            Constants.RN_CONSTANTS.OPEN_AS,
//            Constants.RN_CONSTANTS.OPEN_AS_FRAGMENT
//        )
//        navigationParams.putString(Constants.RN_CONSTANTS.FRAGMENT_ID, screenName)
//        navigationParams.putString(
//            Constants.TRIPETA.ETA_MODE,
//            Constants.TRIPETA.ETA_MODE_NAVIGATION
//        )
//        navigationParams.putString(Constants.TRIPETA.ETA_DESTINATION, "")
//        navigationParams.putString(Constants.TRIPETA.ETA_LAT, currentDestinationDetails?.lat)
//        navigationParams.putString(Constants.TRIPETA.ETA_LONG, currentDestinationDetails?.long)
//        navigationParams.putString(Constants.TRIPETA.CONTACT_NAME, etaRequest!!.recipientName)
//        navigationParams.putString(Constants.TRIPETA.CONTACT_NUMBER, etaRequest!!.recipientNumber)
//        navigationParams.putString(Constants.TRIPETA.ETA_MESSAGE, etaRequest!!.message)
//        navigationParams.putString(Constants.TRIPETA.ETA_RN_STATUS, Constants.TRIPETA.ETA_RN_EDIT)
//        navigationParams.putBoolean(
//            Constants.TRIPETA.ETA_RN_SEND_VOICE,
//            TextUtils.equals(etaRequest!!.voiceCallToRecipient, Constants.TRIPETA.YES)
//        )
//        navigationParams.putBoolean(
//            Constants.TRIPETA.ETA_RN_SHARE_LIVE_LOC,
//            TextUtils.equals(etaRequest!!.shareLiveLocation, Constants.TRIPETA.YES)
//        )
//
//        val initialProperties = Bundle()
//        initialProperties.putString(Constants.RN_TO_SCREEN, screenName)
//        initialProperties.putBundle(Constants.RN_CONSTANTS.NAVIGATION_PARAMS, navigationParams)
//
//
//        val reactNativeFragment: Fragment = ReactFragment.Builder()
//            .setComponentName(Constants.RN_CONSTANTS.COMPONENT_NAME)
//            .setLaunchOptions(initialProperties)
//            .build()
//
//        (requireActivity() as DashboardActivity).supportFragmentManager
//            .beginTransaction()
//            .add(R.id.frame_container, reactNativeFragment, screenName)
//            .addToBackStack(screenName)
//            .commit()
//    }
//
//
//    fun openETAScreen(screenName: String, contactDetails: Utils.ContactDetails) {
//        val navigationParams = requireActivity().getRNFragmentNavigationParams(
//            sessionToken = myServiceInterceptor.getSessionToken(),
//            fragmentTag = screenName
//        )
//
//        navigationParams.putString(
//            Constants.RN_CONSTANTS.OPEN_AS,
//            Constants.RN_CONSTANTS.OPEN_AS_FRAGMENT
//        )
//        navigationParams.putString(Constants.RN_CONSTANTS.FRAGMENT_ID, screenName)
//        navigationParams.putString(
//            Constants.TRIPETA.ETA_MODE,
//            Constants.TRIPETA.ETA_MODE_NAVIGATION
//        )
//        navigationParams.putString(Constants.TRIPETA.ETA_DESTINATION, "")
//        navigationParams.putString(Constants.TRIPETA.ETA_LAT, currentDestinationDetails?.lat)
//        navigationParams.putString(Constants.TRIPETA.ETA_LONG, currentDestinationDetails?.long)
//        navigationParams.putString(Constants.TRIPETA.CONTACT_NAME, contactDetails.contactName)
//        navigationParams.putString(
//            Constants.TRIPETA.CONTACT_NUMBER,
//            contactDetails.phoneNumberList.get(0)
//        )
//        navigationParams.putString(
//            Constants.TRIPETA.ETA_MESSAGE,
//            "Hey, ${getCurrentUserName()} here! I will be around ${currentDestinationDetails?.address1} at ${currentRouteObjects[CURRENT_SELECTED_ROUTE].arrivalTime}."
//        )
//        navigationParams.putString(Constants.TRIPETA.ETA_RN_STATUS, Constants.TRIPETA.ETA_RN_INIT)
//        navigationParams.putBoolean(Constants.TRIPETA.ETA_RN_SEND_VOICE, false)
//        navigationParams.putBoolean(Constants.TRIPETA.ETA_RN_SHARE_LIVE_LOC, true)
//
//        val initialProperties = Bundle()
//        initialProperties.putString(Constants.RN_TO_SCREEN, screenName)
//        initialProperties.putBundle(Constants.RN_CONSTANTS.NAVIGATION_PARAMS, navigationParams)
//
//
//        val reactNativeFragment: Fragment = ReactFragment.Builder()
//            .setComponentName(Constants.RN_CONSTANTS.COMPONENT_NAME)
//            .setLaunchOptions(initialProperties)
//            .build()
//
//        (requireActivity() as DashboardActivity).supportFragmentManager
//            .beginTransaction()
//            .add(R.id.frame_container, reactNativeFragment, screenName)
//            .addToBackStack(screenName)
//            .commit()
//        //(requireActivity() as DashboardActivity).supportFragmentManager.executePendingTransactions()
//
//    }


    private fun init() {
        if (currentDestinationDetails != null) {
            triggerSelectAddressAPI()
        }
        (activity as DashboardActivity).hideKeyboard()

        viewBinding.routeDayPicker.setSelectorColor(
            ContextCompat.getColor(
                viewBinding.root.context,
                R.color.themed_text_grey_profile
            )
        )
        viewBinding.routeDayPicker.setSelectedTextColor(
            ContextCompat.getColor(
                viewBinding.root.context,
                R.color.themed_address_font_color
            )
        )
        viewBinding.routeDayPicker.setIsAmPm(true)//12hr format
//        viewBinding.routeDayPicker?.setDefaultDate(Date())//if not, the current time in the picker will display as AM, until we select a particular time
        viewBinding.tvAlertBannerRoutePlanView.setOnClickCallback { data ->
            when (data) {
                is BroadcastData -> {

                    data.action?.data?.id?.let { carparkId ->
                        when (data.alertLevel?.lowercase()) {
                            "info" -> {
                                Analytics.logClickEvent(
                                    Event.POPUP_LIMITED_PARKING_SPACE_REROUTE_WALK,
                                    getScreenName()
                                )
                            }

                            else -> {
                                Analytics.logClickEvent(
                                    Event.POPUP_CARPARK_FULL_REROUTE_WALK,
                                    getScreenName()
                                )
                            }
                        }
                        shouldShowNCSBroadcast = false
                        getCarparkDetailAndReroute(carparkId)
                    }
                }

                else -> {
                    Timber.d("tvAlertBannerRoutePlanView: $data")
                }
            }
        }
        initManeuverApi()
        showOriginAndDestinationDetails()
        initAdapters()
        initClickListeners()
        disableNavAmenityButtons()

        viewBinding.cameraTracker.setOnClickListener {
            /**
             * disable pan mode
             * if user showing carpark => zoom destination + show carpark around
             * else -> reset current route
             */
            viewBinding.cameraTracker.visibility = View.GONE
            isShouldRecenter = false
            panTrackingManager?.isInPanMode = false
            if (viewBinding.carParkViewOption.isCarParksShown()) {
                showCarPark()
            } else {
                hideCarParks()
            }

        }


        /**
         * collapse carpark option when touch any web
         */
        viewBinding.mainRl.setDispatchListener(object :
            DispatchTouchLayout.OnDispatchListener {
            override fun onTouchDown(x: Float, y: Float) {
                if (!viewBinding.mainRl.isContainView(
                        viewBinding.carParkViewOption,
                        x.toInt(),
                        y.toInt()
                    )
                ) {
                    viewBinding.carParkViewOption.collapse()
                }
            }
        })

        viewBinding.carParkViewOption.setListenerTransition(object :
            CarParkStateView.ListenTransition {
            override fun collapse() {
                if (isShouldRecenter) {
                    viewBinding.cameraTracker.visibility = View.VISIBLE
                } else {
                    viewBinding.cameraTracker.visibility = View.GONE
                }
            }

            override fun expand() {
                viewBinding.cameraTracker.visibility = View.GONE
            }
        })

        /**
         * setup pantracking
         */
        setUpPanTrackingCarPark()
    }


    /**
     * show carpark broadcast message
     */
    private fun showCarParkBroadcastMessage(
        title: String? = null,
        message: String,
        notificationCriterias: ArrayList<ValueConditionShowBroadcast>?,
        theme: AlertRoutePlanView.Theme = AlertRoutePlanView.ThemeDefault,
        otherData: Any? = null
    ) {
        viewBinding.tvAlertBannerRoutePlanView.setData(
            theme,
            title,
            message,
            notificationCriterias,
            otherData
        )
        /**
         * reset current broadcast message
         */
        Timber.e("showCarParkBroadcastMessage: ${viewBinding.layoutAlertCongestion.isVisible}-${viewBinding.voucherLayout.isVisible}-${viewBinding.tvAlertBannerRoutePlanView.isVisible}")
        mCurrentMessageBroadcastMessageShouldShow = ""
        if ((!viewBinding.layoutAlertCongestion.isVisible)
            && (!viewBinding.voucherLayout.isVisible)
            && (!viewBinding.tvAlertBannerRoutePlanView.isVisible)
        ) {
            // viewBinding.bottomView.visibility = View.VISIBLE
            viewBinding.tvAlertBannerRoutePlanView.showAnimation()
        }
    }


    /**
     * hide carpark broadcast message
     */
    private fun hideCarparkBroadCastMessage() {
        if (viewBinding.tvAlertBannerRoutePlanView.isVisible) {
            viewBinding.tvAlertBannerRoutePlanView.hideAnimation()
        }
    }

    private fun initManeuverApi() {
        maneuverApi = MapboxManeuverApi(
            MapboxDistanceFormatter(
                DistanceFormatterOptions.Builder(requireContext())
                    .locale(Locale(Constants.MAPBOX_DISTANCE_FORMATTER_LOCALE))
                    .build()
            )
        )
    }


    private fun initClickListeners() {

        handleFindPetrolNearBy()
        handleFindEVChangerNearBy()

        viewBinding.voucherLayout.voucherClickHandler = voucherClickHandler

        /**
         * new carPark option
         */
        panTrackingManager?.isCarParkEnable = viewBinding.carParkViewOption.isCarParksShown()
        viewBinding.carParkViewOption.setListener(object :
            CarParkStateView.ListenerFilterCarParkOption {
            override fun all() {
                Analytics.logClickEvent(Event.CAR_PARK_ALL_NEARBY, getScreenName())
                ViewAnimator.hideViewSlideDown(
                    viewBinding.layoutAlertCongestion,
                    VIEW_ANIMATION_DURATION
                )
                /**
                 * update is carpark enable for PanTrackingManager
                 */
                panTrackingManager?.isCarParkEnable =
                    viewBinding.carParkViewOption.isCarParksShown()
                if (panTrackingManager?.isInPanMode == true) {
                    handleGetCarParkWithPanMap()
                } else {
                    showCarPark()
                }
            }

            override fun available() {
                Analytics.logClickEvent(Event.CAR_PARK_AVAILABLE_ONLY, getScreenName())
                ViewAnimator.hideViewSlideDown(
                    viewBinding.layoutAlertCongestion,
                    VIEW_ANIMATION_DURATION
                )
                /**
                 * update is carpark enable for PanTrackingManager
                 */
                panTrackingManager?.isCarParkEnable =
                    viewBinding.carParkViewOption.isCarParksShown()
                if (panTrackingManager?.isInPanMode == true) {
                    handleGetCarParkWithPanMap()
                } else {
                    showCarPark()
                }
            }

            override fun hide() {
                Analytics.logClickEvent(Event.CAR_PARK_HIDE, getScreenName())
                anotherPoints = listOf()
                // Hide carparks
                hideCarParks()
            }
        })



        viewBinding.departTime.setOnClickListener {
            showTimePickerBottomPanel()
        }

        viewBinding.amenitySelectionHeaderClose.setOnClickListener {
            viewBinding.amenities.setAllButtonsToUnselectedState()
            showMainBottomPanel()
        }


        viewBinding.backButton.setOnClickListener {
            postEventWithCacheLast(AppToRoutePlanningEndEvent())
            onBackPressed()
        }

        // BREEZE2-1845
//        viewBinding.rlEtaButtons.setOnClickListener {
//            handleNotifyArrival()
//        }

        viewBinding.swapRoute.setOnClickListener {
            Analytics.logClickEvent(Event.SWAP_ROUTE_CLICK, Screen.ROUTE_PLANNING)
            if (routeplanningMode == RoutePlanningMode.PLANNED) {
                routeplanningMode = RoutePlanningMode.NORMAL
            }

            /**
             * hide heavy traffic congestion banner first
             */
            viewBinding.layoutAlertCongestion.visibility = View.GONE
            hideCarparkBroadCastMessage()
            swapOriginAndDestinationDetails()
            routeInitiate()
        }

        viewBinding.currentLocationTextView.setOnClickListener {
            Analytics.logClickEvent(Event.SEARCH_ORIGIN_CLICK, Screen.ROUTE_PLANNING)
            isSourceLocation = true
            (requireActivity() as DashboardActivity).openSearch()
        }

        viewBinding.destinationLocationTextView.setOnClickListener {
            Analytics.logClickEvent(Event.SEARCH_DESTINATION_CLICK, Screen.ROUTE_PLANNING)
            isSourceLocation = false
            (requireActivity() as DashboardActivity).openSearch()
        }

        viewBinding.routePlanningGo.setOnClickListener {
            validateCurLocAndConfirmNavigation()
        }

        viewBinding.routePlanningLater.setOnClickListener {
            Analytics.logClickEvent(Event.LATER, Screen.ROUTE_PLANNING)
            sendLocationAnalyticsOrigin(Event.LATER_ORIGIN)
            sendLocationAnalyticsDestination(Event.LATER_DESTINATION)
            sendLocationAnalyticsWayPointsAdded(Event.LATER_ADDED_STOP)

            showTimePickerBottomPanel()
        }
        viewBinding.routePlanningSave.setOnClickListener {
            createTripPlan()
        }
        viewBinding.pickerCancel.setOnClickListener {
            Analytics.logClickEvent(Event.DATETIME_CANCEL, Screen.ROUTE_PLANNING)
            showMainBottomPanel()
        }

        viewBinding.pickerNext.setOnClickListener {
            Analytics.logClickEvent(Event.DATETIME_NEXT, Screen.ROUTE_PLANNING)
            val curDate = Date()
            if (routeplanningMode != RoutePlanningMode.PLANNED_RESUME) {
                routeplanningMode = RoutePlanningMode.PLANNED
            }
            plannedSelectedDate =
                if (curDate.time > viewBinding.routeDayPicker.date.time) curDate else viewBinding.routeDayPicker.date
            isDepartureDate = viewBinding.pickerDepart.buttonSelected
            routeInitiate(false, true)
        }

        viewBinding.routePlannedGo.setOnClickListener {
            validateCurLocAndConfirmNavigation()
        }

        viewBinding.routePlannedSave.setOnClickListener {
            updateTrip()
        }

        viewBinding.rlDirectionsButton.setOnClickListener {
            Analytics.logClickEvent(Event.DIRECTION_CLICK, Screen.ROUTE_PLANNING)
            val currentRoute = currentRouteObjects[CURRENT_SELECTED_ROUTE].route
            ManueverListDialogFragment.Builder()
                .renderManeuvers(maneuverApi.getManeuvers(currentRoute))
                .build().show(
                    childFragmentManager, ManueverListDialogFragment::class.simpleName
                )
        }

        viewBinding.rlShareDestination.setOnClickListener {
            shareDestination()
        }
        viewBinding.pickerDepart.callOnClick()
    }

    private fun shareDestination() {
        Analytics.logClickEvent(Event.SHARE_DESTINATION, Screen.ROUTE_PLANNING_MODE)
        context?.getShareBusinessLogicPackage()?.getRequestsModule()?.flatMap {
            val name = when {
                !originalDestinationDetails?.destinationName.isNullOrEmpty() -> {
                    if (originalDestinationDetails?.destinationName != Constants.TAGS.CURRENT_LOCATION_TAG) {
                        originalDestinationDetails?.destinationName
                    } else {
                        originalDestinationDetails?.address1
                    }
                }

                !originalDestinationDetails?.address1.isNullOrEmpty() -> originalDestinationDetails?.address1
                else -> ""
            }
            it.callRN(
                ShareBusinessLogicEvent.SHARE_LOCATION_FROM_NATIVE.eventName,
                Arguments.fromBundle(
                    bundleOf(
                        "address1" to (originalDestinationDetails?.address1 ?: ""),
                        "address2" to (originalDestinationDetails?.address2 ?: ""),
                        "latitude" to (originalDestinationDetails?.lat ?: ""),
                        "longitude" to (originalDestinationDetails?.long ?: ""),
                        "amenityId" to (originalDestinationDetails?.amenityId ?: ""),
                        "amenityType" to (originalDestinationDetails?.amenityType ?: ""),
                        "name" to name,
                        "fullAddress" to (originalDestinationDetails?.fullAddress ?: ""),
                        "type" to ShareDestinationType.ROUTE_PLANNING,
                        "placeId" to originalDestinationDetails?.placeId,
                        "userLocationLinkIdRef" to originalDestinationDetails?.userLocationLinkIdRef,
                    )
                )
            )
        }?.subscribeOn(Schedulers.io())?.observeOn(AndroidSchedulers.mainThread())?.subscribe()
    }

    private fun handleFindEVChangerNearBy() {
        viewBinding.evToggle.onAmenityStateChange =
            object : BreezeAmenityRadioButton.OnAmenityStateChange {
                override fun onSelectedState() {
                    Analytics.logToggleEvent(Event.EV_ON, getScreenName())
                    viewBinding.amenitySelectionLabel.text =
                        getString(R.string.route_amenity_ev_header)
                    amenityMarkerManager?.showAllMarker(EVCHARGER)
                    viewBinding.amenities.setOtherButtonsToUnselectedState(viewBinding.evToggle)
                    initAmenityAdapterLayoutManager(EVCHARGER)
                    val evData = viewModel.amenityDataMap[EVCHARGER]?.distinctAmenities?.values
                    evData?.let { ev ->
                        amenityItemAdapter.dataList = ArrayList(ev)
                        amenityItemAdapter.notifyDataSetChanged()
                        if (ev.isNotEmpty()) {
                            showAmenityBottomPanel()
                        }
                    }
                }

                override fun onUnSelectedState(isResetState: Boolean) {
                    if (!isResetState)
                        Analytics.logToggleEvent(Event.EV_OFF, getScreenName())
                    showMainBottomPanel()
                    amenityMarkerManager?.removeMarkerTooltipView()
                    amenityMarkerManager?.hideNonWaypointMarkers(EVCHARGER)
                }
            }
    }

    /**
     * handle action when user want to find Petrol changer nearby route
     */
    private fun handleFindPetrolNearBy() {
        viewBinding.petrolToggle.onAmenityStateChange =
            object : BreezeAmenityRadioButton.OnAmenityStateChange {
                override fun onSelectedState() {
                    Analytics.logToggleEvent(Event.PETROL_ON, getScreenName())
                    viewBinding.amenitySelectionLabel.text =
                        getString(R.string.route_amenity_petrol_header)
                    amenityMarkerManager?.showAllMarker(PETROL)
                    viewBinding.amenities.setOtherButtonsToUnselectedState(viewBinding.petrolToggle)
                    initAmenityAdapterLayoutManager(PETROL)
                    val petrolData = viewModel.amenityDataMap[PETROL]?.distinctAmenities?.values
                    petrolData?.let { petrol ->
                        amenityItemAdapter.dataList = ArrayList(petrol)
                        amenityItemAdapter.notifyDataSetChanged()
                        if (petrol.isNotEmpty()) {
                            showAmenityBottomPanel()
                        }
                    }
                }

                override fun onUnSelectedState(isResetState: Boolean) {
                    if (!isResetState)
                        Analytics.logToggleEvent(Event.PETROL_OFF, getScreenName())
                    showMainBottomPanel()
                    amenityMarkerManager?.removeMarkerTooltipView()
                    amenityMarkerManager?.hideNonWaypointMarkers(PETROL)
                }
            }
    }


    // BREEZE2-1845
//    /**
//     * handle notify Arrival (ETA)
//     */
//    private fun handleNotifyArrival() {
//        Analytics.logClickEvent(Event.NOTIFY_ARRIVAL_CLICK, Screen.ROUTE_PLANNING)
//        if (etaEnabled) {
//            openEditETAScreen(Constants.TRIPETA.ETA_SCREEN)
//        } else {
//            Analytics.logClickEvent(Event.SHARE_MY_ETA, getScreenName())
//            showShareLiveLocation()
//        }
//    }

    private fun createTripPlan() {
        Analytics.logClickEvent(Event.SAVE_LATER, Screen.ROUTE_PLANNING)
        val selectedRoute = currentRouteObjects[CURRENT_SELECTED_ROUTE]
        val timeDiff = getTripDepartureTime(selectedRoute)
        if (timeDiff < 0) {
            showTripPreSaveWarningToast()
            return
        }
        val trip = createTripPlannerRequest() ?: return

        disableNavAmenityButtons()
        viewModel.planSaved.observe(viewLifecycleOwner, Observer {
            viewModel.planSaved.removeObservers(viewLifecycleOwner)
            enableNavAmenityButtons()
            if (it) {
                /**
                 * check trip dot red when add done
                 */
                notyUpcomingTripEvent(trip.tripEstStartTime)
                refreshTripLogData()
                showTripSaveSuccessDialog()
            } else {
                showSaveRouteFailed()
            }
        })
        viewModel.createTripPlan(trip)

    }


    /**
     * update trip planed
     */
    private fun updateTrip() {
        val tripPlainID = plannedTripDetails?.tripPlannerId ?: return
        Analytics.logClickEvent(Event.SAVE_LATER, getScreenName())
        sendLocationAnalyticsOrigin(Event.SAVE_LATER_ORIGIN)
        sendLocationAnalyticsDestination(Event.SAVE_LATER_DESTINATION)
        sendLocationAnalyticsWayPointsAdded(Event.SAVE_LATER_ADDED_STOP)

        val selectedRoute = currentRouteObjects[CURRENT_SELECTED_ROUTE]
        val timeDiff = getTripDepartureTime(selectedRoute)
        if (timeDiff < 0) {
            showTripPreSaveWarningToast()
            return
        }

        viewModel.planSaved.observe(viewLifecycleOwner) {
            viewModel.planSaved.removeObservers(this)
            if (it) {
                refreshTripLogData()
                showTripSaveSuccessToast()
                viewModel.plannedTripChanged = false
                viewBinding.routePlannedSave.isEnabled = false
                /**
                 * send event upcomming trip
                 */
                (activity?.application as? App)?.let { app ->
                    val shareBusinessLogicPackage = app.shareBusinessLogicPackage
                    compositeDisposable.add(
                        shareBusinessLogicPackage.getRequestsModule().flatMap {
                            Arguments.fromBundle(
                                bundleOf("hasUpcomingTrips" to true)
                            ).let { args ->
                                it.callRN(
                                    ShareBusinessLogicEvent.FN_UPDATE_UPCOMING_TRIPS.eventName,
                                    args
                                )
                            }
                        }.subscribeOn(Schedulers.io())
                            .subscribe()
                    )
                }
            } else {
                showSaveRouteFailed()
            }
        }
        val trip = createTripPlannerRequest()
        trip?.let {
            viewModel.updateTripPlan(tripPlainID, it)
        }
    }

    private fun validateCurLocAndConfirmNavigation() {
        Analytics.logClickEvent(Event.LETS_GO_CLICK, getScreenName())
        sendLocationAnalyticsOrigin(Event.LETS_GO_ORIGIN)
        sendLocationAnalyticsDestination(Event.LETS_GO_DESTINATION)
        sendLocationAnalyticsWayPointsAdded(Event.LETS_GO_ADDED_STOP)
        onConfirmClick()
//        val currentLocation=LocationBreezeManager.getInstance().currentLocation
//        if (currentLocation == null) {
//            Analytics.logClickEvent(Event.LETS_GO_CLICK, getScreenName())
//            sendLocationAnalyticsOrigin(Event.LETS_GO_ORIGIN)
//            sendLocationAnalyticsDestination(Event.LETS_GO_DESTINATION)
//            sendLocationAnalyticsWayPointsAdded(Event.LETS_GO_ADDED_STOP)
//            onConfirmClick()
//        } else {
//            val distanceFromCurrentLocation = TurfMeasurement.distance(
//                Point.fromLngLat(currentLocation.longitude, currentLocation.latitude),
//                Point.fromLngLat(
//                    sourceAddressDetails?.long?.toDouble() ?: currentLocation.longitude,
//                    sourceAddressDetails?.lat?.toDouble() ?: currentLocation.latitude
//                ),
//                TurfConstants.UNIT_METERS
//            )
//            if (distanceFromCurrentLocation > 300) {
//                showLocationWarningConfirmationDialog(currentLocation)
//            } else {
//                Analytics.logClickEvent(Event.LETS_GO_CLICK, getScreenName())
//                sendLocationAnalyticsOrigin(Event.LETS_GO_ORIGIN)
//                sendLocationAnalyticsDestination(Event.LETS_GO_DESTINATION)
//                sendLocationAnalyticsWayPointsAdded(Event.LETS_GO_ADDED_STOP)
//                onConfirmClick()
//            }
//        }
    }

    private fun refreshTripLogData() {
        val fragment = parentFragmentManager.findFragmentByTag(Constants.TAGS.TRAVEL_LOG_TAG)
        if (fragment != null) {
            parentFragmentManager.setFragmentResult(
                Constants.TRAVEL_LOG_FRAGMENT_REQUEST_KEY,
                Bundle.EMPTY
            )
        }
    }

    // BREEZE2-1845
//    /**
//     * show dialog confirm cancel share driver
//     */
//    private fun showConfirmCancelShareDriverDialog() {
//        context?.let {
//            val builder = AlertDialog.Builder(it)
//            val nameUserShare = etaRequest?.recipientName ?: ""
//            builder.setMessage(
//                getString(
//                    R.string.message_confirm_cancel_share_driver,
//                    nameUserShare
//                )
//            )
//            builder.setPositiveButton(
//                "Yes"
//            ) { dialog, _ ->
//                Analytics.logClickEvent(
//                    Event.ROUTE_PLANNING_CLICK_CANCEL_SHARE_DRIVE_CONFIRM,
//                    getScreenName()
//                )
//                dialog.cancel()
//                disableETA(isDisableETA = false)
//            }
//            builder.setNegativeButton(
//                "No"
//            ) { dialog, which -> dialog.cancel() }
//            val dialog = builder.create()
//            dialog.setOnShowListener(OnShowListener {
//                val color = ContextCompat.getColor(requireContext(), R.color.themed_passion_pink)
//                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(color)
//            })
//            dialog.show()
//        }
//    }

    private fun sendLocationAnalyticsOrigin(eventValue: String) {
        val currentLocationStr =
            (if (sourceAddressDetails?.destinationName.isNullOrEmpty()) sourceAddressDetails?.address1 else sourceAddressDetails?.destinationName)
                ?: getString(R.string.default_location)
        Analytics.logLocationDataEvent(
            eventValue,
            getScreenName(),
            currentLocationStr.toString(),
            sourceAddressDetails!!.lat!!.toString(),
            sourceAddressDetails!!.long!!.toString()
        )
    }

    private fun sendLocationAnalyticsDestination(eventValue: String) {
        val destinationStr =
            if (currentDestinationDetails?.destinationName.isNullOrEmpty()) currentDestinationDetails?.address1 else currentDestinationDetails?.destinationName
        Analytics.logLocationDataEvent(
            eventValue,
            getScreenName(),
            destinationStr.toString(),
            currentDestinationDetails!!.lat!!.toString(),
            currentDestinationDetails!!.long!!.toString()
        )
    }

    private fun sendLocationAnalyticsWayPointsAdded(eventValue: String) {
        waypointManager.wayPoints.keys.forEach {
            val wpLongitude = waypointManager.wayPoints[it]?.point?.coordinates()?.get(0)
            val wpLatitude = waypointManager.wayPoints[it]?.point?.coordinates()?.get(1)
            Analytics.logLocationDataEvent(
                eventValue,
                getScreenName(),
                "",
                wpLatitude.toString(),
                wpLongitude.toString()
            )
        }
    }

    private fun showLocationWarningConfirmationDialog(currentLocation: Location) {
        DialogFactory.createChoiceDialog(
            requireContext(),
            null,
            if (routeplanningMode == RoutePlanningMode.PLANNED_RESUME) getString(R.string.route_location_planned_warning) else getString(
                R.string.route_location_normal_warning
            ),
            getString(R.string.route_dialog_update_route),
            getString(R.string.route_dialog_cancel),
            {
                Analytics.logOriginAlertPopupEvent(
                    Event.POPUP_ORIGIN_ALERT,
                    Event.YES,
                    getScreenName()
                )
                updateCurrentLocationAndReroute(currentLocation)
            },
            {
                Analytics.logOriginAlertPopupEvent(
                    Event.POPUP_ORIGIN_ALERT,
                    Event.CANCEL,
                    getScreenName()
                )
            },
            false,
            null
        ).show()
    }

    private fun notyUpcomingTripEvent(trips: Long?) {
        val isExistTriplogToday = DateUtils.isToday((trips ?: 0) * 1000)
        App.instance?.run {
            compositeDisposable.add(
                shareBusinessLogicPackage.getRequestsModule().flatMap {
                    Arguments.fromBundle(
                        bundleOf("hasUpcomingTrips" to isExistTriplogToday)
                    ).let { args ->
                        it.callRN(ShareBusinessLogicEvent.FN_UPDATE_UPCOMING_TRIPS.eventName, args)
                    }
                }
                    .subscribeOn(Schedulers.io())
                    .subscribe()
            )
        }
    }


    private fun updateAmenityPreference(
        elementName: String,
        subItemName: String,
        isSelected: Boolean
    ) {
        BreezeMapDataHolder.updateAmenityPreference(elementName, subItemName, isSelected)
            ?.let { subItemMatched ->
                val elementId =
                    BreezeMapDataHolder.amenitiesPreferenceHashmap[elementName]!!.elementId
                val subItemId = subItemMatched.elementId
                val isParentSelected =
                    BreezeMapDataHolder.amenitiesPreferenceHashmap[elementName]!!.isSelected
                        ?: false
                App.instance?.run {
                    compositeDisposable.add(
                        shareBusinessLogicPackage.getRequestsModule().flatMap {
                            val subItemData = Arguments.createMap().apply {
                                putString(
                                    Constants.RN_CONSTANTS.USER_PREFERENCE_ELEMENT_ID,
                                    subItemId
                                )
                                putBoolean(
                                    Constants.RN_CONSTANTS.USER_PREFERENCE_SELECTED,
                                    isSelected
                                )
                            }

                            val data = Arguments.createMap().apply {
                                putString(
                                    Constants.RN_CONSTANTS.USER_PREFERENCE_ELEMENT_ID,
                                    elementId
                                )
                                putBoolean(
                                    Constants.RN_CONSTANTS.USER_PREFERENCE_SELECTED,
                                    isParentSelected
                                )
                                putMap(Constants.RN_CONSTANTS.USER_PREFERENCE_SUB_ITEM, subItemData)
                            }
                            it.callRN(ShareBusinessLogicEvent.UPDATE_AMENITIES_PREF.eventName, data)
                        }
                            .subscribeOn(Schedulers.io())
                            .subscribe()
                    )
                }
                isAmenitySelectionUpdated = true
            }
    }

    private fun forceRefreshAmenityPreference() {
        App.instance?.run {
            compositeDisposable.add(
                shareBusinessLogicPackage.getRequestsModule().flatMap {
                    it.callRN(ShareBusinessLogicEvent.TRIGGER_INIT_USER_PREF.eventName, null)
                }
                    .subscribeOn(Schedulers.io())
                    .subscribe()
            )
        }
    }


    private fun showSaveRouteFailed() {
        viewBinding.routeSavedTick.setImageResource(R.drawable.exclaimation_warning)
        viewBinding.routeSavedText.setText(R.string.route_saved_failed)
        viewBinding.routeSaved.visibility = View.VISIBLE
        handler = Handler(Looper.getMainLooper())
        handler.postDelayed({
            viewBinding.routeSaved.visibility = View.GONE
        }, 5000)
    }


    /**
     * create trip planner request
     */
    private fun createTripPlannerRequest(): TripPlannerRequest? {
        val routeData = currentRouteObjects[CURRENT_SELECTED_ROUTE]
        val geometry = LineString.fromPolyline(
            routeData.route.geometry()!!,
            Constants.POLYLINE_PRECISION
        )
        val trip = TripPlannerRequest()
        trip.tripEstStartTime =
            getPlannedDepartureTime(routeData) / 1000
        trip.tripEstArrivalTime =
            (if (isDepartureDate) (plannedSelectedDate!!.time + routeData.duration * 60 * 1000) else plannedSelectedDate!!.time) / 1000
        trip.tripStartAddress1 =
            sourceAddressDetails!!.address1 ?: getString(R.string.default_location)
        trip.tripStartAddress2 = sourceAddressDetails!!.address2
        trip.tripStartLat = sourceAddressDetails!!.lat!!.toDouble()
        trip.tripStartLong = sourceAddressDetails!!.long!!.toDouble()
        trip.tripDestAddress1 = currentDestinationDetails!!.address1
        trip.tripDestAddress2 = currentDestinationDetails!!.address2
        trip.tripDestLat = currentDestinationDetails!!.lat!!.toDouble()
        trip.tripDestLong = currentDestinationDetails!!.long!!.toDouble()
        trip.totalDuration = routeData.route.duration()
        trip.totalDistance = routeData.route.distance()
        trip.erpExpense = routeData.erpRate?.toDouble()
        trip.tripPlanBy = if (isDepartureDate) DEPART_AT else ARRIVE_BY
        trip.erpEntries = routeData.erpList?.map { it.erpId }
        trip.routeStops = waypointManager!!.getBreezeWaypointCoordinates()
        trip.routeCoordinates = geometry!!.coordinates().map { it.coordinates().reversed() }
        trip.isDestinationCarpark =
            if (currentDestinationDetails!!.carParkID == null) false else currentDestinationDetails!!.isDestinationCarPark()
        trip.carParkId = currentDestinationDetails!!.carParkID
        // BREEZE2-1845
//        if (etaEnabled && etaRequest != null) {
//            trip.tripEtaFavouriteId = etaRequest!!.tripEtaFavouriteId
//        }
        return trip
    }

    private fun getPlannedDepartureTime(routeData: SimpleRouteAdapterObjects) =
        (if (isDepartureDate) plannedSelectedDate!!.time else (plannedSelectedDate!!.time - routeData.duration * 60 * 1000))

    private val amenityItemListener = object : AmenityItemAdapter.AmenityItemListener {
        override fun onItemClick(type: String, data: AmenityItemAdapter.AmenityCount) {
            if (type == EVCHARGER) {
                viewModel.recalculateEvChargerAmenities()
                amenityMarkerManager?.removeMarkerTooltipView()
                viewModel.amenityDataMap[EVCHARGER]?.amenities?.let {
                    handleAmenityTypeMapLayerCreation(EVCHARGER, it, false)
                }
                updateAmenityPreference(data.type, data.itemType, data.selected)
                context?.getUserDataPreference()
                    ?.saveEVAmenityFilter(BreezeMapDataHolder.getSelectedEVPlugTypes().toTypedArray())
            } else if (type == PETROL) {
                viewModel.recalculatePetrolAmenities()
                amenityMarkerManager?.removeMarkerTooltipView()
                viewModel.amenityDataMap[PETROL]?.amenities?.let {
                    handleAmenityTypeMapLayerCreation(PETROL, it, false)
                }
                updateAmenityPreference(data.type, data.itemType, data.selected)
                context?.getUserDataPreference()
                    ?.savePetrolAmenityFilter(BreezeMapDataHolder.getSelectedPetrolProviders().toTypedArray())
            }
        }
    }

    fun initAdapters() {

        initAmenityAdapter()
    }


    private fun initAmenityAdapter() {
        amenityItemAdapter = AmenityItemAdapter(
            requireContext(),
            listOf(),
            amenityItemListener
        )
        viewBinding.amenitiesSelector.adapter = amenityItemAdapter
    }

    private fun initAmenityAdapterLayoutManager(type: String) {

        val layoutManager = FlexboxLayoutManager(this@RoutePlanningFragment.activity)
        layoutManager.flexWrap = FlexWrap.WRAP
        layoutManager.flexDirection = FlexDirection.COLUMN

        if (type == EVCHARGER) {
            layoutManager.justifyContent = JustifyContent.FLEX_END
            layoutManager.alignItems = AlignItems.FLEX_END
            viewBinding.amenitiesSelector.layoutParams.height =
                (140 * requireContext().resources.displayMetrics.density).toInt()
            viewBinding.amenitiesSelector.overScrollMode = View.OVER_SCROLL_NEVER
            viewBinding.amenitiesSelector.layoutManager = layoutManager
            viewBinding.amenitiesSelector.setPadding(
                Utils.adjustDPToPixels(10, pixelDensity),
                Utils.adjustDPToPixels(10, pixelDensity),
                Utils.adjustDPToPixels(10, pixelDensity),
                Utils.adjustDPToPixels(10, pixelDensity)
            )
        } else {
            layoutManager.justifyContent = JustifyContent.FLEX_START
            layoutManager.alignItems = AlignItems.FLEX_START
            viewBinding.amenitiesSelector.layoutParams.height =
                (140 * requireContext().resources.displayMetrics.density).toInt()
            viewBinding.amenitiesSelector.overScrollMode = View.OVER_SCROLL_NEVER
            viewBinding.amenitiesSelector.layoutManager = layoutManager
            viewBinding.amenitiesSelector.setPadding(
                Utils.adjustDPToPixels(20, pixelDensity),
                Utils.adjustDPToPixels(10, pixelDensity),
                0,
                0
            )
        }
    }

    fun setUserSettings(routePref: RoutePreference) {
        breezeUserPreferenceUtil.saveUserRoutePreference(routePref)
        viewModel.saveUserSettings(
            breezeUserPreferenceUtil.getUserThemePreference(),
            routePref.type
        )
    }


    private fun showDialog2OriginAndDestinationSame() {
        val builder: AlertDialog.Builder? = requireContext().let {
            AlertDialog.Builder(it)
        }
        builder?.let {
            val message: String = getString(R.string.same_location_both_origin_destination)
            it.setTitle("")
                .setMessage(message)
                .setPositiveButton(
                    R.string.dialog_okay
                ) { dialog, _ ->
                    dialog.dismiss()
                }
            val alertDialog: AlertDialog = it.create()
            alertDialog.setOnShowListener {
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).isAllCaps = false
                alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).isAllCaps = false
            }
            alertDialog.setCanceledOnTouchOutside(false)
            alertDialog.show()
        }
    }

    // BREEZE2-1845
//    override fun disableETA(isDisableETA: Boolean) {
//        etaRequest = null
//        etaEnabled = false
//        enableOrDisableNotyArrive(isDisableETA = true)
//    }
//
//    private fun etaRNCAllback(data: HashMap<String, Any>) {
//        if (data.containsKey(Constants.TRIPETA.CONTACT_NAME) && data.containsKey(Constants.TRIPETA.CONTACT_NUMBER)
//        ) {
//            createETARequest(data)
//        } else {
//            // Reset ETA
//            Analytics.logClickEvent(Event.RESET_MY_ETA, getScreenName())
//
//            etaEnabled = false
//            etaRequest = null
//
//            viewBinding.etaButtonText.text = getString(R.string.share_drive)
//        }
//
//    }
//
//    private fun createETARequest(data: HashMap<String, Any>) {
//        (data[Constants.TRIPETA.TRIP_ETA_FAVOURITE_ID] as? Double)?.toInt()?.let {
//            createETARequest(
//                data[Constants.TRIPETA.CONTACT_NAME].toString(),
//                data[Constants.TRIPETA.CONTACT_NUMBER].toString(),
//                it,
//                data[Constants.TRIPETA.ETA_MESSAGE].toString()
//            )
//        }
//    }
//
//    /**
//     * @param recipientName
//     * @param recipientNumber
//     * @param tripEtaFavouriteId
//     * @param message
//     */
//    private fun createETARequest(
//        recipientName: String, recipientNumber: String,
//        tripEtaFavouriteId: Int, message: String
//    ) {
//        etaEnabled = true
//        etaRequest = ETARequest()
//        etaRequest!!.type = Constants.TRIPETA.INIT
//        etaRequest!!.recipientName = recipientName
//        etaRequest!!.recipientNumber = recipientNumber
//        etaRequest!!.tripEtaFavouriteId =
//            tripEtaFavouriteId
//        etaRequest!!.message = message
//        etaRequest!!.shareLiveLocation = Constants.TRIPETA.YES
//        etaRequest!!.voiceCallToRecipient = Constants.TRIPETA.NO
//        enableOrDisableNotyArrive(isDisableETA = false)
//    }
//
//    private fun enableOrDisableNotyArrive(isDisableETA: Boolean) {
//        if (isDisableETA) {
//            viewBinding.etaImg.setImageResource(R.drawable.notify_arrival)
//            viewBinding.etaButtonText.text = getString(R.string.share_drive)
//            viewBinding.rlEtaButtons.setOnClickListener {
//                showShareLiveLocation()
//            }
//        } else {
//            viewBinding.etaImg.setImageResource(R.drawable.cancel_noty_arrival)
//            viewBinding.etaButtonText.text = getString(R.string.cancel_sharing)
//            viewBinding.rlEtaButtons.setOnClickListener {
//                Analytics.logClickEvent(
//                    Event.ROUTE_PLANNING_CLICK_CANCEL_SHARE_DRIVE,
//                    getScreenName()
//                )
//                showConfirmCancelShareDriverDialog()
//            }
//        }
//    }


    private fun showOriginAndDestinationDetails() {
        val destinationStr =
            if (currentDestinationDetails?.destinationName.isNullOrEmpty()) currentDestinationDetails?.address1 else currentDestinationDetails?.destinationName
        val currentLocationStr =
            (if (sourceAddressDetails?.destinationName.isNullOrEmpty()) sourceAddressDetails?.address1 else sourceAddressDetails?.destinationName)
                ?: getString(R.string.default_location)
        viewBinding.destinationLocationTextView.text =
            if (destinationStr != null) Utils.capitalize(destinationStr) else null
        viewBinding.currentLocationTextView.text = Utils.capitalize(currentLocationStr)

        /**
         * when swap location should check carpark broadcast & get carpark detail if need
         */
        checkCarParkBroadcast()
    }

    private fun swapOriginAndDestinationDetails() {

        val tempSource = sourceAddressDetails
        sourceAddressDetails = currentDestinationDetails
        currentDestinationDetails = tempSource
        originalDestinationDetails = tempSource
        showOriginAndDestinationDetails()
    }

    private fun initStyle() {
        mapboxMap.loadStyleUri(
            (requireActivity() as DashboardActivity).getMapboxStyle(basicMapRequired = true)
        ) { style ->

            if (isFragmentDestroyed()) {
                return@loadStyleUri
            }
            onInitialized {
                initLayoutManager()
                initMarkerViews()
                val currentLocation = LocationBreezeManager.getInstance().currentLocation

                viewBinding.mapViewRoutePlan.camera.apply {
                    val zoom = createZoomAnimator(
                        CameraAnimatorOptions.cameraAnimatorOptions(
                            12.0
                        )
                    ) {
                        currentLocation?.let {
                            viewBinding.mapViewRoutePlan.camera.flyTo(
                                CameraOptions.Builder()
                                    .center(
                                        Point.fromLngLat(
                                            it.longitude,
                                            it.latitude
                                        )
                                    )
                                    .bearing(0.0)
                                    .zoom(12.0)
                                    .pitch(0.0)
                                    .build()
                            )
                        }
                    }
                    playAnimatorsSequentially(zoom)

                    viewBinding.mapViewRoutePlan.gestures.addOnFlingListener {
                        Timber.d("On fling is called")
                        Analytics.logMapInteractionEvents(Event.DRAG_MAP, getScreenName())
                    }

                    viewBinding.mapViewRoutePlan.gestures.addOnScaleListener(object :
                        OnScaleListener {
                        override fun onScale(detector: StandardScaleGestureDetector) {}

                        override fun onScaleBegin(detector: StandardScaleGestureDetector) {}

                        override fun onScaleEnd(detector: StandardScaleGestureDetector) {
                            Timber.d("On scale end is called")
                            Analytics.logMapInteractionEvents(Event.PINCH_MAP, getScreenName())
                        }
                    })

                }

                val STYLE_IMAGES: HashMap<String, Int> = hashMapOf(
                    SELECTED_ERP_RATE_IMG to R.drawable.route_info_blue_down,
                    UNSELECTED_ERP_RATE_IMG to R.drawable.route_info_white_down,
                    ROUTE_ERP_IMG to R.drawable.erp_small,
                    Constants.UNSAVED_DESTINATION_ICON to R.drawable.destination_puck,
                    Constants.FAVOURITES_DESTINATION_ICON to R.drawable.destination_puck,
                )
                for ((key, value) in STYLE_IMAGES) {
                    value.toBitmap(requireContext())?.let {
                        style.addImage(key, it, false)
                    }
                }
                if (currentRouteObjects.size == 0 || (CURRENT_SELECTED_ROUTE >= currentRouteObjects.size)) {
                    initiateRouteBasedOnMode()
                } else {
                    initiatePreSelectedRouteBasedOnMode()
                }
                routeLabelHelper.initRouteLabelStyle(style)
                erpRefreshListener()
                viewLoadComplete = true
            }
        }
    }

    private fun initiatePreSelectedRouteBasedOnMode() {
        //called to disable/reset ui to mimick ui state similar to initiateroute function
        showRouteLoadingUI()
        viewBinding.progressBar.visibility = View.GONE
        val destinationPoint = Point.fromLngLat(
            currentDestinationDetails?.long!!.toDouble(),
            currentDestinationDetails?.lat!!.toDouble()
        )
        val originPoint = Point.fromLngLat(
            sourceAddressDetails!!.long!!.toDouble(),
            sourceAddressDetails!!.lat!!.toDouble()
        )
        //val routeLines: MutableList<NavigationRoute> = ArrayList()
        val routeObjects = currentRouteObjects.mapIndexed { index, it ->
            RouteAdapterTypeObjects(
                currentRouteTypes[index],
                it
            )
        }
        val routeLines = ArrayList<NavigationRoute>()
        routeObjects.forEach { route ->
            createNavigationRoute(route)?.let {
                routeLines.add(it)
            }
        }
        fetchAmenities(originPoint, destinationPoint)
        drawRouteOnMap(routeLines, CURRENT_SELECTED_ROUTE)
    }


    /**
     * NOTE:
     * destination icon manager map clicks has more precedence and should be initialized
     * before other amenities managers
     */
    private fun initLayoutManager() {

        carParkDestinationIconManager = CarParkDestinationIconManager(
            requireActivity(), viewBinding.mapViewRoutePlan
        )
        if (SHOW_WALKING_PATH_IN_ROUTEPLANNING) {
            walkingDestinationIconManager = DestinationIconManager(
                requireActivity(),
                destinationSourceId = Constants.WALKING_DESTINATION_SOURCE,
                destinationLayerId = Constants.WALKING_DESTINATION_LAYER,
                destinationImageId = Constants.WALKING_DESTINATION_ICON_ID,
                viewBinding.mapViewRoutePlan
            )
        }

        amenityMarkerManager = WaypointAmenitiesMarkerLayerManager(
            viewBinding.mapViewRoutePlan,
            waypointManager,
            nightModeEnabled
        ).also {
            amenityMarkerHelper =
                WaypointAmenityMarkerHelper(it, nightModeEnabled, true)
            it.markerLayerClickHandler = markerLayerClickHandler
        }
        lifecycleScope.launch(Dispatchers.IO) {
            amenityMarkerManager?.addStyleImages(CARPARK)
        }
    }

    private fun erpRefreshListener() {
        Timber.d("Routeplanning ERP refresh listener is initialized")
        erpRefreshDisposable = RxBus.listen(RxEvent.ERPRefresh::class.java)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                Timber.d("Routeplanning ERP refresh listener is called")
                if (
                    routeplanningMode != RoutePlanningMode.PLANNED &&
                    routeplanningMode != RoutePlanningMode.PLANNED_RESUME
                ) {
                    lastCarParkToggleState = viewBinding.carParkViewOption.currentState
                    routeInitiate(
                        clearWaypoints = false,
                        refreshVouchers = false,
                        refreshAmenities = false
                    )
                }
            }
        compositeDisposable.add(erpRefreshDisposable!!)
    }

    private fun updateCardDetails() {

        if (context == null || activity == null) {
            return
        }


        if (routeplanningMode != RoutePlanningMode.NORMAL) {
            viewBinding.departTime.text =
                DateFormat.format("hh:mmaa, dd LLL, EEE", plannedSelectedDate)
            if (isDepartureDate) {
                viewBinding.departText.text = getString(R.string.depart_at)
            } else {
                viewBinding.departText.text = getString(R.string.arrive_by)

            }
            viewBinding.routeDepartDetails.visibility = View.VISIBLE
        }
    }


    private fun onConfirmClick() {
        if (originalDestinationDetails == null || currentDestinationDetails == null)
            return
        // BREEZE2-1845
//        if (etaEnabled && etaRequest != null) {
//            etaRequest!!.tripStartTime = Timestamp.now().seconds
//            etaRequest!!.tripEndTime =
//                etaRequest!!.tripStartTime?.plus(routeData.duration().toLong())
//            etaRequest!!.destination = currentDestinationDetails!!.address1
//            etaRequest!!.message = resources.getString(
//                R.string.eta_message_template_to_send_messsage,
//                breezeUserPreferenceUtil.retrieveUserName(),
//                currentDestinationDetails?.address1 ?: "",
//                RouteDisplayUtils.formatArrivalTimeWithTime12H(currentRouteObjects[CURRENT_SELECTED_ROUTE].arrivalTime)
//            )
//
//            etaRequest!!.tripDestLat = currentDestinationDetails!!.lat?.toDouble()
//            etaRequest!!.tripDestLong = currentDestinationDetails!!.long?.toDouble()
//            etaRequest!!.arrivalTime =
//                RouteDisplayUtils.formatArrivalTimeWithTime12H(currentRouteObjects[CURRENT_SELECTED_ROUTE].arrivalTime)
//            viewModel.etaAPISuccessful.observe(viewLifecycleOwner) {
//                triggerNavigationStartEvent(
//                    originalDestinationDetails!!,
//                    currentDestinationDetails!!,
//                    routeData,
//                    currentRouteObjects[CURRENT_SELECTED_ROUTE].originalResponse,
//                    breezeERPUtil.getERPData(),
//                    it
//                )
//            }
//            viewModel.sendETAMessage(etaRequest!!)
//        } else {
        triggerNavigationStartEvent(
            originalDestinationDetails!!,
            currentDestinationDetails!!,
            currentRouteObjects[CURRENT_SELECTED_ROUTE].route,
            currentRouteObjects[CURRENT_SELECTED_ROUTE].originalResponse,
            breezeERPUtil.getERPData(),
            null
        )
//        }
    }

    private fun triggerNavigationStartEvent(
        originalDestination: DestinationAddressDetails,
        destinationAddressDetails: DestinationAddressDetails,
        route: DirectionsRoute,
        originalResponse: DirectionsResponse,
        erpData: ERPResponseData.ERPResponse?,
        etaRequest: ETARequest?
    ) {
        disableNavigateButton()
        lifecycleScope.launch(Dispatchers.IO) {
            try {
                // when invoked from erp route preview details.. data wont be cached
                val compressedRoute = viewModel.compressedOriginalRouteData[originalResponse]
                    ?: originalResponse.compressToGzip()
                withContext(Dispatchers.Main) {
                    ensureActive()
                    val phoneData = PhoneNavigationStartEventData(
                        originalDestination,
                        destinationAddressDetails,
                        route,
                        compressedRoute,
                        erpData,
                        etaRequest,
                        walkingRoute,
                        selectedCarParkIconRes,
                        viewBinding.tvAlertBannerRoutePlanView.notificationCriterias
                    )
                    postEvent(AppToPhoneNavigationStartEvent(phoneData))

                    val carData = CarNavigationStartEventData(
                        originalDestination,
                        destinationAddressDetails,
                        route,
                        compressedRoute,
                        originalResponse,
                        erpData,
                        etaRequest,
                        walkingRoute,
                        selectedCarParkIconRes
                    )
                    postEventWithCacheLast(AppToNavigationStartEvent(carData))
                    erpRefreshDisposable?.dispose()
                    routeLineAPI?.cancel()
                    Handler(Looper.getMainLooper()).post {
                        activity?.supportFragmentManager?.popBackStack()
                    }
                }
            } catch (e: CancellationException) {
                Timber.e(e, "Start navigation was cancelled")
            } catch (e: Exception) {
                Timber.e(e, "Failed to start navigation")
            }
        }
    }

    private fun routeInitiate(
        clearWaypoints: Boolean = true,
        refreshVouchers: Boolean = clearWaypoints,
        refreshAmenities: Boolean = true,
        enablePlannedTripSave: Boolean = true,
        showWalkingRoute: Boolean = false
    ) {
        if (
            !currentDestinationDetails.isValid() ||
            !sourceAddressDetails.isValid()
        ) {
            showUnsupportedAddressDialog()
            return
        }
        val destLocation = currentDestinationDetails?.getRoutableLocation()
            ?: Location("").apply {
                latitude = 0.0
                longitude = 0.0
            }
        val finalDestPoint = originalDestinationDetails?.getRoutablePointLatLong()

        val originLocation =
            sourceAddressDetails!!.getRoutableLocation() ?: Location("").apply {
                latitude = 0.0
                longitude = 0.0
            }

        lifecycleScope.launch(Dispatchers.IO) {
            try {
                val isAddressSupported = Utils.areAddressesSupported(
                    context = requireContext(),
                    origin = originLocation,
                    destination = destLocation
                )

                withContext(Dispatchers.Main) {
                    ensureActive()
                    if (isAddressSupported) {
                        val destinationPoint = currentDestinationDetails!!.getRoutablePointLatLong()
                        val originPoint = sourceAddressDetails!!.getRoutablePointLatLong()
                        if (clearWaypoints) {
                            waypointManager.clear()
                        }
                        if (refreshAmenities) {
                            fetchAmenities(originPoint, destinationPoint)
                        }
                        if (showWalkingRoute) {
                            viewModel.walkingRoutesRetrieved.observe(
                                viewLifecycleOwner,
                                Observer { route ->
                                    viewModel.walkingRoutesRetrieved.removeObservers(
                                        viewLifecycleOwner
                                    )
                                    processWalkingRoutes(route)
                                })
                            viewModel.findWalkingRoutes(
                                destinationPoint,
                                finalDestPoint,
                                originalDestinationDetails!!
                            )
                        }
                        if (enablePlannedTripSave && routeplanningMode == RoutePlanningMode.PLANNED_RESUME) {
                            viewModel.plannedTripChanged = true
                        }
                        findRoute(originPoint, destinationPoint, refreshVouchers)
                    } else {
                        showUnsupportedAddressDialog()
                    }

                }
            } catch (e: CancellationException) {
                Timber.e(e, "routeInitiate Job was cancelled")
            }
        }
    }

    private fun DestinationAddressDetails?.isValid(): Boolean {
        return !(this == null || this.lat == null || this.long == null)
    }

    private fun showUnsupportedAddressDialog() {
        //Sometimes, route planning screen is not popping out when user comes back from Navigation
        //it may result crash here.
        if (context == null || !isAdded) {
            return
        }
        DialogFactory.createSingleChoiceDialog(
            context = requireContext(),
            title = getString(R.string.dialog_title_country_not_supported),
            message = getString(R.string.dialog_msg_country_not_supported),
            btnTextPositive = getString(R.string.dialog_okay),
            actionPositive = null,
            canDismiss = false,
            dissmissListener = {
                onBackPressed()
            }
        ).show()
    }

    private fun showNoRouteFoundDialog() {
        //Sometimes, route planning screen is not popping out when user comes back from Navigation
        //it may result crash here.
        if (context == null || !isAdded) {
            return
        }
        Analytics.logOpenPopupEvent("no_drivable_route", "[routeplanning]")
        Analytics.logLocationDataEvent(
            "no_drivable_route_origin",
            "[routeplanning]",
            originalDestinationDetails?.destinationName ?: "",
            originalDestinationDetails?.lat ?: "",
            originalDestinationDetails?.long ?: ""
        )
        Analytics.logLocationDataEvent(
            "no_drivable_route_destination",
            "[routeplanning]",
            currentDestinationDetails?.destinationName ?: "",
            currentDestinationDetails?.lat ?: "",
            currentDestinationDetails?.long ?: ""
        )
        DialogFactory.createSingleChoiceDialog(
            context = requireContext(),
            title = null,
            message = getString(R.string.dialog_msg_no_route_found),
            btnTextPositive = getString(R.string.dialog_okay),
            actionPositive = null,
            canDismiss = false,
            dissmissListener = {
                Analytics.logClickEvent("[popup_no_drivable_route]:got_it", "[routeplanning]")
                onBackPressed()
            }
        ).show()
    }

    private fun findRoute(originPoint: Point, destination: Point, refreshVouchers: Boolean) {
        showRouteLoadingUI()
        viewModel.routesRetrieved.observe(viewLifecycleOwner, Observer { routesMap ->
            viewModel.routesRetrieved.removeObservers(viewLifecycleOwner)
            viewBinding.progressBar.visibility = View.GONE
            if (routesMap.isEmpty()) {
                routeplanningMode = RoutePlanningMode.NORMAL
                showMainBottomPanel()
                clearRoutes()
                showNoRouteFoundDialog()
//if (currentRouteObjects.size > 0) {
//                    enableNavAmenityButtons()
//                    enableCarPark()
//                }
            } else {
                //performing a shallow clone
                val allRoutes: ArrayList<RouteAdapterTypeObjects> =
                    getRouteTypeDataList(HashMap(routesMap))
                processRoutes(allRoutes)
                // running after the route line is selected and drawn
                lifecycleScope.launch(Dispatchers.Main) {
                    detectCurrentLocationInProfileZone()
                    detectCarParkCongestionAndVouchers(destination, refreshVouchers)
                }
                checkBroadcastTravelZone()
            }

        })
        findRouteForMode(originPoint, destination)
    }

    private fun checkBroadcastTravelZone() {
        var longestDuration: Long = 0 // in Milliseconds from duration(minutes)
        currentRouteObjects.forEach {
            if (it.duration.toLong().times(60000) > longestDuration) {
                longestDuration = it.duration.toLong().times(60000)
            }
        }
        val departTime: Long
        val arrivalTime: Long
        if (routeplanningMode != RoutePlanningMode.NORMAL) {
            if (isDepartureDate) {
                departTime = plannedSelectedDate?.time ?: System.currentTimeMillis()
                arrivalTime = departTime + longestDuration
            } else {
                arrivalTime = plannedSelectedDate?.time ?: System.currentTimeMillis()
                departTime = arrivalTime - longestDuration
            }
        } else {
            departTime = System.currentTimeMillis()
            arrivalTime = departTime + longestDuration
        }
        viewModel.getBroadcastFilter(
            currentRouteObjects,
            departTime,
            arrivalTime,
            originalDestinationDetails?.lat?.toDoubleOrNull() ?: 0.0,
            originalDestinationDetails?.long?.toDoubleOrNull() ?: 0.0,
            object : ApiObserver<BroadcastTravelZoneFilterResponse>(compositeDisposable) {
                override fun onSuccess(data: BroadcastTravelZoneFilterResponse) {
                    if (data.success) {
                        handleBroadcastFilterData(data.broadcasts)
                    }
                }

                override fun onError(e: ErrorData) {
                    Timber.e("getBroadcastFilter - ${e.message}")
                }

            })
    }


    private fun handleBroadcastFilterData(broadcasts: List<BroadcastData>) {
        if (broadcasts.isNotEmpty()) {
            val broadcastData: BroadcastData? =
                if (shouldShowNCSBroadcast) broadcasts[0] else broadcasts.find { it.type != CARPARK }

            // type = carpark if destination is ncs location and broadcastData.action must not null if type = carpark
            if (broadcastData?.type == CARPARK) {
                val theme = when (broadcastData.alertLevel?.lowercase()) {
                    "info" -> {
                        Analytics.logPopupEvent(
                            Event.LIMITED_PARKING_SPACE,
                            Event.VALUE_POPUP_OPEN,
                            getScreenName()
                        )
                        AlertRoutePlanView.ThemeRoutePlanningNCSCarparkLimited
                    }

                    else -> {
                        Analytics.logPopupEvent(
                            Event.CARPARK_FULL,
                            Event.VALUE_POPUP_OPEN,
                            getScreenName()
                        )
                        AlertRoutePlanView.ThemeRoutePlanningNCSCarparkFull
                    }
                }

                showCarParkBroadcastMessage(
                    title = broadcastData.title,
                    message = broadcastData.message ?: "",
                    notificationCriterias = null,
                    theme = theme,
                    otherData = broadcastData
                )
            } else {
                broadcastData?.let { data ->
                    showCarParkBroadcastMessage(
                        message = data.message ?: "",
                        notificationCriterias = null,
                        theme = AlertRoutePlanView.ThemeNationalDayBroadcast
                    )
                }
            }
        } else {
            hideCarparkBroadCastMessage()
        }

        fetchCarPark()

    }

    private fun showRouteLoadingUI() {
        viewBinding.progressBar.visibility = View.VISIBLE
        disableNavigationButtons()
        showMainBottomPanel()
        resetAmenitiesAndCarParkButtons()
    }

    private fun enableCarPark() {
        if (viewBinding.carParkViewOption.isCarParksShown()) {
            amenityMarkerManager?.showAllMarker(CARPARK)
        }
    }

    /**
     * converts the route map to a ordered selectable list
     */
    private fun getRouteTypeDataList(routesMap: HashMap<RoutePreference, List<SimpleRouteAdapterObjects>>): ArrayList<RouteAdapterTypeObjects> {
        val allRoutes: ArrayList<RouteAdapterTypeObjects> = ArrayList()
        val alternates = routesMap.remove(RoutePreference.ALTERNATE_ROUTE)
        RouteSelectorUtil.handleRoutePreference(
            routesMap,
            allRoutes,
            RoutePreference.FASTEST_ROUTE.type
        )
        routesMap.forEach { map ->
            map.value.forEach {
                allRoutes.add(RouteAdapterTypeObjects(map.key, it))
            }
        }
        alternates?.let { alternates ->
            alternates.forEach {
                allRoutes.add(RouteAdapterTypeObjects(RoutePreference.ALTERNATE_ROUTE, it))
            }
        }
        preselectRouteForPlannedMode(allRoutes)
        return allRoutes
    }


    //todo need to check by anirudh
    private fun preselectRouteForPlannedMode(allRoutes: ArrayList<RouteAdapterTypeObjects>) {
        if (routeplanningMode == RoutePlanningMode.PLANNED_RESUME) {
            plannedTripDetails?.routeCoordinates?.let { routeCoordinates ->
                val coordinatesLngLat = routeCoordinates.map { coordinate -> coordinate.reversed() }
                var routeSelectedIndex = -1
                var selectedRoute: RouteAdapterTypeObjects? = null
                for ((index, route) in allRoutes.withIndex()) {
                    if (route.routeData.route.geometry() != null) {
                        val ls = LineString.fromPolyline(
                            route.routeData.route.geometry()!!,
                            Constants.POLYLINE_PRECISION
                        )
                        val lsCoordinates = ls.coordinates().map { it.coordinates() }
                        if (lsCoordinates == coordinatesLngLat) {
                            routeSelectedIndex = index
                            selectedRoute = route
                            break
                        }
                    }
                }
                if (routeSelectedIndex > -1) {
                    allRoutes.removeAt(routeSelectedIndex)
                    allRoutes.add(0, selectedRoute!!)
                }
            }
        }
    }

    private fun findRouteForMode(
        originPoint: Point,
        destination: Point
    ) {
        when (routeplanningMode) {
            RoutePlanningMode.PLANNED -> {
                val departureDetails: RouteDepartureDetails
                val date = plannedSelectedDate!!
                departureDetails = if (isDepartureDate) {
                    RouteDepartureDetails(date.time, DepartureType.DEPARTURE)
                } else {
                    RouteDepartureDetails(date.time, DepartureType.ARRIVAL)
                }
                viewModel.findRoutes(
                    originPoint,
                    destination,
                    waypointManager.getAllWayPointsList(),
                    currentDestinationDetails!!,
                    departureDetails
                )
            }

            RoutePlanningMode.PLANNED_RESUME -> {
                if (plannedTripDetails != null && plannedSelectedDate != null) {
                    val departureDetails = RouteDepartureDetails(
                        plannedSelectedDate!!.time,
                        if (isDepartureDate) DepartureType.DEPARTURE else DepartureType.ARRIVAL
                    )
                    viewModel.findRoutes(
                        originPoint,
                        destination,
                        waypointManager.getAllWayPointsList(),
                        currentDestinationDetails!!,
                        departureDetails
                    )
                } else {
                    viewModel.findRoutes(
                        originPoint,
                        destination,
                        waypointManager.getAllWayPointsList(),
                        currentDestinationDetails!!,
                        null
                    )
                }
            }

            else -> {
                viewModel.findRoutes(
                    originPoint,
                    destination,
                    waypointManager.getAllWayPointsList(),
                    currentDestinationDetails!!,
                    null
                )
            }
        }
    }

    private fun disableNavigationButtons() {
        if (context == null) {
            return
        }
        viewBinding.routePlanningGo.isEnabled = false
        viewBinding.routePlanningLater.isEnabled = false
        viewBinding.routePlannedSave.isEnabled = false
        viewBinding.routePlannedGo.isEnabled = false
        viewBinding.routePlanningSave.isEnabled = false
        viewBinding.rlDirectionsButton.isEnabled = false
        // BREEZE2-1845
//        viewBinding.rlEtaButtons.isEnabled = false
    }

    private fun enableNavigationClick() {
        if (context == null) {
            return
        }
        viewBinding.routePlanningGo.isEnabled = true
        viewBinding.routePlanningLater.isEnabled = true
        if (viewModel.plannedTripChanged) {
            viewBinding.routePlannedSave.isEnabled = true
        }
        viewBinding.routePlannedGo.isEnabled = true
        viewBinding.routePlanningSave.isEnabled = true
        viewBinding.rlDirectionsButton.isEnabled = true
        // BREEZE2-1845
//        viewBinding.rlEtaButtons.isEnabled = true
    }

    private fun showMainBottomPanel() {
        viewBinding.amenitySelectionView.visibility = View.GONE
        viewBinding.pickerSelectionView.visibility = View.GONE
        viewBinding.bottomView.visibility = View.VISIBLE

        when (routeplanningMode) {
            RoutePlanningMode.NORMAL -> {
                viewBinding.routeBottomPlanningButtons.visibility = View.VISIBLE
                viewBinding.routeBottomPlanningLaterButtons.visibility = View.GONE
                viewBinding.routeBottomPlanningSavedButtons.visibility = View.GONE
                viewBinding.routeDepartDetails.visibility = View.GONE
            }

            RoutePlanningMode.PLANNED -> {
                viewBinding.routeBottomPlanningButtons.visibility = View.GONE
                viewBinding.routeBottomPlanningLaterButtons.visibility = View.VISIBLE
                viewBinding.routeBottomPlanningSavedButtons.visibility = View.GONE

            }

            RoutePlanningMode.PLANNED_RESUME -> {
                viewBinding.routeBottomPlanningButtons.visibility = View.GONE
                viewBinding.routeBottomPlanningLaterButtons.visibility = View.GONE
                viewBinding.routeBottomPlanningSavedButtons.visibility = View.VISIBLE
            }
        }

    }

    private fun showAmenityBottomPanel() {
        viewBinding.amenitySelectionView.visibility = View.VISIBLE
        viewBinding.pickerSelectionView.visibility = View.GONE
        viewBinding.bottomView.visibility = View.GONE
    }

    private fun showTimePickerBottomPanel() {
        viewBinding.amenitySelectionView.visibility = View.GONE
        viewBinding.pickerSelectionView.visibility = View.VISIBLE
        viewBinding.bottomView.visibility = View.GONE
        updateSelectedTime()
    }

    private fun updateSelectedTime() {
        if (routeplanningMode == RoutePlanningMode.NORMAL) {
            viewBinding.routeDayPicker.setDefaultDate(Date())
        } else {
            if (isDepartureDate) {
                viewBinding.pickerDepart.callOnClick()
            } else {
                viewBinding.pickerArrive.callOnClick()
            }
            viewBinding.routeDayPicker.setDefaultDate(plannedSelectedDate)
        }
    }

    private fun resetAmenitiesAndCarParkButtons() {
        viewBinding.amenities.setAllButtonsToUnselectedState()
        resetCarParks()
        disableAmenities()
        viewBinding.layoutAlertCongestion.visibility = View.GONE
        viewBinding.voucherLayout.visibility = View.GONE
    }

    private fun disableAmenities() {
        viewBinding.rlAmenitySelection.visibility = View.GONE
    }


    private fun enableAmenities() {
        viewBinding.rlAmenitySelection.visibility = View.VISIBLE
    }


    /**
     * reset car park
     * + show destination
     * + hide carpark
     */
    private fun resetCarParks() {
        if (viewBinding.carParkViewOption.isCarParksShown()) {
            viewBinding.carParkViewOption.setState(CarParkViewOption.ALL_NEARBY)
            amenityMarkerManager?.hideNonWaypointMarkers(CARPARK)
            carParkDestinationIconManager.showDestinationIcon()
        }
    }

    override fun clearRoutes() {
        currentRouteObjects.clear()
        currentRouteTypes.clear()
        navigationCamera.requestNavigationCameraToIdle()
        viewportDataSource.clearRouteData()
    }

    override fun processRoutes(routeObjects: List<RouteAdapterTypeObjects>?) {
        if (routeObjects?.isNotEmpty() == true && routeLineView != null) {
            currentRouteObjects.clear()
            currentRouteTypes.clear()
            val routeLines: MutableList<NavigationRoute> = ArrayList()
            for (routeObj in routeObjects) {
                val navigationRoute = createNavigationRoute(routeObj)
                navigationRoute?.let {
                    routeLines.add(navigationRoute)
                    currentRouteObjects.add(routeObj.routeData)
                    currentRouteTypes.add(routeObj.type)
                }
            }
            drawRouteOnMap(routeLines, 0)
        } else {
            navigationCamera.requestNavigationCameraToIdle()
            viewportDataSource.clearRouteData()
        }
    }

    fun processWalkingRoutes(route: NavigationRoute?) {
        val mContext = context ?: return
        walkingRoute = route
        if (SHOW_WALKING_PATH_IN_ROUTEPLANNING) {
            val lineString = LineString.fromPolyline(
                route!!.directionsRoute.geometry()!!,
                Constants.POLYLINE_PRECISION
            )
            lineString.coordinates().last()?.let {
                walkingDestinationIconManager.addDestinationIcon(it.latitude(), it.longitude())
            }

            WalkingRouteLineLayer(mapboxMap.getStyle()!!, mContext).generateRouteLineLayer(
                lineString
            )
        }
    }

    private fun drawRouteOnMap(
        routeLines: List<NavigationRoute>,
        routeIndex: Int
    ) {
        cardAdapterInitialize()
        lifecycleScope.launch(Dispatchers.Main) {
            val geometry = LineString.fromPolyline(
                currentRouteObjects[routeIndex]!!.route.geometry()!!,
                Constants.POLYLINE_PRECISION
            )
            if (geometry.coordinates().size > 0) {
                updateCarParkDestinationIcon(geometry, selectedCarParkDestinationIconRes)
            }
            val suspendCancelExpected = routeLineAPI?.setRoutesSuspend(routeLines)
            suspendCancelExpected?.let { data ->
                ifNonNull(routeLineView, mapboxMap.getStyle()) { view, style ->
                    try {
                        ensureActive()
                        view.renderRouteDrawData(style, data)
                        removeRouteLineDestinationMarker()
                        // Selecting the first route in the list
                        setRouteLineProperties(routeIndex)
                        enableNavAmenityButtons()
                        enableAndToggleCarPark()
                        if (routeLines.size > CURRENT_SELECTED_ROUTE) {
                            updatePrimaryRoute(routeLines[CURRENT_SELECTED_ROUTE])
                        }
                    } catch (e: CancellationException) {
                    }
                }
            }
        }
        resetCameraToRoutes(
            routeLines,
            mapboxMap,
            viewBinding.bottomView,
            viewBinding.routeLlHeading,
            viewBinding.carParkViewOption,
            null
        )

        /**
         * show circle layer TBR
         */
        if (isDestinationInZoneTBR) {
            mapCongestionManager.showProfileZoneLayer(false)
        }

        /**
         * show broadcast message if have message
         */

        if (!mCurrentMessageBroadcastMessageShouldShow.isNullOrEmpty()) {
            showCarParkBroadcastMessage(
                message = mCurrentMessageBroadcastMessageShouldShow,
                notificationCriterias = mCurrentNotificationCriterias
            )
        }
    }

    private fun updateCarParkDestinationIcon(geometry: LineString, carParkDestinationIcon: Int) {
        var markerIcon = R.drawable.destination_puck
        if (carParkDestinationIcon != -1 && currentDestinationDetails!!.isCarPark) {
            markerIcon = carParkDestinationIcon
        }
        carParkDestinationIconManager.addDestinationIcon(
            lat = geometry.coordinates().last()!!.latitude(),
            long = geometry.coordinates().last()!!.longitude(),
            res = markerIcon
        )
    }

    private fun updateCarParkDestinationForVoucher(selectedAmenity: SelectedAmenity) {
        // car park icon is already the destination icon
        if (selectedCarParkDestinationIconRes != -1) {
            return
        }
        val currentSelectedRoute = currentRouteObjects[CURRENT_SELECTED_ROUTE]
        if (currentSelectedRoute != null) {
            val geometry =
                LineString.fromPolyline(
                    currentSelectedRoute.route.geometry()!!,
                    Constants.POLYLINE_PRECISION
                )
            val voucherIcon = ParkingIconUtils.getCarParkIconForNavigation(
                selectedAmenity.baseAmenity,
                isDestination = true
            )
            updateCarParkDestinationIcon(geometry, voucherIcon)
        }
    }


    suspend fun MapboxRouteLineApi.setRoutesSuspend(newRoutes: List<NavigationRoute>) =
        suspendCancellableCoroutine<Expected<RouteLineError, RouteSetValue>> { cout ->
            setNavigationRoutes(newRoutes) {
                cout.resume(it) {
                }
            }
        }

    private fun enableNavAmenityButtons() {
        enableNavigationClick()
        enableAmenities()
    }

    private fun disableNavAmenityButtons() {
        disableNavigationButtons()
        disableAmenities()
    }


    /**
     * Called after fetching route to enable or disable car park state
     * (for example it will be enabled if route was refreshed by erp)
     */
    private fun enableAndToggleCarPark() {
        lastCarParkToggleState?.let {
            if (it != CarParkViewOption.HIDE) {
                viewBinding.carParkViewOption.setState(it)
                showCarPark()
            }
        }
        lastCarParkToggleState = null
    }

    override fun displayERPIcons() {
        clearMarkerViews()

        val timeToCheckERP: Long
        var isTimeAlreadyChanged = false
        if (routeplanningMode == RoutePlanningMode.PLANNED) {
            timeToCheckERP = plannedSelectedDate!!.time
            isTimeAlreadyChanged = true
        } else if (routeplanningMode == RoutePlanningMode.PLANNED_RESUME) {
            if (plannedTripDetails != null && plannedTripDetails!!.tripEstStartTime != null) {
                timeToCheckERP = plannedTripDetails!!.tripEstStartTime!! * 1000
                isTimeAlreadyChanged = true
            } else {
                timeToCheckERP = System.currentTimeMillis()
            }
        } else {
            timeToCheckERP = System.currentTimeMillis()
        }

        // After calculating ERP rates in all routes, create FeatureCollection to display on screen
        for ((index, data) in currentRouteObjects.withIndex()) {
            if (index == CURRENT_SELECTED_ROUTE) {
                continue
            }
            renderERPAndRouteLabel(data, index, timeToCheckERP, isTimeAlreadyChanged)
        }
        // currentRouteObjects cannot be empty
        // Handling current routeline last, so all added views would be on top
        val a = arrayListOf<Int>()
        if (CURRENT_SELECTED_ROUTE in currentRouteObjects.indices) {
            renderERPAndRouteLabel(
                currentRouteObjects[CURRENT_SELECTED_ROUTE],
                CURRENT_SELECTED_ROUTE,
                timeToCheckERP,
                isTimeAlreadyChanged
            )
        }
    }

    private fun renderERPAndRouteLabel(
        data: SimpleRouteAdapterObjects,
        index: Int,
        timeToCheckERP: Long,
        pIsTimeAlreadyChanged: Boolean = false
    ) {
        //if (data.erpRate != null && data.erpRate!! > 0.0) {
        val geometry = LineString.fromPolyline(
            data.route.geometry()!!,
            Constants.POLYLINE_PRECISION
        )
        var feature = Feature.fromGeometry(
            geometry, JsonObject()
        )

        // Calculating feature that is at center using TurfMeasurement
        feature = TurfMeasurement.center(feature)
        val nearestPoint = TurfMisc.nearestPointOnLine(
            Point.fromLngLat(
                (feature.geometry() as Point).longitude(),
                (feature.geometry() as Point).latitude()
            ), geometry.coordinates()
        )
        feature = nearestPoint
        feature.addStringProperty(
            PROP_ERP_DISPLAY, String.format(
                "%dmin | $%.2f",
                data.duration,
                data.erpRate
            )
        )

        if (CURRENT_SELECTED_ROUTE == index) {
            if (data.erpList != null) {
                for (erpChargeInfo in data.erpList!!) {
                    mapERPManager.handleSearchERP(
                        erpChargeInfo.erpId,
                        expandAutomatically = false,
                        showInfoIcon = true,
                        timeToCheckERP = timeToCheckERP,
                        screenName = getScreenName(),
                        forceShowErp = false,
                        zoomValue = 15.0,
                        showMinimalView = false,
                        forceCheckNextDayERP = false,
                        isTimeAlreadyChanged = pIsTimeAlreadyChanged
                    )
                }
            }
        } else {
            if (data.erpList != null) {
                for (erpChargeInfo in data.erpList!!) {
                    mapERPManager.handleSearchERP(
                        erpChargeInfo.erpId,
                        expandAutomatically = false,
                        showInfoIcon = true,
                        timeToCheckERP = timeToCheckERP,
                        screenName = getScreenName(),
                        forceShowErp = false,
                        zoomValue = 15.0,
                        showMinimalView = true,
                        forceCheckNextDayERP = false,
                        isTimeAlreadyChanged = pIsTimeAlreadyChanged
                    )
                }
            }
        }

        //For force showing ERP for testing
        //            for(i in 1..10) {
        //                dashBoardMapLanding.handleSearchERP(
        //                    i,
        //                    expandAutomatically = false,
        //                    showInfoIcon = true,
        //                    timeToCheckERP = timeToCheckERP,
        //                    screenName = getScreenName(),
        //                    forceShowErp = true
        //                )
        //            }

        if ((currentRouteObjects.size > 1)) {
            if (currentRouteTypes[index] != RoutePreference.ALTERNATE_ROUTE) {
                routeLabelHelper.showRouteLabel(
                    geometry = geometry,
                    routePreference = currentRouteTypes[index],
                    isRootSelected = CURRENT_SELECTED_ROUTE == index
                )
            }
        }
    }

    override fun onStart() {
        super.onStart()
        viewBinding.mapViewRoutePlan.onStart()
    }

    override fun onStop() {
        viewBinding.mapViewRoutePlan.onStop()
        (activity as DashboardActivity?)?.setRefreshRouteDestinationListener(null)

        if (isAmenitySelectionUpdated) {
            isAmenitySelectionUpdated = false
            BreezeMapDataHolder.shouldReLoadAmenities = true
            forceRefreshAmenityPreference()
        }

        super.onStop()
    }

    override fun onResume() {
        super.onResume()

        (activity as DashboardActivity?)?.setRefreshRouteDestinationListener(this)
        (activity as DashboardActivity?)?.setRoutePlanningListener(this)

        (activity as DashboardActivity?)?.setDashboardActivityStatusBarBasedOnTheme()

    }

    override fun onDestroyView() {
        super.onDestroyView()
        lifecycleScope.cancel()
        OBUStripStateManager.getInstance()?.enable()
    }

    override fun onDestroy() {
        if (::handler.isInitialized) {
            handler.removeCallbacksAndMessages(null)
        }

        if (!compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }

        (activity as DashboardActivity?)?.setRefreshRouteDestinationListener(null)
        (activity as DashboardActivity?)?.setRoutePlanningListener(null)

        super.onDestroy()
    }

    override fun showCarparkNavigation(carparkDetails: BaseAmenity) {
        //Toast.makeText(requireContext(), "Navigate clicked", Toast.LENGTH_SHORT).show()
        showingCarparkDetails = true
        updateRoutes(carparkDetails = carparkDetails)
    }

    // FIXME: should be removed
    @Deprecated("Legacy Function")
    private fun updateRoutes(carparkDetails: BaseAmenity? = null) {
        if (showingCarparkDetails && carparkDetails != null) {
            lifecycleScope.launch(Dispatchers.IO) {
                //Updating view of carpark button

                //Loop through EasyBreezies list and check if existing item

                //else create a dummy destination details
                var carparkLocation = Location("")
                carparkLocation.latitude = carparkDetails.lat!!
                carparkLocation.longitude = carparkDetails.long!!
                var address = Utils.getAddressObject(carparkLocation, requireContext())
                if (address != null) {
                    currentDestinationDetails = DestinationAddressDetails(
                        null,
                        carparkDetails.name,
                        address.getAddressLine(0),
                        carparkDetails.lat.toString(),
                        carparkDetails.long.toString(),
                        null,
                        carparkDetails.name,
                        null,
                        -1,
                        true
                    )
                } else {
                    currentDestinationDetails = DestinationAddressDetails(
                        null,
                        carparkDetails.name,
                        null,
                        carparkDetails.lat.toString(),
                        carparkDetails.long.toString(),
                        null,
                        carparkDetails.name,
                        null,
                        -1,
                        true
                    )
                }
                withContext(Dispatchers.Main) {
                    try {
                        ensureActive()
                        triggerSelectAddressAPI()
                        refreshRoute()
                    } catch (e: CancellationException) {
                    }
                }
            }
        } else {
            //Updating view of carpark button
            currentDestinationDetails = originalDestinationDetails
            refreshRoute()
        }
    }


    private fun updateCurrentLocationAndReroute(currentLocation: Location) {
        lifecycleScope.launch(Dispatchers.IO) {
            sourceAddressDetails = DestinationAddressDetails(
                null,
                Utils.getAddressFromLocation(currentLocation, requireContext()),
                null,
                currentLocation.latitude.toString(),
                currentLocation.longitude.toString(),
                null,
                Constants.TAGS.CURRENT_LOCATION_TAG,
                null,
                -1
            )
            withContext(Dispatchers.Main) {
                try {
                    ensureActive()
                    showOriginAndDestinationDetails()
                    routeInitiate(false, refreshAmenities = true)
                } catch (e: CancellationException) {
                }
            }
        }
    }

    private fun refreshRoute() {
        selectedCarParkIconRes = -1
        selectedCarParkDestinationIconRes = -1
        showOriginAndDestinationDetails()
        initRouteLine()
        routeInitiate()
    }

    private fun handleMapFeatureClick(point: Point) {
        mapboxMap.getStyle()?.let {
            val primaryLineVisibility = routeLineView?.getPrimaryRouteVisibility(it)
            val alternativeRouteLinesVisibility = routeLineView?.getAlternativeRoutesVisibility(it)
            if (primaryLineVisibility == Visibility.VISIBLE && alternativeRouteLinesVisibility == Visibility.VISIBLE) {
                routeLineAPI?.findClosestRoute(
                    point,
                    mapboxMap,
                    routeClickPadding,
                    closestRouteResultConsumer
                )
            }
        }
    }

    private val closestRouteResultConsumer: MapboxNavigationConsumer<Expected<RouteNotFound, ClosestRouteValue>> =
        MapboxNavigationConsumer { closestRouteResult ->
            if (closestRouteResult.isValue) {
                val closestRouteValue =
                    closestRouteResult.value as ClosestRouteValue
                val routeFound = closestRouteValue.navigationRoute

                Analytics.logClickEvent(Event.TAP_ROUTE_CLICK, getScreenName())
                if (routeFound != routeLineAPI!!.getPrimaryNavigationRoute()) {
                    updatePrimaryRoute(routeFound)
                    for ((index, routeObject) in currentRouteObjects.withIndex()) {
                        if (routeObject.route == routeFound.directionsRoute) {
                            setRouteLineProperties(index)
                            break
                        }
                    }
                }
            }
        }


    fun setRouteLineProperties(index: Int) {

        if (context == null) {
            return
        }
        CURRENT_SELECTED_ROUTE = index
        viewBinding.rvRouteCards.smoothScrollToPosition(index)
        updateCardDetails()
        displayERPIcons()
    }

    fun updatePrimaryRoute(route: NavigationRoute) {
        routeLineAPI?.apply {
            if (route != getPrimaryNavigationRoute()) {
                val reOrderedRoutes = getNavigationRoutes()
                    .filter { it != route }
                    .toMutableList()
                    .also {
                        it.add(0, route)
                    }
                setNavigationRoutes(reOrderedRoutes) { value ->
                    mapboxMap.getStyle()?.let {
                        routeLineView?.renderRouteDrawData(it, value)
                    }
                    removeRouteLineDestinationMarker()
                }
            }
        }
    }

    //TODO: this is a workaround to remove the default grey marker : https://github.com/mapbox-collab/ncs-collab/issues/365
    private fun removeRouteLineDestinationMarker() {
        mapboxMap.getStyle()?.removeStyleImage("destinationMarker")
    }


    override fun updateRouteDestination(easyBreezyList: List<EasyBreezieAddress>) {
        val temCurrentLocation = currentDestinationDetails ?: return

        for ((index, item) in easyBreezyList.withIndex()) {
            if (temCurrentLocation.lat == item.lat && temCurrentLocation.long == item.long) {
                val destinationAddressDetails = DestinationAddressDetails(
                    item.addressid,
                    item.address1,
                    item.address2,
                    item.lat,
                    item.long,
                    null,
                    item.name,
                    DestinationAddressTypes.EASY_BREEZIES,
                    (index + 1)
                )
                currentDestinationDetails = destinationAddressDetails
                if (!showingCarparkDetails) {
                    originalDestinationDetails = currentDestinationDetails
                }
                showOriginAndDestinationDetails()
                initRouteLine()
                routeInitiate()
                break
            }
        }
    }


    private fun initiateRouteBasedOnMode() {

        when (routeplanningMode) {
            RoutePlanningMode.PLANNED_RESUME -> {
                // BREEZE2-1845
//                extractPlanedTripEtaDetails()
                routeInitiate(false, enablePlannedTripSave = false)
            }

            else -> {
                routeInitiate(refreshVouchers = (!currentDestinationDetails!!.isDestinationCarPark()))
            }
        }
    }

    /**
     * detect if destination in TBR zone
     */
    private fun detectCarParkCongestionAndVouchers(destination: Point, refreshVouchers: Boolean) {

        val locationDestination = Location("")
        val selectedTime =
            if (routeplanningMode != RoutePlanningMode.NORMAL && plannedSelectedDate != null) {
                plannedSelectedDate!!.time
            } else {
                System.currentTimeMillis()
            }
        val destinationName =
            if (originalDestinationDetails?.destinationName.isNullOrEmpty()) originalDestinationDetails?.address1 else originalDestinationDetails?.destinationName

        isDestinationInZoneTBR = false
        locationDestination.latitude = destination.latitude()
        locationDestination.longitude = destination.longitude()


        viewModel.carParkProfileDetails.observe(viewLifecycleOwner, voucherDetectedEvent)

        if (BreezeMapDataHolder.isNotExistProfileAmenities()) {
            viewModel.validateCarParkCongestionAndVouchers(
                null, null, destination, selectedTime, refreshVouchers, destinationName,
                viewBinding.carParkViewOption.currentState.mode
            )
        } else {
            lifecycleScope.launch(Dispatchers.IO) {

                val zonePair = ProfileZoneLayer.detectAllProfileLocationInZone(locationDestination)
                if (zonePair.type != ProfileZoneLayer.NO_ZONE) {
                    isDestinationInZoneTBR = true
                    withContext(Dispatchers.Main) {
                        viewModel.validateCarParkCongestionAndVouchers(
                            profile = zonePair.selectedProfile,
                            typeZone = zonePair,
                            destination = destination,
                            timestamp = selectedTime,
                            refreshVouchers = refreshVouchers,
                            dest = destinationName,
                            viewBinding.carParkViewOption.currentState.mode
                        )
                    }
                } else {
                    viewModel.validateCarParkCongestionAndVouchers(
                        null, null, destination, selectedTime, refreshVouchers, destinationName,
                        viewBinding.carParkViewOption.currentState.mode
                    )
                    activity?.runOnUiThread {
                        mapCongestionManager.hideProfileZoneLayer()
                    }
                }
            }
        }
    }


    private fun isZoneCongestion(
        typeZone: String,
        listCongestion: ArrayList<ResponseCongestionDetail>
    ): Boolean {
        var isZoneCongestion = false
        listCongestion.forEach {
            if (it.name.equals(typeZone)) {
                if (it.availablePercent != null) {
                    isZoneCongestion =
                        it.availablePercent!! < ProfileZoneLayer.LIMIT_CHECK_CONGESTION
                }
            }
        }
        return isZoneCongestion
    }


    private fun getCurrentUserName(): String {
        var userName = breezeUserPreferenceUtil.retrieveUserName()
        if (TextUtils.isEmpty(userName)) {
            userName = getString(R.string.menu_private_car)
        }
        return userName
    }

    private fun showAlert(message: String) {
        val builder: AlertDialog.Builder = requireContext().let {
            AlertDialog.Builder(it)
        }

        builder.setMessage(message)
            .setPositiveButton(
                R.string.alert_close
            ) { dialog, _ ->
                dialog.dismiss()
            }

        val alertDialog: AlertDialog = builder.create()

        alertDialog.setOnShowListener {
            alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).isAllCaps = false
            alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).isAllCaps = false
        }

        alertDialog.show()
    }

    private fun processContactsResult(data: Intent?) {
        Timber.d("Contacts result launcher reached")
        val contactDetails =
            Utils.extractPhoneNumberFromContactIntent(data!!, requireContext())

        if (contactDetails != null) {
            Analytics.logClickEvent(Event.CONTACT_SELECT, Screen.SEARCH_CONTACT)
            // BREEZE2-1845
//            openETAScreen(Constants.TRIPETA.ETA_SCREEN, contactDetails)

        } else {
            showAlert(getString(R.string.incorrect_phone_number))
        }
    }

    override fun getScreenName(): String {
        if (routeplanningMode == RoutePlanningMode.PLANNED_RESUME) {
            return Screen.ROUTE_PLANNING_EDIT
        }
        return Screen.ROUTE_PLANNING_MODE
    }

    private val markerLayerClickHandler = object : MarkerLayerManager.MarkerLayerClickHandler {
        override fun handleOnclick(
            layerType: String,
            feature: Feature,
            currentMarker: MarkerLayerManager.MarkerViewType?
        ) {
            resetAllViews()
            if (currentMarker != null && currentMarker.feature.id() == feature.id()) {
                return
            }
            when (layerType) {
                CARPARK -> {
                    viewModel.amenityDataMap[layerType]!!.amenities
                        .find { it.baseAmenity.id == feature.id() }
                        ?.let { amenity -> showCarParkToolTip(amenity) }
                }

                EVCHARGER -> {
                    viewModel.amenityDataMap[layerType]!!.amenities
                        .find { it.baseAmenity.id == feature.id() }
                        ?.let { amenity -> showEvChargerToolTip(amenity, layerType) }
                }

                PETROL -> {
                    viewModel.amenityDataMap[layerType]!!.amenities
                        .find { it.baseAmenity.id == feature.id() }
                        ?.let { amenity -> showPetrolToolTip(amenity, layerType) }
                }
            }
        }

        override fun handleOnFreeMapClick(point: Point) {
            // no op
        }
    }

    private fun showPetrolToolTip(
        amenity: SelectedAmenity,
        type: String
    ) {
        Analytics.logClickEvent(Event.PETROL_MAP_ICON, getScreenName())
        val amenitiesMarkerView = PetrolMarkerView(
            LatLng(amenity.baseAmenity.lat!!, amenity.baseAmenity.long!!),
            mapboxMap,
            requireActivity(),
            amenity.baseAmenity.id,
            waypointManager.wayPoints.count() >= Constants.MAX_WAY_POINT_LIMIT
        )
        var amenityFeature = addAmenityFeature(amenity.baseAmenity)
        amenityMarkerManager?.renderMarkerToolTipView(
            MarkerLayerManager.MarkerViewType(
                amenitiesMarkerView,
                type,
                amenityFeature
            )
        )
        val selected = waypointManager.getWaypoint(amenity.baseAmenity.id!!) != null
        amenitiesMarkerView.handleMarkerClick(
            selected,
            amenity.baseAmenity,
            addStopClick = { item, eventSource ->
                Analytics.logClickEvent(Event.ADD_STOP_PETROL, getScreenName())
                return@handleMarkerClick addWayPoint(item)
            },
            removeStopClick = { item, eventSource ->
                Analytics.logClickEvent(Event.REMOVE_STOP_PETROL, getScreenName())
                item.id?.let {
                    waypointManager.removeWayPoint(it)
                    routeInitiate(false, false)
                }
                return@handleMarkerClick true
            },
            markerClick = {
            }
        )
    }

    private fun showEvChargerToolTip(
        amenity: SelectedAmenity,
        type: String
    ) {
        val amenitiesMarkerView = EVMarkerView(
            LatLng(amenity.baseAmenity.lat!!, amenity.baseAmenity.long!!),
            mapboxMap,
            requireActivity(),
            amenity.baseAmenity.id,
            waypointManager.wayPoints.count() >= Constants.MAX_WAY_POINT_LIMIT
        )
        val amenityFeature = addAmenityFeature(amenity.baseAmenity)
        amenityMarkerManager?.renderMarkerToolTipView(
            MarkerLayerManager.MarkerViewType(
                amenitiesMarkerView,
                type,
                amenityFeature
            )
        )
        val selected = waypointManager.getWaypoint(amenity.baseAmenity.id!!) != null
        amenitiesMarkerView.handleMarkerClick(
            selected,
            amenity.baseAmenity,
            addStopClick = { item, _ ->
                Analytics.logClickEvent(Event.ADD_STOP_EV, getScreenName())
                return@handleMarkerClick addWayPoint(item)
            },
            removeStopClick = { item, eventSource ->
                Analytics.logClickEvent(Event.REMOVE_STOP_EV, getScreenName())
                item.id?.let {
                    waypointManager.removeWayPoint(it)
                    routeInitiate(false, false)
                }
                return@handleMarkerClick true
            },
            markerClick = {
            }
        )
    }

    private fun addWayPoint(item: BaseAmenity): Boolean {
        if (waypointManager.wayPoints.size < Constants.MAX_WAY_POINT_LIMIT) {
            item.id?.let { itemId ->
                waypointManager.addWayPoint(
                    itemId,
                    Point.fromLngLat(item.long!!, item.lat!!),
                    item
                )
                routeInitiate(false, false)
                return true
            }
        } else {
            showAlert("Maximum ${Constants.MAX_WAY_POINT_LIMIT} waypoints are allowed for now")
            return false
        }
        return false
    }

    private fun addAmenityFeature(amenity: BaseAmenity): Feature {
        val properties = JsonObject()
        amenityMarkerHelper?.handleSelectedAmenityProperties(properties, false, amenity)
        val amenityFeature = Feature.fromGeometry(
            Point.fromLngLat(
                amenity.long!!,
                amenity.lat!!
            ), properties, amenity.id
        )
        return amenityFeature
    }

    private fun openTripNotificationSettings() {
        val settingsIntent: Intent =
            Intent(android.provider.Settings.ACTION_APP_NOTIFICATION_SETTINGS)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                .putExtra(android.provider.Settings.EXTRA_APP_PACKAGE, BuildConfig.APPLICATION_ID)
        //.putExtra(android.provider.Settings.EXTRA_CHANNEL_ID, FCMListenerService.NOTIFICATION_CHANNEL_ID_UPCOMING_TRIPS)
        startActivity(settingsIntent)
    }

    private fun isTripUpdateNotificationOn(): Boolean {
        if (!NotificationManagerCompat.from(requireContext()).areNotificationsEnabled()) {
            return false
        }
        val manager =
            requireContext().getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val channel =
            manager.getNotificationChannel(FCMListenerService.NOTIFICATION_CHANNEL_ID_UPCOMING_TRIPS)
        return channel != null && channel.importance != NotificationManager.IMPORTANCE_NONE
    }

    private fun showTripSaveSuccessToast() {
        setupTripNotificationChannel()
        val notification = GlobalNotification()
            .setTitle(getString(R.string.msg_trip_updated_success))
            .setTheme(GlobalNotification.ThemeSuccess)
            .setDismissDurationSeconds(10)

        val (selectedRoute, timeDiff) = getTripDepatureTimeDetails()

        if (timeDiff <= Constants.THIRTY_MINUTES) {
            notification.setDescription(getString(R.string.msg_trip_custom, selectedRoute.duration))
            notification.setActionText(null)
            notification.setActionListener(null)
        } else {
            if (isTripUpdateNotificationOn()) {
                notification.setDescription(getString(R.string.msg_trip_notification_on))
                notification.setActionText(null)
                notification.setActionListener(null)
            } else {
                notification.setDescription(getString(R.string.msg_trip_notification_off))
                notification.setActionText(getString(R.string.action_enable_notification))
                notification.setActionListener {
                    openTripNotificationSettings()
                }
            }
        }
        notification.setAnalyticsData(
            getScreenName(),
            Event.POPUP_TRIP_UPDATED,
            Event.CLOSE_POPUP_TRIP_UPDATED
        )
        notification.show(activity)
    }

    private fun showTripPreSaveWarningToast() {
        val builder: AlertDialog.Builder = requireContext().let {
            AlertDialog.Builder(it)
        }
        val selectedRoute = currentRouteObjects[CURRENT_SELECTED_ROUTE]
        val message: String = if (isDepartureDate) {
            getString(R.string.desc_trip_presave_warning_1)
        } else {
            getString(
                R.string.desc_trip_presave_warning_2,
                RouteDisplayUtils.formatArrivalTimeWithTime12H(selectedRoute.arrivalTime)
            )
        }
        builder.setTitle(getString(R.string.msg_trip_presave_warning))
            .setMessage(message)
            .setPositiveButton(
                R.string.alert_close
            ) { dialog, _ ->
                Analytics.logClickEvent(Event.GOT_IT_UNABLE_TO_SAVE, getScreenName())
                Analytics.logClosePopupEvent(Event.POPUP_UNABLE_TO_SAVE, getScreenName())
                dialog.dismiss()
            }
        val alertDialog: AlertDialog = builder.create()
        alertDialog.setOnShowListener {
            alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).isAllCaps = false
            alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).isAllCaps = false
        }
        alertDialog.show()
        Analytics.logOpenPopupEvent(Event.POPUP_UNABLE_TO_SAVE, getScreenName())
    }

    private fun disableNavigateButton() {
        viewBinding.routePlanningGo.isClickable = false
        viewBinding.routePlannedGo.isClickable = false
    }


    private fun getTripDepatureTimeDetails(): Pair<SimpleRouteAdapterObjects, Long> {
        val selectedRoute = currentRouteObjects[CURRENT_SELECTED_ROUTE]
        val timeDiff = getTripDepartureTime(selectedRoute)
        return Pair(selectedRoute, timeDiff)
    }

    private fun getTripDepartureTime(selectedRoute: SimpleRouteAdapterObjects): Long {
        val departureTimeMS = getPlannedDepartureTime(selectedRoute)
        return departureTimeMS - System.currentTimeMillis()
    }

    private fun setupTripNotificationChannel() {
        if (context == null) {
            return
        }
        val notificationManager =
            requireContext().getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        PushNotificationUtils.setupNotificationChannel(
            notificationManager = notificationManager,
            channelId = FCMListenerService.NOTIFICATION_CHANNEL_ID_UPCOMING_TRIPS,
            channelName = FCMListenerService.NOTIFICATION_CHANNEL_NAME_UPCOMING_TRIPS,
            importanceLevel = NotificationManager.IMPORTANCE_HIGH,
        )
    }

    private fun showTripSaveSuccessDialog() {
        if (context == null || activity == null) {
            return
        }

        setupTripNotificationChannel()

        val (selectedRoute, timeDiff) = getTripDepatureTimeDetails()

        val dialog = activity?.let {
            Dialog(it)
        }!!

        dialog.setContentView(R.layout.dialog_trip_save_success)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCanceledOnTouchOutside(false)
        //dialog.setCancelable(true)

        val greetingMsg = dialog.findViewById<TextView>(R.id.greeting_msg)
        val actionButton = dialog.findViewById<BreezeButton>(R.id.button_back_to_home)

        if (timeDiff <= Constants.THIRTY_MINUTES) {
            greetingMsg.text =
                getString(R.string.desc_trip_save_success_custom, selectedRoute.duration)
            actionButton.updateText(getString(R.string.lets_go))
            actionButton.setOnClickListener {
                dialog.dismiss()
                Analytics.logClosePopupEvent(Event.TRIP_BEEN_SAVED, getScreenName())
                Analytics.logClickEvent(Event.LETS_GO_TRIP_SAVED, getScreenName())
                viewBinding.routePlanningGo.callOnClick()
            }
        } else {
            if (isTripUpdateNotificationOn()) {
                greetingMsg.text = getString(R.string.desc_trip_save_success)
            } else {
                greetingMsg.text = getString(R.string.msg_trip_notification_off).highlightWords(
                    world = "enable notification",
                    color = ContextCompat.getColor(requireContext(), R.color.themed_breeze_primary),
                    shouldBold = true
                )
                greetingMsg.setOnClickListener {
                    dialog.dismiss()
                    (requireActivity() as DashboardActivity).popAllFragmentsInBackStack()
                    openTripNotificationSettings()
                }
            }

            actionButton.updateText(getString(R.string.action_back_to_home))
            actionButton.setOnClickListener {
                dialog.dismiss()
                Analytics.logClosePopupEvent(Event.TRIP_BEEN_SAVED, getScreenName())
                Analytics.logClickEvent(Event.TRIP_BACK_TO_HOME, getScreenName())
                (requireActivity() as DashboardActivity).apply {
                    navigateToHomeTabAndReset()
                    popAllFragmentsInBackStack()
                }
            }
        }

        val btn_trip_log = dialog.findViewById<View>(R.id.btn_trip_log)
        btn_trip_log.setOnClickListener {
            dialog.dismiss()
            Analytics.logClosePopupEvent(Event.TRIP_BEEN_SAVED, getScreenName())
            Analytics.logClickEvent(Event.CHECK_IN_TRIP_LOG_TRIP_SAVED, getScreenName())
            (requireActivity() as DashboardActivity).addTripLogsFragmentAndPop()
        }

        dialog.show()

        val displayRectangle = Rect()
        val window: Window = requireActivity().window
        window.decorView.getWindowVisibleDisplayFrame(displayRectangle)

        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window!!.attributes)
        lp.width = (displayRectangle.width() * 0.9f).toInt()
        dialog.window!!.attributes = lp

        Analytics.logOpenPopupEvent(Event.TRIP_BEEN_SAVED, getScreenName())
    }


    companion object {
        private const val DEPART_AT = "DEPART_AT"
        private const val ARRIVE_BY = "ARRIVE_BY"
        private const val VIEW_ANIMATION_DURATION = 1000L
        private const val VIEW_ANIMATION_SWITCH_DELAY = 5000L
        private const val SHOW_WALKING_PATH_IN_ROUTEPLANNING = false
        private const val CALCULATE_WALKING_PATH_IN_ROUTEPLANNING = false
    }

    private open inner class CarParkObserver : Observer<AmenityData> {
        override fun onChanged(it: AmenityData?) {
            renderUpdatedData(it, false)
        }

        fun renderUpdatedData(it: AmenityData?, voucherFilterEnabled: Boolean) {
            it?.amenities?.let { allCarParkAmenities ->
                val carParkAmenities =
                    if (voucherFilterEnabled) {
                        ArrayList(allCarParkAmenities.filter { it.baseAmenity.hasVouchers })
                    } else {
                        allCarParkAmenities
                    }
                carParkDestinationIconManager.replaceSelectedDestinationCarParkIcon(
                    carParkAmenities,
                    R.drawable.destination_endpoint_ellipse,
                    true
                )
                handleAmenityTypeMapLayerCreation(CARPARK, carParkAmenities, true)
                adjustZoomToShowAllCarParks(carParkAmenities)
            }
            enableAndDisplayCarparkToggle()
        }
    }

    private inner class PanCarParkObserver : CarParkObserver() {
        override fun onChanged(it: AmenityData?) {
            if (panTrackingManager?.isInPanMode == true && panTrackingManager?.isEnable == true)
                super.onChanged(it)
        }
    }

}


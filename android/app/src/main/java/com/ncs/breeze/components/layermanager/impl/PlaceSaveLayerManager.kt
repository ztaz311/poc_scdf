package com.ncs.breeze.components.layermanager.impl

import android.content.Context
import androidx.annotation.UiThread
import com.breeze.model.SearchLocation
import com.breeze.model.enums.BookmarkType
import com.breeze.model.enums.CollectionCode
import com.breeze.model.extensions.toBitmap
import com.google.android.gms.maps.model.LatLng
import com.google.gson.JsonObject
import com.mapbox.geojson.Feature
import com.mapbox.geojson.Point
import com.mapbox.maps.MapView
import com.mapbox.maps.extension.style.expressions.dsl.generated.eq
import com.mapbox.maps.extension.style.expressions.generated.Expression
import com.mapbox.maps.extension.style.layers.addLayer
import com.mapbox.maps.extension.style.layers.addLayerAbove
import com.mapbox.maps.extension.style.layers.generated.symbolLayer
import com.mapbox.maps.extension.style.layers.properties.generated.IconAnchor
import com.ncs.breeze.R
import com.ncs.breeze.common.utils.ParkingIconUtils.ImageIconType
import com.ncs.breeze.components.layermanager.MarkerLayerManager
import com.ncs.breeze.components.marker.markerview2.DropPinMarkerTooltip
import com.ncs.breeze.components.marker.markerview2.SearchLocationPlaceSaveTooltip

class PlaceSaveLayerManager(mapView: MapView, val collectionCode: CollectionCode) :
    MarkerLayerManager(mapView) {

    companion object {
        const val DROP_PIN = "drop-pin"
        const val SEARCHED_LOCATION = "search-location"
    }

    init {
        loadMapIconImages(collectionCode, mapView.context)
    }

    @UiThread
    private fun loadMapIconImages(collectionCode: CollectionCode, context: Context) {
        val drawableIdsMap = hashMapOf(
            "$DROP_PIN-${ImageIconType.SELECTED.type}$ICON_POSTFIX" to R.drawable.ic_map_pin,
            "$SEARCHED_LOCATION-${ImageIconType.SELECTED.type}$ICON_POSTFIX" to R.drawable.destination_puck,
        )
        if (collectionCode == CollectionCode.SHORTCUTS) {
            drawableIdsMap["${BookmarkType.HOME.name}-${ImageIconType.SELECTED_NIGHT.type}$ICON_POSTFIX"] =
                R.drawable.ic_poi_home_selected_dark
            drawableIdsMap["${BookmarkType.HOME.name}-${ImageIconType.UNSELECTED_NIGHT.type}$ICON_POSTFIX"] =
                R.drawable.ic_poi_home_unselected_dark
            drawableIdsMap["${BookmarkType.HOME.name}-${ImageIconType.SELECTED.type}$ICON_POSTFIX"] =
                R.drawable.c_poi_home_selected
            drawableIdsMap["${BookmarkType.HOME.name}-${ImageIconType.UNSELECTED.type}$ICON_POSTFIX"] =
                R.drawable.ic_poi_home_unselected
            drawableIdsMap["${BookmarkType.WORK.name}-${ImageIconType.SELECTED_NIGHT.type}$ICON_POSTFIX"] =
                R.drawable.ic_poi_work_selected_dark
            drawableIdsMap["${BookmarkType.WORK.name}-${ImageIconType.UNSELECTED_NIGHT.type}$ICON_POSTFIX"] =
                R.drawable.ic_poi_work_unselected_dark
            drawableIdsMap["${BookmarkType.WORK.name}-${ImageIconType.SELECTED.type}$ICON_POSTFIX"] =
                R.drawable.ic_poi_work_selected
            drawableIdsMap["${BookmarkType.WORK.name}-${ImageIconType.UNSELECTED.type}$ICON_POSTFIX"] =
                R.drawable.ic_poi_work_unselected
            drawableIdsMap["${BookmarkType.NORMAL}-${ImageIconType.SELECTED_NIGHT.type}$ICON_POSTFIX"] =
                R.drawable.ic_poi_shortcut_selected_dark
            drawableIdsMap["${BookmarkType.NORMAL}-${ImageIconType.UNSELECTED_NIGHT.type}$ICON_POSTFIX"] =
                R.drawable.ic_poi_shortcut_unselected_dark
            drawableIdsMap["${BookmarkType.NORMAL}-${ImageIconType.SELECTED.type}$ICON_POSTFIX"] =
                R.drawable.ic_poi_shortcut_selected
            drawableIdsMap["${BookmarkType.NORMAL}-${ImageIconType.UNSELECTED.type}$ICON_POSTFIX"] =
                R.drawable.ic_poi_shortcut_unselected
        } else {
            drawableIdsMap["${BookmarkType.CUSTOM.name}-${ImageIconType.SELECTED_NIGHT.type}$ICON_POSTFIX"] =
                R.drawable.ic_poi_sellected_dark
            drawableIdsMap["${BookmarkType.CUSTOM.name}-${ImageIconType.UNSELECTED_NIGHT.type}$ICON_POSTFIX"] =
                R.drawable.ic_poi_unsellected_dark
            drawableIdsMap["${BookmarkType.CUSTOM.name}-${ImageIconType.SELECTED.type}$ICON_POSTFIX"] =
                R.drawable.ic_poi_sellected_light
            drawableIdsMap["${BookmarkType.CUSTOM.name}-${ImageIconType.UNSELECTED.type}$ICON_POSTFIX"] =
                R.drawable.ic_poi_unsellected_light
        }
        for ((key, value) in drawableIdsMap) {
            value.toBitmap(context)?.let {
                mapViewRef.get()?.getMapboxMap()?.getStyle()?.addImage(key, it, false)
            }
        }

    }

    override fun shouldClearMarkersOnMapFreeClick() = false

    private fun initDropPinTooltip(initialLocation: Point) {
        val mapView = mapViewRef.get() ?: return
        val dropPinMarkerTooltip =
            DropPinMarkerTooltip(
                mapView,
                DROP_PIN,
                DropPinMarkerTooltip.Screen.COLLECTION_DETAILS
            ).apply {
                analyticsScreenName = when (collectionCode) {
                    CollectionCode.SHORTCUTS -> "[shortcut_collection]"
                    CollectionCode.SAVED_PLACES -> "[my_saved_places_collection]"
                    else -> "[custom_saved_places_collection]"
                }
                onCloseTooltip = {
                    (marker?.markerView2 as? DropPinMarkerTooltip)?.hideTooltip()
                    removeMarkers(DROP_PIN)
                }
            }
        marker = MarkerViewType(dropPinMarkerTooltip, DROP_PIN, Feature.fromGeometry(initialLocation))
        mapView.addView(dropPinMarkerTooltip.mViewMarker)
    }

    fun showDropPinMarkerWithTooltip(point: Point) {
        if (marker?.markerView2 !is DropPinMarkerTooltip) {
            initDropPinTooltip(point)
        }
        val properties = JsonObject()
        properties.addProperty(HIDDEN, false)
        (marker?.markerView2 as? DropPinMarkerTooltip)?.run {
            hideMarker()
            mLatLng = LatLng(point.latitude(), point.longitude())
            addOrReplaceMarker(DROP_PIN, Feature.fromGeometry(point, properties))
            showTooltip()
        }
    }

    fun showSearchLocationMarkerWithTooltip(searchedLocation: SearchLocation) {
        val mapView = mapViewRef.get() ?: return
        val latitude = searchedLocation.lat?.toDoubleOrNull() ?: return
        val longitude = searchedLocation.long?.toDoubleOrNull() ?: return
        val pointLocation = Point.fromLngLat(longitude, latitude)
        val tooltip = SearchLocationPlaceSaveTooltip(mapView).apply {
            analyticsScreenName = when (collectionCode) {
                CollectionCode.SHORTCUTS -> "[shortcut_collection]"
                CollectionCode.SAVED_PLACES -> "[my_saved_places_collection]"
                else -> "[custom_saved_places_collection]"
            }
        }
        marker = MarkerViewType(
            tooltip,
            SEARCHED_LOCATION,
            Feature.fromGeometry(pointLocation)
        )
        mapView.addView(tooltip.mViewMarker)
        val properties = JsonObject()
        properties.addProperty(HIDDEN, false)
        addOrReplaceMarker(SEARCHED_LOCATION, Feature.fromGeometry((pointLocation), properties))
        tooltip.showTooltip(searchedLocation)
        tooltip.updateTooltipPosition()
    }

    fun removeDropPinMarkerAndTooltip() {
        removeMarkers(DROP_PIN)
    }

    fun removeSearchedLocationMarker() {
        removeSourceAndLayer(SEARCHED_LOCATION)
    }

    fun removeSearchedLocationTooltip() {
        removeMarkerTooltipView(SEARCHED_LOCATION)
    }

    override fun addStyleLayer(
        type: String,
        sourceId: String,
        iconSizeExp: Expression
    ) {
        symbolLayer(type + LAYER_POSTFIX, sourceId) {
            addIconImageExpression(this, type)
            iconSize(iconSizeExp)
            iconAnchor(IconAnchor.BOTTOM)
            iconAllowOverlap(true)
            iconIgnorePlacement(true)
            textAllowOverlap(true)
            textIgnorePlacement(true)
            filter(eq {
                get {
                    literal(HIDDEN)
                }
                literal(false)
            })
        }.let { symbolLayer ->
            if (mapViewRef.get()?.getMapboxMap()?.getStyle()?.styleLayers.isNullOrEmpty()) {
                mapViewRef.get()?.getMapboxMap()?.getStyle()?.addLayer(symbolLayer)
            } else {
                mapViewRef.get()?.getMapboxMap()?.getStyle()?.addLayerAbove(
                    symbolLayer,
                    mapViewRef.get()?.getMapboxMap()?.getStyle()?.styleLayers?.last()?.id
                )
            }
        }
    }

    fun removeDropPinMarker() {
        removeMarkers(DROP_PIN)
    }


    fun removeFocusedBookMarkToolTip() {
        removeMarkerTooltipView(BookmarkType.CUSTOM.name)
        removeMarkerTooltipView(BookmarkType.HOME.name)
        removeMarkerTooltipView(BookmarkType.WORK.name)
        removeMarkerTooltipView(BookmarkType.NORMAL.name)
    }

}

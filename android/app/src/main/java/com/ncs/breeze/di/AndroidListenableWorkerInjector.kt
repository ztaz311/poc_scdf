package com.ncs.breeze.di

import androidx.work.ListenableWorker
import dagger.Module
import dagger.android.AndroidInjector
import dagger.multibindings.Multibinds

object AndroidListenableWorkerInjection {
    fun inject(worker: ListenableWorker) {
        val application = worker.applicationContext
        if (application !is HasListenableWorkerInjector) {
            throw RuntimeException(
                "${application.javaClass.canonicalName} does not implement ${HasListenableWorkerInjector::class.java.canonicalName}"
            )
        }

        val workerInjector = (application as HasListenableWorkerInjector).workerInjector()
        workerInjector.inject(worker)
    }
}

interface HasListenableWorkerInjector {
    fun workerInjector(): AndroidInjector<ListenableWorker>
}

@Module
abstract class AndroidListenableWorkerInjectionModule {

    @Multibinds
    abstract fun workerInjectorFactories(): Map<Class<out ListenableWorker>, AndroidInjector.Factory<out ListenableWorker>>

//    @ContributesAndroidInjector
//    abstract fun contributeOBUTransactionsHistoryUploadWorker(): OBUTransactionsHistoryUploadWorker
//
//    @ContributesAndroidInjector
//    abstract fun contributeOBUChargingInfoUploadWorker(): OBUChargingInfoUploadWorker
}
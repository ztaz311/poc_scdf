package com.ncs.breeze.car.breeze.screen.routeplanning.routesurface

import androidx.lifecycle.Lifecycle
import com.mapbox.androidauto.internal.extensions.getStyle
import com.mapbox.androidauto.internal.logAndroidAuto
import com.mapbox.maps.extension.androidauto.MapboxCarMapObserver
import com.mapbox.maps.extension.androidauto.MapboxCarMapSurface
import com.mapbox.maps.extension.style.layers.getLayer
import com.mapbox.maps.plugin.locationcomponent.LocationComponentConstants
import com.mapbox.maps.plugin.locationcomponent.OnIndicatorPositionChangedListener
import com.mapbox.maps.plugin.locationcomponent.location
import com.mapbox.navigation.core.directions.session.RoutesObserver
import com.mapbox.navigation.core.lifecycle.MapboxNavigationApp
import com.mapbox.navigation.core.trip.session.RouteProgressObserver
import com.mapbox.navigation.ui.maps.route.RouteLayerConstants
import com.mapbox.navigation.ui.maps.route.arrow.api.MapboxRouteArrowApi
import com.mapbox.navigation.ui.maps.route.arrow.api.MapboxRouteArrowView
import com.mapbox.navigation.ui.maps.route.arrow.model.RouteArrowOptions
import com.mapbox.navigation.ui.maps.route.line.api.MapboxRouteLineApi
import com.mapbox.navigation.ui.maps.route.line.api.MapboxRouteLineView
import com.mapbox.navigation.ui.maps.route.line.model.MapboxRouteLineOptions
import com.mapbox.navigation.ui.maps.route.line.model.RouteLineColorResources
import com.mapbox.navigation.ui.maps.route.line.model.RouteLineResources
import com.ncs.breeze.R
import com.ncs.breeze.car.breeze.MainBreezeCarContext
import com.breeze.model.constants.Constants


class CarRouteLine(
    val mainCarContext: MainBreezeCarContext,
    val lifecycle: Lifecycle
) : MapboxCarMapObserver {

    val resources = mainCarContext.carContext.resources

    private val routeLineColorResources by lazy {
        RouteLineColorResources.Builder().routeSevereCongestionColor(
            resources.getColor(
                R.color.themed_traffic_severe_color,
                null
            )
        )
            .routeHeavyCongestionColor(
                resources.getColor(
                    R.color.themed_traffic_heavy_color,
                    null
                )
            )
            .routeModerateCongestionColor(
                resources.getColor(
                    R.color.themed_traffic_moderate_color,
                    null
                )
            )
            .routeDefaultColor(
                resources.getColor(
                    R.color.themed_traffic_low_color,
                    null
                )
            )
            .routeLowCongestionColor(
                resources.getColor(
                    R.color.themed_traffic_low_color,
                    null
                )
            )
            .routeCasingColor(resources.getColor(R.color.transparent, null))
            .routeUnknownCongestionColor(
                resources.getColor(
                    R.color.themed_traffic_unknown_color,
                    null
                )
            )
            .alternativeRouteSevereCongestionColor(
                resources.getColor(
                    R.color.themed_alt_traffic_severe_color,
                    null
                )
            )
            .alternativeRouteHeavyCongestionColor(
                resources.getColor(
                    R.color.themed_alt_traffic_heavy_color,
                    null
                )
            )
            .alternativeRouteModerateCongestionColor(
                resources.getColor(
                    R.color.themed_alt_traffic_moderate_color,
                    null
                )
            )
            .alternativeRouteDefaultColor(
                resources.getColor(
                    R.color.themed_alt_traffic_default_color,
                    null
                )
            )
            .alternativeRouteLowCongestionColor(
                resources.getColor(
                    R.color.themed_alt_traffic_low_color,
                    null
                )
            )
            .alternativeRouteCasingColor(
                resources.getColor(
                    R.color.transparent,
                    null
                )
            )
            .alternativeRouteUnknownCongestionColor(
                resources.getColor(
                    R.color.themed_alt_traffic_unknown_color,
                    null
                )
            )
            .build()
    }

    private val routeLineResources: RouteLineResources by lazy {
        RouteLineResources.Builder()
            .routeLineColorResources(routeLineColorResources)
            .originWaypointIcon(R.drawable.cursor_with_layer)
            .destinationWaypointIcon(R.drawable.destination_puck)
            .build()
    }

    private val options: MapboxRouteLineOptions by lazy {
        var routeLineBelowLayerID = LocationComponentConstants.LOCATION_INDICATOR_LAYER
        mainCarContext.mapboxCarMap.carMapSurface?.getStyle()?.let {
            if (it?.getLayer(Constants.SPEED_CAMERA) != null) {
                routeLineBelowLayerID = Constants.SPEED_CAMERA
            }
        }
        MapboxRouteLineOptions.Builder(mainCarContext.carContext)
            .withRouteLineResources(routeLineResources)
            .withRouteLineBelowLayerId(routeLineBelowLayerID)
            .withVanishingRouteLineEnabled(true) //AAL
            .build()
    }

    private val routeLineView by lazy {
        MapboxRouteLineView(options)
    }

    private val routeLineApi: MapboxRouteLineApi by lazy {
        MapboxRouteLineApi(options)
    }

    val routeArrowApi: MapboxRouteArrowApi by lazy {
        MapboxRouteArrowApi()
    }

    private val routeArrowOptions by lazy {
        RouteArrowOptions.Builder(mainCarContext.carContext)
            .withAboveLayerId(RouteLayerConstants.TOP_LEVEL_ROUTE_LINE_LAYER_ID)
            .build()
    }

    private val routeArrowView: MapboxRouteArrowView by lazy {
        MapboxRouteArrowView(routeArrowOptions)
    }

    private val onPositionChangedListener = OnIndicatorPositionChangedListener { point ->
        val result = routeLineApi.updateTraveledRouteLine(point)
        mainCarContext.mapboxCarMap.carMapSurface?.getStyle()?.let { style ->
            routeLineView.renderRouteLineUpdate(style, result) //AAL
        }
    }

    private val routesObserver = RoutesObserver { result ->
        logAndroidAuto("CarRouteLine onRoutesChanged ${result.navigationRoutes.size}")
        val carMapSurface = mainCarContext.mapboxCarMap.carMapSurface!!
        if (result.navigationRoutes.isNotEmpty()) {
            routeLineApi.setNavigationRoutes(result.navigationRoutes) { value ->
                carMapSurface.getStyle()?.let { style ->
                    routeLineView.renderRouteDrawData(style, value)
                }
            }
        } else {
            routeLineApi.clearRouteLine { value ->
                carMapSurface.getStyle()?.let { style ->
                    routeLineView.renderClearRouteLineValue(style, value)
                }
            }
            val clearArrowValue = routeArrowApi.clearArrows()
            carMapSurface.getStyle()?.let { style ->
                routeArrowView.render(style, clearArrowValue)
            }
        }
    }

    val routeProgressObserver = RouteProgressObserver { routeProgress ->
        mainCarContext.mapboxCarMap.carMapSurface?.let { carMapSurface ->
            routeLineApi.updateWithRouteProgress(routeProgress) { result ->
                carMapSurface.getStyle()?.let { style ->
                    routeLineView.renderRouteLineUpdate(style, result) //AAL
                }
            }
            routeArrowApi.addUpcomingManeuverArrow(routeProgress).also { arrowUpdate ->
                carMapSurface.getStyle()?.let { style ->
                    routeArrowView.renderManeuverUpdate(style, arrowUpdate)
                }
            }
        }
    }

    override fun onAttached(mapboxCarMapSurface: MapboxCarMapSurface) {
        logAndroidAuto("CarRouteLine carMapSurface loaded $mapboxCarMapSurface")
        val locationPlugin = mapboxCarMapSurface.mapSurface.location
        mapboxCarMapSurface.getStyle()?.let { style ->
            routeLineView.initializeLayers(style)
        }
        locationPlugin.addOnIndicatorPositionChangedListener(onPositionChangedListener)
        MapboxNavigationApp.current()?.run {
            registerRouteProgressObserver(routeProgressObserver)
            registerRoutesObserver(routesObserver)
            historyRecorder.startRecording()
        }
    }

    override fun onDetached(mapboxCarMapSurface: MapboxCarMapSurface) {
        logAndroidAuto("CarRouteLine carMapSurface detached $mapboxCarMapSurface")
        mapboxCarMapSurface?.mapSurface?.location
            ?.removeOnIndicatorPositionChangedListener(onPositionChangedListener)
        MapboxNavigationApp.current()?.run {
            unregisterRouteProgressObserver(routeProgressObserver)
            unregisterRoutesObserver(routesObserver)
        }
        routeLineView.cancel()
        routeLineApi.cancel()
        mapboxCarMapSurface?.getStyle()?.let { style ->
            //routeLineView.hideOriginAndDestinationPoints(style)
            routeLineApi.clearRouteLine { value ->
                routeLineView.renderClearRouteLineValue(style, value)
                routeArrowView?.render(style, routeArrowApi.clearArrows())
            }

        }
        MapboxNavigationApp.current()?.historyRecorder?.stopRecording {
            logAndroidAuto("CarRouteLine saved history $it")
        }
    }
}

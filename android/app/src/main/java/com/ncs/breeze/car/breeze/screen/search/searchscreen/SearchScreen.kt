package com.ncs.breeze.car.breeze.screen.search.searchscreen

import androidx.annotation.StringRes
import androidx.car.app.model.*
import com.mapbox.androidauto.internal.logAndroidAutoFailure
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.Event
import com.breeze.model.SearchLocation
import com.ncs.breeze.common.model.rx.AddToRecentList
import com.ncs.breeze.common.model.rx.AppToRoutePlanningStartEvent
import com.ncs.breeze.common.model.rx.AppToSearchLocationEvent
import com.ncs.breeze.common.model.rx.AppToSearchLocationLatLngGoogleEvent
import com.ncs.breeze.common.model.rx.DataForRoutePreview
import com.ncs.breeze.common.model.rx.GetLatLngGoogleIfNeed
import com.ncs.breeze.common.model.rx.GetSearchLocationResult
import com.ncs.breeze.common.model.rx.ToCarEventData
import com.ncs.breeze.common.model.rx.ToCarRxEvent
import com.breeze.model.extensions.safeDouble
import com.ncs.breeze.car.breeze.MainBreezeCarContext
import com.ncs.breeze.car.breeze.base.BaseScreenCar
import com.ncs.breeze.car.breeze.utils.CarConstants
import com.ncs.breeze.car.breeze.utils.LimitClick
import com.ncs.breeze.car.breeze.utils.setClickSafe
import com.ncs.breeze.R
import com.mapbox.geojson.Point
import com.ncs.breeze.common.analytics.Screen
import com.ncs.breeze.common.event.ToCarRx
import com.ncs.breeze.common.event.ToAppRx
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class SearchScreen(val mainCarContext: MainBreezeCarContext, val keySearch: String = "") :
    BaseScreenCar(mainCarContext.carContext) {
    var listData: ArrayList<SearchLocation> = ArrayList()

    override fun onGetTemplate(): Template {
        return SearchTemplate.Builder(
            object : SearchTemplate.SearchCallback {
                override fun onSearchTextChanged(searchText: String) {
                    Analytics.logEditEvent("[textbox]:location_input", "[androidauto_search]")
                    ToAppRx.postEvent(GetSearchLocationResult(searchText))
                }

                override fun onSearchSubmitted(searchTerm: String) {
                    logAndroidAutoFailure("onSearchSubmitted not implemented $searchTerm")
                }
            })
            .setHeaderAction(Action.BACK)
            .setShowKeyboardByDefault(false)
            .setItemList(buildErrorItemList(R.string.car_search_no_results))
            .build()
    }

    override fun getScreenName(): String {
        return Screen.AA_SEARCH_SCREEN
    }

    override fun onCreateScreen() {
        super.onCreateScreen()
        handleSearchWithKey(keySearch)
    }

    private fun handleEventFromApp(event: ToCarRxEvent?) {
        when (event) {
            is AppToSearchLocationEvent -> {
                listData = event.data.list
                invalidate()
            }

            is AppToSearchLocationLatLngGoogleEvent -> {
                onFinishedSelect(event.data.data)
            }

            else -> {}
        }
    }

    override fun onReceiverEventFromApp(event: ToCarRxEvent?) {
        super.onReceiverEventFromApp(event)
        handleEventFromApp(event)
    }


    private fun buildErrorItemList(@StringRes stringRes: Int): ItemList {
        val itemListBuilder = ItemList.Builder()
        if (listData.size == 0) {
            itemListBuilder
                .setNoItemsMessage(carContext.getString(stringRes))
        } else {
            for (item in listData) {
                itemListBuilder.addItem(
                    Row.Builder()
                        .setTitle(item.address1 ?: "N/A")
                        .addText(item.address2 ?: item.address1 ?: "N/A")
                        .setClickSafe {
                            LimitClick.handleSafe {
                                Analytics.logClickEvent("[textbox]:result_select", "[androidauto_search]")
                                onSelected(item)
                            }
                        }
                        .build()
                )
            }
        }

        return itemListBuilder.build()
    }

    fun handleSearchWithKey(key: String = "") {
        if (key != "") {
            ToAppRx.postEvent(GetSearchLocationResult(key))
        }
    }


    override fun onScreenUpdateEvent(data: ToCarEventData?) {
        super.onScreenUpdateEvent(data)
    }

    private fun onSelected(item: SearchLocation) {
        // Get Lat long Google if need
        if (item.source == "GOOGLE") {
            ToAppRx.postEvent(GetLatLngGoogleIfNeed((item)))
            return
        }
        onFinishedSelect(item)
    }

    private fun onFinishedSelect(item: SearchLocation) {
        val originalDestination = item.convertToDestinationAddressDetails()
        ToAppRx.postEvent(AddToRecentList((item)))
        screenManager.popTo(CarConstants.HOME_MARKER)
        CoroutineScope(Dispatchers.Main).launch {
            ToCarRx.postEventWithCacheLast(
                AppToRoutePlanningStartEvent(
                    DataForRoutePreview(
                        selectedDestination = Point.fromLngLat(
                            item.long!!.safeDouble(),
                            item.lat!!.safeDouble(),
                        ),
                        originalDestination = originalDestination,
                        destinationSearchLocation = item
                    )
                )
            )
        }
    }
}
package com.ncs.breeze.common.utils

import android.content.Context
import android.util.Log
import com.breeze.voice_engine.service.BreezeSpeechService
import com.breeze.voice_engine.service.impl.riva.RivaSpeechService
import com.breeze.voice_engine.utils.VoiceInstructionsHelper
import com.mapbox.api.directions.v5.models.VoiceInstructions
import com.mapbox.navigation.core.lifecycle.MapboxNavigationApp
import com.mapbox.navigation.core.trip.session.VoiceInstructionsObserver
import com.ncs.breeze.common.event.ToAppRx
import com.ncs.breeze.common.event.ToCarRx
import com.ncs.breeze.common.model.rx.UpdateStateVoiceApp
import com.ncs.breeze.common.model.rx.UpdateStateVoiceCar
import com.ncs.breeze.common.storage.BreezeUserPreference
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class VoiceInstructionsManager(context: Context) {

    var isUserMute: Boolean = false
    var isWalkingPath = false
    private lateinit var breezeSpeechService: BreezeSpeechService
    private var isRegisterVoiceInstructionManager = false
    val context: Context = context.applicationContext

    private val voiceInstructionsObserver =
        VoiceInstructionsObserver { voiceInstructions ->

            var normalInstructionTextBefore = ""
            var SSMLInstructionTextBefore = ""

            if (!voiceInstructions.announcement().isNullOrEmpty()) {
                normalInstructionTextBefore = voiceInstructions.announcement()!!
            }
            if (!voiceInstructions.ssmlAnnouncement().isNullOrEmpty()) {
                SSMLInstructionTextBefore = voiceInstructions.ssmlAnnouncement()!!
            }

            Log.d(TAG, "-- Voice instructions received --")
            Log.d(TAG, "-- Normal text Before [${normalInstructionTextBefore}] --")
            Log.d(TAG, "-- SSML text Before [${SSMLInstructionTextBefore}] --")

            if (ALTER_VOICE_INSTRUCTIONS) {
                CoroutineScope(Dispatchers.IO).launch {
                    try {
                        var normalInstructionTextAfter = ""
                        var SSMLInstructionTextAfter = ""
                        normalInstructionTextAfter =
                            VoiceInstructionsHelper.getModifiedVoiceInstruction(
                                false,
                                normalInstructionTextBefore
                            )
                        SSMLInstructionTextAfter = if (SSMLInstructionTextBefore.isNullOrBlank()) {
                            ""
                        } else {
                            VoiceInstructionsHelper.getModifiedVoiceInstruction(
                                true,
                                SSMLInstructionTextBefore
                            )
                        }

                        Log.d(TAG, "-- Normal text After [${normalInstructionTextAfter}] --")
                        Log.d(TAG, "-- SSML text After [${SSMLInstructionTextAfter}] --")

                        val modifiedInstructions = VoiceInstructions.builder()
                        modifiedInstructions.announcement(normalInstructionTextAfter)
                        modifiedInstructions.ssmlAnnouncement(SSMLInstructionTextAfter)
                        modifiedInstructions.distanceAlongGeometry(voiceInstructions.distanceAlongGeometry())
                        val inst = modifiedInstructions.build()
                        breezeSpeechService.playVoiceInstructions(
                            voiceStringPlain = inst.announcement()!!,
                            voiceStringSSML = inst.ssmlAnnouncement()!!
                        )
                    } catch (e: Exception) {
                        Log.e(TAG, "error occured altering speech text", e)
                        breezeSpeechService.playVoiceInstructions(
                            voiceStringPlain = voiceInstructions.announcement()!!,
                            voiceStringSSML = voiceInstructions.ssmlAnnouncement()!!
                        )
                    }
                }
            } else {
                breezeSpeechService.playVoiceInstructions(
                    voiceStringPlain = voiceInstructions.announcement()!!,
                    voiceStringSSML = voiceInstructions.ssmlAnnouncement()!!
                )
            }
        }

    /**
     * setup voice instruction
     */
    fun setUp() {
        breezeSpeechService = RivaSpeechService()
        breezeSpeechService.setup(context)
    }


    fun registerVoiceInstruction() {
        MapboxNavigationApp.current()
            ?.takeIf { !isRegisterVoiceInstructionManager}
            ?.let {
                it.registerVoiceInstructionsObserver(voiceInstructionsObserver)
                isRegisterVoiceInstructionManager = true
            }
    }

    fun unRegisterVoiceInstruction() {
        MapboxNavigationApp.current()
            ?.takeIf { isRegisterVoiceInstructionManager }
            ?.let {
                it.unregisterVoiceInstructionsObserver(
                    voiceInstructionsObserver
                )
                isRegisterVoiceInstructionManager = false
            }
    }

    fun cancelSpeech() {
        breezeSpeechService.stopSpeech()
    }

    private fun destroy() {
        isRegisterVoiceInstructionManager = false
        breezeSpeechService.destroy()
    }


    fun playVoiceInstructions(voicestring: String) {
        Log.d(TAG, "-- Custom voice command received --")
        Log.d(TAG, "-- Normal text instruction [${voicestring}] --")

        if (!isUserMute && !BreezeUserPreference.getInstance(context)
                .isStateOfVoiceStructions()
        ) {
            breezeSpeechService.playVoiceInstructions(voicestring, null)
        }
    }

    fun playSSMLVoiceInstructions(voicestring: String) {
        Log.d(TAG, "-- Custom voice command received --")
        Log.d(TAG, "-- SSML text instruction [${voicestring}] --")

        if (!isUserMute) {
            CoroutineScope(Dispatchers.IO).launch {
                var voiceInstructions = VoiceInstructions.builder()
                    .announcement("test")
                    .ssmlAnnouncement(
                        VoiceInstructionsHelper.getModifiedVoiceInstruction(
                            true,
                            voicestring
                        )
                    )
                    .build()
                breezeSpeechService.playVoiceInstructions(
                    voiceStringPlain = voiceInstructions.announcement()!!,
                    voiceStringSSML = voiceInstructions.ssmlAnnouncement()!!
                )

            }
        }
    }

    fun muteVoiceInstructions() {
        isUserMute = true
        breezeSpeechService.toggleMute(isUserMute)
        ToAppRx.postEvent(UpdateStateVoiceApp(isUserMute))
        ToCarRx.postEvent(UpdateStateVoiceCar(isUserMute))
    }

    fun unmuteVoiceInstructions() {
        isUserMute = false
        breezeSpeechService.toggleMute(isUserMute)
        ToAppRx.postEvent(UpdateStateVoiceApp(isUserMute))
        ToCarRx.postEvent(UpdateStateVoiceCar(isUserMute))
    }

    fun toggleMute() {
        isUserMute = !isUserMute
        breezeSpeechService.toggleMute(isUserMute)
        BreezeUserPreference.getInstance(context)
            .setStateOfVoiceStructions(isUserMute)
        ToAppRx.postEvent(UpdateStateVoiceApp(isUserMute))
        ToCarRx.postEvent(UpdateStateVoiceCar(isUserMute))
    }

    companion object {
        var mInstance: VoiceInstructionsManager? = null
        private const val TAG = "SPEECH"
        private const val ALTER_VOICE_INSTRUCTIONS: Boolean = true

        fun getInstance(): VoiceInstructionsManager? {
            return mInstance
        }

        fun setupVoice(context: Context) {
            if (mInstance == null) {
                mInstance = VoiceInstructionsManager(context).apply {
                    setUp()
                }
            }
            mInstance!!.isUserMute = BreezeUserPreference.getInstance(context)
                .isStateOfVoiceStructions()
            if (mInstance?.isWalkingPath == true) {
                mInstance?.muteVoiceInstructions()
            } else {
                if (mInstance?.isUserMute == true) {
                    mInstance?.muteVoiceInstructions()
                } else {
                    mInstance?.unmuteVoiceInstructions()
                }
            }

        }

        fun destroyInstance() {
            mInstance?.run {
                unRegisterVoiceInstruction()
                destroy()
            }
            mInstance = null
        }
    }
}
package com.ncs.breeze.components.map.profile

import android.content.Context
import android.graphics.Color
import android.location.Location
import androidx.annotation.UiThread
import com.breeze.model.api.response.tbr.ResponseCongestionDetail
import com.breeze.model.api.response.tbr.Zones
import com.ncs.breeze.common.utils.BreezeMapDataHolder
import com.mapbox.geojson.Feature
import com.mapbox.geojson.LineString
import com.mapbox.geojson.Point
import com.mapbox.geojson.Polygon
import com.mapbox.maps.MapboxMap
import com.mapbox.maps.Style
import com.mapbox.maps.extension.style.layers.addLayer
import com.mapbox.maps.extension.style.layers.generated.FillLayer
import com.mapbox.maps.extension.style.layers.getLayerAs
import com.mapbox.maps.extension.style.layers.properties.generated.Visibility
import com.mapbox.maps.extension.style.sources.addSource
import com.mapbox.maps.extension.style.sources.generated.GeoJsonSource
import com.mapbox.maps.extension.style.sources.generated.geoJsonSource
import com.mapbox.maps.extension.style.sources.getSourceAs
import com.mapbox.turf.TurfConstants
import com.mapbox.turf.TurfMeasurement
import com.mapbox.turf.TurfMeta
import com.mapbox.turf.TurfTransformation
import com.ncs.breeze.ui.dashboard.manager.DashboardMapStateManager

open class ProfileZoneLayer
    (
    protected val mapboxMap: MapboxMap,
    protected val context: Context,
    protected val style: Style,
) {

    companion object {
        const val CIRCLE_STEPS = 360
        const val LAYER_ID = "_layer_id"
        const val SOURCE_ID = "_source_id"

        const val INNER_ZONE = "INNER_ZONE"
        const val OUTER_ZONE = "OUTER_ZONE"
        const val NO_ZONE = "NO_ZONE"

        /**
         * const define user in zone or not
         */
        const val LIMIT_CHECK_CONGESTION = 20


        fun getZoneMaxRadius(selectedMapProfile: String): Double {
            BreezeMapDataHolder.profilePreferenceHashmap[selectedMapProfile]?.zones?.let { profileZones ->
                if (profileZones.size == 0) {
                    return@let
                }
                profileZones.last().radius?.let {
                    return@getZoneMaxRadius it.toDouble()
                }
            }
            return 1000.0
        }

        fun getZoneMinRadius(selectedMapProfile: String): Double {
            BreezeMapDataHolder.profilePreferenceHashmap[selectedMapProfile]?.zones?.let { profileZones ->
                if (profileZones.size == 0) {
                    return@let
                }
                profileZones.first().radius?.let {
                    return@getZoneMinRadius it.toDouble()
                }
            }
            return 250.0
        }

        fun getZoneCenter(selectedMapProfile: String): Point? {
            BreezeMapDataHolder.profilePreferenceHashmap[selectedMapProfile]?.zones?.let { profileZones ->
                if (profileZones.size == 0) {
                    return@let
                }
                profileZones.first()?.let {
                    if (it.centerPointLong != null && it.centerPointLat != null) {
                        return@getZoneCenter Point.fromLngLat(
                            it.centerPointLong!!,
                            it.centerPointLat!!
                        )
                    }
                }
            }
            return null
        }

        /**
         * @return a pair zone type and profile
         */
        fun detectAllProfileLocationInZone(userLocation: Location?): ProfileZoneType {
            BreezeMapDataHolder.profilePreference.forEach { profile ->
                val profileZones = profile.zones
                if (profileZones.size == 0) {
                    return@forEach
                }
                val profilePoint = Point.fromLngLat(
                    profileZones.first().centerPointLong!!,
                    profileZones.first().centerPointLat!!
                )
                if (profilePoint == null || userLocation == null || profileZones.isEmpty()) {
                    return ProfileZoneType(
                        null,
                        null,
                        NO_ZONE
                    )
                }
                val distance = TurfMeasurement.distance(
                    profilePoint,
                    Point.fromLngLat(userLocation.longitude, userLocation.latitude),
                    TurfConstants.UNIT_METRES
                )
                if (profileZones.last().radius != null && profileZones[0].radius != null) {
                    if (distance > (profileZones.last().radius!!)) {
                        return@forEach
                    }
//                    else if (distance <= (profileZones.last().radius!!) && distance > (profileZones[0].radius!!)) {
//                        return@detectAllProfileLocationInZone ProfileZoneType(profile.elementName,profileZones.last().zoneId!!,
//                            OUTER_ZONE)
//                    }
//                    else if ((distance <= (profileZones[0].radius!!))) {
//                        return@detectAllProfileLocationInZone ProfileZoneType(profile.elementName,profileZones[0].zoneId!!,
//                            INNER_ZONE)
//                    }
                    else if ((distance <= (profileZones[0].radius!!))) {
                        return@detectAllProfileLocationInZone ProfileZoneType(
                            profile.elementName, profileZones[0].zoneId!!,
                            OUTER_ZONE
                        )
                    }
                }
                return@forEach
            }
            return ProfileZoneType(null, null, NO_ZONE)
        }

        fun detectSelectedProfileLocationInZone(userLocation: Location?): String {
            DashboardMapStateManager.selectedMapProfile?.let { selectedMapProfile ->
                BreezeMapDataHolder.profilePreferenceHashmap[selectedMapProfile]?.zones?.let { profileZones ->
                    if (profileZones.size == 0) {
                        return@let
                    }
                    val profilePoint = Point.fromLngLat(
                        profileZones.first().centerPointLong!!,
                        profileZones.first().centerPointLat!!
                    )
                    if (profilePoint == null || userLocation == null || profileZones.isNullOrEmpty()) return NO_ZONE
                    val distance = TurfMeasurement.distance(
                        profilePoint!!,
                        Point.fromLngLat(userLocation.longitude, userLocation.latitude),
                        TurfConstants.UNIT_METRES
                    )
                    if (profileZones.last().radius != null && profileZones[0].radius != null) {
                        if (distance > (profileZones.last().radius!!)) {
                            return@let
                        } else if (distance <= (profileZones.last().radius!!) && distance > (profileZones[0].radius!!)) {
                            return@detectSelectedProfileLocationInZone profileZones.last().zoneId!!
                        } else if ((distance <= (profileZones[0].radius!!))) {
                            return@detectSelectedProfileLocationInZone profileZones[0].zoneId!!
                        }
                    }
                    return@let
                }
            }
            return NO_ZONE
        }

        fun detectSelectedProfileLocationInZone(
            selectedProfile: String,
            userLocation: Location?
        ): ProfileZoneType {
            BreezeMapDataHolder.profilePreferenceHashmap[selectedProfile]?.let { profile ->
                val profileZones = profile.zones
                if (profileZones.size == 0) {
                    return@let
                }
                val profilePoint = Point.fromLngLat(
                    profileZones.first().centerPointLong!!,
                    profileZones.first().centerPointLat!!
                )
                if (profilePoint == null || userLocation == null || profileZones.isNullOrEmpty()) return ProfileZoneType(
                    null,
                    null,
                    NO_ZONE
                )
                val distance = TurfMeasurement.distance(
                    profilePoint!!,
                    Point.fromLngLat(userLocation.longitude, userLocation.latitude),
                    TurfConstants.UNIT_METRES
                )
                if (profileZones.last().radius != null && profileZones[0].radius != null) {
                    if (distance > (profileZones.last().radius!!)) {
                        return@let
                    }
//                    else if (distance <= (profileZones.last().radius!!) && distance > (profileZones[0].radius!!)) {
//                        return@detectSelectedProfileLocationInZone ProfileZoneType(profile.elementName,profileZones.last().zoneId!!,
//                            OUTER_ZONE)
//                    }
//                    else if ((distance <= (profileZones.last().radius!!))) {
//                        return@detectSelectedProfileLocationInZone ProfileZoneType(profile.elementName,profileZones.last().zoneId!!,
//                            INNER_ZONE)
//                    }
                    else if ((distance <= (profileZones.last().radius!!))) {
                        return@detectSelectedProfileLocationInZone ProfileZoneType(
                            profile.elementName, profileZones.last().zoneId!!,
                            OUTER_ZONE
                        )
                    }
                }
                return@let
            }
            return ProfileZoneType(null, null, NO_ZONE)
        }

        fun getInnerZone(selectedMapProfile: String): String? {
            BreezeMapDataHolder.profilePreferenceHashmap[selectedMapProfile]?.zones?.let { profileZones ->
                if (profileZones.size == 0) {
                    return@let
                }
                return@getInnerZone profileZones[0].zoneId
            }
            return null
        }

    }

    var selectedProfilePoint: Point? = null
    var maxRadius: Double = 1000.0
    private var isDrawCircle = false
    private var selectedZoneList: ArrayList<Zones> = ArrayList<Zones>()

    fun addSourceAndLayerID() {
        selectedZoneList.forEach {
            mapboxMap.getStyle { style ->
                style.addSource(
                    geoJsonSource("${it.zoneId}$SOURCE_ID") {
                    }
                )
                val fillLayerOuter = FillLayer(
                    "${it.zoneId}$LAYER_ID",
                    "${it.zoneId}$SOURCE_ID"
                )
                fillLayerOuter.fillOpacity(0.7)
                style.addLayer(fillLayerOuter)
            }
        }
    }


    fun updateColorOuterRing(pListColorResponse: ArrayList<ResponseCongestionDetail>) {
        pListColorResponse.forEachIndexed { _, data ->
            if (style.styleLayerExists("${data.name}$LAYER_ID")) {
                val fillLayer = style.getLayerAs<FillLayer>("${data.name}$LAYER_ID")
                fillLayer?.fillColor(Color.parseColor(data.color))
                fillLayer?.fillOpacity(data.opacity ?: 0.5)
            }
        }
    }

    fun updateZoneZoomDetails(profileZone: ArrayList<Zones>) {
        profileZone.forEachIndexed { _, data ->
            if (style.styleLayerExists("${data.zoneId}$LAYER_ID")) {
                val fillLayer = style.getLayerAs<FillLayer>("${data.zoneId}$LAYER_ID")
                fillLayer?.let { fillLayer ->
                    data.activeZoomLevel?.let {
                        fillLayer.minZoom(it.toDouble())
                    }
                    data.deactiveZoomLevel?.let {
                        fillLayer.maxZoom(it.toDouble())
                    }
                }
            }
        }
    }


    fun drawCircle() {
        if (selectedProfilePoint == null) {
            return
        }
        mapboxMap.getStyle { style ->
            isDrawCircle = true
            selectedZoneList.forEachIndexed() { position, zone ->
                if (position == 0) {
                    val innerCirclePolygon = getTurfPolygon(
                        (zone.radius)!!.toDouble(),
                        selectedProfilePoint!!
                    )
                    val innerCircleSource: GeoJsonSource =
                        style.getSourceAs("${zone.zoneId}$SOURCE_ID")!!

                    val feature = Feature.fromGeometry(
                        Polygon.fromOuterInner(
                            LineString.fromLngLats(TurfMeta.coordAll(innerCirclePolygon!!, false))
                        )
                    )
                    innerCircleSource.feature(feature)

                } else {

                    /**
                     * set radius max
                     */
                    maxRadius = (zone.radius ?: 0).toDouble()

                    val innerCirclePolygon = getTurfPolygon(
                        (selectedZoneList[position - 1].radius ?: 0).toDouble(),
                        selectedProfilePoint!!
                    )
                    val outerCirclePolygon = getTurfPolygon(
                        (zone.radius ?: 0).toDouble(),
                        selectedProfilePoint!!
                    )
                    val outerCircleSource: GeoJsonSource =
                        style.getSourceAs("${zone.zoneId}$SOURCE_ID")!!
                    outerCircleSource.feature(
                        Feature.fromGeometry(
                            Polygon.fromOuterInner(  // Create outer LineString
                                LineString.fromLngLats(
                                    TurfMeta.coordAll(
                                        outerCirclePolygon!!,
                                        false
                                    )
                                ),
                                LineString.fromLngLats(
                                    TurfMeta.coordAll(
                                        innerCirclePolygon!!,
                                        false
                                    )
                                )
                            )
                        )
                    )
                }
            }
        }
    }


    /**
     * remove all style & layer
     */
    fun remove() {
        isDrawCircle = false
        selectedZoneList.forEach {
            style.removeStyleLayer("${it.zoneId}$LAYER_ID")
            style.removeStyleSource("${it.zoneId}$SOURCE_ID")
        }
        selectedZoneList.clear()
    }

    fun hideCircleLayer() {
        mapboxMap.getStyle {
            selectedZoneList.forEach {
                val layer = style.getLayerAs<FillLayer>("${it.zoneId}$LAYER_ID")
                layer?.visibility(Visibility.NONE)
            }
        }
    }


    fun showCircleLayer() {
        mapboxMap.getStyle {
            selectedZoneList.forEach {
                val layer = style.getLayerAs<FillLayer>("${it.zoneId}$LAYER_ID")
                layer?.visibility(Visibility.VISIBLE)
            }
        }
    }


    /**
     * update config circle layer
     */
    fun updateConfigCircleLayer(pListZone: ArrayList<Zones>) {
        remove()
        selectedZoneList.addAll(pListZone)
        if (!selectedZoneList.isNullOrEmpty()) {
            val lat = selectedZoneList[0].centerPointLat ?: 1.2856954590697074
            val long = selectedZoneList[0].centerPointLong ?: 103.83289725755539
            selectedProfilePoint = Point.fromLngLat(long, lat)
        }
    }


    private fun getTurfPolygon(radius: Double, centerPoint: Point): Polygon? {
        return TurfTransformation.circle(
            centerPoint,
            radius,
            CIRCLE_STEPS,
            TurfConstants.UNIT_METERS
        )
    }

    @UiThread
    fun onDestroy() {
        isDrawCircle = false
    }


    data class ProfileZoneType(
        val selectedProfile: String?,
        val profileZoneId: String?,
        val type: String
    )
}
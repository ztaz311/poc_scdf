package com.ncs.breeze.di.module.fragment

import com.ncs.breeze.ui.dashboard.fragments.view.RoutePreferenceFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class RoutePreferenceModule {
    @ContributesAndroidInjector
    abstract fun contributeRoutePrefFragmentFragment(): RoutePreferenceFragment
}
package com.ncs.breeze.common.utils.mapStyle

import android.content.Context
import com.ncs.breeze.R
import com.mapbox.maps.extension.style.expressions.generated.Expression
import com.mapbox.navigation.ui.maps.route.line.model.RouteLineColorResources
import com.mapbox.navigation.ui.maps.route.line.model.RouteLineResources
import com.mapbox.navigation.ui.maps.route.line.model.RouteLineScaleValue

class BreezeRoutePlanningMapStyle {
    private val routeThicknessScale = 1.5f

    private val routeLineScaleExpression: Expression = buildScalingExpression(
        listOf(
            RouteLineScaleValue(10f, 3f, routeThicknessScale),
            RouteLineScaleValue(14f, 6.5f, routeThicknessScale),
            RouteLineScaleValue(16.5f, 3f, routeThicknessScale),
            RouteLineScaleValue(19f, 3f, routeThicknessScale), //modified as an example
            RouteLineScaleValue(22f, 3f, routeThicknessScale)
        )
    )

    fun generateRouteLineResourceBuilder(context: Context) = RouteLineResources.Builder()
        .routeLineColorResources(createRouteLineColorResources(context))
        .routeCasingLineScaleExpression(routeLineScaleExpression)
        .routeLineScaleExpression(routeLineScaleExpression)
        .routeTrafficLineScaleExpression(routeLineScaleExpression)
        .alternativeRouteCasingLineScaleExpression(routeLineScaleExpression)
        .alternativeRouteLineScaleExpression(routeLineScaleExpression)
        .alternativeRouteTrafficLineScaleExpression(routeLineScaleExpression)
        .originWaypointIcon(R.drawable.cursor_with_layer)

    private fun createRouteLineColorResources(context: Context) = RouteLineColorResources.Builder()
        .routeSevereCongestionColor(context.getColor(R.color.rp_route_severe_congestion_color))
        .routeHeavyCongestionColor(
            context.getColor(R.color.rp_route_heavy_congestion_color)
        )
        .routeModerateCongestionColor(
            context.getColor(R.color.rp_route_moderate_congestion_color)
        )
        .routeDefaultColor(context.getColor(R.color.themed_breeze_primary))
        .routeLowCongestionColor(
            context.getColor(
                R.color.rp_route_low_congestion_color
            )
        )
        .routeCasingColor(context.getColor(R.color.rp_route_casing_color))
        .routeUnknownCongestionColor(
            context.getColor(
                R.color.rp_route_unknown_congestion_color
            )
        )
        .alternativeRouteSevereCongestionColor(
            context.getColor(
                R.color.rp_alternative_route_severe_congestion_color
            )
        )
        .alternativeRouteHeavyCongestionColor(
            context.getColor(
                R.color.rp_alternative_route_heavy_congestion_color
            )
        )
        .alternativeRouteModerateCongestionColor(
            context.getColor(
                R.color.rp_alternative_route_moderate_congestion_color
            )
        )
        .alternativeRouteDefaultColor(
            context.getColor(
                R.color.rp_alternative_route_default_color
            )
        )
        .alternativeRouteLowCongestionColor(
            context.getColor(
                R.color.rp_alternative_route_low_congestion_color
            )
        )
        .alternativeRouteCasingColor(
            context.getColor(
                R.color.rp_alternative_route_casing_color
            )
        )
        .alternativeRouteUnknownCongestionColor(
            context.getColor(
                R.color.rp_alternative_route_unknown_congestion_color
            )
        )
        .build()


    private fun buildScalingExpression(scalingValues: List<RouteLineScaleValue>): Expression {
        val expressionBuilder = Expression.ExpressionBuilder("interpolate")
        expressionBuilder.addArgument(Expression.exponential { literal(1.5) })
        expressionBuilder.zoom()
        scalingValues.forEach { routeLineScaleValue ->
            expressionBuilder.stop {
                this.literal(routeLineScaleValue.scaleStop.toDouble())
                product {
                    literal(routeLineScaleValue.scaleMultiplier.toDouble())
                    literal(routeLineScaleValue.scale.toDouble())
                }
            }
        }
        return expressionBuilder.build()
    }
}
package com.ncs.breeze.car.breeze.utils

import android.content.Context
import android.graphics.Color
import androidx.car.app.CarContext
import com.breeze.model.ERPFeatures
import com.breeze.model.extensions.visibleAmount
import com.breeze.model.enums.TrafficIncidentData
import com.breeze.model.api.response.amenities.BaseAmenity
import com.breeze.model.constants.Constants
import com.ncs.breeze.common.utils.ParkingIconUtils
import com.ncs.breeze.common.utils.ParkingIconUtils.ParkingMarkerConstants
import com.ncs.breeze.common.utils.RoutePlanningConsts
import com.ncs.breeze.car.breeze.model.CarParkLayerModel
import com.ncs.breeze.car.breeze.style.CarParkSelectionStyle.Companion.CARPARK_INDEX
import com.ncs.breeze.car.breeze.style.CarParkSelectionStyle.Companion.CARPARK_IS_DESTINATION
import com.ncs.breeze.car.breeze.style.CarParkSelectionStyle.Companion.CARPARK_PRICE
import com.ncs.breeze.R
import com.mapbox.geojson.Feature
import com.mapbox.geojson.FeatureCollection
import com.mapbox.geojson.Point
import com.mapbox.maps.Style
import com.mapbox.maps.extension.style.expressions.dsl.generated.switchCase
import com.mapbox.maps.extension.style.expressions.generated.Expression
import com.mapbox.maps.extension.style.layers.addLayer
import com.mapbox.maps.extension.style.layers.generated.symbolLayer
import com.mapbox.maps.extension.style.layers.properties.generated.IconAnchor
import com.mapbox.maps.extension.style.layers.properties.generated.IconTextFit
import com.mapbox.maps.extension.style.layers.properties.generated.TextAnchor
import com.mapbox.maps.extension.style.sources.addSource
import com.mapbox.maps.extension.style.sources.generated.geoJsonSource

object BreezeMapBoxUtil {


    fun addERPLayerToStyle(
        style: Style,
        erpFeatures: ERPFeatures,
        carContext: Context
    ) {

        //Removing all the source and layers relaated to ERP
        style.removeStyleLayer(Constants.ERP_LAYER)
        //style.removeStyleLayer(NO_RATES_ERP_LAYER)
        style.removeStyleSource(Constants.ERP_GEOJSONSOURCE)
        //style.removeStyleSource(NO_RATES_ERP_GEOJSONSOURCE)

        var erpFeatureCollection = FeatureCollection.fromFeatures(erpFeatures.featureListERP)


        var icon_size_exp = Expression.interpolate {
            linear()
            zoom()
            stop {
                literal(0.0)
                literal(0.0)
            }
            stop {
                literal(9.0)
                literal(0.0)
            }
            stop {
                literal(12.9)
                literal(0.0)
            }
            stop {
                literal(13.0)
                literal(0.8)
            }
            stop {
                literal(16.4)
                literal(1.0)
            }
        }

        var text_size_exp = Expression.interpolate {
            linear()
            zoom()
            stop {
                literal(0.0)
                literal(0.0)
            }
            stop {
                literal(9.0)
                literal(0.0)
            }
            stop {
                literal(12.9)
                literal(0.0)
            }
            stop {
                literal(13.0)
                literal(14.0)
            }
            stop {
                literal(16.4)
                literal(18.0)
            }
        }

        style.addSource(
            geoJsonSource(Constants.ERP_GEOJSONSOURCE) {
                featureCollection(erpFeatureCollection!!)
                //maxzoom(14)
            }
        )

        style.addLayer(
            symbolLayer(Constants.ERP_LAYER, Constants.ERP_GEOJSONSOURCE) {
                iconImage(
                    Constants.ERP_ICON
                )
                iconSize(icon_size_exp)
                iconAnchor(IconAnchor.BOTTOM)
                iconAllowOverlap(true)

                textField(Expression.get(Constants.ERP.ERP_RATE))
                textColor(carContext.getColor(R.color.themed_erp_price_text))
                textAnchor(TextAnchor.CENTER)
                textOffset(listOf(0.0, -1.6))
                textSize(text_size_exp)
                iconIgnorePlacement(true)
                //textAllowOverlap(true)
            }
        )
    }


    fun addNavigationERPLayerToStyle(
        style: Style,
        featureCollection: FeatureCollection,
        carContext: Context
    ) {

        //Removing all the source and layers relaated to ERP
        style.removeStyleLayer(Constants.ERP_LAYER)
        style.removeStyleSource(Constants.ERP_GEOJSONSOURCE)

        var erpFeatureCollection = featureCollection


        var icon_size_exp = Expression.interpolate {
            linear()
            zoom()
            stop {
                literal(0.0)
                literal(0.0)
            }
            stop {
                literal(9.0)
                literal(0.0)
            }
            stop {
                literal(12.9)
                literal(0.0)
            }
            stop {
                literal(13.0)
                literal(0.8)
            }
            stop {
                literal(16.4)
                literal(1.0)
            }
        }

        var text_size_exp = Expression.interpolate {
            linear()
            zoom()
            stop {
                literal(0.0)
                literal(0.0)
            }
            stop {
                literal(9.0)
                literal(0.0)
            }
            stop {
                literal(12.9)
                literal(0.0)
            }
            stop {
                literal(13.0)
                literal(14.0)
            }
            stop {
                literal(16.4)
                literal(18.0)
            }
        }

        style.addSource(
            geoJsonSource(Constants.ERP_GEOJSONSOURCE) {
                featureCollection(erpFeatureCollection!!)
                //maxzoom(14)
            }
        )

        style.addLayer(
            symbolLayer(Constants.ERP_LAYER, Constants.ERP_GEOJSONSOURCE) {
                iconImage(
                    Constants.ERP_ICON
                )
                iconSize(icon_size_exp)
                iconAnchor(IconAnchor.BOTTOM)
                iconAllowOverlap(true)

                textField(Expression.get(RoutePlanningConsts.PROP_ERP_DISPLAY))
                textColor(carContext.getColor(R.color.themed_erp_price_text))
                textAnchor(TextAnchor.CENTER)
                textOffset(listOf(0.0, -1.6))
                textSize(text_size_exp)
                iconIgnorePlacement(true)
                //textAllowOverlap(true)
            }
        )
    }


    fun displayIncidentData(style: Style, incidentMap: HashMap<String, FeatureCollection>) {
        TrafficIncidentData.values().forEach {
            val layerId = it.type + Constants.LAYER
            val sourceId = it.type + Constants.SOURCE
            val imgId = it.type + Constants.IMAGE

            style.removeStyleLayer(layerId)
            style.removeStyleSource(sourceId)

            if (incidentMap.get(it.type) != null) {
                addIncidentSymbolLayer(
                    style,
                    sourceId,
                    layerId,
                    imgId,
                    incidentMap.get(it.type)!!
                )
            }
        }
    }

    private fun addIncidentSymbolLayer(
        style: Style,
        sourceName: String,
        layerName: String,
        imgName: String,
        collection: FeatureCollection
    ) {
        var icon_size_exp = Expression.interpolate {
            linear()
            zoom()
            stop {
                literal(0.0)
                literal(0.0)
            }
            stop {
                literal(9.0)
                literal(0.0)
            }
            stop {
                literal(12.9)
                literal(0.0)
            }
            stop {
                literal(13.0)
                literal(0.8)
            }
            stop {
                literal(16.5)
                literal(1.0)
            }
        }

        style.addSource(
            geoJsonSource(sourceName) {
                featureCollection(collection)
            }
        )

        style.addLayer(
            symbolLayer(layerName, sourceName) {
                iconImage(
                    imgName
                )
                iconSize(icon_size_exp)
                iconAnchor(IconAnchor.CENTER)
                iconAllowOverlap(true)
            }
        )

    }

    @Deprecated("No longer in use", ReplaceWith("BreezeRoadObjectObserver"))
    fun addSilverZoneLayerToStyle(style: Style, featureList: ArrayList<Feature>) {

        style.removeStyleLayer(Constants.SILVER_ZONE_LAYER)
        style.removeStyleSource(Constants.SILVER_ZONE_SOURCE)
        var icon_size_exp = Expression.interpolate {
            linear()
            zoom()
            stop {
                literal(0.0)
                literal(0.0)
            }
            stop {
                literal(9.0)
                literal(0.0)
            }
            stop {
                literal(12.9)
                literal(0.0)
            }
            stop {
                literal(13.0)
                literal(0.8)
            }
        }


        var mFeatureCollection = FeatureCollection.fromFeatures(featureList)
        style.addSource(
            geoJsonSource(Constants.SILVER_ZONE_SOURCE) {
                featureCollection(mFeatureCollection!!)
                //maxzoom(14)
            }
        )

        style.addLayer(
            symbolLayer(
                Constants.SILVER_ZONE_LAYER,
                Constants.SILVER_ZONE_SOURCE
            ) {
                iconImage(
                    Constants.SILVER_ICON
                )
                iconSize(icon_size_exp)
                iconAnchor(IconAnchor.CENTER)
                iconAllowOverlap(false)
            }
        )
    }

    @Deprecated("No longer in use", ReplaceWith("BreezeRoadObjectObserver"))
    fun addSchoolZoneLayerToStyle(style: Style, featureList: ArrayList<Feature>) {

        style.removeStyleLayer(Constants.SCHOOL_ZONE_LAYER)
        style.removeStyleSource(Constants.SCHOOL_ZONE_SOURCE)

        var icon_size_exp = Expression.interpolate {
            linear()
            zoom()
            stop {
                literal(0.0)
                literal(0.0)
            }
            stop {
                literal(9.0)
                literal(0.0)
            }
            stop {
                literal(12.9)
                literal(0.0)
            }
            stop {
                literal(13.0)
                literal(0.8)
            }
        }


        var mFeatureCollection = FeatureCollection.fromFeatures(featureList)
        style.addSource(
            geoJsonSource(Constants.SCHOOL_ZONE_SOURCE) {
                featureCollection(mFeatureCollection!!)
                //maxzoom(14)
            }
        )

        style.addLayer(
            symbolLayer(
                Constants.SCHOOL_ZONE_LAYER,
                Constants.SCHOOL_ZONE_SOURCE
            ) {
                iconImage(
                    Constants.SCHOOL_ICON
                )
                iconSize(icon_size_exp)
                iconAnchor(IconAnchor.CENTER)
                iconAllowOverlap(false)
            }
        )
    }

    @Deprecated("No longer in use", ReplaceWith("BreezeRoadObjectObserver"))
    fun removeEhorizonLayers(style: Style) {

        style.removeStyleLayer(Constants.SCHOOL_ZONE_LAYER)
        style.removeStyleLayer(Constants.SILVER_ZONE_LAYER)

        style.removeStyleSource(Constants.SCHOOL_ZONE_SOURCE)
        style.removeStyleSource(Constants.SILVER_ZONE_SOURCE)

    }

    fun removeDynamicLayers(style: Style) {

        TrafficIncidentData.values().forEach {
            val layerId = it.type + Constants.LAYER
            val sourceId = it.type + Constants.SOURCE
            style.removeStyleLayer(layerId)
            style.removeStyleSource(sourceId)
        }

        style.removeStyleLayer(Constants.ERP_LAYER)
        style.removeStyleSource(Constants.ERP_GEOJSONSOURCE)


    }

    fun displayCarkParkData(
        carParkFeatureDataList: ArrayList<CarParkLayerModel>,
        style: Style,
        carParkDataList: List<BaseAmenity>,
        carContext: CarContext,
        isShowIndex: Boolean = true
    ) {

        removeCarkParkData(style, carParkFeatureDataList);
        var icon_size_exp = Expression.interpolate {
            linear()
            zoom()
            stop {
                literal(0.0)
                literal(0.0)
            }
            stop {
                literal(9.0)
                literal(0.0)
            }
            stop {
                literal(12.9)
                literal(0.0)
            }
            stop {
                literal(13.0)
                literal(0.8)
            }
            stop {
                literal(16.5)
                literal(1.0)
            }
        }


        val featureGreenPriceList = ArrayList<Feature>();
        val featurePriceList = ArrayList<Feature>();
        val featureCarParkIndexList = ArrayList<Feature>();

        carParkDataList.forEachIndexed { index, it ->
            val feature = Feature.fromGeometry(Point.fromLngLat(it.long!!, it.lat!!));
            val featureIndex = Feature.fromGeometry(Point.fromLngLat(it.long!!, it.lat!!));

            if (it.currentHrRate?.oneHrRate != null) {
                var rate = it.currentHrRate!!.oneHrRate!!.visibleAmount()
                if (it.currentHrRate!!.oneHrRate == 0.0) {
                    rate = "Free"
                }
                //rate  = "(${index+1}) $rate"
                feature.addStringProperty(CARPARK_PRICE, rate)

                featureIndex.addStringProperty(CARPARK_INDEX, "${index + 1}")
                featureIndex.addBooleanProperty(CARPARK_IS_DESTINATION, it.destinationCarPark)
                featureCarParkIndexList.add(featureIndex)

                if (it.isCheapest == true) {
                    if (it.currentHrRate!!.oneHrRate != -2.0) {
                        featureGreenPriceList.add(feature);
                    }
                } else {
                    if (it.currentHrRate!!.oneHrRate != -2.0) {
                        featurePriceList.add(feature)
                    }
                }
            }


            val DESTINATIONTYPE = if (it.destinationCarPark == true) {
                ParkingMarkerConstants.MARKER_DESTINATION
            } else {
                ParkingMarkerConstants.MARKER_NONDESTINATION
            }

            //There's no selected vs non-selected icons for Android auto
            val SELECTEDTYPE = ParkingMarkerConstants.MARKER_SELECTED

            val MARKERTYPE = ParkingIconUtils.getParkingMarkerTypeString(it)
            addCarParkFeature(
                ParkingMarkerConstants.PARKINGMARKER + "_" + MARKERTYPE + "_" + DESTINATIONTYPE + "_" + SELECTEDTYPE,
                carParkFeatureDataList, index, feature, it
            )
        }
        carParkFeatureDataList.forEach {

            style.addSource(
                geoJsonSource(it.sourceName) {
                    featureCollection(FeatureCollection.fromFeatures(it.features))
                }
            )

            style.addLayer(
                symbolLayer(it.layerName, it.sourceName) {
                    iconImage(
                        it.imageName
                    )
                    iconSize(icon_size_exp)
                    iconAnchor(IconAnchor.CENTER)
                    iconAllowOverlap(true)
                    iconIgnorePlacement(true)
                }
            )
        }



        style.addSource(
            geoJsonSource(Constants.CARPARK_PRICE_GREEN_SOURCE) {
                featureCollection(FeatureCollection.fromFeatures(featureGreenPriceList))
            }
        )


        style.addLayer(
            symbolLayer(Constants.CARPARK_PRICE_GREEN_LAYER, Constants.CARPARK_PRICE_GREEN_SOURCE) {
                textField(Expression.get(CARPARK_PRICE))
                textAllowOverlap(true)
                textIgnorePlacement(true)
                textOffset(listOf(0.0, -2.3))
                textColor(Color.WHITE)
                iconImage(Constants.CARPARK_PRICE_GREEN_IMAGE)
                iconSize(icon_size_exp)
                iconAnchor(IconAnchor.CENTER)
                iconAllowOverlap(true)
                iconIgnorePlacement(true)
                iconTextFit(IconTextFit.BOTH)
                iconTextFitPadding(listOf(1.0, 6.0, 1.0, 5.0))
            }
        )

        //-------------------------------------------------------------------

        style.addSource(
            geoJsonSource(Constants.CARPARK_PRICE_SOURCE) {
                featureCollection(FeatureCollection.fromFeatures(featurePriceList))
            }
        )
        style.addLayer(
            symbolLayer(Constants.CARPARK_PRICE_LAYER, Constants.CARPARK_PRICE_SOURCE) {
                textField(Expression.get(CARPARK_PRICE))
                textAllowOverlap(true)
                textIgnorePlacement(true)
                textOffset(listOf(0.0, -2.3))
                textColor(carContext.getColor(R.color.themed_black))
                iconImage(Constants.CARPARK_PRICE_IMAGE)
                iconSize(icon_size_exp)
                iconAnchor(IconAnchor.CENTER)
                iconAllowOverlap(true)
                iconIgnorePlacement(true)
                iconTextFit(IconTextFit.BOTH)
                iconTextFitPadding(listOf(1.0, 6.0, 1.0, 5.0))
            }
        )

        if(isShowIndex) {
            style.addSource(
                geoJsonSource(Constants.CARPARK_INDEX_SOURCE) {
                    featureCollection(FeatureCollection.fromFeatures(featureCarParkIndexList))
                }
            )
            style.addLayer(
                symbolLayer(Constants.CARPARK_INDEX_LAYER, Constants.CARPARK_INDEX_SOURCE) {
                    textField(Expression.get(CARPARK_INDEX))
                    textAllowOverlap(true)
                    textIgnorePlacement(true)
                    textOffset(switchCase {
                        eq {
                            get {
                                literal(CARPARK_IS_DESTINATION)
                            }
                            literal(true) //default value of above expression
                        }
                        literal(listOf(2.0, -0.4)) //if true
                        literal(listOf(1.0, -0.4)) //else
                    })
                    textColor(carContext.getColor(R.color.themed_black))
                    iconImage(Constants.CARPARK_INDEX_IMAGE)
                    iconSize(icon_size_exp)
                    iconAnchor(IconAnchor.CENTER)
                    iconAllowOverlap(true)
                    iconIgnorePlacement(true)
                    iconTextFit(IconTextFit.BOTH)
                    //iconOffset(listOf(0.0, -2.3))
                    iconTextFitPadding(listOf(1.0, 7.5, 1.0, 5.5)) //Top, Right, Bottom, Left

                }
            )
        }
    }

    private fun addCarParkFeature(
        imageName: String,
        carParkFeatureDataList: ArrayList<CarParkLayerModel>,
        index: Int,
        feature: Feature,
        carPark: BaseAmenity
    ) {
        carParkFeatureDataList.add(
            CarParkLayerModel(
                imageName,
                "CARPARK_SOURCE_" + index,
                "CARPARK_LAYER_" + index,
                arrayListOf(feature),
                carPark
            )
        )
    }

    fun removeCarkParkData(style: Style, carParkFeatureDataList: ArrayList<CarParkLayerModel>) {

        style.removeStyleLayer(Constants.CARPARK_PRICE_LAYER)
        style.removeStyleLayer(Constants.CARPARK_PRICE_GREEN_LAYER)
        style.removeStyleLayer(Constants.CARPARK_INDEX_LAYER)

        style.removeStyleSource(Constants.CARPARK_PRICE_SOURCE)
        style.removeStyleSource(Constants.CARPARK_INDEX_SOURCE)
        style.removeStyleSource(Constants.CARPARK_PRICE_GREEN_SOURCE)

        carParkFeatureDataList.forEach {
            style.removeStyleLayer(it.layerName)
            style.removeStyleSource(it.sourceName)

        }
        carParkFeatureDataList.clear()

    }
}
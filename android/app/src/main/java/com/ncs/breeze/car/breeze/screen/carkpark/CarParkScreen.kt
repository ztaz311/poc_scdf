package com.ncs.breeze.car.breeze.screen.carkpark

import android.location.Location
import android.util.Log
import androidx.activity.OnBackPressedCallback
import androidx.car.app.ScreenManager
import androidx.car.app.model.Action
import androidx.car.app.model.ItemList
import androidx.car.app.model.Template
import androidx.car.app.navigation.model.PlaceListNavigationTemplate
import com.breeze.model.DestinationAddressDetails
import com.breeze.model.SearchLocation
import com.breeze.model.api.response.amenities.BaseAmenity
import com.mapbox.geojson.Point
import com.ncs.breeze.car.breeze.base.BaseScreenCar
import com.ncs.breeze.car.breeze.model.CarParkLayerModel
import com.ncs.breeze.car.breeze.screen.homescreen.HomeCarScreen
import com.ncs.breeze.car.breeze.screen.navigation.NavigationReRouteLoadingScreen
import com.ncs.breeze.car.breeze.screen.obulite.CarOBULiteManager
import com.ncs.breeze.car.breeze.style.CarParkSelectionStyle
import com.ncs.breeze.car.breeze.utils.CarConstants
import com.ncs.breeze.car.breeze.utils.LimitClick
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.Event
import com.ncs.breeze.common.analytics.Screen
import com.ncs.breeze.common.event.ToAppRx.postEvent
import com.ncs.breeze.common.event.ToCarRx
import com.ncs.breeze.common.model.rx.AppToPhoneCarParkEndEvent
import com.ncs.breeze.common.model.rx.AppToRoutePlanningStartEvent
import com.ncs.breeze.common.model.rx.DataForRoutePreview
import com.ncs.breeze.common.model.rx.ToCarEventData
import com.ncs.breeze.common.utils.OBUStripStateManager
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.concurrent.CopyOnWriteArrayList

/**
 * Renders the carparks screen.
 * When the showRouteLine is enabled (true), the parent screen must ensure that the
 * routeline is loaded to the singleton instance of mapboxnavigation
 */
class CarParkScreen(
    private val carParkCarContext: CarParkCarContext,
    val data: ToCarEventData? = null,
    val location: Location? = null,
    val showRouteLine: Boolean = false,
    val originalDestination: DestinationAddressDetails?,
    val isFromNavigation: Boolean = false,
    val fromScreen: String? = null
) : BaseScreenCar(carParkCarContext.carContext) {

    private val carParkRecords by lazy { CopyOnWriteArrayList<CarParkLayerModel>() }
    private val carParkMapper by lazy { CarParkMapper(carParkCarContext) }
    private var currentLocation: Point? = null
    private var isLoadingData = true
    private var carDynamicStyle = CarParkSelectionStyle(carParkCarContext.mainCarContext)
    private var carParkRouteLine = CarParkRouteLine(carParkCarContext.mainCarContext)
    private var carParkLayerRenderer = CarParkLayerRenderer(carParkCarContext, this, location)

    private val backPressedCallback = object : OnBackPressedCallback(
        true // default to enabled
    ) {
        override fun handleOnBackPressed() {
            fromScreen?.let {
                if (it == Screen.ANDROID_AUTO_NAVIGATION) {
                    Analytics.logClickEvent(
                        Event.NEARBY_CARPARKS_BACK,
                        it
                    )
                } else {
                    Analytics.logClickEvent(
                        Event.AA_CARPARKS_BACK,
                        it
                    )
                }
            }

            screenManager.pop()
        }
    }

    override fun onCreateScreen() {
        super.onCreateScreen()
        carContext.onBackPressedDispatcher.addCallback({ lifecycle }, backPressedCallback)
    }


    override fun onGetTemplate(): Template {
        val templateBuilder = PlaceListNavigationTemplate.Builder().setTitle(TITLE)
            .setLoading(isLoadingData)
            .setHeaderAction(Action.BACK)
        if (!isLoadingData) {
            val itemList: ItemList =
                carParkMapper.mapToItemList(carParkRecords, carParkClickListener)
            templateBuilder.setItemList(itemList)
        }
        return templateBuilder.build()
    }

    override fun getScreenName(): String {
        return Screen.AA_PARKING_INFO_SCREEN
    }

    override fun onStartScreen() {
        super.onStartScreen()
        OBUStripStateManager.getInstance()?.disable()
        carParkCarContext.mainCarContext.mapboxCarMap.registerObserver(carDynamicStyle)
        carParkCarContext.mainCarContext.mapboxCarMap.registerObserver(carParkLayerRenderer)
        if (showRouteLine) {
            carParkCarContext.mainCarContext.mapboxCarMap.registerObserver(carParkRouteLine)
        }
    }

    override fun onStopScreen() {
        fromScreen?.let {
            Analytics.logClickEvent(Event.AA_HIDE_CARPARK, it)
        }

        super.onStopScreen()
        Log.d("androidauto", "CarParkScreen onStopScreen")
        carParkCarContext.mainCarContext.mapboxCarMap.unregisterObserver(carDynamicStyle)
        carParkCarContext.mainCarContext.mapboxCarMap.unregisterObserver(carParkLayerRenderer)
        carParkCarContext.mainCarContext.mapboxCarMap.unregisterObserver(carParkRouteLine)
    }

    override fun onDestroyScreen() {
        super.onDestroyScreen()
        if (screenManager.top is HomeCarScreen) {
            postEvent(AppToPhoneCarParkEndEvent())
        }
    }

    fun loadPlaceRecords(carparkDataList: List<CarParkLayerModel>, curLocPoint: Point) {
        carParkRecords.clear()
        carParkRecords.addAll(carparkDataList)
        currentLocation = curLocPoint
        isLoadingData = false
        invalidate()
    }

    private val carParkClickListener = object : CarParkListItemClickListener {
        override fun onItemClick(carParkItem: BaseAmenity) {

            LimitClick.handleSafe {
                fromScreen?.let {
                    Analytics.logClickEvent(
                        Event.AA_CARPARKS_NAVIGATE_HERE,
                        it
                    )
                    Analytics.logClickEvent(
                        Event.NEARBY_CARPARKS_LOCATION_SELECT,
                        it
                    )
                }
                val screenManager = carContext.getCarService(ScreenManager::class.java)
                if (!isFromNavigation) {
                    screenManager.popTo(CarConstants.HOME_MARKER)
                    if (fromScreen == Screen.ANDROID_AUTO_CRUISE) {
                        Analytics.logClickEvent(Event.AA_END, Screen.ANDROID_AUTO_CRUISE)
                        OBUStripStateManager.getInstance()?.closeFromCar()
                        CarOBULiteManager.closePhoneOBUScreen()
                    }
                }
                CoroutineScope(Dispatchers.Main).launch {
                    val screenData = DataForRoutePreview(
                        Point.fromLngLat(
                            carParkItem.long!!,
                            carParkItem.lat!!,
                        ), originalDestination
                    ).apply {
                        destinationSearchLocation = SearchLocation(
                            isCarPark = true,
                            lat = carParkItem.lat.toString(),
                            long = carParkItem.long.toString(),
                            name = carParkItem.name,
                            address1 = carParkItem.name,
                            address2 = carParkItem.name,
                            carparkId = carParkItem.id,
                            showAvailabilityFB = carParkItem.showAvailabilityFB
                        )
                    }
                    if (isFromNavigation) {
                        screenManager.push(
                            NavigationReRouteLoadingScreen(
                                carParkCarContext.mainCarContext,
                                screenData
                            )
                        )
                    } else {
                        ToCarRx.postEventWithCacheLast(
                            AppToRoutePlanningStartEvent(
                                screenData
                            )
                        )
                    }
                }
            }
        }
    }


    companion object {
        private const val TITLE = "Carparks"
    }

}

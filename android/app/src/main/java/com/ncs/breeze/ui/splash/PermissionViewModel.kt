package com.ncs.breeze.ui.splash

import android.app.Application
import com.ncs.breeze.common.remote.ApiHelper
import com.ncs.breeze.ui.base.BaseViewModel
import javax.inject.Inject

class PermissionViewModel @Inject constructor(application: Application, apiHelper: ApiHelper) :
    BaseViewModel(application, apiHelper) {
}
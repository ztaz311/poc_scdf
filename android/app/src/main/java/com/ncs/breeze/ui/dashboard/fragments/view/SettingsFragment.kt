package com.ncs.breeze.ui.dashboard.fragments.view

import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.ncs.breeze.R
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.Event
import com.ncs.breeze.common.analytics.Screen
import com.ncs.breeze.common.extensions.android.getAppPreference
import com.ncs.breeze.common.extensions.android.getRNFragmentNavigationParams
import com.breeze.model.enums.DebugMode
import com.ncs.breeze.common.storage.AppPrefsKey
import com.breeze.model.enums.UserTheme
import com.ncs.breeze.common.utils.BreezeFileLoggingUtil
import com.ncs.breeze.common.utils.BreezeUserPreferenceUtil
import com.breeze.model.constants.Constants
import com.ncs.breeze.BuildConfig
import com.ncs.breeze.databinding.FragmentSettingsBinding
import com.ncs.breeze.ui.base.BaseFragment
import com.ncs.breeze.ui.dashboard.activity.DashboardActivity
import com.ncs.breeze.ui.dashboard.fragments.viewmodel.SettingsViewModel
import com.ncs.breeze.ui.rn.CustomReactFragment
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

class SettingsFragment :
    BaseFragment<FragmentSettingsBinding, SettingsViewModel>(), /*IncidentSettingAdapter.OnItemClickListener,*/
    DashboardActivity.SettingsChangeListener {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val viewModel: SettingsViewModel by viewModels {
        viewModelFactory
    }
    //private var incidentAdapter: IncidentSettingAdapter? = null

    @Inject
    lateinit var userPreferenceUtil: BreezeUserPreferenceUtil

    @Inject
    lateinit var breezeFileLoggingUtil: BreezeFileLoggingUtil

    var settingsClickedCount: Int = 0

    var debugToast: Toast? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initialize()
    }

    private fun initialize() {
        viewBinding.backButton.setOnClickListener {
            onBackPressed()
        }

        viewBinding.rlAccount.setOnClickListener {
            Analytics.logClickEvent(Event.SETTINGS_ACCOUNT_CLICK, getScreenName())
//            (requireActivity() as DashboardActivity).addFragment(ProfileFragment(), null, Constants.TAGS.SETTINGS_TAG)
            (requireActivity() as DashboardActivity).addFragment(
                ProfileFragment(),
                null,
                Constants.TAGS.ACCOUNT_TAG
            )
        }

        viewBinding.rlPreferences.setOnClickListener {
            Analytics.logClickEvent(Event.SETTINGS_PREFERENCES_CLICK, getScreenName())
            openPreferencesScreenRN()
        }

        displayUserThemePreference()
//        rl_map_display.setOnClickListener {
//            Analytics.logClickEvent(Event.SETTINGS_MAP_DISPLAY_CLICK, getScreenName())
//            (requireActivity() as DashboardActivity).addFragment(MapDisplayPreferenceFragment(), null, Constants.TAGS.SETTINGS_TAG)
//        }

        /*displayUserRoutePreference()
        rl_route_pref.setOnClickListener {
            Analytics.logClickEvent(Event.SETTINGS_ROUTE_PREF_CLICK, getScreenName())
            (requireActivity() as DashboardActivity).addFragment(RoutePreferenceFragment(), null, Constants.TAGS.SETTINGS_TAG)
        }*/

        viewBinding.rlPrivacyPolicy.setOnClickListener {
            Analytics.logClickEvent(Event.SETTINGS_PRIVACY_POLICY_CLICK, getScreenName())
            (requireActivity() as DashboardActivity).addFragment(
                PrivacyPolicyFragment(),
                null,
                Constants.TAGS.SETTINGS_TAG
            )
        }

        viewBinding.rlTnc.setOnClickListener {
            Analytics.logClickEvent(Event.SETTINGS_TNC_CLICK, getScreenName())
            (requireActivity() as DashboardActivity).addFragment(
                TermsAndConditionFragment(),
                null,
                Constants.TAGS.SETTINGS_TAG
            )

        }

        viewBinding.rlFaq.setOnClickListener {
            Analytics.logClickEvent(Event.SETTINGS_FAQ_CLICK, getScreenName())
            (requireActivity() as DashboardActivity).addFragment(
                FAQFragment(),
                null,
                Constants.TAGS.SETTINGS_TAG
            )

        }

        viewBinding.rlAbout.setOnClickListener {
            Analytics.logClickEvent(Event.SETTINGS_ABOUT_CLICK, getScreenName())
            (requireActivity() as DashboardActivity).addFragment(
                AboutFragment(),
                null,
                Constants.TAGS.SETTINGS_TAG
            )

        }

        setUpDebugModeUI()
        toggleDebugMode(
            DebugMode.extractMode(
                context?.getAppPreference()?.getInt(AppPrefsKey.DEBUG_MODE,
                    if (BuildConfig.DEBUG) 1 else 0)
            )
        )

        viewBinding.settingsHdr.setOnClickListener {
            val isDebuggingEnabled = DebugMode.isDebugOn(viewModel.debugModeFlow.value)
            if (debugToast != null) {
                debugToast!!.cancel()
            }

            debugToast = Toast.makeText(
                context,
                if (isDebuggingEnabled)
                    "Debug option is already enabled"
                else
                    "Settings has been clicked ${++settingsClickedCount} times",
                Toast.LENGTH_SHORT
            )?.also { it.show() }


            if (settingsClickedCount >= 7 && !isDebuggingEnabled) {
                context?.getAppPreference()
                    ?.saveInt(AppPrefsKey.DEBUG_MODE, DebugMode.USER_ENABLED.value, true)
                toggleDebugMode(DebugMode.USER_ENABLED)
                breezeFileLoggingUtil.enableLog(description = "Debugging Enabled")
            }
        }

        try {
            val pInfo: PackageInfo =
                requireActivity().packageManager.getPackageInfo(requireContext().packageName, 0)
            viewBinding.tvCurrentVersion.text =
                "Version ${pInfo.versionName}(${pInfo.versionCode})"

        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }

        /*//Show Traffic Incidents
        incidentAdapter = IncidentSettingAdapter(requireContext(), ArrayList(userPreferenceUtil.getIncidentSettings(requireContext()).values), this@SettingsFragment)
        val layoutManager = LinearLayoutManager(requireContext())
        layoutManager.orientation = LinearLayoutManager.VERTICAL
        incident_settings.setLayoutManager(layoutManager)
        incident_settings.setAdapter(incidentAdapter)

        chkbox_voice_instructions.isChecked = userPreferenceUtil.retrieveUserNavigationVoicePreference(requireContext())
        chkbox_voice_instructions.setOnCheckedChangeListener(object: CompoundButton.OnCheckedChangeListener {
            override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
                userPreferenceUtil.saveUserNavigationVoicePreference(requireContext(), isChecked)
            }

        })*/
    }

    private fun openPreferencesScreenRN() {
        val navigationParams = requireActivity().getRNFragmentNavigationParams(
            sessionToken = (activity as? DashboardActivity)?.myServiceInterceptor?.getSessionToken(),
            fragmentTag = Constants.RN_CONSTANTS.PREFERENCES_SCREEN
        )
        val initialProperties = Bundle().apply {
            putString(Constants.RN_TO_SCREEN, Constants.RN_CONSTANTS.PREFERENCES_SCREEN)
            putBundle(Constants.RN_CONSTANTS.NAVIGATION_PARAMS, navigationParams)
        }
        val reactNativeFragment = CustomReactFragment.newInstance(
            componentName = Constants.RN_CONSTANTS.COMPONENT_NAME,
            launchOptions = initialProperties,
            cleanOnDestroyed = false
        )
        (requireActivity() as DashboardActivity).addFragment(
            reactNativeFragment,
            null,
            Constants.RN_CONSTANTS.PREFERENCES_SCREEN
        )
    }

    private fun displayUserThemePreference() {
        var userTheme = userPreferenceUtil.getUserThemePreference()
        var themeDisplayText = ""
        if (userTheme == UserTheme.LIGHT.type) {
            themeDisplayText = getString(R.string.theme_light)
        } else if (userTheme == UserTheme.DARK.type) {
            themeDisplayText = getString(R.string.theme_dark)
        } else if (userTheme == UserTheme.SYSTEM_DEFAULT.type) {
            themeDisplayText = getString(R.string.theme_system_default)
        } else {
            themeDisplayText = getString(R.string.theme_system_default)
        }

        viewBinding.tvCurrentTheme.text = themeDisplayText
    }

    /*private fun displayUserRoutePreference() {
        // Show Route Preference
        var routePreference = userPreferenceUtil.getRoutePreference()
        var routePreferenceText = ""
        if (routePreference == RoutePreference.CHEAPEST_ROUTE.type) {
            routePreferenceText = getString(R.string.route_pref_cheapest)
        } else {
            routePreferenceText = getString(R.string.route_pref_fastest)
        }

        tv_current_route_pref.text = routePreferenceText
    }*/
    override fun inflateViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?,
        attachToContainer: Boolean
    ) = FragmentSettingsBinding.inflate(inflater, container, attachToContainer)

    override fun getViewModelReference(): SettingsViewModel {
        return viewModel
    }

    /*override fun onClick(item: TrafficIncidentPreference) {
        userPreferenceUtil.saveIncidentSettings(item)
    }*/

    override fun onResume() {
        super.onResume()
        (activity as DashboardActivity?)!!.setSettingsChangeListener(this)
    }

    override fun onPause() {
        super.onPause()
        (activity as DashboardActivity?)!!.setSettingsChangeListener(null)
    }

    override fun onStop() {
        super.onStop()
        (activity as DashboardActivity?)!!.setSettingsChangeListener(null)
    }

    override fun refreshRoutePreference() {
        //displayUserRoutePreference()
    }

    override fun refreshThemePreference() {
        displayUserThemePreference()
    }

    override fun getScreenName(): String {
        return Screen.SETTINGS
    }

    fun toggleDebugMode(mode: DebugMode) {
        viewModel.updateDebugMode(mode)
    }

    private fun setUpDebugModeUI() {
        lifecycleScope.launch(Dispatchers.Main) {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.debugModeFlow.collectLatest {
                    viewBinding.rlDebug.isVisible = DebugMode.isDebugOn(it)
                }
            }
        }

        viewBinding.rlDebug.setOnClickListener {
            (activity as? DashboardActivity)?.addFragment(
                DebugFragment(),
                null,
                DebugFragment.TAG
            )
        }
    }

    companion object {
        const val TAG = "SettingsFragment"
    }
}
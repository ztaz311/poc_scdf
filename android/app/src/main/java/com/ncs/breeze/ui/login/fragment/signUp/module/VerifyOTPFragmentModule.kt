package com.ncs.breeze.ui.login.fragment.signUp.module

import com.ncs.breeze.ui.login.fragment.signUp.view.VerifyOTPFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class VerifyOTPFragmentModule {
    @ContributesAndroidInjector
    abstract fun contributeVerifyOTPFragment(): VerifyOTPFragment
}
package com.ncs.breeze.ui.login.fragment.signIn.view

import android.content.Context
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.ncs.breeze.R
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.Event
import com.ncs.breeze.common.analytics.Screen
import com.breeze.model.constants.Constants
import com.ncs.breeze.common.utils.InitObjectUtilsController
import com.ncs.breeze.common.utils.Utils
import com.breeze.customization.view.otpview.OTPListener
import com.ncs.breeze.databinding.FragmentVerifyMobileBinding
import com.ncs.breeze.ui.base.BaseFragment
import com.ncs.breeze.ui.login.activity.LoginActivity
import com.ncs.breeze.ui.login.fragment.signIn.viewmodel.VerifyMobileViewModel
import com.ncs.breeze.ui.login.fragment.signUp.view.TermsAndCondnFragment
import com.ncs.breeze.ui.utils.DialogUtilsBreeze
import timber.log.Timber
import javax.inject.Inject

class VerifyMobileFragment : BaseFragment<FragmentVerifyMobileBinding, VerifyMobileViewModel>() {

    companion object {
        const val IS_USER_FROM_LOGIN = "IS_USER_FROM_LOGIN"
        const val IS_USER_NOT_CONFIRMED = "IS_USER_NOT_CONFIRMED"
        const val SCREEN_DISMISS_WAIT_TIME: Long = 1500
        const val OTP_PROGRESS_WAIT: Long = 1000

        const val SCREEN_NAME = "VerifyMobileFragment"

        @JvmStatic
        fun newInstance() =
            VerifyMobileFragment().apply {
                arguments = Bundle().apply {}
            }
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val viewModel: VerifyMobileViewModel by viewModels {
        viewModelFactory
    }

    var isUserFromLogin = false
    var isUserConfirmed = true
    var isOtpExpiredErrorShowing = false

    lateinit var countDownTimer: CountDownTimer
    var isCountDownTimerRunning: Boolean = false
    var phonenumber: String? = null
    var last4digits: String? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (requireArguments().containsKey(Constants.KEYS.KEY_PHONENUMBER)) {
            phonenumber = requireArguments().getString(Constants.KEYS.KEY_PHONENUMBER)
            last4digits = phonenumber!!.takeLast(4)
        }
        if (requireArguments().containsKey(IS_USER_FROM_LOGIN)) {
            isUserFromLogin = requireArguments().getBoolean(IS_USER_FROM_LOGIN)
        }
        if (requireArguments().containsKey(IS_USER_NOT_CONFIRMED)) {
            isUserConfirmed = requireArguments().getBoolean(IS_USER_NOT_CONFIRMED)
        }

        initialize()
    }

    private fun initialize() {
        viewBinding.description.text = resources.getString(R.string.verify_mobile_info, last4digits)
        viewBinding.btnClose.setOnClickListener {
            stopTimer()
            (activity as LoginActivity).hideKeyboard()
            onBackPressed()
        }

        if (isUserFromLogin && !isUserConfirmed) {
            isUserFromLogin = false
            resendOTP()
        }

        startTimer()
        handleObserve()


        viewBinding.otpView.otpListener = object : OTPListener {
            override fun onInteractionListener(otp: String) {

            }

            override fun onOTPComplete(otp: String) {
                Timber.d("OTP entered callback triggered")
                if (isCountDownTimerRunning) {
                    Analytics.logClickEvent(Event.SMS_OTP_INPUT, getScreenName())

                    /**
                     * should call signup or signin depend on next step
                     */
                    if (InitObjectUtilsController.currentObjectFlowLoginSignUp?.nextStepSignInWithPhoneNumber
                        == Constants.AMPLIFY_CONSTANTS.SIGN_IN_WITH_CUSTOM_CHALLENGE
                    ) {
                        confirmSignIn(otp)
                    } else {
                        confirmSignUp(otp)
                    }
                } else {
                    // OTP Expiry scenario
                    // Adding a timer as the OTP view has issues when immediately setting error view
                    viewBinding.progressBar.visibility = View.VISIBLE
                    val handler = Handler(Looper.getMainLooper())
                    val r = Runnable {
                        isOtpExpiredErrorShowing = true
                        viewBinding.progressBar.visibility = View.GONE
                        showOTPErrorView(getString(R.string.verify_mobile_code_expired))
                    }
                    handler.postDelayed(r, OTP_PROGRESS_WAIT)
                }
            }
        }

        viewBinding.otpView.requestFocusOTP()

        val imm: InputMethodManager? =
            requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
        imm?.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)

        viewModel.closeOTPScreen.observe(viewLifecycleOwner, Observer {
            if (it) {
                val handler = Handler(Looper.getMainLooper())
                val r = Runnable {
                    stopTimer()
                    (activity as? LoginActivity)?.hideKeyboard()
                    kotlin.runCatching {
                        onBackPressed()
                    }.onFailure { t ->
                        t.printStackTrace()
                    }
                }
                handler.postDelayed(r, SCREEN_DISMISS_WAIT_TIME)
            }
        })
    }


    /**
     * register some case
     */
    private fun handleObserve() {
        viewModel.errorThrow.observe(viewLifecycleOwner) {
            activity?.let {
                DialogUtilsBreeze.showDialogGetUserFails(it, retryAction = {
                    viewModel.restartVerifyOTP()
                }, cancel = {
                    viewBinding.progressBar.visibility = View.GONE
                    viewBinding.otpView.setOTP("")
                    viewBinding.error.visibility = View.VISIBLE
                    viewBinding.error.text = "please enter OTP and try again"
                })

            }
        }

        viewModel.confirmSignInSuccessful.observe(viewLifecycleOwner) {
            viewModel.confirmSignInSuccessful.removeObservers(viewLifecycleOwner)
            viewBinding.progressBar.visibility = View.GONE
            if (InitObjectUtilsController.currentObjectFlowLoginSignUp?.isUserNameAvailable == true) {
                (activity as? LoginActivity)?.openDashboardActivity()
            } else {
                (activity as? LoginActivity)?.openCreateUserNameScreen(TermsAndCondnFragment.FROM_SIGN_IN)
            }
        }

        viewModel.clearOTP.observe(viewLifecycleOwner) {
            viewModel.resendFailed.removeObservers(viewLifecycleOwner)
            viewBinding.otpView.setOTP("")
            viewBinding.error.visibility = View.VISIBLE
            viewBinding.error.text = "please enter OTP and try again"
        }
    }

    private fun startTimer() {
        val countDownDuration = Constants.OTP_RESEND_DURATION
        countDownTimer = object : CountDownTimer(
            countDownDuration.toLong(),
            1000 /*Tick duration*/
        ) {
            override fun onTick(millisUntilFinished: Long) {
                isCountDownTimerRunning = true
                val displayText =
                    Utils.convertTimeToMinsAndSec((millisUntilFinished / 1000).toInt())
                viewBinding.tvCountdown.text = "in ${displayText}"
            }

            override fun onFinish() {
                isCountDownTimerRunning = false
                viewBinding.tvCountdown.visibility = View.GONE
                setResendClickListener()
                viewBinding.txtResendOtp.setTextColor(resources.getColor(R.color.breeze_primary, null))
            }
        }

        viewBinding.tvCountdown.visibility = View.VISIBLE

        // Removing resend click listener when timer is running
        viewBinding.txtResendOtp.setOnClickListener(null)
        viewBinding.txtResendOtp.setTextColor(resources.getColor(R.color.text_grey_light, null))
        countDownTimer.start()
    }

    private fun stopTimer() {
        if (::countDownTimer.isInitialized) {
            Timber.d("Timer stopped")
            countDownTimer.cancel()
        }
    }

    override fun onDestroyView() {
        if (::countDownTimer.isInitialized) {
            Timber.d("Timer stopped")
            countDownTimer.cancel()
        }
        super.onDestroyView()
    }

    fun setResendClickListener() {
        viewBinding.txtResendOtp.setOnClickListener {
            Analytics.logClickEvent(Event.SMS_RESEND, getScreenName())
            if (isOtpExpiredErrorShowing) {
                isOtpExpiredErrorShowing = false
                viewBinding.error.text = ""
                viewBinding.error.visibility = View.GONE
            }
            resendOTP()
            startTimer()
        }
    }

    private fun resendOTP() {
        viewModel.resetAttempts()
        viewBinding.otpView.setOTP("")
        if (!isUserFromLogin) {
            viewModel.resendSuccessful.observe(viewLifecycleOwner) {
                viewModel.resendSuccessful.removeObservers(viewLifecycleOwner)
            }
            viewModel.resendFailed.observe(viewLifecycleOwner) {
                viewModel.resendFailed.removeObservers(viewLifecycleOwner)
                viewBinding.otpView.setOTP("")
                viewBinding.error.visibility = View.VISIBLE
                viewBinding.error.text = it.errorMessage
            }
            viewModel.resendSignUpOTP(phonenumber!!)
        } else {
            Timber.d("resending otp")
            viewModel.resendSignInOTP(phonenumber!!)
        }
    }

    fun setResult() {
        stopTimer()
    }

    override fun inflateViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?,
        attachToContainer: Boolean
    ) = FragmentVerifyMobileBinding.inflate(inflater, container, attachToContainer)

    override fun getViewModelReference(): VerifyMobileViewModel {
        return viewModel
    }

    override fun getScreenName(): String {
        return Screen.ONBOARDING_SMS_VERIFY
    }


    fun showOTPErrorView(errorMessage: String) {
        viewBinding.otpView.setOTP("")
        viewBinding.otpView.showError()
        viewBinding.error.visibility = View.VISIBLE
        viewBinding.error.text = errorMessage
    }

    fun confirmSignUp(otpText: String?) {
        viewBinding.progressBar.visibility = View.VISIBLE
        viewModel.confirmSignUpFailed.observe(viewLifecycleOwner, Observer {
            viewModel.confirmSignUpFailed.removeObservers(viewLifecycleOwner)
            Timber.d("Confirm sign up failed event triggered: ${it.errorMessage}")
            viewBinding.progressBar.visibility = View.GONE
            showOTPErrorView(it.errorMessage)
        })
        viewModel.confirmSignUp(phonenumber!!, otpText!!)
    }

    fun confirmSignIn(otpText: String?) {
        viewBinding.progressBar.visibility = View.VISIBLE
        viewModel.confirmSignInFailed.observe(viewLifecycleOwner, Observer {
            viewModel.confirmSignInFailed.removeObservers(viewLifecycleOwner)
            viewBinding.progressBar.visibility = View.GONE
            showOTPErrorView(it.errorMessage)
        })
        viewModel.confirmSignIn(otpText!!, true)
    }
}
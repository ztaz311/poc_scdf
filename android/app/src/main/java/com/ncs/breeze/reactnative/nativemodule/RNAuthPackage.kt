package com.ncs.breeze.reactnative.nativemodule


import com.ncs.breeze.common.remote.MyServiceInterceptor
import com.facebook.react.ReactPackage
import com.facebook.react.bridge.NativeModule
import com.facebook.react.bridge.ReactApplicationContext
import com.facebook.react.uimanager.ViewManager

class RNAuthPackage(val myServiceInterceptor: MyServiceInterceptor) : ReactPackage {

    override fun createViewManagers(reactContext: ReactApplicationContext): List<ViewManager<*, *>> {
        return emptyList()
    }

    override fun createNativeModules(reactContext: ReactApplicationContext): List<NativeModule> {
        val modules: MutableList<NativeModule> = ArrayList()
        modules.add(RNAuthModule(reactContext, myServiceInterceptor))
        return modules
    }
}

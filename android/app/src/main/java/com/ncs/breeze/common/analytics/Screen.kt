package com.ncs.breeze.common.analytics

/**
 * @author Aiswarya
 */
object Screen {

    const val OUTSIDE_APP = "outside_app"

    //Oboarding
    const val ONBOARDING_CREATE_ACCOUNT = "onboarding"
    const val ONBOARDING_TNC = "onboarding_tnc"
    const val ONBOARDING_CREATE_ACCOUNT_STEP1 = "onboarding_mobile_signup"
    const val ONBOARDING_CREATE_ACCOUNT_STEP2 = "onboarding_sms_verify"
    const val ONBOARDING_CREATE_ACCOUNT_STEP3 = "onboarding_username"
    const val ONBOARDING_GUEST_SIGNUP = "onboarding_guest_signup"
    const val ONBOARDING_CREATE_ACC_SIGNUP = "[signin_create]"
    const val ONBOARDING_SIGNIN = "onboarding_signin"
    const val ONBOARDING_SMS_VERIFY = "onboarding_sms_verify"
    const val ONBOARDING_TUTORIAL = "onboarding_tutorial"//UNUSED
    const val FORGOT_PASSWORD_STEP1 = "onboarding_signin_forgot_password_step1"//UNUSED
    const val FORGOT_PASSWORD_STEP2 = "onboarding_signin_forgot_password_step2"//UNUSED
    const val FORGOT_PASSWORD_STEP3 = "onboarding_signin_forgot_password_step3"//UNUSED
    const val CREATE_USER_NAME_ACTIVITY = "create_user_name_activity"

    //Home inclusive of 3 tabs
    const val HOME = "[homepage]"
    const val FAVOURITES_ETA = "home"
    const val HOME_MENU = "home"
    const val HOME_SEARCH = "home_search"
    const val HOME_CRUISE_MODE = "cruise"
    const val CRUISE_MODE = "cruise"
    const val FRAGMENT_CRUISE_MODE = "fragment_cruise_mode"
    const val FRAGMENT_ERP_DETAIL = "fragment_erp_detail"
    const val FRAGMENT_TRAFFIC_DETAIL = "fragment_traffic_detail"
    const val HOME_TRAFFIC_CHECKER = "home_traffic_checker"
    const val FRAGMENT_EXPLORE = "custom_map"
    const val FRAGMENT_SAVED_PLACES = "saved_collection"
    const val SAVED_COLLECTION_SHORTCUT = "saved_collection_shortcut"
    const val SAVED_COLLECTION_MY_SAVED_PLACES = "saved_collection_my_saved_places"
    const val SAVED_COLLECTION_CUSTOM = "saved_collection_custom"
    const val SHARED_COLLECTION = "shared_collection"

    //User Response
    const val REPORT_ISSUE = "report_issue"
    const val RATING = "feedback"

    //Favorites
    const val FAVOURITES = "favourites"
    const val FAVOURITES_ADD_NEW = "favourites_add_new"
    const val FAVOURITES_EDIT = "favourites_edit"
    const val FAVOURITES_NEW_FAVOURITE_DETAILS = "favourites_new_favourite_details"

    //Settings
    const val SETTINGS = "settings"
    const val SETTINGS_ACCOUNT = "settings_account"
    const val SETTINGS_ACCOUNT_GUEST = "settings_account_guest"
    const val SETTINGS_ACCOUNT_EDIT_USERNAME = "settings_account_edit_username"
    const val SETTINGS_ACCOUNT_RESET_PASSWORD = "settings_account_reset_password"
    const val SETTINGS_ACCOUNT_EDIT_EMAIL = "settings_account_edit_email"
    const val SETTINGS_MAP_DISPLAY = "settings_map_display"
    const val SETTINGS_ROUTE_PREFERENCE = "settings_route_preference"
    const val SETTINGS_PRIVACY_POLICY = "settings_privacy_policy"
    const val SETTINGS_TERMS = "settings_terms_conditions"
    const val SETTINGS_FAQS = "setting_faqs"
    const val SETTINGS_ABOUT = "settings_about"

    //Search Contact
    const val SEARCH_CONTACT = "search_contact"

    //Route Planning
    const val ROUTE_PLANNING = "route_planning"

    const val ROUTE_PLANNING_ERP = "route_planning_erp"
    const val ROUTE_PLANNING_CAR_PARK = "route_planning_carpark"
    const val ROUTE_PLANNING_CAR_PARK_INFORMATION = "route_planning_carpark_information"
    const val ROUTE_PLANNING_EDIT = "edit_route_planning"
    const val ROUTE_PLANNING_MODE = "[routePlanning]"

    //Travel Log
    const val TRAVEL_LOG = "travel_log"
    const val TRAVEL_LOG_TRIP_SUMMARY = "travel_log_travelsummary"
    const val TRIP_LOG = "[trip_log]"

    //Android Auto Screen
    const val AA_SIGNIN_SCREEN = "androidauto_signin"
    const val AA_NEED_PERMISSION_SCREEN = "androidauto_need_permission"
    const val AA_HOMEPAGE_SCREEN = "androidauto_homepage"
    const val AA_PARKING_INFO_SCREEN = "androidauto_parking_info"
    const val AA_RECENT_SEARCH_SCREEN = "androidauto_recent_search"
    const val AA_RECENT_SEARCH_LOADING_SCREEN = "androidauto_recent_search_loading"
    const val AA_SHORTCUT_LIST_SCREEN = "androidauto_shortcut_list"
    const val AA_SHORTCUTS_LOADING_SCREEN = "androidauto_shortcuts_loading"
    const val AA_SEARCH_SCREEN = "androidauto_search"
    const val AA_ROUTE_PREVIEW_SCREEN = "androidauto_route_preview"
    const val AA_ROUTE_PLAN_SCREEN_LOADING = "androidauto_route_plan_loading"
    const val AA_ROUTE_PREVIEW_PARKING_SCREEN = "androidauto_route_preview_parking"
    const val AA_ROUTE_PREVIEW_OTHER_ROUTES_SCREEN = "androidauto_route_preview_other_routes"
    const val AA_NAVIGATION_SCREEN = "androidauto_navigation"
    const val AA_CRUISE_MODE = "androidauto_cruise"
    const val AA_OBU_LITE_DISPLAY = "androidauto_obu_lite"
    const val AA_NOTIFY_ARRIVAL_LIST_SCREEN = "androidauto_notify_arrival_list"
    const val AA_NOTIFY_ARRIVAL_OPTIONS_SCREEN = "androidauto_notify_arrival_options"
    const val AA_SETUP_NEW_NOTIFY_ARRIVAL_SCREEN = "androidauto_setup_new_notify_arrival"
    const val AA_SHARE_CONFIRMATION = "androidauto_share_confirmation"
    const val AA_OBU_CONNECTED_SCREEN = "androidauto_obu_connected"
    const val AA_OBU_NOT_CONNECTED_SCREEN = "androidauto_obu_not_connected"
    const val AA_OBU_CONNECTION_ERROR_SCREEN = "androidauto_obu_connection_error"
    const val AA_OBU_CONNECTION_STATUS_SCREEN = "androidauto_obu_connection_status"
    const val AA_OBU_NOT_PAIRED_SCREEN = "androidauto_obu_not_paired"
    const val AA_OBU_LITE_SCREEN = "androidauto_congestion_alert"
    const val AA_CONGESTION_ALERT_SCREEN = "androidauto_congestion_alert"

    //obu
    const val OBU_LITE_SCREEN = "obu_lite"
    const val OBU_DISPLAY_MODE = "[obu_display_mode]"
    const val NAVIGATION = "[navigation]"
    const val WALKING_GUIDE = "[walking_guide]"
    const val OBU_CRUISE = "[cruise]"
    const val OBU_TRIP_LOG = "[triplog]"
    const val OBU_APP = "[app]"
    const val AA_DHU_HOMEPAGE = "dhu_homepage"
    const val AA_DHU_NAVIGATION = "dhu_navigation"
    const val AA_DHU_CRUISE = "dhu_cruise"
    const val OBU_PAIRING_SCREEN = "[obu_pairing]"

    const val DRIVE_TREND = "[drive_trend]"

    const val ANDROID_AUTO_HOMEPAGE = "[androidauto_homepage]"
    const val ANDROID_AUTO_NAVIGATION = "[androidauto_navigation]"
    const val ANDROID_AUTO_CRUISE = "[androidauto_cruise]"
    const val SHARED_COLLECTION_RECIPIENT = "[shared_collection_recipient]"

    fun getScreenNameGlobalNotification(title: String): String {
        return Event.POPUP_GLOBAL_NOTIFICATION + "_" + title
    }
}
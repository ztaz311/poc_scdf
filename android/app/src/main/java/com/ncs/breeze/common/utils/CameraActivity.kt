package com.ncs.breeze.common.utils/*
package com.ncs.breeze.common.utils

import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity


class CameraActivity : AppCompatActivity() {
    */
/**
 * Getters and setters.
 *//*

    // Required for camera operations in order to save the image file on resume.
    var currentPhotoPath: String? = null
    var capturedImageURI: Uri? = null

    public override fun onSaveInstanceState(savedInstanceState: Bundle) {
        if (currentPhotoPath != null) {
            savedInstanceState.putString(
                CAPTURED_PHOTO_PATH_KEY,
                currentPhotoPath
            )
        }
        if (capturedImageURI != null) {
            savedInstanceState.putString(CAPTURED_PHOTO_URI_KEY, capturedImageURI.toString())
        }
        super.onSaveInstanceState(savedInstanceState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        if (savedInstanceState.containsKey(CAPTURED_PHOTO_PATH_KEY)) {
            currentPhotoPath = savedInstanceState.getString(CAPTURED_PHOTO_PATH_KEY)
        }
        if (savedInstanceState.containsKey(CAPTURED_PHOTO_URI_KEY)) {
            capturedImageURI = Uri.parse(
                savedInstanceState.getString(
                    CAPTURED_PHOTO_URI_KEY
                )
            )
        }
        super.onRestoreInstanceState(savedInstanceState)
    }

    companion object {
        // Storage for camera image URI components
        private const val CAPTURED_PHOTO_PATH_KEY = "mCurrentPhotoPath"
        private const val CAPTURED_PHOTO_URI_KEY = "mCapturedImageURI"
    }
}*/

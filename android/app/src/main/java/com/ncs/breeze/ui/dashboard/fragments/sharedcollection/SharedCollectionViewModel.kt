package com.ncs.breeze.ui.dashboard.fragments.sharedcollection

import android.app.Application
import com.breeze.model.SharedCollection
import com.breeze.model.SharedLocation
import com.breeze.model.api.ErrorData
import com.breeze.model.api.response.DeleteAllBookMarkItemResponse
import com.breeze.model.extensions.rn.getArrayOrNull
import com.breeze.model.extensions.rn.getBoolean
import com.breeze.model.extensions.rn.getIntOrNull
import com.breeze.model.extensions.rn.getMapOrNull
import com.breeze.model.extensions.rn.getStringOrNull
import com.facebook.react.bridge.ReadableMap
import com.facebook.react.bridge.ReadableType
import com.ncs.breeze.common.remote.ApiHelper
import com.ncs.breeze.common.remote.ApiObserver
import com.ncs.breeze.ui.base.BaseFragmentViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject

class SharedCollectionViewModel @Inject constructor(
    val apiHelper: ApiHelper,
    application: Application
) : BaseFragmentViewModel(application) {
    var sharedCollection: SharedCollection? = null
    var currentSelectedIndex = -1

    fun deselectingBookmarkTooltip(
        amenitiesID: String,
        onSuccess: (data: DeleteAllBookMarkItemResponse) -> Unit
    ) {
        apiHelper.deselectingBookmarkTooltip(amenitiesID)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object :
                ApiObserver<DeleteAllBookMarkItemResponse>(compositeDisposable) {
                override fun onSuccess(data: DeleteAllBookMarkItemResponse) {
                    onSuccess.invoke(data)
                }

                override fun onError(e: ErrorData) {
                    Timber.i("deselectingBookmarkTooltip failed $e")
                }
            })
    }

    fun parseRNData(rnData: ReadableMap) {
        val locationMapData = rnData.getMapOrNull("data") ?: return
        currentSelectedIndex = locationMapData.getIntOrNull("selectedIndex") ?: -1
        sharedCollection = SharedCollection(
            collectionSnapshotId = locationMapData.getIntOrNull("collectionSnapshotId"),
            name = locationMapData.getStringOrNull("name"),
            code = locationMapData.getStringOrNull("code"),
            pinned = locationMapData.getBoolean("pinned", false),
            description = locationMapData.getStringOrNull("description"),
        ).apply {
            val newBookmarks = arrayListOf<SharedLocation>()
            locationMapData.getArrayOrNull("bookmarks")?.takeIf { it.size() > 0 }?.let { array ->
                for (index in 0 until array.size()) {
                    if (!array.isNull(index) && array.getType(index) == ReadableType.Map) {
                        val data = array.getMap(index)
                        newBookmarks.add(SharedLocation.fromRNData(data))
                    }
                }
                bookmarks = newBookmarks

            }
        }
    }
}
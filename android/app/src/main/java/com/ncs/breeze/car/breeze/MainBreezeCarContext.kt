package com.ncs.breeze.car.breeze

import androidx.car.app.CarContext
import com.mapbox.androidauto.MapboxCarContext
import com.mapbox.maps.extension.androidauto.MapboxCarMap
import com.mapbox.maps.plugin.locationcomponent.LocationProvider
import com.mapbox.navigation.base.formatter.DistanceFormatterOptions
import com.ncs.breeze.car.breeze.storage.BreezeCarSettingsStorage
import com.breeze.model.constants.Constants
import com.ncs.breeze.common.utils.distanceFormater.BreezeMapboxDistanceFormatter
import java.util.Locale


class MainBreezeCarContext internal constructor(
    val carContext: CarContext,
    val mapboxCarMap: MapboxCarMap,
    val mapboxCarContext: MapboxCarContext,
) {

    val carSettingsStorage = BreezeCarSettingsStorage(carContext)
    var defaultLocationProvider: LocationProvider? = null
//    val navigationLocationProvider = NavigationLocationProvider()

    val distanceFormatter: BreezeMapboxDistanceFormatter by lazy {
        BreezeMapboxDistanceFormatter(
            DistanceFormatterOptions.Builder(carContext.applicationContext)
                .locale(Locale(Constants.MAPBOX_DISTANCE_FORMATTER_LOCALE))
                .build()
        )
    }
    var phoneAppLoggedIn = false
}

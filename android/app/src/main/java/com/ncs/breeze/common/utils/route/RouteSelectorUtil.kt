package com.ncs.breeze.common.utils.route

import com.breeze.model.enums.RoutePreference
import com.ncs.breeze.common.model.RouteAdapterTypeObjects
import com.ncs.breeze.common.model.SimpleRouteAdapterObjects

object RouteSelectorUtil {

    fun handleRoutePreference(
        routesMap: HashMap<RoutePreference, List<SimpleRouteAdapterObjects>>,
        allRoutes: ArrayList<RouteAdapterTypeObjects>,
        selectedRoutePreference: String,
    ) {
        when (selectedRoutePreference) {
            RoutePreference.FASTEST_ROUTE.type -> {
                addPreferredRoutesWithPriority(routesMap, allRoutes, RoutePreference.FASTEST_ROUTE)
                addPreferredRoutesWithPriority(
                    routesMap,
                    allRoutes,
                    RoutePreference.FASTEST_SHORTEST_ROUTE
                )
                addPreferredRoutesWithPriority(
                    routesMap,
                    allRoutes,
                    RoutePreference.FASTEST_CHEAPEST_ROUTE
                )
            }
            RoutePreference.CHEAPEST_ROUTE.type -> {
                addPreferredRoutesWithPriority(routesMap, allRoutes, RoutePreference.CHEAPEST_ROUTE)
                addPreferredRoutesWithPriority(
                    routesMap,
                    allRoutes,
                    RoutePreference.FASTEST_CHEAPEST_ROUTE
                )
                addPreferredRoutesWithPriority(
                    routesMap,
                    allRoutes,
                    RoutePreference.SHORTEST_CHEAPEST_ROUTE
                )
            }
            RoutePreference.SHORTEST_ROUTE.type -> {
                addPreferredRoutesWithPriority(routesMap, allRoutes, RoutePreference.SHORTEST_ROUTE)
                addPreferredRoutesWithPriority(
                    routesMap,
                    allRoutes,
                    RoutePreference.FASTEST_SHORTEST_ROUTE
                )
                addPreferredRoutesWithPriority(
                    routesMap,
                    allRoutes,
                    RoutePreference.SHORTEST_CHEAPEST_ROUTE
                )
            }
        }
    }

    private fun addPreferredRoutesWithPriority(
        routesMap: HashMap<RoutePreference, List<SimpleRouteAdapterObjects>>,
        allRoutes: ArrayList<RouteAdapterTypeObjects>,
        pref: RoutePreference
    ) {
        val routes = routesMap.remove(pref)
        routes?.let { routes ->
            routes.forEach {
                allRoutes.add(RouteAdapterTypeObjects(pref, it))
            }
        }
    }


}
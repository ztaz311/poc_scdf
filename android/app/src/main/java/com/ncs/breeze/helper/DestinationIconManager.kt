package com.ncs.breeze.helper

import android.app.Activity
import androidx.annotation.DrawableRes
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.toBitmap
import androidx.core.os.bundleOf
import com.breeze.model.DestinationSearch
import com.breeze.model.api.ErrorData
import com.breeze.model.api.response.DeleteAllBookMarkItemResponse
import com.breeze.model.constants.Constants
import com.breeze.model.extensions.safeDouble
import com.breeze.model.extensions.toBitmap
import com.facebook.react.bridge.Arguments
import com.google.android.gms.maps.model.LatLng
import com.mapbox.geojson.Feature
import com.mapbox.geojson.Point
import com.mapbox.maps.MapView
import com.mapbox.maps.RenderedQueryGeometry
import com.mapbox.maps.RenderedQueryOptions
import com.mapbox.maps.ScreenBox
import com.mapbox.maps.ScreenCoordinate
import com.mapbox.maps.extension.style.expressions.dsl.generated.literal
import com.mapbox.maps.extension.style.layers.addLayerAbove
import com.mapbox.maps.extension.style.layers.generated.SymbolLayer
import com.mapbox.maps.extension.style.layers.generated.symbolLayer
import com.mapbox.maps.extension.style.layers.getLayerAs
import com.mapbox.maps.extension.style.layers.properties.generated.IconAnchor
import com.mapbox.maps.extension.style.layers.properties.generated.Visibility
import com.mapbox.maps.extension.style.sources.addSource
import com.mapbox.maps.extension.style.sources.generated.geoJsonSource
import com.mapbox.maps.plugin.delegates.listeners.OnCameraChangeListener
import com.mapbox.maps.plugin.gestures.OnMapClickListener
import com.mapbox.maps.plugin.gestures.addOnMapClickListener
import com.ncs.breeze.App
import com.ncs.breeze.R
import com.ncs.breeze.common.remote.ApiObserver
import com.ncs.breeze.components.marker.markerview2.DestinationMarkerView
import com.ncs.breeze.reactnative.nativemodule.ShareBusinessLogicEvent
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import timber.log.Timber
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit

/**
 * DestinationIconManager to render destination icon layer on the map
 * Map Clicks on the destination icon overrides other click listeners with lesser priority
 */
open class DestinationIconManager
    (
    val activity: Activity,
    val destinationSourceId: String = Constants.DESTINATION_SOURCE,
    val destinationLayerId: String = Constants.DESTINATION_LAYER,
    val destinationImageId: String = Constants.UNSAVED_DESTINATION_ICON,
    val mapView: MapView,
    isMapClickEnabled: Boolean = false,
) {
    var compositeDisposable: CompositeDisposable = CompositeDisposable()
    var destinationClickHandler: DestinationClickHandler? = null
    var analyticsScreenName = ""

    /**
     * NOTE: Map Clicks on the destination icon overrides other click listeners with lesser priority
     * (order is based on order of addOnMapClickListener invocation on the same mapbox object)
     */
    private val mapClickListener =
        OnMapClickListener { point ->
            return@OnMapClickListener handleMapClick(point)
        }

    private fun handleMapClick(point: Point): Boolean {
        var didRenderToolTip = false
        val latch = CountDownLatch(1)

        val clicked = mapView.getMapboxMap().pixelForCoordinate(point)
        //executing on render thread to avoid deadlock with ui thread
        mapView.getMapboxMap().executeOnRenderThread {
            mapView.getMapboxMap().queryRenderedFeatures(
                RenderedQueryGeometry(
                    ScreenBox(
                        ScreenCoordinate(clicked!!.x - 10, clicked!!.y - 10),
                        ScreenCoordinate(clicked.x + 10, clicked.y + 10)
                    )
                ),
                RenderedQueryOptions(listOf(destinationLayerId), literal(true))
            ) { expected ->
                if (expected.isValue) {
                    if (expected.value!!.size > 0) {
                        didRenderToolTip = (hashMapDestinationInfo != null)
                        CoroutineScope(Dispatchers.Main).launch {
                            showDestinationTooltip()
                        }
                    } else {
                        CoroutineScope(Dispatchers.Main).launch {
                            removeDestinationIconTooltip()
                        }
                    }
                } else {
                    CoroutineScope(Dispatchers.Main).launch {
                        removeDestinationIconTooltip()
                    }
                }
                latch.countDown()
            }
        }

        latch.await(LATCH_TIME, TimeUnit.SECONDS)
        return didRenderToolTip
    }

    private val onCameraChangedListener = OnCameraChangeListener {
        destinationMarkerView?.updateTooltipPosition()
    }

    init {
        mapView.getMapboxMap().addOnCameraChangeListener(onCameraChangedListener)
        if (isMapClickEnabled) {
            mapView.getMapboxMap().addOnMapClickListener(mapClickListener)
        }
    }

    protected var selectedDestinationPoint: Point? = null
    protected var hashMapDestinationInfo: HashMap<String, Any>? = null
    private var destinationMarkerView: DestinationMarkerView? = null
    protected var isDestinationMarkerViewDisplayed: Boolean? = false
    protected var destinationInfo: DestinationSearch? = null

    fun addDestinationIcon(lat: Double, long: Double, res: Int = R.drawable.destination_puck) {
        val iconToAdd = destinationImageId
        res.toBitmap(activity)?.let {
            mapView.getMapboxMap().getStyle()?.addImage(
                iconToAdd,
                it,
                false
            )
        }
        removeDestinationIcon()
        selectedDestinationPoint = Point.fromLngLat(
            long,
            lat
        )

        mapView.getMapboxMap().getStyle()?.addSource(
            geoJsonSource(destinationSourceId) {
                feature(
                    Feature.fromGeometry(
                        selectedDestinationPoint
                    )
                )
            }
        )

        mapView.getMapboxMap().getStyle()?.addLayerAbove(
            symbolLayer(destinationLayerId, destinationSourceId) {
                iconImage(
                    iconToAdd
                )
                iconAllowOverlap(true)
                iconIgnorePlacement(true)
                iconSize(1.0)
                iconAnchor(IconAnchor.BOTTOM)
                visibility(Visibility.VISIBLE)
            }, mapView.getMapboxMap().getStyle()?.styleLayers?.last()?.id
        )
    }

    fun addDestinationIconWithTooltip(
        lat: Double,
        long: Double,
        hashMap: HashMap<String, Any>,
        info: DestinationSearch,
        @DrawableRes res: Int = R.drawable.destination_puck
    ) {
        mapView.getMapboxMap().getStyle()?.let { style ->
            ContextCompat.getDrawable(activity, res)?.toBitmap()?.let {
                style.addImage(destinationImageId, it, false)
            }
        }

        selectedDestinationPoint = Point.fromLngLat(long, lat)

        hashMapDestinationInfo = hashMap
        destinationInfo = info

        removeDestinationIcon()
        mapView.getMapboxMap().getStyle { style ->
            style.addSource(
                geoJsonSource(destinationSourceId) {
                    feature(Feature.fromGeometry(selectedDestinationPoint))
                }
            )

            style.addLayerAbove(
                symbolLayer(destinationLayerId, destinationSourceId) {
                    iconImage(destinationImageId)
                    iconAllowOverlap(true)
                    iconIgnorePlacement(true)
                    iconSize(1.0)
                    iconAnchor(IconAnchor.BOTTOM)
                    visibility(Visibility.VISIBLE)
                }, style.styleLayers.last().id
            )
        }
    }

//    fun addDestinationIconWithTooltipTest(lat: Double, long: Double, hashMap: HashMap<String, Any>, info: DestinationSearch, res: Int=R.drawable.destination_mark) {
//
//        var iconToAdd = destinationImageId
//        res.toBitmap(activity)?.let {
//            style.addImage(
//                iconToAdd,
//                it,
//                false
//            )
//        }
//
//        selectedDestinationPoint = Point.fromLngLat(
//            long,
//            lat
//        )
//
//        hashMapDestinationInfo = hashMap
//        destinationInfo = info
//
//        removeDestinationIcon()
//        style.addSource(
//            geoJsonSource(destinationSourceId) {
//                feature(
//                    Feature.fromGeometry(
//                        selectedDestinationPoint
//                    )
//                )
//            }
//        )
//
//        style.addLayerAbove(
//            symbolLayer(destinationLayerId, destinationSourceId) {
//                iconImage(
//                    iconToAdd
//                )
//                iconAllowOverlap(true)
//                iconIgnorePlacement(true)
//                iconSize(1.0)
//                iconAnchor(IconAnchor.BOTTOM)
//                visibility(Visibility.VISIBLE)
//            }, style.styleLayers.last().id
//        )
//    }

    open fun removeDestinationIcon() {
        mapView.getMapboxMap().getStyle { style ->
            style.removeStyleLayer(destinationLayerId)
            style.removeStyleSource(destinationSourceId)
            removeDestinationIconTooltip()
        }
    }


    open fun showDestinationIcon() {
        mapView.getMapboxMap().getStyle()?.getLayerAs<SymbolLayer>(destinationLayerId)?.let {
            it.iconImage(destinationImageId)
            it.visibility(Visibility.VISIBLE)
        }
    }

    fun hideDestinationIcon() {
        mapView.getMapboxMap().getStyle {
            it.getLayerAs<SymbolLayer>(Constants.DESTINATION_LAYER)
                ?.visibility(Visibility.NONE)
        }
        hideDestinationIconTooltip()
    }

    fun removeDestinationIconTooltip() {
        destinationMarkerView?.let {
            isDestinationMarkerViewDisplayed = false
            mapView.removeView(it.mViewMarker)
        }
        destinationMarkerView = null
    }

    private fun hideDestinationIconTooltip() {
        if (destinationMarkerView != null) {
            isDestinationMarkerViewDisplayed = false
            destinationMarkerView!!.hideMarker()
        }
    }

    fun showDestinationTooltip() {
        removeDestinationIconTooltip()
        createDestinationToolTip()
    }

    private fun createDestinationToolTip() {
        if (hashMapDestinationInfo == null) return
        isDestinationMarkerViewDisplayed = true
        destinationMarkerView = DestinationMarkerView(
            LatLng(
                selectedDestinationPoint!!.latitude(),
                selectedDestinationPoint!!.longitude()
            ),
            mapView.getMapboxMap(),
            activity
        ).also {
            mapView.addView(it.mViewMarker)
        }


        destinationInfo?.let { desInfor ->
            destinationMarkerView?.handleClick(
                hashMapDestinationInfo!!,
                destinationInfo!!,
                bookmarkIconClick = { _ ->
                    sendEventToBookmarkItem()
                },
                removeBookmarkIconClick = { _ ->
                    destinationInfo?.let { destinationInfo ->
                        showDeleteSavedPlaceConfirmation(destinationInfo)
                    }
                })
        }

        destinationMarkerView?.updateTooltipPosition()
        destinationClickHandler?.onToolTipShown()
    }

    private fun showDeleteSavedPlaceConfirmation(destinationInfo: DestinationSearch) {
        AlertDialog.Builder(activity)
            .setMessage(R.string.desc_delete_saved_place_confirmation_2)
            .setNegativeButton(
                R.string.cancel_uppercase
            ) { dialog, _ ->
                dialog.dismiss()
            }
            .setPositiveButton(
                R.string.confirm
            ) { dialog, _ ->
                deselectingBookmarkTooltip(
                    address1 = destinationInfo.address1 ?: "",
                    address2 = destinationInfo.address2 ?: ""
                )
                dialog.dismiss()
            }
            .show()
    }

    private fun deselectingBookmarkTooltip(address1: String, address2: String) {
        App.instance?.apiHelper?.deselectingBookmarkDestinationTooltip(address1, address2)
            ?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe(object :
                ApiObserver<DeleteAllBookMarkItemResponse>(compositeDisposable) {
                override fun onSuccess(data: DeleteAllBookMarkItemResponse) {
                    refreshBookmarkIconTooltip(false)
                }

                override fun onError(e: ErrorData) {
                    Timber.i("AAA ${e}")
                }
            })
    }

    /**
     * send event to save the item
     */
    private fun sendEventToBookmarkItem() {
        App.instance?.shareBusinessLogicPackage?.getRequestsModule()?.flatMap {
            val rnData = Arguments.fromBundle(
                bundleOf(
                    "lat" to (hashMapDestinationInfo?.get("lat")?.toString())?.safeDouble(),
                    "long" to (hashMapDestinationInfo?.get("long")?.toString())?.safeDouble(),
                    "name" to (hashMapDestinationInfo?.get("address1")?.toString() ?: ""),
                    "address1" to (hashMapDestinationInfo?.get("address1")?.toString() ?: ""),
                    "address2" to (hashMapDestinationInfo?.get("address2")?.toString() ?: ""),
                    "fullAddress" to (hashMapDestinationInfo?.get("fullAddress")?.toString() ?: ""),
                    "placeId" to (hashMapDestinationInfo?.get("placeId")?.toString() ?: ""),
                    "amenityType" to "",
                    "amenityId" to "",
                    "is_selected" to false,
                    "isDestination" to true,
                    "showAvailabilityFB" to (hashMapDestinationInfo?.get("showAvailabilityFB") as? Boolean
                        ?: false),
                    "placeId" to (hashMapDestinationInfo?.get("placeId") ?: "").toString(),
                    "userLocationLinkIdRef" to (hashMapDestinationInfo?.get("userLocationLinkIdRef"))?.toString(),
                )
            )
            Timber.i("call RN event [UPDATE_SAVED_LOCATION] $rnData")
            it.callRN(ShareBusinessLogicEvent.UPDATE_SAVED_LOCATION.eventName, rnData)
        }?.subscribeOn(Schedulers.io())?.observeOn(AndroidSchedulers.mainThread())?.subscribe()
    }

    open fun refreshBookmarkIconTooltip(saved: Boolean, bookmarkId: Int = -1) {
        if (destinationMarkerView != null) {
            destinationMarkerView?.updateBookMarkIconState(saved, bookmarkId)
        }
    }

    private companion object {

        // Keeping max wait time to 2 sec to prevent blocking ui thread for too long
        private const val LATCH_TIME = 2L
    }


    fun interface DestinationClickHandler {
        fun onToolTipShown()
    }

}
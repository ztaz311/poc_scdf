package com.ncs.breeze.components.marker.markerview2

import android.app.Activity
import android.content.Context
import android.content.res.Resources
import android.view.LayoutInflater
import android.view.View
import android.widget.RelativeLayout
import android.widget.TextView
import com.google.android.gms.maps.model.LatLng
import com.mapbox.geojson.Point
import com.mapbox.maps.EdgeInsets
import com.mapbox.maps.MapboxMap
import com.ncs.breeze.R
import com.ncs.breeze.car.breeze.utils.LimitClick
import com.breeze.model.api.response.amenities.BaseAmenity
import com.ncs.breeze.common.utils.BreezeMapZoomUtil
import com.breeze.model.constants.Constants
import com.ncs.breeze.common.utils.LocationBreezeManager
import com.ncs.breeze.components.map.profile.ProfileZoneLayer
import com.ncs.breeze.ui.base.BaseActivity

class PoiMarkerView constructor(
    latLng: LatLng,
    mapboxMap: MapboxMap,
    val activity: Activity,
    itemId: String? = null,
    itemType: String? = null,
    private val toolTipZoomRange: Double = Constants.POI_TOOLTIP_ZOOM_RADIUS
) : MarkerView2(mapboxMap = mapboxMap, itemId) {

    private var annotationView: View? = null
    private var walkHereView: RelativeLayout? = null
    private var driveHereView: RelativeLayout? = null
    private var nameView: TextView? = null
    private var addressView: TextView? = null
    private val pixelDensity = Resources.getSystem().displayMetrics.density
    var currentAmenity: BaseAmenity? = null

    init {
        mLatLng = latLng
        val inflater = activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        annotationView = inflater.inflate(R.layout.poi_marker_tooltip, null)
        annotationView?.apply {
            walkHereView = findViewById(R.id.walk_here_button)
            driveHereView = findViewById(R.id.drive_here_button)
            nameView = findViewById(R.id.textview_name)
            addressView = findViewById(R.id.textview_address)
        }

        //show 'Walk Here' option only if user is inside the TBR zone
        if (ProfileZoneLayer.detectSelectedProfileLocationInZone(LocationBreezeManager.getInstance().currentLocation) == ProfileZoneLayer.NO_ZONE) {
            walkHereView!!.visibility = View.GONE
        } else {
            walkHereView!!.visibility = View.VISIBLE
        }
        mViewMarker = annotationView
    }

    fun handleClickMarker(
        item: BaseAmenity,
        shouldRecenter: Boolean,
        walkHereButtonClick: (BaseAmenity, BaseActivity.EventSource) -> Unit,
        driveHereButtonClick: (BaseAmenity, BaseActivity.EventSource) -> Unit,
        amenitiesIconClick: (BaseAmenity, Boolean) -> Unit
    ) {
        currentAmenity = item
        nameView?.text = item.name ?: item.provider
        addressView?.text = item.address

        /**
         * walk here button click
         */
        walkHereView?.setOnClickListener {
            LimitClick.handleSafe {
                resetView()
                walkHereButtonClick.invoke(item, BaseActivity.EventSource.NONE)
            }
        }

        /**
         * drive here button click
         */
        driveHereView?.setOnClickListener {
            LimitClick.handleSafe {
                resetView()
                driveHereButtonClick.invoke(item, BaseActivity.EventSource.NONE)
            }
        }

        amenitiesIconClick.invoke(item, true)
        renderMarkerToolTip(item, shouldRecenter)

    }

    private fun renderMarkerToolTip(item: BaseAmenity, shouldRecenter: Boolean) {

        if (!shouldRecenter) {
            annotationView?.bringToFront()
            updateTooltipPosition()
            return
        }

        BreezeMapZoomUtil.zoomViewToRange(mapboxMap, Point.fromLngLat(item.long!!, item.lat!!),
            toolTipZoomRange, EdgeInsets(
                10.0 * pixelDensity,
                10.0 * pixelDensity,
                200.0 * pixelDensity,
                10.0 * pixelDensity
            ), animationEndCB = {
                annotationView?.bringToFront()
            }
        )
    }

    fun sendEventInfoIsSelected(id: String) {
//        App.instance?.let { app ->
//            val shareBusinessLogicPackage = app.shareBusinessLogicPackage
//            shareBusinessLogicPackage.getRequestsModule().flatMap {
//                val rnData = Arguments.createMap().apply {
//                    putString("parkingID", id)
//                }
//                it.callRN(ShareBusinessLogicEvent.PARKING_OPEN_INFOR_CARPARK.eventName, rnData)
//            }
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe()
//        }
    }

    override fun showMarker() {
        super.showMarker()
        annotationView?.visibility = View.VISIBLE
    }

    override fun hideMarker() {
        super.hideMarker()
        annotationView?.visibility = View.INVISIBLE
    }

    override fun updateTooltipPosition() {
        mLatLng?.let { location ->
            val screenCoordinate = mapboxMap.pixelForCoordinate(
                Point.fromLngLat(
                    location.longitude,
                    location.latitude
                )
            )
            mViewMarker?.let {
                it.measure(
                    View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                    View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
                );
                it.x = screenCoordinate.x.toFloat() - (it.measuredWidth / 2)
                it.y = screenCoordinate.y.toFloat() - it.measuredHeight
            }
        }
    }
}
package com.ncs.breeze.common.storage

import android.content.Context
import android.content.SharedPreferences
import android.location.Location
import android.util.ArraySet
import com.breeze.model.LocalUserData
import com.breeze.model.api.response.EasyBreeziesResponse
import com.breeze.model.api.response.Masterlists
import com.breeze.model.api.response.notification.SeenNotification
import com.breeze.model.constants.Constants
import com.breeze.model.constants.Constants.PARKING_DEFAULT_RESULT_COUNT
import com.breeze.model.enums.CarParkViewOption
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.ncs.breeze.BuildConfig
import com.ncs.breeze.common.extensions.toV2Object
import com.ncs.breeze.common.helper.obu.OBUConnectionHelper
import java.util.*
import com.breeze.model.common.UserPreference as UserPreferenceV2
import com.breeze.model.common.UserStoredData as UserStoredDataV2


class BreezeUserPreference private constructor(mContext: Context) {
    private val sharedPreference: SharedPreferences =
        mContext.getSharedPreferences(SHARE_PREFERENCE_NAME, Context.MODE_PRIVATE)

    fun getString(key: String): String? {
        return sharedPreference.getString(key, "")
    }

    fun saveString(key: String, value: String, commitTransaction: Boolean = false) {
        val editor = sharedPreference.edit()
        editor.putString(key, value)
        if (commitTransaction) {
            editor.commit()
        } else {
            editor.commit()
        }

    }

    fun saveBoolean(key: String, value: Boolean) {
        val editor = sharedPreference.edit()
        editor.putBoolean(key, value)
        editor.commit()
    }

    fun getBoolean(key: String): Boolean {
        return sharedPreference.getBoolean(key, false)
    }

    fun getLastLocation(): Location? {
        val lat: String? = sharedPreference.getString(KEY_GEOLOCATION_LAT, null)
        val lon: String? = sharedPreference.getString(KEY_GEOLOCATION_LONG, null)
        val provider: String? = sharedPreference.getString(KEY_GEOLOCATION_PROVIDER, null)
        if (lat != null && lon != null) {
            val location = Location(provider ?: Constants.DEFAULT_LOCATION_PROVIDER)
            location.latitude = lat.toDouble()
            location.longitude = lon.toDouble()
            return location
        }
        return null
    }

    fun saveLastLocation(value: Location) {
        val editor = sharedPreference.edit()
        editor.putString(KEY_GEOLOCATION_LAT, value.latitude.toString())
        editor.putString(KEY_GEOLOCATION_LONG, value.longitude.toString())
        editor.putString(KEY_GEOLOCATION_PROVIDER, value.provider)
        editor.apply()
    }


    fun saveUserPreference(userPreference: UserPreferenceV2) {
        val gson = Gson()
        val json = gson.toJson(userPreference)
        sharedPreference.edit().putString(Constants.SHARED_PREF_KEY.USER_PREFERENCE_V2, json)
            .commit()
    }

    fun retrieveUserPreference(): UserPreferenceV2? {
        val userPreferenceString =
            sharedPreference.getString(Constants.SHARED_PREF_KEY.USER_PREFERENCE_V2, "")
        return if (userPreferenceString.isNullOrBlank()) {
            tryToGetOldUserPreference()
        } else {
            Gson().fromJson(userPreferenceString, UserPreferenceV2::class.java)
        }
    }

    private fun tryToGetOldUserPreference(): UserPreferenceV2? {
        return sharedPreference.getString(Constants.SHARED_PREF_KEY.USER_PREFERENCE, "")
            ?.let { dataString ->
                kotlin.runCatching {
                    val oldData = Gson().fromJson(dataString, UserPreference::class.java)
                    oldData.toV2Object()
                }.getOrNull()?.also {
                    saveUserPreference(it)
                    sharedPreference.edit().putString(Constants.SHARED_PREF_KEY.USER_PREFERENCE, "")
                        .apply()
                }

            }
    }

    fun saveUserStoredData(data: UserStoredDataV2) {
        val key = "${KEY_STORED_USER_DATA_PREFIX_V2}${data.cognitoID}"
        sharedPreference.edit().putString(key, Gson().toJson(data)).commit()
    }

    private fun migrateCurrentUserDataFromV3(): UserStoredData? {
        getUserId()?.takeIf { it.isNotEmpty() }?.let { cognitoId ->
            val userPreference = retrieveUserPreference()
            val currentUserEmail = when {
                !(userPreference?.userEmail).isNullOrEmpty() -> userPreference!!.userEmail
                !(userPreference?.customEmail).isNullOrEmpty() -> userPreference!!.customEmail
                else -> ""
            }
            val currentUserName = userPreference?.userName ?: ""
            UserStoredData(
                cognitoID = cognitoId,
                email = currentUserEmail,
                userName = currentUserName
            ).let { data ->
                saveUserStoredData(data.toV2Object())
                userPreference?.apply {
                    userEmail = ""
                    customEmail = ""
                    userName = ""
                }?.let {
                    saveUserPreference(it)
                }

                return data
            }
        }
        return null
    }

    fun retrieveUserStoredDataV2(): UserStoredDataV2? {
        return getUserId()
            ?.takeIf { it.isNotEmpty() }
            ?.let { cognitoId ->
                var data: UserStoredDataV2? = null
                kotlin.runCatching {
                    data = Gson().fromJson(
                        sharedPreference.getString("${KEY_STORED_USER_DATA_PREFIX_V2}$cognitoId", ""),
                        UserStoredDataV2::class.java
                    )
                    if (data == null) {
                        // try to get the old data from version < 6.2.0
                        data = retrieveUserStoredData()?.toV2Object()
                        data?.let {
                            saveUserStoredData(it)
                            sharedPreference.edit()
                                .putString("${KEY_STORED_USER_DATA_PREFIX}$cognitoId", "").apply()
                        }
                    }
                }

                return data
            }
    }

    private fun retrieveUserStoredData(): UserStoredData? {
        getUserId()
            ?.takeIf { it.isNotEmpty() }
            ?.let { cognitoId ->
                var data: UserStoredData? = null
                kotlin.runCatching {
                    data = Gson().fromJson(
                        sharedPreference.getString("${KEY_STORED_USER_DATA_PREFIX}$cognitoId", ""),
                        UserStoredData::class.java
                    )
                }
                if (data == null && retrieveUserPreference() != null) {
                    data = migrateCurrentUserDataFromV3()
                }
                return data
            }
        return null
    }

    fun isNotificationSeenFiltered(): Boolean {
        return sharedPreference.getBoolean(
            Constants.SHARED_PREF_KEY.TEMP_NOTIFICATION_FILTER,
            false
        )
    }

    fun setNotificationSeenFiltered() {
        sharedPreference.edit().putBoolean(Constants.SHARED_PREF_KEY.TEMP_NOTIFICATION_FILTER, true)
            ?.apply()
    }

    /**
     * this function for save notification see in local
     */
    suspend fun saveNotification(listNotificationSee: List<SeenNotification>?) {
        listNotificationSee?.let {
            val json = Gson().toJson(uniqueItems(it).toTypedArray())
            sharedPreference.edit()
                .putString(Constants.SHARED_PREF_KEY.USER_NOTIFICATION_SEEN, json)
                .commit()
        }
    }

    suspend fun clearNotificationLocalExpired() {
        saveNotification(
            getLocalNotification().filter { item ->
                System.currentTimeMillis() < ((item.expireTime ?: 0).toLong() * 1000)
            }
        )
    }

    suspend fun getLocalNotification(): List<SeenNotification> {
        try {
            val gson = Gson()
            val json =
                sharedPreference.getString(Constants.SHARED_PREF_KEY.USER_NOTIFICATION_SEEN, "")
            json?.let {
                if (it.length > 100000) {
                    sharedPreference.edit()
                        .putString(Constants.SHARED_PREF_KEY.USER_NOTIFICATION_SEEN, "")
                    setNotificationSeenFiltered()
                } else {
                    val listNotificationItem =
                        gson.fromJson(json, Array<SeenNotification>::class.java)
                    if (isNotificationSeenFiltered()) {
                        return listNotificationItem.toList()
                    } else {
                        val uniqueItems = uniqueItems(listNotificationItem.toList())
                        setNotificationSeenFiltered()
                        return uniqueItems
                    }
                }
            }
        } catch (_: Exception) {
        }
        return arrayListOf()
    }

    fun saveCarParkDistance(carparkDistanceCode: String) {
        sharedPreference.edit()
            .putString(Constants.SHARED_PREF_KEY.KEY_CAR_PARK_DISTANCE, carparkDistanceCode)
            .commit()
    }

    fun saveCarParkCount(carParkCountCode: String) {
        sharedPreference.edit()
            .putString(Constants.SHARED_PREF_KEY.KEY_CAR_PARK_COUNT, carParkCountCode)
            .commit()
    }

    fun getCarParkDistanceCode(): String {
        val codeCarParkDistance =
            sharedPreference.getString(Constants.SHARED_PREF_KEY.KEY_CAR_PARK_DISTANCE, "")
        return codeCarParkDistance ?: ""
    }

    fun getCarParkCountCode(): String {
        val codeCarParkCount =
            sharedPreference.getString(Constants.SHARED_PREF_KEY.KEY_CAR_PARK_COUNT, "")
        return codeCarParkCount ?: ""
    }


    fun getCarParkDistanceSettingValue(masterList: Masterlists?): Double {
        val carparkDistanceSettingCode = getCarParkDistanceCode()
        if (carparkDistanceSettingCode.isEmpty()) return Constants.PARKING_CHECK_RADIUS
        masterList?.carparkDistance?.forEach { item ->
            if (carparkDistanceSettingCode == item.code) {
                val value = item.value ?: "500.0"
                return value.toDouble() / 1000
            }
        }
        return Constants.PARKING_CHECK_RADIUS
    }

    fun getCarParkCountSettingValue(masterList: Masterlists?): Int {
        val carparkCountSettingCode = getCarParkCountCode()
        if (carparkCountSettingCode.isEmpty()) return PARKING_DEFAULT_RESULT_COUNT
        masterList?.carparkCount?.forEach { item ->
            if (carparkCountSettingCode == item.code) {
                val value = item.value ?: "$PARKING_DEFAULT_RESULT_COUNT"
                return value.toInt()
            }
        }
        return PARKING_DEFAULT_RESULT_COUNT
    }


    fun getCarParkAvailabilityValue(
        masterList: Masterlists?,
        carParkAvailabilityCode: String
    ): String {
        if (carParkAvailabilityCode.isEmpty()) return Constants.CAR_PARK_BAND.ALL
        masterList?.carparkAvailability?.forEach { item ->
            if (carParkAvailabilityCode == item.code) {
                val value = item.value ?: return Constants.CAR_PARK_BAND.ALL
                return value
            }
        }
        return Constants.CAR_PARK_BAND.ALL
    }

    fun saveParkingAvailabilityAudio(parkingAvailabilityAudio: String): Boolean {
        return sharedPreference.edit().putString(
            Constants.SHARED_PREF_KEY.KEY_PARKING_AVAILABILITY_AUDIO,
            parkingAvailabilityAudio
        ).commit()
    }

    fun saveParkingAvailabilityDisplay(parkingAvailabilityDisplay: String): Boolean {
        return sharedPreference.edit().putString(
            Constants.SHARED_PREF_KEY.KEY_PARKING_AVAILABILITY_DISPLAY,
            parkingAvailabilityDisplay
        ).commit()
    }

    fun saveEstimatedTravelTimeAudio(estimatedTravelTimeAudio: String): Boolean {
        return sharedPreference.edit().putString(
            Constants.SHARED_PREF_KEY.KEY_ESTIMATED_TRAVEL_TIME_AUDIO,
            estimatedTravelTimeAudio
        ).commit()
    }

    fun saveEstimatedTravelTimeDisplay(estimatedTravelTimeDisplay: String): Boolean {
        return sharedPreference.edit().putString(
            Constants.SHARED_PREF_KEY.KEY_ESTIMATED_TRAVEL_TIME_DISPLAY,
            estimatedTravelTimeDisplay
        ).commit()
    }

    fun getParkingAvailabilityAudio(): Boolean {
        val parkingAvailabilityAudioCode = sharedPreference.getString(
            Constants.SHARED_PREF_KEY.KEY_PARKING_AVAILABILITY_AUDIO,
            "Yes"
        )
        return parkingAvailabilityAudioCode.contentEquals("Yes", true)
    }

    fun getParkingAvailabilityDisplay(): Boolean {
        val parkingAvailabilityAudioCode = sharedPreference.getString(
            Constants.SHARED_PREF_KEY.KEY_PARKING_AVAILABILITY_DISPLAY,
            "Yes"
        )
        return parkingAvailabilityAudioCode.contentEquals("Yes", true)
    }

    fun getEstimatedTravelTimeAudio(): Boolean {
        val parkingAvailabilityAudioCode = sharedPreference.getString(
            Constants.SHARED_PREF_KEY.KEY_ESTIMATED_TRAVEL_TIME_AUDIO,
            "Yes"
        )
        return parkingAvailabilityAudioCode.contentEquals("Yes", true)
    }

    fun getEstimatedTravelTimeDisplay(): Boolean {
        val parkingAvailabilityAudioCode = sharedPreference.getString(
            Constants.SHARED_PREF_KEY.KEY_ESTIMATED_TRAVEL_TIME_DISPLAY,
            "Yes"
        )
        return parkingAvailabilityAudioCode.contentEquals("Yes", true)
    }

    fun getNewUserThresholdDaysSettings(masterList: Masterlists?): Int {
        masterList?.newUserThresholdDays?.forEach { item ->
            val value = item.value ?: "14"
            return value.toInt()
        }
        return 14
    }

    fun showNudge(masterList: Masterlists?): Boolean {
        masterList?.showNudge?.forEach { item ->
            return item.value == "Yes"
        }
        return false
    }

    /**
     * clears all shared preference excluding filters
     */
    fun clearAllSharedPreferences(excludeSet: HashSet<String>) {
        val editor = sharedPreference.edit()
        for (prefKey in sharedPreference.all.keys) {
            if (!excludeSet.contains(prefKey)
                && !prefKey.contains(KEY_STORED_USER_DATA_PREFIX)
                && !prefKey.contains(KEY_STORED_USER_DATA_PREFIX_V2)
            ) {
                editor.remove(prefKey)
            }
        }
        editor.apply()
    }

    fun isInAppGuideTutorialPending(): Boolean {
        return false
    }

    fun setInAppGuideTutorialPending(displayStatus: Boolean) {
        sharedPreference.edit()
            .putBoolean(Constants.SHARED_PREF_KEY.IS_IN_APP_GUIDE_SHOWN, displayStatus)
            .commit()
    }

    fun isStateOfVoiceStructions(): Boolean {
        return sharedPreference.getBoolean(Constants.SHARED_PREF_KEY.KEY_VOICE_STATE, false)
    }

    fun setStateOfVoiceStructions(isMuted: Boolean) {
        sharedPreference.edit().putBoolean(Constants.SHARED_PREF_KEY.KEY_VOICE_STATE, isMuted)
            .commit()
    }

    fun setStateCarpParkOption(viewOption: CarParkViewOption) {
        sharedPreference.edit()
            .putString(Constants.SHARED_PREF_KEY.KEY_STATE_CARPARK_OPTION, viewOption.mode)
            .commit()
    }

    fun getStateCarpParkOption(): CarParkViewOption {
        val savedValue = sharedPreference.getString(
            Constants.SHARED_PREF_KEY.KEY_STATE_CARPARK_OPTION, ""
        )
        return CarParkViewOption.values().find { it.mode == savedValue }
            ?: CarParkViewOption.ALL_NEARBY

    }

    fun setPNudgeShown() {
        val list = getLocaluserDataList()
        if (list.isEmpty()) {
            list.add(LocalUserData(getUserId(), true, (getAppLaunchCount() + 1)))
        } else {
            list.find { getUserId() == it.userId }?.let {
                it.nudgeShown = true
            }
        }

        val gson = Gson()
        val json = gson.toJson(list)
        sharedPreference.edit().putString(Constants.SHARED_PREF_KEY.KEY_LOCAL_USER_DATA, json)
            .commit()
    }

    fun getLocaluserDataList(): ArrayList<LocalUserData> {
        var list: ArrayList<LocalUserData> = ArrayList()
        val gson = Gson()
        val json = sharedPreference.getString(Constants.SHARED_PREF_KEY.KEY_LOCAL_USER_DATA, "")
        if (json != null) {
            list = if (json.isEmpty()) {
                ArrayList()
            } else {
                val type = object : TypeToken<ArrayList<LocalUserData>>() {}.type
                gson.fromJson(json, type)
            }
        }
        return list
    }

    fun saveSchoolZoneDisplay(schoolZoneDisplayAlert: String): Boolean {
        return sharedPreference.edit().putString(
            Constants.SHARED_PREF_KEY.KEY_SCHOOL_ZONE_DISPLAY,
            schoolZoneDisplayAlert
        ).commit()
    }

    fun getSchoolZoneDisplay(): Boolean {
        val schoolZoneDisplayAlertCode = sharedPreference.getString(
            Constants.SHARED_PREF_KEY.KEY_SCHOOL_ZONE_DISPLAY,
            "Yes"
        )
        return schoolZoneDisplayAlertCode.contentEquals("Yes", true)
    }

    fun saveSchoolZoneAudio(schoolZoneVoiceAlert: String): Boolean {
        return sharedPreference.edit().putString(
            Constants.SHARED_PREF_KEY.KEY_SCHOOL_ZONE_AUDIO,
            schoolZoneVoiceAlert
        ).commit()
    }

    fun getSchoolZoneAudio(): Boolean {
        val schoolZoneVoiceAlertCode = sharedPreference.getString(
            Constants.SHARED_PREF_KEY.KEY_SCHOOL_ZONE_AUDIO,
            "Yes"
        )
        return schoolZoneVoiceAlertCode.contentEquals("Yes", true)
    }

    fun saveSilverZoneDisplay(silverZoneDisplayAlert: String): Boolean {
        return sharedPreference.edit().putString(
            Constants.SHARED_PREF_KEY.KEY_SILVER_ZONE_DISPLAY,
            silverZoneDisplayAlert
        ).commit()
    }

    fun getSilverZoneDisplay(): Boolean {
        val silverZoneDisplayAlertCode = sharedPreference.getString(
            Constants.SHARED_PREF_KEY.KEY_SILVER_ZONE_DISPLAY,
            "Yes"
        )
        return silverZoneDisplayAlertCode.contentEquals("Yes", true)
    }

    fun saveSilverZoneAudio(silverZoneAudioAlert: String): Boolean {
        return sharedPreference.edit().putString(
            Constants.SHARED_PREF_KEY.KEY_SILVER_ZONE_AUDIO,
            silverZoneAudioAlert
        ).commit()
    }

    fun getSilverZoneAudio(): Boolean {
        val silverZoneAudioAlertCode = sharedPreference.getString(
            Constants.SHARED_PREF_KEY.KEY_SILVER_ZONE_AUDIO,
            "Yes"
        )
        return silverZoneAudioAlertCode.contentEquals("Yes", true)
    }

    fun saveBusLaneDisplay(busLaneDisplayAlert: String): Boolean {
        return sharedPreference.edit().putString(
            Constants.SHARED_PREF_KEY.KEY_BUS_LANE_DISPLAY,
            busLaneDisplayAlert
        ).commit()
    }

    fun getBusLaneDisplay(): Boolean {
        val busLaneDisplayAlertCode = sharedPreference.getString(
            Constants.SHARED_PREF_KEY.KEY_BUS_LANE_DISPLAY,
            "Yes"
        )
        return busLaneDisplayAlertCode.contentEquals("Yes", true)
    }

    fun saveBusLaneAudio(busLaneAudioAlert: String): Boolean {
        return sharedPreference.edit().putString(
            Constants.SHARED_PREF_KEY.KEY_BUS_LANE_AUDIO,
            busLaneAudioAlert
        ).commit()
    }

    fun getBusLaneAudio(): Boolean {
        val busLaneAudioAlertCode = sharedPreference.getString(
            Constants.SHARED_PREF_KEY.KEY_BUS_LANE_AUDIO,
            "Yes"
        )
        return busLaneAudioAlertCode.contentEquals("Yes", true)
    }

    fun isPNudgeShown(): Boolean {
        val list = getLocaluserDataList()
        for (item in list) {
            if (item.userId.equals(getUserId()) && item.nudgeShown == true) {
                return true
            }
        }
        return false
    }

    fun setAppLaunchCount() {
        val list = getLocaluserDataList()
        if (list.isEmpty()) {
            list.add(LocalUserData(getUserId(), false, (getAppLaunchCount() + 1)))
        } else {
            val item = list.find {
                it.userId.equals(getUserId())
            }
            if (item != null) {
                item.appLaunchCount = item.appLaunchCount?.plus(1)
            } else {
                list.add(LocalUserData(getUserId(), false, (getAppLaunchCount() + 1)))
            }
        }

        val gson = Gson()
        val json = gson.toJson(list)
        sharedPreference.edit().putString(Constants.SHARED_PREF_KEY.KEY_LOCAL_USER_DATA, json)
            .commit()
    }

    fun getAppLaunchCount(): Int {
        val list = getLocaluserDataList()
        for (item in list) {
            if (item.userId.equals(getUserId())) {
                return item.appLaunchCount ?: 0
            }
        }
        return 0
    }

    fun setCarAnimationShown() {
        sharedPreference.edit().putBoolean(Constants.SHARED_PREF_KEY.KEY_CAR_ANIMATION_SHOWN, true)
            .commit()
    }

    fun isCarAnimationShown(): Boolean {
        return sharedPreference.getBoolean(Constants.SHARED_PREF_KEY.KEY_CAR_ANIMATION_SHOWN, false)
    }

    fun saveTncStatus(status: Boolean) {
        val editor = sharedPreference.edit()
        editor.putBoolean(Constants.SHARED_PREF_KEY.TNC_STATUS, status)
        editor.commit()
    }

    fun getTncStatus(): Boolean {
        return sharedPreference.getBoolean(Constants.SHARED_PREF_KEY.TNC_STATUS, false)
    }

    fun clearTncStatus() {
        sharedPreference.edit().remove(Constants.SHARED_PREF_KEY.TNC_STATUS)?.commit()
    }

    fun addSeenContentId(contentId: String) {
        val seenContentIds = getSeenContentIds()
        if (!seenContentIds.contains(contentId)) {
            seenContentIds.add(contentId)
            sharedPreference
                .edit()
                .putStringSet(Constants.SHARED_PREF_KEY.KEY_SEEN_CONTENT_IDS, seenContentIds)
                .commit()
        }
    }

    fun isSeenContentId(contentId: String): Boolean {
        val seenContentIds = getSeenContentIds()
        return seenContentIds.contains(contentId)
    }

    fun getSeenContentIds(): MutableSet<String> {
        return sharedPreference.getStringSet(
            Constants.SHARED_PREF_KEY.KEY_SEEN_CONTENT_IDS,
            ArraySet()
        ) ?: ArraySet()
    }

    fun saveUserId(userId: String) {
        sharedPreference.edit().putString(Constants.SHARED_PREF_KEY.KEY_USER_ID, userId).commit()
    }

    fun getUserId(): String? {
        return sharedPreference.getString(Constants.SHARED_PREF_KEY.KEY_USER_ID, null)
    }

    fun isEnableOBUSimulation() = sharedPreference.getBoolean(KEY_OBU_SIMULATION, BuildConfig.DEBUG)
    fun toggleOBUSimulation(isEnableSimulation: Boolean) {
        OBUConnectionHelper.isOBUSimulationEnabled = isEnableSimulation
        sharedPreference.edit().putBoolean(
            KEY_OBU_SIMULATION, isEnableSimulation
        ).commit()
    }

    fun isEnableAutoDriveSimulation() = sharedPreference.getBoolean(KEY_AUTO_DRIVE_SIMULATION, BuildConfig.DEBUG)
    fun toggleAutoDriveSimulation(isEnableSimulation: Boolean) {
        sharedPreference.edit().putBoolean(
            KEY_AUTO_DRIVE_SIMULATION, isEnableSimulation
        ).commit()
    }

    companion object {

        private const val SHARE_PREFERENCE_NAME = "Breeze_Preference"
        private const val KEY_GEOFENCE_LAT = "KEY_GEOFENCE_LAT"
        private const val KEY_GEOFENCE_LON = "KEY_GEOFENCE_LON"
        private const val KEY_GEOLOCATION_LAT = "KEY_GEOLOCATION_LAT"
        private const val KEY_GEOLOCATION_LONG = "KEY_GEOLOCATION_LONG"
        private const val KEY_GEOLOCATION_PROVIDER = "KEY_GEOLOCATION_PROVIDER"
        private const val KEY_OBU_SIMULATION = "KEY_OBU_SIMULATION"
        private const val KEY_AUTO_DRIVE_SIMULATION = "KEY_AUTO_DRIVE_SIMULATION"

        private const val KEY_STORED_USER_DATA_PREFIX = "STORED_USER_DATA_"
        private const val KEY_STORED_USER_DATA_PREFIX_V2 = "STORED_USER_DATA_V2_"

        private const val KEY_COGNITO_ID = "KEY_COGNITO_ID"

        private var breezeUserPreference: BreezeUserPreference? = null

        val DEFAULT_CLEAR_EXCLUSIONS = hashSetOf(
            Constants.SHARED_PREF_KEY.USER_NOTIFICATION_SEEN,
            Constants.SHARED_PREF_KEY.KEY_SEEN_CONTENT_IDS,
            Constants.SHARED_PREF_KEY.KEY_VOICE_STATE,
            Constants.SHARED_PREF_KEY.KEY_LOCAL_USER_DATA,
            KEY_OBU_SIMULATION,
            KEY_AUTO_DRIVE_SIMULATION
        )

        fun getInstance(context: Context): BreezeUserPreference {
            if (null == breezeUserPreference) {
                breezeUserPreference = BreezeUserPreference(context)
            }
            return breezeUserPreference!!
        }
    }
}

private suspend fun uniqueItems(items: List<SeenNotification>): List<SeenNotification> {
    return items.distinctBy { it.notificationId }
}

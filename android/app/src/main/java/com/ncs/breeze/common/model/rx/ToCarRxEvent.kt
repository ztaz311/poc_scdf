package com.ncs.breeze.common.model.rx

import android.content.Intent
import androidx.core.graphics.drawable.IconCompat
import com.breeze.model.*
import com.breeze.model.annotation.AmenityTypeVal
import com.breeze.model.api.response.eta.RecentETAContact
import com.breeze.model.enums.CarParkViewOption
import com.breeze.model.enums.ETAMode
import com.breeze.model.obu.OBUParkingAvailability
import com.breeze.model.obu.OBUTravelTimeData
import com.mapbox.geojson.Feature
import com.mapbox.geojson.FeatureCollection
import com.mapbox.geojson.Point
import com.ncs.breeze.common.model.RouteAdapterObjects


//ToDo - Ankur - This class should be inside the Shared module
sealed class ToCarRxEvent {

}

class IgnoredEvent() : ToCarRxEvent()

class AppSessionReadyEvent(val data: AppSessionReadyData, val intent: Intent? = null) :
    ToCarRxEvent()

class AppOnPauseEvent(val data: ToCarEventData) : ToCarRxEvent()
class AppOnResumeEvent(val data: ToCarEventData) : ToCarRxEvent()
class AppToHomePageEvent(val data: ToCarEventData) : ToCarRxEvent()
class AppToRecentSearchScreenEvent(val data: CarRecentSearchData) : ToCarRxEvent()
class AppToShortcutListScreenEvent(val data: CarShortcutsListData) : ToCarRxEvent()
class AppToSearchLocationEvent(val data: DataTransferToLocationList) : ToCarRxEvent()
class AppToSearchLocationLatLngGoogleEvent(val data: DataTransferLatLngGoogle) : ToCarRxEvent()

@Deprecated("FavouritesItem is no longer used")
class AppToETAListScreenEvent(val data: DataTransferToETAList) : ToCarRxEvent()

class AppToETAConfirmScreenEvent(val data: DataForETAConfirm) : ToCarRxEvent()
class OpenSearchScreenEvent(val data: DataSearch) : ToCarRxEvent()
object SwitchThemeEvent : ToCarRxEvent()

class AppToRoutePlanningStartEvent(val data: DataForRoutePreview? = null) : ToCarRxEvent()
class CarOpenAmenityScreen(@AmenityTypeVal val type: String, val fromScreen: String? = null) :
    ToCarRxEvent()
class AppToListETAFavoriteStartEvent(val data: DataForETAListFavorite? = null) : ToCarRxEvent()
class AppToEmptyFavoriteStartEvent() : ToCarRxEvent()

data class DataForRoutePreview(
    var selectedDestination: Point,
    var originalDestination: DestinationAddressDetails?,
    var currentRouteObjects: ArrayList<RouteAdapterObjects> = arrayListOf(),
    var destinationSearchLocation: SearchLocation? = null
) : ToCarEventData()

data class DataForETAListFavorite(val etaMode: ETAMode, val listFavorite: List<RecentETAContact>) :
    ToCarEventData()

data class DataForETAConfirm(val etaMode: ETAMode, val itemFavorite: RecentETAContact) :
    ToCarEventData()

class AppToRoutePlanningEndEvent(val data: ToCarEventData? = null) : ToCarRxEvent()
class AppToNavigationStartEvent(val data: CarNavigationStartEventData) : ToCarRxEvent()
class AppToNavigationEndEvent(val data: ToCarEventData? = null) : ToCarRxEvent()
class AppToHomeTurnOnCruiseModeEvent(
    val etaEnabled: Boolean,
    val sharingGPSEnabled: Boolean,
    val isLoggedIn: Boolean = false
) :
    ToCarRxEvent()

class AppToHomeTurnOffCruiseModeEvent(val data: ToCarEventData? = null) : ToCarRxEvent()
data class TriggerCarToastEvent(val data: CarToastEventData) : ToCarRxEvent() {
    companion object {
        fun create(title: String, duration: Int = 1) =
            TriggerCarToastEvent(CarToastEventData(title, duration))
    }
}

data class OBUStateChangedEvent(val isConnected: Boolean) : ToCarRxEvent()
object OBUCardBalanceChangedEvent : ToCarRxEvent()

class NavigationNotificationEventShow(
    val data: DataNotificationCruiseMode,
    val roadObjectId: String,
    val zone: NavigationZone,
    val distanceRaw: Int,
    val location: String?
) : ToCarRxEvent()

class NavigationTBTNotificationEventShow(
    val title: String,
    val content: String,
    val icon: IconCompat?,
) : ToCarRxEvent()

class NavigationCongestionAlertEventShow(val data: Array<OBUTravelTimeData>?) : ToCarRxEvent()

class NavigationParkingAvailabilityAlertEventShow(val data: Array<OBUParkingAvailability>?) :
    ToCarRxEvent()

class NavigationNotificationEventReset(val roadObjectId: String) : ToCarRxEvent()

class TestNotificationEventShow() : ToCarRxEvent()

class AppToCarParkEvent(val typeCarPark: CarParkViewOption = CarParkViewOption.ALL_NEARBY) :
    ToCarRxEvent()

class AppToCarParkEndEvent(val data: ToAppEventData? = null) : ToCarRxEvent()

class CarNoInternetConnection(val isConnected: Boolean)
class CarERPRefresh(val erpFeature: ERPFeatures?) : ToCarRxEvent()
class CarIncidentDataRefresh(val incidentData: HashMap<String, FeatureCollection>) :
    ToCarRxEvent()

class CarUpdateCalendarEvent(val calendarUpdate: Boolean)
class CarNotifyCalendarEventDestinationChange(val destinationDestails: DestinationAddressDetails)
class CarIncidentAppSyncEvent(val incidentDataUpdated: String?)
class CarERPAppSyncEvent(val erpDataUpdated: String?)
class CarSilverZoneAppSyncEvent(val silverZoneDataUpdated: String?)
class CarSchoolZoneAppSyncEvent(val schoolZoneUpdated: String?)
class CarERPDataChange(val dataUpdated: Boolean)
class CarSchoolSilverZoneDataChange(val dataUpdated: Boolean)
class CarETAMessage(val message: String)
class CarETARNDataCallback(val data: HashMap<String, Any>)
class CarRefreshETAFavouritesFragment(val refresh: Boolean)
class CarRefreshEasyBreeziesFavouritesList(val refresh: Boolean)

class AppEventPermissionsGranted() : ToCarRxEvent()
class AppEventLogout() : ToCarRxEvent()

class ETAStartedCar(val recipientName: String = "") : ToCarRxEvent()
class ETAMessageReceivedCar(val etaMessage: String) : ToCarRxEvent()
class UpdateETACar(
    val isETARunning: Boolean,
    val recipientName: String
) : ToCarRxEvent()

class UpdateStateVoiceCar(val isVoiceMuted: Boolean) : ToCarRxEvent()
class CarCruiseTurnByTurnVoiceState(val isEnableVoice: Boolean) : ToCarRxEvent()


//Ehorizon
@Deprecated("No longer in use", ReplaceWith("BreezeRoadObjectObserver"))
class showEhorizonLayers(
    val ehorizonLayerType: EhorizonLayerType,
    val featureList: ArrayList<Feature>
) : ToCarRxEvent()

@Deprecated("No longer in use", ReplaceWith("BreezeRoadObjectObserver"))
class removeEhorizonLayers() : ToCarRxEvent()

package com.ncs.breeze.common.helper.obu

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.provider.Settings
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Lifecycle
import com.instabug.library.Instabug
import com.ncs.breeze.R
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.Event
import com.ncs.breeze.common.analytics.Screen
import com.ncs.breeze.common.extensions.android.getApp
import com.ncs.breeze.ui.base.BaseFragment
import com.ncs.breeze.ui.dashboard.activity.DashboardActivity
import com.ncs.breeze.ui.dashboard.fragments.obuconnect.OBUConnectFragment
import com.ncs.breeze.ui.dashboard.fragments.obulite.OBULiteFragment
import com.ncs.breeze.ui.navigation.TurnByTurnNavigationActivity
import sg.gov.lta.obu.sdk.core.types.OBUError
import timber.log.Timber
import java.lang.ref.WeakReference


class OBUAlertHelper(activity: AppCompatActivity) {
    private val activityRef = WeakReference(activity)

    private var noCardErrorDialog: AlertDialog? = null
    fun showNoCardError() {
        activityRef.get()?.let {
            if (it is TurnByTurnNavigationActivity) return
            if (it is DashboardActivity) {
                val fragment = it.supportFragmentManager.findFragmentById(R.id.frame_container)
                if (fragment is OBULiteFragment) return
            }
            Analytics.logPopupEvent(
                Event.OBU_CARD_ERROR,
                Event.VALUE_POPUP_OPEN,
                Screen.OBU_APP
            )
            if (noCardErrorDialog != null) return

            val title = "Card Error"
            val message = "Check your cashcard and try connecting to OBU again."
            noCardErrorDialog = AlertDialog.Builder(it)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("Got it") { _, _ ->
                    Analytics.logClickEvent(
                        Event.POPUP_OBU_CARD_ERROR_GOT_IT,
                        Screen.OBU_APP
                    )
                }
                .setOnDismissListener {
                    noCardErrorDialog = null
                }
                .show()
        }
    }
    fun showMissingPermissionAlert() {
        val screenName = getCurrentScreenName()
        val message = "Pairing failed Permission is needed to Pair OBU to \"Breeze\"."
        activityRef.get()?.let { activity ->
            AlertDialog.Builder(activity)
                .setMessage(message)
                .setPositiveButton(R.string.close) { _, _ ->
                    if (screenName.isNotEmpty())
                        Analytics.logClickEvent(
                            Event.OBU_PAIRING_POPUP_OBU_PERMISSION_OK,
                            screenName
                        )
                }
                .setNegativeButton(R.string.menu_settings) { _, _ ->
                    activity.startActivity(Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS).apply {
                        data = Uri.fromParts("package", activity.packageName, "")
                    })
                    if (screenName.isNotEmpty())
                        Analytics.logClickEvent(
                            Event.OBU_PAIRING_POPUP_OBU_PERMISSION_SETTINGS,
                            screenName
                        )
                }
                .show()
        }

    }

    fun showBluetoothDisabledAlert() {
        val screenName = getCurrentScreenName()
        val message = "Turn on Bluetooth to allow \"Breeze\" to connect to Accessories."
        activityRef.get()?.let { activity ->
            AlertDialog.Builder(activity)
                .setMessage(message)
                .setPositiveButton(R.string.close) { _, _ ->
                    if (screenName.isNotEmpty())
                        Analytics.logClickEvent(
                            Event.OBU_PAIRING_POPUP_BLUETOOTH_OK,
                            screenName
                        )
                }
                .setNegativeButton(R.string.menu_settings) { _, _ ->
                    activity.startActivity(Intent(Settings.ACTION_BLUETOOTH_SETTINGS))
                    if (screenName.isNotEmpty())
                        Analytics.logClickEvent(
                            Event.OBU_PAIRING_POPUP_BLUETOOTH_SETTINGS,
                            screenName
                        )
                }
                .show()

        }
    }

    fun showInternetDisabledAlert() {
        val screenName = getCurrentScreenName()

        val message = "Turn on Internet to allow \"Breeze\" to connect to Accessories."
        activityRef.get()?.let { activity ->
            AlertDialog.Builder(activity)
                .setMessage(message)
                .setPositiveButton(R.string.obu_connection_ok) { _, _ ->
                    if (screenName.isNotEmpty())
                        Analytics.logClickEvent(
                            Event.OBU_PAIRING_POPUP_OBU_INTERNET_OK,
                            screenName
                        )
                }
                .setNegativeButton(R.string.menu_settings) { _, _ ->
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                        activity.startActivity(Intent(Settings.Panel.ACTION_INTERNET_CONNECTIVITY))
                    } else {
                        activity.startActivity(Intent(Settings.ACTION_DATA_ROAMING_SETTINGS))
                    }
                    if (screenName.isNotEmpty())
                        Analytics.logClickEvent(
                            Event.OBU_PAIRING_POPUP_OBU_INTERNET_SETTINGS,
                            screenName
                        )
                }
                .show()
        }
    }

    fun showSystemErrorDialog() {
        activityRef.get()
            ?.takeIf { it.lifecycle.currentState.isAtLeast(Lifecycle.State.CREATED) }
            ?.let { activity ->
                OBUCustomDialogFragment.Builder(
                    dialogType = OBUCustomDialogFragment.OBUCustomDialogType.TYPE_2.value,
                    title = "Pairing Error",
                    contentDescription = "Something went wrong. Try Pairing again. Report issue if the problem persists.",
                    callBackType = OBUCustomDialogFragment.CallbackType.TypeSystemErrorDialog.value
                ).build().show(activity.supportFragmentManager, "OBUCustomDialogFragment.SystemError")
            }
    }

    fun showNoOBUFoundAlert() {
        val screenName = getCurrentScreenName()
        val message =
            "Make sure you have registered your phone's bluetooth Mac address with LTA's e-portal. Then try pairing again."
        val title = "OBU not found"
        activityRef.get()?.let { activity ->
            AlertDialog.Builder(activity)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(R.string.obu_connection_ok) { _, _ ->
                    if (screenName.isNotEmpty())
                        Analytics.logClickEvent(
                            Event.OBU_PAIRING_POPUP_OBU_NOT_FOUND_OK,
                            screenName
                        )
                }
                .show()
            if (screenName.isNotEmpty())
                Analytics.logPopupEvent(
                    Event.POPUP_OBU_NOT_FOUND,
                    Event.VALUE_POPUP_OPEN,
                    screenName
                )
        }
    }

    fun showPairingError() {
        activityRef.get()
            ?.takeIf { it.lifecycle.currentState.isAtLeast(Lifecycle.State.CREATED) }
            ?.let { activity ->
                OBUCustomDialogFragment.Builder(
                    dialogType = OBUCustomDialogFragment.OBUCustomDialogType.TYPE_2.value,
                    title = "Pairing Error",
                    contentDescription = "Something went wrong. Try Pairing again. Report issue if the problem persists.",
                    callBackType = OBUCustomDialogFragment.CallbackType.TypePairingError.value
                ).build().show(activity.supportFragmentManager, "OBUCustomDialogFragment.PairingError")
            }
    }

    fun showOBUAlreadyPaired() {
        val message =
            "OBU already paired"
        activityRef.get()?.let { activity ->
            AlertDialog.Builder(activity)
                .setMessage(message)
                .setPositiveButton(R.string.obu_connection_ok) { _, _ ->
                    Analytics.logClickEvent(
                        Event.OBU_PAIRING_OBU_ALREADY_PAIRED_OK,
                        Screen.OBU_PAIRING_SCREEN
                    )
                }
                .show()
        }
    }

    fun showVehicleNumberAlreadyPaired() {
        val message =
            "Vehicle number already paired"
        activityRef.get()?.let { activity ->
            AlertDialog.Builder(activity)
                .setMessage(message)
                .setPositiveButton(R.string.obu_connection_ok) { _, _ -> }
                .show()
        }
    }

    fun showOBUConnectionFailFirstTry() {
        activityRef.get()
            ?.takeIf { it.lifecycle.currentState.isAtLeast(Lifecycle.State.CREATED) }
            ?.let { activity ->

                OBUCustomDialogFragment.Builder(
                    dialogType = OBUCustomDialogFragment.OBUCustomDialogType.TYPE_3.value,
                    title = "OBU not detected",
                    contentDescriptionList = arrayListOf(
                        "Vehicle started",
                        "OBU is within 10 metres of your device",
                        "Bluetooth enabled",
                        "Mobile data enabled",
                    ),
                    callBackType = OBUCustomDialogFragment.CallbackType.TypeOBUConnectionFailFirstTry.value
                ).build().show(activity.supportFragmentManager, "OBUCustomDialogFragment.SystemError")
            }
    }

    private fun getCurrentScreenName(): String {
        return activityRef.get()?.let { activity ->
            (activity as? DashboardActivity)?.run {
                supportFragmentManager.findFragmentById(R.id.frame_container)
            }?.let {
                when (it) {
                    is OBUConnectFragment -> {
                        Screen.OBU_PAIRING_SCREEN
                    }

                    is BaseFragment<*, *> -> it.getScreenName()
                    else -> null
                }
            }
        } ?: ""
    }

    fun showOBUConnectionLost() {
        activityRef.get()
            ?.takeIf { it.lifecycle.currentState.isAtLeast(Lifecycle.State.CREATED) }
            ?.let { activity ->

                OBUCustomDialogFragment.Builder(
                    dialogType = OBUCustomDialogFragment.OBUCustomDialogType.TYPE_3.value,
                    title = "OBU Connection Lost",
                    contentDescriptionList = arrayListOf(
                        "Vehicle started",
                        "Bluetooth enabled",
                        "Mobile data enabled",
                        "No other phones are connected to your OBU"
                    ),
                    callBackType = OBUCustomDialogFragment.CallbackType.TypeOBUConnectionLost.value
                ).build().show(activity.supportFragmentManager, "OBUCustomDialogFragment.SystemError")
            }
    }

    private fun checkCurrentOBUFragmentAndClose(activity: Activity?) {
        val currentFragment = activity?.run {
            (this as? DashboardActivity)?.run {
                supportFragmentManager.findFragmentById(R.id.frame_container) as? OBULiteFragment
            }
        }
        currentFragment?.closeOBULiteScreen()
    }

    fun showErrorAlert(obuError: OBUError, isRetry: Boolean = false) {
        Timber.d("OBUAlertHelper showErrorAlert: obuError=$obuError, isRetry=$isRetry")
        var popupName: String = ""
        val currentFragment = activityRef.get()?.run {
            (this as? DashboardActivity)?.run {
                supportFragmentManager.findFragmentById(R.id.frame_container) as? OBULiteFragment
            }
        }
        val screenName = if (currentFragment is BaseFragment<*, *>) {
            currentFragment.getScreenName()
        } else ""

        when (obuError.code) {
            OBUError.Codes.INTERNET_NOT_AVAILABLE -> {
                popupName = Event.POPUP_OBU_PERMISSION_TURN_ON_INTERNET
                showInternetDisabledAlert()
            }

            OBUError.Codes.BLUETOOTH_DISABLED -> {
                popupName = Event.POPUP_OBU_PERMISSION_TURN_ON_BLUETOOTH
                showBluetoothDisabledAlert()
            }

            OBUError.Codes.OBU_DATA_ACCESS_PERMISSION_DENIED -> {
                popupName = Event.POPUP_OBU_CONNECTION_ERROR_OBU
                showMissingPermissionAlert()
            }

            OBUError.Codes.CONNECTION_FAILED -> {
                popupName = Event.POPUP_OBU_CONNECTION_ERROR_OBU
                if (isRetry) {
                    showSystemErrorDialog()
                } else if (currentFragment != null) {
                    showOBUConnectionLost()
                } else
                    showOBUConnectionFailFirstTry()
            }

            OBUError.Codes.OBU_NOT_PAIRED -> {
                popupName = Event.POPUP_OBU_CONNECTION_ERROR_OBU
                showPairingError()
            }

            OBUError.Codes.DEVELOPER_UNAUTHORISED,
            OBUError.Codes.APPLICATION_UNAUTHORISED,
            OBUError.Codes.DEVELOPER_DEACTIVATED,
            OBUError.Codes.SEARCH_ERROR,
            OBUError.Codes.DEVICE_DATA_ACCESS_ERROR,
            OBUError.Codes.SDK_AUTHENTICATION_REQUIRED,
            OBUError.Codes.CODE_SIGNATURE_FAIL,
            OBUError.Codes.NO_PUBLIC_KEY_AVAILABLE,
            OBUError.Codes.BAD_REQUEST,
            OBUError.Codes.INVALID_SDK_ACCOUNT_KEY,
            OBUError.Codes.REQUEST_TIMEOUT,
            OBUError.Codes.SERVER_ERROR,
            OBUError.Codes.MOCK_SEQUENCE_ENDED -> {
                popupName = Event.POPUP_OBU_CONNECTION_ERROR_OBU
                if (currentFragment != null) {
                    showOBUConnectionLost()
                } else {
                    showSystemErrorDialog()
                }
            }
        }
        if (screenName.isNotEmpty())
            Analytics.logPopupEvent(
                popupName,
                Event.VALUE_POPUP_OPEN,
                screenName,
                "Error Code: ${obuError.code}, message: ${obuError.localizedMessage}"
            )
    }

    fun getCallbackFromType(type:OBUCustomDialogFragment.CallbackType):OBUCustomDialogFragment.OBUCustomDialogListener?{
        return when (type) {
            OBUCustomDialogFragment.CallbackType.TypeSystemErrorDialog -> systemErrorCallback
            OBUCustomDialogFragment.CallbackType.TypePairingError -> pairingErrorCallback
            OBUCustomDialogFragment.CallbackType.TypeOBUConnectionFailFirstTry -> obuConnectionFailFirstTryCallback
            OBUCustomDialogFragment.CallbackType.TypeOBUConnectionLost -> obuConnectionLostCallback
            else -> null
        }
    }

    private val systemErrorCallback = object : OBUCustomDialogFragment.OBUCustomDialogListener {
        override fun onActionOneButtonClicked() {
            val screenName = getCurrentScreenName()
            super.onActionOneButtonClicked()
            activity.getApp()?.obuConnectionHelper?.retryConnect()
            if (screenName.isNotEmpty()) {
                Analytics.logClickEvent(
                    Event.POPUP_SYSTEM_ERROR_RETRY,
                    screenName
                )
            }
        }

        override fun onTextButtonClicked() {
            val screenName = getCurrentScreenName()
            Instabug.show()
            checkCurrentOBUFragmentAndClose(activityRef.get())
            if (screenName.isNotEmpty()) {
                Analytics.logClickEvent(
                    Event.POPUP_SYSTEM_ERROR_REPORT_ISSUE,
                    screenName
                )
            }
        }

        override fun onActionTwoButtonClicked() {
            val screenName = getCurrentScreenName()
            checkCurrentOBUFragmentAndClose(activityRef.get())
            if (screenName.isNotEmpty()) {
                Analytics.logClickEvent(
                    Event.POPUP_SYSTEM_ERROR_SKIP,
                    screenName
                )
            }
        }

        override fun onActionThreeButtonClicked() {
            val screenName = getCurrentScreenName()
            checkCurrentOBUFragmentAndClose(activityRef.get())
            if (screenName.isNotEmpty()) {
                Analytics.logClickEvent(
                    Event.POPUP_SYSTEM_ERROR_CLOSE,
                    screenName
                )
            }
        }
    }

    private val pairingErrorCallback = object : OBUCustomDialogFragment.OBUCustomDialogListener {
        override fun onActionOneButtonClicked() {
            val screenName = getCurrentScreenName()
            activity.getApp()?.obuConnectionHelper?.retryConnect()
            if (screenName.isNotEmpty()) {
                Analytics.logClickEvent(
                    Event.POPUP_PAIRING_ERROR_RETRY,
                    screenName
                )
            }
        }

        override fun onTextButtonClicked() {
            val screenName = getCurrentScreenName()
            Instabug.show()
            checkCurrentOBUFragmentAndClose(activityRef.get())
            if (screenName.isNotEmpty()) {
                Analytics.logClickEvent(
                    Event.POPUP_PAIRING_ERROR_REPORT_ISSUE,
                    screenName
                )
            }
        }

        override fun onActionTwoButtonClicked() {
            val screenName = getCurrentScreenName()
            checkCurrentOBUFragmentAndClose(activityRef.get())
            if (screenName.isNotEmpty()) {
                Analytics.logClickEvent(
                    Event.POPUP_PAIRING_ERROR_SKIP,
                    screenName
                )
            }
        }

        override fun onActionThreeButtonClicked() {
            checkCurrentOBUFragmentAndClose(activityRef.get())
            val screenName = getCurrentScreenName()
            if (screenName.isNotEmpty()) {
                Analytics.logClickEvent(
                    Event.POPUP_PAIRING_ERROR_CLOSE,
                    screenName
                )
            }
        }
    }

    private val obuConnectionFailFirstTryCallback=object : OBUCustomDialogFragment.OBUCustomDialogListener {
        override fun onActionOneButtonClicked() {
            val screenName = getCurrentScreenName()
            activity.getApp()?.obuConnectionHelper?.retryConnect()
            if (screenName.isNotEmpty()) {
                Analytics.logClickEvent(
                    Event.POPUP_OBU_NOT_DETECTED_RETRY,
                    screenName
                )
            }
        }

        override fun onTextButtonClicked() {
            val screenName = getCurrentScreenName()
            Instabug.show()
            checkCurrentOBUFragmentAndClose(activityRef.get())
            if (screenName.isNotEmpty()) {
                Analytics.logClickEvent(
                    Event.POPUP_OBU_NOT_DETECTED_REPORT_ISSUE,
                    screenName
                )
            }
        }

        override fun onActionTwoButtonClicked() {
            val screenName = getCurrentScreenName()
            checkCurrentOBUFragmentAndClose(activityRef.get())
            if (screenName.isNotEmpty()) {
                Analytics.logClickEvent(
                    Event.POPUP_OBU_NOT_DETECTED_SKIP,
                    screenName
                )
            }
        }

        override fun onActionThreeButtonClicked() {
            val screenName = getCurrentScreenName()
            checkCurrentOBUFragmentAndClose(activityRef.get())
            if (screenName.isNotEmpty()) {
                Analytics.logClickEvent(
                    Event.POPUP_OBU_NOT_DETECTED_CLOSE,
                    screenName
                )
            }
        }
    }

    private val obuConnectionLostCallback =
        object : OBUCustomDialogFragment.OBUCustomDialogListener {
            override fun onActionOneButtonClicked() {
                val screenName = getCurrentScreenName()
                activity.getApp()?.obuConnectionHelper?.retryConnect()
                if (screenName.isNotEmpty()) {
                    Analytics.logClickEvent(
                        Event.POPUP_OBU_CONNECTION_LOST_RETRY, screenName
                    )
                }
            }

            override fun onTextButtonClicked() {
                val screenName = getCurrentScreenName()
                Instabug.show()
                checkCurrentOBUFragmentAndClose(activityRef.get())
                if (screenName.isNotEmpty()) {
                    Analytics.logClickEvent(
                        Event.POPUP_OBU_CONNECTION_LOST_REPORT_ISSUE,
                        screenName
                    )
                }
            }

            override fun onActionTwoButtonClicked() {
                val screenName = getCurrentScreenName()
                checkCurrentOBUFragmentAndClose(activityRef.get())
                if (screenName.isNotEmpty()) {
                    Analytics.logClickEvent(
                        Event.POPUP_OBU_CONNECTION_LOST_SKIP,
                        screenName
                    )
                }
            }

            override fun onActionThreeButtonClicked() {
                val screenName = getCurrentScreenName()
                checkCurrentOBUFragmentAndClose(activityRef.get())
                if (screenName.isNotEmpty()) {
                    Analytics.logClickEvent(
                        Event.POPUP_OBU_CONNECTION_LOST_CLOSE,
                        screenName
                    )
                }
            }
        }
}
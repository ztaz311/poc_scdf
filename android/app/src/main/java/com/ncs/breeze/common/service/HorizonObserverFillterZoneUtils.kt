package com.ncs.breeze.common.service

import android.content.Context
import android.graphics.drawable.Drawable
import android.graphics.drawable.GradientDrawable
import androidx.core.content.ContextCompat
import com.ncs.breeze.R
import com.breeze.model.DataNotificationCruiseMode
import com.breeze.model.NavigationZone


object HorizonObserverFillterZoneUtils {


    /**
     * this function for filter and create object for set UI notification
     */
    fun filterZone(
        zone: NavigationZone,
        distance: Int
    ): DataNotificationCruiseMode {
        val dataNotificationCruiseMode = DataNotificationCruiseMode().apply {
            zonePriority = zone.priority
            mDistance = if (distance > 0) {
                "$distance m"
            } else {
                "0 m"
            }

            when (zone) {
                NavigationZone.SCHOOL_ZONE -> {
                }
                NavigationZone.SILVER_ZONE -> {
                }
                NavigationZone.ERP_ZONE -> {
                    mIsShowAmount = true
                }
                NavigationZone.ACCIDENT -> {
                }
                NavigationZone.VEHICLE_BREAKDOWN -> {
                }
                NavigationZone.UNATTENDED_VEHICLE -> {
                }
                NavigationZone.ROAD_BLOCK -> {
                }
                NavigationZone.ROAD_WORK -> {
                }
                NavigationZone.OBSTACLE -> {
                }
                NavigationZone.MISCELLANEOUS -> {
                }
                else -> {}
            }
        }

        return dataNotificationCruiseMode

    }


    fun getCruiseNotificationBG(colorRes: Int, context: Context): Drawable {
        val drawable =
            ContextCompat.getDrawable(
                context,
                R.drawable.feature_notification_bg
            ) as GradientDrawable
        drawable.setColor(context.resources.getColor(colorRes, null))
        return drawable
    }


}
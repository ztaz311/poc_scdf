package com.ncs.breeze.common.utils

import android.location.Location
import android.text.TextUtils
import com.mapbox.geojson.Point
import com.mapbox.turf.TurfMeasurement
import com.breeze.model.api.response.ERPResponseData
import com.breeze.model.api.response.checkCorrectDayType
import com.breeze.model.constants.Constants
import java.text.SimpleDateFormat
import java.util.Calendar

object ERPControllerData {


    val ACTIVE = 1
    val INACTIVE = 0
    val NO_SHOWING = -1
    val THIRTY_MINUTES_MILLS: Long = 30 * 60 * 1000

    var listErpNearByLocation = ArrayList<ERPResponseData.Erp>()

    var mapErpZone = HashMap<String, ArrayList<ERPResponseData.Rate>>()
    var erpResponse: ERPResponseData.ERPResponse? = null
        set(value) {
            /**
             * erp rate
             */
            value?.erprates?.forEach {
                mapErpZone[it.zoneid] = it.rates

            }
            field = value
        }

    /**
     * Calculates additional Erp metadata for given erp zoneid and timestamp
     * Steps:
     * 1) Checks if input timestamp is on a holiday or inactive day
     * 2) Check if time of day (of input ts) matches a time slot for erp
     * 3) Looks for the next erp slot
     * 4) returns associated metadata for the two timeslots
     *
     * @return  returns the erpinfo for the given timestamp as ErpDataExcute object
     */
    fun getDataERp(
        zoneID: String,
        checkTime: Long,
        forceShow: Boolean,
        forceCheckNextDayERP: Boolean
    ): ErpDataExcute {
        if (isNotActiveErp(checkTime) && !forceShow) {
            return getNoShowERPInfo()
        } else {
            val currentERPRate = getCurrentERPRateForTime(zoneID, checkTime)
            //No slot available for given time
            if (currentERPRate == null) {
                val slotAfter30Mins =
                    getCurrentERPRateForTime(zoneID, checkTime + THIRTY_MINUTES_MILLS)
                //No slot available for given time + 30 minutes
                if (slotAfter30Mins == null && !forceShow) {
                    return getNoShowERPInfo()
                } else {
                    if (forceShow) {
                        val nextSlot = getNextERPRateForTime(
                            zoneID,
                            checkTime,
                            forceShow,
                            forceCheckNextDayERP
                        )
                        return getERPInfo(currentERPRate, nextSlot, checkTime)
                    } else {
                        return getERPInfo(currentERPRate, slotAfter30Mins, checkTime)
                    }
                }
            } else {
                val nextERPSlot =
                    getNextERPRateForTime(zoneID, checkTime, forceShow, forceCheckNextDayERP)
                if (currentERPRate.chargeamount == 0.0f) {
                    if (nextERPSlot == null && !forceShow) {
                        return getNoShowERPInfo()
                    } else {
                        return getERPInfo(currentERPRate, nextERPSlot, checkTime)
                    }
                } else {
                    return getERPInfo(currentERPRate, nextERPSlot, checkTime)
                }
            }
        }
    }

    private fun getNoShowERPInfo(): ErpDataExcute {
        val erpAfterExecute = ErpDataExcute()
        erpAfterExecute.erpStatus = INACTIVE
        return erpAfterExecute
    }

    private fun getERPInfo(
        currentERPRate: ERPResponseData.Rate?,
        nextERPRate: ERPResponseData.Rate?,
        checkTime: Long,
    ): ErpDataExcute {
        val erpAfterExecute = ErpDataExcute()

        erpAfterExecute.erpStatus = ACTIVE

        val currentCharge: Float
        val nextCharge: Float
        val currentStartTime: String
        val currentEndTime: String
        val nextTimeRange: String
        val isInOperation: Boolean
        val nextTimeTS: Long
        val timeDiff: Long

        if (currentERPRate != null) {
            currentCharge = currentERPRate.chargeamount
            currentStartTime = currentERPRate.starttime
            currentEndTime = currentERPRate.endtime

            if (currentCharge > 0) {
                isInOperation = true
            } else {
                if (nextERPRate != null && nextERPRate.chargeamount > 0) {
                    isInOperation = true
                } else {
                    isInOperation = false
                }
            }
        } else {
            currentCharge = 0.0f
            currentStartTime =
                SimpleDateFormat(BreezeERPRefreshUtil.ERP_TIME_FORMAT).format(checkTime)
            if (nextERPRate != null && nextERPRate.chargeamount > 0) {
                currentEndTime = nextERPRate.starttime
                isInOperation = true
            } else {
                currentEndTime = ""
                isInOperation = false
            }
        }

        if (nextERPRate != null) {
            nextCharge = nextERPRate.chargeamount
            nextTimeRange =
                Utils.formatTimeWith12HFromHH(nextERPRate.starttime) + " - " + Utils.formatTimeWith12HFromHH(
                    nextERPRate.endtime
                )
            nextTimeTS = Utils.convertHHMMToTimeStamp(nextERPRate.starttime)
        } else {
            nextTimeTS = 0L
            nextCharge = 0.0f
            nextTimeRange = ""
        }
        timeDiff = nextTimeTS - checkTime

        erpAfterExecute.currntErpAmount = currentCharge
        erpAfterExecute.currentStartTime = currentStartTime
        erpAfterExecute.currentEndTime = currentEndTime
        erpAfterExecute.isPriceIncrease = nextCharge > currentCharge
        erpAfterExecute.strRangeNextTime = nextTimeRange
        erpAfterExecute.nextErpAmount = nextCharge
        erpAfterExecute.isInOperation = isInOperation
        erpAfterExecute.minimumThresholdSatisfied =
            (timeDiff < THIRTY_MINUTES_MILLS && timeDiff > 0)

        return erpAfterExecute
    }

    private fun getCurrentERPRateForTime(
        zoneID: String,
        currentTimeMillis: Long
    ): ERPResponseData.Rate? {
        return mapErpZone[zoneID]?.find {
            checkInTimeRange(
                it.starttime,
                it.endtime,
                currentTimeMillis
            )
        }?.takeIf {
            it.checkCorrectDayType(
                Calendar.getInstance().apply { timeInMillis = currentTimeMillis })
        }
    }

    private fun getNextERPRateForTime(
        zoneID: String,
        currentTimeMillis: Long,
        forceShow: Boolean,
        forceCheckNextDayERP: Boolean,
    ): ERPResponseData.Rate? {
        val listErpRates = mapErpZone[zoneID]

        listErpRates?.forEachIndexed { index, erp ->
            if (checkInTimeRange(erp.starttime, erp.endtime, currentTimeMillis)) {
                if (index < listErpRates.size - 1) {
                    val nextERPRate = listErpRates[index + 1]
                    if (!nextERPRate.checkCorrectDayType(
                            Calendar.getInstance()
                                .apply { timeInMillis = currentTimeMillis }
                        )
                    ) return null
                    return if (isTimeBeforeMillis(nextERPRate.starttime, currentTimeMillis)) {
                        //This is next day.
                        if (!forceCheckNextDayERP) {
                            null
                        } else {
                            nextERPRate
                        }
                    } else {
                        nextERPRate
                    }
                }

                // todo: need review whether first slot of day is always have correct data, for real data it's correct because no erp operate at mid night
                if (forceShow && forceCheckNextDayERP) {
                    return listErpRates[0]
                } else {
                    return null
                }
            }
        }
        if (forceShow) {
            return listErpRates?.get(0)
        } else {
            return null
        }
    }

    private fun getCurrentAmountERP(erpRateData: ERPResponseData.Rate, currentTime: Long): Float {
        var chargeAmount = 0F

        val cal = Calendar.getInstance()
        cal.timeInMillis = currentTime

        val isWeekDay =
            !(cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY)
        val isSaturday = cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY

        val isPublicHoliday = checkIfDateIsPublicHoliday(
            erpResponse?.publicholiday,
            getCurrentDateInStringFormat(currentTime),
        )

        chargeAmount = if (isPublicHoliday != null) {
            isPublicHoliday.chargeAmount
        } else {
            if ((isWeekDay && erpRateData.daytype == "Weekdays") || (!isWeekDay && isSaturday && erpRateData.daytype == "Saturday")) {
                erpRateData.chargeamount
            } else {
                0F
            }
        }

        return chargeAmount
    }


    /**
     * is grey
     *
     * if current time is sunday || current time is public holiday + in range time of public holiday -> showing grey icon
     */
    private fun isNotActiveErp(currentTime: Long): Boolean {

        val cal = Calendar.getInstance()
        cal.timeInMillis = currentTime

        if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
            return true
        }

        val isPublicHoliday = checkIfDateIsPublicHoliday(
            erpResponse?.publicholiday,
            getCurrentDateInStringFormat(currentTime),
        )

        if (isPublicHoliday != null) {
            return checkInTimeRange(isPublicHoliday.startTime, isPublicHoliday.endTime, currentTime)
        }

        return false
    }


    /**
     * convert current timeinmillis to string with format
     * 2021-12-31 to compare is public holiday
     *
     */
    private fun getCurrentDateInStringFormat(currenttimeMiliss: Long): String {
        val formatter = SimpleDateFormat(
            "yyyy-MM-dd"
        )
        val calendar: Calendar = Utils.CURRENT_CALENDER()
        calendar.timeInMillis = currenttimeMiliss
        return formatter.format(calendar.time)
    }


    /**
     * check if
     *  "date": "2021-12-31", with format like that
     */
    private fun checkIfDateIsPublicHoliday(
        publicHolidaysList: ArrayList<ERPResponseData.PublicholidayItem>?,
        dateStr: String
    ): ERPResponseData.PublicholidayItem? {
        var matchingPublicHoliday: ERPResponseData.PublicholidayItem? = null
        if (!publicHolidaysList.isNullOrEmpty()) {
            matchingPublicHoliday = publicHolidaysList.firstOrNull() {
                TextUtils.equals(it.date, dateStr)
            }
        }
        return matchingPublicHoliday
    }


    /**
     * check current time is in range time of erp
     */
    private fun checkInTimeRange(startTime: String, endTime: String, currentTime: Long): Boolean {
        val currentTimeInt =
            SimpleDateFormat(BreezeERPRefreshUtil.ERP_TIME_FORMAT).format(currentTime)
                .replace(":", "").toInt()
        val startTimeToInt = startTime.replace(":", "").toInt()
        val endTimeInt = endTime.replace(":", "").toInt()
        return currentTimeInt >= startTimeToInt && currentTimeInt < endTimeInt
    }

    private fun isTimeBeforeMillis(checkTime: String, referenceTime: Long): Boolean {
        val referenceTimeInt =
            SimpleDateFormat(BreezeERPRefreshUtil.ERP_TIME_FORMAT).format(referenceTime)
                .replace(":", "").toInt()
        val checkTimeDate = checkTime.replace(":", "").toInt()
        return checkTimeDate < referenceTimeInt
    }

    /**
     * get list erp around 3km
     */
    fun filterERPNearByLocation(location: Location): ArrayList<ERPResponseData.Erp> {
        listErpNearByLocation.clear()
        val radiusERP = Constants.ERP_RADIUS * 1000L //3 kilometer

        val nearByERPS = HashMap<ERPResponseData.Erp, Double>()

        erpResponse?.erps?.forEach {
            val erpCenterPoint = it.centerPoint()
            val dynamicRadius = TurfMeasurement.distance(
                Point.fromLngLat(
                    location.longitude,
                    location.latitude
                ),
                erpCenterPoint
            ) * 1000

            if (dynamicRadius < radiusERP) {
                nearByERPS.put(it, dynamicRadius)
            }
        }

        val sortedMap = nearByERPS.entries.sortedBy { it.value }.associate { it.toPair() }

        var itemCount = 0
        for ((key, value) in sortedMap) {
            listErpNearByLocation.add(key)
            itemCount++
            if (itemCount >= 3) {
                break
            }
        }

        return listErpNearByLocation
    }

    fun ERPResponseData.Erp.startPoint(): Point {
        return Point.fromLngLat(startlong.toDouble(), startlat.toDouble())
    }

    fun ERPResponseData.Erp.endPoint(): Point {
        return Point.fromLngLat(endlong.toDouble(), endlat.toDouble())
    }

    fun ERPResponseData.Erp.centerPoint(): Point {
        return TurfMeasurement.midpoint(
            startPoint(),
            endPoint()
        )
    }


    /**
     * get list erp around 3km
     */
    fun getERPByID(erpID: Int): ERPResponseData.Erp? {
        listErpNearByLocation.clear()
        erpResponse?.erps?.forEach {
            if (it.erpid == erpID) return it
        }
        return null
    }


}

class ErpDataExcute {
    var erpStatus: Int = 0
    var currntErpAmount: Float = 0.0f
    var isPriceIncrease: Boolean = false

    var minimumThresholdSatisfied: Boolean = false

    var isInOperation: Boolean = true

    var nextErpAmount: Float = 0.0f
    var strRangeNextTime: String = ""

    var currentStartTime = ""
    var currentEndTime = ""


}
package com.ncs.breeze.car.breeze.style

import androidx.car.app.ScreenManager
import com.mapbox.geojson.Feature
import com.mapbox.geojson.Point
import com.mapbox.maps.Style
import com.mapbox.maps.extension.androidauto.MapboxCarMapObserver
import com.mapbox.maps.extension.androidauto.MapboxCarMapSurface
import com.mapbox.maps.extension.style.layers.addLayer
import com.mapbox.maps.extension.style.layers.generated.symbolLayer
import com.mapbox.maps.extension.style.layers.properties.generated.IconAnchor
import com.mapbox.maps.extension.style.sources.addSource
import com.mapbox.maps.extension.style.sources.generated.geoJsonSource
import com.ncs.breeze.car.breeze.screen.homescreen.HomeCarScreen
import com.ncs.breeze.car.breeze.screen.navigation.CarNavigationCarContext
import com.ncs.breeze.car.breeze.screen.navigation.NavigationScreen
import com.ncs.breeze.car.breeze.screen.obulite.CarOBULiteScreen
import com.ncs.breeze.car.breeze.utils.BreezeCarUtil
import com.ncs.breeze.car.breeze.utils.BreezeMapBoxUtil
import com.ncs.breeze.common.constant.MapIcon
import com.breeze.model.extensions.toBitmap
import com.breeze.model.DestinationAddressDetails
import com.breeze.model.ERPRouteInfo
import com.breeze.model.constants.Constants
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class CarNavigationStyle(
    private val carNavigationCarContext: CarNavigationCarContext,
    private var currentDestinationDetails: DestinationAddressDetails,
) : MapboxCarMapObserver {

    var currentRouteERPInfo: ERPRouteInfo? = null

    override fun onAttached(mapboxCarMapSurface: MapboxCarMapSurface) {
        mapboxCarMapSurface.mapSurface.getMapboxMap().getStyle {

            MapIcon.NavigationMapIcon.forEach { (key, value) ->
                value.toBitmap(carNavigationCarContext.carContext)
                    ?.let { bitmap ->
                        it.addImage(key, bitmap, false)
                    }
            }
            it.removeStyleLayer(Constants.TRAFFIC_LAYER_ID)
            //addDestinationIcon(it)
            val incidentData = BreezeCarUtil.breezeIncidentsUtil.getLatestIncidentData()
            if (!incidentData.isNullOrEmpty()) {
                BreezeMapBoxUtil.displayIncidentData(it, incidentData);
            }

            if (BreezeCarUtil.breezeERPUtil.getCurrentERPData() != null) {
                CoroutineScope(Dispatchers.Default).launch {
                    currentRouteERPInfo = BreezeCarUtil.breezeERPUtil.getERPInRoute(
                        BreezeCarUtil.breezeERPUtil.getERPData()!!,
                        carNavigationCarContext.route,
                        false
                    )
                    withContext(Dispatchers.Main) {
                        BreezeMapBoxUtil.addNavigationERPLayerToStyle(
                            it,
                            currentRouteERPInfo!!.featureCollection,
                            carNavigationCarContext.carContext
                        );
                    }

                }
            }
        }
    }


    override fun onDetached(mapboxCarMapSurface: MapboxCarMapSurface) {
        mapboxCarMapSurface.mapSurface.getMapboxMap().getStyle()?.let {
            val topScreen = carNavigationCarContext.carContext.getCarService(
                ScreenManager::class.java
            ).top
            if ((topScreen !is HomeCarScreen) &&
                (topScreen !is NavigationScreen) &&
                (topScreen !is CarOBULiteScreen)
            ) {
                BreezeMapBoxUtil.removeDynamicLayers(it);
            }
            removeDestinationIcon(it);
        }
    }

    private fun addDestinationIcon(style: Style) {
        var iconToAdd = Constants.UNSAVED_DESTINATION_ICON
        if (!currentDestinationDetails.destinationName.isNullOrBlank() && currentDestinationDetails.easyBreezyAddressID != -1) {
            iconToAdd = Constants.FAVOURITES_DESTINATION_ICON
        }

        style.removeStyleLayer(Constants.DESTINATION_LAYER)
        style.removeStyleSource(Constants.DESTINATION_SOURCE)

        style.addSource(
            geoJsonSource(Constants.DESTINATION_SOURCE) {
                feature(
                    Feature.fromGeometry(
                        Point.fromLngLat(
                            currentDestinationDetails.long!!.toDouble(),
                            currentDestinationDetails.lat!!.toDouble()
                        )
                    )
                )
                //maxzoom(14)
            }
        )

        style.addLayer(
            symbolLayer(Constants.DESTINATION_LAYER, Constants.DESTINATION_SOURCE) {
                iconImage(
                    iconToAdd
                )
                iconSize(1.0)
                iconAnchor(IconAnchor.BOTTOM)
            }
        )
    }


    private fun removeDestinationIcon(style: Style) {
        style.removeStyleLayer(Constants.DESTINATION_LAYER)
        style.removeStyleSource(Constants.DESTINATION_SOURCE)
    }

}
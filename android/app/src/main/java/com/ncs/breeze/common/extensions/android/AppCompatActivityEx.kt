package com.ncs.breeze.common.extensions.android

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

fun <T> AppCompatActivity.repeatLifecycleCollectLatestFlow(
    state: Lifecycle.State,
    flow: Flow<T>,
    collector: suspend (T) -> Unit
) {
    lifecycleScope.launch {
        repeatOnLifecycle(state) {
            flow.collectLatest(collector)
        }
    }
}

fun <T> AppCompatActivity.repeatCollectLatestFlowOnCreated(
    flow: Flow<T>,
    collector: suspend (T) -> Unit
) = repeatLifecycleCollectLatestFlow(Lifecycle.State.CREATED, flow, collector)
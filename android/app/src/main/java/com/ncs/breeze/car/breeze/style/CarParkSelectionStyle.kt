package com.ncs.breeze.car.breeze.style

import com.mapbox.maps.extension.androidauto.MapboxCarMapObserver
import com.mapbox.maps.extension.androidauto.MapboxCarMapSurface
import com.ncs.breeze.R
import com.ncs.breeze.car.breeze.MainBreezeCarContext
import com.ncs.breeze.common.extensions.car.toBitmap
import com.breeze.model.constants.Constants
import com.ncs.breeze.common.utils.ParkingIconUtils.ImageIconType
import com.ncs.breeze.common.utils.ParkingIconUtils.ParkingMarkerImageIconType

class CarParkSelectionStyle(
    private val mainCarContext: MainBreezeCarContext
) : MapboxCarMapObserver {


    override fun onAttached(mapboxCarMapSurface: MapboxCarMapSurface) {
        mapboxCarMapSurface.mapSurface.getMapboxMap().getStyle()?.let { style ->
            val resources = mainCarContext.carContext.resources

            mainCarContext.carContext.toBitmap(R.drawable.destination_puck)?.let { bitmap ->
                style.addImage(Constants.DESTINATION_IMAGE, bitmap, false)
            }

            hashMapOf(
                //Keeping this icon as fail safe case because this icon is in DEFAULT block
                ImageIconType.SELECTED.type to R.drawable.new_car_park_icon,

                //Finalised Colored Parking icons
                ParkingMarkerImageIconType.PARKINGMARKER_SEASON_DESTINATION_SELECTED.type to R.drawable.ic_parkingmarker_season_destination_selected,
                ParkingMarkerImageIconType.PARKINGMARKER_SEASON_DESTINATION_NOTSELECTED.type to R.drawable.ic_parkingmarker_season_destination_notselected,
                ParkingMarkerImageIconType.PARKINGMARKER_SEASON_NONDESTINATION_SELECTED.type to R.drawable.ic_parkingmarker_season_nondestination_selected,
                ParkingMarkerImageIconType.PARKINGMARKER_SEASON_NONDESTINATION_NOTSELECTED.type to R.drawable.ic_parkingmarker_season_nondestination_notselected,
                ParkingMarkerImageIconType.PARKINGMARKER_TYPE0_DESTINATION_SELECTED.type to R.drawable.ic_parkingmarker_typezero_destination_selected,
                ParkingMarkerImageIconType.PARKINGMARKER_TYPE0_DESTINATION_NOTSELECTED.type to R.drawable.ic_parkingmarker_typezero_destination_notselected,
                ParkingMarkerImageIconType.PARKINGMARKER_TYPE0_NONDESTINATION_SELECTED.type to R.drawable.ic_parkingmarker_type0_nondestination_selected,
                ParkingMarkerImageIconType.PARKINGMARKER_TYPE0_NONDESTINATION_NOTSELECTED.type to R.drawable.ic_parkingmarker_type0_nondestination_notselected,
                ParkingMarkerImageIconType.PARKINGMARKER_TYPE1_DESTINATION_SELECTED.type to R.drawable.ic_parkingmarker_typeone_destination_selected,
                ParkingMarkerImageIconType.PARKINGMARKER_TYPE1_DESTINATION_NOTSELECTED.type to R.drawable.ic_parkingmarker_typeone_destination_notselected,
                ParkingMarkerImageIconType.PARKINGMARKER_TYPE1_NONDESTINATION_SELECTED.type to R.drawable.ic_parkingmarker_type1_nondestination_selected,
                ParkingMarkerImageIconType.PARKINGMARKER_TYPE1_NONDESTINATION_NOTSELECTED.type to R.drawable.ic_parkingmarker_type1_nondestination_notselected,
                ParkingMarkerImageIconType.PARKINGMARKER_TYPE2_DESTINATION_SELECTED.type to R.drawable.ic_parkingmarker_typetwo_destination_selected,
                ParkingMarkerImageIconType.PARKINGMARKER_TYPE2_DESTINATION_NOTSELECTED.type to R.drawable.ic_parkingmarker_typetwo_destination_notselected,
                ParkingMarkerImageIconType.PARKINGMARKER_TYPE2_NONDESTINATION_SELECTED.type to R.drawable.ic_parkingmarker_type2_nondestination_selected,
                ParkingMarkerImageIconType.PARKINGMARKER_TYPE2_NONDESTINATION_NOTSELECTED.type to R.drawable.ic_parkingmarker_type2_nondestination_notselected,
                ParkingMarkerImageIconType.PARKINGMARKER_NORATE_DESTINATION_SELECTED.type to R.drawable.ic_parkingmarker_norate_destination_selected,
                ParkingMarkerImageIconType.PARKINGMARKER_NORATE_DESTINATION_NOTSELECTED.type to R.drawable.ic_parkingmarker_norate_destination_notselected,
                ParkingMarkerImageIconType.PARKINGMARKER_NORATE_NONDESTINATION_SELECTED.type to R.drawable.ic_parkingmarker_norate_nondestination_selected,
                ParkingMarkerImageIconType.PARKINGMARKER_NORATE_NONDESTINATION_NOTSELECTED.type to R.drawable.ic_parkingmarker_norate_nondestination_notselected,
            ).forEach { (key, value) ->
                mainCarContext.carContext.toBitmap(value)?.let { bitmap ->
                    style.addImage(key, bitmap, false)
                }
            }
            mainCarContext.carContext.toBitmap(R.drawable.new_car_park_icon)?.let { bitmap ->
                style.addImage(Constants.CARPARK_FEATURE_NEW_CARPARK_IMAGE, bitmap, false)
            }
            mainCarContext.carContext.toBitmap(R.drawable.ic_bg_parking_price_aa)?.let { bitmap ->
                style.addImage(Constants.CARPARK_PRICE_IMAGE, bitmap, false)
            }

            mainCarContext.carContext.toBitmap(R.drawable.bg_car_map_icon_index)?.let { bitmap ->
                style.addImage(Constants.CARPARK_INDEX_IMAGE, bitmap, false)
            }
            mainCarContext.carContext.toBitmap(R.drawable.ic_bg_parking_price_cheapest_aa)
                ?.let { bitmap ->
                    style.addImage(Constants.CARPARK_PRICE_GREEN_IMAGE, bitmap, false)
                }
            style.removeStyleLayer(Constants.TRAFFIC_LAYER_ID)
        }
    }

    companion object {

        const val CARPARK_PRICE = "curPrice"
        const val CARPARK_INDEX = "curIndex"
        const val CARPARK_IS_DESTINATION = "cpIsDestination"

    }

}
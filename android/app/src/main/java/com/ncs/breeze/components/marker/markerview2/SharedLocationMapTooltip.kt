package com.ncs.breeze.components.marker.markerview2

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.lifecycle.Lifecycle
import com.breeze.model.DestinationAddressDetails
import com.breeze.model.SharedLocation
import com.breeze.model.constants.Constants
import com.breeze.model.enums.AmenityBand
import com.facebook.react.bridge.Arguments
import com.google.android.gms.maps.model.LatLng
import com.mapbox.geojson.Point
import com.mapbox.maps.MapView
import com.ncs.breeze.R
import com.ncs.breeze.common.analytics.Analytics
import com.ncs.breeze.common.analytics.Event
import com.ncs.breeze.common.analytics.Screen
import com.ncs.breeze.common.extensions.android.getShareBusinessLogicPackage
import com.ncs.breeze.common.utils.LocationBreezeManager
import com.ncs.breeze.databinding.MapTooltipSharedLocationBinding
import com.ncs.breeze.reactnative.nativemodule.ShareBusinessLogicEvent
import com.ncs.breeze.ui.dashboard.activity.DashboardActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class SharedLocationMapTooltip(mapView: MapView) : MarkerView2(mapView.getMapboxMap()) {
    val viewBinding: MapTooltipSharedLocationBinding
    var currentSharedLocation: SharedLocation? = null

    init {
        viewBinding = MapTooltipSharedLocationBinding.inflate(
            LayoutInflater.from(mapView.context),
            null,
            false
        )
        mViewMarker = viewBinding.root
    }

    fun bindSharedLocation(sharedLocation: SharedLocation) {
        currentSharedLocation = sharedLocation
        mLatLng = LatLng(
            sharedLocation.lat.toDoubleOrNull() ?: 0.0,
            sharedLocation.long.toDoubleOrNull() ?: 0.0,
        )
        viewBinding.tvName.text =
            currentSharedLocation?.name?.takeIf { it.isNotEmpty() }
                ?: sharedLocation.address1
        val description = when {
            !sharedLocation.description.isNullOrBlank() -> sharedLocation.description
            sharedLocation.amenityType.isNullOrEmpty() -> {
                if (!sharedLocation.fullAddress.isNullOrBlank()) sharedLocation.fullAddress
                else if (!sharedLocation.address2.isNullOrBlank()) sharedLocation.address2
                else if (!sharedLocation.address1.isNullOrBlank()) sharedLocation.address1
                else "${sharedLocation.lat},${sharedLocation.long}"
            }

            else -> sharedLocation.address1
        }
        displayCrowdsourceInfo()
        viewBinding.tvDescription.text = description

        viewBinding.buttonShare.setOnClickListener {
            shareLocation()
        }

        viewBinding.buttonInvite.setOnClickListener {
            inviteLocation()
        }

        viewBinding.buttonNavigateHere.setOnClickListener {
            onNavigateHereClicked()
        }

        viewBinding.imgFlagSaved.setImageDrawable(
            ContextCompat.getDrawable(
                viewBinding.root.context,
                if (sharedLocation.saved) R.drawable.ic_bookmarked else R.drawable.ic_nobookmark
            )
        )
        viewBinding.imgFlagSaved.setOnClickListener {
            val location = currentSharedLocation ?: return@setOnClickListener
            if (location.saved) {
                deleteSharedLocation(location)
            } else {
                bookmarkSharedLocation(location)
            }
        }
    }

    private fun displayCrowdsourceInfo() {
        val availabilityCSData = currentSharedLocation?.availabilityCSData
        viewBinding.layoutCarparkStatus.root.isVisible = availabilityCSData != null
        if (availabilityCSData != null) {
            val title: String = availabilityCSData.title ?: ""
//            Calculate last update time
            val timeUpdate =
                "${availabilityCSData.primaryDesc ?: viewBinding.root.context.getString(com.breeze.customization.R.string.update_carpark_stt_time)} ${availabilityCSData.generateDisplayedLastUpdate()}"
            val drawableHeaderCpStatus =
                AppCompatResources.getDrawable(viewBinding.root.context, R.drawable.bg_header_carpark_status)
            drawableHeaderCpStatus?.setTint(Color.parseColor(availabilityCSData?.themeColor ?: "#F26415"))
            viewBinding.layoutCarparkStatus.root.background = drawableHeaderCpStatus
            viewBinding.layoutCarparkStatus.icCarparkStatus.setImageResource(
                when (currentSharedLocation?.availablePercentImageBand) {
                    AmenityBand.TYPE0.type -> R.drawable.ic_cp_stt_full
                    AmenityBand.TYPE2.type -> R.drawable.ic_cp_stt_available
                    else -> R.drawable.ic_cp_stt_few
                }
            )
            viewBinding.layoutCarparkStatus.tvStatusCarpark.text = title
            viewBinding.layoutCarparkStatus.tvTimeUpdateCarparkStatus.text = timeUpdate
        }
    }

    private fun bookmarkSharedLocation(sharedLocation: SharedLocation) {
        Analytics.logClickEvent(
            Event.MAP_TOOLTIP_SAVED_BOOKMARK,
            Screen.SHARED_COLLECTION_RECIPIENT
        )
        viewBinding.root.context?.getShareBusinessLogicPackage()?.getRequestsModule()
            ?.flatMap {
                val data = Arguments.fromBundle(
                    bundleOf(
                        "action" to "ADD",
                        "bookmark" to bundleOf(
                            "lat" to sharedLocation.lat,
                            "long" to sharedLocation.long,
                            "address1" to sharedLocation.address1,
                            "name" to sharedLocation.name,
                            "address2" to sharedLocation.address2,
                            "amenityType" to sharedLocation.amenityType,
                            "amenityId" to sharedLocation.amenityId,
                            "isDestination" to sharedLocation.isDestination,
                            "code" to sharedLocation.code,
                            "bookmarkSnapshotId" to sharedLocation.bookmarkSnapshotId,
                            "userLocationLinkId" to sharedLocation.userLocationLinkId,
                            "fullAddress" to (sharedLocation.fullAddress),
                            "placeId" to (sharedLocation.placeId),
                            "userLocationLinkIdRef" to (sharedLocation.userLocationLinkIdRef),
                            "showAvailabilityFB" to (sharedLocation.showAvailabilityFB),
                        )
                    )
                )
                it.callRN(
                    ShareBusinessLogicEvent.TOGGLE_SAVE_LOCATION_SHARED_COLLECTION.name, data
                )
            }?.subscribeOn(Schedulers.io())?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe()
    }

    private fun deleteSharedLocation(sharedLocation: SharedLocation) {
        Analytics.logClickEvent(
            Event.MAP_TOOLTIP_SAVED_UNBOOKMARK,
            Screen.SHARED_COLLECTION_RECIPIENT
        )
        viewBinding.root.context?.getShareBusinessLogicPackage()?.getRequestsModule()
            ?.flatMap {
                val data = Arguments.fromBundle(
                    bundleOf(
                        "action" to "DELETE",
                        "lat" to sharedLocation.lat,
                        "long" to sharedLocation.long,
                        "bookmarkId" to sharedLocation.bookmarkId,
                        "address1" to sharedLocation.address1,
                        "amenityId" to sharedLocation.amenityId,
                        "bookmarkSnapshotId" to sharedLocation.bookmarkSnapshotId,
                        "userLocationLinkId" to sharedLocation.userLocationLinkId
                    )
                )
                it.callRN(
                    ShareBusinessLogicEvent.TOGGLE_SAVE_LOCATION_SHARED_COLLECTION.name,
                    data
                )
            }?.subscribeOn(Schedulers.io())?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe()
    }

    override fun updateTooltipPosition() {
        val latLng = mLatLng ?: return
        val viewMarker = mViewMarker ?: return
        val screenCoordinate = mapboxMap.pixelForCoordinate(
            Point.fromLngLat(latLng.longitude, latLng.latitude)
        )
        viewMarker.measure(
            View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
            View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
        )
        viewMarker.x = screenCoordinate.x.toFloat() - (viewMarker.measuredWidth / 2)
        viewMarker.y = screenCoordinate.y.toFloat() - viewMarker.measuredHeight
    }


    override fun showMarker() {
        viewBinding.root.isVisible = true
    }

    override fun hideMarker() {
        viewBinding.root.isVisible = false
    }

    private fun createShareLocationData() = currentSharedLocation?.let { sharedLocation ->
        Arguments.fromBundle(
            bundleOf(
                "address1" to sharedLocation.address1,
                "latitude" to sharedLocation.lat,
                "longitude" to sharedLocation.long,
                "amenityId" to sharedLocation.amenityId,
                "amenityType" to sharedLocation.amenityType,
                "name" to sharedLocation.name,
                "code" to sharedLocation.code,
                "isDestination" to sharedLocation.isDestination,
                "description" to (sharedLocation.description ?: ""),
                "fullAddress" to sharedLocation.fullAddress,
                "bookmarkId" to sharedLocation.bookmarkId,
                "placeId" to sharedLocation.placeId,
                "userLocationLinkIdRef" to sharedLocation.userLocationLinkIdRef,
            )
        )
    }


    private fun shareLocation() {
        val sharedLocationData = createShareLocationData() ?: return
        Analytics.logClickEvent(
            Event.MAP_TOOLTIP_SAVED_SHARE,
            Screen.SHARED_COLLECTION_RECIPIENT
        )
        viewBinding.root.context.getShareBusinessLogicPackage()?.getRequestsModule()?.flatMap {
            it.callRN(
                ShareBusinessLogicEvent.SHARE_LOCATION_FROM_NATIVE.eventName,
                sharedLocationData
            )
        }?.subscribeOn(Schedulers.io())?.observeOn(AndroidSchedulers.mainThread())?.subscribe()
    }

    private fun inviteLocation() {
        val sharedLocationData = createShareLocationData() ?: return
        Analytics.logClickEvent(
            "[map]:tooltip_saved_invite",
            Screen.SHARED_COLLECTION_RECIPIENT
        )
        viewBinding.root.context.getShareBusinessLogicPackage()?.getRequestsModule()?.flatMap {
            it.callRN(
                ShareBusinessLogicEvent.LOCATION_INVITE_FROM_NATIVE.eventName,
                sharedLocationData
            )
        }?.subscribeOn(Schedulers.io())?.observeOn(AndroidSchedulers.mainThread())?.subscribe()
    }

    private fun onNavigateHereClicked() {
        val dashboardActivity = (viewBinding.root.context as? DashboardActivity)?.takeIf {
            it.lifecycle.currentState.isAtLeast(
                Lifecycle.State.STARTED
            )
        } ?: return

        Analytics.logClickEvent(
            Event.MAP_TOOLTIP_SAVED_NAVIGATE_HERE,
            Screen.SHARED_COLLECTION_RECIPIENT
        )
        val sharedLocation = currentSharedLocation ?: return
        val destinationDetail = sharedLocation.toDestinationDetails()
        val currentLocation = LocationBreezeManager.getInstance().currentLocation
        val sourceAddressDetails = DestinationAddressDetails(
            null,
            address1 = null,
            null,
            currentLocation?.latitude?.toString(),
            currentLocation?.longitude?.toString(),
            null,
            Constants.TAGS.CURRENT_LOCATION_TAG,
            null,
            -1
        )
        dashboardActivity.showRouteAndResetToCenter(
            bundleOf(
                Constants.DESTINATION_ADDRESS_DETAILS to destinationDetail,
                Constants.SOURCE_ADDRESS_DETAILS to sourceAddressDetails
            )
        )
    }

}
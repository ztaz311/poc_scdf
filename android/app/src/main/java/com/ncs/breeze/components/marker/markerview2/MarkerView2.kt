package com.ncs.breeze.components.marker.markerview2

import android.view.View
import androidx.annotation.NonNull
import com.google.android.gms.maps.model.LatLng
import com.mapbox.geojson.Point
import com.mapbox.maps.MapView
import com.mapbox.maps.MapboxMap
import com.mapbox.maps.ScreenCoordinate


open class MarkerView2  (val mapboxMap: MapboxMap, val itemId: String? = null) {


    var mViewMarker: View? = null
    var mLatLng: LatLng? = null
    private var projection: ScreenCoordinate? = null

    open fun resetView() {}

    open fun hideAllToolTips() {}

    open fun updateTooltipPosition() {
        mLatLng?.let {
            val screenCoordinate =
                mapboxMap.pixelForCoordinate(Point.fromLngLat(it.longitude, it.latitude))
            mViewMarker?.x = screenCoordinate.x.toFloat() - 360
            mViewMarker?.y = screenCoordinate.y.toFloat() - 260
        }
    }

    open fun hideMarker() {}

    open fun showMarker() {}

    override fun equals(other: Any?): Boolean {
        return if (this === other) {
            true
        } else if (other !is MarkerView2) {
            false
        } else {
            if (this.itemId != null && (this.itemId == other.itemId)) {
                true
            } else {
                super.equals(other)
            }
        }
    }

    open fun updateBookMarkIconState(saved: Boolean, bookmarkId: Int = -1) {}
}

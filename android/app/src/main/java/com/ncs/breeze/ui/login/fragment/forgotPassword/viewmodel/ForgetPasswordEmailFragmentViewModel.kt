package com.ncs.breeze.ui.login.fragment.forgotPassword.viewmodel

import android.app.Application
import com.amplifyframework.auth.result.step.AuthNextResetPasswordStep
import com.amplifyframework.core.Amplify
import com.facebook.react.bridge.UiThreadUtil.runOnUiThread
import com.ncs.breeze.R
import com.ncs.breeze.common.extensions.android.getApp
import com.ncs.breeze.common.utils.AWSUtils
import com.ncs.breeze.components.AmplifyErrorHandler
import com.ncs.breeze.components.SingleLiveEvent
import com.ncs.breeze.ui.base.BaseFragmentViewModel
import javax.inject.Inject

class ForgetPasswordEmailFragmentViewModel @Inject constructor(application: Application) :
    BaseFragmentViewModel(
        application
    ) {

    var resetPasswordSuccessful: SingleLiveEvent<Boolean> = SingleLiveEvent()
    var resetPasswordFailed: SingleLiveEvent<AmplifyErrorHandler.AuthObject> = SingleLiveEvent()

    fun resetPassword(email: String) {
        AWSUtils.resetPassword(
            email,
            { result ->
                runOnUiThread {
                    if (!result.isPasswordReset && (result.nextStep is AuthNextResetPasswordStep)) {
                        resetPasswordSuccessful.value = true
                    } else {
                        resetPasswordFailed.value = AmplifyErrorHandler.AuthObject(
                            getApp().getString(R.string.error_email_no_match),
                            AmplifyErrorHandler.FieldType.DEFAULT
                        )
                    }
                }
            },
            { error ->
                runOnUiThread {
                    AmplifyErrorHandler.getErrorMessage(
                        error,
                        getApp(),
                        AmplifyErrorHandler.AuthType.FORGOT_PASSWORD
                    ).also { resetPasswordFailed.value = it }
                }
            }
        )
    }
}
package com.ncs.breeze.common.worker

import android.content.Context
import android.content.pm.PackageInfo
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.ncs.breeze.BuildConfig
import com.ncs.breeze.common.extensions.android.getAppPreference
import com.breeze.model.FirebaseDB
import com.ncs.breeze.common.storage.AppPrefsKey
import com.ncs.breeze.common.utils.BreezeHistoryRecorderUtil
import com.ncs.breeze.common.utils.Utils
import timber.log.Timber
import java.util.Date

class WorkerNavigationLogDBUpdate(
    appContext: Context,
    workerParams: WorkerParameters
) : CoroutineWorker(appContext, workerParams) {
    override suspend fun doWork(): Result {
        Timber.d("Save Navigation log metadata: $inputData")
        val userId = inputData.getString(WorkerNavigationLogFileUpload.KEY_USER_ID)
            ?.takeIf { it.isNotEmpty() } ?: return Result.failure()
        val downloadUrl = inputData.getString(KEY_DOWNLOAD_URL)
            ?.takeIf { it.isNotEmpty() } ?: return Result.failure()
        val description = inputData.getString(WorkerNavigationLogFileUpload.KEY_DESCRIPTION)
            ?.takeIf { it.isNotEmpty() } ?: return Result.failure()
        val fileName = inputData.getString(WorkerNavigationLogFileUpload.KEY_SAVED_FILE_NAME)
            ?.takeIf { it.isNotEmpty() } ?: return Result.failure()

        val fcmId = if (BuildConfig.FLAVOR != "production")
            applicationContext.getAppPreference()?.getString(AppPrefsKey.PUSH_MESSAGING_TOKEN) ?: ""
        else ""
        val pInfo: PackageInfo =
            applicationContext.packageManager.getPackageInfo(applicationContext.packageName, 0)
        val version = pInfo.versionName
        val buildNumber = pInfo.versionCode

        val dbItem = FirebaseDB(
            description = description,
            fileDownloadURL = downloadUrl,
            deviceOS = "ANDROID",
            deviceOSVersion = Utils.getDeviceOS(),
            deviceModel = Utils.getDeviceModel(),
            deviceManufacturer = Utils.getDeviceManufacturer(),
            appVersion = String.format("V%s(%s)", version, buildNumber),
            time = BreezeHistoryRecorderUtil.utcFormatter.format(Date()),
            secondaryFirebaseID = "",
            fcmId = fcmId,
        )
        val updateFirestoreDbResult =
            Firebase.firestore.collection(userId).document(fileName).set(dbItem)
        Timber.d("Update db result for $downloadUrl: $updateFirestoreDbResult")
        return Result.success()
    }

    companion object {
        const val KEY_DOWNLOAD_URL = "KEY_DOWNLOAD_URL"

    }

}
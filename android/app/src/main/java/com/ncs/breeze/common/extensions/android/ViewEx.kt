package com.ncs.breeze.common.extensions.android

import android.view.View
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat

fun View.setBackgroundColorResource(@ColorRes colorRes: Int) =
    setBackgroundColor(ContextCompat.getColor(context, colorRes))

/**
 * Check if this view contain the other
 * @param view the view to check if it's contained by this view
 * @param rx touch position x
 * @param ry touch position y
 * */
fun View.isContainView(
    view: View, rx: Int,
    ry: Int
): Boolean {
    val l = IntArray(2)
    view.getLocationOnScreen(l)
    var x = l[0]
    var y = l[1]
    this.getLocationOnScreen(l)
    x -= l[0]
    y -= l[1]
    val w = view.width
    val h = view.height
    return !(rx < x || rx > x + w || ry < y || ry > y + h)
}
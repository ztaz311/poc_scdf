package com.ncs.breeze.ui.dashboard.fragments.obulite

import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.drawable.AnimationDrawable
import android.location.Location
import android.media.AudioManager
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.car.app.connection.CarConnection
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.breeze.customization.view.OBUAmenityToggleView
import com.breeze.model.*
import com.breeze.model.api.request.ETARequest
import com.breeze.model.api.response.ERPResponseData
import com.breeze.model.api.response.amenities.BaseAmenity
import com.breeze.model.constants.AmenityType.CARPARK
import com.breeze.model.constants.AmenityType.EVCHARGER
import com.breeze.model.constants.AmenityType.PETROL
import com.breeze.model.constants.Constants
import com.breeze.model.constants.NavigationType
import com.breeze.model.enums.RoutePreference
import com.breeze.model.enums.TrafficIncidentData
import com.breeze.model.extensions.dpToPx
import com.breeze.model.extensions.toBitmap
import com.mapbox.android.gestures.MoveGestureDetector
import com.mapbox.android.gestures.RotateGestureDetector
import com.mapbox.api.directions.v5.models.DirectionsResponse
import com.mapbox.api.directions.v5.models.DirectionsRoute
import com.mapbox.geojson.FeatureCollection
import com.mapbox.geojson.Point
import com.mapbox.maps.CameraOptions
import com.mapbox.maps.EdgeInsets
import com.mapbox.maps.MapboxMap
import com.mapbox.maps.Style
import com.mapbox.maps.extension.style.expressions.dsl.generated.interpolate
import com.mapbox.maps.extension.style.expressions.generated.Expression
import com.mapbox.maps.extension.style.layers.addLayer
import com.mapbox.maps.extension.style.layers.addLayerAbove
import com.mapbox.maps.extension.style.layers.generated.symbolLayer
import com.mapbox.maps.extension.style.layers.getLayer
import com.mapbox.maps.extension.style.layers.properties.generated.IconAnchor
import com.mapbox.maps.extension.style.layers.properties.generated.TextAnchor
import com.mapbox.maps.extension.style.sources.addSource
import com.mapbox.maps.extension.style.sources.generated.geoJsonSource
import com.mapbox.maps.plugin.LocationPuck2D
import com.mapbox.maps.plugin.animation.camera
import com.mapbox.maps.plugin.gestures.OnMoveListener
import com.mapbox.maps.plugin.gestures.OnRotateListener
import com.mapbox.maps.plugin.gestures.gestures
import com.mapbox.maps.plugin.locationcomponent.LocationComponentPlugin
import com.mapbox.maps.plugin.locationcomponent.location
import com.mapbox.navigation.base.ExperimentalPreviewMapboxNavigationAPI
import com.mapbox.navigation.base.road.model.RoadComponent
import com.mapbox.navigation.core.lifecycle.MapboxNavigationApp
import com.mapbox.navigation.core.trip.session.*
import com.mapbox.navigation.ui.maps.camera.NavigationCamera
import com.mapbox.navigation.ui.maps.camera.data.MapboxNavigationViewportDataSource
import com.mapbox.navigation.ui.maps.camera.transition.NavigationCameraTransitionOptions
import com.mapbox.navigation.ui.maps.camera.transition.TransitionEndListener
import com.mapbox.navigation.ui.maps.location.NavigationLocationProvider
import com.ncs.breeze.R
import com.ncs.breeze.car.breeze.screen.obulite.CarOBULiteManager
import com.ncs.breeze.car.breeze.utils.LimitClick
import com.ncs.breeze.common.analytics.*
import com.ncs.breeze.common.appSync.ClientFactory
import com.ncs.breeze.common.constant.MapIcon
import com.ncs.breeze.common.constant.RNScreen
import com.ncs.breeze.common.event.ToAppRx
import com.ncs.breeze.common.event.ToCarRx.postEventWithCacheLast
import com.ncs.breeze.common.extensions.android.getApp
import com.ncs.breeze.common.extensions.android.getRNFragmentNavigationParams
import com.ncs.breeze.common.extensions.android.getUserDataPreference
import com.ncs.breeze.common.extensions.mapbox.compressToGzip
import com.ncs.breeze.common.extensions.mapbox.setBreezeDefaultOptions
import com.ncs.breeze.common.helper.ehorizon.BreezeEdgeMetaDataObserver
import com.ncs.breeze.common.helper.ehorizon.BreezeNotificationObserver
import com.ncs.breeze.common.helper.ehorizon.BreezeRoadObjectHandler
import com.ncs.breeze.common.helper.ehorizon.EHorizonMode
import com.ncs.breeze.common.model.RouteAdapterObjects
import com.ncs.breeze.common.model.rx.AppToHomeTurnOffCruiseModeEvent
import com.ncs.breeze.common.model.rx.AppToNavigationStartEvent
import com.ncs.breeze.common.model.rx.AppToPhoneNavigationStartEvent
import com.ncs.breeze.common.model.rx.CarNavigationStartEventData
import com.ncs.breeze.common.model.rx.PhoneNavigationStartEventData
import com.ncs.breeze.common.model.rx.ToAppRxEvent
import com.ncs.breeze.common.model.rx.UpdateStateVoiceApp
import com.ncs.breeze.common.remote.MyServiceInterceptor
import com.ncs.breeze.common.rxbus.RxBus
import com.ncs.breeze.common.rxbus.RxEvent
import com.ncs.breeze.common.storage.BreezeUserPreference
import com.ncs.breeze.common.utils.*
import com.ncs.breeze.common.utils.eta.ETAEngine
import com.ncs.breeze.components.GlobalNotification
import com.ncs.breeze.components.map.profile.ProfileZoneLayer
import com.ncs.breeze.components.map.profile.ProfileZoneState
import com.ncs.breeze.components.navigationlog.HistoryRecorder
import com.ncs.breeze.databinding.FragmentObuLiteBinding
import com.ncs.breeze.service.CruiseControllerService
import com.ncs.breeze.ui.base.BaseFragment
import com.ncs.breeze.ui.dashboard.activity.DashboardActivity
import com.ncs.breeze.ui.navigation.CarConnectedWarningFragment
import com.ncs.breeze.ui.rn.CustomReactFragment
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import kotlinx.coroutines.*
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import timber.log.Timber
import javax.inject.Inject
import kotlin.math.roundToInt


class OBULiteFragment : BaseFragment<FragmentObuLiteBinding, OBULiteFragmentViewModel>() {

    companion object {
        const val TAG = "com.ncs.breeze.ui.dashboard.fragments.obulite.OBULiteFragment"
        private const val FOLLOW_MAX_ZOOM = 15.0
        private const val FOLLOW_MIN_ZOOM = 13.5
        private const val WHAT_CLEAR_AMENITY_TOGGLE = 9981
    }

    private var isCurrentLocationHasCarPark = true
    private var isCarparkDisableByUser = false

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val mViewModel: OBULiteFragmentViewModel by viewModels {
        viewModelFactory
    }

    var compositeDisposable: CompositeDisposable = CompositeDisposable()

    @Inject
    lateinit var userPreferenceUtil: BreezeUserPreferenceUtil

    @Inject
    lateinit var myServiceInterceptor: MyServiceInterceptor

    @Inject
    lateinit var breezeERPUtil: BreezeERPRefreshUtil

    @Inject
    lateinit var breezeIncidentsUtil: BreezeTrafficIncidentsUtil

    @Inject
    lateinit var breezeMiscRoadObjectUtil: BreezeMiscRoadObjectUtil

    @Inject
    lateinit var breezeSchoolSilverZoneUpdateUtil: BreezeSchoolSilverZoneUpdateUtil

    @Inject
    lateinit var breezeFeatureDetectionUtil: BreezeFeatureDetectionUtil

    private lateinit var locationComponent: LocationComponentPlugin

    private lateinit var mapboxHistoryRecorder: HistoryRecorder

    @Inject
    lateinit var breezeHistoryRecorderUtil: BreezeHistoryRecorderUtil

    private var etaRequest: ETARequest? = null

    var carParkLocationSelected: BaseAmenity? = null

    private var displayedNavigationZone: NavigationZone? = null

    private var isViewLoaded: Boolean = false
    private var shouldTerminateView: Boolean = false

    var currentLocation: Location? = null

    var isCameraTrackingDismissed: Boolean = false
    var shareDriveAnnounced: Boolean = false
    private var monitorLocationUpdates: Boolean = true

    var currentLocationZoneFirstTime: ProfileZoneLayer.ProfileZoneType? =
        ProfileZoneLayer.ProfileZoneType(null, null, ProfileZoneLayer.OUTER_ZONE)

    private lateinit var mapboxMap: MapboxMap
    private lateinit var navigationCamera: NavigationCamera
    private lateinit var viewportDataSource: MapboxNavigationViewportDataSource
    private lateinit var breezeRoadObjectHandler: BreezeRoadObjectHandler
    private val navigationLocationProvider = NavigationLocationProvider()
    private lateinit var breezeEdgeMetaDataObserver: BreezeEdgeMetaDataObserver
    private lateinit var breezeNotificationObserver: BreezeNotificationObserver

    private var dashBoardMapLanding: OBULiteHelper? = null
    private var autoRecenterMapHandler: Handler? = null

    var currentRoadName: String? = null
    private var carConnection: CarConnection? = null
    private var lastCarConnectionState = CarConnection.CONNECTION_TYPE_NOT_CONNECTED

    private val overviewEdgeInsets =
        EdgeInsets(60.0.dpToPx(), 120.0.dpToPx(), 200.0.dpToPx(), 120.0.dpToPx())

    private var followingBottomEdgeInsets = EdgeInsets(0.0, 0.0, 360.0.dpToPx(), 0.0)

    private val profileDetectionMutex = Mutex()
    private val profileZoneState = ProfileZoneState()

    private var carEventListener: Disposable? = null
    private var removeAmenitiesHandler: Handler? = null
    private var obuCheckSpeedHelper: OBUCheckSpeedHelper? = null

    private val speedLimitHandler = object : SpeedLimitHandler {
        override fun alertOnThreshold() {
            //VoiceInstructionsManager.getInstance()?.playVoiceInstructions(getString(R.string.voice_speed_limit_alert))
            BreezeAudioUtils.playOverSpeedAlert(context)
        }
    }

    private val onMoveListener = object : OnMoveListener {
        override fun onMoveBegin(detector: MoveGestureDetector) {

            onCameraTrackingDismissed()
            if (viewBinding.layoutMapController.obuAmenitiesView.toggleType != OBUAmenityToggleView.AmenityToggleType.NONE) {
                removeAmenitiesHandler?.removeMessages(WHAT_CLEAR_AMENITY_TOGGLE)
                shouldResetAmenity = false
                removeAmenitiesHandler?.sendEmptyMessageDelayed(
                    WHAT_CLEAR_AMENITY_TOGGLE,
                    10000
                )
            }
        }

        override fun onMove(detector: MoveGestureDetector): Boolean {
            return false
        }

        override fun onMoveEnd(detector: MoveGestureDetector) {}
    }


    private val locationObserver = object : LocationObserver {
        override fun onNewLocationMatcherResult(locationMatcherResult: LocationMatcherResult) {}

        override fun onNewRawLocation(rawLocation: Location) {
            //Timber.w("Mock location ? [${rawLocation.isMock}]")

            currentLocation = rawLocation
            currentLocation?.let { location ->
                Timber.d("Location changed from check 1 [${location.latitude}],[${location.longitude}]")

                val point = Point.fromLngLat(rawLocation.longitude, rawLocation.latitude)
                val cameraOptions =
                    CameraOptions.Builder().center(point).zoom(FOLLOW_MAX_ZOOM)
                        .build()
                viewBinding.mapViewCruiseMode.getMapboxMap().setCamera(cameraOptions)

                MapboxNavigationApp.current()?.unregisterLocationObserver(this)
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        OBUStripStateManager.getInstance()?.setPhoneOpened()
        OBUStripStateManager.getInstance()?.obuTriggered()
        activity?.volumeControlStream = AudioManager.STREAM_MUSIC
        if (shouldTerminateView) {
            onBackPressed()
            return
        }

        // BREEZE2-1845
//        mViewModel.etaStartProgress.observe(viewLifecycleOwner) {
//            if (true == it) {
//                viewBinding.layoutMapController.ETAView.visibility = View.GONE
//            } else {
//                viewBinding.layoutMapController.ETAView.visibility = View.VISIBLE
//            }
//        }
//
//        mViewModel.etaStartSuccessful.observe(viewLifecycleOwner) {
//            ETAEngine.getInstance()?.let {
//                it.startETA(null)
//                handleETAStartedEvent()
//            }
//        }
//
//        /**
//         * check eta first time
//         */
//        handleETAFirstTime()

        showRNOBULiteDisplay()
        setupCarEventListener()
        initialize()
        triggerCruiseMode()

        compositeDisposable.add(
            RxBus.listen(RxEvent.SpeedChanged::class.java)
                .subscribe {
                    handleSpeedChange(it.value)
                })


        compositeDisposable.add(
            RxBus.listen(RxEvent.ETARNDataCallback::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    etaRNCallback(it.data)
                })

        compositeDisposable.add(
            RxBus.listen(RxEvent.CloseNotifyArrivalScreen::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    closeEtaPopUp()
                })

        compositeDisposable.add(
            RxBus.listen(RxEvent.CloseCarparkListScreen::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    closeCarparkListScreen()
                })

        compositeDisposable.add(
            RxBus.listen(RxEvent.OpenCarparkListScreen::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    LimitClick.handleSafe {
                        currentLocation?.let {
                            showCarparkListScreen(it.latitude, it.longitude)
                        }
                    }


                })

        /**
         * first time open cruise mode open all carpark
         */
        isViewLoaded = true

        /**
         * register play sound alert when user go to TBR
         */
        mViewModel.mResponseZoneAlertDetail.observe(viewLifecycleOwner) {
            if (context != null && it.alertMessage != null && it.alertMessage!!.isNotEmpty()) {
                VoiceInstructionsManager.getInstance()?.playVoiceInstructions(it.alertMessage!!)
            }
        }

        viewBinding.mapViewCruiseMode.run {
            gestures.addOnMoveListener(onMoveListener)
        }

        /**
         * start detect if current user in the zone of profile
         */
        detectCurrentLocationInProfileZone()
        listenCarConnectionStateChanged()
    }

    private fun listenCarConnectionStateChanged() {
        carConnection = CarConnection(requireContext())
        carConnection?.type?.observe(viewLifecycleOwner) {
            if (it == CarConnection.CONNECTION_TYPE_NOT_CONNECTED && lastCarConnectionState != CarConnection.CONNECTION_TYPE_NOT_CONNECTED) {
                OBUStripStateManager.getInstance()?.closeFromPhone()
                CarOBULiteManager.closePhoneOBUScreen()
            } else {
                lastCarConnectionState = it
            }
        }
        childFragmentManager.beginTransaction().replace(
            viewBinding.carConnectedWarningContainer.id,
            CarConnectedWarningFragment.newInstance(
                isDefaultHideScreen = false,
                isDismissible = false
            ), CarConnectedWarningFragment.TAG
        ).commit()
        CarOBULiteManager.carScreenState.observe(viewLifecycleOwner) { lifecycleState ->
            val isVisible =
                lifecycleState.isAtLeast(Lifecycle.State.STARTED) || CarOBULiteManager.movedToAnotherScreen
            if (isVisible) {
                Analytics.logPopupEvent(
                    Event.OBU_PROJECTED_TO_DHU,
                    Event.VALUE_POPUP_OPEN,
                    Screen.OBU_DISPLAY_MODE
                )
            }
            viewBinding.carConnectedWarningContainer.isVisible = isVisible

        }

        OBUStripStateManager.getInstance()?.let { obuStripStateManager ->
            obuStripStateManager.obuStripCurrentState.observe(viewLifecycleOwner) {
                if (it == OBUStripState.CLOSED) {
                    if (isViewLoaded) {
                        handleCruiseModeEndEvent()
                        onBackPressed()
                    } else {
                        shouldTerminateView = true
                    }
                }
            }
        }
    }

    fun isProjectedInAA() = viewBinding.carConnectedWarningContainer.isVisible

    private fun handleSpeedChange(speedType: Int) {
        when (speedType) {
            SpeedLimitUtil.NORMAL_SPEED -> {
                viewBinding.layoutMapController.layoutSpeedLimit.ivSpeedLimit.setImageResource(
                    R.drawable.img_bg_speedlimit_normal
                )
            }

            SpeedLimitUtil.OVER_SPEED -> {
                viewBinding.layoutMapController.layoutSpeedLimit.ivSpeedLimit.setImageResource(
                    R.drawable.speed_limit_over_blinking
                )

                val animationDrawable: AnimationDrawable? =
                    viewBinding.layoutMapController.layoutSpeedLimit.ivSpeedLimit.drawable as? AnimationDrawable

                animationDrawable?.run {
                    isOneShot = true//stops animation after running one time
                    start()
                }

            }
        }
    }

    fun checkTripIdle(speedKmH: Double) {
        obuCheckSpeedHelper?.changeSpeed(speedKmH)

        TripLoggingManager.getInstance()?.checkIdle(speedKmH)
    }

    private fun showRNOBULiteDisplay() {
        childFragmentManager.beginTransaction()
            .replace(viewBinding.layoutOBUDisplayContainerRN.id, OBULiteRNFragment())
            .commitNow()
    }

    /**
     * detect current user go to profile zone
     */
    private fun detectCurrentLocationInProfileZone() {
        LocationBreezeManager.getInstance().currentLocation.let {
            lifecycleScope.launch(Dispatchers.IO) {
                val currentLocationZoneData =
                    ProfileZoneLayer.detectAllProfileLocationInZone(it)
                currentLocationZoneFirstTime = currentLocationZoneData
            }
        }
    }

    private fun setupCarEventListener() {
        carEventListener =
            ToAppRx.listenEvent(ToAppRxEvent::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    onEventFromCar(it)
                }
        compositeDisposable.add(carEventListener!!)
    }

    private fun etaRNCallback(data: java.util.HashMap<String, Any>) {
        //BREEZE2-1845
//        if (data.containsKey(Constants.TRIPETA.CONTACT_NAME) && data.containsKey(Constants.TRIPETA.CONTACT_NUMBER)
//        ) {
//            etaRequest = ETARequest(
//                type = Constants.TRIPETA.INIT,
//                recipientName = data[Constants.TRIPETA.CONTACT_NAME].toString(),
//                tripStartTime = Timestamp.now().seconds,
//                recipientNumber = data[Constants.TRIPETA.CONTACT_NUMBER].toString(),
//                tripEtaFavouriteId =
//                (data[Constants.TRIPETA.TRIP_ETA_FAVOURITE_ID] as? Double)?.toInt(),
//                message = getString(
//                    R.string.car_share_message_cruise,
//                    BreezeCarUtil.breezeUserPreferenceUtil.retrieveUserName(),
//                ),
//                shareLiveLocation = Constants.TRIPETA.YES,
//                voiceCallToRecipient = Constants.TRIPETA.NO,
//            )
//        } else {
//            // Reset ETA
//            Analytics.logClickEvent(Event.RESET_MY_ETA, getScreenName())
//            etaRequest = null
//        }
//
//        if (etaRequest != null) {
//            /**
//             * call api get ETA request
//             */
//            ETAEngine.updateETPRequest(etaRequest)
//            //ETAEngine.getInstance()!!.startETA(null)
//            mViewModel.startETA(etaRequest!!)
//        }
    }

    /**
     * get data want to show in map
     */
    private fun getCarparkDataShowInMap() {

        val tempLocation = currentLocation
        dashBoardMapLanding?.activeLocationUse = tempLocation

        tempLocation?.let {
            mViewModel.getAmenitiesOnMap(it, CARPARK)
        }
    }

    private fun getPetrolDataShowInMap() {

        val tempLocation = currentLocation
        dashBoardMapLanding?.activeLocationUse = tempLocation


        tempLocation?.let {
            mViewModel.getAmenitiesOnMap(it, PETROL)
        }
    }

    private fun getEVDataShowInMap() {

        val tempLocation = currentLocation
        dashBoardMapLanding?.activeLocationUse = tempLocation

        tempLocation?.let {
            mViewModel.getAmenitiesOnMap(it, EVCHARGER)
        }
    }

    private fun toggleAmenities(
        isParkingShown: Boolean,
        isPetrolShown: Boolean,
        isEVChargerShown: Boolean
    ) {
        if (!isParkingShown) {
            updateCarparkListButtonState(true)
        }
        dashBoardMapLanding?.clearAmenities(!isParkingShown, !isPetrolShown, !isEVChargerShown)
        mViewModel.amenityType.observe(viewLifecycleOwner) {
            it?.let {
                when (it) {
                    CARPARK -> {
                        if (mViewModel.mListAmenities.size == 0) {
                            handleShowNotyNoAmenitiesIn500M(CARPARK)
                            updateCarparkListButtonState(true)
                            viewBinding.layoutMapController.obuAmenitiesView.toggleType =
                                OBUAmenityToggleView.AmenityToggleType.NONE
                            if (obuCheckSpeedHelper.isStationary()) {
                                isCurrentLocationHasCarPark = false
                            }
                        } else {
                            isCurrentLocationHasCarPark = true
                            updateCarparkListButtonState(false)
                        }

                        dashBoardMapLanding?.handleCarkPark(
                            mViewModel.mListAmenities,
//                        true,
                            showDestination = false,
                            isMoveCameraWrapperAll = false
                        )
                        viewBinding.layoutMapController.btnRecenterMap.isInvisible = false
                        setToOverViewMode(mViewModel.mListAmenities.map { amenity ->
                            Point.fromLngLat(
                                amenity.long!!,
                                amenity.lat!!
                            )
                        })
                    }

                    PETROL -> {
                        if (mViewModel.mListAmenities.size == 0) {
                            handleShowNotyNoAmenitiesIn500M(PETROL)
                        }
                        dashBoardMapLanding?.handlePetrol(
                            mViewModel.mListAmenities,
//                        true,
                            showDestination = false,
                            isMoveCameraWrapperAll = false
                        )

                        viewBinding.layoutMapController.btnRecenterMap.isInvisible = false
                        setToOverViewMode(mViewModel.mListAmenities.map { amenity ->
                            Point.fromLngLat(
                                amenity.long!!,
                                amenity.lat!!
                            )
                        })
                    }

                    EVCHARGER -> {
                        if (mViewModel.mListAmenities.size == 0) {
                            handleShowNotyNoAmenitiesIn500M(EVCHARGER)
                        }
                        dashBoardMapLanding?.handleEVCharger(
                            mViewModel.mListAmenities,
//                        true,
                            showDestination = false,
                            isMoveCameraWrapperAll = false
                        )

                        viewBinding.layoutMapController.btnRecenterMap.isInvisible = false
                        setToOverViewMode(mViewModel.mListAmenities.map { amenity ->
                            Point.fromLngLat(
                                amenity.long!!,
                                amenity.lat!!
                            )
                        })
                    }
                }

            }
        }
        when {
            isParkingShown -> {
                getCarparkDataShowInMap()
            }

            isPetrolShown -> {
                getPetrolDataShowInMap()
            }

            isEVChargerShown -> {
                getEVDataShowInMap()
            }

            else -> {
            }
        }

        removeAmenitiesHandler?.removeMessages(WHAT_CLEAR_AMENITY_TOGGLE)
        if (isParkingShown || isPetrolShown || isEVChargerShown) {
            shouldResetAmenity = false
            removeAmenitiesHandler?.sendEmptyMessageDelayed(WHAT_CLEAR_AMENITY_TOGGLE, 10000)
        } else {
            shouldResetAmenity = true
            recenterMap()
        }
    }

    private fun sendToggleCarparkAnalytics(isParkingShown: Boolean) {
        isCarparkDisableByUser = true
        if (isParkingShown) {
            Analytics.logToggleEvent(Event.PARKING_ICON_ON, getScreenName())

        } else {
            Analytics.logToggleEvent(Event.PARKING_ICON_OFF, getScreenName())
            Analytics.logEvent(Event.USER_TOGGLE, Event.OBU_CAR_PARK_CLOSE, Screen.OBU_DISPLAY_MODE)
        }
    }

    private fun sendTogglePetrolAnalytics(isPetrolShown: Boolean) {
        if (isPetrolShown) {
            Analytics.logToggleEvent(Event.OBU_DISPLAY_TOGGLE_PETROL_ON, getScreenName())

        } else {
            Analytics.logToggleEvent(Event.OBU_DISPLAY_TOGGLE_PETROL_OFF, getScreenName())
        }
    }

    private fun sendToggleEVAnalytics(isEVChargerShown: Boolean) {
        if (isEVChargerShown) {
            Analytics.logToggleEvent(Event.OBU_DISPLAY_TOGGLE_EV_ON, getScreenName())

        } else {
            Analytics.logToggleEvent(Event.OBU_DISPLAY_TOGGLE_EV_OFF, getScreenName())
        }
    }


    fun initialize() {
        addMapViewAlertsFragment()
        viewBinding.mapViewCruiseMode.setMaximumFps(30)
        mapboxMap = viewBinding.mapViewCruiseMode.getMapboxMap()
        viewBinding.mapViewCruiseMode.setBreezeDefaultOptions()
        viewportDataSource = MapboxNavigationViewportDataSource(
            viewBinding.mapViewCruiseMode.getMapboxMap()
        )

        viewportDataSource.options.followingFrameOptions.maxZoom = FOLLOW_MAX_ZOOM
        viewportDataSource.options.followingFrameOptions.minZoom = FOLLOW_MIN_ZOOM

        navigationCamera = NavigationCamera(
            viewBinding.mapViewCruiseMode.getMapboxMap(),
            viewBinding.mapViewCruiseMode.camera,
            viewportDataSource
        )

        initNavigation()

        navigationCamera.registerNavigationCameraStateChangeObserver { navigationCameraState -> }

        viewBinding.layoutMapController.obuAmenitiesView.run {
            setAmenitiesViewCallback { isParkingShown, isPetrolShown, isEVChargerShown ->
                toggleAmenities(isParkingShown, isPetrolShown, isEVChargerShown)
            }
            setToggleCarparkClickedCallback(::sendToggleCarparkAnalytics)
            setTogglePetrolClickedCallback(::sendTogglePetrolAnalytics)
            setToggleEVChargerClickedCallback(::sendToggleEVAnalytics)
        }

        viewBinding.layoutMapController.btnSearch.setOnClickListener {
            Analytics.logClickEvent(Event.SEARCH_COLON, Screen.OBU_DISPLAY_MODE)
            (activity as? DashboardActivity)?.openSearch(Constants.RN_CONSTANTS.OBU_DISPLAY)
        }


        viewBinding.layoutMapController.tvEndOBUMode.setOnClickListener {
            closeOBULiteScreen()
        }


        viewBinding.layoutMapController.btnToggleVoice.setOnClickListener {
            VoiceInstructionsManager.getInstance()?.toggleMute()
        }

        initMuteButtonState()

        viewBinding.layoutMapController.btnRecenterMap.setOnClickListener {
            Analytics.logClickEvent(Event.RECENTER, getScreenName())
            Analytics.logClickEvent(Event.OBU_DISPLAY_CLICK_RECENTER_MAP, Screen.OBU_DISPLAY_MODE)
            recenterMap()
        }
        viewBinding.spaceBottom.run {
            val hasPairedDevices =
                !getApp()?.getUserDataPreference()?.getConnectedOBUDevices().isNullOrEmpty()
            layoutParams.height =
                if (!hasPairedDevices) {
                    82.dpToPx()
                } else {
                    132.dpToPx()
                }
            if (hasPairedDevices) {
                Analytics.logEvent(
                    Event.OBU_SYSTEM_EVENT,
                    bundleOf(
                        Param.KEY_EVENT_VALUE to Event.BOTTOM_OBU_CARD_SHOWN,
                        Param.KEY_SCREEN_NAME to getScreenName()
                    )
                )
            }
        }

    }

    private fun addMapViewAlertsFragment() {
        childFragmentManager.beginTransaction().replace(
            viewBinding.layoutAlertMessageFragmentContainer.id,
            OBULiteMapAlertsFragment(),
            OBULiteMapAlertsFragment.TAG
        )
            .commitNow()
    }

    fun closeOBULiteScreen() {
        Analytics.logEvent(Event.USER_CLICK, Event.OBU_CLOSE, Screen.OBU_DISPLAY_MODE)
        postEventWithCacheLast(AppToHomeTurnOffCruiseModeEvent())

        handleCruiseModeEndEvent()
        val currentTopFragment = parentFragmentManager.findFragmentById(R.id.frame_container)
        if (currentTopFragment is OBULiteFragment)
            onBackPressed()
        else {
            val obuLiteFragment = parentFragmentManager.findFragmentByTag(TAG)
            if (obuLiteFragment is OBULiteFragment) {
                parentFragmentManager.beginTransaction().remove(obuLiteFragment)
                    .commitAllowingStateLoss()
            }
        }
    }


    /**
     * start feedback fragment
     */
    private fun newFeedbackFragment() {
        (activity as? DashboardActivity)?.newFeedbackFragment()
    }


    private fun initDashBoardMapLanding() {
        activity?.let {
            dashBoardMapLanding = OBULiteHelper()
            dashBoardMapLanding!!.setUp(
                viewBinding.mapViewCruiseMode,
                it,
                nightModeEnabled = userPreferenceUtil.isDarkTheme(),
                markerLayerClickHandlerEnabled = true,
                markerLayerClickFromCruise = true,
                { selectedCarPark ->
                    Analytics.logClickEvent(
                        eventValue = Event.OBU_DISPLAY_CAR_PARK_NAVIGATE_HERE,
                        getScreenName(),
                        bundleOf(
                            Param.KEY_LOCATION_NAME to "location_${selectedCarPark.name}",
                            Param.KEY_LONGITUDE to "${selectedCarPark.long}",
                            Param.KEY_LATITUDE to "${selectedCarPark.lat}",
                        )
                    )
                    navigateToCarPark(selectedCarPark)
                }, {
                    onCameraTrackingDismissed()
                }
            )
            removeAmenitiesHandler = Handler(Looper.getMainLooper()) { msg ->
                when (msg.what) {
                    WHAT_CLEAR_AMENITY_TOGGLE -> {
                        with(viewBinding.layoutMapController.obuAmenitiesView) {
                            val isStationary =
                                obuCheckSpeedHelper.isStationary() && toggleType == OBUAmenityToggleView.AmenityToggleType.CARPARK
                            if (!isStationary) {
                                clearToggle()
                            }
                        }
                    }
                }

                false
            }
        }

        if (obuCheckSpeedHelper == null) {
            obuCheckSpeedHelper = OBUCheckSpeedHelper()
            obuCheckSpeedHelper?.setStateChangeCallback(::onSpeedStateChanged)
        }
        updateCarparkListButtonState(true)
    }

    private fun onSpeedStateChanged(
        speedState: OBUCheckSpeedHelper.SpeedState,
        stateChanged: Boolean
    ) {
        Timber.e("onSpeedStateChanged: $speedState, $stateChanged")
        when (speedState) {
            OBUCheckSpeedHelper.SpeedState.RUNNING -> {
                isCarparkDisableByUser = false
                isCurrentLocationHasCarPark = true
                if (stateChanged) {
                    updateCarparkListButtonState(true)
                    closeCarparkListScreen()
                }
                if (shouldResetAmenity) {
                    if (viewBinding.layoutMapController.obuAmenitiesView.toggleType != OBUAmenityToggleView.AmenityToggleType.NONE) {
                        viewBinding.layoutMapController.obuAmenitiesView.clearToggle()
                    }
                } else {
                    if (removeAmenitiesHandler?.hasMessages(WHAT_CLEAR_AMENITY_TOGGLE) != true) {
                        removeAmenitiesHandler?.sendEmptyMessageDelayed(
                            WHAT_CLEAR_AMENITY_TOGGLE,
                            8000
                        )
                    }
                }
            }

            OBUCheckSpeedHelper.SpeedState.STATIONARY -> {
                isCameraTrackingDismissed = false
                if (viewBinding.layoutMapController.obuAmenitiesView.toggleType == OBUAmenityToggleView.AmenityToggleType.CARPARK) {
                    updateCarparkListButtonState(false)
                }
                if (isCurrentLocationHasCarPark && !isCarparkDisableByUser) {
                    val toggleType =
                        viewBinding.layoutMapController.obuAmenitiesView.toggleType
                    if (toggleType == OBUAmenityToggleView.AmenityToggleType.NONE) {
                        viewBinding.layoutMapController.obuAmenitiesView.toggleType =
                            OBUAmenityToggleView.AmenityToggleType.CARPARK
                        toggleAmenities(true, false, false)
                    }
                }
            }

            else -> {}
        }
    }

    fun navigateToCarPark(carPark: BaseAmenity) {
        currentLocation?.let {
            closeCarparkListScreen()
            showToastMessage("Routing to carpark")
            carParkLocationSelected = carPark
            routeInitiate()
        }
    }

    private fun Location?.isValid() = !(this == null || latitude == null || longitude == null)

    /**
     * find route
     */
    private fun routeInitiate() {
        if (!currentLocation.isValid() || carParkLocationSelected == null) {
            showUnsupportedAddressDialog()
            return
        }
        lifecycleScope.launch(Dispatchers.IO) {
            try {
                val destinationLocation = Location("")
                destinationLocation.latitude = carParkLocationSelected!!.lat!!
                destinationLocation.longitude = carParkLocationSelected!!.long!!
                val isAddressSupported = Utils.areAddressesSupported(
                    context = requireContext(),
                    origin = currentLocation!!,
                    destination = destinationLocation
                )
                withContext(Dispatchers.Main) {
                    ensureActive()
                    if (isAddressSupported) {
                        val originPoint = Point.fromLngLat(
                            currentLocation!!.longitude,
                            currentLocation!!.latitude
                        )
                        val destinationPoint = Point.fromLngLat(
                            carParkLocationSelected!!.long!!,
                            carParkLocationSelected!!.lat!!
                        )
                        findRoute(
                            locationName = getCarParkTitle(carParkLocationSelected!!),
                            originPoint = originPoint,
                            destination = destinationPoint
                        )
                    } else {
                        showUnsupportedAddressDialog()
                    }
                }
            } catch (e: CancellationException) {
                Timber.e(e, "routeInitiate Job was cancelled")
            }
        }
    }

    private fun getCarParkTitle(carPark: BaseAmenity): String {
        return carPark.name
            ?: carPark.address
            ?: "Car Park"
    }

    private fun findRoute(locationName: String, originPoint: Point, destination: Point) {

        mViewModel.routesRetrieved.observe(viewLifecycleOwner) { routesMap ->

            mViewModel.routesRetrieved.removeObservers(viewLifecycleOwner)

            var routeNavigation: RouteAdapterObjects? = null
            if (routesMap.containsKey(RoutePreference.FASTEST_ROUTE) && (routesMap[RoutePreference.FASTEST_ROUTE]?.size
                    ?: 0) > 0
            ) {
                routeNavigation = routesMap[RoutePreference.FASTEST_ROUTE]?.get(0)
            } else if (routesMap.containsKey(RoutePreference.CHEAPEST_ROUTE) && (routesMap[RoutePreference.CHEAPEST_ROUTE]?.size
                    ?: 0) > 0
            ) {
                routeNavigation = routesMap[RoutePreference.CHEAPEST_ROUTE]?.get(0)
            } else if (routesMap.containsKey(RoutePreference.SHORTEST_ROUTE) && (routesMap[RoutePreference.SHORTEST_ROUTE]?.size
                    ?: 0) > 0
            ) {
                routeNavigation = routesMap[RoutePreference.SHORTEST_ROUTE]?.get(0)
            }

            carParkLocationSelected?.let {
                val currentCarParkAddressDetail = DestinationAddressDetails(
                    null,
                    it.name,
                    null,
                    it.lat.toString(),
                    it.long.toString(),
                    null,
                    it.name,
                    null,
                    -1,
                    isCarPark = true,
                    carParkID = it.id
                )
                if (routeNavigation != null) {
                    triggerNavigationStartEvent(
                        destinationAddressDetails = currentCarParkAddressDetail,
                        route = routeNavigation.route,
                        originalResponse = routeNavigation.originalResponse,
                        erpData = null,
                        etaRequest = etaRequest
                    )
                } else {
                    showUnsupportedAddressDialog()
                }

            }


        }
        mViewModel.findRoutes(
            originPoint,
            destination,
            ArrayList(),
            locationName,
            null
        )
    }

    private fun triggerNavigationStartEvent(
        destinationAddressDetails: DestinationAddressDetails,
        route: DirectionsRoute,
        originalResponse: DirectionsResponse,
        erpData: ERPResponseData.ERPResponse?,
        etaRequest: ETARequest?
    ) {
        lifecycleScope.launch(Dispatchers.IO) {
            try {
                val compressedRoute = originalResponse.compressToGzip()
                withContext(Dispatchers.Main) {
                    ensureActive()
                    val phoneData = PhoneNavigationStartEventData(
                        destinationAddressDetails,
                        destinationAddressDetails,
                        route,
                        compressedRoute,
                        erpData,
                        etaRequest
                    )
                    ToAppRx.postEvent(AppToPhoneNavigationStartEvent(phoneData))
                    val carData = CarNavigationStartEventData(
                        destinationAddressDetails,
                        destinationAddressDetails,
                        route,
                        compressedRoute,
                        originalResponse,
                        erpData,
                        etaRequest,
                        isFromCruiseMode = true
                    )
                    postEventWithCacheLast(AppToNavigationStartEvent(carData))
                    cruiseModeStopped()

                    Handler(Looper.getMainLooper()).post {
                        activity?.supportFragmentManager?.popBackStack()
                    }
                }
            } catch (e: CancellationException) {
                Timber.e(e, "Start navigation cancelled")
            } catch (e: Exception) {
                Timber.e(e, "Failed to start navigation")
            }
        }
    }


    private fun showUnsupportedAddressDialog() {
        //Sometimes, route planning screen is not popping out when user comes back from Navigation
        //it may result crash here.
        if (context == null) {
            return
        }
        DialogFactory.createSingleChoiceDialog(
            context = requireContext(),
            title = null,
            message = getString(R.string.dialog_msg_no_route_found),
            btnTextPositive = getString(R.string.dialog_okay),
            actionPositive = null,
            canDismiss = false,
            dissmissListener = {
//                onBackPressed()
            }
        ).show()
    }

    override fun onStart() {
        super.onStart()
        (activity as? DashboardActivity)?.viewBinding?.mapView?.onStop()
    }

    override fun onStop() {
        super.onStop()
        (activity as? DashboardActivity)?.viewBinding?.mapView?.onStart()
    }

    override fun onDestroy() {
        super.onDestroy()
        OBUStripStateManager.getInstance()?.onEndCruise()
        OBUStripStateManager.getInstance()?.closeFromPhone()
    }

    private fun initNavigation() {

        monitorLocationUpdates = true

        mapboxHistoryRecorder = HistoryRecorder(
//            navigation = MapboxNavigationApp.current(),
//            firebaseUploader = ServiceLocator.firebaseUploader,
//            searchAnalyticsData = null,
//            breezeHistoryListener = this
        ).apply {
            navigationMode = "OBUDisplay"
            currentUserId = userPreferenceUtil.getUserStoredData()?.cognitoID ?: "unknown_user"
        }

        viewBinding.mapViewCruiseMode.gestures.addOnFlingListener {
            Analytics.logMapInteractionEvents(Event.DRAG_MAP, getScreenName())
        }

        viewBinding.mapViewCruiseMode.gestures.addOnRotateListener(object : OnRotateListener {
            override fun onRotate(detector: RotateGestureDetector) {}

            override fun onRotateBegin(detector: RotateGestureDetector) {}

            override fun onRotateEnd(detector: RotateGestureDetector) {
                Analytics.logMapInteractionEvents(Event.ROTATE_MAP, getScreenName())
            }
        })

        MapboxNavigationApp.current()?.let { mapboxNavigation ->
            mapboxNavigation.registerLocationObserver(locationObserver)
            mapboxNavigation.registerLocationObserver(mapMatcherResultObserver)
            SpeedLimitUtil.setupLocationObserver(mapboxNavigation, speedLimitHandler)
            initEhorizonObserver()
        }
    }

    private fun initEhorizonObserver() {
        initRoadMetaDataObserver()
        initRoadNotificationObserver()
        context?.let {
            breezeRoadObjectHandler = BreezeEHorizonProvider.create(
                it,
                breezeERPUtil,
                breezeIncidentsUtil,
                EHorizonMode.CRUISE,
                null,
            )
        }
    }

    private fun initRoadMetaDataObserver() {
        breezeEdgeMetaDataObserver = object : BreezeEdgeMetaDataObserver {
            override fun updateEdgeMetaData(
                speedLimit: Double?,
                names: List<RoadComponent>?,
                tunnel: Boolean
            ) {
                viewBinding.layoutMapController.layoutSpeedLimit.root.isInvisible =
                    speedLimit == null

                if (speedLimit != null) {
                    val roundSpeed = speedLimit.times(3.6).roundToInt()
                    viewBinding.layoutMapController.layoutSpeedLimit.roadSpeed.text =
                        roundSpeed.toString()
                    SpeedLimitUtil.setCurrentSpeedLimit(roundSpeed)
                } else {
                    SpeedLimitUtil.setCurrentSpeedLimit(-1)
                }

                currentRoadName = if (names.isNullOrEmpty()) null else names[0].text
                if (!names.isNullOrEmpty()) {
                    if (currentRoadName.isNullOrEmpty()) {
                        viewBinding.layoutMapController.tvStreetName.isInvisible = true
                    } else {
                        viewBinding.layoutMapController.tvStreetName.isVisible = true
                        viewBinding.layoutMapController.tvStreetName.text = currentRoadName
                    }
                } else {
                    viewBinding.layoutMapController.tvStreetName.isInvisible = true
                }
            }

        }
    }

    private var shouldResetAmenity = true
    private val mapMatcherResultObserver = object : LocationObserver {
        override fun onNewLocationMatcherResult(locationMatcherResult: LocationMatcherResult) {
            currentLocation = locationMatcherResult.enhancedLocation
            val transitionOptions: ValueAnimator.() -> Unit =
                if (locationMatcherResult.isTeleport) {
                    {
                        duration = 0
                    }
                } else {
                    {
                        duration = 1000
                    }
                }
            navigationLocationProvider.changePosition(
                locationMatcherResult.enhancedLocation,
                locationMatcherResult.keyPoints,
                latLngTransitionOptions = transitionOptions,
                bearingTransitionOptions = transitionOptions
            )

            viewportDataSource.onLocationChanged(locationMatcherResult.enhancedLocation)

            viewportDataSource.evaluate()

            /**
             * detect if user go to zone of tiong bahru
             */
            if (!BreezeMapDataHolder.isNotExistProfileAmenities() && currentLocationZoneFirstTime!!.type == ProfileZoneLayer.NO_ZONE) {
                lifecycleScope.launch(Dispatchers.IO) {
                    profileDetectionMutex.withLock {
                        val profileData =
                            ProfileZoneLayer.detectAllProfileLocationInZone(currentLocation)
                        when (profileData.type) {
                            ProfileZoneLayer.OUTER_ZONE -> {
                                if (!profileZoneState.isOuterTrigger) {
                                    profileZoneState.isOuterTrigger = true
                                    ProfileZoneLayer.getInnerZone(profileData.selectedProfile!!)
                                        ?.let {
                                            mViewModel.getZoneAlertDetail(
                                                profile = profileData.selectedProfile!!,
                                                navigationtype = NavigationType.CRUISE,
                                                zoneid = profileData.profileZoneId!!,
                                                targetZoneId = it
                                            )
                                        }

                                }
                            }

                            ProfileZoneLayer.INNER_ZONE -> {
//                                if (!profileZoneState.isInnnerTrigger) {
//                                    profileZoneState.isInnnerTrigger = true
//                                    profileZoneState.isOuterTrigger=true
//                                    mViewModel.getZoneAlertDetail(
//                                        profile = profileData.selectedProfile!!,
//                                        zoneid = profileData.profileZoneId!!,
//                                        targetZoneId = null
//                                    )
//                                    if (!isCarparksShown) {
//                                        isCarparksShown = false
//                                        withContext(Dispatchers.Main) {
//                                            toggleCarparks()
//                                        }
//                                    }
//                                }
                            }

                            else -> {

                            }
                        }
                    }
                }
            }

//            if (!isCameraTrackingDismissed) {
//                resetCameraTrackingToCurrentLocation()
//            }
        }

        override fun onNewRawLocation(rawLocation: Location) {
            //Timber.w("Mock location ? [${rawLocation.isMock}]")
        }
    }


//    private var needCheckParkingToggle = false

    private fun onCameraTrackingDismissed() {
        navigationCamera.requestNavigationCameraToIdle()
        isCameraTrackingDismissed = true
        viewBinding.layoutMapController.btnRecenterMap.isInvisible = false
        if (autoRecenterMapHandler == null) {
            autoRecenterMapHandler = Handler(Looper.getMainLooper())
        }
        autoRecenterMapHandler?.removeCallbacksAndMessages(null)
        autoRecenterMapHandler?.postDelayed({
            if (isCameraTrackingDismissed) {
                recenterMap()
            }
        }, Constants.AUTO_RECENTER_MAP_DURATION)
    }

    private fun recenterMap() {
        dashBoardMapLanding?.hideAllToolTipCarPark()
        resetTrackerMode()
        setToFollowingMode {}
    }


    /**
     * setup style of map
     */
    private fun initStyle(basicMapStyleReqd: Boolean = false, after: () -> Unit = {}) {
        unloadStyleObservers()
        (activity as? DashboardActivity)?.let { act ->
            mapboxMap.loadStyleUri(
                act.getMapboxStyle(basicMapStyleReqd)
            ) { style ->

                if (isFragmentDestroyed()) {
                    return@loadStyleUri
                }

                /**
                 * init maplanding cruise mode
                 */
                initDashBoardMapLanding()

                MapIcon.CommonMapIcon.forEach { (key, value) ->
                    value.toBitmap(requireContext())?.let {
                        style.addImage(key, it, false)
                    }
                }


                callERP(style)

                getLatestIncidentData()
                after()
            }

        }


    }


    /**
     * get data for incident data
     */
    private fun getLatestIncidentData() {
        val data = breezeIncidentsUtil.getLatestIncidentData()

        if (!data.isNullOrEmpty() && mapboxMap.getStyle() != null) {
            displayIncidentData(mapboxMap.getStyle()!!, data)
        }
        refreshIncidentData()
    }

    private fun refreshIncidentData() {
        compositeDisposable.add(
            RxBus.listen(RxEvent.IncidentDataRefresh::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    if (::mapboxMap.isInitialized && mapboxMap.getStyle() != null && mapboxMap.isFullyLoaded()) {
                        displayIncidentData(mapboxMap.getStyle()!!, it.incidentData)
                    }
                })
    }


    private fun displayIncidentData(style: Style, incidentMap: HashMap<String, FeatureCollection>) {
        TrafficIncidentData.values().forEach {
            val layerId = it.type + Constants.LAYER
            val sourceId = it.type + Constants.SOURCE
            val imgId = it.type + Constants.IMAGE

            style.removeStyleLayer(layerId)
            style.removeStyleSource(sourceId)

            if (incidentMap[it.type] != null) {
                addIncidentSymbolLayer(
                    style,
                    sourceId,
                    layerId,
                    imgId,
                    incidentMap[it.type]!!
                )
            }
        }
    }


    private fun addIncidentSymbolLayer(
        style: Style,
        sourceName: String,
        layerName: String,
        imgName: String,
        collection: FeatureCollection
    ) {
        val icon_size_exp = Expression.interpolate {
            linear()
            zoom()
            stop {
                literal(0.0)
                literal(0.0)
            }
            stop {
                literal(9.0)
                literal(0.0)
            }
            stop {
                literal(12.9)
                literal(0.0)
            }
            stop {
                literal(13.0)
                literal(0.8)
            }
            stop {
                literal(16.5)
                literal(1.0)
            }
        }

        style.addSource(
            geoJsonSource(sourceName) {
                featureCollection(collection)
            }
        )

        if (style.getLayer(Constants.ILLEGAL_PARKING_CAMERA) != null) {
            style.addLayerAbove(
                symbolLayer(layerName, sourceName) {
                    iconImage(
                        imgName
                    )
                    iconSize(icon_size_exp)
                    iconAnchor(IconAnchor.CENTER)
                    iconAllowOverlap(true)
                }, Constants.ILLEGAL_PARKING_CAMERA
            )
        } else {
            style.addLayer(
                symbolLayer(layerName, sourceName) {
                    iconImage(
                        imgName
                    )
                    iconSize(icon_size_exp)
                    iconAnchor(IconAnchor.CENTER)
                    iconAllowOverlap(true)
                }
            )
        }
    }


    /**
     * for ERP cruise mode
     */
    private fun callERP(style: Style) {
        if (breezeERPUtil.getCurrentERPData() != null) {
            displayERP(style, breezeERPUtil.getCurrentERPData()!!)
        } else {
            //Timber.d( "ERP data was null: ")
        }

        compositeDisposable.add(
            RxBus.listen(RxEvent.ERPRefresh::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    Timber.d("Dashboard: Received rxevent for ERP refresh")
                    if (::mapboxMap.isInitialized && mapboxMap.getStyle() != null && mapboxMap.isFullyLoaded()) {
                        it.erpFeature?.let { erpFeature ->
                            displayERP(
                                mapboxMap.getStyle()!!,
                                erpFeature
                            )
                        }
                    }
                })
    }


    private fun displayERP(style: Style, erpFeatures: ERPFeatures) {

        //Removing all the source and layers relaated to ERP
        style.removeStyleLayer(Constants.ERP_LAYER)
        style.removeStyleSource(Constants.ERP_GEOJSONSOURCE)
        val erpFeatureCollection = FeatureCollection.fromFeatures(erpFeatures.featureListERP)

        val icon_size_exp = Expression.interpolate {
            linear()
            zoom()
            stop {
                literal(0.0)
                literal(0.0)
            }
            stop {
                literal(9.0)
                literal(0.0)
            }
            stop {
                literal(12.9)
                literal(0.0)
            }
            stop {
                literal(13.0)
                literal(0.8)
            }
            stop {
                literal(16.4)
                literal(1.0)
            }
        }

        val text_size_exp = Expression.interpolate {
            linear()
            zoom()
            stop {
                literal(0.0)
                literal(0.0)
            }
            stop {
                literal(9.0)
                literal(0.0)
            }
            stop {
                literal(12.9)
                literal(0.0)
            }
            stop {
                literal(13.0)
                literal(14.0)
            }
            stop {
                literal(16.4)
                literal(18.0)
            }
        }

        style.addSource(
            geoJsonSource(Constants.ERP_GEOJSONSOURCE) {
                featureCollection(erpFeatureCollection!!)
            }
        )

        style.addLayer(
            symbolLayer(Constants.ERP_LAYER, Constants.ERP_GEOJSONSOURCE) {
                iconImage(
                    Constants.ERP_ICON
                )
                iconSize(icon_size_exp)
                iconAnchor(IconAnchor.BOTTOM)
                iconAllowOverlap(true)

                textFont(listOf("SF Pro Display Medium"))
                textField(Expression.get(Constants.ERP.ERP_RATE))
                activity?.let {
                    textColor(it.getColor(R.color.themed_erp_price_text))
                }

                textAnchor(TextAnchor.CENTER)
                textOffset(listOf(0.0, -1.4))
                textSize(text_size_exp)
                iconIgnorePlacement(true)
            }
        )
    }

    private fun handleShowNotyNoAmenitiesIn500M(type: String) {

        activity?.let {
            if (BreezeUserPreference.getInstance(it).isInAppGuideTutorialPending()) {
                return
            }
            val popupName: String
            val eventValue: String
            val name = when (type) {
                CARPARK -> {
                    popupName = Event.NO_PARKING_VICINITY
                    eventValue = Event.CLOSE_POPUP_PARKING_VICINITY
                    "carpark"
                }

                PETROL -> {
                    popupName = Event.NO_PETROL_VICINITY
                    eventValue = Event.CLOSE_POPUP_NO_PETROL_VICINITY
                    "petrol station"
                }

                EVCHARGER -> {
                    popupName = Event.NO_EV_VICINITY
                    eventValue = Event.CLOSE_POPUP_NO_EV_VICINITY

                    "EV charger"
                }

                else -> return
            }

            if (dashBoardMapLanding != null) {
                if (popupName.isNotEmpty()) {
                    Analytics.logPopupEvent(
                        popupName,
                        Event.POPUP_OPEN2,
                        Screen.OBU_DISPLAY_MODE
                    )
                }


                GlobalNotification().setIcon(R.drawable.ic_no_data_warning)
                    .setTheme(GlobalNotification.ThemeTutorial)
                    .setTitle(getString(R.string.no_available_data))
                    .setDescription(getString(R.string.msg_no_available_data, name))
                    .setActionText(null).setActionListener(null).setDismissDurationSeconds(10)
                    .setSeeListener { isAutoDismiss, isDismissWithoutAction ->
                        if (!isAutoDismiss && isDismissWithoutAction) {
                            if (eventValue.isNotEmpty()) {
                                Analytics.logClickEvent(
                                    eventValue,
                                    Screen.OBU_DISPLAY_MODE
                                )
                            }
                        }
                    }
                    .show(requireActivity(), 20)
            }

        }
    }

    private fun unloadStyleObservers() {
        if (mapboxMap.getStyle() != null) {
            breezeRoadObjectHandler.removeBreezeNotificationStyle(mapboxMap.getStyle()!!)
        }
    }

    /**
     * automatic start cruise mode
     */
    @SuppressLint("MissingPermission")
    private fun triggerCruiseMode() {
        Timber.d("Cruise mode has been triggered")
        Timber.d("Init style called")
        initStyle(basicMapStyleReqd = true) {

            activity?.let { act ->
                locationComponent = viewBinding.mapViewCruiseMode.location.apply {
                    enabled = true
                    pulsingEnabled = true
                }

                Timber.d("Internal unit function called")
                TripLoggingManager.getInstance()
                    ?.startTripLogging(TripType.OBULITE, getScreenName(), null)
                activity?.volumeControlStream = AudioManager.STREAM_MUSIC
                VoiceInstructionsManager.setupVoice(act)
                viewBinding.root.keepScreenOn = true

                //Resetting Camera tracker settings
                resetTrackerMode()

                locationComponent.setLocationProvider(navigationLocationProvider)
                locationComponent.locationPuck = LocationPuck2D(
                    bearingImage = ContextCompat.getDrawable(
                        act,
                        R.drawable.ic_navigaion_puck
                    ),
                    scaleExpression = interpolate {
                        linear()
                        zoom()
                        stop {
                            literal(0.0)
                            literal(0.6)
                        }
                        stop {
                            literal(20.0)
                            literal(1.0)
                        }
                    }.toJson()
                )
                locationComponent.apply {
                    pulsingColor = act.getColor(R.color.themed_nav_pulse_color)
                    pulsingMaxRadius = 50.0F
                }
                currentLocation?.let { location -> viewportDataSource.onLocationChanged(location) }

//                mViewModel.sendSecondaryFirebaseID(userPreferenceUtil.retrieveUserName())

                MapboxNavigationApp.current()?.let { mapboxNavigation ->
                    ClientFactory.clearMutationCache()
                    mapboxNavigation.registerTripSessionStateObserver(mapboxHistoryRecorder)
                    mapboxNavigation.startTripSession()
                    mapboxNavigation.registerTripSessionStateObserver(tripSessionStateObserver)
                }
                startCruiseControllerService()
                setToFollowingMode {}
                lifecycleScope.launch(Dispatchers.IO) {
                    breezeFeatureDetectionUtil.initFeatureDetectionRoadObjects()
                }

                registerEhorizonNotificationObservers(mapboxMap.getStyle()!!)
            }

        }
    }

    private fun startCruiseControllerService() {
        requireContext().startService(Intent(requireContext(), CruiseControllerService::class.java))
    }

//
//    override suspend fun generateNavigationLogData(filePath: String) {
//        val userId = userPreferenceUtil.getUserStoredData()?.cognitoID ?: "unknown_user"
//        val uploadWorkRequest = OneTimeWorkRequestBuilder<WorkerNavigationLogFileUpload>()
//            .setInputData(
//                Data.Builder()
//                    .putString(WorkerNavigationLogFileUpload.KEY_FILE_PATH, filePath)
//                    .putString(WorkerNavigationLogFileUpload.KEY_USER_ID, userId)
//                    .putString(WorkerNavigationLogFileUpload.KEY_DESCRIPTION, "OBUDisplay")
//                    .build()
//            ).setConstraints(
//                Constraints.Builder().setRequiredNetworkType(NetworkType.CONNECTED).build()
//            )
//            .build()
//
//        context?.let { WorkManager.getInstance(it).enqueue(uploadWorkRequest) }
//
////        Timber.d("filepath=${filePath}")
////        context?.let {
////            breezeHistoryRecorderUtil.saveHistoryData(
////                it,
////                "CruiseMode", filePath
////            )
////        }
//    }


    @OptIn(ExperimentalPreviewMapboxNavigationAPI::class)
    private val tripSessionStateObserver = TripSessionStateObserver { tripSessionState ->
        when (tripSessionState) {
            TripSessionState.STARTED -> {
                handleCruiseModeStartEvent()
            }

            TripSessionState.STOPPED -> {
                cruiseModeStopped()
            }
        }
    }

    private fun handleCruiseModeStartEvent() {
        Analytics.logClickEvent(Event.START_CRUISE, getScreenName())
        monitorLocationUpdates = false
        updateViewOnNavigationStateChange(true)
    }


    private fun updateViewOnNavigationStateChange(navigationStarted: Boolean) {
//        if (navigationStarted) {
//            val listener = TransitionEndListener { }
//            setToFollowingMode(listener)
//        } else {
//
//            viewportDataSource.overviewPadding = overviewEdgeInsets
//            viewportDataSource.evaluate()
//            navigationCamera.requestNavigationCameraToOverview {
//                val cameraOptions = CameraOptions.Builder()
//                    .zoom(FOLLOW_MAX_ZOOM)
//                    .build()
//                viewBinding.mapViewCruiseMode.getMapboxMap().easeTo(cameraOptions)
//            }
//
//
//        }
    }


    private fun setToFollowingMode(listener: TransitionEndListener?) {
//        viewportDataSource.options.followingFrameOptions.zoomUpdatesAllowed = true
        viewportDataSource.followingZoomPropertyOverride(16.5)
        viewportDataSource.followingPitchPropertyOverride(50.0)
        viewportDataSource.followingPadding = followingBottomEdgeInsets
        viewportDataSource.evaluate()
        navigationCamera.requestNavigationCameraToFollowing(transitionEndListener = listener)
    }

    private fun setToOverViewMode(points: List<Point>) {
        with(viewportDataSource) {
            overviewBearingPropertyOverride(currentLocation?.bearing?.toDouble() ?: 0.0)
            additionalPointsToFrameForOverview(points)
            overviewPitchPropertyOverride(0.0)
            overviewPadding = overviewEdgeInsets
            viewportDataSource.evaluate()
        }
        navigationCamera.requestNavigationCameraToOverview(
            NavigationCameraTransitionOptions.Builder().maxDuration(1000L).build(),
            NavigationCameraTransitionOptions.Builder().maxDuration(700L).build()
        ) {}
    }

    private fun handleCruiseModeEndEvent() {
        if (isFragmentDestroyed()) {
            return
        }
        Analytics.logClickEvent(Event.END_CRUISE, getScreenName())
        monitorLocationUpdates = true
        cruiseModeStopped()
    }


    @OptIn(ExperimentalPreviewMapboxNavigationAPI::class)
    private fun cruiseModeStopped() {
        SpeedLimitUtil.gc()
        ETAEngine.getInstance()!!.stopETA(false)
        breezeFeatureDetectionUtil.stopFeatureDetection()
        TripLoggingManager.getInstance()?.stopTripLogging(
            null,
            getScreenName(),
            bookmarkId = carParkLocationSelected?.bookmarkId,
            bookmarkName = carParkLocationSelected?.name
        )
        breezeRoadObjectHandler.removeLastNotification(
            mapboxMap.getStyle(),
            breezeNotificationObserver
        );
        BreezeEHorizonProvider.destroy()
        VoiceInstructionsManager.getInstance()?.cancelSpeech()
        viewBinding.root.keepScreenOn = false
        //unregister tripSessionStateObserver before stopping trip session to prevent end cruise to be invoked multiple times
        MapboxNavigationApp.current()?.let { mapboxNavigation ->
            mapboxNavigation.unregisterTripSessionStateObserver(tripSessionStateObserver)
            if (!mapboxNavigation.isDestroyed) {
                mapboxNavigation.stopTripSession()
                mapboxNavigation.resetTripSession { }
            }
        }
        //unregister other observers after stopping trip session
        stopMonitoringCruiseMode()

    }

    private fun stopMonitoringCruiseMode() {
        MapboxNavigationApp.current()?.let { mapboxNavigation ->
            mapboxNavigation.unregisterLocationObserver(locationObserver)
            mapboxNavigation.unregisterLocationObserver(mapMatcherResultObserver)
            mapboxNavigation.unregisterTripSessionStateObserver(mapboxHistoryRecorder)
            SpeedLimitUtil.removeLocationObserver(mapboxNavigation)
        }
    }

    private fun resetTrackerMode() {
        isCameraTrackingDismissed = false
        viewBinding.layoutMapController.btnRecenterMap.isInvisible = true
    }

    private fun registerEhorizonNotificationObservers(style: Style) {
        unregisterEhorizonNotificationObservers(style)
        activity?.let {
            breezeRoadObjectHandler = BreezeEHorizonProvider.create(
                it,
                breezeERPUtil,
                breezeIncidentsUtil,
                EHorizonMode.CRUISE,
                null
            )
            breezeRoadObjectHandler.addBreezeEdgeMetaDataObserver(breezeEdgeMetaDataObserver)
            breezeRoadObjectHandler.addBreezeNotificationObserver(breezeNotificationObserver)
            breezeRoadObjectHandler.addBreezeNotificationStyle(style)
        }
    }

    private fun unregisterEhorizonNotificationObservers(style: Style) {
        breezeRoadObjectHandler.removeBreezeEdgeMetaDataObserver(breezeEdgeMetaDataObserver)
        breezeRoadObjectHandler.removeBreezeNotificationObserver(breezeNotificationObserver)
        breezeRoadObjectHandler.removeBreezeNotificationStyle(style)
    }


    private fun initRoadNotificationObserver() {
        breezeNotificationObserver = object : BreezeNotificationObserver {
            override fun showNotificationView(
                zone: NavigationZone,
                distance: Int,
                mERPName: String?,
                chargeAmount: String?,
                roadObjectID: String,
                location: String?
            ) {
                showNotificationViewCruise(
                    zone,
                    distance,
                    mERPName,
                    chargeAmount,
                    location
                )
            }

            override fun removeNotificationView(roadObjectId: String) {}
        }
    }


    private fun showNotificationViewCruise(
        zone: NavigationZone,
        distance: Int,
        mERPName: String?,
        chargeAmount: String?,
        location: String?
    ) {
        val fragment = childFragmentManager.findFragmentByTag(OBULiteMapAlertsFragment.TAG)
        (fragment as? OBULiteMapAlertsFragment)?.showNotificationViewCruise(
            zone,
            distance,
            mERPName,
            chargeAmount,
            location
        )
    }

    override fun inflateViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?,
        attachToContainer: Boolean
    ) = FragmentObuLiteBinding.inflate(inflater, container, attachToContainer)

    override fun getViewModelReference(): OBULiteFragmentViewModel {
        return mViewModel
    }

    override fun getScreenName(): String {
        return Screen.OBU_DISPLAY_MODE
    }

    override fun onDestroyView() {
        viewBinding.mapViewCruiseMode.location.apply {
            enabled = false
        }
        super.onDestroyView()
        removeAmenitiesHandler?.removeCallbacksAndMessages(null)
        removeAmenitiesHandler = null
        obuCheckSpeedHelper?.destroy()
        obuCheckSpeedHelper = null
        activity?.volumeControlStream = AudioManager.USE_DEFAULT_STREAM_TYPE
        postEventWithCacheLast(AppToHomeTurnOffCruiseModeEvent())
        (activity as? DashboardActivity)?.checkForUpdates()
        MapboxNavigationApp.current()
            ?.takeIf { !it.isDestroyed }?.run {
                stopTripSession()
                resetTripSession {}
            }
        ETAEngine.getInstance()?.gc()
        SpeedLimitUtil.gc()
        carEventListener?.let {
            compositeDisposable.remove(it)
        }
        compositeDisposable.dispose()
        lifecycleScope.cancel()
    }


    /**
     * handle event from car
     */
    fun onEventFromCar(event: ToAppRxEvent) {

        when (event) {
            is UpdateStateVoiceApp -> {
                handleMuteToggleEvent()
            }

//            is StopOBULiteDisplay -> {
//                if (isViewLoaded) {
//                    handleCruiseModeEndEvent()
//                    onBackPressed()
//                } else {
//                    shouldTerminateView = true
//                }
//            }

            // BREEZE2-1845
//            is UpdateETAPhone -> {
//                viewBinding.etaToast.visibility = View.GONE
//                handleETALiveLocationToggleEvent()
//            }
//
//            is ETAStartedPhone -> {
//                handleETAStartedEvent()
//            }
//
//            is ETAMessageReceivedPhone -> {
//                showETAMessage(event.etaMessage)
//            }

            else -> {}
        }
    }

    private fun handleMuteToggleEvent() {
        with(viewBinding) {
            if (VoiceInstructionsManager.getInstance()?.isUserMute == true) {
                layoutMapController.btnToggleVoice.setImageResource(R.drawable.mute_on)
                Analytics.logEvent(Event.USER_CLICK, Event.OBU_MUTE_OFF, Screen.OBU_DISPLAY_MODE)
                Analytics.logClickEvent(Event.UNMUTE, getScreenName())
            } else {
                layoutMapController.btnToggleVoice.setImageResource(R.drawable.mute_off)
                Analytics.logEvent(Event.USER_CLICK, Event.OBU_MUTE_ON, Screen.OBU_DISPLAY_MODE)
                Analytics.logClickEvent(Event.MUTE, getScreenName())
            }
        }

    }

    private fun initMuteButtonState() {
        with(viewBinding) {
            if (VoiceInstructionsManager.getInstance()?.isUserMute == true) {
                layoutMapController.btnToggleVoice.setImageResource(R.drawable.mute_on)
            } else {
                layoutMapController.btnToggleVoice.setImageResource(R.drawable.mute_off)
            }
        }
    }


    //BREEZE2-1845
//    private fun announceShareDriveStarted() {
//        if (!shareDriveAnnounced) {
//            shareDriveAnnounced = true
//            val message = getString(
//                R.string.eta_start_voice_message,
//                ETAEngine.getInstance()?.getETARecipientName()
//            )
//            VoiceInstructionsManager.getInstance()?.playVoiceInstructions(message)
//        }
//    }

    fun toggleControllersLayoutVisibility(isVisible: Boolean) {
        activity?.runOnUiThread {
            viewBinding.layoutMapController.root.isVisible = isVisible
        }
    }

    // BREEZE2-1845
//    private fun handleETAStartedEvent() {
//        if (ETAEngine.getInstance()!!.isLiveLocationSharingEnabled()) {
//            viewBinding.layoutMapController.ETAView.setVisibleETA()
//            ETAEngine.getInstance()!!.updateETAStatus(ETAStatus.INPROGRESS)
//        }
//        viewBinding.layoutMapController.ETAView.updateUIOnlyForETA()
//        announceShareDriveStarted()
//    }
//
//    private fun handleETAFirstTime() {
//        if (ETAEngine.getInstance()!!.isETADisabled()) {
//            viewBinding.layoutMapController.ETAView.updateUIOnlyForETA()
//            viewBinding.layoutMapController.ETAView.ivEtaStatus?.setOnClickListener {
//                Analytics.logClickEvent(
//                    Event.OBU_DISPLAY_CLICK_SHARE_DRIVE,
//                    Screen.OBU_DISPLAY_MODE
//                )
//                showShareLiveLocation("")
//            }
//        }
//    }
//
//    private fun handleETALiveLocationToggleEvent() {
//        if (ETAEngine.getInstance()!!.isETARunning()) {
//            Analytics.logClickEvent(Event.ETA_RESUME, getScreenName())
//            showETAMessage(getString(R.string.eta_liveloc_started), false)
//        } else {
//            Analytics.logClickEvent(Event.ETA_PAUSE, getScreenName())
//            showETAMessage(getString(R.string.eta_liveloc_paused), true)
//        }
//        viewBinding.layoutMapController.ETAView.updateUIOnlyForETA()
//    }

    private fun showETAMessage(message: String, showPausedImage: Boolean = false) {
        val iconEta = if (showPausedImage) {
            R.drawable.eta_toast_live_location_paused
        } else {
            R.drawable.eta_toast_live_location
        }
        viewBinding.etaToast.setData(message, iconEta)
        viewBinding.etaToast.show()
    }

    private fun showToastMessage(message: String) {
        (activity as? DashboardActivity)?.showToast(message)
    }

    private fun showShareLiveLocation(messsage: String?) {
        val navigationParams = requireActivity().getRNFragmentNavigationParams(
            sessionToken = (activity as? DashboardActivity)?.myServiceInterceptor?.getSessionToken(),
            fragmentTag = RNScreen.NOTIFY_ARRIVAL
        ).apply {
            putString("etamessage", messsage)
            putString(Constants.RN_FROM_SCREEN, Screen.CRUISE_MODE)
        }
        val initialProperties = Bundle().apply {
            putString(Constants.RN_TO_SCREEN, RNScreen.NOTIFY_ARRIVAL)
            putBundle(Constants.RN_CONSTANTS.NAVIGATION_PARAMS, navigationParams)
        }

        val reactNativeFragment = CustomReactFragment.newInstance(
            componentName = Constants.RN_CONSTANTS.COMPONENT_NAME,
            launchOptions = initialProperties,
            cleanOnDestroyed = false
        )

        childFragmentManager
            .beginTransaction()
            .add(R.id.frBottomDetail, reactNativeFragment, RNScreen.NOTIFY_ARRIVAL)
            .commit()
    }

    private fun showCarparkListScreen(lat: Double, lng: Double) {
        val navigationParams = requireActivity().getRNFragmentNavigationParams(
            sessionToken = (activity as? DashboardActivity)?.myServiceInterceptor?.getSessionToken(),
            fragmentTag = RNScreen.CARPARK_LIST
        ).apply {
            putParcelable(
                "location", bundleOf(
                    "lat" to lat.toString(),
                    "long" to lng.toString()
                )
            )
            putString(Constants.RN_FROM_SCREEN, Screen.CRUISE_MODE)
        }
        val initialProperties = Bundle().apply {
            putString(Constants.RN_TO_SCREEN, RNScreen.CARPARK_LIST)
            putBundle(Constants.RN_CONSTANTS.NAVIGATION_PARAMS, navigationParams)
        }

        val reactNativeFragment = CustomReactFragment.newInstance(
            componentName = Constants.RN_CONSTANTS.COMPONENT_NAME,
            launchOptions = initialProperties,
            cleanOnDestroyed = false
        )

        childFragmentManager
            .beginTransaction()
            .replace(R.id.frCarparkList, reactNativeFragment, RNScreen.CARPARK_LIST)
            .commit()
        viewBinding.frCarparkList.isVisible = true
    }

    private fun closeCarparkListScreen() {
        with(childFragmentManager) {
            findFragmentByTag(RNScreen.CARPARK_LIST)?.let {
                beginTransaction().remove(it).runOnCommit {
//                    val fragment = findFragmentById(viewBinding.layoutOBUDisplayContainerRN.id)
//                    (fragment as? CustomReactFragment)?.reactDelegate?.onHostResume()
                }
            }
        }
        viewBinding.frCarparkList.isVisible = false
    }

    private fun updateCarparkListButtonState(disable: Boolean) {
        activity?.getApp()?.shareBusinessLogicHelper?.toggleButtonCarparkList(disable)
    }

    fun closeEtaPopUp() {
        childFragmentManager.findFragmentByTag(RNScreen.NOTIFY_ARRIVAL)?.let {
            childFragmentManager.beginTransaction().remove(it).commitNow()
        }
    }
}
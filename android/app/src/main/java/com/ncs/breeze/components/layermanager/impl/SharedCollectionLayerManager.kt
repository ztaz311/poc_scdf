package com.ncs.breeze.components.layermanager.impl

import com.breeze.model.SharedCollection
import com.breeze.model.SharedLocation
import com.breeze.model.constants.AmenityType
import com.breeze.model.enums.AmenityBand
import com.breeze.model.extensions.toBitmap
import com.google.gson.JsonObject
import com.mapbox.geojson.Feature
import com.mapbox.geojson.Point
import com.mapbox.maps.MapView
import com.ncs.breeze.R
import com.ncs.breeze.common.utils.ParkingIconUtils
import com.ncs.breeze.components.layermanager.MarkerLayerManager
import com.ncs.breeze.components.marker.markerview2.SharedLocationMapTooltip

class SharedCollectionLayerManager(mapView: MapView) : MarkerLayerManager(mapView) {
    companion object {
        const val TYPE_LOCATION = "location"
        const val TYPE_CARPARK_TYPE0 = "carpark-type0"
        const val TYPE_CARPARK_TYPE0_BOOKMARK = "carpark-type0-bookmark"
        const val TYPE_CARPARK_TYPE1 = "carpark-type1"
        const val TYPE_CARPARK_TYPE1_BOOKMARK = "carpark-type1-bookmark"
        const val TYPE_CARPARK_TYPE2 = "carpark-type2"
        const val TYPE_CARPARK_TYPE2_BOOKMARK = "carpark-type2-bookmark"
        const val TYPE_EVCHARGER = "evcharger"
        const val TYPE_EVCHARGER_BOOKMARK = "evcharger-bookmark"
        const val TYPE_PETROL = "petrol"
        const val TYPE_PETROL_BOOKMARK = "petrol-bookmark"
        const val PROP_LIST_INDEX = "list-index"
    }


    var onToggleIconSelect: (selectedIndex: Int) -> Unit = {}
    private var currentSelectedIndex = -1

    init {
        loadStyleImages(mapView)
        markerLayerClickHandler = object : MarkerLayerClickHandler {
            override fun handleOnclick(
                layerType: String,
                feature: Feature,
                currentMarker: MarkerViewType?
            ) {
                val iconIndex = feature.getNumberProperty(PROP_LIST_INDEX)?.toInt()
                onToggleIconSelect.invoke(
                    if (iconIndex == null || currentSelectedIndex == iconIndex) -1 else iconIndex
                )
            }

            override fun handleOnFreeMapClick(point: Point) {
                onToggleIconSelect.invoke(-1)
            }

        }
    }

    override fun shouldClearMarkersOnMapFreeClick() = false

    private fun loadStyleImages(mapView: MapView) {
        val ids = mapOf(
            "$TYPE_LOCATION-${ParkingIconUtils.ImageIconType.SELECTED.type}$ICON_POSTFIX" to R.drawable.destination_puck,
            "$TYPE_LOCATION-${ParkingIconUtils.ImageIconType.UNSELECTED.type}$ICON_POSTFIX" to R.drawable.destination_puck,
            "$TYPE_LOCATION-${ParkingIconUtils.ImageIconType.SELECTED_NIGHT.type}$ICON_POSTFIX" to R.drawable.destination_puck,
            "$TYPE_LOCATION-${ParkingIconUtils.ImageIconType.UNSELECTED_NIGHT.type}$ICON_POSTFIX" to R.drawable.destination_puck,

            "$TYPE_CARPARK_TYPE0-${ParkingIconUtils.ImageIconType.SELECTED.type}$ICON_POSTFIX" to R.drawable.ic_parkingmarker_type0_nondestination_selected,
            "$TYPE_CARPARK_TYPE0-${ParkingIconUtils.ImageIconType.UNSELECTED.type}$ICON_POSTFIX" to R.drawable.ic_parkingmarker_type0_nondestination_notselected,
            "$TYPE_CARPARK_TYPE0-${ParkingIconUtils.ImageIconType.SELECTED_NIGHT.type}$ICON_POSTFIX" to R.drawable.ic_parkingmarker_type0_nondestination_selected,
            "$TYPE_CARPARK_TYPE0-${ParkingIconUtils.ImageIconType.UNSELECTED_NIGHT.type}$ICON_POSTFIX" to R.drawable.ic_parkingmarker_type0_nondestination_notselected,

            "$TYPE_CARPARK_TYPE0_BOOKMARK-${ParkingIconUtils.ImageIconType.SELECTED.type}$ICON_POSTFIX" to R.drawable.ic_map_carpark_type0_bookmarked_selected,
            "$TYPE_CARPARK_TYPE0_BOOKMARK-${ParkingIconUtils.ImageIconType.UNSELECTED.type}$ICON_POSTFIX" to R.drawable.ic_map_carpark_type0_bookmarked,
            "$TYPE_CARPARK_TYPE0_BOOKMARK-${ParkingIconUtils.ImageIconType.SELECTED_NIGHT.type}$ICON_POSTFIX" to R.drawable.ic_map_carpark_type0_bookmarked_selected,
            "$TYPE_CARPARK_TYPE0_BOOKMARK-${ParkingIconUtils.ImageIconType.UNSELECTED_NIGHT.type}$ICON_POSTFIX" to R.drawable.ic_map_carpark_type0_bookmarked,

            "$TYPE_CARPARK_TYPE1-${ParkingIconUtils.ImageIconType.SELECTED.type}$ICON_POSTFIX" to R.drawable.ic_parkingmarker_type1_nondestination_selected,
            "$TYPE_CARPARK_TYPE1-${ParkingIconUtils.ImageIconType.UNSELECTED.type}$ICON_POSTFIX" to R.drawable.ic_parkingmarker_type1_nondestination_notselected,
            "$TYPE_CARPARK_TYPE1-${ParkingIconUtils.ImageIconType.SELECTED_NIGHT.type}$ICON_POSTFIX" to R.drawable.ic_parkingmarker_type1_nondestination_selected,
            "$TYPE_CARPARK_TYPE1-${ParkingIconUtils.ImageIconType.UNSELECTED_NIGHT.type}$ICON_POSTFIX" to R.drawable.ic_parkingmarker_type1_nondestination_notselected,

            "$TYPE_CARPARK_TYPE1_BOOKMARK-${ParkingIconUtils.ImageIconType.SELECTED.type}$ICON_POSTFIX" to R.drawable.ic_map_carpark_type1_bookmarked,
            "$TYPE_CARPARK_TYPE1_BOOKMARK-${ParkingIconUtils.ImageIconType.UNSELECTED.type}$ICON_POSTFIX" to R.drawable.ic_map_carpark_type1_bookmarked_selected,
            "$TYPE_CARPARK_TYPE1_BOOKMARK-${ParkingIconUtils.ImageIconType.SELECTED_NIGHT.type}$ICON_POSTFIX" to R.drawable.ic_map_carpark_type1_bookmarked,
            "$TYPE_CARPARK_TYPE1_BOOKMARK-${ParkingIconUtils.ImageIconType.UNSELECTED_NIGHT.type}$ICON_POSTFIX" to R.drawable.ic_map_carpark_type1_bookmarked_selected,

            "$TYPE_CARPARK_TYPE2-${ParkingIconUtils.ImageIconType.SELECTED.type}$ICON_POSTFIX" to R.drawable.ic_parkingmarker_type2_nondestination_selected,
            "$TYPE_CARPARK_TYPE2-${ParkingIconUtils.ImageIconType.UNSELECTED.type}$ICON_POSTFIX" to R.drawable.ic_parkingmarker_type2_nondestination_notselected,
            "$TYPE_CARPARK_TYPE2-${ParkingIconUtils.ImageIconType.SELECTED_NIGHT.type}$ICON_POSTFIX" to R.drawable.ic_parkingmarker_type2_nondestination_selected,
            "$TYPE_CARPARK_TYPE2-${ParkingIconUtils.ImageIconType.UNSELECTED_NIGHT.type}$ICON_POSTFIX" to R.drawable.ic_parkingmarker_type2_nondestination_notselected,

            "$TYPE_CARPARK_TYPE2_BOOKMARK-${ParkingIconUtils.ImageIconType.SELECTED.type}$ICON_POSTFIX" to R.drawable.ic_map_carpark_type2_bookmarked_selected,
            "$TYPE_CARPARK_TYPE2_BOOKMARK-${ParkingIconUtils.ImageIconType.UNSELECTED.type}$ICON_POSTFIX" to R.drawable.ic_map_carpark_type2_bookmarked,
            "$TYPE_CARPARK_TYPE2_BOOKMARK-${ParkingIconUtils.ImageIconType.SELECTED_NIGHT.type}$ICON_POSTFIX" to R.drawable.ic_map_carpark_type2_bookmarked_selected,
            "$TYPE_CARPARK_TYPE2_BOOKMARK-${ParkingIconUtils.ImageIconType.UNSELECTED_NIGHT.type}$ICON_POSTFIX" to R.drawable.ic_map_carpark_type2_bookmarked,

            "$TYPE_EVCHARGER-${ParkingIconUtils.ImageIconType.SELECTED.type}$ICON_POSTFIX" to R.drawable.ic_map_evcharger_selected,
            "$TYPE_EVCHARGER-${ParkingIconUtils.ImageIconType.UNSELECTED.type}$ICON_POSTFIX" to R.drawable.ic_map_evcharger,
            "$TYPE_EVCHARGER-${ParkingIconUtils.ImageIconType.SELECTED_NIGHT.type}$ICON_POSTFIX" to R.drawable.ic_map_evcharger_selected,
            "$TYPE_EVCHARGER-${ParkingIconUtils.ImageIconType.UNSELECTED_NIGHT.type}$ICON_POSTFIX" to R.drawable.ic_map_evcharger,

            "$TYPE_EVCHARGER_BOOKMARK-${ParkingIconUtils.ImageIconType.SELECTED.type}$ICON_POSTFIX" to R.drawable.ic_map_evcharger_bookmarked_selected,
            "$TYPE_EVCHARGER_BOOKMARK-${ParkingIconUtils.ImageIconType.UNSELECTED.type}$ICON_POSTFIX" to R.drawable.ic_map_evcharger_bookmarked,
            "$TYPE_EVCHARGER_BOOKMARK-${ParkingIconUtils.ImageIconType.SELECTED_NIGHT.type}$ICON_POSTFIX" to R.drawable.ic_map_evcharger_bookmarked_selected,
            "$TYPE_EVCHARGER_BOOKMARK-${ParkingIconUtils.ImageIconType.UNSELECTED_NIGHT.type}$ICON_POSTFIX" to R.drawable.ic_map_evcharger_bookmarked,

            "$TYPE_PETROL-${ParkingIconUtils.ImageIconType.SELECTED.type}$ICON_POSTFIX" to R.drawable.ic_map_petrol_selected,
            "$TYPE_PETROL-${ParkingIconUtils.ImageIconType.UNSELECTED.type}$ICON_POSTFIX" to R.drawable.ic_map_petrol,
            "$TYPE_PETROL-${ParkingIconUtils.ImageIconType.SELECTED_NIGHT.type}$ICON_POSTFIX" to R.drawable.ic_map_petrol_selected,
            "$TYPE_PETROL-${ParkingIconUtils.ImageIconType.UNSELECTED_NIGHT.type}$ICON_POSTFIX" to R.drawable.ic_map_petrol,

            "$TYPE_PETROL_BOOKMARK-${ParkingIconUtils.ImageIconType.SELECTED.type}$ICON_POSTFIX" to R.drawable.ic_map_petrol_bookmarked_selected,
            "$TYPE_PETROL_BOOKMARK-${ParkingIconUtils.ImageIconType.UNSELECTED.type}$ICON_POSTFIX" to R.drawable.ic_map_petrol_bookmarked,
            "$TYPE_PETROL_BOOKMARK-${ParkingIconUtils.ImageIconType.SELECTED_NIGHT.type}$ICON_POSTFIX" to R.drawable.ic_map_petrol_bookmarked_selected,
            "$TYPE_PETROL_BOOKMARK-${ParkingIconUtils.ImageIconType.UNSELECTED_NIGHT.type}$ICON_POSTFIX" to R.drawable.ic_map_petrol_bookmarked,
        )
        mapView.getMapboxMap().getStyle { style ->
            ids.forEach { (key, drawableRes) ->
                drawableRes.toBitmap(mapView.context)?.let { bitmap ->
                    style.addImage(key, bitmap)
                }
            }
        }
    }

    fun refreshMapMarkers(sharedCollection: SharedCollection, selectedIndex: Int) {
        val iconProperties = arrayListOf<IconProperties>()
        sharedCollection.bookmarks?.forEachIndexed { index, sharedLocation ->
            val lat = sharedLocation.lat.toDoubleOrNull()
            val long = sharedLocation.long.toDoubleOrNull()
            var point: Point? = null
            if (lat != null && long != null)
                point = Point.fromLngLat(long, lat)

            val markerProperties = JsonObject()
            val selectedState =
                if (selectedIndex == index) ParkingIconUtils.ImageIconType.SELECTED.type
                else ParkingIconUtils.ImageIconType.UNSELECTED.type
            markerProperties.addProperty(HIDDEN, false)
            markerProperties.addProperty(IMAGE_TYPE, "")
            markerProperties.addProperty(ICON_KEY, selectedState)
            markerProperties.addProperty(PROP_LIST_INDEX, index)
            iconProperties.add(
                IconProperties(
                    type = getSharedLocationType(sharedLocation),
                    point = point,
                    properties = markerProperties
                )
            )
        }

        removeAllMarkers()
        iconProperties.filter { it.type == TYPE_CARPARK_TYPE0 }
            .map { Feature.fromGeometry(it.point, it.properties) }
            .let { addOrReplaceMarkers(TYPE_CARPARK_TYPE0, it) }
        iconProperties.filter { it.type == TYPE_CARPARK_TYPE0_BOOKMARK }
            .map { Feature.fromGeometry(it.point, it.properties) }
            .let { addOrReplaceMarkers(TYPE_CARPARK_TYPE0_BOOKMARK, it) }
        iconProperties.filter { it.type == TYPE_CARPARK_TYPE1 }
            .map { Feature.fromGeometry(it.point, it.properties) }
            .let { addOrReplaceMarkers(TYPE_CARPARK_TYPE1, it) }
        iconProperties.filter { it.type == TYPE_CARPARK_TYPE1_BOOKMARK }
            .map { Feature.fromGeometry(it.point, it.properties) }
            .let { addOrReplaceMarkers(TYPE_CARPARK_TYPE1_BOOKMARK, it) }
        iconProperties.filter { it.type == TYPE_CARPARK_TYPE2 }
            .map { Feature.fromGeometry(it.point, it.properties) }
            .let { addOrReplaceMarkers(TYPE_CARPARK_TYPE2, it) }
        iconProperties.filter { it.type == TYPE_CARPARK_TYPE2_BOOKMARK }
            .map { Feature.fromGeometry(it.point, it.properties) }
            .let { addOrReplaceMarkers(TYPE_CARPARK_TYPE2_BOOKMARK, it) }
        iconProperties.filter { it.type == TYPE_EVCHARGER }
            .map { Feature.fromGeometry(it.point, it.properties) }
            .let { addOrReplaceMarkers(TYPE_EVCHARGER, it) }
        iconProperties.filter { it.type == TYPE_EVCHARGER_BOOKMARK }
            .map { Feature.fromGeometry(it.point, it.properties) }
            .let { addOrReplaceMarkers(TYPE_EVCHARGER_BOOKMARK, it) }
        iconProperties.filter { it.type == TYPE_PETROL }
            .map { Feature.fromGeometry(it.point, it.properties) }
            .let { addOrReplaceMarkers(TYPE_PETROL, it) }
        iconProperties.filter { it.type == TYPE_PETROL_BOOKMARK }
            .map { Feature.fromGeometry(it.point, it.properties) }
            .let { addOrReplaceMarkers(TYPE_PETROL_BOOKMARK, it) }
        iconProperties.filter { it.type == TYPE_LOCATION }
            .map { Feature.fromGeometry(it.point, it.properties) }
            .let { addOrReplaceMarkers(TYPE_LOCATION, it) }

        if (!sharedCollection.bookmarks.isNullOrEmpty() && selectedIndex in 0 until sharedCollection.bookmarks!!.size) {
            val selectedSharedLocation = sharedCollection.bookmarks!![selectedIndex]
            showSharedLocationTooltip(selectedSharedLocation)
        } else {
            (marker?.markerView2 as? SharedLocationMapTooltip)?.hideMarker()
        }

    }

    private fun getSharedLocationType(sharedLocation: SharedLocation) = when {
        sharedLocation.amenityType == AmenityType.CARPARK && sharedLocation.availablePercentImageBand == AmenityBand.TYPE0.name ->
            if (sharedLocation.saved) TYPE_CARPARK_TYPE0_BOOKMARK else TYPE_CARPARK_TYPE0

        sharedLocation.amenityType == AmenityType.CARPARK && sharedLocation.availablePercentImageBand == AmenityBand.TYPE1.name ->
            if (sharedLocation.saved) TYPE_CARPARK_TYPE1_BOOKMARK else TYPE_CARPARK_TYPE1

        sharedLocation.amenityType == AmenityType.CARPARK && sharedLocation.availablePercentImageBand == AmenityBand.TYPE2.name ->
            if (sharedLocation.saved) TYPE_CARPARK_TYPE2_BOOKMARK else TYPE_CARPARK_TYPE2

        sharedLocation.amenityType == AmenityType.EVCHARGER ->
            if (sharedLocation.saved) TYPE_EVCHARGER_BOOKMARK else TYPE_EVCHARGER

        sharedLocation.amenityType == AmenityType.PETROL ->
            if (sharedLocation.saved) TYPE_PETROL_BOOKMARK else TYPE_PETROL

        else -> TYPE_LOCATION
    }

    private fun initTooltip(sharedLocation: SharedLocation) {
        val mapView = mapViewRef.get() ?: return
        val tooltip = SharedLocationMapTooltip(mapView)
        marker = MarkerViewType(
            markerView2 = tooltip,
            getSharedLocationType(sharedLocation),
            Feature.fromGeometry(
                Point.fromLngLat(
                    sharedLocation.long.toDoubleOrNull() ?: 0.0,
                    sharedLocation.lat.toDoubleOrNull() ?: 0.0
                )
            )
        )
        mapView.addView(tooltip.mViewMarker)
    }

    private fun showSharedLocationTooltip(sharedLocation: SharedLocation) {
        if (marker?.markerView2 !is SharedLocationMapTooltip
            || marker?.type != getSharedLocationType(sharedLocation)
        ) {
            initTooltip(sharedLocation)
        }
        (marker?.markerView2 as? SharedLocationMapTooltip)
            ?.run {
                bindSharedLocation(sharedLocation)
                updateTooltipPosition()
                showMarker()
            }
    }

    inner class IconProperties(
        val type: String,
        val properties: JsonObject,
        val point: Point?,
    )

}
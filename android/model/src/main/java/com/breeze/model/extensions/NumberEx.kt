package com.breeze.model.extensions

import android.content.Context
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.TypedValue
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.graphics.drawable.toBitmap
import java.math.RoundingMode
import java.text.DecimalFormat


fun Double.round(decimals: Int = 2): String {
    val df = DecimalFormat("#.##")
    df.roundingMode = RoundingMode.HALF_UP
    var str = df.format(this)
    return "%.${decimals}f".format(str.safeDouble())
}

fun Number?.formatNumber() = "%.8f".format(this)

fun Double.visibleAmount(): String {
    return String.format("$%.2f", this)
}

fun Float.roundToAmount(): String {
    return String.format("$%.0f", this)
}

fun Int.toBitmap(context: Context): Bitmap? {
    return AppCompatResources.getDrawable(context, this)?.toBitmap()
        ?: BitmapFactory.decodeResource(context.resources, this)
}


val Number.toPx
    get() = TypedValue.applyDimension(
        TypedValue.COMPLEX_UNIT_DIP,
        this.toFloat(),
        Resources.getSystem().displayMetrics
    )

fun Float.dpToPx() = this * Resources.getSystem().displayMetrics.density
fun Int.dpToPx() = (this.toFloat().dpToPx() + 0.5f).toInt()
fun Double.dpToPx() = this.toFloat().dpToPx().toDouble()

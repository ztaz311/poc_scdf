package com.breeze.model

import com.google.gson.annotations.SerializedName
import com.mapbox.geojson.FeatureCollection
import java.io.Serializable

data class ERPRouteInfo(
    @SerializedName("erpList") public var erpList: List<ERPChargeInfo>,
    @SerializedName("featureCollection") public var featureCollection: FeatureCollection,
    @SerializedName("erpRate") public var erpRate: Float?
)


data class ERPChargeInfo(
    @SerializedName("erpId") val erpId: Int,
    @SerializedName("chargeAmount") val chargeAmount: Float
) : Serializable

data class ERPRouteData(
    @SerializedName("routeId") val routeId: Int,
    @SerializedName("eRPCharges") val eRPCharges: List<ERPChargeInfo>
)

data class ERPFullRouteData(
    @SerializedName("id") val id: String,
    @SerializedName("erpRoutes") val erpRoutes: List<ERPRouteData>,
    @SerializedName("timestamp") val timestamp: Long,
    @SerializedName("source") val source: DestinationAddressDetails,
    @SerializedName("destination") val destination: DestinationAddressDetails
) : Serializable

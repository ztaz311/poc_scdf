package com.breeze.model

/**
 * NORMAL mode, indicates that the route plan is for the current time
 * PLANNED mode, is when user is still planning a new trip for a different date and can save the trip
 * PLANNED_RESUME mode, is when user returns to route planning from a saved trip
 */
enum class RoutePlanningMode {
    NORMAL,
    PLANNED,
    PLANNED_RESUME
}

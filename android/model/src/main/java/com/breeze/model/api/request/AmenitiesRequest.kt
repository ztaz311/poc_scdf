package com.breeze.model.api.request

import com.breeze.model.api.response.AmenitiesPreference
import com.breeze.model.api.response.SubItems
import com.breeze.model.constants.AmenityType.CARPARK
import com.breeze.model.constants.AmenityType.EVCHARGER
import com.breeze.model.constants.AmenityType.PETROL
import com.breeze.model.constants.Constants
import com.google.gson.annotations.SerializedName


class AmenitiesRequest(
    @field:SerializedName("amenities")
    var listAmenities: ArrayList<AmenitieFilter> = arrayListOf(),

    @field:SerializedName("lat")
    var lat: Double? = 1.2746,

    @field:SerializedName("lng")
    var long: Double? = 103.799,

    @field:SerializedName("rad")
    var rad: Double = 1.0,

    @field:SerializedName("maxRad")
    var maxRad: Double? = null,

    @field:SerializedName("arrivalTime")
    var arrivalTime: Long = System.currentTimeMillis() / 1000,

    @field:SerializedName("destName")
    var destName: String = "",

    @field:SerializedName("resultcount")
    var resultCount: Int = Constants.REQUEST_NEARBY_AMENITY_RESULT_COUNT,

    @field:SerializedName("isVoucher")
    var isVoucher: Boolean = false,

    @field:SerializedName("carparkId")
    var carParkId: String? = null,
) {


    fun setListType(listType: Collection<String>, availableBand: String? = null) {
        val listAmenitiesTemp: ArrayList<AmenitieFilter> =
            arrayListOf()
        listType.forEach { value ->
            val amenitieFilter = AmenitieFilter()
            amenitieFilter.type = value
            if (availableBand != null) {
                val amenityFilterTypeData = AmenityFilterTypeData()
                amenityFilterTypeData.availableBand = availableBand
                amenitieFilter.filter = amenityFilterTypeData
            }
            listAmenitiesTemp.add(amenitieFilter)
        }
        listAmenities = listAmenitiesTemp
    }

    fun setAmenitiesPreferenceListType(listType: Collection<AmenitiesPreference>) {
        val listAmenitiesTemp: ArrayList<AmenitieFilter> =
            arrayListOf()
        listType.forEach { amenityPref ->
            var filter: AmenityFilterTypeData? = null
            amenityPref.subItems?.let { subElement ->
                if (amenityPref.elementName == PETROL) {
                    filter = handlePetrolProviderFilter(subElement)
                } else if (amenityPref.elementName == EVCHARGER) {
                    filter = handleEvPlugTypeFilter(subElement)
                }
            }
            listAmenitiesTemp.add(
                AmenitieFilter(
                    amenityPref.elementName,
                    filter
                )
            )
        }
        listAmenities = listAmenitiesTemp
    }

    private fun handlePetrolProviderFilter(
        subElement: List<SubItems>
    ): AmenityFilterTypeData? {
        var filter: AmenityFilterTypeData? = null
        val petrolListSelected = ArrayList<String>()
        val petrolListAll = ArrayList<String>()
        subElement.forEach { provider ->
            provider.elementName?.let {
                petrolListAll.add(it)
                if (provider.isSelected == true) {
                    petrolListSelected.add(it)
                }
            }
        }
        if (petrolListSelected.isNotEmpty()) {
            filter =
                AmenityFilterTypeData(provider = petrolListSelected)
        } else if (petrolListAll.isNotEmpty()) {
            filter = AmenityFilterTypeData(provider = petrolListAll)
        }
        return filter
    }

    private fun handleEvPlugTypeFilter(
        subElement: List<SubItems>
    ): AmenityFilterTypeData? {
        var filter: AmenityFilterTypeData? = null
        val evListSelected = ArrayList<String>()
        val evListAll = ArrayList<String>()
        subElement.forEach { plugType ->
            plugType.elementName?.let {
                evListAll.add(it)
                if (plugType.isSelected == true) {
                    evListSelected.add(it)
                }
            }
        }
        if (evListSelected.isNotEmpty()) {
            filter = AmenityFilterTypeData(plugTypes = evListSelected)
        } else if (evListAll.isNotEmpty()) {
            filter = AmenityFilterTypeData(plugTypes = evListAll)
        }
        return filter
    }


    fun setCarParkCategoryFilter(carParkCategory: CarParkCategory = CarParkCategory.SHORT_TERM): Boolean {
        val carParkAmenity = this.listAmenities.find { it.type == CARPARK } ?: return false
        if (carParkAmenity.filter != null) {
            carParkAmenity.filter!!.category = carParkCategory.type
        }
        return true
    }
}


class AmenitieFilter(
    @field:SerializedName("type")
    var type: String? = null,
    @field:SerializedName("filter")
    var filter: AmenityFilterTypeData? = null
)

class AmenityFilterTypeData(
    @field:SerializedName("provider")
    var provider: ArrayList<String>? = null,

    @field:SerializedName("plugTypes")
    var plugTypes: ArrayList<String>? = null,

    @field:SerializedName("category")
    var category: String? = null,

    @field:SerializedName("availableBand")
    var availableBand: String? = null,
)

enum class CarParkCategory(val type: String) {
    SHORT_TERM("SHORT_TERM")
}
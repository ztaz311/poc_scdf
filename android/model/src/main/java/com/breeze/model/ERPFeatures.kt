package com.breeze.model

import com.breeze.model.api.response.ERPResponseData
import com.mapbox.geojson.Feature

class ERPFeatures {
    var featureListERPNoCharge = arrayListOf<Feature>()
    var featureListERP = arrayListOf<Feature>()
    var mapOfERP: HashMap<String, ERPDetails> = hashMapOf()

    constructor(
        featureListERPNoCharge: ArrayList<Feature>,
        featureListERP: ArrayList<Feature>,
        mapOfERP: HashMap<String, ERPDetails>
    ) {
        this.featureListERPNoCharge = featureListERPNoCharge
        this.featureListERP = featureListERP
        this.mapOfERP = mapOfERP
    }

    data class ERPDetails(var erp: ERPResponseData.Erp, var charge: Float)
}

package com.breeze.model.api.response

import com.google.gson.annotations.SerializedName

data class DeleteAllBookMarkItemResponse(
    @SerializedName("message") var message: String? = null
)

package com.breeze.model.enums

enum class AndroidScreen(val nameStr: String) {
    ETA_FAVOURITES("ETAFavouriteSelect"),
    LANDING("MapLanding"),
    EDIT_SHORTCUTS("ShortcutInfoEditor"),
    TRIP_LOG("TripLog"),
    ROUTE_PLANNING("ParkingRoutePlanning"),
    COLLECTION_DETAILS("CollectionDetails"),
    CREATE_ACCOUNT("createAccount"),
    SIGN_IN("signIn"),
    EXPLORE_MAP("Explore"),
    CONNECT_OBU_DEVICE("ConnectOBUDevice"),
    OBU_PAIRED_DEVICES("OpenAllPairedVehicles"),
    OBU_LITE_DISPLAY("OBUDisplay"),
    SETTINGS("Settings"),
    ERP_DETAILS_PLAN_TRIP( "ERPDetailsPlanTrip"),
    SHARED_LOCATION( "SharedLocation"),
    SHARED_COLLECTION( "SharedCollection"),

}
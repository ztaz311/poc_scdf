package com.breeze.model

import androidx.core.os.bundleOf
import com.breeze.model.annotation.AmenityTypeVal
import com.breeze.model.api.response.amenities.AvailabilityCSData
import com.breeze.model.constants.AmenityType
import com.breeze.model.enums.AmenityBand
import com.breeze.model.extensions.rn.getBoolean
import com.breeze.model.extensions.rn.getBooleanOrNull
import com.breeze.model.extensions.rn.getIntOrNull
import com.breeze.model.extensions.rn.getMapOrNull
import com.breeze.model.extensions.rn.getString
import com.breeze.model.extensions.rn.getStringOrNull
import com.facebook.react.bridge.ReadableMap
import com.google.gson.annotations.SerializedName
import com.mapbox.geojson.Point

data class SharedLocation(

    @SerializedName("lat")
    var lat: String,

    @SerializedName("long")
    var long: String,

    @SerializedName("address1")
    var address1: String,

    @SerializedName("address2")
    var address2: String? = null,

    @SerializedName("code")
    var code: String? = null,

    @SerializedName("name")
    var name: String? = null,

    @SerializedName("description")
    var description: String? = null,

    @SerializedName("saved")
    var saved: Boolean = false,

    @SerializedName("bookmarkSnapshotId")
    var bookmarkSnapshotId: Int? = null,

    @SerializedName("bookmarkId")
    var bookmarkId: Int? = null,

    @SerializedName("amenityId")
    var amenityId: String? = null,

    @SerializedName("amenityType")
    @AmenityTypeVal
    var amenityType: String? = null,

    @SerializedName("isDestination")
    var isDestination: Boolean = false,

    @SerializedName("userLocationLinkId")
    var userLocationLinkId: Int?,

    @SerializedName("availablePercentImageBand")
    var availablePercentImageBand: String? = null, // "TYPE2"

    @SerializedName("availabilityCSData")
    var availabilityCSData: AvailabilityCSData? = null,

    @SerializedName("showAvailabilityFB")
    var showAvailabilityFB: Boolean = false,

    @SerializedName("hasAvailabilityCS")
    var hasAvailabilityCS: Boolean = false,

    @SerializedName("fullAddress")
    var fullAddress: String? = null,

    @SerializedName("shareType")
    var shareType: String? = null, // "share" or "invite"

    @SerializedName("placeId")
    var placeId: String? = null,

    @SerializedName("userLocationLinkIdRef")
    var userLocationLinkIdRef: String? = null,
) {

    fun getParkingAvailabilityType(): AmenityBand? {
        return availablePercentImageBand?.takeIf { it.isNotEmpty() }
            ?.let {
                AmenityBand.values().find {
                    it.type == availablePercentImageBand
                }
            }
    }

    fun getLocation(): Point? {
        val latitude = lat.toDoubleOrNull() ?: return null
        val longitude = long.toDoubleOrNull() ?: return null
        return Point.fromLngLat(longitude, latitude)
    }

    fun toDestinationDetails() = DestinationAddressDetails(
        lat = this.lat,
        long = this.long,
        address1 = this.address1,
        address2 = this.address2,
        code = this.code,
        destinationName = this.name,
        amenityId = this.amenityId,
        carParkID = this.amenityId?.takeIf { this.amenityType == AmenityType.CARPARK },
        amenityType = this.amenityType,
        availablePercentImageBand = this.availablePercentImageBand,
        showAvailabilityFB = this.showAvailabilityFB,
        availabilityCSData = this.availabilityCSData,
        hasAvailabilityCS = this.hasAvailabilityCS,
        fullAddress = this.fullAddress,
        placeId = this.placeId,
        userLocationLinkIdRef = this.userLocationLinkIdRef,
    )

    fun toBundle() =
        bundleOf(
            "lat" to lat,
            "long" to long,
            "address1" to address1,
            "address2" to address2,
            "code" to code,
            "name" to name,
            "description" to description,
            "saved" to saved,
            "bookmarkSnapshotId" to bookmarkSnapshotId,
            "bookmarkId" to bookmarkSnapshotId,
            "amenityId" to amenityId,
            "amenityType" to amenityType,
            "isDestination" to isDestination,
            "availablePercentImageBand" to availablePercentImageBand,
            "userLocationLinkId" to userLocationLinkId,
            "fullAddress" to fullAddress,
            "shareType" to shareType,
            "placeId" to placeId,
            "userLocationLinkIdRef" to userLocationLinkIdRef,
            "showAvailabilityFB" to showAvailabilityFB,
        )

    companion object {
        fun fromRNData(rnData: ReadableMap): SharedLocation = SharedLocation(
            lat = rnData.getString("lat", ""),
            long = rnData.getString("long", ""),
            address1 = rnData.getString("address1", ""),
            address2 = rnData.getStringOrNull("address2"),
            code = rnData.getStringOrNull("code"),
            name = rnData.getStringOrNull("name"),
            description = rnData.getStringOrNull("description"),
            saved = rnData.getBoolean("saved", false),
            bookmarkSnapshotId = rnData.getIntOrNull("bookmarkSnapshotId"),
            bookmarkId = rnData.getIntOrNull("bookmarkId"),
            amenityId = rnData.getStringOrNull("amenityId"),
            amenityType = rnData.getStringOrNull("amenityType"),
            isDestination = rnData.getBoolean("isDestination", false),
            availablePercentImageBand = rnData.getStringOrNull("availablePercentImageBand"),
            availabilityCSData = rnData.getMapOrNull("availabilityCSData")?.let { AvailabilityCSData.fromRNData(it) },
            showAvailabilityFB = rnData.getBooleanOrNull("showAvailabilityFB") ?: false,
            hasAvailabilityCS = rnData.getBooleanOrNull("hasAvailabilityCS") ?: false,
            userLocationLinkId = rnData.getIntOrNull("userLocationLinkId"),
            fullAddress = rnData.getStringOrNull("fullAddress"),
            shareType = rnData.getStringOrNull("shareType"),
            placeId = rnData.getStringOrNull("placeId"),
            userLocationLinkIdRef = rnData.getStringOrNull("userLocationLinkIdRef"),
        )
    }

    enum class ShareType(val typeStr: String) {
        SHARE("share"),
        INVITE("invite"),
    }
}
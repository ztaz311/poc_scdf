package com.breeze.model.api.request

import com.google.gson.annotations.SerializedName

class SaveOBUTripSummaryRequest(
    @SerializedName("vehicleNumber"       ) var vehicleNumber       : String?            = null,
    @SerializedName("tripGUID"            ) var tripGUID            : String?            = null,
    @SerializedName("totalTravelTime"     ) var totalTravelTime     : Int?               = null,
    @SerializedName("totalTravelDistance" ) var totalTravelDistance : Int?               = null,
    @SerializedName("totalTravelCharge"   ) var totalTravelCharge   : TotalTravelCharge? = TotalTravelCharge()
)

data class TotalTravelCharge (

    @SerializedName("erp" ) var erp : Int? = null,
    @SerializedName("eep" ) var eep : Int? = null,
    @SerializedName("rep" ) var rep : Int? = null,
    @SerializedName("opc" ) var opc : Int? = null,
    @SerializedName("cpt" ) var cpt : Int? = null

)
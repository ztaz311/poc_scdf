package com.breeze.model.api.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class EasyBreeziesResponse(
    @field:SerializedName("address")
    var addresses: MutableList<EasyBreezieAddress>? = null
) : Parcelable

@Parcelize
data class EasyBreezieAddress(

    @field:SerializedName("addressid")
    var addressid: Int? = null,

    @field:SerializedName("address2")
    var address2: String? = null,

    @field:SerializedName("address1")
    var address1: String? = null,

    @field:SerializedName("lat")
    var lat: String? = null,

    @field:SerializedName("long")
    var long: String? = null,

    @field:SerializedName("name")
    val name: String? = null,

    var distance: Float? = null
) : Parcelable



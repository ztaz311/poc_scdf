package com.breeze.model.api.request

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class EasyBreeziesRequest(
    @field:SerializedName("deviceosversion")
    var deviceosversion: String? = null,
    @field:SerializedName("deviceos")
    var deviceos: String? = null,
    @field:SerializedName("appversion")
    var appversion: String? = null,
    @field:SerializedName("devicemodel")
    var devicemodel: String? = null,
    @field:SerializedName("easybreezies")
    var easybreezies: MutableList<EasyBreezieCreateUpdate?>? = null
) : Parcelable

@Parcelize
data class EasyBreezieCreateUpdate(
    @field:SerializedName("addressid")
    var addressid: Int? = null,
    @field:SerializedName("lat")
    var lat: String? = null,
    @field:SerializedName("long")
    var long: String? = null,
    @field:SerializedName("name")
    var name: String? = null,
    @field:SerializedName("address1")
    var address1: String? = null,
    @field:SerializedName("address2")
    var address2: String? = null
) : Parcelable

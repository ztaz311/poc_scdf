package com.breeze.model

import com.breeze.model.enums.RemoteImage
import com.facebook.react.bridge.Arguments
import com.facebook.react.bridge.ReadableMap
import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class RoutablePoint(
    @SerializedName("lat")
    var lat: Double,

    @SerializedName("long")
    var long: Double,

    @SerializedName("type")
    var type: String,

    @SerializedName("icon")
    var icon: RemoteImage? = null,

    @SerializedName("name")
    var name: String? = null,

    @SerializedName("carparkId")
    var carparkId: String? = null,

    @SerializedName("address1")
    var address1: String? = null,

    @SerializedName("address2")
    var address2: String? = null,

    @SerializedName("fullAddress")
    var fullAddress: String? = null,
) : Serializable {

    fun toRNWritableMap(): ReadableMap = Arguments.createMap().apply {
        putDouble("lat", lat)
        putDouble("long", long)
        putString("type", type)
        icon?.let { putMap("icon", it.toRNWritableMap()) }
        putString("name", name)
        putString("carparkId", carparkId)
        putString("address1", address1)
        putString("address2", address2)
        putString("fullAddress", fullAddress)
    }
}

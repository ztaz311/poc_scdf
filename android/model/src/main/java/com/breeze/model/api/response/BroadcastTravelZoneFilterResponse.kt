package com.breeze.model.api.response

import com.google.gson.annotations.SerializedName

data class BroadcastTravelZoneFilterResponse(
    @SerializedName("success")
    val success: Boolean = false,
    @SerializedName("broadcasts")
    val broadcasts: List<BroadcastData> = arrayListOf()
)

data class BroadcastData(
    @SerializedName("name")
    val name: String? = null,
    @SerializedName("title")
    val title: String? = null,
    @SerializedName("message")
    val message: String? = null,
    @SerializedName("type")
    val type: String? = null,
    @SerializedName("value")
    val value: String? = null,
    @SerializedName("action")
    val action: BroadcastAction? = null,
    @SerializedName("alertLevel")
    val alertLevel: String? = null,

    )

data class BroadcastAction(
    @SerializedName("data")
    val data: BroadcastActionData? = null,
    @SerializedName("type")
    val type: String? = null,
)

data class BroadcastActionData(
    @SerializedName("id")
    val id: String? = null,
    @SerializedName("type")
    val type: String? = null,
)

package com.breeze.model.api.response

import android.os.Parcelable
import com.breeze.model.BaseTripItem
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.io.Serializable

@Parcelize
data class UpcomingTripDetailResponse(
    @field:SerializedName("tripPlannerId")
    val tripPlannerId: Long? = null,

    @field:SerializedName("tripEstStartTime")
    override val tripEstStartTime: Long? = null,

    @field:SerializedName("tripEstArrivalTime")
    override val tripEstArrivalTime: Long? = null,

    @field:SerializedName("tripPlanBy")
    override val tripPlanBy: String? = null,

    @field:SerializedName("tripStartAddress1")
    override val tripStartAddress1: String? = null,

    @field:SerializedName("tripStartAddress2")
    override var tripStartAddress2: String? = null,

    @field:SerializedName("tripDestAddress1")
    override val tripDestAddress1: String? = null,

    @field:SerializedName("tripDestAddress2")
    override val tripDestAddress2: String? = null,

    @field:SerializedName("tripStartLat")
    val tripStartLat: String? = null,

    @field:SerializedName("tripStartLong")
    val tripStartLong: String? = null,

    @field:SerializedName("tripDestLat")
    val tripDestLat: String? = null,

    @field:SerializedName("tripDestLong")
    val tripDestLong: String? = null,

    @field:SerializedName("totalDistance")
    override val totalDistance: Double? = null,

    @field:SerializedName("erpExpense")
    override var erpExpense: Double? = null,

    @field:SerializedName("routeStops")
    val routeStops: List<BreezeWaypointCoordinates> = emptyList(),

    @field:SerializedName("routeCoordinates")
    val routeCoordinates: List<List<Double>>? = null,

    @field:SerializedName("isDestinationCarpark")
    override var isDestinationCarpark: Boolean = false,

    @field:SerializedName("carParkId")
    override var carParkId: String? = null,

    @SerializedName("tripEtaFavouriteId")
    override var tripEtaFavouriteId: Int? = null,

    @SerializedName("recipientNumber")
    var recipientNumber: String? = null,

    @SerializedName("recipientName")
    var recipientName: String? = null,

    @SerializedName("isEtaAvailable")
    var isEtaAvailable: Boolean = false,

    @field:SerializedName("totalDuration")
    override val totalDuration: Double? = null,


//    @field:SerializedName("erpEntries")
//    val erpEntries :List<Long> = emptyList(),


) : BaseTripItem(), Parcelable, Serializable


@Parcelize
data class BreezeWaypointCoordinates(
    @field:SerializedName("amenityId")
    val amenityId: String?,
    @field:SerializedName("type")
    val type: String,
    @field:SerializedName("coordinates")
    val coordinates: List<Double>
) : Parcelable
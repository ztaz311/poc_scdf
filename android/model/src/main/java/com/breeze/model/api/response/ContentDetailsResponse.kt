package com.breeze.model.api.response

import com.breeze.model.api.response.amenities.BaseAmenity
import com.google.gson.annotations.SerializedName

data class ContentDetailsResponse(
    @SerializedName("content_id") var content_id: Int? = null,
    @SerializedName("name") var name: String? = null,
    @SerializedName("type") var type: String? = null,
    @SerializedName("recentre_zone_radius") var recenterZoneRadius: Double? = 0.0,
    @SerializedName("is_new_layer") var isNewLayer: Boolean? = false,
    @SerializedName("layer_code") var layerCode: String? = null,
    @SerializedName("details") var details: List<ContentDetails> = arrayListOf(),
    @SerializedName("zones") var zones: List<Zones> = arrayListOf(),
    @SerializedName("enableWalking") var enableWalking: Boolean? = true,
    @SerializedName("enable_zone_drive") var enableZoneDrive: Boolean? = null,
    @SerializedName("track_navigation") var track_navigation: Boolean = false,
    @SerializedName("stats_icon_dark") var stats_icon_dark: String? = null,
    @SerializedName("stats_icon_light") var stats_icon_light: String? = null,
    @SerializedName("message_category") var message_category: String? = null,
    @SerializedName("message_type") var message_type: String? = null,
    @SerializedName("message_title") var message_title: String? = null,

    )

data class ContentDetails(
    @SerializedName("content_details_id") var content_details_id: Int? = null,
    @SerializedName("name") var name: String? = null,
    @SerializedName("code") var code: String? = null,
    @SerializedName("map_light_selected_url") var map_light_selected_url: String? = null,
    @SerializedName("map_light_unselected_url") var map_light_unselected_url: String? = null,
    @SerializedName("map_dark_selected_url") var map_dark_selected_url: String? = null,
    @SerializedName("map_dark_unselected_url") var map_dark_unselected_url: String? = null,
    @SerializedName("tooltip_radius") var tooltip_radius: Int? = null,
    @SerializedName("tooltip_type") var tooltip_type: String? = null,
    @SerializedName("cluster_enabled") var cluster_enabled: Boolean? = null,
    @SerializedName("cluster_id") var cluster_id: Int? = null,
    @SerializedName("data") var data: List<BaseAmenity> = arrayListOf()
)

data class LocationInfo(
    @SerializedName("type") var type: String? = null,
    @SerializedName("coordinates") var coordinates: ArrayList<String> = arrayListOf()
)

data class Zones(
    @SerializedName("content_zone_id") var content_zone_id: Int? = null,
    @SerializedName("active_zoom_level") var activeZoomLevel: Int? = null,
    @SerializedName("deactive_zoom_level") var deactiveZoomLevel: Int? = null,
    @SerializedName("radius") var radius: Int? = null,
    @SerializedName("zone_id") var zoneId: String? = null,
    @SerializedName("center_point_lat") var centerPointLat: Double? = null,
    @SerializedName("center_point_long") var centerPointLong: Double? = null
)

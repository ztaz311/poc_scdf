package com.breeze.model.annotation

import androidx.annotation.StringDef
import com.breeze.model.constants.AmenityType

@Retention(AnnotationRetention.SOURCE)
@MustBeDocumented
@StringDef(
    "",
    AmenityType.EVCHARGER,
    AmenityType.CARPARK,
    AmenityType.MUSEUM,
    AmenityType.HERITAGETREE,
    AmenityType.PCN_CONST,
    AmenityType.PETROL,
    AmenityType.PARKS,
    AmenityType.LIBRARY,
    AmenityType.PASAR_MALAM,
    AmenityType.POI,
    AmenityType.CLINIC,
    AmenityType.FIREWORKS,
)
annotation class AmenityTypeVal
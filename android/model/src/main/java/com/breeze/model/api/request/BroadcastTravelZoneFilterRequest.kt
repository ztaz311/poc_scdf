package com.breeze.model.api.request

import com.google.gson.annotations.SerializedName

class BroadcastTravelZoneFilterRequest(
    @SerializedName("routes")
    var routes: List<BroadcastTravelZoneRoute?> = listOf(),
    @SerializedName("arrivalTimestamp")
    var arrivalTimestamp: Long?,
    @SerializedName("departureTimestamp")
    var departureTimestamp: Long?,
    @SerializedName("destination")
    var destination: BroadcastTravelZoneDestination?
)

class BroadcastTravelZoneRoute(
    @SerializedName("coordinates" ) var coordinates : List<List<Double>> = arrayListOf()
)

class BroadcastTravelZoneDestination(
    @SerializedName("lat")
    var lat: Double,
    @SerializedName("long")
    var long: Double,
)
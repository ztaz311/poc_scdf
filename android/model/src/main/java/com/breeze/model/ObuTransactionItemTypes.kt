package com.breeze.model

enum class ObuTransactionItemTypes(val type: String) {
    PARKING("parking"),
    ERP("erp"),

}
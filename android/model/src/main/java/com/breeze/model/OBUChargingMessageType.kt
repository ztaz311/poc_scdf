package com.breeze.model

enum class OBUChargingMessageType(val type: String) {
    DEDUCTION_SUCCESSFUL("DeductionSuccessful"),
    DEDUCTION_FAILURE("DeductionFailure"),

}
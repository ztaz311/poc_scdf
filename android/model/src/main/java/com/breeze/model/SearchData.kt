package com.breeze.model

class SearchData {
    var address1: String? = null
    var distance: String? = null
    var address2: String? = null
    var lat: String? = null
    var jsonMemberLong: String? = null
    var alias: String? = null

    var calculatedDistance: Float? = null

    override fun toString(): String {
        return "Data [name=$address1]"
    }
}
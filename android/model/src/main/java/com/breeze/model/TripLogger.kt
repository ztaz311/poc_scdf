package com.breeze.model

import com.breeze.model.constants.TripOBUStatus


data class ERPCost(
    var erpId: String,
    var name: String,
    var originalAmount: Float,
    var time: Long
)

data class TripLog(
    var tripId: String,
    var timeStamp: Long,
    var status: Int,
    var latitude: Double,
    var longitude: Double,
    var distanceTravelled: Double,
    var erps: List<ERPCost>
)

enum class TripStatus(var type: Int) {
    START(0),
    END(1),
    NAVIGATION(2)
}

enum class TripType(var type: String) {
    CRUISE("CRUISE"),
    NAVIGATION("NAVIGATION"),
    OBULITE("OBULITE")
}

data class TripSummary(
    var type: String,
    var tripGUID: String,
    var tripStartTime: Long,
    var tripStartAddress1: String,
    var tripStartAddress2: String,
    var tripStartLat: Double,
    var tripStartLong: Double,
    var tripEndTime: Long,
    var tripDestAddress1: String,
    var tripDestAddress2: String,
    var tripDestLat: Double,
    var tripDestLong: Double,
    var totalExpense: Float,
    var totalDistance: Double,
    var erp: List<ERPCost>,
    val isDestinationCarpark: Boolean,
    val navigationEndType: String,
    val carParkId: String?,
    val layerCode: String?,
    val amenityId: String?,

    /**
     * this field apply to destination complete
     */
    val plannedDestAddress1: String?,
    val plannedDestAddress2: String?,
    val plannedDestLat: Double,
    val plannedDestLong: Double,
    val amenityType: String?,
    val idleTime: Long?,
    val vehicleNumber: String?,
    val bookmarkId: Int?,
    val bookmarkName: String?,
    val obuStatus: String = TripOBUStatus.NOT_PAIRED
    )
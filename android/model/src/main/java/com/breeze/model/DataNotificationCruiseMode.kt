package com.breeze.model

import java.io.Serializable


class DataNotificationCruiseMode : Serializable {
    var mDistance: String = ""
    var zonePriority: Int = 0
    var mIsShowAmount: Boolean = false
}
package com.breeze.model.api.request

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.io.Serializable

@Parcelize
data class CreateEasyBreezieRequest(
    @SerializedName("easybreezies")
    var easybreezies: CreateEasyEasybreezies? = null
) : Parcelable, Serializable

@Parcelize
data class CreateEasyEasybreezies(
    @SerializedName("address1")
    var address1: String? = null,
    @SerializedName("address2")
    var address2: String? = null,
    @SerializedName("addressid")
    var addressid: Int? = null,
    @SerializedName("lat")
    var lat: String? = null,
    @SerializedName("long")
    var long: String? = null,
    @SerializedName("name")
    var name: String? = null
) : Parcelable, Serializable
package com.breeze.model.api.request

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class RegisterDeviceRequest(

    @field:SerializedName("deviceos")
    val deviceos: String? = null,

    @field:SerializedName("deviceID")
    val deviceID: String? = null,

    @field:SerializedName("token")
    val token: String? = null
) : Parcelable

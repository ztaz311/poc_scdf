package com.breeze.model.api.request

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.io.Serializable

@Parcelize
data class AddressRecommendationRequest(
    @SerializedName("deviceosversion")
    var deviceosversion: String? = null,
    @SerializedName("deviceos")
    var deviceos: String? = null,
    @SerializedName("appversion")
    var appversion: String? = null,
    @SerializedName("lat")
    var lat: String? = null,
    @SerializedName("long")
    var long: String? = null,
    @SerializedName("devicemodel")
    var devicemodel: String? = null
) : Parcelable, Serializable

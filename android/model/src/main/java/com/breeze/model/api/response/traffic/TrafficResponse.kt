package com.breeze.model.api.response.traffic

import com.google.gson.annotations.SerializedName
import com.mapbox.geojson.Point

class TrafficResponse {
    @SerializedName("lastImageTime")
    var lastImageTime: Int? = null

    @SerializedName("distance")
    var distance: Int? = null

    @SerializedName("orderSequence")
    var orderSequence: Int? = null

    @SerializedName("long")
    var long: Double? = null

    @SerializedName("imageLink")
    var imageLink: String? = null

    @SerializedName("tag")
    var tag: String? = null

    @SerializedName("lat")
    var lat: Double? = null

    @SerializedName("name")
    var name: String? = null

    @SerializedName("cameraID")
    var cameraID: Int? = null

    @SerializedName("id")
    var id: String? = null

    fun toPoint(): Point {
        return Point.fromLngLat(
            this.long!!.toDouble(),
            this.lat!!.toDouble()
        )
    }
}
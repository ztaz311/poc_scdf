package com.breeze.model.api.request

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

/**
 * Created by Aiswarya on 24,June,2021
 */
@Parcelize
data class SendTripLogByMonthRequest(
    @field:SerializedName("type")
    var type: String = "BY_MONTH",
    @field:SerializedName("month")
    var month: String? = null,
    @field:SerializedName("year")
    var year: String? = null,
) : Parcelable
package com.breeze.model.extensions

import com.mapbox.api.directions.v5.models.DirectionsResponse
import java.io.ByteArrayInputStream
import java.io.IOException
import java.io.ObjectInputStream
import java.util.zip.GZIPInputStream


@Throws(IOException::class)
fun ByteArray.decompressToDirectionsResponse(): DirectionsResponse {
    val time = System.currentTimeMillis()
    val gzipInputStream = ByteArrayInputStream(this)
    var gis: GZIPInputStream? = null
    var objInputStream: ByteArrayInputStream? = null
    try {
        gis = GZIPInputStream(gzipInputStream)
        objInputStream = ByteArrayInputStream(gis.readBytes())
        val objectInput = ObjectInputStream(objInputStream)
        return objectInput.readObject() as DirectionsResponse
    } finally {
        gzipInputStream.close()
        gis?.close()
        objInputStream?.close()
    }
}
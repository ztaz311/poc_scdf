package com.breeze.model

import com.google.gson.annotations.SerializedName

data class SharedCollection(
    @SerializedName("collectionSnapshotId")
    var collectionSnapshotId: Int? = null,

    @SerializedName("collectionId")
    var collectionId: Int? = null,

    @SerializedName("name")
    var name: String? = null,

    @SerializedName("code")
    var code: String? = null,

    @SerializedName("pinned")
    var pinned: Boolean = false,

    @SerializedName("description")
    var description: String? = null,

    @SerializedName("bookmarks")
    var bookmarks: List<SharedLocation>? = null,
) {
    fun isSaved() = collectionId != null
}
package com.breeze.model

import com.breeze.model.api.response.amenities.BaseAmenity

data class SelectedAmenity(
    val baseAmenity: BaseAmenity,
    var selected: Boolean,
    val amenityAddressType: AmenityAddressType
)

enum class AmenityAddressType {
    SOURCE, DESTINATION
}
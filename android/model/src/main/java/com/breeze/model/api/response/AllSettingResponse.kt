package com.breeze.model.api.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class AllSettingResponse(

    @field:SerializedName("masterlists")
    var masterlists: Masterlists? = null,
) : Parcelable

@Parcelize
data class ItemSetting(

    @field:SerializedName("code")
    var code: String? = null,

    @field:SerializedName("value")
    var value: String? = null,

    @field:SerializedName("module")
    var module: String? = null,

    @field:SerializedName("default")
    var default: String? = null,
) : Parcelable

@Parcelize
data class Masterlists(

    @field:SerializedName("MapDisplay")
    var mapDisplay: MutableList<ItemSetting>? = null,

    @field:SerializedName("RoutePreference")
    var routePreference: MutableList<ItemSetting>? = null,

    @field:SerializedName("ShowDemo")
    var showDemo: MutableList<ItemSetting>? = null,

    @field:SerializedName("CarparkDistance")
    var carparkDistance: MutableList<ItemSetting>? = null,

    @field:SerializedName("CarparkCount")
    var carparkCount: MutableList<ItemSetting>? = null,

    @field:SerializedName("CarparkAvailability")
    var carparkAvailability: MutableList<ItemSetting>? = null,

    @field:SerializedName("BookmarkLimit")
    var bookmarkLimit: MutableList<ItemSetting>? = null,

    @field:SerializedName("CollectionLimit")
    var collectionLimit: MutableList<ItemSetting>? = null,

    @field:SerializedName("NewUserThresholdDays")
    var newUserThresholdDays: MutableList<ItemSetting>? = null,

    @field:SerializedName("ShowNudge")
    var showNudge: MutableList<ItemSetting>? = null,

    @field:SerializedName("ShowSavedCarparkListView")
    var showSavedCarparkListView: MutableList<ItemSetting>? = null,

    @field:SerializedName("TrafficInformation")
    var trafficInformation: MutableList<ItemSetting>? = null,

    @field:SerializedName("ParkingAvailabilityDisplay")
    var parkingAvailabilityDisplay: MutableList<ItemSetting>? = null,

    @field:SerializedName("ParkingAvailabilityAudio")
    var parkingAvailabilityAudio: MutableList<ItemSetting>? = null,

    @field:SerializedName("EstimatedTravelTimeDisplay")
    var estimatedTravelTimeDisplay: MutableList<ItemSetting>? = null,

    @field:SerializedName("EstimatedTravelTimeAudio")
    var estimatedTravelTimeAudio: MutableList<ItemSetting>? = null,

    @field:SerializedName("vehicleTypes")
    var vehicleTypes: MutableList<ItemSetting>? = null,

    @field:SerializedName("vehiclePowerTypes")
    var vehiclePowerTypes: MutableList<ItemSetting>? = null,

    @field:SerializedName("vehicleFuelGrade")
    var vehicleFuelGrade: MutableList<ItemSetting>? = null,

    @field:SerializedName("SchoolZoneDisplayAlert")
    var SchoolZoneDisplayAlert: MutableList<ItemSetting>? = null,

    @field:SerializedName("SchoolZoneVoiceAlert")
    var SchoolZoneVoiceAlert: MutableList<ItemSetting>? = null,

    @field:SerializedName("SilverZoneDisplayAlert")
    var SilverZoneDisplayAlert: MutableList<ItemSetting>? = null,

    @field:SerializedName("SilverZoneVoiceAlert")
    var SilverZoneVoiceAlert: MutableList<ItemSetting>? = null,

    @field:SerializedName("BusLaneDisplayAlert")
    var BusLaneDisplayAlert: MutableList<ItemSetting>? = null,

    @field:SerializedName("BusLaneVoiceAlert")
    var BusLaneVoiceAlert: MutableList<ItemSetting>? = null,

    @field:SerializedName("CarparkListDistance")
    var carparkListDistance: MutableList<ItemSetting>? = null,

    @field:SerializedName("CarparkAvailabilityDistance")
    var carparkAvailabilityDistance: MutableList<ItemSetting>? = null,

    @field:SerializedName("CarparkAvailabilityAlertDisplayTime")
    var carparkAvailabilityAlertDisplayTime: MutableList<ItemSetting>? = null,

    /**
     * radius to load EV/Petro in meters,
     * should be only 1 item List returned from BE
     */
    @field:SerializedName("EVPetrolRange")
    var EVPetrolRange: List<ItemSetting> = emptyList(),

    /**
     * max radius to load EV/Petro in meters,
     * should be only 1 item returned from BE
     * */
    @field:SerializedName("MaxEVPetrolRange")
    var MaxEVPetrolRange: List<ItemSetting> = emptyList(),

    @field:SerializedName("EVPetrolCount")
    var EVPetrolCount: List<ItemSetting> = emptyList(),

    @field:SerializedName("CarparkListAlertDistance")
    var carparkListAlertDistance: List<ItemSetting> = emptyList(),
) : Parcelable



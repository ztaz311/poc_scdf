package com.breeze.model

abstract class BaseTripItem {
    abstract val tripEstStartTime: Long?
    abstract val tripStartAddress1: String?
    abstract var tripStartAddress2: String?
    abstract val tripEstArrivalTime: Long?
    abstract val totalDuration: Double?
    abstract val totalDistance: Double?
    abstract val tripDestAddress1: String?
    abstract val tripDestAddress2: String?
    abstract var erpExpense: Double?
    abstract val tripPlanBy: String?
    abstract var isDestinationCarpark: Boolean
    abstract var carParkId: String?
    abstract var tripEtaFavouriteId: Int?
}
package com.breeze.model.api.response


data class NavigationZoneData(
    val features: List<Feature>?,
    val fileName: String,
    val type: String
)

data class Feature(
    val geometry: Geometry?,
    val properties: Properties,
    val type: String
)

data class Geometry(
    val bbox: List<Double>,
    val coordinates: List<List<List<Any>>>,
    val type: String
)

data class Properties(
    val FMEL_UPD_D: String,
    val INC_CRC: String,
    val SITENAME: String
)

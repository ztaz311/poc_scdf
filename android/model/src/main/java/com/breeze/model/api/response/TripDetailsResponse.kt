package com.breeze.model.api.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.io.Serializable

@Parcelize
data class TripDetailsResponse(

    @field:SerializedName("erp")
    var erp: List<ERPDetails> = emptyList(),

    @field:SerializedName("tripGUID")
    val tripGUID: String? = null,

    @field:SerializedName("tripId")
    val tripId: Long,

    @field:SerializedName("parkingExpense")
    var parkingExpense: Double? = null,

    @field:SerializedName("attachementFilename")
    val attachementFilename: String? = null,

    @field:SerializedName("millegeExpense")
    val millegeExpense: Float? = null,

    @field:SerializedName("tripEndTime")
    val tripEndTime: Long? = null,

    @field:SerializedName("tripStartAddress1")
    val tripStartAddress1: String? = null,

    @field:SerializedName("tripStartAddress2")
    val tripStartAddress2: String? = null,

    @field:SerializedName("tripStartTime")
    val tripStartTime: Long? = null,

    @field:SerializedName("tripDestAddress2")
    val tripDestAddress2: String? = null,

    @field:SerializedName("totalDistance")
    val totalDistance: Double? = null,

    @field:SerializedName("tripDestAddress1")
    val tripDestAddress1: String? = null,

    @field:SerializedName("totalExpense")
    var totalExpense: Double? = null,

    @field:SerializedName("isParkingEditable")
    var isParkingEditable: Boolean? = true,

    @field:SerializedName("remark")
    var remark: String = "",
) : Parcelable, Serializable

@Parcelize
data class ERPDetails(
    @field:SerializedName("tripErpId")
    var tripErpId: Long,
    @field:SerializedName("erpId")
    var erpId: Long,
    @field:SerializedName("name")
    var name: String,
    @field:SerializedName("originalAmount")
    var originalAmount: Double,
    @field:SerializedName("actualAmount")
    var actualAmount: Double,
    @field:SerializedName("time")
    var time: Long,
    @field:SerializedName("isERPEditable")
    var isERPEditable: Boolean? = true
) : Parcelable, Serializable
package com.breeze.model.enums

enum class CollectionCode {
    SHORTCUTS,
    SAVED_PLACES,
    NORMAL,
}
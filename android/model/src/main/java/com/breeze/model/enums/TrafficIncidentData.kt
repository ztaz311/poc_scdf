package com.breeze.model.enums

import com.breeze.model.NavigationZone

/**
 * NOTE: TrafficIncidentData "type" should not end with "digits" characters
 */
enum class TrafficIncidentData(val type: String, val zone: NavigationZone) {
    MAJOR_ACCIDENT("Major Accident", NavigationZone.MAJOR_ACCIDENT),
    ACCIDENT("Accident", NavigationZone.ACCIDENT),
    VEHICLE_BREAKDOWN("Vehicle breakdown", NavigationZone.VEHICLE_BREAKDOWN),
    ROAD_WORK("Road Work", NavigationZone.ROAD_WORK),
    OBSTACLE("Obstacle", NavigationZone.OBSTACLE),
    ROAD_BLOCK("Road Block", NavigationZone.ROAD_BLOCK),
    MISCELLANEOUS("Miscellaneous", NavigationZone.MISCELLANEOUS),
    UNATTENDED_VEHICLE("Unattended Vehicle", NavigationZone.UNATTENDED_VEHICLE),
    ROAD_CLOSURE("Road Closure", NavigationZone.ROAD_CLOSURE),
    HEAVY_TRAFFIC("Heavy Traffic", NavigationZone.HEAVY_TRAFFIC),
    FLASH_FLOOD("Flash Flood", NavigationZone.FLASH_FLOOD);

    /*MISCELLANEOUS("Miscellaneous"),

    HEAVY_TRAFFIC("Heavy Traffic"),
    WEATHER("Weather"),
    UNATTENDED_VEHICLE("Unattended Vehicle"),
    REVERSE_FLOW("Reverse Flow"),
    PLANT_FAILURE("Plant Failure"),
    FIRE("Fire"),
    DIVERSION("Diversion");*/

    companion object {
        private val TRAFFIC_TYPE_MAP: Map<String, TrafficIncidentData> =
            TrafficIncidentData.values().associate {
                return@associate Pair(it.type, it)
            }

        fun getIncidentType(requiredType: String): TrafficIncidentData? {
            return TRAFFIC_TYPE_MAP[requiredType]
        }

    }


}
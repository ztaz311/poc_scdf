package com.breeze.model.api.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.io.Serializable

@Parcelize
data class TripsOBUTransactionResponse(

    @field:SerializedName("totalItems")
    val totalItems: Int? = null,

    @field:SerializedName("transactions")
    val transactions: List<TripObuTransactionItem> = emptyList(),

    @field:SerializedName("totalPages")
    val totalPages: Int? = null,

    @field:SerializedName("currentPage")
    val currentPage: Int? = null
) : Parcelable, Serializable


@Parcelize
data class TripObuTransactionItem(

    @field:SerializedName("name")
    val transactionName: String = "",

    @field:SerializedName("type")
    val type: String,

    @field:SerializedName("tripGUID")
    val tripGUID: String,

    @field:SerializedName("chargeAmount")
    val chargeAmount: Double,

    @field:SerializedName("chargeTimeStamp")
    val chargeTimeStamp: Long,

    @field:SerializedName("chargingMessageType")
    val chargingMessageType: String,

    @field:SerializedName("cardStatus")
    val cardStatus: String? = null,
    @field:SerializedName("startTime")
    val startTime: Long? = null,
    @field:SerializedName("endTime")
    val endTime: Long? = null,

    ) : Parcelable, Serializable


@Parcelize
data class ObuVehicleResponse(
    @field:SerializedName("success")
    val success: Boolean,

    @field:SerializedName("data")
    val data: ArrayList<ObuVehicleDetails>,
) : Parcelable, Serializable

@Parcelize
data class ObuVehicleDetails(
    @field:SerializedName("vehicleNumber")
    val vehicleNumber: String,

    @field:SerializedName("vehicleType")
    val vehicleType: String,

    @field:SerializedName("powerType")
    val powerType: String,

    @field:SerializedName("price")
    val price: Double,

    @field:SerializedName("consumptionPerKm")
    val consumptionPerKm: Double,

    ) : Parcelable, Serializable
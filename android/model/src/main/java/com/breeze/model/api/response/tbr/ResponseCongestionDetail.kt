package com.breeze.model.api.response.tbr

import com.google.gson.annotations.SerializedName

data class ResponseCongestionDetail(
    @SerializedName("name") var name: String? = null,
    @SerializedName("color") var color: String? = null,
    @SerializedName("opacity") var opacity: Double? = null,
    @SerializedName("availablePercent") var availablePercent: Double? = null

)

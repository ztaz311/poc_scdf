package com.breeze.model.firebase

data class NavigationLogData(
    val filePath: String,
    var userId: String,
    val description: String
)
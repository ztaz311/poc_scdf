package com.breeze.model.api.response.amenities


/**
 * this class for split list carPark
 */
class DataAmenitiesDes {
    var mListCarkPark: ArrayList<BaseAmenity> = arrayListOf()
    var mListAmenities: ArrayList<BaseAmenity> = arrayListOf()
}
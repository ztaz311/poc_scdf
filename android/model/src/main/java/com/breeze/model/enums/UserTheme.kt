package com.breeze.model.enums

import androidx.appcompat.app.AppCompatDelegate

enum class UserTheme(val type: String) {
    SYSTEM_DEFAULT("Auto"),
    LIGHT("Light"),
    DARK("Dark");


    companion object {
        fun setDefaultNightMode(userTheme: UserTheme) {
            when (userTheme) {
                LIGHT -> {
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
                }

                DARK -> {
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
                }

                SYSTEM_DEFAULT -> {
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM)
                }
            }
        }

        fun setDefaultNightMode(themeStr: String) {
            val userTheme = UserTheme.values().find { it.type == themeStr } ?: return
            setDefaultNightMode(userTheme)
        }
    }
}
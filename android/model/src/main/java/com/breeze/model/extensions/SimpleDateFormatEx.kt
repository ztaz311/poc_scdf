package com.breeze.model.extensions

import java.text.SimpleDateFormat
import java.util.*

fun SimpleDateFormat.parseOrNull(string: String): Date? {
    runCatching {
        return parse(string)
    }
    return null
}
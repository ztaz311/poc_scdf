package com.breeze.model

data class LocalUserData(
    var userId: String? = null,
    var nudgeShown: Boolean? = false,
    var appLaunchCount: Int? = 0
)
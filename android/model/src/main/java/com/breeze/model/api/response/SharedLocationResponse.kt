package com.breeze.model.api.response

import com.breeze.model.SharedLocation
import com.google.gson.annotations.SerializedName

data class SharedLocationResponse(
    @SerializedName("success")
    var success: Boolean,

    @SerializedName("data")
    var data: SharedLocation,
)
package com.breeze.model.api.request

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class CopilotAPIRequest(
    @SerializedName("mapBoxfireBaseId")
    var mapBoxfireBaseId: String? = null,
    @SerializedName("name")
    var name: String? = null
) : Serializable
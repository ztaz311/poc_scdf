package com.breeze.model.api.request

data class GetParkingAlternativeRequest(
    private val lat: Double,
    private val long: Double,
    private val destinationName: String
)
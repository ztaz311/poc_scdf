package com.breeze.model.api.response

import com.google.gson.annotations.SerializedName

data class SaveCollectionResponse(
    @SerializedName("collectionId") var collectionId: Int? = null,
    @SerializedName("collectionLinkId") var collectionLinkId: Int? = null,
    @SerializedName("code") var code: String? = null,
    @SerializedName("message") var message: String? = null,
    @SerializedName("name") var name: String? = null,
    @SerializedName("errorCode") var errorCode: String? = null,
    @SerializedName("collectionCreated") var collectionCreated: Boolean? = null
)

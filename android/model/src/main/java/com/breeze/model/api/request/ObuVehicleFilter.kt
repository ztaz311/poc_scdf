package com.breeze.model.api.request

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.io.Serializable

@Parcelize
data class ObuVehicleFilter(
    @field:SerializedName("vehicleNumbers")
    var vehicleNumbers: List<String>? = null,
) : Parcelable, Serializable
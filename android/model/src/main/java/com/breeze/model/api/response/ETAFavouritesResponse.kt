package com.breeze.model.api.response


import android.os.Parcelable

import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Deprecated(
    "Use RecentETAContact instead of FavouritesItem",
    ReplaceWith("RecentETAContact", "com.breeze.model.response.eta.RecentETAContact")
)
@Parcelize
data class FavouritesItem(
    @SerializedName("tripEtaFavouriteId")
    var tripEtaFavouriteId: Int? = 0,
    @SerializedName("shareLiveLocation")
    var shareLiveLocation: String = "",
    @SerializedName("module")
    var module: String = "",
    @SerializedName("destination")
    var destination: String = "",
    @SerializedName("count")
    var count: Int? = 0,
    @SerializedName("recipientName")
    var recipientName: String = "",
    @SerializedName("tripDestLong")
    var tripDestLong: String = "",
    @SerializedName("message")
    var message: String = "",
    @SerializedName("tripDestLat")
    var tripDestLat: String = "",
    @SerializedName("recipientNumber")
    var recipientNumber: String = "",
    @SerializedName("voiceCallToRecipient")
    var voiceCallToRecipient: String = ""
) : Parcelable

@Deprecated("Use RecentETAContact instead of FavouritesItem")
@Parcelize
data class ETAFavouritesResponse(
    @SerializedName("favourites")
    val favourites: List<FavouritesItem>?
) : Parcelable



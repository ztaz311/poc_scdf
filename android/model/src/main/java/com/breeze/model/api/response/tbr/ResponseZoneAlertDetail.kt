package com.breeze.model.api.response.tbr

import com.google.gson.annotations.SerializedName

data class ResponseZoneAlertDetail(
    @SerializedName("alertMessage") var alertMessage: String? = "",
    @SerializedName("isCongestion") var isCongestion: Boolean? = null,

    )



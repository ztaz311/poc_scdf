package com.breeze.model

import com.breeze.model.api.response.amenities.AvailabilityCSData
import com.facebook.react.bridge.Arguments
import com.facebook.react.bridge.WritableMap
import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Map data with search-location.model.ts of React Native
 */
data class SearchLocation(
    @SerializedName("placeId") val placeId: String? = null,
    @SerializedName("addressid") val addressid: Int? = null,
    @SerializedName("lat") val lat: String? = null,
    @SerializedName("long") val long: String? = null,
    @SerializedName("name") val name: String? = null,
    @SerializedName("address1") val address1: String? = null,
    @SerializedName("address2") val address2: String? = null,
    @SerializedName("fullAddress") val fullAddress: String? = null,
    @SerializedName("time") val time: String? = null,
    @SerializedName("distance") val distance: String? = null, // e.g: 1.0 KM
    @SerializedName("type") val type: String? = null,
    @SerializedName("index") val index: Number? = null, // array index
    @SerializedName("source") val source: String? = null, // returned from response of API
    @SerializedName("token") val token: String? = null, // returned from response of API
    @SerializedName("isCarPark") val isCarPark: Boolean = false, // returned from response of API
    @SerializedName("routablePoints") val routablePoints: Array<RoutablePoint> = arrayOf(),
    @SerializedName("carparkId") var carparkId: String? = null,
    @SerializedName("showAvailabilityFB") var showAvailabilityFB: Boolean = false,
    @SerializedName("availablePercentImageBand") var availablePercentImageBand: String? = null, // "TYPE2"
    @SerializedName("availabilityCSData") var availabilityCSData: AvailabilityCSData? = null,
    @SerializedName("hasAvailabilityCS") var hasAvailabilityCS: Boolean = false,
) : Serializable {
    fun convertToDestinationAddressDetails() = DestinationAddressDetails(
        addressid,
        address1,
        address2,
        lat,
        long,
        null,
        name,
        DestinationAddressTypes.EASY_BREEZIES,
        isCarPark = isCarPark,
        fullAddress = fullAddress,
        routableLocations = ArrayList(routablePoints.asList()),
        carParkID = carparkId,
        showAvailabilityFB = showAvailabilityFB
    )

    fun toRNWritableMap(): WritableMap = Arguments.createMap().apply {
        putString("placeId", placeId)
        addressid?.let { putInt("addressid", it) }
        putString("lat", lat)
        putString("long", long)
        putString("name", name)
        putString("address1", address1)
        putString("address2", address2)
        putString("time", time)
        putString("distance", distance)
        putString("type", type)
        (index as? Int)?.let { putInt("index", it) }
        putString("source", source)
        putString("token", token)
        putBoolean("isCarPark", isCarPark)
        putArray("routablePoints", Arguments.fromList(routablePoints.map { it.toRNWritableMap() }))
        putString("carparkId", carparkId)
        putBoolean("showAvailabilityFB", showAvailabilityFB)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as SearchLocation

        if (placeId != other.placeId) return false
        if (addressid != other.addressid) return false
        if (lat != other.lat) return false
        if (long != other.long) return false
        if (name != other.name) return false
        if (address1 != other.address1) return false
        if (address2 != other.address2) return false
        if (time != other.time) return false
        if (distance != other.distance) return false
        if (type != other.type) return false
        if (index != other.index) return false
        if (source != other.source) return false
        if (token != other.token) return false
        if (isCarPark != other.isCarPark) return false
        if (!routablePoints.contentEquals(other.routablePoints)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = placeId?.hashCode() ?: 0
        result = 31 * result + (addressid ?: 0)
        result = 31 * result + (lat?.hashCode() ?: 0)
        result = 31 * result + (long?.hashCode() ?: 0)
        result = 31 * result + (name?.hashCode() ?: 0)
        result = 31 * result + (address1?.hashCode() ?: 0)
        result = 31 * result + (address2?.hashCode() ?: 0)
        result = 31 * result + (time?.hashCode() ?: 0)
        result = 31 * result + (distance?.hashCode() ?: 0)
        result = 31 * result + (type?.hashCode() ?: 0)
        result = 31 * result + (index?.hashCode() ?: 0)
        result = 31 * result + (source?.hashCode() ?: 0)
        result = 31 * result + (token?.hashCode() ?: 0)
        result = 31 * result + isCarPark.hashCode()
        result = 31 * result + routablePoints.contentHashCode()
        return result
    }
}
package com.breeze.model.enums

enum class RoutePreference(val type: String) {

    FASTEST_ROUTE("Fastest"),
    CHEAPEST_ROUTE("Cheapest"),
    SHORTEST_ROUTE("Shortest"),

    FASTEST_CHEAPEST_ROUTE("Fastest & Cheapest"),
    SHORTEST_CHEAPEST_ROUTE("Shortest & Cheapest"),
    FASTEST_SHORTEST_ROUTE("Fastest & Shortest"),


    FASTEST_CHEAPEST_SHORTEST_ROUTE("Fastest & Cheapest & Shortest"),
    ALTERNATE_ROUTE("Alternate"),
}
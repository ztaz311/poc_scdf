package com.breeze.model.api.request

import com.breeze.model.RoutablePoint
import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ShortcutDetails(
    @SerializedName("address1")
    var address1: String? = null,

    @SerializedName("address2")
    var address2: String? = null,

    @SerializedName("lat")
    var lat: String? = null,

    @SerializedName("long")
    var long: String? = null,

    @SerializedName("distance")
    val distance: String? = null,

    @SerializedName("destinationName")
    var destinationName: String? = null,

    @SerializedName("name")
    var name: String? = null,

    @SerializedName("bookmarkId")
    var bookmarkId: Int? = null,

    @SerializedName("code")
    var code: String? = null,

    @SerializedName("shortcutName")
    var shortcutName: String? = null,

    @SerializedName("collectionId")
    var collectionId: Int? = null,

    @SerializedName("amenityType")
    val amenityType: String? = null,

    @SerializedName("amenityId")
    val amenityId: String? = null,

    @SerializedName("routablePoints")
    var routablePoints: ArrayList<RoutablePoint> = arrayListOf()

) : Serializable {
    fun parseRoutablePoints(data: HashMap<*, *>) {
        (data["routablePoints"] as? ArrayList<*>)?.takeIf { it.isNotEmpty() }?.let { mapData ->
            routablePoints = arrayListOf()
            mapData.forEach { item ->
                (item as? HashMap<*, *>)?.let { mapItem ->
                    routablePoints.add(
                        RoutablePoint(
                            lat = mapItem["lat"]?.toString()?.toDoubleOrNull() ?: 0.0,
                            long = mapItem["long"]?.toString()?.toDoubleOrNull() ?: 0.0,
                            type = mapItem["type"]?.toString() ?: "",
                            name = mapItem["name"]?.toString(),
                            carparkId = mapItem["carparkId"]?.toString(),
                            address1 = mapItem["address1"]?.toString(),
                            address2 = mapItem["address2"]?.toString(),
                            fullAddress = mapItem["fullAddress"]?.toString(),
                        )
                    )
                }
            }
        }
    }

    companion object {
        fun parseFromRNBookmarkData(hashMapData: HashMap<*, *>): ShortcutDetails = ShortcutDetails(
            address1 = hashMapData["address1"]?.toString() ?: "",
            address2 = hashMapData["address2"]?.toString() ?: "",
            lat = hashMapData["lat"]?.toString(),
            long = hashMapData["long"]?.toString(),
            distance = null,
            name = hashMapData["name"]?.toString() ?: "",
            bookmarkId = hashMapData["bookmarkId"]?.toString()?.toDoubleOrNull()?.toInt(),
            code = hashMapData["code"]?.toString(),
            shortcutName = hashMapData["shortcutName"]?.toString(),
            collectionId = hashMapData["collectionId"]?.toString()?.toDoubleOrNull()?.toInt()
        ).apply {
            parseRoutablePoints(data = hashMapData)
        }
    }
}
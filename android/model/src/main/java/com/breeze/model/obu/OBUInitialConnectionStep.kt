package com.breeze.model.obu

enum class OBUInitialConnectionStep {
    INTRO,
    VEHICLE_NUMBER,
    OUTRO
}
package com.breeze.model.api.response.amenities

import com.google.gson.annotations.SerializedName

data class CarParkDetailResponse(
    @SerializedName("success") var success: Boolean = false,
    @SerializedName("data") var data: BaseAmenity? = null,
)

data class CarparkAvailabilityResponse(
    @SerializedName("success") var success: Boolean = false,
    @SerializedName("data") var data: CarparkAvailabilityData? = null,
)

data class CarparkAvailabilityData(
    @SerializedName("id")
    var id: String? = null,
    @SerializedName("name")
    var name: String? = null,
    @SerializedName("availablePercentImageBand")
    var availablePercentImageBand: String? = null,
    @SerializedName("availablelot")
    var availablelot: Int? = null,
    @SerializedName("alternateCarparks")
    val alternateCarparks: List<BaseAmenity>? = null,
    @SerializedName("hasAvailabilityCS")
    val hasAvailabilityCS: Boolean = false,
    @SerializedName("navVoiceAlert")
    val navVoiceAlert: String? = null,
) {
    private fun isAvailableLotValid() = availablelot != null && availablelot!! > -1
    fun shouldShowParkingAvailabilityAlert() = isAvailableLotValid() || hasAvailabilityCS
}

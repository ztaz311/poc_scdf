package com.breeze.model.enums


/**
 * Denotes type of parking availability status of a carpark
 * */
enum class AmenityBand(val type: String) {
    /**
     * Car park is almost full
     * */
    TYPE0("TYPE0"),

    /**
     * Car park is < 15% available
     * */
    TYPE1("TYPE1"),

    /**
     * Car park is >= 15% available
     * */
    TYPE2("TYPE2");

    companion object {
        private val AMENITY_BAND_MAP: Map<String, AmenityBand> =
            AmenityBand.values().associate {
                return@associate Pair(it.type, it)
            }

        fun getAmenityBand(type: String?): AmenityBand {
            return AMENITY_BAND_MAP[type] ?: AmenityBand.TYPE2
        }
    }
}
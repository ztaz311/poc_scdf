package com.breeze.model.api.request


import android.os.Parcelable
import com.breeze.model.BaseTripItem
import com.breeze.model.api.response.BreezeWaypointCoordinates
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.io.Serializable


@Parcelize
data class TripPlannerRequest(
    @field:SerializedName("tripEstStartTime")
    override var tripEstStartTime: Long? = null,

    @field:SerializedName("tripEstArrivalTime")
    override var tripEstArrivalTime: Long? = null,

    @field:SerializedName("tripStartAddress1")
    override var tripStartAddress1: String? = null,

    @field:SerializedName("tripStartAddress2")
    override var tripStartAddress2: String? = null,

    @field:SerializedName("tripStartLat")
    var tripStartLat: Double? = null,

    @field:SerializedName("tripStartLong")
    var tripStartLong: Double? = null,


    @field:SerializedName("tripDestAddress1")
    override var tripDestAddress1: String? = null,

    @field:SerializedName("tripDestAddress2")
    override var tripDestAddress2: String? = null,


    @field:SerializedName("tripDestLat")
    var tripDestLat: Double? = null,

    @field:SerializedName("tripDestLong")
    var tripDestLong: Double? = null,


    @field:SerializedName("totalDuration")
    override var totalDuration: Double? = null,

    @field:SerializedName("totalDistance")
    override var totalDistance: Double? = null,

    @field:SerializedName("erpExpense")
    override var erpExpense: Double? = null,

    @field:SerializedName("tripPlanBy")
    override var tripPlanBy: String? = null,

    @field:SerializedName("erpEntries")
    var erpEntries: List<Int>? = null,

    @field:SerializedName("routeStops")
    var routeStops: List<BreezeWaypointCoordinates>? = null,

    @field:SerializedName("routeCoordinates")
    var routeCoordinates: List<List<Double>>? = null,

    @field:SerializedName("isDestinationCarpark")
    override var isDestinationCarpark: Boolean = false,

    @field:SerializedName("carParkId")
    override var carParkId: String? = null,

    @field:SerializedName("tripEtaFavouriteId")
    override var tripEtaFavouriteId: Int? = null,

    ) : BaseTripItem(), Serializable, Parcelable

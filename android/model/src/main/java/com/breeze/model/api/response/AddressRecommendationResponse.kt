package com.breeze.model.api.response

import android.os.Parcelable
import com.breeze.model.DestinationAddressTypes
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class AddressRecommendationResponse(
    @field:SerializedName("addresses")
    var addresses: MutableList<AddressesItem?>? = null
) : Parcelable

@Parcelize
data class AddressesItem(

    @field:SerializedName("address2")
    var address2: String? = null,

    @field:SerializedName("address1")
    var address1: String? = null,

    @field:SerializedName("lat")
    var lat: String? = null,

    @field:SerializedName("long")
    var long: String? = null,

    @field:SerializedName("addressid")
    val addressId: Int? = null,

    @field:SerializedName("name")
    var name: String? = null,

    var destinationAddressType: DestinationAddressTypes? = null,
    var ranking: Int? = null
) : Parcelable

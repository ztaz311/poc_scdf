package com.breeze.model.api.response

import com.google.gson.annotations.SerializedName


data class SettingNameBlacklist(
    @SerializedName("data" ) var data : ArrayList<String> = arrayListOf()
)
package com.breeze.model

/**
 * Map data with eta-fav-api.model.ts of React Native
 */
data class ETAFavourite(
    val tripEtaFavouriteId: Int?,
    val recipientName: String?,
    val recipientNumber: String?,
    val shareLiveLocation: String?,
    val voiceCallToRecipient: String?,
    val destination: String?,
    val tripDestLat: String?,
    val tripDestLong: String?,
    val message: String?,
    val module: String?,
    val count: Int?,
)
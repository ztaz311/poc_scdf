package com.breeze.model.api.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SettingsResponse(

    @field:SerializedName("settings")
    val settings: ArrayList<SettingsItem>
) : Parcelable

@Parcelize
data class SettingsItem(

    @field:SerializedName("attribute")
    val attribute: String,

    @field:SerializedName("value")
    val value: String,

    @field:SerializedName("attribute_code")
    val attribute_code: String
) : Parcelable

package com.breeze.model.api.request

class SaveOBUTransactionsRequest(
    val vehicleNumber: String?,
    val tripGUID: String?,
    val payments: List<OBUTransaction>
)

class OBUTransaction(
    val obuPaymentId: Int,
    val businessFunction: String,
    val paymentDate: String?,
    val paymentMode: String,
    val chargeAmount: Int
)
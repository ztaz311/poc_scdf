package com.breeze.model.obu

import android.os.Bundle
import androidx.core.os.bundleOf
import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class OBUTravelTimeData(

    @SerializedName("name")
    val name: String,

    @SerializedName("estTime")
    val estTime: String,

    @SerializedName("color")
    val color: String,

) : Serializable {

    fun toBundle(): Bundle = bundleOf(
        "name" to name,
        "estTime" to estTime,
        "color" to color,
    )

    companion object {
        const val EVENT_TYPE_TRAVEL_TIME = "TRAVEL_TIME"
    }
}

data class OBUTravelTimeDataAppSync(

    @SerializedName("eventType")
    val eventType: String,

    @SerializedName("travelTime")
    val travelTime: Array<OBUTravelTimeData>

)
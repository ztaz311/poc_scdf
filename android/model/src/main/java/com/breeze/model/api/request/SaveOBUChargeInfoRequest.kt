package com.breeze.model.api.request;

import com.google.gson.annotations.SerializedName

data class SaveOBUChargeInfoRequest(
    @SerializedName("tripGUID") var tripGUID: String? = null,
    @SerializedName("vehicleNumber") var vehicleNumber: String? = null,
    @SerializedName("lat") var lat: Double? = null,
    @SerializedName("long") var long: Double? = null,
    @SerializedName("chargingInfo") var chargingInfo: List<ChargingInfo> = listOf()
)

data class ChargingInfo(
    @SerializedName("businessFunction") var businessFunction: String? = null,
    @SerializedName("roadName") var roadName: String? = null,
    @SerializedName("chargingAmount") var chargingAmount: Int? = null,
    @SerializedName("chargingType") var chargingType: String? = null,
    @SerializedName("chargingMessageType") var chargingMessageType: String? = null,
    @SerializedName("content1") var content1: String? = null,
    @SerializedName("content2") var content2: String? = null,
    @SerializedName("content3") var content3: String? = null,
    @SerializedName("content4") var content4: String? = null,
    @SerializedName("content5") var content5: String? = null,
    @SerializedName("cardStatus") var cardStatus: String? = null,
    @SerializedName("startTime") var startTime: String? = null,
    @SerializedName("endTime") var endTime: String? = null,
    @SerializedName("cardBalance") var cardBalance: Double? = null,
)

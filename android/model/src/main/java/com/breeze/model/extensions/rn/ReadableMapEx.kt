package com.breeze.model.extensions.rn

import com.facebook.react.bridge.ReadableMap

fun ReadableMap.hasNonNullKey(key: String) = hasKey(key) && !isNull(key)

fun ReadableMap.getBooleanOrNull(key: String) = kotlin.runCatching {
    getBoolean(key)
}.getOrNull()

fun ReadableMap.getBoolean(key: String, default: Boolean) =
    this.takeIf { it.hasNonNullKey(key) }?.getBoolean(key) ?: default

fun ReadableMap.getDoubleOrNull(key: String) = kotlin.runCatching {
    getDouble(key)
}.getOrNull()

fun ReadableMap.getDouble(key: String, default: Double) =
    this.takeIf { it.hasNonNullKey(key) }?.getBoolean(key) ?: default

fun ReadableMap.getIntOrNull(key: String) = kotlin.runCatching {
    getInt(key)
}.getOrNull()

fun ReadableMap.getInt(key: String, default: Int) =
    this.takeIf { it.hasNonNullKey(key) }?.getInt(key) ?: default

fun ReadableMap.getMapOrNull(key: String) = this.takeIf { it.hasNonNullKey(key) }?.getMap(key)

fun ReadableMap.getStringOrNull(key: String) = this.takeIf { it.hasNonNullKey(key) }?.getString(key)

fun ReadableMap.getString(key: String, default: String) =
    this.takeIf { it.hasNonNullKey(key) }?.getString(key) ?: default

fun ReadableMap.getArrayOrNull(key: String) = this.takeIf { it.hasNonNullKey(key) }?.getArray(key)
package com.breeze.model.api.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.io.Serializable

@Parcelize
data class TripSummaryResponse(

    @field:SerializedName("isFeatureUsed")
    val isFeatureUsed: Boolean? = true,

    @field:SerializedName("totalItems")
    val totalItems: Int? = null,

    @field:SerializedName("trips")
    val trips: List<TripsItem> = emptyList(),

    @field:SerializedName("totalPages")
    val totalPages: Int? = null,

    @field:SerializedName("currentPage")
    val currentPage: Int? = null
) : Parcelable, Serializable

@Parcelize
data class TripsItem(

    @field:SerializedName("tripEndTime")
    val tripEndTime: Long? = null,

    @field:SerializedName("tripStartAddress1")
    val tripStartAddress1: String? = null,

    @field:SerializedName("tripGUID")
    val tripGUID: String? = null,

    @field:SerializedName("tripId")
    val tripId: Long,

    @field:SerializedName("tripStartTime")
    val tripStartTime: Long? = null,

    @field:SerializedName("totalDistance")
    val totalDistance: Double? = null,

    @field:SerializedName("tripDestAddress1")
    val tripDestAddress1: String? = null,

    @field:SerializedName("totalExpense")
    var totalExpense: Double? = null
) : Parcelable, Serializable

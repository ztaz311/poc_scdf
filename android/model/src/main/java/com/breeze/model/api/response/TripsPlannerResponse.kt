package com.breeze.model.api.response

import android.os.Parcelable
import com.breeze.model.BaseTripItem
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.io.Serializable

@Parcelize
data class TripsPlannerResponse(

    @field:SerializedName("isFeatureUsed")
    val isFeatureUsed: Boolean? = true,

    @field:SerializedName("totalItems")
    val totalItems: Int? = null,

    @field:SerializedName("trips")
    val trips: List<TripPlannerItem> = emptyList(),

    @field:SerializedName("totalPages")
    val totalPages: Int? = null,

    @field:SerializedName("currentPage")
    val currentPage: Int? = null
) : Parcelable, Serializable

@Parcelize
data class TripPlannerCreateResponse(

    @field:SerializedName("tripPlannerId")
    val tripPlannerId: Long? = null,

    @field:SerializedName("message")
    val message: String? = null,

    ) : Parcelable, Serializable


@Parcelize
data class TripPlannerItem(

    @field:SerializedName("tripEstStartTime")
    override val tripEstStartTime: Long? = null,

    @field:SerializedName("tripStartAddress1")
    override val tripStartAddress1: String? = null,

    @field:SerializedName("tripEstArrivalTime")
    override val tripEstArrivalTime: Long? = null,

    @field:SerializedName("tripPlannerId")
    val tripPlannerId: Long,

    @field:SerializedName("totalDuration")
    override val totalDuration: Double? = null,

    @field:SerializedName("totalDistance")
    override val totalDistance: Double? = null,

    @field:SerializedName("tripDestAddress1")
    override val tripDestAddress1: String? = null,

    @field:SerializedName("erpExpense")
    override var erpExpense: Double? = null,

    @field:SerializedName("tripPlanBy")
    override val tripPlanBy: String? = null,

    @field:SerializedName("isDestinationCarpark")
    override var isDestinationCarpark: Boolean = false,

    @field:SerializedName("carParkId")
    override var carParkId: String? = null,

    @SerializedName("tripEtaFavouriteId")
    override var tripEtaFavouriteId: Int? = null,

    @SerializedName("recipientNumber")
    var recipientNumber: String? = null,

    @SerializedName("recipientName")
    var recipientName: String? = null,

    @SerializedName("isEtaAvailable")
    var isEtaAvailable: Boolean = false,

    @field:SerializedName("tripStartAddress2")
    override var tripStartAddress2: String? = null,

    @field:SerializedName("tripDestAddress2")
    override val tripDestAddress2: String? = null,


    ) : BaseTripItem(), Parcelable, Serializable
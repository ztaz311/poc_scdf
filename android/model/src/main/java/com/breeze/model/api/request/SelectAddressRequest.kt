package com.breeze.model.api.request

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class SelectAddressRequest(
    @field:SerializedName("startAddress1")
    var startAddress1: String? = null,
    @field:SerializedName("startAddress2")
    var startAddress2: String? = null,
    @field:SerializedName("startLatitude")
    var startLatitude: String? = null,
    @field:SerializedName("startLongtitude")
    var startLongtitude: String? = null,

    @field:SerializedName("destAddress1")
    var destAddress1: String? = null,
    @field:SerializedName("destAddress2")
    var destAddress2: String? = null,
    @field:SerializedName("destLatitude")
    var destLatitude: String? = null,
    @field:SerializedName("destLongtitude")
    var destLongtitude: String? = null,
    @field:SerializedName("addressid")
    var addressid: String? = null,

    @field:SerializedName("ranking")
    var ranking: Int? = null,
    @field:SerializedName("source")
    var source: String? = null,
    @field:SerializedName("eventtime")
    var eventtime: Date? = null,
) : Parcelable
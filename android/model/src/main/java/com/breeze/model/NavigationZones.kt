package com.breeze.model

enum class NavigationZone(
    val zoneType: String,
    val priority: Int = 0,
    val maxDistanceMtr: Int = 300,
    val maxRouteRadius: Int = 3,
    val allDistanceThreshold: List<Int> = listOf(300)
) {
/*    SCHOOL_ZONE(0),
    SILVER_ZONE(1),
    YELLOW_DENGUE_ZONE(2),
    RED_DENGUE_ZONE(3)*/

    ERP_ZONE("erp", 100),
    FLASH_FLOOD(
        "flashflood",
        200,
        3000,
        maxRouteRadius = 20,
        allDistanceThreshold = listOf(1000, 3000)
    ),
    MAJOR_ACCIDENT("Major Accident", 201, 3000, allDistanceThreshold = listOf(1000, 3000)),
    HEAVY_TRAFFIC("heavytraffic", 202, 3000, allDistanceThreshold = listOf(1000, 3000)),
    SPEED_CAMERA("speedcamera", 301),
    SEASON_PARKING("seasonparking", 302),
    SCHOOL_ZONE("schoolzone", 400),
    SILVER_ZONE("silverzone", 401),
    ACCIDENT("Accident", 500),
    VEHICLE_BREAKDOWN("Vehicle breakdown", 501),
    ROAD_WORK("Roadwork", 502),
    OBSTACLE("Obstacle", 503),
    ROAD_BLOCK("Road Block", 504),
    MISCELLANEOUS("Miscellaneous", 505),
    UNATTENDED_VEHICLE("Unattended Vehicle", 506),
    TREE_PRUNING("treepruning", 507),
    ROAD_CLOSURE("roadclosure", 600, 3000, allDistanceThreshold = listOf(1000, 3000)),
    ROAD_DIVERSION("roaddiversion", 601),
    BUS_LANE("buslane", 602),
    YELLOW_DENGUE_ZONE("yellowdenguezone"),
    RED_DENGUE_ZONE("reddenguezone");


}

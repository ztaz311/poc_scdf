package com.breeze.model.api.response.notification

import com.google.gson.JsonElement
import com.google.gson.annotations.SerializedName

data class NotificationResponse(
    @SerializedName("totalItems") var totalItems: Int? = 0,
    @SerializedName("totalPages") var totalPages: Int? = 0,
    @SerializedName("notifications") var listNotification: List<NotificationItem> = arrayListOf()
)

data class NotificationItem(
    @SerializedName("notificationId")
    var notificationId: Int? = null,

    @SerializedName("category")
    var category: String? = null,

    @SerializedName("type")
    var type: String? = null,

    @SerializedName("title")
    var title: String? = null,

    @SerializedName("message")
    var message: String? = null,

    @SerializedName("description")
    var description: String? = null,

    @SerializedName("short_name")
    var short_name: String? = null,

    @SerializedName("latitude")
    var latitude: Double? = null,

    @SerializedName("longitude")
    var longitude: Double? = null,

    @SerializedName("startTime")
    var startTime: Int? = null,

    @SerializedName("expireTime")
    var expireTime: Int? = null,

    @SerializedName("imageLinkLight")
    var imageLinkLight: String? = null,

    @SerializedName("imageLinkDark")
    var imageLinkDark: String? = null,

    @SerializedName("priority")
    var priority: Int? = null,

    @SerializedName("showInappNotification")
    var isShowInAppNotification: Boolean? = null,

    @SerializedName("openScreen")
    var openScreen: String? = null,

    @SerializedName("screenProperties")
    var screenProperties: JsonElement? = null,
) {
    enum class ScreenName(val nameStr: String) {
        EXPLORE_MAP("EXPLORE_MAP"),
        VOUCHER("VOUCHER"),
    }

    fun getThemedIconUrl(isDarkTheme: Boolean) = if (isDarkTheme) imageLinkDark else imageLinkLight

    override fun equals(other: Any?): Boolean {
        if (this === other) {
            return true
        }
        if (javaClass != other?.javaClass) {
            return false
        }
        other as NotificationItem
        return notificationId == other.notificationId
    }
}


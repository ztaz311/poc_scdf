package com.breeze.model.constants

object AmenityType {
    const val EVCHARGER = "evcharger"
    const val CARPARK = "carpark"
    const val MUSEUM = "museum"
    const val HERITAGETREE = "heritagetree"
    const val PCN_CONST = "PCN"
    const val PETROL = "petrol"
    const val PARKS = "parks"
    const val LIBRARY = "library"
    const val PASAR_MALAM = "pasarmalam"
    const val POI = "poi"
    const val CLINIC = "clinic"
    const val FIREWORKS = "fireworks"

    /**
     * Explore map amenities like Durians Everywhere/Late-night Supper Spots/etc
     * */
    const val GENERAL = "GENERAL"
}

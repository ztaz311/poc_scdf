package com.breeze.model

data class CoordinateType(
    val coordinates: List<List<Double>>,
    val type: String
)
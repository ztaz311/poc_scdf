package com.breeze.model.api.request

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.io.Serializable

@Parcelize
data class EasyBreeziesDeleteRequest(
    @SerializedName("easybreezies")
    var easybreezies: List<EasyBreezieDelete?>? = null
) : Parcelable, Serializable

@Parcelize
data class EasyBreezieDelete(
    @SerializedName("addressid")
    var addressid: Int? = null,
) : Parcelable, Serializable

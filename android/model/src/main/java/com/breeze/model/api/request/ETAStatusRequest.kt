package com.breeze.model.api.request


import com.google.gson.annotations.SerializedName

data class ETAStatusRequest(
    @SerializedName("tripEtaUUID")
    val tripEtaUUID: String = "",
    @SerializedName("status")
    val status: String = ""
)



package com.breeze.model.api.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.io.Serializable


@Parcelize
data class BroadcastMessageResponse(
    @SerializedName("success") var success: Boolean = false,
    @SerializedName("message") var message: String = "",
    @SerializedName("notificationCriterias") var notificationCriterias: ArrayList<ValueConditionShowBroadcast>? = null,
): Parcelable

@Parcelize
data class ValueConditionShowBroadcast(
    @SerializedName("distance") var distance: Int? = null,
    @SerializedName("time") var time: Int? = null,
    @SerializedName("duration") var duration: Int = 15,
): Parcelable
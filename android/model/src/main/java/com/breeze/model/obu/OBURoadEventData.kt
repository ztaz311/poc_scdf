package com.breeze.model.obu

import androidx.core.os.bundleOf
import com.google.gson.annotations.SerializedName
import com.breeze.model.api.request.ChargingInfo
import com.breeze.model.api.request.OBUTransaction
import com.breeze.model.api.request.SaveOBUTripSummaryRequest

data class OBURoadEventData(
    @SerializedName("eventType") val eventType: String,
    @SerializedName("message") val message: String?,
    @SerializedName("distance") val distance: String?,
    @SerializedName("detailTitle") val detailTitle: String?,
    @SerializedName("detailMessage") val detailMessage: String?,
    @SerializedName("chargeAmount") val chargeAmount: String?,
    @SerializedName("spokenText") var spokenText: String? = null,
    @SerializedName("chargingInfo") var chargingInfo: List<ChargingInfo>? = null,
    @SerializedName("tripSummary") var tripSummary: SaveOBUTripSummaryRequest? = null,
    @SerializedName("payments") var payments: List<OBUTransaction>? = null,
) {
    var startTimeAnalytic: String? = null
    var endTimeAnalytic: String? = null
    fun toBundle() = bundleOf(
        "type" to eventType,
        "message" to message,
        "distance" to distance,
        "detailTitle" to detailTitle,
        "detailMessage" to detailMessage,
        "chargeAmount" to chargeAmount,
        "chargeAmount" to chargeAmount,
        "spokenText" to spokenText,
    )

    fun shouldCheckLowCardBalance() = eventType == EVENT_TYPE_ERP_SUCCESS  || eventType == EVENT_TYPE_PARKING_SUCCESS

    companion object {
        const val EVENT_TYPE_TRAFFIC = "TRAFFIC"
        const val EVENT_TYPE_SCHOOL_ZONE = "SCHOOL_ZONE"
        const val EVENT_TYPE_SILVER_ZONE = "SILVER_ZONE"
        const val EVENT_TYPE_ERP_CHARGING = "ERP_CHARGING"
        const val EVENT_TYPE_ERP_SUCCESS = "ERP_SUCCESS"
        const val EVENT_TYPE_ERP_FAILURE = "ERP_FAILURE"
        const val EVENT_TYPE_PARKING_FEE = "PARKING_FEE"
        const val EVENT_TYPE_PARKING_SUCCESS = "PARKING_SUCCESS"
        const val EVENT_TYPE_PARKING_FAILURE = "PARKING_FAILURE"
        const val EVENT_TYPE_ROAD_CLOSURE = "ROAD_CLOSURE"
        const val EVENT_TYPE_EVENT = "EVENT"
        const val EVENT_TYPE_ROAD_DIVERSION = "ROAD_DIVERSION"
        const val EVENT_TYPE_GENERAL_MESSAGE = "GENERAL_MESSAGE"
        const val EVENT_TYPE_SPEED_CAMERA = "SPEED_CAMERA"
        const val EVENT_TYPE_FLASH_FLOOD = "FLASH_FLOOD"
        const val EVENT_TYPE_HEAVY_TRAFFIC = "HEAVY_TRAFFIC"
        const val EVENT_TYPE_UNATTENDED_VEHICLE = "UNATTENDED_VEHICLE"
        const val EVENT_TYPE_MISCELLANEOUS = "MISCELLANEOUS"
        const val EVENT_TYPE_ROAD_BLOCK = "ROAD_BLOCK"
        const val EVENT_TYPE_OBSTACLE = "OBSTACLE"
        const val EVENT_TYPE_ROAD_WORK = "ROAD_WORK"
        const val EVENT_TYPE_VEHICLE_BREAKDOWN = "VEHICLE_BREAKDOWN"
        const val EVENT_TYPE_MAJOR_ACCIDENT = "MAJOR_ACCIDENT"
        const val EVENT_TYPE_SEASON_PARKING = "SEASON_PARKING"
        const val EVENT_TYPE_TREE_PRUNING = "TREE_PRUNING"
        const val EVENT_TYPE_YELLOW_DENGUE_ZONE = "YELLOW_DENGUE_ZONE"
        const val EVENT_TYPE_RED_DENGUE_ZONE = "RED_DENGUE_ZONE"
        const val EVENT_TYPE_BUS_LANE = "BUS_LANE"
        const val EVENT_TYPE_LOW_CARD = "LOW_CARD" // to sent to RN only

        const val ICON_TYPE_SCHOOL_ZONE = "2002"
        const val ICON_TYPE_SILVER_ZONE = "2003"
        const val ICON_TYPE_BUS_LANE = "2004"
        const val ICON_TYPE_SPEED_CAMERA = "9011"
        const val ICON_TYPE_ACCIDENT = "1412"
        const val ICON_TYPE_EVENT = "1410"
    }
}
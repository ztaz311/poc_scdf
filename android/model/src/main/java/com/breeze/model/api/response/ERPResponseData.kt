package com.breeze.model.api.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.io.Serializable
import java.util.Calendar

class ERPResponseData : Serializable {
    @Parcelize
    data class ERPResponse(
        @SerializedName("erprates")
        val erprates: ArrayList<Erprate>,

        @SerializedName("erp")
        val erps: ArrayList<Erp>,

        @SerializedName("publicholiday")
        val publicholiday: ArrayList<PublicholidayItem>
    ) : Parcelable

    @Parcelize
    data class Erprate(
        @SerializedName("rates")
        val rates: ArrayList<Rate>,

        @SerializedName("zoneid")
        val zoneid: String,

        @SerializedName("revised")
        val revised: ArrayList<RevisedRate>?,
    ) : Parcelable

    @Parcelize
    data class RevisedRate(
        @SerializedName("date")
        val date: String,

        @SerializedName("rates")
        val rates: ArrayList<Rate>
    ) : Parcelable

    @Parcelize
    data class Rate(
        @SerializedName("chargeamount")
        val chargeamount: Float,

        @SerializedName("daytype")
        val daytype: String,

        @SerializedName("effectivedate")
        val effectivedate: String,

        @SerializedName("endtime")
        val endtime: String,

        @SerializedName("starttime")
        val starttime: String,

        @SerializedName("vehicletype")
        val vehicletype: String,

        @SerializedName("isRevised")
        val isRevised: Boolean?
    ) : Parcelable

    @Parcelize
    data class Erp(
        @SerializedName("endlat")
        val endlat: String,

        @SerializedName("endlong")
        val endlong: String,

        @SerializedName("erpid")
        val erpid: Int,

        @SerializedName("name")
        val name: String,

        @SerializedName("startlat")
        val startlat: String,

        @SerializedName("startlong")
        val startlong: String,

        @SerializedName("zoneid")
        val zoneid: String
    ) : Parcelable

    @Parcelize
    data class PublicholidayItem(
        @SerializedName("date")
        val date: String,

        @SerializedName("EndTime")
        val endTime: String,

        @SerializedName("StartTime")
        val startTime: String,

        @SerializedName("ChargeAmount")
        val chargeAmount: Float
    ) : Parcelable
}

object ERP {
    object DayType {
        const val WEEKDAYS = "Weekdays"
        const val SATURDAY = "Saturday"
    }
}

fun ERPResponseData.Rate.checkCorrectDayType(calendar: Calendar): Boolean {
    val isWeekDay =
        !(calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY)
    val isSaturday = calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY
    return (isWeekDay && daytype == ERP.DayType.WEEKDAYS) || (!isWeekDay && isSaturday && daytype == ERP.DayType.SATURDAY)
}


package com.breeze.model.api.response

import com.google.gson.annotations.SerializedName

data class ImageTypes(
    @SerializedName("imageType") var imageType: String? = null,
    @SerializedName("map_icon_light_selected_url") var map_icon_light_selected_url: String? = null,
    @SerializedName("map_icon_light_unselected_url") var map_icon_light_unselected_url: String? = null,
    @SerializedName("map_icon_dark_selected_url") var map_icon_dark_selected_url: String? = null,
    @SerializedName("map_icon_dark_unselected_url") var map_icon_dark_unselected_url: String? = null,
)
package com.breeze.model

data class FirebaseDB(
    var description: String,
    var fileDownloadURL: String,
    var deviceOS: String,
    var deviceOSVersion: String,
    var deviceModel: String,
    var deviceManufacturer: String,
    var appVersion: String,
    var time: String,
    var secondaryFirebaseID: String?,
    var fcmId: String?,
)
package com.breeze.model.api.request


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.io.Serializable


@Parcelize
data class ETARequest(
    @SerializedName("type")
    var type: String = "",
    @SerializedName("tripEtaFavouriteId")
    var tripEtaFavouriteId: Int? = null,
    @SerializedName("tripEtaUUID")
    var tripEtaUUID: String? = null,
    @SerializedName("tripEndTime")
    var tripEndTime: Long? = null,
    @SerializedName("shareLiveLocation")
    var shareLiveLocation: String? = null,
    @SerializedName("destination")
    var destination: String? = null,
    @SerializedName("recipientName")
    var recipientName: String? = null,
    @SerializedName("tripStartTime")
    var tripStartTime: Long? = null,
    @SerializedName("tripDestLong")
    var tripDestLong: Double? = null,
    @SerializedName("message")
    var message: String? = null,
    @SerializedName("tripDestLat")
    var tripDestLat: Double? = null,
    @SerializedName("recipientNumber")
    var recipientNumber: String? = null,
    @SerializedName("destinationAddress1")
    var destinationAddress1: String? = null,
    @SerializedName("voiceCallToRecipient")
    var voiceCallToRecipient: String? = null,
    var arrivalTime: String = "",
    @SerializedName("coordinates")
    var coordinates: String = "",
) : Serializable, Parcelable
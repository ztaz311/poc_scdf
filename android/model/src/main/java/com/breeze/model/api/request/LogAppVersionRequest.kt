package com.breeze.model.api.request

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class LogAppVersionRequest(
    @field:SerializedName("deviceos")
    var deviceos: String? = null,
    @field:SerializedName("appversion")
    var appversion: String? = null
) : Parcelable
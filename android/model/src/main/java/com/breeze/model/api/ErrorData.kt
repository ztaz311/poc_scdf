package com.breeze.model.api

class ErrorData(
    var message: String = "",
    var errorCode: String = "",
    var throwable: Throwable? = null
) {
}
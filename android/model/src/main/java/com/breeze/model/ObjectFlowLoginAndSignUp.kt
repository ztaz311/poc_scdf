package com.breeze.model

import com.breeze.model.constants.Constants
import com.breeze.model.constants.MODE_LOGIN_TYPE

/**
 * Map data with eta-fav-api.model.ts of React Native
 */
class ObjectFlowLoginAndSignUp {
    var mode: Int = MODE_LOGIN_TYPE.CREATE_ACCOUNT
    var nextStepSignInWithPhoneNumber = Constants.AMPLIFY_CONSTANTS.SIGN_IN_WITH_CUSTOM_CHALLENGE
    var isUserNameAvailable: Boolean = false
}

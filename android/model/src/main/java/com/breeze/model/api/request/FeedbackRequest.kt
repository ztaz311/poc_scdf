package com.breeze.model.api.request


import com.google.gson.annotations.SerializedName

data class TypeItem(
    @SerializedName("issuetype")
    val issuetype: String = "",
    @SerializedName("lat")
    val lat: String = "",
    @SerializedName("long")
    val long: String = ""
)


data class FeedbackRequest(
    @SerializedName("issuedescription")
    var issuedescription: String = "",
    @SerializedName("type")
    val type: ArrayList<TypeItem> = arrayListOf<TypeItem>()
)



package com.breeze.model.extensions

import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale


fun String.toDate(format: String): Date? {
    return kotlin.runCatching {
        SimpleDateFormat(format, Locale.getDefault()).parse(this)
    }.getOrNull()
}

fun String.changeStringDateFormat(fromFormat: String, toFormat: String): String? {
    val fromSdf = SimpleDateFormat(fromFormat, Locale.getDefault())
    val toSdf = SimpleDateFormat(toFormat, Locale.getDefault())
    return kotlin.runCatching {
        fromSdf.parse(this)?.let {
            toSdf.format(it)
        }
    }.getOrNull()
}

fun String.highlightWords(world: String, color: Int, shouldBold: Boolean = false): Spannable? {
    val spannable: Spannable = SpannableString(this)
    val boldSpan = StyleSpan(android.graphics.Typeface.BOLD)
    val foregroundColorSpan = ForegroundColorSpan(color)

    var ofe = this.indexOf(world, 0)
    var ofs = 0
    while (ofs < this.length && ofe != -1) {
        ofe = this.indexOf(world, ofs)
        if (ofe == -1) break else {
            spannable.setSpan(
                foregroundColorSpan,
                ofe,
                ofe + world.length,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
            )
            if (shouldBold) {
                spannable.setSpan(
                    boldSpan,
                    ofe,
                    ofe + world.length,
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                )
            }
        }
        ofs = ofe + 1
    }
    //new BackgroundColorSpan(0xFFFFFF00)
    //tv.setText(wordToSpan, TextView.BufferType.SPANNABLE);
    return spannable
}

fun String.hasExcludedRoadNameCharacters(): Boolean =
    Regex("[\\u0b80-\\u0bFF|\\u4e00-\\u9fff/]").containsMatchIn(this)

fun String?.safeDouble(): Double {
    return try {
        this?.toDouble() ?: 0.0
    } catch (e: Exception) {
        0.0
    }
}
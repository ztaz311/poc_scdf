package com.breeze.model.enums

enum class AmplifyIdentityProviderType(val type: String) {
    GOOGLE("Google"),
    FACEBOOK("Facebook"),
    AMPLIFY_DEFAULT("Default");

    fun getProviderType(typeName: String): AmplifyIdentityProviderType {
        for (item in values()) {
            if (type == typeName) {
                return item
            }
        }
        return AMPLIFY_DEFAULT
    }
}
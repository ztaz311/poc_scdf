package com.breeze.model.api.response

import com.google.gson.annotations.SerializedName

data class AvailabilityAlertResponse(
    @SerializedName("success")
    val success: Boolean = false,
    @SerializedName("data")
    val data: AvailabilityAlertData? = null
)

data class AvailabilityAlertData(
    @SerializedName("availablePercentImageBand")
    val availablePercentImageBand: String? = null,
    @SerializedName("hasAvailabilityCS")
    val hasAvailabilityCS: Boolean = false,
    @SerializedName("navVoiceAlert")
    val navVoiceAlert: String = "",
)
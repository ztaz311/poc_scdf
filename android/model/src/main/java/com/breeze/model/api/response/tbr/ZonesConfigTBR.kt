package com.breeze.model.api.response.tbr

import com.mapbox.geojson.Point


class ZonesConfigTBR {
    var activeZoomLevel: Int = 17
    var deactiveZoomLevel: Int = 15
    var radius: Double = 1.0
        set(value) {
            value?.div(1000)
        }
    var zoneId: String = ""
    var centerPoint: Point? = null
    var color: String? = "#E515B765"
}
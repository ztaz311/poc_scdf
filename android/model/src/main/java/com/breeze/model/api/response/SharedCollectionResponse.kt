package com.breeze.model.api.response

import com.breeze.model.SharedCollection
import com.google.gson.annotations.SerializedName

data class SharedCollectionResponse(
    @SerializedName("success")
    var success: Boolean,

    @SerializedName("data")
    var data: SharedCollection,
)
package com.breeze.model.api.response.notification

enum class NotificationCategoryEnum(val text: String) {
    CATEGORY_ADMIN("ADMIN"),
    CATEGORY_TRAFFIC_ALERTS("TRAFFIC_ALERTS"),
    TUTORIAL("TUTORIAL"),
    CUSTOM_INBOX("CUSTOM_INBOX"),
}

package com.breeze.model.common

import androidx.core.os.bundleOf
import com.facebook.react.bridge.Arguments
import com.facebook.react.bridge.WritableMap

data class UserStoredData(
    var cognitoID: String,
    var carplateNumber: String? = null,
    var iuNumber: String? = null,
    var userName: String = "",
    var email: String = ""
) {
    fun toRNMap(): WritableMap = Arguments.fromBundle(
        bundleOf(
            "cognitoID" to cognitoID,
            "carplateNumber" to (carplateNumber ?: ""),
            "iuNumber" to (iuNumber ?: ""),
            "userName" to userName,
            "email" to email,
        )
    )
}
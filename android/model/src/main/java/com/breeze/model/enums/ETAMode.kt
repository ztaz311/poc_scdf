package com.breeze.model.enums

enum class ETAMode(mode: String) {
    Cruise("Cruise"),
    Navigation("Navigation"),
    RoutePlanning("RoutePlanning")
}
package com.breeze.model.common

import com.breeze.model.enums.RoutePreference
import com.breeze.model.enums.UserTheme

data class UserPreference(
    var userName: String,
    var userEmail: String,
    var amplifyProvider: String,
    var preferredTheme: String,
    var currentTheme: String = UserTheme.LIGHT.type,
    var trafficIncidentMap: HashMap<String, TrafficIncidentPreference>,
    var playNavigationVoice: Boolean,
    var routePreference: String? = RoutePreference.FASTEST_ROUTE.type,
    var emailUpdatePopupCount: Int = 0,
    var isUserEmailVerified: Boolean = false,
    var userPhoneNumber: String = "",
    var instabugGroup: String? = null,
    var onBoardingState: String? = null,
    var userCreatedDate: Long? = 0,
    var userId: String? = null,
    var isGuestMode: Boolean = false,
    var emailGuestUser: String = "",
    var customEmail: String = "",
    var isCustomEmailVerified: Boolean = false,
)


data class TrafficIncidentPreference(
    var incidentName: String,
    var showNotification: Boolean,
    var showIcon: Boolean
)
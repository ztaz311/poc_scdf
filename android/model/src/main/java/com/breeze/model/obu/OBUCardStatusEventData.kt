package com.breeze.model.obu

import com.google.gson.annotations.SerializedName

data class OBUCardStatusEventData(

    @SerializedName("status")
    val status: String,

    /**
     * Card's balance in Cent
     * */
    @SerializedName("balance")
    val balance: Double,
) {

    companion object {
        const val EVENT_TYPE_CARD_ERROR = "CARD_ERROR"
        const val EVENT_TYPE_CARD_EXPIRED = "CARD_EXPIRED"
        const val EVENT_TYPE_CARD_INSUFFICIENT = "CARD_INSUFFICIENT"
        const val EVENT_TYPE_CARD_VALID = "CARD_VALID"
        const val EVENT_TYPE_NO_CARD = "NO_CARD"
    }
}

data class OBUCardStatusDataAppSync(
    @SerializedName("eventType")
    val eventType: String,

    /**
     * Card's balance in Cent
     * */
    @SerializedName("balance")
    val balance: Double,

    @SerializedName("chargeAmount")
    val chargeAmount: String,
) {
    fun toOBUCardStatusEventData() = OBUCardStatusEventData(
        status = eventType,
        balance = balance
    )
}
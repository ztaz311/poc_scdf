package com.breeze.model.api

import com.google.gson.annotations.SerializedName

data class ErrorDataResponse(
    @SerializedName("message") var message: String? = null,
    @SerializedName("error_code") var errorCode: String? = null
)
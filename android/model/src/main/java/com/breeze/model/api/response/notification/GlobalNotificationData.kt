package com.breeze.model.api.response.notification

data class GlobalNotificationData(
    var notificationItem: NotificationItem,
    var seenNotifications: Array<SeenNotification>
) {
    fun generateTitle(): String? = if (seenNotifications.size > 1)
        notificationItem.short_name + " and ${seenNotifications.size - 1} other alerts"
    else
        notificationItem.title

    fun generateTitle(defaultValue: String): String = generateTitle() ?: defaultValue

    fun isGroupNotification() = seenNotifications.size > 1

    fun getThemedImageURL(isDarkTheme: Boolean) = notificationItem.getThemedIconUrl(isDarkTheme)
}

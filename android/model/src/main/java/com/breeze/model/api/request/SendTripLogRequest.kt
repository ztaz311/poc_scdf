package com.breeze.model.api.request

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SendTripLogRequest(
    @field:SerializedName("type")
    var type: String = "BY_TRIP",
    @field:SerializedName("tripIds")
    var tripIds: ArrayList<Long> = arrayListOf()
) : Parcelable

package com.breeze.model.api.response.eta

import com.google.gson.annotations.SerializedName


data class RecentETAContact(
    @SerializedName("tripEtaFavouriteId")
    var tripEtaFavouriteId: Int? = null,
    @SerializedName("recipientName")
    var recipientName: String? = null,
    @SerializedName("recipientNumber")
    var recipientNumber: String? = null,
    @SerializedName("lastETATime")
    var lastETATime: Long? = null,
)
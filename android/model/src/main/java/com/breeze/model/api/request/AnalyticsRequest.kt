package com.breeze.model.api.request

import com.google.gson.annotations.SerializedName

class AnalyticsRequest {

    @field:SerializedName("event_name")
    var eventName: String = ""

    @field:SerializedName("event_value")
    var eventValue: String = ""

    @field:SerializedName("screen_name")
    var screenName: String = ""

    @field:SerializedName("event_timestamp")
    var eventTimestamp: Long = 0

    @field:SerializedName("amenity_id")
    var amenityId: String = ""

    @field:SerializedName("layer_code")
    var layerCode: String = ""

    @field:SerializedName("amenity_type")
    var amenityType: String = ""
}
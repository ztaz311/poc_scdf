package com.breeze.model.api.response.notification

import com.google.gson.annotations.SerializedName

class SeenNotification {
    @SerializedName("notificationId")
    var notificationId: Int? = null

    @SerializedName("startTime")
    var startTime: Int? = null

    @SerializedName("expireTime")
    var expireTime: Int? = null
}
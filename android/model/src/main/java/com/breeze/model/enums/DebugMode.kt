package com.breeze.model.enums

enum class DebugMode(val value: Int) {
    OFF(0),
    USER_ENABLED(1),
    BE_ENABLED(2);

    companion object {
        fun isDebugOn(modeValue: Int?) =
            modeValue == USER_ENABLED.value || modeValue == BE_ENABLED.value

        fun extractMode(modeValue: Int?, defaultValue: DebugMode = OFF): DebugMode {
            return DebugMode.values().find { it.value == modeValue } ?: defaultValue
        }
    }
}

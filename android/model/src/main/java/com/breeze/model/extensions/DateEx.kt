package com.breeze.model.extensions

import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

fun Date.formatDate() =
    DateFormat.getDateInstance(DateFormat.DEFAULT, Locale.getDefault()).format(this)

fun Date.formatDate(format: String): String? {
    kotlin.runCatching {
        return SimpleDateFormat(format, Locale.getDefault()).format(this)
    }
    return null
}



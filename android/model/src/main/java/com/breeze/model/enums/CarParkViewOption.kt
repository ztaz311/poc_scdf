package com.breeze.model.enums

enum class CarParkViewOption(val mode: String) {
    ALL_NEARBY("ALL_CARPARKS"),
    AVAILABLE_ONLY("TYPE2_CARPARKS"),
    HIDE("HIDE_CARPARKS");

    fun isShowingCarParks() = this != HIDE
}
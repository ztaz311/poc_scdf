package com.breeze.model.obu

import android.os.Bundle
import androidx.core.os.bundleOf
import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class OBUParkingAvailability(
    @SerializedName("name")
    val name: String,

    @SerializedName("availableLots")
    val availableLots: String,

    @SerializedName("color")
    val color: String,

) : Serializable {
    fun toBundle(): Bundle = bundleOf(
        "name" to name,
        "availableLots" to availableLots,
        "color" to color,
    )

    companion object {
        const val EVENT_TYPE_PARKING_AVAILABILITY = "PARKING_AVAILABILITY"
    }
}

data class OBUParkingAvailabilitiesAppSync(

    @SerializedName("eventType")
    val eventType: String,

    @SerializedName("carparks")
    val carparks: Array<OBUParkingAvailability>
)
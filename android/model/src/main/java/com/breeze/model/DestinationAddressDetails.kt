package com.breeze.model

import android.location.Location
import com.breeze.model.api.response.amenities.AvailabilityCSData
import com.google.gson.annotations.SerializedName
import com.mapbox.geojson.Point
import java.io.Serializable

data class DestinationAddressDetails(
    @SerializedName("easyBreezyAddressID")
    var easyBreezyAddressID: Int? = null,

    @SerializedName("address1")
    var address1: String? = null,

    @SerializedName("address2")
    var address2: String? = null,

    @SerializedName("lat")
    var lat: String? = null,

    @SerializedName("long")
    var long: String? = null,

    @SerializedName("distance")
    val distance: String? = null,

    @SerializedName("destinationName")
    var destinationName: String? = null,

    @SerializedName("destinationAddressType")
    var destinationAddressType: DestinationAddressTypes? = null,

    @SerializedName("ranking")
    var ranking: Int? = null,

    @SerializedName("isCarPark")
    var isCarPark: Boolean = false,

    @SerializedName("carParkID")
    var carParkID: String? = null,

    @SerializedName("availablePercentImageBand")
    var availablePercentImageBand: String? = null,

    @SerializedName("code")
    var code: String? = null,

    @SerializedName("shortcutName")
    var shortcutName: String? = null,

    @SerializedName("collectionId")
    var collectionId: Int? = null,

    @SerializedName("amenityId")
    var amenityId: String? = null,

    @SerializedName("layerCode")
    var layerCode: String? = null,

    @SerializedName("trackNavigation")
    var trackNavigation: Boolean = false,

    @SerializedName("amenityType")
    var amenityType: String? = null,

    @SerializedName("routableLocations")
    var routableLocations: ArrayList<RoutablePoint> = arrayListOf(),

    @SerializedName("selectedPinIndex")
    var selectedPinIndex: Int = -1,

    /**
     * for carpark availability
     */
    @SerializedName("showAvailabilityFB")
    var showAvailabilityFB: Boolean = false,

    @SerializedName("hasAvailabilityCS")
    var hasAvailabilityCS: Boolean = false,

    @SerializedName("availabilityCSData")
    var availabilityCSData: AvailabilityCSData? = null,

    @SerializedName("fullAddress")
    var fullAddress: String? = null,

    @SerializedName("placeId")
    var placeId: String? = null,

    @SerializedName("userLocationLinkIdRef")
    var userLocationLinkIdRef: String? = null,
) : Serializable {
    fun getRoutableLocation(): Location? {
        val selectedRoutablePoint = getSelectedRoutablePoint()
        val routablePointLat =
            if (selectedRoutablePoint != null) routableLocations[selectedPinIndex].lat else this@DestinationAddressDetails.lat?.toDoubleOrNull()
        val routablePointLong =
            if (selectedRoutablePoint != null) routableLocations[selectedPinIndex].long else this@DestinationAddressDetails.long?.toDoubleOrNull()

        if (routablePointLat != null && routablePointLong !== null)
            return Location("").apply {
                latitude = routablePointLat
                longitude = routablePointLong
            }
        return null
    }

    fun getRoutablePointLatLong(defaultLat: Double = 0.0, defaultLong: Double = 0.0): Point {
        val routableLocation = getRoutableLocation()
        return Point.fromLngLat(
            routableLocation?.longitude ?: defaultLong,
            routableLocation?.latitude ?: defaultLat
        )
    }

    fun getSelectedRoutablePoint() =
        if (selectedPinIndex >= 0 && routableLocations.size > selectedPinIndex) routableLocations[selectedPinIndex] else null

    fun getSelectedCarParkID(): String? {
        val selectedRoutablePoint = getSelectedRoutablePoint()
        return if (selectedRoutablePoint == null) carParkID else selectedRoutablePoint.carparkId
    }

    fun isDestinationCarPark(): Boolean {
        val selectedRoutablePoint = getSelectedRoutablePoint()
        val carParkID =
            if (selectedRoutablePoint == null) carParkID else selectedRoutablePoint.carparkId
        return !carParkID.isNullOrEmpty()
    }

    fun parseRoutablePoints(data: HashMap<String, Any>) {
        (data["routablePoints"] as? ArrayList<*>)?.takeIf { it.isNotEmpty() }?.let { mapData ->
            routableLocations = arrayListOf()
            mapData.forEach { item ->
                (item as? HashMap<*, *>)?.let { mapItem ->
                    routableLocations.add(
                        RoutablePoint(
                            lat = mapItem["lat"]?.toString()?.toDoubleOrNull() ?: 0.0,
                            long = mapItem["long"]?.toString()?.toDoubleOrNull() ?: 0.0,
                            type = mapItem["type"]?.toString() ?: "",
                            name = mapItem["name"]?.toString(),
                            carparkId = mapItem["carparkId"]?.toString(),
                            address1 = mapItem["address1"]?.toString(),
                            address2 = mapItem["address2"]?.toString(),
                            fullAddress = mapItem["fullAddress"]?.toString(),
                        )
                    )
                }
            }
        }
    }

    companion object {
        fun parseSearchLocationDataFromRN(data: HashMap<String, Any>) =
            DestinationAddressDetails(
                easyBreezyAddressID = data["addressid"].toString().toIntOrNull(),
                address1 = (data["address1"] ?: "").toString(),
                address2 = (data["address2"] ?: "").toString(),
                lat = (data["lat"] ?: "").toString(),
                long = (data["long"] ?: "").toString(),
                distance = null,
                destinationName = (data["name"] ?: "").toString(),
                destinationAddressType = DestinationAddressTypes.getDestinationAddressType(
                    data["type"].toString()
                ),
                selectedPinIndex = data["selectedPinIndex"]?.toString()?.toDoubleOrNull()?.toInt() ?: -1,
                ranking = data["selectedIndex"]?.toString()?.toIntOrNull(),
                carParkID = (data["carparkId"])?.toString(),
                isCarPark = (data["carparkId"] ?: "").toString().isNotEmpty(),
                fullAddress = (data["fullAddress"] ?: "").toString(),
                hasAvailabilityCS = data["availabilityCSData"] != null,
                availablePercentImageBand = data["availablePercentImageBand"]?.toString(),
                availabilityCSData = (data["availabilityCSData"] as? HashMap<String, Any>)
                    ?.let { mapData -> AvailabilityCSData.fromRNHashMapData(mapData) },
                showAvailabilityFB = (data["showAvailabilityFB"] as? Boolean ?: false),
                placeId = (data["placeId"] ?: "").toString(),
                userLocationLinkIdRef = (data["userLocationLinkIdRef"])?.toString(),
            ).apply {
                parseRoutablePoints(data)
            }
    }
}

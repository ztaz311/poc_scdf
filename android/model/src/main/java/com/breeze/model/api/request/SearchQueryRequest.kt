package com.breeze.model.api.request

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.io.Serializable

@Parcelize
data class SearchQueryRequest(
    @field:SerializedName("deviceosversion")
    var deviceosversion: String? = null,
    @field:SerializedName("deviceos")
    var deviceos: String? = null,
    @field:SerializedName("appversion")
    var appversion: String? = null,
    @field:SerializedName("lat")
    var lat: String? = null,
    @field:SerializedName("long")
    var long: String? = null,
    @field:SerializedName("token")
    var token: String? = null,
    @field:SerializedName("devicemodel")
    var devicemodel: String? = null,
    @field:SerializedName("query")
    var query: String? = null
) : Parcelable, Serializable
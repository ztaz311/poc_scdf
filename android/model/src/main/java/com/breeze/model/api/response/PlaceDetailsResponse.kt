package com.breeze.model.api.response

import com.breeze.model.RoutablePoint
import com.breeze.model.api.response.amenities.AvailabilityCSData
import com.google.gson.annotations.SerializedName

class PlaceDetailsResponse {
    @SerializedName("bookmarkId")
    var bookmarkId: Int? = null

    @SerializedName("lat")
    var lat: String? = null

    @SerializedName("long")
    var long: String? = null

    @SerializedName("name")
    var name: String? = null

    @SerializedName("description")
    var description: String? = null

    @SerializedName("address1")
    var address1: String? = null

    @SerializedName("address2")
    var address2: String? = null

    @SerializedName("amenityType")
    var amenityType: String? = null

    @SerializedName("amenityId")
    var amenityId: String? = null

    @SerializedName("isDestination")
    var isDestination: Boolean? = null

    @SerializedName("code")
    var code: String? = null

    @SerializedName("collectionId")
    var collectionId: Int? = null

    @SerializedName("isSelected")
    var isSelected: Boolean? = null

    @SerializedName("routablePoints")
    var routablePoints: Array<RoutablePoint>? = null

    @SerializedName("fullAddress")
    var fullAddress: String? = null

    @SerializedName("availablePercentImageBand")
    var availablePercentImageBand: String? = null

    @SerializedName("userLocationLinkIdRef")
    var userLocationLinkIdRef: String? = null

    @SerializedName("placeId")
    var placeId: String? = null

    @SerializedName("availabilityCSData")
    var availabilityCSData: AvailabilityCSData? = null

    @SerializedName("showAvailabilityFB")
    var showAvailabilityFB: Boolean = false
}
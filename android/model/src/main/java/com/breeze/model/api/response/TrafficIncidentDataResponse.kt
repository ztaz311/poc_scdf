package com.breeze.model.api.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class TrafficIncidentDataResponse(

    @field:SerializedName("Obstacle")
    val obstacle: List<ObstacleItem>? = null,

    @field:SerializedName("Road Work")
    val roadwork: List<RoadworkItem>? = null,

    @field:SerializedName("Accident")
    val accident: List<AccidentItem>? = null,

    @field:SerializedName("Heavy Traffic")
    val heavyTraffic: List<HeavyTrafficItem>? = null
) : Parcelable

@Parcelize
data class ObstacleItem(

    @field:SerializedName("Message")
    val message: String? = null,

    @field:SerializedName("Latitude")
    val latitude: Double? = null,

    @field:SerializedName("Longitude")
    val longitude: Double? = null
) : Parcelable

@Parcelize
data class HeavyTrafficItem(

    @field:SerializedName("Message")
    val message: String? = null,

    @field:SerializedName("Latitude")
    val latitude: Double? = null,

    @field:SerializedName("Longitude")
    val longitude: Double? = null
) : Parcelable

@Parcelize
data class AccidentItem(

    @field:SerializedName("Message")
    val message: String? = null,

    @field:SerializedName("Latitude")
    val latitude: Double? = null,

    @field:SerializedName("Longitude")
    val longitude: Double? = null
) : Parcelable

@Parcelize
data class RoadworkItem(

    @field:SerializedName("Message")
    val message: String? = null,

    @field:SerializedName("Latitude")
    val latitude: Double? = null,

    @field:SerializedName("Longitude")
    val longitude: Double? = null
) : Parcelable

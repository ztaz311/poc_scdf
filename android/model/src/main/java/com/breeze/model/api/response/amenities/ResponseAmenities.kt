package com.breeze.model.api.response.amenities

import com.breeze.model.annotation.AmenityTypeVal
import com.breeze.model.constants.AmenityType.CARPARK
import com.breeze.model.constants.Constants
import com.breeze.model.extensions.rn.getBooleanOrNull
import com.breeze.model.extensions.rn.getDoubleOrNull
import com.breeze.model.extensions.rn.getIntOrNull
import com.breeze.model.extensions.rn.getStringOrNull
import com.facebook.react.bridge.ReadableMap
import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.util.concurrent.TimeUnit

data class ResponseAmenities(
    @SerializedName("amenities") var amenities: List<Amenities> = arrayListOf()
)

data class Amenities(
    @SerializedName("type")
    @AmenityTypeVal
    var type: String? = null,

    @SerializedName("icon_url")
    var iconUrl: String? = null,

    @SerializedName("data")
    var data: List<BaseAmenity> = arrayListOf(),
)

data class BaseAmenity(
    @SerializedName("id")
    var id: String? = null,

    @SerializedName("name")
    var name: String? = null,

    @SerializedName("parkingSystem")
    var parkingSystem: String? = null,

    @SerializedName("provider")
    var provider: String? = null,

    @SerializedName("stationName")
    var stationName: String? = null,

    @SerializedName("address")
    var address: String? = null,

    @SerializedName("plugTypes")
    var plugTypes: List<PlugTypes>? = null,

    @SerializedName("capacity")
    var capacity: Int? = null,

    @SerializedName("category")
    var category: String? = null,

    @SerializedName("lat")
    var lat: Double? = null,

    @SerializedName("long")
    var long: Double? = null,

    @SerializedName("source")
    var source: String? = null,

    @SerializedName("distance")
    var distance: Int? = null,

    @SerializedName("availablelot")
    var availablelot: Int? = null,

    @SerializedName("currentHrRate")
    var currentHrRate: HourRate? = null,

    @SerializedName("nextHrRate")
    var nextHrRate: HourRate? = null,

    @SerializedName("hasRatesInfo")
    var hasRatesInfo: Boolean? = null,

    @SerializedName("isCheapest")
    var isCheapest: Boolean? = null,

    @SerializedName("destinationCarPark")
    var destinationCarPark: Boolean? = null,

    @SerializedName("parkingType")
    var parkingType: String? = null,

    @SerializedName("startDate")
    var startDate: Long? = null,

    @SerializedName("endDate")
    var endDate: Long? = null,

    @SerializedName("availablePercentImageBand")
    var availablePercentImageBand: String? = null,

    @SerializedName("availablePercent")
    var availablePercent: Double = 0.0,

    @SerializedName("hasVouchers")
    var hasVouchers: Boolean = false,

    @SerializedName("voucherAmount")
    var voucherAmount: Float = 0.0F,

    @SerializedName("imageType")
    var imageType: String? = null,

    @SerializedName("isBookmarked")
    var isBookmarked: Boolean = false,

    @SerializedName("bookmarkId")
    var bookmarkId: Int? = null,

    @SerializedName("additionalInfo")
    var additionalInfo: AdditionalInfo? = null,

    /**
     * for carpark availability
     */
    @SerializedName("showAvailabilityFB")
    var showAvailabilityFB: Boolean = false,

    @SerializedName("hasAvailabilityCS")
    var hasAvailabilityCS: Boolean = false,

    @SerializedName("availabilityCSData")
    var availabilityCSData: AvailabilityCSData? = null,

    @SerializedName("typeAmenniti")
    @AmenityTypeVal
    var amenityType: String = "",

    @SerializedName("iconUrl")
    var iconUrl: String? = null,
) {

    fun isSeasonParking(): Boolean {
        if (amenityType == CARPARK) {
            if (parkingType.equals(Constants.PARKING_TYPE_SEASON_PARKING)) {
                return true
            } else if (parkingType.equals(Constants.PARKING_TYPE_CUSTOMER_PARKING)) {
                return availablelot == 0
            }
        }
        return false
    }

    override fun equals(other: Any?): Boolean {
        if (other == null) {
            return false
        }
        if (other !is BaseAmenity) {
            return false
        }
        if (other.id == this.id) {
            return true
        }
        return super.equals(other)
    }

    companion object {
        fun fromRNData(data: ReadableMap) = BaseAmenity(
            id = data.getString("id"),
            name = data.getString("name"),
            parkingSystem = data.getString("parkingSystem"),
            provider = data.getString("provider"),
            stationName = data.getString("stationName"),
            address = data.getString("address"),
//                plugTypes =  data.getList("plugTypes"),
            capacity = data.getIntOrNull("capacity"),
            category = data.getString("category"),
            lat = data.getDoubleOrNull("lat"),
            long = data.getDoubleOrNull("long"),
            source = data.getString("source"),
            distance = data.getIntOrNull("distance"),
            availablelot = data.getIntOrNull("availablelot"),
            currentHrRate = data.getMap("currentHrRate")?.let { HourRate.fromRN(it) },
            nextHrRate = data.getMap("nextHrRate")?.let { HourRate.fromRN(it) },
            hasRatesInfo = data.getBooleanOrNull("hasRatesInfo"),
            isCheapest = data.getBooleanOrNull("isCheapest"),
            destinationCarPark = data.getBooleanOrNull("destinationCarPark"),
            parkingType = data.getString("parkingType"),
//                startDate =  data.getLong("startDate"),
//                endDate =  data.getLong("endDate"),
            availablePercentImageBand = data.getString("availablePercentImageBand"),
            availablePercent = data.getDoubleOrNull("availablePercent") ?: 0.0,
            hasVouchers = data.getBooleanOrNull("hasVouchers") ?: false,
//                voucherAmount =  data.getFloat("voucherAmount"),
            imageType = data.getString("imageType"),
            isBookmarked = data.getBooleanOrNull("isBookmarked") ?: false,
            bookmarkId = data.getIntOrNull("bookmarkId"),
//                additionalInfo =  data.getAdditionalInfo("additionalInfo"),
            showAvailabilityFB = data.getBooleanOrNull("showAvailabilityFB") ?: false,
            hasAvailabilityCS = data.getBooleanOrNull("hasAvailabilityCS") ?: false,
//                availabilityCSData =  data.getAvailabilityCSData("availabilityCSData"),
            amenityType = data.getString("typeAmenniti") ?: "",
            iconUrl = data.getString("iconUrl"),
        )
    }

}

data class PlugTypes(
    @SerializedName("name") var name: String? = null,
)

data class HourRate(
    @SerializedName("start_time") var start_time: String? = null,
    @SerializedName("end_time") var end_time: String? = null,
    @SerializedName("oneHrRate") var oneHrRate: Double? = null
) {
    companion object {
        fun fromRN(data: ReadableMap) = HourRate(
            start_time = data.getString("start_time"),
            end_time = data.getString("end_time"),
            oneHrRate = data.getDoubleOrNull("oneHrRate"),
        )
    }
}


data class AdditionalInfo(
    @SerializedName("text") var text: String? = null,
    @SerializedName("style") var style: AdditionalInfoStyle? = null

)

data class AdditionalInfoStyle(
    @SerializedName("light") var light: AdditionalInfoStyleParams? = null,
    @SerializedName("dark") var dark: AdditionalInfoStyleParams? = null

)

data class AdditionalInfoStyleParams(
    @SerializedName("color") var color: String? = null

)

data class AvailabilityCSData(
    @SerializedName("updateTS") var updateTS: Long? = null,
    @SerializedName("title") var title: String? = null,
    @SerializedName("desc") var desc: String? = null,
    @SerializedName("themeColor") var themeColor: String? = null,
    @SerializedName("primaryDesc") var primaryDesc: String? = null,
    @SerializedName("secondaryDesc") var secondaryDesc: String? = null,
) : Serializable {

    fun generateDisplayedLastUpdate(): String {
        val now = System.currentTimeMillis()
        val diffInMillis = now - (updateTS ?: 0)
        val minutes = TimeUnit.MILLISECONDS.toMinutes(diffInMillis)
        val hours = TimeUnit.MILLISECONDS.toHours(diffInMillis)
        val days = TimeUnit.MILLISECONDS.toDays(diffInMillis)

        return when {
            days > 0 -> "$days day${if (days > 1) "s" else ""}"
            hours > 0 -> "$hours hour${if (hours > 1) "s" else ""}"
            else -> "$minutes minute${if (minutes > 1) "s" else ""}"
        } + " ago"
    }

    companion object {
        fun fromRNHashMapData(data: HashMap<String, Any>): AvailabilityCSData = AvailabilityCSData(
            updateTS = data["updateTS"]?.toString()?.toDoubleOrNull()?.toLong(),
            title = data["title"]?.toString(),
            desc = data["desc"]?.toString(),
            themeColor = data["themeColor"]?.toString(),
            primaryDesc = data["primaryDesc"]?.toString(),
            secondaryDesc = data["secondaryDesc"]?.toString(),
        )

        fun fromRNData(data: ReadableMap): AvailabilityCSData = AvailabilityCSData(
            updateTS = data.getDoubleOrNull("updateTS")?.toLong(),
            title = data.getStringOrNull("title"),
            desc = data.getStringOrNull("desc"),
            themeColor = data.getStringOrNull("themeColor"),
            primaryDesc = data.getStringOrNull("primaryDesc"),
            secondaryDesc = data.getStringOrNull("secondaryDesc"),
        )
    }
}


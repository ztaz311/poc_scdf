package com.breeze.model.api.response.guest

import com.google.gson.annotations.SerializedName

data class GuestGenResponse(
    @SerializedName("success") var success: Boolean = false,
    @SerializedName("id") var id: String? = null,
)

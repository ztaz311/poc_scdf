package com.breeze.model

data class DestinationSearch(
    var isBookmarked: Boolean? = false,
    var bookmarkId: String? = null,
    var address1: String? = null,
    var address2: String? = null,
    var latitude: String? = null,
    var longitude: String? = null,
    val fullAddress: String,
    val placeId: String? = null,
    var routablePoint: RoutablePoint? = null
) {
    fun parseRoutablePointFromLocationSearchData(searchLocationMapData: HashMap<String, Any>) {
        val selectedRoutblePointIndex =
            searchLocationMapData["selectedPinIndex"]?.toString()?.toIntOrNull() ?: -1
        (searchLocationMapData["routablePoints"] as? Array<*>)
            ?.takeIf { it.isNotEmpty() && selectedRoutblePointIndex >= 0 && it.size > selectedRoutblePointIndex }
            ?.let { mapData ->
                (mapData[selectedRoutblePointIndex] as? HashMap<*, *>)?.let { routablePointData ->
                    this.routablePoint = RoutablePoint(
                        address1 = routablePointData["address1"]?.toString(),
                        address2 = routablePointData["address2"]?.toString(),
                        lat = routablePointData["lat"]?.toString()?.toDoubleOrNull() ?: 0.0,
                        long = routablePointData["long"]?.toString()?.toDoubleOrNull() ?: 0.0,
                        type = routablePointData["type"]?.toString() ?: ""
                    )
                }
            }
    }

    companion object {
        fun fromRNHashMapData(hashMap: HashMap<String, Any>) = DestinationSearch(
            isBookmarked = hashMap["isBookmarked"] == true,
            bookmarkId = hashMap["bookmarkId"]?.toString() ?: "",
            latitude = hashMap["lat"]?.toString(),
            longitude = hashMap["long"]?.toString(),
            address1 = hashMap["address1"]?.toString() ?: "",
            address2 = hashMap["address2"]?.toString() ?: "",
            fullAddress = hashMap["fullAddress"]?.toString() ?: "",
            placeId = hashMap["placeId"]?.toString(),
        ).apply {
            parseRoutablePointFromLocationSearchData(hashMap)
        }
    }
}

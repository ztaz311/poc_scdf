package com.breeze.model.api.response.tbr


import com.google.gson.annotations.SerializedName

data class ProfileAmenities(

    @SerializedName("default_amenities") var defaultAmenities: ArrayList<DefaultAmenities> = arrayListOf(),
    @SerializedName("amenities") var amenities: ArrayList<String> = arrayListOf(),
    @SerializedName("season_parking") var SeasonParking: ArrayList<locationZone> = arrayListOf(),
    @SerializedName("landing_amenities_dark_unselected_url") var landingAmenitiesDarkUnselectedUrl: String? = null,
    @SerializedName("zones") var zones: ArrayList<Zones> = arrayListOf(),
    @SerializedName("landing_amenities_dark_selected_url") var landingAmenitiesDarkSelectedUrl: String? = null,
    @SerializedName("landing_amenities_light_selected_url") var landingAmenitiesLightSelectedUrl: String? = null,
    @SerializedName("display_text") var displayText: String? = null,
    @SerializedName("landing_amenities_light_unselected_url") var landingAmenitiesLightUnselectedUrl: String? = null,
    @SerializedName("element_name") var elementName: String? = null,
    @SerializedName("element_id") var elementId: String? = null,
    @SerializedName("isActive") var isActive: Boolean? = false

)

data class DefaultAmenities(
    @SerializedName("ids") var ids: ArrayList<String> = arrayListOf(),
    @SerializedName("type") var type: String? = null
)

data class locationZone(
    @SerializedName("lat") var lat: Double? = null,
    @SerializedName("long") var long: Double? = null
)


data class Zones(
    @SerializedName("active_zoom_level") var activeZoomLevel: Int? = null,
    @SerializedName("deactive_zoom_level") var deactiveZoomLevel: Int? = null,
    @SerializedName("radius") var radius: Int? = null,
    @SerializedName("zone_id") var zoneId: String? = null,
    @SerializedName("center_point_lat") var centerPointLat: Double? = null,
    @SerializedName("center_point_long") var centerPointLong: Double? = null,
)
package com.breeze.model.obu

import androidx.core.os.bundleOf
import com.google.gson.annotations.SerializedName
import kotlin.math.max

data class OBUDevice(
    @SerializedName("obuName")
    val obuName: String,

    /**
     * User inputted vehicle name
     * can be empty or blank
     * */
    @SerializedName("carPlate")
    val vehicleNumber: String,

    @SerializedName("userId")
    val userId: String? = null,
) {
    override fun equals(other: Any?): Boolean {
        (other as? OBUDevice)?.let {
            return it.obuName == this.obuName && it.userId == this.userId
        }
        return false
    }

    fun toBundle() = bundleOf(
        "obuName" to obuName,
        "vehicleNumber" to vehicleNumber
    )

    fun getDisplayedVehicleName(): String {
        if (vehicleNumber.isEmpty() || vehicleNumber.isBlank()) {
            val asterisks = "*".repeat(max(0, obuName.length - 4))
            val visibleNamePart = obuName.substring(obuName.length - 4, obuName.length)
            return "$asterisks$visibleNamePart"
        }
        return vehicleNumber
    }
}
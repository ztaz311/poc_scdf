package com.breeze.model.api.request

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ResetAmenitiesRequest(
    @field:SerializedName("notification_preference")
    val notificationPreference: ArrayList<String> = arrayListOf(),

    @field:SerializedName("amenities_preference")
    var amenities_preference: ArrayList<String> = arrayListOf(),
) : Parcelable

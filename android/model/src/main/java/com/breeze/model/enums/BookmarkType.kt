package com.breeze.model.enums

enum class BookmarkType {
    CUSTOM,
    HOME,
    WORK,
    NORMAL;
}
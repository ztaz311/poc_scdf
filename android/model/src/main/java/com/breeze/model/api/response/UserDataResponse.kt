package com.breeze.model.api.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class UserDataResponse(

    @field:SerializedName("tncAcceptStatus")
    val tncAcceptStatus: Boolean? = null,

    @field:SerializedName("carplateNumber")
    val carplateNumber: String? = null,

    @field:SerializedName("iuNumber")
    val iuNumber: String? = null,

    @field:SerializedName("showNudge")
    val showNudge: Boolean = false,

    @field:SerializedName("createdTimeStamp")
    val createdTimeStamp: Long = 0,

    @field:SerializedName("isLogEnabled")
    var isLogEnabled: Boolean = false
) : Parcelable

package com.breeze.model.api.request

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class TripUpdateRequest(

    @field:SerializedName("erp")
    val erp: ArrayList<ErpItem> = arrayListOf(),

    @field:SerializedName("parkingExpense")
    var parkingExpense: Double? = null,

    @field:SerializedName("totalExpense")
    var totalExpense: Double? = null
) : Parcelable

@Parcelize
data class ErpItem(

    @field:SerializedName("actualAmount")
    var actualAmount: Double? = null,

    @field:SerializedName("erpId")
    var erpId: Long? = null,

    @field:SerializedName("tripErpId")
    var tripErpId: Long? = null
) : Parcelable


@Parcelize
data class TripFileRequest(

    @field:SerializedName("tripId")
    var tripId: Long? = null,

    @field:SerializedName("fileType")
    var fileType: String? = null,

    @field:SerializedName("attachmentFile")
    var attachmentFile: String? = null
) : Parcelable

@Parcelize
data class TripIdRequest(
    @field:SerializedName("tripId")
    var tripId: Long? = null
) : Parcelable
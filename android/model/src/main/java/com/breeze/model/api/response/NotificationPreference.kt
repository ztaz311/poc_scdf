package com.breeze.model.api.response

import com.google.gson.annotations.SerializedName

data class NotificationPreference(

    @SerializedName("element_id") var elementId: String? = null,
    @SerializedName("display_text") var displayText: String? = null,
    @SerializedName("element_name") var elementName: String? = null,
    @SerializedName("button_inactive_color") var buttonInactiveColor: String? = null,
    @SerializedName("tutorial_image_cordinate_y") var tutorialImageCordinateY: String? = null,
    @SerializedName("button_active_color") var buttonActiveColor: String? = null,
    @SerializedName("is_updatable") var isUpdatable: Boolean? = null,
    @SerializedName("button_inactive_image_url") var buttonInactiveImageUrl: String? = null,
    @SerializedName("button_type") var buttonType: String? = null,
    @SerializedName("font_type") var fontType: String? = null,
    @SerializedName("button_active_image_url") var buttonActiveImageUrl: String? = null,
    @SerializedName("map_icon_url") var mapIconUrl: String? = null,
    @SerializedName("tutorial_image_cordinate_x") var tutorialImageCordinateX: String? = null,
    @SerializedName("sub_items") var subItems: List<SubItems>? = null,
    @SerializedName("button_text_color") var buttonTextColor: String? = null,
    @SerializedName("tutorial_image_url") var tutorialImageUrl: String? = null,
    @SerializedName("order") var order: Int? = null,
    @SerializedName("is_selected") var isSelected: Boolean? = null

)
package com.breeze.model.api.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class RetrieveFileResponse(
    @field:SerializedName("file")
    val `file`: String? = null,
    @field:SerializedName("tripId")
    val tripId: String? = null
) : Parcelable

data class DeleteFileResponse(
    val `file`: String,
    val tripId: String
)

data class UploadFileResponse(
    val `file`: String,
    val tripId: String
)
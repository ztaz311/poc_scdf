package com.breeze.model.api.response


import com.google.gson.annotations.SerializedName

data class ETAResponse(
    @SerializedName("tripEtaUUID")
    val tripEtaUUID: String = "",
    @SerializedName("message")
    val message: String = ""
)



package com.breeze.model.api.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SearchQueryResponse(

    @field:SerializedName("totalRecords")
    val totalrecords: String? = null,

    @field:SerializedName("addresses")
    val searchItems: List<SearchItemQuery>? = null,

    @field:SerializedName("source")
    val source: String? = null,

    @field:SerializedName("token")
    val token: String? = null,

    ) : Parcelable

@Parcelize
data class SearchItemQuery(

    @field:SerializedName("distance")
    val distance: String? = null,

    @field:SerializedName("address2")
    val address2: String? = null,

    @field:SerializedName("address1")
    val address1: String? = null,

    @field:SerializedName("lat")
    var lat: String? = null,

    @field:SerializedName("long")
    var jsonMemberLong: String? = null,

    @field:SerializedName("alias")
    val alias: String? = null,

    @field:SerializedName("placeId")
    val placeId: String? = null
) : Parcelable


package com.breeze.model

enum class DestinationAddressTypes(val type: String) {
    EASY_BREEZIES("B"),
    SEARCH_HISTORY("H"),
    ADDRESS_SEARCH("S"),
    CALENDAR("C"),
    RECOMMENDED_ADDRESS("R");

    companion object {
        fun getDestinationAddressType(requiredType: String): DestinationAddressTypes? {
            for (type in DestinationAddressTypes.values()) {
                if (type.type == requiredType) {
                    return type
                }
            }
            return null
        }
    }
}
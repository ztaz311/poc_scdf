package com.breeze.model.api.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ExploreMaintenanceResponse(

    @field:SerializedName("content_id")
    var contentId: Int? = null,
    @field:SerializedName("name")
    var name: String? = null,
    @field:SerializedName("is_new_layer")
    var is_new_layer: Boolean = false

) : Parcelable

@Parcelize
data class ExploreMapContentResponse(
    @field:SerializedName("content_details")
    var item: List<ExploreMaintenanceResponse>? = null
) : Parcelable
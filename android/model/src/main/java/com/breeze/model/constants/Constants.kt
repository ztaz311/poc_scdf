package com.breeze.model.constants

import com.mapbox.api.directions.v5.DirectionsCriteria
import com.mapbox.geojson.Point

object Constants {

    const val REQUEST_NEARBY_AMENITY_RESULT_COUNT = 5

    //for navigation
    const val TRAFFIC_LAYER_ID = "traffic"

    const val MAX_WAY_POINT_LIMIT = 3

    // For location

    const val DEFAULT_LOCATION_PROVIDER = ""

    const val RESET_AMENITY_SELECTION_ON_LAUNCH = true

    // common
    const val SCHOOL_ZONE_RO = "SCHOOL_ZONE"
    const val SILVER_ZONE_RO = "SILVER_ZONE"
    const val SPEED_CAMERA_RO = "SPEED_CAMERA"
    const val SEASON_PARKING_RO = "SEASON_PARKING_RO"

    const val TRAFFIC_MARKER = "traffic"

    const val POI_MARKER = "POI_MARKER"

    const val MAINTAINENCE_MODE_ENABLED = "MAINTAINENCE_MODE_ENABLED"
    const val MAINTAINENCE_MODE_MESSAGE = "MAINTAINENCE_MODE_MESSAGE"

    const val MAPBOX_DISTANCE_FORMATTER_LOCALE = "en_SG"
    const val MAPBOX_ROUTE_OPTION_LOCALE = "en_SG"


    const val CARPARK_FEATURE_DESTINATION_CARPARK_LAYER =
        "CARPARK_FEATURE_DESTINATION_CARPARK_LAYER"
    const val CARPARK_FEATURE_GREY_CARPARK_LAYER = "CARPARK_FEATURE_GREY_CARPARK_LAYER"
    const val CARPARK_FEATURE_NEW_CARPARK_LAYER = "CARPARK_FEATURE_NEW_CARPARK_LAYER"
    const val CARPARK_PRICE_LAYER = "CARPARK_PRICE_LAYER"
    const val CARPARK_INDEX_LAYER = "CARPARK_INDEX_LAYER"
    const val CARPARK_PRICE_GREEN_LAYER = "CARPARK_PRICE_GREEN_LAYER"

    const val CARPARK_FEATURE_DESTINATION_CARPARK_SOURCE =
        "CARPARK_FEATURE_DESTINATION_CARPARK_SOURCE"
    const val CARPARK_FEATURE_GREY_CARPARK_SOURCE = "CARPARK_FEATURE_GREY_CARPARK_SOURCE"
    const val CARPARK_FEATURE_NEW_CARPARK_SOURCE = "CARPARK_FEATURE_NEW_CARPARK_SOURCE"
    const val CARPARK_PRICE_SOURCE = "CARPARK_PRICE_SOURCE"
    const val CARPARK_INDEX_SOURCE = "CARPARK_INDEX_SOURCE"
    const val CARPARK_PRICE_GREEN_SOURCE = "CARPARK_PRICE_GREEN_SOURCE"

    const val CARPARK_FEATURE_NEW_CARPARK_IMAGE = "CARPARK_FEATURE_NEW_CARPARK_IMAGE"

    const val CARPARK_SEASON_BAR_IMAGE = "CARPARK_SEASON_BAR_IMAGE"
    const val CARPARK_PRICE_IMAGE = "CARPARK_PRICE_IMAGE"
    const val CARPARK_INDEX_IMAGE = "CARPARK_INDEX_IMAGE"
    const val CARPARK_PRICE_GREEN_IMAGE = "CARPARK_PRICE_GREEN_IMAGE"


    const val CARPARK_FEATURE_LAYER = "CARPARK_FEATURE_LAYER"


    const val UNSAVED_DESTINATION_ICON = "UNSAVED_DESTINATION_ICON"
    const val FAVOURITES_DESTINATION_ICON = "FAVOURITES_DESTINATION_ICON"
    const val DESTINATION_SOURCE = "DESTINATION_SOURCE"
    const val DESTINATION_LAYER = "DESTINATION_LAYER"
    const val DESTINATION_IMAGE = "DESTINATION_IMAGE"

    const val WALKING_DESTINATION_SOURCE = "WALKING_DESTINATION_SOURCE"
    const val WALKING_DESTINATION_LAYER = "WALKING_DESTINATION_LAYER"
    const val WALKING_DESTINATION_ICON_ID = "WALKING_DESTINATION_ICON_ID"

    const val ILLEGAL_PARKING_CAMERA = "illegalparkingcamera"
    const val SCHOOL_ZONE = "schoolzone"
    const val SPEED_CAMERA = "speedcamera"

    const val COUNTRY_LABEL = "country-label"

    const val SCHOOL_ICON = "SCHOOL_ICON"

    const val SILVER_ICON = "SILVER_ICON"
    const val NO_RATES_ERP_ICON = "NO_RATES_ERP_ICON"
    const val ERP_ICON = "ERP_ICON"
    const val ERP_GEOJSONSOURCE = "ERP_GEOJSONSOURCE"
    const val DROP_PIN = "DROP_PIN"

    //const val NO_RATES_ERP_GEOJSONSOURCE = "NO_RATES_ERP_GEOJSONSOURCE"
    const val ERP_LAYER = "ERP_LAYER"
    //const val NO_RATES_ERP_LAYER = "NO_RATES_ERP_LAYER"


    const val SILVER_ZONE_SOURCE = "SILVER_ZONE_SOURCE"
    const val SILVER_ZONE_LAYER = "SILVER_ZONE_LAYER"
    const val SCHOOL_ZONE_SOURCE = "SCHOOL_ZONE_SOURCE"
    const val SCHOOL_ZONE_LAYER = "SCHOOL_ZONE_LAYER"

    const val VALIDATE_NAME_BREEZE =
        "Breeze Breezer Breeze Support Breeze official or Official Breeze Breeze NCS or NCS Breeze"


    const val SOURCE = "_INCIDENT_SOURCE"
    const val LAYER = "_INCIDENT_LAYER"
    const val IMAGE = "_INCIDENT_IMAGE"


    /*const val INCIDENT_HEAVYTRAFFIC_SOURCE = "INCIDENT_HEAVYTRAFFIC_SOURCE"
    const val INCIDENT_HEAVYTRAFFIC_LAYER = "INCIDENT_HEAVYTRAFFIC_LAYER"
    const val INCIDENT_HEAVYTRAFFIC_IMG = "INCIDENT_HEAVYTRAFFIC_IMG"*/
    const val INCIDENT_ROADBLOCK_SOURCE = "INCIDENT_ROADBLOCK_SOURCE"
    const val INCIDENT_ROADBLOCK_LAYER = "INCIDENT_ROADBLOCK_LAYER"
    const val INCIDENT_ROADBLOCK_IMG = "INCIDENT_ROADBLOCK_IMG"
    const val INCIDENT_MISCELLANEOUS_SOURCE = "INCIDENT_MISCELLANEOUS_SOURCE"
    const val INCIDENT_MISCELLANEOUS_LAYER = "INCIDENT_MISCELLANEOUS_LAYER"
    const val INCIDENT_MISCELLANEOUS_IMG = "INCIDENT_MISCELLANEOUS_IMG"
    const val INCIDENT_UNATTENDED_VEHICLE_SOURCE = "INCIDENT_UNATTENDED_VEHICLE_SOURCE"
    const val INCIDENT_UNATTENDED_VEHICLE_LAYER = "INCIDENT_UNATTENDED_VEHICLE_LAYER"
    const val INCIDENT_UNATTENDED_VEHICLE_IMG = "INCIDENT_UNATTENDED_VEHICLE_IMG"

    const val VOICE_INSTRUCTION_CACHE = "voice-instruction-cache"
    const val PRIMARY_ROUTE_BUNDLE_KEY = "myPrimaryRouteBundleKey"

    const val PERMISSION_FROM_SIGNIN = "PERMISSION_FROM_SIGNIN"
    const val PERMISSION_FROM_LIFECYCLE_HANDLER = "PERMISSION_FROM_LIFECYCLE_HANDLER"

    const val TERMS_OF_SERVICE_URL = "https://dev.breeze.com.sg/app/terms-of-use.html"
    const val PRIVACY_POLICY_URL = "https://dev.breeze.com.sg/app/privacy.html"
    const val CLOUD_STORAGE_FOLDER = "ANDROID"
    const val FIRESTORE_DB_COLLECTION = "ANDROID"

    const val DEVICE_PLATFORM = "Android"

    const val FEEDBACK_DESCRIPTION_LENGTH = 160

    const val MAX_USERNAME_LENGTH = 14
    const val RECENT_SEARCH_LIMIT = 8

    const val No_Internet_Connection = "No Internet Connection"
    const val ERP_REFRESH_DELAY = 180000L

    const val NAVIGATION_BUNDLE = "NAVIGATION_BUNDLE"
    const val NAVIGATION_ETA_DATA = "NAVIGATION_ETA_DATA"

    //const val NAVIGATION_BUNDLE_ROUTE = "NAVIGATION_BUNDLE_ROUTE"
    const val NAVIGATION_BUNDLE_ROUTE_RESPONSE = "NAVIGATION_BUNDLE_ROUTE_RESPONSE"
    const val NAVIGATION_BUNDLE_ROUTE_OPTIONS = "NAVIGATION_BUNDLE_ROUTE_OPTIONS"
    const val NAVIGATION_BUNDLE_SELECTED_ROUTE_INDEX = "NAVIGATION_BUNDLE_SELECTED_ROUTE_INDEX"
    const val NAVIGATION_BUNDLE_DESTINATION_DETAILS = "NAVIGATION_BUNDLE_DESTINATION_DETAILS"
    const val NAVIGATION_BUNDLE_ORIGINAL_DESTINATION_DETAILS =
        "NAVIGATION_BUNDLE_ORIGINAL_DESTINATION_DETAILS"
    const val NAVIGATION_BUNDLE_ERPDATA = "NAVIGATION_BUNDLE_ERPDATA"
    const val NAVIGATION_BUNDLE_WALKING_ROUTE_DIRECTION =
        "NAVIGATION_BUNDLE_WALKING_ROUTE_DIRECTION"
    const val NAVIGATION_BUNDLE_WALKING_ROUTE_ROUTE_OPTIONS =
        "NAVIGATION_BUNDLE_WALKING_ROUTE_ROUTE_OPTIONS"
    const val NAVIGATION_BUNDLE_SKIP_DRIVING = "NAVIGATION_BUNDLE_SKIP_DRIVING"
    const val NAVIGATION_BUNDLE_SELECTED_CARPARK_ICON_RES =
        "NAVIGATION_BUNDLE_SELECTED_CARPARK_ICON_RES"

    const val FRAGMENT_TO_LOAD = "FRAGMENT_TO_LOAD"
    const val NAVIGATION_ACTIVITY_RESULT_CODE = 10012

    const val NAVIGATION_END = "NAVIGATION_ENDED"
    const val NAVIGATION_END_REFRESH_LANDING_CAR_PARKS = "NAVIGATION_END_REFRESH_LANDING_CAR_PARKS"
    const val NAVIGATION_END_REFRESH_MAP = "NAVIGATION_END_REFRESH_MAP"
    const val SAVE_EASY_BREEZY = "SAVE_EASY_BREEZY"

    const val ROUTE_PLANNING_TRIP_DETAILS = "ROUTE_PLANNING_TRIP_DETAILS"
    const val ROUTE_PLANNING_MODE = "ROUTE_PLANNING_MODE"
    const val DESTINATION_ADDRESS_DETAILS = "DESTINATION_ADDRESS_DETAILS"
    const val SOURCE_ADDRESS_DETAILS = "SOURCE_ADDRESS_DETAILS"
    const val SELECTED_ADDRESS_DETAILS = "SELECTED_ADDRESS_DETAILS"
    const val SELECTED_ERP_ROUTE_DETAILS = "SELECTED_ERP_ROUTE_DETAILS"
    const val SELECTED_ERP_ROUTE_INDEX = "SELECTED_ERP_ROUTE_INDEX"
    const val SELECTED_ERP_ROUTE_TS = "SELECTED_ERP_ROUTE_TS"
    const val ERP_ROUTE_DETAILS_LIST = "ERP_ROUTE_DETAILS_LIST"
    const val LIST_CRITERIAS = "LIST_CRITERIAS"
    const val KEY_IS_TAMPINES = "KEY_IS_TAMPINES"
    const val IS_TIME_CHANGED = "IS_TIME_CHANGED"

    const val VEHICLE_NUMBERS = "VEHICLE_NUMBERS"
    const val VEHICLE_NUMBER = "VEHICLE_NUMBER"

    const val OBU_SELECTION_FRAGMENT_NAME = "obu_vehicle_selection_fragment"

    const val FAVOURITE_EDIT = "FAVOURITE_EDIT"
    const val TRIP_SUMMARY_DETAILS = "TRIP_SUMMARY_DETAILS"
    const val NEARBY_CARPARK_ROUTE = "NEARBY_CARPARK_ROUTE"
    const val NEARBY_CARPARK_TRAVEL_DURATION = "NEARBY_CARPARK_TRAVEL_DURATION"
    const val CARPARK_RADIUS: Double = 0.5
    const val AMENITIES_RADIUS: Double = 2.0
    const val ERP_RADIUS: Double = 3.0

    const val ETA_TOAST_DURATION = 10000
    const val OTP_RESEND_DURATION = 60000 * 3 //3 minutes
    const val CRUISE_MODE_TOAST_DURATION = 5000
    const val CRUISE_MODE_TRIGGER_SPEED = 20   //Speed in kmph
    const val CRUISE_MODE_STOP_SPEED = 1
    const val CRUISE_WAIT_TIME = 300000L
    const val OTHER_MODE_TOAST_DURATION = 10000

    const val POLYLINE_PRECISION = 6
    const val POLYLINE_GEOMETRY = DirectionsCriteria.GEOMETRY_POLYLINE6
    //const val POLYLINE_PRECISION = 5
    //const val POLYLINE_PRECISION_PARAM = DirectionsCriteria.GEOMETRY_POLYLINE

    const val THIRTY_MINUTES: Long = 30 * 60 * 1000


    const val DEFAULT_RECOMMENDATION_COUNT = 2

    const val DEVICE_OS = "ANDROID"
    const val NOTIFICATION_TRIP_EMAIL = "TRIP_EMAIL"
    const val NOTIFICATION_APP_UPDATE = "APP_UPDATE"
    const val NOTIFICATION_UPCOMING_TRIP = "UPCOMING_TRIP"
    const val NOTIFICATION_CARPARK_VOUCHER = "VOUCHER"
    const val NOTIFICATION_EXPLORE_MAP = "EXPLORE_MAP"
    const val NOTIFICATION_INBOX = "INBOX"

    const val ERP_MAX_THRESHOLD = 6.60
    const val TRIP_NOTIFICATION_SHOW_DURATION: Long = 5000 //5 Seconds
    const val AUTO_RECENTER_MAP_DURATION: Long = 10000 //10 Seconds
    const val SEARCH_SOURCE_GOOGLE = "GOOGLE"

    const val RN_TO_SCREEN = "toScreen"
    const val RN_FROM_SCREEN = "fromScreen"
    const val ERP_ZONE_NAME_PREFIX = "ERP_ZONE_"
    const val RN_DARK_MODE = "isDarkMode"

    const val ERP_ID = "erpId"
    const val SINGAPORE_COUNTRY_CODE = "+65"

    const val TRAVEL_LOG_FRAGMENT_REQUEST_KEY = "TRAVEL_LOG_FRAGMENT_REQUEST"
    const val ROUTE_PLANNING_FRAGMENT_REQUEST_KEY = "ROUTE_PLANNING_REQUEST"
    const val ROUTE_PLANNING_CARPARK_REQUEST_KEY = "ROUTE_PLANNING_CARPARK_REQUEST"

    const val VEHICLE_SELECTION_FRAGMENT_REQUEST_KEY = "VEHICLE_SELECTION_FRAGMENT_REQUEST_KEY"


    const val CARPARK_ZOOM_RADIUS: Double = 500.0
    const val AMENITIES_ZOOM_RADIUS: Double = 3000.0
    const val DEFAULT_TOOLTIP_ZOOM_RADIUS = 500.0
    const val PARKING_CHECK_RADIUS_ZOOM_MAX = 3000.0
    const val POI_TOOLTIP_ZOOM_RADIUS = 91.0 // ~300 feet

    const val PROFILE_CHECK_RADIUS = 1.0
    const val PARKING_CHECK_RADIUS = 0.5
    const val PARKING_CHECK_RADIUS_MAX = 3.0
    const val PARKING_DEFAULT_RESULT_COUNT = 5

    /**
     * radius to load ev/petrol in kilometer
     */
    const val DEFAULT_PETROL_EV_RADIUS = 5.0

    /**
     * max radius to load ev/petrol in kilometer
     * */
    const val DEFAULT_PETROL_EV_MAX_RADIUS = 5.0

    const val DEFAULT_PETROL_EV_RESULT_COUNT = 3

    const val PARKING_TYPE_SHORT_TERM_PARKING = "SHORT_TERM_PARKING"
    const val PARKING_TYPE_SEASON_PARKING = "SEASON_PARKING"
    const val PARKING_TYPE_PARTIAL_SEASON_PARKING = "PARTIAL_SEASON_PARKING"
    const val PARKING_TYPE_CUSTOMER_PARKING = "CUSTOMER_PARKING"


    const val MAX_ZOOM_LEVEL = 16.5

    //cluster properties

    const val CLUSTER_MAX_ZOOM = 14L // this value is actual zoom level minus 1

    const val CONGESTION_ALERT_EXTRA_DATA = "CONGESTION_ALERT_EXTRA_DATA"
    const val CONGESTION_ALERT_LIST = "CONGESTION_ALERT_LIST"
    const val CONGESTION_ALERT_TITLE = "CONGESTION_ALERT_TITLE"
    const val CONGESTION_ALERT_SCREEN = "CONGESTION_ALERT_SCREEN"
    const val PARKING_AVAILABILITY_EXTRA_DATA = "PARKING_AVAILABILITY_EXTRA_DATA"
    const val PARKING_AVAILABILITY_LIST = "PARKING_AVAILABILITY_LIST"
    const val PARKING_AVAILABILITY_TITLE = "PARKING_AVAILABILITY_TITLE"
    const val PARKING_AVAILABILITY_SCREEN = "PARKING_AVAILABILITY_SCREEN"

    const val DISTANCE_SHOW_CAR_PARK_LIST = 1000f
    const val DISTANCE_SHOW_CAR_PARK_AVAILABILITY = 2000f
    const val TIME_SHOW_CAR_PARK_AVAILABILITY = 8000L

    const val DISTANCE_GET_AVAILABILITY_ALERT_VOICE = "1000"

    //obu
    object OBU_ANALYTICS_MESSAGE {
        const val CHEAPEST = "cheapest"
        const val NEAREST = "nearest"
        const val NORMAL = "normal"
    }

    object REQUEST_CODES {
        const val REQUEST_CODE_REQUIRED_PERMISSION = 101
        const val REQUEST_PERMISSION_SIGN_IN_MAIN = 102
        const val CONTACTS_REQUEST_CODE_FROM_RN = 103
        const val CONTACTS_REQUEST_CODE = 129
        const val CONTACTS_REQUEST_CODE_ROUTE_PLANNING = 104
        const val CONTACTS_PERMISSION_REQUEST = 105
        const val PERMISSION_ACTIVITY_REQUEST = 106
        const val GPS_BOTTOM_SHEET_REQUEST = 107
        const val CALENDAR_PERMISSION_REQUEST = 108
        const val START_NAVIGATION_REQUEST = 109
        const val TRIP_INFO_SELECT_PHOTO = 110
        const val TRIP_INFO_TAKE_PHOTO = 111
        const val TRIP_INFO_PERMISSION_GALLERY = 112
        const val TRIP_INFO_PERMISSION_CAMERA = 113
    }

    object TYPE_END_NAVIGATION {
        const val NATURAL: String = "NATURAL"
        const val MANUAL: String = "MANUAL"
    }


    object TAGS {
        const val NEW_ADDRESS_FRAGMENT_TAG: String = "NewAddressFragment"
        const val LOGIN_TAG: String = "LOGIN_TAG"
        const val CREATE_ACCOUNT_TAG: String = "CREATE_ACCOUNT_TAG"
        const val FORGET_PASSWORD_TAG: String = "FORGET_PASSWORD_TAG"
        const val ADD_NEW_BREEZE: String = "ADD_NEW_BREEZE"
        const val SEARCH_TAG: String = "SEARCH_TAG"

        const val TESTING_TAG: String = "testing-abc"
        const val CURRENT_LOCATION_TAG: String = "Current Location" //Do not change the value
        const val QUERY_ERROR_TAG: String = "API_ERROR"

        const val MENU_TAG: String = "MENU_TAG"
        const val EASY_BREEZIES_TAG: String = "EASY_BREEZIES_TAG"
        const val ROUTE_PLANNING: String = "ROUTE_PLANNING"
        const val FEATURE_NOT_READY: String = "FEATURE_NOT_READY"
        const val ACCOUNT_TAG: String = "ACCOUNT_TAG"
        const val SETTINGS_TAG: String = "SETTINGS_TAG"
        const val TRAVEL_LOG_TAG: String = "TRAVEL_LOG_TAG"
        const val ONBOARDING_TAG: String = "ONBOARDING_TAG"
        const val NEARBY_CARPARKS_TAG: String = "NEARBY_CARPARKS_TAG"
        const val FAVOURITES_ETA_TAG: String = "FAVOURITES_ETA_TAG"
        const val FEEDBACK_TAG: String = "FEEDBACK_TAG"
        const val ERP_DETAIL_TAG: String = "ERP_DETAIL_TAG"
        const val TRAFFIC_DETAIL_TAG: String = "TRAFFIC_DETAIL_TAG"
        const val ROUTE_PREVIEW_ERP: String = "ROUTE_PREVIEW_ERP"
        const val INBOX_SCREEN: String = "INBOX_SCREEN"
        const val COLLECTION_DETAILS_TAG: String = "COLLECTION_DETAILS_TAG"
        const val EXPLORE_MAP: String = "EXPLORE_MAP"
    }

    object SETTINGS {
        const val ATTR_MAP_DISPLAY = "MapDisplay"
        const val VALUE_MAP_DISPLAY_AUTO = "Auto"
        const val VALUE_MAP_DISPLAY_DARK = "Dark"
        const val VALUE_MAP_DISPLAY_LIGHT = "Light"
        const val ATTR_ROUTE_PREFERENCE = "RoutePreference"
        const val VALUE_ROUTE_PREFERENCE_FASTEST = "Fastest"
        const val VALUE_ROUTE_PREFERENCE_CHEAPEST = "Cheapest"
        const val ATTR_SHOW_DEMO = "ShowDemo"
        const val CARPARK_AVAILABILITY = "CarparkAvailability"
        const val CARPARK_COUNT = "CarparkCount"
        const val CARPARK_DISTANCE = "CarparkDistance"
    }

    object AMPLIFY_CONSTANTS {
        const val CUSTOM_NAME = "custom:fullname"
        const val CUSTOM_FIRST_TIME_LOGIN = "custom:firstTimeLogin"
        const val CUSTOM_ONBOARDING_STATE = "custom:onboarding_state"
        const val CUSTOM_EMAIL = "custom:email"
        const val AMPLIFY_PASSWORD = "Breeze@45678"
        const val SIGN_IN_WITH_CUSTOM_CHALLENGE = "CONFIRM_SIGN_IN_WITH_CUSTOM_CHALLENGE"
        const val SIGN_IN_DONE = "DONE"
        const val FULL_NAME = "name"
        const val EMAIL = "email"
        const val EMAIL_VERIFIED = "email_verified"
        const val IDENTITIES = "identities"
        const val ERROR_CODE = "errorCode"
        const val ERROR_EXPIRY_DATE = "expiryDate"
        const val ERROR_REMAINING_ATTEMPTS = "remainingAttempts"
        const val ERROR_INVALID_OTP = "Invalid OTP"
        const val CUSTOM_USER_EMAIL = "custom:user_email"
        const val INSTABUG_USER_GROUP_ATTRIBUTE = "custom:user_group"
        const val INSTABUG_USER_GROUP_ATTRIBUTE_UI = "user_group"
        const val ONBOARDING_STATE_INIT = "ONBOARDING_INIT"
        const val SUB = "sub"
        const val DONE = "DONE"
    }

    object KEYS {
        const val KEY_NAME: String = "KEY_NAME"
        const val KEY_PHONENUMBER: String = "KEY_PHONENUMBER"
        const val KEY_PASSWORD: String = "KEY_PASSWORD"
        const val KEY_EMAIL: String = "KEY_EMAIL"
        const val KEY_USER_NOT_CONFIRMED: String = "KEY_USER_NOT_CONFIRMED"
    }

    object REGEX {
        const val NAME_REGEX = "^[a-zA-Z ]{1,14}$"
        const val VEHICLE_NAME_CHAR = "[a-zA-Z0-9 ]"
        const val VEHICLE_NAME = "^[a-zA-Z0-9 ]{1,8}\$"
        const val EMAIL_REGEX =
            "^[a-zA-Z0-9_+&*-]+(?:\\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$"

        //const val PASSWORD_REGEX = "^(?=.*[a-zA-Z])(?=.*\\d)(?=.*[#@$!%*?&^/+-_)(])[A-Za-z\\d#@$!%*?&^/+-_)(]{8,}$"
        const val PASSWORD_REGEX =
            "^(?=.*[A-Za-z])(?=.*\\d)(?=.*[@$!%*#?&_+-])[A-Za-z\\d@$!%*#?&_+-]{8,}$"
        const val PHONE_REGEX = "^(8|9)\\d{7}$"
    }

    object SHARED_PREF_KEY {
        val PUSH_REFRESH: String = "PUSH_REFRESH"

        const val SEARCHSTRING = "searchString"
        const val IS_ONBOARDING_SHOWN = "IS_ONBOARDING_SHOWN"
        const val PUSH_MESSAGING_TOKEN: String = "PUSH_MESSAGING_TOKEN"
        const val DEVICE_ID: String = "DEVICE_ID"

        const val USER_PREFERENCE = "USER_PREFERENCE"
        const val USER_PREFERENCE_V2 = "USER_PREFERENCE_V2"
        const val IS_DASHBOARD_TOOLTIP_SHOWN: String = "IS_DASHBOARD_TOOLTIP_SHOWN"

        const val MAP_EXPLORE_ENABLE = "mapExploreEnable"

        const val HANDLE_PUSH_NOTIFICATION_CHANNEL = "HANDLE_PUSH_NOTIFICATION_CHANNEL"
        const val HANDLE_PUSH_NOTIFICATION_ID = "HANDLE_PUSH_NOTIFICATION_ID"
        const val HANDLE_PUSH_NOTIFICATION_EXTRA = "HANDLE_PUSH_NOTIFICATION_EXTRA"
        const val HANDLE_PUSH_NOTIFICATION_ANALYTICS_STATISTIC =
            "HANDLE_PUSH_NOTIFICATION_ANALYTICS_STATISTIC"

        const val KEY_CONTENT_CATEGORY_ID = "KEY_CONTENT_CATEGORY_ID"

        const val TEMP_NOTIFICATION_FILTER = "TEMP_NOTIFICATION_FILTER"
        const val USER_NOTIFICATION_SEEN = "USER_NOTIFICATION_SEEN"
        const val IS_IN_APP_GUIDE_SHOWN = "IS_IN_APP_GUIDE_SHOWN"
        const val TNC_STATUS = "TNC_STATUS"

        const val KEY_STATE_CARPARK_OPTION = "KEY_STATE_CARPARK_OPTION"
        const val KEY_CAR_PARK_DISTANCE = "CarparkDistance"
        const val KEY_CAR_PARK_COUNT = "CarparkCount"
        const val KEY_SEEN_CONTENT_IDS = "SEEN_CONTENT_IDS"
        const val KEY_CAR_ANIMATION_SHOWN = "CAR_ANIMATION_SHOWN"
        const val KEY_VOICE_STATE = "KEY_VOICE_STATE"
        const val KEY_APP_LAUNCH_COUNT = "KEY_APP_LAUNCH_COUNT"
        const val KEY_LOCAL_USER_DATA = "KEY_LOCAL_USER_DATA"
        const val KEY_USER_ID = "KEY_USER_ID"
        const val KEY_SAVE_COLLECTION_TOKEN = "KEY_SAVE_COLLECTION_TOKEN"

        //        OBU
        const val KEY_SHOW_OBU_INTRO = "KEY_SHOW_OBU_INTRO"
        const val KEY_STORED_OBU_DEVICES = "STORED_OBU_DEVICES"
        const val KEY_PARKING_AVAILABILITY_AUDIO =
            "ParkingAvailabilityAudio"                       // I want to hear
        const val KEY_PARKING_AVAILABILITY_DISPLAY =
            "ParkingAvailabilityDisplay"                   // I want to see
        const val KEY_ESTIMATED_TRAVEL_TIME_AUDIO =
            "EstimatedTravelTimeAudio"                      // I want to hear
        const val KEY_ESTIMATED_TRAVEL_TIME_DISPLAY =
            "EstimatedTravelTimeDisplay"                  // I want to see

        const val KEY_SCHOOL_ZONE_AUDIO =
            "SchoolZoneVoiceAlert"                       // I want to hear
        const val KEY_SCHOOL_ZONE_DISPLAY =
            "SchoolZoneDisplayAlert"                   // I want to see
        const val KEY_SILVER_ZONE_AUDIO =
            "SilverZoneVoiceAlert"                      // I want to hear
        const val KEY_SILVER_ZONE_DISPLAY =
            "SilverZoneDisplayAlert"                  // I want to see
        const val KEY_BUS_LANE_AUDIO =
            "BusLaneVoiceAlert"                      // I want to hear
        const val KEY_BUS_LANE_DISPLAY =
            "BusLaneDisplayAlert"                  // I want to see
    }

    object PushNotification {
        const val KEY_DATE_PUSHED = "NotificationFCM.KEY_DATE_PUSHED"
        const val KEY_MESSAGE = "NotificationFCM.KEY_MESSAGE"
        const val KEY_CATEGORY = "NotificationFCM.KEY_CATEGORY"
    }

    object CAR_PARK_BAND {
        const val ALL = "All"
    }

    object LOCATION {
        const val DEFAULT_INTERVAL_IN_MILLISECONDS = 5000L
        const val DEFAULT_FASTEST_INTERVAL_IN_MILLISECONDS = 2000L
        const val DEFAULT_MAX_WAIT_TIME = DEFAULT_INTERVAL_IN_MILLISECONDS * 10
        const val DEFAULT_LOCATION_CIRCLE_PULSE_DURATION_MS = 2300
    }

    object ERP {
        const val ERP_FEATURE_COLLECTION = "ERP_FEATURE_COLLECTION"
        const val NO_CHARGE_ERP_FEATURE_COLLECTION = "NO_CHARGE_ERP_FEATURE_COLLECTION"
        const val ERP_RATE = "ERP_RATE"
    }

    object TRIPETA {
        const val INIT = "INIT"
        const val BEFORE_15MIN = "15MIN_BFR"
        const val REMINDER_1 = "REMINDER_1"
        const val MESSAGE_INTERVAL_15 = 15 * 60
        const val MESSAGE_INTERVAL_5 = 5 * 60
        const val YES = "Y"
        const val NO = "N"
        const val LIVE_LOCATION_INPROGRESS = "INPROGRESS"
        const val LIVE_LOCATION_PAUSE = "PAUSE"
        const val LIVE_LOCATION_COMPLETE = "COMPLETE"
        const val LIVE_LOCATION_CANCEL = "CANCEL"
        const val CONTACT_NAME = "contactname"
        const val CONTACT_NUMBER = "contactnumber"
        const val ETA_MESSAGE = "etamessage"
        const val ETA_RN_STATUS = "etastatus"
        const val ETA_RN_INIT = "init"
        const val ETA_RN_EDIT = "edit"
        const val ETA_RN_SHARE_LIVE_LOC = "shareliveloc"
        const val ETA_RN_SEND_VOICE = "sendvoice"
        const val ETA_SCREEN = "ETAScreen"
        const val ETA_MODE = "etaMode"
        const val ETA_MODE_NAVIGATION = "Navigation"
        const val ETA_MODE_CRUISE = "Cruise"
        const val ETA_DESTINATION = "destination"
        const val ETA_LAT = "lat"
        const val ETA_LONG = "long"
        const val FAV_ETA_MESSAGE = "etaMessage"
        const val TRIP_ETA_FAVOURITE_ID = "tripEtaFavouriteId"
    }


    object WALKATHON_TRACKING {
        const val LIVE_LOCATION_INPROGRESS = "INPROGRESS"
        const val LIVE_LOCATION_PAUSE = "PAUSE"
        const val LIVE_LOCATION_COMPLETE = "COMPLETE"
        const val LIVE_LOCATION_CANCEL = "CANCEL"
    }

    object CARPARK_MOREINFO {
        const val TO_PARKING_MOREINFO_SCREEN = "ParkingPriceDetail"
        const val PARKING_ID = "parkingId"
        const val AWS_ID_TOKEN = "idToken"
    }

    object CONTENT_LAYER {
        const val CONTENT_TYPE_ZONE = "ZONE"
        const val CONTENT_TYPE_GLOBAL_POI = "GLOBAL_POIS"
    }

    object AMENITY {
        const val ANE = "AnE"
    }

    object POI_AMENITY_MOREINFO {
        const val TO_POI_MOREINFO_SCREEN = "TBRAmenityDetails"
        const val POI_ID = "id"
        const val AWS_ID_TOKEN = "idToken"
    }

    object RN_CONSTANTS {
        const val COMPONENT_NAME = "Breeze"
        const val SEARCH_SCREEN = "SearchLocation"
        const val ROUTE_PLANNING = "RoutePlanning"
        const val OBU_DISPLAY = "OBUDisplay"
        const val FAV_ADD_NEW_SCREEN = "FavouriteAddNew"
        const val FAV_LIST_SCREEN = "FavouriteList"
        const val ETA_FAVOURITES_SCREEN = "ETAFavouriteList"
        const val ONBOARDING_USER_PREFERENCE_SCREEN = "UserPreference"
        const val ONBOARDING_STATE = "onboardingState"
        const val ONBOARDING_INIT = "ONBOARDING_INIT"
        const val ONBOARDING_STEP_1 = "ONBOARDING_STEP_1"
        const val ONBOARDING_COMPLETED = "ONBOARDING_COMPLETED"
        const val FRAGMENT_ID = "fragmentId"
        const val OPEN_AS = "openAs"
        const val OPEN_AS_FRAGMENT = "Fragment"
        const val OPEN_AS_ACTIVITY = "Activity"
        const val NAVIGATION_PARAMS = "navigationParams"
        const val BASE_URL = "baseURL"
        const val ID_TOKEN = "idToken"
        const val DEVICE_OS = "deviceos"
        const val DEVICE_OS_VERSION = "deviceosversion"
        const val DEVICE_MODEL = "devicemodel"
        const val APP_VERSION = "appversion"
        const val ERP_ID = "erpId"
        const val ERP_IS_SELECTED = "isSelected"
        const val ERP_SELECTED_TS = "erpSelTS"
        const val INBOX_SCREEN = "NAVIGATE_TO_INBOX_SCREEN"
        const val SHOW_PLAN_TRIP = "showPlanTrip"
        const val ONBOARDING_TUTORIAL_SCREEN = "Tutorial"
        const val CAR_DETAILS_SCREEN = "CarDetailScreen"
        const val PREFERENCES_SCREEN = "PreferenceSettings"
        const val BTM_SHEET_STATE_INDEX = "index"
        const val TBR_AMENITY_DETAILS = "TBRAmenityDetails"

        const val FOCUSED_LAT = "lat"
        const val FOCUSED_LONG = "long"

        const val USER_PREFERENCE_ELEMENT_ID = "element_id"
        const val USER_PREFERENCE_SUB_ITEM = "sub_item"
        const val USER_PREFERENCE_SELECTED = "is_selected"

        const val APP_TUTORIAL_SCREEN = "AppTutorial"
        const val WEBVIEWURL = "webviewUrl"
        const val TITLE = "title"
    }

    object MAPBOXSTUDIOFONTFAMILY {
        const val BOLD = "SF Pro Display Semibold"
        const val REGULAR = "SF Pro Display Regular"
        const val LIGHT = "SF Pro Display Light"
        const val MEDIUM = "SF Pro Display Medium"
    }

    val LIST_VOICE_TEXT = mapOf(
        "PIE" to "Pee Aai Eee",
        "CTE" to "See Tee Eee",
        "KPE" to "Ke Pee Eee",
        "ECP" to "Ee See Pee",
        "AYE" to "Ae Vai Eee",
        "TPE" to "Tee Pee Eee",
        "SLE" to "Es Ell Eee",
        "KJE" to "Kai Jey Eee",
        "ERP" to "Eee Aar Pee",
        "MCE" to "Em See Eee",
        "BKE" to "Bee Ke Eee",
        "NSC" to "En Es See"
    )

    val LIST_VOICE_TEXT_SPACES = mapOf(
        "PIE" to "P I E ",
        "CTE" to "C T E ",
        "KPE" to "K P E ",
        "ECP" to "E C P ",
        "AYE" to "A Y E ",
        "TPE" to "T P E ",
        "SLE" to "S L E ",
        "KJE" to "K J E ",
        "ERP" to "E R P ",
        "MCE" to "M C E ",
        "BKE" to "B K E ",
        "NSC" to "N S C "
    )

    val LIST_VOICE_TEXT_IOS = mapOf(
        "PIE" to "P.I.E.",
        "CTE" to "C.T.E.",
        "KPE" to "K.P.E.",
        "ECP" to "E.C.P.",
        "AYE" to "A.Y.E.",
        "TPE" to "T.P.E.",
        "SLE" to "S.L.E.",
        "KJE" to "K.J.E.",
        "ERP" to "E.R.P.",
        "MCE" to "M.C.E.",
        "BKE" to "B.K.E.",
        "NSC" to "N.S.C."
    )

    val LIST_VOICE_TEXT_ORIGINAL = mapOf(
        "PIE" to "P.I.E",
        "CTE" to "C.T.E",
        "KPE" to "K.P.E",
        "ECP" to "E.C.P",
        "AYE" to "A.Y.E",
        "TPE" to "T.P.E",
        "SLE" to "S.L.E",
        "KJE" to "K.J.E",
        "ERP" to "E.R.P",
        "MCE" to "M.C.E",
        "BKE" to "B.K.E",
        "NSC" to "N.S.C"
    )

    object HISTORY_MODE {
        const val UNKNOWN_MODE = "UnknownMode"
    }


    val SINGAPORE_REGION_COORDS =
        listOf<Point>(
            Point.fromLngLat(103.5989916, 1.2567006),
            Point.fromLngLat(103.5940314, 1.1947298),
            Point.fromLngLat(103.7753058, 1.2173841),
            Point.fromLngLat(103.8288642, 1.2331734),
            Point.fromLngLat(104.0115119, 1.2880917),
            Point.fromLngLat(104.0575172, 1.3457547),
            Point.fromLngLat(104.0266181, 1.3714967),
            Point.fromLngLat(103.998809, 1.4178315),
            Point.fromLngLat(103.940941, 1.4291728),
            Point.fromLngLat(103.9052355, 1.4253974),
            Point.fromLngLat(103.8403475, 1.4696719),
            Point.fromLngLat(103.8080751, 1.4761929),
            Point.fromLngLat(103.7747728, 1.4538842),
            Point.fromLngLat(103.7212145, 1.4549138),
            Point.fromLngLat(103.6805307, 1.43741),
            Point.fromLngLat(103.6669695, 1.4152725),
            Point.fromLngLat(103.6539232, 1.3919336),
            Point.fromLngLat(103.627144, 1.3473144),
            Point.fromLngLat(103.6113512, 1.3109319),
            Point.fromLngLat(103.5989916, 1.2567006)
        )

    const val IS_FROM_SIGN_IN = "IS_FROM_SIGN_IN"
}

object INTENT_KEY {
    const val AUTO_SIGN_IN_GEUST_MODE = "AUTO_SIGN_IN_GEUST_MODE"
    const val IS_OPEN_GUEST_FROM_CREATE_ACCOUNT_SCREEN = "IS_OPEN_GUEST_FROM_CREATE_ACCOUNT_SCREEN"

}

object RNOpenAs {
    const val FRAGMENT = "Fragment"
    const val ACTIVITY = "Activity"
}

object TYPE_BROADCAST_MESSAGE {
    const val PLANNING = "planning"
    const val NAVIGATION = "navigation"
}


object RNOnboardingState {
    const val INIT = "ONBOARDING_INIT"
    const val STEP_1 = "ONBOARDING_STEP_1"
    const val COMPLETED = "ONBOARDING_COMPLETED"
}


object NavigationType {
    const val NAVIGATION = "NAVIGATION"
    const val CRUISE = "CRUISE"
}


object BreezeDynamicLink {
    const val HOST_DYNAMIC_LINK = "breeze.com"
    const val PREFIX_SHARE_COLLECTION = "sharecollection"
    const val PARAM_TOKEN = "token"
}


object RNArgKey {
    const val TO_SCREEN = "toScreen"
    const val ONBOARDING_STATE = "onboardingState"
    const val FRAGMENT_ID = "fragmentId"
    const val OPEN_AS = "openAs"
    const val NAVIGATION_PARAMS = "navigationParams"
    const val BASE_URL = "baseURL"
    const val ID_TOKEN = "idToken"
    const val DEVICE_OS = "deviceos"
    const val DEVICE_OS_VERSION = "deviceosversion"
    const val DEVICE_MODEL = "devicemodel"
    const val APP_VERSION = "appversion"
}

object GUEST_SIGN_UP_STATE {
    val GET_ID_GUEST_NOT_SUCCESS = "GET_ID_GUEST_NOT_SUCCESS"
    val GET_ID_GUEST_SUCCESS = "GET_ID_GUEST_SUCCESS"
    val SIGNUP_SUCCESS = "SIGN_UP_SUCCESS"
    val SIGNIN_SUCCESS = "SIGN_IN_SUCCESS"
    val NEED_CONFIRM_SIGNIN = "NEED_CONFIRM_SIGN_IN"
    val CONFIRM_SIGNIN = "CONFIRM_SIGN_IN"
    val FETCH_TOKEN_SUCCESS = "FETCH_TOKEN_SUCCESS"
    val FETCH_USER_ATTR = "FETCH_USER_ATTR"
    val FETCH_USER_SETTING_AND_ALL_SETTING = "FETCH_USER_SETTING_AND_ALL_SETTING"
}

object PHONE_NUMBER_LOGIN_STATE {
    val CONFIRM_SIGN_UP_SUCCESS = "CONFIRM_SIGN_UP_SUCCESS"
    val SIGNIN_SUCCESS = "SIGN_IN_SUCCESS"
    val CONFIRM_SIGN_IN = "CONFIRM_SIGN_IN_SUCCESS"
    val FETCH_TOKEN_SUCCESS = "FETCH_TOKEN_SUCCESS"
    val FETCH_USER_ATTR = "FETCH_USER_ATTR"
    val NOT_START = "NOT_START"
}


object LOGIN_WITH_SOCIAL_STATE {
    val SIGNIN_SUCCESS = "SIGNIN_SUCCESS"
    val FETCH_TOKEN_SUCCESS = "FETCH_TOKEN_SUCCESS"
    val FETCH_USER_ATTR = "FETCH_USER_ATTR"
    val NOT_START = ""
}


object STATE_FETCH_USER_ATTR {
    val USER_NO_USERNAME_ATTRIBUTE = "USER_NO_USERNAME_ATTRIBUTE"
    val USER_USERNAME_AVAILABLE = "USER_USERNAME_AVAILABLE"
    val FETCH_ATTR_USER_FAILS = "FETCH_ATTR_USER_FAILS"
}


object LOGIN_TYPE {
    val GOOGLE = "GOOGLE"
    val FACEBOOK = "FACEBOOK"
}

object INSTABUG {
    const val KEY_USER_ID = "user_id"
}

object MODE_LOGIN_TYPE {
    val GUEST_MODE_SIGN_IN = 0
    val GUEST_MODE_CREATE_ACCOUNT = 1
    val SIGN_IN = 2
    val CREATE_ACCOUNT = 3
    val CREATE_ACCOUNT_FROM_PROFILE = 4
    val SIGN_IN_FROM_PROFILE = 5
}

object TripOBUStatus {
    val NOT_PAIRED = "NOT_PAIRED"
    val NOT_CONNECTED = "NOT_CONNECTED"
    val CONNECTED = "CONNECTED"
    val PARTIALLY_CONNECTED = "PARTIALLY_CONNECTED"
}

object ShareDestinationType {
    const val DEFAULT = "DEFAULT"
    const val ROUTE_PLANNING = "ROUTE_PLANNING"
    const val NAVIGATION = "NAVIGATION"
}

package com.breeze.model.enums

import com.facebook.react.bridge.Arguments
import com.facebook.react.bridge.ReadableMap
import com.google.gson.annotations.SerializedName

data class RemoteImage(
    @SerializedName("dark")
    var dark: String,
    @SerializedName("light")
    var light: String
) {

    fun toRNWritableMap(): ReadableMap = Arguments.createMap().apply {
        putString("dark", dark)
        putString("light", light)
    }
}
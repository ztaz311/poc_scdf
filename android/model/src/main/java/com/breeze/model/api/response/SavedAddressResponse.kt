package com.breeze.model.api.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SavedAddressResponse(
    @field:SerializedName("addresses")
    val addresses: List<SavedSearchItem?>? = null
) : Parcelable

@Parcelize
data class SavedSearchItem(

    @field:SerializedName("address2")
    val address2: String? = null,

    @field:SerializedName("address1")
    val address1: String? = null,

    @field:SerializedName("lat")
    val lat: String? = null,

    @field:SerializedName("long")
    val jsonMemberLong: String? = null,

    @field:SerializedName("alias")
    val alias: String? = null
) : Parcelable


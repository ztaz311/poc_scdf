/**
 * @format
 */

import { AppRegistry } from 'react-native'
import 'react-native-gesture-handler'
import 'react-native-reanimated'
import App from './App'
import { name as appName } from './app.json'
import ShareBusinessLogic from './app/src/services/share-business-logic/share-business-logic.service'

ShareBusinessLogic.startHandleShareBusinessLogic()

AppRegistry.registerComponent(appName, () => App)

import { observer } from 'mobx-react'
import React, { useCallback, useEffect, useState } from 'react'
import { Image, Pressable, StyleSheet, View } from 'react-native'
import FastImage from 'react-native-fast-image'
import { SafeAreaView } from 'react-native-safe-area-context'
import { AppText } from '../../../components'
import ScreenHeader from '../../../components/ScreenHeader'
import { analyticEvents } from '../../../constants/analyticEvents'
import { Colors } from '../../../constants/appColors'
import { Images } from '../../../constants/appImages'
import { useTheme } from '../../../contexts/ThemeContext'
import { VoucherTabsEnum } from '../../../enums/voucher-tabs.enum'
import AlertService from '../../../services/alert.service'
import CollectionService from '../../../services/collection.service'
import LandingDataService from '../../../services/landing-data.service'
import NavigationDataService from '../../../services/navigation-data.service'
import PreferencesSettingsService from '../../../services/preferences-settings.service'
import VoucherService from '../../../services/voucher.service'
import AddNewModal from './components/AddNewModal'
import SavedList from './components/SavedList'

const { sendAnalyticsEventsToNative, sendAnalyticsEventsScreenNameToNative } = NavigationDataService

const sendAnalytics = (event: string, value: string, saveToLocal = false) => {
  sendAnalyticsEventsToNative(event, value, analyticEvents.SavedCollectionEvents.screen_name, saveToLocal)
}

type Props = {}

const CollectionListScreen: React.FC<Props> = () => {
  const [isShowModal, setIsShowModal] = useState<boolean>(false)
  const [checkEdit, setCheckEdit] = useState<number>(0)
  const { themeColors, themeImages } = useTheme()
  const [dataEdit, setDataEdit] = useState({
    name: '',
    description: '',
  })

  const fetchDataFromAPI = async () => {
    console.log('6')
    await CollectionService.getCollectionDataFromAPI()
  }

  useEffect(() => {
    sendAnalyticsEventsScreenNameToNative(analyticEvents.SavedCollectionsV2Events.screen_name)
    VoucherService.changeActiveTab(VoucherTabsEnum.Vouchers)
    fetchDataFromAPI()
  }, [])

  useEffect(() => {
    return () => {
      LandingDataService.setShowBottomBar(true)
    }
  }, [])

  const onToggleModal = (idEdit?: number, data?: { name: string; description: string }) => {
    fetchDataFromAPI()
    setIsShowModal(!isShowModal)
    if (idEdit) {
      setCheckEdit(idEdit)
    }
    if (data) {
      setDataEdit(data)
    }
  }

  useEffect(() => {
    if (!isShowModal) {
      setCheckEdit(0)
      setDataEdit({
        name: '',
        description: '',
      })
    }
  }, [checkEdit, isShowModal])

  const showCollectionLimitedWarning = useCallback(() => {
    AlertService.alert(
      '',
      `You have reached the maximum of ${PreferencesSettingsService.collectionsLimitSetting} collections.`,
      [
        {
          text: 'Got it',
        },
      ],
    )
  }, [])

  const onAddNew = () => {
    NavigationDataService.sendAnalyticsEventsToNative(
      analyticEvents.user_click,
      analyticEvents.SavedCollectionsV2Events.user_click.add_new_collection,
      analyticEvents.SavedCollectionsV2Events.screen_name,
    )
    const newArray = CollectionService.collections.filter(element => true)
    if (newArray.length >= PreferencesSettingsService.collectionsLimitSetting) {
      showCollectionLimitedWarning()
    } else {
      onToggleModal()
    }
  }
  const handleInfoClose = () => {
    NavigationDataService.sendAnalyticsEventsToNative(
      analyticEvents.user_click,
      analyticEvents.SavedCollectionsV2Events.user_click.close_dialogue,
      analyticEvents.SavedCollectionsV2Events.screen_name,
    )
    CollectionService.setInfoPopupDismissed(true)
  }

  return (
    <SafeAreaView style={[styles.container, { backgroundColor: themeColors.primaryBackground }]}>
      <ScreenHeader
        title="Saved Collection"
        hasBackButton={false}
        containerStyle={{ backgroundColor: themeColors.primaryBackground }}
      />
      {!CollectionService.infoPopupDismissed && (
        <View style={styles.infoContainer}>
          <AppText style={styles.infoText}>
            Save your favourite places and organise them into folders to share with friends and family or find them
            easily.
          </AppText>

          <Pressable onPress={handleInfoClose} style={styles.infoCrossBtn}>
            <FastImage source={Images.icPlusPurpleThin} style={styles.infoCrossIcon} />
          </Pressable>
        </View>
      )}
      <SavedList
        onEditModalOpen={collection => {
          onToggleModal(collection.collectionId, {
            name: collection.name,
            description: collection.description,
          })
        }}
      />
      <Pressable onPress={onAddNew} style={[styles.addNewButton, { borderColor: themeColors.primaryColor }]}>
        <Image
          source={themeImages.icPlusPurpleThin}
          style={[styles.plusIcon, { tintColor: themeColors.primaryColor }]}
        />
        <AppText style={[styles.addNewTxt, { color: themeColors.primaryColor }]}>Add New Collection</AppText>
      </Pressable>
      <AddNewModal
        listData={CollectionService.collections}
        themeColors={themeColors}
        isVisible={isShowModal}
        onClose={onToggleModal}
        checkEdit={checkEdit}
        dataEdit={dataEdit}
      />
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  listHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 32,
    paddingTop: 20,
    paddingBottom: 12,
    borderBottomWidth: 0.5,
    backgroundColor: '#FCFDFC',
    borderBottomColor: '#999999',
  },
  plusIcon: {
    width: 17.5,
    height: 20,
  },
  addNewButton: {
    borderRadius: 28,
    borderWidth: 1,
    borderColor: '#782EB1',
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
    paddingVertical: 8,
    marginBottom: 28,
    marginHorizontal: 24,
  },
  addNewTxt: {
    fontSize: 20,
    color: Colors.primaryColor,
    fontWeight: '500',
    marginLeft: 8,
  },
  infoContainer: {
    paddingHorizontal: 15,
    paddingVertical: 12,
    borderRadius: 16,
    backgroundColor: '#F5EEFC',
    flexDirection: 'row',
    marginHorizontal: 18,
    marginTop: 16,
    marginBottom: 8,
  },
  infoCrossIcon: {
    width: 15,
    height: 17,
    marginLeft: 'auto',
    transform: [{ rotateZ: '45deg' }],
  },
  infoText: {
    fontSize: 14,
    color: '#782EB1',
    maxWidth: '95%',
    paddingRight: 12,
  },
  infoCrossBtn: {
    alignSelf: 'flex-start',
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 'auto',
  },
})

export default observer(CollectionListScreen)

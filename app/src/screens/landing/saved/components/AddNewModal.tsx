import { useNavigation } from '@react-navigation/native'
import React, { useCallback, useEffect, useRef, useState } from 'react'
import { Animated, Image, Pressable, StyleSheet, TextInput, TouchableOpacity, View } from 'react-native'
import { AppText } from '../../../../components'
import ScreenHeader from '../../../../components/ScreenHeader'
import { analyticEvents } from '../../../../constants/analyticEvents'
import { AppColors, Colors } from '../../../../constants/appColors'
import { Images } from '../../../../constants/appImages'
import { hybridScreens } from '../../../../constants/screensName'
import { CollectionModel } from '../../../../models/collection.model'
import AlertService from '../../../../services/alert.service'
import CollectionService from '../../../../services/collection.service'
import NavigationDataService from '../../../../services/navigation-data.service'

type Props = {
  themeColors: AppColors
  isVisible: boolean
  checkEdit: number
  dataEdit: {
    name: string
    description: string
  }
  listData?: CollectionModel[]
  onClose: () => void
  saveLocation?: (data: CollectionModel | any) => void
  chooseCollection?: boolean
}

const { sendAnalyticsEventsToNative, sendAnalyticsPopupEvent } = NavigationDataService

const AddNewModal: React.FC<Props> = ({
  isVisible,
  onClose,
  checkEdit,
  dataEdit,
  listData,
  saveLocation,
  themeColors,
  chooseCollection,
}) => {
  const [nameCollection, setNameCollection] = useState('')
  const [optional, setOptional] = useState('')
  const [errorMsg, setErrorMsg] = useState('')
  const [errorMsgOptional, setErrorMsgOptional] = useState('')
  const [initialSaveDisabled, setInitialSaveDisabled] = useState(true)
  const inputNameRef = useRef<any>('')
  const navigation = useNavigation()
  useEffect(() => {
    if (checkEdit !== 0) {
      setNameCollection(dataEdit?.name)
      setOptional(dataEdit?.description)
    } else {
      setNameCollection('')
      setOptional('')
      setErrorMsg('')
      setErrorMsgOptional('')
    }
  }, [checkEdit, dataEdit?.description, dataEdit?.name])

  // const { themeColors } = useTheme()

  useEffect(() => {
    if (!isVisible) {
      setInitialSaveDisabled(true)
    }
  }, [isVisible])

  const onChangeNameCollection = (value: string) => {
    setInitialSaveDisabled(false)
    if (chooseCollection && checkEdit === 0) {
      NavigationDataService.sendAnalyticsEventsToNative(
        analyticEvents.user_click,
        analyticEvents.HomePage.user_edit.new_collection_name_input,
        analyticEvents.HomePage.screen_name,
      )
    } else if (checkEdit !== 0) {
      NavigationDataService.sendAnalyticsEventsToNative(
        analyticEvents.user_edit,
        analyticEvents.SavedCollectionsV2Events.user_edit.edit_collection_name_input,
        analyticEvents.SavedCollectionsV2Events.screen_name,
      )
    } else {
      NavigationDataService.sendAnalyticsEventsToNative(
        analyticEvents.user_edit,
        analyticEvents.SavedCollectionsV2Events.user_edit.new_collection_description_input,
        analyticEvents.SavedCollectionsV2Events.screen_name,
      )
    }
    setNameCollection(value)
    setErrorMsg('')
  }

  const onChangeOption = (value: string) => {
    setInitialSaveDisabled(false)
    if (chooseCollection && checkEdit === 0) {
      NavigationDataService.sendAnalyticsEventsToNative(
        analyticEvents.user_edit,
        analyticEvents.HomePage.user_edit.new_collection_description_input,
        analyticEvents.HomePage.screen_name,
      )
    } else if (checkEdit !== 0) {
      NavigationDataService.sendAnalyticsEventsToNative(
        analyticEvents.user_edit,
        analyticEvents.SavedCollectionsV2Events.user_edit.edit_collection_description_input,
        analyticEvents.SavedCollectionsV2Events.screen_name,
      )
    } else {
      NavigationDataService.sendAnalyticsEventsToNative(
        analyticEvents.user_edit,
        analyticEvents.SavedCollectionsV2Events.user_edit.new_collection_description_input,
        analyticEvents.SavedCollectionsV2Events.screen_name,
      )
    }
    setOptional(value)
    setErrorMsgOptional('')
  }

  const onClearNameCollection = () => {
    setInitialSaveDisabled(false)
    if (chooseCollection && checkEdit === 0) {
      NavigationDataService.sendAnalyticsEventsToNative(
        analyticEvents.user_click,
        analyticEvents.HomePage.user_click.new_collection_name_textbox_clear,
        analyticEvents.HomePage.screen_name,
      )
    } else if (checkEdit !== 0) {
      NavigationDataService.sendAnalyticsEventsToNative(
        analyticEvents.user_click,
        analyticEvents.SavedCollectionsV2Events.user_click.edit_collection_name_textbox_clear,
        analyticEvents.SavedCollectionsV2Events.screen_name,
      )
    } else {
      NavigationDataService.sendAnalyticsEventsToNative(
        analyticEvents.user_click,
        analyticEvents.SavedCollectionsV2Events.user_click.edit_collection_name_textbox_clear,
        analyticEvents.SavedCollectionsV2Events.screen_name,
      )
    }
    setNameCollection('')
  }

  const onEditCollection = (id: number) => {
    if (nameCollection.trim() !== '') {
      const checkCollection =
        listData?.filter(el => el.name.trim().toLowerCase() === nameCollection.trim().toLowerCase()) || []
      if (checkCollection?.length === 0 || checkCollection[0].description !== optional) {
        CollectionService.editCollectionFromAPI(id, nameCollection, optional).then(() => {
          onClose()
          NavigationDataService.showCustomToast({ message: 'Saved edits' })
        })
      } else {
        setErrorMsg('This name has been saved before. Please try a new name.')
      }
    } else {
      setErrorMsg('Name of collection is required.')
    }
  }

  const moveCollectionDetails = (id: number) => {
    NavigationDataService.openNativeScreen(hybridScreens.CollectionDetails, {
      collection_id: id,
      collection_name: nameCollection,
      to_rn_screen: hybridScreens.CollectionDetails,
    })
  }

  const onSubmit = () => {
    if (initialSaveDisabled) return
    let checkValidate = false
    if (nameCollection.trim() !== '') {
      //edit
      if (checkEdit !== 0) {
        onEditCollection(checkEdit)
        return
      }
      if (!/^[a-zA-Z0-9`!@#$%^&*()_+\-=\[\]{};':"\\|,.<>₫’”¥€•\/?~\s]*$/.test(nameCollection)) {
        setErrorMsg('Name should only contain English alphabets')
        checkValidate = true
      }
      if (!/^[a-zA-Z0-9`!@#$%^&*()_+\-=\[\]{};':"\\|,.<>₫’”¥€•\/?~\s]*$/.test(optional)) {
        setErrorMsgOptional('Description should only contain English alphabets')
        checkValidate = true
      }
      if (checkValidate) {
        return
      }
      //create
      const found = listData?.some(el => el.name.trim().toLowerCase() === nameCollection.trim().toLowerCase())
      if (!found) {
        CollectionService.createCollectionFromAPI(nameCollection, optional).then(s => {
          onClose()
          setErrorMsg('')
          setNameCollection('')
          setOptional('')
          setErrorMsgOptional('')
          if (saveLocation) {
            saveLocation?.({
              name: nameCollection,
              collectionId: s.collectionId,
              code: 'NORMAL',
              description: optional,
            })
            navigation.goBack()
          } else {
            CollectionService.getCollectionDataFromAPI().finally(() => {
              moveCollectionDetails(s.collectionId)
            })
          }
        })
      } else {
        setErrorMsg('This name has been saved before. Please try a new name.')
      }
    } else {
      setErrorMsg('Name of collection is required.')
    }
  }

  const openShowKeyboard = () => {
    inputNameRef.current.focus()
  }

  const onDelete = useCallback(() => {
    CollectionService.deleteCollectionFromAPI(checkEdit).then(() => {
      onClose()
      NavigationDataService.showCustomToast({ message: `Deleted ${dataEdit?.name}`, icon: Images.icRemove })
    })
  }, [checkEdit, onClose])

  const onConfirmDelete = useCallback(() => {
    if (checkEdit !== 0) {
      NavigationDataService.sendAnalyticsEventsToNative(
        analyticEvents.user_click,
        analyticEvents.SavedCollectionsV2Events.user_click.edit_collection_delete_collection,
        analyticEvents.SavedCollectionsV2Events.screen_name,
      )

      NavigationDataService.sendAnalyticsEventsToNative(
        analyticEvents.user_click,
        analyticEvents.SavedCollectionsV2Events.user_popup.popup_open,
        analyticEvents.SavedCollectionsV2Events.screen_name,
      )
    }
    AlertService.alert('', 'Are you sure you want to delete this collection?', [
      {
        text: 'Cancel',
        onPress: () => {
          NavigationDataService.sendAnalyticsEventsToNative(
            analyticEvents.user_click,
            analyticEvents.SavedCollectionsV2Events.user_click.popup_delete_collection_cancel,
            analyticEvents.SavedCollectionsV2Events.screen_name,
          )
        },
        style: 'cancel',
      },
      {
        text: 'Delete',
        style: 'destructive',
        onPress: () => {
          NavigationDataService.sendAnalyticsEventsToNative(
            analyticEvents.user_click,
            analyticEvents.SavedCollectionsV2Events.user_click.popup_delete_collection_delete,
            analyticEvents.SavedCollectionsV2Events.screen_name,
          )
          onDelete()
        },
      },
    ])
  }, [onDelete])

  return isVisible ? (
    <View style={styles.container} pointerEvents="box-none">
      {!chooseCollection && <View style={styles.wrapperField} />}
      <Animated.View style={[styles.bottom, { backgroundColor: themeColors?.primaryBackground || Colors.white }]}>
        <ScreenHeader
          title={checkEdit !== 0 ? 'Edit Collection' : 'New Collection'}
          isRounded
          isSmall
          showShadow={false}
          actionBtnRight={() => {
            if (chooseCollection && checkEdit === 0) {
              NavigationDataService.sendAnalyticsEventsToNative(
                analyticEvents.user_click,
                analyticEvents.HomePage.user_click.new_collection_close,
                analyticEvents.HomePage.screen_name,
              )
            } else if (checkEdit !== 0) {
              NavigationDataService.sendAnalyticsEventsToNative(
                analyticEvents.user_click,
                analyticEvents.SavedCollectionsV2Events.user_click.edit_collection_close,
                analyticEvents.SavedCollectionsV2Events.screen_name,
              )
            } else {
              NavigationDataService.sendAnalyticsEventsToNative(
                analyticEvents.user_click,
                analyticEvents.SavedCollectionsV2Events.user_click.new_collection_close,
                analyticEvents.SavedCollectionsV2Events.screen_name,
              )
            }

            onClose()
          }}
          hasBackButton={false}
          textBtnRight="Close"
          showDashedLine
        />

        <View style={styles.wrapper}>
          <View style={styles.txtOptions}>
            <AppText style={[styles.descriptionTxt, { color: themeColors?.primaryText || Colors.primaryText }]}>
              Collection Name
            </AppText>
          </View>
          <View>
            <TextInput
              onLayout={openShowKeyboard}
              ref={inputNameRef}
              style={[
                styles.inputNameStyle,
                {
                  backgroundColor: themeColors?.inputModal || '#EBEBEB',
                  color: themeColors?.primaryText || '#222638',
                  borderWidth: errorMsg === '' ? 0 : 1,
                  borderColor: errorMsg === '' ? 'transparent' : '#E82370',
                },
              ]}
              autoCapitalize="none"
              autoCorrect={false}
              maxLength={20}
              keyboardType="default"
              returnKeyType="default"
              onChangeText={onChangeNameCollection}
              blurOnSubmit={false}
              value={nameCollection}
              allowFontScaling={false}
            />
            {nameCollection !== '' && (
              <TouchableOpacity style={styles.clearTextIcon} onPress={onClearNameCollection}>
                <Image source={Images.clearTextIcon} style={styles.imgClearIcon} resizeMode="contain" />
              </TouchableOpacity>
            )}
          </View>
          <AppText style={styles.errorMsg}>{errorMsg}</AppText>
        </View>
        <View style={styles.wrapper}>
          <View style={styles.txtOptions}>
            <AppText style={[styles.descriptionTxt, { color: themeColors?.primaryText || Colors.primaryText }]}>
              A brief description (Optional)
            </AppText>
          </View>
          <TextInput
            multiline={false}
            style={[
              styles.inputFieldStyle,
              {
                backgroundColor: themeColors?.inputModal || '#EBEBEB',
                color: themeColors?.primaryText || '#222638',
                borderWidth: errorMsgOptional === '' ? 0 : 1,
                borderColor: errorMsgOptional === '' ? 'transparent' : '#E82370',
              },
            ]}
            blurOnSubmit={false}
            returnKeyType="default"
            maxLength={26}
            onChangeText={onChangeOption}
            value={optional}
            allowFontScaling={false}
          />
          <AppText style={styles.errorMsg}>{errorMsgOptional}</AppText>
        </View>
        <View style={styles.actionContainer}>
          {checkEdit !== 0 && (
            <Pressable style={[styles.actionBtn, { borderColor: themeColors.redPink }]} onPress={onConfirmDelete}>
              <AppText style={[styles.actionBtnText, { color: themeColors.redPink }]}>Delete</AppText>
            </Pressable>
          )}
          <Pressable
            style={[
              styles.actionBtn,
              {
                borderColor: initialSaveDisabled ? '#BCBFC3' : themeColors.primaryColor,
                backgroundColor: initialSaveDisabled ? '#BCBFC3' : themeColors.primaryColor,
                width: checkEdit !== 0 ? styles.actionBtn.width : '100%',
              },
            ]}
            onPress={() => {
              if (chooseCollection && checkEdit === 0) {
                NavigationDataService.sendAnalyticsEventsToNative(
                  analyticEvents.user_click,
                  analyticEvents.SavedCollectionsV2Events.user_click.new_collection_save,
                  analyticEvents.HomePage.screen_name,
                )
              } else if (checkEdit !== 0) {
                NavigationDataService.sendAnalyticsEventsToNative(
                  analyticEvents.user_click,
                  analyticEvents.SavedCollectionsV2Events.user_click.edit_collection_save,
                  analyticEvents.SavedCollectionsV2Events.screen_name,
                )
              } else {
                NavigationDataService.sendAnalyticsEventsToNative(
                  analyticEvents.user_click,
                  analyticEvents.SavedCollectionsV2Events.user_click.new_collection_save,
                  analyticEvents.SavedCollectionsV2Events.screen_name,
                )
              }
              onSubmit()
            }}>
            <AppText style={[styles.actionBtnText, { color: themeColors.primaryBackground }]}>Save</AppText>
          </Pressable>
        </View>
      </Animated.View>
    </View>
  ) : // {/* </KeyboardWrapper> */}
  null
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
  },
  bottom: {
    height: '90%',
    width: '100%',
    position: 'absolute',
    bottom: 0,
    borderTopStartRadius: 16,
    borderTopEndRadius: 16,
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingLeft: 32,
    paddingRight: 24,
    paddingVertical: 24,
    borderTopStartRadius: 16,
    borderTopEndRadius: 16,
  },
  wrapper: {
    marginTop: 10,
    marginHorizontal: 22,
  },
  clearTextIcon: {
    position: 'absolute',
    right: 8,
    top: 11,
    justifyContent: 'center',
    alignItems: 'center',
    width: 25,
    height: 25,
  },
  imgClearIcon: {
    width: 16,
    height: 16,
  },
  txtOptions: {
    // marginTop: 29,
    marginBottom: 12,
  },
  inputFieldStyle: {
    height: 48,
    borderRadius: 8,
    paddingHorizontal: 22,
  },
  inputNameStyle: {
    height: 48,
    borderRadius: 29,
    paddingHorizontal: 22,
  },
  errorMsg: {
    color: '#E82370',
    fontWeight: '500',
    marginVertical: 5,
    marginLeft: 21,
  },
  wrapperField: {
    backgroundColor: '#393F58',
    opacity: 0.7,
    flex: 1,
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
  },
  closeTxt: {
    fontWeight: '500',
  },
  descriptionTxt: {
    fontSize: 14,
    fontWeight: '600',
  },
  backImg: {
    width: 40,
    height: 40,
  },
  titleText: {
    fontSize: 18,
    fontWeight: '500',
  },
  actionContainer: {
    flexDirection: 'row',
    paddingHorizontal: 22,
    marginBottom: 30,
    width: '100%',
    justifyContent: 'space-between',
  },
  actionBtn: {
    borderRadius: 28,
    borderWidth: 1,
    height: 48,
    justifyContent: 'center',
    alignItems: 'center',
    width: '45%',
  },
  actionBtnText: {
    fontSize: 20,
  },
})

export default AddNewModal

import React from 'react'
import { Image, Pressable, StyleSheet, View } from 'react-native'
import FastImage, { Source } from 'react-native-fast-image'
import AppText from '../../../../components/AppText'
import { analyticEvents } from '../../../../constants/analyticEvents'
import { Images } from '../../../../constants/appImages'
import { collectionColors, collectionImages, collectionImagesDarkMode } from '../../../../constants/collectionColor'
import { hybridScreens, nativeScreens } from '../../../../constants/screensName'
import { useTheme } from '../../../../contexts/ThemeContext'
import { useDebounce } from '../../../../hooks/useDebounce'
import { useLocalUserData } from '../../../../hooks/useLocalUserData'
import { CollectionModel } from '../../../../models/collection.model'
import CollectionService from '../../../../services/collection.service'
import NavigationDataService from '../../../../services/navigation-data.service'
type Props = {
  data: CollectionModel
  onEdit: (collectionData: CollectionModel) => void
  showIcon?: Source
}

const SavedItem: React.FC<Props> = ({ data, onEdit, showIcon }) => {
  const { isDarkMode, themeColors } = useTheme()
  const imageIndex = collectionColors.findIndex(s => s === data.image)
  const isDisabled = data.code === 'SAVED_PLACES' || data.code === 'SHORTCUTS'
  const { debounce } = useDebounce()

  const ownerName = useLocalUserData().userName
  const handleShare = () => {
    debounce(() => {
      NavigationDataService.sendAnalyticsEventsToNative(
        analyticEvents.user_click,
        analyticEvents.SavedCollectionsV2Events.user_click.share_collection,
        analyticEvents.SavedCollectionsV2Events.screen_name,
      )
      CollectionService.handleCollectionShare(data.collectionId, ownerName)
    })
  }

  const handleClickItem = () => {
    CollectionService.setRetainSavePendingOnInitialize(false)
    const navigationParamsToNative = {
      collection_id: data?.collectionId,
      collection_name: data.name,
      collection_description: data.description,
      to_rn_screen: data.code === 'SHORTCUTS' ? hybridScreens.ShortcutFullList : hybridScreens.CollectionDetails,
      code: data.code,
    }
    NavigationDataService.openNativeScreen(nativeScreens.CollectionDetails, navigationParamsToNative)
  }

  return (
    <Pressable
      style={[styles.container, { borderColor: themeColors.divider }]}
      onPress={() => {
        showIcon ? onEdit(data) : handleClickItem()
      }}>
      <Image
        source={isDarkMode ? collectionImagesDarkMode[imageIndex] : collectionImages[imageIndex]}
        style={styles.imgStyle}
      />
      <View style={styles.dataContainer}>
        <AppText style={[styles.txtName, { color: themeColors.textColorForDark }]}>{data.name}</AppText>
        <AppText numberOfLines={2} style={[styles.txtPlace, { color: themeColors.secondaryText }]}>
          {data.count} saved place{data.count !== 1 ? 's' : ''}
        </AppText>
        {data.description !== null && data.description !== '' && (
          <AppText numberOfLines={2} style={[styles.txtDescription, { color: themeColors.secondaryText }]}>
            {data.description}
          </AppText>
        )}
      </View>
      {showIcon ? (
        <FastImage source={showIcon} resizeMode="contain" style={styles.icEdit} />
      ) : (
        <>
          <Pressable style={styles.shareButton} onPress={handleShare}>
            <Image
              source={Images.icSharePurple}
              resizeMode="contain"
              style={[styles.icShare, { tintColor: themeColors.primaryColor }]}
            />
          </Pressable>
          <Pressable
            style={styles.editButton}
            onPress={() => {
              if (isDisabled) return
              NavigationDataService.sendAnalyticsEventsToNative(
                analyticEvents.user_click,
                analyticEvents.SavedCollectionsV2Events.user_click.edit_collection,
                analyticEvents.SavedCollectionsV2Events.screen_name,
              )
              onEdit(data)
            }}>
            <Image
              source={isDisabled ? Images.icEditPencilGrey : Images.icEditPencilPurple}
              resizeMode="contain"
              style={[styles.icEdit, { ...(!isDisabled && { tintColor: themeColors.primaryColor }) }]}
            />
          </Pressable>
        </>
      )}
    </Pressable>
  )
}

const styles = StyleSheet.create({
  container: {
    paddingVertical: 19,
    paddingHorizontal: 20,
    borderRadius: 16,
    marginTop: 8,
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: 1,
  },
  imgStyle: {
    width: 45,
    height: 45,
    alignSelf: 'flex-start',
  },
  dataContainer: {
    marginLeft: 12,
    flex: 1,
  },
  txtName: {
    fontSize: 16,
    fontWeight: '700',
    marginBottom: 5,
  },
  txtPlace: {
    fontSize: 14,
    fontWeight: '400',
  },
  txtDescription: {
    fontSize: 14,
    fontWeight: '400',
    fontStyle: 'italic',
  },
  shareButton: {
    marginRight: 22,
  },
  editButton: {
    padding: 2,
  },
  icShare: {
    width: 20,
    height: 20,
  },
  icEdit: {
    width: 16,
    height: 16,
  },
})
export default SavedItem

import { observer } from 'mobx-react-lite'
import React from 'react'
import { Source } from 'react-native-fast-image'
import { FlatList } from 'react-native-gesture-handler'
import { CollectionModel } from '../../../../models/collection.model'
import CollectionService from '../../../../services/collection.service'
import SavedItem from './SavedItem'

type Props = {
  onEditModalOpen: (collection: CollectionModel) => void | undefined
  onScrollList?: () => void
  dataCollection?: CollectionModel[]
  showIcon?: Source
}

const SavedList: React.FC<Props> = ({ showIcon, dataCollection, onScrollList, onEditModalOpen }) => {
  return (
    <FlatList
      onScroll={() => onScrollList?.()}
      data={dataCollection ? dataCollection : CollectionService.collections}
      keyExtractor={item => `${item?.collectionId}`}
      style={{
        paddingHorizontal: 17,
      }}
      contentContainerStyle={{
        paddingBottom: 50,
      }}
      renderItem={({ item }) => <SavedItem data={item} onEdit={onEditModalOpen} showIcon={showIcon} />}
      onEndReachedThreshold={0.1}
    />
  )
}

export default observer(SavedList)

import React from 'react'
import { Image, StyleSheet, View } from 'react-native'
import { Colors } from 'react-native/Libraries/NewAppScreen'
import { AppText } from '../../../../components'
import { Images } from '../../../../constants/appImages'
import { CollectionModel } from '../../../../models/collection.model'

type Props = {
  data: CollectionModel
}

const CollectionInfor: React.FC<Props> = ({ data }) => {
  return (
    <View style={styles.wrapLeft}>
      <Image source={Images.savedViolet} style={{ width: 45, height: 45 }} />
      <View style={{ paddingLeft: 10, flex: 1 }}>
        <View style={styles.top}>
          <AppText style={styles.txtName}>{data.name}</AppText>
        </View>
        <AppText numberOfLines={2} style={styles.txtPlace}>
          {data.count} saved places
        </AppText>
        {data.description !== '' && (
          <AppText numberOfLines={2} style={styles.txtDescription}>
            {data.description}
          </AppText>
        )}
      </View>
    </View>
  )
}

export default CollectionInfor

const styles = StyleSheet.create({
  wrapLeft: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  top: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 2,
  },
  txtName: { color: Colors.primaryText, fontSize: 16, fontWeight: '500' },
  txtPlace: { width: '90%', fontSize: 14, marginTop: 8, fontWeight: '300', color: Colors.primaryText },
  txtDescription: {
    color: '#778188',
    fontSize: 14,
    fontWeight: '400',
    fontStyle: 'italic',
    marginTop: 4,
  },
})

import BottomSheet, { BottomSheetView } from '@gorhom/bottom-sheet'
import { RouteProp } from '@react-navigation/native'
import { StackNavigationProp } from '@react-navigation/stack'
import { observer } from 'mobx-react'
import React, { useCallback, useEffect, useMemo, useRef, useState } from 'react'
import { Dimensions, Image, Platform, Pressable, StyleSheet, View } from 'react-native'
import { SafeAreaView } from 'react-native-safe-area-context'
import { AppText } from '../../../../components'
import ScreenHeader from '../../../../components/ScreenHeader'
import { analyticEvents } from '../../../../constants/analyticEvents'
import { Images } from '../../../../constants/appImages'
import { hybridScreens } from '../../../../constants/screensName'
import { useTheme } from '../../../../contexts/ThemeContext'
import { BottomSheetIndexesEnum } from '../../../../enums/bottom-sheet-index.enum'
import { useDebounce } from '../../../../hooks/useDebounce'
import { useLocalUserData } from '../../../../hooks/useLocalUserData'
import { useShareBusinessLogic } from '../../../../hooks/useNativeEvents'
import { BookmarkLocation, searchLocationToBookmarkLocation } from '../../../../models/collection.model'
import { RootStackParamList } from '../../../../models/root-stack-param-list'
import { SearchLocationModel } from '../../../../models/search-location.model'
import AlertService from '../../../../services/alert.service'
import CollectionService from '../../../../services/collection.service'
import LocationInviteService from '../../../../services/location-invite.service'
import NavigationDataService from '../../../../services/navigation-data.service'
import PreferencesSettingsService from '../../../../services/preferences-settings.service'
import ShareBusinessLogic from '../../../../services/share-business-logic/share-business-logic.service'
import ShortcutDataService from '../../../../services/shortcut-data.service'
import AddNewModal from '../components/AddNewModal'
import AddBookMarkModal from './components/AddBookMarkModal'
import ListBookMark from './components/ListBookMark'
const { sendAnalyticsEventsToNative, sendAnalyticsEventsScreenNameToNative } = NavigationDataService

type Props = {
  route: RouteProp<RootStackParamList, 'CollectionDetails'>
  navigation: StackNavigationProp<RootStackParamList, 'CollectionDetails'>
}

type SaveActionsFromNative = 'ADD' | 'DELETE'

let dataCallBack: BookmarkLocation = {
  bookmarkId: -1,
  lat: '',
  long: '',
  code: 'HOME',
  address1: '',
  address2: '',
  isDestination: false,
  collectionId: -1,
  description: '',
}
const CollectionDetailsScreen: React.FC<Props> = ({ route, navigation }) => {
  const bottomSheetRef = useRef<BottomSheet>(null)
  const [bottomSheetIndex, setBottomSheetIndex] = useState(BottomSheetIndexesEnum.HALF)
  const { themeColors } = useTheme()
  const [dataBookMark, setDataBookMark] = useState<BookmarkLocation[]>([])
  const [visibleModalCallback, setVisibleModalCallBack] = useState<boolean>(false)
  const [showCollectionEditModal, setShowCollectionEditModal] = useState<boolean>(false)
  const [isPinDropped, setIsPinDropped] = useState(false)
  const [dataEdit, setDataEdit] = useState<BookmarkLocation>({
    bookmarkId: -1,
    lat: '',
    long: '',
    code: 'CUSTOM',
    address1: '',
    address2: '',
    isDestination: false,
    collectionId: -1,
    description: '',
    fullAddress: '',
    userLocationLinkId: undefined,
    userLocationLinkIdRef: null,
    placeId: undefined,
  })

  const collectionId = route.params?.collection_id || NavigationDataService.paramsFromNative?.collection_id || -1
  const collectionName =
    route.params?.collection_name || NavigationDataService.paramsFromNative?.collection_name || 'Shortcut'
  const collectionDescription =
    route.params?.collection_description || NavigationDataService.paramsFromNative?.collection_description || ''
  const collectionCode = route.params?.code || NavigationDataService.paramsFromNative?.code || ''
  const [snapPoints, setSnapPoints] = useState(['25%', '50%', '90%'])
  const { debounce } = useDebounce()
  const isEditCollectionNameDisabled = collectionCode === 'SAVED_PLACES' || collectionCode === 'SHORTCUTS'

  const collectionScreenNameForAnalytics = useMemo(() => {
    switch (collectionCode) {
      case 'SAVED_PLACES':
        return analyticEvents.SavedSingleCollectionV2Events.screen_names.SAVED_PLACES
      case 'SHORTCUTS':
        return analyticEvents.SavedSingleCollectionV2Events.screen_names.SHORTCUTS
      default:
        return analyticEvents.SavedSingleCollectionV2Events.screen_names.CUSTOM
    }
  }, [collectionCode])

  const ScreenEvents = useMemo(() => {
    if (collectionCode === 'SAVED_PLACES') {
      return analyticEvents.SavedCollectionSavedPlacesEvents
    } else {
      return analyticEvents.SavedCollectionCustomEvents
    }
  }, [collectionCode])

  const [parentViewHeight, setParentViewHeight] = useState(Dimensions.get('window').height)

  const onDelete = useCallback(() => {
    CollectionService.deleteCollectionFromAPI(collectionId).then(async () => {
      console.log('1')
      await CollectionService.getCollectionDataFromAPI()
      onBack()
    })
  }, [collectionId])

  const showAlertDeleteCollection = useCallback(
    (message: string, type: string) => {
      AlertService.alert('', message, [
        {
          text: 'Delete',
          onPress: () => {
            sendAnalyticsEventsToNative(
              analyticEvents.user_click,
              type === 'delete'
                ? analyticEvents.SavedCollectionEvents.user_click.pop_deleted_by_owner_delete
                : analyticEvents.SavedCollectionEvents.user_click.pop_unshared_by_owner_delete,
              analyticEvents.SavedCollectionEvents.screen_name,
            )
            onDelete()
          },
        },
      ])
    },
    [onDelete],
  )

  const onBookDeleteBookmark = useCallback(
    (bookmarkId: number) => {
      AlertService.alert('', 'Are you sure you want to delete this location?', [
        {
          text: 'Cancel',
          style: 'cancel',
        },
        {
          text: 'Delete',
          style: 'destructive',
          onPress: () => {
            CollectionService.deleteDataBookMarkFromAPI(bookmarkId).then(() => {
              NavigationDataService.showCustomToast({
                message: `Removed from ${collectionName}`,
                icon: Images.icRemove,
              })
              fetchDataFromAPI()
            })
          },
        },
      ])
    },
    [collectionName],
  )

  const fetchDataFromAPI = useCallback(
    (savedBookmarkId?: number) => {
      CollectionService.getCollectionBookmarksById(collectionId)
        .then(data => {
          const newData = data.map((s: BookmarkLocation) => ({
            ...s,
            isSelected: savedBookmarkId === s.bookmarkId ? true : false,
          }))
          setDataBookMark(newData)
          ShortcutDataService.fetchShortcutsList()
        })
        .catch(errorResponse => {
          if (errorResponse?.error_code === 'COLLECTION_DELETED') {
            showAlertDeleteCollection('Oops, this map collection has been deleted.', 'delete')
            return
          }
          if (errorResponse?.error_code === 'STOPPED_SHARING') {
            showAlertDeleteCollection('The sender has stopped sharing the map.', 'unshare')
            return
          }
        })
    },
    [collectionId, showAlertDeleteCollection],
  )

  useEffect(() => {
    sendAnalyticsEventsScreenNameToNative(ScreenEvents.screen_name)
    fetchDataFromAPI()
  }, [ScreenEvents.screen_name, fetchDataFromAPI])

  useEffect(() => {
    CollectionService.sendCollectionData(hybridScreens.CollectionDetails, dataBookMark)
    if (dataBookMark.length === 0 && collectionCode !== 'SHORTCUTS') {
      setBottomSheetIndex(BottomSheetIndexesEnum.MIN)
      bottomSheetRef.current?.snapToIndex(BottomSheetIndexesEnum.MIN)
    }
  }, [dataBookMark, collectionCode])

  // clear save pending state on opening component
  useEffect(() => {
    if (CollectionService.retainSavePendingOnInitialize) {
      CollectionService.setRetainSavePendingOnInitialize(false)
      return
    }
    CollectionService.setSavePendingItem(null)
  }, [])

  const onBack = () => {
    CollectionService.setRetainSavePendingOnInitialize(false)
    console.log('2')
    CollectionService.getCollectionDataFromAPI()
    sendAnalyticsEventsToNative(analyticEvents.user_click, ScreenEvents.user_click.back, ScreenEvents.screen_name)
    CollectionService.closeCollectionDetail()
  }

  const handleSearchClick = async () => {
    if (!isPinDropped) await fetchDataFromAPI()
    if (dataBookMark.length >= PreferencesSettingsService.bookmarksLimitSetting) {
      showBookmarksLimitedWarning()
      return
    }
    sendAnalyticsEventsToNative(
      analyticEvents.user_click,
      analyticEvents.SavedSingleCollectionV2Events.user_click.search,
      collectionScreenNameForAnalytics,
    )
    navigation.navigate('CollectionSearch', { onCallBack: onSearchCallback, collectionId })
  }

  const onSearchCallback = (data: SearchLocationModel) => {
    setVisibleModalCallBack(true)
    setDataEdit({
      bookmarkId: -1,
      lat: '',
      long: '',
      code: 'CUSTOM',
      address1: '',
      address2: '',
      isDestination: false,
      collectionId: -1,
      description: '',
      fullAddress: '',
      userLocationLinkId: undefined,
      userLocationLinkIdRef: null,
      placeId: undefined,
    })
    const convertedBookmarkModel = searchLocationToBookmarkLocation(data)
    if (convertedBookmarkModel) {
      dataCallBack = {
        ...convertedBookmarkModel,
        collectionId: collectionId,
      }
    }
  }

  const showBookmarksLimitedWarning = useCallback(() => {
    AlertService.alert(
      '',
      `You have reached the maximum of ${PreferencesSettingsService.bookmarksLimitSetting} saved places.`,
      [
        {
          text: 'Got it',
        },
      ],
    )
  }, [])

  const onSelectBookmarkFromMap = useCallback((bookmarkId: number, isSelected: boolean) => {
    setDataBookMark(bookmarks => {
      const newData = bookmarks.map(item => ({
        ...item,
        isSelected: bookmarkId === item.bookmarkId ? isSelected : false,
      }))
      CollectionService.sendCollectionData(hybridScreens.CollectionDetails, newData)
      return newData
    })
  }, [])

  useShareBusinessLogic(
    useCallback(
      ({ functionName, data, requestId }) => {
        switch (functionName) {
          case ShareBusinessLogic.constants.SELECT_SAVED_LOCATION:
            onSelectBookmarkFromMap(data.bookmark_id, data.is_selected)
            return null
          case ShareBusinessLogic.constants.TOGGLE_SAVE_LOCATION_COLLECTION_DETAILS:
            ShareBusinessLogic.processRequest(requestId, () => {
              const action: SaveActionsFromNative = data.action
              if (action === 'ADD') {
                handleBookmarkSave(isPinDropped ? data.bookmark : undefined)
              } else if (action === 'DELETE') {
                onBookDeleteBookmark(data.bookmarkId)
              }
              return null
            })
            break
          case ShareBusinessLogic.constants.TOGGLE_DROP_PIN_COLLECTION_DETAILS:
            ShareBusinessLogic.processRequest(requestId, () => {
              if (data.isDropPin) {
                setIsPinDropped(true)
                CollectionService.setSavePendingItem(data.data)
              } else {
                setIsPinDropped(false)
                CollectionService.setSavePendingItem(null)
              }
              return null
            })
            break
        }
      },
      [onSelectBookmarkFromMap, dataBookMark],
    ),
  )

  const clearDataCallBack = () => {
    dataCallBack = {
      bookmarkId: 0,
      lat: '',
      long: '',
      code: 'HOME',
      address1: '',
      address2: '',
      isDestination: false,
      collectionId: 0,
      description: '',
    }
    setVisibleModalCallBack(!visibleModalCallback)
  }

  const onSelect = (bookmarkId: number) => {
    NavigationDataService.sendAnalyticsEventsToNative(
      analyticEvents.user_click,
      ScreenEvents.user_click.saved_select,
      ScreenEvents.screen_name,
    )
    bottomSheetRef.current?.snapToIndex(1)
    setDataBookMark(
      dataBookMark.map(item => {
        return {
          ...item,
          isSelected: item.bookmarkId === bookmarkId ? (item.isSelected === true ? false : true) : false,
        }
      }),
    )
  }

  useEffect(() => {
    const isCheckSelected = dataBookMark.find(s => s.bookmarkId === route.params?.bookmarkId)?.isSelected
    if (route.params?.bookmarkId && isCheckSelected !== undefined && !isCheckSelected)
      setDataBookMark(
        dataBookMark.map(item => {
          return {
            ...item,
            isSelected: item.bookmarkId === route.params?.bookmarkId ? true : false,
          }
        }),
      )
  }, [route.params?.bookmarkId, dataBookMark])

  const onSelectEdit = (bookmarkId: number) => {
    bottomSheetRef.current?.snapToIndex(1)
    setDataBookMark(
      dataBookMark.map(item => {
        return {
          ...item,
          isSelected: item.bookmarkId === bookmarkId ? true : false,
        }
      }),
    )
  }

  const isInSavePendingState = useMemo(() => {
    return !!CollectionService.savePendingItem
  }, [CollectionService.savePendingItem])

  const onEditBookMark = (item: BookmarkLocation) => {
    dataCallBack.lat = '-0'
    setVisibleModalCallBack(!visibleModalCallback)
    setDataEdit(item)
  }

  const handleChangeBottomSheetIndex = (index: BottomSheetIndexesEnum) => {
    if (index < bottomSheetIndex) {
      sendAnalyticsEventsToNative(
        analyticEvents.user_click,
        ScreenEvents.user_swipe.swipe_down,
        ScreenEvents.screen_name,
      )
    } else if (index > bottomSheetIndex) {
      sendAnalyticsEventsToNative(analyticEvents.user_click, ScreenEvents.user_swipe.swipe_up, ScreenEvents.screen_name)
    }
    setBottomSheetIndex(index)
  }

  useEffect(() => {
    if (bottomSheetIndex < 2 && !isInSavePendingState) {
      const timeout = setTimeout(() => {
        const snapPointRatio = parseInt(snapPoints[bottomSheetIndex].slice(0, -1), 10) / 100
        const recenterHeight = parentViewHeight * snapPointRatio
        NavigationDataService.adjustRecenterNative(recenterHeight, bottomSheetIndex, hybridScreens.CollectionDetails)
      }, 400)
      return () => clearTimeout(timeout)
    }
  }, [bottomSheetIndex, isInSavePendingState])

  const ownerName = useLocalUserData().userName
  const handleShareCollection = () => {
    debounce(() => {
      CollectionService.handleCollectionShare(collectionId, ownerName)
    })
  }

  const handleBookmarkSave = (data?: BookmarkLocation) => {
    if (dataBookMark.length >= PreferencesSettingsService.bookmarksLimitSetting) {
      showBookmarksLimitedWarning()
      return
    }
    const item = CollectionService.savePendingItem
    if (!data && !item) return

    let dataSubmit: any = data
    if (!data && item)
      dataSubmit = {
        lat: item.lat,
        long: item.long,
        name: item.name || item.address1,
        address1: item.address1,
        address2: item.address2,
        fullAddress: item.fullAddress,
        code: collectionId === CollectionService.shortcutCollectionId ? 'NORMAL' : 'CUSTOM',
        //note: description available only when saving dropped pin from LocationInviteModal
        ...(!!item.description && { description: item.description }),
        ...(!!item.placeId && { placeId: item.placeId }),
        ...(!!item.userLocationLinkIdRef && { userLocationLinkIdRef: item.userLocationLinkIdRef }),
      }
    dataSubmit = { ...dataSubmit, collectionId: collectionId, isDestination: false, code: dataSubmit.code || 'CUSTOM' }

    CollectionService.createDataBookMarkFromAPI(dataSubmit).then(res => {
      if (LocationInviteService.shareLocationData) {
        LocationInviteService.setShareLocationData({
          ...LocationInviteService.shareLocationData,
          bookmarkId: res.bookmarkId,
          ...(!!item?.description && { description: item.description }),
        })
        NavigationDataService.showCustomToast({ message: `Saved to ${collectionName}` })
      } else {
        NavigationDataService.showCustomToast({ message: 'Address saved' })
      }
      if (!!CollectionService.savePendingItem && !isPinDropped) {
        CollectionService.onCollectionDetailSearchClear()
      }
      CollectionService.setSavePendingItem(null)
      fetchDataFromAPI(res.bookmarkId)
    })
  }

  const handleClearSearch = () => {
    sendAnalyticsEventsToNative(
      analyticEvents.user_click,
      analyticEvents.SavedSingleCollectionV2Events.user_click.search_bar_textbox_clear,
      collectionScreenNameForAnalytics,
    )
    CollectionService.onCollectionDetailSearchClear()
  }

  const renderSaveSection = () => {
    return (
      <View style={[styles.saveBtnCnt]}>
        <Pressable
          style={[styles.btnSearchAdd, { backgroundColor: themeColors.dayTabBGColor }]}
          onPress={handleSearchClick}>
          <Image source={Images.icSearchGray} />
          <AppText style={[styles.txtSearchAdd, { color: themeColors.greyText }]}>
            {isPinDropped ? 'Search for a location to save' : CollectionService.savePendingItem?.address1}
          </AppText>
          {!isPinDropped && (
            <Pressable style={styles.clearBtnCnt} onPress={handleClearSearch}>
              <Image source={Images.clearTextIcon} style={styles.buttonClearIcon} />
            </Pressable>
          )}
        </Pressable>
        <View style={[styles.saveLocationCnt, { backgroundColor: themeColors.dayTabBGColor }]}>
          <Pressable
            style={[styles.saveLocationBtn, { backgroundColor: themeColors.primaryColor }]}
            onPress={() => {
              sendAnalyticsEventsToNative(
                analyticEvents.user_click,
                analyticEvents.SavedSingleCollectionV2Events.user_click.save,
                collectionScreenNameForAnalytics,
              )
              handleBookmarkSave()
            }}>
            <AppText style={styles.saveLocationTxt}>Save</AppText>
          </Pressable>
        </View>
      </View>
    )
  }

  const toggleCollectionEditModal = (toggle = false) => {
    setShowCollectionEditModal(toggle)
  }

  useEffect(() => {
    if (isInSavePendingState) {
      NavigationDataService.adjustRecenterNative(180, 0, hybridScreens.CollectionDetails)
    }
  }, [isInSavePendingState])

  return (
    <>
      {!visibleModalCallback && (
        <SafeAreaView
          style={{ flex: Platform.OS === 'android' ? 0.001 : 0, backgroundColor: themeColors.primaryBackground }}
        />
      )}
      <View
        style={styles.container}
        pointerEvents="box-none"
        onLayout={event => {
          setParentViewHeight(event.nativeEvent.layout.height)
        }}>
        {!visibleModalCallback && (
          <ScreenHeader
            title={collectionName}
            description={collectionDescription}
            actionBtnRight={handleShareCollection}
            onBack={onBack}
            textBtnRight="Share"
            styleBtnRight={{ color: themeColors.primaryColor }}
            // note: disable this feature for this release, since some flows are not mapped out
            // onTitleClick={!isEditCollectionNameDisabled ? () => toggleCollectionEditModal(true) : undefined}
          />
        )}
        {isInSavePendingState ? (
          renderSaveSection()
        ) : (
          <BottomSheet
            ref={bottomSheetRef}
            index={1}
            backdropComponent={null}
            backgroundComponent={null}
            handleComponent={null}
            animateOnMount={false}
            snapPoints={snapPoints}
            onChange={handleChangeBottomSheetIndex}
            enableOverDrag={false}>
            <BottomSheetView
              style={{
                flex: 1,
              }}>
              <Pressable
                style={[styles.btnSearchAdd, { backgroundColor: themeColors.dayTabBGColor }]}
                onPress={handleSearchClick}>
                <Image source={Images.icSearchGray} style={{ tintColor: themeColors.greyText }} />
                <AppText style={[styles.txtSearchAdd, { color: themeColors.greyText }]}>
                  Search for a location to save
                </AppText>
              </Pressable>
              <View style={[styles.modal, { backgroundColor: themeColors.primaryBackground }]}>
                <View style={styles.drag} />
                <ListBookMark
                  screenName={collectionScreenNameForAnalytics}
                  dataBookMark={dataBookMark}
                  onSelect={onSelect}
                  onSelectEdit={onSelectEdit}
                  fetchDataFromAPI={fetchDataFromAPI}
                  onEditBookMark={onEditBookMark}
                  setSnapPoints={setSnapPoints}
                />
              </View>
            </BottomSheetView>
          </BottomSheet>
        )}
        {visibleModalCallback && (
          <AddBookMarkModal
            screenEvents={ScreenEvents}
            bookmarkData={dataBookMark}
            data={dataCallBack}
            isVisible={visibleModalCallback}
            onClose={clearDataCallBack}
            collectionId={collectionId}
            collectionName={collectionName}
            fetchDataFromAPI={fetchDataFromAPI}
            dataEdit={dataEdit}
            isEditMode={true}
          />
        )}
        <AddNewModal
          listData={CollectionService.collections}
          themeColors={themeColors}
          isVisible={showCollectionEditModal}
          onClose={() => toggleCollectionEditModal(false)}
          checkEdit={collectionId}
          dataEdit={{
            description: collectionDescription,
            name: collectionName,
          }}
        />
      </View>
    </>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'transparent',
  },
  modal: {
    flex: 1,
    height: '100%',
    borderTopRightRadius: 16,
    borderTopLeftRadius: 16,
  },
  drag: {
    backgroundColor: '#BCBFC4',
    width: 43,
    height: 4,
    borderRadius: 6,
    opacity: 0.69,
    marginTop: 8,
    alignSelf: 'center',
    marginBottom: 10,
  },
  txtFoodMap: {
    textAlign: 'center',
    fontWeight: '500',
    fontSize: 18,
    color: '#222638',
  },
  txtAddNew: {
    fontWeight: '500',
    fontSize: 16,
  },
  listCollection: {
    flexDirection: 'row',
    paddingHorizontal: 24,
    paddingVertical: 27,
    alignItems: 'center',
  },
  imgBookMark: {
    width: 21,
    height: 28,
  },
  wrapTitle: {
    marginLeft: 25,
  },
  txtTitle: {
    color: '#222638',
    fontWeight: '500',
    fontSize: 20,
    lineHeight: 24,
  },
  txtDes: {
    color: '#778188',
    fontWeight: '400',
    fontSize: 14,
    lineHeight: 17,
    marginTop: 2,
  },
  shareFromTxt: {
    fontSize: 14,
    fontWeight: '300',
    color: '#778188',
    marginLeft: 10,
  },
  btnSearchAdd: {
    borderRadius: 29,
    borderWidth: 1,
    borderColor: '#E2E2E2',
    backgroundColor: 'white',
    flexDirection: 'row',
    alignItems: 'center',
    width: '90%',
    alignSelf: 'center',
    marginBottom: 13,
    paddingVertical: 10,
    paddingHorizontal: 8,
  },
  txtSearchAdd: {
    fontWeight: '400',
    fontSize: 17,
    color: '#778188',
    maxWidth: '85%',
    borderWidth: 1,
    borderColor: 'transparent',
  },
  saveBtnCnt: {
    position: 'absolute',
    width: '100%',
    bottom: 0,
  },
  saveLocationCnt: {
    alignContent: 'center',
    paddingHorizontal: 24,
    paddingTop: 22,
    paddingBottom: 36,
    backgroundColor: '#FFF',
    borderTopLeftRadius: 16,
    borderTopRightRadius: 16,
  },
  saveLocationBtn: {
    alignContent: 'center',
    backgroundColor: '#782EB1',
    borderRadius: 28,
    paddingVertical: 8,
    paddingHorizontal: 26,
  },
  saveLocationTxt: {
    fontSize: 20,
    fontWeight: '700',
    color: '#FFF',
    textAlign: 'center',
  },
  buttonClearIcon: { width: 16, height: 16 },
  clearBtnCnt: { marginLeft: 'auto', marginRight: 10 },
})

export default observer(CollectionDetailsScreen)

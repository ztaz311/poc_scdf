import React, { useCallback, useEffect, useRef, useState } from 'react'
import { Animated, Image, Pressable, StyleSheet, TextInput, TouchableOpacity, View } from 'react-native'
import { AppText } from '../../../../../components'
import ScreenHeader from '../../../../../components/ScreenHeader'
import { analyticEvents } from '../../../../../constants/analyticEvents'
import { Colors } from '../../../../../constants/appColors'
import { Images } from '../../../../../constants/appImages'
import { useTheme } from '../../../../../contexts/ThemeContext'
import { BookmarkLocation } from '../../../../../models/collection.model'
import AlertService from '../../../../../services/alert.service'
import CollectionService from '../../../../../services/collection.service'
import NavigationDataService from '../../../../../services/navigation-data.service'
const { sendAnalyticsEventsToNative, sendAnalyticsPopupEvent } = NavigationDataService

export const BOOKMARK_DESC_MAX_CHARACTER_COUNT = 200

type Props = {
  data: BookmarkLocation
  isVisible: boolean
  onClose: () => void
  collectionId: number
  collectionName: string
  fetchDataFromAPI: () => void
  dataEdit: BookmarkLocation
  bookmarkData: BookmarkLocation[]
  screenEvents: any
  isEditMode?: boolean
}
const AddBookMarkModal: React.FC<Props> = ({
  isVisible,
  onClose,
  data,
  collectionId,
  fetchDataFromAPI,
  dataEdit,
  screenEvents,
  isEditMode,
  collectionName,
}) => {
  const { themeColors } = useTheme()
  const [nameCollection, setNameCollection] = useState('')
  const [optional, setOptional] = useState('')
  const inputNameRef = useRef<any>()
  const [errorMsg, setErrorMsg] = useState('')
  const [initialSaveDisabled, setInitialSaveDisabled] = useState(true)

  const onChangeNameCollection = (value: string) => {
    setInitialSaveDisabled(false)
    NavigationDataService.sendAnalyticsEventsToNative(
      analyticEvents.user_edit,
      screenEvents.user_edit.edit_location_name,
      screenEvents.screen_name,
    )
    setNameCollection(value)
    setErrorMsg('')
  }

  useEffect(() => {
    if (!isVisible) {
      setInitialSaveDisabled(true)
    }
  }, [isVisible])

  const onChangeOption = (value: string) => {
    setInitialSaveDisabled(false)
    NavigationDataService.sendAnalyticsEventsToNative(
      analyticEvents.user_click,
      screenEvents.user_edit.edit_location_description,
      screenEvents.screen_name,
    )
    setOptional(value)
  }

  const onClearNameCollection = () => {
    setInitialSaveDisabled(false)
    NavigationDataService.sendAnalyticsEventsToNative(
      analyticEvents.user_click,
      screenEvents.user_click.edit_name_clear,
      screenEvents.screen_name,
    )
    setNameCollection('')
  }

  useEffect(() => {
    if (data.lat === '-0') {
      // -0 is edit
      setNameCollection(dataEdit.name || '')
      setOptional(dataEdit.description)
    } else {
      setNameCollection(data.address1)
      setOptional(data.description)
      setErrorMsg('')
    }
  }, [data, dataEdit])

  const onSubmit = () => {
    if (initialSaveDisabled) return
    if (nameCollection.trim() !== '') {
      if (data.lat === '-0') {
        // -0 is edit
        CollectionService.editDataBookMarkFromAPI(dataEdit.bookmarkId, {
          ...dataEdit,
          name: nameCollection,
          description: optional,
        }).then(() => {
          onClose()
          fetchDataFromAPI()
          NavigationDataService.showCustomToast({ message: 'Saved edits' })
        })
        return
      }

      // Add new
      const dataSubmit: BookmarkLocation = {
        lat: data.lat,
        long: data.long,
        name: nameCollection,
        description: optional,
        address1: data.address1,
        address2: data.address2,
        collectionId: collectionId,
        isDestination: false,
        code: 'CUSTOM',
        bookmarkId: -1,
        fullAddress: data.fullAddress || '',
      }
      CollectionService.createDataBookMarkFromAPI(dataSubmit).then(() => {
        onClose()
        fetchDataFromAPI()
      })
    } else {
      setErrorMsg('Name of location is required.')
    }
  }

  const openShowKeyboard = () => {
    inputNameRef.current.focus()
  }

  const onConfirmDelete = useCallback(() => {
    sendAnalyticsPopupEvent(screenEvents.screen_name, screenEvents.popup_name, screenEvents.user_popup.popup_open)
    AlertService.alert('', 'Are you sure you want to delete this location?', [
      {
        text: 'Cancel',
        onPress: () => {
          sendAnalyticsEventsToNative(
            analyticEvents.user_click,
            screenEvents.user_click.delete_cancel,
            screenEvents.screen_name,
          )
        },
        style: 'cancel',
      },
      {
        text: 'Delete',
        style: 'destructive',
        onPress: () => {
          sendAnalyticsEventsToNative(
            analyticEvents.user_click,
            screenEvents.user_click.delete_confirm,
            screenEvents.screen_name,
          )
          CollectionService.deleteDataBookMarkFromAPI(dataEdit.bookmarkId).then(() => {
            NavigationDataService.showCustomToast({
              message: `Removed from ${collectionName}`,
              icon: Images.icRemove,
            })
            fetchDataFromAPI()
          })
          onClose()
        },
      },
    ])
  }, [dataEdit.bookmarkId, fetchDataFromAPI, onClose, screenEvents])

  return isVisible ? (
    <View pointerEvents="box-none" style={{ flex: 1 }}>
      <View style={styles.wrapperField} />
      <Animated.View
        style={[
          styles.bottom,
          {
            backgroundColor: themeColors.primaryBackground,
          },
        ]}>
        <View style={styles.boxHeader}>
          <ScreenHeader
            title={isEditMode ? 'Edit  Location' : 'New Location'}
            actionBtnRight={onClose}
            textBtnRight="Close"
            isSmall
            showShadow={false}
            isRounded={true}
            hasBackButton={false}
          />
        </View>
        <View style={styles.wrapper}>
          <View style={styles.txtOptions}>
            <AppText style={{ fontSize: 14, fontWeight: '600', color: themeColors.primaryText }}>Location name</AppText>
          </View>
          <TextInput
            onLayout={openShowKeyboard}
            ref={inputNameRef}
            style={[
              styles.inputText,
              styles.inputNameStyle,
              {
                backgroundColor: themeColors.inputModal,
                color: themeColors.primaryText,
                borderWidth: errorMsg === '' ? 0 : 1,
                borderColor: errorMsg === '' ? 'transparent' : '#E82370',
              },
            ]}
            blurOnSubmit={false}
            autoCapitalize="none"
            autoCorrect={false}
            maxLength={nameCollection.length > 20 ? undefined : 20}
            keyboardType="default"
            returnKeyType="default"
            onChangeText={onChangeNameCollection}
            value={nameCollection}
            allowFontScaling={false}
          />
          {nameCollection !== '' ? (
            <TouchableOpacity style={styles.clearTextIcon} onPress={onClearNameCollection}>
              <Image source={Images.clearTextIcon} style={styles.imgClearIcon} resizeMode="contain" />
            </TouchableOpacity>
          ) : null}
          <TouchableOpacity style={styles.clearTextIcon} onPress={onClearNameCollection}>
            <Image source={Images.clearTextIcon} style={styles.imgClearIcon} resizeMode="contain" />
          </TouchableOpacity>
        </View>
        <AppText style={styles.errorMsg}>{errorMsg}</AppText>
        <View style={styles.wrapper}>
          <View style={styles.txtOptions}>
            <AppText style={{ fontSize: 14, fontWeight: '600', color: themeColors.primaryText }}>
              A brief description (Optional)
            </AppText>
          </View>
          <TextInput
            multiline={true}
            style={[
              styles.inputText,
              {
                backgroundColor: themeColors.inputModal,
                color: themeColors.primaryText,
              },
            ]}
            blurOnSubmit={false}
            returnKeyType="default"
            maxLength={BOOKMARK_DESC_MAX_CHARACTER_COUNT}
            onChangeText={onChangeOption}
            value={optional}
            allowFontScaling={false}
          />
        </View>
        <View style={styles.actionContainer}>
          {isEditMode && (
            <Pressable style={[styles.actionBtn, { borderColor: themeColors.redPink }]} onPress={onConfirmDelete}>
              <AppText style={[styles.actionBtnText, { color: themeColors.redPink }]}>Delete</AppText>
            </Pressable>
          )}
          <Pressable
            style={[
              styles.actionBtn,
              {
                borderColor: initialSaveDisabled ? '#BCBFC3' : themeColors.primaryColor,
                backgroundColor: initialSaveDisabled ? '#BCBFC3' : themeColors.primaryColor,
                width: isEditMode ? styles.actionBtn.width : '100%',
              },
            ]}
            onPress={onSubmit}>
            <AppText style={[styles.actionBtnText, { color: themeColors.primaryBackground }]}>Save</AppText>
          </Pressable>
        </View>
      </Animated.View>
    </View>
  ) : null
}

const styles = StyleSheet.create({
  bottom: {
    height: '96.5%',
    width: '100%',
    position: 'absolute',
    bottom: 0,
    borderTopStartRadius: 16,
    borderTopEndRadius: 16,
  },
  item: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 64,
    borderBottomColor: '#DDDDDD',
    borderBottomWidth: 1,
    backgroundColor: Colors.white,
  },
  wrapper: {
    marginHorizontal: 24,
  },
  inputNameStyle: {
    borderRadius: 29,
    paddingRight: 40,
  },
  inputText: {
    minHeight: 48,
    borderRadius: 8,
    paddingHorizontal: 22,
    fontWeight: '400',
    fontSize: 18,
  },
  clearTextIcon: {
    position: 'absolute',
    right: 8,
    top: 42,
    justifyContent: 'center',
    alignItems: 'center',
    width: 25,
    height: 25,
  },
  imgClearIcon: {
    width: 16,
    height: 16,
  },
  txtOptions: {
    marginBottom: 12,
  },
  wrapperField: {
    backgroundColor: '#000',
    opacity: 0.2,
    height: '100%',
    top: -50,
  },
  errorMsg: {
    color: '#E82370',
    fontWeight: '500',
    marginVertical: 5,
    marginLeft: 46,
  },
  boxHeader: {
    marginBottom: 15,
    borderBottomColor: '#E2E2E2',
    borderBottomWidth: 1,
  },
  actionContainer: {
    flexDirection: 'row',
    paddingHorizontal: 24,
    marginVertical: 30,
    width: '100%',
    justifyContent: 'space-between',
  },
  actionBtn: {
    borderRadius: 28,
    borderWidth: 1,
    height: 48,
    justifyContent: 'center',
    alignItems: 'center',
    width: '45%',
  },
  actionBtnText: {
    fontSize: 20,
  },
})

export default AddBookMarkModal

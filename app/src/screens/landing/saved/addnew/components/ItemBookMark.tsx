import ReadMore from '@fawazahmed/react-native-read-more'
import { useNavigation } from '@react-navigation/native'
import React, { forwardRef, ForwardRefRenderFunction, useCallback } from 'react'
import { Image, Pressable, StyleSheet, TouchableOpacity, View } from 'react-native'
import { Swipeable } from 'react-native-gesture-handler'
import { AppText } from '../../../../../components'
import { analyticEvents } from '../../../../../constants/analyticEvents'
import { Images } from '../../../../../constants/appImages'
import { reactScreens } from '../../../../../constants/screensName'
import { useTheme } from '../../../../../contexts/ThemeContext'
import { useDebounce } from '../../../../../hooks/useDebounce'
import { BookmarkLocation, ShortcutCode } from '../../../../../models/collection.model'
import AlertService from '../../../../../services/alert.service'
import CollectionService from '../../../../../services/collection.service'
import NavigationDataService from '../../../../../services/navigation-data.service'
import PreferencesSettingsService from '../../../../../services/preferences-settings.service'
import ShortcutDataService from '../../../../../services/shortcut-data.service'
import { BookmarkPlaceHolder, DerivedBookMarkListItem } from './ListBookMark'

type Props = {
  item: DerivedBookMarkListItem
  onSelect: (bookmarkId: number) => void
  fetchDataFromAPI: () => void
  onEditBookMark: (item: BookmarkLocation) => void
  screenName: string
  onLayout?: any
}

const ItemBookMark: ForwardRefRenderFunction<Swipeable, Props> = (
  { item: _item, onSelect, fetchDataFromAPI, onEditBookMark, screenName, onLayout },
  ref,
) => {
  const sendAnalyticsEventsToNative = NavigationDataService.sendAnalyticsEventsToNative
  const { themeColors, isDarkMode } = useTheme()
  const { debounce } = useDebounce()

  const sendAnalytics = useCallback((eventValue: string, eventType = analyticEvents.user_click) => {
    sendAnalyticsEventsToNative(eventType, eventValue, screenName)
  }, [])

  const bookMarkPlaceHolder = _item as BookmarkPlaceHolder
  const showBookmarksLimitedWarning = useCallback(() => {
    AlertService.alert(
      '',
      `You have reached the maximum of ${PreferencesSettingsService.bookmarksLimitSetting} saved places.`,
      [
        {
          text: 'Got it',
        },
      ],
    )
  }, [])
  const navigation = useNavigation()
  const handlePlaceHolderNavigation = useCallback(
    (code: ShortcutCode) => {
      if (ShortcutDataService.shortcutsData.length >= PreferencesSettingsService.bookmarksLimitSetting) {
        showBookmarksLimitedWarning()
        return
      }

      navigation?.navigate(reactScreens.ShortcutAddNew, { shortcutType: code })
    },
    [navigation, showBookmarksLimitedWarning],
  )

  if (bookMarkPlaceHolder.isPlaceHolder) {
    return (
      <TouchableOpacity
        key={bookMarkPlaceHolder.code}
        onPress={() => handlePlaceHolderNavigation(bookMarkPlaceHolder.code)}
        style={[
          styles.listCollection,
          { backgroundColor: themeColors.primaryBackground, borderBottomColor: themeColors.divider },
        ]}
        onLayout={event => onLayout(event.nativeEvent.layout.height)}>
        <View style={styles.itemCont}>
          <View style={styles.itemTitleCont}>
            <Image source={Images.add} style={[styles.imgPlaceHolder]} />
            <View style={styles.wrapTitle}>
              <AppText style={[styles.txtTitle, { color: themeColors.primaryColor, fontSize: 20, fontWeight: '700' }]}>
                {bookMarkPlaceHolder.code === 'HOME' ? 'Add Home' : 'Add Work'}
              </AppText>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    )
  }

  const item = _item as BookmarkLocation

  const handleShare = () => {
    debounce(() => {
      sendAnalytics(analyticEvents.SavedSingleCollectionV2Events.user_click.card_single_saved_share)
      CollectionService.handleCollectionLocationShare(item)
    })
  }

  const getIconImage = (bookmarkItem: DerivedBookMarkListItem) => {
    const isPlaceHolder = (bookmarkItem as BookmarkPlaceHolder).isPlaceHolder
    if (isPlaceHolder) return Images.add
    else if (bookmarkItem.code === 'HOME') return Images.icShortcutHome
    else if (bookmarkItem.code === 'WORK') return Images.icShortcutWork
    return Images.bookMark
  }
  const getIconImageStyles = (bookmarkItem: DerivedBookMarkListItem) => {
    const styleArray: any[] = [styles.imgBookMark]
    if (bookmarkItem.code === 'HOME') styleArray.push({ width: 25, height: 23 })
    else if (bookmarkItem.code === 'WORK') styleArray.push({ width: 21, height: 23 })
    else styleArray.push({ tintColor: isDarkMode ? '#E82370' : '#EA347A' })
    return styleArray
  }

  return (
    <TouchableOpacity
      key={item.bookmarkId}
      onPress={() => {
        onSelect(item.bookmarkId)
      }}
      style={[
        styles.listCollection,
        {
          backgroundColor: item.isSelected ? themeColors.selectedItem : themeColors.primaryBackground,
          borderBottomColor: themeColors.divider,
        },
      ]}
      onLayout={event => onLayout(event.nativeEvent.layout.height)}>
      <View style={styles.itemCont}>
        <View style={styles.itemTitleCont}>
          <Image source={getIconImage(item)} style={getIconImageStyles(item)} />
          <View style={styles.wrapTitle}>
            <AppText style={[styles.txtTitle, { color: themeColors.primaryText }]}>{item.name}</AppText>
            <AppText style={[styles.txtAddress, { color: themeColors.amenityText }]}>
              {item.fullAddress || item.address1}
            </AppText>
            {item.description && (
              <ReadMore
                // animate={false}
                numberOfLines={4}
                allowFontScaling={false}
                seeMoreText="Read more"
                seeLessText={<AppText style={styles.readMoreActionText}>{'\n'}Read less</AppText>}
                style={[styles.txtDes, { color: themeColors.amenityText }]}
                onCollapse={() =>
                  sendAnalytics(
                    analyticEvents.SavedSingleCollectionV2Events.user_toggle.read_less,
                    analyticEvents.user_toggle,
                  )
                }
                onExpand={() =>
                  sendAnalytics(
                    analyticEvents.SavedSingleCollectionV2Events.user_toggle.read_more,
                    analyticEvents.user_toggle,
                  )
                }
                seeMoreStyle={styles.readMoreActionText}>
                {item.description}
              </ReadMore>
            )}
          </View>
        </View>
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          <Pressable style={styles.shareButton} onPress={handleShare}>
            <Image
              source={Images.icSharePurple}
              resizeMode="contain"
              style={[styles.icShare, { tintColor: themeColors.primaryColor }]}
            />
          </Pressable>
          <Pressable
            style={styles.editButton}
            onPress={async () => {
              sendAnalytics(analyticEvents.SavedSingleCollectionV2Events.user_click.card_single_saved_edit)
              await fetchDataFromAPI()
              if (item.code !== 'HOME' && item.code !== 'WORK') {
                onEditBookMark(item)
              } else {
                navigation?.navigate(reactScreens.ShortcutAddNew, {
                  shortcutType: item.code,
                  bookmarkId: item.bookmarkId,
                  collectionId: item.collectionId,
                })
              }
              onSelect(item.bookmarkId)
            }}>
            <Image
              source={Images.icEditPencilPurple}
              resizeMode="contain"
              style={[styles.icEdit, { tintColor: themeColors.primaryColor }]}
            />
          </Pressable>
        </View>
      </View>
    </TouchableOpacity>
  )
  // </Swipeable>
}

const styles = StyleSheet.create({
  listCollection: {
    flexDirection: 'row',
    paddingHorizontal: 24,
    paddingVertical: 17,
    alignItems: 'center',
    borderBottomWidth: 1,
  },
  itemCont: { flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', flex: 1 },
  itemTitleCont: {
    flexDirection: 'row',
    alignItems: 'center',
    flex: 0.9,
  },
  imgPlaceHolder: {
    width: 25,
    height: 25,
  },
  imgBookMark: {
    width: 21,
    height: 28,
  },
  wrapTitle: {
    marginLeft: 25,
    flex: 1,
  },
  txtTitle: {
    fontWeight: '500',
    fontSize: 18,
    lineHeight: 24,
  },
  txtDes: {
    fontWeight: '400',
    fontSize: 14,
    lineHeight: 17,
    marginTop: 6,
    fontStyle: 'italic',
  },
  txtAddress: {
    fontWeight: '400',
    fontSize: 16,
    lineHeight: 18,
    marginTop: 2,
  },
  itemOptions: {
    flexDirection: 'row',
  },
  shareButton: {
    marginRight: 22,
  },
  editButton: {
    padding: 2,
  },
  icShare: {
    width: 20,
    height: 20,
  },
  icEdit: {
    width: 16,
    height: 16,
  },
  readMoreActionText: {
    color: '#782EB1',
    fontWeight: '400',
    fontSize: 14,
  },
})

export default forwardRef(ItemBookMark)

import { BottomSheetFlatList } from '@gorhom/bottom-sheet'
import React, { memo, useEffect, useMemo, useState } from 'react'
import { Dimensions, Platform, StyleSheet, View } from 'react-native'
import { AppText } from '../../../../../components'
import { useTheme } from '../../../../../contexts/ThemeContext'
import { BookmarkLocation } from '../../../../../models/collection.model'
import NavigationDataService from '../../../../../services/navigation-data.service'
import ItemBookMark from './ItemBookMark'

export type BookmarkPlaceHolder = Pick<BookmarkLocation, 'code'> & { isPlaceHolder: true }
export type DerivedBookMarkListItem = BookmarkLocation | BookmarkPlaceHolder

type Props = {
  dataBookMark: BookmarkLocation[]
  onSelect: (bookmarkId: number) => void
  fetchDataFromAPI: () => void
  onEditBookMark: (item: BookmarkLocation) => void
  onSelectEdit: (bookmarkId: number) => void
  setSnapPoints?: any
  screenName: string
}

const ListBookMark: React.FC<Props> = ({
  dataBookMark,
  onSelect,
  fetchDataFromAPI,
  onEditBookMark,
  screenName,
  setSnapPoints,
}) => {
  const [itemHeights, setItemHeights] = useState<number[]>([])
  const isShortcutScreen = useMemo(
    () => NavigationDataService.paramsFromNative?.code === 'SHORTCUTS',
    [NavigationDataService.paramsFromNative?.code],
  )
  const { themeColors } = useTheme()

  useEffect(() => {
    const heightArray = new Array(dataBookMark.length + (isShortcutScreen ? 2 : 0))
    heightArray.fill(0)
    setItemHeights(heightArray)
  }, [dataBookMark])

  const derivedBookMarkList: DerivedBookMarkListItem[] = useMemo(() => {
    if (!isShortcutScreen) return dataBookMark
    const newBookMarkList: DerivedBookMarkListItem[] = [...dataBookMark]
    const workIndex = newBookMarkList.findIndex(e => e.code === 'WORK')
    if (workIndex === -1) newBookMarkList.unshift({ code: 'WORK', isPlaceHolder: true })
    else {
      const workBookmark = newBookMarkList.splice(workIndex, 1)
      newBookMarkList.unshift(workBookmark[0])
    }
    const homeIndex = newBookMarkList.findIndex(e => e.code === 'HOME')
    if (homeIndex === -1) newBookMarkList.unshift({ code: 'HOME', isPlaceHolder: true })
    else {
      const homeBookmark = newBookMarkList.splice(homeIndex, 1)
      newBookMarkList.unshift(homeBookmark[0])
    }
    return newBookMarkList
  }, [dataBookMark, isShortcutScreen])

  const isCustomBookmarkAdded = useMemo(
    () => dataBookMark.some(e => e.code !== 'HOME' && e.code !== 'WORK'),
    [dataBookMark],
  )

  const isShowFooter = isShortcutScreen && !isCustomBookmarkAdded

  // use to calculate snapPointPercent
  useEffect(() => {
    if (itemHeights.includes(0)) {
      return
    }
    const fullWindowHeight = (Dimensions.get('window').height * 90) / 100 //exclude screen header
    const halfWindowHeight = fullWindowHeight / 2
    let isAboveHalf = false
    let heightSum = Platform.OS === 'ios' ? 80 : 60 // height seach bar
    for (const [index, height] of itemHeights.entries()) {
      heightSum += height
      if (heightSum > halfWindowHeight) {
        isAboveHalf = true
        heightSum -= height / 2
        break
      } else {
        if (index === itemHeights.length - 1 && dataBookMark.length > 0 && !isShowFooter) {
          const snap = ((heightSum + 30) / fullWindowHeight) * 100
          setSnapPoints((currentSnapPoints: any) => {
            currentSnapPoints[1] = `${isNaN(snap) ? 50 : snap}%`
            return [...currentSnapPoints]
          })
          return
        }
      }
    }
    if (!isAboveHalf) return

    const snapPointPercent = heightSum / fullWindowHeight
    if (snapPointPercent > 0.35 && snapPointPercent < 0.65 && heightSum > 0) {
      const snap = snapPointPercent * 100
      setSnapPoints((currentSnapPoints: any) => {
        currentSnapPoints[1] = `${isNaN(snap) ? 50 : snap}%`
        return [...currentSnapPoints]
      })
      return
    }
  }, [itemHeights])

  const renderFooter = () => {
    if (isShowFooter) {
      return (
        <View style={[styles.container, { marginTop: 25 }]}>
          <AppText style={[styles.txtAlert, { color: themeColors.primaryText2 }]}>
            To save other locations, you may search or drop a pin by tapping anywhere on the map.
          </AppText>
        </View>
      )
    }
    return null
  }

  if (derivedBookMarkList.length === 0) {
    return (
      <View style={styles.container}>
        <AppText style={[styles.txtAlert, { color: themeColors.primaryText2 }]}>
          Search for a location or drop a pin by tapping anywhere on the map to start adding locations.
        </AppText>
      </View>
    )
  }

  return (
    <BottomSheetFlatList
      data={derivedBookMarkList}
      keyExtractor={(item, index) => `${index}`}
      renderItem={({ item, index }) => (
        <ItemBookMark
          item={item}
          onSelect={onSelect}
          fetchDataFromAPI={fetchDataFromAPI}
          onEditBookMark={onEditBookMark}
          onLayout={(height: number) => {
            setItemHeights(iHeights => {
              iHeights[index] = height
              return [...iHeights]
            })
          }}
          screenName={screenName}
        />
      )}
      onEndReachedThreshold={0.1}
      ListFooterComponent={renderFooter}
    />
  )
}

const styles = StyleSheet.create({
  container: {
    marginTop: 15,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 28,
  },
  txtAlert: {
    color: '#222638',
    fontWeight: '400',
    fontSize: 17,
    textAlign: 'center',
  },
})

export default memo(ListBookMark)

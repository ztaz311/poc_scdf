import { RouteProp } from '@react-navigation/native'
import { StackNavigationProp } from '@react-navigation/stack'
import React, { useCallback, useEffect, useMemo, useState } from 'react'
import {
  FlatList,
  Keyboard,
  KeyboardEventName,
  ListRenderItem,
  Platform,
  StyleSheet,
  TouchableWithoutFeedback,
  View,
} from 'react-native'
import { SafeAreaView } from 'react-native-safe-area-context'
import { LocationSearchItemComponent, SearchInputComponent } from '../../../../components'
import KeyboardSpacerInsertBottom from '../../../../components/KeyboardSpacerInsertBottom'
import ScreenHeader from '../../../../components/ScreenHeader'
import { useTheme } from '../../../../contexts/ThemeContext'
import { useBackHandler } from '../../../../hooks/useBackHandler'
import { useDebounce } from '../../../../hooks/useDebounce'
import { useNativeEvents } from '../../../../hooks/useNativeEvents'
import { RootStackParamList } from '../../../../models/root-stack-param-list'
import { SearchLocationModel } from '../../../../models/search-location.model'
import CollectionService from '../../../../services/collection.service'
import { ICancelerCall } from '../../../../services/http-client.service'
import LocationService from '../../../../services/location.service'
import NavigationDataService from '../../../../services/navigation-data.service'
import RecentLocationService from '../../../../services/recent-location.service'
import SearchLocationService from '../../../../services/search-location.service'
import { deepClone } from '../../../../utils/functions'
import EmptyWarning from '../../../search-location/components/EmptyWarning'
type Props = {
  route: RouteProp<RootStackParamList, 'CollectionSearch'>
  navigation: StackNavigationProp<RootStackParamList, 'CollectionSearch'>
}
const delayForTyping = 250

const CollectionSearchScreen: React.FC<Props> = ({ route, navigation }) => {
  const { themeColors, themeImages } = useTheme()
  const [searchInput, setSearchInput] = useState('')
  const [searchResult, setSearchResult] = useState<SearchLocationModel[]>([])
  const [isSearching, setIsSearching] = useState(false)
  const [keyboardShown, setKeyboardShown] = useState(false)
  const { debounce } = useDebounce()
  /**
   * Handle when search input change
   */
  useEffect(() => {
    /**
     * Empty input
     */
    if (!searchInput.trim()) {
      setSearchResult([])
      return
    }

    setIsSearching(true)
    const canceler: ICancelerCall = {}

    // Delay sometime, wait for user finish input everything
    // This will improve app performance

    const timeoutTyping = setTimeout(() => {
      const search = async () => {
        const dataApi = await SearchLocationService.searchLocation(searchInput, { canceler })
        setSearchResult(dataApi)
        setIsSearching(false)
      }
      search()
    }, delayForTyping)

    return () => {
      setIsSearching(false)
      if (timeoutTyping) {
        clearTimeout(timeoutTyping)
      }
      canceler.call?.()
    }
  }, [searchInput])

  /**
   * Handle keyboard show/hide
   */
  useEffect(() => {
    const onKeyboardShown = () => {
      setKeyboardShown(true)
    }
    const onKeyboardHidden = () => {
      setKeyboardShown(false)
    }

    const showEvent: KeyboardEventName = Platform.OS === 'android' ? 'keyboardDidShow' : 'keyboardWillShow'
    const hideEvent: KeyboardEventName = Platform.OS === 'android' ? 'keyboardDidHide' : 'keyboardWillHide'

    const listeners = [
      Keyboard.addListener(showEvent, onKeyboardShown),
      Keyboard.addListener(hideEvent, onKeyboardHidden),
    ]
    return () => {
      listeners.forEach(item => item.remove())
    }
  }, [])

  /**
   * Request permission + Track location
   */
  useEffect(() => {
    const trackLocation = async () => {
      const granted = await LocationService.requestPermission()
      if (granted) {
        LocationService.retrieveCurrentUserLocation()
      }
    }
    trackLocation()

    return () => {
      // LocationService.stopWatchPosition()
    }
  }, [])

  const onShortcutListChangedEvent = useCallback(() => {
    navigation.goBack()
  }, [navigation])
  /**
   * Listen to event when fav list change in native => back to fav list
   */
  useNativeEvents('favouriteListChange', onShortcutListChangedEvent)

  const onBackPress = useCallback(() => {
    if (NavigationDataService.isCallFromNative(route)) {
      NavigationDataService.backToNative()
    } else {
      navigation.goBack()
    }
    return true
  }, [navigation, route])

  useBackHandler(onBackPress)

  const onSearchInputChange = (text: string) => {
    setSearchInput(text)
  }

  // const showDuplicatedBookmarkLocationWarning = useCallback(() => {
  //   AlertService.alert('', 'This location has already been saved.', [
  //     {
  //       text: 'Got it',
  //     },
  //   ])
  // }, [])

  const onAddNewBookmark = useCallback(
    async (item: SearchLocationModel) => {
      try {
        const data = deepClone(item)
        // await SearchLocationService.addGoogleLatLngIfNeed(data)
        await SearchLocationService.getCrowdSourceParkingAvailability(data)
        RecentLocationService.addRecentItem(data)
        CollectionService.setRetainSavePendingOnInitialize(true)
        CollectionService.onCollectionDetailSearch(data)
        navigation.canGoBack() && navigation.goBack()
      } catch (error) {
        console.error('add New bookmark error: ', error)
      }
    },
    [navigation],
  )

  const resultListKeyExtractor = useCallback(
    (item: SearchLocationModel) => (item.placeId || item.addressid || '')?.toString() + item.type,
    [],
  )

  const renderResultItem: ListRenderItem<SearchLocationModel> = useCallback(
    ({ item, index }) => {
      return (
        <LocationSearchItemComponent
          index={index}
          data={item}
          onSelectItem={() =>
            debounce(() => {
              onAddNewBookmark(item)
            })
          }
        />
      )
    },
    [debounce, onAddNewBookmark],
  )

  const showRecent = useMemo(() => {
    return searchInput.trim().length < 1
  }, [searchInput])

  const showSearchResult = useMemo(() => {
    return searchInput.trim().length >= 1
  }, [searchInput])

  const showNotFoundWarning = useMemo(() => {
    return SearchLocationService.need2ShowNotFound({
      isSearching,
      searchTerm: searchInput,
      searchResult,
      keyboardShown,
    })
  }, [isSearching, searchInput, searchResult, keyboardShown])

  const ResultItemSeparator = useCallback(() => {
    return <View style={styles.resultItemSeparator} />
  }, [])

  const Header = useCallback(() => {
    return (
      <View style={[styles.headerContainer, { backgroundColor: themeColors.primaryBackground }]}>
        <View style={styles.titleContainer}>
          <ScreenHeader title="Add New" showShadow={false} onBack={onBackPress} />
        </View>
        <SearchInputComponent
          // value={searchInput}
          onChangeText={onSearchInputChange}
          placeholder="Search"
          containerStyle={styles.inputStyle}
          showIcon
        />
      </View>
    )
  }, [onBackPress])

  return (
    <TouchableWithoutFeedback
      onPress={() => {
        Keyboard.dismiss()
      }}>
      <SafeAreaView style={[styles.screenContainer, { backgroundColor: themeColors.primaryBackground }]}>
        <View style={{ overflow: 'hidden', paddingBottom: 5 }}>
          <Header />
        </View>
        {showRecent && (
          <FlatList
            data={RecentLocationService.listData.slice(0, 3)}
            keyExtractor={resultListKeyExtractor}
            renderItem={renderResultItem}
            ItemSeparatorComponent={ResultItemSeparator}
          />
        )}
        {showSearchResult && (
          <>
            {searchResult.length > 0 && (
              <FlatList
                data={searchResult}
                keyExtractor={resultListKeyExtractor}
                renderItem={renderResultItem}
                ItemSeparatorComponent={ResultItemSeparator}
              />
            )}
            {showNotFoundWarning && <EmptyWarning />}
          </>
        )}
        <KeyboardSpacerInsertBottom />
      </SafeAreaView>
    </TouchableWithoutFeedback>
  )
}

const styles = StyleSheet.create({
  screenContainer: {
    flex: 1,
    backgroundColor: 'white',
  },
  headerContainer: {
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: 'black',
    shadowOpacity: 0.16,
    shadowOffset: { width: 0, height: 3 },
    shadowRadius: 6,
  },
  titleContainer: {
    width: '100%',
    marginTop: 3,
  },
  headerTitle: {
    fontWeight: '500',
    fontSize: 22,
  },
  buttonBack: {
    position: 'absolute',
    left: 24,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonBackIcon: {
    width: 15,
    height: 15,
    resizeMode: 'contain',
  },
  inputStyle: {
    minHeight: 48,
    marginHorizontal: 24,
    marginBottom: 32,
  },
  resultItemSeparator: {
    borderTopWidth: 0.5,
    borderTopColor: '#999999',
    width: '100%',
  },
  emptyResultContainer: {
    width: '100%',
    borderTopColor: '#999999',
    borderTopWidth: 0.5,
    paddingLeft: 24,
    paddingTop: 11,
    marginTop: 24,
  },
  emptyResult: {
    color: '#E82370',
    fontSize: 14,
    fontWeight: '400',
    lineHeight: 17,
  },
})

export default CollectionSearchScreen

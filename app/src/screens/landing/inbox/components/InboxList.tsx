import React, { useState } from 'react'
import { FlatList } from 'react-native'
import { NotificationCategoryEnum, NotificationType } from '../../../../models/inbox-response'
import InboxMessagesService, { INBOXES_PER_PAGE } from '../../../../services/inbox-messages.service'
import InboxItem from './InboxItem'

type Props = {
  notificationType: NotificationCategoryEnum
  onSelectAdminNotification: (notification: NotificationType) => void
}

const InboxList: React.FC<Props> = ({ notificationType, onSelectAdminNotification }) => {
  const [isRefreshing, setIsRefreshing] = useState(false)

  const handleRefresh = () => {
    setIsRefreshing(true)
    InboxMessagesService.refresh().then(() => setIsRefreshing(false))
  }

  const loadMoreData = () => {
    if (InboxMessagesService.messageList.length >= INBOXES_PER_PAGE) {
      InboxMessagesService.loadMoreData()
    }
  }

  return (
    <FlatList
      data={
        notificationType === NotificationCategoryEnum.ALL
          ? InboxMessagesService.messageList
          : [...InboxMessagesService.messageList].filter(item => item.category === notificationType)
      }
      keyExtractor={item => `${item.notificationId}`}
      onRefresh={handleRefresh}
      refreshing={isRefreshing}
      renderItem={({ item }) => <InboxItem data={item} onSelectAdminNotification={onSelectAdminNotification} />}
      onEndReached={loadMoreData}
      onEndReachedThreshold={0.1}
    />
  )
}

export default InboxList

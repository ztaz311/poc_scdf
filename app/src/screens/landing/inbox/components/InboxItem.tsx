import { useNavigation } from '@react-navigation/native'
import { format } from 'date-fns'
import React from 'react'
import { Image, Pressable, StyleSheet, View } from 'react-native'
import { AppText } from '../../../../components'
import { BottomTabsEnum } from '../../../../components/Navigator/BottomTabs'
import { analyticEvents } from '../../../../constants/analyticEvents'
import { Colors } from '../../../../constants/appColors'
import { reactScreens } from '../../../../constants/screensName'
import { useTheme } from '../../../../contexts/ThemeContext'
import { NotificationCategoryEnum, NotificationType } from '../../../../models/inbox-response'
import InboxMessagesService from '../../../../services/inbox-messages.service'
import LandingDataService from '../../../../services/landing-data.service'
import NavigationDataService from '../../../../services/navigation-data.service'
import TripLogService from '../../../../services/triplog.service'

type Props = {
  data: NotificationType
  onSelectAdminNotification: (notification: NotificationType) => void
}

const InboxItem: React.FC<Props> = ({ data, onSelectAdminNotification }) => {
  const { isDarkMode, themeColors } = useTheme()
  const navigation = useNavigation()

  const handleClickItem = () => {
    NavigationDataService.sendAnalyticsEventsToNative(
      analyticEvents.user_click,
      analyticEvents.NotificationEvents.user_click.single_notification_click,
      analyticEvents.NotificationEvents.screen_name,
    )
    if (
      data.category === NotificationCategoryEnum.ADMIN ||
      data.category === NotificationCategoryEnum.TUTORIAL ||
      data.category === NotificationCategoryEnum.CUSTOM_INBOX
    ) {
      onSelectAdminNotification(data)
      navigation.navigate(reactScreens.Inbox)
    }
    if (data.category === NotificationCategoryEnum.TRAFFIC_ALERTS) {
      NavigationDataService.handleClickTrafficInbox(JSON.stringify(data))
      navigation.navigate(reactScreens.Home, { isNotLoad: true })
      setTimeout(() => {
        LandingDataService.updateBottomSheetIndex(1)
      }, 100)
    }
    if (data.category === NotificationCategoryEnum.VOUCHER) {
      navigation.navigate(BottomTabsEnum.Trends)
    }
    if (data.category === NotificationCategoryEnum.UPCOMING_TRIP) {
      TripLogService.openTripLogNative()
    }
    InboxMessagesService.updateSeenList(data.notificationId)
  }

  return (
    <Pressable style={styles.container} onPress={handleClickItem}>
      <Image
        source={{ uri: isDarkMode ? data.imageLinkDark : data.imageLinkLight }}
        style={{ width: 45, height: 45 }}
      />
      <View style={{ paddingLeft: 10, flex: 1 }}>
        <View style={styles.top}>
          <AppText
            style={{ color: themeColors.textColorForDark, fontSize: 16, fontWeight: '500', flexWrap: 'wrap', flex: 1 }}>
            {data.short_name}
          </AppText>
          <AppText style={[{ color: Colors.greyText, fontSize: 14 }, { color: themeColors.secondaryText }]}>
            {format(new Date(data.startTime * 1000), 'EE, dd MMM yyyy')}
          </AppText>
        </View>
        <AppText
          numberOfLines={2}
          style={{ width: '90%', fontSize: 14, marginTop: 8, fontWeight: '300', color: themeColors.textColorForDark }}>
          {data.message}
        </AppText>
      </View>
      {!data.seen && <View style={styles.dot} />}
    </Pressable>
  )
}

const styles = StyleSheet.create({
  container: {
    paddingTop: 16,
    paddingLeft: 24,
    paddingRight: 24,
    paddingBottom: 16,
    // minHeight: 104,
    flexDirection: 'row',
    borderBottomColor: '#999999',
    borderBottomWidth: 0.5,
  },
  top: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 2,
  },
  dot: {
    width: 12,
    height: 12,
    borderRadius: 6,
    backgroundColor: '#E82370',
    bottom: 16,
    right: 16,
    position: 'absolute',
  },
})

export default InboxItem

import React from 'react'
import { Pressable, StyleSheet, View } from 'react-native'
import { AppText } from '../../../../components'
import Divider from '../../../../components/Divider'
import { Colors } from '../../../../constants/appColors'
import { deviceHeight } from '../../../../constants/constants'
import { useTheme } from '../../../../contexts/ThemeContext'
import { NotificationCategoryEnum } from '../../../../models/inbox-response'

type Props = {
  isVisible: boolean
  onSelectNotificationType: (value: NotificationCategoryEnum) => void
  onClose: () => void
}

export const notificationTypeList = [
  {
    title: 'All Notifications',
    value: NotificationCategoryEnum.ALL,
  },
  {
    title: 'Traffic Alerts',
    value: NotificationCategoryEnum.TRAFFIC_ALERTS,
  },
  {
    title: 'Administrative Messages',
    value: NotificationCategoryEnum.ADMIN,
  },
  {
    title: 'Upcoming Trips',
    value: NotificationCategoryEnum.UPCOMING_TRIP,
  },
]

const FilterModal: React.FC<Props> = ({ isVisible, onSelectNotificationType, onClose }) => {
  const { themeColors } = useTheme()
  return isVisible ? (
    <View style={[{ flex: 1, backgroundColor: 'transparent' }, StyleSheet.absoluteFill]}>
      <View style={{ height: deviceHeight, backgroundColor: '#222638', opacity: 0.7 }} />
      <View style={styles.bottom}>
        <View style={[styles.header, { backgroundColor: themeColors.backgroundColorModal }]}>
          <AppText style={[{ fontSize: 16, fontWeight: '500' }, { color: themeColors.textColorForDark }]}>
            Select type of notification
          </AppText>
          <Pressable hitSlop={{ top: 16, bottom: 16, left: 16, right: 16 }} onPress={onClose}>
            <AppText style={[{ fontWeight: '500' }, { color: themeColors.primaryColor }]}>Close</AppText>
          </Pressable>
        </View>
        <Divider customStyle={{ backgroundColor: themeColors.borderBottomColor }} />
        {notificationTypeList.map((item, index) => (
          <Pressable
            style={[
              styles.item,
              {
                backgroundColor: themeColors.backgroundColorModal,
                borderBottomColor: themeColors.borderBottomItemColor,
              },
            ]}
            key={index}
            onPress={() => onSelectNotificationType(item.value)}>
            <AppText style={[{ fontSize: 16, fontWeight: '500' }, { color: themeColors.primaryColor }]}>
              {item.title}
            </AppText>
          </Pressable>
        ))}
      </View>
    </View>
  ) : null
}

const styles = StyleSheet.create({
  bottom: {
    height: 320,
    width: '100%',
    position: 'absolute',
    bottom: 0,
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingLeft: 32,
    paddingRight: 24,
    paddingVertical: 24,
    backgroundColor: Colors.white,
    borderTopStartRadius: 16,
    borderTopEndRadius: 16,
  },
  item: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 64,
    borderBottomColor: '#DDDDDD',
    borderBottomWidth: 1,
    backgroundColor: Colors.white,
  },
})

export default FilterModal

import React, { useMemo } from 'react'
import { ScrollView, View } from 'react-native'
import WebView from 'react-native-webview'
import { AppText } from '../../../../components'
import { analyticEvents } from '../../../../constants/analyticEvents'
import { deviceWidth } from '../../../../constants/constants'
import { useTheme } from '../../../../contexts/ThemeContext'
import { NotificationType, TutorialExtraData } from '../../../../models/inbox-response'
import NavigationDataService from '../../../../services/navigation-data.service'

type Props = {
  notificationData: NotificationType
}

const TutorialDetailsContent: React.FC<Props> = ({ notificationData }) => {
  const descriptions = useMemo(
    () => JSON.parse(notificationData.description) as TutorialExtraData,
    [notificationData.description],
  )
  const { themeColors } = useTheme()
  return (
    <ScrollView contentContainerStyle={{ paddingHorizontal: 24 }}>
      <View>
        {descriptions.description.length > 0 &&
          descriptions.description.map((text, index) => (
            <AppText key={`text-${index}`} style={[{ fontSize: 16 }, { color: themeColors.textColorForDark }]}>
              {text}
              {'\n'}
            </AppText>
          ))}
        {descriptions.videos &&
          descriptions.videos.length > 0 &&
          descriptions.videos.map((videoData, index) => {
            return (
              <View key={`web-${index}`}>
                <AppText
                  style={[
                    { fontSize: 16, fontWeight: 'bold', marginBottom: 16 },
                    { color: themeColors.textColorForDark },
                  ]}>
                  {videoData.title}
                </AppText>
                <View style={{ height: (deviceWidth / 424) * 148, flex: 0, marginBottom: 24 }}>
                  <WebView
                    onTouchEnd={() => {
                      NavigationDataService.sendAnalyticsEventsToNative(
                        analyticEvents.user_click,
                        analyticEvents.InboxDetailEvents.user_click.play_video,
                        analyticEvents.InboxDetailEvents.screen_name,
                      )
                      NavigationDataService.sendAnalyticsEventsToNative(
                        analyticEvents.statistics,
                        analyticEvents.user_click,
                        analyticEvents.InboxDetailEvents.screen_name,
                        false,
                        {
                          link_value: videoData.video_url,
                          message: notificationData.title,
                        },
                      )
                    }}
                    androidHardwareAccelerationDisabled={true}
                    allowsFullscreenVideo={true}
                    cacheEnabled={false}
                    scrollEnabled={false}
                    mediaPlaybackRequiresUserAction={false}
                    source={{
                      uri: `${videoData.weblink}?thumbnail=${videoData.thumbnail_url}&videoSrc=${videoData.video_url}`,
                    }}
                  />
                </View>
              </View>
            )
          })}
      </View>
    </ScrollView>
  )
}

export default TutorialDetailsContent

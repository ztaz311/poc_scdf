import { format } from 'date-fns'
import React, { useEffect } from 'react'
import { Dimensions, Pressable, StyleSheet, View } from 'react-native'
import { AppText } from '../../../components'
import { analyticEvents } from '../../../constants/analyticEvents'
import { Colors } from '../../../constants/appColors'
import { bottomTabsHeight, deviceHeight } from '../../../constants/constants'
import { useTheme } from '../../../contexts/ThemeContext'
import { NotificationCategoryEnum, NotificationType } from '../../../models/inbox-response'
import { baseURL } from '../../../services/http-client.service'
import NavigationDataService from '../../../services/navigation-data.service'
import InboxWebview from './InboxWebview'
import TutorialDetailsContent from './components/TutorialDetailsContent'

type Props = {
  data: NotificationType
  onClose: () => void
}

const InboxDetail: React.FC<Props> = ({ data, onClose }) => {
  const { themeColors } = useTheme()
  useEffect(() => {
    NavigationDataService.sendAnalyticsEventsScreenNameToNative(analyticEvents.InboxDetailEvents.screen_name)
  }, [])

  const handleClose = () => {
    NavigationDataService.sendAnalyticsEventsToNative(
      analyticEvents.user_click,
      analyticEvents.InboxDetailEvents.user_click.close,
      analyticEvents.InboxDetailEvents.screen_name,
    )
    onClose()
  }

  const renderContent = () => {
    if (data.category === NotificationCategoryEnum.CUSTOM_INBOX) {
      return (
        <InboxWebview
          renderUrl={`${baseURL}user/inbox/messages/${data.notificationId}`}
          messageData={{
            category: data.category,
            type: data.type,
          }}
        />
      )
    } else if (data.category === NotificationCategoryEnum.TUTORIAL) {
      return <TutorialDetailsContent notificationData={data} />
    }
    return (
      <AppText style={[{ fontSize: 16, paddingHorizontal: 24 }, { color: themeColors.textColorForDark }]}>
        {data.description}
      </AppText>
    )
  }

  if (!data) {
    return null
  }

  return (
    <View style={[{ flex: 1, backgroundColor: 'transparent' }, StyleSheet.absoluteFill]}>
      <View style={{ height: deviceHeight, backgroundColor: '#222638', opacity: 0.7 }} />
      <View style={[styles.bottom, { backgroundColor: themeColors.backgroundColorModal }]}>
        <View style={styles.header}>
          <AppText style={[{ fontSize: 16, fontWeight: '500' }, { color: themeColors.textColorForDark }]}>
            {data.short_name}
          </AppText>
          <Pressable hitSlop={{ top: 16, bottom: 16, right: 16, left: 16 }} onPress={handleClose}>
            <AppText style={{ color: themeColors.primaryColor, fontWeight: '500' }}>Close</AppText>
          </Pressable>
        </View>
        <AppText
          style={[
            { color: themeColors.greyText, fontSize: 12, marginBottom: 24, marginTop: 12, paddingHorizontal: 24 },
            { color: themeColors.secondaryText },
          ]}>
          {format(new Date(data.startTime * 1000), 'EE, dd MMM yyyy')}
        </AppText>
        {renderContent()}
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  bottom: {
    height: Dimensions.get('screen').height - bottomTabsHeight - 100,
    position: 'absolute',
    bottom: 0,
    width: '100%',
    paddingVertical: 24,
    backgroundColor: Colors.white,
    borderTopStartRadius: 16,
    borderTopEndRadius: 16,
  },
  header: {
    paddingHorizontal: 24,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  item: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 64,
    borderBottomColor: '#DDDDDD',
    borderBottomWidth: 1,
  },
})

export default InboxDetail

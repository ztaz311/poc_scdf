import { RouteProp } from '@react-navigation/native'
import { StackNavigationProp } from '@react-navigation/stack'
import { observer } from 'mobx-react'
import React, { useEffect, useState } from 'react'
import { Image, Pressable, StyleSheet, View } from 'react-native'
import { SafeAreaView } from 'react-native-safe-area-context'
import { AppText } from '../../../components'
import ScreenHeader from '../../../components/ScreenHeader'
import { analyticEvents } from '../../../constants/analyticEvents'
import { Colors } from '../../../constants/appColors'
import { Images } from '../../../constants/appImages'
import { useTheme } from '../../../contexts/ThemeContext'
import { VoucherTabsEnum } from '../../../enums/voucher-tabs.enum'
import { NotificationCategoryEnum, NotificationType } from '../../../models/inbox-response'
import { RootStackParamList } from '../../../models/root-stack-param-list'
import InboxMessagesService from '../../../services/inbox-messages.service'
import NavigationDataService from '../../../services/navigation-data.service'
import VoucherService from '../../../services/voucher.service'
import FilterModal, { notificationTypeList } from './components/FilterModal'
import InboxList from './components/InboxList'
import InboxDetail from './InboxDetail'

type MessageListRouteProp = RouteProp<RootStackParamList, 'Inbox'>

type MessageListNavigationProp = StackNavigationProp<RootStackParamList, 'Inbox'>

type Props = {
  route: MessageListRouteProp
  navigation: MessageListNavigationProp
}

const InboxListScreen: React.FC<Props> = ({ route, navigation }) => {
  const [isShowFilter, setIsShowFilter] = useState(false)
  const [notificationType, setNotificationType] = useState<NotificationCategoryEnum>(NotificationCategoryEnum.ALL)
  const { themeColors } = useTheme()
  const [selectedNotification, setSelectedNotification] = useState<NotificationType | undefined>(undefined)
  useEffect(() => {
    route.params && setSelectedNotification(route.params.notificationData)
  }, [route.params])
  useEffect(() => {
    InboxMessagesService.fetchMessageList()
    VoucherService.changeActiveTab(VoucherTabsEnum.Vouchers)
    NavigationDataService.sendAnalyticsEventsScreenNameToNative(analyticEvents.NotificationEvents.screen_name)
  }, [])

  const handleSelectNotificationType = (value: NotificationCategoryEnum) => {
    setNotificationType(value)
    setIsShowFilter(false)
    NavigationDataService.sendAnalyticsEventsToNative(
      analyticEvents.user_click,
      getEventValue(value),
      analyticEvents.NotificationEvents.screen_name,
    )
  }

  const getEventValue = (value: NotificationCategoryEnum) => {
    switch (value) {
      case NotificationCategoryEnum.ADMIN:
        return analyticEvents.NotificationEvents.user_click.filter_admin_msg
      case NotificationCategoryEnum.TRAFFIC_ALERTS:
        return analyticEvents.NotificationEvents.user_click.filter_traffic_alerts
      case NotificationCategoryEnum.UPCOMING_TRIP:
        return analyticEvents.NotificationEvents.user_click.filter_upcoming_trips
      default:
        return analyticEvents.NotificationEvents.user_click.filter_all_notifications
    }
  }

  const handleSelectAdminNotification = (notification: NotificationType) => {
    setSelectedNotification(notification)
  }

  const handleFilter = () => {
    setIsShowFilter(true)
    NavigationDataService.sendAnalyticsEventsToNative(
      analyticEvents.user_click,
      analyticEvents.NotificationEvents.user_click.all_notification_filter,
      analyticEvents.NotificationEvents.screen_name,
    )
  }

  const handleClose = () => {
    setIsShowFilter(false)
    NavigationDataService.sendAnalyticsEventsToNative(
      analyticEvents.user_click,
      analyticEvents.NotificationEvents.user_click.filter_close,
      analyticEvents.NotificationEvents.screen_name,
    )
  }

  return (
    <SafeAreaView style={[styles.container, { backgroundColor: themeColors.primaryBackground }]}>
      <ScreenHeader title="Inbox" hasBackButton={false} />
      <View style={[styles.listHeader, { backgroundColor: themeColors.primaryBackground }]}>
        <Pressable
          onPress={handleFilter}
          style={styles.buttonBack}
          hitSlop={{ top: 20, right: 20, bottom: 20, left: 20 }}>
          <Image source={Images.filter} />
          <AppText style={[styles.filterTitle, { color: themeColors.primaryColor }]}>
            {notificationTypeList.find(item => item.value === notificationType)?.title}
          </AppText>
        </Pressable>
      </View>
      <InboxList notificationType={notificationType} onSelectAdminNotification={handleSelectAdminNotification} />
      <FilterModal
        isVisible={isShowFilter}
        onSelectNotificationType={handleSelectNotificationType}
        onClose={handleClose}
      />
      {selectedNotification && (
        <InboxDetail
          data={selectedNotification}
          onClose={() => {
            navigation.navigate('Inbox', { notificationData: undefined })
            setSelectedNotification(undefined)
          }}
        />
      )}
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: '#fff' },

  buttonBack: { flexDirection: 'row', justifyContent: 'center', alignItems: 'center' },
  buttonBackIcon: {
    width: 15,
    height: 15,
    resizeMode: 'contain',
  },
  listHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 32,
    paddingTop: 20,
    paddingBottom: 12,
    borderBottomWidth: 0.5,
    backgroundColor: '#FCFDFC',
    borderBottomColor: '#999999',
  },
  filterTitle: { fontSize: 16, color: Colors.primaryColor, marginLeft: 20, fontWeight: '500' },
})

export default observer(InboxListScreen)

import { useNavigation } from '@react-navigation/native'
import { observer } from 'mobx-react'
import React, { useCallback, useEffect, useState } from 'react'
import { Linking } from 'react-native'
import InAppBrowser from 'react-native-inappbrowser-reborn'
import { SafeAreaView } from 'react-native-safe-area-context'
import WebView, { WebViewMessageEvent } from 'react-native-webview'
import { useTheme } from '../../../contexts/ThemeContext'
import AuthService from '../../../services/auth.service'
import InboxMessagesService, {
  CustomWebviewEvent,
  WebLinkNavigationEvent,
} from '../../../services/inbox-messages.service'
import NavigationDataService from '../../../services/navigation-data.service'

type Props = {
  renderUrl: string
  messageData: {
    category: string
    type: string
  }
}

const InboxWebview: React.FC<Props> = ({ renderUrl, messageData }) => {
  const [idToken, setIdToken] = useState('')
  const { isDarkMode } = useTheme()
  const navigation = useNavigation<any>()
  const breezeMessage = `${messageData.category}@${messageData.type}@${isDarkMode ? 'dark' : 'light'}`
  useEffect(() => {
    AuthService.getIdToken().then(token => {
      setIdToken(token)
    })
  }, [])

  const handleAnalytics = useCallback((analyticsData: any) => {
    NavigationDataService.sendAnalyticsEventsToNative(
      analyticsData.eventName,
      analyticsData.eventValue,
      analyticsData.screenName,
      analyticsData.saveToLocal ? analyticsData.saveToLocal : false,
      analyticsData.extraData,
    )
  }, [])

  const openURLExternally = (message: WebLinkNavigationEvent) => {
    const url = message.data.url
    Linking.canOpenURL(url).then(canOpen => {
      if (canOpen) {
        Linking.openURL(url)
      }
    })
  }

  const openURLInAppBrowser = (message: WebLinkNavigationEvent) => {
    const url = message.data.url
    InAppBrowser.isAvailable()
      .then(() => {
        InAppBrowser.open(url, {
          // iOS Properties
          dismissButtonStyle: 'cancel',
          preferredBarTintColor: '#453AA4',
          preferredControlTintColor: 'white',
          readerMode: false,
          animated: true,
          modalPresentationStyle: 'fullScreen',
          modalTransitionStyle: 'coverVertical',
          modalEnabled: true,
          enableBarCollapsing: false,
          // Android Properties
          showTitle: true,
          toolbarColor: '#6200EE',
          secondaryToolbarColor: 'black',
          navigationBarColor: 'black',
          navigationBarDividerColor: 'white',
          enableUrlBarHiding: true,
          enableDefaultShare: true,
          forceCloseOnRedirection: false,
          // Specify full animation resource identifier(package:anim/name)
          // or only resource name(in case of animation bundled with app).
          animations: {
            startEnter: 'slide_in_right',
            startExit: 'slide_out_left',
            endEnter: 'slide_in_left',
            endExit: 'slide_out_right',
          },
        }).catch(error => {
          console.log('error', error)
          Linking.canOpenURL(url).then(() => {
            Linking.openURL(url)
          })
        })
      })
      .catch(reason => {
        console.log('inappbrowser not available', reason)
        Linking.canOpenURL(url).then(() => {
          Linking.openURL(url)
        })
      })
  }

  const handleWebLinkNavigation = useCallback((message: WebLinkNavigationEvent) => {
    switch (message.data.type) {
      case 'external_browser':
        openURLExternally(message)
        break
      case 'in_app_browser':
        openURLInAppBrowser(message)
        break
      default:
        console.log('No url handler for type ', message.type)
        break
    }
  }, [])
  // logic to handle all messages go here
  const handleMessageFromWebview = useCallback(
    (event: WebViewMessageEvent) => {
      const message: CustomWebviewEvent = event.nativeEvent?.data ? JSON.parse(event.nativeEvent.data) : null
      if (!message) return
      switch (message.type) {
        case 'analytics':
          handleAnalytics(message.data)
          break
        case 'navigation':
          InboxMessagesService.handleInboxNavigation(message.data, navigation)
          break
        case 'open_link':
          handleWebLinkNavigation(message)
          break
        default:
          console.log('unknown message type: ', message.type)
      }
    },
    [handleAnalytics, handleWebLinkNavigation, navigation],
  )

  if (!idToken) return null
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <WebView
        onMessage={handleMessageFromWebview}
        onError={e => {
          console.log('webview error: ', e)
        }}
        onHttpError={e => {
          console.log('webview http error: ', e)
        }}
        androidHardwareAccelerationDisabled={true}
        allowsFullscreenVideo={true}
        cacheEnabled={false}
        mediaPlaybackRequiresUserAction={false}
        source={{
          uri: renderUrl,
          headers: {
            Authorization: `Bearer ${idToken}`,
            'breeze-message': breezeMessage,
            username: 'breeze-app',
          },
        }}
      />
    </SafeAreaView>
  )
}

export default observer(InboxWebview)

import { useNavigation } from '@react-navigation/native'
import { observer } from 'mobx-react'
import React, { useEffect } from 'react'
import { Image, Linking, NativeModules, Pressable, StyleSheet, View } from 'react-native'
import { AppText } from '../../../components'
import { analyticEvents } from '../../../constants/analyticEvents'
import { Colors } from '../../../constants/appColors'
import { Images } from '../../../constants/appImages'
import { PANEL_HEIGHT } from '../../../constants/constants'
import { nativeScreens, reactScreens } from '../../../constants/screensName'
import { useTheme } from '../../../contexts/ThemeContext'
import { VoucherTabsEnum } from '../../../enums/voucher-tabs.enum'
import AlertService from '../../../services/alert.service'
import NavigationDataService from '../../../services/navigation-data.service'
import OBUSettingsService from '../../../services/obu-settings.service'
import OBUService from '../../../services/obu.service'
import PreferencesSettingsService from '../../../services/preferences-settings.service'
import VoucherService from '../../../services/voucher.service'

type Props = {}

const MorePanelScreen: React.FC<Props> = () => {
  const navigation = useNavigation()
  const { themeColors } = useTheme()

  useEffect(() => {
    OBUSettingsService.getDefaultVehicleSettings()
    PreferencesSettingsService.getSettingsDataFromNative()
    NavigationDataService.adjustRecenterNative(PANEL_HEIGHT, 0, reactScreens.More)
    VoucherService.changeActiveTab(VoucherTabsEnum.Vouchers)
  }, [])

  const openSettings = () => {
    NavigationDataService.sendAnalyticsEventsToNative(
      analyticEvents.user_click,
      analyticEvents.More.user_click.breeze_settings,
      analyticEvents.More.screen_name,
    )
    NavigationDataService.openNativeScreen(nativeScreens.Settings)
  }

  const onOpenFeedBack = () => {
    NavigationDataService.sendAnalyticsEventsToNative(
      analyticEvents.user_click,
      analyticEvents.More.user_click.feedback,
      analyticEvents.More.screen_name,
    )
    NativeModules.CommunicateWithNative.appearInstabug()
  }

  const openExploreMap = () => {
    NavigationDataService.sendAnalyticsEventsToNative(
      analyticEvents.user_click,
      analyticEvents.More.user_click.explore_map,
      analyticEvents.More.screen_name,
    )
    NavigationDataService.openNativeScreen(nativeScreens.Explore)
  }

  const openVouchers = () => {
    NavigationDataService.sendAnalyticsEventsToNative(
      analyticEvents.user_click,
      analyticEvents.More.user_click.my_vouchers,
      analyticEvents.More.screen_name,
    )
    navigation.navigate(reactScreens.Voucher)
  }

  const openOBUSettings = () => {
    NavigationDataService.sendAnalyticsEventsToNative(
      analyticEvents.user_click,
      analyticEvents.More.user_click.obu_settings,
      analyticEvents.More.screen_name,
    )
    navigation.navigate(reactScreens.OBUSettingsStack)
  }

  const openNextgenEPR = async () => {
    const url = OBUService.registerLink
    AlertService.alert(
      '',
      'ERP2 Digital Services website will be opened in a new browser window. Please complete registration of your mobile device using the "Manage On-Board Unit" digital service.',
      [
        {
          text: 'Cancel',
          onPress: () => {
            NavigationDataService.sendAnalyticsEventsToNative(
              analyticEvents.user_click,
              analyticEvents.More.user_click.erp2Cancel,
              analyticEvents.More.screen_name,
            )
          },
        },
        {
          text: 'OK',
          onPress: () => {
            NavigationDataService.sendAnalyticsEventsToNative(
              analyticEvents.user_click,
              analyticEvents.More.user_click.erp2Ok,
              analyticEvents.More.screen_name,
            )
            Linking.canOpenURL(url).then(canOpen => {
              if (canOpen) {
                Linking.openURL(url)
              }
            })
          },
        },
      ],
    )
  }

  return (
    <View style={styles.container} pointerEvents="box-none">
      <View style={[styles.wrapContent, { backgroundColor: themeColors.primaryBackground }]}>
        <View style={styles.wrapHead}>
          <AppText style={[styles.txtMore, { color: themeColors.primaryText }]}>More</AppText>
        </View>
        <View style={styles.wrapItems}>
          <View style={[styles.item, { alignItems: 'flex-start' }]}>
            <Pressable style={styles.btnItem} onPress={openSettings}>
              <View style={styles.buttonIconContainer}>
                <Image source={Images.setting} style={[styles.buttonIcon, { tintColor: themeColors.primaryColor }]} />
              </View>
              <AppText style={[styles.buttonLabel, { color: themeColors.greyText }]}>{'Breeze\nSettings'}</AppText>
            </Pressable>
          </View>
          <View style={styles.item}>
            <Pressable style={styles.btnItem} onPress={openOBUSettings}>
              <View style={styles.buttonIconContainer}>
                <Image
                  source={Images.icOBUSetting}
                  style={[styles.buttonIcon, { tintColor: themeColors.primaryColor }]}
                />
              </View>
              <AppText style={[styles.buttonLabel, { color: themeColors.greyText }]}>{'OBU\nSettings'}</AppText>
            </Pressable>
          </View>
          <View style={[styles.item, { alignItems: 'flex-end' }]}>
            <Pressable style={styles.btnItem} onPress={openVouchers}>
              <View style={styles.buttonIconContainer}>
                <Image
                  source={Images.icMyVouchers}
                  style={[styles.buttonIcon, { tintColor: themeColors.primaryColor }]}
                />
              </View>
              <AppText style={[styles.buttonLabel, { color: themeColors.greyText }]}>My Vouchers</AppText>
            </Pressable>
          </View>
          <View style={[styles.item, { alignItems: 'flex-start' }]}>
            <Pressable style={styles.btnItem} onPress={openExploreMap}>
              <View style={styles.buttonIconContainer}>
                <Image
                  source={Images.icExploreMap}
                  style={[styles.buttonIcon, { tintColor: themeColors.primaryColor }]}
                />
              </View>
              <AppText style={[styles.buttonLabel, { color: themeColors.greyText }]}>Explore Map</AppText>
            </Pressable>
          </View>
          <View style={styles.item}>
            <Pressable style={styles.btnItem} onPress={onOpenFeedBack}>
              <View style={styles.buttonIconContainer}>
                <Image
                  source={Images.icFeedback}
                  style={[styles.buttonIcon, [{ tintColor: themeColors.primaryColor }]]}
                />
              </View>
              <AppText style={[styles.buttonLabel, { color: themeColors.greyText }]}>Feedback</AppText>
            </Pressable>
          </View>
          {!!OBUService.registerLink && (
            <View style={[styles.item, { alignItems: 'flex-end' }]}>
              <Pressable style={styles.btnItem} onPress={openNextgenEPR}>
                <View style={styles.buttonIconContainer}>
                  <Image
                    source={Images.icNextGen}
                    style={[styles.buttonIcon, [{ tintColor: themeColors.primaryColor }]]}
                  />
                </View>
                <AppText style={[styles.buttonLabel, { color: themeColors.greyText }]}>ERP 2</AppText>
              </Pressable>
            </View>
          )}
        </View>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column-reverse',
  },
  wrapContent: {
    height: PANEL_HEIGHT,
    borderTopEndRadius: 16,
    borderTopStartRadius: 16,
  },
  buttonLabel: {
    fontWeight: 'normal',
    fontSize: 14,
    marginTop: 8,
    textAlign: 'center',
  },
  wrapHead: {
    marginVertical: 24,
    marginHorizontal: 24,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  wrapItems: {
    flexDirection: 'row',
    marginTop: 8,
    marginHorizontal: 32,
    alignSelf: 'stretch',
    flexWrap: 'wrap',
  },
  btnItem: {
    alignItems: 'center',
  },
  buttonContainer: {
    width: 64,
    height: 64,
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonIcon: {
    width: 35,
    height: 35,
    resizeMode: 'contain',
  },
  buttonIconContainer: {
    borderRadius: 16,
    borderWidth: 0.3,
    padding: 16,
    borderColor: Colors.primaryColor,
  },
  item: {
    width: '33.33%',
    marginBottom: 29,
  },
  redDot: {
    width: 14,
    height: 14,
    borderRadius: 7,
    backgroundColor: '#E82370',
    position: 'absolute',
    right: -3,
    top: -3,
  },
  txtMore: {
    fontWeight: '700',
    flex: 1,
    fontSize: 18,
  },
})

export default observer(MorePanelScreen)

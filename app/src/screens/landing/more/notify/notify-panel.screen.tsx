import { useNavigation } from '@react-navigation/native'
import { observer } from 'mobx-react'
import React, { useEffect } from 'react'
import { Image, Pressable, View } from 'react-native'
import { FlatList } from 'react-native-gesture-handler'
import { AppText } from '../../../../components'
import { analyticEvents } from '../../../../constants/analyticEvents'
import { Colors } from '../../../../constants/appColors'
import { Images } from '../../../../constants/appImages'
import { cruiseMode, deviceWidth, PANEL_HEIGHT } from '../../../../constants/constants'
import { reactScreens } from '../../../../constants/screensName'
import { useTheme } from '../../../../contexts/ThemeContext'
import ETAEditorService from '../../../../services/eta-editor.service'
import ETAFavouriteLocationService from '../../../../services/eta-fav.service'
import NavigationDataService from '../../../../services/navigation-data.service'
import UserService from '../../../../services/user.service'
import ETAFavouriteApiModel from '../../../../models/eta-fav-api.model'

type Props = {}

const NotifyScreen: React.FC<Props> = () => {
  const navigation = useNavigation<any>()
  const { themeColors } = useTheme()

  useEffect(() => {
    NavigationDataService.adjustRecenterNative(PANEL_HEIGHT, 0, reactScreens.More)
    ETAFavouriteLocationService.getETAFavouriteList()
  }, [])

  const renderEmptyList = () => {
    return (
      <View>
        <Pressable
          style={{
            alignItems: 'center',
            justifyContent: 'center',
            marginStart: 40,
            alignSelf: 'flex-start',
          }}
          onPress={() => {
            NavigationDataService.sendAnalyticsEventsToNative(
              analyticEvents.user_click,
              analyticEvents.HomeEvents.user_click.notify_add,
              analyticEvents.HomeEvents.screen_name,
            )
            addNewETA()
          }}>
          <View
            style={{
              borderColor: themeColors.primaryColor,
              borderRadius: 200,
              borderWidth: 1,
              padding: 16,
            }}>
            <Image source={Images.add} style={{ width: 16, height: 16 }} />
          </View>
          <AppText style={{ color: themeColors.primaryText, textAlign: 'center' }}>New arrival{'\n'}message</AppText>
        </Pressable>
      </View>
    )
  }

  const addNewETA = async () => {
    const localUserData = await UserService.getLocalUserData()
    const userName = localUserData?.userName
    const intro = `${userName ? `Hello, ${userName} here!` : 'Hello, '}`
    const message = `${intro} I am on my way to...`
    ETAEditorService.setLastDestination(null, message)
    navigation.navigate(reactScreens.ETAFavouriteList, {
      etaMode: cruiseMode,
      etaMessage: message,
    })
  }

  const renderETAItem = ({ item, index }: { item: ETAFavouriteApiModel; index: number }) => {
    return (
      <View style={{ width: (deviceWidth - 48) / 3, paddingHorizontal: 2 }}>
        <Pressable
          style={{ alignItems: 'center', justifyContent: 'center' }}
          onPress={() => {
            ETAFavouriteLocationService.startETANative(ETAFavouriteLocationService.listData[index])
          }}>
          <View
            style={{
              alignContent: 'center',
              alignItems: 'center',
              justifyContent: 'center',
              borderRadius: 200,
              backgroundColor: Colors.primaryColor,
              width: 64,
              height: 64,
            }}>
            <AppText
              style={{
                textAlign: 'center',
                padding: 2,
                fontSize: 24,
                fontWeight: 'normal',
                color: 'white',
              }}>
              {`${item.recipientName ? item.recipientName.charAt(0) : ' '}`.toUpperCase()}
            </AppText>
          </View>

          <AppText
            style={{ color: themeColors.primaryText, textAlign: 'center', fontSize: 16, fontWeight: '500' }}
            numberOfLines={1}>
            {item.recipientName}
          </AppText>
          <AppText style={{ color: themeColors.greyText, textAlign: 'center', fontSize: 14 }} numberOfLines={1}>
            {item.destination}
          </AppText>
        </Pressable>
      </View>
    )
  }

  return (
    <View style={{ flex: 1, flexDirection: 'column-reverse' }} pointerEvents="box-none">
      <View
        style={{
          backgroundColor: themeColors.primaryBackground,
          height: PANEL_HEIGHT,
          borderTopEndRadius: 16,
          borderTopStartRadius: 16,
        }}>
        <View
          style={{
            marginVertical: 24,
            marginHorizontal: 32,
            justifyContent: 'center',
            alignItems: 'center',
            flexDirection: 'row',
          }}>
          <AppText style={{ color: themeColors.primaryText, fontWeight: '700', flex: 1, fontSize: 18 }}>
            Notify my arrival
          </AppText>
          {ETAFavouriteLocationService?.listData?.length ? (
            <Pressable
              onPress={() => {
                NavigationDataService.sendAnalyticsEventsToNative(
                  analyticEvents.user_click,
                  analyticEvents.ETACruiseNotify.userclick.view_all_arrival,
                  analyticEvents.ETACruiseNotify.screen_name,
                )
                addNewETA()
              }}>
              <AppText style={{ color: themeColors.primaryColor, paddingVertical: 8, fontWeight: '600', fontSize: 16 }}>
                Edit
              </AppText>
            </Pressable>
          ) : null}
        </View>
        {ETAFavouriteLocationService.listData.length > 0 ? (
          <FlatList
            style={{ marginHorizontal: 24 }}
            numColumns={3}
            data={ETAFavouriteLocationService.listData.slice()}
            renderItem={renderETAItem}
            keyExtractor={(item, index) => `${index}`}
          />
        ) : (
          renderEmptyList()
        )}
      </View>
    </View>
  )
}

export default observer(NotifyScreen)

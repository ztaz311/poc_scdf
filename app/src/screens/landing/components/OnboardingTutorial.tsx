import React, { useEffect, useRef, useState } from 'react'
import { NativeModules, Pressable } from 'react-native'
import AppText from '../../../components/AppText'
import { getResolutionImages } from '../../../utils/getResolutionImages'

const { tutorialStep1, tutorialStep2, tutorialStep3, tutorialStep4 } = getResolutionImages()

type Props = {
  onSkip: () => void
}

const communicateWithNative = NativeModules.CommunicateWithNative

const TOTAL_PAGES = 4

const OnboardingTutorial: React.FC<Props> = ({ onSkip }) => {
  const carousel = useRef<any>(null)
  const countToEnd = useRef<any>(null)
  const [slideIndex, setSlideIndex] = useState<number>(0)

  useEffect(() => {
    communicateWithNative.disableCruise(true)
    return () => {
      clearTimeout(countToEnd.current)
    }
  }, [])

  const handleClick = () => {
    // setSlideIndex(slideIndex > 0 ? slideIndex - 1 : slideIndex)
    // clearTimeout(countToEnd.current)
    // carousel.current?.snapToPrev()
    if (slideIndex < TOTAL_PAGES - 1) {
      setSlideIndex(slideIndex < TOTAL_PAGES - 1 ? slideIndex + 1 : slideIndex)
      carousel.current?.snapToNext()
    } else {
      onSkip()
    }
  }
  // const handleClickRight = () => {
  //   if (slideIndex < TOTAL_PAGES - 1) {
  //     setSlideIndex(slideIndex < TOTAL_PAGES - 1 ? slideIndex + 1 : slideIndex)
  //     carousel.current?.snapToNext()
  //   } else {
  //     onSkip()
  //   }
  // }

  return (
    <>
      <Pressable onPress={onSkip} style={{ position: 'absolute', top: 48, right: 16 }}>
        <AppText style={{ color: 'white' }}>
          {slideIndex === TOTAL_PAGES - 1 ? 'End Tutorial' : 'Skip Tutorial'}
        </AppText>
      </Pressable>
    </>
  )
}

export default OnboardingTutorial
